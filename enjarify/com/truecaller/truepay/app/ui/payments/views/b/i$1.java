package com.truecaller.truepay.app.ui.payments.views.b;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ImageView;
import com.truecaller.truepay.app.ui.payments.c.p;

final class i$1
  implements TextWatcher
{
  i$1(i parami, ImageView paramImageView1, ImageView paramImageView2, TextInputLayout paramTextInputLayout) {}
  
  public final void afterTextChanged(Editable paramEditable)
  {
    Object localObject1 = d.l;
    Object localObject2 = paramEditable.toString();
    ((p)localObject1).b((String)localObject2);
    int i = paramEditable.length();
    int j = 13;
    if (i == j)
    {
      localObject1 = d;
      localObject2 = a;
      ImageView localImageView = b;
      TextInputLayout localTextInputLayout = c;
      i.a((i)localObject1, paramEditable, (ImageView)localObject2, localImageView, localTextInputLayout);
      return;
    }
    d.l.j();
    a.setVisibility(0);
    b.setVisibility(8);
  }
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.i.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */