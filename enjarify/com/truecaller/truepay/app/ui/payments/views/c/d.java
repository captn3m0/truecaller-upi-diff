package com.truecaller.truepay.app.ui.payments.views.c;

import com.truecaller.truepay.app.ui.transaction.b.o;
import com.truecaller.truepay.app.ui.transaction.b.p;
import java.util.List;

public abstract interface d
  extends com.truecaller.truepay.app.ui.base.views.a
{
  public abstract void a(int paramInt, String paramString);
  
  public abstract void a(int paramInt, boolean paramBoolean);
  
  public abstract void a(com.truecaller.truepay.app.ui.payments.models.a parama);
  
  public abstract void a(com.truecaller.truepay.app.ui.payments.models.a parama1, com.truecaller.truepay.app.ui.payments.models.a parama2, boolean paramBoolean, com.truecaller.truepay.app.ui.payments.models.a parama3, String paramString);
  
  public abstract void a(com.truecaller.truepay.app.ui.payments.models.a parama, String paramString1, boolean paramBoolean1, boolean paramBoolean2, String paramString2);
  
  public abstract void a(o paramo, p paramp);
  
  public abstract void a(com.truecaller.truepay.data.api.model.a parama);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString1, String paramString2, String paramString3);
  
  public abstract void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5);
  
  public abstract void a(List paramList);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract com.truecaller.truepay.app.ui.payments.models.a b(int paramInt);
  
  public abstract void b();
  
  public abstract void b(int paramInt, String paramString);
  
  public abstract void b(int paramInt, boolean paramBoolean);
  
  public abstract void b(com.truecaller.truepay.app.ui.payments.models.a parama);
  
  public abstract void b(com.truecaller.truepay.app.ui.payments.models.a parama1, com.truecaller.truepay.app.ui.payments.models.a parama2);
  
  public abstract void b(p paramp);
  
  public abstract void b(com.truecaller.truepay.data.api.model.a parama);
  
  public abstract void b(String paramString);
  
  public abstract void b(List paramList);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c();
  
  public abstract void c(int paramInt, String paramString);
  
  public abstract void c(com.truecaller.truepay.app.ui.payments.models.a parama);
  
  public abstract void c(p paramp);
  
  public abstract void c(String paramString);
  
  public abstract void c(boolean paramBoolean);
  
  public abstract boolean c(int paramInt);
  
  public abstract int d();
  
  public abstract String d(int paramInt);
  
  public abstract void d(String paramString);
  
  public abstract void d(boolean paramBoolean);
  
  public abstract void e();
  
  public abstract void e(int paramInt);
  
  public abstract void e(String paramString);
  
  public abstract void e(boolean paramBoolean);
  
  public abstract void f(String paramString);
  
  public abstract void f(boolean paramBoolean);
  
  public abstract boolean f(int paramInt);
  
  public abstract String g(int paramInt);
  
  public abstract void g(String paramString);
  
  public abstract void g(boolean paramBoolean);
  
  public abstract void h();
  
  public abstract void i();
  
  public abstract void j();
  
  public abstract int k();
  
  public abstract void l();
  
  public abstract void m();
  
  public abstract void n();
  
  public abstract void o();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.c.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */