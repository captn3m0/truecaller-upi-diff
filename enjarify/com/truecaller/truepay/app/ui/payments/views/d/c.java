package com.truecaller.truepay.app.ui.payments.views.d;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.base.views.c.a;
import com.truecaller.truepay.app.ui.history.views.a.e;
import com.truecaller.truepay.app.ui.history.views.c.b;
import java.text.DecimalFormat;

public final class c
  extends a
  implements View.OnClickListener
{
  public final ImageView b;
  public final TextView c;
  public final TextView d;
  public final TextView e;
  public final b f;
  public final DecimalFormat g;
  
  public c(View paramView, f paramf, b paramb)
  {
    super(paramView, paramf);
    paramf = new java/text/DecimalFormat;
    paramf.<init>("#,###.##");
    g = paramf;
    int i = R.id.im_profile_pic;
    paramf = (ImageView)paramView.findViewById(i);
    b = paramf;
    i = R.id.tv_person_name;
    paramf = (TextView)paramView.findViewById(i);
    c = paramf;
    i = R.id.tv_payment_status;
    paramf = (TextView)paramView.findViewById(i);
    d = paramf;
    i = R.id.tv_payment_amount;
    paramf = (TextView)paramView.findViewById(i);
    e = paramf;
    f = paramb;
    paramView.setOnClickListener(this);
  }
  
  public final void onClick(View paramView)
  {
    int i = getAdapterPosition();
    if (i >= 0)
    {
      paramView = (e)a;
      int j = getAdapterPosition();
      paramView.a(j);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */