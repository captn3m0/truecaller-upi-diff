package com.truecaller.truepay.app.ui.payments.views.b;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.j;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.util.SortedList;
import android.support.v7.util.SortedList.Callback;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.menu;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.history.models.o;
import com.truecaller.truepay.app.ui.payments.a.f.d;
import com.truecaller.truepay.app.ui.payments.a.f.f;
import com.truecaller.truepay.app.ui.payments.b.a.a;
import com.truecaller.truepay.app.ui.payments.c.i.a;
import com.truecaller.truepay.app.ui.payments.c.i.b;
import com.truecaller.truepay.app.ui.payments.views.activities.PaymentsActivity;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.truepay.app.utils.z;
import com.truecaller.utils.extensions.t;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class f
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements SearchView.OnQueryTextListener, i.b, e.b
{
  public i.a a;
  RecyclerView b;
  TextView c;
  Toolbar d;
  public r e;
  public z f;
  f.a g;
  private List h;
  private List i;
  private com.truecaller.truepay.app.ui.payments.models.a j;
  private boolean k;
  private com.truecaller.truepay.app.ui.payments.models.a l;
  private com.truecaller.truepay.app.ui.payments.models.a n;
  private com.truecaller.truepay.app.ui.payments.models.a o;
  private com.truecaller.truepay.app.ui.payments.a.f p;
  private SortedList q;
  private boolean r;
  private String s;
  private String t = "utilities";
  private HashMap u;
  private com.truecaller.truepay.app.ui.payments.views.c.f v;
  private final f.d w;
  private final f.f x;
  private final SortedList.Callback y;
  
  public f()
  {
    Object localObject = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$f$MCoVHajbe480aI7ZLzIciBEt3mY;
    ((-..Lambda.f.MCoVHajbe480aI7ZLzIciBEt3mY)localObject).<init>(this);
    w = ((f.d)localObject);
    localObject = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$f$1ojXoQGWjKa57RCSfzsVRofSydU;
    ((-..Lambda.f.1ojXoQGWjKa57RCSfzsVRofSydU)localObject).<init>(this);
    x = ((f.f)localObject);
    localObject = new com/truecaller/truepay/app/ui/payments/views/b/f$1;
    ((f.1)localObject).<init>(this);
    y = ((SortedList.Callback)localObject);
  }
  
  public static f a(com.truecaller.truepay.app.ui.payments.models.a parama1, com.truecaller.truepay.app.ui.payments.models.a parama2, boolean paramBoolean1, boolean paramBoolean2, com.truecaller.truepay.app.ui.payments.models.a parama3, String paramString1, HashMap paramHashMap, String paramString2)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putBoolean("should_select_circle", paramBoolean1);
    localBundle.putBoolean("show_opr_first", paramBoolean2);
    localBundle.putSerializable("utility_entry", parama3);
    localBundle.putSerializable("operator_key", parama1);
    localBundle.putSerializable("location_key", parama2);
    localBundle.putSerializable("operator_symbol", paramString1);
    localBundle.putSerializable("utility_fields_map", paramHashMap);
    localBundle.putString("recharge_context_key", paramString2);
    parama1 = new com/truecaller/truepay/app/ui/payments/views/b/f;
    parama1.<init>();
    parama1.setArguments(localBundle);
    return parama1;
  }
  
  public final int a()
  {
    return R.layout.fragment_operator_selection;
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    f.a locala = g;
    if (locala != null)
    {
      com.truecaller.truepay.app.ui.payments.models.a locala1 = j;
      locala.a(locala1, parama);
      parama = getFragmentManager();
      if (parama != null)
      {
        parama = getFragmentManager();
        parama.c();
      }
    }
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.models.a parama1, com.truecaller.truepay.app.ui.history.models.h paramh, HashMap paramHashMap, com.truecaller.truepay.app.ui.payments.models.a parama2)
  {
    String str1 = w.r;
    String str2 = w.k;
    String str3 = t;
    parama1 = i.b(parama2, parama1, paramHashMap, str1, str2, str3);
    paramh = v;
    paramHashMap = Boolean.TRUE;
    paramh.a(parama1, paramHashMap);
  }
  
  public final void a(ArrayList paramArrayList)
  {
    q.replaceAll(paramArrayList);
  }
  
  public final void a(List paramList)
  {
    q.replaceAll(paramList);
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject = c;
    int m = 0;
    int i1;
    if (paramBoolean) {
      i1 = 0;
    } else {
      i1 = 8;
    }
    ((TextView)localObject).setVisibility(i1);
    localObject = b;
    if (paramBoolean) {
      m = 8;
    }
    ((RecyclerView)localObject).setVisibility(m);
  }
  
  public final List b()
  {
    boolean bool1 = getArguments().getBoolean("should_select_circle");
    k = bool1;
    bool1 = getArguments().getBoolean("show_opr_first");
    r = bool1;
    Object localObject1 = getArguments();
    Object localObject2 = "utility_entry";
    localObject1 = (com.truecaller.truepay.app.ui.payments.models.a)((Bundle)localObject1).getSerializable((String)localObject2);
    l = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1);
    bool1 = r;
    if (!bool1)
    {
      bool1 = k;
      if (bool1)
      {
        localObject1 = (com.truecaller.truepay.app.ui.payments.models.a)getArguments().getSerializable("operator_key");
        n = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1);
        localObject1 = getArguments();
        localObject2 = "location_key";
        localObject1 = (com.truecaller.truepay.app.ui.payments.models.a)((Bundle)localObject1).getSerializable((String)localObject2);
        o = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1);
        localObject1 = n.n;
        h = ((List)localObject1);
        break label396;
      }
    }
    localObject1 = l.n.iterator();
    boolean bool2;
    boolean bool3;
    for (;;)
    {
      bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = (com.truecaller.truepay.app.ui.payments.models.a)((Iterator)localObject1).next();
      localObject3 = "operator";
      localObject4 = f;
      bool3 = ((String)localObject3).equalsIgnoreCase((String)localObject4);
      if (bool3)
      {
        n = ((com.truecaller.truepay.app.ui.payments.models.a)localObject2);
        localObject2 = n;
        h = ((List)localObject2);
      }
    }
    localObject1 = s;
    if (localObject1 != null)
    {
      localObject1 = h.iterator();
      do
      {
        bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        localObject2 = (com.truecaller.truepay.app.ui.payments.models.a)((Iterator)localObject1).next();
        localObject3 = s;
        localObject4 = d;
        bool3 = ((String)localObject3).equalsIgnoreCase((String)localObject4);
      } while (!bool3);
      j = ((com.truecaller.truepay.app.ui.payments.models.a)localObject2);
      localObject4 = l;
      localObject5 = j;
      HashMap localHashMap = u;
      String str1 = s;
      String str2 = t;
      localObject1 = i.a((com.truecaller.truepay.app.ui.payments.models.a)localObject4, (com.truecaller.truepay.app.ui.payments.models.a)localObject5, localHashMap, str1, null, str2);
      localObject2 = v;
      localObject3 = Boolean.TRUE;
      ((com.truecaller.truepay.app.ui.payments.views.c.f)localObject2).a((i)localObject1, (Boolean)localObject3);
      getArguments().remove("operator_symbol");
      localObject1 = getArguments();
      localObject2 = "utility_fields_map";
      bool1 = ((Bundle)localObject1).containsKey((String)localObject2);
      if (bool1)
      {
        localObject1 = getArguments();
        localObject2 = "utility_fields_map";
        ((Bundle)localObject1).remove((String)localObject2);
      }
      bool1 = false;
      localObject1 = null;
      s = null;
      u = null;
    }
    label396:
    localObject1 = new android/support/v7/util/SortedList;
    Object localObject3 = y;
    ((SortedList)localObject1).<init>(com.truecaller.truepay.app.ui.payments.c.h.class, (SortedList.Callback)localObject3);
    q = ((SortedList)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/payments/a/f;
    localObject2 = q;
    localObject3 = w;
    Object localObject4 = x;
    Object localObject5 = e;
    ((com.truecaller.truepay.app.ui.payments.a.f)localObject1).<init>((SortedList)localObject2, (f.d)localObject3, (f.f)localObject4, (r)localObject5);
    p = ((com.truecaller.truepay.app.ui.payments.a.f)localObject1);
    return h;
  }
  
  public final com.truecaller.truepay.app.ui.payments.models.a c()
  {
    return l;
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getActivity();
    boolean bool = paramContext instanceof PaymentsActivity;
    if (bool)
    {
      paramContext = (PaymentsActivity)getActivity();
      v = paramContext;
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setHasOptionsMenu(true);
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    int m = R.menu.menu_operator_selection;
    paramMenuInflater.inflate(m, paramMenu);
    m = R.id.action_search;
    MenuItem localMenuItem = paramMenu.findItem(m);
    SearchView localSearchView = (SearchView)localMenuItem.getActionView();
    int i1 = R.string.bank_selection_search;
    String str = getString(i1);
    localSearchView.setQueryHint(str);
    localSearchView.setOnQueryTextListener(this);
    localMenuItem.setVisible(true);
    m = R.id.search_edit_frame;
    findViewByIdgetLayoutParamsleftMargin = 0;
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    Object localObject1 = com.truecaller.truepay.app.ui.payments.b.a.a();
    Object localObject2 = Truepay.getApplicationComponent();
    ((a.a)localObject1).a((com.truecaller.truepay.app.a.a.a)localObject2).a().a(this);
    a.a(this);
    localObject1 = getArguments();
    if (localObject1 != null)
    {
      localObject1 = getArguments().getString("operator_symbol");
      s = ((String)localObject1);
      localObject1 = (HashMap)getArguments().getSerializable("utility_fields_map");
      u = ((HashMap)localObject1);
      localObject1 = getArguments();
      String str = "utilities";
      localObject1 = ((Bundle)localObject1).getString("recharge_context_key", str);
      t = ((String)localObject1);
      localObject1 = a;
      localObject2 = t;
      ((i.a)localObject1).a((String)localObject2);
    }
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    i.a locala = a;
    if (locala != null) {
      locala.y_();
    }
  }
  
  public final void onDetach()
  {
    super.onDetach();
    v = null;
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    i.a locala = a;
    String str = l.g;
    locala.a(paramMenuItem, str);
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  public final boolean onQueryTextChange(String paramString)
  {
    a.b(paramString);
    return true;
  }
  
  public final boolean onQueryTextSubmit(String paramString)
  {
    return false;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    int m = R.id.rv_operator_list;
    Object localObject1 = (RecyclerView)paramView.findViewById(m);
    b = ((RecyclerView)localObject1);
    m = R.id.emptyText;
    localObject1 = (TextView)paramView.findViewById(m);
    c = ((TextView)localObject1);
    m = R.id.toolbar;
    localObject1 = (Toolbar)paramView.findViewById(m);
    d = ((Toolbar)localObject1);
    localObject1 = (AppCompatActivity)getActivity();
    Object localObject2 = d;
    ((AppCompatActivity)localObject1).setSupportActionBar((Toolbar)localObject2);
    localObject1 = ((AppCompatActivity)getActivity()).getSupportActionBar();
    if (localObject1 != null)
    {
      localObject2 = n.b;
      ((ActionBar)localObject1).setTitle((CharSequence)localObject2);
      bool = true;
      ((ActionBar)localObject1).setDisplayShowTitleEnabled(bool);
      ((ActionBar)localObject1).setDisplayHomeAsUpEnabled(bool);
    }
    localObject1 = d;
    localObject2 = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$f$PekV9NL53iu3hETxhokjvIVgm4E;
    ((-..Lambda.f.PekV9NL53iu3hETxhokjvIVgm4E)localObject2).<init>(this);
    ((Toolbar)localObject1).setNavigationOnClickListener((View.OnClickListener)localObject2);
    localObject1 = b;
    boolean bool = false;
    ((RecyclerView)localObject1).setItemAnimator(null);
    localObject1 = new com/truecaller/truepay/app/ui/base/widgets/a;
    localObject2 = getContext();
    ((com.truecaller.truepay.app.ui.base.widgets.a)localObject1).<init>((Context)localObject2);
    localObject2 = getContext();
    int i1 = R.drawable.divider_gray;
    localObject2 = android.support.v4.content.b.a((Context)localObject2, i1);
    if (localObject2 != null)
    {
      a = ((Drawable)localObject2);
      b.addItemDecoration((RecyclerView.ItemDecoration)localObject1);
      localObject1 = b;
      localObject2 = p;
      ((RecyclerView)localObject1).setAdapter((RecyclerView.Adapter)localObject2);
      super.onViewCreated(paramView, paramBundle);
      t.a(paramView, false, 2);
      return;
    }
    paramView = new java/lang/IllegalArgumentException;
    paramView.<init>("Drawable cannot be null.");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */