package com.truecaller.truepay.app.ui.payments.views.b;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.log.d;
import com.truecaller.truepay.R.anim;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.payments.b.a.a;
import com.truecaller.truepay.app.ui.transaction.b.o;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.utils.n;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public final class c
  extends android.support.design.widget.b
  implements com.truecaller.truepay.app.ui.payments.a.a, com.truecaller.truepay.app.ui.payments.views.c.b
{
  public static final c.b b;
  public com.truecaller.truepay.app.ui.payments.c.c a;
  private com.truecaller.truepay.app.ui.payments.views.a.b c;
  private com.truecaller.truepay.app.ui.payments.views.a.a d;
  private c.a e;
  private HashMap f;
  
  static
  {
    c.b localb = new com/truecaller/truepay/app/ui/payments/views/b/c$b;
    localb.<init>((byte)0);
    b = localb;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public static final c a(o paramo, p paramp)
  {
    k.b(paramo, "billFetchData");
    k.b(paramp, "txnModel");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramo = (Serializable)paramo;
    localBundle.putSerializable("bill_details", paramo);
    paramp = (Serializable)paramp;
    localBundle.putSerializable("bill_pay_txn_details", paramp);
    paramo = new com/truecaller/truepay/app/ui/payments/views/b/c;
    paramo.<init>();
    paramo.setArguments(localBundle);
    return paramo;
  }
  
  public final com.truecaller.truepay.app.ui.payments.c.c a()
  {
    com.truecaller.truepay.app.ui.payments.c.c localc = a;
    if (localc == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localc;
  }
  
  public final void a(o paramo, r paramr, n paramn)
  {
    k.b(paramo, "billDetails");
    k.b(paramr, "imageLoader");
    Object localObject1 = "resourceProvider";
    k.b(paramn, (String)localObject1);
    int i = R.id.tvBillAmount;
    paramn = (TextView)a(i);
    if (paramn != null)
    {
      j = R.string.prefixed_amount;
      k = 1;
      localObject2 = new Object[k];
      String str = paramo.e();
      localObject2[0] = str;
      localObject1 = (CharSequence)getString(j, (Object[])localObject2);
      paramn.setText((CharSequence)localObject1);
    }
    i = R.id.tvBillOperatorName;
    paramn = (TextView)a(i);
    if (paramn != null)
    {
      localObject1 = (CharSequence)paramo.d();
      paramn.setText((CharSequence)localObject1);
    }
    paramn = paramo.h();
    int j = R.id.ivBillOperator;
    localObject1 = (ImageView)a(j);
    Object localObject2 = "ivBillOperator";
    k.a(localObject1, (String)localObject2);
    int k = R.drawable.bank_connection_bg;
    paramr.a(paramn, (ImageView)localObject1, k, k);
    int m = R.id.tvBillDueDate;
    paramr = (TextView)a(m);
    if (paramr != null)
    {
      paramn = (CharSequence)paramo.f();
      paramr.setText(paramn);
    }
    m = R.id.tvBillDate;
    paramr = (TextView)a(m);
    if (paramr != null)
    {
      paramo = (CharSequence)paramo.g();
      paramr.setText(paramo);
      return;
    }
  }
  
  public final void a(p paramp)
  {
    k.b(paramp, "txnModel");
    c.a locala = e;
    if (locala != null)
    {
      locala.a(paramp);
      return;
    }
  }
  
  public final void a(r paramr, n paramn, au paramau)
  {
    k.b(paramr, "imageLoader");
    k.b(paramn, "resourceProvider");
    k.b(paramau, "stringUtils");
    Object localObject1 = new com/truecaller/truepay/app/ui/payments/views/a/b;
    Object localObject2 = this;
    localObject2 = (com.truecaller.truepay.app.ui.payments.a.a)this;
    ((com.truecaller.truepay.app.ui.payments.views.a.b)localObject1).<init>((com.truecaller.truepay.app.ui.payments.a.a)localObject2, paramr, paramn, paramau);
    c = ((com.truecaller.truepay.app.ui.payments.views.a.b)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/payments/views/a/a;
    ((com.truecaller.truepay.app.ui.payments.views.a.a)localObject1).<init>((com.truecaller.truepay.app.ui.payments.a.a)localObject2, paramr, paramn, paramau);
    d = ((com.truecaller.truepay.app.ui.payments.views.a.a)localObject1);
    int i = R.id.rvBillPresenterInlineBankList;
    paramr = (RecyclerView)a(i);
    paramn = new android/support/v7/widget/LinearLayoutManager;
    paramau = requireContext();
    localObject1 = null;
    boolean bool = true;
    paramn.<init>(paramau, 0, bool);
    paramn = (RecyclerView.LayoutManager)paramn;
    paramr.setLayoutManager(paramn);
    paramn = c;
    if (paramn == null)
    {
      paramau = "billPresenterInlineBankListAdapter";
      k.a(paramau);
    }
    paramn = (RecyclerView.Adapter)paramn;
    paramr.setAdapter(paramn);
    i = R.id.rvBillPresenterBankDropdownList;
    paramr = (RecyclerView)a(i);
    paramn = new android/support/v7/widget/LinearLayoutManager;
    paramau = requireContext();
    paramn.<init>(paramau);
    paramn = (RecyclerView.LayoutManager)paramn;
    paramr.setLayoutManager(paramn);
    paramn = d;
    if (paramn == null)
    {
      paramau = "billPresenterDropDownBankListAdapter";
      k.a(paramau);
    }
    paramn = (RecyclerView.Adapter)paramn;
    paramr.setAdapter(paramn);
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama, r paramr, String paramString)
  {
    k.b(parama, "selectedAccount");
    k.b(paramr, "imageLoader");
    k.b(paramString, "accountNumber");
    c();
    int i = R.id.ivBillPresenterBank;
    Object localObject1 = (ImageView)a(i);
    Object localObject2 = parama.l();
    String str = "selectedAccount.bank";
    k.a(localObject2, str);
    localObject2 = ((com.truecaller.truepay.data.d.a)localObject2).d();
    paramr = paramr.b((String)localObject2);
    ((ImageView)localObject1).setImageDrawable(paramr);
    int j = R.id.tvBillPresenterBankName;
    paramr = (TextView)a(j);
    k.a(paramr, "tvBillPresenterBankName");
    parama = parama.l();
    localObject1 = "selectedAccount.bank";
    k.a(parama, (String)localObject1);
    parama = (CharSequence)parama.b();
    paramr.setText(parama);
    parama = paramString;
    parama = (CharSequence)paramString;
    int k = parama.length();
    if (k == 0)
    {
      k = 1;
    }
    else
    {
      k = 0;
      parama = null;
    }
    if (k == 0)
    {
      k = R.id.tvBillPresenterBankName;
      parama = (TextView)a(k);
      paramString = String.valueOf(paramString);
      paramr = (CharSequence)" - ".concat(paramString);
      parama.append(paramr);
    }
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama, boolean paramBoolean)
  {
    Object localObject = "account";
    k.b(parama, (String)localObject);
    if (paramBoolean)
    {
      localc = a;
      if (localc == null)
      {
        localObject = "presenter";
        k.a((String)localObject);
      }
      localc.a();
    }
    else
    {
      localc = a;
      if (localc == null)
      {
        localObject = "presenter";
        k.a((String)localObject);
      }
      boolean bool = b;
      if (bool)
      {
        localObject = (com.truecaller.truepay.app.ui.payments.views.c.b)localc.af_();
        if (localObject != null)
        {
          ((com.truecaller.truepay.app.ui.payments.views.c.b)localObject).c();
          ((com.truecaller.truepay.app.ui.payments.views.c.b)localObject).d();
        }
        bool = false;
        localObject = null;
        b = false;
      }
    }
    com.truecaller.truepay.app.ui.payments.c.c localc = a;
    if (localc == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    localc.a(parama);
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "accountList");
    com.truecaller.truepay.app.ui.payments.views.a.b localb = c;
    if (localb == null)
    {
      String str = "billPresenterInlineBankListAdapter";
      k.a(str);
    }
    k.b(paramList, "currentItems");
    localb.a(paramList);
    localb.notifyDataSetChanged();
  }
  
  public final void b()
  {
    Object localObject = requireContext();
    int i = R.anim.rotate_up;
    localObject = AnimationUtils.loadAnimation((Context)localObject, i);
    i = R.id.ivArrow;
    ImageView localImageView = (ImageView)a(i);
    if (localImageView != null)
    {
      localImageView.startAnimation((Animation)localObject);
      return;
    }
  }
  
  public final void b(List paramList)
  {
    k.b(paramList, "accountList");
    com.truecaller.truepay.app.ui.payments.views.a.a locala = d;
    if (locala == null)
    {
      String str = "billPresenterDropDownBankListAdapter";
      k.a(str);
    }
    k.b(paramList, "currentItems");
    locala.a(paramList);
    locala.notifyDataSetChanged();
    int i = R.id.rvBillPresenterBankDropdownList;
    paramList = (RecyclerView)a(i);
    k.a(paramList, "rvBillPresenterBankDropdownList");
    paramList.setVisibility(8);
  }
  
  public final void c()
  {
    Object localObject = requireContext();
    int i = R.anim.rotate_down;
    localObject = AnimationUtils.loadAnimation((Context)localObject, i);
    i = R.id.ivArrow;
    ImageView localImageView = (ImageView)a(i);
    if (localImageView != null)
    {
      localImageView.startAnimation((Animation)localObject);
      return;
    }
  }
  
  public final void d()
  {
    int i = R.id.rvBillPresenterBankDropdownList;
    RecyclerView localRecyclerView = (RecyclerView)a(i);
    if (localRecyclerView != null)
    {
      int j = 8;
      localRecyclerView.setVisibility(j);
    }
    i = R.id.rvBillPresenterInlineBankList;
    localRecyclerView = (RecyclerView)a(i);
    if (localRecyclerView != null)
    {
      localRecyclerView.setVisibility(0);
      return;
    }
  }
  
  public final void e()
  {
    int i = R.id.rvBillPresenterBankDropdownList;
    RecyclerView localRecyclerView = (RecyclerView)a(i);
    if (localRecyclerView != null) {
      localRecyclerView.setVisibility(0);
    }
    i = R.id.rvBillPresenterInlineBankList;
    localRecyclerView = (RecyclerView)a(i);
    if (localRecyclerView != null)
    {
      localRecyclerView.setVisibility(8);
      return;
    }
  }
  
  public final void f()
  {
    int i = R.id.ivArrow;
    ImageView localImageView = (ImageView)a(i);
    if (localImageView != null)
    {
      localImageView.setVisibility(8);
      return;
    }
  }
  
  public final int getTheme()
  {
    return R.style.BottomSheetDialogTheme;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.payments.b.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new android/support/design/widget/a;
    Context localContext = requireContext();
    int i = getTheme();
    paramBundle.<init>(localContext, i);
    return (Dialog)paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i = R.layout.fragment_bill_presenter;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public final void onDismiss(DialogInterface paramDialogInterface)
  {
    super.onDismiss(paramDialogInterface);
    paramDialogInterface = a;
    if (paramDialogInterface == null)
    {
      String str = "presenter";
      k.a(str);
    }
    paramDialogInterface.b();
    e = null;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject1 = "view";
    k.b(paramView, (String)localObject1);
    super.onViewCreated(paramView, paramBundle);
    paramView = getArguments();
    if (paramView != null)
    {
      paramView = a;
      if (paramView == null)
      {
        paramBundle = "presenter";
        k.a(paramBundle);
      }
      paramBundle = this;
      paramBundle = (com.truecaller.truepay.app.ui.base.views.a)this;
      paramView.a(paramBundle);
      paramView = getArguments();
      paramBundle = null;
      int i;
      if (paramView != null)
      {
        localObject1 = "bill_details";
        paramView = paramView.getSerializable((String)localObject1);
      }
      else
      {
        i = 0;
        paramView = null;
      }
      if (paramView != null)
      {
        paramView = (o)paramView;
        localObject1 = getArguments();
        if (localObject1 != null) {
          paramBundle = ((Bundle)localObject1).getSerializable("bill_pay_txn_details");
        }
        if (paramBundle != null)
        {
          paramBundle = (p)paramBundle;
          localObject1 = a;
          if (localObject1 == null)
          {
            localObject2 = "presenter";
            k.a((String)localObject2);
          }
          k.b(paramView, "billDetails");
          Object localObject2 = "txnModel";
          k.b(paramBundle, (String)localObject2);
          a = paramBundle;
          paramBundle = a;
          if (paramBundle == null)
          {
            localObject2 = "txnModel";
            k.a((String)localObject2);
          }
          if (paramBundle != null)
          {
            localObject2 = paramView.e();
            paramBundle.e((String)localObject2);
          }
          paramBundle = a;
          if (paramBundle == null)
          {
            localObject2 = "txnModel";
            k.a((String)localObject2);
          }
          if (paramBundle != null)
          {
            localObject2 = paramView.f();
            paramBundle.s((String)localObject2);
          }
          b = false;
          paramBundle = (com.truecaller.truepay.app.ui.payments.views.c.b)((com.truecaller.truepay.app.ui.payments.c.c)localObject1).af_();
          n localn;
          if (paramBundle != null)
          {
            localObject2 = c;
            localn = f;
            au localau = h;
            paramBundle.a((r)localObject2, localn, localau);
          }
          paramBundle = (com.truecaller.truepay.app.ui.payments.views.c.b)((com.truecaller.truepay.app.ui.payments.c.c)localObject1).af_();
          if (paramBundle != null)
          {
            localObject2 = c;
            localn = f;
            paramBundle.a(paramView, (r)localObject2, localn);
          }
          paramView = g.c();
          k.a(paramView, "accountManager.primaryAccount");
          ((com.truecaller.truepay.app.ui.payments.c.c)localObject1).a(paramView);
          i = R.id.btnBillPayNow;
          paramView = (Button)a(i);
          paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/c$c;
          paramBundle.<init>(this);
          paramBundle = (View.OnClickListener)paramBundle;
          paramView.setOnClickListener(paramBundle);
          i = R.id.containerBillPresenterBankSelection;
          paramView = (ConstraintLayout)a(i);
          paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/c$d;
          paramBundle.<init>(this);
          paramBundle = (View.OnClickListener)paramBundle;
          paramView.setOnClickListener(paramBundle);
          paramView = e;
          if (paramView == null)
          {
            paramView = getTargetFragment();
            boolean bool = paramView instanceof c.a;
            if (bool)
            {
              paramView = (c.a)getTargetFragment();
              e = paramView;
              return;
            }
          }
          paramView = new java/lang/RuntimeException;
          paramBundle = new java/lang/StringBuilder;
          paramBundle.<init>("parent fragment should implement ");
          localObject1 = c.a.class.getSimpleName();
          paramBundle.append((String)localObject1);
          paramBundle = paramBundle.toString();
          paramView.<init>(paramBundle);
          throw ((Throwable)paramView);
        }
        paramView = new c/u;
        paramView.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.transaction.models.TransactionModel");
        throw paramView;
      }
      paramView = new c/u;
      paramView.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.transaction.models.RefIdHolderDO");
      throw paramView;
    }
    paramView = new java/lang/AssertionError;
    paramView.<init>("arguments null on BillShowBottomSheetFragment");
    d.a((Throwable)paramView);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */