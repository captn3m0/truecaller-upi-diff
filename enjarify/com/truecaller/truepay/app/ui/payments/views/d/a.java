package com.truecaller.truepay.app.ui.payments.views.d;

import android.view.View;
import android.view.View.OnClickListener;
import c.g.b.k;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.payments.a.d;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.r;

public final class a
  extends com.truecaller.truepay.app.ui.base.views.c.a
  implements View.OnClickListener
{
  public final r b;
  public final au c;
  
  public a(View paramView, f paramf, r paramr, au paramau)
  {
    super(paramView, paramf);
    b = paramr;
    c = paramau;
    paramf = this;
    paramf = (View.OnClickListener)this;
    paramView.setOnClickListener(paramf);
  }
  
  public final void onClick(View paramView)
  {
    String str = "view";
    k.b(paramView, str);
    int i = getAdapterPosition();
    if (i >= 0)
    {
      paramView = (d)a();
      int j = getAdapterPosition();
      paramView.a(j);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */