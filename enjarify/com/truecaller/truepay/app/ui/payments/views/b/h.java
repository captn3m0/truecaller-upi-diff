package com.truecaller.truepay.app.ui.payments.views.b;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.a.a.d.e;
import com.truecaller.truepay.app.ui.payments.b.a.a;
import com.truecaller.truepay.app.ui.payments.c.o.1;
import com.truecaller.truepay.app.ui.payments.views.c.c;
import com.truecaller.truepay.app.utils.ax;
import com.truecaller.truepay.data.api.model.al;
import io.reactivex.n;
import io.reactivex.q;
import java.util.List;

public final class h
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements c
{
  Toolbar a;
  TabLayout b;
  ViewPager c;
  TextView d;
  public ax e;
  public com.truecaller.truepay.app.ui.payments.c.o f;
  h.a g;
  private com.truecaller.truepay.app.ui.payments.views.c.f h;
  private com.truecaller.truepay.app.ui.payments.a.j i;
  private String j;
  private String k;
  private String l;
  
  public final int a()
  {
    return R.layout.fragment_payment_plans_list;
  }
  
  public final void a(List paramList)
  {
    int m = paramList.size();
    if (m > 0)
    {
      Object localObject = new com/truecaller/truepay/app/ui/payments/a/j;
      android.support.v4.app.j localj = getActivity().getSupportFragmentManager();
      h.1 local1 = new com/truecaller/truepay/app/ui/payments/views/b/h$1;
      local1.<init>(this);
      ((com.truecaller.truepay.app.ui.payments.a.j)localObject).<init>(localj, paramList, local1);
      i = ((com.truecaller.truepay.app.ui.payments.a.j)localObject);
      paramList = c;
      localObject = i;
      paramList.setAdapter((android.support.v4.view.o)localObject);
      paramList = b;
      localObject = c;
      paramList.setupWithViewPager((ViewPager)localObject);
      return;
    }
    b.setVisibility(8);
    d.setVisibility(0);
  }
  
  public final void a(boolean paramBoolean)
  {
    com.truecaller.truepay.app.ui.payments.views.c.f localf = h;
    if (localf != null)
    {
      if (paramBoolean)
      {
        localf.b();
        return;
      }
      localf.c();
    }
  }
  
  public final void b()
  {
    b.setVisibility(8);
    d.setVisibility(0);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getActivity();
    boolean bool = paramContext instanceof com.truecaller.truepay.app.ui.payments.views.c.f;
    if (bool)
    {
      paramContext = (com.truecaller.truepay.app.ui.payments.views.c.f)getActivity();
      h = paramContext;
      return;
    }
    paramContext = new java/lang/IllegalStateException;
    paramContext.<init>("Activity should implement PaymentsView");
    throw paramContext;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    a.a locala = com.truecaller.truepay.app.ui.payments.b.a.a();
    com.truecaller.truepay.app.a.a.a locala1 = Truepay.getApplicationComponent();
    locala.a(locala1).a().a(this);
    f.a(this);
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    f.b();
  }
  
  public final void onDetach()
  {
    super.onDetach();
    h = null;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    int m = R.id.toolbar;
    Object localObject1 = (Toolbar)paramView.findViewById(m);
    a = ((Toolbar)localObject1);
    m = R.id.tab_plans;
    localObject1 = (TabLayout)paramView.findViewById(m);
    b = ((TabLayout)localObject1);
    m = R.id.vp_plans;
    localObject1 = (ViewPager)paramView.findViewById(m);
    c = ((ViewPager)localObject1);
    m = R.id.tv_empty_text;
    localObject1 = (TextView)paramView.findViewById(m);
    d = ((TextView)localObject1);
    localObject1 = (AppCompatActivity)getActivity();
    Object localObject2 = a;
    ((AppCompatActivity)localObject1).setSupportActionBar((Toolbar)localObject2);
    localObject1 = ((AppCompatActivity)getActivity()).getSupportActionBar();
    boolean bool = true;
    if (localObject1 != null)
    {
      int n = R.string.select_a_plan;
      localObject3 = getString(n);
      ((ActionBar)localObject1).setTitle((CharSequence)localObject3);
      ((ActionBar)localObject1).setDisplayShowTitleEnabled(bool);
      ((ActionBar)localObject1).setDisplayHomeAsUpEnabled(bool);
    }
    localObject1 = a;
    Object localObject3 = new com/truecaller/truepay/app/ui/payments/views/b/-$$Lambda$h$rdQn0fi5lIsu_EaDyoi63CUnXo8;
    ((-..Lambda.h.rdQn0fi5lIsu_EaDyoi63CUnXo8)localObject3).<init>(this);
    ((Toolbar)localObject1).setNavigationOnClickListener((View.OnClickListener)localObject3);
    localObject1 = getArguments();
    if (localObject1 != null)
    {
      localObject1 = getArguments().getString("utility_type");
      j = ((String)localObject1);
      localObject1 = getArguments().getString("selected_operator_symbol");
      k = ((String)localObject1);
      localObject1 = getArguments().getString("selected_location_symbol");
      l = ((String)localObject1);
      localObject1 = f;
      localObject3 = j;
      String str1 = k;
      String str2 = l;
      Object localObject4 = d;
      if (localObject4 != null)
      {
        localObject4 = new com/truecaller/truepay/data/api/model/al;
        ((al)localObject4).<init>((String)localObject3, str1, str2);
        ((c)d).a(bool);
        localObject2 = a.a.a((al)localObject4);
        localObject3 = io.reactivex.g.a.b();
        localObject2 = ((io.reactivex.o)localObject2).b((n)localObject3);
        localObject3 = io.reactivex.android.b.a.a();
        localObject2 = ((io.reactivex.o)localObject2).a((n)localObject3);
        localObject3 = new com/truecaller/truepay/app/ui/payments/c/o$1;
        ((o.1)localObject3).<init>((com.truecaller.truepay.app.ui.payments.c.o)localObject1);
        ((io.reactivex.o)localObject2).a((q)localObject3);
      }
    }
    super.onViewCreated(paramView, paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */