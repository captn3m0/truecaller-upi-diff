package com.truecaller.truepay.app.ui.payments.views.b;

import android.support.constraint.ConstraintLayout;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import c.g.b.k;
import com.truecaller.truepay.R.id;

public final class o$b
  extends Animation
{
  o$b(o paramo, int paramInt) {}
  
  protected final void applyTransformation(float paramFloat, Transformation paramTransformation)
  {
    String str = "t";
    k.b(paramTransformation, str);
    float f = 1.0F;
    boolean bool = paramFloat < f;
    if (!bool)
    {
      localObject = a;
      i = R.id.transactionIdSection;
      localObject = (ConstraintLayout)((o)localObject).a(i);
      k.a(localObject, "transactionIdSection");
      ((ConstraintLayout)localObject).setVisibility(8);
      return;
    }
    paramTransformation = a;
    int j = R.id.transactionIdSection;
    paramTransformation = (ConstraintLayout)paramTransformation.a(j);
    k.a(paramTransformation, "transactionIdSection");
    paramTransformation = paramTransformation.getLayoutParams();
    j = b;
    int k = (int)(j * paramFloat);
    j -= k;
    height = j;
    Object localObject = a;
    int i = R.id.transactionIdSection;
    ((ConstraintLayout)((o)localObject).a(i)).requestLayout();
  }
  
  public final boolean willChangeBounds()
  {
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.o.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */