package com.truecaller.truepay.app.ui.payments.views.customviews;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Paint.Align;

public final class a$a
{
  public Integer a;
  public Integer b;
  public Integer c;
  public Float d;
  public Float e;
  public Bitmap f;
  public Boolean g;
  public final Paint h;
  public final Context i;
  
  public a$a(Context paramContext)
  {
    i = paramContext;
    paramContext = new android/graphics/Paint;
    paramContext.<init>();
    boolean bool = true;
    paramContext.setAntiAlias(bool);
    paramContext.setFilterBitmap(bool);
    paramContext.setDither(bool);
    Paint.Align localAlign = Paint.Align.CENTER;
    paramContext.setTextAlign(localAlign);
    h = paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.customviews.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */