package com.truecaller.truepay.app.ui.payments.views.c;

import com.truecaller.truepay.app.ui.payments.models.RedBusTicket;
import com.truecaller.truepay.app.ui.payments.views.activities.PaymentsActivity.a;
import com.truecaller.truepay.app.ui.payments.views.b.e;
import com.truecaller.truepay.app.ui.payments.views.b.h;
import com.truecaller.truepay.app.ui.transaction.b.p;

public abstract interface f
  extends com.truecaller.truepay.app.ui.base.views.a
{
  public abstract void a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(RedBusTicket paramRedBusTicket);
  
  public abstract void a(PaymentsActivity.a parama);
  
  public abstract void a(e parame);
  
  public abstract void a(com.truecaller.truepay.app.ui.payments.views.b.f paramf);
  
  public abstract void a(h paramh);
  
  public abstract void a(com.truecaller.truepay.app.ui.payments.views.b.i parami, Boolean paramBoolean);
  
  public abstract void a(p paramp);
  
  public abstract void a(com.truecaller.truepay.app.ui.transaction.views.a.i parami);
  
  public abstract void a(com.truecaller.truepay.data.api.model.a parama, String paramString);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString1, String paramString2, Boolean paramBoolean);
  
  public abstract void b();
  
  public abstract void b(com.truecaller.truepay.data.api.model.a parama, String paramString);
  
  public abstract void b(String paramString);
  
  public abstract void c();
  
  public abstract void d(p paramp);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.c.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */