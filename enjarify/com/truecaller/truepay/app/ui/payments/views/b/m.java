package com.truecaller.truepay.app.ui.payments.views.b;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.e;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.payments.b.a.a;
import com.truecaller.truepay.app.ui.payments.b.b;
import com.truecaller.truepay.app.ui.payments.c.z.b;
import com.truecaller.truepay.app.ui.payments.c.z.c;
import com.truecaller.truepay.app.ui.payments.models.RedBusTicket;
import com.truecaller.truepay.app.ui.payments.views.c.f;
import java.util.HashMap;
import java.util.List;

public final class m
  extends e
  implements z.c
{
  public static final m.a b;
  public z.b a;
  private com.truecaller.truepay.app.ui.payments.a.m c;
  private f d;
  private HashMap e;
  
  static
  {
    m.a locala = new com/truecaller/truepay/app/ui/payments/views/b/m$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      e = ((HashMap)localObject1);
    }
    localObject1 = e;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = e;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public static final m a(RedBusTicket paramRedBusTicket)
  {
    k.b(paramRedBusTicket, "ticketDetails");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramRedBusTicket = (Parcelable)paramRedBusTicket;
    localBundle.putParcelable("ticket_details", paramRedBusTicket);
    paramRedBusTicket = new com/truecaller/truepay/app/ui/payments/views/b/m;
    paramRedBusTicket.<init>();
    paramRedBusTicket.setArguments(localBundle);
    return paramRedBusTicket;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "operatorName");
    int i = R.id.tvOperatorName;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "tvOperatorName");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "policyList");
    Object localObject = new com/truecaller/truepay/app/ui/payments/a/m;
    Context localContext = getContext();
    ((com.truecaller.truepay.app.ui.payments.a.m)localObject).<init>(paramList, localContext);
    c = ((com.truecaller.truepay.app.ui.payments.a.m)localObject);
    int i = R.id.rvPolicy;
    paramList = (RecyclerView)a(i);
    if (paramList != null)
    {
      localObject = (RecyclerView.Adapter)c;
      paramList.setAdapter((RecyclerView.Adapter)localObject);
      return;
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getActivity();
    boolean bool = paramContext instanceof f;
    if (bool)
    {
      paramContext = (f)getActivity();
      d = paramContext;
      return;
    }
    paramContext = new java/lang/IllegalStateException;
    paramContext.<init>("Activity should implement PaymentsView");
    throw ((Throwable)paramContext);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.payments.b.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new android/app/Dialog;
    Object localObject = (Context)getActivity();
    int i = R.style.Base_ThemeOverlay_AppCompat_Dialog;
    paramBundle.<init>((Context)localObject, i);
    int j = 1;
    paramBundle.requestWindowFeature(j);
    i = R.layout.fragment_policy_details;
    paramBundle.setContentView(i);
    paramBundle.setCanceledOnTouchOutside(j);
    localObject = paramBundle.getWindow();
    k.a(localObject, "dialog.window");
    localObject = ((Window)localObject).getAttributes();
    gravity = 17;
    width = -1;
    height = -2;
    Window localWindow = paramBundle.getWindow();
    k.a(localWindow, "dialog.window");
    localWindow.setAttributes((WindowManager.LayoutParams)localObject);
    return paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    paramBundle = a;
    if (paramBundle == null)
    {
      String str = "presenter";
      k.a(str);
    }
    paramBundle.a(this);
    int i = R.layout.fragment_policy_details;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public final void onDetach()
  {
    super.onDetach();
    z.b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    if (localb != null) {
      localb.y_();
    }
    d = null;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = getArguments();
    if (paramView != null)
    {
      paramBundle = "ticket_details";
      paramView = paramView.getParcelable(paramBundle);
      if (paramView != null)
      {
        paramView = (RedBusTicket)paramView;
        paramBundle = a;
        if (paramBundle == null)
        {
          str = "presenter";
          k.a(str);
        }
        paramBundle.a(paramView);
        return;
      }
      paramView = new c/u;
      paramView.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.payments.models.RedBusTicket");
      throw paramView;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */