package com.truecaller.truepay.app.ui.payments.views.b;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.a;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.style;
import java.util.HashMap;

public final class b
  extends android.support.design.widget.b
{
  public static final b.a a;
  private HashMap b;
  
  static
  {
    b.a locala = new com/truecaller/truepay/app/ui/payments/views/b/b$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public static final b a(String paramString)
  {
    k.b(paramString, "message");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("bill_failure_message", paramString);
    paramString = new com/truecaller/truepay/app/ui/payments/views/b/b;
    paramString.<init>();
    paramString.setArguments(localBundle);
    return paramString;
  }
  
  public final int getTheme()
  {
    return R.style.BottomSheetDialogTheme;
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new android/support/design/widget/a;
    Context localContext = requireContext();
    int i = getTheme();
    paramBundle.<init>(localContext, i);
    return (Dialog)paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i = R.layout.fragment_bill_fetch_fail;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = getArguments();
    if (paramView != null)
    {
      paramBundle = "bill_failure_message";
      paramView = paramView.getString(paramBundle);
    }
    else
    {
      i = 0;
      paramView = null;
    }
    int j = R.id.tvBillFailureMessage;
    paramBundle = (TextView)a(j);
    if (paramBundle != null)
    {
      paramView = (CharSequence)paramView;
      paramBundle.setText(paramView);
    }
    int i = R.id.btnFetchRetry;
    paramView = (Button)a(i);
    if (paramView != null)
    {
      paramBundle = new com/truecaller/truepay/app/ui/payments/views/b/b$b;
      paramBundle.<init>(this);
      paramBundle = (View.OnClickListener)paramBundle;
      paramView.setOnClickListener(paramBundle);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.views.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */