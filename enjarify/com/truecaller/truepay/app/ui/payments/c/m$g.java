package com.truecaller.truepay.app.ui.payments.c;

import c.d.a.a;
import c.d.c;
import c.o.b;
import c.x;
import com.truecaller.common.h.q;
import e.b;
import e.r;
import java.util.HashMap;
import kotlinx.coroutines.ag;

final class m$g
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag d;
  
  m$g(m paramm, HashMap paramHashMap, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    g localg = new com/truecaller/truepay/app/ui/payments/c/m$g;
    m localm = b;
    HashMap localHashMap = c;
    localg.<init>(localm, localHashMap, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localg;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = m.f(b);
        localObject = c;
        paramObject = ((com.truecaller.truepay.data.api.h)paramObject).c((HashMap)localObject);
        localObject = "utilityApiService.validateUtility(enteredValues)";
        c.g.b.k.a(paramObject, (String)localObject);
        paramObject = q.a((b)paramObject);
        if (paramObject != null) {
          return (com.truecaller.truepay.data.api.model.h)((r)paramObject).e();
        }
        return null;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (g)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((g)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.m.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */