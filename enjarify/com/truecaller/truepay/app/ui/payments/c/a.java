package com.truecaller.truepay.app.ui.payments.c;

import c.g.b.k;
import c.n.m;
import c.x;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.a.a.d.d;
import com.truecaller.truepay.app.ui.transaction.b.l;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.utils.n;
import java.text.SimpleDateFormat;
import java.util.List;

public final class a
  extends com.truecaller.truepay.app.ui.base.a.a
{
  public final io.reactivex.a.a a;
  public final int b;
  int c;
  public final int f;
  public final int g;
  public String h;
  public final SimpleDateFormat i;
  public final p j;
  public final d k;
  public final com.truecaller.truepay.data.e.e l;
  public final au m;
  public final r n;
  public final com.truecaller.truepay.data.e.a o;
  public final n p;
  public final com.truecaller.featuretoggles.e q;
  private final int r;
  private final int s;
  private final int t;
  private final com.truecaller.truepay.data.e.a u;
  private final com.truecaller.truepay.app.utils.a v;
  private final com.truecaller.truepay.data.e.e w;
  private final com.truecaller.truepay.data.e.e x;
  
  public a(d paramd, com.truecaller.truepay.data.e.e parame1, au paramau, r paramr, com.truecaller.truepay.data.e.a parama1, com.truecaller.truepay.data.e.a parama2, com.truecaller.truepay.app.utils.a parama, com.truecaller.truepay.data.e.e parame2, com.truecaller.truepay.data.e.e parame3, n paramn, com.truecaller.featuretoggles.e parame)
  {
    k = paramd;
    l = parame1;
    m = paramau;
    n = paramr;
    o = parama1;
    u = parama2;
    v = parama;
    w = parame2;
    x = parame3;
    p = paramn;
    q = parame;
    paramd = new io/reactivex/a/a;
    paramd.<init>();
    a = paramd;
    b = 3;
    r = 2;
    int i1 = 1;
    s = i1;
    f = i1;
    h = "utilities";
    paramd = new java/text/SimpleDateFormat;
    paramd.<init>("yyyy-MM-dd HH:mm:ss");
    i = paramd;
    paramd = new com/truecaller/truepay/app/ui/transaction/b/p;
    paramd.<init>();
    j = paramd;
  }
  
  private final void a()
  {
    com.truecaller.truepay.app.ui.payments.views.c.a locala = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
    if (locala != null) {
      locala.r();
    }
    locala = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
    if (locala != null)
    {
      locala.g();
      return;
    }
  }
  
  private final void a(String paramString)
  {
    if (paramString != null)
    {
      int i1 = paramString.hashCode();
      String str1;
      switch (i1)
      {
      default: 
        break;
      case 1661903715: 
        localObject = "action.page.need_help";
        bool = paramString.equals(localObject);
        if (!bool) {
          break label539;
        }
        paramString = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
        if (paramString != null)
        {
          localObject = w.a();
          if (localObject == null) {
            k.a();
          }
          str1 = "prefFaqUrl.get()!!";
          k.a(localObject, str1);
          paramString.c((String)localObject);
        }
        return;
      case 970112032: 
        localObject = "action.share_receipt";
        bool = paramString.equals(localObject);
        if (!bool) {
          break label539;
        }
        break;
      case 872695663: 
        localObject = "action.page.forgot_upi_pin";
        bool = paramString.equals(localObject);
        if (!bool) {
          break label539;
        }
        break;
      case -399989823: 
        localObject = "action.check_status";
        bool = paramString.equals(localObject);
        if (!bool) {
          break label539;
        }
        paramString = (CharSequence)j.f();
        if (paramString != null)
        {
          bool = m.a(paramString);
          if (!bool)
          {
            bool = false;
            paramString = null;
            break label253;
          }
        }
        bool = true;
        if (bool) {
          return;
        }
        paramString = l.a();
        if (paramString != null)
        {
          localObject = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
          if (localObject != null)
          {
            str1 = j.f();
            k.a(str1, "transactionHolder.transactionId");
            String str2 = "userId";
            k.a(paramString, str2);
            ((com.truecaller.truepay.app.ui.payments.views.c.a)localObject).a(str1, paramString);
          }
        }
        return;
      case -721197651: 
        localObject = "action.page.reset_upi_pin";
        bool = paramString.equals(localObject);
        if (!bool) {
          break label539;
        }
        paramString = j.g();
        if (paramString != null)
        {
          paramString = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
          if (paramString != null)
          {
            localObject = j.g();
            k.a(localObject, "transactionHolder.account");
            paramString.b((com.truecaller.truepay.data.api.model.a)localObject);
            return;
          }
        }
        return;
      case -875490522: 
        localObject = "action.page.home";
        bool = paramString.equals(localObject);
        if (!bool) {
          break label539;
        }
        paramString = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
        if (paramString != null) {
          paramString.k();
        }
        return;
      case -1361457855: 
        localObject = "action.page.retry";
        bool = paramString.equals(localObject);
        if (!bool) {
          break label539;
        }
        paramString = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
        if (paramString != null) {
          paramString.t();
        }
        return;
      case -1635274183: 
        label253:
        localObject = "action.download_receipt";
        bool = paramString.equals(localObject);
        if (!bool) {
          break label539;
        }
        d();
        return;
      }
      Object localObject = "action.page.create_upi_pin";
      boolean bool = paramString.equals(localObject);
      if (bool)
      {
        paramString = j.g();
        if (paramString != null)
        {
          paramString = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
          if (paramString != null)
          {
            localObject = j.g();
            k.a(localObject, "transactionHolder.account");
            paramString.a((com.truecaller.truepay.data.api.model.a)localObject);
            return;
          }
        }
        return;
      }
    }
    label539:
    paramString = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
    if (paramString != null)
    {
      paramString.k();
      return;
    }
  }
  
  private final void d()
  {
    String str1 = au.b(v.d());
    String str2 = String.valueOf(x.a());
    Object localObject1 = j.g();
    Object localObject2 = null;
    if (localObject1 != null)
    {
      localObject1 = ((com.truecaller.truepay.data.api.model.a)localObject1).l();
      if (localObject1 != null)
      {
        localObject1 = ((com.truecaller.truepay.data.d.a)localObject1).b();
        break label56;
      }
    }
    localObject1 = null;
    label56:
    Object localObject3 = j.g();
    if (localObject3 != null) {
      localObject2 = ((com.truecaller.truepay.data.api.model.a)localObject3).d();
    }
    String str3 = au.a((String)localObject1, (String)localObject2);
    localObject1 = af_();
    localObject2 = localObject1;
    localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.a)localObject1;
    if (localObject2 != null)
    {
      localObject3 = j;
      k.a(str1, "bankRegisteredName");
      r localr = n;
      k.a(str3, "displayAccNo");
      ((com.truecaller.truepay.app.ui.payments.views.c.a)localObject2).a((p)localObject3, str1, localr, str2, str3);
      return;
    }
  }
  
  public final void a(int paramInt)
  {
    Object localObject1 = j.a();
    if (localObject1 != null) {
      localObject1 = ((l)localObject1).g();
    } else {
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      Object localObject2 = ((List)localObject1).get(paramInt);
      if (localObject2 != null)
      {
        Object localObject3 = ((List)localObject1).get(paramInt);
        k.a(localObject3, "actionData[buttonIndex]");
        localObject3 = ((com.truecaller.truepay.app.ui.history.models.a)localObject3).b();
        a((String)localObject3);
        return;
      }
    }
    a("action.page.home");
  }
  
  public final void a(l paraml)
  {
    k.b(paraml, "payResponseDO");
    Object localObject = paraml.g();
    if (localObject != null)
    {
      int i1 = ((List)localObject).size();
      int i2 = t;
      if (i1 == i2)
      {
        paraml = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
        if (paraml != null) {
          paraml.h();
        }
        return;
      }
      i2 = r;
      if (i1 == i2)
      {
        localObject = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
        if (localObject != null) {
          ((com.truecaller.truepay.app.ui.payments.views.c.a)localObject).a(paraml);
        }
        return;
      }
      i2 = s;
      if (i1 == i2)
      {
        localObject = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
        if (localObject != null) {
          ((com.truecaller.truepay.app.ui.payments.views.c.a)localObject).b(paraml);
        }
        return;
      }
      paraml = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
      if (paraml != null)
      {
        paraml.h();
        return;
      }
    }
  }
  
  public final void a(p paramp)
  {
    k.b(paramp, "txnModel");
    Object localObject1 = paramp.a();
    if (localObject1 != null)
    {
      localObject1 = ((l)localObject1).d();
      if (localObject1 == null) {
        localObject1 = "unknown";
      }
      int i1 = ((String)localObject1).hashCode();
      int i2 = -1867169789;
      boolean bool;
      if (i1 != i2)
      {
        i2 = -1086574198;
        if (i1 != i2)
        {
          i2 = -682587753;
          if (i1 != i2)
          {
            i2 = -284840886;
            if (i1 == i2)
            {
              localObject2 = "unknown";
              bool = ((String)localObject1).equals(localObject2);
              if (bool) {
                a();
              }
            }
          }
          else
          {
            localObject2 = "pending";
            bool = ((String)localObject1).equals(localObject2);
            if (bool)
            {
              k.b(paramp, "txnModel");
              localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
              if (localObject2 != null) {
                ((com.truecaller.truepay.app.ui.payments.views.c.a)localObject2).r();
              }
              localObject2 = paramp.a();
              if (localObject2 != null)
              {
                bool = paramp.p();
                if (bool)
                {
                  localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
                  if (localObject2 != null) {
                    ((com.truecaller.truepay.app.ui.payments.views.c.a)localObject2).d(paramp);
                  }
                }
                else
                {
                  localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
                  if (localObject2 != null) {
                    ((com.truecaller.truepay.app.ui.payments.views.c.a)localObject2).f();
                  }
                }
              }
            }
          }
        }
        else
        {
          localObject2 = "failure";
          bool = ((String)localObject1).equals(localObject2);
          if (bool)
          {
            k.b(paramp, "txnModel");
            localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
            if (localObject2 != null) {
              ((com.truecaller.truepay.app.ui.payments.views.c.a)localObject2).q();
            }
            bool = paramp.p();
            if (bool)
            {
              localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
              if (localObject2 != null) {
                ((com.truecaller.truepay.app.ui.payments.views.c.a)localObject2).c(paramp);
              }
            }
            else
            {
              localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
              if (localObject2 != null) {
                ((com.truecaller.truepay.app.ui.payments.views.c.a)localObject2).b(paramp);
              }
            }
          }
        }
      }
      else
      {
        localObject2 = "success";
        bool = ((String)localObject1).equals(localObject2);
        if (bool)
        {
          k.b(paramp, "txnModel");
          localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
          if (localObject2 != null) {
            ((com.truecaller.truepay.app.ui.payments.views.c.a)localObject2).p();
          }
          bool = paramp.p();
          if (bool)
          {
            localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
            if (localObject2 != null) {
              ((com.truecaller.truepay.app.ui.payments.views.c.a)localObject2).a(paramp);
            }
          }
          else
          {
            localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.a)af_();
            if (localObject2 != null) {
              ((com.truecaller.truepay.app.ui.payments.views.c.a)localObject2).e();
            }
          }
        }
      }
      Object localObject2 = Truepay.getInstance();
      k.a(localObject2, "Truepay.getInstance()");
      localObject2 = ((Truepay)localObject2).getAnalyticLoggerHelper();
      i2 = 0;
      Object localObject3 = null;
      if (localObject2 != null)
      {
        String str = paramp.c();
        com.truecaller.truepay.data.e.a locala = u;
        paramp = paramp.a();
        if (paramp != null) {
          localObject3 = paramp.h();
        }
        ((com.truecaller.truepay.data.b.a)localObject2).a((String)localObject1, str, locala, (String)localObject3);
        localObject3 = x.a;
      }
      if (localObject3 != null) {}
    }
    else
    {
      paramp = this;
      ((a)this).a();
      paramp = x.a;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */