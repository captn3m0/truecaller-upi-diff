package com.truecaller.truepay.app.ui.payments.c;

import dagger.a.d;
import javax.inject.Provider;

public final class ah
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  private final Provider g;
  private final Provider h;
  private final Provider i;
  private final Provider j;
  private final Provider k;
  private final Provider l;
  private final Provider m;
  private final Provider n;
  
  private ah(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7, Provider paramProvider8, Provider paramProvider9, Provider paramProvider10, Provider paramProvider11, Provider paramProvider12, Provider paramProvider13, Provider paramProvider14)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
    e = paramProvider5;
    f = paramProvider6;
    g = paramProvider7;
    h = paramProvider8;
    i = paramProvider9;
    j = paramProvider10;
    k = paramProvider11;
    l = paramProvider12;
    m = paramProvider13;
    n = paramProvider14;
  }
  
  public static ah a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7, Provider paramProvider8, Provider paramProvider9, Provider paramProvider10, Provider paramProvider11, Provider paramProvider12, Provider paramProvider13, Provider paramProvider14)
  {
    ah localah = new com/truecaller/truepay/app/ui/payments/c/ah;
    localah.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13, paramProvider14);
    return localah;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */