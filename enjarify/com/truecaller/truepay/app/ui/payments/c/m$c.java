package com.truecaller.truepay.app.ui.payments.c;

import c.d.c;
import c.d.f;
import c.o.b;
import c.x;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.transaction.b.j;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.data.api.model.h;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class m$c
  extends c.d.b.a.k
  implements c.g.a.m
{
  Object a;
  Object b;
  int c;
  private ag f;
  
  m$c(m paramm, p paramp, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/truepay/app/ui/payments/c/m$c;
    m localm = d;
    p localp = e;
    localc.<init>(localm, localp, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    c localc = this;
    Object localObject1 = paramObject;
    Object localObject2 = c.d.a.a.a;
    int i = c;
    boolean bool1;
    Object localObject3;
    Object localObject4;
    switch (i)
    {
    default: 
      localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)localObject1);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label628;
      }
      localObject1 = f;
      localObject3 = m.a(d);
      if (localObject3 != null) {
        ((l.b)localObject3).c();
      }
      localObject3 = new com/truecaller/truepay/app/ui/transaction/b/j;
      localObject4 = e.g();
      c.g.b.k.a(localObject4, "txnModel.account");
      Object localObject5 = ((com.truecaller.truepay.data.api.model.a)localObject4).j();
      String str1 = e.c();
      String str2 = e.d();
      String str3 = e.j();
      String str4 = e.k();
      String str5 = e.m();
      String str6 = e.l();
      String str7 = e.r();
      String str8 = e.n();
      String str9 = e.t();
      String str10 = e.s();
      String str11 = e.C();
      localObject4 = localObject3;
      ((j)localObject3).<init>((String)localObject5, str1, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11);
      localObject4 = d;
      a = localObject1;
      b = localObject3;
      int j = 1;
      c = j;
      localObject1 = c;
      localObject5 = new com/truecaller/truepay/app/ui/payments/c/m$d;
      str1 = null;
      ((m.d)localObject5).<init>((m)localObject4, (j)localObject3, null);
      localObject5 = (c.g.a.m)localObject5;
      localObject1 = g.a((f)localObject1, (c.g.a.m)localObject5, localc);
      if (localObject1 == localObject2) {
        return localObject2;
      }
      break;
    }
    localObject1 = (h)localObject1;
    boolean bool2 = false;
    localObject2 = null;
    if (localObject1 != null)
    {
      localObject3 = ((h)localObject1).a();
      localObject4 = "success";
      bool1 = c.g.b.k.a(localObject3, localObject4);
      if (bool1)
      {
        localObject2 = d;
        localObject3 = e;
        m.a((m)localObject2, (h)localObject1, (p)localObject3);
      }
      else
      {
        localObject3 = m.a(d);
        if (localObject3 != null)
        {
          localObject4 = ((h)localObject1).b();
          if (localObject4 == null)
          {
            localObject4 = m.c(d);
            int k = R.string.server_error_message;
            localObject2 = new Object[0];
            localObject4 = ((n)localObject4).a(k, (Object[])localObject2);
            localObject2 = "resourceProvider.getStri…ing.server_error_message)";
            c.g.b.k.a(localObject4, (String)localObject2);
          }
          ((l.b)localObject3).c((String)localObject4);
        }
      }
      localObject2 = d;
      localObject1 = ((h)localObject1).a();
      c.g.b.k.a(localObject1, "it.status");
      localObject3 = e;
      m.a((m)localObject2, (String)localObject1, (p)localObject3);
    }
    else
    {
      localObject1 = d;
      localObject3 = "failure";
      localObject4 = e;
      m.a((m)localObject1, (String)localObject3, (p)localObject4);
      localObject1 = m.a(d);
      if (localObject1 != null)
      {
        localObject3 = m.c(d);
        int m = R.string.server_error_message;
        localObject2 = new Object[0];
        localObject2 = ((n)localObject3).a(m, (Object[])localObject2);
        localObject3 = "resourceProvider.getStri…ing.server_error_message)";
        c.g.b.k.a(localObject2, (String)localObject3);
        ((l.b)localObject1).c((String)localObject2);
      }
    }
    localObject1 = m.a(d);
    if (localObject1 != null) {
      ((l.b)localObject1).d();
    }
    return x.a;
    label628:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.m.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */