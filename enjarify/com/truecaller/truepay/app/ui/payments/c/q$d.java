package com.truecaller.truepay.app.ui.payments.c;

import c.g.b.k;
import c.g.b.v.c;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.npci.e;
import com.truecaller.truepay.app.ui.payments.views.c.d;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.data.api.model.h;
import com.truecaller.utils.n;

public final class q$d
  implements io.reactivex.q
{
  q$d(q paramq, v.c paramc, p paramp) {}
  
  private void a(h paramh)
  {
    k.b(paramh, "responseDO");
    Object localObject1 = b;
    Object localObject2 = (d)a.af_();
    if (localObject2 == null) {
      return;
    }
    a = localObject2;
    ((d)b.a).c(false);
    localObject1 = "success";
    localObject2 = paramh.a();
    boolean bool1 = k.a(localObject1, localObject2);
    if (bool1)
    {
      localObject1 = a;
      localObject2 = c;
      k.b(paramh, "responseDO");
      k.b(localObject2, "txnModel");
      com.truecaller.truepay.app.ui.npci.b.a locala = (com.truecaller.truepay.app.ui.npci.b.a)paramh.c();
      if (locala != null)
      {
        localObject3 = locala.b();
        ((p)localObject2).f((String)localObject3);
        localObject3 = i;
        localObject4 = new com/truecaller/truepay/app/ui/payments/c/q$e;
        ((q.e)localObject4).<init>((q)localObject1, (p)localObject2);
        localObject4 = (com.truecaller.truepay.app.ui.npci.b)localObject4;
        ((e)localObject3).a(locala, (com.truecaller.truepay.app.ui.npci.b)localObject4);
      }
    }
    else
    {
      localObject1 = (d)b.a;
      localObject2 = paramh.b();
      ((d)localObject1).e((String)localObject2);
    }
    localObject1 = a;
    p localp = c;
    localObject2 = Truepay.getInstance();
    k.a(localObject2, "Truepay.getInstance()");
    localObject2 = ((Truepay)localObject2).getAnalyticLoggerHelper();
    Object localObject3 = paramh.a();
    Object localObject4 = h;
    boolean bool2 = c;
    boolean bool3 = localp.A();
    ((com.truecaller.truepay.data.b.a)localObject2).a("app_payment_transaction_initiated", (String)localObject3, (String)localObject4, "pay", "Truecaller Utility", localp, bool2, bool3);
  }
  
  public final void a(io.reactivex.a.b paramb)
  {
    k.b(paramb, "d");
    a.e.a(paramb);
  }
  
  public final void a(Throwable paramThrowable)
  {
    k.b(paramThrowable, "e");
    paramThrowable = b;
    Object localObject = (d)a.af_();
    if (localObject == null) {
      return;
    }
    a = localObject;
    ((d)b.a).c(false);
    paramThrowable = (d)b.a;
    n localn = a.j;
    int i = R.string.server_error_message;
    localObject = new Object[0];
    localObject = localn.a(i, (Object[])localObject);
    paramThrowable.e((String)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.q.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */