package com.truecaller.truepay.app.ui.payments.c;

import c.g.b.k;
import c.g.b.v.c;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.payments.views.c.d;
import com.truecaller.truepay.app.ui.transaction.b.l;
import com.truecaller.truepay.app.ui.transaction.b.m;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.data.api.model.h;
import com.truecaller.utils.n;
import io.reactivex.a.b;

public final class q$a
  implements io.reactivex.q
{
  q$a(q paramq, v.c paramc, p paramp) {}
  
  private void a(h paramh)
  {
    a locala = this;
    Object localObject1 = paramh;
    k.b(paramh, "responseDO");
    Object localObject2 = b;
    Object localObject3 = (d)a.af_();
    if (localObject3 == null) {
      return;
    }
    a = localObject3;
    localObject2 = (d)b.a;
    localObject3 = null;
    ((d)localObject2).c(false);
    localObject2 = "success";
    Object localObject4 = paramh.a();
    boolean bool = k.a(localObject2, localObject4);
    if (bool)
    {
      localObject2 = c;
      localObject1 = paramh.c();
      localObject3 = "responseDO.data";
      k.a(localObject1, (String)localObject3);
      localObject1 = ((m)localObject1).a();
      ((p)localObject2).a((l)localObject1);
      localObject2 = a;
      bool = c;
      if (!bool)
      {
        localObject2 = (d)b.a;
        localObject1 = c;
        ((d)localObject2).b((p)localObject1);
      }
      else
      {
        localObject2 = c;
        localObject1 = a.f;
        ((p)localObject2).r((String)localObject1);
        localObject2 = (d)b.a;
        localObject1 = c;
        ((d)localObject2).c((p)localObject1);
      }
      localObject2 = a;
      localObject1 = c;
      String str1 = "Truecaller Utility";
      localObject3 = "failure";
      localObject4 = "";
      Object localObject5 = "";
      Object localObject6 = ((p)localObject1).a();
      String str2;
      Object localObject7;
      Object localObject8;
      if (localObject6 != null)
      {
        localObject6 = ((p)localObject1).a();
        str2 = "transactionModel.payResponseDO";
        k.a(localObject6, str2);
        localObject6 = ((l)localObject6).d();
        if (localObject6 != null)
        {
          localObject3 = ((p)localObject1).a();
          k.a(localObject3, "transactionModel.payResponseDO");
          localObject3 = ((l)localObject3).d();
          localObject6 = "transactionModel.payResponseDO.status";
          k.a(localObject3, (String)localObject6);
        }
        localObject6 = ((p)localObject1).a();
        str2 = "transactionModel.payResponseDO";
        k.a(localObject6, str2);
        localObject6 = ((l)localObject6).b();
        if (localObject6 != null)
        {
          localObject4 = ((p)localObject1).a();
          k.a(localObject4, "transactionModel.payResponseDO");
          localObject4 = ((l)localObject4).b();
          localObject6 = "transactionModel.payResponseDO.bankRRN";
          k.a(localObject4, (String)localObject6);
        }
        localObject6 = ((p)localObject1).a();
        str2 = "transactionModel.payResponseDO";
        k.a(localObject6, str2);
        localObject6 = ((l)localObject6).h();
        if (localObject6 != null)
        {
          localObject5 = ((p)localObject1).a();
          k.a(localObject5, "transactionModel.payResponseDO");
          localObject5 = ((l)localObject5).h();
          localObject6 = "transactionModel.payResponseDO.responseCode";
          k.a(localObject5, (String)localObject6);
          localObject7 = localObject4;
          localObject8 = localObject5;
          localObject5 = localObject3;
        }
        else
        {
          localObject7 = localObject4;
          localObject8 = localObject5;
          localObject5 = localObject3;
        }
      }
      else
      {
        localObject7 = localObject4;
        localObject8 = localObject5;
        localObject5 = localObject3;
      }
      localObject3 = Truepay.getInstance();
      localObject4 = "Truepay.getInstance()";
      k.a(localObject3, (String)localObject4);
      localObject3 = ((Truepay)localObject3).getAnalyticLoggerHelper();
      if (localObject3 != null)
      {
        localObject6 = h;
        str2 = ((p)localObject1).d();
        String str3 = ((p)localObject1).c();
        String str4 = ((p)localObject1).f();
        Object localObject9 = ((p)localObject1).g();
        k.a(localObject9, "transactionModel.account");
        localObject9 = ((com.truecaller.truepay.data.api.model.a)localObject9).d();
        Object localObject10 = ((p)localObject1).g();
        k.a(localObject10, "transactionModel.account");
        localObject10 = ((com.truecaller.truepay.data.api.model.a)localObject10).l();
        k.a(localObject10, "transactionModel.account.bank");
        localObject10 = ((com.truecaller.truepay.data.d.a)localObject10).b();
        String str5 = ((p)localObject1).n();
        Boolean localBoolean = Boolean.valueOf(c);
        String str6 = ((p)localObject1).B();
        ((com.truecaller.truepay.data.b.a)localObject3).a("app_payment_transaction_status", (String)localObject5, (String)localObject6, str2, str3, str1, str4, (String)localObject9, (String)localObject10, (String)localObject7, (String)localObject8, str5, localBoolean, str6);
        return;
      }
      return;
    }
    localObject2 = (d)b.a;
    localObject1 = paramh.b();
    if (localObject1 == null)
    {
      localObject1 = a.j;
      int i = R.string.server_error_message;
      localObject3 = new Object[0];
      localObject1 = ((n)localObject1).a(i, (Object[])localObject3);
    }
    ((d)localObject2).e((String)localObject1);
  }
  
  public final void a(b paramb)
  {
    k.b(paramb, "d");
    a.e.a(paramb);
  }
  
  public final void a(Throwable paramThrowable)
  {
    k.b(paramThrowable, "e");
    paramThrowable = b;
    Object localObject = (d)a.af_();
    if (localObject == null) {
      return;
    }
    a = localObject;
    ((d)b.a).c(false);
    paramThrowable = (d)b.a;
    n localn = a.j;
    int i = R.string.server_error_message;
    localObject = new Object[0];
    localObject = localn.a(i, (Object[])localObject);
    paramThrowable.e((String)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.q.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */