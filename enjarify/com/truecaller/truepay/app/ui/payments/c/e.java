package com.truecaller.truepay.app.ui.payments.c;

import c.g.b.k;
import com.truecaller.bb;
import com.truecaller.truepay.app.ui.payments.models.a;
import com.truecaller.truepay.app.utils.x;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class e
  extends bb
  implements d.a
{
  private List a;
  private a c;
  private final x d;
  
  public e(x paramx)
  {
    d = paramx;
    paramx = new java/util/ArrayList;
    paramx.<init>();
    paramx = (List)paramx;
    a = paramx;
  }
  
  public final void a()
  {
    d.b localb = (d.b)b;
    if (localb != null)
    {
      localb.c();
      return;
    }
  }
  
  public final void a(a parama)
  {
    k.b(parama, "utility");
    d.b localb = (d.b)b;
    if (localb != null)
    {
      localb.a(parama);
      localb.c();
      return;
    }
  }
  
  public final void a(String paramString)
  {
    Object localObject1 = paramString;
    localObject1 = (CharSequence)paramString;
    d.b localb = null;
    if (localObject1 != null)
    {
      i = ((CharSequence)localObject1).length();
      if (i != 0)
      {
        i = 0;
        localObject1 = null;
        break label40;
      }
    }
    int i = 1;
    label40:
    Object localObject2;
    if (i != 0)
    {
      paramString = (d.b)b;
      if (paramString != null)
      {
        localObject1 = new java/util/ArrayList;
        localObject2 = (Collection)a;
        ((ArrayList)localObject1).<init>((Collection)localObject2);
        localObject1 = (List)localObject1;
        paramString.b((List)localObject1);
        paramString.a(false);
      }
      return;
    }
    localObject1 = a;
    paramString = x.a((List)localObject1, paramString);
    boolean bool = paramString.isEmpty();
    localb = (d.b)b;
    if (localb != null)
    {
      localObject2 = new java/util/ArrayList;
      paramString = (Collection)paramString;
      ((ArrayList)localObject2).<init>(paramString);
      localObject2 = (List)localObject2;
      localb.b((List)localObject2);
      localb.a(bool);
      return;
    }
  }
  
  public final void b()
  {
    d.b localb = (d.b)b;
    if (localb == null) {
      return;
    }
    Object localObject1 = localb.b();
    c = ((a)localObject1);
    localObject1 = c;
    if (localObject1 != null)
    {
      a.clear();
      List localList = a;
      Object localObject2 = ((a)localObject1).c();
      k.a(localObject2, "it.utilityFields");
      localObject2 = (Collection)localObject2;
      localList.addAll((Collection)localObject2);
      localObject1 = ((a)localObject1).d();
      k.a(localObject1, "it.title");
      localb.a((String)localObject1);
      localObject1 = a;
      localb.a((List)localObject1);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */