package com.truecaller.truepay.app.ui.payments.c;

import com.truecaller.truepay.app.ui.history.models.h;
import com.truecaller.truepay.app.ui.payments.models.a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract interface i$b
{
  public abstract void a(a parama1, h paramh, HashMap paramHashMap, a parama2);
  
  public abstract void a(ArrayList paramArrayList);
  
  public abstract void a(List paramList);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract List b();
  
  public abstract a c();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.i.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */