package com.truecaller.truepay.app.ui.payments.c;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.o.b;
import c.x;
import com.google.gson.i;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.transaction.b.d;
import com.truecaller.truepay.app.ui.transaction.b.l;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.data.api.model.h;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class m$a
  extends c.d.b.a.k
  implements c.g.a.m
{
  Object a;
  Object b;
  int c;
  private ag g;
  
  m$a(m paramm, i parami, p paramp, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/truepay/app/ui/payments/c/m$a;
    m localm = d;
    i locali = e;
    p localp = f;
    locala.<init>(localm, locali, localp, paramc);
    paramObject = (ag)paramObject;
    g = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = c;
    int j = 0;
    Object localObject2 = null;
    boolean bool1;
    Object localObject3;
    Object localObject4;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label504;
      }
      paramObject = g;
      localObject3 = m.a(d);
      if (localObject3 != null) {
        ((l.b)localObject3).c();
      }
      localObject3 = new com/truecaller/truepay/app/ui/transaction/b/d;
      localObject4 = e;
      Object localObject5 = f.f();
      String str = f.j();
      ((d)localObject3).<init>((i)localObject4, (String)localObject5, str);
      localObject4 = d;
      a = paramObject;
      b = localObject3;
      int k = 1;
      c = k;
      paramObject = c;
      localObject5 = new com/truecaller/truepay/app/ui/payments/c/m$b;
      ((m.b)localObject5).<init>((m)localObject4, (d)localObject3, null);
      localObject5 = (c.g.a.m)localObject5;
      paramObject = g.a((f)paramObject, (c.g.a.m)localObject5, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (h)paramObject;
    boolean bool2 = false;
    localObject1 = null;
    if (paramObject != null)
    {
      localObject3 = ((h)paramObject).a();
      localObject4 = "success";
      bool1 = c.g.b.k.a(localObject3, localObject4);
      if (bool1)
      {
        localObject3 = f;
        paramObject = ((h)paramObject).c();
        localObject4 = "responseDO.data";
        c.g.b.k.a(paramObject, (String)localObject4);
        paramObject = ((com.truecaller.truepay.app.ui.transaction.b.m)paramObject).a();
        ((p)localObject3).a((l)paramObject);
        paramObject = m.a(d);
        if (paramObject != null)
        {
          localObject3 = f;
          ((l.b)paramObject).a((p)localObject3);
          localObject2 = x.a;
        }
      }
      else
      {
        localObject3 = m.a(d);
        if (localObject3 != null)
        {
          paramObject = ((h)paramObject).b();
          if (paramObject == null)
          {
            paramObject = m.c(d);
            j = R.string.server_error_message;
            localObject4 = new Object[0];
            paramObject = ((n)paramObject).a(j, (Object[])localObject4);
            localObject2 = "resourceProvider.getStri…ing.server_error_message)";
            c.g.b.k.a(paramObject, (String)localObject2);
          }
          ((l.b)localObject3).c((String)paramObject);
          localObject2 = x.a;
        }
      }
      if (localObject2 != null) {}
    }
    else
    {
      paramObject = m.a(d);
      if (paramObject != null)
      {
        localObject3 = m.c(d);
        j = R.string.server_error_message;
        localObject1 = new Object[0];
        localObject1 = ((n)localObject3).a(j, (Object[])localObject1);
        localObject3 = "resourceProvider.getStri…ing.server_error_message)";
        c.g.b.k.a(localObject1, (String)localObject3);
        ((l.b)paramObject).c((String)localObject1);
        paramObject = x.a;
      }
    }
    paramObject = d;
    localObject1 = f;
    m.a((m)paramObject, (p)localObject1);
    paramObject = m.a(d);
    if (paramObject != null) {
      ((l.b)paramObject).d();
    }
    return x.a;
    label504:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.m.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */