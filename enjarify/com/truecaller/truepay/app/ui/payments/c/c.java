package com.truecaller.truepay.app.ui.payments.c;

import c.a.m;
import c.g.b.k;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.utils.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class c
  extends com.truecaller.truepay.app.ui.base.a.a
  implements b
{
  public p a;
  public boolean b;
  public final r c;
  public final n f;
  public final com.truecaller.truepay.app.utils.a g;
  public final au h;
  
  public c(r paramr, n paramn, com.truecaller.truepay.app.utils.a parama, au paramau)
  {
    c = paramr;
    f = paramn;
    g = parama;
    h = paramau;
  }
  
  public final void a()
  {
    boolean bool = b;
    com.truecaller.truepay.app.ui.payments.views.c.b localb;
    if (!bool)
    {
      localb = (com.truecaller.truepay.app.ui.payments.views.c.b)af_();
      if (localb != null)
      {
        localb.b();
        localb.e();
      }
    }
    else
    {
      localb = (com.truecaller.truepay.app.ui.payments.views.c.b)af_();
      if (localb != null)
      {
        localb.c();
        localb.d();
      }
    }
    bool = b ^ true;
    b = bool;
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    k.b(parama, "account");
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "txnModel";
      k.a((String)localObject2);
    }
    ((p)localObject1).a(parama);
    Object localObject2 = parama.j();
    ((p)localObject1).b((String)localObject2);
    localObject1 = (com.truecaller.truepay.app.ui.payments.views.c.b)af_();
    if (localObject1 != null)
    {
      localObject2 = c;
      localObject3 = au.a(parama.d());
      localObject4 = "stringUtils.getFormatted…er(account.accountNumber)";
      k.a(localObject3, (String)localObject4);
      ((com.truecaller.truepay.app.ui.payments.views.c.b)localObject1).a(parama, (r)localObject2, (String)localObject3);
    }
    localObject1 = g;
    parama = ((com.truecaller.truepay.app.utils.a)localObject1).b(parama);
    int i = parama.size();
    if (i == 0)
    {
      parama = (com.truecaller.truepay.app.ui.payments.views.c.b)af_();
      if (parama != null)
      {
        parama.f();
        return;
      }
      return;
    }
    localObject1 = new com/truecaller/truepay/data/api/model/a;
    ((com.truecaller.truepay.data.api.model.a)localObject1).<init>();
    localObject2 = "pay_via_other";
    ((com.truecaller.truepay.data.api.model.a)localObject1).f((String)localObject2);
    boolean bool1 = true;
    ((com.truecaller.truepay.data.api.model.a)localObject1).e(bool1);
    Object localObject3 = new com/truecaller/truepay/data/d/a;
    ((com.truecaller.truepay.data.d.a)localObject3).<init>();
    Object localObject4 = f;
    int j = R.string.pay_via_other;
    Object[] arrayOfObject = new Object[0];
    localObject4 = ((n)localObject4).a(j, arrayOfObject);
    ((com.truecaller.truepay.data.d.a)localObject3).a((String)localObject4);
    ((com.truecaller.truepay.data.d.a)localObject3).b("upi");
    localObject4 = "";
    ((com.truecaller.truepay.data.api.model.a)localObject1).c((String)localObject4);
    ((com.truecaller.truepay.data.api.model.a)localObject1).a((com.truecaller.truepay.data.d.a)localObject3);
    parama.add(localObject1);
    localObject1 = (com.truecaller.truepay.app.ui.payments.views.c.b)af_();
    if (localObject1 != null)
    {
      k.a(parama, "accounts");
      localObject3 = parama;
      localObject3 = (List)parama;
      ((com.truecaller.truepay.app.ui.payments.views.c.b)localObject1).b((List)localObject3);
    }
    k.a(parama, "accounts");
    parama = (Iterable)parama;
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    parama = parama.iterator();
    for (;;)
    {
      boolean bool2 = parama.hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = parama.next();
      localObject4 = localObject3;
      localObject4 = (com.truecaller.truepay.data.api.model.a)localObject3;
      k.a(localObject4, "it");
      localObject4 = ((com.truecaller.truepay.data.api.model.a)localObject4).j();
      String str = "pay_via_other";
      boolean bool3 = k.a(localObject4, str) ^ bool1;
      if (bool3) {
        ((Collection)localObject1).add(localObject3);
      }
    }
    localObject1 = (Iterable)localObject1;
    int k = 2;
    parama = (Collection)m.d((Iterable)localObject1, k);
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>(parama);
    parama = (com.truecaller.truepay.app.ui.payments.views.c.b)af_();
    if (parama != null)
    {
      localObject1 = (List)localObject1;
      parama.a((List)localObject1);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */