package com.truecaller.truepay.app.ui.payments.c;

import c.d.a.a;
import c.d.c;
import c.o.b;
import c.x;
import com.truecaller.common.h.q;
import com.truecaller.truepay.app.ui.transaction.b.d;
import com.truecaller.truepay.data.api.g;
import com.truecaller.truepay.data.api.model.h;
import e.b;
import e.r;
import kotlinx.coroutines.ag;

final class m$b
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag d;
  
  m$b(m paramm, d paramd, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/truepay/app/ui/payments/c/m$b;
    m localm = b;
    d locald = c;
    localb.<init>(localm, locald, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = m.g(b);
        localObject = c;
        paramObject = ((g)paramObject).b((d)localObject);
        localObject = "truepayApiService.confirmPayment(requestDO)";
        c.g.b.k.a(paramObject, (String)localObject);
        paramObject = q.a((b)paramObject);
        if (paramObject != null) {
          return (h)((r)paramObject).e();
        }
        return null;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.m.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */