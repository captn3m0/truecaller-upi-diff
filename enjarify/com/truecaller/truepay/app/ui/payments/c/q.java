package com.truecaller.truepay.app.ui.payments.c;

import android.os.Bundle;
import c.d.f;
import c.g.b.k;
import c.g.b.v.c;
import c.u;
import com.truecaller.log.UnmutedException.f;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.a.a.d.g;
import com.truecaller.truepay.app.ui.history.models.h;
import com.truecaller.truepay.app.ui.transaction.b.j;
import com.truecaller.truepay.app.utils.ae;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.ax;
import com.truecaller.truepay.data.api.model.x;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;

public final class q
  extends p
{
  private boolean A;
  private boolean B;
  private boolean C;
  private boolean D;
  private bn E;
  private final DecimalFormat F;
  private final com.truecaller.truepay.a.a.d.i G;
  private final g H;
  private final com.truecaller.truepay.a.a.f.n I;
  private final com.truecaller.truepay.a.a.f.d J;
  private final com.truecaller.truepay.a.a.d.a K;
  private final com.truecaller.truepay.data.e.c L;
  private final com.truecaller.truepay.app.utils.a M;
  private final au N;
  private final f O;
  com.truecaller.truepay.app.ui.payments.models.a a;
  com.truecaller.truepay.app.ui.payments.models.a b;
  boolean c;
  String f;
  boolean g;
  String h;
  final com.truecaller.truepay.app.ui.npci.e i;
  final com.truecaller.utils.n j;
  final f k;
  final com.truecaller.truepay.app.utils.a.a l;
  private com.truecaller.truepay.app.ui.payments.models.a m;
  private com.truecaller.truepay.app.ui.payments.models.a n;
  private com.truecaller.truepay.app.ui.payments.models.a o;
  private String p;
  private String q;
  private ArrayList r;
  private final HashMap s;
  private final HashMap t;
  private final HashMap u;
  private boolean v;
  private boolean w;
  private boolean x;
  private com.truecaller.truepay.app.ui.payments.models.c y;
  private com.truecaller.truepay.data.api.model.a z;
  
  public q(com.truecaller.truepay.a.a.d.i parami, g paramg, com.truecaller.truepay.a.a.f.n paramn, com.truecaller.truepay.a.a.f.d paramd, com.truecaller.truepay.a.a.d.a parama, com.truecaller.truepay.app.ui.npci.e parame, com.truecaller.utils.n paramn1, com.truecaller.truepay.data.e.c paramc, com.truecaller.truepay.app.utils.a parama1, au paramau, f paramf1, f paramf2, com.truecaller.truepay.app.utils.a.a parama2)
  {
    G = parami;
    H = paramg;
    I = paramn;
    J = paramd;
    K = parama;
    i = parame;
    j = paramn1;
    L = paramc;
    M = parama1;
    N = paramau;
    O = paramf1;
    k = paramf2;
    l = parama2;
    parami = new java/util/ArrayList;
    parami.<init>();
    r = parami;
    parami = new java/util/HashMap;
    parami.<init>();
    s = parami;
    parami = new java/util/HashMap;
    parami.<init>();
    t = parami;
    parami = new java/util/HashMap;
    parami.<init>();
    u = parami;
    B = true;
    h = "utilities";
    parami = new java/text/DecimalFormat;
    parami.<init>("#,###.##");
    F = parami;
  }
  
  private final void a(com.truecaller.truepay.app.ui.transaction.b.p paramp, HashMap paramHashMap)
  {
    v.c localc = new c/g/b/v$c;
    localc.<init>();
    Object localObject = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
    if (localObject == null) {
      return;
    }
    a = localObject;
    ((com.truecaller.truepay.app.ui.payments.views.c.d)a).c(true);
    paramHashMap = K.a(paramHashMap);
    localObject = io.reactivex.g.a.b();
    paramHashMap = paramHashMap.b((io.reactivex.n)localObject);
    localObject = io.reactivex.android.b.a.a();
    paramHashMap = paramHashMap.a((io.reactivex.n)localObject);
    localObject = new com/truecaller/truepay/app/ui/payments/c/q$b;
    ((q.b)localObject).<init>(this, localc, paramp);
    localObject = (io.reactivex.q)localObject;
    paramHashMap.a((io.reactivex.q)localObject);
  }
  
  private static void a(HashMap paramHashMap, com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    if (paramHashMap != null)
    {
      str = parama.e();
      boolean bool1 = paramHashMap.containsKey(str);
      if (bool1)
      {
        str = parama.e();
        paramHashMap = (String)paramHashMap.get(str);
        parama.d(paramHashMap);
        return;
      }
    }
    paramHashMap = parama.f();
    String str = "non_ui";
    boolean bool2 = k.a(paramHashMap, str) ^ true;
    if (bool2)
    {
      bool2 = false;
      paramHashMap = null;
      parama.d(null);
    }
  }
  
  private final void b(com.truecaller.truepay.app.ui.transaction.b.p paramp, HashMap paramHashMap)
  {
    v.c localc = new c/g/b/v$c;
    localc.<init>();
    Object localObject = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
    if (localObject == null) {
      return;
    }
    a = localObject;
    ((com.truecaller.truepay.app.ui.payments.views.c.d)a).c(true);
    paramHashMap = H.a(paramHashMap);
    localObject = io.reactivex.g.a.b();
    paramHashMap = paramHashMap.b((io.reactivex.n)localObject);
    localObject = io.reactivex.android.b.a.a();
    paramHashMap = paramHashMap.a((io.reactivex.n)localObject);
    localObject = new com/truecaller/truepay/app/ui/payments/c/q$h;
    ((q.h)localObject).<init>(this, localc, paramp);
    localObject = (io.reactivex.q)localObject;
    paramHashMap.a((io.reactivex.q)localObject);
  }
  
  private static boolean d(String paramString)
  {
    String str = "postpaid";
    boolean bool1 = c.n.m.a(str, paramString, false);
    if (!bool1)
    {
      str = "prepaid";
      bool1 = c.n.m.a(str, paramString, false);
      if (!bool1)
      {
        str = "datacard";
        boolean bool2 = c.n.m.a(str, paramString, false);
        if (!bool2) {
          return false;
        }
      }
    }
    return true;
  }
  
  private final void n()
  {
    r.clear();
    c = false;
    f = null;
    v = false;
    a = null;
    b = null;
    x = false;
    g = false;
    y = null;
    p = null;
    q = null;
    D = false;
    C = false;
    r.clear();
    s.clear();
    t.clear();
    u.clear();
  }
  
  public final void a()
  {
    com.truecaller.truepay.app.ui.payments.views.c.d locald = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
    if (locald != null)
    {
      Object localObject = m;
      if (localObject == null)
      {
        String str = "utilityEntry";
        k.a(str);
      }
      localObject = ((com.truecaller.truepay.app.ui.payments.models.a)localObject).r();
      locald.a((String)localObject);
      return;
    }
  }
  
  public final void a(int paramInt)
  {
    int i1 = 1;
    B = i1;
    int i2 = 0;
    while (i2 < paramInt)
    {
      Object localObject1 = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
      if (localObject1 != null)
      {
        localObject1 = ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject1).b(i2);
        if (localObject1 != null)
        {
          Object localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
          boolean bool1 = ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject2).c(i2);
          Object localObject3;
          if (bool1)
          {
            localObject2 = ((com.truecaller.truepay.app.ui.payments.views.c.d)af_()).d(i2);
            k.a(localObject2, "mvpView.getInputStringFromView(position)");
            localObject3 = "phone";
            Object localObject4 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).f();
            boolean bool2 = k.a(localObject3, localObject4);
            int i7;
            if (bool2)
            {
              int i3 = ((String)localObject2).length();
              i7 = 10;
              if (i3 > i7)
              {
                i3 = ((String)localObject2).length() - i7;
                if (localObject2 != null)
                {
                  localObject2 = ((String)localObject2).substring(i3);
                  localObject3 = "(this as java.lang.String).substring(startIndex)";
                  k.a(localObject2, (String)localObject3);
                }
                else
                {
                  localObject5 = new c/u;
                  ((u)localObject5).<init>("null cannot be cast to non-null type java.lang.String");
                  throw ((Throwable)localObject5);
                }
              }
            }
            localObject3 = "amount";
            localObject4 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).f();
            boolean bool3 = k.a(localObject3, localObject4);
            if (bool3)
            {
              int i4 = ((String)localObject2).length();
              if (i4 > i1)
              {
                localObject3 = localObject2;
                localObject3 = (CharSequence)localObject2;
                localObject4 = ".";
                int i8 = 6;
                i7 = c.n.m.a((CharSequence)localObject3, (String)localObject4, 0, false, i8);
                int i9 = -1;
                int i5;
                if (i7 == i9)
                {
                  i5 = ((String)localObject2).length();
                }
                else
                {
                  localObject4 = ".";
                  i5 = c.n.m.a((CharSequence)localObject3, (String)localObject4, 0, false, i8);
                }
                if (localObject2 != null)
                {
                  localObject2 = ((String)localObject2).substring(i1, i5);
                  localObject3 = "(this as java.lang.Strin…ing(startIndex, endIndex)";
                  k.a(localObject2, (String)localObject3);
                }
                else
                {
                  localObject5 = new c/u;
                  ((u)localObject5).<init>("null cannot be cast to non-null type java.lang.String");
                  throw ((Throwable)localObject5);
                }
              }
              bool4 = v;
              if (bool4) {
                break label1188;
              }
            }
            localObject3 = "operator_location";
            localObject4 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).f();
            boolean bool4 = k.a(localObject3, localObject4);
            Object localObject6;
            if (bool4)
            {
              localObject2 = a;
              if (localObject2 == null)
              {
                localObject2 = b;
                if (localObject2 == null)
                {
                  localObject1 = this;
                  localObject1 = (q)this;
                  localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.d)((q)localObject1).af_();
                  localObject3 = j;
                  i7 = R.string.select_operator_location_message;
                  localObject6 = new Object[0];
                  localObject3 = ((com.truecaller.utils.n)localObject3).a(i7, (Object[])localObject6);
                  ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject2).a(i2, (String)localObject3);
                  B = false;
                  break label1120;
                }
              }
              localObject2 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).c().iterator();
              for (;;)
              {
                bool4 = ((Iterator)localObject2).hasNext();
                if (!bool4) {
                  break label719;
                }
                localObject3 = (com.truecaller.truepay.app.ui.payments.models.a)((Iterator)localObject2).next();
                localObject4 = "operator";
                k.a(localObject3, "item");
                localObject6 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject3).f();
                boolean bool5 = c.n.m.a((String)localObject4, (String)localObject6, i1);
                if (bool5)
                {
                  localObject4 = (Map)t;
                  localObject3 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject3).e();
                  k.a(localObject3, "item.key");
                  localObject6 = a;
                  if (localObject6 != null)
                  {
                    localObject6 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject6).b();
                    if (localObject6 != null)
                    {
                      ((Map)localObject4).put(localObject3, localObject6);
                      continue;
                    }
                  }
                }
                else
                {
                  localObject4 = "location";
                  localObject6 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject3).f();
                  bool5 = c.n.m.a((String)localObject4, (String)localObject6, i1);
                  if (bool5)
                  {
                    localObject4 = b;
                    if (localObject4 != null)
                    {
                      localObject4 = (Map)t;
                      localObject3 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject3).e();
                      k.a(localObject3, "item.key");
                      localObject6 = b;
                      if (localObject6 == null) {
                        break;
                      }
                      localObject6 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject6).b();
                      if (localObject6 == null) {
                        break;
                      }
                      ((Map)localObject4).put(localObject3, localObject6);
                    }
                  }
                }
              }
              return;
              label719:
              localObject2 = (Map)s;
              localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).e();
              k.a(localObject1, "field.key");
              localObject3 = t;
              ((Map)localObject2).put(localObject1, localObject3);
            }
            else
            {
              localObject3 = "operator";
              localObject4 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).f();
              bool4 = k.a(localObject3, localObject4);
              if (bool4)
              {
                localObject2 = a;
                if (localObject2 == null)
                {
                  localObject1 = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
                  localObject2 = j;
                  int i6 = R.string.select_operator_message;
                  localObject4 = new Object[0];
                  localObject2 = ((com.truecaller.utils.n)localObject2).a(i6, (Object[])localObject4);
                  ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject1).a(i2, (String)localObject2);
                  B = false;
                }
                else
                {
                  localObject2 = (Map)s;
                  localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).e();
                  k.a(localObject1, "field.key");
                  localObject3 = a;
                  if (localObject3 != null)
                  {
                    localObject3 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject3).b();
                    if (localObject3 != null)
                    {
                      ((Map)localObject2).put(localObject1, localObject3);
                      break label1120;
                    }
                  }
                }
              }
              else
              {
                localObject3 = (Map)s;
                localObject4 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).e();
                localObject6 = "field.key";
                k.a(localObject4, (String)localObject6);
                ((Map)localObject3).put(localObject4, localObject2);
                localObject3 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).g();
                bool1 = ax.b((String)localObject3, (String)localObject2);
                if (!bool1)
                {
                  localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
                  localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).i();
                  ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject2).a(i2, (String)localObject1);
                  B = false;
                }
                else
                {
                  localObject1 = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
                  ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject1).e(i2);
                }
              }
            }
          }
          else
          {
            localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
            bool1 = ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject2).f(i2);
            if (bool1)
            {
              localObject2 = (Map)s;
              localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).e();
              k.a(localObject1, "field.key");
              localObject3 = ((com.truecaller.truepay.app.ui.payments.views.c.d)af_()).g(i2);
              if (localObject3 == null) {
                return;
              }
              ((Map)localObject2).put(localObject1, localObject3);
            }
          }
          label1120:
          localObject1 = y;
          if (localObject1 != null)
          {
            localObject2 = (Map)t;
            localObject3 = "plan_id";
            if (localObject1 != null)
            {
              int i10 = ((com.truecaller.truepay.app.ui.payments.models.c)localObject1).b();
              localObject1 = String.valueOf(i10);
              if (localObject1 != null)
              {
                ((Map)localObject2).put(localObject3, localObject1);
                break label1188;
              }
            }
            return;
          }
          label1188:
          i2 += 1;
          continue;
        }
      }
      return;
    }
    Object localObject5 = u;
    paramInt = ((HashMap)localObject5).size();
    if (paramInt > 0)
    {
      localObject5 = s;
      Map localMap = (Map)u;
      ((HashMap)localObject5).putAll(localMap);
    }
  }
  
  public final void a(int paramInt, h paramh)
  {
    k.b(paramh, "historyItem");
    com.truecaller.truepay.app.ui.history.models.o localo = paramh.a();
    int i1 = 1;
    D = i1;
    paramInt -= i1;
    while (paramInt >= 0)
    {
      Object localObject1 = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
      if (localObject1 != null)
      {
        localObject1 = ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject1).b(paramInt);
        if (localObject1 != null)
        {
          Object localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
          boolean bool1 = ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject2).c(paramInt);
          if (bool1)
          {
            localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).f();
            if (localObject1 != null)
            {
              int i2 = ((String)localObject1).hashCode();
              int i3 = -1413853096;
              boolean bool2;
              if (i2 != i3)
              {
                i3 = 106642798;
                if (i2 != i3)
                {
                  i3 = 1552163056;
                  if (i2 == i3)
                  {
                    localObject2 = "operator_location";
                    bool2 = ((String)localObject1).equals(localObject2);
                    if (bool2)
                    {
                      localObject1 = new com/truecaller/truepay/app/ui/payments/models/a;
                      ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).<init>();
                      k.a(localo, "utilityDO");
                      localObject2 = localo.e();
                      ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).a((String)localObject2);
                      localObject2 = localo.e();
                      ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).b((String)localObject2);
                      localObject2 = localo.b();
                      ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).c((String)localObject2);
                      localObject2 = localo.f();
                      ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).e((String)localObject2);
                      localObject2 = localo.g();
                      ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).f((String)localObject2);
                      localObject2 = new com/truecaller/truepay/app/ui/payments/models/a;
                      ((com.truecaller.truepay.app.ui.payments.models.a)localObject2).<init>();
                      Object localObject3 = localo.d();
                      ((com.truecaller.truepay.app.ui.payments.models.a)localObject2).a((String)localObject3);
                      localObject3 = localo.d();
                      ((com.truecaller.truepay.app.ui.payments.models.a)localObject2).b((String)localObject3);
                      localObject3 = localo.c();
                      ((com.truecaller.truepay.app.ui.payments.models.a)localObject2).c((String)localObject3);
                      b = ((com.truecaller.truepay.app.ui.payments.models.a)localObject2);
                      a = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1);
                      localObject3 = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
                      ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject3).b((com.truecaller.truepay.app.ui.payments.models.a)localObject1, (com.truecaller.truepay.app.ui.payments.models.a)localObject2);
                    }
                  }
                }
                else
                {
                  localObject2 = "phone";
                  bool2 = ((String)localObject1).equals(localObject2);
                  if (bool2)
                  {
                    localObject1 = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
                    k.a(localo, "utilityDO");
                    localObject2 = localo.a();
                    ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject1).c(paramInt, (String)localObject2);
                  }
                }
              }
              else
              {
                localObject2 = "amount";
                bool2 = ((String)localObject1).equals(localObject2);
                if (bool2)
                {
                  localObject1 = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
                  localObject2 = j;
                  i3 = R.string.prefixed_amount;
                  Object[] arrayOfObject = new Object[i1];
                  Object localObject4 = F;
                  String str = paramh.c();
                  double d = Double.parseDouble(str);
                  localObject4 = ((DecimalFormat)localObject4).format(d);
                  arrayOfObject[0] = localObject4;
                  localObject2 = ((com.truecaller.utils.n)localObject2).a(i3, arrayOfObject);
                  ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject1).b(paramInt, (String)localObject2);
                }
              }
            }
          }
          paramInt += -1;
          continue;
        }
      }
      return;
    }
  }
  
  public final void a(int paramInt, com.truecaller.truepay.app.ui.payments.models.c paramc)
  {
    k.b(paramc, "plan");
    y = paramc;
    int i1 = 0;
    while (i1 < paramInt)
    {
      Object localObject1 = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
      if (localObject1 != null)
      {
        localObject1 = ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject1).b(i1);
        if (localObject1 != null)
        {
          Object localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
          boolean bool1 = ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject2).c(i1);
          if (bool1)
          {
            localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).f();
            localObject2 = "amount";
            boolean bool2 = true;
            boolean bool3 = c.n.m.a((String)localObject1, (String)localObject2, bool2);
            if (bool3)
            {
              localObject1 = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
              localObject2 = j;
              int i2 = R.string.prefixed_amount;
              Object[] arrayOfObject = new Object[bool2];
              int i3 = paramc.a();
              String str = String.valueOf(i3);
              arrayOfObject[0] = str;
              localObject2 = ((com.truecaller.utils.n)localObject2).a(i2, arrayOfObject);
              ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject1).b(i1, (String)localObject2);
            }
          }
          i1 += 1;
          continue;
        }
      }
      return;
    }
  }
  
  public final void a(Bundle paramBundle)
  {
    k.b(paramBundle, "arguments");
    n();
    Object localObject1 = paramBundle.getString("utility_operator_symbol", null);
    p = ((String)localObject1);
    localObject1 = paramBundle.getString("utility_location_symbol", null);
    q = ((String)localObject1);
    boolean bool1 = paramBundle.getBoolean("do_fetch_bill", false);
    C = bool1;
    bool1 = paramBundle.getBoolean("is_from_recents", false);
    D = bool1;
    localObject1 = paramBundle.getString("recharge_context_key", "utilities");
    Object localObject2 = "arguments.getString(Paym…stants.CONTEXT_UTILITIES)";
    k.a(localObject1, (String)localObject2);
    h = ((String)localObject1);
    localObject1 = paramBundle.getSerializable("recharge_params");
    boolean bool2 = localObject1 instanceof HashMap;
    if (!bool2)
    {
      bool1 = false;
      localObject1 = null;
    }
    localObject1 = (HashMap)localObject1;
    if (localObject1 == null)
    {
      bool1 = false;
      localObject1 = null;
    }
    localObject2 = paramBundle.getSerializable("utility_entry");
    if (localObject2 != null)
    {
      localObject2 = (com.truecaller.truepay.app.ui.payments.models.a)localObject2;
      if (localObject2 == null) {
        return;
      }
      m = ((com.truecaller.truepay.app.ui.payments.models.a)localObject2);
      localObject2 = M.c();
      z = ((com.truecaller.truepay.data.api.model.a)localObject2);
      localObject2 = "utility_operator";
      paramBundle = paramBundle.getSerializable((String)localObject2);
      if (paramBundle != null) {
        if (paramBundle != null)
        {
          paramBundle = (com.truecaller.truepay.app.ui.payments.models.a)paramBundle;
          a = paramBundle;
        }
        else
        {
          paramBundle = new c/u;
          paramBundle.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.payments.models.BaseUtility");
          throw paramBundle;
        }
      }
      paramBundle = "item";
      localObject2 = m;
      Object localObject3;
      if (localObject2 == null)
      {
        localObject3 = "utilityEntry";
        k.a((String)localObject3);
      }
      localObject2 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject2).f();
      boolean bool3 = true;
      boolean bool4 = c.n.m.a(paramBundle, (String)localObject2, bool3);
      if (bool4)
      {
        paramBundle = m;
        if (paramBundle == null)
        {
          localObject2 = "utilityEntry";
          k.a((String)localObject2);
        }
        bool4 = paramBundle.h();
        w = bool4;
        paramBundle = a;
        int i1 = 2;
        if (paramBundle == null)
        {
          paramBundle = new java/util/ArrayList;
          localObject3 = m;
          if (localObject3 == null)
          {
            localObject4 = "utilityEntry";
            k.a((String)localObject4);
          }
          localObject3 = (Collection)((com.truecaller.truepay.app.ui.payments.models.a)localObject3).c();
          paramBundle.<init>((Collection)localObject3);
          r = paramBundle;
          paramBundle = r.iterator();
          for (;;)
          {
            bool3 = paramBundle.hasNext();
            if (!bool3) {
              break;
            }
            localObject3 = (com.truecaller.truepay.app.ui.payments.models.a)paramBundle.next();
            localObject4 = "field";
            k.a(localObject3, (String)localObject4);
            a((HashMap)localObject1, (com.truecaller.truepay.app.ui.payments.models.a)localObject3);
          }
          paramBundle = p;
          if (paramBundle == null)
          {
            paramBundle = q;
            if (paramBundle == null) {
              if (localObject1 != null)
              {
                int i2 = ((HashMap)localObject1).size();
                if (i2 != 0) {}
              }
              else
              {
                paramBundle = m;
                if (paramBundle == null)
                {
                  localObject1 = "utilityEntry";
                  k.a((String)localObject1);
                }
                paramBundle = ae.a(paramBundle);
                localObject1 = m;
                if (localObject1 == null)
                {
                  localObject3 = "utilityEntry";
                  k.a((String)localObject3);
                }
                localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).j();
                bool1 = d((String)localObject1);
                if ((bool1) && (paramBundle != null))
                {
                  localObject1 = (ag)bg.a;
                  localObject3 = O;
                  localObject4 = new com/truecaller/truepay/app/ui/payments/c/q$f;
                  ((q.f)localObject4).<init>(this, paramBundle, null);
                  localObject4 = (c.g.a.m)localObject4;
                  paramBundle = kotlinx.coroutines.e.b((ag)localObject1, (f)localObject3, (c.g.a.m)localObject4, i1);
                  E = paramBundle;
                }
              }
            }
          }
          return;
        }
        if (paramBundle == null) {
          return;
        }
        Object localObject4 = m;
        if (localObject4 == null)
        {
          localObject5 = "utilityEntry";
          k.a((String)localObject5);
        }
        localObject4 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).c().iterator();
        Object localObject6;
        int i3;
        do
        {
          boolean bool6;
          do
          {
            bool6 = ((Iterator)localObject4).hasNext();
            if (!bool6) {
              break;
            }
            localObject5 = (com.truecaller.truepay.app.ui.payments.models.a)((Iterator)localObject4).next();
            k.a(localObject5, "field");
            a((HashMap)localObject1, (com.truecaller.truepay.app.ui.payments.models.a)localObject5);
            r.add(localObject5);
            localObject6 = "operator";
            localObject5 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject5).f();
            bool6 = c.n.m.a((String)localObject6, (String)localObject5, bool3);
          } while (!bool6);
          localObject5 = paramBundle.c();
          i3 = ((List)localObject5).size();
        } while (i3 <= 0);
        Object localObject5 = paramBundle.c().iterator();
        for (;;)
        {
          boolean bool7 = ((Iterator)localObject5).hasNext();
          if (!bool7) {
            break;
          }
          localObject6 = (com.truecaller.truepay.app.ui.payments.models.a)((Iterator)localObject5).next();
          k.a(localObject6, "innerField");
          a((HashMap)localObject1, (com.truecaller.truepay.app.ui.payments.models.a)localObject6);
          ArrayList localArrayList = r;
          localArrayList.add(localObject6);
        }
        b(paramBundle);
        boolean bool5 = C;
        if (bool5)
        {
          bool5 = c;
          if (bool5)
          {
            bool5 = v;
            if (bool5)
            {
              paramBundle = (ag)bg.a;
              localObject1 = O;
              localObject3 = new com/truecaller/truepay/app/ui/payments/c/q$g;
              ((q.g)localObject3).<init>(this, null);
              localObject3 = (c.g.a.m)localObject3;
              paramBundle = kotlinx.coroutines.e.b(paramBundle, (f)localObject1, (c.g.a.m)localObject3, i1);
              E = paramBundle;
            }
          }
        }
      }
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.payments.models.BaseUtility");
    throw paramBundle;
  }
  
  public final void a(com.google.gson.i parami, com.truecaller.truepay.app.ui.transaction.b.p paramp)
  {
    k.b(parami, "data");
    k.b(paramp, "txnModel");
    v.c localc = new c/g/b/v$c;
    localc.<init>();
    Object localObject = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
    if (localObject == null) {
      return;
    }
    a = localObject;
    ((com.truecaller.truepay.app.ui.payments.views.c.d)a).c(true);
    localObject = new com/truecaller/truepay/app/ui/transaction/b/d;
    String str1 = paramp.f();
    String str2 = paramp.j();
    ((com.truecaller.truepay.app.ui.transaction.b.d)localObject).<init>(parami, str1, str2);
    parami = J.a((com.truecaller.truepay.app.ui.transaction.b.d)localObject);
    localObject = io.reactivex.g.a.b();
    parami = parami.b((io.reactivex.n)localObject);
    localObject = io.reactivex.android.b.a.a();
    parami = parami.a((io.reactivex.n)localObject);
    localObject = new com/truecaller/truepay/app/ui/payments/c/q$a;
    ((q.a)localObject).<init>(this, localc, paramp);
    localObject = (io.reactivex.q)localObject;
    parami.a((io.reactivex.q)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    k.b(parama, "field");
    Object localObject1 = af_();
    Object localObject2 = localObject1;
    localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.d)localObject1;
    if (localObject2 == null) {
      return;
    }
    localObject1 = parama.f();
    Object localObject3 = "operator_location";
    boolean bool1 = k.a(localObject1, localObject3);
    if (bool1)
    {
      localObject3 = n;
      com.truecaller.truepay.app.ui.payments.models.a locala1 = o;
      boolean bool2 = w;
      com.truecaller.truepay.app.ui.payments.models.a locala2 = m;
      if (locala2 == null)
      {
        parama = "utilityEntry";
        k.a(parama);
      }
      String str = h;
      ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject2).a((com.truecaller.truepay.app.ui.payments.models.a)localObject3, locala1, bool2, locala2, str);
      return;
    }
    parama = parama.f();
    localObject1 = "operator";
    boolean bool3 = k.a(parama, localObject1);
    if (bool3)
    {
      ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject2).b();
      ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject2).h();
    }
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.models.a parama1, com.truecaller.truepay.app.ui.payments.models.a parama2)
  {
    k.b(parama1, "operator");
    com.truecaller.truepay.app.ui.payments.views.c.d locald = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
    if (locald == null) {
      return;
    }
    a = parama1;
    b = parama2;
    locald.b(parama1, parama2);
    b(parama1);
  }
  
  public final void a(com.truecaller.truepay.app.ui.transaction.b.p paramp)
  {
    k.b(paramp, "txnModel");
    Object localObject1 = af_();
    Object localObject2 = localObject1;
    localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.d)localObject1;
    if (localObject2 == null) {
      return;
    }
    localObject1 = paramp.g();
    k.a(localObject1, "txnModel.account");
    localObject1 = ((com.truecaller.truepay.data.api.model.a)localObject1).j();
    String str1 = "pay_via_other";
    boolean bool1 = true;
    boolean bool2 = c.n.m.a((String)localObject1, str1, bool1);
    if (bool2)
    {
      str1 = paramp.l();
      String str2 = paramp.k();
      String str3 = paramp.d();
      String str4 = paramp.j();
      String str5 = paramp.w();
      ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject2).a(str1, str2, str3, str4, str5);
      return;
    }
    b(paramp);
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    k.b(parama, "selectedAccountvalue");
    z = parama;
    d();
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "rechargeNumber");
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = b;
      if (localObject1 == null)
      {
        localObject1 = new com/truecaller/truepay/data/api/model/x;
        ((x)localObject1).<init>();
        ((x)localObject1).a(paramString);
        paramString = m;
        if (paramString == null)
        {
          localObject2 = "utilityEntry";
          k.a((String)localObject2);
        }
        paramString = paramString.j();
        ((x)localObject1).b(paramString);
        paramString = new c/g/b/v$c;
        paramString.<init>();
        Object localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
        if (localObject2 == null) {
          return;
        }
        a = localObject2;
        ((com.truecaller.truepay.app.ui.payments.views.c.d)a).e(true);
        localObject1 = G.a((x)localObject1);
        localObject2 = io.reactivex.g.a.b();
        localObject1 = ((io.reactivex.o)localObject1).b((io.reactivex.n)localObject2);
        localObject2 = io.reactivex.android.b.a.a();
        localObject1 = ((io.reactivex.o)localObject1).a((io.reactivex.n)localObject2);
        localObject2 = new com/truecaller/truepay/app/ui/payments/c/q$c;
        ((q.c)localObject2).<init>(this, paramString);
        localObject2 = (io.reactivex.q)localObject2;
        ((io.reactivex.o)localObject1).a((io.reactivex.q)localObject2);
        return;
      }
    }
  }
  
  final void a(boolean paramBoolean)
  {
    com.truecaller.truepay.app.ui.payments.views.c.d locald = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
    if (locald == null) {
      return;
    }
    int i1 = locald.d();
    if (i1 <= 0) {
      return;
    }
    i1 = locald.d();
    int i2 = 1;
    int i3 = 1;
    while (i3 < i1)
    {
      locald.a(i3, paramBoolean);
      i3 += 1;
    }
    i1 = locald.k();
    while (i2 < i1)
    {
      locald.b(i2, paramBoolean);
      i2 += 1;
    }
    locald.g(paramBoolean);
  }
  
  public final void b()
  {
    super.b();
    n();
    bn localbn = E;
    if (localbn != null)
    {
      localbn.n();
      return;
    }
  }
  
  final void b(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    if (parama == null) {
      return;
    }
    com.truecaller.truepay.app.ui.payments.views.c.d locald = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
    if (locald == null) {
      return;
    }
    Object localObject = parama.m();
    boolean bool1 = false;
    boolean bool2 = true;
    if (localObject != null)
    {
      localObject = parama.m();
      String str = "selectedOperator.headerPresent";
      k.a(localObject, str);
      boolean bool3 = ((Boolean)localObject).booleanValue();
      if (bool3)
      {
        localObject = parama.n();
        if (localObject != null)
        {
          localObject = parama.n();
          str = "selectedOperator.headerLogo";
          k.a(localObject, str);
          localObject = (CharSequence)localObject;
          bool3 = c.n.m.a((CharSequence)localObject) ^ bool2;
          if (bool3)
          {
            c = bool2;
            localObject = parama.n();
            f = ((String)localObject);
            localObject = parama.n();
            locald.f((String)localObject);
            break label146;
          }
        }
      }
    }
    c = false;
    locald.m();
    label146:
    localObject = parama.l();
    if (localObject != null)
    {
      parama = parama.l();
      localObject = "selectedOperator.billFetchEnabled";
      k.a(parama, (String)localObject);
      bool4 = parama.booleanValue();
      if (bool4) {
        bool1 = true;
      }
    }
    v = bool1;
    boolean bool4 = v;
    if (bool4)
    {
      locald.n();
      return;
    }
    locald.o();
  }
  
  public final void b(com.truecaller.truepay.app.ui.transaction.b.p paramp)
  {
    k.b(paramp, "txnModel");
    v.c localc = new c/g/b/v$c;
    localc.<init>();
    Object localObject1 = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
    if (localObject1 == null) {
      return;
    }
    a = localObject1;
    ((com.truecaller.truepay.app.ui.payments.views.c.d)a).c(true);
    localObject1 = new com/truecaller/truepay/app/ui/transaction/b/j;
    String str1 = paramp.b();
    String str2 = paramp.c();
    String str3 = paramp.d();
    String str4 = paramp.j();
    String str5 = paramp.k();
    String str6 = paramp.m();
    String str7 = paramp.l();
    String str8 = paramp.r();
    String str9 = paramp.n();
    String str10 = paramp.t();
    String str11 = paramp.s();
    String str12 = paramp.C();
    ((j)localObject1).<init>(str1, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, str12);
    localObject1 = I.a((j)localObject1);
    Object localObject2 = io.reactivex.g.a.b();
    localObject1 = ((io.reactivex.o)localObject1).b((io.reactivex.n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((io.reactivex.o)localObject1).a((io.reactivex.n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/payments/c/q$d;
    ((q.d)localObject2).<init>(this, localc, paramp);
    localObject2 = (io.reactivex.q)localObject2;
    ((io.reactivex.o)localObject1).a((io.reactivex.q)localObject2);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "text");
    Object localObject = m;
    if (localObject == null)
    {
      String str = "utilityEntry";
      k.a(str);
    }
    localObject = ((com.truecaller.truepay.app.ui.payments.models.a)localObject).j();
    boolean bool1 = d((String)localObject);
    if (!bool1) {
      return;
    }
    bool1 = g;
    if (!bool1) {
      return;
    }
    localObject = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
    if (localObject == null) {
      return;
    }
    int i1 = paramString.length();
    int i2 = 3;
    boolean bool3 = false;
    boolean bool4 = true;
    if (i1 > i2)
    {
      ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject).a(false);
      paramString = a;
      if (paramString == null)
      {
        paramString = b;
        if (paramString == null) {
          bool3 = true;
        }
      }
      ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject).f(bool3);
      a(bool4);
      return;
    }
    ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject).a(bool4);
    boolean bool2 = g ^ bool4;
    ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject).f(bool2);
    a(false);
  }
  
  public final boolean b(int paramInt)
  {
    int i1 = 13;
    if (paramInt == i1)
    {
      String str = p;
      if (str == null)
      {
        str = q;
        if (str == null) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "tag");
    com.truecaller.truepay.app.ui.payments.views.c.d locald = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
    if (locald == null) {
      return;
    }
    boolean bool1 = x;
    if (bool1)
    {
      String str1 = "datacard";
      Object localObject = m;
      if (localObject == null)
      {
        String str2 = "utilityEntry";
        k.a(str2);
      }
      localObject = ((com.truecaller.truepay.app.ui.payments.models.a)localObject).j();
      bool1 = k.a(str1, localObject);
      if (bool1)
      {
        str1 = "postpaid";
        paramString = paramString.toLowerCase();
        localObject = "(this as java.lang.String).toLowerCase()";
        k.a(paramString, (String)localObject);
        bool2 = k.a(str1, paramString);
        if (bool2)
        {
          locald.b(false);
          return;
        }
      }
      boolean bool2 = true;
      locald.b(bool2);
    }
  }
  
  public final void d()
  {
    com.truecaller.truepay.app.ui.payments.views.c.d locald = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
    if (locald == null) {
      return;
    }
    Object localObject1 = z;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = ((com.truecaller.truepay.data.api.model.a)localObject1).j();
    String str = "pay_via_other";
    boolean bool1 = true;
    boolean bool2 = c.n.m.a((String)localObject2, str, bool1);
    if (bool2)
    {
      localObject2 = j;
      int i1 = R.string.pay_via_other;
      bool1 = false;
      Object[] arrayOfObject = new Object[0];
      localObject2 = ((com.truecaller.utils.n)localObject2).a(i1, arrayOfObject);
      locald.b((String)localObject2);
    }
    else
    {
      localObject2 = ((com.truecaller.truepay.data.api.model.a)localObject1).l();
      k.a(localObject2, "selectedAccount.bank");
      localObject2 = ((com.truecaller.truepay.data.d.a)localObject2).b();
      str = ((com.truecaller.truepay.data.api.model.a)localObject1).d();
      localObject2 = au.a((String)localObject2, str);
      locald.b((String)localObject2);
    }
    localObject1 = ((com.truecaller.truepay.data.api.model.a)localObject1).l();
    k.a(localObject1, "selectedAccount.bank");
    localObject1 = ((com.truecaller.truepay.data.d.a)localObject1).d();
    locald.c((String)localObject1);
  }
  
  public final void e()
  {
    com.truecaller.truepay.app.ui.payments.views.c.d locald = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
    if (locald == null) {
      return;
    }
    Object localObject1 = r;
    boolean bool1 = ((ArrayList)localObject1).isEmpty();
    if (bool1) {
      return;
    }
    localObject1 = r;
    Iterator localIterator = ((ArrayList)localObject1).iterator();
    Object localObject2 = null;
    Object localObject3 = null;
    for (;;)
    {
      bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = localIterator.next();
      Object localObject4 = localObject1;
      localObject4 = (com.truecaller.truepay.app.ui.payments.models.a)localObject1;
      k.a(localObject4, "field");
      localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).f();
      Object localObject5 = "api";
      bool1 = k.a(localObject1, localObject5);
      boolean bool2 = true;
      bool1 ^= bool2;
      if (bool1)
      {
        localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).f();
        Object localObject6 = "amount";
        bool1 = k.a(localObject1, localObject6);
        boolean bool5;
        boolean bool6;
        Object localObject7;
        Object localObject8;
        int i2;
        if (bool1)
        {
          bool1 = v;
          int i1;
          if (!bool1)
          {
            localObject1 = a;
            if (localObject1 != null)
            {
              localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).d();
              localObject6 = localObject1;
            }
            else
            {
              i1 = 0;
              localObject6 = null;
            }
            bool5 = x;
            bool6 = w;
            localObject7 = "";
            localObject1 = locald;
            localObject5 = localObject4;
            locald.a((com.truecaller.truepay.app.ui.payments.models.a)localObject4, (String)localObject6, bool5, bool6, (String)localObject7);
          }
          bool1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).p();
          if (bool1)
          {
            localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).q();
            k.a(localObject1, "field.presetAmountList");
            localObject1 = (Iterable)localObject1;
            localObject5 = new java/util/ArrayList;
            i1 = c.a.m.a((Iterable)localObject1, 10);
            ((ArrayList)localObject5).<init>(i1);
            localObject5 = (Collection)localObject5;
            localObject1 = ((Iterable)localObject1).iterator();
            for (;;)
            {
              boolean bool3 = ((Iterator)localObject1).hasNext();
              if (!bool3) {
                break;
              }
              localObject6 = (String)((Iterator)localObject1).next();
              localObject8 = "it";
              k.a(localObject6, (String)localObject8);
              i2 = Integer.parseInt((String)localObject6);
              localObject6 = Integer.valueOf(i2);
              ((Collection)localObject5).add(localObject6);
            }
            localObject5 = (List)localObject5;
            locald.b((List)localObject5);
          }
        }
        else
        {
          localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).f();
          localObject6 = "phone";
          bool1 = k.a(localObject1, localObject6);
          if (bool1)
          {
            localObject1 = "google_play";
            localObject5 = a;
            if (localObject5 != null)
            {
              localObject5 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject5).b();
            }
            else
            {
              bool2 = false;
              localObject5 = null;
            }
            bool1 = k.a(localObject1, localObject5);
            if (bool1)
            {
              localObject1 = L;
              localObject5 = "s#&bf2)^hn@1lp*n";
              localObject6 = "";
              localObject1 = ((com.truecaller.truepay.data.e.c)localObject1).a((String)localObject5, (String)localObject6);
            }
            else
            {
              localObject1 = "";
            }
            localObject7 = localObject1;
            localObject1 = a;
            if (localObject1 != null)
            {
              localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).d();
              localObject6 = localObject1;
            }
            else
            {
              i2 = 0;
              localObject6 = null;
            }
            bool5 = x;
            bool6 = w;
            localObject1 = locald;
            localObject5 = localObject4;
            locald.a((com.truecaller.truepay.app.ui.payments.models.a)localObject4, (String)localObject6, bool5, bool6, (String)localObject7);
          }
          else
          {
            localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).f();
            localObject6 = "plan_button";
            bool1 = k.a(localObject1, localObject6);
            if (bool1)
            {
              x = bool2;
            }
            else
            {
              localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).f();
              localObject6 = "operator_location";
              bool1 = k.a(localObject1, localObject6);
              if (!bool1)
              {
                localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).f();
                localObject6 = "operator";
                bool1 = k.a(localObject1, localObject6);
                if (!bool1)
                {
                  localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).f();
                  localObject5 = "radio_button";
                  bool1 = k.a(localObject1, localObject5);
                  if (bool1)
                  {
                    locald.a((com.truecaller.truepay.app.ui.payments.models.a)localObject4);
                    continue;
                  }
                  localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).f();
                  localObject5 = "non_ui";
                  bool1 = k.a(localObject1, localObject5);
                  if (bool1)
                  {
                    localObject1 = (Map)u;
                    localObject5 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).e();
                    k.a(localObject5, "field.key");
                    localObject6 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).k();
                    localObject8 = "field.value";
                    k.a(localObject6, (String)localObject8);
                    ((Map)localObject1).put(localObject5, localObject6);
                    continue;
                  }
                  localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).f();
                  localObject5 = "disc";
                  bool1 = k.a(localObject1, localObject5);
                  if (bool1)
                  {
                    locald.b((com.truecaller.truepay.app.ui.payments.models.a)localObject4);
                    continue;
                  }
                  localObject1 = a;
                  if (localObject1 != null)
                  {
                    localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).d();
                    localObject6 = localObject1;
                  }
                  else
                  {
                    i2 = 0;
                    localObject6 = null;
                  }
                  bool5 = x;
                  bool6 = w;
                  localObject7 = "";
                  localObject1 = locald;
                  localObject5 = localObject4;
                  locald.a((com.truecaller.truepay.app.ui.payments.models.a)localObject4, (String)localObject6, bool5, bool6, (String)localObject7);
                  continue;
                }
              }
              localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).f();
              localObject6 = "operator_location";
              bool1 = k.a(localObject1, localObject6);
              if (bool1) {
                A = bool2;
              }
              bool1 = w;
              boolean bool4;
              if (!bool1)
              {
                localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).c();
                if (localObject1 != null)
                {
                  localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).c().iterator();
                  for (;;)
                  {
                    bool4 = ((Iterator)localObject1).hasNext();
                    if (!bool4) {
                      break;
                    }
                    localObject6 = (com.truecaller.truepay.app.ui.payments.models.a)((Iterator)localObject1).next();
                    localObject8 = "operator";
                    k.a(localObject6, "subItem");
                    String str = ((com.truecaller.truepay.app.ui.payments.models.a)localObject6).f();
                    bool5 = c.n.m.a((String)localObject8, str, bool2);
                    if (bool5)
                    {
                      n = ((com.truecaller.truepay.app.ui.payments.models.a)localObject6);
                      localObject8 = p;
                      if (localObject8 != null)
                      {
                        localObject8 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject6).c();
                        if (localObject8 != null)
                        {
                          localObject6 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject6).c().iterator();
                          do
                          {
                            bool5 = ((Iterator)localObject6).hasNext();
                            if (!bool5) {
                              break;
                            }
                            localObject8 = (com.truecaller.truepay.app.ui.payments.models.a)((Iterator)localObject6).next();
                            str = p;
                            k.a(localObject8, "item");
                            localObject7 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject8).b();
                            bool6 = k.a(str, localObject7);
                          } while (!bool6);
                          localObject2 = localObject8;
                        }
                      }
                    }
                    else
                    {
                      localObject8 = "location";
                      str = ((com.truecaller.truepay.app.ui.payments.models.a)localObject6).f();
                      bool5 = c.n.m.a((String)localObject8, str, bool2);
                      if (bool5)
                      {
                        o = ((com.truecaller.truepay.app.ui.payments.models.a)localObject6);
                        localObject8 = q;
                        if (localObject8 != null)
                        {
                          localObject8 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject6).c();
                          if (localObject8 != null)
                          {
                            localObject6 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject6).c().iterator();
                            do
                            {
                              bool5 = ((Iterator)localObject6).hasNext();
                              if (!bool5) {
                                break;
                              }
                              localObject8 = (com.truecaller.truepay.app.ui.payments.models.a)((Iterator)localObject6).next();
                              str = q;
                              k.a(localObject8, "item");
                              localObject7 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject8).b();
                              bool6 = k.a(str, localObject7);
                            } while (!bool6);
                            localObject3 = localObject8;
                          }
                        }
                      }
                    }
                  }
                }
              }
              localObject1 = a;
              if (localObject1 != null)
              {
                localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).d();
                localObject6 = localObject1;
              }
              else
              {
                bool4 = false;
                localObject6 = null;
              }
              bool5 = x;
              bool6 = w;
              localObject7 = "";
              localObject1 = locald;
              localObject5 = localObject4;
              locald.a((com.truecaller.truepay.app.ui.payments.models.a)localObject4, (String)localObject6, bool5, bool6, (String)localObject7);
            }
          }
        }
      }
    }
    boolean bool7 = A;
    if (bool7)
    {
      if ((localObject2 != null) && (localObject3 != null))
      {
        a((com.truecaller.truepay.app.ui.payments.models.a)localObject2, (com.truecaller.truepay.app.ui.payments.models.a)localObject3);
        return;
      }
      return;
    }
    if (localObject2 != null)
    {
      a((com.truecaller.truepay.app.ui.payments.models.a)localObject2, null);
      return;
    }
  }
  
  public final void f()
  {
    com.truecaller.truepay.app.ui.payments.views.c.d locald = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
    if (locald != null)
    {
      com.truecaller.truepay.app.ui.payments.models.a locala = a;
      Object localObject = m;
      if (localObject == null)
      {
        localObject = "utilityEntry";
        k.a((String)localObject);
      }
      locald.c(locala);
      return;
    }
  }
  
  public final void g()
  {
    boolean bool1 = B;
    if (!bool1) {
      return;
    }
    Object localObject1 = L.a("s#&bf2)^hn@1lp*n", "");
    Object localObject2 = localObject1;
    localObject2 = (CharSequence)localObject1;
    int i1 = 0;
    Object localObject3 = null;
    if (localObject2 != null)
    {
      bool2 = c.n.m.a((CharSequence)localObject2);
      if (!bool2)
      {
        bool2 = false;
        localObject2 = null;
        break label66;
      }
    }
    boolean bool2 = true;
    label66:
    if (bool2)
    {
      localObject1 = new com/truecaller/log/UnmutedException$f;
      ((UnmutedException.f)localObject1).<init>("initiator msisdn is not present");
      com.truecaller.log.d.a((Throwable)localObject1);
      return;
    }
    localObject2 = z;
    if (localObject2 == null) {
      return;
    }
    Object localObject4 = a;
    if (localObject4 == null) {
      return;
    }
    com.truecaller.truepay.app.ui.transaction.b.p localp = new com/truecaller/truepay/app/ui/transaction/b/p;
    localp.<init>();
    Object localObject5 = ((com.truecaller.truepay.data.api.model.a)localObject2).j();
    localp.b((String)localObject5);
    localp.a((com.truecaller.truepay.data.api.model.a)localObject2);
    localp.c("");
    localp.d("utility");
    localp.a("Remarks");
    boolean bool3 = D;
    localp.a(bool3);
    localObject5 = s;
    String str = "amount";
    localObject5 = ((HashMap)localObject5).get(str);
    if (localObject5 != null)
    {
      localObject5 = localObject5.toString();
      localp.e((String)localObject5);
    }
    localObject5 = s;
    str = "recharge_number";
    localObject5 = ((HashMap)localObject5).get(str);
    if (localObject5 != null)
    {
      localObject5 = localObject5.toString();
      localp.k((String)localObject5);
    }
    localObject5 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).o();
    localp.m((String)localObject5);
    localObject5 = m;
    if (localObject5 == null)
    {
      str = "utilityEntry";
      k.a(str);
    }
    localObject5 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject5).j();
    localp.l((String)localObject5);
    localObject5 = new com/truecaller/truepay/app/ui/transaction/b/n;
    ((com.truecaller.truepay.app.ui.transaction.b.n)localObject5).<init>();
    str = "Payments";
    ((com.truecaller.truepay.app.ui.transaction.b.n)localObject5).a(str);
    localp.a((com.truecaller.truepay.app.ui.transaction.b.n)localObject5);
    localObject5 = (CharSequence)((com.truecaller.truepay.app.ui.payments.models.a)localObject4).r();
    if (localObject5 != null)
    {
      bool3 = c.n.m.a((CharSequence)localObject5);
      if (!bool3) {}
    }
    else
    {
      i1 = 1;
    }
    if (i1 != 0) {
      localObject3 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).d();
    } else {
      localObject3 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).r();
    }
    localp.j((String)localObject3);
    localObject3 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).b();
    localp.n((String)localObject3);
    localObject3 = b;
    if (localObject3 != null)
    {
      localObject6 = null;
      if (localObject3 != null)
      {
        localObject3 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject3).d();
      }
      else
      {
        i1 = 0;
        localObject3 = null;
      }
      localp.p((String)localObject3);
      localObject3 = b;
      if (localObject3 != null) {
        localObject6 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject3).b();
      }
      localp.o((String)localObject6);
    }
    localObject3 = y;
    if (localObject3 != null) {
      localp.v();
    }
    ((Map)s).put("initiator_msisdn", localObject1);
    localObject1 = (Map)s;
    localObject3 = "type";
    Object localObject6 = m;
    if (localObject6 == null)
    {
      localObject4 = "utilityEntry";
      k.a((String)localObject4);
    }
    localObject6 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject6).j();
    localObject4 = "utilityEntry.utilityType";
    k.a(localObject6, (String)localObject4);
    ((Map)localObject1).put(localObject3, localObject6);
    bool1 = ((com.truecaller.truepay.data.api.model.a)localObject2).n();
    if (!bool1)
    {
      localObject1 = ((com.truecaller.truepay.data.api.model.a)localObject2).l();
      localObject2 = "account.bank";
      k.a(localObject1, (String)localObject2);
      bool1 = ((com.truecaller.truepay.data.d.a)localObject1).e();
      if (bool1)
      {
        localObject1 = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
        if (localObject1 != null) {
          ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject1).l();
        }
        return;
      }
    }
    bool1 = v;
    if (bool1)
    {
      localObject1 = s;
      a(localp, (HashMap)localObject1);
      return;
    }
    localObject1 = s;
    b(localp, (HashMap)localObject1);
  }
  
  public final void h()
  {
    com.truecaller.truepay.app.ui.payments.views.c.d locald = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
    if (locald != null)
    {
      locald.e();
      return;
    }
  }
  
  public final void i()
  {
    com.truecaller.truepay.app.ui.payments.views.c.d locald = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
    if (locald == null) {
      return;
    }
    Object localObject1 = "";
    boolean bool = A;
    Object localObject2 = null;
    Object localObject3;
    if (bool)
    {
      localObject1 = a;
      if (localObject1 == null)
      {
        localObject1 = b;
        if (localObject1 == null)
        {
          localObject1 = j;
          int i1 = R.string.select_operator_location_message;
          localObject2 = new Object[0];
          localObject1 = ((com.truecaller.utils.n)localObject1).a(i1, (Object[])localObject2);
          locald.d((String)localObject1);
          return;
        }
      }
      localObject1 = a;
      if (localObject1 != null)
      {
        localObject1 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).b();
        if (localObject1 != null) {}
      }
      else
      {
        localObject1 = "";
      }
      localObject3 = b;
      if (localObject3 != null)
      {
        localObject3 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject3).b();
        if (localObject3 != null) {}
      }
      else
      {
        localObject3 = "";
      }
    }
    else
    {
      localObject3 = a;
      if (localObject3 == null)
      {
        localObject3 = j;
        int i2 = R.string.select_operator_message;
        localObject2 = new Object[0];
        localObject3 = ((com.truecaller.utils.n)localObject3).a(i2, (Object[])localObject2);
        locald.d((String)localObject3);
      }
      localObject3 = a;
      if (localObject3 != null)
      {
        localObject3 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject3).b();
        if (localObject3 != null) {}
      }
      else
      {
        localObject3 = "";
      }
      Object localObject4 = localObject3;
      localObject3 = localObject1;
      localObject1 = localObject4;
    }
    localObject2 = m;
    if (localObject2 == null)
    {
      String str = "utilityEntry";
      k.a(str);
    }
    localObject2 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject2).j();
    locald.a((String)localObject2, (String)localObject1, (String)localObject3);
    locald.b();
  }
  
  public final void j()
  {
    Object localObject1 = "ola";
    Object localObject2 = a;
    boolean bool1;
    if (localObject2 != null)
    {
      localObject2 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject2).b();
    }
    else
    {
      bool1 = false;
      localObject2 = null;
    }
    boolean bool2 = true;
    boolean bool3 = c.n.m.a((String)localObject1, (String)localObject2, bool2);
    if (!bool3)
    {
      localObject1 = "google_play";
      localObject2 = a;
      if (localObject2 != null)
      {
        localObject2 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject2).b();
      }
      else
      {
        bool1 = false;
        localObject2 = null;
      }
      bool3 = c.n.m.a((String)localObject1, (String)localObject2, bool2);
      if (!bool3) {
        bool2 = false;
      }
    }
    if (!bool2)
    {
      a = null;
      b = null;
      y = null;
      localObject1 = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
      if (localObject1 != null)
      {
        bool1 = g;
        ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject1).d(bool1);
        return;
      }
    }
  }
  
  public final void k()
  {
    boolean bool1 = w;
    if (!bool1)
    {
      localObject = a;
      if (localObject == null)
      {
        localObject = b;
        if (localObject == null)
        {
          localObject = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
          if (localObject != null)
          {
            boolean bool2 = g ^ true;
            ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject).f(bool2);
          }
          return;
        }
      }
    }
    Object localObject = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
    if (localObject != null)
    {
      ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject).f(false);
      return;
    }
  }
  
  public final void l()
  {
    com.truecaller.truepay.data.api.model.a locala = z;
    if (locala == null) {
      return;
    }
    locala.e(true);
    M.c(locala);
  }
  
  public final void m()
  {
    com.truecaller.truepay.data.api.model.a locala = z;
    if (locala == null) {
      return;
    }
    com.truecaller.truepay.app.ui.payments.views.c.d locald = (com.truecaller.truepay.app.ui.payments.views.c.d)af_();
    if (locald != null)
    {
      locald.b(locala);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */