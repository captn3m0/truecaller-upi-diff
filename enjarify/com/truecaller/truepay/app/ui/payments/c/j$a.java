package com.truecaller.truepay.app.ui.payments.c;

import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.truepay.R.string;
import com.truecaller.utils.n;
import java.util.Collection;
import java.util.List;
import kotlinx.coroutines.ag;

final class j$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag g;
  
  j$a(j paramj, com.truecaller.truepay.app.ui.payments.models.a parama, String paramString, List paramList, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/truepay/app/ui/payments/c/j$a;
    j localj = c;
    com.truecaller.truepay.app.ui.payments.models.a locala1 = d;
    String str = e;
    List localList = f;
    locala.<init>(localj, locala1, str, localList, paramc);
    paramObject = (ag)paramObject;
    g = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = b;
    Object localObject2 = null;
    int j = 1;
    boolean bool2;
    boolean bool3;
    Object localObject3;
    Object localObject4;
    Object[] arrayOfObject;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label440;
      }
      j.a(c).clear();
      paramObject = d;
      bool3 = j.a((com.truecaller.truepay.app.ui.payments.models.a)paramObject);
      if (!bool3) {
        break label299;
      }
      paramObject = e;
      if (paramObject == null) {
        break label299;
      }
      localObject3 = j.b(c);
      localObject4 = new com/truecaller/truepay/app/ui/payments/c/j$a$a;
      arrayOfObject = null;
      ((j.a.a)localObject4).<init>(null, this);
      localObject4 = (m)localObject4;
      a = paramObject;
      b = j;
      paramObject = kotlinx.coroutines.g.a((f)localObject3, (m)localObject4, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (List)paramObject;
    Object localObject5;
    int k;
    if (paramObject != null)
    {
      paramObject = (Collection)paramObject;
      bool2 = ((Collection)paramObject).isEmpty() ^ j;
      if (bool2 == j)
      {
        localObject1 = j.a(c);
        localObject3 = new com/truecaller/truepay/app/ui/payments/c/g;
        localObject5 = j.d(c);
        k = R.string.tc_pay_payment_details_recent;
        arrayOfObject = new Object[0];
        localObject5 = ((n)localObject5).a(k, arrayOfObject);
        localObject4 = "resourceProvider.getStri…y_payment_details_recent)";
        c.g.b.k.a(localObject5, (String)localObject4);
        ((g)localObject3).<init>((String)localObject5);
        ((List)localObject1).add(localObject3);
        localObject1 = j.a(c);
        ((List)localObject1).addAll((Collection)paramObject);
      }
    }
    label299:
    paramObject = f;
    if (paramObject != null)
    {
      localObject1 = j.a(c);
      localObject3 = new com/truecaller/truepay/app/ui/payments/c/g;
      localObject5 = j.d(c);
      k = R.string.all_operators;
      localObject2 = new Object[0];
      localObject2 = ((n)localObject5).a(k, (Object[])localObject2);
      localObject5 = "resourceProvider.getString(R.string.all_operators)";
      c.g.b.k.a(localObject2, (String)localObject5);
      ((g)localObject3).<init>((String)localObject2);
      ((List)localObject1).add(localObject3);
      localObject1 = j.a(c);
      paramObject = (Collection)paramObject;
      bool3 = ((List)localObject1).addAll((Collection)paramObject);
      Boolean.valueOf(bool3);
    }
    paramObject = j.e(c);
    if (paramObject != null)
    {
      localObject1 = j.a(c);
      ((i.b)paramObject).a((List)localObject1);
    }
    return x.a;
    label440:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.j.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */