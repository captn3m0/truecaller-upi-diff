package com.truecaller.truepay.app.ui.payments.c;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.transaction.b.l;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.data.api.model.am;
import com.truecaller.truepay.data.api.model.h;
import com.truecaller.utils.n;

final class ag$b
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private kotlinx.coroutines.ag f;
  
  ag$b(ag paramag, String paramString1, String paramString2, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/truepay/app/ui/payments/c/ag$b;
    ag localag = c;
    String str1 = d;
    String str2 = e;
    localb.<init>(localag, str1, str2, paramc);
    paramObject = (kotlinx.coroutines.ag)paramObject;
    f = ((kotlinx.coroutines.ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    int j = 1;
    boolean bool1;
    Object localObject2;
    Object localObject3;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label370;
      }
      paramObject = f;
      localObject2 = c;
      localObject3 = d;
      String str = e;
      a = paramObject;
      b = j;
      paramObject = ((ag)localObject2).a((String)localObject3, str, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (h)paramObject;
    boolean bool2 = false;
    localObject1 = null;
    if (paramObject != null)
    {
      localObject2 = ((h)paramObject).a();
      bool1 = c.g.b.k.a(localObject2, "success");
      localObject3 = null;
      if (bool1)
      {
        localObject2 = (am)((h)paramObject).c();
        if (localObject2 != null)
        {
          localObject2 = ((am)localObject2).a();
        }
        else
        {
          bool1 = false;
          localObject2 = null;
        }
        if (localObject2 != null) {}
      }
      else
      {
        j = 0;
      }
      if (j != 0) {
        localObject3 = paramObject;
      }
      if (localObject3 != null)
      {
        localObject1 = ag.a(c);
        if (localObject1 != null)
        {
          paramObject = ((h)paramObject).c();
          localObject2 = "res.data";
          c.g.b.k.a(paramObject, (String)localObject2);
          paramObject = ((am)paramObject).a();
          ((p)localObject1).a((l)paramObject);
          ((p)localObject1).q();
          paramObject = c;
          ag.a((ag)paramObject, (p)localObject1);
          break label348;
        }
        paramObject = "txnModel should not be null";
        new String[1][0] = paramObject;
        break label348;
      }
    }
    paramObject = ag.b(c);
    if (paramObject != null)
    {
      localObject2 = ag.c(c);
      j = R.string.error_status_check;
      localObject1 = new Object[0];
      localObject1 = ((n)localObject2).a(j, (Object[])localObject1);
      localObject2 = "resourceProvider.getStri…tring.error_status_check)";
      c.g.b.k.a(localObject1, (String)localObject2);
      ((af.c)paramObject).d((String)localObject1);
    }
    label348:
    paramObject = ag.b(c);
    if (paramObject != null) {
      ((af.c)paramObject).i();
    }
    return x.a;
    label370:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.ag.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */