package com.truecaller.truepay.app.ui.payments.c;

import c.g.b.k;
import com.truecaller.truepay.app.ui.payments.views.c.d;
import com.truecaller.truepay.app.ui.transaction.b.p;
import io.reactivex.a.a;

public final class q$e
  extends com.truecaller.truepay.app.ui.npci.b
{
  q$e(q paramq, p paramp) {}
  
  public final void a()
  {
    super.a();
    d locald = (d)a.af_();
    if (locald != null)
    {
      locald.c(true);
      return;
    }
  }
  
  public final void a(io.reactivex.a.b paramb)
  {
    k.b(paramb, "d");
    super.a(paramb);
    a.e.a(paramb);
  }
  
  public final void a(String paramString, int paramInt)
  {
    String str = "data";
    k.b(paramString, str);
    super.a(paramString, paramInt);
    d locald = (d)a.af_();
    if (locald == null) {
      return;
    }
    locald.c(false);
    locald.e(paramString);
  }
  
  public final void b()
  {
    super.b();
    d locald = (d)a.af_();
    if (locald != null)
    {
      locald.c(false);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.q.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */