package com.truecaller.truepay.app.ui.payments.c;

import android.os.Bundle;
import com.truecaller.truepay.app.ui.history.models.h;
import com.truecaller.truepay.app.ui.payments.models.c;

public abstract class p
  extends com.truecaller.truepay.app.ui.base.a.a
{
  public abstract void a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(int paramInt, h paramh);
  
  public abstract void a(int paramInt, c paramc);
  
  public abstract void a(Bundle paramBundle);
  
  public abstract void a(com.truecaller.truepay.app.ui.payments.models.a parama);
  
  public abstract void a(com.truecaller.truepay.app.ui.payments.models.a parama1, com.truecaller.truepay.app.ui.payments.models.a parama2);
  
  public abstract void a(com.truecaller.truepay.app.ui.transaction.b.p paramp);
  
  public abstract void a(com.truecaller.truepay.data.api.model.a parama);
  
  public abstract void a(String paramString);
  
  public abstract void b(String paramString);
  
  public abstract boolean b(int paramInt);
  
  public abstract void c(String paramString);
  
  public abstract void d();
  
  public abstract void e();
  
  public abstract void f();
  
  public abstract void g();
  
  public abstract void h();
  
  public abstract void i();
  
  public abstract void j();
  
  public abstract void k();
  
  public abstract void l();
  
  public abstract void m();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */