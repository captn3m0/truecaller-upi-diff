package com.truecaller.truepay.app.ui.payments.c;

import c.g.b.k;
import com.truecaller.bb;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.payments.models.RedBusTicket;
import com.truecaller.truepay.app.ui.payments.models.b;
import com.truecaller.truepay.data.api.h;
import io.reactivex.o;
import io.reactivex.q;

public final class x
  extends bb
  implements w.b
{
  final io.reactivex.a.a a;
  RedBusTicket c;
  String d;
  final com.truecaller.utils.n e;
  private String f;
  private final h g;
  
  public x(h paramh, com.truecaller.utils.n paramn)
  {
    g = paramh;
    e = paramn;
    paramh = new io/reactivex/a/a;
    paramh.<init>();
    a = paramh;
    f = "";
    d = "";
  }
  
  public final void a()
  {
    RedBusTicket localRedBusTicket = c;
    if (localRedBusTicket != null)
    {
      w.c localc = (w.c)b;
      if (localc != null)
      {
        localc.a(localRedBusTicket);
        return;
      }
      return;
    }
  }
  
  public final void a(String paramString, boolean paramBoolean)
  {
    String str1 = "referenceId";
    k.b(paramString, str1);
    f = paramString;
    if (paramBoolean) {
      localObject = "history";
    } else {
      localObject = "sdk";
    }
    d = ((String)localObject);
    Object localObject = Truepay.getInstance();
    str1 = "Truepay.getInstance()";
    k.a(localObject, str1);
    localObject = ((Truepay)localObject).getAnalyticLoggerHelper();
    if (localObject != null)
    {
      str1 = "triggered";
      String str2 = d;
      String str3 = "bus";
      ((com.truecaller.truepay.data.b.a)localObject).c(str1, str2, str3);
    }
    localObject = (w.c)b;
    if (localObject != null) {
      ((w.c)localObject).b();
    }
    localObject = new com/truecaller/truepay/app/ui/payments/models/b;
    ((b)localObject).<init>(paramString, "data");
    paramString = g.a((b)localObject);
    localObject = io.reactivex.g.a.b();
    paramString = paramString.b((io.reactivex.n)localObject);
    localObject = io.reactivex.android.b.a.a();
    paramString = paramString.a((io.reactivex.n)localObject);
    localObject = new com/truecaller/truepay/app/ui/payments/c/x$b;
    ((x.b)localObject).<init>(this);
    localObject = (q)localObject;
    paramString.a((q)localObject);
  }
  
  public final void b()
  {
    Object localObject1 = Truepay.getInstance();
    Object localObject2 = "Truepay.getInstance()";
    k.a(localObject1, (String)localObject2);
    localObject1 = ((Truepay)localObject1).getAnalyticLoggerHelper();
    if (localObject1 != null)
    {
      localObject2 = "initiated";
      String str1 = d;
      String str2 = "bus";
      ((com.truecaller.truepay.data.b.a)localObject1).c((String)localObject2, str1, str2);
    }
    localObject1 = (w.c)b;
    if (localObject1 != null) {
      ((w.c)localObject1).b();
    }
    localObject1 = new com/truecaller/truepay/app/ui/payments/models/b;
    localObject2 = f;
    ((b)localObject1).<init>((String)localObject2, "cancel");
    localObject1 = g.a((b)localObject1);
    localObject2 = io.reactivex.g.a.b();
    localObject1 = ((o)localObject1).b((io.reactivex.n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((o)localObject1).a((io.reactivex.n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/payments/c/x$a;
    ((x.a)localObject2).<init>(this);
    localObject2 = (q)localObject2;
    ((o)localObject1).a((q)localObject2);
  }
  
  public final void c()
  {
    w.c localc = (w.c)b;
    if (localc != null)
    {
      localc.d();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */