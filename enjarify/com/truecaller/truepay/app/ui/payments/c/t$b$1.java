package com.truecaller.truepay.app.ui.payments.c;

import c.o.b;
import c.x;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import kotlinx.coroutines.ag;

final class t$b$1
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag d;
  
  t$b$1(t.b paramb, HashMap paramHashMap, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/truepay/app/ui/payments/c/t$b$1;
    t.b localb = b;
    HashMap localHashMap = c;
    local1.<init>(localb, localHashMap, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = ((Iterable)b.c.c.a()).iterator();
        Object localObject2;
        String str;
        for (;;)
        {
          bool1 = ((Iterator)paramObject).hasNext();
          if (!bool1) {
            break;
          }
          localObject1 = (com.truecaller.truepay.data.db.c)((Iterator)paramObject).next();
          localObject2 = (Map)c;
          str = b;
          bool1 = c;
          localObject1 = Boolean.valueOf(bool1);
          ((Map)localObject2).put(str, localObject1);
        }
        paramObject = b.d.c();
        localObject1 = "utilityList.utilityFields";
        c.g.b.k.a(paramObject, (String)localObject1);
        paramObject = ((Iterable)c.a.m.e((Iterable)paramObject)).iterator();
        for (;;)
        {
          bool1 = ((Iterator)paramObject).hasNext();
          if (!bool1) {
            break;
          }
          localObject1 = (com.truecaller.truepay.app.ui.payments.models.a)((Iterator)paramObject).next();
          localObject2 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).c();
          c.g.b.k.a(localObject2, "it.utilityFields");
          localObject2 = ((Iterable)localObject2).iterator();
          str = null;
          int j = 0;
          boolean bool3;
          for (;;)
          {
            boolean bool2 = ((Iterator)localObject2).hasNext();
            bool3 = true;
            if (!bool2) {
              break;
            }
            com.truecaller.truepay.app.ui.payments.models.a locala = (com.truecaller.truepay.app.ui.payments.models.a)((Iterator)localObject2).next();
            Object localObject3 = c;
            Object localObject4 = locala.a();
            localObject3 = (Boolean)((HashMap)localObject3).get(localObject4);
            localObject4 = Boolean.TRUE;
            boolean bool4 = c.g.b.k.a(localObject3, localObject4);
            if (bool4)
            {
              locala.a(bool3);
              int k = j + 1;
              Integer.valueOf(j);
              j = k;
            }
            else
            {
              locala.a(false);
            }
          }
          if (j > 0) {
            ((com.truecaller.truepay.app.ui.payments.models.a)localObject1).a(bool3);
          }
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.t.b.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */