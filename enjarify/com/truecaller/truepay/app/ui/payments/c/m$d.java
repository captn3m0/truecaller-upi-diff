package com.truecaller.truepay.app.ui.payments.c;

import c.d.a.a;
import c.d.c;
import c.o.b;
import c.x;
import com.truecaller.common.h.q;
import com.truecaller.truepay.app.ui.transaction.b.j;
import com.truecaller.truepay.data.api.g;
import com.truecaller.truepay.data.api.model.h;
import e.b;
import e.r;
import kotlinx.coroutines.ag;

final class m$d
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag d;
  
  m$d(m paramm, j paramj, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/truepay/app/ui/payments/c/m$d;
    m localm = b;
    j localj = c;
    locald.<init>(localm, localj, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = m.g(b);
        localObject = c;
        paramObject = ((g)paramObject).b((j)localObject);
        localObject = "truepayApiService.initiatePayment(request)";
        c.g.b.k.a(paramObject, (String)localObject);
        paramObject = q.a((b)paramObject);
        if (paramObject != null) {
          return (h)((r)paramObject).e();
        }
        return null;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.m.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */