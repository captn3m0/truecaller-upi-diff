package com.truecaller.truepay.app.ui.payments.c;

import android.content.Context;
import android.support.v4.f.j;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.webapps.models.WebAppConfig;
import com.truecaller.truepay.app.utils.aj;
import com.truecaller.truepay.app.utils.ax;
import io.reactivex.n;
import io.reactivex.o;
import java.io.File;
import java.util.Iterator;
import java.util.List;

public final class s
  extends com.truecaller.truepay.app.ui.base.a.a
{
  public final com.truecaller.featuretoggles.e a;
  com.truecaller.truepay.data.e.e b;
  com.truecaller.truepay.data.e.a c;
  public com.truecaller.truepay.data.e.b f;
  com.truecaller.truepay.data.e.e g;
  com.truecaller.truepay.data.e.a h;
  com.truecaller.truepay.a.a.d.f i;
  com.truecaller.truepay.data.e.a j;
  public com.truecaller.truepay.a.a.a k;
  aj l;
  com.truecaller.truepay.data.e.c m;
  private final com.truecaller.truepay.a.a.c.e n;
  private ax o;
  private com.truecaller.truepay.app.utils.q p;
  private com.truecaller.truepay.a.a.c.c q;
  private com.google.gson.f r;
  
  public s(com.truecaller.truepay.a.a.d.f paramf, ax paramax, com.truecaller.truepay.app.utils.q paramq, com.truecaller.truepay.a.a.c.c paramc, com.truecaller.truepay.data.e.b paramb, com.truecaller.truepay.data.e.e parame1, com.truecaller.truepay.data.e.a parama1, com.truecaller.truepay.data.e.e parame2, com.truecaller.truepay.data.e.a parama2, com.truecaller.truepay.data.e.a parama3, com.truecaller.truepay.a.a.c.e parame, com.truecaller.truepay.a.a.a parama, aj paramaj, com.truecaller.featuretoggles.e parame3, com.google.gson.f paramf1, com.truecaller.truepay.data.e.c paramc1)
  {
    i = paramf;
    o = paramax;
    p = paramq;
    q = paramc;
    f = paramb;
    g = parame1;
    h = parama1;
    b = parame2;
    c = parama2;
    j = parama3;
    n = parame;
    k = parama;
    l = paramaj;
    a = parame3;
    r = paramf1;
    m = paramc1;
  }
  
  /* Error */
  private void c(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 43	com/truecaller/truepay/app/ui/payments/c/s:p	Lcom/truecaller/truepay/app/utils/q;
    //   4: astore_2
    //   5: ldc 112
    //   7: astore_3
    //   8: aconst_null
    //   9: astore 4
    //   11: new 114	java/io/FileWriter
    //   14: astore 5
    //   16: new 116	java/lang/StringBuilder
    //   19: astore 6
    //   21: ldc 118
    //   23: astore 7
    //   25: aload 6
    //   27: aload 7
    //   29: invokespecial 120	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   32: aload_2
    //   33: getfield 125	com/truecaller/truepay/app/utils/q:a	Landroid/content/Context;
    //   36: astore_2
    //   37: aload_2
    //   38: invokevirtual 130	android/content/Context:getPackageName	()Ljava/lang/String;
    //   41: astore_2
    //   42: aload 6
    //   44: aload_2
    //   45: invokevirtual 134	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   48: pop
    //   49: ldc -120
    //   51: astore_2
    //   52: aload 6
    //   54: aload_2
    //   55: invokevirtual 134	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   58: pop
    //   59: aload 6
    //   61: aload_3
    //   62: invokevirtual 134	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   65: pop
    //   66: aload 6
    //   68: invokevirtual 139	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   71: astore_2
    //   72: aload 5
    //   74: aload_2
    //   75: invokespecial 140	java/io/FileWriter:<init>	(Ljava/lang/String;)V
    //   78: aload 5
    //   80: aload_1
    //   81: invokevirtual 143	java/io/FileWriter:write	(Ljava/lang/String;)V
    //   84: aload 5
    //   86: invokevirtual 146	java/io/FileWriter:flush	()V
    //   89: aload 5
    //   91: invokestatic 151	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   94: return
    //   95: astore_1
    //   96: aload 5
    //   98: astore 4
    //   100: goto +32 -> 132
    //   103: pop
    //   104: aload 5
    //   106: astore 4
    //   108: goto +8 -> 116
    //   111: astore_1
    //   112: goto +20 -> 132
    //   115: pop
    //   116: ldc -103
    //   118: astore_1
    //   119: iconst_1
    //   120: anewarray 86	java/lang/String
    //   123: iconst_0
    //   124: aload_1
    //   125: aastore
    //   126: aload 4
    //   128: invokestatic 151	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   131: return
    //   132: aload 4
    //   134: invokestatic 151	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   137: aload_1
    //   138: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	139	0	this	s
    //   0	139	1	paramString	String
    //   4	71	2	localObject1	Object
    //   7	55	3	str1	String
    //   9	124	4	localObject2	Object
    //   14	91	5	localFileWriter	java.io.FileWriter
    //   19	48	6	localStringBuilder	StringBuilder
    //   23	5	7	str2	String
    //   103	1	8	localIOException1	java.io.IOException
    //   115	1	9	localIOException2	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   80	84	95	finally
    //   84	89	95	finally
    //   80	84	103	java/io/IOException
    //   84	89	103	java/io/IOException
    //   11	14	111	finally
    //   16	19	111	finally
    //   27	32	111	finally
    //   32	36	111	finally
    //   37	41	111	finally
    //   44	49	111	finally
    //   54	59	111	finally
    //   61	66	111	finally
    //   66	71	111	finally
    //   74	78	111	finally
    //   119	126	111	finally
    //   11	14	115	java/io/IOException
    //   16	19	115	java/io/IOException
    //   27	32	115	java/io/IOException
    //   32	36	115	java/io/IOException
    //   37	41	115	java/io/IOException
    //   44	49	115	java/io/IOException
    //   54	59	115	java/io/IOException
    //   61	66	115	java/io/IOException
    //   66	71	115	java/io/IOException
    //   74	78	115	java/io/IOException
  }
  
  public final void a()
  {
    Object localObject1 = Truepay.getInstance();
    boolean bool1 = ((Truepay)localObject1).isRegistrationComplete();
    if (bool1)
    {
      localObject1 = p;
      Object localObject2 = "upreftils_v1";
      File localFile = new java/io/File;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      String str = "/data/data/";
      localStringBuilder.<init>(str);
      localObject1 = a.getPackageName();
      localStringBuilder.append((String)localObject1);
      localStringBuilder.append("/");
      localStringBuilder.append((String)localObject2);
      localObject1 = localStringBuilder.toString();
      localFile.<init>((String)localObject1);
      bool1 = localFile.exists();
      if (bool1)
      {
        long l1 = localFile.length();
        long l2 = 0L;
        boolean bool2 = l1 < l2;
        if (bool2)
        {
          bool1 = true;
          break label125;
        }
      }
      bool1 = false;
      localObject1 = null;
      label125:
      if (bool1)
      {
        localObject1 = p.a("upreftils_v1");
        localObject2 = io.reactivex.g.a.b();
        localObject1 = ((o)localObject1).b((n)localObject2);
        localObject2 = new com/truecaller/truepay/app/ui/payments/c/-$$Lambda$s$Zk9FXbSTkeTPGbiGOMXho20i-qE;
        ((-..Lambda.s.Zk9FXbSTkeTPGbiGOMXho20i-qE)localObject2).<init>(this);
        localObject1 = ((o)localObject1).a((io.reactivex.c.e)localObject2);
        localObject2 = io.reactivex.android.b.a.a();
        localObject1 = ((o)localObject1).a((n)localObject2);
        localObject2 = new com/truecaller/truepay/app/ui/payments/c/s$1;
        ((s.1)localObject2).<init>(this);
        ((o)localObject1).a((io.reactivex.q)localObject2);
        return;
      }
    }
    d();
    b.a("");
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    Object localObject1 = d;
    if (localObject1 == null) {
      return;
    }
    localObject1 = a.aa();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
    try
    {
      Object localObject2 = n;
      localObject2 = ((List)localObject2).iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject2).hasNext();
        if (!bool2) {
          break;
        }
        Object localObject3 = ((Iterator)localObject2).next();
        localObject3 = (com.truecaller.truepay.app.ui.payments.models.a)localObject3;
        Object localObject4 = "apps";
        Object localObject5 = a;
        boolean bool3 = ((String)localObject4).equalsIgnoreCase((String)localObject5);
        if (bool3)
        {
          localObject3 = n;
          localObject3 = ((List)localObject3).iterator();
          Object localObject6;
          boolean bool4;
          do
          {
            bool3 = ((Iterator)localObject3).hasNext();
            if (!bool3) {
              break;
            }
            localObject4 = ((Iterator)localObject3).next();
            localObject4 = (com.truecaller.truepay.app.ui.payments.models.a)localObject4;
            localObject5 = "ixigo";
            localObject6 = a;
            bool4 = ((String)localObject5).equalsIgnoreCase((String)localObject6);
          } while (!bool4);
          if (bool1)
          {
            localObject5 = n;
            int i1 = ((List)localObject5).size();
            if (i1 > 0)
            {
              localObject3 = r;
              i1 = 0;
              localObject6 = null;
              localObject5 = ((List)localObject5).get(0);
              localObject5 = (com.truecaller.truepay.app.ui.payments.models.a)localObject5;
              localObject5 = o;
              localObject6 = WebAppConfig.class;
              localObject3 = ((com.google.gson.f)localObject3).a((String)localObject5, (Class)localObject6);
              localObject3 = (WebAppConfig)localObject3;
              q = ((WebAppConfig)localObject3);
            }
            else
            {
              ((Iterator)localObject3).remove();
            }
          }
          else
          {
            ((Iterator)localObject3).remove();
          }
        }
      }
      return;
    }
    catch (Exception localException)
    {
      ((com.truecaller.truepay.app.ui.payments.views.c.e)d).a(parama);
    }
  }
  
  public final void a(String paramString)
  {
    Object localObject1 = o.e(paramString);
    Object localObject2 = io.reactivex.g.a.b();
    localObject1 = ((o)localObject1).b((n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/payments/c/-$$Lambda$s$YcjtGksAKajqPJgWkIJ6eGBCZ6U;
    ((-..Lambda.s.YcjtGksAKajqPJgWkIJ6eGBCZ6U)localObject2).<init>(this, paramString);
    paramString = ((o)localObject1).a((io.reactivex.c.e)localObject2);
    localObject1 = io.reactivex.android.b.a.a();
    paramString = paramString.a((n)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/payments/c/s$4;
    ((s.4)localObject1).<init>(this);
    paramString.a((io.reactivex.q)localObject1);
  }
  
  public final void b(String paramString)
  {
    Object localObject1 = d;
    if (localObject1 == null) {
      return;
    }
    paramString = (com.truecaller.truepay.app.ui.payments.models.a)r.a(paramString, com.truecaller.truepay.app.ui.payments.models.a.class);
    localObject1 = a;
    e.a locala = i;
    Object localObject2 = com.truecaller.featuretoggles.e.a;
    int i1 = 25;
    localObject2 = localObject2[i1];
    localObject1 = locala.a((com.truecaller.featuretoggles.e)localObject1, (c.l.g)localObject2);
    boolean bool = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool)
    {
      ((com.truecaller.truepay.app.ui.payments.views.c.e)d).c(paramString);
      return;
    }
    a(paramString);
  }
  
  public final void d()
  {
    o localo = o.f("utilities_struct");
    Object localObject = io.reactivex.g.a.b();
    localo = localo.b((n)localObject);
    localObject = new com/truecaller/truepay/app/ui/payments/c/-$$Lambda$s$IUKce3B2_nByZng4SclcD5k8hgs;
    ((-..Lambda.s.IUKce3B2_nByZng4SclcD5k8hgs)localObject).<init>(this);
    localo = localo.a((io.reactivex.c.e)localObject);
    localObject = io.reactivex.android.b.a.a();
    localo = localo.a((n)localObject);
    localObject = new com/truecaller/truepay/app/ui/payments/c/s$2;
    ((s.2)localObject).<init>(this);
    localo.a((io.reactivex.q)localObject);
  }
  
  public final void e()
  {
    Object localObject1 = Truepay.getInstance();
    boolean bool = ((Truepay)localObject1).isRegistrationComplete();
    if (!bool)
    {
      localObject1 = d;
      if (localObject1 != null)
      {
        localObject1 = (com.truecaller.truepay.app.ui.payments.views.c.e)d;
        ((com.truecaller.truepay.app.ui.payments.views.c.e)localObject1).d();
      }
      return;
    }
    localObject1 = q;
    Object localObject2 = new android/support/v4/f/j;
    ((j)localObject2).<init>(null, "recent_payments");
    localObject1 = ((com.truecaller.truepay.a.a.c.c)localObject1).a((j)localObject2);
    localObject2 = io.reactivex.g.a.b();
    localObject1 = ((io.reactivex.d)localObject1).b((n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((io.reactivex.d)localObject1).a((n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/payments/c/s$5;
    ((s.5)localObject2).<init>(this);
    ((io.reactivex.d)localObject1).a((io.reactivex.g)localObject2);
  }
  
  public final void f()
  {
    Object localObject1 = Truepay.getInstance();
    boolean bool = ((Truepay)localObject1).isRegistrationComplete();
    if (!bool) {
      return;
    }
    localObject1 = n.a();
    Object localObject2 = io.reactivex.g.a.b();
    localObject1 = ((io.reactivex.d)localObject1).b((n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((io.reactivex.d)localObject1).a((n)localObject2);
    localObject2 = -..Lambda.s.vs4mT_VLv60IHCBly-baiOBR-cI.INSTANCE;
    -..Lambda.s.l8rNWUHrWZ8H80tPK92zoFwerSg locall8rNWUHrWZ8H80tPK92zoFwerSg = new com/truecaller/truepay/app/ui/payments/c/-$$Lambda$s$l8rNWUHrWZ8H80tPK92zoFwerSg;
    locall8rNWUHrWZ8H80tPK92zoFwerSg.<init>(this);
    -..Lambda.s.uvGAbkPvpoRvcSCOwl-vjV6X2W8 localuvGAbkPvpoRvcSCOwl-vjV6X2W8 = new com/truecaller/truepay/app/ui/payments/c/-$$Lambda$s$uvGAbkPvpoRvcSCOwl-vjV6X2W8;
    localuvGAbkPvpoRvcSCOwl-vjV6X2W8.<init>(this);
    -..Lambda.s.hGYusbv3n_3LdBhj0edvA-EcEP8 localhGYusbv3n_3LdBhj0edvA-EcEP8 = -..Lambda.s.hGYusbv3n_3LdBhj0edvA-EcEP8.INSTANCE;
    localObject1 = ((io.reactivex.d)localObject1).a((io.reactivex.c.d)localObject2, locall8rNWUHrWZ8H80tPK92zoFwerSg, localuvGAbkPvpoRvcSCOwl-vjV6X2W8, localhGYusbv3n_3LdBhj0edvA-EcEP8);
    e.a((io.reactivex.a.b)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */