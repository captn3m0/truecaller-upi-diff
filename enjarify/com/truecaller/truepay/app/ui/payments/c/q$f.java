package com.truecaller.truepay.app.ui.payments.c;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.truepay.app.ui.payments.views.c.d;
import java.util.Collection;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class q$f
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  q$f(q paramq, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    f localf = new com/truecaller/truepay/app/ui/payments/c/q$f;
    q localq = b;
    String str = c;
    localf.<init>(localq, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localf;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    int j = 1;
    boolean bool3;
    Object localObject3;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label256;
      }
      paramObject = b.k;
      localObject2 = new com/truecaller/truepay/app/ui/payments/c/q$f$a;
      bool3 = false;
      localObject3 = null;
      ((q.f.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      a = j;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (List)paramObject;
    localObject1 = (d)b.af_();
    if (localObject1 == null) {
      return x.a;
    }
    boolean bool1 = false;
    Object localObject2 = null;
    if (paramObject != null)
    {
      localObject3 = paramObject;
      localObject3 = (Collection)paramObject;
      bool3 = ((Collection)localObject3).isEmpty() ^ j;
      if (bool3 == j)
      {
        ((d)localObject1).a((List)paramObject);
        b.a(false);
        paramObject = b;
        g = j;
        break label239;
      }
    }
    paramObject = b;
    g = false;
    ((d)localObject1).i();
    label239:
    b.k();
    ((d)localObject1).b();
    return x.a;
    label256:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (f)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((f)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.q.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */