package com.truecaller.truepay.app.ui.payments.c;

import c.g.b.k;
import c.g.b.v.c;
import c.n.m;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.transaction.b.o;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.data.api.model.h;
import com.truecaller.utils.n;
import io.reactivex.a.a;
import io.reactivex.a.b;

public final class q$b
  implements io.reactivex.q
{
  q$b(q paramq, v.c paramc, p paramp) {}
  
  private void a(h paramh)
  {
    k.b(paramh, "responseDO");
    Object localObject1 = b;
    Object localObject2 = (com.truecaller.truepay.app.ui.payments.views.c.d)a.af_();
    if (localObject2 == null) {
      return;
    }
    a = localObject2;
    localObject1 = (com.truecaller.truepay.app.ui.payments.views.c.d)b.a;
    localObject2 = null;
    ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject1).c(false);
    localObject1 = "success";
    Object localObject3 = paramh.a();
    boolean bool1 = true;
    boolean bool2 = m.a((String)localObject1, (String)localObject3, bool1);
    if (bool2)
    {
      localObject1 = paramh.c();
      if (localObject1 != null)
      {
        localObject1 = c;
        localObject2 = paramh.c();
        k.a(localObject2, "responseDO.data");
        localObject2 = ((o)localObject2).a();
        ((p)localObject1).h((String)localObject2);
        localObject2 = paramh.c();
        k.a(localObject2, "responseDO.data");
        localObject2 = ((o)localObject2).b();
        ((p)localObject1).i((String)localObject2);
        ((p)localObject1).i();
        localObject1 = c;
        localObject2 = paramh.c();
        k.a(localObject2, "responseDO.data");
        localObject2 = ((o)localObject2).e();
        ((p)localObject1).e((String)localObject2);
        localObject1 = a;
        localObject2 = c;
        localObject3 = paramh.a();
        k.a(localObject3, "responseDO.status");
        q.a((q)localObject1, (p)localObject2, (String)localObject3);
        localObject1 = (com.truecaller.truepay.app.ui.payments.views.c.d)b.a;
        paramh = (o)paramh.c();
        localObject2 = c;
        ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject1).a(paramh, (p)localObject2);
        return;
      }
    }
    localObject1 = a;
    localObject3 = c;
    String str = "failure";
    q.a((q)localObject1, (p)localObject3, str);
    localObject1 = (com.truecaller.truepay.app.ui.payments.views.c.d)b.a;
    paramh = paramh.b();
    if (paramh == null)
    {
      paramh = a.j;
      int i = R.string.bill_pay_error_message;
      localObject2 = new Object[0];
      paramh = paramh.a(i, (Object[])localObject2);
    }
    ((com.truecaller.truepay.app.ui.payments.views.c.d)localObject1).g(paramh);
  }
  
  public final void a(b paramb)
  {
    k.b(paramb, "d");
    a.e.a(paramb);
  }
  
  public final void a(Throwable paramThrowable)
  {
    k.b(paramThrowable, "e");
    paramThrowable = b;
    Object localObject = (com.truecaller.truepay.app.ui.payments.views.c.d)a.af_();
    if (localObject == null) {
      return;
    }
    a = localObject;
    paramThrowable = a;
    localObject = c;
    q.a(paramThrowable, (p)localObject, "failure");
    paramThrowable = (com.truecaller.truepay.app.ui.payments.views.c.d)b.a;
    paramThrowable.c(false);
    n localn = a.j;
    int i = R.string.bill_pay_error_message;
    localObject = new Object[0];
    localObject = localn.a(i, (Object[])localObject);
    paramThrowable.g((String)localObject);
    paramThrowable = new java/lang/AssertionError;
    paramThrowable.<init>("bill fetch error");
    com.truecaller.log.d.a((Throwable)paramThrowable);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.q.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */