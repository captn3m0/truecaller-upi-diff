package com.truecaller.truepay.app.ui.payments.c;

import dagger.a.d;
import javax.inject.Provider;

public final class u
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private u(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static u a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    u localu = new com/truecaller/truepay/app/ui/payments/c/u;
    localu.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localu;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */