package com.truecaller.truepay.app.ui.payments.c;

import c.d.f;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.featuretoggles.b;
import com.truecaller.truepay.R.color;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.TcPaySDKListener;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.utils.ae;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.ax;
import com.truecaller.truepay.app.utils.g;
import com.truecaller.truepay.data.api.d;
import com.truecaller.utils.n;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

public final class ag
  extends ba
  implements af.b
{
  private p c;
  private boolean d;
  private String e;
  private final DecimalFormat f;
  private final f g;
  private final f h;
  private final com.truecaller.truepay.data.e.e i;
  private final com.truecaller.truepay.app.utils.a j;
  private final com.truecaller.truepay.data.e.e k;
  private final com.truecaller.truepay.data.e.a l;
  private final com.truecaller.truepay.data.e.a m;
  private final n n;
  private final com.truecaller.utils.l o;
  private final ax p;
  private final au q;
  private final g r;
  private final com.truecaller.featuretoggles.e s;
  private final d t;
  
  public ag(f paramf1, f paramf2, com.truecaller.truepay.data.e.e parame1, com.truecaller.truepay.app.utils.a parama, com.truecaller.truepay.data.e.e parame2, com.truecaller.truepay.data.e.a parama1, com.truecaller.truepay.data.e.a parama2, n paramn, com.truecaller.utils.l paraml, ax paramax, au paramau, g paramg, com.truecaller.featuretoggles.e parame, d paramd)
  {
    super(paramf2);
    g = paramf1;
    h = paramf2;
    i = parame1;
    j = parama;
    k = parame2;
    l = parama1;
    m = parama2;
    n = paramn;
    o = paraml;
    p = paramax;
    q = paramau;
    r = paramg;
    s = parame;
    t = paramd;
    e = "utilities";
    paramf1 = new java/text/DecimalFormat;
    paramf1.<init>("#,###.##");
    f = paramf1;
  }
  
  private final void a(p paramp)
  {
    Object localObject1 = paramp.a();
    if (localObject1 != null)
    {
      String str = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject1).d();
      if (str == null) {
        str = "unknown";
      }
      int i1 = str.hashCode();
      int i2 = -1867169789;
      boolean bool;
      if (i1 != i2)
      {
        i2 = -1086574198;
        if (i1 != i2)
        {
          i2 = -682587753;
          if (i1 != i2)
          {
            i2 = -284840886;
            if (i1 == i2)
            {
              localObject2 = "unknown";
              bool = str.equals(localObject2);
              if (bool) {
                d(paramp);
              }
            }
          }
          else
          {
            localObject2 = "pending";
            bool = str.equals(localObject2);
            if (bool) {
              e(paramp);
            }
          }
        }
        else
        {
          localObject2 = "failure";
          bool = str.equals(localObject2);
          if (bool) {
            f(paramp);
          }
        }
      }
      else
      {
        localObject2 = "success";
        bool = str.equals(localObject2);
        if (bool) {
          g(paramp);
        }
      }
      paramp = paramp.a();
      k.a(paramp, "txn.payResponseDO");
      paramp = paramp.h();
      Object localObject2 = Truepay.getInstance();
      if (localObject2 != null)
      {
        localObject2 = ((Truepay)localObject2).getAnalyticLoggerHelper();
        if (localObject2 != null)
        {
          localObject1 = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject1).e();
          com.truecaller.truepay.data.e.a locala = m;
          ((com.truecaller.truepay.data.b.a)localObject2).a(str, (String)localObject1, locala, paramp);
          return;
        }
      }
      return;
    }
  }
  
  private final void a(String paramString)
  {
    Object localObject = c;
    if (localObject != null)
    {
      int i1 = paramString.hashCode();
      boolean bool;
      String str;
      switch (i1)
      {
      default: 
        break;
      case 1661903715: 
        localObject = "action.page.need_help";
        bool = paramString.equals(localObject);
        if (!bool) {
          break label435;
        }
        paramString = k.a();
        if (paramString != null)
        {
          localObject = (af.c)b;
          if (localObject != null)
          {
            k.a(paramString, "faqUrl");
            ((af.c)localObject).c(paramString);
            return;
          }
        }
        return;
      case 970112032: 
        localObject = "action.share_receipt";
        bool = paramString.equals(localObject);
        if (!bool) {
          break label435;
        }
        e();
        return;
      case 872695663: 
        str = "action.page.forgot_upi_pin";
        bool = paramString.equals(str);
        if (!bool) {
          break label435;
        }
        break;
      case -399989823: 
        str = "action.check_status";
        bool = paramString.equals(str);
        if (!bool) {
          break label435;
        }
        paramString = ((p)localObject).f();
        localObject = i.a();
        a(paramString, (String)localObject);
        return;
      case -721197651: 
        str = "action.page.reset_upi_pin";
        bool = paramString.equals(str);
        if (!bool) {
          break label435;
        }
        paramString = (af.c)b;
        if (paramString != null)
        {
          localObject = ((p)localObject).g();
          str = "it.account";
          k.a(localObject, str);
          paramString.a((com.truecaller.truepay.data.api.model.a)localObject);
        }
        return;
      case -875490522: 
        localObject = "action.page.home";
        bool = paramString.equals(localObject);
        if (!bool) {
          break label435;
        }
        paramString = (af.c)b;
        if (paramString != null) {
          paramString.h();
        }
        return;
      case -1361457855: 
        str = "action.page.retry";
        bool = paramString.equals(str);
        if (!bool) {
          break label435;
        }
        bool = c((p)localObject);
        if (bool)
        {
          paramString = (af.c)b;
          if (paramString != null) {
            paramString.h();
          }
          return;
        }
        bool = true;
        com.truecaller.truepay.app.ui.payments.a.a = bool;
        paramString = (af.c)b;
        if (paramString != null)
        {
          paramString.g();
          return;
        }
        return;
      case -1912660920: 
        str = "action.page.create_upi_pin";
        bool = paramString.equals(str);
        if (!bool) {
          break label435;
        }
      }
      paramString = (af.c)b;
      if (paramString != null)
      {
        localObject = ((p)localObject).g();
        str = "it.account";
        k.a(localObject, str);
        paramString.b((com.truecaller.truepay.data.api.model.a)localObject);
      }
      return;
      label435:
      paramString = (af.c)b;
      if (paramString != null)
      {
        paramString.h();
        return;
      }
      return;
    }
  }
  
  private final void a(String paramString1, String paramString2)
  {
    Object localObject = paramString1;
    localObject = (CharSequence)paramString1;
    int i1 = 0;
    if (localObject != null)
    {
      bool = c.n.m.a((CharSequence)localObject);
      if (!bool)
      {
        bool = false;
        localObject = null;
        break label39;
      }
    }
    boolean bool = true;
    label39:
    if (!bool)
    {
      localObject = paramString2;
      localObject = (CharSequence)paramString2;
      if (localObject != null)
      {
        bool = c.n.m.a((CharSequence)localObject);
        if (!bool) {}
      }
      else
      {
        i1 = 1;
      }
      if (i1 == 0)
      {
        localObject = new com/truecaller/truepay/app/ui/payments/c/ag$b;
        i1 = 0;
        ((ag.b)localObject).<init>(this, paramString1, paramString2, null);
        localObject = (c.g.a.m)localObject;
        int i2 = 3;
        kotlinx.coroutines.e.b(this, null, (c.g.a.m)localObject, i2);
      }
    }
  }
  
  private final void b(p paramp)
  {
    paramp = paramp.a();
    if (paramp != null)
    {
      Object localObject1 = paramp.g();
      int i1;
      if (localObject1 != null)
      {
        i1 = ((List)localObject1).size();
        localObject1 = Integer.valueOf(i1);
      }
      else
      {
        i1 = 0;
        localObject1 = null;
      }
      if (localObject1 != null)
      {
        i2 = ((Integer)localObject1).intValue();
        if (i2 == 0)
        {
          paramp = (af.c)b;
          if (paramp != null) {
            paramp.d();
          }
          return;
        }
      }
      int i2 = 0;
      String str1 = null;
      int i3 = 1;
      if (localObject1 != null)
      {
        int i4 = ((Integer)localObject1).intValue();
        int i5 = 2;
        if (i4 == i5)
        {
          localObject1 = (af.c)b;
          if (localObject1 != null)
          {
            Object localObject2 = paramp.g().get(i3);
            k.a(localObject2, "it.actionData[1]");
            localObject2 = ((com.truecaller.truepay.app.ui.history.models.a)localObject2).a();
            String str2 = "it.actionData[1].title";
            k.a(localObject2, str2);
            paramp = paramp.g().get(0);
            k.a(paramp, "it.actionData[0]");
            paramp = ((com.truecaller.truepay.app.ui.history.models.a)paramp).a();
            str1 = "it.actionData[0].title";
            k.a(paramp, str1);
            ((af.c)localObject1).a((String)localObject2, paramp);
          }
          return;
        }
      }
      if (localObject1 != null)
      {
        i1 = ((Integer)localObject1).intValue();
        if (i1 == i3)
        {
          localObject1 = (af.c)b;
          if (localObject1 != null)
          {
            paramp = paramp.g().get(0);
            k.a(paramp, "it.actionData[0]");
            paramp = ((com.truecaller.truepay.app.ui.history.models.a)paramp).a();
            str1 = "it.actionData[0].title";
            k.a(paramp, str1);
            ((af.c)localObject1).b(paramp);
          }
          return;
        }
      }
      paramp = (af.c)b;
      if (paramp != null)
      {
        paramp.d();
        return;
      }
      return;
    }
  }
  
  private static boolean c(p paramp)
  {
    String str1 = "booking";
    String str2 = paramp.n();
    boolean bool1 = k.a(str1, str2);
    if (bool1)
    {
      str1 = "redbus";
      paramp = paramp.B();
      boolean bool2 = k.a(str1, paramp);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  private final void d(p paramp)
  {
    boolean bool = c(paramp);
    int i1 = 0;
    Object localObject1 = null;
    int i2;
    Object localObject2;
    if (bool)
    {
      paramp = n;
      i2 = R.string.confirmation_recharge_pending_title;
      localObject2 = new Object[0];
      paramp = paramp.a(i2, (Object[])localObject2);
    }
    else
    {
      paramp = n;
      i2 = R.string.confirmation_bus_booking_pending_title;
      localObject2 = new Object[0];
      paramp = paramp.a(i2, (Object[])localObject2);
    }
    k.a(paramp, "if (isBusBooking(txn))\n …us_booking_pending_title)");
    Object localObject3 = (af.c)b;
    int i3;
    if (localObject3 != null)
    {
      localObject2 = n;
      i3 = R.string.confirmation_recharge_pending_sub_title;
      Object[] arrayOfObject = new Object[0];
      localObject2 = ((n)localObject2).a(i3, arrayOfObject);
      k.a(localObject2, "resourceProvider.getStri…charge_pending_sub_title)");
      Object localObject4 = n;
      int i4 = R.string.confirmation_debited_from_failure;
      localObject1 = new Object[0];
      localObject1 = ((n)localObject4).a(i4, (Object[])localObject1);
      localObject4 = "resourceProvider.getStri…ion_debited_from_failure)";
      k.a(localObject1, (String)localObject4);
      ((af.c)localObject3).a(paramp, (String)localObject2, (String)localObject1);
    }
    paramp = (af.c)b;
    int i5;
    if (paramp != null)
    {
      localObject1 = r;
      i2 = R.color.orange_color;
      i1 = ((g)localObject1).a(i2);
      i2 = R.drawable.rounded_yellow_border;
      localObject2 = r;
      i3 = R.color.white;
      i5 = ((g)localObject2).a(i3);
      i3 = R.drawable.yellow_background;
      paramp.a(i1, i2, i5, i3);
    }
    paramp = (af.c)b;
    if (paramp != null)
    {
      localObject1 = r;
      i2 = R.color.orange_color;
      i1 = ((g)localObject1).a(i2);
      localObject3 = r;
      i5 = R.color.orange_color;
      i2 = ((g)localObject3).a(i5);
      paramp.a(i1, i2);
    }
    paramp = (af.c)b;
    if (paramp != null) {
      paramp.k();
    }
    paramp = (af.c)b;
    if (paramp != null)
    {
      paramp.f("lottie_pending.json");
      return;
    }
  }
  
  private final void e()
  {
    af.c localc = (af.c)b;
    if (localc != null)
    {
      localc.j();
      return;
    }
  }
  
  private final void e(p paramp)
  {
    boolean bool1 = c(paramp);
    boolean bool2 = paramp.p();
    int i3 = 0;
    Object localObject1 = null;
    Object localObject2;
    Object localObject3;
    Object localObject4;
    int i5;
    if (bool2)
    {
      localObject2 = (af.c)b;
      if (localObject2 != null)
      {
        localObject3 = paramp.a();
        k.a(localObject3, "txn.payResponseDO");
        localObject3 = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject3).i();
        k.a(localObject3, "txn.payResponseDO.title");
        paramp = paramp.a();
        k.a(paramp, "txn.payResponseDO");
        paramp = paramp.f();
        k.a(paramp, "txn.payResponseDO.message");
        localObject4 = n;
        int i4 = R.string.confirmation_debited_from;
        localObject1 = new Object[0];
        localObject1 = ((n)localObject4).a(i4, (Object[])localObject1);
        localObject4 = "resourceProvider.getStri…onfirmation_debited_from)";
        k.a(localObject1, (String)localObject4);
        ((af.c)localObject2).a((String)localObject3, paramp, (String)localObject1);
      }
    }
    else
    {
      if (bool1)
      {
        localObject3 = n;
        i5 = R.string.bus_booking_processing;
        localObject5 = new Object[0];
        localObject3 = ((n)localObject3).a(i5, (Object[])localObject5);
      }
      else
      {
        localObject3 = n;
        i5 = R.string.recharge_processing;
        localObject5 = new Object[0];
        localObject3 = ((n)localObject3).a(i5, (Object[])localObject5);
      }
      localObject4 = "if (isRedBusBooking)\n   …ring.recharge_processing)";
      k.a(localObject3, (String)localObject4);
      if (bool1)
      {
        localObject2 = n;
        i5 = R.string.bus_booking_processing_sub_header;
        localObject5 = new Object[0];
        localObject2 = ((n)localObject2).a(i5, (Object[])localObject5);
      }
      else
      {
        localObject2 = n;
        i5 = R.string.recharge_processing_sub_header;
        localObject5 = new Object[0];
        localObject2 = ((n)localObject2).a(i5, (Object[])localObject5);
      }
      k.a(localObject2, "if (isRedBusBooking)\n   …ge_processing_sub_header)");
      localObject4 = "success";
      paramp = paramp.a();
      Object localObject5 = "txn.payResponseDO";
      k.a(paramp, (String)localObject5);
      paramp = paramp.d();
      boolean bool3 = k.a(localObject4, paramp);
      if (bool3)
      {
        paramp = n;
        i5 = R.string.confirmation_debited_from;
        localObject1 = new Object[0];
        paramp = paramp.a(i5, (Object[])localObject1);
      }
      else
      {
        paramp = n;
        i5 = R.string.confirmation_debited_from_failure;
        localObject1 = new Object[0];
        paramp = paramp.a(i5, (Object[])localObject1);
      }
      k.a(paramp, "if (Constants.SUCCESS ==…ion_debited_from_failure)");
      localObject1 = (af.c)b;
      if (localObject1 != null) {
        ((af.c)localObject1).a((String)localObject3, (String)localObject2, paramp);
      }
    }
    paramp = (af.c)b;
    int i2;
    int i1;
    if (paramp != null)
    {
      localObject2 = r;
      i2 = R.color.orange_color;
      i1 = ((g)localObject2).a(i2);
      i2 = R.drawable.rounded_yellow_border;
      localObject1 = r;
      i5 = R.color.white;
      i3 = ((g)localObject1).a(i5);
      i5 = R.drawable.yellow_background;
      paramp.a(i1, i2, i3, i5);
    }
    paramp = (af.c)b;
    if (paramp != null)
    {
      localObject2 = r;
      i2 = R.color.orange_color;
      i1 = ((g)localObject2).a(i2);
      localObject3 = r;
      i3 = R.color.orange_color;
      i2 = ((g)localObject3).a(i3);
      paramp.a(i1, i2);
    }
    paramp = (af.c)b;
    if (paramp != null) {
      paramp.k();
    }
    paramp = (af.c)b;
    if (paramp != null)
    {
      paramp.f("lottie_pending.json");
      return;
    }
  }
  
  private final void f(p paramp)
  {
    boolean bool = paramp.p();
    int i2 = 0;
    Object localObject1 = null;
    Object localObject2;
    Object localObject3;
    Object localObject4;
    int i3;
    int i4;
    if (bool)
    {
      localObject2 = (af.c)b;
      if (localObject2 != null)
      {
        localObject3 = paramp.a();
        k.a(localObject3, "txn.payResponseDO");
        localObject3 = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject3).i();
        k.a(localObject3, "txn.payResponseDO.title");
        paramp = paramp.a();
        k.a(paramp, "txn.payResponseDO");
        paramp = paramp.f();
        k.a(paramp, "txn.payResponseDO.message");
        localObject4 = n;
        i3 = R.string.confirmation_debited_from;
        localObject1 = new Object[0];
        localObject1 = ((n)localObject4).a(i3, (Object[])localObject1);
        localObject4 = "resourceProvider.getStri…onfirmation_debited_from)";
        k.a(localObject1, (String)localObject4);
        ((af.c)localObject2).a((String)localObject3, paramp, (String)localObject1);
      }
    }
    else
    {
      localObject2 = (af.c)b;
      if (localObject2 != null)
      {
        localObject3 = n;
        i4 = R.string.confirmation_payment_failed;
        Object[] arrayOfObject = new Object[0];
        localObject3 = ((n)localObject3).a(i4, arrayOfObject);
        k.a(localObject3, "resourceProvider.getStri…firmation_payment_failed)");
        paramp = paramp.a();
        k.a(paramp, "txn.payResponseDO");
        paramp = paramp.f();
        k.a(paramp, "txn.payResponseDO.message");
        localObject4 = n;
        i3 = R.string.confirmation_debited_from_failure;
        localObject1 = new Object[0];
        localObject1 = ((n)localObject4).a(i3, (Object[])localObject1);
        localObject4 = "resourceProvider.getStri…ion_debited_from_failure)";
        k.a(localObject1, (String)localObject4);
        ((af.c)localObject2).a((String)localObject3, paramp, (String)localObject1);
      }
    }
    paramp = (af.c)b;
    int i1;
    int i5;
    if (paramp != null)
    {
      localObject2 = r;
      i2 = R.color.red_color;
      i1 = ((g)localObject2).a(i2);
      i2 = R.drawable.rounded_red_border;
      localObject3 = r;
      i4 = R.color.white;
      i5 = ((g)localObject3).a(i4);
      i4 = R.drawable.red_background;
      paramp.a(i1, i2, i5, i4);
    }
    paramp = (af.c)b;
    if (paramp != null)
    {
      localObject2 = r;
      i2 = R.color.red_color;
      i1 = ((g)localObject2).a(i2);
      localObject1 = r;
      i5 = R.color.red_color;
      i2 = ((g)localObject1).a(i5);
      paramp.a(i1, i2);
    }
    paramp = (af.c)b;
    if (paramp != null) {
      paramp.l();
    }
    paramp = (af.c)b;
    if (paramp != null)
    {
      paramp.f("lottie_failed.json");
      return;
    }
  }
  
  private final void g(p paramp)
  {
    boolean bool = paramp.p();
    if (bool)
    {
      bool = c(paramp);
      int i1 = 0;
      Object localObject1 = null;
      Object localObject3;
      if (bool)
      {
        localObject2 = paramp.a();
        localObject3 = "txn.payResponseDO";
        k.a(localObject2, (String)localObject3);
        localObject2 = (CharSequence)((com.truecaller.truepay.app.ui.transaction.b.l)localObject2).k();
        if (localObject2 != null)
        {
          bool = c.n.m.a((CharSequence)localObject2);
          if (!bool)
          {
            bool = false;
            localObject2 = null;
            break label78;
          }
        }
        bool = true;
        label78:
        if (!bool)
        {
          localObject2 = (af.c)b;
          if (localObject2 != null)
          {
            localObject1 = paramp.a();
            k.a(localObject1, "txn.payResponseDO");
            localObject1 = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject1).k();
            paramp = paramp.j();
            localObject3 = "txn.referenceId";
            k.a(paramp, (String)localObject3);
            ((af.c)localObject2).b((String)localObject1, paramp);
          }
          return;
        }
      }
      Object localObject2 = (af.c)b;
      Object localObject4;
      if (localObject2 != null)
      {
        localObject3 = paramp.a();
        k.a(localObject3, "txn.payResponseDO");
        localObject3 = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject3).i();
        k.a(localObject3, "txn.payResponseDO.title");
        localObject4 = paramp.a();
        k.a(localObject4, "txn.payResponseDO");
        localObject4 = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject4).f();
        k.a(localObject4, "txn.payResponseDO.message");
        Object localObject5 = n;
        int i2 = R.string.confirmation_debited_from;
        localObject1 = new Object[0];
        localObject1 = ((n)localObject5).a(i2, (Object[])localObject1);
        localObject5 = "resourceProvider.getStri…onfirmation_debited_from)";
        k.a(localObject1, (String)localObject5);
        ((af.c)localObject2).a((String)localObject3, (String)localObject4, (String)localObject1);
      }
      localObject2 = (af.c)b;
      if (localObject2 != null) {
        ((af.c)localObject2).m();
      }
      localObject2 = (af.c)b;
      if (localObject2 != null) {
        ((af.c)localObject2).e();
      }
      localObject2 = (af.c)b;
      int i3;
      int i5;
      if (localObject2 != null)
      {
        localObject1 = r;
        i3 = R.color.light_green_color;
        i1 = ((g)localObject1).a(i3);
        i3 = R.drawable.rounded_green_border;
        localObject4 = r;
        int i4 = R.color.white;
        i5 = ((g)localObject4).a(i4);
        i4 = R.drawable.green_background;
        ((af.c)localObject2).a(i1, i3, i5, i4);
      }
      localObject2 = (af.c)b;
      if (localObject2 != null)
      {
        localObject1 = r;
        i3 = R.color.green_color;
        i1 = ((g)localObject1).a(i3);
        localObject3 = r;
        i5 = R.color.light_green_color;
        i3 = ((g)localObject3).a(i5);
        ((af.c)localObject2).a(i1, i3);
      }
      localObject2 = (af.c)b;
      if (localObject2 != null)
      {
        localObject1 = "lottie_success.json";
        ((af.c)localObject2).f((String)localObject1);
      }
      localObject2 = "google_play";
      localObject1 = paramp.n();
      bool = k.a(localObject2, localObject1);
      if (bool)
      {
        localObject2 = (af.c)b;
        if (localObject2 != null)
        {
          localObject1 = paramp.a();
          k.a(localObject1, "txn.payResponseDO");
          localObject1 = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject1).l();
          localObject3 = "txn.payResponseDO.email";
          k.a(localObject1, (String)localObject3);
          ((af.c)localObject2).g((String)localObject1);
          paramp = paramp.a();
          localObject1 = "txn.payResponseDO";
          k.a(paramp, (String)localObject1);
          paramp = paramp.c();
          if (paramp != null)
          {
            ((af.c)localObject2).h(paramp);
            return;
          }
          return;
        }
      }
      return;
    }
    e(paramp);
  }
  
  private final void h(p paramp)
  {
    ag localag = this;
    a(paramp);
    Object localObject = Truepay.getInstance();
    k.a(localObject, "Truepay.getInstance()");
    com.truecaller.truepay.data.b.a locala = ((Truepay)localObject).getAnalyticLoggerHelper();
    localObject = paramp.a();
    k.a(localObject, "txn.payResponseDO");
    String str1 = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject).d();
    localObject = paramp.a();
    k.a(localObject, "txn.payResponseDO");
    String str2 = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject).a();
    String str3 = paramp.c();
    String str4 = paramp.f();
    localObject = paramp.g();
    k.a(localObject, "txn.account");
    localObject = ((com.truecaller.truepay.data.api.model.a)localObject).l();
    k.a(localObject, "txn.account.bank");
    String str5 = ((com.truecaller.truepay.data.d.a)localObject).b();
    localObject = paramp.a();
    k.a(localObject, "txn.payResponseDO");
    String str6 = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject).b();
    localObject = paramp.a();
    String str7 = "txn.payResponseDO";
    k.a(localObject, str7);
    String str8 = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject).h();
    String str9 = paramp.n();
    String str10 = paramp.B();
    boolean bool1 = paramp.u();
    boolean bool2 = paramp.A();
    localObject = e;
    locala.a(str1, str2, str3, str4, str5, str6, str8, str9, str10, bool1, false, bool2, (String)localObject);
    b(paramp);
    localObject = (af.c)b;
    if (localObject != null) {
      ((af.c)localObject).b();
    }
    localObject = (af.c)b;
    if (localObject != null)
    {
      ((af.c)localObject).a(true);
      return;
    }
  }
  
  public final void a()
  {
    boolean bool = d;
    af.c localc;
    if (bool)
    {
      localc = (af.c)b;
      if (localc != null) {
        localc.e();
      }
      bool = false;
      localc = null;
    }
    else
    {
      localc = (af.c)b;
      if (localc != null) {
        localc.f();
      }
      bool = true;
    }
    d = bool;
  }
  
  public final void a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 != null)
    {
      localObject1 = ((p)localObject1).a();
      if (localObject1 != null)
      {
        localObject1 = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject1).g();
        break label28;
      }
    }
    localObject1 = null;
    label28:
    if (localObject1 != null)
    {
      Object localObject2 = ((List)localObject1).get(paramInt);
      if (localObject2 != null)
      {
        Object localObject3 = ((List)localObject1).get(paramInt);
        k.a(localObject3, "actionData[actionButtonIndex]");
        localObject3 = ((com.truecaller.truepay.app.ui.history.models.a)localObject3).b();
        k.a(localObject3, "actionData[actionButtonIndex].type");
        a((String)localObject3);
        return;
      }
    }
    a("action.page.home");
  }
  
  public final void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    Object localObject1 = "grantResults";
    k.b(paramArrayOfInt, (String)localObject1);
    int i1 = 1005;
    if (paramInt == i1)
    {
      Object localObject2 = o;
      localObject1 = new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" };
      paramInt = ((com.truecaller.utils.l)localObject2).a(paramArrayOfString, paramArrayOfInt, (String[])localObject1);
      if (paramInt != 0)
      {
        localObject2 = c;
        if (localObject2 != null)
        {
          paramArrayOfString = new java/util/Date;
          paramArrayOfString.<init>();
          paramArrayOfString = ax.a(paramArrayOfString);
          ((p)localObject2).g(paramArrayOfString);
          paramArrayOfString = j.d();
          paramArrayOfInt = (af.c)b;
          if (paramArrayOfInt != null)
          {
            k.a(paramArrayOfString, "bankRegisteredName");
            localObject1 = q;
            paramArrayOfInt.a((p)localObject2, paramArrayOfString, (au)localObject1);
            return;
          }
        }
        return;
      }
      localObject2 = (af.c)b;
      if (localObject2 != null)
      {
        paramArrayOfString = n;
        int i2 = R.string.external_directory_permission_denied;
        localObject1 = new Object[0];
        paramArrayOfString = paramArrayOfString.a(i2, (Object[])localObject1);
        k.a(paramArrayOfString, "resourceProvider.getStri…ectory_permission_denied)");
        ((af.c)localObject2).e(paramArrayOfString);
        return;
      }
    }
  }
  
  public final void a(p paramp, String paramString)
  {
    k.b(paramp, "txnModel");
    k.b(paramString, "rechargeContext");
    c = paramp;
    Object localObject1 = "pay_via_other";
    Object localObject2 = paramp.g();
    if (localObject2 != null)
    {
      localObject2 = ((com.truecaller.truepay.data.api.model.a)localObject2).j();
    }
    else
    {
      i1 = 0;
      localObject2 = null;
    }
    boolean bool2 = k.a(localObject1, localObject2);
    Object localObject3;
    if (bool2)
    {
      localObject1 = paramp.g();
      if (localObject1 != null)
      {
        localObject1 = ((com.truecaller.truepay.data.api.model.a)localObject1).l();
        if (localObject1 != null)
        {
          localObject1 = ((com.truecaller.truepay.data.d.a)localObject1).b();
          localObject3 = localObject1;
          break label121;
        }
      }
      localObject3 = null;
    }
    else
    {
      localObject1 = paramp.g();
      if (localObject1 != null)
      {
        localObject1 = ae.a((com.truecaller.truepay.data.api.model.a)localObject1);
        localObject3 = localObject1;
      }
      else
      {
        localObject3 = null;
      }
    }
    label121:
    localObject1 = n;
    int i1 = R.string.amount_string_transaction;
    boolean bool4 = true;
    Object localObject4 = new Object[bool4];
    Object localObject5 = f;
    Object localObject6 = paramp.d();
    if (localObject6 != null) {
      localObject6 = c.n.m.a((String)localObject6);
    } else {
      localObject6 = null;
    }
    localObject5 = ((DecimalFormat)localObject5).format(localObject6);
    localObject4[0] = localObject5;
    String str = ((n)localObject1).a(i1, (Object[])localObject4);
    k.a(str, "resourceProvider.getStri…DoubleOrNull())\n        )");
    localObject1 = paramp.g();
    if (localObject1 != null)
    {
      localObject1 = ((com.truecaller.truepay.data.api.model.a)localObject1).l();
      if (localObject1 != null)
      {
        localObject1 = ((com.truecaller.truepay.data.d.a)localObject1).d();
        localObject7 = localObject1;
        break label238;
      }
    }
    Object localObject7 = null;
    label238:
    localObject1 = localObject3;
    localObject1 = (CharSequence)localObject3;
    if (localObject1 != null)
    {
      bool2 = c.n.m.a((CharSequence)localObject1);
      if (!bool2)
      {
        bool2 = false;
        localObject1 = null;
        break label276;
      }
    }
    bool2 = true;
    label276:
    if (!bool2)
    {
      localObject1 = localObject7;
      localObject1 = (CharSequence)localObject7;
      if (localObject1 != null)
      {
        bool2 = c.n.m.a((CharSequence)localObject1);
        if (!bool2)
        {
          bool2 = false;
          localObject1 = null;
          break label319;
        }
      }
      bool2 = true;
      label319:
      if (!bool2)
      {
        localObject1 = b;
        localObject4 = localObject1;
        localObject4 = (af.c)localObject1;
        if (localObject4 != null)
        {
          localObject5 = paramp.l();
          localObject1 = "txn.utilityOperatorName";
          k.a(localObject5, (String)localObject1);
          localObject6 = paramp.o();
          int i3 = R.drawable.ic_avatar_common;
          ((af.c)localObject4).a((String)localObject5, (String)localObject6, (String)localObject7, str, (String)localObject3, i3);
        }
      }
    }
    localObject1 = (CharSequence)paramp.m();
    if (localObject1 != null)
    {
      i2 = ((CharSequence)localObject1).length();
      if (i2 != 0)
      {
        i2 = 0;
        localObject1 = null;
        break label428;
      }
    }
    int i2 = 1;
    label428:
    if (i2 == 0)
    {
      localObject1 = (af.c)b;
      if (localObject1 != null)
      {
        localObject2 = paramp.m();
        localObject4 = "txn.utilityRechargeNumber";
        k.a(localObject2, (String)localObject4);
        ((af.c)localObject1).g((String)localObject2);
      }
    }
    else
    {
      localObject1 = (af.c)b;
      if (localObject1 != null) {
        ((af.c)localObject1).n();
      }
    }
    a(paramp);
    localObject1 = paramp.a();
    if (localObject1 != null)
    {
      localObject1 = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject1).b();
    }
    else
    {
      i2 = 0;
      localObject1 = null;
    }
    localObject2 = localObject1;
    localObject2 = (CharSequence)localObject1;
    if (localObject2 != null)
    {
      bool1 = c.n.m.a((CharSequence)localObject2);
      if (!bool1)
      {
        bool1 = false;
        localObject2 = null;
        break label560;
      }
    }
    boolean bool1 = true;
    label560:
    if (!bool1)
    {
      localObject2 = (af.c)b;
      if (localObject2 != null) {
        ((af.c)localObject2).a((String)localObject1);
      }
      localObject1 = (af.c)b;
      if (localObject1 != null) {
        ((af.c)localObject1).a(bool4);
      }
    }
    else
    {
      localObject1 = (af.c)b;
      if (localObject1 != null) {
        ((af.c)localObject1).a(false);
      }
    }
    localObject1 = "pending";
    localObject2 = paramp.a();
    localObject4 = "txnModel.payResponseDO";
    k.a(localObject2, (String)localObject4);
    localObject2 = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject2).d();
    boolean bool3 = k.a(localObject1, localObject2);
    if (!bool3)
    {
      localObject1 = "success";
      localObject2 = paramp.a();
      localObject4 = "txnModel.payResponseDO";
      k.a(localObject2, (String)localObject4);
      localObject2 = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject2).d();
      bool3 = k.a(localObject1, localObject2);
      if (!bool3)
      {
        b(paramp);
        break label1162;
      }
    }
    localObject1 = new com/truecaller/truepay/app/ui/history/models/a;
    ((com.truecaller.truepay.app.ui.history.models.a)localObject1).<init>();
    localObject2 = n;
    int i4 = R.string.action_home;
    localObject5 = new Object[0];
    localObject2 = ((n)localObject2).a(i4, (Object[])localObject5);
    ((com.truecaller.truepay.app.ui.history.models.a)localObject1).a((String)localObject2);
    ((com.truecaller.truepay.app.ui.history.models.a)localObject1).b("action.page.home");
    localObject2 = new com/truecaller/truepay/app/ui/history/models/a;
    ((com.truecaller.truepay.app.ui.history.models.a)localObject2).<init>();
    localObject4 = n;
    int i5 = R.string.check_status_confirmation;
    localObject6 = new Object[0];
    localObject4 = ((n)localObject4).a(i5, (Object[])localObject6);
    ((com.truecaller.truepay.app.ui.history.models.a)localObject2).a((String)localObject4);
    ((com.truecaller.truepay.app.ui.history.models.a)localObject2).b("action.check_status");
    i4 = 2;
    localObject4 = new com.truecaller.truepay.app.ui.history.models.a[i4];
    localObject4[0] = localObject1;
    localObject4[bool4] = localObject2;
    localObject1 = c.a.m.b((Object[])localObject4);
    localObject2 = paramp.a();
    if (localObject2 != null) {
      ((com.truecaller.truepay.app.ui.transaction.b.l)localObject2).a((List)localObject1);
    }
    localObject1 = (CharSequence)paramp.f();
    if (localObject1 != null)
    {
      bool3 = c.n.m.a((CharSequence)localObject1);
      if (!bool3)
      {
        bool3 = false;
        localObject1 = null;
        break label908;
      }
    }
    bool3 = true;
    label908:
    if (!bool3)
    {
      localObject1 = i;
      if (localObject1 != null)
      {
        localObject1 = ((com.truecaller.truepay.data.e.e)localObject1).a();
      }
      else
      {
        bool3 = false;
        localObject1 = null;
      }
      localObject1 = (CharSequence)localObject1;
      if (localObject1 != null)
      {
        bool3 = c.n.m.a((CharSequence)localObject1);
        if (!bool3)
        {
          bool3 = false;
          localObject1 = null;
          break label969;
        }
      }
      bool3 = true;
      label969:
      if (!bool3)
      {
        localObject1 = (af.c)b;
        if (localObject1 != null) {
          ((af.c)localObject1).c();
        }
        localObject1 = (af.c)b;
        if (localObject1 != null) {
          ((af.c)localObject1).a(false);
        }
        paramp = paramp.f();
        k.a(paramp, "txnModel.transactionId");
        localObject1 = i.a();
        localObject2 = paramp;
        localObject2 = (CharSequence)paramp;
        if (localObject2 != null)
        {
          bool1 = c.n.m.a((CharSequence)localObject2);
          if (!bool1)
          {
            bool1 = false;
            localObject2 = null;
            break label1072;
          }
        }
        bool1 = true;
        label1072:
        if (bool1) {
          break label1162;
        }
        localObject2 = localObject1;
        localObject2 = (CharSequence)localObject1;
        if (localObject2 != null)
        {
          bool1 = c.n.m.a((CharSequence)localObject2);
          if (!bool1) {
            bool4 = false;
          }
        }
        if (bool4) {
          break label1162;
        }
        localObject2 = new com/truecaller/truepay/app/ui/payments/c/ag$c;
        ((ag.c)localObject2).<init>(this, paramp, (String)localObject1, null);
        localObject2 = (c.g.a.m)localObject2;
        int i6 = 3;
        kotlinx.coroutines.e.b(this, null, (c.g.a.m)localObject2, i6);
        break label1162;
      }
    }
    paramp = "transaction id or user id should not be null";
    new String[1][0] = paramp;
    label1162:
    paramp = l;
    localObject1 = Boolean.TRUE;
    paramp.a((Boolean)localObject1);
    e = paramString;
    paramp = s.ab();
    boolean bool5 = paramp.a();
    if (bool5)
    {
      paramp = (af.c)b;
      if (paramp != null) {
        paramp.p();
      }
      return;
    }
    paramp = (af.c)b;
    if (paramp != null)
    {
      paramp.o();
      return;
    }
  }
  
  public final void b()
  {
    ax localax = p;
    Object localObject1 = c;
    if (localObject1 != null)
    {
      localObject1 = ((p)localObject1).a();
      if (localObject1 != null)
      {
        localObject1 = ((com.truecaller.truepay.app.ui.transaction.b.l)localObject1).b();
        break label33;
      }
    }
    localObject1 = null;
    label33:
    Object localObject2 = n;
    int i1 = R.string.copied_ref_num;
    Object[] arrayOfObject = new Object[0];
    localObject2 = ((n)localObject2).a(i1, arrayOfObject);
    localax.a((String)localObject1, (String)localObject2);
  }
  
  public final void c()
  {
    Truepay localTruepay = Truepay.getInstance();
    localTruepay.getAnalyticLoggerHelper().g("utilities");
    localTruepay.getListener().createShortcut(3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */