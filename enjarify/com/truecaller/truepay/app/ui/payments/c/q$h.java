package com.truecaller.truepay.app.ui.payments.c;

import c.g.b.k;
import c.g.b.v.c;
import c.n.m;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.payments.views.c.d;
import com.truecaller.truepay.app.ui.transaction.b.o;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.data.api.model.h;
import com.truecaller.utils.n;
import io.reactivex.a.b;

public final class q$h
  implements io.reactivex.q
{
  q$h(q paramq, v.c paramc, p paramp) {}
  
  private void a(h paramh)
  {
    k.b(paramh, "responseDO");
    Object localObject1 = b;
    Object localObject2 = (d)a.af_();
    if (localObject2 == null) {
      return;
    }
    a = localObject2;
    ((d)b.a).c(false);
    localObject1 = "success";
    localObject2 = paramh.a();
    boolean bool1 = true;
    boolean bool2 = m.a((String)localObject1, (String)localObject2, bool1);
    if (bool2)
    {
      localObject1 = paramh.c();
      k.a(localObject1, "responseDO.data");
      localObject1 = ((o)localObject1).a();
      c.h((String)localObject1);
      localObject1 = c;
      localObject2 = paramh.c();
      k.a(localObject2, "responseDO.data");
      localObject2 = ((o)localObject2).b();
      ((p)localObject1).i((String)localObject2);
      c.i();
      localObject1 = c;
      localObject2 = paramh.c();
      k.a(localObject2, "responseDO.data");
      localObject2 = ((o)localObject2).c();
      ((p)localObject1).q((String)localObject2);
      localObject1 = c;
      localObject2 = paramh.c();
      String str1 = "responseDO.data";
      k.a(localObject2, str1);
      localObject2 = ((o)localObject2).i();
      ((p)localObject1).t((String)localObject2);
      localObject1 = c.g();
      k.a(localObject1, "txnModel.account");
      localObject1 = ((com.truecaller.truepay.data.api.model.a)localObject1).j();
      localObject2 = "pay_via_other";
      bool2 = m.a((String)localObject1, (String)localObject2, bool1);
      if (bool2)
      {
        localObject1 = b.a;
        localObject2 = localObject1;
        localObject2 = (d)localObject1;
        String str2 = c.l();
        localObject1 = paramh.c();
        k.a(localObject1, "responseDO.data");
        str1 = ((o)localObject1).b();
        String str3 = c.d();
        localObject1 = paramh.c();
        k.a(localObject1, "responseDO.data");
        String str4 = ((o)localObject1).a();
        paramh = paramh.c();
        k.a(paramh, "responseDO.data");
        String str5 = ((o)paramh).c();
        ((d)localObject2).a(str2, str1, str3, str4, str5);
        return;
      }
      paramh = a;
      localObject1 = c;
      paramh.b((p)localObject1);
      return;
    }
    localObject1 = (d)b.a;
    paramh = paramh.b();
    ((d)localObject1).e(paramh);
  }
  
  public final void a(b paramb)
  {
    k.b(paramb, "d");
    a.e.a(paramb);
  }
  
  public final void a(Throwable paramThrowable)
  {
    k.b(paramThrowable, "e");
    paramThrowable = b;
    Object localObject = (d)a.af_();
    if (localObject == null) {
      return;
    }
    a = localObject;
    ((d)b.a).c(false);
    paramThrowable = (d)b.a;
    n localn = a.j;
    int i = R.string.server_error_message;
    localObject = new Object[0];
    localObject = localn.a(i, (Object[])localObject);
    paramThrowable.e((String)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.q.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */