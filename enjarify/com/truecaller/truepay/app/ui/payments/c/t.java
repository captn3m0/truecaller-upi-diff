package com.truecaller.truepay.app.ui.payments.c;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.truepay.app.ui.payments.b.a;
import kotlinx.coroutines.e;

public final class t
  extends ba
  implements b.a
{
  final com.truecaller.truepay.data.db.a c;
  private final f d;
  private final f e;
  
  public t(f paramf1, f paramf2, com.truecaller.truepay.data.db.a parama)
  {
    super(paramf1);
    d = paramf1;
    e = paramf2;
    c = parama;
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    k.b(parama, "utilityList");
    Object localObject = new com/truecaller/truepay/app/ui/payments/c/t$b;
    ((t.b)localObject).<init>(this, parama, null);
    localObject = (m)localObject;
    e.b(this, null, (m)localObject, 3);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "operatorName");
    Object localObject = new com/truecaller/truepay/app/ui/payments/c/t$a;
    ((t.a)localObject).<init>(this, paramString, null);
    localObject = (m)localObject;
    e.b(this, null, (m)localObject, 3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */