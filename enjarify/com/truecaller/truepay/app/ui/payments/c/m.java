package com.truecaller.truepay.app.ui.payments.c;

import android.content.Intent;
import android.os.Bundle;
import c.d.f;
import c.u;
import c.x;
import com.truecaller.ba;
import com.truecaller.log.UnmutedException.f;
import com.truecaller.log.d;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.transaction.b.l;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.utils.ae;
import com.truecaller.truepay.data.api.g;
import com.truecaller.truepay.data.api.h;
import com.truecaller.truepay.data.e.c;
import com.truecaller.utils.n;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class m
  extends ba
  implements l.a
{
  final f c;
  private p d;
  private final io.reactivex.a.a e;
  private String f;
  private final f g;
  private final com.truecaller.truepay.app.utils.a h;
  private final n i;
  private final com.truecaller.truepay.app.ui.npci.e j;
  private final c k;
  private final h l;
  private final g m;
  
  public m(f paramf1, f paramf2, com.truecaller.truepay.app.utils.a parama, n paramn, com.truecaller.truepay.app.ui.npci.e parame, c paramc, h paramh, g paramg)
  {
    super(paramf2);
    c = paramf1;
    g = paramf2;
    h = parama;
    i = paramn;
    j = parame;
    k = paramc;
    l = paramh;
    m = paramg;
    paramf1 = new com/truecaller/truepay/app/ui/transaction/b/p;
    paramf1.<init>();
    d = paramf1;
    paramf1 = new io/reactivex/a/a;
    paramf1.<init>();
    e = paramf1;
    f = "utilities";
  }
  
  public final void a()
  {
    d.d("utility");
    d.a("Remarks");
    Object localObject1 = k;
    Object localObject2 = "";
    localObject1 = ((c)localObject1).a("s#&bf2)^hn@1lp*n", (String)localObject2);
    Object localObject3 = localObject1;
    localObject3 = (CharSequence)localObject1;
    if (localObject3 != null)
    {
      bool = c.n.m.a((CharSequence)localObject3);
      if (!bool)
      {
        bool = false;
        localObject3 = null;
        break label70;
      }
    }
    boolean bool = true;
    label70:
    if (bool)
    {
      localObject1 = new com/truecaller/log/UnmutedException$f;
      ((UnmutedException.f)localObject1).<init>("initiator msisdn is not present");
      d.a((Throwable)localObject1);
      return;
    }
    localObject3 = new java/util/HashMap;
    ((HashMap)localObject3).<init>();
    localObject2 = localObject3;
    localObject2 = (Map)localObject3;
    ((Map)localObject2).put("initiator_msisdn", localObject1);
    String str = d.n();
    ((Map)localObject2).put("type", str);
    str = d.d();
    ((Map)localObject2).put("amount", str);
    str = d.B();
    ((Map)localObject2).put("operator", str);
    str = d.z();
    ((Map)localObject2).put("customer_field1", str);
    str = d.D();
    ((Map)localObject2).put("ref_id", str);
    str = d.E();
    ((Map)localObject2).put("product_type", str);
    localObject1 = new com/truecaller/truepay/app/ui/payments/c/m$f;
    ((m.f)localObject1).<init>(this, (HashMap)localObject3, null);
    localObject1 = (c.g.a.m)localObject1;
    kotlinx.coroutines.e.b(this, null, (c.g.a.m)localObject1, 3);
  }
  
  public final void a(int paramInt1, int paramInt2, Intent paramIntent)
  {
    int n = 5001;
    if (paramInt1 == n)
    {
      paramInt1 = -1;
      if (paramInt2 == paramInt1)
      {
        paramInt1 = 0;
        Object localObject1 = null;
        label329:
        int i1;
        if (paramIntent != null)
        {
          localObject2 = paramIntent.getExtras();
          if (localObject2 != null)
          {
            paramIntent = "response";
            localObject2 = ((Bundle)localObject2).getString(paramIntent);
            if (localObject2 != null)
            {
              paramIntent = new java/util/HashMap;
              paramIntent.<init>();
              localObject2 = (CharSequence)localObject2;
              Object localObject3 = "&";
              Object localObject4 = new c/n/k;
              ((c.n.k)localObject4).<init>((String)localObject3);
              localObject2 = ((c.n.k)localObject4).a((CharSequence)localObject2, 0).iterator();
              for (;;)
              {
                boolean bool = ((Iterator)localObject2).hasNext();
                if (!bool) {
                  break;
                }
                localObject3 = (CharSequence)((Iterator)localObject2).next();
                Object localObject5 = new c/n/k;
                ((c.n.k)localObject5).<init>("=");
                localObject3 = ((c.n.k)localObject5).a((CharSequence)localObject3, 0);
                localObject4 = paramIntent;
                localObject4 = (Map)paramIntent;
                localObject5 = ((List)localObject3).get(0);
                int i2 = 1;
                localObject3 = ((List)localObject3).get(i2);
                ((Map)localObject4).put(localObject5, localObject3);
              }
              localObject2 = new com/truecaller/truepay/app/ui/transaction/b/l;
              ((l)localObject2).<init>();
              localObject3 = (String)paramIntent.get("responseCode");
              ((l)localObject2).d((String)localObject3);
              localObject3 = paramIntent.get("Status");
              if (localObject3 == null) {
                localObject3 = (String)paramIntent.get("status");
              } else {
                localObject3 = (String)paramIntent.get("Status");
              }
              if (localObject3 != null) {
                if (localObject3 != null)
                {
                  localObject3 = ((String)localObject3).toLowerCase();
                  localObject4 = "(this as java.lang.String).toLowerCase()";
                  c.g.b.k.a(localObject3, (String)localObject4);
                  if (localObject3 != null) {
                    break label329;
                  }
                }
                else
                {
                  localObject1 = new c/u;
                  ((u)localObject1).<init>("null cannot be cast to non-null type java.lang.String");
                  throw ((Throwable)localObject1);
                }
              }
              localObject3 = "failure";
              ((l)localObject2).b((String)localObject3);
              localObject3 = (String)paramIntent.get("txnRef");
              ((l)localObject2).a((String)localObject3);
              localObject3 = "message";
              paramIntent = (String)paramIntent.get(localObject3);
              ((l)localObject2).c(paramIntent);
              paramIntent = d;
              paramIntent.a((l)localObject2);
              localObject2 = (l.b)b;
              if (localObject2 != null)
              {
                paramIntent = d;
                ((l.b)localObject2).a(paramIntent);
                localObject2 = x.a;
              }
              else
              {
                paramInt2 = 0;
                localObject2 = null;
              }
              if (localObject2 != null) {}
            }
            else
            {
              localObject2 = (l.b)b;
              if (localObject2 != null)
              {
                paramIntent = i;
                i1 = R.string.intent_recharge_unknown;
                localObject1 = new Object[0];
                localObject1 = paramIntent.a(i1, (Object[])localObject1);
                paramIntent = "resourceProvider.getStri….intent_recharge_unknown)";
                c.g.b.k.a(localObject1, paramIntent);
                ((l.b)localObject2).c((String)localObject1);
                localObject1 = x.a;
              }
            }
            return;
          }
        }
        Object localObject2 = (l.b)b;
        if (localObject2 != null)
        {
          paramIntent = i;
          i1 = R.string.intent_recharge_unknown;
          localObject1 = new Object[0];
          localObject1 = paramIntent.a(i1, (Object[])localObject1);
          c.g.b.k.a(localObject1, "resourceProvider.getStri….intent_recharge_unknown)");
          ((l.b)localObject2).c((String)localObject1);
          return;
        }
      }
    }
  }
  
  public final void a(p paramp, String paramString)
  {
    c.g.b.k.b(paramp, "txnModel");
    String str1 = "rechargeContext";
    c.g.b.k.b(paramString, str1);
    d = paramp;
    f = paramString;
    paramString = (l.b)b;
    if (paramString != null)
    {
      str1 = paramp.l();
      c.g.b.k.a(str1, "txnModel.utilityOperatorName");
      String str2 = paramp.d();
      c.g.b.k.a(str2, "txnModel.amount");
      String str3 = paramp.o();
      String str4 = "txnModel.utilityOperatorImageUrl";
      c.g.b.k.a(str3, str4);
      int n = R.drawable.ic_avatar_common;
      paramString.a(str1, str2, str3, n);
    }
    paramString = (l.b)b;
    if (paramString != null)
    {
      paramp = paramp.F();
      paramString.d(paramp);
    }
    paramp = h.c();
    if (paramp != null)
    {
      paramp = paramp.l();
      if (paramp != null)
      {
        paramp = h.c();
        c.g.b.k.a(paramp, "accountManager.primaryAccount");
        a(paramp);
        return;
      }
    }
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    c.g.b.k.b(parama, "selectedAccount");
    d.a(parama);
    Object localObject1 = parama.j();
    Object localObject2 = "pay_via_other";
    boolean bool = c.g.b.k.a(localObject1, localObject2);
    if (bool)
    {
      localObject1 = i;
      int n = R.string.pay_via_other;
      Object[] arrayOfObject = new Object[0];
      localObject1 = ((n)localObject1).a(n, arrayOfObject);
      localObject2 = "resourceProvider.getString(R.string.pay_via_other)";
      c.g.b.k.a(localObject1, (String)localObject2);
    }
    else
    {
      localObject1 = ae.a(parama);
    }
    localObject2 = (l.b)b;
    if (localObject2 != null)
    {
      ((l.b)localObject2).a((String)localObject1);
      parama = parama.l();
      c.g.b.k.a(parama, "selectedAccount.bank");
      parama = parama.d();
      c.g.b.k.a(parama, "selectedAccount.bank.bankSymbol");
      ((l.b)localObject2).b(parama);
      return;
    }
  }
  
  public final void y_()
  {
    super.y_();
    e.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */