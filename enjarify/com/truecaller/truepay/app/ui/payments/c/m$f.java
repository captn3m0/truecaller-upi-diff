package com.truecaller.truepay.app.ui.payments.c;

import c.d.c;
import c.d.f;
import c.g.b.z;
import c.o.b;
import c.x;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.transaction.b.o;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.data.api.model.h;
import com.truecaller.utils.n;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.e;
import kotlinx.coroutines.g;

final class m$f
  extends c.d.b.a.k
  implements c.g.a.m
{
  Object a;
  int b;
  private ag e;
  
  m$f(m paramm, HashMap paramHashMap, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    f localf = new com/truecaller/truepay/app/ui/payments/c/m$f;
    m localm = c;
    HashMap localHashMap = d;
    localf.<init>(localm, localHashMap, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localf;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = b;
    int k = 0;
    Object localObject2 = null;
    int m = 1;
    boolean bool1;
    Object localObject3;
    Object localObject4;
    Object localObject5;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label1025;
      }
      paramObject = e;
      localObject3 = m.a(c);
      if (localObject3 != null) {
        ((l.b)localObject3).c();
      }
      localObject3 = c;
      localObject4 = d;
      a = paramObject;
      b = m;
      paramObject = c;
      localObject5 = new com/truecaller/truepay/app/ui/payments/c/m$g;
      ((m.g)localObject5).<init>((m)localObject3, (HashMap)localObject4, null);
      localObject5 = (c.g.a.m)localObject5;
      paramObject = g.a((f)paramObject, (c.g.a.m)localObject5, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (h)paramObject;
    boolean bool2 = false;
    localObject1 = null;
    if (paramObject != null)
    {
      localObject3 = "success";
      localObject4 = ((h)paramObject).a();
      bool1 = c.g.b.k.a(localObject3, localObject4);
      label269:
      label331:
      label393:
      Object localObject6;
      if (bool1)
      {
        localObject3 = ((h)paramObject).c();
        localObject4 = "it.data";
        c.g.b.k.a(localObject3, (String)localObject4);
        localObject3 = (CharSequence)((o)localObject3).a();
        if (localObject3 != null)
        {
          bool1 = c.n.m.a((CharSequence)localObject3);
          if (!bool1)
          {
            bool1 = false;
            localObject3 = null;
            break label269;
          }
        }
        bool1 = true;
        if (!bool1)
        {
          localObject3 = ((h)paramObject).c();
          localObject4 = "it.data";
          c.g.b.k.a(localObject3, (String)localObject4);
          localObject3 = (CharSequence)((o)localObject3).b();
          if (localObject3 != null)
          {
            bool1 = c.n.m.a((CharSequence)localObject3);
            if (!bool1)
            {
              bool1 = false;
              localObject3 = null;
              break label331;
            }
          }
          bool1 = true;
          if (!bool1)
          {
            localObject3 = ((h)paramObject).c();
            localObject4 = "it.data";
            c.g.b.k.a(localObject3, (String)localObject4);
            localObject3 = (CharSequence)((o)localObject3).c();
            if (localObject3 != null)
            {
              bool1 = c.n.m.a((CharSequence)localObject3);
              if (!bool1)
              {
                bool1 = false;
                localObject3 = null;
                break label393;
              }
            }
            bool1 = true;
            if (!bool1)
            {
              localObject3 = m.b(c);
              localObject4 = ((h)paramObject).c();
              c.g.b.k.a(localObject4, "it.data");
              localObject4 = ((o)localObject4).a();
              ((p)localObject3).h((String)localObject4);
              localObject4 = ((h)paramObject).c();
              c.g.b.k.a(localObject4, "it.data");
              localObject4 = ((o)localObject4).b();
              ((p)localObject3).i((String)localObject4);
              ((p)localObject3).i();
              localObject4 = ((h)paramObject).c();
              localObject5 = "it.data";
              c.g.b.k.a(localObject4, (String)localObject5);
              localObject4 = ((o)localObject4).c();
              ((p)localObject3).q((String)localObject4);
              paramObject = ((h)paramObject).c();
              localObject4 = "it.data";
              c.g.b.k.a(paramObject, (String)localObject4);
              paramObject = ((o)paramObject).i();
              ((p)localObject3).t((String)paramObject);
              paramObject = m.b(c).g();
              c.g.b.k.a(paramObject, "txnModel.account");
              paramObject = ((com.truecaller.truepay.data.api.model.a)paramObject).j();
              localObject3 = "pay_via_other";
              boolean bool3 = c.g.b.k.a(paramObject, localObject3);
              int j = 3;
              if (bool3)
              {
                paramObject = z.a;
                int n = 5;
                localObject5 = new Object[n];
                String str1 = URLEncoder.encode(m.b(c).l(), "utf-8");
                localObject5[0] = str1;
                str1 = m.b(c).k();
                String str2 = "utf-8";
                str1 = URLEncoder.encode(str1, str2);
                localObject5[m] = str1;
                m = 2;
                str1 = m.b(c).d();
                localObject5[m] = str1;
                localObject6 = m.b(c).j();
                localObject5[j] = localObject6;
                j = 4;
                localObject6 = m.b(c).w();
                localObject5[j] = localObject6;
                localObject3 = Arrays.copyOf((Object[])localObject5, n);
                paramObject = String.format("upi://pay?pn=%s&pa=%s&am=%s&cu=INR&tr=%s&tid=%s&mc=5411", (Object[])localObject3);
                c.g.b.k.a(paramObject, "java.lang.String.format(format, *args)");
                localObject3 = m.a(c);
                if (localObject3 == null) {
                  break label935;
                }
                localObject2 = m.c(c);
                m = R.string.choose_desired_app;
                localObject4 = new Object[0];
                localObject2 = ((n)localObject2).a(m, (Object[])localObject4);
                localObject6 = "resourceProvider.getStri…tring.choose_desired_app)";
                c.g.b.k.a(localObject2, (String)localObject6);
                ((l.b)localObject3).a((String)paramObject, (String)localObject2);
                localObject2 = x.a;
                break label935;
              }
              paramObject = c;
              localObject6 = m.b((m)paramObject);
              c.g.b.k.b(localObject6, "txnModel");
              localObject4 = new com/truecaller/truepay/app/ui/payments/c/m$c;
              ((m.c)localObject4).<init>((m)paramObject, (p)localObject6, null);
              localObject4 = (c.g.a.m)localObject4;
              e.b((ag)paramObject, null, (c.g.a.m)localObject4, j);
              localObject2 = x.a;
              break label935;
            }
          }
        }
      }
      localObject3 = m.a(c);
      if (localObject3 != null)
      {
        paramObject = ((h)paramObject).b();
        if (paramObject == null)
        {
          paramObject = m.c(c);
          k = R.string.server_error_message;
          localObject6 = new Object[0];
          paramObject = ((n)paramObject).a(k, (Object[])localObject6);
          localObject2 = "resourceProvider.getStri…ing.server_error_message)";
          c.g.b.k.a(paramObject, (String)localObject2);
        }
        ((l.b)localObject3).c((String)paramObject);
        localObject2 = x.a;
      }
      label935:
      if (localObject2 != null) {}
    }
    else
    {
      paramObject = m.a(c);
      if (paramObject != null)
      {
        localObject3 = m.c(c);
        k = R.string.server_error_message;
        localObject1 = new Object[0];
        localObject1 = ((n)localObject3).a(k, (Object[])localObject1);
        localObject3 = "resourceProvider.getStri…ing.server_error_message)";
        c.g.b.k.a(localObject1, (String)localObject3);
        ((l.b)paramObject).c((String)localObject1);
        paramObject = x.a;
      }
    }
    paramObject = m.a(c);
    if (paramObject != null) {
      ((l.b)paramObject).d();
    }
    return x.a;
    label1025:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (f)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((f)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.m.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */