package com.truecaller.truepay.app.ui.payments.c;

import android.view.MenuItem;
import c.d.f;
import c.g.b.k;
import c.u;
import com.truecaller.ba;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.history.models.o;
import com.truecaller.truepay.app.utils.ae;
import com.truecaller.truepay.app.utils.z;
import com.truecaller.utils.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class j
  extends ba
  implements i.a
{
  private final List c;
  private String d;
  private final z e;
  private final com.truecaller.truepay.data.b.a f;
  private final com.truecaller.truepay.app.utils.a.a g;
  private final n h;
  private final f i;
  private final f j;
  
  public j(z paramz, com.truecaller.truepay.data.b.a parama, com.truecaller.truepay.app.utils.a.a parama1, n paramn, f paramf1, f paramf2)
  {
    super(paramf1);
    e = paramz;
    f = parama;
    g = parama1;
    h = paramn;
    i = paramf1;
    j = paramf2;
    paramz = new java/util/ArrayList;
    paramz.<init>();
    paramz = (List)paramz;
    c = paramz;
    d = "utilities";
  }
  
  public final void a(MenuItem paramMenuItem, String paramString)
  {
    k.b(paramMenuItem, "item");
    String str = "utilityType";
    k.b(paramString, str);
    int k = paramMenuItem.getItemId();
    int m = R.id.action_search;
    if (k == m)
    {
      paramMenuItem = f;
      str = d;
      paramMenuItem.f(paramString, str);
    }
  }
  
  public final void a(com.truecaller.truepay.app.ui.history.models.h paramh, com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    k.b(paramh, "item");
    k.b(parama, "utilityEntry");
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    Object localObject1 = localHashMap;
    localObject1 = (Map)localHashMap;
    Object localObject2 = paramh.a();
    k.a(localObject2, "item.utilityDO");
    localObject2 = ((o)localObject2).a();
    k.a(localObject2, "item.utilityDO.rechargeNumber");
    ((Map)localObject1).put("recharge_number", localObject2);
    localObject2 = paramh.c();
    Object localObject3 = "item.amount";
    k.a(localObject2, (String)localObject3);
    ((Map)localObject1).put("amount", localObject2);
    localObject1 = (Iterable)c;
    Object localObject4 = new java/util/ArrayList;
    ((ArrayList)localObject4).<init>();
    localObject4 = (Collection)localObject4;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = ((Iterator)localObject1).next();
      localObject3 = localObject2;
      localObject3 = (h)localObject2;
      boolean bool3 = localObject3 instanceof com.truecaller.truepay.app.ui.payments.models.a;
      if (bool3) {
        ((Collection)localObject4).add(localObject2);
      }
    }
    localObject4 = (Iterable)localObject4;
    localObject1 = new java/util/ArrayList;
    int k = c.a.m.a((Iterable)localObject4, 10);
    ((ArrayList)localObject1).<init>(k);
    localObject1 = (Collection)localObject1;
    localObject4 = ((Iterable)localObject4).iterator();
    boolean bool2;
    for (;;)
    {
      bool2 = ((Iterator)localObject4).hasNext();
      if (!bool2) {
        break label301;
      }
      localObject2 = (h)((Iterator)localObject4).next();
      if (localObject2 == null) {
        break;
      }
      localObject2 = (com.truecaller.truepay.app.ui.payments.models.a)localObject2;
      ((Collection)localObject1).add(localObject2);
    }
    paramh = new c/u;
    paramh.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.payments.models.BaseUtility");
    throw paramh;
    label301:
    localObject1 = ((Iterable)localObject1).iterator();
    do
    {
      bool4 = ((Iterator)localObject1).hasNext();
      if (!bool4) {
        break;
      }
      localObject4 = ((Iterator)localObject1).next();
      localObject2 = localObject4;
      localObject2 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject4).b();
      localObject3 = paramh.a();
      String str = "item.utilityDO";
      k.a(localObject3, str);
      localObject3 = ((o)localObject3).e();
      bool2 = k.a(localObject2, localObject3);
    } while (!bool2);
    break label400;
    boolean bool4 = false;
    localObject4 = null;
    label400:
    localObject4 = (com.truecaller.truepay.app.ui.payments.models.a)localObject4;
    if (localObject4 != null)
    {
      localObject1 = (i.b)b;
      if (localObject1 != null)
      {
        ((i.b)localObject1).a((com.truecaller.truepay.app.ui.payments.models.a)localObject4, paramh, localHashMap, parama);
        return;
      }
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "rechargeContext");
    d = paramString;
    paramString = (i.b)b;
    Object localObject1 = null;
    String str1;
    if (paramString != null)
    {
      paramString = paramString.b();
      str1 = paramString;
    }
    else
    {
      str1 = null;
    }
    paramString = (i.b)b;
    String str2;
    if (paramString != null)
    {
      paramString = paramString.c();
      str2 = paramString;
    }
    else
    {
      str2 = null;
    }
    if (str2 != null) {
      localObject1 = ae.a(str2);
    }
    Object localObject2 = localObject1;
    paramString = (ag)bg.a;
    localObject1 = i;
    Object localObject3 = new com/truecaller/truepay/app/ui/payments/c/j$a;
    ((j.a)localObject3).<init>(this, str2, (String)localObject2, str1, null);
    localObject3 = (c.g.a.m)localObject3;
    e.b(paramString, (f)localObject1, (c.g.a.m)localObject3, 2);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "queryText");
    Object localObject1 = paramString;
    localObject1 = (CharSequence)paramString;
    int k = ((CharSequence)localObject1).length();
    i.b localb = null;
    if (k == 0)
    {
      k = 1;
    }
    else
    {
      k = 0;
      localObject1 = null;
    }
    Object localObject2;
    if (k != 0)
    {
      paramString = (i.b)b;
      if (paramString != null)
      {
        localObject1 = new java/util/ArrayList;
        localObject2 = (Collection)c;
        ((ArrayList)localObject1).<init>((Collection)localObject2);
        paramString.a((ArrayList)localObject1);
        paramString.a(false);
      }
      return;
    }
    localObject1 = c;
    paramString = z.a((List)localObject1, paramString);
    boolean bool = paramString.isEmpty();
    localb = (i.b)b;
    if (localb != null)
    {
      localObject2 = new java/util/ArrayList;
      paramString = (Collection)paramString;
      ((ArrayList)localObject2).<init>(paramString);
      localb.a((ArrayList)localObject2);
      localb.a(bool);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.c.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */