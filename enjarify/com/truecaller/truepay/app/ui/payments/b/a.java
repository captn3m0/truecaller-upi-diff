package com.truecaller.truepay.app.ui.payments.b;

import android.content.SharedPreferences;
import com.truecaller.truepay.app.ui.payments.b.a;
import com.truecaller.truepay.app.ui.payments.c.ab;
import com.truecaller.truepay.app.ui.payments.c.ac.b;
import com.truecaller.truepay.app.ui.payments.c.ae;
import com.truecaller.truepay.app.ui.payments.c.af.b;
import com.truecaller.truepay.app.ui.payments.c.ah;
import com.truecaller.truepay.app.ui.payments.c.d.a;
import com.truecaller.truepay.app.ui.payments.c.i.a;
import com.truecaller.truepay.app.ui.payments.c.l.a;
import com.truecaller.truepay.app.ui.payments.c.v;
import com.truecaller.truepay.app.ui.payments.c.w.b;
import com.truecaller.truepay.app.ui.payments.c.z.b;
import com.truecaller.truepay.app.ui.payments.views.activities.PaymentsActivity;
import com.truecaller.truepay.app.ui.payments.views.b.i;
import com.truecaller.truepay.app.ui.payments.views.b.l;
import com.truecaller.truepay.app.ui.payments.views.b.m;
import com.truecaller.truepay.app.utils.aa;
import com.truecaller.truepay.app.utils.aj;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.ax;
import com.truecaller.truepay.app.utils.z;
import javax.inject.Provider;

public final class a
  implements b
{
  private Provider A;
  private Provider B;
  private Provider C;
  private Provider D;
  private Provider E;
  private Provider F;
  private Provider G;
  private Provider H;
  private Provider I;
  private Provider J;
  private Provider K;
  private Provider L;
  private Provider M;
  private Provider N;
  private Provider O;
  private Provider P;
  private Provider Q;
  private Provider R;
  private Provider S;
  private Provider T;
  private Provider U;
  private Provider V;
  private Provider W;
  private Provider X;
  private Provider Y;
  private Provider Z;
  private final com.truecaller.truepay.app.a.a.a a;
  private Provider aa;
  private Provider ab;
  private Provider ac;
  private Provider ad;
  private Provider ae;
  private Provider af;
  private Provider ag;
  private final com.truecaller.truepay.data.a.o b;
  private Provider c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  private Provider o;
  private Provider p;
  private Provider q;
  private Provider r;
  private Provider s;
  private Provider t;
  private Provider u;
  private Provider v;
  private Provider w;
  private Provider x;
  private Provider y;
  private Provider z;
  
  private a(com.truecaller.truepay.app.ui.payments.b.a.a parama, com.truecaller.truepay.data.a.o paramo, com.truecaller.truepay.app.a.a.a parama1)
  {
    a = parama1;
    b = paramo;
    Object localObject4 = new com/truecaller/truepay/app/ui/payments/b/a$s;
    ((a.s)localObject4).<init>(parama1);
    c = ((Provider)localObject4);
    localObject4 = com.truecaller.truepay.data.e.d.a(c);
    d = ((Provider)localObject4);
    localObject4 = d;
    localObject4 = dagger.a.c.a(com.truecaller.truepay.app.ui.payments.b.a.b.a(parama, (Provider)localObject4));
    e = ((Provider)localObject4);
    localObject4 = dagger.a.c.a(com.truecaller.truepay.data.a.q.a(paramo));
    f = ((Provider)localObject4);
    localObject4 = new com/truecaller/truepay/app/ui/payments/b/a$w;
    ((a.w)localObject4).<init>(parama1);
    g = ((Provider)localObject4);
    localObject4 = new com/truecaller/truepay/app/ui/payments/b/a$d;
    ((a.d)localObject4).<init>(parama1);
    h = ((Provider)localObject4);
    localObject4 = new com/truecaller/truepay/app/ui/payments/b/a$e;
    ((a.e)localObject4).<init>(parama1);
    i = ((Provider)localObject4);
    localObject4 = g;
    Provider localProvider1 = h;
    Provider localProvider2 = i;
    localObject4 = com.truecaller.truepay.app.ui.payments.c.u.a((Provider)localObject4, localProvider1, localProvider2);
    j = ((Provider)localObject4);
    localObject4 = dagger.a.c.a(j);
    k = ((Provider)localObject4);
    localObject4 = new com/truecaller/truepay/app/ui/payments/b/a$y;
    ((a.y)localObject4).<init>(parama1);
    l = ((Provider)localObject4);
    localObject4 = com.truecaller.truepay.a.a.d.j.a(l);
    m = ((Provider)localObject4);
    localObject4 = com.truecaller.truepay.a.a.d.h.a(l);
    n = ((Provider)localObject4);
    localObject4 = new com/truecaller/truepay/app/ui/payments/b/a$j;
    ((a.j)localObject4).<init>(parama1);
    o = ((Provider)localObject4);
    localObject4 = o;
    localObject4 = dagger.a.c.a(com.truecaller.truepay.data.a.s.a(paramo, (Provider)localObject4));
    p = ((Provider)localObject4);
    localObject4 = new com/truecaller/truepay/app/ui/payments/b/a$v;
    ((a.v)localObject4).<init>(parama1);
    q = ((Provider)localObject4);
    localObject4 = q;
    localObject4 = dagger.a.c.a(com.truecaller.truepay.data.a.t.a(paramo, (Provider)localObject4));
    r = ((Provider)localObject4);
    localObject4 = p;
    localProvider1 = r;
    localObject4 = com.truecaller.truepay.data.f.y.a((Provider)localObject4, localProvider1);
    s = ((Provider)localObject4);
    localObject4 = com.truecaller.truepay.a.a.f.o.a(s);
    t = ((Provider)localObject4);
    localObject4 = com.truecaller.truepay.a.a.f.e.a(s);
    u = ((Provider)localObject4);
    localObject4 = com.truecaller.truepay.a.a.d.b.a(l);
    v = ((Provider)localObject4);
    localObject4 = new com/truecaller/truepay/app/ui/payments/b/a$g;
    ((a.g)localObject4).<init>(parama1);
    w = ((Provider)localObject4);
    localObject4 = new com/truecaller/truepay/app/ui/payments/b/a$q;
    ((a.q)localObject4).<init>(parama1);
    x = ((Provider)localObject4);
    localObject4 = new com/truecaller/truepay/app/ui/payments/b/a$b;
    ((a.b)localObject4).<init>(parama1);
    y = ((Provider)localObject4);
    localObject4 = new com/truecaller/truepay/app/ui/payments/b/a$t;
    ((a.t)localObject4).<init>(parama1);
    z = ((Provider)localObject4);
    localObject4 = new com/truecaller/truepay/app/ui/payments/b/a$m;
    ((a.m)localObject4).<init>(parama1);
    A = ((Provider)localObject4);
    localObject4 = new com/truecaller/truepay/app/ui/payments/b/a$n;
    ((a.n)localObject4).<init>(parama1);
    B = ((Provider)localObject4);
    localObject4 = new com/truecaller/truepay/app/ui/payments/b/a$h;
    ((a.h)localObject4).<init>(parama1);
    C = ((Provider)localObject4);
    localObject4 = A;
    localProvider1 = d;
    localProvider2 = B;
    Provider localProvider3 = q;
    Provider localProvider4 = C;
    localObject4 = com.truecaller.truepay.app.utils.a.c.a((Provider)localObject4, localProvider1, localProvider2, localProvider3, localProvider4);
    D = ((Provider)localObject4);
    localObject4 = dagger.a.c.a(D);
    E = ((Provider)localObject4);
    localProvider1 = m;
    localProvider2 = n;
    localProvider3 = t;
    localProvider4 = u;
    Provider localProvider5 = v;
    Provider localProvider6 = w;
    Provider localProvider7 = x;
    Provider localProvider8 = d;
    Provider localProvider9 = y;
    Provider localProvider10 = z;
    Provider localProvider11 = g;
    localObject4 = h;
    localObject2 = E;
    localObject2 = com.truecaller.truepay.app.ui.payments.c.r.a(localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6, localProvider7, localProvider8, localProvider9, localProvider10, localProvider11, (Provider)localObject4, (Provider)localObject2);
    F = ((Provider)localObject2);
    localObject2 = dagger.a.c.a(F);
    G = ((Provider)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/payments/b/a$k;
    ((a.k)localObject2).<init>(parama1);
    H = ((Provider)localObject2);
    localObject2 = H;
    localObject1 = dagger.a.c.a(com.truecaller.truepay.app.ui.payments.b.a.c.a(parama, (Provider)localObject2));
    I = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/payments/b/a$c;
    ((a.c)localObject1).<init>(parama1);
    J = ((Provider)localObject1);
    localObject4 = aa.a();
    localProvider1 = J;
    localProvider2 = E;
    localProvider3 = x;
    localProvider4 = g;
    localProvider5 = h;
    localObject1 = com.truecaller.truepay.app.ui.payments.c.k.a((Provider)localObject4, localProvider1, localProvider2, localProvider3, localProvider4, localProvider5);
    K = ((Provider)localObject1);
    localObject1 = dagger.a.c.a(K);
    L = ((Provider)localObject1);
    localObject1 = com.truecaller.truepay.app.ui.payments.c.f.a(com.truecaller.truepay.app.utils.y.a());
    M = ((Provider)localObject1);
    localObject1 = dagger.a.c.a(M);
    N = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/payments/b/a$o;
    ((a.o)localObject1).<init>(parama1);
    O = ((Provider)localObject1);
    localObject1 = O;
    localObject2 = paramo;
    localObject1 = dagger.a.c.a(com.truecaller.truepay.data.a.p.a(paramo, (Provider)localObject1));
    P = ((Provider)localObject1);
    localObject4 = h;
    localProvider1 = g;
    localProvider2 = y;
    localProvider3 = x;
    localProvider4 = w;
    localProvider5 = d;
    localProvider6 = l;
    localProvider7 = q;
    localObject1 = com.truecaller.truepay.app.ui.payments.c.n.a((Provider)localObject4, localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6, localProvider7);
    Q = ((Provider)localObject1);
    localObject1 = dagger.a.c.a(Q);
    R = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/payments/b/a$x;
    ((a.x)localObject1).<init>(parama1);
    S = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/payments/b/a$r;
    ((a.r)localObject1).<init>(parama1);
    T = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/payments/b/a$u;
    ((a.u)localObject1).<init>(parama1);
    U = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/payments/b/a$f;
    ((a.f)localObject1).<init>(parama1);
    V = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/payments/b/a$p;
    ((a.p)localObject1).<init>(parama1);
    W = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/payments/b/a$z;
    ((a.z)localObject1).<init>(parama1);
    X = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/payments/b/a$i;
    ((a.i)localObject1).<init>(parama1);
    Y = ((Provider)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/payments/b/a$l;
    ((a.l)localObject1).<init>(parama1);
    Z = ((Provider)localObject1);
    localObject2 = h;
    localObject3 = g;
    localObject4 = S;
    localProvider1 = y;
    localProvider2 = T;
    localProvider3 = U;
    localProvider4 = V;
    localProvider5 = x;
    localProvider6 = W;
    localProvider7 = X;
    localProvider8 = z;
    localProvider9 = Y;
    localProvider10 = Z;
    localProvider11 = O;
    localObject1 = ah.a((Provider)localObject2, (Provider)localObject3, (Provider)localObject4, localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6, localProvider7, localProvider8, localProvider9, localProvider10, localProvider11);
    aa = ((Provider)localObject1);
    localObject1 = dagger.a.c.a(aa);
    ab = ((Provider)localObject1);
    localObject1 = l;
    localObject2 = x;
    localObject1 = com.truecaller.truepay.app.ui.payments.c.y.a((Provider)localObject1, (Provider)localObject2);
    ac = ((Provider)localObject1);
    localObject1 = dagger.a.c.a(ac);
    ad = ((Provider)localObject1);
    localObject1 = dagger.a.c.a(ab.a());
    ae = ((Provider)localObject1);
    localObject1 = ae.a(U);
    af = ((Provider)localObject1);
    localObject1 = dagger.a.c.a(af);
    ag = ((Provider)localObject1);
  }
  
  public static a.a a()
  {
    a.a locala = new com/truecaller/truepay/app/ui/payments/b/a$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  private com.truecaller.truepay.app.ui.payments.c.s b()
  {
    com.truecaller.truepay.app.ui.payments.c.s locals = new com/truecaller/truepay/app/ui/payments/c/s;
    Object localObject1 = locals;
    Object localObject2 = new com/truecaller/truepay/a/a/d/f;
    Object localObject3 = localObject2;
    Object localObject4 = (com.truecaller.truepay.data.api.h)dagger.a.g.a(a.m(), "Cannot return null from a non-@Nullable component method");
    ((com.truecaller.truepay.a.a.d.f)localObject2).<init>((com.truecaller.truepay.data.api.h)localObject4);
    localObject2 = (ax)dagger.a.g.a(a.e(), "Cannot return null from a non-@Nullable component method");
    localObject4 = (com.truecaller.truepay.app.utils.q)dagger.a.g.a(a.f(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.a.a.c.c localc = (com.truecaller.truepay.a.a.c.c)dagger.a.g.a(a.ah(), "Cannot return null from a non-@Nullable component method");
    dagger.a.g.a(a.C(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.data.e.b localb = (com.truecaller.truepay.data.e.b)e.get();
    com.truecaller.truepay.data.e.e locale1 = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.Q(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.data.e.a locala1 = (com.truecaller.truepay.data.e.a)dagger.a.g.a(a.T(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.data.e.e locale2 = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.B(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.data.e.a locala2 = (com.truecaller.truepay.data.e.a)dagger.a.g.a(a.C(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.data.e.a locala3 = (com.truecaller.truepay.data.e.a)dagger.a.g.a(a.z(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.a.a.c.e locale = (com.truecaller.truepay.a.a.c.e)dagger.a.g.a(a.ag(), "Cannot return null from a non-@Nullable component method");
    Object localObject5 = new com/truecaller/truepay/a/a/a;
    Object localObject6 = localObject5;
    Object localObject7 = new com/truecaller/truepay/data/f/t;
    localObject1 = com.truecaller.truepay.data.a.r.a((com.truecaller.truepay.data.c.e)f.get());
    Object localObject8 = localObject3;
    localObject3 = a.p();
    Object localObject9 = localObject2;
    localObject3 = (com.truecaller.truepay.data.api.g)dagger.a.g.a(localObject3, "Cannot return null from a non-@Nullable component method");
    localObject2 = (com.truecaller.truepay.data.c.e)f.get();
    localObject3 = com.truecaller.truepay.data.a.u.a((com.truecaller.truepay.data.api.g)localObject3, (com.truecaller.truepay.data.c.e)localObject2);
    ((com.truecaller.truepay.data.f.t)localObject7).<init>((com.truecaller.truepay.data.f.s)localObject1, (com.truecaller.truepay.data.f.s)localObject3);
    ((com.truecaller.truepay.a.a.a)localObject5).<init>((com.truecaller.truepay.data.f.t)localObject7);
    localObject1 = dagger.a.g.a(a.i(), "Cannot return null from a non-@Nullable component method");
    localObject5 = localObject1;
    localObject5 = (aj)localObject1;
    localObject1 = dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    localObject7 = localObject1;
    localObject7 = (com.truecaller.featuretoggles.e)localObject1;
    localObject1 = dagger.a.g.a(a.am(), "Cannot return null from a non-@Nullable component method");
    Object localObject10 = localObject1;
    localObject10 = (com.google.gson.f)localObject1;
    localObject1 = new com/truecaller/truepay/data/e/c;
    Object localObject11 = localObject1;
    localObject3 = (SharedPreferences)dagger.a.g.a(a.aa(), "Cannot return null from a non-@Nullable component method");
    ((com.truecaller.truepay.data.e.c)localObject1).<init>((SharedPreferences)localObject3);
    localObject1 = locals;
    localObject3 = localObject8;
    localObject2 = localObject9;
    locals.<init>((com.truecaller.truepay.a.a.d.f)localObject8, (ax)localObject9, (com.truecaller.truepay.app.utils.q)localObject4, localc, localb, locale1, locala1, locale2, locala2, locala3, locale, (com.truecaller.truepay.a.a.a)localObject6, (aj)localObject5, (com.truecaller.featuretoggles.e)localObject7, (com.google.gson.f)localObject10, (com.truecaller.truepay.data.e.c)localObject11);
    return locals;
  }
  
  public final void a(PaymentsActivity paramPaymentsActivity)
  {
    Object localObject = (com.truecaller.featuretoggles.e)dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    featuresRegistry = ((com.truecaller.featuretoggles.e)localObject);
    localObject = (com.truecaller.truepay.app.utils.u)dagger.a.g.a(a.x(), "Cannot return null from a non-@Nullable component method");
    instantRewardHandler = ((com.truecaller.truepay.app.utils.u)localObject);
    localObject = b();
    b = ((com.truecaller.truepay.app.ui.payments.c.s)localObject);
    localObject = (ax)dagger.a.g.a(a.e(), "Cannot return null from a non-@Nullable component method");
    c = ((ax)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.views.b.a parama)
  {
    com.truecaller.truepay.app.ui.payments.c.a locala = new com/truecaller/truepay/app/ui/payments/c/a;
    com.truecaller.truepay.a.a.d.d locald = new com/truecaller/truepay/a/a/d/d;
    Object localObject1 = new com/truecaller/truepay/data/f/o;
    Object localObject2 = (com.truecaller.truepay.data.f.p)P.get();
    ((com.truecaller.truepay.data.f.o)localObject1).<init>((com.truecaller.truepay.data.f.p)localObject2);
    locald.<init>((com.truecaller.truepay.data.f.o)localObject1);
    localObject1 = dagger.a.g.a(a.K(), "Cannot return null from a non-@Nullable component method");
    localObject2 = localObject1;
    localObject2 = (com.truecaller.truepay.data.e.e)localObject1;
    localObject1 = dagger.a.g.a(a.k(), "Cannot return null from a non-@Nullable component method");
    Object localObject3 = localObject1;
    localObject3 = (au)localObject1;
    localObject1 = dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    Object localObject4 = localObject1;
    localObject4 = (com.truecaller.truepay.app.utils.r)localObject1;
    localObject1 = dagger.a.g.a(a.V(), "Cannot return null from a non-@Nullable component method");
    Object localObject5 = localObject1;
    localObject5 = (com.truecaller.truepay.data.e.a)localObject1;
    localObject1 = dagger.a.g.a(a.D(), "Cannot return null from a non-@Nullable component method");
    Object localObject6 = localObject1;
    localObject6 = (com.truecaller.truepay.data.e.a)localObject1;
    localObject1 = dagger.a.g.a(a.h(), "Cannot return null from a non-@Nullable component method");
    Object localObject7 = localObject1;
    localObject7 = (com.truecaller.truepay.app.utils.a)localObject1;
    localObject1 = dagger.a.g.a(a.Q(), "Cannot return null from a non-@Nullable component method");
    Object localObject8 = localObject1;
    localObject8 = (com.truecaller.truepay.data.e.e)localObject1;
    localObject1 = dagger.a.g.a(a.J(), "Cannot return null from a non-@Nullable component method");
    Object localObject9 = localObject1;
    localObject9 = (com.truecaller.truepay.data.e.e)localObject1;
    localObject1 = dagger.a.g.a(a.af(), "Cannot return null from a non-@Nullable component method");
    Object localObject10 = localObject1;
    localObject10 = (com.truecaller.utils.n)localObject1;
    localObject1 = dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    Object localObject11 = localObject1;
    localObject11 = (com.truecaller.featuretoggles.e)localObject1;
    localObject1 = locala;
    locala.<init>(locald, (com.truecaller.truepay.data.e.e)localObject2, (au)localObject3, (com.truecaller.truepay.app.utils.r)localObject4, (com.truecaller.truepay.data.e.a)localObject5, (com.truecaller.truepay.data.e.a)localObject6, (com.truecaller.truepay.app.utils.a)localObject7, (com.truecaller.truepay.data.e.e)localObject8, (com.truecaller.truepay.data.e.e)localObject9, (com.truecaller.utils.n)localObject10, (com.truecaller.featuretoggles.e)localObject11);
    a = locala;
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.views.b.c paramc)
  {
    com.truecaller.truepay.app.ui.payments.c.c localc = new com/truecaller/truepay/app/ui/payments/c/c;
    com.truecaller.truepay.app.utils.r localr = (com.truecaller.truepay.app.utils.r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.utils.n localn = (com.truecaller.utils.n)dagger.a.g.a(a.af(), "Cannot return null from a non-@Nullable component method");
    com.truecaller.truepay.app.utils.a locala = (com.truecaller.truepay.app.utils.a)dagger.a.g.a(a.h(), "Cannot return null from a non-@Nullable component method");
    au localau = (au)dagger.a.g.a(a.k(), "Cannot return null from a non-@Nullable component method");
    localc.<init>(localr, localn, locala, localau);
    a = localc;
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.views.b.e parame)
  {
    d.a locala = (d.a)N.get();
    a = locala;
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.views.b.f paramf)
  {
    Object localObject = (i.a)L.get();
    a = ((i.a)localObject);
    localObject = (com.truecaller.truepay.app.utils.r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    e = ((com.truecaller.truepay.app.utils.r)localObject);
    localObject = new com/truecaller/truepay/app/utils/z;
    ((z)localObject).<init>();
    f = ((z)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.views.b.g paramg)
  {
    Object localObject = (l.a)R.get();
    a = ((l.a)localObject);
    localObject = (com.truecaller.truepay.app.utils.r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    b = ((com.truecaller.truepay.app.utils.r)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.views.b.h paramh)
  {
    Object localObject = (ax)dagger.a.g.a(a.e(), "Cannot return null from a non-@Nullable component method");
    e = ((ax)localObject);
    localObject = new com/truecaller/truepay/app/ui/payments/c/o;
    com.truecaller.truepay.a.a.d.e locale = new com/truecaller/truepay/a/a/d/e;
    com.truecaller.truepay.data.api.h localh = (com.truecaller.truepay.data.api.h)dagger.a.g.a(a.m(), "Cannot return null from a non-@Nullable component method");
    locale.<init>(localh);
    ((com.truecaller.truepay.app.ui.payments.c.o)localObject).<init>(locale);
    f = ((com.truecaller.truepay.app.ui.payments.c.o)localObject);
  }
  
  public final void a(i parami)
  {
    Object localObject = (com.truecaller.truepay.app.utils.r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    k = ((com.truecaller.truepay.app.utils.r)localObject);
    localObject = (com.truecaller.truepay.app.ui.payments.c.p)G.get();
    l = ((com.truecaller.truepay.app.ui.payments.c.p)localObject);
    localObject = (com.truecaller.truepay.app.ui.history.views.c.b)I.get();
    n = ((com.truecaller.truepay.app.ui.history.views.c.b)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.views.b.j paramj)
  {
    Object localObject = (com.truecaller.truepay.app.utils.a)dagger.a.g.a(a.h(), "Cannot return null from a non-@Nullable component method");
    n = ((com.truecaller.truepay.app.utils.a)localObject);
    localObject = (com.truecaller.truepay.app.utils.r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    o = ((com.truecaller.truepay.app.utils.r)localObject);
    localObject = (ax)dagger.a.g.a(a.e(), "Cannot return null from a non-@Nullable component method");
    p = ((ax)localObject);
    localObject = b();
    q = ((com.truecaller.truepay.app.ui.payments.c.s)localObject);
    localObject = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.Q(), "Cannot return null from a non-@Nullable component method");
    r = ((com.truecaller.truepay.data.e.e)localObject);
    localObject = (com.truecaller.truepay.data.e.a)dagger.a.g.a(a.V(), "Cannot return null from a non-@Nullable component method");
    s = ((com.truecaller.truepay.data.e.a)localObject);
    localObject = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.U(), "Cannot return null from a non-@Nullable component method");
    t = ((com.truecaller.truepay.data.e.e)localObject);
    localObject = (b.a)k.get();
    u = ((b.a)localObject);
    localObject = (com.truecaller.featuretoggles.e)dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    v = ((com.truecaller.featuretoggles.e)localObject);
    localObject = (com.truecaller.truepay.app.utils.u)dagger.a.g.a(a.x(), "Cannot return null from a non-@Nullable component method");
    w = ((com.truecaller.truepay.app.utils.u)localObject);
    localObject = (com.google.gson.f)dagger.a.g.a(a.am(), "Cannot return null from a non-@Nullable component method");
    x = ((com.google.gson.f)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.views.b.k paramk)
  {
    v localv = new com/truecaller/truepay/app/ui/payments/c/v;
    localv.<init>();
    b = localv;
  }
  
  public final void a(l paraml)
  {
    w.b localb = (w.b)ad.get();
    a = localb;
  }
  
  public final void a(m paramm)
  {
    z.b localb = (z.b)ae.get();
    a = localb;
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.views.b.n paramn)
  {
    ac.b localb = (ac.b)ag.get();
    a = localb;
  }
  
  public final void a(com.truecaller.truepay.app.ui.payments.views.b.o paramo)
  {
    Object localObject = (af.b)ab.get();
    a = ((af.b)localObject);
    localObject = (com.truecaller.truepay.app.utils.r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    b = ((com.truecaller.truepay.app.utils.r)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */