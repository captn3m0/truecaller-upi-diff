package com.truecaller.truepay.app.ui.payments.a;

import android.support.v7.widget.RecyclerView.Adapter;
import java.util.List;

public final class n
  extends RecyclerView.Adapter
{
  private final List a;
  private final n.b b;
  
  public n(List paramList, n.b paramb)
  {
    a = paramList;
    b = paramb;
  }
  
  public final int getItemCount()
  {
    return a.size();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.a.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */