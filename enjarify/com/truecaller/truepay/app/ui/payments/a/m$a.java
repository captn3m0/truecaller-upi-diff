package com.truecaller.truepay.app.ui.payments.a;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.truepay.R.id;

public final class m$a
  extends RecyclerView.ViewHolder
{
  final TextView a;
  final TextView b;
  
  public m$a(m paramm, View paramView)
  {
    super(paramView);
    int i = R.id.tvTimeDetails;
    paramm = paramView.findViewById(i);
    k.a(paramm, "itemLayoutView.findViewById(R.id.tvTimeDetails)");
    paramm = (TextView)paramm;
    a = paramm;
    i = R.id.tvCancellationCharge;
    paramm = paramView.findViewById(i);
    k.a(paramm, "itemLayoutView.findViewB….id.tvCancellationCharge)");
    paramm = (TextView)paramm;
    b = paramm;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.a.m.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */