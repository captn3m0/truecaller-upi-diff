package com.truecaller.truepay.app.ui.payments.a;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.c;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.payments.d.a;

public final class h
  extends c
{
  public h(f paramf)
  {
    super(paramf);
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    a locala = new com/truecaller/truepay/app/ui/payments/d/a;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.list_item_plan;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    locala.<init>(paramViewGroup, (f)localObject);
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.a.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */