package com.truecaller.truepay.app.ui.payments.a;

import android.support.v4.app.Fragment;
import android.support.v4.app.n;
import android.util.SparseArray;
import android.view.ViewGroup;
import com.truecaller.truepay.app.ui.payments.models.d;
import java.util.List;

public final class j
  extends n
{
  k a;
  private final List b;
  private final SparseArray c;
  
  public j(android.support.v4.app.j paramj, List paramList, k paramk)
  {
    super(paramj);
    b = paramList;
    a = paramk;
    paramj = new android/util/SparseArray;
    paramj.<init>();
    c = paramj;
  }
  
  public final Fragment a(int paramInt)
  {
    d locald = (d)b.get(paramInt);
    k localk = a;
    return com.truecaller.truepay.app.ui.payments.views.b.k.a(locald, localk);
  }
  
  public final void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    super.destroyItem(paramViewGroup, paramInt, paramObject);
    c.remove(paramInt);
  }
  
  public final int getCount()
  {
    return b.size();
  }
  
  public final CharSequence getPageTitle(int paramInt)
  {
    return b.get(paramInt)).a;
  }
  
  public final Object instantiateItem(ViewGroup paramViewGroup, int paramInt)
  {
    paramViewGroup = (Fragment)super.instantiateItem(paramViewGroup, paramInt);
    c.put(paramInt, paramViewGroup);
    return paramViewGroup;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.a.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */