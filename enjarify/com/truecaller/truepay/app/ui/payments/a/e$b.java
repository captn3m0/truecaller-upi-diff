package com.truecaller.truepay.app.ui.payments.a;

import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.payments.models.a;

public final class e$b
  extends RecyclerView.ViewHolder
  implements View.OnClickListener
{
  final TextView a;
  private final e.a c;
  
  public e$b(e parame, View paramView, e.a parama)
  {
    super(paramView);
    c = parama;
    int i = R.id.tv_circle_name;
    parame = paramView.findViewById(i);
    k.a(parame, "itemLayoutView.findViewById(R.id.tv_circle_name)");
    parame = (TextView)parame;
    a = parame;
    parame = this;
    parame = (View.OnClickListener)this;
    paramView.setOnClickListener(parame);
  }
  
  public final void onClick(View paramView)
  {
    k.b(paramView, "v");
    paramView = c;
    Object localObject = e.a(b);
    int i = getAdapterPosition();
    localObject = ((SortedList)localObject).get(i);
    k.a(localObject, "circleList[adapterPosition]");
    localObject = (a)localObject;
    paramView.a((a)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.a.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */