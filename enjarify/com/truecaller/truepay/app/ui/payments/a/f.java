package com.truecaller.truepay.app.ui.payments.a;

import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.history.models.o;
import com.truecaller.truepay.app.ui.payments.c.g;
import com.truecaller.truepay.app.ui.payments.models.a;
import com.truecaller.truepay.app.utils.r;

public final class f
  extends RecyclerView.Adapter
{
  public static final f.a a;
  private final SortedList b;
  private final f.d c;
  private final f.f d;
  private final r e;
  
  static
  {
    f.a locala = new com/truecaller/truepay/app/ui/payments/a/f$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public f(SortedList paramSortedList, f.d paramd, f.f paramf, r paramr)
  {
    b = paramSortedList;
    c = paramd;
    d = paramf;
    e = paramr;
  }
  
  public final int getItemCount()
  {
    return b.size();
  }
  
  public final int getItemViewType(int paramInt)
  {
    SortedList localSortedList = b;
    com.truecaller.truepay.app.ui.payments.c.h localh = (com.truecaller.truepay.app.ui.payments.c.h)localSortedList.get(paramInt);
    boolean bool = localh instanceof g;
    if (bool) {
      return 0;
    }
    bool = localh instanceof a;
    if (bool) {
      return 1;
    }
    paramInt = localh instanceof com.truecaller.truepay.app.ui.history.models.h;
    if (paramInt != 0) {
      return 2;
    }
    return -1;
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    k.b(paramViewHolder, "viewHolder");
    Object localObject1 = b;
    Object localObject2 = (com.truecaller.truepay.app.ui.payments.c.h)((SortedList)localObject1).get(paramInt);
    boolean bool = paramViewHolder instanceof f.e;
    Object localObject3;
    int i;
    if (bool)
    {
      bool = localObject2 instanceof a;
      if (bool)
      {
        paramViewHolder = (f.e)paramViewHolder;
        localObject2 = (a)localObject2;
        localObject1 = a;
        localObject3 = (CharSequence)((a)localObject2).d();
        ((TextView)localObject1).setText((CharSequence)localObject3);
        localObject1 = e;
        localObject2 = ((a)localObject2).o();
        paramViewHolder = b;
        i = R.drawable.ic_place_holder_circle;
        ((r)localObject1).a((String)localObject2, paramViewHolder, i, i);
        return;
      }
    }
    bool = paramViewHolder instanceof f.b;
    if (bool)
    {
      bool = localObject2 instanceof g;
      if (bool)
      {
        paramViewHolder = (f.b)paramViewHolder;
        localObject2 = (g)localObject2;
        paramViewHolder = a;
        localObject2 = (CharSequence)a;
        paramViewHolder.setText((CharSequence)localObject2);
        return;
      }
    }
    bool = paramViewHolder instanceof f.c;
    if (bool)
    {
      bool = localObject2 instanceof com.truecaller.truepay.app.ui.history.models.h;
      if (bool)
      {
        paramViewHolder = (f.c)paramViewHolder;
        localObject2 = (com.truecaller.truepay.app.ui.history.models.h)localObject2;
        localObject1 = b;
        localObject3 = ((com.truecaller.truepay.app.ui.history.models.h)localObject2).a();
        k.a(localObject3, "item.utilityDO");
        localObject3 = (CharSequence)((o)localObject3).b();
        ((TextView)localObject1).setText((CharSequence)localObject3);
        localObject1 = c;
        localObject3 = ((com.truecaller.truepay.app.ui.history.models.h)localObject2).a();
        String str = "item.utilityDO";
        k.a(localObject3, str);
        localObject3 = (CharSequence)((o)localObject3).a();
        ((TextView)localObject1).setText((CharSequence)localObject3);
        localObject1 = e;
        localObject2 = ((com.truecaller.truepay.app.ui.history.models.h)localObject2).a();
        localObject3 = "item.utilityDO";
        k.a(localObject2, (String)localObject3);
        localObject2 = ((o)localObject2).f();
        paramViewHolder = a;
        i = R.drawable.ic_place_holder_circle;
        ((r)localObject1).a((String)localObject2, paramViewHolder, i, i);
      }
    }
  }
  
  public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    k.b(paramViewGroup, "viewGroup");
    Object localObject1 = LayoutInflater.from(paramViewGroup.getContext());
    switch (paramInt)
    {
    default: 
      paramViewGroup = new java/lang/RuntimeException;
      paramViewGroup.<init>("Invalid view");
      throw ((Throwable)paramViewGroup);
    case 2: 
      localObject2 = new com/truecaller/truepay/app/ui/payments/a/f$c;
      i = R.layout.list_item_recent_transactions;
      paramViewGroup = ((LayoutInflater)localObject1).inflate(i, paramViewGroup, false);
      k.a(paramViewGroup, "inflater.inflate(R.layou…ctions, viewGroup, false)");
      localObject1 = d;
      ((f.c)localObject2).<init>(this, paramViewGroup, (f.f)localObject1);
      return (RecyclerView.ViewHolder)localObject2;
    case 1: 
      localObject2 = new com/truecaller/truepay/app/ui/payments/a/f$e;
      i = R.layout.item_operator;
      paramViewGroup = ((LayoutInflater)localObject1).inflate(i, paramViewGroup, false);
      k.a(paramViewGroup, "inflater.inflate(R.layou…erator, viewGroup, false)");
      localObject1 = c;
      ((f.e)localObject2).<init>(this, paramViewGroup, (f.d)localObject1);
      return (RecyclerView.ViewHolder)localObject2;
    }
    Object localObject2 = new com/truecaller/truepay/app/ui/payments/a/f$b;
    int i = R.layout.item_operator_header;
    paramViewGroup = ((LayoutInflater)localObject1).inflate(i, paramViewGroup, false);
    k.a(paramViewGroup, "inflater.inflate(R.layou…header, viewGroup, false)");
    ((f.b)localObject2).<init>(this, paramViewGroup);
    return (RecyclerView.ViewHolder)localObject2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */