package com.truecaller.truepay.app.ui.payments.a;

import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.history.models.h;

public final class f$c
  extends RecyclerView.ViewHolder
  implements View.OnClickListener
{
  final ImageView a;
  final TextView b;
  final TextView c;
  private final View e;
  private final f.f f;
  
  public f$c(f paramf, View paramView, f.f paramf1)
  {
    super(paramView);
    f = paramf1;
    paramf = itemView;
    int i = R.id.im_profile_pic;
    paramf = paramf.findViewById(i);
    k.a(paramf, "itemView.findViewById(R.id.im_profile_pic)");
    paramf = (ImageView)paramf;
    a = paramf;
    paramf = itemView;
    i = R.id.tv_person_name;
    paramf = paramf.findViewById(i);
    k.a(paramf, "itemView.findViewById(R.id.tv_person_name)");
    paramf = (TextView)paramf;
    b = paramf;
    paramf = itemView;
    i = R.id.tv_payment_status;
    paramf = paramf.findViewById(i);
    k.a(paramf, "itemView.findViewById(R.id.tv_payment_status)");
    paramf = (TextView)paramf;
    c = paramf;
    paramf = itemView;
    i = R.id.root;
    paramf = paramf.findViewById(i);
    k.a(paramf, "itemView.findViewById(R.id.root)");
    e = paramf;
    paramf = e;
    paramView = this;
    paramView = (View.OnClickListener)this;
    paramf.setOnClickListener(paramView);
  }
  
  public final void onClick(View paramView)
  {
    k.b(paramView, "v");
    f.f localf = f;
    Object localObject = f.a(d);
    int i = getAdapterPosition();
    localObject = ((SortedList)localObject).get(i);
    if (localObject != null)
    {
      localObject = (h)localObject;
      localf.onRecentsClicked(paramView, (h)localObject);
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.history.models.HistoryItem");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.a.f.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */