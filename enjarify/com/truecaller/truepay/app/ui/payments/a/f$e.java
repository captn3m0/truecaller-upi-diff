package com.truecaller.truepay.app.ui.payments.a;

import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.payments.models.a;

public final class f$e
  extends RecyclerView.ViewHolder
  implements View.OnClickListener
{
  final TextView a;
  final ImageView b;
  private final f.d d;
  
  public f$e(f paramf, View paramView, f.d paramd)
  {
    super(paramView);
    d = paramd;
    int i = R.id.tv_operator_name;
    paramf = paramView.findViewById(i);
    k.a(paramf, "itemLayoutView.findViewById(R.id.tv_operator_name)");
    paramf = (TextView)paramf;
    a = paramf;
    i = R.id.img_operator_icon;
    paramf = paramView.findViewById(i);
    k.a(paramf, "itemLayoutView.findViewB…d(R.id.img_operator_icon)");
    paramf = (ImageView)paramf;
    b = paramf;
    paramf = this;
    paramf = (View.OnClickListener)this;
    paramView.setOnClickListener(paramf);
  }
  
  public final void onClick(View paramView)
  {
    k.b(paramView, "v");
    f.d locald = d;
    Object localObject = f.a(c);
    int i = getAdapterPosition();
    localObject = ((SortedList)localObject).get(i);
    if (localObject != null)
    {
      localObject = (a)localObject;
      locald.onOperatorSelected(paramView, (a)localObject);
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.payments.models.BaseUtility");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.a.f.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */