package com.truecaller.truepay.app.ui.payments.a;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.history.views.c.b;

public final class q
  extends com.truecaller.truepay.app.ui.base.views.b.c
{
  private b b;
  
  public q(f paramf, b paramb)
  {
    super(paramf);
    b = paramb;
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    com.truecaller.truepay.app.ui.payments.views.d.c localc = new com/truecaller/truepay/app/ui/payments/views/d/c;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.list_item_history_payment_details;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = a;
    b localb = b;
    localc.<init>(paramViewGroup, (f)localObject, localb);
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.a.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */