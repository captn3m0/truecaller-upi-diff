package com.truecaller.truepay.app.ui.payments.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import java.util.List;

public final class m
  extends RecyclerView.Adapter
{
  private final List a;
  private final Context b;
  
  public m(List paramList, Context paramContext)
  {
    a = paramList;
    b = paramContext;
  }
  
  public final int getItemCount()
  {
    return a.size();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.a.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */