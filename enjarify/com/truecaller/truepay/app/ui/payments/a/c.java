package com.truecaller.truepay.app.ui.payments.a;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import c.g.b.k;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.base.views.b.f;
import com.truecaller.truepay.app.ui.payments.views.d.b;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.utils.n;

public final class c
  extends com.truecaller.truepay.app.ui.base.views.b.c
{
  private final r b;
  private final n c;
  private final au d;
  
  public c(f paramf, r paramr, n paramn, au paramau)
  {
    super(paramf);
    b = paramr;
    c = paramn;
    d = paramau;
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup)
  {
    k.b(paramViewGroup, "parent");
    b localb = new com/truecaller/truepay/app/ui/payments/views/d/b;
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.item_rvbill_inline_bank;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    k.a(paramViewGroup, "LayoutInflater.from(pare…line_bank, parent, false)");
    localObject = a;
    k.a(localObject, "listener");
    r localr = b;
    localb.<init>(paramViewGroup, (f)localObject, localr);
    return (RecyclerView.ViewHolder)localb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */