package com.truecaller.truepay.app.ui.payments.a;

import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.payments.models.a;

public final class e
  extends RecyclerView.Adapter
{
  private final SortedList a;
  private final e.a b;
  
  public e(SortedList paramSortedList, e.a parama)
  {
    a = paramSortedList;
    b = parama;
  }
  
  public final int getItemCount()
  {
    return a.size();
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    String str = "viewHolder";
    k.b(paramViewHolder, str);
    boolean bool = paramViewHolder instanceof e.b;
    if (bool)
    {
      Object localObject = (a)a.get(paramInt);
      paramViewHolder = a;
      str = "circle";
      k.a(localObject, str);
      localObject = (CharSequence)((a)localObject).d();
      paramViewHolder.setText((CharSequence)localObject);
    }
  }
  
  public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    k.b(paramViewGroup, "viewGroup");
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.item_circle;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = new com/truecaller/truepay/app/ui/payments/a/e$b;
    k.a(paramViewGroup, "rowView");
    e.a locala = b;
    ((e.b)localObject).<init>(this, paramViewGroup, locala);
    return (RecyclerView.ViewHolder)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */