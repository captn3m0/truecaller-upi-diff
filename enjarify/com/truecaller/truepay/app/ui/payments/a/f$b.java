package com.truecaller.truepay.app.ui.payments.a;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.truepay.R.id;

public final class f$b
  extends RecyclerView.ViewHolder
{
  final TextView a;
  
  public f$b(f paramf, View paramView)
  {
    super(paramView);
    int i = R.id.title;
    paramf = paramView.findViewById(i);
    k.a(paramf, "itemLayoutView.findViewById(R.id.title)");
    paramf = (TextView)paramf;
    a = paramf;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.a.f.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */