package com.truecaller.truepay.app.ui.payments.a;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.payments.models.a;
import java.util.ArrayList;

final class p$b
  extends RecyclerView.ViewHolder
  implements View.OnClickListener
{
  TextView a;
  ImageView b;
  p.a c;
  
  p$b(p paramp, View paramView, p.a parama)
  {
    super(paramView);
    int i = R.id.utility_name_textView;
    paramp = (TextView)paramView.findViewById(i);
    a = paramp;
    i = R.id.utility_imageView;
    paramp = (ImageView)paramView.findViewById(i);
    b = paramp;
    c = parama;
    paramView.setOnClickListener(this);
  }
  
  public final void onClick(View paramView)
  {
    paramView = c;
    Object localObject = p.a(d);
    int i = getAdapterPosition();
    localObject = (a)((ArrayList)localObject).get(i);
    paramView.a_((a)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.a.p.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */