package com.truecaller.truepay.app.ui.payments.a;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.app.ui.registration.a.d;

public final class n$a
  extends d
{
  private TextView b;
  private TextView d;
  private Button e;
  private ImageView f;
  private View g;
  
  public n$a(n paramn, View paramView)
  {
    super(paramView);
    int i = R.id.quick_payment_title;
    paramn = (TextView)paramView.findViewById(i);
    b = paramn;
    i = R.id.quick_payment_subtitle;
    paramn = (TextView)paramView.findViewById(i);
    d = paramn;
    i = R.id.quick_payment_icon;
    paramn = (ImageView)paramView.findViewById(i);
    f = paramn;
    i = R.id.quick_payment_btn;
    paramn = (Button)paramView.findViewById(i);
    e = paramn;
    i = R.id.quick_payment_container;
    paramn = paramView.findViewById(i);
    g = paramn;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.a.n.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */