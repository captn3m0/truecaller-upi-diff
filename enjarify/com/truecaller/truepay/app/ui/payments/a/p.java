package com.truecaller.truepay.app.ui.payments.a;

import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.app.ui.payments.models.a;
import com.truecaller.truepay.app.utils.r;
import java.util.ArrayList;

public final class p
  extends RecyclerView.Adapter
{
  private final ArrayList a;
  private r b;
  private p.a c;
  
  public p(ArrayList paramArrayList, p.a parama, r paramr)
  {
    a = paramArrayList;
    c = parama;
    b = paramr;
  }
  
  public final int getItemCount()
  {
    return a.size();
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    boolean bool1 = paramViewHolder instanceof p.b;
    if (bool1)
    {
      paramViewHolder = (p.b)paramViewHolder;
      a locala = (a)a.get(paramInt);
      TextView localTextView = a;
      String str1 = b;
      localTextView.setText(str1);
      r localr = b;
      String str2 = l;
      ImageView localImageView = b;
      int i = R.drawable.ic_place_holder_circle;
      boolean bool2 = t;
      localr.a(str2, localImageView, i, i, bool2);
    }
  }
  
  public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.list_utility_item;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = new com/truecaller/truepay/app/ui/payments/a/p$b;
    p.a locala = c;
    ((p.b)localObject).<init>(this, paramViewGroup, locala);
    return (RecyclerView.ViewHolder)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.payments.a.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */