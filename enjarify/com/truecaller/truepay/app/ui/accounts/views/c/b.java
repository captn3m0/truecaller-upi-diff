package com.truecaller.truepay.app.ui.accounts.views.c;

import android.support.v4.app.Fragment;
import com.truecaller.truepay.app.ui.base.views.a;

public abstract interface b
  extends a
{
  public abstract void a(Fragment paramFragment);
  
  public abstract int d();
  
  public abstract void e();
  
  public abstract boolean f();
  
  public abstract void onBackPressed();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.accounts.views.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */