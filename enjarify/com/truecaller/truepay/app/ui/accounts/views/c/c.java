package com.truecaller.truepay.app.ui.accounts.views.c;

public abstract interface c
  extends com.truecaller.truepay.app.ui.base.views.a
{
  public abstract void a(int paramInt1, int paramInt2);
  
  public abstract void a(String paramString);
  
  public abstract void a(Throwable paramThrowable);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b(String paramString);
  
  public abstract void b(Throwable paramThrowable);
  
  public abstract void c();
  
  public abstract void c(String paramString);
  
  public abstract void c(Throwable paramThrowable);
  
  public abstract void d();
  
  public abstract void d(String paramString);
  
  public abstract void e(com.truecaller.truepay.data.api.model.a parama);
  
  public abstract void e(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.accounts.views.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */