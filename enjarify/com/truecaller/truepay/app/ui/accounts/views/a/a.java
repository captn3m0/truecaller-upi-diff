package com.truecaller.truepay.app.ui.accounts.views.a;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.r;
import java.util.List;

public final class a
  extends RecyclerView.Adapter
{
  private final r a;
  private final au b;
  private final Context c;
  private List d;
  private int e;
  private a.c f;
  
  public a(Context paramContext, List paramList, r paramr, au paramau, a.c paramc)
  {
    c = paramContext;
    d = paramList;
    a = paramr;
    b = paramau;
    f = paramc;
  }
  
  private com.truecaller.truepay.data.api.model.a a()
  {
    List localList = d;
    int i = e;
    return (com.truecaller.truepay.data.api.model.a)localList.get(i);
  }
  
  private boolean a(MenuItem paramMenuItem)
  {
    int i = paramMenuItem.getItemId();
    int j = R.id.menu_change_pin;
    com.truecaller.truepay.data.api.model.a locala;
    if (i == j)
    {
      paramMenuItem = f;
      locala = a();
      paramMenuItem.a(locala);
    }
    else
    {
      j = R.id.menu_delete;
      if (i == j)
      {
        paramMenuItem = a();
        boolean bool = g;
        if (!bool)
        {
          paramMenuItem = f;
          locala = a();
          paramMenuItem.b(locala);
        }
      }
    }
    return false;
  }
  
  public final void a(List paramList)
  {
    d = paramList;
    notifyDataSetChanged();
  }
  
  public final int getItemCount()
  {
    List localList = d;
    int i = localList.size();
    int j = 2;
    if (i > j) {
      return d.size() + 1;
    }
    return d.size();
  }
  
  public final int getItemViewType(int paramInt)
  {
    List localList = d;
    int i = localList.size();
    if (paramInt == i) {
      return 1;
    }
    return 2;
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    boolean bool1 = paramViewHolder instanceof a.b;
    if (bool1)
    {
      paramViewHolder = (a.b)paramViewHolder;
      Object localObject1 = (com.truecaller.truepay.data.api.model.a)d.get(paramInt);
      Object localObject2 = j;
      if ((localObject1 != null) && (localObject2 != null))
      {
        boolean bool2 = g;
        int i = 8;
        if (bool2)
        {
          h.setVisibility(0);
          localObject3 = g;
          ((CheckBox)localObject3).setVisibility(i);
        }
        else
        {
          g.setVisibility(0);
          h.setVisibility(i);
          localObject3 = g;
          ((CheckBox)localObject3).setChecked(false);
        }
        bool2 = l;
        int j;
        if (bool2)
        {
          localObject3 = f;
          localObject4 = c.getResources();
          j = R.string.manage_acc_reset_upi_pin;
          localObject4 = ((Resources)localObject4).getString(j);
          ((TextView)localObject3).setText((CharSequence)localObject4);
        }
        else
        {
          localObject3 = f;
          localObject4 = c.getResources();
          j = R.string.manage_acc_set_upi_pin;
          localObject4 = ((Resources)localObject4).getString(j);
          ((TextView)localObject3).setText((CharSequence)localObject4);
        }
        bool2 = f;
        if (bool2)
        {
          localObject3 = f;
          ((TextView)localObject3).setVisibility(0);
        }
        else
        {
          localObject3 = f;
          i = 4;
          ((TextView)localObject3).setVisibility(i);
        }
        Object localObject3 = a;
        Object localObject4 = b;
        ((TextView)localObject3).setText((CharSequence)localObject4);
        localObject3 = c;
        localObject4 = a;
        localObject2 = d;
        localObject2 = ((r)localObject4).b((String)localObject2);
        ((ImageView)localObject3).setImageDrawable((Drawable)localObject2);
        localObject2 = b;
        localObject3 = au.a(c);
        ((TextView)localObject2).setText((CharSequence)localObject3);
        localObject2 = d;
        localObject1 = d;
        ((TextView)localObject2).setText((CharSequence)localObject1);
        localObject1 = e;
        localObject2 = new com/truecaller/truepay/app/ui/accounts/views/a/-$$Lambda$a$AXwNznwSbKjP5uZGXMKHrjLz-oc;
        ((-..Lambda.a.AXwNznwSbKjP5uZGXMKHrjLz-oc)localObject2).<init>(this, paramViewHolder);
        ((ImageView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
        localObject1 = f;
        localObject2 = new com/truecaller/truepay/app/ui/accounts/views/a/-$$Lambda$a$Iqt1WfikMko6fe-9g1m3N_a-kVw;
        ((-..Lambda.a.Iqt1WfikMko6fe-9g1m3N_a-kVw)localObject2).<init>(this, paramViewHolder);
        ((TextView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
        localObject1 = g;
        localObject2 = new com/truecaller/truepay/app/ui/accounts/views/a/-$$Lambda$a$-TcyADy-TVSAXUODdUp_bS4CHpw;
        ((-..Lambda.a.-TcyADy-TVSAXUODdUp_bS4CHpw)localObject2).<init>(this, paramViewHolder);
        ((CheckBox)localObject1).setOnClickListener((View.OnClickListener)localObject2);
      }
      else {}
    }
  }
  
  public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    int i = 1;
    if (paramInt != i)
    {
      localObject = LayoutInflater.from(paramViewGroup.getContext());
      i = R.layout.list_manange_account_item;
      paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
      localObject = new com/truecaller/truepay/app/ui/accounts/views/a/a$b;
      ((a.b)localObject).<init>(this, paramViewGroup);
      return (RecyclerView.ViewHolder)localObject;
    }
    Object localObject = LayoutInflater.from(paramViewGroup.getContext());
    i = R.layout.fragment_manage_account_footer;
    paramViewGroup = ((LayoutInflater)localObject).inflate(i, paramViewGroup, false);
    localObject = new com/truecaller/truepay/app/ui/accounts/views/a/a$a;
    ((a.a)localObject).<init>(this, paramViewGroup);
    return (RecyclerView.ViewHolder)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.accounts.views.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */