package com.truecaller.truepay.app.ui.accounts.views.b;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import c.x;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.accounts.a.a.a;
import com.truecaller.truepay.app.ui.base.widgets.PinEntryEditText;
import com.truecaller.truepay.app.ui.registration.d.m;
import com.truecaller.truepay.app.ui.registration.views.c.h;
import com.truecaller.utils.extensions.p;
import com.truecaller.utils.extensions.t;

public final class c
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements h
{
  public TextView a;
  public PinEntryEditText b;
  public PinEntryEditText c;
  public PinEntryEditText d;
  public TextView e;
  public TextView f;
  public ImageView g;
  public TextInputLayout h;
  Toolbar i;
  public m j;
  public com.truecaller.truepay.data.e.e k;
  public com.truecaller.truepay.app.utils.a l;
  private com.truecaller.truepay.data.api.model.a n;
  private String o;
  private com.truecaller.truepay.app.ui.accounts.views.c.a p;
  
  public static c a(com.truecaller.truepay.data.api.model.a parama, String paramString)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putSerializable("connected_account", parama);
    localBundle.putString("analytic_context", paramString);
    parama = new com/truecaller/truepay/app/ui/accounts/views/b/c;
    parama.<init>();
    parama.setArguments(localBundle);
    return parama;
  }
  
  private void a(android.support.v4.app.e parame)
  {
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      boolean bool = isAdded();
      if (bool) {
        try
        {
          localObject1 = getActivity();
          localObject1 = ((f)localObject1).getSupportFragmentManager();
          Object localObject2 = parame.getClass();
          localObject2 = ((Class)localObject2).getSimpleName();
          parame.show((j)localObject1, (String)localObject2);
          return;
        }
        catch (Exception localException) {}
      }
    }
  }
  
  private static boolean b(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      int i1 = paramString.length();
      int m = 2;
      if (i1 == m) {
        return true;
      }
    }
    return false;
  }
  
  private static boolean c(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      int m = paramString.length();
      int i1 = 2;
      if (m == i1)
      {
        m = Integer.parseInt(paramString);
        if (m > 0)
        {
          int i2 = Integer.parseInt(paramString);
          m = 12;
          if (i2 <= m) {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  private void d()
  {
    String str1 = b.getText().toString();
    Object localObject = c.getText().toString();
    String str2 = d.getText().toString();
    boolean bool1 = d(str1);
    boolean bool2 = c((String)localObject);
    boolean bool3 = b(str2);
    if ((bool1) && (bool2) && (bool3))
    {
      m localm = j;
      String str3 = n.a;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder.append((String)localObject);
      localStringBuilder.append(str2);
      localObject = localStringBuilder.toString();
      localm.a(str3, str1, (String)localObject);
      return;
    }
    str1 = null;
    if (!bool1)
    {
      e.setVisibility(0);
      return;
    }
    if ((!bool2) || (!bool3))
    {
      localObject = f;
      ((TextView)localObject).setVisibility(0);
    }
  }
  
  private static boolean d(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      int i1 = paramString.length();
      int m = 6;
      if (i1 == m) {
        return true;
      }
    }
    return false;
  }
  
  private void e()
  {
    PinEntryEditText localPinEntryEditText = b;
    int m = 2;
    t.a(localPinEntryEditText, false, m);
    t.a(c, false, m);
    t.a(d, false, m);
    p.c();
  }
  
  public final int a()
  {
    return R.layout.fragment_set_upi_pin;
  }
  
  final x a(Editable paramEditable)
  {
    TextView localTextView = e;
    localTextView.setVisibility(8);
    int m = paramEditable.length();
    int i1 = 6;
    if (m == i1)
    {
      paramEditable = c;
      paramEditable.requestFocus();
    }
    else
    {
      int i2 = paramEditable.length();
      if (i2 == 0)
      {
        paramEditable = b;
        paramEditable.requestFocus();
      }
    }
    return x.a;
  }
  
  final x a(boolean paramBoolean)
  {
    Object localObject1;
    Object localObject2;
    int i1;
    if (paramBoolean)
    {
      h.setHint("");
      localObject1 = g;
      localObject2 = getActivity();
      int m = R.drawable.ic_six_digits;
      localObject2 = android.support.v4.content.b.a((Context)localObject2, m);
      ((ImageView)localObject1).setImageDrawable((Drawable)localObject2);
      localObject1 = e;
      i1 = 8;
      ((TextView)localObject1).setVisibility(i1);
    }
    else
    {
      localObject1 = b.getText().toString();
      paramBoolean = d((String)localObject1);
      if (!paramBoolean)
      {
        localObject1 = e;
        i1 = 0;
        localObject2 = null;
        ((TextView)localObject1).setVisibility(0);
      }
    }
    return x.a;
  }
  
  public final void a(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool)
    {
      int m = R.string.set_pin_error_body;
      paramString = getString(m);
    }
    String str = o;
    paramString = b.a(paramString, str);
    a(paramString);
  }
  
  final x b(Editable paramEditable)
  {
    TextView localTextView = f;
    localTextView.setVisibility(8);
    int m = paramEditable.length();
    int i1 = 2;
    if (m == i1)
    {
      paramEditable = d;
      paramEditable.requestFocus();
    }
    else
    {
      int i2 = paramEditable.length();
      if (i2 == 0)
      {
        paramEditable = b;
        paramEditable.requestFocus();
      }
    }
    return x.a;
  }
  
  final x b(boolean paramBoolean)
  {
    Object localObject1;
    Object localObject2;
    int i1;
    if (paramBoolean)
    {
      localObject1 = g;
      localObject2 = getActivity();
      int m = R.drawable.ic_valid_upto;
      localObject2 = android.support.v4.content.b.a((Context)localObject2, m);
      ((ImageView)localObject1).setImageDrawable((Drawable)localObject2);
      localObject1 = f;
      i1 = 8;
      ((TextView)localObject1).setVisibility(i1);
    }
    else
    {
      localObject1 = c.getText().toString();
      paramBoolean = c((String)localObject1);
      if (!paramBoolean)
      {
        localObject1 = f;
        i1 = 0;
        localObject2 = null;
        ((TextView)localObject1).setVisibility(0);
      }
    }
    return x.a;
  }
  
  public final void b()
  {
    Object localObject1 = getArguments();
    Object localObject2 = (com.truecaller.truepay.data.api.model.a)((Bundle)localObject1).getSerializable("connected_account");
    n = ((com.truecaller.truepay.data.api.model.a)localObject2);
    localObject2 = i;
    -..Lambda.c.dpDSywF0oOFz2GLaGllyq_09EHI localdpDSywF0oOFz2GLaGllyq_09EHI = new com/truecaller/truepay/app/ui/accounts/views/b/-$$Lambda$c$dpDSywF0oOFz2GLaGllyq_09EHI;
    localdpDSywF0oOFz2GLaGllyq_09EHI.<init>(this);
    ((Toolbar)localObject2).setNavigationOnClickListener(localdpDSywF0oOFz2GLaGllyq_09EHI);
    localObject1 = ((Bundle)localObject1).getString("analytic_context", "manage_account");
    o = ((String)localObject1);
    localObject1 = k;
    localObject2 = com.truecaller.truepay.data.b.a.a();
    ((com.truecaller.truepay.data.e.e)localObject1).a((String)localObject2);
    localObject1 = Truepay.getInstance().getAnalyticLoggerHelper();
    String str = o;
    ((com.truecaller.truepay.data.b.a)localObject1).a("app_payment_set_pin", "initiated", str, "set_pin");
  }
  
  final x c(Editable paramEditable)
  {
    TextView localTextView = f;
    int m = 8;
    localTextView.setVisibility(m);
    int i1 = paramEditable.length();
    if (i1 == 0)
    {
      paramEditable = c;
      paramEditable.requestFocus();
    }
    return x.a;
  }
  
  final x c(boolean paramBoolean)
  {
    Object localObject1;
    Object localObject2;
    int i1;
    if (paramBoolean)
    {
      localObject1 = g;
      localObject2 = getActivity();
      int m = R.drawable.ic_valid_upto;
      localObject2 = android.support.v4.content.b.a((Context)localObject2, m);
      ((ImageView)localObject1).setImageDrawable((Drawable)localObject2);
      localObject1 = f;
      i1 = 8;
      ((TextView)localObject1).setVisibility(i1);
    }
    else
    {
      localObject1 = d.getText().toString();
      paramBoolean = b((String)localObject1);
      if (!paramBoolean)
      {
        localObject1 = f;
        i1 = 0;
        localObject2 = null;
        ((TextView)localObject1).setVisibility(0);
      }
    }
    return x.a;
  }
  
  public final void c()
  {
    boolean bool1 = com.truecaller.truepay.app.ui.registration.a.c;
    if (bool1)
    {
      localObject1 = "retry_set_pin";
      o = ((String)localObject1);
    }
    Object localObject1 = Truepay.getInstance().getAnalyticLoggerHelper();
    Object localObject2 = "app_payment_set_pin";
    String str1 = "success";
    String str2 = o;
    String str3 = "";
    ((com.truecaller.truepay.data.b.a)localObject1).a((String)localObject2, str1, str2, str3);
    localObject1 = n;
    if (localObject1 != null)
    {
      boolean bool2 = true;
      l = bool2;
      localObject2 = l;
      ((com.truecaller.truepay.app.utils.a)localObject2).c((com.truecaller.truepay.data.api.model.a)localObject1);
    }
    localObject1 = d.a();
    a((android.support.v4.app.e)localObject1);
  }
  
  public final void d(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      p.a();
      return;
    }
    p.b();
  }
  
  public final void e(boolean paramBoolean)
  {
    a.setEnabled(paramBoolean);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    Object localObject = getActivity();
    boolean bool = localObject instanceof com.truecaller.truepay.app.ui.accounts.views.c.a;
    if (bool)
    {
      paramContext = (com.truecaller.truepay.app.ui.accounts.views.c.a)getActivity();
      p = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = paramContext.getClass();
    localStringBuilder.append(paramContext);
    localStringBuilder.append("should implement the ManageAccountsView");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.accounts.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    j.b();
  }
  
  public final void onDetach()
  {
    super.onDetach();
    p = null;
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    int m = paramMenuItem.getItemId();
    int i1 = 16908332;
    if (m == i1) {
      e();
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    int m = 112;
    if (paramInt == m) {
      d();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int m = R.id.generate_mpin;
    Object localObject1 = (TextView)paramView.findViewById(m);
    a = ((TextView)localObject1);
    m = R.id.et_dc_last_6_digits_frag_upi_mpin;
    localObject1 = (PinEntryEditText)paramView.findViewById(m);
    b = ((PinEntryEditText)localObject1);
    m = R.id.et_validity_month;
    localObject1 = (PinEntryEditText)paramView.findViewById(m);
    c = ((PinEntryEditText)localObject1);
    m = R.id.et_validity_year;
    localObject1 = (PinEntryEditText)paramView.findViewById(m);
    d = ((PinEntryEditText)localObject1);
    m = R.id.tv_error_last_6_digits;
    localObject1 = (TextView)paramView.findViewById(m);
    e = ((TextView)localObject1);
    m = R.id.tv_error_expiry_frag_upi_mpin;
    localObject1 = (TextView)paramView.findViewById(m);
    f = ((TextView)localObject1);
    m = R.id.iv_digits_expiry_helper_frag_upi_mpin;
    localObject1 = (ImageView)paramView.findViewById(m);
    g = ((ImageView)localObject1);
    m = R.id.til_dc_last_6_digits_frag_upi_mpin;
    localObject1 = (TextInputLayout)paramView.findViewById(m);
    h = ((TextInputLayout)localObject1);
    m = R.id.toolbar;
    localObject1 = (Toolbar)paramView.findViewById(m);
    i = ((Toolbar)localObject1);
    m = R.id.tv_set_default_expiry;
    localObject1 = paramView.findViewById(m);
    Object localObject2 = new com/truecaller/truepay/app/ui/accounts/views/b/-$$Lambda$c$WR3iTIhtTs1QexY3wTrIyl3PRsU;
    ((-..Lambda.c.WR3iTIhtTs1QexY3wTrIyl3PRsU)localObject2).<init>(this);
    ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    m = R.id.generate_mpin;
    localObject1 = paramView.findViewById(m);
    localObject2 = new com/truecaller/truepay/app/ui/accounts/views/b/-$$Lambda$c$u_5Pcr_AJIpRv3NTnMivAsQpZNY;
    ((-..Lambda.c.u_5Pcr_AJIpRv3NTnMivAsQpZNY)localObject2).<init>(this);
    ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = b;
    localObject2 = new com/truecaller/truepay/app/ui/accounts/views/b/-$$Lambda$fe0PU-43XuZLuSB0GSy13HV8n0M;
    ((-..Lambda.fe0PU-43XuZLuSB0GSy13HV8n0M)localObject2).<init>(this);
    p.a((TextView)localObject1, (c.g.a.b)localObject2);
    localObject1 = c;
    localObject2 = new com/truecaller/truepay/app/ui/accounts/views/b/-$$Lambda$6SmZVRj95wZduetaSeGqKaj_Mvc;
    ((-..Lambda.6SmZVRj95wZduetaSeGqKaj_Mvc)localObject2).<init>(this);
    p.a((TextView)localObject1, (c.g.a.b)localObject2);
    localObject1 = d;
    localObject2 = new com/truecaller/truepay/app/ui/accounts/views/b/-$$Lambda$gLZBBxWjid6243tpqwtQZ2V2HUs;
    ((-..Lambda.gLZBBxWjid6243tpqwtQZ2V2HUs)localObject2).<init>(this);
    p.a((TextView)localObject1, (c.g.a.b)localObject2);
    localObject1 = b;
    localObject2 = new com/truecaller/truepay/app/ui/accounts/views/b/-$$Lambda$BeEWxhMs2PaCZmHTbB8W_Cd014o;
    ((-..Lambda.BeEWxhMs2PaCZmHTbB8W_Cd014o)localObject2).<init>(this);
    t.a((View)localObject1, (c.g.a.b)localObject2);
    localObject1 = c;
    localObject2 = new com/truecaller/truepay/app/ui/accounts/views/b/-$$Lambda$EoTe_huzODdMxIgr8ypuajy3y5I;
    ((-..Lambda.EoTe_huzODdMxIgr8ypuajy3y5I)localObject2).<init>(this);
    t.a((View)localObject1, (c.g.a.b)localObject2);
    localObject1 = d;
    localObject2 = new com/truecaller/truepay/app/ui/accounts/views/b/-$$Lambda$JEb4sSVFicrZZ150SHYBuNRYVJU;
    ((-..Lambda.JEb4sSVFicrZZ150SHYBuNRYVJU)localObject2).<init>(this);
    t.a((View)localObject1, (c.g.a.b)localObject2);
    localObject1 = (AppCompatActivity)getActivity();
    localObject2 = i;
    ((AppCompatActivity)localObject1).setSupportActionBar((Toolbar)localObject2);
    localObject2 = ((AppCompatActivity)localObject1).getSupportActionBar();
    int i1 = R.string.reset_pin_header;
    ((ActionBar)localObject2).setTitle(i1);
    localObject2 = ((AppCompatActivity)localObject1).getSupportActionBar();
    i1 = 1;
    ((ActionBar)localObject2).setDisplayShowTitleEnabled(i1);
    localObject2 = ((AppCompatActivity)localObject1).getSupportActionBar();
    int i2 = R.drawable.ic_close_48_px;
    ((ActionBar)localObject2).setHomeAsUpIndicator(i2);
    ((AppCompatActivity)localObject1).getSupportActionBar().setDisplayHomeAsUpEnabled(i1);
    com.truecaller.truepay.app.ui.registration.a.c = false;
    j.a(this);
    j.a();
    super.onViewCreated(paramView, paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.accounts.views.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */