package com.truecaller.truepay.app.ui.accounts.views.b;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.e;
import android.support.v4.app.f;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;

public final class b
  extends e
{
  TextView a;
  TextView b;
  Button c;
  TextView d;
  private com.truecaller.truepay.app.ui.accounts.views.c.a e;
  private String f;
  
  public static b a(String paramString1, String paramString2)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("error_text", paramString1);
    localBundle.putString("analytic_context", paramString2);
    paramString1 = new com/truecaller/truepay/app/ui/accounts/views/b/b;
    paramString1.<init>();
    paramString1.setArguments(localBundle);
    return paramString1;
  }
  
  private void a(String paramString)
  {
    boolean bool = com.truecaller.truepay.app.ui.registration.a.c;
    if (bool)
    {
      localObject = "retry_set_pin";
      f = ((String)localObject);
    }
    com.truecaller.truepay.app.ui.registration.a.c = true;
    Object localObject = Truepay.getInstance().getAnalyticLoggerHelper();
    String str = f;
    ((com.truecaller.truepay.data.b.a)localObject).a("app_payment_set_pin", "failure", str, paramString);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    Object localObject = getActivity();
    boolean bool = localObject instanceof com.truecaller.truepay.app.ui.accounts.views.c.a;
    if (bool)
    {
      paramContext = (com.truecaller.truepay.app.ui.accounts.views.c.a)getActivity();
      e = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = paramContext.getClass();
    localStringBuilder.append(paramContext);
    localStringBuilder.append("should implement the ManageAccountInteractionListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = getActivity().getLayoutInflater();
    int i = R.layout.fragment_upi_pin_failure;
    paramBundle = paramBundle.inflate(i, null);
    i = R.id.tv_error_header;
    Object localObject1 = (TextView)paramBundle.findViewById(i);
    a = ((TextView)localObject1);
    i = R.id.tv_error_body;
    localObject1 = (TextView)paramBundle.findViewById(i);
    b = ((TextView)localObject1);
    i = R.id.btn_retry;
    localObject1 = (Button)paramBundle.findViewById(i);
    c = ((Button)localObject1);
    i = R.id.btn_do_it_later;
    localObject1 = (TextView)paramBundle.findViewById(i);
    d = ((TextView)localObject1);
    localObject1 = c;
    Object localObject2 = new com/truecaller/truepay/app/ui/accounts/views/b/-$$Lambda$b$dWkYOZRyuiv960s51go4bJecK6M;
    ((-..Lambda.b.dWkYOZRyuiv960s51go4bJecK6M)localObject2).<init>(this);
    ((Button)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = d;
    localObject2 = new com/truecaller/truepay/app/ui/accounts/views/b/-$$Lambda$b$KFIf88UkZRW-8aP2KUizteeERvg;
    ((-..Lambda.b.KFIf88UkZRW-8aP2KUizteeERvg)localObject2).<init>(this);
    ((TextView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = new android/app/AlertDialog$Builder;
    localObject2 = new android/view/ContextThemeWrapper;
    f localf = getActivity();
    int j = R.style.popup_theme;
    ((ContextThemeWrapper)localObject2).<init>(localf, j);
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    localObject1 = ((AlertDialog.Builder)localObject1).create();
    localObject2 = ((AlertDialog)localObject1).getWindow();
    if (localObject2 != null)
    {
      localObject2 = ((AlertDialog)localObject1).getWindow();
      int k = 80;
      ((Window)localObject2).setGravity(k);
    }
    ((AlertDialog)localObject1).setCanceledOnTouchOutside(true);
    ((AlertDialog)localObject1).setView(paramBundle);
    paramBundle = b;
    localObject2 = getArguments().getString("error_text");
    paramBundle.setText((CharSequence)localObject2);
    paramBundle = getArguments().getString("analytic_context");
    f = paramBundle;
    return (Dialog)localObject1;
  }
  
  public final void onDetach()
  {
    super.onDetach();
    e = null;
  }
  
  public final void onStart()
  {
    super.onStart();
    Window localWindow = getDialog().getWindow();
    if (localWindow != null)
    {
      localWindow.setBackgroundDrawableResource(17170445);
      int i = -1;
      int j = -2;
      localWindow.setLayout(i, j);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.accounts.views.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */