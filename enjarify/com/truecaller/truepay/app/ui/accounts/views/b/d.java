package com.truecaller.truepay.app.ui.accounts.views.b;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.e;
import android.support.v4.app.f;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Window;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.app.ui.accounts.views.c.a;

public class d
  extends e
{
  private a a;
  private Handler b;
  private Runnable c;
  
  public d()
  {
    d.1 local1 = new com/truecaller/truepay/app/ui/accounts/views/b/d$1;
    local1.<init>(this);
    c = local1;
  }
  
  public static d a()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    d locald = new com/truecaller/truepay/app/ui/accounts/views/b/d;
    locald.<init>();
    locald.setArguments(localBundle);
    return locald;
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    Object localObject = getActivity();
    boolean bool = localObject instanceof a;
    if (bool)
    {
      paramContext = (a)getActivity();
      a = paramContext;
      return;
    }
    localObject = new java/lang/RuntimeException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = paramContext.getClass();
    localStringBuilder.append(paramContext);
    localStringBuilder.append("should implement the ManageAccountInteractionListener");
    paramContext = localStringBuilder.toString();
    ((RuntimeException)localObject).<init>(paramContext);
    throw ((Throwable)localObject);
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = getActivity().getLayoutInflater();
    int i = R.layout.fragment_set_upi_pin_success;
    paramBundle = paramBundle.inflate(i, null);
    Object localObject1 = new android/app/AlertDialog$Builder;
    Object localObject2 = new android/view/ContextThemeWrapper;
    f localf = getActivity();
    int j = R.style.popup_theme;
    ((ContextThemeWrapper)localObject2).<init>(localf, j);
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    localObject1 = ((AlertDialog.Builder)localObject1).create();
    localObject2 = ((AlertDialog)localObject1).getWindow();
    if (localObject2 != null)
    {
      localObject2 = ((AlertDialog)localObject1).getWindow();
      int k = 80;
      ((Window)localObject2).setGravity(k);
    }
    ((AlertDialog)localObject1).setView(paramBundle);
    paramBundle = new android/os/Handler;
    paramBundle.<init>();
    b = paramBundle;
    paramBundle = b;
    localObject2 = c;
    paramBundle.postDelayed((Runnable)localObject2, 2000L);
    return (Dialog)localObject1;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    Handler localHandler = b;
    Runnable localRunnable = c;
    localHandler.removeCallbacks(localRunnable);
  }
  
  public void onDetach()
  {
    super.onDetach();
    a = null;
  }
  
  public void onStart()
  {
    super.onStart();
    Window localWindow = getDialog().getWindow();
    if (localWindow != null)
    {
      localWindow.setBackgroundDrawableResource(17170445);
      int i = -1;
      int j = -2;
      localWindow.setLayout(i, j);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.accounts.views.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */