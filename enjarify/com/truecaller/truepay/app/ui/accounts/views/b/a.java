package com.truecaller.truepay.app.ui.accounts.views.b;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.accounts.a.a.a;
import com.truecaller.truepay.app.ui.accounts.views.a.a.c;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.utils.i;
import com.truecaller.utils.n;
import java.util.ArrayList;

public final class a
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements a.c, com.truecaller.truepay.app.ui.accounts.views.c.c
{
  public n a;
  public com.truecaller.truepay.app.utils.a b;
  public com.truecaller.truepay.app.ui.accounts.c.c c;
  public r d;
  public au e;
  public i f;
  public Context g;
  RecyclerView h;
  Toolbar i;
  View j;
  private com.truecaller.truepay.app.ui.accounts.views.a.a k;
  private com.truecaller.truepay.app.ui.accounts.views.c.a l;
  
  public static a b()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    a locala = new com/truecaller/truepay/app/ui/accounts/views/b/a;
    locala.<init>();
    locala.setArguments(localBundle);
    return locala;
  }
  
  private void e()
  {
    Object localObject = getContext();
    if (localObject != null)
    {
      localObject = new android/content/Intent;
      ((Intent)localObject).<init>("ACCOUNTS_CHANGED");
      android.support.v4.content.d locald = android.support.v4.content.d.a(getContext());
      locald.a((Intent)localObject);
    }
  }
  
  public final int a()
  {
    return R.layout.fragment_manage_accounts;
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    j.setVisibility(paramInt1);
    Object localObject = j;
    int m = R.id.psp_logo;
    localObject = (ImageView)((View)localObject).findViewById(m);
    Drawable localDrawable = getResources().getDrawable(paramInt2);
    ((ImageView)localObject).setImageDrawable(localDrawable);
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    parama = a;
    c.a(parama);
  }
  
  public final void a(String paramString)
  {
    a(paramString, null);
  }
  
  public final void a(Throwable paramThrowable)
  {
    Object localObject = a;
    int m = R.string.error_change_pin_message;
    Object[] arrayOfObject = new Object[0];
    localObject = ((n)localObject).a(m, arrayOfObject);
    a((String)localObject, paramThrowable);
  }
  
  public final void a(boolean paramBoolean)
  {
    com.truecaller.truepay.app.ui.accounts.views.c.a locala = l;
    if (locala != null)
    {
      if (paramBoolean)
      {
        locala.a();
        return;
      }
      locala.b();
    }
  }
  
  public final void b(com.truecaller.truepay.data.api.model.a parama)
  {
    c.a(parama);
  }
  
  public final void b(String paramString)
  {
    a(paramString, null);
  }
  
  public final void b(Throwable paramThrowable)
  {
    Object localObject = a;
    int m = R.string.error_delete_account;
    Object[] arrayOfObject = new Object[0];
    localObject = ((n)localObject).a(m, arrayOfObject);
    a((String)localObject, paramThrowable);
  }
  
  public final void c()
  {
    Object localObject1 = h;
    int m = 1;
    ((RecyclerView)localObject1).setHasFixedSize(m);
    localObject1 = h;
    Object localObject2 = new android/support/v7/widget/LinearLayoutManager;
    f localf = getActivity();
    ((LinearLayoutManager)localObject2).<init>(localf, m, false);
    ((RecyclerView)localObject1).setLayoutManager((RecyclerView.LayoutManager)localObject2);
    localObject1 = new com/truecaller/truepay/app/ui/accounts/views/a/a;
    Context localContext = g;
    ArrayList localArrayList = b.a();
    r localr = d;
    au localau = e;
    ((com.truecaller.truepay.app.ui.accounts.views.a.a)localObject1).<init>(localContext, localArrayList, localr, localau, this);
    k = ((com.truecaller.truepay.app.ui.accounts.views.a.a)localObject1);
    localObject1 = new android/support/v7/widget/DividerItemDecoration;
    localObject2 = g;
    ((DividerItemDecoration)localObject1).<init>((Context)localObject2, m);
    Object localObject3 = g;
    int n = R.drawable.divider_history;
    localObject3 = android.support.v4.content.b.a((Context)localObject3, n);
    ((DividerItemDecoration)localObject1).setDrawable((Drawable)localObject3);
    h.addItemDecoration((RecyclerView.ItemDecoration)localObject1);
    localObject1 = h;
    localObject3 = k;
    ((RecyclerView)localObject1).setAdapter((RecyclerView.Adapter)localObject3);
  }
  
  public final void c(com.truecaller.truepay.data.api.model.a parama)
  {
    com.truecaller.truepay.app.ui.accounts.views.c.a locala = l;
    if (locala != null) {
      locala.a(parama);
    }
  }
  
  public final void c(String paramString)
  {
    com.truecaller.truepay.app.ui.accounts.views.a.a locala = k;
    ArrayList localArrayList = b.a();
    locala.a(localArrayList);
    a(paramString, null);
    e();
  }
  
  public final void c(Throwable paramThrowable)
  {
    Object localObject = a;
    int m = R.string.error_primary_acc_message;
    Object[] arrayOfObject = new Object[0];
    localObject = ((n)localObject).a(m, arrayOfObject);
    a((String)localObject, paramThrowable);
  }
  
  public final void d()
  {
    d locald = d.a();
    j localj = getActivity().getSupportFragmentManager();
    String str = d.class.getSimpleName();
    locald.show(localj, str);
  }
  
  public final void d(com.truecaller.truepay.data.api.model.a parama)
  {
    c.b(parama);
  }
  
  public final void d(String paramString)
  {
    a(paramString, null);
  }
  
  public final void e(com.truecaller.truepay.data.api.model.a parama)
  {
    Object localObject1 = (AppCompatActivity)getActivity();
    if (localObject1 != null)
    {
      Object localObject2 = i;
      ((AppCompatActivity)localObject1).setSupportActionBar((Toolbar)localObject2);
      localObject2 = ((AppCompatActivity)localObject1).getSupportActionBar();
      int m = R.string.accounts_header;
      ((ActionBar)localObject2).setTitle(m);
      localObject2 = ((AppCompatActivity)localObject1).getSupportActionBar();
      m = 1;
      ((ActionBar)localObject2).setDisplayShowTitleEnabled(m);
      localObject2 = ((AppCompatActivity)localObject1).getSupportActionBar();
      ((ActionBar)localObject2).setDisplayHomeAsUpEnabled(m);
      localObject1 = ((AppCompatActivity)localObject1).getSupportActionBar();
      int n = R.string.upi_id_manage_account;
      Object[] arrayOfObject = new Object[m];
      parama = h;
      arrayOfObject[0] = parama;
      parama = getString(n, arrayOfObject);
      ((ActionBar)localObject1).setSubtitle(parama);
      parama = i;
      localObject1 = new com/truecaller/truepay/app/ui/accounts/views/b/-$$Lambda$a$EY9GFu2KAee0JEPLZo7Hr7gvrUA;
      ((-..Lambda.a.EY9GFu2KAee0JEPLZo7Hr7gvrUA)localObject1).<init>(this);
      parama.setNavigationOnClickListener((View.OnClickListener)localObject1);
    }
  }
  
  public final void e(String paramString)
  {
    com.truecaller.truepay.app.ui.accounts.views.a.a locala = k;
    ArrayList localArrayList = b.a();
    locala.a(localArrayList);
    a(paramString, null);
    e();
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getActivity();
    boolean bool = paramContext instanceof com.truecaller.truepay.app.ui.accounts.views.c.a;
    if (bool)
    {
      paramContext = (com.truecaller.truepay.app.ui.accounts.views.c.a)getActivity();
      l = paramContext;
      return;
    }
    paramContext = new java/lang/IllegalStateException;
    paramContext.<init>("Activity should implement ManageAccountInteractionListener");
    throw paramContext;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.accounts.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    c.b();
  }
  
  public final void onDetach()
  {
    super.onDetach();
    l = null;
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int m = R.id.rv_acc_list_frag_manage_account;
    paramBundle = (RecyclerView)paramView.findViewById(m);
    h = paramBundle;
    m = R.id.toolbar;
    paramBundle = (Toolbar)paramView.findViewById(m);
    i = paramBundle;
    m = R.id.lv_footer_frag_manage_account;
    paramBundle = paramView.findViewById(m);
    j = paramBundle;
    m = R.id.btn_add_accounts_frag_manage_accounts;
    paramView = paramView.findViewById(m);
    paramBundle = new com/truecaller/truepay/app/ui/accounts/views/b/-$$Lambda$a$dV5ueVCdID_3KTYSkGSxh5P3s78;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    c.a(this);
    c.a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.accounts.views.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */