package com.truecaller.truepay.app.ui.accounts.views.a;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.truecaller.truepay.R.id;

final class a$b
  extends RecyclerView.ViewHolder
{
  final TextView a;
  final TextView b;
  final ImageView c;
  final TextView d;
  final ImageView e;
  final TextView f;
  final CheckBox g;
  final LinearLayout h;
  
  a$b(a parama, View paramView)
  {
    super(paramView);
    int j = R.id.tv_bank_name_item_manage_accounts;
    parama = (TextView)paramView.findViewById(j);
    a = parama;
    j = R.id.tv_account_number_item_manage_accounts;
    parama = (TextView)paramView.findViewById(j);
    b = parama;
    j = R.id.iv_bank_logo_item_manage_accounts;
    parama = (ImageView)paramView.findViewById(j);
    c = parama;
    j = R.id.tv_ifsc_item_manage_accounts;
    parama = (TextView)paramView.findViewById(j);
    d = parama;
    j = R.id.iv_more_logo_item_manage_accounts;
    parama = (ImageView)paramView.findViewById(j);
    e = parama;
    j = R.id.tv_reset_pin_item_manage_accounts;
    parama = (TextView)paramView.findViewById(j);
    f = parama;
    j = R.id.set_as_primary_layout;
    parama = (CheckBox)paramView.findViewById(j);
    g = parama;
    j = R.id.primary_layout;
    parama = (LinearLayout)paramView.findViewById(j);
    h = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.accounts.views.a.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */