package com.truecaller.truepay.app.ui.accounts.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.view.View;
import android.widget.FrameLayout;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.accounts.a.a.a;
import com.truecaller.utils.extensions.t;

public class ManageAccountsActivity
  extends com.truecaller.truepay.app.ui.base.views.a.b
  implements com.truecaller.truepay.app.ui.accounts.views.c.a, com.truecaller.truepay.app.ui.accounts.views.c.b
{
  FrameLayout a;
  public com.truecaller.truepay.app.ui.accounts.c.a b;
  private boolean c;
  
  public static void a(Context paramContext, String paramString1, com.truecaller.truepay.data.api.model.a parama, String paramString2)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, ManageAccountsActivity.class);
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putSerializable("account", parama);
    localBundle.putString("action", paramString1);
    localBundle.putString("analytic_context", paramString2);
    localIntent.putExtras(localBundle);
    paramContext.startActivity(localIntent);
  }
  
  public final void a()
  {
    boolean bool = isFinishing();
    if (!bool)
    {
      bool = true;
      c = bool;
      a.setVisibility(0);
      a.setClickable(bool);
      Object localObject = getSupportFragmentManager();
      com.truecaller.truepay.app.ui.registration.views.a locala = new com/truecaller/truepay/app/ui/registration/views/a;
      locala.<init>();
      localObject = ((j)localObject).a();
      int i = R.id.overlay_progress_frame;
      localObject = ((o)localObject).b(i, locala);
      ((o)localObject).d();
    }
  }
  
  public final void a(Fragment paramFragment)
  {
    try
    {
      Object localObject1 = getSupportFragmentManager();
      localObject1 = ((j)localObject1).a();
      Object localObject2 = paramFragment.getClass();
      localObject2 = ((Class)localObject2).getName();
      ((o)localObject1).a((String)localObject2);
      int i = R.id.accounts_container;
      Object localObject3 = getSupportFragmentManager();
      int j = ((j)localObject3).e();
      localObject3 = Integer.toString(j);
      paramFragment = ((o)localObject1).a(i, paramFragment, (String)localObject3);
      paramFragment.d();
      return;
    }
    catch (Exception localException) {}
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    b.a(parama);
  }
  
  public final void b()
  {
    boolean bool = isFinishing();
    if (!bool)
    {
      bool = false;
      c = false;
      FrameLayout localFrameLayout = a;
      int i = 8;
      localFrameLayout.setVisibility(i);
    }
  }
  
  public final void c()
  {
    b.a();
  }
  
  public final int d()
  {
    return getSupportFragmentManager().e();
  }
  
  public final void e()
  {
    finish();
  }
  
  public final boolean f()
  {
    return c;
  }
  
  public int getLayoutId()
  {
    return R.layout.activity_manage_account;
  }
  
  public void initDagger(com.truecaller.truepay.app.a.a.a parama)
  {
    parama = com.truecaller.truepay.app.ui.accounts.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    parama.a(locala).a().a(this);
  }
  
  public void onBackPressed()
  {
    Object localObject = a;
    int i = 2;
    t.a((View)localObject, false, i);
    localObject = getSupportFragmentManager();
    int j = ((j)localObject).e();
    int k = 1;
    if (j != k)
    {
      super.onBackPressed();
      return;
    }
    finish();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getIntent();
    int i = R.id.overlay_progress_frame;
    Object localObject = (FrameLayout)findViewById(i);
    a = ((FrameLayout)localObject);
    if (paramBundle != null)
    {
      localObject = paramBundle.getStringExtra("action");
      com.truecaller.truepay.data.api.model.a locala = (com.truecaller.truepay.data.api.model.a)paramBundle.getSerializableExtra("account");
      paramBundle = paramBundle.getStringExtra("analytic_context");
      b.a(this);
      com.truecaller.truepay.app.ui.accounts.c.a locala1 = b;
      locala1.a((String)localObject, locala, paramBundle);
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    b.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.accounts.views.activities.ManageAccountsActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */