package com.truecaller.truepay.app.ui.accounts.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.truecaller.truepay.a.a.a.d;
import com.truecaller.truepay.a.a.a.f;
import com.truecaller.truepay.a.a.a.h;
import com.truecaller.truepay.a.a.e.q;
import com.truecaller.truepay.app.ui.accounts.views.activities.ManageAccountsActivity;
import com.truecaller.truepay.app.utils.au;
import com.truecaller.truepay.app.utils.u;
import com.truecaller.truepay.data.a.j;
import com.truecaller.truepay.data.a.l;
import com.truecaller.truepay.data.f.ad;
import com.truecaller.truepay.data.f.af;
import com.truecaller.utils.i;
import javax.inject.Provider;

public final class a
  implements b
{
  private final com.truecaller.truepay.app.a.a.a a;
  private final j b;
  private Provider c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  private Provider o;
  private Provider p;
  private Provider q;
  
  private a(com.truecaller.truepay.app.ui.accounts.a.a.a parama, j paramj, com.truecaller.truepay.app.a.a.a parama1)
  {
    a = parama1;
    b = paramj;
    Object localObject = dagger.a.c.a(com.truecaller.truepay.data.a.k.a(paramj));
    c = ((Provider)localObject);
    localObject = new com/truecaller/truepay/app/ui/accounts/a/a$g;
    ((a.g)localObject).<init>(parama1);
    d = ((Provider)localObject);
    localObject = d;
    paramj = dagger.a.c.a(l.a(paramj, (Provider)localObject));
    e = paramj;
    paramj = c;
    localObject = e;
    paramj = com.truecaller.truepay.data.f.k.a(paramj, (Provider)localObject);
    f = paramj;
    paramj = f.a(f);
    g = paramj;
    paramj = com.truecaller.truepay.a.a.a.b.a(f);
    h = paramj;
    paramj = new com/truecaller/truepay/app/ui/accounts/a/a$c;
    paramj.<init>(parama1);
    i = paramj;
    paramj = d.a(i);
    j = paramj;
    paramj = h.a(i);
    k = paramj;
    paramj = new com/truecaller/truepay/app/ui/accounts/a/a$d;
    paramj.<init>(parama1);
    l = paramj;
    paramj = new com/truecaller/truepay/app/ui/accounts/a/a$b;
    paramj.<init>(parama1);
    m = paramj;
    paramj = new com/truecaller/truepay/app/ui/accounts/a/a$f;
    paramj.<init>(parama1);
    n = paramj;
    paramj = new com/truecaller/truepay/app/ui/accounts/a/a$e;
    paramj.<init>(parama1);
    o = paramj;
    Provider localProvider1 = g;
    Provider localProvider2 = h;
    Provider localProvider3 = j;
    Provider localProvider4 = k;
    Provider localProvider5 = l;
    Provider localProvider6 = m;
    Provider localProvider7 = n;
    Provider localProvider8 = o;
    localObject = parama;
    paramj = dagger.a.c.a(com.truecaller.truepay.app.ui.accounts.a.a.c.a(parama, localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6, localProvider7, localProvider8));
    p = paramj;
    parama = dagger.a.c.a(com.truecaller.truepay.app.ui.accounts.a.a.b.a(parama));
    q = parama;
  }
  
  public static a.a a()
  {
    a.a locala = new com/truecaller/truepay/app/ui/accounts/a/a$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  private ad b()
  {
    ad localad = new com/truecaller/truepay/data/f/ad;
    af localaf1 = com.truecaller.truepay.data.a.m.a();
    af localaf2 = com.truecaller.truepay.data.a.n.a((com.truecaller.truepay.data.api.g)dagger.a.g.a(a.p(), "Cannot return null from a non-@Nullable component method"));
    localad.<init>(localaf1, localaf2);
    return localad;
  }
  
  public final void a(ManageAccountsActivity paramManageAccountsActivity)
  {
    Object localObject = (com.truecaller.featuretoggles.e)dagger.a.g.a(a.u(), "Cannot return null from a non-@Nullable component method");
    featuresRegistry = ((com.truecaller.featuretoggles.e)localObject);
    localObject = (u)dagger.a.g.a(a.x(), "Cannot return null from a non-@Nullable component method");
    instantRewardHandler = ((u)localObject);
    localObject = (com.truecaller.truepay.app.ui.accounts.c.a)q.get();
    b = ((com.truecaller.truepay.app.ui.accounts.c.a)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.accounts.views.b.a parama)
  {
    Object localObject = (com.truecaller.utils.n)dagger.a.g.a(a.af(), "Cannot return null from a non-@Nullable component method");
    a = ((com.truecaller.utils.n)localObject);
    localObject = (com.truecaller.truepay.app.utils.a)dagger.a.g.a(a.h(), "Cannot return null from a non-@Nullable component method");
    b = ((com.truecaller.truepay.app.utils.a)localObject);
    localObject = (com.truecaller.truepay.app.ui.accounts.c.c)p.get();
    c = ((com.truecaller.truepay.app.ui.accounts.c.c)localObject);
    localObject = (com.truecaller.truepay.app.utils.r)dagger.a.g.a(a.b(), "Cannot return null from a non-@Nullable component method");
    d = ((com.truecaller.truepay.app.utils.r)localObject);
    localObject = (au)dagger.a.g.a(a.k(), "Cannot return null from a non-@Nullable component method");
    e = ((au)localObject);
    localObject = (i)dagger.a.g.a(a.ai(), "Cannot return null from a non-@Nullable component method");
    f = ((i)localObject);
    localObject = (Context)dagger.a.g.a(a.a(), "Cannot return null from a non-@Nullable component method");
    g = ((Context)localObject);
  }
  
  public final void a(com.truecaller.truepay.app.ui.accounts.views.b.c paramc)
  {
    Object localObject1 = com.truecaller.truepay.app.ui.registration.d.n.a();
    Object localObject2 = new com/truecaller/truepay/a/a/e/q;
    Object localObject3 = b();
    ((q)localObject2).<init>((ad)localObject3);
    a = ((q)localObject2);
    localObject2 = new com/truecaller/truepay/a/a/e/r;
    localObject3 = b();
    ((com.truecaller.truepay.a.a.e.r)localObject2).<init>((ad)localObject3);
    b = ((com.truecaller.truepay.a.a.e.r)localObject2);
    localObject2 = new com/truecaller/truepay/a/a/e/b;
    localObject3 = b();
    ((com.truecaller.truepay.a.a.e.b)localObject2).<init>((ad)localObject3);
    c = ((com.truecaller.truepay.a.a.e.b)localObject2);
    localObject2 = (com.truecaller.truepay.app.ui.npci.e)dagger.a.g.a(a.as(), "Cannot return null from a non-@Nullable component method");
    f = ((com.truecaller.truepay.app.ui.npci.e)localObject2);
    localObject2 = new com/truecaller/truepay/data/e/c;
    localObject3 = (SharedPreferences)dagger.a.g.a(a.aa(), "Cannot return null from a non-@Nullable component method");
    ((com.truecaller.truepay.data.e.c)localObject2).<init>((SharedPreferences)localObject3);
    g = ((com.truecaller.truepay.data.e.c)localObject2);
    j = ((com.truecaller.truepay.app.ui.registration.d.m)localObject1);
    localObject1 = (com.truecaller.truepay.data.e.e)dagger.a.g.a(a.O(), "Cannot return null from a non-@Nullable component method");
    k = ((com.truecaller.truepay.data.e.e)localObject1);
    localObject1 = (com.truecaller.truepay.app.utils.a)dagger.a.g.a(a.h(), "Cannot return null from a non-@Nullable component method");
    l = ((com.truecaller.truepay.app.utils.a)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.accounts.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */