package com.truecaller.truepay.app.ui.accounts.c;

import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.a.a.a.g;
import com.truecaller.truepay.data.f.j;
import io.reactivex.n;
import io.reactivex.o;
import io.reactivex.q;
import java.util.ArrayList;

public final class d
  extends com.truecaller.truepay.app.ui.base.a.a
  implements c
{
  com.truecaller.truepay.a.a.a.a a;
  com.truecaller.truepay.app.ui.npci.e b;
  com.truecaller.truepay.app.utils.a c;
  private com.truecaller.truepay.a.a.a.e f;
  private com.truecaller.truepay.a.a.a.c g;
  private g h;
  private com.truecaller.truepay.data.e.e i;
  private final com.truecaller.featuretoggles.e j;
  
  public d(com.truecaller.truepay.a.a.a.e parame, com.truecaller.truepay.a.a.a.a parama, com.truecaller.truepay.a.a.a.c paramc, g paramg, com.truecaller.truepay.app.ui.npci.e parame1, com.truecaller.truepay.app.utils.a parama1, com.truecaller.truepay.data.e.e parame2, com.truecaller.featuretoggles.e parame3)
  {
    f = parame;
    a = parama;
    g = paramc;
    h = paramg;
    b = parame1;
    c = parama1;
    i = parame2;
    j = parame3;
  }
  
  public final void a()
  {
    com.truecaller.truepay.app.ui.accounts.views.c.c localc = (com.truecaller.truepay.app.ui.accounts.views.c.c)d;
    if (localc != null)
    {
      localc.c();
      com.truecaller.truepay.data.api.model.a locala = c.c();
      if (locala != null)
      {
        locala = c.c();
        localc.e(locala);
      }
      int k = R.drawable.icici_logo;
      ArrayList localArrayList = c.a();
      int m = localArrayList.size();
      int n = 2;
      int i1 = 0;
      String str1 = null;
      if (m > n)
      {
        m = 8;
      }
      else
      {
        m = 0;
        localArrayList = null;
      }
      String str2 = i.a();
      if (str2 == null) {
        str2 = "icici";
      }
      com.truecaller.featuretoggles.b localb = j.q();
      boolean bool2 = localb.a();
      if (bool2)
      {
        int i2 = -1;
        int i3 = str2.hashCode();
        int i4 = -1396222919;
        boolean bool1;
        if (i3 != i4)
        {
          i4 = 100023093;
          if (i3 == i4)
          {
            String str3 = "icici";
            bool1 = str2.equals(str3);
            if (bool1) {
              break label218;
            }
          }
        }
        else
        {
          str1 = "baroda";
          bool1 = str2.equals(str1);
          if (bool1)
          {
            i1 = 1;
            break label218;
          }
        }
        i1 = -1;
        switch (i1)
        {
        default: 
          break;
        case 1: 
          k = R.drawable.bob_logo;
          break;
        case 0: 
          label218:
          k = R.drawable.icici_logo;
        }
      }
      localc.a(m, k);
    }
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    Object localObject1 = new com/truecaller/truepay/app/ui/accounts/b/b;
    Object localObject2 = a;
    ((com.truecaller.truepay.app.ui.accounts.b.b)localObject1).<init>((String)localObject2);
    localObject1 = g.a.a((com.truecaller.truepay.app.ui.accounts.b.b)localObject1);
    localObject2 = io.reactivex.g.a.b();
    localObject1 = ((o)localObject1).b((n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((o)localObject1).a((n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/accounts/c/d$4;
    ((d.4)localObject2).<init>(this, parama);
    ((o)localObject1).a((q)localObject2);
  }
  
  public final void a(String paramString)
  {
    Object localObject1 = new com/truecaller/truepay/app/ui/accounts/b/d;
    ((com.truecaller.truepay.app.ui.accounts.b.d)localObject1).<init>(paramString);
    localObject1 = f.a.a((com.truecaller.truepay.app.ui.accounts.b.d)localObject1);
    Object localObject2 = io.reactivex.g.a.b();
    localObject1 = ((o)localObject1).b((n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((o)localObject1).a((n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/accounts/c/d$1;
    ((d.1)localObject2).<init>(this, paramString);
    ((o)localObject1).a((q)localObject2);
  }
  
  public final void b(com.truecaller.truepay.data.api.model.a parama)
  {
    Object localObject1 = new com/truecaller/truepay/app/ui/accounts/b/e;
    Object localObject2 = a;
    ((com.truecaller.truepay.app.ui.accounts.b.e)localObject1).<init>((String)localObject2);
    localObject1 = h.a.a((com.truecaller.truepay.app.ui.accounts.b.e)localObject1);
    localObject2 = io.reactivex.g.a.b();
    localObject1 = ((o)localObject1).b((n)localObject2);
    localObject2 = io.reactivex.android.b.a.a();
    localObject1 = ((o)localObject1).a((n)localObject2);
    localObject2 = new com/truecaller/truepay/app/ui/accounts/c/d$5;
    ((d.5)localObject2).<init>(this, parama);
    ((o)localObject1).a((q)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.accounts.c.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */