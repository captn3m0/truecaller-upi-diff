package com.truecaller.truepay.app.ui.accounts.c;

import com.truecaller.truepay.app.ui.accounts.views.b.c;
import java.util.Objects;

public final class b
  extends com.truecaller.truepay.app.ui.base.a.a
  implements a
{
  public final void a()
  {
    com.truecaller.truepay.app.ui.accounts.views.c.b localb = (com.truecaller.truepay.app.ui.accounts.views.c.b)d;
    boolean bool = localb.f();
    if (bool) {
      return;
    }
    int i = localb.d();
    int j = 1;
    if (i != j)
    {
      localb.onBackPressed();
      return;
    }
    localb.e();
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    com.truecaller.truepay.app.ui.accounts.views.c.b localb = (com.truecaller.truepay.app.ui.accounts.views.c.b)d;
    parama = c.a(parama, "manage_account");
    localb.a(parama);
  }
  
  public final void a(String paramString1, com.truecaller.truepay.data.api.model.a parama, String paramString2)
  {
    if (paramString1 == null)
    {
      paramString1 = (com.truecaller.truepay.app.ui.accounts.views.c.b)d;
      parama = com.truecaller.truepay.app.ui.accounts.views.b.a.b();
      paramString1.a(parama);
      return;
    }
    String str = "action.page.reset_upi_pin";
    boolean bool1 = str.equalsIgnoreCase(paramString1);
    if (!bool1)
    {
      str = "action.page.forgot_upi_pin";
      boolean bool2 = str.equalsIgnoreCase(paramString1);
      if (!bool2) {}
    }
    else
    {
      paramString1 = (com.truecaller.truepay.app.ui.accounts.views.c.b)d;
      parama = (com.truecaller.truepay.data.api.model.a)Objects.requireNonNull(parama);
      paramString2 = (String)Objects.requireNonNull(paramString2);
      parama = c.a(parama, paramString2);
      paramString1.a(parama);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.accounts.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */