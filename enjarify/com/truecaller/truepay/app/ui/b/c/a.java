package com.truecaller.truepay.app.ui.b.c;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.log.d;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;
import java.util.HashMap;

public final class a
  extends android.support.design.widget.b
  implements b
{
  public static final a.b c;
  a.a a;
  public com.truecaller.truepay.app.ui.b.b.a b;
  private TextView d;
  private TextView e;
  private TextView f;
  private TextView g;
  private HashMap h;
  
  static
  {
    a.b localb = new com/truecaller/truepay/app/ui/b/c/a$b;
    localb.<init>((byte)0);
    c = localb;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "title");
    TextView localTextView = d;
    if (localTextView == null)
    {
      String str = "tvTitle";
      k.a(str);
    }
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "subTitle");
    TextView localTextView = e;
    if (localTextView == null)
    {
      String str = "tvSubTitle";
      k.a(str);
    }
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "leftButton");
    TextView localTextView = f;
    if (localTextView == null)
    {
      String str = "buttonLeft";
      k.a(str);
    }
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "rightButton");
    TextView localTextView = g;
    if (localTextView == null)
    {
      String str = "buttonRight";
      k.a(str);
    }
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final int getTheme()
  {
    return R.style.BottomSheetDialogTheme;
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = a;
    if (paramContext == null)
    {
      paramContext = getTargetFragment();
      boolean bool = paramContext instanceof a.a;
      if (bool)
      {
        paramContext = getTargetFragment();
        if (paramContext != null)
        {
          paramContext = (a.a)paramContext;
          a = paramContext;
        }
        else
        {
          paramContext = new c/u;
          paramContext.<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.common.views.TwoButtonBottomSheet.ActionListener");
          throw paramContext;
        }
      }
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.b.a.a.a();
    Object localObject = Truepay.getApplicationComponent();
    paramBundle.a((com.truecaller.truepay.app.a.a.a)localObject).a().a(this);
    paramBundle = b;
    if (paramBundle == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    localObject = this;
    localObject = (com.truecaller.truepay.app.ui.base.views.a)this;
    paramBundle.a((com.truecaller.truepay.app.ui.base.views.a)localObject);
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new android/support/design/widget/a;
    Context localContext = requireContext();
    int i = getTheme();
    paramBundle.<init>(localContext, i);
    return (Dialog)paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i = R.layout.fragment_tc_pay_two_button_bottom_sheet;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.bottomSheetTitle;
    paramBundle = paramView.findViewById(i);
    k.a(paramBundle, "view.findViewById(R.id.bottomSheetTitle)");
    paramBundle = (TextView)paramBundle;
    d = paramBundle;
    i = R.id.bottomSheetSubTitle;
    paramBundle = paramView.findViewById(i);
    k.a(paramBundle, "view.findViewById(R.id.bottomSheetSubTitle)");
    paramBundle = (TextView)paramBundle;
    e = paramBundle;
    i = R.id.buttonLeft;
    paramBundle = paramView.findViewById(i);
    String str = "view.findViewById(R.id.buttonLeft)";
    k.a(paramBundle, str);
    paramBundle = (TextView)paramBundle;
    f = paramBundle;
    i = R.id.buttonRight;
    paramView = paramView.findViewById(i);
    paramBundle = "view.findViewById(R.id.buttonRight)";
    k.a(paramView, paramBundle);
    paramView = (TextView)paramView;
    g = paramView;
    paramView = getArguments();
    if (paramView != null)
    {
      paramBundle = b;
      if (paramBundle == null)
      {
        str = "presenter";
        k.a(str);
      }
      k.a(paramView, "it");
      k.b(paramView, "bundle");
      str = paramView.getString("title");
      b localb;
      if (str != null)
      {
        localb = (b)paramBundle.af_();
        localb.a(str);
      }
      str = paramView.getString("sub_title");
      if (str != null)
      {
        localb = (b)paramBundle.af_();
        localb.b(str);
      }
      str = paramView.getString("button_left");
      if (str != null)
      {
        localb = (b)paramBundle.af_();
        localb.c(str);
      }
      str = "button_right";
      paramView = paramView.getString(str);
      if (paramView != null)
      {
        paramBundle = (b)paramBundle.af_();
        paramBundle.d(paramView);
      }
    }
    paramView = f;
    if (paramView == null)
    {
      paramBundle = "buttonLeft";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/truepay/app/ui/b/c/a$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = g;
    if (paramView == null)
    {
      paramBundle = "buttonRight";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/truepay/app/ui/b/c/a$d;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
  }
  
  public final void show(j paramj, String paramString)
  {
    if (paramj == null) {
      return;
    }
    try
    {
      paramj = paramj.a();
      Object localObject = "managerCopy.beginTransaction()";
      k.a(paramj, (String)localObject);
      localObject = this;
      localObject = (Fragment)this;
      paramString = paramj.a((Fragment)localObject, paramString);
      localObject = null;
      paramString.a(null);
      paramj.d();
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      d.a((Throwable)localIllegalStateException);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.b.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */