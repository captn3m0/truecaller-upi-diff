package com.truecaller.truepay.app.ui.b.c;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import c.g.b.k;

public final class a$b
{
  public static void a(Fragment paramFragment, String paramString1, String paramString2, String paramString3, String paramString4)
  {
    k.b(paramFragment, "fragment");
    k.b(paramString1, "title");
    k.b(paramString2, "subTitle");
    k.b(paramString3, "leftButtonText");
    k.b(paramString4, "rightButtonText");
    a locala = new com/truecaller/truepay/app/ui/b/c/a;
    locala.<init>();
    Object localObject = paramFragment.getActivity();
    if (localObject == null) {
      return;
    }
    k.a(localObject, "fragment.activity ?: return");
    localObject = ((f)localObject).getIntent();
    String str = "activity.intent";
    k.a(localObject, str);
    localObject = ((Intent)localObject).getExtras();
    if (localObject != null)
    {
      str = "title";
      ((Bundle)localObject).putString(str, paramString1);
      ((Bundle)localObject).putString("sub_title", paramString2);
      ((Bundle)localObject).putString("button_left", paramString3);
      paramString1 = "button_right";
      ((Bundle)localObject).putString(paramString1, paramString4);
    }
    else
    {
      localObject = null;
    }
    locala.setArguments((Bundle)localObject);
    locala.setTargetFragment(paramFragment, 1007);
    paramFragment = paramFragment.getFragmentManager();
    paramString1 = locala.getTag();
    locala.show(paramFragment, paramString1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.b.c.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */