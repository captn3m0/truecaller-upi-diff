package com.truecaller.truepay.app.ui.webapps.a;

import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import com.truecaller.truepay.app.ui.webapps.models.WebAppConfig;
import kotlinx.coroutines.ag;

public abstract class a
  implements b, ag
{
  public Object b;
  public WebAppConfig c;
  private final c.f d;
  private final c.d.f e;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(a.class);
    ((u)localObject).<init>(localb, "job", "getJob()Lkotlinx/coroutines/Job;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public a(c.d.f paramf)
  {
    e = paramf;
    paramf = c.g.a((c.g.a.a)a.a.a);
    d = paramf;
  }
  
  public final c.d.f V_()
  {
    c.d.f localf1 = e;
    c.d.f localf2 = (c.d.f)d.b();
    return localf1.plus(localf2);
  }
  
  public final void a(WebAppConfig paramWebAppConfig)
  {
    k.b(paramWebAppConfig, "config");
    c = paramWebAppConfig;
  }
  
  public final void a(Object paramObject)
  {
    b = paramObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */