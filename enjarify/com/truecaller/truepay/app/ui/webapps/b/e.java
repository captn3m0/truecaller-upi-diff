package com.truecaller.truepay.app.ui.webapps.b;

import dagger.a.d;
import javax.inject.Provider;

public final class e
  implements d
{
  private final c a;
  private final Provider b;
  
  private e(c paramc, Provider paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static e a(c paramc, Provider paramProvider)
  {
    e locale = new com/truecaller/truepay/app/ui/webapps/b/e;
    locale.<init>(paramc, paramProvider);
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */