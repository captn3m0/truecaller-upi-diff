package com.truecaller.truepay.app.ui.webapps.b;

import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d
{
  private final c a;
  private final Provider b;
  
  private g(c paramc, Provider paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static g a(c paramc, Provider paramProvider)
  {
    g localg = new com/truecaller/truepay/app/ui/webapps/b/g;
    localg.<init>(paramc, paramProvider);
    return localg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */