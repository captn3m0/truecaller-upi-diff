package com.truecaller.truepay.app.ui.webapps.d;

import c.g.b.k;
import c.l;
import com.truecaller.truepay.app.ui.webapps.a.a;
import com.truecaller.truepay.app.ui.webapps.models.WebAppConfig;
import com.truecaller.truepay.app.ui.webapps.models.b;
import dagger.a;

public final class e
  implements d
{
  private final a a;
  
  public e(a parama)
  {
    a = parama;
  }
  
  public final a.a a(WebAppConfig paramWebAppConfig)
  {
    k.b(paramWebAppConfig, "config");
    String str = "receiver$0";
    k.b(paramWebAppConfig, str);
    paramWebAppConfig = paramWebAppConfig.getName();
    int i = paramWebAppConfig.hashCode();
    int j = 100648834;
    if (i == j)
    {
      str = "ixigo";
      boolean bool = paramWebAppConfig.equals(str);
      if (bool)
      {
        paramWebAppConfig = (com.truecaller.truepay.app.ui.webapps.models.e)b.a;
        bool = paramWebAppConfig instanceof b;
        if (bool)
        {
          paramWebAppConfig = a.get();
          k.a(paramWebAppConfig, "ixigoJsInterceptorProvider.get()");
          return (a.a)paramWebAppConfig;
        }
        paramWebAppConfig = new c/l;
        paramWebAppConfig.<init>();
        throw paramWebAppConfig;
      }
    }
    paramWebAppConfig = new java/lang/IllegalStateException;
    paramWebAppConfig.<init>("AppNotRecognized");
    throw ((Throwable)paramWebAppConfig);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.d.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */