package com.truecaller.truepay.app.ui.webapps.d;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import c.g.b.k;
import c.l.g;
import com.truecaller.truepay.app.ui.webapps.models.DeviceConfig;
import java.util.UUID;

public final class b
  implements a
{
  private final Context a;
  
  public b(Context paramContext)
  {
    a = paramContext;
  }
  
  public final DeviceConfig a(g paramg)
  {
    k.b(paramg, "property");
    paramg = new com/truecaller/truepay/app/ui/webapps/models/DeviceConfig;
    String str1 = Settings.Secure.getString(a.getContentResolver(), "android_id");
    k.a(str1, "Secure.getString(context…olver, Secure.ANDROID_ID)");
    String str2 = UUID.randomUUID().toString();
    k.a(str2, "UUID.randomUUID().toString()");
    String str3 = Build.BRAND;
    k.a(str3, "Build.BRAND");
    String str4 = Build.MANUFACTURER;
    k.a(str4, "Build.MANUFACTURER");
    int i = Build.VERSION.SDK_INT;
    String str5 = Build.DEVICE;
    k.a(str5, "Build.DEVICE");
    String str6 = Build.PRODUCT;
    k.a(str6, "Build.PRODUCT");
    paramg.<init>(1041006, str1, str2, str3, str4, i, str5, str6);
    return paramg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */