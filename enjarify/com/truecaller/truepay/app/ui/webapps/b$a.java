package com.truecaller.truepay.app.ui.webapps;

import android.content.Intent;
import android.os.Bundle;
import com.truecaller.bm;

public abstract interface b$a
  extends bm
{
  public abstract void a(int paramInt1, int paramInt2, Intent paramIntent);
  
  public abstract void a(Bundle paramBundle1, Bundle paramBundle2);
  
  public abstract void b();
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */