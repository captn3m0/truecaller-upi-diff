package com.truecaller.truepay.app.ui.webapps;

import android.content.Intent;
import android.os.Bundle;
import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import c.l.g;
import c.x;
import com.truecaller.bb;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.webapps.d.d;
import com.truecaller.truepay.app.ui.webapps.ixigo.IxigoPaymentRequest;
import com.truecaller.truepay.app.ui.webapps.models.DeviceConfig;
import com.truecaller.truepay.app.ui.webapps.models.WebAppConfig;
import com.truecaller.utils.n;

public final class c
  extends bb
  implements a.b, b.a
{
  private final com.truecaller.truepay.app.ui.webapps.d.a c;
  private a.a d;
  private com.truecaller.truepay.app.ui.payments.models.a e;
  private final d f;
  private final n g;
  private final com.truecaller.truepay.data.b.a h;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(c.class);
    ((u)localObject).<init>(localb, "deviceConfig", "getDeviceConfig()Lcom/truecaller/truepay/app/ui/webapps/models/DeviceConfig;");
    localObject = (g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public c(d paramd, com.truecaller.truepay.app.ui.webapps.d.a parama, n paramn, com.truecaller.truepay.data.b.a parama1)
  {
    f = paramd;
    g = paramn;
    h = parama1;
    c = parama;
  }
  
  private final DeviceConfig c()
  {
    com.truecaller.truepay.app.ui.webapps.d.a locala = c;
    g localg = a[0];
    return locala.a(localg);
  }
  
  private final void e()
  {
    b.b localb = (b.b)b;
    if (localb != null)
    {
      Object localObject = g;
      int i = R.string.something_went_wrong;
      Object[] arrayOfObject = new Object[0];
      localObject = ((n)localObject).a(i, arrayOfObject);
      k.a(localObject, "resourceProvider.getStri…ing.something_went_wrong)");
      localb.b((String)localObject);
      localb.a();
      return;
    }
  }
  
  public final void a()
  {
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.a();
      return;
    }
  }
  
  public final void a(int paramInt1, int paramInt2, Intent paramIntent)
  {
    a.a locala = d;
    if (locala != null)
    {
      locala.a(paramInt1, paramInt2, paramIntent);
      return;
    }
  }
  
  public final void a(Bundle paramBundle1, Bundle paramBundle2)
  {
    if (paramBundle1 == null)
    {
      if (paramBundle2 == null)
      {
        e();
        return;
      }
      paramBundle1 = paramBundle2.getString("extra_launch_type", "webapp");
      String str1 = paramBundle2.getString("details_url");
      Object localObject1 = "utility_entry";
      paramBundle2 = paramBundle2.getSerializable((String)localObject1);
      boolean bool1 = paramBundle2 instanceof com.truecaller.truepay.app.ui.payments.models.a;
      if (!bool1) {
        paramBundle2 = null;
      }
      paramBundle2 = (com.truecaller.truepay.app.ui.payments.models.a)paramBundle2;
      if (paramBundle2 == null)
      {
        e();
        return;
      }
      e = paramBundle2;
      paramBundle2 = e;
      if (paramBundle2 != null)
      {
        paramBundle2 = paramBundle2.t();
        if (paramBundle2 != null)
        {
          localObject1 = f.a(paramBundle2);
          d = ((a.a)localObject1);
          localObject1 = d;
          if (localObject1 == null) {
            return;
          }
          ((a.a)localObject1).a(paramBundle2);
          ((a.a)localObject1).a(this);
          b.b localb = (b.b)b;
          if (localb != null)
          {
            Object localObject2 = e;
            if (localObject2 != null)
            {
              localObject2 = ((com.truecaller.truepay.app.ui.payments.models.a)localObject2).d();
              String str2 = "it.title";
              k.a(localObject2, str2);
              localb.a((String)localObject2);
            }
            localObject2 = "details";
            boolean bool2 = k.a(paramBundle1, localObject2);
            if ((bool2) && (str1 != null))
            {
              paramBundle1 = c();
              localb.a((a.a)localObject1, paramBundle1, paramBundle2, str1);
              return;
            }
            paramBundle1 = c();
            str1 = paramBundle2.getUrl();
            localb.a((a.a)localObject1, paramBundle1, paramBundle2, str1);
            return;
          }
          return;
        }
      }
      e();
      return;
    }
  }
  
  public final void a(IxigoPaymentRequest paramIxigoPaymentRequest)
  {
    k.b(paramIxigoPaymentRequest, "request");
    com.truecaller.truepay.app.ui.payments.models.a locala = e;
    if (locala != null)
    {
      b.b localb = (b.b)b;
      if (localb != null)
      {
        String str = "webapp_payment_request";
        localb.a(paramIxigoPaymentRequest, locala, str);
        paramIxigoPaymentRequest = x.a;
      }
      else
      {
        paramIxigoPaymentRequest = null;
      }
      if (paramIxigoPaymentRequest != null) {}
    }
    else
    {
      paramIxigoPaymentRequest = this;
      ((c)this).e();
      paramIxigoPaymentRequest = x.a;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "redirectUrl");
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.c(paramString);
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "extraKey");
    k.b(paramString2, "extraValue");
    h.c(paramString2);
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.a(paramString1, paramString2);
      return;
    }
  }
  
  public final void b()
  {
    Object localObject = d;
    if (localObject != null) {
      ((a.a)localObject).a();
    }
    localObject = (b.b)b;
    if (localObject != null)
    {
      ((b.b)localObject).a();
      return;
    }
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "message");
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.b(paramString);
      localb.a();
      return;
    }
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "message");
    b.b localb = (b.b)b;
    if (localb != null)
    {
      localb.b(paramString);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */