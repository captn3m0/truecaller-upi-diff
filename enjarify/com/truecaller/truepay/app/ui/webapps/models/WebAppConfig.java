package com.truecaller.truepay.app.ui.webapps.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;
import java.io.Serializable;

public final class WebAppConfig
  implements Parcelable, Serializable
{
  public static final Parcelable.Creator CREATOR;
  private final String apiKey;
  private final String authUrl;
  private final String clientId;
  private final String currencyCode;
  private final String jsInterfaceName;
  private final String name;
  private final String url;
  private final JsConfig webViewSettings;
  
  static
  {
    WebAppConfig.a locala = new com/truecaller/truepay/app/ui/webapps/models/WebAppConfig$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public WebAppConfig(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, JsConfig paramJsConfig, String paramString7)
  {
    name = paramString1;
    clientId = paramString2;
    apiKey = paramString3;
    currencyCode = paramString4;
    url = paramString5;
    jsInterfaceName = paramString6;
    webViewSettings = paramJsConfig;
    authUrl = paramString7;
  }
  
  public final String component1()
  {
    return name;
  }
  
  public final String component2()
  {
    return clientId;
  }
  
  public final String component3()
  {
    return apiKey;
  }
  
  public final String component4()
  {
    return currencyCode;
  }
  
  public final String component5()
  {
    return url;
  }
  
  public final String component6()
  {
    return jsInterfaceName;
  }
  
  public final JsConfig component7()
  {
    return webViewSettings;
  }
  
  public final String component8()
  {
    return authUrl;
  }
  
  public final WebAppConfig copy(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, JsConfig paramJsConfig, String paramString7)
  {
    k.b(paramString1, "name");
    k.b(paramString2, "clientId");
    k.b(paramString3, "apiKey");
    k.b(paramString4, "currencyCode");
    k.b(paramString5, "url");
    k.b(paramString6, "jsInterfaceName");
    k.b(paramJsConfig, "webViewSettings");
    k.b(paramString7, "authUrl");
    WebAppConfig localWebAppConfig = new com/truecaller/truepay/app/ui/webapps/models/WebAppConfig;
    localWebAppConfig.<init>(paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramJsConfig, paramString7);
    return localWebAppConfig;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof WebAppConfig;
      if (bool1)
      {
        paramObject = (WebAppConfig)paramObject;
        Object localObject1 = name;
        Object localObject2 = name;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = clientId;
          localObject2 = clientId;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = apiKey;
            localObject2 = apiKey;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = currencyCode;
              localObject2 = currencyCode;
              bool1 = k.a(localObject1, localObject2);
              if (bool1)
              {
                localObject1 = url;
                localObject2 = url;
                bool1 = k.a(localObject1, localObject2);
                if (bool1)
                {
                  localObject1 = jsInterfaceName;
                  localObject2 = jsInterfaceName;
                  bool1 = k.a(localObject1, localObject2);
                  if (bool1)
                  {
                    localObject1 = webViewSettings;
                    localObject2 = webViewSettings;
                    bool1 = k.a(localObject1, localObject2);
                    if (bool1)
                    {
                      localObject1 = authUrl;
                      paramObject = authUrl;
                      boolean bool2 = k.a(localObject1, paramObject);
                      if (bool2) {
                        break label200;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label200:
    return true;
  }
  
  public final String getApiKey()
  {
    return apiKey;
  }
  
  public final String getAuthUrl()
  {
    return authUrl;
  }
  
  public final String getClientId()
  {
    return clientId;
  }
  
  public final String getCurrencyCode()
  {
    return currencyCode;
  }
  
  public final String getJsInterfaceName()
  {
    return jsInterfaceName;
  }
  
  public final String getName()
  {
    return name;
  }
  
  public final String getUrl()
  {
    return url;
  }
  
  public final JsConfig getWebViewSettings()
  {
    return webViewSettings;
  }
  
  public final int hashCode()
  {
    String str = name;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = clientId;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = apiKey;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = currencyCode;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = url;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = jsInterfaceName;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = webViewSettings;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = authUrl;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("WebAppConfig(name=");
    Object localObject = name;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", clientId=");
    localObject = clientId;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", apiKey=");
    localObject = apiKey;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", currencyCode=");
    localObject = currencyCode;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", url=");
    localObject = url;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", jsInterfaceName=");
    localObject = jsInterfaceName;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", webViewSettings=");
    localObject = webViewSettings;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", authUrl=");
    localObject = authUrl;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = name;
    paramParcel.writeString(str);
    str = clientId;
    paramParcel.writeString(str);
    str = apiKey;
    paramParcel.writeString(str);
    str = currencyCode;
    paramParcel.writeString(str);
    str = url;
    paramParcel.writeString(str);
    str = jsInterfaceName;
    paramParcel.writeString(str);
    webViewSettings.writeToParcel(paramParcel, 0);
    str = authUrl;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.models.WebAppConfig
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */