package com.truecaller.truepay.app.ui.webapps.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class WebAppConfig$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    WebAppConfig localWebAppConfig = new com/truecaller/truepay/app/ui/webapps/models/WebAppConfig;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    String str4 = paramParcel.readString();
    String str5 = paramParcel.readString();
    String str6 = paramParcel.readString();
    Object localObject1 = JsConfig.CREATOR.createFromParcel(paramParcel);
    Object localObject2 = localObject1;
    localObject2 = (JsConfig)localObject1;
    String str7 = paramParcel.readString();
    localObject1 = localWebAppConfig;
    localWebAppConfig.<init>(str1, str2, str3, str4, str5, str6, (JsConfig)localObject2, str7);
    return localWebAppConfig;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new WebAppConfig[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.models.WebAppConfig.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */