package com.truecaller.truepay.app.ui.webapps.models;

import c.g.b.k;

public final class d
  extends c
{
  public final Object a;
  
  public d(Object paramObject)
  {
    super((byte)0);
    a = paramObject;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof d;
      if (bool1)
      {
        paramObject = (d)paramObject;
        Object localObject = a;
        paramObject = a;
        boolean bool2 = k.a(localObject, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    Object localObject = a;
    if (localObject != null) {
      return localObject.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Success(data=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.models.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */