package com.truecaller.truepay.app.ui.webapps.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;
import java.io.Serializable;

public final class JsConfig
  implements Parcelable, Serializable
{
  public static final Parcelable.Creator CREATOR;
  private final boolean cookies;
  private final boolean database;
  private final boolean domStorage;
  private final boolean geoLocation;
  private final boolean jsEnabled;
  private final boolean overview;
  private final boolean saveForm;
  private final boolean useZoomControls;
  private final boolean wideView;
  
  static
  {
    JsConfig.a locala = new com/truecaller/truepay/app/ui/webapps/models/JsConfig$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public JsConfig(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, boolean paramBoolean8, boolean paramBoolean9)
  {
    wideView = paramBoolean1;
    overview = paramBoolean2;
    jsEnabled = paramBoolean3;
    saveForm = paramBoolean4;
    useZoomControls = paramBoolean5;
    domStorage = paramBoolean6;
    database = paramBoolean7;
    geoLocation = paramBoolean8;
    cookies = paramBoolean9;
  }
  
  public final boolean component1()
  {
    return wideView;
  }
  
  public final boolean component2()
  {
    return overview;
  }
  
  public final boolean component3()
  {
    return jsEnabled;
  }
  
  public final boolean component4()
  {
    return saveForm;
  }
  
  public final boolean component5()
  {
    return useZoomControls;
  }
  
  public final boolean component6()
  {
    return domStorage;
  }
  
  public final boolean component7()
  {
    return database;
  }
  
  public final boolean component8()
  {
    return geoLocation;
  }
  
  public final boolean component9()
  {
    return cookies;
  }
  
  public final JsConfig copy(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, boolean paramBoolean8, boolean paramBoolean9)
  {
    JsConfig localJsConfig = new com/truecaller/truepay/app/ui/webapps/models/JsConfig;
    localJsConfig.<init>(paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4, paramBoolean5, paramBoolean6, paramBoolean7, paramBoolean8, paramBoolean9);
    return localJsConfig;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof JsConfig;
      if (bool2)
      {
        paramObject = (JsConfig)paramObject;
        bool2 = wideView;
        boolean bool3 = wideView;
        if (bool2 == bool3) {
          bool2 = true;
        } else {
          bool2 = false;
        }
        if (bool2)
        {
          bool2 = overview;
          bool3 = overview;
          if (bool2 == bool3) {
            bool2 = true;
          } else {
            bool2 = false;
          }
          if (bool2)
          {
            bool2 = jsEnabled;
            bool3 = jsEnabled;
            if (bool2 == bool3) {
              bool2 = true;
            } else {
              bool2 = false;
            }
            if (bool2)
            {
              bool2 = saveForm;
              bool3 = saveForm;
              if (bool2 == bool3) {
                bool2 = true;
              } else {
                bool2 = false;
              }
              if (bool2)
              {
                bool2 = useZoomControls;
                bool3 = useZoomControls;
                if (bool2 == bool3) {
                  bool2 = true;
                } else {
                  bool2 = false;
                }
                if (bool2)
                {
                  bool2 = domStorage;
                  bool3 = domStorage;
                  if (bool2 == bool3) {
                    bool2 = true;
                  } else {
                    bool2 = false;
                  }
                  if (bool2)
                  {
                    bool2 = database;
                    bool3 = database;
                    if (bool2 == bool3) {
                      bool2 = true;
                    } else {
                      bool2 = false;
                    }
                    if (bool2)
                    {
                      bool2 = geoLocation;
                      bool3 = geoLocation;
                      if (bool2 == bool3) {
                        bool2 = true;
                      } else {
                        bool2 = false;
                      }
                      if (bool2)
                      {
                        bool2 = cookies;
                        boolean bool4 = cookies;
                        if (bool2 == bool4)
                        {
                          bool4 = true;
                        }
                        else
                        {
                          bool4 = false;
                          paramObject = null;
                        }
                        if (bool4) {
                          return bool1;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final boolean getCookies()
  {
    return cookies;
  }
  
  public final boolean getDatabase()
  {
    return database;
  }
  
  public final boolean getDomStorage()
  {
    return domStorage;
  }
  
  public final boolean getGeoLocation()
  {
    return geoLocation;
  }
  
  public final boolean getJsEnabled()
  {
    return jsEnabled;
  }
  
  public final boolean getOverview()
  {
    return overview;
  }
  
  public final boolean getSaveForm()
  {
    return saveForm;
  }
  
  public final boolean getUseZoomControls()
  {
    return useZoomControls;
  }
  
  public final boolean getWideView()
  {
    return wideView;
  }
  
  public final int hashCode()
  {
    boolean bool = wideView;
    int j = 1;
    if (bool) {
      bool = true;
    }
    bool *= true;
    int k = overview;
    if (k != 0) {
      k = 1;
    }
    int i = (i + k) * 31;
    int m = jsEnabled;
    if (m != 0) {
      m = 1;
    }
    i = (i + m) * 31;
    int n = saveForm;
    if (n != 0) {
      n = 1;
    }
    i = (i + n) * 31;
    int i1 = useZoomControls;
    if (i1 != 0) {
      i1 = 1;
    }
    i = (i + i1) * 31;
    int i2 = domStorage;
    if (i2 != 0) {
      i2 = 1;
    }
    i = (i + i2) * 31;
    int i3 = database;
    if (i3 != 0) {
      i3 = 1;
    }
    i = (i + i3) * 31;
    int i4 = geoLocation;
    if (i4 != 0) {
      i4 = 1;
    }
    i = (i + i4) * 31;
    int i5 = cookies;
    if (i5 == 0) {
      j = i5;
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("JsConfig(wideView=");
    boolean bool = wideView;
    localStringBuilder.append(bool);
    localStringBuilder.append(", overview=");
    bool = overview;
    localStringBuilder.append(bool);
    localStringBuilder.append(", jsEnabled=");
    bool = jsEnabled;
    localStringBuilder.append(bool);
    localStringBuilder.append(", saveForm=");
    bool = saveForm;
    localStringBuilder.append(bool);
    localStringBuilder.append(", useZoomControls=");
    bool = useZoomControls;
    localStringBuilder.append(bool);
    localStringBuilder.append(", domStorage=");
    bool = domStorage;
    localStringBuilder.append(bool);
    localStringBuilder.append(", database=");
    bool = database;
    localStringBuilder.append(bool);
    localStringBuilder.append(", geoLocation=");
    bool = geoLocation;
    localStringBuilder.append(bool);
    localStringBuilder.append(", cookies=");
    bool = cookies;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    paramInt = wideView;
    paramParcel.writeInt(paramInt);
    paramInt = overview;
    paramParcel.writeInt(paramInt);
    paramInt = jsEnabled;
    paramParcel.writeInt(paramInt);
    paramInt = saveForm;
    paramParcel.writeInt(paramInt);
    paramInt = useZoomControls;
    paramParcel.writeInt(paramInt);
    paramInt = domStorage;
    paramParcel.writeInt(paramInt);
    paramInt = database;
    paramParcel.writeInt(paramInt);
    paramInt = geoLocation;
    paramParcel.writeInt(paramInt);
    paramInt = cookies;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.models.JsConfig
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */