package com.truecaller.truepay.app.ui.webapps.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class JsConfig$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    JsConfig localJsConfig = new com/truecaller/truepay/app/ui/webapps/models/JsConfig;
    int i = paramParcel.readInt();
    boolean bool1 = true;
    boolean bool2 = false;
    if (i != 0) {
      bool3 = true;
    } else {
      bool3 = false;
    }
    i = paramParcel.readInt();
    if (i != 0) {
      bool4 = true;
    } else {
      bool4 = false;
    }
    i = paramParcel.readInt();
    if (i != 0)
    {
      bool5 = true;
      f1 = Float.MIN_VALUE;
    }
    else
    {
      bool5 = false;
      f1 = 0.0F;
    }
    i = paramParcel.readInt();
    if (i != 0)
    {
      bool6 = true;
      f2 = Float.MIN_VALUE;
    }
    else
    {
      bool6 = false;
      f2 = 0.0F;
    }
    i = paramParcel.readInt();
    if (i != 0)
    {
      bool7 = true;
      f3 = Float.MIN_VALUE;
    }
    else
    {
      bool7 = false;
      f3 = 0.0F;
    }
    i = paramParcel.readInt();
    if (i != 0)
    {
      bool8 = true;
      f4 = Float.MIN_VALUE;
    }
    else
    {
      bool8 = false;
      f4 = 0.0F;
    }
    i = paramParcel.readInt();
    if (i != 0)
    {
      bool9 = true;
      f5 = Float.MIN_VALUE;
    }
    else
    {
      bool9 = false;
      f5 = 0.0F;
    }
    i = paramParcel.readInt();
    boolean bool10;
    float f6;
    if (i != 0)
    {
      bool10 = true;
      f6 = Float.MIN_VALUE;
    }
    else
    {
      bool10 = false;
      f6 = 0.0F;
    }
    int j = paramParcel.readInt();
    float f7;
    if (j != 0)
    {
      j = 1;
      f7 = Float.MIN_VALUE;
    }
    else
    {
      j = 0;
      f7 = 0.0F;
      paramParcel = null;
    }
    bool1 = bool3;
    bool2 = bool4;
    boolean bool3 = bool5;
    boolean bool4 = bool6;
    boolean bool5 = bool7;
    float f1 = f3;
    boolean bool6 = bool8;
    float f2 = f4;
    boolean bool7 = bool9;
    float f3 = f5;
    boolean bool8 = bool10;
    float f4 = f6;
    boolean bool9 = j;
    float f5 = f7;
    localJsConfig.<init>(bool1, bool2, bool3, bool4, bool5, bool6, bool7, bool10, j);
    return localJsConfig;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new JsConfig[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.models.JsConfig.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */