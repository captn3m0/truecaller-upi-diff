package com.truecaller.truepay.app.ui.webapps.models;

import c.g.b.k;

public final class a
  extends c
{
  private final Throwable a = null;
  private final String b;
  
  public a(String paramString)
  {
    super((byte)0);
    b = paramString;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof a;
      if (bool1)
      {
        paramObject = (a)paramObject;
        Object localObject = a;
        Throwable localThrowable = a;
        bool1 = k.a(localObject, localThrowable);
        if (bool1)
        {
          localObject = b;
          paramObject = b;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final int hashCode()
  {
    Throwable localThrowable = a;
    int i = 0;
    int j;
    if (localThrowable != null)
    {
      j = localThrowable.hashCode();
    }
    else
    {
      j = 0;
      localThrowable = null;
    }
    j *= 31;
    String str = b;
    if (str != null) {
      i = str.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Failure(error=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", message=");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.models.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */