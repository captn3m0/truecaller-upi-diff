package com.truecaller.truepay.app.ui.webapps.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class DeviceConfig
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final int a;
  public final String b;
  public final String c;
  public final String d;
  public final String e;
  public final int f;
  public final String g;
  public final String h;
  
  static
  {
    DeviceConfig.a locala = new com/truecaller/truepay/app/ui/webapps/models/DeviceConfig$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public DeviceConfig(int paramInt1, String paramString1, String paramString2, String paramString3, String paramString4, int paramInt2, String paramString5, String paramString6)
  {
    a = paramInt1;
    b = paramString1;
    c = paramString2;
    d = paramString3;
    e = paramString4;
    f = paramInt2;
    g = paramString5;
    h = paramString6;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof DeviceConfig;
      if (bool2)
      {
        paramObject = (DeviceConfig)paramObject;
        int i = a;
        int k = a;
        String str1;
        if (i == k)
        {
          i = 1;
        }
        else
        {
          i = 0;
          str1 = null;
        }
        if (i != 0)
        {
          str1 = b;
          String str2 = b;
          boolean bool3 = k.a(str1, str2);
          if (bool3)
          {
            str1 = c;
            str2 = c;
            bool3 = k.a(str1, str2);
            if (bool3)
            {
              str1 = d;
              str2 = d;
              bool3 = k.a(str1, str2);
              if (bool3)
              {
                str1 = e;
                str2 = e;
                bool3 = k.a(str1, str2);
                if (bool3)
                {
                  int j = f;
                  k = f;
                  if (j == k)
                  {
                    j = 1;
                  }
                  else
                  {
                    j = 0;
                    str1 = null;
                  }
                  if (j != 0)
                  {
                    str1 = g;
                    str2 = g;
                    boolean bool4 = k.a(str1, str2);
                    if (bool4)
                    {
                      str1 = h;
                      paramObject = h;
                      boolean bool5 = k.a(str1, paramObject);
                      if (bool5) {
                        return bool1;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = a * 31;
    String str = b;
    int j = 0;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    i = (i + k) * 31;
    str = c;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    i = (i + k) * 31;
    str = d;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    i = (i + k) * 31;
    str = e;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    i = (i + k) * 31;
    int k = f;
    i = (i + k) * 31;
    str = g;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    i = (i + k) * 31;
    str = h;
    if (str != null) {
      j = str.hashCode();
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("DeviceConfig(appVersion=");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(", deviceId=");
    String str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(", uuid=");
    str = c;
    localStringBuilder.append(str);
    localStringBuilder.append(", brand=");
    str = d;
    localStringBuilder.append(str);
    localStringBuilder.append(", manufacturer=");
    str = e;
    localStringBuilder.append(str);
    localStringBuilder.append(", apiLevel=");
    i = f;
    localStringBuilder.append(i);
    localStringBuilder.append(", device=");
    str = g;
    localStringBuilder.append(str);
    localStringBuilder.append(", product=");
    str = h;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    paramInt = a;
    paramParcel.writeInt(paramInt);
    String str = b;
    paramParcel.writeString(str);
    str = c;
    paramParcel.writeString(str);
    str = d;
    paramParcel.writeString(str);
    str = e;
    paramParcel.writeString(str);
    paramInt = f;
    paramParcel.writeInt(paramInt);
    str = g;
    paramParcel.writeString(str);
    str = h;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.models.DeviceConfig
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */