package com.truecaller.truepay.app.ui.webapps.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class DeviceConfig$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    DeviceConfig localDeviceConfig = new com/truecaller/truepay/app/ui/webapps/models/DeviceConfig;
    int i = paramParcel.readInt();
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    String str4 = paramParcel.readString();
    int j = paramParcel.readInt();
    String str5 = paramParcel.readString();
    String str6 = paramParcel.readString();
    localDeviceConfig.<init>(i, str1, str2, str3, str4, j, str5, str6);
    return localDeviceConfig;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new DeviceConfig[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.models.DeviceConfig.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */