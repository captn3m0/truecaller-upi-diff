package com.truecaller.truepay.app.ui.webapps;

import com.truecaller.truepay.app.ui.payments.models.a;
import com.truecaller.truepay.app.ui.webapps.ixigo.IxigoPaymentRequest;
import com.truecaller.truepay.app.ui.webapps.models.DeviceConfig;
import com.truecaller.truepay.app.ui.webapps.models.WebAppConfig;

public abstract interface b$b
{
  public static final b.b.a c = b.b.a.a;
  
  public abstract void a();
  
  public abstract void a(a.a parama, DeviceConfig paramDeviceConfig, WebAppConfig paramWebAppConfig, String paramString);
  
  public abstract void a(IxigoPaymentRequest paramIxigoPaymentRequest, a parama, String paramString);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString1, String paramString2);
  
  public abstract void b(String paramString);
  
  public abstract void c(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */