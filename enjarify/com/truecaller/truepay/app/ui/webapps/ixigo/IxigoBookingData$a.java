package com.truecaller.truepay.app.ui.webapps.ixigo;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;
import java.util.List;

public final class IxigoBookingData$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    IxigoBookingData localIxigoBookingData = new com/truecaller/truepay/app/ui/webapps/ixigo/IxigoBookingData;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    String str4 = paramParcel.readString();
    String str5 = paramParcel.readString();
    String str6 = paramParcel.readString();
    String str7 = paramParcel.readString();
    String str8 = paramParcel.readString();
    String str9 = paramParcel.readString();
    String str10 = paramParcel.readString();
    String str11 = paramParcel.readString();
    paramParcel = paramParcel.createStringArrayList();
    Object localObject = paramParcel;
    localObject = (List)paramParcel;
    localIxigoBookingData.<init>(str1, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, (List)localObject);
    return localIxigoBookingData;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new IxigoBookingData[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.ixigo.IxigoBookingData.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */