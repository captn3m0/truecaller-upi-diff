package com.truecaller.truepay.app.ui.webapps.ixigo;

import c.g.b.k;

public final class IxigoTokenRequest
{
  private final String initiator_msisdn;
  private final String initiator_name;
  private final String operator;
  private final String type;
  
  public IxigoTokenRequest(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    type = paramString1;
    initiator_msisdn = paramString2;
    initiator_name = paramString3;
    operator = paramString4;
  }
  
  public final String component1()
  {
    return type;
  }
  
  public final String component2()
  {
    return initiator_msisdn;
  }
  
  public final String component3()
  {
    return initiator_name;
  }
  
  public final String component4()
  {
    return operator;
  }
  
  public final IxigoTokenRequest copy(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    k.b(paramString1, "type");
    k.b(paramString2, "initiator_msisdn");
    k.b(paramString3, "initiator_name");
    k.b(paramString4, "operator");
    IxigoTokenRequest localIxigoTokenRequest = new com/truecaller/truepay/app/ui/webapps/ixigo/IxigoTokenRequest;
    localIxigoTokenRequest.<init>(paramString1, paramString2, paramString3, paramString4);
    return localIxigoTokenRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof IxigoTokenRequest;
      if (bool1)
      {
        paramObject = (IxigoTokenRequest)paramObject;
        String str1 = type;
        String str2 = type;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = initiator_msisdn;
          str2 = initiator_msisdn;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = initiator_name;
            str2 = initiator_name;
            bool1 = k.a(str1, str2);
            if (bool1)
            {
              str1 = operator;
              paramObject = operator;
              boolean bool2 = k.a(str1, paramObject);
              if (bool2) {
                break label112;
              }
            }
          }
        }
      }
      return false;
    }
    label112:
    return true;
  }
  
  public final String getInitiator_msisdn()
  {
    return initiator_msisdn;
  }
  
  public final String getInitiator_name()
  {
    return initiator_name;
  }
  
  public final String getOperator()
  {
    return operator;
  }
  
  public final String getType()
  {
    return type;
  }
  
  public final int hashCode()
  {
    String str1 = type;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = initiator_msisdn;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = initiator_name;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = operator;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("IxigoTokenRequest(type=");
    String str = type;
    localStringBuilder.append(str);
    localStringBuilder.append(", initiator_msisdn=");
    str = initiator_msisdn;
    localStringBuilder.append(str);
    localStringBuilder.append(", initiator_name=");
    str = initiator_name;
    localStringBuilder.append(str);
    localStringBuilder.append(", operator=");
    str = operator;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.ixigo.IxigoTokenRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */