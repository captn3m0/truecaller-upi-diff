package com.truecaller.truepay.app.ui.webapps.ixigo;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class IxigoAuthResponse$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    IxigoAuthResponse localIxigoAuthResponse = new com/truecaller/truepay/app/ui/webapps/ixigo/IxigoAuthResponse;
    int i = paramParcel.readInt();
    if (i != 0)
    {
      Parcelable.Creator localCreator = IxigoAuthData.CREATOR;
      paramParcel = (IxigoAuthData)localCreator.createFromParcel(paramParcel);
    }
    else
    {
      paramParcel = null;
    }
    localIxigoAuthResponse.<init>(paramParcel);
    return localIxigoAuthResponse;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new IxigoAuthResponse[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.ixigo.IxigoAuthResponse.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */