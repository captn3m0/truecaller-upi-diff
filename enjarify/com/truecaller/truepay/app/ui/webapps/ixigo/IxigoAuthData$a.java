package com.truecaller.truepay.app.ui.webapps.ixigo;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class IxigoAuthData$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    IxigoAuthData localIxigoAuthData = new com/truecaller/truepay/app/ui/webapps/ixigo/IxigoAuthData;
    paramParcel = paramParcel.readString();
    localIxigoAuthData.<init>(paramParcel);
    return localIxigoAuthData;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new IxigoAuthData[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.ixigo.IxigoAuthData.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */