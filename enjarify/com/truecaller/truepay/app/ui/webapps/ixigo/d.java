package com.truecaller.truepay.app.ui.webapps.ixigo;

import android.content.Intent;
import c.g.a.m;
import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.g;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.ui.webapps.a.a;
import com.truecaller.truepay.app.ui.webapps.a.b;
import com.truecaller.truepay.app.ui.webapps.models.DeviceConfig;
import com.truecaller.truepay.app.ui.webapps.models.WebAppConfig;
import com.truecaller.truepay.data.e.c;
import com.truecaller.utils.n;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

public final class d
  extends com.truecaller.truepay.app.ui.webapps.a.a
  implements a.a
{
  public static final d.a f;
  final c.d.f e;
  private final com.truecaller.truepay.app.ui.webapps.d.a g;
  private String h;
  private String i;
  private final c.d.f j;
  private final c k;
  private final a l;
  private final com.truecaller.truepay.app.ui.webapps.c.b m;
  private final com.truecaller.truepay.data.e.e n;
  private final com.google.gson.f o;
  private final com.truecaller.common.g.a p;
  private final n q;
  private final com.truecaller.truepay.data.b.a r;
  
  static
  {
    Object localObject1 = new g[1];
    Object localObject2 = new c/g/b/u;
    c.l.b localb = w.a(d.class);
    ((u)localObject2).<init>(localb, "deviceConfig", "getDeviceConfig()Lcom/truecaller/truepay/app/ui/webapps/models/DeviceConfig;");
    localObject2 = (g)w.a((t)localObject2);
    localObject1[0] = localObject2;
    d = (g[])localObject1;
    localObject1 = new com/truecaller/truepay/app/ui/webapps/ixigo/d$a;
    ((d.a)localObject1).<init>((byte)0);
    f = (d.a)localObject1;
  }
  
  public d(c.d.f paramf1, c.d.f paramf2, com.truecaller.truepay.app.ui.webapps.d.a parama, c paramc, a parama1, com.truecaller.truepay.app.ui.webapps.c.b paramb, com.truecaller.truepay.data.e.e parame, com.google.gson.f paramf, com.truecaller.common.g.a parama2, n paramn, com.truecaller.truepay.data.b.a parama3)
  {
    super(paramf1);
    j = paramf1;
    e = paramf2;
    k = paramc;
    l = parama1;
    m = paramb;
    n = parame;
    o = paramf;
    p = parama2;
    q = paramn;
    r = parama3;
    g = parama;
  }
  
  private final void a(String paramString)
  {
    a.b localb = (a.b)b;
    if (localb != null)
    {
      Object localObject = q;
      int i1 = R.string.ixigo_login_failed;
      Object[] arrayOfObject = new Object[0];
      localObject = ((n)localObject).a(i1, arrayOfObject);
      k.a(localObject, "resourceProvider.getStri…tring.ixigo_login_failed)");
      localb.c((String)localObject);
      localb.a(paramString);
      return;
    }
  }
  
  private final DeviceConfig b()
  {
    com.truecaller.truepay.app.ui.webapps.d.a locala = g;
    g localg = d[0];
    return locala.a(localg);
  }
  
  private final void d()
  {
    a.b localb = (a.b)b;
    if (localb != null)
    {
      Object localObject = q;
      int i1 = R.string.ixigo_login_failed;
      Object[] arrayOfObject = new Object[0];
      localObject = ((n)localObject).a(i1, arrayOfObject);
      k.a(localObject, "resourceProvider.getStri…tring.ixigo_login_failed)");
      localb.c((String)localObject);
      return;
    }
  }
  
  public final void a()
  {
    r.d("ixigo");
  }
  
  public final void a(int paramInt1, int paramInt2, Intent paramIntent)
  {
    int i1 = -1;
    Object[] arrayOfObject = null;
    int i2 = 100;
    Object localObject1;
    Object localObject2;
    Object localObject3;
    if (paramInt1 == i2)
    {
      if (paramInt2 == i1)
      {
        if (paramIntent != null)
        {
          localObject1 = (IxigoPaymentRequest)paramIntent.getParcelableExtra("payment_request");
        }
        else
        {
          paramInt1 = 0;
          localObject1 = null;
        }
        if (paramIntent != null)
        {
          localObject2 = paramIntent.getSerializableExtra("transaction_data");
        }
        else
        {
          paramInt2 = 0;
          localObject2 = null;
        }
        boolean bool = localObject2 instanceof p;
        if (!bool)
        {
          paramInt2 = 0;
          localObject2 = null;
        }
        localObject2 = (p)localObject2;
        if ((localObject1 != null) && (localObject2 != null))
        {
          localObject2 = (a.b)b;
          if (localObject2 != null)
          {
            paramIntent = q;
            i1 = R.string.ixigo_payment_success;
            arrayOfObject = new Object[0];
            paramIntent = paramIntent.a(i1, arrayOfObject);
            k.a(paramIntent, "resourceProvider.getStri…ng.ixigo_payment_success)");
            ((a.b)localObject2).c(paramIntent);
            k.b(localObject1, "receiver$0");
            paramIntent = new java/lang/StringBuilder;
            paramIntent.<init>();
            localObject3 = ((IxigoPaymentRequest)localObject1).getReturnUrl();
            paramIntent.append((String)localObject3);
            localObject3 = "?orderId=";
            paramIntent.append((String)localObject3);
            localObject1 = ((IxigoPaymentRequest)localObject1).getOrderId();
            paramIntent.append((String)localObject1);
            localObject1 = paramIntent.toString();
            ((a.b)localObject2).a((String)localObject1);
          }
          return;
        }
        localObject1 = (a.b)b;
        if (localObject1 != null)
        {
          localObject2 = q;
          i3 = R.string.ixigo_payment_not_found;
          localObject3 = new Object[0];
          localObject2 = ((n)localObject2).a(i3, (Object[])localObject3);
          k.a(localObject2, "resourceProvider.getStri….ixigo_payment_not_found)");
          ((a.b)localObject1).c((String)localObject2);
          return;
        }
        return;
      }
      localObject1 = (a.b)b;
      if (localObject1 != null)
      {
        localObject2 = q;
        i3 = R.string.ixigo_payment_failed;
        localObject3 = new Object[0];
        localObject2 = ((n)localObject2).a(i3, (Object[])localObject3);
        k.a(localObject2, "resourceProvider.getStri…ing.ixigo_payment_failed)");
        ((a.b)localObject1).c((String)localObject2);
        return;
      }
      return;
    }
    int i3 = 110;
    if (paramInt1 == i3)
    {
      if (paramInt2 == i1)
      {
        localObject1 = r;
        localObject2 = "ixigo";
        i3 = 1;
        ((com.truecaller.truepay.data.b.a)localObject1).a((String)localObject2, i3);
        localObject1 = h;
        if (localObject1 == null)
        {
          d();
          return;
        }
        localObject2 = i;
        if (localObject2 == null)
        {
          d();
          return;
        }
        try
        {
          paramIntent = new com/truecaller/truepay/app/ui/webapps/ixigo/d$d;
          paramIntent.<init>(this, (String)localObject1, (String)localObject2, null);
          paramIntent = (m)paramIntent;
          paramInt1 = 3;
          kotlinx.coroutines.e.b(this, null, paramIntent, paramInt1);
          return;
        }
        catch (IOException localIOException)
        {
          a((String)localObject2);
          return;
        }
      }
      localObject1 = r;
      localObject2 = "ixigo";
      ((com.truecaller.truepay.data.b.a)localObject1).a((String)localObject2, false);
      localObject1 = (a.b)b;
      if (localObject1 != null)
      {
        localObject2 = q;
        i3 = R.string.consent_denied;
        localObject3 = new Object[0];
        localObject2 = ((n)localObject2).a(i3, (Object[])localObject3);
        k.a(localObject2, "resourceProvider.getStri…(R.string.consent_denied)");
        ((a.b)localObject1).c((String)localObject2);
        ((a.b)localObject1).a();
        return;
      }
    }
  }
  
  public final String getAppData()
  {
    Object localObject1 = c;
    JSONObject localJSONObject1 = null;
    if (localObject1 == null) {
      return null;
    }
    Object localObject2 = b();
    String str1 = k.c("S,p+?735A8Y2.n'c");
    k.b(localObject2, "receiver$0");
    k.b(localObject1, "config");
    JSONObject localJSONObject2 = new org/json/JSONObject;
    localJSONObject2.<init>();
    int i1 = f;
    localJSONObject2.put("apiLevel", i1);
    String str2 = d;
    localJSONObject2.put("brand", str2);
    str2 = e;
    localJSONObject2.put("manufacturer", str2);
    str2 = g;
    localJSONObject2.put("device", str2);
    str2 = h;
    localJSONObject2 = localJSONObject2.put("product", str2);
    JSONObject localJSONObject3 = new org/json/JSONObject;
    localJSONObject3.<init>();
    String str3 = ((WebAppConfig)localObject1).getClientId();
    localJSONObject3.put("clientId", str3);
    str3 = b;
    localJSONObject3.put("deviceId", str3);
    str3 = c;
    localJSONObject3.put("uuid", str3);
    str3 = ((WebAppConfig)localObject1).getApiKey();
    localJSONObject3.put("apiKey", str3);
    str2 = "appVersion";
    int i2 = a;
    localJSONObject3.put(str2, i2);
    localJSONObject3.put("deviceInfo", localJSONObject2);
    localObject2 = "currencyCode";
    localObject1 = ((WebAppConfig)localObject1).getCurrencyCode();
    localJSONObject3.put((String)localObject2, localObject1);
    if (str1 != null)
    {
      localObject1 = str1;
      localObject1 = (CharSequence)str1;
      int i3 = ((CharSequence)localObject1).length();
      if (i3 > 0)
      {
        i3 = 1;
      }
      else
      {
        i3 = 0;
        localObject1 = null;
      }
      if (i3 == 0) {
        str1 = null;
      }
      if (str1 != null)
      {
        localObject1 = "Authorization";
        localJSONObject1 = localJSONObject3.put((String)localObject1, str1);
      }
    }
    return String.valueOf(localJSONObject1);
  }
  
  public final void loginUser(String paramString1, String paramString2)
  {
    k.b(paramString1, "logInSuccessJsFunction");
    String str = "logInFailureJsFunction";
    k.b(paramString2, str);
    h = paramString1;
    i = paramString2;
    paramString1 = (a.b)b;
    if (paramString1 != null)
    {
      paramString1.a("partnerName", "ixigo");
      return;
    }
  }
  
  public final void logoutUser()
  {
    k.b("S,p+?735A8Y2.n'c");
  }
  
  public final void onPaymentRequested(String paramString)
  {
    k.b(paramString, "paymentRequestJsonString");
    Object localObject = new com/truecaller/truepay/app/ui/webapps/ixigo/d$c;
    ((d.c)localObject).<init>(this, paramString, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.b(this, null, (m)localObject, 3);
  }
  
  public final void quit()
  {
    Object localObject = new com/truecaller/truepay/app/ui/webapps/ixigo/d$e;
    ((d.e)localObject).<init>(this, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.b(this, null, (m)localObject, 3);
  }
  
  public final void trackEvent(String paramString1, String paramString2)
  {
    k.b(paramString1, "eventName");
    Object localObject = "eventJson";
    k.b(paramString2, (String)localObject);
    try
    {
      localObject = new com/truecaller/truepay/app/ui/webapps/ixigo/d$f;
      ((d.f)localObject).<init>(this, paramString2, paramString1, null);
      localObject = (m)localObject;
      int i1 = 3;
      kotlinx.coroutines.e.b(this, null, (m)localObject, i1);
      return;
    }
    catch (JSONException localJSONException) {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.ixigo.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */