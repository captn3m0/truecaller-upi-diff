package com.truecaller.truepay.app.ui.webapps.ixigo;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.google.gson.f;
import com.truecaller.truepay.app.ui.webapps.models.WebAppConfig;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import kotlinx.coroutines.ag;
import org.json.JSONObject;

final class d$f$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  d$f$a(d.f paramf, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/truepay/app/ui/webapps/ixigo/d$f$a;
    d.f localf = b;
    locala.<init>(localf, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = d.c(b.b);
        localObject1 = b.c;
        Object localObject2 = new com/truecaller/truepay/app/ui/webapps/ixigo/d$f$a$a;
        ((d.f.a.a)localObject2).<init>();
        localObject2 = b;
        paramObject = ((f)paramObject).a((String)localObject1, (Type)localObject2);
        c.g.b.k.a(paramObject, "gson.fromJson(eventJson,…<String, Any>>() {}.type)");
        paramObject = (Map)paramObject;
        localObject1 = new java/util/LinkedHashMap;
        ((LinkedHashMap)localObject1).<init>();
        localObject1 = (Map)localObject1;
        paramObject = ((Map)paramObject).entrySet().iterator();
        boolean bool3;
        for (;;)
        {
          bool2 = ((Iterator)paramObject).hasNext();
          if (!bool2) {
            break;
          }
          localObject2 = (Map.Entry)((Iterator)paramObject).next();
          localObject3 = ((Map.Entry)localObject2).getValue();
          bool3 = localObject3 instanceof String;
          boolean bool4 = ((Map.Entry)localObject2).getValue() instanceof Integer;
          bool3 |= bool4;
          bool4 = ((Map.Entry)localObject2).getValue() instanceof Boolean;
          bool3 |= bool4;
          bool4 = ((Map.Entry)localObject2).getValue() instanceof Long;
          bool3 |= bool4;
          Object localObject4 = ((Map.Entry)localObject2).getValue();
          bool4 = localObject4 instanceof Double;
          bool3 |= bool4;
          if (bool3)
          {
            localObject3 = ((Map.Entry)localObject2).getKey();
            localObject2 = ((Map.Entry)localObject2).getValue();
            ((Map)localObject1).put(localObject3, localObject2);
          }
        }
        int j = 2;
        paramObject = new String[j];
        boolean bool2 = false;
        localObject2 = null;
        Object localObject3 = d.a(b.b);
        if (localObject3 != null)
        {
          localObject3 = ((WebAppConfig)localObject3).getJsInterfaceName();
        }
        else
        {
          bool3 = false;
          localObject3 = null;
        }
        paramObject[0] = localObject3;
        localObject3 = localObject1.toString();
        paramObject[1] = localObject3;
        paramObject = new org/json/JSONObject;
        ((JSONObject)paramObject).<init>((Map)localObject1);
        return paramObject;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.ixigo.d.f.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */