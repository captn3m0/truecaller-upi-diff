package com.truecaller.truepay.app.ui.webapps.ixigo;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class IxigoPaymentRequest$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    IxigoPaymentRequest localIxigoPaymentRequest = new com/truecaller/truepay/app/ui/webapps/ixigo/IxigoPaymentRequest;
    long l1 = paramParcel.readLong();
    int i = paramParcel.readInt();
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    long l2 = paramParcel.readLong();
    long l3 = paramParcel.readLong();
    String str3 = paramParcel.readString();
    localIxigoPaymentRequest.<init>(l1, i, str1, str2, l2, l3, str3);
    return localIxigoPaymentRequest;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new IxigoPaymentRequest[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.ixigo.IxigoPaymentRequest.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */