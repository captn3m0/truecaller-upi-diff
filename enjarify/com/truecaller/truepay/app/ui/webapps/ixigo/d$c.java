package com.truecaller.truepay.app.ui.webapps.ixigo;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.google.gson.f;
import com.truecaller.truepay.app.ui.webapps.a.b;
import com.truecaller.truepay.app.ui.webapps.models.WebAppConfig;
import kotlinx.coroutines.ag;

final class d$c
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  d$c(d paramd, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/truepay/app/ui/webapps/ixigo/d$c;
    d locald = b;
    String str = c;
    localc.<init>(locald, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        int j = 2;
        paramObject = new String[j];
        bool = false;
        localObject1 = null;
        Object localObject2 = d.a(b);
        if (localObject2 != null) {
          localObject2 = ((WebAppConfig)localObject2).getJsInterfaceName();
        } else {
          localObject2 = null;
        }
        paramObject[0] = localObject2;
        bool = true;
        localObject2 = c;
        paramObject[bool] = localObject2;
        paramObject = d.b(b);
        if (paramObject == null) {
          return x.a;
        }
        localObject1 = d.c(b);
        localObject2 = c;
        localObject1 = (IxigoPaymentRequest)((f)localObject1).a((String)localObject2, IxigoPaymentRequest.class);
        localObject2 = d.d(b);
        String str = ((IxigoPaymentRequest)localObject1).getOrderId();
        ((com.truecaller.truepay.data.b.a)localObject2).e("ixigo", str);
        c.g.b.k.a(localObject1, "request");
        ((a.b)paramObject).a((IxigoPaymentRequest)localObject1);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.ixigo.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */