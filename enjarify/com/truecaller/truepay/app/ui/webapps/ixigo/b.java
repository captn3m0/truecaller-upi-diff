package com.truecaller.truepay.app.ui.webapps.ixigo;

import com.google.firebase.perf.network.FirebasePerfOkHttpClient;
import com.google.gson.f;
import com.truecaller.log.d;
import com.truecaller.truepay.app.ui.webapps.models.DeviceConfig;
import com.truecaller.truepay.app.ui.webapps.models.WebAppConfig;
import java.io.IOException;
import okhttp3.ab.a;
import okhttp3.ac;
import okhttp3.ad;
import okhttp3.ae;
import okhttp3.r.a;
import okhttp3.y;

public final class b
  implements a
{
  private final y a;
  private final f b;
  
  public b(y paramy, f paramf)
  {
    a = paramy;
    b = paramf;
  }
  
  public final Object a(DeviceConfig paramDeviceConfig, WebAppConfig paramWebAppConfig, String paramString)
  {
    Object localObject = new okhttp3/r$a;
    ((r.a)localObject).<init>();
    paramString = ((r.a)localObject).a("authCode", paramString).a();
    localObject = new okhttp3/ab$a;
    ((ab.a)localObject).<init>();
    String str1 = paramWebAppConfig.getAuthUrl();
    ((ab.a)localObject).a(str1);
    String str2 = paramWebAppConfig.getClientId();
    ((ab.a)localObject).b("ixiSrc", str2);
    str2 = paramWebAppConfig.getClientId();
    ((ab.a)localObject).b("clientId", str2);
    str1 = "apiKey";
    paramWebAppConfig = paramWebAppConfig.getApiKey();
    ((ab.a)localObject).b(str1, paramWebAppConfig);
    paramDeviceConfig = b;
    ((ab.a)localObject).b("deviceId", paramDeviceConfig);
    paramString = (ac)paramString;
    ((ab.a)localObject).a(paramString);
    paramDeviceConfig = ((ab.a)localObject).a();
    paramWebAppConfig = null;
    try
    {
      paramString = a;
      paramDeviceConfig = paramString.a(paramDeviceConfig);
      paramDeviceConfig = FirebasePerfOkHttpClient.execute(paramDeviceConfig);
      paramDeviceConfig = paramDeviceConfig.d();
      if (paramDeviceConfig != null)
      {
        paramDeviceConfig = paramDeviceConfig.g();
        if (paramDeviceConfig != null)
        {
          paramString = b;
          localObject = IxigoAuthResponse.class;
          paramDeviceConfig = paramString.a(paramDeviceConfig, (Class)localObject);
          paramDeviceConfig = (IxigoAuthResponse)paramDeviceConfig;
          paramDeviceConfig = paramDeviceConfig.getData();
          if (paramDeviceConfig != null) {
            return paramDeviceConfig.getAccess_token();
          }
          return null;
        }
      }
      return null;
    }
    catch (IOException localIOException)
    {
      d.a((Throwable)localIOException);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.ixigo.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */