package com.truecaller.truepay.app.ui.webapps.ixigo;

import c.d.a.a;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.truepay.app.ui.webapps.c.b;
import com.truecaller.truepay.app.ui.webapps.models.WebAppConfig;
import com.truecaller.truepay.data.e.e;
import kotlinx.coroutines.ag;

final class d$b
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag d;
  
  d$b(d paramd, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/truepay/app/ui/webapps/ixigo/d$b;
    d locald = c;
    localb.<init>(locald, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    int j = 1;
    String str1 = null;
    Object localObject3;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label341;
      }
      paramObject = d.j(c).a();
      if (paramObject == null) {
        return null;
      }
      c.g.b.k.a(paramObject, "msisdnPref.get() ?: return@withContext null");
      localObject2 = d.k(c);
      IxigoTokenRequest localIxigoTokenRequest = new com/truecaller/truepay/app/ui/webapps/ixigo/IxigoTokenRequest;
      String str2 = d.a(d.l(c));
      int k = 9;
      localObject3 = localIxigoTokenRequest;
      localIxigoTokenRequest.<init>(null, (String)paramObject, str2, null, k, null);
      a = paramObject;
      b = j;
      paramObject = ((b)localObject2).a(localIxigoTokenRequest);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (com.truecaller.truepay.app.ui.webapps.models.c)paramObject;
    boolean bool2 = paramObject instanceof com.truecaller.truepay.app.ui.webapps.models.d;
    boolean bool1 = false;
    Object localObject2 = null;
    int m = 2;
    if (bool2)
    {
      localObject1 = new String[m];
      localObject3 = d.a(c);
      if (localObject3 != null) {
        str1 = ((WebAppConfig)localObject3).getJsInterfaceName();
      }
      localObject1[0] = str1;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Got user token: ");
      paramObject = (com.truecaller.truepay.app.ui.webapps.models.d)paramObject;
      str1 = ((IxigoTokenResponse)a).getToken();
      ((StringBuilder)localObject2).append(str1);
      localObject2 = ((StringBuilder)localObject2).toString();
      localObject1[j] = localObject2;
      return ((IxigoTokenResponse)a).getToken();
    }
    paramObject = new String[m];
    localObject1 = d.a(c);
    if (localObject1 != null)
    {
      localObject1 = ((WebAppConfig)localObject1).getJsInterfaceName();
    }
    else
    {
      bool2 = false;
      localObject1 = null;
    }
    paramObject[0] = localObject1;
    paramObject[j] = "Failed to get user token";
    return null;
    label341:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.ixigo.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */