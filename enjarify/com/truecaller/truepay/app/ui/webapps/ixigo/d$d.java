package com.truecaller.truepay.app.ui.webapps.ixigo;

import c.d.a.a;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.webapps.a.b;
import com.truecaller.truepay.app.ui.webapps.models.WebAppConfig;
import com.truecaller.utils.n;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class d$d
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  Object c;
  int d;
  private ag h;
  
  d$d(d paramd, String paramString1, String paramString2, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/truepay/app/ui/webapps/ixigo/d$d;
    d locald1 = e;
    String str1 = f;
    String str2 = g;
    locald.<init>(locald1, str1, str2, paramc);
    paramObject = (ag)paramObject;
    h = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = d;
    int k = 0;
    Object[] arrayOfObject = null;
    Object localObject2 = null;
    int m = 1;
    Object localObject3;
    Object localObject4;
    Object localObject5;
    Object localObject6;
    Object localObject7;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label319;
      }
      throw a;
    case 1: 
      localObject3 = (WebAppConfig)b;
      localObject4 = (ag)a;
      boolean bool3 = paramObject instanceof o.b;
      if (!bool3) {
        localObject5 = localObject4;
      } else {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label505;
      }
      localObject4 = h;
      paramObject = e;
      localObject3 = d.a((d)paramObject);
      if (localObject3 == null) {
        break label482;
      }
      paramObject = e;
      a = localObject4;
      b = localObject3;
      d = m;
      localObject6 = e;
      localObject7 = new com/truecaller/truepay/app/ui/webapps/ixigo/d$b;
      ((d.b)localObject7).<init>((d)paramObject, null);
      localObject7 = (m)localObject7;
      paramObject = g.a((f)localObject6, (m)localObject7, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      localObject5 = localObject4;
    }
    paramObject = (String)paramObject;
    if (paramObject != null)
    {
      localObject2 = d.e(e);
      Object localObject8 = new com/truecaller/truepay/app/ui/webapps/ixigo/d$d$a;
      localObject7 = null;
      localObject4 = localObject8;
      localObject6 = paramObject;
      ((d.d.a)localObject8).<init>((String)paramObject, null, (WebAppConfig)localObject3, this, (ag)localObject5);
      localObject8 = (m)localObject8;
      a = localObject5;
      b = localObject3;
      c = paramObject;
      int i1 = 2;
      d = i1;
      paramObject = g.a((f)localObject2, (m)localObject8, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      label319:
      paramObject = (String)paramObject;
      if (paramObject != null)
      {
        localObject1 = paramObject;
        localObject1 = (CharSequence)paramObject;
        int n = ((CharSequence)localObject1).length();
        if (n > 0) {
          k = 1;
        }
        if (k == m)
        {
          d.h(e).b("S,p+?735A8Y2.n'c", (String)paramObject);
          localObject1 = e;
          localObject3 = f;
          d.a((d)localObject1, (String)localObject3, (String)paramObject);
          break label407;
        }
      }
      paramObject = e;
      localObject1 = g;
      d.a((d)paramObject, (String)localObject1);
      label407:
      localObject2 = x.a;
    }
    else
    {
      paramObject = d.b(e);
      if (paramObject != null)
      {
        localObject1 = d.i(e);
        int j = R.string.ixigo_login_failed;
        arrayOfObject = new Object[0];
        localObject1 = ((n)localObject1).a(j, arrayOfObject);
        localObject3 = "resourceProvider.getStri…tring.ixigo_login_failed)";
        c.g.b.k.a(localObject1, (String)localObject3);
        ((a.b)paramObject).b((String)localObject1);
        localObject2 = x.a;
      }
    }
    if (localObject2 == null)
    {
      label482:
      paramObject = e;
      localObject1 = g;
      d.a((d)paramObject, (String)localObject1);
      paramObject = x.a;
    }
    return x.a;
    label505:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.ixigo.d.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */