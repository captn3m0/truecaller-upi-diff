package com.truecaller.truepay.app.ui.webapps.ixigo;

import c.g.b.k;
import java.util.List;

public final class IxigoBookingDetail
{
  private final String cancel_status;
  private final List details;
  private final String details_url;
  
  public IxigoBookingDetail(String paramString1, String paramString2, List paramList)
  {
    cancel_status = paramString1;
    details_url = paramString2;
    details = paramList;
  }
  
  public final String component1()
  {
    return cancel_status;
  }
  
  public final String component2()
  {
    return details_url;
  }
  
  public final List component3()
  {
    return details;
  }
  
  public final IxigoBookingDetail copy(String paramString1, String paramString2, List paramList)
  {
    k.b(paramString1, "cancel_status");
    k.b(paramString2, "details_url");
    k.b(paramList, "details");
    IxigoBookingDetail localIxigoBookingDetail = new com/truecaller/truepay/app/ui/webapps/ixigo/IxigoBookingDetail;
    localIxigoBookingDetail.<init>(paramString1, paramString2, paramList);
    return localIxigoBookingDetail;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof IxigoBookingDetail;
      if (bool1)
      {
        paramObject = (IxigoBookingDetail)paramObject;
        Object localObject = cancel_status;
        String str = cancel_status;
        bool1 = k.a(localObject, str);
        if (bool1)
        {
          localObject = details_url;
          str = details_url;
          bool1 = k.a(localObject, str);
          if (bool1)
          {
            localObject = details;
            paramObject = details;
            boolean bool2 = k.a(localObject, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String getCancel_status()
  {
    return cancel_status;
  }
  
  public final List getDetails()
  {
    return details;
  }
  
  public final String getDetails_url()
  {
    return details_url;
  }
  
  public final int hashCode()
  {
    String str = cancel_status;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = details_url;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = details;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("IxigoBookingDetail(cancel_status=");
    Object localObject = cancel_status;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", details_url=");
    localObject = details_url;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", details=");
    localObject = details;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.ixigo.IxigoBookingDetail
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */