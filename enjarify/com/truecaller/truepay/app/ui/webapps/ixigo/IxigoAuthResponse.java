package com.truecaller.truepay.app.ui.webapps.ixigo;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class IxigoAuthResponse
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final IxigoAuthData data;
  
  static
  {
    IxigoAuthResponse.a locala = new com/truecaller/truepay/app/ui/webapps/ixigo/IxigoAuthResponse$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public IxigoAuthResponse(IxigoAuthData paramIxigoAuthData)
  {
    data = paramIxigoAuthData;
  }
  
  public final IxigoAuthData component1()
  {
    return data;
  }
  
  public final IxigoAuthResponse copy(IxigoAuthData paramIxigoAuthData)
  {
    IxigoAuthResponse localIxigoAuthResponse = new com/truecaller/truepay/app/ui/webapps/ixigo/IxigoAuthResponse;
    localIxigoAuthResponse.<init>(paramIxigoAuthData);
    return localIxigoAuthResponse;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof IxigoAuthResponse;
      if (bool1)
      {
        paramObject = (IxigoAuthResponse)paramObject;
        IxigoAuthData localIxigoAuthData = data;
        paramObject = data;
        boolean bool2 = k.a(localIxigoAuthData, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final IxigoAuthData getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    IxigoAuthData localIxigoAuthData = data;
    if (localIxigoAuthData != null) {
      return localIxigoAuthData.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("IxigoAuthResponse(data=");
    IxigoAuthData localIxigoAuthData = data;
    localStringBuilder.append(localIxigoAuthData);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    IxigoAuthData localIxigoAuthData = data;
    if (localIxigoAuthData != null)
    {
      paramParcel.writeInt(1);
      localIxigoAuthData.writeToParcel(paramParcel, 0);
      return;
    }
    paramParcel.writeInt(0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.ixigo.IxigoAuthResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */