package com.truecaller.truepay.app.ui.webapps.ixigo;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;
import java.util.List;

public final class IxigoBookingData
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String arrival_time;
  private final String booking_date;
  private final String depart_time;
  private final String destination_code;
  private final String destination_name;
  private final String flight_number;
  private final List passenger_names;
  private final String pnr;
  private final String service_provider_code;
  private final String service_provider_name;
  private final String source_code;
  private final String source_name;
  
  static
  {
    IxigoBookingData.a locala = new com/truecaller/truepay/app/ui/webapps/ixigo/IxigoBookingData$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public IxigoBookingData(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11, List paramList)
  {
    booking_date = paramString1;
    source_name = paramString2;
    source_code = paramString3;
    destination_name = paramString4;
    destination_code = paramString5;
    pnr = paramString6;
    depart_time = paramString7;
    arrival_time = paramString8;
    service_provider_name = paramString9;
    service_provider_code = paramString10;
    flight_number = paramString11;
    passenger_names = paramList;
  }
  
  public final String component1()
  {
    return booking_date;
  }
  
  public final String component10()
  {
    return service_provider_code;
  }
  
  public final String component11()
  {
    return flight_number;
  }
  
  public final List component12()
  {
    return passenger_names;
  }
  
  public final String component2()
  {
    return source_name;
  }
  
  public final String component3()
  {
    return source_code;
  }
  
  public final String component4()
  {
    return destination_name;
  }
  
  public final String component5()
  {
    return destination_code;
  }
  
  public final String component6()
  {
    return pnr;
  }
  
  public final String component7()
  {
    return depart_time;
  }
  
  public final String component8()
  {
    return arrival_time;
  }
  
  public final String component9()
  {
    return service_provider_name;
  }
  
  public final IxigoBookingData copy(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11, List paramList)
  {
    k.b(paramString1, "booking_date");
    k.b(paramString2, "source_name");
    k.b(paramString3, "source_code");
    k.b(paramString4, "destination_name");
    k.b(paramString5, "destination_code");
    k.b(paramString6, "pnr");
    k.b(paramString7, "depart_time");
    k.b(paramString8, "arrival_time");
    k.b(paramString9, "service_provider_name");
    k.b(paramString10, "service_provider_code");
    k.b(paramString11, "flight_number");
    k.b(paramList, "passenger_names");
    IxigoBookingData localIxigoBookingData = new com/truecaller/truepay/app/ui/webapps/ixigo/IxigoBookingData;
    localIxigoBookingData.<init>(paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7, paramString8, paramString9, paramString10, paramString11, paramList);
    return localIxigoBookingData;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof IxigoBookingData;
      if (bool1)
      {
        paramObject = (IxigoBookingData)paramObject;
        Object localObject = booking_date;
        String str = booking_date;
        bool1 = k.a(localObject, str);
        if (bool1)
        {
          localObject = source_name;
          str = source_name;
          bool1 = k.a(localObject, str);
          if (bool1)
          {
            localObject = source_code;
            str = source_code;
            bool1 = k.a(localObject, str);
            if (bool1)
            {
              localObject = destination_name;
              str = destination_name;
              bool1 = k.a(localObject, str);
              if (bool1)
              {
                localObject = destination_code;
                str = destination_code;
                bool1 = k.a(localObject, str);
                if (bool1)
                {
                  localObject = pnr;
                  str = pnr;
                  bool1 = k.a(localObject, str);
                  if (bool1)
                  {
                    localObject = depart_time;
                    str = depart_time;
                    bool1 = k.a(localObject, str);
                    if (bool1)
                    {
                      localObject = arrival_time;
                      str = arrival_time;
                      bool1 = k.a(localObject, str);
                      if (bool1)
                      {
                        localObject = service_provider_name;
                        str = service_provider_name;
                        bool1 = k.a(localObject, str);
                        if (bool1)
                        {
                          localObject = service_provider_code;
                          str = service_provider_code;
                          bool1 = k.a(localObject, str);
                          if (bool1)
                          {
                            localObject = flight_number;
                            str = flight_number;
                            bool1 = k.a(localObject, str);
                            if (bool1)
                            {
                              localObject = passenger_names;
                              paramObject = passenger_names;
                              boolean bool2 = k.a(localObject, paramObject);
                              if (bool2) {
                                break label288;
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label288:
    return true;
  }
  
  public final String getArrival_time()
  {
    return arrival_time;
  }
  
  public final String getBooking_date()
  {
    return booking_date;
  }
  
  public final String getDepart_time()
  {
    return depart_time;
  }
  
  public final String getDestination_code()
  {
    return destination_code;
  }
  
  public final String getDestination_name()
  {
    return destination_name;
  }
  
  public final String getFlight_number()
  {
    return flight_number;
  }
  
  public final List getPassenger_names()
  {
    return passenger_names;
  }
  
  public final String getPnr()
  {
    return pnr;
  }
  
  public final String getService_provider_code()
  {
    return service_provider_code;
  }
  
  public final String getService_provider_name()
  {
    return service_provider_name;
  }
  
  public final String getSource_code()
  {
    return source_code;
  }
  
  public final String getSource_name()
  {
    return source_name;
  }
  
  public final int hashCode()
  {
    String str = booking_date;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = source_name;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = source_code;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = destination_name;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = destination_code;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = pnr;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = depart_time;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = arrival_time;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = service_provider_name;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = service_provider_code;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = flight_number;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = passenger_names;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("IxigoBookingData(booking_date=");
    Object localObject = booking_date;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", source_name=");
    localObject = source_name;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", source_code=");
    localObject = source_code;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", destination_name=");
    localObject = destination_name;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", destination_code=");
    localObject = destination_code;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", pnr=");
    localObject = pnr;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", depart_time=");
    localObject = depart_time;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", arrival_time=");
    localObject = arrival_time;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", service_provider_name=");
    localObject = service_provider_name;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", service_provider_code=");
    localObject = service_provider_code;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", flight_number=");
    localObject = flight_number;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", passenger_names=");
    localObject = passenger_names;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    Object localObject = booking_date;
    paramParcel.writeString((String)localObject);
    localObject = source_name;
    paramParcel.writeString((String)localObject);
    localObject = source_code;
    paramParcel.writeString((String)localObject);
    localObject = destination_name;
    paramParcel.writeString((String)localObject);
    localObject = destination_code;
    paramParcel.writeString((String)localObject);
    localObject = pnr;
    paramParcel.writeString((String)localObject);
    localObject = depart_time;
    paramParcel.writeString((String)localObject);
    localObject = arrival_time;
    paramParcel.writeString((String)localObject);
    localObject = service_provider_name;
    paramParcel.writeString((String)localObject);
    localObject = service_provider_code;
    paramParcel.writeString((String)localObject);
    localObject = flight_number;
    paramParcel.writeString((String)localObject);
    localObject = passenger_names;
    paramParcel.writeStringList((List)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.ixigo.IxigoBookingData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */