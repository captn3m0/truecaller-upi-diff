package com.truecaller.truepay.app.ui.webapps.ixigo;

public class BaseWebAppResponse
{
  private String status;
  
  public final String getStatus()
  {
    return status;
  }
  
  public final void setStatus(String paramString)
  {
    status = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.ixigo.BaseWebAppResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */