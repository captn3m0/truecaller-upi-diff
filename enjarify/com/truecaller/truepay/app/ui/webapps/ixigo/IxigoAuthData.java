package com.truecaller.truepay.app.ui.webapps.ixigo;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class IxigoAuthData
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String access_token;
  
  static
  {
    IxigoAuthData.a locala = new com/truecaller/truepay/app/ui/webapps/ixigo/IxigoAuthData$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public IxigoAuthData(String paramString)
  {
    access_token = paramString;
  }
  
  public final String component1()
  {
    return access_token;
  }
  
  public final IxigoAuthData copy(String paramString)
  {
    IxigoAuthData localIxigoAuthData = new com/truecaller/truepay/app/ui/webapps/ixigo/IxigoAuthData;
    localIxigoAuthData.<init>(paramString);
    return localIxigoAuthData;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof IxigoAuthData;
      if (bool1)
      {
        paramObject = (IxigoAuthData)paramObject;
        String str = access_token;
        paramObject = access_token;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getAccess_token()
  {
    return access_token;
  }
  
  public final int hashCode()
  {
    String str = access_token;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("IxigoAuthData(access_token=");
    String str = access_token;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = access_token;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.ixigo.IxigoAuthData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */