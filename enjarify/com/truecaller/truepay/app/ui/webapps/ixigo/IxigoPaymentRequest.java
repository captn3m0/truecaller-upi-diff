package com.truecaller.truepay.app.ui.webapps.ixigo;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class IxigoPaymentRequest
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String currencyCode;
  private final long ixigoMoney;
  private final String orderId;
  private final long payableAmount;
  private final int productType;
  private final String returnUrl;
  private final long totalAmount;
  
  static
  {
    IxigoPaymentRequest.a locala = new com/truecaller/truepay/app/ui/webapps/ixigo/IxigoPaymentRequest$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public IxigoPaymentRequest(long paramLong1, int paramInt, String paramString1, String paramString2, long paramLong2, long paramLong3, String paramString3)
  {
    ixigoMoney = paramLong1;
    productType = paramInt;
    orderId = paramString1;
    currencyCode = paramString2;
    payableAmount = paramLong2;
    totalAmount = paramLong3;
    returnUrl = paramString3;
  }
  
  public final long component1()
  {
    return ixigoMoney;
  }
  
  public final int component2()
  {
    return productType;
  }
  
  public final String component3()
  {
    return orderId;
  }
  
  public final String component4()
  {
    return currencyCode;
  }
  
  public final long component5()
  {
    return payableAmount;
  }
  
  public final long component6()
  {
    return totalAmount;
  }
  
  public final String component7()
  {
    return returnUrl;
  }
  
  public final IxigoPaymentRequest copy(long paramLong1, int paramInt, String paramString1, String paramString2, long paramLong2, long paramLong3, String paramString3)
  {
    k.b(paramString1, "orderId");
    k.b(paramString2, "currencyCode");
    k.b(paramString3, "returnUrl");
    IxigoPaymentRequest localIxigoPaymentRequest = new com/truecaller/truepay/app/ui/webapps/ixigo/IxigoPaymentRequest;
    localIxigoPaymentRequest.<init>(paramLong1, paramInt, paramString1, paramString2, paramLong2, paramLong3, paramString3);
    return localIxigoPaymentRequest;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof IxigoPaymentRequest;
      if (bool2)
      {
        paramObject = (IxigoPaymentRequest)paramObject;
        long l1 = ixigoMoney;
        long l2 = ixigoMoney;
        bool2 = l1 < l2;
        String str1;
        if (!bool2)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          str1 = null;
        }
        if (bool2)
        {
          int i = productType;
          int j = productType;
          if (i == j)
          {
            i = 1;
          }
          else
          {
            i = 0;
            str1 = null;
          }
          if (i != 0)
          {
            str1 = orderId;
            String str2 = orderId;
            boolean bool3 = k.a(str1, str2);
            if (bool3)
            {
              str1 = currencyCode;
              str2 = currencyCode;
              bool3 = k.a(str1, str2);
              if (bool3)
              {
                l1 = payableAmount;
                l2 = payableAmount;
                bool3 = l1 < l2;
                if (!bool3)
                {
                  bool3 = true;
                }
                else
                {
                  bool3 = false;
                  str1 = null;
                }
                if (bool3)
                {
                  l1 = totalAmount;
                  l2 = totalAmount;
                  bool3 = l1 < l2;
                  if (!bool3)
                  {
                    bool3 = true;
                  }
                  else
                  {
                    bool3 = false;
                    str1 = null;
                  }
                  if (bool3)
                  {
                    str1 = returnUrl;
                    paramObject = returnUrl;
                    boolean bool4 = k.a(str1, paramObject);
                    if (bool4) {
                      return bool1;
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final String getCurrencyCode()
  {
    return currencyCode;
  }
  
  public final long getIxigoMoney()
  {
    return ixigoMoney;
  }
  
  public final String getOrderId()
  {
    return orderId;
  }
  
  public final long getPayableAmount()
  {
    return payableAmount;
  }
  
  public final int getProductType()
  {
    return productType;
  }
  
  public final String getReturnUrl()
  {
    return returnUrl;
  }
  
  public final long getTotalAmount()
  {
    return totalAmount;
  }
  
  public final int hashCode()
  {
    long l1 = ixigoMoney;
    int i = 32;
    long l2 = l1 >>> i;
    l1 ^= l2;
    int j = (int)l1 * 31;
    int k = productType;
    j = (j + k) * 31;
    String str = orderId;
    int m = 0;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    j = (j + k) * 31;
    str = currencyCode;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    j = (j + k) * 31;
    long l3 = payableAmount;
    long l4 = l3 >>> i;
    k = (int)(l3 ^ l4);
    j = (j + k) * 31;
    l3 = totalAmount;
    l4 = l3 >>> i;
    l3 ^= l4;
    k = (int)l3;
    j = (j + k) * 31;
    str = returnUrl;
    if (str != null) {
      m = str.hashCode();
    }
    return j + m;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("IxigoPaymentRequest(ixigoMoney=");
    long l = ixigoMoney;
    localStringBuilder.append(l);
    localStringBuilder.append(", productType=");
    int i = productType;
    localStringBuilder.append(i);
    localStringBuilder.append(", orderId=");
    String str = orderId;
    localStringBuilder.append(str);
    localStringBuilder.append(", currencyCode=");
    str = currencyCode;
    localStringBuilder.append(str);
    localStringBuilder.append(", payableAmount=");
    l = payableAmount;
    localStringBuilder.append(l);
    localStringBuilder.append(", totalAmount=");
    l = totalAmount;
    localStringBuilder.append(l);
    localStringBuilder.append(", returnUrl=");
    str = returnUrl;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    long l = ixigoMoney;
    paramParcel.writeLong(l);
    paramInt = productType;
    paramParcel.writeInt(paramInt);
    String str = orderId;
    paramParcel.writeString(str);
    str = currencyCode;
    paramParcel.writeString(str);
    l = payableAmount;
    paramParcel.writeLong(l);
    l = totalAmount;
    paramParcel.writeLong(l);
    str = returnUrl;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.ixigo.IxigoPaymentRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */