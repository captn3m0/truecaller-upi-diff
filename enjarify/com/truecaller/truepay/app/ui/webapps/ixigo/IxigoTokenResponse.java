package com.truecaller.truepay.app.ui.webapps.ixigo;

import c.g.b.k;

public final class IxigoTokenResponse
  extends BaseWebAppResponse
{
  private final String token;
  
  public IxigoTokenResponse(String paramString)
  {
    token = paramString;
  }
  
  public final String component1()
  {
    return token;
  }
  
  public final IxigoTokenResponse copy(String paramString)
  {
    k.b(paramString, "token");
    IxigoTokenResponse localIxigoTokenResponse = new com/truecaller/truepay/app/ui/webapps/ixigo/IxigoTokenResponse;
    localIxigoTokenResponse.<init>(paramString);
    return localIxigoTokenResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof IxigoTokenResponse;
      if (bool1)
      {
        paramObject = (IxigoTokenResponse)paramObject;
        String str = token;
        paramObject = token;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getToken()
  {
    return token;
  }
  
  public final int hashCode()
  {
    String str = token;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("IxigoTokenResponse(token=");
    String str = token;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.ixigo.IxigoTokenResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */