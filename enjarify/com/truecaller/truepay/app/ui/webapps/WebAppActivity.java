package com.truecaller.truepay.app.ui.webapps;

import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.f;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import c.a.ag;
import c.g.b.k;
import c.n;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.payments.views.activities.PaymentsActivity;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.ui.webapps.b.b;
import com.truecaller.truepay.app.ui.webapps.ixigo.IxigoPaymentRequest;
import com.truecaller.truepay.app.ui.webapps.models.DeviceConfig;
import com.truecaller.truepay.app.ui.webapps.models.JsConfig;
import com.truecaller.truepay.app.ui.webapps.models.WebAppConfig;
import java.io.Serializable;
import java.util.HashMap;

public class WebAppActivity
  extends AppCompatActivity
  implements b.b
{
  public static final WebAppActivity.a b;
  public b.a a;
  private HashMap d;
  
  static
  {
    WebAppActivity.a locala = new com/truecaller/truepay/app/ui/webapps/WebAppActivity$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public static final Intent a(Context paramContext, com.truecaller.truepay.app.ui.payments.models.a parama)
  {
    k.b(paramContext, "context");
    k.b(parama, "utilityEntry");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, WebAppActivity.class);
    parama = (Serializable)parama;
    localIntent.putExtra("utility_entry", parama);
    localIntent.putExtra("extra_launch_type", "webapp");
    return localIntent;
  }
  
  public static final Intent a(Context paramContext, com.truecaller.truepay.app.ui.payments.models.a parama, String paramString)
  {
    k.b(paramContext, "context");
    k.b(parama, "utilityEntry");
    k.b(paramString, "detailsUrl");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, WebAppActivity.class);
    parama = (Serializable)parama;
    localIntent.putExtra("utility_entry", parama);
    localIntent.putExtra("extra_launch_type", "details");
    localIntent.putExtra("details_url", paramString);
    return localIntent;
  }
  
  public static final Intent a(p paramp)
  {
    k.b(paramp, "txnModel");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    paramp = (Serializable)paramp;
    localIntent.putExtra("transaction_data", paramp);
    localIntent.putExtra("payment_error_type", "config_missing");
    return localIntent;
  }
  
  public static final Intent a(p paramp, IxigoPaymentRequest paramIxigoPaymentRequest)
  {
    k.b(paramp, "txnModel");
    k.b(paramIxigoPaymentRequest, "paymentRequest");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    paramp = (Serializable)paramp;
    localIntent.putExtra("transaction_data", paramp);
    paramIxigoPaymentRequest = (Parcelable)paramIxigoPaymentRequest;
    localIntent.putExtra("payment_request", paramIxigoPaymentRequest);
    return localIntent;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a()
  {
    finish();
  }
  
  public final void a(a.a parama, DeviceConfig paramDeviceConfig, WebAppConfig paramWebAppConfig, String paramString)
  {
    k.b(parama, "presenter");
    k.b(paramDeviceConfig, "config");
    k.b(paramWebAppConfig, "appConfig");
    k.b(paramString, "url");
    int i = R.id.webview;
    Object localObject1 = (WebView)a(i);
    Object localObject2 = paramWebAppConfig.getJsInterfaceName();
    ((WebView)localObject1).addJavascriptInterface(parama, (String)localObject2);
    parama = new android/webkit/WebViewClient;
    parama.<init>();
    ((WebView)localObject1).setWebViewClient(parama);
    parama = new android/webkit/WebChromeClient;
    parama.<init>();
    ((WebView)localObject1).setWebChromeClient(parama);
    int j = R.id.webview;
    parama = (WebView)a(j);
    k.a(parama, "webview");
    parama = parama.getSettings();
    boolean bool1 = paramWebAppConfig.getWebViewSettings().getWideView();
    parama.setUseWideViewPort(bool1);
    bool1 = paramWebAppConfig.getWebViewSettings().getOverview();
    parama.setLoadWithOverviewMode(bool1);
    bool1 = paramWebAppConfig.getWebViewSettings().getJsEnabled();
    parama.setJavaScriptEnabled(bool1);
    bool1 = paramWebAppConfig.getWebViewSettings().getSaveForm();
    parama.setSaveFormData(bool1);
    bool1 = paramWebAppConfig.getWebViewSettings().getUseZoomControls();
    parama.setSupportZoom(bool1);
    bool1 = paramWebAppConfig.getWebViewSettings().getUseZoomControls();
    parama.setBuiltInZoomControls(bool1);
    bool1 = paramWebAppConfig.getWebViewSettings().getUseZoomControls();
    parama.setDisplayZoomControls(bool1);
    bool1 = paramWebAppConfig.getWebViewSettings().getDomStorage();
    parama.setDomStorageEnabled(bool1);
    bool1 = paramWebAppConfig.getWebViewSettings().getDatabase();
    parama.setDatabaseEnabled(bool1);
    bool1 = paramWebAppConfig.getWebViewSettings().getGeoLocation();
    parama.setGeolocationEnabled(bool1);
    j = Build.VERSION.SDK_INT;
    bool1 = false;
    localObject1 = null;
    int k = 19;
    if (j >= k) {
      WebView.setWebContentsDebuggingEnabled(false);
    }
    j = Build.VERSION.SDK_INT;
    k = 21;
    if (j >= k)
    {
      parama = CookieManager.getInstance();
      k = R.id.webview;
      localObject2 = (WebView)a(k);
      localObject3 = paramWebAppConfig.getWebViewSettings();
      boolean bool2 = ((JsConfig)localObject3).getCookies();
      parama.setAcceptThirdPartyCookies((WebView)localObject2, bool2);
    }
    j = R.id.webview;
    parama = (WebView)a(j);
    k.b(paramDeviceConfig, "receiver$0");
    k.b(paramWebAppConfig, "config");
    localObject2 = new n[6];
    String str = paramWebAppConfig.getClientId();
    Object localObject3 = c.t.a("clientId", str);
    localObject2[0] = localObject3;
    str = paramWebAppConfig.getApiKey();
    localObject3 = c.t.a("apiKey", str);
    localObject2[1] = localObject3;
    paramWebAppConfig = paramWebAppConfig.getCurrencyCode();
    paramWebAppConfig = c.t.a("currencyCode", paramWebAppConfig);
    localObject2[2] = paramWebAppConfig;
    localObject3 = String.valueOf(a);
    localObject1 = c.t.a("appVersion", localObject3);
    localObject2[3] = localObject1;
    localObject3 = b;
    localObject1 = c.t.a("deviceId", localObject3);
    localObject2[4] = localObject1;
    paramDeviceConfig = c;
    paramDeviceConfig = c.t.a("uuid", paramDeviceConfig);
    localObject2[5] = paramDeviceConfig;
    paramDeviceConfig = ag.a((n[])localObject2);
    parama.loadUrl(paramString, paramDeviceConfig);
  }
  
  public final void a(IxigoPaymentRequest paramIxigoPaymentRequest, com.truecaller.truepay.app.ui.payments.models.a parama, String paramString)
  {
    k.b(paramIxigoPaymentRequest, "request");
    k.b(parama, "utility");
    k.b(paramString, "screenContext");
    paramIxigoPaymentRequest = PaymentsActivity.a((Context)this, parama, paramIxigoPaymentRequest, paramString);
    startActivityForResult(paramIxigoPaymentRequest, 100);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "title");
    int i = R.id.toolbar;
    Toolbar localToolbar = (Toolbar)a(i);
    if (localToolbar != null)
    {
      Object localObject = paramString;
      localObject = (CharSequence)paramString;
      localToolbar.setTitle((CharSequence)localObject);
      localObject = new com/truecaller/truepay/app/ui/webapps/WebAppActivity$b;
      ((WebAppActivity.b)localObject).<init>(this, paramString);
      localObject = (View.OnClickListener)localObject;
      localToolbar.setNavigationOnClickListener((View.OnClickListener)localObject);
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "extraKey");
    k.b(paramString2, "extraValue");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("com.truecaller.android.sdk.intent.action.v1.SHARE_PROFILE");
    localIntent.putExtra(paramString1, paramString2);
    startActivityForResult(localIntent, 110);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "message");
    Object localObject = this;
    localObject = (Context)this;
    paramString = (CharSequence)paramString;
    Toast.makeText((Context)localObject, paramString, 0).show();
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "redirectUrl");
    int i = R.id.webview;
    ((WebView)a(i)).loadUrl(paramString);
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    b.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.a(paramInt1, paramInt2, paramIntent);
  }
  
  public void onBackPressed()
  {
    int i = R.id.webview;
    WebView localWebView = (WebView)a(i);
    boolean bool = localWebView.canGoBack();
    if (bool)
    {
      int j = R.id.webview;
      ((WebView)a(j)).goBack();
      return;
    }
    super.onBackPressed();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = R.layout.activity_web_app;
    setContentView(i);
    Object localObject1 = com.truecaller.truepay.app.ui.webapps.b.a.a();
    Object localObject2 = Truepay.getApplicationComponent();
    ((com.truecaller.truepay.app.ui.webapps.b.a.a)localObject1).a((com.truecaller.truepay.app.a.a.a)localObject2).a().a(this);
    localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    ((b.a)localObject1).a(this);
    localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    localObject2 = getIntent();
    if (localObject2 != null) {
      localObject2 = ((Intent)localObject2).getExtras();
    } else {
      localObject2 = null;
    }
    ((b.a)localObject1).a(paramBundle, (Bundle)localObject2);
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    int i = R.id.webview;
    WebView localWebView = (WebView)a(i);
    if (localWebView != null)
    {
      boolean bool = isFinishing();
      if (bool)
      {
        int j = R.id.webview;
        localWebView = (WebView)a(j);
        Object localObject = localWebView;
        localObject = (View)localWebView;
        com.truecaller.utils.extensions.t.b((View)localObject);
        localWebView.destroy();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.webapps.WebAppActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */