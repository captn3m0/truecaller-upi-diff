package com.truecaller.truepay.app.ui.scan.views.a;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnKeyListener;
import android.os.Bundle;
import android.support.v4.app.e;
import android.support.v4.app.f;
import android.text.InputFilter;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.app.utils.k;
import com.truecaller.utils.extensions.t;

public class a
  extends e
{
  a.a a;
  private View b;
  private EditText c;
  private TextView d;
  private TextView e;
  
  public void dismiss()
  {
    super.dismiss();
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = getActivity().getLayoutInflater();
    int i = R.layout.fragment_edit_amount;
    paramBundle = paramBundle.inflate(i, null);
    b = paramBundle;
    paramBundle = b;
    i = R.id.et_edit_amount_frag_edit_amount;
    paramBundle = (EditText)paramBundle.findViewById(i);
    c = paramBundle;
    paramBundle = c;
    i = 1;
    Object localObject1 = new InputFilter[i];
    k localk = new com/truecaller/truepay/app/utils/k;
    localk.<init>();
    localObject1[0] = localk;
    paramBundle.setFilters((InputFilter[])localObject1);
    paramBundle = b;
    int j = R.id.doneText;
    paramBundle = (TextView)paramBundle.findViewById(j);
    d = paramBundle;
    paramBundle = b;
    j = R.id.cancelText;
    paramBundle = (TextView)paramBundle.findViewById(j);
    e = paramBundle;
    t.a(c, i, 2);
    paramBundle = d;
    Object localObject2 = new com/truecaller/truepay/app/ui/scan/views/a/-$$Lambda$a$-V3geY5F_qVdUl8S5I2B0sRhShI;
    ((-..Lambda.a.-V3geY5F_qVdUl8S5I2B0sRhShI)localObject2).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject2);
    paramBundle = e;
    localObject2 = new com/truecaller/truepay/app/ui/scan/views/a/-$$Lambda$a$cZkAxiaWFVAjQdLdsdXrjC0BnFc;
    ((-..Lambda.a.cZkAxiaWFVAjQdLdsdXrjC0BnFc)localObject2).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject2);
    paramBundle = new android/app/AlertDialog$Builder;
    localObject2 = new android/view/ContextThemeWrapper;
    localObject1 = getActivity();
    int k = R.style.popup_theme;
    ((ContextThemeWrapper)localObject2).<init>((Context)localObject1, k);
    paramBundle.<init>((Context)localObject2);
    paramBundle = paramBundle.create();
    View localView = b;
    paramBundle.setView(localView, 0, 0, 0, 0);
    localObject2 = new com/truecaller/truepay/app/ui/scan/views/a/-$$Lambda$a$HYxGYrbb8WK_ysBOH5JCgY-Dw54;
    ((-..Lambda.a.HYxGYrbb8WK_ysBOH5JCgY-Dw54)localObject2).<init>(this);
    paramBundle.setOnKeyListener((DialogInterface.OnKeyListener)localObject2);
    return paramBundle;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.scan.views.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */