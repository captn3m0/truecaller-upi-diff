package com.truecaller.truepay.app.ui.scan.views.a;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.i.q;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.scan.BharathQRParser;
import com.truecaller.truepay.app.ui.scan.b.1;
import com.truecaller.truepay.data.e.e;

public final class c
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements com.truecaller.truepay.app.ui.scan.a, com.truecaller.truepay.app.ui.scan.views.b.a, me.dm7.barcodescanner.zxing.a.a
{
  com.truecaller.truepay.app.ui.scan.views.b.b a;
  public com.truecaller.truepay.app.ui.scan.b.a b;
  Toolbar c;
  FrameLayout d;
  RelativeLayout e;
  TextView f;
  LinearLayout g;
  public e h;
  com.truecaller.truepay.app.ui.scan.d i;
  boolean j;
  boolean k;
  boolean l;
  
  public static c a(String paramString)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("flow_from", paramString);
    paramString = new com/truecaller/truepay/app/ui/scan/views/a/c;
    paramString.<init>();
    paramString.setArguments(localBundle);
    return paramString;
  }
  
  private boolean b()
  {
    Object localObject = getContext();
    String str1 = "android.permission.CAMERA";
    int m = android.support.v4.app.a.a((Context)localObject, str1);
    boolean bool = true;
    if (m != 0)
    {
      localObject = getContext();
      String str2 = "android.permission.CAMERA";
      m = android.support.v4.content.b.a((Context)localObject, str2);
      if (m != 0)
      {
        localObject = new String[] { "android.permission.CAMERA" };
        requestPermissions((String[])localObject, 112);
        return false;
      }
      return bool;
    }
    return bool;
  }
  
  private void d()
  {
    try
    {
      com.truecaller.truepay.app.ui.scan.d locald = i;
      if (locald != null)
      {
        locald = i;
        locald.a();
      }
      return;
    }
    catch (RuntimeException localRuntimeException) {}
  }
  
  private void e()
  {
    g.setVisibility(8);
    Object localObject1 = d;
    float f1 = 0.0F;
    ((FrameLayout)localObject1).setVisibility(0);
    localObject1 = new com/truecaller/truepay/app/ui/scan/views/a/c$1;
    Object localObject2 = getActivity();
    ((c.1)localObject1).<init>(this, (Context)localObject2);
    i = ((com.truecaller.truepay.app.ui.scan.d)localObject1);
    localObject1 = i;
    int m = R.id.scannerViewId;
    ((com.truecaller.truepay.app.ui.scan.d)localObject1).setId(m);
    localObject1 = d;
    m = R.id.scannerViewId;
    localObject1 = ((FrameLayout)localObject1).findViewById(m);
    if (localObject1 != null)
    {
      localObject1 = d;
      localObject2 = i;
      ((FrameLayout)localObject1).removeView((View)localObject2);
    }
    localObject1 = i;
    m = 1056964608;
    f1 = 0.5F;
    ((com.truecaller.truepay.app.ui.scan.d)localObject1).setAspectTolerance(f1);
    localObject1 = d;
    localObject2 = i;
    ((FrameLayout)localObject1).addView((View)localObject2);
    try
    {
      localObject1 = i;
      m = me.dm7.barcodescanner.core.d.a();
      ((me.dm7.barcodescanner.core.a)localObject1).a(m);
      localObject1 = i;
      ((com.truecaller.truepay.app.ui.scan.d)localObject1).setResultHandler(this);
      return;
    }
    catch (Exception localException) {}
  }
  
  public final int a()
  {
    return R.layout.fragment_scan_pay;
  }
  
  public final void a(q paramq)
  {
    paramq = a;
    int m = 1;
    Object localObject1 = new String[m];
    int i1 = 0;
    localObject1[0] = paramq;
    boolean bool2 = TextUtils.isEmpty(paramq);
    if (!bool2)
    {
      localObject1 = i;
      i1 = 2;
      ((com.truecaller.truepay.app.ui.scan.d)localObject1).performHapticFeedback(m, i1);
      j = m;
      Object localObject2 = "000201";
      boolean bool1 = paramq.startsWith((String)localObject2);
      if (bool1)
      {
        i.a();
        localObject2 = new com/truecaller/truepay/app/ui/scan/BharathQRParser;
        ((BharathQRParser)localObject2).<init>();
        paramq = ((BharathQRParser)localObject2).a(paramq);
        a.a(paramq);
        return;
      }
      localObject2 = "upi://";
      bool1 = paramq.startsWith((String)localObject2);
      if (bool1)
      {
        i.a();
        a.a(paramq);
        return;
      }
      paramq = getResources();
      int n = R.string.invalid_qr;
      paramq = paramq.getString(n);
      n = 0;
      localObject2 = null;
      a(paramq, null);
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      k = true;
      d();
      return;
    }
    k = false;
    FrameLayout localFrameLayout = d;
    com.truecaller.truepay.app.ui.scan.d locald = i;
    localFrameLayout.removeView(locald);
    paramBoolean = b();
    if (paramBoolean) {
      e();
    }
  }
  
  public final void c()
  {
    Object localObject1 = c;
    Object localObject2 = new com/truecaller/truepay/app/ui/scan/views/a/-$$Lambda$c$pNKhoyuzrLE0BOPf2p3iXcAGJEk;
    ((-..Lambda.c.pNKhoyuzrLE0BOPf2p3iXcAGJEk)localObject2).<init>(this);
    ((Toolbar)localObject1).setNavigationOnClickListener((View.OnClickListener)localObject2);
    localObject1 = getActivity();
    int m = 16908290;
    localObject1 = ((Activity)localObject1).findViewById(m);
    localObject2 = ((View)localObject1).getViewTreeObserver();
    b.1 local1 = new com/truecaller/truepay/app/ui/scan/b$1;
    local1.<init>((View)localObject1, this);
    ((ViewTreeObserver)localObject2).addOnGlobalLayoutListener(local1);
    boolean bool = b();
    if (bool) {
      e();
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getActivity();
    boolean bool = paramContext instanceof com.truecaller.truepay.app.ui.scan.views.b.b;
    if (bool)
    {
      paramContext = (com.truecaller.truepay.app.ui.scan.views.b.b)getActivity();
      a = paramContext;
      return;
    }
    paramContext = new java/lang/IllegalStateException;
    paramContext.<init>("Activity should implement ScanAndPayView");
    throw paramContext;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.scan.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    b.b();
  }
  
  public final void onDetach()
  {
    super.onDetach();
    a = null;
  }
  
  public final void onPause()
  {
    d();
    super.onPause();
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    int m = 112;
    if (paramInt == m)
    {
      paramInt = paramArrayOfInt.length;
      m = 1;
      if (paramInt == m)
      {
        paramInt = paramArrayOfInt[0];
        if (paramInt == 0)
        {
          l = false;
          return;
        }
      }
      l = m;
      g.setVisibility(0);
      FrameLayout localFrameLayout = d;
      m = 8;
      localFrameLayout.setVisibility(m);
    }
  }
  
  public final void onResume()
  {
    super.onResume();
    boolean bool = l;
    if (!bool)
    {
      Object localObject = b;
      if (localObject != null)
      {
        localObject = (com.truecaller.truepay.app.ui.scan.views.b.a)d;
        ((com.truecaller.truepay.app.ui.scan.views.b.a)localObject).c();
      }
    }
  }
  
  public final void onStop()
  {
    d();
    super.onStop();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int m = R.id.toolbar;
    Object localObject1 = (Toolbar)paramView.findViewById(m);
    c = ((Toolbar)localObject1);
    m = R.id.camera_preview;
    localObject1 = (FrameLayout)paramView.findViewById(m);
    d = ((FrameLayout)localObject1);
    m = R.id.qr_error_layout;
    localObject1 = (RelativeLayout)paramView.findViewById(m);
    e = ((RelativeLayout)localObject1);
    m = R.id.qr_error_textView;
    localObject1 = (TextView)paramView.findViewById(m);
    f = ((TextView)localObject1);
    m = R.id.ll_empty_layout_frag_scan_n_pay;
    localObject1 = (LinearLayout)paramView.findViewById(m);
    g = ((LinearLayout)localObject1);
    m = R.id.generate_qr_layout;
    localObject1 = paramView.findViewById(m);
    Object localObject2 = new com/truecaller/truepay/app/ui/scan/views/a/-$$Lambda$c$EePLtOyQg4qbjtkUqX9wxlLLr-U;
    ((-..Lambda.c.EePLtOyQg4qbjtkUqX9wxlLLr-U)localObject2).<init>(this);
    ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    m = R.id.btn_enable_camera_frag_scan_n_pay;
    localObject1 = paramView.findViewById(m);
    localObject2 = new com/truecaller/truepay/app/ui/scan/views/a/-$$Lambda$c$sQoXz2mMhOro78nceQp_Z_hq2f0;
    ((-..Lambda.c.sQoXz2mMhOro78nceQp_Z_hq2f0)localObject2).<init>(this);
    ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = (AppCompatActivity)getActivity();
    localObject2 = c;
    ((AppCompatActivity)localObject1).setSupportActionBar((Toolbar)localObject2);
    ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Scan & Pay");
    localObject1 = ((AppCompatActivity)getActivity()).getSupportActionBar();
    boolean bool = true;
    ((ActionBar)localObject1).setDisplayShowTitleEnabled(bool);
    ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(bool);
    super.onViewCreated(paramView, paramBundle);
    b.a(this);
    String str = getArguments().getString("flow_from");
    paramView = h;
    paramBundle = com.truecaller.truepay.data.b.a.a();
    paramView.a(paramBundle);
    localObject1 = Truepay.getInstance().getAnalyticLoggerHelper();
    Boolean localBoolean = Boolean.valueOf(Truepay.getInstance().isRegistrationComplete());
    ((com.truecaller.truepay.data.b.a)localObject1).a("app_payment_transaction_intent", str, "scan_pay", null, localBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.scan.views.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */