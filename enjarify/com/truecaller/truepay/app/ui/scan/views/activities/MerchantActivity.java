package com.truecaller.truepay.app.ui.scan.views.activities;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.d;
import android.widget.Toast;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.scan.a.a.a;
import com.truecaller.truepay.app.ui.transaction.b.n;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import java.io.Serializable;
import java.net.URLDecoder;
import java.util.HashMap;

public class MerchantActivity
  extends com.truecaller.truepay.app.ui.base.views.a.b
{
  private final BroadcastReceiver a;
  
  public MerchantActivity()
  {
    MerchantActivity.1 local1 = new com/truecaller/truepay/app/ui/scan/views/activities/MerchantActivity$1;
    local1.<init>(this);
    a = local1;
  }
  
  private static String a(String paramString)
  {
    try
    {
      float f = Float.parseFloat(paramString);
      return String.valueOf(f);
    }
    catch (Exception localException) {}
    return null;
  }
  
  private static HashMap a(String[] paramArrayOfString)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    int i = paramArrayOfString.length;
    int j = 0;
    while (j < i)
    {
      String str1 = paramArrayOfString[j];
      String str2 = c(str1);
      str1 = b(str1);
      localHashMap.put(str2, str1);
      j += 1;
    }
    return localHashMap;
  }
  
  private static String b(String paramString)
  {
    String str = "\\=";
    try
    {
      paramString = paramString.split(str);
      int i = 1;
      return paramString[i];
    }
    catch (Exception localException) {}
    return "";
  }
  
  private static String c(String paramString)
  {
    String str = "\\=";
    try
    {
      paramString = paramString.split(str);
      str = null;
      return paramString[0];
    }
    catch (Exception localException) {}
    return "";
  }
  
  public int getLayoutId()
  {
    return R.layout.activity_scan_pay;
  }
  
  public void initDagger(com.truecaller.truepay.app.a.a.a parama)
  {
    if (parama != null)
    {
      parama = com.truecaller.truepay.app.ui.scan.a.a.a();
      com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
      parama = parama.a(locala).a();
      parama.a(this);
    }
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    paramInt2 = 1011;
    if (paramInt1 == paramInt2) {
      finish();
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = Truepay.getApplicationComponent();
    BroadcastReceiver localBroadcastReceiver = null;
    if (paramBundle == null)
    {
      int i = R.string.feature_not_available;
      Toast.makeText(this, i, 0).show();
      finish();
      return;
    }
    paramBundle = new com/truecaller/truepay/app/ui/transaction/b/n;
    paramBundle.<init>();
    Object localObject1 = new com/truecaller/truepay/app/ui/transaction/b/p;
    ((p)localObject1).<init>();
    Intent localIntent = getIntent();
    boolean bool1 = true;
    if (localIntent != null)
    {
      localIntent = getIntent();
      localObject2 = "isFromScan";
      bool2 = localIntent.getBooleanExtra((String)localObject2, false);
      if (bool2)
      {
        bool2 = true;
        break label102;
      }
    }
    boolean bool2 = false;
    localIntent = null;
    for (;;)
    {
      try
      {
        label102:
        localObject2 = getIntent();
        localObject2 = ((Intent)localObject2).getData();
        localObject2 = ((Uri)localObject2).getEncodedQuery();
        localObject3 = "\\&";
        localObject2 = ((String)localObject2).split((String)localObject3);
        localObject2 = a((String[])localObject2);
        localObject3 = "pa";
        localObject3 = ((HashMap)localObject2).get(localObject3);
        localObject3 = (String)localObject3;
        e = ((String)localObject3);
        localObject3 = "pn";
        localObject3 = ((HashMap)localObject2).get(localObject3);
        if (localObject3 != null)
        {
          localObject3 = "pn";
          localObject3 = ((HashMap)localObject2).get(localObject3);
          localObject3 = (String)localObject3;
          localObject4 = "%20|%";
          str1 = " ";
          localObject3 = ((String)localObject3).replaceAll((String)localObject4, str1);
        }
        else
        {
          localObject3 = "";
        }
        h = ((String)localObject3);
        localObject3 = "an";
        localObject3 = ((HashMap)localObject2).get(localObject3);
        localObject3 = (String)localObject3;
        a = ((String)localObject3);
        localObject3 = "ifsc";
        localObject3 = ((HashMap)localObject2).get(localObject3);
        localObject3 = (String)localObject3;
        b = ((String)localObject3);
        localObject3 = "pmo";
        localObject3 = ((HashMap)localObject2).get(localObject3);
        if (localObject3 != null)
        {
          localObject3 = new java/lang/StringBuilder;
          localObject4 = "+91";
          ((StringBuilder)localObject3).<init>((String)localObject4);
          localObject4 = "pmo";
          localObject4 = ((HashMap)localObject2).get(localObject4);
          localObject4 = (String)localObject4;
          ((StringBuilder)localObject3).append((String)localObject4);
          localObject3 = ((StringBuilder)localObject3).toString();
        }
        else
        {
          localObject3 = "";
        }
        f = ((String)localObject3);
        h = paramBundle;
        localObject3 = "Stores";
        c = ((String)localObject3);
        localObject3 = "mc";
        localObject3 = ((HashMap)localObject2).get(localObject3);
        localObject3 = (String)localObject3;
        r = ((String)localObject3);
        localObject3 = "tr";
        localObject3 = ((HashMap)localObject2).get(localObject3);
        bool3 = false;
        localObject4 = null;
        if (localObject3 != null)
        {
          localObject3 = "tr";
          localObject3 = ((HashMap)localObject2).get(localObject3);
          localObject3 = (String)localObject3;
          str1 = "[^A-Za-z0-9]";
          str2 = "";
          localObject3 = ((String)localObject3).replaceAll(str1, str2);
        }
        else
        {
          localObject3 = null;
        }
        n = ((String)localObject3);
        localObject3 = "tid";
        localObject3 = ((HashMap)localObject2).get(localObject3);
        if (localObject3 != null)
        {
          localObject3 = "tid";
          localObject3 = ((HashMap)localObject2).get(localObject3);
          localObject3 = (String)localObject3;
          localObject4 = "[^A-Za-z0-9]";
          str1 = "";
          localObject4 = ((String)localObject3).replaceAll((String)localObject4, str1);
        }
        o = ((String)localObject4);
        localObject3 = "tn";
        localObject3 = ((HashMap)localObject2).get(localObject3);
        if (localObject3 != null)
        {
          localObject3 = "tn";
          localObject3 = ((HashMap)localObject2).get(localObject3);
          localObject3 = (String)localObject3;
          localObject4 = "%20|%";
          str1 = " ";
          localObject3 = ((String)localObject3).replaceAll((String)localObject4, str1);
        }
        else
        {
          localObject3 = "";
        }
        i = ((String)localObject3);
        localObject3 = "url";
        localObject3 = ((HashMap)localObject2).get(localObject3);
        localObject3 = (String)localObject3;
        q = ((String)localObject3);
        localObject3 = "am";
        localObject3 = ((HashMap)localObject2).get(localObject3);
        localObject3 = (String)localObject3;
        localObject3 = a((String)localObject3);
        e = ((String)localObject3);
        localObject3 = "am";
        localObject3 = ((HashMap)localObject2).get(localObject3);
        localObject3 = (String)localObject3;
        localObject3 = a((String)localObject3);
        f = ((String)localObject3);
        localObject3 = "mam";
        localObject2 = ((HashMap)localObject2).get(localObject3);
        localObject2 = (String)localObject2;
        localObject2 = a((String)localObject2);
        p = ((String)localObject2);
      }
      catch (Exception localException1)
      {
        Object localObject3;
        Object localObject4;
        String str1;
        boolean bool3;
        String str2;
        Boolean localBoolean;
        continue;
      }
      try
      {
        localObject2 = getIntent();
        localObject2 = ((Intent)localObject2).getData();
        localObject2 = ((Uri)localObject2).getEncodedQuery();
        localObject3 = "\\&";
        localObject2 = ((String)localObject2).split((String)localObject3);
        localObject2 = a((String[])localObject2);
        localObject3 = "pa";
        localObject3 = ((HashMap)localObject2).get(localObject3);
        localObject3 = (String)localObject3;
        localObject4 = "UTF-8";
        localObject3 = URLDecoder.decode((String)localObject3, (String)localObject4);
        localObject4 = "pn";
        localObject4 = ((HashMap)localObject2).get(localObject4);
        localObject4 = (String)localObject4;
        str1 = "UTF-8";
        localObject4 = URLDecoder.decode((String)localObject4, str1);
        e = ((String)localObject3);
        if (localObject4 != null)
        {
          str1 = "%20|%";
          str2 = " ";
          localObject4 = ((String)localObject4).replaceAll(str1, str2);
        }
        else
        {
          localObject4 = "";
        }
        h = ((String)localObject4);
        if (localObject3 != null)
        {
          localObject4 = ".npci";
          bool3 = ((String)localObject3).endsWith((String)localObject4);
          if (bool3)
          {
            localObject4 = "@";
            localObject3 = ((String)localObject3).split((String)localObject4);
            localObject4 = localObject3[0];
            a = ((String)localObject4);
            localObject3 = localObject3[bool1];
            localObject4 = ".ifsc";
            localObject3 = ((String)localObject3).split((String)localObject4);
            localObject3 = localObject3[0];
            localObject3 = ((String)localObject3).toUpperCase();
            b = ((String)localObject3);
          }
        }
        paramBundle = "tn";
        paramBundle = ((HashMap)localObject2).get(paramBundle);
        paramBundle = (String)paramBundle;
        localObject2 = "UTF-8";
        paramBundle = URLDecoder.decode(paramBundle, (String)localObject2);
        i = paramBundle;
      }
      catch (Exception localException2) {}
    }
    paramBundle = new android/content/Intent;
    paramBundle.<init>(this, TransactionActivity.class);
    paramBundle.putExtra("payable_object", (Serializable)localObject1);
    Object localObject2 = "type";
    localObject3 = "pay";
    paramBundle.putExtra((String)localObject2, (String)localObject3);
    if (bool2)
    {
      d = "qr_pay";
      m = false;
      startActivityForResult(paramBundle, 1011);
      return;
    }
    str2 = getCallingActivity().getPackageName();
    localObject2 = Truepay.getInstance().getAnalyticLoggerHelper();
    localBoolean = Boolean.valueOf(Truepay.getInstance().isRegistrationComplete());
    ((com.truecaller.truepay.data.b.a)localObject2).a("app_payment_transaction_intent", "deeplink", "send_money", str2, localBoolean);
    d = "pay_other";
    m = bool1;
    paramBundle.putExtra("is_merchant_txn", bool1);
    paramBundle.setFlags(402653184);
    startActivity(paramBundle);
    paramBundle = d.a(this);
    localBroadcastReceiver = a;
    localObject1 = new android/content/IntentFilter;
    ((IntentFilter)localObject1).<init>("MERCHANT_INTENT");
    paramBundle.a(localBroadcastReceiver, (IntentFilter)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.scan.views.activities.MerchantActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */