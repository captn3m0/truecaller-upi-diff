package com.truecaller.truepay.app.ui.scan.views.a;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.google.i.v;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.scan.a.a.a;

public class b
  extends com.truecaller.truepay.app.ui.base.views.fragments.b
  implements com.truecaller.truepay.app.ui.scan.views.b.a
{
  Toolbar a;
  ImageView b;
  TextView c;
  TextView d;
  TextView e;
  com.truecaller.truepay.app.ui.scan.views.b.b f;
  public com.truecaller.truepay.app.utils.a g;
  
  private static Bitmap a(String paramString)
  {
    Object localObject1 = new com/google/i/g/b;
    ((com.google.i.g.b)localObject1).<init>();
    try
    {
      Object localObject2 = com.google.i.a.l;
      paramString = ((com.google.i.g.b)localObject1).a(paramString, (com.google.i.a)localObject2);
      int i = a;
      int j = b;
      int k = i * j;
      localObject2 = new int[k];
      k = 0;
      localObject1 = null;
      int m = 0;
      while (m < j)
      {
        n = m * i;
        i1 = 0;
        while (i1 < i)
        {
          i2 = n + i1;
          boolean bool = paramString.a(i1, m);
          int i3;
          if (bool) {
            i3 = -16777216;
          } else {
            i3 = -1;
          }
          localObject2[i2] = i3;
          i1 += 1;
        }
        m += 1;
      }
      paramString = Bitmap.Config.ARGB_8888;
      paramString = Bitmap.createBitmap(i, j, paramString);
      m = 0;
      int i1 = 0;
      int i2 = 0;
      localObject1 = paramString;
      int n = i;
      paramString.setPixels((int[])localObject2, 0, i, 0, 0, i, j);
      return paramString;
    }
    catch (v localv)
    {
      localv;
    }
    return null;
  }
  
  private static String a(String paramString1, String paramString2, String paramString3)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("upi://pay?pa=");
    localStringBuilder.append(paramString2);
    localStringBuilder.append("&pn=");
    localStringBuilder.append(paramString1);
    localStringBuilder.append("&mc=0000&tn=");
    localStringBuilder.append(paramString3);
    localStringBuilder.append("&cu=INR&appid=com.truecaller&appname=Truecaller");
    return localStringBuilder.toString();
  }
  
  private static String a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("upi://pay?pa=");
    localStringBuilder.append(paramString2);
    localStringBuilder.append("&pn=");
    localStringBuilder.append(paramString1);
    localStringBuilder.append("&mc=0000&tn=");
    localStringBuilder.append(paramString3);
    localStringBuilder.append("&am=");
    localStringBuilder.append(paramString4);
    localStringBuilder.append("&cu=INR&appid=com.truecaller&appname=Truecaller");
    return localStringBuilder.toString();
  }
  
  private void a(Bitmap paramBitmap)
  {
    Object localObject = "share";
    boolean bool = b((String)localObject);
    if (bool) {
      return;
    }
    paramBitmap = c(paramBitmap);
    localObject = new android/content/Intent;
    ((Intent)localObject).<init>("android.intent.action.SEND");
    ((Intent)localObject).setType("image/jpeg");
    ((Intent)localObject).putExtra("android.intent.extra.STREAM", paramBitmap);
    ((Intent)localObject).addFlags(1);
    paramBitmap = Intent.createChooser((Intent)localObject, "Share Image");
    startActivity(paramBitmap);
  }
  
  public static b b()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    b localb = new com/truecaller/truepay/app/ui/scan/views/a/b;
    localb.<init>();
    localb.setArguments(localBundle);
    return localb;
  }
  
  private void b(Bitmap paramBitmap)
  {
    String str = "save";
    boolean bool = b(str);
    if (bool) {
      return;
    }
    paramBitmap = c(paramBitmap);
    int i = R.string.qr_saved_to_message;
    Object[] arrayOfObject = new Object[1];
    paramBitmap = paramBitmap.getPath();
    arrayOfObject[0] = paramBitmap;
    paramBitmap = getString(i, arrayOfObject);
    a(paramBitmap, null);
  }
  
  private boolean b(String paramString)
  {
    Object localObject = getContext();
    String str = "android.permission.WRITE_EXTERNAL_STORAGE";
    int i = android.support.v4.app.a.a((Context)localObject, str);
    if (i != 0)
    {
      localObject = "share";
      boolean bool = ((String)localObject).equals(paramString);
      if (bool)
      {
        paramString = new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" };
        i = 1006;
        requestPermissions(paramString, i);
      }
      else
      {
        paramString = new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" };
        i = 1005;
        requestPermissions(paramString, i);
      }
      return true;
    }
    return false;
  }
  
  /* Error */
  private Uri c(Bitmap paramBitmap)
  {
    // Byte code:
    //   0: new 222	android/content/ContentValues
    //   3: astore_2
    //   4: aload_2
    //   5: invokespecial 223	android/content/ContentValues:<init>	()V
    //   8: aload_2
    //   9: ldc -31
    //   11: ldc -31
    //   13: invokevirtual 229	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   16: aload_2
    //   17: ldc -25
    //   19: ldc 105
    //   21: invokevirtual 229	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   24: aload_0
    //   25: invokevirtual 141	com/truecaller/truepay/app/ui/scan/views/a/b:getActivity	()Landroid/support/v4/app/f;
    //   28: invokevirtual 235	android/support/v4/app/f:getContentResolver	()Landroid/content/ContentResolver;
    //   31: astore_3
    //   32: getstatic 241	android/provider/MediaStore$Images$Media:EXTERNAL_CONTENT_URI	Landroid/net/Uri;
    //   35: astore 4
    //   37: aload_3
    //   38: aload 4
    //   40: aload_2
    //   41: invokevirtual 247	android/content/ContentResolver:insert	(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    //   44: astore_2
    //   45: aconst_null
    //   46: astore_3
    //   47: aload_0
    //   48: invokevirtual 141	com/truecaller/truepay/app/ui/scan/views/a/b:getActivity	()Landroid/support/v4/app/f;
    //   51: astore 4
    //   53: aload 4
    //   55: invokevirtual 235	android/support/v4/app/f:getContentResolver	()Landroid/content/ContentResolver;
    //   58: astore 4
    //   60: aload 4
    //   62: aload_2
    //   63: invokevirtual 251	android/content/ContentResolver:openOutputStream	(Landroid/net/Uri;)Ljava/io/OutputStream;
    //   66: astore_3
    //   67: getstatic 257	android/graphics/Bitmap$CompressFormat:JPEG	Landroid/graphics/Bitmap$CompressFormat;
    //   70: astore 4
    //   72: bipush 100
    //   74: istore 5
    //   76: aload_1
    //   77: aload 4
    //   79: iload 5
    //   81: aload_3
    //   82: invokevirtual 262	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   85: pop
    //   86: goto +11 -> 97
    //   89: astore_1
    //   90: aload_3
    //   91: invokestatic 267	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   94: aload_1
    //   95: athrow
    //   96: pop
    //   97: aload_3
    //   98: invokestatic 267	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   101: aload_2
    //   102: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	103	0	this	b
    //   0	103	1	paramBitmap	Bitmap
    //   3	99	2	localObject1	Object
    //   31	67	3	localObject2	Object
    //   35	43	4	localObject3	Object
    //   74	6	5	i	int
    //   96	1	6	localFileNotFoundException	java.io.FileNotFoundException
    // Exception table:
    //   from	to	target	type
    //   47	51	89	finally
    //   53	58	89	finally
    //   62	66	89	finally
    //   67	70	89	finally
    //   81	86	89	finally
    //   47	51	96	java/io/FileNotFoundException
    //   53	58	96	java/io/FileNotFoundException
    //   62	66	96	java/io/FileNotFoundException
    //   67	70	96	java/io/FileNotFoundException
    //   81	86	96	java/io/FileNotFoundException
  }
  
  private Bitmap d()
  {
    RelativeLayout localRelativeLayout = new android/widget/RelativeLayout;
    Object localObject1 = getActivity();
    localRelativeLayout.<init>((Context)localObject1);
    localObject1 = (LayoutInflater)getActivity().getSystemService("layout_inflater");
    int i = R.layout.fragment_share_qr;
    int j = 1;
    localObject1 = ((LayoutInflater)localObject1).inflate(i, localRelativeLayout, j);
    i = R.id.iv_fragment_qr_share;
    Object localObject2 = (ImageView)((View)localObject1).findViewById(i);
    int m = R.id.tv_user_name_fragment_qr_share;
    TextView localTextView = (TextView)((View)localObject1).findViewById(m);
    int n = R.id.tv_upi_id_fragment_qr_share;
    localObject1 = (TextView)((View)localObject1).findViewById(n);
    Object localObject3 = g.c();
    if (localObject3 != null)
    {
      String str1 = b;
      localObject3 = h;
      String str2 = "Truecaller Pay";
      CharSequence localCharSequence = e.getText();
      boolean bool = TextUtils.isEmpty(localCharSequence);
      if (bool)
      {
        localObject4 = a(str1, (String)localObject3, str2);
      }
      else
      {
        int i1 = localCharSequence.length();
        localObject4 = String.valueOf(localCharSequence.subSequence(j, i1));
        localObject4 = a(str1, (String)localObject3, str2, (String)localObject4);
      }
      localObject4 = a((String)localObject4);
      ((ImageView)localObject2).setImageBitmap((Bitmap)localObject4);
      localTextView.setText(str1);
      ((TextView)localObject1).setText((CharSequence)localObject3);
    }
    localObject1 = new android/widget/RelativeLayout$LayoutParams;
    ((RelativeLayout.LayoutParams)localObject1).<init>(-1, -2);
    localRelativeLayout.setLayoutParams((ViewGroup.LayoutParams)localObject1);
    int i2 = View.MeasureSpec.makeMeasureSpec(getView().getWidth(), -1 << -1);
    int k = View.MeasureSpec.makeMeasureSpec(0, 0);
    localRelativeLayout.measure(i2, k);
    i2 = localRelativeLayout.getMeasuredWidth();
    k = localRelativeLayout.getMeasuredHeight();
    localRelativeLayout.layout(0, 0, i2, k);
    i2 = localRelativeLayout.getMeasuredWidth();
    i = localRelativeLayout.getMeasuredHeight();
    Object localObject4 = Bitmap.Config.ARGB_8888;
    localObject1 = Bitmap.createBitmap(i2, i, (Bitmap.Config)localObject4);
    localObject2 = new android/graphics/Canvas;
    ((Canvas)localObject2).<init>((Bitmap)localObject1);
    localRelativeLayout.draw((Canvas)localObject2);
    return (Bitmap)localObject1;
  }
  
  public final int a()
  {
    return R.layout.fragment_generate_qr;
  }
  
  public final void c()
  {
    Object localObject1 = a;
    Object localObject2 = new com/truecaller/truepay/app/ui/scan/views/a/-$$Lambda$b$0lzRsmcAhjIdv58IaxKHPmy7XWg;
    ((-..Lambda.b.0lzRsmcAhjIdv58IaxKHPmy7XWg)localObject2).<init>(this);
    ((Toolbar)localObject1).setNavigationOnClickListener((View.OnClickListener)localObject2);
    localObject1 = g.c();
    if (localObject1 != null)
    {
      localObject2 = b;
      localObject1 = h;
      String str = a((String)localObject2, (String)localObject1, "Truecaller Pay");
      TextView localTextView = c;
      localTextView.setText((CharSequence)localObject2);
      d.setText((CharSequence)localObject1);
      localObject1 = b;
      localObject2 = a(str);
      ((ImageView)localObject1).setImageBitmap((Bitmap)localObject2);
    }
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getActivity();
    boolean bool = paramContext instanceof com.truecaller.truepay.app.ui.scan.views.b.b;
    if (bool)
    {
      paramContext = (com.truecaller.truepay.app.ui.scan.views.b.b)getActivity();
      f = paramContext;
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.scan.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public void onDetach()
  {
    super.onDetach();
    f = null;
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    int i = paramArrayOfInt.length;
    int j = 1;
    if (i == j)
    {
      paramArrayOfString = null;
      i = paramArrayOfInt[0];
      if (i == 0)
      {
        i = 1006;
        if (paramInt == i)
        {
          localObject = d();
          a((Bitmap)localObject);
          return;
        }
        localObject = d();
        b((Bitmap)localObject);
        return;
      }
    }
    paramInt = R.string.external_directory_permission_denied;
    Object localObject = getString(paramInt);
    a((String)localObject, null);
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.toolbar;
    paramBundle = (Toolbar)paramView.findViewById(i);
    a = paramBundle;
    i = R.id.iv_fragment_qr_generate;
    paramBundle = (ImageView)paramView.findViewById(i);
    b = paramBundle;
    i = R.id.tv_user_name_fragment_qr_generate;
    paramBundle = (TextView)paramView.findViewById(i);
    c = paramBundle;
    i = R.id.tv_upi_id_fragment_qr_generate;
    paramBundle = (TextView)paramView.findViewById(i);
    d = paramBundle;
    i = R.id.tv_set_amount_fragment_qr_generate;
    paramBundle = (TextView)paramView.findViewById(i);
    e = paramBundle;
    i = R.id.action_right_button_share;
    paramBundle = paramView.findViewById(i);
    Object localObject = new com/truecaller/truepay/app/ui/scan/views/a/-$$Lambda$b$Nesyafwy2eLt4wPI-bSb8kZs0ro;
    ((-..Lambda.b.Nesyafwy2eLt4wPI-bSb8kZs0ro)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    i = R.id.action_left_button_download;
    paramBundle = paramView.findViewById(i);
    localObject = new com/truecaller/truepay/app/ui/scan/views/a/-$$Lambda$b$Unt4ZGX3Vsg6MN1lr6WJZ5FaZZc;
    ((-..Lambda.b.Unt4ZGX3Vsg6MN1lr6WJZ5FaZZc)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    i = R.id.tv_set_amount_fragment_qr_generate;
    paramView = paramView.findViewById(i);
    paramBundle = new com/truecaller/truepay/app/ui/scan/views/a/-$$Lambda$b$dstTI7mFwRnVY5l92lAL0ADZwUo;
    paramBundle.<init>(this);
    paramView.setOnClickListener(paramBundle);
    paramView = (AppCompatActivity)getActivity();
    paramBundle = a;
    paramView.setSupportActionBar(paramBundle);
    paramView = ((AppCompatActivity)getActivity()).getSupportActionBar();
    i = R.string.accept_payments_generate_qr;
    paramBundle = getString(i);
    paramView.setTitle(paramBundle);
    paramView = ((AppCompatActivity)getActivity()).getSupportActionBar();
    i = 1;
    paramView.setDisplayShowTitleEnabled(i);
    ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(i);
    paramView = ((AppCompatActivity)getActivity()).getSupportActionBar();
    int j = R.drawable.ic_close_black_48_px;
    paramView.setHomeAsUpIndicator(j);
    c();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.scan.views.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */