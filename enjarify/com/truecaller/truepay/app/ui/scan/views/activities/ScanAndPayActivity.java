package com.truecaller.truepay.app.ui.scan.views.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.text.TextUtils;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.scan.BharathQRParser.SubTag;
import com.truecaller.truepay.app.ui.scan.BharathQRParser.Tags;
import com.truecaller.truepay.app.ui.scan.a.a.a;
import com.truecaller.truepay.app.ui.scan.views.a.c;
import com.truecaller.truepay.app.ui.transaction.b.n;
import com.truecaller.truepay.app.ui.transaction.b.p;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import java.io.Serializable;
import java.util.HashMap;

public class ScanAndPayActivity
  extends com.truecaller.truepay.app.ui.base.views.a.b
  implements com.truecaller.truepay.app.ui.scan.views.b.b
{
  public static boolean a;
  FrameLayout b;
  
  private void a(Fragment paramFragment)
  {
    try
    {
      Object localObject1 = getSupportFragmentManager();
      localObject1 = ((j)localObject1).a();
      Object localObject2 = paramFragment.getClass();
      localObject2 = ((Class)localObject2).getName();
      ((o)localObject1).a((String)localObject2);
      int i = R.id.scanner_container;
      Object localObject3 = getSupportFragmentManager();
      int j = ((j)localObject3).e();
      localObject3 = Integer.toString(j);
      paramFragment = ((o)localObject1).a(i, paramFragment, (String)localObject3);
      paramFragment.d();
      return;
    }
    catch (Exception localException) {}
  }
  
  public final void a()
  {
    com.truecaller.truepay.app.ui.scan.views.a.b localb = com.truecaller.truepay.app.ui.scan.views.a.b.b();
    a(localb);
  }
  
  public final void a(String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(this, MerchantActivity.class);
    paramString = Uri.parse(paramString);
    localIntent.setData(paramString);
    localIntent.putExtra("isFromScan", true);
    startActivityForResult(localIntent, 1011);
  }
  
  public final void a(HashMap paramHashMap)
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = BharathQRParser.Tags.PA.getValue();
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject2 = BharathQRParser.SubTag.S01.getValue();
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localObject1 = (String)paramHashMap.get(localObject1);
    localObject2 = BharathQRParser.Tags.MC.getValue();
    localObject2 = (String)paramHashMap.get(localObject2);
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    String str1 = BharathQRParser.Tags.TR.getValue();
    ((StringBuilder)localObject3).append(str1);
    str1 = BharathQRParser.SubTag.S01.getValue();
    ((StringBuilder)localObject3).append(str1);
    localObject3 = ((StringBuilder)localObject3).toString();
    localObject3 = (String)paramHashMap.get(localObject3);
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1);
    str1 = null;
    if (bool1)
    {
      localObject1 = "0000";
      bool1 = ((String)localObject1).equals(localObject2);
      if (!bool1)
      {
        bool1 = TextUtils.isEmpty((CharSequence)localObject3);
        if (!bool1) {}
      }
      else
      {
        int i = R.string.transaction_declined;
        paramHashMap = getString(i);
        Toast.makeText(this, paramHashMap, 0).show();
        finish();
        return;
      }
    }
    localObject1 = new com/truecaller/truepay/app/ui/transaction/b/n;
    ((n)localObject1).<init>();
    localObject2 = new com/truecaller/truepay/app/ui/transaction/b/p;
    ((p)localObject2).<init>();
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    String str2 = BharathQRParser.Tags.PA.getValue();
    ((StringBuilder)localObject3).append(str2);
    str2 = BharathQRParser.SubTag.S01.getValue();
    ((StringBuilder)localObject3).append(str2);
    localObject3 = ((StringBuilder)localObject3).toString();
    localObject3 = (String)paramHashMap.get(localObject3);
    e = ((String)localObject3);
    localObject3 = BharathQRParser.Tags.PN.getValue();
    localObject3 = (String)paramHashMap.get(localObject3);
    h = ((String)localObject3);
    localObject3 = BharathQRParser.Tags.AC.getValue();
    localObject3 = (String)paramHashMap.get(localObject3);
    boolean bool2 = TextUtils.isEmpty((CharSequence)localObject3);
    if (!bool2)
    {
      int j = ((String)localObject3).length();
      int k = 11;
      if (j > k)
      {
        str1 = ((String)localObject3).substring(0, k);
        j = ((String)localObject3).length();
        localObject3 = ((String)localObject3).substring(k, j);
        a = ((String)localObject3);
        b = str1;
      }
    }
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    str1 = BharathQRParser.Tags.AD.getValue();
    ((StringBuilder)localObject3).append(str1);
    str1 = BharathQRParser.SubTag.S01.getValue();
    ((StringBuilder)localObject3).append(str1);
    localObject3 = ((StringBuilder)localObject3).toString();
    localObject3 = (String)paramHashMap.get(localObject3);
    c = ((String)localObject3);
    h = ((n)localObject1);
    c = "Stores";
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    localObject3 = BharathQRParser.Tags.TR.getValue();
    ((StringBuilder)localObject1).append((String)localObject3);
    localObject3 = BharathQRParser.SubTag.S01.getValue();
    ((StringBuilder)localObject1).append((String)localObject3);
    localObject1 = ((StringBuilder)localObject1).toString();
    localObject1 = (String)paramHashMap.get(localObject1);
    n = ((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    localObject3 = BharathQRParser.Tags.TN.getValue();
    ((StringBuilder)localObject1).append((String)localObject3);
    localObject3 = BharathQRParser.SubTag.S08.getValue();
    ((StringBuilder)localObject1).append((String)localObject3);
    localObject1 = ((StringBuilder)localObject1).toString();
    localObject1 = (String)paramHashMap.get(localObject1);
    i = ((String)localObject1);
    localObject1 = BharathQRParser.Tags.MC.getValue();
    localObject1 = (String)paramHashMap.get(localObject1);
    r = ((String)localObject1);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    localObject3 = BharathQRParser.Tags.TR.getValue();
    ((StringBuilder)localObject1).append((String)localObject3);
    localObject3 = BharathQRParser.SubTag.S02.getValue();
    ((StringBuilder)localObject1).append((String)localObject3);
    localObject1 = ((StringBuilder)localObject1).toString();
    localObject1 = paramHashMap.get(localObject1);
    localObject1 = (String)paramHashMap.get(localObject1);
    q = ((String)localObject1);
    localObject1 = "qr_pay";
    d = ((String)localObject1);
    try
    {
      localObject1 = BharathQRParser.Tags.AM;
      localObject1 = ((BharathQRParser.Tags)localObject1).getValue();
      localObject1 = paramHashMap.get(localObject1);
      localObject1 = (String)localObject1;
      float f = Float.parseFloat((String)localObject1);
      localObject1 = String.valueOf(f);
      e = ((String)localObject1);
      localObject1 = BharathQRParser.Tags.AM;
      localObject1 = ((BharathQRParser.Tags)localObject1).getValue();
      localObject1 = paramHashMap.get(localObject1);
      localObject1 = (String)localObject1;
      f = Float.parseFloat((String)localObject1);
      localObject1 = String.valueOf(f);
      f = ((String)localObject1);
    }
    catch (Exception localException1)
    {
      label814:
      for (;;) {}
    }
    try
    {
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      localObject3 = BharathQRParser.Tags.PA;
      localObject3 = ((BharathQRParser.Tags)localObject3).getValue();
      ((StringBuilder)localObject1).append((String)localObject3);
      localObject3 = BharathQRParser.SubTag.S02;
      localObject3 = ((BharathQRParser.SubTag)localObject3).getValue();
      ((StringBuilder)localObject1).append((String)localObject3);
      localObject1 = ((StringBuilder)localObject1).toString();
      paramHashMap = paramHashMap.get(localObject1);
      paramHashMap = (String)paramHashMap;
      p = paramHashMap;
    }
    catch (Exception localException2)
    {
      break label814;
    }
    paramHashMap = new android/content/Intent;
    paramHashMap.<init>(this, TransactionActivity.class);
    paramHashMap.putExtra("payable_object", (Serializable)localObject2);
    paramHashMap.putExtra("type", "pay");
    startActivityForResult(paramHashMap, 1011);
  }
  
  public int getLayoutId()
  {
    return R.layout.activity_scan_pay;
  }
  
  public void initDagger(com.truecaller.truepay.app.a.a.a parama)
  {
    parama = com.truecaller.truepay.app.ui.scan.a.a.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    parama.a(locala).a().a(this);
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    paramInt2 = 1011;
    if (paramInt1 == paramInt2) {
      finish();
    }
  }
  
  public void onBackPressed()
  {
    boolean bool = a;
    if (bool) {
      return;
    }
    j localj = getSupportFragmentManager();
    int i = localj.e();
    int j = 1;
    if (i != j)
    {
      super.onBackPressed();
      return;
    }
    finish();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = R.id.overlay_progress_frame;
    paramBundle = (FrameLayout)findViewById(i);
    b = paramBundle;
    paramBundle = c.a(getIntent().getStringExtra("from"));
    a(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.scan.views.activities.ScanAndPayActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */