package com.truecaller.truepay.app.ui.scan;

import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;

public final class b$1
  implements ViewTreeObserver.OnGlobalLayoutListener
{
  private int c;
  
  public b$1(View paramView, a parama) {}
  
  public final void onGlobalLayout()
  {
    View localView = a;
    int i = localView.getHeight();
    int j = c;
    if (j != 0)
    {
      a locala;
      boolean bool;
      if (j > i)
      {
        locala = b;
        bool = true;
        locala.a(bool);
      }
      else if (j < i)
      {
        locala = b;
        bool = false;
        locala.a(false);
      }
    }
    c = i;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.scan.b.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */