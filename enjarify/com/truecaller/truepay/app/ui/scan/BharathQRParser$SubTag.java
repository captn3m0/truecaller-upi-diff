package com.truecaller.truepay.app.ui.scan;

public enum BharathQRParser$SubTag
{
  public final String value;
  
  static
  {
    Object localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$SubTag;
    ((SubTag)localObject).<init>("S00", 0, "00");
    S00 = (SubTag)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$SubTag;
    int i = 1;
    ((SubTag)localObject).<init>("S01", i, "01");
    S01 = (SubTag)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$SubTag;
    int j = 2;
    ((SubTag)localObject).<init>("S02", j, "02");
    S02 = (SubTag)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$SubTag;
    int k = 3;
    ((SubTag)localObject).<init>("S03", k, "03");
    S03 = (SubTag)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$SubTag;
    int m = 4;
    ((SubTag)localObject).<init>("S04", m, "04");
    S04 = (SubTag)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$SubTag;
    int n = 5;
    ((SubTag)localObject).<init>("S05", n, "05");
    S05 = (SubTag)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$SubTag;
    int i1 = 6;
    ((SubTag)localObject).<init>("S06", i1, "06");
    S06 = (SubTag)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$SubTag;
    int i2 = 7;
    ((SubTag)localObject).<init>("S07", i2, "07");
    S07 = (SubTag)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$SubTag;
    int i3 = 8;
    ((SubTag)localObject).<init>("S08", i3, "08");
    S08 = (SubTag)localObject;
    localObject = new SubTag[9];
    SubTag localSubTag = S00;
    localObject[0] = localSubTag;
    localSubTag = S01;
    localObject[i] = localSubTag;
    localSubTag = S02;
    localObject[j] = localSubTag;
    localSubTag = S03;
    localObject[k] = localSubTag;
    localSubTag = S04;
    localObject[m] = localSubTag;
    localSubTag = S05;
    localObject[n] = localSubTag;
    localSubTag = S06;
    localObject[i1] = localSubTag;
    localSubTag = S07;
    localObject[i2] = localSubTag;
    localSubTag = S08;
    localObject[i3] = localSubTag;
    $VALUES = (SubTag[])localObject;
  }
  
  private BharathQRParser$SubTag(String paramString1)
  {
    value = paramString1;
  }
  
  public static String valueOf(BharathQRParser.Tags paramTags)
  {
    return paramTags.getValue();
  }
  
  public final String getValue()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.scan.BharathQRParser.SubTag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */