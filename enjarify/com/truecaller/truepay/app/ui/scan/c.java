package com.truecaller.truepay.app.ui.scan;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import com.truecaller.truepay.R.color;
import me.dm7.barcodescanner.core.h;

public final class c
  extends h
{
  public c(Context paramContext)
  {
    super(paramContext);
    paramContext = paramContext.getResources();
    int i = R.color.white;
    int j = paramContext.getColor(i);
    setBorderColor(j);
    setBorderLineLength(60);
  }
  
  public final void onDraw(Canvas paramCanvas)
  {
    a(paramCanvas);
    b(paramCanvas);
    c(paramCanvas);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.scan.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */