package com.truecaller.truepay.app.ui.scan;

public enum BharathQRParser$Tags
{
  public final String value;
  
  static
  {
    Object localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$Tags;
    ((Tags)localObject).<init>("T00", 0, "00");
    T00 = (Tags)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$Tags;
    int i = 1;
    ((Tags)localObject).<init>("T01", i, "01");
    T01 = (Tags)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$Tags;
    int j = 2;
    ((Tags)localObject).<init>("T02", j, "02");
    T02 = (Tags)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$Tags;
    int k = 3;
    ((Tags)localObject).<init>("T04", k, "04");
    T04 = (Tags)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$Tags;
    int m = 4;
    ((Tags)localObject).<init>("T06", m, "06");
    T06 = (Tags)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$Tags;
    int n = 5;
    ((Tags)localObject).<init>("AC", n, "08");
    AC = (Tags)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$Tags;
    int i1 = 6;
    ((Tags)localObject).<init>("PA", i1, "26");
    PA = (Tags)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$Tags;
    int i2 = 7;
    ((Tags)localObject).<init>("TR", i2, "27");
    TR = (Tags)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$Tags;
    int i3 = 8;
    ((Tags)localObject).<init>("AD", i3, "28");
    AD = (Tags)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$Tags;
    int i4 = 9;
    ((Tags)localObject).<init>("MC", i4, "52");
    MC = (Tags)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$Tags;
    int i5 = 10;
    ((Tags)localObject).<init>("CU", i5, "53");
    CU = (Tags)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$Tags;
    int i6 = 11;
    ((Tags)localObject).<init>("AM", i6, "54");
    AM = (Tags)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$Tags;
    ((Tags)localObject).<init>("IN", 12, "58");
    IN = (Tags)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$Tags;
    ((Tags)localObject).<init>("PN", 13, "59");
    PN = (Tags)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$Tags;
    ((Tags)localObject).<init>("T60", 14, "60");
    T60 = (Tags)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$Tags;
    ((Tags)localObject).<init>("T61", 15, "61");
    T61 = (Tags)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$Tags;
    ((Tags)localObject).<init>("TN", 16, "62");
    TN = (Tags)localObject;
    localObject = new com/truecaller/truepay/app/ui/scan/BharathQRParser$Tags;
    ((Tags)localObject).<init>("T63", 17, "63");
    T63 = (Tags)localObject;
    localObject = new Tags[18];
    Tags localTags = T00;
    localObject[0] = localTags;
    localTags = T01;
    localObject[i] = localTags;
    localTags = T02;
    localObject[j] = localTags;
    localTags = T04;
    localObject[k] = localTags;
    localTags = T06;
    localObject[m] = localTags;
    localTags = AC;
    localObject[n] = localTags;
    localTags = PA;
    localObject[i1] = localTags;
    localTags = TR;
    localObject[i2] = localTags;
    localTags = AD;
    localObject[i3] = localTags;
    localTags = MC;
    localObject[i4] = localTags;
    localTags = CU;
    localObject[i5] = localTags;
    localTags = AM;
    localObject[i6] = localTags;
    localTags = IN;
    localObject[12] = localTags;
    localTags = PN;
    localObject[13] = localTags;
    localTags = T60;
    localObject[14] = localTags;
    localTags = T61;
    localObject[15] = localTags;
    localTags = TN;
    localObject[16] = localTags;
    localTags = T63;
    localObject[17] = localTags;
    $VALUES = (Tags[])localObject;
  }
  
  private BharathQRParser$Tags(String paramString1)
  {
    value = paramString1;
  }
  
  public static String valueOf(Tags paramTags)
  {
    return paramTags.getValue();
  }
  
  public final String getValue()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.scan.BharathQRParser.Tags
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */