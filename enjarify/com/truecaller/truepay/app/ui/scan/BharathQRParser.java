package com.truecaller.truepay.app.ui.scan;

import android.text.TextUtils;
import java.util.HashMap;

public final class BharathQRParser
{
  private HashMap a;
  
  public BharathQRParser()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    a = localHashMap;
  }
  
  public final HashMap a(String paramString)
  {
    BharathQRParser localBharathQRParser = this;
    int i = 1;
    boolean bool1 = false;
    String str1 = null;
    int j = 2;
    try
    {
      String str2 = paramString.trim();
      Object localObject1 = BharathQRParser.Tags.values();
      int k = localObject1.length;
      Object localObject2 = str2;
      int m = 0;
      str2 = null;
      while (m < k)
      {
        Object localObject3 = localObject1[m];
        boolean bool2 = TextUtils.isEmpty((CharSequence)localObject2);
        if (!bool2)
        {
          String str3 = ((String)localObject2).substring(0, j);
          String str4 = ((BharathQRParser.Tags)localObject3).getValue();
          bool2 = str4.equals(str3);
          if (bool2)
          {
            str3 = ((String)localObject2).substring(j);
            int n = str3.length();
            if (n > i)
            {
              localObject2 = str3.substring(0, j);
              int i1 = Integer.parseInt((String)localObject2) + j;
              str4 = str3.substring(j, i1);
              Object localObject4 = ((BharathQRParser.Tags)localObject3).getValue();
              String str5 = "26|27|28|62";
              boolean bool3 = ((String)localObject4).matches(str5);
              if (bool3)
              {
                localObject4 = BharathQRParser.SubTag.values();
                int i2 = localObject4.length;
                Object localObject5 = str4;
                n = 0;
                str4 = null;
                while (n < i2)
                {
                  Object localObject6 = localObject4[n];
                  boolean bool4 = TextUtils.isEmpty((CharSequence)localObject5);
                  if (!bool4)
                  {
                    String str6 = ((String)localObject5).substring(0, j);
                    str1 = ((BharathQRParser.SubTag)localObject6).getValue();
                    bool1 = str1.equals(str6);
                    if (bool1)
                    {
                      str1 = ((String)localObject5).substring(j);
                      int i3 = str1.length();
                      if (i3 > i)
                      {
                        i3 = 0;
                        str6 = null;
                        localObject5 = str1.substring(0, j);
                        int i4 = Integer.parseInt((String)localObject5) + j;
                        str6 = str1.substring(j, i4);
                        localObject7 = a;
                        Object localObject8 = new java/lang/StringBuilder;
                        ((StringBuilder)localObject8).<init>();
                        paramString = (String)localObject1;
                        localObject1 = ((BharathQRParser.Tags)localObject3).getValue();
                        ((StringBuilder)localObject8).append((String)localObject1);
                        localObject1 = ((BharathQRParser.SubTag)localObject6).getValue();
                        ((StringBuilder)localObject8).append((String)localObject1);
                        localObject8 = ((StringBuilder)localObject8).toString();
                        ((HashMap)localObject7).put(localObject8, str6);
                        localObject7 = str1.substring(i4);
                        localObject5 = localObject7;
                      }
                      else
                      {
                        paramString = (String)localObject1;
                      }
                    }
                    else
                    {
                      paramString = (String)localObject1;
                    }
                  }
                  else
                  {
                    paramString = (String)localObject1;
                  }
                  n += 1;
                  localObject1 = paramString;
                  i = 1;
                  bool1 = false;
                  str1 = null;
                  j = 2;
                }
                paramString = (String)localObject1;
              }
              else
              {
                paramString = (String)localObject1;
                localObject7 = a;
                str1 = ((BharathQRParser.Tags)localObject3).getValue();
                ((HashMap)localObject7).put(str1, str4);
              }
              localObject7 = str3.substring(i1);
              localObject2 = localObject7;
            }
            else
            {
              paramString = (String)localObject1;
            }
          }
          else
          {
            paramString = (String)localObject1;
          }
        }
        else
        {
          paramString = (String)localObject1;
        }
        m += 1;
        localObject1 = paramString;
        i = 1;
        bool1 = false;
        str1 = null;
        j = 2;
      }
      i = 2;
    }
    catch (Exception localException)
    {
      i = 2;
    }
    Object localObject7 = new String[i];
    localObject7[0] = "hashMap";
    str1 = a.toString();
    localObject7[1] = str1;
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.scan.BharathQRParser
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */