package com.truecaller.truepay.app.ui.scan;

import android.util.Patterns;
import java.util.regex.Pattern;

public final class TextDetectors
{
  private static final Pattern a = Patterns.PHONE;
  private static final Pattern b = Pattern.compile("([\\da-zA-Z-._]+@[a-zA-Z\\d]{3,})(?![\\w\\d.])", 2);
  private static final Pattern c = Pattern.compile("[0-9]");
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.scan.TextDetectors
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */