package com.truecaller.truepay.app.ui.bankList;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.GridLayoutManager.SpanSizeLookup;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import c.g.b.k;
import c.u;
import com.truecaller.truepay.R.drawable;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.integer;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.menu;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.registration.c.e;
import com.truecaller.truepay.app.ui.registration.views.b.c;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.utils.extensions.t;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public final class a
  extends com.truecaller.truepay.app.ui.registration.views.b.d
  implements com.truecaller.truepay.app.ui.registration.a.a.b, com.truecaller.truepay.app.ui.registration.views.c.b
{
  public static final a.a c;
  public com.truecaller.truepay.app.ui.registration.d.b a;
  public r b;
  private Toolbar d;
  private ImageView e;
  private RecyclerView f;
  private ArrayList g;
  private ArrayList h;
  private com.truecaller.truepay.app.ui.registration.a.a i;
  private GridLayoutManager j;
  private String k;
  private a.b l;
  private HashMap n;
  
  static
  {
    a.a locala = new com/truecaller/truepay/app/ui/bankList/a$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public final int a()
  {
    return R.layout.fragment_bank_list;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = n;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      n = ((HashMap)localObject1);
    }
    localObject1 = n;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = n;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(com.truecaller.truepay.app.ui.registration.c.d paramd)
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    g = ((ArrayList)localObject1);
    localObject1 = new com/truecaller/truepay/app/ui/registration/c/e;
    ((e)localObject1).<init>();
    boolean bool1 = false;
    Object localObject2 = null;
    boolean bool2;
    if (paramd != null)
    {
      localObject3 = paramd.b();
    }
    else
    {
      bool2 = false;
      localObject3 = null;
    }
    int i2 = 1;
    if (localObject3 != null)
    {
      localObject3 = paramd.b();
      localObject4 = "bankListDO.popularBanks";
      k.a(localObject3, (String)localObject4);
      localObject3 = (Collection)localObject3;
      bool2 = ((Collection)localObject3).isEmpty() ^ i2;
      if (bool2)
      {
        i1 = R.string.pupular_banks_bank_selection;
        localObject3 = getString(i1);
        ((e)localObject1).a((String)localObject3);
        localObject3 = paramd.b();
        ((e)localObject1).a((List)localObject3);
        localObject3 = g;
        if (localObject3 == null)
        {
          localObject4 = "bankItems";
          k.a((String)localObject4);
        }
        ((ArrayList)localObject3).add(localObject1);
      }
    }
    if (paramd != null) {
      localObject2 = paramd.a();
    }
    localObject1 = null;
    if (localObject2 != null)
    {
      localObject2 = paramd.a();
      localObject3 = "bankListDO.banks";
      k.a(localObject2, (String)localObject3);
      localObject2 = (Collection)localObject2;
      bool1 = ((Collection)localObject2).isEmpty() ^ i2;
      if (bool1)
      {
        localObject2 = new com/truecaller/truepay/app/ui/registration/c/e;
        ((e)localObject2).<init>();
        localObject3 = new java/util/ArrayList;
        localObject4 = (Collection)paramd.a();
        ((ArrayList)localObject3).<init>((Collection)localObject4);
        h = ((ArrayList)localObject3);
        paramd = paramd.a();
        ((e)localObject2).a(paramd);
        int i3 = R.string.all_banks_bank_selection;
        paramd = getString(i3);
        ((e)localObject2).a(paramd);
        paramd = g;
        if (paramd == null)
        {
          localObject3 = "bankItems";
          k.a((String)localObject3);
        }
        paramd.add(localObject2);
        break label325;
      }
    }
    paramd = e;
    if (paramd == null)
    {
      localObject2 = "emptyIcon";
      k.a((String)localObject2);
    }
    paramd.setVisibility(0);
    label325:
    paramd = new com/truecaller/truepay/app/ui/registration/a/a;
    localObject2 = getContext();
    Object localObject3 = g;
    if (localObject3 == null)
    {
      localObject4 = "bankItems";
      k.a((String)localObject4);
    }
    Object localObject4 = this;
    localObject4 = (com.truecaller.truepay.app.ui.registration.a.a.b)this;
    r localr = b;
    if (localr == null)
    {
      String str = "imageLoader";
      k.a(str);
    }
    paramd.<init>((Context)localObject2, (ArrayList)localObject3, (com.truecaller.truepay.app.ui.registration.a.a.b)localObject4, localr);
    i = paramd;
    paramd = new android/support/v7/widget/GridLayoutManager;
    localObject2 = getContext();
    localObject3 = getResources();
    int i4 = R.integer.grid_span;
    int i1 = ((Resources)localObject3).getInteger(i4);
    paramd.<init>((Context)localObject2, i1);
    j = paramd;
    paramd = j;
    if (paramd == null)
    {
      localObject2 = "layoutManager";
      k.a((String)localObject2);
    }
    localObject2 = new com/truecaller/truepay/app/ui/bankList/a$d;
    ((a.d)localObject2).<init>(this);
    localObject2 = (GridLayoutManager.SpanSizeLookup)localObject2;
    paramd.setSpanSizeLookup((GridLayoutManager.SpanSizeLookup)localObject2);
    paramd = new android/support/v7/widget/DividerItemDecoration;
    localObject2 = getContext();
    paramd.<init>((Context)localObject2, i2);
    localObject2 = getContext();
    if (localObject2 == null) {
      k.a();
    }
    i1 = R.drawable.divider;
    localObject2 = android.support.v4.content.b.a((Context)localObject2, i1);
    if (localObject2 == null) {
      k.a();
    }
    paramd.setDrawable((Drawable)localObject2);
    localObject2 = f;
    if (localObject2 == null)
    {
      localObject3 = "bankRecyclerView";
      k.a((String)localObject3);
    }
    paramd = (RecyclerView.ItemDecoration)paramd;
    ((RecyclerView)localObject2).addItemDecoration(paramd);
    paramd = new android/support/v7/widget/DividerItemDecoration;
    localObject2 = getContext();
    paramd.<init>((Context)localObject2, 0);
    localObject1 = getContext();
    if (localObject1 == null) {
      k.a();
    }
    int m = R.drawable.divider;
    localObject1 = android.support.v4.content.b.a((Context)localObject1, m);
    if (localObject1 == null) {
      k.a();
    }
    paramd.setDrawable((Drawable)localObject1);
    localObject1 = f;
    if (localObject1 == null)
    {
      localObject2 = "bankRecyclerView";
      k.a((String)localObject2);
    }
    paramd = (RecyclerView.ItemDecoration)paramd;
    ((RecyclerView)localObject1).addItemDecoration(paramd);
    paramd = f;
    if (paramd == null)
    {
      localObject1 = "bankRecyclerView";
      k.a((String)localObject1);
    }
    localObject1 = j;
    if (localObject1 == null)
    {
      localObject2 = "layoutManager";
      k.a((String)localObject2);
    }
    localObject1 = (RecyclerView.LayoutManager)localObject1;
    paramd.setLayoutManager((RecyclerView.LayoutManager)localObject1);
    paramd = f;
    if (paramd == null)
    {
      localObject1 = "bankRecyclerView";
      k.a((String)localObject1);
    }
    localObject1 = i;
    if (localObject1 == null)
    {
      localObject2 = "recyclerAdapter";
      k.a((String)localObject2);
    }
    localObject1 = (RecyclerView.Adapter)localObject1;
    paramd.setAdapter((RecyclerView.Adapter)localObject1);
  }
  
  public final void a(com.truecaller.truepay.data.d.a parama)
  {
    k.b(parama, "bank");
    Object localObject = Truepay.getInstance();
    String str1 = "Truepay.getInstance()";
    k.a(localObject, str1);
    com.truecaller.truepay.data.b.a locala = ((Truepay)localObject).getAnalyticLoggerHelper();
    String str2 = "app_payment_select_bank";
    String str3 = "initiated";
    String str4 = k;
    if (str4 == null)
    {
      localObject = "bankSelectionContext";
      k.a((String)localObject);
    }
    String str5 = "select_bank";
    String str6 = parama.b();
    double d1 = 0.0D;
    String str7 = "";
    locala.a(str2, str3, str4, str5, str6, d1, str7);
    localObject = l;
    if (localObject != null)
    {
      ((a.b)localObject).a(parama);
      return;
    }
  }
  
  public final void a(Throwable paramThrowable)
  {
    Object localObject1 = requireContext();
    int m = R.string.server_error_message;
    Object localObject2 = (CharSequence)getString(m);
    Toast.makeText((Context)localObject1, (CharSequence)localObject2, 0).show();
    localObject1 = new java/lang/AssertionError;
    localObject2 = new java/lang/StringBuilder;
    String str = "fetchBankListError";
    ((StringBuilder)localObject2).<init>(str);
    if (paramThrowable != null) {
      paramThrowable = paramThrowable.getMessage();
    } else {
      paramThrowable = null;
    }
    ((StringBuilder)localObject2).append(paramThrowable);
    paramThrowable = ((StringBuilder)localObject2).toString();
    ((AssertionError)localObject1).<init>(paramThrowable);
    com.truecaller.log.d.a((Throwable)localObject1);
  }
  
  public final void a(boolean paramBoolean)
  {
    int m = R.id.pbBankList;
    ProgressBar localProgressBar = (ProgressBar)a(m);
    k.a(localProgressBar, "pbBankList");
    t.a((View)localProgressBar, paramBoolean);
  }
  
  public final void b()
  {
    com.truecaller.truepay.app.ui.registration.b.a.a locala = com.truecaller.truepay.app.ui.registration.b.a.a();
    com.truecaller.truepay.app.a.a.a locala1 = Truepay.getApplicationComponent();
    locala.a(locala1).a().a(this);
  }
  
  public final void c()
  {
    HashMap localHashMap = n;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void onAttach(Activity paramActivity)
  {
    super.onAttach(paramActivity);
    if (paramActivity != null) {
      localObject1 = paramActivity;
    }
    try
    {
      localObject1 = (a.b)paramActivity;
      l = ((a.b)localObject1);
      return;
    }
    catch (ClassCastException localClassCastException)
    {
      Object localObject2;
      for (;;) {}
    }
    Object localObject1 = new c/u;
    localObject2 = "null cannot be cast to non-null type com.truecaller.truepay.app.ui.bankList.BankListFragment.OnFragmentInteractionListener";
    ((u)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
    localObject1 = new java/lang/ClassCastException;
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    paramActivity = String.valueOf(paramActivity);
    ((StringBuilder)localObject2).append(paramActivity);
    ((StringBuilder)localObject2).append(" must implement OnFragmentInteractionListener");
    paramActivity = ((StringBuilder)localObject2).toString();
    ((ClassCastException)localObject1).<init>(paramActivity);
    throw ((Throwable)localObject1);
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (paramMenu != null) {
      paramMenu.clear();
    }
    if (paramMenuInflater != null)
    {
      int m = R.menu.menu_frag_banks;
      paramMenuInflater.inflate(m, paramMenu);
    }
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    setHasOptionsMenu(true);
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    l = null;
    com.truecaller.truepay.app.ui.registration.d.b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localb.b();
    c();
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem != null)
    {
      int m = paramMenuItem.getItemId();
      int i1 = R.id.action_search;
      if (m == i1)
      {
        Object localObject1 = h;
        Object localObject2;
        if (localObject1 == null)
        {
          localObject2 = "bankList";
          k.a((String)localObject2);
        }
        localObject1 = (Collection)localObject1;
        boolean bool = ((Collection)localObject1).isEmpty() ^ true;
        if (bool)
        {
          localObject1 = h;
          if (localObject1 == null)
          {
            localObject2 = "bankList";
            k.a((String)localObject2);
          }
          localObject1 = c.a((ArrayList)localObject1);
          localObject2 = getActivity();
          if (localObject2 != null)
          {
            localObject2 = ((f)localObject2).getSupportFragmentManager();
          }
          else
          {
            i1 = 0;
            localObject2 = null;
          }
          String str = c.class.getName();
          ((c)localObject1).show((j)localObject2, str);
        }
      }
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    int m = R.id.toolbar;
    paramBundle = paramView.findViewById(m);
    k.a(paramBundle, "view.findViewById(R.id.toolbar)");
    paramBundle = (Toolbar)paramBundle;
    d = paramBundle;
    m = R.id.rvBankList;
    paramBundle = paramView.findViewById(m);
    Object localObject = "view.findViewById(R.id.rvBankList)";
    k.a(paramBundle, (String)localObject);
    paramBundle = (RecyclerView)paramBundle;
    f = paramBundle;
    m = R.id.ivEmptyIcon;
    paramView = paramView.findViewById(m);
    paramBundle = "view.findViewById(R.id.ivEmptyIcon)";
    k.a(paramView, paramBundle);
    paramView = (ImageView)paramView;
    e = paramView;
    paramView = (AppCompatActivity)getActivity();
    if (paramView != null)
    {
      paramBundle = d;
      if (paramBundle == null)
      {
        localObject = "toolbar";
        k.a((String)localObject);
      }
      paramView.setSupportActionBar(paramBundle);
    }
    int i1;
    if (paramView != null)
    {
      paramBundle = paramView.getSupportActionBar();
      if (paramBundle != null)
      {
        i1 = R.string.message_bank_list_toolbar;
        localObject = (CharSequence)getString(i1);
        paramBundle.setTitle((CharSequence)localObject);
      }
    }
    m = 1;
    if (paramView != null)
    {
      localObject = paramView.getSupportActionBar();
      if (localObject != null) {
        ((ActionBar)localObject).setDisplayShowTitleEnabled(m);
      }
    }
    if (paramView != null)
    {
      localObject = paramView.getSupportActionBar();
      if (localObject != null) {
        ((ActionBar)localObject).setDisplayHomeAsUpEnabled(m);
      }
    }
    if (paramView != null)
    {
      paramBundle = paramView.getSupportActionBar();
      if (paramBundle != null)
      {
        i1 = R.drawable.ic_close_48_px;
        paramBundle.setHomeAsUpIndicator(i1);
      }
    }
    paramBundle = d;
    if (paramBundle == null)
    {
      localObject = "toolbar";
      k.a((String)localObject);
    }
    localObject = new com/truecaller/truepay/app/ui/bankList/a$c;
    ((a.c)localObject).<init>(paramView);
    localObject = (View.OnClickListener)localObject;
    paramBundle.setNavigationOnClickListener((View.OnClickListener)localObject);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramBundle = this;
    paramBundle = (com.truecaller.truepay.app.ui.base.views.a)this;
    paramView.a(paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a();
    paramView = getArguments();
    if (paramView != null)
    {
      paramBundle = "bank_selection_context";
      paramView = paramView.getString(paramBundle);
      if (paramView != null) {}
    }
    else
    {
      paramView = "other";
    }
    k = paramView;
    paramView = Truepay.getInstance();
    k.a(paramView, "Truepay.getInstance()");
    paramView = paramView.getAnalyticLoggerHelper();
    paramBundle = "shown";
    localObject = k;
    if (localObject == null)
    {
      String str = "bankSelectionContext";
      k.a(str);
    }
    paramView.a(paramBundle, (String)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.bankList.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */