package com.truecaller.truepay.app.ui.bankList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.AppCompatActivity;
import c.g.b.k;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import java.io.Serializable;

public final class BankListActivity
  extends AppCompatActivity
  implements a.b
{
  public final void a(com.truecaller.truepay.data.d.a parama)
  {
    k.b(parama, "bank");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    parama = (Serializable)parama;
    localIntent.putExtra("selected_bank", parama);
    setResult(-1, localIntent);
    finish();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = R.layout.activity_bank_list;
    setContentView(i);
    paramBundle = getSupportFragmentManager().a();
    int j = R.id.container;
    Object localObject = a.c;
    localObject = getIntent().getStringExtra("bank_selection_context");
    k.a(localObject, "intent.getStringExtra(BANK_SELECTION_CONTEXT)");
    k.b(localObject, "bankSelectionContext");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localObject = (Serializable)localObject;
    localBundle.putSerializable("bank_selection_context", (Serializable)localObject);
    localObject = new com/truecaller/truepay/app/ui/bankList/a;
    ((a)localObject).<init>();
    ((a)localObject).setArguments(localBundle);
    localObject = (Fragment)localObject;
    paramBundle.b(j, (Fragment)localObject).c();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.bankList.BankListActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */