package com.truecaller.truepay.app.ui.bankList;

import android.support.v7.widget.GridLayoutManager.SpanSizeLookup;

public final class a$d
  extends GridLayoutManager.SpanSizeLookup
{
  a$d(a parama) {}
  
  public final int getSpanSize(int paramInt)
  {
    com.truecaller.truepay.app.ui.registration.a.a locala = a.a(a);
    paramInt = locala.getItemViewType(paramInt);
    int i = 3;
    switch (paramInt)
    {
    default: 
      return i;
    case 2: 
      return i;
    }
    return 1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.bankList.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */