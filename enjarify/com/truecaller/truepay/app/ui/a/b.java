package com.truecaller.truepay.app.ui.a;

import android.content.Context;
import android.content.res.AssetManager;
import com.google.gson.f;
import com.truecaller.utils.extensions.d;
import java.io.Closeable;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;

public final class b
  implements a
{
  private final Context a;
  private final f b;
  
  public b(Context paramContext, f paramf)
  {
    a = paramContext;
    b = paramf;
  }
  
  public final Object a(String paramString, Type paramType)
  {
    Object localObject1 = a.getAssets();
    paramString = ((AssetManager)localObject1).open(paramString);
    try
    {
      localObject1 = b;
      Object localObject2 = new java/io/InputStreamReader;
      ((InputStreamReader)localObject2).<init>(paramString);
      localObject2 = (Reader)localObject2;
      paramType = ((f)localObject1).a((Reader)localObject2, paramType);
      return paramType;
    }
    finally
    {
      d.a((Closeable)paramString);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */