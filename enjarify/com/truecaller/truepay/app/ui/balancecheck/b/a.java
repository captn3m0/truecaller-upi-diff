package com.truecaller.truepay.app.ui.balancecheck.b;

import android.graphics.drawable.Drawable;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.app.ui.balancecheck.c.a.a.a;
import com.truecaller.truepay.app.ui.balancecheck.c.a.a.b;
import com.truecaller.truepay.app.utils.r;
import com.truecaller.truepay.data.api.g;
import com.truecaller.truepay.data.e.c;
import com.truecaller.utils.n;

public final class a
  extends ba
  implements a.a
{
  private boolean c;
  private final f d;
  private final f e;
  private final r f;
  private final g g;
  private final com.truecaller.truepay.app.ui.npci.e h;
  private final c i;
  private final n j;
  private final com.truecaller.truepay.app.utils.a k;
  private final com.truecaller.utils.a l;
  
  public a(f paramf1, f paramf2, r paramr, g paramg, com.truecaller.truepay.app.ui.npci.e parame, c paramc, n paramn, com.truecaller.truepay.app.utils.a parama, com.truecaller.utils.a parama1)
  {
    super(paramf1);
    d = paramf1;
    e = paramf2;
    f = paramr;
    g = paramg;
    h = parame;
    i = paramc;
    j = paramn;
    k = parama;
    l = parama1;
    c = true;
  }
  
  private final void a(String paramString)
  {
    a.b localb = (a.b)b;
    if (localb == null) {
      return;
    }
    localb.b(false);
    localb.a(false);
    localb.d(paramString);
    paramString = j;
    int m = R.string.pay_bank_account_balance_failed;
    Object[] arrayOfObject = new Object[0];
    paramString = paramString.a(m, arrayOfObject);
    k.a(paramString, "resourceProvider.getStri…k_account_balance_failed)");
    localb.a(paramString);
  }
  
  public final void a()
  {
    a.b localb = (a.b)b;
    if (localb == null) {
      return;
    }
    Object localObject1 = k;
    Object localObject2 = localb.a();
    if (localObject2 != null) {
      localObject2 = ((com.truecaller.truepay.app.ui.dashboard.b.a)localObject2).c();
    } else {
      localObject2 = null;
    }
    localObject1 = ((com.truecaller.truepay.app.utils.a)localObject1).a((String)localObject2);
    k.a(localObject1, "accountManager.getAccoun…countID(getAccount()?.id)");
    localb.a((com.truecaller.truepay.data.api.model.a)localObject1);
  }
  
  public final void a(com.truecaller.truepay.app.ui.dashboard.b.a parama)
  {
    if (parama != null)
    {
      Object localObject1 = (a.b)b;
      if (localObject1 != null)
      {
        Object localObject2 = j;
        int m = R.string.pay_bank_account_balance;
        Object[] arrayOfObject = new Object[0];
        localObject2 = ((n)localObject2).a(m, arrayOfObject);
        k.a(localObject2, "resourceProvider.getStri…pay_bank_account_balance)");
        ((a.b)localObject1).a((String)localObject2);
        boolean bool = true;
        ((a.b)localObject1).a(bool);
        localObject2 = parama.e();
        k.a(localObject2, "account.accNumber");
        ((a.b)localObject1).b((String)localObject2);
        localObject2 = parama.a();
        ((a.b)localObject1).c((String)localObject2);
        localObject2 = f;
        String str = parama.b();
        localObject2 = ((r)localObject2).b(str);
        ((a.b)localObject1).a((Drawable)localObject2);
      }
      parama = parama.c();
      k.a(parama, "it.id");
      localObject1 = new com/truecaller/truepay/app/ui/balancecheck/b/a$c;
      ((a.c)localObject1).<init>(this, parama, null);
      localObject1 = (m)localObject1;
      kotlinx.coroutines.e.b(this, null, (m)localObject1, 3);
      return;
    }
  }
  
  public final void e()
  {
    boolean bool = c;
    if (!bool)
    {
      Object localObject = j;
      int m = R.string.something_went_wrong;
      Object[] arrayOfObject = new Object[0];
      localObject = ((n)localObject).a(m, arrayOfObject);
      String str = "resourceProvider.getStri…ing.something_went_wrong)";
      k.a(localObject, str);
      a((String)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.balancecheck.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */