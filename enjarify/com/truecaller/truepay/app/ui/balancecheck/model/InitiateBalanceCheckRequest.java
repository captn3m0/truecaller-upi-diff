package com.truecaller.truepay.app.ui.balancecheck.model;

import c.g.b.k;

public final class InitiateBalanceCheckRequest
{
  private final String accountId;
  
  public InitiateBalanceCheckRequest(String paramString)
  {
    accountId = paramString;
  }
  
  public final String component1()
  {
    return accountId;
  }
  
  public final InitiateBalanceCheckRequest copy(String paramString)
  {
    k.b(paramString, "accountId");
    InitiateBalanceCheckRequest localInitiateBalanceCheckRequest = new com/truecaller/truepay/app/ui/balancecheck/model/InitiateBalanceCheckRequest;
    localInitiateBalanceCheckRequest.<init>(paramString);
    return localInitiateBalanceCheckRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof InitiateBalanceCheckRequest;
      if (bool1)
      {
        paramObject = (InitiateBalanceCheckRequest)paramObject;
        String str = accountId;
        paramObject = accountId;
        boolean bool2 = k.a(str, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final String getAccountId()
  {
    return accountId;
  }
  
  public final int hashCode()
  {
    String str = accountId;
    if (str != null) {
      return str.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("InitiateBalanceCheckRequest(accountId=");
    String str = accountId;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.balancecheck.model.InitiateBalanceCheckRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */