package com.truecaller.truepay.app.ui.balancecheck.model;

import c.g.b.k;
import com.google.gson.i;

public final class ConfirmBalanceCheckRequest
{
  private String bankAccountId;
  private final i data;
  private String pspRefNo;
  
  public ConfirmBalanceCheckRequest(i parami, String paramString1, String paramString2)
  {
    data = parami;
    pspRefNo = paramString1;
    bankAccountId = paramString2;
  }
  
  private final String component2()
  {
    return pspRefNo;
  }
  
  private final String component3()
  {
    return bankAccountId;
  }
  
  public final i component1()
  {
    return data;
  }
  
  public final ConfirmBalanceCheckRequest copy(i parami, String paramString1, String paramString2)
  {
    ConfirmBalanceCheckRequest localConfirmBalanceCheckRequest = new com/truecaller/truepay/app/ui/balancecheck/model/ConfirmBalanceCheckRequest;
    localConfirmBalanceCheckRequest.<init>(parami, paramString1, paramString2);
    return localConfirmBalanceCheckRequest;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof ConfirmBalanceCheckRequest;
      if (bool1)
      {
        paramObject = (ConfirmBalanceCheckRequest)paramObject;
        Object localObject1 = data;
        Object localObject2 = data;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = pspRefNo;
          localObject2 = pspRefNo;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = bankAccountId;
            paramObject = bankAccountId;
            boolean bool2 = k.a(localObject1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final i getData()
  {
    return data;
  }
  
  public final int hashCode()
  {
    i locali = data;
    int i = 0;
    if (locali != null)
    {
      j = locali.hashCode();
    }
    else
    {
      j = 0;
      locali = null;
    }
    j *= 31;
    String str = pspRefNo;
    int k;
    if (str != null)
    {
      k = str.hashCode();
    }
    else
    {
      k = 0;
      str = null;
    }
    int j = (j + k) * 31;
    str = bankAccountId;
    if (str != null) {
      i = str.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ConfirmBalanceCheckRequest(data=");
    Object localObject = data;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", pspRefNo=");
    localObject = pspRefNo;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", bankAccountId=");
    localObject = bankAccountId;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.balancecheck.model.ConfirmBalanceCheckRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */