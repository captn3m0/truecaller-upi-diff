package com.truecaller.truepay.app.ui.balancecheck.c.a;

import android.graphics.drawable.Drawable;

public abstract interface a$b
{
  public static final a.b.a d = a.b.a.a;
  
  public abstract com.truecaller.truepay.app.ui.dashboard.b.a a();
  
  public abstract void a(long paramLong);
  
  public abstract void a(Drawable paramDrawable);
  
  public abstract void a(com.truecaller.truepay.data.api.model.a parama);
  
  public abstract void a(String paramString);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b();
  
  public abstract void b(String paramString);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c(String paramString);
  
  public abstract void d(String paramString);
  
  public abstract void e(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.balancecheck.c.a.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */