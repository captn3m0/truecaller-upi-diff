package com.truecaller.truepay.app.ui.balancecheck.c;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import c.f;
import c.g.b.u;
import c.g.b.w;
import c.l.g;
import com.truecaller.truepay.R.id;

public final class b
{
  final f b;
  private final f c;
  private final f d;
  private final b.a e;
  
  static
  {
    g[] arrayOfg = new g[3];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(b.class);
    ((u)localObject).<init>(localb, "textError", "getTextError()Landroid/widget/TextView;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(b.class);
    ((u)localObject).<init>(localb, "buttonSetupUpiPin", "getButtonSetupUpiPin()Landroid/widget/Button;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(b.class);
    ((u)localObject).<init>(localb, "buttonRetry", "getButtonRetry()Landroid/widget/Button;");
    localObject = (g)w.a((c.g.b.t)localObject);
    arrayOfg[2] = localObject;
    a = arrayOfg;
  }
  
  public b(View paramView, b.a parama)
  {
    e = parama;
    int i = R.id.textError;
    parama = com.truecaller.utils.extensions.t.a(paramView, i);
    b = parama;
    i = R.id.buttonSetUpiPin;
    parama = com.truecaller.utils.extensions.t.a(paramView, i);
    c = parama;
    i = R.id.buttonRetry;
    paramView = com.truecaller.utils.extensions.t.a(paramView, i);
    d = paramView;
    paramView = (Button)c.b();
    parama = new com/truecaller/truepay/app/ui/balancecheck/c/b$1;
    parama.<init>(this);
    parama = (View.OnClickListener)parama;
    paramView.setOnClickListener(parama);
    paramView = (Button)d.b();
    parama = new com/truecaller/truepay/app/ui/balancecheck/c/b$2;
    parama.<init>(this);
    parama = (View.OnClickListener)parama;
    paramView.setOnClickListener(parama);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.balancecheck.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */