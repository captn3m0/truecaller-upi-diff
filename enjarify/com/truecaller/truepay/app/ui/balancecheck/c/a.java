package com.truecaller.truepay.app.ui.balancecheck.c;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.o;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import c.f;
import c.g.b.k;
import c.u;
import com.truecaller.log.d;
import com.truecaller.truepay.R.id;
import com.truecaller.truepay.R.layout;
import com.truecaller.truepay.R.string;
import com.truecaller.truepay.R.style;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.accounts.views.activities.ManageAccountsActivity;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class a
  extends android.support.design.widget.b
  implements com.truecaller.truepay.app.ui.balancecheck.c.a.a.b, b.a
{
  public static final a.a c;
  public b a;
  public com.truecaller.truepay.app.ui.balancecheck.c.a.a.a b;
  private HashMap e;
  
  static
  {
    a.a locala = new com/truecaller/truepay/app/ui/balancecheck/c/a$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      e = ((HashMap)localObject1);
    }
    localObject1 = e;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = e;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final com.truecaller.truepay.app.ui.dashboard.b.a a()
  {
    Object localObject = getArguments();
    if (localObject != null)
    {
      String str = "account";
      localObject = ((Bundle)localObject).getSerializable(str);
    }
    else
    {
      localObject = null;
    }
    if (localObject != null) {
      return (com.truecaller.truepay.app.ui.dashboard.b.a)localObject;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type com.truecaller.truepay.app.ui.dashboard.models.AccountDo");
    throw ((Throwable)localObject);
  }
  
  public final void a(long paramLong)
  {
    Context localContext = getContext();
    if (localContext != null)
    {
      int i = R.id.textLastChecked;
      TextView localTextView = (TextView)a(i);
      k.a(localTextView, "textLastChecked");
      Resources localResources = getResources();
      int j = R.string.pay_bank_account_balance_last_checked;
      Object[] arrayOfObject = new Object[1];
      Object localObject = com.truecaller.truepay.app.utils.j.a(localContext, paramLong);
      arrayOfObject[0] = localObject;
      localObject = (CharSequence)localResources.getString(j, arrayOfObject);
      localTextView.setText((CharSequence)localObject);
      return;
    }
  }
  
  public final void a(Drawable paramDrawable)
  {
    k.b(paramDrawable, "bankImage");
    int i = R.id.bankIcon;
    ((ImageView)a(i)).setImageDrawable(paramDrawable);
  }
  
  public final void a(com.truecaller.truepay.data.api.model.a parama)
  {
    k.b(parama, "account");
    Context localContext = getContext();
    if (localContext != null)
    {
      ManageAccountsActivity.a(localContext, "action.page.forgot_upi_pin", parama, "balance_check");
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "title");
    int i = R.id.balanceCheckTitle;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "balanceCheckTitle");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.balanceSuccess;
    View localView = a(i);
    k.a(localView, "balanceSuccess");
    t.a(localView, paramBoolean);
  }
  
  public final void b()
  {
    int i = R.id.textBalance;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "textBalance");
    t.a((View)localTextView, true);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "accNumber");
    int i = R.id.textAccountNumber;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "textAccountNumber");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    int i = R.id.progressBarBalanceCheck;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    k.a(localProgressBar, "progressBarBalanceCheck");
    t.a((View)localProgressBar, paramBoolean);
  }
  
  public final void c()
  {
    com.truecaller.truepay.app.ui.balancecheck.c.a.a.a locala = b;
    if (locala == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    Object localObject = a();
    locala.a((com.truecaller.truepay.app.ui.dashboard.b.a)localObject);
  }
  
  public final void c(String paramString)
  {
    int i = R.id.textAccountName;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "textAccountName");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void d()
  {
    com.truecaller.truepay.app.ui.balancecheck.c.a.a.a locala = b;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.a();
    dismiss();
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "message");
    int i = R.id.balanceError;
    Object localObject = a(i);
    String str = "balanceError";
    k.a(localObject, str);
    boolean bool = true;
    t.a((View)localObject, bool);
    localObject = a;
    if (localObject == null)
    {
      str = "errorView";
      k.a(str);
    }
    k.b(paramString, "message");
    localObject = (TextView)b.b();
    k.a(localObject, "textError");
    paramString = (CharSequence)paramString;
    ((TextView)localObject).setText(paramString);
  }
  
  public final void e(String paramString)
  {
    k.b(paramString, "balanceText");
    int i = R.id.textBalance;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "textBalance");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final int getTheme()
  {
    return R.style.BottomSheetDialogTheme;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.truepay.app.ui.balancecheck.a.b.a();
    com.truecaller.truepay.app.a.a.a locala = Truepay.getApplicationComponent();
    paramBundle.a(locala).a().a(this);
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new android/support/design/widget/a;
    Context localContext = requireContext();
    int i = getTheme();
    paramBundle.<init>(localContext, i);
    return (Dialog)paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i = R.layout.fragment_balance_check_bottom_sheet;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public final void onDismiss(DialogInterface paramDialogInterface)
  {
    super.onDismiss(paramDialogInterface);
    paramDialogInterface = b;
    if (paramDialogInterface == null)
    {
      String str = "presenter";
      k.a(str);
    }
    paramDialogInterface.y_();
  }
  
  public final void onResume()
  {
    super.onResume();
    com.truecaller.truepay.app.ui.balancecheck.c.a.a.a locala = b;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.e();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject = "view";
    k.b(paramView, (String)localObject);
    super.onViewCreated(paramView, paramBundle);
    paramBundle = b;
    if (paramBundle == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    paramBundle.a(this);
    paramBundle = b;
    if (paramBundle == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    localObject = a();
    paramBundle.a((com.truecaller.truepay.app.ui.dashboard.b.a)localObject);
    paramBundle = new com/truecaller/truepay/app/ui/balancecheck/c/b;
    int i = R.id.balanceError;
    paramView = paramView.findViewById(i);
    k.a(paramView, "view.findViewById(R.id.balanceError)");
    localObject = this;
    localObject = (b.a)this;
    paramBundle.<init>(paramView, (b.a)localObject);
    a = paramBundle;
    int j = R.id.buttonDone;
    paramView = (Button)a(j);
    paramBundle = new com/truecaller/truepay/app/ui/balancecheck/c/a$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
  }
  
  public final void show(android.support.v4.app.j paramj, String paramString)
  {
    if (paramj == null) {
      return;
    }
    try
    {
      paramj = paramj.a();
      Object localObject = "managerCopy.beginTransaction()";
      k.a(paramj, (String)localObject);
      localObject = this;
      localObject = (Fragment)this;
      paramString = paramj.a((Fragment)localObject, paramString);
      localObject = null;
      paramString.a(null);
      paramj.d();
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      d.a((Throwable)localIllegalStateException);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.ui.balancecheck.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */