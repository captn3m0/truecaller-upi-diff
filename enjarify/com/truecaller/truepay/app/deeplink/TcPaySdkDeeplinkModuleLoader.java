package com.truecaller.truepay.app.deeplink;

import com.airbnb.deeplinkdispatch.DeepLinkEntry;
import com.airbnb.deeplinkdispatch.DeepLinkEntry.Type;
import com.airbnb.deeplinkdispatch.Parser;
import com.truecaller.truepay.app.ui.accounts.views.activities.ManageAccountsActivity;
import com.truecaller.truepay.app.ui.payments.views.activities.PaymentsActivity;
import com.truecaller.truepay.app.ui.registration.views.activities.AccountConnectionActivity;
import com.truecaller.truepay.app.ui.scan.views.activities.ScanAndPayActivity;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class TcPaySdkDeeplinkModuleLoader
  implements Parser
{
  public static final List REGISTRY;
  
  static
  {
    DeepLinkEntry[] arrayOfDeepLinkEntry = new DeepLinkEntry[10];
    DeepLinkEntry localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    DeepLinkEntry.Type localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://utility/{utility_type}/{operator_symbol}/{location_symbol}", localType, PaymentsActivity.class, null);
    arrayOfDeepLinkEntry[0] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://utility/{utility_type}/{operator_symbol}", localType, PaymentsActivity.class, null);
    arrayOfDeepLinkEntry[1] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://add_account", localType, AccountConnectionActivity.class, null);
    arrayOfDeepLinkEntry[2] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://add_beneficiary", localType, TransactionActivity.class, null);
    arrayOfDeepLinkEntry[3] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://beneficiaries", localType, TransactionActivity.class, null);
    arrayOfDeepLinkEntry[4] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://manage_accounts", localType, ManageAccountsActivity.class, null);
    arrayOfDeepLinkEntry[5] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://pay/scan", localType, ScanAndPayActivity.class, null);
    arrayOfDeepLinkEntry[6] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://request", localType, TransactionActivity.class, null);
    arrayOfDeepLinkEntry[7] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://send", localType, TransactionActivity.class, null);
    arrayOfDeepLinkEntry[8] = localDeepLinkEntry;
    localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    localType = DeepLinkEntry.Type.CLASS;
    localDeepLinkEntry.<init>("truecaller://utility/{utility_type}", localType, PaymentsActivity.class, null);
    arrayOfDeepLinkEntry[9] = localDeepLinkEntry;
    REGISTRY = Collections.unmodifiableList(Arrays.asList(arrayOfDeepLinkEntry));
  }
  
  public final DeepLinkEntry parseUri(String paramString)
  {
    Iterator localIterator = REGISTRY.iterator();
    DeepLinkEntry localDeepLinkEntry;
    boolean bool2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localDeepLinkEntry = (DeepLinkEntry)localIterator.next();
      bool2 = localDeepLinkEntry.matches(paramString);
    } while (!bool2);
    return localDeepLinkEntry;
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.deeplink.TcPaySdkDeeplinkModuleLoader
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */