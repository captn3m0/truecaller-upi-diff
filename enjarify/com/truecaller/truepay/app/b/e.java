package com.truecaller.truepay.app.b;

import java.io.IOException;

public final class e
  extends IOException
{
  public final String getMessage()
  {
    return "Internet connection not available";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */