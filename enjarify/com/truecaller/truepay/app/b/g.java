package com.truecaller.truepay.app.b;

import android.content.Context;
import com.truecaller.truepay.R.string;
import java.io.IOException;

public final class g
  extends IOException
{
  private Context a;
  
  g(Context paramContext)
  {
    a = paramContext;
  }
  
  public final String getMessage()
  {
    Context localContext = a;
    int i = R.string.server_error_message;
    return localContext.getString(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */