package com.truecaller.truepay.app.b;

public final class b
  extends Throwable
{
  public final String getMessage()
  {
    return "Checksum received from the server does not match with one generated on device. Possible tampering of response detected.";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.app.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */