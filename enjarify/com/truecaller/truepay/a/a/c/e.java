package com.truecaller.truepay.a.a.c;

import com.truecaller.truepay.app.b.c;
import com.truecaller.truepay.data.f.v;
import io.reactivex.d.b.b;
import io.reactivex.d.e.a.h;

public final class e
{
  private final v a;
  private volatile boolean b;
  
  public e(v paramv)
  {
    a = paramv;
  }
  
  public final io.reactivex.d a()
  {
    boolean bool = b;
    if (bool)
    {
      localObject1 = new com/truecaller/truepay/app/b/c;
      ((c)localObject1).<init>();
      return io.reactivex.d.b((Throwable)localObject1);
    }
    Object localObject1 = a.b();
    Object localObject2 = new com/truecaller/truepay/a/a/c/-$$Lambda$e$LD0qgho17VMchzq9Rfx_88_S3tY;
    ((-..Lambda.e.LD0qgho17VMchzq9Rfx_88_S3tY)localObject2).<init>(this);
    Object localObject3 = io.reactivex.d.b.a.g;
    Object localObject4 = io.reactivex.d.b.a.c;
    b.a(localObject2, "onSubscribe is null");
    b.a(localObject3, "onRequest is null");
    b.a(localObject4, "onCancel is null");
    Object localObject5 = new io/reactivex/d/e/a/h;
    ((h)localObject5).<init>((io.reactivex.d)localObject1, (io.reactivex.c.d)localObject2, (io.reactivex.c.f)localObject3, (io.reactivex.c.a)localObject4);
    localObject1 = io.reactivex.e.a.a((io.reactivex.d)localObject5);
    localObject2 = new com/truecaller/truepay/a/a/c/-$$Lambda$e$7ZDOSq_n0ZyvK7C6nyjHDmzyi_0;
    ((-..Lambda.e.7ZDOSq_n0ZyvK7C6nyjHDmzyi_0)localObject2).<init>(this);
    localObject3 = io.reactivex.d.b.a.a();
    localObject4 = io.reactivex.d.b.a.c;
    localObject1 = ((io.reactivex.d)localObject1).a((io.reactivex.c.d)localObject3, (io.reactivex.c.d)localObject2, (io.reactivex.c.a)localObject4, (io.reactivex.c.a)localObject4);
    localObject2 = new com/truecaller/truepay/a/a/c/-$$Lambda$e$DqMVhv-zJHtyOSddBsFMya0cj5U;
    ((-..Lambda.e.DqMVhv-zJHtyOSddBsFMya0cj5U)localObject2).<init>(this);
    localObject3 = io.reactivex.d.b.a.a();
    localObject4 = io.reactivex.d.b.a.a();
    localObject5 = io.reactivex.d.b.a.c;
    localObject1 = ((io.reactivex.d)localObject1).a((io.reactivex.c.d)localObject3, (io.reactivex.c.d)localObject4, (io.reactivex.c.a)localObject2, (io.reactivex.c.a)localObject5);
    localObject2 = new com/truecaller/truepay/a/a/c/-$$Lambda$e$HdDy4RYojB4Bj-qVKLVw4oqLbcc;
    ((-..Lambda.e.HdDy4RYojB4Bj-qVKLVw4oqLbcc)localObject2).<init>(this);
    b.a(localObject2, "onFinally is null");
    localObject3 = new io/reactivex/d/e/a/f;
    ((io.reactivex.d.e.a.f)localObject3).<init>((io.reactivex.d)localObject1, (io.reactivex.c.a)localObject2);
    return io.reactivex.e.a.a((io.reactivex.d)localObject3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.a.a.c.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */