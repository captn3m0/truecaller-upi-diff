package com.truecaller.truepay.a.a.e;

import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d
{
  private final Provider a;
  
  private j(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static j a(Provider paramProvider)
  {
    j localj = new com/truecaller/truepay/a/a/e/j;
    localj.<init>(paramProvider);
    return localj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.a.a.e.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */