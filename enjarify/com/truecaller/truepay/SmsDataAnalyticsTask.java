package com.truecaller.truepay;

import android.content.Context;
import android.os.Bundle;
import c.g.b.k;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.background.e;
import com.truecaller.common.background.e.a;
import com.truecaller.truepay.data.f.ag;
import java.util.concurrent.TimeUnit;

public final class SmsDataAnalyticsTask
  extends PersistentBackgroundTask
{
  public ag a;
  public com.truecaller.truepay.data.b.a b;
  
  public final int a()
  {
    return 20003;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    paramBundle = "serviceContext";
    k.b(paramContext, paramBundle);
    paramContext = Truepay.getApplicationComponent();
    if (paramContext != null)
    {
      Truepay.getApplicationComponent().a(this);
      paramContext = b;
      if (paramContext == null)
      {
        paramBundle = "analyticsLogger";
        k.a(paramBundle);
      }
      paramBundle = a;
      if (paramBundle == null)
      {
        String str = "smsDataRepository";
        k.a(str);
      }
      paramBundle = paramBundle.a();
      boolean bool = paramContext.a(paramBundle);
      if (bool) {
        return PersistentBackgroundTask.RunResult.Success;
      }
      return PersistentBackgroundTask.RunResult.FailedSkip;
    }
    return PersistentBackgroundTask.RunResult.FailedRetry;
  }
  
  public final boolean a(Context paramContext)
  {
    String str = "serviceContext";
    k.b(paramContext, str);
    paramContext = Truepay.getApplicationComponent();
    return paramContext != null;
  }
  
  public final e b()
  {
    Object localObject = new com/truecaller/common/background/e$a;
    int i = 1;
    ((e.a)localObject).<init>(i);
    TimeUnit localTimeUnit = TimeUnit.DAYS;
    long l = 1L;
    localObject = ((e.a)localObject).a(l, localTimeUnit);
    localTimeUnit = TimeUnit.HOURS;
    localObject = ((e.a)localObject).b(12, localTimeUnit);
    localTimeUnit = TimeUnit.HOURS;
    localObject = ((e.a)localObject).c(l, localTimeUnit).a(i).a().b();
    k.a(localObject, "TaskConfiguration.Builde…lse)\n            .build()");
    return (e)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.SmsDataAnalyticsTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */