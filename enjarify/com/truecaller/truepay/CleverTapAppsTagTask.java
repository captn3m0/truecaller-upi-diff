package com.truecaller.truepay;

import android.content.Context;
import android.os.Bundle;
import c.g.b.k;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.background.e;
import com.truecaller.common.background.e.a;
import java.util.concurrent.TimeUnit;

public final class CleverTapAppsTagTask
  extends PersistentBackgroundTask
{
  public a a;
  
  public final int a()
  {
    return 20004;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    paramBundle = "serviceContext";
    k.b(paramContext, paramBundle);
    paramContext = Truepay.getApplicationComponent();
    if (paramContext == null) {
      return PersistentBackgroundTask.RunResult.FailedRetry;
    }
    Truepay.getApplicationComponent().a(this);
    paramContext = a;
    if (paramContext == null)
    {
      paramBundle = "cleverTapAppsTagManager";
      k.a(paramBundle);
    }
    boolean bool = paramContext.a();
    if (bool) {
      return PersistentBackgroundTask.RunResult.Success;
    }
    return PersistentBackgroundTask.RunResult.FailedSkip;
  }
  
  public final boolean a(Context paramContext)
  {
    String str = "serviceContext";
    k.b(paramContext, str);
    paramContext = Truepay.getApplicationComponent();
    return paramContext != null;
  }
  
  public final e b()
  {
    Object localObject = new com/truecaller/common/background/e$a;
    int i = 1;
    ((e.a)localObject).<init>(i);
    TimeUnit localTimeUnit = TimeUnit.DAYS;
    localObject = ((e.a)localObject).a(14, localTimeUnit);
    localTimeUnit = TimeUnit.HOURS;
    localObject = ((e.a)localObject).b(12, localTimeUnit);
    localTimeUnit = TimeUnit.HOURS;
    localObject = ((e.a)localObject).c(1L, localTimeUnit).a(i).a().b();
    k.a(localObject, "TaskConfiguration.Builde…lse)\n            .build()");
    return (e)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.truepay.CleverTapAppsTagTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */