package com.truecaller;

import com.truecaller.backup.l;
import com.truecaller.calling.contacts_list.ContactsHolder;
import com.truecaller.calling.contacts_list.aa;
import com.truecaller.calling.contacts_list.ab;
import com.truecaller.calling.contacts_list.ad;
import com.truecaller.calling.contacts_list.b.b;
import com.truecaller.calling.contacts_list.data.f;
import com.truecaller.calling.contacts_list.data.i;
import com.truecaller.calling.contacts_list.j.a;
import com.truecaller.calling.contacts_list.k;
import com.truecaller.calling.contacts_list.l.a;
import com.truecaller.calling.contacts_list.o;
import com.truecaller.calling.contacts_list.p.a;
import com.truecaller.calling.contacts_list.q;
import com.truecaller.calling.contacts_list.v;
import com.truecaller.calling.contacts_list.y;
import com.truecaller.calling.dialer.s;
import com.truecaller.common.account.r;
import com.truecaller.ui.view.VoipTintedImageView;
import com.truecaller.utils.t;
import com.truecaller.voip.ai;
import javax.inject.Provider;

final class be$h
  implements com.truecaller.calling.contacts_list.j
{
  private Provider b;
  private Provider c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  private Provider o;
  private Provider p;
  private Provider q;
  private Provider r;
  private Provider s;
  private Provider t;
  private Provider u;
  private Provider v;
  private Provider w;
  
  private be$h(be parambe)
  {
    parambe = com.truecaller.calling.contacts_list.data.c.a(be.X(a));
    b = parambe;
    parambe = dagger.a.c.a(b);
    c = parambe;
    parambe = c;
    Object localObject = be.l(a);
    parambe = f.a(parambe, (Provider)localObject);
    d = parambe;
    parambe = dagger.a.c.a(d);
    e = parambe;
    parambe = dagger.a.c.a(be.aq(a));
    f = parambe;
    localObject = e;
    Provider localProvider1 = be.w(a);
    Provider localProvider2 = be.G(a);
    Provider localProvider3 = be.ar(a);
    Provider localProvider4 = be.C(a);
    Provider localProvider5 = f;
    Provider localProvider6 = be.S(a);
    Provider localProvider7 = be.t(a);
    parambe = v.a((Provider)localObject, localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6, localProvider7);
    g = parambe;
    parambe = dagger.a.c.a(g);
    h = parambe;
    parambe = dagger.a.c.a(o.a(be.X(a)));
    i = parambe;
    parambe = dagger.a.c.a(com.truecaller.calling.contacts_list.n.a(be.X(a)));
    j = parambe;
    parambe = dagger.a.e.a(this);
    k = parambe;
    parambe = ad.a(k);
    l = parambe;
    parambe = dagger.a.c.a(l);
    m = parambe;
    parambe = dagger.a.c.a(i.a());
    n = parambe;
    parambe = h;
    localObject = f;
    localProvider1 = be.S(a);
    localProvider2 = n;
    parambe = aa.a(parambe, (Provider)localObject, parambe, localProvider1, localProvider2);
    o = parambe;
    parambe = dagger.a.c.a(o);
    p = parambe;
    parambe = dagger.a.c.a(l.a(be.as(a)));
    q = parambe;
    localProvider1 = h;
    localProvider2 = be.w(a);
    localProvider3 = q;
    localProvider4 = be.S(a);
    localProvider5 = be.at(a);
    localObject = localProvider1;
    parambe = com.truecaller.calling.contacts_list.d.a(localProvider1, localProvider1, localProvider2, localProvider3, localProvider4, localProvider5);
    r = parambe;
    parambe = dagger.a.c.a(r);
    s = parambe;
    parambe = com.truecaller.calling.a.c.a(be.X(a));
    t = parambe;
    parambe = dagger.a.c.a(t);
    u = parambe;
    parambe = u;
    localObject = be.B(a);
    parambe = com.truecaller.calling.a.g.a(parambe, (Provider)localObject);
    v = parambe;
    parambe = dagger.a.c.a(v);
    w = parambe;
  }
  
  public final com.truecaller.calling.a.e a()
  {
    return (com.truecaller.calling.a.e)w.get();
  }
  
  public final j.a a(l.a parama)
  {
    dagger.a.g.a(parama);
    be.h.a locala = new com/truecaller/be$h$a;
    locala.<init>(this, parama, (byte)0);
    return locala;
  }
  
  public final void a(k paramk)
  {
    Object localObject1 = (p.a)h.get();
    a = ((p.a)localObject1);
    localObject1 = (s)i.get();
    b = ((s)localObject1);
    localObject1 = (s)j.get();
    c = ((s)localObject1);
    localObject1 = new com/truecaller/calling/contacts_list/q;
    Object localObject2 = h.get();
    Object localObject3 = localObject2;
    localObject3 = (ContactsHolder)localObject2;
    localObject2 = h.get();
    Object localObject4 = localObject2;
    localObject4 = (p.a)localObject2;
    localObject2 = m.get();
    Object localObject5 = localObject2;
    localObject5 = (ab)localObject2;
    localObject2 = dagger.a.g.a(be.L(a).g(), "Cannot return null from a non-@Nullable component method");
    Object localObject6 = localObject2;
    localObject6 = (com.truecaller.utils.n)localObject2;
    localObject2 = p.get();
    Object localObject7 = localObject2;
    localObject7 = (y)localObject2;
    localObject2 = dagger.a.g.a(be.f(a).k(), "Cannot return null from a non-@Nullable component method");
    Object localObject8 = localObject2;
    localObject8 = (r)localObject2;
    localObject2 = s.get();
    Object localObject9 = localObject2;
    localObject9 = (b.b)localObject2;
    localObject2 = be.ap(a).get();
    Object localObject10 = localObject2;
    localObject10 = (com.truecaller.i.a)localObject2;
    localObject2 = localObject1;
    ((q)localObject1).<init>((ContactsHolder)localObject3, (p.a)localObject4, (ab)localObject5, (com.truecaller.utils.n)localObject6, (y)localObject7, (r)localObject8, (b.b)localObject9, (com.truecaller.i.a)localObject10);
    d = ((q)localObject1);
    localObject1 = be.au(a);
    e = ((ai)localObject1);
  }
  
  public final void a(VoipTintedImageView paramVoipTintedImageView)
  {
    Object localObject = be.au(a);
    a = ((ai)localObject);
    localObject = (com.truecaller.voip.d)dagger.a.g.a(be.av(a).b(), "Cannot return null from a non-@Nullable component method");
    b = ((com.truecaller.voip.d)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.be.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */