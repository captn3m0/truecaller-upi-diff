package com.truecaller.d;

import c.g.b.k;
import com.truecaller.common.h.af;
import com.truecaller.common.h.i;
import com.truecaller.common.network.e;
import com.truecaller.common.network.f;
import okhttp3.ab;
import okhttp3.ab.a;
import okhttp3.ad;
import okhttp3.u;
import okhttp3.u.a;
import okhttp3.v;
import okhttp3.v.a;

public final class a
  implements v
{
  private final b a;
  private final af b;
  
  public a(b paramb, af paramaf)
  {
    a = paramb;
    b = paramaf;
  }
  
  public final ad intercept(v.a parama)
  {
    k.b(parama, "chain");
    ab localab = parama.a();
    k.a(localab, "chain.request()");
    String str = localab.a().f();
    k.a(str, "request.url().host()");
    Object localObject = a;
    e locale = f.a(localab);
    i locali = (i)b;
    localObject = ((b)localObject).a(locale, locali);
    if (localObject == null)
    {
      parama = parama.a(localab);
      k.a(parama, "chain.proceed(request)");
      return parama;
    }
    localObject = localab.a().j().b((String)localObject).b();
    k.a(localObject, "request.url().newBuilder…host(frontedHost).build()");
    localab = localab.e().a((u)localObject).b("Host", str).a();
    parama = parama.a(localab);
    k.a(parama, "chain.proceed(it)");
    k.a(parama, "request.newBuilder()\n   …let { chain.proceed(it) }");
    return parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */