package com.truecaller.d;

import c.g.b.k;
import c.l.g;
import c.m.l;
import c.u;
import com.truecaller.common.g.a;
import com.truecaller.common.h.i;
import com.truecaller.common.network.KnownDomain;
import com.truecaller.common.network.e.b;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.featuretoggles.f;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public final class c
  implements b
{
  private final com.truecaller.featuretoggles.e a;
  private final List b;
  private final a c;
  
  public c(com.truecaller.featuretoggles.e parame, List paramList, a parama)
  {
    a = parame;
    b = paramList;
    c = parama;
  }
  
  public final String a(com.truecaller.common.network.e parame, i parami)
  {
    k.b(parami, "crossDomainSupport");
    Object localObject1 = a;
    parame = parami.b(parame);
    int i = 0;
    parami = null;
    int j;
    if (parame != null)
    {
      parame = a;
    }
    else
    {
      j = 0;
      parame = null;
    }
    if (parame != null)
    {
      localObject2 = d.a;
      j = parame.ordinal();
      j = localObject2[j];
      k = 1;
      if (j == k) {}
    }
    else
    {
      parame = D;
      localObject2 = com.truecaller.featuretoggles.e.a;
      m = 89;
      localObject2 = localObject2[m];
      parame = ((f)parame.a((com.truecaller.featuretoggles.e)localObject1, (g)localObject2)).e();
      break label151;
    }
    parame = E;
    Object localObject2 = com.truecaller.featuretoggles.e.a;
    int m = 90;
    localObject2 = localObject2[m];
    parame = ((f)parame.a((com.truecaller.featuretoggles.e)localObject1, (g)localObject2)).e();
    label151:
    parame = (CharSequence)parame;
    localObject1 = new String[] { "," };
    int k = 0;
    localObject2 = null;
    m = 6;
    parame = c.a.m.n((Iterable)c.n.m.c(parame, (String[])localObject1, false, m));
    localObject1 = (c.g.a.b)c.a.a;
    parame = l.c(parame, (c.g.a.b)localObject1);
    localObject1 = (c.g.a.b)c.b.a;
    parame = l.d(l.b(parame, (c.g.a.b)localObject1));
    boolean bool = parame.isEmpty();
    if (bool)
    {
      j = 0;
      parame = null;
    }
    if (parame != null)
    {
      parami = c.j.c.c;
      i = parame.size();
      i = c.j.c.d().b(i);
      return (String)parame.get(i);
    }
    return null;
  }
  
  public final boolean a()
  {
    Object localObject1 = c;
    String str1 = "qaEnableDomainFronting";
    boolean bool1 = ((a)localObject1).a(str1, false);
    boolean bool2 = true;
    if (bool1) {
      return bool2;
    }
    localObject1 = a;
    Object localObject2 = F;
    Object localObject3 = com.truecaller.featuretoggles.e.a;
    int i = 91;
    localObject3 = localObject3[i];
    localObject1 = ((f)((e.a)localObject2).a((com.truecaller.featuretoggles.e)localObject1, (g)localObject3)).e();
    localObject2 = Locale.ENGLISH;
    localObject3 = "Locale.ENGLISH";
    k.a(localObject2, (String)localObject3);
    if (localObject1 != null)
    {
      localObject1 = ((String)localObject1).toLowerCase((Locale)localObject2);
      k.a(localObject1, "(this as java.lang.String).toLowerCase(locale)");
      localObject1 = (CharSequence)localObject1;
      localObject2 = new String[] { "," };
      int k = 6;
      localObject1 = (Collection)c.n.m.c((CharSequence)localObject1, (String[])localObject2, false, k);
      localObject2 = c.a("profileCountryIso");
      localObject2 = (Iterable)c.a.m.a((Collection)b, localObject2);
      localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>();
      localObject3 = (Collection)localObject3;
      localObject2 = ((Iterable)localObject2).iterator();
      Object localObject4;
      Object localObject5;
      for (;;)
      {
        boolean bool3 = ((Iterator)localObject2).hasNext();
        if (!bool3) {
          break;
        }
        localObject4 = ((Iterator)localObject2).next();
        localObject5 = localObject4;
        localObject5 = (CharSequence)localObject4;
        if (localObject5 != null)
        {
          bool5 = c.n.m.a((CharSequence)localObject5);
          if (!bool5)
          {
            bool5 = false;
            localObject5 = null;
            break label262;
          }
        }
        boolean bool5 = true;
        label262:
        if (!bool5) {
          ((Collection)localObject3).add(localObject4);
        }
      }
      localObject3 = (Iterable)localObject3;
      localObject2 = new java/util/ArrayList;
      int j = c.a.m.a((Iterable)localObject3, 10);
      ((ArrayList)localObject2).<init>(j);
      localObject2 = (Collection)localObject2;
      localObject3 = ((Iterable)localObject3).iterator();
      for (;;)
      {
        boolean bool4 = ((Iterator)localObject3).hasNext();
        if (!bool4) {
          break;
        }
        localObject4 = (String)((Iterator)localObject3).next();
        if (localObject4 != null)
        {
          localObject5 = Locale.ENGLISH;
          String str2 = "Locale.ENGLISH";
          k.a(localObject5, str2);
          if (localObject4 != null)
          {
            localObject4 = ((String)localObject4).toLowerCase((Locale)localObject5);
            localObject5 = "(this as java.lang.String).toLowerCase(locale)";
            k.a(localObject4, (String)localObject5);
          }
          else
          {
            localObject1 = new c/u;
            ((u)localObject1).<init>("null cannot be cast to non-null type java.lang.String");
            throw ((Throwable)localObject1);
          }
        }
        else
        {
          bool4 = false;
          localObject4 = null;
        }
        ((Collection)localObject2).add(localObject4);
      }
      localObject2 = (Iterable)localObject2;
      localObject1 = (Iterable)localObject1;
      localObject1 = (Collection)c.a.m.b((Iterable)localObject2, (Iterable)localObject1);
      bool1 = ((Collection)localObject1).isEmpty();
      if (!bool1) {
        return bool2;
      }
      return false;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type java.lang.String");
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */