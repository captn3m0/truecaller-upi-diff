package com.truecaller.util;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.messaging.data.types.Entity;

final class bb$a
  extends u
{
  private final Entity[] b;
  
  private bb$a(e parame, Entity[] paramArrayOfEntity)
  {
    super(parame);
    b = paramArrayOfEntity;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".addToDownloads(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bb.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */