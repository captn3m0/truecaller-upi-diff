package com.truecaller.util;

import android.app.Activity;
import android.content.ComponentName;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.webkit.WebView;
import android.widget.Toast;
import com.truecaller.debug.log.a;
import com.truecaller.log.AssertionUtil;
import com.truecaller.old.data.access.Settings;
import java.util.ArrayList;
import java.util.List;

public final class c
  extends b
{
  private final List a;
  private boolean b;
  private final Handler c;
  private final Runnable d;
  
  public c()
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    a = ((List)localObject);
    localObject = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    ((Handler)localObject).<init>(localLooper);
    c = ((Handler)localObject);
    localObject = new com/truecaller/util/-$$Lambda$c$PskNldMJvJeD-ugFkCEm0s646mg;
    ((-..Lambda.c.PskNldMJvJeD-ugFkCEm0s646mg)localObject).<init>(this);
    d = ((Runnable)localObject);
  }
  
  public final boolean a()
  {
    List localList = a;
    boolean bool = localList.isEmpty();
    return !bool;
  }
  
  public final void onActivityCreated(Activity paramActivity, Bundle paramBundle) {}
  
  public final void onActivityDestroyed(Activity paramActivity) {}
  
  public final void onActivityPaused(Activity paramActivity)
  {
    Object localObject = new Object[2];
    localObject[0] = "Activity paused: ";
    paramActivity = paramActivity.getLocalClassName();
    localObject[1] = paramActivity;
    a.a((Object[])localObject);
    paramActivity = c;
    localObject = d;
    paramActivity.postDelayed((Runnable)localObject, 1000L);
  }
  
  public final void onActivityResumed(Activity paramActivity)
  {
    Object localObject1 = new Object[2];
    localObject1[0] = "Activity resumed: ";
    Object localObject2 = paramActivity.getLocalClassName();
    int i = 1;
    localObject1[i] = localObject2;
    a.a((Object[])localObject1);
    localObject1 = c;
    localObject2 = d;
    ((Handler)localObject1).removeCallbacks((Runnable)localObject2);
    boolean bool = b;
    if (bool) {
      try
      {
        localObject1 = new android/webkit/WebView;
        ((WebView)localObject1).<init>(paramActivity);
        ((WebView)localObject1).resumeTimers();
        return;
      }
      catch (Exception paramActivity)
      {
        AssertionUtil.reportThrowableButNeverCrash(paramActivity);
      }
    }
  }
  
  public final void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
  
  public final void onActivityStarted(Activity paramActivity)
  {
    Object localObject = a;
    ComponentName localComponentName = paramActivity.getComponentName();
    ((List)localObject).add(localComponentName);
    localObject = new Object[2];
    localObject[0] = "Activity started: ";
    paramActivity = paramActivity.getLocalClassName();
    localObject[1] = paramActivity;
    a.a((Object[])localObject);
  }
  
  public final void onActivityStopped(Activity paramActivity)
  {
    Object localObject = a;
    ComponentName localComponentName = paramActivity.getComponentName();
    ((List)localObject).remove(localComponentName);
    localObject = a;
    boolean bool1 = ((List)localObject).isEmpty();
    boolean bool2 = true;
    if (bool1)
    {
      localObject = "onboardingDragToDockShown";
      bool1 = Settings.e((String)localObject);
      if (!bool1)
      {
        localObject = "hasShownWelcome";
        bool1 = Settings.e((String)localObject);
        if (bool1)
        {
          int i = 2131886784;
          Toast.makeText(paramActivity, i, 0).show();
          localObject = "onboardingDragToDockShown";
          Settings.a((String)localObject, bool2);
        }
      }
    }
    localObject = new Object[2];
    localObject[0] = "Activity stopped: ";
    paramActivity = paramActivity.getLocalClassName();
    localObject[bool2] = paramActivity;
    a.a((Object[])localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */