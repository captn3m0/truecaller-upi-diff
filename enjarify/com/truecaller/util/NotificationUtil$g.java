package com.truecaller.util;

import android.content.Context;
import android.widget.Toast;

public final class NotificationUtil$g
  implements NotificationUtil.b
{
  private final Context a;
  private final String b;
  
  public NotificationUtil$g(Context paramContext, String paramString)
  {
    a = paramContext;
    b = paramString;
  }
  
  public final void a()
  {
    Context localContext = a;
    String str = b;
    Toast.makeText(localContext, str, 0).show();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.NotificationUtil.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */