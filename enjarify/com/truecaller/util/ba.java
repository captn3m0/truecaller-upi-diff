package com.truecaller.util;

import android.net.Uri;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.types.Entity;
import java.util.Collection;
import java.util.List;

public abstract interface ba
{
  public abstract w a(Uri paramUri);
  
  public abstract w a(Uri paramUri, boolean paramBoolean);
  
  public abstract w a(Uri paramUri, boolean paramBoolean, long paramLong);
  
  public abstract w a(Collection paramCollection, long paramLong);
  
  public abstract w a(List paramList);
  
  public abstract w a(Entity[] paramArrayOfEntity);
  
  public abstract w b(Uri paramUri, boolean paramBoolean);
}

/* Location:
 * Qualified Name:     com.truecaller.util.ba
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */