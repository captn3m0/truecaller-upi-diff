package com.truecaller.util;

import android.content.Context;
import com.truecaller.old.data.entity.Notification;

public final class NotificationUtil$d
  implements NotificationUtil.b
{
  private final Context a;
  private final String b;
  private final boolean c;
  
  public NotificationUtil$d(Context paramContext, Notification paramNotification)
  {
    a = paramContext;
    paramContext = paramNotification.a("u");
    b = paramContext;
    c = true;
  }
  
  public NotificationUtil$d(Context paramContext, Notification paramNotification, byte paramByte)
  {
    a = paramContext;
    paramContext = paramNotification.a("u");
    b = paramContext;
    c = false;
  }
  
  public final void a()
  {
    Context localContext = a;
    String str = b;
    boolean bool = c;
    dh.a(localContext, str, bool);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.NotificationUtil.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */