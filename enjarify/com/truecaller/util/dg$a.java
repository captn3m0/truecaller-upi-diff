package com.truecaller.util;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;
import c.g.b.k;
import com.truecaller.utils.extensions.b;

public final class dg$a
  implements ViewTreeObserver.OnGlobalLayoutListener
{
  dg$a(ImageView paramImageView, int paramInt, View paramView) {}
  
  public final void onGlobalLayout()
  {
    boolean bool = b;
    Object localObject1 = null;
    int i;
    View localView;
    int j;
    if (bool)
    {
      localObject2 = a.getResources();
      i = c;
      localObject2 = BitmapFactory.decodeResource((Resources)localObject2, i);
      k.a(localObject2, "BitmapFactory.decodeResource(resources, resource)");
      localView = d;
      i = localView.getWidth();
      j = 2;
      localObject2 = b.a((Bitmap)localObject2, i, 0, j);
    }
    else
    {
      localObject2 = a.getResources();
      i = c;
      localObject2 = BitmapFactory.decodeResource((Resources)localObject2, i);
      k.a(localObject2, "BitmapFactory.decodeResource(resources, resource)");
      localView = d;
      i = localView.getHeight();
      j = 1;
      localObject2 = b.a((Bitmap)localObject2, 0, i, j);
    }
    localObject1 = a;
    if (localObject2 == null)
    {
      localObject2 = ((ImageView)localObject1).getResources();
      i = c;
      localObject2 = BitmapFactory.decodeResource((Resources)localObject2, i);
    }
    ((ImageView)localObject1).setImageBitmap((Bitmap)localObject2);
    Object localObject2 = d.getViewTreeObserver();
    localObject1 = this;
    localObject1 = (ViewTreeObserver.OnGlobalLayoutListener)this;
    ((ViewTreeObserver)localObject2).removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.dg.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */