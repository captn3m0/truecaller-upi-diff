package com.truecaller.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.presence.AvailabilityTrigger;
import com.truecaller.presence.c;

public class CallMonitoringReceiver
  extends BroadcastReceiver
{
  private static volatile String a = TelephonyManager.EXTRA_STATE_IDLE;
  
  public static String a()
  {
    return a;
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    String str = "android.intent.action.PHONE_STATE";
    Object localObject = paramIntent.getAction();
    boolean bool = str.equals(localObject);
    if (!bool) {
      return;
    }
    str = "state";
    paramIntent = paramIntent.getStringExtra(str);
    bool = TextUtils.isEmpty(paramIntent);
    if (!bool)
    {
      str = a;
      bool = str.equals(paramIntent);
      if (!bool)
      {
        str = a;
        localObject = TelephonyManager.EXTRA_STATE_RINGING;
        bool = str.equals(localObject);
        if (bool)
        {
          str = TelephonyManager.EXTRA_STATE_OFFHOOK;
          bool = paramIntent.equals(str);
          if (bool) {}
        }
        else
        {
          new String[1][0] = "Phone state changed, triggering a presence update";
          str = TelephonyManager.EXTRA_STATE_OFFHOOK;
          bool = paramIntent.equals(str);
          if (!bool)
          {
            str = a;
            localObject = TelephonyManager.EXTRA_STATE_OFFHOOK;
            bool = str.equals(localObject);
            if (!bool)
            {
              bool = false;
              str = null;
              break label160;
            }
          }
          bool = true;
          label160:
          paramContext = (c)((bk)paramContext.getApplicationContext()).a().ae().a();
          localObject = AvailabilityTrigger.USER_ACTION;
          paramContext.a((AvailabilityTrigger)localObject, bool);
        }
        a = paramIntent;
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.CallMonitoringReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */