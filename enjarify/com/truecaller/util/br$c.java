package com.truecaller.util;

import android.content.Context;
import com.d.b.z;
import com.truecaller.analytics.e.a;
import com.truecaller.common.network.util.d;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;
import okhttp3.aa;
import okhttp3.ab.a;
import okhttp3.f;
import okhttp3.u;
import okhttp3.v;
import okhttp3.y;
import okhttp3.y.a;

final class br$c
  implements f
{
  final okhttp3.e a;
  private br.b c;
  
  br$c(br parambr, Context paramContext, u paramu)
  {
    parambr = new okhttp3/ab$a;
    parambr.<init>();
    parambr = parambr.a(paramu).a("GET", null).a();
    paramu = d.a().c();
    Object localObject = new com/truecaller/common/network/d/e;
    ((com.truecaller.common.network.d.e)localObject).<init>(2000L);
    paramu = paramu.a((v)localObject);
    localObject = TimeUnit.MILLISECONDS;
    paramu = paramu.b(5000L, (TimeUnit)localObject);
    localObject = z.a;
    paramContext = z.a(paramContext);
    parambr = aa.a(paramu.a(paramContext).d(), parambr, false);
    a = parambr;
  }
  
  final void a(br.b paramb)
  {
    Object localObject = c;
    if (localObject != null) {
      return;
    }
    c = paramb;
    paramb = br.a(b);
    localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>("SaveContactPhoto");
    String str = c.toString();
    localObject = ((e.a)localObject).a("Result", str).a();
    paramb.a((com.truecaller.analytics.e)localObject);
  }
  
  public final void a(okhttp3.e parame, IOException paramIOException)
  {
    boolean bool = parame.d();
    if (bool) {
      return;
    }
    parame = "Failed to load bitmap";
    new String[1][0] = parame;
    bool = paramIOException instanceof SocketTimeoutException;
    if (!bool)
    {
      bool = paramIOException instanceof com.truecaller.common.network.util.b;
      if (!bool)
      {
        parame = br.b.c;
        a(parame);
        break label64;
      }
    }
    parame = br.b.d;
    a(parame);
    label64:
    br.a(b, null);
  }
  
  /* Error */
  public final void a(okhttp3.e parame, okhttp3.ad paramad)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokeinterface 128 1 0
    //   6: istore_3
    //   7: iload_3
    //   8: ifeq +4 -> 12
    //   11: return
    //   12: iconst_0
    //   13: istore_3
    //   14: aconst_null
    //   15: astore_1
    //   16: aload_2
    //   17: invokevirtual 149	okhttp3/ad:c	()Z
    //   20: istore 4
    //   22: iload 4
    //   24: ifne +21 -> 45
    //   27: getstatic 137	com/truecaller/util/br$b:c	Lcom/truecaller/util/br$b;
    //   30: astore_2
    //   31: aload_0
    //   32: aload_2
    //   33: invokevirtual 140	com/truecaller/util/br$c:a	(Lcom/truecaller/util/br$b;)V
    //   36: aload_0
    //   37: getfield 14	com/truecaller/util/br$c:b	Lcom/truecaller/util/br;
    //   40: aconst_null
    //   41: invokestatic 145	com/truecaller/util/br:a	(Lcom/truecaller/util/br;[B)V
    //   44: return
    //   45: ldc -105
    //   47: astore 5
    //   49: ldc -103
    //   51: astore 6
    //   53: aload_2
    //   54: aload 5
    //   56: aload 6
    //   58: invokevirtual 156	okhttp3/ad:a	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   61: astore 5
    //   63: aload 5
    //   65: ifnull +105 -> 170
    //   68: ldc -98
    //   70: astore 6
    //   72: aload 5
    //   74: aload 6
    //   76: invokevirtual 162	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   79: istore 4
    //   81: iload 4
    //   83: ifne +6 -> 89
    //   86: goto +84 -> 170
    //   89: aload_2
    //   90: getfield 166	okhttp3/ad:g	Lokhttp3/ae;
    //   93: astore 5
    //   95: aload 5
    //   97: ifnonnull +21 -> 118
    //   100: getstatic 137	com/truecaller/util/br$b:c	Lcom/truecaller/util/br$b;
    //   103: astore_2
    //   104: aload_0
    //   105: aload_2
    //   106: invokevirtual 140	com/truecaller/util/br$c:a	(Lcom/truecaller/util/br$b;)V
    //   109: aload_0
    //   110: getfield 14	com/truecaller/util/br$c:b	Lcom/truecaller/util/br;
    //   113: aconst_null
    //   114: invokestatic 145	com/truecaller/util/br:a	(Lcom/truecaller/util/br;[B)V
    //   117: return
    //   118: aload 5
    //   120: invokevirtual 172	okhttp3/ae:e	()[B
    //   123: astore_1
    //   124: aload_1
    //   125: ifnonnull +6 -> 131
    //   128: goto +69 -> 197
    //   131: aload_2
    //   132: getfield 176	okhttp3/ad:i	Lokhttp3/ad;
    //   135: astore_2
    //   136: aload_2
    //   137: ifnonnull +15 -> 152
    //   140: getstatic 178	com/truecaller/util/br$b:a	Lcom/truecaller/util/br$b;
    //   143: astore_2
    //   144: aload_0
    //   145: aload_2
    //   146: invokevirtual 140	com/truecaller/util/br$c:a	(Lcom/truecaller/util/br$b;)V
    //   149: goto +12 -> 161
    //   152: getstatic 180	com/truecaller/util/br$b:b	Lcom/truecaller/util/br$b;
    //   155: astore_2
    //   156: aload_0
    //   157: aload_2
    //   158: invokevirtual 140	com/truecaller/util/br$c:a	(Lcom/truecaller/util/br$b;)V
    //   161: aload_0
    //   162: getfield 14	com/truecaller/util/br$c:b	Lcom/truecaller/util/br;
    //   165: aload_1
    //   166: invokestatic 145	com/truecaller/util/br:a	(Lcom/truecaller/util/br;[B)V
    //   169: return
    //   170: getstatic 137	com/truecaller/util/br$b:c	Lcom/truecaller/util/br$b;
    //   173: astore_2
    //   174: aload_0
    //   175: aload_2
    //   176: invokevirtual 140	com/truecaller/util/br$c:a	(Lcom/truecaller/util/br$b;)V
    //   179: aload_0
    //   180: getfield 14	com/truecaller/util/br$c:b	Lcom/truecaller/util/br;
    //   183: aconst_null
    //   184: invokestatic 145	com/truecaller/util/br:a	(Lcom/truecaller/util/br;[B)V
    //   187: return
    //   188: astore_2
    //   189: goto +20 -> 209
    //   192: astore_2
    //   193: aload_2
    //   194: invokestatic 186	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   197: getstatic 137	com/truecaller/util/br$b:c	Lcom/truecaller/util/br$b;
    //   200: astore_2
    //   201: aload_0
    //   202: aload_2
    //   203: invokevirtual 140	com/truecaller/util/br$c:a	(Lcom/truecaller/util/br$b;)V
    //   206: goto -45 -> 161
    //   209: getstatic 137	com/truecaller/util/br$b:c	Lcom/truecaller/util/br$b;
    //   212: astore 5
    //   214: aload_0
    //   215: aload 5
    //   217: invokevirtual 140	com/truecaller/util/br$c:a	(Lcom/truecaller/util/br$b;)V
    //   220: aload_0
    //   221: getfield 14	com/truecaller/util/br$c:b	Lcom/truecaller/util/br;
    //   224: aconst_null
    //   225: invokestatic 145	com/truecaller/util/br:a	(Lcom/truecaller/util/br;[B)V
    //   228: aload_2
    //   229: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	230	0	this	c
    //   0	230	1	parame	okhttp3.e
    //   0	230	2	paramad	okhttp3.ad
    //   6	8	3	bool1	boolean
    //   20	62	4	bool2	boolean
    //   47	169	5	localObject	Object
    //   51	24	6	str	String
    // Exception table:
    //   from	to	target	type
    //   16	20	188	finally
    //   56	61	188	finally
    //   74	79	188	finally
    //   89	93	188	finally
    //   118	123	188	finally
    //   193	197	188	finally
    //   16	20	192	java/io/IOException
    //   56	61	192	java/io/IOException
    //   74	79	192	java/io/IOException
    //   89	93	192	java/io/IOException
    //   118	123	192	java/io/IOException
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.br.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */