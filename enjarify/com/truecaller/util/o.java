package com.truecaller.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.provider.CallLog.Calls;
import android.support.v4.content.b;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.f;
import com.truecaller.bp;
import com.truecaller.callhistory.a;

public final class o
  extends ContentObserver
{
  private static boolean a = false;
  private static final Uri b = CallLog.Calls.CONTENT_URI;
  private final f c;
  
  private o(Handler paramHandler, f paramf)
  {
    super(paramHandler);
    c = paramf;
  }
  
  public static void a(Context paramContext)
  {
    boolean bool1 = a;
    if (!bool1)
    {
      Object localObject = "android.permission.READ_CALL_LOG";
      int i = b.a(paramContext, (String)localObject);
      if (i == 0)
      {
        localObject = (TrueApp)paramContext.getApplicationContext();
        o localo = new com/truecaller/util/o;
        Handler localHandler = new android/os/Handler;
        localHandler.<init>();
        localObject = ((TrueApp)localObject).a().ad();
        localo.<init>(localHandler, (f)localObject);
        paramContext = paramContext.getContentResolver();
        localObject = b;
        boolean bool2 = true;
        paramContext.registerContentObserver((Uri)localObject, bool2, localo);
        a = bool2;
        return;
      }
    }
  }
  
  public final void onChange(boolean paramBoolean)
  {
    ((a)c.a()).a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */