package com.truecaller.util;

import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.i.c;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import com.truecaller.utils.d;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

public final class aj
  implements bz
{
  private final al a;
  private final d b;
  private final c c;
  private final h d;
  
  public aj(al paramal, d paramd, c paramc, h paramh)
  {
    a = paramal;
    b = paramd;
    c = paramc;
    d = paramh;
  }
  
  public final ca a(String paramString)
  {
    k.b(paramString, "rawInput");
    Object localObject1 = paramString;
    localObject1 = (CharSequence)paramString;
    int i = ((CharSequence)localObject1).length();
    Object localObject2;
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject2 = null;
    }
    boolean bool3 = false;
    Object localObject3 = null;
    if (i == 0)
    {
      localObject2 = c;
      Object localObject4 = "hasNativeDialerCallerId";
      int j = ((c)localObject2).b((String)localObject4);
      if (j == 0)
      {
        j = 0;
        localObject2 = null;
      }
      else
      {
        localObject2 = a;
        j = ((al)localObject2).l();
      }
      if (j != 0)
      {
        paramString = new com/truecaller/util/ca$a;
        paramString.<init>();
        return (ca)paramString;
      }
      j = 0;
      localObject2 = null;
      int i2;
      for (;;)
      {
        int i1 = ((CharSequence)localObject1).length();
        if (j >= i1) {
          break;
        }
        i2 = ((CharSequence)localObject1).charAt(j);
        int i3 = 35;
        if (i2 != i3)
        {
          i3 = 42;
          if (i2 != i3)
          {
            i2 = 1;
            break label169;
          }
        }
        i2 = 0;
        localObject4 = null;
        label169:
        if (i2 != 0)
        {
          bool4 = false;
          localObject1 = null;
          break label192;
        }
        int k;
        j += 1;
      }
      boolean bool4 = true;
      label192:
      if (!bool4)
      {
        localObject1 = "*#*#";
        bool4 = m.b(paramString, (String)localObject1, false);
        localObject2 = "#*#*";
        boolean bool1 = m.c(paramString, (String)localObject2, false);
        bool4 &= bool1;
        if (bool4)
        {
          int i4 = paramString.length();
          int m = 4;
          i4 -= m;
          if (paramString != null)
          {
            paramString = paramString.substring(m, i4);
            k.a(paramString, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            localObject1 = new com/truecaller/util/ca$d;
            ((ca.d)localObject1).<init>(paramString);
            return (ca)localObject1;
          }
          paramString = new c/u;
          paramString.<init>("null cannot be cast to non-null type java.lang.String");
          throw paramString;
        }
        localObject1 = "*";
        boolean bool5 = m.b(paramString, (String)localObject1, false);
        boolean bool2 = m.b(paramString, "#", false);
        bool5 |= bool2;
        localObject2 = "#";
        bool2 = m.c(paramString, (String)localObject2, false);
        bool5 &= bool2;
        if (bool5)
        {
          int i5 = paramString.hashCode();
          int n = 39878404;
          if (i5 != n)
          {
            n = 39878435;
            if (i5 == n)
            {
              localObject1 = "*#07#";
              boolean bool6 = paramString.equals(localObject1);
              if (bool6)
              {
                paramString = b;
                int i6 = paramString.h();
                i5 = 21;
                if (i6 >= i5)
                {
                  paramString = new com/truecaller/util/ca$c;
                  paramString.<init>();
                  return (ca)paramString;
                }
                return null;
              }
            }
          }
          else
          {
            localObject1 = "*#06#";
            boolean bool7 = paramString.equals(localObject1);
            if (bool7)
            {
              paramString = b;
              bool7 = paramString.b();
              if (bool7) {
                paramString = "MEID";
              } else {
                paramString = "IMEI";
              }
              localObject1 = d.h();
              k.a(localObject1, "multiSimManager.allSimInfos");
              localObject1 = (Iterable)localObject1;
              localObject2 = new java/util/ArrayList;
              ((ArrayList)localObject2).<init>();
              localObject2 = (Collection)localObject2;
              localObject1 = ((Iterable)localObject1).iterator();
              for (;;)
              {
                bool3 = ((Iterator)localObject1).hasNext();
                if (!bool3) {
                  break;
                }
                localObject3 = nextg;
                if (localObject3 != null) {
                  ((Collection)localObject2).add(localObject3);
                }
              }
              localObject2 = (Iterable)localObject2;
              localObject1 = new java/util/ArrayList;
              ((ArrayList)localObject1).<init>();
              localObject1 = (Collection)localObject1;
              localObject2 = ((Iterable)localObject2).iterator();
              for (;;)
              {
                bool3 = ((Iterator)localObject2).hasNext();
                if (!bool3) {
                  break;
                }
                localObject3 = ((Iterator)localObject2).next();
                localObject4 = localObject3;
                localObject4 = (CharSequence)localObject3;
                i2 = ((CharSequence)localObject4).length();
                if (i2 > 0)
                {
                  i2 = 1;
                }
                else
                {
                  i2 = 0;
                  localObject4 = null;
                }
                if (i2 != 0) {
                  ((Collection)localObject1).add(localObject3);
                }
              }
              localObject1 = (Collection)localObject1;
              localObject2 = new String[0];
              localObject1 = ((Collection)localObject1).toArray((Object[])localObject2);
              if (localObject1 != null)
              {
                localObject1 = (String[])localObject1;
                n = localObject1.length;
                localObject1 = (String[])Arrays.copyOf((Object[])localObject1, n);
                localObject2 = new com/truecaller/util/ca$b;
                ((ca.b)localObject2).<init>(paramString, (String[])localObject1);
                return (ca)localObject2;
              }
              paramString = new c/u;
              paramString.<init>("null cannot be cast to non-null type kotlin.Array<T>");
              throw paramString;
            }
          }
          return null;
        }
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.aj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */