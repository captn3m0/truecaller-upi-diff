package com.truecaller.util;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.text.TextUtils;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.data.access.c;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.d;
import com.truecaller.old.data.access.Settings;

public class w$a
  extends com.truecaller.old.a.a
{
  private final Contact a;
  private final Uri c;
  
  public w$a(Contact paramContact, Uri paramUri)
  {
    a = paramContact;
    c = paramUri;
  }
  
  private static long a(Context paramContext, Uri paramUri)
  {
    l1 = 0L;
    if (paramUri != null) {
      try
      {
        ContentResolver localContentResolver = paramContext.getContentResolver();
        paramUri = ContactsContract.Contacts.lookupContact(localContentResolver, paramUri);
        if (paramUri != null)
        {
          localContentResolver = paramContext.getContentResolver();
          Uri localUri = ContactsContract.RawContacts.CONTENT_URI;
          paramContext = "_id";
          String[] arrayOfString = { paramContext };
          paramContext = new java/lang/StringBuilder;
          String str = "contact_id=";
          paramContext.<init>(str);
          long l2 = ContentUris.parseId(paramUri);
          paramContext.append(l2);
          str = paramContext.toString();
          paramContext = localContentResolver.query(localUri, arrayOfString, str, null, null);
          if (paramContext != null) {
            try
            {
              boolean bool = paramContext.moveToNext();
              if (bool)
              {
                bool = false;
                paramUri = null;
                l1 = paramContext.getLong(0);
              }
            }
            finally
            {
              paramContext.close();
            }
          }
        }
        return l1;
      }
      catch (SQLiteException paramContext)
      {
        d.a(paramContext);
      }
    }
  }
  
  public void a(Object paramObject) {}
  
  protected Object doInBackground(Object... paramVarArgs)
  {
    paramVarArgs = TrueApp.x();
    try
    {
      Object localObject1 = paramVarArgs.getContentResolver();
      Object localObject2 = c;
      localObject1 = ContactsContract.Contacts.lookupContact((ContentResolver)localObject1, (Uri)localObject2);
      if (localObject1 == null) {
        return null;
      }
      long l1 = ContentUris.parseId((Uri)localObject1);
      localObject1 = c;
      long l2 = a(paramVarArgs, (Uri)localObject1);
      Object localObject3 = TrueApp.y();
      localObject3 = ((TrueApp)localObject3).a();
      localObject3 = ((bp)localObject3).bG();
      Object localObject4 = a;
      boolean bool1 = ((com.truecaller.premium.b.a)localObject3).a((Contact)localObject4, false);
      if (!bool1)
      {
        bool1 = t.d(paramVarArgs, l1);
        if (!bool1)
        {
          localObject3 = a;
          t.a(paramVarArgs, l2, (Contact)localObject3);
        }
      }
      localObject3 = a;
      localObject3 = ((Contact)localObject3).v();
      bool1 = TextUtils.isEmpty((CharSequence)localObject3);
      int i = 1;
      Object localObject5;
      if (!bool1)
      {
        bool1 = t.a(paramVarArgs, l1);
        if (!bool1)
        {
          localObject3 = paramVarArgs.getContentResolver();
          localObject5 = a;
          localObject5 = ((Contact)localObject5).v();
          boolean bool2 = Settings.d();
          if (!bool2) {
            bool2 = true;
          } else {
            bool2 = false;
          }
          localObject5 = ap.a(paramVarArgs, (String)localObject5, bool2);
          t.a((ContentResolver)localObject3, (Bitmap)localObject5, l2);
        }
      }
      localObject1 = TrueApp.y();
      localObject1 = ((TrueApp)localObject1).a();
      localObject2 = ((bp)localObject1).bb();
      localObject1 = c;
      localObject1 = ((bn)localObject2).a((Uri)localObject1);
      if (localObject1 == null)
      {
        long l3 = 0L;
        bool1 = l1 < l3;
        if (bool1)
        {
          boolean bool3 = l1 < l3;
          if (bool3)
          {
            bool3 = ((bn)localObject2).b();
            if (bool3)
            {
              localObject3 = ContactsContract.Data.CONTENT_URI;
              localObject1 = "contact_id=?";
              localObject5 = new String[i];
              localObject4 = String.valueOf(l1);
              localObject5[0] = localObject4;
              localObject4 = localObject1;
              localObject1 = ((bn)localObject2).a((Uri)localObject3, l1, (String)localObject1, (String[])localObject5);
              break label335;
            }
          }
          bool3 = false;
          localObject1 = null;
        }
      }
      label335:
      if (localObject1 != null)
      {
        localObject2 = new com/truecaller/data/access/c;
        ((c)localObject2).<init>(paramVarArgs);
        return ((c)localObject2).a((Uri)localObject1);
      }
    }
    catch (SQLiteException paramVarArgs)
    {
      d.a(paramVarArgs);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.w.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */