package com.truecaller.util;

import android.content.Context;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.common.a;
import com.truecaller.common.h.g;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class ai
{
  public static final ai a;
  
  static
  {
    ai localai = new com/truecaller/util/ai;
    localai.<init>();
    a = localai;
  }
  
  public static final ao a(Context paramContext)
  {
    k.b(paramContext, "context");
    ag localag = (ag)bg.a;
    f localf = g.a(paramContext).r();
    Object localObject = new com/truecaller/util/ai$c;
    ((ai.c)localObject).<init>(paramContext, null);
    localObject = (m)localObject;
    return e.a(localag, localf, (m)localObject, 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ai
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */