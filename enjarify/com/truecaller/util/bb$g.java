package com.truecaller.util;

import android.net.Uri;
import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class bb$g
  extends u
{
  private final Uri b;
  private final boolean c;
  private final long d;
  
  private bb$g(e parame, Uri paramUri, boolean paramBoolean, long paramLong)
  {
    super(parame);
    b = paramUri;
    c = paramBoolean;
    d = paramLong;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".videoEntityFromUri(");
    Object localObject = a(b, 1);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = Boolean.valueOf(c);
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Long.valueOf(d), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bb.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */