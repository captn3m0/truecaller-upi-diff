package com.truecaller.util;

import com.google.gson.l;
import com.google.gson.n;
import com.google.gson.o;

public final class ay
{
  public static String a(String paramString, o paramo)
  {
    paramString = paramo.b(paramString);
    if (paramString != null)
    {
      boolean bool = paramString instanceof n;
      if (!bool) {
        return paramString.c();
      }
    }
    return "";
  }
  
  public static int b(String paramString, o paramo)
  {
    paramString = paramo.b(paramString);
    if (paramString != null)
    {
      boolean bool = paramString instanceof n;
      if (!bool) {
        return paramString.g();
      }
    }
    return 0;
  }
  
  public static long c(String paramString, o paramo)
  {
    paramString = paramo.b(paramString);
    if (paramString != null)
    {
      boolean bool = paramString instanceof n;
      if (!bool) {
        return paramString.f();
      }
    }
    return 0L;
  }
  
  public static boolean d(String paramString, o paramo)
  {
    paramString = paramo.b(paramString);
    if (paramString != null)
    {
      boolean bool = paramString instanceof n;
      if (!bool) {
        return paramString.h();
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ay
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */