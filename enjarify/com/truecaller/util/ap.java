package com.truecaller.util;

import android.content.Context;
import android.graphics.Bitmap;
import com.d.b.ab;
import com.d.b.t;
import com.d.b.w;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.log.f;
import com.truecaller.util.a.b;
import com.truecaller.utils.extensions.o;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public final class ap
{
  private static final Object a;
  private static b b;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    a = localObject;
  }
  
  public static Bitmap a(Context paramContext, String paramString)
  {
    return a(paramContext, paramString, false);
  }
  
  public static Bitmap a(Context paramContext, String paramString, boolean paramBoolean)
  {
    try
    {
      paramContext = w.a(paramContext);
      paramContext = paramContext.a(paramString);
      if (paramBoolean)
      {
        paramString = t.c;
        paramBoolean = false;
        new t[0];
        paramContext.a(paramString);
      }
      return paramContext.d();
    }
    catch (IOException localIOException) {}
    return null;
  }
  
  public static String a(InputStream paramInputStream)
  {
    int i = 16384;
    char[] arrayOfChar = new char[i];
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(i);
    int j = 0;
    Object localObject = null;
    try
    {
      InputStreamReader localInputStreamReader = new java/io/InputStreamReader;
      String str1 = "UTF-8";
      localInputStreamReader.<init>(paramInputStream, str1);
      long l1 = 0L;
      for (;;)
      {
        paramInputStream = null;
        try
        {
          j = localInputStreamReader.read(arrayOfChar, 0, i);
          if (j >= 0)
          {
            long l2 = 4194304L;
            long l3 = j;
            l1 += l3;
            boolean bool = l2 < l1;
            if (!bool)
            {
              localStringBuilder.append(arrayOfChar, 0, j);
            }
            else
            {
              paramInputStream = new java/lang/IllegalStateException;
              String str2 = "Unexpectedly long input stream. Total size should be less than 4194304 bytes.";
              paramInputStream.<init>(str2);
              throw paramInputStream;
            }
          }
          else
          {
            q.a(localInputStreamReader);
          }
        }
        finally
        {
          localObject = localInputStreamReader;
        }
      }
      q.a((Closeable)localObject);
    }
    finally {}
    return localStringBuilder.toString();
  }
  
  public static void a(Context paramContext)
  {
    synchronized (a)
    {
      paramContext = c(paramContext);
      if (paramContext == null) {}
    }
    try
    {
      paramContext.b();
      paramContext = null;
      b = null;
      return;
      paramContext = finally;
      throw paramContext;
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
  }
  
  static void a(Context paramContext, long paramLong)
  {
    c(paramContext).a(paramLong);
  }
  
  public static boolean a(File paramFile)
  {
    boolean bool1 = paramFile.isDirectory();
    int i = 1;
    if (bool1)
    {
      File[] arrayOfFile = paramFile.listFiles();
      if (arrayOfFile != null)
      {
        int j = arrayOfFile.length;
        i = 0;
        int k = 1;
        while (i < j)
        {
          File localFile = arrayOfFile[i];
          boolean bool2 = a(localFile);
          if (!bool2) {
            k = 0;
          }
          i += 1;
        }
        i = k;
      }
    }
    boolean bool3 = paramFile.delete();
    if (!bool3) {
      i = 0;
    }
    return i;
  }
  
  static long b(Context paramContext)
  {
    paramContext = c(paramContext);
    long l1 = paramContext.a();
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (bool)
    {
      bool = false;
      paramContext = null;
      String[] arrayOfString = new String[0];
      AssertionUtil.OnlyInDebug.isTrue(false, arrayOfString);
      l1 = l2;
    }
    return l1;
  }
  
  /* Error */
  public static String b(File paramFile)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: new 125	java/io/FileInputStream
    //   5: astore_2
    //   6: aload_2
    //   7: aload_0
    //   8: invokespecial 128	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   11: aload_2
    //   12: invokestatic 131	com/truecaller/util/ap:a	(Ljava/io/InputStream;)Ljava/lang/String;
    //   15: astore_1
    //   16: goto +19 -> 35
    //   19: astore_0
    //   20: aload_2
    //   21: astore_1
    //   22: goto +4 -> 26
    //   25: astore_0
    //   26: aload_1
    //   27: invokestatic 134	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   30: aload_0
    //   31: athrow
    //   32: pop
    //   33: aconst_null
    //   34: astore_2
    //   35: aload_2
    //   36: invokestatic 134	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   39: aload_1
    //   40: areturn
    //   41: pop
    //   42: goto -7 -> 35
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	45	0	paramFile	File
    //   1	39	1	localObject	Object
    //   5	31	2	localFileInputStream	java.io.FileInputStream
    //   32	1	3	localIOException1	IOException
    //   41	1	4	localIOException2	IOException
    // Exception table:
    //   from	to	target	type
    //   11	15	19	finally
    //   2	5	25	finally
    //   7	11	25	finally
    //   2	5	32	java/io/IOException
    //   7	11	32	java/io/IOException
    //   11	15	41	java/io/IOException
  }
  
  public static void b(Context paramContext, String paramString)
  {
    synchronized (a)
    {
      paramContext = c(paramContext);
      paramString = o.b(paramString);
    }
    try
    {
      paramContext.a(paramString);
      return;
      paramContext = finally;
      throw paramContext;
    }
    finally {}
  }
  
  private static b c(Context paramContext)
  {
    ??? = b;
    if (??? == null) {
      synchronized (a)
      {
        Object localObject2 = b;
        if (localObject2 == null)
        {
          int i = 1;
          try
          {
            localObject3 = new java/io/File;
            Object localObject4 = paramContext.getCacheDir();
            String str = "disklru";
            ((File)localObject3).<init>((File)localObject4, str);
            localObject3 = b.a((File)localObject3);
            b = (b)localObject3;
            localObject3 = new java/lang/Thread;
            localObject4 = new com/truecaller/util/-$$Lambda$ap$wSg8gVNe_In5CGnER_48di-WaNA;
            ((-..Lambda.ap.wSg8gVNe_In5CGnER_48di-WaNA)localObject4).<init>(paramContext);
            paramContext = "tempCacheOptimizer";
            ((Thread)localObject3).<init>((Runnable)localObject4, paramContext);
            ((Thread)localObject3).setPriority(i);
            ((Thread)localObject3).start();
          }
          catch (Exception paramContext)
          {
            localObject2 = new String[i];
            Object localObject3 = null;
            paramContext = f.a(paramContext);
            localObject2[0] = paramContext;
          }
        }
      }
    }
    return b;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ap
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */