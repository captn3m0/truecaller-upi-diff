package com.truecaller.util;

import android.support.v4.f.g;

final class aw$i
  extends g
{
  aw$i(int paramInt)
  {
    super(paramInt);
    String[] arrayOfString = new String[1];
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Memory Image Cache size: ");
    localStringBuilder.append(paramInt);
    localStringBuilder.append(" byte(s).");
    String str = localStringBuilder.toString();
    arrayOfString[0] = str;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.aw.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */