package com.truecaller.util;

import android.content.res.AssetManager;
import android.net.Uri;
import com.google.c.a.c;
import com.google.common.base.Strings;
import com.truecaller.log.AssertionUtil;
import java.io.IOException;
import java.io.InputStream;

public final class cm
  implements c
{
  private final AssetManager a;
  
  public cm(AssetManager paramAssetManager)
  {
    a = paramAssetManager;
  }
  
  public final InputStream a(String paramString)
  {
    try
    {
      boolean bool = Strings.isNullOrEmpty(paramString);
      if (!bool)
      {
        paramString = Uri.parse(paramString);
        paramString = paramString.getLastPathSegment();
        try
        {
          AssetManager localAssetManager = a;
          String str = "libphonenumber/";
          paramString = String.valueOf(paramString);
          paramString = str.concat(paramString);
          paramString = localAssetManager.open(paramString);
          return paramString;
        }
        catch (IOException paramString)
        {
          AssertionUtil.reportThrowableButNeverCrash(paramString);
        }
      }
      return null;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cm
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */