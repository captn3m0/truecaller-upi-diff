package com.truecaller.util;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.Collection;

final class bb$c
  extends u
{
  private final Collection b;
  private final long c;
  
  private bb$c(e parame, Collection paramCollection, long paramLong)
  {
    super(parame);
    b = paramCollection;
    c = paramLong;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".entitiesFromUri(");
    String str = a(b, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(Long.valueOf(c), 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bb.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */