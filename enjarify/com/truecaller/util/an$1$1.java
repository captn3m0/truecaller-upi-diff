package com.truecaller.util;

import android.media.ToneGenerator;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Vibrator;

final class an$1$1
  extends Handler
{
  an$1$1(an.1 param1, Looper paramLooper)
  {
    super(paramLooper);
  }
  
  public final void handleMessage(Message paramMessage)
  {
    int i = what;
    Object localObject;
    int j;
    switch (i)
    {
    default: 
      break;
    case 2: 
      localObject = an.c(a.a);
      if (localObject != null)
      {
        localObject = an.c(a.a);
        j = arg1;
        long l = j;
        ((Vibrator)localObject).vibrate(l);
      }
      break;
    case 1: 
      localObject = an.b(a.a);
      if (localObject != null)
      {
        localObject = an.b(a.a);
        ((ToneGenerator)localObject).stopTone();
      }
      break;
    case 0: 
      localObject = an.b(a.a);
      if (localObject != null)
      {
        localObject = an.b(a.a);
        j = arg1;
        int k = arg2;
        ((ToneGenerator)localObject).startTone(j, k);
      }
      break;
    }
    super.handleMessage(paramMessage);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.an.1.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */