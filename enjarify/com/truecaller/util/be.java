package com.truecaller.util;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.net.Uri;
import c.f.b;
import c.g.b.k;
import c.n.m;
import com.truecaller.utils.extensions.Scheme;
import com.truecaller.utils.extensions.r;
import java.io.Closeable;
import java.io.InputStream;

public final class be
  implements bd
{
  private final Context a;
  
  public be(Context paramContext)
  {
    a = paramContext;
  }
  
  private final String a(Uri paramUri, String paramString)
  {
    MediaExtractor localMediaExtractor = new android/media/MediaExtractor;
    localMediaExtractor.<init>();
    Context localContext = a;
    localMediaExtractor.setDataSource(localContext, paramUri, null);
    int i = localMediaExtractor.getTrackCount();
    int j = 0;
    int k;
    for (;;)
    {
      k = 1;
      if (j >= i) {
        break;
      }
      Object localObject = localMediaExtractor.getTrackFormat(j);
      String str = "mime";
      localObject = ((MediaFormat)localObject).getString(str);
      if (localObject == null) {
        localObject = "";
      }
      int m = paramString.hashCode();
      int i2 = 93166550;
      boolean bool2;
      label276:
      String[] arrayOfString;
      if (m != i2)
      {
        i2 = 112202875;
        if (m == i2)
        {
          str = "video";
          boolean bool1 = paramString.equals(str);
          if (bool1)
          {
            str = "video";
            bool1 = m.b((String)localObject, str, false);
            if (bool1)
            {
              int n = ((String)localObject).hashCode();
              i2 = -1664118616;
              if (n != i2)
              {
                i2 = -1662541442;
                if (n != i2)
                {
                  i2 = 1187890754;
                  if (n != i2)
                  {
                    i2 = 1331836730;
                    if (n != i2) {
                      break label276;
                    }
                    str = "video/avc";
                    bool2 = ((String)localObject).equals(str);
                    if (!bool2) {
                      break label276;
                    }
                  }
                  else
                  {
                    str = "video/mp4v-es";
                    bool2 = ((String)localObject).equals(str);
                    if (!bool2) {
                      break label276;
                    }
                  }
                }
                else
                {
                  str = "video/hevc";
                  bool2 = ((String)localObject).equals(str);
                  if (!bool2) {
                    break label276;
                  }
                }
                return "video/mp4";
              }
              else
              {
                str = "video/3gpp";
                bool2 = ((String)localObject).equals(str);
                if (bool2) {
                  return "video/3gpp";
                }
              }
              arrayOfString = new String[k];
              str = "Unknown track video type ";
              localObject = String.valueOf(localObject);
              localObject = str.concat((String)localObject);
              arrayOfString[0] = localObject;
            }
          }
        }
      }
      else
      {
        str = "audio";
        bool2 = paramString.equals(str);
        if (bool2)
        {
          str = "audio";
          bool2 = m.b((String)localObject, str, false);
          if (bool2)
          {
            int i1 = ((String)localObject).hashCode();
            i2 = -53558318;
            if (i1 == i2)
            {
              str = "audio/mp4a-latm";
              boolean bool3 = ((String)localObject).equals(str);
              if (bool3) {
                return "audio/aac";
              }
            }
            arrayOfString = new String[k];
            str = "Unknown track video type ";
            localObject = String.valueOf(localObject);
            localObject = str.concat((String)localObject);
            arrayOfString[0] = localObject;
          }
        }
      }
      j += 1;
    }
    paramString = new String[k];
    paramUri = String.valueOf(paramUri);
    paramUri = "No suitable media track found for ".concat(paramUri);
    paramString[0] = paramUri;
    return null;
  }
  
  /* Error */
  public final de a(Uri paramUri)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 107
    //   3: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aconst_null
    //   7: astore_2
    //   8: new 109	android/media/MediaMetadataRetriever
    //   11: astore_3
    //   12: aload_3
    //   13: invokespecial 110	android/media/MediaMetadataRetriever:<init>	()V
    //   16: aload_0
    //   17: getfield 22	com/truecaller/util/be:a	Landroid/content/Context;
    //   20: astore 4
    //   22: aload_3
    //   23: aload 4
    //   25: aload_1
    //   26: invokevirtual 113	android/media/MediaMetadataRetriever:setDataSource	(Landroid/content/Context;Landroid/net/Uri;)V
    //   29: ldc 59
    //   31: astore 4
    //   33: aload_0
    //   34: aload_1
    //   35: aload 4
    //   37: invokespecial 116	com/truecaller/util/be:a	(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    //   40: astore_1
    //   41: bipush 9
    //   43: istore 5
    //   45: aload_3
    //   46: iload 5
    //   48: invokevirtual 121	android/media/MediaMetadataRetriever:extractMetadata	(I)Ljava/lang/String;
    //   51: astore 4
    //   53: aload 4
    //   55: ifnull +20 -> 75
    //   58: aload 4
    //   60: invokestatic 127	java/lang/Long:parseLong	(Ljava/lang/String;)J
    //   63: lstore 6
    //   65: lload 6
    //   67: invokestatic 130	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   70: astore 4
    //   72: goto +9 -> 81
    //   75: iconst_0
    //   76: istore 5
    //   78: aconst_null
    //   79: astore 4
    //   81: iconst_m1
    //   82: istore 8
    //   84: aload 4
    //   86: ifnull +49 -> 135
    //   89: aload 4
    //   91: astore 9
    //   93: aload 4
    //   95: checkcast 132	java/lang/Number
    //   98: astore 9
    //   100: aload 9
    //   102: invokevirtual 136	java/lang/Number:longValue	()J
    //   105: pop2
    //   106: getstatic 142	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   109: astore 9
    //   111: aload 4
    //   113: invokevirtual 143	java/lang/Long:longValue	()J
    //   116: lstore 10
    //   118: aload 9
    //   120: lload 10
    //   122: invokevirtual 147	java/util/concurrent/TimeUnit:toSeconds	(J)J
    //   125: lstore 12
    //   127: lload 12
    //   129: l2i
    //   130: istore 5
    //   132: goto +6 -> 138
    //   135: iconst_m1
    //   136: istore 5
    //   138: bipush 24
    //   140: istore 14
    //   142: aload_3
    //   143: iload 14
    //   145: invokevirtual 121	android/media/MediaMetadataRetriever:extractMetadata	(I)Ljava/lang/String;
    //   148: astore 9
    //   150: aload 9
    //   152: ifnull +13 -> 165
    //   155: aload 9
    //   157: invokestatic 154	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   160: istore 14
    //   162: goto +6 -> 168
    //   165: iconst_m1
    //   166: istore 14
    //   168: bipush 90
    //   170: istore 15
    //   172: bipush 18
    //   174: istore 16
    //   176: bipush 19
    //   178: istore 17
    //   180: iload 14
    //   182: iload 15
    //   184: if_icmpeq +67 -> 251
    //   187: sipush 270
    //   190: istore 15
    //   192: iload 14
    //   194: iload 15
    //   196: if_icmpne +6 -> 202
    //   199: goto +52 -> 251
    //   202: aload_3
    //   203: iload 16
    //   205: invokevirtual 121	android/media/MediaMetadataRetriever:extractMetadata	(I)Ljava/lang/String;
    //   208: astore 9
    //   210: aload 9
    //   212: ifnull +13 -> 225
    //   215: aload 9
    //   217: invokestatic 154	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   220: istore 14
    //   222: goto +6 -> 228
    //   225: iconst_m1
    //   226: istore 14
    //   228: aload_3
    //   229: iload 17
    //   231: invokevirtual 121	android/media/MediaMetadataRetriever:extractMetadata	(I)Ljava/lang/String;
    //   234: astore 18
    //   236: aload 18
    //   238: ifnull +59 -> 297
    //   241: aload 18
    //   243: invokestatic 154	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   246: istore 8
    //   248: goto +49 -> 297
    //   251: aload_3
    //   252: iload 17
    //   254: invokevirtual 121	android/media/MediaMetadataRetriever:extractMetadata	(I)Ljava/lang/String;
    //   257: astore 9
    //   259: aload 9
    //   261: ifnull +13 -> 274
    //   264: aload 9
    //   266: invokestatic 154	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   269: istore 14
    //   271: goto +6 -> 277
    //   274: iconst_m1
    //   275: istore 14
    //   277: aload_3
    //   278: iload 16
    //   280: invokevirtual 121	android/media/MediaMetadataRetriever:extractMetadata	(I)Ljava/lang/String;
    //   283: astore 18
    //   285: aload 18
    //   287: ifnull +10 -> 297
    //   290: aload 18
    //   292: invokestatic 154	java/lang/Integer:parseInt	(Ljava/lang/String;)I
    //   295: istore 8
    //   297: new 160	com/truecaller/util/de
    //   300: astore 18
    //   302: aload 18
    //   304: iload 14
    //   306: iload 8
    //   308: iload 5
    //   310: aload_1
    //   311: invokespecial 163	com/truecaller/util/de:<init>	(IIILjava/lang/String;)V
    //   314: aload_3
    //   315: invokevirtual 166	android/media/MediaMetadataRetriever:release	()V
    //   318: aload 18
    //   320: astore_2
    //   321: goto +68 -> 389
    //   324: astore_1
    //   325: goto +26 -> 351
    //   328: astore_1
    //   329: aconst_null
    //   330: astore_3
    //   331: goto +37 -> 368
    //   334: pop
    //   335: aconst_null
    //   336: astore_3
    //   337: aload_3
    //   338: ifnull +51 -> 389
    //   341: aload_3
    //   342: invokevirtual 166	android/media/MediaMetadataRetriever:release	()V
    //   345: goto +44 -> 389
    //   348: astore_1
    //   349: aconst_null
    //   350: astore_3
    //   351: aload_1
    //   352: checkcast 168	java/lang/Throwable
    //   355: astore_1
    //   356: aload_1
    //   357: invokestatic 174	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   360: aload_3
    //   361: ifnull +28 -> 389
    //   364: goto -23 -> 341
    //   367: astore_1
    //   368: aload_3
    //   369: ifnull +7 -> 376
    //   372: aload_3
    //   373: invokevirtual 166	android/media/MediaMetadataRetriever:release	()V
    //   376: aload_1
    //   377: athrow
    //   378: pop
    //   379: aconst_null
    //   380: astore_3
    //   381: aload_3
    //   382: ifnull +7 -> 389
    //   385: goto -44 -> 341
    //   388: pop
    //   389: aload_2
    //   390: areturn
    //   391: pop
    //   392: goto -74 -> 318
    //   395: pop
    //   396: goto -59 -> 337
    //   399: pop
    //   400: goto -24 -> 376
    //   403: pop
    //   404: goto -23 -> 381
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	407	0	this	be
    //   0	407	1	paramUri	Uri
    //   7	383	2	localObject1	Object
    //   11	371	3	localMediaMetadataRetriever	android.media.MediaMetadataRetriever
    //   20	92	4	localObject2	Object
    //   43	266	5	i	int
    //   63	3	6	l1	long
    //   82	225	8	j	int
    //   91	174	9	localObject3	Object
    //   116	5	10	l2	long
    //   125	3	12	l3	long
    //   140	165	14	k	int
    //   170	27	15	m	int
    //   174	105	16	n	int
    //   334	1	16	localIOException1	java.io.IOException
    //   178	75	17	i1	int
    //   378	1	17	localRuntimeException1	RuntimeException
    //   234	85	18	localObject4	Object
    //   388	1	18	localException1	Exception
    //   391	1	19	localException2	Exception
    //   395	1	20	localIOException2	java.io.IOException
    //   399	1	21	localException3	Exception
    //   403	1	22	localRuntimeException2	RuntimeException
    // Exception table:
    //   from	to	target	type
    //   16	20	324	java/io/FileNotFoundException
    //   25	29	324	java/io/FileNotFoundException
    //   35	40	324	java/io/FileNotFoundException
    //   46	51	324	java/io/FileNotFoundException
    //   58	63	324	java/io/FileNotFoundException
    //   65	70	324	java/io/FileNotFoundException
    //   93	98	324	java/io/FileNotFoundException
    //   100	106	324	java/io/FileNotFoundException
    //   106	109	324	java/io/FileNotFoundException
    //   111	116	324	java/io/FileNotFoundException
    //   120	125	324	java/io/FileNotFoundException
    //   143	148	324	java/io/FileNotFoundException
    //   155	160	324	java/io/FileNotFoundException
    //   203	208	324	java/io/FileNotFoundException
    //   215	220	324	java/io/FileNotFoundException
    //   229	234	324	java/io/FileNotFoundException
    //   241	246	324	java/io/FileNotFoundException
    //   252	257	324	java/io/FileNotFoundException
    //   264	269	324	java/io/FileNotFoundException
    //   278	283	324	java/io/FileNotFoundException
    //   290	295	324	java/io/FileNotFoundException
    //   297	300	324	java/io/FileNotFoundException
    //   310	314	324	java/io/FileNotFoundException
    //   8	11	328	finally
    //   12	16	328	finally
    //   8	11	334	java/io/IOException
    //   12	16	334	java/io/IOException
    //   8	11	348	java/io/FileNotFoundException
    //   12	16	348	java/io/FileNotFoundException
    //   16	20	367	finally
    //   25	29	367	finally
    //   35	40	367	finally
    //   46	51	367	finally
    //   58	63	367	finally
    //   65	70	367	finally
    //   93	98	367	finally
    //   100	106	367	finally
    //   106	109	367	finally
    //   111	116	367	finally
    //   120	125	367	finally
    //   143	148	367	finally
    //   155	160	367	finally
    //   203	208	367	finally
    //   215	220	367	finally
    //   229	234	367	finally
    //   241	246	367	finally
    //   252	257	367	finally
    //   264	269	367	finally
    //   278	283	367	finally
    //   290	295	367	finally
    //   297	300	367	finally
    //   310	314	367	finally
    //   351	355	367	finally
    //   356	360	367	finally
    //   8	11	378	java/lang/RuntimeException
    //   12	16	378	java/lang/RuntimeException
    //   341	345	388	java/lang/Exception
    //   314	318	391	java/lang/Exception
    //   16	20	395	java/io/IOException
    //   25	29	395	java/io/IOException
    //   35	40	395	java/io/IOException
    //   46	51	395	java/io/IOException
    //   58	63	395	java/io/IOException
    //   65	70	395	java/io/IOException
    //   93	98	395	java/io/IOException
    //   100	106	395	java/io/IOException
    //   106	109	395	java/io/IOException
    //   111	116	395	java/io/IOException
    //   120	125	395	java/io/IOException
    //   143	148	395	java/io/IOException
    //   155	160	395	java/io/IOException
    //   203	208	395	java/io/IOException
    //   215	220	395	java/io/IOException
    //   229	234	395	java/io/IOException
    //   241	246	395	java/io/IOException
    //   252	257	395	java/io/IOException
    //   264	269	395	java/io/IOException
    //   278	283	395	java/io/IOException
    //   290	295	395	java/io/IOException
    //   297	300	395	java/io/IOException
    //   310	314	395	java/io/IOException
    //   372	376	399	java/lang/Exception
    //   16	20	403	java/lang/RuntimeException
    //   25	29	403	java/lang/RuntimeException
    //   35	40	403	java/lang/RuntimeException
    //   46	51	403	java/lang/RuntimeException
    //   58	63	403	java/lang/RuntimeException
    //   65	70	403	java/lang/RuntimeException
    //   93	98	403	java/lang/RuntimeException
    //   100	106	403	java/lang/RuntimeException
    //   106	109	403	java/lang/RuntimeException
    //   111	116	403	java/lang/RuntimeException
    //   120	125	403	java/lang/RuntimeException
    //   143	148	403	java/lang/RuntimeException
    //   155	160	403	java/lang/RuntimeException
    //   203	208	403	java/lang/RuntimeException
    //   215	220	403	java/lang/RuntimeException
    //   229	234	403	java/lang/RuntimeException
    //   241	246	403	java/lang/RuntimeException
    //   252	257	403	java/lang/RuntimeException
    //   264	269	403	java/lang/RuntimeException
    //   278	283	403	java/lang/RuntimeException
    //   290	295	403	java/lang/RuntimeException
    //   297	300	403	java/lang/RuntimeException
    //   310	314	403	java/lang/RuntimeException
  }
  
  /* Error */
  public final e b(Uri paramUri)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 107
    //   3: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aconst_null
    //   7: astore_2
    //   8: new 109	android/media/MediaMetadataRetriever
    //   11: astore_3
    //   12: aload_3
    //   13: invokespecial 110	android/media/MediaMetadataRetriever:<init>	()V
    //   16: aload_0
    //   17: getfield 22	com/truecaller/util/be:a	Landroid/content/Context;
    //   20: astore 4
    //   22: aload_3
    //   23: aload 4
    //   25: aload_1
    //   26: invokevirtual 113	android/media/MediaMetadataRetriever:setDataSource	(Landroid/content/Context;Landroid/net/Uri;)V
    //   29: ldc 97
    //   31: astore 4
    //   33: aload_0
    //   34: aload_1
    //   35: aload 4
    //   37: invokespecial 116	com/truecaller/util/be:a	(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    //   40: astore_1
    //   41: bipush 9
    //   43: istore 5
    //   45: aload_3
    //   46: iload 5
    //   48: invokevirtual 121	android/media/MediaMetadataRetriever:extractMetadata	(I)Ljava/lang/String;
    //   51: astore 4
    //   53: aload 4
    //   55: ifnull +20 -> 75
    //   58: aload 4
    //   60: invokestatic 127	java/lang/Long:parseLong	(Ljava/lang/String;)J
    //   63: lstore 6
    //   65: lload 6
    //   67: invokestatic 130	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   70: astore 4
    //   72: goto +9 -> 81
    //   75: iconst_0
    //   76: istore 5
    //   78: aconst_null
    //   79: astore 4
    //   81: aload 4
    //   83: ifnull +49 -> 132
    //   86: aload 4
    //   88: astore 8
    //   90: aload 4
    //   92: checkcast 132	java/lang/Number
    //   95: astore 8
    //   97: aload 8
    //   99: invokevirtual 136	java/lang/Number:longValue	()J
    //   102: pop2
    //   103: getstatic 142	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   106: astore 8
    //   108: aload 4
    //   110: invokevirtual 143	java/lang/Long:longValue	()J
    //   113: lstore 9
    //   115: aload 8
    //   117: lload 9
    //   119: invokevirtual 147	java/util/concurrent/TimeUnit:toSeconds	(J)J
    //   122: lstore 6
    //   124: lload 6
    //   126: l2i
    //   127: istore 11
    //   129: goto +6 -> 135
    //   132: iconst_m1
    //   133: istore 11
    //   135: new 184	com/truecaller/util/e
    //   138: astore 4
    //   140: aload 4
    //   142: iload 11
    //   144: aload_1
    //   145: invokespecial 187	com/truecaller/util/e:<init>	(ILjava/lang/String;)V
    //   148: aload_3
    //   149: invokevirtual 166	android/media/MediaMetadataRetriever:release	()V
    //   152: aload 4
    //   154: astore_2
    //   155: goto +68 -> 223
    //   158: astore_1
    //   159: goto +26 -> 185
    //   162: astore_1
    //   163: aconst_null
    //   164: astore_3
    //   165: goto +37 -> 202
    //   168: pop
    //   169: aconst_null
    //   170: astore_3
    //   171: aload_3
    //   172: ifnull +51 -> 223
    //   175: aload_3
    //   176: invokevirtual 166	android/media/MediaMetadataRetriever:release	()V
    //   179: goto +44 -> 223
    //   182: astore_1
    //   183: aconst_null
    //   184: astore_3
    //   185: aload_1
    //   186: checkcast 168	java/lang/Throwable
    //   189: astore_1
    //   190: aload_1
    //   191: invokestatic 174	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   194: aload_3
    //   195: ifnull +28 -> 223
    //   198: goto -23 -> 175
    //   201: astore_1
    //   202: aload_3
    //   203: ifnull +7 -> 210
    //   206: aload_3
    //   207: invokevirtual 166	android/media/MediaMetadataRetriever:release	()V
    //   210: aload_1
    //   211: athrow
    //   212: pop
    //   213: aconst_null
    //   214: astore_3
    //   215: aload_3
    //   216: ifnull +7 -> 223
    //   219: goto -44 -> 175
    //   222: pop
    //   223: aload_2
    //   224: areturn
    //   225: pop
    //   226: goto -74 -> 152
    //   229: pop
    //   230: goto -59 -> 171
    //   233: pop
    //   234: goto -24 -> 210
    //   237: pop
    //   238: goto -23 -> 215
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	241	0	this	be
    //   0	241	1	paramUri	Uri
    //   7	217	2	localObject1	Object
    //   11	205	3	localMediaMetadataRetriever	android.media.MediaMetadataRetriever
    //   20	133	4	localObject2	Object
    //   43	34	5	i	int
    //   63	62	6	l1	long
    //   88	28	8	localObject3	Object
    //   113	5	9	l2	long
    //   168	1	10	localIOException1	java.io.IOException
    //   127	16	11	j	int
    //   212	1	11	localRuntimeException1	RuntimeException
    //   222	1	12	localException1	Exception
    //   225	1	13	localException2	Exception
    //   229	1	14	localIOException2	java.io.IOException
    //   233	1	15	localException3	Exception
    //   237	1	16	localRuntimeException2	RuntimeException
    // Exception table:
    //   from	to	target	type
    //   16	20	158	java/io/FileNotFoundException
    //   25	29	158	java/io/FileNotFoundException
    //   35	40	158	java/io/FileNotFoundException
    //   46	51	158	java/io/FileNotFoundException
    //   58	63	158	java/io/FileNotFoundException
    //   65	70	158	java/io/FileNotFoundException
    //   90	95	158	java/io/FileNotFoundException
    //   97	103	158	java/io/FileNotFoundException
    //   103	106	158	java/io/FileNotFoundException
    //   108	113	158	java/io/FileNotFoundException
    //   117	122	158	java/io/FileNotFoundException
    //   135	138	158	java/io/FileNotFoundException
    //   144	148	158	java/io/FileNotFoundException
    //   8	11	162	finally
    //   12	16	162	finally
    //   8	11	168	java/io/IOException
    //   12	16	168	java/io/IOException
    //   8	11	182	java/io/FileNotFoundException
    //   12	16	182	java/io/FileNotFoundException
    //   16	20	201	finally
    //   25	29	201	finally
    //   35	40	201	finally
    //   46	51	201	finally
    //   58	63	201	finally
    //   65	70	201	finally
    //   90	95	201	finally
    //   97	103	201	finally
    //   103	106	201	finally
    //   108	113	201	finally
    //   117	122	201	finally
    //   135	138	201	finally
    //   144	148	201	finally
    //   185	189	201	finally
    //   190	194	201	finally
    //   8	11	212	java/lang/RuntimeException
    //   12	16	212	java/lang/RuntimeException
    //   175	179	222	java/lang/Exception
    //   148	152	225	java/lang/Exception
    //   16	20	229	java/io/IOException
    //   25	29	229	java/io/IOException
    //   35	40	229	java/io/IOException
    //   46	51	229	java/io/IOException
    //   58	63	229	java/io/IOException
    //   65	70	229	java/io/IOException
    //   90	95	229	java/io/IOException
    //   97	103	229	java/io/IOException
    //   103	106	229	java/io/IOException
    //   108	113	229	java/io/IOException
    //   117	122	229	java/io/IOException
    //   135	138	229	java/io/IOException
    //   144	148	229	java/io/IOException
    //   206	210	233	java/lang/Exception
    //   16	20	237	java/lang/RuntimeException
    //   25	29	237	java/lang/RuntimeException
    //   35	40	237	java/lang/RuntimeException
    //   46	51	237	java/lang/RuntimeException
    //   58	63	237	java/lang/RuntimeException
    //   65	70	237	java/lang/RuntimeException
    //   90	95	237	java/lang/RuntimeException
    //   97	103	237	java/lang/RuntimeException
    //   103	106	237	java/lang/RuntimeException
    //   108	113	237	java/lang/RuntimeException
    //   117	122	237	java/lang/RuntimeException
    //   135	138	237	java/lang/RuntimeException
    //   144	148	237	java/lang/RuntimeException
  }
  
  public final au c(Uri paramUri)
  {
    k.b(paramUri, "uri");
    BitmapFactory.Options localOptions = new android/graphics/BitmapFactory$Options;
    localOptions.<init>();
    inJustDecodeBounds = true;
    int i = 0;
    try
    {
      Object localObject3 = r.a(paramUri);
      if (localObject3 != null)
      {
        localObject4 = bf.a;
        int j = ((Scheme)localObject3).ordinal();
        j = localObject4[j];
        switch (j)
        {
        default: 
          break;
        case 2: 
          localObject3 = a;
          localObject3 = ((Context)localObject3).getContentResolver();
          paramUri = ((ContentResolver)localObject3).openInputStream(paramUri);
          paramUri = (Closeable)paramUri;
          localObject3 = paramUri;
          try
          {
            localObject3 = (InputStream)paramUri;
            BitmapFactory.decodeStream((InputStream)localObject3, null, localOptions);
            b.a(paramUri, null);
          }
          finally
          {
            try
            {
              throw ((Throwable)localObject1);
            }
            finally
            {
              localObject3 = localObject1;
              localObject2 = localObject5;
              b.a(paramUri, (Throwable)localObject3);
            }
          }
        }
        paramUri = paramUri.getPath();
        BitmapFactory.decodeFile(paramUri, (BitmapFactory.Options)localObject2);
        paramUri = new com/truecaller/util/au;
        i = outWidth;
        j = outHeight;
        localObject2 = outMimeType;
        paramUri.<init>(i, j, (String)localObject2);
        return paramUri;
      }
      Object localObject2 = new java/lang/IllegalArgumentException;
      localObject3 = new java/lang/StringBuilder;
      Object localObject4 = "Uri scheme: ";
      ((StringBuilder)localObject3).<init>((String)localObject4);
      ((StringBuilder)localObject3).append(paramUri);
      paramUri = ".scheme is not supported";
      ((StringBuilder)localObject3).append(paramUri);
      paramUri = ((StringBuilder)localObject3).toString();
      ((IllegalArgumentException)localObject2).<init>(paramUri);
      localObject2 = (Throwable)localObject2;
      throw ((Throwable)localObject2);
    }
    catch (RuntimeException localRuntimeException)
    {
      for (;;) {}
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.be
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */