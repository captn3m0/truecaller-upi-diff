package com.truecaller.util;

import android.content.Context;
import android.content.Intent;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.w;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.calling.recorder.h;
import com.truecaller.credit.e;
import com.truecaller.engagementrewards.EngagementRewardActionType;
import com.truecaller.engagementrewards.EngagementRewardState;
import com.truecaller.network.notification.NotificationScope;
import com.truecaller.network.notification.NotificationType;
import com.truecaller.network.notification.c.a;
import com.truecaller.notifications.GenericWebViewNotificationService;
import com.truecaller.old.data.access.Settings;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.premium.PremiumNotificationService;
import com.truecaller.premium.a.d.a;
import com.truecaller.referral.ReferralNotificationService;
import com.truecaller.service.CallMeBackNotificationService;
import com.truecaller.truepay.app.fcm.TruepayFcmManager;
import com.truecaller.ui.SettingsFragment;
import com.truecaller.ui.SettingsFragment.SettingsViewType;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.dialogs.PremiumObtainedDialogActivity;
import com.truecaller.ui.u;
import com.truecaller.voip.ai;
import com.truecaller.whoviewedme.WhoViewedMeNotificationService;
import e.r;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class NotificationUtil
{
  public static NotificationUtil.c a(Context paramContext)
  {
    try
    {
      Object localObject1 = new com/truecaller/old/data/access/f;
      ((com.truecaller.old.data.access.f)localObject1).<init>(paramContext);
      paramContext = new java/util/ArrayList;
      paramContext.<init>();
      Object localObject2 = ((com.truecaller.old.data.access.f)localObject1).g();
      localObject2 = ((Map)localObject2).entrySet();
      localObject2 = ((Set)localObject2).iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject2).hasNext();
        if (!bool) {
          break;
        }
        localObject3 = ((Iterator)localObject2).next();
        localObject3 = (Map.Entry)localObject3;
        Object localObject4 = ((Map.Entry)localObject3).getKey();
        localObject4 = (NotificationScope)localObject4;
        int i = value;
        if (i > 0)
        {
          Object localObject5 = ((Map.Entry)localObject3).getValue();
          long l;
          if (localObject5 != null)
          {
            localObject3 = ((Map.Entry)localObject3).getValue();
            localObject3 = (Long)localObject3;
            l = ((Long)localObject3).longValue();
          }
          else
          {
            l = 0L;
          }
          localObject3 = "language";
          localObject3 = Settings.b((String)localObject3);
          localObject3 = com.truecaller.network.notification.b.a(l, (NotificationScope)localObject4, (String)localObject3);
          localObject3 = ((e.b)localObject3).c();
          localObject3 = b;
          localObject3 = (com.truecaller.network.notification.c)localObject3;
          if (localObject3 != null)
          {
            localObject4 = a;
            if (localObject4 != null)
            {
              localObject3 = a;
              paramContext.addAll((Collection)localObject3);
            }
          }
        }
      }
      paramContext = ((com.truecaller.old.data.access.f)localObject1).a(paramContext);
      localObject2 = Boolean.TRUE;
      ((com.truecaller.old.data.access.f)localObject1).a(paramContext, (Boolean)localObject2);
      localObject2 = ((com.truecaller.old.data.access.f)localObject1).h();
      int j = ((Collection)localObject2).size();
      paramContext = com.truecaller.old.data.access.f.b(paramContext);
      int k = paramContext.size();
      Object localObject3 = new com/truecaller/util/NotificationUtil$c;
      localObject1 = ((com.truecaller.old.data.access.f)localObject1).l();
      k = Math.min(j, k);
      ((NotificationUtil.c)localObject3).<init>((List)localObject1, k);
      return (NotificationUtil.c)localObject3;
    }
    catch (IOException paramContext) {}catch (RuntimeException paramContext) {}
    com.truecaller.log.d.a(paramContext);
    return null;
  }
  
  public static void a(Notification paramNotification, Context paramContext, long paramLong)
  {
    Object localObject1 = ((bk)paramContext.getApplicationContext()).a();
    Object localObject2 = NotificationUtil.1.a;
    Object localObject3 = paramNotification.b();
    int i = ((NotificationType)localObject3).ordinal();
    int j = localObject2[i];
    Object localObject4;
    switch (j)
    {
    case 5: 
    case 6: 
    case 7: 
    case 12: 
    default: 
      break;
    case 30: 
      paramContext = ((bp)localObject1).bO();
      paramNotification = paramNotification.s();
      paramContext.a(paramNotification);
      break;
    case 29: 
      ((bp)localObject1).cc().a(paramNotification, paramLong);
      return;
    case 28: 
      ((bp)localObject1).bK().a(paramNotification);
      return;
    case 27: 
      WhoViewedMeNotificationService.a(paramContext, paramNotification);
      return;
    case 26: 
      localObject4 = a.b;
      if (localObject4 != null)
      {
        localObject4 = ((bp)localObject1).bJ();
        paramNotification = a.b;
        ((com.truecaller.sdk.push.b)localObject4).a(paramContext, paramNotification);
        return;
      }
      break;
    case 24: 
    case 25: 
      PremiumNotificationService.a(paramContext, paramNotification);
      return;
    case 20: 
    case 21: 
    case 22: 
    case 23: 
      paramContext = TrueApp.y();
      boolean bool2 = paramContext.isTcPayEnabled();
      if (bool2)
      {
        paramContext = ((bp)localObject1).aT();
        int k = bvalue;
        paramNotification = paramNotification.s();
        paramContext.handleNotification(k, paramNotification);
        return;
      }
      break;
    case 19: 
      ReferralNotificationService.a(paramContext, paramNotification);
      return;
    case 18: 
      CallMeBackNotificationService.a(paramContext, paramNotification);
      return;
    case 17: 
      ((com.truecaller.config.a)TrueApp.y().a().aZ().a()).b().c();
      return;
    case 16: 
      
    case 14: 
    case 15: 
      GenericWebViewNotificationService.a(paramContext, paramNotification);
      return;
    case 13: 
      ((bp)localObject1).ai().c();
      localObject4 = paramNotification.a("ro");
      boolean bool3 = org.c.a.a.a.b.a((String)localObject4);
      if (bool3) {
        return;
      }
      localObject4 = paramNotification.a("pl");
      String str = paramNotification.a(paramContext);
      localObject2 = paramNotification.b(paramContext);
      localObject3 = "gold";
      boolean bool1 = ((String)localObject3).equalsIgnoreCase((String)localObject4);
      if (bool1)
      {
        paramNotification = paramNotification.a("d");
        j = 2131886866;
        bool1 = true;
        localObject3 = new Object[bool1];
        localObject3[0] = paramNotification;
        localObject2 = paramContext.getString(j, (Object[])localObject3);
      }
      else
      {
        paramNotification = ((bp)localObject1).ak();
        localObject3 = bb;
        bool1 = ((List)localObject3).isEmpty();
        paramNotification.d();
        if (bool1)
        {
          str = paramContext.getString(2131886879);
          paramNotification = ((bp)localObject1).bh();
          boolean bool4 = paramNotification.a();
          int m;
          if (bool4) {
            m = 2131886880;
          } else {
            m = 2131886878;
          }
          localObject2 = paramContext.getString(m);
        }
      }
      paramNotification = ((bp)localObject1).j();
      localObject1 = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
      paramNotification = paramNotification.a((EngagementRewardActionType)localObject1);
      localObject1 = EngagementRewardState.PENDING;
      if (paramNotification == localObject1)
      {
        paramNotification = new java/lang/StringBuilder;
        paramNotification.<init>();
        paramNotification.append((String)localObject2);
        paramNotification.append("\n\n");
        int n = 2131887979;
        localObject1 = paramContext.getString(n);
        paramNotification.append((String)localObject1);
        localObject2 = paramNotification.toString();
      }
      PremiumObtainedDialogActivity.a(paramContext, str, (String)localObject2, (String)localObject4);
      return;
    case 11: 
      
    case 10: 
      
    case 9: 
      
    case 8: 
      
    case 4: 
      
    case 3: 
      
    case 2: 
      
    case 1: 
      
    }
  }
  
  public static boolean a()
  {
    Calendar localCalendar = Calendar.getInstance();
    int i = localCalendar.get(11);
    int j = 9;
    if (i >= j)
    {
      j = 21;
      if (i <= j) {
        return true;
      }
    }
    return false;
  }
  
  public static void b(Context paramContext)
  {
    Object localObject1 = "checkServerNotifications";
    { "NotificationUtil" }[1] = localObject1;
    com.truecaller.old.data.access.f localf = new com/truecaller/old/data/access/f;
    localf.<init>(paramContext);
    Object localObject2 = (com.truecaller.common.b.a)paramContext.getApplicationContext();
    int i = ((com.truecaller.common.b.a)localObject2).p();
    if (i != 0)
    {
      localObject1 = null;
      int j;
      Object localObject4;
      try
      {
        localObject2 = a(paramContext);
        if (localObject2 != null)
        {
          j = b;
          localObject2 = "notificationLast";
          try
          {
            Settings.g((String)localObject2);
          }
          catch (Exception localException1)
          {
            break label93;
          }
        }
        else
        {
          j = 0;
          localObject4 = null;
        }
      }
      catch (Exception localException2)
      {
        j = 0;
        localObject4 = null;
        label93:
        com.truecaller.log.d.a(localException2);
      }
      Object localObject3 = localf.i();
      Object localObject5 = "swUpdateNotificationLast";
      long l = 604800000L;
      boolean bool1 = Settings.b((String)localObject5, l);
      boolean bool2;
      int k;
      if (j > 0)
      {
        localObject5 = "swUpdateNotificationLast";
        Settings.g((String)localObject5);
        if (localObject3 != null) {
          bl.a(paramContext, (Notification)localObject3);
        }
        i = 1;
        if (j == i)
        {
          localObject1 = (Notification)localf.l().get(0);
          localObject5 = NotificationUtil.1.a;
          Object localObject6 = ((Notification)localObject1).b();
          int n = ((NotificationType)localObject6).ordinal();
          int m = localObject5[n];
          boolean bool3;
          String str;
          switch (m)
          {
          default: 
            break;
          case 3: 
            localObject5 = NotificationUtil.ShowUIAction.UiType.SETTINGS_GENERAL_LANGUAGE;
            localObject6 = NotificationUtil.ShowUIAction.UiType.getUiType(((Notification)localObject1).a("v"));
            bool2 = ((NotificationUtil.ShowUIAction.UiType)localObject5).equals(localObject6);
            if (bool2)
            {
              localObject4 = ((Notification)localObject1).a(paramContext);
              localObject5 = ((Notification)localObject1).a("a");
              localObject3 = SettingsFragment.SettingsViewType.SETTINGS_LANGUAGE_SELECTOR;
              localObject6 = SettingsFragment.a(paramContext, (SettingsFragment.SettingsViewType)localObject3);
              bool3 = true;
              str = "showView";
              localObject1 = paramContext;
              bl.a(paramContext, (String)localObject4, (String)localObject5, (Intent)localObject6, bool3, str);
            }
            break;
          case 2: 
            localObject4 = ((Notification)localObject1).a(paramContext);
            localObject3 = "a";
            localObject5 = ((Notification)localObject1).a((String)localObject3);
            localObject6 = u.a(paramContext);
            bool3 = true;
            str = "general";
            localObject1 = paramContext;
            bl.a(paramContext, (String)localObject4, (String)localObject5, (Intent)localObject6, bool3, str);
            break;
          case 1: 
            localObject1 = ((Notification)localObject1).a("a");
            localObject4 = TruecallerInit.a(paramContext, "search", "notification");
            localObject5 = "announcement";
            bl.a(paramContext, (String)localObject1, (Intent)localObject4, i, (String)localObject5);
            break;
          }
          localObject1 = "other";
          bl.a(paramContext, j, i, (String)localObject1);
        }
        else
        {
          localObject1 = "multiple";
          bl.a(paramContext, j, i, (String)localObject1);
        }
        localObject3 = new android/content/Intent;
        ((Intent)localObject3).<init>("com.truecaller.notification.action.NOTIFICATIONS_UPDATED");
        k = localf.f();
        ((Intent)localObject3).putExtra("notifications_count", k);
        android.support.v4.content.d.a(paramContext).a((Intent)localObject3);
        return;
      }
      if ((localObject3 != null) && ((bool2) || (k > 0)))
      {
        localObject1 = "swUpdateNotificationLast";
        Settings.g((String)localObject1);
        bl.a(paramContext, (Notification)localObject3);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.NotificationUtil
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */