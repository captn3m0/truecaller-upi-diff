package com.truecaller.util;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import c.n;
import com.truecaller.androidactors.w;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.d;
import com.truecaller.messaging.data.types.AudioEntity;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.ImageEntity;
import com.truecaller.messaging.data.types.VideoEntity;
import com.truecaller.utils.extensions.r;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

final class bc
  implements ba
{
  private final Context a;
  private final g b;
  private final bd c;
  private final ch d;
  private final bg e;
  private final com.truecaller.featuretoggles.e f;
  
  bc(Context paramContext, g paramg, bd parambd, ch paramch, bg parambg, com.truecaller.featuretoggles.e parame)
  {
    a = paramContext;
    b = paramg;
    c = parambd;
    d = paramch;
    e = parambg;
    f = parame;
  }
  
  /* Error */
  private Uri b(Uri paramUri)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 24	com/truecaller/util/bc:a	Landroid/content/Context;
    //   4: astore_2
    //   5: aconst_null
    //   6: astore_3
    //   7: aload_2
    //   8: aconst_null
    //   9: aconst_null
    //   10: invokestatic 39	com/truecaller/util/TempContentProvider:a	(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Integer;)Landroid/net/Uri;
    //   13: astore_2
    //   14: aload_2
    //   15: ifnonnull +5 -> 20
    //   18: aconst_null
    //   19: areturn
    //   20: aload_0
    //   21: getfield 24	com/truecaller/util/bc:a	Landroid/content/Context;
    //   24: astore 4
    //   26: aload 4
    //   28: invokevirtual 45	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   31: astore 4
    //   33: aload 4
    //   35: aload_1
    //   36: invokevirtual 51	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   39: astore_1
    //   40: aload_0
    //   41: getfield 24	com/truecaller/util/bc:a	Landroid/content/Context;
    //   44: astore 4
    //   46: aload 4
    //   48: invokevirtual 45	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   51: astore 4
    //   53: aload 4
    //   55: aload_2
    //   56: invokevirtual 55	android/content/ContentResolver:openOutputStream	(Landroid/net/Uri;)Ljava/io/OutputStream;
    //   59: astore 4
    //   61: aload_1
    //   62: ifnull +39 -> 101
    //   65: aload 4
    //   67: ifnonnull +6 -> 73
    //   70: goto +31 -> 101
    //   73: aload_1
    //   74: aload 4
    //   76: invokestatic 60	com/truecaller/utils/extensions/m:a	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   79: pop2
    //   80: aload_1
    //   81: invokestatic 65	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   84: aload 4
    //   86: invokestatic 65	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   89: aload_2
    //   90: areturn
    //   91: astore 5
    //   93: goto +54 -> 147
    //   96: astore 5
    //   98: goto +49 -> 147
    //   101: aload_1
    //   102: invokestatic 65	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   105: aload 4
    //   107: invokestatic 65	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   110: aconst_null
    //   111: areturn
    //   112: astore_2
    //   113: goto +68 -> 181
    //   116: astore 5
    //   118: goto +5 -> 123
    //   121: astore 5
    //   123: aconst_null
    //   124: astore 4
    //   126: goto +21 -> 147
    //   129: astore_2
    //   130: aconst_null
    //   131: astore_1
    //   132: goto +49 -> 181
    //   135: astore 5
    //   137: goto +5 -> 142
    //   140: astore 5
    //   142: aconst_null
    //   143: astore_1
    //   144: aconst_null
    //   145: astore 4
    //   147: aload 5
    //   149: invokestatic 71	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   152: aload_0
    //   153: getfield 30	com/truecaller/util/bc:d	Lcom/truecaller/util/ch;
    //   156: astore 5
    //   158: aload 5
    //   160: aload_2
    //   161: invokeinterface 76 2 0
    //   166: aload_1
    //   167: invokestatic 65	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   170: aload 4
    //   172: invokestatic 65	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   175: aconst_null
    //   176: areturn
    //   177: astore_2
    //   178: aload 4
    //   180: astore_3
    //   181: aload_1
    //   182: invokestatic 65	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   185: aload_3
    //   186: invokestatic 65	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   189: aload_2
    //   190: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	191	0	this	bc
    //   0	191	1	paramUri	Uri
    //   4	86	2	localObject1	Object
    //   112	1	2	localObject2	Object
    //   129	32	2	localUri	Uri
    //   177	13	2	localObject3	Object
    //   6	180	3	localObject4	Object
    //   24	155	4	localObject5	Object
    //   91	1	5	localSecurityException1	SecurityException
    //   96	1	5	localIOException1	java.io.IOException
    //   116	1	5	localSecurityException2	SecurityException
    //   121	1	5	localIOException2	java.io.IOException
    //   135	1	5	localSecurityException3	SecurityException
    //   140	8	5	localIOException3	java.io.IOException
    //   156	3	5	localch	ch
    // Exception table:
    //   from	to	target	type
    //   74	80	91	java/lang/SecurityException
    //   74	80	96	java/io/IOException
    //   40	44	112	finally
    //   46	51	112	finally
    //   55	59	112	finally
    //   40	44	116	java/lang/SecurityException
    //   46	51	116	java/lang/SecurityException
    //   55	59	116	java/lang/SecurityException
    //   40	44	121	java/io/IOException
    //   46	51	121	java/io/IOException
    //   55	59	121	java/io/IOException
    //   20	24	129	finally
    //   26	31	129	finally
    //   35	39	129	finally
    //   20	24	135	java/lang/SecurityException
    //   26	31	135	java/lang/SecurityException
    //   35	39	135	java/lang/SecurityException
    //   20	24	140	java/io/IOException
    //   26	31	140	java/io/IOException
    //   35	39	140	java/io/IOException
    //   74	80	177	finally
    //   147	152	177	finally
    //   152	156	177	finally
    //   160	166	177	finally
  }
  
  private n b(Uri paramUri, boolean paramBoolean, long paramLong)
  {
    bc localbc = this;
    Object localObject1 = paramUri;
    long l1 = paramLong;
    Object localObject2 = a;
    localObject2 = r.a(paramUri, (Context)localObject2);
    if (localObject2 == null)
    {
      localObject1 = new c/n;
      localObject3 = az.b.a;
      ((n)localObject1).<init>(null, localObject3);
      return (n)localObject1;
    }
    de localde = c.a(paramUri);
    if (localde != null)
    {
      boolean bool1 = localde.a();
      if (bool1)
      {
        Object localObject4 = f.d();
        bool1 = ((b)localObject4).a();
        long l2;
        if (bool1)
        {
          localObject4 = e;
          int i = c;
          l2 = ((bg)localObject4).b(i);
        }
        else
        {
          l2 = ((Long)localObject2).longValue();
        }
        boolean bool2 = l2 < l1;
        if (bool2)
        {
          localObject1 = new c/n;
          localObject2 = new com/truecaller/util/az$a;
          ((az.a)localObject2).<init>(l1);
          ((n)localObject1).<init>(null, localObject2);
          return (n)localObject1;
        }
        Uri localUri1 = b(paramUri);
        if (localUri1 == null)
        {
          localObject1 = new c/n;
          localObject3 = az.b.a;
          ((n)localObject1).<init>(null, localObject3);
          return (n)localObject1;
        }
        if (paramBoolean)
        {
          localObject3 = d;
          ((ch)localObject3).a((Uri)localObject1);
        }
        long l3 = -1;
        String str = d;
        bool2 = false;
        int j = a;
        int k = b;
        int m = c;
        boolean bool3 = true;
        long l4 = ((Long)localObject2).longValue();
        Uri localUri2 = Uri.EMPTY;
        localObject1 = Entity.a(l3, str, 0, localUri1, j, k, m, bool3, l4, localUri2);
        boolean bool4 = localObject1 instanceof VideoEntity;
        if (bool4)
        {
          localObject3 = new c/n;
          ((n)localObject3).<init>(localObject1, null);
          return (n)localObject3;
        }
        d.a((BinaryEntity)localObject1);
        localObject1 = new c/n;
        localObject3 = az.b.a;
        ((n)localObject1).<init>(null, localObject3);
        return (n)localObject1;
      }
    }
    localObject1 = new c/n;
    Object localObject3 = az.b.a;
    ((n)localObject1).<init>(null, localObject3);
    return (n)localObject1;
  }
  
  private n c(Uri paramUri, boolean paramBoolean)
  {
    Object localObject1;
    try
    {
      localObject1 = b;
      localObject1 = ((g)localObject1).a(paramUri);
      if (paramBoolean) {}
      try
      {
        localObject2 = d;
        ((ch)localObject2).a(paramUri);
        paramUri = new c/n;
        paramUri.<init>(localObject1, null);
        return paramUri;
      }
      catch (SecurityException paramUri) {}
      AssertionUtil.reportThrowableButNeverCrash(paramUri);
    }
    catch (SecurityException paramUri)
    {
      localObject1 = null;
    }
    d.a((BinaryEntity)localObject1);
    paramUri = new c/n;
    Object localObject2 = az.b.a;
    paramUri.<init>(null, localObject2);
    return paramUri;
  }
  
  /* Error */
  private boolean c(Uri paramUri)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 24	com/truecaller/util/bc:a	Landroid/content/Context;
    //   4: astore_2
    //   5: aload_2
    //   6: invokevirtual 45	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   9: astore_2
    //   10: aload_2
    //   11: aload_1
    //   12: invokevirtual 51	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   15: invokestatic 65	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   18: iconst_1
    //   19: ireturn
    //   20: astore_1
    //   21: aconst_null
    //   22: invokestatic 65	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   25: aload_1
    //   26: athrow
    //   27: pop
    //   28: aconst_null
    //   29: invokestatic 65	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   32: iconst_0
    //   33: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	34	0	this	bc
    //   0	34	1	paramUri	Uri
    //   4	7	2	localObject	Object
    //   27	1	3	localFileNotFoundException	java.io.FileNotFoundException
    // Exception table:
    //   from	to	target	type
    //   0	4	20	finally
    //   5	9	20	finally
    //   11	15	20	finally
    //   0	4	27	java/io/FileNotFoundException
    //   5	9	27	java/io/FileNotFoundException
    //   11	15	27	java/io/FileNotFoundException
  }
  
  private n d(Uri paramUri, boolean paramBoolean)
  {
    bc localbc = this;
    Object localObject1 = paramUri;
    Object localObject2 = a;
    localObject2 = r.a(paramUri, (Context)localObject2);
    if (localObject2 == null)
    {
      localObject1 = new c/n;
      localObject2 = az.b.a;
      ((n)localObject1).<init>(null, localObject2);
      return (n)localObject1;
    }
    e locale = c.b(paramUri);
    if (locale != null)
    {
      boolean bool1 = locale.a();
      if (bool1)
      {
        Uri localUri1 = b(paramUri);
        if (localUri1 == null)
        {
          localObject1 = new c/n;
          localObject2 = az.b.a;
          ((n)localObject1).<init>(null, localObject2);
          return (n)localObject1;
        }
        if (paramBoolean)
        {
          ch localch = d;
          localch.a(paramUri);
        }
        long l1 = -1;
        String str = b;
        int i = -1;
        int j = -1;
        int k = a;
        boolean bool2 = true;
        long l2 = ((Long)localObject2).longValue();
        Uri localUri2 = Uri.EMPTY;
        localObject1 = Entity.a(l1, str, 0, localUri1, i, j, k, bool2, l2, localUri2);
        boolean bool3 = localObject1 instanceof AudioEntity;
        if (bool3)
        {
          localObject2 = new c/n;
          ((n)localObject2).<init>(localObject1, null);
          return (n)localObject2;
        }
        d.a((BinaryEntity)localObject1);
        localObject1 = new c/n;
        localObject2 = az.b.a;
        ((n)localObject1).<init>(null, localObject2);
        return (n)localObject1;
      }
    }
    localObject1 = new c/n;
    localObject2 = az.b.a;
    ((n)localObject1).<init>(null, localObject2);
    return (n)localObject1;
  }
  
  /* Error */
  private File d(Uri paramUri)
  {
    // Byte code:
    //   0: invokestatic 183	android/webkit/MimeTypeMap:getSingleton	()Landroid/webkit/MimeTypeMap;
    //   3: astore_2
    //   4: aload_0
    //   5: getfield 24	com/truecaller/util/bc:a	Landroid/content/Context;
    //   8: invokevirtual 45	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   11: aload_1
    //   12: invokevirtual 187	android/content/ContentResolver:getType	(Landroid/net/Uri;)Ljava/lang/String;
    //   15: astore_3
    //   16: aload_2
    //   17: aload_3
    //   18: invokevirtual 191	android/webkit/MimeTypeMap:getExtensionFromMimeType	(Ljava/lang/String;)Ljava/lang/String;
    //   21: astore_2
    //   22: getstatic 196	android/os/Environment:DIRECTORY_DOWNLOADS	Ljava/lang/String;
    //   25: invokestatic 200	android/os/Environment:getExternalStoragePublicDirectory	(Ljava/lang/String;)Ljava/io/File;
    //   28: astore_3
    //   29: new 202	org/a/a/b
    //   32: astore 4
    //   34: invokestatic 207	java/lang/System:currentTimeMillis	()J
    //   37: lstore 5
    //   39: aload 4
    //   41: lload 5
    //   43: invokespecial 208	org/a/a/b:<init>	(J)V
    //   46: ldc -46
    //   48: invokestatic 215	org/a/a/d/a:a	(Ljava/lang/String;)Lorg/a/a/d/b;
    //   51: astore 7
    //   53: aload 7
    //   55: ifnonnull +13 -> 68
    //   58: aload 4
    //   60: invokevirtual 221	org/a/a/a/c:toString	()Ljava/lang/String;
    //   63: astore 4
    //   65: goto +12 -> 77
    //   68: aload 7
    //   70: aload 4
    //   72: invokevirtual 226	org/a/a/d/b:a	(Lorg/a/a/x;)Ljava/lang/String;
    //   75: astore 4
    //   77: iconst_0
    //   78: istore 8
    //   80: aconst_null
    //   81: astore 7
    //   83: new 228	java/io/File
    //   86: astore 9
    //   88: new 230	java/lang/StringBuilder
    //   91: astore 10
    //   93: aload 10
    //   95: ldc -24
    //   97: invokespecial 235	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   100: ldc -19
    //   102: astore 11
    //   104: aload 10
    //   106: aload 11
    //   108: invokevirtual 241	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   111: pop
    //   112: aload 10
    //   114: aload 4
    //   116: invokevirtual 241	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   119: pop
    //   120: iload 8
    //   122: ifle +31 -> 153
    //   125: aload 10
    //   127: ldc -13
    //   129: invokevirtual 241	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   132: pop
    //   133: aload 10
    //   135: iload 8
    //   137: invokevirtual 246	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   140: pop
    //   141: ldc -8
    //   143: astore 11
    //   145: aload 10
    //   147: aload 11
    //   149: invokevirtual 241	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   152: pop
    //   153: aload_2
    //   154: invokestatic 253	org/c/a/a/a/k:b	(Ljava/lang/CharSequence;)Z
    //   157: istore 12
    //   159: iload 12
    //   161: ifne +22 -> 183
    //   164: ldc -1
    //   166: astore 11
    //   168: aload 10
    //   170: aload 11
    //   172: invokevirtual 241	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   175: pop
    //   176: aload 10
    //   178: aload_2
    //   179: invokevirtual 241	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   182: pop
    //   183: aload 10
    //   185: invokevirtual 256	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   188: astore 10
    //   190: aload 9
    //   192: aload_3
    //   193: aload 10
    //   195: invokespecial 259	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   198: iload 8
    //   200: iconst_1
    //   201: iadd
    //   202: istore 8
    //   204: aload 9
    //   206: invokevirtual 262	java/io/File:exists	()Z
    //   209: istore 13
    //   211: iload 13
    //   213: ifne -130 -> 83
    //   216: aconst_null
    //   217: astore_2
    //   218: aload_0
    //   219: getfield 24	com/truecaller/util/bc:a	Landroid/content/Context;
    //   222: astore_3
    //   223: aload_3
    //   224: invokevirtual 45	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   227: astore_3
    //   228: aload_3
    //   229: aload_1
    //   230: invokevirtual 51	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   233: astore_1
    //   234: new 264	java/io/FileOutputStream
    //   237: astore_3
    //   238: aload_3
    //   239: aload 9
    //   241: invokespecial 267	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   244: aload_1
    //   245: ifnonnull +13 -> 258
    //   248: aload_1
    //   249: invokestatic 65	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   252: aload_3
    //   253: invokestatic 65	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   256: aconst_null
    //   257: areturn
    //   258: aload_1
    //   259: aload_3
    //   260: invokestatic 60	com/truecaller/utils/extensions/m:a	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   263: pop2
    //   264: aload_1
    //   265: invokestatic 65	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   268: goto +80 -> 348
    //   271: astore 14
    //   273: aload_1
    //   274: astore_2
    //   275: aload 14
    //   277: astore_1
    //   278: goto +78 -> 356
    //   281: astore_2
    //   282: goto +4 -> 286
    //   285: astore_2
    //   286: aload_2
    //   287: astore 14
    //   289: aload_1
    //   290: astore_2
    //   291: aload 14
    //   293: astore_1
    //   294: goto +46 -> 340
    //   297: astore_3
    //   298: aconst_null
    //   299: astore 14
    //   301: aload_1
    //   302: astore_2
    //   303: aload_3
    //   304: astore_1
    //   305: aconst_null
    //   306: astore_3
    //   307: goto +49 -> 356
    //   310: astore_3
    //   311: goto +4 -> 315
    //   314: astore_3
    //   315: aconst_null
    //   316: astore 14
    //   318: aload_1
    //   319: astore_2
    //   320: aload_3
    //   321: astore_1
    //   322: aconst_null
    //   323: astore_3
    //   324: goto +16 -> 340
    //   327: astore_1
    //   328: aconst_null
    //   329: astore_3
    //   330: goto +26 -> 356
    //   333: astore_1
    //   334: goto +4 -> 338
    //   337: astore_1
    //   338: aconst_null
    //   339: astore_3
    //   340: aload_1
    //   341: invokestatic 71	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   344: aload_2
    //   345: invokestatic 65	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   348: aload_3
    //   349: invokestatic 65	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   352: aload 9
    //   354: areturn
    //   355: astore_1
    //   356: aload_2
    //   357: invokestatic 65	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   360: aload_3
    //   361: invokestatic 65	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   364: aload_1
    //   365: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	366	0	this	bc
    //   0	366	1	paramUri	Uri
    //   3	272	2	localObject1	Object
    //   281	1	2	localSecurityException1	SecurityException
    //   285	2	2	localIOException1	java.io.IOException
    //   290	67	2	localUri	Uri
    //   15	245	3	localObject2	Object
    //   297	7	3	localObject3	Object
    //   306	1	3	localObject4	Object
    //   310	1	3	localSecurityException2	SecurityException
    //   314	7	3	localIOException2	java.io.IOException
    //   323	38	3	localCloseable	java.io.Closeable
    //   32	83	4	localObject5	Object
    //   37	5	5	l	long
    //   51	31	7	localb	org.a.a.d.b
    //   78	125	8	i	int
    //   86	267	9	localFile	File
    //   91	103	10	localObject6	Object
    //   102	69	11	str	String
    //   157	3	12	bool1	boolean
    //   209	3	13	bool2	boolean
    //   271	5	14	localObject7	Object
    //   287	30	14	localIOException3	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   259	264	271	finally
    //   259	264	281	java/lang/SecurityException
    //   259	264	285	java/io/IOException
    //   234	237	297	finally
    //   239	244	297	finally
    //   234	237	310	java/lang/SecurityException
    //   239	244	310	java/lang/SecurityException
    //   234	237	314	java/io/IOException
    //   239	244	314	java/io/IOException
    //   218	222	327	finally
    //   223	227	327	finally
    //   229	233	327	finally
    //   218	222	333	java/lang/SecurityException
    //   223	227	333	java/lang/SecurityException
    //   229	233	333	java/lang/SecurityException
    //   218	222	337	java/io/IOException
    //   223	227	337	java/io/IOException
    //   229	233	337	java/io/IOException
    //   340	344	355	finally
  }
  
  public final w a(Uri paramUri)
  {
    return w.b(Boolean.valueOf(c(paramUri)));
  }
  
  public final w a(Uri paramUri, boolean paramBoolean)
  {
    return w.b(c(paramUri, paramBoolean));
  }
  
  public final w a(Uri paramUri, boolean paramBoolean, long paramLong)
  {
    return w.b(b(paramUri, paramBoolean, paramLong));
  }
  
  public final w a(Collection paramCollection, long paramLong)
  {
    bc localbc = this;
    n localn = new c/n;
    Object localObject1 = az.b.a;
    localn.<init>(null, localObject1);
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Iterator localIterator = paramCollection.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject2 = (d)localIterator.next();
      if (localObject2 == null)
      {
        ((List)localObject1).add(localn);
      }
      else
      {
        Object localObject3 = a;
        Object localObject4 = b;
        boolean bool2 = org.c.a.a.a.k.c((CharSequence)localObject4);
        if (bool2)
        {
          localObject2 = b;
        }
        else
        {
          localObject2 = a;
          localObject2 = r.c((Uri)localObject3, (Context)localObject2);
        }
        long l1;
        if (localObject2 != null)
        {
          localObject4 = "image/gif";
          bool2 = ((String)localObject4).equalsIgnoreCase((String)localObject2);
          if (bool2)
          {
            localObject4 = f;
            localObject5 = T;
            Object localObject6 = com.truecaller.featuretoggles.e.a;
            int i = 120;
            localObject6 = localObject6[i];
            localObject4 = ((e.a)localObject5).a((com.truecaller.featuretoggles.e)localObject4, (c.l.g)localObject6);
            bool2 = ((b)localObject4).a();
            if (bool2)
            {
              localObject2 = a;
              localObject2 = r.a((Uri)localObject3, (Context)localObject2);
              if (localObject2 == null)
              {
                localObject2 = new c/n;
                localObject3 = az.b.a;
                ((n)localObject2).<init>(null, localObject3);
              }
              else
              {
                localObject4 = c.c((Uri)localObject3);
                if (localObject4 != null)
                {
                  localObject5 = "image/gif";
                  localObject6 = c;
                  bool3 = c.g.b.k.a(localObject5, localObject6);
                  if (bool3)
                  {
                    Uri localUri1 = localbc.b((Uri)localObject3);
                    if (localUri1 == null)
                    {
                      localObject2 = new c/n;
                      localObject3 = az.b.a;
                      ((n)localObject2).<init>(null, localObject3);
                      break label489;
                    }
                    l1 = -1;
                    String str = c;
                    int j = a;
                    int k = b;
                    int m = -1;
                    boolean bool4 = true;
                    long l2 = ((Long)localObject2).longValue();
                    Uri localUri2 = Uri.EMPTY;
                    localObject2 = Entity.a(l1, str, 0, localUri1, j, k, m, bool4, l2, localUri2);
                    boolean bool5 = localObject2 instanceof ImageEntity;
                    if (bool5)
                    {
                      localObject3 = new c/n;
                      ((n)localObject3).<init>(localObject2, null);
                      localObject2 = localObject3;
                      break label489;
                    }
                    d.a((BinaryEntity)localObject2);
                    localObject2 = new c/n;
                    localObject3 = az.b.a;
                    ((n)localObject2).<init>(null, localObject3);
                    break label489;
                  }
                }
                localObject2 = new c/n;
                localObject3 = az.b.a;
                ((n)localObject2).<init>(null, localObject3);
              }
              label489:
              l1 = paramLong;
              break label692;
            }
          }
          bool2 = Entity.c((String)localObject2);
          boolean bool3 = false;
          Object localObject5 = null;
          if (bool2)
          {
            localObject4 = "image/gif";
            bool2 = ((String)localObject4).equalsIgnoreCase((String)localObject2);
            if (!bool2)
            {
              localObject2 = localbc.c((Uri)localObject3, false);
              l1 = paramLong;
              break label692;
            }
          }
          bool2 = Entity.d((String)localObject2);
          if (bool2)
          {
            l1 = paramLong;
            localObject2 = localbc.b((Uri)localObject3, false, paramLong);
          }
          else
          {
            l1 = paramLong;
            bool2 = Entity.e((String)localObject2);
            if (bool2)
            {
              localObject2 = localbc.d((Uri)localObject3, false);
            }
            else
            {
              bool1 = Entity.f((String)localObject2);
              if (bool1)
              {
                localObject3 = ((Uri)localObject3).toString();
                long l3 = -1;
                localObject2 = (BinaryEntity)Entity.a("text/x-vcard", 0, (String)localObject3, l3);
                localObject3 = new c/n;
                ((n)localObject3).<init>(localObject2, null);
                localObject2 = localObject3;
              }
              else
              {
                localObject2 = new c/n;
                localObject3 = az.c.a;
                ((n)localObject2).<init>(null, localObject3);
              }
            }
          }
        }
        else
        {
          l1 = paramLong;
          localObject2 = localn;
        }
        label692:
        ((List)localObject1).add(localObject2);
      }
    }
    return w.b(localObject1);
  }
  
  public final w a(List paramList)
  {
    paramList = paramList.iterator();
    boolean bool;
    do
    {
      bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      Uri localUri = (Uri)paramList.next();
      bool = c(localUri);
    } while (bool);
    return w.b(Boolean.FALSE);
    return w.b(Boolean.TRUE);
  }
  
  public final w a(Entity[] paramArrayOfEntity)
  {
    int i = paramArrayOfEntity.length;
    int j = 0;
    boolean bool1 = false;
    Object localObject1 = null;
    while (j < i)
    {
      Object localObject2 = paramArrayOfEntity[j];
      boolean bool2 = ((Entity)localObject2).b();
      if (!bool2)
      {
        bool2 = ((Entity)localObject2).d();
        if (!bool2) {}
      }
      else
      {
        localObject2 = (BinaryEntity)localObject2;
        Object localObject3 = b;
        localObject3 = d((Uri)localObject3);
        if (localObject3 != null)
        {
          String str1 = j;
          localObject1 = a;
          localObject2 = "download";
          localObject1 = ((Context)localObject1).getSystemService((String)localObject2);
          Object localObject4 = localObject1;
          localObject4 = (DownloadManager)localObject1;
          if (localObject4 != null)
          {
            String str2 = ((File)localObject3).getName();
            String str3 = ((File)localObject3).getName();
            boolean bool3 = true;
            String str4 = ((File)localObject3).getAbsolutePath();
            long l = ((File)localObject3).length();
            boolean bool4 = true;
            ((DownloadManager)localObject4).addCompletedDownload(str2, str3, bool3, str1, str4, l, bool4);
          }
          bool1 = true;
        }
      }
      j += 1;
    }
    return w.b(Boolean.valueOf(bool1));
  }
  
  public final w b(Uri paramUri, boolean paramBoolean)
  {
    return w.b(d(paramUri, paramBoolean));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bc
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */