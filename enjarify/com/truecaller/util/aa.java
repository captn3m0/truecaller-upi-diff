package com.truecaller.util;

import android.net.Uri;
import com.truecaller.androidactors.w;
import com.truecaller.data.entity.HistoryEvent;
import java.util.List;

public abstract interface aa
{
  public abstract w a();
  
  public abstract w a(long paramLong);
  
  public abstract w a(Uri paramUri);
  
  public abstract w a(String paramString);
  
  public abstract w a(List paramList);
  
  public abstract void a(HistoryEvent paramHistoryEvent);
  
  public abstract w b(Uri paramUri);
  
  public abstract w b(String paramString);
  
  public abstract w c(Uri paramUri);
  
  public abstract w c(String paramString);
  
  public abstract w d(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.util.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */