package com.truecaller.util;

import dagger.a.d;
import javax.inject.Provider;

public final class db
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private db(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static db a(Provider paramProvider1, Provider paramProvider2)
  {
    db localdb = new com/truecaller/util/db;
    localdb.<init>(paramProvider1, paramProvider2);
    return localdb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.db
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */