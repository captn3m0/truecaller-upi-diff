package com.truecaller.util;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.net.Uri.Builder;
import android.provider.ContactsContract.Contacts;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import com.truecaller.content.TruecallerContract;
import com.truecaller.log.d;

public final class y
{
  private static Intent a()
  {
    Intent localIntent = new android/content/Intent;
    Uri localUri = ContactsContract.Contacts.CONTENT_URI;
    localIntent.<init>("android.intent.action.INSERT", localUri);
    localIntent.putExtra("finishActivityOnSaveCompleted", true);
    return localIntent;
  }
  
  public static Uri a(long paramLong, String paramString, boolean paramBoolean)
  {
    Object localObject = TruecallerContract.b;
    Uri localUri = null;
    if (localObject == null) {
      return null;
    }
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      localUri = Uri.parse(paramString);
      if (paramBoolean) {
        localUri = aw.a(localUri);
      }
    }
    long l = 0L;
    bool = paramLong < l;
    if (bool)
    {
      paramString = TruecallerContract.b.buildUpon().appendPath("photo");
      String str1 = "tcphoto";
      if (localUri != null) {
        localObject = localUri.toString();
      } else {
        localObject = "";
      }
      paramString = paramString.appendQueryParameter(str1, (String)localObject);
      String str2 = String.valueOf(paramLong);
      return paramString.appendQueryParameter("pbid", str2).build();
    }
    return localUri;
  }
  
  public static void a(Fragment paramFragment)
  {
    try
    {
      Intent localIntent = a();
      int i = 4097;
      paramFragment.startActivityForResult(localIntent, i);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      d.a(localActivityNotFoundException;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */