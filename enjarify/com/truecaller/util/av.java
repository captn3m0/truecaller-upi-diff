package com.truecaller.util;

import c.g.b.k;
import com.truecaller.calling.dialer.CallIconType;
import com.truecaller.data.entity.HistoryEvent;

public final class av
{
  public static final CallIconType a(HistoryEvent paramHistoryEvent)
  {
    Object localObject = "receiver$0";
    k.b(paramHistoryEvent, (String)localObject);
    int i = paramHistoryEvent.h();
    int j = 1;
    if (i != j)
    {
      j = 3;
      if (i != j)
      {
        i = 0;
        localObject = null;
      }
      else
      {
        localObject = CallIconType.MUTED_CALL_ICON;
      }
    }
    else
    {
      localObject = CallIconType.HUNG_UP_CALL_ICON;
    }
    if (localObject == null)
    {
      int k = paramHistoryEvent.f();
      switch (k)
      {
      default: 
        paramHistoryEvent = new java/lang/IllegalStateException;
        paramHistoryEvent.<init>("We expect history event always has call type(incoming/outgoing/missed).");
        throw ((Throwable)paramHistoryEvent);
      case 3: 
        return CallIconType.MISSED_CALL_ICON;
      case 2: 
        return CallIconType.OUTGOING_CALL_ICON;
      }
      return CallIconType.INCOMING_CALL_ICON;
    }
    return (CallIconType)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.av
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */