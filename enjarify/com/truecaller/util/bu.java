package com.truecaller.util;

import android.content.Context;
import android.content.SharedPreferences;
import c.g.b.k;
import com.truecaller.utils.a.a;

public final class bu
  extends a
  implements bt
{
  public bu(SharedPreferences paramSharedPreferences)
  {
    super(paramSharedPreferences);
  }
  
  public final int a()
  {
    return 1;
  }
  
  public final void a(int paramInt, Context paramContext)
  {
    k.b(paramContext, "context");
  }
  
  public final void a(boolean paramBoolean)
  {
    b("imDebugVersioning", paramBoolean);
  }
  
  public final String b()
  {
    return "qa-menu";
  }
  
  public final void b(boolean paramBoolean)
  {
    b("imDebugCommands", paramBoolean);
  }
  
  public final void c(boolean paramBoolean)
  {
    b("imEmptyUserInfo", paramBoolean);
  }
  
  public final boolean c()
  {
    return a("imDebugVersioning", false);
  }
  
  public final boolean d()
  {
    return a("imDebugCommands", false);
  }
  
  public final boolean e()
  {
    return b("imEmptyUserInfo");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bu
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */