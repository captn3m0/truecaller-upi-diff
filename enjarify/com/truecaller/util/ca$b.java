package com.truecaller.util;

import android.content.Context;
import android.support.v7.app.AlertDialog.Builder;
import c.g.b.k;

public final class ca$b
  extends ca
{
  private final String a;
  private final String[] b;
  
  public ca$b(String paramString, String... paramVarArgs)
  {
    super((byte)0);
    a = paramString;
    b = paramVarArgs;
  }
  
  public final void a(Context paramContext)
  {
    k.b(paramContext, "context");
    Object localObject = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject).<init>(paramContext);
    paramContext = (CharSequence)a;
    paramContext = ((AlertDialog.Builder)localObject).setTitle(paramContext);
    localObject = (CharSequence[])b;
    paramContext.setItems((CharSequence[])localObject, null).setPositiveButton(2131887217, null).setCancelable(false).show();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ca.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */