package com.truecaller.util;

import android.app.Activity;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.search.global.n;

public final class NotificationUtil$f
  implements NotificationUtil.b
{
  private final Activity a;
  private final String b;
  private final String c;
  
  public NotificationUtil$f(Activity paramActivity, Notification paramNotification)
  {
    a = paramActivity;
    paramActivity = paramNotification.a("q");
    b = paramActivity;
    paramActivity = paramNotification.a("c");
    c = paramActivity;
  }
  
  public final void a()
  {
    Activity localActivity = a;
    String str1 = b;
    String str2 = c;
    n.a(localActivity, str1, str2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.NotificationUtil.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */