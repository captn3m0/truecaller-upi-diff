package com.truecaller.util;

import android.content.Context;
import android.media.ToneGenerator;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.Vibrator;
import com.a.a.k;
import com.a.a.l;
import com.truecaller.old.data.access.Settings;

public final class an
{
  public static final l a;
  public final Context b;
  public final boolean c;
  public Handler d;
  private HandlerThread e;
  private ToneGenerator f;
  private Vibrator g;
  
  static
  {
    k localk = new com/a/a/k;
    localk.<init>(12, 0.99D);
    a = localk;
    localk.a('1', 1);
    a.a('2', 2);
    a.a('3', 3);
    a.a('4', 4);
    a.a('5', 5);
    a.a('6', 6);
    a.a('7', 7);
    a.a('8', 8);
    a.a('9', 9);
    a.a('0', 0);
    a.a('*', 10);
    a.a('#', 11);
  }
  
  public an(Context paramContext)
  {
    b = paramContext;
    boolean bool = Settings.e(paramContext);
    c = bool;
    paramContext = new com/truecaller/util/an$1;
    paramContext.<init>(this, "feedback");
    e = paramContext;
    e.start();
  }
  
  public final void a()
  {
    HandlerThread localHandlerThread = e;
    if (localHandlerThread != null)
    {
      localHandlerThread.quit();
      localHandlerThread = null;
      e = null;
    }
  }
  
  public final void b()
  {
    Handler localHandler = d;
    if (localHandler == null) {
      return;
    }
    Message localMessage = Message.obtain(localHandler, 1);
    localHandler.sendMessageDelayed(localMessage, 20);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.an
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */