package com.truecaller.util.b;

import android.text.TextUtils;
import com.truecaller.TrueApp;
import com.truecaller.analytics.e.a;
import com.truecaller.bp;
import com.truecaller.old.data.access.Settings;

public final class e
{
  public static j a()
  {
    Object localObject1 = com.truecaller.common.b.e.a("featureOperatorCustomization");
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1);
    Object localObject2 = null;
    if (bool1) {
      return null;
    }
    int i = -1;
    int j = ((String)localObject1).hashCode();
    int k = 2554;
    boolean bool2 = true;
    String str1;
    boolean bool3;
    Object localObject3;
    if (j != k)
    {
      k = 84088;
      if (j != k)
      {
        k = 2666700;
        if (j != k)
        {
          k = 65190197;
          if (j != k)
          {
            k = 235300551;
            if (j == k)
            {
              str1 = "Telenor";
              bool3 = ((String)localObject1).equals(str1);
              if (bool3) {
                i = 2;
              }
            }
          }
          else
          {
            str1 = "Claro";
            bool3 = ((String)localObject1).equals(str1);
            if (bool3) {
              i = 4;
            }
          }
        }
        else
        {
          str1 = "Vivo";
          bool3 = ((String)localObject1).equals(str1);
          if (bool3) {
            i = 1;
          }
        }
      }
      else
      {
        str1 = "Tim";
        bool3 = ((String)localObject1).equals(str1);
        if (bool3) {
          i = 3;
        }
      }
    }
    else
    {
      str1 = "Oi";
      bool3 = ((String)localObject1).equals(str1);
      if (bool3)
      {
        i = 0;
        localObject3 = null;
      }
    }
    switch (i)
    {
    default: 
      break;
    case 4: 
      localObject2 = new com/truecaller/util/b/a;
      ((a)localObject2).<init>();
      break;
    case 3: 
      localObject2 = new com/truecaller/util/b/d;
      ((d)localObject2).<init>();
      break;
    case 2: 
      localObject2 = new com/truecaller/util/b/c;
      ((c)localObject2).<init>();
      break;
    case 1: 
      localObject2 = new com/truecaller/util/b/f;
      ((f)localObject2).<init>();
      break;
    case 0: 
      localObject2 = new com/truecaller/util/b/b;
      ((b)localObject2).<init>();
    }
    if (localObject2 != null)
    {
      localObject1 = ((j)localObject2).a();
      if (localObject1 != null)
      {
        localObject1 = "key_carrier_logged";
        bool3 = Settings.e((String)localObject1);
        if (!bool3)
        {
          Settings.a("key_carrier_logged", bool2);
          localObject1 = TrueApp.y().a().c();
          localObject3 = new com/truecaller/analytics/e$a;
          ((e.a)localObject3).<init>("CARRIER_Customization_Applied");
          str1 = "Partner";
          String str2 = aa;
          localObject3 = ((e.a)localObject3).a(str1, str2).a();
          ((com.truecaller.analytics.b)localObject1).a((com.truecaller.analytics.e)localObject3);
        }
      }
    }
    return (j)localObject2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */