package com.truecaller.util.b;

import android.app.Activity;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.bp;
import com.truecaller.common.h.o;

public final class e$1
  implements AdapterView.OnItemClickListener
{
  public e$1(AlertDialog paramAlertDialog, Activity paramActivity, j.a parama) {}
  
  public final void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    a.dismiss();
    paramAdapterView = b;
    paramView = paramAdapterView.getResources();
    Object localObject = c;
    int i = f;
    paramView = paramView.getStringArray(i)[paramInt];
    boolean bool = TextUtils.isEmpty(paramView);
    if (!bool)
    {
      localObject = Uri.parse(paramView).getScheme();
      int j = -1;
      int k = ((String)localObject).hashCode();
      int m = 114009;
      String str;
      if (k != m)
      {
        m = 3213448;
        if (k != m)
        {
          m = 99617003;
          if (k != m)
          {
            m = 109566356;
            if (k == m)
            {
              str = "smsto";
              bool = ((String)localObject).equals(str);
              if (bool) {
                j = 3;
              }
            }
          }
          else
          {
            str = "https";
            bool = ((String)localObject).equals(str);
            if (bool) {
              j = 1;
            }
          }
        }
        else
        {
          str = "http";
          bool = ((String)localObject).equals(str);
          if (bool) {
            j = 0;
          }
        }
      }
      else
      {
        str = "sms";
        bool = ((String)localObject).equals(str);
        if (bool) {
          j = 2;
        }
      }
      switch (j)
      {
      default: 
        break;
      case 2: 
      case 3: 
        paramView = o.b(Uri.parse(paramView));
        o.a(paramAdapterView, paramView);
        break;
      case 0: 
      case 1: 
        paramView = o.a(Uri.parse(paramView));
        o.a(paramAdapterView, paramView);
      }
    }
    paramAdapterView = TrueApp.y().a().c();
    paramView = new com/truecaller/analytics/e$a;
    paramView.<init>("CARRIER_Menu_Item_Selected");
    paramView = paramView.a("Position", paramInt);
    localObject = c.a;
    paramView = paramView.a("Partner", (String)localObject).a();
    paramAdapterView.a(paramView);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.b.e.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */