package com.truecaller.util.b;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public final class e$a
  extends ArrayAdapter
{
  public e$a(Context paramContext, String[] paramArrayOfString)
  {
    super(paramContext, 0, paramArrayOfString);
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    int i = 0;
    if (paramView == null)
    {
      paramView = LayoutInflater.from(getContext());
      j = 2131559162;
      paramView = paramView.inflate(j, paramViewGroup, false);
    }
    paramViewGroup = (TextView)paramView.findViewById(2131364884);
    CharSequence localCharSequence = (CharSequence)getItem(paramInt);
    paramViewGroup.setText(localCharSequence);
    int k = 2131362867;
    paramViewGroup = paramView.findViewById(k);
    int j = getCount() + -1;
    if (paramInt == j) {
      i = 8;
    }
    paramViewGroup.setVisibility(i);
    return paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.b.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */