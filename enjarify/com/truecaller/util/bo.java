package com.truecaller.util;

import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import com.google.c.a.a;
import com.google.c.a.k;

public final class bo
  implements TextWatcher
{
  private boolean a = false;
  private boolean b;
  private a c;
  
  public bo(String paramString)
  {
    if (paramString != null)
    {
      k.a();
      paramString = k.d(paramString);
      c = paramString;
      return;
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>();
    throw paramString;
  }
  
  private String a(char paramChar, boolean paramBoolean)
  {
    if (paramBoolean) {
      return c.b(paramChar);
    }
    return c.a(paramChar);
  }
  
  private void a()
  {
    b = true;
    c.a();
  }
  
  private static boolean a(CharSequence paramCharSequence, int paramInt1, int paramInt2)
  {
    int i = paramInt1;
    for (;;)
    {
      int j = paramInt1 + paramInt2;
      if (i >= j) {
        break;
      }
      boolean bool = PhoneNumberUtils.isNonSeparator(paramCharSequence.charAt(i));
      if (!bool) {
        return true;
      }
      i += 1;
    }
    return false;
  }
  
  public final void afterTextChanged(Editable paramEditable)
  {
    try
    {
      boolean bool1 = b;
      int j = 1;
      String str1;
      if (bool1)
      {
        int k = paramEditable.length();
        if (k == 0)
        {
          j = 0;
          str1 = null;
        }
        b = j;
        return;
      }
      bool1 = a;
      if (bool1) {
        return;
      }
      int i = Selection.getSelectionEnd(paramEditable) - j;
      int m = 0;
      Object localObject1 = null;
      a locala = c;
      locala.a();
      int n = paramEditable.length();
      int i3 = 0;
      String str2 = null;
      m = 0;
      localObject1 = null;
      int i4 = 0;
      boolean bool3 = false;
      Object localObject2 = null;
      int i11;
      while (m < n)
      {
        int i9 = paramEditable.charAt(m);
        i11 = PhoneNumberUtils.isNonSeparator(i9);
        if (i11 != 0)
        {
          if (i4 != 0)
          {
            str2 = a(i4, bool3);
            bool3 = false;
            localObject2 = null;
          }
          i4 = i9;
        }
        if (m == i) {
          bool3 = true;
        }
        m += 1;
      }
      String str3;
      if (i4 != 0)
      {
        str2 = a(i4, bool3);
        str3 = str2;
      }
      else
      {
        str3 = str2;
      }
      if (str3 != null)
      {
        localObject1 = c;
        int i1 = c;
        if (i1 == 0)
        {
          m = d;
          i11 = m;
        }
        else
        {
          i1 = 0;
          locala = null;
          i4 = 0;
          for (;;)
          {
            int i6 = e;
            if (i1 >= i6) {
              break;
            }
            localObject2 = a;
            int i7 = ((String)localObject2).length();
            if (i4 >= i7) {
              break;
            }
            localObject2 = b;
            int i8 = ((StringBuilder)localObject2).charAt(i1);
            str2 = a;
            i3 = str2.charAt(i4);
            if (i8 == i3) {
              i1 += 1;
            }
            i4 += 1;
          }
          i11 = i5;
        }
        a = j;
        int i2 = 0;
        locala = null;
        int i5 = paramEditable.length();
        i3 = 0;
        str2 = null;
        int i10 = str3.length();
        localObject1 = paramEditable;
        localObject2 = str3;
        paramEditable.replace(0, i5, str3, 0, i10);
        str1 = paramEditable.toString();
        boolean bool2 = str3.equals(str1);
        if (bool2) {
          Selection.setSelection(paramEditable, i11);
        }
        a = false;
      }
      return;
    }
    finally {}
  }
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    paramInt3 = a;
    if (paramInt3 == 0)
    {
      paramInt3 = b;
      if (paramInt3 == 0)
      {
        if (paramInt2 > 0)
        {
          boolean bool = a(paramCharSequence, paramInt1, paramInt2);
          if (bool) {
            a();
          }
        }
        return;
      }
    }
  }
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    paramInt2 = a;
    if (paramInt2 == 0)
    {
      paramInt2 = b;
      if (paramInt2 == 0)
      {
        if (paramInt3 > 0)
        {
          boolean bool = a(paramCharSequence, paramInt1, paramInt3);
          if (bool) {
            a();
          }
        }
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */