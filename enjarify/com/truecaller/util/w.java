package com.truecaller.util;

import android.content.Context;
import android.os.Parcel;
import android.text.TextUtils;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.ai;
import com.truecaller.common.h.am;
import com.truecaller.content.a.a;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Business;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Link;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.Style;
import com.truecaller.filters.FilterManager.ActionSource;
import com.truecaller.filters.g;
import com.truecaller.log.AssertionUtil;
import com.truecaller.profile.data.dto.OpenHours;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

public final class w
{
  private static final String[] a = tmp5_4;
  
  static
  {
    String[] tmp4_1 = new String[3];
    String[] tmp5_4 = tmp4_1;
    String[] tmp5_4 = tmp4_1;
    tmp5_4[0] = "_id";
    tmp5_4[1] = "data3";
    tmp5_4[2] = "mimetype";
  }
  
  public static int a(int paramInt)
  {
    int i = 32;
    boolean bool = a.a(paramInt, i);
    if (bool) {
      return i;
    }
    i = 4;
    bool = a.a(paramInt, i);
    if (bool) {
      return i;
    }
    i = 64;
    bool = a.a(paramInt, i);
    if (bool) {
      return i;
    }
    i = 8;
    bool = a.a(paramInt, i);
    if (bool) {
      return i;
    }
    i = 1;
    paramInt = a.a(paramInt, i);
    if (paramInt != 0) {
      return i;
    }
    return 0;
  }
  
  public static int a(int paramInt, boolean paramBoolean)
  {
    paramInt = a(paramInt);
    int i = 32;
    if (paramInt == i) {
      return 2131234164;
    }
    i = 4;
    if (paramInt == i) {
      return 2131234394;
    }
    i = 64;
    if ((paramInt == i) && (paramBoolean)) {
      return 2131233900;
    }
    i = 8;
    if ((paramInt == i) && (paramBoolean)) {
      return 2131233827;
    }
    i = 1;
    if ((paramInt == i) && (paramBoolean)) {
      return 2131234653;
    }
    return 0;
  }
  
  public static Number a(Contact paramContact, Number paramNumber)
  {
    if ((paramContact != null) && (paramNumber != null))
    {
      paramContact = paramContact.A().iterator();
      Number localNumber;
      boolean bool2;
      do
      {
        boolean bool1 = paramContact.hasNext();
        if (!bool1) {
          break;
        }
        localNumber = (Number)paramContact.next();
        bool2 = a(paramNumber, localNumber);
      } while (!bool2);
      return localNumber;
    }
    return null;
  }
  
  public static Number a(Contact paramContact, String paramString)
  {
    if (paramContact != null)
    {
      boolean bool1 = TextUtils.isEmpty(paramString);
      if (!bool1)
      {
        paramContact = paramContact.A().iterator();
        Number localNumber;
        boolean bool2;
        do
        {
          bool1 = paramContact.hasNext();
          if (!bool1) {
            break label84;
          }
          localNumber = (Number)paramContact.next();
          String str = localNumber.a();
          bool2 = ab.a(str, paramString);
          if (bool2) {
            break;
          }
          str = localNumber.d();
          bool2 = ab.a(str, paramString);
        } while (!bool2);
        return localNumber;
      }
    }
    label84:
    return null;
  }
  
  private static Number a(Contact paramContact, boolean paramBoolean)
  {
    String str1 = paramContact.p();
    if (str1 != null)
    {
      boolean bool1 = ab.e(str1);
      if (bool1) {
        if (paramBoolean)
        {
          bool1 = paramContact.p(str1);
          if (bool1) {}
        }
        else
        {
          return paramContact.r();
        }
      }
    }
    Iterator localIterator = paramContact.A().iterator();
    Number localNumber;
    boolean bool4;
    do
    {
      String str2;
      boolean bool3;
      do
      {
        do
        {
          do
          {
            boolean bool2 = localIterator.hasNext();
            if (!bool2) {
              break;
            }
            localNumber = (Number)localIterator.next();
            str2 = localNumber.a();
            bool3 = TextUtils.isEmpty(str2);
          } while (bool3);
          bool3 = str2.equals(str1);
        } while (bool3);
        bool3 = ab.e(str2);
      } while (!bool3);
      if (!paramBoolean) {
        break;
      }
      bool4 = paramContact.p(str2);
    } while (bool4);
    return localNumber;
    return null;
  }
  
  public static List a(Context paramContext, Long paramLong)
  {
    return a(paramContext, paramLong, null);
  }
  
  /* Error */
  public static List a(Context paramContext, Long paramLong, List paramList)
  {
    // Byte code:
    //   0: new 105	java/util/ArrayList
    //   3: astore_3
    //   4: aload_3
    //   5: invokespecial 109	java/util/ArrayList:<init>	()V
    //   8: aload_1
    //   9: ifnull +433 -> 442
    //   12: aload_1
    //   13: invokevirtual 115	java/lang/Long:longValue	()J
    //   16: lstore 4
    //   18: lconst_0
    //   19: lstore 6
    //   21: lload 4
    //   23: lload 6
    //   25: lcmp
    //   26: istore 8
    //   28: iload 8
    //   30: ifne +6 -> 36
    //   33: goto +409 -> 442
    //   36: iconst_1
    //   37: anewarray 14	java/lang/String
    //   40: astore 9
    //   42: aload_1
    //   43: invokevirtual 118	java/lang/Long:toString	()Ljava/lang/String;
    //   46: astore_1
    //   47: aload 9
    //   49: iconst_0
    //   50: aload_1
    //   51: aastore
    //   52: aconst_null
    //   53: astore_1
    //   54: aload_0
    //   55: invokevirtual 124	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   58: astore 10
    //   60: getstatic 130	android/provider/ContactsContract$Data:CONTENT_URI	Landroid/net/Uri;
    //   63: astore 11
    //   65: getstatic 16	com/truecaller/util/w:a	[Ljava/lang/String;
    //   68: astore 12
    //   70: ldc -124
    //   72: astore 13
    //   74: iconst_0
    //   75: istore 14
    //   77: aconst_null
    //   78: astore 15
    //   80: aload 10
    //   82: aload 11
    //   84: aload 12
    //   86: aload 13
    //   88: aload 9
    //   90: aconst_null
    //   91: invokevirtual 138	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   94: astore_1
    //   95: aload_1
    //   96: ifnonnull +15 -> 111
    //   99: aload_1
    //   100: ifnull +9 -> 109
    //   103: aload_1
    //   104: invokeinterface 143 1 0
    //   109: aload_3
    //   110: areturn
    //   111: aload_0
    //   112: invokevirtual 147	android/content/Context:getPackageManager	()Landroid/content/pm/PackageManager;
    //   115: astore 10
    //   117: aload_1
    //   118: invokeinterface 150 1 0
    //   123: istore 16
    //   125: iload 16
    //   127: ifeq +277 -> 404
    //   130: ldc 10
    //   132: astore 11
    //   134: aload_1
    //   135: aload 11
    //   137: invokeinterface 154 2 0
    //   142: istore 16
    //   144: aload_1
    //   145: iload 16
    //   147: invokeinterface 158 2 0
    //   152: astore 11
    //   154: ldc 12
    //   156: astore 12
    //   158: aload_1
    //   159: aload 12
    //   161: invokeinterface 154 2 0
    //   166: istore 17
    //   168: aload_1
    //   169: iload 17
    //   171: invokeinterface 158 2 0
    //   176: astore 12
    //   178: ldc 8
    //   180: astore 13
    //   182: aload_1
    //   183: aload 13
    //   185: invokeinterface 154 2 0
    //   190: istore 8
    //   192: aload_1
    //   193: iload 8
    //   195: invokeinterface 162 2 0
    //   200: lstore 18
    //   202: aload 11
    //   204: invokestatic 72	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   207: istore 14
    //   209: iload 14
    //   211: ifne -94 -> 117
    //   214: aload 12
    //   216: invokestatic 72	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   219: istore 14
    //   221: iload 14
    //   223: ifne -106 -> 117
    //   226: new 164	android/content/Intent
    //   229: astore 15
    //   231: ldc -90
    //   233: astore 20
    //   235: aload 15
    //   237: aload 20
    //   239: invokespecial 169	android/content/Intent:<init>	(Ljava/lang/String;)V
    //   242: getstatic 130	android/provider/ContactsContract$Data:CONTENT_URI	Landroid/net/Uri;
    //   245: astore 20
    //   247: aload 20
    //   249: lload 18
    //   251: invokestatic 175	android/content/ContentUris:withAppendedId	(Landroid/net/Uri;J)Landroid/net/Uri;
    //   254: astore 13
    //   256: aload 15
    //   258: aload 13
    //   260: aload 12
    //   262: invokevirtual 179	android/content/Intent:setDataAndType	(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    //   265: pop
    //   266: aload 10
    //   268: aload 15
    //   270: iconst_0
    //   271: invokevirtual 185	android/content/pm/PackageManager:queryIntentActivities	(Landroid/content/Intent;I)Ljava/util/List;
    //   274: astore 12
    //   276: aload 12
    //   278: ifnull -161 -> 117
    //   281: aload 12
    //   283: invokeinterface 187 1 0
    //   288: istore 8
    //   290: iload 8
    //   292: ifne -175 -> 117
    //   295: aload 12
    //   297: iconst_0
    //   298: invokeinterface 191 2 0
    //   303: astore 12
    //   305: aload 12
    //   307: checkcast 193	android/content/pm/ResolveInfo
    //   310: astore 12
    //   312: aload 12
    //   314: aload 10
    //   316: invokevirtual 197	android/content/pm/ResolveInfo:loadIcon	(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    //   319: astore 13
    //   321: aload 12
    //   323: getfield 201	android/content/pm/ResolveInfo:activityInfo	Landroid/content/pm/ActivityInfo;
    //   326: astore 12
    //   328: aload 12
    //   330: getfield 207	android/content/pm/ActivityInfo:packageName	Ljava/lang/String;
    //   333: astore 12
    //   335: aload_0
    //   336: invokevirtual 210	android/content/Context:getPackageName	()Ljava/lang/String;
    //   339: astore 9
    //   341: aload 9
    //   343: aload 12
    //   345: invokevirtual 100	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   348: istore 21
    //   350: iload 21
    //   352: ifne -235 -> 117
    //   355: aload_2
    //   356: ifnull +18 -> 374
    //   359: aload_2
    //   360: aload 12
    //   362: invokeinterface 213 2 0
    //   367: istore 21
    //   369: iload 21
    //   371: ifeq -254 -> 117
    //   374: new 215	com/truecaller/data/entity/e
    //   377: astore 9
    //   379: aload 9
    //   381: aload 11
    //   383: aload 13
    //   385: aload 15
    //   387: aload 12
    //   389: invokespecial 218	com/truecaller/data/entity/e:<init>	(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/content/Intent;Ljava/lang/String;)V
    //   392: aload_3
    //   393: aload 9
    //   395: invokeinterface 221 2 0
    //   400: pop
    //   401: goto -284 -> 117
    //   404: aload_1
    //   405: ifnull +9 -> 414
    //   408: aload_1
    //   409: invokeinterface 143 1 0
    //   414: aload_3
    //   415: areturn
    //   416: astore_0
    //   417: aload_1
    //   418: ifnull +9 -> 427
    //   421: aload_1
    //   422: invokeinterface 143 1 0
    //   427: aload_0
    //   428: athrow
    //   429: pop
    //   430: aload_1
    //   431: ifnull +9 -> 440
    //   434: aload_1
    //   435: invokeinterface 143 1 0
    //   440: aload_3
    //   441: areturn
    //   442: aload_3
    //   443: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	444	0	paramContext	Context
    //   0	444	1	paramLong	Long
    //   0	444	2	paramList	List
    //   3	440	3	localArrayList	ArrayList
    //   16	6	4	l1	long
    //   19	5	6	l2	long
    //   26	3	8	bool1	boolean
    //   190	4	8	i	int
    //   288	3	8	bool2	boolean
    //   40	354	9	localObject1	Object
    //   58	257	10	localObject2	Object
    //   63	319	11	localObject3	Object
    //   68	320	12	localObject4	Object
    //   72	312	13	localObject5	Object
    //   75	147	14	bool3	boolean
    //   78	308	15	localIntent	android.content.Intent
    //   123	3	16	bool4	boolean
    //   142	4	16	j	int
    //   166	4	17	k	int
    //   200	50	18	l3	long
    //   233	15	20	localObject6	Object
    //   348	22	21	bool5	boolean
    //   429	1	22	localException	Exception
    // Exception table:
    //   from	to	target	type
    //   54	58	416	finally
    //   60	63	416	finally
    //   65	68	416	finally
    //   90	94	416	finally
    //   111	115	416	finally
    //   117	123	416	finally
    //   135	142	416	finally
    //   145	152	416	finally
    //   159	166	416	finally
    //   169	176	416	finally
    //   183	190	416	finally
    //   193	200	416	finally
    //   202	207	416	finally
    //   214	219	416	finally
    //   226	229	416	finally
    //   237	242	416	finally
    //   242	245	416	finally
    //   249	254	416	finally
    //   260	266	416	finally
    //   270	274	416	finally
    //   281	288	416	finally
    //   297	303	416	finally
    //   305	310	416	finally
    //   314	319	416	finally
    //   321	326	416	finally
    //   328	333	416	finally
    //   335	339	416	finally
    //   343	348	416	finally
    //   360	367	416	finally
    //   374	377	416	finally
    //   387	392	416	finally
    //   393	401	416	finally
    //   54	58	429	java/lang/Exception
    //   60	63	429	java/lang/Exception
    //   65	68	429	java/lang/Exception
    //   90	94	429	java/lang/Exception
    //   111	115	429	java/lang/Exception
    //   117	123	429	java/lang/Exception
    //   135	142	429	java/lang/Exception
    //   145	152	429	java/lang/Exception
    //   159	166	429	java/lang/Exception
    //   169	176	429	java/lang/Exception
    //   183	190	429	java/lang/Exception
    //   193	200	429	java/lang/Exception
    //   202	207	429	java/lang/Exception
    //   214	219	429	java/lang/Exception
    //   226	229	429	java/lang/Exception
    //   237	242	429	java/lang/Exception
    //   242	245	429	java/lang/Exception
    //   249	254	429	java/lang/Exception
    //   260	266	429	java/lang/Exception
    //   270	274	429	java/lang/Exception
    //   281	288	429	java/lang/Exception
    //   297	303	429	java/lang/Exception
    //   305	310	429	java/lang/Exception
    //   314	319	429	java/lang/Exception
    //   321	326	429	java/lang/Exception
    //   328	333	429	java/lang/Exception
    //   335	339	429	java/lang/Exception
    //   343	348	429	java/lang/Exception
    //   360	367	429	java/lang/Exception
    //   374	377	429	java/lang/Exception
    //   387	392	429	java/lang/Exception
    //   393	401	429	java/lang/Exception
  }
  
  public static List a(Contact paramContact)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    if (paramContact != null)
    {
      paramContact = paramContact.y().iterator();
      for (;;)
      {
        boolean bool1 = paramContact.hasNext();
        if (!bool1) {
          break;
        }
        Object localObject = (Link)paramContact.next();
        String str1 = "email";
        String str2 = ((Link)localObject).getService();
        boolean bool2 = str1.equals(str2);
        if (bool2)
        {
          localObject = ((Link)localObject).getInfo();
          bool2 = TextUtils.isEmpty((CharSequence)localObject);
          if (!bool2) {
            localArrayList.add(localObject);
          }
        }
      }
    }
    return localArrayList;
  }
  
  public static void a(Context paramContext, Contact paramContact)
  {
    String str = "";
    Object localObject1 = paramContact.g();
    if (localObject1 != null) {
      str = ((Address)localObject1).getCountryCode();
    }
    boolean bool1 = TextUtils.isEmpty(str);
    if (bool1)
    {
      localObject1 = paramContact.r();
      if (localObject1 != null) {
        str = ((Number)localObject1).l();
      }
    }
    localObject1 = paramContact.t();
    boolean bool2 = TextUtils.isEmpty((CharSequence)localObject1);
    Object localObject2;
    if (bool2)
    {
      localObject2 = paramContact.r();
      if (localObject2 != null) {
        localObject1 = ((Number)localObject2).n();
      }
      bool2 = TextUtils.isEmpty((CharSequence)localObject1);
      if (bool2) {
        localObject1 = paramContact.p();
      }
    }
    boolean bool3 = TextUtils.isEmpty((CharSequence)localObject1);
    if (!bool3) {
      try
      {
        paramContact = new java/lang/StringBuilder;
        localObject2 = "https://www.google.com/search?q=";
        paramContact.<init>((String)localObject2);
        localObject2 = "UTF-8";
        localObject1 = URLEncoder.encode((String)localObject1, (String)localObject2);
        paramContact.append((String)localObject1);
        paramContact = paramContact.toString();
        if (str != null)
        {
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>();
          ((StringBuilder)localObject1).append(paramContact);
          paramContact = "&cr=country";
          ((StringBuilder)localObject1).append(paramContact);
          ((StringBuilder)localObject1).append(str);
          paramContact = ((StringBuilder)localObject1).toString();
        }
        str = null;
        dh.a(paramContext, paramContact, false);
        return;
      }
      catch (UnsupportedEncodingException paramContext)
      {
        AssertionUtil.reportThrowableButNeverCrash(paramContext);
      }
    }
  }
  
  public static void a(Context paramContext, cb paramcb, Contact paramContact)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str = paramContact.t();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append("\r\n");
    str = paramContact.p();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append("\r\n");
    str = paramContact.a();
    boolean bool = am.c(str);
    if (bool)
    {
      str = paramContact.a();
      ((StringBuilder)localObject).append(str);
      str = "\r\n";
      ((StringBuilder)localObject).append(str);
    }
    paramContact = paramContact.p();
    paramcb = paramcb.a(paramContact);
    ((StringBuilder)localObject).append(paramcb);
    ((StringBuilder)localObject).append("\r\n\r\n");
    paramcb = paramContext.getString(2131887227);
    ((StringBuilder)localObject).append(paramcb);
    paramcb = ((StringBuilder)localObject).toString();
    paramContact = paramContext.getString(2131887142);
    localObject = paramContext.getString(2131887141);
    ai.a(paramContext, paramContact, (String)localObject, paramcb, null);
  }
  
  public static void a(Contact paramContact, Collection paramCollection)
  {
    if (paramCollection != null)
    {
      paramCollection = paramCollection.iterator();
      for (;;)
      {
        boolean bool1 = paramCollection.hasNext();
        if (!bool1) {
          break;
        }
        String str1 = (String)paramCollection.next();
        boolean bool2 = TextUtils.isEmpty(str1);
        if (!bool2)
        {
          Link localLink = new com/truecaller/data/entity/Link;
          localLink.<init>();
          String str2 = "email";
          localLink.setService(str2);
          localLink.setInfo(str1);
          paramContact.a(localLink);
        }
      }
    }
  }
  
  public static boolean a(Contact paramContact1, Contact paramContact2)
  {
    if (paramContact1 == paramContact2) {
      return true;
    }
    boolean bool = false;
    if ((paramContact1 != null) && (paramContact2 != null))
    {
      Parcel localParcel1 = Parcel.obtain();
      Parcel localParcel2 = Parcel.obtain();
      localParcel1.setDataPosition(0);
      localParcel2.setDataPosition(0);
      paramContact1.writeToParcel(localParcel1, 0);
      paramContact2.writeToParcel(localParcel2, 0);
      int i = localParcel1.dataAvail();
      int j = localParcel2.dataAvail();
      if (i == j)
      {
        paramContact1 = localParcel1.marshall();
        paramContact2 = localParcel2.marshall();
        bool = Arrays.equals(paramContact1, paramContact2);
      }
      localParcel1.recycle();
      localParcel2.recycle();
      return bool;
    }
    return false;
  }
  
  public static boolean a(Contact paramContact, g paramg)
  {
    FilterManager.ActionSource localActionSource1 = j;
    FilterManager.ActionSource localActionSource2 = FilterManager.ActionSource.CUSTOM_WHITELIST;
    if (localActionSource1 != localActionSource2)
    {
      if (paramContact != null)
      {
        boolean bool = paramContact.U();
        if (bool) {}
      }
      else
      {
        paramContact = j;
        localActionSource1 = FilterManager.ActionSource.CUSTOM_BLACKLIST;
        if (paramContact != localActionSource1)
        {
          paramContact = j;
          paramg = FilterManager.ActionSource.TOP_SPAMMER;
          if (paramContact != paramg) {
            break label59;
          }
        }
      }
      return true;
    }
    label59:
    return false;
  }
  
  private static boolean a(Number paramNumber1, Number paramNumber2)
  {
    if ((paramNumber1 != null) && (paramNumber2 != null))
    {
      String str1 = paramNumber1.a();
      String str2 = paramNumber2.a();
      boolean bool1 = ab.a(str1, str2);
      if (!bool1)
      {
        paramNumber1 = paramNumber1.d();
        paramNumber2 = paramNumber2.d();
        boolean bool2 = ab.a(paramNumber1, paramNumber2);
        if (!bool2) {
          return false;
        }
      }
      return true;
    }
    return false;
  }
  
  public static int b(Contact paramContact, Number paramNumber)
  {
    int i = 0;
    if ((paramContact != null) && (paramNumber != null))
    {
      paramContact = paramContact.A().iterator();
      for (;;)
      {
        boolean bool1 = paramContact.hasNext();
        if (!bool1) {
          break;
        }
        Number localNumber = (Number)paramContact.next();
        boolean bool2 = a(paramNumber, localNumber);
        if (bool2)
        {
          int j = localNumber.h();
          i = Math.max(i, j);
        }
      }
    }
    return i;
  }
  
  public static Number b(Contact paramContact)
  {
    if (paramContact != null)
    {
      boolean bool1 = true;
      Number localNumber = a(paramContact, bool1);
      if (localNumber != null)
      {
        String str = localNumber.a();
        boolean bool2 = TextUtils.isEmpty(str);
        if (bool2)
        {
          str = localNumber.d();
          bool2 = TextUtils.isEmpty(str);
          if (!bool2) {}
        }
      }
      else
      {
        bool1 = false;
        localNumber = a(paramContact, false);
      }
      return localNumber;
    }
    return null;
  }
  
  public static List c(Contact paramContact)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    if (paramContact != null)
    {
      paramContact = paramContact.y().iterator();
      for (;;)
      {
        boolean bool1 = paramContact.hasNext();
        if (!bool1) {
          break;
        }
        Link localLink = (Link)paramContact.next();
        String str1 = "email";
        String str2 = localLink.getService();
        boolean bool2 = str1.equals(str2);
        if (!bool2)
        {
          str1 = "link";
          str2 = localLink.getService();
          bool2 = str1.equals(str2);
          if (!bool2) {
            localArrayList.add(localLink);
          }
        }
      }
    }
    return localArrayList;
  }
  
  public static String d(Contact paramContact)
  {
    if (paramContact != null)
    {
      paramContact = paramContact.y().iterator();
      Link localLink;
      boolean bool2;
      do
      {
        boolean bool1 = paramContact.hasNext();
        if (!bool1) {
          break;
        }
        localLink = (Link)paramContact.next();
        String str1 = "link";
        String str2 = localLink.getService();
        bool2 = str1.equals(str2);
      } while (!bool2);
      return localLink.getInfo();
    }
    return null;
  }
  
  public static List e(Contact paramContact)
  {
    Object localObject1 = paramContact.i;
    if (localObject1 != null)
    {
      localObject1 = paramContact.i.getOpeningHours();
      if (localObject1 != null)
      {
        paramContact = paramContact.i.getOpeningHours().split("\\|");
        localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>();
        int i = paramContact.length;
        int j = 0;
        while (j < i)
        {
          Object localObject2 = paramContact[j];
          boolean bool1 = am.d((CharSequence)localObject2);
          if (!bool1)
          {
            Object localObject3 = " ";
            localObject2 = ((String)localObject2).split((String)localObject3);
            int k = localObject2.length;
            int m = 3;
            if (k != m) {
              return Collections.emptyList();
            }
            localObject3 = new java/util/TreeSet;
            ((TreeSet)localObject3).<init>();
            Object localObject4 = localObject2[0].toCharArray();
            int n = localObject4.length;
            int i1 = 0;
            while (i1 < n)
            {
              char c = localObject4[i1];
              boolean bool2 = Character.isDigit(c);
              if (bool2)
              {
                int i2 = Character.digit(c, 10);
                int i3 = -1;
                if (i2 == i3) {
                  return Collections.emptyList();
                }
                Integer localInteger = Integer.valueOf(i2);
                ((SortedSet)localObject3).add(localInteger);
              }
              i1 += 1;
            }
            localObject4 = new com/truecaller/profile/data/dto/OpenHours;
            n = 1;
            String str = localObject2[n];
            i1 = 2;
            localObject2 = localObject2[i1];
            ((OpenHours)localObject4).<init>((SortedSet)localObject3, str, (String)localObject2);
            ((List)localObject1).add(localObject4);
          }
          j += 1;
        }
        return (List)localObject1;
      }
    }
    return Collections.emptyList();
  }
  
  public static List f(Contact paramContact)
  {
    Object localObject1 = paramContact.j;
    if (localObject1 != null)
    {
      localObject1 = paramContact.j.getImageUrls();
      if (localObject1 != null)
      {
        paramContact = paramContact.j.getImageUrls().split("\\|");
        localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>();
        int i = paramContact.length;
        int j = 0;
        while (j < i)
        {
          Object localObject2 = paramContact[j];
          boolean bool = am.d((CharSequence)localObject2);
          if (!bool)
          {
            String str = "UTF-8";
            try
            {
              localObject2 = URLDecoder.decode((String)localObject2, str);
              ((List)localObject1).add(localObject2);
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException)
            {
              AssertionUtil.reportThrowableButNeverCrash(localUnsupportedEncodingException);
            }
          }
          j += 1;
        }
        return (List)localObject1;
      }
    }
    return Collections.emptyList();
  }
  
  public static int g(Contact paramContact)
  {
    return a(f);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */