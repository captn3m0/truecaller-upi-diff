package com.truecaller.util;

import android.net.Uri;
import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class bb$b
  extends u
{
  private final Uri b;
  private final boolean c;
  
  private bb$b(e parame, Uri paramUri, boolean paramBoolean)
  {
    super(parame);
    b = paramUri;
    c = paramBoolean;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".audioEntityFromFile(");
    String str = a(b, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(Boolean.valueOf(c), 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bb.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */