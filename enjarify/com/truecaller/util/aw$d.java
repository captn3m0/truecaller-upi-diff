package com.truecaller.util;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import com.truecaller.log.d;
import com.truecaller.old.a.a;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class aw$d
  extends a
{
  private final Uri a;
  private final Uri c;
  private final ContentResolver d;
  
  public aw$d(Context paramContext, Uri paramUri1, Uri paramUri2)
  {
    a = paramUri1;
    c = paramUri2;
    paramContext = paramContext.getContentResolver();
    d = paramContext;
  }
  
  public final void a(Object paramObject) {}
  
  protected Object doInBackground(Object... paramVarArgs)
  {
    try
    {
      paramVarArgs = d;
      Object localObject1 = a;
      paramVarArgs = paramVarArgs.openInputStream((Uri)localObject1);
      localObject1 = d;
      Object localObject2 = c;
      localObject1 = ((ContentResolver)localObject1).openOutputStream((Uri)localObject2);
      int i = 8192;
      localObject2 = new byte[i];
      for (;;)
      {
        int j = paramVarArgs.read((byte[])localObject2);
        int k = -1;
        if (j == k) {
          break;
        }
        k = 0;
        ((OutputStream)localObject1).write((byte[])localObject2, 0, j);
      }
      paramVarArgs.close();
      ((OutputStream)localObject1).close();
    }
    catch (IOException paramVarArgs)
    {
      d.a(paramVarArgs);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.aw.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */