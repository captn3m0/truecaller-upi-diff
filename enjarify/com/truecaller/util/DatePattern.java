package com.truecaller.util;

public enum DatePattern
{
  static
  {
    DatePattern[] arrayOfDatePattern = new DatePattern[2];
    DatePattern localDatePattern = new com/truecaller/util/DatePattern;
    localDatePattern.<init>("GROUP_HEADER_WITH_YEAR", 0);
    GROUP_HEADER_WITH_YEAR = localDatePattern;
    arrayOfDatePattern[0] = localDatePattern;
    localDatePattern = new com/truecaller/util/DatePattern;
    int i = 1;
    localDatePattern.<init>("GROUP_HEADER_WITHOUT_YEAR", i);
    GROUP_HEADER_WITHOUT_YEAR = localDatePattern;
    arrayOfDatePattern[i] = localDatePattern;
    $VALUES = arrayOfDatePattern;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.DatePattern
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */