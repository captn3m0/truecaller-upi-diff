package com.truecaller.util;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.text.TextUtils;
import com.google.c.a.k.d;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.h.ab;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.log.d;
import com.truecaller.premium.b.a;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class t
{
  private static final String a;
  private static final String b;
  private static final String[] c;
  private static final String[] d;
  private static final String[] e;
  private static final String[] f;
  private static final String[] g = tmp122_108;
  
  static
  {
    String[] tmp4_1 = new String[3];
    String[] tmp5_4 = tmp4_1;
    String[] tmp5_4 = tmp4_1;
    tmp5_4[0] = "phone";
    tmp5_4[1] = "secondary_phone";
    tmp5_4[2] = "tertiary_phone";
    c = tmp5_4;
    String[] tmp25_22 = new String[3];
    String[] tmp26_25 = tmp25_22;
    String[] tmp26_25 = tmp25_22;
    tmp26_25[0] = "phone_type";
    tmp26_25[1] = "secondary_phone_type";
    tmp26_25[2] = "tertiary_phone_type";
    d = tmp26_25;
    String[] tmp46_43 = new String[3];
    String[] tmp47_46 = tmp46_43;
    String[] tmp47_46 = tmp46_43;
    tmp47_46[0] = "email";
    tmp47_46[1] = "secondary_email";
    tmp47_46[2] = "tertiary_email";
    e = tmp47_46;
    String[] tmp67_64 = new String[3];
    String[] tmp68_67 = tmp67_64;
    String[] tmp68_67 = tmp67_64;
    tmp68_67[0] = "email_type";
    tmp68_67[1] = "secondary_email_type";
    tmp68_67[2] = "tertiary_email_type";
    f = tmp68_67;
    String[] tmp89_86 = new String[8];
    String[] tmp90_89 = tmp89_86;
    String[] tmp90_89 = tmp89_86;
    tmp90_89[0] = "data1";
    tmp90_89[1] = "data4";
    String[] tmp99_90 = tmp90_89;
    String[] tmp99_90 = tmp90_89;
    tmp99_90[2] = "data5";
    tmp99_90[3] = "data6";
    String[] tmp108_99 = tmp99_90;
    String[] tmp108_99 = tmp99_90;
    tmp108_99[4] = "data7";
    tmp108_99[5] = "data8";
    tmp108_99[6] = "data9";
    String[] tmp122_108 = tmp108_99;
    tmp122_108[7] = "data10";
  }
  
  public static Intent a(Contact paramContact, byte[] paramArrayOfByte)
  {
    a locala = TrueApp.y().a().bG();
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.INSERT_OR_EDIT");
    localIntent.setType("vnd.android.cursor.item/contact");
    localIntent.addCategory("android.intent.category.DEFAULT");
    int i = 1;
    localIntent.putExtra("finishActivityOnSaveCompleted", i);
    Object localObject1 = a;
    localIntent.putExtra("account_type", (String)localObject1);
    localObject1 = b;
    localIntent.putExtra("account_name", (String)localObject1);
    Object localObject2 = paramContact.A();
    localObject1 = c;
    int j = localObject1.length;
    int m = ((List)localObject2).size();
    j = Math.min(j, m);
    m = 0;
    String str1 = null;
    int n = 0;
    String str2 = null;
    Object localObject3;
    while (n < j)
    {
      localObject3 = (Number)((List)localObject2).get(n);
      String str3 = ((Number)localObject3).a();
      boolean bool2 = TextUtils.isEmpty(str3);
      if (bool2)
      {
        str3 = ((Number)localObject3).d();
        bool2 = TextUtils.isEmpty(str3);
        if (bool2) {
          str3 = ((Number)localObject3).c();
        }
      }
      String str4 = c[n];
      localIntent.putExtra(str4, str3);
      int i1 = ((Number)localObject3).i();
      localObject3 = ((Number)localObject3).m();
      int i2 = ab.a(i1, (k.d)localObject3);
      str3 = d[n];
      if (i2 == 0) {
        i2 = 7;
      }
      localIntent.putExtra(str3, i2);
      n += 1;
    }
    boolean bool3 = locala.a(paramContact);
    if (!bool3)
    {
      localObject2 = w.a(paramContact);
      localObject1 = e;
      j = localObject1.length;
      n = ((List)localObject2).size();
      j = Math.min(j, n);
      while (m < j)
      {
        str2 = e[m];
        localObject3 = (String)((List)localObject2).get(m);
        localIntent.putExtra(str2, (String)localObject3);
        str2 = f[m];
        localIntent.putExtra(str2, i);
        m += 1;
      }
    }
    bool3 = locala.c(paramContact);
    if (!bool3)
    {
      localObject2 = "notes";
      localObject1 = paramContact.h();
      a(localIntent, (String)localObject2, (String)localObject1);
    }
    bool3 = locala.b(paramContact);
    if (!bool3)
    {
      localObject1 = paramContact.x();
      a(localIntent, "job_title", (String)localObject1);
      localObject2 = "company";
      localObject1 = paramContact.o();
      a(localIntent, (String)localObject2, (String)localObject1);
    }
    localObject2 = "name";
    boolean bool1 = paramContact.P();
    if (bool1) {
      localObject1 = paramContact.z();
    } else {
      localObject1 = "";
    }
    a(localIntent, (String)localObject2, (String)localObject1);
    if (paramArrayOfByte != null)
    {
      int i3 = paramArrayOfByte.length;
      int k = 358400;
      if (i3 > k)
      {
        bool5 = false;
        paramArrayOfByte = null;
      }
      else
      {
        localObject2 = new java/util/ArrayList;
        ((ArrayList)localObject2).<init>();
        localObject1 = new android/content/ContentValues;
        ((ContentValues)localObject1).<init>();
        str2 = "vnd.android.cursor.item/photo";
        ((ContentValues)localObject1).put("mimetype", str2);
        str1 = "data15";
        ((ContentValues)localObject1).put(str1, paramArrayOfByte);
        ((ArrayList)localObject2).add(localObject1);
        paramArrayOfByte = (byte[])localObject2;
      }
      if (paramArrayOfByte != null)
      {
        boolean bool4 = paramArrayOfByte.isEmpty();
        if (!bool4)
        {
          localObject2 = "data";
          localIntent.putParcelableArrayListExtra((String)localObject2, paramArrayOfByte);
        }
      }
    }
    boolean bool5 = locala.a(paramContact, i);
    if (!bool5)
    {
      paramArrayOfByte = paramContact.b();
      bool5 = TextUtils.isEmpty(paramArrayOfByte);
      if (!bool5)
      {
        paramArrayOfByte = "postal";
        paramContact = paramContact.b();
        localIntent.putExtra(paramArrayOfByte, paramContact);
      }
    }
    return localIntent;
  }
  
  /* Error */
  public static String a(Context paramContext, String paramString)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_0
    //   3: ifnull +248 -> 251
    //   6: aload_1
    //   7: invokestatic 259	com/truecaller/common/h/am:a	(Ljava/lang/CharSequence;)Z
    //   10: istore_3
    //   11: iload_3
    //   12: ifne +6 -> 18
    //   15: goto +236 -> 251
    //   18: getstatic 265	android/provider/ContactsContract$CommonDataKinds$Phone:CONTENT_URI	Landroid/net/Uri;
    //   21: astore 4
    //   23: iconst_2
    //   24: anewarray 21	java/lang/String
    //   27: dup
    //   28: iconst_0
    //   29: ldc_w 267
    //   32: aastore
    //   33: dup
    //   34: iconst_1
    //   35: ldc 49
    //   37: aastore
    //   38: astore 5
    //   40: iconst_0
    //   41: istore_3
    //   42: iconst_0
    //   43: anewarray 21	java/lang/String
    //   46: astore 6
    //   48: aload_0
    //   49: invokevirtual 273	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   52: astore 7
    //   54: ldc -43
    //   56: astore 8
    //   58: aload 7
    //   60: aload 4
    //   62: aload 5
    //   64: aload 8
    //   66: aload 6
    //   68: aconst_null
    //   69: invokevirtual 279	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   72: astore_0
    //   73: aload_0
    //   74: ifnull +168 -> 242
    //   77: aload_0
    //   78: invokeinterface 284 1 0
    //   83: istore 9
    //   85: iload 9
    //   87: ifeq +155 -> 242
    //   90: ldc_w 267
    //   93: astore 7
    //   95: aload_0
    //   96: aload 7
    //   98: invokeinterface 288 2 0
    //   103: istore 9
    //   105: aload_0
    //   106: iload 9
    //   108: invokeinterface 292 2 0
    //   113: astore 7
    //   115: ldc 49
    //   117: astore 4
    //   119: aload_0
    //   120: aload 4
    //   122: invokeinterface 288 2 0
    //   127: istore 10
    //   129: aload_0
    //   130: iload 10
    //   132: invokeinterface 292 2 0
    //   137: astore 4
    //   139: aload 4
    //   141: aload_1
    //   142: invokestatic 295	com/truecaller/common/h/ab:a	(Ljava/lang/String;Ljava/lang/String;)Z
    //   145: istore 10
    //   147: iload 10
    //   149: ifeq -72 -> 77
    //   152: aload 7
    //   154: astore_2
    //   155: goto +87 -> 242
    //   158: astore_1
    //   159: goto +71 -> 230
    //   162: astore_1
    //   163: aload_1
    //   164: invokestatic 300	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   167: iconst_1
    //   168: istore 9
    //   170: iload 9
    //   172: anewarray 21	java/lang/String
    //   175: astore 7
    //   177: new 302	java/lang/StringBuilder
    //   180: astore 4
    //   182: ldc_w 304
    //   185: astore 5
    //   187: aload 4
    //   189: aload 5
    //   191: invokespecial 305	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   194: aload_1
    //   195: invokevirtual 310	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   198: astore_1
    //   199: aload 4
    //   201: aload_1
    //   202: invokevirtual 314	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   205: pop
    //   206: aload 4
    //   208: invokevirtual 317	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   211: astore_1
    //   212: aload 7
    //   214: iconst_0
    //   215: aload_1
    //   216: aastore
    //   217: aload_0
    //   218: ifnull +31 -> 249
    //   221: aload_0
    //   222: invokeinterface 320 1 0
    //   227: goto +22 -> 249
    //   230: aload_0
    //   231: ifnull +9 -> 240
    //   234: aload_0
    //   235: invokeinterface 320 1 0
    //   240: aload_1
    //   241: athrow
    //   242: aload_0
    //   243: ifnull +6 -> 249
    //   246: goto -25 -> 221
    //   249: aload_2
    //   250: areturn
    //   251: aconst_null
    //   252: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	253	0	paramContext	Context
    //   0	253	1	paramString	String
    //   1	249	2	localObject1	Object
    //   10	32	3	bool1	boolean
    //   21	186	4	localObject2	Object
    //   38	152	5	localObject3	Object
    //   46	21	6	arrayOfString	String[]
    //   52	161	7	localObject4	Object
    //   56	9	8	str	String
    //   83	3	9	bool2	boolean
    //   103	68	9	i	int
    //   127	4	10	j	int
    //   145	3	10	bool3	boolean
    // Exception table:
    //   from	to	target	type
    //   77	83	158	finally
    //   96	103	158	finally
    //   106	113	158	finally
    //   120	127	158	finally
    //   130	137	158	finally
    //   141	145	158	finally
    //   163	167	158	finally
    //   170	175	158	finally
    //   177	180	158	finally
    //   189	194	158	finally
    //   194	198	158	finally
    //   201	206	158	finally
    //   206	211	158	finally
    //   215	217	158	finally
    //   77	83	162	java/lang/Exception
    //   96	103	162	java/lang/Exception
    //   106	113	162	java/lang/Exception
    //   120	127	162	java/lang/Exception
    //   130	137	162	java/lang/Exception
    //   141	145	162	java/lang/Exception
  }
  
  /* Error */
  public static List a(Context paramContext)
  {
    // Byte code:
    //   0: new 217	java/util/ArrayList
    //   3: astore_1
    //   4: aload_1
    //   5: invokespecial 220	java/util/ArrayList:<init>	()V
    //   8: new 322	com/truecaller/old/data/access/e
    //   11: astore_2
    //   12: aload_2
    //   13: aload_0
    //   14: invokespecial 325	com/truecaller/old/data/access/e:<init>	(Landroid/content/Context;)V
    //   17: iconst_1
    //   18: anewarray 21	java/lang/String
    //   21: dup
    //   22: iconst_0
    //   23: ldc_w 327
    //   26: aastore
    //   27: astore_3
    //   28: iconst_2
    //   29: anewarray 21	java/lang/String
    //   32: dup
    //   33: iconst_0
    //   34: ldc_w 329
    //   37: aastore
    //   38: dup
    //   39: iconst_1
    //   40: ldc_w 331
    //   43: aastore
    //   44: astore 4
    //   46: aload_0
    //   47: invokevirtual 273	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   50: astore 5
    //   52: getstatic 334	android/provider/ContactsContract$Contacts:CONTENT_URI	Landroid/net/Uri;
    //   55: astore 6
    //   57: ldc_w 336
    //   60: astore 7
    //   62: aconst_null
    //   63: astore 8
    //   65: aload 5
    //   67: aload 6
    //   69: aload 4
    //   71: aload 7
    //   73: aload_3
    //   74: aconst_null
    //   75: invokevirtual 279	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   78: astore 9
    //   80: aload 9
    //   82: ifnull +327 -> 409
    //   85: aconst_null
    //   86: astore 5
    //   88: iconst_1
    //   89: istore 10
    //   91: aload 9
    //   93: invokeinterface 284 1 0
    //   98: istore 11
    //   100: iload 11
    //   102: ifeq +307 -> 409
    //   105: new 338	com/truecaller/old/data/a/b
    //   108: astore 4
    //   110: aload 4
    //   112: invokespecial 339	com/truecaller/old/data/a/b:<init>	()V
    //   115: ldc_w 329
    //   118: astore 7
    //   120: aload 9
    //   122: aload 7
    //   124: invokeinterface 288 2 0
    //   129: istore 12
    //   131: aload 9
    //   133: iload 12
    //   135: invokeinterface 343 2 0
    //   140: lstore 13
    //   142: aload 4
    //   144: lload 13
    //   146: putfield 346	com/truecaller/old/data/a/b:a	J
    //   149: ldc_w 331
    //   152: astore 7
    //   154: aload 9
    //   156: aload 7
    //   158: invokeinterface 288 2 0
    //   163: istore 12
    //   165: aload 9
    //   167: iload 12
    //   169: invokeinterface 292 2 0
    //   174: astore 7
    //   176: aload 4
    //   178: aload 7
    //   180: putfield 347	com/truecaller/old/data/a/b:b	Ljava/lang/String;
    //   183: aload 4
    //   185: getfield 350	com/truecaller/old/data/a/a:a	J
    //   188: lstore 13
    //   190: lload 13
    //   192: invokestatic 354	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   195: astore 7
    //   197: aload_0
    //   198: aload 7
    //   200: invokestatic 357	com/truecaller/util/t:b	(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    //   203: astore 7
    //   205: aload 4
    //   207: aload 7
    //   209: putfield 360	com/truecaller/old/data/a/b:d	Ljava/util/List;
    //   212: aload 4
    //   214: getfield 350	com/truecaller/old/data/a/a:a	J
    //   217: lstore 13
    //   219: lload 13
    //   221: invokestatic 354	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   224: astore 7
    //   226: aload_0
    //   227: aload 7
    //   229: invokestatic 362	com/truecaller/util/t:c	(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    //   232: astore 7
    //   234: aload 4
    //   236: aload 7
    //   238: putfield 364	com/truecaller/old/data/a/b:e	Ljava/util/List;
    //   241: aload 4
    //   243: getfield 346	com/truecaller/old/data/a/b:a	J
    //   246: lstore 13
    //   248: aload_2
    //   249: lload 13
    //   251: invokevirtual 367	com/truecaller/old/data/access/e:a	(J)Lcom/truecaller/old/data/entity/c;
    //   254: astore 7
    //   256: aload 4
    //   258: aload 7
    //   260: putfield 370	com/truecaller/old/data/a/b:c	Lcom/truecaller/old/data/entity/c;
    //   263: goto +60 -> 323
    //   266: astore 7
    //   268: aload 7
    //   270: invokestatic 300	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   273: iload 10
    //   275: anewarray 21	java/lang/String
    //   278: astore_3
    //   279: new 302	java/lang/StringBuilder
    //   282: astore 8
    //   284: ldc_w 372
    //   287: astore 15
    //   289: aload 8
    //   291: aload 15
    //   293: invokespecial 305	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   296: aload 7
    //   298: invokevirtual 310	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   301: astore 7
    //   303: aload 8
    //   305: aload 7
    //   307: invokevirtual 314	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   310: pop
    //   311: aload 8
    //   313: invokevirtual 317	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   316: astore 7
    //   318: aload_3
    //   319: iconst_0
    //   320: aload 7
    //   322: aastore
    //   323: aload_1
    //   324: aload 4
    //   326: invokevirtual 240	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   329: pop
    //   330: goto -245 -> 85
    //   333: astore_0
    //   334: goto +61 -> 395
    //   337: astore_0
    //   338: aload_0
    //   339: invokestatic 300	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   342: iload 10
    //   344: anewarray 21	java/lang/String
    //   347: astore_2
    //   348: new 302	java/lang/StringBuilder
    //   351: astore 6
    //   353: ldc_w 374
    //   356: astore 4
    //   358: aload 6
    //   360: aload 4
    //   362: invokespecial 305	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   365: aload_0
    //   366: invokevirtual 310	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   369: astore_0
    //   370: aload 6
    //   372: aload_0
    //   373: invokevirtual 314	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   376: pop
    //   377: aload 6
    //   379: invokevirtual 317	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   382: astore_0
    //   383: aload_2
    //   384: iconst_0
    //   385: aload_0
    //   386: aastore
    //   387: aload 9
    //   389: ifnull +32 -> 421
    //   392: goto +22 -> 414
    //   395: aload 9
    //   397: ifnull +10 -> 407
    //   400: aload 9
    //   402: invokeinterface 320 1 0
    //   407: aload_0
    //   408: athrow
    //   409: aload 9
    //   411: ifnull +10 -> 421
    //   414: aload 9
    //   416: invokeinterface 320 1 0
    //   421: aload_1
    //   422: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	423	0	paramContext	Context
    //   3	419	1	localArrayList	ArrayList
    //   11	373	2	localObject1	Object
    //   27	292	3	arrayOfString	String[]
    //   44	317	4	localObject2	Object
    //   50	37	5	localContentResolver	ContentResolver
    //   55	323	6	localObject3	Object
    //   60	199	7	localObject4	Object
    //   266	31	7	localException	Exception
    //   301	20	7	str1	String
    //   63	249	8	localStringBuilder	StringBuilder
    //   78	337	9	localCursor	Cursor
    //   89	254	10	i	int
    //   98	3	11	bool	boolean
    //   129	39	12	j	int
    //   140	110	13	l	long
    //   287	5	15	str2	String
    // Exception table:
    //   from	to	target	type
    //   241	246	266	java/lang/Exception
    //   249	254	266	java/lang/Exception
    //   258	263	266	java/lang/Exception
    //   91	98	333	finally
    //   105	108	333	finally
    //   110	115	333	finally
    //   122	129	333	finally
    //   133	140	333	finally
    //   144	149	333	finally
    //   156	163	333	finally
    //   167	174	333	finally
    //   178	183	333	finally
    //   183	188	333	finally
    //   190	195	333	finally
    //   198	203	333	finally
    //   207	212	333	finally
    //   212	217	333	finally
    //   219	224	333	finally
    //   227	232	333	finally
    //   236	241	333	finally
    //   241	246	333	finally
    //   249	254	333	finally
    //   258	263	333	finally
    //   268	273	333	finally
    //   273	278	333	finally
    //   279	282	333	finally
    //   291	296	333	finally
    //   296	301	333	finally
    //   305	311	333	finally
    //   311	316	333	finally
    //   320	323	333	finally
    //   324	330	333	finally
    //   338	342	333	finally
    //   342	347	333	finally
    //   348	351	333	finally
    //   360	365	333	finally
    //   365	369	333	finally
    //   372	377	333	finally
    //   377	382	333	finally
    //   385	387	333	finally
    //   91	98	337	java/lang/Exception
    //   105	108	337	java/lang/Exception
    //   110	115	337	java/lang/Exception
    //   122	129	337	java/lang/Exception
    //   133	140	337	java/lang/Exception
    //   144	149	337	java/lang/Exception
    //   156	163	337	java/lang/Exception
    //   167	174	337	java/lang/Exception
    //   178	183	337	java/lang/Exception
    //   183	188	337	java/lang/Exception
    //   190	195	337	java/lang/Exception
    //   198	203	337	java/lang/Exception
    //   207	212	337	java/lang/Exception
    //   212	217	337	java/lang/Exception
    //   219	224	337	java/lang/Exception
    //   227	232	337	java/lang/Exception
    //   236	241	337	java/lang/Exception
    //   268	273	337	java/lang/Exception
    //   273	278	337	java/lang/Exception
    //   279	282	337	java/lang/Exception
    //   291	296	337	java/lang/Exception
    //   296	301	337	java/lang/Exception
    //   305	311	337	java/lang/Exception
    //   311	316	337	java/lang/Exception
    //   320	323	337	java/lang/Exception
    //   324	330	337	java/lang/Exception
  }
  
  public static void a(Context paramContext, long paramLong, Contact paramContact)
  {
    long l = 0L;
    boolean bool1 = paramLong < l;
    if (bool1)
    {
      paramContact = paramContact.d();
      boolean bool2 = paramContact.isEmpty();
      if (!bool2)
      {
        ArrayList localArrayList = new java/util/ArrayList;
        int i = paramContact.size();
        localArrayList.<init>(i);
        paramContact = paramContact.iterator();
        for (;;)
        {
          boolean bool3 = paramContact.hasNext();
          if (!bool3) {
            break;
          }
          Object localObject1 = (Address)paramContact.next();
          ContentValues localContentValues = new android/content/ContentValues;
          localContentValues.<init>();
          Object localObject2 = Long.valueOf(paramLong);
          localContentValues.put("raw_contact_id", (Long)localObject2);
          localContentValues.put("mimetype", "vnd.android.cursor.item/postal-address_v2");
          localObject2 = ((Address)localObject1).getCity();
          localContentValues.put("data7", (String)localObject2);
          localObject2 = ((Address)localObject1).getCountryName();
          localContentValues.put("data10", (String)localObject2);
          localObject2 = ((Address)localObject1).getStreet();
          localContentValues.put("data4", (String)localObject2);
          Object localObject3 = "data9";
          localObject2 = ((Address)localObject1).getZipCode();
          localContentValues.put((String)localObject3, (String)localObject2);
          int j = ((Address)localObject1).getType();
          switch (j)
          {
          default: 
            break;
          case 1: 
          case 2: 
          case 3: 
            localObject3 = "data2";
            localObject1 = ((Address)localObject1).getTcId();
            localContentValues.put((String)localObject3, (String)localObject1);
            break;
          case 0: 
            localObject3 = ((Address)localObject1).getTypeLabel();
            int k = TextUtils.isEmpty((CharSequence)localObject3);
            if (k != 0)
            {
              localObject1 = "data2";
              k = 1;
              localObject3 = Integer.valueOf(k);
              localContentValues.put((String)localObject1, (Integer)localObject3);
            }
            else
            {
              localObject2 = Integer.valueOf(0);
              localContentValues.put("data2", (Integer)localObject2);
              localObject3 = "data3";
              localObject1 = ((Address)localObject1).getTypeLabel();
              localContentValues.put((String)localObject3, (String)localObject1);
            }
            break;
          }
          localObject1 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValues(localContentValues).build();
          localArrayList.add(localObject1);
        }
        try
        {
          paramContext = paramContext.getContentResolver();
          String str = "com.android.contacts";
          paramContext.applyBatch(str, localArrayList);
          return;
        }
        catch (OperationApplicationException paramContext) {}catch (RemoteException paramContext) {}catch (RuntimeException paramContext) {}
        d.a(paramContext);
      }
    }
  }
  
  private static void a(Intent paramIntent, String paramString1, String paramString2)
  {
    boolean bool = TextUtils.isEmpty(paramString2);
    if (!bool) {
      paramIntent.putExtra(paramString1, paramString2);
    }
  }
  
  public static boolean a(ContentResolver paramContentResolver, Bitmap paramBitmap, long paramLong)
  {
    int i = 0;
    if (paramBitmap == null)
    {
      new String[1][0] = "setPhotoByRawContactId - no bitmap to work with, returning.";
      return false;
    }
    Object localObject1 = new java/io/ByteArrayOutputStream;
    ((ByteArrayOutputStream)localObject1).<init>();
    Object localObject2 = Bitmap.CompressFormat.PNG;
    int j = 100;
    paramBitmap.compress((Bitmap.CompressFormat)localObject2, j, (OutputStream)localObject1);
    paramBitmap = ((ByteArrayOutputStream)localObject1).toByteArray();
    localObject2 = ContactsContract.Data.CONTENT_URI;
    Object localObject3 = { "_id" };
    Object localObject4 = "raw_contact_id=? AND mimetype=?";
    int k = 2;
    String[] arrayOfString1 = new String[k];
    localObject1 = String.valueOf(paramLong);
    arrayOfString1[0] = localObject1;
    int m = 1;
    arrayOfString1[m] = "vnd.android.cursor.item/photo";
    localObject1 = paramContentResolver;
    localObject1 = paramContentResolver.query((Uri)localObject2, (String[])localObject3, (String)localObject4, arrayOfString1, null);
    if (localObject1 != null)
    {
      localObject2 = "_id";
      try
      {
        n = ((Cursor)localObject1).getColumnIndexOrThrow((String)localObject2);
        boolean bool = ((Cursor)localObject1).moveToFirst();
        if (bool)
        {
          n = ((Cursor)localObject1).getInt(n);
        }
        else
        {
          n = 0;
          localObject2 = null;
        }
      }
      finally
      {
        ((Cursor)localObject1).close();
      }
    }
    int n = 0;
    localObject2 = null;
    localObject1 = new android/content/ContentValues;
    ((ContentValues)localObject1).<init>();
    localObject4 = Long.valueOf(paramLong);
    ((ContentValues)localObject1).put("raw_contact_id", (Long)localObject4);
    localObject4 = Integer.valueOf(m);
    ((ContentValues)localObject1).put("is_super_primary", (Integer)localObject4);
    ((ContentValues)localObject1).put("data15", paramBitmap);
    ((ContentValues)localObject1).put("mimetype", "vnd.android.cursor.item/photo");
    paramBitmap = new String[m];
    localObject3 = new java/lang/StringBuilder;
    localObject4 = "found photoRow for rawContactId ";
    ((StringBuilder)localObject3).<init>((String)localObject4);
    ((StringBuilder)localObject3).append(paramLong);
    ((StringBuilder)localObject3).append(", photoRow: ");
    ((StringBuilder)localObject3).append(n);
    String str = ((StringBuilder)localObject3).toString();
    paramBitmap[0] = str;
    if (n != 0)
    {
      paramBitmap = ContactsContract.Data.CONTENT_URI;
      str = "_id=?";
      String[] arrayOfString2 = new String[m];
      localObject2 = String.valueOf(n);
      arrayOfString2[0] = localObject2;
      int i1 = paramContentResolver.update(paramBitmap, (ContentValues)localObject1, str, arrayOfString2);
      if (m == i1) {
        i = 1;
      }
    }
    if (i == 0)
    {
      paramBitmap = ContactsContract.Data.CONTENT_URI;
      paramContentResolver.insert(paramBitmap, (ContentValues)localObject1);
    }
    return m;
  }
  
  public static boolean a(Context paramContext, long paramLong)
  {
    Uri localUri1 = ContactsContract.Contacts.CONTENT_URI;
    Uri localUri2 = ContentUris.withAppendedId(localUri1, paramLong);
    String[] arrayOfString = { "photo_uri" };
    ContentResolver localContentResolver = paramContext.getContentResolver();
    paramContext = localContentResolver.query(localUri2, arrayOfString, null, null, null);
    Object localObject1 = null;
    if (paramContext != null) {
      try
      {
        boolean bool = paramContext.moveToFirst();
        if (bool)
        {
          bool = paramContext.isNull(0);
          if (!bool)
          {
            String str = paramContext.getString(0);
            bool = TextUtils.isEmpty(str);
            if (!bool) {
              return true;
            }
          }
        }
        return false;
      }
      finally
      {
        paramContext.close();
      }
    }
    return false;
  }
  
  public static Bitmap b(Context paramContext, long paramLong)
  {
    return c(paramContext, paramLong);
  }
  
  /* Error */
  private static ArrayList b(Context paramContext, String paramString)
  {
    // Byte code:
    //   0: ldc 49
    //   2: astore_2
    //   3: ldc_w 397
    //   6: astore_3
    //   7: iconst_3
    //   8: anewarray 21	java/lang/String
    //   11: dup
    //   12: dup2
    //   13: iconst_0
    //   14: aload_2
    //   15: aastore
    //   16: iconst_1
    //   17: ldc_w 424
    //   20: aastore
    //   21: iconst_2
    //   22: aload_3
    //   23: aastore
    //   24: astore 4
    //   26: iconst_1
    //   27: istore 5
    //   29: iload 5
    //   31: anewarray 21	java/lang/String
    //   34: astore 6
    //   36: aload 6
    //   38: iconst_0
    //   39: aload_1
    //   40: aastore
    //   41: aload_0
    //   42: invokevirtual 273	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   45: astore 7
    //   47: getstatic 265	android/provider/ContactsContract$CommonDataKinds$Phone:CONTENT_URI	Landroid/net/Uri;
    //   50: astore 8
    //   52: ldc_w 548
    //   55: astore 9
    //   57: aload 7
    //   59: aload 8
    //   61: aload 4
    //   63: aload 9
    //   65: aload 6
    //   67: aconst_null
    //   68: invokevirtual 279	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   71: astore_0
    //   72: iconst_0
    //   73: istore 10
    //   75: aconst_null
    //   76: astore_1
    //   77: aload_0
    //   78: ifnull +270 -> 348
    //   81: aload_0
    //   82: invokeinterface 551 1 0
    //   87: istore 11
    //   89: iload 11
    //   91: ifle +257 -> 348
    //   94: new 217	java/util/ArrayList
    //   97: astore_3
    //   98: aload_3
    //   99: invokespecial 220	java/util/ArrayList:<init>	()V
    //   102: aload_0
    //   103: invokeinterface 284 1 0
    //   108: istore 10
    //   110: iload 10
    //   112: ifeq +241 -> 353
    //   115: new 553	com/truecaller/old/data/a/c
    //   118: astore_1
    //   119: aload_1
    //   120: invokespecial 554	com/truecaller/old/data/a/c:<init>	()V
    //   123: ldc 49
    //   125: astore 7
    //   127: aload_0
    //   128: aload 7
    //   130: invokeinterface 288 2 0
    //   135: istore 12
    //   137: aload_0
    //   138: iload 12
    //   140: invokeinterface 292 2 0
    //   145: astore 7
    //   147: aload_1
    //   148: aload 7
    //   150: putfield 555	com/truecaller/old/data/a/c:b	Ljava/lang/String;
    //   153: ldc_w 424
    //   156: astore 7
    //   158: aload_0
    //   159: aload 7
    //   161: invokeinterface 288 2 0
    //   166: istore 12
    //   168: aload_0
    //   169: iload 12
    //   171: invokeinterface 507 2 0
    //   176: istore 12
    //   178: iconst_3
    //   179: istore 13
    //   181: iload 12
    //   183: iload 5
    //   185: if_icmpne +9 -> 194
    //   188: iconst_1
    //   189: istore 12
    //   191: goto +19 -> 210
    //   194: iload 12
    //   196: iload 13
    //   198: if_icmpne +9 -> 207
    //   201: iconst_2
    //   202: istore 12
    //   204: goto +6 -> 210
    //   207: iconst_3
    //   208: istore 12
    //   210: aload_1
    //   211: iload 12
    //   213: putfield 559	com/truecaller/old/data/a/c:c	I
    //   216: ldc_w 397
    //   219: astore 7
    //   221: aload_0
    //   222: aload 7
    //   224: invokeinterface 288 2 0
    //   229: istore 12
    //   231: aload_0
    //   232: iload 12
    //   234: invokeinterface 292 2 0
    //   239: astore 7
    //   241: aload_1
    //   242: aload 7
    //   244: putfield 560	com/truecaller/old/data/a/c:a	Ljava/lang/String;
    //   247: aload_3
    //   248: aload_1
    //   249: invokevirtual 240	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   252: pop
    //   253: goto -151 -> 102
    //   256: astore_1
    //   257: goto +17 -> 274
    //   260: astore_1
    //   261: goto +75 -> 336
    //   264: astore 14
    //   266: iconst_0
    //   267: istore 11
    //   269: aconst_null
    //   270: astore_3
    //   271: aload 14
    //   273: astore_1
    //   274: aload_1
    //   275: invokestatic 300	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   278: iload 5
    //   280: anewarray 21	java/lang/String
    //   283: astore_2
    //   284: new 302	java/lang/StringBuilder
    //   287: astore 7
    //   289: ldc_w 562
    //   292: astore 8
    //   294: aload 7
    //   296: aload 8
    //   298: invokespecial 305	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   301: aload_1
    //   302: invokevirtual 310	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   305: astore_1
    //   306: aload 7
    //   308: aload_1
    //   309: invokevirtual 314	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   312: pop
    //   313: aload 7
    //   315: invokevirtual 317	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   318: astore_1
    //   319: aload_2
    //   320: iconst_0
    //   321: aload_1
    //   322: aastore
    //   323: aload_0
    //   324: ifnull +36 -> 360
    //   327: aload_0
    //   328: invokeinterface 320 1 0
    //   333: goto +27 -> 360
    //   336: aload_0
    //   337: ifnull +9 -> 346
    //   340: aload_0
    //   341: invokeinterface 320 1 0
    //   346: aload_1
    //   347: athrow
    //   348: iconst_0
    //   349: istore 11
    //   351: aconst_null
    //   352: astore_3
    //   353: aload_0
    //   354: ifnull +6 -> 360
    //   357: goto -30 -> 327
    //   360: aload_3
    //   361: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	362	0	paramContext	Context
    //   0	362	1	paramString	String
    //   2	318	2	localObject1	Object
    //   6	355	3	localObject2	Object
    //   24	38	4	arrayOfString1	String[]
    //   27	252	5	i	int
    //   34	32	6	arrayOfString2	String[]
    //   45	269	7	localObject3	Object
    //   50	247	8	localObject4	Object
    //   55	9	9	str	String
    //   73	38	10	bool	boolean
    //   87	263	11	j	int
    //   135	98	12	k	int
    //   179	20	13	m	int
    //   264	8	14	localException	Exception
    // Exception table:
    //   from	to	target	type
    //   102	108	256	java/lang/Exception
    //   115	118	256	java/lang/Exception
    //   119	123	256	java/lang/Exception
    //   128	135	256	java/lang/Exception
    //   138	145	256	java/lang/Exception
    //   148	153	256	java/lang/Exception
    //   159	166	256	java/lang/Exception
    //   169	176	256	java/lang/Exception
    //   211	216	256	java/lang/Exception
    //   222	229	256	java/lang/Exception
    //   232	239	256	java/lang/Exception
    //   242	247	256	java/lang/Exception
    //   248	253	256	java/lang/Exception
    //   81	87	260	finally
    //   94	97	260	finally
    //   98	102	260	finally
    //   102	108	260	finally
    //   115	118	260	finally
    //   119	123	260	finally
    //   128	135	260	finally
    //   138	145	260	finally
    //   148	153	260	finally
    //   159	166	260	finally
    //   169	176	260	finally
    //   211	216	260	finally
    //   222	229	260	finally
    //   232	239	260	finally
    //   242	247	260	finally
    //   248	253	260	finally
    //   274	278	260	finally
    //   278	283	260	finally
    //   284	287	260	finally
    //   296	301	260	finally
    //   301	305	260	finally
    //   308	313	260	finally
    //   313	318	260	finally
    //   321	323	260	finally
    //   81	87	264	java/lang/Exception
    //   94	97	264	java/lang/Exception
    //   98	102	264	java/lang/Exception
  }
  
  public static Bitmap c(Context paramContext, long paramLong)
  {
    long l = 0L;
    boolean bool = paramLong < l;
    if (bool) {
      try
      {
        t.1 local1 = new com/truecaller/util/t$1;
        local1.<init>(paramLong, paramContext);
        return local1.a();
      }
      catch (Exception paramContext)
      {
        d.a(paramContext);
      }
    }
    return null;
  }
  
  /* Error */
  private static ArrayList c(Context paramContext, String paramString)
  {
    // Byte code:
    //   0: ldc 49
    //   2: astore_2
    //   3: iconst_1
    //   4: anewarray 21	java/lang/String
    //   7: dup
    //   8: iconst_0
    //   9: aload_2
    //   10: aastore
    //   11: astore_3
    //   12: iconst_1
    //   13: istore 4
    //   15: iload 4
    //   17: anewarray 21	java/lang/String
    //   20: astore 5
    //   22: aload 5
    //   24: iconst_0
    //   25: aload_1
    //   26: aastore
    //   27: aload_0
    //   28: invokevirtual 273	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   31: astore 6
    //   33: getstatic 573	android/provider/ContactsContract$CommonDataKinds$Email:CONTENT_URI	Landroid/net/Uri;
    //   36: astore 7
    //   38: ldc_w 548
    //   41: astore 8
    //   43: aload 6
    //   45: aload 7
    //   47: aload_3
    //   48: aload 8
    //   50: aload 5
    //   52: aconst_null
    //   53: invokevirtual 279	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   56: astore_0
    //   57: iconst_0
    //   58: istore 9
    //   60: aconst_null
    //   61: astore_1
    //   62: aload_0
    //   63: ifnull +170 -> 233
    //   66: aload_0
    //   67: invokeinterface 551 1 0
    //   72: istore 10
    //   74: iload 10
    //   76: ifle +157 -> 233
    //   79: ldc 49
    //   81: astore 6
    //   83: aload_0
    //   84: aload 6
    //   86: invokeinterface 288 2 0
    //   91: istore 10
    //   93: new 217	java/util/ArrayList
    //   96: astore 7
    //   98: aload 7
    //   100: invokespecial 220	java/util/ArrayList:<init>	()V
    //   103: aload_0
    //   104: invokeinterface 284 1 0
    //   109: istore 9
    //   111: iload 9
    //   113: ifeq +22 -> 135
    //   116: aload_0
    //   117: iload 10
    //   119: invokeinterface 292 2 0
    //   124: astore_1
    //   125: aload 7
    //   127: aload_1
    //   128: invokevirtual 240	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   131: pop
    //   132: goto -29 -> 103
    //   135: aload 7
    //   137: astore_1
    //   138: goto +95 -> 233
    //   141: astore 6
    //   143: aload 7
    //   145: astore_1
    //   146: goto +9 -> 155
    //   149: astore_1
    //   150: goto +71 -> 221
    //   153: astore 6
    //   155: aload 6
    //   157: invokestatic 300	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   160: iload 4
    //   162: anewarray 21	java/lang/String
    //   165: astore_2
    //   166: new 302	java/lang/StringBuilder
    //   169: astore 7
    //   171: ldc_w 575
    //   174: astore_3
    //   175: aload 7
    //   177: aload_3
    //   178: invokespecial 305	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   181: aload 6
    //   183: invokevirtual 310	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   186: astore 6
    //   188: aload 7
    //   190: aload 6
    //   192: invokevirtual 314	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   195: pop
    //   196: aload 7
    //   198: invokevirtual 317	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   201: astore 6
    //   203: aload_2
    //   204: iconst_0
    //   205: aload 6
    //   207: aastore
    //   208: aload_0
    //   209: ifnull +31 -> 240
    //   212: aload_0
    //   213: invokeinterface 320 1 0
    //   218: goto +22 -> 240
    //   221: aload_0
    //   222: ifnull +9 -> 231
    //   225: aload_0
    //   226: invokeinterface 320 1 0
    //   231: aload_1
    //   232: athrow
    //   233: aload_0
    //   234: ifnull +6 -> 240
    //   237: goto -25 -> 212
    //   240: aload_1
    //   241: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	242	0	paramContext	Context
    //   0	242	1	paramString	String
    //   2	202	2	localObject1	Object
    //   11	167	3	localObject2	Object
    //   13	148	4	i	int
    //   20	31	5	arrayOfString	String[]
    //   31	54	6	localObject3	Object
    //   141	1	6	localException1	Exception
    //   153	29	6	localException2	Exception
    //   186	20	6	str1	String
    //   36	161	7	localObject4	Object
    //   41	8	8	str2	String
    //   58	54	9	bool	boolean
    //   72	46	10	j	int
    // Exception table:
    //   from	to	target	type
    //   103	109	141	java/lang/Exception
    //   117	124	141	java/lang/Exception
    //   127	132	141	java/lang/Exception
    //   66	72	149	finally
    //   84	91	149	finally
    //   93	96	149	finally
    //   98	103	149	finally
    //   103	109	149	finally
    //   117	124	149	finally
    //   127	132	149	finally
    //   155	160	149	finally
    //   160	165	149	finally
    //   166	169	149	finally
    //   177	181	149	finally
    //   181	186	149	finally
    //   190	196	149	finally
    //   196	201	149	finally
    //   205	208	149	finally
    //   66	72	153	java/lang/Exception
    //   84	91	153	java/lang/Exception
    //   93	96	153	java/lang/Exception
    //   98	103	153	java/lang/Exception
  }
  
  public static boolean d(Context paramContext, long paramLong)
  {
    Object localObject1 = paramContext.getContentResolver();
    Uri localUri = ContactsContract.Data.CONTENT_URI;
    String[] arrayOfString1 = g;
    String str = "mimetype=? AND contact_id=?";
    int i = 2;
    String[] arrayOfString2 = new String[i];
    arrayOfString2[0] = "vnd.android.cursor.item/postal-address_v2";
    paramContext = String.valueOf(paramLong);
    boolean bool1 = true;
    arrayOfString2[bool1] = paramContext;
    paramContext = ((ContentResolver)localObject1).query(localUri, arrayOfString1, str, arrayOfString2, null);
    if (paramContext != null) {
      try
      {
        int j = paramContext.moveToNext();
        if (j != 0)
        {
          j = 0;
          for (;;)
          {
            localObject1 = g;
            int m = localObject1.length;
            if (j >= m) {
              break;
            }
            localObject1 = paramContext.getString(j);
            boolean bool2 = TextUtils.isEmpty((CharSequence)localObject1);
            if (!bool2) {
              return bool1;
            }
            int k;
            j += 1;
          }
        }
      }
      finally
      {
        paramContext.close();
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */