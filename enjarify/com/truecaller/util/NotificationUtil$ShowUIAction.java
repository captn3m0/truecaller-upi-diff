package com.truecaller.util;

import android.content.Context;
import android.content.Intent;
import com.truecaller.bk;
import com.truecaller.filters.blockedevents.BlockedEventsActivity;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.br;
import com.truecaller.profile.a;
import com.truecaller.ui.SettingsFragment;
import com.truecaller.ui.SettingsFragment.SettingsViewType;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.u;

public final class NotificationUtil$ShowUIAction
  implements NotificationUtil.b
{
  private final Context a;
  private final NotificationUtil.ShowUIAction.UiType b;
  
  public NotificationUtil$ShowUIAction(Context paramContext, Notification paramNotification)
  {
    a = paramContext;
    paramContext = NotificationUtil.ShowUIAction.UiType.getUiType(paramNotification.a("v"));
    b = paramContext;
  }
  
  public final void a()
  {
    Object localObject1 = NotificationUtil.1.b;
    int i = b.ordinal();
    int j = localObject1[i];
    i = 0;
    Object localObject2 = null;
    switch (j)
    {
    default: 
      TruecallerInit.b(a, "search", null);
      return;
    case 11: 
      localObject1 = a;
      localObject2 = SettingsFragment.SettingsViewType.SETTINGS_LANGUAGE_SELECTOR;
      SettingsFragment.b((Context)localObject1, (SettingsFragment.SettingsViewType)localObject2);
      return;
    case 10: 
      localObject1 = a;
      localObject2 = SettingsFragment.SettingsViewType.SETTINGS_ABOUT;
      SettingsFragment.b((Context)localObject1, (SettingsFragment.SettingsViewType)localObject2);
      return;
    case 9: 
      localObject1 = a;
      localObject2 = SettingsFragment.SettingsViewType.SETTINGS_PRIVACY;
      SettingsFragment.b((Context)localObject1, (SettingsFragment.SettingsViewType)localObject2);
      return;
    case 8: 
      localObject1 = a;
      localObject2 = SettingsFragment.SettingsViewType.SETTINGS_CALLERID;
      SettingsFragment.b((Context)localObject1, (SettingsFragment.SettingsViewType)localObject2);
      return;
    case 7: 
      localObject1 = a;
      localObject2 = SettingsFragment.SettingsViewType.SETTINGS_GENERAL;
      SettingsFragment.b((Context)localObject1, (SettingsFragment.SettingsViewType)localObject2);
      return;
    case 6: 
      localObject1 = a;
      localObject2 = SettingsFragment.SettingsViewType.SETTINGS_MAIN;
      SettingsFragment.b((Context)localObject1, (SettingsFragment.SettingsViewType)localObject2);
      return;
    case 5: 
      ((bk)a.getApplicationContext()).a();
      localObject1 = a;
      localObject2 = PremiumPresenterView.LaunchContext.NOTIFICATION;
      br.a((Context)localObject1, (PremiumPresenterView.LaunchContext)localObject2);
      return;
    case 4: 
      localObject1 = a;
      localObject2 = new android/content/Intent;
      ((Intent)localObject2).<init>((Context)localObject1, BlockedEventsActivity.class);
      ((Context)localObject1).startActivity((Intent)localObject2);
      return;
    case 3: 
      u.b(a);
      return;
    case 2: 
      TruecallerInit.b(a, "search", null);
      return;
    }
    a.a(a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.NotificationUtil.ShowUIAction
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */