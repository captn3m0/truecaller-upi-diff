package com.truecaller.util.d;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutInfo.Builder;
import android.content.pm.ShortcutManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Icon;
import android.os.Build.VERSION;
import android.support.v4.content.a.a.a;
import android.support.v4.graphics.drawable.IconCompat;
import android.text.TextUtils;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.MessagesActivity;
import com.truecaller.payments.BankingActivity;
import com.truecaller.ui.ContactsActivity;
import com.truecaller.ui.TruecallerInit;

public final class b
  implements a
{
  private final Context a;
  
  public b(Context paramContext)
  {
    a = paramContext;
  }
  
  public final void a(int paramInt)
  {
    try
    {
      Object localObject1 = a;
      boolean bool1 = android.support.v4.content.a.b.a((Context)localObject1);
      if (bool1)
      {
        localObject1 = a;
        Object localObject2 = new android/support/v4/content/a/a$a;
        Object localObject3 = a;
        int i = 3;
        int j = 2;
        int k = 1;
        if (paramInt == k) {
          localObject4 = "home-shortcut-messages-id";
        } else if (paramInt == j) {
          localObject4 = "home-shortcut-contacts-id";
        } else if (paramInt == i) {
          localObject4 = "home-shortcut-banking-id";
        } else {
          localObject4 = "home-shortcut-dialer-id";
        }
        ((a.a)localObject2).<init>((Context)localObject3, (String)localObject4);
        localObject3 = a;
        int m = 2131886117;
        switch (paramInt)
        {
        default: 
          break;
        case 3: 
          m = 2131888830;
          break;
        case 2: 
          m = 2131888872;
          break;
        case 1: 
          m = 2131887268;
        }
        localObject3 = ((Context)localObject3).getString(m);
        Object localObject4 = a;
        e = ((CharSequence)localObject3);
        localObject3 = a;
        m = 2131689472;
        switch (paramInt)
        {
        default: 
          break;
        case 3: 
          m = 2131234543;
          break;
        case 2: 
          m = 2131689473;
          break;
        case 1: 
          m = 2131689476;
        }
        localObject3 = IconCompat.a((Context)localObject3, m);
        localObject4 = a;
        h = ((IconCompat)localObject3);
        localObject3 = TruecallerInit.class;
        if (paramInt == k) {
          localObject3 = MessagesActivity.class;
        } else if (paramInt == j) {
          localObject3 = ContactsActivity.class;
        } else if (paramInt == i) {
          localObject3 = BankingActivity.class;
        }
        Object localObject5 = new android/content/Intent;
        Object localObject6 = a;
        ((Intent)localObject5).<init>((Context)localObject6, (Class)localObject3);
        localObject3 = "android.intent.action.MAIN";
        ((Intent)localObject5).setAction((String)localObject3);
        int n = 335544320;
        ((Intent)localObject5).addFlags(n);
        localObject3 = new Intent[k];
        i = 0;
        localObject6 = null;
        localObject3[0] = localObject5;
        localObject5 = a;
        c = ((Intent[])localObject3);
        localObject5 = a;
        localObject5 = e;
        paramInt = TextUtils.isEmpty((CharSequence)localObject5);
        if (paramInt == 0)
        {
          localObject5 = a;
          localObject5 = c;
          if (localObject5 != null)
          {
            localObject5 = a;
            localObject5 = c;
            paramInt = localObject5.length;
            if (paramInt != 0)
            {
              localObject5 = a;
              int i1 = Build.VERSION.SDK_INT;
              n = 26;
              if (i1 >= n)
              {
                localObject2 = ShortcutManager.class;
                localObject1 = ((Context)localObject1).getSystemService((Class)localObject2);
                localObject1 = (ShortcutManager)localObject1;
                localObject2 = new android/content/pm/ShortcutInfo$Builder;
                Object localObject7 = a;
                Object localObject8 = b;
                ((ShortcutInfo.Builder)localObject2).<init>((Context)localObject7, (String)localObject8);
                localObject7 = e;
                localObject2 = ((ShortcutInfo.Builder)localObject2).setShortLabel((CharSequence)localObject7);
                localObject7 = c;
                localObject2 = ((ShortcutInfo.Builder)localObject2).setIntents((Intent[])localObject7);
                localObject7 = h;
                if (localObject7 != null)
                {
                  localObject7 = h;
                  k = a;
                  m = -1;
                  if (k != m)
                  {
                    switch (k)
                    {
                    default: 
                      localObject5 = new java/lang/IllegalArgumentException;
                      localObject1 = "Unknown type";
                      ((IllegalArgumentException)localObject5).<init>((String)localObject1);
                      throw ((Throwable)localObject5);
                    case 5: 
                      k = Build.VERSION.SDK_INT;
                      if (k >= n)
                      {
                        localObject3 = b;
                        localObject3 = (Bitmap)localObject3;
                        localObject3 = Icon.createWithAdaptiveBitmap((Bitmap)localObject3);
                      }
                      else
                      {
                        localObject3 = b;
                        localObject3 = (Bitmap)localObject3;
                        localObject3 = IconCompat.a((Bitmap)localObject3, false);
                        localObject3 = Icon.createWithBitmap((Bitmap)localObject3);
                      }
                      break;
                    case 4: 
                      localObject3 = b;
                      localObject3 = (String)localObject3;
                      localObject3 = Icon.createWithContentUri((String)localObject3);
                      break;
                    case 3: 
                      localObject3 = b;
                      localObject3 = (byte[])localObject3;
                      localObject3 = (byte[])localObject3;
                      i = e;
                      k = f;
                      localObject3 = Icon.createWithData((byte[])localObject3, i, k);
                      break;
                    case 2: 
                      localObject3 = ((IconCompat)localObject7).a();
                      i = e;
                      localObject3 = Icon.createWithResource((String)localObject3, i);
                      break;
                    case 1: 
                      localObject3 = b;
                      localObject3 = (Bitmap)localObject3;
                      localObject3 = Icon.createWithBitmap((Bitmap)localObject3);
                    }
                    localObject6 = g;
                    if (localObject6 != null)
                    {
                      localObject6 = g;
                      ((Icon)localObject3).setTintList((ColorStateList)localObject6);
                    }
                    localObject6 = i;
                    localObject8 = IconCompat.h;
                    if (localObject6 != localObject8)
                    {
                      localObject6 = i;
                      ((Icon)localObject3).setTintMode((PorterDuff.Mode)localObject6);
                    }
                  }
                  else
                  {
                    localObject3 = b;
                    localObject3 = (Icon)localObject3;
                  }
                  ((ShortcutInfo.Builder)localObject2).setIcon((Icon)localObject3);
                }
                localObject3 = f;
                boolean bool2 = TextUtils.isEmpty((CharSequence)localObject3);
                if (!bool2)
                {
                  localObject3 = f;
                  ((ShortcutInfo.Builder)localObject2).setLongLabel((CharSequence)localObject3);
                }
                localObject3 = g;
                bool2 = TextUtils.isEmpty((CharSequence)localObject3);
                if (!bool2)
                {
                  localObject3 = g;
                  ((ShortcutInfo.Builder)localObject2).setDisabledMessage((CharSequence)localObject3);
                }
                localObject3 = d;
                if (localObject3 != null)
                {
                  localObject5 = d;
                  ((ShortcutInfo.Builder)localObject2).setActivity((ComponentName)localObject5);
                }
                localObject5 = ((ShortcutInfo.Builder)localObject2).build();
                i1 = 0;
                localObject2 = null;
                ((ShortcutManager)localObject1).requestPinShortcut((ShortcutInfo)localObject5, null);
                return;
              }
              boolean bool3 = android.support.v4.content.a.b.a((Context)localObject1);
              if (!bool3) {
                break label1099;
              }
              localObject2 = new android/content/Intent;
              localObject3 = "com.android.launcher.action.INSTALL_SHORTCUT";
              ((Intent)localObject2).<init>((String)localObject3);
              localObject5 = ((android.support.v4.content.a.a)localObject5).a((Intent)localObject2);
              ((Context)localObject1).sendBroadcast((Intent)localObject5);
              break label1099;
            }
          }
          localObject5 = new java/lang/IllegalArgumentException;
          localObject1 = "Shortcut must have an intent";
          ((IllegalArgumentException)localObject5).<init>((String)localObject1);
          throw ((Throwable)localObject5);
        }
        else
        {
          localObject5 = new java/lang/IllegalArgumentException;
          localObject1 = "Shortcut must have a non-empty label";
          ((IllegalArgumentException)localObject5).<init>((String)localObject1);
          throw ((Throwable)localObject5);
        }
      }
      label1099:
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localRuntimeException;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */