package com.truecaller.util;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public final class RingtoneUtils$a
  extends Handler
  implements RingtoneUtils.b, RingtoneUtils.c, Runnable
{
  protected final Context a;
  final RingtoneUtils.Ringtone b;
  private final RingtoneUtils.b c;
  private final RingtoneUtils.c d;
  
  public RingtoneUtils$a(Context paramContext, RingtoneUtils.Ringtone paramRingtone, RingtoneUtils.b paramb, RingtoneUtils.c paramc)
  {
    super(localLooper);
    b = paramRingtone;
    paramContext = paramContext.getApplicationContext();
    a = paramContext;
    c = paramb;
    d = paramc;
  }
  
  public final void b(boolean paramBoolean)
  {
    RingtoneUtils.c localc = d;
    if (localc != null) {
      localc.b(paramBoolean);
    }
  }
  
  public final void handleMessage(Message paramMessage)
  {
    int i = what;
    int j = 3;
    if (i == j)
    {
      paramMessage = (Uri)obj;
      onRingtoneCopied(paramMessage);
      return;
    }
    int k = what;
    i = 1;
    if (k != i) {
      i = 0;
    }
    b(i);
  }
  
  public final void onRingtoneCopied(Uri paramUri)
  {
    RingtoneUtils.b localb = c;
    if (localb != null) {
      localb.onRingtoneCopied(paramUri);
    }
  }
  
  /* Error */
  public final void run()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 30	com/truecaller/util/RingtoneUtils$a:b	Lcom/truecaller/util/RingtoneUtils$Ringtone;
    //   4: astore_1
    //   5: aload_1
    //   6: invokestatic 71	com/truecaller/util/RingtoneUtils$Ringtone:access$000	(Lcom/truecaller/util/RingtoneUtils$Ringtone;)I
    //   9: istore_2
    //   10: iconst_2
    //   11: istore_3
    //   12: iconst_m1
    //   13: istore 4
    //   15: iload_2
    //   16: iload 4
    //   18: if_icmpeq +774 -> 792
    //   21: aload_0
    //   22: getfield 30	com/truecaller/util/RingtoneUtils$a:b	Lcom/truecaller/util/RingtoneUtils$Ringtone;
    //   25: invokestatic 76	com/truecaller/util/RingtoneUtils$Ringtone:access$100	(Lcom/truecaller/util/RingtoneUtils$Ringtone;)Ljava/lang/String;
    //   28: astore_1
    //   29: aload_1
    //   30: invokestatic 82	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   33: istore_2
    //   34: iload_2
    //   35: ifne +757 -> 792
    //   38: aload_0
    //   39: getfield 30	com/truecaller/util/RingtoneUtils$a:b	Lcom/truecaller/util/RingtoneUtils$Ringtone;
    //   42: invokestatic 85	com/truecaller/util/RingtoneUtils$Ringtone:access$200	(Lcom/truecaller/util/RingtoneUtils$Ringtone;)Ljava/lang/String;
    //   45: astore_1
    //   46: aload_1
    //   47: invokestatic 82	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   50: istore_2
    //   51: iload_2
    //   52: ifeq +6 -> 58
    //   55: goto +737 -> 792
    //   58: aload_0
    //   59: getfield 30	com/truecaller/util/RingtoneUtils$a:b	Lcom/truecaller/util/RingtoneUtils$Ringtone;
    //   62: astore_1
    //   63: aload_1
    //   64: invokestatic 88	com/truecaller/util/RingtoneUtils$Ringtone:access$300	(Lcom/truecaller/util/RingtoneUtils$Ringtone;)I
    //   67: istore_2
    //   68: iconst_1
    //   69: istore 4
    //   71: iload_2
    //   72: iload 4
    //   74: if_icmpne +10 -> 84
    //   77: getstatic 94	android/os/Environment:DIRECTORY_RINGTONES	Ljava/lang/String;
    //   80: astore_1
    //   81: goto +7 -> 88
    //   84: getstatic 97	android/os/Environment:DIRECTORY_NOTIFICATIONS	Ljava/lang/String;
    //   87: astore_1
    //   88: aload_1
    //   89: invokestatic 101	android/os/Environment:getExternalStoragePublicDirectory	(Ljava/lang/String;)Ljava/io/File;
    //   92: astore_1
    //   93: ldc 103
    //   95: astore 5
    //   97: aload_1
    //   98: invokestatic 108	android/support/v4/os/a:a	(Ljava/io/File;)Ljava/lang/String;
    //   101: astore 6
    //   103: aload 5
    //   105: aload 6
    //   107: invokevirtual 114	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   110: istore 7
    //   112: iload 7
    //   114: ifeq +671 -> 785
    //   117: aload_1
    //   118: invokevirtual 120	java/io/File:exists	()Z
    //   121: istore 7
    //   123: iload 7
    //   125: ifne +17 -> 142
    //   128: aload_1
    //   129: invokevirtual 123	java/io/File:mkdirs	()Z
    //   132: istore 7
    //   134: iload 7
    //   136: ifne +6 -> 142
    //   139: goto +646 -> 785
    //   142: new 116	java/io/File
    //   145: astore 5
    //   147: aload_0
    //   148: getfield 30	com/truecaller/util/RingtoneUtils$a:b	Lcom/truecaller/util/RingtoneUtils$Ringtone;
    //   151: invokestatic 76	com/truecaller/util/RingtoneUtils$Ringtone:access$100	(Lcom/truecaller/util/RingtoneUtils$Ringtone;)Ljava/lang/String;
    //   154: astore 6
    //   156: aload 5
    //   158: aload_1
    //   159: aload 6
    //   161: invokespecial 126	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   164: new 116	java/io/File
    //   167: astore 6
    //   169: aload_0
    //   170: getfield 30	com/truecaller/util/RingtoneUtils$a:b	Lcom/truecaller/util/RingtoneUtils$Ringtone;
    //   173: invokestatic 76	com/truecaller/util/RingtoneUtils$Ringtone:access$100	(Lcom/truecaller/util/RingtoneUtils$Ringtone;)Ljava/lang/String;
    //   176: astore 8
    //   178: ldc -128
    //   180: astore 9
    //   182: ldc -126
    //   184: astore 10
    //   186: aload 8
    //   188: aload 9
    //   190: aload 10
    //   192: invokevirtual 134	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   195: astore 8
    //   197: aload 6
    //   199: aload_1
    //   200: aload 8
    //   202: invokespecial 126	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   205: aload 6
    //   207: invokevirtual 120	java/io/File:exists	()Z
    //   210: istore_2
    //   211: aconst_null
    //   212: astore 8
    //   214: iload_2
    //   215: ifne +150 -> 365
    //   218: aload 5
    //   220: invokevirtual 120	java/io/File:exists	()Z
    //   223: istore_2
    //   224: iload_2
    //   225: ifne +140 -> 365
    //   228: new 136	java/io/FileOutputStream
    //   231: astore_1
    //   232: aload_1
    //   233: aload 5
    //   235: invokespecial 139	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   238: aload_0
    //   239: getfield 38	com/truecaller/util/RingtoneUtils$a:a	Landroid/content/Context;
    //   242: astore 9
    //   244: aload 9
    //   246: invokevirtual 143	android/content/Context:getResources	()Landroid/content/res/Resources;
    //   249: astore 9
    //   251: aload_0
    //   252: getfield 30	com/truecaller/util/RingtoneUtils$a:b	Lcom/truecaller/util/RingtoneUtils$Ringtone;
    //   255: astore 10
    //   257: aload 10
    //   259: invokestatic 71	com/truecaller/util/RingtoneUtils$Ringtone:access$000	(Lcom/truecaller/util/RingtoneUtils$Ringtone;)I
    //   262: istore 11
    //   264: aload 9
    //   266: iload 11
    //   268: invokevirtual 149	android/content/res/Resources:openRawResource	(I)Ljava/io/InputStream;
    //   271: astore 9
    //   273: aload 9
    //   275: aload_1
    //   276: invokestatic 154	com/truecaller/utils/extensions/m:a	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   279: pop2
    //   280: aload 9
    //   282: invokestatic 159	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   285: aload_1
    //   286: invokestatic 159	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   289: goto +76 -> 365
    //   292: astore 12
    //   294: aload 9
    //   296: astore 8
    //   298: goto +55 -> 353
    //   301: astore 13
    //   303: aload 9
    //   305: astore 8
    //   307: goto +23 -> 330
    //   310: astore 13
    //   312: goto +18 -> 330
    //   315: astore 12
    //   317: iconst_0
    //   318: istore_2
    //   319: aconst_null
    //   320: astore_1
    //   321: goto +32 -> 353
    //   324: astore 13
    //   326: iconst_0
    //   327: istore_2
    //   328: aconst_null
    //   329: astore_1
    //   330: aload 13
    //   332: invokestatic 165	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   335: aload_0
    //   336: iload_3
    //   337: invokevirtual 169	com/truecaller/util/RingtoneUtils$a:sendEmptyMessage	(I)Z
    //   340: pop
    //   341: aload 8
    //   343: invokestatic 159	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   346: aload_1
    //   347: invokestatic 159	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   350: return
    //   351: astore 12
    //   353: aload 8
    //   355: invokestatic 159	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   358: aload_1
    //   359: invokestatic 159	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   362: aload 12
    //   364: athrow
    //   365: aload 6
    //   367: invokevirtual 120	java/io/File:exists	()Z
    //   370: istore_2
    //   371: iload_2
    //   372: ifeq +7 -> 379
    //   375: aload 6
    //   377: astore 5
    //   379: aload 5
    //   381: invokevirtual 173	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   384: invokestatic 179	android/provider/MediaStore$Audio$Media:getContentUriForPath	(Ljava/lang/String;)Landroid/net/Uri;
    //   387: astore_1
    //   388: aload_0
    //   389: getfield 38	com/truecaller/util/RingtoneUtils$a:a	Landroid/content/Context;
    //   392: invokevirtual 183	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   395: astore 6
    //   397: invokestatic 188	com/truecaller/util/RingtoneUtils:a	()[Ljava/lang/String;
    //   400: astore 14
    //   402: ldc -66
    //   404: astore 15
    //   406: iload 4
    //   408: anewarray 110	java/lang/String
    //   411: astore 16
    //   413: aload 5
    //   415: invokevirtual 173	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   418: astore 9
    //   420: iconst_0
    //   421: istore 17
    //   423: aload 16
    //   425: iconst_0
    //   426: aload 9
    //   428: aastore
    //   429: aload 6
    //   431: astore 9
    //   433: aload_1
    //   434: astore 10
    //   436: aload 6
    //   438: aload_1
    //   439: aload 14
    //   441: aload 15
    //   443: aload 16
    //   445: aconst_null
    //   446: invokevirtual 196	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   449: astore 9
    //   451: aload 9
    //   453: ifnull +67 -> 520
    //   456: aload 9
    //   458: invokeinterface 201 1 0
    //   463: istore 11
    //   465: iload 11
    //   467: ifeq +53 -> 520
    //   470: aload 9
    //   472: iconst_0
    //   473: invokeinterface 205 2 0
    //   478: astore 8
    //   480: aload_1
    //   481: invokevirtual 209	android/net/Uri:buildUpon	()Landroid/net/Uri$Builder;
    //   484: astore 10
    //   486: aload 10
    //   488: aload 8
    //   490: invokevirtual 215	android/net/Uri$Builder:appendPath	(Ljava/lang/String;)Landroid/net/Uri$Builder;
    //   493: astore 8
    //   495: aload 8
    //   497: invokevirtual 219	android/net/Uri$Builder:build	()Landroid/net/Uri;
    //   500: astore 8
    //   502: goto +18 -> 520
    //   505: astore_1
    //   506: aload 9
    //   508: ifnull +10 -> 518
    //   511: aload 9
    //   513: invokeinterface 223 1 0
    //   518: aload_1
    //   519: athrow
    //   520: aload 9
    //   522: ifnull +10 -> 532
    //   525: aload 9
    //   527: invokeinterface 223 1 0
    //   532: aload 8
    //   534: ifnonnull +229 -> 763
    //   537: new 225	android/content/ContentValues
    //   540: astore 8
    //   542: aload 8
    //   544: invokespecial 227	android/content/ContentValues:<init>	()V
    //   547: aload 5
    //   549: invokevirtual 173	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   552: astore 5
    //   554: aload 8
    //   556: ldc -27
    //   558: aload 5
    //   560: invokevirtual 233	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   563: aload_0
    //   564: getfield 30	com/truecaller/util/RingtoneUtils$a:b	Lcom/truecaller/util/RingtoneUtils$Ringtone;
    //   567: invokestatic 238	com/truecaller/util/RingtoneUtils$Ringtone:access$500	(Lcom/truecaller/util/RingtoneUtils$Ringtone;)Ljava/lang/String;
    //   570: astore 9
    //   572: aload 8
    //   574: ldc -21
    //   576: aload 9
    //   578: invokevirtual 233	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   581: aload_0
    //   582: getfield 30	com/truecaller/util/RingtoneUtils$a:b	Lcom/truecaller/util/RingtoneUtils$Ringtone;
    //   585: invokestatic 238	com/truecaller/util/RingtoneUtils$Ringtone:access$500	(Lcom/truecaller/util/RingtoneUtils$Ringtone;)Ljava/lang/String;
    //   588: astore 9
    //   590: aload 8
    //   592: ldc -16
    //   594: aload 9
    //   596: invokevirtual 233	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   599: aload_0
    //   600: getfield 30	com/truecaller/util/RingtoneUtils$a:b	Lcom/truecaller/util/RingtoneUtils$Ringtone;
    //   603: invokestatic 85	com/truecaller/util/RingtoneUtils$Ringtone:access$200	(Lcom/truecaller/util/RingtoneUtils$Ringtone;)Ljava/lang/String;
    //   606: astore 9
    //   608: aload 8
    //   610: ldc -14
    //   612: aload 9
    //   614: invokevirtual 233	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   617: getstatic 250	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   620: astore 9
    //   622: aload 8
    //   624: ldc -12
    //   626: aload 9
    //   628: invokevirtual 253	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Boolean;)V
    //   631: getstatic 250	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   634: astore 9
    //   636: aload 8
    //   638: ldc -1
    //   640: aload 9
    //   642: invokevirtual 253	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Boolean;)V
    //   645: ldc_w 257
    //   648: astore 5
    //   650: aload_0
    //   651: getfield 30	com/truecaller/util/RingtoneUtils$a:b	Lcom/truecaller/util/RingtoneUtils$Ringtone;
    //   654: astore 9
    //   656: aload 9
    //   658: invokestatic 88	com/truecaller/util/RingtoneUtils$Ringtone:access$300	(Lcom/truecaller/util/RingtoneUtils$Ringtone;)I
    //   661: istore 18
    //   663: iload 18
    //   665: iload_3
    //   666: if_icmpne +8 -> 674
    //   669: iconst_1
    //   670: istore_3
    //   671: goto +8 -> 679
    //   674: iconst_0
    //   675: istore_3
    //   676: aconst_null
    //   677: astore 12
    //   679: iload_3
    //   680: invokestatic 261	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   683: astore 12
    //   685: aload 8
    //   687: aload 5
    //   689: aload 12
    //   691: invokevirtual 253	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Boolean;)V
    //   694: getstatic 250	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   697: astore 5
    //   699: aload 8
    //   701: ldc_w 263
    //   704: aload 5
    //   706: invokevirtual 253	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Boolean;)V
    //   709: ldc_w 265
    //   712: astore 12
    //   714: aload_0
    //   715: getfield 30	com/truecaller/util/RingtoneUtils$a:b	Lcom/truecaller/util/RingtoneUtils$Ringtone;
    //   718: astore 5
    //   720: aload 5
    //   722: invokestatic 88	com/truecaller/util/RingtoneUtils$Ringtone:access$300	(Lcom/truecaller/util/RingtoneUtils$Ringtone;)I
    //   725: istore 7
    //   727: iload 7
    //   729: iload 4
    //   731: if_icmpne +6 -> 737
    //   734: iconst_1
    //   735: istore 17
    //   737: iload 17
    //   739: invokestatic 261	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   742: astore 5
    //   744: aload 8
    //   746: aload 12
    //   748: aload 5
    //   750: invokevirtual 253	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Boolean;)V
    //   753: aload 6
    //   755: aload_1
    //   756: aload 8
    //   758: invokevirtual 269	android/content/ContentResolver:insert	(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    //   761: astore 8
    //   763: aload_0
    //   764: iconst_3
    //   765: aload 8
    //   767: invokevirtual 273	com/truecaller/util/RingtoneUtils$a:obtainMessage	(ILjava/lang/Object;)Landroid/os/Message;
    //   770: astore_1
    //   771: aload_0
    //   772: aload_1
    //   773: invokevirtual 277	com/truecaller/util/RingtoneUtils$a:sendMessage	(Landroid/os/Message;)Z
    //   776: pop
    //   777: aload_0
    //   778: iload 4
    //   780: invokevirtual 169	com/truecaller/util/RingtoneUtils$a:sendEmptyMessage	(I)Z
    //   783: pop
    //   784: return
    //   785: aload_0
    //   786: iload_3
    //   787: invokevirtual 169	com/truecaller/util/RingtoneUtils$a:sendEmptyMessage	(I)Z
    //   790: pop
    //   791: return
    //   792: aload_0
    //   793: iload_3
    //   794: invokevirtual 169	com/truecaller/util/RingtoneUtils$a:sendEmptyMessage	(I)Z
    //   797: pop
    //   798: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	799	0	this	a
    //   4	477	1	localObject1	Object
    //   505	251	1	localUri	Uri
    //   770	3	1	localMessage	Message
    //   9	10	2	i	int
    //   33	19	2	bool1	boolean
    //   67	8	2	j	int
    //   210	162	2	bool2	boolean
    //   11	783	3	k	int
    //   13	766	4	m	int
    //   95	654	5	localObject2	Object
    //   101	653	6	localObject3	Object
    //   110	25	7	bool3	boolean
    //   725	7	7	n	int
    //   176	590	8	localObject4	Object
    //   180	477	9	localObject5	Object
    //   184	303	10	localObject6	Object
    //   262	5	11	i1	int
    //   463	3	11	bool4	boolean
    //   292	1	12	localObject7	Object
    //   315	1	12	localObject8	Object
    //   351	12	12	localObject9	Object
    //   677	70	12	localObject10	Object
    //   301	1	13	localIOException1	java.io.IOException
    //   310	1	13	localIOException2	java.io.IOException
    //   324	7	13	localIOException3	java.io.IOException
    //   400	40	14	arrayOfString1	String[]
    //   404	38	15	str	String
    //   411	33	16	arrayOfString2	String[]
    //   421	317	17	bool5	boolean
    //   661	6	18	i2	int
    // Exception table:
    //   from	to	target	type
    //   275	280	292	finally
    //   275	280	301	java/io/IOException
    //   238	242	310	java/io/IOException
    //   244	249	310	java/io/IOException
    //   251	255	310	java/io/IOException
    //   257	262	310	java/io/IOException
    //   266	271	310	java/io/IOException
    //   228	231	315	finally
    //   233	238	315	finally
    //   228	231	324	java/io/IOException
    //   233	238	324	java/io/IOException
    //   238	242	351	finally
    //   244	249	351	finally
    //   251	255	351	finally
    //   257	262	351	finally
    //   266	271	351	finally
    //   330	335	351	finally
    //   336	341	351	finally
    //   456	463	505	finally
    //   472	478	505	finally
    //   480	484	505	finally
    //   488	493	505	finally
    //   495	500	505	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.RingtoneUtils.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */