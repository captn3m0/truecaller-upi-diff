package com.truecaller.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.presence.AvailabilityTrigger;
import com.truecaller.presence.c;

public class RingerModeChangedReceiver
  extends BroadcastReceiver
{
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent != null)
    {
      String str = "android.media.RINGER_MODE_CHANGED";
      paramIntent = paramIntent.getAction();
      boolean bool1 = str.equals(paramIntent);
      if (bool1)
      {
        new String[1][0] = "Ringer changed, triggering a presence update";
        paramContext = (c)((bk)paramContext.getApplicationContext()).a().ae().a();
        paramIntent = AvailabilityTrigger.USER_ACTION;
        boolean bool2 = true;
        paramContext.a(paramIntent, bool2);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.RingerModeChangedReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */