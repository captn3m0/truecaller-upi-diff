package com.truecaller.util;

import android.content.Context;
import android.content.pm.PackageManager;
import c.a.ag;
import c.g.a.b;
import c.g.b.k;
import c.m.i;
import c.t;
import com.truecaller.analytics.ae;
import com.truecaller.common.g.a;
import com.truecaller.common.h.au;
import com.truecaller.log.AssertionUtil;
import com.truecaller.old.data.access.Settings.BuildName;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import java.util.Iterator;
import java.util.List;

public final class l
  implements com.truecaller.common.h.c
{
  private final String a;
  private final boolean b;
  private final Context c;
  private final com.truecaller.utils.d d;
  private final a e;
  private final com.truecaller.i.c f;
  private final com.truecaller.androidactors.f g;
  
  public l(Context paramContext, com.truecaller.utils.d paramd, a parama, com.truecaller.i.c paramc, com.truecaller.androidactors.f paramf)
  {
    c = paramContext;
    d = paramd;
    e = parama;
    f = paramc;
    g = paramf;
    try
    {
      paramContext = c;
      paramContext = paramContext.getPackageManager();
      paramd = c;
      paramd = paramd.getPackageName();
      paramContext = paramContext.getInstallerPackageName(paramd);
      if (paramContext == null) {
        paramContext = "";
      }
    }
    catch (Exception localException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localException);
      bool1 = false;
      paramContext = null;
    }
    a = paramContext;
    paramContext = a;
    boolean bool1 = k.a(paramContext, "com.android.vending");
    boolean bool2 = false;
    paramd = null;
    if (!bool1)
    {
      paramContext = (CharSequence)a;
      if (paramContext != null)
      {
        i = paramContext.length();
        if (i != 0)
        {
          i = 0;
          paramContext = null;
          break label181;
        }
      }
      int i = 1;
      label181:
      if (i == 0) {}
    }
    else
    {
      bool2 = true;
    }
    b = bool2;
  }
  
  private final String a(String paramString)
  {
    Object localObject1 = c.getPackageManager();
    k.a(localObject1, "context.packageManager");
    String str1 = i();
    Object localObject2 = str1;
    localObject2 = (CharSequence)str1;
    int i = 0;
    boolean bool1 = true;
    if (localObject2 != null)
    {
      bool2 = c.n.m.a((CharSequence)localObject2);
      if (!bool2)
      {
        bool2 = false;
        localObject2 = null;
        break label66;
      }
    }
    boolean bool2 = true;
    label66:
    if (bool2)
    {
      str1 = "GOOGLE_PLAY";
      localObject2 = (CharSequence)a;
      if (localObject2 != null)
      {
        bool2 = c.n.m.a((CharSequence)localObject2);
        if (!bool2)
        {
          bool2 = false;
          localObject2 = null;
          break label115;
        }
      }
      bool2 = true;
      label115:
      if (bool2)
      {
        localObject2 = Settings.BuildName.GOOGLE_PLAY.name();
        bool2 = c.n.m.a(str1, (String)localObject2, bool1);
        if (bool2) {
          str1 = Settings.BuildName.TC_SHARED.name();
        }
      }
    }
    else
    {
      localObject2 = e;
      String str2 = "IS_PREALOAD_BUILD";
      ((a)localObject2).b(str2, bool1);
    }
    localObject2 = "com.truecaller.dialer.integration";
    boolean bool3 = ((PackageManager)localObject1).hasSystemFeature((String)localObject2);
    if (bool3)
    {
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      ((StringBuilder)localObject1).append(str1);
      ((StringBuilder)localObject1).append("_NATIVE");
      str1 = ((StringBuilder)localObject1).toString();
      localObject1 = f;
      localObject2 = "hasNativeDialerCallerId";
      ((com.truecaller.i.c)localObject1).b((String)localObject2, bool1);
      bool3 = true;
    }
    else
    {
      bool3 = false;
      localObject1 = null;
    }
    localObject2 = paramString;
    localObject2 = (CharSequence)paramString;
    if (localObject2 != null)
    {
      bool2 = c.n.m.a((CharSequence)localObject2);
      if (!bool2)
      {
        bool2 = false;
        localObject2 = null;
        break label288;
      }
    }
    bool2 = true;
    label288:
    if ((!bool2) && (!bool3)) {
      return paramString;
    }
    localObject1 = e;
    localObject2 = "BUILD_KEY";
    ((a)localObject1).a((String)localObject2, str1);
    localObject1 = str1;
    localObject1 = (CharSequence)str1;
    if (localObject1 != null)
    {
      bool3 = c.n.m.a((CharSequence)localObject1);
      if (!bool3) {}
    }
    else
    {
      i = 1;
    }
    if (i != 0) {
      a(str1, paramString);
    }
    return str1;
  }
  
  private final void a(String paramString1, String paramString2)
  {
    ao.a locala = ao.b();
    CharSequence localCharSequence = (CharSequence)"AppMissingBuildName";
    locala = locala.a(localCharSequence);
    paramString1 = b(paramString1);
    paramString1 = ag.a(t.a("BuildName", paramString1));
    paramString1 = locala.a(paramString1);
    paramString2 = b(paramString2);
    paramString2 = ag.a(t.a("OldBuildName", paramString2));
    paramString1 = paramString1.a(paramString2).a();
    paramString2 = (ae)g.a();
    paramString1 = (org.apache.a.d.d)paramString1;
    paramString2.a(paramString1);
  }
  
  private static String b(String paramString)
  {
    if (paramString == null) {
      return "<null>";
    }
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    int i = ((CharSequence)localObject).length();
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (i != 0) {
      return "<empty>";
    }
    return paramString;
  }
  
  public final boolean a()
  {
    return e.b("IS_PREALOAD_BUILD");
  }
  
  public final boolean b()
  {
    return b;
  }
  
  public final boolean c()
  {
    boolean bool = b;
    if (!bool)
    {
      String str1 = f();
      String str2 = "CAFEBAZAAR";
      bool = c.n.m.b(str1, str2, false);
      if (!bool) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean d()
  {
    boolean bool = b;
    if (!bool)
    {
      String str1 = f();
      String str2 = Settings.BuildName.SAMSUNG.name();
      bool = c.n.m.b(str1, str2, false);
      if (!bool)
      {
        str1 = f();
        str2 = Settings.BuildName.AMAZON.name();
        bool = c.n.m.b(str1, str2, false);
        if (!bool) {
          return false;
        }
      }
    }
    return true;
  }
  
  public final au e()
  {
    Object localObject1 = (CharSequence)"10.41.6";
    int i = 1;
    Object localObject2 = new char[i];
    localObject2[0] = 46;
    int j = 6;
    localObject1 = c.n.m.a((CharSequence)localObject1, (char[])localObject2, 0, j);
    localObject2 = (String)c.a.m.a((List)localObject1, 0);
    Integer localInteger = null;
    int k;
    if (localObject2 != null)
    {
      k = Integer.parseInt((String)localObject2);
      localObject2 = Integer.valueOf(k);
    }
    else
    {
      k = 0;
      localObject2 = null;
    }
    Object localObject3 = (String)c.a.m.a((List)localObject1, i);
    if (localObject3 != null)
    {
      i = Integer.parseInt((String)localObject3);
      localObject3 = Integer.valueOf(i);
    }
    else
    {
      i = 0;
      localObject3 = null;
    }
    j = 2;
    localObject1 = (String)c.a.m.a((List)localObject1, j);
    if (localObject1 != null)
    {
      int m = Integer.parseInt((String)localObject1);
      localInteger = Integer.valueOf(m);
    }
    localObject1 = new com/truecaller/common/h/au;
    ((au)localObject1).<init>((Integer)localObject2, (Integer)localObject3, localInteger);
    return (au)localObject1;
  }
  
  public final String f()
  {
    Object localObject = e;
    String str = "BUILD_KEY";
    localObject = ((a)localObject).a(str);
    if (localObject == null) {
      localObject = a(null);
    }
    k.a(localObject, "coreSettings.getString(C…teBuildNameInternal(null)");
    return (String)localObject;
  }
  
  public final String g()
  {
    return a;
  }
  
  public final void h()
  {
    String str = e.a("BUILD_KEY");
    a(str);
  }
  
  public final String i()
  {
    Object localObject1 = c.a.f.j(Settings.BuildName.values());
    Object localObject2 = (b)l.a.a;
    localObject1 = c.m.l.a((i)localObject1, (b)localObject2).a();
    boolean bool3;
    do
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = ((Iterator)localObject1).next();
      Object localObject3 = localObject2;
      localObject3 = (Settings.BuildName)localObject2;
      Object localObject4 = c.getPackageManager();
      String str1;
      if (localObject4 != null)
      {
        str1 = packageName;
        bool2 = ((PackageManager)localObject4).hasSystemFeature(str1);
        localObject4 = Boolean.valueOf(bool2);
      }
      else
      {
        bool2 = false;
        localObject4 = null;
      }
      boolean bool2 = com.truecaller.utils.extensions.c.a((Boolean)localObject4);
      if (!bool2)
      {
        localObject4 = d;
        str1 = packageName;
        String str2 = "it.packageName";
        k.a(str1, str2);
        bool2 = ((com.truecaller.utils.d)localObject4).c(str1);
        if (bool2)
        {
          localObject4 = d;
          localObject3 = packageName;
          str1 = "it.packageName";
          k.a(localObject3, str1);
          bool3 = ((com.truecaller.utils.d)localObject4).d((String)localObject3);
          if (bool3) {}
        }
        else
        {
          bool3 = false;
          localObject3 = null;
          continue;
        }
      }
      bool3 = true;
    } while (!bool3);
    break label219;
    boolean bool1 = false;
    localObject2 = null;
    label219:
    localObject2 = (Settings.BuildName)localObject2;
    if (localObject2 != null) {
      return ((Settings.BuildName)localObject2).name();
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */