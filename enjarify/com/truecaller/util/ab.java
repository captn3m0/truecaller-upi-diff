package com.truecaller.util;

import android.net.Uri;
import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.data.entity.HistoryEvent;
import java.util.List;

public final class ab
  implements aa
{
  private final v a;
  
  public ab(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return aa.class.equals(paramClass);
  }
  
  public final w a()
  {
    v localv = a;
    ab.k localk = new com/truecaller/util/ab$k;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localk.<init>(locale, (byte)0);
    return w.a(localv, localk);
  }
  
  public final w a(long paramLong)
  {
    v localv = a;
    ab.f localf = new com/truecaller/util/ab$f;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localf.<init>(locale, paramLong, (byte)0);
    return w.a(localv, localf);
  }
  
  public final w a(Uri paramUri)
  {
    v localv = a;
    ab.j localj = new com/truecaller/util/ab$j;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localj.<init>(locale, paramUri, (byte)0);
    return w.a(localv, localj);
  }
  
  public final w a(String paramString)
  {
    v localv = a;
    ab.e locale = new com/truecaller/util/ab$e;
    e locale1 = new com/truecaller/androidactors/e;
    locale1.<init>();
    locale.<init>(locale1, paramString, (byte)0);
    return w.a(localv, locale);
  }
  
  public final w a(List paramList)
  {
    v localv = a;
    ab.c localc = new com/truecaller/util/ab$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, paramList, (byte)0);
    return w.a(localv, localc);
  }
  
  public final void a(HistoryEvent paramHistoryEvent)
  {
    v localv = a;
    ab.a locala = new com/truecaller/util/ab$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramHistoryEvent, (byte)0);
    localv.a(locala);
  }
  
  public final w b(Uri paramUri)
  {
    v localv = a;
    ab.h localh = new com/truecaller/util/ab$h;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localh.<init>(locale, paramUri, (byte)0);
    return w.a(localv, localh);
  }
  
  public final w b(String paramString)
  {
    v localv = a;
    ab.d locald = new com/truecaller/util/ab$d;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locald.<init>(locale, paramString, (byte)0);
    return w.a(localv, locald);
  }
  
  public final w c(Uri paramUri)
  {
    v localv = a;
    ab.i locali = new com/truecaller/util/ab$i;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locali.<init>(locale, paramUri, (byte)0);
    return w.a(localv, locali);
  }
  
  public final w c(String paramString)
  {
    v localv = a;
    ab.b localb = new com/truecaller/util/ab$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramString, (byte)0);
    return w.a(localv, localb);
  }
  
  public final w d(String paramString)
  {
    v localv = a;
    ab.g localg = new com/truecaller/util/ab$g;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localg.<init>(locale, paramString, (byte)0);
    return w.a(localv, localg);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */