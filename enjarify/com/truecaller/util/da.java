package com.truecaller.util;

import android.content.Context;
import com.truecaller.utils.l;
import dagger.a.g;
import javax.inject.Provider;

public final class da
  implements dagger.a.d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private da(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static cf a(Context paramContext, l paraml, com.truecaller.utils.d paramd)
  {
    cg localcg = new com/truecaller/util/cg;
    localcg.<init>(paramContext, paraml, paramd);
    return (cf)g.a(localcg, "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static da a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    da localda = new com/truecaller/util/da;
    localda.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localda;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.da
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */