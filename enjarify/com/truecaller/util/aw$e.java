package com.truecaller.util;

import android.graphics.Bitmap;
import android.widget.ImageView;

public abstract interface aw$e
{
  public abstract void a(ImageView paramImageView);
  
  public abstract void a(ImageView paramImageView, Bitmap paramBitmap, String paramString);
  
  public abstract void b(ImageView paramImageView);
}

/* Location:
 * Qualified Name:     com.truecaller.util.aw.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */