package com.truecaller.util;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.provider.ContactsContract.Contacts;
import com.android.b.a.b;
import com.android.b.j;
import com.android.b.n;
import com.android.b.o;
import com.truecaller.androidactors.w;
import com.truecaller.common.h.am;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.data.access.m;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.truepay.d;
import com.truecaller.utils.l;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.c.a.a.a.k;

final class ac
  implements aa
{
  private final bn a;
  private final com.truecaller.androidactors.f b;
  private final com.truecaller.data.access.c c;
  private final m d;
  private final ContentResolver e;
  private final d f;
  private final l g;
  
  ac(bn parambn, com.truecaller.androidactors.f paramf, com.truecaller.data.access.c paramc, ContentResolver paramContentResolver, m paramm, d paramd, l paraml)
  {
    a = parambn;
    b = paramf;
    c = paramc;
    e = paramContentResolver;
    d = paramm;
    f = paramd;
    g = paraml;
  }
  
  /* Error */
  private void a(Uri paramUri, com.android.b.f paramf)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aload_0
    //   3: getfield 32	com/truecaller/util/ac:e	Landroid/content/ContentResolver;
    //   6: astore 4
    //   8: aload 4
    //   10: aload_1
    //   11: invokevirtual 44	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   14: astore_3
    //   15: aload_2
    //   16: aload_3
    //   17: invokevirtual 49	com/android/b/f:a	(Ljava/io/InputStream;)V
    //   20: aload_3
    //   21: invokestatic 54	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   24: return
    //   25: astore_1
    //   26: goto +84 -> 110
    //   29: astore_2
    //   30: goto +4 -> 34
    //   33: astore_2
    //   34: iconst_1
    //   35: istore 5
    //   37: iload 5
    //   39: anewarray 57	java/lang/String
    //   42: astore 4
    //   44: new 59	java/lang/StringBuilder
    //   47: astore 6
    //   49: ldc 61
    //   51: astore 7
    //   53: aload 6
    //   55: aload 7
    //   57: invokespecial 64	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   60: aload 6
    //   62: aload_1
    //   63: invokevirtual 68	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   66: pop
    //   67: ldc 70
    //   69: astore_1
    //   70: aload 6
    //   72: aload_1
    //   73: invokevirtual 73	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   76: pop
    //   77: aload 6
    //   79: aload_2
    //   80: invokevirtual 68	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   83: pop
    //   84: ldc 75
    //   86: astore_1
    //   87: aload 6
    //   89: aload_1
    //   90: invokevirtual 73	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   93: pop
    //   94: aload 6
    //   96: invokevirtual 79	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   99: astore_1
    //   100: aload 4
    //   102: iconst_0
    //   103: aload_1
    //   104: aastore
    //   105: aload_3
    //   106: invokestatic 54	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   109: return
    //   110: aload_3
    //   111: invokestatic 54	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   114: aload_1
    //   115: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	116	0	this	ac
    //   0	116	1	paramUri	Uri
    //   0	116	2	paramf	com.android.b.f
    //   1	110	3	localInputStream	java.io.InputStream
    //   6	95	4	localObject	Object
    //   35	3	5	i	int
    //   47	48	6	localStringBuilder	StringBuilder
    //   51	5	7	str	String
    // Exception table:
    //   from	to	target	type
    //   2	6	25	finally
    //   10	14	25	finally
    //   16	20	25	finally
    //   37	42	25	finally
    //   44	47	25	finally
    //   55	60	25	finally
    //   62	67	25	finally
    //   72	77	25	finally
    //   79	84	25	finally
    //   89	94	25	finally
    //   94	99	25	finally
    //   103	105	25	finally
    //   2	6	29	java/io/IOException
    //   10	14	29	java/io/IOException
    //   16	20	29	java/io/IOException
    //   2	6	33	com/android/b/a/b
    //   10	14	33	com/android/b/a/b
    //   16	20	33	com/android/b/a/b
  }
  
  public final w a()
  {
    a.a();
    return w.b(Boolean.TRUE);
  }
  
  public final w a(long paramLong)
  {
    return w.b(c.a(paramLong));
  }
  
  public final w a(Uri paramUri)
  {
    return w.b(a.a(paramUri));
  }
  
  public final w a(String paramString)
  {
    return w.b(c.b(paramString));
  }
  
  public final w a(List paramList)
  {
    boolean bool1 = paramList.isEmpty();
    if (bool1) {
      return w.b(null);
    }
    dd localdd = new com/truecaller/util/dd;
    localdd.<init>();
    com.android.b.c localc = new com/android/b/c;
    localc.<init>();
    a.add(localdd);
    n localn = new com/android/b/n;
    localn.<init>();
    j localj = new com/android/b/j;
    localj.<init>();
    localj.a(localn);
    android.support.v4.f.a locala = new android/support/v4/f/a;
    int i = paramList.size();
    locala.<init>(i);
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool2 = paramList.hasNext();
      if (!bool2) {
        break;
      }
      Object localObject1 = (Uri)paramList.next();
      if (localObject1 != null)
      {
        a((Uri)localObject1, localj);
        int j = a;
        int k = 1;
        switch (j)
        {
        default: 
          j = b;
          if (j == 0) {
            j = -1073741824;
          }
          break;
        case 3: 
          j = 939524104;
          break;
        case 2: 
          j = 402653192;
          break;
        }
        j = b;
        Object localObject2;
        if (j == k)
        {
          j = -1073741823;
        }
        else
        {
          j = b;
          int m = 2;
          if (j == m)
          {
            j = -1073741822;
          }
          else
          {
            j = 0;
            localObject2 = null;
          }
        }
        try
        {
          localObject2 = o.b(j);
          ((com.android.b.f)localObject2).a(localc);
          a((Uri)localObject1, (com.android.b.f)localObject2);
          localObject2 = a;
          if (localObject2 != null)
          {
            a = ((Uri)localObject1);
            boolean bool3 = ((x)localObject2).a();
            if (bool3) {
              locala.put(localObject1, localObject2);
            }
          }
        }
        catch (b localb)
        {
          String[] arrayOfString = new String[k];
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          String str = "Unable to read vcard from ";
          localStringBuilder.<init>(str);
          localStringBuilder.append(localObject1);
          localStringBuilder.append(" (");
          localStringBuilder.append(localb);
          localStringBuilder.append(")");
          localObject1 = localStringBuilder.toString();
          arrayOfString[0] = localObject1;
        }
      }
    }
    return w.b(locala);
  }
  
  public final void a(HistoryEvent paramHistoryEvent)
  {
    Object localObject = f;
    if (localObject != null)
    {
      localObject = f;
      boolean bool = ((Contact)localObject).S();
      if (bool)
      {
        localObject = (com.truecaller.callhistory.a)b.a();
        Contact localContact = f;
        ((com.truecaller.callhistory.a)localObject).a(paramHistoryEvent, localContact).c();
        return;
      }
    }
    ((com.truecaller.callhistory.a)b.a()).a(paramHistoryEvent);
  }
  
  public final w b(Uri paramUri)
  {
    boolean bool1 = false;
    String str = null;
    if (paramUri == null) {
      return w.b(null);
    }
    Object localObject2 = g;
    Object localObject3 = { "android.permission.READ_CONTACTS" };
    boolean bool2 = ((l)localObject2).a((String[])localObject3);
    if (!bool2)
    {
      new String[1][0] = "Trying to obtain contact info without contacts permission";
      return w.b(null);
    }
    localObject2 = e;
    Object localObject4 = { "display_name", "data1" };
    StringBuilder localStringBuilder = null;
    boolean bool3 = false;
    CharSequence[] arrayOfCharSequence1 = null;
    CharSequence[] arrayOfCharSequence2 = null;
    localObject3 = paramUri;
    paramUri = ((ContentResolver)localObject2).query(paramUri, (String[])localObject4, null, null, null);
    if (paramUri != null) {
      try
      {
        bool2 = paramUri.moveToFirst();
        if (bool2)
        {
          bool1 = false;
          str = null;
          localObject2 = paramUri.getString(0);
          int i = 1;
          localObject4 = paramUri.getString(i);
          localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          arrayOfCharSequence1 = new CharSequence[i];
          arrayOfCharSequence1[0] = localObject2;
          bool3 = am.a(arrayOfCharSequence1);
          arrayOfCharSequence2 = new CharSequence[i];
          arrayOfCharSequence2[0] = localObject4;
          bool1 = k.b(arrayOfCharSequence2) ^ i;
          if (bool3)
          {
            localStringBuilder.append((String)localObject2);
            if (bool1)
            {
              localObject2 = " (";
              localStringBuilder.append((String)localObject2);
            }
          }
          if (bool1)
          {
            localStringBuilder.append((String)localObject4);
            if (bool3)
            {
              str = ")";
              localStringBuilder.append(str);
            }
          }
          str = localStringBuilder.toString();
        }
      }
      finally
      {
        q.a(paramUri);
      }
    }
    q.a(paramUri);
    return w.b(localObject1);
  }
  
  public final w b(String paramString)
  {
    com.truecaller.data.access.c localc = c;
    Uri localUri = TruecallerContract.ah.a().buildUpon().appendQueryParameter("limit", "1").build();
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramString;
    long l = localc.a(localUri, "aggregated_contact_id", "contact_im_id=?", arrayOfString);
    return w.b(localc.a(l));
  }
  
  public final w c(Uri paramUri)
  {
    x localx = null;
    if (paramUri == null) {
      return w.b(null);
    }
    Object localObject2 = g;
    Object localObject3 = { "android.permission.READ_CONTACTS" };
    boolean bool = ((l)localObject2).a((String[])localObject3);
    if (!bool)
    {
      new String[1][0] = "Trying to obtain contact info without contacts permission";
      return w.b(null);
    }
    localObject2 = e;
    String[] arrayOfString = { "lookup", "display_name" };
    CharSequence[] arrayOfCharSequence = null;
    localObject3 = paramUri;
    paramUri = ((ContentResolver)localObject2).query(paramUri, arrayOfString, null, null, null);
    if (paramUri != null) {
      try
      {
        bool = paramUri.moveToFirst();
        if (bool)
        {
          bool = false;
          localObject2 = null;
          localObject3 = paramUri.getString(0);
          int i = 1;
          arrayOfCharSequence = new CharSequence[i];
          arrayOfCharSequence[0] = localObject3;
          bool = am.a(arrayOfCharSequence);
          if (bool)
          {
            localx = new com/truecaller/util/x;
            localx.<init>();
            localObject2 = ContactsContract.Contacts.CONTENT_VCARD_URI;
            localObject2 = Uri.withAppendedPath((Uri)localObject2, (String)localObject3);
            a = ((Uri)localObject2);
            localObject2 = paramUri.getString(i);
            b = ((String)localObject2);
            localx.a(i);
          }
        }
      }
      finally
      {
        q.a(paramUri);
      }
    }
    q.a(paramUri);
    return w.b(localObject1);
  }
  
  public final w c(String paramString)
  {
    m localm = d;
    int[] arrayOfInt = new int[3];
    int[] tmp10_9 = arrayOfInt;
    tmp10_9[0] = 1;
    int[] tmp14_10 = tmp10_9;
    tmp14_10[1] = 4;
    tmp14_10[2] = 8;
    return w.b(Boolean.valueOf(localm.a(paramString, arrayOfInt)));
  }
  
  public final w d(String paramString)
  {
    return w.b(f.a(paramString));
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */