package com.truecaller.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.support.v4.view.r;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.TextView;

public final class ax
  extends AnimatorListenerAdapter
  implements ValueAnimator.AnimatorUpdateListener, View.OnTouchListener
{
  private final ValueAnimator a;
  private final EditText b;
  private final TextView c;
  private final float d;
  private float e;
  private float f;
  private boolean g;
  
  public ax(EditText paramEditText)
  {
    this(paramEditText, (byte)0);
  }
  
  private ax(EditText paramEditText, byte paramByte)
  {
    ValueAnimator localValueAnimator = new android/animation/ValueAnimator;
    localValueAnimator.<init>();
    a = localValueAnimator;
    b = paramEditText;
    c = null;
    float f1 = at.a(paramEditText.getContext(), 5.0F);
    d = f1;
    a();
    a.addUpdateListener(this);
    a.addListener(this);
  }
  
  private void a()
  {
    TextView localTextView1 = c;
    if (localTextView1 == null) {
      return;
    }
    int i = localTextView1.getGravity();
    TextView localTextView2 = c;
    int j = r.g(localTextView2);
    i = Gravity.getAbsoluteGravity(i, j);
    j = i & 0x1;
    float f1 = 0.5F;
    float f2;
    if (j != 0)
    {
      i = 1056964608;
      f2 = 0.5F;
    }
    else
    {
      j = 8388613;
      f3 = 1.175495E-38F;
      i &= j;
      if (i != 0)
      {
        i = 1063675494;
        f2 = 0.9F;
      }
      else
      {
        i = 1036831949;
        f2 = 0.1F;
      }
    }
    localTextView2 = c;
    float f4 = localTextView2.getWidth() * f2;
    localTextView2.setPivotX(f4);
    localTextView1 = c;
    float f3 = localTextView1.getHeight() * f1;
    localTextView1.setPivotY(f3);
  }
  
  private void a(float paramFloat)
  {
    float f1 = 1.0F;
    float f2 = f1 - paramFloat;
    f2 = Math.min(f1, f2);
    f2 = Math.max(0.0F, f2);
    b.setAlpha(f2);
    EditText localEditText = b;
    f1 -= f2;
    float f3 = -localEditText.getWidth();
    f1 *= f3;
    f3 = 2.0F;
    f1 /= f3;
    localEditText.setTranslationX(f1);
    boolean bool = f2 < 0.0F;
    if (bool)
    {
      TextView localTextView = c;
      if (localTextView != null)
      {
        f2 = 0.0F;
        localTextView.setVisibility(0);
        c.setAlpha(paramFloat);
        int i = 1056964608;
        f1 = 0.5F;
        paramFloat = paramFloat * f1 + f1;
        c.setScaleX(paramFloat);
        localTextView = c;
        localTextView.setScaleY(paramFloat);
      }
    }
  }
  
  private float b()
  {
    float f1 = b.getTranslationX();
    float f2 = -(b.getWidth() / 2);
    return f1 / f2;
  }
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    paramAnimator = b;
    float f1 = paramAnimator.getAlpha();
    int i = 0;
    float f2 = 0.0F;
    boolean bool = f1 < 0.0F;
    if (!bool)
    {
      paramAnimator = b;
      String str = "";
      paramAnimator.setText(str);
      b.setTranslationX(0.0F);
      paramAnimator = b;
      i = 1065353216;
      f2 = 1.0F;
      paramAnimator.setAlpha(f2);
      b.requestFocus();
      paramAnimator = c;
      if (paramAnimator != null)
      {
        i = 4;
        f2 = 5.6E-45F;
        paramAnimator.setVisibility(i);
      }
    }
  }
  
  public final void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    float f1 = ((Float)paramValueAnimator.getAnimatedValue()).floatValue();
    a(f1);
  }
  
  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    int i = paramMotionEvent.getAction();
    int k = 2;
    float f1 = 2.8E-45F;
    boolean bool2 = true;
    float f2 = 0.0F;
    EditText localEditText1 = null;
    float f3;
    switch (i)
    {
    default: 
      break;
    case 2: 
      paramView = b.getText();
      if (paramView != null)
      {
        i = paramView.length();
        if (i != 0)
        {
          f3 = f;
          f2 = paramMotionEvent.getX();
          float f4 = e;
          f2 -= f4;
          f3 += f2;
          localEditText1 = b;
          f2 = localEditText1.getTranslationX() + f3;
          boolean bool3 = g;
          if (!bool3)
          {
            f4 = Math.abs(f2);
            float f5 = d;
            bool3 = f4 < f5;
            if (bool3) {}
          }
          else
          {
            bool3 = g;
            if (!bool3)
            {
              EditText localEditText2 = b;
              localEditText2.setTextIsSelectable(false);
              g = bool2;
            }
            f = f3;
            paramView = b;
            i = -(paramView.getWidth() / k);
            f3 = i;
            f2 /= f3;
            a(f2);
          }
          f3 = paramMotionEvent.getX();
          e = f3;
        }
      }
      break;
    case 1: 
    case 3: 
      boolean bool1 = g;
      if (bool1)
      {
        paramView = b;
        paramView.setTextIsSelectable(bool2);
        f3 = b();
        float f6 = f;
        boolean bool4 = f6 < 0.0F;
        if (bool4)
        {
          f6 = 0.1F;
          bool4 = f3 < f6;
          if (!bool4)
          {
            f2 = 1.0F;
            break label339;
          }
        }
        f6 = f;
        bool4 = f6 < 0.0F;
        if (!bool4)
        {
          int j = Math.round(f3);
          f2 = j;
        }
        f3 = b();
        paramMotionEvent = a;
        float[] arrayOfFloat = new float[k];
        arrayOfFloat[0] = f3;
        arrayOfFloat[bool2] = f2;
        paramMotionEvent.setFloatValues(arrayOfFloat);
        paramMotionEvent = a;
        k = 1132068864;
        f1 = 250.0F;
        f3 = Math.abs(f3 - f2) * f1;
        long l = f3;
        paramMotionEvent.setDuration(l);
        paramView = a;
        paramView.start();
      }
      break;
    case 0: 
      label339:
      f3 = paramMotionEvent.getX();
      e = f3;
      f = 0.0F;
      g = false;
      a();
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ax
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */