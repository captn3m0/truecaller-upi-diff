package com.truecaller.util;

import android.net.Uri;
import android.net.Uri.Builder;
import c.g.b.k;
import java.util.Locale;

public final class cc
  implements cb
{
  private final com.truecaller.common.h.u a;
  
  public cc(com.truecaller.common.h.u paramu)
  {
    a = paramu;
  }
  
  public final String a(String paramString)
  {
    if (paramString != null)
    {
      String str1 = a.e(paramString);
      if (str1 != null)
      {
        Uri.Builder localBuilder = Uri.parse("https://truecaller.com").buildUpon().appendPath("search");
        Locale localLocale = Locale.ENGLISH;
        String str2 = "Locale.ENGLISH";
        k.a(localLocale, str2);
        if (str1 != null)
        {
          str1 = str1.toLowerCase(localLocale);
          k.a(str1, "(this as java.lang.String).toLowerCase(locale)");
          paramString = localBuilder.appendPath(str1).appendEncodedPath(paramString).build().toString();
          k.a(paramString, "Uri.parse(TRUECALLER_BAS…              .toString()");
          return paramString;
        }
        paramString = new c/u;
        paramString.<init>("null cannot be cast to non-null type java.lang.String");
        throw paramString;
      }
    }
    return "https://truecaller.com";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cc
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */