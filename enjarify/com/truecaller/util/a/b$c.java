package com.truecaller.util.a;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

final class b$c
{
  final String a;
  final long[] b;
  boolean c;
  b.b d;
  
  private b$c(b paramb, String paramString)
  {
    a = paramString;
    paramb = new long[b.f(paramb)];
    b = paramb;
  }
  
  private static IOException b(String[] paramArrayOfString)
  {
    IOException localIOException = new java/io/IOException;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("unexpected journal line: ");
    paramArrayOfString = Arrays.toString(paramArrayOfString);
    localStringBuilder.append(paramArrayOfString);
    paramArrayOfString = localStringBuilder.toString();
    localIOException.<init>(paramArrayOfString);
    throw localIOException;
  }
  
  public final File a(int paramInt)
  {
    File localFile1 = new java/io/File;
    File localFile2 = b.g(e);
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str1 = a;
    localStringBuilder.append(str1);
    localStringBuilder.append(".");
    localStringBuilder.append(paramInt);
    String str2 = localStringBuilder.toString();
    localFile1.<init>(localFile2, str2);
    return localFile1;
  }
  
  public final String a()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    long[] arrayOfLong = b;
    int i = arrayOfLong.length;
    int j = 0;
    while (j < i)
    {
      long l = arrayOfLong[j];
      char c1 = ' ';
      localStringBuilder.append(c1);
      localStringBuilder.append(l);
      j += 1;
    }
    return localStringBuilder.toString();
  }
  
  final void a(String[] paramArrayOfString)
  {
    int i = paramArrayOfString.length;
    Object localObject = e;
    int j = b.f((b)localObject);
    if (i == j)
    {
      i = 0;
      try
      {
        for (;;)
        {
          j = paramArrayOfString.length;
          if (i >= j) {
            break;
          }
          localObject = b;
          String str = paramArrayOfString[i];
          long l = Long.parseLong(str);
          localObject[i] = l;
          i += 1;
        }
        return;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        throw b(paramArrayOfString);
      }
    }
    throw b(paramArrayOfString);
  }
  
  public final File b(int paramInt)
  {
    File localFile1 = new java/io/File;
    File localFile2 = b.g(e);
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str1 = a;
    localStringBuilder.append(str1);
    localStringBuilder.append(".");
    localStringBuilder.append(paramInt);
    localStringBuilder.append(".tmp");
    String str2 = localStringBuilder.toString();
    localFile1.<init>(localFile2, str2);
    return localFile1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.a.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */