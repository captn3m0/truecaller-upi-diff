package com.truecaller.util.a;

import com.truecaller.util.bi;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class b
  implements Closeable
{
  private static final Pattern a = Pattern.compile("[a-z0-9_-]{1,64}");
  private static final BlockingQueue m;
  private static final ThreadFactory n;
  private static final ThreadPoolExecutor o;
  private static final OutputStream p;
  private final File b;
  private final File c;
  private final File d;
  private final File e;
  private final int f;
  private long g;
  private final int h;
  private long i;
  private Writer j;
  private final LinkedHashMap k;
  private int l;
  private final Callable q;
  
  static
  {
    Object localObject = new java/util/concurrent/LinkedBlockingQueue;
    ((LinkedBlockingQueue)localObject).<init>();
    m = (BlockingQueue)localObject;
    n = bi.a;
    localObject = new java/util/concurrent/ThreadPoolExecutor;
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    BlockingQueue localBlockingQueue = m;
    ThreadFactory localThreadFactory = n;
    ((ThreadPoolExecutor)localObject).<init>(0, 1, 60, localTimeUnit, localBlockingQueue, localThreadFactory);
    o = (ThreadPoolExecutor)localObject;
    localObject = new com/truecaller/util/a/b$d;
    ((b.d)localObject).<init>((byte)0);
    p = (OutputStream)localObject;
  }
  
  private b(File paramFile)
  {
    Object localObject = new java/util/LinkedHashMap;
    int i1 = 1;
    ((LinkedHashMap)localObject).<init>(0, 0.75F, i1);
    k = ((LinkedHashMap)localObject);
    localObject = new com/truecaller/util/a/b$a;
    ((b.a)localObject).<init>(this);
    q = ((Callable)localObject);
    b = paramFile;
    f = i1;
    localObject = new java/io/File;
    ((File)localObject).<init>(paramFile, "journal");
    c = ((File)localObject);
    localObject = new java/io/File;
    ((File)localObject).<init>(paramFile, "journal.tmp");
    d = ((File)localObject);
    localObject = new java/io/File;
    ((File)localObject).<init>(paramFile, "journal.bkp");
    e = ((File)localObject);
    h = i1;
    g = 52428800L;
  }
  
  public static b a(File paramFile)
  {
    Object localObject1 = new java/io/File;
    Object localObject2 = "journal.bkp";
    ((File)localObject1).<init>(paramFile, (String)localObject2);
    boolean bool1 = ((File)localObject1).exists();
    Object localObject3;
    if (bool1)
    {
      localObject2 = new java/io/File;
      localObject3 = "journal";
      ((File)localObject2).<init>(paramFile, (String)localObject3);
      boolean bool2 = ((File)localObject2).exists();
      if (bool2)
      {
        ((File)localObject1).delete();
      }
      else
      {
        bool2 = false;
        localObject3 = null;
        a((File)localObject1, (File)localObject2, false);
      }
    }
    localObject1 = new com/truecaller/util/a/b;
    ((b)localObject1).<init>(paramFile);
    localObject2 = c;
    bool1 = ((File)localObject2).exists();
    if (bool1) {
      try
      {
        ((b)localObject1).c();
        ((b)localObject1).d();
        localObject2 = new java/io/BufferedWriter;
        localObject3 = new java/io/OutputStreamWriter;
        localObject4 = new java/io/FileOutputStream;
        localObject5 = c;
        boolean bool3 = true;
        ((FileOutputStream)localObject4).<init>((File)localObject5, bool3);
        localObject5 = d.a;
        ((OutputStreamWriter)localObject3).<init>((OutputStream)localObject4, (Charset)localObject5);
        ((BufferedWriter)localObject2).<init>((Writer)localObject3);
        j = ((Writer)localObject2);
        return (b)localObject1;
      }
      catch (IOException localIOException)
      {
        localObject3 = System.out;
        Object localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>("DiskLruCache ");
        ((StringBuilder)localObject4).append(paramFile);
        Object localObject5 = " is corrupt: ";
        ((StringBuilder)localObject4).append((String)localObject5);
        String str = localIOException.getMessage();
        ((StringBuilder)localObject4).append(str);
        ((StringBuilder)localObject4).append(", removing");
        str = ((StringBuilder)localObject4).toString();
        ((PrintStream)localObject3).println(str);
        ((b)localObject1).b();
      }
    }
    paramFile.mkdirs();
    localObject1 = new com/truecaller/util/a/b;
    ((b)localObject1).<init>(paramFile);
    ((b)localObject1).e();
    return (b)localObject1;
  }
  
  private void a(b.b paramb)
  {
    try
    {
      Object localObject1 = a;
      b.b localb = d;
      if (localb == paramb)
      {
        boolean bool1 = false;
        paramb = null;
        int i1 = 0;
        localb = null;
        Object localObject2;
        for (;;)
        {
          i2 = h;
          if (i1 >= i2) {
            break;
          }
          localObject2 = ((b.c)localObject1).b(i1);
          b((File)localObject2);
          i1 += 1;
        }
        i1 = l;
        int i2 = 1;
        i1 += i2;
        l = i1;
        i1 = 0;
        localb = null;
        d = null;
        boolean bool2 = c;
        bool1 = false | bool2;
        char c1 = '\n';
        String str;
        if (bool1)
        {
          c = i2;
          paramb = j;
          localObject2 = new java/lang/StringBuilder;
          str = "CLEAN ";
          ((StringBuilder)localObject2).<init>(str);
          str = a;
          ((StringBuilder)localObject2).append(str);
          localObject1 = ((b.c)localObject1).a();
          ((StringBuilder)localObject2).append((String)localObject1);
          ((StringBuilder)localObject2).append(c1);
          localObject1 = ((StringBuilder)localObject2).toString();
          paramb.write((String)localObject1);
        }
        else
        {
          paramb = k;
          localObject2 = a;
          paramb.remove(localObject2);
          paramb = j;
          localObject2 = new java/lang/StringBuilder;
          str = "REMOVE ";
          ((StringBuilder)localObject2).<init>(str);
          localObject1 = a;
          ((StringBuilder)localObject2).append((String)localObject1);
          ((StringBuilder)localObject2).append(c1);
          localObject1 = ((StringBuilder)localObject2).toString();
          paramb.write((String)localObject1);
        }
        paramb = j;
        paramb.flush();
        long l1 = i;
        long l2 = g;
        bool1 = l1 < l2;
        if (!bool1)
        {
          bool1 = f();
          if (!bool1) {}
        }
        else
        {
          paramb = o;
          localObject1 = q;
          paramb.submit((Callable)localObject1);
        }
        return;
      }
      paramb = new java/lang/IllegalStateException;
      paramb.<init>();
      throw paramb;
    }
    finally {}
  }
  
  private static void a(File paramFile1, File paramFile2, boolean paramBoolean)
  {
    if (paramBoolean) {
      b(paramFile2);
    }
    boolean bool = paramFile1.renameTo(paramFile2);
    if (bool) {
      return;
    }
    paramFile1 = new java/io/IOException;
    paramFile1.<init>();
    throw paramFile1;
  }
  
  private static void b(File paramFile)
  {
    boolean bool1 = paramFile.exists();
    if (bool1)
    {
      boolean bool2 = paramFile.delete();
      if (!bool2)
      {
        paramFile = new java/io/IOException;
        paramFile.<init>();
        throw paramFile;
      }
    }
  }
  
  private void c()
  {
    localc = new com/truecaller/util/a/c;
    Object localObject1 = new java/io/FileInputStream;
    Object localObject3 = c;
    ((FileInputStream)localObject1).<init>((File)localObject3);
    localObject3 = d.a;
    localc.<init>((InputStream)localObject1, (Charset)localObject3);
    for (;;)
    {
      try
      {
        localObject1 = localc.a();
        localObject3 = localc.a();
        localObject4 = localc.a();
        str1 = localc.a();
        str2 = localc.a();
        localObject5 = "libcore.io.DiskLruCache";
        boolean bool1 = ((String)localObject5).equals(localObject1);
        if (bool1)
        {
          localObject5 = "1";
          bool1 = ((String)localObject5).equals(localObject3);
          if (bool1)
          {
            i1 = f;
            localObject5 = Integer.toString(i1);
            boolean bool2 = ((String)localObject5).equals(localObject4);
            if (bool2)
            {
              int i2 = h;
              localObject4 = Integer.toString(i2);
              boolean bool3 = ((String)localObject4).equals(str1);
              if (bool3)
              {
                localObject4 = "";
                bool3 = ((String)localObject4).equals(str2);
                if (bool3)
                {
                  i3 = 0;
                  localObject1 = null;
                  i4 = 0;
                  localObject3 = null;
                }
              }
            }
          }
        }
      }
      finally
      {
        Object localObject4;
        String str1;
        String str2;
        Object localObject5;
        int i1;
        int i3;
        int i4;
        int i5;
        int i6;
        String str3;
        d.a(localc);
      }
      try
      {
        localObject4 = localc.a();
        i5 = 32;
        i6 = ((String)localObject4).indexOf(i5);
        i1 = -1;
        if (i6 != i1)
        {
          int i7 = i6 + 1;
          i5 = ((String)localObject4).indexOf(i5, i7);
          if (i5 == i1)
          {
            str3 = ((String)localObject4).substring(i7);
            int i8 = 6;
            if (i6 == i8)
            {
              localObject6 = "REMOVE";
              boolean bool6 = ((String)localObject4).startsWith((String)localObject6);
              if (bool6)
              {
                localObject4 = k;
                ((LinkedHashMap)localObject4).remove(str3);
                continue;
              }
            }
          }
          else
          {
            str3 = ((String)localObject4).substring(i7, i5);
          }
          Object localObject6 = k;
          localObject6 = ((LinkedHashMap)localObject6).get(str3);
          localObject6 = (b.c)localObject6;
          Object localObject7;
          if (localObject6 == null)
          {
            localObject6 = new com/truecaller/util/a/b$c;
            ((b.c)localObject6).<init>(this, str3, (byte)0);
            localObject7 = k;
            ((LinkedHashMap)localObject7).put(str3, localObject6);
          }
          i7 = 5;
          if ((i5 != i1) && (i6 == i7))
          {
            localObject7 = "CLEAN";
            boolean bool7 = ((String)localObject4).startsWith((String)localObject7);
            if (bool7)
            {
              i5 += 1;
              localObject4 = ((String)localObject4).substring(i5);
              str1 = " ";
              localObject4 = ((String)localObject4).split(str1);
              i5 = 1;
              c = i5;
              i5 = 0;
              str1 = null;
              d = null;
              ((b.c)localObject6).a((String[])localObject4);
              continue;
            }
          }
          if ((i5 == i1) && (i6 == i7))
          {
            str3 = "DIRTY";
            boolean bool5 = ((String)localObject4).startsWith(str3);
            if (bool5)
            {
              localObject4 = new com/truecaller/util/a/b$b;
              ((b.b)localObject4).<init>(this, (b.c)localObject6, (byte)0);
              d = ((b.b)localObject4);
              continue;
            }
          }
          if (i5 == i1)
          {
            i5 = 4;
            if (i6 == i5)
            {
              str1 = "READ";
              boolean bool4 = ((String)localObject4).startsWith(str1);
              if (bool4)
              {
                i4 += 1;
                continue;
              }
            }
          }
          localObject1 = new java/io/IOException;
          str1 = "unexpected journal line: ";
          localObject4 = String.valueOf(localObject4);
          localObject4 = str1.concat((String)localObject4);
          ((IOException)localObject1).<init>((String)localObject4);
          throw ((Throwable)localObject1);
        }
        else
        {
          localObject1 = new java/io/IOException;
          str1 = "unexpected journal line: ";
          localObject4 = String.valueOf(localObject4);
          localObject4 = str1.concat((String)localObject4);
          ((IOException)localObject1).<init>((String)localObject4);
          throw ((Throwable)localObject1);
        }
      }
      catch (EOFException localEOFException) {}
    }
    localObject1 = k;
    i3 = ((LinkedHashMap)localObject1).size();
    i4 -= i3;
    l = i4;
    d.a(localc);
    return;
    localObject4 = new java/io/IOException;
    localObject5 = new java/lang/StringBuilder;
    str3 = "unexpected journal header: [";
    ((StringBuilder)localObject5).<init>(str3);
    ((StringBuilder)localObject5).append((String)localObject1);
    localObject1 = ", ";
    ((StringBuilder)localObject5).append((String)localObject1);
    ((StringBuilder)localObject5).append((String)localObject3);
    localObject1 = ", ";
    ((StringBuilder)localObject5).append((String)localObject1);
    ((StringBuilder)localObject5).append(str1);
    localObject1 = ", ";
    ((StringBuilder)localObject5).append((String)localObject1);
    ((StringBuilder)localObject5).append(str2);
    localObject1 = "]";
    ((StringBuilder)localObject5).append((String)localObject1);
    localObject1 = ((StringBuilder)localObject5).toString();
    ((IOException)localObject4).<init>((String)localObject1);
    throw ((Throwable)localObject4);
  }
  
  private void d()
  {
    b(d);
    Iterator localIterator = k.values().iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      b.c localc = (b.c)localIterator.next();
      Object localObject = d;
      int i1 = 0;
      if (localObject == null) {
        for (;;)
        {
          i2 = h;
          if (i1 >= i2) {
            break;
          }
          long l1 = i;
          localObject = b;
          long l2 = localObject[i1];
          l1 += l2;
          i = l1;
          i1 += 1;
        }
      }
      int i2 = 0;
      localObject = null;
      d = null;
      for (;;)
      {
        i2 = h;
        if (i1 >= i2) {
          break;
        }
        b(localc.a(i1));
        localObject = localc.b(i1);
        b((File)localObject);
        i1 += 1;
      }
      localIterator.remove();
    }
  }
  
  /* Error */
  private void e()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 167	com/truecaller/util/a/b:j	Ljava/io/Writer;
    //   6: astore_1
    //   7: aload_1
    //   8: ifnull +12 -> 20
    //   11: aload_0
    //   12: getfield 167	com/truecaller/util/a/b:j	Ljava/io/Writer;
    //   15: astore_1
    //   16: aload_1
    //   17: invokevirtual 407	java/io/Writer:close	()V
    //   20: new 147	java/io/BufferedWriter
    //   23: astore_1
    //   24: new 149	java/io/OutputStreamWriter
    //   27: astore_2
    //   28: new 151	java/io/FileOutputStream
    //   31: astore_3
    //   32: aload_0
    //   33: getfield 116	com/truecaller/util/a/b:d	Ljava/io/File;
    //   36: astore 4
    //   38: aload_3
    //   39: aload 4
    //   41: invokespecial 408	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   44: getstatic 159	com/truecaller/util/a/d:a	Ljava/nio/charset/Charset;
    //   47: astore 4
    //   49: aload_2
    //   50: aload_3
    //   51: aload 4
    //   53: invokespecial 162	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V
    //   56: aload_1
    //   57: aload_2
    //   58: invokespecial 165	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
    //   61: ldc_w 286
    //   64: astore_2
    //   65: aload_1
    //   66: aload_2
    //   67: invokevirtual 248	java/io/Writer:write	(Ljava/lang/String;)V
    //   70: ldc_w 410
    //   73: astore_2
    //   74: aload_1
    //   75: aload_2
    //   76: invokevirtual 248	java/io/Writer:write	(Ljava/lang/String;)V
    //   79: ldc_w 294
    //   82: astore_2
    //   83: aload_1
    //   84: aload_2
    //   85: invokevirtual 248	java/io/Writer:write	(Ljava/lang/String;)V
    //   88: ldc_w 410
    //   91: astore_2
    //   92: aload_1
    //   93: aload_2
    //   94: invokevirtual 248	java/io/Writer:write	(Ljava/lang/String;)V
    //   97: aload_0
    //   98: getfield 103	com/truecaller/util/a/b:f	I
    //   101: istore 5
    //   103: iload 5
    //   105: invokestatic 299	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   108: astore_2
    //   109: aload_1
    //   110: aload_2
    //   111: invokevirtual 248	java/io/Writer:write	(Ljava/lang/String;)V
    //   114: ldc_w 410
    //   117: astore_2
    //   118: aload_1
    //   119: aload_2
    //   120: invokevirtual 248	java/io/Writer:write	(Ljava/lang/String;)V
    //   123: aload_0
    //   124: getfield 122	com/truecaller/util/a/b:h	I
    //   127: istore 5
    //   129: iload 5
    //   131: invokestatic 299	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   134: astore_2
    //   135: aload_1
    //   136: aload_2
    //   137: invokevirtual 248	java/io/Writer:write	(Ljava/lang/String;)V
    //   140: ldc_w 410
    //   143: astore_2
    //   144: aload_1
    //   145: aload_2
    //   146: invokevirtual 248	java/io/Writer:write	(Ljava/lang/String;)V
    //   149: ldc_w 410
    //   152: astore_2
    //   153: aload_1
    //   154: aload_2
    //   155: invokevirtual 248	java/io/Writer:write	(Ljava/lang/String;)V
    //   158: aload_0
    //   159: getfield 92	com/truecaller/util/a/b:k	Ljava/util/LinkedHashMap;
    //   162: astore_2
    //   163: aload_2
    //   164: invokevirtual 382	java/util/LinkedHashMap:values	()Ljava/util/Collection;
    //   167: astore_2
    //   168: aload_2
    //   169: invokeinterface 388 1 0
    //   174: astore_2
    //   175: aload_2
    //   176: invokeinterface 393 1 0
    //   181: istore 6
    //   183: iload 6
    //   185: ifeq +145 -> 330
    //   188: aload_2
    //   189: invokeinterface 397 1 0
    //   194: astore_3
    //   195: aload_3
    //   196: checkcast 219	com/truecaller/util/a/b$c
    //   199: astore_3
    //   200: aload_3
    //   201: getfield 222	com/truecaller/util/a/b$c:d	Lcom/truecaller/util/a/b$b;
    //   204: astore 4
    //   206: bipush 10
    //   208: istore 7
    //   210: aload 4
    //   212: ifnull +54 -> 266
    //   215: new 175	java/lang/StringBuilder
    //   218: astore 4
    //   220: ldc_w 412
    //   223: astore 8
    //   225: aload 4
    //   227: aload 8
    //   229: invokespecial 180	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   232: aload_3
    //   233: getfield 238	com/truecaller/util/a/b$c:a	Ljava/lang/String;
    //   236: astore_3
    //   237: aload 4
    //   239: aload_3
    //   240: invokevirtual 189	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   243: pop
    //   244: aload 4
    //   246: iload 7
    //   248: invokevirtual 243	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   251: pop
    //   252: aload 4
    //   254: invokevirtual 200	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   257: astore_3
    //   258: aload_1
    //   259: aload_3
    //   260: invokevirtual 248	java/io/Writer:write	(Ljava/lang/String;)V
    //   263: goto -88 -> 175
    //   266: new 175	java/lang/StringBuilder
    //   269: astore 4
    //   271: ldc -21
    //   273: astore 8
    //   275: aload 4
    //   277: aload 8
    //   279: invokespecial 180	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   282: aload_3
    //   283: getfield 238	com/truecaller/util/a/b$c:a	Ljava/lang/String;
    //   286: astore 8
    //   288: aload 4
    //   290: aload 8
    //   292: invokevirtual 189	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   295: pop
    //   296: aload_3
    //   297: invokevirtual 240	com/truecaller/util/a/b$c:a	()Ljava/lang/String;
    //   300: astore_3
    //   301: aload 4
    //   303: aload_3
    //   304: invokevirtual 189	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   307: pop
    //   308: aload 4
    //   310: iload 7
    //   312: invokevirtual 243	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   315: pop
    //   316: aload 4
    //   318: invokevirtual 200	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   321: astore_3
    //   322: aload_1
    //   323: aload_3
    //   324: invokevirtual 248	java/io/Writer:write	(Ljava/lang/String;)V
    //   327: goto -152 -> 175
    //   330: aload_1
    //   331: invokevirtual 407	java/io/Writer:close	()V
    //   334: aload_0
    //   335: getfield 112	com/truecaller/util/a/b:c	Ljava/io/File;
    //   338: astore_1
    //   339: aload_1
    //   340: invokevirtual 132	java/io/File:exists	()Z
    //   343: istore 9
    //   345: iconst_1
    //   346: istore 5
    //   348: iload 9
    //   350: ifeq +20 -> 370
    //   353: aload_0
    //   354: getfield 112	com/truecaller/util/a/b:c	Ljava/io/File;
    //   357: astore_1
    //   358: aload_0
    //   359: getfield 120	com/truecaller/util/a/b:e	Ljava/io/File;
    //   362: astore_3
    //   363: aload_1
    //   364: aload_3
    //   365: iload 5
    //   367: invokestatic 138	com/truecaller/util/a/b:a	(Ljava/io/File;Ljava/io/File;Z)V
    //   370: aload_0
    //   371: getfield 116	com/truecaller/util/a/b:d	Ljava/io/File;
    //   374: astore_1
    //   375: aload_0
    //   376: getfield 112	com/truecaller/util/a/b:c	Ljava/io/File;
    //   379: astore_3
    //   380: aconst_null
    //   381: astore 4
    //   383: aload_1
    //   384: aload_3
    //   385: iconst_0
    //   386: invokestatic 138	com/truecaller/util/a/b:a	(Ljava/io/File;Ljava/io/File;Z)V
    //   389: aload_0
    //   390: getfield 120	com/truecaller/util/a/b:e	Ljava/io/File;
    //   393: astore_1
    //   394: aload_1
    //   395: invokevirtual 135	java/io/File:delete	()Z
    //   398: pop
    //   399: new 147	java/io/BufferedWriter
    //   402: astore_1
    //   403: new 149	java/io/OutputStreamWriter
    //   406: astore_3
    //   407: new 151	java/io/FileOutputStream
    //   410: astore 4
    //   412: aload_0
    //   413: getfield 112	com/truecaller/util/a/b:c	Ljava/io/File;
    //   416: astore 10
    //   418: aload 4
    //   420: aload 10
    //   422: iload 5
    //   424: invokespecial 154	java/io/FileOutputStream:<init>	(Ljava/io/File;Z)V
    //   427: getstatic 159	com/truecaller/util/a/d:a	Ljava/nio/charset/Charset;
    //   430: astore_2
    //   431: aload_3
    //   432: aload 4
    //   434: aload_2
    //   435: invokespecial 162	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V
    //   438: aload_1
    //   439: aload_3
    //   440: invokespecial 165	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
    //   443: aload_0
    //   444: aload_1
    //   445: putfield 167	com/truecaller/util/a/b:j	Ljava/io/Writer;
    //   448: aload_0
    //   449: monitorexit
    //   450: return
    //   451: astore_2
    //   452: aload_1
    //   453: invokevirtual 407	java/io/Writer:close	()V
    //   456: aload_2
    //   457: athrow
    //   458: astore_1
    //   459: aload_0
    //   460: monitorexit
    //   461: aload_1
    //   462: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	463	0	this	b
    //   6	447	1	localObject1	Object
    //   458	4	1	localObject2	Object
    //   27	408	2	localObject3	Object
    //   451	6	2	localObject4	Object
    //   31	409	3	localObject5	Object
    //   36	397	4	localObject6	Object
    //   101	322	5	i1	int
    //   181	3	6	bool1	boolean
    //   208	103	7	c1	char
    //   223	68	8	str	String
    //   343	6	9	bool2	boolean
    //   416	5	10	localFile	File
    // Exception table:
    //   from	to	target	type
    //   66	70	451	finally
    //   75	79	451	finally
    //   84	88	451	finally
    //   93	97	451	finally
    //   97	101	451	finally
    //   103	108	451	finally
    //   110	114	451	finally
    //   119	123	451	finally
    //   123	127	451	finally
    //   129	134	451	finally
    //   136	140	451	finally
    //   145	149	451	finally
    //   154	158	451	finally
    //   158	162	451	finally
    //   163	167	451	finally
    //   168	174	451	finally
    //   175	181	451	finally
    //   188	194	451	finally
    //   195	199	451	finally
    //   200	204	451	finally
    //   215	218	451	finally
    //   227	232	451	finally
    //   232	236	451	finally
    //   239	244	451	finally
    //   246	252	451	finally
    //   252	257	451	finally
    //   259	263	451	finally
    //   266	269	451	finally
    //   277	282	451	finally
    //   282	286	451	finally
    //   290	296	451	finally
    //   296	300	451	finally
    //   303	308	451	finally
    //   310	316	451	finally
    //   316	321	451	finally
    //   323	327	451	finally
    //   2	6	458	finally
    //   11	15	458	finally
    //   16	20	458	finally
    //   20	23	458	finally
    //   24	27	458	finally
    //   28	31	458	finally
    //   32	36	458	finally
    //   39	44	458	finally
    //   44	47	458	finally
    //   51	56	458	finally
    //   57	61	458	finally
    //   330	334	458	finally
    //   334	338	458	finally
    //   339	343	458	finally
    //   353	357	458	finally
    //   358	362	458	finally
    //   365	370	458	finally
    //   370	374	458	finally
    //   375	379	458	finally
    //   385	389	458	finally
    //   389	393	458	finally
    //   394	399	458	finally
    //   399	402	458	finally
    //   403	406	458	finally
    //   407	410	458	finally
    //   412	416	458	finally
    //   422	427	458	finally
    //   427	430	458	finally
    //   434	438	458	finally
    //   439	443	458	finally
    //   444	448	458	finally
    //   452	456	458	finally
    //   456	458	458	finally
  }
  
  private boolean f()
  {
    int i1 = l;
    int i2 = 2000;
    if (i1 >= i2)
    {
      LinkedHashMap localLinkedHashMap = k;
      i2 = localLinkedHashMap.size();
      if (i1 >= i2) {
        return true;
      }
    }
    return false;
  }
  
  private void g()
  {
    for (;;)
    {
      long l1 = i;
      long l2 = g;
      boolean bool = l1 < l2;
      if (!bool) {
        break;
      }
      String str = (String)((Map.Entry)k.entrySet().iterator().next()).getKey();
      a(str);
    }
  }
  
  public final long a()
  {
    try
    {
      long l1 = i;
      return l1;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public final void a(long paramLong)
  {
    try
    {
      g = paramLong;
      ThreadPoolExecutor localThreadPoolExecutor = o;
      Callable localCallable = q;
      localThreadPoolExecutor.submit(localCallable);
      return;
    }
    finally {}
  }
  
  public final boolean a(String paramString)
  {
    try
    {
      Object localObject1 = j;
      if (localObject1 != null)
      {
        localObject1 = a;
        localObject1 = ((Pattern)localObject1).matcher(paramString);
        boolean bool1 = ((Matcher)localObject1).matches();
        if (bool1)
        {
          localObject1 = k;
          localObject1 = ((LinkedHashMap)localObject1).get(paramString);
          localObject1 = (b.c)localObject1;
          int i2 = 0;
          localObject2 = null;
          if (localObject1 != null)
          {
            localObject3 = d;
            if (localObject3 == null)
            {
              for (;;)
              {
                int i3 = h;
                if (i2 >= i3) {
                  break;
                }
                localObject3 = ((b.c)localObject1).a(i2);
                boolean bool2 = ((File)localObject3).exists();
                if (bool2)
                {
                  bool2 = ((File)localObject3).delete();
                  if (!bool2)
                  {
                    paramString = new java/io/IOException;
                    localObject1 = "failed to delete ";
                    localObject2 = String.valueOf(localObject3);
                    localObject1 = ((String)localObject1).concat((String)localObject2);
                    paramString.<init>((String)localObject1);
                    throw paramString;
                  }
                }
                long l1 = i;
                long[] arrayOfLong = b;
                long l2 = arrayOfLong[i2];
                l1 -= l2;
                i = l1;
                localObject3 = b;
                long l3 = 0L;
                localObject3[i2] = l3;
                i2 += 1;
              }
              int i1 = l;
              i2 = 1;
              i1 += i2;
              l = i1;
              localObject1 = j;
              localObject3 = new java/lang/StringBuilder;
              String str = "REMOVE ";
              ((StringBuilder)localObject3).<init>(str);
              ((StringBuilder)localObject3).append(paramString);
              char c1 = '\n';
              ((StringBuilder)localObject3).append(c1);
              localObject3 = ((StringBuilder)localObject3).toString();
              ((Writer)localObject1).append((CharSequence)localObject3);
              localObject1 = k;
              ((LinkedHashMap)localObject1).remove(paramString);
              boolean bool3 = f();
              if (bool3)
              {
                paramString = o;
                localObject1 = q;
                paramString.submit((Callable)localObject1);
              }
              return i2;
            }
          }
          return false;
        }
        localObject1 = new java/lang/IllegalArgumentException;
        Object localObject2 = new java/lang/StringBuilder;
        Object localObject3 = "keys must match regex [a-z0-9_-]{1,64}: \"";
        ((StringBuilder)localObject2).<init>((String)localObject3);
        ((StringBuilder)localObject2).append(paramString);
        paramString = "\"";
        ((StringBuilder)localObject2).append(paramString);
        paramString = ((StringBuilder)localObject2).toString();
        ((IllegalArgumentException)localObject1).<init>(paramString);
        throw ((Throwable)localObject1);
      }
      paramString = new java/lang/IllegalStateException;
      localObject1 = "cache is closed";
      paramString.<init>((String)localObject1);
      throw paramString;
    }
    finally {}
  }
  
  public final void b()
  {
    close();
    d.a(b);
  }
  
  public final void close()
  {
    try
    {
      Object localObject1 = j;
      if (localObject1 == null) {
        return;
      }
      localObject1 = new java/util/ArrayList;
      Object localObject3 = k;
      localObject3 = ((LinkedHashMap)localObject3).values();
      ((ArrayList)localObject1).<init>((Collection)localObject3);
      localObject1 = ((ArrayList)localObject1).iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject1).hasNext();
        if (!bool) {
          break;
        }
        localObject3 = ((Iterator)localObject1).next();
        localObject3 = (b.c)localObject3;
        Object localObject4 = d;
        if (localObject4 != null)
        {
          localObject3 = d;
          localObject4 = c;
          ((b)localObject4).a((b.b)localObject3);
        }
      }
      g();
      localObject1 = j;
      ((Writer)localObject1).close();
      localObject1 = null;
      j = null;
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */