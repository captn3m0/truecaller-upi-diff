package com.truecaller.util.a;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

final class c
  implements Closeable
{
  final Charset a;
  private final InputStream b;
  private byte[] c;
  private int d;
  private int e;
  
  public c(InputStream paramInputStream, Charset paramCharset)
  {
    this(paramInputStream, paramCharset, (byte)0);
  }
  
  private c(InputStream paramInputStream, Charset paramCharset, byte paramByte)
  {
    if ((paramInputStream != null) && (paramCharset != null))
    {
      Charset localCharset = d.a;
      paramByte = paramCharset.equals(localCharset);
      if (paramByte != 0)
      {
        b = paramInputStream;
        a = paramCharset;
        paramInputStream = new byte[' '];
        c = paramInputStream;
        return;
      }
      paramInputStream = new java/lang/IllegalArgumentException;
      paramInputStream.<init>("Unsupported encoding");
      throw paramInputStream;
    }
    paramInputStream = new java/lang/NullPointerException;
    paramInputStream.<init>();
    throw paramInputStream;
  }
  
  private void b()
  {
    Object localObject = b;
    byte[] arrayOfByte = c;
    int i = arrayOfByte.length;
    int j = ((InputStream)localObject).read(arrayOfByte, 0, i);
    int k = -1;
    if (j != k)
    {
      d = 0;
      e = j;
      return;
    }
    localObject = new java/io/EOFException;
    ((EOFException)localObject).<init>();
    throw ((Throwable)localObject);
  }
  
  public final String a()
  {
    synchronized (b)
    {
      Object localObject1 = c;
      if (localObject1 != null)
      {
        int i = d;
        int j = e;
        if (i >= j) {
          b();
        }
        i = d;
        int k;
        byte[] arrayOfByte1;
        for (;;)
        {
          j = e;
          k = 10;
          if (i == j) {
            break;
          }
          localObject3 = c;
          j = localObject3[i];
          if (j == k)
          {
            j = d;
            if (i != j)
            {
              localObject3 = c;
              k = i + -1;
              j = localObject3[k];
              m = 13;
              if (j == m) {}
            }
            else
            {
              k = i;
            }
            localObject3 = new java/lang/String;
            arrayOfByte1 = c;
            n = d;
            i1 = d;
            k -= i1;
            Object localObject4 = a;
            localObject4 = ((Charset)localObject4).name();
            ((String)localObject3).<init>(arrayOfByte1, n, k, (String)localObject4);
            i += 1;
            d = i;
            return (String)localObject3;
          }
          i += 1;
        }
        localObject1 = new com/truecaller/util/a/c$1;
        j = e;
        int m = d;
        j = j - m + 80;
        ((c.1)localObject1).<init>(this, j);
        localObject3 = c;
        m = d;
        int n = e;
        int i1 = d;
        n -= i1;
        ((ByteArrayOutputStream)localObject1).write((byte[])localObject3, m, n);
        j = -1;
        e = j;
        b();
        j = d;
        for (;;)
        {
          m = e;
          if (j == m) {
            break;
          }
          arrayOfByte1 = c;
          m = arrayOfByte1[j];
          if (m == k)
          {
            k = d;
            if (j != k)
            {
              byte[] arrayOfByte2 = c;
              m = d;
              n = d;
              n = j - n;
              ((ByteArrayOutputStream)localObject1).write(arrayOfByte2, m, n);
            }
            j += 1;
            d = j;
            localObject1 = ((ByteArrayOutputStream)localObject1).toString();
            return (String)localObject1;
          }
          j += 1;
        }
      }
      localObject1 = new java/io/IOException;
      Object localObject3 = "LineReader is closed";
      ((IOException)localObject1).<init>((String)localObject3);
      throw ((Throwable)localObject1);
    }
  }
  
  public final void close()
  {
    synchronized (b)
    {
      Object localObject1 = c;
      if (localObject1 != null)
      {
        localObject1 = null;
        c = null;
        localObject1 = b;
        ((InputStream)localObject1).close();
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */