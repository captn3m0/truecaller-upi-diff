package com.truecaller.util.a;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

final class d
{
  static final Charset a = Charset.forName("US-ASCII");
  
  static void a(Closeable paramCloseable)
  {
    try
    {
      paramCloseable.close();
      return;
    }
    catch (Exception localException) {}catch (RuntimeException localRuntimeException)
    {
      throw localRuntimeException;
    }
  }
  
  static void a(File paramFile)
  {
    Object localObject = paramFile.listFiles();
    if (localObject != null)
    {
      int i = localObject.length;
      int j = 0;
      while (j < i)
      {
        File localFile = localObject[j];
        boolean bool = localFile.isDirectory();
        if (bool) {
          a(localFile);
        }
        bool = localFile.delete();
        if (bool)
        {
          j += 1;
        }
        else
        {
          paramFile = new java/io/IOException;
          localObject = String.valueOf(localFile);
          localObject = "failed to delete file: ".concat((String)localObject);
          paramFile.<init>((String)localObject);
          throw paramFile;
        }
      }
      return;
    }
    localObject = new java/io/IOException;
    paramFile = String.valueOf(paramFile);
    paramFile = "not a readable directory: ".concat(paramFile);
    ((IOException)localObject).<init>(paramFile);
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */