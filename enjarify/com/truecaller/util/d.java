package com.truecaller.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;
import com.truecaller.common.h.o;
import com.truecaller.data.entity.Contact;

public final class d
{
  public static Intent a(Context paramContext, Contact paramContact)
  {
    boolean bool = paramContact.Z();
    if (bool)
    {
      paramContext = String.valueOf(paramContact.E());
    }
    else
    {
      paramContact = paramContact.q();
      paramContext = t.a(paramContext, paramContact);
    }
    paramContext = o.c(Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, paramContext));
    paramContext.putExtra("finishActivityOnSaveCompleted", true);
    return paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */