package com.truecaller.util;

import dagger.a.d;
import javax.inject.Provider;

public final class cw
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private cw(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static cw a(Provider paramProvider1, Provider paramProvider2)
  {
    cw localcw = new com/truecaller/util/cw;
    localcw.<init>(paramProvider1, paramProvider2);
    return localcw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cw
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */