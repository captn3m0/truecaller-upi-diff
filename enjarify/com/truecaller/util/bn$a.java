package com.truecaller.util;

import com.truecaller.data.access.m;
import com.truecaller.data.entity.Contact;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

final class bn$a
{
  final ArrayList a;
  final List b;
  int c;
  private final List e;
  private final ArrayList f;
  
  private bn$a(bn parambn)
  {
    parambn = new java/util/ArrayList;
    parambn.<init>();
    a = parambn;
    parambn = new java/util/ArrayList;
    parambn.<init>();
    e = parambn;
    parambn = new java/util/ArrayList;
    parambn.<init>();
    f = parambn;
    parambn = new java/util/ArrayList;
    parambn.<init>();
    b = parambn;
    c = 0;
  }
  
  private void c()
  {
    Object localObject = a;
    boolean bool = ((ArrayList)localObject).isEmpty();
    if (bool) {
      return;
    }
    localObject = d.a;
    List localList = Collections.unmodifiableList(a);
    ((m)localObject).a(localList);
    int i = c;
    int j = e.size();
    i += j;
    c = i;
    a.clear();
  }
  
  private void d()
  {
    Object localObject1 = e;
    boolean bool1 = ((List)localObject1).isEmpty();
    if (bool1)
    {
      localObject1 = f;
      bool1 = ((ArrayList)localObject1).isEmpty();
      if (bool1) {
        return;
      }
    }
    localObject1 = d.a;
    Object localObject2 = Collections.unmodifiableList(e);
    List localList = Collections.unmodifiableList(f);
    ((m)localObject1).a((List)localObject2, localList);
    localObject1 = f.iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = ((Contact)((Iterator)localObject1).next()).getId();
      if (localObject2 != null)
      {
        localList = b;
        localList.add(localObject2);
      }
    }
    int i = c;
    int j = e.size();
    i += j;
    c = i;
    e.clear();
    f.clear();
  }
  
  final void a()
  {
    c();
    d();
  }
  
  final void a(Contact paramContact)
  {
    f.add(paramContact);
    b();
  }
  
  final void a(String paramString)
  {
    e.add(paramString);
    b();
  }
  
  final void b()
  {
    Object localObject = a;
    int i = ((ArrayList)localObject).size();
    int j = 100;
    if (i < j)
    {
      localObject = f;
      i = ((ArrayList)localObject).size();
      if (i < j)
      {
        localObject = e;
        i = ((List)localObject).size();
        if (i < j) {
          return;
        }
      }
    }
    a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bn.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */