package com.truecaller.util;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.aj;
import android.support.v4.app.z.d;
import android.support.v4.content.b;
import com.truecaller.analytics.bb;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.c;
import com.truecaller.notificationchannels.e;
import com.truecaller.notifications.a;
import com.truecaller.service.NotificationsService;
import com.truecaller.service.PushNotificationLoggingService;
import com.truecaller.ui.u;
import java.util.concurrent.TimeUnit;

public final class bl
{
  public static PendingIntent a(Context paramContext, Intent paramIntent, int paramInt)
  {
    return PendingIntent.getActivity(paramContext, paramInt, paramIntent, 134217728);
  }
  
  private static Bundle a(String paramString)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("Subtype", paramString);
    return localBundle;
  }
  
  public static void a(Context paramContext)
  {
    Thread localThread = new java/lang/Thread;
    -..Lambda.bl.ABKu3GpXQjKFe8u671PH_SaT2QI localABKu3GpXQjKFe8u671PH_SaT2QI = new com/truecaller/util/-$$Lambda$bl$ABKu3GpXQjKFe8u671PH_SaT2QI;
    localABKu3GpXQjKFe8u671PH_SaT2QI.<init>(paramContext);
    localThread.<init>(localABKu3GpXQjKFe8u671PH_SaT2QI);
    localThread.start();
  }
  
  public static void a(Context paramContext, int paramInt, boolean paramBoolean, String paramString)
  {
    Object localObject = paramContext.getResources();
    Object[] arrayOfObject = new Object[1];
    Integer localInteger = Integer.valueOf(paramInt);
    arrayOfObject[0] = localInteger;
    String str = ((Resources)localObject).getQuantityString(2131755029, paramInt, arrayOfObject);
    localObject = u.a(paramContext);
    a(paramContext, str, (Intent)localObject, paramBoolean, paramString);
  }
  
  public static void a(Context paramContext, com.truecaller.old.data.entity.Notification paramNotification)
  {
    boolean bool1 = NotificationUtil.a();
    if (!bool1)
    {
      long l = TimeUnit.HOURS.toMillis(12);
      NotificationsService.a(paramContext, l, paramNotification);
      return;
    }
    bp localbp = ((bk)paramContext.getApplicationContext()).a();
    int i = 2131886778;
    Object localObject1 = paramContext.getString(i);
    int j = 2131886776;
    Object localObject2 = new Object[1];
    Object localObject3 = null;
    String str1 = paramNotification.a("v");
    localObject2[0] = str1;
    String str2 = paramContext.getString(j, (Object[])localObject2);
    localObject2 = localbp.aI();
    boolean bool2 = ((c)localObject2).d();
    int m = 2131364158;
    if (bool2)
    {
      localObject2 = new android/content/Intent;
      str1 = "android.intent.action.VIEW";
      localObject4 = "u";
      paramNotification = Uri.parse(paramNotification.a((String)localObject4));
      ((Intent)localObject2).<init>(str1, paramNotification);
      int n = 268435456;
      ((Intent)localObject2).setFlags(n);
      paramNotification = a(paramContext, (Intent)localObject2, m);
    }
    else
    {
      paramNotification = u.a(paramContext);
      str1 = "openApp";
      bb.a(paramNotification, "notification", str1);
      localObject2 = aj.a(paramContext);
      paramNotification = ((aj)localObject2).b(paramNotification);
      int k = 134217728;
      paramNotification = paramNotification.a(m, k);
    }
    localObject2 = localbp.aC().a();
    localObject3 = new android/support/v4/app/z$d;
    ((z.d)localObject3).<init>(paramContext, (String)localObject2);
    localObject2 = ((z.d)localObject3).a(2131234787);
    m = b.c(paramContext, 2131100594);
    C = m;
    paramContext = paramContext.getString(2131886117);
    paramContext = ((z.d)localObject2).d(paramContext).a((CharSequence)localObject1).b(str2);
    f = paramNotification;
    paramContext.d(16);
    localObject3 = paramContext.h();
    localObject1 = localbp.W();
    ((a)localObject1).a("OsNotificationUtils", 555);
    Object localObject4 = a("softwareUpdate");
    ((a)localObject1).a("OsNotificationUtils", 555, (android.app.Notification)localObject3, "notificationBackend", (Bundle)localObject4);
  }
  
  static void a(Context paramContext, String paramString1, Intent paramIntent, boolean paramBoolean, String paramString2)
  {
    String str = paramContext.getString(2131886778);
    a(paramContext, str, paramString1, paramIntent, paramBoolean, paramString2);
  }
  
  public static void a(Context paramContext, String paramString1, String paramString2, Intent paramIntent, boolean paramBoolean, String paramString3)
  {
    boolean bool = NotificationUtil.a();
    if (!bool)
    {
      long l = TimeUnit.HOURS.toMillis(12);
      localObject1 = paramString1;
      NotificationsService.a(paramContext, l, paramString1, paramString2, paramIntent, paramString3);
      return;
    }
    bb.a(paramIntent, "notification", "openApp");
    paramIntent = aj.a(paramContext).b(paramIntent).a(0, 134217728);
    Object localObject2 = ((bk)paramContext.getApplicationContext()).a().aC().a();
    Object localObject3 = new android/support/v4/app/z$d;
    ((z.d)localObject3).<init>(paramContext, (String)localObject2);
    int i = 2131234787;
    localObject2 = ((z.d)localObject3).a(i);
    int j = b.c(paramContext, 2131100594);
    C = j;
    j = 2131886117;
    localObject3 = paramContext.getString(j);
    localObject2 = ((z.d)localObject2).d((CharSequence)localObject3);
    paramString1 = ((z.d)localObject2).a(paramString1).b(paramString2);
    if (paramBoolean) {
      paramIntent = PushNotificationLoggingService.a(paramContext, paramIntent);
    }
    f = paramIntent;
    if (paramBoolean) {
      paramString2 = PushNotificationLoggingService.a(paramContext);
    } else {
      paramString2 = null;
    }
    paramString1 = paramString1.b(paramString2);
    paramString1.d(16);
    android.app.Notification localNotification = paramString1.h();
    localObject2 = ((bk)paramContext.getApplicationContext()).a().W();
    ((a)localObject2).a("OsNotificationUtils", 444);
    Object localObject1 = a(paramString3);
    ((a)localObject2).a("OsNotificationUtils", 444, localNotification, "notificationBackend", (Bundle)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */