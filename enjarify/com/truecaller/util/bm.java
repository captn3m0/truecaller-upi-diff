package com.truecaller.util;

import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.b;

public final class bm
  implements Cursor
{
  final com.truecaller.utils.extensions.g b;
  final com.truecaller.utils.extensions.g c;
  final com.truecaller.utils.extensions.g d;
  final com.truecaller.utils.extensions.g e;
  final com.truecaller.utils.extensions.g f;
  final com.truecaller.utils.extensions.g g;
  final com.truecaller.utils.extensions.g h;
  final com.truecaller.utils.extensions.g i;
  final com.truecaller.utils.extensions.g j;
  final com.truecaller.utils.extensions.g k;
  final com.truecaller.utils.extensions.g l;
  final com.truecaller.utils.extensions.g m;
  final com.truecaller.utils.extensions.g n;
  final com.truecaller.utils.extensions.g o;
  final com.truecaller.utils.extensions.g p;
  final com.truecaller.utils.extensions.g q;
  final com.truecaller.utils.extensions.g r;
  private final com.truecaller.utils.extensions.g s;
  private final com.truecaller.utils.extensions.g t;
  private final com.truecaller.utils.extensions.g u;
  private final com.truecaller.utils.extensions.g v;
  private final com.truecaller.utils.extensions.g w;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[22];
    Object localObject = new c/g/b/u;
    b localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "dataId", "getDataId()J");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "contactId", "getContactId()J");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[1] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "lookupKey", "getLookupKey()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[2] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "dataVersion", "getDataVersion()I");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[3] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "mimeType", "getMimeType()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[4] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "displayName", "getDisplayName()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[5] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "starred", "getStarred()I");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[6] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "dataIsSuperPrimary", "getDataIsSuperPrimary()I");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[7] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "phoneNumber", "getPhoneNumber()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[8] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "phoneType", "getPhoneType()I");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[9] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "phoneLabel", "getPhoneLabel()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[10] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "emailAddress", "getEmailAddress()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[11] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "postalStreet", "getPostalStreet()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[12] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "postalPostCode", "getPostalPostCode()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[13] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "postalCity", "getPostalCity()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[14] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "postalCountry", "getPostalCountry()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[15] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "company", "getCompany()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[16] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "jobTitle", "getJobTitle()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[17] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "familyName", "getFamilyName()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[18] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "givenName", "getGivenName()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[19] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "middleName", "getMiddleName()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[20] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(bm.class);
    ((u)localObject).<init>(localb, "note", "getNote()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[21] = localObject;
    a = arrayOfg;
  }
  
  public bm(Cursor paramCursor)
  {
    x = paramCursor;
    long l1 = 0L;
    Object localObject1 = Long.valueOf(l1);
    Object localObject2 = new com/truecaller/utils/extensions/g;
    b localb = w.a(Long.class);
    ((com.truecaller.utils.extensions.g)localObject2).<init>("_id", localb, localObject1);
    s = ((com.truecaller.utils.extensions.g)localObject2);
    Object localObject3 = Long.valueOf(l1);
    Object localObject4 = new com/truecaller/utils/extensions/g;
    localObject1 = w.a(Long.class);
    ((com.truecaller.utils.extensions.g)localObject4).<init>("contact_id", (b)localObject1, localObject3);
    t = ((com.truecaller.utils.extensions.g)localObject4);
    localObject3 = new com/truecaller/utils/extensions/g;
    localObject4 = w.a(String.class);
    ((com.truecaller.utils.extensions.g)localObject3).<init>("lookup", (b)localObject4, null);
    b = ((com.truecaller.utils.extensions.g)localObject3);
    localObject4 = Integer.valueOf(0);
    localObject2 = new com/truecaller/utils/extensions/g;
    localb = w.a(Integer.class);
    ((com.truecaller.utils.extensions.g)localObject2).<init>("data_version", localb, localObject4);
    c = ((com.truecaller.utils.extensions.g)localObject2);
    localObject4 = new com/truecaller/utils/extensions/g;
    localObject2 = w.a(String.class);
    ((com.truecaller.utils.extensions.g)localObject4).<init>("mimetype", (b)localObject2, null);
    d = ((com.truecaller.utils.extensions.g)localObject4);
    localObject4 = new com/truecaller/utils/extensions/g;
    localObject2 = w.a(String.class);
    ((com.truecaller.utils.extensions.g)localObject4).<init>("display_name", (b)localObject2, null);
    u = ((com.truecaller.utils.extensions.g)localObject4);
    localObject4 = Integer.valueOf(0);
    localObject2 = new com/truecaller/utils/extensions/g;
    localb = w.a(Integer.class);
    ((com.truecaller.utils.extensions.g)localObject2).<init>("starred", localb, localObject4);
    v = ((com.truecaller.utils.extensions.g)localObject2);
    localObject4 = Integer.valueOf(0);
    localObject2 = new com/truecaller/utils/extensions/g;
    localb = w.a(Integer.class);
    ((com.truecaller.utils.extensions.g)localObject2).<init>("is_super_primary", localb, localObject4);
    e = ((com.truecaller.utils.extensions.g)localObject2);
    localObject4 = new com/truecaller/utils/extensions/g;
    localObject2 = w.a(String.class);
    ((com.truecaller.utils.extensions.g)localObject4).<init>("data1", (b)localObject2, null);
    f = ((com.truecaller.utils.extensions.g)localObject4);
    localObject3 = Integer.valueOf(0);
    localObject4 = new com/truecaller/utils/extensions/g;
    localObject2 = w.a(Integer.class);
    ((com.truecaller.utils.extensions.g)localObject4).<init>("data2", (b)localObject2, localObject3);
    g = ((com.truecaller.utils.extensions.g)localObject4);
    localObject3 = new com/truecaller/utils/extensions/g;
    localObject4 = w.a(String.class);
    ((com.truecaller.utils.extensions.g)localObject3).<init>("data3", (b)localObject4, null);
    h = ((com.truecaller.utils.extensions.g)localObject3);
    localObject3 = new com/truecaller/utils/extensions/g;
    localObject4 = w.a(String.class);
    ((com.truecaller.utils.extensions.g)localObject3).<init>("data1", (b)localObject4, null);
    w = ((com.truecaller.utils.extensions.g)localObject3);
    localObject3 = new com/truecaller/utils/extensions/g;
    localObject4 = w.a(String.class);
    ((com.truecaller.utils.extensions.g)localObject3).<init>("data4", (b)localObject4, null);
    i = ((com.truecaller.utils.extensions.g)localObject3);
    localObject3 = new com/truecaller/utils/extensions/g;
    localObject4 = w.a(String.class);
    ((com.truecaller.utils.extensions.g)localObject3).<init>("data9", (b)localObject4, null);
    j = ((com.truecaller.utils.extensions.g)localObject3);
    localObject3 = new com/truecaller/utils/extensions/g;
    localObject4 = w.a(String.class);
    ((com.truecaller.utils.extensions.g)localObject3).<init>("data7", (b)localObject4, null);
    k = ((com.truecaller.utils.extensions.g)localObject3);
    localObject3 = new com/truecaller/utils/extensions/g;
    localObject4 = w.a(String.class);
    ((com.truecaller.utils.extensions.g)localObject3).<init>("data10", (b)localObject4, null);
    l = ((com.truecaller.utils.extensions.g)localObject3);
    localObject3 = new com/truecaller/utils/extensions/g;
    localObject4 = w.a(String.class);
    ((com.truecaller.utils.extensions.g)localObject3).<init>("data1", (b)localObject4, null);
    m = ((com.truecaller.utils.extensions.g)localObject3);
    localObject3 = new com/truecaller/utils/extensions/g;
    localObject4 = w.a(String.class);
    ((com.truecaller.utils.extensions.g)localObject3).<init>("data4", (b)localObject4, null);
    n = ((com.truecaller.utils.extensions.g)localObject3);
    localObject3 = new com/truecaller/utils/extensions/g;
    localObject4 = w.a(String.class);
    ((com.truecaller.utils.extensions.g)localObject3).<init>("data3", (b)localObject4, null);
    o = ((com.truecaller.utils.extensions.g)localObject3);
    localObject3 = new com/truecaller/utils/extensions/g;
    localObject4 = w.a(String.class);
    ((com.truecaller.utils.extensions.g)localObject3).<init>("data2", (b)localObject4, null);
    p = ((com.truecaller.utils.extensions.g)localObject3);
    localObject3 = new com/truecaller/utils/extensions/g;
    localObject4 = w.a(String.class);
    ((com.truecaller.utils.extensions.g)localObject3).<init>("data5", (b)localObject4, null);
    q = ((com.truecaller.utils.extensions.g)localObject3);
    localObject3 = new com/truecaller/utils/extensions/g;
    localObject4 = w.a(String.class);
    ((com.truecaller.utils.extensions.g)localObject3).<init>("data1", (b)localObject4, null);
    r = ((com.truecaller.utils.extensions.g)localObject3);
  }
  
  public final long a()
  {
    com.truecaller.utils.extensions.g localg = s;
    Object localObject = this;
    localObject = (Cursor)this;
    c.l.g localg1 = a[0];
    return ((Number)localg.a((Cursor)localObject, localg1)).longValue();
  }
  
  public final long b()
  {
    com.truecaller.utils.extensions.g localg = t;
    Object localObject = this;
    localObject = (Cursor)this;
    c.l.g localg1 = a[1];
    return ((Number)localg.a((Cursor)localObject, localg1)).longValue();
  }
  
  public final String c()
  {
    com.truecaller.utils.extensions.g localg = u;
    Object localObject = this;
    localObject = (Cursor)this;
    c.l.g localg1 = a[5];
    return (String)localg.a((Cursor)localObject, localg1);
  }
  
  public final void close()
  {
    x.close();
  }
  
  public final void copyStringToBuffer(int paramInt, CharArrayBuffer paramCharArrayBuffer)
  {
    x.copyStringToBuffer(paramInt, paramCharArrayBuffer);
  }
  
  public final int d()
  {
    com.truecaller.utils.extensions.g localg = v;
    Object localObject = this;
    localObject = (Cursor)this;
    c.l.g localg1 = a[6];
    return ((Number)localg.a((Cursor)localObject, localg1)).intValue();
  }
  
  public final void deactivate()
  {
    x.deactivate();
  }
  
  public final String e()
  {
    com.truecaller.utils.extensions.g localg = w;
    Object localObject = this;
    localObject = (Cursor)this;
    c.l.g localg1 = a[11];
    return (String)localg.a((Cursor)localObject, localg1);
  }
  
  public final byte[] getBlob(int paramInt)
  {
    return x.getBlob(paramInt);
  }
  
  public final int getColumnCount()
  {
    return x.getColumnCount();
  }
  
  public final int getColumnIndex(String paramString)
  {
    return x.getColumnIndex(paramString);
  }
  
  public final int getColumnIndexOrThrow(String paramString)
  {
    return x.getColumnIndexOrThrow(paramString);
  }
  
  public final String getColumnName(int paramInt)
  {
    return x.getColumnName(paramInt);
  }
  
  public final String[] getColumnNames()
  {
    return x.getColumnNames();
  }
  
  public final int getCount()
  {
    return x.getCount();
  }
  
  public final double getDouble(int paramInt)
  {
    return x.getDouble(paramInt);
  }
  
  public final Bundle getExtras()
  {
    return x.getExtras();
  }
  
  public final float getFloat(int paramInt)
  {
    return x.getFloat(paramInt);
  }
  
  public final int getInt(int paramInt)
  {
    return x.getInt(paramInt);
  }
  
  public final long getLong(int paramInt)
  {
    return x.getLong(paramInt);
  }
  
  public final Uri getNotificationUri()
  {
    return x.getNotificationUri();
  }
  
  public final int getPosition()
  {
    return x.getPosition();
  }
  
  public final short getShort(int paramInt)
  {
    return x.getShort(paramInt);
  }
  
  public final String getString(int paramInt)
  {
    return x.getString(paramInt);
  }
  
  public final int getType(int paramInt)
  {
    return x.getType(paramInt);
  }
  
  public final boolean getWantsAllOnMoveCalls()
  {
    return x.getWantsAllOnMoveCalls();
  }
  
  public final boolean isAfterLast()
  {
    return x.isAfterLast();
  }
  
  public final boolean isBeforeFirst()
  {
    return x.isBeforeFirst();
  }
  
  public final boolean isClosed()
  {
    return x.isClosed();
  }
  
  public final boolean isFirst()
  {
    return x.isFirst();
  }
  
  public final boolean isLast()
  {
    return x.isLast();
  }
  
  public final boolean isNull(int paramInt)
  {
    return x.isNull(paramInt);
  }
  
  public final boolean move(int paramInt)
  {
    return x.move(paramInt);
  }
  
  public final boolean moveToFirst()
  {
    return x.moveToFirst();
  }
  
  public final boolean moveToLast()
  {
    return x.moveToLast();
  }
  
  public final boolean moveToNext()
  {
    return x.moveToNext();
  }
  
  public final boolean moveToPosition(int paramInt)
  {
    return x.moveToPosition(paramInt);
  }
  
  public final boolean moveToPrevious()
  {
    return x.moveToPrevious();
  }
  
  public final void registerContentObserver(ContentObserver paramContentObserver)
  {
    x.registerContentObserver(paramContentObserver);
  }
  
  public final void registerDataSetObserver(DataSetObserver paramDataSetObserver)
  {
    x.registerDataSetObserver(paramDataSetObserver);
  }
  
  public final boolean requery()
  {
    return x.requery();
  }
  
  public final Bundle respond(Bundle paramBundle)
  {
    return x.respond(paramBundle);
  }
  
  public final void setExtras(Bundle paramBundle)
  {
    x.setExtras(paramBundle);
  }
  
  public final void setNotificationUri(ContentResolver paramContentResolver, Uri paramUri)
  {
    x.setNotificationUri(paramContentResolver, paramUri);
  }
  
  public final void unregisterContentObserver(ContentObserver paramContentObserver)
  {
    x.unregisterContentObserver(paramContentObserver);
  }
  
  public final void unregisterDataSetObserver(DataSetObserver paramDataSetObserver)
  {
    x.unregisterDataSetObserver(paramDataSetObserver);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bm
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */