package com.truecaller.util;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.io.Closeable;
import java.io.IOException;

public final class q
{
  public static void a(Cursor paramCursor)
  {
    if (paramCursor != null) {
      paramCursor.close();
    }
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase)
  {
    if (paramSQLiteDatabase != null) {
      paramSQLiteDatabase.close();
    }
  }
  
  public static void a(Closeable paramCloseable)
  {
    if (paramCloseable != null) {
      try
      {
        paramCloseable.close();
        return;
      }
      catch (IOException paramCloseable)
      {
        int i = 1;
        String[] arrayOfString = new String[i];
        paramCloseable = paramCloseable.getMessage();
        arrayOfString[0] = paramCloseable;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */