package com.truecaller.util;

import java.io.Serializable;
import java.util.AbstractSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;

final class s$b
  extends AbstractSet
  implements Serializable, SortedSet
{
  private static final long serialVersionUID = 1582296315990362920L;
  
  private Object readResolve()
  {
    return Collections.EMPTY_SET;
  }
  
  public final Comparator comparator()
  {
    return s.b;
  }
  
  public final boolean contains(Object paramObject)
  {
    return false;
  }
  
  public final Object first()
  {
    return null;
  }
  
  public final SortedSet headSet(Object paramObject)
  {
    return s.a;
  }
  
  public final Iterator iterator()
  {
    return s.c;
  }
  
  public final Object last()
  {
    return null;
  }
  
  public final int size()
  {
    return 0;
  }
  
  public final SortedSet subSet(Object paramObject1, Object paramObject2)
  {
    return s.a;
  }
  
  public final SortedSet tailSet(Object paramObject)
  {
    return s.a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.s.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */