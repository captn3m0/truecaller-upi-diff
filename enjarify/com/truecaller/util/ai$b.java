package com.truecaller.util;

import c.a.f;
import c.d.a.a;
import c.d.c;
import c.f.e;
import c.n.d;
import c.o.b;
import c.x;
import com.truecaller.content.c.ab;
import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;

final class ai$b
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private kotlinx.coroutines.ag c;
  
  ai$b(File paramFile, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/util/ai$b;
    File localFile = b;
    localb.<init>(localFile, paramc);
    paramObject = (kotlinx.coroutines.ag)paramObject;
    c = ((kotlinx.coroutines.ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = com.truecaller.content.c.ag.b();
        localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>();
        localObject1 = (Collection)localObject1;
        int j = 0;
        Object localObject2 = null;
        int k = 0;
        Object localObject3 = null;
        int m;
        Object localObject5;
        for (;;)
        {
          m = 19;
          if (k >= m) {
            break;
          }
          Object localObject4 = paramObject[k];
          c.g.b.k.a(localObject4, "it");
          localObject4 = ((ab)localObject4).a();
          localObject5 = "it.createStatements";
          c.g.b.k.a(localObject4, (String)localObject5);
          localObject4 = (Iterable)f.a((Object[])localObject4);
          c.a.m.a((Collection)localObject1, (Iterable)localObject4);
          k += 1;
        }
        localObject1 = (Collection)localObject1;
        localObject3 = new java/util/ArrayList;
        ((ArrayList)localObject3).<init>();
        localObject3 = (Collection)localObject3;
        while (j < m)
        {
          localObject5 = paramObject[j];
          c.g.b.k.a(localObject5, "it");
          localObject5 = ((ab)localObject5).b();
          String str = "it.createViewsStatements";
          c.g.b.k.a(localObject5, str);
          localObject5 = (Iterable)f.a((Object[])localObject5);
          c.a.m.a((Collection)localObject3, (Iterable)localObject5);
          j += 1;
        }
        localObject3 = (Iterable)localObject3;
        paramObject = c.a.m.c((Collection)localObject1, (Iterable)localObject3);
        localObject1 = b;
        localObject2 = paramObject;
        localObject2 = (Iterable)paramObject;
        localObject3 = (CharSequence)"\n-- END --\n";
        paramObject = c.a.m.a((Iterable)localObject2, (CharSequence)localObject3, null, null, 0, null, null, 62);
        localObject2 = d.a;
        c.g.b.k.b(localObject1, "receiver$0");
        c.g.b.k.b(paramObject, "text");
        c.g.b.k.b(localObject2, "charset");
        paramObject = ((String)paramObject).getBytes((Charset)localObject2);
        c.g.b.k.a(paramObject, "(this as java.lang.String).getBytes(charset)");
        e.a((File)localObject1, (byte[])paramObject);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ai.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */