package com.truecaller.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Handler;
import android.os.Looper;
import android.os.StatFs;
import android.provider.ContactsContract.Contacts;
import android.support.v4.f.g;
import android.text.TextUtils;
import android.widget.ImageView;
import com.truecaller.common.h.am;
import com.truecaller.data.entity.Contact;
import com.truecaller.old.a.b;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.components.n;
import com.truecaller.ui.w;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import okhttp3.c;
import okhttp3.y;
import okhttp3.y.a;

public final class aw
{
  private static final Object b;
  private static volatile g c;
  private static final Handler d;
  private static final Bitmap e;
  public int a;
  private int f;
  private final Context g;
  private final Map h;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    b = localObject;
    localObject = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    ((Handler)localObject).<init>(localLooper);
    d = (Handler)localObject;
    localObject = Bitmap.Config.ALPHA_8;
    int i = 1;
    e = Bitmap.createBitmap(i, i, (Bitmap.Config)localObject);
  }
  
  private aw(Context paramContext)
  {
    int i = 2131230837;
    f = i;
    a = i;
    Object localObject = new java/util/WeakHashMap;
    ((WeakHashMap)localObject).<init>();
    localObject = Collections.synchronizedMap((Map)localObject);
    h = ((Map)localObject);
    paramContext = paramContext.getApplicationContext();
    g = paramContext;
    d(g);
  }
  
  public static Bitmap a(String paramString)
  {
    Object localObject = c;
    if (localObject == null) {
      return null;
    }
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool) {
      return null;
    }
    paramString = (Bitmap)c.get(paramString);
    localObject = e;
    if (paramString == localObject) {
      return null;
    }
    return paramString;
  }
  
  public static Uri a(Uri paramUri)
  {
    Object localObject1 = paramUri.getHost();
    Object localObject2 = "truecaller.com";
    boolean bool = org.c.a.a.a.k.f((CharSequence)localObject1, (CharSequence)localObject2);
    if (bool)
    {
      localObject1 = "1";
      localObject2 = paramUri.getLastPathSegment();
      bool = ((String)localObject1).equals(localObject2);
      if (bool)
      {
        localObject1 = new java/util/ArrayList;
        localObject2 = paramUri.getPathSegments();
        ((ArrayList)localObject1).<init>((Collection)localObject2);
        int i = ((List)localObject1).size() + -1;
        ((List)localObject1).set(i, "3");
        paramUri = paramUri.buildUpon();
        localObject1 = TextUtils.join("/", (Iterable)localObject1);
        return paramUri.path((String)localObject1).build();
      }
    }
    return paramUri;
  }
  
  public static y a(Context paramContext)
  {
    File localFile = new java/io/File;
    paramContext = paramContext.getCacheDir();
    String str = "picasso-cache";
    localFile.<init>(paramContext, str);
    long l1 = 5242880L;
    try
    {
      paramContext = new android/os/StatFs;
      localObject = localFile.getAbsolutePath();
      paramContext.<init>((String)localObject);
      int i = paramContext.getBlockCount();
      long l2 = i;
      int j = paramContext.getBlockSize();
      long l3 = j;
      l2 *= l3;
      l3 = 50;
      l2 /= l3;
      l1 = Math.max(l1, l2);
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      Object localObject;
      TimeUnit localTimeUnit;
      for (;;) {}
    }
    paramContext = new okhttp3/y$a;
    paramContext.<init>();
    localTimeUnit = TimeUnit.SECONDS;
    paramContext = paramContext.a(15, localTimeUnit);
    localTimeUnit = TimeUnit.SECONDS;
    paramContext = paramContext.b(20, localTimeUnit);
    localObject = new okhttp3/c;
    l1 = Math.min(l1, 52428800L);
    ((c)localObject).<init>(localFile, l1);
    return paramContext.a((c)localObject).d();
  }
  
  public static void a()
  {
    g localg = c;
    if (localg != null)
    {
      localg = c;
      localg.evictAll();
    }
  }
  
  public static void a(Context paramContext, String paramString)
  {
    g localg = c;
    if ((localg != null) && (paramString != null))
    {
      localg = c;
      localg.remove(paramString);
    }
    ap.b(paramContext, paramString);
  }
  
  private void a(ImageView paramImageView, Bitmap paramBitmap)
  {
    if (paramImageView != null)
    {
      if (paramBitmap != null)
      {
        Bitmap localBitmap = e;
        if (paramBitmap != localBitmap)
        {
          paramImageView.setImageBitmap(paramBitmap);
          return;
        }
      }
      int i = f;
      if (i > 0) {
        paramImageView.setImageResource(i);
      }
    }
  }
  
  public static void a(String paramString, Bitmap paramBitmap)
  {
    if (paramString == null) {
      return;
    }
    if (paramBitmap == null)
    {
      c.remove(paramString);
      return;
    }
    c.put(paramString, paramBitmap);
  }
  
  public static Bitmap b(String paramString)
  {
    return BitmapFactory.decodeFile(paramString);
  }
  
  public static aw b(Context paramContext)
  {
    aw localaw = new com/truecaller/util/aw;
    localaw.<init>(paramContext);
    return localaw;
  }
  
  private static List b(Context paramContext, Contact paramContact)
  {
    if ((paramContact != null) && (paramContext != null))
    {
      paramContext = new java/util/ArrayList;
      int i = 2;
      paramContext.<init>(i);
      Object localObject = paramContact.E();
      if (localObject != null)
      {
        long l1 = ((Long)localObject).longValue();
        long l2 = 0L;
        boolean bool2 = l1 < l2;
        if (bool2)
        {
          localObject = c(String.valueOf(localObject));
          paramContext.add(localObject);
        }
      }
      boolean bool1 = paramContact.O();
      if (bool1)
      {
        paramContact = c(paramContact.p());
        paramContext.add(paramContact);
      }
      return paramContext;
    }
    return Collections.emptyList();
  }
  
  private void b(n paramn, ImageView paramImageView)
  {
    aw.j localj = new com/truecaller/util/aw$j;
    localj.<init>(paramn, paramImageView);
    paramn = new com/truecaller/util/aw$k;
    paramn.<init>(this, localj);
    b.a.execute(paramn);
  }
  
  private static Uri c(String paramString)
  {
    return ContactsContract.Contacts.CONTENT_URI.buildUpon().appendPath(paramString).appendEncodedPath("photo_uri").build();
  }
  
  public static void c(Context paramContext)
  {
    g localg = c;
    localg.evictAll();
    if (paramContext != null) {
      ap.a(paramContext);
    }
  }
  
  private static g d(Context paramContext)
  {
    ??? = c;
    if (??? == null) {
      synchronized (b)
      {
        Object localObject2 = c;
        if (localObject2 == null)
        {
          int i = com.truecaller.common.h.k.h(paramContext);
          localObject2 = new com/truecaller/util/aw$i;
          i /= 7;
          ((aw.i)localObject2).<init>(i);
          c = (g)localObject2;
        }
      }
    }
    return c;
  }
  
  public final aw a(int paramInt1, int paramInt2)
  {
    f = paramInt1;
    a = paramInt2;
    return this;
  }
  
  public final void a(ImageView paramImageView)
  {
    h.put(paramImageView, null);
  }
  
  public final void a(Contact paramContact)
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Object localObject2 = paramContact.v();
    boolean bool1 = am.a((CharSequence)localObject2);
    if (bool1)
    {
      localObject2 = Uri.parse(paramContact.v());
      ((ArrayList)localObject1).add(localObject2);
    }
    localObject2 = g;
    paramContact = b((Context)localObject2, paramContact);
    ((ArrayList)localObject1).addAll(paramContact);
    paramContact = ((ArrayList)localObject1).iterator();
    for (;;)
    {
      boolean bool2 = paramContact.hasNext();
      if (!bool2) {
        break;
      }
      localObject1 = ((Uri)paramContact.next()).toString();
      c.remove(localObject1);
      localObject2 = c;
      localObject1 = w.f((String)localObject1);
      ((g)localObject2).remove(localObject1);
    }
  }
  
  public final void a(Contact paramContact, ImageView paramImageView, aw.e parame)
  {
    aw.c localc = new com/truecaller/util/aw$c;
    String str = paramContact.v();
    boolean bool1 = Settings.d();
    boolean bool2 = true;
    bool1 ^= bool2;
    localc.<init>(str, bool2, bool1, (byte)0);
    h.put(paramImageView, localc);
    str = paramContact.v();
    bool1 = am.a(str);
    if (bool1)
    {
      localObject1 = (Bitmap)c.get(str);
    }
    else
    {
      bool1 = false;
      localObject1 = null;
    }
    Object localObject2 = e;
    if (localObject1 == localObject2) {
      return;
    }
    if (localObject1 != null)
    {
      a(paramImageView, (Bitmap)localObject1);
      parame.a(paramImageView, (Bitmap)localObject1, str);
      localObject1 = paramContact.v();
      bool1 = am.a((CharSequence)localObject1);
      if (bool1)
      {
        localObject1 = paramContact.v();
        boolean bool3 = ((String)localObject1).equals(str);
        if (bool3) {
          return;
        }
      }
    }
    aw.j localj = new com/truecaller/util/aw$j;
    localj.<init>(localc, paramImageView);
    paramImageView = new com/truecaller/util/aw$b;
    Object localObject1 = paramImageView;
    localObject2 = this;
    paramImageView.<init>(this, paramContact, localj, parame, (byte)0);
    b.a.execute(paramImageView);
  }
  
  public final void a(n paramn, ImageView paramImageView)
  {
    if ((paramImageView != null) && (paramn != null))
    {
      h.put(paramImageView, paramn);
      Object localObject = am.a(paramn.q());
      boolean bool = am.a((CharSequence)localObject);
      if (bool)
      {
        g localg = c;
        localObject = (Bitmap)localg.get(localObject);
      }
      else
      {
        localObject = null;
      }
      if (localObject != null)
      {
        paramn = e;
        if (localObject != paramn) {
          a(paramImageView, (Bitmap)localObject);
        }
      }
      else
      {
        b(paramn, paramImageView);
        int i = a;
        if (i > 0) {
          paramImageView.setImageResource(i);
        }
      }
      return;
    }
  }
  
  public final void a(String paramString, ImageView paramImageView)
  {
    aw.c localc = new com/truecaller/util/aw$c;
    localc.<init>(paramString, false, false, (byte)0);
    a(localc, paramImageView);
  }
  
  final boolean a(aw.j paramj)
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = h;
      ImageView localImageView = b;
      localObject = (n)((Map)localObject).get(localImageView);
      if (localObject != null)
      {
        paramj = a;
        boolean bool = localObject.equals(paramj);
        if (bool) {}
      }
      else
      {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.aw
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */