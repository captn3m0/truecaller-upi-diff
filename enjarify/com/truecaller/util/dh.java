package com.truecaller.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.support.customtabs.c;
import android.support.customtabs.c.a;
import android.text.TextUtils;
import com.truecaller.common.h.o;
import com.truecaller.old.ui.activities.DialogBrowserActivity;
import com.truecaller.utils.ui.b;

public final class dh
{
  public static boolean a(Context paramContext, String paramString, boolean paramBoolean)
  {
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (bool1) {
      return false;
    }
    if (paramBoolean) {
      return o.d(paramContext, paramString);
    }
    Uri localUri = Uri.parse(paramString);
    Object localObject1 = localUri.getScheme();
    String str1 = "tel";
    boolean bool2 = str1.equalsIgnoreCase((String)localObject1);
    boolean bool3 = true;
    if (bool2)
    {
      o.b(paramContext, paramString);
      return bool3;
    }
    str1 = "truecaller";
    bool2 = str1.equalsIgnoreCase((String)localObject1);
    if (bool2)
    {
      o.d(paramContext, paramString);
      return bool3;
    }
    str1 = "market";
    bool2 = str1.equalsIgnoreCase((String)localObject1);
    if (bool2)
    {
      bool2 = o.d(paramContext);
      if (bool2)
      {
        o.d(paramContext, paramString);
        return bool3;
      }
    }
    str1 = "file";
    bool1 = str1.equalsIgnoreCase((String)localObject1);
    if (bool1)
    {
      DialogBrowserActivity.a(paramContext, paramString);
    }
    else
    {
      paramString = new android/support/customtabs/c$a;
      paramString.<init>();
      int i = b.a(paramContext, 2130969548);
      paramString.a(i);
      i = b.a(paramContext, 2130969591);
      paramString.b(i);
      paramString = paramString.a();
      localObject1 = a;
      str1 = "android.intent.extra.REFERRER";
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("android-app://");
      String str2 = paramContext.getPackageName();
      ((StringBuilder)localObject2).append(str2);
      localObject2 = Uri.parse(((StringBuilder)localObject2).toString());
      ((Intent)localObject1).putExtra(str1, (Parcelable)localObject2);
      paramString.a(paramContext, localUri);
    }
    return bool3;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.dh
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */