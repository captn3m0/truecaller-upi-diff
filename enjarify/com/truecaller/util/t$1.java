package com.truecaller.util;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;
import java.io.Closeable;
import java.io.InputStream;

final class t$1
  extends aw.f
{
  t$1(long paramLong, Context paramContext) {}
  
  protected final Bitmap a(BitmapFactory.Options paramOptions)
  {
    Object localObject = ContactsContract.Contacts.CONTENT_URI;
    long l = a;
    localObject = ContentUris.withAppendedId((Uri)localObject, l);
    ContentResolver localContentResolver = b.getContentResolver();
    boolean bool = c;
    localObject = ContactsContract.Contacts.openContactPhotoInputStream(localContentResolver, (Uri)localObject, bool);
    localContentResolver = null;
    try
    {
      paramOptions = BitmapFactory.decodeStream((InputStream)localObject, null, paramOptions);
      return paramOptions;
    }
    finally
    {
      q.a((Closeable)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.t.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */