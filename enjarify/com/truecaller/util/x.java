package com.truecaller.util;

import android.net.Uri;

public final class x
{
  public Uri a;
  public String b;
  public int c;
  
  public final void a(int paramInt)
  {
    if (paramInt >= 0)
    {
      c = paramInt;
      return;
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    localIllegalArgumentException.<init>("Numbers count cannot be less than 0");
    throw localIllegalArgumentException;
  }
  
  public final boolean a()
  {
    Uri localUri = a;
    if (localUri != null)
    {
      int i = c;
      if (i > 0) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        paramObject = (x)paramObject;
        int i = c;
        int j = c;
        if (i != j) {
          return false;
        }
        localObject1 = a;
        if (localObject1 != null)
        {
          localObject2 = a;
          boolean bool2 = ((Uri)localObject1).equals(localObject2);
          if (bool2) {
            break label101;
          }
        }
        else
        {
          localObject1 = a;
          if (localObject1 == null) {
            break label101;
          }
        }
        return false;
        label101:
        localObject1 = b;
        if (localObject1 != null)
        {
          paramObject = b;
          return ((String)localObject1).equals(paramObject);
        }
        paramObject = b;
        if (paramObject == null) {
          return bool1;
        }
        return false;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    Uri localUri = a;
    int i = 0;
    if (localUri != null)
    {
      j = localUri.hashCode();
    }
    else
    {
      j = 0;
      localUri = null;
    }
    j *= 31;
    String str = b;
    if (str != null) {
      i = str.hashCode();
    }
    int j = (j + i) * 31;
    i = c;
    return j + i;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */