package com.truecaller.util;

import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.messaging.h;
import com.truecaller.utils.d;

public final class bw
  implements bv
{
  private final String a;
  private final String b;
  private final String c;
  private final Context d;
  private final h e;
  private final d f;
  
  public bw(Context paramContext, h paramh, d paramd)
  {
    d = paramContext;
    e = paramh;
    f = paramd;
    a = "/raw/tc_message_tone";
    b = "/raw/tc_receive_money";
    c = "/2131821031";
  }
  
  public final Uri a()
  {
    Object localObject = e;
    boolean bool = ((h)localObject).q();
    if (bool)
    {
      localObject = e.r();
      if (localObject == null) {
        return null;
      }
      k.a(localObject, "settings.messagingRingtone ?: return null");
      localObject = Uri.parse((String)localObject);
      RingtoneManager localRingtoneManager = new android/media/RingtoneManager;
      Context localContext = d;
      localRingtoneManager.<init>(localContext);
      localRingtoneManager.setType(2);
      int i = localRingtoneManager.getRingtonePosition((Uri)localObject);
      int j = -1;
      if (i != j) {
        return (Uri)localObject;
      }
    }
    return c();
  }
  
  public final boolean b()
  {
    return e.s();
  }
  
  public final Uri c()
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("android.resource://");
    String str = f.n();
    ((StringBuilder)localObject).append(str);
    str = a;
    ((StringBuilder)localObject).append(str);
    localObject = Uri.parse(((StringBuilder)localObject).toString());
    k.a(localObject, "Uri.parse(\"android.resou…ame()}$MESSAGE_TONE_URI\")");
    return (Uri)localObject;
  }
  
  public final Uri d()
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("android.resource://");
    String str = f.n();
    ((StringBuilder)localObject).append(str);
    str = b;
    ((StringBuilder)localObject).append(str);
    localObject = Uri.parse(((StringBuilder)localObject).toString());
    k.a(localObject, "Uri.parse(\"android.resou…Name()}$TC_PAY_TONE_URI\")");
    return (Uri)localObject;
  }
  
  public final Uri e()
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("android.resource://");
    String str = f.n();
    ((StringBuilder)localObject).append(str);
    str = c;
    ((StringBuilder)localObject).append(str);
    localObject = Uri.parse(((StringBuilder)localObject).toString());
    k.a(localObject, "Uri.parse(\"android.resou…eName()}$FLASH_TONE_URI\")");
    return (Uri)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bw
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */