package com.truecaller.util;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.support.a.a;
import android.util.DisplayMetrics;
import c.a.j.b;
import c.a.y;
import c.f.e;
import c.g.b.k;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.ImageEntity;
import com.truecaller.utils.extensions.Scheme;
import com.truecaller.utils.extensions.l;
import com.truecaller.utils.extensions.r;
import com.truecaller.utils.extensions.s;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

public final class h
  implements g
{
  private final DisplayMetrics a;
  private final Context b;
  
  public h(Context paramContext)
  {
    b = paramContext;
    paramContext = b.getResources();
    k.a(paramContext, "context.resources");
    paramContext = paramContext.getDisplayMetrics();
    a = paramContext;
  }
  
  private static int a(BitmapFactory.Options paramOptions, int paramInt1, int paramInt2)
  {
    int i = outHeight;
    int j = outWidth;
    int k = 1;
    if ((i > paramInt2) || (j > paramInt1))
    {
      i /= 2;
      j /= 2;
      for (;;)
      {
        int m = i / k;
        if (m <= paramInt2) {
          break;
        }
        m = j / k;
        if (m <= paramInt1) {
          break;
        }
        k *= 2;
      }
    }
    return k;
  }
  
  private static int a(String paramString)
  {
    a locala = new android/support/a/a;
    locala.<init>(paramString);
    paramString = "Orientation";
    int i = locala.a(paramString);
    int j = 3;
    if (i != j)
    {
      j = 6;
      if (i != j)
      {
        j = 8;
        if (i != j) {
          return 0;
        }
        return 270;
      }
      return 90;
    }
    return 180;
  }
  
  /* Error */
  private static Uri a(String paramString, BitmapFactory.Options paramOptions)
  {
    // Byte code:
    //   0: aload_1
    //   1: iconst_0
    //   2: putfield 83	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   5: iconst_1
    //   6: istore_2
    //   7: aload_1
    //   8: iload_2
    //   9: putfield 86	android/graphics/BitmapFactory$Options:inSampleSize	I
    //   12: aload_0
    //   13: aload_1
    //   14: invokestatic 92	android/graphics/BitmapFactory:decodeFile	(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   17: astore_3
    //   18: new 94	java/io/FileOutputStream
    //   21: astore 4
    //   23: aload 4
    //   25: aload_0
    //   26: invokespecial 95	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
    //   29: aload_1
    //   30: invokestatic 98	com/truecaller/util/h:b	(Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap$CompressFormat;
    //   33: astore_1
    //   34: bipush 100
    //   36: istore 5
    //   38: aload 4
    //   40: astore 6
    //   42: aload 4
    //   44: checkcast 101	java/io/OutputStream
    //   47: astore 6
    //   49: aload_3
    //   50: aload_1
    //   51: iload 5
    //   53: aload 6
    //   55: invokevirtual 107	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   58: pop
    //   59: aload 4
    //   61: invokevirtual 110	java/io/FileOutputStream:flush	()V
    //   64: aload 4
    //   66: invokevirtual 113	java/io/FileOutputStream:close	()V
    //   69: new 115	java/io/File
    //   72: astore_1
    //   73: aload_1
    //   74: aload_0
    //   75: invokespecial 116	java/io/File:<init>	(Ljava/lang/String;)V
    //   78: aload_1
    //   79: invokestatic 122	android/net/Uri:fromFile	(Ljava/io/File;)Landroid/net/Uri;
    //   82: astore_0
    //   83: goto +18 -> 101
    //   86: astore_0
    //   87: aload_3
    //   88: invokevirtual 125	android/graphics/Bitmap:recycle	()V
    //   91: aload 4
    //   93: invokevirtual 113	java/io/FileOutputStream:close	()V
    //   96: aload_0
    //   97: athrow
    //   98: pop
    //   99: aconst_null
    //   100: astore_0
    //   101: aload_3
    //   102: invokevirtual 125	android/graphics/Bitmap:recycle	()V
    //   105: aload 4
    //   107: invokevirtual 113	java/io/FileOutputStream:close	()V
    //   110: aload_0
    //   111: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	112	0	paramString	String
    //   0	112	1	paramOptions	BitmapFactory.Options
    //   6	3	2	i	int
    //   17	85	3	localBitmap	Bitmap
    //   21	85	4	localFileOutputStream	java.io.FileOutputStream
    //   36	16	5	j	int
    //   40	14	6	localObject	Object
    //   98	1	7	localIOException	IOException
    // Exception table:
    //   from	to	target	type
    //   29	33	86	finally
    //   42	47	86	finally
    //   53	59	86	finally
    //   59	64	86	finally
    //   64	69	86	finally
    //   69	72	86	finally
    //   74	78	86	finally
    //   78	82	86	finally
    //   29	33	98	java/io/IOException
    //   42	47	98	java/io/IOException
    //   53	59	98	java/io/IOException
    //   59	64	98	java/io/IOException
    //   64	69	98	java/io/IOException
    //   69	72	98	java/io/IOException
    //   74	78	98	java/io/IOException
    //   78	82	98	java/io/IOException
  }
  
  /* Error */
  private static ImageEntity a(String paramString, BitmapFactory.Options paramOptions, int paramInt)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: iload_2
    //   3: invokestatic 131	com/truecaller/util/h:c	(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;I)Landroid/graphics/Bitmap;
    //   6: astore_3
    //   7: aload_3
    //   8: ifnull +194 -> 202
    //   11: aload_0
    //   12: invokestatic 132	com/truecaller/util/h:a	(Ljava/lang/String;)I
    //   15: istore 4
    //   17: aload_3
    //   18: iload 4
    //   20: invokestatic 137	com/truecaller/utils/extensions/b:a	(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    //   23: astore_3
    //   24: aload_3
    //   25: ifnonnull +6 -> 31
    //   28: goto +174 -> 202
    //   31: new 94	java/io/FileOutputStream
    //   34: astore 5
    //   36: aload 5
    //   38: aload_0
    //   39: invokespecial 95	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
    //   42: aload_1
    //   43: invokestatic 98	com/truecaller/util/h:b	(Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap$CompressFormat;
    //   46: astore 6
    //   48: bipush 80
    //   50: istore 7
    //   52: aload 5
    //   54: astore 8
    //   56: aload 5
    //   58: checkcast 101	java/io/OutputStream
    //   61: astore 8
    //   63: aload_3
    //   64: aload 6
    //   66: iload 7
    //   68: aload 8
    //   70: invokevirtual 107	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   73: pop
    //   74: aload 5
    //   76: invokevirtual 110	java/io/FileOutputStream:flush	()V
    //   79: aload 5
    //   81: invokevirtual 113	java/io/FileOutputStream:close	()V
    //   84: new 115	java/io/File
    //   87: astore 6
    //   89: aload 6
    //   91: aload_0
    //   92: invokespecial 116	java/io/File:<init>	(Ljava/lang/String;)V
    //   95: aload_1
    //   96: getfield 142	android/graphics/BitmapFactory$Options:outMimeType	Ljava/lang/String;
    //   99: astore 9
    //   101: aload 6
    //   103: invokestatic 122	android/net/Uri:fromFile	(Ljava/io/File;)Landroid/net/Uri;
    //   106: astore 8
    //   108: aload_3
    //   109: invokevirtual 146	android/graphics/Bitmap:getWidth	()I
    //   112: istore 10
    //   114: aload_3
    //   115: invokevirtual 149	android/graphics/Bitmap:getHeight	()I
    //   118: istore 11
    //   120: iconst_1
    //   121: istore 12
    //   123: aload 6
    //   125: invokestatic 154	com/truecaller/utils/extensions/l:a	(Ljava/io/File;)J
    //   128: lstore 13
    //   130: aload 9
    //   132: aload 8
    //   134: iload 10
    //   136: iload 11
    //   138: iload 12
    //   140: lload 13
    //   142: invokestatic 159	com/truecaller/messaging/data/types/Entity:a	(Ljava/lang/String;Landroid/net/Uri;IIZJ)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   145: astore_0
    //   146: aload_0
    //   147: instanceof 161
    //   150: istore 15
    //   152: iload 15
    //   154: ifne +5 -> 159
    //   157: aconst_null
    //   158: astore_0
    //   159: aload_0
    //   160: checkcast 161	com/truecaller/messaging/data/types/ImageEntity
    //   163: astore_0
    //   164: aload_3
    //   165: invokevirtual 125	android/graphics/Bitmap:recycle	()V
    //   168: aload 5
    //   170: invokevirtual 113	java/io/FileOutputStream:close	()V
    //   173: goto +27 -> 200
    //   176: astore_0
    //   177: aload_3
    //   178: invokevirtual 125	android/graphics/Bitmap:recycle	()V
    //   181: aload 5
    //   183: invokevirtual 113	java/io/FileOutputStream:close	()V
    //   186: aload_0
    //   187: athrow
    //   188: pop
    //   189: aload_3
    //   190: invokevirtual 125	android/graphics/Bitmap:recycle	()V
    //   193: aload 5
    //   195: invokevirtual 113	java/io/FileOutputStream:close	()V
    //   198: aconst_null
    //   199: astore_0
    //   200: aload_0
    //   201: areturn
    //   202: aconst_null
    //   203: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	204	0	paramString	String
    //   0	204	1	paramOptions	BitmapFactory.Options
    //   0	204	2	paramInt	int
    //   6	184	3	localBitmap	Bitmap
    //   15	4	4	i	int
    //   34	160	5	localFileOutputStream	java.io.FileOutputStream
    //   46	78	6	localObject1	Object
    //   50	17	7	j	int
    //   54	79	8	localObject2	Object
    //   99	32	9	str	String
    //   112	23	10	k	int
    //   118	19	11	m	int
    //   121	18	12	bool1	boolean
    //   128	13	13	l	long
    //   150	3	15	bool2	boolean
    //   188	1	15	localIOException	IOException
    // Exception table:
    //   from	to	target	type
    //   42	46	176	finally
    //   56	61	176	finally
    //   68	74	176	finally
    //   74	79	176	finally
    //   79	84	176	finally
    //   84	87	176	finally
    //   91	95	176	finally
    //   95	99	176	finally
    //   101	106	176	finally
    //   108	112	176	finally
    //   114	118	176	finally
    //   123	128	176	finally
    //   140	145	176	finally
    //   159	163	176	finally
    //   42	46	188	java/io/IOException
    //   56	61	188	java/io/IOException
    //   68	74	188	java/io/IOException
    //   74	79	188	java/io/IOException
    //   79	84	188	java/io/IOException
    //   84	87	188	java/io/IOException
    //   91	95	188	java/io/IOException
    //   95	99	188	java/io/IOException
    //   101	106	188	java/io/IOException
    //   108	112	188	java/io/IOException
    //   114	118	188	java/io/IOException
    //   123	128	188	java/io/IOException
    //   140	145	188	java/io/IOException
    //   159	163	188	java/io/IOException
  }
  
  private final File a(Bitmap paramBitmap)
  {
    File localFile1 = b.getCacheDir();
    File localFile2 = e.a("image", ".jpg", localFile1);
    Bitmap.CompressFormat localCompressFormat = Bitmap.CompressFormat.PNG;
    boolean bool = a(paramBitmap, localFile2, localCompressFormat);
    if (bool) {
      return localFile2;
    }
    return null;
  }
  
  /* Error */
  private static boolean a(Bitmap paramBitmap, File paramFile, Bitmap.CompressFormat paramCompressFormat)
  {
    // Byte code:
    //   0: aload_0
    //   1: ldc -71
    //   3: invokestatic 17	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_1
    //   7: ldc -69
    //   9: invokestatic 17	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   12: aload_2
    //   13: ldc -67
    //   15: invokestatic 17	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   18: new 94	java/io/FileOutputStream
    //   21: astore_3
    //   22: aload_3
    //   23: aload_1
    //   24: invokespecial 192	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   27: bipush 100
    //   29: istore 4
    //   31: aload_3
    //   32: astore 5
    //   34: aload_3
    //   35: checkcast 101	java/io/OutputStream
    //   38: astore 5
    //   40: aload_0
    //   41: aload_2
    //   42: iload 4
    //   44: aload 5
    //   46: invokevirtual 107	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   49: pop
    //   50: aload_3
    //   51: invokevirtual 110	java/io/FileOutputStream:flush	()V
    //   54: aload_3
    //   55: invokevirtual 113	java/io/FileOutputStream:close	()V
    //   58: iconst_1
    //   59: ireturn
    //   60: astore_0
    //   61: goto +19 -> 80
    //   64: astore_0
    //   65: aload_0
    //   66: checkcast 194	java/lang/Throwable
    //   69: astore_0
    //   70: aload_0
    //   71: invokestatic 200	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   74: aload_3
    //   75: invokevirtual 113	java/io/FileOutputStream:close	()V
    //   78: iconst_0
    //   79: ireturn
    //   80: aload_3
    //   81: invokevirtual 113	java/io/FileOutputStream:close	()V
    //   84: aload_0
    //   85: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	86	0	paramBitmap	Bitmap
    //   0	86	1	paramFile	File
    //   0	86	2	paramCompressFormat	Bitmap.CompressFormat
    //   21	60	3	localFileOutputStream	java.io.FileOutputStream
    //   29	14	4	i	int
    //   32	13	5	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   34	38	60	finally
    //   44	50	60	finally
    //   50	54	60	finally
    //   65	69	60	finally
    //   70	74	60	finally
    //   34	38	64	java/io/IOException
    //   44	50	64	java/io/IOException
    //   50	54	64	java/io/IOException
  }
  
  private static boolean a(BitmapFactory.Options paramOptions)
  {
    int i = outWidth;
    int j = 1280;
    if (i <= j)
    {
      int k = outHeight;
      if (k <= j) {
        return false;
      }
    }
    return true;
  }
  
  private static int b(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    float f1 = paramInt1 + paramInt2;
    float f2 = 2.0F;
    f1 /= f2;
    float f3 = (paramInt3 + paramInt4) / f2;
    return (int)((f1 + f3) / f2) & 0xFF;
  }
  
  private static Bitmap.CompressFormat b(BitmapFactory.Options paramOptions)
  {
    String str = outMimeType;
    if (str == null) {
      return Bitmap.CompressFormat.JPEG;
    }
    paramOptions = outMimeType;
    k.a(paramOptions, "outMimeType");
    str = "png";
    boolean bool = c.n.m.c(paramOptions, str, false);
    if (bool) {
      return Bitmap.CompressFormat.PNG;
    }
    return Bitmap.CompressFormat.JPEG;
  }
  
  private final Uri b(Bitmap paramBitmap)
  {
    int i = paramBitmap.getWidth();
    int j = 1280;
    Object localObject;
    if (i <= j)
    {
      i = paramBitmap.getHeight();
      if (i <= j)
      {
        i = 0;
        localObject = null;
        break label37;
      }
    }
    i = 1;
    label37:
    if (i == 0)
    {
      paramBitmap = a(paramBitmap);
      if (paramBitmap == null) {
        return null;
      }
      return Uri.fromFile(paramBitmap);
    }
    paramBitmap = a(paramBitmap);
    if (paramBitmap == null) {
      return null;
    }
    try
    {
      localObject = Uri.fromFile(paramBitmap);
      String str = "Uri.fromFile(file)";
      k.a(localObject, str);
      localObject = g((Uri)localObject);
      paramBitmap = paramBitmap.getPath();
      k.a(paramBitmap, "file.path");
      return b(paramBitmap, (BitmapFactory.Options)localObject, j);
    }
    catch (IOException localIOException) {}
    return null;
  }
  
  /* Error */
  private static Uri b(String paramString, BitmapFactory.Options paramOptions, int paramInt)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: iload_2
    //   3: invokestatic 131	com/truecaller/util/h:c	(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;I)Landroid/graphics/Bitmap;
    //   6: astore_3
    //   7: aconst_null
    //   8: astore 4
    //   10: aload_3
    //   11: ifnull +117 -> 128
    //   14: aload_0
    //   15: invokestatic 132	com/truecaller/util/h:a	(Ljava/lang/String;)I
    //   18: istore 5
    //   20: aload_3
    //   21: iload 5
    //   23: invokestatic 137	com/truecaller/utils/extensions/b:a	(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    //   26: astore_3
    //   27: aload_3
    //   28: ifnonnull +6 -> 34
    //   31: goto +97 -> 128
    //   34: new 94	java/io/FileOutputStream
    //   37: astore 6
    //   39: aload 6
    //   41: aload_0
    //   42: invokespecial 95	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
    //   45: aload_1
    //   46: invokestatic 98	com/truecaller/util/h:b	(Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap$CompressFormat;
    //   49: astore_1
    //   50: bipush 80
    //   52: istore 7
    //   54: aload 6
    //   56: astore 8
    //   58: aload 6
    //   60: checkcast 101	java/io/OutputStream
    //   63: astore 8
    //   65: aload_3
    //   66: aload_1
    //   67: iload 7
    //   69: aload 8
    //   71: invokevirtual 107	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   74: pop
    //   75: aload 6
    //   77: invokevirtual 110	java/io/FileOutputStream:flush	()V
    //   80: aload 6
    //   82: invokevirtual 113	java/io/FileOutputStream:close	()V
    //   85: new 115	java/io/File
    //   88: astore_1
    //   89: aload_1
    //   90: aload_0
    //   91: invokespecial 116	java/io/File:<init>	(Ljava/lang/String;)V
    //   94: aload_1
    //   95: invokestatic 122	android/net/Uri:fromFile	(Ljava/io/File;)Landroid/net/Uri;
    //   98: astore 4
    //   100: goto +16 -> 116
    //   103: astore_0
    //   104: aload_3
    //   105: invokevirtual 125	android/graphics/Bitmap:recycle	()V
    //   108: aload 6
    //   110: invokevirtual 113	java/io/FileOutputStream:close	()V
    //   113: aload_0
    //   114: athrow
    //   115: pop
    //   116: aload_3
    //   117: invokevirtual 125	android/graphics/Bitmap:recycle	()V
    //   120: aload 6
    //   122: invokevirtual 113	java/io/FileOutputStream:close	()V
    //   125: aload 4
    //   127: areturn
    //   128: aconst_null
    //   129: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	130	0	paramString	String
    //   0	130	1	paramOptions	BitmapFactory.Options
    //   0	130	2	paramInt	int
    //   6	111	3	localBitmap	Bitmap
    //   8	118	4	localUri	Uri
    //   18	4	5	i	int
    //   37	84	6	localFileOutputStream	java.io.FileOutputStream
    //   52	16	7	j	int
    //   56	14	8	localObject	Object
    //   115	1	9	localIOException	IOException
    // Exception table:
    //   from	to	target	type
    //   45	49	103	finally
    //   58	63	103	finally
    //   69	75	103	finally
    //   75	80	103	finally
    //   80	85	103	finally
    //   85	88	103	finally
    //   90	94	103	finally
    //   94	98	103	finally
    //   45	49	115	java/io/IOException
    //   58	63	115	java/io/IOException
    //   69	75	115	java/io/IOException
    //   75	80	115	java/io/IOException
    //   80	85	115	java/io/IOException
    //   85	88	115	java/io/IOException
    //   90	94	115	java/io/IOException
    //   94	98	115	java/io/IOException
  }
  
  private static Bitmap c(String paramString, BitmapFactory.Options paramOptions, int paramInt)
  {
    int i = 1;
    inJustDecodeBounds = i;
    BitmapFactory.decodeFile(paramString, paramOptions);
    int j = outWidth;
    int k = outHeight;
    float f1 = j;
    float f2 = paramInt;
    float f3 = f1 / f2;
    float f4 = k;
    f2 = f4 / f2;
    f2 = Math.max(f3, f2);
    f1 /= f2;
    j = (int)f1;
    j = Math.max(i, j);
    paramInt = (int)(f4 / f2);
    paramInt = Math.max(i, paramInt);
    f4 = 0.0F;
    inJustDecodeBounds = false;
    k = a(paramOptions, j, paramInt);
    inSampleSize = k;
    paramString = BitmapFactory.decodeFile(paramString, paramOptions);
    paramOptions = "BitmapFactory.decodeFile(path, options)";
    k.a(paramString, paramOptions);
    int m = paramString.getWidth();
    if (m == j)
    {
      m = paramString.getHeight();
      if (m == paramInt) {
        return paramString;
      }
    }
    paramOptions = Bitmap.createScaledBitmap(paramString, j, paramInt, i);
    k.a(paramOptions, "Bitmap.createScaledBitma…idth, targetHeight, true)");
    paramString.recycle();
    return paramOptions;
  }
  
  /* Error */
  private final BitmapFactory.Options g(Uri paramUri)
  {
    // Byte code:
    //   0: new 55	android/graphics/BitmapFactory$Options
    //   3: astore_2
    //   4: aload_2
    //   5: invokespecial 253	android/graphics/BitmapFactory$Options:<init>	()V
    //   8: iconst_1
    //   9: istore_3
    //   10: aload_2
    //   11: iload_3
    //   12: putfield 83	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   15: aload_1
    //   16: invokestatic 258	com/truecaller/utils/extensions/r:a	(Landroid/net/Uri;)Lcom/truecaller/utils/extensions/Scheme;
    //   19: astore 4
    //   21: aload 4
    //   23: ifnull +120 -> 143
    //   26: getstatic 263	com/truecaller/util/i:a	[I
    //   29: astore 5
    //   31: aload 4
    //   33: invokevirtual 268	com/truecaller/utils/extensions/Scheme:ordinal	()I
    //   36: istore_3
    //   37: aload 5
    //   39: iload_3
    //   40: iaload
    //   41: istore_3
    //   42: iload_3
    //   43: tableswitch	default:+21->64, 1:+87->130, 2:+24->67
    //   64: goto +79 -> 143
    //   67: aload_0
    //   68: getfield 23	com/truecaller/util/h:b	Landroid/content/Context;
    //   71: invokevirtual 272	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   74: aload_1
    //   75: invokevirtual 278	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   78: checkcast 280	java/io/Closeable
    //   81: astore_1
    //   82: iconst_0
    //   83: istore_3
    //   84: aconst_null
    //   85: astore 4
    //   87: aload_1
    //   88: astore 5
    //   90: aload_1
    //   91: checkcast 282	java/io/InputStream
    //   94: astore 5
    //   96: aload 5
    //   98: aconst_null
    //   99: aload_2
    //   100: invokestatic 286	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   103: pop
    //   104: aload_1
    //   105: aconst_null
    //   106: invokestatic 291	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   109: goto +32 -> 141
    //   112: astore_2
    //   113: goto +9 -> 122
    //   116: astore_2
    //   117: aload_2
    //   118: astore 4
    //   120: aload_2
    //   121: athrow
    //   122: aload_1
    //   123: aload 4
    //   125: invokestatic 291	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   128: aload_2
    //   129: athrow
    //   130: aload_1
    //   131: invokevirtual 292	android/net/Uri:getPath	()Ljava/lang/String;
    //   134: astore_1
    //   135: aload_1
    //   136: aload_2
    //   137: invokestatic 92	android/graphics/BitmapFactory:decodeFile	(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   140: pop
    //   141: aload_2
    //   142: areturn
    //   143: new 294	java/lang/IllegalArgumentException
    //   146: astore_2
    //   147: new 296	java/lang/StringBuilder
    //   150: astore 4
    //   152: aload 4
    //   154: ldc_w 298
    //   157: invokespecial 299	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   160: aload_1
    //   161: invokevirtual 302	android/net/Uri:getScheme	()Ljava/lang/String;
    //   164: astore_1
    //   165: aload 4
    //   167: aload_1
    //   168: invokevirtual 306	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   171: pop
    //   172: aload 4
    //   174: ldc_w 308
    //   177: invokevirtual 306	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   180: pop
    //   181: aload 4
    //   183: invokevirtual 311	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   186: astore_1
    //   187: aload_2
    //   188: aload_1
    //   189: invokespecial 312	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   192: aload_2
    //   193: checkcast 194	java/lang/Throwable
    //   196: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	197	0	this	h
    //   0	197	1	paramUri	Uri
    //   3	97	2	localOptions1	BitmapFactory.Options
    //   112	1	2	localObject1	Object
    //   116	26	2	localOptions2	BitmapFactory.Options
    //   146	47	2	localIllegalArgumentException	IllegalArgumentException
    //   9	75	3	i	int
    //   19	163	4	localObject2	Object
    //   29	68	5	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   120	122	112	finally
    //   90	94	116	finally
    //   99	104	116	finally
  }
  
  public final int a()
  {
    int i = a.widthPixels;
    int j = a.heightPixels;
    return (int)(Math.min(i, j) * 0.667F);
  }
  
  public final Bitmap a(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
  {
    Object localObject1 = paramArrayOfByte;
    int i = paramInt1;
    int k = paramInt2;
    Object localObject2 = "thumbnail";
    k.b(paramArrayOfByte, (String)localObject2);
    int m = paramArrayOfByte.length;
    int n = 0;
    float f1 = 0.0F;
    Object localObject3 = null;
    int i1 = 1;
    if (m == 0)
    {
      m = 1;
      f2 = Float.MIN_VALUE;
    }
    else
    {
      m = 0;
      f2 = 0.0F;
      localObject2 = null;
    }
    cl localcl = null;
    if (m != 0) {
      return null;
    }
    float f2 = Math.max(paramInt1, paramInt2);
    int i2 = localObject1[0];
    float f3 = i2;
    f2 /= f3;
    f3 = i;
    float f4 = f3 / f2;
    int i3 = (int)f4;
    float f5 = k;
    f2 = f5 / f2;
    m = (int)f2;
    int i4 = localObject1.length;
    int i5 = i3 * m * 2 + i1;
    if (i4 != i5) {
      return null;
    }
    localcl = new com/truecaller/util/cl;
    localcl.<init>(i3, m);
    localObject2 = "receiver$0";
    k.b(localObject1, (String)localObject2);
    m = localObject1.length;
    if (m == 0)
    {
      m = 1;
      f2 = Float.MIN_VALUE;
    }
    else
    {
      m = 0;
      f2 = 0.0F;
      localObject2 = null;
    }
    if (m != 0)
    {
      localObject1 = (Iterable)y.a;
    }
    else
    {
      localObject2 = new c/a/j$b;
      ((j.b)localObject2).<init>((byte[])localObject1);
      localObject1 = localObject2;
      localObject1 = (Iterable)localObject2;
    }
    localObject1 = (Iterable)c.a.m.c((Iterable)localObject1, i1);
    localObject2 = "src";
    k.b(localObject1, (String)localObject2);
    localObject1 = ((Iterable)localObject1).iterator();
    m = a;
    i4 = b;
    m *= i4;
    while (n < m)
    {
      Number localNumber1 = (Number)((Iterator)localObject1).next();
      i4 = (localNumber1.byteValue() & 0xFF) << 8;
      Number localNumber2 = (Number)((Iterator)localObject1).next();
      i5 = localNumber2.byteValue() & 0xFF;
      i4 |= i5;
      i5 = (i4 >> 11 & 0x1F) * 527 + 23 >> 6;
      int i6 = (i4 >> 5 & 0x3F) * 259 + 23 >> 6;
      i4 = (i4 & 0x1F) * 527 + 23 >> 6;
      int[] arrayOfInt = c;
      int i7 = -16777216;
      i5 = i5 << 16 | i7;
      i6 <<= 8;
      i5 |= i6;
      i4 |= i5;
      arrayOfInt[n] = i4;
      n += 1;
    }
    int i8 = a();
    int j;
    if ((i < i8) && (k < i8))
    {
      i8 = k;
    }
    else
    {
      f3 /= f5;
      f6 = 1.0F;
      boolean bool = f3 < f6;
      if (bool)
      {
        f6 = i8 / f3;
        int i9 = (int)f6;
        bool = i8;
        i8 = i9;
      }
      else
      {
        f6 = i8 * f3;
        j = (int)f6;
      }
    }
    Object localObject4 = Bitmap.Config.ARGB_8888;
    localObject4 = Bitmap.createBitmap(j, i8, (Bitmap.Config)localObject4);
    localObject2 = new com/truecaller/util/cl;
    ((cl)localObject2).<init>(j, i8);
    n = paramInt3 / 2 * 2 + i1;
    i8 = Math.max(3, n);
    float f6 = j;
    f1 = i3;
    f6 /= f1;
    k.b(localcl, "src");
    localObject3 = new com/truecaller/util/cl$c;
    ((cl.c)localObject3).<init>(localcl, f6);
    localObject3 = (bq)localObject3;
    Object localObject5 = localObject2;
    localObject5 = (bp)localObject2;
    ((cl)localObject2).a(i8, (bq)localObject3, (bp)localObject5);
    localObject3 = localObject2;
    localObject3 = (bq)localObject2;
    ((cl)localObject2).b(i8, (bq)localObject3, (bp)localObject5);
    k.a(localObject4, "img");
    k.b(localObject4, "out");
    ((cl)localObject2).a(i8, (bq)localObject3, (bp)localObject5);
    localObject5 = new com/truecaller/util/cl$b;
    ((cl.b)localObject5).<init>((Bitmap)localObject4);
    localObject5 = (bp)localObject5;
    ((cl)localObject2).b(i8, (bq)localObject3, (bp)localObject5);
    return (Bitmap)localObject4;
  }
  
  public final ImageEntity a(Uri paramUri)
  {
    k.b(paramUri, "uri");
    return a(paramUri, 1280);
  }
  
  public final ImageEntity a(Uri paramUri, int paramInt)
  {
    k.b(paramUri, "uri");
    try
    {
      BitmapFactory.Options localOptions = g(paramUri);
      Object localObject1 = b;
      paramUri = r.a(paramUri, (Context)localObject1, null);
      if (paramUri == null) {
        return null;
      }
      localObject1 = paramUri.getPath();
      Object localObject2 = "file.path";
      k.a(localObject1, (String)localObject2);
      boolean bool1 = a(localOptions);
      if (!bool1)
      {
        paramInt = a((String)localObject1);
        if (paramInt != 0)
        {
          localObject1 = BitmapFactory.decodeFile((String)localObject1);
          if (localObject1 != null)
          {
            Bitmap localBitmap = com.truecaller.utils.extensions.b.a((Bitmap)localObject1, paramInt);
            if (localBitmap != null)
            {
              localObject1 = b(localOptions);
              paramInt = a(localBitmap, paramUri, (Bitmap.CompressFormat)localObject1);
              if (paramInt != 0) {
                break label122;
              }
              return null;
            }
          }
          return null;
        }
        label122:
        localObject1 = outMimeType;
        localObject2 = Uri.fromFile(paramUri);
        int i = outWidth;
        int j = outHeight;
        boolean bool2 = true;
        long l = l.a(paramUri);
        paramUri = Entity.a((String)localObject1, (Uri)localObject2, i, j, bool2, l);
        paramInt = paramUri instanceof ImageEntity;
        if (paramInt == 0) {
          paramUri = null;
        }
        return (ImageEntity)paramUri;
      }
      return a((String)localObject1, localOptions, paramInt);
    }
    catch (Exception localException) {}
    return null;
  }
  
  public final byte[] b(Uri paramUri)
  {
    k.b(paramUri, "uri");
    try
    {
      Object localObject1 = b;
      localObject1 = ((Context)localObject1).getContentResolver();
      paramUri = ((ContentResolver)localObject1).openInputStream(paramUri);
      paramUri = (Closeable)paramUri;
      localObject1 = paramUri;
      try
      {
        localObject1 = (InputStream)paramUri;
        localObject1 = BitmapFactory.decodeStream((InputStream)localObject1);
        c.f.b.a(paramUri, null);
        paramUri = new com/truecaller/util/h$a;
        paramUri.<init>(this);
        paramUri = (c.g.a.b)paramUri;
        paramUri = com.truecaller.utils.extensions.b.a((Bitmap)localObject1, paramUri);
        return (byte[])paramUri;
      }
      finally
      {
        try
        {
          throw ((Throwable)localObject2);
        }
        finally
        {
          Object localObject5 = localObject2;
          Object localObject3 = localObject4;
          c.f.b.a(paramUri, (Throwable)localObject5);
        }
      }
      return null;
    }
    catch (IOException localIOException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localIOException);
    }
  }
  
  /* Error */
  public final Uri c(Uri paramUri)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 416
    //   4: invokestatic 17	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: new 450	android/media/MediaMetadataRetriever
    //   10: astore_2
    //   11: aload_2
    //   12: invokespecial 451	android/media/MediaMetadataRetriever:<init>	()V
    //   15: aconst_null
    //   16: astore_3
    //   17: aload_0
    //   18: getfield 23	com/truecaller/util/h:b	Landroid/content/Context;
    //   21: astore 4
    //   23: aload_2
    //   24: aload 4
    //   26: aload_1
    //   27: invokevirtual 455	android/media/MediaMetadataRetriever:setDataSource	(Landroid/content/Context;Landroid/net/Uri;)V
    //   30: aload_2
    //   31: invokevirtual 459	android/media/MediaMetadataRetriever:getFrameAtTime	()Landroid/graphics/Bitmap;
    //   34: astore_1
    //   35: ldc -71
    //   37: astore 4
    //   39: aload_1
    //   40: aload 4
    //   42: invokestatic 33	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   45: aload_0
    //   46: aload_1
    //   47: invokespecial 462	com/truecaller/util/h:b	(Landroid/graphics/Bitmap;)Landroid/net/Uri;
    //   50: astore_3
    //   51: aload_2
    //   52: invokevirtual 465	android/media/MediaMetadataRetriever:release	()V
    //   55: aload_1
    //   56: invokevirtual 125	android/graphics/Bitmap:recycle	()V
    //   59: aload_3
    //   60: areturn
    //   61: astore 5
    //   63: aload_1
    //   64: astore_3
    //   65: aload 5
    //   67: astore_1
    //   68: goto +4 -> 72
    //   71: astore_1
    //   72: aload_2
    //   73: invokevirtual 465	android/media/MediaMetadataRetriever:release	()V
    //   76: aload_3
    //   77: ifnull +7 -> 84
    //   80: aload_3
    //   81: invokevirtual 125	android/graphics/Bitmap:recycle	()V
    //   84: aload_1
    //   85: athrow
    //   86: pop
    //   87: aconst_null
    //   88: astore_1
    //   89: aload_2
    //   90: invokevirtual 465	android/media/MediaMetadataRetriever:release	()V
    //   93: aload_1
    //   94: ifnull +21 -> 115
    //   97: goto +14 -> 111
    //   100: pop
    //   101: aconst_null
    //   102: astore_1
    //   103: aload_2
    //   104: invokevirtual 465	android/media/MediaMetadataRetriever:release	()V
    //   107: aload_1
    //   108: ifnull +7 -> 115
    //   111: aload_1
    //   112: invokevirtual 125	android/graphics/Bitmap:recycle	()V
    //   115: aconst_null
    //   116: areturn
    //   117: pop
    //   118: goto -29 -> 89
    //   121: pop
    //   122: goto -19 -> 103
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	125	0	this	h
    //   0	125	1	paramUri	Uri
    //   10	94	2	localMediaMetadataRetriever	android.media.MediaMetadataRetriever
    //   16	65	3	localUri	Uri
    //   21	20	4	localObject1	Object
    //   61	5	5	localObject2	Object
    //   86	1	6	localSecurityException1	SecurityException
    //   100	1	7	localIllegalArgumentException1	IllegalArgumentException
    //   117	1	8	localSecurityException2	SecurityException
    //   121	1	9	localIllegalArgumentException2	IllegalArgumentException
    // Exception table:
    //   from	to	target	type
    //   40	45	61	finally
    //   46	50	61	finally
    //   17	21	71	finally
    //   26	30	71	finally
    //   30	34	71	finally
    //   17	21	86	java/lang/SecurityException
    //   26	30	86	java/lang/SecurityException
    //   30	34	86	java/lang/SecurityException
    //   17	21	100	java/lang/IllegalArgumentException
    //   26	30	100	java/lang/IllegalArgumentException
    //   30	34	100	java/lang/IllegalArgumentException
    //   40	45	117	java/lang/SecurityException
    //   46	50	117	java/lang/SecurityException
    //   40	45	121	java/lang/IllegalArgumentException
    //   46	50	121	java/lang/IllegalArgumentException
  }
  
  public final Uri d(Uri paramUri)
  {
    k.b(paramUri, "uri");
    boolean bool = false;
    String str1 = null;
    try
    {
      BitmapFactory.Options localOptions = g(paramUri);
      Context localContext = b;
      String str2 = ".jpg";
      paramUri = r.a(paramUri, localContext, str2);
      if (paramUri == null) {
        return null;
      }
      paramUri = paramUri.getPath();
      str1 = "file.path";
      k.a(paramUri, str1);
      bool = a(localOptions);
      if (!bool) {
        return a(paramUri, localOptions);
      }
      return b(paramUri, localOptions, 640);
    }
    catch (Exception localException) {}
    return null;
  }
  
  public final boolean e(Uri paramUri)
  {
    if (paramUri != null)
    {
      Object localObject = b;
      k.b(paramUri, "receiver$0");
      k.b(localObject, "context");
      Scheme localScheme = r.a(paramUri);
      int i = 1;
      if (localScheme != null)
      {
        int[] arrayOfInt = s.d;
        int j = localScheme.ordinal();
        j = arrayOfInt[j];
        switch (j)
        {
        default: 
          break;
        case 2: 
          localObject = new java/io/File;
          paramUri = paramUri.getPath();
          ((File)localObject).<init>(paramUri);
          boolean bool = ((File)localObject).delete();
          break;
        case 1: 
          localObject = ((Context)localObject).getContentResolver();
          j = 0;
          localScheme = null;
          int k = ((ContentResolver)localObject).delete(paramUri, null, null);
          if (k == i)
          {
            m = 1;
          }
          else
          {
            m = 0;
            paramUri = null;
          }
          break;
        }
      }
      int m = 0;
      paramUri = null;
      if (m == i) {
        return i;
      }
    }
    return false;
  }
  
  /* Error */
  public final Uri f(Uri paramUri)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 416
    //   4: invokestatic 17	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: aload_1
    //   8: invokevirtual 292	android/net/Uri:getPath	()Ljava/lang/String;
    //   11: astore_2
    //   12: aload_2
    //   13: invokestatic 428	android/graphics/BitmapFactory:decodeFile	(Ljava/lang/String;)Landroid/graphics/Bitmap;
    //   16: astore_2
    //   17: aload_2
    //   18: ifnull +36 -> 54
    //   21: sipush 640
    //   24: istore_3
    //   25: aload_2
    //   26: iload_3
    //   27: iload_3
    //   28: invokestatic 486	com/truecaller/utils/extensions/b:b	(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    //   31: astore 4
    //   33: goto +26 -> 59
    //   36: astore_1
    //   37: iconst_0
    //   38: istore_3
    //   39: aconst_null
    //   40: astore 4
    //   42: goto +132 -> 174
    //   45: pop
    //   46: iconst_0
    //   47: istore_3
    //   48: aconst_null
    //   49: astore 4
    //   51: goto +151 -> 202
    //   54: iconst_0
    //   55: istore_3
    //   56: aconst_null
    //   57: astore 4
    //   59: aload 4
    //   61: ifnull +55 -> 116
    //   64: new 115	java/io/File
    //   67: astore 5
    //   69: aload_1
    //   70: invokevirtual 292	android/net/Uri:getPath	()Ljava/lang/String;
    //   73: astore 6
    //   75: aload 5
    //   77: aload 6
    //   79: invokespecial 116	java/io/File:<init>	(Ljava/lang/String;)V
    //   82: getstatic 206	android/graphics/Bitmap$CompressFormat:JPEG	Landroid/graphics/Bitmap$CompressFormat;
    //   85: astore 6
    //   87: aload 4
    //   89: aload 5
    //   91: aload 6
    //   93: invokestatic 183	com/truecaller/util/h:a	(Landroid/graphics/Bitmap;Ljava/io/File;Landroid/graphics/Bitmap$CompressFormat;)Z
    //   96: istore 7
    //   98: iload 7
    //   100: invokestatic 492	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   103: astore 5
    //   105: goto +17 -> 122
    //   108: astore_1
    //   109: goto +65 -> 174
    //   112: pop
    //   113: goto +89 -> 202
    //   116: iconst_0
    //   117: istore 7
    //   119: aconst_null
    //   120: astore 5
    //   122: getstatic 496	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   125: astore 6
    //   127: aload 5
    //   129: aload 6
    //   131: invokestatic 499	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/Object;)Z
    //   134: istore 7
    //   136: iload 7
    //   138: ifeq +6 -> 144
    //   141: goto +5 -> 146
    //   144: aconst_null
    //   145: astore_1
    //   146: aload_2
    //   147: ifnull +7 -> 154
    //   150: aload_2
    //   151: invokevirtual 125	android/graphics/Bitmap:recycle	()V
    //   154: aload 4
    //   156: ifnull +8 -> 164
    //   159: aload 4
    //   161: invokevirtual 125	android/graphics/Bitmap:recycle	()V
    //   164: aload_1
    //   165: areturn
    //   166: astore_1
    //   167: aconst_null
    //   168: astore_2
    //   169: iconst_0
    //   170: istore_3
    //   171: aconst_null
    //   172: astore 4
    //   174: aload_2
    //   175: ifnull +7 -> 182
    //   178: aload_2
    //   179: invokevirtual 125	android/graphics/Bitmap:recycle	()V
    //   182: aload 4
    //   184: ifnull +8 -> 192
    //   187: aload 4
    //   189: invokevirtual 125	android/graphics/Bitmap:recycle	()V
    //   192: aload_1
    //   193: athrow
    //   194: pop
    //   195: aconst_null
    //   196: astore_2
    //   197: iconst_0
    //   198: istore_3
    //   199: aconst_null
    //   200: astore 4
    //   202: aload_2
    //   203: ifnull +7 -> 210
    //   206: aload_2
    //   207: invokevirtual 125	android/graphics/Bitmap:recycle	()V
    //   210: aload 4
    //   212: ifnull +8 -> 220
    //   215: aload 4
    //   217: invokevirtual 125	android/graphics/Bitmap:recycle	()V
    //   220: aconst_null
    //   221: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	222	0	this	h
    //   0	222	1	paramUri	Uri
    //   11	196	2	localObject1	Object
    //   24	175	3	i	int
    //   31	185	4	localBitmap	Bitmap
    //   67	61	5	localObject2	Object
    //   73	57	6	localObject3	Object
    //   96	41	7	bool	boolean
    //   45	1	8	localIllegalArgumentException1	IllegalArgumentException
    //   112	1	9	localIllegalArgumentException2	IllegalArgumentException
    //   194	1	10	localIllegalArgumentException3	IllegalArgumentException
    // Exception table:
    //   from	to	target	type
    //   27	31	36	finally
    //   27	31	45	java/lang/IllegalArgumentException
    //   64	67	108	finally
    //   69	73	108	finally
    //   77	82	108	finally
    //   82	85	108	finally
    //   91	96	108	finally
    //   98	103	108	finally
    //   122	125	108	finally
    //   129	134	108	finally
    //   64	67	112	java/lang/IllegalArgumentException
    //   69	73	112	java/lang/IllegalArgumentException
    //   77	82	112	java/lang/IllegalArgumentException
    //   82	85	112	java/lang/IllegalArgumentException
    //   91	96	112	java/lang/IllegalArgumentException
    //   98	103	112	java/lang/IllegalArgumentException
    //   122	125	112	java/lang/IllegalArgumentException
    //   129	134	112	java/lang/IllegalArgumentException
    //   7	11	166	finally
    //   12	16	166	finally
    //   7	11	194	java/lang/IllegalArgumentException
    //   12	16	194	java/lang/IllegalArgumentException
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */