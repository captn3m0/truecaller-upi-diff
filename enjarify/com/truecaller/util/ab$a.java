package com.truecaller.util;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.HistoryEvent;

final class ab$a
  extends u
{
  private final HistoryEvent b;
  
  private ab$a(e parame, HistoryEvent paramHistoryEvent)
  {
    super(parame);
    b = paramHistoryEvent;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".addRecordToCallHistory(");
    String str = a(b, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ab.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */