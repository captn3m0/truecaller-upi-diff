package com.truecaller.util;

import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;
import c.g.b.k;
import c.u;

public final class dg
{
  public static final void a(ImageView paramImageView, int paramInt)
  {
    k.b(paramImageView, "receiver$0");
    Object localObject1 = paramImageView.getParent();
    boolean bool = localObject1 instanceof View;
    if (bool)
    {
      localObject1 = paramImageView.getParent();
      if (localObject1 != null)
      {
        localObject1 = (View)localObject1;
        ViewTreeObserver localViewTreeObserver = ((View)localObject1).getViewTreeObserver();
        Object localObject2 = new com/truecaller/util/dg$a;
        ((dg.a)localObject2).<init>(paramImageView, paramInt, (View)localObject1);
        localObject2 = (ViewTreeObserver.OnGlobalLayoutListener)localObject2;
        localViewTreeObserver.addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)localObject2);
        return;
      }
      paramImageView = new c/u;
      paramImageView.<init>("null cannot be cast to non-null type android.view.View");
      throw paramImageView;
    }
    paramImageView.setImageResource(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.dg
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */