package com.truecaller.util;

import android.net.Uri;
import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.types.Entity;
import java.util.Collection;
import java.util.List;

public final class bb
  implements ba
{
  private final v a;
  
  public bb(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return ba.class.equals(paramClass);
  }
  
  public final w a(Uri paramUri)
  {
    v localv = a;
    bb.e locale = new com/truecaller/util/bb$e;
    e locale1 = new com/truecaller/androidactors/e;
    locale1.<init>();
    locale.<init>(locale1, paramUri, (byte)0);
    return w.a(localv, locale);
  }
  
  public final w a(Uri paramUri, boolean paramBoolean)
  {
    v localv = a;
    bb.d locald = new com/truecaller/util/bb$d;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locald.<init>(locale, paramUri, paramBoolean, (byte)0);
    return w.a(localv, locald);
  }
  
  public final w a(Uri paramUri, boolean paramBoolean, long paramLong)
  {
    v localv = a;
    bb.g localg = new com/truecaller/util/bb$g;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localg.<init>(locale, paramUri, paramBoolean, paramLong, (byte)0);
    return w.a(localv, localg);
  }
  
  public final w a(Collection paramCollection, long paramLong)
  {
    v localv = a;
    bb.c localc = new com/truecaller/util/bb$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, paramCollection, paramLong, (byte)0);
    return w.a(localv, localc);
  }
  
  public final w a(List paramList)
  {
    v localv = a;
    bb.f localf = new com/truecaller/util/bb$f;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localf.<init>(locale, paramList, (byte)0);
    return w.a(localv, localf);
  }
  
  public final w a(Entity[] paramArrayOfEntity)
  {
    v localv = a;
    bb.a locala = new com/truecaller/util/bb$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramArrayOfEntity, (byte)0);
    return w.a(localv, locala);
  }
  
  public final w b(Uri paramUri, boolean paramBoolean)
  {
    v localv = a;
    bb.b localb = new com/truecaller/util/bb$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramUri, paramBoolean, (byte)0);
    return w.a(localv, localb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bb
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */