package com.truecaller.util.f;

import android.media.MediaFormat;
import c.g.b.k;

public final class a
  implements net.ypresto.androidtranscoder.b.a
{
  public final MediaFormat a(MediaFormat paramMediaFormat)
  {
    k.b(paramMediaFormat, "inputFormat");
    Object localObject1 = "width";
    int i = paramMediaFormat.getInteger((String)localObject1);
    Object localObject2 = "height";
    int j = paramMediaFormat.getInteger((String)localObject2);
    int k = 640;
    int m = 360;
    int n;
    int i1;
    int i2;
    int i3;
    if (i >= j)
    {
      n = j;
      i1 = i;
      i2 = 640;
      i3 = 360;
    }
    else
    {
      i1 = j;
      n = i;
      i2 = 360;
      i3 = 640;
    }
    int i4 = 1;
    Object localObject3;
    if (n <= m)
    {
      localObject2 = new String[i4];
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("This video is less or equal to 360p, pass-through. (");
      ((StringBuilder)localObject3).append(i);
      ((StringBuilder)localObject3).append(" x ");
      ((StringBuilder)localObject3).append(j);
      ((StringBuilder)localObject3).append(" )");
      paramMediaFormat = ((StringBuilder)localObject3).toString();
      localObject2[0] = paramMediaFormat;
      return null;
    }
    m = i1 * 9;
    int i5 = n * 16;
    if (m != i5)
    {
      localObject3 = new String[i4];
      Object localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>("This video is not 16:9. (");
      ((StringBuilder)localObject4).append(i);
      ((StringBuilder)localObject4).append(" x ");
      ((StringBuilder)localObject4).append(j);
      String str = " )";
      ((StringBuilder)localObject4).append(str);
      localObject4 = ((StringBuilder)localObject4).toString();
      localObject3[0] = localObject4;
      n *= 640;
      k = n / i1;
      if (i >= j) {
        i3 = k;
      } else {
        i2 = k;
      }
      paramMediaFormat = new String[i4];
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("Calculated new resolution.  (");
      ((StringBuilder)localObject1).append(i2);
      ((StringBuilder)localObject1).append(" x ");
      ((StringBuilder)localObject1).append(i3);
      localObject2 = " )";
      ((StringBuilder)localObject1).append((String)localObject2);
      localObject1 = ((StringBuilder)localObject1).toString();
      paramMediaFormat[0] = localObject1;
    }
    paramMediaFormat = MediaFormat.createVideoFormat("video/avc", i2, i3);
    paramMediaFormat.setInteger("bitrate", 2000000);
    paramMediaFormat.setInteger("frame-rate", 30);
    paramMediaFormat.setInteger("i-frame-interval", 3);
    paramMediaFormat.setInteger("color-format", 2130708361);
    return paramMediaFormat;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.f.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */