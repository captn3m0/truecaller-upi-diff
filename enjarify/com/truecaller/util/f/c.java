package com.truecaller.util.f;

import android.content.Context;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.VideoEntity;
import com.truecaller.util.bd;
import com.truecaller.util.de;
import com.truecaller.utils.extensions.r;
import java.io.File;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Future;

public final class c
  implements b
{
  private final ConcurrentMap a;
  private final Context b;
  private final bd c;
  
  public c(Context paramContext, bd parambd)
  {
    b = paramContext;
    c = parambd;
    paramContext = new java/util/concurrent/ConcurrentHashMap;
    paramContext.<init>();
    paramContext = (ConcurrentMap)paramContext;
    a = paramContext;
  }
  
  private final VideoEntity a(Uri paramUri, long paramLong)
  {
    Object localObject1 = c.a(paramUri);
    Object localObject2 = b;
    localObject2 = r.a(paramUri, (Context)localObject2);
    long l;
    if (localObject2 != null) {
      l = ((Long)localObject2).longValue();
    } else {
      l = -1;
    }
    boolean bool = true;
    localObject2 = new String[bool];
    String str1 = null;
    String str2 = String.valueOf(l);
    String str3 = "Compressed video Size: ".concat(str2);
    localObject2[0] = str3;
    if (localObject1 != null)
    {
      localObject2 = d;
      if (localObject2 != null)
      {
        str1 = d;
        int i = a;
        int j = b;
        int k = c;
        Uri localUri = Uri.EMPTY;
        localObject1 = Entity.a(paramLong, str1, paramUri, i, j, k, l, localUri);
        bool = localObject1 instanceof VideoEntity;
        if (!bool) {
          localObject1 = null;
        }
        return (VideoEntity)localObject1;
      }
    }
    return null;
  }
  
  private final VideoEntity a(String paramString, long paramLong)
  {
    File localFile = new java/io/File;
    localFile.<init>(paramString);
    paramString = Uri.fromFile(localFile);
    k.a(paramString, "uri");
    return a(paramString, paramLong);
  }
  
  /* Error */
  public final VideoEntity a(BinaryEntity paramBinaryEntity)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 109
    //   3: invokestatic 19	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_1
    //   7: getfield 113	com/truecaller/messaging/data/types/BinaryEntity:b	Landroid/net/Uri;
    //   10: astore_2
    //   11: aload_2
    //   12: ldc 115
    //   14: invokestatic 104	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   17: aload_0
    //   18: getfield 27	com/truecaller/util/f/c:b	Landroid/content/Context;
    //   21: invokevirtual 125	android/content/Context:getCacheDir	()Ljava/io/File;
    //   24: astore_3
    //   25: ldc 117
    //   27: ldc 119
    //   29: aload_3
    //   30: invokestatic 130	c/f/e:a	(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    //   33: astore 4
    //   35: aload_0
    //   36: getfield 27	com/truecaller/util/f/c:b	Landroid/content/Context;
    //   39: astore_3
    //   40: aload_3
    //   41: invokevirtual 134	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   44: astore_3
    //   45: ldc -120
    //   47: astore 5
    //   49: aload_3
    //   50: aload_2
    //   51: aload 5
    //   53: invokevirtual 142	android/content/ContentResolver:openFileDescriptor	(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    //   56: astore_3
    //   57: aload_3
    //   58: ifnull +11 -> 69
    //   61: aload_3
    //   62: invokevirtual 148	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
    //   65: astore_3
    //   66: goto +5 -> 71
    //   69: aconst_null
    //   70: astore_3
    //   71: new 150	com/truecaller/util/f/c$a
    //   74: astore 5
    //   76: aload 5
    //   78: invokespecial 151	com/truecaller/util/f/c$a:<init>	()V
    //   81: invokestatic 156	net/ypresto/androidtranscoder/a:a	()Lnet/ypresto/androidtranscoder/a;
    //   84: astore 6
    //   86: aload 4
    //   88: invokevirtual 160	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   91: astore 7
    //   93: new 162	com/truecaller/util/f/a
    //   96: astore 8
    //   98: aload 8
    //   100: invokespecial 163	com/truecaller/util/f/a:<init>	()V
    //   103: aload 8
    //   105: checkcast 165	net/ypresto/androidtranscoder/b/a
    //   108: astore 8
    //   110: aload 5
    //   112: checkcast 167	net/ypresto/androidtranscoder/a$a
    //   115: astore 5
    //   117: aload 6
    //   119: aload_3
    //   120: aload 7
    //   122: aload 8
    //   124: aload 5
    //   126: invokevirtual 170	net/ypresto/androidtranscoder/a:a	(Ljava/io/FileDescriptor;Ljava/lang/String;Lnet/ypresto/androidtranscoder/b/a;Lnet/ypresto/androidtranscoder/a$a;)Ljava/util/concurrent/Future;
    //   129: astore_3
    //   130: aload_0
    //   131: getfield 36	com/truecaller/util/f/c:a	Ljava/util/concurrent/ConcurrentMap;
    //   134: astore 5
    //   136: aload 5
    //   138: checkcast 172	java/util/Map
    //   141: astore 5
    //   143: aload_1
    //   144: getfield 176	com/truecaller/messaging/data/types/BinaryEntity:h	J
    //   147: lstore 9
    //   149: lload 9
    //   151: invokestatic 179	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   154: astore 6
    //   156: aload 5
    //   158: aload 6
    //   160: aload_3
    //   161: invokeinterface 183 3 0
    //   166: pop
    //   167: aload_3
    //   168: invokeinterface 189 1 0
    //   173: pop
    //   174: aload 4
    //   176: invokevirtual 192	java/io/File:getPath	()Ljava/lang/String;
    //   179: astore_3
    //   180: ldc -62
    //   182: astore 5
    //   184: aload_3
    //   185: aload 5
    //   187: invokestatic 104	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   190: aload_1
    //   191: getfield 176	com/truecaller/messaging/data/types/BinaryEntity:h	J
    //   194: lstore 11
    //   196: aload_0
    //   197: aload_3
    //   198: lload 11
    //   200: invokespecial 197	com/truecaller/util/f/c:a	(Ljava/lang/String;J)Lcom/truecaller/messaging/data/types/VideoEntity;
    //   203: astore_2
    //   204: goto +121 -> 325
    //   207: astore_2
    //   208: goto +142 -> 350
    //   211: pop
    //   212: ldc -57
    //   214: astore_2
    //   215: iconst_1
    //   216: anewarray 55	java/lang/String
    //   219: iconst_0
    //   220: aload_2
    //   221: aastore
    //   222: aload 4
    //   224: invokevirtual 203	java/io/File:delete	()Z
    //   227: pop
    //   228: new 205	java/util/concurrent/CancellationException
    //   231: astore_2
    //   232: aload_2
    //   233: invokespecial 206	java/util/concurrent/CancellationException:<init>	()V
    //   236: aload_2
    //   237: checkcast 208	java/lang/Throwable
    //   240: astore_2
    //   241: aload_2
    //   242: athrow
    //   243: ldc -46
    //   245: astore 4
    //   247: iconst_1
    //   248: anewarray 55	java/lang/String
    //   251: iconst_0
    //   252: aload 4
    //   254: aastore
    //   255: aload_0
    //   256: getfield 27	com/truecaller/util/f/c:b	Landroid/content/Context;
    //   259: astore 4
    //   261: aload_2
    //   262: aload 4
    //   264: aconst_null
    //   265: invokestatic 213	com/truecaller/utils/extensions/r:a	(Landroid/net/Uri;Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    //   268: astore_2
    //   269: aload_2
    //   270: ifnonnull +26 -> 296
    //   273: aload_0
    //   274: getfield 36	com/truecaller/util/f/c:a	Ljava/util/concurrent/ConcurrentMap;
    //   277: astore_2
    //   278: aload_1
    //   279: getfield 176	com/truecaller/messaging/data/types/BinaryEntity:h	J
    //   282: invokestatic 179	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   285: astore_1
    //   286: aload_2
    //   287: aload_1
    //   288: invokeinterface 217 2 0
    //   293: pop
    //   294: aconst_null
    //   295: areturn
    //   296: aload_2
    //   297: invokevirtual 192	java/io/File:getPath	()Ljava/lang/String;
    //   300: astore_2
    //   301: ldc -37
    //   303: astore 4
    //   305: aload_2
    //   306: aload 4
    //   308: invokestatic 104	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   311: aload_1
    //   312: getfield 176	com/truecaller/messaging/data/types/BinaryEntity:h	J
    //   315: lstore 13
    //   317: aload_0
    //   318: aload_2
    //   319: lload 13
    //   321: invokespecial 197	com/truecaller/util/f/c:a	(Ljava/lang/String;J)Lcom/truecaller/messaging/data/types/VideoEntity;
    //   324: astore_2
    //   325: aload_0
    //   326: getfield 36	com/truecaller/util/f/c:a	Ljava/util/concurrent/ConcurrentMap;
    //   329: astore 4
    //   331: aload_1
    //   332: getfield 176	com/truecaller/messaging/data/types/BinaryEntity:h	J
    //   335: invokestatic 179	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   338: astore_1
    //   339: aload 4
    //   341: aload_1
    //   342: invokeinterface 217 2 0
    //   347: pop
    //   348: aload_2
    //   349: areturn
    //   350: aload_0
    //   351: getfield 36	com/truecaller/util/f/c:a	Ljava/util/concurrent/ConcurrentMap;
    //   354: astore 4
    //   356: aload_1
    //   357: getfield 176	com/truecaller/messaging/data/types/BinaryEntity:h	J
    //   360: invokestatic 179	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   363: astore_1
    //   364: aload 4
    //   366: aload_1
    //   367: invokeinterface 217 2 0
    //   372: pop
    //   373: aload_2
    //   374: athrow
    //   375: checkcast 208	java/lang/Throwable
    //   378: invokestatic 225	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   381: aconst_null
    //   382: areturn
    //   383: pop
    //   384: goto -141 -> 243
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	387	0	this	c
    //   0	387	1	paramBinaryEntity	BinaryEntity
    //   10	194	2	localObject1	Object
    //   207	1	2	localObject2	Object
    //   214	160	2	localObject3	Object
    //   24	174	3	localObject4	Object
    //   33	332	4	localObject5	Object
    //   47	139	5	localObject6	Object
    //   84	75	6	localObject7	Object
    //   91	30	7	str	String
    //   96	27	8	localObject8	Object
    //   147	3	9	l1	long
    //   194	5	11	l2	long
    //   315	5	13	l3	long
    //   211	1	14	localCancellationException	java.util.concurrent.CancellationException
    //   375	1	15	localFileNotFoundException	java.io.FileNotFoundException
    //   383	1	16	localExecutionException	java.util.concurrent.ExecutionException
    // Exception table:
    //   from	to	target	type
    //   81	84	207	finally
    //   86	91	207	finally
    //   93	96	207	finally
    //   98	103	207	finally
    //   103	108	207	finally
    //   110	115	207	finally
    //   124	129	207	finally
    //   130	134	207	finally
    //   136	141	207	finally
    //   143	147	207	finally
    //   149	154	207	finally
    //   160	167	207	finally
    //   167	174	207	finally
    //   174	179	207	finally
    //   185	190	207	finally
    //   190	194	207	finally
    //   198	203	207	finally
    //   215	222	207	finally
    //   222	228	207	finally
    //   228	231	207	finally
    //   232	236	207	finally
    //   236	240	207	finally
    //   241	243	207	finally
    //   247	255	207	finally
    //   255	259	207	finally
    //   264	268	207	finally
    //   296	300	207	finally
    //   306	311	207	finally
    //   311	315	207	finally
    //   319	324	207	finally
    //   81	84	211	java/util/concurrent/CancellationException
    //   86	91	211	java/util/concurrent/CancellationException
    //   93	96	211	java/util/concurrent/CancellationException
    //   98	103	211	java/util/concurrent/CancellationException
    //   103	108	211	java/util/concurrent/CancellationException
    //   110	115	211	java/util/concurrent/CancellationException
    //   124	129	211	java/util/concurrent/CancellationException
    //   130	134	211	java/util/concurrent/CancellationException
    //   136	141	211	java/util/concurrent/CancellationException
    //   143	147	211	java/util/concurrent/CancellationException
    //   149	154	211	java/util/concurrent/CancellationException
    //   160	167	211	java/util/concurrent/CancellationException
    //   167	174	211	java/util/concurrent/CancellationException
    //   174	179	211	java/util/concurrent/CancellationException
    //   185	190	211	java/util/concurrent/CancellationException
    //   190	194	211	java/util/concurrent/CancellationException
    //   198	203	211	java/util/concurrent/CancellationException
    //   35	39	375	java/io/FileNotFoundException
    //   40	44	375	java/io/FileNotFoundException
    //   51	56	375	java/io/FileNotFoundException
    //   81	84	383	java/util/concurrent/ExecutionException
    //   86	91	383	java/util/concurrent/ExecutionException
    //   93	96	383	java/util/concurrent/ExecutionException
    //   98	103	383	java/util/concurrent/ExecutionException
    //   103	108	383	java/util/concurrent/ExecutionException
    //   110	115	383	java/util/concurrent/ExecutionException
    //   124	129	383	java/util/concurrent/ExecutionException
    //   130	134	383	java/util/concurrent/ExecutionException
    //   136	141	383	java/util/concurrent/ExecutionException
    //   143	147	383	java/util/concurrent/ExecutionException
    //   149	154	383	java/util/concurrent/ExecutionException
    //   160	167	383	java/util/concurrent/ExecutionException
    //   167	174	383	java/util/concurrent/ExecutionException
    //   174	179	383	java/util/concurrent/ExecutionException
    //   185	190	383	java/util/concurrent/ExecutionException
    //   190	194	383	java/util/concurrent/ExecutionException
    //   198	203	383	java/util/concurrent/ExecutionException
  }
  
  public final void b(BinaryEntity paramBinaryEntity)
  {
    k.b(paramBinaryEntity, "binaryEntity");
    ConcurrentMap localConcurrentMap = a;
    long l = h;
    paramBinaryEntity = Long.valueOf(l);
    paramBinaryEntity = (Future)localConcurrentMap.get(paramBinaryEntity);
    if (paramBinaryEntity != null)
    {
      paramBinaryEntity.cancel(true);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.f.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */