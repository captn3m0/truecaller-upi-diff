package com.truecaller.util;

import c.a.m;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public final class cl
  implements bp, bq
{
  final int a;
  final int b;
  final int[] c;
  
  private cl(int paramInt1, int paramInt2, int[] paramArrayOfInt)
  {
    a = paramInt1;
    b = paramInt2;
    c = paramArrayOfInt;
  }
  
  public final int a(int paramInt1, int paramInt2)
  {
    int[] arrayOfInt = c;
    int i = b + -1;
    paramInt2 = Math.max(paramInt2, 0);
    paramInt2 = Math.min(i, paramInt2);
    i = a;
    paramInt2 *= i;
    i += -1;
    paramInt1 = Math.max(paramInt1, 0);
    paramInt1 = Math.min(i, paramInt1);
    paramInt2 += paramInt1;
    return arrayOfInt[paramInt2];
  }
  
  public final void a(int paramInt1, int paramInt2, int paramInt3)
  {
    int[] arrayOfInt = c;
    int i = a;
    paramInt2 = paramInt2 * i + paramInt1;
    arrayOfInt[paramInt2] = paramInt3;
  }
  
  final void a(int paramInt, bq parambq, bp parambp)
  {
    int i = b;
    cl.a locala = new com/truecaller/util/cl$a;
    locala.<init>(paramInt);
    int j = 0;
    while (j < i)
    {
      int k = paramInt / 2;
      int m = -k;
      locala.b();
      while (m <= k)
      {
        n = parambq.a(m, j);
        locala.a(n);
        m += 1;
      }
      m = a;
      int n = 0;
      while (n < m)
      {
        int i1 = locala.a();
        parambp.a(n, j, i1);
        n += 1;
        i1 = n + k;
        i1 = parambq.a(i1, j);
        locala.a(i1);
      }
      j += 1;
    }
  }
  
  public final byte[] a()
  {
    Object localObject1 = c;
    Object localObject2 = new java/util/ArrayList;
    int i = localObject1.length;
    ((ArrayList)localObject2).<init>(i);
    localObject2 = (Collection)localObject2;
    i = localObject1.length;
    int j = 0;
    Object localObject3 = null;
    int m;
    int n;
    while (j < i)
    {
      m = localObject1[j];
      n = j.a(m) * 249 + 1014 >> 11 & 0x1F;
      int i1 = j.b(m) * 253 + 505 >> 10 & 0x3F;
      m = (m & 0xFF) * 249 + 1014 >> 11 & 0x1F;
      n <<= 3;
      int i2 = i1 >> 3;
      n = (n | i2) << 8;
      i1 = (i1 & 0x7) << 5;
      m = m | i1 | n;
      Integer localInteger = Integer.valueOf(m);
      ((Collection)localObject2).add(localInteger);
      j += 1;
    }
    localObject2 = (Iterable)localObject2;
    int i3 = a;
    i = b;
    i3 = i3 * i * 2 + 1;
    localObject1 = new byte[i3];
    localObject2 = ((Iterable)localObject2).iterator();
    for (i = 0;; i = m)
    {
      boolean bool = ((Iterator)localObject2).hasNext();
      if (!bool) {
        break;
      }
      localObject3 = ((Iterator)localObject2).next();
      m = i + 1;
      if (i < 0) {
        m.a();
      }
      localObject3 = (Number)localObject3;
      int k = ((Number)localObject3).intValue();
      i = i * 2 + 1;
      n = (byte)(k >> 8 & 0xFF);
      localObject1[i] = n;
      i += 1;
      k = (byte)(k & 0xFF);
      localObject1[i] = k;
    }
    localObject1[0] = 24;
    return (byte[])localObject1;
  }
  
  final void b(int paramInt, bq parambq, bp parambp)
  {
    int i = a;
    cl.a locala = new com/truecaller/util/cl$a;
    locala.<init>(paramInt);
    int j = 0;
    while (j < i)
    {
      int k = paramInt / 2;
      int m = -k;
      locala.b();
      while (m <= k)
      {
        n = parambq.a(j, m);
        locala.a(n);
        m += 1;
      }
      m = b;
      int n = 0;
      while (n < m)
      {
        int i1 = locala.a();
        parambp.a(j, n, i1);
        n += 1;
        i1 = n + k;
        i1 = parambq.a(j, i1);
        locala.a(i1);
      }
      j += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */