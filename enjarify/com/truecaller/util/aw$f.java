package com.truecaller.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory.Options;

public abstract class aw$f
{
  public final Bitmap a()
  {
    BitmapFactory.Options localOptions = new android/graphics/BitmapFactory$Options;
    localOptions.<init>();
    int i = 1;
    inJustDecodeBounds = i;
    a(localOptions);
    int j = 0;
    inJustDecodeBounds = false;
    int k = outWidth;
    int m = outHeight;
    k = Math.max(k, m);
    m = k / 160;
    k %= 160;
    int n = 80;
    if (k > n) {
      j = 1;
    }
    m += j;
    i = Math.max(i, m);
    inSampleSize = i;
    return a(localOptions);
  }
  
  protected abstract Bitmap a(BitmapFactory.Options paramOptions);
}

/* Location:
 * Qualified Name:     com.truecaller.util.aw.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */