package com.truecaller.util;

import android.os.Looper;
import com.truecaller.TrueApp;
import com.truecaller.log.d;

public final class ck
{
  public static void a(Looper paramLooper, String paramString)
  {
    boolean bool = a(paramLooper);
    if (!bool) {
      a(paramString);
    }
  }
  
  public static void a(String paramString)
  {
    ck.a locala = new com/truecaller/util/ck$a;
    locala.<init>(paramString);
    TrueApp.y();
    boolean bool = TrueApp.G();
    if (!bool)
    {
      d.a(locala);
      return;
    }
    throw locala;
  }
  
  public static boolean a(Looper paramLooper)
  {
    Looper localLooper = Looper.myLooper();
    return paramLooper == localLooper;
  }
  
  public static void b(String paramString)
  {
    Looper localLooper = Looper.getMainLooper();
    boolean bool = a(localLooper);
    if (!bool) {
      a(paramString);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ck
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */