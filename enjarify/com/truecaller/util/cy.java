package com.truecaller.util;

import dagger.a.d;
import javax.inject.Provider;

public final class cy
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private cy(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
  }
  
  public static cy a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    cy localcy = new com/truecaller/util/cy;
    localcy.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localcy;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cy
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */