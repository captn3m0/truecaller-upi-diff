package com.truecaller.util;

import android.graphics.drawable.LayerDrawable;
import android.widget.ImageView;
import com.truecaller.ui.components.n;

final class aw$j
{
  final n a;
  final ImageView b;
  final LayerDrawable c;
  final int d;
  final aw.e e;
  
  public aw$j(n paramn, ImageView paramImageView)
  {
    a = paramn;
    b = paramImageView;
    c = null;
    d = -1;
    e = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.aw.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */