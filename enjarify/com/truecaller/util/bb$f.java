package com.truecaller.util;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.List;

final class bb$f
  extends u
{
  private final List b;
  
  private bb$f(e parame, List paramList)
  {
    super(parame);
    b = paramList;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".verifyFilesExist(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bb.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */