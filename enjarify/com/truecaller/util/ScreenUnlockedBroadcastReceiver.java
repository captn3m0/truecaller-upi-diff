package com.truecaller.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.presence.c;

public class ScreenUnlockedBroadcastReceiver
  extends BroadcastReceiver
{
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent != null)
    {
      String str = "android.intent.action.USER_PRESENT";
      paramIntent = paramIntent.getAction();
      boolean bool = str.equals(paramIntent);
      if (bool)
      {
        paramIntent = "Screen unlocked, triggering a last seen update";
        new String[1][0] = paramIntent;
        paramContext = (c)((bk)paramContext.getApplicationContext()).a().ae().a();
        paramContext.b();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ScreenUnlockedBroadcastReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */