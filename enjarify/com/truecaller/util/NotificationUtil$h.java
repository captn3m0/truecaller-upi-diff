package com.truecaller.util;

import android.content.Context;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.update.c;
import com.truecaller.update.d;

public final class NotificationUtil$h
  implements NotificationUtil.b
{
  private final Context a;
  private final String b;
  
  public NotificationUtil$h(Context paramContext, Notification paramNotification)
  {
    a = paramContext;
    paramContext = paramNotification.a("u");
    b = paramContext;
  }
  
  public final void a()
  {
    d locald = ((bk)a.getApplicationContext()).a().cz().a();
    Context localContext = a;
    String str = b;
    locald.a(localContext, str, "notificationsList");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.NotificationUtil.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */