package com.truecaller.util;

import android.net.Uri;
import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class ab$j
  extends u
{
  private final Uri b;
  
  private ab$j(e parame, Uri paramUri)
  {
    super(parame);
    b = paramUri;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".syncContactByUri(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ab.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */