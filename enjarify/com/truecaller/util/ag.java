package com.truecaller.util;

import android.content.Context;
import c.g.b.k;
import c.l;
import com.truecaller.common.h.j;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import org.a.a.a.g;
import org.a.a.c;
import org.a.a.i;
import org.a.a.p;
import org.a.a.x;
import org.a.a.z;

public final class ag
  implements af
{
  private final Context a;
  
  public ag(Context paramContext)
  {
    a = paramContext;
  }
  
  public final String a()
  {
    String str = j.c();
    k.a(str, "DateTimeUtils.getCurrentTimeZoneString()");
    return str;
  }
  
  public final String a(int paramInt)
  {
    String str = j.a(a, paramInt);
    k.a(str, "DateTimeUtils.getShortFo…atDuration(context, secs)");
    return str;
  }
  
  public final String a(long paramLong, DatePattern paramDatePattern)
  {
    k.b(paramDatePattern, "datePattern");
    int[] arrayOfInt = ah.a;
    int i = paramDatePattern.ordinal();
    i = arrayOfInt[i];
    switch (i)
    {
    default: 
      localObject = new c/l;
      ((l)localObject).<init>();
      throw ((Throwable)localObject);
    case 2: 
      paramDatePattern = org.a.a.d.a.a("EEEE, dd MMM YYYY");
      break;
    case 1: 
      paramDatePattern = org.a.a.d.a.a("EEEE, dd MMM");
    }
    Object localObject = paramDatePattern.a(paramLong);
    k.a(localObject, "when (datePattern) {\n   …       }.print(timestamp)");
    return (String)localObject;
  }
  
  public final boolean a(long paramLong)
  {
    p localp = p.a();
    Object localObject = new org/a/a/p;
    ((p)localObject).<init>(paramLong);
    localObject = (z)localObject;
    int i = localp.a((z)localObject);
    return i == 0;
  }
  
  public final boolean a(long paramLong1, long paramLong2)
  {
    p localp = new org/a/a/p;
    localp.<init>(paramLong1);
    Object localObject = new org/a/a/p;
    ((p)localObject).<init>(paramLong2);
    localObject = (z)localObject;
    int i = localp.a((z)localObject);
    return i == 0;
  }
  
  public final boolean a(org.a.a.b paramb1, org.a.a.b paramb2)
  {
    k.b(paramb1, "date");
    k.b(paramb2, "compareDate");
    paramb2 = (x)paramb2;
    return paramb1.c(paramb2);
  }
  
  public final String b(int paramInt)
  {
    int i = paramInt / 60;
    paramInt %= 60;
    int j = 2;
    Object[] arrayOfObject = new Object[j];
    Integer localInteger = Integer.valueOf(i);
    arrayOfObject[0] = localInteger;
    Object localObject = Integer.valueOf(paramInt);
    arrayOfObject[1] = localObject;
    localObject = Arrays.copyOf(arrayOfObject, j);
    localObject = String.format("%02d:%02d", (Object[])localObject);
    k.a(localObject, "java.lang.String.format(format, *args)");
    return (String)localObject;
  }
  
  public final org.a.a.b b()
  {
    org.a.a.b localb = org.a.a.b.ay_();
    k.a(localb, "DateTime.now()");
    return localb;
  }
  
  public final boolean b(long paramLong)
  {
    Object localObject1 = p.a();
    Object localObject2 = b.s();
    long l1 = a;
    int i = 1;
    long l2 = ((i)localObject2).b(l1, i);
    Object localObject3 = b.u();
    l2 = ((c)localObject3).d(l2);
    long l3 = a;
    boolean bool = l2 < l3;
    if (bool)
    {
      localObject3 = new org/a/a/p;
      localObject1 = b;
      ((p)localObject3).<init>(l2, (org.a.a.a)localObject1);
      localObject1 = localObject3;
    }
    localObject2 = new org/a/a/p;
    ((p)localObject2).<init>(paramLong);
    localObject2 = (z)localObject2;
    int j = ((p)localObject1).a((z)localObject2);
    if (j == 0) {
      return i;
    }
    return false;
  }
  
  public final boolean b(org.a.a.b paramb1, org.a.a.b paramb2)
  {
    k.b(paramb1, "date");
    k.b(paramb2, "compareDate");
    paramb2 = (x)paramb2;
    return paramb1.b(paramb2);
  }
  
  public final long c()
  {
    TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
    long l = ba;
    return localTimeUnit.toSeconds(l);
  }
  
  public final boolean c(long paramLong)
  {
    p localp1 = p.a();
    k.a(localp1, "LocalDate.now()");
    int i = localp1.c();
    p localp2 = new org/a/a/p;
    localp2.<init>(paramLong);
    int j = localp2.c();
    return i == j;
  }
  
  public final int d(long paramLong)
  {
    org.a.a.b localb = new org/a/a/b;
    localb.<init>(paramLong);
    return localb.g();
  }
  
  public final int e(long paramLong)
  {
    org.a.a.b localb = new org/a/a/b;
    localb.<init>(paramLong);
    return localb.h();
  }
  
  public final int f(long paramLong)
  {
    org.a.a.b localb = new org/a/a/b;
    localb.<init>(paramLong);
    return localb.i();
  }
  
  public final CharSequence g(long paramLong)
  {
    CharSequence localCharSequence = j.a(a, paramLong, false);
    k.a(localCharSequence, "DateTimeUtils.getRelativ…ate(context, date, false)");
    return localCharSequence;
  }
  
  public final String h(long paramLong)
  {
    String str = j.f(a, paramLong);
    k.a(str, "DateTimeUtils.getFormattedTime(context, millis)");
    return str;
  }
  
  public final String i(long paramLong)
  {
    String str = j.c(a, paramLong);
    k.a(str, "DateTimeUtils.getFormatt…Duration(context, millis)");
    return str;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */