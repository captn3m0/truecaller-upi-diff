package com.truecaller.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v4.f.g;
import com.truecaller.ui.components.n;

final class aw$k
  implements Runnable
{
  final aw.j a;
  private final Runnable c;
  private final Runnable d;
  
  aw$k(aw paramaw, aw.j paramj)
  {
    paramaw = new com/truecaller/util/aw$k$1;
    paramaw.<init>(this);
    c = paramaw;
    paramaw = new com/truecaller/util/aw$k$2;
    paramaw.<init>(this);
    d = paramaw;
    a = paramj;
  }
  
  public final void run()
  {
    Object localObject1 = a.a;
    try
    {
      Object localObject2 = b;
      localObject3 = a;
      boolean bool1 = ((aw)localObject2).a((aw.j)localObject3);
      if (bool1) {
        return;
      }
      localObject2 = aw.b();
      localObject3 = c;
      ((Handler)localObject2).post((Runnable)localObject3);
      localObject2 = b;
      localObject2 = aw.a((aw)localObject2);
      localObject2 = ((n)localObject1).c((Context)localObject2);
      localObject3 = ((n)localObject1).q();
      localObject3 = String.valueOf(localObject3);
      Object localObject4 = "no_cache";
      boolean bool2 = ((String)localObject4).equals(localObject3);
      if (!bool2)
      {
        localObject4 = aw.d();
        if (localObject2 == null) {
          localObject5 = aw.c();
        } else {
          localObject5 = localObject2;
        }
        ((g)localObject4).put(localObject3, localObject5);
      }
      localObject4 = b;
      localObject5 = a;
      bool2 = ((aw)localObject4).a((aw.j)localObject5);
      if (bool2)
      {
        localObject2 = aw.b();
        localObject3 = d;
        ((Handler)localObject2).post((Runnable)localObject3);
        return;
      }
      localObject4 = new com/truecaller/util/aw$a;
      localObject5 = b;
      aw.j localj = a;
      ((aw.a)localObject4).<init>((aw)localObject5, (String)localObject3, (Bitmap)localObject2, localj);
      localObject2 = aw.b();
      ((Handler)localObject2).post((Runnable)localObject4);
      localObject2 = aw.b();
      localObject3 = d;
      ((Handler)localObject2).post((Runnable)localObject3);
      return;
    }
    catch (Exception localException)
    {
      Object localObject3 = new String[1];
      Object localObject5 = new java/lang/StringBuilder;
      ((StringBuilder)localObject5).<init>("In PhotosLoader run - ");
      localObject1 = localObject1.getClass().getSimpleName();
      ((StringBuilder)localObject5).append((String)localObject1);
      ((StringBuilder)localObject5).append(" - Exception: ");
      localObject1 = localException.getMessage();
      ((StringBuilder)localObject5).append((String)localObject1);
      localObject1 = ((StringBuilder)localObject5).toString();
      localObject3[0] = localObject1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.aw.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */