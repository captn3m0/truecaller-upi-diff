package com.truecaller.util;

import android.content.Context;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class dc
  implements d
{
  private final Provider a;
  
  private dc(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static bd a(Context paramContext)
  {
    be localbe = new com/truecaller/util/be;
    localbe.<init>(paramContext);
    return (bd)g.a(localbe, "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static dc a(Provider paramProvider)
  {
    dc localdc = new com/truecaller/util/dc;
    localdc.<init>(paramProvider);
    return localdc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.dc
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */