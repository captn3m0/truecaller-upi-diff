package com.truecaller.util;

import android.graphics.Bitmap;
import android.net.Uri;
import com.truecaller.messaging.data.types.ImageEntity;

public abstract interface g
{
  public abstract int a();
  
  public abstract Bitmap a(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3);
  
  public abstract ImageEntity a(Uri paramUri);
  
  public abstract ImageEntity a(Uri paramUri, int paramInt);
  
  public abstract byte[] b(Uri paramUri);
  
  public abstract Uri c(Uri paramUri);
  
  public abstract Uri d(Uri paramUri);
  
  public abstract boolean e(Uri paramUri);
  
  public abstract Uri f(Uri paramUri);
}

/* Location:
 * Qualified Name:     com.truecaller.util.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */