package com.truecaller.util;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build.VERSION;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.a;
import android.support.v4.graphics.drawable.f;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.TouchDelegate;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Checkable;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.b.ab;
import com.d.b.t;
import com.d.b.w;
import com.truecaller.common.b.e;
import com.truecaller.common.h.l;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.components.ComboBase;
import com.truecaller.ui.components.ComboBase.a;
import com.truecaller.ui.components.n;
import com.truecaller.ui.view.c;
import java.util.Iterator;
import java.util.List;

public final class at
  extends l
{
  public static int a(int paramInt1, int paramInt2, float paramFloat)
  {
    float f1 = paramInt1 >> 24 & 0xFF;
    float f2 = 1.0F - paramFloat;
    f1 *= f2;
    float f3 = (paramInt2 >> 24 & 0xFF) * paramFloat;
    int i = (int)(f1 + f3);
    f3 = (paramInt1 >> 16 & 0xFF) * f2;
    float f4 = (paramInt2 >> 16 & 0xFF) * paramFloat;
    int j = (int)(f3 + f4);
    f4 = (paramInt1 >> 8 & 0xFF) * f2;
    float f5 = (paramInt2 >> 8 & 0xFF) * paramFloat;
    int k = (int)(f4 + f5);
    float f6 = (paramInt1 & 0xFF) * f2;
    float f7 = (paramInt2 & 0xFF) * paramFloat;
    paramInt1 = (int)(f6 + f7);
    paramInt2 = i << 24;
    int m = j << 16;
    paramInt2 |= m;
    m = k << 8;
    paramInt2 |= m;
    return paramInt1 | paramInt2;
  }
  
  public static int a(Resources paramResources)
  {
    String str1 = "status_bar_height";
    String str2 = "dimen";
    String str3 = "android";
    int i = paramResources.getIdentifier(str1, str2, str3);
    int j;
    if (i > 0)
    {
      j = paramResources.getDimensionPixelSize(i);
    }
    else
    {
      j = 0;
      paramResources = null;
    }
    return j;
  }
  
  public static int a(String paramString, float paramFloat, Typeface paramTypeface)
  {
    TextPaint localTextPaint = new android/text/TextPaint;
    int i = 129;
    localTextPaint.<init>(i);
    localTextPaint.setTextSize(paramFloat);
    localTextPaint.setTypeface(paramTypeface);
    int j = paramString.length();
    paramTypeface = null;
    int k = 0;
    int m = 0;
    for (;;)
    {
      i = j + -1;
      if (k >= i) {
        break;
      }
      boolean bool = true;
      float f = 999.0F;
      i = localTextPaint.breakText(paramString, k, j, bool, f, null);
      k += i;
      m += 1;
    }
    paramString = new android/graphics/Rect;
    paramString.<init>();
    localTextPaint.getTextBounds("Py", 0, 2, paramString);
    int n = paramString.height();
    return m * n;
  }
  
  public static Bitmap a(Context paramContext, String paramString)
  {
    boolean bool1 = TextUtils.isEmpty(paramString);
    Context localContext = null;
    if (bool1) {
      return null;
    }
    int i = 1115684864;
    float f = 64.0F;
    try
    {
      i = a(paramContext, f);
      Object localObject1 = w.a(paramContext);
      paramString = ((w)localObject1).a(paramString);
      paramString = paramString.b();
      paramString = paramString.b(i, i);
      boolean bool2 = Settings.d();
      Object localObject2;
      if (!bool2)
      {
        localObject2 = t.c;
        localObject1 = null;
        localObject1 = new t[0];
        paramString.a((t)localObject2);
      }
      paramString = paramString.d();
      if (paramString != null)
      {
        paramContext = paramContext.getResources();
        paramContext = android.support.v4.graphics.drawable.d.a(paramContext, paramString);
        boolean bool3 = true;
        g = bool3;
        paramContext.f = bool3;
        paramContext.b();
        paramString = b;
        localObject2 = c;
        paramString.setShader((Shader)localObject2);
        paramContext.invalidateSelf();
        paramContext = a(paramContext);
        localContext = paramContext;
      }
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    return localContext;
  }
  
  public static Drawable a(Context paramContext, int paramInt, ColorStateList paramColorStateList)
  {
    paramContext = a.e(a(paramContext, paramInt).mutate());
    a.a(paramContext, paramColorStateList);
    return paramContext;
  }
  
  public static SwitchCompat a(View paramView, int paramInt, boolean paramBoolean, CompoundButton.OnCheckedChangeListener paramOnCheckedChangeListener)
  {
    paramView = g(paramView, paramInt);
    if (paramView == null) {
      return null;
    }
    paramView.setChecked(paramBoolean);
    paramView.setOnCheckedChangeListener(paramOnCheckedChangeListener);
    return paramView;
  }
  
  public static SwitchCompat a(View paramView, String paramString)
  {
    int i = 2131364331;
    SwitchCompat localSwitchCompat = g(paramView, i);
    if (localSwitchCompat == null) {
      return null;
    }
    boolean bool = Settings.e(paramString);
    localSwitchCompat.setChecked(bool);
    int k = 2131364332;
    int m = 2131887071;
    int n = 2131887070;
    -..Lambda.at.tF5ejvPY7uLjq1vzYRmSh1W_J4k localtF5ejvPY7uLjq1vzYRmSh1W_J4k = new com/truecaller/util/-$$Lambda$at$tF5ejvPY7uLjq1vzYRmSh1W_J4k;
    localtF5ejvPY7uLjq1vzYRmSh1W_J4k.<init>(paramString, paramView, k, m, n);
    localSwitchCompat.setOnCheckedChangeListener(localtF5ejvPY7uLjq1vzYRmSh1W_J4k);
    int i1 = 2131364332;
    bool = localSwitchCompat.isChecked();
    int j;
    if (bool) {
      j = 2131887071;
    } else {
      j = 2131887070;
    }
    a(paramView, i1, j);
    return localSwitchCompat;
  }
  
  public static View a(View paramView, int paramInt, View.OnClickListener paramOnClickListener)
  {
    if (paramView != null)
    {
      paramView = paramView.findViewById(paramInt);
      if (paramView != null)
      {
        paramInt = 1;
        paramView.setClickable(paramInt);
        paramView.setOnClickListener(paramOnClickListener);
      }
      return paramView;
    }
    return paramView;
  }
  
  public static View a(View paramView, int paramInt, boolean paramBoolean)
  {
    return a(paramView.findViewById(paramInt), paramBoolean, true);
  }
  
  public static View a(View paramView, boolean paramBoolean)
  {
    return a(paramView, paramBoolean, true);
  }
  
  public static View a(View paramView, boolean paramBoolean1, boolean paramBoolean2)
  {
    if (paramView != null)
    {
      if (paramBoolean1) {
        paramBoolean1 = false;
      } else if (paramBoolean2) {
        paramBoolean1 = true;
      } else {
        paramBoolean1 = true;
      }
      paramView.setVisibility(paramBoolean1);
    }
    return paramView;
  }
  
  public static ComboBase a(View paramView, int paramInt, List paramList, String paramString)
  {
    if (paramView != null)
    {
      Context localContext = paramView.getContext();
      if (localContext != null)
      {
        paramView = h(paramView, paramInt);
        paramView.setData(paramList);
        Object localObject = Settings.b(paramString);
        localObject = a(paramList, (String)localObject);
        paramView.setSelection((n)localObject);
        localObject = new com/truecaller/util/-$$Lambda$at$BZn-_d--RrrCRJWjqV4hVcuhMbk;
        ((-..Lambda.at.BZn-_d--RrrCRJWjqV4hVcuhMbk)localObject).<init>(paramString);
        paramView.a((ComboBase.a)localObject);
        return paramView;
      }
    }
    return null;
  }
  
  public static ComboBase a(View paramView, List paramList, String paramString)
  {
    String str = null;
    if (paramView != null)
    {
      Context localContext = paramView.getContext();
      if (localContext != null)
      {
        int i = 2131364328;
        paramView = h(paramView, i);
        if (paramView == null) {
          return null;
        }
        paramView.setData(paramList);
        str = e.a(paramString);
        paramList = a(paramList, str);
        paramView.setSelection(paramList);
        paramList = new com/truecaller/util/-$$Lambda$at$EV-KDbTsEv17epR-u3qYmJ7IlmM;
        paramList.<init>(paramString);
        paramView.a(paramList);
        return paramView;
      }
    }
    return null;
  }
  
  public static n a(List paramList, String paramString)
  {
    Iterator localIterator = paramList.iterator();
    n localn;
    boolean bool2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localn = (n)localIterator.next();
      String str = localn.q().toString();
      bool2 = str.equalsIgnoreCase(paramString);
    } while (!bool2);
    return localn;
    return (n)paramList.get(0);
  }
  
  public static void a(Context paramContext, Drawable paramDrawable, int paramInt)
  {
    a(paramContext, paramDrawable, paramInt, false);
  }
  
  private static void a(Context paramContext, Drawable paramDrawable, int paramInt, boolean paramBoolean)
  {
    boolean bool1 = paramDrawable instanceof f;
    if (bool1)
    {
      paramDrawable = (LayerDrawable)a.f((Drawable)paramDrawable);
    }
    else
    {
      bool1 = paramDrawable instanceof LayerDrawable;
      if (!bool1) {
        return;
      }
      paramDrawable = (LayerDrawable)paramDrawable;
    }
    int i = 2131363291;
    Object localObject = paramDrawable.findDrawableByLayerId(i);
    boolean bool2 = localObject instanceof c;
    if (bool2)
    {
      localObject = (c)localObject;
    }
    else
    {
      localObject = new com/truecaller/ui/view/c;
      int j = 2130969528;
      boolean bool3 = true;
      ((c)localObject).<init>(paramContext, bool3, bool3, j);
    }
    ((c)localObject).a(paramBoolean);
    if (!paramBoolean) {
      ((c)localObject).a(paramInt);
    }
    paramDrawable.mutate();
    paramDrawable.setDrawableByLayerId(i, (Drawable)localObject);
    return;
  }
  
  public static void a(Context paramContext, Drawable paramDrawable, boolean paramBoolean)
  {
    a(paramContext, paramDrawable, 0, paramBoolean);
  }
  
  public static void a(Context paramContext, Toolbar paramToolbar)
  {
    Drawable localDrawable = paramToolbar.getNavigationIcon();
    int i = 2130969592;
    Object localObject;
    if (localDrawable != null)
    {
      localDrawable = a.e(localDrawable);
      localObject = paramToolbar.getContext();
      com.truecaller.utils.ui.b.a((Context)localObject, localDrawable, i);
      paramToolbar.setNavigationIcon(localDrawable);
    }
    paramToolbar = paramToolbar.getMenu();
    int j = 0;
    localDrawable = null;
    for (;;)
    {
      int k = paramToolbar.size();
      if (j >= k) {
        break;
      }
      localObject = paramToolbar.getItem(j);
      a(paramContext, (MenuItem)localObject, i);
      j += 1;
    }
  }
  
  public static void a(Context paramContext, MenuItem paramMenuItem, int paramInt)
  {
    if (paramMenuItem == null) {
      return;
    }
    Drawable localDrawable1 = paramMenuItem.getIcon();
    if (localDrawable1 == null) {
      return;
    }
    Drawable localDrawable2 = a.e(localDrawable1);
    com.truecaller.utils.ui.b.a(paramContext, localDrawable2, paramInt);
    paramMenuItem.setIcon(localDrawable1);
  }
  
  public static void a(Toolbar paramToolbar)
  {
    Drawable localDrawable = paramToolbar.getOverflowIcon();
    if (localDrawable == null) {
      return;
    }
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    int k = 2130969581;
    if (i >= j)
    {
      localDrawable = a.e(localDrawable);
      com.truecaller.utils.ui.b.a(paramToolbar.getContext(), localDrawable, k);
      paramToolbar.setOverflowIcon(localDrawable);
      return;
    }
    localDrawable = localDrawable.mutate();
    i = com.truecaller.utils.ui.b.a(paramToolbar.getContext(), k);
    PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
    localDrawable.setColorFilter(i, localMode);
    paramToolbar.setOverflowIcon(localDrawable);
  }
  
  public static void a(View paramView, int paramInt)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i < j)
    {
      i = paramView.getPaddingTop();
      j = paramView.getPaddingLeft();
      int k = paramView.getPaddingRight();
      int m = paramView.getPaddingBottom();
      paramView.setBackgroundResource(paramInt);
      paramView.setPadding(j, i, k, m);
      return;
    }
    paramView.setBackgroundResource(paramInt);
  }
  
  public static void a(View paramView, int paramInt1, int paramInt2)
  {
    a(c(paramView, paramInt1), paramInt2);
  }
  
  public static void a(View paramView, int paramInt, Drawable paramDrawable)
  {
    a(d(paramView, paramInt), paramDrawable);
  }
  
  public static void a(View paramView, int paramInt, CharSequence paramCharSequence)
  {
    b(c(paramView, paramInt), paramCharSequence);
  }
  
  public static void a(View paramView, Drawable paramDrawable)
  {
    if (paramView != null) {
      paramView.setBackground(paramDrawable);
    }
  }
  
  public static void a(ImageView paramImageView, int paramInt)
  {
    if (paramImageView == null) {
      return;
    }
    boolean bool = true;
    if (paramInt > 0)
    {
      paramImageView.setImageResource(paramInt);
      a(paramImageView, bool, bool);
      return;
    }
    a(paramImageView, false, bool);
  }
  
  public static void a(ImageView paramImageView, Drawable paramDrawable)
  {
    if (paramImageView == null) {
      return;
    }
    paramImageView.setImageDrawable(paramDrawable);
    boolean bool1 = true;
    boolean bool2;
    if (paramDrawable != null)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      paramDrawable = null;
    }
    a(paramImageView, bool2, bool1);
  }
  
  public static void a(TextView paramTextView, int paramInt)
  {
    if (paramTextView == null) {
      return;
    }
    paramTextView.setText(paramInt);
    CharSequence localCharSequence = paramTextView.getText();
    paramInt = TextUtils.isEmpty(localCharSequence);
    boolean bool = true;
    if (paramInt != 0)
    {
      localCharSequence = paramTextView.getHint();
      paramInt = TextUtils.isEmpty(localCharSequence);
      if (paramInt != 0)
      {
        paramInt = 0;
        localCharSequence = null;
        break label52;
      }
    }
    paramInt = 1;
    label52:
    a(paramTextView, paramInt, bool);
  }
  
  public static void a(TextView paramTextView, CharSequence paramCharSequence)
  {
    paramCharSequence = a(paramCharSequence);
    b(paramTextView, paramCharSequence);
  }
  
  public static boolean a(Fragment paramFragment)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.PICK");
    String str = "vnd.android.cursor.dir/phone_v2";
    localIntent = localIntent.setType(str);
    int i = 1003;
    try
    {
      paramFragment.startActivityForResult(localIntent, i);
      return true;
    }
    catch (Exception localException)
    {
      com.truecaller.log.d.a(localException;
    }
    return false;
  }
  
  public static Drawable b(Context paramContext, int paramInt1, int paramInt2)
  {
    ColorStateList localColorStateList = com.truecaller.utils.ui.b.b(paramContext, paramInt2);
    return a(paramContext, paramInt1, localColorStateList);
  }
  
  public static View b(Context paramContext, int paramInt)
  {
    return LayoutInflater.from(paramContext).inflate(paramInt, null);
  }
  
  public static View b(View paramView, int paramInt)
  {
    return paramView.findViewById(paramInt);
  }
  
  public static View b(View paramView, int paramInt, boolean paramBoolean)
  {
    return a(paramView.findViewById(paramInt), paramBoolean, true);
  }
  
  public static void b(View paramView, int paramInt1, int paramInt2)
  {
    -..Lambda.at.fX6KfgNhuzl5GhXpvGs5pytP-mY localfX6KfgNhuzl5GhXpvGs5pytP-mY = new com/truecaller/util/-$$Lambda$at$fX6KfgNhuzl5GhXpvGs5pytP-mY;
    localfX6KfgNhuzl5GhXpvGs5pytP-mY.<init>(paramView, paramInt1, paramInt2);
    paramView.post(localfX6KfgNhuzl5GhXpvGs5pytP-mY);
  }
  
  public static void b(TextView paramTextView, int paramInt)
  {
    if (paramTextView == null) {
      return;
    }
    paramTextView.setTextColor(paramInt);
  }
  
  public static void b(TextView paramTextView, CharSequence paramCharSequence)
  {
    if (paramTextView == null) {
      return;
    }
    paramTextView.setText(paramCharSequence);
    paramCharSequence = paramTextView.getText();
    boolean bool1 = TextUtils.isEmpty(paramCharSequence);
    boolean bool2 = true;
    if (bool1)
    {
      paramCharSequence = paramTextView.getHint();
      bool1 = TextUtils.isEmpty(paramCharSequence);
      if (bool1)
      {
        bool1 = false;
        paramCharSequence = null;
        break label52;
      }
    }
    bool1 = true;
    label52:
    a(paramTextView, bool1, bool2);
  }
  
  public static TouchDelegate c(View paramView, int paramInt1, int paramInt2)
  {
    Rect localRect = new android/graphics/Rect;
    localRect.<init>();
    paramView.getHitRect(localRect);
    int i = localRect.width();
    paramInt1 = Math.max(paramInt1 - i, 0) / 2;
    float f1 = paramInt1;
    int k = localRect.height();
    paramInt2 = Math.max(paramInt2 - k, 0) / 2;
    float f2 = paramInt2;
    i = 0;
    boolean bool2 = f1 < 0.0F;
    if (!bool2)
    {
      boolean bool1 = f2 < 0.0F;
      if (!bool1) {
        return null;
      }
    }
    int j = (int)(left - f1);
    left = j;
    paramInt1 = (int)(right + f1);
    right = paramInt1;
    paramInt1 = (int)(top - f2);
    top = paramInt1;
    paramInt1 = (int)(bottom + f2);
    bottom = paramInt1;
    TouchDelegate localTouchDelegate = new android/view/TouchDelegate;
    localTouchDelegate.<init>(localRect, paramView);
    return localTouchDelegate;
  }
  
  public static TextView c(View paramView, int paramInt)
  {
    return (TextView)paramView.findViewById(paramInt);
  }
  
  public static void c(View paramView, int paramInt, boolean paramBoolean)
  {
    paramView = paramView.findViewById(paramInt);
    paramInt = paramView instanceof Checkable;
    if (paramInt != 0)
    {
      paramView = (Checkable)paramView;
      paramView.setChecked(paramBoolean);
    }
  }
  
  public static void c(TextView paramTextView, int paramInt)
  {
    if (paramTextView == null) {
      return;
    }
    paramTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(paramInt, 0, 0, 0);
  }
  
  public static ImageView d(View paramView, int paramInt)
  {
    return (ImageView)paramView.findViewById(paramInt);
  }
  
  public static void d(TextView paramTextView, int paramInt)
  {
    Object localObject = paramTextView.getCompoundDrawables();
    int i = localObject.length;
    Drawable localDrawable1 = null;
    int j = 0;
    while (j < i)
    {
      Drawable localDrawable2 = localObject[j];
      if (localDrawable2 != null)
      {
        localDrawable2 = a.e(localDrawable2);
        Context localContext = paramTextView.getContext();
        com.truecaller.utils.ui.b.a(localContext, localDrawable2, paramInt);
      }
      j += 1;
    }
    Drawable localDrawable3 = localObject[0];
    Drawable localDrawable4 = localObject[1];
    localDrawable1 = localObject[2];
    localObject = localObject[3];
    paramTextView.setCompoundDrawables(localDrawable3, localDrawable4, localDrawable1, (Drawable)localObject);
  }
  
  public static void e(View paramView, int paramInt)
  {
    a(d(paramView, 2131363654), paramInt);
  }
  
  public static void e(TextView paramTextView, int paramInt)
  {
    Object localObject = paramTextView.getCompoundDrawablesRelative();
    int i = localObject.length;
    Drawable localDrawable1 = null;
    int j = 0;
    while (j < i)
    {
      Drawable localDrawable2 = localObject[j];
      if (localDrawable2 != null)
      {
        localDrawable2 = a.e(localDrawable2);
        Context localContext = paramTextView.getContext();
        com.truecaller.utils.ui.b.a(localContext, localDrawable2, paramInt);
      }
      j += 1;
    }
    Drawable localDrawable3 = localObject[0];
    Drawable localDrawable4 = localObject[1];
    localDrawable1 = localObject[2];
    localObject = localObject[3];
    paramTextView.setCompoundDrawablesRelative(localDrawable3, localDrawable4, localDrawable1, (Drawable)localObject);
  }
  
  public static String f(View paramView, int paramInt)
  {
    paramView = (EditText)paramView.findViewById(paramInt);
    if (paramView == null) {
      return null;
    }
    return paramView.getText().toString();
  }
  
  public static SwitchCompat g(View paramView, int paramInt)
  {
    if (paramView != null) {
      return (SwitchCompat)paramView.findViewById(paramInt);
    }
    return null;
  }
  
  public static ComboBase h(View paramView, int paramInt)
  {
    return (ComboBase)paramView.findViewById(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.at
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */