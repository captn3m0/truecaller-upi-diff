package com.truecaller.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

final class s$1
  implements Iterator
{
  public final boolean hasNext()
  {
    return false;
  }
  
  public final Object next()
  {
    NoSuchElementException localNoSuchElementException = new java/util/NoSuchElementException;
    localNoSuchElementException.<init>();
    throw localNoSuchElementException;
  }
  
  public final void remove()
  {
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>();
    throw localIllegalStateException;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.s.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */