package com.truecaller.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.log.AssertionUtil;

public final class ca$d
  extends ca
{
  private final String a;
  
  public ca$d(String paramString)
  {
    super((byte)0);
    a = paramString;
  }
  
  public final void a(Context paramContext)
  {
    Object localObject1 = "context";
    k.b(paramContext, (String)localObject1);
    try
    {
      localObject1 = new android/content/Intent;
      String str1 = "android.provider.Telephony.SECRET_CODE";
      Object localObject2 = new java/lang/StringBuilder;
      String str2 = "android_secret_code://";
      ((StringBuilder)localObject2).<init>(str2);
      str2 = a;
      ((StringBuilder)localObject2).append(str2);
      localObject2 = ((StringBuilder)localObject2).toString();
      localObject2 = Uri.parse((String)localObject2);
      ((Intent)localObject1).<init>(str1, (Uri)localObject2);
      paramContext.sendBroadcast((Intent)localObject1);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSecurityException);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ca.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */