package com.truecaller.util;

import android.content.Context;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.data.entity.Number;
import com.truecaller.search.e;
import com.truecaller.search.f;
import com.truecaller.utils.l;

final class v
  implements u
{
  private final Context a;
  
  v(Context paramContext)
  {
    a = paramContext;
  }
  
  public final boolean a(Number paramNumber)
  {
    Context localContext = a;
    Object localObject = ((bk)localContext.getApplicationContext()).a().bw();
    String[] arrayOfString = { "android.permission.READ_CONTACTS" };
    boolean bool1 = ((l)localObject).a(arrayOfString);
    if (bool1)
    {
      localObject = e.a();
      boolean bool2 = ((e)localObject).a(localContext, paramNumber);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean a(String paramString)
  {
    return f.a(a, paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */