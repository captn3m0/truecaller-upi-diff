package com.truecaller.util;

import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;

public final class bk
{
  public final ObjectAnimator a;
  
  private bk(ObjectAnimator paramObjectAnimator)
  {
    a = paramObjectAnimator;
  }
  
  public static bk a(Object paramObject, String paramString, float... paramVarArgs)
  {
    paramObject = ObjectAnimator.ofFloat(paramObject, paramString, paramVarArgs);
    paramString = new com/truecaller/util/bk;
    paramString.<init>((ObjectAnimator)paramObject);
    return paramString;
  }
  
  public final bk a(long paramLong)
  {
    a.setStartDelay(paramLong);
    return this;
  }
  
  public final bk a(Animator.AnimatorListener paramAnimatorListener)
  {
    a.addListener(paramAnimatorListener);
    return this;
  }
  
  public final bk a(TimeInterpolator paramTimeInterpolator)
  {
    a.setInterpolator(paramTimeInterpolator);
    return this;
  }
  
  public final bk b(long paramLong)
  {
    a.setDuration(paramLong);
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bk
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */