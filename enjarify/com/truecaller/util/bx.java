package com.truecaller.util;

import dagger.a.d;
import javax.inject.Provider;

public final class bx
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private bx(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static bx a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    bx localbx = new com/truecaller/util/bx;
    localbx.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localbx;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bx
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */