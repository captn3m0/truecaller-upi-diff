package com.truecaller.util;

import android.app.Activity;
import android.app.FragmentManager;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.bp;
import com.truecaller.common.h.am;
import com.truecaller.notifications.m;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.old.data.entity.Notification.NotificationState;

public final class NotificationUtil$e
  implements NotificationUtil.b
{
  private final Activity a;
  private final Notification b;
  
  public NotificationUtil$e(Activity paramActivity, Notification paramNotification)
  {
    a = paramActivity;
    b = paramNotification;
  }
  
  public final void a()
  {
    Object localObject1 = m.a(b.d);
    Object localObject2 = a.getFragmentManager();
    String str1 = "dialog";
    ((m)localObject1).show((FragmentManager)localObject2, str1);
    localObject1 = b.b;
    localObject2 = Notification.NotificationState.NEW;
    if (localObject1 == localObject2)
    {
      localObject1 = TrueApp.y().a().c();
      localObject2 = new com/truecaller/analytics/e$a;
      ((e.a)localObject2).<init>("ANDROID_webview_notification_shown");
      str1 = "campaign";
      Object localObject3 = b;
      String str2 = "ci";
      localObject3 = am.a(((Notification)localObject3).a(str2));
      localObject2 = ((e.a)localObject2).a(str1, (String)localObject3).a();
      ((b)localObject1).a((e)localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.NotificationUtil.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */