package com.truecaller.util;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public final class bi
  implements ThreadFactory
{
  public static final bi a;
  private static final AtomicInteger b;
  
  static
  {
    Object localObject = new com/truecaller/util/bi;
    ((bi)localObject).<init>();
    a = (bi)localObject;
    localObject = new java/util/concurrent/atomic/AtomicInteger;
    ((AtomicInteger)localObject).<init>();
    b = (AtomicInteger)localObject;
  }
  
  public final Thread newThread(Runnable paramRunnable)
  {
    Thread localThread = new java/lang/Thread;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("image-utils-thread-pool-");
    int i = b.getAndIncrement();
    ((StringBuilder)localObject).append(i);
    localObject = ((StringBuilder)localObject).toString();
    localThread.<init>(paramRunnable, (String)localObject);
    int j = 1;
    localThread.setPriority(j);
    localThread.setDaemon(j);
    return localThread;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bi
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */