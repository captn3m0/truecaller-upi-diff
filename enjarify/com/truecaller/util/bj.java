package com.truecaller.util;

import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.google.c.a.g;
import com.google.c.a.k;
import com.google.c.a.k.c;
import com.google.c.a.m.a;
import com.truecaller.TrueApp;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.h;
import com.truecaller.common.h.u;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.messaging.conversation.ConversationActivity;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.search.local.b.f;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class bj
{
  private static final ThreadLocal a;
  private static String b = "";
  private static final Pattern c = Pattern.compile("[+0-9.\\p{Space}()\\p{Pd}*#]*[0-9*#][+0-9.\\p{Space}()\\p{Pd}*#,;]*");
  
  static
  {
    bj.1 local1 = new com/truecaller/util/bj$1;
    local1.<init>();
    a = local1;
  }
  
  public static String a(String paramString)
  {
    boolean bool = ab.b(paramString);
    if (bool) {
      return paramString.trim();
    }
    return PhoneNumberUtils.extractNetworkPortion(paramString);
  }
  
  public static void a(Context paramContext, String paramString)
  {
    boolean bool = ab.a(paramString);
    if (bool)
    {
      Object localObject = ((bk)paramContext.getApplicationContext()).a().V();
      paramString = Participant.b(paramString, (u)localObject, "-1");
      localObject = new android/content/Intent;
      ((Intent)localObject).<init>(paramContext, ConversationActivity.class);
      String str = "participants";
      int i = 1;
      Participant[] arrayOfParticipant = new Participant[i];
      arrayOfParticipant[0] = paramString;
      ((Intent)localObject).putExtra(str, arrayOfParticipant);
      int j = 268435456;
      ((Intent)localObject).addFlags(j);
      paramContext.startActivity((Intent)localObject);
    }
  }
  
  public static boolean a(String paramString1, String paramString2)
  {
    boolean bool1 = true;
    if (paramString1 != paramString2)
    {
      if ((paramString1 != null) && (paramString2 != null))
      {
        int i = e(paramString1);
        int j = e(paramString2);
        if (i == j)
        {
          if (paramString1 != paramString2)
          {
            boolean bool2 = TextUtils.isEmpty(paramString1);
            if (bool2)
            {
              bool2 = TextUtils.isEmpty(paramString2);
              if (bool2) {}
            }
            else
            {
              bool3 = ab.a(paramString1, paramString2, bool1);
              if (!bool3)
              {
                bool3 = false;
                paramString1 = null;
                break label82;
              }
            }
          }
          boolean bool3 = true;
          label82:
          if (bool3) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public static String b(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool) {
      return "";
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int i = paramString.length();
    int j = 0;
    for (;;)
    {
      if (j >= i) {
        break label164;
      }
      int k = paramString.charAt(j);
      int m = Character.digit(k, 10);
      int i4 = -1;
      if (m != i4)
      {
        localStringBuilder.append(m);
      }
      else
      {
        m = localStringBuilder.length();
        if (m == 0)
        {
          m = 43;
          if (k == m)
          {
            localStringBuilder.append(k);
            break label155;
          }
        }
        int n = 97;
        if (k >= n)
        {
          int i1 = 122;
          if (k <= i1) {}
        }
        else
        {
          int i2 = 65;
          if (k < i2) {
            break label155;
          }
          int i3 = 90;
          if (k > i3) {
            break label155;
          }
        }
        paramString = f(paramString);
        break;
      }
      label155:
      j += 1;
    }
    label164:
    return localStringBuilder.toString();
  }
  
  public static boolean b(Context paramContext, String paramString)
  {
    String str = b;
    if (str != null)
    {
      int i = str.length();
      if (i == 0)
      {
        str = "phone";
        paramContext = ((TelephonyManager)paramContext.getSystemService(str)).getVoiceMailNumber();
        b = paramContext;
      }
    }
    paramContext = b;
    if (paramContext != null)
    {
      boolean bool = paramContext.equals(paramString);
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public static boolean c(String paramString)
  {
    if (paramString != null)
    {
      Pattern localPattern = c;
      paramString = localPattern.matcher(paramString);
      boolean bool = paramString.matches();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public static String d(String paramString)
  {
    String str = g(paramString);
    boolean bool = TextUtils.isEmpty(str);
    if (!bool) {
      paramString = PhoneNumberUtils.extractNetworkPortion(str);
    }
    return paramString;
  }
  
  public static int e(String paramString)
  {
    int i = 0;
    if (paramString == null) {
      return 0;
    }
    int j = paramString.length();
    if (j == 0) {
      return 0;
    }
    int k = j + -7;
    k = Math.max(0, k);
    j += -1;
    while (j >= k)
    {
      i *= 31;
      int m = paramString.charAt(j);
      i += m;
      j += -1;
    }
    return i;
  }
  
  private static String f(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    int i = paramString.length();
    if (i == 0) {
      return paramString;
    }
    paramString = paramString.toCharArray();
    int j = 0;
    while (j < i)
    {
      char c1 = paramString[j];
      f localf = f.a;
      c1 = localf.a(c1);
      paramString[j] = c1;
      j += 1;
    }
    String str = new java/lang/String;
    str.<init>(paramString);
    return str;
  }
  
  private static String g(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    Object localObject1 = h.b(TrueApp.y().t());
    if (localObject1 != null)
    {
      Object localObject2 = d;
      if (localObject2 != null)
      {
        localObject2 = d;
        String str = String.valueOf(localObject2);
        Object localObject3 = "00".concat(str);
        boolean bool1 = paramString.startsWith((String)localObject3);
        str = null;
        int i = 1;
        if (!bool1)
        {
          localObject3 = "+";
          localObject2 = String.valueOf(localObject2);
          localObject2 = ((String)localObject3).concat((String)localObject2);
          bool2 = paramString.startsWith((String)localObject2);
          if (!bool2)
          {
            bool2 = false;
            localObject2 = null;
            break label107;
          }
        }
        boolean bool2 = true;
        label107:
        if (!bool2) {
          return null;
        }
        try
        {
          localObject2 = a;
          localObject2 = ((ThreadLocal)localObject2).get();
          localObject2 = (m.a)localObject2;
          localObject3 = k.a();
          localObject1 = c;
          ((k)localObject3).a(paramString, (String)localObject1, (m.a)localObject2);
          paramString = k.a();
          localObject1 = k.c.c;
          return paramString.a((m.a)localObject2, (k.c)localObject1);
        }
        catch (g paramString)
        {
          localObject1 = new String[i];
          localObject2 = new java/lang/StringBuilder;
          localObject3 = "NumberParseException was thrown: ";
          ((StringBuilder)localObject2).<init>((String)localObject3);
          paramString = paramString.toString();
          ((StringBuilder)localObject2).append(paramString);
          paramString = ((StringBuilder)localObject2).toString();
          localObject1[0] = paramString;
        }
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */