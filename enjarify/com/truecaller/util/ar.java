package com.truecaller.util;

import c.g.b.k;
import java.io.File;

public final class ar
  implements aq
{
  public final boolean a(String paramString)
  {
    k.b(paramString, "absolutePath");
    File localFile = new java/io/File;
    localFile.<init>(paramString);
    return localFile.isDirectory();
  }
  
  public final boolean b(String paramString)
  {
    k.b(paramString, "absolutePath");
    File localFile = new java/io/File;
    localFile.<init>(paramString);
    return localFile.mkdirs();
  }
  
  public final boolean c(String paramString)
  {
    k.b(paramString, "absolutePath");
    File localFile = new java/io/File;
    localFile.<init>(paramString);
    return localFile.delete();
  }
  
  public final boolean d(String paramString)
  {
    k.b(paramString, "absolutePath");
    File localFile = new java/io/File;
    localFile.<init>(paramString);
    return localFile.exists();
  }
  
  public final boolean e(String paramString)
  {
    k.b(paramString, "absolutePath");
    File localFile = new java/io/File;
    localFile.<init>(paramString);
    return localFile.createNewFile();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ar
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */