package com.truecaller.util;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.calling.initiate_call.b;

public final class a
{
  public static void a(Activity paramActivity, Intent paramIntent)
  {
    Object localObject1 = (com.truecaller.common.b.a)paramActivity.getApplicationContext();
    Object localObject2 = localObject1;
    localObject2 = ((bk)localObject1).a();
    boolean bool1 = ((com.truecaller.common.b.a)localObject1).o();
    if ((!bool1) && (paramIntent != null))
    {
      paramIntent = paramIntent.getExtras();
      if (paramIntent != null)
      {
        localObject1 = "VOICEMAIL";
        boolean bool2 = paramIntent.getBoolean((String)localObject1);
        if (bool2)
        {
          ((bp)localObject2).bM().a(paramActivity);
          paramIntent = new android/content/Intent;
          paramIntent.<init>("com.android.dialer.calllog.ACTION_MARK_NEW_VOICEMAILS_AS_OLD");
          localObject1 = ComponentName.unflattenFromString("com.android.dialer/.calllog.CallLogNotificationsService");
          paramIntent.setComponent((ComponentName)localObject1);
          paramActivity.startService(paramIntent);
        }
      }
    }
  }
  
  public static void a(Context paramContext)
  {
    Object localObject = (com.truecaller.common.b.a)paramContext.getApplicationContext();
    boolean bool = ((com.truecaller.common.b.a)localObject).o();
    if (bool) {
      return;
    }
    localObject = new android/content/Intent;
    ((Intent)localObject).<init>("com.truecaller.GRANT_DEFAULT_PERMISSIONS");
    paramContext.sendBroadcast((Intent)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */