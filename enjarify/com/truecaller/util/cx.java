package com.truecaller.util;

import android.content.Context;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class cx
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private cx(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static bg a(com.truecaller.multisim.h paramh, com.truecaller.messaging.h paramh1, Context paramContext)
  {
    bh localbh = new com/truecaller/util/bh;
    localbh.<init>(paramh, paramh1, paramContext);
    return (bg)g.a(localbh, "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static cx a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    cx localcx = new com/truecaller/util/cx;
    localcx.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localcx;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cx
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */