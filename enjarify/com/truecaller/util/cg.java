package com.truecaller.util;

import android.content.Context;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import com.truecaller.utils.l;
import java.util.Iterator;
import java.util.List;

public final class cg
  implements cf
{
  private final Context b;
  private final l c;
  private final com.truecaller.utils.d d;
  
  public cg(Context paramContext, l paraml, com.truecaller.utils.d paramd)
  {
    b = paramContext;
    c = paraml;
    d = paramd;
  }
  
  private final int a(Context paramContext)
  {
    Object localObject1 = c;
    String[] arrayOfString = { "android.permission.READ_PHONE_STATE" };
    int i = ((l)localObject1).a(arrayOfString);
    arrayOfString = null;
    if (i == 0) {
      return 0;
    }
    localObject1 = "telecom";
    paramContext = paramContext.getSystemService((String)localObject1);
    i = paramContext instanceof TelecomManager;
    boolean bool1;
    if (i == 0)
    {
      bool1 = false;
      paramContext = null;
    }
    paramContext = (TelecomManager)paramContext;
    if (paramContext == null) {
      return 0;
    }
    try
    {
      localObject1 = paramContext.getCallCapablePhoneAccounts();
      Object localObject2 = "telecomManager.callCapablePhoneAccounts";
      c.g.b.k.a(localObject1, (String)localObject2);
      localObject1 = ((List)localObject1).iterator();
      boolean bool3;
      do
      {
        do
        {
          boolean bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          localObject2 = (PhoneAccountHandle)((Iterator)localObject1).next();
          localObject2 = paramContext.getPhoneAccount((PhoneAccountHandle)localObject2);
        } while (localObject2 == null);
        bool3 = ((PhoneAccount)localObject2).hasCapabilities(8);
      } while (!bool3);
      bool1 = com.truecaller.common.h.k.g();
      i = 1;
      if (!bool1) {
        return i;
      }
      bool1 = ((PhoneAccount)localObject2).hasCapabilities(256);
      int j;
      if (bool1) {
        j = 3;
      }
      return j;
      return 0;
    }
    catch (SecurityException localSecurityException)
    {
      com.truecaller.log.d.a((Throwable)localSecurityException, "Couldn't get video calling availability");
    }
    return 0;
  }
  
  public final boolean a()
  {
    Object localObject = d;
    int i = ((com.truecaller.utils.d)localObject).h();
    int j = 23;
    if (i >= j)
    {
      localObject = b;
      i = a((Context)localObject);
      j = 1;
      i &= j;
      if (i != 0) {
        return j;
      }
    }
    return false;
  }
  
  public final boolean a(int paramInt)
  {
    int i = 1;
    paramInt &= i;
    if (paramInt != 0) {
      return i;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cg
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */