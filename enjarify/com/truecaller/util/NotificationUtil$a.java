package com.truecaller.util;

import android.app.Activity;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.search.global.n;

public final class NotificationUtil$a
  implements NotificationUtil.b
{
  private final Activity a;
  private final String b;
  
  public NotificationUtil$a(Activity paramActivity, Notification paramNotification)
  {
    a = paramActivity;
    paramActivity = paramNotification.a("n");
    b = paramActivity;
  }
  
  public final void a()
  {
    Activity localActivity = a;
    String str = b;
    n.a(localActivity, str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.NotificationUtil.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */