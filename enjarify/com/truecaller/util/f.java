package com.truecaller.util;

import android.content.Context;
import android.content.pm.PackageManager;
import android.telephony.TelephonyManager;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.e.a;
import com.truecaller.log.d;
import com.truecaller.utils.extensions.i;

public final class f
{
  public static void a(Context paramContext, int paramInt)
  {
    if (paramContext == null) {
      return;
    }
    Object localObject1 = ((bk)paramContext.getApplicationContext()).a().D();
    Object localObject2 = "phone";
    try
    {
      localObject2 = paramContext.getSystemService((String)localObject2);
      localObject2 = (TelephonyManager)localObject2;
      localObject3 = "hasNativeDialerCallerId";
      boolean bool = ((com.truecaller.i.c)localObject1).b((String)localObject3);
      if (bool)
      {
        bool = localObject2 instanceof a;
        if (bool) {}
      }
      else
      {
        me.leolin.shortcutbadger.c.a(paramContext, paramInt);
      }
    }
    catch (RuntimeException localRuntimeException)
    {
      localObject1 = paramContext.getPackageName();
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Package name: ");
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(". Launch intent: ");
      Object localObject3 = paramContext.getPackageManager();
      localObject1 = String.valueOf(((PackageManager)localObject3).getLaunchIntentForPackage((String)localObject1));
      ((StringBuilder)localObject2).append((String)localObject1);
      localObject1 = ((StringBuilder)localObject2).toString();
      d.a(localRuntimeException, (String)localObject1);
    }
    i.a(paramContext, "com.truecaller.action.UPDATE_CALL_BADGE");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */