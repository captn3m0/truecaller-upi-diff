package com.truecaller.util;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.RemoteException;
import android.text.TextUtils;
import com.truecaller.TrueApp;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.c;
import com.truecaller.common.h.h;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.a;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.content.TruecallerContract.ae;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.log.d;
import com.truecaller.log.f;
import com.truecaller.utils.l;
import java.util.ArrayList;

public final class co
{
  public static boolean a(Context paramContext)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    int i = 3;
    localArrayList.<init>(i);
    Object localObject = ContentProviderOperation.newDelete(TruecallerContract.ah.a()).build();
    localArrayList.add(localObject);
    localObject = ContentProviderOperation.newDelete(TruecallerContract.a.a()).build();
    localArrayList.add(localObject);
    localObject = ContentProviderOperation.newDelete(TruecallerContract.n.a()).build();
    localArrayList.add(localObject);
    localObject = ContentProviderOperation.newDelete(TruecallerContract.aa.a()).build();
    localArrayList.add(localObject);
    localObject = TruecallerContract.b;
    String str = "msg/msg_conversations";
    localObject = ContentProviderOperation.newDelete(Uri.withAppendedPath((Uri)localObject, str)).build();
    localArrayList.add(localObject);
    localObject = ContentProviderOperation.newDelete(TruecallerContract.ae.a()).build();
    localArrayList.add(localObject);
    boolean bool;
    try
    {
      paramContext = paramContext.getContentResolver();
      localObject = TruecallerContract.a;
      paramContext.applyBatch((String)localObject, localArrayList);
      bool = true;
    }
    catch (RemoteException|OperationApplicationException localRemoteException)
    {
      bool = false;
      paramContext = null;
    }
    return bool;
  }
  
  public static Location b(Context paramContext)
  {
    Object localObject1 = ((bk)paramContext.getApplicationContext()).a().bw();
    Object localObject2 = { "android.permission.ACCESS_COARSE_LOCATION" };
    boolean bool = ((l)localObject1).a((String[])localObject2);
    if (bool) {
      localObject1 = "location";
    }
    try
    {
      try
      {
        paramContext = paramContext.getSystemService((String)localObject1);
        paramContext = (LocationManager)paramContext;
        localObject1 = "network";
        paramContext = paramContext.getLastKnownLocation((String)localObject1);
      }
      catch (RuntimeException paramContext)
      {
        int i = 3;
        localObject2 = "AdsUtil";
        paramContext = f.a(paramContext);
        d.a(i, (String)localObject2, paramContext);
      }
    }
    catch (SecurityException localSecurityException)
    {
      for (;;) {}
    }
    paramContext = null;
    return paramContext;
  }
  
  public static boolean c(Context paramContext)
  {
    Object localObject = ((TrueApp)paramContext.getApplicationContext()).a().aI();
    boolean bool1 = ((c)localObject).b();
    if (!bool1) {
      return false;
    }
    paramContext = h.c(paramContext);
    if (paramContext != null)
    {
      localObject = c;
      bool1 = TextUtils.isEmpty((CharSequence)localObject);
      if (!bool1)
      {
        paramContext = c;
        localObject = "gb";
        boolean bool2 = paramContext.equalsIgnoreCase((String)localObject);
        if (bool2) {
          return true;
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.co
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */