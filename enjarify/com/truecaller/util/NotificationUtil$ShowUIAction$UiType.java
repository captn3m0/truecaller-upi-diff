package com.truecaller.util;

public enum NotificationUtil$ShowUIAction$UiType
{
  public final String name;
  
  static
  {
    Object localObject = new com/truecaller/util/NotificationUtil$ShowUIAction$UiType;
    ((UiType)localObject).<init>("PROFILE_EDIT", 0, "profileEdit");
    PROFILE_EDIT = (UiType)localObject;
    localObject = new com/truecaller/util/NotificationUtil$ShowUIAction$UiType;
    int i = 1;
    ((UiType)localObject).<init>("SEARCH", i, "search");
    SEARCH = (UiType)localObject;
    localObject = new com/truecaller/util/NotificationUtil$ShowUIAction$UiType;
    int j = 2;
    ((UiType)localObject).<init>("HISTORY", j, "history");
    HISTORY = (UiType)localObject;
    localObject = new com/truecaller/util/NotificationUtil$ShowUIAction$UiType;
    int k = 3;
    ((UiType)localObject).<init>("NOTIFICATIONS", k, "notifications");
    NOTIFICATIONS = (UiType)localObject;
    localObject = new com/truecaller/util/NotificationUtil$ShowUIAction$UiType;
    int m = 4;
    ((UiType)localObject).<init>("BLOCK", m, "filterMy");
    BLOCK = (UiType)localObject;
    localObject = new com/truecaller/util/NotificationUtil$ShowUIAction$UiType;
    int n = 5;
    ((UiType)localObject).<init>("PREMIUM", n, "premium");
    PREMIUM = (UiType)localObject;
    localObject = new com/truecaller/util/NotificationUtil$ShowUIAction$UiType;
    int i1 = 6;
    ((UiType)localObject).<init>("SETTINGS", i1, "settings");
    SETTINGS = (UiType)localObject;
    localObject = new com/truecaller/util/NotificationUtil$ShowUIAction$UiType;
    int i2 = 7;
    ((UiType)localObject).<init>("SETTINGS_GENERAL", i2, "settingsGeneral");
    SETTINGS_GENERAL = (UiType)localObject;
    localObject = new com/truecaller/util/NotificationUtil$ShowUIAction$UiType;
    int i3 = 8;
    ((UiType)localObject).<init>("SETTINGS_UPDATE", i3, "settingsUpdate");
    SETTINGS_UPDATE = (UiType)localObject;
    localObject = new com/truecaller/util/NotificationUtil$ShowUIAction$UiType;
    int i4 = 9;
    ((UiType)localObject).<init>("SETTINGS_CALLERID", i4, "settingsCallerId");
    SETTINGS_CALLERID = (UiType)localObject;
    localObject = new com/truecaller/util/NotificationUtil$ShowUIAction$UiType;
    int i5 = 10;
    ((UiType)localObject).<init>("SETTINGS_PRIVACY", i5, "settingsPrivacy");
    SETTINGS_PRIVACY = (UiType)localObject;
    localObject = new com/truecaller/util/NotificationUtil$ShowUIAction$UiType;
    int i6 = 11;
    ((UiType)localObject).<init>("SETTINGS_ABOUT", i6, "settingsAbout");
    SETTINGS_ABOUT = (UiType)localObject;
    localObject = new com/truecaller/util/NotificationUtil$ShowUIAction$UiType;
    int i7 = 12;
    ((UiType)localObject).<init>("SETTINGS_GENERAL_LANGUAGE", i7, "settingsGeneralLanguage");
    SETTINGS_GENERAL_LANGUAGE = (UiType)localObject;
    localObject = new UiType[13];
    UiType localUiType = PROFILE_EDIT;
    localObject[0] = localUiType;
    localUiType = SEARCH;
    localObject[i] = localUiType;
    localUiType = HISTORY;
    localObject[j] = localUiType;
    localUiType = NOTIFICATIONS;
    localObject[k] = localUiType;
    localUiType = BLOCK;
    localObject[m] = localUiType;
    localUiType = PREMIUM;
    localObject[n] = localUiType;
    localUiType = SETTINGS;
    localObject[i1] = localUiType;
    localUiType = SETTINGS_GENERAL;
    localObject[i2] = localUiType;
    localUiType = SETTINGS_UPDATE;
    localObject[i3] = localUiType;
    localUiType = SETTINGS_CALLERID;
    localObject[i4] = localUiType;
    localUiType = SETTINGS_PRIVACY;
    localObject[i5] = localUiType;
    localUiType = SETTINGS_ABOUT;
    localObject[i6] = localUiType;
    localUiType = SETTINGS_GENERAL_LANGUAGE;
    localObject[i7] = localUiType;
    $VALUES = (UiType[])localObject;
  }
  
  private NotificationUtil$ShowUIAction$UiType(String paramString1)
  {
    name = paramString1;
  }
  
  public static UiType getUiType(String paramString)
  {
    UiType[] arrayOfUiType = values();
    int i = arrayOfUiType.length;
    int j = 0;
    while (j < i)
    {
      UiType localUiType = arrayOfUiType[j];
      String str = localUiType.getName();
      boolean bool = str.equals(paramString);
      if (bool) {
        return localUiType;
      }
      j += 1;
    }
    return null;
  }
  
  public final String getName()
  {
    return name;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.NotificationUtil.ShowUIAction.UiType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */