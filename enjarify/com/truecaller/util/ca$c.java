package com.truecaller.util;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import c.g.b.k;

public final class ca$c
  extends ca
{
  public ca$c()
  {
    super((byte)0);
  }
  
  public final void a(Context paramContext)
  {
    Object localObject = "context";
    k.b(paramContext, (String)localObject);
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i < j) {
      return;
    }
    try
    {
      localObject = new android/content/Intent;
      String str = "android.settings.SHOW_REGULATORY_INFO";
      ((Intent)localObject).<init>(str);
      paramContext.startActivity((Intent)localObject);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException) {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ca.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */