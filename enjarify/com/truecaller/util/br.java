package com.truecaller.util;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog.Builder;
import android.text.TextUtils;
import com.google.firebase.perf.network.FirebasePerfOkHttpClient;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.bp;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import okhttp3.u;

public final class br
  extends android.support.v4.app.e
{
  public static String a = "contact_save";
  private Contact b;
  private br.a c;
  private b d;
  private br.c e;
  
  public static br a(Contact paramContact, br.a parama)
  {
    br localbr = new com/truecaller/util/br;
    localbr.<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putParcelable("arg_contact", paramContact);
    localbr.setArguments(localBundle);
    c = parama;
    return localbr;
  }
  
  private static u a(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool) {
      return null;
    }
    try
    {
      return u.f(paramString);
    }
    catch (Exception localException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localException;
    }
    return null;
  }
  
  private void a(byte[] paramArrayOfByte)
  {
    br.a locala = c;
    if (locala == null)
    {
      AssertionUtil.OnlyInDebug.fail(new String[] { "Callback must always be set" });
      dismissAllowingStateLoss();
      return;
    }
    Contact localContact = b;
    locala.onContactPrepared(localContact, paramArrayOfByte);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = ((TrueApp)paramContext.getApplicationContext()).a().c();
    d = paramContext;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = c;
    if (paramBundle == null)
    {
      paramBundle = new String[] { "Callback not set for dialog" };
      AssertionUtil.OnlyInDebug.fail(paramBundle);
    }
    paramBundle = getArguments();
    Object localObject;
    if (paramBundle != null)
    {
      paramBundle = getArguments();
      localObject = "arg_contact";
      paramBundle = (Contact)paramBundle.getParcelable((String)localObject);
      b = paramBundle;
    }
    paramBundle = getContext();
    if (paramBundle != null)
    {
      paramBundle = b;
      if (paramBundle != null)
      {
        paramBundle = a(paramBundle.v());
        if (paramBundle != null)
        {
          localObject = new com/truecaller/util/br$c;
          Context localContext = getContext();
          ((br.c)localObject).<init>(this, localContext, paramBundle);
          e = ((br.c)localObject);
          paramBundle = e;
          FirebasePerfOkHttpClient.enqueue(a, paramBundle);
          return;
        }
        a(null);
        dismissAllowingStateLoss();
        return;
      }
    }
    dismissAllowingStateLoss();
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new android/support/v7/app/AlertDialog$Builder;
    Context localContext = getContext();
    paramBundle.<init>(localContext);
    return paramBundle.setCancelable(false).setMessage(2131887213).create();
  }
  
  public final void onDismiss(DialogInterface paramDialogInterface)
  {
    super.onDismiss(paramDialogInterface);
    paramDialogInterface = e;
    if (paramDialogInterface != null)
    {
      Object localObject = a;
      boolean bool = ((okhttp3.e)localObject).d();
      if (!bool)
      {
        localObject = br.b.e;
        paramDialogInterface.a((br.b)localObject);
        paramDialogInterface = a;
        paramDialogInterface.c();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.br
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */