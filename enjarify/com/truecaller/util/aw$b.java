package com.truecaller.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.f.g;
import android.text.TextUtils;
import android.util.Pair;
import com.truecaller.common.h.am;
import com.truecaller.data.entity.Contact;
import com.truecaller.ui.components.n;
import com.truecaller.utils.extensions.b;
import java.util.Collection;
import java.util.List;

final class aw$b
  implements Runnable
{
  final aw.j a;
  final aw.e b;
  private final Contact d;
  private final boolean e;
  private final Runnable f;
  
  private aw$b(aw paramaw, Contact paramContact, aw.j paramj, aw.e parame)
  {
    paramaw = new com/truecaller/util/aw$b$1;
    paramaw.<init>(this);
    f = paramaw;
    d = paramContact;
    a = paramj;
    b = parame;
    e = false;
  }
  
  private void a(Pair paramPair)
  {
    Handler localHandler = aw.b();
    -..Lambda.aw.b.nfxhd4c3GQrCL9zFCRIaYYuN18w localnfxhd4c3GQrCL9zFCRIaYYuN18w = new com/truecaller/util/-$$Lambda$aw$b$nfxhd4c3GQrCL9zFCRIaYYuN18w;
    localnfxhd4c3GQrCL9zFCRIaYYuN18w.<init>(this, paramPair);
    localHandler.post(localnfxhd4c3GQrCL9zFCRIaYYuN18w);
  }
  
  public final void run()
  {
    Object localObject1 = c;
    Object localObject2 = a;
    boolean bool1 = ((aw)localObject1).a((aw.j)localObject2);
    if (bool1) {
      return;
    }
    localObject1 = aw.b();
    localObject2 = f;
    ((Handler)localObject1).post((Runnable)localObject2);
    localObject1 = a.a;
    localObject2 = am.a(((n)localObject1).q());
    boolean bool2 = TextUtils.isEmpty((CharSequence)localObject2);
    int i = 0;
    Object localObject3 = null;
    Object localObject4;
    if (bool2)
    {
      bool2 = false;
      localObject4 = null;
    }
    else
    {
      localObject4 = "no_cache";
      bool2 = ((String)localObject4).equals(localObject2);
      if (!bool2)
      {
        localObject4 = (Bitmap)aw.d().get(localObject2);
      }
      else
      {
        bool2 = false;
        localObject4 = null;
      }
      Bitmap localBitmap = aw.c();
      if (localObject4 == localBitmap)
      {
        bool2 = false;
        localObject4 = null;
      }
      else
      {
        localObject4 = aw.a(c);
        localObject1 = ((n)localObject1).c((Context)localObject4);
        if (localObject1 != null)
        {
          localObject4 = aw.a(c);
          int j = 800;
          localObject1 = b.a((Bitmap)localObject1, (Context)localObject4, j);
        }
        localObject4 = "no_cache";
        bool2 = ((String)localObject4).equals(localObject2);
        if (!bool2)
        {
          if (localObject1 == null) {
            localObject4 = aw.c();
          } else {
            localObject4 = localObject1;
          }
          aw.a((String)localObject2, (Bitmap)localObject4);
        }
        localObject4 = new android/util/Pair;
        ((Pair)localObject4).<init>(localObject2, localObject1);
      }
    }
    if (localObject4 != null)
    {
      a((Pair)localObject4);
      return;
    }
    bool1 = e;
    if (bool1)
    {
      localObject1 = aw.a(c);
      localObject2 = d;
      localObject1 = aw.a((Context)localObject1, (Contact)localObject2);
      boolean bool3 = ((List)localObject1).isEmpty();
      if (!bool3)
      {
        localObject2 = aw.a((Collection)localObject1);
        if (localObject2 != null)
        {
          localObject3 = localObject2;
        }
        else
        {
          localObject2 = d.E();
          if (localObject2 != null)
          {
            bool2 = false;
            localObject1 = ((Uri)((List)localObject1).get(0)).toString();
            localObject4 = aw.a(c);
            long l = ((Long)localObject2).longValue();
            localObject2 = t.c((Context)localObject4, l);
            if (localObject2 != null)
            {
              localObject4 = aw.a(c);
              i = 600;
              localObject2 = b.a((Bitmap)localObject2, (Context)localObject4, i);
            }
            localObject4 = aw.d();
            if (localObject2 == null) {
              localObject3 = aw.c();
            } else {
              localObject3 = localObject2;
            }
            ((g)localObject4).put(localObject1, localObject3);
            localObject3 = new android/util/Pair;
            ((Pair)localObject3).<init>(localObject1, localObject2);
          }
        }
      }
    }
    a((Pair)localObject3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.aw.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */