package com.truecaller.util;

import android.content.Context;
import java.lang.reflect.Field;

public final class k
{
  public static final Object a(Context paramContext, String paramString)
  {
    c.g.b.k.b(paramContext, "receiver$0");
    c.g.b.k.b(paramString, "fieldName");
    try
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      paramContext = paramContext.getPackageName();
      localStringBuilder.append(paramContext);
      paramContext = ".BuildConfig";
      localStringBuilder.append(paramContext);
      paramContext = localStringBuilder.toString();
      paramContext = Class.forName(paramContext);
      paramContext = paramContext.getField(paramString);
      return paramContext.get(null);
    }
    catch (IllegalAccessException paramContext)
    {
      paramContext.printStackTrace();
    }
    catch (NoSuchFieldException paramContext)
    {
      paramContext.printStackTrace();
    }
    catch (ClassNotFoundException paramContext)
    {
      paramContext.printStackTrace();
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */