package com.truecaller.util.background;

import androidx.work.f;
import androidx.work.m;
import androidx.work.p;
import com.truecaller.common.background.g;
import com.truecaller.common.background.h;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class b
  implements a
{
  private final p a;
  
  public b(p paramp)
  {
    a = paramp;
  }
  
  public final void a(boolean paramBoolean)
  {
    Iterator localIterator = d.a().entrySet().iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = (Map.Entry)localIterator.next();
      String str = (String)((Map.Entry)localObject).getKey();
      localObject = (h)((Map.Entry)localObject).getValue();
      f localf;
      if (paramBoolean) {
        localf = f.a;
      } else {
        localf = f.b;
      }
      localObject = ((h)localObject).a().a();
      p localp = a;
      localp.a(str, localf, (m)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.background.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */