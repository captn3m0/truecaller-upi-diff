package com.truecaller.util.background;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.bp;
import com.truecaller.common.background.TrackedWorker;
import com.truecaller.util.ao;

public final class CleanUpBackgroundWorker
  extends TrackedWorker
{
  public static final CleanUpBackgroundWorker.a c;
  public b b;
  private final Context d;
  
  static
  {
    CleanUpBackgroundWorker.a locala = new com/truecaller/util/background/CleanUpBackgroundWorker$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public CleanUpBackgroundWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    d = paramContext;
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final b b()
  {
    b localb = b;
    if (localb == null)
    {
      String str = "analytics";
      k.a(str);
    }
    return localb;
  }
  
  public final boolean c()
  {
    return true;
  }
  
  public final ListenableWorker.a d()
  {
    ao.a(d);
    ListenableWorker.a locala = ListenableWorker.a.a();
    k.a(locala, "Result.success()");
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.background.CleanUpBackgroundWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */