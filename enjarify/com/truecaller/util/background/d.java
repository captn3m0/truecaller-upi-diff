package com.truecaller.util.background;

import c.a.ag;
import c.n;
import c.t;
import com.truecaller.analytics.InstalledAppsHeartbeatWorker;
import com.truecaller.analytics.sync.EventsUploadWorker;
import com.truecaller.backup.BackupLogWorker;
import com.truecaller.common.edge.EdgeLocationsWorker;
import com.truecaller.common.tag.sync.AvailableTagsDownloadWorker;
import com.truecaller.common.tag.sync.TagKeywordsDownloadWorker;
import com.truecaller.config.UpdateConfigWorker;
import com.truecaller.config.UpdateInstallationWorker;
import com.truecaller.credit.app.core.CreditFeatureSyncWorker;
import com.truecaller.filters.sync.TopSpammersSyncRecurringWorker;
import com.truecaller.messaging.transport.im.FetchImContactsWorker;
import com.truecaller.network.spamUrls.FetchSpamLinksWhiteListWorker;
import com.truecaller.presence.SendPresenceSettingWorker;
import com.truecaller.ugc.UGCBackgroundWorker;
import java.util.Map;

public final class d
{
  private static final Map a;
  
  static
  {
    n[] arrayOfn = new n[15];
    Object localObject = EdgeLocationsWorker.f;
    n localn = t.a("EdgeLocations", localObject);
    arrayOfn[0] = localn;
    localObject = FetchImContactsWorker.d;
    localn = t.a("FetchImContacts", localObject);
    arrayOfn[1] = localn;
    localObject = AvailableTagsDownloadWorker.d;
    localn = t.a("AvailableTagsDownloadWorker", localObject);
    arrayOfn[2] = localn;
    localObject = TagKeywordsDownloadWorker.e;
    localn = t.a("TagKeywordsDownloadWorker", localObject);
    arrayOfn[3] = localn;
    localObject = SendPresenceSettingWorker.e;
    localn = t.a("SendPresenceSetting", localObject);
    arrayOfn[4] = localn;
    localObject = UpdateInstallationWorker.d;
    localn = t.a("UpdateInstallation", localObject);
    arrayOfn[5] = localn;
    localObject = UpdateConfigWorker.e;
    localn = t.a("UpdateConfig", localObject);
    arrayOfn[6] = localn;
    localObject = FetchSpamLinksWhiteListWorker.e;
    localn = t.a("FetchSpamLinksWhiteList", localObject);
    arrayOfn[7] = localn;
    localObject = UGCBackgroundWorker.f;
    localn = t.a("UGCBackgroundWorker", localObject);
    arrayOfn[8] = localn;
    localObject = TopSpammersSyncRecurringWorker.e;
    localn = t.a("TopSpammersSyncRecurringWorker", localObject);
    arrayOfn[9] = localn;
    localObject = CleanUpBackgroundWorker.c;
    localn = t.a("CleanUpBackgroundWorker", localObject);
    arrayOfn[10] = localn;
    localObject = EventsUploadWorker.e;
    localn = t.a("EventsUploadWorker", localObject);
    arrayOfn[11] = localn;
    localObject = InstalledAppsHeartbeatWorker.f;
    localn = t.a("InstalledAppsHeartbeatWorker", localObject);
    arrayOfn[12] = localn;
    localObject = CreditFeatureSyncWorker.d;
    localn = t.a("CreditFeatureSyncWorker", localObject);
    arrayOfn[13] = localn;
    localObject = BackupLogWorker.d;
    localn = t.a("BackupLogWorker", localObject);
    arrayOfn[14] = localn;
    a = ag.a(arrayOfn);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.background.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */