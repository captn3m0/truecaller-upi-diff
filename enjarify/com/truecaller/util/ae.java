package com.truecaller.util;

import android.content.Context;
import com.truecaller.common.h.h;
import com.truecaller.common.network.country.CountryListDto.a;
import java.util.List;

final class ae
  implements ad
{
  private final Context a;
  
  ae(Context paramContext)
  {
    a = paramContext;
  }
  
  public final CountryListDto.a a(String paramString)
  {
    return h.c(paramString);
  }
  
  public final List a()
  {
    return h.a();
  }
  
  public final CountryListDto.a b()
  {
    return h.c(a);
  }
  
  public final CountryListDto.a b(String paramString)
  {
    return h.b(paramString);
  }
  
  public final CountryListDto.a c(String paramString)
  {
    return h.a(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ae
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */