package com.truecaller.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.AppCompatImageView;
import java.lang.ref.WeakReference;

public final class aw$g
  extends AppCompatImageView
{
  private final WeakReference a;
  
  public aw$g(Context paramContext, aw.e parame)
  {
    super(paramContext);
    paramContext = new java/lang/ref/WeakReference;
    paramContext.<init>(parame);
    a = paramContext;
  }
  
  public final void setImageBitmap(Bitmap paramBitmap)
  {
    aw.e locale = (aw.e)a.get();
    if (locale != null) {
      locale.a(this, paramBitmap, null);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.aw.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */