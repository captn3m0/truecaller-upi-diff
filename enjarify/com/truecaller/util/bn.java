package com.truecaller.util;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;
import android.text.TextUtils;
import com.truecaller.analytics.b;
import com.truecaller.common.h.am;
import com.truecaller.common.h.h;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.data.access.m;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Link;
import com.truecaller.data.entity.Note;
import com.truecaller.data.entity.StructuredName;
import com.truecaller.messaging.transport.im.j;
import com.truecaller.utils.l;
import java.util.Locale;

public final class bn
{
  final m a;
  private final Context b;
  private final b c;
  private final l d;
  private final j e;
  
  public bn(Context paramContext, b paramb, l paraml, j paramj)
  {
    b = paramContext;
    m localm = new com/truecaller/data/access/m;
    localm.<init>(paramContext);
    a = localm;
    c = paramb;
    d = paraml;
    e = paramj;
  }
  
  private static long a(long paramLong1, long paramLong2)
  {
    long l = paramLong1 << 7;
    return paramLong1 >> 57 ^ l ^ paramLong2;
  }
  
  private static Contact a(bm parambm)
  {
    long l1 = parambm.b();
    Contact localContact = new com/truecaller/data/entity/Contact;
    localContact.<init>();
    Object localObject1 = parambm.c();
    localContact.l((String)localObject1);
    localObject1 = Long.valueOf(l1);
    localContact.c((Long)localObject1);
    int i = 2;
    localContact.setSource(i);
    int j = parambm.d();
    int i4 = 1;
    Object localObject2;
    if (j == i4)
    {
      j = 1;
    }
    else
    {
      j = 0;
      localObject2 = null;
    }
    localContact.b(j);
    boolean bool7;
    do
    {
      localObject2 = d;
      Object localObject3 = parambm;
      localObject3 = (Cursor)parambm;
      Object localObject4 = bm.a;
      int i5 = 4;
      localObject4 = localObject4[i5];
      localObject2 = (String)((com.truecaller.utils.extensions.g)localObject2).a((Cursor)localObject3, (c.l.g)localObject4);
      localObject4 = e;
      Object localObject5 = bm.a;
      int i6 = 7;
      localObject5 = localObject5[i6];
      localObject4 = (Number)((com.truecaller.utils.extensions.g)localObject4).a((Cursor)localObject3, (c.l.g)localObject5);
      int i7 = ((Number)localObject4).intValue();
      boolean bool8;
      if (i7 == i4)
      {
        bool8 = true;
      }
      else
      {
        bool8 = false;
        localObject4 = null;
      }
      long l2 = parambm.a();
      int i9 = -1;
      int i10 = ((String)localObject2).hashCode();
      Object localObject6;
      boolean bool2;
      switch (i10)
      {
      default: 
        break;
      case 689862072: 
        String str = "vnd.android.cursor.item/organization";
        boolean bool1 = ((String)localObject2).equals(str);
        if (bool1) {
          int k = 4;
        }
        break;
      case 684173810: 
        localObject6 = "vnd.android.cursor.item/phone_v2";
        bool2 = ((String)localObject2).equals(localObject6);
        if (bool2)
        {
          bool2 = false;
          localObject2 = null;
        }
        break;
      case 456415478: 
        localObject6 = "vnd.android.cursor.item/website";
        bool2 = ((String)localObject2).equals(localObject6);
        if (bool2) {
          int m = 2;
        }
        break;
      case -601229436: 
        localObject6 = "vnd.android.cursor.item/postal-address_v2";
        boolean bool3 = ((String)localObject2).equals(localObject6);
        if (bool3) {
          int n = 3;
        }
        break;
      case -1079210633: 
        localObject6 = "vnd.android.cursor.item/note";
        boolean bool4 = ((String)localObject2).equals(localObject6);
        if (bool4) {
          int i1 = 6;
        }
        break;
      case -1079224304: 
        localObject6 = "vnd.android.cursor.item/name";
        boolean bool5 = ((String)localObject2).equals(localObject6);
        if (bool5) {
          int i2 = 5;
        }
        break;
      case -1569536764: 
        localObject6 = "vnd.android.cursor.item/email_v2";
        boolean bool6 = ((String)localObject2).equals(localObject6);
        if (bool6) {
          bool6 = true;
        }
        break;
      }
      int i3 = -1;
      int i11;
      switch (i3)
      {
      default: 
        break;
      case 6: 
        localObject2 = r;
        localObject4 = bm.a;
        i5 = 21;
        localObject4 = localObject4[i5];
        localObject2 = (String)((com.truecaller.utils.extensions.g)localObject2).a((Cursor)localObject3, (c.l.g)localObject4);
        if (localObject2 != null)
        {
          localObject3 = new com/truecaller/data/entity/Note;
          ((Note)localObject3).<init>();
          ((Note)localObject3).setValue((String)localObject2);
          h = ((Note)localObject3);
        }
        break;
      case 5: 
        localObject2 = new com/truecaller/data/entity/StructuredName;
        ((StructuredName)localObject2).<init>();
        localObject4 = o;
        localObject6 = bm.a[18];
        localObject4 = (String)((com.truecaller.utils.extensions.g)localObject4).a((Cursor)localObject3, (c.l.g)localObject6);
        ((StructuredName)localObject2).setFamilyName((String)localObject4);
        localObject4 = p;
        localObject6 = bm.a[19];
        localObject4 = (String)((com.truecaller.utils.extensions.g)localObject4).a((Cursor)localObject3, (c.l.g)localObject6);
        ((StructuredName)localObject2).setGivenName((String)localObject4);
        localObject4 = q;
        localObject6 = bm.a;
        i11 = 20;
        localObject6 = localObject6[i11];
        localObject3 = (String)((com.truecaller.utils.extensions.g)localObject4).a((Cursor)localObject3, (c.l.g)localObject6);
        ((StructuredName)localObject2).setMiddleName((String)localObject3);
        g = ((StructuredName)localObject2);
        break;
      case 4: 
        localObject2 = parambm.n;
        localObject4 = bm.a[17];
        localObject2 = (String)((com.truecaller.utils.extensions.g)localObject2).a((Cursor)localObject3, (c.l.g)localObject4);
        localContact.k((String)localObject2);
        localObject2 = parambm.m;
        localObject4 = bm.a;
        i5 = 16;
        localObject4 = localObject4[i5];
        localObject2 = (String)((com.truecaller.utils.extensions.g)localObject2).a((Cursor)localObject3, (c.l.g)localObject4);
        localContact.f((String)localObject2);
        break;
      case 3: 
        localObject2 = new com/truecaller/data/entity/Address;
        ((Address)localObject2).<init>();
        ((Address)localObject2).setIsPrimary(bool8);
        localObject4 = Long.valueOf(l2);
        ((Address)localObject2).setDataPhonebookId((Long)localObject4);
        localObject4 = parambm.i;
        localObject6 = bm.a[12];
        localObject4 = (String)((com.truecaller.utils.extensions.g)localObject4).a((Cursor)localObject3, (c.l.g)localObject6);
        ((Address)localObject2).setStreet((String)localObject4);
        localObject4 = parambm.j;
        localObject6 = bm.a[13];
        localObject4 = (String)((com.truecaller.utils.extensions.g)localObject4).a((Cursor)localObject3, (c.l.g)localObject6);
        ((Address)localObject2).setZipCode((String)localObject4);
        localObject4 = parambm.k;
        localObject6 = bm.a[14];
        localObject4 = (String)((com.truecaller.utils.extensions.g)localObject4).a((Cursor)localObject3, (c.l.g)localObject6);
        ((Address)localObject2).setCity((String)localObject4);
        localObject4 = l;
        localObject6 = bm.a;
        i11 = 15;
        localObject6 = localObject6[i11];
        localObject3 = am.a((String)((com.truecaller.utils.extensions.g)localObject4).a((Cursor)localObject3, (c.l.g)localObject6)).trim();
        localObject4 = h.b((String)localObject3);
        if (localObject4 == null) {
          localObject4 = h.c((String)localObject3);
        }
        if (localObject4 != null)
        {
          localObject3 = c;
          localObject4 = Locale.ENGLISH;
          localObject3 = am.a((String)localObject3, (Locale)localObject4);
          ((Address)localObject2).setCountryCode((String)localObject3);
        }
        localContact.a((Address)localObject2);
        break;
      case 2: 
        localObject2 = new com/truecaller/data/entity/Link;
        ((Link)localObject2).<init>();
        ((Link)localObject2).setIsPrimary(bool8);
        localObject3 = Long.valueOf(l2);
        ((Link)localObject2).setDataPhonebookId((Long)localObject3);
        ((Link)localObject2).setService("link");
        localObject3 = parambm.e();
        ((Link)localObject2).setInfo((String)localObject3);
        localContact.a((Link)localObject2);
        break;
      case 1: 
        localObject2 = new com/truecaller/data/entity/Link;
        ((Link)localObject2).<init>();
        ((Link)localObject2).setIsPrimary(bool8);
        localObject3 = Long.valueOf(l2);
        ((Link)localObject2).setDataPhonebookId((Long)localObject3);
        ((Link)localObject2).setService("email");
        localObject3 = parambm.e();
        ((Link)localObject2).setInfo((String)localObject3);
        localContact.a((Link)localObject2);
        break;
      case 0: 
        localObject2 = f;
        localObject6 = bm.a[8];
        localObject2 = (String)((com.truecaller.utils.extensions.g)localObject2).a((Cursor)localObject3, (c.l.g)localObject6);
        localObject6 = am.d((String)localObject2);
        boolean bool9 = TextUtils.isEmpty((CharSequence)localObject6);
        if (!bool9)
        {
          com.truecaller.data.entity.Number localNumber = new com/truecaller/data/entity/Number;
          localNumber.<init>((String)localObject6);
          localNumber.setIsPrimary(bool8);
          localObject4 = Long.valueOf(l2);
          localNumber.setDataPhonebookId((Long)localObject4);
          localObject4 = g;
          localObject6 = bm.a[9];
          int i8 = ((Number)((com.truecaller.utils.extensions.g)localObject4).a((Cursor)localObject3, (c.l.g)localObject6)).intValue();
          localNumber.b(i8);
          localObject4 = h;
          localObject6 = bm.a;
          i11 = 10;
          localObject6 = localObject6[i11];
          localObject3 = (String)((com.truecaller.utils.extensions.g)localObject4).a((Cursor)localObject3, (c.l.g)localObject6);
          localNumber.e((String)localObject3);
          localNumber.c((String)localObject2);
          localContact.a(localNumber);
          localObject2 = localContact.p();
          if (localObject2 == null)
          {
            localObject2 = localNumber.a();
            localContact.g((String)localObject2);
          }
        }
        break;
      }
      bool7 = parambm.moveToNext();
      if (!bool7) {
        break;
      }
      long l3 = parambm.b();
      bool7 = l1 < l3;
    } while (!bool7);
    return localContact;
  }
  
  /* Error */
  private bn.b a(Cursor paramCursor, Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aload_1
    //   1: astore 5
    //   3: ldc_w 311
    //   6: astore 6
    //   8: ldc_w 313
    //   11: astore 7
    //   13: ldc_w 315
    //   16: astore 8
    //   18: ldc_w 317
    //   21: astore 9
    //   23: ldc_w 319
    //   26: astore 10
    //   28: ldc_w 321
    //   31: astore 11
    //   33: ldc_w 323
    //   36: astore 12
    //   38: ldc_w 325
    //   41: astore 13
    //   43: ldc_w 327
    //   46: astore 14
    //   48: ldc_w 329
    //   51: astore 15
    //   53: ldc_w 331
    //   56: astore 16
    //   58: ldc_w 333
    //   61: astore 17
    //   63: ldc_w 335
    //   66: astore 18
    //   68: ldc_w 337
    //   71: astore 19
    //   73: ldc_w 321
    //   76: astore 20
    //   78: bipush 17
    //   80: anewarray 87	java/lang/String
    //   83: dup
    //   84: dup2
    //   85: iconst_0
    //   86: ldc_w 307
    //   89: aastore
    //   90: iconst_1
    //   91: ldc_w 309
    //   94: aastore
    //   95: dup2
    //   96: iconst_2
    //   97: aload 6
    //   99: aastore
    //   100: iconst_3
    //   101: aload 7
    //   103: aastore
    //   104: dup2
    //   105: iconst_4
    //   106: aload 8
    //   108: aastore
    //   109: iconst_5
    //   110: aload 9
    //   112: aastore
    //   113: dup2
    //   114: bipush 6
    //   116: aload 10
    //   118: aastore
    //   119: bipush 7
    //   121: aload 11
    //   123: aastore
    //   124: dup2
    //   125: bipush 8
    //   127: aload 12
    //   129: aastore
    //   130: bipush 9
    //   132: aload 13
    //   134: aastore
    //   135: dup2
    //   136: bipush 10
    //   138: aload 14
    //   140: aastore
    //   141: bipush 11
    //   143: aload 15
    //   145: aastore
    //   146: dup2
    //   147: bipush 12
    //   149: aload 16
    //   151: aastore
    //   152: bipush 13
    //   154: aload 17
    //   156: aastore
    //   157: dup2
    //   158: bipush 14
    //   160: aload 18
    //   162: aastore
    //   163: bipush 15
    //   165: aload 19
    //   167: aastore
    //   168: bipush 16
    //   170: aload 20
    //   172: aastore
    //   173: astore 21
    //   175: aload_0
    //   176: getfield 20	com/truecaller/util/bn:b	Landroid/content/Context;
    //   179: invokevirtual 343	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   182: astore 22
    //   184: ldc_w 345
    //   187: astore 23
    //   189: aload 22
    //   191: aload_2
    //   192: aload 21
    //   194: aload_3
    //   195: aload 4
    //   197: aload 23
    //   199: invokevirtual 351	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   202: astore 24
    //   204: iconst_0
    //   205: istore 25
    //   207: aconst_null
    //   208: astore 26
    //   210: aload 24
    //   212: ifnonnull +31 -> 243
    //   215: new 353	com/truecaller/util/bn$b
    //   218: astore 5
    //   220: new 355	java/util/ArrayList
    //   223: astore 24
    //   225: aload 24
    //   227: invokespecial 356	java/util/ArrayList:<init>	()V
    //   230: aload 5
    //   232: aload 24
    //   234: iconst_0
    //   235: iconst_0
    //   236: iconst_0
    //   237: invokespecial 359	com/truecaller/util/bn$b:<init>	(Ljava/util/List;IIB)V
    //   240: aload 5
    //   242: areturn
    //   243: new 361	com/truecaller/util/bn$a
    //   246: astore 6
    //   248: aload 6
    //   250: aload_0
    //   251: iconst_0
    //   252: invokespecial 364	com/truecaller/util/bn$a:<init>	(Lcom/truecaller/util/bn;B)V
    //   255: new 37	com/truecaller/util/bm
    //   258: astore 7
    //   260: aload 7
    //   262: aload 24
    //   264: invokespecial 367	com/truecaller/util/bm:<init>	(Landroid/database/Cursor;)V
    //   267: aload 7
    //   269: invokevirtual 370	com/truecaller/util/bm:moveToFirst	()Z
    //   272: pop
    //   273: aload_1
    //   274: invokeinterface 371 1 0
    //   279: istore 27
    //   281: iload 27
    //   283: ifeq +65 -> 348
    //   286: ldc_w 373
    //   289: astore 8
    //   291: aload_1
    //   292: aload 8
    //   294: invokeinterface 377 2 0
    //   299: istore 27
    //   301: ldc_w 379
    //   304: astore 9
    //   306: aload_1
    //   307: aload 9
    //   309: invokeinterface 377 2 0
    //   314: istore 28
    //   316: ldc_w 381
    //   319: astore 10
    //   321: aload_1
    //   322: aload 10
    //   324: invokeinterface 377 2 0
    //   329: istore 29
    //   331: iload 28
    //   333: istore 30
    //   335: iload 27
    //   337: istore 28
    //   339: iconst_0
    //   340: istore 27
    //   342: aconst_null
    //   343: astore 8
    //   345: goto +27 -> 372
    //   348: iconst_0
    //   349: istore 27
    //   351: aconst_null
    //   352: astore 8
    //   354: iconst_0
    //   355: istore 28
    //   357: aconst_null
    //   358: astore 9
    //   360: iconst_0
    //   361: istore 29
    //   363: aconst_null
    //   364: astore 10
    //   366: iconst_0
    //   367: istore 30
    //   369: aconst_null
    //   370: astore 11
    //   372: aload 7
    //   374: invokevirtual 384	com/truecaller/util/bm:isAfterLast	()Z
    //   377: istore 31
    //   379: iload 31
    //   381: ifeq +79 -> 460
    //   384: aload_1
    //   385: invokeinterface 385 1 0
    //   390: istore 31
    //   392: iload 31
    //   394: ifne +6 -> 400
    //   397: goto +63 -> 460
    //   400: aload 7
    //   402: invokevirtual 388	com/truecaller/util/bm:close	()V
    //   405: aload 6
    //   407: invokevirtual 390	com/truecaller/util/bn$a:a	()V
    //   410: aload 6
    //   412: getfield 393	com/truecaller/util/bn$a:b	Ljava/util/List;
    //   415: astore 5
    //   417: aload 5
    //   419: invokeinterface 398 1 0
    //   424: istore 32
    //   426: aload 6
    //   428: getfield 401	com/truecaller/util/bn$a:c	I
    //   431: istore 33
    //   433: iload 32
    //   435: iload 33
    //   437: iadd
    //   438: istore 32
    //   440: new 353	com/truecaller/util/bn$b
    //   443: astore 6
    //   445: aload 6
    //   447: aload 5
    //   449: iload 32
    //   451: iload 27
    //   453: iconst_0
    //   454: invokespecial 359	com/truecaller/util/bn$b:<init>	(Ljava/util/List;IIB)V
    //   457: aload 6
    //   459: areturn
    //   460: aload 7
    //   462: invokevirtual 384	com/truecaller/util/bm:isAfterLast	()Z
    //   465: istore 31
    //   467: iload 31
    //   469: ifeq +10 -> 479
    //   472: iconst_m1
    //   473: i2l
    //   474: lstore 34
    //   476: goto +10 -> 486
    //   479: aload 7
    //   481: invokevirtual 40	com/truecaller/util/bm:b	()J
    //   484: lstore 34
    //   486: aload_1
    //   487: invokeinterface 385 1 0
    //   492: istore 36
    //   494: iload 36
    //   496: ifeq +11 -> 507
    //   499: ldc2_w 402
    //   502: lstore 37
    //   504: goto +14 -> 518
    //   507: aload 5
    //   509: iload 28
    //   511: invokeinterface 407 2 0
    //   516: lstore 37
    //   518: aload 7
    //   520: invokevirtual 384	com/truecaller/util/bm:isAfterLast	()Z
    //   523: istore 39
    //   525: iload 39
    //   527: ifne +580 -> 1107
    //   530: lload 37
    //   532: lload 34
    //   534: lcmp
    //   535: istore 39
    //   537: iload 39
    //   539: ifge +6 -> 545
    //   542: goto +565 -> 1107
    //   545: aload_1
    //   546: invokeinterface 385 1 0
    //   551: istore 39
    //   553: iload 39
    //   555: ifne +27 -> 582
    //   558: lload 37
    //   560: lload 34
    //   562: lcmp
    //   563: istore 39
    //   565: iload 39
    //   567: ifle +6 -> 573
    //   570: goto +12 -> 582
    //   573: iconst_0
    //   574: istore 36
    //   576: aconst_null
    //   577: astore 14
    //   579: goto +6 -> 585
    //   582: iconst_1
    //   583: istore 36
    //   585: aload 7
    //   587: invokevirtual 410	com/truecaller/util/bm:getPosition	()I
    //   590: istore 40
    //   592: aload 7
    //   594: getfield 412	com/truecaller/util/bm:b	Lcom/truecaller/utils/extensions/g;
    //   597: astore 16
    //   599: aload 7
    //   601: astore 17
    //   603: aload 7
    //   605: checkcast 76	android/database/Cursor
    //   608: astore 17
    //   610: getstatic 79	com/truecaller/util/bm:a	[Lc/l/g;
    //   613: astore 18
    //   615: iconst_2
    //   616: istore 41
    //   618: aload 18
    //   620: iload 41
    //   622: aaload
    //   623: astore 24
    //   625: aload 16
    //   627: aload 17
    //   629: aload 24
    //   631: invokevirtual 85	com/truecaller/utils/extensions/g:a	(Landroid/database/Cursor;Lc/l/g;)Ljava/lang/Object;
    //   634: astore 24
    //   636: aload 24
    //   638: checkcast 87	java/lang/String
    //   641: astore 24
    //   643: aload 7
    //   645: invokevirtual 67	com/truecaller/util/bm:d	()I
    //   648: istore 39
    //   650: aload 6
    //   652: astore 4
    //   654: lconst_0
    //   655: lstore 42
    //   657: aload 7
    //   659: invokevirtual 46	com/truecaller/util/bm:c	()Ljava/lang/String;
    //   662: astore 17
    //   664: aload 17
    //   666: invokestatic 213	com/truecaller/common/h/am:a	(Ljava/lang/String;)Ljava/lang/String;
    //   669: astore 17
    //   671: aload 17
    //   673: invokevirtual 99	java/lang/String:hashCode	()I
    //   676: istore 44
    //   678: iload 27
    //   680: istore 45
    //   682: iload 28
    //   684: istore 41
    //   686: iload 44
    //   688: i2l
    //   689: lstore 46
    //   691: lload 42
    //   693: lload 46
    //   695: invokestatic 415	com/truecaller/util/bn:a	(JJ)J
    //   698: lstore 42
    //   700: aload 24
    //   702: invokevirtual 99	java/lang/String:hashCode	()I
    //   705: istore 27
    //   707: iload 27
    //   709: i2l
    //   710: lstore 46
    //   712: lload 42
    //   714: lload 46
    //   716: invokestatic 415	com/truecaller/util/bn:a	(JJ)J
    //   719: lstore 42
    //   721: iload 39
    //   723: i2l
    //   724: lstore 46
    //   726: lload 42
    //   728: lload 46
    //   730: invokestatic 415	com/truecaller/util/bn:a	(JJ)J
    //   733: lstore 42
    //   735: aload 7
    //   737: invokevirtual 96	com/truecaller/util/bm:a	()J
    //   740: lstore 46
    //   742: lload 42
    //   744: lload 46
    //   746: invokestatic 415	com/truecaller/util/bn:a	(JJ)J
    //   749: lstore 42
    //   751: aload 7
    //   753: getfield 417	com/truecaller/util/bm:c	Lcom/truecaller/utils/extensions/g;
    //   756: astore 8
    //   758: aload 7
    //   760: astore 9
    //   762: aload 7
    //   764: checkcast 76	android/database/Cursor
    //   767: astore 9
    //   769: getstatic 79	com/truecaller/util/bm:a	[Lc/l/g;
    //   772: astore 16
    //   774: iconst_3
    //   775: istore 44
    //   777: aload 16
    //   779: iload 44
    //   781: aaload
    //   782: astore 16
    //   784: aload 8
    //   786: aload 9
    //   788: aload 16
    //   790: invokevirtual 85	com/truecaller/utils/extensions/g:a	(Landroid/database/Cursor;Lc/l/g;)Ljava/lang/Object;
    //   793: astore 8
    //   795: aload 8
    //   797: checkcast 91	java/lang/Number
    //   800: astore 8
    //   802: aload 8
    //   804: invokevirtual 94	java/lang/Number:intValue	()I
    //   807: istore 27
    //   809: iload 27
    //   811: i2l
    //   812: lstore 46
    //   814: lload 42
    //   816: lload 46
    //   818: invokestatic 415	com/truecaller/util/bn:a	(JJ)J
    //   821: lstore 42
    //   823: aload 7
    //   825: invokevirtual 305	com/truecaller/util/bm:moveToNext	()Z
    //   828: istore 27
    //   830: iload 27
    //   832: ifeq +22 -> 854
    //   835: aload 7
    //   837: invokevirtual 40	com/truecaller/util/bm:b	()J
    //   840: lstore 46
    //   842: lload 34
    //   844: lload 46
    //   846: lcmp
    //   847: istore 39
    //   849: iload 39
    //   851: ifeq -116 -> 735
    //   854: iload 36
    //   856: ifne +36 -> 892
    //   859: aload 5
    //   861: iload 30
    //   863: invokeinterface 407 2 0
    //   868: lstore 46
    //   870: lload 42
    //   872: lload 46
    //   874: lcmp
    //   875: istore 31
    //   877: iload 31
    //   879: ifeq +6 -> 885
    //   882: goto +10 -> 892
    //   885: aload 4
    //   887: astore 26
    //   889: goto +185 -> 1074
    //   892: aload 7
    //   894: iload 40
    //   896: invokevirtual 421	com/truecaller/util/bm:moveToPosition	(I)Z
    //   899: pop
    //   900: aload 7
    //   902: invokestatic 424	com/truecaller/util/bn:a	(Lcom/truecaller/util/bm;)Lcom/truecaller/data/entity/Contact;
    //   905: astore 8
    //   907: lload 42
    //   909: invokestatic 56	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   912: astore 26
    //   914: aload 8
    //   916: aload 26
    //   918: invokevirtual 426	com/truecaller/data/entity/Contact:b	(Ljava/lang/Long;)V
    //   921: aload 8
    //   923: aload 24
    //   925: invokevirtual 428	com/truecaller/data/entity/Contact:m	(Ljava/lang/String;)V
    //   928: iload 36
    //   930: ifne +100 -> 1030
    //   933: aload 5
    //   935: iload 29
    //   937: invokeinterface 432 2 0
    //   942: astore 24
    //   944: aload 8
    //   946: aload 24
    //   948: invokevirtual 435	com/truecaller/data/entity/Contact:setTcId	(Ljava/lang/String;)V
    //   951: aload 8
    //   953: invokevirtual 438	com/truecaller/data/entity/Contact:O	()Z
    //   956: istore 25
    //   958: iload 25
    //   960: ifeq +37 -> 997
    //   963: aload 4
    //   965: astore 26
    //   967: aload 4
    //   969: getfield 441	com/truecaller/util/bn$a:a	Ljava/util/ArrayList;
    //   972: astore 6
    //   974: aload 6
    //   976: aload 24
    //   978: invokevirtual 444	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   981: pop
    //   982: aload 4
    //   984: invokevirtual 446	com/truecaller/util/bn$a:b	()V
    //   987: aload 4
    //   989: aload 8
    //   991: invokevirtual 449	com/truecaller/util/bn$a:a	(Lcom/truecaller/data/entity/Contact;)V
    //   994: goto +59 -> 1053
    //   997: aload 4
    //   999: astore 26
    //   1001: aload 8
    //   1003: invokevirtual 452	com/truecaller/data/entity/Contact:getTcId	()Ljava/lang/String;
    //   1006: astore 24
    //   1008: aload 24
    //   1010: ifnull +43 -> 1053
    //   1013: aload 8
    //   1015: invokevirtual 452	com/truecaller/data/entity/Contact:getTcId	()Ljava/lang/String;
    //   1018: astore 24
    //   1020: aload 4
    //   1022: aload 24
    //   1024: invokevirtual 454	com/truecaller/util/bn$a:a	(Ljava/lang/String;)V
    //   1027: goto +26 -> 1053
    //   1030: aload 4
    //   1032: astore 26
    //   1034: aload 8
    //   1036: invokevirtual 438	com/truecaller/data/entity/Contact:O	()Z
    //   1039: istore 32
    //   1041: iload 32
    //   1043: ifeq +10 -> 1053
    //   1046: aload 4
    //   1048: aload 8
    //   1050: invokevirtual 449	com/truecaller/util/bn$a:a	(Lcom/truecaller/data/entity/Contact;)V
    //   1053: aload 8
    //   1055: invokevirtual 438	com/truecaller/data/entity/Contact:O	()Z
    //   1058: istore 32
    //   1060: iload 32
    //   1062: ifne +12 -> 1074
    //   1065: iload 45
    //   1067: iconst_1
    //   1068: iadd
    //   1069: istore 27
    //   1071: goto +7 -> 1078
    //   1074: iload 45
    //   1076: istore 27
    //   1078: iload 36
    //   1080: ifne +10 -> 1090
    //   1083: aload_1
    //   1084: invokeinterface 455 1 0
    //   1089: pop
    //   1090: aload 26
    //   1092: astore 6
    //   1094: iload 41
    //   1096: istore 28
    //   1098: iconst_0
    //   1099: istore 25
    //   1101: aconst_null
    //   1102: astore 26
    //   1104: goto -732 -> 372
    //   1107: aload 6
    //   1109: astore 26
    //   1111: iload 27
    //   1113: istore 45
    //   1115: iload 28
    //   1117: istore 41
    //   1119: aload 5
    //   1121: iload 29
    //   1123: invokeinterface 432 2 0
    //   1128: astore 24
    //   1130: aload 6
    //   1132: aload 24
    //   1134: invokevirtual 454	com/truecaller/util/bn$a:a	(Ljava/lang/String;)V
    //   1137: aload_1
    //   1138: invokeinterface 455 1 0
    //   1143: pop
    //   1144: iconst_0
    //   1145: istore 25
    //   1147: aconst_null
    //   1148: astore 26
    //   1150: goto -778 -> 372
    //   1153: astore 5
    //   1155: aload 5
    //   1157: astore 24
    //   1159: aload 5
    //   1161: athrow
    //   1162: astore 5
    //   1164: aload 24
    //   1166: astore 26
    //   1168: aload 5
    //   1170: astore 24
    //   1172: aload 26
    //   1174: ifnull +27 -> 1201
    //   1177: aload 7
    //   1179: invokevirtual 388	com/truecaller/util/bm:close	()V
    //   1182: goto +24 -> 1206
    //   1185: astore 5
    //   1187: aload 5
    //   1189: astore 6
    //   1191: aload 26
    //   1193: aload 5
    //   1195: invokevirtual 461	java/lang/Throwable:addSuppressed	(Ljava/lang/Throwable;)V
    //   1198: goto +8 -> 1206
    //   1201: aload 7
    //   1203: invokevirtual 388	com/truecaller/util/bm:close	()V
    //   1206: aload 24
    //   1208: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1209	0	this	bn
    //   0	1209	1	paramCursor	Cursor
    //   0	1209	2	paramUri	Uri
    //   0	1209	3	paramString	String
    //   0	1209	4	paramArrayOfString	String[]
    //   1	1119	5	localObject1	Object
    //   1153	7	5	localObject2	Object
    //   1162	7	5	localObject3	Object
    //   1185	9	5	localThrowable	Throwable
    //   6	1184	6	localObject4	Object
    //   11	1191	7	localObject5	Object
    //   16	1038	8	localObject6	Object
    //   21	766	9	localObject7	Object
    //   26	339	10	str1	String
    //   31	340	11	str2	String
    //   36	92	12	str3	String
    //   41	92	13	str4	String
    //   46	532	14	str5	String
    //   51	93	15	str6	String
    //   56	733	16	localObject8	Object
    //   61	611	17	localObject9	Object
    //   66	553	18	localObject10	Object
    //   71	95	19	str7	String
    //   76	95	20	str8	String
    //   173	20	21	arrayOfString	String[]
    //   182	8	22	localContentResolver	ContentResolver
    //   187	11	23	str9	String
    //   202	1005	24	localObject11	Object
    //   205	941	25	bool1	boolean
    //   208	984	26	localObject12	Object
    //   279	3	27	bool2	boolean
    //   299	511	27	i	int
    //   828	3	27	bool3	boolean
    //   1069	43	27	j	int
    //   314	802	28	k	int
    //   329	793	29	m	int
    //   333	529	30	n	int
    //   377	501	31	bool4	boolean
    //   424	26	32	i1	int
    //   1039	22	32	bool5	boolean
    //   431	7	33	i2	int
    //   474	369	34	l1	long
    //   492	587	36	bool6	boolean
    //   502	57	37	l2	long
    //   523	43	39	bool7	boolean
    //   648	74	39	i3	int
    //   847	3	39	bool8	boolean
    //   590	305	40	i4	int
    //   616	502	41	i5	int
    //   655	253	42	l3	long
    //   676	104	44	i6	int
    //   680	434	45	i7	int
    //   689	184	46	l4	long
    // Exception table:
    //   from	to	target	type
    //   267	273	1153	finally
    //   273	279	1153	finally
    //   292	299	1153	finally
    //   307	314	1153	finally
    //   322	329	1153	finally
    //   372	377	1153	finally
    //   384	390	1153	finally
    //   460	465	1153	finally
    //   479	484	1153	finally
    //   486	492	1153	finally
    //   509	516	1153	finally
    //   518	523	1153	finally
    //   545	551	1153	finally
    //   585	590	1153	finally
    //   592	597	1153	finally
    //   603	608	1153	finally
    //   610	613	1153	finally
    //   620	623	1153	finally
    //   629	634	1153	finally
    //   636	641	1153	finally
    //   643	648	1153	finally
    //   657	662	1153	finally
    //   664	669	1153	finally
    //   671	676	1153	finally
    //   693	698	1153	finally
    //   700	705	1153	finally
    //   714	719	1153	finally
    //   728	733	1153	finally
    //   735	740	1153	finally
    //   744	749	1153	finally
    //   751	756	1153	finally
    //   762	767	1153	finally
    //   769	772	1153	finally
    //   779	782	1153	finally
    //   788	793	1153	finally
    //   795	800	1153	finally
    //   802	807	1153	finally
    //   816	821	1153	finally
    //   823	828	1153	finally
    //   835	840	1153	finally
    //   861	868	1153	finally
    //   894	900	1153	finally
    //   900	905	1153	finally
    //   907	912	1153	finally
    //   916	921	1153	finally
    //   923	928	1153	finally
    //   935	942	1153	finally
    //   946	951	1153	finally
    //   951	956	1153	finally
    //   967	972	1153	finally
    //   976	982	1153	finally
    //   982	987	1153	finally
    //   989	994	1153	finally
    //   1001	1006	1153	finally
    //   1013	1018	1153	finally
    //   1022	1027	1153	finally
    //   1034	1039	1153	finally
    //   1048	1053	1153	finally
    //   1053	1058	1153	finally
    //   1083	1090	1153	finally
    //   1121	1128	1153	finally
    //   1132	1137	1153	finally
    //   1137	1144	1153	finally
    //   1159	1162	1162	finally
    //   1177	1182	1185	finally
  }
  
  public final Uri a(Uri paramUri)
  {
    if (paramUri != null)
    {
      boolean bool = b();
      if (bool) {
        try
        {
          Object localObject = b;
          localObject = ((Context)localObject).getContentResolver();
          localObject = ContactsContract.Contacts.lookupContact((ContentResolver)localObject, paramUri);
          long l = ContentUris.parseId((Uri)localObject);
          localObject = "data";
          Uri localUri = Uri.withAppendedPath(paramUri, (String)localObject);
          return a(localUri, l, null, null);
        }
        catch (IllegalArgumentException|SQLiteException|SecurityException localIllegalArgumentException)
        {
          return null;
        }
      }
    }
    return null;
  }
  
  /* Error */
  final Uri a(Uri paramUri, long paramLong, String paramString, String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 6
    //   3: lconst_1
    //   4: lstore 7
    //   6: lload_2
    //   7: lload 7
    //   9: lcmp
    //   10: istore 9
    //   12: iload 9
    //   14: ifge +31 -> 45
    //   17: getstatic 500	java/lang/System:out	Ljava/io/PrintStream;
    //   20: astore_1
    //   21: lload_2
    //   22: invokestatic 505	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   25: astore 10
    //   27: ldc_w 502
    //   30: aload 10
    //   32: invokevirtual 508	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   35: astore 10
    //   37: aload_1
    //   38: aload 10
    //   40: invokevirtual 513	java/io/PrintStream:println	(Ljava/lang/String;)V
    //   43: aconst_null
    //   44: areturn
    //   45: aload_0
    //   46: getfield 27	com/truecaller/util/bn:a	Lcom/truecaller/data/access/m;
    //   49: getfield 516	com/truecaller/data/access/m:b	Landroid/content/ContentResolver;
    //   52: astore 11
    //   54: invokestatic 521	com/truecaller/content/TruecallerContract$ah:a	()Landroid/net/Uri;
    //   57: astore 12
    //   59: ldc_w 381
    //   62: astore 13
    //   64: iconst_3
    //   65: anewarray 87	java/lang/String
    //   68: dup
    //   69: dup2
    //   70: iconst_0
    //   71: aload 13
    //   73: aastore
    //   74: iconst_1
    //   75: ldc_w 373
    //   78: aastore
    //   79: iconst_2
    //   80: ldc_w 379
    //   83: aastore
    //   84: astore 14
    //   86: ldc_w 523
    //   89: astore 15
    //   91: iconst_1
    //   92: istore 16
    //   94: iload 16
    //   96: anewarray 87	java/lang/String
    //   99: astore 17
    //   101: lload_2
    //   102: invokestatic 505	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   105: astore 10
    //   107: aload 17
    //   109: iconst_0
    //   110: aload 10
    //   112: aastore
    //   113: aload 11
    //   115: aload 12
    //   117: aload 14
    //   119: aload 15
    //   121: aload 17
    //   123: aconst_null
    //   124: invokevirtual 351	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   127: astore 10
    //   129: aload_0
    //   130: aload 10
    //   132: aload_1
    //   133: aload 4
    //   135: aload 5
    //   137: invokespecial 526	com/truecaller/util/bn:a	(Landroid/database/Cursor;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/truecaller/util/bn$b;
    //   140: astore_1
    //   141: aload_1
    //   142: getfield 528	com/truecaller/util/bn$b:a	Ljava/util/List;
    //   145: astore_1
    //   146: aload_1
    //   147: ifnull +67 -> 214
    //   150: aload_1
    //   151: invokeinterface 398 1 0
    //   156: istore 18
    //   158: iload 18
    //   160: iload 16
    //   162: if_icmpeq +6 -> 168
    //   165: goto +49 -> 214
    //   168: invokestatic 521	com/truecaller/content/TruecallerContract$ah:a	()Landroid/net/Uri;
    //   171: astore 4
    //   173: aload_1
    //   174: iconst_0
    //   175: invokeinterface 532 2 0
    //   180: astore_1
    //   181: aload_1
    //   182: checkcast 52	java/lang/Long
    //   185: astore_1
    //   186: aload_1
    //   187: invokevirtual 535	java/lang/Long:longValue	()J
    //   190: lstore 7
    //   192: aload 4
    //   194: lload 7
    //   196: invokestatic 539	android/content/ContentUris:withAppendedId	(Landroid/net/Uri;J)Landroid/net/Uri;
    //   199: astore_1
    //   200: aload 10
    //   202: ifnull +10 -> 212
    //   205: aload 10
    //   207: invokeinterface 540 1 0
    //   212: aload_1
    //   213: areturn
    //   214: aload 10
    //   216: ifnull +10 -> 226
    //   219: aload 10
    //   221: invokeinterface 540 1 0
    //   226: aconst_null
    //   227: areturn
    //   228: astore_1
    //   229: goto +9 -> 238
    //   232: astore_1
    //   233: aload_1
    //   234: astore 6
    //   236: aload_1
    //   237: athrow
    //   238: aload 10
    //   240: ifnull +37 -> 277
    //   243: aload 6
    //   245: ifnull +25 -> 270
    //   248: aload 10
    //   250: invokeinterface 540 1 0
    //   255: goto +22 -> 277
    //   258: astore 10
    //   260: aload 6
    //   262: aload 10
    //   264: invokevirtual 461	java/lang/Throwable:addSuppressed	(Ljava/lang/Throwable;)V
    //   267: goto +10 -> 277
    //   270: aload 10
    //   272: invokeinterface 540 1 0
    //   277: aload_1
    //   278: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	279	0	this	bn
    //   0	279	1	paramUri	Uri
    //   0	279	2	paramLong	long
    //   0	279	4	paramString	String
    //   0	279	5	paramArrayOfString	String[]
    //   1	260	6	localUri1	Uri
    //   4	191	7	l	long
    //   10	3	9	bool	boolean
    //   25	224	10	localObject	Object
    //   258	13	10	localThrowable	Throwable
    //   52	62	11	localContentResolver	ContentResolver
    //   57	59	12	localUri2	Uri
    //   62	10	13	str1	String
    //   84	34	14	arrayOfString1	String[]
    //   89	31	15	str2	String
    //   92	71	16	i	int
    //   99	23	17	arrayOfString2	String[]
    //   156	7	18	j	int
    // Exception table:
    //   from	to	target	type
    //   236	238	228	finally
    //   135	140	232	finally
    //   141	145	232	finally
    //   150	156	232	finally
    //   168	171	232	finally
    //   174	180	232	finally
    //   181	185	232	finally
    //   186	190	232	finally
    //   194	199	232	finally
    //   248	255	258	finally
  }
  
  /* Error */
  public final void a()
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 463	com/truecaller/util/bn:b	()Z
    //   4: istore_1
    //   5: iload_1
    //   6: ifne +4 -> 10
    //   9: return
    //   10: aload_0
    //   11: getfield 20	com/truecaller/util/bn:b	Landroid/content/Context;
    //   14: invokevirtual 544	android/content/Context:getApplicationContext	()Landroid/content/Context;
    //   17: checkcast 546	com/truecaller/common/b/a
    //   20: astore_2
    //   21: aload_2
    //   22: invokevirtual 548	com/truecaller/common/b/a:o	()Z
    //   25: istore_3
    //   26: iload_3
    //   27: ifeq +18 -> 45
    //   30: aload_2
    //   31: invokevirtual 551	com/truecaller/common/b/a:H	()Ljava/lang/String;
    //   34: astore_2
    //   35: aload_2
    //   36: invokestatic 274	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   39: istore_1
    //   40: iload_1
    //   41: ifeq +4 -> 45
    //   44: return
    //   45: iconst_1
    //   46: anewarray 87	java/lang/String
    //   49: dup
    //   50: iconst_0
    //   51: ldc_w 553
    //   54: aastore
    //   55: astore_2
    //   56: new 555	com/truecaller/analytics/e$a
    //   59: astore 4
    //   61: aload 4
    //   63: ldc_w 557
    //   66: invokespecial 558	com/truecaller/analytics/e$a:<init>	(Ljava/lang/String;)V
    //   69: ldc_w 560
    //   72: astore 5
    //   74: ldc_w 562
    //   77: astore 6
    //   79: aload 6
    //   81: invokestatic 567	com/truecaller/old/data/access/Settings:e	(Ljava/lang/String;)Z
    //   84: istore 7
    //   86: iconst_1
    //   87: istore 8
    //   89: iload 7
    //   91: iload 8
    //   93: ixor
    //   94: istore 7
    //   96: aload 4
    //   98: aload 5
    //   100: iload 7
    //   102: invokevirtual 570	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Z)Lcom/truecaller/analytics/e$a;
    //   105: pop
    //   106: invokestatic 573	java/lang/System:currentTimeMillis	()J
    //   109: lstore 9
    //   111: aload_0
    //   112: getfield 27	com/truecaller/util/bn:a	Lcom/truecaller/data/access/m;
    //   115: astore 11
    //   117: aload 11
    //   119: invokevirtual 576	com/truecaller/data/access/m:a	()Landroid/database/Cursor;
    //   122: astore 11
    //   124: aconst_null
    //   125: astore 12
    //   127: getstatic 582	android/provider/ContactsContract$Data:CONTENT_URI	Landroid/net/Uri;
    //   130: astore 13
    //   132: ldc_w 584
    //   135: astore 14
    //   137: aload_0
    //   138: aload 11
    //   140: aload 13
    //   142: aload 14
    //   144: aload_2
    //   145: invokespecial 526	com/truecaller/util/bn:a	(Landroid/database/Cursor;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/truecaller/util/bn$b;
    //   148: astore_2
    //   149: ldc_w 562
    //   152: astore 13
    //   154: aload 13
    //   156: invokestatic 586	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;)Z
    //   159: istore 15
    //   161: iload 15
    //   163: ifne +48 -> 211
    //   166: ldc_w 562
    //   169: astore 13
    //   171: aload 13
    //   173: iload 8
    //   175: invokestatic 589	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   178: aload_0
    //   179: getfield 33	com/truecaller/util/bn:e	Lcom/truecaller/messaging/transport/im/j;
    //   182: astore 16
    //   184: aload 16
    //   186: invokeinterface 593 1 0
    //   191: istore 8
    //   193: iload 8
    //   195: ifeq +16 -> 211
    //   198: aload_0
    //   199: getfield 33	com/truecaller/util/bn:e	Lcom/truecaller/messaging/transport/im/j;
    //   202: astore 16
    //   204: aload 16
    //   206: invokeinterface 595 1 0
    //   211: ldc_w 597
    //   214: astore 16
    //   216: ldc_w 599
    //   219: astore 13
    //   221: aload 4
    //   223: aload 16
    //   225: aload 13
    //   227: invokevirtual 602	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   230: pop
    //   231: ldc_w 604
    //   234: astore 16
    //   236: aload_2
    //   237: getfield 605	com/truecaller/util/bn$b:c	I
    //   240: istore 15
    //   242: aload 4
    //   244: aload 16
    //   246: iload 15
    //   248: invokevirtual 608	com/truecaller/analytics/e$a:a	(Ljava/lang/String;I)Lcom/truecaller/analytics/e$a;
    //   251: pop
    //   252: ldc_w 610
    //   255: astore 16
    //   257: aload_2
    //   258: getfield 612	com/truecaller/util/bn$b:b	I
    //   261: istore_1
    //   262: aload 4
    //   264: aload 16
    //   266: iload_1
    //   267: invokevirtual 608	com/truecaller/analytics/e$a:a	(Ljava/lang/String;I)Lcom/truecaller/analytics/e$a;
    //   270: pop
    //   271: aload 11
    //   273: ifnull +10 -> 283
    //   276: aload 11
    //   278: invokeinterface 540 1 0
    //   283: invokestatic 573	java/lang/System:currentTimeMillis	()J
    //   286: lload 9
    //   288: lsub
    //   289: lstore 17
    //   291: lload 17
    //   293: l2d
    //   294: dstore 19
    //   296: dload 19
    //   298: invokestatic 617	java/lang/Double:valueOf	(D)Ljava/lang/Double;
    //   301: astore_2
    //   302: aload 4
    //   304: aload_2
    //   305: putfield 620	com/truecaller/analytics/e$a:a	Ljava/lang/Double;
    //   308: aload_0
    //   309: getfield 29	com/truecaller/util/bn:c	Lcom/truecaller/analytics/b;
    //   312: astore_2
    //   313: aload 4
    //   315: invokevirtual 623	com/truecaller/analytics/e$a:a	()Lcom/truecaller/analytics/e;
    //   318: astore 4
    //   320: aload_2
    //   321: aload 4
    //   323: invokeinterface 628 2 0
    //   328: return
    //   329: astore_2
    //   330: goto +9 -> 339
    //   333: astore_2
    //   334: aload_2
    //   335: astore 12
    //   337: aload_2
    //   338: athrow
    //   339: aload 11
    //   341: ifnull +37 -> 378
    //   344: aload 12
    //   346: ifnull +25 -> 371
    //   349: aload 11
    //   351: invokeinterface 540 1 0
    //   356: goto +22 -> 378
    //   359: astore 16
    //   361: aload 12
    //   363: aload 16
    //   365: invokevirtual 461	java/lang/Throwable:addSuppressed	(Ljava/lang/Throwable;)V
    //   368: goto +10 -> 378
    //   371: aload 11
    //   373: invokeinterface 540 1 0
    //   378: aload_2
    //   379: athrow
    //   380: astore_2
    //   381: goto +61 -> 442
    //   384: astore_2
    //   385: ldc_w 630
    //   388: astore 16
    //   390: aload_2
    //   391: aload 16
    //   393: invokestatic 635	com/truecaller/log/d:a	(Ljava/lang/Throwable;Ljava/lang/String;)V
    //   396: ldc_w 597
    //   399: astore_2
    //   400: ldc_w 637
    //   403: astore 16
    //   405: aload 4
    //   407: aload_2
    //   408: aload 16
    //   410: invokevirtual 602	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   413: pop
    //   414: invokestatic 573	java/lang/System:currentTimeMillis	()J
    //   417: lload 9
    //   419: lsub
    //   420: lstore 17
    //   422: lload 17
    //   424: l2d
    //   425: dstore 19
    //   427: dload 19
    //   429: invokestatic 617	java/lang/Double:valueOf	(D)Ljava/lang/Double;
    //   432: astore_2
    //   433: aload 4
    //   435: aload_2
    //   436: putfield 620	com/truecaller/analytics/e$a:a	Ljava/lang/Double;
    //   439: goto -131 -> 308
    //   442: invokestatic 573	java/lang/System:currentTimeMillis	()J
    //   445: lload 9
    //   447: lsub
    //   448: l2d
    //   449: invokestatic 617	java/lang/Double:valueOf	(D)Ljava/lang/Double;
    //   452: astore 5
    //   454: aload 4
    //   456: aload 5
    //   458: putfield 620	com/truecaller/analytics/e$a:a	Ljava/lang/Double;
    //   461: aload_0
    //   462: getfield 29	com/truecaller/util/bn:c	Lcom/truecaller/analytics/b;
    //   465: astore 5
    //   467: aload 4
    //   469: invokevirtual 623	com/truecaller/analytics/e$a:a	()Lcom/truecaller/analytics/e;
    //   472: astore 4
    //   474: aload 5
    //   476: aload 4
    //   478: invokeinterface 628 2 0
    //   483: aload_2
    //   484: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	485	0	this	bn
    //   4	37	1	bool1	boolean
    //   261	6	1	i	int
    //   20	301	2	localObject1	Object
    //   329	1	2	localObject2	Object
    //   333	46	2	localObject3	Object
    //   380	1	2	localObject4	Object
    //   384	7	2	localRuntimeException	RuntimeException
    //   399	85	2	localObject5	Object
    //   25	2	3	bool2	boolean
    //   59	418	4	localObject6	Object
    //   72	403	5	localObject7	Object
    //   77	3	6	str1	String
    //   84	17	7	bool3	boolean
    //   87	107	8	bool4	boolean
    //   109	337	9	l1	long
    //   115	257	11	localObject8	Object
    //   125	237	12	localObject9	Object
    //   130	96	13	localObject10	Object
    //   135	8	14	str2	String
    //   159	3	15	bool5	boolean
    //   240	7	15	j	int
    //   182	83	16	localObject11	Object
    //   359	5	16	localThrowable	Throwable
    //   388	21	16	str3	String
    //   289	134	17	l2	long
    //   294	134	19	d1	double
    // Exception table:
    //   from	to	target	type
    //   337	339	329	finally
    //   127	130	333	finally
    //   144	148	333	finally
    //   154	159	333	finally
    //   173	178	333	finally
    //   178	182	333	finally
    //   184	191	333	finally
    //   198	202	333	finally
    //   204	211	333	finally
    //   225	231	333	finally
    //   236	240	333	finally
    //   246	252	333	finally
    //   257	261	333	finally
    //   266	271	333	finally
    //   349	356	359	finally
    //   111	115	380	finally
    //   117	122	380	finally
    //   276	283	380	finally
    //   363	368	380	finally
    //   371	378	380	finally
    //   378	380	380	finally
    //   391	396	380	finally
    //   408	414	380	finally
    //   111	115	384	java/lang/RuntimeException
    //   117	122	384	java/lang/RuntimeException
    //   276	283	384	java/lang/RuntimeException
    //   363	368	384	java/lang/RuntimeException
    //   371	378	384	java/lang/RuntimeException
    //   378	380	384	java/lang/RuntimeException
  }
  
  final boolean b()
  {
    l locall = d;
    String[] arrayOfString = { "android.permission.READ_CONTACTS" };
    return locall.a(arrayOfString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bn
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */