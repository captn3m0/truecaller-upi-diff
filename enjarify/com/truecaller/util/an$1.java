package com.truecaller.util;

import android.content.Context;
import android.media.ToneGenerator;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Vibrator;
import com.truecaller.log.d;

final class an$1
  extends HandlerThread
{
  an$1(an paraman, String paramString)
  {
    super(paramString);
  }
  
  protected final void onLooperPrepared()
  {
    super.onLooperPrepared();
    an localan = a;
    an.1.1 local1 = new com/truecaller/util/an$1$1;
    Looper localLooper = getLooper();
    local1.<init>(this, localLooper);
    an.a(localan, local1);
  }
  
  public final void run()
  {
    Object localObject2;
    try
    {
      an localan1 = a;
      localObject2 = new android/media/ToneGenerator;
      int i = 8;
      int j = 70;
      ((ToneGenerator)localObject2).<init>(i, j);
      an.a(localan1, (ToneGenerator)localObject2);
    }
    catch (Exception localException1)
    {
      localObject2 = "Could not create tone generator";
      d.a(localException1, (String)localObject2);
    }
    try
    {
      an localan2 = a;
      localObject2 = a;
      localObject2 = an.a((an)localObject2);
      String str = "vibrator";
      localObject2 = ((Context)localObject2).getSystemService(str);
      localObject2 = (Vibrator)localObject2;
      an.a(localan2, (Vibrator)localObject2);
    }
    catch (Exception localException2)
    {
      localObject2 = "Could not create vibrator";
      d.a(localException2, (String)localObject2);
    }
    super.run();
    Object localObject1 = an.b(a);
    if (localObject1 != null)
    {
      an.b(a).stopTone();
      an.b(a).release();
      localObject1 = a;
      localObject2 = null;
      an.a((an)localObject1, null);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.an.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */