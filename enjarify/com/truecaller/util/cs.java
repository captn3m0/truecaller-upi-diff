package com.truecaller.util;

import dagger.a.d;
import javax.inject.Provider;

public final class cs
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private cs(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static cs a(Provider paramProvider1, Provider paramProvider2)
  {
    cs localcs = new com/truecaller/util/cs;
    localcs.<init>(paramProvider1, paramProvider2);
    return localcs;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.cs
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */