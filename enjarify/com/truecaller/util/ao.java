package com.truecaller.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.StatFs;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.log.d;
import java.io.File;

public final class ao
{
  private static final Object a;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    a = localObject;
  }
  
  private static long a(StatFs paramStatFs)
  {
    try
    {
      return paramStatFs.getAvailableBytes();
    }
    catch (NoSuchMethodError localNoSuchMethodError)
    {
      long l1 = paramStatFs.getAvailableBlocks();
      long l2 = paramStatFs.getBlockSize();
      return l1 * l2;
    }
  }
  
  public static long a(String paramString)
  {
    long l1 = 0L;
    try
    {
      StatFs localStatFs = new android/os/StatFs;
      localStatFs.<init>(paramString);
      long l2 = a(localStatFs);
      paramString = null;
      boolean bool1 = l2 < l1;
      boolean bool2;
      if (!bool1) {
        bool2 = true;
      } else {
        bool2 = false;
      }
      paramString = new String[0];
      AssertionUtil.OnlyInDebug.isTrue(bool2, paramString);
      return l2;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      paramString = String.valueOf(paramString);
      paramString = "Failed to get available space for path ".concat(paramString);
      d.a(localIllegalArgumentException, paramString);
    }
    return l1;
  }
  
  /* Error */
  public static void a(Context paramContext)
  {
    // Byte code:
    //   0: getstatic 12	com/truecaller/util/ao:a	Ljava/lang/Object;
    //   3: astore_1
    //   4: aload_1
    //   5: monitorenter
    //   6: ldc 61
    //   8: astore_2
    //   9: iconst_1
    //   10: anewarray 36	java/lang/String
    //   13: iconst_0
    //   14: aload_2
    //   15: aastore
    //   16: aload_0
    //   17: invokestatic 67	com/truecaller/util/ap:b	(Landroid/content/Context;)J
    //   20: lstore_3
    //   21: aload_0
    //   22: invokevirtual 73	android/content/Context:getCacheDir	()Ljava/io/File;
    //   25: astore_2
    //   26: new 75	java/io/File
    //   29: astore 5
    //   31: ldc 77
    //   33: astore 6
    //   35: aload 5
    //   37: aload_2
    //   38: aload 6
    //   40: invokespecial 80	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   43: aload 5
    //   45: invokevirtual 84	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   48: astore 5
    //   50: aload 5
    //   52: invokestatic 87	com/truecaller/util/ao:b	(Ljava/lang/String;)J
    //   55: lstore 7
    //   57: lconst_0
    //   58: lstore 9
    //   60: lload 7
    //   62: lload 9
    //   64: ladd
    //   65: lstore 7
    //   67: new 75	java/io/File
    //   70: astore 11
    //   72: ldc 89
    //   74: astore 12
    //   76: aload 11
    //   78: aload_2
    //   79: aload 12
    //   81: invokespecial 80	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   84: aload 11
    //   86: invokevirtual 84	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   89: astore 11
    //   91: aload 11
    //   93: invokestatic 87	com/truecaller/util/ao:b	(Ljava/lang/String;)J
    //   96: lstore 13
    //   98: lload 7
    //   100: lload 13
    //   102: ladd
    //   103: lstore 7
    //   105: new 75	java/io/File
    //   108: astore 11
    //   110: ldc 91
    //   112: astore 12
    //   114: aload 11
    //   116: aload_2
    //   117: aload 12
    //   119: invokespecial 80	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   122: aload 11
    //   124: invokevirtual 84	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   127: astore_2
    //   128: aload_2
    //   129: invokestatic 87	com/truecaller/util/ao:b	(Ljava/lang/String;)J
    //   132: lstore 13
    //   134: lload 7
    //   136: lload 13
    //   138: ladd
    //   139: lstore 7
    //   141: aload_0
    //   142: invokevirtual 94	android/content/Context:getFilesDir	()Ljava/io/File;
    //   145: astore_2
    //   146: aload_2
    //   147: invokevirtual 84	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   150: astore_2
    //   151: aload_2
    //   152: invokestatic 96	com/truecaller/util/ao:a	(Ljava/lang/String;)J
    //   155: lstore 13
    //   157: iconst_1
    //   158: istore 15
    //   160: ldc 34
    //   162: fstore 16
    //   164: aload_0
    //   165: invokestatic 97	com/truecaller/util/ao:b	(Landroid/content/Context;)J
    //   168: lstore 17
    //   170: lload_3
    //   171: lload 7
    //   173: ladd
    //   174: lstore 19
    //   176: lload 19
    //   178: lload 17
    //   180: lcmp
    //   181: istore 21
    //   183: iload 21
    //   185: ifgt +13 -> 198
    //   188: iconst_1
    //   189: istore 22
    //   191: ldc 34
    //   193: fstore 23
    //   195: goto +12 -> 207
    //   198: iconst_0
    //   199: istore 22
    //   201: fconst_0
    //   202: fstore 23
    //   204: aconst_null
    //   205: astore 24
    //   207: iload 15
    //   209: anewarray 36	java/lang/String
    //   212: astore 25
    //   214: new 99	java/lang/StringBuilder
    //   217: astore 26
    //   219: ldc 101
    //   221: astore_2
    //   222: aload 26
    //   224: aload_2
    //   225: invokespecial 102	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   228: aload 26
    //   230: lload_3
    //   231: invokevirtual 106	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   234: pop
    //   235: ldc 108
    //   237: astore_2
    //   238: aload 26
    //   240: aload_2
    //   241: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   244: pop
    //   245: aload 26
    //   247: lload 7
    //   249: invokevirtual 106	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   252: pop
    //   253: ldc 113
    //   255: astore_2
    //   256: aload 26
    //   258: aload_2
    //   259: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   262: pop
    //   263: aload 26
    //   265: lload 17
    //   267: invokevirtual 106	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   270: pop
    //   271: aload 26
    //   273: invokevirtual 116	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   276: astore_2
    //   277: aload 25
    //   279: iconst_0
    //   280: aload_2
    //   281: aastore
    //   282: iload 22
    //   284: aload 25
    //   286: invokestatic 42	com/truecaller/log/AssertionUtil$OnlyInDebug:isTrue	(Z[Ljava/lang/String;)V
    //   289: goto +18 -> 307
    //   292: pop
    //   293: lload 9
    //   295: lstore 17
    //   297: iconst_0
    //   298: anewarray 36	java/lang/String
    //   301: astore_2
    //   302: iconst_0
    //   303: aload_2
    //   304: invokestatic 42	com/truecaller/log/AssertionUtil$OnlyInDebug:isTrue	(Z[Ljava/lang/String;)V
    //   307: lload 13
    //   309: lload 9
    //   311: lcmp
    //   312: istore 15
    //   314: iload 15
    //   316: iflt +13 -> 329
    //   319: iconst_1
    //   320: istore 15
    //   322: ldc 34
    //   324: fstore 16
    //   326: goto +11 -> 337
    //   329: iconst_0
    //   330: istore 15
    //   332: fconst_0
    //   333: fstore 16
    //   335: aconst_null
    //   336: astore_2
    //   337: iconst_0
    //   338: anewarray 36	java/lang/String
    //   341: astore 24
    //   343: iload 15
    //   345: aload 24
    //   347: invokestatic 42	com/truecaller/log/AssertionUtil$OnlyInDebug:isTrue	(Z[Ljava/lang/String;)V
    //   350: lload 17
    //   352: lload 9
    //   354: lcmp
    //   355: istore 15
    //   357: iload 15
    //   359: iflt +13 -> 372
    //   362: iconst_1
    //   363: istore 15
    //   365: ldc 34
    //   367: fstore 16
    //   369: goto +11 -> 380
    //   372: iconst_0
    //   373: istore 15
    //   375: fconst_0
    //   376: fstore 16
    //   378: aconst_null
    //   379: astore_2
    //   380: iconst_0
    //   381: anewarray 36	java/lang/String
    //   384: astore 24
    //   386: iload 15
    //   388: aload 24
    //   390: invokestatic 42	com/truecaller/log/AssertionUtil$OnlyInDebug:isTrue	(Z[Ljava/lang/String;)V
    //   393: lload 13
    //   395: lload 17
    //   397: ladd
    //   398: l2f
    //   399: ldc 118
    //   401: fsub
    //   402: fstore 16
    //   404: ldc 119
    //   406: istore 22
    //   408: ldc 120
    //   410: fstore 23
    //   412: fload 16
    //   414: fload 23
    //   416: fmul
    //   417: fstore 16
    //   419: fload 16
    //   421: f2l
    //   422: lstore 19
    //   424: lload 17
    //   426: lstore 27
    //   428: ldc2_w 121
    //   431: lstore 29
    //   433: lload 29
    //   435: lload 19
    //   437: invokestatic 130	java/lang/Math:min	(JJ)J
    //   440: lstore 29
    //   442: lload 29
    //   444: lload 9
    //   446: invokestatic 133	java/lang/Math:max	(JJ)J
    //   449: lstore 29
    //   451: lload 29
    //   453: lload 9
    //   455: lcmp
    //   456: istore 31
    //   458: iload 31
    //   460: iflt +17 -> 477
    //   463: iconst_0
    //   464: istore 15
    //   466: fconst_0
    //   467: fstore 16
    //   469: aconst_null
    //   470: astore_2
    //   471: iconst_1
    //   472: istore 31
    //   474: goto +14 -> 488
    //   477: iconst_0
    //   478: istore 15
    //   480: fconst_0
    //   481: fstore 16
    //   483: aconst_null
    //   484: astore_2
    //   485: iconst_0
    //   486: istore 31
    //   488: iconst_0
    //   489: anewarray 36	java/lang/String
    //   492: astore 24
    //   494: iload 31
    //   496: aload 24
    //   498: invokestatic 42	com/truecaller/log/AssertionUtil$OnlyInDebug:isTrue	(Z[Ljava/lang/String;)V
    //   501: lload 27
    //   503: lload 29
    //   505: lsub
    //   506: lstore 32
    //   508: lload 32
    //   510: lload 9
    //   512: lcmp
    //   513: istore 34
    //   515: iload 34
    //   517: ifle +37 -> 554
    //   520: ldc2_w 134
    //   523: lstore 35
    //   525: lload 7
    //   527: lload 35
    //   529: lcmp
    //   530: istore 37
    //   532: iload 37
    //   534: ifle +20 -> 554
    //   537: lload 32
    //   539: lload 7
    //   541: lsub
    //   542: lstore 35
    //   544: lload 9
    //   546: lstore 38
    //   548: iconst_1
    //   549: istore 40
    //   551: goto +14 -> 565
    //   554: lload 7
    //   556: lstore 38
    //   558: lload 32
    //   560: lstore 35
    //   562: iconst_0
    //   563: istore 40
    //   565: lload 35
    //   567: lload 9
    //   569: lcmp
    //   570: istore 41
    //   572: iload 41
    //   574: ifle +112 -> 686
    //   577: ldc2_w 138
    //   580: lstore 42
    //   582: lload_3
    //   583: lload 42
    //   585: lcmp
    //   586: istore 44
    //   588: iload 44
    //   590: ifle +96 -> 686
    //   593: aload_1
    //   594: astore 45
    //   596: lload_3
    //   597: lload 35
    //   599: lsub
    //   600: lstore 46
    //   602: lload 42
    //   604: lload 46
    //   606: invokestatic 133	java/lang/Math:max	(JJ)J
    //   609: lstore 48
    //   611: lload 48
    //   613: lload_3
    //   614: lcmp
    //   615: istore 15
    //   617: iload 15
    //   619: ifgt +17 -> 636
    //   622: iconst_0
    //   623: istore 15
    //   625: fconst_0
    //   626: fstore 16
    //   628: aconst_null
    //   629: astore_2
    //   630: iconst_1
    //   631: istore 50
    //   633: goto +16 -> 649
    //   636: iconst_0
    //   637: istore 15
    //   639: fconst_0
    //   640: fstore 16
    //   642: aconst_null
    //   643: astore_2
    //   644: iconst_0
    //   645: istore 50
    //   647: aconst_null
    //   648: astore_1
    //   649: iconst_0
    //   650: anewarray 36	java/lang/String
    //   653: astore 51
    //   655: iload 50
    //   657: aload 51
    //   659: invokestatic 42	com/truecaller/log/AssertionUtil$OnlyInDebug:isTrue	(Z[Ljava/lang/String;)V
    //   662: lload 48
    //   664: lload 42
    //   666: lsub
    //   667: lstore 9
    //   669: lload 35
    //   671: lload 9
    //   673: lsub
    //   674: lstore 35
    //   676: lload 48
    //   678: lstore 9
    //   680: lconst_0
    //   681: lstore 42
    //   683: goto +16 -> 699
    //   686: aload_1
    //   687: astore 45
    //   689: lload_3
    //   690: lstore 48
    //   692: iconst_m1
    //   693: i2l
    //   694: lstore 9
    //   696: lconst_0
    //   697: lstore 42
    //   699: lload 35
    //   701: lload 42
    //   703: lcmp
    //   704: istore 50
    //   706: iload 50
    //   708: ifle +25 -> 733
    //   711: iload 40
    //   713: ifne +20 -> 733
    //   716: lload 35
    //   718: lload 38
    //   720: lsub
    //   721: lstore 35
    //   723: lload 42
    //   725: lstore 38
    //   727: iconst_1
    //   728: istore 50
    //   730: goto +7 -> 737
    //   733: iload 40
    //   735: istore 50
    //   737: lload 35
    //   739: lload 42
    //   741: lcmp
    //   742: istore 40
    //   744: iload 40
    //   746: ifle +100 -> 846
    //   749: lload 48
    //   751: lload 35
    //   753: lsub
    //   754: lstore 9
    //   756: lload 42
    //   758: lload 9
    //   760: invokestatic 133	java/lang/Math:max	(JJ)J
    //   763: lstore 9
    //   765: lload 9
    //   767: lload 48
    //   769: lcmp
    //   770: istore 52
    //   772: iload 52
    //   774: ifgt +17 -> 791
    //   777: iconst_0
    //   778: istore 15
    //   780: fconst_0
    //   781: fstore 16
    //   783: aconst_null
    //   784: astore_2
    //   785: iconst_1
    //   786: istore 52
    //   788: goto +17 -> 805
    //   791: iconst_0
    //   792: istore 15
    //   794: fconst_0
    //   795: fstore 16
    //   797: aconst_null
    //   798: astore_2
    //   799: iconst_0
    //   800: istore 52
    //   802: aconst_null
    //   803: astore 25
    //   805: iconst_0
    //   806: anewarray 36	java/lang/String
    //   809: astore 26
    //   811: iload 52
    //   813: aload 26
    //   815: invokestatic 42	com/truecaller/log/AssertionUtil$OnlyInDebug:isTrue	(Z[Ljava/lang/String;)V
    //   818: lload 48
    //   820: lload 9
    //   822: lsub
    //   823: lstore 48
    //   825: lload 35
    //   827: lload 48
    //   829: lsub
    //   830: lstore 35
    //   832: lload 9
    //   834: lstore 48
    //   836: lload 35
    //   838: lstore 42
    //   840: lconst_0
    //   841: lstore 35
    //   843: goto +15 -> 858
    //   846: lload 42
    //   848: lstore 53
    //   850: lload 35
    //   852: lstore 42
    //   854: lload 53
    //   856: lstore 35
    //   858: lload 9
    //   860: lload 35
    //   862: lcmp
    //   863: istore 40
    //   865: iload 40
    //   867: ifge +101 -> 968
    //   870: lload 42
    //   872: lload 35
    //   874: lcmp
    //   875: istore 55
    //   877: iload 55
    //   879: ifgt +13 -> 892
    //   882: iconst_1
    //   883: istore 15
    //   885: ldc 34
    //   887: fstore 16
    //   889: goto +11 -> 900
    //   892: iconst_0
    //   893: istore 15
    //   895: fconst_0
    //   896: fstore 16
    //   898: aconst_null
    //   899: astore_2
    //   900: iconst_0
    //   901: istore 55
    //   903: aconst_null
    //   904: astore 51
    //   906: iconst_0
    //   907: anewarray 36	java/lang/String
    //   910: astore 56
    //   912: iload 15
    //   914: aload 56
    //   916: invokestatic 42	com/truecaller/log/AssertionUtil$OnlyInDebug:isTrue	(Z[Ljava/lang/String;)V
    //   919: lload 48
    //   921: lload_3
    //   922: lcmp
    //   923: istore 15
    //   925: iload 15
    //   927: ifne +9 -> 936
    //   930: iconst_1
    //   931: istore 57
    //   933: goto +9 -> 942
    //   936: iconst_0
    //   937: istore 57
    //   939: aconst_null
    //   940: astore 56
    //   942: iconst_0
    //   943: anewarray 36	java/lang/String
    //   946: astore_2
    //   947: aload_2
    //   948: astore 51
    //   950: iload 57
    //   952: aload_2
    //   953: invokestatic 42	com/truecaller/log/AssertionUtil$OnlyInDebug:isTrue	(Z[Ljava/lang/String;)V
    //   956: lload 42
    //   958: lneg
    //   959: lstore 9
    //   961: lload 48
    //   963: lload 9
    //   965: ladd
    //   966: lstore 9
    //   968: iload 50
    //   970: ifeq +38 -> 1008
    //   973: lconst_0
    //   974: lstore 42
    //   976: lload 38
    //   978: lload 42
    //   980: lcmp
    //   981: istore 34
    //   983: iload 34
    //   985: ifne +6 -> 991
    //   988: goto +20 -> 1008
    //   991: iconst_0
    //   992: istore 15
    //   994: fconst_0
    //   995: fstore 16
    //   997: aconst_null
    //   998: astore_2
    //   999: iconst_0
    //   1000: istore 52
    //   1002: aconst_null
    //   1003: astore 25
    //   1005: goto +14 -> 1019
    //   1008: iconst_0
    //   1009: istore 15
    //   1011: fconst_0
    //   1012: fstore 16
    //   1014: aconst_null
    //   1015: astore_2
    //   1016: iconst_1
    //   1017: istore 52
    //   1019: iconst_0
    //   1020: anewarray 36	java/lang/String
    //   1023: astore 26
    //   1025: iload 52
    //   1027: aload 26
    //   1029: invokestatic 42	com/truecaller/log/AssertionUtil$OnlyInDebug:isTrue	(Z[Ljava/lang/String;)V
    //   1032: new 143	com/truecaller/util/a/a
    //   1035: astore 25
    //   1037: aload 25
    //   1039: iload 50
    //   1041: lload 9
    //   1043: invokespecial 146	com/truecaller/util/a/a:<init>	(ZJ)V
    //   1046: iconst_1
    //   1047: istore 50
    //   1049: iload 50
    //   1051: anewarray 36	java/lang/String
    //   1054: astore_1
    //   1055: new 99	java/lang/StringBuilder
    //   1058: astore 51
    //   1060: ldc -108
    //   1062: astore 56
    //   1064: aload 51
    //   1066: aload 56
    //   1068: invokespecial 102	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1071: aload 51
    //   1073: lload 13
    //   1075: invokevirtual 106	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   1078: pop
    //   1079: ldc -106
    //   1081: astore 56
    //   1083: aload 51
    //   1085: aload 56
    //   1087: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1090: pop
    //   1091: aload 51
    //   1093: lload 27
    //   1095: invokevirtual 106	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   1098: pop
    //   1099: ldc -104
    //   1101: astore 56
    //   1103: aload 51
    //   1105: aload 56
    //   1107: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1110: pop
    //   1111: aload 51
    //   1113: lload 29
    //   1115: invokevirtual 106	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   1118: pop
    //   1119: ldc -102
    //   1121: astore 56
    //   1123: aload 51
    //   1125: aload 56
    //   1127: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1130: pop
    //   1131: aload 51
    //   1133: lload 32
    //   1135: invokevirtual 106	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   1138: pop
    //   1139: ldc -100
    //   1141: astore 56
    //   1143: aload 51
    //   1145: aload 56
    //   1147: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1150: pop
    //   1151: aload 51
    //   1153: lload_3
    //   1154: invokevirtual 106	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   1157: pop
    //   1158: ldc -98
    //   1160: astore 58
    //   1162: aload 51
    //   1164: aload 58
    //   1166: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1169: pop
    //   1170: aload 51
    //   1172: lload 7
    //   1174: invokevirtual 106	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   1177: pop
    //   1178: ldc -96
    //   1180: astore 58
    //   1182: aload 51
    //   1184: aload 58
    //   1186: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1189: pop
    //   1190: aload 25
    //   1192: getfield 163	com/truecaller/util/a/a:a	Z
    //   1195: istore 59
    //   1197: aload 51
    //   1199: iload 59
    //   1201: invokevirtual 166	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
    //   1204: pop
    //   1205: ldc -88
    //   1207: astore 58
    //   1209: aload 51
    //   1211: aload 58
    //   1213: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1216: pop
    //   1217: aload 25
    //   1219: getfield 171	com/truecaller/util/a/a:b	J
    //   1222: lstore_3
    //   1223: aload 51
    //   1225: lload_3
    //   1226: invokevirtual 106	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   1229: pop
    //   1230: aload 51
    //   1232: invokevirtual 116	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1235: astore 58
    //   1237: iconst_0
    //   1238: istore 15
    //   1240: fconst_0
    //   1241: fstore 16
    //   1243: aconst_null
    //   1244: astore_2
    //   1245: aload_1
    //   1246: iconst_0
    //   1247: aload 58
    //   1249: aastore
    //   1250: aload 25
    //   1252: getfield 163	com/truecaller/util/a/a:a	Z
    //   1255: istore 15
    //   1257: iload 15
    //   1259: ifeq +68 -> 1327
    //   1262: aload_0
    //   1263: invokevirtual 73	android/content/Context:getCacheDir	()Ljava/io/File;
    //   1266: astore_2
    //   1267: new 75	java/io/File
    //   1270: astore_1
    //   1271: ldc 77
    //   1273: astore 58
    //   1275: aload_1
    //   1276: aload_2
    //   1277: aload 58
    //   1279: invokespecial 80	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   1282: aload_1
    //   1283: invokestatic 174	com/truecaller/util/ap:a	(Ljava/io/File;)Z
    //   1286: pop
    //   1287: new 75	java/io/File
    //   1290: astore_1
    //   1291: ldc 89
    //   1293: astore 58
    //   1295: aload_1
    //   1296: aload_2
    //   1297: aload 58
    //   1299: invokespecial 80	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   1302: aload_1
    //   1303: invokestatic 174	com/truecaller/util/ap:a	(Ljava/io/File;)Z
    //   1306: pop
    //   1307: new 75	java/io/File
    //   1310: astore_1
    //   1311: ldc 91
    //   1313: astore 58
    //   1315: aload_1
    //   1316: aload_2
    //   1317: aload 58
    //   1319: invokespecial 80	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   1322: aload_1
    //   1323: invokestatic 174	com/truecaller/util/ap:a	(Ljava/io/File;)Z
    //   1326: pop
    //   1327: aload 25
    //   1329: getfield 171	com/truecaller/util/a/a:b	J
    //   1332: lstore 46
    //   1334: aload_0
    //   1335: astore 58
    //   1337: aload_0
    //   1338: lload 46
    //   1340: invokestatic 177	com/truecaller/util/ap:a	(Landroid/content/Context;J)V
    //   1343: aload 45
    //   1345: monitorexit
    //   1346: return
    //   1347: astore_2
    //   1348: aload_1
    //   1349: astore 45
    //   1351: aload 45
    //   1353: monitorexit
    //   1354: aload_2
    //   1355: athrow
    //   1356: astore_2
    //   1357: goto -6 -> 1351
    //   1360: pop
    //   1361: goto -1064 -> 297
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1364	0	paramContext	Context
    //   3	1346	1	localObject1	Object
    //   8	1309	2	localObject2	Object
    //   1347	8	2	localObject3	Object
    //   1356	1	2	localObject4	Object
    //   20	1206	3	l1	long
    //   29	22	5	localObject5	Object
    //   33	6	6	str1	String
    //   55	1118	7	l2	long
    //   58	984	9	l3	long
    //   70	53	11	localObject6	Object
    //   74	44	12	str2	String
    //   96	978	13	l4	long
    //   158	1100	15	bool1	boolean
    //   162	1080	16	f1	float
    //   168	257	17	l5	long
    //   174	262	19	l6	long
    //   181	3	21	bool2	boolean
    //   189	94	22	bool3	boolean
    //   406	1	22	i	int
    //   193	222	23	f2	float
    //   205	292	24	arrayOfString	String[]
    //   212	1116	25	localObject7	Object
    //   217	811	26	localObject8	Object
    //   426	668	27	l7	long
    //   431	683	29	l8	long
    //   456	39	31	bool4	boolean
    //   506	628	32	l9	long
    //   513	471	34	bool5	boolean
    //   523	350	35	l10	long
    //   530	3	37	bool6	boolean
    //   546	431	38	l11	long
    //   549	317	40	bool7	boolean
    //   570	3	41	bool8	boolean
    //   580	399	42	l12	long
    //   586	3	44	bool9	boolean
    //   594	758	45	localObject9	Object
    //   600	739	46	l13	long
    //   292	1	48	localNameNotFoundException1	android.content.pm.PackageManager.NameNotFoundException
    //   609	353	48	l14	long
    //   1360	1	49	localNameNotFoundException2	android.content.pm.PackageManager.NameNotFoundException
    //   631	419	50	bool10	boolean
    //   653	578	51	localObject10	Object
    //   770	256	52	bool11	boolean
    //   848	7	53	l15	long
    //   875	27	55	bool12	boolean
    //   910	236	56	localObject11	Object
    //   931	20	57	bool13	boolean
    //   1160	176	58	localObject12	Object
    //   1195	5	59	bool14	boolean
    // Exception table:
    //   from	to	target	type
    //   164	168	292	android/content/pm/PackageManager$NameNotFoundException
    //   9	16	1347	finally
    //   16	20	1347	finally
    //   21	25	1347	finally
    //   26	29	1347	finally
    //   38	43	1347	finally
    //   43	48	1347	finally
    //   50	55	1347	finally
    //   67	70	1347	finally
    //   79	84	1347	finally
    //   84	89	1347	finally
    //   91	96	1347	finally
    //   105	108	1347	finally
    //   117	122	1347	finally
    //   122	127	1347	finally
    //   128	132	1347	finally
    //   141	145	1347	finally
    //   146	150	1347	finally
    //   151	155	1347	finally
    //   164	168	1347	finally
    //   207	212	1347	finally
    //   214	217	1347	finally
    //   224	228	1347	finally
    //   230	235	1347	finally
    //   240	245	1347	finally
    //   247	253	1347	finally
    //   258	263	1347	finally
    //   265	271	1347	finally
    //   271	276	1347	finally
    //   280	282	1347	finally
    //   284	289	1347	finally
    //   297	301	1347	finally
    //   303	307	1347	finally
    //   337	341	1347	finally
    //   345	350	1347	finally
    //   380	384	1347	finally
    //   388	393	1347	finally
    //   435	440	1347	finally
    //   444	449	1347	finally
    //   488	492	1347	finally
    //   496	501	1347	finally
    //   604	609	1356	finally
    //   649	653	1356	finally
    //   657	662	1356	finally
    //   758	763	1356	finally
    //   805	809	1356	finally
    //   813	818	1356	finally
    //   906	910	1356	finally
    //   914	919	1356	finally
    //   942	946	1356	finally
    //   952	956	1356	finally
    //   1019	1023	1356	finally
    //   1027	1032	1356	finally
    //   1032	1035	1356	finally
    //   1041	1046	1356	finally
    //   1049	1054	1356	finally
    //   1055	1058	1356	finally
    //   1066	1071	1356	finally
    //   1073	1079	1356	finally
    //   1085	1091	1356	finally
    //   1093	1099	1356	finally
    //   1105	1111	1356	finally
    //   1113	1119	1356	finally
    //   1125	1131	1356	finally
    //   1133	1139	1356	finally
    //   1145	1151	1356	finally
    //   1153	1158	1356	finally
    //   1164	1170	1356	finally
    //   1172	1178	1356	finally
    //   1184	1190	1356	finally
    //   1190	1195	1356	finally
    //   1199	1205	1356	finally
    //   1211	1217	1356	finally
    //   1217	1222	1356	finally
    //   1225	1230	1356	finally
    //   1230	1235	1356	finally
    //   1247	1250	1356	finally
    //   1250	1255	1356	finally
    //   1262	1266	1356	finally
    //   1267	1270	1356	finally
    //   1277	1282	1356	finally
    //   1282	1287	1356	finally
    //   1287	1290	1356	finally
    //   1297	1302	1356	finally
    //   1302	1307	1356	finally
    //   1307	1310	1356	finally
    //   1317	1322	1356	finally
    //   1322	1327	1356	finally
    //   1327	1332	1356	finally
    //   1338	1343	1356	finally
    //   1343	1346	1356	finally
    //   1351	1354	1356	finally
    //   207	212	1360	android/content/pm/PackageManager$NameNotFoundException
    //   214	217	1360	android/content/pm/PackageManager$NameNotFoundException
    //   224	228	1360	android/content/pm/PackageManager$NameNotFoundException
    //   230	235	1360	android/content/pm/PackageManager$NameNotFoundException
    //   240	245	1360	android/content/pm/PackageManager$NameNotFoundException
    //   247	253	1360	android/content/pm/PackageManager$NameNotFoundException
    //   258	263	1360	android/content/pm/PackageManager$NameNotFoundException
    //   265	271	1360	android/content/pm/PackageManager$NameNotFoundException
    //   271	276	1360	android/content/pm/PackageManager$NameNotFoundException
    //   280	282	1360	android/content/pm/PackageManager$NameNotFoundException
    //   284	289	1360	android/content/pm/PackageManager$NameNotFoundException
  }
  
  private static long b(Context paramContext)
  {
    PackageManager localPackageManager = paramContext.getPackageManager();
    paramContext = paramContext.getPackageName();
    return b(getApplicationInfo0dataDir);
  }
  
  private static long b(String paramString)
  {
    File localFile = new java/io/File;
    localFile.<init>(paramString);
    boolean bool1 = localFile.exists();
    if (bool1)
    {
      bool1 = localFile.isDirectory();
      if (!bool1)
      {
        bool1 = false;
        paramString = null;
        break label39;
      }
    }
    bool1 = true;
    label39:
    String[] arrayOfString1 = new String[0];
    AssertionUtil.OnlyInDebug.isTrue(bool1, arrayOfString1);
    paramString = localFile.listFiles();
    long l1 = 0L;
    if (paramString != null)
    {
      int i = paramString.length;
      long l2 = l1;
      int j = 0;
      while (j < i)
      {
        String str = paramString[j];
        boolean bool2 = str.isDirectory();
        long l3;
        if (bool2)
        {
          str = str.getAbsolutePath();
          l3 = b(str);
        }
        else
        {
          l3 = str.length();
        }
        boolean bool3 = l3 < l1;
        if (!bool3) {
          bool3 = true;
        } else {
          bool3 = false;
        }
        String[] arrayOfString2 = new String[0];
        AssertionUtil.OnlyInDebug.isTrue(bool3, arrayOfString2);
        l2 += l3;
        j += 1;
      }
      l1 = l2;
    }
    return l1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ao
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */