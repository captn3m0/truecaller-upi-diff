package com.truecaller.util.e;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.SparseArray;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.p;
import com.google.gson.l;
import com.google.gson.o;
import com.truecaller.common.h.am;
import com.truecaller.old.a.a;
import com.truecaller.old.a.c;
import com.truecaller.util.ay;
import org.json.JSONObject;

final class b$b
  extends a
{
  private final b a;
  private final d c;
  private boolean d = true;
  
  private b$b(b paramb, c paramc, d paramd)
  {
    super(paramc, false, null);
    a = paramb;
    c = paramd;
  }
  
  private static void a(SparseArray paramSparseArray, o paramo, int paramInt, String paramString)
  {
    boolean bool1 = paramo.a(paramString);
    if (bool1)
    {
      paramo = ay.a(paramString, paramo);
      boolean bool2 = am.a(paramo);
      if (bool2) {
        paramSparseArray.put(paramInt, paramo);
      }
    }
  }
  
  public final void a(Object paramObject)
  {
    boolean bool = d;
    if (bool)
    {
      d locald = c;
      paramObject = (SparseArray)paramObject;
      b.a(locald, paramObject);
      return;
    }
    paramObject = c;
    if (paramObject != null) {
      ((d)paramObject).a();
    }
  }
  
  protected final Object doInBackground(Object... paramVarArgs)
  {
    paramVarArgs = new android/util/SparseArray;
    paramVarArgs.<init>();
    Object localObject1 = new android/os/Bundle;
    ((Bundle)localObject1).<init>();
    ((Bundle)localObject1).putString("fields", "id,first_name,last_name,email,location,gender,picture.width(500).height(500)");
    Object localObject2 = new com/facebook/GraphRequest;
    Object localObject3 = AccessToken.a();
    String str = "me";
    com.facebook.q localq = com.facebook.q.a;
    ((GraphRequest)localObject2).<init>((AccessToken)localObject3, str, (Bundle)localObject1, localq);
    localObject1 = GraphRequest.a((GraphRequest)localObject2);
    try
    {
      localObject1 = a;
      localObject1 = ((JSONObject)localObject1).toString();
      localObject1 = com.google.gson.q.a((String)localObject1);
      localObject1 = ((l)localObject1).i();
      int i = 2131363094;
      localObject3 = "first_name";
      a(paramVarArgs, (o)localObject1, i, (String)localObject3);
      i = 2131363586;
      localObject3 = "last_name";
      a(paramVarArgs, (o)localObject1, i, (String)localObject3);
      i = 2131363026;
      localObject3 = "id";
      a(paramVarArgs, (o)localObject1, i, (String)localObject3);
      i = 2131362940;
      localObject3 = "email";
      a(paramVarArgs, (o)localObject1, i, (String)localObject3);
      localObject2 = "location";
      boolean bool1 = ((o)localObject1).a((String)localObject2);
      if (bool1)
      {
        localObject2 = "location";
        localObject2 = ((o)localObject1).c((String)localObject2);
        localObject3 = "name";
        localObject2 = ay.a((String)localObject3, (o)localObject2);
        boolean bool2 = TextUtils.isEmpty((CharSequence)localObject2);
        if (!bool2)
        {
          int k = 2131362471;
          paramVarArgs.put(k, localObject2);
        }
      }
      localObject2 = "gender";
      bool1 = ((o)localObject1).a((String)localObject2);
      if (bool1)
      {
        localObject2 = "gender";
        localObject2 = ay.a((String)localObject2, (o)localObject1);
        localObject3 = "male";
        bool1 = ((String)localObject2).equalsIgnoreCase((String)localObject3);
        if (bool1) {
          localObject2 = "M";
        } else {
          localObject2 = "F";
        }
        boolean bool3 = TextUtils.isEmpty((CharSequence)localObject2);
        if (!bool3)
        {
          int m = 2131363167;
          paramVarArgs.put(m, localObject2);
        }
      }
      localObject2 = "picture";
      localObject1 = ((o)localObject1).c((String)localObject2);
      localObject2 = "data";
      localObject1 = ((o)localObject1).c((String)localObject2);
      localObject2 = "url";
      localObject1 = ay.a((String)localObject2, (o)localObject1);
      int j = 2131363993;
      paramVarArgs.put(j, localObject1);
    }
    catch (RuntimeException localRuntimeException)
    {
      com.truecaller.log.d.a(localRuntimeException);
      localObject1 = null;
      d = false;
    }
    return paramVarArgs;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.e.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */