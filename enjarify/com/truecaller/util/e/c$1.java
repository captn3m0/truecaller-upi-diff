package com.truecaller.util.e;

import android.net.Uri;
import android.text.TextUtils;
import android.util.SparseArray;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.truecaller.old.a.a;

final class c$1
  extends a
{
  c$1(c paramc, com.truecaller.old.a.c paramc1, Object[] paramArrayOfObject, d paramVarArgs)
  {
    super(paramc1, false, paramArrayOfObject);
  }
  
  public final void a(Object paramObject) {}
  
  protected final Object doInBackground(Object... paramVarArgs)
  {
    return null;
  }
  
  public final void onPreExecute()
  {
    d locald = a;
    Object localObject1 = c;
    SparseArray localSparseArray = new android/util/SparseArray;
    localSparseArray.<init>();
    Object localObject2 = c;
    if (localObject2 != null)
    {
      localObject2 = c.h;
      String str1 = c.i;
      String str2 = c.c;
      boolean bool1 = TextUtils.isEmpty((CharSequence)localObject2);
      boolean bool3;
      if (bool1)
      {
        bool1 = TextUtils.isEmpty(str1);
        if (bool1)
        {
          bool1 = TextUtils.isEmpty(str2);
          if (!bool1)
          {
            String str3 = " ";
            int i = str2.indexOf(str3);
            if (i > 0)
            {
              bool3 = false;
              localObject2 = str2.substring(0, i);
              i += 1;
              str1 = str2.substring(i);
            }
          }
        }
      }
      boolean bool2 = TextUtils.isEmpty(str1);
      if (!bool2)
      {
        j = 2131363586;
        localSparseArray.put(j, str1);
      }
      boolean bool4 = TextUtils.isEmpty((CharSequence)localObject2);
      int j = 2131363094;
      if (!bool4)
      {
        localSparseArray.put(j, localObject2);
      }
      else
      {
        bool3 = TextUtils.isEmpty(str2);
        if (!bool3) {
          localSparseArray.put(j, str2);
        }
      }
      localObject2 = c.b;
      if (localObject2 != null)
      {
        int m = 2131362940;
        localSparseArray.put(m, localObject2);
      }
      localObject1 = c.d;
      if (localObject1 != null)
      {
        int k = 2131363993;
        localObject1 = ((Uri)localObject1).toString();
        localSparseArray.put(k, localObject1);
      }
    }
    c.a(locald, localSparseArray);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.e.c.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */