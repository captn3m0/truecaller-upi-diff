package com.truecaller.util.e;

import android.app.Application;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import com.facebook.AccessToken;
import com.truecaller.common.h.am;
import com.truecaller.log.d;
import com.truecaller.ui.o;
import com.truecaller.util.dh;
import java.util.Collections;
import java.util.List;

public final class b
  extends g
{
  private static final List a = Collections.singletonList("email");
  
  b(Application paramApplication)
  {
    super(paramApplication, 1);
  }
  
  public static void a(Context paramContext, String paramString)
  {
    boolean bool = am.b(paramString);
    String str1;
    if (bool)
    {
      try
      {
        Object localObject1 = paramContext.getPackageManager();
        String str2 = "com.facebook.katana";
        ((PackageManager)localObject1).getPackageInfo(str2, 0);
        localObject1 = new android/content/Intent;
        str2 = "android.intent.action.VIEW";
        Object localObject2 = "fb://profile/";
        String str3 = String.valueOf(paramString);
        localObject2 = ((String)localObject2).concat(str3);
        localObject2 = Uri.parse((String)localObject2);
        ((Intent)localObject1).<init>(str2, (Uri)localObject2);
        paramContext.startActivity((Intent)localObject1);
        bool = true;
      }
      catch (ActivityNotFoundException localActivityNotFoundException) {}catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
      d.a(localNameNotFoundException);
    }
    else
    {
      bool = false;
      str1 = null;
    }
    if (!bool)
    {
      str1 = "http://www.facebook.com/";
      paramString = String.valueOf(paramString);
      paramString = str1.concat(paramString);
      dh.a(paramContext, paramString, false);
    }
  }
  
  public final e a(o paramo, f paramf)
  {
    b.a locala = new com/truecaller/util/e/b$a;
    locala.<init>(this, paramo, paramf);
    return locala;
  }
  
  public final boolean a()
  {
    AccessToken localAccessToken = AccessToken.a();
    if (localAccessToken != null)
    {
      boolean bool = localAccessToken.d();
      if (!bool) {
        return true;
      }
    }
    return false;
  }
  
  public final void b()
  {
    com.facebook.login.f.a().b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */