package com.truecaller.util.e;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions.Builder;
import com.google.android.gms.common.api.Api.ApiOptions.HasOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.truecaller.ui.o;

public final class c
  extends g
{
  ProgressDialog a = null;
  boolean b = false;
  GoogleSignInAccount c = null;
  private GoogleApiClient f;
  
  c(Application paramApplication)
  {
    super(paramApplication, 4);
  }
  
  static GoogleApiClient a(Context paramContext, android.support.v4.app.f paramf, GoogleApiClient.ConnectionCallbacks paramConnectionCallbacks)
  {
    Object localObject1 = new com/google/android/gms/auth/api/signin/GoogleSignInOptions$Builder;
    Object localObject2 = GoogleSignInOptions.f;
    ((GoogleSignInOptions.Builder)localObject1).<init>((GoogleSignInOptions)localObject2);
    localObject1 = ((GoogleSignInOptions.Builder)localObject1).b().c().d();
    localObject2 = new com/google/android/gms/common/api/GoogleApiClient$Builder;
    ((GoogleApiClient.Builder)localObject2).<init>(paramContext);
    if (paramf != null)
    {
      paramContext = null;
      ((GoogleApiClient.Builder)localObject2).a(paramf, null);
    }
    if (paramConnectionCallbacks != null) {
      ((GoogleApiClient.Builder)localObject2).a(paramConnectionCallbacks);
    }
    paramContext = Auth.e;
    return ((GoogleApiClient.Builder)localObject2).a(paramContext, (Api.ApiOptions.HasOptions)localObject1).b();
  }
  
  public final e a(o paramo, f paramf)
  {
    c.a locala = new com/truecaller/util/e/c$a;
    locala.<init>(this, paramo, paramf);
    return locala;
  }
  
  final void a(GoogleApiClient paramGoogleApiClient)
  {
    try
    {
      f = paramGoogleApiClient;
      return;
    }
    finally
    {
      paramGoogleApiClient = finally;
      throw paramGoogleApiClient;
    }
  }
  
  public final boolean a()
  {
    GoogleSignInAccount localGoogleSignInAccount = c;
    return localGoogleSignInAccount != null;
  }
  
  public final void b()
  {
    Object localObject1 = null;
    c = null;
    boolean bool = d();
    if (bool)
    {
      localObject1 = Auth.h;
      localObject2 = c();
      ((GoogleSignInApi)localObject1).c((GoogleApiClient)localObject2);
      localObject1 = Auth.h;
      localObject2 = c();
      ((GoogleSignInApi)localObject1).d((GoogleApiClient)localObject2);
      return;
    }
    localObject1 = a(d, null, null);
    Object localObject2 = new com/truecaller/util/e/c$2;
    ((c.2)localObject2).<init>(this, (GoogleApiClient)localObject1);
    ((GoogleApiClient)localObject1).a((GoogleApiClient.ConnectionCallbacks)localObject2);
    ((GoogleApiClient)localObject1).e();
  }
  
  final GoogleApiClient c()
  {
    try
    {
      GoogleApiClient localGoogleApiClient = f;
      return localGoogleApiClient;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  final boolean d()
  {
    GoogleApiClient localGoogleApiClient = f;
    if (localGoogleApiClient != null)
    {
      boolean bool = localGoogleApiClient.j();
      if (bool) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.e.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */