package com.truecaller.util.e;

import android.app.Application;
import com.truecaller.ui.o;

public abstract class g
{
  protected final Application d;
  protected final int e;
  
  g(Application paramApplication, int paramInt)
  {
    d = paramApplication;
    e = paramInt;
  }
  
  public static g a(Application paramApplication, int paramInt)
  {
    int i = 1;
    Object localObject;
    if (paramInt != i)
    {
      i = 4;
      if (paramInt != i)
      {
        paramApplication = null;
        paramInt = 0;
        localObject = null;
      }
      else
      {
        localObject = new com/truecaller/util/e/c;
        ((c)localObject).<init>(paramApplication);
      }
    }
    else
    {
      localObject = new com/truecaller/util/e/b;
      ((b)localObject).<init>(paramApplication);
    }
    return (g)localObject;
  }
  
  static void a(d paramd, Object paramObject)
  {
    if (paramd != null) {
      paramd.a(paramObject);
    }
  }
  
  public abstract e a(o paramo, f paramf);
  
  final void a(f paramf)
  {
    if (paramf != null)
    {
      int i = e;
      paramf.a(i);
    }
  }
  
  public abstract boolean a();
  
  public abstract void b();
}

/* Location:
 * Qualified Name:     com.truecaller.util.e.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */