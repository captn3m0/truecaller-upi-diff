package com.truecaller.util.e;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.truecaller.old.a.a;
import com.truecaller.ui.o;

final class c$a
  extends g.a
{
  c$a(c paramc, o paramo, f paramf)
  {
    super(paramc, paramo, paramf);
  }
  
  protected final a a(com.truecaller.old.a.c paramc, d paramd)
  {
    c localc = a;
    c.1 local1 = new com/truecaller/util/e/c$1;
    Object[] arrayOfObject = new Object[0];
    local1.<init>(localc, paramc, arrayOfObject, paramd);
    return local1;
  }
  
  public final void a(int paramInt1, int paramInt2, Intent paramIntent)
  {
    paramInt2 = 9001;
    if (paramInt1 == paramInt2)
    {
      GoogleSignInResult localGoogleSignInResult = Auth.h.a(paramIntent);
      a(localGoogleSignInResult);
    }
  }
  
  public final void a(Bundle paramBundle)
  {
    Object localObject1 = "---------> GoogleUtil onCreate";
    new String[1][0] = localObject1;
    if (paramBundle != null)
    {
      localObject1 = a;
      localObject2 = "resolutionInProgress";
      boolean bool = paramBundle.getBoolean((String)localObject2);
      b = bool;
    }
    paramBundle = new com/truecaller/util/e/c$a$1;
    paramBundle.<init>(this);
    localObject1 = a;
    Object localObject2 = b;
    android.support.v4.app.f localf = b;
    paramBundle = c.a((Context)localObject2, localf, paramBundle);
    ((c)localObject1).a(paramBundle);
    paramBundle = a;
    localObject1 = new android/app/ProgressDialog;
    localObject2 = b;
    ((ProgressDialog)localObject1).<init>((Context)localObject2);
    a = ((ProgressDialog)localObject1);
    paramBundle = a.a;
    localObject1 = b.getString(2131887213);
    paramBundle.setMessage((CharSequence)localObject1);
  }
  
  final void a(GoogleSignInResult paramGoogleSignInResult)
  {
    Object localObject = a;
    boolean bool = ((Status)localObject).b();
    if (bool)
    {
      localObject = a;
      paramGoogleSignInResult = b;
      c = paramGoogleSignInResult;
      d.a(4);
      return;
    }
    a.c = null;
  }
  
  public final void b(Bundle paramBundle)
  {
    boolean bool = a.b;
    paramBundle.putBoolean("resolutionInProgress", bool);
  }
  
  public final void d()
  {
    Object localObject1 = GoogleApiAvailability.a();
    Object localObject2 = a.d;
    int i = ((GoogleApiAvailability)localObject1).a((Context)localObject2);
    if (i != 0)
    {
      localObject2 = GoogleApiAvailability.a();
      android.support.v4.app.f localf = b;
      ((GoogleApiAvailability)localObject2).a(localf, i, 0).show();
      return;
    }
    localObject1 = a;
    boolean bool = ((c)localObject1).d();
    if (bool)
    {
      localObject1 = a;
      bool = ((c)localObject1).a();
      if (!bool)
      {
        localObject1 = Auth.h;
        localObject2 = a.c();
        localObject1 = ((GoogleSignInApi)localObject1).a((GoogleApiClient)localObject2);
        c.startActivityForResult((Intent)localObject1, 9001);
        return;
      }
      localObject1 = a;
      localObject2 = d;
      ((c)localObject1).a((f)localObject2);
    }
  }
  
  public final void e()
  {
    a.b();
  }
  
  public final g f()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.e.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */