package com.truecaller.util.e;

import android.os.Bundle;
import com.truecaller.old.a.a;
import com.truecaller.old.a.c;
import com.truecaller.ui.o;
import java.util.concurrent.LinkedBlockingQueue;

abstract class g$a
  implements e
{
  private final LinkedBlockingQueue a;
  final android.support.v4.app.f b;
  final o c;
  final f d;
  
  g$a(g paramg, o paramo, f paramf)
  {
    paramg = new java/util/concurrent/LinkedBlockingQueue;
    paramg.<init>();
    a = paramg;
    c = paramo;
    paramg = paramo.getActivity();
    b = paramg;
    d = paramf;
  }
  
  private void a(a parama)
  {
    if (parama != null) {}
    try
    {
      Object localObject = e;
      boolean bool = ((g)localObject).a();
      if (bool)
      {
        parama.a();
        return;
      }
      localObject = a;
      ((LinkedBlockingQueue)localObject).add(parama);
      d();
      return;
    }
    finally {}
  }
  
  protected abstract a a(c paramc, d paramd);
  
  public void a() {}
  
  public void a(Bundle paramBundle) {}
  
  public void b(Bundle paramBundle) {}
  
  public final void b(c paramc, d paramd)
  {
    paramc = a(paramc, paramd);
    a(paramc);
  }
  
  final void g()
  {
    try
    {
      Object localObject1 = a;
      boolean bool = ((LinkedBlockingQueue)localObject1).isEmpty();
      if (!bool)
      {
        localObject1 = a;
        localObject1 = ((LinkedBlockingQueue)localObject1).poll();
        localObject1 = (a)localObject1;
        ((a)localObject1).a();
      }
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.e.g.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */