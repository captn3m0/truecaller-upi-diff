package com.truecaller.util;

import android.content.Context;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.a;
import com.truecaller.utils.extensions.r;

public final class bh
  implements bg
{
  private final com.truecaller.multisim.h a;
  private final com.truecaller.messaging.h b;
  private final Context c;
  
  public bh(com.truecaller.multisim.h paramh, com.truecaller.messaging.h paramh1, Context paramContext)
  {
    a = paramh;
    b = paramh1;
    c = paramContext;
  }
  
  private final Long c(int paramInt)
  {
    com.truecaller.multisim.h localh = a;
    Object localObject = localh.a(paramInt);
    if (localObject == null) {
      return null;
    }
    k.a(localObject, "multiSimManager.getSimIn…SlotIndex) ?: return null");
    localh = a;
    localObject = b;
    localObject = localh.c((String)localObject);
    k.a(localObject, "multiSimManager.getCarri…uration(simInfo.simToken)");
    return Long.valueOf(((a)localObject).j());
  }
  
  public final long a(int paramInt)
  {
    int i = 2;
    if (paramInt != i)
    {
      Object localObject1 = a;
      paramInt = ((com.truecaller.multisim.h)localObject1).j();
      if (paramInt == 0)
      {
        localObject1 = a;
        localObject2 = ((com.truecaller.multisim.h)localObject1).f();
        localObject1 = ((com.truecaller.multisim.h)localObject1).c((String)localObject2);
        k.a(localObject1, "multiSimManager.getCarri…mManager.defaultSimToken)");
        return ((a)localObject1).j();
      }
      paramInt = 0;
      localObject1 = c(0);
      i = 1;
      Object localObject2 = c(i);
      if ((localObject1 != null) && (localObject2 != null))
      {
        long l1 = ((Long)localObject1).longValue();
        long l2 = ((Long)localObject2).longValue();
        return Math.min(l1, l2);
      }
      if (localObject1 == null) {
        localObject1 = localObject2;
      }
      if (localObject1 != null) {
        return ((Long)localObject1).longValue();
      }
      return 307200L;
    }
    return b.L();
  }
  
  public final long a(long paramLong)
  {
    return paramLong / 250000L;
  }
  
  public final String a(Uri paramUri)
  {
    k.b(paramUri, "uri");
    Context localContext = c;
    return r.c(paramUri, localContext);
  }
  
  public final long b(int paramInt)
  {
    return paramInt * 2000000 / 8;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.bh
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */