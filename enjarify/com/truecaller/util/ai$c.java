package com.truecaller.util;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.content.FileProvider;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import java.io.File;
import kotlinx.coroutines.ag;

final class ai$c
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  ai$c(Context paramContext, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/util/ai$c;
    Context localContext = b;
    localc.<init>(localContext, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool = paramObject instanceof o.b;
      if (bool) {
        throw a;
      }
      break;
    case 0: 
      int j = paramObject instanceof o.b;
      if (j != 0) {
        break label166;
      }
      paramObject = ai.a;
      j = 1;
      a = j;
      paramObject = ((ai)paramObject).a(this);
      if (paramObject == localObject) {
        return localObject;
      }
      break;
    }
    paramObject = (File)paramObject;
    localObject = new android/content/Intent;
    ((Intent)localObject).<init>("android.intent.action.SEND");
    Context localContext = b;
    String str = com.truecaller.common.h.ai.a(localContext);
    paramObject = (Parcelable)FileProvider.a(localContext, str, (File)paramObject);
    ((Intent)localObject).putExtra("android.intent.extra.STREAM", (Parcelable)paramObject);
    ((Intent)localObject).setType("application/binary");
    b.startActivity((Intent)localObject);
    return x.a;
    label166:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ai.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */