package com.truecaller.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;

public final class s
{
  public static final SortedSet a;
  public static final Comparator b;
  public static final Iterator c;
  
  static
  {
    Object localObject = new com/truecaller/util/s$b;
    ((s.b)localObject).<init>((byte)0);
    a = (SortedSet)localObject;
    localObject = new com/truecaller/util/s$a;
    ((s.a)localObject).<init>((byte)0);
    b = (Comparator)localObject;
    localObject = new com/truecaller/util/s$1;
    ((s.1)localObject).<init>();
    c = (Iterator)localObject;
  }
  
  public static SortedSet a()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */