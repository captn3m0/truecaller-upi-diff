package com.truecaller.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.widget.ImageView;

final class aw$a
  implements Runnable
{
  private final String b;
  private final Bitmap c;
  private final aw.j d;
  
  public aw$a(aw paramaw, String paramString, Bitmap paramBitmap, aw.j paramj)
  {
    b = paramString;
    c = paramBitmap;
    d = paramj;
  }
  
  public final void run()
  {
    Object localObject1 = a;
    Object localObject2 = d;
    boolean bool = ((aw)localObject1).a((aw.j)localObject2);
    if (bool) {
      return;
    }
    localObject1 = d.b;
    Bitmap localBitmap;
    if (localObject1 == null)
    {
      localObject1 = new android/graphics/drawable/BitmapDrawable;
      localObject2 = aw.a(a).getResources();
      localBitmap = c;
      ((BitmapDrawable)localObject1).<init>((Resources)localObject2, localBitmap);
      localObject2 = d.c;
      int i = d.d;
      ((LayerDrawable)localObject2).setDrawableByLayerId(i, (Drawable)localObject1);
      return;
    }
    localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = d.e;
      if (localObject1 != null) {}
    }
    else
    {
      localObject1 = a;
      localObject2 = d.b;
      localBitmap = c;
      aw.a((aw)localObject1, (ImageView)localObject2, localBitmap);
      localObject1 = d.e;
      if (localObject1 != null)
      {
        localObject1 = d.e;
        localObject2 = d.b;
        localBitmap = c;
        String str = b;
        ((aw.e)localObject1).a((ImageView)localObject2, localBitmap, str);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.aw.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */