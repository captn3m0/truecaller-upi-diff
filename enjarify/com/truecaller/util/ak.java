package com.truecaller.util;

import dagger.a.d;
import javax.inject.Provider;

public final class ak
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private ak(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
  }
  
  public static ak a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    ak localak = new com/truecaller/util/ak;
    localak.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localak;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ak
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */