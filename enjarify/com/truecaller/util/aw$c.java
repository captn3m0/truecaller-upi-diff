package com.truecaller.util;

import android.content.Context;
import android.graphics.Bitmap;
import com.truecaller.common.h.am;
import com.truecaller.ui.components.n;

final class aw$c
  extends n
{
  private final String a;
  private final boolean b;
  private final boolean c;
  
  private aw$c(String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    a = paramString;
    b = paramBoolean1;
    c = paramBoolean2;
  }
  
  public final Bitmap c(Context paramContext)
  {
    String str = a;
    boolean bool = c;
    return ap.a(paramContext, str, bool);
  }
  
  public final Object q()
  {
    boolean bool = b;
    if (bool) {
      return am.a(a);
    }
    return "no_cache";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.aw.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */