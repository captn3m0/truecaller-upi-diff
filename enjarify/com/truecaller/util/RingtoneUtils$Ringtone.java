package com.truecaller.util;

public enum RingtoneUtils$Ringtone
{
  private final String mFileName;
  private final String mMimeType;
  private final int mResource;
  private final String mTitle;
  private final int mType;
  
  static
  {
    Ringtone localRingtone = new com/truecaller/util/RingtoneUtils$Ringtone;
    Object localObject1 = localRingtone;
    localRingtone.<init>("Message", 0, 2131821032, "Truecaller Message.ogg", "Truecaller Message", "audio/ogg", 2);
    Message = localRingtone;
    localObject1 = new com/truecaller/util/RingtoneUtils$Ringtone;
    ((Ringtone)localObject1).<init>("Ringtone", 1, 2131821035, "Truecaller Ringtone.ogg", "Truecaller Ringtone", "audio/ogg", 1);
    Ringtone = (Ringtone)localObject1;
    localObject1 = new com/truecaller/util/RingtoneUtils$Ringtone;
    Object localObject2 = localObject1;
    ((Ringtone)localObject1).<init>("FlashRingtone", 2, 2131821031, "Truecaller Flash.ogg", "Truecaller Flash", "audio/ogg", 1);
    FlashRingtone = (Ringtone)localObject1;
    localObject1 = new Ringtone[3];
    localObject2 = Message;
    localObject1[0] = localObject2;
    localObject2 = Ringtone;
    localObject1[1] = localObject2;
    localObject2 = FlashRingtone;
    localObject1[2] = localObject2;
    $VALUES = (Ringtone[])localObject1;
  }
  
  private RingtoneUtils$Ringtone(int paramInt2, String paramString2, String paramString3, String paramString4, int paramInt3)
  {
    mResource = paramInt2;
    mFileName = paramString2;
    mTitle = paramString3;
    mMimeType = paramString4;
    mType = paramInt3;
  }
  
  public final String getTitle()
  {
    return mTitle;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.RingtoneUtils.Ringtone
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */