package com.truecaller.util;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;

public final class r
{
  public static void a(Context paramContext, CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    String str = "clipboard";
    paramContext = (ClipboardManager)paramContext.getSystemService(str);
    if (paramContext == null) {
      return;
    }
    paramCharSequence1 = ClipData.newPlainText(paramCharSequence2, paramCharSequence1);
    paramContext.setPrimaryClip(paramCharSequence1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */