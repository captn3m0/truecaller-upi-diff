package com.truecaller.util;

import android.content.ContentResolver;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.utils.extensions.Scheme;
import java.io.File;

public final class ci
  implements ch
{
  private final ContentResolver a;
  
  public ci(ContentResolver paramContentResolver)
  {
    a = paramContentResolver;
  }
  
  public final void a(Uri paramUri)
  {
    k.b(paramUri, "uri");
    Object localObject = paramUri.getScheme();
    String str = Scheme.CONTENT.getValue();
    boolean bool1 = k.a(localObject, str);
    if (bool1)
    {
      a.delete(paramUri, null, null);
      return;
    }
    str = Scheme.FILE.getValue();
    boolean bool2 = k.a(localObject, str);
    if (bool2)
    {
      localObject = new java/io/File;
      paramUri = paramUri.getPath();
      ((File)localObject).<init>(paramUri);
      ((File)localObject).delete();
      return;
    }
    localObject = new String[1];
    paramUri = String.valueOf(paramUri);
    paramUri = "URI scheme is not supported for deletion: ".concat(paramUri);
    localObject[0] = paramUri;
  }
  
  public final void a(BinaryEntity paramBinaryEntity)
  {
    if (paramBinaryEntity == null) {
      return;
    }
    boolean bool = c;
    if (!bool) {
      return;
    }
    paramBinaryEntity = b;
    k.a(paramBinaryEntity, "entity.content");
    a(paramBinaryEntity);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ci
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */