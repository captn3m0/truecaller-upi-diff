package com.truecaller.util;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class ab$g
  extends u
{
  private final String b;
  
  private ab$g(e parame, String paramString)
  {
    super(parame);
    b = paramString;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".getAlphaNumericContactInfo(");
    String str = a(b, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.ab.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */