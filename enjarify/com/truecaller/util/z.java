package com.truecaller.util;

import c.u;
import com.truecaller.data.entity.Contact;
import java.util.Collection;
import java.util.List;

public final class z
{
  /* Error */
  public static final List a(android.database.Cursor paramCursor, c.g.a.b paramb1, c.g.a.b paramb2, c.g.a.q paramq)
  {
    // Byte code:
    //   0: ldc 17
    //   2: astore 4
    //   4: aload_3
    //   5: aload 4
    //   7: invokestatic 23	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   10: aload_0
    //   11: ifnull +272 -> 283
    //   14: aload_0
    //   15: astore 4
    //   17: aload_0
    //   18: checkcast 25	java/io/Closeable
    //   21: astore 4
    //   23: aconst_null
    //   24: astore 5
    //   26: aload 4
    //   28: astore 6
    //   30: aload 4
    //   32: checkcast 27	android/database/Cursor
    //   35: astore 6
    //   37: new 29	java/util/ArrayList
    //   40: astore 7
    //   42: aload 7
    //   44: invokespecial 33	java/util/ArrayList:<init>	()V
    //   47: aload 7
    //   49: checkcast 35	java/util/List
    //   52: astore 7
    //   54: new 37	com/truecaller/data/access/e
    //   57: astore 8
    //   59: aload 8
    //   61: aload_0
    //   62: invokespecial 40	com/truecaller/data/access/e:<init>	(Landroid/database/Cursor;)V
    //   65: aconst_null
    //   66: astore 9
    //   68: aload 6
    //   70: invokeinterface 44 1 0
    //   75: istore 10
    //   77: iload 10
    //   79: ifeq +176 -> 255
    //   82: ldc 46
    //   84: astore 11
    //   86: aload 6
    //   88: aload 11
    //   90: invokestatic 51	com/truecaller/utils/extensions/k:a	(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    //   93: astore 11
    //   95: aload 11
    //   97: ifnonnull +6 -> 103
    //   100: goto -32 -> 68
    //   103: aload 9
    //   105: ifnull +13 -> 118
    //   108: aload 9
    //   110: invokevirtual 57	com/truecaller/data/entity/Contact:getTcId	()Ljava/lang/String;
    //   113: astore 12
    //   115: goto +6 -> 121
    //   118: aconst_null
    //   119: astore 12
    //   121: aload 12
    //   123: aload 11
    //   125: invokestatic 60	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/Object;)Z
    //   128: iconst_1
    //   129: ixor
    //   130: istore 10
    //   132: iload 10
    //   134: ifeq +108 -> 242
    //   137: aload 9
    //   139: ifnull +8 -> 147
    //   142: aload 9
    //   144: invokevirtual 63	com/truecaller/data/entity/Contact:ad	()V
    //   147: aload_1
    //   148: aload_0
    //   149: invokeinterface 69 2 0
    //   154: astore 9
    //   156: aload_2
    //   157: aload_0
    //   158: invokeinterface 69 2 0
    //   163: astore 11
    //   165: aload 8
    //   167: aload 6
    //   169: invokevirtual 72	com/truecaller/data/access/e:a	(Landroid/database/Cursor;)Lcom/truecaller/data/entity/Contact;
    //   172: astore 12
    //   174: aload 12
    //   176: ifnull +60 -> 236
    //   179: ldc 74
    //   181: astore 13
    //   183: aload 12
    //   185: aload 13
    //   187: invokestatic 76	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   190: aload_3
    //   191: aload 12
    //   193: aload 9
    //   195: aload 11
    //   197: invokeinterface 81 4 0
    //   202: astore 9
    //   204: aload 9
    //   206: ifnull +13 -> 219
    //   209: aload 7
    //   211: aload 9
    //   213: invokeinterface 85 2 0
    //   218: pop
    //   219: aload 8
    //   221: aload 6
    //   223: aload 12
    //   225: invokevirtual 88	com/truecaller/data/access/e:a	(Landroid/database/Cursor;Lcom/truecaller/data/entity/Contact;)Lcom/truecaller/data/entity/Entity;
    //   228: pop
    //   229: aload 12
    //   231: astore 9
    //   233: goto -165 -> 68
    //   236: aconst_null
    //   237: astore 9
    //   239: goto -171 -> 68
    //   242: aload 8
    //   244: aload 6
    //   246: aload 9
    //   248: invokevirtual 88	com/truecaller/data/access/e:a	(Landroid/database/Cursor;Lcom/truecaller/data/entity/Contact;)Lcom/truecaller/data/entity/Entity;
    //   251: pop
    //   252: goto -184 -> 68
    //   255: aload 4
    //   257: aconst_null
    //   258: invokestatic 93	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   261: aload 7
    //   263: areturn
    //   264: astore_0
    //   265: goto +9 -> 274
    //   268: astore_0
    //   269: aload_0
    //   270: astore 5
    //   272: aload_0
    //   273: athrow
    //   274: aload 4
    //   276: aload 5
    //   278: invokestatic 93	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   281: aload_0
    //   282: athrow
    //   283: getstatic 98	c/a/y:a	Lc/a/y;
    //   286: checkcast 35	java/util/List
    //   289: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	290	0	paramCursor	android.database.Cursor
    //   0	290	1	paramb1	c.g.a.b
    //   0	290	2	paramb2	c.g.a.b
    //   0	290	3	paramq	c.g.a.q
    //   2	273	4	localObject1	Object
    //   24	253	5	localCursor	android.database.Cursor
    //   28	217	6	localObject2	Object
    //   40	222	7	localObject3	Object
    //   57	186	8	locale	com.truecaller.data.access.e
    //   66	181	9	localObject4	Object
    //   75	58	10	bool	boolean
    //   84	112	11	localObject5	Object
    //   113	117	12	localObject6	Object
    //   181	5	13	str	String
    // Exception table:
    //   from	to	target	type
    //   272	274	264	finally
    //   30	35	268	finally
    //   37	40	268	finally
    //   42	47	268	finally
    //   47	52	268	finally
    //   54	57	268	finally
    //   61	65	268	finally
    //   68	75	268	finally
    //   88	93	268	finally
    //   108	113	268	finally
    //   123	128	268	finally
    //   142	147	268	finally
    //   148	154	268	finally
    //   157	163	268	finally
    //   167	172	268	finally
    //   185	190	268	finally
    //   195	202	268	finally
    //   211	219	268	finally
    //   223	229	268	finally
    //   246	252	268	finally
  }
  
  /* Error */
  public static final List a(android.database.Cursor paramCursor, c.g.a.b paramb, c.g.a.m paramm)
  {
    // Byte code:
    //   0: ldc 17
    //   2: astore_3
    //   3: aload_2
    //   4: aload_3
    //   5: invokestatic 23	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   8: aload_0
    //   9: ifnull +265 -> 274
    //   12: aload_0
    //   13: astore_3
    //   14: aload_0
    //   15: checkcast 25	java/io/Closeable
    //   18: astore_3
    //   19: aconst_null
    //   20: astore 4
    //   22: aload_3
    //   23: astore 5
    //   25: aload_3
    //   26: checkcast 27	android/database/Cursor
    //   29: astore 5
    //   31: new 29	java/util/ArrayList
    //   34: astore 6
    //   36: aload 6
    //   38: invokespecial 33	java/util/ArrayList:<init>	()V
    //   41: aload 6
    //   43: checkcast 35	java/util/List
    //   46: astore 6
    //   48: new 37	com/truecaller/data/access/e
    //   51: astore 7
    //   53: aload 7
    //   55: aload_0
    //   56: invokespecial 40	com/truecaller/data/access/e:<init>	(Landroid/database/Cursor;)V
    //   59: aconst_null
    //   60: astore 8
    //   62: aload 5
    //   64: invokeinterface 44 1 0
    //   69: istore 9
    //   71: iload 9
    //   73: ifeq +175 -> 248
    //   76: ldc 46
    //   78: astore 10
    //   80: aload 5
    //   82: aload 10
    //   84: invokestatic 51	com/truecaller/utils/extensions/k:a	(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    //   87: astore 10
    //   89: aload 10
    //   91: ifnonnull +6 -> 97
    //   94: goto -32 -> 62
    //   97: aload 8
    //   99: ifnull +13 -> 112
    //   102: aload 8
    //   104: invokevirtual 57	com/truecaller/data/entity/Contact:getTcId	()Ljava/lang/String;
    //   107: astore 11
    //   109: goto +6 -> 115
    //   112: aconst_null
    //   113: astore 11
    //   115: aload 11
    //   117: aload 10
    //   119: invokestatic 60	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/Object;)Z
    //   122: iconst_1
    //   123: ixor
    //   124: istore 9
    //   126: iload 9
    //   128: ifeq +107 -> 235
    //   131: aload 8
    //   133: ifnull +8 -> 141
    //   136: aload 8
    //   138: invokevirtual 63	com/truecaller/data/entity/Contact:ad	()V
    //   141: aload_1
    //   142: ifnull +15 -> 157
    //   145: aload_1
    //   146: aload_0
    //   147: invokeinterface 69 2 0
    //   152: astore 8
    //   154: goto +6 -> 160
    //   157: aconst_null
    //   158: astore 8
    //   160: aload 7
    //   162: aload 5
    //   164: invokevirtual 72	com/truecaller/data/access/e:a	(Landroid/database/Cursor;)Lcom/truecaller/data/entity/Contact;
    //   167: astore 10
    //   169: aload 10
    //   171: ifnull +58 -> 229
    //   174: ldc 74
    //   176: astore 11
    //   178: aload 10
    //   180: aload 11
    //   182: invokestatic 76	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   185: aload_2
    //   186: aload 10
    //   188: aload 8
    //   190: invokeinterface 101 3 0
    //   195: astore 8
    //   197: aload 8
    //   199: ifnull +13 -> 212
    //   202: aload 6
    //   204: aload 8
    //   206: invokeinterface 85 2 0
    //   211: pop
    //   212: aload 7
    //   214: aload 5
    //   216: aload 10
    //   218: invokevirtual 88	com/truecaller/data/access/e:a	(Landroid/database/Cursor;Lcom/truecaller/data/entity/Contact;)Lcom/truecaller/data/entity/Entity;
    //   221: pop
    //   222: aload 10
    //   224: astore 8
    //   226: goto -164 -> 62
    //   229: aconst_null
    //   230: astore 8
    //   232: goto -170 -> 62
    //   235: aload 7
    //   237: aload 5
    //   239: aload 8
    //   241: invokevirtual 88	com/truecaller/data/access/e:a	(Landroid/database/Cursor;Lcom/truecaller/data/entity/Contact;)Lcom/truecaller/data/entity/Entity;
    //   244: pop
    //   245: goto -183 -> 62
    //   248: aload_3
    //   249: aconst_null
    //   250: invokestatic 93	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   253: aload 6
    //   255: areturn
    //   256: astore_0
    //   257: goto +9 -> 266
    //   260: astore_0
    //   261: aload_0
    //   262: astore 4
    //   264: aload_0
    //   265: athrow
    //   266: aload_3
    //   267: aload 4
    //   269: invokestatic 93	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   272: aload_0
    //   273: athrow
    //   274: getstatic 98	c/a/y:a	Lc/a/y;
    //   277: checkcast 35	java/util/List
    //   280: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	281	0	paramCursor	android.database.Cursor
    //   0	281	1	paramb	c.g.a.b
    //   0	281	2	paramm	c.g.a.m
    //   2	265	3	localObject1	Object
    //   20	248	4	localCursor	android.database.Cursor
    //   23	215	5	localObject2	Object
    //   34	220	6	localObject3	Object
    //   51	185	7	locale	com.truecaller.data.access.e
    //   60	180	8	localObject4	Object
    //   69	58	9	bool	boolean
    //   78	145	10	localObject5	Object
    //   107	74	11	str	String
    // Exception table:
    //   from	to	target	type
    //   264	266	256	finally
    //   25	29	260	finally
    //   31	34	260	finally
    //   36	41	260	finally
    //   41	46	260	finally
    //   48	51	260	finally
    //   55	59	260	finally
    //   62	69	260	finally
    //   82	87	260	finally
    //   102	107	260	finally
    //   117	122	260	finally
    //   136	141	260	finally
    //   146	152	260	finally
    //   162	167	260	finally
    //   180	185	260	finally
    //   188	195	260	finally
    //   204	212	260	finally
    //   216	222	260	finally
    //   239	245	260	finally
  }
  
  public static final boolean a(Contact paramContact)
  {
    if (paramContact != null)
    {
      paramContact = paramContact.p();
      if (paramContact != null) {
        if (paramContact != null)
        {
          paramContact = c.n.m.b((CharSequence)paramContact).toString();
          if (paramContact != null)
          {
            paramContact = (CharSequence)paramContact;
            boolean bool1 = false;
            int i = 0;
            for (;;)
            {
              int j = paramContact.length();
              if (i >= j) {
                break;
              }
              j = paramContact.charAt(i);
              Object localObject = new c/k/c;
              char c1 = '9';
              ((c.k.c)localObject).<init>('0', c1);
              localObject = (Iterable)localObject;
              Character localCharacter1 = Character.valueOf(',');
              localObject = (Collection)c.a.m.c((Iterable)localObject, localCharacter1);
              localCharacter1 = Character.valueOf('*');
              localObject = (Collection)c.a.m.a((Collection)localObject, localCharacter1);
              localCharacter1 = Character.valueOf('#');
              localObject = (Collection)c.a.m.a((Collection)localObject, localCharacter1);
              localCharacter1 = Character.valueOf(';');
              localObject = (Collection)c.a.m.a((Collection)localObject, localCharacter1);
              char c2 = '+';
              localCharacter1 = Character.valueOf(c2);
              localObject = c.a.m.a((Collection)localObject, localCharacter1);
              Character localCharacter2 = Character.valueOf(j);
              boolean bool2 = ((List)localObject).contains(localCharacter2);
              if (!bool2) {
                break label214;
              }
              i += 1;
            }
            bool1 = true;
            label214:
            paramContact = Boolean.valueOf(bool1);
            break label236;
          }
        }
        else
        {
          paramContact = new c/u;
          paramContact.<init>("null cannot be cast to non-null type kotlin.CharSequence");
          throw paramContact;
        }
      }
    }
    paramContact = null;
    label236:
    return com.truecaller.utils.extensions.c.a(paramContact);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */