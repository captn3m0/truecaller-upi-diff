package com.truecaller.util;

import android.content.Context;
import android.net.Uri;
import com.d.b.aa;
import com.d.b.ac;

public final class df
  extends ac
{
  private final Context a;
  
  public df(Context paramContext)
  {
    paramContext = paramContext.getApplicationContext();
    a = paramContext;
  }
  
  /* Error */
  public final com.d.b.ac.a a(aa paramaa, int paramInt)
  {
    // Byte code:
    //   0: new 20	android/media/MediaMetadataRetriever
    //   3: astore_3
    //   4: aload_3
    //   5: invokespecial 21	android/media/MediaMetadataRetriever:<init>	()V
    //   8: aconst_null
    //   9: astore 4
    //   11: aload_0
    //   12: getfield 18	com/truecaller/util/df:a	Landroid/content/Context;
    //   15: astore 5
    //   17: aload_1
    //   18: getfield 27	com/d/b/aa:d	Landroid/net/Uri;
    //   21: astore_1
    //   22: aload_3
    //   23: aload 5
    //   25: aload_1
    //   26: invokevirtual 31	android/media/MediaMetadataRetriever:setDataSource	(Landroid/content/Context;Landroid/net/Uri;)V
    //   29: aload_3
    //   30: invokevirtual 35	android/media/MediaMetadataRetriever:getFrameAtTime	()Landroid/graphics/Bitmap;
    //   33: astore_1
    //   34: aload_3
    //   35: invokevirtual 38	android/media/MediaMetadataRetriever:release	()V
    //   38: goto +17 -> 55
    //   41: astore_1
    //   42: aload_3
    //   43: invokevirtual 38	android/media/MediaMetadataRetriever:release	()V
    //   46: aload_1
    //   47: athrow
    //   48: pop
    //   49: aload_3
    //   50: invokevirtual 38	android/media/MediaMetadataRetriever:release	()V
    //   53: aconst_null
    //   54: astore_1
    //   55: aload_1
    //   56: ifnonnull +5 -> 61
    //   59: aconst_null
    //   60: areturn
    //   61: new 40	com/d/b/ac$a
    //   64: astore_3
    //   65: getstatic 46	com/d/b/w$d:b	Lcom/d/b/w$d;
    //   68: astore 4
    //   70: aload_3
    //   71: aload_1
    //   72: aload 4
    //   74: invokespecial 49	com/d/b/ac$a:<init>	(Landroid/graphics/Bitmap;Lcom/d/b/w$d;)V
    //   77: aload_3
    //   78: areturn
    //   79: pop
    //   80: goto -34 -> 46
    //   83: pop
    //   84: goto -31 -> 53
    //   87: pop
    //   88: goto -33 -> 55
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	91	0	this	df
    //   0	91	1	paramaa	aa
    //   0	91	2	paramInt	int
    //   3	75	3	localObject	Object
    //   9	64	4	locald	com.d.b.w.d
    //   15	9	5	localContext	Context
    //   48	1	6	localRuntimeException1	RuntimeException
    //   79	1	7	localRuntimeException2	RuntimeException
    //   83	1	8	localRuntimeException3	RuntimeException
    //   87	1	9	localRuntimeException4	RuntimeException
    // Exception table:
    //   from	to	target	type
    //   11	15	41	finally
    //   17	21	41	finally
    //   25	29	41	finally
    //   29	33	41	finally
    //   11	15	48	java/lang/RuntimeException
    //   17	21	48	java/lang/RuntimeException
    //   25	29	48	java/lang/RuntimeException
    //   29	33	48	java/lang/RuntimeException
    //   42	46	79	java/lang/RuntimeException
    //   49	53	83	java/lang/RuntimeException
    //   34	38	87	java/lang/RuntimeException
  }
  
  public final boolean a(aa paramaa)
  {
    paramaa = d;
    boolean bool1 = paramaa.isHierarchical();
    if (bool1)
    {
      String str = "com.truecaller.util.VideoRequestHandler.IS_VIDEO";
      boolean bool2 = paramaa.getBooleanQueryParameter(str, false);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.df
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */