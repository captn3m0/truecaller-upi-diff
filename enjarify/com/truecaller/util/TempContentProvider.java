package com.truecaller.util;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.ParcelFileDescriptor;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.extensions.l;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.c.a.a.a.a;
import org.c.a.a.a.i;
import org.c.a.a.a.k;

public class TempContentProvider
  extends ContentProvider
{
  private static final Uri a = Uri.parse("content://com.truecaller.TempContentProvider");
  private static final String[] b = { "_size" };
  
  public static Uri a(Context paramContext, Intent paramIntent, Integer paramInteger)
  {
    Object localObject = a(paramContext);
    boolean bool1 = ((File)localObject).exists();
    if (!bool1)
    {
      bool1 = ((File)localObject).mkdirs();
      if (!bool1) {
        return null;
      }
    }
    if (paramInteger == null) {
      paramInteger = i.a();
    } else {
      paramInteger = paramInteger.toString();
    }
    File localFile = new java/io/File;
    localFile.<init>((File)localObject, paramInteger);
    try
    {
      boolean bool2 = localFile.exists();
      if (!bool2)
      {
        bool2 = localFile.createNewFile();
        if (!bool2) {
          return null;
        }
      }
      localObject = a;
      paramInteger = Uri.withAppendedPath((Uri)localObject, paramInteger);
      if (paramIntent != null)
      {
        int j = Build.VERSION.SDK_INT;
        int i = 21;
        if (j < i)
        {
          localObject = paramContext.getPackageManager();
          i = 65536;
          paramIntent = ((PackageManager)localObject).queryIntentActivities(paramIntent, i).iterator();
          for (;;)
          {
            boolean bool3 = paramIntent.hasNext();
            if (!bool3) {
              break;
            }
            localObject = nextactivityInfo.packageName;
            i = 3;
            paramContext.grantUriPermission((String)localObject, paramInteger, i);
          }
        }
      }
      return paramInteger;
    }
    catch (IOException localIOException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localIOException;
    }
    return null;
  }
  
  private static File a(Context paramContext)
  {
    File localFile = new java/io/File;
    paramContext = paramContext.getCacheDir();
    localFile.<init>(paramContext, "temp_provider");
    return localFile;
  }
  
  private File a(Uri paramUri)
  {
    Object localObject1 = getContext();
    File localFile = null;
    if (localObject1 == null) {
      return null;
    }
    paramUri = paramUri.getPath();
    if (paramUri == null) {
      return null;
    }
    Object localObject2 = "/";
    boolean bool = paramUri.startsWith((String)localObject2);
    int i = 1;
    if (bool) {
      paramUri = paramUri.substring(i);
    }
    localObject2 = "-";
    bool = paramUri.startsWith((String)localObject2);
    if (bool) {
      localObject2 = paramUri.substring(i);
    } else {
      localObject2 = paramUri;
    }
    bool = k.g((CharSequence)localObject2);
    if (!bool) {
      return null;
    }
    localFile = new java/io/File;
    localObject1 = a((Context)localObject1);
    localFile.<init>((File)localObject1, paramUri);
    return localFile;
  }
  
  public int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    paramUri = a(paramUri);
    if (paramUri != null)
    {
      boolean bool = paramUri.delete();
      if (bool) {
        return 1;
      }
    }
    return 0;
  }
  
  public String getType(Uri paramUri)
  {
    return null;
  }
  
  public Uri insert(Uri paramUri, ContentValues paramContentValues)
  {
    return null;
  }
  
  public boolean onCreate()
  {
    return true;
  }
  
  public ParcelFileDescriptor openFile(Uri paramUri, String paramString)
  {
    paramUri = a(paramUri);
    if (paramUri == null) {
      return null;
    }
    String str = "r";
    boolean bool = str.equals(paramString);
    int i;
    if (bool) {
      i = 268435456;
    } else {
      i = 603979776;
    }
    return ParcelFileDescriptor.open(paramUri, i);
  }
  
  public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
  {
    int i = 0;
    int j = 1;
    Object localObject1;
    boolean bool;
    Object localObject2;
    if (paramArrayOfString1 == null)
    {
      paramArrayOfString1 = b;
    }
    else
    {
      k = paramArrayOfString1.length;
      if (k > 0)
      {
        k = paramArrayOfString1.length;
        int m = 0;
        localMatrixCursor = null;
        while (m < k)
        {
          String str1 = paramArrayOfString1[m];
          localObject1 = b;
          bool = a.a((Object[])localObject1, str1);
          localObject2 = new String[j];
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          String str2 = "Unsupported column '";
          localStringBuilder.<init>(str2);
          localStringBuilder.append(str1);
          localStringBuilder.append("' queried");
          str1 = localStringBuilder.toString();
          localObject2[0] = str1;
          AssertionUtil.isTrue(bool, (String[])localObject2);
          m += 1;
        }
      }
    }
    paramUri = a(paramUri);
    int k = 0;
    if (paramUri == null) {
      return null;
    }
    MatrixCursor localMatrixCursor = new android/database/MatrixCursor;
    localMatrixCursor.<init>(paramArrayOfString1, j);
    paramArrayOfString2 = new java/util/ArrayList;
    int n = paramArrayOfString1.length;
    paramArrayOfString2.<init>(n);
    n = paramArrayOfString1.length;
    while (i < n)
    {
      localObject1 = paramArrayOfString1[i];
      localObject2 = "_size";
      bool = ((String)localObject1).equals(localObject2);
      if (bool)
      {
        long l = l.a(paramUri);
        localObject1 = Long.valueOf(l);
      }
      else
      {
        bool = false;
        localObject1 = null;
      }
      paramArrayOfString2.add(localObject1);
      i += 1;
    }
    localMatrixCursor.addRow(paramArrayOfString2);
    return localMatrixCursor;
  }
  
  public int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    return 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.util.TempContentProvider
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */