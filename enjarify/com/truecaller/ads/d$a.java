package com.truecaller.ads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import c.g.b.k;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.truecaller.ads.provider.holders.d;
import com.truecaller.ads.ui.CustomNativeVideoAdActivity;
import com.truecaller.log.AssertionUtil;

final class d$a
  implements View.OnClickListener
{
  d$a(Activity paramActivity, d paramd, NativeCustomTemplateAd paramNativeCustomTemplateAd) {}
  
  public final void onClick(View paramView)
  {
    paramView = CustomNativeVideoAdActivity.b;
    paramView = a;
    Object localObject1 = b;
    k.b(paramView, "activity");
    k.b(localObject1, "adHolder");
    Object localObject2 = (NativeCustomTemplateAd)((d)localObject1).g();
    Object localObject3 = ((NativeCustomTemplateAd)localObject2).getVideoMediaView();
    int i;
    if (localObject3 == null)
    {
      paramView = new java/lang/StringBuilder;
      paramView.<init>("Video ad contain no MediaView, hasVideoContent=");
      localObject1 = ((NativeCustomTemplateAd)localObject2).getVideoController();
      boolean bool = ((VideoController)localObject1).g();
      paramView.append(bool);
      AssertionUtil.reportWeirdnessButNeverCrash(paramView.toString());
      i = 0;
      paramView = null;
    }
    else
    {
      CustomNativeVideoAdActivity.a((d)localObject1);
      localObject1 = new android/content/Intent;
      localObject2 = paramView;
      localObject2 = (Context)paramView;
      localObject3 = CustomNativeVideoAdActivity.class;
      ((Intent)localObject1).<init>((Context)localObject2, (Class)localObject3);
      paramView.startActivity((Intent)localObject1);
      i = 1;
    }
    if (i != 0)
    {
      paramView = c;
      localObject1 = "Image";
      paramView.performClick((String)localObject1);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */