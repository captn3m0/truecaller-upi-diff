package com.truecaller.ads.c;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import com.google.android.gms.ads.formats.MediaView;

public final class c
  extends FrameLayout
{
  private View a;
  private View b;
  private View c;
  private View d;
  private MediaView e;
  private View f;
  private a g;
  
  private c(Context paramContext)
  {
    super(paramContext, null, 0);
  }
  
  public final View getBodyView()
  {
    return b;
  }
  
  public final View getCallToActionView()
  {
    return c;
  }
  
  public final View getHeadlineView()
  {
    return a;
  }
  
  public final View getIconView()
  {
    return d;
  }
  
  public final View getImageView()
  {
    return f;
  }
  
  public final MediaView getMediaView()
  {
    return e;
  }
  
  public final a getNativeAd()
  {
    return g;
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    a locala = g;
    if (locala != null)
    {
      locala.c();
      return;
    }
  }
  
  public final void setBodyView(View paramView)
  {
    b = paramView;
  }
  
  public final void setCallToActionView(View paramView)
  {
    c = paramView;
  }
  
  public final void setHeadlineView(View paramView)
  {
    a = paramView;
  }
  
  public final void setIconView(View paramView)
  {
    d = paramView;
  }
  
  public final void setImageView(View paramView)
  {
    f = paramView;
  }
  
  public final void setMediaView(MediaView paramMediaView)
  {
    e = paramMediaView;
  }
  
  public final void setNativeAd(a parama)
  {
    g = parama;
    Object localObject1 = new com/truecaller/ads/c/c$a;
    ((c.a)localObject1).<init>(parama);
    localObject1 = (View.OnClickListener)localObject1;
    setOnClickListener((View.OnClickListener)localObject1);
    localObject1 = a;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = new com/truecaller/ads/c/c$b;
      ((c.b)localObject2).<init>(parama);
      localObject2 = (View.OnClickListener)localObject2;
      ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      localObject2 = new com/truecaller/ads/c/c$c;
      ((c.c)localObject2).<init>(parama);
      localObject2 = (View.OnClickListener)localObject2;
      ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject2 = new com/truecaller/ads/c/c$d;
      ((c.d)localObject2).<init>(parama);
      localObject2 = (View.OnClickListener)localObject2;
      ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      localObject2 = new com/truecaller/ads/c/c$e;
      ((c.e)localObject2).<init>(parama);
      localObject2 = (View.OnClickListener)localObject2;
      ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    }
    localObject1 = f;
    if (localObject1 != null)
    {
      localObject2 = new com/truecaller/ads/c/c$f;
      ((c.f)localObject2).<init>(parama);
      localObject2 = (View.OnClickListener)localObject2;
      ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    }
    boolean bool = isAttachedToWindow();
    if (bool)
    {
      parama = g;
      if (parama != null)
      {
        parama.c();
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */