package com.truecaller.ads.c;

import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.truecaller.ads.provider.holders.AdHolderType;
import com.truecaller.ads.provider.holders.d;
import com.truecaller.ads.provider.holders.e;
import java.util.Set;

public final class a$a
{
  public static boolean a(e parame)
  {
    Object localObject;
    boolean bool1;
    if (parame != null)
    {
      localObject = parame.a();
    }
    else
    {
      bool1 = false;
      localObject = null;
    }
    AdHolderType localAdHolderType = AdHolderType.CUSTOM_AD;
    if (localObject == localAdHolderType)
    {
      bool1 = parame instanceof d;
      if (bool1)
      {
        localObject = b.a();
        parame = ((NativeCustomTemplateAd)((d)parame).g()).getCustomTemplateId();
        boolean bool2 = ((Set)localObject).contains(parame);
        if (bool2) {
          return true;
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.c.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */