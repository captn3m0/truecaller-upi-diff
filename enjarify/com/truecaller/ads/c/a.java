package com.truecaller.ads.c;

import c.a.m;
import c.g.b.k;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.truecaller.ads.provider.holders.d;
import com.truecaller.ads.provider.holders.e;
import java.util.List;

public final class a
{
  public static final a.a b;
  public final NativeCustomTemplateAd a;
  
  static
  {
    a.a locala = new com/truecaller/ads/c/a$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public a(d paramd)
  {
    NativeCustomTemplateAd localNativeCustomTemplateAd = (NativeCustomTemplateAd)paramd.g();
    a = localNativeCustomTemplateAd;
    paramd = (e)paramd;
    boolean bool = a.a.a(paramd);
    if (bool) {
      return;
    }
    paramd = new java/lang/IllegalArgumentException;
    paramd.<init>("AdHolder contains unsupported ad");
    throw ((Throwable)paramd);
  }
  
  public static final boolean a(e parame)
  {
    return a.a.a(parame);
  }
  
  public final VideoController a()
  {
    return a.getVideoController();
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "s");
    a.performClick(paramString);
  }
  
  public final List b()
  {
    return m.a(a.getImage("Image"));
  }
  
  public final void c()
  {
    a.recordImpression();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */