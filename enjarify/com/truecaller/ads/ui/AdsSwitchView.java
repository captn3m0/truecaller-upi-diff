package com.truecaller.ads.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import c.f;
import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import com.truecaller.ads.R.id;
import com.truecaller.ads.provider.holders.AdNativeHolder;
import com.truecaller.ads.provider.holders.d;
import com.truecaller.ads.provider.holders.e;

public final class AdsSwitchView
  extends FrameLayout
{
  private final f b;
  private final f c;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[2];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(AdsSwitchView.class);
    ((u)localObject).<init>(localb, "bannerPosition", "getBannerPosition()I");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/u;
    localb = w.a(AdsSwitchView.class);
    ((u)localObject).<init>(localb, "nativePosition", "getNativePosition()I");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[1] = localObject;
    a = arrayOfg;
  }
  
  public AdsSwitchView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private AdsSwitchView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    paramContext = new com/truecaller/ads/ui/AdsSwitchView$a;
    paramContext.<init>(this);
    paramContext = c.g.a((c.g.a.a)paramContext);
    b = paramContext;
    paramContext = new com/truecaller/ads/ui/AdsSwitchView$b;
    paramContext.<init>(this);
    paramContext = c.g.a((c.g.a.a)paramContext);
    c = paramContext;
    a(-1);
  }
  
  private final void a(int paramInt)
  {
    int i = getChildCount();
    int j = 0;
    while (j < i)
    {
      View localView = b(j);
      int k;
      if (j == paramInt) {
        k = 0;
      } else {
        k = 8;
      }
      localView.setVisibility(k);
      j += 1;
    }
  }
  
  private final View b(int paramInt)
  {
    View localView = getChildAt(paramInt);
    k.a(localView, "getChildAt(position)");
    return localView;
  }
  
  private final int getBannerPosition()
  {
    return ((Number)b.b()).intValue();
  }
  
  private final int getNativePosition()
  {
    return ((Number)c.b()).intValue();
  }
  
  public final void a(e parame, com.truecaller.ads.a parama)
  {
    k.b(parame, "ad");
    Object localObject = "layout";
    k.b(parama, (String)localObject);
    boolean bool1 = parame instanceof com.truecaller.ads.provider.holders.a;
    int n;
    if (bool1)
    {
      parame = (com.truecaller.ads.provider.holders.a)parame;
      int m = getBannerPosition();
      parama = b(m);
      int i = R.id.container;
      parama = (ViewGroup)parama.findViewById(i);
      parama.removeAllViews();
      parame = com.truecaller.ads.b.a(parame);
      parama.addView(parame);
      n = getBannerPosition();
      a(n);
      return;
    }
    boolean bool2 = parame instanceof AdNativeHolder;
    int i1;
    Context localContext;
    if (bool2)
    {
      parame = (AdNativeHolder)parame;
      int j = getNativePosition();
      localObject = b(j);
      i1 = R.id.container;
      localObject = (ViewGroup)((View)localObject).findViewById(i1);
      ((ViewGroup)localObject).removeAllViews();
      localContext = ((ViewGroup)localObject).getContext();
      k.a(localContext, "context");
      parame = com.truecaller.ads.b.a(parame, localContext, parama);
      ((ViewGroup)localObject).addView(parame);
      n = getNativePosition();
      a(n);
      return;
    }
    boolean bool3 = parame instanceof d;
    if (bool3)
    {
      parame = (d)parame;
      int k = getNativePosition();
      localObject = b(k);
      i1 = R.id.container;
      localObject = (ViewGroup)((View)localObject).findViewById(i1);
      ((ViewGroup)localObject).removeAllViews();
      localContext = ((ViewGroup)localObject).getContext();
      String str = "context";
      k.a(localContext, str);
      parame = com.truecaller.ads.b.a(parame, localContext, parama);
      ((ViewGroup)localObject).addView(parame);
      n = getNativePosition();
      a(n);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.ui.AdsSwitchView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */