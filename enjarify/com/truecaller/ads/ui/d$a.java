package com.truecaller.ads.ui;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.utils.extensions.t;

public final class d$a
  implements d
{
  public static final a a;
  
  static
  {
    a locala = new com/truecaller/ads/ui/d$a;
    locala.<init>();
    a = locala;
  }
  
  public final void a(ImageView paramImageView, TextView paramTextView)
  {
    k.b(paramImageView, "fallbackImage");
    k.b(paramTextView, "fallbackTextView");
    t.b((View)paramImageView);
    paramTextView.setText(2131886084);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.ui.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */