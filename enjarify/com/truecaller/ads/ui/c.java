package com.truecaller.ads.ui;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.R.id;
import com.truecaller.ads.a;
import com.truecaller.ads.campaigns.AdCampaign.CtaStyle;
import com.truecaller.ads.i;
import com.truecaller.ads.provider.b.e;
import com.truecaller.common.h.aq.d;
import java.util.HashMap;

public final class c
  extends FrameLayout
{
  private HashMap a;
  
  public c(Context paramContext, a parama)
  {
    super(paramContext);
    paramContext = LayoutInflater.from(paramContext);
    int i = parama.getHouseLayout();
    Object localObject = this;
    localObject = (ViewGroup)this;
    paramContext.inflate(i, (ViewGroup)localObject);
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      a = ((HashMap)localObject1);
    }
    localObject1 = a;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = a;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(e parame, AdCampaign.CtaStyle paramCtaStyle)
  {
    k.b(parame, "ad");
    Object localObject1 = new com/truecaller/ads/ui/c$b;
    ((c.b)localObject1).<init>(this, parame);
    localObject1 = (View.OnClickListener)localObject1;
    setOnClickListener((View.OnClickListener)localObject1);
    int i = R.id.adTitle;
    localObject1 = (TextView)a(i);
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = (CharSequence)a;
      ((TextView)localObject1).setText((CharSequence)localObject2);
    }
    i = R.id.adText;
    localObject1 = (TextView)a(i);
    if (localObject1 != null)
    {
      localObject2 = (CharSequence)b;
      ((TextView)localObject1).setText((CharSequence)localObject2);
    }
    i = R.id.adCtaText;
    localObject1 = (CtaButton)a(i);
    int j;
    int k;
    if (localObject1 != null)
    {
      localObject2 = (CharSequence)c;
      ((CtaButton)localObject1).setText((CharSequence)localObject2);
      if (paramCtaStyle != null)
      {
        j = a;
        k = b;
        ((CtaButton)localObject1).a(j, k);
      }
      localObject2 = new com/truecaller/ads/ui/c$a;
      ((c.a)localObject2).<init>((CtaButton)localObject1, parame, paramCtaStyle);
      localObject2 = (View.OnClickListener)localObject2;
      ((CtaButton)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    }
    paramCtaStyle = e;
    i = 0;
    localObject1 = null;
    int m;
    Object localObject3;
    if (paramCtaStyle != null)
    {
      m = R.id.adIcon;
      paramCtaStyle = (ImageView)a(m);
      if (paramCtaStyle != null)
      {
        i.b().d(paramCtaStyle);
        localObject2 = i.b();
        localObject3 = e;
        localObject2 = ((w)localObject2).a((String)localObject3);
        k = 2131165467;
        localObject2 = ((ab)localObject2).a(k, k).b();
        localObject3 = (ai)aq.d.b();
        localObject2 = ((ab)localObject2).a((ai)localObject3);
        ((ab)localObject2).a(paramCtaStyle, null);
      }
    }
    paramCtaStyle = f;
    if (paramCtaStyle != null)
    {
      m = R.id.adLargeGraphic;
      paramCtaStyle = (ImageView)a(m);
      if (paramCtaStyle != null)
      {
        i.b().d(paramCtaStyle);
        j = (int)paramCtaStyle.getResources().getDimension(2131165289);
        localObject3 = i.b();
        parame = f;
        ((w)localObject3).a(parame).b(0, j).a(paramCtaStyle, null);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.ui.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */