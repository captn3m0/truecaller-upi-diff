package com.truecaller.ads.ui;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v4.view.r;
import android.support.v7.widget.AppCompatButton;
import android.view.KeyEvent;
import android.view.KeyEvent.DispatcherState;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.truecaller.ads.CustomTemplate;
import com.truecaller.common.h.am;
import com.truecaller.log.AssertionUtil;

public final class a
  extends FrameLayout
{
  private final com.truecaller.ads.provider.holders.d a;
  private final ViewGroup b;
  private final VideoFrame c;
  private final ViewGroup d;
  private boolean e;
  
  private a(ViewGroup paramViewGroup, com.truecaller.ads.provider.holders.d paramd)
  {
    super((Context)localObject1);
    localObject1 = null;
    e = false;
    b = paramViewGroup;
    a = paramd;
    paramd = (NativeCustomTemplateAd)paramd.g();
    Object localObject2 = paramd.getCustomTemplateId();
    Object localObject3 = CLICK_TO_PLAY_VIDEOtemplateId;
    boolean bool1 = am.a((CharSequence)localObject2, (CharSequence)localObject3);
    if (bool1)
    {
      localObject2 = "CTAbuttoncolor";
      int i;
      try
      {
        localObject2 = paramd.getText((String)localObject2);
        localObject2 = ((CharSequence)localObject2).toString();
        i = Color.parseColor((String)localObject2);
      }
      catch (IllegalArgumentException localIllegalArgumentException1)
      {
        AssertionUtil.reportThrowableButNeverCrash(localIllegalArgumentException1);
        i = -16777216;
      }
      localObject3 = "CTAbuttontextcolor";
      int j;
      try
      {
        localObject3 = paramd.getText((String)localObject3);
        localObject3 = ((CharSequence)localObject3).toString();
        j = Color.parseColor((String)localObject3);
      }
      catch (IllegalArgumentException localIllegalArgumentException2)
      {
        AssertionUtil.reportThrowableButNeverCrash(localIllegalArgumentException2);
        j = -1;
      }
      CharSequence localCharSequence = paramd.getText("CTAtext");
      paramViewGroup = (ViewGroup)LayoutInflater.from(paramViewGroup.getContext()).inflate(2131558485, this, false);
      d = paramViewGroup;
      paramViewGroup = d;
      addView(paramViewGroup);
      paramViewGroup = (VideoFrame)d.findViewById(2131362647);
      c = paramViewGroup;
      paramViewGroup = c;
      MediaView localMediaView = paramd.getVideoMediaView();
      paramd = paramd.getVideoController();
      Object localObject4 = d.a.a;
      paramViewGroup.a(localMediaView, paramd, (d)localObject4);
      paramViewGroup = d.findViewById(2131362507);
      paramd = new com/truecaller/ads/ui/-$$Lambda$a$OhS_lk_tTzpc_ereO8XH7MEFCog;
      paramd.<init>(this);
      paramViewGroup.setOnClickListener(paramd);
      paramViewGroup = (AppCompatButton)d.findViewById(2131363779);
      paramd = new android/content/res/ColorStateList;
      boolean bool2 = true;
      localObject4 = new int[bool2][];
      int[] arrayOfInt = new int[0];
      localObject4[0] = arrayOfInt;
      arrayOfInt = new int[bool2];
      arrayOfInt[0] = i;
      paramd.<init>((int[][])localObject4, arrayOfInt);
      r.a(paramViewGroup, paramd);
      paramViewGroup.setTextColor(j);
      paramViewGroup.setText(localCharSequence);
      paramd = new com/truecaller/ads/ui/-$$Lambda$a$34y0_Mi4sJWnPLnLLUuQn0bD3zU;
      paramd.<init>(this);
      paramViewGroup.setOnClickListener(paramd);
      setFocusable(bool2);
      setFocusableInTouchMode(bool2);
      setClickable(bool2);
      b.setVisibility(0);
      return;
    }
    paramViewGroup = new java/lang/IllegalArgumentException;
    paramd = new java/lang/StringBuilder;
    paramd.<init>("Only ");
    localObject1 = CLICK_TO_PLAY_VIDEOtemplateId;
    paramd.append((String)localObject1);
    paramd.append(" template supported");
    paramd = paramd.toString();
    paramViewGroup.<init>(paramd);
    throw paramViewGroup;
  }
  
  private void a()
  {
    c.b();
    d.removeAllViews();
    b.removeView(this);
    b.setVisibility(8);
  }
  
  public static boolean a(Activity paramActivity, com.truecaller.ads.provider.holders.d paramd)
  {
    Object localObject = (NativeCustomTemplateAd)paramd.g();
    MediaView localMediaView = ((NativeCustomTemplateAd)localObject).getVideoMediaView();
    if (localMediaView == null)
    {
      paramActivity = new java/lang/StringBuilder;
      paramActivity.<init>("Video ad contain no MediaView, hasVideoContent=");
      boolean bool1 = ((NativeCustomTemplateAd)localObject).getVideoController().g();
      paramActivity.append(bool1);
      AssertionUtil.reportWeirdnessButNeverCrash(paramActivity.toString());
      return false;
    }
    int i = 2131362002;
    paramActivity = paramActivity.findViewById(i);
    if (paramActivity == null)
    {
      AssertionUtil.reportWeirdnessButNeverCrash("Can't find view with ID ads_view_video_frame ");
      return false;
    }
    boolean bool2 = paramActivity instanceof ViewGroup;
    if (!bool2)
    {
      AssertionUtil.reportWeirdnessButNeverCrash("Can't attach to View that is not ViewGroup");
      return false;
    }
    paramActivity = (ViewGroup)paramActivity;
    localObject = new com/truecaller/ads/ui/a;
    ((a)localObject).<init>(paramActivity, paramd);
    paramActivity.addView((View)localObject);
    ((a)localObject).requestFocus();
    return true;
  }
  
  public final boolean dispatchKeyEventPreIme(KeyEvent paramKeyEvent)
  {
    int i = paramKeyEvent.getKeyCode();
    int j = 4;
    if (i == j)
    {
      KeyEvent.DispatcherState localDispatcherState = getKeyDispatcherState();
      if (localDispatcherState != null)
      {
        j = paramKeyEvent.getAction();
        int k = 1;
        if (j == 0)
        {
          j = paramKeyEvent.getRepeatCount();
          if (j == 0)
          {
            localDispatcherState.startTracking(paramKeyEvent, this);
            return k;
          }
        }
        j = paramKeyEvent.getAction();
        if (j == k)
        {
          boolean bool2 = paramKeyEvent.isCanceled();
          if (!bool2)
          {
            boolean bool1 = localDispatcherState.isTracking(paramKeyEvent);
            if (bool1)
            {
              a();
              return k;
            }
          }
        }
      }
    }
    return super.dispatchKeyEventPreIme(paramKeyEvent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.ui.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */