package com.truecaller.ads.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import c.g.b.k;
import com.truecaller.ads.R.attr;
import com.truecaller.utils.ui.b;

public final class CtaButton
  extends AppCompatTextView
{
  private int b;
  
  public CtaButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    int i = b;
    if (i == 0)
    {
      paramContext = getContext();
      int j = R.attr.theme_actionColor;
      i = b.a(paramContext, j);
      paramAttributeSet = getContext();
      int k = R.attr.theme_textColorAccentedControl;
      j = b.a(paramAttributeSet, k);
      a(i, j);
    }
  }
  
  private final float a(int paramInt)
  {
    float f = paramInt;
    Object localObject = getContext();
    k.a(localObject, "context");
    localObject = ((Context)localObject).getResources();
    k.a(localObject, "context.resources");
    localObject = ((Resources)localObject).getDisplayMetrics();
    return TypedValue.applyDimension(1, f, (DisplayMetrics)localObject);
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    int i = b;
    if (paramInt1 != i)
    {
      i = 2;
      float f1 = a(i);
      int j = 1;
      float f2 = a(j);
      int k = Math.round(f2);
      j = Math.max(j, k);
      Context localContext = getContext();
      int m = R.attr.ad_outline;
      k = b.a(localContext, m);
      Object localObject = new android/graphics/drawable/GradientDrawable;
      ((GradientDrawable)localObject).<init>();
      ((GradientDrawable)localObject).setColor(paramInt1);
      ((GradientDrawable)localObject).setCornerRadius(f1);
      ((GradientDrawable)localObject).setStroke(j, k);
      localObject = (Drawable)localObject;
      setBackground((Drawable)localObject);
      b = paramInt1;
      invalidate();
    }
    setTextColor(paramInt2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.ui.CtaButton
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */