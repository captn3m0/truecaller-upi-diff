package com.truecaller.ads.ui;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.r;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import c.f;
import c.g.a.a;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.truecaller.ads.CustomTemplate;
import com.truecaller.common.h.am;
import com.truecaller.log.AssertionUtil;
import com.truecaller.utils.extensions.t.a;

public final class CustomNativeVideoAdActivity
  extends AppCompatActivity
{
  public static final CustomNativeVideoAdActivity.a b;
  private static com.truecaller.ads.provider.holders.d e;
  private final f c;
  private boolean d;
  
  static
  {
    Object localObject1 = new c.l.g[1];
    Object localObject2 = new c/g/b/u;
    b localb = w.a(CustomNativeVideoAdActivity.class);
    ((u)localObject2).<init>(localb, "mVideoFrame", "getMVideoFrame()Lcom/truecaller/ads/ui/VideoFrame;");
    localObject2 = (c.l.g)w.a((t)localObject2);
    localObject1[0] = localObject2;
    a = (c.l.g[])localObject1;
    localObject1 = new com/truecaller/ads/ui/CustomNativeVideoAdActivity$a;
    ((CustomNativeVideoAdActivity.a)localObject1).<init>((byte)0);
    b = (CustomNativeVideoAdActivity.a)localObject1;
  }
  
  public CustomNativeVideoAdActivity()
  {
    c.g.b.k.b(this, "receiver$0");
    Object localObject1 = c.k.c;
    Object localObject2 = new com/truecaller/utils/extensions/t$a;
    ((t.a)localObject2).<init>(this);
    localObject2 = (a)localObject2;
    localObject1 = c.g.a((c.k)localObject1, (a)localObject2);
    c = ((f)localObject1);
  }
  
  private final VideoFrame a()
  {
    return (VideoFrame)c.b();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = 2131558485;
    setContentView(i);
    paramBundle = e;
    if (paramBundle == null)
    {
      AssertionUtil.reportWeirdnessButNeverCrash("Last holder not set");
      finish();
      return;
    }
    paramBundle = (NativeCustomTemplateAd)paramBundle.g();
    Object localObject1 = (CharSequence)paramBundle.getCustomTemplateId();
    Object localObject2 = (CharSequence)CLICK_TO_PLAY_VIDEOtemplateId;
    boolean bool = am.a((CharSequence)localObject1, (CharSequence)localObject2);
    if (bool)
    {
      localObject1 = "CTAbuttoncolor";
      int j;
      try
      {
        localObject1 = paramBundle.getText((String)localObject1);
        localObject1 = localObject1.toString();
        j = Color.parseColor((String)localObject1);
      }
      catch (IllegalArgumentException localIllegalArgumentException1)
      {
        localObject1 = (Throwable)localIllegalArgumentException1;
        AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
        j = -16777216;
      }
      localObject2 = "CTAbuttontextcolor";
      int k;
      try
      {
        localObject2 = paramBundle.getText((String)localObject2);
        localObject2 = localObject2.toString();
        k = Color.parseColor((String)localObject2);
      }
      catch (IllegalArgumentException localIllegalArgumentException2)
      {
        localObject2 = (Throwable)localIllegalArgumentException2;
        AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject2);
        k = -1;
      }
      CharSequence localCharSequence = paramBundle.getText("CTAtext");
      Object localObject3 = a();
      Object localObject4 = paramBundle.getVideoMediaView();
      paramBundle = paramBundle.getVideoController();
      Object localObject5 = (d)d.a.a;
      ((VideoFrame)localObject3).a((MediaView)localObject4, paramBundle, (d)localObject5);
      paramBundle = findViewById(2131362507);
      localObject3 = new com/truecaller/ads/ui/CustomNativeVideoAdActivity$b;
      ((CustomNativeVideoAdActivity.b)localObject3).<init>(this);
      localObject3 = (View.OnClickListener)localObject3;
      paramBundle.setOnClickListener((View.OnClickListener)localObject3);
      paramBundle = (AppCompatButton)findViewById(2131363779);
      localObject3 = paramBundle;
      localObject3 = (View)paramBundle;
      localObject4 = new android/content/res/ColorStateList;
      int m = 1;
      int[][] arrayOfInt = new int[m][];
      int[] arrayOfInt1 = new int[0];
      arrayOfInt[0] = arrayOfInt1;
      arrayOfInt = (int[][])arrayOfInt;
      localObject5 = new int[m];
      localObject5[0] = j;
      ((ColorStateList)localObject4).<init>(arrayOfInt, (int[])localObject5);
      r.a((View)localObject3, (ColorStateList)localObject4);
      paramBundle.setTextColor(k);
      c.g.b.k.a(paramBundle, "button");
      paramBundle.setText(localCharSequence);
      localObject1 = new com/truecaller/ads/ui/CustomNativeVideoAdActivity$c;
      ((CustomNativeVideoAdActivity.c)localObject1).<init>(this);
      localObject1 = (View.OnClickListener)localObject1;
      paramBundle.setOnClickListener((View.OnClickListener)localObject1);
      return;
    }
    paramBundle = new java/lang/IllegalArgumentException;
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("Only ");
    localObject2 = CLICK_TO_PLAY_VIDEOtemplateId;
    ((StringBuilder)localObject1).append((String)localObject2);
    ((StringBuilder)localObject1).append(" template supported");
    localObject1 = ((StringBuilder)localObject1).toString();
    paramBundle.<init>((String)localObject1);
    throw ((Throwable)paramBundle);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    e = null;
    a().b();
  }
  
  public final void onPause()
  {
    super.onPause();
    finish();
  }
  
  public final void onWindowFocusChanged(boolean paramBoolean)
  {
    super.onWindowFocusChanged(paramBoolean);
    if (paramBoolean)
    {
      Object localObject = getWindow();
      c.g.b.k.a(localObject, "window");
      localObject = ((Window)localObject).getDecorView();
      String str = "decorView";
      c.g.b.k.a(localObject, str);
      ((View)localObject).setSystemUiVisibility(1798);
      int i = ((View)localObject).getSystemUiVisibility() | 0x1000;
      ((View)localObject).setSystemUiVisibility(i);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.ui.CustomNativeVideoAdActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */