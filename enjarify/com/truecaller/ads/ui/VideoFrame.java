package com.truecaller.ads.ui;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.g.b.k;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoController.VideoLifecycleCallbacks;
import com.google.android.gms.ads.formats.MediaView;
import com.truecaller.R.id;
import com.truecaller.utils.extensions.c;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class VideoFrame
  extends ConstraintLayout
{
  private VideoController k;
  private final VideoFrame.a l;
  private HashMap m;
  
  public VideoFrame(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private VideoFrame(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, (byte)0);
    paramAttributeSet = this;
    paramAttributeSet = (ViewGroup)this;
    View.inflate(paramContext, 2131559145, paramAttributeSet);
    int i = R.id.adVideoPlayPause;
    paramContext = (ImageView)c(i);
    paramAttributeSet = new com/truecaller/ads/ui/VideoFrame$1;
    Object localObject1 = this;
    localObject1 = (VideoFrame)this;
    paramAttributeSet.<init>((VideoFrame)localObject1);
    paramAttributeSet = (c.g.a.b)paramAttributeSet;
    Object localObject2 = new com/truecaller/ads/ui/e;
    ((e)localObject2).<init>(paramAttributeSet);
    localObject2 = (View.OnClickListener)localObject2;
    paramContext.setOnClickListener((View.OnClickListener)localObject2);
    i = R.id.adVideoMuteUnmute;
    paramContext = (ImageView)c(i);
    paramAttributeSet = new com/truecaller/ads/ui/VideoFrame$2;
    paramAttributeSet.<init>((VideoFrame)localObject1);
    paramAttributeSet = (c.g.a.b)paramAttributeSet;
    localObject1 = new com/truecaller/ads/ui/e;
    ((e)localObject1).<init>(paramAttributeSet);
    localObject1 = (View.OnClickListener)localObject1;
    paramContext.setOnClickListener((View.OnClickListener)localObject1);
    paramContext = new com/truecaller/ads/ui/VideoFrame$a;
    paramContext.<init>(this);
    l = paramContext;
  }
  
  private final void a(MediaView paramMediaView, VideoController paramVideoController)
  {
    int i = R.id.adVideo;
    Object localObject1 = (FrameLayout)c(i);
    k.a(localObject1, "adVideo");
    t.a((View)localObject1);
    i = R.id.adVideoControls;
    localObject1 = (LinearLayout)c(i);
    k.a(localObject1, "adVideoControls");
    t.a((View)localObject1);
    i = R.id.adFallbackImage;
    localObject1 = (ImageView)c(i);
    k.a(localObject1, "adFallbackImage");
    t.b((View)localObject1);
    i = R.id.adFallbackText;
    localObject1 = (TextView)c(i);
    Object localObject2 = "adFallbackText";
    k.a(localObject1, (String)localObject2);
    t.b((View)localObject1);
    localObject1 = paramMediaView.getParent();
    boolean bool = localObject1 instanceof ViewGroup;
    if (!bool)
    {
      i = 0;
      localObject1 = null;
    }
    localObject1 = (ViewGroup)localObject1;
    if (localObject1 != null)
    {
      localObject2 = paramMediaView;
      localObject2 = (View)paramMediaView;
      ((ViewGroup)localObject1).removeView((View)localObject2);
    }
    i = R.id.adVideo;
    localObject1 = (FrameLayout)c(i);
    paramMediaView = (View)paramMediaView;
    ((FrameLayout)localObject1).addView(paramMediaView);
    k = paramVideoController;
    paramMediaView = (VideoController.VideoLifecycleCallbacks)l;
    paramVideoController.a(paramMediaView);
  }
  
  private View c(int paramInt)
  {
    Object localObject1 = m;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      m = ((HashMap)localObject1);
    }
    localObject1 = m;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = m;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  private final void c()
  {
    int i = R.id.adVideoControls;
    Object localObject1 = (LinearLayout)c(i);
    k.a(localObject1, "adVideoControls");
    localObject1 = (View)localObject1;
    Object localObject2 = k;
    if (localObject2 != null)
    {
      bool2 = ((VideoController)localObject2).f();
      localObject2 = Boolean.valueOf(bool2);
    }
    else
    {
      bool2 = false;
      localObject2 = null;
    }
    boolean bool2 = c.a((Boolean)localObject2);
    t.a((View)localObject1, bool2);
    localObject1 = k;
    if (localObject1 != null)
    {
      int n = R.id.adVideoPlayPause;
      localObject2 = (ImageView)c(n);
      int i1 = ((VideoController)localObject1).e();
      int i2 = 1;
      if (i1 == i2) {
        i1 = 2131234362;
      } else {
        i1 = 2131234384;
      }
      ((ImageView)localObject2).setImageResource(i1);
      n = R.id.adVideoMuteUnmute;
      localObject2 = (ImageView)c(n);
      boolean bool1 = ((VideoController)localObject1).d();
      int j;
      if (bool1) {
        j = 2131234294;
      } else {
        j = 2131234643;
      }
      ((ImageView)localObject2).setImageResource(j);
      return;
    }
  }
  
  private final void setupFallback(d paramd)
  {
    int i = R.id.adVideo;
    Object localObject1 = (FrameLayout)c(i);
    k.a(localObject1, "adVideo");
    t.b((View)localObject1);
    i = R.id.adVideoControls;
    localObject1 = (LinearLayout)c(i);
    k.a(localObject1, "adVideoControls");
    t.b((View)localObject1);
    i = R.id.adFallbackImage;
    localObject1 = (ImageView)c(i);
    k.a(localObject1, "adFallbackImage");
    t.a((View)localObject1);
    i = R.id.adFallbackText;
    localObject1 = (TextView)c(i);
    Object localObject2 = "adFallbackText";
    k.a(localObject1, (String)localObject2);
    localObject1 = (View)localObject1;
    t.a((View)localObject1);
    if (paramd != null)
    {
      i = R.id.adFallbackImage;
      localObject1 = (ImageView)c(i);
      k.a(localObject1, "adFallbackImage");
      int j = R.id.adFallbackText;
      localObject2 = (TextView)c(j);
      k.a(localObject2, "adFallbackText");
      paramd.a((ImageView)localObject1, (TextView)localObject2);
      return;
    }
  }
  
  public final boolean a(MediaView paramMediaView, VideoController paramVideoController, d paramd)
  {
    b();
    if (paramVideoController != null)
    {
      Boolean localBoolean = Boolean.valueOf(paramVideoController.g());
      boolean bool = c.a(localBoolean);
      if ((bool) && (paramMediaView != null))
      {
        a(paramMediaView, paramVideoController);
        c();
        return true;
      }
    }
    setupFallback(paramd);
    return false;
  }
  
  public final void b()
  {
    int i = R.id.adVideo;
    ((FrameLayout)c(i)).removeAllViews();
    VideoController localVideoController = k;
    if (localVideoController != null)
    {
      VideoController.VideoLifecycleCallbacks localVideoLifecycleCallbacks = (VideoController.VideoLifecycleCallbacks)b.a;
      localVideoController.a(localVideoLifecycleCallbacks);
    }
    k = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.ui.VideoFrame
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */