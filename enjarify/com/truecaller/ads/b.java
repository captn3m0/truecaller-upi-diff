package com.truecaller.ads;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;
import c.a.m;
import c.g.b.k;
import c.u;
import com.d.b.ab;
import com.d.b.w;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeAd.Image;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeAppInstallAdView;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeContentAdView;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.truecaller.ads.campaigns.AdCampaign.CtaStyle;
import com.truecaller.ads.provider.holders.AdNativeHolder;
import com.truecaller.ads.provider.holders.AdNativeHolder.Type;
import com.truecaller.ads.provider.holders.d;
import com.truecaller.ads.ui.CtaButton;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import java.util.List;

public final class b
{
  public static final View a(AdNativeHolder paramAdNativeHolder, Context paramContext, a parama)
  {
    k.b(paramAdNativeHolder, "receiver$0");
    k.b(paramContext, "context");
    k.b(parama, "layout");
    AdNativeHolder.Type localType = paramAdNativeHolder.i();
    int[] arrayOfInt = c.a;
    int i = localType.ordinal();
    i = arrayOfInt[i];
    switch (i)
    {
    default: 
      paramAdNativeHolder = new c/l;
      paramAdNativeHolder.<init>();
      throw paramAdNativeHolder;
    case 2: 
      paramContext = b(parama, paramContext);
      parama = paramAdNativeHolder.k();
      if (parama != null)
      {
        parama = (NativeAppInstallAd)parama;
        paramAdNativeHolder = a.d;
        a(paramContext, parama, paramAdNativeHolder);
        return (View)paramContext;
      }
      paramAdNativeHolder = new c/u;
      paramAdNativeHolder.<init>("null cannot be cast to non-null type com.google.android.gms.ads.formats.NativeAppInstallAd");
      throw paramAdNativeHolder;
    }
    paramContext = a(parama, paramContext);
    parama = paramAdNativeHolder.k();
    if (parama != null)
    {
      parama = (NativeContentAd)parama;
      paramAdNativeHolder = a.d;
      a(paramContext, parama, paramAdNativeHolder);
      return (View)paramContext;
    }
    paramAdNativeHolder = new c/u;
    paramAdNativeHolder.<init>("null cannot be cast to non-null type com.google.android.gms.ads.formats.NativeContentAd");
    throw paramAdNativeHolder;
  }
  
  public static final View a(d paramd, Context paramContext, a parama)
  {
    k.b(paramd, "receiver$0");
    k.b(paramContext, "context");
    k.b(parama, "layout");
    Object localObject = CustomTemplate.Companion;
    localObject = CustomTemplate.a.a(((NativeCustomTemplateAd)paramd.g()).getCustomTemplateId());
    if (localObject != null)
    {
      int[] arrayOfInt = c.b;
      int i = ((CustomTemplate)localObject).ordinal();
      int j = arrayOfInt[i];
      i = 1;
      if (j == i) {}
    }
    else
    {
      paramd = new java/lang/IllegalArgumentException;
      paramContext = new java/lang/StringBuilder;
      parama = "Custom holder inflation not supported for ";
      paramContext.<init>(parama);
      if (localObject != null) {
        parama = templateId;
      } else {
        parama = null;
      }
      paramContext.append(parama);
      paramContext = paramContext.toString();
      paramd.<init>(paramContext);
      throw ((Throwable)paramd);
    }
    paramContext = c(parama, paramContext);
    parama = com.truecaller.ads.c.b.a(paramd);
    paramd = a.d;
    a(paramContext, parama, paramd);
    return (View)paramContext;
  }
  
  public static final NativeContentAdView a(a parama, Context paramContext)
  {
    k.b(parama, "receiver$0");
    k.b(paramContext, "context");
    NativeContentAdView localNativeContentAdView = new com/google/android/gms/ads/formats/NativeContentAdView;
    localNativeContentAdView.<init>(paramContext);
    paramContext = LayoutInflater.from(paramContext);
    int i = parama.getNativeLayout();
    Object localObject = localNativeContentAdView;
    localObject = (ViewGroup)localNativeContentAdView;
    paramContext.inflate(i, (ViewGroup)localObject);
    i = R.id.adTitle;
    parama = localNativeContentAdView.findViewById(i);
    localNativeContentAdView.setHeadlineView(parama);
    i = R.id.adText;
    parama = localNativeContentAdView.findViewById(i);
    localNativeContentAdView.setBodyView(parama);
    i = R.id.adCtaText;
    parama = localNativeContentAdView.findViewById(i);
    localNativeContentAdView.setCallToActionView(parama);
    i = R.id.adIcon;
    parama = localNativeContentAdView.findViewById(i);
    localNativeContentAdView.setLogoView(parama);
    i = R.id.adMainMedia;
    parama = (MediaView)localNativeContentAdView.findViewById(i);
    localNativeContentAdView.setMediaView(parama);
    return localNativeContentAdView;
  }
  
  private static final void a(ImageView paramImageView, NativeAd.Image paramImage, c.g.a.b paramb)
  {
    i.b().d(paramImageView);
    paramImageView.setImageDrawable(null);
    if (paramImage != null)
    {
      Object localObject = paramImage.getDrawable();
      if (localObject != null)
      {
        paramImage = paramImage.getDrawable();
        paramImageView.setImageDrawable(paramImage);
        return;
      }
      localObject = paramImage.getUri();
      if (localObject != null)
      {
        localObject = i.b();
        paramImage = paramImage.getUri();
        paramImage = ((w)localObject).a(paramImage);
        paramb.invoke(paramImage);
        paramImage.a(paramImageView, null);
      }
    }
  }
  
  private static final void a(TextView paramTextView1, TextView paramTextView2, CtaButton paramCtaButton, ImageView paramImageView, CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, NativeAd.Image paramImage, AdCampaign.CtaStyle paramCtaStyle)
  {
    boolean bool1 = true;
    String[] arrayOfString1 = null;
    boolean bool2;
    if (paramTextView1 != null) {
      bool2 = true;
    } else {
      bool2 = false;
    }
    String[] arrayOfString2 = new String[0];
    AssertionUtil.OnlyInDebug.isTrue(bool2, arrayOfString2);
    if (paramTextView2 != null) {
      bool2 = true;
    } else {
      bool2 = false;
    }
    arrayOfString2 = new String[0];
    AssertionUtil.OnlyInDebug.isTrue(bool2, arrayOfString2);
    if (paramCtaButton != null) {
      bool2 = true;
    } else {
      bool2 = false;
    }
    arrayOfString2 = new String[0];
    AssertionUtil.OnlyInDebug.isTrue(bool2, arrayOfString2);
    if (paramImageView != null) {
      bool2 = true;
    } else {
      bool2 = false;
    }
    arrayOfString2 = new String[0];
    AssertionUtil.OnlyInDebug.isTrue(bool2, arrayOfString2);
    if (paramCharSequence1 != null) {
      bool2 = true;
    } else {
      bool2 = false;
    }
    arrayOfString2 = new String[0];
    AssertionUtil.OnlyInDebug.isTrue(bool2, arrayOfString2);
    if (paramCharSequence2 != null) {
      bool2 = true;
    } else {
      bool2 = false;
    }
    arrayOfString2 = new String[0];
    AssertionUtil.OnlyInDebug.isTrue(bool2, arrayOfString2);
    if (paramCharSequence3 == null) {
      bool1 = false;
    }
    arrayOfString1 = new String[0];
    AssertionUtil.OnlyInDebug.isTrue(bool1, arrayOfString1);
    if (paramTextView1 != null)
    {
      paramCharSequence1 = (CharSequence)com.truecaller.common.h.l.a(paramCharSequence1);
      paramTextView1.setText(paramCharSequence1);
    }
    if (paramTextView2 != null)
    {
      paramTextView1 = (CharSequence)com.truecaller.common.h.l.a(paramCharSequence2);
      paramTextView2.setText(paramTextView1);
    }
    if (paramCtaButton != null)
    {
      paramTextView1 = (CharSequence)com.truecaller.common.h.l.a(paramCharSequence3);
      paramCtaButton.setText(paramTextView1);
    }
    if ((paramCtaStyle != null) && (paramCtaButton != null))
    {
      int i = a;
      int j = b;
      paramCtaButton.a(i, j);
    }
    if (paramImageView != null)
    {
      paramTextView1 = (c.g.a.b)b.a.a;
      a(paramImageView, paramImage, paramTextView1);
      return;
    }
  }
  
  private static void a(MediaView paramMediaView, VideoController paramVideoController, NativeAd.Image paramImage)
  {
    k.b(paramMediaView, "receiver$0");
    Object localObject1 = paramMediaView.getParent();
    boolean bool = localObject1 instanceof ConstraintLayout;
    if (!bool) {
      localObject1 = null;
    }
    Object localObject2 = localObject1;
    localObject2 = (ConstraintLayout)localObject1;
    if (localObject2 == null) {
      return;
    }
    float f1;
    float f2;
    if (paramVideoController != null)
    {
      f1 = paramVideoController.h();
      f2 = f1;
    }
    else
    {
      f1 = 0.0F;
      paramVideoController = null;
      f2 = 0.0F;
    }
    paramVideoController = paramMediaView;
    paramVideoController = (View)paramMediaView;
    localObject1 = new com/truecaller/ads/b$b;
    ((b.b)localObject1).<init>(paramVideoController, paramMediaView, paramImage, (ConstraintLayout)localObject2, f2);
    paramMediaView = paramVideoController.getViewTreeObserver();
    localObject1 = (ViewTreeObserver.OnGlobalLayoutListener)localObject1;
    paramMediaView.addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)localObject1);
  }
  
  public static final void a(NativeAppInstallAdView paramNativeAppInstallAdView, NativeAppInstallAd paramNativeAppInstallAd, AdCampaign.CtaStyle paramCtaStyle)
  {
    k.b(paramNativeAppInstallAdView, "receiver$0");
    Object localObject1 = "ad";
    k.b(paramNativeAppInstallAd, (String)localObject1);
    try
    {
      localObject1 = paramNativeAppInstallAdView.getHeadlineView();
      boolean bool = localObject1 instanceof TextView;
      Object localObject2 = null;
      if (!bool) {
        localObject1 = null;
      }
      Object localObject3 = localObject1;
      localObject3 = (TextView)localObject1;
      localObject1 = paramNativeAppInstallAdView.getBodyView();
      bool = localObject1 instanceof TextView;
      if (!bool) {
        localObject1 = null;
      }
      Object localObject4 = localObject1;
      localObject4 = (TextView)localObject1;
      localObject1 = paramNativeAppInstallAdView.getCallToActionView();
      bool = localObject1 instanceof CtaButton;
      if (!bool) {
        localObject1 = null;
      }
      Object localObject5 = localObject1;
      localObject5 = (CtaButton)localObject1;
      localObject1 = paramNativeAppInstallAdView.getIconView();
      bool = localObject1 instanceof ImageView;
      if (!bool) {
        localObject1 = null;
      }
      Object localObject6 = localObject1;
      localObject6 = (ImageView)localObject1;
      CharSequence localCharSequence1 = paramNativeAppInstallAd.getHeadline();
      CharSequence localCharSequence2 = paramNativeAppInstallAd.getBody();
      CharSequence localCharSequence3 = paramNativeAppInstallAd.getCallToAction();
      NativeAd.Image localImage = paramNativeAppInstallAd.getIcon();
      a((TextView)localObject3, (TextView)localObject4, (CtaButton)localObject5, (ImageView)localObject6, localCharSequence1, localCharSequence2, localCharSequence3, localImage, paramCtaStyle);
      paramCtaStyle = paramNativeAppInstallAdView.getMediaView();
      if (paramCtaStyle != null)
      {
        localObject1 = paramNativeAppInstallAd.getVideoController();
        Object localObject7 = paramNativeAppInstallAd.getImages();
        if (localObject7 != null)
        {
          localObject7 = m.e((List)localObject7);
          localObject2 = localObject7;
          localObject2 = (NativeAd.Image)localObject7;
        }
        a(paramCtaStyle, (VideoController)localObject1, (NativeAd.Image)localObject2);
      }
      paramNativeAppInstallAd = (NativeAd)paramNativeAppInstallAd;
      paramNativeAppInstallAdView.setNativeAd(paramNativeAppInstallAd);
      return;
    }
    catch (NullPointerException localNullPointerException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localNullPointerException);
    }
  }
  
  public static final void a(NativeContentAdView paramNativeContentAdView, NativeContentAd paramNativeContentAd, AdCampaign.CtaStyle paramCtaStyle)
  {
    k.b(paramNativeContentAdView, "receiver$0");
    Object localObject1 = "ad";
    k.b(paramNativeContentAd, (String)localObject1);
    try
    {
      localObject1 = paramNativeContentAdView.getHeadlineView();
      boolean bool = localObject1 instanceof TextView;
      Object localObject2 = null;
      if (!bool) {
        localObject1 = null;
      }
      Object localObject3 = localObject1;
      localObject3 = (TextView)localObject1;
      localObject1 = paramNativeContentAdView.getBodyView();
      bool = localObject1 instanceof TextView;
      if (!bool) {
        localObject1 = null;
      }
      Object localObject4 = localObject1;
      localObject4 = (TextView)localObject1;
      localObject1 = paramNativeContentAdView.getCallToActionView();
      bool = localObject1 instanceof CtaButton;
      if (!bool) {
        localObject1 = null;
      }
      Object localObject5 = localObject1;
      localObject5 = (CtaButton)localObject1;
      localObject1 = paramNativeContentAdView.getLogoView();
      bool = localObject1 instanceof ImageView;
      if (!bool) {
        localObject1 = null;
      }
      Object localObject6 = localObject1;
      localObject6 = (ImageView)localObject1;
      CharSequence localCharSequence1 = paramNativeContentAd.getHeadline();
      CharSequence localCharSequence2 = paramNativeContentAd.getBody();
      CharSequence localCharSequence3 = paramNativeContentAd.getCallToAction();
      NativeAd.Image localImage = paramNativeContentAd.getLogo();
      a((TextView)localObject3, (TextView)localObject4, (CtaButton)localObject5, (ImageView)localObject6, localCharSequence1, localCharSequence2, localCharSequence3, localImage, paramCtaStyle);
      paramCtaStyle = paramNativeContentAdView.getMediaView();
      if (paramCtaStyle != null)
      {
        localObject1 = paramNativeContentAd.getVideoController();
        Object localObject7 = paramNativeContentAd.getImages();
        if (localObject7 != null)
        {
          localObject7 = m.e((List)localObject7);
          localObject2 = localObject7;
          localObject2 = (NativeAd.Image)localObject7;
        }
        a(paramCtaStyle, (VideoController)localObject1, (NativeAd.Image)localObject2);
      }
      paramNativeContentAd = (NativeAd)paramNativeContentAd;
      paramNativeContentAdView.setNativeAd(paramNativeContentAd);
      return;
    }
    catch (NullPointerException localNullPointerException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localNullPointerException);
    }
  }
  
  public static final void a(com.truecaller.ads.c.c paramc, com.truecaller.ads.c.a parama, AdCampaign.CtaStyle paramCtaStyle)
  {
    k.b(paramc, "receiver$0");
    Object localObject1 = "ad";
    k.b(parama, (String)localObject1);
    try
    {
      localObject1 = paramc.getHeadlineView();
      boolean bool1 = localObject1 instanceof TextView;
      Object localObject2 = null;
      if (!bool1) {
        localObject1 = null;
      }
      Object localObject3 = localObject1;
      localObject3 = (TextView)localObject1;
      localObject1 = paramc.getBodyView();
      bool1 = localObject1 instanceof TextView;
      if (!bool1) {
        localObject1 = null;
      }
      Object localObject4 = localObject1;
      localObject4 = (TextView)localObject1;
      localObject1 = paramc.getCallToActionView();
      bool1 = localObject1 instanceof CtaButton;
      if (!bool1) {
        localObject1 = null;
      }
      Object localObject5 = localObject1;
      localObject5 = (CtaButton)localObject1;
      localObject1 = paramc.getIconView();
      bool1 = localObject1 instanceof ImageView;
      if (!bool1) {
        localObject1 = null;
      }
      Object localObject6 = localObject1;
      localObject6 = (ImageView)localObject1;
      localObject1 = a;
      Object localObject7 = "Headline";
      CharSequence localCharSequence1 = ((NativeCustomTemplateAd)localObject1).getText((String)localObject7);
      localObject1 = a;
      localObject7 = "Body";
      CharSequence localCharSequence2 = ((NativeCustomTemplateAd)localObject1).getText((String)localObject7);
      localObject1 = a;
      localObject7 = "Calltoaction";
      CharSequence localCharSequence3 = ((NativeCustomTemplateAd)localObject1).getText((String)localObject7);
      localObject1 = a;
      localObject7 = "Secondaryimage";
      NativeAd.Image localImage = ((NativeCustomTemplateAd)localObject1).getImage((String)localObject7);
      a((TextView)localObject3, (TextView)localObject4, (CtaButton)localObject5, (ImageView)localObject6, localCharSequence1, localCharSequence2, localCharSequence3, localImage, paramCtaStyle);
      paramCtaStyle = parama.a();
      if (paramCtaStyle != null)
      {
        bool2 = paramCtaStyle.g();
        paramCtaStyle = Boolean.valueOf(bool2);
      }
      else
      {
        bool2 = false;
        paramCtaStyle = null;
      }
      boolean bool2 = com.truecaller.utils.extensions.c.a(paramCtaStyle);
      if (!bool2)
      {
        paramCtaStyle = new android/widget/ImageView;
        localObject1 = paramc.getContext();
        paramCtaStyle.<init>((Context)localObject1);
        localObject1 = paramc.getMediaView();
        if (localObject1 != null)
        {
          localObject7 = paramCtaStyle;
          localObject7 = (View)paramCtaStyle;
          localObject3 = new android/widget/FrameLayout$LayoutParams;
          int i = -1;
          ((FrameLayout.LayoutParams)localObject3).<init>(i, i);
          localObject3 = (ViewGroup.LayoutParams)localObject3;
          ((MediaView)localObject1).addView((View)localObject7, (ViewGroup.LayoutParams)localObject3);
        }
        localObject1 = ImageView.ScaleType.FIT_CENTER;
        paramCtaStyle.setScaleType((ImageView.ScaleType)localObject1);
        localObject1 = parama.b();
        if (localObject1 != null)
        {
          localObject1 = m.e((List)localObject1);
          localObject1 = (NativeAd.Image)localObject1;
        }
        else
        {
          localObject1 = null;
        }
        localObject7 = b.c.a;
        localObject7 = (c.g.a.b)localObject7;
        a(paramCtaStyle, (NativeAd.Image)localObject1, (c.g.a.b)localObject7);
      }
      paramCtaStyle = paramc.getMediaView();
      if (paramCtaStyle != null)
      {
        localObject1 = parama.a();
        localObject7 = parama.b();
        if (localObject7 != null)
        {
          localObject7 = m.e((List)localObject7);
          localObject2 = localObject7;
          localObject2 = (NativeAd.Image)localObject7;
        }
        a(paramCtaStyle, (VideoController)localObject1, (NativeAd.Image)localObject2);
      }
      paramc.setNativeAd(parama);
      return;
    }
    catch (NullPointerException localNullPointerException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localNullPointerException);
    }
  }
  
  public static final NativeAppInstallAdView b(a parama, Context paramContext)
  {
    k.b(parama, "receiver$0");
    k.b(paramContext, "context");
    NativeAppInstallAdView localNativeAppInstallAdView = new com/google/android/gms/ads/formats/NativeAppInstallAdView;
    localNativeAppInstallAdView.<init>(paramContext);
    paramContext = LayoutInflater.from(paramContext);
    int i = parama.getNativeLayout();
    Object localObject = localNativeAppInstallAdView;
    localObject = (ViewGroup)localNativeAppInstallAdView;
    paramContext.inflate(i, (ViewGroup)localObject);
    i = R.id.adTitle;
    parama = localNativeAppInstallAdView.findViewById(i);
    localNativeAppInstallAdView.setHeadlineView(parama);
    i = R.id.adText;
    parama = localNativeAppInstallAdView.findViewById(i);
    localNativeAppInstallAdView.setBodyView(parama);
    i = R.id.adCtaText;
    parama = localNativeAppInstallAdView.findViewById(i);
    localNativeAppInstallAdView.setCallToActionView(parama);
    i = R.id.adIcon;
    parama = localNativeAppInstallAdView.findViewById(i);
    localNativeAppInstallAdView.setIconView(parama);
    i = R.id.adMainMedia;
    parama = (MediaView)localNativeAppInstallAdView.findViewById(i);
    localNativeAppInstallAdView.setMediaView(parama);
    return localNativeAppInstallAdView;
  }
  
  public static final com.truecaller.ads.c.c c(a parama, Context paramContext)
  {
    k.b(parama, "receiver$0");
    k.b(paramContext, "context");
    com.truecaller.ads.c.c localc = new com/truecaller/ads/c/c;
    localc.<init>(paramContext, (byte)0);
    paramContext = LayoutInflater.from(paramContext);
    int i = parama.getNativeLayout();
    Object localObject = localc;
    localObject = (ViewGroup)localc;
    paramContext.inflate(i, (ViewGroup)localObject);
    i = R.id.adTitle;
    parama = localc.findViewById(i);
    localc.setHeadlineView(parama);
    i = R.id.adText;
    parama = localc.findViewById(i);
    localc.setBodyView(parama);
    i = R.id.adCtaText;
    parama = localc.findViewById(i);
    localc.setCallToActionView(parama);
    i = R.id.adIcon;
    parama = localc.findViewById(i);
    localc.setIconView(parama);
    i = R.id.adMainMedia;
    parama = (MediaView)localc.findViewById(i);
    localc.setMediaView(parama);
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */