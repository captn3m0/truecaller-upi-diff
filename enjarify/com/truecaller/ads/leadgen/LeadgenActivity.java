package com.truecaller.ads.leadgen;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import c.g.a.a;
import c.g.b.k;
import c.g.b.t;
import com.d.b.ab;
import com.truecaller.R.id;
import com.truecaller.ads.i;
import com.truecaller.ads.leadgen.a.d;
import com.truecaller.ads.leadgen.a.e;
import com.truecaller.ads.leadgen.dto.LeadgenInput;
import com.truecaller.ads.ui.CtaButton;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.ui.ThemeManager.Theme;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public final class LeadgenActivity
  extends AppCompatActivity
  implements r
{
  public h b;
  public e c;
  private final c.f d;
  private final Map e;
  private HashMap f;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = c.g.b.w.a(LeadgenActivity.class);
    ((c.g.b.u)localObject).<init>(localb, "leadgenId", "getLeadgenId()Ljava/lang/String;");
    localObject = (c.l.g)c.g.b.w.a((t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public LeadgenActivity()
  {
    Object localObject = new com/truecaller/ads/leadgen/LeadgenActivity$a;
    ((LeadgenActivity.a)localObject).<init>(this);
    localObject = c.g.a((a)localObject);
    d = ((c.f)localObject);
    localObject = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject).<init>();
    localObject = (Map)localObject;
    e = ((Map)localObject);
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final h a()
  {
    h localh = b;
    if (localh == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localh;
  }
  
  public final void a(LeadgenInput paramLeadgenInput)
  {
    k.b(paramLeadgenInput, "input");
    Object localObject = e;
    paramLeadgenInput = a;
    paramLeadgenInput = (com.truecaller.ads.leadgen.a.c)((Map)localObject).get(paramLeadgenInput);
    if (paramLeadgenInput != null) {
      paramLeadgenInput = paramLeadgenInput.b();
    } else {
      paramLeadgenInput = null;
    }
    if (paramLeadgenInput != null)
    {
      localObject = getCurrentFocus();
      if (localObject != null) {
        ((View)localObject).clearFocus();
      }
      int i = R.id.scrollContainer;
      ((NestedScrollView)a(i)).b(0);
      i = R.id.header;
      ((AppBarLayout)a(i)).setExpanded(false);
      i = R.id.scrollContainer;
      localObject = (NestedScrollView)a(i);
      int j = paramLeadgenInput.getTop();
      ((NestedScrollView)localObject).scrollTo(0, j);
      paramLeadgenInput.requestFocus();
      return;
    }
  }
  
  public final void a(LeadgenInput paramLeadgenInput, d paramd, String paramString)
  {
    k.b(paramLeadgenInput, "input");
    k.b(paramd, "callback");
    e locale = c;
    if (locale == null)
    {
      localObject = "itemFactory";
      k.a((String)localObject);
    }
    int i = R.id.itemContainer;
    Object localObject = (LinearLayout)a(i);
    String str = "itemContainer";
    k.a(localObject, str);
    localObject = (ViewGroup)localObject;
    paramd = locale.a(paramLeadgenInput, paramString, paramd, (ViewGroup)localObject);
    if (paramd == null) {
      return;
    }
    paramString = e;
    paramLeadgenInput = a;
    paramString.put(paramLeadgenInput, paramd);
    int j = R.id.itemContainer;
    paramLeadgenInput = (LinearLayout)a(j);
    paramd = paramd.b();
    paramLeadgenInput.addView(paramd);
  }
  
  public final void a(LeadgenInput paramLeadgenInput, String paramString)
  {
    k.b(paramLeadgenInput, "input");
    Map localMap = e;
    paramLeadgenInput = a;
    paramLeadgenInput = (com.truecaller.ads.leadgen.a.c)localMap.get(paramLeadgenInput);
    if (paramLeadgenInput != null)
    {
      paramLeadgenInput.a(paramString);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "error");
    Object localObject = this;
    localObject = (Context)this;
    paramString = (CharSequence)paramString;
    Toast.makeText((Context)localObject, paramString, 0).show();
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    k.b(paramString1, "headerUrl");
    k.b(paramString2, "logoUrl");
    k.b(paramString3, "buttonBackgroundColor");
    k.b(paramString4, "headerBackgroundColor");
    paramString1 = i.b().a(paramString1);
    int i = R.id.headerImage;
    ImageView localImageView = (ImageView)a(i);
    k.a(localImageView, "headerImage");
    i = localImageView.getWidth();
    paramString1 = paramString1.b(i, 0);
    i = R.id.headerImage;
    localImageView = (ImageView)a(i);
    paramString1.a(localImageView, null);
    paramString1 = i.b().a(paramString2);
    int j = R.id.logoImage;
    paramString2 = (ImageView)a(j);
    paramString1.a(paramString2, null);
    int k = Color.parseColor(paramString3);
    j = Color.parseColor(paramString4);
    int m = R.id.actionButton;
    paramString3 = (CtaButton)a(m);
    int n = -1;
    paramString3.a(k, n);
    k = R.id.header;
    ((AppBarLayout)a(k)).setBackgroundColor(j);
    k = R.id.bodyContainer;
    ((LinearLayout)a(k)).setBackgroundColor(j);
    k = R.id.headerTitle;
    ((TextView)a(k)).setTextColor(n);
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = 0;
    int j;
    if (paramBoolean) {
      j = 0;
    } else {
      j = 8;
    }
    if (paramBoolean) {
      i = 4;
    }
    paramBoolean = R.id.loadingOverlay;
    Object localObject = (FrameLayout)a(paramBoolean);
    k.a(localObject, "loadingOverlay");
    ((FrameLayout)localObject).setVisibility(j);
    paramBoolean = R.id.actionButton;
    localObject = (CtaButton)a(paramBoolean);
    k.a(localObject, "actionButton");
    ((CtaButton)localObject).setVisibility(i);
  }
  
  public final void b()
  {
    int i = R.id.itemContainer;
    ((LinearLayout)a(i)).removeAllViews();
    e.clear();
  }
  
  public final void b(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    k.b(paramString1, "title");
    k.b(paramString2, "body");
    k.b(paramString3, "legal");
    k.b(paramString4, "action");
    int i = R.id.headerTitle;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "headerTitle");
    paramString1 = (CharSequence)paramString1;
    localTextView.setText(paramString1);
    int j = R.id.body;
    paramString1 = (TextView)a(j);
    k.a(paramString1, "this.body");
    paramString2 = (CharSequence)Html.fromHtml(paramString2);
    paramString1.setText(paramString2);
    j = R.id.body;
    paramString1 = (TextView)a(j);
    k.a(paramString1, "this.body");
    paramString2 = LinkMovementMethod.getInstance();
    paramString1.setMovementMethod(paramString2);
    j = R.id.legal;
    paramString1 = (TextView)a(j);
    k.a(paramString1, "this.legal");
    paramString2 = (CharSequence)Html.fromHtml(paramString3);
    paramString1.setText(paramString2);
    j = R.id.legal;
    paramString1 = (TextView)a(j);
    k.a(paramString1, "this.legal");
    paramString2 = LinkMovementMethod.getInstance();
    paramString1.setMovementMethod(paramString2);
    j = R.id.actionButton;
    paramString1 = (CtaButton)a(j);
    k.a(paramString1, "this.actionButton");
    paramString4 = (CharSequence)paramString4;
    paramString1.setText(paramString4);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    int i = DEFAULTresId;
    setTheme(i);
    super.onCreate(paramBundle);
    i = 2131558996;
    setContentView(i);
    Object localObject1 = getApplicationContext();
    if (localObject1 != null)
    {
      localObject1 = ((bk)localObject1).a();
      Object localObject2 = new com/truecaller/ads/leadgen/c;
      String str = (String)d.b();
      ((c)localObject2).<init>(str);
      ((bp)localObject1).a((c)localObject2).a(this);
      localObject1 = b;
      if (localObject1 == null)
      {
        localObject2 = "presenter";
        k.a((String)localObject2);
      }
      ((h)localObject1).a(this);
      localObject1 = b;
      if (localObject1 == null)
      {
        localObject2 = "presenter";
        k.a((String)localObject2);
      }
      ((h)localObject1).b(paramBundle);
      int j = R.id.actionButton;
      paramBundle = (CtaButton)a(j);
      localObject1 = new com/truecaller/ads/leadgen/LeadgenActivity$b;
      ((LeadgenActivity.b)localObject1).<init>(this);
      localObject1 = (View.OnClickListener)localObject1;
      paramBundle.setOnClickListener((View.OnClickListener)localObject1);
      j = R.id.closeButton;
      paramBundle = (ImageView)a(j);
      localObject1 = new com/truecaller/ads/leadgen/LeadgenActivity$c;
      ((LeadgenActivity.c)localObject1).<init>(this);
      localObject1 = (View.OnClickListener)localObject1;
      paramBundle.setOnClickListener((View.OnClickListener)localObject1);
      j = Build.VERSION.SDK_INT;
      i = 21;
      if (j >= i)
      {
        getWindow().clearFlags(67108864);
        getWindow().addFlags(-1 << -1);
        paramBundle = getWindow();
        localObject1 = "window";
        k.a(paramBundle, (String)localObject1);
        i = -16777216;
        paramBundle.setStatusBarColor(i);
      }
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramBundle;
  }
  
  public final boolean onCreateOptionsMenu(Menu paramMenu)
  {
    if (paramMenu != null)
    {
      int i = 2131362507;
      int j = 2131887200;
      paramMenu = paramMenu.add(0, i, 0, j);
      if (paramMenu != null)
      {
        paramMenu.setShowAsAction(2);
        paramMenu.setIcon(2131233992);
        paramMenu = paramMenu.getIcon();
        paramMenu.mutate();
        i = -1;
        PorterDuff.Mode localMode = PorterDuff.Mode.SRC_ATOP;
        paramMenu.setColorFilter(i, localMode);
      }
    }
    return true;
  }
  
  public final void onDestroy()
  {
    h localh = b;
    if (localh == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localh.y_();
    super.onDestroy();
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    int i;
    Object localObject;
    if (paramMenuItem != null)
    {
      i = paramMenuItem.getItemId();
      localObject = Integer.valueOf(i);
    }
    else
    {
      i = 0;
      localObject = null;
    }
    if (localObject != null)
    {
      i = ((Integer)localObject).intValue();
      int j = 2131362507;
      if (i == j)
      {
        paramMenuItem = b;
        if (paramMenuItem == null)
        {
          localObject = "presenter";
          k.a((String)localObject);
        }
        paramMenuItem.e();
        return true;
      }
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  public final void onSaveInstanceState(Bundle paramBundle)
  {
    k.b(paramBundle, "outState");
    super.onSaveInstanceState(paramBundle);
    h localh = b;
    if (localh == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localh.a(paramBundle);
  }
  
  public final void onStart()
  {
    super.onStart();
    h localh = b;
    if (localh == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localh.a();
  }
  
  public final void onStop()
  {
    h localh = b;
    if (localh == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localh.b();
    super.onStop();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.leadgen.LeadgenActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */