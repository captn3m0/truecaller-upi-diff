package com.truecaller.ads.leadgen;

import android.content.Context;
import android.support.v4.content.b;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import c.g.b.k;
import java.util.List;

public final class a
  extends ArrayAdapter
{
  public a(Context paramContext, List paramList)
  {
    super(paramContext, 17367049, paramList);
  }
  
  private final int a(int paramInt)
  {
    if (paramInt == 0) {
      paramInt = 2131100130;
    } else {
      paramInt = 2131100131;
    }
    return b.c(getContext(), paramInt);
  }
  
  public final View getDropDownView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    paramView = super.getView(0, paramView, paramViewGroup);
    paramViewGroup = (TextView)paramView.findViewById(16908308);
    CharSequence localCharSequence = (CharSequence)getItem(paramInt);
    paramViewGroup.setText(localCharSequence);
    paramInt = a(paramInt);
    paramViewGroup.setTextColor(paramInt);
    k.a(paramView, "view");
    return paramView;
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    paramView = super.getView(0, paramView, paramViewGroup);
    paramViewGroup = (TextView)paramView.findViewById(16908308);
    CharSequence localCharSequence = (CharSequence)getItem(paramInt);
    paramViewGroup.setText(localCharSequence);
    paramInt = a(paramInt);
    paramViewGroup.setTextColor(paramInt);
    k.a(paramView, "view");
    paramInt = paramView.getPaddingTop();
    int i = paramView.getPaddingRight();
    int j = paramView.getPaddingBottom();
    paramView.setPaddingRelative(0, paramInt, i, j);
    return paramView;
  }
  
  public final boolean isEnabled(int paramInt)
  {
    return paramInt != 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.leadgen.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */