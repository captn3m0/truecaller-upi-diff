package com.truecaller.ads.leadgen.a;

import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import c.g.b.k;
import c.g.b.n;
import c.g.b.o;
import c.g.b.w;
import c.i.a;
import c.l.g;
import com.truecaller.ads.leadgen.dto.LeadgenInput;

public final class h
  extends c
{
  private final int f = 2131558999;
  private final c.i.d g;
  private final c.i.d h;
  
  static
  {
    g[] arrayOfg = new g[2];
    Object localObject = new c/g/b/o;
    c.l.b localb = w.a(h.class);
    ((o)localObject).<init>(localb, "title", "getTitle()Landroid/widget/TextView;");
    localObject = (g)w.a((n)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/o;
    localb = w.a(h.class);
    ((o)localObject).<init>(localb, "value", "getValue()Landroid/widget/EditText;");
    localObject = (g)w.a((n)localObject);
    arrayOfg[1] = localObject;
    e = arrayOfg;
  }
  
  public h(LeadgenInput paramLeadgenInput, String paramString, d paramd, ViewGroup paramViewGroup)
  {
    super(paramLeadgenInput, paramString, paramd, paramViewGroup);
    paramLeadgenInput = a.a;
    paramLeadgenInput = a.a();
    g = paramLeadgenInput;
    paramLeadgenInput = a.a;
    paramLeadgenInput = a.a();
    h = paramLeadgenInput;
  }
  
  private final EditText c()
  {
    c.i.d locald = h;
    g localg = e[1];
    return (EditText)locald.a(localg);
  }
  
  public final int a()
  {
    return f;
  }
  
  protected final void a(View paramView)
  {
    k.b(paramView, "view");
    Object localObject1 = paramView.findViewById(2131364884);
    k.a(localObject1, "view.findViewById(R.id.title)");
    localObject1 = (TextView)localObject1;
    Object localObject2 = g;
    Object localObject3 = e[0];
    ((c.i.d)localObject2).a((g)localObject3, localObject1);
    int i = 2131365417;
    paramView = paramView.findViewById(i);
    k.a(paramView, "view.findViewById(R.id.value)");
    paramView = (EditText)paramView;
    localObject1 = h;
    localObject2 = e;
    int m = 1;
    localObject2 = localObject2[m];
    ((c.i.d)localObject1).a((g)localObject2, paramView);
    paramView = g;
    localObject1 = e[0];
    paramView = (TextView)paramView.a((g)localObject1);
    localObject1 = (CharSequence)b.c;
    paramView.setText((CharSequence)localObject1);
    paramView = c();
    localObject1 = c;
    if (localObject1 == null) {
      localObject1 = b.b;
    }
    localObject1 = (CharSequence)localObject1;
    paramView.setText((CharSequence)localObject1);
    localObject1 = (CharSequence)b.e;
    paramView.setHint((CharSequence)localObject1);
    localObject1 = b.f;
    if (localObject1 != null)
    {
      int n = ((String)localObject1).hashCode();
      m = 96619420;
      if (n != m)
      {
        m = 106642798;
        if (n == m)
        {
          localObject2 = "phone";
          boolean bool1 = ((String)localObject1).equals(localObject2);
          if (bool1)
          {
            int j = 3;
            paramView.setInputType(j);
          }
        }
      }
      else
      {
        localObject2 = "email";
        boolean bool2 = ((String)localObject1).equals(localObject2);
        if (bool2)
        {
          int k = 32;
          paramView.setInputType(k);
        }
      }
    }
    localObject1 = new com/truecaller/ads/leadgen/a/b;
    localObject2 = b.a;
    localObject3 = d;
    ((b)localObject1).<init>((String)localObject2, (d)localObject3);
    localObject1 = (TextWatcher)localObject1;
    paramView.addTextChangedListener((TextWatcher)localObject1);
  }
  
  public final void a(String paramString)
  {
    EditText localEditText = c();
    paramString = (CharSequence)paramString;
    localEditText.setError(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.leadgen.a.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */