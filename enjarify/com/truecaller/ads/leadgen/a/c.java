package com.truecaller.ads.leadgen.a;

import android.view.View;
import android.view.ViewGroup;
import c.f;
import c.g.a.a;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import com.truecaller.ads.leadgen.dto.LeadgenInput;

public abstract class c
{
  final LeadgenInput b;
  final String c;
  final d d;
  private final f e;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(c.class);
    ((u)localObject).<init>(localb, "view", "getView()Landroid/view/View;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public c(LeadgenInput paramLeadgenInput, String paramString, d paramd, ViewGroup paramViewGroup)
  {
    b = paramLeadgenInput;
    c = paramString;
    d = paramd;
    paramLeadgenInput = new com/truecaller/ads/leadgen/a/c$a;
    paramLeadgenInput.<init>(this, paramViewGroup);
    paramLeadgenInput = c.g.a((a)paramLeadgenInput);
    e = paramLeadgenInput;
  }
  
  public abstract int a();
  
  protected abstract void a(View paramView);
  
  public abstract void a(String paramString);
  
  public final View b()
  {
    return (View)e.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.leadgen.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */