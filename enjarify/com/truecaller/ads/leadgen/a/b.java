package com.truecaller.ads.leadgen.a;

import android.text.Editable;
import android.text.TextWatcher;
import c.g.b.k;

public final class b
  implements TextWatcher
{
  private final String a;
  private final d b;
  
  public b(String paramString, d paramd)
  {
    a = paramString;
    b = paramd;
  }
  
  public final void afterTextChanged(Editable paramEditable)
  {
    k.b(paramEditable, "e");
    d locald = b;
    String str = a;
    paramEditable = paramEditable.toString();
    locald.a(str, paramEditable);
  }
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
}

/* Location:
 * Qualified Name:     com.truecaller.ads.leadgen.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */