package com.truecaller.ads.leadgen.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import c.a.m;
import c.a.y;
import c.g.b.k;
import c.g.b.n;
import c.g.b.o;
import c.g.b.w;
import c.l.b;
import c.u;
import com.truecaller.ads.leadgen.dto.LeadgenInput;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class g
  extends c
{
  private final int f = 2131558998;
  private final c.i.d g;
  private final c.i.d h;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[2];
    Object localObject = new c/g/b/o;
    b localb = w.a(g.class);
    ((o)localObject).<init>(localb, "title", "getTitle()Landroid/widget/TextView;");
    localObject = (c.l.g)w.a((n)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/o;
    localb = w.a(g.class);
    ((o)localObject).<init>(localb, "value", "getValue()Landroid/widget/Spinner;");
    localObject = (c.l.g)w.a((n)localObject);
    arrayOfg[1] = localObject;
    e = arrayOfg;
  }
  
  public g(LeadgenInput paramLeadgenInput, String paramString, d paramd, ViewGroup paramViewGroup)
  {
    super(paramLeadgenInput, paramString, paramd, paramViewGroup);
    paramLeadgenInput = c.i.a.a;
    paramLeadgenInput = c.i.a.a();
    g = paramLeadgenInput;
    paramLeadgenInput = c.i.a.a;
    paramLeadgenInput = c.i.a.a();
    h = paramLeadgenInput;
  }
  
  private final Spinner c()
  {
    c.i.d locald = h;
    c.l.g localg = e[1];
    return (Spinner)locald.a(localg);
  }
  
  public final int a()
  {
    return f;
  }
  
  protected final void a(View paramView)
  {
    k.b(paramView, "view");
    Object localObject1 = paramView.findViewById(2131364884);
    k.a(localObject1, "view.findViewById(R.id.title)");
    localObject1 = (TextView)localObject1;
    Object localObject2 = g;
    Object localObject3 = e;
    Object localObject4 = null;
    localObject3 = localObject3[0];
    ((c.i.d)localObject2).a((c.l.g)localObject3, localObject1);
    int i = 2131365417;
    localObject1 = paramView.findViewById(i);
    k.a(localObject1, "view.findViewById(R.id.value)");
    localObject1 = (Spinner)localObject1;
    localObject2 = h;
    localObject3 = e;
    int j = 1;
    localObject3 = localObject3[j];
    ((c.i.d)localObject2).a((c.l.g)localObject3, localObject1);
    localObject1 = b.g;
    if (localObject1 == null) {
      localObject1 = (List)y.a;
    }
    localObject1 = (Iterable)localObject1;
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (Collection)localObject2;
    localObject1 = ((Iterable)localObject1).iterator();
    boolean bool;
    int m;
    for (;;)
    {
      bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject3 = ((Iterator)localObject1).next();
      localObject5 = localObject3;
      localObject5 = (CharSequence)localObject3;
      m = ((CharSequence)localObject5).length();
      if (m == 0)
      {
        m = 1;
      }
      else
      {
        m = 0;
        localObject5 = null;
      }
      if (m == 0) {
        ((Collection)localObject2).add(localObject3);
      }
    }
    localObject1 = m.d((Collection)localObject2);
    localObject2 = paramView.getContext();
    k.a(localObject2, "view.context");
    localObject3 = b.e;
    Object localObject5 = localObject3;
    localObject5 = (CharSequence)localObject3;
    if (localObject5 != null)
    {
      m = ((CharSequence)localObject5).length();
      if (m != 0) {
        j = 0;
      }
    }
    if (j != 0)
    {
      bool = false;
      localObject3 = null;
    }
    if (localObject3 == null)
    {
      int k = 2131886626;
      localObject3 = ((Context)localObject2).getString(k);
    }
    k.a(localObject3, "getHint(view.context)");
    ((List)localObject1).add(0, localObject3);
    localObject2 = g;
    localObject3 = e[0];
    localObject2 = (TextView)((c.i.d)localObject2).a((c.l.g)localObject3);
    localObject3 = (CharSequence)b.c;
    ((TextView)localObject2).setText((CharSequence)localObject3);
    c();
    localObject2 = c();
    localObject3 = new com/truecaller/ads/leadgen/a;
    paramView = paramView.getContext();
    localObject4 = "view.context";
    k.a(paramView, (String)localObject4);
    ((com.truecaller.ads.leadgen.a)localObject3).<init>(paramView, (List)localObject1);
    localObject3 = (SpinnerAdapter)localObject3;
    ((Spinner)localObject2).setAdapter((SpinnerAdapter)localObject3);
    paramView = c;
    if (paramView == null) {
      paramView = b.b;
    }
    int n = ((List)localObject1).indexOf(paramView);
    if (n >= 0)
    {
      localObject2 = c();
      ((Spinner)localObject2).setSelection(n);
    }
    paramView = c();
    localObject2 = new com/truecaller/ads/leadgen/a/a;
    localObject3 = b.a;
    localObject4 = d;
    ((a)localObject2).<init>((String)localObject3, (d)localObject4, (List)localObject1);
    localObject2 = (AdapterView.OnItemSelectedListener)localObject2;
    paramView.setOnItemSelectedListener((AdapterView.OnItemSelectedListener)localObject2);
  }
  
  public final void a(String paramString)
  {
    Object localObject = c().getSelectedView();
    if (localObject != null)
    {
      localObject = (TextView)localObject;
      paramString = (CharSequence)paramString;
      ((TextView)localObject).setError(paramString);
      return;
    }
    paramString = new c/u;
    paramString.<init>("null cannot be cast to non-null type android.widget.TextView");
    throw paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.leadgen.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */