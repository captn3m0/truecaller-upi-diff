package com.truecaller.ads.leadgen.a;

import android.view.ViewGroup;
import c.g.b.k;
import com.truecaller.ads.leadgen.dto.LeadgenInput;

public final class f
  implements e
{
  public final c a(LeadgenInput paramLeadgenInput, String paramString, d paramd, ViewGroup paramViewGroup)
  {
    k.b(paramLeadgenInput, "input");
    k.b(paramd, "callback");
    k.b(paramViewGroup, "container");
    Object localObject = d;
    int i = ((String)localObject).hashCode();
    int j = -1822154468;
    String str;
    boolean bool;
    if (i != j)
    {
      j = 2603341;
      if (i == j)
      {
        str = "Text";
        bool = ((String)localObject).equals(str);
        if (bool)
        {
          localObject = new com/truecaller/ads/leadgen/a/h;
          ((h)localObject).<init>(paramLeadgenInput, paramString, paramd, paramViewGroup);
          return (c)localObject;
        }
      }
    }
    else
    {
      str = "Select";
      bool = ((String)localObject).equals(str);
      if (bool)
      {
        localObject = new com/truecaller/ads/leadgen/a/g;
        ((g)localObject).<init>(paramLeadgenInput, paramString, paramd, paramViewGroup);
        return (c)localObject;
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.leadgen.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */