package com.truecaller.ads.leadgen.a;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import java.util.List;

public final class a
  implements AdapterView.OnItemSelectedListener
{
  private final String a;
  private final d b;
  private final List c;
  
  public a(String paramString, d paramd, List paramList)
  {
    a = paramString;
    b = paramd;
    c = paramList;
  }
  
  public final void onItemSelected(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    if (paramInt == 0)
    {
      onNothingSelected(paramAdapterView);
      return;
    }
    paramAdapterView = b;
    paramView = a;
    String str = (String)c.get(paramInt);
    paramAdapterView.a(paramView, str);
  }
  
  public final void onNothingSelected(AdapterView paramAdapterView)
  {
    paramAdapterView = b;
    String str = a;
    paramAdapterView.a(str, "");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.leadgen.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */