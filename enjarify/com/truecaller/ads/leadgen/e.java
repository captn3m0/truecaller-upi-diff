package com.truecaller.ads.leadgen;

import com.truecaller.androidactors.f;
import com.truecaller.utils.n;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class e
  implements d
{
  private final c a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  public static h a(c paramc, com.truecaller.androidactors.k paramk, f paramf, n paramn)
  {
    c.g.b.k.b(paramk, "actorsThreads");
    c.g.b.k.b(paramf, "leadgenRestManagerActorRef");
    c.g.b.k.b(paramn, "resourceProvider");
    i locali = new com/truecaller/ads/leadgen/i;
    paramc = a;
    paramk = paramk.a();
    c.g.b.k.a(paramk, "actorsThreads.ui()");
    locali.<init>(paramc, paramk, paramf, paramn);
    return (h)g.a((h)locali, "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.leadgen.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */