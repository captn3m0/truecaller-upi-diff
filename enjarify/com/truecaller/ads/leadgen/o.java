package com.truecaller.ads.leadgen;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import java.util.Map;

public final class o
  implements n
{
  private final v a;
  
  public o(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return n.class.equals(paramClass);
  }
  
  public final w a(String paramString)
  {
    v localv = a;
    o.a locala = new com/truecaller/ads/leadgen/o$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramString, (byte)0);
    return w.a(localv, locala);
  }
  
  public final w a(String paramString, Map paramMap)
  {
    v localv = a;
    o.b localb = new com/truecaller/ads/leadgen/o$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramString, paramMap, (byte)0);
    return w.a(localv, localb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.leadgen.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */