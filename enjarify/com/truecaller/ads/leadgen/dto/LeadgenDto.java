package com.truecaller.ads.leadgen.dto;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;
import java.util.List;

public final class LeadgenDto
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public static final LeadgenDto.a d;
  public final LeadgenDescription a;
  public final LeadgenTheme b;
  public final List c;
  
  static
  {
    Object localObject = new com/truecaller/ads/leadgen/dto/LeadgenDto$a;
    ((LeadgenDto.a)localObject).<init>((byte)0);
    d = (LeadgenDto.a)localObject;
    localObject = new com/truecaller/ads/leadgen/dto/LeadgenDto$b;
    ((LeadgenDto.b)localObject).<init>();
    CREATOR = (Parcelable.Creator)localObject;
  }
  
  public LeadgenDto(LeadgenDescription paramLeadgenDescription, LeadgenTheme paramLeadgenTheme, List paramList)
  {
    a = paramLeadgenDescription;
    b = paramLeadgenTheme;
    c = paramList;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "dest");
    Parcelable localParcelable = (Parcelable)a;
    paramParcel.writeParcelable(localParcelable, paramInt);
    localParcelable = (Parcelable)b;
    paramParcel.writeParcelable(localParcelable, paramInt);
    List localList = c;
    paramParcel.writeTypedList(localList);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.leadgen.dto.LeadgenDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */