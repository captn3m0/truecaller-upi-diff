package com.truecaller.ads.leadgen.dto;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;
import java.util.List;

public final class LeadgenInput
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public static final LeadgenInput.a h;
  public final String a;
  public final String b;
  public final String c;
  public final String d;
  public final String e;
  public final String f;
  public final List g;
  
  static
  {
    Object localObject = new com/truecaller/ads/leadgen/dto/LeadgenInput$a;
    ((LeadgenInput.a)localObject).<init>((byte)0);
    h = (LeadgenInput.a)localObject;
    localObject = new com/truecaller/ads/leadgen/dto/LeadgenInput$b;
    ((LeadgenInput.b)localObject).<init>();
    CREATOR = (Parcelable.Creator)localObject;
  }
  
  public LeadgenInput(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, List paramList)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
    d = paramString4;
    e = paramString5;
    f = paramString6;
    g = paramList;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "dest");
    Object localObject = a;
    paramParcel.writeString((String)localObject);
    localObject = b;
    paramParcel.writeString((String)localObject);
    localObject = c;
    paramParcel.writeString((String)localObject);
    localObject = d;
    paramParcel.writeString((String)localObject);
    localObject = e;
    paramParcel.writeString((String)localObject);
    localObject = f;
    int i = 1;
    if (localObject == null)
    {
      paramParcel.writeInt(0);
    }
    else
    {
      paramParcel.writeInt(i);
      localObject = f;
      paramParcel.writeString((String)localObject);
    }
    localObject = g;
    if (localObject == null)
    {
      paramParcel.writeInt(0);
      return;
    }
    paramParcel.writeInt(i);
    localObject = g;
    paramParcel.writeStringList((List)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.leadgen.dto.LeadgenInput
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */