package com.truecaller.ads.leadgen.dto;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class LeadgenDescription
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public static final LeadgenDescription.a f;
  public final String a;
  public final String b;
  public final String c;
  public final String d;
  public final String e;
  
  static
  {
    Object localObject = new com/truecaller/ads/leadgen/dto/LeadgenDescription$a;
    ((LeadgenDescription.a)localObject).<init>((byte)0);
    f = (LeadgenDescription.a)localObject;
    localObject = new com/truecaller/ads/leadgen/dto/LeadgenDescription$b;
    ((LeadgenDescription.b)localObject).<init>();
    CREATOR = (Parcelable.Creator)localObject;
  }
  
  public LeadgenDescription(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
    d = paramString4;
    e = paramString5;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "dest");
    String str = a;
    paramParcel.writeString(str);
    str = b;
    paramParcel.writeString(str);
    str = c;
    paramParcel.writeString(str);
    str = d;
    paramParcel.writeString(str);
    str = e;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.leadgen.dto.LeadgenDescription
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */