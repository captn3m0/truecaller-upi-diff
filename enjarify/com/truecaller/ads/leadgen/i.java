package com.truecaller.ads.leadgen;

import android.os.Bundle;
import android.os.Parcelable;
import c.a.ag;
import c.g.a.b;
import c.t;
import com.truecaller.ads.leadgen.a.d;
import com.truecaller.ads.leadgen.dto.LeadgenDescription;
import com.truecaller.ads.leadgen.dto.LeadgenDto;
import com.truecaller.ads.leadgen.dto.LeadgenInput;
import com.truecaller.ads.leadgen.dto.LeadgenTheme;
import com.truecaller.androidactors.a;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class i
  extends h
  implements d
{
  a a;
  LeadgenDto c;
  private a d;
  private Map e;
  private boolean f;
  private String g;
  private final com.truecaller.androidactors.i h;
  private final f i;
  private final com.truecaller.utils.n j;
  
  public i(String paramString, com.truecaller.androidactors.i parami, f paramf, com.truecaller.utils.n paramn)
  {
    g = paramString;
    h = parami;
    i = paramf;
    j = paramn;
    paramString = new java/util/LinkedHashMap;
    paramString.<init>();
    paramString = (Map)paramString;
    e = paramString;
  }
  
  private final String a(LeadgenInput paramLeadgenInput)
  {
    Object localObject = e;
    String str = a;
    localObject = (String)((Map)localObject).get(str);
    if (localObject == null) {
      localObject = "";
    }
    paramLeadgenInput = f;
    if (paramLeadgenInput != null)
    {
      int k = paramLeadgenInput.hashCode();
      int m = 106642798;
      if (k == m)
      {
        str = "phone";
        boolean bool = paramLeadgenInput.equals(str);
        if (bool) {
          return c.n.m.a(c.n.m.a((String)localObject, "+", ""), " ", "");
        }
      }
    }
    return (String)localObject;
  }
  
  private final void g()
  {
    Object localObject1 = (r)b;
    if (localObject1 != null) {
      ((r)localObject1).b();
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject1 = c;
      if (localObject1 != null)
      {
        localObject1 = ((Iterable)localObject1).iterator();
        for (;;)
        {
          boolean bool1 = ((Iterator)localObject1).hasNext();
          if (!bool1) {
            break;
          }
          LeadgenInput localLeadgenInput = (LeadgenInput)((Iterator)localObject1).next();
          Object localObject2 = e;
          Object localObject3 = a;
          boolean bool2 = ((Map)localObject2).containsKey(localObject3);
          Object localObject4;
          if (!bool2)
          {
            localObject2 = e;
            localObject3 = a;
            localObject4 = b;
            ((Map)localObject2).put(localObject3, localObject4);
          }
          localObject2 = (r)b;
          if (localObject2 != null)
          {
            localObject3 = this;
            localObject3 = (d)this;
            localObject4 = e;
            String str = a;
            localObject4 = (String)((Map)localObject4).get(str);
            ((r)localObject2).a(localLeadgenInput, (d)localObject3, (String)localObject4);
          }
        }
        return;
      }
    }
  }
  
  private final void h()
  {
    r localr = (r)b;
    if (localr != null)
    {
      a locala = a;
      if (locala == null)
      {
        locala = d;
        if (locala == null)
        {
          bool = false;
          locala = null;
          break label42;
        }
      }
      boolean bool = true;
      label42:
      localr.a(bool);
      return;
    }
  }
  
  public final void a()
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = g;
      if (localObject1 != null)
      {
        localObject1 = ((n)i.a()).a((String)localObject1);
        com.truecaller.androidactors.i locali = h;
        Object localObject2 = new com/truecaller/ads/leadgen/i$b;
        Object localObject3 = this;
        localObject3 = (i)this;
        ((i.b)localObject2).<init>((i)localObject3);
        localObject2 = (b)localObject2;
        localObject3 = new com/truecaller/ads/leadgen/l;
        ((l)localObject3).<init>((b)localObject2);
        localObject3 = (ac)localObject3;
        localObject1 = ((w)localObject1).a(locali, (ac)localObject3);
        a = ((a)localObject1);
      }
      else
      {
        localObject1 = (r)b;
        if (localObject1 != null) {
          ((r)localObject1).finish();
        }
      }
    }
    f();
  }
  
  public final void a(Bundle paramBundle)
  {
    c.g.b.k.b(paramBundle, "state");
    String str1 = g;
    if (str1 == null) {
      return;
    }
    Object localObject1 = c;
    if (localObject1 == null) {
      return;
    }
    paramBundle.putString("leadgen_id", str1);
    localObject1 = (Parcelable)c;
    paramBundle.putParcelable("leadgen_dto", (Parcelable)localObject1);
    str1 = "leadgen_map";
    localObject1 = e;
    c.g.b.k.b(localObject1, "receiver$0");
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localObject1 = ((Iterable)((Map)localObject1).entrySet()).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      Object localObject2 = (Map.Entry)((Iterator)localObject1).next();
      String str2 = (String)((Map.Entry)localObject2).getKey();
      localObject2 = (String)((Map.Entry)localObject2).getValue();
      localBundle.putString(str2, (String)localObject2);
    }
    paramBundle.putBundle(str1, localBundle);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "key");
    c.g.b.k.b(paramString2, "value");
    e.put(paramString1, paramString2);
  }
  
  public final void b()
  {
    a locala = a;
    if (locala != null) {
      locala.a();
    }
    a = null;
  }
  
  public final void b(Bundle paramBundle)
  {
    if (paramBundle != null)
    {
      Object localObject1 = paramBundle.getString("leadgen_id");
      g = ((String)localObject1);
      localObject1 = (LeadgenDto)paramBundle.getParcelable("leadgen_dto");
      c = ((LeadgenDto)localObject1);
      paramBundle = paramBundle.getBundle("leadgen_map");
      c.g.b.k.a(paramBundle, "getBundle(BUNDLE_KEY_ANSWERS)");
      c.g.b.k.b(paramBundle, "receiver$0");
      localObject1 = paramBundle.keySet();
      Object localObject2 = "keySet()";
      c.g.b.k.a(localObject1, (String)localObject2);
      localObject1 = (Iterable)localObject1;
      int k = ag.a(c.a.m.a((Iterable)localObject1, 10));
      int m = 16;
      k = c.k.i.c(k, m);
      Object localObject3 = new java/util/LinkedHashMap;
      ((LinkedHashMap)localObject3).<init>(k);
      localObject3 = (Map)localObject3;
      localObject1 = ((Iterable)localObject1).iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject1).hasNext();
        if (!bool) {
          break;
        }
        localObject2 = (String)((Iterator)localObject1).next();
        Object localObject4 = paramBundle.getString((String)localObject2);
        localObject2 = t.a(localObject2, localObject4);
        localObject4 = a;
        localObject2 = b;
        ((Map)localObject3).put(localObject4, localObject2);
      }
      e.putAll((Map)localObject3);
      return;
    }
  }
  
  public final void c()
  {
    Object localObject1 = g;
    if (localObject1 == null) {
      return;
    }
    boolean bool1 = f;
    if (bool1)
    {
      localObject2 = (r)b;
      if (localObject2 != null) {
        ((r)localObject2).finish();
      }
    }
    Object localObject2 = a;
    if (localObject2 == null)
    {
      localObject2 = d;
      if (localObject2 == null)
      {
        bool1 = f;
        if (!bool1)
        {
          localObject2 = c;
          Object localObject3 = null;
          int k = 1;
          int m = 0;
          Object localObject4 = null;
          Object localObject5;
          if (localObject2 != null)
          {
            localObject2 = c;
            if (localObject2 != null)
            {
              localObject2 = ((Iterable)localObject2).iterator();
              localObject5 = null;
              n = 0;
              locali = null;
              for (;;)
              {
                boolean bool3 = ((Iterator)localObject2).hasNext();
                if (!bool3) {
                  break;
                }
                LeadgenInput localLeadgenInput = (LeadgenInput)((Iterator)localObject2).next();
                Object localObject6 = a(localLeadgenInput);
                Object localObject7 = localObject6;
                localObject7 = (CharSequence)localObject6;
                int i1 = c.n.m.a((CharSequence)localObject7);
                Object localObject8;
                if (i1 != 0)
                {
                  localObject6 = j;
                  int i3 = 2131886624;
                  localObject8 = new Object[0];
                  localObject6 = ((com.truecaller.utils.n)localObject6).a(i3, (Object[])localObject8);
                }
                else
                {
                  localObject8 = f;
                  String str = "email";
                  i1 = c.g.b.k.a(localObject8, str);
                  if (i1 != 0)
                  {
                    localObject8 = j.a();
                    localObject7 = ((Pattern)localObject8).matcher((CharSequence)localObject7);
                    boolean bool4 = ((Matcher)localObject7).matches();
                    if (!bool4)
                    {
                      localObject6 = j;
                      int i4 = 2131886623;
                      localObject8 = new Object[0];
                      localObject6 = ((com.truecaller.utils.n)localObject6).a(i4, (Object[])localObject8);
                      break label473;
                    }
                  }
                  localObject7 = f;
                  localObject8 = "phone";
                  boolean bool5 = c.g.b.k.a(localObject7, localObject8);
                  if (bool5)
                  {
                    localObject6 = c.n.m.a((String)localObject6, " ", "");
                    localObject7 = localObject6;
                    localObject7 = (CharSequence)localObject6;
                    i1 = 0;
                    localObject8 = null;
                    for (;;)
                    {
                      int i6 = ((CharSequence)localObject7).length();
                      if (i1 >= i6) {
                        break;
                      }
                      boolean bool6 = Character.isDigit(((CharSequence)localObject7).charAt(i1));
                      if (!bool6)
                      {
                        bool5 = false;
                        localObject7 = null;
                        break label395;
                      }
                      int i2;
                      i1 += 1;
                    }
                    bool5 = true;
                    label395:
                    int i5;
                    if (bool5)
                    {
                      i7 = ((String)localObject6).length();
                      i5 = 5;
                      if (i7 >= i5)
                      {
                        i7 = 1;
                        break label429;
                      }
                    }
                    i7 = 0;
                    localObject6 = null;
                    label429:
                    if (i7 == 0)
                    {
                      localObject6 = j;
                      i5 = 2131886625;
                      localObject8 = new Object[0];
                      localObject6 = ((com.truecaller.utils.n)localObject6).a(i5, (Object[])localObject8);
                      break label473;
                    }
                  }
                  int i7 = 0;
                  localObject6 = null;
                }
                label473:
                localObject7 = (r)b;
                if (localObject7 != null) {
                  ((r)localObject7).a(localLeadgenInput, (String)localObject6);
                }
                if (localObject6 != null)
                {
                  if (localObject5 == null) {
                    localObject5 = localLeadgenInput;
                  }
                  n += 1;
                }
              }
              localObject3 = localObject5;
              break label534;
            }
          }
          int n = 0;
          i locali = null;
          label534:
          if (localObject3 != null)
          {
            localObject2 = (r)b;
            if (localObject2 != null) {
              ((r)localObject2).a((LeadgenInput)localObject3);
            }
          }
          Object localObject9;
          if (n <= 0)
          {
            k = 0;
            localObject9 = null;
          }
          if (k == 0)
          {
            localObject2 = (n)i.a();
            localObject3 = c;
            if (localObject3 != null)
            {
              localObject3 = c;
              if (localObject3 != null)
              {
                localObject3 = (Iterable)localObject3;
                k = ag.a(c.a.m.a((Iterable)localObject3, 10));
                m = 16;
                k = c.k.i.c(k, m);
                localObject4 = new java/util/LinkedHashMap;
                ((LinkedHashMap)localObject4).<init>(k);
                localObject4 = (Map)localObject4;
                localObject3 = ((Iterable)localObject3).iterator();
                for (;;)
                {
                  boolean bool2 = ((Iterator)localObject3).hasNext();
                  if (!bool2) {
                    break;
                  }
                  localObject9 = (LeadgenInput)((Iterator)localObject3).next();
                  localObject5 = a;
                  locali = this;
                  locali = (i)this;
                  localObject9 = locali.a((LeadgenInput)localObject9);
                  ((Map)localObject4).put(localObject5, localObject9);
                }
              }
            }
            localObject4 = ag.a();
            localObject1 = ((n)localObject2).a((String)localObject1, (Map)localObject4);
            localObject2 = h;
            localObject3 = new com/truecaller/ads/leadgen/i$a;
            localObject9 = this;
            localObject9 = (i)this;
            ((i.a)localObject3).<init>((i)localObject9);
            localObject3 = (b)localObject3;
            localObject9 = new com/truecaller/ads/leadgen/k;
            ((k)localObject9).<init>((b)localObject3);
            localObject9 = (ac)localObject9;
            localObject1 = ((w)localObject1).a((com.truecaller.androidactors.i)localObject2, (ac)localObject9);
            d = ((a)localObject1);
            h();
            return;
          }
        }
      }
    }
  }
  
  public final void e()
  {
    r localr = (r)b;
    if (localr != null)
    {
      localr.finish();
      return;
    }
  }
  
  final void f()
  {
    h();
    Object localObject1 = c;
    if (localObject1 != null)
    {
      Object localObject2 = a;
      localObject1 = b;
      boolean bool = f;
      String str1;
      Object localObject4;
      if (bool)
      {
        localObject1 = (r)b;
        if (localObject1 != null) {
          ((r)localObject1).b();
        }
        localObject1 = (r)b;
        if (localObject1 != null)
        {
          localObject3 = d;
          localObject2 = e;
          str1 = "";
          localObject4 = j;
          int k = 2131886619;
          Object[] arrayOfObject = new Object[0];
          localObject4 = ((com.truecaller.utils.n)localObject4).a(k, arrayOfObject);
          String str2 = "resourceProvider.getString(R.string.LeadgenClose)";
          c.g.b.k.a(localObject4, str2);
          ((r)localObject1).b((String)localObject3, (String)localObject2, str1, (String)localObject4);
        }
        return;
      }
      g();
      Object localObject3 = (r)b;
      if (localObject3 != null)
      {
        str1 = a;
        localObject4 = b;
        localObject2 = c;
        localObject1 = e;
        ((r)localObject3).b(str1, (String)localObject4, (String)localObject2, (String)localObject1);
        return;
      }
      return;
    }
  }
  
  public final void y_()
  {
    super.y_();
    a locala = d;
    if (locala != null) {
      locala.a();
    }
    d = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.leadgen.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */