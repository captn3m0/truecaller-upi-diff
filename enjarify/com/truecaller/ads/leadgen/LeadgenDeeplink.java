package com.truecaller.ads.leadgen;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import c.g.b.k;

public final class LeadgenDeeplink
{
  private static final String EXTRA_LEADGEN_ID = "extraLeadgenId";
  public static final LeadgenDeeplink INSTANCE;
  
  static
  {
    LeadgenDeeplink localLeadgenDeeplink = new com/truecaller/ads/leadgen/LeadgenDeeplink;
    localLeadgenDeeplink.<init>();
    INSTANCE = localLeadgenDeeplink;
  }
  
  public static final Intent createDeeplink(Context paramContext, Bundle paramBundle)
  {
    k.b(paramContext, "context");
    k.b(paramBundle, "extras");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, LeadgenActivity.class);
    paramContext = EXTRA_LEADGEN_ID;
    paramBundle = paramBundle.getString("leadgenId");
    localIntent.putExtra(paramContext, paramBundle);
    localIntent.addFlags(268435456);
    return localIntent;
  }
  
  public final String getEXTRA_LEADGEN_ID()
  {
    return EXTRA_LEADGEN_ID;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.leadgen.LeadgenDeeplink
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */