package com.truecaller.ads.leadgen;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.c;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.ImageView;
import c.g.b.k;
import com.truecaller.R.id;
import java.io.PrintStream;
import java.util.HashMap;

public final class LeadgenHeaderLayout
  extends ConstraintLayout
  implements AppBarLayout.c
{
  private float k = 0.35F;
  private int l;
  private boolean m = true;
  private HashMap n;
  
  public LeadgenHeaderLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private LeadgenHeaderLayout(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet, (byte)0);
  }
  
  private final void b()
  {
    boolean bool = m;
    int i;
    float f;
    if (bool)
    {
      i = 1065353216;
      f = 1.0F;
    }
    else
    {
      i = 0;
      f = 0.0F;
    }
    int j = R.id.headerImage;
    ((ImageView)c(j)).animate().alpha(f);
    j = R.id.logoImage;
    ((ImageView)c(j)).animate().alpha(f);
  }
  
  private View c(int paramInt)
  {
    Object localObject1 = n;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      n = ((HashMap)localObject1);
    }
    localObject1 = n;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = n;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(AppBarLayout paramAppBarLayout, int paramInt)
  {
    String str1 = "appBarLayout";
    k.b(paramAppBarLayout, str1);
    int i = l;
    if (i == paramInt) {
      return;
    }
    l = paramInt;
    float f1 = paramInt;
    float f2 = paramAppBarLayout.getHeight();
    f1 = Math.abs(f1 / f2);
    int j = R.id.animationGuideline;
    Object localObject = (Guideline)c(j);
    int i1 = Math.abs(paramInt);
    ((Guideline)localObject).setGuidelineBegin(i1);
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("LGD ");
    ((StringBuilder)localObject).append(paramInt);
    i1 = 32;
    ((StringBuilder)localObject).append(i1);
    boolean bool1 = m;
    ((StringBuilder)localObject).append(bool1);
    ((StringBuilder)localObject).append(i1);
    ((StringBuilder)localObject).append(f1);
    ((StringBuilder)localObject).append(" -> ");
    float f3 = k;
    ((StringBuilder)localObject).append(f3);
    String str2 = " vo:";
    ((StringBuilder)localObject).append(str2);
    ((StringBuilder)localObject).append(paramInt);
    ((StringBuilder)localObject).append(" height:");
    int i2 = paramAppBarLayout.getHeight();
    ((StringBuilder)localObject).append(i2);
    paramAppBarLayout = ((StringBuilder)localObject).toString();
    PrintStream localPrintStream = System.out;
    localPrintStream.println(paramAppBarLayout);
    boolean bool2 = m;
    float f4;
    if (bool2)
    {
      f4 = k;
      bool2 = f1 < f4;
      if (bool2)
      {
        m = false;
        b();
        return;
      }
    }
    bool2 = m;
    if (!bool2)
    {
      f4 = k;
      bool2 = f1 < f4;
      if (bool2)
      {
        bool2 = true;
        f4 = Float.MIN_VALUE;
        m = bool2;
        b();
      }
    }
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    Object localObject1 = getParent();
    boolean bool = localObject1 instanceof AppBarLayout;
    if (!bool) {
      localObject1 = null;
    }
    localObject1 = (AppBarLayout)localObject1;
    if (localObject1 != null)
    {
      Object localObject2 = this;
      localObject2 = (AppBarLayout.c)this;
      ((AppBarLayout)localObject1).a((AppBarLayout.c)localObject2);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.leadgen.LeadgenHeaderLayout
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */