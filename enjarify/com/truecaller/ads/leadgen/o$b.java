package com.truecaller.ads.leadgen;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.Map;

final class o$b
  extends u
{
  private final String b;
  private final Map c;
  
  private o$b(e parame, String paramString, Map paramMap)
  {
    super(parame);
    b = paramString;
    c = paramMap;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".submitLeadgenForm(");
    String str = b;
    int i = 2;
    str = a(str, i);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(c, i);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.leadgen.o.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */