package com.truecaller.ads.qa;

import android.support.v7.widget.RecyclerView.Adapter;
import c.a.m;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

public final class j
  extends RecyclerView.Adapter
{
  private final List a;
  
  public j(Set paramSet)
  {
    paramSet = (Iterable)paramSet;
    Object localObject = new com/truecaller/ads/qa/j$a;
    ((j.a)localObject).<init>();
    localObject = (Comparator)localObject;
    paramSet = m.a(paramSet, (Comparator)localObject);
    a = paramSet;
  }
  
  public final int getItemCount()
  {
    return a.size();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.qa.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */