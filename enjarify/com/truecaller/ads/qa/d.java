package com.truecaller.ads.qa;

import android.support.v7.widget.RecyclerView.Adapter;
import c.g.a.m;
import c.m.l;
import java.util.List;

public final class d
  extends RecyclerView.Adapter
{
  private final List a;
  
  public d(List paramList)
  {
    d.a locala = new com/truecaller/ads/qa/d$a;
    locala.<init>(paramList, null);
    paramList = l.d(l.a((m)locala));
    a = paramList;
  }
  
  public final int getItemCount()
  {
    return a.size();
  }
  
  public final int getItemViewType(int paramInt)
  {
    List localList = a;
    Object localObject = localList.get(paramInt);
    boolean bool = localObject instanceof c;
    if (bool) {
      return 1;
    }
    paramInt = localObject instanceof a;
    if (paramInt != 0) {
      return 2;
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("Wrong object");
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.qa.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */