package com.truecaller.ads.qa;

import java.text.SimpleDateFormat;
import java.util.Locale;

public final class k
{
  private static final SimpleDateFormat a;
  
  static
  {
    SimpleDateFormat localSimpleDateFormat = new java/text/SimpleDateFormat;
    Locale localLocale = Locale.US;
    localSimpleDateFormat.<init>("yyyy.MM.dd HH:mm:ss", localLocale);
    a = localSimpleDateFormat;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.qa.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */