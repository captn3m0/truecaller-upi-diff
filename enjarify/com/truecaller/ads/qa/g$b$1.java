package com.truecaller.ads.qa;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.R.id;
import java.util.List;
import kotlinx.coroutines.ag;

final class g$b$1
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  g$b$1(g.b paramb, List paramList, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/ads/qa/g$b$1;
    g.b localb = b;
    List localList = c;
    local1.<init>(localb, localList, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = b.d;
        int j = R.id.list;
        paramObject = (RecyclerView)((View)paramObject).findViewById(j);
        localObject = new com/truecaller/ads/qa/d;
        List localList = c;
        c.g.b.k.a(localList, "campaigns");
        ((d)localObject).<init>(localList);
        localObject = (RecyclerView.Adapter)localObject;
        ((RecyclerView)paramObject).setAdapter((RecyclerView.Adapter)localObject);
        return paramObject;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.qa.g.b.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */