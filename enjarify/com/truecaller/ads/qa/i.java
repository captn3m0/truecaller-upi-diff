package com.truecaller.ads.qa;

import android.content.Context;
import c.g.b.k;
import com.truecaller.ads.campaigns.e;
import com.truecaller.ads.campaigns.f;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class i
  implements d
{
  private final Provider a;
  
  public static e a(Context paramContext)
  {
    k.b(paramContext, "context");
    paramContext = f.a(paramContext);
    k.a(paramContext, "AdManagerFactory.create(context)");
    return (e)g.a(paramContext, "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.qa.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */