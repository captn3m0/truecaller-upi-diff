package com.truecaller.ads.qa;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import c.g.b.k;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;

public final class QaKeywordsActivity
  extends AppCompatActivity
{
  public static final QaKeywordsActivity.a a;
  
  static
  {
    QaKeywordsActivity.a locala = new com/truecaller/ads/qa/QaKeywordsActivity$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public static final void a(Activity paramActivity)
  {
    k.b(paramActivity, "context");
    Intent localIntent = new android/content/Intent;
    Object localObject = paramActivity;
    localObject = (Context)paramActivity;
    localIntent.<init>((Context)localObject, QaKeywordsActivity.class);
    paramActivity.startActivity(localIntent);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    Object localObject1 = this;
    localObject1 = (Context)this;
    boolean bool = Settings.a((Context)localObject1);
    if (!bool)
    {
      finish();
      return;
    }
    int i = aresId;
    setTheme(i);
    super.onCreate(paramBundle);
    paramBundle = new android/widget/FrameLayout;
    paramBundle.<init>((Context)localObject1);
    int j = 2131362594;
    paramBundle.setId(j);
    paramBundle = (View)paramBundle;
    setContentView(paramBundle);
    paramBundle = getSupportFragmentManager().a();
    Object localObject2 = new com/truecaller/ads/qa/l;
    ((l)localObject2).<init>();
    localObject2 = (Fragment)localObject2;
    String str = QaKeywordsActivity.class.getName();
    paramBundle.b(j, (Fragment)localObject2, str).c();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.qa.QaKeywordsActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */