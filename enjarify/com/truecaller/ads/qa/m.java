package com.truecaller.ads.qa;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import com.truecaller.common.h.j;
import java.util.HashMap;
import kotlinx.a.a.a;

public final class m
  extends RecyclerView.ViewHolder
  implements a
{
  final View a;
  private HashMap b;
  
  public m(View paramView)
  {
    super(paramView);
    a = paramView;
  }
  
  static String a(int paramInt)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str1 = String.valueOf(paramInt / 60);
    char c = '0';
    int i = 2;
    str1 = c.n.m.a(str1, i, c);
    localStringBuilder.append(str1);
    localStringBuilder.append(":");
    String str2 = c.n.m.a(String.valueOf(paramInt % 60), i, c);
    localStringBuilder.append(str2);
    return localStringBuilder.toString();
  }
  
  static String a(long paramLong, Context paramContext)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str1 = j.d(paramContext, paramLong);
    localStringBuilder.append(str1);
    localStringBuilder.append(" ");
    String str2 = j.f(paramContext, paramLong);
    localStringBuilder.append(str2);
    return localStringBuilder.toString();
  }
  
  public final View a()
  {
    return a;
  }
  
  public final View b(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.qa.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */