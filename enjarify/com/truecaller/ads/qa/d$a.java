package com.truecaller.ads.qa;

import c.d.b.a.j;
import c.n;
import c.o.b;
import c.x;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class d$a
  extends j
  implements c.g.a.m
{
  Object a;
  Object b;
  Object c;
  Object d;
  Object e;
  Object f;
  Object g;
  Object h;
  Object i;
  int j;
  private c.m.k l;
  
  d$a(List paramList, c.d.c paramc)
  {
    super(paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/ads/qa/d$a;
    List localList = k;
    locala.<init>(localList, paramc);
    paramObject = (c.m.k)paramObject;
    l = ((c.m.k)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = this;
    Object localObject2 = paramObject;
    Object localObject3 = c.d.a.a.a;
    int m = j;
    Object localObject4;
    Object localObject5;
    Object localObject6;
    Object localObject7;
    Object localObject8;
    Object localObject9;
    Object localObject10;
    boolean bool2;
    Object localObject11;
    int n;
    label687:
    Object localObject14;
    switch (m)
    {
    default: 
      localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)localObject1);
    case 2: 
      localObject4 = (Iterator)g;
      localObject5 = (Iterable)f;
      localObject6 = (c)e;
      localObject7 = d;
      localObject8 = (Iterator)c;
      localObject9 = (Iterable)b;
      localObject10 = (c.m.k)a;
      bool2 = paramObject instanceof o.b;
      if (!bool2)
      {
        localObject11 = localObject5;
        localObject5 = localObject7;
        localObject7 = localObject9;
        n = 1;
        localObject9 = localObject3;
        localObject3 = localObject6;
        localObject6 = localObject8;
        localObject8 = localObject10;
      }
      else
      {
        throw a;
      }
      break;
    case 1: 
      localObject4 = (c)e;
      localObject5 = d;
      localObject6 = (Iterator)c;
      localObject7 = (Iterable)b;
      localObject8 = (c.m.k)a;
      boolean bool3 = paramObject instanceof o.b;
      if (!bool3) {
        n = 1;
      } else {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label1403;
      }
      localObject2 = l;
      localObject4 = (Iterable)k;
      localObject5 = new com/truecaller/ads/qa/d$a$a;
      ((d.a.a)localObject5).<init>();
      localObject5 = (Comparator)localObject5;
      localObject4 = (Iterable)c.a.m.a((Iterable)localObject4, (Comparator)localObject5);
      localObject5 = new java/util/LinkedHashMap;
      ((LinkedHashMap)localObject5).<init>();
      localObject5 = (Map)localObject5;
      localObject4 = ((Iterable)localObject4).iterator();
      for (;;)
      {
        boolean bool5 = ((Iterator)localObject4).hasNext();
        if (!bool5) {
          break;
        }
        localObject6 = ((Iterator)localObject4).next();
        localObject7 = localObject6;
        localObject7 = (String)((Map)localObject6).get("placement");
        localObject8 = ((Map)localObject5).get(localObject7);
        if (localObject8 == null)
        {
          localObject8 = new java/util/ArrayList;
          ((ArrayList)localObject8).<init>();
          ((Map)localObject5).put(localObject7, localObject8);
        }
        localObject8 = (List)localObject8;
        ((List)localObject8).add(localObject6);
      }
      localObject4 = new java/util/ArrayList;
      int i2 = ((Map)localObject5).size();
      ((ArrayList)localObject4).<init>(i2);
      localObject4 = (Collection)localObject4;
      localObject5 = ((Map)localObject5).entrySet().iterator();
      for (;;)
      {
        boolean bool6 = ((Iterator)localObject5).hasNext();
        if (!bool6) {
          break;
        }
        localObject6 = (Map.Entry)((Iterator)localObject5).next();
        localObject7 = (String)((Map.Entry)localObject6).getKey();
        localObject6 = (List)((Map.Entry)localObject6).getValue();
        if (localObject7 == null) {
          localObject7 = "";
        }
        localObject6 = (Iterable)localObject6;
        localObject8 = new java/util/ArrayList;
        int i1 = c.a.m.a((Iterable)localObject6, 10);
        ((ArrayList)localObject8).<init>(i1);
        localObject8 = (Collection)localObject8;
        localObject6 = ((Iterable)localObject6).iterator();
        for (;;)
        {
          boolean bool4 = ((Iterator)localObject6).hasNext();
          if (!bool4) {
            break;
          }
          localObject9 = (Map)((Iterator)localObject6).next();
          localObject10 = (String)((Map)localObject9).get("campaign_id");
          if (localObject10 == null) {
            localObject10 = "";
          }
          Object localObject12 = localObject10;
          localObject10 = (String)((Map)localObject9).get("number");
          if (localObject10 == null) {
            localObject10 = "";
          }
          Object localObject13 = localObject10;
          localObject10 = (String)((Map)localObject9).get("expiration");
          if (localObject10 != null)
          {
            localObject10 = c.n.m.d((String)localObject10);
            if (localObject10 != null)
            {
              l1 = ((Long)localObject10).longValue();
              break label687;
            }
          }
          long l1 = 0L;
          long l2 = l1;
          localObject10 = (String)((Map)localObject9).get("start");
          bool2 = false;
          localObject14 = null;
          int i3;
          if (localObject10 != null)
          {
            localObject10 = c.n.m.b((String)localObject10);
            if (localObject10 != null)
            {
              i3 = ((Integer)localObject10).intValue();
              i5 = i3;
              break label745;
            }
          }
          int i5 = 0;
          label745:
          localObject10 = (String)((Map)localObject9).get("end");
          if (localObject10 != null)
          {
            localObject10 = c.n.m.b((String)localObject10);
            if (localObject10 != null)
            {
              i3 = ((Integer)localObject10).intValue();
              i6 = i3;
              break label793;
            }
          }
          int i6 = 0;
          label793:
          localObject10 = new java/util/LinkedHashMap;
          ((LinkedHashMap)localObject10).<init>();
          localObject9 = ((Map)localObject9).entrySet().iterator();
          for (;;)
          {
            bool2 = ((Iterator)localObject9).hasNext();
            if (!bool2) {
              break;
            }
            localObject14 = (Map.Entry)((Iterator)localObject9).next();
            localObject15 = ((Map.Entry)localObject14).getKey();
            localObject11 = localObject15;
            localObject11 = (String)localObject15;
            localObject1 = e.a();
            boolean bool8 = ((Set)localObject1).contains(localObject11);
            boolean bool9 = true;
            bool8 ^= bool9;
            if (bool8)
            {
              localObject1 = ((Map.Entry)localObject14).getKey();
              localObject11 = ((Map.Entry)localObject14).getValue();
              ((LinkedHashMap)localObject10).put(localObject1, localObject11);
            }
            localObject1 = this;
          }
          localObject10 = (Map)localObject10;
          localObject1 = new java/util/ArrayList;
          int i7 = ((Map)localObject10).size();
          ((ArrayList)localObject1).<init>(i7);
          localObject1 = (Collection)localObject1;
          localObject11 = ((Map)localObject10).entrySet().iterator();
          for (;;)
          {
            bool4 = ((Iterator)localObject11).hasNext();
            if (!bool4) {
              break;
            }
            localObject9 = (Map.Entry)((Iterator)localObject11).next();
            localObject10 = (String)((Map.Entry)localObject9).getKey();
            localObject9 = (String)((Map.Entry)localObject9).getValue();
            localObject14 = new c/n;
            ((n)localObject14).<init>(localObject10, localObject9);
            ((Collection)localObject1).add(localObject14);
          }
          Object localObject15 = localObject1;
          localObject15 = (List)localObject1;
          localObject1 = new com/truecaller/ads/qa/a;
          localObject14 = localObject1;
          ((a)localObject1).<init>((String)localObject12, (String)localObject13, l2, i5, i6, (List)localObject15);
          ((Collection)localObject8).add(localObject1);
          localObject1 = this;
        }
        localObject8 = (List)localObject8;
        localObject1 = new com/truecaller/ads/qa/c;
        ((c)localObject1).<init>((String)localObject7, (List)localObject8);
        ((Collection)localObject4).add(localObject1);
        localObject1 = this;
      }
      localObject4 = (Iterable)localObject4;
      localObject6 = ((Iterable)localObject4).iterator();
      localObject8 = localObject2;
      localObject7 = localObject4;
      localObject1 = this;
    }
    for (;;)
    {
      n = ((Iterator)localObject6).hasNext();
      if (n == 0) {
        break;
      }
      localObject5 = ((Iterator)localObject6).next();
      localObject4 = localObject5;
      localObject4 = (c)localObject5;
      a = localObject8;
      b = localObject7;
      c = localObject6;
      d = localObject5;
      e = localObject4;
      n = 1;
      j = n;
      localObject11 = ((c.m.k)localObject8).a(localObject4, (c.d.c)localObject1);
      if (localObject11 == localObject3) {
        return localObject3;
      }
      localObject11 = (Iterable)b;
      Iterator localIterator = ((Iterable)localObject11).iterator();
      localObject9 = localObject3;
      localObject3 = localObject4;
      localObject4 = localIterator;
      do
      {
        boolean bool7 = ((Iterator)localObject4).hasNext();
        if (!bool7) {
          break;
        }
        localObject10 = ((Iterator)localObject4).next();
        localObject14 = localObject10;
        localObject14 = (a)localObject10;
        a = localObject8;
        b = localObject7;
        c = localObject6;
        d = localObject5;
        e = localObject3;
        f = localObject11;
        g = localObject4;
        h = localObject10;
        i = localObject14;
        int i4 = 2;
        j = i4;
        localObject10 = ((c.m.k)localObject8).a(localObject14, (c.d.c)localObject1);
      } while (localObject10 != localObject9);
      return localObject9;
      localObject3 = localObject9;
    }
    return x.a;
    label1403:
    localObject1 = paramObject;
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.qa.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */