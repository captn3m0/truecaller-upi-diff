package com.truecaller.ads.qa;

import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.common.a;
import java.util.HashMap;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class g
  extends Fragment
{
  public c.d.f a;
  public c.d.f b;
  public com.truecaller.ads.campaigns.e c;
  private HashMap d;
  
  private View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = b.a();
    k.b(this, "receiver$0");
    a locala = com.truecaller.common.h.g.a(getContext());
    paramBundle.a(locala).a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558762, paramViewGroup, false);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.toolbar;
    paramBundle = (Toolbar)a(i);
    Object localObject1 = "toolbar";
    k.a(paramBundle, (String)localObject1);
    paramBundle = paramBundle.getNavigationIcon();
    if (paramBundle != null)
    {
      localObject1 = paramBundle.mutate();
      if (localObject1 != null)
      {
        int j = R.id.toolbar;
        localObject2 = (Toolbar)a(j);
        k.a(localObject2, "toolbar");
        localObject2 = ((Toolbar)localObject2).getContext();
        int k = 2130969592;
        j = com.truecaller.utils.ui.b.a((Context)localObject2, k);
        PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
        ((Drawable)localObject1).setColorFilter(j, localMode);
      }
    }
    int m = R.id.toolbar;
    localObject1 = (Toolbar)a(m);
    Object localObject2 = "toolbar";
    k.a(localObject1, (String)localObject2);
    ((Toolbar)localObject1).setNavigationIcon(paramBundle);
    i = R.id.toolbar;
    paramBundle = (Toolbar)a(i);
    localObject1 = new com/truecaller/ads/qa/g$a;
    ((g.a)localObject1).<init>(this);
    localObject1 = (View.OnClickListener)localObject1;
    paramBundle.setNavigationOnClickListener((View.OnClickListener)localObject1);
    paramBundle = (ag)bg.a;
    localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "asyncCoroutineContext";
      k.a((String)localObject2);
    }
    localObject2 = new com/truecaller/ads/qa/g$b;
    ((g.b)localObject2).<init>(this, paramView, null);
    localObject2 = (m)localObject2;
    kotlinx.coroutines.e.b(paramBundle, (c.d.f)localObject1, (m)localObject2, 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.qa.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */