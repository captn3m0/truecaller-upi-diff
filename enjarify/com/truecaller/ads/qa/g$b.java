package com.truecaller.ads.qa;

import android.view.View;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.ads.campaigns.e;
import java.util.List;
import kotlinx.coroutines.ag;

final class g$b
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag e;
  
  g$b(g paramg, View paramView, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/ads/qa/g$b;
    g localg = c;
    View localView = d;
    localb.<init>(localg, localView, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = b;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label180;
      }
      paramObject = c.c;
      if (paramObject == null)
      {
        localObject1 = "adManager";
        c.g.b.k.a((String)localObject1);
      }
      paramObject = ((e)paramObject).c();
      Object localObject1 = c.a;
      if (localObject1 == null)
      {
        localObject2 = "uiCoroutineContext";
        c.g.b.k.a((String)localObject2);
      }
      Object localObject2 = new com/truecaller/ads/qa/g$b$1;
      ((g.b.1)localObject2).<init>(this, (List)paramObject, null);
      localObject2 = (m)localObject2;
      a = paramObject;
      int j = 1;
      b = j;
      paramObject = kotlinx.coroutines.g.a((f)localObject1, (m)localObject2, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    return x.a;
    label180:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.qa.g.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */