package com.truecaller.ads.qa;

import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import c.a.aa;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.TrueApp;
import com.truecaller.ads.provider.fetch.p;
import com.truecaller.bp;
import com.truecaller.utils.ui.b;
import java.util.HashMap;
import java.util.Set;

public final class l
  extends Fragment
{
  private HashMap a;
  
  private View a(int paramInt)
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      a = ((HashMap)localObject1);
    }
    localObject1 = a;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = a;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558762, paramViewGroup, false);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.toolbar;
    paramBundle = (Toolbar)a(i);
    Object localObject1 = "toolbar";
    k.a(paramBundle, (String)localObject1);
    paramBundle = paramBundle.getNavigationIcon();
    if (paramBundle != null)
    {
      localObject1 = paramBundle.mutate();
      if (localObject1 != null)
      {
        j = R.id.toolbar;
        localObject2 = (Toolbar)a(j);
        k.a(localObject2, "toolbar");
        localObject2 = ((Toolbar)localObject2).getContext();
        int k = 2130969592;
        j = b.a((Context)localObject2, k);
        PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
        ((Drawable)localObject1).setColorFilter(j, localMode);
      }
    }
    int m = R.id.toolbar;
    localObject1 = (Toolbar)a(m);
    k.a(localObject1, "toolbar");
    ((Toolbar)localObject1).setNavigationIcon(paramBundle);
    i = R.id.toolbar;
    paramBundle = (Toolbar)a(i);
    localObject1 = new com/truecaller/ads/qa/l$a;
    ((l.a)localObject1).<init>(this);
    localObject1 = (View.OnClickListener)localObject1;
    paramBundle.setNavigationOnClickListener((View.OnClickListener)localObject1);
    i = R.id.list;
    paramView = (RecyclerView)paramView.findViewById(i);
    k.a(paramView, "view.list");
    paramBundle = TrueApp.y();
    localObject1 = "TrueApp.getApp()";
    k.a(paramBundle, (String)localObject1);
    paramBundle = paramBundle.a().av();
    boolean bool = paramBundle instanceof p;
    int j = 0;
    Object localObject2 = null;
    if (!bool)
    {
      i = 0;
      paramBundle = null;
    }
    paramBundle = (p)paramBundle;
    if (paramBundle != null) {
      localObject2 = paramBundle.a();
    }
    if (localObject2 == null)
    {
      paramBundle = aa.a;
      localObject2 = paramBundle;
      localObject2 = (Set)paramBundle;
    }
    paramBundle = new com/truecaller/ads/qa/j;
    paramBundle.<init>((Set)localObject2);
    paramBundle = (RecyclerView.Adapter)paramBundle;
    paramView.setAdapter(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.qa.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */