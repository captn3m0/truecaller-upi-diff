package com.truecaller.ads;

public enum CustomTemplate
{
  public static final CustomTemplate.a Companion;
  public final boolean openUrl;
  public final String templateId;
  
  static
  {
    Object localObject = new CustomTemplate[5];
    CustomTemplate localCustomTemplate = new com/truecaller/ads/CustomTemplate;
    int i = 1;
    localCustomTemplate.<init>("NATIVE_BANNER", 0, "11726661", i);
    NATIVE_BANNER = localCustomTemplate;
    localObject[0] = localCustomTemplate;
    localCustomTemplate = new com/truecaller/ads/CustomTemplate;
    localCustomTemplate.<init>("NATIVE_BANNER_DUAL_TRACKER", i, "11788491", i);
    NATIVE_BANNER_DUAL_TRACKER = localCustomTemplate;
    localObject[i] = localCustomTemplate;
    localCustomTemplate = new com/truecaller/ads/CustomTemplate;
    int j = 2;
    localCustomTemplate.<init>("CLICK_TO_PLAY_VIDEO", j, "11732026", false);
    CLICK_TO_PLAY_VIDEO = localCustomTemplate;
    localObject[j] = localCustomTemplate;
    localCustomTemplate = new com/truecaller/ads/CustomTemplate;
    j = 3;
    localCustomTemplate.<init>("VIDEO_WITH_FALLBACK_IMAGE", j, "11777280", i);
    VIDEO_WITH_FALLBACK_IMAGE = localCustomTemplate;
    localObject[j] = localCustomTemplate;
    localCustomTemplate = new com/truecaller/ads/CustomTemplate;
    j = 4;
    localCustomTemplate.<init>("NATIVE_CONTENT_DUAL_TRACKER", j, "11788194", i);
    NATIVE_CONTENT_DUAL_TRACKER = localCustomTemplate;
    localObject[j] = localCustomTemplate;
    $VALUES = (CustomTemplate[])localObject;
    localObject = new com/truecaller/ads/CustomTemplate$a;
    ((CustomTemplate.a)localObject).<init>((byte)0);
    Companion = (CustomTemplate.a)localObject;
  }
  
  private CustomTemplate(String paramString1, boolean paramBoolean)
  {
    templateId = paramString1;
    openUrl = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.CustomTemplate
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */