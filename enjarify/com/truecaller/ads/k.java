package com.truecaller.ads;

import c.u;
import java.util.List;

public final class k
{
  public static final k.d n;
  public final String a;
  public final String b;
  public final int c;
  public final List d;
  public final List e;
  public final j f;
  public final int g;
  public final String h;
  public final boolean i;
  public final boolean j;
  public final boolean k;
  public final boolean l;
  public final boolean m;
  
  static
  {
    k.d locald = new com/truecaller/ads/k$d;
    locald.<init>((byte)0);
    n = locald;
  }
  
  public k(k.b paramb)
  {
    this(str2, str3, i1, localList1, localList2, localj, i2, str4, bool1, bool2, bool3, bool4, bool5);
  }
  
  private k(String paramString1, String paramString2, int paramInt1, List paramList1, List paramList2, j paramj, int paramInt2, String paramString3, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5)
  {
    a = paramString1;
    b = paramString2;
    c = paramInt1;
    d = paramList1;
    e = paramList2;
    f = paramj;
    g = paramInt2;
    h = paramString3;
    i = paramBoolean1;
    j = paramBoolean2;
    k = paramBoolean3;
    l = paramBoolean4;
    m = paramBoolean5;
  }
  
  public static final k.a a()
  {
    return k.d.a();
  }
  
  public final boolean equals(Object paramObject)
  {
    Object localObject1 = this;
    localObject1 = (k)this;
    boolean bool1 = true;
    if (localObject1 == paramObject) {
      return bool1;
    }
    localObject1 = getClass();
    if (paramObject != null) {
      localClass = paramObject.getClass();
    } else {
      localClass = null;
    }
    boolean bool2 = c.g.b.k.a(localObject1, localClass) ^ bool1;
    Class localClass = null;
    if (bool2) {
      return false;
    }
    if (paramObject != null)
    {
      localObject1 = a;
      paramObject = (k)paramObject;
      Object localObject2 = a;
      bool2 = c.g.b.k.a(localObject1, localObject2) ^ bool1;
      if (bool2) {
        return false;
      }
      localObject1 = b;
      localObject2 = b;
      bool2 = c.g.b.k.a(localObject1, localObject2) ^ bool1;
      if (bool2) {
        return false;
      }
      int i1 = c;
      int i3 = c;
      if (i1 != i3) {
        return false;
      }
      localObject1 = d;
      localObject2 = d;
      boolean bool3 = c.g.b.k.a(localObject1, localObject2) ^ bool1;
      if (bool3) {
        return false;
      }
      localObject1 = e;
      localObject2 = e;
      bool3 = c.g.b.k.a(localObject1, localObject2) ^ bool1;
      if (bool3) {
        return false;
      }
      localObject1 = f;
      localObject2 = f;
      bool3 = c.g.b.k.a(localObject1, localObject2) ^ bool1;
      if (bool3) {
        return false;
      }
      int i2 = g;
      i3 = g;
      if (i2 != i3) {
        return false;
      }
      localObject1 = h;
      localObject2 = h;
      boolean bool4 = c.g.b.k.a(localObject1, localObject2) ^ bool1;
      if (bool4) {
        return false;
      }
      bool4 = i;
      boolean bool5 = i;
      if (bool4 != bool5) {
        return false;
      }
      bool4 = j;
      bool5 = j;
      if (bool4 != bool5) {
        return false;
      }
      bool4 = k;
      bool5 = k;
      if (bool4 != bool5) {
        return false;
      }
      bool4 = l;
      bool5 = l;
      if (bool4 != bool5) {
        return false;
      }
      bool4 = m;
      boolean bool6 = m;
      if (bool4 != bool6) {
        return false;
      }
      return bool1;
    }
    paramObject = new c/u;
    ((u)paramObject).<init>("null cannot be cast to non-null type com.truecaller.ads.UnitConfig");
    throw ((Throwable)paramObject);
  }
  
  public final int hashCode()
  {
    String str1 = a;
    int i1 = str1.hashCode() * 31;
    String str2 = b;
    int i2 = 0;
    if (str2 != null)
    {
      i3 = str2.hashCode();
    }
    else
    {
      i3 = 0;
      str2 = null;
    }
    i1 = (i1 + i3) * 31;
    int i3 = c;
    i1 = (i1 + i3) * 31;
    i3 = d.hashCode();
    i1 = (i1 + i3) * 31;
    i3 = e.hashCode();
    i1 = (i1 + i3) * 31;
    i3 = f.hashCode();
    i1 = (i1 + i3) * 31;
    i3 = g;
    i1 = (i1 + i3) * 31;
    str2 = h;
    if (str2 != null) {
      i2 = str2.hashCode();
    }
    i1 = (i1 + i2) * 31;
    i3 = Boolean.valueOf(i).hashCode();
    i1 = (i1 + i3) * 31;
    i3 = Boolean.valueOf(j).hashCode();
    i1 = (i1 + i3) * 31;
    i3 = Boolean.valueOf(k).hashCode();
    i1 = (i1 + i3) * 31;
    i3 = Boolean.valueOf(l).hashCode();
    i1 = (i1 + i3) * 31;
    i3 = Boolean.valueOf(m).hashCode();
    return i1 + i3;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("'");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append("'//'");
    str = b;
    localStringBuilder.append(str);
    localStringBuilder.append('\'');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */