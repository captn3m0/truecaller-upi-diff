package com.truecaller.ads;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import c.g.b.k;

public final class AdsContextInitializer
  extends ContentProvider
{
  public final int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    k.b(paramUri, "uri");
    return 0;
  }
  
  public final String getType(Uri paramUri)
  {
    k.b(paramUri, "uri");
    return null;
  }
  
  public final Uri insert(Uri paramUri, ContentValues paramContentValues)
  {
    k.b(paramUri, "uri");
    return null;
  }
  
  public final boolean onCreate()
  {
    Context localContext = getContext();
    k.a(localContext, "context");
    localContext = localContext.getApplicationContext();
    k.a(localContext, "context.applicationContext");
    i.a(localContext);
    return true;
  }
  
  public final Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
  {
    k.b(paramUri, "uri");
    return null;
  }
  
  public final int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    k.b(paramUri, "uri");
    return 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.AdsContextInitializer
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */