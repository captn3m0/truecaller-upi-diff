package com.truecaller.ads;

import com.truecaller.common.h.am;
import java.util.Collections;
import java.util.List;

public final class j
{
  public static final j h;
  public final String a;
  public final int b;
  public final String c;
  public final Integer d;
  public final String e;
  public final String f;
  public final List g;
  
  static
  {
    j.a locala = new com/truecaller/ads/j$a;
    locala.<init>("");
    h = locala.a();
  }
  
  private j(j.a parama)
  {
    Object localObject = a;
    a = ((String)localObject);
    int i = b;
    b = i;
    localObject = c;
    c = ((String)localObject);
    localObject = d;
    d = ((Integer)localObject);
    localObject = e;
    e = ((String)localObject);
    localObject = f;
    f = ((String)localObject);
    localObject = g;
    if (localObject != null) {
      parama = Collections.unmodifiableList(g);
    } else {
      parama = null;
    }
    g = parama;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        paramObject = (j)paramObject;
        int i = b;
        int j = b;
        if (i != j) {
          return false;
        }
        localObject1 = d;
        boolean bool2;
        if (localObject1 != null)
        {
          localObject2 = d;
          bool2 = ((Integer)localObject1).equals(localObject2);
          if (bool2) {
            break label101;
          }
        }
        else
        {
          localObject1 = d;
          if (localObject1 == null) {
            break label101;
          }
        }
        return false;
        label101:
        boolean bool3;
        if (paramObject == null)
        {
          bool3 = true;
        }
        else
        {
          localObject1 = a;
          localObject2 = a;
          bool2 = ((String)localObject1).equals(localObject2);
          if (!bool2)
          {
            bool3 = true;
          }
          else
          {
            localObject1 = c;
            localObject2 = c;
            bool2 = am.a((CharSequence)localObject1, (CharSequence)localObject2);
            if (!bool2)
            {
              bool3 = true;
            }
            else
            {
              localObject1 = e;
              localObject2 = e;
              bool2 = am.a((CharSequence)localObject1, (CharSequence)localObject2);
              if (!bool2)
              {
                bool3 = true;
              }
              else
              {
                localObject1 = f;
                localObject2 = f;
                bool2 = am.a((CharSequence)localObject1, (CharSequence)localObject2);
                if (!bool2)
                {
                  bool3 = true;
                }
                else
                {
                  localObject1 = g;
                  if (localObject1 != null)
                  {
                    paramObject = g;
                    bool3 = ((List)localObject1).equals(paramObject);
                    if (!bool3)
                    {
                      bool3 = true;
                    }
                    else
                    {
                      bool3 = false;
                      paramObject = null;
                    }
                  }
                  else
                  {
                    paramObject = g;
                    if (paramObject != null)
                    {
                      bool3 = true;
                    }
                    else
                    {
                      bool3 = false;
                      paramObject = null;
                    }
                  }
                }
              }
            }
          }
        }
        if (!bool3) {
          return bool1;
        }
        return false;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    String str = a;
    int i = str.hashCode() * 31;
    int j = b;
    i = (i + j) * 31;
    j = c.hashCode();
    i = (i + j) * 31;
    Object localObject = d;
    int k = 0;
    if (localObject != null)
    {
      j = ((Integer)localObject).hashCode();
    }
    else
    {
      j = 0;
      localObject = null;
    }
    i = (i + j) * 31;
    localObject = e;
    if (localObject != null)
    {
      j = ((String)localObject).hashCode();
    }
    else
    {
      j = 0;
      localObject = null;
    }
    i = (i + j) * 31;
    localObject = f;
    if (localObject != null)
    {
      j = ((String)localObject).hashCode();
    }
    else
    {
      j = 0;
      localObject = null;
    }
    i = (i + j) * 31;
    localObject = g;
    if (localObject != null) {
      k = ((List)localObject).hashCode();
    }
    return i + k;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */