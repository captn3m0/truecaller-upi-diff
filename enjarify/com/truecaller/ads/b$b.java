package com.truecaller.ads;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.d;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAd.Image;

public final class b$b
  implements ViewTreeObserver.OnGlobalLayoutListener
{
  public b$b(View paramView, MediaView paramMediaView, NativeAd.Image paramImage, ConstraintLayout paramConstraintLayout, float paramFloat) {}
  
  public final void onGlobalLayout()
  {
    Object localObject1 = a.getViewTreeObserver();
    Object localObject2 = this;
    localObject2 = (ViewTreeObserver.OnGlobalLayoutListener)this;
    ((ViewTreeObserver)localObject1).removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)localObject2);
    localObject1 = c;
    float f1;
    if (localObject1 != null)
    {
      localObject1 = ((NativeAd.Image)localObject1).getDrawable();
    }
    else
    {
      i = 0;
      f1 = 0.0F;
      localObject1 = null;
    }
    localObject2 = b;
    int j = ((MediaView)localObject2).getWidth();
    float f2 = j;
    Object localObject3 = b.getResources();
    int k = R.dimen.ads_mega_image_max_height;
    int m = ((Resources)localObject3).getDimensionPixelSize(k);
    float f3 = m;
    f2 /= f3;
    localObject3 = new android/support/constraint/d;
    ((d)localObject3).<init>();
    ConstraintLayout localConstraintLayout = d;
    ((d)localObject3).a(localConstraintLayout);
    float f4 = e;
    StringBuilder localStringBuilder = null;
    boolean bool = f4 < 0.0F;
    if (bool)
    {
      f1 = f4;
    }
    else
    {
      if (localObject1 != null)
      {
        k = ((Drawable)localObject1).getIntrinsicWidth();
        if (k > 0)
        {
          k = ((Drawable)localObject1).getIntrinsicHeight();
          if (k > 0)
          {
            k = ((Drawable)localObject1).getIntrinsicWidth();
            f4 = k;
            i = ((Drawable)localObject1).getIntrinsicHeight();
            f1 = i;
            f1 = f4 / f1;
            break label205;
          }
        }
      }
      f1 = f2;
    }
    label205:
    k = b.getId();
    localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int i = (int)(Math.max(f1, f2) * 1000.0F);
    localStringBuilder.append(i);
    localStringBuilder.append(":1000");
    localObject1 = localStringBuilder.toString();
    ((d)localObject3).a(k, (String)localObject1);
    localObject1 = d;
    ((d)localObject3).b((ConstraintLayout)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */