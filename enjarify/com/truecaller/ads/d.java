package com.truecaller.ads;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import c.l;
import c.u;
import com.d.b.ab;
import com.d.b.w;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAd.Image;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.truecaller.ads.campaigns.AdCampaign.CtaStyle;
import com.truecaller.ads.provider.holders.AdHolderType;
import com.truecaller.ads.provider.holders.AdNativeHolder;
import com.truecaller.ads.provider.holders.g;
import com.truecaller.ads.ui.CtaButton;
import com.truecaller.ads.ui.VideoFrame;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.log.AssertionUtil;

public final class d
{
  private static final int a(String paramString, int paramInt)
  {
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    boolean bool = TextUtils.isEmpty((CharSequence)localObject);
    if (bool) {
      return paramInt;
    }
    try
    {
      paramInt = Color.parseColor(paramString);
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      paramString = (Throwable)localIllegalArgumentException;
      AssertionUtil.reportThrowableButNeverCrash(paramString);
    }
    return paramInt;
  }
  
  public static final View a(Activity paramActivity, a parama, com.truecaller.ads.provider.holders.e parame)
  {
    k.b(paramActivity, "context");
    k.b(parama, "type");
    k.b(parame, "ad");
    Object localObject1 = parame.a();
    Object localObject2 = e.a;
    int i = ((AdHolderType)localObject1).ordinal();
    i = localObject2[i];
    switch (i)
    {
    default: 
      paramActivity = new c/l;
      paramActivity.<init>();
      throw paramActivity;
    case 4: 
      parame = (com.truecaller.ads.provider.holders.d)parame;
      localObject1 = (NativeCustomTemplateAd)parame.g();
      localObject2 = ((NativeCustomTemplateAd)localObject1).getCustomTemplateId();
      Object localObject3 = CustomTemplate.Companion;
      localObject3 = CustomTemplate.a.a((String)localObject2);
      if (localObject3 != null)
      {
        localObject2 = e.b;
        int j = ((CustomTemplate)localObject3).ordinal();
        int k = localObject2[j];
        switch (k)
        {
        default: 
          paramActivity = new c/l;
          paramActivity.<init>();
          throw paramActivity;
        case 5: 
          paramActivity = (Context)paramActivity;
          return b.a(parame, paramActivity, parama);
        case 4: 
          paramActivity = (Context)paramActivity;
          parama = (NativeCustomTemplateAd)parame.g();
          paramActivity = LayoutInflater.from(paramActivity);
          int m = 2131558483;
          i = 0;
          localObject1 = null;
          paramActivity = paramActivity.inflate(m, null);
          if (paramActivity != null)
          {
            paramActivity = (ViewGroup)paramActivity;
            parame = (CtaButton)paramActivity.findViewById(2131363779);
            k.a(parame, "ctaButton");
            localObject1 = parama.getText("Calltoaction");
            parame.setText((CharSequence)localObject1);
            localObject1 = new com/truecaller/ads/d$c;
            ((d.c)localObject1).<init>(parama);
            localObject1 = (View.OnClickListener)localObject1;
            parame.setOnClickListener((View.OnClickListener)localObject1);
            i = a(parama.getText("CTAbuttoncolor").toString(), -16777216);
            localObject2 = parama.getText("CTAbuttontextcolor").toString();
            j = -1;
            k = a((String)localObject2, j);
            parame.a(i, k);
            parame = (VideoFrame)paramActivity.findViewById(2131362647);
            localObject1 = parama.getVideoMediaView();
            localObject2 = parama.getVideoController();
            localObject3 = new com/truecaller/ads/d$d;
            ((d.d)localObject3).<init>(parama);
            localObject3 = (com.truecaller.ads.ui.d)localObject3;
            boolean bool = parame.a((MediaView)localObject1, (VideoController)localObject2, (com.truecaller.ads.ui.d)localObject3);
            if (bool) {
              parama.recordImpression();
            }
            return (View)paramActivity;
          }
          paramActivity = new c/u;
          paramActivity.<init>("null cannot be cast to non-null type android.view.ViewGroup");
          throw paramActivity;
        case 3: 
          return a(paramActivity, parame);
        case 2: 
          return a((Context)paramActivity, (NativeCustomTemplateAd)localObject1);
        }
        return a((Context)paramActivity, (NativeCustomTemplateAd)localObject1);
      }
      parame = String.valueOf(localObject2);
      AssertionUtil.reportWeirdnessButNeverCrash("Unknown template ID ".concat(parame));
      parama = new android/view/View;
      paramActivity = (Context)paramActivity;
      parama.<init>(paramActivity);
      return parama;
    case 3: 
      paramActivity = (Context)paramActivity;
      parame = (g)parame;
      k.b(paramActivity, "context");
      k.b(parama, "adType");
      k.b(parame, "ad");
      paramActivity = a(paramActivity, parama);
      parama = (com.truecaller.ads.provider.b.e)parame.g();
      parame = a.d;
      a(paramActivity, parama, parame);
      return (View)paramActivity;
    case 2: 
      parame = (AdNativeHolder)parame;
      paramActivity = (Context)paramActivity;
      return b.a(parame, paramActivity, parama);
    }
    return b.a((com.truecaller.ads.provider.holders.a)parame);
  }
  
  private static final View a(Activity paramActivity, com.truecaller.ads.provider.holders.d paramd)
  {
    NativeCustomTemplateAd localNativeCustomTemplateAd = (NativeCustomTemplateAd)paramd.g();
    Object localObject1 = paramActivity;
    localObject1 = LayoutInflater.from((Context)paramActivity);
    boolean bool = false;
    Object localObject2 = null;
    int i = 2131558482;
    localObject1 = ((LayoutInflater)localObject1).inflate(i, null);
    Object localObject3 = localNativeCustomTemplateAd.getText("Textcolor").toString();
    try
    {
      i = Color.parseColor((String)localObject3);
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      localObject3 = (Throwable)localIllegalArgumentException;
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject3);
      i = -16777216;
    }
    Object localObject4 = localNativeCustomTemplateAd.getText("Headline");
    Object localObject5 = localNativeCustomTemplateAd.getText("Body");
    Object localObject6 = localNativeCustomTemplateAd.getImage("Image");
    int j = 2131362645;
    Object localObject7 = (TextView)((View)localObject1).findViewById(j);
    String str = "headlineView";
    k.a(localObject7, str);
    ((TextView)localObject7).setText((CharSequence)localObject4);
    ((TextView)localObject7).setTextColor(i);
    localObject4 = (TextView)((View)localObject1).findViewById(2131362648);
    k.a(localObject4, "secondLineView");
    ((TextView)localObject4).setText((CharSequence)localObject5);
    ((TextView)localObject4).setTextColor(i);
    i = 2131362646;
    localObject3 = (ImageView)((View)localObject1).findViewById(i);
    k.a(localObject3, "mainImageView");
    localObject4 = ((ImageView)localObject3).getResources();
    int k = 2131165290;
    float f = ((Resources)localObject4).getDimension(k);
    int m = (int)f;
    localObject5 = i.b();
    localObject7 = "image";
    k.a(localObject6, (String)localObject7);
    localObject6 = ((NativeAd.Image)localObject6).getUri();
    localObject5 = ((w)localObject5).a((Uri)localObject6);
    localObject6 = null;
    localObject4 = ((ab)localObject5).b(0, m);
    ((ab)localObject4).a((ImageView)localObject3, null);
    localObject2 = paramActivity.getApplicationContext();
    if (localObject2 != null)
    {
      localObject2 = ((bk)localObject2).a().I();
      localObject3 = "featureAdCtpRotation";
      bool = ((com.truecaller.common.g.a)localObject2).b((String)localObject3);
      if (bool)
      {
        localObject2 = new com/truecaller/ads/d$a;
        ((d.a)localObject2).<init>(paramActivity, paramd, localNativeCustomTemplateAd);
        localObject2 = (View.OnClickListener)localObject2;
        ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
      }
      else
      {
        localObject2 = new com/truecaller/ads/d$b;
        ((d.b)localObject2).<init>(paramActivity, paramd, localNativeCustomTemplateAd);
        localObject2 = (View.OnClickListener)localObject2;
        ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
      }
      localNativeCustomTemplateAd.recordImpression();
      k.a(localObject1, "view");
      return (View)localObject1;
    }
    paramActivity = new c/u;
    paramActivity.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramActivity;
  }
  
  private static final View a(Context paramContext, NativeCustomTemplateAd paramNativeCustomTemplateAd)
  {
    paramContext = LayoutInflater.from(paramContext).inflate(2131558484, null);
    int i = 2131362646;
    ImageView localImageView = (ImageView)paramContext.findViewById(i);
    Object localObject1 = new com/truecaller/ads/d$e;
    ((d.e)localObject1).<init>(paramNativeCustomTemplateAd);
    localObject1 = (View.OnClickListener)localObject1;
    localImageView.setOnClickListener((View.OnClickListener)localObject1);
    localObject1 = paramNativeCustomTemplateAd.getImage("Image");
    if (localObject1 != null)
    {
      k.a(localImageView, "imageView");
      float f = localImageView.getResources().getDimension(2131165290);
      int j = (int)f;
      w localw = i.b();
      localObject1 = ((NativeAd.Image)localObject1).getUri();
      localObject1 = localw.a((Uri)localObject1);
      localw = null;
      localObject1 = ((ab)localObject1).b(0, j);
      Object localObject2 = new com/truecaller/ads/d$f;
      ((d.f)localObject2).<init>(paramNativeCustomTemplateAd, localImageView);
      localObject2 = (com.d.b.e)localObject2;
      ((ab)localObject1).a(localImageView, (com.d.b.e)localObject2);
    }
    k.a(paramContext, "view");
    return paramContext;
  }
  
  public static final View a(Context paramContext, a parama, ViewGroup paramViewGroup)
  {
    k.b(paramContext, "context");
    k.b(parama, "adType");
    paramContext = LayoutInflater.from(paramContext);
    int i = parama.getPlaceholderLayout();
    paramContext = paramContext.inflate(i, paramViewGroup, false);
    k.a(paramContext, "LayoutInflater.from(cont…derLayout, parent, false)");
    return paramContext;
  }
  
  public static final com.truecaller.ads.ui.c a(Context paramContext, a parama)
  {
    k.b(paramContext, "context");
    k.b(parama, "adType");
    com.truecaller.ads.ui.c localc = new com/truecaller/ads/ui/c;
    localc.<init>(paramContext, parama);
    return localc;
  }
  
  public static final void a(com.truecaller.ads.ui.c paramc, com.truecaller.ads.provider.b.e parame, AdCampaign.CtaStyle paramCtaStyle)
  {
    k.b(paramc, "adView");
    k.b(parame, "ad");
    paramc.a(parame, paramCtaStyle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */