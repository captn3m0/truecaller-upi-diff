package com.truecaller.ads.provider;

import com.truecaller.ads.h;
import com.truecaller.ads.k;
import com.truecaller.ads.provider.holders.e;

public abstract class o
  extends h
{
  private final f a;
  private final k b;
  
  protected o(f paramf, k paramk)
  {
    a = paramf;
    b = paramk;
    paramf = a;
    paramk = b;
    paramf.a(paramk, this);
  }
  
  public final void a()
  {
    Object localObject = a;
    k localk = b;
    localObject = ((f)localObject).a(localk, 0);
    if (localObject != null)
    {
      b();
      a((e)localObject);
    }
  }
  
  public abstract void a(e parame);
  
  public final void b()
  {
    f localf = a;
    k localk = b;
    localf.b(localk, this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */