package com.truecaller.ads.provider;

import com.truecaller.androidactors.k;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class i
  implements d
{
  private final Provider a;
  
  private i(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static i a(Provider paramProvider)
  {
    i locali = new com/truecaller/ads/provider/i;
    locali.<init>(paramProvider);
    return locali;
  }
  
  public static com.truecaller.androidactors.i a(k paramk)
  {
    return (com.truecaller.androidactors.i)g.a(paramk.a("adsProvider"), "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */