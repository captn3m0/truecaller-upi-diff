package com.truecaller.ads.provider.fetch;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

final class AdsConsentRefreshTask$a$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  AdsConsentRefreshTask$a$a(AdsConfigurationManager.a parama, c paramc, AdsConsentRefreshTask.a parama1)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/ads/provider/fetch/AdsConsentRefreshTask$a$a;
    AdsConfigurationManager.a locala1 = b;
    AdsConsentRefreshTask.a locala2 = c;
    locala.<init>(locala1, paramc, locala2);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = c.e;
        localObject = b.a;
        AdsConfigurationManager.PromotionState localPromotionState = b.b;
        ((AdsConfigurationManager)paramObject).a((AdsConfigurationManager.TargetingState)localObject, localPromotionState);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.AdsConsentRefreshTask.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */