package com.truecaller.ads.provider.fetch;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.common.account.r;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.h.ac;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class AdsConsentRefreshTask$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag g;
  
  AdsConsentRefreshTask$a(r paramr, ac paramac, AdsConfigurationManager paramAdsConfigurationManager, f paramf, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/ads/provider/fetch/AdsConsentRefreshTask$a;
    r localr = c;
    ac localac = d;
    AdsConfigurationManager localAdsConfigurationManager = e;
    f localf = f;
    locala.<init>(localr, localac, localAdsConfigurationManager, localf, paramc);
    paramObject = (ag)paramObject;
    g = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = b;
    boolean bool2;
    Object localObject1;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label267;
      }
      throw a;
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label273;
      }
      paramObject = d;
      int k = ((ac)paramObject).a();
      bool1 = false;
      if (k == 0) {
        break label221;
      }
      paramObject = c;
      k = ((r)paramObject).c();
      if (k == 0) {
        break label221;
      }
      paramObject = e.c();
      if (paramObject == null) {
        break label217;
      }
      localObject1 = f;
      Object localObject2 = new com/truecaller/ads/provider/fetch/AdsConsentRefreshTask$a$a;
      ((AdsConsentRefreshTask.a.a)localObject2).<init>((AdsConfigurationManager.a)paramObject, null, this);
      localObject2 = (m)localObject2;
      a = paramObject;
      k = 1;
      b = k;
      paramObject = g.a((f)localObject1, (m)localObject2, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    paramObject = PersistentBackgroundTask.RunResult.Success;
    if (paramObject == null)
    {
      label217:
      return PersistentBackgroundTask.RunResult.FailedRetry;
      label221:
      paramObject = f;
      localObject1 = new com/truecaller/ads/provider/fetch/AdsConsentRefreshTask$a$1;
      ((AdsConsentRefreshTask.a.1)localObject1).<init>(this, null);
      localObject1 = (m)localObject1;
      int j = 2;
      b = j;
      paramObject = g.a((f)paramObject, (m)localObject1, this);
      if (paramObject == locala) {
        return locala;
      }
      label267:
      paramObject = PersistentBackgroundTask.RunResult.Success;
    }
    return paramObject;
    label273:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.AdsConsentRefreshTask.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */