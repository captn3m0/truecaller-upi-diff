package com.truecaller.ads.provider.fetch;

import com.truecaller.utils.d;

public final class t
  implements g
{
  private final c.d.f a;
  private final com.truecaller.utils.a b;
  private final d c;
  private final com.truecaller.ads.provider.a d;
  private final com.truecaller.i.a e;
  private final com.truecaller.ads.provider.a.a f;
  private final l g;
  private final AdsConfigurationManager h;
  
  public t(c.d.f paramf, com.truecaller.utils.a parama, d paramd, com.truecaller.ads.provider.a parama1, com.truecaller.i.a parama2, com.truecaller.ads.provider.a.a parama3, l paraml, AdsConfigurationManager paramAdsConfigurationManager)
  {
    a = paramf;
    b = parama;
    c = paramd;
    d = parama1;
    e = parama2;
    f = parama3;
    g = paraml;
    h = paramAdsConfigurationManager;
  }
  
  public final f a(k paramk, com.truecaller.ads.k paramk1)
  {
    c.g.b.k.b(paramk, "callback");
    c.g.b.k.b(paramk1, "config");
    h localh = new com/truecaller/ads/provider/fetch/h;
    c.d.f localf = a;
    com.truecaller.utils.a locala = b;
    d locald = c;
    com.truecaller.ads.provider.a locala1 = d;
    com.truecaller.i.a locala2 = e;
    com.truecaller.ads.provider.a.a locala3 = f;
    l locall = g;
    AdsConfigurationManager localAdsConfigurationManager = h;
    localh.<init>(paramk1, localf, paramk, locala, locald, locala1, locala2, locala3, locall, localAdsConfigurationManager);
    return (f)localh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */