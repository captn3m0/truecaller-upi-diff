package com.truecaller.ads.provider.fetch;

import c.g.a.m;
import c.g.b.k;
import com.truecaller.common.background.b;
import com.truecaller.common.background.b.a;
import com.truecaller.common.h.ac;
import com.truecaller.common.network.optout.OptOutRestAdapter.OptOutsDto;
import com.truecaller.old.data.access.Settings;
import java.util.List;

public final class q
  implements AdsConfigurationManager
{
  private AdsConfigurationManager.a a;
  private long b;
  private boolean c;
  private final com.truecaller.common.network.optout.a d;
  private final com.truecaller.utils.a e;
  private final com.truecaller.i.a f;
  private final b g;
  private final ac h;
  private final com.truecaller.ads.provider.fetch.a.a i;
  
  public q(com.truecaller.common.network.optout.a parama, com.truecaller.utils.a parama1, com.truecaller.i.a parama2, b paramb, ac paramac, com.truecaller.ads.provider.fetch.a.a parama3)
  {
    d = parama;
    e = parama1;
    f = parama2;
    g = paramb;
    h = paramac;
    i = parama3;
    parama = f;
    long l1 = 0L;
    long l2 = parama.a("adsTargetingRefreshTimestamp", l1);
    b = l2;
    parama = f;
    parama2 = AdsConfigurationManager.TargetingState.UNKNOWN.getKey();
    parama = parama.b("adsTargetingLastValue", parama2);
    parama1 = AdsConfigurationManager.TargetingState.TARGETING.getKey();
    boolean bool1 = k.a(parama, parama1);
    if (bool1)
    {
      parama = AdsConfigurationManager.TargetingState.TARGETING;
    }
    else
    {
      parama1 = AdsConfigurationManager.TargetingState.NON_TARGETING.getKey();
      boolean bool2 = k.a(parama, parama1);
      if (bool2) {
        parama = AdsConfigurationManager.TargetingState.NON_TARGETING;
      } else {
        parama = AdsConfigurationManager.TargetingState.UNKNOWN;
      }
    }
    parama1 = f;
    paramb = AdsConfigurationManager.PromotionState.UNKNOWN.getKey();
    parama1 = parama1.b("promotionConsentLastValue", paramb);
    parama2 = AdsConfigurationManager.PromotionState.OPT_IN.getKey();
    boolean bool3 = k.a(parama1, parama2);
    if (bool3)
    {
      parama1 = AdsConfigurationManager.PromotionState.OPT_IN;
    }
    else
    {
      parama2 = AdsConfigurationManager.PromotionState.OPT_OUT.getKey();
      bool1 = k.a(parama1, parama2);
      if (bool1) {
        parama1 = AdsConfigurationManager.PromotionState.OPT_OUT;
      } else {
        parama1 = AdsConfigurationManager.PromotionState.UNKNOWN;
      }
    }
    parama2 = new com/truecaller/ads/provider/fetch/AdsConfigurationManager$a;
    parama2.<init>(parama, parama1);
    a = parama2;
    parama = a;
    a(parama);
    g();
  }
  
  private final void a(AdsConfigurationManager.a parama)
  {
    i.a(parama);
  }
  
  private final void g()
  {
    long l1 = b;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (!bool) {
      f();
    }
  }
  
  private final void h()
  {
    long l1 = e.a();
    b = l1;
    Object localObject = f;
    long l2 = b;
    ((com.truecaller.i.a)localObject).b("adsTargetingRefreshTimestamp", l2);
    localObject = a;
    a((AdsConfigurationManager.a)localObject);
  }
  
  public final void a(AdsConfigurationManager.PromotionState paramPromotionState)
  {
    k.b(paramPromotionState, "state");
    paramPromotionState = AdsConfigurationManager.a.a(a, null, paramPromotionState, 1);
    a = paramPromotionState;
    paramPromotionState = f;
    String str = a.b.getKey();
    paramPromotionState.a("promotionConsentLastValue", str);
    h();
  }
  
  public final void a(AdsConfigurationManager.TargetingState paramTargetingState)
  {
    k.b(paramTargetingState, "state");
    paramTargetingState = AdsConfigurationManager.a.a(a, paramTargetingState, null, 2);
    a = paramTargetingState;
    paramTargetingState = f;
    String str = a.a.getKey();
    paramTargetingState.a("adsTargetingLastValue", str);
    h();
  }
  
  public final void a(AdsConfigurationManager.TargetingState paramTargetingState, AdsConfigurationManager.PromotionState paramPromotionState)
  {
    k.b(paramTargetingState, "targetingState");
    k.b(paramPromotionState, "promotionState");
    paramTargetingState = AdsConfigurationManager.a.a(paramTargetingState, paramPromotionState);
    a = paramTargetingState;
    paramTargetingState = f;
    String str = a.a.getKey();
    paramTargetingState.a("adsTargetingLastValue", str);
    paramTargetingState = f;
    str = a.b.getKey();
    paramTargetingState.a("promotionConsentLastValue", str);
    h();
  }
  
  public final boolean a()
  {
    Object localObject = h;
    boolean bool1 = ((ac)localObject).a();
    boolean bool2 = true;
    if (!bool1) {
      return bool2;
    }
    g();
    localObject = a.a;
    AdsConfigurationManager.TargetingState localTargetingState = AdsConfigurationManager.TargetingState.TARGETING;
    if (localObject == localTargetingState) {
      return bool2;
    }
    return false;
  }
  
  public final AdsConfigurationManager.PromotionState b()
  {
    g();
    return a.b;
  }
  
  public final AdsConfigurationManager.a c()
  {
    Object localObject1 = d.e();
    if (localObject1 == null) {
      return null;
    }
    Object localObject2 = ((OptOutRestAdapter.OptOutsDto)localObject1).getOptIns();
    boolean bool1 = ((List)localObject2).contains("ads");
    Object localObject3 = ((OptOutRestAdapter.OptOutsDto)localObject1).getOptOuts();
    Object localObject4 = "ads";
    boolean bool2 = ((List)localObject3).contains(localObject4);
    if (bool1)
    {
      localObject2 = AdsConfigurationManager.TargetingState.TARGETING;
    }
    else if (bool2)
    {
      localObject2 = AdsConfigurationManager.TargetingState.NON_TARGETING;
    }
    else
    {
      bool1 = ((OptOutRestAdapter.OptOutsDto)localObject1).getConsentRefresh();
      if (bool1) {
        localObject2 = AdsConfigurationManager.TargetingState.TARGETING;
      } else {
        localObject2 = AdsConfigurationManager.TargetingState.UNKNOWN;
      }
    }
    localObject3 = ((OptOutRestAdapter.OptOutsDto)localObject1).getOptIns();
    bool2 = ((List)localObject3).contains("dm");
    localObject4 = ((OptOutRestAdapter.OptOutsDto)localObject1).getOptOuts();
    String str = "dm";
    boolean bool3 = ((List)localObject4).contains(str);
    if (bool2)
    {
      localObject1 = AdsConfigurationManager.PromotionState.OPT_IN;
    }
    else if (bool3)
    {
      localObject1 = AdsConfigurationManager.PromotionState.OPT_OUT;
    }
    else
    {
      ((OptOutRestAdapter.OptOutsDto)localObject1).getConsentRefresh();
      localObject1 = AdsConfigurationManager.PromotionState.UNKNOWN;
    }
    localObject3 = new com/truecaller/ads/provider/fetch/AdsConfigurationManager$a;
    ((AdsConfigurationManager.a)localObject3).<init>((AdsConfigurationManager.TargetingState)localObject2, (AdsConfigurationManager.PromotionState)localObject1);
    return (AdsConfigurationManager.a)localObject3;
  }
  
  public final boolean d()
  {
    return Settings.h();
  }
  
  public final void e()
  {
    f.d("adsTargetingRefreshTimestamp");
    f.d("adsTargetingLastValue");
    f.d("promotionConsentLastValue");
    b = 0L;
    AdsConfigurationManager.a locala = new com/truecaller/ads/provider/fetch/AdsConfigurationManager$a;
    AdsConfigurationManager.TargetingState localTargetingState = AdsConfigurationManager.TargetingState.UNKNOWN;
    AdsConfigurationManager.PromotionState localPromotionState = AdsConfigurationManager.PromotionState.UNKNOWN;
    locala.<init>(localTargetingState, localPromotionState);
    a = locala;
  }
  
  public final void f()
  {
    boolean bool = c;
    if (!bool)
    {
      bool = true;
      c = bool;
      b localb = g;
      int j = 10025;
      Object localObject1 = new com/truecaller/ads/provider/fetch/q$a;
      Object localObject2 = this;
      localObject2 = (q)this;
      ((q.a)localObject1).<init>((q)localObject2);
      localObject1 = (m)localObject1;
      localObject2 = new com/truecaller/ads/provider/fetch/r;
      ((r)localObject2).<init>((m)localObject1);
      localObject2 = (b.a)localObject2;
      localObject1 = null;
      localb.a(j, null, (b.a)localObject2, null);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */