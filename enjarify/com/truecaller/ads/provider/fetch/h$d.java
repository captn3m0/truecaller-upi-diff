package com.truecaller.ads.provider.fetch;

import c.d.b.a.b;
import c.d.c;
import c.o.b;
import c.x;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ar;
import kotlinx.coroutines.cq;

final class h$d
  extends c.d.b.a.k
  implements c.g.a.m
{
  long a;
  long b;
  int c;
  private ag e;
  
  h$d(h paramh, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/ads/provider/fetch/h$d;
    h localh = d;
    locald.<init>(localh, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    c.d.a.a locala = c.d.a.a.a;
    int i = c;
    long l1;
    long l2;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label333;
      }
      throw a;
    case 1: 
      l1 = b;
      l2 = a;
      boolean bool3 = paramObject instanceof o.b;
      if (bool3) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label348;
      }
      l2 = d.f.c();
      paramObject = (Iterable)d.c;
      Object localObject = new java/util/ArrayList;
      int j = c.a.m.a((Iterable)paramObject, 10);
      ((ArrayList)localObject).<init>(j);
      localObject = (Collection)localObject;
      paramObject = ((Iterable)paramObject).iterator();
      for (;;)
      {
        boolean bool4 = ((Iterator)paramObject).hasNext();
        if (!bool4) {
          break;
        }
        l3 = ((v)((Iterator)paramObject).next()).a(l2);
        Long localLong = b.a(l3);
        ((Collection)localObject).add(localLong);
      }
      localObject = (Iterable)localObject;
      paramObject = (Long)c.a.m.m((Iterable)localObject);
      if (paramObject == null) {
        break label344;
      }
      l1 = ((Long)paramObject).longValue();
      long l3 = 0L;
      int k = l1 < l3;
      if (k <= 0) {
        break label333;
      }
      a = l2;
      b = l1;
      k = 1;
      c = k;
      paramObject = ar.a(l1, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    a = l2;
    b = l1;
    int m = 2;
    c = m;
    paramObject = cq.a(this);
    if (paramObject == locala) {
      return locala;
    }
    label333:
    h.a(d);
    return x.a;
    label344:
    return x.a;
    label348:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.h.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */