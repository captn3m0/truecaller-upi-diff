package com.truecaller.ads.provider.fetch;

import com.google.android.gms.ads.AdListener;

public final class b
  extends AdListener
{
  c.g.a.b a;
  a b;
  
  public final void onAdClicked()
  {
    a locala = b;
    if (locala != null)
    {
      locala.a();
      return;
    }
  }
  
  public final void onAdFailedToLoad(int paramInt)
  {
    c.g.a.b localb = a;
    if (localb != null)
    {
      Integer localInteger = Integer.valueOf(paramInt);
      localb.invoke(localInteger);
    }
    a = null;
  }
  
  public final void onAdImpression() {}
  
  public final void onAdLoaded()
  {
    a = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */