package com.truecaller.ads.provider.fetch;

import com.truecaller.ads.campaigns.AdCampaign.CtaStyle;
import com.truecaller.ads.campaigns.AdCampaign.Style;
import com.truecaller.ads.k;

public final class c
{
  public final k a;
  public final String b;
  final String[] c;
  public final AdCampaign.CtaStyle d;
  final boolean e;
  public final boolean f;
  public final String g;
  private final AdCampaign.Style h;
  
  public c(k paramk, String paramString1, String[] paramArrayOfString, AdCampaign.Style paramStyle, AdCampaign.CtaStyle paramCtaStyle, boolean paramBoolean1, boolean paramBoolean2, String paramString2)
  {
    a = paramk;
    b = paramString1;
    c = paramArrayOfString;
    h = paramStyle;
    d = paramCtaStyle;
    e = paramBoolean1;
    f = paramBoolean2;
    g = paramString2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */