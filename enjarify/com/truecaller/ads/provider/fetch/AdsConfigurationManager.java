package com.truecaller.ads.provider.fetch;

public abstract interface AdsConfigurationManager
{
  public abstract void a(AdsConfigurationManager.PromotionState paramPromotionState);
  
  public abstract void a(AdsConfigurationManager.TargetingState paramTargetingState);
  
  public abstract void a(AdsConfigurationManager.TargetingState paramTargetingState, AdsConfigurationManager.PromotionState paramPromotionState);
  
  public abstract boolean a();
  
  public abstract AdsConfigurationManager.PromotionState b();
  
  public abstract AdsConfigurationManager.a c();
  
  public abstract boolean d();
  
  public abstract void e();
  
  public abstract void f();
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.AdsConfigurationManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */