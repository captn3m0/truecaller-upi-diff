package com.truecaller.ads.provider.fetch.a;

import android.content.Context;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.mopub.common.MoPub;
import com.mopub.common.SdkConfiguration;
import com.mopub.common.SdkConfiguration.Builder;
import com.mopub.common.SdkInitializationListener;
import kotlinx.coroutines.ag;

final class b$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  b$b(b paramb, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/ads/provider/fetch/a/b$b;
    b localb1 = b;
    localb.<init>(localb1, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = b.c;
        localObject1 = new com/mopub/common/SdkConfiguration$Builder;
        ((SdkConfiguration.Builder)localObject1).<init>("c899990e3188405ab22656014f6e26d7");
        localObject1 = ((SdkConfiguration.Builder)localObject1).build();
        Object localObject2 = new com/truecaller/ads/provider/fetch/a/b$b$1;
        ((b.b.1)localObject2).<init>(this);
        localObject2 = (SdkInitializationListener)localObject2;
        MoPub.initializeSdk((Context)paramObject, (SdkConfiguration)localObject1, (SdkInitializationListener)localObject2);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.a.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */