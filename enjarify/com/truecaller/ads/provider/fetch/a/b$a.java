package com.truecaller.ads.provider.fetch.a;

import c.d.a.a;
import c.g.a.m;
import c.o.b;
import c.x;
import com.google.ads.mediation.inmobi.InMobiConsent;
import com.truecaller.ads.provider.fetch.AdsConfigurationManager.TargetingState;
import com.truecaller.ads.provider.fetch.AdsConfigurationManager.a;
import com.truecaller.common.h.ac;
import kotlinx.coroutines.ag;
import org.json.JSONObject;

final class b$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  b$a(b paramb, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/ads/provider/fetch/a/b$a;
    b localb = b;
    locala.<init>(localb, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = new org/json/JSONObject;
        ((JSONObject)paramObject).<init>();
        localObject1 = b.a;
        if (localObject1 == null)
        {
          localObject2 = "currentState";
          c.g.b.k.a((String)localObject2);
        }
        localObject1 = a;
        Object localObject2 = c.a;
        int j = ((AdsConfigurationManager.TargetingState)localObject1).ordinal();
        j = localObject2[j];
        switch (j)
        {
        default: 
          break;
        case 3: 
          localObject1 = "gdpr_consent_available";
          bool2 = false;
          localObject2 = null;
          ((JSONObject)paramObject).put((String)localObject1, false);
          break;
        case 2: 
          localObject1 = "gdpr_consent_available";
          bool2 = true;
          ((JSONObject)paramObject).put((String)localObject1, bool2);
          break;
        case 1: 
          localObject1 = "gdpr_consent_available";
          localObject2 = "";
          ((JSONObject)paramObject).put((String)localObject1, localObject2);
        }
        boolean bool2 = b.b.a();
        ((JSONObject)paramObject).put("gdpr", bool2);
        InMobiConsent.updateGDPRConsent((JSONObject)paramObject);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.a.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */