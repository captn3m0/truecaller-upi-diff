package com.truecaller.ads.provider.fetch.a;

import android.content.Context;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.mopub.common.MoPub;
import com.truecaller.ads.provider.fetch.AdsConfigurationManager.a;
import com.truecaller.common.h.ac;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.e;

public final class b
  implements a, ag
{
  public AdsConfigurationManager.a a;
  final ac b;
  final Context c;
  private boolean d;
  private final f e;
  
  public b(ac paramac, Context paramContext, f paramf)
  {
    b = paramac;
    c = paramContext;
    e = paramf;
  }
  
  public final f V_()
  {
    return e;
  }
  
  public final void a(AdsConfigurationManager.a parama)
  {
    k.b(parama, "targetingState");
    Object localObject = this;
    localObject = a;
    boolean bool1 = true;
    String str;
    if (localObject != null)
    {
      localObject = a;
      if (localObject == null)
      {
        str = "currentState";
        k.a(str);
      }
      boolean bool2 = k.a(localObject, parama) ^ bool1;
      if (!bool2) {}
    }
    else
    {
      a = parama;
      parama = e;
      localObject = new com/truecaller/ads/provider/fetch/a/b$a;
      str = null;
      ((b.a)localObject).<init>(this, null);
      localObject = (m)localObject;
      int i = 2;
      e.b(this, parama, (m)localObject, i);
      boolean bool3 = MoPub.isSdkInitialized();
      if (!bool3)
      {
        bool3 = d;
        if (!bool3)
        {
          d = bool1;
          parama = e;
          localObject = new com/truecaller/ads/provider/fetch/a/b$b;
          ((b.b)localObject).<init>(this, null);
          localObject = (m)localObject;
          e.b(this, parama, (m)localObject, i);
        }
      }
      else
      {
        parama = e;
        localObject = new com/truecaller/ads/provider/fetch/a/b$c;
        ((b.c)localObject).<init>(this, null);
        localObject = (m)localObject;
        e.b(this, parama, (m)localObject, i);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */