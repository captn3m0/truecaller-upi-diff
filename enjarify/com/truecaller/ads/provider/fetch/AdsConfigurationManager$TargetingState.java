package com.truecaller.ads.provider.fetch;

public enum AdsConfigurationManager$TargetingState
{
  private final String key;
  
  static
  {
    TargetingState[] arrayOfTargetingState = new TargetingState[3];
    TargetingState localTargetingState = new com/truecaller/ads/provider/fetch/AdsConfigurationManager$TargetingState;
    localTargetingState.<init>("UNKNOWN", 0, "");
    UNKNOWN = localTargetingState;
    arrayOfTargetingState[0] = localTargetingState;
    localTargetingState = new com/truecaller/ads/provider/fetch/AdsConfigurationManager$TargetingState;
    int i = 1;
    localTargetingState.<init>("TARGETING", i, "T");
    TARGETING = localTargetingState;
    arrayOfTargetingState[i] = localTargetingState;
    localTargetingState = new com/truecaller/ads/provider/fetch/AdsConfigurationManager$TargetingState;
    i = 2;
    localTargetingState.<init>("NON_TARGETING", i, "N");
    NON_TARGETING = localTargetingState;
    arrayOfTargetingState[i] = localTargetingState;
    $VALUES = arrayOfTargetingState;
  }
  
  private AdsConfigurationManager$TargetingState(String paramString1)
  {
    key = paramString1;
  }
  
  public final String getKey()
  {
    return key;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.AdsConfigurationManager.TargetingState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */