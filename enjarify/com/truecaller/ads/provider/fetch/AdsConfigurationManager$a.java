package com.truecaller.ads.provider.fetch;

import c.g.b.k;

public final class AdsConfigurationManager$a
{
  public final AdsConfigurationManager.TargetingState a;
  final AdsConfigurationManager.PromotionState b;
  
  public AdsConfigurationManager$a(AdsConfigurationManager.TargetingState paramTargetingState, AdsConfigurationManager.PromotionState paramPromotionState)
  {
    a = paramTargetingState;
    b = paramPromotionState;
  }
  
  public static a a(AdsConfigurationManager.TargetingState paramTargetingState, AdsConfigurationManager.PromotionState paramPromotionState)
  {
    k.b(paramTargetingState, "adsTargetingState");
    k.b(paramPromotionState, "promotionState");
    a locala = new com/truecaller/ads/provider/fetch/AdsConfigurationManager$a;
    locala.<init>(paramTargetingState, paramPromotionState);
    return locala;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof a;
      if (bool1)
      {
        paramObject = (a)paramObject;
        Object localObject = a;
        AdsConfigurationManager.TargetingState localTargetingState = a;
        bool1 = k.a(localObject, localTargetingState);
        if (bool1)
        {
          localObject = b;
          paramObject = b;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final int hashCode()
  {
    AdsConfigurationManager.TargetingState localTargetingState = a;
    int i = 0;
    int j;
    if (localTargetingState != null)
    {
      j = localTargetingState.hashCode();
    }
    else
    {
      j = 0;
      localTargetingState = null;
    }
    j *= 31;
    AdsConfigurationManager.PromotionState localPromotionState = b;
    if (localPromotionState != null) {
      i = localPromotionState.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("UserConsents(adsTargetingState=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", promotionState=");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.AdsConfigurationManager.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */