package com.truecaller.ads.provider.fetch;

import android.content.Context;
import android.os.Bundle;
import c.g.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.background.e;
import com.truecaller.common.background.e.a;
import com.truecaller.common.h.ac;
import java.util.concurrent.TimeUnit;

public final class AdsConsentRefreshTask
  extends PersistentBackgroundTask
{
  public final int a()
  {
    return 10025;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    paramBundle = "serviceContext";
    k.b(paramContext, paramBundle);
    paramContext = paramContext.getApplicationContext();
    if (paramContext != null)
    {
      paramContext = ((bk)paramContext).a();
      k.a(paramContext, "(serviceContext.applicat…GraphHolder).objectsGraph");
      AdsConfigurationManager localAdsConfigurationManager = paramContext.aw();
      k.a(localAdsConfigurationManager, "objectsGraph.adsConfigurationManager()");
      c.d.f localf = paramContext.bl();
      k.a(localf, "objectsGraph.uiCoroutineContext()");
      ac localac = paramContext.J();
      k.a(localac, "objectsGraph.regionUtils()");
      r localr = paramContext.T();
      k.a(localr, "objectsGraph.accountManager()");
      paramContext = new com/truecaller/ads/provider/fetch/AdsConsentRefreshTask$a;
      paramContext.<init>(localr, localac, localAdsConfigurationManager, localf, null);
      return (PersistentBackgroundTask.RunResult)kotlinx.coroutines.f.a((m)paramContext);
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramContext;
  }
  
  public final boolean a(Context paramContext)
  {
    k.b(paramContext, "serviceContext");
    return true;
  }
  
  public final e b()
  {
    Object localObject = new com/truecaller/common/background/e$a;
    int i = 1;
    ((e.a)localObject).<init>(i);
    TimeUnit localTimeUnit = TimeUnit.DAYS;
    localObject = ((e.a)localObject).a(7, localTimeUnit);
    localTimeUnit = TimeUnit.HOURS;
    localObject = ((e.a)localObject).b(12, localTimeUnit);
    localTimeUnit = TimeUnit.HOURS;
    localObject = ((e.a)localObject).c(1L, localTimeUnit).a(i).a().b();
    k.a(localObject, "TaskConfiguration.Builde…lse)\n            .build()");
    return (e)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.AdsConsentRefreshTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */