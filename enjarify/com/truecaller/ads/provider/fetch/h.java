package com.truecaller.ads.provider.fetch;

import c.g.a.m;
import java.util.ArrayDeque;
import java.util.Iterator;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.bs;
import kotlinx.coroutines.e.b;

public final class h
  implements f, ag
{
  final b a;
  boolean b;
  final ArrayDeque c;
  final com.truecaller.ads.k d;
  final k e;
  final com.truecaller.utils.a f;
  final com.truecaller.i.a g;
  final AdsConfigurationManager h;
  private bn i;
  private bn j;
  private final boolean k;
  private final c.d.f l;
  private final com.truecaller.ads.provider.a m;
  private final com.truecaller.ads.provider.a.a n;
  private final l o;
  
  public h(com.truecaller.ads.k paramk, c.d.f paramf, k paramk1, com.truecaller.utils.a parama, com.truecaller.utils.d paramd, com.truecaller.ads.provider.a parama1, com.truecaller.i.a parama2, com.truecaller.ads.provider.a.a parama3, l paraml, AdsConfigurationManager paramAdsConfigurationManager)
  {
    d = paramk;
    l = paramf;
    e = paramk1;
    f = parama;
    m = parama1;
    g = parama2;
    n = parama3;
    o = paraml;
    h = paramAdsConfigurationManager;
    paramf = bs.a(null);
    i = paramf;
    paramf = kotlinx.coroutines.e.d.a();
    a = paramf;
    paramk = bs.a(null);
    j = paramk;
    paramk = new java/util/ArrayDeque;
    paramk.<init>();
    c = paramk;
    int i1 = paramd.o();
    int i2 = 11000000;
    if (i1 >= i2)
    {
      i2 = 11460000;
      if (i1 <= i2)
      {
        i1 = 0;
        paramk = null;
        break label213;
      }
    }
    i1 = 1;
    label213:
    k = i1;
  }
  
  private final void b(boolean paramBoolean)
  {
    Object localObject = new com/truecaller/ads/provider/fetch/h$e;
    ((h.e)localObject).<init>(this, paramBoolean, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.b(this, null, (m)localObject, 3);
  }
  
  private final void e()
  {
    com.truecaller.utils.a locala = f;
    long l1 = locala.c();
    Iterator localIterator = c.iterator();
    Object localObject = "prefetchedAds.iterator()";
    c.g.b.k.a(localIterator, (String)localObject);
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localObject = (v)localIterator.next();
      long l2 = ((v)localObject).a(l1);
      long l3 = 0L;
      boolean bool2 = l2 < l3;
      int i1;
      com.truecaller.ads.provider.holders.e locale;
      if (!bool2)
      {
        i1 = 1;
      }
      else
      {
        i1 = 0;
        locale = null;
      }
      if (i1 != 0)
      {
        localIterator.remove();
        i1 = -1;
        locale = ((v)localObject).a(i1);
        com.truecaller.ads.provider.a locala1 = m;
        locala1.b(locale);
        ((v)localObject).a();
      }
    }
    f();
  }
  
  private final void f()
  {
    j.n();
    Object localObject = new com/truecaller/ads/provider/fetch/h$d;
    ((h.d)localObject).<init>(this, null);
    localObject = (m)localObject;
    localObject = kotlinx.coroutines.e.b(this, null, (m)localObject, 3);
    j = ((bn)localObject);
  }
  
  public final c.d.f V_()
  {
    c.d.f localf1 = l;
    c.d.f localf2 = (c.d.f)i;
    return localf1.plus(localf2);
  }
  
  public final com.truecaller.ads.provider.holders.e a(int paramInt)
  {
    Object localObject = (v)c.poll();
    int i1;
    if (localObject != null) {
      i1 = 1;
    } else {
      i1 = 0;
    }
    com.truecaller.ads.provider.holders.e locale;
    if (i1 != 0)
    {
      locale = ((v)localObject).a(paramInt);
    }
    else
    {
      paramInt = 0;
      locale = null;
    }
    b(false);
    if (locale != null)
    {
      localObject = m;
      ((com.truecaller.ads.provider.a)localObject).d(locale);
    }
    return locale;
  }
  
  public final void a(boolean paramBoolean)
  {
    b = paramBoolean;
    if (paramBoolean)
    {
      paramBoolean = false;
      b(false);
    }
  }
  
  public final boolean a()
  {
    e();
    ArrayDeque localArrayDeque = c;
    boolean bool = localArrayDeque.isEmpty();
    return !bool;
  }
  
  public final void b()
  {
    i.n();
    Iterator localIterator = c.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      v localv = (v)localIterator.next();
      int i1 = -1;
      com.truecaller.ads.provider.holders.e locale = localv.a(i1);
      com.truecaller.ads.provider.a locala = m;
      locala.c(locale);
      localv.a();
    }
    c.clear();
  }
  
  public final void d()
  {
    b(true);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */