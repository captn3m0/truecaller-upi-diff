package com.truecaller.ads.provider.fetch;

public enum AdsConfigurationManager$PromotionState
{
  private final String key;
  
  static
  {
    PromotionState[] arrayOfPromotionState = new PromotionState[3];
    PromotionState localPromotionState = new com/truecaller/ads/provider/fetch/AdsConfigurationManager$PromotionState;
    localPromotionState.<init>("UNKNOWN", 0, "");
    UNKNOWN = localPromotionState;
    arrayOfPromotionState[0] = localPromotionState;
    localPromotionState = new com/truecaller/ads/provider/fetch/AdsConfigurationManager$PromotionState;
    int i = 1;
    localPromotionState.<init>("OPT_IN", i, "I");
    OPT_IN = localPromotionState;
    arrayOfPromotionState[i] = localPromotionState;
    localPromotionState = new com/truecaller/ads/provider/fetch/AdsConfigurationManager$PromotionState;
    i = 2;
    localPromotionState.<init>("OPT_OUT", i, "O");
    OPT_OUT = localPromotionState;
    arrayOfPromotionState[i] = localPromotionState;
    $VALUES = arrayOfPromotionState;
  }
  
  private AdsConfigurationManager$PromotionState(String paramString1)
  {
    key = paramString1;
  }
  
  public final String getKey()
  {
    return key;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.AdsConfigurationManager.PromotionState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */