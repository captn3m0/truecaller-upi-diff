package com.truecaller.ads.provider.fetch;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import c.a.ag;
import c.t;
import c.u;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdLoader.Builder;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.VideoOptions.Builder;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest.Builder;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.NativeAdOptions.Builder;
import com.google.android.gms.ads.formats.NativeAppInstallAd.OnAppInstallAdLoadedListener;
import com.google.android.gms.ads.formats.NativeContentAd.OnContentAdLoadedListener;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd.OnCustomClickListener;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener;
import com.google.android.gms.ads.formats.OnPublisherAdViewLoadedListener;
import com.mopub.mobileads.dfp.adapters.MoPubAdapter;
import com.mopub.mobileads.dfp.adapters.MoPubAdapter.BundleBuilder;
import com.truecaller.ads.CustomTemplate;
import com.truecaller.util.co;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class m
  implements l, p
{
  private final Map a;
  private final Context b;
  private final com.truecaller.utils.d c;
  private final com.truecaller.utils.a d;
  private final com.truecaller.common.h.c e;
  
  public m(Context paramContext, com.truecaller.utils.d paramd, com.truecaller.utils.a parama, com.truecaller.common.h.c paramc)
  {
    b = paramContext;
    c = paramd;
    d = parama;
    e = paramc;
    paramContext = new java/util/LinkedHashMap;
    paramContext.<init>();
    paramContext = (Map)paramContext;
    a = paramContext;
  }
  
  private final Map a(Context paramContext, String[] paramArrayOfString)
  {
    int i = 3;
    Object localObject1 = new c.n[i];
    String str1 = e.f();
    Object localObject2 = t.a("buildname", str1);
    str1 = null;
    localObject1[0] = localObject2;
    String str2 = "10.41.6";
    localObject2 = t.a("appversion", str2);
    int j = 1;
    localObject1[j] = localObject2;
    localObject2 = "sms";
    Object localObject3 = c;
    boolean bool1 = ((com.truecaller.utils.d)localObject3).a();
    if (bool1) {
      localObject3 = "t";
    } else {
      localObject3 = "f";
    }
    localObject2 = t.a(localObject2, localObject3);
    int k = 2;
    localObject1[k] = localObject2;
    localObject1 = ag.b((c.n[])localObject1);
    localObject2 = e;
    boolean bool2 = ((com.truecaller.common.h.c)localObject2).a();
    if (bool2)
    {
      localObject2 = "OEM_build";
      ((Map)localObject1).put(localObject2, null);
    }
    try
    {
      paramContext = com.truecaller.common.h.k.d(paramContext);
      localObject2 = paramContext;
      localObject2 = (CharSequence)paramContext;
      bool2 = TextUtils.isEmpty((CharSequence)localObject2);
      if (!bool2)
      {
        localObject2 = "carrier";
        ((Map)localObject1).put(localObject2, paramContext);
      }
    }
    catch (SecurityException localSecurityException) {}
    paramContext = com.truecaller.common.h.k.a();
    localObject2 = paramContext;
    localObject2 = (CharSequence)paramContext;
    bool2 = TextUtils.isEmpty((CharSequence)localObject2);
    if (!bool2)
    {
      localObject2 = "device";
      ((Map)localObject1).put(localObject2, paramContext);
    }
    if (paramArrayOfString == null) {
      paramArrayOfString = new String[0];
    }
    paramContext = new java/util/ArrayList;
    paramContext.<init>();
    paramContext = (Collection)paramContext;
    int m = paramArrayOfString.length;
    int n = 0;
    c.n.k localk = null;
    while (n < m)
    {
      String str3 = paramArrayOfString[n];
      Object localObject4 = str3;
      localObject4 = (CharSequence)str3;
      int i1 = ((CharSequence)localObject4).length();
      if (i1 == 0)
      {
        i1 = 1;
      }
      else
      {
        i1 = 0;
        localObject4 = null;
      }
      if (i1 == 0) {
        paramContext.add(str3);
      }
      n += 1;
    }
    paramContext = ((List)paramContext).iterator();
    for (;;)
    {
      boolean bool3 = paramContext.hasNext();
      if (!bool3) {
        break label505;
      }
      paramArrayOfString = (CharSequence)paramContext.next();
      localObject2 = ":";
      localk = new c/n/k;
      localk.<init>((String)localObject2);
      paramArrayOfString = (Collection)localk.a(paramArrayOfString, k);
      if (paramArrayOfString == null) {
        break label493;
      }
      localObject2 = new String[0];
      paramArrayOfString = paramArrayOfString.toArray((Object[])localObject2);
      if (paramArrayOfString == null) {
        break;
      }
      paramArrayOfString = (String[])paramArrayOfString;
      localObject2 = paramArrayOfString[0];
      n = paramArrayOfString.length;
      if (n > j)
      {
        paramArrayOfString = paramArrayOfString[j];
      }
      else
      {
        bool3 = false;
        paramArrayOfString = null;
      }
      ((Map)localObject1).put(localObject2, paramArrayOfString);
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw paramContext;
    label493:
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type java.util.Collection<T>");
    throw paramContext;
    label505:
    return (Map)localObject1;
  }
  
  public final PublisherAdRequest a(Context paramContext, String[] paramArrayOfString, boolean paramBoolean)
  {
    c.g.b.k.b(paramContext, "context");
    PublisherAdRequest.Builder localBuilder = new com/google/android/gms/ads/doubleclick/PublisherAdRequest$Builder;
    localBuilder.<init>();
    Object localObject1 = co.b(paramContext);
    if (localObject1 != null) {
      localBuilder.a((Location)localObject1);
    }
    Object localObject2 = new com/mopub/mobileads/dfp/adapters/MoPubAdapter$BundleBuilder;
    ((MoPubAdapter.BundleBuilder)localObject2).<init>();
    int i = 14;
    localObject2 = ((MoPubAdapter.BundleBuilder)localObject2).setPrivacyIconSize(i).build();
    localBuilder.a(MoPubAdapter.class, (Bundle)localObject2);
    localObject1 = AdMobAdapter.class;
    localObject2 = new android/os/Bundle;
    ((Bundle)localObject2).<init>();
    String str1 = "npa";
    String str2;
    if (paramBoolean) {
      str2 = "0";
    } else {
      str2 = "1";
    }
    ((Bundle)localObject2).putString(str1, str2);
    localBuilder.a((Class)localObject1, (Bundle)localObject2);
    paramContext = a(paramContext, paramArrayOfString).entrySet().iterator();
    for (;;)
    {
      boolean bool1 = paramContext.hasNext();
      if (!bool1) {
        break;
      }
      paramArrayOfString = (Map.Entry)paramContext.next();
      str2 = (String)paramArrayOfString.getKey();
      paramArrayOfString = (String)paramArrayOfString.getValue();
      localObject1 = paramArrayOfString;
      localObject1 = (CharSequence)paramArrayOfString;
      boolean bool2 = TextUtils.isEmpty((CharSequence)localObject1);
      if (bool2) {
        paramArrayOfString = str2;
      }
      localBuilder.a(str2, paramArrayOfString);
    }
    paramContext = localBuilder.a();
    c.g.b.k.a(paramContext, "builder.build()");
    return paramContext;
  }
  
  public final Object a(c paramc, c.d.c paramc1)
  {
    m localm = this;
    c localc = paramc;
    kotlinx.coroutines.k localk = new kotlinx/coroutines/k;
    Object localObject1 = c.d.a.b.a(paramc1);
    int i = 1;
    localk.<init>((c.d.c)localObject1, i);
    Object localObject2 = localk;
    localObject2 = (kotlinx.coroutines.j)localk;
    com.truecaller.ads.k localk1 = a;
    Object localObject5;
    try
    {
      AdLoader.Builder localBuilder = new com/google/android/gms/ads/AdLoader$Builder;
      localObject1 = b;
      localObject3 = b;
      localBuilder.<init>((Context)localObject1, (String)localObject3);
      b localb = new com/truecaller/ads/provider/fetch/b;
      localb.<init>();
      localObject1 = new com/truecaller/ads/provider/fetch/m$a;
      ((m.a)localObject1).<init>((kotlinx.coroutines.j)localObject2);
      localObject1 = (c.g.a.b)localObject1;
      a = ((c.g.a.b)localObject1);
      localObject1 = localb;
      localObject1 = (AdListener)localb;
      localBuilder.a((AdListener)localObject1);
      localObject1 = new com/truecaller/ads/provider/fetch/m$b;
      ((m.b)localObject1).<init>((kotlinx.coroutines.j)localObject2, localb, this, paramc);
      localObject1 = (NativeAppInstallAd.OnAppInstallAdLoadedListener)localObject1;
      localObject1 = localBuilder.a((NativeAppInstallAd.OnAppInstallAdLoadedListener)localObject1);
      localObject3 = new com/truecaller/ads/provider/fetch/m$c;
      ((m.c)localObject3).<init>((kotlinx.coroutines.j)localObject2, localb, this, paramc);
      localObject3 = (NativeContentAd.OnContentAdLoadedListener)localObject3;
      localObject1 = ((AdLoader.Builder)localObject1).a((NativeContentAd.OnContentAdLoadedListener)localObject3);
      localObject3 = new com/google/android/gms/ads/formats/NativeAdOptions$Builder;
      ((NativeAdOptions.Builder)localObject3).<init>();
      ((NativeAdOptions.Builder)localObject3).b();
      boolean bool1 = m ^ i;
      ((NativeAdOptions.Builder)localObject3).a(bool1);
      int j = g;
      boolean bool2 = com.truecaller.common.e.f.b();
      Object localObject4 = null;
      if (bool2) {
        switch (j)
        {
        default: 
          break;
        case 3: 
          j = 2;
          break;
        case 2: 
          j = 3;
          break;
        case 1: 
          j = 0;
          localObject5 = null;
          break;
        case 0: 
          j = 1;
        }
      }
      ((NativeAdOptions.Builder)localObject3).a(j);
      ((NativeAdOptions.Builder)localObject3).a();
      localObject5 = new com/google/android/gms/ads/VideoOptions$Builder;
      ((VideoOptions.Builder)localObject5).<init>();
      bool2 = i ^ i;
      localObject5 = ((VideoOptions.Builder)localObject5).a(bool2);
      bool2 = j;
      localObject5 = ((VideoOptions.Builder)localObject5).b(bool2).a();
      ((NativeAdOptions.Builder)localObject3).a((VideoOptions)localObject5);
      localObject3 = ((NativeAdOptions.Builder)localObject3).c();
      ((AdLoader.Builder)localObject1).a((NativeAdOptions)localObject3);
      localObject1 = d;
      int k = ((List)localObject1).isEmpty();
      if (k == 0)
      {
        localObject1 = new com/truecaller/ads/provider/fetch/m$d;
        ((m.d)localObject1).<init>((kotlinx.coroutines.j)localObject2, localb, localm, localc);
        localObject1 = (OnPublisherAdViewLoadedListener)localObject1;
        localObject3 = (Collection)d;
        if (localObject3 != null)
        {
          localObject5 = new AdSize[0];
          localObject3 = ((Collection)localObject3).toArray((Object[])localObject5);
          if (localObject3 != null)
          {
            localObject3 = (AdSize[])localObject3;
            j = localObject3.length;
            localObject3 = (AdSize[])Arrays.copyOf((Object[])localObject3, j);
            localBuilder.a((OnPublisherAdViewLoadedListener)localObject1, (AdSize[])localObject3);
          }
          else
          {
            localObject1 = new c/u;
            ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.Array<T>");
            throw ((Throwable)localObject1);
          }
        }
        else
        {
          localObject1 = new c/u;
          ((u)localObject1).<init>("null cannot be cast to non-null type java.util.Collection<T>");
          throw ((Throwable)localObject1);
        }
      }
      localObject1 = (Iterable)e;
      Iterator localIterator = ((Iterable)localObject1).iterator();
      for (;;)
      {
        k = localIterator.hasNext();
        if (k == 0) {
          break label761;
        }
        localObject1 = localIterator.next();
        Object localObject6 = localObject1;
        localObject6 = (CustomTemplate)localObject1;
        k = openUrl;
        int m;
        if (k == i)
        {
          m = 0;
          localObject1 = null;
          localObject7 = null;
        }
        else
        {
          if (m != 0) {
            break;
          }
          localObject8 = new com/truecaller/ads/provider/fetch/m$e;
          localObject1 = localObject8;
          localObject3 = localb;
          localObject5 = localBuilder;
          localObject9 = localObject2;
          localObject4 = this;
          localObject7 = paramc;
          ((m.e)localObject8).<init>(localb, localBuilder, (kotlinx.coroutines.j)localObject2, this, paramc);
          localObject1 = (NativeCustomTemplateAd.OnCustomClickListener)localObject8;
          localObject7 = localObject1;
        }
        localObject6 = templateId;
        Object localObject8 = new com/truecaller/ads/provider/fetch/m$f;
        localObject1 = localObject8;
        localObject3 = localb;
        localObject5 = localBuilder;
        localObject9 = localObject2;
        localObject4 = this;
        Object localObject10 = localObject7;
        localObject7 = paramc;
        ((m.f)localObject8).<init>(localb, localBuilder, (kotlinx.coroutines.j)localObject2, this, paramc);
        localObject1 = (NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener)localObject8;
        localBuilder.a((String)localObject6, (NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener)localObject1, (NativeCustomTemplateAd.OnCustomClickListener)localObject10);
        i = 1;
      }
      localObject1 = new c/l;
      ((c.l)localObject1).<init>();
      throw ((Throwable)localObject1);
      label761:
      localObject1 = b;
      localObject3 = c;
      localObject5 = a;
      Object localObject9 = a;
      localObject4 = new com/truecaller/ads/provider/fetch/j;
      long l = d.a();
      Object localObject7 = a;
      localObject1 = localm.a((Context)localObject1, (String[])localObject3);
      ((j)localObject4).<init>(l, (String)localObject7, (Map)localObject1);
      ((Map)localObject5).put(localObject9, localObject4);
      localObject1 = localBuilder.a();
      localObject3 = b;
      localObject5 = c;
      bool2 = e;
      localObject3 = localm.a((Context)localObject3, (String[])localObject5, bool2);
      ((AdLoader)localObject1).a((PublisherAdRequest)localObject3);
    }
    catch (Exception localException)
    {
      localObject1 = new com/truecaller/ads/provider/fetch/d;
      int n = com.truecaller.ads.f.b();
      ((d)localObject1).<init>(n);
      n.a((kotlinx.coroutines.j)localObject2, (d)localObject1);
    }
    localObject1 = localk.h();
    Object localObject3 = c.d.a.a.a;
    if (localObject1 == localObject3)
    {
      localObject3 = "frame";
      localObject5 = paramc1;
      c.g.b.k.b(paramc1, (String)localObject3);
    }
    return localObject1;
  }
  
  public final Set a()
  {
    return c.a.m.i((Iterable)a.values());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */