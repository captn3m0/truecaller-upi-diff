package com.truecaller.ads.provider.fetch;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.ads.f;
import java.util.ArrayDeque;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.e.b;

final class h$e
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag e;
  
  h$e(h paramh, boolean paramBoolean, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    e locale = new com/truecaller/ads/provider/fetch/h$e;
    h localh = c;
    boolean bool = d;
    locale.<init>(localh, bool, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return locale;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = b;
    int j = 1;
    Object localObject2;
    boolean bool2;
    Object localObject3;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      localObject2 = (b)a;
      bool2 = paramObject instanceof o.b;
      if (!bool2)
      {
        localObject3 = localObject1;
        localObject1 = this;
        break label583;
      }
      try
      {
        paramObject = (o.b)paramObject;
        paramObject = a;
        throw ((Throwable)paramObject);
      }
      finally
      {
        break label620;
      }
    case 1: 
      localObject2 = (b)a;
      bool2 = paramObject instanceof o.b;
      if (!bool2)
      {
        paramObject = this;
        break label215;
      }
      throw a;
    }
    boolean bool1 = paramObject instanceof o.b;
    if (!bool1)
    {
      paramObject = c.a;
      boolean bool4 = ((b)paramObject).a();
      if (bool4)
      {
        bool4 = d;
        if (!bool4) {
          return x.a;
        }
      }
      paramObject = c.a;
      a = paramObject;
      b = j;
      localObject2 = ((b)paramObject).a(this);
      if (localObject2 == localObject1) {
        return localObject1;
      }
      localObject2 = paramObject;
      paramObject = this;
      for (;;)
      {
        label215:
        localObject3 = c;
        localObject3 = h;
        bool2 = ((AdsConfigurationManager)localObject3).d();
        if (!bool2)
        {
          paramObject = x.a;
          ((b)localObject2).b();
          return paramObject;
        }
        localObject3 = c;
        boolean bool5 = d;
        if (bool5)
        {
          localObject4 = c;
          m = ((ArrayDeque)localObject4).size();
          if (m <= 0)
          {
            localObject4 = d;
            m = c;
            if (m <= 0)
            {
              bool2 = true;
              break label363;
            }
          }
        }
        Object localObject4 = c;
        int m = ((ArrayDeque)localObject4).size();
        localObject3 = d;
        int k = c;
        if (m < k)
        {
          k = 1;
        }
        else
        {
          k = 0;
          localObject3 = null;
        }
        label363:
        if (k == 0)
        {
          paramObject = x.a;
          ((b)localObject2).b();
          return paramObject;
        }
        localObject3 = c;
        boolean bool3 = b;
        if (!bool3)
        {
          bool3 = d;
          if (!bool3)
          {
            paramObject = x.a;
            ((b)localObject2).b();
            return paramObject;
          }
        }
        localObject3 = c;
        localObject3 = g;
        localObject4 = "adsQaDisableRequests";
        bool3 = ((com.truecaller.i.a)localObject3).b((String)localObject4);
        if (bool3)
        {
          localObject1 = c;
          localObject1 = e;
          Object localObject5 = c;
          localObject5 = d;
          ((k)localObject1).e((com.truecaller.ads.k)localObject5);
          localObject1 = c;
          localObject1 = e;
          paramObject = c;
          paramObject = d;
          j = f.a();
          ((k)localObject1).b((com.truecaller.ads.k)paramObject, j);
          paramObject = x.a;
          ((b)localObject2).b();
          return paramObject;
        }
        localObject3 = c;
        a = localObject2;
        m = 2;
        b = m;
        localObject3 = ((h)localObject3).a((c)paramObject);
        if (localObject3 == localObject1) {
          return localObject1;
        }
        Object localObject6 = localObject1;
        localObject1 = paramObject;
        paramObject = localObject3;
        localObject3 = localObject6;
        label583:
        paramObject = (Boolean)paramObject;
        bool4 = ((Boolean)paramObject).booleanValue();
        if (!bool4)
        {
          paramObject = x.a;
          ((b)localObject2).b();
          return paramObject;
        }
        paramObject = localObject1;
        localObject1 = localObject3;
      }
      label620:
      ((b)localObject2).b();
      throw ((Throwable)paramObject);
    }
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (e)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((e)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.fetch.h.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */