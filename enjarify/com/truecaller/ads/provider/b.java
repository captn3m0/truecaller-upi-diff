package com.truecaller.ads.provider;

import c.a.f;
import com.google.android.gms.ads.AdSize;
import com.truecaller.ads.j;
import com.truecaller.analytics.e.a;
import java.util.LinkedHashMap;
import java.util.Map;

public final class b
  implements a
{
  private final Map a;
  private final Map b;
  private final com.truecaller.analytics.b c;
  private final com.truecaller.utils.a d;
  
  public b(com.truecaller.analytics.b paramb, com.truecaller.utils.a parama)
  {
    c = paramb;
    d = parama;
    paramb = new java/util/LinkedHashMap;
    paramb.<init>();
    paramb = (Map)paramb;
    a = paramb;
    paramb = new java/util/LinkedHashMap;
    paramb.<init>();
    paramb = (Map)paramb;
    b = paramb;
  }
  
  private static String a(long paramLong)
  {
    long[] arrayOfLong = c.a();
    int i = arrayOfLong.length;
    int j = 0;
    while (j < i)
    {
      long l = arrayOfLong[j];
      boolean bool = paramLong < l;
      if (bool) {
        bool = true;
      } else {
        bool = false;
      }
      if (bool)
      {
        localObject = Long.valueOf(l);
        break label69;
      }
      j += 1;
    }
    Object localObject = null;
    label69:
    if (localObject != null)
    {
      paramLong = ((Long)localObject).longValue();
    }
    else
    {
      localObject = c.a();
      paramLong = f.a((long[])localObject);
    }
    return String.valueOf(paramLong);
  }
  
  private final void a(String paramString, com.truecaller.ads.provider.fetch.c paramc, Integer paramInteger)
  {
    String str1 = g;
    String str2 = b;
    String str3 = a.f.c;
    c.g.b.k.a(str3, "config.campaignConfig.placement");
    String str4 = a.h;
    a(this, paramString, str1, str2, str3, str4, null, null, paramInteger, 96);
  }
  
  private final void a(String paramString, com.truecaller.ads.provider.holders.e parame)
  {
    boolean bool = parame.f();
    if (!bool) {
      return;
    }
    com.truecaller.ads.provider.fetch.c localc = parame.h();
    String str1 = g;
    String str2 = b;
    String str3 = a.f.c;
    c.g.b.k.a(str3, "config.campaignConfig.placement");
    String str4 = a.h;
    String str5 = parame.b();
    String str6 = parame.c();
    a(this, paramString, str1, str2, str3, str4, str5, str6, null, 128);
  }
  
  private static void a(String paramString1, Long paramLong, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, Integer paramInteger)
  {
    paramString1 = (CharSequence)paramString1;
    Object localObject = { "-" };
    char c1 = '\006';
    paramString1 = (String)c.a.m.f(c.n.m.c(paramString1, (String[])localObject, false, c1));
    long l;
    if (paramLong != null) {
      l = paramLong.longValue();
    } else {
      l = 0L;
    }
    paramLong = String.valueOf(l);
    int i = 7;
    c1 = ' ';
    paramLong = c.n.m.a(paramLong, i, c1);
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    paramString2 = c.n.m.g(paramString2);
    ((StringBuilder)localObject).append(paramString2);
    if (paramInteger != null)
    {
      int j = ((Number)paramInteger).intValue();
      paramInteger = ":";
      paramString2 = String.valueOf(j);
      paramString2 = paramInteger.concat(paramString2);
      if (paramString2 != null) {}
    }
    else
    {
      paramString2 = "  ";
    }
    ((StringBuilder)localObject).append(paramString2);
    paramString2 = ((StringBuilder)localObject).toString();
    paramInteger = new String[1];
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("[");
    ((StringBuilder)localObject).append(paramString1);
    ((StringBuilder)localObject).append("] T:");
    ((StringBuilder)localObject).append(paramLong);
    ((StringBuilder)localObject).append(c1);
    ((StringBuilder)localObject).append(paramString2);
    ((StringBuilder)localObject).append(c1);
    ((StringBuilder)localObject).append(paramString3);
    ((StringBuilder)localObject).append(c1);
    ((StringBuilder)localObject).append(paramString4);
    ((StringBuilder)localObject).append(c1);
    ((StringBuilder)localObject).append(paramString5);
    ((StringBuilder)localObject).append(c1);
    ((StringBuilder)localObject).append(paramString6);
    ((StringBuilder)localObject).append(c1);
    ((StringBuilder)localObject).append(paramString7);
    ((StringBuilder)localObject).append(c1);
    paramString1 = ((StringBuilder)localObject).toString();
    paramInteger[0] = paramString1;
  }
  
  private final void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, Integer paramInteger)
  {
    b localb = this;
    String str1 = paramString6;
    String str2 = paramString7;
    Object localObject1 = paramString2;
    Object localObject2 = c(paramString2);
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("Ads");
    locala.a("Event", paramString1);
    locala.a("UnitId", paramString3);
    Object localObject3 = "Placement";
    locala.a((String)localObject3, paramString4);
    if (paramString5 != null)
    {
      localObject3 = "Context";
      locala.a((String)localObject3, paramString5);
    }
    if (str1 != null)
    {
      localObject3 = "AdType";
      locala.a((String)localObject3, str1);
    }
    if (str2 != null)
    {
      localObject3 = "AdSubtype";
      locala.a((String)localObject3, str2);
    }
    if (paramInteger != null)
    {
      localObject3 = paramInteger;
      localObject3 = (Number)paramInteger;
      int i = ((Number)localObject3).intValue();
      String str3 = "Fail";
      locala.a(str3, i);
    }
    if (localObject2 != null)
    {
      localObject3 = localObject2;
      long l = ((Number)localObject2).longValue();
      String str4 = a(l);
      locala.a("TimeBucket", str4);
      double d1 = l;
      double d2 = 1000.0D;
      Double.isNaN(d1);
      d1 /= d2;
      localObject3 = Double.valueOf(d1);
      locala.a((Double)localObject3);
    }
    localObject1 = paramString2;
    localObject3 = paramString1;
    str1 = paramString6;
    str2 = paramString7;
    a(paramString2, (Long)localObject2, paramString1, paramString3, paramString4, paramString5, paramString6, paramString7, paramInteger);
    localObject1 = c;
    localObject2 = locala.a();
    c.g.b.k.a(localObject2, "builder.build()");
    ((com.truecaller.analytics.b)localObject1).a((com.truecaller.analytics.e)localObject2);
  }
  
  private final Long c(String paramString)
  {
    com.truecaller.utils.a locala = d;
    long l1 = locala.c();
    Long localLong1 = (Long)b.get(paramString);
    Map localMap = b;
    Long localLong2 = Long.valueOf(l1);
    localMap.put(paramString, localLong2);
    if (localLong1 != null)
    {
      long l2 = ((Number)localLong1).longValue();
      return Long.valueOf((l1 - l2) / 1000000L);
    }
    return null;
  }
  
  public final void a(com.truecaller.ads.provider.fetch.c paramc)
  {
    c.g.b.k.b(paramc, "adRequest");
    a("requested", paramc, null);
  }
  
  public final void a(com.truecaller.ads.provider.fetch.c paramc, int paramInt)
  {
    c.g.b.k.b(paramc, "adRequest");
    Object localObject = Integer.valueOf(paramInt);
    a("failed", paramc, (Integer)localObject);
    localObject = b;
    paramc = g;
    ((Map)localObject).remove(paramc);
  }
  
  public final void a(com.truecaller.ads.provider.holders.e parame)
  {
    c.g.b.k.b(parame, "adHolder");
    a("loaded", parame);
  }
  
  public final void a(com.truecaller.ads.provider.holders.e parame, String paramString)
  {
    c.g.b.k.b(parame, "adHolder");
    c.g.b.k.b(paramString, "event");
    a(paramString, parame);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "placement");
    Object localObject1 = a;
    Object localObject2 = Long.valueOf(d.c());
    ((Map)localObject1).put(paramString, localObject2);
    localObject1 = c;
    localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("AdsKeywords");
    paramString = ((e.a)localObject2).a("Event", "requested").a("Placement", paramString).a();
    c.g.b.k.a(paramString, "AnalyticsEvent.Builder(A…\n                .build()");
    ((com.truecaller.analytics.b)localObject1).a(paramString);
  }
  
  public final void a(String paramString, AdSize paramAdSize)
  {
    c.g.b.k.b(paramString, "adUnit");
    c.g.b.k.b(paramAdSize, "size");
    b.remove("legacyBanner");
    String str = paramAdSize.toString();
    a(this, "requested", "legacyBanner", paramString, "AFTERCALL", "afterCall", "aftercallBanner", str, null, 128);
  }
  
  public final void a(String paramString, AdSize paramAdSize, int paramInt)
  {
    c.g.b.k.b(paramString, "adUnit");
    c.g.b.k.b(paramAdSize, "size");
    String str = paramAdSize.toString();
    Integer localInteger = Integer.valueOf(paramInt);
    a("failed", "legacyBanner", paramString, "AFTERCALL", "afterCall", "aftercallBanner", str, localInteger);
  }
  
  public final void a(String... paramVarArgs)
  {
    String str1 = "placements";
    c.g.b.k.b(paramVarArgs, str1);
    int i = paramVarArgs.length;
    int j = 0;
    while (j < i)
    {
      Object localObject1 = paramVarArgs[j];
      com.truecaller.analytics.b localb = c;
      Object localObject2 = new com/truecaller/analytics/e$a;
      ((e.a)localObject2).<init>("AdsKeywords");
      String str2 = "preloaded";
      localObject2 = ((e.a)localObject2).a("Event", str2);
      String str3 = "Placement";
      localObject1 = ((e.a)localObject2).a(str3, (String)localObject1).a();
      localObject2 = "AnalyticsEvent.Builder(A…                 .build()";
      c.g.b.k.a(localObject1, (String)localObject2);
      localb.a((com.truecaller.analytics.e)localObject1);
      j += 1;
    }
  }
  
  public final void b(com.truecaller.ads.provider.fetch.c paramc)
  {
    c.g.b.k.b(paramc, "adRequest");
    a("canceled", paramc, null);
    Map localMap = b;
    paramc = g;
    localMap.remove(paramc);
  }
  
  public final void b(com.truecaller.ads.provider.holders.e parame)
  {
    c.g.b.k.b(parame, "adHolder");
    a("expired", parame);
    Map localMap = b;
    parame = hg;
    localMap.remove(parame);
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "placement");
    Object localObject1 = d;
    long l1 = ((com.truecaller.utils.a)localObject1).c();
    Object localObject2 = (Long)a.remove(paramString);
    long l2;
    if (localObject2 != null) {
      l2 = ((Long)localObject2).longValue();
    } else {
      l2 = l1;
    }
    l1 = (l1 - l2) / 1000000L;
    localObject2 = c;
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("AdsKeywords");
    paramString = locala.a("Event", "received").a("Placement", paramString);
    String str = a(l1);
    paramString = paramString.a("TimeBucket", str);
    double d1 = l1;
    Double.isNaN(d1);
    localObject1 = Double.valueOf(d1 / 1000.0D);
    paramString = paramString.a((Double)localObject1).a();
    c.g.b.k.a(paramString, "AnalyticsEvent.Builder(A…\n                .build()");
    ((com.truecaller.analytics.b)localObject2).a(paramString);
  }
  
  public final void b(String paramString, AdSize paramAdSize)
  {
    c.g.b.k.b(paramString, "adUnit");
    c.g.b.k.b(paramAdSize, "size");
    String str = paramAdSize.toString();
    int i = 128;
    a(this, "loaded", "legacyBanner", paramString, "AFTERCALL", "afterCall", "aftercallBanner", str, null, i);
    str = paramAdSize.toString();
    a(this, "displayed", "legacyBanner", paramString, "AFTERCALL", "afterCall", "aftercallBanner", str, null, i);
  }
  
  public final void c(com.truecaller.ads.provider.holders.e parame)
  {
    c.g.b.k.b(parame, "adHolder");
    a("dropped", parame);
    Map localMap = b;
    parame = hg;
    localMap.remove(parame);
  }
  
  public final void c(String paramString, AdSize paramAdSize)
  {
    c.g.b.k.b(paramString, "adUnit");
    c.g.b.k.b(paramAdSize, "size");
    String str = paramAdSize.toString();
    a(this, "opened", "legacyBanner", paramString, "AFTERCALL", "afterCall", "aftercallBanner", str, null, 128);
  }
  
  public final void d(com.truecaller.ads.provider.holders.e parame)
  {
    c.g.b.k.b(parame, "adHolder");
    a("displayed", parame);
  }
  
  public final void e(com.truecaller.ads.provider.holders.e parame)
  {
    c.g.b.k.b(parame, "adHolder");
    a("opened", parame);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */