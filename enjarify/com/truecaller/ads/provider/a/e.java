package com.truecaller.ads.provider.a;

import dagger.a.d;
import javax.inject.Provider;

public final class e
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private e(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
  }
  
  public static e a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    e locale = new com/truecaller/ads/provider/a/e;
    locale.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */