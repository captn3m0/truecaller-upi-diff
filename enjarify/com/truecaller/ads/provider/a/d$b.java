package com.truecaller.ads.provider.a;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.ads.campaigns.AdCampaigns;
import com.truecaller.log.AssertionUtil;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import kotlinx.coroutines.ag;

final class d$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  d$b(Future paramFuture, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/ads/provider/a/d$b;
    Future localFuture = b;
    localb.<init>(localFuture, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = null;
        try
        {
          localObject = b;
          localObject = ((Future)localObject).get();
          localObject = (AdCampaigns[])localObject;
          paramObject = localObject;
        }
        catch (ExecutionException localExecutionException)
        {
          localObject = (Throwable)localExecutionException;
          AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject);
        }
        catch (InterruptedException localInterruptedException)
        {
          localObject = (Throwable)localInterruptedException;
          AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject);
        }
        return paramObject;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.a.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */