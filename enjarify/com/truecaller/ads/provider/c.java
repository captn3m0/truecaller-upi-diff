package com.truecaller.ads.provider;

public final class c
{
  private static final long[] a;
  
  static
  {
    long[] arrayOfLong = new long[14];
    arrayOfLong[0] = 100;
    arrayOfLong[1] = 500L;
    arrayOfLong[2] = 1000L;
    arrayOfLong[3] = 2000L;
    arrayOfLong[4] = 5000L;
    arrayOfLong[5] = 10000L;
    arrayOfLong[6] = 60000L;
    arrayOfLong[7] = 120000L;
    arrayOfLong[8] = 300000L;
    arrayOfLong[9] = 600000L;
    arrayOfLong[10] = 1200000L;
    arrayOfLong[11] = 3000000L;
    arrayOfLong[12] = 6000000L;
    arrayOfLong[13] = Long.MAX_VALUE;
    a = arrayOfLong;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */