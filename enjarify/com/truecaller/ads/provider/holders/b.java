package com.truecaller.ads.provider.holders;

import com.truecaller.ads.provider.fetch.c;

public abstract class b
  implements e
{
  public final c a;
  private final long b;
  private final boolean c;
  private final Object d;
  
  public b(Object paramObject, c paramc)
  {
    d = paramObject;
    a = paramc;
    long l = f.a();
    b = l;
    c = true;
  }
  
  public long e()
  {
    return b;
  }
  
  public boolean f()
  {
    return c;
  }
  
  public Object g()
  {
    return d;
  }
  
  public final c h()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.holders.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */