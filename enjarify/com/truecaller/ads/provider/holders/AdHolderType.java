package com.truecaller.ads.provider.holders;

public enum AdHolderType
{
  static
  {
    AdHolderType[] arrayOfAdHolderType = new AdHolderType[4];
    AdHolderType localAdHolderType = new com/truecaller/ads/provider/holders/AdHolderType;
    localAdHolderType.<init>("PUBLISHER_VIEW", 0);
    PUBLISHER_VIEW = localAdHolderType;
    arrayOfAdHolderType[0] = localAdHolderType;
    localAdHolderType = new com/truecaller/ads/provider/holders/AdHolderType;
    int i = 1;
    localAdHolderType.<init>("NATIVE_AD", i);
    NATIVE_AD = localAdHolderType;
    arrayOfAdHolderType[i] = localAdHolderType;
    localAdHolderType = new com/truecaller/ads/provider/holders/AdHolderType;
    i = 2;
    localAdHolderType.<init>("CUSTOM_AD", i);
    CUSTOM_AD = localAdHolderType;
    arrayOfAdHolderType[i] = localAdHolderType;
    localAdHolderType = new com/truecaller/ads/provider/holders/AdHolderType;
    i = 3;
    localAdHolderType.<init>("HOUSE_AD", i);
    HOUSE_AD = localAdHolderType;
    arrayOfAdHolderType[i] = localAdHolderType;
    $VALUES = arrayOfAdHolderType;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.holders.AdHolderType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */