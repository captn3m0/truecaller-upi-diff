package com.truecaller.ads.provider.holders;

import com.truecaller.ads.provider.b.e;
import com.truecaller.ads.provider.fetch.c;

public final class g
  extends b
{
  private final AdHolderType b;
  private final String c;
  private final String d;
  private final long e;
  private final boolean f;
  
  public g(e parame, c paramc)
  {
    super(parame, paramc);
    parame = AdHolderType.HOUSE_AD;
    b = parame;
    c = "house";
    d = "normal";
    long l = h.a();
    e = l;
  }
  
  public final AdHolderType a()
  {
    return b;
  }
  
  public final String b()
  {
    return c;
  }
  
  public final String c()
  {
    return d;
  }
  
  public final void d() {}
  
  public final long e()
  {
    return e;
  }
  
  public final boolean f()
  {
    return f;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.holders.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */