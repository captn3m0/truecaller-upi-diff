package com.truecaller.ads.provider.holders;

import com.google.android.gms.ads.formats.NativeContentAd;

public final class c
  extends AdNativeHolder
{
  private final String b = "content";
  private final AdNativeHolder.Type c;
  
  public c(NativeContentAd paramNativeContentAd, com.truecaller.ads.provider.fetch.c paramc)
  {
    super(paramc, paramNativeContentAd);
    paramNativeContentAd = AdNativeHolder.Type.CONTENT;
    c = paramNativeContentAd;
  }
  
  public final String c()
  {
    return b;
  }
  
  public final AdNativeHolder.Type i()
  {
    return c;
  }
  
  protected final void j()
  {
    ((NativeContentAd)k()).destroy();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.holders.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */