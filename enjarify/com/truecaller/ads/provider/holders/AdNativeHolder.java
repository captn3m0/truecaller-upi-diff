package com.truecaller.ads.provider.holders;

import com.google.android.gms.ads.formats.NativeAd;
import com.truecaller.ads.provider.fetch.c;

public abstract class AdNativeHolder
  extends b
{
  private final boolean b;
  private boolean c;
  private final AdHolderType d;
  private final String e;
  private final NativeAd f;
  
  public AdNativeHolder(c paramc, NativeAd paramNativeAd)
  {
    super(paramNativeAd, paramc);
    boolean bool = f;
    b = bool;
    paramc = AdHolderType.NATIVE_AD;
    d = paramc;
    e = "native";
    f = paramNativeAd;
  }
  
  public final AdHolderType a()
  {
    return d;
  }
  
  public final String b()
  {
    return e;
  }
  
  public final void d()
  {
    boolean bool = c;
    if (!bool)
    {
      bool = b;
      if (bool) {
        j();
      }
    }
    c = true;
  }
  
  public abstract AdNativeHolder.Type i();
  
  protected abstract void j();
  
  public final NativeAd k()
  {
    boolean bool = c;
    if (!bool) {
      return f;
    }
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("Can't unwrap destroyed ad");
    throw ((Throwable)localIllegalStateException);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.holders.AdNativeHolder
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */