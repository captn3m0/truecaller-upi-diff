package com.truecaller.ads.provider.holders;

import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.truecaller.ads.provider.fetch.c;

public final class d
  extends b
{
  private final AdHolderType b;
  private final String c;
  private final String d;
  
  public d(NativeCustomTemplateAd paramNativeCustomTemplateAd, c paramc)
  {
    super(paramNativeCustomTemplateAd, paramc);
    paramc = AdHolderType.CUSTOM_AD;
    b = paramc;
    paramc = "custom";
    c = paramc;
    paramNativeCustomTemplateAd = paramNativeCustomTemplateAd.getCustomTemplateId();
    if (paramNativeCustomTemplateAd == null) {
      paramNativeCustomTemplateAd = "";
    }
    d = paramNativeCustomTemplateAd;
  }
  
  public final AdHolderType a()
  {
    return b;
  }
  
  public final String b()
  {
    return c;
  }
  
  public final String c()
  {
    return d;
  }
  
  public final void d()
  {
    ((NativeCustomTemplateAd)g()).destroy();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.holders.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */