package com.truecaller.ads.provider.holders;

public enum AdNativeHolder$Type
{
  static
  {
    Type[] arrayOfType = new Type[2];
    Type localType = new com/truecaller/ads/provider/holders/AdNativeHolder$Type;
    localType.<init>("CONTENT", 0);
    CONTENT = localType;
    arrayOfType[0] = localType;
    localType = new com/truecaller/ads/provider/holders/AdNativeHolder$Type;
    int i = 1;
    localType.<init>("INSTALL", i);
    INSTALL = localType;
    arrayOfType[i] = localType;
    $VALUES = arrayOfType;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.holders.AdNativeHolder.Type
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */