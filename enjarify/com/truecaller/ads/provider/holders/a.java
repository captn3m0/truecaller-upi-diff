package com.truecaller.ads.provider.holders;

import c.g.b.k;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.truecaller.ads.provider.fetch.c;

public final class a
  extends b
{
  private final AdHolderType b;
  private final String c;
  private final String d;
  
  public a(PublisherAdView paramPublisherAdView, c paramc)
  {
    super(paramPublisherAdView, paramc);
    paramc = AdHolderType.PUBLISHER_VIEW;
    b = paramc;
    c = "banner";
    paramPublisherAdView = paramPublisherAdView.getAdSize().toString();
    k.a(paramPublisherAdView, "ad.adSize.toString()");
    d = paramPublisherAdView;
  }
  
  public final AdHolderType a()
  {
    return b;
  }
  
  public final String b()
  {
    return c;
  }
  
  public final String c()
  {
    return d;
  }
  
  public final void d() {}
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.holders.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */