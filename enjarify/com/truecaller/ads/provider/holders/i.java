package com.truecaller.ads.provider.holders;

import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.truecaller.ads.provider.fetch.c;

public final class i
  extends AdNativeHolder
{
  private final AdNativeHolder.Type b;
  
  public i(NativeAppInstallAd paramNativeAppInstallAd, c paramc)
  {
    super(paramc, paramNativeAppInstallAd);
    paramNativeAppInstallAd = AdNativeHolder.Type.INSTALL;
    b = paramNativeAppInstallAd;
  }
  
  public final String c()
  {
    return "install";
  }
  
  public final AdNativeHolder.Type i()
  {
    return b;
  }
  
  protected final void j()
  {
    ((NativeAppInstallAd)k()).destroy();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.holders.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */