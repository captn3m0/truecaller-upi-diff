package com.truecaller.ads.provider;

import javax.inject.Provider;

public final class d
  implements dagger.a.d
{
  private final Provider a;
  private final Provider b;
  
  private d(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static d a(Provider paramProvider1, Provider paramProvider2)
  {
    d locald = new com/truecaller/ads/provider/d;
    locald.<init>(paramProvider1, paramProvider2);
    return locald;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */