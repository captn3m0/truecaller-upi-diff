package com.truecaller.ads.provider;

import android.support.v4.f.o;
import c.g.a.m;
import com.truecaller.ads.g;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.bs;

public final class l
  implements g, e, ag
{
  final o a;
  final HashSet b;
  private bn c;
  private g d;
  private final o e;
  private boolean f;
  private bn g;
  private final f h;
  private final com.truecaller.ads.k i;
  private final c.d.f j;
  
  public l(f paramf, com.truecaller.ads.k paramk, c.d.f paramf1)
  {
    h = paramf;
    i = paramk;
    j = paramf1;
    paramf = bs.a(null);
    c = paramf;
    paramf = new android/support/v4/f/o;
    paramf.<init>();
    a = paramf;
    paramf = new android/support/v4/f/o;
    paramf.<init>();
    e = paramf;
    paramf = new java/util/HashSet;
    paramf.<init>();
    b = paramf;
    paramf = h;
    paramk = i;
    paramf1 = this;
    paramf1 = (g)this;
    paramf.a(paramk, paramf1);
  }
  
  private final void h()
  {
    boolean bool = f;
    if (!bool)
    {
      Object localObject = h;
      com.truecaller.ads.k localk = i;
      bool = ((f)localObject).a(localk);
      if (bool)
      {
        localObject = d;
        if (localObject != null)
        {
          ((g)localObject).a();
          return;
        }
      }
    }
  }
  
  public final c.d.f V_()
  {
    c.d.f localf1 = j;
    c.d.f localf2 = (c.d.f)c;
    return localf1.plus(localf2);
  }
  
  public final void a()
  {
    h();
  }
  
  public final void a(int paramInt)
  {
    g localg = d;
    if (localg != null)
    {
      localg.a(paramInt);
      return;
    }
  }
  
  public final void a(long paramLong)
  {
    Object localObject = new com/truecaller/ads/provider/l$a;
    ((l.a)localObject).<init>(this, paramLong, null);
    localObject = (m)localObject;
    bn localbn = kotlinx.coroutines.e.b(this, null, (m)localObject, 3);
    g = localbn;
  }
  
  public final void a(g paramg)
  {
    d = paramg;
    f localf = h;
    com.truecaller.ads.k localk = i;
    boolean bool = localf.a(localk);
    if (bool)
    {
      bool = f;
      if ((!bool) && (paramg != null))
      {
        paramg.a();
        return;
      }
    }
  }
  
  public final void a(com.truecaller.ads.provider.holders.e parame, int paramInt)
  {
    c.g.b.k.b(parame, "ad");
    g localg = d;
    if (localg != null)
    {
      localg.a(parame, paramInt);
      return;
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    boolean bool = f;
    f = paramBoolean;
    if ((bool != paramBoolean) && (!paramBoolean))
    {
      f localf = h;
      com.truecaller.ads.k localk = i;
      paramBoolean = localf.a(localk);
      if (paramBoolean) {
        h();
      }
    }
  }
  
  public final com.truecaller.ads.provider.holders.e b(int paramInt)
  {
    Object localObject1 = (com.truecaller.ads.provider.holders.e)a.a(paramInt);
    if (localObject1 != null) {
      return (com.truecaller.ads.provider.holders.e)localObject1;
    }
    boolean bool = f;
    if (!bool)
    {
      localObject1 = h;
      localObject2 = i;
      localObject1 = ((f)localObject1).a((com.truecaller.ads.k)localObject2, paramInt);
      if (localObject1 != null)
      {
        localObject2 = b;
        Integer localInteger = Integer.valueOf(paramInt);
        ((HashSet)localObject2).remove(localInteger);
        a.b(paramInt, localObject1);
        localObject2 = (com.truecaller.ads.provider.holders.e)e.a(paramInt);
        if (localObject2 != null) {
          ((com.truecaller.ads.provider.holders.e)localObject2).d();
        }
        e.b(paramInt, localObject1);
        return (com.truecaller.ads.provider.holders.e)localObject1;
      }
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    ((HashSet)localObject1).add(localObject2);
    return (com.truecaller.ads.provider.holders.e)e.a(paramInt);
  }
  
  public final Set b()
  {
    HashSet localHashSet = new java/util/HashSet;
    Collection localCollection = (Collection)b;
    localHashSet.<init>(localCollection);
    b.clear();
    return (Set)localHashSet;
  }
  
  public final void d()
  {
    o localo = a;
    int k = localo.c();
    int m = 0;
    while (m < k)
    {
      int n = localo.c(m);
      Object localObject = localo.d(m);
      String str = "valueAt(i)";
      c.g.b.k.a(localObject, str);
      localObject = b;
      Integer localInteger = Integer.valueOf(n);
      ((HashSet)localObject).add(localInteger);
      m += 1;
    }
    a.d();
  }
  
  public final void e()
  {
    c.n();
    Object localObject1 = h;
    com.truecaller.ads.k localk = i;
    Object localObject2 = this;
    localObject2 = (g)this;
    ((f)localObject1).b(localk, (g)localObject2);
    localObject1 = e;
    int k = ((o)localObject1).c();
    int m = 0;
    localObject2 = null;
    while (m < k)
    {
      ((o)localObject1).c(m);
      Object localObject3 = ((o)localObject1).d(m);
      String str = "valueAt(i)";
      c.g.b.k.a(localObject3, str);
      localObject3 = (com.truecaller.ads.provider.holders.e)localObject3;
      ((com.truecaller.ads.provider.holders.e)localObject3).d();
      m += 1;
    }
    e.d();
  }
  
  public final void f()
  {
    bn localbn = g;
    if (localbn != null)
    {
      boolean bool = localbn.av_();
      if (bool)
      {
        Object localObject = new java/util/concurrent/CancellationException;
        String str = "View restored";
        ((CancellationException)localObject).<init>(str);
        localObject = (Throwable)localObject;
        localbn.c((Throwable)localObject);
      }
      return;
    }
  }
  
  public final boolean g()
  {
    Object localObject = h;
    boolean bool = ((f)localObject).a();
    if (bool)
    {
      localObject = h;
      bool = ((f)localObject).b();
      if (bool)
      {
        localObject = i;
        bool = k;
        if (bool) {
          return true;
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */