package com.truecaller.ads.provider;

import android.support.v4.f.o;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import java.util.HashSet;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ar;

final class l$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  l$a(l paraml, long paramLong, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/ads/provider/l$a;
    l locall = b;
    long l = c;
    locala.<init>(locall, l, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      k = paramObject instanceof o.b;
      if (k != 0) {
        throw a;
      }
      break;
    case 0: 
      boolean bool = paramObject instanceof o.b;
      if (bool) {
        break label215;
      }
      long l = c;
      int n = 1;
      a = n;
      paramObject = ar.a(l, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    paramObject = b.a;
    int k = 0;
    locala = null;
    int j = ((o)paramObject).c();
    while (k < j)
    {
      Integer localInteger = Integer.valueOf(((o)paramObject).c(k));
      Object localObject = ((o)paramObject).d(k);
      String str = "valueAt(i)";
      c.g.b.k.a(localObject, str);
      int i1 = ((Number)localInteger).intValue();
      localObject = b.b;
      localInteger = Integer.valueOf(i1);
      ((HashSet)localObject).add(localInteger);
      int m;
      k += 1;
    }
    b.a.d();
    return x.a;
    label215:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.l.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */