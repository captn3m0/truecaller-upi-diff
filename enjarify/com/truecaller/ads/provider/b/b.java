package com.truecaller.ads.provider.b;

import dagger.a.d;
import javax.inject.Provider;

public final class b
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private b(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
  }
  
  public static b a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    b localb = new com/truecaller/ads/provider/b/b;
    localb.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */