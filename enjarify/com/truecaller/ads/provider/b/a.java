package com.truecaller.ads.provider.b;

import com.truecaller.ads.provider.fetch.c;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e.a;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bn;

public final class a
  implements i, ag
{
  private final Map a;
  private final AtomicLong b;
  private final c.d.f c;
  private final com.truecaller.i.a d;
  private final com.truecaller.featuretoggles.e e;
  private final j f;
  
  public a(c.d.f paramf, com.truecaller.i.a parama, com.truecaller.featuretoggles.e parame, j paramj)
  {
    c = paramf;
    d = parama;
    e = parame;
    f = paramj;
    paramf = new java/util/LinkedHashMap;
    paramf.<init>();
    paramf = (Map)paramf;
    a = paramf;
    paramf = new java/util/concurrent/atomic/AtomicLong;
    paramf.<init>();
    b = paramf;
  }
  
  public final c.d.f V_()
  {
    return c;
  }
  
  final long a()
  {
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    long l = d.a("adsFeatureHouseAdsTimeout", 0L);
    return localTimeUnit.toMillis(l);
  }
  
  public final void a(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    Map localMap = a;
    paramk = (f)localMap.remove(paramk);
    if (paramk != null)
    {
      paramk = e;
      if (paramk != null)
      {
        paramk.n();
        return;
      }
    }
  }
  
  public final void a(com.truecaller.ads.k paramk, h paramh)
  {
    c.g.b.k.b(paramk, "config");
    Object localObject1 = "listener";
    c.g.b.k.b(paramh, (String)localObject1);
    a(paramk);
    long l1 = a();
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    if (bool1)
    {
      localObject1 = e;
      Object localObject2 = l;
      Object localObject3 = com.truecaller.featuretoggles.e.a;
      int i = 33;
      localObject3 = localObject3[i];
      localObject1 = ((e.a)localObject2).a((com.truecaller.featuretoggles.e)localObject1, (c.l.g)localObject3);
      boolean bool2 = ((b)localObject1).a();
      if (bool2)
      {
        bool2 = l;
        if (bool2)
        {
          localObject1 = a;
          localObject2 = new com/truecaller/ads/provider/b/f;
          ((f)localObject2).<init>(paramk, paramh);
          ((Map)localObject1).put(paramk, localObject2);
        }
      }
    }
  }
  
  public final com.truecaller.ads.provider.holders.e b(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    Object localObject1 = (f)a.get(paramk);
    com.truecaller.ads.provider.holders.g localg = null;
    if (localObject1 == null) {
      return null;
    }
    boolean bool = c(paramk);
    if (bool)
    {
      bool = true;
      d = bool;
      localObject1 = f.a();
      if (localObject1 == null) {
        return null;
      }
      localg = new com/truecaller/ads/provider/holders/g;
      c localc = new com/truecaller/ads/provider/fetch/c;
      String str1 = a;
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("house ");
      Object localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("0000");
      long l = b.getAndIncrement();
      ((StringBuilder)localObject3).append(l);
      ((StringBuilder)localObject3).append('}');
      localObject3 = c.n.m.b(((StringBuilder)localObject3).toString(), 5);
      ((StringBuilder)localObject2).append((String)localObject3);
      String str2 = ((StringBuilder)localObject2).toString();
      localObject2 = localc;
      localObject3 = paramk;
      localc.<init>(paramk, str1, null, null, null, false, false, str2);
      localg.<init>((e)localObject1, localc);
      return (com.truecaller.ads.provider.holders.e)localg;
    }
    return null;
  }
  
  public final boolean c(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    paramk = (f)a.get(paramk);
    if (paramk == null) {
      return false;
    }
    boolean bool1 = c;
    if (!bool1)
    {
      bool1 = b;
      if (!bool1) {}
    }
    else
    {
      boolean bool2 = d;
      if (!bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final void d(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    f localf1 = (f)a.get(paramk);
    if (localf1 == null) {
      return;
    }
    f localf2 = null;
    d = false;
    boolean bool = localf1.a();
    if (!bool)
    {
      localf2 = (f)a.get(paramk);
      if (localf2 != null)
      {
        Object localObject = e;
        if (localObject != null) {
          ((bn)localObject).n();
        }
        localObject = new com/truecaller/ads/provider/b/a$a;
        ((a.a)localObject).<init>(this, localf2, paramk, null);
        localObject = (c.g.a.m)localObject;
        i = 3;
        paramk = kotlinx.coroutines.e.b(this, null, (c.g.a.m)localObject, i);
        e = paramk;
      }
    }
    int i = a + 1;
    a = i;
  }
  
  public final void e(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    Object localObject = a;
    paramk = (f)((Map)localObject).get(paramk);
    if (paramk == null) {
      return;
    }
    int i = a + -1;
    a = i;
    boolean bool = paramk.a();
    if (bool) {
      return;
    }
    localObject = e;
    if (localObject != null) {
      ((bn)localObject).n();
    }
    c = false;
    b = false;
  }
  
  public final void f(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    f localf = (f)a.get(paramk);
    if (localf == null) {
      return;
    }
    int i = a + -1;
    a = i;
    boolean bool = localf.a();
    if (bool) {
      return;
    }
    bn localbn = e;
    if (localbn != null) {
      localbn.n();
    }
    b = true;
    g(paramk);
  }
  
  final void g(com.truecaller.ads.k paramk)
  {
    boolean bool = c(paramk);
    if (bool)
    {
      Object localObject = (f)a.get(paramk);
      if (localObject != null)
      {
        localObject = f;
        if (localObject != null)
        {
          ((h)localObject).d(paramk);
          return;
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */