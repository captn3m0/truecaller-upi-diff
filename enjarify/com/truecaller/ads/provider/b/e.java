package com.truecaller.ads.provider.b;

import c.g.b.k;
import c.u;

public final class e
{
  public final String a;
  public final String b;
  public final String c;
  public final String d;
  public final String e;
  public final String f;
  
  public e(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
    d = paramString4;
    e = paramString5;
    f = paramString6;
  }
  
  public final boolean equals(Object paramObject)
  {
    Object localObject = this;
    localObject = (e)this;
    boolean bool1 = true;
    if (localObject == paramObject) {
      return bool1;
    }
    localObject = getClass();
    if (paramObject != null) {
      localClass = paramObject.getClass();
    } else {
      localClass = null;
    }
    boolean bool2 = k.a(localObject, localClass) ^ bool1;
    Class localClass = null;
    if (bool2) {
      return false;
    }
    if (paramObject != null)
    {
      localObject = a;
      paramObject = (e)paramObject;
      String str = a;
      bool2 = k.a(localObject, str) ^ bool1;
      if (bool2) {
        return false;
      }
      localObject = b;
      str = b;
      bool2 = k.a(localObject, str) ^ bool1;
      if (bool2) {
        return false;
      }
      localObject = c;
      str = c;
      bool2 = k.a(localObject, str) ^ bool1;
      if (bool2) {
        return false;
      }
      localObject = d;
      str = d;
      bool2 = k.a(localObject, str) ^ bool1;
      if (bool2) {
        return false;
      }
      localObject = e;
      str = e;
      bool2 = k.a(localObject, str) ^ bool1;
      if (bool2) {
        return false;
      }
      localObject = f;
      paramObject = f;
      boolean bool3 = k.a(localObject, paramObject) ^ bool1;
      if (bool3) {
        return false;
      }
      return bool1;
    }
    paramObject = new c/u;
    ((u)paramObject).<init>("null cannot be cast to non-null type com.truecaller.ads.provider.house.HouseAd");
    throw ((Throwable)paramObject);
  }
  
  public final int hashCode()
  {
    String str1 = a;
    int i = str1.hashCode() * 31;
    int j = b.hashCode();
    i = (i + j) * 31;
    j = c.hashCode();
    i = (i + j) * 31;
    j = d.hashCode();
    i = (i + j) * 31;
    String str2 = e;
    int k = 0;
    if (str2 != null)
    {
      j = str2.hashCode();
    }
    else
    {
      j = 0;
      str2 = null;
    }
    i = (i + j) * 31;
    str2 = f;
    if (str2 != null) {
      k = str2.hashCode();
    }
    return i + k;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */