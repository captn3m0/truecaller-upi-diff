package com.truecaller.ads.provider;

import c.a.m;
import com.truecaller.ads.provider.b.h;
import com.truecaller.ads.provider.b.i;
import com.truecaller.ads.provider.fetch.AdsConfigurationManager;
import com.truecaller.ads.provider.fetch.c;
import com.truecaller.analytics.ae;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.log.AssertionUtil;
import com.truecaller.tracking.events.b.a;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import kotlinx.coroutines.ag;
import org.apache.a.a;
import org.apache.a.d.d;

public final class g
  implements h, f, com.truecaller.ads.provider.fetch.k, ag
{
  private final Map a;
  private final Map b;
  private final c.d.f c;
  private final com.truecaller.androidactors.f d;
  private final AdsConfigurationManager e;
  private final com.truecaller.featuretoggles.e f;
  private final com.truecaller.ads.provider.fetch.g g;
  private final i h;
  
  public g(c.d.f paramf, com.truecaller.androidactors.f paramf1, AdsConfigurationManager paramAdsConfigurationManager, com.truecaller.featuretoggles.e parame, com.truecaller.ads.provider.fetch.g paramg, i parami)
  {
    c = paramf;
    d = paramf1;
    e = paramAdsConfigurationManager;
    f = parame;
    g = paramg;
    h = parami;
    paramf = new java/util/LinkedHashMap;
    paramf.<init>();
    paramf = (Map)paramf;
    a = paramf;
    paramf = new java/util/LinkedHashMap;
    paramf.<init>();
    paramf = (Map)paramf;
    b = paramf;
  }
  
  private final com.truecaller.ads.provider.fetch.f f(com.truecaller.ads.k paramk)
  {
    Object localObject1 = (com.truecaller.ads.provider.fetch.f)a.get(paramk);
    if (localObject1 == null)
    {
      localObject1 = ((Iterable)a.keySet()).iterator();
      Object localObject3;
      boolean bool3;
      do
      {
        bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject2 = ((Iterator)localObject1).next();
        localObject3 = localObject2;
        localObject3 = (com.truecaller.ads.k)localObject2;
        String str1 = a;
        String str2 = a;
        boolean bool2 = c.g.b.k.a(str1, str2);
        if (bool2)
        {
          localObject3 = b;
          str1 = b;
          bool3 = c.g.b.k.a(localObject3, str1);
          if (bool3)
          {
            bool3 = true;
            continue;
          }
        }
        bool3 = false;
        localObject3 = null;
      } while (!bool3);
      break label145;
      boolean bool1 = false;
      Object localObject2 = null;
      label145:
      localObject2 = (com.truecaller.ads.k)localObject2;
      if (localObject2 != null)
      {
        localObject1 = (com.truecaller.ads.provider.fetch.f)a.remove(localObject2);
        if (localObject1 != null)
        {
          ((com.truecaller.ads.provider.fetch.f)localObject1).b();
          localObject1 = h;
          ((i)localObject1).a((com.truecaller.ads.k)localObject2);
        }
        localObject1 = b;
        ((Map)localObject1).remove(localObject2);
      }
      localObject1 = g;
      localObject2 = this;
      localObject2 = (com.truecaller.ads.provider.fetch.k)this;
      localObject1 = ((com.truecaller.ads.provider.fetch.g)localObject1).a((com.truecaller.ads.provider.fetch.k)localObject2, paramk);
      localObject2 = a;
      ((Map)localObject2).put(paramk, localObject1);
      bool1 = l;
      if (bool1)
      {
        localObject2 = h;
        localObject3 = this;
        localObject3 = (h)this;
        ((i)localObject2).a(paramk, (h)localObject3);
      }
      else
      {
        localObject2 = h;
        ((i)localObject2).a(paramk);
      }
    }
    return (com.truecaller.ads.provider.fetch.f)localObject1;
  }
  
  private final Set g(com.truecaller.ads.k paramk)
  {
    Object localObject = (Set)b.get(paramk);
    if (localObject == null)
    {
      localObject = new java/util/LinkedHashSet;
      ((LinkedHashSet)localObject).<init>();
      localObject = (Set)localObject;
      Map localMap = b;
      localMap.put(paramk, localObject);
    }
    return (Set)localObject;
  }
  
  public final c.d.f V_()
  {
    return c;
  }
  
  public final com.truecaller.ads.provider.holders.e a(com.truecaller.ads.k paramk, int paramInt)
  {
    Object localObject = "config";
    c.g.b.k.b(paramk, (String)localObject);
    boolean bool = a();
    if (!bool) {
      return null;
    }
    localObject = f(paramk);
    com.truecaller.ads.provider.holders.e locale = ((com.truecaller.ads.provider.fetch.f)localObject).a(paramInt);
    if (locale == null) {
      locale = h.b(paramk);
    }
    return locale;
  }
  
  public final void a(com.truecaller.ads.k paramk, com.truecaller.ads.g paramg)
  {
    c.g.b.k.b(paramk, "config");
    c.g.b.k.b(paramg, "listener");
    boolean bool = true;
    String[] arrayOfString = new String[bool];
    String str1 = String.valueOf(paramk);
    String str2 = "Subscribing to ".concat(str1);
    arrayOfString[0] = str2;
    g(paramk).add(paramg);
    f(paramk).a(bool);
  }
  
  public final void a(com.truecaller.ads.k paramk, com.truecaller.ads.provider.holders.e parame, int paramInt)
  {
    c.g.b.k.b(paramk, "config");
    c.g.b.k.b(parame, "ad");
    Object localObject1 = h;
    if (localObject1 != null)
    {
      Object localObject2 = com.truecaller.tracking.events.b.b();
      localObject1 = (CharSequence)localObject1;
      localObject1 = ((b.a)localObject2).b((CharSequence)localObject1);
      localObject2 = (CharSequence)hb;
      localObject1 = ((b.a)localObject1).a((CharSequence)localObject2);
      localObject2 = Integer.valueOf(paramInt);
      localObject1 = ((b.a)localObject1).a((Integer)localObject2);
      localObject2 = (CharSequence)parame.b();
      localObject1 = ((b.a)localObject1).c((CharSequence)localObject2);
      localObject2 = (CharSequence)parame.c();
      localObject1 = ((b.a)localObject1).d((CharSequence)localObject2);
      try
      {
        localObject2 = d;
        localObject2 = ((com.truecaller.androidactors.f)localObject2).a();
        localObject2 = (ae)localObject2;
        localObject1 = ((b.a)localObject1).a();
        localObject1 = (d)localObject1;
        ((ae)localObject2).a((d)localObject1);
      }
      catch (a locala)
      {
        localObject1 = (Throwable)locala;
        AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
      }
    }
    paramk = ((Iterable)g(paramk)).iterator();
    for (;;)
    {
      boolean bool = paramk.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = (com.truecaller.ads.g)paramk.next();
      ((com.truecaller.ads.g)localObject1).a(parame, paramInt);
    }
  }
  
  public final boolean a()
  {
    return e.d();
  }
  
  public final boolean a(com.truecaller.ads.k paramk)
  {
    Object localObject = "config";
    c.g.b.k.b(paramk, (String)localObject);
    boolean bool1 = a();
    if (bool1)
    {
      localObject = f(paramk);
      bool1 = ((com.truecaller.ads.provider.fetch.f)localObject).a();
      if (!bool1)
      {
        localObject = h;
        boolean bool2 = ((i)localObject).c(paramk);
        if (!bool2) {}
      }
      else
      {
        return true;
      }
    }
    return false;
  }
  
  public final void b(com.truecaller.ads.k paramk)
  {
    String str = "config";
    c.g.b.k.b(paramk, str);
    boolean bool = a();
    if (!bool) {
      return;
    }
    f(paramk).d();
  }
  
  public final void b(com.truecaller.ads.k paramk, int paramInt)
  {
    c.g.b.k.b(paramk, "config");
    Iterator localIterator = ((Iterable)m.l((Iterable)g(paramk))).iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      com.truecaller.ads.g localg = (com.truecaller.ads.g)localIterator.next();
      localg.a(paramInt);
    }
    h.f(paramk);
  }
  
  public final void b(com.truecaller.ads.k paramk, com.truecaller.ads.g paramg)
  {
    c.g.b.k.b(paramk, "config");
    c.g.b.k.b(paramg, "listener");
    Set localSet = g(paramk);
    boolean bool = localSet.remove(paramg);
    if (bool)
    {
      paramg = g(paramk);
      bool = paramg.isEmpty();
      if (bool)
      {
        paramg = f(paramk);
        localSet = null;
        paramg.a(false);
        bool = true;
        paramg = new String[bool];
        String str = "Unsubscribing from ";
        paramk = String.valueOf(paramk);
        paramk = str.concat(paramk);
        paramg[0] = paramk;
      }
    }
  }
  
  public final boolean b()
  {
    com.truecaller.featuretoggles.e locale = f;
    e.a locala = k;
    c.l.g localg = com.truecaller.featuretoggles.e.a[32];
    return locala.a(locale, localg).a();
  }
  
  public final void c(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    Object localObject = h;
    ((i)localObject).e(paramk);
    paramk = ((Iterable)m.l((Iterable)g(paramk))).iterator();
    for (;;)
    {
      boolean bool = paramk.hasNext();
      if (!bool) {
        break;
      }
      localObject = (com.truecaller.ads.g)paramk.next();
      ((com.truecaller.ads.g)localObject).a();
    }
  }
  
  public final void d()
  {
    Iterator localIterator = ((Iterable)m.i((Iterable)a.values())).iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      com.truecaller.ads.provider.fetch.f localf = (com.truecaller.ads.provider.fetch.f)localIterator.next();
      localf.b();
    }
    a.clear();
  }
  
  public final void d(com.truecaller.ads.k paramk)
  {
    Object localObject = "config";
    c.g.b.k.b(paramk, (String)localObject);
    paramk = ((Iterable)m.l((Iterable)g(paramk))).iterator();
    for (;;)
    {
      boolean bool = paramk.hasNext();
      if (!bool) {
        break;
      }
      localObject = (com.truecaller.ads.g)paramk.next();
      ((com.truecaller.ads.g)localObject).a();
    }
  }
  
  public final void e(com.truecaller.ads.k paramk)
  {
    c.g.b.k.b(paramk, "config");
    h.d(paramk);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.provider.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */