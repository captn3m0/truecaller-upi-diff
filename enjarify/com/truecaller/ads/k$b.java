package com.truecaller.ads;

import c.a.f;
import c.a.y;
import com.google.android.gms.ads.AdSize;
import java.util.List;

public final class k$b
  implements k.a, k.c
{
  public String a;
  public j b;
  String c;
  int d;
  List e;
  List f;
  String g;
  public boolean h;
  public boolean i;
  boolean j;
  boolean k;
  boolean l;
  public int m;
  
  private k$b()
  {
    int n = 1;
    d = n;
    List localList = (List)y.a;
    e = localList;
    localList = (List)y.a;
    f = localList;
    m = n;
  }
  
  private b b(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "adUnit");
    b localb = this;
    localb = (b)this;
    a = paramString1;
    c = paramString2;
    return localb;
  }
  
  public final b a()
  {
    d = 0;
    return (b)this;
  }
  
  public final b a(int paramInt)
  {
    b localb = this;
    localb = (b)this;
    m = paramInt;
    return localb;
  }
  
  public final b a(j paramj)
  {
    c.g.b.k.b(paramj, "campaign");
    b localb = this;
    localb = (b)this;
    b = paramj;
    return localb;
  }
  
  public final b a(AdSize... paramVarArgs)
  {
    c.g.b.k.b(paramVarArgs, "supportedBanners");
    b localb = this;
    localb = (b)this;
    paramVarArgs = f.f(paramVarArgs);
    e = paramVarArgs;
    return localb;
  }
  
  public final b a(CustomTemplate... paramVarArgs)
  {
    c.g.b.k.b(paramVarArgs, "supportedCustomTemplates");
    b localb = this;
    localb = (b)this;
    paramVarArgs = f.f(paramVarArgs);
    f = paramVarArgs;
    return localb;
  }
  
  public final b b()
  {
    b localb = this;
    localb = (b)this;
    j = true;
    return localb;
  }
  
  public final b b(String paramString)
  {
    c.g.b.k.b(paramString, "campaign");
    j.a locala = new com/truecaller/ads/j$a;
    locala.<init>(paramString);
    paramString = locala.a();
    c.g.b.k.a(paramString, "CampaignConfig.Builder(campaign).build()");
    b = paramString;
    paramString = this;
    return (b)this;
  }
  
  public final b c()
  {
    b localb = this;
    localb = (b)this;
    k = true;
    return localb;
  }
  
  public final b c(String paramString)
  {
    b localb = this;
    localb = (b)this;
    g = paramString;
    return localb;
  }
  
  public final b d()
  {
    b localb = this;
    localb = (b)this;
    l = true;
    return localb;
  }
  
  public final k e()
  {
    k localk = new com/truecaller/ads/k;
    localk.<init>(this);
    return localk;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.k.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */