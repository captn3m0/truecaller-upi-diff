package com.truecaller.ads.campaigns;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class AdCampaign
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final String a;
  public final AdCampaign.Style b;
  public final AdCampaign.CtaStyle c;
  final String[] d;
  
  static
  {
    AdCampaign.1 local1 = new com/truecaller/ads/campaigns/AdCampaign$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  private AdCampaign(Parcel paramParcel)
  {
    Object localObject = paramParcel.readString();
    a = ((String)localObject);
    localObject = paramParcel.createStringArray();
    d = ((String[])localObject);
    localObject = AdCampaign.class.getClassLoader();
    AdCampaign.Style localStyle = (AdCampaign.Style)paramParcel.readParcelable((ClassLoader)localObject);
    b = localStyle;
    paramParcel = (AdCampaign.CtaStyle)paramParcel.readParcelable((ClassLoader)localObject);
    c = paramParcel;
  }
  
  private AdCampaign(String paramString, AdCampaign.Style paramStyle, AdCampaign.CtaStyle paramCtaStyle, String[] paramArrayOfString)
  {
    a = paramString;
    b = paramStyle;
    c = paramCtaStyle;
    d = paramArrayOfString;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    Object localObject = a;
    paramParcel.writeString((String)localObject);
    localObject = d;
    paramParcel.writeStringArray((String[])localObject);
    localObject = b;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    localObject = c;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.campaigns.AdCampaign
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */