package com.truecaller.ads.campaigns;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class AdCampaign$CtaStyle
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final int a;
  public final int b;
  
  static
  {
    AdCampaign.CtaStyle.1 local1 = new com/truecaller/ads/campaigns/AdCampaign$CtaStyle$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  private AdCampaign$CtaStyle(int paramInt1, int paramInt2)
  {
    a = paramInt1;
    b = paramInt2;
  }
  
  private AdCampaign$CtaStyle(Parcel paramParcel)
  {
    int i = paramParcel.readInt();
    a = i;
    int j = paramParcel.readInt();
    b = j;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = a;
    paramParcel.writeInt(paramInt);
    paramInt = b;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.campaigns.AdCampaign.CtaStyle
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */