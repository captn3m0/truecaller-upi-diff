package com.truecaller.ads.campaigns;

import android.database.sqlite.SQLiteDatabase;
import com.truecaller.common.c.b.c;
import com.truecaller.common.c.b.c.a;

final class d
  extends c
{
  private static final c.a[] c;
  
  static
  {
    c.a[] arrayOfa = new c.a[15];
    c.a locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("_id", "INTEGER PRIMARY KEY");
    arrayOfa[0] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("number", "TEXT");
    arrayOfa[1] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("campaign_id", "TEXT");
    arrayOfa[2] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("placement", "TEXT");
    arrayOfa[3] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("expiration", "INTEGER");
    arrayOfa[4] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("start", "INTEGER");
    arrayOfa[5] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("end", "INTEGER");
    arrayOfa[6] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("request_order", "TEXT");
    arrayOfa[7] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("mainColor", "TEXT");
    arrayOfa[8] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("lightColor", "TEXT");
    arrayOfa[9] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("buttonColor", "TEXT");
    arrayOfa[10] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("imageUrl", "TEXT");
    arrayOfa[11] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("bannerBackgroundColor", "TEXT");
    arrayOfa[12] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("ctaBackgroundColor", "TEXT");
    arrayOfa[13] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("ctaTextColor", "TEXT");
    arrayOfa[14] = locala;
    c = arrayOfa;
  }
  
  d()
  {
    super("campaigns", arrayOfa);
  }
  
  public final void a(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    b(paramSQLiteDatabase);
    a(paramSQLiteDatabase);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.campaigns.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */