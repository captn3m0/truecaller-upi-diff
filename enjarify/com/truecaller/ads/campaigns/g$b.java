package com.truecaller.ads.campaigns;

import android.text.TextUtils;
import com.truecaller.common.h.j;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.h;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.UnmutedException.AdsIllegalStateException;
import com.truecaller.log.UnmutedException.AdsIllegalStateException.Cause;
import e.r;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import okhttp3.ad;
import org.c.a.a.a.k;

final class g$b
  implements Runnable
{
  private final String b;
  private final Set c;
  private final Integer d;
  private final String e;
  private final String f;
  private final String g;
  private final int h;
  
  private g$b(g paramg, String paramString1, int paramInt, Set paramSet, Integer paramInteger, String paramString2, String paramString3, List paramList)
  {
    b = paramString1;
    h = paramInt;
    c = paramSet;
    d = paramInteger;
    e = paramString2;
    f = paramString3;
    char c1;
    if (paramList == null)
    {
      c1 = '\000';
      paramg = null;
    }
    else
    {
      c1 = ',';
      paramg = k.a(paramList, c1);
    }
    g = paramg;
  }
  
  private b a(String paramString1, Integer paramInteger, String paramString2, String paramString3, String paramString4, String paramString5, int paramInt)
  {
    b localb = null;
    Object localObject1;
    Object localObject3;
    try
    {
      localObject1 = a;
      String str = ((g)localObject1).a();
      Object localObject2;
      if (paramInt < 0)
      {
        localObject2 = null;
      }
      else
      {
        localObject1 = Integer.valueOf(paramInt);
        localObject2 = localObject1;
      }
      localObject1 = KnownEndpoints.ADS;
      localObject3 = c.a.class;
      localObject1 = h.a((KnownEndpoints)localObject1, (Class)localObject3);
      localObject3 = localObject1;
      localObject3 = (c.a)localObject1;
      localObject1 = ((c.a)localObject3).a(str, paramString1, paramInteger, paramString2, paramString3, paramString4, paramString5, (Integer)localObject2);
      localObject1 = ((e.b)localObject1).c();
    }
    catch (IllegalStateException localIllegalStateException)
    {
      localObject1 = new com/truecaller/log/UnmutedException$AdsIllegalStateException;
      localObject3 = UnmutedException.AdsIllegalStateException.Cause.CAMPAIGN_REQUEST_ILLEGAL_STATE;
      ((UnmutedException.AdsIllegalStateException)localObject1).<init>((UnmutedException.AdsIllegalStateException.Cause)localObject3);
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      localObject3 = a;
      boolean bool = ((ad)localObject3).c();
      if (bool) {
        localb = (b)b;
      }
    }
    return localb;
  }
  
  public final void run()
  {
    Object localObject1 = c.iterator();
    Object localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    Object localObject4 = new java/util/ArrayList;
    ((ArrayList)localObject4).<init>();
    Object localObject5;
    Object localObject6;
    Object localObject7;
    long l1;
    long l2;
    boolean bool3;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      ((List)localObject3).clear();
      ((List)localObject4).clear();
      localObject5 = (String)((Iterator)localObject1).next();
      localObject6 = a.a;
      localObject7 = b;
      l1 = ((a)localObject6).a((String)localObject7, (String)localObject5, (List)localObject3, (List)localObject4);
      l2 = System.currentTimeMillis();
      bool3 = l1 < l2;
      if (bool3)
      {
        ((Iterator)localObject1).remove();
        localObject6 = new com/truecaller/ads/campaigns/AdCampaigns$a;
        ((AdCampaigns.a)localObject6).<init>((String)localObject5);
        localObject5 = org.c.a.a.a.a.c;
        localObject5 = (String[])((List)localObject4).toArray((Object[])localObject5);
        ((AdCampaigns.a)localObject6).a((String[])localObject5).a((List)localObject3);
        localObject5 = a;
        localObject7 = b;
        localObject6 = ((AdCampaigns.a)localObject6).a();
        g.a((g)localObject5, (String)localObject7, (AdCampaigns)localObject6);
      }
    }
    localObject1 = c;
    boolean bool4 = ((Set)localObject1).isEmpty();
    boolean bool5 = false;
    localObject4 = null;
    if (!bool4) {
      try
      {
        localObject1 = c;
        char c1 = ',';
        localObject7 = k.a((Iterable)localObject1, c1);
        Object localObject8 = d;
        Object localObject9 = e;
        String str1 = f;
        Object localObject10 = g;
        String str2 = b;
        int i = h;
        localObject6 = this;
        localObject1 = a((String)localObject7, (Integer)localObject8, (String)localObject9, str1, (String)localObject10, str2, i);
        if (localObject1 != null)
        {
          localObject5 = a;
          if (localObject5 != null)
          {
            localObject1 = a;
            localObject1 = ((List)localObject1).iterator();
            do
            {
              do
              {
                do
                {
                  do
                  {
                    boolean bool2 = ((Iterator)localObject1).hasNext();
                    if (!bool2) {
                      break;
                    }
                    localObject5 = ((Iterator)localObject1).next();
                    localObject5 = (b.a)localObject5;
                  } while (localObject5 == null);
                  l1 = c;
                  l2 = 0L;
                  bool3 = l1 < l2;
                } while (!bool3);
                localObject6 = b;
              } while (localObject6 == null);
              localObject6 = a;
              j = TextUtils.isEmpty((CharSequence)localObject6);
            } while (j != 0);
            localObject6 = a;
            localObject7 = b;
            g.a((g)localObject6, (String)localObject7, (b.a)localObject5);
            ((List)localObject3).clear();
            int j = 0;
            localObject6 = null;
            for (;;)
            {
              localObject7 = b;
              int m = localObject7.length;
              if (j >= m) {
                break;
              }
              localObject7 = b;
              localObject7 = localObject7[j];
              localObject8 = a;
              boolean bool7 = TextUtils.isEmpty((CharSequence)localObject8);
              if (!bool7)
              {
                int n = j.a();
                int i1 = c;
                long l3 = i1;
                int i2 = d;
                long l4;
                if (i2 != 0)
                {
                  i2 = d;
                  l4 = i2;
                }
                else
                {
                  localObject10 = TimeUnit.DAYS;
                  long l5 = 1L;
                  l4 = ((TimeUnit)localObject10).toMinutes(l5);
                }
                long l6 = n;
                boolean bool8 = l3 < l6;
                if (!bool8)
                {
                  bool8 = l4 < l6;
                  if (!bool8) {}
                }
                else
                {
                  bool8 = l3 < l4;
                  if (!bool8) {
                    break label768;
                  }
                  bool8 = l6 < l4;
                  if (bool8)
                  {
                    bool8 = l6 < l3;
                    if (bool8) {
                      break label768;
                    }
                  }
                }
                localObject8 = new com/truecaller/ads/campaigns/AdCampaign$a;
                localObject9 = a;
                ((AdCampaign.a)localObject8).<init>((String)localObject9);
                localObject9 = b;
                ((AdCampaign.a)localObject8).a((String[])localObject9);
                localObject7 = e;
                if (localObject7 != null)
                {
                  localObject9 = a;
                  a = ((String)localObject9);
                  localObject9 = b;
                  b = ((String)localObject9);
                  localObject9 = c;
                  c = ((String)localObject9);
                  localObject9 = e;
                  d = ((String)localObject9);
                  localObject9 = d;
                  e = ((String)localObject9);
                  localObject9 = f;
                  f = ((String)localObject9);
                  localObject7 = g;
                  g = ((String)localObject7);
                }
                localObject7 = ((AdCampaign.a)localObject8).a();
                ((List)localObject3).add(localObject7);
              }
              label768:
              int k;
              j += 1;
            }
            localObject6 = c;
            localObject6 = ((Set)localObject6).iterator();
            for (;;)
            {
              boolean bool6 = ((Iterator)localObject6).hasNext();
              if (!bool6) {
                break;
              }
              localObject7 = ((Iterator)localObject6).next();
              localObject7 = (String)localObject7;
              localObject8 = a;
              bool6 = ((String)localObject7).equalsIgnoreCase((String)localObject8);
              if (bool6)
              {
                localObject7 = new com/truecaller/ads/campaigns/AdCampaigns$a;
                localObject8 = a;
                ((AdCampaigns.a)localObject7).<init>((String)localObject8);
                localObject8 = ((AdCampaigns.a)localObject7).a((List)localObject3);
                localObject9 = d;
                ((AdCampaigns.a)localObject8).a((String[])localObject9);
                localObject8 = a;
                localObject9 = b;
                localObject7 = ((AdCampaigns.a)localObject7).a();
                g.a((g)localObject8, (String)localObject9, (AdCampaigns)localObject7);
                ((Iterator)localObject6).remove();
              }
            }
          }
        }
        localObject2 = c;
      }
      catch (IOException localIOException)
      {
        AssertionUtil.reportThrowableButNeverCrash(localIOException);
      }
    }
    Object localObject2;
    bool4 = ((Set)localObject2).isEmpty();
    if (!bool4)
    {
      localObject2 = new AdCampaign[0];
      localObject3 = c.iterator();
      for (;;)
      {
        bool5 = ((Iterator)localObject3).hasNext();
        if (!bool5) {
          break;
        }
        localObject4 = (String)((Iterator)localObject3).next();
        localObject5 = a;
        localObject6 = b;
        localObject7 = new com/truecaller/ads/campaigns/AdCampaigns$a;
        ((AdCampaigns.a)localObject7).<init>((String)localObject4);
        a = ((AdCampaign[])localObject2);
        localObject4 = ((AdCampaigns.a)localObject7).a();
        g.a((g)localObject5, (String)localObject6, (AdCampaigns)localObject4);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.campaigns.g.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */