package com.truecaller.ads.campaigns;

import android.graphics.Color;
import android.text.TextUtils;
import com.truecaller.log.AssertionUtil;
import org.c.a.a.a.a;

public final class AdCampaign$a
{
  String a;
  String b;
  String c;
  String d;
  String e;
  String f;
  String g;
  private final String h;
  private String[] i;
  
  public AdCampaign$a(String paramString)
  {
    String[] arrayOfString = a.c;
    i = arrayOfString;
    h = paramString;
  }
  
  public final a a(String[] paramArrayOfString)
  {
    paramArrayOfString = a.a(paramArrayOfString);
    i = paramArrayOfString;
    return this;
  }
  
  public final AdCampaign a()
  {
    Object localObject1 = a;
    boolean bool = TextUtils.isEmpty((CharSequence)localObject1);
    int j;
    if (!bool)
    {
      localObject1 = b;
      bool = TextUtils.isEmpty((CharSequence)localObject1);
      if (!bool)
      {
        localObject1 = c;
        bool = TextUtils.isEmpty((CharSequence)localObject1);
        if (!bool)
        {
          localObject1 = d;
          bool = TextUtils.isEmpty((CharSequence)localObject1);
          if (!bool) {
            try
            {
              localObject1 = new com/truecaller/ads/campaigns/AdCampaign$Style;
              localObject3 = a;
              j = Color.parseColor((String)localObject3);
              localObject3 = b;
              k = Color.parseColor((String)localObject3);
              localObject3 = c;
              m = Color.parseColor((String)localObject3);
              localObject3 = d;
              int n = Color.parseColor((String)localObject3);
              String str1 = e;
              localObject3 = localObject1;
              ((AdCampaign.Style)localObject1).<init>(j, k, m, n, str1, (byte)0);
              localObject4 = localObject1;
            }
            catch (IllegalArgumentException localIllegalArgumentException1)
            {
              AssertionUtil.reportThrowableButNeverCrash(localIllegalArgumentException1);
            }
          }
        }
      }
    }
    int k = 0;
    Object localObject4 = null;
    Object localObject2 = f;
    bool = TextUtils.isEmpty((CharSequence)localObject2);
    if (!bool)
    {
      localObject2 = g;
      bool = TextUtils.isEmpty((CharSequence)localObject2);
      if (!bool) {
        try
        {
          localObject2 = new com/truecaller/ads/campaigns/AdCampaign$CtaStyle;
          localObject3 = f;
          int i1 = Color.parseColor((String)localObject3);
          str2 = g;
          j = Color.parseColor(str2);
          m = 0;
          localObject5 = null;
          ((AdCampaign.CtaStyle)localObject2).<init>(i1, j, (byte)0);
          localObject5 = localObject2;
        }
        catch (IllegalArgumentException localIllegalArgumentException2)
        {
          AssertionUtil.reportThrowableButNeverCrash(localIllegalArgumentException2);
        }
      }
    }
    int m = 0;
    Object localObject5 = null;
    AdCampaign localAdCampaign = new com/truecaller/ads/campaigns/AdCampaign;
    String str2 = h;
    String[] arrayOfString = i;
    Object localObject3 = localAdCampaign;
    localAdCampaign.<init>(str2, (AdCampaign.Style)localObject4, (AdCampaign.CtaStyle)localObject5, arrayOfString, (byte)0);
    return localAdCampaign;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.campaigns.AdCampaign.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */