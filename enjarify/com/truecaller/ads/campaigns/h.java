package com.truecaller.ads.campaigns;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

final class h
  implements ThreadFactory
{
  private final AtomicInteger a;
  
  h()
  {
    AtomicInteger localAtomicInteger = new java/util/concurrent/atomic/AtomicInteger;
    localAtomicInteger.<init>(1);
    a = localAtomicInteger;
  }
  
  public final Thread newThread(Runnable paramRunnable)
  {
    Thread localThread = new java/lang/Thread;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("AdManager-Worker-");
    int i = a.getAndIncrement();
    ((StringBuilder)localObject).append(i);
    localObject = ((StringBuilder)localObject).toString();
    localThread.<init>(paramRunnable, (String)localObject);
    localThread.setDaemon(true);
    localThread.setPriority(5);
    return localThread;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.campaigns.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */