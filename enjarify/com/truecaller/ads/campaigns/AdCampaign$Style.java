package com.truecaller.ads.campaigns;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class AdCampaign$Style
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final int a;
  public final int b;
  public final int c;
  public final int d;
  public final String e;
  
  static
  {
    AdCampaign.Style.1 local1 = new com/truecaller/ads/campaigns/AdCampaign$Style$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  private AdCampaign$Style(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString)
  {
    a = paramInt1;
    b = paramInt2;
    c = paramInt3;
    d = paramInt4;
    e = paramString;
  }
  
  private AdCampaign$Style(Parcel paramParcel)
  {
    int i = paramParcel.readInt();
    a = i;
    i = paramParcel.readInt();
    b = i;
    i = paramParcel.readInt();
    c = i;
    i = paramParcel.readInt();
    d = i;
    paramParcel = paramParcel.readString();
    e = paramParcel;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramInt = a;
    paramParcel.writeInt(paramInt);
    paramInt = b;
    paramParcel.writeInt(paramInt);
    paramInt = c;
    paramParcel.writeInt(paramInt);
    paramInt = d;
    paramParcel.writeInt(paramInt);
    String str = e;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.campaigns.AdCampaign.Style
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */