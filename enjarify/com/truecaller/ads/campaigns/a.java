package com.truecaller.ads.campaigns;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.truecaller.common.c.b.c;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.c.a.a.a.k;

final class a
{
  private static final c[] b;
  private static final com.truecaller.common.c.b.d[] c;
  private static final String[] d = tmp77_61;
  final com.truecaller.common.c.b.a a;
  
  static
  {
    c[] arrayOfc = new c[1];
    d locald = new com/truecaller/ads/campaigns/d;
    locald.<init>();
    arrayOfc[0] = locald;
    b = arrayOfc;
    c = new com.truecaller.common.c.b.d[0];
    String[] tmp33_30 = new String[10];
    String[] tmp34_33 = tmp33_30;
    String[] tmp34_33 = tmp33_30;
    tmp34_33[0] = "campaign_id";
    tmp34_33[1] = "request_order";
    String[] tmp43_34 = tmp34_33;
    String[] tmp43_34 = tmp34_33;
    tmp43_34[2] = "expiration";
    tmp43_34[3] = "mainColor";
    String[] tmp52_43 = tmp43_34;
    String[] tmp52_43 = tmp43_34;
    tmp52_43[4] = "lightColor";
    tmp52_43[5] = "buttonColor";
    String[] tmp61_52 = tmp52_43;
    String[] tmp61_52 = tmp52_43;
    tmp61_52[6] = "imageUrl";
    tmp61_52[7] = "bannerBackgroundColor";
    tmp61_52[8] = "ctaBackgroundColor";
    String[] tmp77_61 = tmp61_52;
    tmp77_61[9] = "ctaTextColor";
  }
  
  a(Context paramContext)
  {
    c[] arrayOfc = b;
    Object localObject = c;
    paramContext = com.truecaller.common.c.b.a.a(paramContext, "adCampaigns.db", 3, arrayOfc, (com.truecaller.common.c.b.d[])localObject);
    a = paramContext;
    paramContext = a.getWritableDatabase();
    localObject = new String[1];
    String str = String.valueOf(System.currentTimeMillis());
    localObject[0] = str;
    paramContext.delete("campaigns", "expiration<?", (String[])localObject);
  }
  
  private static String a(String[] paramArrayOfString)
  {
    return k.a(org.c.a.a.a.a.a(paramArrayOfString), "|");
  }
  
  private static String[] a(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool) {
      return org.c.a.a.a.a.c;
    }
    return k.a(paramString, '|');
  }
  
  /* Error */
  public final long a(String paramString1, String paramString2, List paramList1, List paramList2)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 62	com/truecaller/ads/campaigns/a:a	Lcom/truecaller/common/c/b/a;
    //   4: invokevirtual 66	com/truecaller/common/c/b/a:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   7: astore 5
    //   9: invokestatic 115	com/truecaller/common/h/j:a	()I
    //   12: invokestatic 118	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   15: astore 6
    //   17: ldc 68
    //   19: astore 7
    //   21: getstatic 51	com/truecaller/ads/campaigns/a:d	[Ljava/lang/String;
    //   24: astore 8
    //   26: ldc 120
    //   28: astore 9
    //   30: bipush 6
    //   32: istore 10
    //   34: iload 10
    //   36: anewarray 49	java/lang/String
    //   39: astore 11
    //   41: iconst_0
    //   42: istore 12
    //   44: aload 11
    //   46: iconst_0
    //   47: aload_1
    //   48: aastore
    //   49: iconst_1
    //   50: istore 13
    //   52: aload 11
    //   54: iload 13
    //   56: aload_2
    //   57: aastore
    //   58: aload 11
    //   60: iconst_2
    //   61: aload 6
    //   63: aastore
    //   64: iconst_3
    //   65: istore 14
    //   67: aload 11
    //   69: iload 14
    //   71: aload 6
    //   73: aastore
    //   74: iconst_4
    //   75: istore 15
    //   77: aload 11
    //   79: iload 15
    //   81: aload 6
    //   83: aastore
    //   84: aload 11
    //   86: iconst_5
    //   87: aload 6
    //   89: aastore
    //   90: iconst_0
    //   91: istore 16
    //   93: aconst_null
    //   94: astore 6
    //   96: iconst_5
    //   97: istore 17
    //   99: iconst_0
    //   100: istore 18
    //   102: aconst_null
    //   103: astore 19
    //   105: aload 5
    //   107: aload 7
    //   109: aload 8
    //   111: aload 9
    //   113: aload 11
    //   115: aconst_null
    //   116: aconst_null
    //   117: aconst_null
    //   118: invokevirtual 128	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   121: astore 5
    //   123: lconst_0
    //   124: lstore 20
    //   126: aload 5
    //   128: ifnonnull +6 -> 134
    //   131: lload 20
    //   133: lreturn
    //   134: aload 5
    //   136: invokeinterface 134 1 0
    //   141: istore 16
    //   143: iload 16
    //   145: ifeq +345 -> 490
    //   148: ldc2_w 135
    //   151: lstore 22
    //   153: lload 22
    //   155: lstore 24
    //   157: aload 5
    //   159: iconst_0
    //   160: invokeinterface 139 2 0
    //   165: astore 6
    //   167: aload 6
    //   169: invokestatic 104	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   172: istore 18
    //   174: iload 18
    //   176: ifne +204 -> 380
    //   179: new 141	com/truecaller/ads/campaigns/AdCampaign$a
    //   182: astore 19
    //   184: aload 19
    //   186: aload 6
    //   188: invokespecial 144	com/truecaller/ads/campaigns/AdCampaign$a:<init>	(Ljava/lang/String;)V
    //   191: aload 5
    //   193: iload 13
    //   195: invokeinterface 139 2 0
    //   200: astore 6
    //   202: aload 6
    //   204: invokestatic 147	com/truecaller/ads/campaigns/a:a	(Ljava/lang/String;)[Ljava/lang/String;
    //   207: astore 6
    //   209: aload 19
    //   211: aload 6
    //   213: invokevirtual 150	com/truecaller/ads/campaigns/AdCampaign$a:a	([Ljava/lang/String;)Lcom/truecaller/ads/campaigns/AdCampaign$a;
    //   216: pop
    //   217: aload 5
    //   219: iload 14
    //   221: invokeinterface 139 2 0
    //   226: astore 6
    //   228: aload 19
    //   230: aload 6
    //   232: putfield 153	com/truecaller/ads/campaigns/AdCampaign$a:a	Ljava/lang/String;
    //   235: aload 5
    //   237: iload 15
    //   239: invokeinterface 139 2 0
    //   244: astore 6
    //   246: aload 19
    //   248: aload 6
    //   250: putfield 155	com/truecaller/ads/campaigns/AdCampaign$a:b	Ljava/lang/String;
    //   253: aload 5
    //   255: iload 17
    //   257: invokeinterface 139 2 0
    //   262: astore 6
    //   264: aload 19
    //   266: aload 6
    //   268: putfield 157	com/truecaller/ads/campaigns/AdCampaign$a:c	Ljava/lang/String;
    //   271: bipush 7
    //   273: istore 16
    //   275: aload 5
    //   277: iload 16
    //   279: invokeinterface 139 2 0
    //   284: astore 6
    //   286: aload 19
    //   288: aload 6
    //   290: putfield 160	com/truecaller/ads/campaigns/AdCampaign$a:d	Ljava/lang/String;
    //   293: aload 5
    //   295: iload 10
    //   297: invokeinterface 139 2 0
    //   302: astore 6
    //   304: aload 19
    //   306: aload 6
    //   308: putfield 163	com/truecaller/ads/campaigns/AdCampaign$a:e	Ljava/lang/String;
    //   311: bipush 8
    //   313: istore 16
    //   315: aload 5
    //   317: iload 16
    //   319: invokeinterface 139 2 0
    //   324: astore 6
    //   326: aload 19
    //   328: aload 6
    //   330: putfield 167	com/truecaller/ads/campaigns/AdCampaign$a:f	Ljava/lang/String;
    //   333: bipush 9
    //   335: istore 16
    //   337: aload 5
    //   339: iload 16
    //   341: invokeinterface 139 2 0
    //   346: astore 6
    //   348: aload 19
    //   350: aload 6
    //   352: putfield 171	com/truecaller/ads/campaigns/AdCampaign$a:g	Ljava/lang/String;
    //   355: aload 19
    //   357: invokevirtual 174	com/truecaller/ads/campaigns/AdCampaign$a:a	()Lcom/truecaller/ads/campaigns/AdCampaign;
    //   360: astore 6
    //   362: aload_3
    //   363: astore 19
    //   365: aload_3
    //   366: aload 6
    //   368: invokeinterface 180 2 0
    //   373: pop
    //   374: iconst_2
    //   375: istore 16
    //   377: goto +35 -> 412
    //   380: aload_3
    //   381: astore 19
    //   383: aload 5
    //   385: iload 13
    //   387: invokeinterface 139 2 0
    //   392: astore 6
    //   394: aload 6
    //   396: invokestatic 147	com/truecaller/ads/campaigns/a:a	(Ljava/lang/String;)[Ljava/lang/String;
    //   399: astore 6
    //   401: aload 4
    //   403: aload 6
    //   405: invokestatic 186	java/util/Collections:addAll	(Ljava/util/Collection;[Ljava/lang/Object;)Z
    //   408: pop
    //   409: iconst_2
    //   410: istore 16
    //   412: aload 5
    //   414: iload 16
    //   416: invokeinterface 190 2 0
    //   421: lstore 26
    //   423: lload 26
    //   425: lload 20
    //   427: lcmp
    //   428: istore 28
    //   430: iload 28
    //   432: ifeq +12 -> 444
    //   435: lload 24
    //   437: lload 26
    //   439: invokestatic 196	java/lang/Math:min	(JJ)J
    //   442: lstore 24
    //   444: aload 5
    //   446: invokeinterface 199 1 0
    //   451: istore 12
    //   453: iload 12
    //   455: ifne +22 -> 477
    //   458: lload 24
    //   460: lload 22
    //   462: lcmp
    //   463: istore 16
    //   465: iload 16
    //   467: ifeq +23 -> 490
    //   470: lload 24
    //   472: lstore 20
    //   474: goto +16 -> 490
    //   477: bipush 6
    //   479: istore 10
    //   481: iconst_0
    //   482: istore 12
    //   484: iconst_1
    //   485: istore 13
    //   487: goto -330 -> 157
    //   490: aload 5
    //   492: invokeinterface 202 1 0
    //   497: goto +18 -> 515
    //   500: astore 6
    //   502: goto +16 -> 518
    //   505: astore 6
    //   507: aload 6
    //   509: invokestatic 208	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   512: goto -22 -> 490
    //   515: lload 20
    //   517: lreturn
    //   518: aload 5
    //   520: invokeinterface 202 1 0
    //   525: aload 6
    //   527: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	528	0	this	a
    //   0	528	1	paramString1	String
    //   0	528	2	paramString2	String
    //   0	528	3	paramList1	List
    //   0	528	4	paramList2	List
    //   7	512	5	localObject1	Object
    //   15	389	6	localObject2	Object
    //   500	1	6	localObject3	Object
    //   505	21	6	localException	Exception
    //   19	89	7	str1	String
    //   24	86	8	arrayOfString1	String[]
    //   28	84	9	str2	String
    //   32	448	10	i	int
    //   39	75	11	arrayOfString2	String[]
    //   42	441	12	bool1	boolean
    //   50	436	13	j	int
    //   65	155	14	k	int
    //   75	163	15	m	int
    //   91	53	16	bool2	boolean
    //   273	142	16	n	int
    //   463	3	16	bool3	boolean
    //   97	159	17	i1	int
    //   100	75	18	bool4	boolean
    //   103	279	19	localObject4	Object
    //   124	392	20	l1	long
    //   151	310	22	l2	long
    //   155	316	24	l3	long
    //   421	17	26	l4	long
    //   428	3	28	bool5	boolean
    // Exception table:
    //   from	to	target	type
    //   134	141	500	finally
    //   159	165	500	finally
    //   167	172	500	finally
    //   179	182	500	finally
    //   186	191	500	finally
    //   193	200	500	finally
    //   202	207	500	finally
    //   211	217	500	finally
    //   219	226	500	finally
    //   230	235	500	finally
    //   237	244	500	finally
    //   248	253	500	finally
    //   255	262	500	finally
    //   266	271	500	finally
    //   277	284	500	finally
    //   288	293	500	finally
    //   295	302	500	finally
    //   306	311	500	finally
    //   317	324	500	finally
    //   328	333	500	finally
    //   339	346	500	finally
    //   350	355	500	finally
    //   355	360	500	finally
    //   366	374	500	finally
    //   385	392	500	finally
    //   394	399	500	finally
    //   403	409	500	finally
    //   414	421	500	finally
    //   437	442	500	finally
    //   444	451	500	finally
    //   507	512	500	finally
    //   134	141	505	java/lang/Exception
    //   159	165	505	java/lang/Exception
    //   167	172	505	java/lang/Exception
    //   179	182	505	java/lang/Exception
    //   186	191	505	java/lang/Exception
    //   193	200	505	java/lang/Exception
    //   202	207	505	java/lang/Exception
    //   211	217	505	java/lang/Exception
    //   219	226	505	java/lang/Exception
    //   230	235	505	java/lang/Exception
    //   237	244	505	java/lang/Exception
    //   248	253	505	java/lang/Exception
    //   255	262	505	java/lang/Exception
    //   266	271	505	java/lang/Exception
    //   277	284	505	java/lang/Exception
    //   288	293	505	java/lang/Exception
    //   295	302	505	java/lang/Exception
    //   306	311	505	java/lang/Exception
    //   317	324	505	java/lang/Exception
    //   328	333	505	java/lang/Exception
    //   339	346	505	java/lang/Exception
    //   350	355	505	java/lang/Exception
    //   355	360	505	java/lang/Exception
    //   366	374	505	java/lang/Exception
    //   385	392	505	java/lang/Exception
    //   394	399	505	java/lang/Exception
    //   403	409	505	java/lang/Exception
    //   414	421	505	java/lang/Exception
    //   437	442	505	java/lang/Exception
    //   444	451	505	java/lang/Exception
  }
  
  public final List a()
  {
    Object localObject1 = a.getWritableDatabase();
    String str1 = "campaigns";
    String[] arrayOfString = null;
    int i = 0;
    HashMap localHashMap = null;
    int j = 0;
    String str2 = null;
    String str3 = null;
    String str4 = null;
    Cursor localCursor = ((SQLiteDatabase)localObject1).query(str1, null, null, null, null, null, null);
    if (localCursor == null) {
      return Collections.emptyList();
    }
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    try
    {
      int k = localCursor.getColumnCount();
      arrayOfString = new String[k];
      i = 0;
      localHashMap = null;
      while (i < k)
      {
        str2 = localCursor.getColumnName(i);
        arrayOfString[i] = str2;
        i += 1;
      }
      for (;;)
      {
        boolean bool = localCursor.moveToNext();
        if (!bool) {
          break;
        }
        localHashMap = new java/util/HashMap;
        localHashMap.<init>(k);
        j = 0;
        str2 = null;
        while (j < k)
        {
          str3 = arrayOfString[j];
          str4 = localCursor.getString(j);
          localHashMap.put(str3, str4);
          j += 1;
        }
        ((List)localObject1).add(localHashMap);
      }
      return (List)localObject1;
    }
    finally
    {
      localCursor.close();
    }
  }
  
  public final void a(String paramString1, String paramString2, long paramLong, String[] paramArrayOfString, b.a.a[] paramArrayOfa)
  {
    SQLiteDatabase localSQLiteDatabase = a.getWritableDatabase();
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    try
    {
      localSQLiteDatabase.beginTransaction();
      String str = "campaigns";
      Object localObject1 = "number=? AND placement=?";
      int i = 2;
      Object localObject2 = new String[i];
      localObject2[0] = paramString1;
      int j = 1;
      localObject2[j] = paramString2;
      localSQLiteDatabase.delete(str, (String)localObject1, (String[])localObject2);
      str = "number";
      localContentValues.put(str, paramString1);
      paramString1 = "placement";
      localContentValues.put(paramString1, paramString2);
      paramString1 = "expiration";
      long l1 = System.currentTimeMillis();
      long l2 = 1000L;
      paramLong *= l2;
      l1 += paramLong;
      paramString2 = Long.valueOf(l1);
      localContentValues.put(paramString1, paramString2);
      int k = paramArrayOfa.length;
      paramString2 = null;
      paramLong = 1L;
      if (k > 0)
      {
        k = paramArrayOfa.length;
        int m = 0;
        str = null;
        while (m < k)
        {
          localObject1 = paramArrayOfa[m];
          localObject2 = "campaign_id";
          Object localObject3 = a;
          localContentValues.put((String)localObject2, (String)localObject3);
          localObject2 = "start";
          j = c;
          localObject3 = Integer.valueOf(j);
          localContentValues.put((String)localObject2, (Integer)localObject3);
          localObject2 = "end";
          j = d;
          if (j != 0)
          {
            j = d;
            l2 = j;
          }
          else
          {
            localObject3 = TimeUnit.DAYS;
            l2 = ((TimeUnit)localObject3).toMinutes(paramLong);
          }
          localObject3 = Long.valueOf(l2);
          localContentValues.put((String)localObject2, (Long)localObject3);
          localObject2 = "request_order";
          localObject3 = b;
          localObject3 = a((String[])localObject3);
          localContentValues.put((String)localObject2, (String)localObject3);
          localObject2 = e;
          if (localObject2 != null)
          {
            localObject2 = "mainColor";
            localObject3 = e;
            localObject3 = a;
            localContentValues.put((String)localObject2, (String)localObject3);
            localObject2 = "lightColor";
            localObject3 = e;
            localObject3 = b;
            localContentValues.put((String)localObject2, (String)localObject3);
            localObject2 = "buttonColor";
            localObject3 = e;
            localObject3 = c;
            localContentValues.put((String)localObject2, (String)localObject3);
            localObject2 = "imageUrl";
            localObject3 = e;
            localObject3 = d;
            localContentValues.put((String)localObject2, (String)localObject3);
            localObject2 = "bannerBackgroundColor";
            localObject3 = e;
            localObject3 = e;
            localContentValues.put((String)localObject2, (String)localObject3);
            localObject2 = "ctaBackgroundColor";
            localObject3 = e;
            localObject3 = f;
            localContentValues.put((String)localObject2, (String)localObject3);
            localObject2 = "ctaTextColor";
            localObject1 = e;
            localObject1 = g;
            localContentValues.put((String)localObject2, (String)localObject1);
          }
          else
          {
            localObject1 = "mainColor";
            localObject2 = "";
            localContentValues.put((String)localObject1, (String)localObject2);
            localObject1 = "lightColor";
            localObject2 = "";
            localContentValues.put((String)localObject1, (String)localObject2);
            localObject1 = "buttonColor";
            localObject2 = "";
            localContentValues.put((String)localObject1, (String)localObject2);
            localObject1 = "imageUrl";
            localObject2 = "";
            localContentValues.put((String)localObject1, (String)localObject2);
            localObject1 = "bannerBackgroundColor";
            localObject2 = "";
            localContentValues.put((String)localObject1, (String)localObject2);
            localObject1 = "ctaBackgroundColor";
            localObject2 = "";
            localContentValues.put((String)localObject1, (String)localObject2);
            localObject1 = "ctaTextColor";
            localObject2 = "";
            localContentValues.put((String)localObject1, (String)localObject2);
          }
          localObject1 = "campaigns";
          localSQLiteDatabase.insert((String)localObject1, null, localContentValues);
          m += 1;
        }
      }
      paramString1 = "campaign_id";
      paramArrayOfa = "";
      localContentValues.put(paramString1, paramArrayOfa);
      paramString1 = "request_order";
      paramArrayOfString = a(paramArrayOfString);
      localContentValues.put(paramString1, paramArrayOfString);
      paramString1 = "start";
      paramArrayOfString = Integer.valueOf(0);
      localContentValues.put(paramString1, paramArrayOfString);
      paramString1 = "end";
      paramArrayOfString = TimeUnit.DAYS;
      paramLong = paramArrayOfString.toMinutes(paramLong);
      Object localObject4 = Long.valueOf(paramLong);
      localContentValues.put(paramString1, (Long)localObject4);
      paramString1 = "mainColor";
      localObject4 = "";
      localContentValues.put(paramString1, (String)localObject4);
      paramString1 = "lightColor";
      localObject4 = "";
      localContentValues.put(paramString1, (String)localObject4);
      paramString1 = "buttonColor";
      localObject4 = "";
      localContentValues.put(paramString1, (String)localObject4);
      paramString1 = "imageUrl";
      localObject4 = "";
      localContentValues.put(paramString1, (String)localObject4);
      paramString1 = "bannerBackgroundColor";
      localObject4 = "";
      localContentValues.put(paramString1, (String)localObject4);
      paramString1 = "ctaBackgroundColor";
      localObject4 = "";
      localContentValues.put(paramString1, (String)localObject4);
      paramString1 = "ctaTextColor";
      localObject4 = "";
      localContentValues.put(paramString1, (String)localObject4);
      paramString1 = "campaigns";
      localSQLiteDatabase.insert(paramString1, null, localContentValues);
      localSQLiteDatabase.setTransactionSuccessful();
      return;
    }
    finally
    {
      localSQLiteDatabase.endTransaction();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.campaigns.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */