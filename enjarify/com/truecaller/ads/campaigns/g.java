package com.truecaller.ads.campaigns;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.truecaller.log.AssertionUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

final class g
  implements e
{
  final a a;
  private final Context b;
  private Executor c;
  private final List d;
  private volatile String e;
  
  g(Context paramContext)
  {
    Object localObject = new com/truecaller/ads/campaigns/h;
    ((h)localObject).<init>();
    localObject = Executors.newCachedThreadPool((ThreadFactory)localObject);
    c = ((Executor)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    d = ((List)localObject);
    localObject = paramContext.getApplicationContext();
    b = ((Context)localObject);
    localObject = new com/truecaller/ads/campaigns/a;
    ((a)localObject).<init>(paramContext);
    a = ((a)localObject);
  }
  
  private void a(String paramString, b.a parama)
  {
    Object localObject = a;
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject);
    if (!bool1)
    {
      long l1 = c;
      long l2 = 0L;
      boolean bool2 = l1 < l2;
      if (bool2)
      {
        localObject = b;
        if (localObject != null)
        {
          a locala = a;
          String str = a.toUpperCase();
          long l3 = c;
          String[] arrayOfString = d;
          b.a.a[] arrayOfa = b;
          locala.a(paramString, str, l3, arrayOfString, arrayOfa);
          return;
        }
      }
    }
  }
  
  public final String a()
  {
    String str1 = e;
    if (str1 == null) {
      try
      {
        try
        {
          str1 = e;
          if (str1 == null)
          {
            Object localObject2 = b;
            localObject2 = AdvertisingIdClient.getAdvertisingIdInfo((Context)localObject2);
            if (localObject2 != null)
            {
              boolean bool1 = ((AdvertisingIdClient.Info)localObject2).isLimitAdTrackingEnabled();
              if (!bool1) {
                str1 = ((AdvertisingIdClient.Info)localObject2).getId();
              }
            }
            boolean bool2 = TextUtils.isEmpty(str1);
            if (bool2) {
              str1 = "";
            }
            e = str1;
          }
        }
        finally {}
        String str2;
        return str2;
      }
      catch (Exception localException)
      {
        AssertionUtil.reportThrowableButNeverCrash(localException);
        str2 = "";
      }
    }
  }
  
  public final String a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 1: 
      return "SEARCHRESULTS,HISTORY,DETAILS";
    }
    return "CALLERID,AFTERCALL,DETAILS";
  }
  
  public final Future a(String paramString1, int paramInt, String[] paramArrayOfString, Integer paramInteger, String paramString2, String paramString3, List paramList)
  {
    g localg = this;
    String str = paramString1;
    Object localObject2 = paramArrayOfString;
    boolean bool1 = true;
    int i = 0;
    Object localObject3 = null;
    boolean bool2;
    if (paramString1 != null)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      arrayOfString = null;
    }
    Object localObject4 = { "Key can not be null" };
    AssertionUtil.isTrue(bool2, (String[])localObject4);
    if (localObject2 != null)
    {
      j = localObject2.length;
      if (j > 0) {}
    }
    else
    {
      bool1 = false;
      localObject5 = null;
    }
    String[] arrayOfString = { "Placements array can not be null or empty" };
    AssertionUtil.isTrue(bool1, arrayOfString);
    Object localObject5 = d;
    int j = 0;
    arrayOfString = null;
    try
    {
      for (;;)
      {
        localObject4 = d;
        int k = ((List)localObject4).size();
        if (j >= k) {
          break;
        }
        localObject4 = d;
        localObject4 = ((List)localObject4).get(j);
        localObject4 = (g.a)localObject4;
        int m = 0;
        for (;;)
        {
          int n = localObject2.length;
          if (m >= n) {
            break;
          }
          localObject6 = g.a.a((g.a)localObject4);
          localObject7 = localObject2[m];
          boolean bool3 = ((Set)localObject6).contains(localObject7);
          if (!bool3) {
            break;
          }
          m += 1;
        }
        int i1 = localObject2.length;
        if (m == i1) {
          return (Future)localObject4;
        }
        j += 1;
      }
      g.a locala = new com/truecaller/ads/campaigns/g$a;
      i = localObject2.length;
      locala.<init>(str, i);
      localObject3 = new java/util/HashSet;
      ((HashSet)localObject3).<init>();
      g.a.a(locala, (Set)localObject3);
      localObject3 = g.a.a(locala);
      Collections.addAll((Collection)localObject3, (Object[])localObject2);
      localObject3 = d;
      ((List)localObject3).add(locala);
      localObject4 = new java/util/HashSet;
      ((HashSet)localObject4).<init>();
      Collections.addAll(g.a.a(locala), (Object[])localObject2);
      Collections.addAll((Collection)localObject4, (Object[])localObject2);
      Executor localExecutor = c;
      g.b localb = new com/truecaller/ads/campaigns/g$b;
      localObject2 = localb;
      localObject5 = this;
      localObject3 = paramString1;
      j = paramInt;
      Object localObject6 = paramString2;
      Object localObject7 = paramString3;
      localb.<init>(this, paramString1, paramInt, (Set)localObject4, paramInteger, paramString2, paramString3, paramList, (byte)0);
      localExecutor.execute(localb);
      return locala;
    }
    finally {}
  }
  
  public final void a(String paramString, b paramb)
  {
    Object localObject = a;
    if (localObject == null) {
      return;
    }
    paramb = a.iterator();
    for (;;)
    {
      boolean bool = paramb.hasNext();
      if (!bool) {
        break;
      }
      localObject = (b.a)paramb.next();
      if (localObject != null) {
        a(paramString, (b.a)localObject);
      }
    }
  }
  
  public final void b()
  {
    a.a.getWritableDatabase().delete("campaigns", null, null);
  }
  
  public final List c()
  {
    return a.a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.campaigns.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */