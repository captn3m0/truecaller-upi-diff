package com.truecaller.ads.campaigns;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public final class AdCampaigns
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private static final String[] c = { "unified" };
  private static final String[] d = { "unified", "banner-320x100" };
  public final String a;
  public final AdCampaign[] b;
  private final String[] e;
  private volatile String[] f = null;
  
  static
  {
    AdCampaigns.1 local1 = new com/truecaller/ads/campaigns/AdCampaigns$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  private AdCampaigns(Parcel paramParcel)
  {
    Object localObject = paramParcel.readString();
    a = ((String)localObject);
    localObject = AdCampaign.CREATOR;
    localObject = (AdCampaign[])paramParcel.createTypedArray((Parcelable.Creator)localObject);
    b = ((AdCampaign[])localObject);
    paramParcel = paramParcel.createStringArray();
    e = paramParcel;
  }
  
  private AdCampaigns(String paramString, AdCampaign[] paramArrayOfAdCampaign, String[] paramArrayOfString)
  {
    a = paramString;
    b = paramArrayOfAdCampaign;
    e = paramArrayOfString;
  }
  
  public final String[] a()
  {
    String[] arrayOfString = f;
    if (arrayOfString != null) {
      return arrayOfString;
    }
    int i = b.length;
    arrayOfString = new String[i];
    int j = 0;
    for (;;)
    {
      Object localObject1 = b;
      int k = localObject1.length;
      if (j >= k) {
        break;
      }
      Object localObject2 = localObject1[j];
      if (localObject2 != null)
      {
        localObject1 = a;
        arrayOfString[j] = localObject1;
      }
      j += 1;
    }
    f = arrayOfString;
    return arrayOfString;
  }
  
  public final AdCampaign b()
  {
    AdCampaign[] arrayOfAdCampaign = b;
    int i = arrayOfAdCampaign.length;
    int j = 0;
    while (j < i)
    {
      AdCampaign localAdCampaign = arrayOfAdCampaign[j];
      AdCampaign.Style localStyle = b;
      if (localStyle != null) {
        return localAdCampaign;
      }
      j += 1;
    }
    return null;
  }
  
  public final AdCampaign c()
  {
    AdCampaign[] arrayOfAdCampaign = b;
    int i = arrayOfAdCampaign.length;
    int j = 0;
    while (j < i)
    {
      AdCampaign localAdCampaign = arrayOfAdCampaign[j];
      AdCampaign.CtaStyle localCtaStyle = c;
      if (localCtaStyle != null) {
        return localAdCampaign;
      }
      j += 1;
    }
    return null;
  }
  
  public final String[] d()
  {
    AdCampaign localAdCampaign = b();
    if (localAdCampaign != null)
    {
      arrayOfString = d;
      int i = arrayOfString.length;
      if (i > 0) {
        return d;
      }
    }
    String[] arrayOfString = e;
    int j = arrayOfString.length;
    if (j > 0) {
      return arrayOfString;
    }
    if (localAdCampaign == null) {
      return c;
    }
    return d;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    Object localObject = a;
    paramParcel.writeString((String)localObject);
    localObject = b;
    paramParcel.writeTypedArray((Parcelable[])localObject, 0);
    localObject = e;
    paramParcel.writeStringArray((String[])localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.campaigns.AdCampaigns
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */