package com.truecaller.ads.campaigns;

import android.content.Context;

public final class f
{
  private static e a;
  
  public static e a(Context paramContext)
  {
    synchronized (f.class)
    {
      Object localObject = a;
      if (localObject == null)
      {
        localObject = new com/truecaller/ads/campaigns/g;
        ((g)localObject).<init>(paramContext);
        a = (e)localObject;
      }
      paramContext = a;
      return paramContext;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.campaigns.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */