package com.truecaller.ads.campaigns;

import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

final class g$a
  implements Future
{
  private final String a;
  private final AdCampaigns[] b;
  private int c = 0;
  private volatile AdCampaigns[] d = null;
  private final Lock e;
  private final Condition f;
  private Set g;
  
  g$a(String paramString, int paramInt)
  {
    Object localObject = new java/util/concurrent/locks/ReentrantLock;
    ((ReentrantLock)localObject).<init>();
    e = ((Lock)localObject);
    localObject = e.newCondition();
    f = ((Condition)localObject);
    a = paramString;
    paramString = new AdCampaigns[paramInt];
    b = paramString;
  }
  
  private boolean a(AdCampaigns paramAdCampaigns)
  {
    Object localObject = e;
    ((Lock)localObject).lock();
    try
    {
      localObject = b;
      int i = c;
      localObject[i] = paramAdCampaigns;
      int j = c;
      int k = 1;
      j += k;
      c = j;
      AdCampaigns[] arrayOfAdCampaigns = b;
      i = arrayOfAdCampaigns.length;
      if (j == i)
      {
        paramAdCampaigns = b;
        d = paramAdCampaigns;
        paramAdCampaigns = f;
        paramAdCampaigns.signalAll();
      }
      else
      {
        k = 0;
        localObject = null;
      }
      return k;
    }
    finally
    {
      e.unlock();
    }
  }
  
  private AdCampaigns[] a()
  {
    Object localObject = d;
    if (localObject == null)
    {
      localObject = e;
      ((Lock)localObject).lock();
      try
      {
        localObject = d;
        if (localObject == null)
        {
          localObject = f;
          ((Condition)localObject).await();
          localObject = d;
        }
        Lock localLock = e;
        localLock.unlock();
      }
      finally
      {
        e.unlock();
      }
    }
    return arrayOfAdCampaigns;
  }
  
  private AdCampaigns[] a(long paramLong, TimeUnit paramTimeUnit)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = e;
      ((Lock)localObject1).lock();
      try
      {
        localObject1 = d;
        if (localObject1 == null)
        {
          localObject1 = f;
          boolean bool = ((Condition)localObject1).await(paramLong, paramTimeUnit);
          if (bool)
          {
            localObject2 = d;
            localObject1 = localObject2;
          }
          else
          {
            localObject2 = new java/util/concurrent/TimeoutException;
            ((TimeoutException)localObject2).<init>();
            throw ((Throwable)localObject2);
          }
        }
        Object localObject2 = e;
        ((Lock)localObject2).unlock();
      }
      finally
      {
        e.unlock();
      }
    }
    return (AdCampaigns[])localObject1;
  }
  
  public final boolean cancel(boolean paramBoolean)
  {
    return false;
  }
  
  public final boolean isCancelled()
  {
    return false;
  }
  
  public final boolean isDone()
  {
    AdCampaigns[] arrayOfAdCampaigns = d;
    return arrayOfAdCampaigns != null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.campaigns.g.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */