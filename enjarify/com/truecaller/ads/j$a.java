package com.truecaller.ads;

import com.truecaller.log.AssertionUtil;
import com.truecaller.log.UnmutedException.AdsIllegalStateException;
import com.truecaller.log.UnmutedException.AdsIllegalStateException.Cause;
import java.util.List;

public final class j$a
{
  public String a = "";
  public int b = 0;
  final String c;
  public Integer d = null;
  public String e = null;
  public String f = null;
  public List g = null;
  
  public j$a(String paramString)
  {
    c = paramString;
  }
  
  public final j a()
  {
    Object localObject = a;
    UnmutedException.AdsIllegalStateException.Cause localCause;
    if (localObject == null)
    {
      localObject = new com/truecaller/log/UnmutedException$AdsIllegalStateException;
      localCause = UnmutedException.AdsIllegalStateException.Cause.CAMPAIGN_CONFIG_NULL_KEY;
      ((UnmutedException.AdsIllegalStateException)localObject).<init>(localCause);
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject);
    }
    localObject = c;
    if (localObject == null)
    {
      localObject = new com/truecaller/log/UnmutedException$AdsIllegalStateException;
      localCause = UnmutedException.AdsIllegalStateException.Cause.CAMPAIGN_CONFIG_NULL_PLACEMENT;
      ((UnmutedException.AdsIllegalStateException)localObject).<init>(localCause);
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject);
    }
    localObject = new com/truecaller/ads/j;
    ((j)localObject).<init>(this, (byte)0);
    return (j)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.j.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */