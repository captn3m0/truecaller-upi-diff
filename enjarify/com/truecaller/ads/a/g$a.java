package com.truecaller.ads.a;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.truecaller.ui.q.a;

final class g$a
  extends RecyclerView.ViewHolder
  implements q.a
{
  final RecyclerView.ViewHolder a;
  private String c;
  private boolean d;
  
  g$a(g paramg, RecyclerView.ViewHolder paramViewHolder, ViewGroup paramViewGroup)
  {
    super(paramViewGroup);
    a = paramViewHolder;
  }
  
  public final String a()
  {
    return c;
  }
  
  public final boolean b()
  {
    return d;
  }
  
  public final void c_(String paramString)
  {
    c = paramString;
  }
  
  public final void c_(boolean paramBoolean)
  {
    d = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.a.g.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */