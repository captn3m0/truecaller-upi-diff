package com.truecaller.ads.a;

import android.support.v4.f.o;
import c.g.a.m;
import com.truecaller.ads.g;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.bs;

public final class f
  implements e, g, ag
{
  final o a;
  private final bn b;
  private final ArrayList c;
  private final o d;
  private boolean e;
  private bn f;
  private final com.truecaller.ads.provider.f g;
  private final com.truecaller.ads.k h;
  private final c.d.f i;
  
  public f(com.truecaller.ads.provider.f paramf, com.truecaller.ads.k paramk, c.d.f paramf1)
  {
    g = paramf;
    h = paramk;
    i = paramf1;
    paramf = bs.a(null);
    b = paramf;
    paramf = new java/util/ArrayList;
    paramf.<init>();
    c = paramf;
    paramf = new android/support/v4/f/o;
    paramf.<init>();
    a = paramf;
    paramf = new android/support/v4/f/o;
    paramf.<init>();
    d = paramf;
    paramf = g;
    paramk = h;
    paramf1 = this;
    paramf1 = (g)this;
    paramf.a(paramk, paramf1);
  }
  
  public final c.d.f V_()
  {
    c.d.f localf1 = i;
    c.d.f localf2 = (c.d.f)b;
    return localf1.plus(localf2);
  }
  
  public final void a()
  {
    Iterator localIterator = ((Iterable)c).iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      g localg = (g)localIterator.next();
      localg.a();
    }
  }
  
  public final void a(int paramInt)
  {
    Iterator localIterator = ((Iterable)c).iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      g localg = (g)localIterator.next();
      localg.a(paramInt);
    }
  }
  
  public final void a(long paramLong)
  {
    Object localObject = new com/truecaller/ads/a/f$a;
    ((f.a)localObject).<init>(this, paramLong, null);
    localObject = (m)localObject;
    bn localbn = kotlinx.coroutines.e.b(this, null, (m)localObject, 3);
    f = localbn;
  }
  
  public final void a(g paramg)
  {
    c.g.b.k.b(paramg, "listener");
    c.add(paramg);
    com.truecaller.ads.provider.f localf = g;
    com.truecaller.ads.k localk = h;
    boolean bool = localf.a(localk);
    if (bool)
    {
      bool = e;
      if (!bool) {
        paramg.a();
      }
    }
  }
  
  public final void a(com.truecaller.ads.provider.holders.e parame, int paramInt)
  {
    c.g.b.k.b(parame, "ad");
    Iterator localIterator = ((Iterable)c).iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      g localg = (g)localIterator.next();
      localg.a(parame, paramInt);
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    boolean bool1 = e;
    if ((bool1 != paramBoolean) && (!paramBoolean))
    {
      Object localObject1 = g;
      Object localObject2 = h;
      bool1 = ((com.truecaller.ads.provider.f)localObject1).a((com.truecaller.ads.k)localObject2);
      if (bool1)
      {
        localObject1 = ((Iterable)c).iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          localObject2 = (g)((Iterator)localObject1).next();
          ((g)localObject2).a();
        }
      }
    }
    e = paramBoolean;
  }
  
  public final com.truecaller.ads.provider.holders.e b(int paramInt)
  {
    Object localObject1 = (com.truecaller.ads.provider.holders.e)a.a(paramInt);
    if (localObject1 != null) {
      return (com.truecaller.ads.provider.holders.e)localObject1;
    }
    boolean bool = e;
    if (!bool)
    {
      localObject1 = g;
      Object localObject2 = h;
      localObject1 = ((com.truecaller.ads.provider.f)localObject1).a((com.truecaller.ads.k)localObject2, paramInt);
      if (localObject1 != null)
      {
        a.b(paramInt, localObject1);
        localObject2 = (com.truecaller.ads.provider.holders.e)d.a(paramInt);
        if (localObject2 != null) {
          ((com.truecaller.ads.provider.holders.e)localObject2).d();
        }
        d.b(paramInt, localObject1);
        return (com.truecaller.ads.provider.holders.e)localObject1;
      }
    }
    return (com.truecaller.ads.provider.holders.e)d.a(paramInt);
  }
  
  public final void b()
  {
    a.d();
  }
  
  public final void b(g paramg)
  {
    c.g.b.k.b(paramg, "listener");
    c.remove(paramg);
  }
  
  public final void d()
  {
    b.n();
    Object localObject1 = g;
    com.truecaller.ads.k localk = h;
    Object localObject2 = this;
    localObject2 = (g)this;
    ((com.truecaller.ads.provider.f)localObject1).b(localk, (g)localObject2);
    localObject1 = d;
    int j = ((o)localObject1).c();
    int k = 0;
    localk = null;
    while (k < j)
    {
      localObject2 = (com.truecaller.ads.provider.holders.e)d.d(k);
      ((com.truecaller.ads.provider.holders.e)localObject2).d();
      k += 1;
    }
    d.d();
  }
  
  public final void e()
  {
    bn localbn = f;
    if (localbn != null)
    {
      boolean bool = localbn.av_();
      if (bool)
      {
        Object localObject = new java/util/concurrent/CancellationException;
        String str = "View restored";
        ((CancellationException)localObject).<init>(str);
        localObject = (Throwable)localObject;
        localbn.c((Throwable)localObject);
      }
      return;
    }
  }
  
  public final boolean f()
  {
    Object localObject = g;
    boolean bool = ((com.truecaller.ads.provider.f)localObject).a();
    if (bool)
    {
      localObject = g;
      bool = ((com.truecaller.ads.provider.f)localObject).b();
      if (bool)
      {
        localObject = h;
        bool = k;
        if (bool) {
          return true;
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */