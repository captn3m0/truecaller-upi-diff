package com.truecaller.ads.a;

import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;
import java.util.List;

public final class g
  extends a
{
  final int b;
  final int c;
  
  public g(int paramInt, RecyclerView.Adapter paramAdapter, com.truecaller.ads.a parama, b paramb, e parame)
  {
    super(paramAdapter, parama, paramb, parame);
    b = paramInt;
    c = 2131362562;
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    boolean bool = paramViewHolder instanceof g.a;
    if (bool)
    {
      paramViewHolder = a;
      super.onBindViewHolder(paramViewHolder, paramInt);
      return;
    }
    super.onBindViewHolder(paramViewHolder, paramInt);
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt, List paramList)
  {
    boolean bool = paramViewHolder instanceof g.a;
    if (bool)
    {
      paramViewHolder = a;
      super.onBindViewHolder(paramViewHolder, paramInt, paramList);
      return;
    }
    super.onBindViewHolder(paramViewHolder, paramInt, paramList);
  }
  
  public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    RecyclerView.ViewHolder localViewHolder = super.onCreateViewHolder(paramViewGroup, paramInt);
    int i = 2131365502;
    if (paramInt != i) {
      switch (paramInt)
      {
      default: 
        return localViewHolder;
      }
    }
    g.a locala = new com/truecaller/ads/a/g$a;
    locala.<init>(this, localViewHolder, paramViewGroup);
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */