package com.truecaller.ads.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import c.g.b.k;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeAppInstallAdView;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeContentAdView;
import com.truecaller.ads.campaigns.AdCampaign.CtaStyle;
import com.truecaller.ads.provider.holders.AdHolderType;
import com.truecaller.ads.provider.holders.AdNativeHolder;
import com.truecaller.ads.provider.holders.AdNativeHolder.Type;
import com.truecaller.ads.provider.holders.i;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import java.util.List;

public class a
  extends com.truecaller.ui.b
  implements com.truecaller.ads.g
{
  public b a;
  private final com.truecaller.ads.a b;
  private e c;
  
  public a(RecyclerView.Adapter paramAdapter, com.truecaller.ads.a parama, b paramb, e parame)
  {
    super(paramAdapter);
    b = parama;
    if (paramb == null)
    {
      paramb = new com/truecaller/ads/a/c;
      paramb.<init>();
    }
    a = paramb;
    if (parame == null)
    {
      parame = new com/truecaller/ads/a/h;
      parame.<init>();
    }
    c = parame;
  }
  
  public final void a()
  {
    notifyDataSetChanged();
  }
  
  public final void a(int paramInt) {}
  
  public final void a(b paramb)
  {
    a = paramb;
    notifyDataSetChanged();
  }
  
  public final void a(com.truecaller.ads.provider.holders.e parame, int paramInt) {}
  
  public final int b(int paramInt)
  {
    return a.c(paramInt);
  }
  
  public final int c(int paramInt)
  {
    return a.a(paramInt);
  }
  
  public final boolean d(int paramInt)
  {
    int i = 2131365482;
    if (paramInt != i)
    {
      i = 2131365489;
      if (paramInt != i)
      {
        i = 2131365502;
        if (paramInt != i) {
          switch (paramInt)
          {
          default: 
            return false;
          }
        }
      }
    }
    return true;
  }
  
  public int getItemCount()
  {
    b localb = a;
    int i = super.getItemCount();
    return localb.b(i);
  }
  
  public long getItemId(int paramInt)
  {
    boolean bool = hasStableIds();
    if (!bool) {
      return -1;
    }
    b localb = a;
    bool = localb.e(paramInt);
    if (bool)
    {
      paramInt = a.d(paramInt);
      return -1000000 - paramInt;
    }
    return super.getItemId(paramInt);
  }
  
  public int getItemViewType(int paramInt)
  {
    Object localObject1 = a;
    boolean bool1 = ((b)localObject1).e(paramInt);
    if (bool1)
    {
      localObject1 = c;
      Object localObject2 = a;
      paramInt = ((b)localObject2).d(paramInt);
      Object localObject3 = ((e)localObject1).b(paramInt);
      if (localObject3 == null)
      {
        localObject3 = c;
        paramInt = ((e)localObject3).f();
        if (paramInt != 0) {
          return 2131365502;
        }
        return 2131365500;
      }
      localObject1 = AdHolderType.NATIVE_AD;
      localObject2 = ((com.truecaller.ads.provider.holders.e)localObject3).a();
      if (localObject1 == localObject2)
      {
        localObject1 = a.1.a;
        localObject2 = localObject3;
        localObject2 = ((AdNativeHolder)localObject3).i();
        int j = ((AdNativeHolder.Type)localObject2).ordinal();
        int i = localObject1[j];
        switch (i)
        {
        default: 
          break;
        case 2: 
          return 2131365497;
        case 1: 
          return 2131365498;
        }
      }
      else
      {
        localObject1 = AdHolderType.PUBLISHER_VIEW;
        localObject2 = ((com.truecaller.ads.provider.holders.e)localObject3).a();
        if (localObject1 == localObject2) {
          return 2131365482;
        }
        localObject1 = AdHolderType.HOUSE_AD;
        localObject2 = ((com.truecaller.ads.provider.holders.e)localObject3).a();
        if (localObject1 == localObject2) {
          return 2131365489;
        }
        boolean bool2 = com.truecaller.ads.c.a.a((com.truecaller.ads.provider.holders.e)localObject3);
        if (bool2) {
          return 2131365499;
        }
      }
      localObject1 = new java/lang/IllegalStateException;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Ad type ");
      localObject3 = ((com.truecaller.ads.provider.holders.e)localObject3).b();
      ((StringBuilder)localObject2).append((String)localObject3);
      ((StringBuilder)localObject2).append(" not supported");
      localObject3 = ((StringBuilder)localObject2).toString();
      ((IllegalStateException)localObject1).<init>((String)localObject3);
      throw ((Throwable)localObject1);
    }
    return super.getItemViewType(paramInt);
  }
  
  public void onAttachedToRecyclerView(RecyclerView paramRecyclerView)
  {
    super.onAttachedToRecyclerView(paramRecyclerView);
    c.a(this);
  }
  
  public void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    int i = getItemViewType(paramInt);
    int j = 2131365482;
    if (i != j)
    {
      j = 2131365489;
      if (i != j)
      {
        j = 2131365502;
        if (i != j) {
          switch (i)
          {
          default: 
            super.onBindViewHolder(paramViewHolder, paramInt);
            return;
          case 2131365499: 
            localObject1 = c;
            localb = a;
            paramInt = localb.d(paramInt);
            localObject2 = (com.truecaller.ads.provider.holders.d)((e)localObject1).b(paramInt);
            if (localObject2 == null)
            {
              AssertionUtil.OnlyInDebug.fail(new String[] { "Prefetcher returned null for existing ad" });
              return;
            }
            localObject1 = com.truecaller.ads.c.b.a((com.truecaller.ads.provider.holders.d)localObject2);
            paramViewHolder = (com.truecaller.ads.c.c)itemView;
            localObject2 = a.d;
            com.truecaller.ads.b.a(paramViewHolder, (com.truecaller.ads.c.a)localObject1, (AdCampaign.CtaStyle)localObject2);
            return;
          case 2131365498: 
            localObject1 = c;
            localb = a;
            paramInt = localb.d(paramInt);
            localObject2 = (com.truecaller.ads.provider.holders.c)((e)localObject1).b(paramInt);
            if (localObject2 == null)
            {
              AssertionUtil.OnlyInDebug.fail(new String[] { "Prefetcher returned null for existing ad" });
              return;
            }
            localObject1 = (NativeContentAd)((com.truecaller.ads.provider.holders.c)localObject2).k();
            paramViewHolder = (NativeContentAdView)itemView;
            localObject2 = a.d;
            com.truecaller.ads.b.a(paramViewHolder, (NativeContentAd)localObject1, (AdCampaign.CtaStyle)localObject2);
            return;
          case 2131365497: 
            localObject1 = c;
            localb = a;
            paramInt = localb.d(paramInt);
            localObject2 = (i)((e)localObject1).b(paramInt);
            if (localObject2 == null)
            {
              AssertionUtil.OnlyInDebug.fail(new String[] { "Prefetcher returned null for existing ad" });
              return;
            }
            localObject1 = (NativeAppInstallAd)((i)localObject2).k();
            paramViewHolder = (NativeAppInstallAdView)itemView;
            localObject2 = a.d;
            com.truecaller.ads.b.a(paramViewHolder, (NativeAppInstallAd)localObject1, (AdCampaign.CtaStyle)localObject2);
            return;
          }
        }
        return;
      }
      localObject1 = c;
      localb = a;
      paramInt = localb.d(paramInt);
      localObject2 = (com.truecaller.ads.provider.holders.g)((e)localObject1).b(paramInt);
      if (localObject2 == null)
      {
        AssertionUtil.OnlyInDebug.fail(new String[] { "Prefetcher returned null for existing ad" });
        return;
      }
      localObject1 = (com.truecaller.ads.provider.b.e)((com.truecaller.ads.provider.holders.g)localObject2).g();
      paramViewHolder = (com.truecaller.ads.ui.c)itemView;
      localObject2 = a.d;
      com.truecaller.ads.d.a(paramViewHolder, (com.truecaller.ads.provider.b.e)localObject1, (AdCampaign.CtaStyle)localObject2);
      return;
    }
    paramViewHolder = (ViewGroup)itemView;
    Object localObject1 = c;
    b localb = a;
    paramInt = localb.d(paramInt);
    Object localObject2 = (com.truecaller.ads.provider.holders.a)((e)localObject1).b(paramInt);
    if (localObject2 == null)
    {
      AssertionUtil.OnlyInDebug.fail(new String[] { "Prefetcher returned null for existing ad" });
      return;
    }
    localObject2 = (PublisherAdView)((com.truecaller.ads.provider.holders.a)localObject2).g();
    paramViewHolder.removeAllViews();
    localObject1 = ((PublisherAdView)localObject2).getParent();
    boolean bool = localObject1 instanceof ViewGroup;
    if (bool)
    {
      localObject1 = (ViewGroup)localObject1;
      ((ViewGroup)localObject1).removeView((View)localObject2);
    }
    paramViewHolder.addView((View)localObject2);
  }
  
  public void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt, List paramList)
  {
    int i = getItemViewType(paramInt);
    boolean bool = d(i);
    if (bool)
    {
      onBindViewHolder(paramViewHolder, paramInt);
      return;
    }
    super.onBindViewHolder(paramViewHolder, paramInt, paramList);
  }
  
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    Object localObject1 = paramViewGroup.getContext();
    int i = 2131365482;
    if (paramInt != i)
    {
      i = 2131365489;
      if (paramInt != i)
      {
        i = 2131365502;
        if (paramInt != i)
        {
          switch (paramInt)
          {
          default: 
            return super.onCreateViewHolder(paramViewGroup, paramInt);
          case 2131365500: 
            localObject2 = new com/truecaller/ads/a/a$a;
            locala = b;
            k.b(localObject1, "context");
            k.b(locala, "adType");
            localObject1 = LayoutInflater.from((Context)localObject1);
            i = locala.getEmptyLayout();
            paramViewGroup = ((LayoutInflater)localObject1).inflate(i, paramViewGroup, false);
            k.a(paramViewGroup, "LayoutInflater.from(cont…ptyLayout, parent, false)");
            ((a.a)localObject2).<init>(paramViewGroup);
            return (RecyclerView.ViewHolder)localObject2;
          case 2131365499: 
            paramViewGroup = new com/truecaller/ads/a/a$a;
            localObject2 = com.truecaller.ads.b.c(b, (Context)localObject1);
            paramViewGroup.<init>((View)localObject2);
            return paramViewGroup;
          case 2131365498: 
            paramViewGroup = new com/truecaller/ads/a/a$a;
            localObject2 = com.truecaller.ads.b.a(b, (Context)localObject1);
            paramViewGroup.<init>((View)localObject2);
            return paramViewGroup;
          }
          paramViewGroup = new com/truecaller/ads/a/a$a;
          localObject2 = com.truecaller.ads.b.b(b, (Context)localObject1);
          paramViewGroup.<init>((View)localObject2);
          return paramViewGroup;
        }
        localObject2 = new com/truecaller/ads/a/a$a;
        locala = b;
        paramViewGroup = com.truecaller.ads.d.a((Context)localObject1, locala, paramViewGroup);
        ((a.a)localObject2).<init>(paramViewGroup);
        return (RecyclerView.ViewHolder)localObject2;
      }
      paramViewGroup = new com/truecaller/ads/a/a$a;
      localObject2 = b;
      localObject2 = com.truecaller.ads.d.a((Context)localObject1, (com.truecaller.ads.a)localObject2);
      paramViewGroup.<init>((View)localObject2);
      return paramViewGroup;
    }
    Object localObject2 = new com/truecaller/ads/a/a$a;
    com.truecaller.ads.a locala = b;
    k.b(localObject1, "context");
    k.b(locala, "adType");
    localObject1 = LayoutInflater.from((Context)localObject1);
    i = locala.getBannerLayout();
    paramViewGroup = ((LayoutInflater)localObject1).inflate(i, paramViewGroup, false);
    k.a(paramViewGroup, "LayoutInflater.from(cont…nerLayout, parent, false)");
    ((a.a)localObject2).<init>(paramViewGroup);
    return (RecyclerView.ViewHolder)localObject2;
  }
  
  public void onDetachedFromRecyclerView(RecyclerView paramRecyclerView)
  {
    super.onDetachedFromRecyclerView(paramRecyclerView);
    c.b(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */