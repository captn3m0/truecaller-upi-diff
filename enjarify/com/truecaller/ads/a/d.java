package com.truecaller.ads.a;

public final class d
  implements b
{
  private final int a;
  private final int b;
  
  public d(int paramInt)
  {
    this(paramInt, paramInt);
  }
  
  public d(int paramInt1, int paramInt2)
  {
    a = paramInt1;
    b = paramInt2;
  }
  
  public final int a(int paramInt)
  {
    int i = a;
    if (paramInt < i) {
      return paramInt;
    }
    return paramInt + 1;
  }
  
  public final int b(int paramInt)
  {
    int i = a;
    if (paramInt < i) {
      return paramInt;
    }
    return paramInt + 1;
  }
  
  public final int c(int paramInt)
  {
    boolean bool = e(paramInt);
    if (bool) {
      return -1;
    }
    int i = a;
    if (paramInt < i) {
      return paramInt;
    }
    return paramInt + -1;
  }
  
  public final int d(int paramInt)
  {
    paramInt = e(paramInt);
    if (paramInt == 0) {
      return -1;
    }
    return b;
  }
  
  public final boolean e(int paramInt)
  {
    int i = a;
    paramInt -= i;
    return paramInt == 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */