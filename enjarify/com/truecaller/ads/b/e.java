package com.truecaller.ads.b;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.FrameLayout;
import c.g.b.k;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeAppInstallAdView;
import com.truecaller.R.id;
import com.truecaller.ads.a;
import com.truecaller.ads.provider.fetch.c;
import com.truecaller.ads.provider.holders.i;

public final class e
  extends RecyclerView.ViewHolder
  implements f.b.e
{
  private final NativeAppInstallAdView a;
  
  public e(View paramView)
  {
    super(paramView);
    Object localObject1 = j.a();
    Object localObject2 = paramView.getContext();
    String str = "view.context";
    k.a(localObject2, str);
    localObject1 = com.truecaller.ads.b.b((a)localObject1, (Context)localObject2);
    int i = R.id.container;
    paramView = (FrameLayout)paramView.findViewById(i);
    if (paramView != null)
    {
      localObject2 = localObject1;
      localObject2 = (View)localObject1;
      paramView.addView((View)localObject2);
    }
    a = ((NativeAppInstallAdView)localObject1);
  }
  
  public final void a(i parami)
  {
    k.b(parami, "ad");
    NativeAppInstallAdView localNativeAppInstallAdView = a;
    NativeAppInstallAd localNativeAppInstallAd = (NativeAppInstallAd)parami.k();
    parami = a.d;
    com.truecaller.ads.b.a(localNativeAppInstallAdView, localNativeAppInstallAd, parami);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */