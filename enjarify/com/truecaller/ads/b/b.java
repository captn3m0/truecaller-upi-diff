package com.truecaller.ads.b;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.FrameLayout;
import c.g.b.k;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeContentAdView;
import com.truecaller.R.id;
import com.truecaller.ads.a;

public final class b
  extends RecyclerView.ViewHolder
  implements f.b.b
{
  private final NativeContentAdView a;
  
  public b(View paramView)
  {
    super(paramView);
    Object localObject1 = j.a();
    Object localObject2 = paramView.getContext();
    String str = "view.context";
    k.a(localObject2, str);
    localObject1 = com.truecaller.ads.b.a((a)localObject1, (Context)localObject2);
    int i = R.id.container;
    paramView = (FrameLayout)paramView.findViewById(i);
    if (paramView != null)
    {
      localObject2 = localObject1;
      localObject2 = (View)localObject1;
      paramView.addView((View)localObject2);
    }
    a = ((NativeContentAdView)localObject1);
  }
  
  public final void a(com.truecaller.ads.provider.holders.c paramc)
  {
    k.b(paramc, "ad");
    NativeContentAdView localNativeContentAdView = a;
    NativeContentAd localNativeContentAd = (NativeContentAd)paramc.k();
    paramc = a.d;
    com.truecaller.ads.b.a(localNativeContentAdView, localNativeContentAd, paramc);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */