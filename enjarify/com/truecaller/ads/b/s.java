package com.truecaller.ads.b;

import com.truecaller.ads.provider.holders.AdHolderType;

public final class s
  extends i
  implements f.a.d
{
  public s(com.truecaller.ads.provider.e parame)
  {
    super(parame);
  }
  
  public final boolean a(com.truecaller.ads.provider.holders.e parame)
  {
    if (parame != null) {
      parame = parame.a();
    } else {
      parame = null;
    }
    AdHolderType localAdHolderType = AdHolderType.HOUSE_AD;
    return parame == localAdHolderType;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.b.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */