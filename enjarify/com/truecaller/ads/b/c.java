package com.truecaller.ads.b;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.FrameLayout;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.ads.provider.holders.d;

public final class c
  extends RecyclerView.ViewHolder
  implements f.b.c
{
  private final com.truecaller.ads.c.c a;
  
  public c(View paramView)
  {
    super(paramView);
    Object localObject1 = j.a();
    Object localObject2 = paramView.getContext();
    String str = "view.context";
    k.a(localObject2, str);
    localObject1 = com.truecaller.ads.b.c((com.truecaller.ads.a)localObject1, (Context)localObject2);
    int i = R.id.container;
    paramView = (FrameLayout)paramView.findViewById(i);
    if (paramView != null)
    {
      localObject2 = localObject1;
      localObject2 = (View)localObject1;
      paramView.addView((View)localObject2);
    }
    a = ((com.truecaller.ads.c.c)localObject1);
  }
  
  public final void a(d paramd)
  {
    k.b(paramd, "ad");
    com.truecaller.ads.c.c localc = a;
    com.truecaller.ads.c.a locala = com.truecaller.ads.c.b.a(paramd);
    paramd = a.d;
    com.truecaller.ads.b.a(localc, locala, paramd);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */