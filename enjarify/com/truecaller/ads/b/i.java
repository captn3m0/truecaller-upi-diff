package com.truecaller.ads.b;

import com.truecaller.adapter_delegates.d;

public abstract class i
  extends d
{
  private final com.truecaller.ads.provider.e b;
  
  public i(com.truecaller.ads.provider.e parame)
  {
    b = parame;
  }
  
  public abstract void a(f.b paramb, com.truecaller.ads.provider.holders.e parame);
  
  public final boolean a(int paramInt)
  {
    com.truecaller.ads.provider.holders.e locale = b.b(paramInt);
    return a(locale);
  }
  
  public abstract boolean a(com.truecaller.ads.provider.holders.e parame);
  
  public int getItemCount()
  {
    return -1 >>> 1;
  }
  
  public long getItemId(int paramInt)
  {
    return paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.b.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */