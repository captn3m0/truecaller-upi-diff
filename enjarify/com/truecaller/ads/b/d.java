package com.truecaller.ads.b;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import c.g.b.k;
import com.truecaller.ads.provider.b.e;
import com.truecaller.ads.provider.holders.b;
import com.truecaller.ads.provider.holders.g;

public final class d
  extends RecyclerView.ViewHolder
  implements f.b.d
{
  private final com.truecaller.ads.ui.c a;
  
  public d(View paramView)
  {
    super(paramView);
    paramView = (com.truecaller.ads.ui.c)paramView;
    a = paramView;
  }
  
  public final void a(g paramg)
  {
    k.b(paramg, "ad");
    com.truecaller.ads.ui.c localc = a;
    e locale = (e)paramg.g();
    paramg = a.d;
    com.truecaller.ads.d.a(localc, locale, paramg);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */