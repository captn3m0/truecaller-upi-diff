package com.truecaller.ads.b;

import com.truecaller.ads.provider.holders.AdHolderType;
import com.truecaller.ads.provider.holders.AdNativeHolder;
import com.truecaller.ads.provider.holders.AdNativeHolder.Type;

public final class u
  extends i
  implements f.a.e
{
  public u(com.truecaller.ads.provider.e parame)
  {
    super(parame);
  }
  
  public final boolean a(com.truecaller.ads.provider.holders.e parame)
  {
    AdNativeHolder.Type localType = null;
    AdHolderType localAdHolderType1;
    boolean bool;
    if (parame != null)
    {
      localAdHolderType1 = parame.a();
    }
    else
    {
      bool = false;
      localAdHolderType1 = null;
    }
    AdHolderType localAdHolderType2 = AdHolderType.NATIVE_AD;
    if (localAdHolderType1 == localAdHolderType2)
    {
      bool = parame instanceof AdNativeHolder;
      if (!bool) {
        parame = null;
      }
      parame = (AdNativeHolder)parame;
      if (parame != null) {
        localType = parame.i();
      }
      parame = AdNativeHolder.Type.INSTALL;
      if (localType == parame) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.b.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */