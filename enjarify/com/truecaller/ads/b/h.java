package com.truecaller.ads.b;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.FrameLayout;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.ads.a;
import com.truecaller.ads.d;

public final class h
  extends RecyclerView.ViewHolder
  implements f.b.g
{
  public h(View paramView)
  {
    super(paramView);
    Object localObject = paramView.getContext();
    k.a(localObject, "view.context");
    a locala = j.a();
    int i = R.id.container;
    FrameLayout localFrameLayout = (FrameLayout)paramView.findViewById(i);
    localObject = d.a((Context)localObject, locala, localFrameLayout);
    int j = R.id.container;
    paramView = (FrameLayout)paramView.findViewById(j);
    if (paramView != null) {
      paramView.addView((View)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.b.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */