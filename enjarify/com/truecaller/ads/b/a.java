package com.truecaller.ads.b;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import c.g.b.k;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.truecaller.R.id;

public final class a
  extends RecyclerView.ViewHolder
  implements f.b.a
{
  private final FrameLayout a;
  
  public a(View paramView)
  {
    super(paramView);
    paramView = (FrameLayout)paramView;
    a = paramView;
  }
  
  public final void a(com.truecaller.ads.provider.holders.a parama)
  {
    k.b(parama, "ad");
    parama = (PublisherAdView)parama.g();
    Object localObject = parama.getParent();
    boolean bool = localObject instanceof ViewGroup;
    if (!bool) {
      localObject = null;
    }
    localObject = (ViewGroup)localObject;
    if (localObject != null) {
      ((ViewGroup)localObject).removeAllViews();
    }
    localObject = (View)a;
    int i = R.id.container;
    localObject = (FrameLayout)((View)localObject).findViewById(i);
    parama = (View)parama;
    ((FrameLayout)localObject).addView(parama);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */