package com.truecaller.ads;

import android.content.res.Resources;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.d.b.ab;
import com.d.b.e;
import com.d.b.w;
import com.google.android.gms.ads.formats.NativeAd.Image;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.truecaller.ads.ui.d;
import com.truecaller.utils.extensions.t;

public final class d$d
  implements d
{
  d$d(NativeCustomTemplateAd paramNativeCustomTemplateAd) {}
  
  public final void a(ImageView paramImageView, TextView paramTextView)
  {
    k.b(paramImageView, "fallbackImage");
    k.b(paramTextView, "fallbackTextView");
    t.b((View)paramTextView);
    paramTextView = a;
    Object localObject = "Image";
    paramTextView = paramTextView.getImage((String)localObject);
    if (paramTextView != null)
    {
      float f = paramImageView.getResources().getDimension(2131165290);
      int i = (int)f;
      w localw = i.b();
      paramTextView = paramTextView.getUri();
      paramTextView = localw.a(paramTextView);
      localw = null;
      paramTextView = paramTextView.b(0, i);
      localObject = new com/truecaller/ads/d$d$a;
      ((d.d.a)localObject).<init>(this, paramImageView);
      localObject = (e)localObject;
      paramTextView.a(paramImageView, (e)localObject);
      paramTextView = new com/truecaller/ads/d$d$b;
      paramTextView.<init>(this);
      paramTextView = (View.OnClickListener)paramTextView;
      paramImageView.setOnClickListener(paramTextView);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.d.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */