package com.truecaller.ads.installedapps;

import android.arch.persistence.db.c;
import android.arch.persistence.db.c.a;
import android.arch.persistence.db.c.b;
import android.arch.persistence.db.c.b.a;
import android.arch.persistence.db.c.c;
import android.arch.persistence.room.a;
import android.arch.persistence.room.d;
import android.arch.persistence.room.h;
import android.arch.persistence.room.h.a;

public class InstalledAppsDatabase_Impl
  extends InstalledAppsDatabase
{
  private volatile e g;
  
  public final d a()
  {
    d locald = new android/arch/persistence/room/d;
    String[] arrayOfString = { "installed_packages" };
    locald.<init>(this, arrayOfString);
    return locald;
  }
  
  public final c b(a parama)
  {
    Object localObject1 = new android/arch/persistence/room/h;
    Object localObject2 = new com/truecaller/ads/installedapps/InstalledAppsDatabase_Impl$1;
    ((InstalledAppsDatabase_Impl.1)localObject2).<init>(this);
    ((h)localObject1).<init>(parama, (h.a)localObject2, "7104f80cc2774b51ae9edace6941a587", "25a22eaf06bcbfff12f8251de9c4aa01");
    localObject2 = c.b.a(b);
    String str = c;
    b = str;
    c = ((c.a)localObject1);
    localObject1 = ((c.b.a)localObject2).a();
    return a.a((c.b)localObject1);
  }
  
  public final e h()
  {
    Object localObject1 = g;
    if (localObject1 != null) {
      return g;
    }
    try
    {
      localObject1 = g;
      if (localObject1 == null)
      {
        localObject1 = new com/truecaller/ads/installedapps/f;
        ((f)localObject1).<init>(this);
        g = ((e)localObject1);
      }
      localObject1 = g;
      return (e)localObject1;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.installedapps.InstalledAppsDatabase_Impl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */