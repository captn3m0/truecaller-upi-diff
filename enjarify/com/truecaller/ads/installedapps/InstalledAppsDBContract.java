package com.truecaller.ads.installedapps;

public final class InstalledAppsDBContract
{
  public static final InstalledAppsDBContract INSTANCE;
  
  static
  {
    InstalledAppsDBContract localInstalledAppsDBContract = new com/truecaller/ads/installedapps/InstalledAppsDBContract;
    localInstalledAppsDBContract.<init>();
    INSTANCE = localInstalledAppsDBContract;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.installedapps.InstalledAppsDBContract
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */