package com.truecaller.ads.installedapps;

import android.arch.persistence.room.b;
import android.arch.persistence.room.c;
import android.arch.persistence.room.i;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;

public final class f
  implements e
{
  private final android.arch.persistence.room.f a;
  private final c b;
  private final b c;
  
  public f(android.arch.persistence.room.f paramf)
  {
    a = paramf;
    Object localObject = new com/truecaller/ads/installedapps/f$1;
    ((f.1)localObject).<init>(this, paramf);
    b = ((c)localObject);
    localObject = new com/truecaller/ads/installedapps/f$2;
    ((f.2)localObject).<init>(this, paramf);
    c = ((b)localObject);
  }
  
  public final List a()
  {
    i locali = i.a("SELECT * FROM installed_packages", 0);
    Cursor localCursor = a.a(locali);
    String str1 = "package_name";
    try
    {
      int i = localCursor.getColumnIndexOrThrow(str1);
      String str2 = "version_name";
      int j = localCursor.getColumnIndexOrThrow(str2);
      String str3 = "version_code";
      int k = localCursor.getColumnIndexOrThrow(str3);
      String str4 = "first_install_time";
      int m = localCursor.getColumnIndexOrThrow(str4);
      String str5 = "last_update_time";
      int n = localCursor.getColumnIndexOrThrow(str5);
      ArrayList localArrayList = new java/util/ArrayList;
      int i1 = localCursor.getCount();
      localArrayList.<init>(i1);
      for (;;)
      {
        boolean bool = localCursor.moveToNext();
        if (!bool) {
          break;
        }
        String str6 = localCursor.getString(i);
        String str7 = localCursor.getString(j);
        int i2 = localCursor.getInt(k);
        long l1 = localCursor.getLong(m);
        long l2 = localCursor.getLong(n);
        d locald = new com/truecaller/ads/installedapps/d;
        locald.<init>(str6, str7, i2, l1, l2);
        localArrayList.add(locald);
      }
      return localArrayList;
    }
    finally
    {
      localCursor.close();
      locali.b();
    }
  }
  
  public final void a(List paramList)
  {
    Object localObject = a;
    ((android.arch.persistence.room.f)localObject).d();
    try
    {
      localObject = b;
      ((c)localObject).a(paramList);
      paramList = a;
      paramList.f();
      return;
    }
    finally
    {
      a.e();
    }
  }
  
  public final void b(List paramList)
  {
    Object localObject = a;
    ((android.arch.persistence.room.f)localObject).d();
    try
    {
      localObject = c;
      ((b)localObject).a(paramList);
      paramList = a;
      paramList.f();
      return;
    }
    finally
    {
      a.e();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.installedapps.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */