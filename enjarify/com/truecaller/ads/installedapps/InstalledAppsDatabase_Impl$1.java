package com.truecaller.ads.installedapps;

import android.arch.persistence.room.b.b.a;
import android.arch.persistence.room.f.b;
import android.arch.persistence.room.h.a;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class InstalledAppsDatabase_Impl$1
  extends h.a
{
  InstalledAppsDatabase_Impl$1(InstalledAppsDatabase_Impl paramInstalledAppsDatabase_Impl)
  {
    super(1);
  }
  
  public final void a(android.arch.persistence.db.b paramb)
  {
    paramb.c("DROP TABLE IF EXISTS `installed_packages`");
  }
  
  public final void b(android.arch.persistence.db.b paramb)
  {
    paramb.c("CREATE TABLE IF NOT EXISTS `installed_packages` (`package_name` TEXT NOT NULL, `version_name` TEXT NOT NULL, `version_code` INTEGER NOT NULL, `first_install_time` INTEGER NOT NULL, `last_update_time` INTEGER NOT NULL, PRIMARY KEY(`package_name`))");
    paramb.c("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    paramb.c("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"7104f80cc2774b51ae9edace6941a587\")");
  }
  
  public final void c(android.arch.persistence.db.b paramb)
  {
    InstalledAppsDatabase_Impl.a(b, paramb);
    InstalledAppsDatabase_Impl.b(b, paramb);
    List localList1 = InstalledAppsDatabase_Impl.d(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = InstalledAppsDatabase_Impl.e(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)InstalledAppsDatabase_Impl.f(b).get(i);
        localb.b(paramb);
        i += 1;
      }
    }
  }
  
  public final void d(android.arch.persistence.db.b paramb)
  {
    List localList1 = InstalledAppsDatabase_Impl.a(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = InstalledAppsDatabase_Impl.b(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)InstalledAppsDatabase_Impl.c(b).get(i);
        localb.a(paramb);
        i += 1;
      }
    }
  }
  
  public final void e(android.arch.persistence.db.b paramb)
  {
    Object localObject1 = new java/util/HashMap;
    int i = 5;
    ((HashMap)localObject1).<init>(i);
    Object localObject2 = new android/arch/persistence/room/b/b$a;
    int j = 1;
    ((b.a)localObject2).<init>("package_name", "TEXT", j, j);
    ((HashMap)localObject1).put("package_name", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("version_name", "TEXT", j, 0);
    ((HashMap)localObject1).put("version_name", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("version_code", "INTEGER", j, 0);
    ((HashMap)localObject1).put("version_code", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("first_install_time", "INTEGER", j, 0);
    ((HashMap)localObject1).put("first_install_time", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("last_update_time", "INTEGER", j, 0);
    ((HashMap)localObject1).put("last_update_time", localObject2);
    Object localObject3 = new java/util/HashSet;
    ((HashSet)localObject3).<init>(0);
    localObject2 = new java/util/HashSet;
    ((HashSet)localObject2).<init>(0);
    android.arch.persistence.room.b.b localb = new android/arch/persistence/room/b/b;
    String str = "installed_packages";
    localb.<init>(str, (Map)localObject1, (Set)localObject3, (Set)localObject2);
    localObject1 = "installed_packages";
    paramb = android.arch.persistence.room.b.b.a(paramb, (String)localObject1);
    boolean bool = localb.equals(paramb);
    if (bool) {
      return;
    }
    localObject1 = new java/lang/IllegalStateException;
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("Migration didn't properly handle installed_packages(com.truecaller.ads.installedapps.InstalledPackage).\n Expected:\n");
    ((StringBuilder)localObject3).append(localb);
    ((StringBuilder)localObject3).append("\n Found:\n");
    ((StringBuilder)localObject3).append(paramb);
    paramb = ((StringBuilder)localObject3).toString();
    ((IllegalStateException)localObject1).<init>(paramb);
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.installedapps.InstalledAppsDatabase_Impl.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */