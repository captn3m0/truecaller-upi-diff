package com.truecaller.ads.installedapps;

import c.g.b.k;

public final class d
{
  public final String a;
  public final String b;
  public final int c;
  public final long d;
  public final long e;
  
  public d(String paramString1, String paramString2, int paramInt, long paramLong1, long paramLong2)
  {
    a = paramString1;
    b = paramString2;
    c = paramInt;
    d = paramLong1;
    e = paramLong2;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof d;
    if (bool1)
    {
      paramObject = (d)paramObject;
      String str1 = a;
      String str2 = a;
      bool1 = k.a(str1, str2);
      if (bool1)
      {
        str1 = b;
        str2 = b;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          int i = c;
          int j = c;
          if (i == j)
          {
            long l1 = d;
            long l2 = d;
            boolean bool2 = l1 < l2;
            if (!bool2)
            {
              l1 = e;
              l2 = e;
              boolean bool3 = l1 < l2;
              if (!bool3) {
                return true;
              }
            }
          }
        }
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    return a.hashCode();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.installedapps.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */