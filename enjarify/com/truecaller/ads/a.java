package com.truecaller.ads;

public abstract interface a
{
  public abstract int getBannerLayout();
  
  public abstract int getEmptyLayout();
  
  public abstract int getHouseLayout();
  
  public abstract int getNativeLayout();
  
  public abstract int getPlaceholderLayout();
}

/* Location:
 * Qualified Name:     com.truecaller.ads.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */