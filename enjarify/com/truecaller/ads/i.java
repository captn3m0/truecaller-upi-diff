package com.truecaller.ads;

import android.content.Context;
import c.f;
import c.g.a.a;
import c.g.b.r;
import c.g.b.s;
import c.k;
import c.l.c;

public final class i
{
  private static Context b;
  private static final f c;
  private static final f d;
  private static final f e;
  
  static
  {
    Object localObject1 = new c.l.g[2];
    Object localObject2 = new c/g/b/s;
    c localc = c.g.b.w.a(i.class, "ads_release");
    ((s)localObject2).<init>(localc, "adsImageCache", "getAdsImageCache()Lcom/squareup/picasso/LruCache;");
    localObject2 = (c.l.g)c.g.b.w.a((r)localObject2);
    localObject1[0] = localObject2;
    localObject2 = new c/g/b/s;
    localc = c.g.b.w.a(i.class, "ads_release");
    ((s)localObject2).<init>(localc, "adsPicasso", "getAdsPicasso()Lcom/squareup/picasso/Picasso;");
    localObject2 = (c.l.g)c.g.b.w.a((r)localObject2);
    localObject1[1] = localObject2;
    a = (c.l.g[])localObject1;
    localObject1 = k.a;
    localObject2 = (a)i.b.a;
    localObject1 = c.g.a((k)localObject1, (a)localObject2);
    c = (f)localObject1;
    d = (f)localObject1;
    localObject1 = k.a;
    localObject2 = (a)i.a.a;
    e = c.g.a((k)localObject1, (a)localObject2);
  }
  
  public static final f a()
  {
    return c;
  }
  
  public static final com.d.b.w b()
  {
    return (com.d.b.w)e.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */