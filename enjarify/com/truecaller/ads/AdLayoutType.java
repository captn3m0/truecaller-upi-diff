package com.truecaller.ads;

public enum AdLayoutType
  implements a
{
  private final int bannerLayout;
  private final int emptyLayout;
  private final int houseLayout;
  private final int nativeLayout;
  private final int placeholderLayout;
  
  static
  {
    AdLayoutType[] arrayOfAdLayoutType = new AdLayoutType[4];
    AdLayoutType localAdLayoutType1 = new com/truecaller/ads/AdLayoutType;
    AdLayoutType localAdLayoutType2 = localAdLayoutType1;
    localAdLayoutType1.<init>("SMALL", 0, 2131558500, 2131558492, 2131558495, 0, 2131558502, 8, null);
    SMALL = localAdLayoutType1;
    arrayOfAdLayoutType[0] = localAdLayoutType1;
    localAdLayoutType2 = new com/truecaller/ads/AdLayoutType;
    localAdLayoutType2.<init>("LARGE", 1, 2131558498, 0, 2131558493, 0, 0, 26, null);
    LARGE = localAdLayoutType2;
    arrayOfAdLayoutType[1] = localAdLayoutType2;
    localAdLayoutType2 = new com/truecaller/ads/AdLayoutType;
    localAdLayoutType2.<init>("DETAILS", 2, 2131558496, 0, 0, 0, 0, 30, null);
    DETAILS = localAdLayoutType2;
    arrayOfAdLayoutType[2] = localAdLayoutType2;
    localAdLayoutType2 = new com/truecaller/ads/AdLayoutType;
    localAdLayoutType2.<init>("MEGA_VIDEO", 3, 2131558499, 0, 2131558494, 0, 2131558501, 10, null);
    MEGA_VIDEO = localAdLayoutType2;
    arrayOfAdLayoutType[3] = localAdLayoutType2;
    $VALUES = arrayOfAdLayoutType;
  }
  
  private AdLayoutType(int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
  {
    nativeLayout = paramInt2;
    bannerLayout = paramInt3;
    houseLayout = paramInt4;
    emptyLayout = paramInt5;
    placeholderLayout = paramInt6;
  }
  
  public final int getBannerLayout()
  {
    return bannerLayout;
  }
  
  public final int getEmptyLayout()
  {
    return emptyLayout;
  }
  
  public final int getHouseLayout()
  {
    return houseLayout;
  }
  
  public final int getNativeLayout()
  {
    return nativeLayout;
  }
  
  public final int getPlaceholderLayout()
  {
    return placeholderLayout;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ads.AdLayoutType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */