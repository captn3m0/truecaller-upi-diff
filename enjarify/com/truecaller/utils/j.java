package com.truecaller.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import c.g.b.k;

public final class j
  implements i
{
  private final Context a;
  
  public j(Context paramContext)
  {
    a = paramContext;
  }
  
  public final boolean a()
  {
    NetworkInfo localNetworkInfo = com.truecaller.utils.extensions.i.d(a).getActiveNetworkInfo();
    if (localNetworkInfo != null) {
      return localNetworkInfo.isConnected();
    }
    return false;
  }
  
  public final String b()
  {
    Object localObject = com.truecaller.utils.extensions.i.d(a).getActiveNetworkInfo();
    int i;
    Integer localInteger;
    if (localObject != null)
    {
      i = ((NetworkInfo)localObject).getType();
      localInteger = Integer.valueOf(i);
    }
    else
    {
      i = 0;
      localInteger = null;
    }
    if (localInteger == null) {
      return "no-connection";
    }
    int j = localInteger.intValue();
    if (j != 0)
    {
      j = localInteger.intValue();
      int k = 4;
      if (j != k)
      {
        j = localInteger.intValue();
        k = 5;
        if (j != k)
        {
          j = localInteger.intValue();
          k = 2;
          if (j != k)
          {
            i = localInteger.intValue();
            j = 3;
            if (i != j)
            {
              localObject = ((NetworkInfo)localObject).getTypeName();
              k.a(localObject, "typeName");
              return (String)localObject;
            }
          }
        }
      }
    }
    localObject = ((NetworkInfo)localObject).getSubtypeName();
    k.a(localObject, "subtypeName");
    return (String)localObject;
  }
  
  public final boolean c()
  {
    NetworkInfo localNetworkInfo = com.truecaller.utils.extensions.i.d(a).getActiveNetworkInfo();
    if (localNetworkInfo != null)
    {
      int i = localNetworkInfo.getType();
      if (i == 0) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean d()
  {
    NetworkInfo localNetworkInfo = com.truecaller.utils.extensions.i.d(a).getActiveNetworkInfo();
    if (localNetworkInfo != null)
    {
      int i = localNetworkInfo.getType();
      int j = 1;
      if (i == j) {
        return j;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */