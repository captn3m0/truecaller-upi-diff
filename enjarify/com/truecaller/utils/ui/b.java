package com.truecaller.utils.ui;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.content.b.f;
import android.support.v4.graphics.drawable.a;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;

public final class b
{
  public static int a(Context paramContext, int paramInt)
  {
    TypedValue localTypedValue = new android/util/TypedValue;
    localTypedValue.<init>();
    Resources.Theme localTheme = paramContext.getTheme();
    boolean bool = true;
    localTheme.resolveAttribute(paramInt, localTypedValue, bool);
    paramInt = resourceId;
    if (paramInt == 0) {
      return data;
    }
    paramContext = paramContext.getResources();
    paramInt = resourceId;
    return paramContext.getColor(paramInt);
  }
  
  public static Drawable a(Context paramContext, int paramInt1, int paramInt2)
  {
    PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
    return a(paramContext, paramInt1, paramInt2, localMode);
  }
  
  public static Drawable a(Context paramContext, int paramInt1, int paramInt2, PorterDuff.Mode paramMode)
  {
    Resources localResources = paramContext.getResources();
    Resources.Theme localTheme = paramContext.getTheme();
    Drawable localDrawable = a.e(f.a(localResources, paramInt1, localTheme).mutate());
    int i = a(paramContext, paramInt2);
    a.a(localDrawable, i);
    a.a(localDrawable, paramMode);
    return localDrawable;
  }
  
  private static Drawable a(ImageView paramImageView, Drawable paramDrawable)
  {
    paramDrawable = a.e(paramDrawable.mutate());
    paramImageView.setImageDrawable(paramDrawable);
    return paramDrawable;
  }
  
  public static void a(Context paramContext, Drawable paramDrawable, int paramInt)
  {
    int i = a(paramContext, paramInt);
    a.a(paramDrawable, i);
  }
  
  public static void a(Context paramContext, ImageView paramImageView, int paramInt)
  {
    Drawable localDrawable = paramImageView.getDrawable();
    if (localDrawable == null) {
      return;
    }
    paramImageView = a(paramImageView, localDrawable);
    paramContext = b(paramContext, paramInt);
    a.a(paramImageView, paramContext);
    paramImageView.invalidateSelf();
  }
  
  public static void a(View paramView, int paramInt)
  {
    paramInt = a(paramView.getContext(), paramInt);
    b(paramView, paramInt);
  }
  
  public static void a(View paramView, int paramInt, PorterDuff.Mode paramMode)
  {
    Drawable localDrawable = paramView.getBackground();
    if (localDrawable == null) {
      return;
    }
    localDrawable = a.e(localDrawable.mutate());
    paramView.setBackgroundDrawable(localDrawable);
    a.a(localDrawable, paramInt);
    a.a(localDrawable, paramMode);
    localDrawable.invalidateSelf();
  }
  
  public static void a(ImageView paramImageView, int paramInt)
  {
    Drawable localDrawable = paramImageView.getDrawable();
    if (localDrawable == null) {
      return;
    }
    paramImageView = a(paramImageView, localDrawable);
    a.a(paramImageView, paramInt);
    paramImageView.invalidateSelf();
  }
  
  public static ColorStateList b(Context paramContext, int paramInt)
  {
    TypedValue localTypedValue = new android/util/TypedValue;
    localTypedValue.<init>();
    Resources.Theme localTheme = paramContext.getTheme();
    boolean bool = true;
    localTheme.resolveAttribute(paramInt, localTypedValue, bool);
    paramInt = resourceId;
    if (paramInt == 0) {
      return ColorStateList.valueOf(data);
    }
    paramContext = paramContext.getResources();
    paramInt = resourceId;
    return paramContext.getColorStateList(paramInt);
  }
  
  public static void b(View paramView, int paramInt)
  {
    PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
    a(paramView, paramInt, localMode);
  }
  
  public static Drawable c(Context paramContext, int paramInt)
  {
    TypedValue localTypedValue = new android/util/TypedValue;
    localTypedValue.<init>();
    Resources.Theme localTheme = paramContext.getTheme();
    boolean bool = true;
    paramInt = localTheme.resolveAttribute(paramInt, localTypedValue, bool);
    if (paramInt != 0)
    {
      paramInt = resourceId;
      if (paramInt == 0)
      {
        paramInt = type;
        i = 28;
        if (paramInt >= i)
        {
          paramInt = type;
          i = 31;
          if (paramInt <= i)
          {
            paramContext = new android/graphics/drawable/ColorDrawable;
            paramInt = data;
            paramContext.<init>(paramInt);
            return paramContext;
          }
        }
      }
      paramInt = Build.VERSION.SDK_INT;
      int i = 21;
      if (paramInt >= i)
      {
        Resources localResources = paramContext.getResources();
        int j = resourceId;
        paramContext = paramContext.getTheme();
        return localResources.getDrawable(j, paramContext);
      }
      paramContext = paramContext.getResources();
      paramInt = resourceId;
      return paramContext.getDrawable(paramInt);
    }
    return null;
  }
  
  public static int d(Context paramContext, int paramInt)
  {
    TypedValue localTypedValue = new android/util/TypedValue;
    localTypedValue.<init>();
    paramContext.getTheme().resolveAttribute(paramInt, localTypedValue, true);
    return resourceId;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.ui.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */