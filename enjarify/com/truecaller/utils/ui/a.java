package com.truecaller.utils.ui;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import c.g.b.k;

public final class a
  extends ClickableSpan
{
  private final int a;
  private final c.g.a.a b;
  
  public a(int paramInt, c.g.a.a parama)
  {
    a = paramInt;
    b = parama;
  }
  
  public final void onClick(View paramView)
  {
    k.b(paramView, "widget");
    b.invoke();
  }
  
  public final void updateDrawState(TextPaint paramTextPaint)
  {
    k.b(paramTextPaint, "ds");
    int i = a;
    paramTextPaint.setColor(i);
    paramTextPaint.setUnderlineText(false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.ui.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */