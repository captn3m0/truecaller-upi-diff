package com.truecaller.utils.ui;

import android.content.Context;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.widget.TextView;
import c.g.a.m;
import c.g.b.k;

public final class LinkClickMovementMethod
  extends LinkMovementMethod
{
  private final GestureDetector a;
  private TextView b;
  private Spannable c;
  private final m d;
  
  public LinkClickMovementMethod(Context paramContext, m paramm)
  {
    d = paramm;
    paramm = new android/view/GestureDetector;
    Object localObject = new com/truecaller/utils/ui/LinkClickMovementMethod$a;
    ((LinkClickMovementMethod.a)localObject).<init>(this);
    localObject = (GestureDetector.OnGestureListener)localObject;
    paramm.<init>(paramContext, (GestureDetector.OnGestureListener)localObject);
    a = paramm;
  }
  
  public final boolean onTouchEvent(TextView paramTextView, Spannable paramSpannable, MotionEvent paramMotionEvent)
  {
    k.b(paramTextView, "widget");
    k.b(paramSpannable, "buffer");
    k.b(paramMotionEvent, "event");
    b = paramTextView;
    c = paramSpannable;
    a.onTouchEvent(paramMotionEvent);
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.ui.LinkClickMovementMethod
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */