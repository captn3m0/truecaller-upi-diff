package com.truecaller.utils.ui;

public enum LinkClickMovementMethod$LinkType
{
  static
  {
    LinkType[] arrayOfLinkType = new LinkType[4];
    LinkType localLinkType = new com/truecaller/utils/ui/LinkClickMovementMethod$LinkType;
    localLinkType.<init>("PHONE", 0);
    PHONE = localLinkType;
    arrayOfLinkType[0] = localLinkType;
    localLinkType = new com/truecaller/utils/ui/LinkClickMovementMethod$LinkType;
    int i = 1;
    localLinkType.<init>("WEB_URL", i);
    WEB_URL = localLinkType;
    arrayOfLinkType[i] = localLinkType;
    localLinkType = new com/truecaller/utils/ui/LinkClickMovementMethod$LinkType;
    i = 2;
    localLinkType.<init>("EMAIL_ADDRESS", i);
    EMAIL_ADDRESS = localLinkType;
    arrayOfLinkType[i] = localLinkType;
    localLinkType = new com/truecaller/utils/ui/LinkClickMovementMethod$LinkType;
    i = 3;
    localLinkType.<init>("NONE", i);
    NONE = localLinkType;
    arrayOfLinkType[i] = localLinkType;
    $VALUES = arrayOfLinkType;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.ui.LinkClickMovementMethod.LinkType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */