package com.truecaller.utils.ui;

import android.text.Layout;
import android.text.Spannable;
import android.text.style.ClickableSpan;
import android.util.Patterns;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.widget.TextView;
import c.g.a.m;
import c.g.b.k;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class LinkClickMovementMethod$a
  extends GestureDetector.SimpleOnGestureListener
{
  public LinkClickMovementMethod$a(LinkClickMovementMethod paramLinkClickMovementMethod) {}
  
  public final boolean onDown(MotionEvent paramMotionEvent)
  {
    k.b(paramMotionEvent, "event");
    return true;
  }
  
  public final boolean onSingleTapConfirmed(MotionEvent paramMotionEvent)
  {
    k.b(paramMotionEvent, "event");
    Object localObject1 = LinkClickMovementMethod.a(a);
    Object localObject2 = LinkClickMovementMethod.b(a);
    int i = (int)paramMotionEvent.getX();
    float f1 = paramMotionEvent.getY();
    int j = (int)f1;
    int k = ((TextView)localObject1).getTotalPaddingLeft();
    i -= k;
    k = ((TextView)localObject1).getTotalPaddingTop();
    j -= k;
    k = ((TextView)localObject1).getScrollX();
    i += k;
    k = ((TextView)localObject1).getScrollY();
    j += k;
    localObject1 = ((TextView)localObject1).getLayout();
    j = ((Layout)localObject1).getLineForVertical(j);
    float f2 = i;
    j = ((Layout)localObject1).getOffsetForHorizontal(j, f2);
    localObject1 = ClickableSpan.class;
    paramMotionEvent = (ClickableSpan[])((Spannable)localObject2).getSpans(j, j, (Class)localObject1);
    int m = 1;
    i = 0;
    f2 = 0.0F;
    if (paramMotionEvent != null)
    {
      k = paramMotionEvent.length;
      if (k == 0) {
        k = 1;
      } else {
        k = 0;
      }
      if (k == 0)
      {
        k = 0;
        break label186;
      }
    }
    k = 1;
    label186:
    if (k == m)
    {
      paramMotionEvent = "";
    }
    else
    {
      localObject1 = paramMotionEvent[0];
      m = ((Spannable)localObject2).getSpanStart(localObject1);
      paramMotionEvent = paramMotionEvent[0];
      j = ((Spannable)localObject2).getSpanEnd(paramMotionEvent);
      paramMotionEvent = ((Spannable)localObject2).subSequence(m, j).toString();
    }
    localObject1 = Patterns.PHONE;
    localObject2 = paramMotionEvent;
    localObject2 = (CharSequence)paramMotionEvent;
    localObject1 = ((Pattern)localObject1).matcher((CharSequence)localObject2);
    boolean bool = ((Matcher)localObject1).matches();
    if (bool)
    {
      localObject1 = LinkClickMovementMethod.LinkType.PHONE;
    }
    else
    {
      localObject1 = Patterns.WEB_URL.matcher((CharSequence)localObject2);
      bool = ((Matcher)localObject1).matches();
      if (bool)
      {
        localObject1 = LinkClickMovementMethod.LinkType.WEB_URL;
      }
      else
      {
        localObject1 = Patterns.EMAIL_ADDRESS.matcher((CharSequence)localObject2);
        bool = ((Matcher)localObject1).matches();
        if (bool) {
          localObject1 = LinkClickMovementMethod.LinkType.EMAIL_ADDRESS;
        } else {
          localObject1 = LinkClickMovementMethod.LinkType.NONE;
        }
      }
    }
    LinkClickMovementMethod.c(a).invoke(paramMotionEvent, localObject1);
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.ui.LinkClickMovementMethod.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */