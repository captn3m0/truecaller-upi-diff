package com.truecaller.utils.extensions;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.OperationCanceledException;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.g.b.v.c;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

final class h$b$1
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  h$b$1(h.b paramb, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/utils/extensions/h$b$1;
    h.b localb = b;
    local1.<init>(localb, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (bool) {}
    }
    try
    {
      paramObject = b;
      paramObject = c;
      localObject = b;
      ContentResolver localContentResolver = b;
      localObject = b;
      Uri localUri = d;
      localObject = b;
      String[] arrayOfString1 = e;
      localObject = b;
      String str1 = f;
      localObject = b;
      String[] arrayOfString2 = g;
      localObject = b;
      String str2 = h;
      localObject = b;
      CancellationSignal localCancellationSignal = i;
      localObject = localContentResolver.query(localUri, arrayOfString1, str1, arrayOfString2, str2, localCancellationSignal);
      a = localObject;
    }
    catch (OperationCanceledException localOperationCanceledException)
    {
      for (;;) {}
    }
    return x.a;
    throw a;
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.h.b.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */