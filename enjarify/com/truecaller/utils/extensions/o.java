package com.truecaller.utils.extensions;

import c.a.ag;
import c.g.b.k;
import c.n.d;
import c.u;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;

public final class o
{
  public static final String a(String paramString)
  {
    k.b(paramString, "receiver$0");
    return a(paramString, "SHA-256");
  }
  
  private static final String a(String paramString1, String paramString2)
  {
    try
    {
      paramString2 = MessageDigest.getInstance(paramString2);
      Object localObject1 = d.a;
      if (paramString1 != null)
      {
        paramString1 = paramString1.getBytes((Charset)localObject1);
        localObject1 = "(this as java.lang.String).getBytes(charset)";
        k.a(paramString1, (String)localObject1);
        paramString2.update(paramString1);
        paramString1 = paramString2.digest();
        paramString2 = "digest";
        k.a(paramString1, paramString2);
        paramString2 = "";
        int i = paramString1.length;
        str = paramString2;
        int j = 0;
        paramString2 = null;
        while (j < i)
        {
          byte b = paramString1[j];
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          localStringBuilder.append(str);
          str = "%02x";
          int k = 1;
          Object[] arrayOfObject = new Object[k];
          Object localObject2 = Byte.valueOf(b);
          arrayOfObject[0] = localObject2;
          localObject2 = Arrays.copyOf(arrayOfObject, k);
          str = String.format(str, (Object[])localObject2);
          localObject2 = "java.lang.String.format(this, *args)";
          k.a(str, (String)localObject2);
          localStringBuilder.append(str);
          str = localStringBuilder.toString();
          j += 1;
        }
      }
      paramString1 = new c/u;
      paramString2 = "null cannot be cast to non-null type java.lang.String";
      paramString1.<init>(paramString2);
      throw paramString1;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      String str;
      for (;;) {}
    }
    str = null;
    return str;
  }
  
  public static final String a(String paramString, char... paramVarArgs)
  {
    k.b(paramString, "receiver$0");
    String str = "charsToEscape";
    k.b(paramVarArgs, str);
    int i = 92;
    Object localObject1 = (Collection)c.a.m.a(Character.valueOf(i));
    k.b(paramVarArgs, "receiver$0");
    k.b(paramVarArgs, "receiver$0");
    Object localObject2 = new java/util/LinkedHashSet;
    int j = ag.a(paramVarArgs.length);
    ((LinkedHashSet)localObject2).<init>(j);
    j = paramVarArgs.length;
    int k = 0;
    Object localObject3 = null;
    while (k < j)
    {
      char c2 = paramVarArgs[k];
      Character localCharacter = Character.valueOf(c2);
      ((LinkedHashSet)localObject2).add(localCharacter);
      k += 1;
    }
    paramVarArgs = (Iterable)c.a.m.g((Iterable)localObject2);
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (Collection)localObject2;
    paramVarArgs = paramVarArgs.iterator();
    for (;;)
    {
      boolean bool2 = paramVarArgs.hasNext();
      if (!bool2) {
        break;
      }
      Object localObject4 = paramVarArgs.next();
      localObject3 = localObject4;
      localObject3 = (Character)localObject4;
      k = ((Character)localObject3).charValue();
      int m;
      if (k == i)
      {
        m = 1;
      }
      else
      {
        m = 0;
        localObject3 = null;
      }
      if (m == 0) {
        ((Collection)localObject2).add(localObject4);
      }
    }
    localObject2 = (Iterable)localObject2;
    paramVarArgs = ((Iterable)c.a.m.c((Collection)localObject1, (Iterable)localObject2)).iterator();
    for (;;)
    {
      boolean bool1 = paramVarArgs.hasNext();
      if (!bool1) {
        break;
      }
      char c1 = ((Character)paramVarArgs.next()).charValue();
      localObject1 = String.valueOf(c1);
      localObject2 = "\\";
      str = String.valueOf(c1);
      str = ((String)localObject2).concat(str);
      paramString = c.n.m.a(paramString, (String)localObject1, str);
    }
    return paramString;
  }
  
  public static final String b(String paramString)
  {
    k.b(paramString, "receiver$0");
    return a(paramString, "MD5");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */