package com.truecaller.utils.extensions;

import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import c.d.f;
import c.g.a.b;
import c.g.a.m;
import c.g.b.k;
import java.lang.reflect.Constructor;
import kotlinx.coroutines.a.h;
import kotlinx.coroutines.a.q;
import kotlinx.coroutines.a.r;
import kotlinx.coroutines.a.s;
import kotlinx.coroutines.a.u;
import kotlinx.coroutines.a.w;
import kotlinx.coroutines.a.y;
import kotlinx.coroutines.aa;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ai;
import kotlinx.coroutines.android.c;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.e;

public final class j
{
  private static Handler a(Looper paramLooper)
  {
    Object localObject1 = "receiver$0";
    k.b(paramLooper, (String)localObject1);
    int i = Build.VERSION.SDK_INT;
    int j = 28;
    if (i >= j)
    {
      paramLooper = Handler.createAsync(paramLooper);
      k.a(paramLooper, "Handler.createAsync(this)");
      return paramLooper;
    }
    localObject1 = Handler.class;
    j = 3;
    try
    {
      Object localObject2 = new Class[j];
      Class localClass = Looper.class;
      localObject2[0] = localClass;
      localClass = Handler.Callback.class;
      int k = 1;
      localObject2[k] = localClass;
      localClass = Boolean.TYPE;
      int m = 2;
      localObject2[m] = localClass;
      localObject1 = ((Class)localObject1).getDeclaredConstructor((Class[])localObject2);
      localObject2 = "Handler::class.java.getD…vaPrimitiveType\n        )";
      k.a(localObject1, (String)localObject2);
      Object[] arrayOfObject = new Object[j];
      arrayOfObject[0] = paramLooper;
      arrayOfObject[k] = null;
      paramLooper = Boolean.TRUE;
      arrayOfObject[m] = paramLooper;
      paramLooper = ((Constructor)localObject1).newInstance(arrayOfObject);
      k.a(paramLooper, "constructor.newInstance(this, null, true)");
      return (Handler)paramLooper;
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      localObject1 = new android/os/Handler;
      ((Handler)localObject1).<init>(paramLooper);
    }
    return (Handler)localObject1;
  }
  
  public static final Object a(h paramh)
  {
    Object localObject1 = "receiver$0";
    k.b(paramh, (String)localObject1);
    paramh = paramh.a();
    try
    {
      localObject1 = paramh.au_();
      return localObject1;
    }
    finally
    {
      paramh.n();
    }
  }
  
  public static final y a(ag paramag, int paramInt, b paramb)
  {
    k.b(paramag, "receiver$0");
    k.b(paramb, "action");
    Object localObject1 = new com/truecaller/utils/extensions/j$a;
    ((j.a)localObject1).<init>(paramb, null);
    localObject1 = (m)localObject1;
    paramb = (f)c.d.g.a;
    ai localai = ai.a;
    k.b(paramag, "receiver$0");
    k.b(paramb, "context");
    k.b(localai, "start");
    String str = "block";
    k.b(localObject1, str);
    paramag = aa.a(paramag, paramb);
    int i = -1 >>> 1;
    Object localObject2;
    if (paramInt != i)
    {
      switch (paramInt)
      {
      default: 
        paramb = new kotlinx/coroutines/a/g;
        paramb.<init>(paramInt);
        paramb = (kotlinx.coroutines.a.j)paramb;
        break;
      case 0: 
        localObject2 = new kotlinx/coroutines/a/w;
        ((w)localObject2).<init>();
        paramb = (b)localObject2;
        paramb = (kotlinx.coroutines.a.j)localObject2;
        break;
      case -1: 
        localObject2 = new kotlinx/coroutines/a/q;
        ((q)localObject2).<init>();
        paramb = (b)localObject2;
        paramb = (kotlinx.coroutines.a.j)localObject2;
        break;
      }
    }
    else
    {
      localObject2 = new kotlinx/coroutines/a/s;
      ((s)localObject2).<init>();
      paramb = (b)localObject2;
      paramb = (kotlinx.coroutines.a.j)localObject2;
    }
    paramInt = localai.a();
    if (paramInt != 0)
    {
      localObject2 = new kotlinx/coroutines/a/r;
      ((r)localObject2).<init>(paramag, paramb, (m)localObject1);
      localObject2 = (kotlinx.coroutines.a.d)localObject2;
    }
    else
    {
      localObject2 = new kotlinx/coroutines/a/d;
      boolean bool = true;
      ((kotlinx.coroutines.a.d)localObject2).<init>(paramag, paramb, bool);
    }
    ((kotlinx.coroutines.a.d)localObject2).a(localai, localObject2, (m)localObject1);
    return (y)localObject2;
  }
  
  public static final c a()
  {
    Looper localLooper = Looper.getMainLooper();
    k.a(localLooper, "Looper.getMainLooper()");
    return kotlinx.coroutines.android.d.a(a(localLooper), "Main");
  }
  
  public static final void a(ag paramag, h paramh, m paramm)
  {
    k.b(paramag, "receiver$0");
    k.b(paramh, "channel");
    k.b(paramm, "block");
    paramh = paramh.a();
    Object localObject = new com/truecaller/utils/extensions/j$b;
    ((j.b)localObject).<init>(paramh, paramm, null);
    localObject = (m)localObject;
    paramag = e.b(paramag, null, (m)localObject, 3);
    paramm = new com/truecaller/utils/extensions/j$c;
    paramm.<init>(paramh);
    paramm = (b)paramm;
    paramag.a_(paramm);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */