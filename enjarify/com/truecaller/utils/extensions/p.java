package com.truecaller.utils.extensions;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.widget.TextView;
import c.g.a.b;
import c.g.a.m;
import c.g.b.k;
import java.util.Arrays;

public final class p
{
  public static final TextView a(TextView paramTextView, int paramInt, Object... paramVarArgs)
  {
    k.b(paramTextView, "receiver$0");
    k.b(paramVarArgs, "args");
    Resources localResources = paramTextView.getResources();
    int i = paramVarArgs.length;
    paramVarArgs = Arrays.copyOf(paramVarArgs, i);
    CharSequence localCharSequence = (CharSequence)Html.fromHtml(localResources.getString(paramInt, paramVarArgs));
    paramTextView.setText(localCharSequence);
    return paramTextView;
  }
  
  public static final void a(TextView paramTextView, int paramInt)
  {
    k.b(paramTextView, "receiver$0");
    paramTextView = paramTextView.getCompoundDrawablesRelative();
    String str = "compoundDrawablesRelative";
    k.a(paramTextView, str);
    int i = paramTextView.length;
    int j = 0;
    while (j < i)
    {
      GradientDrawable localGradientDrawable = paramTextView[j];
      boolean bool = localGradientDrawable instanceof GradientDrawable;
      if (bool)
      {
        localGradientDrawable = (GradientDrawable)localGradientDrawable;
        localGradientDrawable.setColor(paramInt);
      }
      j += 1;
    }
  }
  
  private static void a(TextView paramTextView, Drawable paramDrawable1, Drawable paramDrawable2, Drawable paramDrawable3, Drawable paramDrawable4)
  {
    k.b(paramTextView, "receiver$0");
    paramTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(paramDrawable1, paramDrawable2, paramDrawable3, paramDrawable4);
  }
  
  public static final void a(TextView paramTextView, b paramb)
  {
    k.b(paramTextView, "receiver$0");
    k.b(paramb, "onTextChangedImpl");
    Object localObject = new com/truecaller/utils/extensions/p$a;
    ((p.a)localObject).<init>(paramb);
    localObject = (TextWatcher)localObject;
    paramTextView.addTextChangedListener((TextWatcher)localObject);
  }
  
  public static final void a(TextView paramTextView, m paramm)
  {
    k.b(paramTextView, "receiver$0");
    k.b(paramm, "spanProvider");
    Object localObject1 = new android/text/SpannableString;
    Object localObject2 = paramTextView.getText();
    ((SpannableString)localObject1).<init>((CharSequence)localObject2);
    localObject1 = (Spannable)localObject1;
    int i = ((Spannable)localObject1).length();
    Class localClass = CharacterStyle.class;
    int j = 0;
    localObject2 = (CharacterStyle[])((Spannable)localObject1).getSpans(0, i, localClass);
    if (localObject2 != null)
    {
      int k = localObject2.length;
      int m = 0;
      Object localObject3 = null;
      while (j < k)
      {
        Object localObject4 = localObject2[j];
        int n = m + 1;
        String str = "span";
        k.a(localObject4, str);
        localObject3 = Integer.valueOf(m);
        localObject3 = (CharacterStyle)paramm.invoke(localObject4, localObject3);
        if (localObject3 != null)
        {
          int i1 = ((Spannable)localObject1).getSpanStart(localObject4);
          int i2 = ((Spannable)localObject1).getSpanEnd(localObject4);
          int i3 = ((Spannable)localObject1).getSpanFlags(localObject4);
          ((Spannable)localObject1).removeSpan(localObject4);
          ((Spannable)localObject1).setSpan(localObject3, i1, i2, i3);
        }
        j += 1;
        m = n;
      }
    }
    localObject1 = (CharSequence)localObject1;
    paramTextView.setText((CharSequence)localObject1);
  }
  
  public static final void a(TextView paramTextView, String paramString)
  {
    k.b(paramTextView, "receiver$0");
    paramString = (CharSequence)paramString;
    int i = 0;
    if (paramString != null)
    {
      j = paramString.length();
      if (j != 0)
      {
        j = 0;
        break label38;
      }
    }
    int j = 1;
    label38:
    if (j != 0) {
      i = 8;
    }
    paramTextView.setVisibility(i);
    paramTextView.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */