package com.truecaller.utils.extensions;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.CancellationSignal;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.g.b.v.c;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.bs;
import kotlinx.coroutines.e;

final class h$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag j;
  
  h$b(ContentResolver paramContentResolver, v.c paramc, Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal, c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/utils/extensions/h$b;
    ContentResolver localContentResolver = b;
    v.c localc = c;
    Uri localUri = d;
    String[] arrayOfString1 = e;
    String str1 = f;
    String[] arrayOfString2 = g;
    String str2 = h;
    CancellationSignal localCancellationSignal = i;
    localb.<init>(localContentResolver, localc, localUri, arrayOfString1, str1, arrayOfString2, str2, localCancellationSignal, paramc);
    paramObject = (ag)paramObject;
    j = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int k = a;
    switch (k)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool = paramObject instanceof o.b;
      if (bool) {
        throw a;
      }
      break;
    case 0: 
      int m = paramObject instanceof o.b;
      if (m != 0) {
        break label144;
      }
      paramObject = j;
      f localf = (f)bs.a(null);
      Object localObject = new com/truecaller/utils/extensions/h$b$1;
      ((h.b.1)localObject).<init>(this, null);
      localObject = (m)localObject;
      paramObject = e.a((ag)paramObject, localf, (m)localObject, 2);
      m = 1;
      a = m;
      paramObject = ((ao)paramObject).a(this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    return x.a;
    label144:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.h.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */