package com.truecaller.utils.extensions;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import c.f;
import c.g;
import c.g.a.a;
import c.g.a.b;

public final class t
{
  public static final View a(ViewGroup paramViewGroup, int paramInt, boolean paramBoolean)
  {
    c.g.b.k.b(paramViewGroup, "receiver$0");
    paramViewGroup = LayoutInflater.from(paramViewGroup.getContext()).inflate(paramInt, paramViewGroup, paramBoolean);
    c.g.b.k.a(paramViewGroup, "LayoutInflater.from(cont…yout, this, attachToRoot)");
    return paramViewGroup;
  }
  
  public static final f a(View paramView, int paramInt)
  {
    c.g.b.k.b(paramView, "receiver$0");
    c.k localk = c.k.c;
    Object localObject = new com/truecaller/utils/extensions/t$b;
    ((t.b)localObject).<init>(paramView, paramInt);
    localObject = (a)localObject;
    return g.a(localk, (a)localObject);
  }
  
  public static final void a(View paramView)
  {
    c.g.b.k.b(paramView, "receiver$0");
    paramView.setVisibility(0);
  }
  
  public static final void a(View paramView, b paramb)
  {
    c.g.b.k.b(paramView, "receiver$0");
    c.g.b.k.b(paramb, "onFocusChangedImpl");
    Object localObject = new com/truecaller/utils/extensions/t$c;
    ((t.c)localObject).<init>(paramb);
    localObject = (View.OnFocusChangeListener)localObject;
    paramView.setOnFocusChangeListener((View.OnFocusChangeListener)localObject);
  }
  
  public static final void a(View paramView, boolean paramBoolean)
  {
    String str = "receiver$0";
    c.g.b.k.b(paramView, str);
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    paramView.setVisibility(paramBoolean);
  }
  
  public static final void a(View paramView, boolean paramBoolean, float paramFloat)
  {
    String str = "receiver$0";
    c.g.b.k.b(paramView, str);
    paramView.setEnabled(paramBoolean);
    if (paramBoolean) {
      paramFloat = 1.0F;
    }
    paramView.setAlpha(paramFloat);
  }
  
  public static final void a(View paramView, boolean paramBoolean, long paramLong)
  {
    c.g.b.k.b(paramView, "receiver$0");
    Object localObject = new com/truecaller/utils/extensions/t$d;
    ((t.d)localObject).<init>(paramView, paramBoolean);
    localObject = (Runnable)localObject;
    paramView.postDelayed((Runnable)localObject, paramLong);
  }
  
  public static final void b(View paramView)
  {
    c.g.b.k.b(paramView, "receiver$0");
    paramView.setVisibility(8);
  }
  
  public static final void c(View paramView)
  {
    c.g.b.k.b(paramView, "receiver$0");
    paramView.setVisibility(4);
  }
  
  public static final boolean d(View paramView)
  {
    String str = "receiver$0";
    c.g.b.k.b(paramView, str);
    int i = paramView.getVisibility();
    return i == 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */