package com.truecaller.utils.extensions;

import android.os.Build.VERSION;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import c.g.b.k;
import java.lang.reflect.Field;

public final class n
{
  public static final PowerManager.WakeLock a(PowerManager paramPowerManager)
  {
    k.b(paramPowerManager, "receiver$0");
    PowerManager localPowerManager = null;
    try
    {
      int i = Build.VERSION.SDK_INT;
      int j = 21;
      Object localObject;
      String str;
      if (i >= j)
      {
        i = 32;
        boolean bool = paramPowerManager.isWakeLockLevelSupported(i);
        if (bool)
        {
          localObject = Integer.valueOf(i);
        }
        else
        {
          i = 0;
          localObject = null;
        }
      }
      else
      {
        localObject = PowerManager.class;
        str = "PROXIMITY_SCREEN_OFF_WAKE_LOCK";
        localObject = ((Class)localObject).getField(str);
        i = ((Field)localObject).getInt(null);
        localObject = Integer.valueOf(i);
      }
      if (localObject != null)
      {
        localObject = (Number)localObject;
        i = ((Number)localObject).intValue();
        str = "truecaller:proximityLock";
        paramPowerManager = paramPowerManager.newWakeLock(i, str);
        localPowerManager = paramPowerManager;
      }
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    return localPowerManager;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */