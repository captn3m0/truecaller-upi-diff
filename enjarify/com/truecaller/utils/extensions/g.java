package com.truecaller.utils.extensions;

import android.database.Cursor;
import c.g.b.k;
import c.g.b.w;
import c.l.b;

public final class g
{
  private Integer a;
  private final g.a b;
  private final String c;
  private final b d;
  private final Object e;
  
  public g(String paramString, b paramb, Object paramObject)
  {
    c = paramString;
    d = paramb;
    e = paramObject;
    paramString = d;
    paramb = w.a(String.class);
    boolean bool1 = k.a(paramString, paramb);
    if (bool1)
    {
      paramString = new com/truecaller/utils/extensions/g$b;
      paramString.<init>(this);
      paramString = (g.a)paramString;
    }
    else
    {
      paramb = w.a(Integer.TYPE);
      bool1 = k.a(paramString, paramb);
      if (bool1)
      {
        paramString = new com/truecaller/utils/extensions/g$c;
        paramString.<init>(this);
        paramString = (g.a)paramString;
      }
      else
      {
        paramb = w.a(Long.TYPE);
        boolean bool2 = k.a(paramString, paramb);
        if (!bool2) {
          break label150;
        }
        paramString = new com/truecaller/utils/extensions/g$d;
        paramString.<init>(this);
        paramString = (g.a)paramString;
      }
    }
    b = paramString;
    return;
    label150:
    paramString = new java/lang/IllegalArgumentException;
    paramb = new java/lang/StringBuilder;
    paramb.<init>("Unsupported variable type ");
    paramObject = d;
    paramb.append(paramObject);
    paramb = paramb.toString();
    paramString.<init>(paramb);
    throw ((Throwable)paramString);
  }
  
  final int a(Cursor paramCursor)
  {
    Object localObject = a;
    if (localObject == null)
    {
      localObject = c;
      int i = paramCursor.getColumnIndexOrThrow((String)localObject);
      localObject = Integer.valueOf(i);
      a = ((Integer)localObject);
    }
    return ((Integer)localObject).intValue();
  }
  
  public final Object a(Cursor paramCursor, c.l.g paramg)
  {
    k.b(paramCursor, "cursor");
    String str = "property";
    k.b(paramg, str);
    int i = a(paramCursor);
    boolean bool = paramCursor.isNull(i);
    if (bool) {
      return e;
    }
    return b.a(paramCursor);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */