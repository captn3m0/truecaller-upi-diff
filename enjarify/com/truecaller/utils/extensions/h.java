package com.truecaller.utils.extensions;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.CancellationSignal;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.g.b.v.c;
import c.o.b;
import java.io.Closeable;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.ah;

public final class h
{
  /* Error */
  public static final Integer a(ContentResolver paramContentResolver, Uri paramUri, String paramString1, String paramString2, String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aload_0
    //   1: ldc 6
    //   3: invokestatic 12	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_1
    //   7: ldc 14
    //   9: invokestatic 12	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   12: aload_2
    //   13: ldc 16
    //   15: invokestatic 12	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   18: iconst_1
    //   19: anewarray 19	java/lang/String
    //   22: astore 5
    //   24: aload 5
    //   26: iconst_0
    //   27: aload_2
    //   28: aastore
    //   29: aload_0
    //   30: aload_1
    //   31: aload 5
    //   33: aload_3
    //   34: aload 4
    //   36: aconst_null
    //   37: invokevirtual 25	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   40: astore_0
    //   41: aconst_null
    //   42: astore_1
    //   43: aload_0
    //   44: ifnull +76 -> 120
    //   47: aload_0
    //   48: checkcast 27	java/io/Closeable
    //   51: astore_0
    //   52: aload_0
    //   53: astore_2
    //   54: aload_0
    //   55: checkcast 29	android/database/Cursor
    //   58: astore_2
    //   59: aload_2
    //   60: invokeinterface 33 1 0
    //   65: istore 6
    //   67: iload 6
    //   69: ifeq +25 -> 94
    //   72: aload_2
    //   73: iconst_0
    //   74: invokeinterface 37 2 0
    //   79: istore 7
    //   81: iload 7
    //   83: invokestatic 43	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   86: astore_2
    //   87: aload_0
    //   88: aconst_null
    //   89: invokestatic 49	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   92: aload_2
    //   93: areturn
    //   94: getstatic 54	c/x:a	Lc/x;
    //   97: astore_2
    //   98: aload_0
    //   99: aconst_null
    //   100: invokestatic 49	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   103: goto +17 -> 120
    //   106: astore_2
    //   107: goto +6 -> 113
    //   110: astore_1
    //   111: aload_1
    //   112: athrow
    //   113: aload_0
    //   114: aload_1
    //   115: invokestatic 49	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   118: aload_2
    //   119: athrow
    //   120: aconst_null
    //   121: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	122	0	paramContentResolver	ContentResolver
    //   0	122	1	paramUri	Uri
    //   0	122	2	paramString1	String
    //   0	122	3	paramString2	String
    //   0	122	4	paramArrayOfString	String[]
    //   22	10	5	arrayOfString	String[]
    //   65	3	6	bool	boolean
    //   79	3	7	i	int
    // Exception table:
    //   from	to	target	type
    //   111	113	106	finally
    //   54	58	110	finally
    //   59	65	110	finally
    //   73	79	110	finally
    //   81	86	110	finally
    //   94	97	110	finally
  }
  
  public static final Object a(ContentResolver paramContentResolver, Uri paramUri, String paramString1, String paramString2, c paramc)
  {
    Object localObject1 = paramc;
    boolean bool1 = paramc instanceof h.a;
    int k;
    if (bool1)
    {
      localObject3 = paramc;
      localObject3 = (h.a)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        k = b - j;
        b = k;
        break label86;
      }
    }
    Object localObject3 = new com/truecaller/utils/extensions/h$a;
    ((h.a)localObject3).<init>((c)localObject1);
    label86:
    localObject1 = a;
    Object localObject4 = a.a;
    int j = b;
    boolean bool2;
    Object localObject5;
    Object localObject6;
    Object localObject2;
    switch (j)
    {
    default: 
      localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)localObject1);
    case 1: 
      localObject4 = (v.c)j;
      localObject3 = (CancellationSignal)i;
      bool2 = localObject1 instanceof o.b;
      if (bool2) {
        try
        {
          localObject1 = (o.b)localObject1;
          localObject1 = a;
          throw ((Throwable)localObject1);
        }
        catch (CancellationException localCancellationException1)
        {
          localObject5 = localObject3;
          localObject6 = localObject4;
        }
      }
      break;
    case 0: 
      bool2 = localCancellationException1 instanceof o.b;
      if (bool2) {
        break label423;
      }
      localObject5 = new android/os/CancellationSignal;
      ((CancellationSignal)localObject5).<init>();
      localObject6 = new c/g/b/v$c;
      ((v.c)localObject6).<init>();
      k = 0;
      localObject2 = null;
      a = null;
    }
    try
    {
      Object localObject7 = new com/truecaller/utils/extensions/h$b;
      ((h.b)localObject7).<init>(paramContentResolver, (v.c)localObject6, paramUri, null, paramString1, null, paramString2, (CancellationSignal)localObject5, null);
      localObject7 = (m)localObject7;
      c = paramContentResolver;
      d = paramUri;
      e = null;
      f = paramString1;
      g = null;
      localObject2 = paramString2;
      h = paramString2;
      i = localObject5;
      j = localObject6;
      k = 1;
      b = k;
      localObject2 = ah.a((m)localObject7, (c)localObject3);
      if (localObject2 == localObject4) {
        return localObject4;
      }
      localObject3 = localObject5;
      localObject4 = localObject6;
      localObject2 = a;
      return (Cursor)localObject2;
    }
    catch (CancellationException localCancellationException2) {}
    ((CancellationSignal)localObject5).cancel();
    localObject3 = (Cursor)a;
    if (localObject3 != null)
    {
      localObject3 = (Closeable)localObject3;
      d.a((Closeable)localObject3);
    }
    throw ((Throwable)localCancellationException2);
    label423:
    throw a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */