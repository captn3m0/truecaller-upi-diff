package com.truecaller.utils.extensions;

import android.view.View;
import android.view.View.OnFocusChangeListener;
import c.g.a.b;

final class t$c
  implements View.OnFocusChangeListener
{
  t$c(b paramb) {}
  
  public final void onFocusChange(View paramView, boolean paramBoolean)
  {
    paramView = a;
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    paramView.invoke(localBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.t.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */