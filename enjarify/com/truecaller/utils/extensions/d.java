package com.truecaller.utils.extensions;

import java.io.Closeable;
import java.io.IOException;

public final class d
{
  public static final void a(Closeable paramCloseable)
  {
    if (paramCloseable != null) {
      try
      {
        paramCloseable.close();
        return;
      }
      catch (IOException localIOException) {}
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */