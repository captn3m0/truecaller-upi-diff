package com.truecaller.utils.extensions;

import android.database.Cursor;

public final class k
{
  public static final String a(Cursor paramCursor, String paramString)
  {
    c.g.b.k.b(paramCursor, "receiver$0");
    c.g.b.k.b(paramString, "columnName");
    int i = paramCursor.getColumnIndex(paramString);
    return paramCursor.getString(i);
  }
  
  public static final int b(Cursor paramCursor, String paramString)
  {
    c.g.b.k.b(paramCursor, "receiver$0");
    c.g.b.k.b(paramString, "columnName");
    int i = paramCursor.getColumnIndex(paramString);
    return paramCursor.getInt(i);
  }
  
  public static final long c(Cursor paramCursor, String paramString)
  {
    c.g.b.k.b(paramCursor, "receiver$0");
    c.g.b.k.b(paramString, "columnName");
    int i = paramCursor.getColumnIndex(paramString);
    return paramCursor.getLong(i);
  }
  
  public static final String d(Cursor paramCursor, String paramString)
  {
    c.g.b.k.b(paramCursor, "receiver$0");
    c.g.b.k.b(paramString, "columnName");
    int i = paramCursor.getColumnIndexOrThrow(paramString);
    return paramCursor.getString(i);
  }
  
  public static final int e(Cursor paramCursor, String paramString)
  {
    c.g.b.k.b(paramCursor, "receiver$0");
    c.g.b.k.b(paramString, "columnName");
    int i = paramCursor.getColumnIndexOrThrow(paramString);
    return paramCursor.getInt(i);
  }
  
  public static final long f(Cursor paramCursor, String paramString)
  {
    c.g.b.k.b(paramCursor, "receiver$0");
    c.g.b.k.b(paramString, "columnName");
    int i = paramCursor.getColumnIndexOrThrow(paramString);
    return paramCursor.getLong(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */