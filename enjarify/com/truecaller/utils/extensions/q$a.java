package com.truecaller.utils.extensions;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ah;
import kotlinx.coroutines.e.b;

final class q$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag d;
  
  q$a(q paramq, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/utils/extensions/q$a;
    q localq = c;
    locala.<init>(localq, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    boolean bool2;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      localObject2 = (ag)a;
      bool2 = paramObject instanceof o.b;
      if (!bool2) {
        paramObject = localObject2;
      } else {
        throw a;
      }
      break;
    case 1: 
      localObject2 = (ag)a;
      bool2 = paramObject instanceof o.b;
      if (!bool2)
      {
        paramObject = localObject2;
        localObject2 = localObject1;
        localObject1 = this;
        break label194;
      }
      throw a;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label274;
      }
      paramObject = d;
    }
    Object localObject2 = localObject1;
    localObject1 = this;
    Object localObject3;
    label194:
    do
    {
      bool2 = ah.a((ag)paramObject);
      if (!bool2) {
        break;
      }
      localObject3 = c.a;
      a = paramObject;
      int j = 1;
      b = j;
      localObject3 = ((b)localObject3).a((c)localObject1);
      if (localObject3 == localObject2) {
        return localObject2;
      }
      localObject3 = c.d;
      Object localObject4 = c.b;
      if (localObject4 == null)
      {
        localObject5 = "param";
        c.g.b.k.a((String)localObject5);
      }
      Object localObject5 = c;
      a = paramObject;
      int k = 2;
      b = k;
      localObject3 = ((c.g.a.q)localObject3).a(localObject4, localObject5, localObject1);
    } while (localObject3 != localObject2);
    return localObject2;
    return x.a;
    label274:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.q.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */