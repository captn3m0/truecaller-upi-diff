package com.truecaller.utils.extensions;

import android.support.v4.graphics.a;

public final class f
{
  public static final double a(int paramInt)
  {
    double[] arrayOfDouble = new double[3];
    a.a(paramInt, arrayOfDouble);
    return arrayOfDouble[0];
  }
  
  public static final int b(int paramInt)
  {
    double[] arrayOfDouble = new double[3];
    a.a(paramInt, arrayOfDouble);
    double d1 = arrayOfDouble[0] * 0.9D;
    arrayOfDouble[0] = d1;
    double d2 = arrayOfDouble[0];
    double d3 = arrayOfDouble[1];
    double d4 = arrayOfDouble[2];
    return a.a(d2, d3, d4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */