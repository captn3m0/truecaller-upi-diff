package com.truecaller.utils.extensions;

import android.text.Editable;
import android.text.TextWatcher;
import c.g.a.b;

public final class p$a
  implements TextWatcher
{
  p$a(b paramb) {}
  
  public final void afterTextChanged(Editable paramEditable)
  {
    a.invoke(paramEditable);
  }
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.p.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */