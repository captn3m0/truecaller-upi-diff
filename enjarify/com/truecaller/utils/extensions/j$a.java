package com.truecaller.utils.extensions;

import c.d.a.a;
import c.d.c;
import c.g.a.b;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.a.e;
import kotlinx.coroutines.a.j;
import kotlinx.coroutines.a.l;

final class j$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private e d;
  
  j$a(b paramb, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/utils/extensions/j$a;
    b localb = c;
    locala.<init>(localb, paramc);
    paramObject = (e)paramObject;
    d = ((e)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    Object localObject2;
    int j;
    Object localObject3;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      localObject2 = (l)a;
      j = paramObject instanceof o.b;
      if (j == 0)
      {
        localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = this;
      }
      else
      {
        throw a;
      }
      break;
    case 1: 
      localObject2 = (l)a;
      j = paramObject instanceof o.b;
      if (j == 0)
      {
        localObject3 = localObject1;
        localObject1 = this;
      }
      else
      {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label286;
      }
      paramObject = d.b().f();
      localObject2 = localObject1;
      localObject1 = this;
    }
    for (;;)
    {
      a = paramObject;
      j = 1;
      b = j;
      localObject3 = ((l)paramObject).a((c)localObject1);
      if (localObject3 == localObject2) {
        return localObject2;
      }
      Object localObject4 = localObject2;
      localObject2 = paramObject;
      paramObject = localObject3;
      localObject3 = localObject4;
      paramObject = (Boolean)paramObject;
      boolean bool2 = ((Boolean)paramObject).booleanValue();
      if (!bool2) {
        break;
      }
      a = localObject2;
      int k = 2;
      b = k;
      paramObject = ((l)localObject2).b((c)localObject1);
      if (paramObject == localObject3) {
        return localObject3;
      }
      localObject4 = localObject3;
      localObject3 = localObject2;
      localObject2 = localObject4;
      b localb = c;
      localb.invoke(paramObject);
      paramObject = localObject3;
    }
    return x.a;
    label286:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.j.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */