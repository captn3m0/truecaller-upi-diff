package com.truecaller.utils.extensions;

import android.content.Context;
import android.os.IBinder;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import c.g.b.k;

final class t$d
  implements Runnable
{
  t$d(View paramView, boolean paramBoolean) {}
  
  public final void run()
  {
    boolean bool = b;
    if (bool)
    {
      a.requestFocus();
      localObject1 = a.getContext();
      k.a(localObject1, "context");
      localObject1 = i.e((Context)localObject1);
      localObject2 = a;
      ((InputMethodManager)localObject1).showSoftInput((View)localObject2, 1);
      return;
    }
    Object localObject1 = a.getContext();
    k.a(localObject1, "context");
    localObject1 = i.e((Context)localObject1);
    Object localObject2 = a.getWindowToken();
    ((InputMethodManager)localObject1).hideSoftInputFromWindow((IBinder)localObject2, 0);
    a.clearFocus();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.t.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */