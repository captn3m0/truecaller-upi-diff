package com.truecaller.utils.extensions;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.webkit.MimeTypeMap;
import c.g.b.k;

public final class r
{
  /* Error */
  public static final Uri a(Uri paramUri1, Context paramContext, Uri paramUri2)
  {
    // Byte code:
    //   0: aload_0
    //   1: ldc 6
    //   3: invokestatic 12	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_1
    //   7: ldc 14
    //   9: invokestatic 12	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   12: aload_2
    //   13: ldc 16
    //   15: invokestatic 12	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   18: aload_0
    //   19: invokestatic 20	com/truecaller/utils/extensions/r:a	(Landroid/net/Uri;)Lcom/truecaller/utils/extensions/Scheme;
    //   22: astore_3
    //   23: aload_3
    //   24: ifnonnull +6 -> 30
    //   27: goto +144 -> 171
    //   30: getstatic 26	com/truecaller/utils/extensions/s:c	[I
    //   33: astore 4
    //   35: aload_3
    //   36: invokevirtual 32	com/truecaller/utils/extensions/Scheme:ordinal	()I
    //   39: istore 5
    //   41: aload 4
    //   43: iload 5
    //   45: iaload
    //   46: istore 5
    //   48: iload 5
    //   50: tableswitch	default:+22->72, 1:+47->97, 2:+25->75
    //   72: goto +99 -> 171
    //   75: new 34	java/io/FileInputStream
    //   78: astore_3
    //   79: aload_0
    //   80: invokevirtual 40	android/net/Uri:getPath	()Ljava/lang/String;
    //   83: astore_0
    //   84: aload_3
    //   85: aload_0
    //   86: invokespecial 44	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
    //   89: aload_3
    //   90: checkcast 46	java/io/InputStream
    //   93: astore_3
    //   94: goto +12 -> 106
    //   97: aload_1
    //   98: invokevirtual 52	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   101: aload_0
    //   102: invokevirtual 58	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   105: astore_3
    //   106: aload_1
    //   107: invokevirtual 52	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   110: aload_2
    //   111: invokevirtual 62	android/content/ContentResolver:openOutputStream	(Landroid/net/Uri;)Ljava/io/OutputStream;
    //   114: astore_0
    //   115: aload_0
    //   116: ifnonnull +5 -> 121
    //   119: aconst_null
    //   120: areturn
    //   121: ldc 64
    //   123: astore_1
    //   124: aload_3
    //   125: aload_1
    //   126: invokestatic 66	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   129: aload_3
    //   130: aload_0
    //   131: invokestatic 71	com/truecaller/utils/extensions/m:a	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   134: pop2
    //   135: aload_0
    //   136: invokevirtual 77	java/io/OutputStream:flush	()V
    //   139: aload_0
    //   140: invokevirtual 80	java/io/OutputStream:close	()V
    //   143: aload_3
    //   144: invokevirtual 81	java/io/InputStream:close	()V
    //   147: aload_2
    //   148: areturn
    //   149: astore_1
    //   150: aload_0
    //   151: invokevirtual 80	java/io/OutputStream:close	()V
    //   154: aload_3
    //   155: invokevirtual 81	java/io/InputStream:close	()V
    //   158: aload_1
    //   159: athrow
    //   160: pop
    //   161: aload_0
    //   162: invokevirtual 80	java/io/OutputStream:close	()V
    //   165: aload_3
    //   166: invokevirtual 81	java/io/InputStream:close	()V
    //   169: aconst_null
    //   170: areturn
    //   171: aconst_null
    //   172: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	173	0	paramUri1	Uri
    //   0	173	1	paramContext	Context
    //   0	173	2	paramUri2	Uri
    //   22	144	3	localObject	Object
    //   33	9	4	arrayOfInt	int[]
    //   39	10	5	i	int
    //   160	1	6	localIOException	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   125	129	149	finally
    //   130	135	149	finally
    //   135	139	149	finally
    //   125	129	160	java/io/IOException
    //   130	135	160	java/io/IOException
    //   135	139	160	java/io/IOException
  }
  
  public static final Uri a(String paramString)
  {
    k.b(paramString, "number");
    paramString = Uri.fromParts(Scheme.TEL.getValue(), paramString, null);
    k.a(paramString, "Uri.fromParts(Scheme.TEL.value, number, null)");
    return paramString;
  }
  
  public static final Scheme a(Uri paramUri)
  {
    k.b(paramUri, "receiver$0");
    paramUri = paramUri.getScheme();
    String str = Scheme.FILE.getValue();
    boolean bool1 = k.a(paramUri, str);
    if (bool1) {
      return Scheme.FILE;
    }
    str = Scheme.CONTENT.getValue();
    bool1 = k.a(paramUri, str);
    if (bool1) {
      return Scheme.CONTENT;
    }
    str = Scheme.TEL.getValue();
    boolean bool2 = k.a(paramUri, str);
    if (bool2) {
      return Scheme.TEL;
    }
    return null;
  }
  
  /* Error */
  public static final java.io.File a(Uri paramUri, Context paramContext, String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: ldc 6
    //   3: invokestatic 12	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_1
    //   7: ldc 14
    //   9: invokestatic 12	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   12: ldc 112
    //   14: aload_2
    //   15: aconst_null
    //   16: invokestatic 117	c/f/e:a	(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    //   19: astore_2
    //   20: aload_0
    //   21: invokestatic 20	com/truecaller/utils/extensions/r:a	(Landroid/net/Uri;)Lcom/truecaller/utils/extensions/Scheme;
    //   24: astore_3
    //   25: aload_3
    //   26: ifnonnull +6 -> 32
    //   29: goto +147 -> 176
    //   32: getstatic 119	com/truecaller/utils/extensions/s:b	[I
    //   35: astore 4
    //   37: aload_3
    //   38: invokevirtual 32	com/truecaller/utils/extensions/Scheme:ordinal	()I
    //   41: istore 5
    //   43: aload 4
    //   45: iload 5
    //   47: iaload
    //   48: istore 5
    //   50: iload 5
    //   52: tableswitch	default:+24->76, 1:+49->101, 2:+27->79
    //   76: goto +100 -> 176
    //   79: new 34	java/io/FileInputStream
    //   82: astore_1
    //   83: aload_0
    //   84: invokevirtual 40	android/net/Uri:getPath	()Ljava/lang/String;
    //   87: astore_0
    //   88: aload_1
    //   89: aload_0
    //   90: invokespecial 44	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
    //   93: aload_1
    //   94: checkcast 46	java/io/InputStream
    //   97: astore_1
    //   98: goto +12 -> 110
    //   101: aload_1
    //   102: invokevirtual 52	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   105: aload_0
    //   106: invokevirtual 58	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   109: astore_1
    //   110: new 121	java/io/FileOutputStream
    //   113: astore_0
    //   114: aload_0
    //   115: aload_2
    //   116: invokespecial 124	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   119: ldc 64
    //   121: astore_3
    //   122: aload_1
    //   123: aload_3
    //   124: invokestatic 66	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   127: aload_0
    //   128: astore_3
    //   129: aload_0
    //   130: checkcast 73	java/io/OutputStream
    //   133: astore_3
    //   134: aload_1
    //   135: aload_3
    //   136: invokestatic 71	com/truecaller/utils/extensions/m:a	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   139: pop2
    //   140: aload_0
    //   141: invokevirtual 125	java/io/FileOutputStream:flush	()V
    //   144: aload_0
    //   145: invokevirtual 126	java/io/FileOutputStream:close	()V
    //   148: aload_1
    //   149: invokevirtual 81	java/io/InputStream:close	()V
    //   152: aload_2
    //   153: areturn
    //   154: astore_2
    //   155: aload_0
    //   156: invokevirtual 126	java/io/FileOutputStream:close	()V
    //   159: aload_1
    //   160: invokevirtual 81	java/io/InputStream:close	()V
    //   163: aload_2
    //   164: athrow
    //   165: pop
    //   166: aload_0
    //   167: invokevirtual 126	java/io/FileOutputStream:close	()V
    //   170: aload_1
    //   171: invokevirtual 81	java/io/InputStream:close	()V
    //   174: aconst_null
    //   175: areturn
    //   176: aconst_null
    //   177: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	178	0	paramUri	Uri
    //   0	178	1	paramContext	Context
    //   0	178	2	paramString	String
    //   24	112	3	localObject	Object
    //   35	9	4	arrayOfInt	int[]
    //   41	10	5	i	int
    //   165	1	6	localIOException	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   123	127	154	finally
    //   129	133	154	finally
    //   135	140	154	finally
    //   140	144	154	finally
    //   123	127	165	java/io/IOException
    //   129	133	165	java/io/IOException
    //   135	140	165	java/io/IOException
    //   140	144	165	java/io/IOException
  }
  
  /* Error */
  public static final Long a(Uri paramUri, Context paramContext)
  {
    // Byte code:
    //   0: aload_0
    //   1: ldc 6
    //   3: invokestatic 12	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_1
    //   7: ldc 14
    //   9: invokestatic 12	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   12: aload_0
    //   13: invokestatic 20	com/truecaller/utils/extensions/r:a	(Landroid/net/Uri;)Lcom/truecaller/utils/extensions/Scheme;
    //   16: astore_2
    //   17: aconst_null
    //   18: astore_3
    //   19: aload_2
    //   20: ifnonnull +5 -> 25
    //   23: aconst_null
    //   24: areturn
    //   25: getstatic 128	com/truecaller/utils/extensions/s:a	[I
    //   28: astore 4
    //   30: aload_2
    //   31: invokevirtual 32	com/truecaller/utils/extensions/Scheme:ordinal	()I
    //   34: istore 5
    //   36: aload 4
    //   38: iload 5
    //   40: iaload
    //   41: istore 5
    //   43: iload 5
    //   45: tableswitch	default:+23->68, 1:+145->190, 2:+25->70
    //   68: aconst_null
    //   69: areturn
    //   70: aload_1
    //   71: invokevirtual 52	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   74: astore 4
    //   76: aload 4
    //   78: aload_0
    //   79: aconst_null
    //   80: aconst_null
    //   81: aconst_null
    //   82: aconst_null
    //   83: invokevirtual 132	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   86: astore_0
    //   87: aload_0
    //   88: ifnonnull +5 -> 93
    //   91: aconst_null
    //   92: areturn
    //   93: aload_0
    //   94: astore_1
    //   95: aload_0
    //   96: checkcast 134	java/io/Closeable
    //   99: astore_1
    //   100: aload_1
    //   101: astore_2
    //   102: aload_1
    //   103: checkcast 136	android/database/Cursor
    //   106: astore_2
    //   107: ldc -118
    //   109: astore 4
    //   111: aload_2
    //   112: aload 4
    //   114: invokeinterface 142 2 0
    //   119: istore 6
    //   121: iload 6
    //   123: ifge +10 -> 133
    //   126: aload_1
    //   127: aconst_null
    //   128: invokestatic 147	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   131: aconst_null
    //   132: areturn
    //   133: aload_2
    //   134: invokeinterface 151 1 0
    //   139: istore 5
    //   141: iload 5
    //   143: ifne +8 -> 151
    //   146: aconst_null
    //   147: astore_0
    //   148: goto +19 -> 167
    //   151: aload_0
    //   152: iload 6
    //   154: invokeinterface 155 2 0
    //   159: lstore 7
    //   161: lload 7
    //   163: invokestatic 161	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   166: astore_0
    //   167: aload_1
    //   168: aconst_null
    //   169: invokestatic 147	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   172: aload_0
    //   173: areturn
    //   174: astore_0
    //   175: goto +8 -> 183
    //   178: astore_0
    //   179: aload_0
    //   180: astore_3
    //   181: aload_0
    //   182: athrow
    //   183: aload_1
    //   184: aload_3
    //   185: invokestatic 147	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   188: aload_0
    //   189: athrow
    //   190: new 163	java/io/File
    //   193: astore_1
    //   194: aload_0
    //   195: invokevirtual 40	android/net/Uri:getPath	()Ljava/lang/String;
    //   198: astore_0
    //   199: aload_1
    //   200: aload_0
    //   201: invokespecial 164	java/io/File:<init>	(Ljava/lang/String;)V
    //   204: aload_1
    //   205: invokestatic 169	com/truecaller/utils/extensions/l:a	(Ljava/io/File;)J
    //   208: lstore 9
    //   210: lconst_0
    //   211: lstore 7
    //   213: lload 9
    //   215: lload 7
    //   217: lcmp
    //   218: istore 5
    //   220: iload 5
    //   222: ifge +5 -> 227
    //   225: aconst_null
    //   226: areturn
    //   227: lload 9
    //   229: invokestatic 161	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   232: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	233	0	paramUri	Uri
    //   0	233	1	paramContext	Context
    //   16	118	2	localObject1	Object
    //   18	167	3	localUri	Uri
    //   28	85	4	localObject2	Object
    //   34	10	5	i	int
    //   139	82	5	bool	boolean
    //   119	34	6	j	int
    //   159	57	7	l1	long
    //   208	20	9	l2	long
    // Exception table:
    //   from	to	target	type
    //   181	183	174	finally
    //   102	106	178	finally
    //   112	119	178	finally
    //   133	139	178	finally
    //   152	159	178	finally
    //   161	166	178	finally
  }
  
  public static final Object b(Uri paramUri)
  {
    String str = "receiver$0";
    k.b(paramUri, str);
    int i = -1;
    try
    {
      long l = ContentUris.parseId(paramUri);
      paramUri = Long.valueOf(l);
    }
    catch (UnsupportedOperationException localUnsupportedOperationException)
    {
      paramUri = Integer.valueOf(i);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      paramUri = Integer.valueOf(i);
    }
    return paramUri;
  }
  
  public static final String b(Uri paramUri, Context paramContext)
  {
    k.b(paramUri, "receiver$0");
    k.b(paramContext, "context");
    Object localObject = Scheme.TEL.getValue();
    String str = paramUri.getScheme();
    boolean bool1 = k.a(localObject, str);
    if (bool1) {
      return paramUri.getSchemeSpecificPart();
    }
    bool1 = false;
    localObject = null;
    str = "data1";
    try
    {
      String[] arrayOfString = { str };
      ContentResolver localContentResolver = paramContext.getContentResolver();
      paramUri = localContentResolver.query(paramUri, arrayOfString, null, null, null);
      if (paramUri != null) {
        try
        {
          boolean bool2 = paramUri.moveToFirst();
          if (bool2)
          {
            bool2 = false;
            paramContext = null;
            paramContext = paramUri.getString(0);
            paramUri.close();
            return paramContext;
          }
        }
        finally
        {
          localObject = paramUri;
          break label134;
        }
      }
      if (paramUri != null) {
        paramUri.close();
      }
      return null;
    }
    finally
    {
      label134:
      if (localObject != null) {
        ((Cursor)localObject).close();
      }
    }
  }
  
  public static final String c(Uri paramUri, Context paramContext)
  {
    k.b(paramUri, "receiver$0");
    String str = "context";
    k.b(paramContext, str);
    paramContext = paramContext.getContentResolver().getType(paramUri);
    if (paramContext == null)
    {
      paramUri = MimeTypeMap.getFileExtensionFromUrl(paramUri.toString());
      if (paramUri != null) {
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(paramUri);
      }
      paramContext = null;
    }
    return paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */