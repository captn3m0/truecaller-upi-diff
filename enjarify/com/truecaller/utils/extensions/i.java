package com.truecaller.utils.extensions;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources.Theme;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v4.content.b;
import android.support.v4.content.d;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.util.TypedValue;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import c.g.b.k;
import c.u;

public final class i
{
  public static final int a(Context paramContext, int paramInt)
  {
    k.b(paramContext, "receiver$0");
    TypedValue localTypedValue = new android/util/TypedValue;
    localTypedValue.<init>();
    Resources.Theme localTheme = paramContext.getTheme();
    boolean bool = true;
    localTheme.resolveAttribute(paramInt, localTypedValue, bool);
    paramInt = resourceId;
    if (paramInt == 0) {
      return data;
    }
    paramInt = resourceId;
    return b.c(paramContext, paramInt);
  }
  
  public static final Toast a(Context paramContext)
  {
    return a(paramContext, 2131886537, null, 0, 6);
  }
  
  private static Toast a(Context paramContext, int paramInt1, CharSequence paramCharSequence, int paramInt2)
  {
    String str1 = "receiver$0";
    k.b(paramContext, str1);
    if (paramCharSequence == null)
    {
      String str2 = paramContext.getString(paramInt1);
      paramCharSequence = str2;
      paramCharSequence = (CharSequence)str2;
    }
    paramContext = Toast.makeText(paramContext, paramCharSequence, paramInt2);
    paramContext.show();
    k.a(paramContext, "Toast.makeText(this, mes…        .apply { show() }");
    return paramContext;
  }
  
  public static final void a(Context paramContext, BroadcastReceiver paramBroadcastReceiver, String... paramVarArgs)
  {
    k.b(paramContext, "receiver$0");
    k.b(paramBroadcastReceiver, "broadcastReceiver");
    k.b(paramVarArgs, "actions");
    paramContext = d.a(paramContext);
    k.a(paramContext, "LocalBroadcastManager.getInstance(this)");
    IntentFilter localIntentFilter = new android/content/IntentFilter;
    localIntentFilter.<init>();
    int i = paramVarArgs.length;
    int j = 0;
    while (j < i)
    {
      String str = paramVarArgs[j];
      localIntentFilter.addAction(str);
      j += 1;
    }
    paramContext.a(paramBroadcastReceiver, localIntentFilter);
  }
  
  public static final void a(Context paramContext, String paramString, Bundle paramBundle)
  {
    k.b(paramContext, "receiver$0");
    k.b(paramString, "action");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramString);
    localIntent.putExtras(paramBundle);
    d.a(paramContext).b(localIntent);
  }
  
  public static final TelephonyManager b(Context paramContext)
  {
    k.b(paramContext, "receiver$0");
    String str = "phone";
    paramContext = paramContext.getSystemService(str);
    if (paramContext != null) {
      return (TelephonyManager)paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.telephony.TelephonyManager");
    throw paramContext;
  }
  
  public static final TelecomManager c(Context paramContext)
  {
    k.b(paramContext, "receiver$0");
    String str = "telecom";
    paramContext = paramContext.getSystemService(str);
    if (paramContext != null) {
      return (TelecomManager)paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.telecom.TelecomManager");
    throw paramContext;
  }
  
  public static final ConnectivityManager d(Context paramContext)
  {
    k.b(paramContext, "receiver$0");
    String str = "connectivity";
    paramContext = paramContext.getSystemService(str);
    if (paramContext != null) {
      return (ConnectivityManager)paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.net.ConnectivityManager");
    throw paramContext;
  }
  
  public static final InputMethodManager e(Context paramContext)
  {
    k.b(paramContext, "receiver$0");
    String str = "input_method";
    paramContext = paramContext.getSystemService(str);
    if (paramContext != null) {
      return (InputMethodManager)paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
    throw paramContext;
  }
  
  public static final NotificationManager f(Context paramContext)
  {
    k.b(paramContext, "receiver$0");
    String str = "notification";
    paramContext = paramContext.getSystemService(str);
    if (paramContext != null) {
      return (NotificationManager)paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.app.NotificationManager");
    throw paramContext;
  }
  
  public static final PowerManager g(Context paramContext)
  {
    k.b(paramContext, "receiver$0");
    String str = "power";
    paramContext = paramContext.getSystemService(str);
    if (paramContext != null) {
      return (PowerManager)paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.os.PowerManager");
    throw paramContext;
  }
  
  public static final Vibrator h(Context paramContext)
  {
    k.b(paramContext, "receiver$0");
    String str = "vibrator";
    paramContext = paramContext.getSystemService(str);
    if (paramContext != null) {
      return (Vibrator)paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.os.Vibrator");
    throw paramContext;
  }
  
  public static final AudioManager i(Context paramContext)
  {
    k.b(paramContext, "receiver$0");
    String str = "audio";
    paramContext = paramContext.getSystemService(str);
    if (paramContext != null) {
      return (AudioManager)paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.media.AudioManager");
    throw paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */