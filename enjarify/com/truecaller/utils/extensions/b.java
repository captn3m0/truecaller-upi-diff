package com.truecaller.utils.extensions;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.view.Display;
import android.view.WindowManager;
import c.g.b.k;
import c.u;

public final class b
{
  public static final int a(Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    String str = "receiver$0";
    k.b(paramBitmap, str);
    int i = paramBitmap.getPixel(paramInt1, paramInt2);
    long l1 = i & 0xFF000000;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (!bool) {
      return -1;
    }
    return i;
  }
  
  public static final Bitmap.Config a(Bitmap paramBitmap)
  {
    String str = "receiver$0";
    k.b(paramBitmap, str);
    paramBitmap = paramBitmap.getConfig();
    if (paramBitmap == null) {
      paramBitmap = Bitmap.Config.ARGB_8888;
    }
    return paramBitmap;
  }
  
  public static final Bitmap a(Bitmap paramBitmap, int paramInt)
  {
    k.b(paramBitmap, "receiver$0");
    float f = paramInt;
    boolean bool = f < 0.0F;
    if (bool)
    {
      Matrix localMatrix = new android/graphics/Matrix;
      localMatrix.<init>();
      localMatrix.postRotate(f);
      int i = paramBitmap.getWidth();
      int j = paramBitmap.getHeight();
      paramBitmap = Bitmap.createBitmap(paramBitmap, 0, 0, i, j, localMatrix, true);
      k.a(paramBitmap, "Bitmap.createBitmap(this…his.height, matrix, true)");
      return paramBitmap;
    }
    return paramBitmap;
  }
  
  public static final Bitmap a(Bitmap paramBitmap, Context paramContext, int paramInt)
  {
    k.b(paramBitmap, "receiver$0");
    k.b(paramContext, "context");
    Object localObject = "window";
    paramContext = paramContext.getSystemService((String)localObject);
    if (paramContext != null)
    {
      paramContext = ((WindowManager)paramContext).getDefaultDisplay();
      localObject = new android/graphics/Point;
      ((Point)localObject).<init>();
      paramContext.getSize((Point)localObject);
      int i = x;
      int j = y;
      i = Math.min(Math.min(i, j), paramInt);
      paramInt = paramBitmap.getWidth();
      if (paramInt <= i)
      {
        paramInt = paramBitmap.getHeight();
        if (paramInt <= i) {
          return paramBitmap;
        }
      }
      double d1 = paramBitmap.getWidth();
      double d2 = paramBitmap.getHeight();
      Double.isNaN(d1);
      Double.isNaN(d2);
      d1 /= d2;
      paramInt = paramBitmap.getWidth();
      int k = paramBitmap.getHeight();
      if (paramInt > k)
      {
        d2 = i;
        Double.isNaN(d2);
        d2 /= d1;
        paramInt = (int)d2;
      }
      else
      {
        d2 = i;
        Double.isNaN(d2);
        d2 *= d1;
        int m = (int)d2;
        paramInt = i;
        i = m;
      }
      paramBitmap = Bitmap.createScaledBitmap(paramBitmap, i, paramInt, false);
      k.a(paramBitmap, "Bitmap.createScaledBitma…wWidth, newHeight, false)");
      return paramBitmap;
    }
    paramBitmap = new c/u;
    paramBitmap.<init>("null cannot be cast to non-null type android.view.WindowManager");
    throw paramBitmap;
  }
  
  public static final Object a(Bitmap paramBitmap, c.g.a.b paramb)
  {
    String str = "block";
    k.b(paramb, str);
    try
    {
      paramb = paramb.invoke(paramBitmap);
      return paramb;
    }
    finally
    {
      if (paramBitmap != null) {
        paramBitmap.recycle();
      }
    }
  }
  
  public static final Bitmap b(Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    k.b(paramBitmap, "receiver$0");
    int i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    float f1 = paramInt2;
    float f2 = i;
    float f3 = f1 / f2;
    float f4 = paramInt1;
    float f5 = j;
    float f6 = f4 / f5;
    f3 = Math.max(f3, f6);
    f2 *= f3;
    f3 *= f5;
    f1 -= f2;
    f5 = 2.0F;
    f1 /= f5;
    f4 = (f4 - f3) / f5;
    RectF localRectF = new android/graphics/RectF;
    f2 += f1;
    f3 += f4;
    localRectF.<init>(f1, f4, f2, f3);
    Bitmap.Config localConfig = a(paramBitmap);
    Bitmap localBitmap = Bitmap.createBitmap(paramInt2, paramInt1, localConfig);
    Canvas localCanvas = new android/graphics/Canvas;
    localCanvas.<init>(localBitmap);
    localCanvas.drawBitmap(paramBitmap, null, localRectF, null);
    k.a(localBitmap, "dest");
    return localBitmap;
  }
  
  private static Bitmap c(Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    String str = "receiver$0";
    k.b(paramBitmap, str);
    int i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    float f1;
    float f2;
    if (i > j)
    {
      f1 = i;
      f2 = paramInt1;
      f1 /= f2;
      f2 = j / f1;
      paramInt2 = (int)f2;
    }
    else
    {
      float f3;
      if (j > i)
      {
        f3 = j;
        float f4 = paramInt2;
        f3 /= f4;
        f2 = i / f3;
        paramInt1 = (int)f2;
      }
      else
      {
        f3 = j;
        f1 = paramInt2;
        f1 = f3 / f1;
        f3 /= f1;
        paramInt1 = (int)f3;
        f2 = i / f1;
        int k = (int)f2;
        paramInt2 = paramInt1;
        paramInt1 = k;
      }
    }
    if ((paramInt1 != 0) && (paramInt2 != 0)) {
      return Bitmap.createScaledBitmap(paramBitmap, paramInt1, paramInt2, true);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */