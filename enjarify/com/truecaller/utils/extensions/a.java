package com.truecaller.utils.extensions;

import android.app.Activity;
import android.app.KeyguardManager;
import android.os.Build.VERSION;
import android.view.Window;
import c.g.b.k;
import c.u;

public final class a
{
  public static final void a(Activity paramActivity)
  {
    String str = "receiver$0";
    k.b(paramActivity, str);
    int i = Build.VERSION.SDK_INT;
    int j = 26;
    if (i == j) {
      return;
    }
    paramActivity.setRequestedOrientation(1);
  }
  
  public static final void a(Activity paramActivity, boolean paramBoolean)
  {
    String str = "receiver$0";
    k.b(paramActivity, str);
    int i = Build.VERSION.SDK_INT;
    int k = 27;
    if (i >= k)
    {
      i = 1;
      paramActivity.setShowWhenLocked(i);
      paramActivity.setTurnScreenOn(i);
      if (paramBoolean)
      {
        Object localObject = paramActivity.getSystemService("keyguard");
        if (localObject != null)
        {
          localObject = (KeyguardManager)localObject;
          int j = 0;
          str = null;
          ((KeyguardManager)localObject).requestDismissKeyguard(paramActivity, null);
        }
        else
        {
          paramActivity = new c/u;
          paramActivity.<init>("null cannot be cast to non-null type android.app.KeyguardManager");
          throw paramActivity;
        }
      }
      paramBoolean = true;
    }
    else
    {
      paramBoolean = 6815872;
    }
    paramActivity.getWindow().addFlags(paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */