package com.truecaller.utils.extensions;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.a.l;
import kotlinx.coroutines.a.u;
import kotlinx.coroutines.ag;

final class j$b
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  int c;
  private ag f;
  
  j$b(u paramu, m paramm, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/utils/extensions/j$b;
    u localu = d;
    m localm = e;
    localb.<init>(localu, localm, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = c;
    l locall;
    int j;
    Object localObject2;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 3: 
      locall = (l)b;
      j = paramObject instanceof o.b;
      if (j == 0) {
        paramObject = this;
      } else {
        throw a;
      }
      break;
    case 2: 
      locall = (l)a;
      j = paramObject instanceof o.b;
      if (j == 0)
      {
        localObject2 = localObject1;
        localObject1 = this;
      }
      else
      {
        throw a;
      }
      break;
    case 1: 
      locall = (l)a;
      j = paramObject instanceof o.b;
      if (j == 0)
      {
        localObject2 = localObject1;
        localObject1 = this;
      }
      else
      {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label330;
      }
      locall = d.f();
      paramObject = this;
    }
    for (;;)
    {
      a = locall;
      j = 1;
      c = j;
      localObject2 = locall.a((c)paramObject);
      if (localObject2 == localObject1) {
        return localObject1;
      }
      Object localObject3 = localObject1;
      localObject1 = paramObject;
      paramObject = localObject2;
      localObject2 = localObject3;
      paramObject = (Boolean)paramObject;
      boolean bool2 = ((Boolean)paramObject).booleanValue();
      if (!bool2) {
        break;
      }
      a = locall;
      int k = 2;
      c = k;
      paramObject = locall.b((c)localObject1);
      if (paramObject == localObject2) {
        return localObject2;
      }
      m localm = e;
      a = paramObject;
      b = locall;
      int m = 3;
      c = m;
      paramObject = localm.invoke(paramObject, localObject1);
      if (paramObject == localObject2) {
        return localObject2;
      }
      paramObject = localObject1;
      localObject1 = localObject2;
    }
    return x.a;
    label330:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.extensions.j.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */