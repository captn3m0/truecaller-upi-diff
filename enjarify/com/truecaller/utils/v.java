package com.truecaller.utils;

import android.content.Context;
import c.g.b.k;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class v
  implements d
{
  private final Provider a;
  
  public static String a(Context paramContext)
  {
    k.b(paramContext, "context");
    return (String)g.a(paramContext.getPackageName(), "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */