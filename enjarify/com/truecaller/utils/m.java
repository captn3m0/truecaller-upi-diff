package com.truecaller.utils;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Build.VERSION;
import android.provider.Settings;
import android.support.v4.app.ac;
import android.support.v4.content.b;
import c.a.f;
import c.g.b.k;
import c.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class m
  implements l
{
  private final Context a;
  
  public m(Context paramContext)
  {
    a = paramContext;
  }
  
  public final boolean a()
  {
    int i = Build.VERSION.SDK_INT;
    boolean bool2 = true;
    int j = 23;
    Context localContext;
    if (i < j)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localContext = null;
    }
    if (i == 0)
    {
      localContext = a;
      boolean bool1 = Settings.canDrawOverlays(localContext);
      if (!bool1) {
        return false;
      }
    }
    return bool2;
  }
  
  public final boolean a(String... paramVarArgs)
  {
    k.b(paramVarArgs, "permissions");
    boolean bool = false;
    try
    {
      int i = paramVarArgs.length;
      int j = 0;
      while (j < i)
      {
        str = paramVarArgs[j];
        Context localContext = a;
        int k = b.a(localContext, str);
        if (k != 0)
        {
          k = 1;
        }
        else
        {
          k = 0;
          localContext = null;
        }
        if (k != 0) {
          break label78;
        }
        j += 1;
      }
      String str = null;
      label78:
      if (str == null) {
        bool = true;
      }
    }
    catch (RuntimeException localRuntimeException)
    {
      for (;;) {}
    }
    return bool;
  }
  
  public final boolean a(String[] paramArrayOfString1, int[] paramArrayOfInt, String... paramVarArgs)
  {
    k.b(paramArrayOfString1, "permissions");
    k.b(paramArrayOfInt, "grantResults");
    Object localObject1 = "desiredPermissions";
    k.b(paramVarArgs, (String)localObject1);
    paramArrayOfInt = (Iterable)f.a(paramArrayOfInt);
    paramArrayOfString1 = (Iterable)f.a(paramArrayOfString1, paramArrayOfInt);
    paramArrayOfInt = new java/util/ArrayList;
    paramArrayOfInt.<init>();
    paramArrayOfInt = (Collection)paramArrayOfInt;
    paramArrayOfString1 = paramArrayOfString1.iterator();
    for (;;)
    {
      boolean bool1 = paramArrayOfString1.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = paramArrayOfString1.next();
      Object localObject2 = localObject1;
      localObject2 = (Number)b;
      int j = ((Number)localObject2).intValue();
      if (j == 0)
      {
        j = 1;
      }
      else
      {
        j = 0;
        localObject2 = null;
      }
      if (j != 0) {
        paramArrayOfInt.add(localObject1);
      }
    }
    paramArrayOfInt = (Iterable)paramArrayOfInt;
    paramArrayOfString1 = new java/util/ArrayList;
    int i = c.a.m.a(paramArrayOfInt, 10);
    paramArrayOfString1.<init>(i);
    paramArrayOfString1 = (Collection)paramArrayOfString1;
    paramArrayOfInt = paramArrayOfInt.iterator();
    for (;;)
    {
      boolean bool2 = paramArrayOfInt.hasNext();
      if (!bool2) {
        break;
      }
      localObject1 = (String)nexta;
      paramArrayOfString1.add(localObject1);
    }
    paramArrayOfString1 = (List)paramArrayOfString1;
    paramArrayOfInt = (Collection)f.a(paramVarArgs);
    return paramArrayOfString1.containsAll(paramArrayOfInt);
  }
  
  public final boolean b()
  {
    String[] arrayOfString = { "android.permission.READ_EXTERNAL_STORAGE" };
    return a(arrayOfString);
  }
  
  public final boolean c()
  {
    String[] arrayOfString = { "android.permission.WRITE_EXTERNAL_STORAGE" };
    return a(arrayOfString);
  }
  
  public final boolean d()
  {
    Object localObject1 = ac.b(a);
    Object localObject2 = "NotificationManagerCompa…ListenerPackages(context)";
    k.a(localObject1, (String)localObject2);
    localObject1 = ((Iterable)localObject1).iterator();
    boolean bool2;
    do
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = ((Iterator)localObject1).next();
      Object localObject3 = localObject2;
      localObject3 = (String)localObject2;
      String str = a.getPackageName();
      bool2 = k.a(localObject3, str);
    } while (!bool2);
    break label83;
    boolean bool1 = false;
    localObject2 = null;
    label83:
    localObject2 = (String)localObject2;
    return localObject2 != null;
  }
  
  public final boolean e()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 24;
    if (i < j) {
      return true;
    }
    Object localObject = a;
    String str = "notification";
    localObject = ((Context)localObject).getSystemService(str);
    boolean bool = localObject instanceof NotificationManager;
    if (!bool)
    {
      i = 0;
      localObject = null;
    }
    localObject = (NotificationManager)localObject;
    if (localObject != null) {
      return ((NotificationManager)localObject).isNotificationPolicyAccessGranted();
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */