package com.truecaller.utils;

import android.content.Context;

public final class c
  implements t
{
  private final Context a;
  private final u b;
  
  private c(u paramu, Context paramContext)
  {
    a = paramContext;
    b = paramu;
  }
  
  public static t.a a()
  {
    c.a locala = new com/truecaller/utils/c$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  private m h()
  {
    m localm = new com/truecaller/utils/m;
    Context localContext = a;
    localm.<init>(localContext);
    return localm;
  }
  
  public final l b()
  {
    return h();
  }
  
  public final d c()
  {
    e locale = new com/truecaller/utils/e;
    Context localContext = a;
    m localm = h();
    String str = v.a(a);
    locale.<init>(localContext, localm, str);
    return locale;
  }
  
  public final a d()
  {
    b localb = new com/truecaller/utils/b;
    localb.<init>();
    return localb;
  }
  
  public final s e()
  {
    g localg = new com/truecaller/utils/g;
    localg.<init>();
    return localg;
  }
  
  public final i f()
  {
    j localj = new com/truecaller/utils/j;
    Context localContext = a;
    localj.<init>(localContext);
    return localj;
  }
  
  public final n g()
  {
    p localp = new com/truecaller/utils/p;
    Context localContext = a;
    localp.<init>(localContext);
    return localp;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */