package com.truecaller.utils;

import c.g.b.k;
import com.google.firebase.perf.metrics.Trace;

public final class g$a
  implements q
{
  private final Trace a;
  
  public g$a(Trace paramTrace)
  {
    a = paramTrace;
  }
  
  public final void a()
  {
    a.stop();
  }
  
  public final void a(String paramString, int paramInt)
  {
    k.b(paramString, "counter");
    Trace localTrace = a;
    long l = paramInt;
    localTrace.incrementCounter(paramString, l);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.g.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */