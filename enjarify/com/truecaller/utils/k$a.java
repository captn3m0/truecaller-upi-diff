package com.truecaller.utils;

import c.g.b.k;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.nio.channels.SocketChannel;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

class k$a
  extends SSLSocket
{
  final SSLSocket a;
  
  public k$a(SSLSocket paramSSLSocket)
  {
    a = paramSSLSocket;
  }
  
  public void addHandshakeCompletedListener(HandshakeCompletedListener paramHandshakeCompletedListener)
  {
    k.b(paramHandshakeCompletedListener, "listener");
    a.addHandshakeCompletedListener(paramHandshakeCompletedListener);
  }
  
  public void bind(SocketAddress paramSocketAddress)
  {
    k.b(paramSocketAddress, "localAddr");
    a.bind(paramSocketAddress);
  }
  
  public void close()
  {
    try
    {
      SSLSocket localSSLSocket = a;
      localSSLSocket.close();
      return;
    }
    finally {}
  }
  
  public void connect(SocketAddress paramSocketAddress)
  {
    k.b(paramSocketAddress, "remoteAddr");
    a.connect(paramSocketAddress);
  }
  
  public void connect(SocketAddress paramSocketAddress, int paramInt)
  {
    k.b(paramSocketAddress, "remoteAddr");
    a.connect(paramSocketAddress, paramInt);
  }
  
  public boolean equals(Object paramObject)
  {
    return k.a(a, paramObject);
  }
  
  public SocketChannel getChannel()
  {
    SocketChannel localSocketChannel = a.getChannel();
    k.a(localSocketChannel, "delegate.channel");
    return localSocketChannel;
  }
  
  public boolean getEnableSessionCreation()
  {
    return a.getEnableSessionCreation();
  }
  
  public String[] getEnabledCipherSuites()
  {
    String[] arrayOfString = a.getEnabledCipherSuites();
    k.a(arrayOfString, "delegate.enabledCipherSuites");
    return arrayOfString;
  }
  
  public String[] getEnabledProtocols()
  {
    String[] arrayOfString = a.getEnabledProtocols();
    k.a(arrayOfString, "delegate.enabledProtocols");
    return arrayOfString;
  }
  
  public InetAddress getInetAddress()
  {
    InetAddress localInetAddress = a.getInetAddress();
    k.a(localInetAddress, "delegate.inetAddress");
    return localInetAddress;
  }
  
  public InputStream getInputStream()
  {
    InputStream localInputStream = a.getInputStream();
    k.a(localInputStream, "delegate.inputStream");
    return localInputStream;
  }
  
  public boolean getKeepAlive()
  {
    return a.getKeepAlive();
  }
  
  public InetAddress getLocalAddress()
  {
    InetAddress localInetAddress = a.getLocalAddress();
    k.a(localInetAddress, "delegate.localAddress");
    return localInetAddress;
  }
  
  public int getLocalPort()
  {
    return a.getLocalPort();
  }
  
  public SocketAddress getLocalSocketAddress()
  {
    SocketAddress localSocketAddress = a.getLocalSocketAddress();
    k.a(localSocketAddress, "delegate.localSocketAddress");
    return localSocketAddress;
  }
  
  public boolean getNeedClientAuth()
  {
    return a.getNeedClientAuth();
  }
  
  public boolean getOOBInline()
  {
    return a.getOOBInline();
  }
  
  public OutputStream getOutputStream()
  {
    OutputStream localOutputStream = a.getOutputStream();
    k.a(localOutputStream, "delegate.outputStream");
    return localOutputStream;
  }
  
  public int getPort()
  {
    return a.getPort();
  }
  
  public int getReceiveBufferSize()
  {
    try
    {
      SSLSocket localSSLSocket = a;
      int i = localSSLSocket.getReceiveBufferSize();
      return i;
    }
    finally {}
  }
  
  public SocketAddress getRemoteSocketAddress()
  {
    SocketAddress localSocketAddress = a.getRemoteSocketAddress();
    k.a(localSocketAddress, "delegate.remoteSocketAddress");
    return localSocketAddress;
  }
  
  public boolean getReuseAddress()
  {
    return a.getReuseAddress();
  }
  
  public int getSendBufferSize()
  {
    try
    {
      SSLSocket localSSLSocket = a;
      int i = localSSLSocket.getSendBufferSize();
      return i;
    }
    finally {}
  }
  
  public SSLSession getSession()
  {
    SSLSession localSSLSession = a.getSession();
    k.a(localSSLSession, "delegate.session");
    return localSSLSession;
  }
  
  public int getSoLinger()
  {
    return a.getSoLinger();
  }
  
  public int getSoTimeout()
  {
    try
    {
      SSLSocket localSSLSocket = a;
      int i = localSSLSocket.getSoTimeout();
      return i;
    }
    finally {}
  }
  
  public String[] getSupportedCipherSuites()
  {
    String[] arrayOfString = a.getSupportedCipherSuites();
    k.a(arrayOfString, "delegate.supportedCipherSuites");
    return arrayOfString;
  }
  
  public String[] getSupportedProtocols()
  {
    String[] arrayOfString = a.getSupportedProtocols();
    k.a(arrayOfString, "delegate.supportedProtocols");
    return arrayOfString;
  }
  
  public boolean getTcpNoDelay()
  {
    return a.getTcpNoDelay();
  }
  
  public int getTrafficClass()
  {
    return a.getTrafficClass();
  }
  
  public boolean getUseClientMode()
  {
    return a.getUseClientMode();
  }
  
  public boolean getWantClientAuth()
  {
    return a.getWantClientAuth();
  }
  
  public int hashCode()
  {
    return a.hashCode();
  }
  
  public boolean isBound()
  {
    return a.isBound();
  }
  
  public boolean isClosed()
  {
    return a.isClosed();
  }
  
  public boolean isConnected()
  {
    return a.isConnected();
  }
  
  public boolean isInputShutdown()
  {
    return a.isInputShutdown();
  }
  
  public boolean isOutputShutdown()
  {
    return a.isOutputShutdown();
  }
  
  public void removeHandshakeCompletedListener(HandshakeCompletedListener paramHandshakeCompletedListener)
  {
    k.b(paramHandshakeCompletedListener, "listener");
    a.removeHandshakeCompletedListener(paramHandshakeCompletedListener);
  }
  
  public void sendUrgentData(int paramInt)
  {
    a.sendUrgentData(paramInt);
  }
  
  public void setEnableSessionCreation(boolean paramBoolean)
  {
    a.setEnableSessionCreation(paramBoolean);
  }
  
  public void setEnabledCipherSuites(String[] paramArrayOfString)
  {
    k.b(paramArrayOfString, "suites");
    a.setEnabledCipherSuites(paramArrayOfString);
  }
  
  public void setEnabledProtocols(String[] paramArrayOfString)
  {
    a.setEnabledProtocols(paramArrayOfString);
  }
  
  public void setKeepAlive(boolean paramBoolean)
  {
    a.setKeepAlive(paramBoolean);
  }
  
  public void setNeedClientAuth(boolean paramBoolean)
  {
    a.setNeedClientAuth(paramBoolean);
  }
  
  public void setOOBInline(boolean paramBoolean)
  {
    a.setOOBInline(paramBoolean);
  }
  
  public void setPerformancePreferences(int paramInt1, int paramInt2, int paramInt3)
  {
    a.setPerformancePreferences(paramInt1, paramInt2, paramInt3);
  }
  
  public void setReceiveBufferSize(int paramInt)
  {
    try
    {
      SSLSocket localSSLSocket = a;
      localSSLSocket.setReceiveBufferSize(paramInt);
      return;
    }
    finally {}
  }
  
  public void setReuseAddress(boolean paramBoolean)
  {
    a.setReuseAddress(paramBoolean);
  }
  
  public void setSSLParameters(SSLParameters paramSSLParameters)
  {
    k.b(paramSSLParameters, "p");
    a.setSSLParameters(paramSSLParameters);
  }
  
  public void setSendBufferSize(int paramInt)
  {
    try
    {
      SSLSocket localSSLSocket = a;
      localSSLSocket.setSendBufferSize(paramInt);
      return;
    }
    finally {}
  }
  
  public void setSoLinger(boolean paramBoolean, int paramInt)
  {
    a.setSoLinger(paramBoolean, paramInt);
  }
  
  public void setSoTimeout(int paramInt)
  {
    try
    {
      SSLSocket localSSLSocket = a;
      localSSLSocket.setSoTimeout(paramInt);
      return;
    }
    finally {}
  }
  
  public void setTcpNoDelay(boolean paramBoolean)
  {
    a.setTcpNoDelay(paramBoolean);
  }
  
  public void setTrafficClass(int paramInt)
  {
    a.setTrafficClass(paramInt);
  }
  
  public void setUseClientMode(boolean paramBoolean)
  {
    a.setUseClientMode(paramBoolean);
  }
  
  public void setWantClientAuth(boolean paramBoolean)
  {
    a.setWantClientAuth(paramBoolean);
  }
  
  public void shutdownInput()
  {
    a.shutdownInput();
  }
  
  public void shutdownOutput()
  {
    a.shutdownOutput();
  }
  
  public void startHandshake()
  {
    a.startHandshake();
  }
  
  public String toString()
  {
    String str = a.toString();
    k.a(str, "delegate.toString()");
    return str;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.k.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */