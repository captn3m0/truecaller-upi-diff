package com.truecaller.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.text.Html;
import android.text.Spanned;
import c.g.b.k;
import com.truecaller.utils.ui.b;
import java.util.Arrays;

public final class p
  implements o
{
  private final Context a;
  
  public p(Context paramContext)
  {
    a = paramContext;
  }
  
  public final Drawable a(int paramInt1, int paramInt2)
  {
    Drawable localDrawable = b.a(a, paramInt1, paramInt2);
    k.a(localDrawable, "ThemeUtils.getTintedDraw…, drawableRes, colorAttr)");
    return localDrawable;
  }
  
  public final String a(int paramInt1, int paramInt2, Object... paramVarArgs)
  {
    k.b(paramVarArgs, "formatArgs");
    Resources localResources = a.getResources();
    int i = paramVarArgs.length;
    paramVarArgs = Arrays.copyOf(paramVarArgs, i);
    String str = localResources.getQuantityString(paramInt1, paramInt2, paramVarArgs);
    k.a(str, "context.resources.getQua…d, quantity, *formatArgs)");
    return str;
  }
  
  public final String a(int paramInt, Object... paramVarArgs)
  {
    k.b(paramVarArgs, "formatArgs");
    Context localContext = a;
    int i = paramVarArgs.length;
    paramVarArgs = Arrays.copyOf(paramVarArgs, i);
    String str = localContext.getString(paramInt, paramVarArgs);
    k.a(str, "context.getString(resId, *formatArgs)");
    return str;
  }
  
  public final String[] a(int paramInt)
  {
    String[] arrayOfString = a.getResources().getStringArray(paramInt);
    k.a(arrayOfString, "context.resources.getStringArray(resId)");
    return arrayOfString;
  }
  
  public final int b(int paramInt)
  {
    return a.getResources().getDimensionPixelSize(paramInt);
  }
  
  public final CharSequence b(int paramInt, Object... paramVarArgs)
  {
    String str = "formatArgs";
    k.b(paramVarArgs, str);
    int i = Build.VERSION.SDK_INT;
    int j = 24;
    if (i >= j)
    {
      i = paramVarArgs.length;
      paramVarArgs = Arrays.copyOf(paramVarArgs, i);
      localSpanned = Html.fromHtml(a(paramInt, paramVarArgs), 0);
      k.a(localSpanned, "Html.fromHtml(getString(…AGRAPH_LINES_CONSECUTIVE)");
      return (CharSequence)localSpanned;
    }
    i = paramVarArgs.length;
    paramVarArgs = Arrays.copyOf(paramVarArgs, i);
    Spanned localSpanned = Html.fromHtml(a(paramInt, paramVarArgs));
    k.a(localSpanned, "Html.fromHtml(getString(resId, *formatArgs))");
    return (CharSequence)localSpanned;
  }
  
  public final Drawable c(int paramInt)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i >= j)
    {
      localDrawable = a.getDrawable(paramInt);
      k.a(localDrawable, "context.getDrawable(drawableRes)");
      return localDrawable;
    }
    Drawable localDrawable = a.getResources().getDrawable(paramInt);
    k.a(localDrawable, "context.resources.getDrawable(drawableRes)");
    return localDrawable;
  }
  
  public final int d(int paramInt)
  {
    return a.getResources().getColor(paramInt);
  }
  
  public final int e(int paramInt)
  {
    return b.a(a, paramInt);
  }
  
  public final Drawable f(int paramInt)
  {
    return b.c(a, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */