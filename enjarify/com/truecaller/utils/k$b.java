package com.truecaller.utils;

import c.g.b.k;
import c.u;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import javax.net.ssl.SSLSocket;

final class k$b
  extends k.a
{
  public k$b(SSLSocket paramSSLSocket)
  {
    super(paramSSLSocket);
    Object localObject1 = paramSSLSocket.getClass().getCanonicalName();
    Object localObject2 = "org.apache.harmony.xnet.provider.jsse.OpenSSLSocketImpl";
    boolean bool1 = k.a(localObject1, localObject2);
    boolean bool2 = true;
    bool1 ^= bool2;
    if (bool1) {
      try
      {
        localObject1 = paramSSLSocket.getClass();
        Object localObject3 = "setUseSessionTickets";
        Class[] arrayOfClass = new Class[bool2];
        Class localClass = Boolean.TYPE;
        if (localClass == null) {
          k.a();
        }
        arrayOfClass[0] = localClass;
        localObject1 = ((Class)localObject1).getMethod((String)localObject3, arrayOfClass);
        localObject2 = new Object[bool2];
        localObject3 = Boolean.TRUE;
        localObject2[0] = localObject3;
        ((Method)localObject1).invoke(paramSSLSocket, (Object[])localObject2);
        return;
      }
      catch (InvocationTargetException localInvocationTargetException) {}catch (IllegalAccessException localIllegalAccessException) {}catch (NoSuchMethodException localNoSuchMethodException) {}
    }
  }
  
  public final void setEnabledProtocols(String[] paramArrayOfString)
  {
    if (paramArrayOfString != null)
    {
      paramArrayOfString = new java/util/ArrayList;
      Object localObject = a.getSupportedProtocols();
      int i = localObject.length;
      localObject = (Collection)Arrays.asList((String[])Arrays.copyOf((Object[])localObject, i));
      paramArrayOfString.<init>((Collection)localObject);
      int j = paramArrayOfString.size();
      i = 1;
      if (j > i)
      {
        localObject = "SSLv3";
        paramArrayOfString.remove(localObject);
      }
      paramArrayOfString = (Collection)paramArrayOfString;
      j = 0;
      localObject = new String[0];
      paramArrayOfString = paramArrayOfString.toArray((Object[])localObject);
      if (paramArrayOfString != null)
      {
        paramArrayOfString = (String[])paramArrayOfString;
      }
      else
      {
        paramArrayOfString = new c/u;
        paramArrayOfString.<init>("null cannot be cast to non-null type kotlin.Array<T>");
        throw paramArrayOfString;
      }
    }
    super.setEnabledProtocols(paramArrayOfString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.k.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */