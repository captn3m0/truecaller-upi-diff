package com.truecaller.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public final class h
{
  private static Boolean a;
  
  /* Error */
  public static boolean a()
  {
    // Byte code:
    //   0: getstatic 8	com/truecaller/utils/h:a	Ljava/lang/Boolean;
    //   3: astore_0
    //   4: aload_0
    //   5: ifnonnull +186 -> 191
    //   8: getstatic 14	android/os/Build:MANUFACTURER	Ljava/lang/String;
    //   11: astore_0
    //   12: ldc 16
    //   14: astore_1
    //   15: aload_0
    //   16: aload_1
    //   17: invokevirtual 22	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   20: istore_2
    //   21: iload_2
    //   22: ifeq +122 -> 144
    //   25: ldc 24
    //   27: astore_0
    //   28: aload_0
    //   29: invokestatic 30	java/lang/Class:forName	(Ljava/lang/String;)Ljava/lang/Class;
    //   32: astore_0
    //   33: ldc 32
    //   35: astore_1
    //   36: iconst_2
    //   37: istore_3
    //   38: iload_3
    //   39: anewarray 26	java/lang/Class
    //   42: astore 4
    //   44: ldc 18
    //   46: astore 5
    //   48: aload 4
    //   50: iconst_0
    //   51: aload 5
    //   53: aastore
    //   54: getstatic 39	java/lang/Integer:TYPE	Ljava/lang/Class;
    //   57: astore 5
    //   59: iconst_1
    //   60: istore 6
    //   62: aload 4
    //   64: iload 6
    //   66: aload 5
    //   68: aastore
    //   69: aload_0
    //   70: aload_1
    //   71: aload 4
    //   73: invokevirtual 44	java/lang/Class:getMethod	(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    //   76: astore_0
    //   77: aconst_null
    //   78: astore_1
    //   79: iload_3
    //   80: anewarray 4	java/lang/Object
    //   83: astore 7
    //   85: ldc 46
    //   87: astore 4
    //   89: aload 7
    //   91: iconst_0
    //   92: aload 4
    //   94: aastore
    //   95: iconst_m1
    //   96: istore 8
    //   98: iload 8
    //   100: invokestatic 50	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   103: astore 5
    //   105: aload 7
    //   107: iload 6
    //   109: aload 5
    //   111: aastore
    //   112: aload_0
    //   113: aconst_null
    //   114: aload 7
    //   116: invokevirtual 56	java/lang/reflect/Method:invoke	(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    //   119: astore_0
    //   120: iload 8
    //   122: invokestatic 50	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   125: astore_1
    //   126: aload_0
    //   127: aload_1
    //   128: invokevirtual 60	java/lang/Object:equals	(Ljava/lang/Object;)Z
    //   131: istore_2
    //   132: iload_2
    //   133: ifne +11 -> 144
    //   136: getstatic 65	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   139: astore_0
    //   140: aload_0
    //   141: putstatic 8	com/truecaller/utils/h:a	Ljava/lang/Boolean;
    //   144: getstatic 8	com/truecaller/utils/h:a	Ljava/lang/Boolean;
    //   147: astore_0
    //   148: aload_0
    //   149: ifnonnull +42 -> 191
    //   152: goto +31 -> 183
    //   155: astore_0
    //   156: getstatic 8	com/truecaller/utils/h:a	Ljava/lang/Boolean;
    //   159: astore_1
    //   160: aload_1
    //   161: ifnonnull +11 -> 172
    //   164: getstatic 68	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   167: astore_1
    //   168: aload_1
    //   169: putstatic 8	com/truecaller/utils/h:a	Ljava/lang/Boolean;
    //   172: aload_0
    //   173: athrow
    //   174: pop
    //   175: getstatic 8	com/truecaller/utils/h:a	Ljava/lang/Boolean;
    //   178: astore_0
    //   179: aload_0
    //   180: ifnonnull +11 -> 191
    //   183: getstatic 68	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   186: astore_0
    //   187: aload_0
    //   188: putstatic 8	com/truecaller/utils/h:a	Ljava/lang/Boolean;
    //   191: getstatic 8	com/truecaller/utils/h:a	Ljava/lang/Boolean;
    //   194: invokevirtual 72	java/lang/Boolean:booleanValue	()Z
    //   197: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   3	146	0	localObject1	Object
    //   155	18	0	localObject2	Object
    //   178	10	0	localBoolean	Boolean
    //   14	155	1	localObject3	Object
    //   20	113	2	bool	boolean
    //   37	43	3	i	int
    //   42	51	4	localObject4	Object
    //   46	64	5	localObject5	Object
    //   60	48	6	j	int
    //   83	32	7	arrayOfObject	Object[]
    //   96	25	8	k	int
    //   174	1	11	localClassNotFoundException	ClassNotFoundException
    // Exception table:
    //   from	to	target	type
    //   8	11	155	finally
    //   16	20	155	finally
    //   28	32	155	finally
    //   38	42	155	finally
    //   51	54	155	finally
    //   54	57	155	finally
    //   66	69	155	finally
    //   71	76	155	finally
    //   79	83	155	finally
    //   92	95	155	finally
    //   98	103	155	finally
    //   109	112	155	finally
    //   114	119	155	finally
    //   120	125	155	finally
    //   127	131	155	finally
    //   136	139	155	finally
    //   140	144	155	finally
    //   8	11	174	java/lang/ClassNotFoundException
    //   8	11	174	java/lang/NoSuchMethodException
    //   8	11	174	java/lang/IllegalAccessException
    //   8	11	174	java/lang/reflect/InvocationTargetException
    //   16	20	174	java/lang/ClassNotFoundException
    //   16	20	174	java/lang/NoSuchMethodException
    //   16	20	174	java/lang/IllegalAccessException
    //   16	20	174	java/lang/reflect/InvocationTargetException
    //   28	32	174	java/lang/ClassNotFoundException
    //   28	32	174	java/lang/NoSuchMethodException
    //   28	32	174	java/lang/IllegalAccessException
    //   28	32	174	java/lang/reflect/InvocationTargetException
    //   38	42	174	java/lang/ClassNotFoundException
    //   38	42	174	java/lang/NoSuchMethodException
    //   38	42	174	java/lang/IllegalAccessException
    //   38	42	174	java/lang/reflect/InvocationTargetException
    //   51	54	174	java/lang/ClassNotFoundException
    //   51	54	174	java/lang/NoSuchMethodException
    //   51	54	174	java/lang/IllegalAccessException
    //   51	54	174	java/lang/reflect/InvocationTargetException
    //   54	57	174	java/lang/ClassNotFoundException
    //   54	57	174	java/lang/NoSuchMethodException
    //   54	57	174	java/lang/IllegalAccessException
    //   54	57	174	java/lang/reflect/InvocationTargetException
    //   66	69	174	java/lang/ClassNotFoundException
    //   66	69	174	java/lang/NoSuchMethodException
    //   66	69	174	java/lang/IllegalAccessException
    //   66	69	174	java/lang/reflect/InvocationTargetException
    //   71	76	174	java/lang/ClassNotFoundException
    //   71	76	174	java/lang/NoSuchMethodException
    //   71	76	174	java/lang/IllegalAccessException
    //   71	76	174	java/lang/reflect/InvocationTargetException
    //   79	83	174	java/lang/ClassNotFoundException
    //   79	83	174	java/lang/NoSuchMethodException
    //   79	83	174	java/lang/IllegalAccessException
    //   79	83	174	java/lang/reflect/InvocationTargetException
    //   92	95	174	java/lang/ClassNotFoundException
    //   92	95	174	java/lang/NoSuchMethodException
    //   92	95	174	java/lang/IllegalAccessException
    //   92	95	174	java/lang/reflect/InvocationTargetException
    //   98	103	174	java/lang/ClassNotFoundException
    //   98	103	174	java/lang/NoSuchMethodException
    //   98	103	174	java/lang/IllegalAccessException
    //   98	103	174	java/lang/reflect/InvocationTargetException
    //   109	112	174	java/lang/ClassNotFoundException
    //   109	112	174	java/lang/NoSuchMethodException
    //   109	112	174	java/lang/IllegalAccessException
    //   109	112	174	java/lang/reflect/InvocationTargetException
    //   114	119	174	java/lang/ClassNotFoundException
    //   114	119	174	java/lang/NoSuchMethodException
    //   114	119	174	java/lang/IllegalAccessException
    //   114	119	174	java/lang/reflect/InvocationTargetException
    //   120	125	174	java/lang/ClassNotFoundException
    //   120	125	174	java/lang/NoSuchMethodException
    //   120	125	174	java/lang/IllegalAccessException
    //   120	125	174	java/lang/reflect/InvocationTargetException
    //   127	131	174	java/lang/ClassNotFoundException
    //   127	131	174	java/lang/NoSuchMethodException
    //   127	131	174	java/lang/IllegalAccessException
    //   127	131	174	java/lang/reflect/InvocationTargetException
    //   136	139	174	java/lang/ClassNotFoundException
    //   136	139	174	java/lang/NoSuchMethodException
    //   136	139	174	java/lang/IllegalAccessException
    //   136	139	174	java/lang/reflect/InvocationTargetException
    //   140	144	174	java/lang/ClassNotFoundException
    //   140	144	174	java/lang/NoSuchMethodException
    //   140	144	174	java/lang/IllegalAccessException
    //   140	144	174	java/lang/reflect/InvocationTargetException
  }
  
  public static String b()
  {
    boolean bool = a();
    if (bool)
    {
      Object localObject1 = "android.os.SystemProperties";
      try
      {
        localObject1 = Class.forName((String)localObject1);
        Object localObject2 = "get";
        int i = 1;
        Object localObject3 = new Class[i];
        Class localClass = String.class;
        localObject3[0] = localClass;
        localObject2 = ((Class)localObject1).getMethod((String)localObject2, (Class[])localObject3);
        Object[] arrayOfObject = new Object[i];
        localObject3 = "ro.build.version.incremental";
        arrayOfObject[0] = localObject3;
        localObject1 = ((Method)localObject2).invoke(localObject1, arrayOfObject);
        return (String)localObject1;
      }
      catch (ClassNotFoundException|NoSuchMethodException|IllegalAccessException|InvocationTargetException localClassNotFoundException)
      {
        return "ErrorWhileReadingProperty";
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */