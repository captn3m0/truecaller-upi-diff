package com.truecaller.utils.a;

import android.content.SharedPreferences.Editor;
import c.f;
import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import java.io.Closeable;

public final class b
  implements c, Closeable
{
  private final f b;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(b.class);
    ((u)localObject).<init>(localb, "edit", "getEdit()Landroid/content/SharedPreferences$Editor;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public b(a parama)
  {
    c = parama;
    b.a locala = new com/truecaller/utils/a/b$a;
    locala.<init>(parama);
    parama = c.g.a((c.g.a.a)locala);
    b = parama;
  }
  
  public final SharedPreferences.Editor a()
  {
    return (SharedPreferences.Editor)b.b();
  }
  
  public final void b(String paramString, int paramInt)
  {
    k.b(paramString, "key");
    a().putInt(paramString, paramInt);
  }
  
  public final void close()
  {
    a().commit();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */