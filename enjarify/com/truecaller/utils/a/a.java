package com.truecaller.utils.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import c.a.aa;
import c.a.m;
import c.g.b.k;
import java.util.LinkedHashSet;
import java.util.Set;

public abstract class a
  implements c
{
  final SharedPreferences a;
  
  public a(SharedPreferences paramSharedPreferences)
  {
    a = paramSharedPreferences;
  }
  
  private String c()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("VERSION_");
    String str = b();
    localStringBuilder.append(str);
    return localStringBuilder.toString();
  }
  
  public abstract int a();
  
  /* Error */
  public final int a(SharedPreferences paramSharedPreferences, Set paramSet)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 41
    //   3: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_1
    //   7: invokeinterface 47 1 0
    //   12: astore_3
    //   13: ldc 49
    //   15: astore 4
    //   17: aload_3
    //   18: aload 4
    //   20: invokestatic 51	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   23: aload_3
    //   24: invokeinterface 57 1 0
    //   29: iconst_1
    //   30: ixor
    //   31: istore 5
    //   33: aconst_null
    //   34: astore 6
    //   36: iload 5
    //   38: ifeq +6 -> 44
    //   41: goto +5 -> 46
    //   44: aconst_null
    //   45: astore_3
    //   46: aload_3
    //   47: ifnonnull +5 -> 52
    //   50: iconst_0
    //   51: ireturn
    //   52: new 59	com/truecaller/utils/a/b
    //   55: astore 4
    //   57: aload 4
    //   59: aload_0
    //   60: invokespecial 62	com/truecaller/utils/a/b:<init>	(Lcom/truecaller/utils/a/a;)V
    //   63: aload 4
    //   65: checkcast 64	java/io/Closeable
    //   68: astore 4
    //   70: aload 4
    //   72: astore 7
    //   74: aload 4
    //   76: checkcast 59	com/truecaller/utils/a/b
    //   79: astore 7
    //   81: aload_3
    //   82: invokestatic 70	c/a/ag:c	(Ljava/util/Map;)Lc/m/i;
    //   85: astore 8
    //   87: new 72	com/truecaller/utils/a/a$a
    //   90: astore 9
    //   92: aload 9
    //   94: aload_3
    //   95: aload_2
    //   96: aload_1
    //   97: invokespecial 75	com/truecaller/utils/a/a$a:<init>	(Ljava/util/Map;Ljava/util/Set;Landroid/content/SharedPreferences;)V
    //   100: aload 9
    //   102: checkcast 77	c/g/a/b
    //   105: astore 9
    //   107: aload 8
    //   109: aload 9
    //   111: invokestatic 82	c/m/l:a	(Lc/m/i;Lc/g/a/b;)Lc/m/i;
    //   114: astore_2
    //   115: aload_2
    //   116: invokeinterface 87 1 0
    //   121: astore_2
    //   122: aload_2
    //   123: invokeinterface 92 1 0
    //   128: istore 10
    //   130: iload 10
    //   132: ifeq +633 -> 765
    //   135: aload_2
    //   136: invokeinterface 96 1 0
    //   141: astore 8
    //   143: aload 8
    //   145: checkcast 98	java/util/Map$Entry
    //   148: astore 8
    //   150: aload 8
    //   152: invokeinterface 101 1 0
    //   157: astore 9
    //   159: aload 9
    //   161: checkcast 103	java/lang/String
    //   164: astore 9
    //   166: aload 8
    //   168: invokeinterface 106 1 0
    //   173: astore 8
    //   175: aload 8
    //   177: instanceof 108
    //   180: istore 11
    //   182: iload 11
    //   184: ifeq +50 -> 234
    //   187: aload 8
    //   189: checkcast 110	java/lang/Number
    //   192: astore 8
    //   194: aload 8
    //   196: invokevirtual 114	java/lang/Number:longValue	()J
    //   199: lstore 12
    //   201: ldc 116
    //   203: astore 8
    //   205: aload 9
    //   207: aload 8
    //   209: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   212: aload 7
    //   214: invokevirtual 119	com/truecaller/utils/a/b:a	()Landroid/content/SharedPreferences$Editor;
    //   217: astore 8
    //   219: aload 8
    //   221: aload 9
    //   223: lload 12
    //   225: invokeinterface 125 4 0
    //   230: pop
    //   231: goto +443 -> 674
    //   234: aload 8
    //   236: instanceof 127
    //   239: istore 11
    //   241: iload 11
    //   243: ifeq +57 -> 300
    //   246: aload 8
    //   248: checkcast 110	java/lang/Number
    //   251: astore 8
    //   253: aload 8
    //   255: invokevirtual 131	java/lang/Number:doubleValue	()D
    //   258: dstore 14
    //   260: ldc 116
    //   262: astore 8
    //   264: aload 9
    //   266: aload 8
    //   268: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   271: aload 7
    //   273: invokevirtual 119	com/truecaller/utils/a/b:a	()Landroid/content/SharedPreferences$Editor;
    //   276: astore 8
    //   278: dload 14
    //   280: invokestatic 135	java/lang/Double:doubleToRawLongBits	(D)J
    //   283: lstore 12
    //   285: aload 8
    //   287: aload 9
    //   289: lload 12
    //   291: invokeinterface 125 4 0
    //   296: pop
    //   297: goto +377 -> 674
    //   300: aload 8
    //   302: instanceof 137
    //   305: istore 11
    //   307: iload 11
    //   309: ifeq +50 -> 359
    //   312: aload 8
    //   314: checkcast 110	java/lang/Number
    //   317: astore 8
    //   319: aload 8
    //   321: invokevirtual 141	java/lang/Number:floatValue	()F
    //   324: fstore 16
    //   326: ldc 116
    //   328: astore 17
    //   330: aload 9
    //   332: aload 17
    //   334: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   337: aload 7
    //   339: invokevirtual 119	com/truecaller/utils/a/b:a	()Landroid/content/SharedPreferences$Editor;
    //   342: astore 17
    //   344: aload 17
    //   346: aload 9
    //   348: fload 16
    //   350: invokeinterface 145 3 0
    //   355: pop
    //   356: goto +318 -> 674
    //   359: aload 8
    //   361: instanceof 147
    //   364: istore 11
    //   366: iload 11
    //   368: ifeq +29 -> 397
    //   371: aload 8
    //   373: checkcast 110	java/lang/Number
    //   376: astore 8
    //   378: aload 8
    //   380: invokevirtual 151	java/lang/Number:intValue	()I
    //   383: istore 10
    //   385: aload 7
    //   387: aload 9
    //   389: iload 10
    //   391: invokevirtual 154	com/truecaller/utils/a/b:b	(Ljava/lang/String;I)V
    //   394: goto +280 -> 674
    //   397: aload 8
    //   399: instanceof 156
    //   402: istore 11
    //   404: iload 11
    //   406: ifeq +50 -> 456
    //   409: aload 8
    //   411: checkcast 156	java/lang/Boolean
    //   414: astore 8
    //   416: aload 8
    //   418: invokevirtual 159	java/lang/Boolean:booleanValue	()Z
    //   421: istore 10
    //   423: ldc 116
    //   425: astore 17
    //   427: aload 9
    //   429: aload 17
    //   431: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   434: aload 7
    //   436: invokevirtual 119	com/truecaller/utils/a/b:a	()Landroid/content/SharedPreferences$Editor;
    //   439: astore 17
    //   441: aload 17
    //   443: aload 9
    //   445: iload 10
    //   447: invokeinterface 163 3 0
    //   452: pop
    //   453: goto +221 -> 674
    //   456: aload 8
    //   458: instanceof 103
    //   461: istore 11
    //   463: iload 11
    //   465: ifeq +43 -> 508
    //   468: aload 8
    //   470: checkcast 103	java/lang/String
    //   473: astore 8
    //   475: ldc 116
    //   477: astore 17
    //   479: aload 9
    //   481: aload 17
    //   483: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   486: aload 7
    //   488: invokevirtual 119	com/truecaller/utils/a/b:a	()Landroid/content/SharedPreferences$Editor;
    //   491: astore 17
    //   493: aload 17
    //   495: aload 9
    //   497: aload 8
    //   499: invokeinterface 167 3 0
    //   504: pop
    //   505: goto +169 -> 674
    //   508: aload 8
    //   510: instanceof 169
    //   513: istore 11
    //   515: iload 11
    //   517: ifeq +152 -> 669
    //   520: aload 8
    //   522: checkcast 169	java/util/Set
    //   525: astore 8
    //   527: ldc 116
    //   529: astore 17
    //   531: aload 9
    //   533: aload 17
    //   535: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   538: ldc -85
    //   540: astore 17
    //   542: aload 8
    //   544: aload 17
    //   546: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   549: aload 8
    //   551: astore 17
    //   553: aload 8
    //   555: checkcast 173	java/lang/Iterable
    //   558: astore 17
    //   560: aload 17
    //   562: invokeinterface 176 1 0
    //   567: astore 17
    //   569: aload 17
    //   571: invokeinterface 92 1 0
    //   576: istore 18
    //   578: iload 18
    //   580: ifeq +56 -> 636
    //   583: aload 17
    //   585: invokeinterface 96 1 0
    //   590: astore 19
    //   592: aload 19
    //   594: instanceof 103
    //   597: istore 18
    //   599: getstatic 181	c/z:a	Z
    //   602: istore 20
    //   604: iload 20
    //   606: ifeq -37 -> 569
    //   609: iload 18
    //   611: ifeq +6 -> 617
    //   614: goto -45 -> 569
    //   617: ldc -73
    //   619: astore_1
    //   620: new 185	java/lang/AssertionError
    //   623: astore_2
    //   624: aload_2
    //   625: aload_1
    //   626: invokespecial 188	java/lang/AssertionError:<init>	(Ljava/lang/Object;)V
    //   629: aload_2
    //   630: checkcast 190	java/lang/Throwable
    //   633: astore_2
    //   634: aload_2
    //   635: athrow
    //   636: ldc 116
    //   638: astore 17
    //   640: aload 9
    //   642: aload 17
    //   644: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   647: aload 7
    //   649: invokevirtual 119	com/truecaller/utils/a/b:a	()Landroid/content/SharedPreferences$Editor;
    //   652: astore 17
    //   654: aload 17
    //   656: aload 9
    //   658: aload 8
    //   660: invokeinterface 194 3 0
    //   665: pop
    //   666: goto +8 -> 674
    //   669: aload 8
    //   671: ifnonnull +33 -> 704
    //   674: aload_1
    //   675: invokeinterface 197 1 0
    //   680: astore 8
    //   682: aload 8
    //   684: aload 9
    //   686: invokeinterface 201 2 0
    //   691: astore 8
    //   693: aload 8
    //   695: invokeinterface 204 1 0
    //   700: pop
    //   701: goto -579 -> 122
    //   704: new 206	java/lang/IllegalStateException
    //   707: astore_1
    //   708: new 24	java/lang/StringBuilder
    //   711: astore_2
    //   712: ldc -48
    //   714: astore_3
    //   715: aload_2
    //   716: aload_3
    //   717: invokespecial 29	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   720: aload 8
    //   722: invokevirtual 212	java/lang/Object:getClass	()Ljava/lang/Class;
    //   725: astore_3
    //   726: aload_2
    //   727: aload_3
    //   728: invokevirtual 215	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   731: pop
    //   732: ldc -39
    //   734: astore_3
    //   735: aload_2
    //   736: aload_3
    //   737: invokevirtual 36	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   740: pop
    //   741: aload_2
    //   742: aload 9
    //   744: invokevirtual 36	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   747: pop
    //   748: aload_2
    //   749: invokevirtual 39	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   752: astore_2
    //   753: aload_1
    //   754: aload_2
    //   755: invokespecial 218	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   758: aload_1
    //   759: checkcast 190	java/lang/Throwable
    //   762: astore_1
    //   763: aload_1
    //   764: athrow
    //   765: getstatic 223	c/x:a	Lc/x;
    //   768: astore_1
    //   769: aload 4
    //   771: aconst_null
    //   772: invokestatic 228	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   775: aload_3
    //   776: invokeinterface 231 1 0
    //   781: ireturn
    //   782: astore_1
    //   783: goto +9 -> 792
    //   786: astore_1
    //   787: aload_1
    //   788: astore 6
    //   790: aload_1
    //   791: athrow
    //   792: aload 4
    //   794: aload 6
    //   796: invokestatic 228	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   799: aload_1
    //   800: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	801	0	this	a
    //   0	801	1	paramSharedPreferences	SharedPreferences
    //   0	801	2	paramSet	Set
    //   12	764	3	localObject1	Object
    //   15	778	4	localObject2	Object
    //   31	6	5	bool1	boolean
    //   34	761	6	localSharedPreferences	SharedPreferences
    //   72	576	7	localObject3	Object
    //   85	636	8	localObject4	Object
    //   90	653	9	localObject5	Object
    //   128	3	10	bool2	boolean
    //   383	7	10	i	int
    //   421	25	10	bool3	boolean
    //   180	336	11	bool4	boolean
    //   199	91	12	l	long
    //   258	21	14	d	double
    //   324	25	16	f	float
    //   328	327	17	localObject6	Object
    //   576	34	18	bool5	boolean
    //   590	3	19	localObject7	Object
    //   602	3	20	bool6	boolean
    // Exception table:
    //   from	to	target	type
    //   790	792	782	finally
    //   74	79	786	finally
    //   81	85	786	finally
    //   87	90	786	finally
    //   96	100	786	finally
    //   100	105	786	finally
    //   109	114	786	finally
    //   115	121	786	finally
    //   122	128	786	finally
    //   135	141	786	finally
    //   143	148	786	finally
    //   150	157	786	finally
    //   159	164	786	finally
    //   166	173	786	finally
    //   187	192	786	finally
    //   194	199	786	finally
    //   207	212	786	finally
    //   212	217	786	finally
    //   223	231	786	finally
    //   246	251	786	finally
    //   253	258	786	finally
    //   266	271	786	finally
    //   271	276	786	finally
    //   278	283	786	finally
    //   289	297	786	finally
    //   312	317	786	finally
    //   319	324	786	finally
    //   332	337	786	finally
    //   337	342	786	finally
    //   348	356	786	finally
    //   371	376	786	finally
    //   378	383	786	finally
    //   389	394	786	finally
    //   409	414	786	finally
    //   416	421	786	finally
    //   429	434	786	finally
    //   434	439	786	finally
    //   445	453	786	finally
    //   468	473	786	finally
    //   481	486	786	finally
    //   486	491	786	finally
    //   497	505	786	finally
    //   520	525	786	finally
    //   533	538	786	finally
    //   544	549	786	finally
    //   553	558	786	finally
    //   560	567	786	finally
    //   569	576	786	finally
    //   583	590	786	finally
    //   599	602	786	finally
    //   620	623	786	finally
    //   625	629	786	finally
    //   629	633	786	finally
    //   634	636	786	finally
    //   642	647	786	finally
    //   647	652	786	finally
    //   658	666	786	finally
    //   674	680	786	finally
    //   684	691	786	finally
    //   693	701	786	finally
    //   704	707	786	finally
    //   708	711	786	finally
    //   716	720	786	finally
    //   720	725	786	finally
    //   727	732	786	finally
    //   736	741	786	finally
    //   742	748	786	finally
    //   748	752	786	finally
    //   754	758	786	finally
    //   758	762	786	finally
    //   763	765	786	finally
    //   765	768	786	finally
  }
  
  public int a(String paramString, int paramInt)
  {
    k.b(paramString, "key");
    return a.getInt(paramString, paramInt);
  }
  
  public final long a(String paramString, long paramLong)
  {
    k.b(paramString, "key");
    return a.getLong(paramString, paramLong);
  }
  
  public final String a(String paramString)
  {
    k.b(paramString, "key");
    return a.getString(paramString, null);
  }
  
  public abstract void a(int paramInt, Context paramContext);
  
  public final void a(Context paramContext)
  {
    k.b(paramContext, "context");
    Object localObject = a;
    String str = c();
    int i = ((SharedPreferences)localObject).getInt(str, 0);
    int j = a();
    if (i < j) {
      a(i, paramContext);
    }
    paramContext = a.edit();
    localObject = c();
    paramContext.putInt((String)localObject, j).apply();
  }
  
  public final void a(String paramString, double paramDouble)
  {
    k.b(paramString, "key");
    SharedPreferences.Editor localEditor = a.edit();
    long l = Double.doubleToRawLongBits(paramDouble);
    localEditor.putLong(paramString, l).apply();
  }
  
  public final void a(String paramString, Long paramLong)
  {
    String str = "key";
    k.b(paramString, str);
    if (paramLong == null)
    {
      d(paramString);
      return;
    }
    long l = paramLong.longValue();
    b(paramString, l);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "key");
    a.edit().putString(paramString1, paramString2).apply();
  }
  
  public final void a(String paramString, Set paramSet)
  {
    k.b(paramString, "key");
    a.edit().putStringSet(paramString, paramSet).apply();
  }
  
  public final boolean a(String paramString, boolean paramBoolean)
  {
    k.b(paramString, "key");
    return a.getBoolean(paramString, paramBoolean);
  }
  
  public final int a_(String paramString)
  {
    k.b(paramString, "key");
    int i = a(paramString, 0) + 1;
    b(paramString, i);
    return i;
  }
  
  public abstract String b();
  
  public final String b(String paramString1, String paramString2)
  {
    k.b(paramString1, "key");
    k.b(paramString2, "defaultValue");
    SharedPreferences localSharedPreferences = a;
    paramString1 = localSharedPreferences.getString(paramString1, paramString2);
    if (paramString1 == null) {
      paramString1 = paramString2;
    }
    return paramString1;
  }
  
  public void b(String paramString, int paramInt)
  {
    k.b(paramString, "key");
    a.edit().putInt(paramString, paramInt).apply();
  }
  
  public final void b(String paramString, long paramLong)
  {
    k.b(paramString, "key");
    a.edit().putLong(paramString, paramLong).apply();
  }
  
  public final void b(String paramString, boolean paramBoolean)
  {
    k.b(paramString, "key");
    a.edit().putBoolean(paramString, paramBoolean).apply();
  }
  
  public final boolean b(String paramString)
  {
    k.b(paramString, "key");
    return a.getBoolean(paramString, false);
  }
  
  public final double c(String paramString)
  {
    k.b(paramString, "key");
    SharedPreferences localSharedPreferences = a;
    long l = Double.doubleToLongBits(0.0D);
    return Double.longBitsToDouble(localSharedPreferences.getLong(paramString, l));
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "key");
    a.edit().remove(paramString).apply();
  }
  
  public final boolean e(String paramString)
  {
    k.b(paramString, "key");
    return a.contains(paramString);
  }
  
  public final Set f(String paramString)
  {
    k.b(paramString, "key");
    SharedPreferences localSharedPreferences = a;
    Set localSet = (Set)aa.a;
    paramString = localSharedPreferences.getStringSet(paramString, localSet);
    if (paramString != null)
    {
      paramString = m.l((Iterable)paramString);
      if (paramString != null) {}
    }
    else
    {
      paramString = new java/util/LinkedHashSet;
      paramString.<init>();
      paramString = (Set)paramString;
    }
    return paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */