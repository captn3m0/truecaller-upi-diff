package com.truecaller.utils;

public final class R$string
{
  public static final int common_google_play_services_enable_button = 2131887712;
  public static final int common_google_play_services_enable_text = 2131887713;
  public static final int common_google_play_services_enable_title = 2131887714;
  public static final int common_google_play_services_install_button = 2131887715;
  public static final int common_google_play_services_install_text = 2131887716;
  public static final int common_google_play_services_install_title = 2131887717;
  public static final int common_google_play_services_notification_channel_name = 2131887718;
  public static final int common_google_play_services_notification_ticker = 2131887719;
  public static final int common_google_play_services_unknown_issue = 2131887720;
  public static final int common_google_play_services_unsupported_text = 2131887721;
  public static final int common_google_play_services_update_button = 2131887722;
  public static final int common_google_play_services_update_text = 2131887723;
  public static final int common_google_play_services_update_title = 2131887724;
  public static final int common_google_play_services_updating_text = 2131887725;
  public static final int common_google_play_services_wear_update_text = 2131887726;
  public static final int common_open_on_phone = 2131887727;
  public static final int common_signin_button_text = 2131887728;
  public static final int common_signin_button_text_long = 2131887729;
  public static final int status_bar_notification_info_overflow = 2131888855;
}

/* Location:
 * Qualified Name:     com.truecaller.utils.R.string
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */