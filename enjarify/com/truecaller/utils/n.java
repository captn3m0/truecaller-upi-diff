package com.truecaller.utils;

import android.graphics.drawable.Drawable;

public abstract interface n
{
  public abstract String a(int paramInt1, int paramInt2, Object... paramVarArgs);
  
  public abstract String a(int paramInt, Object... paramVarArgs);
  
  public abstract String[] a(int paramInt);
  
  public abstract int b(int paramInt);
  
  public abstract CharSequence b(int paramInt, Object... paramVarArgs);
  
  public abstract Drawable c(int paramInt);
  
  public abstract int d(int paramInt);
}

/* Location:
 * Qualified Name:     com.truecaller.utils.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */