package com.truecaller.utils;

import java.net.InetAddress;
import java.net.Socket;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public final class k
  extends SSLSocketFactory
{
  private final SSLSocketFactory a;
  
  public k(SSLSocketFactory paramSSLSocketFactory)
  {
    a = paramSSLSocketFactory;
  }
  
  private static Socket a(Socket paramSocket)
  {
    boolean bool = paramSocket instanceof SSLSocket;
    if (bool)
    {
      bool = paramSocket instanceof k.b;
      if (!bool)
      {
        k.b localb = new com/truecaller/utils/k$b;
        paramSocket = (SSLSocket)paramSocket;
        localb.<init>(paramSocket);
        paramSocket = localb;
        paramSocket = (Socket)localb;
      }
    }
    return (Socket)paramSocket;
  }
  
  public final Socket createSocket(String paramString, int paramInt)
  {
    c.g.b.k.b(paramString, "host");
    paramString = a.createSocket(paramString, paramInt);
    c.g.b.k.a(paramString, "delegate.createSocket(host, port)");
    return a(paramString);
  }
  
  public final Socket createSocket(String paramString, int paramInt1, InetAddress paramInetAddress, int paramInt2)
  {
    c.g.b.k.b(paramString, "host");
    c.g.b.k.b(paramInetAddress, "localHost");
    paramString = a.createSocket(paramString, paramInt1, paramInetAddress, paramInt2);
    c.g.b.k.a(paramString, "delegate.createSocket(ho…rt, localHost, localPort)");
    return a(paramString);
  }
  
  public final Socket createSocket(InetAddress paramInetAddress, int paramInt)
  {
    c.g.b.k.b(paramInetAddress, "host");
    paramInetAddress = a.createSocket(paramInetAddress, paramInt);
    c.g.b.k.a(paramInetAddress, "delegate.createSocket(host, port)");
    return a(paramInetAddress);
  }
  
  public final Socket createSocket(InetAddress paramInetAddress1, int paramInt1, InetAddress paramInetAddress2, int paramInt2)
  {
    c.g.b.k.b(paramInetAddress1, "address");
    c.g.b.k.b(paramInetAddress2, "localAddress");
    paramInetAddress1 = a.createSocket(paramInetAddress1, paramInt1, paramInetAddress2, paramInt2);
    c.g.b.k.a(paramInetAddress1, "delegate.createSocket(ad… localAddress, localPort)");
    return a(paramInetAddress1);
  }
  
  public final Socket createSocket(Socket paramSocket, String paramString, int paramInt, boolean paramBoolean)
  {
    c.g.b.k.b(paramSocket, "s");
    c.g.b.k.b(paramString, "host");
    paramSocket = a.createSocket(paramSocket, paramString, paramInt, paramBoolean);
    c.g.b.k.a(paramSocket, "delegate.createSocket(s, host, port, autoClose)");
    return a(paramSocket);
  }
  
  public final String[] getDefaultCipherSuites()
  {
    String[] arrayOfString = a.getDefaultCipherSuites();
    c.g.b.k.a(arrayOfString, "delegate.defaultCipherSuites");
    return arrayOfString;
  }
  
  public final String[] getSupportedCipherSuites()
  {
    String[] arrayOfString = a.getSupportedCipherSuites();
    c.g.b.k.a(arrayOfString, "delegate.supportedCipherSuites");
    return arrayOfString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */