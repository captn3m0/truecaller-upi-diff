package com.truecaller.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Point;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.PowerManager;
import android.provider.Settings.System;
import android.provider.Telephony.Sms;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.view.WindowManager;
import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.utils.extensions.i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class e
  implements d
{
  private final Context a;
  private final l b;
  private final String c;
  
  public e(Context paramContext, l paraml, String paramString)
  {
    a = paramContext;
    b = paraml;
    c = paramString;
  }
  
  private PackageInfo a(String paramString, int paramInt)
  {
    Object localObject = "packageName";
    k.b(paramString, (String)localObject);
    try
    {
      localObject = a;
      localObject = ((Context)localObject).getPackageManager();
      paramString = ((PackageManager)localObject).getPackageInfo(paramString, paramInt);
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      paramString = null;
    }
    return paramString;
  }
  
  private boolean t()
  {
    return a.getPackageManager().hasSystemFeature("android.hardware.telephony");
  }
  
  public final boolean a()
  {
    String str = k();
    return a(str);
  }
  
  public final boolean a(String paramString)
  {
    l locall = b;
    String[] arrayOfString = { "android.permission.RECEIVE_SMS" };
    boolean bool1 = locall.a(arrayOfString);
    if (bool1)
    {
      boolean bool2 = b(paramString);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean b()
  {
    TelephonyManager localTelephonyManager = i.b(a);
    int i = localTelephonyManager.getPhoneType();
    int j = 2;
    return i == j;
  }
  
  public final boolean b(String paramString)
  {
    return k.a(c, paramString);
  }
  
  public final boolean c()
  {
    String str1 = a.getPackageName();
    String str2 = j();
    return m.a(str1, str2, true);
  }
  
  public final boolean c(String paramString)
  {
    String str = "pkgName";
    k.b(paramString, str);
    int i = 128;
    paramString = a(paramString, i);
    return paramString != null;
  }
  
  public final boolean d()
  {
    String str = k();
    return b(str);
  }
  
  public final boolean d(String paramString)
  {
    k.b(paramString, "packageName");
    PackageManager localPackageManager = a.getPackageManager();
    if (localPackageManager != null)
    {
      String str = "com.truecaller";
      int i = localPackageManager.checkSignatures(str, paramString);
      if (i == 0) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean e()
  {
    return m.a(Build.BRAND, "HUAWEI", true);
  }
  
  public final boolean f()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 23;
    if (i >= j)
    {
      Object localObject = a;
      String str = "power";
      localObject = ((Context)localObject).getSystemService(str);
      if (localObject != null)
      {
        localObject = (PowerManager)localObject;
        str = n();
        return ((PowerManager)localObject).isIgnoringBatteryOptimizations(str);
      }
      localObject = new c/u;
      ((u)localObject).<init>("null cannot be cast to non-null type android.os.PowerManager");
      throw ((Throwable)localObject);
    }
    return true;
  }
  
  public final boolean g()
  {
    l locall = b;
    String[] arrayOfString = { "android.permission.MODIFY_PHONE_STATE" };
    return locall.a(arrayOfString);
  }
  
  public final int h()
  {
    return Build.VERSION.SDK_INT;
  }
  
  /* Error */
  public final String i()
  {
    // Byte code:
    //   0: getstatic 150	android/os/Build$VERSION:SECURITY_PATCH	Ljava/lang/String;
    //   3: areturn
    //   4: pop
    //   5: aconst_null
    //   6: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	7	0	this	e
    // Exception table:
    //   from	to	target	type
    //   0	3	4	finally
  }
  
  public final String j()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 23;
    if (i >= j)
    {
      boolean bool = t();
      if (bool) {
        return i.c(a).getDefaultDialerPackage();
      }
    }
    return null;
  }
  
  public final String k()
  {
    boolean bool = t();
    if (bool) {
      return Telephony.Sms.getDefaultSmsPackage(a);
    }
    return null;
  }
  
  public final String l()
  {
    return Build.DEVICE;
  }
  
  public final String m()
  {
    return Build.MANUFACTURER;
  }
  
  public final String n()
  {
    Context localContext = a.getApplicationContext();
    k.a(localContext, "context.applicationContext");
    return localContext.getPackageName();
  }
  
  public final int o()
  {
    PackageInfo localPackageInfo = a("com.google.android.gms", 0);
    if (localPackageInfo == null) {
      return -1;
    }
    return versionCode;
  }
  
  public final boolean p()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 23;
    if (i >= j) {
      return Settings.System.canWrite(a);
    }
    return true;
  }
  
  public final boolean q()
  {
    return a.getPackageManager().hasSystemFeature("android.hardware.camera");
  }
  
  public final boolean r()
  {
    Object localObject1 = new android/graphics/Point;
    ((Point)localObject1).<init>();
    Object localObject2 = a;
    String str = "window";
    localObject2 = ((Context)localObject2).getSystemService(str);
    if (localObject2 != null)
    {
      localObject2 = ((WindowManager)localObject2).getDefaultDisplay();
      ((Display)localObject2).getSize((Point)localObject1);
      int i = x;
      int j = y;
      return i > j;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type android.view.WindowManager");
    throw ((Throwable)localObject1);
  }
  
  public final List s()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 1;
    int k = 21;
    String[] arrayOfString;
    if (i >= k)
    {
      arrayOfString = Build.SUPPORTED_ABIS;
      if (arrayOfString == null) {
        arrayOfString = new String[0];
      }
    }
    else
    {
      i = 2;
      arrayOfString = new String[i];
      localObject1 = Build.CPU_ABI;
      arrayOfString[0] = localObject1;
      localObject1 = Build.CPU_ABI2;
      arrayOfString[j] = localObject1;
    }
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    int m = arrayOfString.length;
    int n = 0;
    while (n < m)
    {
      String str = arrayOfString[n];
      Object localObject2 = str;
      localObject2 = (CharSequence)str;
      if (localObject2 != null)
      {
        bool = m.a((CharSequence)localObject2);
        if (!bool)
        {
          bool = false;
          localObject2 = null;
          break label145;
        }
      }
      boolean bool = true;
      label145:
      if (!bool) {
        ((Collection)localObject1).add(str);
      }
      n += 1;
    }
    return (List)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.utils.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */