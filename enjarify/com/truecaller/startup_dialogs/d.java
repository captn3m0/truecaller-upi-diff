package com.truecaller.startup_dialogs;

import android.support.v7.app.AppCompatActivity;
import c.d.f;
import com.truecaller.startup_dialogs.a.a;
import com.truecaller.startup_dialogs.a.aa;
import com.truecaller.startup_dialogs.a.ac;
import com.truecaller.startup_dialogs.a.ae;
import com.truecaller.startup_dialogs.a.ak;
import com.truecaller.startup_dialogs.a.an;
import com.truecaller.startup_dialogs.a.ap;
import com.truecaller.startup_dialogs.a.av;
import com.truecaller.startup_dialogs.a.ax;
import com.truecaller.startup_dialogs.a.az;
import com.truecaller.startup_dialogs.a.g;
import com.truecaller.startup_dialogs.a.i;
import com.truecaller.startup_dialogs.a.n;
import com.truecaller.startup_dialogs.a.p;
import com.truecaller.startup_dialogs.a.r;
import com.truecaller.startup_dialogs.a.t;
import com.truecaller.startup_dialogs.a.v;
import com.truecaller.startup_dialogs.a.y;
import com.truecaller.util.cn;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.bg;

public final class d
  implements c
{
  final List a;
  AppCompatActivity b;
  final cn c;
  private final f d;
  
  public d(f paramf, a parama, av paramav, az paramaz, ak paramak, com.truecaller.startup_dialogs.a.e parame, v paramv, n paramn, y paramy, i parami, p paramp, g paramg, t paramt, com.truecaller.startup_dialogs.a.ag paramag, aa paramaa, com.truecaller.startup_dialogs.a.c paramc, ap paramap, ae paramae, an paraman, ax paramax, r paramr, ac paramac, com.truecaller.startup_dialogs.a.k paramk, cn paramcn)
  {
    d = paramf;
    c = paramcn;
    localObject1 = new b[22];
    localObject4 = paramaa;
    localObject4 = (b)paramaa;
    localObject1[0] = localObject4;
    localObject3 = (b)paramav;
    localObject1[1] = localObject3;
    localObject3 = paramc;
    localObject3 = (b)paramc;
    localObject1[2] = localObject3;
    localObject3 = paramac;
    localObject3 = (b)paramac;
    localObject1[3] = localObject3;
    localObject3 = paramap;
    localObject3 = (b)paramap;
    localObject1[4] = localObject3;
    localObject3 = paramae;
    localObject3 = (b)paramae;
    localObject1[5] = localObject3;
    localObject2 = (b)parama;
    localObject1[6] = localObject2;
    localObject2 = paramv;
    localObject2 = (b)paramv;
    localObject1[7] = localObject2;
    localObject2 = paramak;
    localObject2 = (b)paramak;
    localObject1[8] = localObject2;
    localObject2 = paramaz;
    localObject2 = (b)paramaz;
    localObject1[9] = localObject2;
    localObject2 = paramk;
    localObject2 = (b)paramk;
    localObject1[10] = localObject2;
    localObject2 = paraman;
    localObject2 = (b)paraman;
    localObject1[11] = localObject2;
    localObject2 = paramn;
    localObject2 = (b)paramn;
    localObject1[12] = localObject2;
    localObject2 = paramy;
    localObject2 = (b)paramy;
    localObject1[13] = localObject2;
    localObject2 = parami;
    localObject2 = (b)parami;
    localObject1[14] = localObject2;
    localObject2 = paramp;
    localObject2 = (b)paramp;
    localObject1[15] = localObject2;
    localObject2 = paramg;
    localObject2 = (b)paramg;
    localObject1[16] = localObject2;
    localObject2 = paramt;
    localObject2 = (b)paramt;
    localObject1[17] = localObject2;
    localObject2 = paramag;
    localObject2 = (b)paramag;
    localObject1[18] = localObject2;
    localObject2 = parame;
    localObject2 = (b)parame;
    localObject1[19] = localObject2;
    localObject2 = paramax;
    localObject2 = (b)paramax;
    localObject1[20] = localObject2;
    localObject2 = paramr;
    localObject2 = (b)paramr;
    localObject1[21] = localObject2;
    localObject1 = c.a.m.b((Object[])localObject1);
    a = ((List)localObject1);
  }
  
  private final void a(boolean paramBoolean)
  {
    kotlinx.coroutines.ag localag = (kotlinx.coroutines.ag)bg.a;
    f localf = d;
    Object localObject = new com/truecaller/startup_dialogs/d$a;
    ((d.a)localObject).<init>(this, paramBoolean, null);
    localObject = (c.g.a.m)localObject;
    kotlinx.coroutines.e.b(localag, localf, (c.g.a.m)localObject, 2);
  }
  
  public final void a()
  {
    a(true);
  }
  
  public final void a(AppCompatActivity paramAppCompatActivity)
  {
    c.g.b.k.b(paramAppCompatActivity, "activity");
    b = paramAppCompatActivity;
  }
  
  public final void a(StartupDialogType paramStartupDialogType, StartupDialogDismissReason paramStartupDialogDismissReason)
  {
    c.g.b.k.b(paramStartupDialogType, "type");
    Iterator localIterator = ((Iterable)a).iterator();
    int i;
    do
    {
      bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = localIterator.next();
      Object localObject2 = localObject1;
      localObject2 = ((b)localObject1).a();
      if (localObject2 == paramStartupDialogType)
      {
        i = 1;
      }
      else
      {
        i = 0;
        localObject2 = null;
      }
    } while (i == 0);
    break label88;
    boolean bool = false;
    Object localObject1 = null;
    label88:
    localObject1 = (b)localObject1;
    if (localObject1 != null)
    {
      ((b)localObject1).a(paramStartupDialogDismissReason);
      a(false);
      return;
    }
  }
  
  public final boolean a(int paramInt)
  {
    Iterator localIterator = ((Iterable)a).iterator();
    boolean bool2;
    int i;
    do
    {
      bool1 = localIterator.hasNext();
      bool2 = true;
      if (!bool1) {
        break;
      }
      localObject1 = localIterator.next();
      Object localObject2 = localObject1;
      localObject2 = ((b)localObject1).a();
      i = ((StartupDialogType)localObject2).requestCode();
      if (i == paramInt)
      {
        i = 1;
      }
      else
      {
        i = 0;
        localObject2 = null;
      }
    } while (i == 0);
    break label89;
    boolean bool1 = false;
    Object localObject1 = null;
    label89:
    localObject1 = (b)localObject1;
    if (localObject1 != null)
    {
      StartupDialogDismissReason localStartupDialogDismissReason = StartupDialogDismissReason.USER_PRESSED_DISMISS_BUTTON;
      ((b)localObject1).a(localStartupDialogDismissReason);
      a(false);
      return bool2;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */