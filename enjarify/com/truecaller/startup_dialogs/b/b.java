package com.truecaller.startup_dialogs.b;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDialog;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.R.id;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.multisim.h;
import com.truecaller.startup_dialogs.fragments.j;
import com.truecaller.truepay.SmsBankData;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.registration.views.activities.AccountConnectionActivity;
import com.truecaller.truepay.app.ui.registration.views.activities.PreRegistrationActivity;
import com.truecaller.utils.extensions.p;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class b
  extends j
  implements c.b
{
  public c.a a;
  public h b;
  private HashMap c;
  
  public final View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final c.a a()
  {
    c.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void a(SmsBankData paramSmsBankData)
  {
    Context localContext = getContext();
    if (localContext != null)
    {
      Object localObject1 = Truepay.getInstance();
      k.a(localObject1, "Truepay.getInstance()");
      localObject1 = ((Truepay)localObject1).getAnalyticLoggerHelper();
      String str1 = "walkthrough";
      boolean bool1 = true;
      boolean bool2;
      if (paramSmsBankData != null)
      {
        bool2 = true;
      }
      else
      {
        bool2 = false;
        localObject2 = null;
      }
      String str2 = "walkthrough";
      h localh = b;
      if (localh == null)
      {
        String str3 = "multiSimManager";
        k.a(str3);
      }
      boolean bool3 = localh.j();
      ((com.truecaller.truepay.data.b.a)localObject1).a(str1, bool2, str2, bool3);
      dismissAllowingStateLoss();
      localObject1 = PreRegistrationActivity.c;
      k.a(localContext, "context");
      int i = 0;
      localObject1 = null;
      if (paramSmsBankData != null) {
        str1 = paramSmsBankData.getBankSymbol();
      } else {
        str1 = null;
      }
      if (paramSmsBankData != null)
      {
        int j = paramSmsBankData.getSimSlotIndex();
        localObject1 = Integer.valueOf(j);
      }
      k.b(localContext, "context");
      paramSmsBankData = Truepay.getInstance();
      Object localObject2 = "Truepay.getInstance()";
      k.a(paramSmsBankData, (String)localObject2);
      boolean bool4 = paramSmsBankData.isRegistrationComplete();
      if (bool4)
      {
        paramSmsBankData = new android/content/Intent;
        localObject2 = AccountConnectionActivity.class;
        paramSmsBankData.<init>(localContext, (Class)localObject2);
      }
      else
      {
        paramSmsBankData = new android/content/Intent;
        localObject2 = PreRegistrationActivity.class;
        paramSmsBankData.<init>(localContext, (Class)localObject2);
      }
      localObject2 = new android/os/Bundle;
      ((Bundle)localObject2).<init>();
      str2 = "bank_symbol";
      ((Bundle)localObject2).putString(str2, str1);
      if (localObject1 != null)
      {
        localObject1 = (Number)localObject1;
        i = ((Number)localObject1).intValue();
        str1 = "sim_slot";
        ((Bundle)localObject2).putInt(str1, i);
      }
      ((Bundle)localObject2).putString("action", "add_account");
      ((Bundle)localObject2).putBoolean("from_wizard", bool1);
      paramSmsBankData.setFlags(536870912);
      paramSmsBankData.putExtras((Bundle)localObject2);
      localContext.startActivity(paramSmsBankData);
      return;
    }
  }
  
  public final void a(SmsBankData paramSmsBankData, String paramString)
  {
    k.b(paramSmsBankData, "bankData");
    k.b(paramString, "carrierName");
    Object localObject = getView();
    int i;
    if (localObject != null)
    {
      i = R.id.description;
      localObject = (TextView)((View)localObject).findViewById(i);
      if (localObject != null)
      {
        localObject = (View)localObject;
        t.a((View)localObject);
      }
    }
    localObject = getView();
    if (localObject != null)
    {
      i = R.id.description;
      localObject = (TextView)((View)localObject).findViewById(i);
      if (localObject != null)
      {
        Object[] arrayOfObject = new Object[3];
        int j = paramSmsBankData.getSimSlotIndex();
        int k = 1;
        Integer localInteger = Integer.valueOf(j + k);
        arrayOfObject[0] = localInteger;
        arrayOfObject[k] = paramString;
        paramSmsBankData = paramSmsBankData.getBankName();
        k.a(paramSmsBankData, "bankData.bankName");
        arrayOfObject[2] = paramSmsBankData;
        p.a((TextView)localObject, 2131889044, arrayOfObject);
        return;
      }
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "message");
    Object localObject = getView();
    int i;
    if (localObject != null)
    {
      i = R.id.description;
      localObject = (TextView)((View)localObject).findViewById(i);
      if (localObject != null)
      {
        localObject = (View)localObject;
        t.a((View)localObject);
      }
    }
    localObject = getView();
    if (localObject != null)
    {
      i = R.id.description;
      localObject = (TextView)((View)localObject).findViewById(i);
      if (localObject != null)
      {
        paramString = (CharSequence)paramString;
        ((TextView)localObject).setText(paramString);
        return;
      }
    }
  }
  
  public final void f()
  {
    HashMap localHashMap = c;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getContext();
    if (paramBundle != null) {
      paramBundle = paramBundle.getApplicationContext();
    } else {
      paramBundle = null;
    }
    if (paramBundle != null)
    {
      ((TrueApp)paramBundle).a().cw().a(this);
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.TrueApp");
    throw paramBundle;
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new android/support/v7/app/AppCompatDialog;
    Context localContext = (Context)getActivity();
    paramBundle.<init>(localContext, 2131952156);
    return (Dialog)paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    paramLayoutInflater = paramLayoutInflater.inflate(2131558576, paramViewGroup, false);
    k.a(paramLayoutInflater, "view");
    int i = R.id.button_skip;
    paramViewGroup = (Button)paramLayoutInflater.findViewById(i);
    paramBundle = new com/truecaller/startup_dialogs/b/b$a;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramViewGroup.setOnClickListener(paramBundle);
    i = R.id.button_setup;
    paramViewGroup = (Button)paramLayoutInflater.findViewById(i);
    paramBundle = new com/truecaller/startup_dialogs/b/b$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramViewGroup.setOnClickListener(paramBundle);
    i = R.id.text_terms;
    paramViewGroup = (TextView)paramLayoutInflater.findViewById(i);
    k.a(paramViewGroup, "view.text_terms");
    paramBundle = LinkMovementMethod.getInstance();
    paramViewGroup.setMovementMethod(paramBundle);
    return paramLayoutInflater;
  }
  
  public final void onDestroyView()
  {
    c.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.y_();
    super.onDestroyView();
    f();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str1 = "view";
    k.b(paramView, str1);
    super.onViewCreated(paramView, paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
    paramView = Truepay.getInstance();
    k.a(paramView, "Truepay.getInstance()");
    paramView = paramView.getAnalyticLoggerHelper();
    paramBundle = "walkthrough";
    str1 = "walkthrough";
    h localh = b;
    if (localh == null)
    {
      String str2 = "multiSimManager";
      k.a(str2);
    }
    boolean bool = localh.j();
    paramView.a(paramBundle, str1, bool);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */