package com.truecaller.startup_dialogs.b;

import c.d.f;
import c.g.a.m;
import com.truecaller.ba;
import com.truecaller.multisim.h;
import com.truecaller.utils.n;
import kotlinx.coroutines.ao;

public final class d
  extends ba
  implements c.a
{
  final h c;
  final com.truecaller.truepay.d d;
  final n e;
  private ao f;
  private final com.truecaller.featuretoggles.e g;
  private final f h;
  private final f i;
  
  public d(h paramh, com.truecaller.truepay.d paramd, com.truecaller.featuretoggles.e parame, n paramn, f paramf1, f paramf2)
  {
    super(paramf1);
    c = paramh;
    d = paramd;
    g = parame;
    e = paramn;
    h = paramf1;
    i = paramf2;
  }
  
  public final void a()
  {
    c.b localb = (c.b)b;
    if (localb != null)
    {
      localb.dismiss();
      return;
    }
  }
  
  public final void b()
  {
    Object localObject = new com/truecaller/startup_dialogs/b/d$b;
    ((d.b)localObject).<init>(this, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.b(this, null, (m)localObject, 3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */