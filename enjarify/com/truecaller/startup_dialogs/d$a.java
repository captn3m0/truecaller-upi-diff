package com.truecaller.startup_dialogs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.AppCompatActivity;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.util.cn;
import java.util.Iterator;
import kotlinx.coroutines.ag;

final class d$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  Object c;
  int d;
  private ag g;
  
  d$a(d paramd, boolean paramBoolean, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/startup_dialogs/d$a;
    d locald = e;
    boolean bool = f;
    locala.<init>(locald, bool, paramc);
    paramObject = (ag)paramObject;
    g = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = d;
    int j = 1;
    Object localObject3;
    boolean bool2;
    Object localObject4;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      localObject2 = b;
      localObject3 = (Iterator)a;
      bool2 = paramObject instanceof o.b;
      if (!bool2)
      {
        localObject4 = localObject2;
        localObject2 = localObject1;
        localObject1 = this;
      }
      else
      {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label547;
      }
      localObject3 = ((Iterable)e.a).iterator();
      paramObject = this;
    }
    boolean bool4;
    do
    {
      bool1 = ((Iterator)localObject3).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = ((Iterator)localObject3).next();
      localObject4 = localObject2;
      localObject4 = (b)localObject2;
      boolean bool3 = f;
      if (!bool3)
      {
        bool3 = ((b)localObject4).b();
        if (!bool3) {}
      }
      else
      {
        a = localObject3;
        b = localObject2;
        c = localObject4;
        d = j;
        localObject4 = ((b)localObject4).a((c)paramObject);
        if (localObject4 == localObject1) {
          return localObject1;
        }
        Object localObject5 = localObject1;
        localObject1 = paramObject;
        paramObject = localObject4;
        localObject4 = localObject2;
        localObject2 = localObject5;
        paramObject = (Boolean)paramObject;
        bool4 = ((Boolean)paramObject).booleanValue();
        if (bool4)
        {
          paramObject = localObject1;
          localObject1 = localObject2;
          localObject2 = localObject4;
          bool2 = true;
          continue;
        }
        paramObject = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject4;
      }
      bool2 = false;
      localObject4 = null;
    } while (!bool2);
    break label296;
    boolean bool1 = false;
    Object localObject2 = null;
    label296:
    localObject2 = (b)localObject2;
    if (localObject2 != null)
    {
      localObject1 = ((b)localObject2).d();
      Object localObject6;
      int k;
      if (localObject1 != null)
      {
        localObject6 = ((Fragment)localObject1).getArguments();
        if (localObject6 == null)
        {
          localObject6 = new android/os/Bundle;
          ((Bundle)localObject6).<init>();
        }
        localObject3 = "StartupDialogType";
        localObject4 = ((b)localObject2).a().name();
        ((Bundle)localObject6).putString((String)localObject3, (String)localObject4);
        ((Fragment)localObject1).setArguments((Bundle)localObject6);
        if (localObject1 != null)
        {
          localObject6 = e.b;
          if (localObject6 != null)
          {
            localObject6 = ((AppCompatActivity)localObject6).getSupportFragmentManager();
            if (localObject6 != null)
            {
              localObject6 = ((j)localObject6).a();
              localObject3 = "StartupDialog";
              k = ((o)localObject6).a((Fragment)localObject1, (String)localObject3).d();
              localObject1 = Integer.valueOf(k);
              break label440;
            }
          }
          k = 0;
          localObject1 = null;
          label440:
          if (localObject1 != null) {
            break label536;
          }
        }
      }
      localObject1 = e.b;
      if (localObject1 != null)
      {
        paramObject = e.c;
        bool4 = ((cn)paramObject).a();
        if (!bool4)
        {
          k = 0;
          localObject1 = null;
        }
        if (localObject1 != null)
        {
          paramObject = localObject1;
          paramObject = (Activity)localObject1;
          paramObject = ((b)localObject2).a((Activity)paramObject);
          if (paramObject != null)
          {
            localObject6 = ((b)localObject2).a();
            j = ((StartupDialogType)localObject6).requestCode();
            ((AppCompatActivity)localObject1).startActivityForResult((Intent)paramObject, j);
            paramObject = x.a;
          }
        }
      }
      label536:
      ((b)localObject2).c();
    }
    return x.a;
    label547:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */