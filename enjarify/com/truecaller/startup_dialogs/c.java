package com.truecaller.startup_dialogs;

import android.support.v7.app.AppCompatActivity;

public abstract interface c
{
  public abstract void a();
  
  public abstract void a(AppCompatActivity paramAppCompatActivity);
  
  public abstract void a(StartupDialogType paramStartupDialogType, StartupDialogDismissReason paramStartupDialogDismissReason);
  
  public abstract boolean a(int paramInt);
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */