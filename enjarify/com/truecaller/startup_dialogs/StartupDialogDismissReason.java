package com.truecaller.startup_dialogs;

public enum StartupDialogDismissReason
{
  static
  {
    StartupDialogDismissReason[] arrayOfStartupDialogDismissReason = new StartupDialogDismissReason[1];
    StartupDialogDismissReason localStartupDialogDismissReason = new com/truecaller/startup_dialogs/StartupDialogDismissReason;
    localStartupDialogDismissReason.<init>("USER_PRESSED_DISMISS_BUTTON", 0);
    USER_PRESSED_DISMISS_BUTTON = localStartupDialogDismissReason;
    arrayOfStartupDialogDismissReason[0] = localStartupDialogDismissReason;
    $VALUES = arrayOfStartupDialogDismissReason;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.StartupDialogDismissReason
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */