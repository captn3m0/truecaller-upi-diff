package com.truecaller.startup_dialogs;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import c.d.c;

public abstract interface b
{
  public abstract Intent a(Activity paramActivity);
  
  public abstract StartupDialogType a();
  
  public abstract Object a(c paramc);
  
  public abstract void a(StartupDialogDismissReason paramStartupDialogDismissReason);
  
  public abstract boolean b();
  
  public abstract void c();
  
  public abstract Fragment d();
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */