package com.truecaller.startup_dialogs.a;

import dagger.a.d;
import javax.inject.Provider;

public final class ab
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private ab(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static ab a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    ab localab = new com/truecaller/startup_dialogs/a/ab;
    localab.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localab;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */