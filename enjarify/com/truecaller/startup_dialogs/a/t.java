package com.truecaller.startup_dialogs.a;

import c.d.c;
import com.truecaller.common.h.an;
import com.truecaller.i.e;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.util.al;
import com.truecaller.utils.l;

public final class t
  extends ar
{
  private final boolean a;
  private final StartupDialogType b;
  private final al c;
  private final l d;
  
  public t(al paramal, l paraml, e parame, an paraman)
  {
    super("key_location_promo_last_time", parame, paraman);
    c = paramal;
    d = paraml;
    paramal = StartupDialogType.POPUP_LOCATION_PERMISSION;
    b = paramal;
  }
  
  public final StartupDialogType a()
  {
    return b;
  }
  
  public final Object a(c paramc)
  {
    Object localObject = c;
    boolean bool = ((al)localObject).a();
    if (bool)
    {
      localObject = d;
      String[] arrayOfString = { "android.permission.ACCESS_COARSE_LOCATION" };
      bool = ((l)localObject).a(arrayOfString);
      if (!bool) {
        return super.a(paramc);
      }
    }
    return Boolean.FALSE;
  }
  
  public final boolean b()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */