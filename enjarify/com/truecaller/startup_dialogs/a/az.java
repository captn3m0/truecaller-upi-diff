package com.truecaller.startup_dialogs.a;

import android.app.Activity;
import android.content.Intent;
import c.d.a.a;
import c.d.c;
import c.g.b.k;
import c.o.b;
import com.truecaller.common.h.an;
import com.truecaller.i.e;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.startup_dialogs.b;
import com.truecaller.startup_dialogs.fragments.m;

public final class az
  implements b
{
  private final boolean a;
  private final StartupDialogType b;
  private final e c;
  private final b d;
  private final m e;
  private final an f;
  
  public az(e parame, b paramb, m paramm, an paraman)
  {
    c = parame;
    d = paramb;
    e = paramm;
    f = paraman;
    parame = StartupDialogType.DIALOG_WHATS_NEW;
    b = parame;
  }
  
  public final Intent a(Activity paramActivity)
  {
    k.b(paramActivity, "fromActivity");
    k.b(paramActivity, "fromActivity");
    return null;
  }
  
  public final StartupDialogType a()
  {
    return b;
  }
  
  public final Object a(c paramc)
  {
    boolean bool1 = paramc instanceof az.a;
    if (bool1)
    {
      localObject = paramc;
      localObject = (az.a)paramc;
      int j = b;
      k = -1 << -1;
      j &= k;
      if (j != 0)
      {
        int m = b - k;
        b = m;
        break label70;
      }
    }
    Object localObject = new com/truecaller/startup_dialogs/a/az$a;
    ((az.a)localObject).<init>(this, (c)paramc);
    label70:
    paramc = a;
    a locala = a.a;
    int k = b;
    int i1 = 1;
    switch (k)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 1: 
      localObject = (az)d;
      bool2 = paramc instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool3 = paramc instanceof o.b;
      if (bool3) {
        break label259;
      }
      paramc = d;
      d = this;
      b = i1;
      paramc = paramc.a((c)localObject);
      if (paramc == locala) {
        return locala;
      }
      localObject = this;
    }
    paramc = (Boolean)paramc;
    boolean bool4 = paramc.booleanValue();
    boolean bool2 = false;
    locala = null;
    if (bool4)
    {
      paramc = c;
      localObject = "whatsNewDialogShownRevision";
      int n = paramc.a((String)localObject, 0);
      int i = m.f;
      if (n < i) {
        bool2 = true;
      }
    }
    return Boolean.valueOf(bool2);
    label259:
    throw a;
  }
  
  public final void a(StartupDialogDismissReason paramStartupDialogDismissReason) {}
  
  public final boolean b()
  {
    return a;
  }
  
  public final void c()
  {
    e locale = c;
    int i = m.f;
    locale.b("whatsNewDialogShownRevision", i);
    locale = c;
    long l = f.a();
    locale.b("whatsNewShownTimestamp", l);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.az
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */