package com.truecaller.startup_dialogs.a;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import c.g.b.k;
import c.o.b;
import com.truecaller.backup.ag;
import com.truecaller.backup.bp;
import com.truecaller.backup.n;
import com.truecaller.common.account.r;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.utils.d;

public final class c
  implements com.truecaller.startup_dialogs.b
{
  private final boolean a;
  private final StartupDialogType b;
  private final com.truecaller.featuretoggles.e c;
  private final r d;
  private final d e;
  private final com.truecaller.common.g.a f;
  private final com.truecaller.i.e g;
  private final ag h;
  
  public c(com.truecaller.featuretoggles.e parame, r paramr, d paramd, com.truecaller.common.g.a parama, com.truecaller.i.e parame1, ag paramag)
  {
    c = parame;
    d = paramr;
    e = paramd;
    f = parama;
    g = parame1;
    h = paramag;
    a = true;
    parame = StartupDialogType.BACKUP_ONBOARDING;
    b = parame;
  }
  
  public final Intent a(Activity paramActivity)
  {
    k.b(paramActivity, "fromActivity");
    k.b(paramActivity, "fromActivity");
    return null;
  }
  
  public final StartupDialogType a()
  {
    return b;
  }
  
  public final Object a(c.d.c paramc)
  {
    int i = paramc instanceof c.a;
    int n;
    if (i != 0)
    {
      localObject1 = paramc;
      localObject1 = (c.a)paramc;
      int j = b;
      k = -1 << -1;
      j &= k;
      if (j != 0)
      {
        n = b - k;
        b = n;
        break label70;
      }
    }
    Object localObject1 = new com/truecaller/startup_dialogs/a/c$a;
    ((c.a)localObject1).<init>(this, (c.d.c)paramc);
    label70:
    paramc = a;
    c.d.a.a locala = c.d.a.a.a;
    int k = b;
    boolean bool2;
    switch (k)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 1: 
      i = e;
      boolean bool1 = paramc instanceof o.b;
      if (!bool1) {
        n = i;
      } else {
        throw a;
      }
      break;
    case 0: 
      int m = paramc instanceof o.b;
      if (m != 0) {
        break label368;
      }
      paramc = g;
      String str1 = "backupOnboardingAvailable";
      bool2 = paramc.b(str1);
      m = 1;
      ag localag = null;
      if (bool2)
      {
        paramc = g;
        localObject2 = "backupOnboardingShown";
        bool2 = paramc.b((String)localObject2);
        if (!bool2)
        {
          bool2 = true;
          break label230;
        }
      }
      bool2 = false;
      paramc = null;
      label230:
      Object localObject2 = c.l();
      boolean bool3 = ((com.truecaller.featuretoggles.b)localObject2).a();
      if (bool3)
      {
        localObject2 = d;
        bool3 = ((r)localObject2).c();
        if (bool3)
        {
          localObject2 = e.l();
          str2 = "kenzo";
          k.a(localObject2, str2);
        }
      }
      localObject2 = g;
      String str2 = "backupOnboardingAvailable";
      ((com.truecaller.i.e)localObject2).b(str2, false);
      if (bool2)
      {
        localag = h;
        d = this;
        e = bool2;
        b = m;
        localObject1 = localag.a((c.d.c)localObject1);
        if (localObject1 == locala) {
          return locala;
        }
      }
      break;
    }
    return Boolean.valueOf(bool2);
    label368:
    throw a;
  }
  
  public final void a(StartupDialogDismissReason paramStartupDialogDismissReason) {}
  
  public final boolean b()
  {
    return a;
  }
  
  public final void c()
  {
    g.b("backupOnboardingShown", true);
  }
  
  public final Fragment d()
  {
    Object localObject = f;
    String str = "key_backup_fetched_timestamp";
    long l1 = 0L;
    long l2 = ((com.truecaller.common.g.a)localObject).a(str, l1);
    boolean bool = l2 < l1;
    if (!bool)
    {
      localObject = new com/truecaller/backup/n;
      ((n)localObject).<init>();
      return (Fragment)localObject;
    }
    bp localbp = new com/truecaller/backup/bp;
    localbp.<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putLong("last_backup_time", l2);
    localBundle.putString("context", "wizard");
    localbp.setArguments(localBundle);
    return (Fragment)localbp;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */