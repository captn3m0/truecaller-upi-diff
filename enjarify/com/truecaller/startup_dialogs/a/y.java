package com.truecaller.startup_dialogs.a;

import com.truecaller.common.h.an;
import com.truecaller.i.e;
import com.truecaller.notifications.i;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.StartupDialogType;

public final class y
  extends ar
{
  private final boolean a;
  private final StartupDialogType b;
  private final com.truecaller.i.c c;
  private final i d;
  
  public y(com.truecaller.i.c paramc, i parami, e parame, an paraman)
  {
    super("key_missed_call_notif_promo_last_time", parame, paraman);
    c = paramc;
    d = parami;
    paramc = StartupDialogType.POPUP_MISSED_CALLS_NOTIFICATIONS;
    b = paramc;
  }
  
  public final StartupDialogType a()
  {
    return b;
  }
  
  public final Object a(c.d.c paramc)
  {
    Object localObject = c;
    String str = "hasNativeDialerCallerId";
    boolean bool = ((com.truecaller.i.c)localObject).b(str);
    if (bool) {
      return Boolean.FALSE;
    }
    localObject = d;
    bool = ((i)localObject).a();
    if (bool) {
      return Boolean.FALSE;
    }
    return super.a(paramc);
  }
  
  public final void a(StartupDialogDismissReason paramStartupDialogDismissReason)
  {
    Object localObject = StartupDialogDismissReason.USER_PRESSED_DISMISS_BUTTON;
    if (paramStartupDialogDismissReason == localObject)
    {
      localObject = c;
      String str = "showMissedCallsNotifications";
      ((com.truecaller.i.c)localObject).b(str, false);
    }
    super.a(paramStartupDialogDismissReason);
  }
  
  public final boolean b()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */