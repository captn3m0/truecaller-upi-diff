package com.truecaller.startup_dialogs.a;

import com.truecaller.common.h.an;
import com.truecaller.i.e;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.utils.l;

public final class i
  extends ar
{
  private final boolean a;
  private final StartupDialogType b;
  private final l c;
  private final com.truecaller.i.c d;
  
  public i(l paraml, com.truecaller.i.c paramc, e parame, an paraman)
  {
    super("key_draw_promo_last_time", parame, paraman);
    c = paraml;
    d = paramc;
    paraml = StartupDialogType.POPUP_DRAW_PERMISSION;
    b = paraml;
  }
  
  public final StartupDialogType a()
  {
    return b;
  }
  
  public final Object a(c.d.c paramc)
  {
    Object localObject = d;
    String str = "hasNativeDialerCallerId";
    boolean bool = ((com.truecaller.i.c)localObject).b(str);
    if (bool) {
      return Boolean.FALSE;
    }
    localObject = c;
    bool = ((l)localObject).a();
    if (bool) {
      return Boolean.FALSE;
    }
    return super.a(paramc);
  }
  
  public final boolean b()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */