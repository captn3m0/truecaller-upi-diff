package com.truecaller.startup_dialogs.a;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import c.d.c;
import c.g.b.k;
import com.truecaller.common.h.an;
import com.truecaller.i.e;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.b;
import java.util.concurrent.TimeUnit;

public abstract class ar
  implements b
{
  private final String a;
  private final e b;
  private final an c;
  
  public ar(String paramString, e parame, an paraman)
  {
    a = paramString;
    b = parame;
    c = paraman;
  }
  
  public final Intent a(Activity paramActivity)
  {
    k.b(paramActivity, "fromActivity");
    k.b(paramActivity, "fromActivity");
    return null;
  }
  
  public Object a(c paramc)
  {
    paramc = b;
    long l1 = 0L;
    long l2 = paramc.a("key_unimportant_promo_last_time", l1);
    paramc = b;
    String str = "feature_global_unimportant_promo_period_days";
    long l3 = as.a();
    l3 = paramc.a(str, l3);
    an localan = c;
    TimeUnit localTimeUnit = TimeUnit.DAYS;
    boolean bool = localan.a(l2, l3, localTimeUnit);
    if (!bool) {
      return Boolean.FALSE;
    }
    paramc = b;
    str = a;
    l2 = paramc.a(str, l1);
    paramc = b;
    l1 = as.b();
    l3 = paramc.a("feature_unimportant_promo_period_days", l1);
    localan = c;
    localTimeUnit = TimeUnit.DAYS;
    return Boolean.valueOf(localan.a(l2, l3, localTimeUnit));
  }
  
  public void a(StartupDialogDismissReason paramStartupDialogDismissReason)
  {
    Object localObject = StartupDialogDismissReason.USER_PRESSED_DISMISS_BUTTON;
    if (paramStartupDialogDismissReason == localObject)
    {
      paramStartupDialogDismissReason = b;
      long l1 = as.c();
      long l2 = paramStartupDialogDismissReason.a("feature_unimportant_promo_dismissed_delay_days", l1);
      long l3 = c.a();
      l2 = TimeUnit.DAYS.toMillis(l2);
      l3 += l2;
      paramStartupDialogDismissReason = b;
      localObject = a;
      paramStartupDialogDismissReason.b((String)localObject, l3);
    }
  }
  
  public final void c()
  {
    long l = c.a();
    b.b("key_unimportant_promo_last_time", l);
    e locale = b;
    String str = a;
    locale.b(str, l);
  }
  
  public Fragment d()
  {
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.ar
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */