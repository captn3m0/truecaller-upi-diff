package com.truecaller.startup_dialogs.a;

import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private q(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
  }
  
  public static q a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    q localq = new com/truecaller/startup_dialogs/a/q;
    localq.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */