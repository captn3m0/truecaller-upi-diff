package com.truecaller.startup_dialogs.a;

import c.d.c;
import com.truecaller.common.h.an;
import com.truecaller.notifications.i;
import com.truecaller.startup_dialogs.StartupDialogType;

public final class ax
  extends ar
{
  private final boolean a;
  private final StartupDialogType b;
  private final com.truecaller.calling.e.e c;
  private final i d;
  private final com.truecaller.i.e e;
  
  public ax(com.truecaller.calling.e.e parame, i parami, com.truecaller.i.e parame1, an paraman)
  {
    super("key_whats_app_in_call_log_notif_promo_last_time", parame1, paraman);
    c = parame;
    d = parami;
    e = parame1;
    parame = StartupDialogType.WHATS_APP_IN_CALL_LOG;
    b = parame;
  }
  
  public final StartupDialogType a()
  {
    return b;
  }
  
  public final Object a(c paramc)
  {
    Object localObject = e;
    String str = "key_whats_app_in_call_log_notif_promo_last_time";
    long l1 = 0L;
    long l2 = ((com.truecaller.i.e)localObject).a(str, l1);
    boolean bool1 = l2 < l1;
    if (bool1) {
      return Boolean.FALSE;
    }
    localObject = c;
    boolean bool2 = ((com.truecaller.calling.e.e)localObject).a();
    if (!bool2) {
      return Boolean.FALSE;
    }
    localObject = d;
    bool2 = ((i)localObject).a();
    if (!bool2) {
      return super.a(paramc);
    }
    return Boolean.FALSE;
  }
  
  public final boolean b()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.ax
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */