package com.truecaller.startup_dialogs.a;

import android.app.Activity;
import android.content.Intent;
import c.d.c;
import c.g.b.k;
import com.truecaller.common.account.r;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.truepay.Truepay;

public final class ap
  implements com.truecaller.startup_dialogs.b
{
  private final boolean a;
  private final StartupDialogType b;
  private final com.truecaller.featuretoggles.e c;
  private final r d;
  private final com.truecaller.i.e e;
  
  public ap(com.truecaller.featuretoggles.e parame, r paramr, com.truecaller.i.e parame1)
  {
    c = parame;
    d = paramr;
    e = parame1;
    a = true;
    parame = StartupDialogType.TCPAY_ONBOARDING;
    b = parame;
  }
  
  public final Intent a(Activity paramActivity)
  {
    k.b(paramActivity, "fromActivity");
    k.b(paramActivity, "fromActivity");
    return null;
  }
  
  public final StartupDialogType a()
  {
    return b;
  }
  
  public final Object a(c paramc)
  {
    paramc = e;
    String str1 = "tcpayOnboardingShown";
    boolean bool1 = paramc.b(str1);
    boolean bool2 = true;
    if (!bool1)
    {
      paramc = c.m();
      bool1 = paramc.a();
      if (bool1)
      {
        paramc = c.n();
        bool1 = paramc.a();
        if (bool1)
        {
          paramc = d;
          bool1 = paramc.c();
          if (bool1)
          {
            paramc = Truepay.getInstance();
            String str2 = "Truepay.getInstance()";
            k.a(paramc, str2);
            bool1 = paramc.isRegistrationComplete();
            if (!bool1)
            {
              bool1 = true;
              break label109;
            }
          }
        }
      }
    }
    bool1 = false;
    paramc = null;
    label109:
    e.b("tcpayOnboardingShown", bool2);
    return Boolean.valueOf(bool1);
  }
  
  public final void a(StartupDialogDismissReason paramStartupDialogDismissReason) {}
  
  public final boolean b()
  {
    return a;
  }
  
  public final void c() {}
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.ap
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */