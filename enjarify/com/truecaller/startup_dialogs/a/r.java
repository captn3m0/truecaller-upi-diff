package com.truecaller.startup_dialogs.a;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import c.d.c;
import c.g.b.k;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.startup_dialogs.b;

public final class r
  implements b
{
  private final boolean a;
  private final StartupDialogType b;
  private final com.truecaller.i.e c;
  
  public r(com.truecaller.i.e parame)
  {
    c = parame;
    parame = StartupDialogType.POPUP_IMAGE_FLASH;
    b = parame;
  }
  
  public final Intent a(Activity paramActivity)
  {
    k.b(paramActivity, "fromActivity");
    k.b(paramActivity, "fromActivity");
    return null;
  }
  
  public final StartupDialogType a()
  {
    return b;
  }
  
  public final Object a(c paramc)
  {
    return Boolean.FALSE;
  }
  
  public final void a(StartupDialogDismissReason paramStartupDialogDismissReason) {}
  
  public final boolean b()
  {
    return a;
  }
  
  public final void c()
  {
    c.b("imageFlashWhatsNewShown", true);
  }
  
  public final Fragment d()
  {
    com.truecaller.startup_dialogs.fragments.e locale = new com/truecaller/startup_dialogs/fragments/e;
    locale.<init>();
    return (Fragment)locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */