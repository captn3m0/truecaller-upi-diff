package com.truecaller.startup_dialogs.a;

import c.d.c;
import com.truecaller.common.h.an;
import com.truecaller.startup_dialogs.StartupDialogType;

public final class av
  extends ar
{
  private final boolean a;
  private final StartupDialogType b;
  private final com.truecaller.calling.e.e c;
  private final com.truecaller.i.e d;
  
  public av(com.truecaller.calling.e.e parame, com.truecaller.i.e parame1, an paraman)
  {
    super("key_whats_app_in_call_log_notif_promo_last_time", parame1, paraman);
    c = parame;
    d = parame1;
    parame = StartupDialogType.WHATS_APP_IN_CALL_LOG;
    b = parame;
  }
  
  public final StartupDialogType a()
  {
    return b;
  }
  
  public final Object a(c paramc)
  {
    paramc = d;
    String str = "key_whats_app_in_call_log_notif_promo_last_time";
    long l1 = 0L;
    long l2 = paramc.a(str, l1);
    boolean bool = l2 < l1;
    if (bool) {
      return Boolean.FALSE;
    }
    paramc = c;
    bool = paramc.c();
    if (bool)
    {
      paramc = c;
      bool = paramc.b();
      if (bool)
      {
        bool = true;
        break label82;
      }
    }
    bool = false;
    paramc = null;
    label82:
    return Boolean.valueOf(bool);
  }
  
  public final boolean b()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.av
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */