package com.truecaller.startup_dialogs.a;

import dagger.a.d;
import javax.inject.Provider;

public final class aq
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private aq(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static aq a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    aq localaq = new com/truecaller/startup_dialogs/a/aq;
    localaq.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localaq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.aq
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */