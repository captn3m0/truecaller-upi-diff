package com.truecaller.startup_dialogs.a;

import dagger.a.d;
import javax.inject.Provider;

public final class aw
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private aw(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static aw a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    aw localaw = new com/truecaller/startup_dialogs/a/aw;
    localaw.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localaw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.aw
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */