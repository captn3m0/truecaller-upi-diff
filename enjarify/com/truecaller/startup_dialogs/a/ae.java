package com.truecaller.startup_dialogs.a;

import android.app.Activity;
import android.content.Intent;
import c.g.b.k;
import com.truecaller.i.e;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.startup_dialogs.b;
import com.truecaller.utils.d;

public final class ae
  implements b
{
  private final boolean a;
  private final StartupDialogType b;
  private final e c;
  private final com.truecaller.common.h.c d;
  private final d e;
  
  public ae(e parame, com.truecaller.common.h.c paramc, d paramd)
  {
    c = parame;
    d = paramc;
    e = paramd;
    a = true;
    parame = StartupDialogType.PIN_SHORTCUTS_REQUEST_DIALOG;
    b = parame;
  }
  
  public final Intent a(Activity paramActivity)
  {
    k.b(paramActivity, "fromActivity");
    k.b(paramActivity, "fromActivity");
    return null;
  }
  
  public final StartupDialogType a()
  {
    return b;
  }
  
  public final Object a(c.d.c paramc)
  {
    paramc = e;
    int i = paramc.h();
    int j = 26;
    if (i >= j)
    {
      paramc = d;
      bool = paramc.a();
      if (!bool)
      {
        paramc = d;
        bool = paramc.b();
        if (!bool)
        {
          paramc = c;
          String str = "general_requestPinDialerShortcutShown";
          bool = paramc.b(str);
          if (!bool)
          {
            bool = true;
            break label83;
          }
        }
      }
    }
    boolean bool = false;
    paramc = null;
    label83:
    return Boolean.valueOf(bool);
  }
  
  public final void a(StartupDialogDismissReason paramStartupDialogDismissReason) {}
  
  public final boolean b()
  {
    return a;
  }
  
  public final void c()
  {
    c.b("general_requestPinDialerShortcutShown", true);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.ae
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */