package com.truecaller.startup_dialogs.a;

import com.truecaller.common.h.an;
import com.truecaller.i.e;
import com.truecaller.old.data.access.Settings;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.utils.l;

public final class g
  extends ar
{
  private final boolean a;
  private final StartupDialogType b;
  private final l c;
  private final com.truecaller.i.c d;
  
  public g(l paraml, com.truecaller.i.c paramc, e parame, an paraman)
  {
    super("key_dnd_promo_last_time", parame, paraman);
    c = paraml;
    d = paramc;
    paraml = StartupDialogType.POPUP_DO_NOT_DISTURB_ACCESS;
    b = paraml;
  }
  
  public final StartupDialogType a()
  {
    return b;
  }
  
  public final Object a(c.d.c paramc)
  {
    Object localObject = c;
    boolean bool = ((l)localObject).e();
    if (bool) {
      return Boolean.FALSE;
    }
    localObject = d;
    String str = "blockCallMethod";
    bool = Settings.b(((com.truecaller.i.c)localObject).a(str, 0));
    if (!bool) {
      return Boolean.FALSE;
    }
    return super.a(paramc);
  }
  
  public final boolean b()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */