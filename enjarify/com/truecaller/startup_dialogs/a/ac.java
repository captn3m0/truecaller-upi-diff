package com.truecaller.startup_dialogs.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import c.d.c;
import c.g.b.k;
import com.truecaller.common.h.an;
import com.truecaller.i.e;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.br;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.StartupDialogType;

public final class ac
  implements com.truecaller.startup_dialogs.b
{
  private final StartupDialogType a;
  private final boolean b;
  private final com.truecaller.common.f.b c;
  private final e d;
  private final an e;
  private final br f;
  
  public ac(com.truecaller.common.f.b paramb, e parame, an paraman, br parambr)
  {
    c = paramb;
    d = parame;
    e = paraman;
    f = parambr;
    paramb = StartupDialogType.ONBOARDING_PREMIUM_POPUP;
    a = paramb;
    b = true;
  }
  
  public final Intent a(Activity paramActivity)
  {
    k.b(paramActivity, "fromActivity");
    paramActivity = (Context)paramActivity;
    PremiumPresenterView.LaunchContext localLaunchContext = PremiumPresenterView.LaunchContext.ONBOARDING_POPUP;
    return br.a(paramActivity, localLaunchContext, null, 12);
  }
  
  public final StartupDialogType a()
  {
    return a;
  }
  
  public final Object a(c paramc)
  {
    paramc = d;
    String str = "general_onboarding_premium_shown";
    boolean bool = paramc.b(str);
    if (!bool)
    {
      paramc = c;
      bool = paramc.a();
      if (bool)
      {
        bool = true;
        break label45;
      }
    }
    bool = false;
    paramc = null;
    label45:
    return Boolean.valueOf(bool);
  }
  
  public final void a(StartupDialogDismissReason paramStartupDialogDismissReason) {}
  
  public final boolean b()
  {
    return b;
  }
  
  public final void c()
  {
    e locale = d;
    long l = e.a();
    locale.b("promo_popup_last_shown_timestamp", l);
    d.b("general_onboarding_premium_shown", true);
  }
  
  public final Fragment d()
  {
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */