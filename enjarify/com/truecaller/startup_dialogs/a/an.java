package com.truecaller.startup_dialogs.a;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.f;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.startup_dialogs.fragments.i;
import com.truecaller.truepay.Truepay;
import com.truecaller.util.af;

public final class an
  extends ai
{
  private final StartupDialogType a;
  private final boolean b;
  private final com.truecaller.featuretoggles.e c;
  
  public an(com.truecaller.i.e parame, com.truecaller.featuretoggles.e parame1, com.truecaller.common.h.an paraman, af paramaf)
  {
    super((f)localObject3, "feature_tcpay_promo_popup_last_time", paraman, parame, paramaf, parame1);
    c = parame1;
    parame = StartupDialogType.POPUP_TCPAY_PROMO;
    a = parame;
  }
  
  public final StartupDialogType a()
  {
    return a;
  }
  
  public final Fragment d()
  {
    com.truecaller.startup_dialogs.fragments.k localk = new com/truecaller/startup_dialogs/fragments/k;
    localk.<init>();
    String str1 = g();
    c.g.b.k.b(str1, "promoType");
    Bundle localBundle = localk.getArguments();
    if (localBundle == null)
    {
      localBundle = new android/os/Bundle;
      localBundle.<init>();
      String str2 = "promoType";
      localBundle.putString(str2, str1);
    }
    return (Fragment)localk;
  }
  
  public final boolean e()
  {
    return b;
  }
  
  public final boolean f()
  {
    Object localObject = c.m();
    boolean bool = ((b)localObject).a();
    if (!bool)
    {
      localObject = c.n();
      bool = ((b)localObject).a();
      if (bool)
      {
        localObject = Truepay.getInstance();
        String str = "Truepay.getInstance()";
        c.g.b.k.a(localObject, str);
        bool = ((Truepay)localObject).isRegistrationComplete();
        if (!bool) {
          return true;
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.an
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */