package com.truecaller.startup_dialogs.a;

import android.app.Activity;
import android.content.Intent;
import c.g.b.k;
import com.truecaller.common.h.an;
import com.truecaller.i.e;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.startup_dialogs.b;
import com.truecaller.utils.d;
import java.util.concurrent.TimeUnit;

public final class v
  implements b
{
  private final boolean a;
  private final StartupDialogType b;
  private final d c;
  private final com.truecaller.i.c d;
  private final e e;
  private final an f;
  
  public v(d paramd, com.truecaller.i.c paramc, e parame, an paraman)
  {
    c = paramd;
    d = paramc;
    e = parame;
    f = paraman;
    paramd = StartupDialogType.DIALOG_MDAU_PROMO;
    b = paramd;
  }
  
  public final Intent a(Activity paramActivity)
  {
    k.b(paramActivity, "fromActivity");
    k.b(paramActivity, "fromActivity");
    return null;
  }
  
  public final StartupDialogType a()
  {
    return b;
  }
  
  public final Object a(c.d.c paramc)
  {
    paramc = d;
    Object localObject1 = "hasNativeDialerCallerId";
    boolean bool1 = paramc.b((String)localObject1);
    if (bool1) {
      return Boolean.FALSE;
    }
    paramc = c;
    bool1 = paramc.d();
    if (bool1) {
      return Boolean.FALSE;
    }
    paramc = e;
    String str = "key_init_timestamp";
    String[] tmp57_54 = new String[3];
    String[] tmp58_57 = tmp57_54;
    String[] tmp58_57 = tmp57_54;
    tmp58_57[0] = "mdauPromoShownTimestamp";
    tmp58_57[1] = "key_upgrade_timestamp";
    tmp58_57[2] = str;
    localObject1 = tmp58_57;
    boolean bool2 = false;
    long l1 = 0L;
    long l2 = l1;
    int j = 0;
    Object localObject2;
    for (;;)
    {
      int k = 3;
      if (j >= k) {
        break;
      }
      localObject2 = localObject1[j];
      boolean bool3 = l2 < l1;
      if (!bool3)
      {
        long l3 = paramc.a((String)localObject2, l1);
        l2 = l3;
      }
      j += 1;
    }
    paramc = e;
    localObject1 = "mdauPromoShownTimes";
    int i = paramc.a((String)localObject1, 0);
    long l4;
    TimeUnit localTimeUnit;
    switch (i)
    {
    default: 
      break;
    case 2: 
      localObject2 = f;
      l4 = w.c();
      localTimeUnit = TimeUnit.DAYS;
      bool2 = ((an)localObject2).a(l2, l4, localTimeUnit);
      break;
    case 1: 
      localObject2 = f;
      l4 = w.b();
      localTimeUnit = TimeUnit.DAYS;
      bool2 = ((an)localObject2).a(l2, l4, localTimeUnit);
      break;
    case 0: 
      localObject2 = f;
      l4 = w.a();
      localTimeUnit = TimeUnit.DAYS;
      bool2 = ((an)localObject2).a(l2, l4, localTimeUnit);
    }
    return Boolean.valueOf(bool2);
  }
  
  public final void a(StartupDialogDismissReason paramStartupDialogDismissReason) {}
  
  public final boolean b()
  {
    return a;
  }
  
  public final void c()
  {
    e locale = e;
    long l = f.a();
    locale.b("mdauPromoShownTimestamp", l);
    e.a_("mdauPromoShownTimes");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */