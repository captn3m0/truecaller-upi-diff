package com.truecaller.startup_dialogs.a;

import dagger.a.d;
import javax.inject.Provider;

public final class ad
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private ad(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
  }
  
  public static ad a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    ad localad = new com/truecaller/startup_dialogs/a/ad;
    localad.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localad;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */