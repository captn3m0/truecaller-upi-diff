package com.truecaller.startup_dialogs.a;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import c.g.b.k;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.calling.recorder.CallRecordingOnBoardingLaunchContext;
import com.truecaller.calling.recorder.FreeTrialStatus;
import com.truecaller.calling.recorder.h;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.startup_dialogs.fragments.CallRecordingOnBoardingDialog;
import com.truecaller.startup_dialogs.fragments.CallRecordingOnBoardingDialog.Companion;
import com.truecaller.startup_dialogs.fragments.CallRecordingOnBoardingDialog.Companion.Mode;
import com.truecaller.util.al;
import com.truecaller.util.cn;

public final class e
  implements com.truecaller.startup_dialogs.b
{
  private final StartupDialogType a;
  private final boolean b;
  private final com.truecaller.common.f.c c;
  private final h d;
  private final com.truecaller.common.g.a e;
  private final al f;
  private final cn g;
  private final com.truecaller.utils.a h;
  private final CallRecordingManager i;
  
  public e(com.truecaller.common.f.c paramc, h paramh, com.truecaller.common.g.a parama, al paramal, cn paramcn, com.truecaller.utils.a parama1, CallRecordingManager paramCallRecordingManager)
  {
    c = paramc;
    d = paramh;
    e = parama;
    f = paramal;
    g = paramcn;
    h = parama1;
    i = paramCallRecordingManager;
    paramc = StartupDialogType.CALL_RECORDING_PREMIUM_NUDGE;
    a = paramc;
  }
  
  public final Intent a(Activity paramActivity)
  {
    k.b(paramActivity, "fromActivity");
    k.b(paramActivity, "fromActivity");
    return null;
  }
  
  public final StartupDialogType a()
  {
    return a;
  }
  
  public final Object a(c.d.c paramc)
  {
    paramc = e;
    long l1 = 0L;
    long l2 = paramc.a("keyCallRecordingProNudgeShownCount", l1);
    paramc = e;
    String str = "keyCallRecordingProNudgeLastShown";
    long l3 = paramc.a(str, l1);
    paramc = i.n();
    boolean bool1 = true;
    Object localObject1 = new String[bool1];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("shouldShow:: Free trial status: ");
    ((StringBuilder)localObject2).append(paramc);
    ((StringBuilder)localObject2).append(" Shown count: ");
    ((StringBuilder)localObject2).append(l2);
    ((StringBuilder)localObject2).append(" last shown: ");
    org.a.a.b localb = new org/a/a/b;
    localb.<init>(l3);
    ((StringBuilder)localObject2).append(localb);
    ((StringBuilder)localObject2).append(' ');
    localObject2 = ((StringBuilder)localObject2).toString();
    localb = null;
    localObject1[0] = localObject2;
    localObject1 = f;
    boolean bool2 = ((al)localObject1).a();
    if (bool2)
    {
      localObject1 = g;
      bool2 = ((cn)localObject1).b();
      if (!bool2)
      {
        localObject1 = d;
        bool2 = ((h)localObject1).a();
        if (bool2)
        {
          localObject1 = c;
          bool2 = ((com.truecaller.common.f.c)localObject1).d();
          if (!bool2)
          {
            localObject1 = FreeTrialStatus.EXPIRED;
            if (paramc == localObject1)
            {
              long l4 = 3;
              boolean bool3 = l2 < l4;
              if (bool3)
              {
                paramc = new org/a/a/b;
                paramc.<init>(l3);
                int j = 5;
                paramc = paramc.a(j);
                str = "DateTime(lastShownTimeStamp).plusDays(nudgeDelay)";
                k.a(paramc, str);
                l3 = org.a.a.e.a();
                bool3 = paramc.d(l3);
                if (bool3) {
                  break label306;
                }
              }
            }
          }
        }
      }
    }
    bool1 = false;
    label306:
    return Boolean.valueOf(bool1);
  }
  
  public final void a(StartupDialogDismissReason paramStartupDialogDismissReason) {}
  
  public final boolean b()
  {
    return b;
  }
  
  public final void c()
  {
    com.truecaller.common.g.a locala = e;
    long l = h.a();
    locala.b("keyCallRecordingProNudgeLastShown", l);
    locala = e;
    l = locala.a("keyCallRecordingProNudgeShownCount", 0L) + 1L;
    locala.b("keyCallRecordingProNudgeShownCount", l);
  }
  
  public final Fragment d()
  {
    Object localObject = CallRecordingOnBoardingDialog.e;
    localObject = CallRecordingOnBoardingDialog.Companion.Mode.ON_EXPIRY;
    CallRecordingOnBoardingLaunchContext localCallRecordingOnBoardingLaunchContext = CallRecordingOnBoardingLaunchContext.DIALOG;
    return (Fragment)CallRecordingOnBoardingDialog.Companion.a((CallRecordingOnBoardingDialog.Companion.Mode)localObject, localCallRecordingOnBoardingLaunchContext);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */