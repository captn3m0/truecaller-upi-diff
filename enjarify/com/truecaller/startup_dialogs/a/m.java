package com.truecaller.startup_dialogs.a;

import c.g.b.k;

final class m
  implements Comparable
{
  final int a;
  final String b;
  
  public m(int paramInt, String paramString)
  {
    a = paramInt;
    b = paramString;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof m;
      if (bool2)
      {
        paramObject = (m)paramObject;
        int i = a;
        int j = a;
        String str;
        if (i == j)
        {
          i = 1;
        }
        else
        {
          i = 0;
          str = null;
        }
        if (i != 0)
        {
          str = b;
          paramObject = b;
          boolean bool3 = k.a(str, paramObject);
          if (bool3) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = a * 31;
    String str = b;
    int j;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Entry(day=");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(", type=");
    String str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */