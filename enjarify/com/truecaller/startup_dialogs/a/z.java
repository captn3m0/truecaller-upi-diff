package com.truecaller.startup_dialogs.a;

import dagger.a.d;
import javax.inject.Provider;

public final class z
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private z(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
  }
  
  public static z a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    z localz = new com/truecaller/startup_dialogs/a/z;
    localz.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localz;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */