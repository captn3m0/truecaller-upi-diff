package com.truecaller.startup_dialogs.a;

import android.app.Activity;
import android.content.Intent;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.o.b;
import com.google.common.base.Strings;
import com.truecaller.callhistory.z;
import com.truecaller.common.h.an;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.startup_dialogs.b;
import com.truecaller.util.al;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.bg;

public final class a
  implements b
{
  final com.truecaller.callhistory.a a;
  private final boolean b;
  private final StartupDialogType c;
  private final al d;
  private final com.truecaller.i.e e;
  private final an f;
  private final f g;
  
  public a(al paramal, com.truecaller.i.e parame, an paraman, com.truecaller.callhistory.a parama, f paramf)
  {
    d = paramal;
    e = parame;
    f = paraman;
    a = parama;
    g = paramf;
    paramal = StartupDialogType.POPUP_ADD_ACCOUNT;
    c = paramal;
  }
  
  public final Intent a(Activity paramActivity)
  {
    k.b(paramActivity, "fromActivity");
    k.b(paramActivity, "fromActivity");
    return null;
  }
  
  public final StartupDialogType a()
  {
    return c;
  }
  
  public final Object a(c paramc)
  {
    a locala = this;
    Object localObject1 = paramc;
    boolean bool1 = paramc instanceof a.a;
    if (bool1)
    {
      localObject2 = paramc;
      localObject2 = (a.a)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        int k = b - j;
        b = k;
        break label83;
      }
    }
    Object localObject2 = new com/truecaller/startup_dialogs/a/a$a;
    ((a.a)localObject2).<init>(locala, (c)localObject1);
    label83:
    localObject1 = a;
    Object localObject3 = c.d.a.a.a;
    int j = b;
    int n = 3;
    int i1 = 0;
    boolean bool2;
    boolean bool3;
    Object localObject4;
    Object localObject5;
    long l1;
    switch (j)
    {
    default: 
      localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)localObject1);
    case 1: 
      localObject2 = (a)d;
      bool2 = localObject1 instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool3 = localObject1 instanceof o.b;
      if (bool3) {
        break label602;
      }
      localObject1 = d;
      boolean bool4 = ((al)localObject1).a();
      if (bool4) {
        return Boolean.FALSE;
      }
      localObject1 = e;
      int m = ((com.truecaller.i.e)localObject1).a("key_add_account_sticky_times", 0);
      localObject4 = e;
      localObject5 = "key_add_account_sticky_last_time";
      l1 = 0L;
      long l2 = ((com.truecaller.i.e)localObject4).a((String)localObject5, l1);
      long l3;
      TimeUnit localTimeUnit1;
      if (m >= n)
      {
        localObject6 = f;
        l3 = 30;
        localTimeUnit1 = TimeUnit.DAYS;
        bool3 = ((an)localObject6).a(l2, l3, localTimeUnit1);
      }
      else
      {
        localObject6 = f;
        l3 = 7;
        localTimeUnit1 = TimeUnit.DAYS;
        bool3 = ((an)localObject6).a(l2, l3, localTimeUnit1);
      }
      if (!bool3) {
        return Boolean.FALSE;
      }
      Object localObject7 = (ag)bg.a;
      Object localObject6 = g;
      Object localObject8 = new com/truecaller/startup_dialogs/a/a$b;
      ((a.b)localObject8).<init>(locala, null);
      localObject8 = (m)localObject8;
      int i2 = 2;
      localObject7 = kotlinx.coroutines.e.a((ag)localObject7, (f)localObject6, (m)localObject8, i2);
      d = locala;
      e = m;
      f = l2;
      g = bool3;
      m = 1;
      b = m;
      localObject1 = ((ao)localObject7).a((c)localObject2);
      if (localObject1 == localObject3) {
        return localObject3;
      }
      localObject2 = locala;
    }
    localObject1 = (z)localObject1;
    if (localObject1 == null) {
      return Boolean.FALSE;
    }
    localObject3 = "GlobalScope.async(fetchA…          ?: return false";
    k.a(localObject1, (String)localObject3);
    do
    {
      do
      {
        do
        {
          bool2 = ((z)localObject1).moveToNext();
          if (!bool2) {
            break;
          }
          localObject3 = ((z)localObject1).d();
        } while (localObject3 == null);
        localObject4 = "historyCursor.historyEvent ?: continue";
        k.a(localObject3, (String)localObject4);
        localObject5 = f;
        l1 = ((HistoryEvent)localObject3).j();
        long l4 = 7;
        TimeUnit localTimeUnit2 = TimeUnit.DAYS;
        bool3 = ((an)localObject5).a(l1, l4, localTimeUnit2);
        if (bool3) {
          break;
        }
        localObject3 = ((HistoryEvent)localObject3).s();
        if (localObject3 != null)
        {
          localObject3 = ((Contact)localObject3).t();
        }
        else
        {
          bool2 = false;
          localObject3 = null;
        }
        bool2 = Strings.isNullOrEmpty((String)localObject3);
      } while (!bool2);
      i1 += 1;
    } while (i1 < n);
    return Boolean.TRUE;
    return Boolean.FALSE;
    label602:
    throw a;
  }
  
  public final void a(StartupDialogDismissReason paramStartupDialogDismissReason) {}
  
  public final boolean b()
  {
    return b;
  }
  
  public final void c()
  {
    com.truecaller.i.e locale = e;
    long l = f.a();
    locale.b("key_add_account_sticky_last_time", l);
    e.a_("key_add_account_sticky_times");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */