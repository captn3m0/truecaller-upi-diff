package com.truecaller.startup_dialogs.a;

import com.truecaller.common.h.an;
import com.truecaller.i.e;
import com.truecaller.notifications.i;
import com.truecaller.startup_dialogs.StartupDialogType;

public final class p
  extends ar
{
  private final boolean a;
  private final StartupDialogType b;
  private final com.truecaller.i.c c;
  private final i d;
  
  public p(com.truecaller.i.c paramc, i parami, e parame, an paraman)
  {
    super("key_notif_access_promo_last_time", parame, paraman);
    c = paramc;
    d = parami;
    paramc = StartupDialogType.POPUP_IDENTIFY_OTHERS;
    b = paramc;
  }
  
  public final StartupDialogType a()
  {
    return b;
  }
  
  public final Object a(c.d.c paramc)
  {
    Object localObject = c;
    String str = "hasNativeDialerCallerId";
    boolean bool = ((com.truecaller.i.c)localObject).b(str);
    if (!bool) {
      return Boolean.FALSE;
    }
    localObject = d;
    bool = ((i)localObject).a();
    if (bool) {
      return Boolean.FALSE;
    }
    return super.a(paramc);
  }
  
  public final boolean b()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */