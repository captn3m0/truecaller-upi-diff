package com.truecaller.startup_dialogs.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import c.d.a.a;
import c.g.a.m;
import c.g.b.k;
import c.o.b;
import com.truecaller.common.h.an;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.br;
import com.truecaller.premium.data.SubscriptionPromoEventMetaData;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.util.af;
import java.util.UUID;

public final class ag
  extends ai
{
  public static final ag.a a;
  private final StartupDialogType b;
  private final com.truecaller.i.e c;
  private final com.truecaller.featuretoggles.e d;
  private final an e;
  private final af f;
  private final com.truecaller.common.f.c g;
  private final br h;
  private final com.truecaller.common.f.b i;
  private final c.d.f j;
  
  static
  {
    ag.a locala = new com/truecaller/startup_dialogs/a/ag$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public ag(com.truecaller.i.e parame, com.truecaller.featuretoggles.e parame1, an paraman, af paramaf, com.truecaller.common.f.c paramc, br parambr, com.truecaller.common.f.b paramb, c.d.f paramf)
  {
    super((com.truecaller.featuretoggles.f)localObject2, "feature_pro_promo_popup_last_time", paraman, parame, paramaf, parame1);
    c = parame;
    d = parame1;
    e = paraman;
    f = paramaf;
    g = paramc;
    h = parambr;
    i = paramb;
    j = paramf;
    localObject1 = StartupDialogType.POPUP_PREMIUM_PROMO;
    b = ((StartupDialogType)localObject1);
  }
  
  private final boolean b(long paramLong)
  {
    af localaf1 = f;
    long l = e.a();
    int k = localaf1.e(l);
    af localaf2 = f;
    int m = localaf2.e(paramLong);
    if (k == m)
    {
      localaf1 = f;
      l = e.a();
      k = localaf1.d(l);
      localaf2 = f;
      int n = localaf2.d(paramLong);
      if (k == n) {
        return false;
      }
    }
    return true;
  }
  
  private final boolean h()
  {
    Object localObject = g;
    boolean bool1 = ((com.truecaller.common.f.c)localObject).d();
    if (bool1)
    {
      localObject = g;
      bool1 = ((com.truecaller.common.f.c)localObject).m();
      boolean bool2 = true;
      if (bool1)
      {
        bool1 = true;
      }
      else
      {
        localObject = g;
        bool1 = ((com.truecaller.common.f.c)localObject).n();
        if (bool1)
        {
          localObject = new org/a/a/b;
          long l = g.e();
          ((org.a.a.b)localObject).<init>(l);
          localObject = ((org.a.a.b)localObject).d();
          an localan = e;
          l = localan.a();
          bool1 = ((org.a.a.b)localObject).d(l);
          if (bool1)
          {
            bool1 = true;
            break label114;
          }
        }
        bool1 = false;
        localObject = null;
      }
      label114:
      if (bool1) {
        return bool2;
      }
    }
    return false;
  }
  
  public final Intent a(Activity paramActivity)
  {
    k.b(paramActivity, "fromActivity");
    paramActivity = (Context)paramActivity;
    PremiumPresenterView.LaunchContext localLaunchContext = PremiumPresenterView.LaunchContext.ONCE_PER_MONTH_POPUP;
    String str1 = g();
    SubscriptionPromoEventMetaData localSubscriptionPromoEventMetaData = new com/truecaller/premium/data/SubscriptionPromoEventMetaData;
    String str2 = UUID.randomUUID().toString();
    localSubscriptionPromoEventMetaData.<init>(str2, null);
    return br.a(paramActivity, localLaunchContext, str1, localSubscriptionPromoEventMetaData);
  }
  
  public final StartupDialogType a()
  {
    return b;
  }
  
  public final Object a(c.d.c paramc)
  {
    boolean bool1 = paramc instanceof ag.c;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (ag.c)paramc;
      int k = b;
      m = -1 << -1;
      k &= m;
      if (k != 0)
      {
        int n = b - m;
        b = n;
        break label70;
      }
    }
    Object localObject1 = new com/truecaller/startup_dialogs/a/ag$c;
    ((ag.c)localObject1).<init>(this, (c.d.c)paramc);
    label70:
    paramc = a;
    a locala = a.a;
    int m = b;
    int i2 = 1;
    ag localag;
    switch (m)
    {
    default: 
      paramc = new java/lang/IllegalStateException;
      paramc.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramc;
    case 2: 
      bool1 = paramc instanceof o.b;
      if (!bool1) {
        break label293;
      }
      throw a;
    case 1: 
      localag = (ag)d;
      boolean bool5 = paramc instanceof o.b;
      if (bool5) {
        throw a;
      }
      break;
    case 0: 
      boolean bool2 = paramc instanceof o.b;
      if (bool2) {
        break label321;
      }
      d = this;
      b = i2;
      paramc = super.a((c.d.c)localObject1);
      if (paramc == locala) {
        return locala;
      }
      localag = this;
    }
    paramc = (Boolean)paramc;
    boolean bool3 = paramc.booleanValue();
    if (bool3)
    {
      d = localag;
      int i1 = 2;
      b = i1;
      paramc = j;
      Object localObject2 = new com/truecaller/startup_dialogs/a/ag$b;
      ((ag.b)localObject2).<init>(localag, null);
      localObject2 = (m)localObject2;
      paramc = kotlinx.coroutines.g.a(paramc, (m)localObject2, (c.d.c)localObject1);
      if (paramc == locala) {
        return locala;
      }
      label293:
      paramc = (Boolean)paramc;
      boolean bool4 = paramc.booleanValue();
      if (bool4) {}
    }
    else
    {
      i2 = 0;
    }
    return Boolean.valueOf(i2);
    label321:
    throw a;
  }
  
  protected final boolean a(long paramLong)
  {
    boolean bool1 = h();
    if (bool1) {
      return super.a(paramLong);
    }
    Object localObject1 = d;
    Object localObject2 = S;
    Object localObject3 = com.truecaller.featuretoggles.e.a;
    int m = 116;
    localObject3 = localObject3[m];
    localObject1 = (com.truecaller.featuretoggles.f)((e.a)localObject2).a((com.truecaller.featuretoggles.e)localObject1, (c.l.g)localObject3);
    int n = -1;
    int k = ((com.truecaller.featuretoggles.f)localObject1).a(n);
    switch (k)
    {
    default: 
      localObject2 = c;
      localObject3 = "feature_premium_promo_popup_shown_count";
      m = 0;
      n = ((com.truecaller.i.e)localObject2).a((String)localObject3, 0);
      if (n > k) {
        return b(paramLong);
      }
      break;
    case 0: 
      return b(paramLong);
    case -1: 
      return super.a(paramLong);
    }
    boolean bool2 = super.a(paramLong);
    if (bool2)
    {
      com.truecaller.i.e locale = c;
      localObject1 = "feature_premium_promo_popup_shown_count";
      locale.a_((String)localObject1);
    }
    return bool2;
  }
  
  public final boolean e()
  {
    com.truecaller.common.f.c localc = g;
    boolean bool = localc.o();
    return !bool;
  }
  
  public final boolean f()
  {
    com.truecaller.common.f.c localc = g;
    boolean bool = localc.d();
    if (bool)
    {
      bool = h();
      if (!bool) {
        return false;
      }
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */