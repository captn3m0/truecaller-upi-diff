package com.truecaller.startup_dialogs.a;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import c.d.c;
import c.g.b.k;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.startup_dialogs.b;
import com.truecaller.voip.d;

public final class at
  implements b
{
  private final boolean a;
  private final StartupDialogType b;
  private final d c;
  
  public at(d paramd)
  {
    c = paramd;
    paramd = StartupDialogType.DIALOG_WHATS_NEW;
    b = paramd;
  }
  
  public final Intent a(Activity paramActivity)
  {
    k.b(paramActivity, "fromActivity");
    k.b(paramActivity, "fromActivity");
    return null;
  }
  
  public final StartupDialogType a()
  {
    return b;
  }
  
  public final Object a(c paramc)
  {
    return Boolean.valueOf(c.a());
  }
  
  public final void a(StartupDialogDismissReason paramStartupDialogDismissReason) {}
  
  public final boolean b()
  {
    return a;
  }
  
  public final void c() {}
  
  public final Fragment d()
  {
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.at
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */