package com.truecaller.startup_dialogs.a;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import c.g.b.k;
import c.u;
import com.google.gson.c.a;
import com.truecaller.common.h.an;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.util.af;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;

public abstract class ai
  implements com.truecaller.startup_dialogs.b
{
  private final boolean a;
  private final com.google.gson.f b;
  private List c;
  private final com.truecaller.featuretoggles.f d;
  private final String e;
  private final an f;
  private final com.truecaller.i.e g;
  private final af h;
  private final com.truecaller.featuretoggles.e i;
  
  public ai(com.truecaller.featuretoggles.f paramf, String paramString, an paraman, com.truecaller.i.e parame, af paramaf, com.truecaller.featuretoggles.e parame1)
  {
    d = paramf;
    e = paramString;
    f = paraman;
    g = parame;
    h = paramaf;
    i = parame1;
    paramf = new com/google/gson/f;
    paramf.<init>();
    b = paramf;
    paramf = Collections.emptyList();
    k.a(paramf, "Collections.emptyList()");
    c = paramf;
  }
  
  private final m a(long paramLong, boolean paramBoolean)
  {
    List localList = h();
    boolean bool1 = localList.isEmpty();
    if (bool1) {
      return aj.a();
    }
    int j = h.f(paramLong);
    bool1 = false;
    localList = null;
    Object localObject1 = null;
    Object localObject3;
    Object localObject4;
    int n;
    if (!paramBoolean)
    {
      paramBoolean = e();
      if (paramBoolean)
      {
        localObject2 = i.ao();
        paramBoolean = ((com.truecaller.featuretoggles.f)localObject2).a();
        if (!paramBoolean)
        {
          localObject2 = h();
          int k = ((List)localObject2).size();
          localObject2 = ((List)localObject2).listIterator(k);
          do
          {
            boolean bool2 = ((ListIterator)localObject2).hasPrevious();
            if (!bool2) {
              break;
            }
            localObject3 = ((ListIterator)localObject2).previous();
            localObject4 = localObject3;
            localObject4 = (m)localObject3;
            n = a;
            if (n == j)
            {
              n = 1;
            }
            else
            {
              n = 0;
              localObject4 = null;
            }
          } while (n == 0);
          localObject1 = localObject3;
          localObject1 = (m)localObject1;
          if (localObject1 == null) {
            localObject1 = aj.a();
          }
          return (m)localObject1;
        }
      }
    }
    Object localObject2 = h();
    int m = ((List)localObject2).size();
    localObject2 = ((List)localObject2).listIterator(m);
    do
    {
      boolean bool3 = ((ListIterator)localObject2).hasPrevious();
      if (!bool3) {
        break;
      }
      localObject3 = ((ListIterator)localObject2).previous();
      localObject4 = localObject3;
      localObject4 = (m)localObject3;
      n = a;
      if (n <= j)
      {
        n = 1;
      }
      else
      {
        n = 0;
        localObject4 = null;
      }
    } while (n == 0);
    localObject1 = localObject3;
    localObject1 = (m)localObject1;
    if (localObject1 == null)
    {
      m localm = (m)c.a.m.f(h());
      localObject1 = m.a(localm);
    }
    return (m)localObject1;
  }
  
  private final boolean b(long paramLong)
  {
    long l1 = f.a();
    Object localObject1 = a(l1, false);
    int j = 1;
    Object localObject2 = a(paramLong, j);
    m localm1 = aj.a();
    boolean bool1 = k.a(localObject1, localm1);
    if (!bool1)
    {
      localm1 = aj.a();
      bool1 = k.a(localObject2, localm1);
      if (!bool1)
      {
        int k = a;
        int m = -1;
        long l2;
        if (k == m)
        {
          k = a;
          m localm2 = (m)c.a.m.f(h());
          m = a;
          if (k == m)
          {
            localObject1 = h;
            l2 = f.a();
            int n = ((af)localObject1).e(l2);
            localObject2 = h;
            int i2 = ((af)localObject2).e(paramLong);
            n -= i2;
            i2 = Math.abs(n);
            if (i2 != j) {
              return j;
            }
            return false;
          }
        }
        boolean bool2 = k.a(localObject1, localObject2) ^ j;
        if (!bool2)
        {
          localObject1 = h;
          l2 = f.a();
          int i1 = ((af)localObject1).e(l2);
          localObject2 = h;
          int i4 = ((af)localObject2).e(paramLong);
          if (i1 == i4)
          {
            localObject1 = h;
            l2 = f.a();
            i1 = ((af)localObject1).d(l2);
            localObject2 = h;
            int i3 = ((af)localObject2).d(paramLong);
            if (i1 == i3) {
              return false;
            }
          }
        }
        return j;
      }
    }
    return false;
  }
  
  private final List h()
  {
    Object localObject1 = c;
    boolean bool1 = ((List)localObject1).isEmpty();
    int j;
    Object localObject2;
    int k;
    if (bool1)
    {
      localObject1 = (CharSequence)d.e();
      j = ((CharSequence)localObject1).length();
      localObject2 = null;
      k = 1;
      if (j == 0)
      {
        j = 1;
      }
      else
      {
        j = 0;
        localObject1 = null;
      }
      if (j != 0) {}
    }
    try
    {
      localObject1 = b;
      Object localObject3 = d;
      localObject3 = ((com.truecaller.featuretoggles.f)localObject3).e();
      Object localObject4 = new com/truecaller/startup_dialogs/a/ai$a;
      ((ai.a)localObject4).<init>();
      localObject4 = b;
      localObject1 = ((com.google.gson.f)localObject1).a((String)localObject3, (Type)localObject4);
      localObject3 = "gson.fromJson(feature.fi…ing, String>>>() {}.type)";
      k.a(localObject1, (String)localObject3);
      localObject1 = (List)localObject1;
      localObject1 = (Iterable)localObject1;
      localObject3 = new java/util/ArrayList;
      int m = 10;
      int n = c.a.m.a((Iterable)localObject1, m);
      ((ArrayList)localObject3).<init>(n);
      localObject3 = (Collection)localObject3;
      localObject1 = ((Iterable)localObject1).iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        Object localObject5 = ((Iterator)localObject1).next();
        localObject5 = (Map)localObject5;
        localObject5 = ((Map)localObject5).entrySet();
        localObject5 = (Iterable)localObject5;
        Object localObject6 = new java/util/ArrayList;
        int i1 = c.a.m.a((Iterable)localObject5, m);
        ((ArrayList)localObject6).<init>(i1);
        localObject6 = (Collection)localObject6;
        localObject5 = ((Iterable)localObject5).iterator();
        for (;;)
        {
          boolean bool3 = ((Iterator)localObject5).hasNext();
          if (!bool3) {
            break;
          }
          Object localObject7 = ((Iterator)localObject5).next();
          localObject7 = (Map.Entry)localObject7;
          m localm = new com/truecaller/startup_dialogs/a/m;
          Object localObject8 = ((Map.Entry)localObject7).getKey();
          localObject8 = (String)localObject8;
          int i2 = Integer.parseInt((String)localObject8);
          localObject7 = ((Map.Entry)localObject7).getValue();
          localObject7 = (String)localObject7;
          localm.<init>(i2, (String)localObject7);
          ((Collection)localObject6).add(localm);
        }
        localObject6 = (List)localObject6;
        localObject5 = c.a.m.d((List)localObject6);
        localObject5 = (m)localObject5;
        ((Collection)localObject3).add(localObject5);
      }
      localObject3 = (List)localObject3;
      localObject3 = (Iterable)localObject3;
      localObject1 = "receiver$0";
      k.b(localObject3, (String)localObject1);
      localObject1 = localObject3;
      localObject1 = (Collection)localObject3;
      j = ((Collection)localObject1).size();
      if (j <= k)
      {
        localObject1 = c.a.m.g((Iterable)localObject3);
      }
      else
      {
        localObject3 = (Collection)localObject3;
        localObject1 = new Comparable[0];
        localObject1 = ((Collection)localObject3).toArray((Object[])localObject1);
        if (localObject1 == null) {
          break label553;
        }
        if (localObject1 == null) {
          break label538;
        }
        localObject1 = (Comparable[])localObject1;
        if (localObject1 == null) {
          break label523;
        }
        localObject2 = localObject1;
        localObject2 = (Object[])localObject1;
        localObject3 = "receiver$0";
        k.b(localObject2, (String)localObject3);
        int i3 = localObject2.length;
        if (i3 > k) {
          Arrays.sort((Object[])localObject2);
        }
        localObject1 = (Object[])localObject1;
        localObject1 = c.a.f.a((Object[])localObject1);
      }
      c = ((List)localObject1);
      localObject1 = c;
      break label573;
      label523:
      localObject1 = new c/u;
      localObject2 = "null cannot be cast to non-null type kotlin.Array<kotlin.Any?>";
      ((u)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
      label538:
      localObject1 = new c/u;
      localObject2 = "null cannot be cast to non-null type kotlin.Array<T>";
      ((u)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
      label553:
      localObject1 = new c/u;
      localObject2 = "null cannot be cast to non-null type kotlin.Array<T>";
      ((u)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    localObject1 = c;
    label573:
    return (List)localObject1;
    return c;
  }
  
  public Intent a(Activity paramActivity)
  {
    k.b(paramActivity, "fromActivity");
    k.b(paramActivity, "fromActivity");
    return null;
  }
  
  public Object a(c.d.c paramc)
  {
    paramc = g;
    String str = e;
    long l1 = 0L;
    long l2 = paramc.a(str, l1);
    long l3 = g.a("promo_popup_last_shown_timestamp", l1);
    paramc = new org/a/a/b;
    paramc.<init>(l3);
    paramc = paramc.a(2);
    str = "DateTime(lastPromoShownTime).plusDays(2)";
    k.a(paramc, str);
    l3 = org.a.a.e.a();
    boolean bool1 = paramc.c(l3);
    boolean bool2 = true;
    String[] arrayOfString = new String[bool2];
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("shouldShow:: ");
    StartupDialogType localStartupDialogType = a();
    ((StringBuilder)localObject).append(localStartupDialogType);
    ((StringBuilder)localObject).append(" in cooloff : ");
    ((StringBuilder)localObject).append(bool1);
    ((StringBuilder)localObject).append(" isEligible: ");
    boolean bool3 = f();
    ((StringBuilder)localObject).append(bool3);
    ((StringBuilder)localObject).append(" period: ");
    bool3 = b(l2);
    ((StringBuilder)localObject).append(bool3);
    localObject = ((StringBuilder)localObject).toString();
    bool3 = false;
    localStartupDialogType = null;
    arrayOfString[0] = localObject;
    if (!bool1)
    {
      bool1 = f();
      if (bool1)
      {
        bool1 = a(l2);
        if (bool1) {
          break label233;
        }
      }
    }
    bool2 = false;
    str = null;
    label233:
    return Boolean.valueOf(bool2);
  }
  
  public final void a(StartupDialogDismissReason paramStartupDialogDismissReason) {}
  
  protected boolean a(long paramLong)
  {
    long l = 0L;
    boolean bool = paramLong < l;
    if (!bool) {
      return true;
    }
    return b(paramLong);
  }
  
  public final boolean b()
  {
    return a;
  }
  
  public final void c()
  {
    com.truecaller.i.e locale = g;
    String str = e;
    long l = f.a();
    locale.b(str, l);
    locale = g;
    l = f.a();
    locale.b("promo_popup_last_shown_timestamp", l);
  }
  
  public Fragment d()
  {
    return null;
  }
  
  public abstract boolean e();
  
  public abstract boolean f();
  
  public final String g()
  {
    long l = f.a();
    return ab;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.ai
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */