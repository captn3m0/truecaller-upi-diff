package com.truecaller.startup_dialogs.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import com.truecaller.common.h.an;
import com.truecaller.engagementrewards.EngagementRewardActionType;
import com.truecaller.engagementrewards.EngagementRewardState;
import com.truecaller.engagementrewards.d;
import com.truecaller.engagementrewards.s;
import com.truecaller.engagementrewards.ui.h;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.br;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.StartupDialogType;
import org.a.a.e;

public final class k
  implements com.truecaller.startup_dialogs.b
{
  private final StartupDialogType a;
  private final boolean b;
  private final an c;
  private final com.truecaller.common.f.c d;
  private final com.truecaller.engagementrewards.c e;
  private final br f;
  
  public k(an paraman, com.truecaller.common.f.c paramc, com.truecaller.engagementrewards.c paramc1, br parambr)
  {
    c = paraman;
    d = paramc;
    e = paramc1;
    f = parambr;
    paraman = StartupDialogType.ENGAGEMENT_REWARD_REDEEM;
    a = paraman;
  }
  
  public final Intent a(Activity paramActivity)
  {
    c.g.b.k.b(paramActivity, "fromActivity");
    Object localObject1 = e;
    Object localObject2 = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
    localObject1 = ((com.truecaller.engagementrewards.c)localObject1).a((EngagementRewardActionType)localObject2);
    localObject2 = EngagementRewardState.COMPLETED;
    if (localObject1 != localObject2)
    {
      paramActivity = new String[1];
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("launchActivity:: State: ");
      localStringBuilder.append(localObject1);
      localStringBuilder.append(" is wrong state to show PremiumObtained screen, returning.");
      localObject1 = localStringBuilder.toString();
      paramActivity[0] = localObject1;
      return null;
    }
    paramActivity = (Context)paramActivity;
    localObject1 = PremiumPresenterView.LaunchContext.ONBOARDING_POPUP;
    return br.c(paramActivity, (PremiumPresenterView.LaunchContext)localObject1);
  }
  
  public final StartupDialogType a()
  {
    return a;
  }
  
  public final Object a(c.d.c paramc)
  {
    paramc = e;
    boolean bool1 = paramc.b();
    com.truecaller.common.f.c localc = d;
    boolean bool2 = localc.d();
    Object localObject1 = e;
    Object localObject2 = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
    localObject2 = ((com.truecaller.engagementrewards.c)localObject1).a((EngagementRewardActionType)localObject2);
    int[] arrayOfInt = d.b;
    int i = ((EngagementRewardState)localObject2).ordinal();
    i = arrayOfInt[i];
    boolean bool4 = false;
    arrayOfInt = null;
    int j = 1;
    if (i == j)
    {
      localObject1 = b;
      localObject2 = "keyStartUpDialogShownCount";
      i = ((s)localObject1).a((String)localObject2, 0);
      int k = 4;
      if (i < k)
      {
        long l1 = 0L;
        long l2 = ((s)localObject1).a("keyStartUpDialogShownTime", l1);
        localObject1 = Long.valueOf(l2);
        localObject2 = localObject1;
        localObject2 = (Number)localObject1;
        long l3 = ((Number)localObject2).longValue();
        boolean bool3 = l3 < l1;
        if (bool3)
        {
          bool3 = true;
        }
        else
        {
          bool3 = false;
          localObject2 = null;
        }
        if (!bool3)
        {
          bool5 = false;
          localObject1 = null;
        }
        if (localObject1 != null)
        {
          l2 = ((Number)localObject1).longValue();
          localObject3 = new org/a/a/b;
          ((org.a.a.b)localObject3).<init>(l2);
          localObject1 = ((org.a.a.b)localObject3).a(j);
          localObject2 = "DateTime(it).plusDays(1)";
          c.g.b.k.a(localObject1, (String)localObject2);
          l1 = e.a();
          bool5 = ((org.a.a.a.c)localObject1).d(l1);
          break label269;
        }
        bool5 = true;
        break label269;
      }
    }
    boolean bool5 = false;
    localObject1 = null;
    label269:
    localObject2 = new String[j];
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("shouldShow:: isEnabled: ");
    ((StringBuilder)localObject3).append(bool1);
    ((StringBuilder)localObject3).append(" isPremium:");
    ((StringBuilder)localObject3).append(bool2);
    ((StringBuilder)localObject3).append(" should Show redeem:");
    ((StringBuilder)localObject3).append(bool5);
    ((StringBuilder)localObject3).append(" (state: ");
    Object localObject4 = e;
    EngagementRewardActionType localEngagementRewardActionType = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
    localObject4 = ((com.truecaller.engagementrewards.c)localObject4).a(localEngagementRewardActionType);
    ((StringBuilder)localObject3).append(localObject4);
    char c1 = ')';
    ((StringBuilder)localObject3).append(c1);
    localObject3 = ((StringBuilder)localObject3).toString();
    localObject2[0] = localObject3;
    if ((bool1) && (bool2) && (bool5)) {
      bool4 = true;
    }
    return Boolean.valueOf(bool4);
  }
  
  public final void a(StartupDialogDismissReason paramStartupDialogDismissReason) {}
  
  public final boolean b()
  {
    return b;
  }
  
  public final void c()
  {
    Object localObject = e;
    long l = c.a();
    localObject = b;
    ((s)localObject).b("keyStartUpDialogShownTime", l);
    int i = ((s)localObject).a("keyStartUpDialogShownCount", 0) + 1;
    ((s)localObject).b("keyStartUpDialogShownCount", i);
  }
  
  public final Fragment d()
  {
    Object localObject1 = e;
    Object localObject2 = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
    localObject1 = ((com.truecaller.engagementrewards.c)localObject1).a((EngagementRewardActionType)localObject2);
    localObject2 = EngagementRewardState.REDEEMED;
    if (localObject1 == localObject2)
    {
      localObject1 = new com/truecaller/engagementrewards/ui/h;
      ((h)localObject1).<init>();
    }
    else
    {
      localObject1 = null;
    }
    localObject2 = new String[1];
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("buildDialog:: Reward state: ");
    Object localObject4 = e;
    EngagementRewardActionType localEngagementRewardActionType = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
    localObject4 = ((com.truecaller.engagementrewards.c)localObject4).a(localEngagementRewardActionType);
    ((StringBuilder)localObject3).append(localObject4);
    ((StringBuilder)localObject3).append(" dialog:  ");
    ((StringBuilder)localObject3).append(localObject1);
    localObject3 = ((StringBuilder)localObject3).toString();
    localObject2[0] = localObject3;
    return (Fragment)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */