package com.truecaller.startup_dialogs.a;

import android.app.Activity;
import android.content.Intent;
import c.g.b.k;
import com.truecaller.common.g.a;
import com.truecaller.i.e;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.startup_dialogs.b;

public final class aa
  implements b
{
  private final boolean a;
  private final StartupDialogType b;
  private final com.truecaller.i.c c;
  private final e d;
  private final a e;
  
  public aa(com.truecaller.i.c paramc, e parame, a parama)
  {
    c = paramc;
    d = parame;
    e = parama;
    a = true;
    paramc = StartupDialogType.TRUECALLER_ONBOARDING;
    b = paramc;
  }
  
  public final Intent a(Activity paramActivity)
  {
    k.b(paramActivity, "fromActivity");
    k.b(paramActivity, "fromActivity");
    return null;
  }
  
  public final StartupDialogType a()
  {
    return b;
  }
  
  public final Object a(c.d.c paramc)
  {
    paramc = e;
    String str1 = "core_isReturningUser";
    boolean bool1 = paramc.b(str1);
    boolean bool2 = true;
    if (bool1)
    {
      paramc = d;
      str2 = "backupOnboardingAvailable";
      paramc.b(str2, bool2);
    }
    paramc = e;
    String str2 = "core_isReturningUser";
    bool1 = paramc.b(str2);
    if (!bool1)
    {
      paramc = d;
      str2 = "hasShownWelcome";
      bool1 = paramc.b(str2);
      if (!bool1)
      {
        paramc = c;
        str2 = "hasNativeDialerCallerId";
        bool1 = paramc.b(str2);
        if (!bool1) {
          break label116;
        }
      }
    }
    bool2 = false;
    str1 = null;
    label116:
    return Boolean.valueOf(bool2);
  }
  
  public final void a(StartupDialogDismissReason paramStartupDialogDismissReason)
  {
    d.b("backupOnboardingAvailable", true);
  }
  
  public final boolean b()
  {
    return a;
  }
  
  public final void c()
  {
    d.b("hasShownWelcome", true);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */