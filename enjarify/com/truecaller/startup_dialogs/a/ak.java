package com.truecaller.startup_dialogs.a;

import android.app.Activity;
import android.content.Intent;
import c.d.c;
import c.g.b.k;
import com.truecaller.common.h.an;
import com.truecaller.i.e;
import com.truecaller.old.data.access.f;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.startup_dialogs.b;
import java.util.concurrent.TimeUnit;

public final class ak
  implements b
{
  private final boolean a;
  private final StartupDialogType b;
  private final e c;
  private final f d;
  private final an e;
  private final String f;
  
  public ak(e parame, f paramf, an paraman, String paramString)
  {
    c = parame;
    d = paramf;
    e = paraman;
    f = paramString;
    parame = StartupDialogType.POPUP_SOFTWARE_UPDATE;
    b = parame;
  }
  
  public final Intent a(Activity paramActivity)
  {
    k.b(paramActivity, "fromActivity");
    k.b(paramActivity, "fromActivity");
    return null;
  }
  
  public final StartupDialogType a()
  {
    return b;
  }
  
  public final Object a(c paramc)
  {
    paramc = d.i();
    if (paramc == null) {
      return Boolean.FALSE;
    }
    k.a(paramc, "notificationDao.software…dateIfAny ?: return false");
    String str = "v";
    paramc = paramc.a(str);
    if (paramc == null) {
      return Boolean.FALSE;
    }
    k.a(paramc, "notification.getApplicat….VERSION) ?: return false");
    str = f;
    int i = str.compareTo(paramc);
    if (i >= 0) {
      return Boolean.FALSE;
    }
    paramc = c;
    long l1 = 0L;
    long l2 = paramc.a("key_new_version_last_time", l1);
    paramc = c;
    str = "key_new_version_promo_times";
    i = paramc.a(str, 0);
    an localan;
    long l3;
    TimeUnit localTimeUnit;
    boolean bool;
    switch (i)
    {
    default: 
      localan = e;
      l3 = al.c();
      localTimeUnit = TimeUnit.DAYS;
      bool = localan.a(l2, l3, localTimeUnit);
      break;
    case 1: 
      localan = e;
      l3 = al.b();
      localTimeUnit = TimeUnit.DAYS;
      bool = localan.a(l2, l3, localTimeUnit);
      break;
    case 0: 
      localan = e;
      l3 = al.a();
      localTimeUnit = TimeUnit.DAYS;
      bool = localan.a(l2, l3, localTimeUnit);
    }
    return Boolean.valueOf(bool);
  }
  
  public final void a(StartupDialogDismissReason paramStartupDialogDismissReason) {}
  
  public final boolean b()
  {
    return a;
  }
  
  public final void c()
  {
    e locale = c;
    long l = e.a();
    locale.b("key_new_version_last_time", l);
    c.a_("key_new_version_promo_times");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.a.ak
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */