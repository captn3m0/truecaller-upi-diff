package com.truecaller.startup_dialogs.fragments;

public enum BottomPopupDialogFragment$Action
{
  private final int accept;
  private final String analyticsType;
  private final int icon;
  private final int text;
  
  static
  {
    Action[] arrayOfAction = new Action[7];
    Action localAction1 = new com/truecaller/startup_dialogs/fragments/BottomPopupDialogFragment$Action;
    Action localAction2 = localAction1;
    localAction1.<init>("MISSED_CALL_NOTIFICATION", 0, 2131233826, 2131887279, "MissedCalls");
    MISSED_CALL_NOTIFICATION = localAction1;
    arrayOfAction[0] = localAction1;
    localAction2 = new com/truecaller/startup_dialogs/fragments/BottomPopupDialogFragment$Action;
    localAction2.<init>("IDENTIFY_UNKNOWN_SENDERS", 1, 2131234191, 2131887281, "IdentifySenders");
    IDENTIFY_UNKNOWN_SENDERS = localAction2;
    arrayOfAction[1] = localAction2;
    localAction2 = new com/truecaller/startup_dialogs/fragments/BottomPopupDialogFragment$Action;
    localAction2.<init>("ADD_ACCOUNT", 2, 2131234190, 2131887280, 2131888396, "AddAccount");
    ADD_ACCOUNT = localAction2;
    arrayOfAction[2] = localAction2;
    localAction2 = new com/truecaller/startup_dialogs/fragments/BottomPopupDialogFragment$Action;
    int i = 2131886809;
    localAction2.<init>("REQUEST_LOCATION_PERMISSION", 3, 2131234259, 2131886792, i, "AccessLocation");
    REQUEST_LOCATION_PERMISSION = localAction2;
    arrayOfAction[3] = localAction2;
    localAction2 = new com/truecaller/startup_dialogs/fragments/BottomPopupDialogFragment$Action;
    localAction2.<init>("REQUEST_DRAW_PERMISSION", 4, 2131234066, 2131886791, i, "DrawOverlay");
    REQUEST_DRAW_PERMISSION = localAction2;
    arrayOfAction[4] = localAction2;
    localAction2 = new com/truecaller/startup_dialogs/fragments/BottomPopupDialogFragment$Action;
    localAction2.<init>("SOFTWARE_UPDATE", 5, 2131234645, 2131886383, 2131888148, "SoftwareUpdate");
    SOFTWARE_UPDATE = localAction2;
    arrayOfAction[5] = localAction2;
    localAction2 = new com/truecaller/startup_dialogs/fragments/BottomPopupDialogFragment$Action;
    localAction2.<init>("REQUEST_DO_NOT_DISTURB_ACCESS", 6, 2131234470, 2131886790, 2131886809, "RingSilent");
    REQUEST_DO_NOT_DISTURB_ACCESS = localAction2;
    arrayOfAction[6] = localAction2;
    $VALUES = arrayOfAction;
  }
  
  private BottomPopupDialogFragment$Action(int paramInt2, int paramInt3, int paramInt4, String paramString1)
  {
    icon = paramInt2;
    text = paramInt3;
    accept = paramInt4;
    analyticsType = paramString1;
  }
  
  private BottomPopupDialogFragment$Action(int paramInt2, int paramInt3, String paramString1)
  {
    this(paramInt2, paramInt3, -1, paramString1);
  }
  
  public final int getAccept()
  {
    return accept;
  }
  
  public final String getAnalyticsType()
  {
    return analyticsType;
  }
  
  public final int getIcon()
  {
    return icon;
  }
  
  public final int getText()
  {
    return text;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.BottomPopupDialogFragment.Action
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */