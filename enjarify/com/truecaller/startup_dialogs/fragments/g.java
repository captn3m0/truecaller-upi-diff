package com.truecaller.startup_dialogs.fragments;

import android.animation.Animator;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.f;
import android.support.v4.view.ViewPager.f;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.truecaller.TrueApp;
import com.truecaller.analytics.e.a;
import com.truecaller.backup.s;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.messaging.defaultsms.DefaultSmsActivity;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.view.DotPagerIndicator;
import com.truecaller.ui.view.NonSwipeableViewPager;
import com.truecaller.ui.view.h;
import com.truecaller.utils.l;
import java.util.ArrayList;
import java.util.Locale;

public class g
  extends a
{
  private NonSwipeableViewPager a;
  private DotPagerIndicator b;
  private Button c;
  private ArrayList d;
  private long e;
  private Handler f;
  private boolean g;
  private h h;
  
  public g()
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    d = ((ArrayList)localObject);
    localObject = new android/os/Handler;
    ((Handler)localObject).<init>();
    f = ((Handler)localObject);
    localObject = d;
    g.a locala = g.a.a;
    ((ArrayList)localObject).add(locala);
    localObject = d;
    locala = g.a.b;
    ((ArrayList)localObject).add(locala);
    localObject = d;
    locala = g.a.c;
    ((ArrayList)localObject).add(locala);
    boolean bool = Settings.f();
    if (bool)
    {
      localObject = d;
      locala = g.a.d;
      ((ArrayList)localObject).add(locala);
    }
  }
  
  private void a()
  {
    Object localObject1 = a;
    int i = ((NonSwipeableViewPager)localObject1).getCurrentItem();
    Object localObject2 = TrueApp.y().a().bw();
    com.truecaller.utils.d locald = TrueApp.y().a().bx();
    Object localObject3 = d.get(i);
    Object localObject4 = g.a.c;
    int j = 1;
    int k;
    if (localObject3 == localObject4)
    {
      k = 1;
    }
    else
    {
      k = 0;
      localObject3 = null;
    }
    localObject4 = new String[] { "android.permission.READ_SMS" };
    boolean bool = ((l)localObject2).a((String[])localObject4);
    if (bool)
    {
      bool = locald.a();
      if (bool)
      {
        bool = false;
        localObject2 = null;
        break label126;
      }
    }
    bool = true;
    label126:
    if ((k != 0) && (bool))
    {
      bool = g;
      if (!bool)
      {
        g = j;
        try
        {
          localObject1 = getContext();
          if (localObject1 == null) {
            return;
          }
          localObject2 = "onboarding";
          localObject1 = DefaultSmsActivity.a((Context)localObject1, (String)localObject2);
          startActivityForResult((Intent)localObject1, j);
          localObject1 = c;
          ((Button)localObject1).setEnabled(false);
          return;
        }
        catch (ActivityNotFoundException localActivityNotFoundException)
        {
          com.truecaller.log.d.a(localActivityNotFoundException);
          c.setEnabled(j);
          return;
        }
      }
    }
    localObject2 = d;
    int m = ((ArrayList)localObject2).size() - j;
    if (i == m)
    {
      localObject1 = TrueApp.y().a().c();
      localObject2 = new com/truecaller/analytics/e$a;
      ((e.a)localObject2).<init>("ONBOARDING_Finished");
      localObject2 = ((e.a)localObject2).a();
      ((com.truecaller.analytics.b)localObject1).a((com.truecaller.analytics.e)localObject2);
      dismissAllowingStateLoss();
      return;
    }
    localObject2 = a;
    i += j;
    ((NonSwipeableViewPager)localObject2).setCurrentItem(i);
  }
  
  private void b()
  {
    com.truecaller.analytics.b localb = TrueApp.y().a().c();
    Object localObject1 = new com/truecaller/analytics/e$a;
    Object localObject2 = Locale.US;
    int i = 1;
    Object[] arrayOfObject = new Object[i];
    Integer localInteger = Integer.valueOf(a.getCurrentItem() + i);
    arrayOfObject[0] = localInteger;
    localObject2 = String.format((Locale)localObject2, "ONBOARDING_Step_%d", arrayOfObject);
    ((e.a)localObject1).<init>((String)localObject2);
    long l1 = System.currentTimeMillis();
    long l2 = e;
    l1 -= l2;
    localObject1 = ((e.a)localObject1).a("time", l1).a();
    localb.a((com.truecaller.analytics.e)localObject1);
    long l3 = System.currentTimeMillis();
    e = l3;
  }
  
  public final String c()
  {
    return "Onboarding";
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    int i = 1;
    if (paramInt1 == i)
    {
      paramInt1 = -1;
      if (paramInt2 != paramInt1)
      {
        Object localObject1 = getContext();
        if (localObject1 == null) {
          return;
        }
        Object localObject2 = new android/support/v7/app/AlertDialog$Builder;
        ((AlertDialog.Builder)localObject2).<init>((Context)localObject1);
        localObject2 = ((AlertDialog.Builder)localObject2).setCancelable(false).setTitle(2131887150).setMessage(2131886783);
        -..Lambda.g.u0URKHIHI5V-93Y99i9dcIxVdIc localu0URKHIHI5V-93Y99i9dcIxVdIc = new com/truecaller/startup_dialogs/fragments/-$$Lambda$g$u0URKHIHI5V-93Y99i9dcIxVdIc;
        localu0URKHIHI5V-93Y99i9dcIxVdIc.<init>(this, (Context)localObject1);
        localObject1 = ((AlertDialog.Builder)localObject2).setPositiveButton(2131887202, localu0URKHIHI5V-93Y99i9dcIxVdIc);
        paramIntent = new com/truecaller/startup_dialogs/fragments/-$$Lambda$g$mkHzWI-gvtFEahjJ-Y9kykm1ASs;
        paramIntent.<init>(this);
        localObject1 = ((AlertDialog.Builder)localObject1).setNegativeButton(2131886566, paramIntent);
        localObject2 = new com/truecaller/startup_dialogs/fragments/-$$Lambda$g$ZzJbOGP0cZuXCTQkMPcfpHDxANc;
        ((-..Lambda.g.ZzJbOGP0cZuXCTQkMPcfpHDxANc)localObject2).<init>(this);
        ((AlertDialog.Builder)localObject1).setOnDismissListener((DialogInterface.OnDismissListener)localObject2).show();
        return;
      }
      c.setEnabled(i);
      a();
      return;
    }
    i = 2;
    if (paramInt1 == i)
    {
      a();
      return;
    }
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = ((TrueApp)paramContext.getApplicationContext()).a();
    Object localObject1 = paramContext.aF().l();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool)
    {
      localObject1 = paramContext.T();
      bool = ((r)localObject1).c();
      if (bool)
      {
        localObject1 = paramContext.bx().l();
        Object localObject2 = "kenzo";
        bool = ((String)localObject1).equals(localObject2);
        if (!bool)
        {
          localObject1 = s.a;
          localObject1 = s.a();
          localObject2 = new com/truecaller/startup_dialogs/fragments/g$3;
          ((g.3)localObject2).<init>(this, paramContext);
          ((e.b)localObject1).a((e.d)localObject2);
          return;
        }
      }
    }
  }
  
  public void onClick(View paramView)
  {
    a();
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new android/support/v7/app/AppCompatDialog;
    f localf = getActivity();
    paramBundle.<init>(localf, 2131952156);
    return paramBundle;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2131559062, paramViewGroup, false);
    paramViewGroup = (NonSwipeableViewPager)paramLayoutInflater.findViewById(2131365476);
    a = paramViewGroup;
    paramViewGroup = (DotPagerIndicator)paramLayoutInflater.findViewById(2131363866);
    b = paramViewGroup;
    paramViewGroup = (Button)paramLayoutInflater.findViewById(2131363805);
    c = paramViewGroup;
    return paramLayoutInflater;
  }
  
  public void onDismiss(DialogInterface paramDialogInterface)
  {
    Object localObject = h;
    if (localObject != null)
    {
      localObject = a;
      ((Animator)localObject).cancel();
    }
    com.truecaller.common.b.a.F();
    b();
    super.onDismiss(paramDialogInterface);
  }
  
  public void onResume()
  {
    super.onResume();
    long l = System.currentTimeMillis();
    e = l;
  }
  
  public void onStop()
  {
    super.onStop();
    f.removeCallbacksAndMessages(null);
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    paramView = b;
    int i = d.size();
    paramView.setNumberOfPages(i);
    b.setFirstPage(0);
    paramView = a;
    paramBundle = new com/truecaller/startup_dialogs/fragments/g$1;
    paramBundle.<init>(this);
    paramView.setAdapter(paramBundle);
    paramView = new com/truecaller/startup_dialogs/fragments/g$2;
    paramView.<init>(this);
    a.a(paramView);
    paramBundle = a;
    Object localObject = b;
    paramBundle.a((ViewPager.f)localObject);
    paramBundle = a;
    localObject = new com/truecaller/startup_dialogs/fragments/-$$Lambda$g$7Os4plezPxvASKjiK6dw-7sfYSk;
    ((-..Lambda.g.7Os4plezPxvASKjiK6dw-7sfYSk)localObject).<init>(this, paramView);
    paramBundle.post((Runnable)localObject);
    c.setOnClickListener(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */