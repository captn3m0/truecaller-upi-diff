package com.truecaller.startup_dialogs.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import c.d.f.c;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import java.util.HashMap;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bn;
import kotlinx.coroutines.bs;

public final class d
  extends a
  implements ag
{
  public c.d.f a;
  public com.truecaller.common.profile.e b;
  private final String c = "FillProfile";
  private HashMap d;
  
  public final c.d.f V_()
  {
    c.d.f localf = a;
    if (localf == null)
    {
      String str = "coroutineContext";
      c.g.b.k.a(str);
    }
    return localf;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final String c()
  {
    return c;
  }
  
  protected final void d()
  {
    Object localObject1 = getView();
    int i;
    if (localObject1 != null)
    {
      i = 2131363000;
      localObject1 = (EditText)((View)localObject1).findViewById(i);
      if (localObject1 != null)
      {
        localObject1 = ((EditText)localObject1).getText();
        if (localObject1 != null)
        {
          localObject1 = localObject1.toString();
          if (localObject1 != null) {
            if (localObject1 != null)
            {
              localObject1 = c.n.m.b((CharSequence)localObject1).toString();
              if (localObject1 != null) {
                break label80;
              }
            }
            else
            {
              localObject1 = new c/u;
              ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.CharSequence");
              throw ((Throwable)localObject1);
            }
          }
        }
      }
    }
    localObject1 = "";
    label80:
    Object localObject2 = localObject1;
    localObject1 = getView();
    if (localObject1 != null)
    {
      i = 2131363001;
      localObject1 = (EditText)((View)localObject1).findViewById(i);
      if (localObject1 != null)
      {
        localObject1 = ((EditText)localObject1).getText();
        if (localObject1 != null)
        {
          localObject1 = localObject1.toString();
          if (localObject1 != null) {
            if (localObject1 != null)
            {
              localObject1 = c.n.m.b((CharSequence)localObject1).toString();
              if (localObject1 != null) {
                break label162;
              }
            }
            else
            {
              localObject1 = new c/u;
              ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.CharSequence");
              throw ((Throwable)localObject1);
            }
          }
        }
      }
    }
    localObject1 = "";
    label162:
    Object localObject3 = localObject1;
    com.truecaller.ui.dialogs.k localk = com.truecaller.ui.dialogs.k.c(2131888051);
    c.g.b.k.a(localk, "ProgressDialogFragment.n…ring.fill_profile_saving)");
    localObject1 = getActivity();
    com.truecaller.ui.dialogs.a.a(localk, (android.support.v4.app.f)localObject1);
    Object localObject4 = new com/truecaller/startup_dialogs/fragments/d$a;
    ((d.a)localObject4).<init>(this, (String)localObject2, (String)localObject3, localk, null);
    localObject4 = (c.g.a.m)localObject4;
    kotlinx.coroutines.e.b(this, null, (c.g.a.m)localObject4, 3);
    super.d();
  }
  
  public final void f()
  {
    HashMap localHashMap = d;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  protected final boolean g()
  {
    return true;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558568, paramViewGroup, false);
  }
  
  public final void onDestroyView()
  {
    Object localObject = V_();
    c.g.b.k.b(localObject, "receiver$0");
    f.c localc = (f.c)bn.c;
    localObject = (bn)((c.d.f)localObject).get(localc);
    if (localObject != null) {
      ((bn)localObject).n();
    }
    super.onDestroyView();
    f();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    c.g.b.k.b(paramView, "view");
    paramBundle = paramView.getContext();
    Object localObject = "view.context";
    c.g.b.k.a(paramBundle, (String)localObject);
    paramBundle = paramBundle.getApplicationContext();
    if (paramBundle != null)
    {
      paramBundle = ((TrueApp)paramBundle).a();
      c.g.b.k.a(paramBundle, "(view.context.applicatio… as TrueApp).objectsGraph");
      localObject = paramBundle.bf();
      c.g.b.k.a(localObject, "objectsGraph.profileRepository()");
      b = ((com.truecaller.common.profile.e)localObject);
      localObject = bs.a(null);
      paramBundle = paramBundle.bl();
      String str = "objectsGraph.uiCoroutineContext()";
      c.g.b.k.a(paramBundle, str);
      paramBundle = ((bn)localObject).plus(paramBundle);
      localObject = "<set-?>";
      c.g.b.k.b(paramBundle, (String)localObject);
      a = paramBundle;
      int i = 2131362303;
      paramBundle = (TextView)paramView.findViewById(i);
      if (paramBundle != null)
      {
        localObject = this;
        localObject = (View.OnClickListener)this;
        paramBundle.setOnClickListener((View.OnClickListener)localObject);
      }
      i = 2131362326;
      paramView = (TextView)paramView.findViewById(i);
      if (paramView != null)
      {
        paramBundle = this;
        paramBundle = (View.OnClickListener)this;
        paramView.setOnClickListener(paramBundle);
        return;
      }
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type com.truecaller.TrueApp");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */