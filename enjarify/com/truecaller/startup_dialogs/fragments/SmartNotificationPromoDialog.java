package com.truecaller.startup_dialogs.fragments;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.content.b;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.R.id;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.messaging.defaultsms.DefaultSmsActivity;
import com.truecaller.ui.SettingsFragment;
import com.truecaller.ui.SettingsFragment.SettingsViewType;
import com.truecaller.utils.d;
import java.util.HashMap;

public final class SmartNotificationPromoDialog
  extends m
{
  private final boolean a;
  private HashMap b;
  
  public SmartNotificationPromoDialog()
  {
    TrueApp localTrueApp = TrueApp.y();
    k.a(localTrueApp, "TrueApp.getApp()");
    boolean bool = localTrueApp.a().bx().d();
    a = bool;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void b()
  {
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "context ?: return");
    int i = R.id.title_text;
    Object localObject2 = (TextView)a(i);
    k.a(localObject2, "title_text");
    Object localObject3 = (CharSequence)getString(2131887328);
    ((TextView)localObject2).setText((CharSequence)localObject3);
    i = R.id.subtitle;
    localObject2 = (TextView)a(i);
    k.a(localObject2, "subtitle");
    int j = 2131887329;
    localObject3 = (CharSequence)getString(j);
    ((TextView)localObject2).setText((CharSequence)localObject3);
    i = R.id.logo;
    localObject2 = (ImageView)a(i);
    localObject3 = "logo";
    k.a(localObject2, (String)localObject3);
    localObject2 = ((ImageView)localObject2).getLayoutParams();
    if (localObject2 != null)
    {
      localObject2 = (LinearLayout.LayoutParams)localObject2;
      j = topMargin;
      topMargin = 0;
      i = R.id.logo;
      ((ImageView)a(i)).setPadding(0, j, 0, j);
      i = R.id.logo;
      ((ImageView)a(i)).setImageResource(2131234558);
      i = R.id.logo;
      localObject2 = (ImageView)a(i);
      int k = b.c((Context)localObject1, 2131100594);
      ((ImageView)localObject2).setBackgroundColor(k);
      k = R.id.button_accept;
      localObject1 = (Button)a(k);
      k.a(localObject1, "button_accept");
      localObject2 = (CharSequence)getString(2131887217);
      ((Button)localObject1).setText((CharSequence)localObject2);
      k = R.id.button_dismiss;
      localObject1 = (Button)a(k);
      k.a(localObject1, "button_dismiss");
      localObject2 = (CharSequence)getString(2131887330);
      ((Button)localObject1).setText((CharSequence)localObject2);
      return;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type android.widget.LinearLayout.LayoutParams");
    throw ((Throwable)localObject1);
  }
  
  protected final void d()
  {
    boolean bool = a;
    if (!bool)
    {
      localObject = getContext();
      if (localObject == null) {
        return;
      }
      k.a(localObject, "context ?: return");
      String str = "SmartNotification";
      localObject = DefaultSmsActivity.a((Context)localObject, str);
      startActivity((Intent)localObject);
    }
    Object localObject = getActivity();
    if (localObject != null)
    {
      ((f)localObject).finish();
      return;
    }
  }
  
  protected final void e()
  {
    Object localObject = (Context)getActivity();
    SettingsFragment.SettingsViewType localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_GENERAL;
    SettingsFragment.b((Context)localObject, localSettingsViewType);
    localObject = getActivity();
    if (localObject != null)
    {
      ((f)localObject).finish();
      return;
    }
  }
  
  public final void f()
  {
    HashMap localHashMap = b;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.SmartNotificationPromoDialog
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */