package com.truecaller.startup_dialogs.fragments;

import android.os.Bundle;
import android.support.v4.app.e;
import android.support.v4.app.j;
import c.g.b.k;
import com.truecaller.calling.recorder.CallRecordingOnBoardingLaunchContext;
import java.io.Serializable;

public final class CallRecordingOnBoardingDialog$Companion
{
  public static e a(CallRecordingOnBoardingDialog.Companion.Mode paramMode, CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext)
  {
    k.b(paramMode, "mode");
    k.b(paramCallRecordingOnBoardingLaunchContext, "launchContext");
    CallRecordingOnBoardingDialog localCallRecordingOnBoardingDialog = new com/truecaller/startup_dialogs/fragments/CallRecordingOnBoardingDialog;
    localCallRecordingOnBoardingDialog.<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramMode = (Serializable)paramMode;
    localBundle.putSerializable("KEY_MODE", paramMode);
    paramCallRecordingOnBoardingLaunchContext = (Serializable)paramCallRecordingOnBoardingLaunchContext;
    localBundle.putSerializable("LaunchContext", paramCallRecordingOnBoardingLaunchContext);
    localCallRecordingOnBoardingDialog.setArguments(localBundle);
    return (e)localCallRecordingOnBoardingDialog;
  }
  
  public static void a(j paramj, boolean paramBoolean, CallRecordingOnBoardingLaunchContext paramCallRecordingOnBoardingLaunchContext)
  {
    k.b(paramj, "fragmentManager");
    String str = "launchContext";
    k.b(paramCallRecordingOnBoardingLaunchContext, str);
    if (paramBoolean) {
      localObject = CallRecordingOnBoardingDialog.Companion.Mode.ON_BOARDING_PREMIUM_USER;
    } else {
      localObject = CallRecordingOnBoardingDialog.Companion.Mode.ON_BOARDING;
    }
    Object localObject = a((CallRecordingOnBoardingDialog.Companion.Mode)localObject, paramCallRecordingOnBoardingLaunchContext);
    paramCallRecordingOnBoardingLaunchContext = CallRecordingOnBoardingDialog.class.getName();
    ((e)localObject).show(paramj, paramCallRecordingOnBoardingLaunchContext);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.CallRecordingOnBoardingDialog.Companion
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */