package com.truecaller.startup_dialogs.fragments;

import android.content.Context;
import android.widget.Toast;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.common.profile.e;
import com.truecaller.common.profile.f;
import com.truecaller.common.profile.g;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlinx.coroutines.ag;

final class d$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag g;
  
  d$a(d paramd, String paramString1, String paramString2, com.truecaller.ui.dialogs.k paramk, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/startup_dialogs/fragments/d$a;
    d locald = c;
    String str1 = d;
    String str2 = e;
    com.truecaller.ui.dialogs.k localk = f;
    locala.<init>(locald, str1, str2, localk, paramc);
    paramObject = (ag)paramObject;
    g = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    boolean bool2;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label201;
      }
      try
      {
        paramObject = (o.b)paramObject;
        paramObject = a;
        throw ((Throwable)paramObject);
      }
      finally
      {
        break label273;
      }
    }
    boolean bool1 = paramObject instanceof o.b;
    if (!bool1)
    {
      paramObject = new java/util/LinkedHashMap;
      ((LinkedHashMap)paramObject).<init>();
      Object localObject2 = paramObject;
      localObject2 = (Map)paramObject;
      paramObject = "first_name";
      Object localObject3 = d;
      ((Map)localObject2).put(paramObject, localObject3);
      paramObject = "last_name";
      localObject3 = e;
      ((Map)localObject2).put(paramObject, localObject3);
      paramObject = c;
      localObject3 = b;
      if (localObject3 == null)
      {
        paramObject = "profileRepository";
        c.g.b.k.a((String)paramObject);
      }
      a = localObject2;
      int j = 1;
      b = j;
      paramObject = ((e)localObject3).a(false, null, false, null, (Map)localObject2, false, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      label201:
      paramObject = (f)paramObject;
      bool2 = a;
      if (bool2)
      {
        paramObject = c;
        ((d)paramObject).dismissAllowingStateLoss();
      }
      else
      {
        localObject1 = c;
        localObject1 = ((d)localObject1).getContext();
        j = g.a((f)paramObject);
        bool1 = false;
        localObject3 = null;
        paramObject = Toast.makeText((Context)localObject1, j, 0);
        ((Toast)paramObject).show();
      }
      f.dismissAllowingStateLoss();
      return x.a;
      label273:
      f.dismissAllowingStateLoss();
      throw ((Throwable)paramObject);
    }
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */