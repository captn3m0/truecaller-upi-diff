package com.truecaller.startup_dialogs.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog.Builder;
import android.view.View;
import android.view.View.OnClickListener;
import c.g.b.k;
import java.util.HashMap;

public final class h
  extends a
  implements View.OnClickListener
{
  private final String a = "PinShortcutRequest";
  private HashMap b;
  
  public final View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final String c()
  {
    return a;
  }
  
  public final void f()
  {
    HashMap localHashMap = b;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    Object localObject = getContext();
    if (localObject != null)
    {
      AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
      localBuilder.<init>((Context)localObject);
      int i = 2131887402;
      localObject = localBuilder.setMessage(i);
      localBuilder = null;
      localObject = ((AlertDialog.Builder)localObject).setCancelable(false);
      DialogInterface.OnClickListener localOnClickListener = (DialogInterface.OnClickListener)h.a.a;
      localObject = ((AlertDialog.Builder)localObject).setPositiveButton(2131887400, localOnClickListener);
      int j = 2131888828;
      localOnClickListener = null;
      localObject = ((AlertDialog.Builder)localObject).setNegativeButton(j, null).create();
      if (localObject != null) {
        return (Dialog)localObject;
      }
    }
    paramBundle = super.onCreateDialog(paramBundle);
    k.a(paramBundle, "super.onCreateDialog(savedInstanceState)");
    return paramBundle;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */