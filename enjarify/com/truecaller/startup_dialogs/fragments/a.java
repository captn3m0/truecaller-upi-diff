package com.truecaller.startup_dialogs.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import c.g.b.k;
import c.u;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.bk;
import com.truecaller.bp;
import java.util.HashMap;

public abstract class a
  extends j
  implements View.OnClickListener
{
  private final e.a a;
  private b b;
  private HashMap c;
  
  public a()
  {
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("StartupDialog");
    a = locala;
  }
  
  public View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public abstract String c();
  
  protected void d() {}
  
  protected void e() {}
  
  public void f()
  {
    HashMap localHashMap = c;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  protected boolean g()
  {
    return false;
  }
  
  public void onCancel(DialogInterface paramDialogInterface)
  {
    paramDialogInterface = b;
    if (paramDialogInterface == null)
    {
      localObject = "analytics";
      k.a((String)localObject);
    }
    Object localObject = a.a("Action", "DialogCancelled").a();
    k.a(localObject, "analyticsEvent\n         …\n                .build()");
    paramDialogInterface.a((e)localObject);
  }
  
  public void onClick(View paramView)
  {
    Object localObject = "view";
    k.b(paramView, (String)localObject);
    int i = paramView.getId();
    int j = 2131362303;
    String str1;
    String str2;
    if (i == j)
    {
      paramView = b;
      if (paramView == null)
      {
        localObject = "analytics";
        k.a((String)localObject);
      }
      localObject = a;
      str1 = "PositiveBtnClicked";
      localObject = ((e.a)localObject).a("Action", str1).a();
      str2 = "analyticsEvent\n         …                 .build()";
      k.a(localObject, str2);
      paramView.a((e)localObject);
      d();
      boolean bool = g();
      if (bool) {
        dismissAllowingStateLoss();
      }
    }
    else
    {
      int k = paramView.getId();
      i = 2131362326;
      if (k == i)
      {
        paramView = b;
        if (paramView == null)
        {
          localObject = "analytics";
          k.a((String)localObject);
        }
        localObject = a;
        str1 = "NegativeBtnClicked";
        localObject = ((e.a)localObject).a("Action", str1).a();
        str2 = "analyticsEvent\n         …                 .build()";
        k.a(localObject, str2);
        paramView.a((e)localObject);
        e();
        dismissAllowingStateLoss();
      }
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getContext();
    if (paramBundle == null) {
      return;
    }
    Object localObject = "context ?: return";
    k.a(paramBundle, (String)localObject);
    paramBundle = paramBundle.getApplicationContext();
    if (paramBundle != null)
    {
      paramBundle = ((bk)paramBundle).a().c();
      b = paramBundle;
      paramBundle = b;
      if (paramBundle == null)
      {
        localObject = "analytics";
        k.a((String)localObject);
      }
      localObject = a;
      String str = c();
      localObject = ((e.a)localObject).a("Type", str).a("Action", "Shown").a();
      k.a(localObject, "analyticsEvent\n         …\n                .build()");
      paramBundle.a((e)localObject);
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramBundle;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */