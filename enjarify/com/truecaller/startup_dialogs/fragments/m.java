package com.truecaller.startup_dialogs.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.ui.ThemeManager.Theme;
import java.util.HashMap;

public abstract class m
  extends a
{
  public static final int f = 21;
  public static final m.a g;
  private final String a = "WhatsNew";
  private HashMap b;
  
  static
  {
    m.a locala = new com/truecaller/startup_dialogs/fragments/m$a;
    locala.<init>((byte)0);
    g = locala;
  }
  
  public int a()
  {
    return 2131558603;
  }
  
  public View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public abstract void b();
  
  public String c()
  {
    return a;
  }
  
  public void f()
  {
    HashMap localHashMap = b;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setCancelable(false);
    setStyle(0, 2131952158);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    paramBundle = new android/view/ContextThemeWrapper;
    paramLayoutInflater = paramLayoutInflater.getContext();
    int i = DEFAULTresId;
    paramBundle.<init>(paramLayoutInflater, i);
    paramLayoutInflater = LayoutInflater.from((Context)paramBundle);
    int j = a();
    return paramLayoutInflater.inflate(j, paramViewGroup, false);
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.button_accept;
    paramView = (Button)a(i);
    if (paramView != null)
    {
      paramBundle = this;
      paramBundle = (View.OnClickListener)this;
      paramView.setOnClickListener(paramBundle);
    }
    i = R.id.button_dismiss;
    paramView = (Button)a(i);
    if (paramView != null)
    {
      paramBundle = this;
      paramBundle = (View.OnClickListener)this;
      paramView.setOnClickListener(paramBundle);
    }
    b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */