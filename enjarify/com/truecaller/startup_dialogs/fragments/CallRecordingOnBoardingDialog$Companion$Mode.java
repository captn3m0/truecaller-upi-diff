package com.truecaller.startup_dialogs.fragments;

public enum CallRecordingOnBoardingDialog$Companion$Mode
{
  static
  {
    Mode[] arrayOfMode = new Mode[6];
    Mode localMode = new com/truecaller/startup_dialogs/fragments/CallRecordingOnBoardingDialog$Companion$Mode;
    localMode.<init>("ON_BOARDING", 0);
    ON_BOARDING = localMode;
    arrayOfMode[0] = localMode;
    localMode = new com/truecaller/startup_dialogs/fragments/CallRecordingOnBoardingDialog$Companion$Mode;
    int i = 1;
    localMode.<init>("ON_BOARDING_PREMIUM_USER", i);
    ON_BOARDING_PREMIUM_USER = localMode;
    arrayOfMode[i] = localMode;
    localMode = new com/truecaller/startup_dialogs/fragments/CallRecordingOnBoardingDialog$Companion$Mode;
    i = 2;
    localMode.<init>("AFTER_ENABLE", i);
    AFTER_ENABLE = localMode;
    arrayOfMode[i] = localMode;
    localMode = new com/truecaller/startup_dialogs/fragments/CallRecordingOnBoardingDialog$Companion$Mode;
    i = 3;
    localMode.<init>("ON_EXPIRY", i);
    ON_EXPIRY = localMode;
    arrayOfMode[i] = localMode;
    localMode = new com/truecaller/startup_dialogs/fragments/CallRecordingOnBoardingDialog$Companion$Mode;
    i = 4;
    localMode.<init>("PAY_WALL", i);
    PAY_WALL = localMode;
    arrayOfMode[i] = localMode;
    localMode = new com/truecaller/startup_dialogs/fragments/CallRecordingOnBoardingDialog$Companion$Mode;
    i = 5;
    localMode.<init>("PAY_WALL_ON_EXPIRY", i);
    PAY_WALL_ON_EXPIRY = localMode;
    arrayOfMode[i] = localMode;
    $VALUES = arrayOfMode;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.CallRecordingOnBoardingDialog.Companion.Mode
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */