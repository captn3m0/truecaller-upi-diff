package com.truecaller.startup_dialogs.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.R.id;
import java.util.HashMap;

public abstract class i
  extends m
{
  private HashMap a;
  
  public View a(int paramInt)
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      a = ((HashMap)localObject1);
    }
    localObject1 = a;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = a;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public abstract void a(ImageView paramImageView);
  
  public abstract void a(String paramString);
  
  public final void b()
  {
    int i = R.id.title_text;
    Object localObject = (TextView)a(i);
    k.a(localObject, "title_text");
    CharSequence localCharSequence = (CharSequence)h();
    ((TextView)localObject).setText(localCharSequence);
    i = R.id.subtitle;
    localObject = (TextView)a(i);
    k.a(localObject, "subtitle");
    localCharSequence = (CharSequence)i();
    ((TextView)localObject).setText(localCharSequence);
    i = R.id.button_accept;
    localObject = (Button)a(i);
    k.a(localObject, "button_accept");
    localCharSequence = (CharSequence)j();
    ((Button)localObject).setText(localCharSequence);
    i = R.id.button_dismiss;
    localObject = (Button)a(i);
    k.a(localObject, "button_dismiss");
    localCharSequence = (CharSequence)k();
    ((Button)localObject).setText(localCharSequence);
    i = R.id.logo;
    localObject = (ImageView)a(i);
    k.a(localObject, "logo");
    a((ImageView)localObject);
  }
  
  protected final void d()
  {
    super.d();
    Object localObject = getArguments();
    if (localObject != null)
    {
      String str = "promoType";
      localObject = ((Bundle)localObject).getString(str);
    }
    else
    {
      localObject = null;
    }
    a((String)localObject);
  }
  
  public void f()
  {
    HashMap localHashMap = a;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public abstract String h();
  
  public abstract String i();
  
  public abstract String j();
  
  public abstract String k();
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */