package com.truecaller.startup_dialogs.fragments;

import android.animation.Animator;
import android.support.v4.view.o;
import android.view.View;
import android.view.ViewGroup;
import com.truecaller.ui.view.h;
import java.util.ArrayList;

final class g$1
  extends o
{
  g$1(g paramg) {}
  
  public final void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    paramObject = (View)paramObject;
    paramViewGroup.removeView((View)paramObject);
    if (paramInt == 0)
    {
      paramViewGroup = g.b(a);
      if (paramViewGroup != null)
      {
        ba).a.cancel();
        paramViewGroup = a;
        g.c(paramViewGroup);
      }
    }
  }
  
  public final int getCount()
  {
    return g.a(a).size();
  }
  
  public final Object instantiateItem(ViewGroup paramViewGroup, int paramInt)
  {
    View localView = g.a(a, paramViewGroup, paramInt);
    paramViewGroup.addView(localView);
    return localView;
  }
  
  public final boolean isViewFromObject(View paramView, Object paramObject)
  {
    return paramView == paramObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.g.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */