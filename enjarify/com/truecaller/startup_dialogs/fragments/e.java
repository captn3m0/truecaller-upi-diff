package com.truecaller.startup_dialogs.fragments;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.R.id;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.flashsdk.core.b;
import com.truecaller.util.dg;
import com.truecaller.utils.n;
import java.util.HashMap;

public final class e
  extends m
{
  private final n a;
  private final b b;
  private final String c;
  private HashMap d;
  
  public e()
  {
    Object localObject = TrueApp.y();
    k.a(localObject, "TrueApp.getApp()");
    localObject = ((TrueApp)localObject).a().r();
    k.a(localObject, "TrueApp.getApp().objectsGraph.resourceProvider()");
    a = ((n)localObject);
    localObject = TrueApp.y();
    k.a(localObject, "TrueApp.getApp()");
    localObject = ((TrueApp)localObject).a().aV();
    k.a(localObject, "TrueApp.getApp().objectsGraph.flashManager()");
    b = ((b)localObject);
    c = "ImageFlashPromo";
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void b()
  {
    int i = R.id.logo;
    Object localObject1 = (ImageView)a(i);
    Object localObject2 = "logo";
    k.a(localObject1, (String)localObject2);
    localObject1 = ((ImageView)localObject1).getLayoutParams();
    if (localObject1 != null)
    {
      topMargin = 0;
      i = R.id.logo;
      localObject1 = (ImageView)a(i);
      k.a(localObject1, "logo");
      dg.a((ImageView)localObject1, 2131234703);
      i = R.id.title_text;
      localObject1 = (TextView)a(i);
      k.a(localObject1, "title_text");
      Object localObject3 = a;
      Object[] arrayOfObject = new Object[0];
      localObject3 = (CharSequence)((n)localObject3).a(2131886581, arrayOfObject);
      ((TextView)localObject1).setText((CharSequence)localObject3);
      i = R.id.subtitle;
      localObject1 = (TextView)a(i);
      k.a(localObject1, "subtitle");
      localObject3 = a;
      arrayOfObject = new Object[0];
      localObject3 = (CharSequence)((n)localObject3).a(2131886580, arrayOfObject);
      ((TextView)localObject1).setText((CharSequence)localObject3);
      i = R.id.button_accept;
      localObject1 = (Button)a(i);
      k.a(localObject1, "button_accept");
      localObject3 = a;
      arrayOfObject = new Object[0];
      localObject3 = (CharSequence)((n)localObject3).a(2131886578, arrayOfObject);
      ((Button)localObject1).setText((CharSequence)localObject3);
      i = R.id.button_dismiss;
      localObject1 = (Button)a(i);
      k.a(localObject1, "button_dismiss");
      localObject3 = a;
      localObject2 = new Object[0];
      localObject2 = (CharSequence)((n)localObject3).a(2131886579, (Object[])localObject2);
      ((Button)localObject1).setText((CharSequence)localObject2);
      return;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type android.widget.LinearLayout.LayoutParams");
    throw ((Throwable)localObject1);
  }
  
  public final String c()
  {
    return c;
  }
  
  protected final void d()
  {
    super.d();
    Context localContext = getContext();
    if (localContext != null)
    {
      Object localObject = b;
      k.a(localContext, "it");
      localObject = ((b)localObject).a(localContext, null, null, null, null, false, null);
      localContext.startActivity((Intent)localObject);
      return;
    }
  }
  
  public final void f()
  {
    HashMap localHashMap = d;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */