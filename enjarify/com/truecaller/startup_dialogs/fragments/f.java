package com.truecaller.startup_dialogs.fragments;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.R.id;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.messaging.defaultsms.DefaultSmsActivity;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.utils.d;
import java.util.HashMap;

public final class f
  extends m
{
  public String a = "whatsNew";
  private final String b = "SpamProtection";
  private HashMap c;
  
  private static boolean h()
  {
    Object localObject = TrueApp.y();
    k.a(localObject, "TrueApp.getApp()");
    localObject = ((TrueApp)localObject).a().bx();
    k.a(localObject, "TrueApp.getApp().objectsGraph.deviceInfoHelper()");
    return ((d)localObject).d();
  }
  
  private final void i()
  {
    Object localObject = getActivity();
    boolean bool = localObject instanceof TruecallerInit;
    if (bool)
    {
      localObject = getActivity();
      if (localObject != null)
      {
        localObject = (TruecallerInit)localObject;
        String str = "messages";
        ((TruecallerInit)localObject).a(str);
      }
      else
      {
        localObject = new c/u;
        ((u)localObject).<init>("null cannot be cast to non-null type com.truecaller.ui.TruecallerInit");
        throw ((Throwable)localObject);
      }
    }
    dismissAllowingStateLoss();
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void b()
  {
    int i = R.id.title_text;
    Object localObject1 = (TextView)a(i);
    k.a(localObject1, "title_text");
    Object localObject2 = (CharSequence)getString(2131887332);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    i = R.id.subtitle;
    localObject1 = (TextView)a(i);
    k.a(localObject1, "subtitle");
    localObject2 = (CharSequence)getString(2131887331);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    i = R.id.logo;
    ((ImageView)a(i)).setImageResource(2131234566);
    i = R.id.button_accept;
    localObject1 = (Button)a(i);
    k.a(localObject1, "button_accept");
    localObject2 = (CharSequence)getString(2131887326);
    ((Button)localObject1).setText((CharSequence)localObject2);
    i = R.id.button_accept;
    localObject1 = (Button)a(i);
    localObject2 = this;
    localObject2 = (View.OnClickListener)this;
    ((Button)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    i = R.id.button_dismiss;
    localObject1 = (Button)a(i);
    k.a(localObject1, "button_dismiss");
    CharSequence localCharSequence = (CharSequence)getString(2131887325);
    ((Button)localObject1).setText(localCharSequence);
    i = R.id.button_dismiss;
    ((Button)a(i)).setOnClickListener((View.OnClickListener)localObject2);
  }
  
  public final String c()
  {
    return b;
  }
  
  protected final void d()
  {
    boolean bool = h();
    if (!bool)
    {
      Object localObject = getContext();
      if (localObject == null) {
        return;
      }
      k.a(localObject, "context ?: return");
      String str = a;
      localObject = DefaultSmsActivity.a((Context)localObject, str);
      startActivityForResult((Intent)localObject, 101);
      return;
    }
    i();
  }
  
  public final void f()
  {
    HashMap localHashMap = c;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    paramInt2 = 101;
    if (paramInt1 == paramInt2)
    {
      paramInt1 = h();
      if (paramInt1 != 0) {
        i();
      }
    }
  }
  
  public final void onResume()
  {
    super.onResume();
    boolean bool = h();
    int i;
    if (bool) {
      i = 2131887326;
    } else {
      i = 2131887327;
    }
    Object localObject = getView();
    if (localObject != null)
    {
      int j = 2131362303;
      localObject = (Button)((View)localObject).findViewById(j);
      if (localObject != null)
      {
        ((Button)localObject).setText(i);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */