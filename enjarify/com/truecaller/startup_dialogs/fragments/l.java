package com.truecaller.startup_dialogs.fragments;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.util.dg;
import java.util.HashMap;

public final class l
  extends m
{
  private final String a = "VoipWhatsNew";
  private HashMap b;
  
  public final int a()
  {
    return 2131558604;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void b()
  {
    int i = R.id.logo;
    Object localObject1 = (ImageView)a(i);
    k.a(localObject1, "logo");
    dg.a((ImageView)localObject1, 2131234682);
    i = R.id.title_text;
    localObject1 = (TextView)a(i);
    k.a(localObject1, "title_text");
    Object localObject2 = new Object[1];
    String str = getString(2131889034);
    localObject2[0] = str;
    localObject2 = (CharSequence)getString(2131889038, (Object[])localObject2);
    ((TextView)localObject1).setText((CharSequence)localObject2);
  }
  
  public final String c()
  {
    return a;
  }
  
  public final void f()
  {
    HashMap localHashMap = b;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */