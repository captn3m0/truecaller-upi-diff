package com.truecaller.startup_dialogs.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import c.u;
import com.truecaller.R.id;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.util.dg;
import com.truecaller.utils.n;
import java.util.HashMap;

public final class k
  extends i
{
  private final String a = "TCPayPromo";
  private final n b;
  private final String c;
  private final String d;
  private final String e;
  private final String h;
  private HashMap i;
  
  public k()
  {
    Object localObject = TrueApp.y();
    c.g.b.k.a(localObject, "TrueApp.getApp()");
    localObject = ((TrueApp)localObject).a().r();
    c.g.b.k.a(localObject, "TrueApp.getApp().objectsGraph.resourceProvider()");
    b = ((n)localObject);
    localObject = b;
    Object[] arrayOfObject1 = new Object[0];
    localObject = ((n)localObject).a(2131887262, arrayOfObject1);
    c.g.b.k.a(localObject, "res.getString(R.string.TCPay_PromoTitle)");
    c = ((String)localObject);
    localObject = b;
    arrayOfObject1 = new Object[0];
    localObject = ((n)localObject).a(2131887261, arrayOfObject1);
    c.g.b.k.a(localObject, "res.getString(R.string.TCPay_PromoSubtitle)");
    d = ((String)localObject);
    localObject = b;
    arrayOfObject1 = new Object[0];
    localObject = ((n)localObject).a(2131887258, arrayOfObject1);
    c.g.b.k.a(localObject, "res.getString(R.string.TCPay_PromoCtaPrimary)");
    e = ((String)localObject);
    localObject = b;
    Object[] arrayOfObject2 = new Object[0];
    localObject = ((n)localObject).a(2131887259, arrayOfObject2);
    c.g.b.k.a(localObject, "res.getString(R.string.TCPay_PromoCtaSecondary)");
    h = ((String)localObject);
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = i;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      i = ((HashMap)localObject1);
    }
    localObject1 = i;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = i;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(ImageView paramImageView)
  {
    c.g.b.k.b(paramImageView, "imageView");
    int j = R.id.logo;
    paramImageView = (ImageView)a(j);
    String str = "logo";
    c.g.b.k.a(paramImageView, str);
    paramImageView = paramImageView.getLayoutParams();
    if (paramImageView != null)
    {
      topMargin = 0;
      j = R.id.logo;
      paramImageView = (ImageView)a(j);
      c.g.b.k.a(paramImageView, "logo");
      dg.a(paramImageView, 2131234973);
      return;
    }
    paramImageView = new c/u;
    paramImageView.<init>("null cannot be cast to non-null type android.widget.LinearLayout.LayoutParams");
    throw paramImageView;
  }
  
  public final void a(String paramString)
  {
    Context localContext = (Context)getActivity();
    if (paramString != null)
    {
      int j = paramString.hashCode();
      int k = 1382682413;
      if (j == k)
      {
        String str = "payments";
        boolean bool = paramString.equals(str);
        if (bool)
        {
          paramString = "payments";
          break label59;
        }
      }
    }
    paramString = "banking";
    label59:
    TruecallerInit.a(localContext, paramString, false, "whatsNew");
  }
  
  public final String c()
  {
    return a;
  }
  
  public final void f()
  {
    HashMap localHashMap = i;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final String h()
  {
    return c;
  }
  
  public final String i()
  {
    return d;
  }
  
  public final String j()
  {
    return e;
  }
  
  public final String k()
  {
    return h;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */