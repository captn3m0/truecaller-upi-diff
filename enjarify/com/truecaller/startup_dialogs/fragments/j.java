package com.truecaller.startup_dialogs.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.View;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.startup_dialogs.a;
import java.util.HashMap;

public abstract class j
  extends AppCompatDialogFragment
{
  private HashMap a;
  
  public View a(int paramInt)
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      a = ((HashMap)localObject1);
    }
    localObject1 = a;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = a;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public void f()
  {
    HashMap localHashMap = a;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public void onDismiss(DialogInterface paramDialogInterface)
  {
    Object localObject1 = getArguments();
    if (localObject1 != null)
    {
      Object localObject2 = ((Bundle)localObject1).getString("StartupDialogType");
      Object localObject3 = null;
      if (localObject2 != null) {
        localObject2 = StartupDialogType.valueOf((String)localObject2);
      } else {
        localObject2 = null;
      }
      Object localObject4 = "StartupDialogDismissReason";
      localObject1 = ((Bundle)localObject1).getString((String)localObject4);
      if (localObject1 != null) {
        localObject1 = StartupDialogDismissReason.valueOf((String)localObject1);
      } else {
        localObject1 = null;
      }
      if (localObject2 != null)
      {
        localObject4 = getActivity();
        boolean bool = localObject4 instanceof a;
        if (bool) {
          localObject3 = localObject4;
        }
        localObject3 = (a)localObject3;
        if (localObject3 != null) {
          ((a)localObject3).a((StartupDialogType)localObject2, (StartupDialogDismissReason)localObject1);
        }
      }
    }
    super.onDismiss(paramDialogInterface);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */