package com.truecaller.startup_dialogs.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import c.l;
import c.u;
import com.truecaller.R.id;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.calling.recorder.CallRecordingOnBoardingActivity;
import com.truecaller.calling.recorder.CallRecordingOnBoardingLaunchContext;
import com.truecaller.calling.recorder.CallRecordingOnBoardingState;
import com.truecaller.calling.recorder.CallRecordingSettingsActivity;
import com.truecaller.calling.recorder.aj;
import com.truecaller.calling.recorder.b;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.br;
import com.truecaller.util.dg;
import com.truecaller.utils.n;
import java.io.Serializable;
import java.util.HashMap;

public final class CallRecordingOnBoardingDialog
  extends m
{
  public static final CallRecordingOnBoardingDialog.Companion e;
  public CallRecordingManager a;
  public n b;
  public br c;
  public aj d;
  private CallRecordingOnBoardingDialog.Companion.Mode h;
  private CallRecordingOnBoardingLaunchContext i;
  private HashMap j;
  
  static
  {
    CallRecordingOnBoardingDialog.Companion localCompanion = new com/truecaller/startup_dialogs/fragments/CallRecordingOnBoardingDialog$Companion;
    localCompanion.<init>((byte)0);
    e = localCompanion;
  }
  
  public final int a()
  {
    Object localObject1 = h;
    if (localObject1 == null)
    {
      localObject2 = "mode";
      k.a((String)localObject2);
    }
    Object localObject2 = c.a;
    int k = ((CallRecordingOnBoardingDialog.Companion.Mode)localObject1).ordinal();
    k = localObject2[k];
    switch (k)
    {
    default: 
      localObject1 = new c/l;
      ((l)localObject1).<init>();
      throw ((Throwable)localObject1);
    case 6: 
      return 2131558563;
    }
    return super.a();
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = j;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      j = ((HashMap)localObject1);
    }
    localObject1 = j;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = j;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void b()
  {
    int k = R.id.title_text;
    Object localObject1 = (TextView)a(k);
    k.a(localObject1, "title_text");
    Object localObject2 = h;
    if (localObject2 == null)
    {
      localObject3 = "mode";
      k.a((String)localObject3);
    }
    Object localObject3 = c.b;
    int m = ((CallRecordingOnBoardingDialog.Companion.Mode)localObject2).ordinal();
    m = localObject3[m];
    switch (m)
    {
    default: 
      localObject1 = new c/l;
      ((l)localObject1).<init>();
      throw ((Throwable)localObject1);
    case 5: 
    case 6: 
      m = 2131887592;
      break;
    case 4: 
      m = 2131887588;
      break;
    case 2: 
    case 3: 
      m = 2131887627;
      break;
    case 1: 
      m = 2131887629;
    }
    localObject2 = getString(m);
    localObject3 = "getString(\n        when …log_title\n        }\n    )";
    k.a(localObject2, (String)localObject3);
    localObject2 = (CharSequence)localObject2;
    ((TextView)localObject1).setText((CharSequence)localObject2);
    k = R.id.subtitle;
    localObject1 = (TextView)a(k);
    k.a(localObject1, "subtitle");
    localObject2 = h;
    if (localObject2 == null)
    {
      localObject3 = "mode";
      k.a((String)localObject3);
    }
    localObject3 = c.c;
    m = ((CallRecordingOnBoardingDialog.Companion.Mode)localObject2).ordinal();
    m = localObject3[m];
    switch (m)
    {
    default: 
      localObject1 = new c/l;
      ((l)localObject1).<init>();
      throw ((Throwable)localObject1);
    case 5: 
    case 6: 
      m = 2131887591;
      break;
    case 4: 
      m = 2131887587;
      break;
    case 2: 
    case 3: 
      m = 2131887626;
      break;
    case 1: 
      m = 2131887628;
    }
    localObject3 = b;
    if (localObject3 == null)
    {
      localObject4 = "resourceProvider";
      k.a((String)localObject4);
    }
    Object localObject4 = new Object[0];
    localObject2 = ((n)localObject3).b(m, (Object[])localObject4);
    k.a(localObject2, "resourceProvider.getRichString(it)");
    localObject3 = "when (mode) {\n        ON…vider.getRichString(it) }";
    k.a(localObject2, (String)localObject3);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    k = R.id.button_accept;
    localObject1 = (Button)a(k);
    k.a(localObject1, "button_accept");
    localObject2 = h;
    if (localObject2 == null)
    {
      localObject3 = "mode";
      k.a((String)localObject3);
    }
    localObject3 = c.d;
    m = ((CallRecordingOnBoardingDialog.Companion.Mode)localObject2).ordinal();
    m = localObject3[m];
    switch (m)
    {
    default: 
      localObject1 = new c/l;
      ((l)localObject1).<init>();
      throw ((Throwable)localObject1);
    case 5: 
    case 6: 
      m = 2131887589;
      break;
    case 4: 
      m = 2131887585;
      break;
    case 2: 
    case 3: 
      m = 2131887622;
      break;
    case 1: 
      m = 2131887617;
    }
    localObject2 = getString(m);
    localObject3 = "getString(\n        when …a_primary\n        }\n    )";
    k.a(localObject2, (String)localObject3);
    localObject2 = (CharSequence)localObject2;
    ((Button)localObject1).setText((CharSequence)localObject2);
    k = R.id.button_dismiss;
    localObject1 = (Button)a(k);
    k.a(localObject1, "button_dismiss");
    localObject2 = h;
    if (localObject2 == null)
    {
      localObject3 = "mode";
      k.a((String)localObject3);
    }
    localObject3 = c.e;
    m = ((CallRecordingOnBoardingDialog.Companion.Mode)localObject2).ordinal();
    m = localObject3[m];
    switch (m)
    {
    default: 
      localObject1 = new c/l;
      ((l)localObject1).<init>();
      throw ((Throwable)localObject1);
    case 5: 
    case 6: 
      m = 2131887590;
      break;
    case 4: 
      m = 2131887586;
      break;
    case 2: 
    case 3: 
      m = 2131887623;
      break;
    case 1: 
      m = 2131887618;
    }
    localObject2 = getString(m);
    k.a(localObject2, "getString(\n        when …secondary\n        }\n    )");
    localObject2 = (CharSequence)localObject2;
    ((Button)localObject1).setText((CharSequence)localObject2);
    k = R.id.logo;
    localObject1 = (ImageView)a(k);
    k.a(localObject1, "logo");
    dg.a((ImageView)localObject1, 2131230993);
  }
  
  public final String c()
  {
    Object localObject1 = getArguments();
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = "KEY_MODE";
      localObject1 = ((Bundle)localObject1).getSerializable((String)localObject2);
      if (localObject1 != null) {}
    }
    else
    {
      localObject1 = (Serializable)CallRecordingOnBoardingDialog.Companion.Mode.ON_BOARDING;
    }
    if (localObject1 != null)
    {
      localObject1 = (CallRecordingOnBoardingDialog.Companion.Mode)localObject1;
      h = ((CallRecordingOnBoardingDialog.Companion.Mode)localObject1);
      localObject1 = h;
      if (localObject1 == null)
      {
        localObject2 = "mode";
        k.a((String)localObject2);
      }
      localObject2 = CallRecordingOnBoardingDialog.Companion.Mode.ON_EXPIRY;
      if (localObject1 == localObject2) {
        return "CallRecProNudge";
      }
      return "CallRecOnboarding";
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type com.truecaller.startup_dialogs.fragments.CallRecordingOnBoardingDialog.Companion.Mode");
    throw ((Throwable)localObject1);
  }
  
  protected final void d()
  {
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      Object localObject2 = h;
      if (localObject2 == null)
      {
        localObject3 = "mode";
        k.a((String)localObject3);
      }
      Object localObject3 = c.f;
      int k = ((CallRecordingOnBoardingDialog.Companion.Mode)localObject2).ordinal();
      k = localObject3[k];
      localObject3 = null;
      Object localObject4;
      Object localObject5;
      switch (k)
      {
      default: 
        break;
      case 6: 
        localObject2 = c;
        if (localObject2 == null)
        {
          localObject2 = "premiumScreenNavigator";
          k.a((String)localObject2);
        }
        k.a(localObject1, "it");
        localObject2 = localObject1;
        localObject2 = (Context)localObject1;
        localObject4 = PremiumPresenterView.LaunchContext.CALL_RECORDING_PAY_WALL_ON_EXPIRY;
        br.a((Context)localObject2, (PremiumPresenterView.LaunchContext)localObject4);
        break;
      case 5: 
        localObject2 = c;
        if (localObject2 == null)
        {
          localObject2 = "premiumScreenNavigator";
          k.a((String)localObject2);
        }
        k.a(localObject1, "it");
        localObject2 = localObject1;
        localObject2 = (Context)localObject1;
        localObject4 = PremiumPresenterView.LaunchContext.CALL_RECORDING_NUDGE;
        br.a((Context)localObject2, (PremiumPresenterView.LaunchContext)localObject4);
        break;
      case 4: 
        localObject2 = new android/content/Intent;
        localObject4 = localObject1;
        localObject4 = (Context)localObject1;
        ((Intent)localObject2).<init>((Context)localObject4, CallRecordingSettingsActivity.class);
        localObject4 = "LaunchContext";
        localObject5 = i;
        if (localObject5 == null)
        {
          String str = "launchContext";
          k.a(str);
        }
        localObject5 = (Serializable)localObject5;
        ((Intent)localObject2).putExtra((String)localObject4, (Serializable)localObject5);
        ((f)localObject1).startActivity((Intent)localObject2);
        break;
      case 3: 
        localObject2 = c;
        if (localObject2 == null)
        {
          localObject2 = "premiumScreenNavigator";
          k.a((String)localObject2);
        }
        localObject2 = requireContext();
        k.a(localObject2, "requireContext()");
        localObject4 = PremiumPresenterView.LaunchContext.CALL_RECORDING_PAY_WALL;
        localObject5 = "premiumCallRecording";
        br.a((Context)localObject2, (PremiumPresenterView.LaunchContext)localObject4, (String)localObject5);
        break;
      case 1: 
      case 2: 
        localObject2 = d;
        if (localObject2 == null)
        {
          localObject2 = "callRecordingOnBoardingNavigator";
          k.a((String)localObject2);
        }
        k.a(localObject1, "it");
        localObject2 = localObject1;
        localObject2 = (Context)localObject1;
        localObject4 = i;
        if (localObject4 == null)
        {
          localObject5 = "launchContext";
          k.a((String)localObject5);
        }
        k.b(localObject2, "context");
        localObject5 = "launchContext";
        k.b(localObject4, (String)localObject5);
        localObject4 = aj.a((Context)localObject2, (CallRecordingOnBoardingLaunchContext)localObject4, null);
        ((Context)localObject2).startActivity((Intent)localObject4);
      }
      boolean bool = localObject1 instanceof CallRecordingOnBoardingActivity;
      if (!bool) {
        localObject1 = null;
      }
      localObject1 = (CallRecordingOnBoardingActivity)localObject1;
      if (localObject1 != null)
      {
        ((CallRecordingOnBoardingActivity)localObject1).finish();
        return;
      }
      return;
    }
  }
  
  protected final void e()
  {
    super.e();
    Object localObject1 = getActivity();
    if (localObject1 != null)
    {
      Object localObject2 = h;
      if (localObject2 == null)
      {
        localObject3 = "mode";
        k.a((String)localObject3);
      }
      Object localObject3 = c.g;
      int k = ((CallRecordingOnBoardingDialog.Companion.Mode)localObject2).ordinal();
      k = localObject3[k];
      switch (k)
      {
      default: 
        break;
      case 1: 
      case 2: 
        localObject2 = d;
        if (localObject2 == null)
        {
          localObject2 = "callRecordingOnBoardingNavigator";
          k.a((String)localObject2);
        }
        k.a(localObject1, "it");
        localObject2 = localObject1;
        localObject2 = (Context)localObject1;
        localObject3 = CallRecordingOnBoardingState.WHATS_NEW_NEGATIVE;
        CallRecordingOnBoardingLaunchContext localCallRecordingOnBoardingLaunchContext = i;
        if (localCallRecordingOnBoardingLaunchContext == null)
        {
          String str = "launchContext";
          k.a(str);
        }
        aj.a((Context)localObject2, (CallRecordingOnBoardingState)localObject3, localCallRecordingOnBoardingLaunchContext);
      }
      boolean bool = localObject1 instanceof CallRecordingOnBoardingActivity;
      if (!bool) {
        localObject1 = null;
      }
      localObject1 = (CallRecordingOnBoardingActivity)localObject1;
      if (localObject1 != null)
      {
        ((CallRecordingOnBoardingActivity)localObject1).finish();
        return;
      }
      return;
    }
  }
  
  public final void f()
  {
    HashMap localHashMap = j;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getContext();
    if (paramBundle != null) {
      paramBundle = paramBundle.getApplicationContext();
    } else {
      paramBundle = null;
    }
    if (paramBundle != null)
    {
      ((bk)paramBundle).a().cy().a(this);
      paramBundle = getArguments();
      if (paramBundle != null)
      {
        String str = "LaunchContext";
        paramBundle = paramBundle.getSerializable(str);
        if (paramBundle != null) {}
      }
      else
      {
        paramBundle = (Serializable)CallRecordingOnBoardingLaunchContext.UNKNOWN;
      }
      if (paramBundle != null)
      {
        paramBundle = (CallRecordingOnBoardingLaunchContext)paramBundle;
        i = paramBundle;
        return;
      }
      paramBundle = new c/u;
      paramBundle.<init>("null cannot be cast to non-null type com.truecaller.calling.recorder.CallRecordingOnBoardingLaunchContext");
      throw paramBundle;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramBundle;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.CallRecordingOnBoardingDialog
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */