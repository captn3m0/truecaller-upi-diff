package com.truecaller.startup_dialogs.fragments;

import android.os.Bundle;
import c.g.b.k;

public final class BottomPopupDialogFragment$a
{
  public static BottomPopupDialogFragment a(BottomPopupDialogFragment.Action paramAction)
  {
    k.b(paramAction, "action");
    BottomPopupDialogFragment localBottomPopupDialogFragment = new com/truecaller/startup_dialogs/fragments/BottomPopupDialogFragment;
    localBottomPopupDialogFragment.<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramAction = paramAction.name();
    localBundle.putString("action", paramAction);
    localBottomPopupDialogFragment.setArguments(localBundle);
    return localBottomPopupDialogFragment;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.BottomPopupDialogFragment.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */