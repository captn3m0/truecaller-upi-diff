package com.truecaller.startup_dialogs.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bl;
import com.truecaller.bp;
import com.truecaller.common.h.o;
import com.truecaller.notifications.g;
import com.truecaller.old.data.access.Settings;
import com.truecaller.old.data.access.f;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.WizardActivity;
import com.truecaller.utils.l;
import com.truecaller.wizard.utils.PermissionPoller;
import com.truecaller.wizard.utils.PermissionPoller.Permission;
import com.truecaller.wizard.utils.i;
import java.util.HashMap;

public final class BottomPopupDialogFragment
  extends a
  implements View.OnClickListener
{
  public static final BottomPopupDialogFragment.a e;
  public com.truecaller.update.d a;
  public com.truecaller.utils.d b;
  public l c;
  public g d;
  private BottomPopupDialogFragment.Action f;
  private HashMap g;
  
  static
  {
    BottomPopupDialogFragment.a locala = new com/truecaller/startup_dialogs/fragments/BottomPopupDialogFragment$a;
    locala.<init>((byte)0);
    e = locala;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = g;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      g = ((HashMap)localObject1);
    }
    localObject1 = g;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = g;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final String c()
  {
    BottomPopupDialogFragment.Action localAction = f;
    if (localAction == null)
    {
      String str = "action";
      k.a(str);
    }
    return localAction.getAnalyticsType();
  }
  
  protected final void d()
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject2 = "action";
      k.a((String)localObject2);
    }
    Object localObject2 = getActivity();
    boolean bool1 = localObject2 instanceof TruecallerInit;
    if (!bool1) {
      localObject2 = null;
    }
    localObject2 = (TruecallerInit)localObject2;
    if (localObject2 != null)
    {
      Object localObject3 = b.a;
      int j = ((BottomPopupDialogFragment.Action)localObject1).ordinal();
      int i = localObject3[j];
      j = 1;
      boolean bool2;
      switch (i)
      {
      default: 
        break;
      case 7: 
        localObject1 = b;
        if (localObject1 == null)
        {
          localObject3 = "deviceInfoUtil";
          k.a((String)localObject3);
        }
        int m = ((com.truecaller.utils.d)localObject1).h();
        i = 24;
        if (m >= i)
        {
          localObject2 = (Context)localObject2;
          localObject1 = new android/content/Intent;
          localObject3 = "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS";
          ((Intent)localObject1).<init>((String)localObject3);
          o.a((Context)localObject2, (Intent)localObject1);
          m = 2131888935;
          localObject1 = Toast.makeText((Context)localObject2, m, j);
          ((Toast)localObject1).show();
        }
        break;
      case 6: 
        localObject1 = new com/truecaller/old/data/access/f;
        localObject2 = (Context)localObject2;
        ((f)localObject1).<init>((Context)localObject2);
        localObject1 = ((f)localObject1).i();
        if (localObject1 != null)
        {
          localObject3 = a;
          if (localObject3 == null)
          {
            str = "updateStarter";
            k.a(str);
          }
          k.a(localObject1, "it");
          localObject1 = ((Notification)localObject1).p();
          String str = "startupDialog";
          ((com.truecaller.update.d)localObject3).a((Context)localObject2, (String)localObject1, str);
        }
        break;
      case 5: 
        localObject1 = localObject2;
        i.a((Activity)localObject2);
        localObject1 = ((TruecallerInit)localObject2).e();
        localObject2 = PermissionPoller.Permission.DRAW_OVERLAY;
        ((PermissionPoller)localObject1).a((PermissionPoller.Permission)localObject2);
        break;
      case 4: 
        localObject1 = localObject2;
        localObject1 = (Activity)localObject2;
        localObject3 = "android.permission.ACCESS_COARSE_LOCATION";
        bool2 = i.a((Activity)localObject1, (String)localObject3);
        if (!bool2)
        {
          localObject1 = this;
          localObject1 = (Fragment)this;
          localObject2 = "android.permission.ACCESS_COARSE_LOCATION";
          i.a((Fragment)localObject1, (String)localObject2, j);
        }
        else
        {
          localObject1 = new com/truecaller/bl;
          localObject2 = (Context)localObject2;
          i = 2131886799;
          j = 2131886798;
          int n = 2131234258;
          ((bl)localObject1).<init>((Context)localObject2, i, j, n);
          ((bl)localObject1).show();
        }
        break;
      case 2: 
      case 3: 
        localObject3 = BottomPopupDialogFragment.Action.IDENTIFY_UNKNOWN_SENDERS;
        if (localObject1 == localObject3)
        {
          localObject1 = "enhancedNotificationsEnabled";
          Settings.a((String)localObject1, j);
        }
        localObject1 = d;
        if (localObject1 == null)
        {
          localObject3 = "notificationAccessRequester";
          k.a((String)localObject3);
        }
        localObject3 = localObject2;
        localObject3 = (Context)localObject2;
        int k = 2131886528;
        bool2 = ((g)localObject1).a((Context)localObject3, k);
        if (bool2)
        {
          localObject1 = ((TruecallerInit)localObject2).e();
          localObject2 = PermissionPoller.Permission.NOTIFICATION_ACCESS;
          ((PermissionPoller)localObject1).a((PermissionPoller.Permission)localObject2);
        }
        break;
      case 1: 
        localObject2 = (Context)localObject2;
        localObject1 = WizardActivity.class;
        localObject3 = "tooltip";
        com.truecaller.wizard.b.c.a((Context)localObject2, (Class)localObject1, (String)localObject3);
      }
    }
    super.e();
  }
  
  protected final void e()
  {
    Bundle localBundle = getArguments();
    if (localBundle == null)
    {
      localBundle = new android/os/Bundle;
      localBundle.<init>();
    }
    String str = StartupDialogDismissReason.USER_PRESSED_DISMISS_BUTTON.name();
    localBundle.putString("StartupDialogDismissReason", str);
    setArguments(localBundle);
    super.e();
  }
  
  public final void f()
  {
    HashMap localHashMap = g;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    Object localObject = getArguments();
    if (localObject != null)
    {
      String str = "action";
      localObject = ((Bundle)localObject).getString(str);
      if (localObject != null)
      {
        localObject = BottomPopupDialogFragment.Action.valueOf((String)localObject);
        f = ((BottomPopupDialogFragment.Action)localObject);
        super.onCreate(paramBundle);
        paramBundle = TrueApp.y();
        k.a(paramBundle, "TrueApp.getApp()");
        paramBundle.a().cz().a(this);
        return;
      }
    }
    paramBundle = new java/lang/IllegalStateException;
    paramBundle.<init>("Action is missed");
    throw ((Throwable)paramBundle);
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = super.onCreateDialog(paramBundle);
    k.a(paramBundle, "super.onCreateDialog(savedInstanceState)");
    Window localWindow = paramBundle.getWindow();
    if (localWindow != null)
    {
      int i = 1;
      localWindow.requestFeature(i);
    }
    return paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i = 2131558580;
    int j = 0;
    paramLayoutInflater = paramLayoutInflater.inflate(i, paramViewGroup, false);
    int k = 2131362303;
    paramViewGroup = (TextView)paramLayoutInflater.findViewById(k);
    paramBundle = this;
    paramBundle = (View.OnClickListener)this;
    paramViewGroup.setOnClickListener(paramBundle);
    Object localObject = f;
    String str;
    if (localObject == null)
    {
      str = "action";
      k.a(str);
    }
    j = ((BottomPopupDialogFragment.Action)localObject).getAccept();
    if (j >= 0)
    {
      localObject = f;
      if (localObject == null)
      {
        str = "action";
        k.a(str);
      }
      j = ((BottomPopupDialogFragment.Action)localObject).getAccept();
      paramViewGroup.setText(j);
    }
    paramLayoutInflater.findViewById(2131362326).setOnClickListener(paramBundle);
    k = 2131363718;
    paramViewGroup = (TextView)paramLayoutInflater.findViewById(k);
    paramBundle = f;
    if (paramBundle == null)
    {
      localObject = "action";
      k.a((String)localObject);
    }
    i = paramBundle.getText();
    paramViewGroup.setText(i);
    k = 2131363301;
    paramViewGroup = (ImageView)paramLayoutInflater.findViewById(k);
    paramBundle = f;
    if (paramBundle == null)
    {
      localObject = "action";
      k.a((String)localObject);
    }
    i = paramBundle.getIcon();
    paramViewGroup.setImageResource(i);
    return paramLayoutInflater;
  }
  
  public final void onStart()
  {
    super.onStart();
    Object localObject1 = getDialog();
    if (localObject1 != null)
    {
      localObject1 = ((Dialog)localObject1).getWindow();
      if (localObject1 != null)
      {
        Object localObject2 = new android/graphics/drawable/ColorDrawable;
        ((ColorDrawable)localObject2).<init>(0);
        localObject2 = (Drawable)localObject2;
        ((Window)localObject1).setBackgroundDrawable((Drawable)localObject2);
        ((Window)localObject1).setDimAmount(0.0F);
        localObject2 = ((Window)localObject1).getAttributes();
        width = -1;
        height = -2;
        gravity = 80;
        windowAnimations = 2131951952;
        ((Window)localObject1).setAttributes((WindowManager.LayoutParams)localObject2);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.BottomPopupDialogFragment
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */