package com.truecaller.startup_dialogs.fragments;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.util.d.a;

final class h$a
  implements DialogInterface.OnClickListener
{
  public static final a a;
  
  static
  {
    a locala = new com/truecaller/startup_dialogs/fragments/h$a;
    locala.<init>();
    a = locala;
  }
  
  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    paramDialogInterface = TrueApp.y();
    k.a(paramDialogInterface, "TrueApp.getApp()");
    paramDialogInterface.a().aA().a(0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.fragments.h.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */