package com.truecaller.startup_dialogs;

import android.content.Context;
import com.truecaller.old.data.access.f;
import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d
{
  private final Provider a;
  
  private g(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static f a(Context paramContext)
  {
    f localf = new com/truecaller/old/data/access/f;
    localf.<init>(paramContext);
    return (f)dagger.a.g.a(localf, "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static g a(Provider paramProvider)
  {
    g localg = new com/truecaller/startup_dialogs/g;
    localg.<init>(paramProvider);
    return localg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */