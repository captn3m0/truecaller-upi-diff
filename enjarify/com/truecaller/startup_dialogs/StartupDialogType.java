package com.truecaller.startup_dialogs;

public enum StartupDialogType
{
  private final int reqCode;
  
  static
  {
    StartupDialogType[] arrayOfStartupDialogType = new StartupDialogType[21];
    StartupDialogType localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    localStartupDialogType.<init>("DIALOG_WHATS_NEW", 0, 1001);
    DIALOG_WHATS_NEW = localStartupDialogType;
    arrayOfStartupDialogType[0] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    int i = 1;
    localStartupDialogType.<init>("DIALOG_MDAU_PROMO", i, 1002);
    DIALOG_MDAU_PROMO = localStartupDialogType;
    arrayOfStartupDialogType[i] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    i = 2;
    localStartupDialogType.<init>("POPUP_MISSED_CALLS_NOTIFICATIONS", i, 1003);
    POPUP_MISSED_CALLS_NOTIFICATIONS = localStartupDialogType;
    arrayOfStartupDialogType[i] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    i = 3;
    localStartupDialogType.<init>("POPUP_IDENTIFY_OTHERS", i, 1004);
    POPUP_IDENTIFY_OTHERS = localStartupDialogType;
    arrayOfStartupDialogType[i] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    i = 4;
    localStartupDialogType.<init>("POPUP_ADD_ACCOUNT", i, 1005);
    POPUP_ADD_ACCOUNT = localStartupDialogType;
    arrayOfStartupDialogType[i] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    i = 5;
    localStartupDialogType.<init>("POPUP_LOCATION_PERMISSION", i, 1006);
    POPUP_LOCATION_PERMISSION = localStartupDialogType;
    arrayOfStartupDialogType[i] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    i = 6;
    localStartupDialogType.<init>("POPUP_DRAW_PERMISSION", i, 1007);
    POPUP_DRAW_PERMISSION = localStartupDialogType;
    arrayOfStartupDialogType[i] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    i = 7;
    localStartupDialogType.<init>("POPUP_SOFTWARE_UPDATE", i, 1008);
    POPUP_SOFTWARE_UPDATE = localStartupDialogType;
    arrayOfStartupDialogType[i] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    i = 8;
    localStartupDialogType.<init>("POPUP_DO_NOT_DISTURB_ACCESS", i, 1009);
    POPUP_DO_NOT_DISTURB_ACCESS = localStartupDialogType;
    arrayOfStartupDialogType[i] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    i = 9;
    localStartupDialogType.<init>("DIALOG_BLANK_PROFILE_NAME", i, 1010);
    DIALOG_BLANK_PROFILE_NAME = localStartupDialogType;
    arrayOfStartupDialogType[i] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    i = 10;
    localStartupDialogType.<init>("POPUP_PREMIUM_PROMO", i, 1011);
    POPUP_PREMIUM_PROMO = localStartupDialogType;
    arrayOfStartupDialogType[i] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    i = 11;
    localStartupDialogType.<init>("POPUP_TCPAY_PROMO", i, 1012);
    POPUP_TCPAY_PROMO = localStartupDialogType;
    arrayOfStartupDialogType[i] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    i = 12;
    localStartupDialogType.<init>("TRUECALLER_ONBOARDING", i, 1013);
    TRUECALLER_ONBOARDING = localStartupDialogType;
    arrayOfStartupDialogType[i] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    i = 13;
    localStartupDialogType.<init>("BACKUP_ONBOARDING", i, 1014);
    BACKUP_ONBOARDING = localStartupDialogType;
    arrayOfStartupDialogType[i] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    i = 14;
    localStartupDialogType.<init>("TCPAY_ONBOARDING", i, 1015);
    TCPAY_ONBOARDING = localStartupDialogType;
    arrayOfStartupDialogType[i] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    localStartupDialogType.<init>("PIN_SHORTCUTS_REQUEST_DIALOG", 15, 1016);
    PIN_SHORTCUTS_REQUEST_DIALOG = localStartupDialogType;
    arrayOfStartupDialogType[15] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    localStartupDialogType.<init>("CALL_RECORDING_PREMIUM_NUDGE", 16, 1017);
    CALL_RECORDING_PREMIUM_NUDGE = localStartupDialogType;
    arrayOfStartupDialogType[16] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    localStartupDialogType.<init>("WHATS_APP_IN_CALL_LOG", 17, 1018);
    WHATS_APP_IN_CALL_LOG = localStartupDialogType;
    arrayOfStartupDialogType[17] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    localStartupDialogType.<init>("POPUP_IMAGE_FLASH", 18, 1019);
    POPUP_IMAGE_FLASH = localStartupDialogType;
    arrayOfStartupDialogType[18] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    localStartupDialogType.<init>("ONBOARDING_PREMIUM_POPUP", 19, 1021);
    ONBOARDING_PREMIUM_POPUP = localStartupDialogType;
    arrayOfStartupDialogType[19] = localStartupDialogType;
    localStartupDialogType = new com/truecaller/startup_dialogs/StartupDialogType;
    localStartupDialogType.<init>("ENGAGEMENT_REWARD_REDEEM", 20, 1022);
    ENGAGEMENT_REWARD_REDEEM = localStartupDialogType;
    arrayOfStartupDialogType[20] = localStartupDialogType;
    $VALUES = arrayOfStartupDialogType;
  }
  
  private StartupDialogType(int paramInt1)
  {
    reqCode = paramInt1;
  }
  
  public final int requestCode()
  {
    return reqCode;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.startup_dialogs.StartupDialogType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */