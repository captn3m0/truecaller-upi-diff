package com.truecaller;

public final class BuildConfig
{
  public static final String APPLICATION_ID = "com.truecaller";
  public static final String BUILD_NAME = "GOOGLE_PLAY";
  public static final String BUILD_TYPE = "release";
  public static final boolean DEBUG = false;
  public static final String FLAVOR = "googlePlay";
  public static final String GIT_REVISION = "1114.0dbb949";
  public static final int VERSION_CODE = 1041006;
  public static final String VERSION_NAME = "10.41.6";
}

/* Location:
 * Qualified Name:     com.truecaller.BuildConfig
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */