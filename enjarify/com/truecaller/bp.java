package com.truecaller;

import android.content.ContentResolver;
import android.content.Context;
import com.truecaller.ads.provider.fetch.AdsConfigurationManager;
import com.truecaller.analytics.AppHeartBeatTask;
import com.truecaller.analytics.AppSettingsTask;
import com.truecaller.analytics.InstalledAppsHeartbeatWorker;
import com.truecaller.analytics.sync.EventsUploadWorker;
import com.truecaller.backup.BackupLogWorker;
import com.truecaller.calling.ar;
import com.truecaller.calling.c.a.a;
import com.truecaller.calling.dialer.ax;
import com.truecaller.calling.dialer.bi;
import com.truecaller.calling.dialer.suggested_contacts.SuggestionsChooserTargetService;
import com.truecaller.calling.dialer.y;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.calling.recorder.aj;
import com.truecaller.config.UpdateConfigWorker;
import com.truecaller.config.UpdateInstallationWorker;
import com.truecaller.fcm.DelayedPushReceiver;
import com.truecaller.filters.FilterManager;
import com.truecaller.filters.sync.FilterRestoreWorker;
import com.truecaller.filters.sync.FilterSettingsUploadWorker;
import com.truecaller.filters.sync.FilterUploadWorker;
import com.truecaller.filters.sync.TopSpammersSyncRecurringWorker;
import com.truecaller.filters.v;
import com.truecaller.messaging.categorizer.UnclassifiedMessagesTask;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.notifications.ReactionBroadcastReceiver;
import com.truecaller.messaging.transport.im.FetchImContactsWorker;
import com.truecaller.messaging.transport.im.ImSubscriptionService;
import com.truecaller.messaging.transport.im.JoinedImUsersNotificationTask;
import com.truecaller.messaging.transport.im.RetryImMessageWorker;
import com.truecaller.messaging.transport.im.SendImReportWorker;
import com.truecaller.messaging.transport.im.SendReactionWorker;
import com.truecaller.messaging.transport.im.bu;
import com.truecaller.multisim.ae;
import com.truecaller.network.spamUrls.FetchSpamLinksWhiteListWorker;
import com.truecaller.notifications.OTPCopierService;
import com.truecaller.notifications.RegistrationNudgeTask;
import com.truecaller.notifications.aa;
import com.truecaller.notifications.ah;
import com.truecaller.premium.PremiumStatusRecurringTask;
import com.truecaller.premium.br;
import com.truecaller.presence.RingerModeListenerWorker;
import com.truecaller.presence.SendPresenceSettingWorker;
import com.truecaller.push.PushIdRegistrationTask;
import com.truecaller.service.MissedCallsNotificationService;
import com.truecaller.service.UGCBackgroundTask;
import com.truecaller.swish.g.a;
import com.truecaller.truepay.app.fcm.TruepayFcmManager;
import com.truecaller.ui.QaOtpListActivity;
import com.truecaller.util.ad;
import com.truecaller.util.background.CleanUpBackgroundWorker;
import com.truecaller.util.bd;
import com.truecaller.util.bg;
import com.truecaller.util.bt;
import com.truecaller.util.bv;
import com.truecaller.util.cb;
import com.truecaller.util.cf;
import com.truecaller.voip.ai;
import com.truecaller.whoviewedme.ProfileViewService;
import com.truecaller.whoviewedme.WhoViewedMeNotificationService;
import java.util.Set;

public abstract interface bp
  extends com.truecaller.analytics.d, com.truecaller.engagementrewards.i
{
  public abstract ad A();
  
  public abstract com.truecaller.notifications.g B();
  
  public abstract com.truecaller.messaging.h C();
  
  public abstract com.truecaller.i.c D();
  
  public abstract Set E();
  
  public abstract com.truecaller.i.e F();
  
  public abstract com.truecaller.calling.d.q G();
  
  public abstract com.truecaller.i.g H();
  
  public abstract com.truecaller.common.g.a I();
  
  public abstract com.truecaller.common.h.ac J();
  
  public abstract com.truecaller.search.local.model.g K();
  
  public abstract com.truecaller.network.search.e L();
  
  public abstract com.truecaller.androidactors.f M();
  
  public abstract com.truecaller.search.local.model.c N();
  
  public abstract com.truecaller.search.local.model.c O();
  
  public abstract FilterManager P();
  
  public abstract v Q();
  
  public abstract com.truecaller.filters.p R();
  
  public abstract com.truecaller.androidactors.f S();
  
  public abstract com.truecaller.common.account.r T();
  
  public abstract com.truecaller.multisim.h U();
  
  public abstract com.truecaller.common.h.u V();
  
  public abstract com.truecaller.notifications.a W();
  
  public abstract ContentResolver X();
  
  public abstract com.truecaller.aftercall.a Y();
  
  public abstract com.truecaller.service.e Z();
  
  public abstract com.truecaller.ads.leadgen.b a(com.truecaller.ads.leadgen.c paramc);
  
  public abstract com.truecaller.callerid.m a(com.truecaller.callerid.q paramq);
  
  public abstract y a(bi parambi);
  
  public abstract com.truecaller.consentrefresh.a a(com.truecaller.consentrefresh.c paramc);
  
  public abstract com.truecaller.fcm.a a(com.truecaller.fcm.b paramb);
  
  public abstract com.truecaller.feature_toggles.control_panel.h a(com.truecaller.feature_toggles.control_panel.i parami);
  
  public abstract void a(AppHeartBeatTask paramAppHeartBeatTask);
  
  public abstract void a(AppSettingsTask paramAppSettingsTask);
  
  public abstract void a(InstalledAppsHeartbeatWorker paramInstalledAppsHeartbeatWorker);
  
  public abstract void a(EventsUploadWorker paramEventsUploadWorker);
  
  public abstract void a(BackupLogWorker paramBackupLogWorker);
  
  public abstract void a(SuggestionsChooserTargetService paramSuggestionsChooserTargetService);
  
  public abstract void a(UpdateConfigWorker paramUpdateConfigWorker);
  
  public abstract void a(UpdateInstallationWorker paramUpdateInstallationWorker);
  
  public abstract void a(DelayedPushReceiver paramDelayedPushReceiver);
  
  public abstract void a(FilterRestoreWorker paramFilterRestoreWorker);
  
  public abstract void a(FilterSettingsUploadWorker paramFilterSettingsUploadWorker);
  
  public abstract void a(FilterUploadWorker paramFilterUploadWorker);
  
  public abstract void a(TopSpammersSyncRecurringWorker paramTopSpammersSyncRecurringWorker);
  
  public abstract void a(UnclassifiedMessagesTask paramUnclassifiedMessagesTask);
  
  public abstract void a(ReactionBroadcastReceiver paramReactionBroadcastReceiver);
  
  public abstract void a(FetchImContactsWorker paramFetchImContactsWorker);
  
  public abstract void a(ImSubscriptionService paramImSubscriptionService);
  
  public abstract void a(JoinedImUsersNotificationTask paramJoinedImUsersNotificationTask);
  
  public abstract void a(RetryImMessageWorker paramRetryImMessageWorker);
  
  public abstract void a(SendImReportWorker paramSendImReportWorker);
  
  public abstract void a(SendReactionWorker paramSendReactionWorker);
  
  public abstract void a(FetchSpamLinksWhiteListWorker paramFetchSpamLinksWhiteListWorker);
  
  public abstract void a(OTPCopierService paramOTPCopierService);
  
  public abstract void a(RegistrationNudgeTask paramRegistrationNudgeTask);
  
  public abstract void a(PremiumStatusRecurringTask paramPremiumStatusRecurringTask);
  
  public abstract void a(RingerModeListenerWorker paramRingerModeListenerWorker);
  
  public abstract void a(SendPresenceSettingWorker paramSendPresenceSettingWorker);
  
  public abstract void a(PushIdRegistrationTask paramPushIdRegistrationTask);
  
  public abstract void a(MissedCallsNotificationService paramMissedCallsNotificationService);
  
  public abstract void a(UGCBackgroundTask paramUGCBackgroundTask);
  
  public abstract void a(com.truecaller.smsparser.k paramk);
  
  public abstract void a(QaOtpListActivity paramQaOtpListActivity);
  
  public abstract void a(com.truecaller.ui.dialogs.m paramm);
  
  public abstract void a(CleanUpBackgroundWorker paramCleanUpBackgroundWorker);
  
  public abstract void a(ProfileViewService paramProfileViewService);
  
  public abstract void a(WhoViewedMeNotificationService paramWhoViewedMeNotificationService);
  
  public abstract com.truecaller.util.d.a aA();
  
  public abstract com.truecaller.notificationchannels.p aB();
  
  public abstract com.truecaller.notificationchannels.e aC();
  
  public abstract com.truecaller.notificationchannels.b aD();
  
  public abstract com.truecaller.notificationchannels.j aE();
  
  public abstract com.truecaller.featuretoggles.e aF();
  
  public abstract bw aG();
  
  public abstract com.truecaller.androidactors.f aH();
  
  public abstract com.truecaller.common.h.c aI();
  
  public abstract com.truecaller.util.af aJ();
  
  public abstract ah aK();
  
  public abstract aa aL();
  
  public abstract com.truecaller.clevertap.l aM();
  
  public abstract com.truecaller.androidactors.f aN();
  
  public abstract android.support.v4.app.ac aO();
  
  public abstract com.truecaller.scanner.l aP();
  
  public abstract com.truecaller.util.g aQ();
  
  public abstract com.truecaller.androidactors.f aR();
  
  public abstract com.truecaller.f.a aS();
  
  public abstract TruepayFcmManager aT();
  
  public abstract g.a aU();
  
  public abstract com.truecaller.flashsdk.core.b aV();
  
  public abstract com.truecaller.flashsdk.core.s aW();
  
  public abstract com.truecaller.a.f aX();
  
  public abstract com.truecaller.flash.d aY();
  
  public abstract com.truecaller.androidactors.f aZ();
  
  public abstract com.truecaller.data.entity.g aa();
  
  public abstract com.truecaller.calling.ak ab();
  
  public abstract com.truecaller.calling.k ac();
  
  public abstract com.truecaller.androidactors.f ad();
  
  public abstract com.truecaller.androidactors.f ae();
  
  public abstract com.truecaller.presence.r af();
  
  public abstract com.truecaller.analytics.w ag();
  
  public abstract com.truecaller.util.b ah();
  
  public abstract com.truecaller.common.f.c ai();
  
  public abstract com.truecaller.util.b.j aj();
  
  public abstract com.truecaller.premium.a.d ak();
  
  public abstract com.truecaller.premium.data.m al();
  
  public abstract com.truecaller.premium.data.s am();
  
  public abstract com.truecaller.common.f.b an();
  
  public abstract com.truecaller.abtest.c ao();
  
  public abstract com.google.c.a.k ap();
  
  public abstract com.truecaller.ads.provider.f aq();
  
  public abstract com.truecaller.ads.provider.a ar();
  
  public abstract com.truecaller.i.a as();
  
  public abstract com.truecaller.ads.provider.a.a at();
  
  public abstract com.truecaller.androidactors.f au();
  
  public abstract com.truecaller.ads.provider.fetch.l av();
  
  public abstract AdsConfigurationManager aw();
  
  public abstract ae ax();
  
  public abstract com.truecaller.profile.data.c ay();
  
  public abstract com.truecaller.androidactors.f az();
  
  public abstract com.truecaller.b.c bA();
  
  public abstract cb bB();
  
  public abstract bt bC();
  
  public abstract bu bD();
  
  public abstract com.truecaller.payments.a bE();
  
  public abstract br bF();
  
  public abstract com.truecaller.premium.b.a bG();
  
  public abstract com.truecaller.messaging.data.c bH();
  
  public abstract com.truecaller.androidactors.f bI();
  
  public abstract com.truecaller.sdk.push.b bJ();
  
  public abstract com.truecaller.callerid.a.a bK();
  
  public abstract com.truecaller.utils.a bL();
  
  public abstract com.truecaller.calling.initiate_call.b bM();
  
  public abstract com.truecaller.messaging.conversation.a.b.k bN();
  
  public abstract com.truecaller.credit.e bO();
  
  public abstract com.truecaller.androidactors.f bP();
  
  public abstract com.truecaller.calling.recorder.u bQ();
  
  public abstract com.truecaller.calling.after_call.b bR();
  
  public abstract com.truecaller.calling.after_call.a bS();
  
  public abstract com.truecaller.calling.after_call.d bT();
  
  public abstract aj bU();
  
  public abstract com.truecaller.messaging.j bV();
  
  public abstract com.truecaller.messaging.g.d bW();
  
  public abstract com.truecaller.scanner.n bX();
  
  public abstract com.truecaller.messaging.data.al bY();
  
  public abstract com.truecaller.push.e bZ();
  
  public abstract com.truecaller.scanner.r ba();
  
  public abstract com.truecaller.util.bn bb();
  
  public abstract com.truecaller.messaging.c.a bc();
  
  public abstract com.truecaller.flash.m bd();
  
  public abstract com.truecaller.flash.o be();
  
  public abstract com.truecaller.common.profile.e bf();
  
  public abstract CallRecordingManager bg();
  
  public abstract com.truecaller.calling.recorder.h bh();
  
  public abstract com.truecaller.androidactors.f bi();
  
  public abstract com.truecaller.flash.b bj();
  
  public abstract com.truecaller.whoviewedme.w bk();
  
  public abstract c.d.f bl();
  
  public abstract c.d.f bm();
  
  public abstract c.d.f bn();
  
  public abstract com.truecaller.data.access.c bo();
  
  public abstract com.truecaller.clevertap.e bp();
  
  public abstract com.truecaller.calling.dialer.suggested_contacts.f bq();
  
  public abstract com.truecaller.data.access.i br();
  
  public abstract a.a bs();
  
  public abstract com.truecaller.tcpermissions.l bt();
  
  public abstract com.truecaller.tcpermissions.o bu();
  
  public abstract ar bv();
  
  public abstract com.truecaller.utils.l bw();
  
  public abstract com.truecaller.utils.d bx();
  
  public abstract bv by();
  
  public abstract cf bz();
  
  public abstract com.truecaller.whoviewedme.g cA();
  
  public abstract com.truecaller.service.d cB();
  
  public abstract com.truecaller.calling.initiate_call.j cC();
  
  public abstract com.truecaller.push.b ca();
  
  public abstract com.truecaller.voip.d cb();
  
  public abstract ai cc();
  
  public abstract com.truecaller.voip.util.ak cd();
  
  public abstract com.truecaller.incallui.a ce();
  
  public abstract com.truecaller.smsparser.b.a cf();
  
  public abstract com.truecaller.util.background.a cg();
  
  public abstract com.truecaller.search.b ch();
  
  public abstract com.truecaller.androidactors.f ci();
  
  public abstract com.truecaller.androidactors.f cj();
  
  public abstract com.truecaller.messaging.transport.im.bn ck();
  
  public abstract com.truecaller.content.d.a cl();
  
  public abstract androidx.work.p cm();
  
  public abstract com.truecaller.premium.m cn();
  
  public abstract void co();
  
  public abstract com.truecaller.messaging.transport.im.a.i cp();
  
  public abstract com.truecaller.messaging.h.c cq();
  
  public abstract com.truecaller.filters.r cr();
  
  public abstract com.truecaller.ui.af cs();
  
  public abstract com.truecaller.calling.contacts_list.j ct();
  
  public abstract com.truecaller.calling.d.d cu();
  
  public abstract com.truecaller.backup.b cv();
  
  public abstract com.truecaller.startup_dialogs.b.a cw();
  
  public abstract com.truecaller.consentrefresh.e cx();
  
  public abstract com.truecaller.calling.recorder.b cy();
  
  public abstract com.truecaller.update.c cz();
  
  public abstract Context l();
  
  public abstract com.truecaller.androidactors.k m();
  
  public abstract com.truecaller.calling.e.e n();
  
  public abstract com.truecaller.messaging.transport.m o();
  
  public abstract com.truecaller.androidactors.f p();
  
  public abstract com.truecaller.androidactors.f q();
  
  public abstract com.truecaller.utils.n r();
  
  public abstract ax s();
  
  public abstract com.truecaller.util.al t();
  
  public abstract com.truecaller.network.search.l u();
  
  public abstract com.truecaller.utils.i v();
  
  public abstract com.truecaller.androidactors.f w();
  
  public abstract com.truecaller.androidactors.f x();
  
  public abstract bg y();
  
  public abstract bd z();
}

/* Location:
 * Qualified Name:     com.truecaller.bp
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */