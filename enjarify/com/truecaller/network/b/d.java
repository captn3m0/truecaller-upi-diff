package com.truecaller.network.b;

import c.g.b.k;
import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.tracking.events.p;
import com.truecaller.tracking.events.p.a;
import com.truecaller.utils.a;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import okhttp3.ab;
import okhttp3.ad;
import okhttp3.e;
import okhttp3.i;
import okhttp3.q;

final class d
  extends q
{
  private final String a;
  private final Map c;
  private final f d;
  private final a e;
  private final String f;
  
  public d(f paramf, a parama, String paramString)
  {
    d = paramf;
    e = parama;
    f = paramString;
    paramf = UUID.randomUUID().toString();
    k.a(paramf, "UUID.randomUUID().toString()");
    a = paramf;
    paramf = new java/util/LinkedHashMap;
    paramf.<init>();
    paramf = (Map)paramf;
    c = paramf;
  }
  
  private final void a(String paramString)
  {
    c localc = (c)c.get(paramString);
    if (localc == null)
    {
      localc = new com/truecaller/network/b/c;
      localc.<init>(paramString);
      Map localMap = c;
      localMap.put(paramString, localc);
    }
    long l = e.c();
    a = l;
  }
  
  private final void a(String paramString, boolean paramBoolean)
  {
    c localc = (c)c.remove(paramString);
    if (localc == null) {
      return;
    }
    long l1 = e.c();
    b = l1;
    c = paramBoolean;
    ae localae = (ae)d.a();
    p.a locala = p.b();
    CharSequence localCharSequence = (CharSequence)a;
    locala.c(localCharSequence);
    localCharSequence = (CharSequence)f;
    locala.a(localCharSequence);
    paramString = (CharSequence)paramString;
    locala.b(paramString);
    long l2 = a;
    locala.a(l2);
    l2 = b;
    long l3 = a;
    l2 -= l3;
    locala.b(l2);
    paramString = Boolean.valueOf(c);
    locala.a(paramString);
    paramString = (org.apache.a.d.d)locala.a();
    localae.a(paramString);
  }
  
  public final void a(e parame)
  {
    k.b(parame, "call");
    a("call");
  }
  
  public final void a(e parame, IOException paramIOException)
  {
    k.b(parame, "call");
    k.b(paramIOException, "ioe");
    a("call", false);
  }
  
  public final void a(e parame, String paramString)
  {
    k.b(parame, "call");
    k.b(paramString, "domainName");
    a("dns");
  }
  
  public final void a(e parame, String paramString, List paramList)
  {
    k.b(parame, "call");
    k.b(paramString, "domainName");
    k.b(paramList, "inetAddressList");
    a("dns", true);
  }
  
  public final void a(e parame, InetSocketAddress paramInetSocketAddress, Proxy paramProxy)
  {
    k.b(parame, "call");
    k.b(paramInetSocketAddress, "inetSocketAddress");
    k.b(paramProxy, "proxy");
    a("connect");
  }
  
  public final void a(e parame, InetSocketAddress paramInetSocketAddress, Proxy paramProxy, IOException paramIOException)
  {
    k.b(parame, "call");
    k.b(paramInetSocketAddress, "addr");
    k.b(paramProxy, "proxy");
    k.b(paramIOException, "ioe");
    a("connect", false);
  }
  
  public final void a(e parame, ab paramab)
  {
    k.b(parame, "call");
    k.b(paramab, "request");
    a("requestHeaders", true);
  }
  
  public final void a(e parame, ad paramad)
  {
    k.b(parame, "call");
    k.b(paramad, "response");
    a("responseHeaders", true);
  }
  
  public final void a(e parame, i parami)
  {
    k.b(parame, "call");
    k.b(parami, "connection");
    a("connection");
  }
  
  public final void b(e parame)
  {
    k.b(parame, "call");
    a("secureConnect");
  }
  
  public final void b(e parame, InetSocketAddress paramInetSocketAddress, Proxy paramProxy)
  {
    k.b(parame, "call");
    k.b(paramInetSocketAddress, "addr");
    k.b(paramProxy, "proxy");
    a("connect", true);
  }
  
  public final void b(e parame, i parami)
  {
    k.b(parame, "call");
    k.b(parami, "connection");
    a("connection", true);
  }
  
  public final void c(e parame)
  {
    k.b(parame, "call");
    a("secureConnect", true);
  }
  
  public final void d(e parame)
  {
    k.b(parame, "call");
    a("requestHeaders");
  }
  
  public final void e(e parame)
  {
    k.b(parame, "call");
    a("requestBody");
  }
  
  public final void f(e parame)
  {
    k.b(parame, "call");
    a("requestBody", true);
  }
  
  public final void g(e parame)
  {
    k.b(parame, "call");
    a("responseHeaders");
  }
  
  public final void h(e parame)
  {
    k.b(parame, "call");
    a("responseBody");
  }
  
  public final void i(e parame)
  {
    k.b(parame, "call");
    a("responseBody", true);
  }
  
  public final void j(e parame)
  {
    k.b(parame, "call");
    a("call", true);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */