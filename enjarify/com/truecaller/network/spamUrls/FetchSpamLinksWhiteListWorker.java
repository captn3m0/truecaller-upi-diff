package com.truecaller.network.spamUrls;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.common.background.TrackedWorker;

public final class FetchSpamLinksWhiteListWorker
  extends TrackedWorker
{
  public static final FetchSpamLinksWhiteListWorker.a e;
  public d b;
  public r c;
  public b d;
  
  static
  {
    FetchSpamLinksWhiteListWorker.a locala = new com/truecaller/network/spamUrls/FetchSpamLinksWhiteListWorker$a;
    locala.<init>((byte)0);
    e = locala;
  }
  
  public FetchSpamLinksWhiteListWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final b b()
  {
    b localb = d;
    if (localb == null)
    {
      String str = "analytics";
      k.a(str);
    }
    return localb;
  }
  
  public final boolean c()
  {
    r localr = c;
    if (localr == null)
    {
      String str = "accountManager";
      k.a(str);
    }
    return localr.c();
  }
  
  public final ListenableWorker.a d()
  {
    Object localObject = b;
    if (localObject == null)
    {
      str = "whiteListRepository";
      k.a(str);
    }
    boolean bool = ((d)localObject).a();
    if (bool) {
      localObject = ListenableWorker.a.a();
    }
    for (String str = "Result.success()";; str = "Result.retry()")
    {
      k.a(localObject, str);
      return (ListenableWorker.a)localObject;
      localObject = ListenableWorker.a.b();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.spamUrls.FetchSpamLinksWhiteListWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */