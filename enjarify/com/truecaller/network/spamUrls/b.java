package com.truecaller.network.spamUrls;

import c.g.b.k;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.h;
import com.truecaller.messaging.conversation.spamLinks.GetUrlReportDto;
import java.util.List;

public final class b
  implements a
{
  private final b.a a;
  
  public b()
  {
    b.a locala = (b.a)h.a(KnownEndpoints.SPAM_URL, b.a.class);
    a = locala;
  }
  
  public final b.a a()
  {
    return a;
  }
  
  public final e.b a(String paramString)
  {
    k.b(paramString, "url");
    b.a locala = a;
    GetUrlReportDto localGetUrlReportDto = new com/truecaller/messaging/conversation/spamLinks/GetUrlReportDto;
    localGetUrlReportDto.<init>(paramString);
    return locala.a(localGetUrlReportDto);
  }
  
  public final e.b a(List paramList)
  {
    k.b(paramList, "urlReports");
    return a.a(paramList);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.spamUrls.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */