package com.truecaller.network.spamUrls;

import android.content.Context;
import c.f.b;
import c.g.a.m;
import com.google.gson.p;
import com.truecaller.messaging.conversation.spamLinks.UrlDto;
import com.truecaller.messaging.conversation.spamLinks.WhitelistDto;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class e
  implements d
{
  final File a;
  private final Set b;
  private final Set c;
  private final a d;
  private final c.d.f e;
  
  public e(a parama, c.d.f paramf, Context paramContext)
  {
    d = parama;
    e = paramf;
    parama = new java/io/File;
    paramf = paramContext.getFilesDir();
    parama.<init>(paramf, "whitelisted_urls.json");
    a = parama;
    parama = new java/util/LinkedHashSet;
    parama.<init>();
    parama = (Set)parama;
    b = parama;
    parama = new java/util/LinkedHashSet;
    parama.<init>();
    parama = (Set)parama;
    c = parama;
    parama = (ag)bg.a;
    paramf = e;
    paramContext = new com/truecaller/network/spamUrls/e$1;
    paramContext.<init>(this, null);
    paramContext = (m)paramContext;
    kotlinx.coroutines.e.b(parama, paramf, paramContext, 2);
  }
  
  static WhitelistDto a(File paramFile)
  {
    localObject1 = null;
    try
    {
      Object localObject2 = c.n.d.a;
      Object localObject5 = new java/io/FileInputStream;
      ((FileInputStream)localObject5).<init>(paramFile);
      localObject5 = (InputStream)localObject5;
      paramFile = new java/io/InputStreamReader;
      paramFile.<init>((InputStream)localObject5, (Charset)localObject2);
      paramFile = (Closeable)paramFile;
      localObject2 = paramFile;
      try
      {
        localObject2 = (InputStreamReader)paramFile;
        localObject5 = new com/google/gson/f;
        ((com.google.gson.f)localObject5).<init>();
        localObject2 = (Reader)localObject2;
        Class localClass = WhitelistDto.class;
        localObject2 = ((com.google.gson.f)localObject5).a((Reader)localObject2, localClass);
        localObject2 = (WhitelistDto)localObject2;
        b.a(paramFile, null);
        localObject1 = localObject2;
      }
      finally
      {
        try
        {
          throw ((Throwable)localObject3);
        }
        finally
        {
          localObject5 = localObject3;
          Object localObject4 = localObject6;
          b.a(paramFile, (Throwable)localObject5);
        }
      }
      return (WhitelistDto)localObject1;
    }
    catch (Exception localException)
    {
      paramFile = (Throwable)localException;
      com.truecaller.log.d.a(paramFile);
    }
    catch (p localp)
    {
      paramFile = (Throwable)localp;
      com.truecaller.log.d.a(paramFile);
    }
  }
  
  final void a(WhitelistDto paramWhitelistDto)
  {
    try
    {
      paramWhitelistDto = paramWhitelistDto.getUrls();
      paramWhitelistDto = (Iterable)paramWhitelistDto;
      paramWhitelistDto = paramWhitelistDto.iterator();
      for (;;)
      {
        boolean bool1 = paramWhitelistDto.hasNext();
        if (!bool1) {
          break;
        }
        Object localObject1 = paramWhitelistDto.next();
        localObject1 = (UrlDto)localObject1;
        Object localObject2 = ((UrlDto)localObject1).getKind();
        int i = ((String)localObject2).hashCode();
        int j = 3210;
        String str;
        boolean bool2;
        if (i != j)
        {
          j = 3654;
          if (i == j)
          {
            str = "rx";
            bool2 = ((String)localObject2).equals(str);
            if (bool2)
            {
              localObject2 = c;
              localObject1 = ((UrlDto)localObject1).getUrl();
              ((Set)localObject2).add(localObject1);
            }
          }
        }
        else
        {
          str = "dn";
          bool2 = ((String)localObject2).equals(str);
          if (bool2)
          {
            localObject2 = b;
            localObject1 = ((UrlDto)localObject1).getUrl();
            ((Set)localObject2).add(localObject1);
          }
        }
      }
      return;
    }
    finally {}
  }
  
  /* Error */
  public final boolean a()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 32	com/truecaller/network/spamUrls/e:d	Lcom/truecaller/network/spamUrls/a;
    //   4: invokeinterface 176 1 0
    //   9: invokeinterface 181 1 0
    //   14: invokestatic 186	com/truecaller/common/h/q:a	(Le/b;)Le/r;
    //   17: astore_1
    //   18: aload_1
    //   19: ifnull +193 -> 212
    //   22: aload_1
    //   23: invokevirtual 190	e/r:d	()Z
    //   26: istore_2
    //   27: aconst_null
    //   28: astore_3
    //   29: iload_2
    //   30: ifeq +6 -> 36
    //   33: goto +5 -> 38
    //   36: aconst_null
    //   37: astore_1
    //   38: aload_1
    //   39: ifnull +173 -> 212
    //   42: aload_1
    //   43: invokevirtual 192	e/r:e	()Ljava/lang/Object;
    //   46: checkcast 104	com/truecaller/messaging/conversation/spamLinks/WhitelistDto
    //   49: astore_1
    //   50: aload_1
    //   51: ifnull +161 -> 212
    //   54: ldc -62
    //   56: astore 4
    //   58: aload_1
    //   59: aload 4
    //   61: invokestatic 196	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   64: aload_0
    //   65: getfield 49	com/truecaller/network/spamUrls/e:a	Ljava/io/File;
    //   68: astore 4
    //   70: getstatic 83	c/n/d:a	Ljava/nio/charset/Charset;
    //   73: astore 5
    //   75: new 198	java/io/FileOutputStream
    //   78: astore 6
    //   80: aload 6
    //   82: aload 4
    //   84: invokespecial 199	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   87: aload 6
    //   89: checkcast 201	java/io/OutputStream
    //   92: astore 6
    //   94: new 203	java/io/OutputStreamWriter
    //   97: astore 4
    //   99: aload 4
    //   101: aload 6
    //   103: aload 5
    //   105: invokespecial 206	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V
    //   108: aload 4
    //   110: checkcast 97	java/io/Closeable
    //   113: astore 4
    //   115: aload 4
    //   117: astore 5
    //   119: aload 4
    //   121: checkcast 203	java/io/OutputStreamWriter
    //   124: astore 5
    //   126: new 99	com/google/gson/f
    //   129: astore 6
    //   131: aload 6
    //   133: invokespecial 100	com/google/gson/f:<init>	()V
    //   136: aload 5
    //   138: checkcast 208	java/lang/Appendable
    //   141: astore 5
    //   143: aload 6
    //   145: aload_1
    //   146: aload 5
    //   148: invokevirtual 211	com/google/gson/f:a	(Ljava/lang/Object;Ljava/lang/Appendable;)V
    //   151: getstatic 216	c/x:a	Lc/x;
    //   154: astore 5
    //   156: aload 4
    //   158: aconst_null
    //   159: invokestatic 112	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   162: goto +43 -> 205
    //   165: astore 5
    //   167: goto +6 -> 173
    //   170: astore_3
    //   171: aload_3
    //   172: athrow
    //   173: aload 4
    //   175: aload_3
    //   176: invokestatic 112	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   179: aload 5
    //   181: athrow
    //   182: checkcast 114	java/lang/Throwable
    //   185: astore 4
    //   187: aload 4
    //   189: invokestatic 119	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   192: goto +13 -> 205
    //   195: checkcast 114	java/lang/Throwable
    //   198: astore 4
    //   200: aload 4
    //   202: invokestatic 119	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   205: aload_0
    //   206: aload_1
    //   207: invokevirtual 219	com/truecaller/network/spamUrls/e:a	(Lcom/truecaller/messaging/conversation/spamLinks/WhitelistDto;)V
    //   210: iconst_1
    //   211: ireturn
    //   212: iconst_0
    //   213: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	214	0	this	e
    //   17	190	1	localObject1	Object
    //   26	4	2	bool	boolean
    //   28	1	3	localObject2	Object
    //   170	6	3	localThrowable	Throwable
    //   56	145	4	localObject3	Object
    //   73	82	5	localObject4	Object
    //   165	15	5	localObject5	Object
    //   78	66	6	localObject6	Object
    //   182	1	9	localException	Exception
    //   195	1	10	localp	p
    // Exception table:
    //   from	to	target	type
    //   171	173	165	finally
    //   119	124	170	finally
    //   126	129	170	finally
    //   131	136	170	finally
    //   136	141	170	finally
    //   146	151	170	finally
    //   151	154	170	finally
    //   64	68	182	java/lang/Exception
    //   70	73	182	java/lang/Exception
    //   75	78	182	java/lang/Exception
    //   82	87	182	java/lang/Exception
    //   87	92	182	java/lang/Exception
    //   94	97	182	java/lang/Exception
    //   103	108	182	java/lang/Exception
    //   108	113	182	java/lang/Exception
    //   158	162	182	java/lang/Exception
    //   175	179	182	java/lang/Exception
    //   179	182	182	java/lang/Exception
    //   64	68	195	com/google/gson/p
    //   70	73	195	com/google/gson/p
    //   75	78	195	com/google/gson/p
    //   82	87	195	com/google/gson/p
    //   87	92	195	com/google/gson/p
    //   94	97	195	com/google/gson/p
    //   103	108	195	com/google/gson/p
    //   108	113	195	com/google/gson/p
    //   158	162	195	com/google/gson/p
    //   175	179	195	com/google/gson/p
    //   179	182	195	com/google/gson/p
  }
  
  public final Set b()
  {
    Set localSet = b;
    boolean bool = localSet.isEmpty();
    if (bool) {
      localSet = null;
    }
    if (localSet == null) {
      localSet = (Set)g.a();
    }
    return localSet;
  }
  
  public final Set c()
  {
    return c;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.spamUrls.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */