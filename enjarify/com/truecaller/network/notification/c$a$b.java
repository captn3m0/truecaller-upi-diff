package com.truecaller.network.notification;

public final class c$a$b
  implements Comparable
{
  public long a;
  public NotificationType b;
  public NotificationScope c;
  public long d;
  
  public c$a$b()
  {
    Object localObject = NotificationType.UNSUPPORTED;
    b = ((NotificationType)localObject);
    localObject = NotificationScope.GLOBAL;
    c = ((NotificationScope)localObject);
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof b;
    if (bool1)
    {
      paramObject = (b)paramObject;
      if (paramObject != this)
      {
        long l1 = a;
        long l2 = a;
        bool1 = l1 < l2;
        if (!bool1)
        {
          Object localObject1 = b;
          Object localObject2 = b;
          if (localObject1 == localObject2)
          {
            localObject1 = c;
            localObject2 = c;
            if (localObject1 == localObject2)
            {
              l1 = d;
              l2 = d;
              boolean bool2 = l1 < l2;
              if (!bool2) {
                break label104;
              }
            }
          }
        }
        return false;
      }
      label104:
      return true;
    }
    return false;
  }
  
  public final int hashCode()
  {
    long l1 = a;
    int i = 32;
    long l2 = l1 >>> i;
    l1 ^= l2;
    int j = ((int)l1 + 403) * 31;
    Object localObject = b;
    int k = 0;
    if (localObject == null)
    {
      m = 0;
      localObject = null;
    }
    else
    {
      m = ((NotificationType)localObject).hashCode();
    }
    j = (j + m) * 31;
    localObject = c;
    if (localObject != null) {
      k = ((NotificationScope)localObject).hashCode();
    }
    j = (j + k) * 31;
    l2 = d;
    long l3 = l2 >>> i;
    int m = (int)(l2 ^ l3);
    return j + m;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Envelope{id=");
    long l = a;
    localStringBuilder.append(l);
    localStringBuilder.append(", type=");
    Object localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", scope=");
    localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", timestamp=");
    l = d;
    localStringBuilder.append(l);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.notification.c.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */