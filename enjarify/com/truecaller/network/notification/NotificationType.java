package com.truecaller.network.notification;

public enum NotificationType
{
  public final int value;
  
  static
  {
    Object localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("UNSUPPORTED", 0, -1 << -1);
    UNSUPPORTED = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    int i = 1;
    ((NotificationType)localObject).<init>("ANNOUNCEMENT", i, i);
    ANNOUNCEMENT = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    int j = 2;
    ((NotificationType)localObject).<init>("GENERAL", j, j);
    GENERAL = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    int k = 3;
    ((NotificationType)localObject).<init>("SOFTWARE_UPDATE", k, k);
    SOFTWARE_UPDATE = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    int m = 4;
    ((NotificationType)localObject).<init>("OPEN_URL", m, m);
    OPEN_URL = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    int n = 5;
    ((NotificationType)localObject).<init>("SHOW_HTML", n, n);
    SHOW_HTML = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    int i1 = 6;
    ((NotificationType)localObject).<init>("SHOW_VIEW", i1, i1);
    SHOW_VIEW = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    int i2 = 9;
    ((NotificationType)localObject).<init>("CONTACT_REQUEST", 7, i2);
    CONTACT_REQUEST = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    int i3 = 10;
    ((NotificationType)localObject).<init>("CONTACT_REQUEST_ACCEPTED", 8, i3);
    CONTACT_REQUEST_ACCEPTED = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    int i4 = 11;
    ((NotificationType)localObject).<init>("CONTACT_DETAILS_SHARED", i2, i4);
    CONTACT_DETAILS_SHARED = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    int i5 = 12;
    ((NotificationType)localObject).<init>("PREMIUM_STATUS_CHANGED", i3, i5);
    PREMIUM_STATUS_CHANGED = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    int i6 = 13;
    ((NotificationType)localObject).<init>("SEARCH", i4, i6);
    SEARCH = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    int i7 = 14;
    ((NotificationType)localObject).<init>("PROMO_OPEN_URL", i5, i7);
    PROMO_OPEN_URL = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    int i8 = 15;
    ((NotificationType)localObject).<init>("PROMO_DOWNLOAD_URL", i6, i8);
    PROMO_DOWNLOAD_URL = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    int i9 = 18;
    ((NotificationType)localObject).<init>("INVITE_FRIENDS", i7, i9);
    INVITE_FRIENDS = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    i7 = 21;
    ((NotificationType)localObject).<init>("TRIGGER_INITIALIZE", i8, i7);
    TRIGGER_INITIALIZE = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("CALL_ME_BACK", 16, 23);
    CALL_ME_BACK = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("HINT_ANNOUNCEMENT", 17, 101);
    HINT_ANNOUNCEMENT = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("HINT_GENERAL", i9, 102);
    HINT_GENERAL = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("HINT_PREMIUM_FROM_OFFERWALL", 19, 103);
    HINT_PREMIUM_FROM_OFFERWALL = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("HINT_OPEN_URL", 20, 104);
    HINT_OPEN_URL = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("HINT_SHOW_HTML", i7, 105);
    HINT_SHOW_HTML = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("HINT_SHOW_VIEW", 22, 106);
    HINT_SHOW_VIEW = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("HINT_PROFILE_VIEWED", 23, 107);
    HINT_PROFILE_VIEWED = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("HINT_CONTACT_REQUEST", 24, 109);
    HINT_CONTACT_REQUEST = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("HINT_CONTACT_REQUEST_ACCEPTED", 25, 110);
    HINT_CONTACT_REQUEST_ACCEPTED = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("HINT_CONTACT_DETAILS_SHARED", 26, 111);
    HINT_CONTACT_DETAILS_SHARED = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("HINT_SEARCH", 27, 112);
    HINT_SEARCH = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("WEB_SDK", 28, 25);
    WEB_SDK = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("REFERRAL_PREMIUM", 29, 26);
    REFERRAL_PREMIUM = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("GENERIC_WEBVIEW", 30, 27);
    GENERIC_WEBVIEW = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("GENERIC_DEEPLINK", 31, 28);
    GENERIC_DEEPLINK = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("TC_PAYMENT_REQUEST", 32, 29);
    TC_PAYMENT_REQUEST = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("TC_PAYMENT_INCOMING", 33, 30);
    TC_PAYMENT_INCOMING = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("TC_PAYMENT_CONFIRMATION", 34, 31);
    TC_PAYMENT_CONFIRMATION = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("TC_PAYMENT_CUSTOM", 35, 32);
    TC_PAYMENT_CUSTOM = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("WHO_VIEWED_ME", 36, 33);
    WHO_VIEWED_ME = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("PUSH_CALLER_ID", 37, 19);
    PUSH_CALLER_ID = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("SUBSCRIPTION_GRACE", 38, 34);
    SUBSCRIPTION_GRACE = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("SUBSCRIPTION_STATUS_CHANGED", 39, 36);
    SUBSCRIPTION_STATUS_CHANGED = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("VOIP", 40, 38);
    VOIP = (NotificationType)localObject;
    localObject = new com/truecaller/network/notification/NotificationType;
    ((NotificationType)localObject).<init>("CREDIT", 41, 39);
    CREDIT = (NotificationType)localObject;
    localObject = new NotificationType[42];
    NotificationType localNotificationType = UNSUPPORTED;
    localObject[0] = localNotificationType;
    localNotificationType = ANNOUNCEMENT;
    localObject[i] = localNotificationType;
    localNotificationType = GENERAL;
    localObject[j] = localNotificationType;
    localNotificationType = SOFTWARE_UPDATE;
    localObject[k] = localNotificationType;
    localNotificationType = OPEN_URL;
    localObject[m] = localNotificationType;
    localNotificationType = SHOW_HTML;
    localObject[n] = localNotificationType;
    localNotificationType = SHOW_VIEW;
    localObject[i1] = localNotificationType;
    localNotificationType = CONTACT_REQUEST;
    localObject[7] = localNotificationType;
    localNotificationType = CONTACT_REQUEST_ACCEPTED;
    localObject[8] = localNotificationType;
    localNotificationType = CONTACT_DETAILS_SHARED;
    localObject[i2] = localNotificationType;
    localNotificationType = PREMIUM_STATUS_CHANGED;
    localObject[i3] = localNotificationType;
    localNotificationType = SEARCH;
    localObject[i4] = localNotificationType;
    localNotificationType = PROMO_OPEN_URL;
    localObject[i5] = localNotificationType;
    localNotificationType = PROMO_DOWNLOAD_URL;
    localObject[13] = localNotificationType;
    localNotificationType = INVITE_FRIENDS;
    localObject[14] = localNotificationType;
    localNotificationType = TRIGGER_INITIALIZE;
    localObject[15] = localNotificationType;
    localNotificationType = CALL_ME_BACK;
    localObject[16] = localNotificationType;
    localNotificationType = HINT_ANNOUNCEMENT;
    localObject[17] = localNotificationType;
    localNotificationType = HINT_GENERAL;
    localObject[i9] = localNotificationType;
    localNotificationType = HINT_PREMIUM_FROM_OFFERWALL;
    localObject[19] = localNotificationType;
    localNotificationType = HINT_OPEN_URL;
    localObject[20] = localNotificationType;
    localNotificationType = HINT_SHOW_HTML;
    localObject[i7] = localNotificationType;
    localNotificationType = HINT_SHOW_VIEW;
    localObject[22] = localNotificationType;
    localNotificationType = HINT_PROFILE_VIEWED;
    localObject[23] = localNotificationType;
    localNotificationType = HINT_CONTACT_REQUEST;
    localObject[24] = localNotificationType;
    localNotificationType = HINT_CONTACT_REQUEST_ACCEPTED;
    localObject[25] = localNotificationType;
    localNotificationType = HINT_CONTACT_DETAILS_SHARED;
    localObject[26] = localNotificationType;
    localNotificationType = HINT_SEARCH;
    localObject[27] = localNotificationType;
    localNotificationType = WEB_SDK;
    localObject[28] = localNotificationType;
    localNotificationType = REFERRAL_PREMIUM;
    localObject[29] = localNotificationType;
    localNotificationType = GENERIC_WEBVIEW;
    localObject[30] = localNotificationType;
    localNotificationType = GENERIC_DEEPLINK;
    localObject[31] = localNotificationType;
    localNotificationType = TC_PAYMENT_REQUEST;
    localObject[32] = localNotificationType;
    localNotificationType = TC_PAYMENT_INCOMING;
    localObject[33] = localNotificationType;
    localNotificationType = TC_PAYMENT_CONFIRMATION;
    localObject[34] = localNotificationType;
    localNotificationType = TC_PAYMENT_CUSTOM;
    localObject[35] = localNotificationType;
    localNotificationType = WHO_VIEWED_ME;
    localObject[36] = localNotificationType;
    localNotificationType = PUSH_CALLER_ID;
    localObject[37] = localNotificationType;
    localNotificationType = SUBSCRIPTION_GRACE;
    localObject[38] = localNotificationType;
    localNotificationType = SUBSCRIPTION_STATUS_CHANGED;
    localObject[39] = localNotificationType;
    localNotificationType = VOIP;
    localObject[40] = localNotificationType;
    localNotificationType = CREDIT;
    localObject[41] = localNotificationType;
    $VALUES = (NotificationType[])localObject;
  }
  
  private NotificationType(int paramInt1)
  {
    value = paramInt1;
  }
  
  public static NotificationType valueOf(int paramInt)
  {
    NotificationType[] arrayOfNotificationType = values();
    int i = arrayOfNotificationType.length;
    int j = 0;
    while (j < i)
    {
      NotificationType localNotificationType = arrayOfNotificationType[j];
      int k = value;
      if (k == paramInt) {
        return localNotificationType;
      }
      j += 1;
    }
    return UNSUPPORTED;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.notification.NotificationType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */