package com.truecaller.network.notification;

import com.google.gson.g;
import com.truecaller.common.network.util.KnownEndpoints;
import e.f.a;

public final class b
{
  private static final e.a.a.a a;
  
  static
  {
    g localg = new com/google/gson/g;
    localg.<init>();
    Object localObject = new com/truecaller/network/notification/b$b;
    ((b.b)localObject).<init>((byte)0);
    localObject = ((b.b)localObject).a();
    localg = localg.a(NotificationScope.class, localObject);
    localObject = new com/truecaller/network/notification/b$c;
    ((b.c)localObject).<init>((byte)0);
    localObject = ((b.c)localObject).a();
    a = e.a.a.a.a(localg.a(NotificationType.class, localObject).a());
  }
  
  public static e.b a(long paramLong, NotificationScope paramNotificationScope, String paramString)
  {
    Object localObject1 = new com/truecaller/common/network/util/a;
    ((com.truecaller.common.network.util.a)localObject1).<init>();
    Object localObject2 = KnownEndpoints.NOTIFICATION;
    localObject1 = ((com.truecaller.common.network.util.a)localObject1).a((KnownEndpoints)localObject2).a(b.a.class);
    localObject2 = a;
    localObject1 = (b.a)((com.truecaller.common.network.util.a)localObject1).a((f.a)localObject2).b(b.a.class);
    paramNotificationScope = stringValue;
    return ((b.a)localObject1).a(paramNotificationScope, paramLong, paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.notification.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */