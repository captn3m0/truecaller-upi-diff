package com.truecaller.network.notification;

import java.util.Map;
import org.c.a.a.a.h;

public final class c$a
  implements Comparable
{
  public c.a.b a;
  public Map b;
  public c.a.a c;
  
  public final int a(a parama)
  {
    c.a.b localb = a;
    parama = a;
    return h.a(localb, parama);
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof a;
    if (bool1)
    {
      paramObject = (a)paramObject;
      if (paramObject != this)
      {
        Object localObject1 = a;
        Object localObject2 = a;
        bool1 = h.b(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = b;
          localObject2 = b;
          bool1 = h.b(localObject1, localObject2);
          if (bool1)
          {
            paramObject = c;
            localObject1 = c;
            boolean bool2 = h.b(paramObject, localObject1);
            if (bool2) {
              break label90;
            }
          }
        }
        return false;
      }
      label90:
      return true;
    }
    return false;
  }
  
  public final int hashCode()
  {
    Object[] arrayOfObject = new Object[3];
    Object localObject = a;
    arrayOfObject[0] = localObject;
    localObject = b;
    arrayOfObject[1] = localObject;
    localObject = c;
    arrayOfObject[2] = localObject;
    return h.b(arrayOfObject);
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("NotificationDto{envelope=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", control=");
    localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.notification.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */