package com.truecaller.network.notification;

public enum NotificationScope
{
  public final String stringValue;
  public final int value;
  
  static
  {
    Object localObject = new com/truecaller/network/notification/NotificationScope;
    ((NotificationScope)localObject).<init>("LOCAL", 0, -1, "");
    LOCAL = (NotificationScope)localObject;
    localObject = new com/truecaller/network/notification/NotificationScope;
    int i = 1;
    ((NotificationScope)localObject).<init>("GLOBAL", i, i, "global");
    GLOBAL = (NotificationScope)localObject;
    localObject = new com/truecaller/network/notification/NotificationScope;
    int j = 2;
    ((NotificationScope)localObject).<init>("PERSONAL", j, j, "personal");
    PERSONAL = (NotificationScope)localObject;
    localObject = new NotificationScope[3];
    NotificationScope localNotificationScope = LOCAL;
    localObject[0] = localNotificationScope;
    localNotificationScope = GLOBAL;
    localObject[i] = localNotificationScope;
    localNotificationScope = PERSONAL;
    localObject[j] = localNotificationScope;
    $VALUES = (NotificationScope[])localObject;
  }
  
  private NotificationScope(int paramInt1, String paramString1)
  {
    value = paramInt1;
    stringValue = paramString1;
  }
  
  public static NotificationScope valueOf(int paramInt)
  {
    Object localObject = values();
    int i = localObject.length;
    int j = 0;
    while (j < i)
    {
      NotificationScope localNotificationScope = localObject[j];
      int k = value;
      if (k == paramInt) {
        return localNotificationScope;
      }
      j += 1;
    }
    localObject = new java/lang/IllegalArgumentException;
    String str = String.valueOf(paramInt);
    str = "Unknown NotificationScope value, ".concat(str);
    ((IllegalArgumentException)localObject).<init>(str);
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.notification.NotificationScope
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */