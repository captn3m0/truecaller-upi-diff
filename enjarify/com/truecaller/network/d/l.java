package com.truecaller.network.d;

import c.g.b.k;
import io.grpc.c.a;

final class l
{
  final a a;
  final a b;
  final String c;
  final String d;
  
  public l(a parama1, a parama2, String paramString1, String paramString2)
  {
    a = parama1;
    b = parama2;
    c = paramString1;
    d = paramString2;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof l;
      if (bool1)
      {
        paramObject = (l)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = b;
          localObject2 = b;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = c;
            localObject2 = c;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = d;
              paramObject = d;
              boolean bool2 = k.a(localObject1, paramObject);
              if (bool2) {
                break label112;
              }
            }
          }
        }
      }
      return false;
    }
    label112:
    return true;
  }
  
  public final int hashCode()
  {
    a locala = a;
    int i = 0;
    if (locala != null)
    {
      j = locala.hashCode();
    }
    else
    {
      j = 0;
      locala = null;
    }
    j *= 31;
    Object localObject = b;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = c;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = d;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("StubDescriptor(asyncStub=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", syncStub=");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", authToken=");
    localObject = c;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", host=");
    localObject = d;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.d.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */