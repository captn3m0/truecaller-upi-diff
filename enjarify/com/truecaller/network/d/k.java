package com.truecaller.network.d;

import c.a.m;
import c.u;
import com.truecaller.common.account.r;
import com.truecaller.common.network.KnownDomain;
import com.truecaller.common.network.e.b;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.utils.d;
import io.grpc.al;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public abstract class k
  implements j
{
  private final Map a;
  private final KnownEndpoints b;
  private final r c;
  private final com.truecaller.common.account.k d;
  private final d e;
  private final Integer f;
  private final b g;
  private final com.truecaller.common.edge.a h;
  private final com.truecaller.common.network.c i;
  private final boolean j;
  private final e k;
  private final String l;
  private final com.truecaller.d.b m;
  private final com.truecaller.common.h.i n;
  
  public k(KnownEndpoints paramKnownEndpoints, r paramr, com.truecaller.common.account.k paramk, d paramd, Integer paramInteger, b paramb, com.truecaller.common.edge.a parama, com.truecaller.common.network.c paramc, boolean paramBoolean, e parame, String paramString, com.truecaller.d.b paramb1, com.truecaller.common.h.i parami)
  {
    b = paramKnownEndpoints;
    c = paramr;
    d = paramk;
    e = paramd;
    f = paramInteger;
    g = paramb;
    h = parama;
    i = paramc;
    j = paramBoolean;
    k = parame;
    l = paramString;
    m = paramb1;
    n = parami;
    paramKnownEndpoints = new java/util/LinkedHashMap;
    paramKnownEndpoints.<init>();
    paramKnownEndpoints = (Map)paramKnownEndpoints;
    a = paramKnownEndpoints;
  }
  
  private static io.grpc.c.a a(io.grpc.c.a parama, Integer paramInteger)
  {
    if (paramInteger == null) {
      return parama;
    }
    long l1 = paramInteger.intValue();
    paramInteger = TimeUnit.SECONDS;
    parama = parama.withDeadlineAfter(l1, paramInteger);
    c.g.b.k.a(parama, "this.withDeadlineAfter(timeout.toLong(), SECONDS)");
    return parama;
  }
  
  private final io.grpc.i[] b()
  {
    Object localObject = (Collection)m.d(a());
    io.grpc.i[] arrayOfi = new io.grpc.i[0];
    localObject = ((Collection)localObject).toArray(arrayOfi);
    if (localObject != null) {
      return (io.grpc.i[])localObject;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw ((Throwable)localObject);
  }
  
  private final boolean d(com.truecaller.common.network.e parame)
  {
    try
    {
      Object localObject1 = n;
      localObject1 = ((com.truecaller.common.h.i)localObject1).b(parame);
      if (localObject1 != null)
      {
        localObject2 = h;
        localObject1 = a;
        localObject1 = ((KnownDomain)localObject1).getValue();
        localObject3 = b;
        localObject3 = ((KnownEndpoints)localObject3).getKey();
        localObject1 = ((com.truecaller.common.edge.a)localObject2).a((String)localObject1, (String)localObject3);
        if (localObject1 != null) {}
      }
      else
      {
        localObject1 = b;
        localObject2 = h;
        localObject3 = i;
        str = "receiver$0";
        c.g.b.k.b(localObject1, str);
        str = "edgeLocationsManager";
        c.g.b.k.b(localObject2, str);
        str = "domainResolver";
        c.g.b.k.b(localObject3, str);
        localObject1 = ((KnownEndpoints)localObject1).getKey();
        localObject1 = com.truecaller.common.edge.f.a((String)localObject1, (com.truecaller.common.edge.a)localObject2, (com.truecaller.common.network.c)localObject3);
      }
      Object localObject2 = null;
      if (localObject1 == null) {
        return false;
      }
      Object localObject3 = m;
      boolean bool1 = ((com.truecaller.d.b)localObject3).a();
      String str = null;
      if (bool1)
      {
        localObject3 = m;
        com.truecaller.common.h.i locali = n;
        localObject3 = ((com.truecaller.d.b)localObject3).a(parame, locali);
        if (localObject3 == null) {
          return false;
        }
        localObject2 = localObject1;
        localObject1 = localObject3;
      }
      else
      {
        localObject2 = null;
      }
      localObject3 = a;
      localObject3 = ((Map)localObject3).get(parame);
      localObject3 = (l)localObject3;
      if (localObject3 != null)
      {
        localObject3 = d;
      }
      else
      {
        bool1 = false;
        localObject3 = null;
      }
      bool1 = c.g.b.k.a(localObject3, localObject1);
      boolean bool2 = true;
      if (bool1) {
        return bool2;
      }
      localObject3 = io.grpc.okhttp.e.d((String)localObject1);
      boolean bool3 = j;
      if (!bool3)
      {
        localObject4 = k;
        localObject4 = ((e)localObject4).a();
        if (localObject4 != null) {
          ((io.grpc.okhttp.e)localObject3).a((com.d.a.k)localObject4);
        }
      }
      Object localObject4 = TimeUnit.SECONDS;
      ((io.grpc.okhttp.e)localObject3).a((TimeUnit)localObject4);
      localObject4 = l;
      ((io.grpc.okhttp.e)localObject3).a((String)localObject4);
      localObject4 = "this";
      c.g.b.k.a(localObject3, (String)localObject4);
      a((io.grpc.okhttp.e)localObject3);
      if (localObject2 != null) {
        ((io.grpc.okhttp.e)localObject3).b((String)localObject2);
      }
      localObject2 = ((io.grpc.okhttp.e)localObject3).a();
      localObject3 = "OkHttpChannelBuilder.for…y(it) }\n        }.build()";
      c.g.b.k.a(localObject2, (String)localObject3);
      localObject3 = g;
      ((b)localObject3).a(parame, (al)localObject2);
      localObject3 = a;
      localObject4 = new com/truecaller/network/d/l;
      Object localObject5 = localObject2;
      localObject5 = (io.grpc.f)localObject2;
      localObject5 = a((io.grpc.f)localObject5);
      localObject2 = (io.grpc.f)localObject2;
      localObject2 = b((io.grpc.f)localObject2);
      ((l)localObject4).<init>((io.grpc.c.a)localObject5, (io.grpc.c.a)localObject2, null, (String)localObject1);
      ((Map)localObject3).put(parame, localObject4);
      return bool2;
    }
    finally {}
  }
  
  public io.grpc.c.a a(com.truecaller.common.network.e parame)
  {
    Object localObject = "targetDomain";
    c.g.b.k.b(parame, (String)localObject);
    boolean bool = c(parame);
    if (bool)
    {
      localObject = a;
      parame = (l)((Map)localObject).get(parame);
      if (parame != null)
      {
        parame = a;
        if (parame != null) {
          return a(this, parame);
        }
      }
    }
    return null;
  }
  
  public abstract io.grpc.c.a a(io.grpc.f paramf);
  
  public abstract Collection a();
  
  public void a(io.grpc.okhttp.e parame)
  {
    c.g.b.k.b(parame, "builder");
  }
  
  public io.grpc.c.a b(com.truecaller.common.network.e parame)
  {
    Object localObject = "targetDomain";
    c.g.b.k.b(parame, (String)localObject);
    boolean bool = c(parame);
    if (bool)
    {
      localObject = a;
      parame = (l)((Map)localObject).get(parame);
      if (parame != null)
      {
        parame = b;
        if (parame != null) {
          return a(this, parame);
        }
      }
    }
    return null;
  }
  
  public abstract io.grpc.c.a b(io.grpc.f paramf);
  
  public final boolean c(com.truecaller.common.network.e parame)
  {
    Object localObject1 = "targetDomain";
    try
    {
      c.g.b.k.b(parame, (String)localObject1);
      boolean bool1 = d(parame);
      boolean bool2 = false;
      Object localObject2 = null;
      if (!bool1) {
        return false;
      }
      localObject1 = a;
      localObject1 = ((Map)localObject1).get(parame);
      localObject1 = (l)localObject1;
      if (localObject1 == null) {
        return false;
      }
      Object localObject3 = n;
      boolean bool3 = ((com.truecaller.common.h.i)localObject3).a(parame);
      if (bool3)
      {
        localObject3 = d;
        localObject3 = ((com.truecaller.common.account.k)localObject3).a();
      }
      else
      {
        localObject3 = c;
        localObject3 = ((r)localObject3).e();
      }
      if (localObject3 == null) {
        return false;
      }
      localObject2 = c;
      bool2 = c.g.b.k.a(localObject2, localObject3);
      boolean bool4 = true;
      if (bool2) {
        return bool4;
      }
      localObject2 = new com/truecaller/network/d/a;
      ((a)localObject2).<init>((String)localObject3);
      Map localMap = a;
      io.grpc.c.a locala = a;
      Object localObject4 = localObject2;
      localObject4 = (io.grpc.c)localObject2;
      locala = locala.withCallCredentials((io.grpc.c)localObject4);
      localObject4 = b();
      int i1 = localObject4.length;
      localObject4 = Arrays.copyOf((Object[])localObject4, i1);
      localObject4 = (io.grpc.i[])localObject4;
      locala = locala.withInterceptors((io.grpc.i[])localObject4);
      localObject4 = "asyncStub.withCallCreden…ors(*buildInterceptors())";
      c.g.b.k.a(locala, (String)localObject4);
      localObject4 = b;
      localObject2 = (io.grpc.c)localObject2;
      localObject2 = ((io.grpc.c.a)localObject4).withCallCredentials((io.grpc.c)localObject2);
      localObject4 = b();
      i1 = localObject4.length;
      localObject4 = Arrays.copyOf((Object[])localObject4, i1);
      localObject4 = (io.grpc.i[])localObject4;
      localObject2 = ((io.grpc.c.a)localObject2).withInterceptors((io.grpc.i[])localObject4);
      localObject4 = "syncStub.withCallCredent…ors(*buildInterceptors())";
      c.g.b.k.a(localObject2, (String)localObject4);
      localObject1 = d;
      localObject4 = "asyncStub";
      c.g.b.k.b(locala, (String)localObject4);
      localObject4 = "syncStub";
      c.g.b.k.b(localObject2, (String)localObject4);
      localObject4 = "host";
      c.g.b.k.b(localObject1, (String)localObject4);
      localObject4 = new com/truecaller/network/d/l;
      ((l)localObject4).<init>(locala, (io.grpc.c.a)localObject2, (String)localObject3, (String)localObject1);
      localMap.put(parame, localObject4);
      return bool4;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.d.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */