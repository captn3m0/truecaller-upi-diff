package com.truecaller.network.d;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.ConnectivityManager.NetworkCallback;
import android.net.Network;
import c.g.b.k;
import c.u;
import io.grpc.al;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public final class d
  extends ConnectivityManager.NetworkCallback
  implements b
{
  private final ConnectivityManager a;
  private boolean b;
  private boolean c;
  private final Map d;
  
  public d(Context paramContext)
  {
    String str = "connectivity";
    paramContext = paramContext.getSystemService(str);
    if (paramContext != null)
    {
      paramContext = (ConnectivityManager)paramContext;
      a = paramContext;
      paramContext = new java/util/LinkedHashMap;
      paramContext.<init>();
      paramContext = (Map)paramContext;
      d = paramContext;
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.net.ConnectivityManager");
    throw paramContext;
  }
  
  public final void a(Object paramObject, al paramal)
  {
    k.b(paramObject, "tag");
    k.b(paramal, "channel");
    Map localMap = d;
    localMap.put(paramObject, paramal);
    boolean bool = b;
    if (!bool)
    {
      paramObject = a;
      if (paramObject != null)
      {
        paramal = this;
        paramal = (ConnectivityManager.NetworkCallback)this;
        ((ConnectivityManager)paramObject).registerDefaultNetworkCallback(paramal);
        b = true;
        return;
      }
    }
  }
  
  public final void onAvailable(Network paramNetwork)
  {
    Object localObject = "network";
    k.b(paramNetwork, (String)localObject);
    boolean bool1 = c;
    if (bool1)
    {
      paramNetwork = ((Iterable)d.values()).iterator();
      for (;;)
      {
        boolean bool2 = paramNetwork.hasNext();
        if (!bool2) {
          break;
        }
        localObject = (al)paramNetwork.next();
        ((al)localObject).c();
      }
    }
    c = false;
  }
  
  public final void onLost(Network paramNetwork)
  {
    k.b(paramNetwork, "network");
    c = true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.d.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */