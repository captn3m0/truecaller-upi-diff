package com.truecaller.network.d;

import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d
{
  private final g a;
  private final Provider b;
  
  private h(g paramg, Provider paramProvider)
  {
    a = paramg;
    b = paramProvider;
  }
  
  public static h a(g paramg, Provider paramProvider)
  {
    h localh = new com/truecaller/network/d/h;
    localh.<init>(paramg, paramProvider);
    return localh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.d.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */