package com.truecaller.network.d;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import c.g.b.k;
import com.truecaller.utils.i;
import io.grpc.al;
import java.util.LinkedHashMap;
import java.util.Map;

public final class c
  implements b
{
  final Map a;
  final i b;
  private boolean c;
  private final c.a d;
  private final Context e;
  
  public c(Context paramContext, i parami)
  {
    e = paramContext;
    b = parami;
    paramContext = new java/util/LinkedHashMap;
    paramContext.<init>();
    paramContext = (Map)paramContext;
    a = paramContext;
    paramContext = new com/truecaller/network/d/c$a;
    paramContext.<init>(this);
    d = paramContext;
  }
  
  public final void a(Object paramObject, al paramal)
  {
    k.b(paramObject, "tag");
    k.b(paramal, "channel");
    Object localObject = a;
    ((Map)localObject).put(paramObject, paramal);
    boolean bool = c;
    if (!bool)
    {
      paramObject = new android/content/IntentFilter;
      ((IntentFilter)paramObject).<init>();
      ((IntentFilter)paramObject).addAction("android.net.conn.CONNECTIVITY_CHANGE");
      paramal = e;
      localObject = (BroadcastReceiver)d;
      paramal.registerReceiver((BroadcastReceiver)localObject, (IntentFilter)paramObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */