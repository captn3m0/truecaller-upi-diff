package com.truecaller.network.d;

import c.g.b.k;
import com.truecaller.network.a.d;
import io.grpc.ap;
import io.grpc.ba;
import io.grpc.e;
import io.grpc.x.a;
import java.io.IOException;

public final class f$a
  extends x.a
{
  f$a(f paramf, io.grpc.f paramf1, ap paramap, e parame, io.grpc.h paramh)
  {
    super(paramh);
  }
  
  public final void a(String paramString, Throwable paramThrowable)
  {
    super.a(paramString, paramThrowable);
    paramString = ba.a(paramThrowable);
    k.a(paramString, "status");
    paramThrowable = paramString.a();
    Object localObject = ba.i;
    String str = "Status.UNAUTHENTICATED";
    k.a(localObject, str);
    localObject = ((ba)localObject).a();
    if (paramThrowable == localObject) {
      try
      {
        paramString = a;
        paramString = a;
        paramThrowable = b;
        paramThrowable = paramThrowable.a();
        localObject = "next.authority()";
        k.a(paramThrowable, (String)localObject);
        paramString.a(paramThrowable);
        return;
      }
      catch (IOException localIOException)
      {
        return;
      }
    }
    paramThrowable = paramString.a();
    localObject = ba.k;
    str = "Status.FAILED_PRECONDITION";
    k.a(localObject, str);
    localObject = ((ba)localObject).a();
    if (paramThrowable == localObject)
    {
      paramString = paramString.b();
      paramThrowable = "NOT_REGISTERED";
      boolean bool = k.a(paramString, paramThrowable);
      if (bool)
      {
        paramString = a.b;
        paramThrowable = null;
        paramString.d(null);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.d.f.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */