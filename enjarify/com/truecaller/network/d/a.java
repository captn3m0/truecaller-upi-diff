package com.truecaller.network.d;

import io.grpc.ao;
import io.grpc.ao.b;
import io.grpc.ao.e;
import io.grpc.c;
import io.grpc.c.a;
import io.grpc.c.b;
import java.util.concurrent.Executor;

public final class a
  extends c
{
  private static ao.e a;
  private final String b;
  
  static
  {
    ao.b localb = ao.b;
    a = ao.e.a("Authorization", localb);
  }
  
  public a(String paramString)
  {
    paramString = String.valueOf(paramString);
    paramString = "Bearer ".concat(paramString);
    b = paramString;
  }
  
  public final void a(c.b paramb, Executor paramExecutor, c.a parama)
  {
    paramb = new io/grpc/ao;
    paramb.<init>();
    paramExecutor = a;
    String str = b;
    paramb.a(paramExecutor, str);
    parama.a(paramb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */