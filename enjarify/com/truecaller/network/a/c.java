package com.truecaller.network.a;

import android.os.Bundle;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.AppHeartBeatTask;
import com.truecaller.analytics.AppSettingsTask;
import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.log.AssertionUtil;
import java.io.IOException;
import okhttp3.y;

public final class c
  implements a
{
  private final com.truecaller.common.background.b a;
  private final ae b;
  private final y c;
  private final com.truecaller.common.account.r d;
  private final f e;
  
  public c(com.truecaller.common.background.b paramb, ae paramae, y paramy, com.truecaller.common.account.r paramr, f paramf)
  {
    a = paramb;
    b = paramae;
    c = paramy;
    d = paramr;
    e = paramf;
  }
  
  private final boolean a(boolean paramBoolean1, boolean paramBoolean2)
  {
    Object localObject1 = TrueApp.y();
    boolean bool = ((TrueApp)localObject1).p();
    if (!bool) {
      return false;
    }
    AppSettingsTask.b(a);
    localObject1 = AppHeartBeatTask.k;
    localObject1 = a;
    k.b(localObject1, "scheduler");
    Object localObject2 = new android/os/Bundle;
    ((Bundle)localObject2).<init>();
    String str1 = "beatType";
    String str2 = "deactivation";
    ((Bundle)localObject2).putString(str1, str2);
    int i = 10028;
    ((com.truecaller.common.background.b)localObject1).a(i, (Bundle)localObject2);
    localObject1 = b;
    localObject2 = c;
    ((ae)localObject1).a((y)localObject2);
    localObject1 = ((com.truecaller.presence.c)e.a()).c();
    ((w)localObject1).d();
    if (paramBoolean1) {}
    try
    {
      Object localObject3 = com.truecaller.common.network.account.a.a;
      localObject3 = com.truecaller.common.network.account.a.b();
      localObject3 = ((e.b)localObject3).c();
      localObject1 = "AccountRestAdapter.deactivateAndDelete().execute()";
      k.a(localObject3, (String)localObject1);
      break label188;
      localObject3 = com.truecaller.common.network.account.a.a;
      localObject3 = com.truecaller.common.network.account.a.a();
      localObject3 = ((e.b)localObject3).c();
      localObject1 = "AccountRestAdapter.deactivate().execute()";
      k.a(localObject3, (String)localObject1);
      label188:
      paramBoolean1 = ((e.r)localObject3).d();
      if (paramBoolean1)
      {
        localObject3 = d;
        localObject3 = ((com.truecaller.common.account.r)localObject3).e();
        bool = true;
        if (localObject3 != null)
        {
          localObject2 = localObject3;
          localObject2 = (CharSequence)localObject3;
          int j = ((CharSequence)localObject2).length();
          if (j > 0)
          {
            j = 1;
          }
          else
          {
            j = 0;
            localObject2 = null;
          }
          if (j != 0) {
            try
            {
              localObject2 = TrueApp.y();
              str1 = "Deactivate";
              ((TrueApp)localObject2).a((String)localObject3, bool, paramBoolean2, str1);
            }
            catch (SecurityException localSecurityException)
            {
              localThrowable = (Throwable)localSecurityException;
              String[] arrayOfString = new String[0];
              AssertionUtil.shouldNeverHappen(localThrowable, arrayOfString);
            }
          }
        }
        return bool;
      }
    }
    catch (RuntimeException localRuntimeException)
    {
      localThrowable = (Throwable)localRuntimeException;
      AssertionUtil.reportThrowableButNeverCrash(localThrowable);
    }
    catch (IOException localIOException)
    {
      Throwable localThrowable = (Throwable)localIOException;
      AssertionUtil.reportThrowableButNeverCrash(localThrowable);
    }
    return false;
  }
  
  public final w a(boolean paramBoolean)
  {
    w localw = w.b(Boolean.valueOf(a(paramBoolean, false)));
    k.a(localw, "Promise.wrap(deactivateAccount(deleteData, false))");
    return localw;
  }
  
  public final w b(boolean paramBoolean)
  {
    w localw = w.b(Boolean.valueOf(a(paramBoolean, true)));
    k.a(localw, "Promise.wrap(deactivateAccount(deleteData, true))");
    return localw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */