package com.truecaller.network.a;

import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.common.network.account.CheckCredentialsRequestDto;
import com.truecaller.common.network.account.CheckCredentialsResponseDto;
import com.truecaller.common.network.c;
import com.truecaller.log.AssertionUtil;
import e.b;
import java.util.concurrent.TimeUnit;

public final class e
  implements d
{
  private final com.truecaller.common.account.r a;
  private final com.truecaller.common.g.a b;
  private final c c;
  
  public e(com.truecaller.common.account.r paramr, com.truecaller.common.g.a parama, c paramc)
  {
    a = paramr;
    b = parama;
    c = paramc;
  }
  
  public final void a(String paramString)
  {
    Object localObject1 = "requestUrl";
    try
    {
      k.b(paramString, (String)localObject1);
      localObject1 = a;
      boolean bool1 = ((com.truecaller.common.account.r)localObject1).c();
      if (!bool1) {
        return;
      }
      localObject1 = b;
      Object localObject2 = "checkCredentialsLastTime";
      long l1 = 0L;
      long l2 = ((com.truecaller.common.g.a)localObject1).a((String)localObject2, l1);
      Object localObject3 = b;
      Object localObject4 = "checkCredentialsTtl";
      long l3 = ((com.truecaller.common.g.a)localObject3).a((String)localObject4, l1);
      long l4 = System.currentTimeMillis();
      l3 += l2;
      boolean bool2 = l3 < l4;
      if (bool2)
      {
        bool3 = l2 < l4;
        if (bool3)
        {
          localObject1 = new com/truecaller/network/a/f;
          localObject2 = "Token is valid by request TTL, but server returned UNAUTHORIZED to ";
          paramString = String.valueOf(paramString);
          paramString = ((String)localObject2).concat(paramString);
          ((f)localObject1).<init>(paramString);
          localObject1 = (Throwable)localObject1;
          throw ((Throwable)localObject1);
        }
      }
      localObject1 = com.truecaller.common.network.account.a.a;
      localObject1 = new com/truecaller/common/network/account/CheckCredentialsRequestDto;
      localObject4 = "received_unauthorized";
      int j = 4;
      localObject3 = localObject1;
      Object localObject5 = paramString;
      ((CheckCredentialsRequestDto)localObject1).<init>((String)localObject4, paramString, null, j, null);
      localObject1 = com.truecaller.common.network.account.a.a((CheckCredentialsRequestDto)localObject1);
      localObject1 = ((b)localObject1).c();
      localObject2 = "AccountRestAdapter.check…D, requestUrl)).execute()";
      k.a(localObject1, (String)localObject2);
      localObject2 = ((e.r)localObject1).e();
      localObject2 = (CheckCredentialsResponseDto)localObject2;
      boolean bool3 = ((e.r)localObject1).d();
      if (bool3)
      {
        if (localObject2 != null)
        {
          localObject1 = b;
          localObject3 = "checkCredentialsLastTime";
          long l5 = System.currentTimeMillis();
          ((com.truecaller.common.g.a)localObject1).b((String)localObject3, l5);
          localObject1 = b;
          localObject3 = "checkCredentialsTtl";
          localObject4 = TimeUnit.SECONDS;
          l4 = ((CheckCredentialsResponseDto)localObject2).getNextCallDuration();
          l5 = ((TimeUnit)localObject4).toMillis(l4);
          ((com.truecaller.common.g.a)localObject1).b((String)localObject3, l5);
          localObject1 = ((CheckCredentialsResponseDto)localObject2).getInstallationId();
          if (localObject1 != null)
          {
            localObject3 = a;
            localObject4 = TimeUnit.SECONDS;
            localObject5 = ((CheckCredentialsResponseDto)localObject2).getTtl();
            if (localObject5 != null) {
              l1 = ((Long)localObject5).longValue();
            }
            l1 = ((TimeUnit)localObject4).toMillis(l1);
            ((com.truecaller.common.account.r)localObject3).a((String)localObject1, l1);
          }
          localObject1 = c;
          localObject2 = ((CheckCredentialsResponseDto)localObject2).getDomain();
          ((c)localObject1).a((String)localObject2);
          localObject1 = new com/truecaller/network/a/f;
          localObject2 = "Token is valid by request, but server returned UNAUTHORIZED to ";
          paramString = String.valueOf(paramString);
          paramString = ((String)localObject2).concat(paramString);
          ((f)localObject1).<init>(paramString);
          localObject1 = (Throwable)localObject1;
          throw ((Throwable)localObject1);
        }
        return;
      }
      int k = ((e.r)localObject1).b();
      int i = 401;
      if (k == i)
      {
        paramString = a;
        paramString = paramString.e();
        if (paramString != null)
        {
          localObject1 = paramString;
          localObject1 = (CharSequence)paramString;
          i = ((CharSequence)localObject1).length();
          boolean bool4 = true;
          if (i > 0)
          {
            i = 1;
          }
          else
          {
            i = 0;
            localObject1 = null;
          }
          if (i != 0) {
            try
            {
              localObject1 = TrueApp.y();
              String str = "CheckCredentials";
              ((TrueApp)localObject1).a(paramString, bool4, str);
              return;
            }
            catch (SecurityException paramString)
            {
              paramString = (Throwable)paramString;
              AssertionUtil.reportThrowableButNeverCrash(paramString);
            }
          }
        }
      }
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */