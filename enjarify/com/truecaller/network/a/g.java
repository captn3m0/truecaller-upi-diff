package com.truecaller.network.a;

import c.n.m;
import com.truecaller.common.h.af;
import com.truecaller.common.network.e;
import com.truecaller.common.network.f;
import dagger.a;
import okhttp3.ab;
import okhttp3.ad;
import okhttp3.u;
import okhttp3.v;
import okhttp3.v.a;

public final class g
  implements v
{
  private final boolean a;
  private final a b;
  private final a c;
  private final af d;
  
  public g(boolean paramBoolean, a parama1, a parama2, af paramaf)
  {
    a = paramBoolean;
    b = parama1;
    c = parama2;
    d = paramaf;
  }
  
  public final ad intercept(v.a parama)
  {
    c.g.b.k.b(parama, "chain");
    Object localObject1 = parama.a();
    c.g.b.k.a(localObject1, "chain.request()");
    Object localObject2 = d;
    Object localObject3 = f.a((ab)localObject1);
    boolean bool1 = ((af)localObject2).a((e)localObject3);
    localObject3 = parama.a((ab)localObject1);
    String str = "chain.proceed(request)";
    c.g.b.k.a(localObject3, str);
    int i = ((ad)localObject3).b();
    int j = 401;
    if (i != j) {
      return (ad)localObject3;
    }
    boolean bool2 = a;
    if ((bool2) && (!bool1))
    {
      parama = (d)b.get();
      localObject1 = ((ab)localObject1).a().toString();
      localObject2 = "request.url().toString()";
      c.g.b.k.a(localObject1, (String)localObject2);
      parama.a((String)localObject1);
    }
    else if (bool1)
    {
      localObject2 = (CharSequence)((com.truecaller.common.account.k)c.get()).b();
      if (localObject2 != null)
      {
        bool1 = m.a((CharSequence)localObject2);
        if (!bool1)
        {
          bool1 = false;
          localObject2 = null;
          break label190;
        }
      }
      bool1 = true;
      label190:
      if (!bool1)
      {
        parama = parama.a((ab)localObject1);
        c.g.b.k.a(parama, "chain.proceed(request)");
        return parama;
      }
    }
    return (ad)localObject3;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */