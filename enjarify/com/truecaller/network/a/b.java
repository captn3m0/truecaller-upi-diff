package com.truecaller.network.a;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;

public final class b
  implements a
{
  private final v a;
  
  public b(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return a.class.equals(paramClass);
  }
  
  public final w a(boolean paramBoolean)
  {
    v localv = a;
    b.b localb = new com/truecaller/network/a/b$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramBoolean, (byte)0);
    return w.a(localv, localb);
  }
  
  public final w b(boolean paramBoolean)
  {
    v localv = a;
    b.a locala = new com/truecaller/network/a/b$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramBoolean, (byte)0);
    return w.a(localv, locala);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */