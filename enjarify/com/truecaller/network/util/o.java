package com.truecaller.network.util;

import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d
{
  private final k a;
  private final Provider b;
  
  private o(k paramk, Provider paramProvider)
  {
    a = paramk;
    b = paramProvider;
  }
  
  public static o a(k paramk, Provider paramProvider)
  {
    o localo = new com/truecaller/network/util/o;
    localo.<init>(paramk, paramProvider);
    return localo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */