package com.truecaller.network.util;

import dagger.a.d;
import javax.inject.Provider;

public final class v
  implements d
{
  private final k a;
  private final Provider b;
  private final Provider c;
  
  private v(k paramk, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramk;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static v a(k paramk, Provider paramProvider1, Provider paramProvider2)
  {
    v localv = new com/truecaller/network/util/v;
    localv.<init>(paramk, paramProvider1, paramProvider2);
    return localv;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */