package com.truecaller.network.util;

import android.os.AsyncTask;
import com.truecaller.old.a.b;
import com.truecaller.old.a.c;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;

public abstract class x
  extends AsyncTask
{
  private final WeakReference a;
  private final boolean b;
  private final boolean c;
  
  public x(c paramc, boolean paramBoolean1, boolean paramBoolean2)
  {
    if (paramc == null)
    {
      paramc = null;
    }
    else
    {
      WeakReference localWeakReference = new java/lang/ref/WeakReference;
      localWeakReference.<init>(paramc);
      paramc = localWeakReference;
    }
    a = paramc;
    b = paramBoolean1;
    c = paramBoolean2;
  }
  
  private c a()
  {
    WeakReference localWeakReference = a;
    if (localWeakReference == null) {
      return null;
    }
    return (c)localWeakReference.get();
  }
  
  private void a(x.b paramb)
  {
    if (paramb != null)
    {
      paramb = b;
      a(paramb);
      return;
    }
    a(null);
  }
  
  private void b(Exception paramException)
  {
    Object localObject = a();
    if (localObject != null)
    {
      bool = c;
      if (bool) {
        ((c)localObject).ac_();
      }
    }
    int i = 0;
    localObject = null;
    boolean bool = paramException instanceof x.a;
    if (bool)
    {
      localObject = paramException;
      localObject = (x.a)paramException;
      i = a;
    }
    a((Exception)paramException, i);
  }
  
  private x.b c(Object... paramVarArgs)
  {
    x.b localb;
    try
    {
      localb = new com/truecaller/network/util/x$b;
      paramVarArgs = a(paramVarArgs);
      localb.<init>(paramVarArgs);
      return localb;
    }
    catch (Exception paramVarArgs)
    {
      localb = new com/truecaller/network/util/x$b;
      localb.<init>(paramVarArgs);
    }
    return localb;
  }
  
  protected abstract Object a(Object[] paramArrayOfObject);
  
  protected void a(Exception paramException) {}
  
  protected void a(Exception paramException, int paramInt) {}
  
  protected abstract void a(Object paramObject);
  
  public final void b(Object... paramVarArgs)
  {
    Executor localExecutor = b.a;
    executeOnExecutor(localExecutor, paramVarArgs);
  }
  
  protected void onPreExecute()
  {
    c localc = a();
    if (localc != null)
    {
      boolean bool = localc.isFinishing();
      if (!bool)
      {
        bool = b;
        localc.d_(bool);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */