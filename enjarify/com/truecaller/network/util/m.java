package com.truecaller.network.util;

import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d
{
  private final k a;
  private final Provider b;
  private final Provider c;
  
  private m(k paramk, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramk;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static m a(k paramk, Provider paramProvider1, Provider paramProvider2)
  {
    m localm = new com/truecaller/network/util/m;
    localm.<init>(paramk, paramProvider1, paramProvider2);
    return localm;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */