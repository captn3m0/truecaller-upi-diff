package com.truecaller.network.util.calling_cache;

import android.arch.persistence.room.a.a;
import android.arch.persistence.room.e;
import android.arch.persistence.room.f.a;
import android.content.Context;
import c.g.b.k;

public final class CallingCacheDatabase$a
{
  public final CallingCacheDatabase a(Context paramContext)
  {
    Object localObject1 = "context";
    try
    {
      k.b(paramContext, (String)localObject1);
      localObject1 = CallingCacheDatabase.i();
      if (localObject1 == null)
      {
        paramContext = paramContext.getApplicationContext();
        localObject1 = CallingCacheDatabase.class;
        String str = "calling-cache.db";
        paramContext = e.a(paramContext, (Class)localObject1, str);
        int i = 1;
        localObject1 = new a[i];
        str = null;
        Object localObject2 = CallingCacheDatabase.j();
        localObject2 = (a)localObject2;
        localObject1[0] = localObject2;
        paramContext = paramContext.a((a[])localObject1);
        paramContext = paramContext.b();
        paramContext = (CallingCacheDatabase)paramContext;
        CallingCacheDatabase.a(paramContext);
      }
      paramContext = CallingCacheDatabase.i();
      return paramContext;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.calling_cache.CallingCacheDatabase.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */