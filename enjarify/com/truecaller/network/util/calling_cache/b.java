package com.truecaller.network.util.calling_cache;

import android.arch.persistence.room.c;
import android.arch.persistence.room.f;
import android.arch.persistence.room.i;
import android.database.Cursor;

public final class b
  implements a
{
  private final f a;
  private final c b;
  
  public b(f paramf)
  {
    a = paramf;
    b.1 local1 = new com/truecaller/network/util/calling_cache/b$1;
    local1.<init>(this, paramf);
    b = local1;
  }
  
  public final CallCacheEntry a(String paramString1, String paramString2)
  {
    Object localObject1 = paramString1;
    Object localObject3 = paramString2;
    int i = 2;
    i locali = i.a("SELECT * FROM call_cache WHERE number = ? AND state = ? LIMIT 1", i);
    int j = 1;
    if (paramString1 == null)
    {
      localObject1 = e;
      localObject1[j] = j;
    }
    else
    {
      locali.a(j, paramString1);
    }
    if (localObject3 == null)
    {
      localObject1 = e;
      localObject1[i] = j;
      localObject3 = this;
    }
    else
    {
      locali.a(i, (String)localObject3);
      localObject3 = this;
    }
    Cursor localCursor = a.a(locali);
    localObject1 = "number";
    try
    {
      int k = localCursor.getColumnIndexOrThrow((String)localObject1);
      String str1 = "timestamp";
      j = localCursor.getColumnIndexOrThrow(str1);
      String str2 = "state";
      int m = localCursor.getColumnIndexOrThrow(str2);
      String str3 = "maxAgeSeconds";
      int n = localCursor.getColumnIndexOrThrow(str3);
      String str4 = "_id";
      int i1 = localCursor.getColumnIndexOrThrow(str4);
      boolean bool2 = localCursor.moveToFirst();
      Long localLong1 = null;
      boolean bool1;
      if (bool2)
      {
        String str5 = localCursor.getString(k);
        long l1 = localCursor.getLong(j);
        String str6 = localCursor.getString(m);
        long l2 = localCursor.getLong(n);
        bool1 = localCursor.isNull(i1);
        Long localLong2;
        if (bool1)
        {
          localLong2 = null;
        }
        else
        {
          long l3 = localCursor.getLong(i1);
          localLong1 = Long.valueOf(l3);
          localLong2 = localLong1;
        }
        localObject1 = new com/truecaller/network/util/calling_cache/CallCacheEntry;
        ((CallCacheEntry)localObject1).<init>(str5, l1, str6, l2, localLong2);
      }
      else
      {
        bool1 = false;
        localObject1 = null;
      }
      return (CallCacheEntry)localObject1;
    }
    finally
    {
      localCursor.close();
      locali.b();
    }
  }
  
  public final void a(CallCacheEntry paramCallCacheEntry)
  {
    Object localObject = a;
    ((f)localObject).d();
    try
    {
      localObject = b;
      ((c)localObject).a(paramCallCacheEntry);
      paramCallCacheEntry = a;
      paramCallCacheEntry.f();
      return;
    }
    finally
    {
      a.e();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.calling_cache.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */