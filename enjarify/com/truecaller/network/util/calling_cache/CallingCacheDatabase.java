package com.truecaller.network.util.calling_cache;

import android.arch.persistence.room.f;

public abstract class CallingCacheDatabase
  extends f
{
  public static final CallingCacheDatabase.a g;
  private static CallingCacheDatabase h;
  private static final CallingCacheDatabase.b i;
  
  static
  {
    Object localObject = new com/truecaller/network/util/calling_cache/CallingCacheDatabase$a;
    ((CallingCacheDatabase.a)localObject).<init>((byte)0);
    g = (CallingCacheDatabase.a)localObject;
    localObject = new com/truecaller/network/util/calling_cache/CallingCacheDatabase$b;
    ((CallingCacheDatabase.b)localObject).<init>();
    i = (CallingCacheDatabase.b)localObject;
  }
  
  public abstract a h();
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.calling_cache.CallingCacheDatabase
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */