package com.truecaller.network.util.calling_cache;

import c.g.b.k;
import java.util.concurrent.TimeUnit;
import okhttp3.ad;

public final class d
  implements c
{
  private final a a;
  private final com.truecaller.utils.a b;
  
  public d(a parama, com.truecaller.utils.a parama1)
  {
    a = parama;
    b = parama1;
  }
  
  private static String a(com.truecaller.data.entity.Number paramNumber)
  {
    String str = paramNumber.a();
    if (str == null) {
      str = paramNumber.d();
    }
    if (str == null) {
      str = "";
    }
    return str;
  }
  
  public final void a(com.truecaller.data.entity.Number paramNumber, String paramString, ad paramad)
  {
    k.b(paramNumber, "number");
    k.b(paramString, "callState");
    Object localObject = "response";
    k.b(paramad, (String)localObject);
    paramad = paramad.f();
    int i;
    if (paramad != null)
    {
      i = paramad.a();
      long l1 = i;
      paramad = Long.valueOf(l1);
    }
    else
    {
      i = 0;
      paramad = null;
    }
    if (paramad != null)
    {
      long l2 = ((Number)paramad).longValue();
      paramad = a;
      CallCacheEntry localCallCacheEntry = new com/truecaller/network/util/calling_cache/CallCacheEntry;
      String str = a(paramNumber);
      long l3 = b.a();
      localObject = localCallCacheEntry;
      localCallCacheEntry.<init>(str, l3, paramString, l2, null, 16, null);
      paramad.a(localCallCacheEntry);
      return;
    }
  }
  
  public final boolean a(com.truecaller.data.entity.Number paramNumber, String paramString)
  {
    k.b(paramNumber, "number");
    k.b(paramString, "callState");
    a locala = a;
    paramNumber = a(paramNumber);
    paramNumber = locala.a(paramNumber, paramString);
    paramString = null;
    if (paramNumber != null)
    {
      long l1 = paramNumber.getTimestamp();
      TimeUnit localTimeUnit = TimeUnit.SECONDS;
      long l2 = paramNumber.getMaxAgeSeconds();
      long l3 = localTimeUnit.toMillis(l2);
      l1 += l3;
      paramNumber = b;
      l3 = paramNumber.a();
      boolean bool = l1 < l3;
      return bool;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.calling_cache.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */