package com.truecaller.network.util.calling_cache;

import android.arch.persistence.room.b.b.a;
import android.arch.persistence.room.b.b.d;
import android.arch.persistence.room.f.b;
import android.arch.persistence.room.h.a;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class CallingCacheDatabase_Impl$1
  extends h.a
{
  CallingCacheDatabase_Impl$1(CallingCacheDatabase_Impl paramCallingCacheDatabase_Impl)
  {
    super(2);
  }
  
  public final void a(android.arch.persistence.db.b paramb)
  {
    paramb.c("DROP TABLE IF EXISTS `call_cache`");
  }
  
  public final void b(android.arch.persistence.db.b paramb)
  {
    paramb.c("CREATE TABLE IF NOT EXISTS `call_cache` (`number` TEXT NOT NULL, `timestamp` INTEGER NOT NULL, `state` TEXT NOT NULL, `maxAgeSeconds` INTEGER NOT NULL, `_id` INTEGER PRIMARY KEY AUTOINCREMENT)");
    paramb.c("CREATE UNIQUE INDEX `index_call_cache_number_state` ON `call_cache` (`number`, `state`)");
    paramb.c("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    paramb.c("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"af35447d6c059b339496371a9145b79d\")");
  }
  
  public final void c(android.arch.persistence.db.b paramb)
  {
    CallingCacheDatabase_Impl.a(b, paramb);
    CallingCacheDatabase_Impl.b(b, paramb);
    List localList1 = CallingCacheDatabase_Impl.d(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = CallingCacheDatabase_Impl.e(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)CallingCacheDatabase_Impl.f(b).get(i);
        localb.b(paramb);
        i += 1;
      }
    }
  }
  
  public final void d(android.arch.persistence.db.b paramb)
  {
    List localList1 = CallingCacheDatabase_Impl.a(b);
    if (localList1 != null)
    {
      int i = 0;
      localList1 = null;
      List localList2 = CallingCacheDatabase_Impl.b(b);
      int j = localList2.size();
      while (i < j)
      {
        f.b localb = (f.b)CallingCacheDatabase_Impl.c(b).get(i);
        localb.a(paramb);
        i += 1;
      }
    }
  }
  
  public final void e(android.arch.persistence.db.b paramb)
  {
    Object localObject1 = new java/util/HashMap;
    int i = 5;
    ((HashMap)localObject1).<init>(i);
    Object localObject2 = new android/arch/persistence/room/b/b$a;
    int j = 1;
    ((b.a)localObject2).<init>("number", "TEXT", j, 0);
    ((HashMap)localObject1).put("number", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("timestamp", "INTEGER", j, 0);
    ((HashMap)localObject1).put("timestamp", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("state", "TEXT", j, 0);
    ((HashMap)localObject1).put("state", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("maxAgeSeconds", "INTEGER", j, 0);
    ((HashMap)localObject1).put("maxAgeSeconds", localObject2);
    localObject2 = new android/arch/persistence/room/b/b$a;
    ((b.a)localObject2).<init>("_id", "INTEGER", false, j);
    ((HashMap)localObject1).put("_id", localObject2);
    Object localObject3 = new java/util/HashSet;
    ((HashSet)localObject3).<init>(0);
    localObject2 = new java/util/HashSet;
    ((HashSet)localObject2).<init>(j);
    Object localObject4 = new android/arch/persistence/room/b/b$d;
    String str1 = "state";
    List localList = Arrays.asList(new String[] { "number", str1 });
    ((b.d)localObject4).<init>("index_call_cache_number_state", j, localList);
    ((HashSet)localObject2).add(localObject4);
    localObject4 = new android/arch/persistence/room/b/b;
    String str2 = "call_cache";
    ((android.arch.persistence.room.b.b)localObject4).<init>(str2, (Map)localObject1, (Set)localObject3, (Set)localObject2);
    localObject1 = "call_cache";
    paramb = android.arch.persistence.room.b.b.a(paramb, (String)localObject1);
    boolean bool = ((android.arch.persistence.room.b.b)localObject4).equals(paramb);
    if (bool) {
      return;
    }
    localObject1 = new java/lang/IllegalStateException;
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("Migration didn't properly handle call_cache(com.truecaller.network.util.calling_cache.CallCacheEntry).\n Expected:\n");
    ((StringBuilder)localObject3).append(localObject4);
    ((StringBuilder)localObject3).append("\n Found:\n");
    ((StringBuilder)localObject3).append(paramb);
    paramb = ((StringBuilder)localObject3).toString();
    ((IllegalStateException)localObject1).<init>(paramb);
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.calling_cache.CallingCacheDatabase_Impl.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */