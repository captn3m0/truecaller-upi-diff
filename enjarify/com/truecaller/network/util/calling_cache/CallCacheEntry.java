package com.truecaller.network.util.calling_cache;

import c.g.b.k;

public final class CallCacheEntry
{
  private Long id;
  private final long maxAgeSeconds;
  private final String number;
  private final String state;
  private final long timestamp;
  
  public CallCacheEntry(String paramString1, long paramLong1, String paramString2, long paramLong2, Long paramLong)
  {
    number = paramString1;
    timestamp = paramLong1;
    state = paramString2;
    maxAgeSeconds = paramLong2;
    id = paramLong;
  }
  
  public final String component1()
  {
    return number;
  }
  
  public final long component2()
  {
    return timestamp;
  }
  
  public final String component3()
  {
    return state;
  }
  
  public final long component4()
  {
    return maxAgeSeconds;
  }
  
  public final Long component5()
  {
    return id;
  }
  
  public final CallCacheEntry copy(String paramString1, long paramLong1, String paramString2, long paramLong2, Long paramLong)
  {
    k.b(paramString1, "number");
    k.b(paramString2, "state");
    CallCacheEntry localCallCacheEntry = new com/truecaller/network/util/calling_cache/CallCacheEntry;
    localCallCacheEntry.<init>(paramString1, paramLong1, paramString2, paramLong2, paramLong);
    return localCallCacheEntry;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof CallCacheEntry;
      if (bool2)
      {
        paramObject = (CallCacheEntry)paramObject;
        Object localObject = number;
        String str = number;
        bool2 = k.a(localObject, str);
        if (bool2)
        {
          long l1 = timestamp;
          long l2 = timestamp;
          bool2 = l1 < l2;
          if (!bool2)
          {
            bool2 = true;
          }
          else
          {
            bool2 = false;
            localObject = null;
          }
          if (bool2)
          {
            localObject = state;
            str = state;
            bool2 = k.a(localObject, str);
            if (bool2)
            {
              l1 = maxAgeSeconds;
              l2 = maxAgeSeconds;
              bool2 = l1 < l2;
              if (!bool2)
              {
                bool2 = true;
              }
              else
              {
                bool2 = false;
                localObject = null;
              }
              if (bool2)
              {
                localObject = id;
                paramObject = id;
                boolean bool3 = k.a(localObject, paramObject);
                if (bool3) {
                  return bool1;
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final Long getId()
  {
    return id;
  }
  
  public final long getMaxAgeSeconds()
  {
    return maxAgeSeconds;
  }
  
  public final String getNumber()
  {
    return number;
  }
  
  public final String getState()
  {
    return state;
  }
  
  public final long getTimestamp()
  {
    return timestamp;
  }
  
  public final int hashCode()
  {
    String str = number;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    long l1 = timestamp;
    int k = 32;
    long l2 = l1 >>> k;
    l1 ^= l2;
    int m = (int)l1;
    int j = (j + m) * 31;
    Object localObject = state;
    int n;
    if (localObject != null)
    {
      n = localObject.hashCode();
    }
    else
    {
      n = 0;
      localObject = null;
    }
    j = (j + n) * 31;
    l1 = maxAgeSeconds;
    long l3 = l1 >>> k;
    l1 ^= l3;
    m = (int)l1;
    j = (j + m) * 31;
    localObject = id;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final void setId(Long paramLong)
  {
    id = paramLong;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CallCacheEntry(number=");
    Object localObject = number;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", timestamp=");
    long l = timestamp;
    localStringBuilder.append(l);
    localStringBuilder.append(", state=");
    localObject = state;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", maxAgeSeconds=");
    l = maxAgeSeconds;
    localStringBuilder.append(l);
    localStringBuilder.append(", id=");
    localObject = id;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.calling_cache.CallCacheEntry
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */