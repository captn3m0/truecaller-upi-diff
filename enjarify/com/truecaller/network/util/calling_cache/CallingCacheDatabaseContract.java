package com.truecaller.network.util.calling_cache;

public final class CallingCacheDatabaseContract
{
  public static final CallingCacheDatabaseContract INSTANCE;
  
  static
  {
    CallingCacheDatabaseContract localCallingCacheDatabaseContract = new com/truecaller/network/util/calling_cache/CallingCacheDatabaseContract;
    localCallingCacheDatabaseContract.<init>();
    INSTANCE = localCallingCacheDatabaseContract;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.calling_cache.CallingCacheDatabaseContract
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */