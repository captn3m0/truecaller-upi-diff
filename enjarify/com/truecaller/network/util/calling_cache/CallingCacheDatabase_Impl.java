package com.truecaller.network.util.calling_cache;

import android.arch.persistence.db.c;
import android.arch.persistence.db.c.a;
import android.arch.persistence.db.c.b;
import android.arch.persistence.db.c.b.a;
import android.arch.persistence.db.c.c;
import android.arch.persistence.room.d;
import android.arch.persistence.room.h;
import android.arch.persistence.room.h.a;

public class CallingCacheDatabase_Impl
  extends CallingCacheDatabase
{
  private volatile a h;
  
  public final d a()
  {
    d locald = new android/arch/persistence/room/d;
    String[] arrayOfString = { "call_cache" };
    locald.<init>(this, arrayOfString);
    return locald;
  }
  
  public final c b(android.arch.persistence.room.a parama)
  {
    Object localObject1 = new android/arch/persistence/room/h;
    Object localObject2 = new com/truecaller/network/util/calling_cache/CallingCacheDatabase_Impl$1;
    ((CallingCacheDatabase_Impl.1)localObject2).<init>(this);
    ((h)localObject1).<init>(parama, (h.a)localObject2, "af35447d6c059b339496371a9145b79d", "8a64d2a33b0c5da3ba2903f74bce733f");
    localObject2 = c.b.a(b);
    String str = c;
    b = str;
    c = ((c.a)localObject1);
    localObject1 = ((c.b.a)localObject2).a();
    return a.a((c.b)localObject1);
  }
  
  public final a h()
  {
    Object localObject1 = h;
    if (localObject1 != null) {
      return h;
    }
    try
    {
      localObject1 = h;
      if (localObject1 == null)
      {
        localObject1 = new com/truecaller/network/util/calling_cache/b;
        ((b)localObject1).<init>(this);
        h = ((a)localObject1);
      }
      localObject1 = h;
      return (a)localObject1;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.calling_cache.CallingCacheDatabase_Impl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */