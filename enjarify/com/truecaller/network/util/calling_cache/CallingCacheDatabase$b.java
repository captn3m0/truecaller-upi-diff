package com.truecaller.network.util.calling_cache;

import android.arch.persistence.db.b;
import android.arch.persistence.room.a.a;
import c.g.b.k;

public final class CallingCacheDatabase$b
  extends a
{
  CallingCacheDatabase$b()
  {
    super(1, 2);
  }
  
  public final void a(b paramb)
  {
    k.b(paramb, "database");
    paramb.c("\n                            CREATE TABLE IF NOT EXISTS `call_cache_new` (\n                            `_id` INTEGER PRIMARY KEY AUTOINCREMENT,\n                            `number` TEXT NOT NULL,\n                            `timestamp` INTEGER NOT NULL,\n                            `state` TEXT NOT NULL,\n                            `maxAgeSeconds` INTEGER NOT NULL)\n                            ");
    paramb.c("\n                            INSERT INTO call_cache_new (\n                            number, timestamp, maxAgeSeconds, state)\n                            SELECT number, timestamp, maxAgeSeconds, 'initiated'\n                            FROM call_cache\n                            ");
    paramb.c("\n                            INSERT INTO call_cache_new (\n                            number, timestamp, maxAgeSeconds, state)\n                            SELECT number, timestamp, maxAgeSeconds, 'ended'\n                            FROM call_cache\n                            ");
    paramb.c("DROP TABLE call_cache");
    paramb.c("ALTER TABLE call_cache_new RENAME TO call_cache");
    paramb.c("CREATE UNIQUE INDEX `index_call_cache_number_state` ON `call_cache` (`number`, `state`)");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.calling_cache.CallingCacheDatabase.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */