package com.truecaller.network.util;

import dagger.a.d;
import javax.inject.Provider;

public final class s
  implements d
{
  private final k a;
  private final Provider b;
  private final Provider c;
  
  private s(k paramk, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramk;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static s a(k paramk, Provider paramProvider1, Provider paramProvider2)
  {
    s locals = new com/truecaller/network/util/s;
    locals.<init>(paramk, paramProvider1, paramProvider2);
    return locals;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */