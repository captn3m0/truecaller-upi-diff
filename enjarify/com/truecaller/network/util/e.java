package com.truecaller.network.util;

import android.support.v4.f.j;
import com.truecaller.androidactors.w;
import com.truecaller.data.entity.Number;
import e.b;
import e.r;
import java.io.IOException;
import okhttp3.ad;

public final class e
  implements c
{
  private final com.truecaller.network.util.calling_cache.c a;
  private final e.a b;
  
  public e(com.truecaller.network.util.calling_cache.c paramc, e.a parama)
  {
    a = paramc;
    b = parama;
  }
  
  public final w a(String paramString, Number paramNumber)
  {
    c.g.b.k.b(paramString, "callState");
    c.g.b.k.b(paramNumber, "number");
    Object localObject1 = paramNumber.o();
    if (localObject1 != null)
    {
      Object localObject2 = localObject1;
      localObject2 = (CharSequence)localObject1;
      boolean bool = org.c.a.a.a.k.b((CharSequence)localObject2);
      if (!bool)
      {
        localObject2 = a;
        bool = ((com.truecaller.network.util.calling_cache.c)localObject2).a(paramNumber, paramString);
        if (bool)
        {
          paramString = w.b(j.a(Boolean.FALSE, paramString));
          c.g.b.k.a(paramString, "Promise.wrap(Pair.create(false, callState))");
          return paramString;
        }
        localObject2 = b;
        String str = paramNumber.l();
        localObject1 = ((e.a)localObject2).a(paramString, (String)localObject1, str);
        try
        {
          localObject1 = ((b)localObject1).c();
          localObject2 = a;
          localObject1 = ((r)localObject1).a();
          str = "response.raw()";
          c.g.b.k.a(localObject1, str);
          ((com.truecaller.network.util.calling_cache.c)localObject2).a(paramNumber, paramString, (ad)localObject1);
          paramString = w.b(j.a(Boolean.TRUE, paramString));
          c.g.b.k.a(paramString, "Promise.wrap(Pair.create(true, callState))");
          return paramString;
        }
        catch (IOException localIOException)
        {
          paramString = w.b(j.a(Boolean.FALSE, paramString));
          c.g.b.k.a(paramString, "Promise.wrap(Pair.create(false, callState))");
          return paramString;
        }
      }
    }
    paramString = w.b(j.a(Boolean.FALSE, paramString));
    c.g.b.k.a(paramString, "Promise.wrap(Pair.create(false, callState))");
    return paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */