package com.truecaller.network.util;

import dagger.a.d;
import javax.inject.Provider;

public final class r
  implements d
{
  private final k a;
  private final Provider b;
  
  private r(k paramk, Provider paramProvider)
  {
    a = paramk;
    b = paramProvider;
  }
  
  public static r a(k paramk, Provider paramProvider)
  {
    r localr = new com/truecaller/network/util/r;
    localr.<init>(paramk, paramProvider);
    return localr;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */