package com.truecaller.network.util;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.data.entity.Number;

public final class d
  implements c
{
  private final v a;
  
  public d(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return c.class.equals(paramClass);
  }
  
  public final w a(String paramString, Number paramNumber)
  {
    v localv = a;
    d.a locala = new com/truecaller/network/util/d$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramString, paramNumber, (byte)0);
    return w.a(localv, locala);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */