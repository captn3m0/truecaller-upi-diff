package com.truecaller.network.util;

import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d
{
  private final k a;
  private final Provider b;
  private final Provider c;
  
  private q(k paramk, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramk;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static q a(k paramk, Provider paramProvider1, Provider paramProvider2)
  {
    q localq = new com/truecaller/network/util/q;
    localq.<init>(paramk, paramProvider1, paramProvider2);
    return localq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */