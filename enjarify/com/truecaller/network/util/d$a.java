package com.truecaller.network.util;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.Number;

final class d$a
  extends u
{
  private final String b;
  private final Number c;
  
  private d$a(e parame, String paramString, Number paramNumber)
  {
    super(parame);
    b = paramString;
    c = paramNumber;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".sendCallerIdNotification(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(c, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */