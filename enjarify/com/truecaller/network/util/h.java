package com.truecaller.network.util;

import android.os.Build.VERSION;
import com.truecaller.androidactors.w;
import com.truecaller.common.b.a;
import com.truecaller.common.h.k;
import com.truecaller.common.network.feedback.Feedback;
import com.truecaller.common.network.feedback.a.a;
import com.truecaller.common.network.util.KnownEndpoints;
import e.b;
import e.r;
import java.io.IOException;
import okhttp3.ad;

final class h
  implements f
{
  public final w a(CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, CharSequence paramCharSequence4, String paramString)
  {
    a locala = a.F();
    String str = "gold";
    boolean bool1 = str.equals(paramString);
    if (bool1)
    {
      paramString = "(GOLD_USER)";
    }
    else
    {
      str = "regular";
      bool2 = str.equals(paramString);
      if (bool2) {
        paramString = "(PREMIUM_USER)";
      } else {
        paramString = "";
      }
    }
    str = "FEEDBACK FORM ANDROID %s\r\nName: %s\r\nSubject: %s\r\nDevice Name: %s\r\nAndroid OS Version: %s\r\n%s Version: %s\r\nFeedback:\r\n\r\n%s";
    int i = 8;
    Object[] arrayOfObject = new Object[i];
    arrayOfObject[0] = paramString;
    boolean bool2 = true;
    arrayOfObject[bool2] = paramCharSequence1;
    arrayOfObject[2] = paramCharSequence3;
    paramCharSequence3 = k.c();
    arrayOfObject[3] = paramCharSequence3;
    paramCharSequence3 = Build.VERSION.RELEASE;
    arrayOfObject[4] = paramCharSequence3;
    paramCharSequence3 = locala.k();
    arrayOfObject[5] = paramCharSequence3;
    paramCharSequence3 = locala.l();
    arrayOfObject[6] = paramCharSequence3;
    int j = 7;
    arrayOfObject[j] = paramCharSequence4;
    paramCharSequence1 = String.format(str, arrayOfObject);
    paramCharSequence3 = new com/truecaller/common/network/feedback/Feedback;
    paramCharSequence2 = paramCharSequence2.toString();
    paramCharSequence3.<init>(paramCharSequence2, paramCharSequence1);
    paramCharSequence1 = KnownEndpoints.FEEDBACK;
    paramCharSequence2 = a.a.class;
    paramCharSequence1 = ((a.a)com.truecaller.common.network.util.h.a(paramCharSequence1, paramCharSequence2)).a(paramCharSequence3);
    try
    {
      paramCharSequence1 = paramCharSequence1.c();
      if (paramCharSequence1 != null)
      {
        paramCharSequence1 = a;
        j = c;
        paramCharSequence1 = Integer.valueOf(j);
        return w.b(paramCharSequence1);
      }
    }
    catch (IOException localIOException) {}
    return w.b(null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */