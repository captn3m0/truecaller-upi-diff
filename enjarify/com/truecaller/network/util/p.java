package com.truecaller.network.util;

import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d
{
  private final k a;
  private final Provider b;
  private final Provider c;
  
  private p(k paramk, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramk;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static p a(k paramk, Provider paramProvider1, Provider paramProvider2)
  {
    p localp = new com/truecaller/network/util/p;
    localp.<init>(paramk, paramProvider1, paramProvider2);
    return localp;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */