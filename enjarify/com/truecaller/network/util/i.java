package com.truecaller.network.util;

import c.g.b.k;
import c.l;
import com.truecaller.common.account.r;
import com.truecaller.common.h.af;
import com.truecaller.common.network.a.a.a;
import com.truecaller.common.network.a.a.b;
import com.truecaller.common.network.a.a.c;
import com.truecaller.common.network.a.a.d;
import com.truecaller.common.network.a.a.e;
import com.truecaller.common.network.a.a.f;
import com.truecaller.common.network.a.a.g;
import com.truecaller.common.network.util.AuthRequirement;
import com.truecaller.network.a.g;
import com.truecaller.network.a.h;
import okhttp3.v;

public final class i
  implements com.truecaller.common.network.d.c
{
  private final String a;
  private final String b;
  private final dagger.a c;
  private final dagger.a d;
  private final dagger.a e;
  private final dagger.a f;
  private final dagger.a g;
  private final dagger.a h;
  private final dagger.a i;
  private final dagger.a j;
  private final dagger.a k;
  
  public i(String paramString1, String paramString2, dagger.a parama1, dagger.a parama2, dagger.a parama3, dagger.a parama4, dagger.a parama5, dagger.a parama6, dagger.a parama7, dagger.a parama8, dagger.a parama9)
  {
    a = paramString1;
    b = paramString2;
    c = parama1;
    d = parama2;
    e = parama3;
    f = parama4;
    g = parama5;
    h = parama6;
    i = parama7;
    j = parama8;
    k = parama9;
  }
  
  public final v a(com.truecaller.common.network.a.a parama)
  {
    Object localObject1 = "attribute";
    k.b(parama, (String)localObject1);
    boolean bool1 = parama instanceof a.b;
    Object localObject3;
    Object localObject4;
    if (bool1)
    {
      localObject1 = new com/truecaller/network/a/g;
      bool2 = c;
      localObject2 = f;
      localObject3 = j;
      localObject4 = k.get();
      k.a(localObject4, "restCrossDcSupport.get()");
      localObject4 = (af)localObject4;
      ((g)localObject1).<init>(bool2, (dagger.a)localObject2, (dagger.a)localObject3, (af)localObject4);
      return (v)localObject1;
    }
    bool1 = parama instanceof a.g;
    if (bool1)
    {
      parama = new com/truecaller/network/a/i;
      localObject1 = c.get();
      k.a(localObject1, "domainResolver.get()");
      localObject1 = (com.truecaller.common.network.c)localObject1;
      localObject2 = d.get();
      k.a(localObject2, "analytics.get()");
      localObject2 = (com.truecaller.analytics.b)localObject2;
      localObject3 = e.get();
      k.a(localObject3, "accountManager.get()");
      localObject3 = (r)localObject3;
      parama.<init>((com.truecaller.common.network.c)localObject1, (com.truecaller.analytics.b)localObject2, (r)localObject3);
      return (v)parama;
    }
    bool1 = parama instanceof a.a;
    Object localObject2 = null;
    Object localObject5;
    if (bool1)
    {
      localObject1 = parama;
      localObject1 = c;
      localObject3 = AuthRequirement.NONE;
      boolean bool3 = true;
      localObject5 = null;
      if (localObject1 != localObject3)
      {
        bool1 = true;
      }
      else
      {
        bool1 = false;
        localObject1 = null;
      }
      if (!bool1)
      {
        bool2 = false;
        parama = null;
      }
      parama = (a.a)parama;
      if (parama != null)
      {
        localObject2 = new com/truecaller/common/network/a;
        parama = c;
        localObject1 = AuthRequirement.REQUIRED;
        if (parama != localObject1)
        {
          bool3 = false;
          localObject4 = null;
        }
        parama = e.get();
        k.a(parama, "accountManager.get()");
        parama = (r)parama;
        localObject1 = j;
        localObject3 = k.get();
        localObject5 = "restCrossDcSupport.get()";
        k.a(localObject3, (String)localObject5);
        localObject3 = (com.truecaller.common.h.i)localObject3;
        ((com.truecaller.common.network.a)localObject2).<init>(bool3, parama, (dagger.a)localObject1, (com.truecaller.common.h.i)localObject3);
      }
      return (v)localObject2;
    }
    bool1 = parama instanceof a.f;
    if (bool1)
    {
      parama = new com/truecaller/network/a/h;
      localObject1 = g.get();
      k.a(localObject1, "configManager.get()");
      localObject1 = (com.truecaller.androidactors.f)localObject1;
      parama.<init>((com.truecaller.androidactors.f)localObject1);
      return (v)parama;
    }
    bool1 = parama instanceof a.e;
    if (bool1)
    {
      localObject1 = new com/truecaller/network/c/a;
      localObject2 = h.get();
      k.a(localObject2, "edgeLocationsManager.get()");
      localObject4 = localObject2;
      localObject4 = (com.truecaller.common.edge.a)localObject2;
      localObject2 = d.get();
      k.a(localObject2, "analytics.get()");
      localObject5 = localObject2;
      localObject5 = (com.truecaller.analytics.b)localObject2;
      localObject2 = c.get();
      k.a(localObject2, "domainResolver.get()");
      Object localObject6 = localObject2;
      localObject6 = (com.truecaller.common.network.c)localObject2;
      localObject2 = k.get();
      k.a(localObject2, "restCrossDcSupport.get()");
      Object localObject7 = localObject2;
      localObject7 = (af)localObject2;
      boolean bool4 = c;
      localObject3 = localObject1;
      ((com.truecaller.network.c.a)localObject1).<init>((com.truecaller.common.edge.a)localObject4, (com.truecaller.analytics.b)localObject5, (com.truecaller.common.network.c)localObject6, (af)localObject7, bool4);
      return (v)localObject1;
    }
    bool1 = parama instanceof a.d;
    if (bool1)
    {
      parama = (com.truecaller.d.b)i.get();
      if (parama != null)
      {
        bool1 = parama.a();
        if (bool1)
        {
          localObject2 = new com/truecaller/d/a;
          localObject1 = k.get();
          localObject3 = "restCrossDcSupport.get()";
          k.a(localObject1, (String)localObject3);
          localObject1 = (af)localObject1;
          ((com.truecaller.d.a)localObject2).<init>(parama, (af)localObject1);
        }
      }
      return (v)localObject2;
    }
    boolean bool2 = parama instanceof a.c;
    if (bool2)
    {
      parama = new com/truecaller/common/network/d/f;
      localObject1 = a;
      localObject2 = b;
      parama.<init>((String)localObject1, (String)localObject2);
      return (v)parama;
    }
    parama = new c/l;
    parama.<init>();
    throw parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */