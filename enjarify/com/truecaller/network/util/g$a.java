package com.truecaller.network.util;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class g$a
  extends u
{
  private final CharSequence b;
  private final CharSequence c;
  private final CharSequence d;
  private final CharSequence e;
  private final String f;
  
  private g$a(e parame, CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, CharSequence paramCharSequence4, String paramString)
  {
    super(parame);
    b = paramCharSequence1;
    c = paramCharSequence2;
    d = paramCharSequence3;
    e = paramCharSequence4;
    f = paramString;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".feedback(");
    Object localObject = b;
    int i = 1;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(d, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(e, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(f, 2);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.g.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */