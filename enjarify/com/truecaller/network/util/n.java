package com.truecaller.network.util;

import dagger.a.g;
import java.util.concurrent.TimeUnit;
import okhttp3.y;
import okhttp3.y.a;

public final class n
  implements dagger.a.d
{
  private final k a;
  
  private n(k paramk)
  {
    a = paramk;
  }
  
  public static n a(k paramk)
  {
    n localn = new com/truecaller/network/util/n;
    localn.<init>(paramk);
    return localn;
  }
  
  public static y a()
  {
    y.a locala = com.truecaller.common.network.util.d.d();
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    return (y)g.a(locala.a(30, localTimeUnit).d(), "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */