package com.truecaller.network.util;

import dagger.a.d;
import javax.inject.Provider;

public final class l
  implements d
{
  private final k a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  
  private l(k paramk, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    a = paramk;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static l a(k paramk, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    l locall = new com/truecaller/network/util/l;
    locall.<init>(paramk, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
    return locall;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */