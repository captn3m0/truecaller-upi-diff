package com.truecaller.network.util;

import dagger.a.d;
import javax.inject.Provider;

public final class u
  implements d
{
  private final k a;
  private final Provider b;
  
  private u(k paramk, Provider paramProvider)
  {
    a = paramk;
    b = paramProvider;
  }
  
  public static u a(k paramk, Provider paramProvider)
  {
    u localu = new com/truecaller/network/util/u;
    localu.<init>(paramk, paramProvider);
    return localu;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.util.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */