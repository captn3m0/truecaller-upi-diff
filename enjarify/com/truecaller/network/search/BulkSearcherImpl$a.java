package com.truecaller.network.search;

import java.util.List;

final class BulkSearcherImpl$a
  implements j.b
{
  private final List b;
  
  BulkSearcherImpl$a(BulkSearcherImpl paramBulkSearcherImpl, List paramList)
  {
    b = paramList;
  }
  
  public final void a(Throwable paramThrowable)
  {
    paramThrowable = new java/lang/StringBuilder;
    paramThrowable.<init>("Bulk search for ");
    List localList = b;
    paramThrowable.append(localList);
    paramThrowable.append(" failed");
    paramThrowable = a;
    localList = b;
    paramThrowable.a(localList);
    BulkSearcherImpl.a(a, null);
  }
  
  public final void a(List paramList, String paramString1, String paramString2)
  {
    paramList = new String[1];
    paramString1 = new java/lang/StringBuilder;
    paramString1.<init>("Bulk search for ");
    paramString2 = b;
    paramString1.append(paramString2);
    paramString1.append(" successful");
    paramString1 = paramString1.toString();
    paramList[0] = paramString1;
    paramList = a;
    paramString1 = b;
    paramList.b(paramString1);
    BulkSearcherImpl.a(a, null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.search.BulkSearcherImpl.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */