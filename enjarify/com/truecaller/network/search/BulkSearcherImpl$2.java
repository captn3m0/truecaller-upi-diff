package com.truecaller.network.search;

import android.content.Context;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Handler;
import android.text.TextUtils;
import com.truecaller.common.b.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;

final class BulkSearcherImpl$2
  implements Runnable
{
  private AsyncTask b;
  
  BulkSearcherImpl$2(BulkSearcherImpl paramBulkSearcherImpl) {}
  
  public final void run()
  {
    Object localObject1 = b;
    if (localObject1 != null)
    {
      localObject1 = ((AsyncTask)localObject1).getStatus();
      localObject2 = AsyncTask.Status.FINISHED;
      if (localObject1 != localObject2)
      {
        new String[1][0] = "Previous search task not finished, delaying bulk search";
        localObject1 = BulkSearcherImpl.c(a);
        long l = BulkSearcherImpl.b(a);
        ((Handler)localObject1).postDelayed(this, l);
        return;
      }
      BulkSearcherImpl.d(a).clear();
      localObject1 = null;
      b = null;
    }
    localObject1 = new java/util/ArrayList;
    Object localObject2 = BulkSearcherImpl.e(a).keySet();
    ((ArrayList)localObject1).<init>((Collection)localObject2);
    boolean bool1 = true;
    Object localObject3 = new String[bool1];
    Object localObject4 = new java/lang/StringBuilder;
    ((StringBuilder)localObject4).<init>("Triggering bulk search for ");
    Collection localCollection = BulkSearcherImpl.e(a).values();
    ((StringBuilder)localObject4).append(localCollection);
    localObject4 = ((StringBuilder)localObject4).toString();
    localCollection = null;
    localObject3[0] = localObject4;
    localObject3 = (a)BulkSearcherImpl.f(a).getApplicationContext();
    boolean bool2 = ((a)localObject3).p();
    if (!bool2)
    {
      a.a((Collection)localObject1);
      return;
    }
    localObject3 = new com/truecaller/network/search/d;
    localObject4 = BulkSearcherImpl.f(a);
    Object localObject5 = UUID.randomUUID();
    Object localObject6 = BulkSearcherImpl.h(a);
    ((d)localObject3).<init>((Context)localObject4, (UUID)localObject5, (String)localObject6);
    localObject4 = BulkSearcherImpl.e(a).values();
    localObject3 = ((d)localObject3).a((Collection)localObject4);
    int i = BulkSearcherImpl.g(a);
    e = i;
    d locald = ((d)localObject3).a();
    c = bool1;
    d = bool1;
    localObject2 = a;
    a.addAll((Collection)localObject1);
    b.addAll((Collection)localObject1);
    c.keySet().removeAll((Collection)localObject1);
    localObject2 = a;
    localObject3 = new com/truecaller/network/search/BulkSearcherImpl$a;
    ((BulkSearcherImpl.a)localObject3).<init>((BulkSearcherImpl)localObject2, (List)localObject1);
    j.b localb = BulkSearcherImpl.a((BulkSearcherImpl)localObject2, (j.b)localObject3);
    localObject1 = new com/truecaller/network/search/k;
    localObject6 = a;
    localObject3 = b;
    String str1 = TextUtils.join(",", (Iterable)localObject3);
    String str2 = f;
    localObject5 = localObject1;
    ((k)localObject1).<init>((Context)localObject6, null, false, false, locald, str1, str2, localb, null);
    localObject2 = new Void[0];
    ((k)localObject1).b((Object[])localObject2);
    b = ((AsyncTask)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.search.BulkSearcherImpl.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */