package com.truecaller.network.search;

import android.text.TextUtils;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.common.h.am;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.Tag;
import com.truecaller.filters.FilterManager;
import com.truecaller.filters.FilterManager.ActionSource;
import com.truecaller.filters.g;
import com.truecaller.log.AssertionUtil;
import com.truecaller.tracking.events.al;
import com.truecaller.tracking.events.al.a;
import com.truecaller.tracking.events.ay;
import com.truecaller.tracking.events.ay.a;
import com.truecaller.tracking.events.bc;
import com.truecaller.tracking.events.bc.a;
import com.truecaller.tracking.events.z;
import com.truecaller.tracking.events.z.a;
import com.truecaller.util.ce;
import com.truecaller.utils.i;
import e.r;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import okhttp3.ad;
import org.apache.a.d.d;
import org.c.a.a.a.k;

final class c
  extends a
{
  private final com.truecaller.data.access.c b;
  private final boolean c;
  private final f d;
  private final FilterManager e;
  private final List f;
  private final int g;
  private final String h;
  private final UUID i;
  private final com.truecaller.analytics.b j;
  private final i k;
  private final com.truecaller.utils.a l;
  private final List m;
  
  c(e.b paramb, com.truecaller.data.access.c paramc, boolean paramBoolean, f paramf, FilterManager paramFilterManager, String paramString1, int paramInt, String paramString2, UUID paramUUID, List paramList, com.truecaller.analytics.b paramb1, i parami, com.truecaller.utils.a parama)
  {
    super(paramb);
    b = paramc;
    c = paramBoolean;
    d = paramf;
    e = paramFilterManager;
    paramb = new java/util/ArrayList;
    paramb.<init>();
    f = paramb;
    g = paramInt;
    h = paramString2;
    i = paramUUID;
    m = paramList;
    j = paramb1;
    k = parami;
    l = parama;
    f.add(paramString1);
  }
  
  c(e.b paramb, com.truecaller.data.access.c paramc, boolean paramBoolean, f paramf, FilterManager paramFilterManager, List paramList1, int paramInt, String paramString, UUID paramUUID, List paramList2, com.truecaller.analytics.b paramb1, i parami, com.truecaller.utils.a parama)
  {
    super(paramb);
    b = paramc;
    c = paramBoolean;
    d = paramf;
    e = paramFilterManager;
    f = paramList1;
    g = paramInt;
    h = paramString;
    i = paramUUID;
    m = paramList2;
    j = paramb1;
    k = parami;
    l = parama;
  }
  
  private void a(al.a parama, String paramString)
  {
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (bool1) {
      return;
    }
    paramString = e.a(paramString);
    Object localObject = j;
    FilterManager.ActionSource localActionSource = FilterManager.ActionSource.CUSTOM_BLACKLIST;
    boolean bool2 = true;
    if (localObject == localActionSource)
    {
      bool1 = true;
    }
    else
    {
      bool1 = false;
      localObject = null;
    }
    localObject = Boolean.valueOf(bool1);
    parama = parama.a((Boolean)localObject);
    localObject = j;
    localActionSource = FilterManager.ActionSource.CUSTOM_WHITELIST;
    if (localObject == localActionSource)
    {
      bool1 = true;
    }
    else
    {
      bool1 = false;
      localObject = null;
    }
    localObject = Boolean.valueOf(bool1);
    parama = parama.c((Boolean)localObject);
    paramString = j;
    localObject = FilterManager.ActionSource.TOP_SPAMMER;
    if (paramString != localObject) {
      bool2 = false;
    }
    paramString = Boolean.valueOf(bool2);
    parama.b(paramString);
  }
  
  private void a(z.a parama, e.a parama1, double paramDouble)
  {
    parama1.a("Result", "Fail");
    Object localObject1 = Double.valueOf(paramDouble);
    a = ((Double)localObject1);
    localObject1 = j;
    parama1 = parama1.a();
    ((com.truecaller.analytics.b)localObject1).a(parama1);
    parama1 = null;
    parama.b(null);
    int n = 1;
    parama.a(n);
    parama.b(false);
    ArrayList localArrayList1 = new java/util/ArrayList;
    localArrayList1.<init>();
    Iterator localIterator1 = f.iterator();
    for (;;)
    {
      boolean bool1 = localIterator1.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject2 = (String)localIterator1.next();
      boolean bool2 = k.b((CharSequence)localObject2);
      if (!bool2)
      {
        Object localObject3 = b.b((String)localObject2);
        al.a locala = al.b();
        if (localObject3 != null)
        {
          localObject4 = ((Contact)localObject3).z();
          bool3 = am.b((CharSequence)localObject4);
          if (!bool3)
          {
            bool3 = true;
            break label175;
          }
        }
        boolean bool3 = false;
        Object localObject4 = null;
        label175:
        locala = locala.b(bool3);
        if (localObject3 != null)
        {
          i1 = ((Contact)localObject3).getSource() & 0x2;
          if (i1 != 0)
          {
            i1 = 1;
            break label215;
          }
        }
        int i1 = 0;
        localObject4 = null;
        label215:
        locala = locala.a(i1);
        if (localObject3 == null)
        {
          i2 = 0;
          localObject4 = null;
        }
        else
        {
          i2 = ((Contact)localObject3).I();
        }
        localObject4 = Integer.valueOf(i2);
        locala = locala.a((Integer)localObject4);
        if (localObject3 != null)
        {
          i2 = ((Contact)localObject3).getSource() & 0x40;
          if (i2 != 0)
          {
            i2 = 1;
            break label293;
          }
        }
        int i2 = 0;
        localObject4 = null;
        label293:
        localObject4 = Boolean.valueOf(i2);
        locala = locala.e((Boolean)localObject4);
        if (localObject3 != null)
        {
          bool4 = ((Contact)localObject3).U();
          if (bool4)
          {
            bool4 = true;
            break label338;
          }
        }
        boolean bool4 = false;
        localObject4 = null;
        label338:
        localObject4 = Boolean.valueOf(bool4);
        locala = locala.d((Boolean)localObject4);
        a(locala, (String)localObject2);
        if (localObject3 != null)
        {
          localObject4 = new java/util/ArrayList;
          ((ArrayList)localObject4).<init>();
          ArrayList localArrayList2 = new java/util/ArrayList;
          localArrayList2.<init>();
          ArrayList localArrayList3 = new java/util/ArrayList;
          localArrayList3.<init>();
          Iterator localIterator2 = ((Contact)localObject3).J().iterator();
          for (;;)
          {
            boolean bool5 = localIterator2.hasNext();
            if (!bool5) {
              break;
            }
            Object localObject5 = (Tag)localIterator2.next();
            int i3 = ((Tag)localObject5).getSource();
            if (i3 == n)
            {
              localObject5 = ((Tag)localObject5).a();
              ((List)localObject4).add(localObject5);
            }
            else
            {
              localObject5 = ((Tag)localObject5).a();
              localArrayList2.add(localObject5);
            }
          }
          localObject3 = ce.a((Contact)localObject3);
          if (localObject3 != null)
          {
            long l1 = a;
            localObject3 = String.valueOf(l1);
            localArrayList3.add(localObject3);
          }
          localObject3 = bc.b();
          boolean bool6 = ((List)localObject4).isEmpty();
          if (bool6)
          {
            bool4 = false;
            localObject4 = null;
          }
          localObject3 = ((bc.a)localObject3).a((List)localObject4);
          bool4 = localArrayList2.isEmpty();
          if (bool4) {
            localArrayList2 = null;
          }
          localObject3 = ((bc.a)localObject3).b(localArrayList2);
          bool4 = localArrayList3.isEmpty();
          if (bool4) {
            localArrayList3 = null;
          }
          localObject3 = ((bc.a)localObject3).c(localArrayList3).a();
        }
        else
        {
          bool2 = false;
          localObject3 = null;
        }
        localObject4 = ay.b();
        localObject2 = ((ay.a)localObject4).a((CharSequence)localObject2).a((bc)localObject3);
        localObject3 = locala.a();
        localObject2 = ((ay.a)localObject2).a((al)localObject3).b(null).c(null).a();
        localArrayList1.add(localObject2);
      }
    }
    parama.a(localArrayList1);
    parama1 = (ae)d.a();
    parama = parama.a();
    parama1.a(parama);
  }
  
  private void a(Collection paramCollection, List paramList, Set paramSet, boolean paramBoolean)
  {
    paramCollection = paramCollection.iterator();
    for (;;)
    {
      boolean bool1 = paramCollection.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject1 = (Contact)paramCollection.next();
      Object localObject2 = ((Contact)localObject1).A().iterator();
      String str1 = null;
      String str2 = null;
      for (;;)
      {
        bool2 = ((Iterator)localObject2).hasNext();
        i3 = 1;
        if (!bool2) {
          break;
        }
        localObject3 = (Number)((Iterator)localObject2).next();
        int i4 = ((Number)localObject3).getSource();
        i3 &= i4;
        if (i3 != 0) {
          str2 = ((Number)localObject3).b();
        }
      }
      boolean bool4 = c;
      if (bool4)
      {
        bool4 = com.truecaller.data.access.c.b((Contact)localObject1);
        if (!bool4)
        {
          localObject2 = b.a((Contact)localObject1);
          if (localObject2 != null)
          {
            localObject1 = ((Contact)localObject1).G();
            ((Contact)localObject2).n((String)localObject1);
            localObject1 = localObject2;
          }
        }
      }
      localObject2 = al.b();
      Object localObject3 = ((Contact)localObject1).z();
      boolean bool2 = am.b((CharSequence)localObject3) ^ i3;
      localObject2 = ((al.a)localObject2).b(bool2);
      int n = ((Contact)localObject1).getSource() & 0x2;
      int i5 = 0;
      Object localObject4 = null;
      if (n != 0)
      {
        n = 1;
      }
      else
      {
        n = 0;
        localObject3 = null;
      }
      localObject2 = ((al.a)localObject2).a(n);
      int i1 = ((Contact)localObject1).getSource() & 0x40;
      if (i1 != 0)
      {
        i1 = 1;
      }
      else
      {
        i1 = 0;
        localObject3 = null;
      }
      localObject3 = Boolean.valueOf(i1);
      localObject2 = ((al.a)localObject2).e((Boolean)localObject3);
      int i2 = ((Contact)localObject1).I();
      localObject3 = Integer.valueOf(Math.max(0, i2));
      localObject2 = ((al.a)localObject2).a((Integer)localObject3);
      boolean bool3 = ((Contact)localObject1).U();
      localObject3 = Boolean.valueOf(bool3);
      localObject2 = ((al.a)localObject2).d((Boolean)localObject3);
      localObject3 = ((Contact)localObject1).G();
      a((al.a)localObject2, (String)localObject3);
      localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>();
      localObject4 = new java/util/ArrayList;
      ((ArrayList)localObject4).<init>();
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      Iterator localIterator = ((Contact)localObject1).J().iterator();
      for (;;)
      {
        boolean bool5 = localIterator.hasNext();
        if (!bool5) {
          break;
        }
        Object localObject5 = (Tag)localIterator.next();
        int i6 = ((Tag)localObject5).getSource();
        if (i6 == i3)
        {
          localObject5 = ((Tag)localObject5).a();
          ((List)localObject3).add(localObject5);
        }
        else
        {
          localObject5 = ((Tag)localObject5).a();
          ((List)localObject4).add(localObject5);
        }
      }
      Object localObject6 = ce.a((Contact)localObject1);
      if (localObject6 != null)
      {
        long l1 = a;
        localObject6 = String.valueOf(l1);
        localArrayList.add(localObject6);
      }
      localObject6 = bc.b();
      boolean bool6 = ((List)localObject3).isEmpty();
      if (bool6)
      {
        bool3 = false;
        localObject3 = null;
      }
      localObject3 = ((bc.a)localObject6).a((List)localObject3);
      int i3 = ((List)localObject4).isEmpty();
      if (i3 != 0)
      {
        i5 = 0;
        localObject4 = null;
      }
      localObject3 = ((bc.a)localObject3).b((List)localObject4);
      i3 = localArrayList.isEmpty();
      if (i3 != 0) {
        localArrayList = null;
      }
      localObject3 = ((bc.a)localObject3).c(localArrayList).a();
      localObject6 = ay.b();
      localObject4 = ((Contact)localObject1).G();
      localObject6 = ((ay.a)localObject6).a((CharSequence)localObject4);
      localObject3 = ((ay.a)localObject6).a((bc)localObject3);
      localObject2 = ((al.a)localObject2).a();
      localObject2 = ((ay.a)localObject3).a((al)localObject2);
      if (paramBoolean) {
        str1 = "validCacheResult";
      }
      localObject2 = ((ay.a)localObject2).b(str1).c(str2).a();
      paramList.add(localObject2);
      localObject1 = ((Contact)localObject1).G();
      paramSet.remove(localObject1);
    }
  }
  
  private void a(List paramList, Set paramSet, boolean paramBoolean)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    int n = paramSet.size();
    localArrayList.<init>(n);
    Iterator localIterator = paramSet.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = (String)localIterator.next();
      com.truecaller.data.access.c localc = b;
      localObject = localc.b((String)localObject);
      if (localObject != null) {
        localArrayList.add(localObject);
      }
    }
    a(localArrayList, paramList, paramSet, paramBoolean);
  }
  
  public final r c()
  {
    z.a locala = z.b();
    e.a locala2 = new com/truecaller/analytics/e$a;
    locala2.<init>("BackendSearch");
    Object localObject1 = k.b();
    locala2.a("NetworkType", (String)localObject1);
    int n = g;
    locala2.a("SearchType", n);
    Object localObject2 = i.toString();
    localObject2 = locala.a((CharSequence)localObject2);
    localObject1 = h;
    localObject2 = ((z.a)localObject2).d((CharSequence)localObject1);
    n = g;
    localObject1 = String.valueOf(n);
    localObject2 = ((z.a)localObject2).c((CharSequence)localObject1);
    localObject1 = m;
    ((z.a)localObject2).b((List)localObject1);
    localObject2 = l;
    long l1 = ((com.truecaller.utils.a)localObject2).b();
    try
    {
      r localr = super.c();
      Object localObject3 = l;
      l2 = ((com.truecaller.utils.a)localObject3).b();
      d1 = l2;
      double d2 = l1;
      Double.isNaN(d1);
      Double.isNaN(d2);
      d1 -= d2;
      try
      {
        Object localObject4 = b;
        localObject4 = (n)localObject4;
        Object localObject5 = a;
        boolean bool1 = ((ad)localObject5).c();
        if ((bool1) && (localObject4 != null))
        {
          localObject5 = new java/util/HashSet;
          Object localObject6 = f;
          ((HashSet)localObject5).<init>((Collection)localObject6);
          int i1 = g;
          int i2 = 1;
          if (i1 == 0)
          {
            i1 = 1;
          }
          else
          {
            i1 = 0;
            localObject6 = null;
          }
          if (i1 != 0)
          {
            localObject6 = a;
            locala.b((CharSequence)localObject6);
            locala.a(i2);
            locala.b(i2);
            localObject6 = "Result";
            String str = "Success";
            locala2.a((String)localObject6, str);
            localObject3 = Double.valueOf(d1);
            a = ((Double)localObject3);
            localObject3 = j;
            localObject7 = locala2.a();
            ((com.truecaller.analytics.b)localObject3).a((e)localObject7);
          }
          else
          {
            locala.b(null);
            locala.a(false);
            locala.b(false);
          }
          int i3 = g;
          boolean bool2;
          if (i3 == i2)
          {
            bool2 = true;
          }
          else
          {
            bool2 = false;
            localObject3 = null;
          }
          Object localObject7 = new java/util/ArrayList;
          ((ArrayList)localObject7).<init>();
          i1 = ((Set)localObject5).size();
          if (i1 > 0)
          {
            localObject6 = c;
            i1 = ((List)localObject6).size();
            if (i1 == 0)
            {
              a((List)localObject7, (Set)localObject5, bool2);
              break label646;
            }
          }
          i1 = ((Set)localObject5).size();
          int i5;
          if (i1 <= i2)
          {
            localObject6 = c;
            i1 = ((List)localObject6).size();
            if (i1 != i2)
            {
              int i4 = ((Set)localObject5).size();
              if (i4 != i2) {
                break label646;
              }
              localObject3 = c;
              i5 = ((List)localObject3).size();
              if (i5 <= i2) {
                break label646;
              }
              localObject3 = ay.b();
              localObject4 = f;
              localObject4 = ((List)localObject4).get(0);
              localObject4 = (CharSequence)localObject4;
              localObject3 = ((ay.a)localObject3).a((CharSequence)localObject4);
              localObject3 = ((ay.a)localObject3).a(null);
              localObject3 = ((ay.a)localObject3).a(null);
              localObject3 = ((ay.a)localObject3).b(null);
              localObject3 = ((ay.a)localObject3).c(null);
              localObject3 = ((ay.a)localObject3).a();
              ((List)localObject7).add(localObject3);
              ((Set)localObject5).clear();
              break label646;
            }
          }
          localObject4 = c;
          a((Collection)localObject4, (List)localObject7, (Set)localObject5, i5);
          label646:
          localObject3 = ((Set)localObject5).iterator();
          for (;;)
          {
            boolean bool3 = ((Iterator)localObject3).hasNext();
            if (!bool3) {
              break;
            }
            localObject4 = ((Iterator)localObject3).next();
            localObject4 = (String)localObject4;
            bool1 = k.b((CharSequence)localObject4);
            if (!bool1)
            {
              localObject5 = al.b();
              localObject5 = ((al.a)localObject5).b(false);
              localObject5 = ((al.a)localObject5).a(false);
              localObject6 = Boolean.FALSE;
              localObject5 = ((al.a)localObject5).e((Boolean)localObject6);
              localObject6 = Integer.valueOf(0);
              localObject5 = ((al.a)localObject5).a((Integer)localObject6);
              localObject6 = Boolean.FALSE;
              localObject5 = ((al.a)localObject5).d((Boolean)localObject6);
              a((al.a)localObject5, (String)localObject4);
              localObject6 = ay.b();
              localObject4 = ((ay.a)localObject6).a((CharSequence)localObject4);
              localObject4 = ((ay.a)localObject4).a(null);
              localObject5 = ((al.a)localObject5).a();
              localObject4 = ((ay.a)localObject4).a((al)localObject5);
              localObject4 = ((ay.a)localObject4).b(null);
              localObject4 = ((ay.a)localObject4).c(null);
              localObject4 = ((ay.a)localObject4).a();
              ((List)localObject7).add(localObject4);
            }
          }
          locala.a((List)localObject7);
          localObject3 = d;
          localObject3 = ((f)localObject3).a();
          localObject3 = (ae)localObject3;
          localObject7 = locala.a();
          ((ae)localObject3).a((d)localObject7);
        }
        else
        {
          a(locala, locala2, d1);
        }
      }
      catch (org.apache.a.a locala3)
      {
        AssertionUtil.reportThrowableButNeverCrash(locala3);
      }
      return localr;
    }
    catch (IOException localIOException)
    {
      com.truecaller.utils.a locala4 = l;
      long l2 = locala4.b();
      double d1 = l2;
      double d3 = l1;
      Double.isNaN(d1);
      Double.isNaN(d3);
      d1 -= d3;
      try
      {
        a(locala, locala2, d1);
      }
      catch (org.apache.a.a locala1)
      {
        AssertionUtil.reportThrowableButNeverCrash(locala1);
      }
      throw localIOException;
    }
  }
  
  public final e.b e()
  {
    c localc = new com/truecaller/network/search/c;
    e.b localb = a;
    com.truecaller.data.access.c localc1 = b;
    boolean bool = c;
    f localf = d;
    FilterManager localFilterManager = e;
    List localList1 = f;
    int n = g;
    String str = h;
    UUID localUUID = i;
    List localList2 = m;
    com.truecaller.analytics.b localb1 = j;
    i locali = k;
    com.truecaller.utils.a locala = l;
    localc.<init>(localb, localc1, bool, localf, localFilterManager, localList1, n, str, localUUID, localList2, localb1, locali, locala);
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.search.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */