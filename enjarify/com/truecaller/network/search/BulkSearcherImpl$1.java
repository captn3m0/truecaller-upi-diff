package com.truecaller.network.search;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

final class BulkSearcherImpl$1
  extends LinkedHashMap
{
  BulkSearcherImpl$1(BulkSearcherImpl paramBulkSearcherImpl)
  {
    super(10);
  }
  
  protected final boolean removeEldestEntry(Map.Entry paramEntry)
  {
    int i = size();
    BulkSearcherImpl localBulkSearcherImpl = a;
    int j = BulkSearcherImpl.a(localBulkSearcherImpl);
    return i > j;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.search.BulkSearcherImpl.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */