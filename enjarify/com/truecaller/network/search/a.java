package com.truecaller.network.search;

import com.truecaller.log.AssertionUtil.AlwaysFatal;
import e.b;
import e.d;
import e.r;
import okhttp3.ab;
import okhttp3.ad;

abstract class a
  implements b
{
  final b a;
  
  a(b paramb)
  {
    a = paramb;
  }
  
  protected r a(r paramr, Object paramObject)
  {
    return paramr;
  }
  
  public final void a(d paramd)
  {
    paramd = new String[] { "Don't call #enqueue()" };
    AssertionUtil.AlwaysFatal.isTrue(false, paramd);
  }
  
  public final boolean a()
  {
    return a.a();
  }
  
  public final ab b()
  {
    return a.b();
  }
  
  public r c()
  {
    r localr = a.c();
    Object localObject = a;
    boolean bool = ((ad)localObject).c();
    if (bool)
    {
      localObject = b;
      if (localObject != null) {
        return a(localr, localObject);
      }
    }
    return localr;
  }
  
  public final void d()
  {
    String[] arrayOfString = { "Don't call #cancel()" };
    AssertionUtil.AlwaysFatal.isTrue(false, arrayOfString);
  }
  
  public abstract b e();
}

/* Location:
 * Qualified Name:     com.truecaller.network.search.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */