package com.truecaller.network.search;

import android.content.Context;
import android.text.TextUtils;
import com.google.c.a.g;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.aa;
import com.truecaller.common.h.am;
import com.truecaller.filters.FilterManager;
import com.truecaller.log.AssertionUtil;
import com.truecaller.network.util.a.a;
import com.truecaller.search.j;
import e.r;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public final class d
  implements a.a
{
  final Context a;
  public final List b;
  public boolean c;
  public boolean d;
  public int e;
  String f;
  private final String g;
  private final UUID h;
  private List i = null;
  
  public d(Context paramContext, UUID paramUUID, String paramString)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    b = localArrayList;
    boolean bool = true;
    c = bool;
    d = bool;
    e = 999;
    paramContext = paramContext.getApplicationContext();
    a = paramContext;
    g = paramString;
    h = paramUUID;
  }
  
  private static List a(List paramList, String paramString)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    int j = paramList.size();
    localArrayList.<init>(j);
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject = (d.b)paramList.next();
      String str = b;
      boolean bool2 = TextUtils.isEmpty(str);
      if (!bool2)
      {
        str = c;
        bool2 = TextUtils.isEmpty(str);
        if (!bool2)
        {
          str = c;
          bool2 = am.b(str, paramString);
          if (!bool2) {}
        }
        else
        {
          localObject = b;
          localArrayList.add(localObject);
          continue;
        }
      }
      str = a;
      bool2 = TextUtils.isEmpty(str);
      if (!bool2)
      {
        localObject = a;
        localArrayList.add(localObject);
      }
      else
      {
        str = b;
        bool2 = TextUtils.isEmpty(str);
        if (!bool2)
        {
          str = c;
          bool2 = TextUtils.isEmpty(str);
          if (!bool2) {
            try
            {
              str = b;
              localObject = c;
              localObject = aa.a(str, (String)localObject);
              localArrayList.add(localObject);
            }
            catch (g localg) {}
          }
        }
      }
    }
    int k = localArrayList.size();
    int m = 25;
    if (k > m)
    {
      AssertionUtil.reportWeirdnessButNeverCrash("Batch size should be less than 25, truncating the list of searched numbers.");
      return localArrayList.subList(0, m);
    }
    return localArrayList;
  }
  
  public final d a()
  {
    String str = ((com.truecaller.common.b.a)a.getApplicationContext()).H();
    f = str;
    return this;
  }
  
  public final d a(Collection paramCollection)
  {
    b.addAll(paramCollection);
    return this;
  }
  
  public final n b()
  {
    return (n)ccb;
  }
  
  public final e.b c()
  {
    d locald = this;
    int j = e;
    boolean bool2 = true;
    int k = 999;
    if (j != k)
    {
      j = 1;
    }
    else
    {
      j = 0;
      localObject1 = null;
    }
    Object localObject2 = { "You must specify a search type" };
    AssertionUtil.isTrue(j, (String[])localObject2);
    boolean bool1 = b.isEmpty() ^ bool2;
    localObject2 = new String[] { "You must supply one or more numbers to search for" };
    AssertionUtil.isTrue(bool1, (String[])localObject2);
    Object localObject1 = f;
    localObject2 = com.truecaller.common.b.a.F().H();
    localObject1 = am.l((String)am.e((CharSequence)localObject1, (CharSequence)localObject2));
    List localList1 = a(b, (String)localObject1);
    bool1 = localList1.isEmpty() ^ bool2;
    Object localObject3 = { "You must supply one or more VALID numbers to search for" };
    AssertionUtil.isTrue(bool1, (String[])localObject3);
    localObject1 = TextUtils.join(",", localList1);
    localObject3 = com.truecaller.search.i.a();
    localObject2 = f;
    Object localObject4 = String.valueOf(e);
    localObject1 = ((j)localObject3).a((String)localObject1, (String)localObject2, (String)localObject4);
    d.a locala = new com/truecaller/network/search/d$a;
    bool2 = c;
    boolean bool3 = d;
    locala.<init>((e.b)localObject1, localList1, bool2, bool3);
    localObject1 = ((bk)a.getApplicationContext()).a();
    localObject3 = new com/truecaller/network/search/c;
    com.truecaller.data.access.c localc = new com/truecaller/data/access/c;
    localObject2 = a;
    localc.<init>((Context)localObject2);
    f localf = ((bp)localObject1).f();
    FilterManager localFilterManager = ((bp)localObject1).P();
    int m = e;
    String str = g;
    UUID localUUID = h;
    List localList2 = i;
    com.truecaller.analytics.b localb = ((bp)localObject1).c();
    com.truecaller.utils.i locali = ((bp)localObject1).v();
    com.truecaller.utils.a locala1 = ((bp)localObject1).bL();
    localObject4 = localObject3;
    ((c)localObject3).<init>(locala, localc, true, localf, localFilterManager, localList1, m, str, localUUID, localList2, localb, locali, locala1);
    return (e.b)localObject3;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.search.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */