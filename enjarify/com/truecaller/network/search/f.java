package com.truecaller.network.search;

import android.content.Context;
import com.truecaller.common.h.aa;
import com.truecaller.data.access.c;
import com.truecaller.data.entity.Contact;
import com.truecaller.search.local.model.g;
import com.truecaller.search.local.model.j;
import e.b;
import e.r;

final class f
  extends a
{
  private final String b;
  
  f(b paramb, String paramString)
  {
    super(paramb);
    b = paramString;
  }
  
  public final r c()
  {
    Object localObject1 = new com/truecaller/data/access/c;
    Object localObject2 = com.truecaller.common.b.a.F();
    ((c)localObject1).<init>((Context)localObject2);
    localObject2 = g.a(com.truecaller.common.b.a.F());
    String str1 = aa.b(b);
    localObject1 = ((c)localObject1).b(str1);
    str1 = null;
    int i = 1;
    Object localObject3;
    String str2;
    if (localObject1 != null)
    {
      boolean bool = ((Contact)localObject1).S();
      if (bool)
      {
        localObject2 = new String[i];
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>("Search result cache hit for ");
        str2 = b;
        ((StringBuilder)localObject3).append(str2);
        localObject3 = ((StringBuilder)localObject3).toString();
        localObject2[0] = localObject3;
        localObject2 = b;
        ((Contact)localObject1).n((String)localObject2);
        localObject2 = new com/truecaller/network/search/n;
        ((n)localObject2).<init>(i, null, (Contact)localObject1);
        return r.a(localObject2);
      }
    }
    localObject1 = b;
    localObject1 = ((g)localObject2).a((String)localObject1);
    if (localObject1 != null)
    {
      int j = g;
      int k = 32;
      if (j != k)
      {
        localObject2 = new String[i];
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>("Search cached lookup hit for ");
        str2 = b;
        ((StringBuilder)localObject3).append(str2);
        localObject3 = ((StringBuilder)localObject3).toString();
        localObject2[0] = localObject3;
        localObject2 = new com/truecaller/network/search/n;
        localObject1 = ((j)localObject1).f();
        ((n)localObject2).<init>(i, null, (Contact)localObject1);
        return r.a(localObject2);
      }
    }
    localObject1 = b;
    localObject1 = ((g)localObject2).d((String)localObject1);
    if (localObject1 == null)
    {
      localObject1 = a.c();
      str1 = b;
      ((g)localObject2).a(str1, (r)localObject1);
    }
    return (r)localObject1;
  }
  
  public final b e()
  {
    f localf = new com/truecaller/network/search/f;
    b localb = a.e();
    String str = b;
    localf.<init>(localb, str);
    return localf;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.search.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */