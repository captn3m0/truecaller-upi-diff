package com.truecaller.network.search;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.d;
import android.support.v7.widget.RecyclerView.Adapter;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.b.a;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.am;
import com.truecaller.utils.i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BulkSearcherImpl
  implements e
{
  private static final Intent d;
  final Set a;
  final Set b;
  final LinkedHashMap c;
  private final Context e;
  private final d f;
  private final int g;
  private final int h;
  private final int i;
  private final RecyclerView.Adapter j;
  private final Handler k;
  private final i l;
  private final int m;
  private j.b mListener;
  private final String n;
  private final Map o;
  private List p;
  private final Runnable q;
  
  static
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("com.truecaller.actions.BULK_SEARCH_COMPLETE");
    d = localIntent;
  }
  
  public BulkSearcherImpl(Context paramContext, int paramInt, String paramString, e.a parama)
  {
    this(paramContext, paramInt, paramString, parama, (byte)0);
  }
  
  private BulkSearcherImpl(Context paramContext, int paramInt, String paramString, e.a parama, byte paramByte)
  {
    Object localObject = new java/util/HashSet;
    ((HashSet)localObject).<init>();
    a = ((Set)localObject);
    localObject = new java/util/HashSet;
    ((HashSet)localObject).<init>();
    b = ((Set)localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    o = ((Map)localObject);
    localObject = new com/truecaller/network/search/BulkSearcherImpl$1;
    ((BulkSearcherImpl.1)localObject).<init>(this);
    c = ((LinkedHashMap)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    p = ((List)localObject);
    localObject = new com/truecaller/network/search/BulkSearcherImpl$2;
    ((BulkSearcherImpl.2)localObject).<init>(this);
    q = ((Runnable)localObject);
    paramContext = paramContext.getApplicationContext();
    e = paramContext;
    paramContext = d.a(e);
    f = paramContext;
    g = 10;
    h = 2;
    i = 500;
    j = null;
    paramContext = new android/os/Handler;
    localObject = Looper.getMainLooper();
    paramContext.<init>((Looper)localObject);
    k = paramContext;
    m = paramInt;
    n = paramString;
    a(parama);
    paramContext = ((bk)e).a().v();
    l = paramContext;
  }
  
  public final void a(e.a parama)
  {
    if (parama != null)
    {
      List localList = p;
      localList.add(parama);
    }
  }
  
  public final void a(String paramString1, String paramString2)
  {
    if (paramString1 == null) {
      return;
    }
    Object localObject = a;
    boolean bool1 = ((Set)localObject).contains(paramString1);
    if (!bool1)
    {
      localObject = b;
      bool1 = ((Set)localObject).contains(paramString1);
      if (!bool1)
      {
        localObject = c;
        bool1 = ((LinkedHashMap)localObject).containsKey(paramString1);
        if (!bool1)
        {
          localObject = (Integer)o.get(paramString1);
          int i4;
          if (localObject != null)
          {
            i1 = ((Integer)localObject).intValue();
            i4 = h;
            if (i1 > i4)
            {
              i1 = 1;
              break label108;
            }
          }
          int i1 = 0;
          localObject = null;
          label108:
          if (i1 == 0)
          {
            boolean bool2 = am.d(paramString1);
            if (!bool2)
            {
              int i2 = 20;
              i4 = m;
              boolean bool3;
              if (i2 != i4)
              {
                bool3 = ab.e(paramString1);
                if (!bool3) {}
              }
              else
              {
                localObject = l;
                bool3 = ((i)localObject).a();
                if (bool3)
                {
                  localObject = (a)e;
                  bool3 = ((a)localObject).p();
                  if (bool3)
                  {
                    localObject = c;
                    d.b localb = new com/truecaller/network/search/d$b;
                    localb.<init>(paramString1, paramString2, null);
                    ((LinkedHashMap)localObject).put(paramString1, localb);
                  }
                }
              }
            }
          }
        }
      }
    }
    paramString1 = k;
    paramString2 = q;
    paramString1.removeCallbacks(paramString2);
    paramString1 = c;
    boolean bool4 = paramString1.isEmpty();
    if (!bool4)
    {
      paramString1 = k;
      paramString2 = q;
      int i3 = i;
      long l1 = i3;
      paramString1.postDelayed(paramString2, l1);
    }
  }
  
  final void a(Collection paramCollection)
  {
    a.removeAll(paramCollection);
    b.removeAll(paramCollection);
    Object localObject1 = paramCollection.iterator();
    boolean bool1;
    Object localObject2;
    Object localObject3;
    for (;;)
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (String)((Iterator)localObject1).next();
      int i1 = 0;
      localObject3 = null;
      Map localMap = o;
      boolean bool2 = localMap.containsKey(localObject2);
      if (bool2)
      {
        localObject3 = (Integer)o.get(localObject2);
        i1 = ((Integer)localObject3).intValue() + 1;
      }
      localMap = o;
      localObject3 = Integer.valueOf(i1);
      localMap.put(localObject2, localObject3);
    }
    localObject1 = j;
    if (localObject1 != null) {
      ((RecyclerView.Adapter)localObject1).notifyDataSetChanged();
    }
    localObject1 = p.iterator();
    for (;;)
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (e.a)((Iterator)localObject1).next();
      localObject3 = new java/util/HashSet;
      ((HashSet)localObject3).<init>(paramCollection);
      ((e.a)localObject2).a((Set)localObject3);
    }
  }
  
  public final boolean a(String paramString)
  {
    if (paramString != null)
    {
      Object localObject = c;
      boolean bool1 = ((LinkedHashMap)localObject).containsKey(paramString);
      if (!bool1)
      {
        localObject = b;
        boolean bool2 = ((Set)localObject).contains(paramString);
        if (!bool2) {}
      }
      else
      {
        return true;
      }
    }
    return false;
  }
  
  public final void b(e.a parama)
  {
    p.remove(parama);
  }
  
  final void b(Collection paramCollection)
  {
    b.removeAll(paramCollection);
    Object localObject1 = f;
    Object localObject2 = d;
    ((d)localObject1).a((Intent)localObject2);
    localObject1 = p.iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (e.a)((Iterator)localObject1).next();
      ((e.a)localObject2).a(paramCollection);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.search.BulkSearcherImpl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */