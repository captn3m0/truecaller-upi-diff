package com.truecaller.network.search;

import com.truecaller.ads.campaigns.b;
import com.truecaller.data.entity.Contact;
import com.truecaller.search.ContactDto.Pagination;
import java.util.ArrayList;
import java.util.List;

public final class n
{
  public final String a;
  public final b b;
  public final List c;
  public final String d;
  public final String e;
  public final String f;
  public final int g;
  
  n(int paramInt, n paramn, Contact paramContact)
  {
    g = paramInt;
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    c = ((List)localObject);
    c.add(paramContact);
    paramInt = 0;
    localObject = null;
    f = null;
    e = null;
    d = null;
    if (paramn == null) {
      paramContact = null;
    } else {
      paramContact = b;
    }
    b = paramContact;
    if (paramn != null) {
      localObject = a;
    }
    a = ((String)localObject);
  }
  
  public n(String paramString, List paramList, ContactDto.Pagination paramPagination, b paramb)
  {
    c = paramList;
    a = paramString;
    b = paramb;
    paramString = null;
    g = 0;
    if (paramPagination == null)
    {
      f = null;
      e = null;
      d = null;
      return;
    }
    paramString = prev;
    d = paramString;
    paramString = pageId;
    e = paramString;
    paramString = next;
    f = paramString;
  }
  
  public final Contact a()
  {
    List localList = c;
    boolean bool = localList.isEmpty();
    if (!bool) {
      return (Contact)c.get(0);
    }
    return null;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SearchResult{requestId='");
    String str = a;
    localStringBuilder.append(str);
    char c1 = '\'';
    localStringBuilder.append(c1);
    localStringBuilder.append(", campaigns=");
    Object localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", data=");
    localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", previousPageId='");
    localObject = d;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(c1);
    localStringBuilder.append(", pageId='");
    localObject = e;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(c1);
    localStringBuilder.append(", nextPageId='");
    localObject = f;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(c1);
    localStringBuilder.append(", source=");
    int i = g;
    localStringBuilder.append(i);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.search.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */