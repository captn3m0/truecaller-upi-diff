package com.truecaller.network.search;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.text.TextUtils;
import com.google.c.a.m.a;
import com.truecaller.TrueApp;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.am;
import com.truecaller.common.h.u;
import com.truecaller.common.network.e.a;
import com.truecaller.data.entity.Contact;
import com.truecaller.filters.FilterManager;
import com.truecaller.log.AssertionUtil;
import com.truecaller.network.util.a.a;
import com.truecaller.util.co;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import okhttp3.ad;

public final class j
  implements a.a
{
  private Double A;
  private Double B;
  public List a = null;
  public boolean b;
  public boolean c;
  public boolean d;
  public boolean e;
  public boolean f;
  public j.a g;
  public int h;
  public String i;
  public String j;
  public String k;
  public int l;
  public TimeUnit m;
  public boolean n;
  private final Context o;
  private final u p;
  private final com.truecaller.common.h.r q;
  private final com.truecaller.featuretoggles.e r;
  private final String s;
  private final UUID t;
  private boolean u;
  private int v;
  private String w;
  private Integer x;
  private Double y;
  private Double z;
  
  public j(Context paramContext, UUID paramUUID, String paramString)
  {
    boolean bool = true;
    b = bool;
    c = bool;
    d = bool;
    e = bool;
    u = false;
    f = bool;
    v = 5;
    h = 999;
    Context localContext = paramContext.getApplicationContext();
    o = localContext;
    s = paramString;
    t = paramUUID;
    paramContext = getApplicationContextb;
    paramUUID = paramContext.h();
    p = paramUUID;
    paramUUID = paramContext.i();
    q = paramUUID;
    paramContext = paramContext.v();
    r = paramContext;
  }
  
  private j c(String paramString)
  {
    Locale localLocale = Locale.ENGLISH;
    paramString = am.c(paramString, localLocale);
    w = paramString;
    return this;
  }
  
  private com.truecaller.common.network.e g()
  {
    Object localObject1 = e.a.a;
    Object localObject2 = r.g();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject2).a();
    if (bool)
    {
      localObject2 = p;
      String str1 = i;
      localObject2 = ((u)localObject2).c(str1);
      if (localObject2 != null) {
        localObject1 = q.a((m.a)localObject2);
      }
    }
    localObject2 = new String[1];
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("Target domain for ");
    String str2 = i;
    ((StringBuilder)localObject3).append(str2);
    ((StringBuilder)localObject3).append(": ");
    ((StringBuilder)localObject3).append(localObject1);
    localObject3 = ((StringBuilder)localObject3).toString();
    localObject2[0] = localObject3;
    return (com.truecaller.common.network.e)localObject1;
  }
  
  public final AsyncTask a(com.truecaller.old.a.c paramc, boolean paramBoolean1, boolean paramBoolean2, j.b paramb)
  {
    k localk = new com/truecaller/network/search/k;
    Context localContext = o;
    String str1 = i;
    String str2 = w;
    j.a locala = g;
    localk.<init>(localContext, paramc, paramBoolean1, paramBoolean2, this, str1, str2, paramb, locala);
    paramc = new Void[0];
    localk.b(paramc);
    return localk;
  }
  
  public final j a()
  {
    String str = com.truecaller.common.b.a.F().H();
    return c(str);
  }
  
  public final j a(int paramInt)
  {
    h = paramInt;
    return this;
  }
  
  public final j a(String paramString)
  {
    i = paramString;
    return this;
  }
  
  public final j b()
  {
    b = true;
    return this;
  }
  
  public final j b(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool) {
      return a();
    }
    return c(paramString);
  }
  
  public final e.b c()
  {
    j localj = this;
    int i1 = h;
    int i4 = 1;
    int i5 = 999;
    if (i1 != i5)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      localObject1 = null;
    }
    Object localObject2 = { "You must specify a search type" };
    AssertionUtil.isTrue(i1, (String[])localObject2);
    boolean bool1 = TextUtils.isEmpty(i) ^ i4;
    localObject2 = new String[] { "You must specify a search query" };
    AssertionUtil.isTrue(bool1, (String[])localObject2);
    Object localObject1 = A;
    if (localObject1 != null)
    {
      localObject1 = B;
      if (localObject1 != null) {}
    }
    else
    {
      localObject1 = co.b(com.truecaller.common.b.a.F());
      if (localObject1 != null)
      {
        localObject2 = Double.valueOf(((Location)localObject1).getLatitude());
        A = ((Double)localObject2);
        double d1 = ((Location)localObject1).getLongitude();
        localObject1 = Double.valueOf(d1);
        B = ((Double)localObject1);
      }
    }
    localObject1 = com.truecaller.ads.campaigns.f.a(o);
    int i6 = h;
    boolean bool5 = false;
    Object localObject3 = null;
    boolean bool3;
    switch (i6)
    {
    default: 
      break;
    case 20: 
      localObject2 = "detailView";
      localObject4 = s;
      bool3 = ((String)localObject2).equals(localObject4);
      if (bool3)
      {
        localObject2 = ((com.truecaller.ads.campaigns.e)localObject1).a(i4);
        localObject5 = localObject2;
      }
      break;
    case 4: 
    case 10: 
    case 15: 
      localObject2 = ((com.truecaller.ads.campaigns.e)localObject1).a(i4);
      localObject5 = localObject2;
      break;
    case 1: 
    case 2: 
    case 6: 
    case 7: 
    case 12: 
      localObject2 = ((com.truecaller.ads.campaigns.e)localObject1).a(0);
      localObject5 = localObject2;
      break;
    }
    Object localObject5 = null;
    Object localObject6 = g();
    if (localObject5 != null)
    {
      bool3 = localObject6 instanceof e.a;
      if (bool3)
      {
        localObject3 = ((com.truecaller.ads.campaigns.e)localObject1).a();
        localObject7 = localObject3;
        break label375;
      }
    }
    Object localObject7 = null;
    label375:
    int i2 = l;
    localObject2 = m;
    bool5 = n;
    Object localObject4 = com.truecaller.search.i.a(i2, (TimeUnit)localObject2, bool5);
    Object localObject8 = i;
    Object localObject9 = w;
    Object localObject10 = String.valueOf(h);
    Object localObject11 = j;
    Double localDouble = y;
    Object localObject12 = z;
    Object localObject13 = A;
    Object localObject14 = B;
    localObject1 = k;
    localObject2 = x;
    Object localObject15 = ((com.truecaller.search.j)localObject4).a((String)localObject8, (com.truecaller.common.network.e)localObject6, (String)localObject9, (String)localObject10, (String)localObject11, localDouble, (Double)localObject12, (Double)localObject13, (Double)localObject14, (String)localObject1, (Integer)localObject2, (String)localObject5, (String)localObject7);
    boolean bool2 = d;
    if (bool2)
    {
      localObject1 = k;
      if (localObject1 == null)
      {
        localObject1 = x;
        if (localObject1 == null)
        {
          bool6 = true;
          break label543;
        }
      }
    }
    boolean bool6 = false;
    label543:
    if (bool6)
    {
      bool2 = e;
      if (bool2)
      {
        bool7 = true;
        break label566;
      }
    }
    boolean bool7 = false;
    label566:
    localObject1 = i;
    bool2 = am.b((String)localObject1);
    if (!bool2)
    {
      i3 = 20;
      int i7 = h;
      if (i3 != i7)
      {
        i3 = 0;
        localObject1 = null;
        break label610;
      }
    }
    int i3 = 1;
    label610:
    boolean bool4 = b;
    if (bool4)
    {
      localObject2 = k;
      if (localObject2 == null)
      {
        localObject2 = x;
        if ((localObject2 == null) && (i3 != 0))
        {
          i3 = 1;
          break label657;
        }
      }
    }
    i3 = 0;
    localObject1 = null;
    label657:
    localObject2 = ((bk)o.getApplicationContext()).a();
    localObject3 = new com/truecaller/network/search/i;
    localObject4 = i;
    int i8 = h;
    localObject6 = t;
    Object localObject16 = localObject3;
    Object localObject17 = localObject4;
    ((i)localObject3).<init>((e.b)localObject15, (String)localObject4, bool6, bool7, i8, (UUID)localObject6);
    if (i3 != 0)
    {
      localObject4 = new com/truecaller/network/search/f;
      localObject8 = i;
      ((f)localObject4).<init>((e.b)localObject3, (String)localObject8);
      localObject3 = localObject4;
    }
    boolean bool8 = c;
    if (bool8)
    {
      localObject4 = new com/truecaller/network/search/b;
      localObject8 = i;
      ((b)localObject4).<init>((e.b)localObject3, (String)localObject8);
      localObject3 = localObject4;
    }
    bool8 = u;
    if (bool8)
    {
      localObject4 = new com/truecaller/network/search/h;
      localObject8 = ((bp)localObject2).ad();
      int i9 = v;
      ((h)localObject4).<init>((e.b)localObject3, (com.truecaller.androidactors.f)localObject8, i9);
      localObject10 = localObject4;
    }
    else
    {
      localObject10 = localObject3;
    }
    bool5 = f;
    if (bool5)
    {
      localObject3 = new com/truecaller/network/search/c;
      localObject11 = new com/truecaller/data/access/c;
      localObject4 = o;
      ((com.truecaller.data.access.c)localObject11).<init>((Context)localObject4);
      boolean bool9 = i3 ^ 0x1;
      localObject12 = ((bp)localObject2).f();
      localObject13 = ((bp)localObject2).P();
      localObject14 = i;
      i3 = h;
      localObject4 = s;
      localObject8 = t;
      localObject6 = a;
      localObject16 = ((bp)localObject2).c();
      localObject15 = ((bp)localObject2).v();
      localObject17 = ((bp)localObject2).bL();
      localObject9 = localObject3;
      localObject5 = localObject8;
      localObject7 = localObject6;
      ((c)localObject3).<init>((e.b)localObject10, (com.truecaller.data.access.c)localObject11, bool9, (com.truecaller.androidactors.f)localObject12, (FilterManager)localObject13, (String)localObject14, i3, (String)localObject4, (UUID)localObject8, (List)localObject6, (com.truecaller.analytics.b)localObject16, (com.truecaller.utils.i)localObject15, (com.truecaller.utils.a)localObject17);
    }
    else
    {
      localObject3 = localObject10;
    }
    localObject1 = new String[i4];
    Object localObject18 = new java/lang/StringBuilder;
    ((StringBuilder)localObject18).<init>("Constructed search call(s) for ");
    localObject2 = i;
    ((StringBuilder)localObject18).append((String)localObject2);
    ((StringBuilder)localObject18).append(", ");
    ((StringBuilder)localObject18).append(localObject3);
    localObject18 = ((StringBuilder)localObject18).toString();
    localObject1[0] = localObject18;
    return (e.b)localObject3;
  }
  
  public final j d()
  {
    d = false;
    return this;
  }
  
  public final j e()
  {
    v = 5;
    u = true;
    return this;
  }
  
  public final n f()
  {
    Object localObject1 = c().c();
    Object localObject2 = a;
    boolean bool = ((ad)localObject2).c();
    if (bool)
    {
      localObject2 = (n)b;
      if (localObject2 != null)
      {
        localObject1 = b;
        Object localObject3;
        if (localObject1 != null)
        {
          localObject3 = o;
          String str1 = i;
          String str2 = w;
          Contact localContact = ((n)localObject2).a();
          k.a((Context)localObject3, str1, str2, localContact, (com.truecaller.ads.campaigns.b)localObject1);
        }
        localObject1 = g;
        if (localObject1 != null)
        {
          localObject3 = i;
          return ((j.a)localObject1).intercept((n)localObject2, (String)localObject3);
        }
        return (n)localObject2;
      }
    }
    return (n)b;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.search.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */