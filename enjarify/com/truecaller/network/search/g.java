package com.truecaller.network.search;

import android.text.TextUtils;
import com.truecaller.common.h.am;
import com.truecaller.log.AssertionUtil;
import com.truecaller.search.ContactDto.Contact;
import com.truecaller.search.ContactDto.Contact.Business;
import com.truecaller.search.ContactDto.Contact.BusinessProfile;
import com.truecaller.search.ContactDto.Contact.BusinessProfile.OpenHours;
import com.truecaller.search.ContactDto.Contact.PhoneNumber;
import com.truecaller.search.ContactDto.Contact.Style;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;

final class g
{
  static void a(ContactDto.Contact paramContact, long paramLong, String paramString1, String paramString2)
  {
    searchTime = paramLong;
    cacheControl = paramString2;
    searchQuery = paramString1;
    int i = 1;
    source = i;
    Object localObject1 = "public";
    paramString1 = access;
    boolean bool1 = ((String)localObject1).equalsIgnoreCase(paramString1);
    paramString1 = null;
    boolean bool2;
    Object localObject2;
    Object localObject3;
    boolean bool5;
    Object localObject4;
    if (!bool1)
    {
      phones = null;
    }
    else
    {
      localObject1 = phones;
      if (localObject1 != null)
      {
        localObject1 = phones.iterator();
        for (;;)
        {
          bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          paramString2 = (ContactDto.Contact.PhoneNumber)((Iterator)localObject1).next();
          localObject2 = "senderId";
          localObject3 = type;
          boolean bool3 = ((String)localObject2).equals(localObject3);
          if (bool3)
          {
            localObject2 = id;
            e164Format = ((String)localObject2);
          }
          localObject2 = telType;
          bool3 = TextUtils.isEmpty((CharSequence)localObject2);
          if (bool3)
          {
            int j = ContactDto.Contact.PhoneNumber.EMPTY_TEL_TYPE;
            localObject2 = String.valueOf(j);
            telType = ((String)localObject2);
          }
          localObject2 = defaultNumber;
          boolean bool4 = TextUtils.isEmpty((CharSequence)localObject2);
          if (bool4)
          {
            int k = 2;
            localObject2 = new String[k];
            bool5 = false;
            localObject3 = null;
            localObject4 = e164Format;
            localObject2[0] = localObject4;
            paramString2 = nationalFormat;
            localObject2[i] = paramString2;
            paramString2 = am.b((String[])localObject2);
            defaultNumber = paramString2;
          }
        }
      }
    }
    Object localObject5 = businessProfileNetworkResponse;
    if (localObject5 != null)
    {
      localObject1 = new com/truecaller/search/ContactDto$Contact$Business;
      ((ContactDto.Contact.Business)localObject1).<init>();
      paramString2 = branch;
      branch = paramString2;
      paramString2 = department;
      department = paramString2;
      paramString2 = companySize;
      companySize = paramString2;
      paramString2 = openHours;
      if (paramString2 != null)
      {
        paramString2 = new java/lang/StringBuilder;
        paramString2.<init>();
        localObject2 = openHours.iterator();
        for (;;)
        {
          bool5 = ((Iterator)localObject2).hasNext();
          if (!bool5) {
            break;
          }
          localObject3 = (ContactDto.Contact.BusinessProfile.OpenHours)((Iterator)localObject2).next();
          localObject4 = weekdays;
          if (localObject4 != null)
          {
            localObject4 = opens;
            if (localObject4 != null)
            {
              localObject4 = closes;
              if (localObject4 != null)
              {
                localObject4 = weekdays.iterator();
                for (;;)
                {
                  boolean bool6 = ((Iterator)localObject4).hasNext();
                  if (!bool6) {
                    break;
                  }
                  Integer localInteger = (Integer)((Iterator)localObject4).next();
                  paramString2.append(localInteger);
                }
                paramString2.append(" ");
                localObject4 = opens;
                paramString2.append((String)localObject4);
                localObject4 = " ";
                paramString2.append((String)localObject4);
                localObject3 = closes;
                paramString2.append((String)localObject3);
                localObject3 = "|";
                paramString2.append((String)localObject3);
              }
            }
          }
        }
        paramString2 = paramString2.toString();
        openingHours = paramString2;
      }
      else
      {
        openingHours = null;
      }
      paramString2 = landLine;
      landline = paramString2;
      paramString2 = score;
      score = paramString2;
      paramString2 = swishNumber;
      swishNumber = paramString2;
      business = ((ContactDto.Contact.Business)localObject1);
      localObject1 = new com/truecaller/search/ContactDto$Contact$Style;
      ((ContactDto.Contact.Style)localObject1).<init>();
      paramString2 = backgroundColor;
      backgroundColor = paramString2;
      paramString2 = imageUrls;
      if (paramString2 != null)
      {
        paramString2 = imageUrls;
        bool2 = paramString2.isEmpty();
        if (!bool2)
        {
          paramString1 = new java/lang/StringBuilder;
          paramString1.<init>();
          localObject5 = imageUrls.iterator();
          for (;;)
          {
            bool2 = ((Iterator)localObject5).hasNext();
            if (!bool2) {
              break;
            }
            paramString2 = (String)((Iterator)localObject5).next();
            localObject2 = "UTF-8";
            try
            {
              paramString2 = URLEncoder.encode(paramString2, (String)localObject2);
              paramString1.append(paramString2);
              paramString2 = "|";
              paramString1.append(paramString2);
            }
            catch (UnsupportedEncodingException paramString2)
            {
              AssertionUtil.reportThrowableButNeverCrash(paramString2);
            }
          }
          localObject5 = paramString1.toString();
          imageUrls = ((String)localObject5);
          break label775;
        }
      }
      imageUrls = null;
      label775:
      style = ((ContactDto.Contact.Style)localObject1);
      paramContact = badges;
      localObject5 = "business";
      paramContact.add(localObject5);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.search.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */