package com.truecaller.network.search;

import com.truecaller.common.h.am;
import java.util.Locale;

public final class d$b
{
  final String a;
  final String b;
  final String c;
  
  public d$b(String paramString1, String paramString2, String paramString3)
  {
    a = paramString1;
    b = paramString2;
    paramString1 = Locale.ENGLISH;
    paramString1 = am.c(paramString3, paramString1);
    c = paramString1;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject != this)
    {
      boolean bool1 = paramObject instanceof b;
      if (bool1)
      {
        String str = a;
        paramObject = a;
        boolean bool2 = str.equals(paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    return a.hashCode();
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("BulkNumber{countryCode='");
    String str = c;
    localStringBuilder.append(str);
    localStringBuilder.append('\'');
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.search.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */