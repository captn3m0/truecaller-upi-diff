package com.truecaller.network.search;

import android.content.Context;
import c.g.b.k;
import java.util.UUID;

public final class m
  implements l
{
  private final Context a;
  
  public m(Context paramContext)
  {
    a = paramContext;
  }
  
  public final j a(UUID paramUUID, String paramString)
  {
    k.b(paramUUID, "requestId");
    k.b(paramString, "searchSource");
    j localj = new com/truecaller/network/search/j;
    Context localContext = a;
    localj.<init>(localContext, paramUUID, paramString);
    return localj;
  }
  
  public final d b(UUID paramUUID, String paramString)
  {
    k.b(paramUUID, "requestId");
    k.b(paramString, "searchSource");
    d locald = new com/truecaller/network/search/d;
    Context localContext = a;
    locald.<init>(localContext, paramUUID, paramString);
    return locald;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.search.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */