package com.truecaller.network.search;

import android.content.Context;
import com.truecaller.common.b.a;
import com.truecaller.common.h.aa;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.search.ContactDto.Contact;
import com.truecaller.search.KeyedContactDto;
import com.truecaller.search.KeyedContactDto.KeyedContact;
import com.truecaller.search.k;
import com.truecaller.search.l;
import e.b;
import e.d;
import e.r;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import okhttp3.ab;
import okhttp3.ad;
import okhttp3.ae;
import okhttp3.t;

final class d$a
  implements b
{
  private final b a;
  private final Collection b;
  private final boolean c;
  private final boolean d;
  
  d$a(b paramb, Collection paramCollection, boolean paramBoolean1, boolean paramBoolean2)
  {
    a = paramb;
    b = paramCollection;
    c = paramBoolean1;
    d = paramBoolean2;
  }
  
  public final void a(d paramd)
  {
    String[] arrayOfString = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(false, arrayOfString);
  }
  
  public final boolean a()
  {
    return a.a();
  }
  
  public final ab b()
  {
    return a.b();
  }
  
  public final r c()
  {
    Object localObject1 = a.c();
    Object localObject2 = a;
    boolean bool1 = ((ad)localObject2).c();
    if (bool1)
    {
      localObject2 = (KeyedContactDto)b;
      if (localObject2 != null)
      {
        Object localObject3 = data;
        if (localObject3 != null)
        {
          localObject3 = b;
          Object localObject4 = k.a((r)localObject1);
          boolean bool2 = c;
          boolean bool3 = d;
          ArrayList localArrayList1 = new java/util/ArrayList;
          localArrayList1.<init>();
          List localList = data;
          if (localList != null)
          {
            long l = System.currentTimeMillis();
            Object localObject5 = data.iterator();
            Object localObject7;
            for (;;)
            {
              boolean bool4 = ((Iterator)localObject5).hasNext();
              if (!bool4) {
                break;
              }
              Object localObject6 = (KeyedContactDto.KeyedContact)((Iterator)localObject5).next();
              localObject7 = value;
              String str1 = key;
              g.a((ContactDto.Contact)localObject7, l, str1, (String)localObject4);
              localObject7 = new com/truecaller/data/entity/Contact;
              localObject6 = value;
              ((Contact)localObject7).<init>((ContactDto.Contact)localObject6);
              localArrayList1.add(localObject7);
            }
            if (bool2)
            {
              localObject4 = new java/util/ArrayList;
              ((ArrayList)localObject4).<init>();
              ArrayList localArrayList2 = new java/util/ArrayList;
              localArrayList2.<init>();
              if (localObject3 == null)
              {
                localObject3 = new java/util/HashSet;
                ((HashSet)localObject3).<init>();
              }
              else
              {
                localObject5 = new java/util/HashSet;
                ((HashSet)localObject5).<init>((Collection)localObject3);
                localObject3 = localObject5;
              }
              localObject2 = data.iterator();
              for (;;)
              {
                boolean bool5 = ((Iterator)localObject2).hasNext();
                if (!bool5) {
                  break;
                }
                localObject5 = (KeyedContactDto.KeyedContact)((Iterator)localObject2).next();
                int i = ((ArrayList)localObject4).size();
                localObject7 = value;
                l.a((List)localObject4, localArrayList2, (ContactDto.Contact)localObject7);
                int j = ((ArrayList)localObject4).size();
                if (j > i)
                {
                  localObject5 = key;
                  ((Set)localObject3).remove(localObject5);
                }
              }
              if (bool3)
              {
                bool1 = ((Set)localObject3).isEmpty();
                if (!bool1)
                {
                  localObject2 = ((Set)localObject3).iterator();
                  for (;;)
                  {
                    boolean bool6 = ((Iterator)localObject2).hasNext();
                    if (!bool6) {
                      break;
                    }
                    localObject3 = (String)((Iterator)localObject2).next();
                    String str2 = aa.b((String)localObject3);
                    l.a((List)localObject4, (String)localObject3, str2, l);
                  }
                }
              }
              localObject2 = a.F();
              l.a((Context)localObject2, (ArrayList)localObject4, localArrayList2);
            }
          }
          localObject2 = a.f.a("tc-event-id");
          localObject3 = new com/truecaller/network/search/n;
          ((n)localObject3).<init>((String)localObject2, localArrayList1, null, null);
          localObject1 = a.f;
          return r.a(localObject3, (t)localObject1);
        }
      }
    }
    localObject2 = c;
    localObject1 = a;
    return r.a((ae)localObject2, (ad)localObject1);
  }
  
  public final void d()
  {
    String[] arrayOfString = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(false, arrayOfString);
  }
  
  public final b e()
  {
    a locala = new com/truecaller/network/search/d$a;
    b localb = a.e();
    Collection localCollection = b;
    boolean bool1 = c;
    boolean bool2 = d;
    locala.<init>(localb, localCollection, bool1, bool2);
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.search.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */