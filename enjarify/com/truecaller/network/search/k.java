package com.truecaller.network.search;

import android.content.Context;
import android.text.TextUtils;
import com.truecaller.ads.campaigns.b;
import com.truecaller.ads.campaigns.e;
import com.truecaller.ads.campaigns.f;
import com.truecaller.common.h.aa;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.network.util.a;
import com.truecaller.network.util.a.a;
import com.truecaller.old.a.c;
import com.truecaller.util.bj;
import e.r;
import java.lang.ref.WeakReference;

final class k
  extends a
{
  private final Context b;
  private final WeakReference c;
  private final WeakReference d;
  private final String e;
  private final String f;
  
  k(Context paramContext, c paramc, boolean paramBoolean1, boolean paramBoolean2, a.a parama, String paramString1, String paramString2, j.b paramb, j.a parama1)
  {
    super(paramc, paramBoolean1, paramBoolean2, parama);
    b = paramContext;
    e = paramString1;
    f = paramString2;
    paramContext = new java/lang/ref/WeakReference;
    paramContext.<init>(paramb);
    c = paramContext;
    paramContext = new java/lang/ref/WeakReference;
    paramContext.<init>(parama1);
    d = paramContext;
  }
  
  static void a(Context paramContext, String paramString1, String paramString2, Contact paramContact, b paramb)
  {
    if (paramb != null)
    {
      String str = null;
      boolean bool1 = bj.c(paramString1);
      if (bool1)
      {
        if (paramContact != null)
        {
          paramContact = paramContact.r();
          if (paramContact != null) {
            str = paramContact.a();
          }
        }
        if (str == null)
        {
          paramString2 = aa.b(paramString1, paramString2);
          str = paramString2;
        }
      }
      boolean bool2 = TextUtils.isEmpty(str);
      if (!bool2) {
        paramString1 = str;
      }
      paramContext = f.a(paramContext);
      paramContext.a(paramString1, paramb);
    }
  }
  
  public final void a(Exception paramException, int paramInt)
  {
    j.b localb = (j.b)c.get();
    if (localb != null) {
      localb.a(paramException);
    }
  }
  
  public final boolean a(r paramr)
  {
    boolean bool = super.a(paramr);
    Object localObject = b;
    if (localObject != null)
    {
      localObject = b).b;
      if (localObject != null)
      {
        paramr = ((n)b).a();
        Context localContext = b;
        String str1 = e;
        String str2 = f;
        a(localContext, str1, str2, paramr, (b)localObject);
      }
    }
    return bool;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.search.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */