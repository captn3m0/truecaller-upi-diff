package com.truecaller.network.search;

import android.content.Context;
import com.truecaller.common.b.a;
import com.truecaller.common.h.aa;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.search.ContactDto;
import com.truecaller.search.ContactDto.Contact;
import com.truecaller.search.ContactDto.Pagination;
import com.truecaller.search.k;
import com.truecaller.search.l;
import e.d;
import e.r;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import okhttp3.ab;
import okhttp3.ad;
import okhttp3.ae;
import okhttp3.t;

final class i
  implements e.b
{
  private final e.b a;
  private final String b;
  private final boolean c;
  private final boolean d;
  private final int e;
  private final UUID f;
  
  i(e.b paramb, String paramString, boolean paramBoolean1, boolean paramBoolean2, int paramInt, UUID paramUUID)
  {
    a = paramb;
    b = paramString;
    c = paramBoolean1;
    d = paramBoolean2;
    e = paramInt;
    f = paramUUID;
  }
  
  public final void a(d paramd)
  {
    String[] arrayOfString = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(false, arrayOfString);
  }
  
  public final boolean a()
  {
    return a.a();
  }
  
  public final ab b()
  {
    return a.b();
  }
  
  public final r c()
  {
    Object localObject1 = a.c();
    Object localObject2 = a;
    boolean bool1 = ((ad)localObject2).c();
    Object localObject3 = null;
    int i = 1;
    if (bool1)
    {
      localObject2 = (ContactDto)b;
      if (localObject2 != null)
      {
        localObject4 = b;
        Object localObject5 = k.a((r)localObject1);
        boolean bool2 = c;
        boolean bool3 = d;
        ArrayList localArrayList = new java/util/ArrayList;
        localArrayList.<init>();
        long l = System.currentTimeMillis();
        Object localObject6 = data;
        Object localObject8;
        if (localObject6 != null)
        {
          localObject6 = data.iterator();
          Object localObject7;
          for (;;)
          {
            boolean bool4 = ((Iterator)localObject6).hasNext();
            if (!bool4) {
              break;
            }
            localObject7 = (ContactDto.Contact)((Iterator)localObject6).next();
            if (localObject7 != null)
            {
              g.a((ContactDto.Contact)localObject7, l, (String)localObject4, (String)localObject5);
              Contact localContact = new com/truecaller/data/entity/Contact;
              localContact.<init>((ContactDto.Contact)localObject7);
              localArrayList.add(localContact);
            }
          }
          if (bool2)
          {
            new String[1][0] = "Persisting search result before converting";
            localObject5 = new java/util/ArrayList;
            ((ArrayList)localObject5).<init>();
            localObject8 = new java/util/ArrayList;
            ((ArrayList)localObject8).<init>();
            l.a((List)localObject5, (List)localObject8, (ContactDto)localObject2);
            boolean bool5 = ((ArrayList)localObject5).isEmpty();
            if ((bool5) && (bool3))
            {
              Object localObject9 = new String[i];
              localObject7 = String.valueOf(localObject4);
              localObject6 = "Creating negative cache for ".concat((String)localObject7);
              localObject9[0] = localObject6;
              localObject9 = aa.b((String)localObject4);
              l.a((List)localObject5, (String)localObject4, (String)localObject9, l);
            }
            localObject4 = a.F();
            l.a((Context)localObject4, (ArrayList)localObject5, (List)localObject8);
          }
        }
        localObject4 = a.f;
        localObject5 = "tc-event-id";
        localObject4 = ((t)localObject4).a((String)localObject5);
        boolean bool6 = localArrayList.isEmpty();
        if (!bool6)
        {
          localObject10 = new String[i];
          localObject5 = new java/lang/StringBuilder;
          ((StringBuilder)localObject5).<init>("Network query successful, ");
          int j = localArrayList.size();
          ((StringBuilder)localObject5).append(j);
          localObject8 = " contacts returned";
          ((StringBuilder)localObject5).append((String)localObject8);
          localObject5 = ((StringBuilder)localObject5).toString();
          localObject10[0] = localObject5;
        }
        else
        {
          localObject3 = "Network query returned no result";
          new String[1][0] = localObject3;
        }
        localObject3 = new com/truecaller/network/search/n;
        localObject10 = pagination;
        localObject2 = campaigns;
        ((n)localObject3).<init>((String)localObject4, localArrayList, (ContactDto.Pagination)localObject10, (com.truecaller.ads.campaigns.b)localObject2);
        localObject1 = a;
        return r.a(localObject3, (ad)localObject1);
      }
    }
    localObject2 = new String[i];
    Object localObject10 = new java/lang/StringBuilder;
    ((StringBuilder)localObject10).<init>("Network query for ");
    Object localObject4 = b;
    ((StringBuilder)localObject10).append((String)localObject4);
    ((StringBuilder)localObject10).append(" failed");
    localObject10 = ((StringBuilder)localObject10).toString();
    localObject2[0] = localObject10;
    localObject2 = c;
    localObject1 = a;
    return r.a((ae)localObject2, (ad)localObject1);
  }
  
  public final void d()
  {
    String[] arrayOfString = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(false, arrayOfString);
  }
  
  public final e.b e()
  {
    i locali = new com/truecaller/network/search/i;
    e.b localb = a.e();
    String str = b;
    boolean bool1 = c;
    boolean bool2 = d;
    int i = e;
    UUID localUUID = f;
    locali.<init>(localb, str, bool1, bool2, i, localUUID);
    return locali;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.search.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */