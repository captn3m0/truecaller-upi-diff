package com.truecaller.network.c;

import c.g.b.k;
import c.n;
import c.x;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.common.h.af;
import com.truecaller.common.network.KnownDomain;
import com.truecaller.common.network.c;
import com.truecaller.common.network.e;
import com.truecaller.common.network.e.b;
import com.truecaller.log.d;
import com.truecaller.network.util.y;
import java.io.IOException;
import java.util.List;
import okhttp3.ab;
import okhttp3.ab.a;
import okhttp3.ad;
import okhttp3.u;
import okhttp3.u.a;
import okhttp3.v;
import okhttp3.v.a;

public final class a
  implements v
{
  private final com.truecaller.common.edge.a a;
  private final b b;
  private final c c;
  private final af d;
  private final boolean e;
  
  public a(com.truecaller.common.edge.a parama, b paramb, c paramc, af paramaf, boolean paramBoolean)
  {
    a = parama;
    b = paramb;
    c = paramc;
    d = paramaf;
    e = paramBoolean;
  }
  
  private final void a(String paramString, int paramInt)
  {
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("FetchEdgeLocations");
    paramString = locala.a("Context", "Interceptor").a("FinalResult", paramString).a("Attempts", paramInt).a();
    b localb = b;
    k.a(paramString, "event");
    localb.a(paramString);
  }
  
  public final ad intercept(v.a parama)
  {
    k.b(parama, "chain");
    parama.a().d();
    Object localObject1 = parama.a();
    Object localObject2 = ((ab)localObject1).a();
    Object localObject3 = "request.url()";
    k.a(localObject2, (String)localObject3);
    localObject2 = y.a((u)localObject2);
    if (localObject2 == null)
    {
      parama = parama.a((ab)localObject1);
      k.a(parama, "chain.proceed(request)");
      return parama;
    }
    boolean bool = e;
    Object localObject4 = null;
    if (bool)
    {
      localObject3 = a;
      localObject5 = this;
      try
      {
        localObject5 = (a)this;
        Object localObject6 = a;
        int j = ((com.truecaller.common.edge.a)localObject6).b();
        if (j == 0)
        {
          localObject6 = "Performing first edge locations request";
          new String[1][0] = localObject6;
          j = 0;
          localObject6 = null;
          for (;;)
          {
            int m = j + 1;
            int n = 3;
            if (j >= n) {
              break;
            }
            try
            {
              localObject6 = a;
              j = ((com.truecaller.common.edge.a)localObject6).a();
              if (j != 0) {
                localObject6 = "Success";
              } else {
                localObject6 = "OtherFailure";
              }
              ((a)localObject5).a((String)localObject6, m);
            }
            catch (IOException localIOException)
            {
              Object localObject7 = (Throwable)localIOException;
              d.a((Throwable)localObject7);
              if (m == n)
              {
                localObject7 = "Exception";
                ((a)localObject5).a((String)localObject7, m);
                break;
              }
              long l = 1000L;
              Thread.sleep(l);
              j = m;
            }
          }
        }
        localObject5 = x.a;
      }
      finally {}
    }
    localObject3 = d;
    k.a(localObject1, "request");
    Object localObject5 = com.truecaller.common.network.f.a((ab)localObject1);
    localObject3 = ((af)localObject3).b((e)localObject5);
    if (localObject3 != null)
    {
      localObject5 = a;
      localObject3 = a.getValue();
      localObject3 = ((com.truecaller.common.edge.a)localObject5).a((String)localObject3, (String)localObject2);
      if (localObject3 != null) {}
    }
    else
    {
      localObject3 = a;
      localObject5 = c;
      localObject3 = com.truecaller.common.edge.f.a((String)localObject2, (com.truecaller.common.edge.a)localObject3, (c)localObject5);
    }
    if (localObject3 == null)
    {
      parama = parama.a((ab)localObject1);
      k.a(parama, "chain.proceed(request)");
      return parama;
    }
    k.b(localObject3, "receiver$0");
    localObject3 = (CharSequence)localObject3;
    int i1 = 1;
    localObject5 = new char[i1];
    localObject5[0] = 58;
    int k = 2;
    localObject3 = c.n.m.a((CharSequence)localObject3, (char[])localObject5, k, k);
    localObject5 = new c/n;
    localObject4 = ((List)localObject3).get(0);
    localObject2 = (String)c.a.m.a((List)localObject3, i1);
    if (localObject2 != null)
    {
      localObject2 = c.n.m.b((String)localObject2);
    }
    else
    {
      i1 = 0;
      localObject2 = null;
    }
    ((n)localObject5).<init>(localObject4, localObject2);
    localObject2 = (String)a;
    localObject3 = (Integer)b;
    localObject2 = ((ab)localObject1).a().j().b((String)localObject2);
    localObject4 = "request.url().newBuilder().host(url)";
    k.a(localObject2, (String)localObject4);
    if (localObject3 != null)
    {
      int i = ((Integer)localObject3).intValue();
      ((u.a)localObject2).a(i);
    }
    localObject1 = ((ab)localObject1).e();
    localObject2 = ((u.a)localObject2).b();
    localObject1 = ((ab.a)localObject1).a((u)localObject2).a();
    parama = parama.a((ab)localObject1);
    k.a(parama, "chain.proceed(edgeRequest)");
    return parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.network.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */