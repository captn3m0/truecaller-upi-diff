package com.truecaller.push;

import c.g.b.k;
import com.truecaller.common.network.util.KnownEndpoints;
import e.b;

public final class h
{
  public static final h a;
  
  static
  {
    h localh = new com/truecaller/push/h;
    localh.<init>();
    a = localh;
  }
  
  public static final b a(PushIdDto paramPushIdDto)
  {
    k.b(paramPushIdDto, "token");
    return ((h.a)com.truecaller.common.network.util.h.a(KnownEndpoints.PUSHID, h.a.class)).a(paramPushIdDto);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.push.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */