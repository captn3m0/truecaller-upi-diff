package com.truecaller.push;

import android.content.Context;
import android.os.Bundle;
import c.g.b.k;
import c.m.o.a;
import com.facebook.appevents.g;
import com.google.gson.f;
import com.google.gson.o;
import com.google.gson.q;
import com.truecaller.common.g.a;
import com.truecaller.log.d;
import com.truecaller.network.notification.c.a.a;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.old.data.entity.Notification.NotificationState;
import com.truecaller.util.NotificationUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class c
  implements b
{
  private final List a;
  private final Context b;
  private final a c;
  private final g d;
  
  public c(Context paramContext, a parama, g paramg)
  {
    b = paramContext;
    c = parama;
    d = paramg;
    paramContext = new java/util/ArrayList;
    paramContext.<init>();
    paramContext = (List)paramContext;
    a = paramContext;
  }
  
  private static Notification a(Bundle paramBundle)
  {
    Object localObject1 = paramBundle.getString("e");
    paramBundle = paramBundle.getString("a");
    Object localObject2 = localObject1;
    localObject2 = (CharSequence)localObject1;
    int i = 0;
    String str1 = null;
    if (localObject2 != null)
    {
      j = ((CharSequence)localObject2).length();
      if (j != 0)
      {
        j = 0;
        localObject2 = null;
        break label57;
      }
    }
    int j = 1;
    label57:
    if (j != 0)
    {
      localObject2 = paramBundle;
      localObject2 = (CharSequence)paramBundle;
      if (localObject2 != null)
      {
        j = ((CharSequence)localObject2).length();
        if (j != 0) {}
      }
      else
      {
        i = 1;
      }
      if (i != 0) {}
    }
    else
    {
      try
      {
        localObject2 = new com/google/gson/o;
        ((o)localObject2).<init>();
        if (localObject1 != null)
        {
          str1 = "e";
          localObject1 = q.a((String)localObject1);
          String str2 = "JsonParser().parse(it)";
          k.a(localObject1, str2);
          localObject1 = ((com.google.gson.l)localObject1).i();
          localObject1 = (com.google.gson.l)localObject1;
          ((o)localObject2).a(str1, (com.google.gson.l)localObject1);
        }
        if (paramBundle != null)
        {
          localObject1 = "a";
          paramBundle = q.a(paramBundle);
          str1 = "JsonParser().parse(it)";
          k.a(paramBundle, str1);
          paramBundle = paramBundle.i();
          paramBundle = (com.google.gson.l)paramBundle;
          ((o)localObject2).a((String)localObject1, paramBundle);
        }
        paramBundle = new com/truecaller/old/data/entity/Notification;
        localObject1 = Notification.NotificationState.NEW;
        paramBundle.<init>((o)localObject2, (Notification.NotificationState)localObject1);
        return paramBundle;
      }
      catch (RuntimeException localRuntimeException)
      {
        paramBundle = (Throwable)localRuntimeException;
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        ((StringBuilder)localObject1).append(b.class);
        localObject2 = " asNotification - error while parsing notification";
        ((StringBuilder)localObject1).append((String)localObject2);
        localObject1 = ((StringBuilder)localObject1).toString();
        d.a(paramBundle, (String)localObject1);
      }
    }
    return null;
  }
  
  private void d()
  {
    Object localObject1 = c.a.m.n((Iterable)a);
    Object localObject2 = (c.g.a.b)c.a.a;
    localObject1 = c.m.l.c((c.m.i)localObject1, (c.g.a.b)localObject2);
    localObject2 = new org/json/JSONArray;
    ((JSONArray)localObject2).<init>();
    localObject1 = ((c.m.i)localObject1).a();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      Object localObject3 = (JSONObject)((Iterator)localObject1).next();
      localObject2 = ((JSONArray)localObject2).put(localObject3);
      localObject3 = "jsonArray.put(jsonObject)";
      k.a(localObject2, (String)localObject3);
    }
    k.a(localObject2, "payloads.asSequence()\n  …onArray.put(jsonObject) }");
    localObject1 = c;
    localObject2 = ((JSONArray)localObject2).toString();
    ((a)localObject1).a("payloads", (String)localObject2);
  }
  
  public final c.a.a a(Map paramMap)
  {
    c.a.a locala = null;
    if (paramMap != null)
    {
      Object localObject1 = (String)paramMap.get("c");
      String str = (String)paramMap.get("c.d");
      paramMap = (String)paramMap.get("c.o");
      Object localObject2 = localObject1;
      localObject2 = (CharSequence)localObject1;
      int i = 1;
      int j = 0;
      if (localObject2 != null)
      {
        k = ((CharSequence)localObject2).length();
        if (k != 0)
        {
          k = 0;
          localObject2 = null;
          break label92;
        }
      }
      int k = 1;
      label92:
      if (k == 0)
      {
        paramMap = new com/google/gson/f;
        paramMap.<init>();
        return (c.a.a)paramMap.a((String)localObject1, c.a.a.class);
      }
      localObject1 = str;
      localObject1 = (CharSequence)str;
      if (localObject1 != null)
      {
        m = ((CharSequence)localObject1).length();
        if (m != 0)
        {
          m = 0;
          localObject1 = null;
          break label156;
        }
      }
      int m = 1;
      label156:
      if (m != 0)
      {
        localObject1 = paramMap;
        localObject1 = (CharSequence)paramMap;
        if (localObject1 != null)
        {
          m = ((CharSequence)localObject1).length();
          if (m != 0) {
            i = 0;
          }
        }
        if (i != 0) {
          return null;
        }
      }
      locala = new com/truecaller/network/notification/c$a$a;
      locala.<init>();
      if (str != null)
      {
        localObject1 = c.n.m.b(str);
        if (localObject1 != null)
        {
          m = ((Integer)localObject1).intValue();
          break label238;
        }
      }
      m = 0;
      localObject1 = null;
      label238:
      a = m;
      if (paramMap != null)
      {
        paramMap = c.n.m.b(paramMap);
        if (paramMap != null) {
          j = paramMap.intValue();
        }
      }
      b = j;
      return locala;
    }
    return null;
  }
  
  public final void a()
  {
    Iterator localIterator = a.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Bundle localBundle = (Bundle)localIterator.next();
      g localg = d;
      localg.a(localBundle);
    }
  }
  
  public final void a(Bundle paramBundle, long paramLong)
  {
    int i = 1;
    Object localObject1 = new String[i];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    ((StringBuilder)localObject2).append(b.class);
    ((StringBuilder)localObject2).append(" onMessage with Intent");
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    if (paramBundle != null)
    {
      localObject1 = a;
      ((List)localObject1).add(paramBundle);
      d();
      paramBundle = a(paramBundle);
      if (paramBundle != null) {
        try
        {
          localObject1 = b;
          NotificationUtil.a(paramBundle, (Context)localObject1, paramLong);
        }
        catch (RuntimeException localRuntimeException)
        {
          paramBundle = (Throwable)localRuntimeException;
          Object localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>();
          ((StringBuilder)localObject3).append(b.class);
          String str = " onNotification - error while handling notification";
          ((StringBuilder)localObject3).append(str);
          localObject3 = ((StringBuilder)localObject3).toString();
          d.a(paramBundle, (String)localObject3);
        }
      }
    }
    NotificationUtil.b(b);
  }
  
  public final void b()
  {
    Object localObject1 = c;
    Object localObject2 = "payloads";
    localObject1 = ((a)localObject1).a((String)localObject2);
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "coreSettings.getString(P…SSAGE_PAYLOADS) ?: return");
    localObject2 = new org/json/JSONArray;
    ((JSONArray)localObject2).<init>((String)localObject1);
    int i = ((JSONArray)localObject2).length();
    localObject1 = c.a.m.n((Iterable)c.k.i.b(0, i));
    Collection localCollection = (Collection)a;
    localObject1 = ((c.m.i)localObject1).a();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      int j = ((Number)((Iterator)localObject1).next()).intValue();
      JSONObject localJSONObject = ((JSONArray)localObject2).getJSONObject(j);
      if (localJSONObject != null)
      {
        Object localObject3 = localJSONObject.keys();
        k.a(localObject3, "jsonObject.keys()");
        k.b(localObject3, "receiver$0");
        localObject4 = new c/m/o$a;
        ((o.a)localObject4).<init>((Iterator)localObject3);
        localObject3 = c.m.l.b((c.m.i)localObject4);
        localObject4 = new android/os/Bundle;
        ((Bundle)localObject4).<init>();
        localObject3 = ((c.m.i)localObject3).a();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject3).hasNext();
          if (!bool2) {
            break;
          }
          String str1 = (String)((Iterator)localObject3).next();
          String str2 = localJSONObject.getString(str1);
          ((Bundle)localObject4).putString(str1, str2);
        }
      }
      Object localObject4 = null;
      if (localObject4 != null) {
        localCollection.add(localObject4);
      }
    }
  }
  
  public final void c()
  {
    a.clear();
    c.d("payloads");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.push.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */