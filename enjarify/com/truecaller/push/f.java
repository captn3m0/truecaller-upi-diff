package com.truecaller.push;

import c.g.a.m;
import c.g.b.k;
import com.facebook.appevents.g;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.iid.FirebaseInstanceId;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.d;
import e.b;
import java.util.concurrent.ExecutionException;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class f
  implements e
{
  private final com.truecaller.i.e a;
  private final com.truecaller.common.account.r b;
  private final c.d.f c;
  
  public f(com.truecaller.i.e parame, com.truecaller.common.account.r paramr, c.d.f paramf)
  {
    a = parame;
    b = paramr;
    c = paramf;
  }
  
  private static void c(String paramString)
  {
    int i = 1;
    Object[] arrayOfObject = new Object[i];
    arrayOfObject[0] = paramString;
    com.truecaller.debug.log.a.a(arrayOfObject);
    new String[i][0] = paramString;
  }
  
  public final String a()
  {
    return a.a("fcmRegisteredOnServer");
  }
  
  public final boolean a(String paramString)
  {
    boolean bool = b();
    StringBuilder localStringBuilder = null;
    if (!bool) {
      return false;
    }
    String str;
    if (paramString == null) {
      str = c();
    } else {
      str = paramString;
    }
    Object localObject1 = str;
    localObject1 = (CharSequence)str;
    int i = 1;
    if (localObject1 != null)
    {
      j = ((CharSequence)localObject1).length();
      if (j != 0)
      {
        j = 0;
        localObject1 = null;
        break label77;
      }
    }
    int j = 1;
    label77:
    if (j != 0)
    {
      paramString = new java/lang/StringBuilder;
      paramString.<init>();
      str = e.class.getName();
      paramString.append(str);
      paramString.append(": push token is NULL");
      c(paramString.toString());
      paramString = new com/truecaller/push/a;
      paramString.<init>();
      d.a((Throwable)paramString);
      return false;
    }
    a.a("gcmRegistrationId", str);
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = e.class.getName();
    ((StringBuilder)localObject1).append((String)localObject2);
    ((StringBuilder)localObject1).append(": push token for registration: ");
    ((StringBuilder)localObject1).append(paramString);
    c(((StringBuilder)localObject1).toString());
    localObject1 = new com/truecaller/push/PushIdDto;
    ((PushIdDto)localObject1).<init>(str, i);
    localObject2 = null;
    try
    {
      localObject1 = h.a((PushIdDto)localObject1);
      localObject2 = ((b)localObject1).c();
    }
    catch (Exception localException)
    {
      localObject1 = (Throwable)localException;
      d.a((Throwable)localObject1);
    }
    catch (SecurityException localSecurityException)
    {
      localObject1 = (Throwable)localSecurityException;
      String[] arrayOfString = new String[0];
      AssertionUtil.shouldNeverHappen((Throwable)localObject1, arrayOfString);
    }
    if (localObject2 != null)
    {
      int k = ((e.r)localObject2).d();
      if (k == i)
      {
        localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localObject1 = e.class.getName();
        localStringBuilder.append((String)localObject1);
        localStringBuilder.append(": push token is registered: ");
        localStringBuilder.append(paramString);
        c(localStringBuilder.toString());
        a.a("fcmRegisteredOnServer", str);
        g.a(str);
        return i;
      }
    }
    return false;
  }
  
  public final void b(String paramString)
  {
    ag localag = (ag)bg.a;
    c.d.f localf = c;
    Object localObject = new com/truecaller/push/f$a;
    ((f.a)localObject).<init>(this, paramString, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.b(localag, localf, (m)localObject, 2);
  }
  
  public final boolean b()
  {
    return b.c();
  }
  
  public final String c()
  {
    Object localObject = FirebaseInstanceId.a();
    k.a(localObject, "FirebaseInstanceId.getInstance()");
    localObject = ((FirebaseInstanceId)localObject).e();
    String str = "FirebaseInstanceId.getInstance().instanceId";
    k.a(localObject, str);
    try
    {
      Tasks.a((Task)localObject);
    }
    catch (InterruptedException localInterruptedException)
    {
      localObject = Tasks.a((Exception)localInterruptedException);
      str = "Tasks.forException(e)";
      k.a(localObject, str);
    }
    catch (ExecutionException localExecutionException)
    {
      localObject = Tasks.a((Exception)localExecutionException);
      str = "Tasks.forException(e)";
      k.a(localObject, str);
    }
    boolean bool = ((Task)localObject).b();
    if (bool)
    {
      localObject = (com.google.firebase.iid.a)((Task)localObject).d();
      if (localObject != null) {
        return ((com.google.firebase.iid.a)localObject).a();
      }
      return null;
    }
    return a.a("gcmRegistrationId");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.push.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */