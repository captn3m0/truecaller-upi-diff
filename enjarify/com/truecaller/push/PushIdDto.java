package com.truecaller.push;

import c.g.b.k;

public final class PushIdDto
{
  private final int provider;
  private final String token;
  
  public PushIdDto(String paramString, int paramInt)
  {
    token = paramString;
    provider = paramInt;
  }
  
  public final String component1()
  {
    return token;
  }
  
  public final int component2()
  {
    return provider;
  }
  
  public final PushIdDto copy(String paramString, int paramInt)
  {
    k.b(paramString, "token");
    PushIdDto localPushIdDto = new com/truecaller/push/PushIdDto;
    localPushIdDto.<init>(paramString, paramInt);
    return localPushIdDto;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof PushIdDto;
      if (bool2)
      {
        paramObject = (PushIdDto)paramObject;
        String str1 = token;
        String str2 = token;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          int i = provider;
          int j = provider;
          if (i == j)
          {
            j = 1;
          }
          else
          {
            j = 0;
            paramObject = null;
          }
          if (j != 0) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int getProvider()
  {
    return provider;
  }
  
  public final String getToken()
  {
    return token;
  }
  
  public final int hashCode()
  {
    String str = token;
    int i;
    if (str != null)
    {
      i = str.hashCode();
    }
    else
    {
      i = 0;
      str = null;
    }
    i *= 31;
    int j = provider;
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("PushIdDto(token=");
    String str = token;
    localStringBuilder.append(str);
    localStringBuilder.append(", provider=");
    int i = provider;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.push.PushIdDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */