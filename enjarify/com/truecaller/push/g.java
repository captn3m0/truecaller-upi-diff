package com.truecaller.push;

import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private g(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static g a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    g localg = new com/truecaller/push/g;
    localg.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.push.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */