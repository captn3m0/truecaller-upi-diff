package com.truecaller.push;

import android.content.Context;
import c.g.b.k;
import com.facebook.appevents.g;

public abstract class i
{
  public static final i.a a;
  
  static
  {
    i.a locala = new com/truecaller/push/i$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public static final g a(Context paramContext)
  {
    k.b(paramContext, "context");
    paramContext = g.a(paramContext);
    k.a(paramContext, "AppEventsLogger.newLogger(context)");
    return paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.push.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */