package com.truecaller.push;

import android.content.Context;
import android.os.Bundle;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.background.e.a;
import java.util.concurrent.TimeUnit;

public final class PushIdRegistrationTask
  extends PersistentBackgroundTask
{
  public e a;
  
  public PushIdRegistrationTask()
  {
    TrueApp localTrueApp = TrueApp.y();
    k.a(localTrueApp, "TrueApp.getApp()");
    localTrueApp.a().a(this);
  }
  
  public final int a()
  {
    return 10024;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    paramBundle = "context";
    k.b(paramContext, paramBundle);
    paramContext = a;
    if (paramContext == null)
    {
      paramBundle = "pushIdManager";
      k.a(paramBundle);
    }
    paramBundle = null;
    boolean bool = paramContext.a(null);
    if (bool) {
      return PersistentBackgroundTask.RunResult.Success;
    }
    return PersistentBackgroundTask.RunResult.FailedRetry;
  }
  
  public final boolean a(Context paramContext)
  {
    String str = "serviceContext";
    k.b(paramContext, str);
    paramContext = a;
    if (paramContext == null)
    {
      str = "pushIdManager";
      k.a(str);
    }
    return paramContext.b();
  }
  
  public final com.truecaller.common.background.e b()
  {
    Object localObject = new com/truecaller/common/background/e$a;
    int i = 1;
    ((e.a)localObject).<init>(i);
    TimeUnit localTimeUnit = TimeUnit.DAYS;
    long l = 1L;
    localObject = ((e.a)localObject).a(l, localTimeUnit);
    localTimeUnit = TimeUnit.HOURS;
    localObject = ((e.a)localObject).b(2, localTimeUnit);
    localTimeUnit = TimeUnit.HOURS;
    localObject = ((e.a)localObject).c(l, localTimeUnit).a(i).b();
    k.a(localObject, "TaskConfiguration.Builde…YPE_ANY)\n        .build()");
    return (com.truecaller.common.background.e)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.push.PushIdRegistrationTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */