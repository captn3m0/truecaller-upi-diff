package com.truecaller.tracking.events;

import org.apache.a.d;
import org.apache.a.d.f;
import org.apache.a.d.f;

public final class l$a
  extends f
{
  private CharSequence c;
  private CharSequence d;
  private CharSequence e;
  private CharSequence f;
  private boolean g;
  private boolean h;
  private CharSequence i;
  
  private l$a()
  {
    super(locald);
  }
  
  public final a a(CharSequence paramCharSequence)
  {
    a(a[0], paramCharSequence);
    c = paramCharSequence;
    b[0] = true;
    return this;
  }
  
  public final a a(boolean paramBoolean)
  {
    Object localObject = a;
    int j = 4;
    localObject = localObject[j];
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    a((d.f)localObject, localBoolean);
    g = paramBoolean;
    b[j] = true;
    return this;
  }
  
  public final l a()
  {
    try
    {
      l locall = new com/truecaller/tracking/events/l;
      locall.<init>();
      localObject = b;
      int j = 0;
      int k = localObject[0];
      if (k != 0)
      {
        localObject = c;
      }
      else
      {
        localObject = a;
        localObject = localObject[0];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      b = ((CharSequence)localObject);
      localObject = b;
      j = 1;
      k = localObject[j];
      if (k != 0)
      {
        localObject = d;
      }
      else
      {
        localObject = a;
        localObject = localObject[j];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      c = ((CharSequence)localObject);
      localObject = b;
      j = 2;
      k = localObject[j];
      if (k != 0)
      {
        localObject = e;
      }
      else
      {
        localObject = a;
        localObject = localObject[j];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      d = ((CharSequence)localObject);
      localObject = b;
      j = 3;
      k = localObject[j];
      if (k != 0)
      {
        localObject = f;
      }
      else
      {
        localObject = a;
        localObject = localObject[j];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      e = ((CharSequence)localObject);
      localObject = b;
      j = 4;
      k = localObject[j];
      boolean bool1;
      if (k != 0)
      {
        bool1 = g;
      }
      else
      {
        localObject = a;
        localObject = localObject[j];
        localObject = a((d.f)localObject);
        localObject = (Boolean)localObject;
        bool1 = ((Boolean)localObject).booleanValue();
      }
      f = bool1;
      localObject = b;
      j = 5;
      int m = localObject[j];
      boolean bool2;
      if (m != 0)
      {
        bool2 = h;
      }
      else
      {
        localObject = a;
        localObject = localObject[j];
        localObject = a((d.f)localObject);
        localObject = (Boolean)localObject;
        bool2 = ((Boolean)localObject).booleanValue();
      }
      g = bool2;
      localObject = b;
      j = 6;
      int n = localObject[j];
      if (n != 0)
      {
        localObject = i;
      }
      else
      {
        localObject = a;
        localObject = localObject[j];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      h = ((CharSequence)localObject);
      return locall;
    }
    catch (Exception localException)
    {
      Object localObject = new org/apache/a/a;
      ((org.apache.a.a)localObject).<init>(localException);
      throw ((Throwable)localObject);
    }
  }
  
  public final a b(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int j = 1;
    a(arrayOff[j], paramCharSequence);
    d = paramCharSequence;
    b[j] = j;
    return this;
  }
  
  public final a b(boolean paramBoolean)
  {
    Object localObject = a;
    int j = 5;
    localObject = localObject[j];
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    a((d.f)localObject, localBoolean);
    h = paramBoolean;
    b[j] = true;
    return this;
  }
  
  public final a c(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int j = 2;
    a(arrayOff[j], paramCharSequence);
    e = paramCharSequence;
    b[j] = true;
    return this;
  }
  
  public final a d(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int j = 3;
    a(arrayOff[j], paramCharSequence);
    f = paramCharSequence;
    b[j] = true;
    return this;
  }
  
  public final a e(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int j = 6;
    a(arrayOff[j], paramCharSequence);
    i = paramCharSequence;
    b[j] = true;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.l.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */