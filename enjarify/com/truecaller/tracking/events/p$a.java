package com.truecaller.tracking.events;

import org.apache.a.d;
import org.apache.a.d.f;
import org.apache.a.d.f;

public final class p$a
  extends f
{
  private CharSequence c;
  private CharSequence d;
  private CharSequence e;
  private long f;
  private long g;
  private Boolean h;
  
  private p$a()
  {
    super(locald);
  }
  
  public final a a(long paramLong)
  {
    Object localObject = a;
    int i = 3;
    localObject = localObject[i];
    Long localLong = Long.valueOf(paramLong);
    a((d.f)localObject, localLong);
    f = paramLong;
    b[i] = true;
    return this;
  }
  
  public final a a(Boolean paramBoolean)
  {
    d.f[] arrayOff = a;
    int i = 5;
    a(arrayOff[i], paramBoolean);
    h = paramBoolean;
    b[i] = true;
    return this;
  }
  
  public final a a(CharSequence paramCharSequence)
  {
    a(a[0], paramCharSequence);
    c = paramCharSequence;
    b[0] = true;
    return this;
  }
  
  public final p a()
  {
    try
    {
      p localp = new com/truecaller/tracking/events/p;
      localp.<init>();
      localObject = b;
      int i = 0;
      int j = localObject[0];
      if (j != 0)
      {
        localObject = c;
      }
      else
      {
        localObject = a;
        localObject = localObject[0];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      b = ((CharSequence)localObject);
      localObject = b;
      i = 1;
      j = localObject[i];
      if (j != 0)
      {
        localObject = d;
      }
      else
      {
        localObject = a;
        localObject = localObject[i];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      c = ((CharSequence)localObject);
      localObject = b;
      i = 2;
      j = localObject[i];
      if (j != 0)
      {
        localObject = e;
      }
      else
      {
        localObject = a;
        localObject = localObject[i];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      d = ((CharSequence)localObject);
      localObject = b;
      i = 3;
      j = localObject[i];
      long l;
      if (j != 0)
      {
        l = f;
      }
      else
      {
        localObject = a;
        localObject = localObject[i];
        localObject = a((d.f)localObject);
        localObject = (Long)localObject;
        l = ((Long)localObject).longValue();
      }
      e = l;
      localObject = b;
      i = 4;
      j = localObject[i];
      if (j != 0)
      {
        l = g;
      }
      else
      {
        localObject = a;
        localObject = localObject[i];
        localObject = a((d.f)localObject);
        localObject = (Long)localObject;
        l = ((Long)localObject).longValue();
      }
      f = l;
      localObject = b;
      i = 5;
      j = localObject[i];
      if (j != 0)
      {
        localObject = h;
      }
      else
      {
        localObject = a;
        localObject = localObject[i];
        localObject = a((d.f)localObject);
        localObject = (Boolean)localObject;
      }
      g = ((Boolean)localObject);
      return localp;
    }
    catch (Exception localException)
    {
      Object localObject = new org/apache/a/a;
      ((org.apache.a.a)localObject).<init>(localException);
      throw ((Throwable)localObject);
    }
  }
  
  public final a b(long paramLong)
  {
    Object localObject = a;
    int i = 4;
    localObject = localObject[i];
    Long localLong = Long.valueOf(paramLong);
    a((d.f)localObject, localLong);
    g = paramLong;
    b[i] = true;
    return this;
  }
  
  public final a b(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int i = 1;
    a(arrayOff[i], paramCharSequence);
    d = paramCharSequence;
    b[i] = i;
    return this;
  }
  
  public final a c(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int i = 2;
    a(arrayOff[i], paramCharSequence);
    e = paramCharSequence;
    b[i] = true;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.p.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */