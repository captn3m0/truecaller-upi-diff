package com.truecaller.tracking.events;

import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class h
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public CharSequence b;
  public boolean c;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"AppCallerIdNotificationReceived\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"pushId\",\"type\":\"string\"},{\"name\":\"ignored\",\"type\":\"boolean\"}]}");
  }
  
  public static h.a b()
  {
    h.a locala = new com/truecaller/tracking/events/h$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 1: 
      return Boolean.valueOf(c);
    }
    return b;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 1: 
      paramInt = ((Boolean)paramObject).booleanValue();
      c = paramInt;
      return;
    }
    paramObject = (CharSequence)paramObject;
    b = ((CharSequence)paramObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */