package com.truecaller.tracking.events;

import org.apache.a.d;
import org.apache.a.d.f;
import org.apache.a.d.f;

public final class n$a
  extends f
{
  private CharSequence c;
  private CharSequence d;
  private CharSequence e;
  private CharSequence f;
  private boolean g;
  private CharSequence h;
  private CharSequence i;
  private int j;
  
  private n$a()
  {
    super(locald);
  }
  
  public final a a(int paramInt)
  {
    Object localObject = a;
    int k = 7;
    localObject = localObject[k];
    Integer localInteger = Integer.valueOf(paramInt);
    a((d.f)localObject, localInteger);
    j = paramInt;
    b[k] = true;
    return this;
  }
  
  public final a a(CharSequence paramCharSequence)
  {
    a(a[0], paramCharSequence);
    c = paramCharSequence;
    b[0] = true;
    return this;
  }
  
  public final a a(boolean paramBoolean)
  {
    Object localObject = a;
    int k = 4;
    localObject = localObject[k];
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    a((d.f)localObject, localBoolean);
    g = paramBoolean;
    b[k] = true;
    return this;
  }
  
  public final n a()
  {
    try
    {
      n localn = new com/truecaller/tracking/events/n;
      localn.<init>();
      localObject = b;
      int k = 0;
      int m = localObject[0];
      if (m != 0)
      {
        localObject = c;
      }
      else
      {
        localObject = a;
        localObject = localObject[0];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      b = ((CharSequence)localObject);
      localObject = b;
      k = 1;
      m = localObject[k];
      if (m != 0)
      {
        localObject = d;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      c = ((CharSequence)localObject);
      localObject = b;
      k = 2;
      m = localObject[k];
      if (m != 0)
      {
        localObject = e;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      d = ((CharSequence)localObject);
      localObject = b;
      k = 3;
      m = localObject[k];
      if (m != 0)
      {
        localObject = f;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      e = ((CharSequence)localObject);
      localObject = b;
      k = 4;
      m = localObject[k];
      boolean bool;
      if (m != 0)
      {
        bool = g;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (Boolean)localObject;
        bool = ((Boolean)localObject).booleanValue();
      }
      f = bool;
      localObject = b;
      k = 5;
      int n = localObject[k];
      if (n != 0)
      {
        localObject = h;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      g = ((CharSequence)localObject);
      localObject = b;
      k = 6;
      n = localObject[k];
      if (n != 0)
      {
        localObject = i;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      h = ((CharSequence)localObject);
      localObject = b;
      k = 7;
      n = localObject[k];
      if (n != 0)
      {
        n = j;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (Integer)localObject;
        n = ((Integer)localObject).intValue();
      }
      i = n;
      return localn;
    }
    catch (Exception localException)
    {
      Object localObject = new org/apache/a/a;
      ((org.apache.a.a)localObject).<init>(localException);
      throw ((Throwable)localObject);
    }
  }
  
  public final a b(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int k = 1;
    a(arrayOff[k], paramCharSequence);
    d = paramCharSequence;
    b[k] = k;
    return this;
  }
  
  public final a c(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int k = 2;
    a(arrayOff[k], paramCharSequence);
    e = paramCharSequence;
    b[k] = true;
    return this;
  }
  
  public final a d(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int k = 3;
    a(arrayOff[k], paramCharSequence);
    f = paramCharSequence;
    b[k] = true;
    return this;
  }
  
  public final a e(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int k = 5;
    a(arrayOff[k], paramCharSequence);
    h = paramCharSequence;
    b[k] = true;
    return this;
  }
  
  public final a f(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int k = 6;
    a(arrayOff[k], paramCharSequence);
    i = paramCharSequence;
    b[k] = true;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.n.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */