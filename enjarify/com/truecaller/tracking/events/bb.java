package com.truecaller.tracking.events;

import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class bb
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public int b;
  public int c;
  public int d;
  public Integer e;
  public Integer f;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"SmsContentMetaData\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"numNumbers\",\"type\":\"int\"},{\"name\":\"numUrls\",\"type\":\"int\"},{\"name\":\"numWords\",\"type\":\"int\"},{\"name\":\"numUnigram\",\"type\":[\"null\",\"int\"]},{\"name\":\"numBigram\",\"type\":[\"null\",\"int\"]}]}");
  }
  
  public static bb.a g()
  {
    bb.a locala = new com/truecaller/tracking/events/bb$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 4: 
      return f;
    case 3: 
      return e;
    case 2: 
      return Integer.valueOf(d);
    case 1: 
      return Integer.valueOf(c);
    }
    return Integer.valueOf(b);
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 4: 
      paramObject = (Integer)paramObject;
      f = ((Integer)paramObject);
      return;
    case 3: 
      paramObject = (Integer)paramObject;
      e = ((Integer)paramObject);
      return;
    case 2: 
      paramInt = ((Integer)paramObject).intValue();
      d = paramInt;
      return;
    case 1: 
      paramInt = ((Integer)paramObject).intValue();
      c = paramInt;
      return;
    }
    paramInt = ((Integer)paramObject).intValue();
    b = paramInt;
  }
  
  public final Integer b()
  {
    return Integer.valueOf(b);
  }
  
  public final Integer c()
  {
    return Integer.valueOf(c);
  }
  
  public final Integer d()
  {
    return Integer.valueOf(d);
  }
  
  public final Integer e()
  {
    return e;
  }
  
  public final Integer f()
  {
    return f;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.bb
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */