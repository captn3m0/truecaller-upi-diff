package com.truecaller.tracking.events;

import java.util.List;
import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class z
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public CharSequence b;
  public CharSequence c;
  public CharSequence d;
  public CharSequence e;
  public List f;
  public List g;
  public boolean h;
  public boolean i;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"AppSearchV3\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"searchId\",\"type\":\"string\"},{\"name\":\"requestId\",\"type\":[\"null\",\"string\"]},{\"name\":\"type\",\"type\":\"string\"},{\"name\":\"source\",\"type\":\"string\"},{\"name\":\"searchEntities\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"SearchEntity\",\"fields\":[{\"name\":\"term\",\"type\":\"string\"},{\"name\":\"noServerSearchReason\",\"type\":[\"null\",\"string\"]},{\"name\":\"normalizedPhoneNumber\",\"type\":[\"null\",\"string\"]},{\"name\":\"tags\",\"type\":[\"null\",{\"type\":\"record\",\"name\":\"TagsServed\",\"fields\":[{\"name\":\"serverTagsReceived\",\"type\":[\"null\",{\"type\":\"array\",\"items\":\"string\"}],\"default\":null},{\"name\":\"manualTagsAvailable\",\"type\":[\"null\",{\"type\":\"array\",\"items\":\"string\"}],\"default\":null},{\"name\":\"shownTags\",\"type\":[\"null\",{\"type\":\"array\",\"items\":\"string\"}],\"default\":null}]}]},{\"name\":\"contactInfo\",\"type\":[\"null\",{\"type\":\"record\",\"name\":\"ContactInfo\",\"fields\":[{\"name\":\"inPhonebook\",\"type\":\"boolean\"},{\"name\":\"hasName\",\"type\":\"boolean\"},{\"name\":\"inUserSpammerList\",\"type\":[\"null\",\"boolean\"]},{\"name\":\"inTopSpammerList\",\"type\":[\"null\",\"boolean\"]},{\"name\":\"inUserWhiteList\",\"type\":[\"null\",\"boolean\"]},{\"name\":\"spammerFromServer\",\"type\":[\"null\",\"boolean\"]},{\"name\":\"spamScore\",\"type\":[\"null\",\"int\"]},{\"name\":\"hasPushData\",\"type\":[\"null\",\"boolean\"],\"default\":null}]}]}]}}},{\"name\":\"correlationIds\",\"type\":[\"null\",{\"type\":\"array\",\"items\":\"string\"}]},{\"name\":\"serverSearchAttempted\",\"type\":\"boolean\"},{\"name\":\"serverSearchResponseSuccess\",\"type\":\"boolean\"}]}");
  }
  
  public static z.a b()
  {
    z.a locala = new com/truecaller/tracking/events/z$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 7: 
      return Boolean.valueOf(i);
    case 6: 
      return Boolean.valueOf(h);
    case 5: 
      return g;
    case 4: 
      return f;
    case 3: 
      return e;
    case 2: 
      return d;
    case 1: 
      return c;
    }
    return b;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 7: 
      paramInt = ((Boolean)paramObject).booleanValue();
      i = paramInt;
      return;
    case 6: 
      paramInt = ((Boolean)paramObject).booleanValue();
      h = paramInt;
      return;
    case 5: 
      paramObject = (List)paramObject;
      g = ((List)paramObject);
      return;
    case 4: 
      paramObject = (List)paramObject;
      f = ((List)paramObject);
      return;
    case 3: 
      paramObject = (CharSequence)paramObject;
      e = ((CharSequence)paramObject);
      return;
    case 2: 
      paramObject = (CharSequence)paramObject;
      d = ((CharSequence)paramObject);
      return;
    case 1: 
      paramObject = (CharSequence)paramObject;
      c = ((CharSequence)paramObject);
      return;
    }
    paramObject = (CharSequence)paramObject;
    b = ((CharSequence)paramObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */