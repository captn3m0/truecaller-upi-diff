package com.truecaller.tracking.events;

import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class l
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public CharSequence b;
  public CharSequence c;
  public CharSequence d;
  public CharSequence e;
  public boolean f;
  public boolean g;
  public CharSequence h;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"AppFlashAction\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"messageId\",\"type\":\"string\"},{\"name\":\"senderId\",\"type\":\"string\"},{\"name\":\"threadId\",\"type\":\"string\"},{\"name\":\"contentType\",\"type\":\"string\"},{\"name\":\"fromPhonebook\",\"type\":\"boolean\"},{\"name\":\"missed\",\"type\":\"boolean\"},{\"name\":\"userAction\",\"type\":[\"null\",\"string\"]}]}");
  }
  
  public static l.a b()
  {
    l.a locala = new com/truecaller/tracking/events/l$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 6: 
      return h;
    case 5: 
      return Boolean.valueOf(g);
    case 4: 
      return Boolean.valueOf(f);
    case 3: 
      return e;
    case 2: 
      return d;
    case 1: 
      return c;
    }
    return b;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 6: 
      paramObject = (CharSequence)paramObject;
      h = ((CharSequence)paramObject);
      return;
    case 5: 
      paramInt = ((Boolean)paramObject).booleanValue();
      g = paramInt;
      return;
    case 4: 
      paramInt = ((Boolean)paramObject).booleanValue();
      f = paramInt;
      return;
    case 3: 
      paramObject = (CharSequence)paramObject;
      e = ((CharSequence)paramObject);
      return;
    case 2: 
      paramObject = (CharSequence)paramObject;
      d = ((CharSequence)paramObject);
      return;
    case 1: 
      paramObject = (CharSequence)paramObject;
      c = ((CharSequence)paramObject);
      return;
    }
    paramObject = (CharSequence)paramObject;
    b = ((CharSequence)paramObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */