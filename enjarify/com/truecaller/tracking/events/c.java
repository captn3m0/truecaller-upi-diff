package com.truecaller.tracking.events;

import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class c
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public CharSequence b;
  public CharSequence c;
  public CharSequence d;
  public CharSequence e;
  public CharSequence f;
  public at g;
  public Integer h;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"AppBlockingAction\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"term\",\"type\":\"string\"},{\"name\":\"type\",\"type\":\"string\"},{\"name\":\"normalizedPhoneNumber\",\"type\":[\"null\",\"string\"]},{\"name\":\"action\",\"type\":\"string\"},{\"name\":\"context\",\"type\":\"string\"},{\"name\":\"numberStateBeforeAction\",\"type\":[\"null\",{\"type\":\"record\",\"name\":\"NumberStateBeforeAction\",\"fields\":[{\"name\":\"inTopSpammerList\",\"type\":\"boolean\"},{\"name\":\"inUserSpammerList\",\"type\":\"boolean\"},{\"name\":\"inUserWhiteList\",\"type\":\"boolean\"},{\"name\":\"spammerFromServer\",\"type\":\"boolean\"}]}]},{\"name\":\"spamCountShown\",\"type\":[\"null\",\"int\"]}]}");
  }
  
  public static c.a b()
  {
    c.a locala = new com/truecaller/tracking/events/c$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 6: 
      return h;
    case 5: 
      return g;
    case 4: 
      return f;
    case 3: 
      return e;
    case 2: 
      return d;
    case 1: 
      return c;
    }
    return b;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 6: 
      paramObject = (Integer)paramObject;
      h = ((Integer)paramObject);
      return;
    case 5: 
      paramObject = (at)paramObject;
      g = ((at)paramObject);
      return;
    case 4: 
      paramObject = (CharSequence)paramObject;
      f = ((CharSequence)paramObject);
      return;
    case 3: 
      paramObject = (CharSequence)paramObject;
      e = ((CharSequence)paramObject);
      return;
    case 2: 
      paramObject = (CharSequence)paramObject;
      d = ((CharSequence)paramObject);
      return;
    case 1: 
      paramObject = (CharSequence)paramObject;
      c = ((CharSequence)paramObject);
      return;
    }
    paramObject = (CharSequence)paramObject;
    b = ((CharSequence)paramObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */