package com.truecaller.tracking.events;

import java.util.List;
import java.util.Map;
import org.apache.a.d;
import org.apache.a.d.f;
import org.apache.a.d.f;

public final class ac$a
  extends f
{
  private List c;
  private CharSequence d;
  private CharSequence e;
  private int f;
  private int g;
  private boolean h;
  private Map i;
  
  private ac$a()
  {
    super(locald);
  }
  
  public final a a()
  {
    Object localObject = a;
    int j = 3;
    localObject = localObject[j];
    int k = 1;
    Integer localInteger = Integer.valueOf(k);
    a((d.f)localObject, localInteger);
    f = k;
    b[j] = k;
    return this;
  }
  
  public final a a(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int j = 1;
    a(arrayOff[j], paramCharSequence);
    d = paramCharSequence;
    b[j] = j;
    return this;
  }
  
  public final a a(List paramList)
  {
    a(a[0], paramList);
    c = paramList;
    b[0] = true;
    return this;
  }
  
  public final a a(Map paramMap)
  {
    d.f[] arrayOff = a;
    int j = 6;
    a(arrayOff[j], paramMap);
    i = paramMap;
    b[j] = true;
    return this;
  }
  
  public final a a(boolean paramBoolean)
  {
    Object localObject = a;
    int j = 5;
    localObject = localObject[j];
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    a((d.f)localObject, localBoolean);
    h = paramBoolean;
    b[j] = true;
    return this;
  }
  
  public final a b()
  {
    Object localObject = a;
    int j = 4;
    localObject = localObject[j];
    int k = 10008;
    Integer localInteger = Integer.valueOf(k);
    a((d.f)localObject, localInteger);
    g = k;
    b[j] = true;
    return this;
  }
  
  public final a b(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int j = 2;
    a(arrayOff[j], paramCharSequence);
    e = paramCharSequence;
    b[j] = true;
    return this;
  }
  
  public final ac c()
  {
    try
    {
      ac localac = new com/truecaller/tracking/events/ac;
      localac.<init>();
      localObject = b;
      int j = 0;
      int k = localObject[0];
      if (k != 0)
      {
        localObject = c;
      }
      else
      {
        localObject = a;
        localObject = localObject[0];
        localObject = a((d.f)localObject);
        localObject = (List)localObject;
      }
      b = ((List)localObject);
      localObject = b;
      j = 1;
      k = localObject[j];
      if (k != 0)
      {
        localObject = d;
      }
      else
      {
        localObject = a;
        localObject = localObject[j];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      c = ((CharSequence)localObject);
      localObject = b;
      j = 2;
      k = localObject[j];
      if (k != 0)
      {
        localObject = e;
      }
      else
      {
        localObject = a;
        localObject = localObject[j];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      d = ((CharSequence)localObject);
      localObject = b;
      j = 3;
      k = localObject[j];
      if (k != 0)
      {
        k = f;
      }
      else
      {
        localObject = a;
        localObject = localObject[j];
        localObject = a((d.f)localObject);
        localObject = (Integer)localObject;
        k = ((Integer)localObject).intValue();
      }
      e = k;
      localObject = b;
      j = 4;
      k = localObject[j];
      if (k != 0)
      {
        k = g;
      }
      else
      {
        localObject = a;
        localObject = localObject[j];
        localObject = a((d.f)localObject);
        localObject = (Integer)localObject;
        k = ((Integer)localObject).intValue();
      }
      f = k;
      localObject = b;
      j = 5;
      k = localObject[j];
      boolean bool;
      if (k != 0)
      {
        bool = h;
      }
      else
      {
        localObject = a;
        localObject = localObject[j];
        localObject = a((d.f)localObject);
        localObject = (Boolean)localObject;
        bool = ((Boolean)localObject).booleanValue();
      }
      g = bool;
      localObject = b;
      j = 6;
      int m = localObject[j];
      if (m != 0)
      {
        localObject = i;
      }
      else
      {
        localObject = a;
        localObject = localObject[j];
        localObject = a((d.f)localObject);
        localObject = (Map)localObject;
      }
      h = ((Map)localObject);
      return localac;
    }
    catch (Exception localException)
    {
      Object localObject = new org/apache/a/a;
      ((org.apache.a.a)localObject).<init>(localException);
      throw ((Throwable)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.ac.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */