package com.truecaller.tracking.events;

import java.nio.ByteBuffer;
import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public class EventRecordVersionedV2
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public int b;
  public CharSequence c;
  public ByteBuffer d;
  public ByteBuffer e;
  public int f;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"EventRecordVersionedV2\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"schemaId\",\"type\":\"int\"},{\"name\":\"name\",\"type\":\"string\"},{\"name\":\"header\",\"type\":\"bytes\"},{\"name\":\"body\",\"type\":\"bytes\"},{\"name\":\"headerVersion\",\"type\":\"int\",\"default\":2}]}");
  }
  
  public static org.apache.a.d b()
  {
    return a;
  }
  
  public static EventRecordVersionedV2.a c()
  {
    EventRecordVersionedV2.a locala = new com/truecaller/tracking/events/EventRecordVersionedV2$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 4: 
      return Integer.valueOf(f);
    case 3: 
      return e;
    case 2: 
      return d;
    case 1: 
      return c;
    }
    return Integer.valueOf(b);
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 4: 
      paramInt = ((Integer)paramObject).intValue();
      f = paramInt;
      return;
    case 3: 
      paramObject = (ByteBuffer)paramObject;
      e = ((ByteBuffer)paramObject);
      return;
    case 2: 
      paramObject = (ByteBuffer)paramObject;
      d = ((ByteBuffer)paramObject);
      return;
    case 1: 
      paramObject = (CharSequence)paramObject;
      c = ((CharSequence)paramObject);
      return;
    }
    paramInt = ((Integer)paramObject).intValue();
    b = paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.EventRecordVersionedV2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */