package com.truecaller.tracking.events;

import org.apache.a.d;
import org.apache.a.d.f;
import org.apache.a.d.f;

public final class m$a
  extends f
{
  private CharSequence c;
  private CharSequence d;
  private CharSequence e;
  private CharSequence f;
  private boolean g;
  
  private m$a()
  {
    super(locald);
  }
  
  public final a a(CharSequence paramCharSequence)
  {
    a(a[0], paramCharSequence);
    c = paramCharSequence;
    b[0] = true;
    return this;
  }
  
  public final a a(boolean paramBoolean)
  {
    Object localObject = a;
    int i = 4;
    localObject = localObject[i];
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    a((d.f)localObject, localBoolean);
    g = paramBoolean;
    b[i] = true;
    return this;
  }
  
  public final m a()
  {
    try
    {
      m localm = new com/truecaller/tracking/events/m;
      localm.<init>();
      localObject = b;
      int i = 0;
      int j = localObject[0];
      if (j != 0)
      {
        localObject = c;
      }
      else
      {
        localObject = a;
        localObject = localObject[0];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      b = ((CharSequence)localObject);
      localObject = b;
      i = 1;
      j = localObject[i];
      if (j != 0)
      {
        localObject = d;
      }
      else
      {
        localObject = a;
        localObject = localObject[i];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      c = ((CharSequence)localObject);
      localObject = b;
      i = 2;
      j = localObject[i];
      if (j != 0)
      {
        localObject = e;
      }
      else
      {
        localObject = a;
        localObject = localObject[i];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      d = ((CharSequence)localObject);
      localObject = b;
      i = 3;
      j = localObject[i];
      if (j != 0)
      {
        localObject = f;
      }
      else
      {
        localObject = a;
        localObject = localObject[i];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      e = ((CharSequence)localObject);
      localObject = b;
      i = 4;
      j = localObject[i];
      boolean bool;
      if (j != 0)
      {
        bool = g;
      }
      else
      {
        localObject = a;
        localObject = localObject[i];
        localObject = a((d.f)localObject);
        localObject = (Boolean)localObject;
        bool = ((Boolean)localObject).booleanValue();
      }
      f = bool;
      return localm;
    }
    catch (Exception localException)
    {
      Object localObject = new org/apache/a/a;
      ((org.apache.a.a)localObject).<init>(localException);
      throw ((Throwable)localObject);
    }
  }
  
  public final a b(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int i = 1;
    a(arrayOff[i], paramCharSequence);
    d = paramCharSequence;
    b[i] = i;
    return this;
  }
  
  public final a c(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int i = 2;
    a(arrayOff[i], paramCharSequence);
    e = paramCharSequence;
    b[i] = true;
    return this;
  }
  
  public final a d(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int i = 3;
    a(arrayOff[i], paramCharSequence);
    f = paramCharSequence;
    b[i] = true;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.m.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */