package com.truecaller.tracking.events;

import org.apache.a.d.q;
import org.apache.a.d.e;

public final class ak
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public long b;
  public long c;
  public CharSequence d;
  public CharSequence e;
  public a f;
  public CharSequence g;
  public CharSequence h;
  public am i;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"ClientHeaderV2\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"sequenceNumber\",\"type\":\"long\"},{\"name\":\"timestamp\",\"type\":\"long\"},{\"name\":\"registerId\",\"type\":[\"null\",\"string\"]},{\"name\":\"clientId\",\"type\":\"string\"},{\"name\":\"app\",\"type\":{\"type\":\"record\",\"name\":\"App\",\"fields\":[{\"name\":\"name\",\"type\":\"string\"},{\"name\":\"version\",\"type\":\"string\"},{\"name\":\"buildName\",\"type\":\"string\"}]}},{\"name\":\"connection\",\"type\":\"string\"},{\"name\":\"operator\",\"type\":\"string\"},{\"name\":\"coordinates\",\"type\":[\"null\",{\"type\":\"record\",\"name\":\"Coordinates\",\"fields\":[{\"name\":\"latitude\",\"type\":\"float\"},{\"name\":\"longitude\",\"type\":\"float\"},{\"name\":\"ages\",\"type\":\"long\"}]}],\"default\":null}]}");
  }
  
  public static ak.a b()
  {
    ak.a locala = new com/truecaller/tracking/events/ak$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      org.apache.a.a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 7: 
      return i;
    case 6: 
      return h;
    case 5: 
      return g;
    case 4: 
      return f;
    case 3: 
      return e;
    case 2: 
      return d;
    case 1: 
      return Long.valueOf(c);
    }
    return Long.valueOf(b);
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      org.apache.a.a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 7: 
      paramObject = (am)paramObject;
      i = ((am)paramObject);
      return;
    case 6: 
      paramObject = (CharSequence)paramObject;
      h = ((CharSequence)paramObject);
      return;
    case 5: 
      paramObject = (CharSequence)paramObject;
      g = ((CharSequence)paramObject);
      return;
    case 4: 
      paramObject = (a)paramObject;
      f = ((a)paramObject);
      return;
    case 3: 
      paramObject = (CharSequence)paramObject;
      e = ((CharSequence)paramObject);
      return;
    case 2: 
      paramObject = (CharSequence)paramObject;
      d = ((CharSequence)paramObject);
      return;
    case 1: 
      l = ((Long)paramObject).longValue();
      c = l;
      return;
    }
    long l = ((Long)paramObject).longValue();
    b = l;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.ak
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */