package com.truecaller.tracking.events;

import org.apache.a.a;
import org.apache.a.d.q;

public final class e
  extends org.apache.a.d.e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public CharSequence b;
  public CharSequence c;
  public CharSequence d;
  public CharSequence e;
  public boolean f;
  public long g;
  public CharSequence h;
  public boolean i;
  public CharSequence j;
  public long k;
  public CharSequence l;
  public bc m;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"AppCallFinishedV2\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"normalizedPhoneNumber\",\"type\":\"string\"},{\"name\":\"searchCountryCode\",\"type\":\"string\"},{\"name\":\"direction\",\"type\":\"string\"},{\"name\":\"status\",\"type\":\"string\"},{\"name\":\"afterCallShown\",\"type\":\"boolean\"},{\"name\":\"callDuration\",\"type\":\"long\"},{\"name\":\"searchResult\",\"type\":\"string\"},{\"name\":\"identifiedAsSpam\",\"type\":\"boolean\"},{\"name\":\"blockingAction\",\"type\":\"string\"},{\"name\":\"latency\",\"type\":\"long\"},{\"name\":\"callId\",\"type\":[\"null\",\"string\"]},{\"name\":\"tags\",\"type\":[\"null\",{\"type\":\"record\",\"name\":\"TagsServed\",\"fields\":[{\"name\":\"serverTagsReceived\",\"type\":[\"null\",{\"type\":\"array\",\"items\":\"string\"}],\"default\":null},{\"name\":\"manualTagsAvailable\",\"type\":[\"null\",{\"type\":\"array\",\"items\":\"string\"}],\"default\":null},{\"name\":\"shownTags\",\"type\":[\"null\",{\"type\":\"array\",\"items\":\"string\"}],\"default\":null}]}]}]}");
  }
  
  public static e.a b()
  {
    e.a locala = new com/truecaller/tracking/events/e$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 11: 
      return m;
    case 10: 
      return l;
    case 9: 
      return Long.valueOf(k);
    case 8: 
      return j;
    case 7: 
      return Boolean.valueOf(i);
    case 6: 
      return h;
    case 5: 
      return Long.valueOf(g);
    case 4: 
      return Boolean.valueOf(f);
    case 3: 
      return e;
    case 2: 
      return d;
    case 1: 
      return c;
    }
    return b;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    long l1;
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 11: 
      paramObject = (bc)paramObject;
      m = ((bc)paramObject);
      return;
    case 10: 
      paramObject = (CharSequence)paramObject;
      l = ((CharSequence)paramObject);
      return;
    case 9: 
      l1 = ((Long)paramObject).longValue();
      k = l1;
      return;
    case 8: 
      paramObject = (CharSequence)paramObject;
      j = ((CharSequence)paramObject);
      return;
    case 7: 
      paramInt = ((Boolean)paramObject).booleanValue();
      i = paramInt;
      return;
    case 6: 
      paramObject = (CharSequence)paramObject;
      h = ((CharSequence)paramObject);
      return;
    case 5: 
      l1 = ((Long)paramObject).longValue();
      g = l1;
      return;
    case 4: 
      paramInt = ((Boolean)paramObject).booleanValue();
      f = paramInt;
      return;
    case 3: 
      paramObject = (CharSequence)paramObject;
      e = ((CharSequence)paramObject);
      return;
    case 2: 
      paramObject = (CharSequence)paramObject;
      d = ((CharSequence)paramObject);
      return;
    case 1: 
      paramObject = (CharSequence)paramObject;
      c = ((CharSequence)paramObject);
      return;
    }
    paramObject = (CharSequence)paramObject;
    b = ((CharSequence)paramObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */