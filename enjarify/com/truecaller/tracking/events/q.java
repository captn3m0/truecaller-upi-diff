package com.truecaller.tracking.events;

import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class q
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public CharSequence b;
  public CharSequence c;
  public CharSequence d;
  public CharSequence e;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"AppIMGroupInvite\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"groupId\",\"type\":\"string\",\"doc\":\"Group's IM id\"},{\"name\":\"senderId\",\"type\":\"string\",\"doc\":\"IM id of sender\"},{\"name\":\"receiverId\",\"type\":\"string\",\"doc\":\"IM id of receiver\"},{\"name\":\"action\",\"type\":\"string\",\"doc\":\"possible values send|receive|accept|decline\"}]}");
  }
  
  public static q.a b()
  {
    q.a locala = new com/truecaller/tracking/events/q$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 3: 
      return e;
    case 2: 
      return d;
    case 1: 
      return c;
    }
    return b;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 3: 
      paramObject = (CharSequence)paramObject;
      e = ((CharSequence)paramObject);
      return;
    case 2: 
      paramObject = (CharSequence)paramObject;
      d = ((CharSequence)paramObject);
      return;
    case 1: 
      paramObject = (CharSequence)paramObject;
      c = ((CharSequence)paramObject);
      return;
    }
    paramObject = (CharSequence)paramObject;
    b = ((CharSequence)paramObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */