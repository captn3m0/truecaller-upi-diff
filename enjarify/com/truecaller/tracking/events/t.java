package com.truecaller.tracking.events;

import java.util.List;
import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class t
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public List b;
  public List c;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"AppInstalledPackagesV2\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"installedPackages\",\"type\":[\"null\",{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"PackageDetails\",\"fields\":[{\"name\":\"appName\",\"type\":\"string\"},{\"name\":\"versionId\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"versionCode\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"installedDate\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"updatedDate\",\"type\":[\"null\",\"string\"],\"default\":null}]}}],\"doc\":\"list of packageName values on user's device\",\"default\":null},{\"name\":\"uninstalledPackages\",\"type\":[\"null\",{\"type\":\"array\",\"items\":\"string\"}],\"doc\":\"list of uninstalled packageName values on user's device\",\"default\":null}]}");
  }
  
  public static t.a b()
  {
    t.a locala = new com/truecaller/tracking/events/t$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 1: 
      return c;
    }
    return b;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 1: 
      paramObject = (List)paramObject;
      c = ((List)paramObject);
      return;
    }
    paramObject = (List)paramObject;
    b = ((List)paramObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */