package com.truecaller.tracking.events;

import java.nio.ByteBuffer;
import org.apache.a.d;
import org.apache.a.d.f;
import org.apache.a.d.f;

public final class EventRecordVersionedV2$a
  extends f
{
  private int c;
  private CharSequence d;
  private ByteBuffer e;
  private ByteBuffer f;
  private int g;
  
  private EventRecordVersionedV2$a()
  {
    super(locald);
  }
  
  public final a a()
  {
    d.f localf = a[0];
    int i = 112;
    Integer localInteger = Integer.valueOf(i);
    a(localf, localInteger);
    c = i;
    b[0] = true;
    return this;
  }
  
  public final a a(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int i = 1;
    a(arrayOff[i], paramCharSequence);
    d = paramCharSequence;
    b[i] = i;
    return this;
  }
  
  public final a a(ByteBuffer paramByteBuffer)
  {
    d.f[] arrayOff = a;
    int i = 2;
    a(arrayOff[i], paramByteBuffer);
    e = paramByteBuffer;
    b[i] = true;
    return this;
  }
  
  public final a b()
  {
    Object localObject = a;
    int i = 4;
    localObject = localObject[i];
    int j = 2;
    Integer localInteger = Integer.valueOf(j);
    a((d.f)localObject, localInteger);
    g = j;
    b[i] = true;
    return this;
  }
  
  public final a b(ByteBuffer paramByteBuffer)
  {
    d.f[] arrayOff = a;
    int i = 3;
    a(arrayOff[i], paramByteBuffer);
    f = paramByteBuffer;
    b[i] = true;
    return this;
  }
  
  public final EventRecordVersionedV2 c()
  {
    try
    {
      EventRecordVersionedV2 localEventRecordVersionedV2 = new com/truecaller/tracking/events/EventRecordVersionedV2;
      localEventRecordVersionedV2.<init>();
      localObject = b;
      int i = 0;
      int j = localObject[0];
      if (j != 0)
      {
        j = c;
      }
      else
      {
        localObject = a;
        localObject = localObject[0];
        localObject = a((d.f)localObject);
        localObject = (Integer)localObject;
        j = ((Integer)localObject).intValue();
      }
      b = j;
      localObject = b;
      i = 1;
      j = localObject[i];
      if (j != 0)
      {
        localObject = d;
      }
      else
      {
        localObject = a;
        localObject = localObject[i];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      c = ((CharSequence)localObject);
      localObject = b;
      i = 2;
      j = localObject[i];
      if (j != 0)
      {
        localObject = e;
      }
      else
      {
        localObject = a;
        localObject = localObject[i];
        localObject = a((d.f)localObject);
        localObject = (ByteBuffer)localObject;
      }
      d = ((ByteBuffer)localObject);
      localObject = b;
      i = 3;
      j = localObject[i];
      if (j != 0)
      {
        localObject = f;
      }
      else
      {
        localObject = a;
        localObject = localObject[i];
        localObject = a((d.f)localObject);
        localObject = (ByteBuffer)localObject;
      }
      e = ((ByteBuffer)localObject);
      localObject = b;
      i = 4;
      j = localObject[i];
      if (j != 0)
      {
        j = g;
      }
      else
      {
        localObject = a;
        localObject = localObject[i];
        localObject = a((d.f)localObject);
        localObject = (Integer)localObject;
        j = ((Integer)localObject).intValue();
      }
      f = j;
      return localEventRecordVersionedV2;
    }
    catch (Exception localException)
    {
      Object localObject = new org/apache/a/a;
      ((org.apache.a.a)localObject).<init>(localException);
      throw ((Throwable)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.EventRecordVersionedV2.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */