package com.truecaller.tracking.events;

import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class p
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public CharSequence b;
  public CharSequence c;
  public CharSequence d;
  public long e;
  public long f;
  public Boolean g;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"AppHttpCall\",\"namespace\":\"com.truecaller.tracking.events\",\"doc\":\"* For tracking timing information in all types of Search Http calls.\",\"fields\":[{\"name\":\"domain\",\"type\":\"string\",\"doc\":\"Server that receives the http call request\"},{\"name\":\"type\",\"type\":\"string\",\"doc\":\"Type of call: e.g. dns, connectStart, requestBody\"},{\"name\":\"sessionId\",\"type\":\"string\",\"doc\":\"uuid linked to an active http call connection\"},{\"name\":\"startTimestamp\",\"type\":\"long\",\"doc\":\"Start time of call\"},{\"name\":\"duration\",\"type\":\"long\",\"doc\":\"Duration of call\"},{\"name\":\"status\",\"type\":[\"null\",\"boolean\"],\"doc\":\"Status of call, success or failure\",\"default\":null}]}");
  }
  
  public static p.a b()
  {
    p.a locala = new com/truecaller/tracking/events/p$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 5: 
      return g;
    case 4: 
      return Long.valueOf(f);
    case 3: 
      return Long.valueOf(e);
    case 2: 
      return d;
    case 1: 
      return c;
    }
    return b;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    long l;
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 5: 
      paramObject = (Boolean)paramObject;
      g = ((Boolean)paramObject);
      return;
    case 4: 
      l = ((Long)paramObject).longValue();
      f = l;
      return;
    case 3: 
      l = ((Long)paramObject).longValue();
      e = l;
      return;
    case 2: 
      paramObject = (CharSequence)paramObject;
      d = ((CharSequence)paramObject);
      return;
    case 1: 
      paramObject = (CharSequence)paramObject;
      c = ((CharSequence)paramObject);
      return;
    }
    paramObject = (CharSequence)paramObject;
    b = ((CharSequence)paramObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */