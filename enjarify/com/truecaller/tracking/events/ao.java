package com.truecaller.tracking.events;

import java.util.Map;
import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class ao
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public CharSequence b;
  public CharSequence c;
  public Map d;
  public Map e;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"GenericAnalyticsEvent\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"eventType\",\"type\":\"string\"},{\"name\":\"sessionId\",\"type\":[\"null\",\"string\"],\"default\":null},{\"name\":\"properties\",\"type\":[\"null\",{\"type\":\"map\",\"values\":\"string\"}],\"default\":null},{\"name\":\"measures\",\"type\":[\"null\",{\"type\":\"map\",\"values\":\"double\"}],\"default\":null}]}");
  }
  
  public static ao.a b()
  {
    ao.a locala = new com/truecaller/tracking/events/ao$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 3: 
      return e;
    case 2: 
      return d;
    case 1: 
      return c;
    }
    return b;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 3: 
      paramObject = (Map)paramObject;
      e = ((Map)paramObject);
      return;
    case 2: 
      paramObject = (Map)paramObject;
      d = ((Map)paramObject);
      return;
    case 1: 
      paramObject = (CharSequence)paramObject;
      c = ((CharSequence)paramObject);
      return;
    }
    paramObject = (CharSequence)paramObject;
    b = ((CharSequence)paramObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.ao
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */