package com.truecaller.tracking.events;

import org.apache.a.d;
import org.apache.a.d.f;
import org.apache.a.d.f;

public final class ak$a
  extends f
{
  private long c;
  private long d;
  private CharSequence e;
  private CharSequence f;
  private a g;
  private CharSequence h;
  private CharSequence i;
  private am j;
  
  private ak$a()
  {
    super(locald);
  }
  
  public final a a(long paramLong)
  {
    d.f localf = a[0];
    Long localLong = Long.valueOf(paramLong);
    a(localf, localLong);
    c = paramLong;
    b[0] = true;
    return this;
  }
  
  public final a a(a parama)
  {
    d.f[] arrayOff = a;
    int k = 4;
    a(arrayOff[k], parama);
    g = parama;
    b[k] = true;
    return this;
  }
  
  public final a a(am paramam)
  {
    d.f[] arrayOff = a;
    int k = 7;
    a(arrayOff[k], paramam);
    j = paramam;
    b[k] = true;
    return this;
  }
  
  public final a a(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int k = 2;
    a(arrayOff[k], paramCharSequence);
    e = paramCharSequence;
    b[k] = true;
    return this;
  }
  
  public final ak a()
  {
    try
    {
      ak localak = new com/truecaller/tracking/events/ak;
      localak.<init>();
      localObject = b;
      int k = 0;
      int m = localObject[0];
      long l;
      if (m != 0)
      {
        l = c;
      }
      else
      {
        localObject = a;
        localObject = localObject[0];
        localObject = a((d.f)localObject);
        localObject = (Long)localObject;
        l = ((Long)localObject).longValue();
      }
      b = l;
      localObject = b;
      k = 1;
      m = localObject[k];
      if (m != 0)
      {
        l = d;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (Long)localObject;
        l = ((Long)localObject).longValue();
      }
      c = l;
      localObject = b;
      k = 2;
      m = localObject[k];
      if (m != 0)
      {
        localObject = e;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      d = ((CharSequence)localObject);
      localObject = b;
      k = 3;
      m = localObject[k];
      if (m != 0)
      {
        localObject = f;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      e = ((CharSequence)localObject);
      localObject = b;
      k = 4;
      m = localObject[k];
      if (m != 0)
      {
        localObject = g;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (a)localObject;
      }
      f = ((a)localObject);
      localObject = b;
      k = 5;
      m = localObject[k];
      if (m != 0)
      {
        localObject = h;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      g = ((CharSequence)localObject);
      localObject = b;
      k = 6;
      m = localObject[k];
      if (m != 0)
      {
        localObject = i;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      h = ((CharSequence)localObject);
      localObject = b;
      k = 7;
      m = localObject[k];
      if (m != 0)
      {
        localObject = j;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (am)localObject;
      }
      i = ((am)localObject);
      return localak;
    }
    catch (Exception localException)
    {
      Object localObject = new org/apache/a/a;
      ((org.apache.a.a)localObject).<init>(localException);
      throw ((Throwable)localObject);
    }
  }
  
  public final a b(long paramLong)
  {
    Object localObject = a;
    int k = 1;
    localObject = localObject[k];
    Long localLong = Long.valueOf(paramLong);
    a((d.f)localObject, localLong);
    d = paramLong;
    b[k] = k;
    return this;
  }
  
  public final a b(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int k = 3;
    a(arrayOff[k], paramCharSequence);
    f = paramCharSequence;
    b[k] = true;
    return this;
  }
  
  public final a c(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int k = 5;
    a(arrayOff[k], paramCharSequence);
    h = paramCharSequence;
    b[k] = true;
    return this;
  }
  
  public final a d(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int k = 6;
    a(arrayOff[k], paramCharSequence);
    i = paramCharSequence;
    b[k] = true;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.ak.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */