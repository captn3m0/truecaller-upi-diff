package com.truecaller.tracking.events;

import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class am
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public float b;
  public float c;
  public long d;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"Coordinates\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"latitude\",\"type\":\"float\"},{\"name\":\"longitude\",\"type\":\"float\"},{\"name\":\"ages\",\"type\":\"long\"}]}");
  }
  
  public static am.a b()
  {
    am.a locala = new com/truecaller/tracking/events/am$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 2: 
      return Long.valueOf(d);
    case 1: 
      return Float.valueOf(c);
    }
    return Float.valueOf(b);
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 2: 
      long l = ((Long)paramObject).longValue();
      d = l;
      return;
    case 1: 
      f = ((Float)paramObject).floatValue();
      c = f;
      return;
    }
    float f = ((Float)paramObject).floatValue();
    b = f;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.am
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */