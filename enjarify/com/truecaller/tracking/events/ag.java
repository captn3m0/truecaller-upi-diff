package com.truecaller.tracking.events;

import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class ag
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public long b;
  public CharSequence c;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"AppVoipCallFinished\",\"namespace\":\"com.truecaller.tracking.events\",\"doc\":\"* Track successful call lengths, by channel name.\",\"fields\":[{\"name\":\"duration\",\"type\":\"long\",\"doc\":\"* The length of the call, in milliseconds.\"},{\"name\":\"channel\",\"type\":\"string\",\"doc\":\"* The name of the channel. Used to cross-reference calls.\"}]}");
  }
  
  public static ag.a b()
  {
    ag.a locala = new com/truecaller/tracking/events/ag$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 1: 
      return c;
    }
    return Long.valueOf(b);
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 1: 
      paramObject = (CharSequence)paramObject;
      c = ((CharSequence)paramObject);
      return;
    }
    long l = ((Long)paramObject).longValue();
    b = l;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */