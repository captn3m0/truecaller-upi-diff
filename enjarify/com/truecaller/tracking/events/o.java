package com.truecaller.tracking.events;

import java.util.List;
import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class o
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public CharSequence b;
  public an c;
  public au d;
  public as e;
  public aq f;
  public ap g;
  public ar h;
  public aj i;
  public int j;
  public List k;
  public CharSequence l;
  public aw m;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"AppHeartBeat\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"state\",\"type\":\"string\"},{\"name\":\"deviceInfo\",\"type\":{\"type\":\"record\",\"name\":\"DeviceInfo\",\"fields\":[{\"name\":\"manufacturer\",\"type\":\"string\"},{\"name\":\"model\",\"type\":\"string\"},{\"name\":\"imei\",\"type\":[\"null\",\"string\"]},{\"name\":\"screenHeight\",\"type\":\"int\"},{\"name\":\"screenWidth\",\"type\":\"int\"},{\"name\":\"density\",\"type\":\"int\"}]}},{\"name\":\"os\",\"type\":{\"type\":\"record\",\"name\":\"OS\",\"fields\":[{\"name\":\"name\",\"type\":\"string\"},{\"name\":\"version\",\"type\":\"string\"}]}},{\"name\":\"network\",\"type\":{\"type\":\"record\",\"name\":\"Network\",\"fields\":[{\"name\":\"connection\",\"type\":\"string\"},{\"name\":\"operator\",\"type\":\"string\"},{\"name\":\"ip\",\"type\":[\"null\",{\"type\":\"array\",\"items\":\"string\"}],\"default\":null}]}},{\"name\":\"language\",\"type\":{\"type\":\"record\",\"name\":\"Language\",\"fields\":[{\"name\":\"appLanguage\",\"type\":\"string\"},{\"name\":\"appT9Language\",\"type\":\"string\"},{\"name\":\"deviceLanguage\",\"type\":\"string\"}]}},{\"name\":\"gsmCellInfo\",\"type\":[\"null\",{\"type\":\"record\",\"name\":\"GsmCellInfo\",\"fields\":[{\"name\":\"mcc\",\"type\":\"int\"},{\"name\":\"mnc\",\"type\":\"int\"},{\"name\":\"cid\",\"type\":\"int\"},{\"name\":\"lac\",\"type\":\"int\"}]}],\"default\":null},{\"name\":\"lteCellInfo\",\"type\":[\"null\",{\"type\":\"record\",\"name\":\"LteCellInfo\",\"fields\":[{\"name\":\"mcc\",\"type\":\"int\"},{\"name\":\"mnc\",\"type\":\"int\"},{\"name\":\"ci\",\"type\":\"int\"},{\"name\":\"tac\",\"type\":\"int\"}]}],\"default\":null},{\"name\":\"cdmaCellInfo\",\"type\":[\"null\",{\"type\":\"record\",\"name\":\"CdmaCellInfo\",\"fields\":[{\"name\":\"latitude\",\"type\":\"float\"},{\"name\":\"longitude\",\"type\":\"float\"}]}],\"default\":null},{\"name\":\"simSlots\",\"type\":\"int\"},{\"name\":\"simInfo\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"SimInfo\",\"fields\":[{\"name\":\"mcc\",\"type\":\"int\"},{\"name\":\"mnc\",\"type\":\"int\"},{\"name\":\"msin\",\"type\":[\"null\",\"string\"]},{\"name\":\"isActive\",\"type\":\"boolean\"},{\"name\":\"operator\",\"type\":\"string\"},{\"name\":\"normalizedPhoneNumber\",\"type\":[\"null\",\"string\"]}]}}},{\"name\":\"adId\",\"type\":[\"null\",\"string\"]},{\"name\":\"packageInfo\",\"type\":[\"null\",{\"type\":\"record\",\"name\":\"PackageInfo\",\"fields\":[{\"name\":\"installerPackage\",\"type\":\"string\",\"doc\":\"Package name of the application that installed Truecaller\"},{\"name\":\"preloadPartnerName\",\"type\":\"string\",\"doc\":\"Name of the OEM partner (only for preloads, null otherwise)\"}]}],\"default\":null}]}");
  }
  
  public static o.a b()
  {
    o.a locala = new com/truecaller/tracking/events/o$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 11: 
      return m;
    case 10: 
      return l;
    case 9: 
      return k;
    case 8: 
      return Integer.valueOf(j);
    case 7: 
      return i;
    case 6: 
      return h;
    case 5: 
      return g;
    case 4: 
      return f;
    case 3: 
      return e;
    case 2: 
      return d;
    case 1: 
      return c;
    }
    return b;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 11: 
      paramObject = (aw)paramObject;
      m = ((aw)paramObject);
      return;
    case 10: 
      paramObject = (CharSequence)paramObject;
      l = ((CharSequence)paramObject);
      return;
    case 9: 
      paramObject = (List)paramObject;
      k = ((List)paramObject);
      return;
    case 8: 
      paramInt = ((Integer)paramObject).intValue();
      j = paramInt;
      return;
    case 7: 
      paramObject = (aj)paramObject;
      i = ((aj)paramObject);
      return;
    case 6: 
      paramObject = (ar)paramObject;
      h = ((ar)paramObject);
      return;
    case 5: 
      paramObject = (ap)paramObject;
      g = ((ap)paramObject);
      return;
    case 4: 
      paramObject = (aq)paramObject;
      f = ((aq)paramObject);
      return;
    case 3: 
      paramObject = (as)paramObject;
      e = ((as)paramObject);
      return;
    case 2: 
      paramObject = (au)paramObject;
      d = ((au)paramObject);
      return;
    case 1: 
      paramObject = (an)paramObject;
      c = ((an)paramObject);
      return;
    }
    paramObject = (CharSequence)paramObject;
    b = ((CharSequence)paramObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */