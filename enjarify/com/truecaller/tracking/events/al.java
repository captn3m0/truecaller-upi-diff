package com.truecaller.tracking.events;

import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class al
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public boolean b;
  public boolean c;
  public Boolean d;
  public Boolean e;
  public Boolean f;
  public Boolean g;
  public Integer h;
  public Boolean i;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"ContactInfo\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"inPhonebook\",\"type\":\"boolean\"},{\"name\":\"hasName\",\"type\":\"boolean\"},{\"name\":\"inUserSpammerList\",\"type\":[\"null\",\"boolean\"]},{\"name\":\"inTopSpammerList\",\"type\":[\"null\",\"boolean\"]},{\"name\":\"inUserWhiteList\",\"type\":[\"null\",\"boolean\"]},{\"name\":\"spammerFromServer\",\"type\":[\"null\",\"boolean\"]},{\"name\":\"spamScore\",\"type\":[\"null\",\"int\"]},{\"name\":\"hasPushData\",\"type\":[\"null\",\"boolean\"],\"default\":null}]}");
  }
  
  public static al.a b()
  {
    al.a locala = new com/truecaller/tracking/events/al$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 7: 
      return i;
    case 6: 
      return h;
    case 5: 
      return g;
    case 4: 
      return f;
    case 3: 
      return e;
    case 2: 
      return d;
    case 1: 
      return Boolean.valueOf(c);
    }
    return Boolean.valueOf(b);
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 7: 
      paramObject = (Boolean)paramObject;
      i = ((Boolean)paramObject);
      return;
    case 6: 
      paramObject = (Integer)paramObject;
      h = ((Integer)paramObject);
      return;
    case 5: 
      paramObject = (Boolean)paramObject;
      g = ((Boolean)paramObject);
      return;
    case 4: 
      paramObject = (Boolean)paramObject;
      f = ((Boolean)paramObject);
      return;
    case 3: 
      paramObject = (Boolean)paramObject;
      e = ((Boolean)paramObject);
      return;
    case 2: 
      paramObject = (Boolean)paramObject;
      d = ((Boolean)paramObject);
      return;
    case 1: 
      paramInt = ((Boolean)paramObject).booleanValue();
      c = paramInt;
      return;
    }
    paramInt = ((Boolean)paramObject).booleanValue();
    b = paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.al
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */