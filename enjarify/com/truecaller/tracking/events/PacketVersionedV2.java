package com.truecaller.tracking.events;

import java.util.List;
import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public class PacketVersionedV2
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public List b;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"PacketVersionedV2\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"events\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"EventRecordVersionedV2\",\"fields\":[{\"name\":\"schemaId\",\"type\":\"int\"},{\"name\":\"name\",\"type\":\"string\"},{\"name\":\"header\",\"type\":\"bytes\"},{\"name\":\"body\",\"type\":\"bytes\"},{\"name\":\"headerVersion\",\"type\":\"int\",\"default\":2}]}}}]}");
  }
  
  public static PacketVersionedV2.a b()
  {
    PacketVersionedV2.a locala = new com/truecaller/tracking/events/PacketVersionedV2$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    if (paramInt == 0) {
      return b;
    }
    a locala = new org/apache/a/a;
    locala.<init>("Bad index");
    throw locala;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    if (paramInt == 0)
    {
      paramObject = (List)paramObject;
      b = ((List)paramObject);
      return;
    }
    a locala = new org/apache/a/a;
    locala.<init>("Bad index");
    throw locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.PacketVersionedV2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */