package com.truecaller.tracking.events;

import java.util.List;
import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class ad
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public List b;
  public CharSequence c;
  public CharSequence d;
  public ba e;
  public bb f;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"AppSmsCategorizerReclassify\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"participants\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"Participant\",\"fields\":[{\"name\":\"type\",\"type\":\"string\"},{\"name\":\"id\",\"type\":\"string\"},{\"name\":\"info\",\"type\":{\"type\":\"record\",\"name\":\"ContactInfo\",\"fields\":[{\"name\":\"inPhonebook\",\"type\":\"boolean\"},{\"name\":\"hasName\",\"type\":\"boolean\"},{\"name\":\"inUserSpammerList\",\"type\":[\"null\",\"boolean\"]},{\"name\":\"inTopSpammerList\",\"type\":[\"null\",\"boolean\"]},{\"name\":\"inUserWhiteList\",\"type\":[\"null\",\"boolean\"]},{\"name\":\"spammerFromServer\",\"type\":[\"null\",\"boolean\"]},{\"name\":\"spamScore\",\"type\":[\"null\",\"int\"]},{\"name\":\"hasPushData\",\"type\":[\"null\",\"boolean\"],\"default\":null}]}}]}}},{\"name\":\"oldCategory\",\"type\":\"string\"},{\"name\":\"newCategory\",\"type\":\"string\"},{\"name\":\"classifier\",\"type\":{\"type\":\"record\",\"name\":\"SmsCategorizerModel\",\"fields\":[{\"name\":\"model\",\"type\":\"string\"},{\"name\":\"version\",\"type\":\"int\"}]}},{\"name\":\"content\",\"type\":{\"type\":\"record\",\"name\":\"SmsContentMetaData\",\"fields\":[{\"name\":\"numNumbers\",\"type\":\"int\"},{\"name\":\"numUrls\",\"type\":\"int\"},{\"name\":\"numWords\",\"type\":\"int\"},{\"name\":\"numUnigram\",\"type\":[\"null\",\"int\"]},{\"name\":\"numBigram\",\"type\":[\"null\",\"int\"]}]}}]}");
  }
  
  public static ad.a b()
  {
    ad.a locala = new com/truecaller/tracking/events/ad$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 4: 
      return f;
    case 3: 
      return e;
    case 2: 
      return d;
    case 1: 
      return c;
    }
    return b;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 4: 
      paramObject = (bb)paramObject;
      f = ((bb)paramObject);
      return;
    case 3: 
      paramObject = (ba)paramObject;
      e = ((ba)paramObject);
      return;
    case 2: 
      paramObject = (CharSequence)paramObject;
      d = ((CharSequence)paramObject);
      return;
    case 1: 
      paramObject = (CharSequence)paramObject;
      c = ((CharSequence)paramObject);
      return;
    }
    paramObject = (List)paramObject;
    b = ((List)paramObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */