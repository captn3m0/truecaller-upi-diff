package com.truecaller.tracking.events;

import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class n
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public CharSequence b;
  public CharSequence c;
  public CharSequence d;
  public CharSequence e;
  public boolean f;
  public CharSequence g;
  public CharSequence h;
  public int i;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"AppFlashInitiated\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"messageId\",\"type\":\"string\"},{\"name\":\"receiverId\",\"type\":\"string\"},{\"name\":\"context\",\"type\":\"string\"},{\"name\":\"contentType\",\"type\":\"string\"},{\"name\":\"fromPhonebook\",\"type\":\"boolean\"},{\"name\":\"replyId\",\"type\":[\"null\",\"string\"]},{\"name\":\"threadId\",\"type\":\"string\"},{\"name\":\"threadLength\",\"type\":\"int\"}]}");
  }
  
  public static n.a b()
  {
    n.a locala = new com/truecaller/tracking/events/n$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 7: 
      return Integer.valueOf(i);
    case 6: 
      return h;
    case 5: 
      return g;
    case 4: 
      return Boolean.valueOf(f);
    case 3: 
      return e;
    case 2: 
      return d;
    case 1: 
      return c;
    }
    return b;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 7: 
      paramInt = ((Integer)paramObject).intValue();
      i = paramInt;
      return;
    case 6: 
      paramObject = (CharSequence)paramObject;
      h = ((CharSequence)paramObject);
      return;
    case 5: 
      paramObject = (CharSequence)paramObject;
      g = ((CharSequence)paramObject);
      return;
    case 4: 
      paramInt = ((Boolean)paramObject).booleanValue();
      f = paramInt;
      return;
    case 3: 
      paramObject = (CharSequence)paramObject;
      e = ((CharSequence)paramObject);
      return;
    case 2: 
      paramObject = (CharSequence)paramObject;
      d = ((CharSequence)paramObject);
      return;
    case 1: 
      paramObject = (CharSequence)paramObject;
      c = ((CharSequence)paramObject);
      return;
    }
    paramObject = (CharSequence)paramObject;
    b = ((CharSequence)paramObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */