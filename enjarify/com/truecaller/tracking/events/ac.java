package com.truecaller.tracking.events;

import java.util.List;
import java.util.Map;
import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class ac
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public List b;
  public CharSequence c;
  public CharSequence d;
  public int e;
  public int f;
  public boolean g;
  public Map h;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"AppSmsCategorizerCompare\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"participants\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"Participant\",\"fields\":[{\"name\":\"type\",\"type\":\"string\"},{\"name\":\"id\",\"type\":\"string\"},{\"name\":\"info\",\"type\":{\"type\":\"record\",\"name\":\"ContactInfo\",\"fields\":[{\"name\":\"inPhonebook\",\"type\":\"boolean\"},{\"name\":\"hasName\",\"type\":\"boolean\"},{\"name\":\"inUserSpammerList\",\"type\":[\"null\",\"boolean\"]},{\"name\":\"inTopSpammerList\",\"type\":[\"null\",\"boolean\"]},{\"name\":\"inUserWhiteList\",\"type\":[\"null\",\"boolean\"]},{\"name\":\"spammerFromServer\",\"type\":[\"null\",\"boolean\"]},{\"name\":\"spamScore\",\"type\":[\"null\",\"int\"]},{\"name\":\"hasPushData\",\"type\":[\"null\",\"boolean\"],\"default\":null}]}}]}}},{\"name\":\"categorizerCategory\",\"type\":\"string\"},{\"name\":\"parserCategory\",\"type\":\"string\"},{\"name\":\"categorizerVersion\",\"type\":\"int\"},{\"name\":\"parserVersion\",\"type\":\"int\"},{\"name\":\"reclassifySms\",\"type\":\"boolean\"},{\"name\":\"properties\",\"type\":[\"null\",{\"type\":\"map\",\"values\":\"string\"}],\"default\":null}]}");
  }
  
  public static ac.a b()
  {
    ac.a locala = new com/truecaller/tracking/events/ac$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 6: 
      return h;
    case 5: 
      return Boolean.valueOf(g);
    case 4: 
      return Integer.valueOf(f);
    case 3: 
      return Integer.valueOf(e);
    case 2: 
      return d;
    case 1: 
      return c;
    }
    return b;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 6: 
      paramObject = (Map)paramObject;
      h = ((Map)paramObject);
      return;
    case 5: 
      paramInt = ((Boolean)paramObject).booleanValue();
      g = paramInt;
      return;
    case 4: 
      paramInt = ((Integer)paramObject).intValue();
      f = paramInt;
      return;
    case 3: 
      paramInt = ((Integer)paramObject).intValue();
      e = paramInt;
      return;
    case 2: 
      paramObject = (CharSequence)paramObject;
      d = ((CharSequence)paramObject);
      return;
    case 1: 
      paramObject = (CharSequence)paramObject;
      c = ((CharSequence)paramObject);
      return;
    }
    paramObject = (List)paramObject;
    b = ((List)paramObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */