package com.truecaller.tracking.events;

import java.util.List;
import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class bc
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public List b;
  public List c;
  public List d;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"TagsServed\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"serverTagsReceived\",\"type\":[\"null\",{\"type\":\"array\",\"items\":\"string\"}],\"default\":null},{\"name\":\"manualTagsAvailable\",\"type\":[\"null\",{\"type\":\"array\",\"items\":\"string\"}],\"default\":null},{\"name\":\"shownTags\",\"type\":[\"null\",{\"type\":\"array\",\"items\":\"string\"}],\"default\":null}]}");
  }
  
  public static bc.a b()
  {
    bc.a locala = new com/truecaller/tracking/events/bc$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 2: 
      return d;
    case 1: 
      return c;
    }
    return b;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 2: 
      paramObject = (List)paramObject;
      d = ((List)paramObject);
      return;
    case 1: 
      paramObject = (List)paramObject;
      c = ((List)paramObject);
      return;
    }
    paramObject = (List)paramObject;
    b = ((List)paramObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.bc
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */