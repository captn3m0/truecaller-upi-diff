package com.truecaller.tracking.events;

import java.util.List;
import org.apache.a.d;
import org.apache.a.d.f;
import org.apache.a.d.f;

public final class z$a
  extends f
{
  private CharSequence c;
  private CharSequence d;
  private CharSequence e;
  private CharSequence f;
  private List g;
  private List h;
  private boolean i;
  private boolean j;
  
  private z$a()
  {
    super(locald);
  }
  
  public final a a(CharSequence paramCharSequence)
  {
    a(a[0], paramCharSequence);
    c = paramCharSequence;
    b[0] = true;
    return this;
  }
  
  public final a a(List paramList)
  {
    d.f[] arrayOff = a;
    int k = 4;
    a(arrayOff[k], paramList);
    g = paramList;
    b[k] = true;
    return this;
  }
  
  public final a a(boolean paramBoolean)
  {
    Object localObject = a;
    int k = 6;
    localObject = localObject[k];
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    a((d.f)localObject, localBoolean);
    i = paramBoolean;
    b[k] = true;
    return this;
  }
  
  public final z a()
  {
    try
    {
      z localz = new com/truecaller/tracking/events/z;
      localz.<init>();
      localObject = b;
      int k = 0;
      int m = localObject[0];
      if (m != 0)
      {
        localObject = c;
      }
      else
      {
        localObject = a;
        localObject = localObject[0];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      b = ((CharSequence)localObject);
      localObject = b;
      k = 1;
      m = localObject[k];
      if (m != 0)
      {
        localObject = d;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      c = ((CharSequence)localObject);
      localObject = b;
      k = 2;
      m = localObject[k];
      if (m != 0)
      {
        localObject = e;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      d = ((CharSequence)localObject);
      localObject = b;
      k = 3;
      m = localObject[k];
      if (m != 0)
      {
        localObject = f;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      e = ((CharSequence)localObject);
      localObject = b;
      k = 4;
      m = localObject[k];
      if (m != 0)
      {
        localObject = g;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (List)localObject;
      }
      f = ((List)localObject);
      localObject = b;
      k = 5;
      m = localObject[k];
      if (m != 0)
      {
        localObject = h;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (List)localObject;
      }
      g = ((List)localObject);
      localObject = b;
      k = 6;
      m = localObject[k];
      boolean bool1;
      if (m != 0)
      {
        bool1 = i;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (Boolean)localObject;
        bool1 = ((Boolean)localObject).booleanValue();
      }
      h = bool1;
      localObject = b;
      k = 7;
      int n = localObject[k];
      boolean bool2;
      if (n != 0)
      {
        bool2 = j;
      }
      else
      {
        localObject = a;
        localObject = localObject[k];
        localObject = a((d.f)localObject);
        localObject = (Boolean)localObject;
        bool2 = ((Boolean)localObject).booleanValue();
      }
      i = bool2;
      return localz;
    }
    catch (Exception localException)
    {
      Object localObject = new org/apache/a/a;
      ((org.apache.a.a)localObject).<init>(localException);
      throw ((Throwable)localObject);
    }
  }
  
  public final a b(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int k = 1;
    a(arrayOff[k], paramCharSequence);
    d = paramCharSequence;
    b[k] = k;
    return this;
  }
  
  public final a b(List paramList)
  {
    d.f[] arrayOff = a;
    int k = 5;
    a(arrayOff[k], paramList);
    h = paramList;
    b[k] = true;
    return this;
  }
  
  public final a b(boolean paramBoolean)
  {
    Object localObject = a;
    int k = 7;
    localObject = localObject[k];
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    a((d.f)localObject, localBoolean);
    j = paramBoolean;
    b[k] = true;
    return this;
  }
  
  public final a c(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int k = 2;
    a(arrayOff[k], paramCharSequence);
    e = paramCharSequence;
    b[k] = true;
    return this;
  }
  
  public final a d(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int k = 3;
    a(arrayOff[k], paramCharSequence);
    f = paramCharSequence;
    b[k] = true;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.z.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */