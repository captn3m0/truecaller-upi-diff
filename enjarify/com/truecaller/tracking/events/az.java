package com.truecaller.tracking.events;

import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class az
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public int b;
  public int c;
  public CharSequence d;
  public boolean e;
  public CharSequence f;
  public CharSequence g;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"SimInfo\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"mcc\",\"type\":\"int\"},{\"name\":\"mnc\",\"type\":\"int\"},{\"name\":\"msin\",\"type\":[\"null\",\"string\"]},{\"name\":\"isActive\",\"type\":\"boolean\"},{\"name\":\"operator\",\"type\":\"string\"},{\"name\":\"normalizedPhoneNumber\",\"type\":[\"null\",\"string\"]}]}");
  }
  
  public static az.a b()
  {
    az.a locala = new com/truecaller/tracking/events/az$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 5: 
      return g;
    case 4: 
      return f;
    case 3: 
      return Boolean.valueOf(e);
    case 2: 
      return d;
    case 1: 
      return Integer.valueOf(c);
    }
    return Integer.valueOf(b);
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 5: 
      paramObject = (CharSequence)paramObject;
      g = ((CharSequence)paramObject);
      return;
    case 4: 
      paramObject = (CharSequence)paramObject;
      f = ((CharSequence)paramObject);
      return;
    case 3: 
      paramInt = ((Boolean)paramObject).booleanValue();
      e = paramInt;
      return;
    case 2: 
      paramObject = (CharSequence)paramObject;
      d = ((CharSequence)paramObject);
      return;
    case 1: 
      paramInt = ((Integer)paramObject).intValue();
      c = paramInt;
      return;
    }
    paramInt = ((Integer)paramObject).intValue();
    b = paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.az
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */