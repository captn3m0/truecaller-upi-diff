package com.truecaller.tracking.events;

import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class i
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public CharSequence b;
  public long c;
  public long d;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"AppCallerIdNotificationUsed\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"pushId\",\"type\":\"string\"},{\"name\":\"pushDelay\",\"type\":\"long\"},{\"name\":\"otherDelay\",\"type\":\"long\"}]}");
  }
  
  public static i.a b()
  {
    i.a locala = new com/truecaller/tracking/events/i$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 2: 
      return Long.valueOf(d);
    case 1: 
      return Long.valueOf(c);
    }
    return b;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    long l;
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 2: 
      l = ((Long)paramObject).longValue();
      d = l;
      return;
    case 1: 
      l = ((Long)paramObject).longValue();
      c = l;
      return;
    }
    paramObject = (CharSequence)paramObject;
    b = ((CharSequence)paramObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */