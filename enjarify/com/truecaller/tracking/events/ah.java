package com.truecaller.tracking.events;

import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class ah
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public CharSequence b;
  public CharSequence c;
  public CharSequence d;
  public CharSequence e;
  public CharSequence f;
  public CharSequence g;
  public Integer h;
  public Integer i;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"AppVoipStateChanged\",\"namespace\":\"com.truecaller.tracking.events\",\"doc\":\"* Voip call funnel, for tracking call flow and failures. Where possible, the voip ids and call ids should be passed.\",\"fields\":[{\"name\":\"direction\",\"type\":\"string\",\"doc\":\"* Whether the call was Ingoing or Outgoing.\"},{\"name\":\"state\",\"type\":\"string\",\"doc\":\"* Indicates the point\"},{\"name\":\"reason\",\"type\":[\"null\",\"string\"],\"doc\":\"* Extra context for some states.\",\"default\":null},{\"name\":\"channel\",\"type\":\"string\",\"doc\":\"* The name of the channel. Used to cross-reference calls.\"},{\"name\":\"voipId\",\"type\":[\"null\",\"string\"],\"doc\":\"* Voip id of the user.\",\"default\":null},{\"name\":\"remoteVoipId\",\"type\":[\"null\",\"string\"],\"doc\":\"* Voip id of the remote party on the call.\",\"default\":null},{\"name\":\"rtcUid\",\"type\":[\"null\",\"int\"],\"doc\":\"* RTC user id of the user,\",\"default\":null},{\"name\":\"remoteRtcUid\",\"type\":[\"null\",\"int\"],\"doc\":\"* RTC user id of the remote party on the call.\",\"default\":null}]}");
  }
  
  public static ah.a b()
  {
    ah.a locala = new com/truecaller/tracking/events/ah$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 7: 
      return i;
    case 6: 
      return h;
    case 5: 
      return g;
    case 4: 
      return f;
    case 3: 
      return e;
    case 2: 
      return d;
    case 1: 
      return c;
    }
    return b;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 7: 
      paramObject = (Integer)paramObject;
      i = ((Integer)paramObject);
      return;
    case 6: 
      paramObject = (Integer)paramObject;
      h = ((Integer)paramObject);
      return;
    case 5: 
      paramObject = (CharSequence)paramObject;
      g = ((CharSequence)paramObject);
      return;
    case 4: 
      paramObject = (CharSequence)paramObject;
      f = ((CharSequence)paramObject);
      return;
    case 3: 
      paramObject = (CharSequence)paramObject;
      e = ((CharSequence)paramObject);
      return;
    case 2: 
      paramObject = (CharSequence)paramObject;
      d = ((CharSequence)paramObject);
      return;
    case 1: 
      paramObject = (CharSequence)paramObject;
      c = ((CharSequence)paramObject);
      return;
    }
    paramObject = (CharSequence)paramObject;
    b = ((CharSequence)paramObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */