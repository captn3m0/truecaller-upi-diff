package com.truecaller.tracking.events;

import org.apache.a.d;
import org.apache.a.d.f;
import org.apache.a.d.f;

public final class az$a
  extends f
{
  private int c;
  private int d;
  private CharSequence e;
  private boolean f;
  private CharSequence g;
  private CharSequence h;
  
  private az$a()
  {
    super(locald);
  }
  
  public final a a()
  {
    d.f[] arrayOff = a;
    int i = 2;
    a(arrayOff[i], null);
    e = null;
    b[i] = true;
    return this;
  }
  
  public final a a(int paramInt)
  {
    d.f localf = a[0];
    Integer localInteger = Integer.valueOf(paramInt);
    a(localf, localInteger);
    c = paramInt;
    b[0] = true;
    return this;
  }
  
  public final a a(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int i = 4;
    a(arrayOff[i], paramCharSequence);
    g = paramCharSequence;
    b[i] = true;
    return this;
  }
  
  public final a a(boolean paramBoolean)
  {
    Object localObject = a;
    int i = 3;
    localObject = localObject[i];
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    a((d.f)localObject, localBoolean);
    f = paramBoolean;
    b[i] = true;
    return this;
  }
  
  public final a b(int paramInt)
  {
    Object localObject = a;
    int i = 1;
    localObject = localObject[i];
    Integer localInteger = Integer.valueOf(paramInt);
    a((d.f)localObject, localInteger);
    d = paramInt;
    b[i] = i;
    return this;
  }
  
  public final a b(CharSequence paramCharSequence)
  {
    d.f[] arrayOff = a;
    int i = 5;
    a(arrayOff[i], paramCharSequence);
    h = paramCharSequence;
    b[i] = true;
    return this;
  }
  
  public final az b()
  {
    try
    {
      az localaz = new com/truecaller/tracking/events/az;
      localaz.<init>();
      localObject = b;
      int i = 0;
      int j = localObject[0];
      if (j != 0)
      {
        j = c;
      }
      else
      {
        localObject = a;
        localObject = localObject[0];
        localObject = a((d.f)localObject);
        localObject = (Integer)localObject;
        j = ((Integer)localObject).intValue();
      }
      b = j;
      localObject = b;
      i = 1;
      j = localObject[i];
      if (j != 0)
      {
        j = d;
      }
      else
      {
        localObject = a;
        localObject = localObject[i];
        localObject = a((d.f)localObject);
        localObject = (Integer)localObject;
        j = ((Integer)localObject).intValue();
      }
      c = j;
      localObject = b;
      i = 2;
      j = localObject[i];
      if (j != 0)
      {
        localObject = e;
      }
      else
      {
        localObject = a;
        localObject = localObject[i];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      d = ((CharSequence)localObject);
      localObject = b;
      i = 3;
      j = localObject[i];
      boolean bool;
      if (j != 0)
      {
        bool = f;
      }
      else
      {
        localObject = a;
        localObject = localObject[i];
        localObject = a((d.f)localObject);
        localObject = (Boolean)localObject;
        bool = ((Boolean)localObject).booleanValue();
      }
      e = bool;
      localObject = b;
      i = 4;
      int k = localObject[i];
      if (k != 0)
      {
        localObject = g;
      }
      else
      {
        localObject = a;
        localObject = localObject[i];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      f = ((CharSequence)localObject);
      localObject = b;
      i = 5;
      k = localObject[i];
      if (k != 0)
      {
        localObject = h;
      }
      else
      {
        localObject = a;
        localObject = localObject[i];
        localObject = a((d.f)localObject);
        localObject = (CharSequence)localObject;
      }
      g = ((CharSequence)localObject);
      return localaz;
    }
    catch (Exception localException)
    {
      Object localObject = new org/apache/a/a;
      ((org.apache.a.a)localObject).<init>(localException);
      throw ((Throwable)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.az.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */