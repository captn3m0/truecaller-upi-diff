package com.truecaller.tracking.events;

import org.apache.a.a;
import org.apache.a.d.q;
import org.apache.a.d.e;

public final class an
  extends e
  implements org.apache.a.d.d
{
  public static final org.apache.a.d a;
  public CharSequence b;
  public CharSequence c;
  public CharSequence d;
  public int e;
  public int f;
  public int g;
  
  static
  {
    d.q localq = new org/apache/a/d$q;
    localq.<init>();
    a = localq.a("{\"type\":\"record\",\"name\":\"DeviceInfo\",\"namespace\":\"com.truecaller.tracking.events\",\"fields\":[{\"name\":\"manufacturer\",\"type\":\"string\"},{\"name\":\"model\",\"type\":\"string\"},{\"name\":\"imei\",\"type\":[\"null\",\"string\"]},{\"name\":\"screenHeight\",\"type\":\"int\"},{\"name\":\"screenWidth\",\"type\":\"int\"},{\"name\":\"density\",\"type\":\"int\"}]}");
  }
  
  public static an.a b()
  {
    an.a locala = new com/truecaller/tracking/events/an$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final Object a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 5: 
      return Integer.valueOf(g);
    case 4: 
      return Integer.valueOf(f);
    case 3: 
      return Integer.valueOf(e);
    case 2: 
      return d;
    case 1: 
      return c;
    }
    return b;
  }
  
  public final org.apache.a.d a()
  {
    return a;
  }
  
  public final void a(int paramInt, Object paramObject)
  {
    switch (paramInt)
    {
    default: 
      a locala = new org/apache/a/a;
      locala.<init>("Bad index");
      throw locala;
    case 5: 
      paramInt = ((Integer)paramObject).intValue();
      g = paramInt;
      return;
    case 4: 
      paramInt = ((Integer)paramObject).intValue();
      f = paramInt;
      return;
    case 3: 
      paramInt = ((Integer)paramObject).intValue();
      e = paramInt;
      return;
    case 2: 
      paramObject = (CharSequence)paramObject;
      d = ((CharSequence)paramObject);
      return;
    case 1: 
      paramObject = (CharSequence)paramObject;
      c = ((CharSequence)paramObject);
      return;
    }
    paramObject = (CharSequence)paramObject;
    b = ((CharSequence)paramObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.tracking.events.an
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */