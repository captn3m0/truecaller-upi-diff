package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.List;

final class b$b
  extends u
{
  private final List b;
  
  private b$b(e parame, List paramList)
  {
    super(parame);
    b = paramList;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".addFromBackup(");
    String str = a(b, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */