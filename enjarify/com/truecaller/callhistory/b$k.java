package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class b$k
  extends u
{
  private final FilterType b;
  private final Integer c;
  
  private b$k(e parame, FilterType paramFilterType, Integer paramInteger)
  {
    super(parame);
    b = paramFilterType;
    c = paramInteger;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".getCallHistoryList(");
    Object localObject = b;
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */