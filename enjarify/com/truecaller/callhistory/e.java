package com.truecaller.callhistory;

import android.content.ContentValues;
import android.database.Cursor;
import com.google.c.a.k.d;
import com.truecaller.data.access.d;
import com.truecaller.data.entity.HistoryEvent;
import java.util.UUID;

public final class e
{
  public static final ContentValues a(HistoryEvent paramHistoryEvent)
  {
    c.g.b.k.b(paramHistoryEvent, "receiver$0");
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    Object localObject1 = paramHistoryEvent.getTcId();
    localContentValues.put("tc_id", (String)localObject1);
    Object localObject2 = "normalized_number";
    localObject1 = paramHistoryEvent.a();
    String str = null;
    if (localObject1 != null)
    {
      localObject1 = p.a((String)localObject1);
    }
    else
    {
      i = 0;
      localObject1 = null;
    }
    localContentValues.put((String)localObject2, (String)localObject1);
    localObject2 = "raw_number";
    localObject1 = paramHistoryEvent.b();
    if (localObject1 != null) {
      str = p.a((String)localObject1);
    }
    localContentValues.put((String)localObject2, str);
    localObject2 = "number_type";
    localObject1 = paramHistoryEvent.c();
    if (localObject1 != null) {
      localObject1 = paramHistoryEvent.c();
    } else {
      localObject1 = k.d.l;
    }
    localObject1 = ((k.d)localObject1).name();
    localContentValues.put((String)localObject2, (String)localObject1);
    localObject1 = paramHistoryEvent.d();
    localContentValues.put("country_code", (String)localObject1);
    localObject1 = paramHistoryEvent.e();
    localContentValues.put("cached_name", (String)localObject1);
    localObject1 = Integer.valueOf(paramHistoryEvent.f());
    localContentValues.put("type", (Integer)localObject1);
    int i = paramHistoryEvent.h();
    localObject1 = Integer.valueOf(i);
    localContentValues.put("action", (Integer)localObject1);
    localObject2 = "call_log_id";
    localObject1 = paramHistoryEvent.i();
    localContentValues.put((String)localObject2, (Long)localObject1);
    long l1 = paramHistoryEvent.j();
    long l2 = 1L;
    boolean bool = l1 < l2;
    if (bool)
    {
      localObject2 = "Correcting bad event timestamp";
      new String[1][0] = localObject2;
      l1 = System.currentTimeMillis();
    }
    localObject2 = Long.valueOf(l1);
    localContentValues.put("timestamp", (Long)localObject2);
    localObject1 = Long.valueOf(paramHistoryEvent.k());
    localContentValues.put("duration", (Long)localObject1);
    localObject1 = paramHistoryEvent.l();
    localContentValues.put("subscription_id", (String)localObject1);
    localObject1 = Integer.valueOf(paramHistoryEvent.m());
    localContentValues.put("feature", (Integer)localObject1);
    localObject1 = Integer.valueOf(paramHistoryEvent.o());
    localContentValues.put("new", (Integer)localObject1);
    localObject1 = Integer.valueOf(paramHistoryEvent.p());
    localContentValues.put("is_read", (Integer)localObject1);
    localObject1 = paramHistoryEvent.q();
    localContentValues.put("subscription_component_name", (String)localObject1);
    localObject1 = Integer.valueOf(paramHistoryEvent.r());
    localContentValues.put("tc_flag", (Integer)localObject1);
    paramHistoryEvent = (CharSequence)paramHistoryEvent.u();
    localObject1 = (CharSequence)UUID.randomUUID().toString();
    paramHistoryEvent = (String)org.c.a.a.a.k.e(paramHistoryEvent, (CharSequence)localObject1);
    localContentValues.put("event_id", paramHistoryEvent);
    return localContentValues;
  }
  
  public static final aa a(Cursor paramCursor)
  {
    c.g.b.k.b(paramCursor, "receiver$0");
    aa localaa = new com/truecaller/callhistory/aa;
    com.truecaller.data.access.e locale = new com/truecaller/data/access/e;
    locale.<init>(paramCursor);
    d locald = new com/truecaller/data/access/d;
    locald.<init>(paramCursor);
    localaa.<init>(paramCursor, locale, locald);
    return localaa;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */