package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class b$s
  extends u
{
  private final int b;
  
  private b$s(e parame, int paramInt)
  {
    super(parame);
    b = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".getSearchHistoryWithContact(");
    String str = a(Integer.valueOf(b), 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */