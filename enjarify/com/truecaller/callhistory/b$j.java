package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.Contact;

final class b$j
  extends u
{
  private final Contact b;
  
  private b$j(e parame, Contact paramContact)
  {
    super(parame);
    b = paramContact;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".getCallHistoryForContact(");
    String str = a(b, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */