package com.truecaller.callhistory;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import com.truecaller.androidactors.ab;
import com.truecaller.androidactors.w;
import com.truecaller.content.TruecallerContract.k;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.log.AssertionUtil;
import com.truecaller.service.WidgetListProvider;
import com.truecaller.util.q;
import java.util.Collection;

public final class ag
  implements af
{
  private final Context a;
  
  public ag(Context paramContext)
  {
    a = paramContext;
  }
  
  public final w a(int paramInt, Collection paramCollection)
  {
    Object localObject1 = "ids";
    c.g.b.k.b(paramCollection, (String)localObject1);
    int i = 5;
    boolean bool = true;
    if (paramInt != i)
    {
      i = 6;
      if (paramInt != i)
      {
        i = 0;
        localObject1 = null;
        break label44;
      }
    }
    i = 1;
    label44:
    Object localObject3;
    try
    {
      Object localObject2 = new String[0];
      AssertionUtil.isTrue(i, (String[])localObject2);
      localObject1 = new java/lang/StringBuilder;
      localObject2 = "type=? AND _id IN (";
      ((StringBuilder)localObject1).<init>((String)localObject2);
      paramCollection = (Iterable)paramCollection;
      char c = ',';
      paramCollection = org.c.a.a.a.k.a(paramCollection, c);
      ((StringBuilder)localObject1).append(paramCollection);
      paramCollection = ")";
      ((StringBuilder)localObject1).append(paramCollection);
      paramCollection = ((StringBuilder)localObject1).toString();
      localObject1 = a;
      localObject1 = ((Context)localObject1).getContentResolver();
      localObject2 = TruecallerContract.n.a();
      String[] arrayOfString = new String[bool];
      localObject3 = String.valueOf(paramInt);
      arrayOfString[0] = localObject3;
      paramInt = ((ContentResolver)localObject1).delete((Uri)localObject2, paramCollection, arrayOfString);
      if (paramInt != 0)
      {
        paramCollection = a;
        WidgetListProvider.a(paramCollection);
      }
      if (paramInt <= 0) {
        bool = false;
      }
      localObject3 = Boolean.valueOf(bool);
      localObject3 = w.b(localObject3);
      paramCollection = "Promise.wrap(result > 0)";
      c.g.b.k.a(localObject3, paramCollection);
      return (w)localObject3;
    }
    catch (SQLiteException localSQLiteException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSQLiteException);
      localObject3 = w.b(Boolean.FALSE);
      c.g.b.k.a(localObject3, "Promise.wrap(false)");
    }
    return (w)localObject3;
  }
  
  public final void a(int paramInt)
  {
    int i = 5;
    int j = 1;
    Object localObject1;
    if (paramInt != i)
    {
      i = 6;
      if (paramInt != i)
      {
        i = 0;
        localObject1 = null;
        break label30;
      }
    }
    i = 1;
    try
    {
      label30:
      Object localObject2 = new String[0];
      AssertionUtil.isTrue(i, (String[])localObject2);
      localObject1 = a;
      localObject1 = ((Context)localObject1).getContentResolver();
      localObject2 = TruecallerContract.n.a();
      String str = "type=?";
      String[] arrayOfString = new String[j];
      localObject3 = String.valueOf(paramInt);
      arrayOfString[0] = localObject3;
      paramInt = ((ContentResolver)localObject1).delete((Uri)localObject2, str, arrayOfString);
      if (paramInt != 0)
      {
        localObject3 = a;
        WidgetListProvider.a((Context)localObject3);
        return;
      }
    }
    catch (SQLiteException localSQLiteException)
    {
      Object localObject3 = (Throwable)localSQLiteException;
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject3);
    }
  }
  
  public final boolean a(HistoryEvent paramHistoryEvent)
  {
    String str = "event";
    c.g.b.k.b(paramHistoryEvent, str);
    int i = paramHistoryEvent.f();
    int j = 5;
    if (i != j)
    {
      int k = paramHistoryEvent.f();
      i = 6;
      if (k != i) {
        return false;
      }
    }
    return true;
  }
  
  public final w b(int paramInt)
  {
    int i = 1;
    Object localObject1 = null;
    int j = 5;
    Object localObject2;
    if (paramInt != j)
    {
      j = 6;
      if (paramInt != j)
      {
        j = 0;
        localObject2 = null;
        break label38;
      }
    }
    j = 1;
    label38:
    String[] arrayOfString1 = new String[0];
    AssertionUtil.isTrue(j, arrayOfString1);
    String[] arrayOfString2 = new String[i];
    Object localObject3 = String.valueOf(paramInt);
    arrayOfString2[0] = localObject3;
    paramInt = 0;
    localObject3 = null;
    Object localObject4;
    try
    {
      localObject4 = a;
      ContentResolver localContentResolver = ((Context)localObject4).getContentResolver();
      Uri localUri = TruecallerContract.n.d();
      String str1 = "type=?";
      String str2 = "timestamp DESC";
      localObject4 = localContentResolver.query(localUri, null, str1, arrayOfString2, str2);
      if (localObject4 == null) {
        break label182;
      }
      try
      {
        localObject1 = e.a((Cursor)localObject4);
        localObject2 = ag.a.a;
        localObject2 = (ab)localObject2;
        localObject1 = w.a(localObject1, (ab)localObject2);
        localObject2 = "Promise.wrap<HistoryEven…tCursor()) { it.close() }";
        c.g.b.k.a(localObject1, (String)localObject2);
        return (w)localObject1;
      }
      catch (SQLiteException localSQLiteException1) {}
      localThrowable = (Throwable)localSQLiteException2;
    }
    catch (SQLiteException localSQLiteException2)
    {
      i = 0;
      localObject4 = null;
    }
    Throwable localThrowable;
    AssertionUtil.reportThrowableButNeverCrash(localThrowable);
    q.a((Cursor)localObject4);
    label182:
    localObject3 = w.b(null);
    c.g.b.k.a(localObject3, "Promise.wrap(null)");
    return (w)localObject3;
  }
  
  public final void b(HistoryEvent paramHistoryEvent)
  {
    c.g.b.k.b(paramHistoryEvent, "event");
    Object localObject1 = (CharSequence)paramHistoryEvent.getTcId();
    boolean bool1 = org.c.a.a.a.k.b((CharSequence)localObject1);
    int j = 1;
    Object localObject2;
    Object localObject3;
    Uri localUri;
    Object localObject4;
    if (bool1)
    {
      localObject1 = (CharSequence)paramHistoryEvent.a();
      bool1 = org.c.a.a.a.k.b((CharSequence)localObject1);
      if (!bool1)
      {
        bool1 = false;
        localObject1 = null;
        try
        {
          localObject2 = a;
          localObject3 = ((Context)localObject2).getContentResolver();
          localUri = TruecallerContract.k.a();
          localObject2 = "tc_id";
          localObject4 = new String[] { localObject2 };
          String str = "data1=? AND data_type=4";
          String[] arrayOfString = new String[j];
          localObject2 = paramHistoryEvent.a();
          arrayOfString[0] = localObject2;
          localObject1 = ((ContentResolver)localObject3).query(localUri, (String[])localObject4, str, arrayOfString, null);
          if (localObject1 != null)
          {
            boolean bool2 = ((Cursor)localObject1).moveToFirst();
            if (bool2)
            {
              localObject2 = ((Cursor)localObject1).getString(0);
              paramHistoryEvent.setTcId((String)localObject2);
            }
          }
        }
        finally
        {
          q.a((Cursor)localObject1);
        }
      }
    }
    localObject1 = (CharSequence)paramHistoryEvent.getTcId();
    bool1 = org.c.a.a.a.k.c((CharSequence)localObject1);
    if (bool1)
    {
      int i = paramHistoryEvent.f();
      int k = 6;
      if (i != k)
      {
        localObject1 = new android/content/ContentValues;
        ((ContentValues)localObject1).<init>();
        long l = paramHistoryEvent.j();
        localObject3 = Long.valueOf(l);
        ((ContentValues)localObject1).put("timestamp", (Long)localObject3);
        localObject2 = paramHistoryEvent.getTcId();
        if (localObject2 != null)
        {
          localObject3 = a.getContentResolver();
          localUri = TruecallerContract.n.a();
          localObject4 = "tc_id=? AND type=5";
          localObject5 = new String[j];
          localObject5[0] = localObject2;
          i = ((ContentResolver)localObject3).update(localUri, (ContentValues)localObject1, (String)localObject4, (String[])localObject5);
          if (i != 0)
          {
            WidgetListProvider.a(a);
            return;
          }
        }
      }
    }
    paramHistoryEvent.b(0);
    localObject1 = a.getContentResolver();
    Object localObject5 = TruecallerContract.n.a();
    paramHistoryEvent = e.a(paramHistoryEvent);
    paramHistoryEvent = ((ContentResolver)localObject1).insert((Uri)localObject5, paramHistoryEvent);
    if (paramHistoryEvent != null)
    {
      paramHistoryEvent = a;
      WidgetListProvider.a(paramHistoryEvent);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */