package com.truecaller.callhistory;

public enum FilterType
{
  static
  {
    FilterType[] arrayOfFilterType = new FilterType[6];
    FilterType localFilterType = new com/truecaller/callhistory/FilterType;
    localFilterType.<init>("NONE", 0);
    NONE = localFilterType;
    arrayOfFilterType[0] = localFilterType;
    localFilterType = new com/truecaller/callhistory/FilterType;
    int i = 1;
    localFilterType.<init>("INCOMING", i);
    INCOMING = localFilterType;
    arrayOfFilterType[i] = localFilterType;
    localFilterType = new com/truecaller/callhistory/FilterType;
    i = 2;
    localFilterType.<init>("OUTGOING", i);
    OUTGOING = localFilterType;
    arrayOfFilterType[i] = localFilterType;
    localFilterType = new com/truecaller/callhistory/FilterType;
    i = 3;
    localFilterType.<init>("MISSED", i);
    MISSED = localFilterType;
    arrayOfFilterType[i] = localFilterType;
    localFilterType = new com/truecaller/callhistory/FilterType;
    i = 4;
    localFilterType.<init>("BLOCKED", i);
    BLOCKED = localFilterType;
    arrayOfFilterType[i] = localFilterType;
    localFilterType = new com/truecaller/callhistory/FilterType;
    i = 5;
    localFilterType.<init>("FLASH", i);
    FLASH = localFilterType;
    arrayOfFilterType[i] = localFilterType;
    $VALUES = arrayOfFilterType;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.FilterType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */