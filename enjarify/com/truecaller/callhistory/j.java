package com.truecaller.callhistory;

import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d
{
  private final g a;
  private final Provider b;
  private final Provider c;
  
  private j(g paramg, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramg;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static j a(g paramg, Provider paramProvider1, Provider paramProvider2)
  {
    j localj = new com/truecaller/callhistory/j;
    localj.<init>(paramg, paramProvider1, paramProvider2);
    return localj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */