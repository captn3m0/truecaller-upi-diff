package com.truecaller.callhistory;

import android.content.ContentUris;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.net.Uri;
import com.google.c.a.k.d;
import com.truecaller.common.h.ab;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.data.access.d;
import com.truecaller.data.access.e;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.HistoryEvent.a;
import com.truecaller.data.entity.Number;

public final class aa
  extends CursorWrapper
  implements z
{
  private final int a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  private final int j;
  private final int k;
  private final int l;
  private final int m;
  private final int n;
  private final int o;
  private final int p;
  private final int q;
  private final int r;
  private final int s;
  private final e t;
  private final d u;
  private final boolean v;
  
  public aa(Cursor paramCursor)
  {
    this(paramCursor, null, null);
  }
  
  public aa(Cursor paramCursor, e parame, d paramd)
  {
    this(paramCursor, parame, paramd, false);
  }
  
  public aa(Cursor paramCursor, e parame, d paramd, boolean paramBoolean)
  {
    super(paramCursor);
    t = parame;
    v = paramBoolean;
    u = paramd;
    int i1 = paramCursor.getColumnIndexOrThrow("_id");
    a = i1;
    i1 = paramCursor.getColumnIndexOrThrow("tc_id");
    b = i1;
    i1 = paramCursor.getColumnIndexOrThrow("normalized_number");
    c = i1;
    i1 = paramCursor.getColumnIndexOrThrow("raw_number");
    d = i1;
    i1 = paramCursor.getColumnIndexOrThrow("number_type");
    e = i1;
    i1 = paramCursor.getColumnIndexOrThrow("country_code");
    f = i1;
    i1 = paramCursor.getColumnIndexOrThrow("cached_name");
    g = i1;
    i1 = paramCursor.getColumnIndexOrThrow("type");
    h = i1;
    i1 = paramCursor.getColumnIndexOrThrow("action");
    i = i1;
    i1 = paramCursor.getColumnIndexOrThrow("call_log_id");
    j = i1;
    i1 = paramCursor.getColumnIndexOrThrow("timestamp");
    k = i1;
    i1 = paramCursor.getColumnIndexOrThrow("duration");
    l = i1;
    i1 = paramCursor.getColumnIndexOrThrow("subscription_id");
    m = i1;
    i1 = paramCursor.getColumnIndexOrThrow("feature");
    n = i1;
    i1 = paramCursor.getColumnIndexOrThrow("new");
    o = i1;
    i1 = paramCursor.getColumnIndexOrThrow("is_read");
    p = i1;
    i1 = paramCursor.getColumnIndexOrThrow("subscription_component_name");
    q = i1;
    i1 = paramCursor.getColumnIndexOrThrow("tc_flag");
    r = i1;
    int i2 = paramCursor.getColumnIndexOrThrow("event_id");
    s = i2;
  }
  
  private int a(int paramInt)
  {
    boolean bool = isNull(paramInt);
    if (bool) {
      return 0;
    }
    return getInt(paramInt);
  }
  
  private long a(int paramInt, long paramLong)
  {
    boolean bool = isNull(paramInt);
    if (bool) {
      return paramLong;
    }
    return getLong(paramInt);
  }
  
  public final long a()
  {
    int i1 = a;
    return a(i1, -1);
  }
  
  public final long b()
  {
    int i1 = j;
    return a(i1, -1);
  }
  
  public final long c()
  {
    int i1 = k;
    return getLong(i1);
  }
  
  public final HistoryEvent d()
  {
    aa localaa = this;
    int i1 = a;
    boolean bool1 = isNull(i1);
    CallRecording localCallRecording = null;
    if (!bool1)
    {
      int i2 = h;
      boolean bool2 = isNull(i2);
      if (!bool2)
      {
        HistoryEvent.a locala = new com/truecaller/data/entity/HistoryEvent$a;
        locala.<init>();
        int i3 = a;
        long l1 = getLong(i3);
        int i5 = b;
        Object localObject1 = getString(i5);
        Object localObject2 = Long.valueOf(l1);
        a.setId((Long)localObject2);
        a.setTcId((String)localObject1);
        int i6 = s;
        localObject2 = getString(i6);
        locala.g((String)localObject2);
        i6 = c;
        localObject2 = getString(i6);
        int i7 = d;
        String str1 = getString(i7);
        int i8 = f;
        String str2 = getString(i8);
        int i9 = g;
        String str3 = getString(i9);
        int i10 = e;
        Object localObject3 = getString(i10);
        Object localObject4 = k.d.l;
        localObject3 = ab.a((String)localObject3, (k.d)localObject4);
        locala.a((String)localObject2);
        locala.b(str1);
        locala.a((k.d)localObject3);
        locala.c(str2);
        locala.d(str3);
        int i11 = h;
        i11 = getInt(i11);
        locala.a(i11);
        i11 = i;
        i11 = a(i11);
        locala.b(i11);
        i11 = j;
        localObject4 = Long.valueOf(a(i11, -1));
        locala.a((Long)localObject4);
        i11 = k;
        long l2 = getLong(i11);
        locala.a(l2);
        i11 = l;
        long l3 = 0L;
        long l4 = a(i11, l3);
        locala.b(l4);
        i11 = m;
        localObject4 = getString(i11);
        boolean bool4 = org.c.a.a.a.k.b((CharSequence)localObject4);
        if (!bool4) {
          locala.e((String)localObject4);
        }
        i11 = n;
        i11 = localaa.a(i11);
        locala.c(i11);
        i11 = o;
        i11 = localaa.a(i11);
        locala.d(i11);
        i11 = p;
        i11 = localaa.a(i11);
        locala.e(i11);
        i11 = q;
        localObject4 = localaa.getString(i11);
        locala.f((String)localObject4);
        i11 = r;
        i11 = localaa.a(i11);
        HistoryEvent localHistoryEvent = a;
        r = i11;
        localObject4 = t;
        String str4;
        if (localObject4 != null)
        {
          localObject4 = ((e)localObject4).a(localaa);
          if (localObject4 == null)
          {
            localObject4 = new com/truecaller/data/entity/Contact;
            ((Contact)localObject4).<init>();
            ((Contact)localObject4).l(str3);
            ((Contact)localObject4).setTcId((String)localObject1);
            localObject1 = TruecallerContract.n.a();
            localObject5 = ContentUris.withAppendedId((Uri)localObject1, l1);
            c = ((Uri)localObject5);
            ((Contact)localObject4).a(l2);
          }
          else
          {
            bool3 = v;
            if (bool3)
            {
              localObject5 = t;
              ((e)localObject5).a(localaa, (Contact)localObject4);
            }
          }
          boolean bool3 = ((Contact)localObject4).O();
          if (!bool3)
          {
            localObject5 = Number.a((String)localObject2, str1, str2);
            if (localObject5 != null)
            {
              str4 = ((Contact)localObject4).getTcId();
              ((Number)localObject5).setTcId(str4);
              ((Number)localObject5).a((k.d)localObject3);
              boolean bool5 = ((Contact)localObject4).O();
              if (!bool5)
              {
                str4 = ((Number)localObject5).a();
                ((Contact)localObject4).g(str4);
              }
              ((Contact)localObject4).a((Number)localObject5);
            }
            bool3 = true;
            d = bool3;
          }
          localObject5 = a;
          f = ((Contact)localObject4);
        }
        Object localObject5 = u;
        if (localObject5 != null)
        {
          str4 = "cursor";
          c.g.b.k.b(localaa, str4);
          int i12 = a;
          i5 = -1;
          if (i12 != i5)
          {
            i12 = b;
            if (i12 != i5)
            {
              i12 = a;
              str4 = localaa.getString(i12);
              i6 = b;
              localObject2 = localaa.getString(i6);
              i7 = d;
              int i4;
              if (i7 != i5)
              {
                i4 = d;
                l3 = localaa.getLong(i4);
              }
              else
              {
                i7 = c;
                if (i7 != i5)
                {
                  i4 = c;
                  l3 = localaa.getLong(i4);
                }
                else
                {
                  l3 = -1;
                }
              }
              if (localObject2 != null)
              {
                localCallRecording = new com/truecaller/data/entity/CallRecording;
                localCallRecording.<init>(l3, str4, (String)localObject2);
              }
            }
          }
          if (localCallRecording != null)
          {
            localObject5 = a;
            m = localCallRecording;
          }
        }
        return a;
      }
    }
    return null;
  }
  
  public final String e()
  {
    int i1 = m;
    return (String)org.c.a.a.a.k.e(getString(i1), "-1");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */