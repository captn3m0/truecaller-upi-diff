package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.calling.dialer.HistoryEventsScope;
import java.util.List;

final class b$f
  extends u
{
  private final List b;
  private final List c;
  private final HistoryEventsScope d;
  
  private b$f(e parame, List paramList1, List paramList2, HistoryEventsScope paramHistoryEventsScope)
  {
    super(parame);
    b = paramList1;
    c = paramList2;
    d = paramHistoryEventsScope;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".deleteHistory(");
    Object localObject = b;
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(d, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */