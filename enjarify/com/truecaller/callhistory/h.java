package com.truecaller.callhistory;

import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d
{
  private final g a;
  private final Provider b;
  private final Provider c;
  
  private h(g paramg, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramg;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static h a(g paramg, Provider paramProvider1, Provider paramProvider2)
  {
    h localh = new com/truecaller/callhistory/h;
    localh.<init>(paramg, paramProvider1, paramProvider2);
    return localh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */