package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.Set;

final class b$u
  extends u
{
  private final Set b;
  
  private b$u(e parame, Set paramSet)
  {
    super(parame);
    b = paramSet;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".markAsSeenByHistoryIds(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */