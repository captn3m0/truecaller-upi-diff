package com.truecaller.callhistory;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.SystemClock;
import android.provider.CallLog.Calls;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import com.truecaller.multisim.b.c;
import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.c.a.a.a.a;

class ab
  extends k
{
  private static final long b = TimeUnit.MINUTES.toMillis(2);
  private String[] c;
  private final Object d;
  private final ContentResolver e;
  private final TelephonyManager f;
  private final SubscriptionManager g;
  private List h;
  private volatile long i;
  private final Method j;
  
  ab(Context paramContext)
  {
    Method localMethod = null;
    c = null;
    Object localObject = new java/lang/Object;
    localObject.<init>();
    d = localObject;
    long l = 0L;
    i = l;
    localObject = (TelephonyManager)paramContext.getSystemService("phone");
    f = ((TelephonyManager)localObject);
    localObject = SubscriptionManager.from(paramContext);
    g = ((SubscriptionManager)localObject);
    try
    {
      localObject = f;
      localObject = localObject.getClass();
      String str = "getCallState";
      int k = 1;
      Class[] arrayOfClass = new Class[k];
      Class localClass = Integer.TYPE;
      arrayOfClass[0] = localClass;
      localMethod = ((Class)localObject).getDeclaredMethod(str, arrayOfClass);
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      c.a();
    }
    paramContext = paramContext.getContentResolver();
    e = paramContext;
    j = localMethod;
  }
  
  private List c()
  {
    long l1 = SystemClock.elapsedRealtime();
    long l2 = i;
    List localList = h;
    l2 = l1 - l2;
    long l3 = b;
    boolean bool1 = l2 < l3;
    if (bool1) {
      return localList;
    }
    try
    {
      l2 = i;
      localList = h;
      l1 -= l2;
      l2 = b;
      boolean bool2 = l1 < l2;
      if (bool2) {
        return localList;
      }
      Object localObject1 = g;
      localObject1 = ((SubscriptionManager)localObject1).getActiveSubscriptionInfoList();
      h = ((List)localObject1);
      long l4 = SystemClock.elapsedRealtime();
      i = l4;
      return (List)localObject1;
    }
    finally {}
  }
  
  private boolean d()
  {
    try
    {
      Object localObject1 = e;
      Uri localUri = CallLog.Calls.CONTENT_URI;
      Object localObject2 = "subscription_component_name";
      localObject2 = new String[] { localObject2 };
      String str = "_id ASC LIMIT 1";
      localObject1 = ((ContentResolver)localObject1).query(localUri, (String[])localObject2, null, null, str);
      if (localObject1 != null) {
        ((Closeable)localObject1).close();
      }
    }
    catch (Exception localException)
    {
      return true;
    }
    catch (SecurityException|IOException localSecurityException) {}
    return false;
  }
  
  public int a(int paramInt)
  {
    Object localObject1 = j;
    int k = -1;
    if (localObject1 == null) {
      return k;
    }
    localObject1 = c();
    if (localObject1 == null) {
      return k;
    }
    localObject1 = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      SubscriptionInfo localSubscriptionInfo = (SubscriptionInfo)((Iterator)localObject1).next();
      try
      {
        Object localObject2 = j;
        TelephonyManager localTelephonyManager = f;
        int m = 1;
        Object[] arrayOfObject = new Object[m];
        int n = localSubscriptionInfo.getSubscriptionId();
        Integer localInteger = Integer.valueOf(n);
        arrayOfObject[0] = localInteger;
        localObject2 = ((Method)localObject2).invoke(localTelephonyManager, arrayOfObject);
        localObject2 = (Integer)localObject2;
        int i1 = ((Integer)localObject2).intValue();
        if (i1 == paramInt) {
          return localSubscriptionInfo.getSimSlotIndex();
        }
      }
      catch (IllegalAccessException|InvocationTargetException localIllegalAccessException) {}
    }
    return k;
  }
  
  public String[] b()
  {
    ??? = c;
    if (??? == null) {
      synchronized (d)
      {
        Object localObject2 = c;
        if (localObject2 == null)
        {
          localObject2 = a;
          Object localObject4 = "normalized_number";
          String str = "features";
          localObject4 = new String[] { localObject4, str };
          localObject2 = a.a((Object[])localObject2, (Object[])localObject4);
          localObject2 = (String[])localObject2;
          boolean bool = d();
          if (!bool)
          {
            localObject4 = "subscription_component_name";
            localObject2 = a.b((Object[])localObject2, localObject4);
            localObject2 = (String[])localObject2;
          }
          c = ((String[])localObject2);
        }
      }
    }
    return c;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */