package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class b$aa
  extends u
{
  private final ai.a b;
  
  private b$aa(e parame, ai.a parama)
  {
    super(parame);
    b = parama;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".performNextSyncBatch(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */