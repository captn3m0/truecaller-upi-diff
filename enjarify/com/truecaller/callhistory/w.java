package com.truecaller.callhistory;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.content.TruecallerContract.c;
import com.truecaller.data.entity.CallRecording;

public final class w
  implements v
{
  private final ContentResolver a;
  
  public w(ContentResolver paramContentResolver)
  {
    a = paramContentResolver;
  }
  
  public final void a(CallRecording paramCallRecording)
  {
    k.b(paramCallRecording, "callRecording");
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    String str = c;
    localContentValues.put("recording_path", str);
    paramCallRecording = b;
    localContentValues.put("history_event_id", paramCallRecording);
    paramCallRecording = a;
    Uri localUri = TruecallerContract.c.a();
    paramCallRecording.insert(localUri, localContentValues);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */