package com.truecaller.callhistory;

import android.database.CursorWrapper;
import com.google.c.a.k.d;
import com.truecaller.common.h.ab;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.HistoryEvent.a;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.g;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.multisim.d;
import java.util.UUID;
import org.c.a.a.a.a;
import org.c.a.a.a.k;

final class ad
  extends CursorWrapper
  implements ac
{
  private int[] a;
  private final g b;
  private final d c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  private final int j;
  private final int k;
  private final int l;
  private final int m;
  private final int n;
  private final int o;
  
  ad(g paramg, d paramd)
  {
    super(paramd);
    int[] arrayOfInt = new int[4];
    int[] tmp10_9 = arrayOfInt;
    int[] tmp11_10 = tmp10_9;
    int[] tmp11_10 = tmp10_9;
    tmp11_10[0] = 'È';
    tmp11_10[1] = 'Ĭ';
    tmp11_10[2] = 'Ɛ';
    tmp11_10[3] = 'Ǵ';
    a = arrayOfInt;
    b = paramg;
    c = paramd;
    int i1 = paramd.getColumnIndexOrThrow("_id");
    d = i1;
    i1 = paramd.getColumnIndexOrThrow("date");
    e = i1;
    i1 = paramd.getColumnIndexOrThrow("number");
    f = i1;
    i1 = paramd.getColumnIndex("normalized_number");
    g = i1;
    i1 = paramd.getColumnIndex("type");
    h = i1;
    i1 = paramd.getColumnIndexOrThrow("duration");
    j = i1;
    i1 = paramd.getColumnIndexOrThrow("name");
    k = i1;
    i1 = paramd.getColumnIndex("features");
    l = i1;
    i1 = paramd.getColumnIndex("new");
    m = i1;
    i1 = paramd.getColumnIndex("is_read");
    n = i1;
    i1 = paramd.getColumnIndex("subscription_component_name");
    o = i1;
    i1 = paramd.getColumnIndex("logtype");
    i = i1;
  }
  
  public final boolean a()
  {
    int i1 = i;
    int i3 = -1;
    if (i1 != i3)
    {
      i1 = getInt(i1);
      int[] arrayOfInt = a;
      boolean bool = a.a(arrayOfInt, i1);
      if (bool)
      {
        new String[1][0] = "ContactManager.getCallLog - found non call type";
        return true;
      }
    }
    int i2 = f;
    return isNull(i2);
  }
  
  public final long b()
  {
    int i1 = d;
    return getLong(i1);
  }
  
  public final long c()
  {
    int i1 = e;
    return getLong(i1);
  }
  
  public final HistoryEvent d()
  {
    boolean bool1 = a();
    int i1 = 0;
    Object localObject1 = null;
    if (bool1) {
      return null;
    }
    HistoryEvent.a locala = new com/truecaller/data/entity/HistoryEvent$a;
    locala.<init>();
    int i2 = f;
    Object localObject2 = getString(i2);
    boolean bool2 = ab.a((String)localObject2);
    int i4 = 1;
    int i5 = 2;
    if (bool2)
    {
      locala.b("");
      localObject1 = "";
      locala.a((String)localObject1);
    }
    else
    {
      int i3 = g;
      int i6 = -1;
      if (i3 != i6) {
        localObject1 = getString(i3);
      }
      boolean bool3 = k.b((CharSequence)localObject1);
      if (bool3) {
        localObject1 = k.n((String)localObject2);
      }
      Object localObject3 = new String[0];
      AssertionUtil.AlwaysFatal.isNotNull(localObject1, (String[])localObject3);
      localObject3 = b;
      String[] arrayOfString = new String[i5];
      arrayOfString[0] = localObject1;
      arrayOfString[i4] = localObject2;
      localObject1 = ((g)localObject3).b(arrayOfString);
      localObject2 = k.n(((Number)localObject1).d());
      locala.b((String)localObject2);
      localObject2 = k.n(((Number)localObject1).a());
      locala.a((String)localObject2);
      localObject2 = ((Number)localObject1).m();
      locala.a((k.d)localObject2);
      localObject1 = ((Number)localObject1).l();
      locala.c((String)localObject1);
    }
    i1 = h;
    i1 = getInt(i1);
    switch (i1)
    {
    case 4: 
    case 7: 
    case 8: 
    case 9: 
    default: 
      i4 = 0;
      break;
    case 3: 
    case 5: 
    case 6: 
    case 10: 
      i4 = 3;
      break;
    case 2: 
      i4 = 2;
    }
    locala.a(i4);
    locala.b(4);
    i1 = e;
    long l1 = getLong(i1);
    locala.a(l1);
    i1 = d;
    localObject1 = Long.valueOf(getLong(i1));
    locala.a((Long)localObject1);
    i1 = j;
    l1 = getLong(i1);
    locala.b(l1);
    i1 = k;
    localObject1 = getString(i1);
    locala.d((String)localObject1);
    localObject1 = c.e();
    locala.e((String)localObject1);
    localObject1 = UUID.randomUUID().toString();
    locala.g((String)localObject1);
    i1 = l;
    if (i1 >= 0)
    {
      i1 = getInt(i1);
      locala.c(i1);
    }
    i1 = m;
    if (i1 >= 0)
    {
      i1 = getInt(i1);
      locala.d(i1);
    }
    i1 = n;
    if (i1 >= 0)
    {
      i1 = getInt(i1);
      locala.e(i1);
    }
    i1 = o;
    if (i1 >= 0)
    {
      localObject1 = getString(i1);
      locala.f((String)localObject1);
    }
    return a;
  }
  
  public final String e()
  {
    return c.e();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */