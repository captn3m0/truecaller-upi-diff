package com.truecaller.callhistory;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import c.g.a.b;
import c.u;
import com.google.android.gms.tasks.RuntimeExecutionException;
import com.truecaller.androidactors.ab;
import com.truecaller.androidactors.w;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.i.c;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.service.WidgetListProvider;
import com.truecaller.voip.d;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class m
  implements l
{
  final com.truecaller.calling.e.e a;
  final com.truecaller.common.g.a b;
  final d c;
  private final Context d;
  private final k e;
  private final c f;
  
  public m(Context paramContext, com.truecaller.calling.e.e parame, com.truecaller.common.g.a parama, k paramk, c paramc, d paramd)
  {
    d = paramContext;
    a = parame;
    b = parama;
    e = paramk;
    f = paramc;
    c = paramd;
  }
  
  private final String d(String paramString)
  {
    Object localObject = new com/truecaller/callhistory/m$d;
    ((m.d)localObject).<init>(this);
    localObject = (c.g.a.a)localObject;
    paramString = p.a(paramString, "tc_flag!=3", (c.g.a.a)localObject);
    localObject = new com/truecaller/callhistory/m$e;
    ((m.e)localObject).<init>(this);
    localObject = (c.g.a.a)localObject;
    paramString = p.a(paramString, "(subscription_component_name!='com.whatsapp' OR subscription_component_name IS NULL)", (c.g.a.a)localObject);
    localObject = new com/truecaller/callhistory/m$f;
    ((m.f)localObject).<init>(this);
    localObject = (c.g.a.a)localObject;
    return p.a(paramString, "(subscription_component_name!='com.truecaller.voip.manager.VOIP' OR subscription_component_name IS NULL)", (c.g.a.a)localObject);
  }
  
  public final w a()
  {
    w localw = null;
    Object localObject1;
    try
    {
      localObject1 = d;
      Object localObject2 = ((Context)localObject1).getContentResolver();
      Object localObject3 = TruecallerContract.n.d();
      localObject1 = "type IN (1,2,3) ";
      String str1 = d((String)localObject1);
      String str2 = "timestamp DESC";
      localObject1 = ((ContentResolver)localObject2).query((Uri)localObject3, null, str1, null, str2);
      if (localObject1 == null) {
        break label104;
      }
      try
      {
        localObject2 = e.a((Cursor)localObject1);
        localObject3 = m.a.a;
        localObject3 = (ab)localObject3;
        localObject2 = w.a(localObject2, (ab)localObject3);
        localObject3 = "wrap<HistoryEventCursor>…tCursor()) { it.close() }";
        c.g.b.k.a(localObject2, (String)localObject3);
        return (w)localObject2;
      }
      catch (SQLiteException localSQLiteException1) {}
      localThrowable = (Throwable)localSQLiteException2;
    }
    catch (SQLiteException localSQLiteException2)
    {
      localObject1 = null;
    }
    Throwable localThrowable;
    AssertionUtil.reportThrowableButNeverCrash(localThrowable);
    com.truecaller.util.q.a((Cursor)localObject1);
    label104:
    localw = w.b(null);
    c.g.b.k.a(localw, "wrap(null)");
    return localw;
  }
  
  public final w a(int paramInt)
  {
    try
    {
      Object localObject1 = d;
      Object localObject2 = ((Context)localObject1).getContentResolver();
      Uri localUri = TruecallerContract.n.a(paramInt);
      localObject3 = ((ContentResolver)localObject2).query(localUri, null, null, null, null);
      if (localObject3 == null) {
        break label88;
      }
      try
      {
        localObject1 = e.a((Cursor)localObject3);
        localObject2 = m.g.a;
        localObject2 = (ab)localObject2;
        localObject1 = w.a(localObject1, (ab)localObject2);
        localObject2 = "wrap<HistoryEventCursor>…tCursor()) { it.close() }";
        c.g.b.k.a(localObject1, (String)localObject2);
        return (w)localObject1;
      }
      catch (SQLiteException localSQLiteException1) {}
      localThrowable = (Throwable)localSQLiteException2;
    }
    catch (SQLiteException localSQLiteException2)
    {
      paramInt = 0;
      localObject3 = null;
    }
    Throwable localThrowable;
    AssertionUtil.reportThrowableButNeverCrash(localThrowable);
    com.truecaller.util.q.a((Cursor)localObject3);
    label88:
    Object localObject3 = w.b(null);
    c.g.b.k.a(localObject3, "wrap(null)");
    return (w)localObject3;
  }
  
  public final w a(long paramLong)
  {
    try
    {
      Object localObject1 = d;
      Object localObject2 = ((Context)localObject1).getContentResolver();
      Uri localUri = TruecallerContract.n.d();
      String str1 = "(call_log_id NOT NULL OR tc_flag=2 OR tc_flag=3) AND new=1 AND type=3 AND action NOT IN (5,1,3,4) AND timestamp<=?";
      int i = 1;
      String[] arrayOfString = new String[i];
      i = 0;
      localObject1 = null;
      localObject3 = String.valueOf(paramLong);
      arrayOfString[0] = localObject3;
      String str2 = "timestamp DESC";
      localObject3 = ((ContentResolver)localObject2).query(localUri, null, str1, arrayOfString, str2);
      if (localObject3 == null) {
        break label158;
      }
      try
      {
        Object localObject4 = e.a((Cursor)localObject3);
        localObject1 = m.h.a;
        localObject1 = (b)localObject1;
        if (localObject1 != null)
        {
          localObject2 = new com/truecaller/callhistory/q;
          ((q)localObject2).<init>((b)localObject1);
          localObject1 = localObject2;
        }
        localObject1 = (ab)localObject1;
        localObject4 = w.a(localObject4, (ab)localObject1);
        localObject1 = "wrap<HistoryEventCursor>…istoryEventCursor::close)";
        c.g.b.k.a(localObject4, (String)localObject1);
        return (w)localObject4;
      }
      catch (SQLiteException localSQLiteException1) {}
      localThrowable = (Throwable)localSQLiteException2;
    }
    catch (SQLiteException localSQLiteException2)
    {
      localObject3 = null;
    }
    Throwable localThrowable;
    AssertionUtil.reportThrowableButNeverCrash(localThrowable);
    com.truecaller.util.q.a((Cursor)localObject3);
    label158:
    Object localObject3 = w.b(null);
    c.g.b.k.a(localObject3, "wrap(null)");
    return (w)localObject3;
  }
  
  /* Error */
  public final w a(FilterType paramFilterType, Integer paramInteger)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc -83
    //   3: invokestatic 25	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aconst_null
    //   7: astore_3
    //   8: aload_0
    //   9: getfield 41	com/truecaller/callhistory/m:d	Landroid/content/Context;
    //   12: astore 4
    //   14: aload 4
    //   16: invokevirtual 81	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   19: astore 5
    //   21: invokestatic 175	com/truecaller/content/TruecallerContract$n:e	()Landroid/net/Uri;
    //   24: astore 6
    //   26: ldc 93
    //   28: astore 4
    //   30: aload_2
    //   31: ifnull +52 -> 83
    //   34: new 177	java/lang/StringBuilder
    //   37: astore 7
    //   39: aload 7
    //   41: invokespecial 178	java/lang/StringBuilder:<init>	()V
    //   44: aload 7
    //   46: aload 4
    //   48: invokevirtual 182	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   51: pop
    //   52: ldc -72
    //   54: astore 4
    //   56: aload 7
    //   58: aload 4
    //   60: invokevirtual 182	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   63: pop
    //   64: aload 7
    //   66: aload_2
    //   67: invokevirtual 187	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   70: pop
    //   71: aload 7
    //   73: invokevirtual 191	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   76: astore_2
    //   77: aload_2
    //   78: astore 8
    //   80: goto +7 -> 87
    //   83: aload 4
    //   85: astore 8
    //   87: getstatic 196	com/truecaller/callhistory/n:b	[I
    //   90: astore_2
    //   91: aload_1
    //   92: invokevirtual 202	com/truecaller/callhistory/FilterType:ordinal	()I
    //   95: istore 9
    //   97: aload_2
    //   98: iload 9
    //   100: iaload
    //   101: istore 10
    //   103: iconst_1
    //   104: istore 9
    //   106: iload 10
    //   108: tableswitch	default:+28->136, 1:+101->209, 2:+68->176, 3:+35->143
    //   136: getstatic 204	com/truecaller/callhistory/n:a	[I
    //   139: astore_2
    //   140: goto +102 -> 242
    //   143: aconst_null
    //   144: astore 7
    //   146: ldc -50
    //   148: astore_1
    //   149: aload_0
    //   150: aload_1
    //   151: invokespecial 91	com/truecaller/callhistory/m:d	(Ljava/lang/String;)Ljava/lang/String;
    //   154: astore 11
    //   156: aconst_null
    //   157: astore 12
    //   159: aload 5
    //   161: aload 6
    //   163: aconst_null
    //   164: aload 11
    //   166: aconst_null
    //   167: aload 8
    //   169: invokevirtual 99	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   172: astore_1
    //   173: goto +178 -> 351
    //   176: aconst_null
    //   177: astore 7
    //   179: ldc -48
    //   181: astore_1
    //   182: aload_0
    //   183: aload_1
    //   184: invokespecial 91	com/truecaller/callhistory/m:d	(Ljava/lang/String;)Ljava/lang/String;
    //   187: astore 11
    //   189: aconst_null
    //   190: astore 12
    //   192: aload 5
    //   194: aload 6
    //   196: aconst_null
    //   197: aload 11
    //   199: aconst_null
    //   200: aload 8
    //   202: invokevirtual 99	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   205: astore_1
    //   206: goto +145 -> 351
    //   209: aconst_null
    //   210: astore 7
    //   212: ldc 88
    //   214: astore_1
    //   215: aload_0
    //   216: aload_1
    //   217: invokespecial 91	com/truecaller/callhistory/m:d	(Ljava/lang/String;)Ljava/lang/String;
    //   220: astore 11
    //   222: aconst_null
    //   223: astore 12
    //   225: aload 5
    //   227: aload 6
    //   229: aconst_null
    //   230: aload 11
    //   232: aconst_null
    //   233: aload 8
    //   235: invokevirtual 99	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   238: astore_1
    //   239: goto +112 -> 351
    //   242: aload_1
    //   243: invokevirtual 202	com/truecaller/callhistory/FilterType:ordinal	()I
    //   246: istore 13
    //   248: aload_2
    //   249: iload 13
    //   251: iaload
    //   252: istore 13
    //   254: iconst_0
    //   255: istore 10
    //   257: aconst_null
    //   258: astore_2
    //   259: iload 13
    //   261: tableswitch	default:+27->288, 1:+47->308, 2:+41->302, 3:+35->296
    //   288: iconst_0
    //   289: istore 13
    //   291: aconst_null
    //   292: astore_1
    //   293: goto +18 -> 311
    //   296: iconst_1
    //   297: istore 13
    //   299: goto +12 -> 311
    //   302: iconst_2
    //   303: istore 13
    //   305: goto +6 -> 311
    //   308: iconst_3
    //   309: istore 13
    //   311: aconst_null
    //   312: astore 7
    //   314: ldc -44
    //   316: astore 11
    //   318: iload 9
    //   320: anewarray 153	java/lang/String
    //   323: astore 12
    //   325: iload 13
    //   327: invokestatic 215	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   330: astore_1
    //   331: aload 12
    //   333: iconst_0
    //   334: aload_1
    //   335: aastore
    //   336: aload 5
    //   338: aload 6
    //   340: aconst_null
    //   341: aload 11
    //   343: aload 12
    //   345: aload 8
    //   347: invokevirtual 99	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   350: astore_1
    //   351: aload_1
    //   352: ifnull +123 -> 475
    //   355: new 217	com/truecaller/callhistory/aa
    //   358: astore_2
    //   359: new 219	com/truecaller/data/access/e
    //   362: astore 5
    //   364: aload 5
    //   366: aload_1
    //   367: invokespecial 221	com/truecaller/data/access/e:<init>	(Landroid/database/Cursor;)V
    //   370: new 223	com/truecaller/data/access/d
    //   373: astore 6
    //   375: aload 6
    //   377: aload_1
    //   378: invokespecial 224	com/truecaller/data/access/d:<init>	(Landroid/database/Cursor;)V
    //   381: aload_2
    //   382: aload_1
    //   383: aload 5
    //   385: aload 6
    //   387: iload 9
    //   389: invokespecial 227	com/truecaller/callhistory/aa:<init>	(Landroid/database/Cursor;Lcom/truecaller/data/access/e;Lcom/truecaller/data/access/d;Z)V
    //   392: new 229	java/util/ArrayList
    //   395: astore 4
    //   397: aload 4
    //   399: invokespecial 230	java/util/ArrayList:<init>	()V
    //   402: aload_2
    //   403: invokevirtual 234	com/truecaller/callhistory/aa:moveToNext	()Z
    //   406: istore 14
    //   408: iload 14
    //   410: ifeq +25 -> 435
    //   413: aload_2
    //   414: invokevirtual 237	com/truecaller/callhistory/aa:d	()Lcom/truecaller/data/entity/HistoryEvent;
    //   417: astore 5
    //   419: aload 5
    //   421: ifnull -19 -> 402
    //   424: aload 4
    //   426: aload 5
    //   428: invokevirtual 241	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   431: pop
    //   432: goto -30 -> 402
    //   435: aload 4
    //   437: invokestatic 136	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   440: astore_2
    //   441: ldc -13
    //   443: astore 4
    //   445: aload_2
    //   446: aload 4
    //   448: invokestatic 120	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   451: aload_1
    //   452: invokestatic 133	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   455: aload_2
    //   456: areturn
    //   457: astore_2
    //   458: aload_1
    //   459: astore_3
    //   460: aload_2
    //   461: astore_1
    //   462: goto +59 -> 521
    //   465: astore 15
    //   467: aload_1
    //   468: astore_2
    //   469: aload 15
    //   471: astore_1
    //   472: goto +20 -> 492
    //   475: aload_1
    //   476: invokestatic 133	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   479: goto +26 -> 505
    //   482: astore_1
    //   483: goto +38 -> 521
    //   486: astore_1
    //   487: iconst_0
    //   488: istore 10
    //   490: aconst_null
    //   491: astore_2
    //   492: aload_1
    //   493: checkcast 122	java/lang/Throwable
    //   496: astore_1
    //   497: aload_1
    //   498: invokestatic 128	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   501: aload_2
    //   502: invokestatic 133	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   505: aconst_null
    //   506: invokestatic 136	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   509: astore_1
    //   510: aload_1
    //   511: ldc -118
    //   513: invokestatic 120	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   516: aload_1
    //   517: areturn
    //   518: astore_1
    //   519: aload_2
    //   520: astore_3
    //   521: aload_3
    //   522: invokestatic 133	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   525: aload_1
    //   526: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	527	0	this	m
    //   0	527	1	paramFilterType	FilterType
    //   0	527	2	paramInteger	Integer
    //   7	515	3	localObject1	Object
    //   12	435	4	localObject2	Object
    //   19	408	5	localObject3	Object
    //   24	362	6	localObject4	Object
    //   37	276	7	localStringBuilder	StringBuilder
    //   78	268	8	localObject5	Object
    //   95	293	9	i	int
    //   101	388	10	j	int
    //   154	188	11	str	String
    //   157	187	12	arrayOfString	String[]
    //   246	80	13	k	int
    //   406	3	14	bool	boolean
    //   465	5	15	localSQLiteException	SQLiteException
    // Exception table:
    //   from	to	target	type
    //   355	358	457	finally
    //   359	362	457	finally
    //   366	370	457	finally
    //   370	373	457	finally
    //   377	381	457	finally
    //   387	392	457	finally
    //   392	395	457	finally
    //   397	402	457	finally
    //   402	406	457	finally
    //   413	417	457	finally
    //   426	432	457	finally
    //   435	440	457	finally
    //   446	451	457	finally
    //   355	358	465	android/database/sqlite/SQLiteException
    //   359	362	465	android/database/sqlite/SQLiteException
    //   366	370	465	android/database/sqlite/SQLiteException
    //   370	373	465	android/database/sqlite/SQLiteException
    //   377	381	465	android/database/sqlite/SQLiteException
    //   387	392	465	android/database/sqlite/SQLiteException
    //   392	395	465	android/database/sqlite/SQLiteException
    //   397	402	465	android/database/sqlite/SQLiteException
    //   402	406	465	android/database/sqlite/SQLiteException
    //   413	417	465	android/database/sqlite/SQLiteException
    //   426	432	465	android/database/sqlite/SQLiteException
    //   435	440	465	android/database/sqlite/SQLiteException
    //   446	451	465	android/database/sqlite/SQLiteException
    //   8	12	482	finally
    //   14	19	482	finally
    //   21	24	482	finally
    //   34	37	482	finally
    //   39	44	482	finally
    //   46	52	482	finally
    //   58	64	482	finally
    //   66	71	482	finally
    //   71	76	482	finally
    //   87	90	482	finally
    //   91	95	482	finally
    //   98	101	482	finally
    //   136	139	482	finally
    //   150	154	482	finally
    //   167	172	482	finally
    //   183	187	482	finally
    //   200	205	482	finally
    //   216	220	482	finally
    //   233	238	482	finally
    //   242	246	482	finally
    //   249	252	482	finally
    //   318	323	482	finally
    //   325	330	482	finally
    //   334	336	482	finally
    //   345	350	482	finally
    //   8	12	486	android/database/sqlite/SQLiteException
    //   14	19	486	android/database/sqlite/SQLiteException
    //   21	24	486	android/database/sqlite/SQLiteException
    //   34	37	486	android/database/sqlite/SQLiteException
    //   39	44	486	android/database/sqlite/SQLiteException
    //   46	52	486	android/database/sqlite/SQLiteException
    //   58	64	486	android/database/sqlite/SQLiteException
    //   66	71	486	android/database/sqlite/SQLiteException
    //   71	76	486	android/database/sqlite/SQLiteException
    //   87	90	486	android/database/sqlite/SQLiteException
    //   91	95	486	android/database/sqlite/SQLiteException
    //   98	101	486	android/database/sqlite/SQLiteException
    //   136	139	486	android/database/sqlite/SQLiteException
    //   150	154	486	android/database/sqlite/SQLiteException
    //   167	172	486	android/database/sqlite/SQLiteException
    //   183	187	486	android/database/sqlite/SQLiteException
    //   200	205	486	android/database/sqlite/SQLiteException
    //   216	220	486	android/database/sqlite/SQLiteException
    //   233	238	486	android/database/sqlite/SQLiteException
    //   242	246	486	android/database/sqlite/SQLiteException
    //   249	252	486	android/database/sqlite/SQLiteException
    //   318	323	486	android/database/sqlite/SQLiteException
    //   325	330	486	android/database/sqlite/SQLiteException
    //   334	336	486	android/database/sqlite/SQLiteException
    //   345	350	486	android/database/sqlite/SQLiteException
    //   492	496	518	finally
    //   497	501	518	finally
  }
  
  public final w a(Contact paramContact)
  {
    c.g.b.k.b(paramContact, "contact");
    try
    {
      Object localObject1 = d;
      Object localObject2 = ((Context)localObject1).getContentResolver();
      paramContact = paramContact.getId();
      paramContact = String.valueOf(paramContact);
      Uri localUri = TruecallerContract.n.d();
      localObject1 = "type IN (1,2,3)  AND (history_aggregated_contact_id=? OR history_aggregated_contact_tc_id=?)";
      String str1 = d((String)localObject1);
      int i = 2;
      String[] arrayOfString = new String[i];
      i = 0;
      localObject1 = null;
      arrayOfString[0] = paramContact;
      i = 1;
      arrayOfString[i] = paramContact;
      String str2 = "timestamp DESC";
      paramContact = ((ContentResolver)localObject2).query(localUri, null, str1, arrayOfString, str2);
      if (paramContact == null) {
        break label143;
      }
      try
      {
        localObject1 = e.a(paramContact);
        localObject2 = m.c.a;
        localObject2 = (ab)localObject2;
        localObject1 = w.a(localObject1, (ab)localObject2);
        localObject2 = "wrap<HistoryEventCursor>…tCursor()) { it.close() }";
        c.g.b.k.a(localObject1, (String)localObject2);
        return (w)localObject1;
      }
      catch (SQLiteException localSQLiteException1) {}
      localThrowable = (Throwable)localSQLiteException2;
    }
    catch (SQLiteException localSQLiteException2)
    {
      paramContact = null;
    }
    Throwable localThrowable;
    AssertionUtil.reportThrowableButNeverCrash(localThrowable);
    com.truecaller.util.q.a(paramContact);
    label143:
    paramContact = w.b(null);
    c.g.b.k.a(paramContact, "wrap(null)");
    return paramContact;
  }
  
  public final w a(String paramString)
  {
    c.g.b.k.b(paramString, "normalizedNumber");
    boolean bool = org.c.a.a.a.k.b((CharSequence)paramString);
    Object localObject1 = null;
    Object localObject2 = new String[0];
    AssertionUtil.AlwaysFatal.isFalse(bool, (String[])localObject2);
    bool = false;
    try
    {
      localObject2 = d;
      ContentResolver localContentResolver = ((Context)localObject2).getContentResolver();
      Uri localUri = TruecallerContract.n.d();
      localObject2 = "type IN (1,2,3)  AND normalized_number=?";
      String str1 = d((String)localObject2);
      int i = 1;
      String[] arrayOfString = new String[i];
      arrayOfString[0] = paramString;
      String str2 = "timestamp DESC";
      paramString = localContentResolver.query(localUri, null, str1, arrayOfString, str2);
      if (paramString == null) {
        break label156;
      }
      try
      {
        localObject1 = e.a(paramString);
        localObject2 = m.b.a;
        localObject2 = (ab)localObject2;
        localObject1 = w.a(localObject1, (ab)localObject2);
        localObject2 = "wrap<HistoryEventCursor>…tCursor()) { it.close() }";
        c.g.b.k.a(localObject1, (String)localObject2);
        return (w)localObject1;
      }
      catch (SQLiteException localSQLiteException1) {}
      localThrowable = (Throwable)localSQLiteException2;
    }
    catch (SQLiteException localSQLiteException2)
    {
      paramString = null;
    }
    Throwable localThrowable;
    AssertionUtil.reportThrowableButNeverCrash(localThrowable);
    com.truecaller.util.q.a(paramString);
    label156:
    paramString = w.b(null);
    c.g.b.k.a(paramString, "wrap(null)");
    return paramString;
  }
  
  public final w a(List paramList)
  {
    Object localObject1 = "eventsToRestore";
    c.g.b.k.b(paramList, (String)localObject1);
    boolean bool = paramList.isEmpty();
    if (bool)
    {
      paramList = w.b(Integer.valueOf(0));
      c.g.b.k.a(paramList, "Promise.wrap(0)");
      return paramList;
    }
    localObject1 = d.getContentResolver();
    int i = paramList.size();
    Object localObject2 = new ContentValues[i];
    Object localObject3 = paramList;
    localObject3 = (Collection)paramList;
    int j = ((Collection)localObject3).size();
    int k = 0;
    while (k < j)
    {
      ContentValues localContentValues = e.a((HistoryEvent)paramList.get(k));
      localObject2[k] = localContentValues;
      k += 1;
    }
    paramList = TruecallerContract.n.a();
    int m = ((ContentResolver)localObject1).bulkInsert(paramList, (ContentValues[])localObject2);
    localObject1 = new String[1];
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    ((StringBuilder)localObject2).append(m);
    ((StringBuilder)localObject2).append(" HistoryTable rows inserted from backup");
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    paramList = w.b(Integer.valueOf(m));
    c.g.b.k.a(paramList, "Promise.wrap(insertedRows)");
    return paramList;
  }
  
  public final boolean a(HistoryEvent paramHistoryEvent)
  {
    c.g.b.k.b(paramHistoryEvent, "event");
    Object localObject1 = null;
    try
    {
      long l1 = paramHistoryEvent.j();
      long l2 = 10000L;
      l1 -= l2;
      long l3 = paramHistoryEvent.j() + l2;
      Object localObject2 = paramHistoryEvent.a();
      localObject2 = p.a((String)localObject2);
      Context localContext = d;
      Object localObject3 = localContext.getContentResolver();
      Object localObject4 = TruecallerContract.n.a();
      Object localObject5 = null;
      String str1 = "normalized_number=? AND action=4 AND timestamp>=? AND timestamp<=? AND (call_log_id NOT NULL OR tc_flag=2 OR tc_flag=3) AND tc_flag=0";
      int i = 3;
      String[] arrayOfString = new String[i];
      i = 0;
      localContext = null;
      arrayOfString[0] = localObject2;
      Object localObject6 = String.valueOf(l1);
      int j = 1;
      arrayOfString[j] = localObject6;
      int k = 2;
      localObject2 = String.valueOf(l3);
      arrayOfString[k] = localObject2;
      String str2 = "timestamp";
      localObject1 = ((ContentResolver)localObject3).query((Uri)localObject4, null, str1, arrayOfString, str2);
      if (localObject1 != null)
      {
        localObject6 = new com/truecaller/callhistory/aa;
        ((aa)localObject6).<init>((Cursor)localObject1);
        boolean bool2;
        do
        {
          do
          {
            bool1 = ((aa)localObject6).moveToNext();
            if (!bool1) {
              break;
            }
            localObject2 = ((aa)localObject6).d();
          } while (localObject2 == null);
          int m = paramHistoryEvent.f();
          int i1 = ((HistoryEvent)localObject2).f();
          localObject3 = paramHistoryEvent.a();
          localObject4 = ((HistoryEvent)localObject2).a();
          long l4 = paramHistoryEvent.j();
          long l5 = ((HistoryEvent)localObject2).j();
          bool2 = ao.a(m, i1, (String)localObject3, (String)localObject4, l4, l5);
        } while (!bool2);
        k = paramHistoryEvent.h();
        int n = 5;
        if (k == n)
        {
          k = 1;
        }
        else
        {
          k = 0;
          localObject6 = null;
        }
        ContentValues localContentValues = new android/content/ContentValues;
        localContentValues.<init>();
        Object localObject7 = "action";
        int i2 = paramHistoryEvent.h();
        localObject3 = Integer.valueOf(i2);
        localContentValues.put((String)localObject7, (Integer)localObject3);
        localObject7 = "event_id";
        localObject3 = paramHistoryEvent.u();
        localContentValues.put((String)localObject7, (String)localObject3);
        if (k != 0)
        {
          localObject7 = "call_log_id";
          localContentValues.putNull((String)localObject7);
        }
        localObject7 = d;
        localObject7 = ((Context)localObject7).getContentResolver();
        localObject3 = TruecallerContract.n.a();
        localObject4 = "_id=?";
        Object localObject8 = new String[j];
        localObject5 = ((HistoryEvent)localObject2).getId();
        localObject5 = String.valueOf(localObject5);
        localObject8[0] = localObject5;
        j = ((ContentResolver)localObject7).update((Uri)localObject3, localContentValues, (String)localObject4, (String[])localObject8);
        if (j > 0)
        {
          localObject8 = ((HistoryEvent)localObject2).getId();
          paramHistoryEvent.setId((Long)localObject8);
          if (k == 0)
          {
            localObject8 = ((HistoryEvent)localObject2).i();
            paramHistoryEvent.a((Long)localObject8);
          }
          long l6 = ((HistoryEvent)localObject2).j();
          paramHistoryEvent.a(l6);
          long l7 = ((HistoryEvent)localObject2).k();
          paramHistoryEvent.b(l7);
          return k;
        }
        return false;
      }
      com.truecaller.util.q.a((Cursor)localObject1);
      localObject1 = d.getContentResolver();
      localObject6 = TruecallerContract.n.a();
      localObject2 = e.a(paramHistoryEvent);
      localObject1 = ((ContentResolver)localObject1).insert((Uri)localObject6, (ContentValues)localObject2);
      if (localObject1 == null) {
        return false;
      }
      long l8 = ContentUris.parseId((Uri)localObject1);
      l3 = -1;
      boolean bool1 = l8 < l3;
      if (!bool1) {
        return false;
      }
      localObject1 = Long.valueOf(l8);
      paramHistoryEvent.setId((Long)localObject1);
      return j;
    }
    finally
    {
      com.truecaller.util.q.a((Cursor)localObject1);
    }
  }
  
  public final boolean a(Set paramSet)
  {
    c.g.b.k.b(paramSet, "callLogIds");
    try
    {
      Object localObject1 = new java/lang/StringBuilder;
      Object localObject2 = "IN (";
      ((StringBuilder)localObject1).<init>((String)localObject2);
      localObject2 = "?";
      Object localObject3 = ",";
      int i = paramSet.size();
      localObject2 = org.c.a.a.a.k.a((String)localObject2, (String)localObject3, i);
      ((StringBuilder)localObject1).append((String)localObject2);
      char c1 = ')';
      ((StringBuilder)localObject1).append(c1);
      localObject1 = ((StringBuilder)localObject1).toString();
      paramSet = (Iterable)paramSet;
      localObject2 = new java/util/ArrayList;
      int j = 10;
      j = c.a.m.a(paramSet, j);
      ((ArrayList)localObject2).<init>(j);
      localObject2 = (Collection)localObject2;
      paramSet = paramSet.iterator();
      for (;;)
      {
        boolean bool = paramSet.hasNext();
        if (!bool) {
          break;
        }
        localObject3 = paramSet.next();
        localObject3 = (Number)localObject3;
        long l = ((Number)localObject3).longValue();
        localObject3 = String.valueOf(l);
        ((Collection)localObject2).add(localObject3);
      }
      localObject2 = (List)localObject2;
      localObject2 = (Collection)localObject2;
      paramSet = new String[0];
      paramSet = ((Collection)localObject2).toArray(paramSet);
      if (paramSet != null)
      {
        paramSet = (String[])paramSet;
        localObject2 = d;
        localObject2 = ((Context)localObject2).getContentResolver();
        localObject3 = new android/content/ContentValues;
        ((ContentValues)localObject3).<init>();
        Object localObject4 = "new";
        Integer localInteger = Integer.valueOf(0);
        ((ContentValues)localObject3).put((String)localObject4, localInteger);
        localObject4 = "is_read";
        int k = 1;
        Object localObject5 = Integer.valueOf(k);
        ((ContentValues)localObject3).put((String)localObject4, (Integer)localObject5);
        localObject4 = e;
        localObject4 = ((k)localObject4).a();
        localObject5 = "_id ";
        String str = String.valueOf(localObject1);
        localObject5 = ((String)localObject5).concat(str);
        i = ((ContentResolver)localObject2).update((Uri)localObject4, (ContentValues)localObject3, (String)localObject5, paramSet);
        if (i != 0)
        {
          ((ContentValues)localObject3).clear();
          localObject4 = "new";
          localObject5 = Integer.valueOf(0);
          ((ContentValues)localObject3).put((String)localObject4, (Integer)localObject5);
          localObject4 = "is_read";
          localObject5 = Integer.valueOf(k);
          ((ContentValues)localObject3).put((String)localObject4, (Integer)localObject5);
          localObject4 = TruecallerContract.n.a();
          localObject5 = "call_log_id ";
          localObject1 = String.valueOf(localObject1);
          localObject1 = ((String)localObject5).concat((String)localObject1);
          ((ContentResolver)localObject2).update((Uri)localObject4, (ContentValues)localObject3, (String)localObject1, paramSet);
        }
        return k;
      }
      paramSet = new c/u;
      localObject1 = "null cannot be cast to non-null type kotlin.Array<T>";
      paramSet.<init>((String)localObject1);
      throw paramSet;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localIllegalArgumentException);
      return false;
    }
    catch (SecurityException localSecurityException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSecurityException);
      return false;
    }
    catch (RuntimeExecutionException localRuntimeExecutionException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localRuntimeExecutionException);
    }
    return false;
  }
  
  /* Error */
  public final w b()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aload_0
    //   3: getfield 41	com/truecaller/callhistory/m:d	Landroid/content/Context;
    //   6: astore_2
    //   7: aload_2
    //   8: invokevirtual 81	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   11: astore_3
    //   12: invokestatic 86	com/truecaller/content/TruecallerContract$n:d	()Landroid/net/Uri;
    //   15: astore 4
    //   17: ldc_w 503
    //   20: astore_2
    //   21: iconst_1
    //   22: anewarray 153	java/lang/String
    //   25: dup
    //   26: iconst_0
    //   27: aload_2
    //   28: aastore
    //   29: astore 5
    //   31: ldc_w 505
    //   34: astore 6
    //   36: aload_3
    //   37: aload 4
    //   39: aload 5
    //   41: aload 6
    //   43: aconst_null
    //   44: aconst_null
    //   45: invokevirtual 99	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   48: astore_2
    //   49: aload_2
    //   50: ifnull +84 -> 134
    //   53: aload_2
    //   54: invokeinterface 510 1 0
    //   59: istore 7
    //   61: iload 7
    //   63: ifeq +71 -> 134
    //   66: iconst_0
    //   67: istore 7
    //   69: aconst_null
    //   70: astore_3
    //   71: aload_2
    //   72: iconst_0
    //   73: invokeinterface 514 2 0
    //   78: istore 7
    //   80: iload 7
    //   82: invokestatic 295	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   85: astore_3
    //   86: aload_3
    //   87: invokestatic 136	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   90: astore_3
    //   91: ldc_w 516
    //   94: astore 4
    //   96: aload_3
    //   97: aload 4
    //   99: invokestatic 120	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   102: aload_2
    //   103: invokestatic 133	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   106: aload_3
    //   107: areturn
    //   108: astore_3
    //   109: goto +16 -> 125
    //   112: astore 8
    //   114: aconst_null
    //   115: astore_2
    //   116: aload 8
    //   118: astore_1
    //   119: goto +33 -> 152
    //   122: astore_3
    //   123: aconst_null
    //   124: astore_2
    //   125: aload_3
    //   126: checkcast 122	java/lang/Throwable
    //   129: astore_3
    //   130: aload_3
    //   131: invokestatic 128	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   134: aload_2
    //   135: invokestatic 133	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   138: aconst_null
    //   139: invokestatic 136	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   142: astore_1
    //   143: aload_1
    //   144: ldc -118
    //   146: invokestatic 120	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   149: aload_1
    //   150: areturn
    //   151: astore_1
    //   152: aload_2
    //   153: invokestatic 133	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   156: aload_1
    //   157: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	158	0	this	m
    //   1	149	1	localObject1	Object
    //   151	6	1	localObject2	Object
    //   6	147	2	localObject3	Object
    //   11	96	3	localObject4	Object
    //   108	1	3	localSQLiteException1	SQLiteException
    //   122	4	3	localSQLiteException2	SQLiteException
    //   129	2	3	localThrowable	Throwable
    //   15	83	4	localObject5	Object
    //   29	11	5	arrayOfString	String[]
    //   34	8	6	str	String
    //   59	9	7	bool	boolean
    //   78	3	7	i	int
    //   112	5	8	localObject6	Object
    // Exception table:
    //   from	to	target	type
    //   53	59	108	android/database/sqlite/SQLiteException
    //   72	78	108	android/database/sqlite/SQLiteException
    //   80	85	108	android/database/sqlite/SQLiteException
    //   86	90	108	android/database/sqlite/SQLiteException
    //   97	102	108	android/database/sqlite/SQLiteException
    //   2	6	112	finally
    //   7	11	112	finally
    //   12	15	112	finally
    //   21	29	112	finally
    //   44	48	112	finally
    //   2	6	122	android/database/sqlite/SQLiteException
    //   7	11	122	android/database/sqlite/SQLiteException
    //   12	15	122	android/database/sqlite/SQLiteException
    //   21	29	122	android/database/sqlite/SQLiteException
    //   44	48	122	android/database/sqlite/SQLiteException
    //   53	59	151	finally
    //   72	78	151	finally
    //   80	85	151	finally
    //   86	90	151	finally
    //   97	102	151	finally
    //   125	129	151	finally
    //   130	134	151	finally
  }
  
  public final w b(String paramString)
  {
    c.g.b.k.b(paramString, "normalizedNumber");
    Object localObject1 = d.getContentResolver();
    Object localObject2 = null;
    try
    {
      Uri localUri = TruecallerContract.n.d();
      String str1 = "(call_log_id NOT NULL OR tc_flag=2 OR tc_flag=3) AND type IN (1,2,3) AND action!=1 AND normalized_number=?";
      int i = 1;
      String[] arrayOfString = new String[i];
      String str2 = null;
      arrayOfString[0] = paramString;
      str2 = "timestamp DESC LIMIT 1";
      paramString = ((ContentResolver)localObject1).query(localUri, null, str1, arrayOfString, str2);
      if (paramString != null) {
        try
        {
          localObject1 = e.a(paramString);
          boolean bool = ((aa)localObject1).moveToFirst();
          if (bool)
          {
            localObject2 = ((aa)localObject1).d();
            localObject2 = w.b(localObject2);
            localObject1 = "Promise.wrap(eventCursor.historyEvent)";
            c.g.b.k.a(localObject2, (String)localObject1);
            com.truecaller.util.q.a(paramString);
            return (w)localObject2;
          }
        }
        finally
        {
          localObject2 = paramString;
          paramString = (String)localObject3;
          break label138;
        }
      }
      com.truecaller.util.q.a(paramString);
      paramString = w.b(null);
      c.g.b.k.a(paramString, "Promise.wrap(null)");
      return paramString;
    }
    finally
    {
      label138:
      com.truecaller.util.q.a((Cursor)localObject2);
    }
  }
  
  public final void b(long paramLong)
  {
    try
    {
      Object localObject1 = d;
      localObject1 = ((Context)localObject1).getContentResolver();
      ContentValues localContentValues = new android/content/ContentValues;
      localContentValues.<init>();
      Object localObject2 = "new";
      Object localObject3 = null;
      Object localObject4 = Integer.valueOf(0);
      localContentValues.put((String)localObject2, (Integer)localObject4);
      localObject2 = TruecallerContract.n.a();
      localObject4 = "timestamp<=";
      String str1 = String.valueOf(paramLong);
      localObject4 = ((String)localObject4).concat(str1);
      str1 = null;
      ((ContentResolver)localObject1).update((Uri)localObject2, localContentValues, (String)localObject4, null);
      localContentValues.clear();
      localObject2 = "new";
      localObject3 = Integer.valueOf(0);
      localContentValues.put((String)localObject2, (Integer)localObject3);
      localObject2 = e;
      localObject2 = ((k)localObject2).a();
      localObject3 = "date<=";
      String str2 = String.valueOf(paramLong);
      str2 = ((String)localObject3).concat(str2);
      ((ContentResolver)localObject1).update((Uri)localObject2, localContentValues, str2, null);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localIllegalArgumentException);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSecurityException);
      return;
    }
    catch (RuntimeExecutionException localRuntimeExecutionException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localRuntimeExecutionException);
    }
  }
  
  public final w c(String paramString)
  {
    c.g.b.k.b(paramString, "tcId");
    Object localObject1 = d.getContentResolver();
    Object localObject2 = null;
    try
    {
      Uri localUri = TruecallerContract.n.d();
      String str1 = "(call_log_id NOT NULL OR tc_flag=2 OR tc_flag=3) AND type IN (1,2,3)  AND action!=1 AND tc_id=?";
      int i = 1;
      String[] arrayOfString = new String[i];
      String str2 = null;
      arrayOfString[0] = paramString;
      str2 = "timestamp DESC LIMIT 1";
      paramString = ((ContentResolver)localObject1).query(localUri, null, str1, arrayOfString, str2);
      if (paramString != null) {
        try
        {
          localObject1 = e.a(paramString);
          boolean bool = ((aa)localObject1).moveToFirst();
          if (bool)
          {
            localObject2 = ((aa)localObject1).d();
            localObject2 = w.b(localObject2);
            localObject1 = "Promise.wrap(eventCursor.historyEvent)";
            c.g.b.k.a(localObject2, (String)localObject1);
            com.truecaller.util.q.a(paramString);
            return (w)localObject2;
          }
        }
        finally
        {
          localObject2 = paramString;
          paramString = (String)localObject3;
          break label138;
        }
      }
      com.truecaller.util.q.a(paramString);
      paramString = w.b(null);
      c.g.b.k.a(paramString, "Promise.wrap(null)");
      return paramString;
    }
    finally
    {
      label138:
      com.truecaller.util.q.a((Cursor)localObject2);
    }
  }
  
  public final void c()
  {
    try
    {
      Object localObject1 = d;
      localObject1 = ((Context)localObject1).getContentResolver();
      ContentValues localContentValues = new android/content/ContentValues;
      localContentValues.<init>();
      Object localObject2 = "new";
      Integer localInteger1 = null;
      Integer localInteger2 = Integer.valueOf(0);
      localContentValues.put((String)localObject2, localInteger2);
      localObject2 = "is_read";
      int i = 1;
      Object localObject3 = Integer.valueOf(i);
      localContentValues.put((String)localObject2, (Integer)localObject3);
      localObject2 = TruecallerContract.n.a();
      localObject3 = "new=1 OR is_read=0";
      ((ContentResolver)localObject1).update((Uri)localObject2, localContentValues, (String)localObject3, null);
      localContentValues.clear();
      localObject2 = "new";
      localInteger1 = Integer.valueOf(0);
      localContentValues.put((String)localObject2, localInteger1);
      localObject2 = "is_read";
      localInteger1 = Integer.valueOf(i);
      localContentValues.put((String)localObject2, localInteger1);
      localObject2 = e;
      localObject2 = ((k)localObject2).a();
      ((ContentResolver)localObject1).update((Uri)localObject2, localContentValues, null, null);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localIllegalArgumentException);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSecurityException);
      return;
    }
    catch (RuntimeExecutionException localRuntimeExecutionException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localRuntimeExecutionException);
    }
  }
  
  public final void c(long paramLong)
  {
    try
    {
      Object localObject1 = d;
      localObject1 = ((Context)localObject1).getContentResolver();
      ContentValues localContentValues = new android/content/ContentValues;
      localContentValues.<init>();
      Object localObject2 = "new";
      Integer localInteger1 = Integer.valueOf(0);
      localContentValues.put((String)localObject2, localInteger1);
      localObject2 = "is_read";
      int i = 1;
      Object localObject3 = Integer.valueOf(i);
      localContentValues.put((String)localObject2, (Integer)localObject3);
      localObject2 = e;
      localObject2 = ((k)localObject2).a();
      localObject3 = "_id=?";
      String[] arrayOfString = new String[i];
      localObject4 = String.valueOf(paramLong);
      arrayOfString[0] = localObject4;
      int j = ((ContentResolver)localObject1).update((Uri)localObject2, localContentValues, (String)localObject3, arrayOfString);
      if (j != 0)
      {
        localContentValues.clear();
        localObject4 = "new";
        Integer localInteger2 = Integer.valueOf(0);
        localContentValues.put((String)localObject4, localInteger2);
        localObject4 = "is_read";
        localInteger2 = Integer.valueOf(i);
        localContentValues.put((String)localObject4, localInteger2);
        localObject4 = TruecallerContract.n.a();
        localInteger2 = null;
        ((ContentResolver)localObject1).update((Uri)localObject4, localContentValues, null, null);
        return;
      }
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      Object localObject4 = (Throwable)localIllegalArgumentException;
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject4);
      return;
    }
    catch (SecurityException localSecurityException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSecurityException);
      return;
    }
    catch (RuntimeExecutionException localRuntimeExecutionException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localRuntimeExecutionException);
    }
  }
  
  public final void d()
  {
    Object localObject1 = d.getContentResolver();
    try
    {
      Object localObject2 = TruecallerContract.n.a();
      String str = "type IN (1,2,3)  AND tc_flag!=3";
      ((ContentResolver)localObject1).delete((Uri)localObject2, str, null);
      localObject1 = f;
      localObject2 = "initialCallLogSyncComplete";
      str = null;
      ((c)localObject1).b((String)localObject2, false);
      localObject1 = d;
      WidgetListProvider.a((Context)localObject1);
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSQLiteException);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */