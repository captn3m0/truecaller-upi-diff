package com.truecaller.callhistory;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.g;
import com.truecaller.i.c;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.multisim.h;
import com.truecaller.utils.l;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public final class am
  implements al
{
  private long a;
  private final l b;
  private final dagger.a c;
  private final v d;
  private final k e;
  private final g f;
  private final ai g;
  private final Context h;
  private final c i;
  private final com.truecaller.utils.d j;
  private final CallRecordingManager k;
  
  public am(l paraml, dagger.a parama, v paramv, k paramk, g paramg, ai paramai, Context paramContext, c paramc, com.truecaller.utils.d paramd, CallRecordingManager paramCallRecordingManager)
  {
    b = paraml;
    c = parama;
    d = paramv;
    e = paramk;
    f = paramg;
    g = paramai;
    h = paramContext;
    i = paramc;
    j = paramd;
    k = paramCallRecordingManager;
    a = -1;
  }
  
  private static long a(long paramLong, ArrayList paramArrayList1, ArrayList paramArrayList2)
  {
    boolean bool1 = paramArrayList2.isEmpty();
    if (!bool1)
    {
      int m = paramArrayList2.size() + -1;
      paramArrayList2 = (ContentValues)paramArrayList2.get(m);
      String str = "timestamp";
      paramArrayList2 = paramArrayList2.getAsLong(str);
      if (paramArrayList2 == null) {
        c.g.b.k.a();
      }
      long l1 = paramArrayList2.longValue();
      paramLong = Math.max(paramLong, l1);
    }
    boolean bool2 = paramArrayList1.isEmpty();
    if (!bool2)
    {
      int n = paramArrayList1.size() + -1;
      paramArrayList1 = (ContentValues)paramArrayList1.get(n);
      paramArrayList2 = "timestamp";
      paramArrayList1 = paramArrayList1.getAsLong(paramArrayList2);
      if (paramArrayList1 == null) {
        c.g.b.k.a();
      }
      long l2 = paramArrayList1.longValue();
      paramLong = Math.max(paramLong, l2);
    }
    return paramLong;
  }
  
  private final ac a(ContentResolver paramContentResolver, long paramLong)
  {
    Object localObject1 = b;
    String[] arrayOfString1 = { "android.permission.READ_CALL_LOG" };
    boolean bool = ((l)localObject1).a(arrayOfString1);
    arrayOfString1 = null;
    if (bool)
    {
      localObject1 = b;
      Object localObject2 = { "android.permission.READ_PHONE_STATE" };
      bool = ((l)localObject1).a((String[])localObject2);
      if (bool)
      {
        localObject1 = e.b();
        localObject2 = c.get();
        Object localObject3 = "multiSimManager.get()";
        c.g.b.k.a(localObject2, (String)localObject3);
        localObject2 = ((h)localObject2).d();
        if (localObject2 != null) {
          localObject1 = (String[])org.c.a.a.a.a.b((Object[])localObject1, localObject2);
        }
        Object localObject4 = localObject1;
        bool = true;
        try
        {
          localObject2 = e;
          localObject3 = ((k)localObject2).a();
          String str1 = "date<=?";
          String[] arrayOfString2 = new String[bool];
          Object localObject5 = String.valueOf(paramLong);
          arrayOfString2[0] = localObject5;
          String str2 = "date DESC, _id ASC";
          localObject2 = paramContentResolver;
          paramContentResolver = paramContentResolver.query((Uri)localObject3, (String[])localObject1, str1, arrayOfString2, str2);
          if (paramContentResolver == null) {
            return null;
          }
          try
          {
            localObject5 = new com/truecaller/callhistory/ad;
            localObject7 = f;
            localObject2 = c;
            localObject2 = ((dagger.a)localObject2).get();
            localObject2 = (h)localObject2;
            localObject2 = ((h)localObject2).a(paramContentResolver);
            ((ad)localObject5).<init>((g)localObject7, (com.truecaller.multisim.d)localObject2);
            return (ac)localObject5;
          }
          catch (IllegalArgumentException localIllegalArgumentException1) {}
          if (paramContentResolver == null) {
            break label482;
          }
        }
        catch (SecurityException localSecurityException)
        {
          AssertionUtil.reportThrowableButNeverCrash((Throwable)localSecurityException);
          return null;
        }
        catch (SQLiteException localSQLiteException)
        {
          AssertionUtil.reportThrowableButNeverCrash((Throwable)localSQLiteException);
          return null;
        }
        catch (IllegalArgumentException localIllegalArgumentException2)
        {
          paramContentResolver = null;
        }
        Object localObject6 = paramContentResolver.getColumnNames();
        if (localObject6 == null)
        {
          localObject6 = null;
        }
        else
        {
          int m = localObject6.length;
          if (localObject6 == null)
          {
            localObject6 = null;
          }
          else
          {
            int n = m + 0;
            if (n <= 0)
            {
              localObject6 = "";
            }
            else
            {
              localObject3 = new java/lang/StringBuilder;
              n *= 16;
              ((StringBuilder)localObject3).<init>(n);
              n = 0;
              localObject2 = null;
              while (n < m)
              {
                if (n > 0)
                {
                  char c1 = ',';
                  ((StringBuilder)localObject3).append(c1);
                }
                localObject4 = localObject6[n];
                if (localObject4 != null)
                {
                  localObject4 = localObject6[n];
                  ((StringBuilder)localObject3).append(localObject4);
                }
                n += 1;
              }
              localObject6 = ((StringBuilder)localObject3).toString();
            }
          }
        }
        Object localObject7 = new String[bool];
        localObject1 = "Can't create remote calls cursor. Available columns: ";
        localObject6 = String.valueOf(localObject6);
        localObject6 = ((String)localObject1).concat((String)localObject6);
        localObject7[0] = localObject6;
        AssertionUtil.report((String[])localObject7);
        paramContentResolver.close();
        break label494;
        label482:
        localObject6 = (Throwable)localObject6;
        AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject6);
        label494:
        return null;
      }
    }
    return null;
  }
  
  private final ai.a a(ac paramac, z paramz, List paramList1, List paramList2)
  {
    List localList = paramList1;
    ArrayList localArrayList1 = new java/util/ArrayList;
    localArrayList1.<init>();
    ArrayList localArrayList2 = new java/util/ArrayList;
    localArrayList2.<init>();
    boolean bool1 = paramac.moveToFirst();
    boolean bool4 = paramz.moveToFirst();
    boolean bool5 = bool1;
    boolean bool6 = bool4;
    int i1;
    long l1;
    long l7;
    Object localObject6;
    boolean bool9;
    int m;
    do
    {
      for (;;)
      {
        i1 = 100;
        l1 = -1;
        if ((!bool5) || (!bool6)) {
          break label668;
        }
        bool1 = paramac.a();
        if (!bool1) {
          break;
        }
        bool5 = paramac.moveToNext();
      }
      long l2 = paramz.c();
      long l3 = paramac.c();
      l4 = paramz.b();
      long l5 = paramac.b();
      long l6 = paramz.b();
      boolean bool7 = l6 < l1;
      if (!bool7)
      {
        localObject1 = paramz.d();
        Object localObject2 = localArrayList2;
        localObject2 = (List)localArrayList2;
        Object localObject3 = localArrayList1;
        localObject3 = (List)localArrayList1;
        localObject4 = this;
        l7 = l3;
        localObject5 = localObject2;
        localObject6 = localObject3;
        l1 = l2;
        a((HistoryEvent)localObject1, (List)localObject2, (List)localObject3, paramList1, paramList2);
        bool1 = paramz.moveToNext();
        bool6 = bool1;
      }
      else
      {
        l1 = l2;
        long l8 = l3;
        l3 = l5;
        l7 = l8;
        boolean bool8 = l2 < l8;
        if (bool8)
        {
          l4 = paramz.a();
          a(l4, localList);
          bool1 = paramz.moveToNext();
          bool6 = bool1;
        }
        else
        {
          bool8 = l2 < l8;
          if (bool8)
          {
            localObject1 = paramac.d();
            localObject4 = this;
            localObject5 = localArrayList2;
            localObject6 = localArrayList1;
            a((HistoryEvent)localObject1, localArrayList2, localArrayList1, paramList1, paramList2);
            bool1 = paramac.moveToNext();
            bool5 = bool1;
          }
          else
          {
            bool8 = l4 < l5;
            if (bool8)
            {
              l4 = paramz.a();
              a(l4, localList);
              bool1 = paramz.moveToNext();
              bool6 = bool1;
            }
            else
            {
              bool8 = l4 < l5;
              if (bool8)
              {
                localObject1 = paramac.d();
                localObject4 = this;
                localObject5 = localArrayList2;
                localObject6 = localArrayList1;
                a((HistoryEvent)localObject1, localArrayList2, localArrayList1, paramList1, paramList2);
                bool1 = paramac.moveToNext();
                bool5 = bool1;
              }
              else
              {
                localObject4 = paramz.e();
                c.g.b.k.a(localObject4, "local.simToken");
                localObject1 = paramac.e();
                c.g.b.k.a(localObject1, "remote.simToken");
                localObject4 = (CharSequence)localObject4;
                localObject5 = localObject1;
                localObject5 = (CharSequence)localObject1;
                bool1 = org.c.a.a.a.k.a((CharSequence)localObject4, (CharSequence)localObject5);
                if (!bool1)
                {
                  localObject4 = ContentProviderOperation.newUpdate(TruecallerContract.n.a());
                  ((ContentProviderOperation.Builder)localObject4).withValue("subscription_id", localObject1);
                  bool9 = true;
                  localObject5 = new String[bool9];
                  long l9 = paramz.a();
                  localObject6 = String.valueOf(l9);
                  bool8 = false;
                  localObject5[0] = localObject6;
                  ((ContentProviderOperation.Builder)localObject4).withSelection("_id=?", (String[])localObject5);
                  localObject4 = ((ContentProviderOperation.Builder)localObject4).build();
                  localObject1 = "operation.build()";
                  c.g.b.k.a(localObject4, (String)localObject1);
                  localList.add(localObject4);
                }
                bool1 = paramz.moveToNext();
                bool4 = paramac.moveToNext();
                bool6 = bool1;
                bool5 = bool4;
              }
            }
          }
        }
      }
      m = paramList1.size();
    } while (m < i1);
    long l4 = Math.max(l1, l7);
    Object localObject5 = new com/truecaller/callhistory/ai$a;
    l4 = a(l4, localArrayList2, localArrayList1);
    ((ai.a)localObject5).<init>(0, l4);
    return (ai.a)localObject5;
    label668:
    while (bool5)
    {
      HistoryEvent localHistoryEvent = paramac.d();
      if (localHistoryEvent != null)
      {
        boolean bool2 = localArrayList1.isEmpty();
        if (bool2)
        {
          localObject4 = ContentProviderOperation.newInsert(TruecallerContract.n.a());
          localObject1 = e.a(localHistoryEvent);
          ((ContentProviderOperation.Builder)localObject4).withValues((ContentValues)localObject1);
          localObject4 = ((ContentProviderOperation.Builder)localObject4).build();
          localObject1 = "operation.build()";
          c.g.b.k.a(localObject4, (String)localObject1);
          localList.add(localObject4);
        }
        else
        {
          localObject4 = this;
          localObject1 = localHistoryEvent;
          localObject5 = localArrayList2;
          localObject6 = localArrayList1;
          a(localHistoryEvent, localArrayList2, localArrayList1, paramList1, paramList2);
        }
        int n = paramList1.size();
        if (n >= i1)
        {
          localObject4 = new com/truecaller/callhistory/ai$a;
          long l10 = a(localHistoryEvent.j(), localArrayList2, localArrayList1);
          ((ai.a)localObject4).<init>(0, l10);
          return (ai.a)localObject4;
        }
        localObject6 = null;
      }
      else
      {
        localObject6 = null;
      }
      bool5 = paramac.moveToNext();
    }
    while (bool6)
    {
      l4 = paramz.b();
      bool9 = l4 < l1;
      if (bool9)
      {
        l4 = paramz.a();
        a(l4, localList);
      }
      else
      {
        boolean bool3 = localArrayList2.isEmpty();
        if (!bool3)
        {
          localObject1 = paramz.d();
          localObject5 = localArrayList2;
          localObject5 = (List)localArrayList2;
          localObject6 = localArrayList1;
          localObject6 = (List)localArrayList1;
          localObject4 = this;
          a((HistoryEvent)localObject1, (List)localObject5, (List)localObject6, paramList1, paramList2);
        }
      }
      bool6 = paramz.moveToNext();
    }
    Object localObject4 = localArrayList2.listIterator();
    Object localObject1 = "toBeAdded.listIterator()";
    c.g.b.k.a(localObject4, (String)localObject1);
    for (;;)
    {
      bool4 = ((ListIterator)localObject4).hasNext();
      if (!bool4) {
        break;
      }
      localObject1 = ContentProviderOperation.newInsert(TruecallerContract.n.a());
      localObject5 = (ContentValues)((ListIterator)localObject4).next();
      ((ContentProviderOperation.Builder)localObject1).withValues((ContentValues)localObject5);
      ((ListIterator)localObject4).remove();
      localObject1 = ((ContentProviderOperation.Builder)localObject1).build();
      localObject5 = "operation.build()";
      c.g.b.k.a(localObject1, (String)localObject5);
      localList.add(localObject1);
    }
    localObject4 = new com/truecaller/callhistory/ai$a;
    ((ai.a)localObject4).<init>();
    return (ai.a)localObject4;
  }
  
  private static void a(long paramLong, List paramList)
  {
    ContentProviderOperation.Builder localBuilder = ContentProviderOperation.newDelete(TruecallerContract.n.a());
    String[] arrayOfString = new String[1];
    Object localObject = String.valueOf(paramLong);
    arrayOfString[0] = localObject;
    localBuilder.withSelection("_id=?", arrayOfString);
    localObject = localBuilder.build();
    c.g.b.k.a(localObject, "builder.build()");
    paramList.add(localObject);
  }
  
  private final void a(HistoryEvent paramHistoryEvent, ArrayList paramArrayList1, ArrayList paramArrayList2, List paramList1, List paramList2)
  {
    am localam = this;
    Object localObject1 = paramHistoryEvent;
    Object localObject2 = paramArrayList1;
    Object localObject3 = paramList1;
    Object localObject4 = paramList2;
    int m = 1;
    Object localObject5 = new String[m];
    Object localObject6 = new java/lang/StringBuilder;
    ((StringBuilder)localObject6).<init>("scheduleEventToAdd() called with: remote = [");
    ((StringBuilder)localObject6).append(paramHistoryEvent);
    ((StringBuilder)localObject6).append("], toBeAdded = [");
    ((StringBuilder)localObject6).append(paramArrayList1);
    ((StringBuilder)localObject6).append("], toBeUpdated = [");
    Object localObject7 = paramArrayList2;
    ((StringBuilder)localObject6).append(paramArrayList2);
    ((StringBuilder)localObject6).append("], localOperations = [");
    ((StringBuilder)localObject6).append(paramList1);
    ((StringBuilder)localObject6).append("], remoteOperations = [");
    ((StringBuilder)localObject6).append(paramList2);
    ((StringBuilder)localObject6).append("]");
    localObject6 = ((StringBuilder)localObject6).toString();
    localObject5[0] = localObject6;
    if (paramHistoryEvent == null) {
      return;
    }
    long l1 = paramHistoryEvent.j();
    long l2 = 10000L;
    l1 += l2;
    boolean bool2 = paramArrayList2.isEmpty();
    Object localObject8;
    Object localObject10;
    if (!bool2)
    {
      localObject7 = paramArrayList2.iterator();
      localObject8 = "toBeUpdated.iterator()";
      c.g.b.k.a(localObject7, (String)localObject8);
      boolean bool4;
      do
      {
        for (;;)
        {
          bool2 = ((Iterator)localObject7).hasNext();
          if (!bool2) {
            break label755;
          }
          localObject8 = ((Iterator)localObject7).next();
          c.g.b.k.a(localObject8, "update.next()");
          localObject8 = (ContentValues)localObject8;
          localObject9 = ((ContentValues)localObject8).getAsLong("timestamp");
          l3 = ((Long)localObject9).longValue();
          boolean bool3 = l3 < l1;
          if (!bool3) {
            break;
          }
          ((Iterator)localObject7).remove();
        }
        Object localObject9 = ((ContentValues)localObject8).getAsInteger("type");
        if (localObject9 == null) {
          c.g.b.k.a();
        }
        int i1 = ((Integer)localObject9).intValue();
        int n = paramHistoryEvent.f();
        String str1 = ((ContentValues)localObject8).getAsString("normalized_number");
        String str2 = paramHistoryEvent.a();
        localObject9 = ((ContentValues)localObject8).getAsLong("timestamp");
        if (localObject9 == null) {
          c.g.b.k.a();
        }
        long l4 = ((Long)localObject9).longValue();
        long l5 = paramHistoryEvent.j();
        bool4 = ao.a(i1, n, str1, str2, l4, l5);
      } while (!bool4);
      localObject2 = ((ContentValues)localObject8).getAsInteger("action");
      if (localObject2 != null)
      {
        int i2 = ((Integer)localObject2).intValue();
        int i3 = 5;
        if (i2 == i3)
        {
          localObject2 = ((ContentValues)localObject8).getAsLong("_id");
          if (localObject2 == null) {
            c.g.b.k.a();
          }
          a(((Long)localObject2).longValue(), (List)localObject3);
          localObject2 = ContentProviderOperation.newDelete(e.a());
          localObject3 = "_id=?";
          localObject10 = new String[m];
          l1 = paramHistoryEvent.i().longValue();
          localObject1 = String.valueOf(l1);
          localObject10[0] = localObject1;
          ((ContentProviderOperation.Builder)localObject2).withSelection((String)localObject3, (String[])localObject10);
          localObject1 = ((ContentProviderOperation.Builder)localObject2).build();
          localObject2 = "op.build()";
          c.g.b.k.a(localObject1, (String)localObject2);
          ((List)localObject4).add(localObject1);
          break label747;
        }
      }
      localObject2 = ContentProviderOperation.newUpdate(TruecallerContract.n.a());
      localObject4 = ((ContentValues)localObject8).getAsString("_id");
      localObject6 = Integer.valueOf(paramHistoryEvent.f());
      ((ContentValues)localObject8).put("type", (Integer)localObject6);
      localObject6 = paramHistoryEvent.i();
      ((ContentValues)localObject8).put("call_log_id", (Long)localObject6);
      localObject6 = Long.valueOf(paramHistoryEvent.j());
      ((ContentValues)localObject8).put("timestamp", (Long)localObject6);
      long l3 = paramHistoryEvent.k();
      localObject6 = Long.valueOf(l3);
      ((ContentValues)localObject8).put("duration", (Long)localObject6);
      localObject6 = paramHistoryEvent.l();
      ((ContentValues)localObject8).put("subscription_id", (String)localObject6);
      int i4 = paramHistoryEvent.m();
      localObject6 = Integer.valueOf(i4);
      ((ContentValues)localObject8).put("feature", (Integer)localObject6);
      localObject5 = "subscription_component_name";
      localObject1 = paramHistoryEvent.q();
      ((ContentValues)localObject8).put((String)localObject5, (String)localObject1);
      ((ContentValues)localObject8).remove("normalized_number");
      ((ContentValues)localObject8).remove("action");
      ((ContentProviderOperation.Builder)localObject2).withValues((ContentValues)localObject8);
      localObject10 = new String[m];
      localObject10[0] = localObject4;
      ((ContentProviderOperation.Builder)localObject2).withSelection("_id=?", (String[])localObject10);
      localObject1 = ((ContentProviderOperation.Builder)localObject2).build();
      localObject2 = "builder.build()";
      c.g.b.k.a(localObject1, (String)localObject2);
      ((List)localObject3).add(localObject1);
      label747:
      ((Iterator)localObject7).remove();
      return;
    }
    label755:
    localObject4 = e.a(paramHistoryEvent);
    ((ArrayList)localObject2).add(0, localObject4);
    localObject4 = j;
    boolean bool6 = ((com.truecaller.utils.d)localObject4).e();
    if (bool6)
    {
      int i5 = paramHistoryEvent.f();
      m = 2;
      if (i5 == m)
      {
        localObject4 = k.b();
        localObject10 = localObject4;
        localObject10 = (CharSequence)localObject4;
        boolean bool1 = org.c.a.a.a.k.b((CharSequence)localObject10);
        if (!bool1)
        {
          localObject10 = k;
          bool1 = ((CallRecordingManager)localObject10).i();
          if (bool1)
          {
            new String[1][0] = "scheduleEventToAdd:: Short recording ignoring";
            return;
          }
          new String[1][0] = "scheduleEventToAdd:: Linking call recording to history event.";
          localObject10 = new com/truecaller/data/entity/CallRecording;
          long l6 = -1;
          localObject8 = paramHistoryEvent.u();
          if (localObject4 == null) {
            c.g.b.k.a();
          }
          ((CallRecording)localObject10).<init>(l6, (String)localObject8, (String)localObject4);
          ((HistoryEvent)localObject1).a((CallRecording)localObject10);
          d.a((CallRecording)localObject10);
          localObject1 = k;
          ((CallRecordingManager)localObject1).h();
        }
      }
    }
    int i6 = paramArrayList1.size();
    localObject1 = ((ArrayList)localObject2).listIterator(i6);
    localObject2 = "toBeAdded.listIterator(toBeAdded.size)";
    c.g.b.k.a(localObject1, (String)localObject2);
    for (;;)
    {
      boolean bool5 = ((ListIterator)localObject1).hasPrevious();
      if (!bool5) {
        break;
      }
      localObject2 = ((ListIterator)localObject1).previous();
      c.g.b.k.a(localObject2, "it.previous()");
      localObject2 = (ContentValues)localObject2;
      localObject4 = ((ContentValues)localObject2).getAsLong("timestamp");
      long l7 = ((Long)localObject4).longValue();
      boolean bool7 = l7 < l1;
      if (!bool7) {
        break;
      }
      localObject4 = ContentProviderOperation.newInsert(TruecallerContract.n.a());
      ((ContentProviderOperation.Builder)localObject4).withValues((ContentValues)localObject2);
      localObject2 = ((ContentProviderOperation.Builder)localObject4).build();
      localObject4 = "builder.build()";
      c.g.b.k.a(localObject2, (String)localObject4);
      ((List)localObject3).add(localObject2);
      ((ListIterator)localObject1).remove();
    }
  }
  
  private final void a(HistoryEvent paramHistoryEvent, List paramList1, List paramList2, List paramList3, List paramList4)
  {
    if (paramHistoryEvent == null) {
      return;
    }
    Object localObject = paramHistoryEvent.getId();
    String[] arrayOfString = { "Event must have record in local database" };
    AssertionUtil.AlwaysFatal.isNotNull(localObject, arrayOfString);
    boolean bool1 = paramList1.isEmpty();
    if (bool1)
    {
      paramList1 = new android/content/ContentValues;
      paramList1.<init>();
      paramList4 = paramHistoryEvent.getId();
      paramList1.put("_id", paramList4);
      paramList4 = Long.valueOf(paramHistoryEvent.j());
      paramList1.put("timestamp", paramList4);
      paramList4 = paramHistoryEvent.a();
      paramList1.put("normalized_number", paramList4);
      paramList4 = Integer.valueOf(paramHistoryEvent.h());
      paramList1.put("action", paramList4);
      paramHistoryEvent = Integer.valueOf(paramHistoryEvent.f());
      paramList1.put("type", paramHistoryEvent);
      paramList2.add(paramList1);
      return;
    }
    paramList1 = paramList1.iterator();
    boolean bool2;
    do
    {
      bool1 = paramList1.hasNext();
      arrayOfString = null;
      if (!bool1) {
        break;
      }
      localObject = (ContentValues)paramList1.next();
      int m = paramHistoryEvent.f();
      Integer localInteger = ((ContentValues)localObject).getAsInteger("type");
      if (localInteger == null) {
        c.g.b.k.a();
      }
      i1 = localInteger.intValue();
      String str1 = paramHistoryEvent.a();
      String str2 = ((ContentValues)localObject).getAsString("normalized_number");
      long l1 = paramHistoryEvent.j();
      Long localLong = ((ContentValues)localObject).getAsLong("timestamp");
      if (localLong == null) {
        c.g.b.k.a();
      }
      long l2 = localLong.longValue();
      bool2 = ao.a(m, i1, str1, str2, l1, l2);
    } while (!bool2);
    int i2 = paramHistoryEvent.h();
    int n = 5;
    int i1 = 1;
    if (i2 == n)
    {
      paramHistoryEvent = paramHistoryEvent.getId();
      if (paramHistoryEvent == null) {
        c.g.b.k.a();
      }
      c.g.b.k.a(paramHistoryEvent, "event.id!!");
      long l3 = paramHistoryEvent.longValue();
      a(l3, paramList3);
      paramHistoryEvent = ContentProviderOperation.newDelete(e.a());
      paramList3 = new String[i1];
      String str3 = "call_log_id";
      localObject = ((ContentValues)localObject).getAsString(str3);
      paramList3[0] = localObject;
      paramHistoryEvent.withSelection("_id=?", paramList3);
      paramHistoryEvent = paramHistoryEvent.build();
      paramList2 = "op.build()";
      c.g.b.k.a(paramHistoryEvent, paramList2);
      paramList4.add(paramHistoryEvent);
    }
    else
    {
      paramList2 = ContentProviderOperation.newUpdate(TruecallerContract.n.a());
      ((ContentValues)localObject).remove("tc_id");
      ((ContentValues)localObject).remove("normalized_number");
      ((ContentValues)localObject).remove("raw_number");
      ((ContentValues)localObject).remove("number_type");
      ((ContentValues)localObject).remove("country_code");
      ((ContentValues)localObject).remove("cached_name");
      ((ContentValues)localObject).remove("action");
      paramList2.withValues((ContentValues)localObject);
      paramList4 = "_id=?";
      localObject = new String[i1];
      paramHistoryEvent = String.valueOf(paramHistoryEvent.getId());
      localObject[0] = paramHistoryEvent;
      paramList2.withSelection(paramList4, (String[])localObject);
      paramHistoryEvent = paramList2.build();
      paramList2 = "builder.build()";
      c.g.b.k.a(paramHistoryEvent, paramList2);
      paramList3.add(paramHistoryEvent);
    }
    paramList1.remove();
    return;
    paramList1 = new android/content/ContentValues;
    paramList1.<init>();
    paramList4 = paramHistoryEvent.getId();
    paramList1.put("_id", paramList4);
    paramList4 = paramHistoryEvent.a();
    paramList1.put("normalized_number", paramList4);
    paramList4 = Long.valueOf(paramHistoryEvent.j());
    paramList1.put("timestamp", paramList4);
    paramList4 = Integer.valueOf(paramHistoryEvent.h());
    paramList1.put("action", paramList4);
    paramHistoryEvent = Integer.valueOf(paramHistoryEvent.f());
    paramList1.put("type", paramHistoryEvent);
    paramList2.add(0, paramList1);
  }
  
  private static z b(ContentResolver paramContentResolver, long paramLong)
  {
    Uri localUri = TruecallerContract.n.a();
    String str1 = "type IN (1,2,3)  AND timestamp<=? AND tc_flag!=3 AND (subscription_component_name!='com.whatsapp' OR subscription_component_name IS NULL) AND tc_flag!=2 AND (subscription_component_name!='com.truecaller.voip.manager.VOIP' OR subscription_component_name IS NULL)";
    int m = 1;
    String[] arrayOfString = new String[m];
    Object localObject = String.valueOf(paramLong);
    arrayOfString[0] = localObject;
    String str2 = "timestamp DESC, call_log_id ASC";
    paramContentResolver = paramContentResolver.query(localUri, null, str1, arrayOfString, str2);
    if (paramContentResolver == null)
    {
      AssertionUtil.reportWeirdnessButNeverCrash("Content resolver returned null cursor");
      return null;
    }
    localObject = new com/truecaller/callhistory/aa;
    ((aa)localObject).<init>(paramContentResolver);
    return (z)localObject;
  }
  
  /* Error */
  public final void a(ai.a parama)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore_2
    //   2: aload_1
    //   3: astore_3
    //   4: aload_1
    //   5: ldc_w 567
    //   8: invokestatic 35	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   11: aload_0
    //   12: getfield 71	com/truecaller/callhistory/am:h	Landroid/content/Context;
    //   15: invokevirtual 573	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   18: astore 4
    //   20: new 81	java/util/ArrayList
    //   23: astore 5
    //   25: aload 5
    //   27: invokespecial 234	java/util/ArrayList:<init>	()V
    //   30: new 81	java/util/ArrayList
    //   33: astore 6
    //   35: aload 6
    //   37: invokespecial 234	java/util/ArrayList:<init>	()V
    //   40: aload_0
    //   41: getfield 79	com/truecaller/callhistory/am:a	J
    //   44: lstore 7
    //   46: aload_1
    //   47: getfield 574	com/truecaller/callhistory/ai$a:a	J
    //   50: lstore 9
    //   52: lload 7
    //   54: lload 9
    //   56: lcmp
    //   57: istore 11
    //   59: iload 11
    //   61: ifle +32 -> 93
    //   64: new 320	com/truecaller/callhistory/ai$a
    //   67: astore 12
    //   69: aload_1
    //   70: getfield 577	com/truecaller/callhistory/ai$a:b	I
    //   73: istore 13
    //   75: aload_0
    //   76: getfield 79	com/truecaller/callhistory/am:a	J
    //   79: lstore 14
    //   81: aload 12
    //   83: iload 13
    //   85: lload 14
    //   87: invokespecial 326	com/truecaller/callhistory/ai$a:<init>	(IJ)V
    //   90: aload 12
    //   92: astore_3
    //   93: aconst_null
    //   94: astore 12
    //   96: ldc_w 579
    //   99: astore 16
    //   101: aload 4
    //   103: aload 16
    //   105: invokestatic 140	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   108: aload_3
    //   109: getfield 574	com/truecaller/callhistory/ai$a:a	J
    //   112: lstore 14
    //   114: aload_2
    //   115: aload 4
    //   117: lload 14
    //   119: invokespecial 582	com/truecaller/callhistory/am:a	(Landroid/content/ContentResolver;J)Lcom/truecaller/callhistory/ac;
    //   122: astore 16
    //   124: aload 16
    //   126: ifnonnull +16 -> 142
    //   129: aload 16
    //   131: checkcast 190	android/database/Cursor
    //   134: invokestatic 586	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   137: aconst_null
    //   138: invokestatic 586	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   141: return
    //   142: aload_3
    //   143: getfield 574	com/truecaller/callhistory/ai$a:a	J
    //   146: lstore 9
    //   148: aload 4
    //   150: lload 9
    //   152: invokestatic 589	com/truecaller/callhistory/am:b	(Landroid/content/ContentResolver;J)Lcom/truecaller/callhistory/z;
    //   155: astore 12
    //   157: aload 12
    //   159: ifnonnull +20 -> 179
    //   162: aload 16
    //   164: checkcast 190	android/database/Cursor
    //   167: invokestatic 586	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   170: aload 12
    //   172: checkcast 190	android/database/Cursor
    //   175: invokestatic 586	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   178: return
    //   179: aload 5
    //   181: astore_3
    //   182: aload 5
    //   184: checkcast 257	java/util/List
    //   187: astore_3
    //   188: aload 6
    //   190: astore 17
    //   192: aload 6
    //   194: checkcast 257	java/util/List
    //   197: astore 17
    //   199: aload_2
    //   200: aload 16
    //   202: aload 12
    //   204: aload_3
    //   205: aload 17
    //   207: invokespecial 592	com/truecaller/callhistory/am:a	(Lcom/truecaller/callhistory/ac;Lcom/truecaller/callhistory/z;Ljava/util/List;Ljava/util/List;)Lcom/truecaller/callhistory/ai$a;
    //   210: astore 17
    //   212: aload 16
    //   214: checkcast 190	android/database/Cursor
    //   217: astore 16
    //   219: aload 16
    //   221: invokestatic 586	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   224: aload 12
    //   226: checkcast 190	android/database/Cursor
    //   229: astore 12
    //   231: aload 12
    //   233: invokestatic 586	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   236: aload 5
    //   238: invokevirtual 85	java/util/ArrayList:isEmpty	()Z
    //   241: istore 13
    //   243: iconst_m1
    //   244: i2l
    //   245: lstore 7
    //   247: iconst_1
    //   248: istore 18
    //   250: iload 13
    //   252: ifeq +61 -> 313
    //   255: aload 6
    //   257: invokevirtual 85	java/util/ArrayList:isEmpty	()Z
    //   260: istore 13
    //   262: iconst_1
    //   263: anewarray 119	java/lang/String
    //   266: dup
    //   267: iconst_0
    //   268: ldc_w 594
    //   271: aastore
    //   272: astore 4
    //   274: iload 13
    //   276: aload 4
    //   278: invokestatic 598	com/truecaller/log/AssertionUtil$AlwaysFatal:isTrue	(Z[Ljava/lang/String;)V
    //   281: aload_2
    //   282: getfield 73	com/truecaller/callhistory/am:i	Lcom/truecaller/i/c;
    //   285: ldc_w 600
    //   288: iload 18
    //   290: invokeinterface 605 3 0
    //   295: getstatic 610	com/truecaller/service/MissedCallsNotificationService:q	Lcom/truecaller/service/MissedCallsNotificationService$a;
    //   298: astore_3
    //   299: aload_2
    //   300: getfield 71	com/truecaller/callhistory/am:h	Landroid/content/Context;
    //   303: invokestatic 615	com/truecaller/service/MissedCallsNotificationService$a:a	(Landroid/content/Context;)V
    //   306: aload_2
    //   307: lload 7
    //   309: putfield 79	com/truecaller/callhistory/am:a	J
    //   312: return
    //   313: aload 17
    //   315: getfield 577	com/truecaller/callhistory/ai$a:b	I
    //   318: istore 13
    //   320: iconst_0
    //   321: istore 11
    //   323: iload 13
    //   325: iload 18
    //   327: if_icmpne +9 -> 336
    //   330: iconst_1
    //   331: istore 19
    //   333: goto +6 -> 339
    //   336: iconst_0
    //   337: istore 19
    //   339: iload 19
    //   341: ifeq +97 -> 438
    //   344: invokestatic 287	com/truecaller/content/TruecallerContract$n:a	()Landroid/net/Uri;
    //   347: invokestatic 293	android/content/ContentProviderOperation:newUpdate	(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;
    //   350: astore_3
    //   351: iconst_0
    //   352: invokestatic 438	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   355: astore 20
    //   357: aload_3
    //   358: ldc_w 427
    //   361: aload 20
    //   363: invokevirtual 301	android/content/ContentProviderOperation$Builder:withValue	(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;
    //   366: pop
    //   367: ldc_w 617
    //   370: astore 21
    //   372: iload 18
    //   374: anewarray 119	java/lang/String
    //   377: astore 20
    //   379: invokestatic 622	java/lang/System:currentTimeMillis	()J
    //   382: lstore 22
    //   384: getstatic 628	java/util/concurrent/TimeUnit:DAYS	Ljava/util/concurrent/TimeUnit;
    //   387: astore 24
    //   389: aload 24
    //   391: lconst_1
    //   392: invokevirtual 634	java/util/concurrent/TimeUnit:toMillis	(J)J
    //   395: lstore 7
    //   397: lload 22
    //   399: lload 7
    //   401: lsub
    //   402: lstore 22
    //   404: lload 22
    //   406: invokestatic 162	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   409: astore 12
    //   411: aload 20
    //   413: iconst_0
    //   414: aload 12
    //   416: aastore
    //   417: aload_3
    //   418: aload 21
    //   420: aload 20
    //   422: invokevirtual 307	android/content/ContentProviderOperation$Builder:withSelection	(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;
    //   425: pop
    //   426: aload_3
    //   427: invokevirtual 311	android/content/ContentProviderOperation$Builder:build	()Landroid/content/ContentProviderOperation;
    //   430: astore_3
    //   431: aload 5
    //   433: aload_3
    //   434: invokevirtual 635	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   437: pop
    //   438: invokestatic 638	com/truecaller/content/TruecallerContract:a	()Ljava/lang/String;
    //   441: astore_3
    //   442: aload 4
    //   444: aload_3
    //   445: aload 5
    //   447: invokevirtual 642	android/content/ContentResolver:applyBatch	(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    //   450: astore_3
    //   451: ldc_w 644
    //   454: astore 5
    //   456: aload_3
    //   457: aload 5
    //   459: invokestatic 140	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   462: aload_3
    //   463: arraylength
    //   464: istore 13
    //   466: iload 13
    //   468: ifne +6 -> 474
    //   471: iconst_1
    //   472: istore 11
    //   474: iload 11
    //   476: iconst_1
    //   477: ixor
    //   478: istore 13
    //   480: iload 13
    //   482: ifeq +12 -> 494
    //   485: aload_2
    //   486: getfield 71	com/truecaller/callhistory/am:h	Landroid/content/Context;
    //   489: astore_3
    //   490: aload_3
    //   491: invokestatic 647	com/truecaller/service/WidgetListProvider:a	(Landroid/content/Context;)V
    //   494: aload 6
    //   496: invokevirtual 85	java/util/ArrayList:isEmpty	()Z
    //   499: istore 13
    //   501: iload 13
    //   503: ifne +38 -> 541
    //   506: ldc_w 649
    //   509: astore_3
    //   510: aload 4
    //   512: aload_3
    //   513: aload 6
    //   515: invokevirtual 642	android/content/ContentResolver:applyBatch	(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    //   518: pop
    //   519: goto +22 -> 541
    //   522: checkcast 182	java/lang/Throwable
    //   525: astore_3
    //   526: aload_3
    //   527: invokestatic 188	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   530: goto +11 -> 541
    //   533: checkcast 182	java/lang/Throwable
    //   536: astore_3
    //   537: aload_3
    //   538: invokestatic 188	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   541: iload 19
    //   543: ifeq +35 -> 578
    //   546: aload_2
    //   547: getfield 73	com/truecaller/callhistory/am:i	Lcom/truecaller/i/c;
    //   550: ldc_w 600
    //   553: iload 18
    //   555: invokeinterface 605 3 0
    //   560: getstatic 610	com/truecaller/service/MissedCallsNotificationService:q	Lcom/truecaller/service/MissedCallsNotificationService$a;
    //   563: astore_3
    //   564: aload_2
    //   565: getfield 71	com/truecaller/callhistory/am:h	Landroid/content/Context;
    //   568: invokestatic 615	com/truecaller/service/MissedCallsNotificationService$a:a	(Landroid/content/Context;)V
    //   571: aload_2
    //   572: iconst_m1
    //   573: i2l
    //   574: putfield 79	com/truecaller/callhistory/am:a	J
    //   577: return
    //   578: aload 17
    //   580: getfield 574	com/truecaller/callhistory/ai$a:a	J
    //   583: lstore 25
    //   585: aload_2
    //   586: lload 25
    //   588: putfield 79	com/truecaller/callhistory/am:a	J
    //   591: aload_2
    //   592: getfield 69	com/truecaller/callhistory/am:g	Lcom/truecaller/callhistory/ai;
    //   595: aload 17
    //   597: invokeinterface 654 2 0
    //   602: return
    //   603: checkcast 182	java/lang/Throwable
    //   606: invokestatic 188	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   609: aload_2
    //   610: iconst_m1
    //   611: i2l
    //   612: putfield 79	com/truecaller/callhistory/am:a	J
    //   615: return
    //   616: checkcast 182	java/lang/Throwable
    //   619: invokestatic 188	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   622: aload_2
    //   623: iconst_m1
    //   624: i2l
    //   625: putfield 79	com/truecaller/callhistory/am:a	J
    //   628: return
    //   629: astore_3
    //   630: aload 12
    //   632: astore 4
    //   634: aload 16
    //   636: astore 12
    //   638: goto +53 -> 691
    //   641: astore_3
    //   642: aload 12
    //   644: astore 4
    //   646: aload 16
    //   648: astore 12
    //   650: goto +14 -> 664
    //   653: astore_3
    //   654: aconst_null
    //   655: astore 4
    //   657: goto +34 -> 691
    //   660: astore_3
    //   661: aconst_null
    //   662: astore 4
    //   664: aload_3
    //   665: checkcast 182	java/lang/Throwable
    //   668: astore_3
    //   669: aload_3
    //   670: invokestatic 188	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   673: aload 12
    //   675: checkcast 190	android/database/Cursor
    //   678: invokestatic 586	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   681: aload 4
    //   683: checkcast 190	android/database/Cursor
    //   686: invokestatic 586	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   689: return
    //   690: astore_3
    //   691: aload 12
    //   693: checkcast 190	android/database/Cursor
    //   696: invokestatic 586	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   699: aload 4
    //   701: checkcast 190	android/database/Cursor
    //   704: invokestatic 586	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   707: aload_3
    //   708: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	709	0	this	am
    //   0	709	1	parama	ai.a
    //   1	622	2	localam	am
    //   3	561	3	localObject1	Object
    //   629	1	3	localObject2	Object
    //   641	1	3	localRuntimeException1	RuntimeException
    //   653	1	3	localObject3	Object
    //   660	5	3	localRuntimeException2	RuntimeException
    //   668	2	3	localThrowable	Throwable
    //   690	18	3	localObject4	Object
    //   18	682	4	localObject5	Object
    //   23	435	5	localObject6	Object
    //   33	481	6	localArrayList	ArrayList
    //   44	356	7	l1	long
    //   50	101	9	l2	long
    //   57	421	11	bool1	boolean
    //   67	625	12	localObject7	Object
    //   73	11	13	m	int
    //   241	34	13	bool2	boolean
    //   318	163	13	n	int
    //   499	3	13	bool3	boolean
    //   79	39	14	l3	long
    //   99	548	16	localObject8	Object
    //   190	406	17	localObject9	Object
    //   248	306	18	i1	int
    //   331	211	19	i2	int
    //   355	66	20	localObject10	Object
    //   370	49	21	str	String
    //   382	23	22	l4	long
    //   387	3	24	localTimeUnit	java.util.concurrent.TimeUnit
    //   583	4	25	l5	long
    //   522	1	31	localOperationApplicationException1	android.content.OperationApplicationException
    //   533	1	32	localRemoteException1	android.os.RemoteException
    //   603	1	33	localOperationApplicationException2	android.content.OperationApplicationException
    //   616	1	34	localRemoteException2	android.os.RemoteException
    // Exception table:
    //   from	to	target	type
    //   513	519	522	android/content/OperationApplicationException
    //   513	519	533	android/os/RemoteException
    //   438	441	603	android/content/OperationApplicationException
    //   445	450	603	android/content/OperationApplicationException
    //   457	462	603	android/content/OperationApplicationException
    //   462	464	603	android/content/OperationApplicationException
    //   485	489	603	android/content/OperationApplicationException
    //   490	494	603	android/content/OperationApplicationException
    //   438	441	616	android/os/RemoteException
    //   445	450	616	android/os/RemoteException
    //   457	462	616	android/os/RemoteException
    //   462	464	616	android/os/RemoteException
    //   485	489	616	android/os/RemoteException
    //   490	494	616	android/os/RemoteException
    //   142	146	629	finally
    //   150	155	629	finally
    //   182	187	629	finally
    //   192	197	629	finally
    //   205	210	629	finally
    //   142	146	641	java/lang/RuntimeException
    //   150	155	641	java/lang/RuntimeException
    //   182	187	641	java/lang/RuntimeException
    //   192	197	641	java/lang/RuntimeException
    //   205	210	641	java/lang/RuntimeException
    //   103	108	653	finally
    //   108	112	653	finally
    //   117	122	653	finally
    //   103	108	660	java/lang/RuntimeException
    //   108	112	660	java/lang/RuntimeException
    //   117	122	660	java/lang/RuntimeException
    //   664	668	690	finally
    //   669	673	690	finally
  }
  
  public final void a(boolean paramBoolean)
  {
    long l1 = a;
    long l2 = -1;
    boolean bool = l1 < l2;
    if (bool)
    {
      l1 = System.currentTimeMillis();
      a = l1;
      return;
    }
    ai.a locala = new com/truecaller/callhistory/ai$a;
    locala.<init>((byte)0);
    long l3 = a;
    a = l3;
    if (paramBoolean)
    {
      a(locala);
      return;
    }
    g.a(locala);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.am
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */