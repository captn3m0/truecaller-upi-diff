package com.truecaller.callhistory;

import com.truecaller.androidactors.w;
import com.truecaller.calling.dialer.HistoryEventsScope;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public abstract interface a
{
  public abstract w a(int paramInt, Collection paramCollection);
  
  public abstract w a(long paramLong);
  
  public abstract w a(FilterType paramFilterType, Integer paramInteger);
  
  public abstract w a(Contact paramContact);
  
  public abstract w a(HistoryEvent paramHistoryEvent, Contact paramContact);
  
  public abstract w a(String paramString);
  
  public abstract w a(List paramList);
  
  public abstract w a(List paramList1, List paramList2, HistoryEventsScope paramHistoryEventsScope);
  
  public abstract w a(Set paramSet);
  
  public abstract void a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(ai.a parama);
  
  public abstract void a(CallRecording paramCallRecording);
  
  public abstract void a(HistoryEvent paramHistoryEvent);
  
  public abstract w b();
  
  public abstract w b(int paramInt);
  
  public abstract w b(Set paramSet);
  
  public abstract void b(long paramLong);
  
  public abstract void b(String paramString);
  
  public abstract w c();
  
  public abstract w c(int paramInt);
  
  public abstract w c(String paramString);
  
  public abstract void c(long paramLong);
  
  public abstract w d();
  
  public abstract w d(String paramString);
  
  public abstract w e();
  
  public abstract void f();
  
  public abstract void g();
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */