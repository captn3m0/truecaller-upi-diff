package com.truecaller.callhistory;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.provider.CallLog.Calls;
import c.u;
import com.google.android.gms.tasks.RuntimeExecutionException;
import com.truecaller.androidactors.w;
import com.truecaller.calling.dialer.HistoryEventsScope;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.log.AssertionUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class c
  implements a
{
  private final l a;
  private final af b;
  private final al c;
  private final com.truecaller.data.access.m d;
  private final ap e;
  private final v f;
  private final ContentResolver g;
  
  public c(l paraml, af paramaf, al paramal, com.truecaller.data.access.m paramm, ap paramap, v paramv, ContentResolver paramContentResolver)
  {
    a = paraml;
    b = paramaf;
    c = paramal;
    d = paramm;
    e = paramap;
    f = paramv;
    g = paramContentResolver;
  }
  
  private final void a(Uri paramUri, String paramString1, List paramList, String paramString2)
  {
    Uri localUri = paramUri;
    if (paramList != null)
    {
      localObject1 = paramList;
      localObject1 = (Iterable)paramList;
      int i = 10000;
      localObject1 = c.a.m.e((Iterable)localObject1, i);
      if (localObject1 != null) {}
    }
    else
    {
      localObject1 = c.a.m.a(null);
    }
    Object localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      Object localObject2 = (List)((Iterator)localObject1).next();
      int k;
      if (localObject2 != null)
      {
        localObject3 = localObject2;
        localObject3 = (Iterable)localObject2;
        localObject4 = (CharSequence)"(";
        localObject5 = (CharSequence)")";
        localObject6 = null;
        c1 = '\000';
        localObject7 = null;
        j = 0;
        localContentResolver = null;
        k = 57;
        localObject2 = c.a.m.a((Iterable)localObject3, null, (CharSequence)localObject4, (CharSequence)localObject5, 0, null, null, k);
        if (localObject2 != null)
        {
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>();
          ((StringBuilder)localObject3).append(paramString1);
          localObject4 = " IN ";
          ((StringBuilder)localObject3).append((String)localObject4);
          ((StringBuilder)localObject3).append((String)localObject2);
          localObject2 = ((StringBuilder)localObject3).toString();
          break label203;
        }
      }
      bool1 = false;
      localObject2 = null;
      label203:
      localObject3 = new String[2];
      Object localObject4 = null;
      localObject3[0] = paramString2;
      int m = 1;
      localObject3[m] = localObject2;
      localObject2 = c.a.m.e((Object[])localObject3);
      localObject3 = localObject2;
      localObject3 = (Collection)localObject2;
      boolean bool2 = ((Collection)localObject3).isEmpty() ^ m;
      if (!bool2)
      {
        bool1 = false;
        localObject2 = null;
      }
      if (localObject2 != null)
      {
        localObject6 = localObject2;
        localObject6 = (Iterable)localObject2;
        localObject7 = (CharSequence)" AND ";
        j = 0;
        localContentResolver = null;
        k = 0;
        int n = 62;
        localObject2 = c.a.m.a((Iterable)localObject6, (CharSequence)localObject7, null, null, 0, null, null, n);
      }
      else
      {
        bool1 = false;
        localObject2 = null;
      }
      long l1 = System.currentTimeMillis();
      localObject3 = this;
      ContentResolver localContentResolver = g;
      int j = localContentResolver.delete(localUri, (String)localObject2, null);
      long l2 = System.currentTimeMillis() - l1;
      Object localObject5 = new String[m];
      Object localObject6 = new java/lang/StringBuilder;
      ((StringBuilder)localObject6).<init>("delete from ");
      ((StringBuilder)localObject6).append(localUri);
      char c1 = ' ';
      ((StringBuilder)localObject6).append(c1);
      ((StringBuilder)localObject6).append(j);
      ((StringBuilder)localObject6).append(" items, took: ");
      ((StringBuilder)localObject6).append(l2);
      Object localObject7 = "ms\ndeleteWhere = ";
      ((StringBuilder)localObject6).append((String)localObject7);
      ((StringBuilder)localObject6).append((String)localObject2);
      localObject2 = ((StringBuilder)localObject6).toString();
      localObject5[0] = localObject2;
    }
    Object localObject3 = this;
  }
  
  public final w a(int paramInt, Collection paramCollection)
  {
    c.g.b.k.b(paramCollection, "ids");
    return b.a(paramInt, paramCollection);
  }
  
  public final w a(long paramLong)
  {
    return a.a(paramLong);
  }
  
  public final w a(FilterType paramFilterType, Integer paramInteger)
  {
    c.g.b.k.b(paramFilterType, "callTypeFilter");
    return a.a(paramFilterType, paramInteger);
  }
  
  public final w a(Contact paramContact)
  {
    c.g.b.k.b(paramContact, "contact");
    return a.a(paramContact);
  }
  
  public final w a(HistoryEvent paramHistoryEvent, Contact paramContact)
  {
    c.g.b.k.b(paramHistoryEvent, "event");
    c.g.b.k.b(paramContact, "contact");
    d.a(paramContact);
    paramContact = paramContact.getTcId();
    paramHistoryEvent.setTcId(paramContact);
    a(paramHistoryEvent);
    paramHistoryEvent = w.b(Boolean.TRUE);
    c.g.b.k.a(paramHistoryEvent, "Promise.wrap(true)");
    return paramHistoryEvent;
  }
  
  public final w a(String paramString)
  {
    c.g.b.k.b(paramString, "normalizedNumber");
    return a.a(paramString);
  }
  
  public final w a(List paramList)
  {
    c.g.b.k.b(paramList, "eventsToRestore");
    return a.a(paramList);
  }
  
  public final w a(List paramList1, List paramList2, HistoryEventsScope paramHistoryEventsScope)
  {
    c.g.b.k.b(paramHistoryEventsScope, "scope");
    Object localObject1 = d.a;
    int i = paramHistoryEventsScope.ordinal();
    int j = localObject1[i];
    switch (j)
    {
    default: 
      paramList1 = new c/l;
      paramList1.<init>();
      throw paramList1;
    case 3: 
      localObject1 = "type IN (1,2,3) ";
      break;
    case 2: 
      localObject1 = "type IN (1,2,3) \n     AND tc_flag != 3\n     ";
      break;
    case 1: 
      localObject1 = "tc_flag = 3";
    }
    try
    {
      Object localObject2 = HistoryEventsScope.ONLY_FLASH_EVENTS;
      if (paramHistoryEventsScope != localObject2)
      {
        paramHistoryEventsScope = CallLog.Calls.CONTENT_URI;
        localObject2 = "Calls.CONTENT_URI";
        c.g.b.k.a(paramHistoryEventsScope, (String)localObject2);
        localObject2 = "_id";
        a(paramHistoryEventsScope, (String)localObject2, paramList2, null);
      }
      paramList2 = TruecallerContract.n.a();
      paramHistoryEventsScope = "HistoryTable.getContentUri()";
      c.g.b.k.a(paramList2, paramHistoryEventsScope);
      paramHistoryEventsScope = "_id";
      a(paramList2, paramHistoryEventsScope, paramList1, (String)localObject1);
      paramList1 = w.b(Boolean.TRUE);
      c.g.b.k.a(paramList1, "Promise.wrap(true)");
      return paramList1;
    }
    catch (Exception localException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localException);
      paramList1 = w.b(Boolean.FALSE);
      c.g.b.k.a(paramList1, "Promise.wrap(false)");
    }
    return paramList1;
  }
  
  public final w a(Set paramSet)
  {
    c.g.b.k.b(paramSet, "callLogIds");
    paramSet = w.b(Boolean.valueOf(a.a(paramSet)));
    c.g.b.k.a(paramSet, "Promise.wrap(callLogMana…r.markAsSeen(callLogIds))");
    return paramSet;
  }
  
  public final void a()
  {
    c.a(true);
  }
  
  public final void a(int paramInt)
  {
    b.a(paramInt);
  }
  
  public final void a(ai.a parama)
  {
    c.g.b.k.b(parama, "batch");
    c.a(parama);
  }
  
  public final void a(CallRecording paramCallRecording)
  {
    c.g.b.k.b(paramCallRecording, "callRecording");
    f.a(paramCallRecording);
  }
  
  public final void a(HistoryEvent paramHistoryEvent)
  {
    c.g.b.k.b(paramHistoryEvent, "event");
    Object localObject = b;
    boolean bool1 = ((af)localObject).a(paramHistoryEvent);
    if (bool1)
    {
      b.b(paramHistoryEvent);
      return;
    }
    localObject = a;
    boolean bool2 = ((l)localObject).a(paramHistoryEvent);
    if (bool2)
    {
      paramHistoryEvent = c;
      bool1 = false;
      localObject = null;
      paramHistoryEvent.a(false);
    }
  }
  
  public final w b()
  {
    return e.a();
  }
  
  public final w b(int paramInt)
  {
    return b.b(paramInt);
  }
  
  public final w b(Set paramSet)
  {
    Object localObject1 = "historyIds";
    c.g.b.k.b(paramSet, (String)localObject1);
    try
    {
      localObject1 = new android/content/ContentValues;
      ((ContentValues)localObject1).<init>();
      Object localObject2 = "new";
      boolean bool1 = false;
      Integer localInteger = Integer.valueOf(0);
      ((ContentValues)localObject1).put((String)localObject2, localInteger);
      localObject2 = "is_read";
      int i = 1;
      Object localObject3 = Integer.valueOf(i);
      ((ContentValues)localObject1).put((String)localObject2, (Integer)localObject3);
      localObject2 = g;
      localObject3 = TruecallerContract.n.a();
      Object localObject4 = new java/lang/StringBuilder;
      Object localObject5 = "_id IN (";
      ((StringBuilder)localObject4).<init>((String)localObject5);
      localObject5 = "?";
      Object localObject6 = ",";
      int j = paramSet.size();
      localObject5 = org.c.a.a.a.k.a((String)localObject5, (String)localObject6, j);
      ((StringBuilder)localObject4).append((String)localObject5);
      char c1 = ')';
      ((StringBuilder)localObject4).append(c1);
      localObject4 = ((StringBuilder)localObject4).toString();
      paramSet = (Iterable)paramSet;
      localObject5 = new java/util/ArrayList;
      int k = 10;
      k = c.a.m.a(paramSet, k);
      ((ArrayList)localObject5).<init>(k);
      localObject5 = (Collection)localObject5;
      paramSet = paramSet.iterator();
      for (;;)
      {
        boolean bool2 = paramSet.hasNext();
        if (!bool2) {
          break;
        }
        localObject6 = paramSet.next();
        localObject6 = (Number)localObject6;
        long l = ((Number)localObject6).longValue();
        localObject6 = String.valueOf(l);
        ((Collection)localObject5).add(localObject6);
      }
      localObject5 = (List)localObject5;
      localObject5 = (Collection)localObject5;
      paramSet = new String[0];
      paramSet = ((Collection)localObject5).toArray(paramSet);
      if (paramSet != null)
      {
        paramSet = (String[])paramSet;
        int m = ((ContentResolver)localObject2).update((Uri)localObject3, (ContentValues)localObject1, (String)localObject4, paramSet);
        if (m != 0) {
          bool1 = true;
        }
        paramSet = Boolean.valueOf(bool1);
        paramSet = w.b(paramSet);
        localObject1 = "Promise.wrap(count != 0)";
        c.g.b.k.a(paramSet, (String)localObject1);
        return paramSet;
      }
      paramSet = new c/u;
      localObject1 = "null cannot be cast to non-null type kotlin.Array<T>";
      paramSet.<init>((String)localObject1);
      throw paramSet;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localIllegalArgumentException);
      paramSet = w.b(Boolean.FALSE);
      c.g.b.k.a(paramSet, "Promise.wrap(false)");
      return paramSet;
    }
    catch (SecurityException localSecurityException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localSecurityException);
      paramSet = w.b(Boolean.FALSE);
      c.g.b.k.a(paramSet, "Promise.wrap(false)");
      return paramSet;
    }
    catch (RuntimeExecutionException localRuntimeExecutionException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localRuntimeExecutionException);
      paramSet = w.b(Boolean.FALSE);
      c.g.b.k.a(paramSet, "Promise.wrap(false)");
    }
    return paramSet;
  }
  
  public final void b(long paramLong)
  {
    a.b(paramLong);
  }
  
  /* Error */
  public final void b(String paramString)
  {
    // Byte code:
    //   0: ldc -41
    //   2: astore_2
    //   3: aload_1
    //   4: aload_2
    //   5: invokestatic 27	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   8: aload_0
    //   9: getfield 57	com/truecaller/callhistory/c:g	Landroid/content/ContentResolver;
    //   12: astore_3
    //   13: invokestatic 267	com/truecaller/content/TruecallerContract$n:a	()Landroid/net/Uri;
    //   16: astore 4
    //   18: ldc_w 259
    //   21: astore_2
    //   22: ldc_w 412
    //   25: astore 5
    //   27: iconst_2
    //   28: anewarray 110	java/lang/String
    //   31: dup
    //   32: iconst_0
    //   33: aload_2
    //   34: aastore
    //   35: dup
    //   36: iconst_1
    //   37: aload 5
    //   39: aastore
    //   40: astore 5
    //   42: ldc_w 414
    //   45: astore 6
    //   47: iconst_1
    //   48: istore 7
    //   50: iload 7
    //   52: anewarray 110	java/lang/String
    //   55: astore 8
    //   57: aload 8
    //   59: iconst_0
    //   60: aload_1
    //   61: aastore
    //   62: aload_3
    //   63: aload 4
    //   65: aload 5
    //   67: aload 6
    //   69: aload 8
    //   71: aconst_null
    //   72: invokevirtual 418	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   75: astore_1
    //   76: aload_1
    //   77: ifnull +250 -> 327
    //   80: aload_1
    //   81: checkcast 420	java/io/Closeable
    //   84: astore_1
    //   85: aconst_null
    //   86: astore_3
    //   87: aload_1
    //   88: astore 4
    //   90: aload_1
    //   91: checkcast 422	android/database/Cursor
    //   94: astore 4
    //   96: new 424	java/util/LinkedHashSet
    //   99: astore 5
    //   101: aload 5
    //   103: invokespecial 425	java/util/LinkedHashSet:<init>	()V
    //   106: aload 5
    //   108: checkcast 355	java/util/Set
    //   111: astore 5
    //   113: new 424	java/util/LinkedHashSet
    //   116: astore 6
    //   118: aload 6
    //   120: invokespecial 425	java/util/LinkedHashSet:<init>	()V
    //   123: aload 6
    //   125: checkcast 355	java/util/Set
    //   128: astore 6
    //   130: aload 4
    //   132: invokeinterface 428 1 0
    //   137: istore 9
    //   139: iload 9
    //   141: ifeq +86 -> 227
    //   144: ldc_w 412
    //   147: astore 8
    //   149: aload 4
    //   151: aload 8
    //   153: invokestatic 433	com/truecaller/utils/extensions/k:c	(Landroid/database/Cursor;Ljava/lang/String;)J
    //   156: lstore 10
    //   158: ldc_w 259
    //   161: astore 12
    //   163: aload 4
    //   165: aload 12
    //   167: invokestatic 433	com/truecaller/utils/extensions/k:c	(Landroid/database/Cursor;Ljava/lang/String;)J
    //   170: lstore 13
    //   172: lconst_0
    //   173: lstore 15
    //   175: lload 10
    //   177: lload 15
    //   179: lcmp
    //   180: istore 17
    //   182: iload 17
    //   184: ifle +23 -> 207
    //   187: lload 10
    //   189: invokestatic 438	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   192: astore 8
    //   194: aload 5
    //   196: aload 8
    //   198: invokeinterface 439 2 0
    //   203: pop
    //   204: goto -74 -> 130
    //   207: lload 13
    //   209: invokestatic 438	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   212: astore 8
    //   214: aload 6
    //   216: aload 8
    //   218: invokeinterface 439 2 0
    //   223: pop
    //   224: goto -94 -> 130
    //   227: aload 5
    //   229: astore 4
    //   231: aload 5
    //   233: checkcast 116	java/util/Collection
    //   236: astore 4
    //   238: aload 4
    //   240: invokeinterface 119 1 0
    //   245: iload 7
    //   247: ixor
    //   248: istore 18
    //   250: iload 18
    //   252: ifeq +10 -> 262
    //   255: aload_0
    //   256: aload 5
    //   258: invokevirtual 442	com/truecaller/callhistory/c:a	(Ljava/util/Set;)Lcom/truecaller/androidactors/w;
    //   261: pop
    //   262: aload 6
    //   264: astore 4
    //   266: aload 6
    //   268: checkcast 116	java/util/Collection
    //   271: astore 4
    //   273: aload 4
    //   275: invokeinterface 119 1 0
    //   280: istore 18
    //   282: iload 7
    //   284: iload 18
    //   286: ixor
    //   287: istore 7
    //   289: iload 7
    //   291: ifeq +10 -> 301
    //   294: aload_0
    //   295: aload 6
    //   297: invokevirtual 444	com/truecaller/callhistory/c:b	(Ljava/util/Set;)Lcom/truecaller/androidactors/w;
    //   300: pop
    //   301: getstatic 449	c/x:a	Lc/x;
    //   304: astore_2
    //   305: aload_1
    //   306: aconst_null
    //   307: invokestatic 454	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   310: return
    //   311: astore_2
    //   312: goto +8 -> 320
    //   315: astore_2
    //   316: aload_2
    //   317: astore_3
    //   318: aload_2
    //   319: athrow
    //   320: aload_1
    //   321: aload_3
    //   322: invokestatic 454	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   325: aload_2
    //   326: athrow
    //   327: return
    //   328: checkcast 271	java/lang/Throwable
    //   331: invokestatic 277	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   334: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	335	0	this	c
    //   0	335	1	paramString	String
    //   2	303	2	localObject1	Object
    //   311	1	2	localObject2	Object
    //   315	11	2	localObject3	Object
    //   12	310	3	localObject4	Object
    //   16	258	4	localObject5	Object
    //   25	232	5	localObject6	Object
    //   45	251	6	localObject7	Object
    //   48	242	7	bool1	boolean
    //   55	162	8	localObject8	Object
    //   137	3	9	bool2	boolean
    //   156	32	10	l1	long
    //   161	5	12	str	String
    //   170	38	13	l2	long
    //   173	5	15	l3	long
    //   180	3	17	bool3	boolean
    //   248	39	18	bool4	boolean
    //   328	1	18	localSQLiteException	android.database.sqlite.SQLiteException
    // Exception table:
    //   from	to	target	type
    //   318	320	311	finally
    //   90	94	315	finally
    //   96	99	315	finally
    //   101	106	315	finally
    //   106	111	315	finally
    //   113	116	315	finally
    //   118	123	315	finally
    //   123	128	315	finally
    //   130	137	315	finally
    //   151	156	315	finally
    //   165	170	315	finally
    //   187	192	315	finally
    //   196	204	315	finally
    //   207	212	315	finally
    //   216	224	315	finally
    //   231	236	315	finally
    //   238	245	315	finally
    //   256	262	315	finally
    //   266	271	315	finally
    //   273	280	315	finally
    //   295	301	315	finally
    //   301	304	315	finally
    //   8	12	328	android/database/sqlite/SQLiteException
    //   13	16	328	android/database/sqlite/SQLiteException
    //   27	40	328	android/database/sqlite/SQLiteException
    //   50	55	328	android/database/sqlite/SQLiteException
    //   60	62	328	android/database/sqlite/SQLiteException
    //   71	75	328	android/database/sqlite/SQLiteException
    //   80	84	328	android/database/sqlite/SQLiteException
    //   306	310	328	android/database/sqlite/SQLiteException
    //   321	325	328	android/database/sqlite/SQLiteException
    //   325	327	328	android/database/sqlite/SQLiteException
  }
  
  public final w c()
  {
    return a.a();
  }
  
  public final w c(int paramInt)
  {
    return a.a(paramInt);
  }
  
  public final w c(String paramString)
  {
    c.g.b.k.b(paramString, "normalizedNumber");
    return a.b(paramString);
  }
  
  public final void c(long paramLong)
  {
    a.c(paramLong);
  }
  
  public final w d()
  {
    return a(Long.MAX_VALUE);
  }
  
  public final w d(String paramString)
  {
    c.g.b.k.b(paramString, "tcId");
    return a.c(paramString);
  }
  
  public final w e()
  {
    return a.b();
  }
  
  public final void f()
  {
    a.c();
  }
  
  public final void g()
  {
    a.d();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */