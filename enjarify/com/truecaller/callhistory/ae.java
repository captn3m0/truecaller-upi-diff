package com.truecaller.callhistory;

import android.content.Context;
import android.net.Uri;
import org.c.a.a.a.a;

public final class ae
  extends ab
{
  private static final Uri b = Uri.parse("content://logs/call");
  private static final String[] c;
  
  static
  {
    String[] arrayOfString1 = a;
    String[] tmp16_13 = new String[4];
    String[] tmp17_16 = tmp16_13;
    String[] tmp17_16 = tmp16_13;
    tmp17_16[0] = "normalized_number";
    tmp17_16[1] = "features";
    tmp17_16[2] = "subscription_component_name";
    String[] tmp30_17 = tmp17_16;
    tmp30_17[3] = "logtype";
    String[] arrayOfString2 = tmp30_17;
    c = (String[])a.a(arrayOfString1, arrayOfString2);
  }
  
  ae(Context paramContext)
  {
    super(paramContext);
  }
  
  /* Error */
  public static boolean b(Context paramContext)
  {
    // Byte code:
    //   0: ldc 45
    //   2: astore_1
    //   3: getstatic 51	android/os/Build:BRAND	Ljava/lang/String;
    //   6: astore_2
    //   7: aload_1
    //   8: aload_2
    //   9: invokevirtual 55	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   12: istore_3
    //   13: aconst_null
    //   14: astore_2
    //   15: iload_3
    //   16: ifne +5 -> 21
    //   19: iconst_0
    //   20: ireturn
    //   21: ldc 57
    //   23: astore 4
    //   25: ldc 59
    //   27: astore 5
    //   29: ldc 61
    //   31: astore 6
    //   33: ldc 25
    //   35: astore 7
    //   37: ldc 63
    //   39: astore 8
    //   41: ldc 65
    //   43: astore 9
    //   45: ldc 27
    //   47: astore 10
    //   49: ldc 67
    //   51: astore 11
    //   53: ldc 69
    //   55: astore 12
    //   57: ldc 71
    //   59: astore 13
    //   61: bipush 10
    //   63: anewarray 31	java/lang/String
    //   66: dup
    //   67: dup2
    //   68: iconst_0
    //   69: aload 4
    //   71: aastore
    //   72: iconst_1
    //   73: aload 5
    //   75: aastore
    //   76: dup2
    //   77: iconst_2
    //   78: aload 6
    //   80: aastore
    //   81: iconst_3
    //   82: aload 7
    //   84: aastore
    //   85: dup2
    //   86: iconst_4
    //   87: aload 8
    //   89: aastore
    //   90: iconst_5
    //   91: aload 9
    //   93: aastore
    //   94: dup2
    //   95: bipush 6
    //   97: aload 10
    //   99: aastore
    //   100: bipush 7
    //   102: aload 11
    //   104: aastore
    //   105: bipush 8
    //   107: aload 12
    //   109: aastore
    //   110: dup
    //   111: bipush 9
    //   113: aload 13
    //   115: aastore
    //   116: astore 14
    //   118: iconst_1
    //   119: istore_3
    //   120: aload_0
    //   121: invokevirtual 78	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   124: astore 15
    //   126: getstatic 18	com/truecaller/callhistory/ae:b	Landroid/net/Uri;
    //   129: astore 16
    //   131: aload 15
    //   133: aload 16
    //   135: aload 14
    //   137: aconst_null
    //   138: aconst_null
    //   139: aconst_null
    //   140: invokevirtual 84	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   143: astore 4
    //   145: aload 4
    //   147: ifnull +10 -> 157
    //   150: aload 4
    //   152: invokeinterface 90 1 0
    //   157: aload 4
    //   159: ifnull +5 -> 164
    //   162: iload_3
    //   163: ireturn
    //   164: iconst_0
    //   165: ireturn
    //   166: astore_1
    //   167: goto +57 -> 224
    //   170: pop
    //   171: iconst_0
    //   172: ireturn
    //   173: pop
    //   174: getstatic 96	android/os/Build$VERSION:SDK_INT	I
    //   177: istore 17
    //   179: bipush 23
    //   181: istore 18
    //   183: iload 17
    //   185: iload 18
    //   187: if_icmplt +31 -> 218
    //   190: ldc 99
    //   192: astore 4
    //   194: aload_0
    //   195: astore 5
    //   197: aload_0
    //   198: aload 4
    //   200: invokevirtual 103	android/content/Context:checkSelfPermission	(Ljava/lang/String;)I
    //   203: istore 17
    //   205: iconst_m1
    //   206: istore 18
    //   208: iload 17
    //   210: iload 18
    //   212: if_icmpne +6 -> 218
    //   215: goto +7 -> 222
    //   218: iconst_0
    //   219: istore_3
    //   220: aconst_null
    //   221: astore_1
    //   222: iload_3
    //   223: ireturn
    //   224: aload_1
    //   225: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	226	0	paramContext	Context
    //   2	6	1	str1	String
    //   166	1	1	localObject1	Object
    //   221	4	1	localObject2	Object
    //   6	9	2	str2	String
    //   12	211	3	bool	boolean
    //   23	176	4	localObject3	Object
    //   27	169	5	localObject4	Object
    //   31	48	6	str3	String
    //   35	48	7	str4	String
    //   39	49	8	str5	String
    //   43	49	9	str6	String
    //   47	51	10	str7	String
    //   51	52	11	str8	String
    //   55	53	12	str9	String
    //   59	55	13	str10	String
    //   116	20	14	arrayOfString	String[]
    //   124	8	15	localContentResolver	android.content.ContentResolver
    //   129	5	16	localUri	Uri
    //   177	36	17	i	int
    //   181	32	18	j	int
    //   170	1	21	localException	Exception
    //   173	1	22	localSecurityException	SecurityException
    // Exception table:
    //   from	to	target	type
    //   120	124	166	finally
    //   126	129	166	finally
    //   139	143	166	finally
    //   174	177	166	finally
    //   198	203	166	finally
    //   120	124	170	java/lang/Exception
    //   126	129	170	java/lang/Exception
    //   139	143	170	java/lang/Exception
    //   120	124	173	java/lang/SecurityException
    //   126	129	173	java/lang/SecurityException
    //   139	143	173	java/lang/SecurityException
  }
  
  public final Uri a()
  {
    return b;
  }
  
  public final String[] b()
  {
    return c;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.ae
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */