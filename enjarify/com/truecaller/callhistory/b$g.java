package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.Collection;

final class b$g
  extends u
{
  private final int b;
  private final Collection c;
  
  private b$g(e parame, int paramInt, Collection paramCollection)
  {
    super(parame);
    b = paramInt;
    c = paramCollection;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".deleteSearchHistoryForIds(");
    Object localObject = Integer.valueOf(b);
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */