package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.data.entity.CallRecording;
import java.util.Collection;

public final class s
  implements r
{
  private final v a;
  
  public s(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return r.class.equals(paramClass);
  }
  
  public final w a()
  {
    v localv = a;
    s.c localc = new com/truecaller/callhistory/s$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, (byte)0);
    return w.a(localv, localc);
  }
  
  public final w a(CallRecording paramCallRecording)
  {
    v localv = a;
    s.a locala = new com/truecaller/callhistory/s$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramCallRecording, (byte)0);
    return w.a(localv, locala);
  }
  
  public final w a(Collection paramCollection)
  {
    v localv = a;
    s.b localb = new com/truecaller/callhistory/s$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramCollection, (byte)0);
    return w.a(localv, localb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */