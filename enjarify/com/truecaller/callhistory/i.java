package com.truecaller.callhistory;

import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d
{
  private final g a;
  private final Provider b;
  
  private i(g paramg, Provider paramProvider)
  {
    a = paramg;
    b = paramProvider;
  }
  
  public static i a(g paramg, Provider paramProvider)
  {
    i locali = new com/truecaller/callhistory/i;
    locali.<init>(paramg, paramProvider);
    return locali;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */