package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class b$i
  extends u
{
  private final String b;
  
  private b$i(e parame, String paramString)
  {
    super(parame);
    b = paramString;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".getCallHistoryByNumber(");
    String str = a(b, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */