package com.truecaller.callhistory;

import com.truecaller.androidactors.w;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import java.util.List;
import java.util.Set;

public abstract interface l
{
  public abstract w a();
  
  public abstract w a(int paramInt);
  
  public abstract w a(long paramLong);
  
  public abstract w a(FilterType paramFilterType, Integer paramInteger);
  
  public abstract w a(Contact paramContact);
  
  public abstract w a(String paramString);
  
  public abstract w a(List paramList);
  
  public abstract boolean a(HistoryEvent paramHistoryEvent);
  
  public abstract boolean a(Set paramSet);
  
  public abstract w b();
  
  public abstract w b(String paramString);
  
  public abstract void b(long paramLong);
  
  public abstract w c(String paramString);
  
  public abstract void c();
  
  public abstract void c(long paramLong);
  
  public abstract void d();
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */