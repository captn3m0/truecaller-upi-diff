package com.truecaller.callhistory;

import android.content.Context;
import android.net.Uri;
import android.os.Build.VERSION;
import android.provider.CallLog.Calls;

public abstract class k
{
  static final String[] a = tmp38_24;
  private static volatile k b;
  
  static
  {
    String[] tmp5_2 = new String[8];
    String[] tmp6_5 = tmp5_2;
    String[] tmp6_5 = tmp5_2;
    tmp6_5[0] = "_id";
    tmp6_5[1] = "date";
    String[] tmp15_6 = tmp6_5;
    String[] tmp15_6 = tmp6_5;
    tmp15_6[2] = "number";
    tmp15_6[3] = "type";
    String[] tmp24_15 = tmp15_6;
    String[] tmp24_15 = tmp15_6;
    tmp24_15[4] = "duration";
    tmp24_15[5] = "name";
    tmp24_15[6] = "new";
    String[] tmp38_24 = tmp24_15;
    tmp38_24[7] = "is_read";
  }
  
  public static k a(Context paramContext)
  {
    ??? = b;
    if (??? != null) {
      return (k)???;
    }
    synchronized (k.class)
    {
      Object localObject2 = b;
      if (localObject2 != null) {
        return (k)localObject2;
      }
      int i = Build.VERSION.SDK_INT;
      int j = 22;
      if (i >= j)
      {
        boolean bool = ae.b(paramContext);
        if (bool)
        {
          localObject2 = new com/truecaller/callhistory/ae;
          ((ae)localObject2).<init>(paramContext);
        }
        else
        {
          localObject2 = new com/truecaller/callhistory/ab;
          ((ab)localObject2).<init>(paramContext);
        }
      }
      else
      {
        localObject2 = new com/truecaller/callhistory/y;
        ((y)localObject2).<init>();
      }
      b = (k)localObject2;
      return (k)localObject2;
    }
  }
  
  public abstract int a(int paramInt);
  
  public Uri a()
  {
    return CallLog.Calls.CONTENT_URI;
  }
  
  public abstract String[] b();
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */