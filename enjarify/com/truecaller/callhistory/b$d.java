package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;

final class b$d
  extends u
{
  private final HistoryEvent b;
  private final Contact c;
  
  private b$d(e parame, HistoryEvent paramHistoryEvent, Contact paramContact)
  {
    super(parame);
    b = paramHistoryEvent;
    c = paramContact;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".addWithContact(");
    Object localObject = b;
    int i = 1;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */