package com.truecaller.callhistory;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.androidactors.ab;
import com.truecaller.androidactors.w;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.log.AssertionUtil;
import com.truecaller.util.q;

public final class aq
  implements ap
{
  private final Context a;
  
  public aq(Context paramContext)
  {
    a = paramContext;
  }
  
  public final w a()
  {
    w localw = null;
    Object localObject1;
    try
    {
      localObject1 = a;
      Object localObject2 = ((Context)localObject1).getContentResolver();
      Object localObject3 = TruecallerContract.n.d();
      String str1 = "action NOT IN (5)  AND tc_flag!=3 AND type!=6";
      String str2 = "timestamp DESC LIMIT 20";
      localObject1 = ((ContentResolver)localObject2).query((Uri)localObject3, null, str1, null, str2);
      if (localObject1 == null) {
        break label98;
      }
      try
      {
        localObject2 = e.a((Cursor)localObject1);
        localObject3 = aq.a.a;
        localObject3 = (ab)localObject3;
        localObject2 = w.a(localObject2, (ab)localObject3);
        localObject3 = "Promise.wrap<HistoryEven…tCursor()) { it.close() }";
        k.a(localObject2, (String)localObject3);
        return (w)localObject2;
      }
      catch (SQLiteException localSQLiteException1) {}
      localThrowable = (Throwable)localSQLiteException2;
    }
    catch (SQLiteException localSQLiteException2)
    {
      localObject1 = null;
    }
    Throwable localThrowable;
    AssertionUtil.reportThrowableButNeverCrash(localThrowable);
    q.a((Cursor)localObject1);
    label98:
    localw = w.b(null);
    k.a(localw, "Promise.wrap(null)");
    return localw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.aq
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */