package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.Collection;

final class s$b
  extends u
{
  private final Collection b;
  
  private s$b(e parame, Collection paramCollection)
  {
    super(parame);
    b = paramCollection;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".delete(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.s.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */