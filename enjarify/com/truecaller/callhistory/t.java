package com.truecaller.callhistory;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.androidactors.ab;
import com.truecaller.androidactors.w;
import com.truecaller.content.TruecallerContract.c;
import com.truecaller.data.access.d;
import com.truecaller.data.access.e;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.util.aq;

public final class t
  implements r
{
  private final ContentResolver a;
  private final aq b;
  
  public t(ContentResolver paramContentResolver, aq paramaq)
  {
    a = paramContentResolver;
    b = paramaq;
  }
  
  public final w a()
  {
    Object localObject1 = a;
    Object localObject2 = TruecallerContract.c.b();
    String str = "timestamp DESC";
    Object localObject3 = null;
    d locald = null;
    localObject1 = ((ContentResolver)localObject1).query((Uri)localObject2, null, null, null, str);
    if (localObject1 == null)
    {
      localObject1 = w.b(null);
      k.a(localObject1, "Promise.wrap(null)");
      return (w)localObject1;
    }
    localObject2 = new com/truecaller/callhistory/aa;
    localObject3 = new com/truecaller/data/access/e;
    ((e)localObject3).<init>((Cursor)localObject1);
    locald = new com/truecaller/data/access/d;
    locald.<init>((Cursor)localObject1);
    ((aa)localObject2).<init>((Cursor)localObject1, (e)localObject3, locald);
    localObject3 = new com/truecaller/callhistory/t$a;
    ((t.a)localObject3).<init>((Cursor)localObject1);
    localObject3 = (ab)localObject3;
    localObject1 = w.a(localObject2, (ab)localObject3);
    k.a(localObject1, "Promise.wrap(\n          …eaner { cursor.close() })");
    return (w)localObject1;
  }
  
  public final w a(CallRecording paramCallRecording)
  {
    Object localObject1 = "callRecording";
    k.b(paramCallRecording, (String)localObject1);
    try
    {
      localObject1 = b;
      Object localObject2 = c;
      ((aq)localObject1).c((String)localObject2);
      localObject1 = a;
      localObject2 = TruecallerContract.c.a();
      String str = "history_event_id=?";
      boolean bool = true;
      String[] arrayOfString = new String[bool];
      paramCallRecording = b;
      arrayOfString[0] = paramCallRecording;
      int i = ((ContentResolver)localObject1).delete((Uri)localObject2, str, arrayOfString);
      if (i <= 0) {
        bool = false;
      }
      paramCallRecording = Boolean.valueOf(bool);
      paramCallRecording = w.b(paramCallRecording);
      localObject1 = "Promise.wrap(count > 0)";
      k.a(paramCallRecording, (String)localObject1);
    }
    catch (Exception localException)
    {
      paramCallRecording = w.b(Boolean.FALSE);
      localObject1 = "Promise.wrap(false)";
      k.a(paramCallRecording, (String)localObject1);
    }
    return paramCallRecording;
  }
  
  /* Error */
  public final w a(java.util.Collection paramCollection)
  {
    // Byte code:
    //   0: ldc 116
    //   2: astore_2
    //   3: aload_1
    //   4: aload_2
    //   5: invokestatic 17	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   8: aload_1
    //   9: astore_3
    //   10: aload_1
    //   11: checkcast 118	java/lang/Iterable
    //   14: astore_3
    //   15: ldc 120
    //   17: astore_1
    //   18: aload_1
    //   19: astore 4
    //   21: aload_1
    //   22: checkcast 122	java/lang/CharSequence
    //   25: astore 4
    //   27: aconst_null
    //   28: astore 5
    //   30: iconst_0
    //   31: istore 6
    //   33: aconst_null
    //   34: astore 7
    //   36: iconst_0
    //   37: istore 8
    //   39: bipush 62
    //   41: istore 9
    //   43: aload_3
    //   44: aload 4
    //   46: aconst_null
    //   47: aconst_null
    //   48: iconst_0
    //   49: aconst_null
    //   50: aconst_null
    //   51: iload 9
    //   53: invokestatic 128	c/a/m:a	(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lc/g/a/b;I)Ljava/lang/String;
    //   56: astore_1
    //   57: aload_0
    //   58: getfield 25	com/truecaller/callhistory/t:a	Landroid/content/ContentResolver;
    //   61: astore_2
    //   62: invokestatic 87	com/truecaller/content/TruecallerContract$c:a	()Landroid/net/Uri;
    //   65: astore_3
    //   66: ldc -126
    //   68: astore 4
    //   70: iconst_1
    //   71: anewarray 92	java/lang/String
    //   74: dup
    //   75: iconst_0
    //   76: aload 4
    //   78: aastore
    //   79: astore 4
    //   81: new 132	java/lang/StringBuilder
    //   84: astore 5
    //   86: ldc -122
    //   88: astore 7
    //   90: aload 5
    //   92: aload 7
    //   94: invokespecial 137	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   97: aload 5
    //   99: aload_1
    //   100: invokevirtual 141	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   103: pop
    //   104: bipush 41
    //   106: istore 8
    //   108: aload 5
    //   110: iload 8
    //   112: invokevirtual 145	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   115: pop
    //   116: aload 5
    //   118: invokevirtual 149	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   121: astore 5
    //   123: iconst_0
    //   124: istore 6
    //   126: aconst_null
    //   127: astore 7
    //   129: aload_2
    //   130: aload_3
    //   131: aload 4
    //   133: aload 5
    //   135: aconst_null
    //   136: aconst_null
    //   137: invokevirtual 40	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   140: astore_2
    //   141: aconst_null
    //   142: astore_3
    //   143: aload_2
    //   144: ifnull +164 -> 308
    //   147: aload_2
    //   148: astore 4
    //   150: aload_2
    //   151: checkcast 151	java/io/Closeable
    //   154: astore 4
    //   156: new 153	java/util/ArrayList
    //   159: astore 5
    //   161: aload 5
    //   163: invokespecial 154	java/util/ArrayList:<init>	()V
    //   166: aload 5
    //   168: checkcast 156	java/util/Collection
    //   171: astore 5
    //   173: aload_2
    //   174: invokeinterface 162 1 0
    //   179: istore 6
    //   181: iload 6
    //   183: ifeq +28 -> 211
    //   186: ldc -126
    //   188: astore 7
    //   190: aload_2
    //   191: aload 7
    //   193: invokestatic 167	com/truecaller/utils/extensions/k:a	(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    //   196: astore 7
    //   198: aload 5
    //   200: aload 7
    //   202: invokeinterface 171 2 0
    //   207: pop
    //   208: goto -35 -> 173
    //   211: aload 5
    //   213: checkcast 173	java/util/List
    //   216: astore 5
    //   218: aload 4
    //   220: aconst_null
    //   221: invokestatic 178	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   224: aload 5
    //   226: checkcast 118	java/lang/Iterable
    //   229: astore 5
    //   231: aload 5
    //   233: invokeinterface 182 1 0
    //   238: astore_2
    //   239: aload_2
    //   240: invokeinterface 187 1 0
    //   245: istore 10
    //   247: iload 10
    //   249: ifeq +59 -> 308
    //   252: aload_2
    //   253: invokeinterface 191 1 0
    //   258: astore 4
    //   260: aload 4
    //   262: checkcast 92	java/lang/String
    //   265: astore 4
    //   267: aload 4
    //   269: ifnull -30 -> 239
    //   272: aload_0
    //   273: getfield 27	com/truecaller/callhistory/t:b	Lcom/truecaller/util/aq;
    //   276: astore 5
    //   278: aload 5
    //   280: aload 4
    //   282: invokeinterface 85 2 0
    //   287: pop
    //   288: goto -49 -> 239
    //   291: astore_1
    //   292: goto +8 -> 300
    //   295: astore_1
    //   296: aload_1
    //   297: astore_3
    //   298: aload_1
    //   299: athrow
    //   300: aload 4
    //   302: aload_3
    //   303: invokestatic 178	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   306: aload_1
    //   307: athrow
    //   308: aload_0
    //   309: getfield 25	com/truecaller/callhistory/t:a	Landroid/content/ContentResolver;
    //   312: astore_2
    //   313: invokestatic 87	com/truecaller/content/TruecallerContract$c:a	()Landroid/net/Uri;
    //   316: astore 4
    //   318: new 132	java/lang/StringBuilder
    //   321: astore 5
    //   323: ldc -122
    //   325: astore 7
    //   327: aload 5
    //   329: aload 7
    //   331: invokespecial 137	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   334: aload 5
    //   336: aload_1
    //   337: invokevirtual 141	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   340: pop
    //   341: aload 5
    //   343: iload 8
    //   345: invokevirtual 145	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   348: pop
    //   349: aload 5
    //   351: invokevirtual 149	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   354: astore_1
    //   355: aload_2
    //   356: aload 4
    //   358: aload_1
    //   359: aconst_null
    //   360: invokevirtual 98	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   363: istore 11
    //   365: iload 11
    //   367: ifle +9 -> 376
    //   370: iconst_1
    //   371: istore 11
    //   373: goto +8 -> 381
    //   376: iconst_0
    //   377: istore 11
    //   379: aconst_null
    //   380: astore_1
    //   381: iload 11
    //   383: invokestatic 104	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   386: astore_1
    //   387: aload_1
    //   388: invokestatic 45	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   391: astore_1
    //   392: ldc 106
    //   394: astore_2
    //   395: aload_1
    //   396: aload_2
    //   397: invokestatic 49	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   400: goto +19 -> 419
    //   403: pop
    //   404: getstatic 110	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   407: invokestatic 45	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   410: astore_1
    //   411: ldc 112
    //   413: astore_2
    //   414: aload_1
    //   415: aload_2
    //   416: invokestatic 49	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   419: aload_1
    //   420: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	421	0	this	t
    //   0	421	1	paramCollection	java.util.Collection
    //   2	414	2	localObject1	Object
    //   9	294	3	localObject2	Object
    //   19	338	4	localObject3	Object
    //   28	322	5	localObject4	Object
    //   31	151	6	bool1	boolean
    //   34	296	7	str	String
    //   37	307	8	c	char
    //   41	11	9	i	int
    //   245	3	10	bool2	boolean
    //   363	19	11	j	int
    //   403	1	12	localException	Exception
    // Exception table:
    //   from	to	target	type
    //   298	300	291	finally
    //   156	159	295	finally
    //   161	166	295	finally
    //   166	171	295	finally
    //   173	179	295	finally
    //   191	196	295	finally
    //   200	208	295	finally
    //   211	216	295	finally
    //   10	14	403	java/lang/Exception
    //   21	25	403	java/lang/Exception
    //   51	56	403	java/lang/Exception
    //   57	61	403	java/lang/Exception
    //   62	65	403	java/lang/Exception
    //   70	79	403	java/lang/Exception
    //   81	84	403	java/lang/Exception
    //   92	97	403	java/lang/Exception
    //   99	104	403	java/lang/Exception
    //   110	116	403	java/lang/Exception
    //   116	121	403	java/lang/Exception
    //   136	140	403	java/lang/Exception
    //   150	154	403	java/lang/Exception
    //   220	224	403	java/lang/Exception
    //   224	229	403	java/lang/Exception
    //   231	238	403	java/lang/Exception
    //   239	245	403	java/lang/Exception
    //   252	258	403	java/lang/Exception
    //   260	265	403	java/lang/Exception
    //   272	276	403	java/lang/Exception
    //   280	288	403	java/lang/Exception
    //   302	306	403	java/lang/Exception
    //   306	308	403	java/lang/Exception
    //   308	312	403	java/lang/Exception
    //   313	316	403	java/lang/Exception
    //   318	321	403	java/lang/Exception
    //   329	334	403	java/lang/Exception
    //   336	341	403	java/lang/Exception
    //   343	349	403	java/lang/Exception
    //   349	354	403	java/lang/Exception
    //   359	363	403	java/lang/Exception
    //   381	386	403	java/lang/Exception
    //   387	391	403	java/lang/Exception
    //   396	400	403	java/lang/Exception
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */