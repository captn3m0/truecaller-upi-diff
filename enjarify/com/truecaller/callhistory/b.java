package com.truecaller.callhistory;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.calling.dialer.HistoryEventsScope;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public final class b
  implements a
{
  private final v a;
  
  public b(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return a.class.equals(paramClass);
  }
  
  public final w a(int paramInt, Collection paramCollection)
  {
    v localv = a;
    b.g localg = new com/truecaller/callhistory/b$g;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localg.<init>(locale, paramInt, paramCollection, (byte)0);
    return w.a(localv, localg);
  }
  
  public final w a(long paramLong)
  {
    v localv = a;
    b.r localr = new com/truecaller/callhistory/b$r;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localr.<init>(locale, paramLong, (byte)0);
    return w.a(localv, localr);
  }
  
  public final w a(FilterType paramFilterType, Integer paramInteger)
  {
    v localv = a;
    b.k localk = new com/truecaller/callhistory/b$k;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localk.<init>(locale, paramFilterType, paramInteger, (byte)0);
    return w.a(localv, localk);
  }
  
  public final w a(Contact paramContact)
  {
    v localv = a;
    b.j localj = new com/truecaller/callhistory/b$j;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localj.<init>(locale, paramContact, (byte)0);
    return w.a(localv, localj);
  }
  
  public final w a(HistoryEvent paramHistoryEvent, Contact paramContact)
  {
    v localv = a;
    b.d locald = new com/truecaller/callhistory/b$d;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locald.<init>(locale, paramHistoryEvent, paramContact, (byte)0);
    return w.a(localv, locald);
  }
  
  public final w a(String paramString)
  {
    v localv = a;
    b.i locali = new com/truecaller/callhistory/b$i;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locali.<init>(locale, paramString, (byte)0);
    return w.a(localv, locali);
  }
  
  public final w a(List paramList)
  {
    v localv = a;
    b.b localb = new com/truecaller/callhistory/b$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramList, (byte)0);
    return w.a(localv, localb);
  }
  
  public final w a(List paramList1, List paramList2, HistoryEventsScope paramHistoryEventsScope)
  {
    v localv = a;
    b.f localf = new com/truecaller/callhistory/b$f;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localf.<init>(locale, paramList1, paramList2, paramHistoryEventsScope, (byte)0);
    return w.a(localv, localf);
  }
  
  public final w a(Set paramSet)
  {
    v localv = a;
    b.w localw = new com/truecaller/callhistory/b$w;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localw.<init>(locale, paramSet, (byte)0);
    return w.a(localv, localw);
  }
  
  public final void a()
  {
    v localv = a;
    b.z localz = new com/truecaller/callhistory/b$z;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localz.<init>(locale, (byte)0);
    localv.a(localz);
  }
  
  public final void a(int paramInt)
  {
    v localv = a;
    b.e locale = new com/truecaller/callhistory/b$e;
    e locale1 = new com/truecaller/androidactors/e;
    locale1.<init>();
    locale.<init>(locale1, paramInt, (byte)0);
    localv.a(locale);
  }
  
  public final void a(ai.a parama)
  {
    v localv = a;
    b.aa localaa = new com/truecaller/callhistory/b$aa;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localaa.<init>(locale, parama, (byte)0);
    localv.a(localaa);
  }
  
  public final void a(CallRecording paramCallRecording)
  {
    v localv = a;
    b.a locala = new com/truecaller/callhistory/b$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramCallRecording, (byte)0);
    localv.a(locala);
  }
  
  public final void a(HistoryEvent paramHistoryEvent)
  {
    v localv = a;
    b.c localc = new com/truecaller/callhistory/b$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, paramHistoryEvent, (byte)0);
    localv.a(localc);
  }
  
  public final w b()
  {
    v localv = a;
    b.h localh = new com/truecaller/callhistory/b$h;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localh.<init>(locale, (byte)0);
    return w.a(localv, localh);
  }
  
  public final w b(int paramInt)
  {
    v localv = a;
    b.s locals = new com/truecaller/callhistory/b$s;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locals.<init>(locale, paramInt, (byte)0);
    return w.a(localv, locals);
  }
  
  public final w b(Set paramSet)
  {
    v localv = a;
    b.u localu = new com/truecaller/callhistory/b$u;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localu.<init>(locale, paramSet, (byte)0);
    return w.a(localv, localu);
  }
  
  public final void b(long paramLong)
  {
    v localv = a;
    b.y localy = new com/truecaller/callhistory/b$y;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localy.<init>(locale, paramLong, (byte)0);
    localv.a(localy);
  }
  
  public final void b(String paramString)
  {
    v localv = a;
    b.x localx = new com/truecaller/callhistory/b$x;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localx.<init>(locale, paramString, (byte)0);
    localv.a(localx);
  }
  
  public final w c()
  {
    v localv = a;
    b.l locall = new com/truecaller/callhistory/b$l;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locall.<init>(locale, (byte)0);
    return w.a(localv, locall);
  }
  
  public final w c(int paramInt)
  {
    v localv = a;
    b.p localp = new com/truecaller/callhistory/b$p;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localp.<init>(locale, paramInt, (byte)0);
    return w.a(localv, localp);
  }
  
  public final w c(String paramString)
  {
    v localv = a;
    b.m localm = new com/truecaller/callhistory/b$m;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localm.<init>(locale, paramString, (byte)0);
    return w.a(localv, localm);
  }
  
  public final void c(long paramLong)
  {
    v localv = a;
    b.v localv1 = new com/truecaller/callhistory/b$v;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localv1.<init>(locale, paramLong, (byte)0);
    localv.a(localv1);
  }
  
  public final w d()
  {
    v localv = a;
    b.q localq = new com/truecaller/callhistory/b$q;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localq.<init>(locale, (byte)0);
    return w.a(localv, localq);
  }
  
  public final w d(String paramString)
  {
    v localv = a;
    b.n localn = new com/truecaller/callhistory/b$n;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localn.<init>(locale, paramString, (byte)0);
    return w.a(localv, localn);
  }
  
  public final w e()
  {
    v localv = a;
    b.o localo = new com/truecaller/callhistory/b$o;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localo.<init>(locale, (byte)0);
    return w.a(localv, localo);
  }
  
  public final void f()
  {
    v localv = a;
    b.t localt = new com/truecaller/callhistory/b$t;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localt.<init>(locale, (byte)0);
    localv.a(localt);
  }
  
  public final void g()
  {
    v localv = a;
    b.ab localab = new com/truecaller/callhistory/b$ab;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localab.<init>(locale, (byte)0);
    localv.a(localab);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.callhistory.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */