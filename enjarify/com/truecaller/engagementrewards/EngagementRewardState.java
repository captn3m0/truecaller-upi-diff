package com.truecaller.engagementrewards;

public enum EngagementRewardState
{
  static
  {
    EngagementRewardState[] arrayOfEngagementRewardState = new EngagementRewardState[6];
    EngagementRewardState localEngagementRewardState = new com/truecaller/engagementrewards/EngagementRewardState;
    localEngagementRewardState.<init>("NONE", 0);
    NONE = localEngagementRewardState;
    arrayOfEngagementRewardState[0] = localEngagementRewardState;
    localEngagementRewardState = new com/truecaller/engagementrewards/EngagementRewardState;
    int i = 1;
    localEngagementRewardState.<init>("SHOWN", i);
    SHOWN = localEngagementRewardState;
    arrayOfEngagementRewardState[i] = localEngagementRewardState;
    localEngagementRewardState = new com/truecaller/engagementrewards/EngagementRewardState;
    i = 2;
    localEngagementRewardState.<init>("PENDING", i);
    PENDING = localEngagementRewardState;
    arrayOfEngagementRewardState[i] = localEngagementRewardState;
    localEngagementRewardState = new com/truecaller/engagementrewards/EngagementRewardState;
    i = 3;
    localEngagementRewardState.<init>("COMPLETED", i);
    COMPLETED = localEngagementRewardState;
    arrayOfEngagementRewardState[i] = localEngagementRewardState;
    localEngagementRewardState = new com/truecaller/engagementrewards/EngagementRewardState;
    i = 4;
    localEngagementRewardState.<init>("REDEEMED", i);
    REDEEMED = localEngagementRewardState;
    arrayOfEngagementRewardState[i] = localEngagementRewardState;
    localEngagementRewardState = new com/truecaller/engagementrewards/EngagementRewardState;
    i = 5;
    localEngagementRewardState.<init>("COUPON_COPIED", i);
    COUPON_COPIED = localEngagementRewardState;
    arrayOfEngagementRewardState[i] = localEngagementRewardState;
    $VALUES = arrayOfEngagementRewardState;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.EngagementRewardState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */