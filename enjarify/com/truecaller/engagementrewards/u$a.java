package com.truecaller.engagementrewards;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;

final class u$a
  implements Executor
{
  private final Handler a;
  
  u$a()
  {
    Handler localHandler = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    localHandler.<init>(localLooper);
    a = localHandler;
  }
  
  public final void execute(Runnable paramRunnable)
  {
    a.post(paramRunnable);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.u.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */