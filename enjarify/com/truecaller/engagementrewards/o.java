package com.truecaller.engagementrewards;

import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d
{
  private final m a;
  private final Provider b;
  
  private o(m paramm, Provider paramProvider)
  {
    a = paramm;
    b = paramProvider;
  }
  
  public static o a(m paramm, Provider paramProvider)
  {
    o localo = new com/truecaller/engagementrewards/o;
    localo.<init>(paramm, paramProvider);
    return localo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */