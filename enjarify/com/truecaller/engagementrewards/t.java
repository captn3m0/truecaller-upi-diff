package com.truecaller.engagementrewards;

import android.content.Context;
import android.content.SharedPreferences;
import c.g.b.k;
import com.truecaller.old.data.access.Settings;
import com.truecaller.utils.a.a;

public final class t
  extends a
  implements s
{
  private final int b = 1;
  private final String c = "EngagementRewards";
  
  public t(SharedPreferences paramSharedPreferences)
  {
    super(paramSharedPreferences);
  }
  
  public final int a()
  {
    return b;
  }
  
  public final void a(int paramInt, Context paramContext)
  {
    k.b(paramContext, "context");
    paramContext = new String[1];
    String str = String.valueOf(paramInt);
    str = "migrateFrom:: ".concat(str);
    paramContext[0] = str;
  }
  
  public final String b()
  {
    return c;
  }
  
  public final String c()
  {
    return Settings.b("qa_engagement_reward_state");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */