package com.truecaller.engagementrewards;

import android.content.Context;
import com.google.android.libraries.nbu.engagementrewards.api.EngagementRewardsClient;
import com.google.android.libraries.nbu.engagementrewards.api.EngagementRewardsConstants.RewardsEnvironment;
import com.google.android.libraries.nbu.engagementrewards.api.impl.EngagementRewardsClientFactory;
import com.google.android.libraries.nbu.engagementrewards.external.EngagementRewardsHelper;
import com.google.android.libraries.nbu.engagementrewards.models.AndroidClient;
import com.google.android.libraries.nbu.engagementrewards.models.AndroidClient.Builder;
import com.google.android.libraries.nbu.engagementrewards.models.ClientInfo;
import com.google.android.libraries.nbu.engagementrewards.models.ClientInfo.Builder;
import com.google.android.libraries.nbu.engagementrewards.models.EngagementRewardsConfig;
import com.google.android.libraries.nbu.engagementrewards.models.EngagementRewardsConfig.Builder;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.truecaller.common.g.a;
import com.truecaller.old.data.access.Settings;
import com.truecaller.util.al;
import java.util.Locale;
import java.util.UUID;

public final class j
{
  private static EngagementRewardsClient a;
  private static final EngagementRewardsConstants.RewardsEnvironment b = EngagementRewardsConstants.RewardsEnvironment.FAKE_ENVIRONMENT;
  
  public static EngagementRewardsClient a(Context paramContext, al paramal, a parama)
  {
    Object localObject = a;
    if (localObject == null)
    {
      boolean bool = paramal.e();
      if (bool)
      {
        paramal = Settings.a("qaEngagementRewardEnv", "");
        localObject = EngagementRewardsConstants.RewardsEnvironment.STAGING_ENVIRONMENT.name();
        bool = paramal.equalsIgnoreCase((String)localObject);
        if (bool)
        {
          paramal = EngagementRewardsConstants.RewardsEnvironment.STAGING_ENVIRONMENT;
          break label59;
        }
      }
      paramal = EngagementRewardsConstants.RewardsEnvironment.PROD_ENVIRONMENT;
      label59:
      int i = 1;
      localObject = new String[i];
      String str1 = String.valueOf(paramal);
      String str2 = "getClient:: Env: ".concat(str1);
      localObject[0] = str2;
      paramContext = paramContext.getApplicationContext();
      localObject = EngagementRewardsConfig.builder().setApiKey("AIzaSyCd6tpLEKJi-5w6SDpTpzj6UTZpS47j7fw");
      ListeningExecutorService localListeningExecutorService = u.a();
      localObject = ((EngagementRewardsConfig.Builder)localObject).setBackgroundExecutors(localListeningExecutorService);
      paramContext = ((EngagementRewardsConfig.Builder)localObject).setApplicationContext(paramContext).setRewardsEnvironment(paramal).build();
      paramContext = EngagementRewardsHelper.getInstance().getEngagementRewardsFactory(paramContext);
      paramal = a(parama);
      paramContext = paramContext.getInstance(paramal);
      a = paramContext;
    }
    return a;
  }
  
  public static ClientInfo a(a parama)
  {
    long l1 = UUID.randomUUID().hashCode();
    long l2 = parama.a("profileUserId", l1);
    parama = new String[1];
    String str1 = String.valueOf(l2);
    String str2 = "getClientInfo:: Client id: ".concat(str1);
    parama[0] = str2;
    parama = AndroidClient.builder().setClientVersionCode(1L).setClientId(l2).build();
    ClientInfo.Builder localBuilder = ClientInfo.builder().setSponsorId("TRUECALLER");
    String str3 = Locale.getDefault().toString();
    return localBuilder.setLocale(str3).setAndroidClient(parama).build();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */