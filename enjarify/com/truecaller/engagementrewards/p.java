package com.truecaller.engagementrewards;

import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d
{
  private final m a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  private p(m paramm, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramm;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static p a(m paramm, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    p localp = new com/truecaller/engagementrewards/p;
    localp.<init>(paramm, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localp;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */