package com.truecaller.engagementrewards;

import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private h(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static h a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    h localh = new com/truecaller/engagementrewards/h;
    localh.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */