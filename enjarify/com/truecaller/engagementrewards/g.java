package com.truecaller.engagementrewards;

import android.os.Bundle;
import c.x;
import com.google.android.libraries.nbu.engagementrewards.api.EngagementRewardsClient;
import com.google.android.libraries.nbu.engagementrewards.api.Event;
import com.google.android.libraries.nbu.engagementrewards.api.Parameter;
import com.google.android.libraries.nbu.engagementrewards.models.Promotion;
import com.google.common.util.concurrent.FutureCallback;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import dagger.a;

public final class g
{
  private final a a;
  private final k b;
  private final b c;
  
  public g(a parama, k paramk, b paramb)
  {
    a = parama;
    b = paramk;
    c = paramb;
  }
  
  public final void a(Event paramEvent, Promotion paramPromotion, String paramString)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    String str1 = Parameter.PROMOTION_NAME.name();
    String str2;
    if (paramPromotion != null)
    {
      str2 = paramPromotion.promotionCode();
      if (str2 != null) {}
    }
    else
    {
      str2 = "NO_AVAILABLE_PROMOTIONS";
    }
    localBundle.putString(str1, str2);
    str1 = Parameter.ACTION_NAME.name();
    if (paramPromotion != null) {
      paramPromotion = paramPromotion.actionType();
    } else {
      paramPromotion = null;
    }
    localBundle.putString(str1, paramPromotion);
    paramPromotion = Parameter.ACTION_SOURCE.name();
    localBundle.putString(paramPromotion, paramString);
    ((EngagementRewardsClient)a.get()).logEvent(paramEvent, localBundle);
  }
  
  public final void a(Promotion paramPromotion, String paramString)
  {
    c.g.b.k.b(paramPromotion, "promotion");
    Object localObject = Event.PROMOTION_SEEN;
    a((Event)localObject, paramPromotion, paramString);
    localObject = x.a;
    a("ErPromotionShown", paramPromotion, paramString);
  }
  
  public final void a(String paramString)
  {
    k localk = b;
    Object localObject = new com/truecaller/engagementrewards/g$a;
    ((g.a)localObject).<init>(this, paramString);
    localObject = (FutureCallback)localObject;
    localk.a((FutureCallback)localObject);
  }
  
  public final void a(String paramString1, Promotion paramPromotion, String paramString2)
  {
    b localb = c;
    Object localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>(paramString1);
    paramString1 = "PromoId";
    String str;
    if (paramPromotion != null)
    {
      str = paramPromotion.promotionCode();
      if (str != null) {}
    }
    else
    {
      str = "NOT_PRESENT";
    }
    paramString1 = ((e.a)localObject).a(paramString1, str);
    localObject = "PromoSource";
    if (paramString2 == null) {
      paramString2 = "NOT_PRESENT";
    }
    paramString1 = paramString1.a((String)localObject, paramString2);
    paramString2 = "ActionName";
    if (paramPromotion != null)
    {
      paramPromotion = paramPromotion.actionType();
      if (paramPromotion != null) {}
    }
    else
    {
      paramPromotion = "NOT_PRESENT";
    }
    paramString1 = paramString1.a(paramString2, paramPromotion).a();
    c.g.b.k.a(paramString1, "AnalyticsEvent.Builder(e…\n                .build()");
    localb.b(paramString1);
  }
  
  public final void b(Promotion paramPromotion, String paramString)
  {
    a("ErPurchaseDone", paramPromotion, paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */