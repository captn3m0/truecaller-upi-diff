package com.truecaller.engagementrewards;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import com.google.android.libraries.nbu.engagementrewards.api.EngagementRewardsClient;
import com.google.android.libraries.nbu.engagementrewards.external.EngagementRewardsHelper;
import com.google.android.libraries.nbu.engagementrewards.models.ClientInfo;
import com.google.android.libraries.nbu.engagementrewards.models.PhoneNumber;
import com.google.android.libraries.nbu.engagementrewards.models.PhoneNumber.Builder;
import com.google.android.libraries.nbu.engagementrewards.models.UserInfo;
import com.google.android.libraries.nbu.engagementrewards.models.UserInfo.Builder;
import com.google.c.a.g;
import com.google.c.a.m.a;
import com.google.common.base.Function;
import com.google.common.util.concurrent.AsyncFunction;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.log.AssertionUtil;
import java.util.UUID;
import java.util.concurrent.Executor;

public final class k
{
  private dagger.a a;
  private final com.truecaller.common.g.a b;
  private final com.google.c.a.k c;
  private final Context d;
  private final dagger.a e;
  private final e f;
  
  public k(dagger.a parama1, com.truecaller.common.g.a parama, com.google.c.a.k paramk, Context paramContext, dagger.a parama2, e parame)
  {
    a = parama1;
    b = parama;
    c = paramk;
    d = paramContext;
    e = parama2;
    f = parame;
  }
  
  public static String c()
  {
    return UUID.randomUUID().toString();
  }
  
  private ListenableFuture d()
  {
    Object localObject1 = b.a("profileNumber");
    Object localObject2 = b;
    Object localObject3 = "profileCountryIso";
    localObject2 = ((com.truecaller.common.g.a)localObject2).a((String)localObject3);
    try
    {
      localObject3 = c;
      localObject1 = ((com.google.c.a.k)localObject3).a((CharSequence)localObject1, (String)localObject2);
      int i = b;
      long l = d;
      localObject1 = PhoneNumber.builder();
      localObject1 = ((PhoneNumber.Builder)localObject1).setNationalNumber(l);
      localObject1 = ((PhoneNumber.Builder)localObject1).setCountryCode(i);
      localObject1 = ((PhoneNumber.Builder)localObject1).build();
    }
    catch (g localg)
    {
      AssertionUtil.reportThrowableButNeverCrash(localg);
      localObject1 = null;
    }
    localObject1 = UserInfo.builder().setPhoneNumber((PhoneNumber)localObject1).build();
    localObject2 = ((EngagementRewardsClient)a.get()).isUserPreviouslyAuthenticated();
    localObject3 = new com/truecaller/engagementrewards/-$$Lambda$k$UGImljRilGhGVzbf-QuS1LZMn0M;
    ((-..Lambda.k.UGImljRilGhGVzbf-QuS1LZMn0M)localObject3).<init>(this, (UserInfo)localObject1);
    localObject1 = u.a();
    return Futures.transformAsync((ListenableFuture)localObject2, (AsyncFunction)localObject3, (Executor)localObject1);
  }
  
  public final ListenableFuture a()
  {
    return ((EngagementRewardsClient)a.get()).isUserPreviouslyAuthenticated();
  }
  
  public final ListenableFuture a(Account paramAccount)
  {
    EngagementRewardsClient localEngagementRewardsClient = (EngagementRewardsClient)a.get();
    EngagementRewardsHelper localEngagementRewardsHelper = EngagementRewardsHelper.getInstance();
    UserInfo localUserInfo = UserInfo.builder().build();
    ClientInfo localClientInfo = (ClientInfo)e.get();
    paramAccount = localEngagementRewardsHelper.getRegisterConfig(paramAccount, localUserInfo, localClientInfo);
    boolean bool = f.as().a() ^ true;
    return localEngagementRewardsClient.register(paramAccount, bool);
  }
  
  public final ListenableFuture a(String paramString1, String paramString2)
  {
    Object localObject1 = new String[1];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("getRegisteredPromotionsAndRedeem() called with: actionType = [");
    ((StringBuilder)localObject2).append(paramString1);
    ((StringBuilder)localObject2).append("], requestId = [");
    ((StringBuilder)localObject2).append(paramString2);
    ((StringBuilder)localObject2).append("]");
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    localObject1 = d();
    localObject2 = new com/truecaller/engagementrewards/-$$Lambda$k$xwaR9wFm1_MRp90jKyy77kxLpyY;
    ((-..Lambda.k.xwaR9wFm1_MRp90jKyy77kxLpyY)localObject2).<init>(this, paramString1);
    paramString1 = u.a();
    paramString1 = Futures.transform((ListenableFuture)localObject1, (Function)localObject2, paramString1);
    localObject1 = new com/truecaller/engagementrewards/-$$Lambda$k$YQJ-pEi7ECgrU8HB0Cd7BwMhjfY;
    ((-..Lambda.k.YQJ-pEi7ECgrU8HB0Cd7BwMhjfY)localObject1).<init>(this, paramString2);
    paramString2 = u.a();
    return Futures.transformAsync(paramString1, (AsyncFunction)localObject1, paramString2);
  }
  
  public final void a(Activity paramActivity)
  {
    ((EngagementRewardsClient)a.get()).launchAuthenticationFlow(paramActivity, 333);
  }
  
  public final void a(FutureCallback paramFutureCallback)
  {
    ListenableFuture localListenableFuture = d();
    Executor localExecutor = u.b();
    Futures.addCallback(localListenableFuture, paramFutureCallback, localExecutor);
  }
  
  public final void b()
  {
    ((EngagementRewardsClient)a.get()).unregister();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */