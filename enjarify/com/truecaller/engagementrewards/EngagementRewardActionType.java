package com.truecaller.engagementrewards;

public enum EngagementRewardActionType
{
  private final String persistenceKeyPrefix;
  
  static
  {
    EngagementRewardActionType[] arrayOfEngagementRewardActionType = new EngagementRewardActionType[1];
    EngagementRewardActionType localEngagementRewardActionType = new com/truecaller/engagementrewards/EngagementRewardActionType;
    localEngagementRewardActionType.<init>("BUY_PREMIUM_ANNUAL", 0, "keyActionTypeBuyAnnualSubs");
    BUY_PREMIUM_ANNUAL = localEngagementRewardActionType;
    arrayOfEngagementRewardActionType[0] = localEngagementRewardActionType;
    $VALUES = arrayOfEngagementRewardActionType;
  }
  
  private EngagementRewardActionType(String paramString1)
  {
    persistenceKeyPrefix = paramString1;
  }
  
  public final String getPersistenceKeyPrefix()
  {
    return persistenceKeyPrefix;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.EngagementRewardActionType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */