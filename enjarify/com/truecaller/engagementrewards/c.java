package com.truecaller.engagementrewards;

import c.d.f;
import c.g.b.k;
import c.l;
import com.truecaller.featuretoggles.b;
import com.truecaller.old.data.access.Settings;
import com.truecaller.util.al;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class c
{
  final com.truecaller.premium.a.d a;
  public final s b;
  private final com.truecaller.utils.d c;
  private final com.truecaller.featuretoggles.e d;
  private final com.truecaller.abtest.c e;
  private final al f;
  private final f g;
  
  public c(com.truecaller.premium.a.d paramd, s params, com.truecaller.utils.d paramd1, com.truecaller.featuretoggles.e parame, com.truecaller.abtest.c paramc, al paramal, f paramf)
  {
    a = paramd;
    b = params;
    c = paramd1;
    d = parame;
    e = paramc;
    f = paramal;
    g = paramf;
  }
  
  private final boolean d()
  {
    al localal = f;
    boolean bool = localal.e();
    return bool;
  }
  
  public final EngagementRewardState a(EngagementRewardActionType paramEngagementRewardActionType)
  {
    k.b(paramEngagementRewardActionType, "actionType");
    Object localObject1 = b.c();
    int i = 0;
    int j = 1;
    Object localObject2;
    if (localObject1 != null)
    {
      k.a(localObject1, "it");
      localObject2 = localObject1;
      localObject2 = (CharSequence)localObject1;
      int k = ((CharSequence)localObject2).length();
      if (k > 0)
      {
        k = 1;
      }
      else
      {
        k = 0;
        localObject2 = null;
      }
      if (k == 0)
      {
        bool2 = false;
        localObject1 = null;
      }
      if (localObject1 != null)
      {
        localObject2 = "it";
        k.a(localObject1, (String)localObject2);
        localObject1 = EngagementRewardState.valueOf((String)localObject1);
        break label106;
      }
    }
    boolean bool2 = false;
    localObject1 = null;
    label106:
    if (localObject1 != null)
    {
      localObject2 = EngagementRewardState.NONE;
      if (localObject1 != localObject2)
      {
        boolean bool1 = d();
        if (bool1) {
          i = 1;
        }
      }
    }
    if (i == 0)
    {
      bool2 = false;
      localObject1 = null;
    }
    if (localObject1 == null)
    {
      localObject1 = b;
      paramEngagementRewardActionType = e.a(paramEngagementRewardActionType);
      paramEngagementRewardActionType = ((s)localObject1).a(paramEngagementRewardActionType);
      if (paramEngagementRewardActionType != null)
      {
        k.a(paramEngagementRewardActionType, "it");
        localObject1 = paramEngagementRewardActionType;
        localObject1 = (CharSequence)paramEngagementRewardActionType;
        bool2 = c.n.m.a((CharSequence)localObject1) ^ j;
        if (!bool2) {
          paramEngagementRewardActionType = null;
        }
        if (paramEngagementRewardActionType != null)
        {
          k.a(paramEngagementRewardActionType, "it");
          localObject1 = EngagementRewardState.valueOf(paramEngagementRewardActionType);
          if (localObject1 != null) {
            return localObject1;
          }
        }
      }
      localObject1 = EngagementRewardState.NONE;
    }
    return (EngagementRewardState)localObject1;
  }
  
  public final void a()
  {
    Object localObject1 = "onPremiumPurchaseComplete:: ";
    new String[1][0] = localObject1;
    boolean bool = b();
    if (!bool) {
      return;
    }
    localObject1 = (ag)bg.a;
    f localf = g;
    Object localObject2 = new com/truecaller/engagementrewards/c$a;
    ((c.a)localObject2).<init>(this, null);
    localObject2 = (c.g.a.m)localObject2;
    kotlinx.coroutines.e.b((ag)localObject1, localf, (c.g.a.m)localObject2, 2);
  }
  
  public final void a(EngagementRewardActionType paramEngagementRewardActionType, EngagementRewardState paramEngagementRewardState)
  {
    k.b(paramEngagementRewardActionType, "actionType");
    k.b(paramEngagementRewardState, "state");
    Object localObject1 = new String[1];
    String str = String.valueOf(paramEngagementRewardState);
    Object localObject2 = "setRewardState:: State: ".concat(str);
    str = null;
    localObject1[0] = localObject2;
    localObject1 = a(paramEngagementRewardActionType);
    localObject2 = d.a;
    int i = ((EngagementRewardState)localObject1).ordinal();
    i = localObject2[i];
    switch (i)
    {
    default: 
      paramEngagementRewardActionType = new c/l;
      paramEngagementRewardActionType.<init>();
      throw paramEngagementRewardActionType;
    case 6: 
      localObject1 = EngagementRewardState.NONE;
      break;
    case 5: 
      localObject1 = EngagementRewardState.COUPON_COPIED;
      break;
    case 4: 
      localObject1 = EngagementRewardState.REDEEMED;
      break;
    case 3: 
      localObject1 = EngagementRewardState.COMPLETED;
      break;
    case 2: 
      localObject1 = EngagementRewardState.PENDING;
      break;
    case 1: 
      localObject1 = EngagementRewardState.SHOWN;
    }
    localObject2 = EngagementRewardState.NONE;
    if ((paramEngagementRewardState == localObject2) || (paramEngagementRewardState == localObject1))
    {
      localObject1 = b;
      paramEngagementRewardActionType = e.a(paramEngagementRewardActionType);
      paramEngagementRewardState = paramEngagementRewardState.name();
      ((s)localObject1).a(paramEngagementRewardActionType, paramEngagementRewardState);
    }
  }
  
  public final boolean a(String paramString)
  {
    String str = "truecaller_yearly_subscription";
    boolean bool1 = true;
    boolean bool2 = c.n.m.a(str, paramString, bool1);
    if (!bool2)
    {
      bool2 = d();
      str = null;
      if (bool2)
      {
        paramString = "qaEnableRewardForMonthlySubs";
        bool2 = Settings.b(paramString, false);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return bool1;
  }
  
  public final boolean b()
  {
    Object localObject = f;
    boolean bool1 = ((al)localObject).a();
    if (bool1)
    {
      localObject = c;
      int i = ((com.truecaller.utils.d)localObject).h();
      int j = 16;
      if (i > j)
      {
        localObject = d.aq();
        boolean bool2 = ((b)localObject).a();
        if (bool2) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final String c()
  {
    return e.a("erPromoImage_19751");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */