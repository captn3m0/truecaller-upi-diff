package com.truecaller.engagementrewards.ui;

import android.content.Context;
import android.support.v4.app.j;
import c.a.ag;
import c.n;
import c.t;
import java.util.Map;

public final class g
{
  public final Map a;
  final Context b;
  final j c;
  private final d d;
  
  public g(Context paramContext, j paramj, d paramd)
  {
    b = paramContext;
    c = paramj;
    d = paramd;
    paramContext = new n[7];
    paramd = new com/truecaller/engagementrewards/ui/g$a;
    paramd.<init>(this);
    paramj = t.a("Redeem Success", paramd);
    paramContext[0] = paramj;
    paramd = new com/truecaller/engagementrewards/ui/g$b;
    paramd.<init>(this);
    paramj = t.a("Redeem Pending", paramd);
    paramContext[1] = paramj;
    paramd = new com/truecaller/engagementrewards/ui/g$c;
    paramd.<init>(this);
    paramj = t.a("Redeem promo not available", paramd);
    paramContext[2] = paramj;
    paramd = new com/truecaller/engagementrewards/ui/g$d;
    paramd.<init>(this);
    paramj = t.a("Redeem failed with a retryable error", paramd);
    paramContext[3] = paramj;
    paramd = new com/truecaller/engagementrewards/ui/g$e;
    paramd.<init>(this);
    paramj = t.a("Redeem failed with an auth error", paramd);
    paramContext[4] = paramj;
    paramd = new com/truecaller/engagementrewards/ui/g$f;
    paramd.<init>(this);
    paramj = t.a("Redeem failed with a non retryable error", paramd);
    paramContext[5] = paramj;
    paramd = new com/truecaller/engagementrewards/ui/g$g;
    paramd.<init>(this);
    paramj = t.a("Free trial prompt before purchase", paramd);
    paramContext[6] = paramj;
    paramContext = ag.a(paramContext);
    a = paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.ui.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */