package com.truecaller.engagementrewards.ui;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import c.x;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.nbu.engagementrewards.api.Event;
import com.google.android.libraries.nbu.engagementrewards.models.Promotion;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.Futures.FutureCombiner;
import com.google.common.util.concurrent.ListenableFuture;
import com.truecaller.R.id;
import com.truecaller.TrueApp;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.engagementrewards.g;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;

public final class ClaimEngagementRewardsActivity
  extends AppCompatActivity
  implements DialogInterface.OnDismissListener, c
{
  public static final ClaimEngagementRewardsActivity.a c;
  public d a;
  public a b;
  private HashMap d;
  
  static
  {
    ClaimEngagementRewardsActivity.a locala = new com/truecaller/engagementrewards/ui/ClaimEngagementRewardsActivity$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final a a()
  {
    a locala = b;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    return locala;
  }
  
  public final void a(c.g.a.a parama)
  {
    c.g.b.k.b(parama, "onRetry");
    Object localObject = a;
    if (localObject == null)
    {
      localObject = "engagementRewardPrompter";
      c.g.b.k.a((String)localObject);
    }
    localObject = this;
    d.a((Context)this, parama);
  }
  
  public final void a(com.truecaller.engagementrewards.k paramk)
  {
    c.g.b.k.b(paramk, "engagementRewardsManager");
    Object localObject = this;
    localObject = (Activity)this;
    paramk.a((Activity)localObject);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "text");
    int i = R.id.rewardText;
    TextView localTextView = (TextView)a(i);
    c.g.b.k.a(localTextView, "rewardText");
    t.a((View)localTextView);
    i = R.id.rewardText;
    localTextView = (TextView)a(i);
    c.g.b.k.a(localTextView, "rewardText");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.ctaGetReward;
    Object localObject = (Button)a(i);
    c.g.b.k.a(localObject, "ctaGetReward");
    ((Button)localObject).setEnabled(paramBoolean);
    i = R.id.gotItButton;
    localObject = (Button)a(i);
    c.g.b.k.a(localObject, "gotItButton");
    ((Button)localObject).setEnabled(paramBoolean);
    i = R.id.progressBar;
    localObject = (ProgressBar)a(i);
    c.g.b.k.a(localObject, "progressBar");
    localObject = (View)localObject;
    paramBoolean ^= true;
    t.a((View)localObject, paramBoolean);
  }
  
  public final void b()
  {
    int i = R.id.rewardText;
    Object localObject = (TextView)a(i);
    c.g.b.k.a(localObject, "rewardText");
    t.b((View)localObject);
    i = R.id.ctaGetReward;
    localObject = (Button)a(i);
    c.g.b.k.a(localObject, "ctaGetReward");
    t.b((View)localObject);
    i = R.id.dialogText;
    localObject = (TextView)a(i);
    c.g.b.k.a(localObject, "dialogText");
    CharSequence localCharSequence = (CharSequence)getString(2131887991);
    ((TextView)localObject).setText(localCharSequence);
    i = R.id.dialogTitle;
    localObject = (TextView)a(i);
    c.g.b.k.a(localObject, "dialogTitle");
    localCharSequence = (CharSequence)getString(2131887992);
    ((TextView)localObject).setText(localCharSequence);
  }
  
  public final void b(c.g.a.a parama)
  {
    c.g.b.k.b(parama, "onRetry");
    Object localObject = a;
    if (localObject == null)
    {
      localObject = "engagementRewardPrompter";
      c.g.b.k.a((String)localObject);
    }
    localObject = this;
    d.b((Context)this, parama);
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "message");
    Object localObject = a;
    if (localObject == null)
    {
      localObject = "engagementRewardPrompter";
      c.g.b.k.a((String)localObject);
    }
    localObject = this;
    d.a((Context)this, paramString);
  }
  
  public final void c()
  {
    Object localObject = a;
    if (localObject == null)
    {
      localObject = "engagementRewardPrompter";
      c.g.b.k.a((String)localObject);
    }
    localObject = getSupportFragmentManager();
    c.g.b.k.a(localObject, "supportFragmentManager");
    d.a((j)localObject);
  }
  
  public final void d()
  {
    Object localObject = a;
    if (localObject == null)
    {
      localObject = "engagementRewardPrompter";
      c.g.b.k.a((String)localObject);
    }
    localObject = this;
    d.a((Context)this);
  }
  
  public final void e()
  {
    Object localObject = a;
    if (localObject == null)
    {
      localObject = "engagementRewardPrompter";
      c.g.b.k.a((String)localObject);
    }
    localObject = this;
    d.b((Context)this);
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    Object localObject1 = b;
    if (localObject1 == null)
    {
      String str1 = "presenter";
      c.g.b.k.a(str1);
    }
    int i = 333;
    if (paramInt1 != i)
    {
      new String[1][0] = "onActivityResult:: Not handling";
      return;
    }
    Object localObject2 = (c)b;
    i = 1;
    if (localObject2 != null) {
      ((c)localObject2).a(i);
    }
    if (paramInt2 == 0)
    {
      d.b();
      localObject2 = g;
      localObject3 = c;
      paramIntent = a;
      localObject1 = Event.GOOGLE_ACCOUNT_AUTHENTICATION_FLOW_FAILED;
      ((g)localObject2).a((Event)localObject1, (Promotion)localObject3, paramIntent);
      localObject1 = x.a;
      ((g)localObject2).a("ErGoogleAuthFailed", (Promotion)localObject3, paramIntent);
      return;
    }
    localObject2 = GoogleSignIn.a(paramIntent);
    Object localObject3 = "GoogleSignIn.getSignedInAccountFromIntent(data)";
    c.g.b.k.a(localObject2, (String)localObject3);
    localObject2 = a.a((Task)localObject2);
    if (localObject2 == null)
    {
      new String[1][0] = "onActivityResult:: No signed in account";
      return;
    }
    localObject3 = new String[i];
    paramIntent = new java/lang/StringBuilder;
    paramIntent.<init>("onActivityResult:: Selected account: ");
    String str2 = name;
    paramIntent.append(str2);
    paramIntent = paramIntent.toString();
    localObject3[0] = paramIntent;
    localObject3 = g;
    paramIntent = c;
    String str3 = a;
    Object localObject4 = Event.GOOGLE_ACCOUNT_AUTHENTICATION_FLOW_COMPLETED;
    ((g)localObject3).a((Event)localObject4, paramIntent, str3);
    localObject4 = x.a;
    ((g)localObject3).a("ErGoogleAuthCompleted", paramIntent, str3);
    localObject3 = new ListenableFuture[i];
    localObject2 = d.a((Account)localObject2);
    localObject3[0] = localObject2;
    localObject2 = Futures.whenAllComplete((ListenableFuture[])localObject3);
    localObject3 = new com/truecaller/engagementrewards/ui/a$d;
    ((a.d)localObject3).<init>((a)localObject1);
    localObject3 = (Runnable)localObject3;
    paramIntent = com.truecaller.engagementrewards.u.b();
    ((Futures.FutureCombiner)localObject2).run((Runnable)localObject3, paramIntent);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    com.truecaller.utils.extensions.a.a(this);
    paramBundle = getTheme();
    Object localObject1 = null;
    int i = 2131952145;
    paramBundle.applyStyle(i, false);
    paramBundle = TrueApp.x();
    if (paramBundle != null)
    {
      ((bk)paramBundle).a().a(this);
      int j = 2131558582;
      setContentView(j);
      paramBundle = b;
      if (paramBundle == null)
      {
        localObject2 = "presenter";
        c.g.b.k.a((String)localObject2);
      }
      Object localObject2 = getIntent();
      String str = "ARG_SOURCE";
      localObject2 = ((Intent)localObject2).getStringExtra(str);
      a = ((String)localObject2);
      paramBundle = b;
      if (paramBundle == null)
      {
        localObject2 = "presenter";
        c.g.b.k.a((String)localObject2);
      }
      localObject2 = this;
      localObject2 = (c)this;
      paramBundle.a((c)localObject2);
      j = R.id.ctaGetReward;
      paramBundle = (Button)a(j);
      localObject2 = new com/truecaller/engagementrewards/ui/ClaimEngagementRewardsActivity$b;
      ((ClaimEngagementRewardsActivity.b)localObject2).<init>(this);
      localObject2 = (View.OnClickListener)localObject2;
      paramBundle.setOnClickListener((View.OnClickListener)localObject2);
      a(false);
      j = R.id.gotItButton;
      paramBundle = (Button)a(j);
      localObject1 = new com/truecaller/engagementrewards/ui/ClaimEngagementRewardsActivity$c;
      ((ClaimEngagementRewardsActivity.c)localObject1).<init>(this);
      localObject1 = (View.OnClickListener)localObject1;
      paramBundle.setOnClickListener((View.OnClickListener)localObject1);
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramBundle;
  }
  
  public final void onDismiss(DialogInterface paramDialogInterface)
  {
    paramDialogInterface = b;
    if (paramDialogInterface == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    String str = "onDialogDismiss:: ";
    new String[1][0] = str;
    paramDialogInterface = (c)b;
    if (paramDialogInterface != null)
    {
      paramDialogInterface.finish();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.ui.ClaimEngagementRewardsActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */