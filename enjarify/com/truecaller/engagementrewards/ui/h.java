package com.truecaller.engagementrewards.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.R.id;
import com.truecaller.TrueApp;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.engagementrewards.EngagementRewardActionType;
import com.truecaller.engagementrewards.c;
import com.truecaller.engagementrewards.e;
import com.truecaller.engagementrewards.s;
import java.util.HashMap;

public final class h
  extends AppCompatDialogFragment
{
  public ClipboardManager a;
  public c b;
  private DialogInterface.OnDismissListener c;
  private HashMap d;
  
  private View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final c a()
  {
    c localc = b;
    if (localc == null)
    {
      String str = "engagementRewardUtil";
      k.a(str);
    }
    return localc;
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    boolean bool = paramContext instanceof DialogInterface.OnDismissListener;
    if (bool)
    {
      paramContext = (DialogInterface.OnDismissListener)paramContext;
      c = paramContext;
    }
  }
  
  public final void onCancel(DialogInterface paramDialogInterface)
  {
    super.onCancel(paramDialogInterface);
    DialogInterface.OnDismissListener localOnDismissListener = c;
    if (localOnDismissListener != null)
    {
      localOnDismissListener.onDismiss(paramDialogInterface);
      return;
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getDialog();
    if (paramBundle != null)
    {
      paramBundle = paramBundle.getWindow();
      if (paramBundle != null)
      {
        i = 17170445;
        paramBundle.setBackgroundDrawableResource(i);
      }
    }
    int i = 2131952158;
    setStyle(0, i);
    paramBundle = TrueApp.x();
    if (paramBundle != null)
    {
      ((bk)paramBundle).a().a(this);
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558590, paramViewGroup, false);
  }
  
  public final void onDetach()
  {
    super.onDetach();
    c = null;
  }
  
  public final void onDismiss(DialogInterface paramDialogInterface)
  {
    super.onDismiss(paramDialogInterface);
    DialogInterface.OnDismissListener localOnDismissListener = c;
    if (localOnDismissListener != null)
    {
      localOnDismissListener.onDismiss(paramDialogInterface);
      return;
    }
  }
  
  public final void onStart()
  {
    super.onStart();
    Object localObject = getDialog();
    if (localObject != null)
    {
      localObject = ((Dialog)localObject).getWindow();
      if (localObject != null)
      {
        ((Window)localObject).setLayout(-1, -2);
        return;
      }
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject = "view";
    k.b(paramView, (String)localObject);
    super.onViewCreated(paramView, paramBundle);
    paramView = new android/util/DisplayMetrics;
    paramView.<init>();
    paramBundle = getContext();
    if (paramBundle != null)
    {
      paramBundle = ((Activity)paramBundle).getWindowManager();
      k.a(paramBundle, "(context as Activity).windowManager");
      paramBundle = paramBundle.getDefaultDisplay();
      paramBundle.getRealMetrics(paramView);
      double d1 = widthPixels;
      double d2 = 0.6D;
      Double.isNaN(d1);
      d1 *= d2;
      int i = R.id.container;
      localObject = (LinearLayout)a(i);
      String str = "container";
      k.a(localObject, str);
      int j = (int)d1;
      ((LinearLayout)localObject).setMinimumWidth(j);
      paramView = b;
      if (paramView == null)
      {
        paramBundle = "engagementRewardUtil";
        k.a(paramBundle);
      }
      paramBundle = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
      localObject = "actionType";
      k.b(paramBundle, (String)localObject);
      paramView = b;
      paramBundle = e.b(paramBundle);
      paramView = paramView.a(paramBundle);
      int k;
      if (paramView != null)
      {
        paramBundle = new com/truecaller/engagementrewards/ui/f;
        localObject = "it";
        k.a(paramView, (String)localObject);
        paramBundle.<init>(paramView);
      }
      else
      {
        k = 0;
        paramBundle = null;
      }
      if (paramBundle != null)
      {
        paramView = a;
        k = R.id.couponCode;
        paramBundle = (TextView)a(k);
        k.a(paramBundle, "couponCode");
        localObject = paramView;
        localObject = (CharSequence)paramView;
        paramBundle.setText((CharSequence)localObject);
        k = R.id.title;
        paramBundle = (TextView)a(k);
        k.a(paramBundle, "title");
        localObject = (CharSequence)getString(2131887971);
        paramBundle.setText((CharSequence)localObject);
        k = R.id.subtitle;
        paramBundle = (TextView)a(k);
        k.a(paramBundle, "subtitle");
        localObject = (CharSequence)getString(2131887970);
        paramBundle.setText((CharSequence)localObject);
        k = R.id.ctaButton;
        paramBundle = (Button)a(k);
        localObject = new com/truecaller/engagementrewards/ui/h$a;
        ((h.a)localObject).<init>(paramView, this);
        localObject = (View.OnClickListener)localObject;
        paramBundle.setOnClickListener((View.OnClickListener)localObject);
        return;
      }
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type android.app.Activity");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.ui.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */