package com.truecaller.engagementrewards.ui;

import android.content.Context;
import android.content.Intent;
import c.g.b.k;

public final class ClaimEngagementRewardsActivity$a
{
  public static Intent a(Context paramContext, String paramString)
  {
    k.b(paramContext, "context");
    k.b(paramString, "launchSource");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, ClaimEngagementRewardsActivity.class);
    paramContext = localIntent.addFlags(268435456).putExtra("ARG_SOURCE", paramString);
    k.a(paramContext, "Intent(context, ClaimEng…NCH_SOURCE, launchSource)");
    return paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.ui.ClaimEngagementRewardsActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */