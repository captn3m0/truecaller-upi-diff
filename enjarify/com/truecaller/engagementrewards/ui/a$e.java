package com.truecaller.engagementrewards.ui;

import c.g.b.k;
import c.x;
import com.google.android.libraries.nbu.engagementrewards.api.RetryableException;
import com.google.android.libraries.nbu.engagementrewards.api.impl.authentication.PermanentAuthTokenException;
import com.google.common.util.concurrent.FutureCallback;
import com.truecaller.engagementrewards.EngagementRewardActionType;
import com.truecaller.engagementrewards.EngagementRewardState;

public final class a$e
  implements FutureCallback
{
  a$e(a parama) {}
  
  public final void onFailure(Throwable paramThrowable)
  {
    Object localObject1 = "t";
    k.b(paramThrowable, (String)localObject1);
    boolean bool1 = true;
    Object localObject2 = new String[bool1];
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("onFailure:: ");
    Object localObject4 = paramThrowable.getMessage();
    ((StringBuilder)localObject3).append((String)localObject4);
    localObject3 = ((StringBuilder)localObject3).toString();
    localObject4 = null;
    localObject2[0] = localObject3;
    localObject2 = a;
    k.b(paramThrowable, "t");
    localObject3 = (c)b;
    if (localObject3 != null) {
      ((c)localObject3).a(bool1);
    }
    localObject1 = paramThrowable.getMessage();
    if (localObject1 == null) {
      localObject1 = "";
    }
    boolean bool2 = paramThrowable instanceof RetryableException;
    if (bool2)
    {
      paramThrowable = (c)b;
      if (paramThrowable != null)
      {
        localObject1 = new com/truecaller/engagementrewards/ui/a$b;
        ((a.b)localObject1).<init>((a)localObject2);
        localObject1 = (c.g.a.a)localObject1;
        paramThrowable.a((c.g.a.a)localObject1);
      }
      return;
    }
    boolean bool3 = paramThrowable instanceof PermanentAuthTokenException;
    if (bool3)
    {
      paramThrowable = (c)b;
      if (paramThrowable != null)
      {
        localObject1 = new com/truecaller/engagementrewards/ui/a$c;
        ((a.c)localObject1).<init>((a)localObject2);
        localObject1 = (c.g.a.a)localObject1;
        paramThrowable.b((c.g.a.a)localObject1);
      }
      return;
    }
    paramThrowable = e;
    localObject3 = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
    localObject4 = EngagementRewardState.NONE;
    paramThrowable.a((EngagementRewardActionType)localObject3, (EngagementRewardState)localObject4);
    paramThrowable = x.a;
    paramThrowable = (c)b;
    if (paramThrowable != null)
    {
      paramThrowable.b((String)localObject1);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.ui.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */