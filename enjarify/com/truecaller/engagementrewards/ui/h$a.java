package com.truecaller.engagementrewards.ui;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.view.View;
import android.view.View.OnClickListener;
import c.g.b.k;
import com.truecaller.engagementrewards.EngagementRewardActionType;
import com.truecaller.engagementrewards.EngagementRewardState;
import com.truecaller.engagementrewards.c;
import com.truecaller.engagementrewards.s;
import com.truecaller.utils.extensions.i;

final class h$a
  implements View.OnClickListener
{
  h$a(String paramString, h paramh) {}
  
  public final void onClick(View paramView)
  {
    paramView = b.a;
    if (paramView == null)
    {
      localObject1 = "clipboardManager";
      k.a((String)localObject1);
    }
    Object localObject1 = (CharSequence)"coupon";
    Object localObject2 = (CharSequence)a;
    localObject1 = ClipData.newPlainText((CharSequence)localObject1, (CharSequence)localObject2);
    paramView.setPrimaryClip((ClipData)localObject1);
    paramView = b.a();
    localObject1 = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
    localObject2 = EngagementRewardState.COUPON_COPIED;
    paramView.a((EngagementRewardActionType)localObject1, (EngagementRewardState)localObject2);
    paramView = b.a().b;
    paramView.d("keyStartUpDialogShownTime");
    localObject1 = "keyStartUpDialogShownCount";
    paramView.d((String)localObject1);
    paramView = b.getContext();
    if (paramView != null)
    {
      int i = 2131887973;
      localObject2 = null;
      int j = 6;
      i.a(paramView, i, null, 0, j);
    }
    b.dismiss();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.ui.h.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */