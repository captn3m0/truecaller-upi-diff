package com.truecaller.engagementrewards.ui;

import android.accounts.Account;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.nbu.engagementrewards.models.Promotion;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.truecaller.bb;
import com.truecaller.engagementrewards.EngagementRewardActionType;
import com.truecaller.engagementrewards.g;
import com.truecaller.engagementrewards.u;
import com.truecaller.utils.n;
import java.util.concurrent.Executor;

public final class a
  extends bb
{
  String a;
  Promotion c;
  final com.truecaller.engagementrewards.k d;
  final com.truecaller.engagementrewards.c e;
  final n f;
  final g g;
  
  public a(com.truecaller.engagementrewards.k paramk, com.truecaller.engagementrewards.c paramc, n paramn, g paramg)
  {
    d = paramk;
    e = paramc;
    f = paramn;
    g = paramg;
  }
  
  static Account a(Task paramTask)
  {
    Task localTask = null;
    Object localObject = ApiException.class;
    try
    {
      paramTask = paramTask.a((Class)localObject);
      paramTask = (GoogleSignInAccount)paramTask;
      if (paramTask != null)
      {
        paramTask = paramTask.a();
        localTask = paramTask;
      }
    }
    catch (ApiException paramTask)
    {
      int i = 1;
      localObject = new String[i];
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      String str = "signInResult:failed code=";
      localStringBuilder.<init>(str);
      int j = paramTask.a();
      localStringBuilder.append(j);
      paramTask = localStringBuilder.toString();
      localObject[0] = paramTask;
    }
    return localTask;
  }
  
  public final void a()
  {
    new String[1][0] = "tryAuthAndRedeem:: ";
    ListenableFuture localListenableFuture = d.a();
    Object localObject = new com/truecaller/engagementrewards/ui/a$f;
    ((a.f)localObject).<init>(this);
    localObject = (FutureCallback)localObject;
    Executor localExecutor = u.b();
    Futures.addCallback(localListenableFuture, (FutureCallback)localObject, localExecutor);
  }
  
  public final void a(c paramc)
  {
    c.g.b.k.b(paramc, "presenterView");
    super.a(paramc);
    Object localObject = e;
    boolean bool = ((com.truecaller.engagementrewards.c)localObject).b();
    if (!bool)
    {
      paramc.finish();
      return;
    }
    paramc = d;
    localObject = new com/truecaller/engagementrewards/ui/a$a;
    ((a.a)localObject).<init>(this);
    localObject = (FutureCallback)localObject;
    paramc.a((FutureCallback)localObject);
  }
  
  public final void b()
  {
    Object localObject1 = (c)b;
    if (localObject1 != null)
    {
      localObject2 = null;
      ((c)localObject1).a(false);
    }
    localObject1 = d;
    Object localObject2 = EngagementRewardActionType.BUY_PREMIUM_ANNUAL.name();
    Object localObject3 = com.truecaller.engagementrewards.k.c();
    localObject1 = ((com.truecaller.engagementrewards.k)localObject1).a((String)localObject2, (String)localObject3);
    localObject2 = new com/truecaller/engagementrewards/ui/a$e;
    ((a.e)localObject2).<init>(this);
    localObject2 = (FutureCallback)localObject2;
    localObject3 = u.b();
    Futures.addCallback((ListenableFuture)localObject1, (FutureCallback)localObject2, (Executor)localObject3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.ui.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */