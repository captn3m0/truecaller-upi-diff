package com.truecaller.engagementrewards.ui;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.j;
import android.support.v4.app.z.d;
import android.support.v4.content.b;
import android.support.v7.app.AlertDialog.Builder;
import c.g.b.k;
import com.truecaller.notificationchannels.e;

public final class d
{
  private final Context a;
  private final e b;
  private final com.truecaller.notifications.a c;
  
  public d(Context paramContext, e parame, com.truecaller.notifications.a parama)
  {
    a = paramContext;
    b = parame;
    c = parama;
  }
  
  public static void a(Context paramContext)
  {
    k.b(paramContext, "context");
    Object localObject = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject).<init>(paramContext);
    paramContext = ((AlertDialog.Builder)localObject).setTitle(2131887988).setMessage(2131887987);
    localObject = (DialogInterface.OnClickListener)d.h.a;
    paramContext.setNegativeButton(2131887218, (DialogInterface.OnClickListener)localObject).show();
  }
  
  public static void a(Context paramContext, c.g.a.a parama)
  {
    k.b(paramContext, "context");
    k.b(parama, "onRetry");
    Object localObject = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject).<init>(paramContext);
    paramContext = ((AlertDialog.Builder)localObject).setTitle(2131887994).setMessage(2131887993);
    localObject = (DialogInterface.OnClickListener)d.f.a;
    paramContext = paramContext.setNegativeButton(2131887205, (DialogInterface.OnClickListener)localObject);
    localObject = new com/truecaller/engagementrewards/ui/d$g;
    ((d.g)localObject).<init>(parama);
    localObject = (DialogInterface.OnClickListener)localObject;
    paramContext.setPositiveButton(2131887224, (DialogInterface.OnClickListener)localObject).show();
  }
  
  public static void a(Context paramContext, String paramString)
  {
    k.b(paramContext, "context");
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    localBuilder.<init>(paramContext);
    localBuilder = localBuilder.setTitle(2131887990);
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramString;
    paramContext = (CharSequence)paramContext.getString(2131887989, arrayOfObject);
    paramContext = localBuilder.setMessage(paramContext);
    paramString = (DialogInterface.OnClickListener)d.e.a;
    paramContext.setNegativeButton(2131887205, paramString).show();
  }
  
  public static void a(j paramj)
  {
    k.b(paramj, "fragmentManager");
    h localh = new com/truecaller/engagementrewards/ui/h;
    localh.<init>();
    localh.show(paramj, "RewardsRedeemCelebrationDialog");
  }
  
  public static void b(Context paramContext)
  {
    k.b(paramContext, "context");
    Object localObject = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject).<init>(paramContext);
    paramContext = ((AlertDialog.Builder)localObject).setTitle(2131887992).setMessage(2131887991);
    localObject = (DialogInterface.OnClickListener)d.i.a;
    paramContext.setNegativeButton(2131887205, (DialogInterface.OnClickListener)localObject).show();
  }
  
  public static void b(Context paramContext, c.g.a.a parama)
  {
    k.b(paramContext, "context");
    k.b(parama, "onRetry");
    Object localObject = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject).<init>(paramContext);
    paramContext = ((AlertDialog.Builder)localObject).setTitle(2131887986).setMessage(2131887985);
    localObject = (DialogInterface.OnClickListener)d.c.a;
    paramContext = paramContext.setNegativeButton(2131887205, (DialogInterface.OnClickListener)localObject);
    localObject = new com/truecaller/engagementrewards/ui/d$d;
    ((d.d)localObject).<init>(parama);
    localObject = (DialogInterface.OnClickListener)localObject;
    paramContext.setPositiveButton(2131887224, (DialogInterface.OnClickListener)localObject).show();
  }
  
  public static void c(Context paramContext, c.g.a.a parama)
  {
    k.b(paramContext, "context");
    k.b(parama, "onAgree");
    Object localObject = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject).<init>(paramContext);
    paramContext = ((AlertDialog.Builder)localObject).setTitle(2131887977).setMessage(2131887976);
    localObject = new com/truecaller/engagementrewards/ui/d$a;
    ((d.a)localObject).<init>(parama);
    localObject = (DialogInterface.OnClickListener)localObject;
    paramContext = paramContext.setPositiveButton(2131887193, (DialogInterface.OnClickListener)localObject);
    parama = (DialogInterface.OnClickListener)d.b.a;
    paramContext.setNegativeButton(2131887216, parama).show();
  }
  
  public final void a()
  {
    Object localObject1 = a;
    Object localObject2 = ClaimEngagementRewardsActivity.c;
    localObject2 = ClaimEngagementRewardsActivity.a.a(a, "NOTIFICATION");
    localObject2 = PendingIntent.getActivity((Context)localObject1, 0, (Intent)localObject2, 134217728);
    int i = 2131887996;
    Object localObject3 = ((Context)localObject1).getString(i);
    int j = 2131887995;
    Object localObject4 = ((Context)localObject1).getString(j);
    Object localObject5 = b.k();
    z.d locald;
    if (localObject5 != null)
    {
      locald = new android/support/v4/app/z$d;
      locald.<init>((Context)localObject1, (String)localObject5);
    }
    else
    {
      locald = new android/support/v4/app/z$d;
      locald.<init>((Context)localObject1);
    }
    localObject3 = (CharSequence)localObject3;
    localObject3 = locald.a((CharSequence)localObject3);
    localObject4 = (CharSequence)localObject4;
    localObject3 = ((z.d)localObject3).b((CharSequence)localObject4);
    j = b.c((Context)localObject1, 2131100594);
    localObject3 = ((z.d)localObject3).f(j).c(-1);
    localObject1 = BitmapFactory.decodeResource(((Context)localObject1).getResources(), 2131234704);
    localObject5 = ((z.d)localObject3).a((Bitmap)localObject1).a(2131234787).a((PendingIntent)localObject2).e().h();
    localObject2 = c;
    k.a(localObject5, "notification");
    ((com.truecaller.notifications.a)localObject2).a(null, 2131362982, (Notification)localObject5, "notificationEngagementRewards", null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.ui.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */