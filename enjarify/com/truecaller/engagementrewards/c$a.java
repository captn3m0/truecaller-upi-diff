package com.truecaller.engagementrewards;

import c.d.a.a;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.premium.a.d;
import com.truecaller.premium.a.d.a;
import com.truecaller.premium.bz;
import java.util.Iterator;
import kotlinx.coroutines.ag;

final class c$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  c$a(c paramc, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/engagementrewards/c$a;
    c localc = b;
    locala.<init>(localc, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        new String[1][0] = "onPremiumPurchaseComplete:: Checking billing info to set reward state";
        paramObject = ((Iterable)b.a.b().b).iterator();
        for (;;)
        {
          bool = ((Iterator)paramObject).hasNext();
          if (!bool) {
            break;
          }
          localObject1 = (bz)((Iterator)paramObject).next();
          int j = 1;
          Object localObject2 = new String[j];
          EngagementRewardState localEngagementRewardState = null;
          Object localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>("onPremiumPurchaseComplete:: ");
          Object localObject4 = a;
          ((StringBuilder)localObject3).append((String)localObject4);
          ((StringBuilder)localObject3).append(" state: ");
          localObject4 = b;
          EngagementRewardActionType localEngagementRewardActionType = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
          localObject4 = ((c)localObject4).a(localEngagementRewardActionType);
          ((StringBuilder)localObject3).append(localObject4);
          localObject3 = ((StringBuilder)localObject3).toString();
          localObject2[0] = localObject3;
          localObject2 = b;
          localObject1 = a;
          bool = ((c)localObject2).a((String)localObject1);
          if (bool)
          {
            localObject1 = b;
            localObject2 = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
            localObject1 = ((c)localObject1).a((EngagementRewardActionType)localObject2);
            localObject2 = EngagementRewardState.SHOWN;
            if (localObject1 == localObject2)
            {
              localObject1 = b;
              localObject2 = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
              localEngagementRewardState = EngagementRewardState.PENDING;
              ((c)localObject1).a((EngagementRewardActionType)localObject2, localEngagementRewardState);
            }
          }
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */