package com.truecaller.engagementrewards;

import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d
{
  private final m a;
  private final Provider b;
  
  private q(m paramm, Provider paramProvider)
  {
    a = paramm;
    b = paramProvider;
  }
  
  public static q a(m paramm, Provider paramProvider)
  {
    q localq = new com/truecaller/engagementrewards/q;
    localq.<init>(paramm, paramProvider);
    return localq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */