package com.truecaller.engagementrewards;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public final class u
{
  private static ListeningExecutorService a;
  private static Executor b;
  
  public static ListeningExecutorService a()
  {
    ListeningExecutorService localListeningExecutorService = a;
    if (localListeningExecutorService == null)
    {
      int i = 4;
      localListeningExecutorService = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(i));
      a = localListeningExecutorService;
    }
    return a;
  }
  
  public static Executor b()
  {
    Object localObject = b;
    if (localObject == null)
    {
      localObject = new com/truecaller/engagementrewards/u$a;
      ((u.a)localObject).<init>();
      b = (Executor)localObject;
    }
    return b;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.engagementrewards.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */