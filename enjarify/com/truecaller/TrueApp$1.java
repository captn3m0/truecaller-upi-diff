package com.truecaller;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.common.background.b;
import com.truecaller.push.e;
import com.truecaller.service.SyncPhoneBookService;
import com.truecaller.util.o;
import com.truecaller.wizard.utils.i.a;

final class TrueApp$1
  extends i.a
{
  TrueApp$1(TrueApp paramTrueApp) {}
  
  public final void a(Context paramContext, String paramString)
  {
    Object localObject = "android.permission.READ_CONTACTS";
    boolean bool1 = TextUtils.equals(paramString, (CharSequence)localObject);
    if (bool1)
    {
      SyncPhoneBookService.a(paramContext);
      localObject = a.c;
      int i = 10015;
      ((b)localObject).b(i);
    }
    localObject = "android.permission.READ_CALL_LOG";
    boolean bool2 = ((String)localObject).equals(paramString);
    if (bool2) {
      o.a(paramContext);
    }
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    String str1 = "com.truecaller.wizard.verification.action.PHONE_VERIFIED";
    String str2 = paramIntent.getAction();
    boolean bool = str1.equals(str2);
    if (bool)
    {
      SyncPhoneBookService.a(paramContext, true);
      a.c.b(10015);
      TrueApp.a(a).bZ().b(null);
      ((com.truecaller.config.a)TrueApp.a(a).aZ().a()).a().c();
      return;
    }
    super.onReceive(paramContext, paramIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.TrueApp.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */