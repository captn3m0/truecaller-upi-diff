package com.truecaller.consentrefresh;

import android.content.Context;
import android.content.Intent;
import c.g.b.k;

public final class ConsentRefreshActivity$a
{
  public static void a(Context paramContext, boolean paramBoolean)
  {
    k.b(paramContext, "context");
    Intent localIntent = new android/content/Intent;
    Class localClass = ConsentRefreshActivity.class;
    localIntent.<init>(paramContext, localClass);
    if (paramBoolean)
    {
      String str = "adsChoicesOnly";
      boolean bool = true;
      localIntent.putExtra(str, bool);
    }
    paramContext.startActivity(localIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.consentrefresh.ConsentRefreshActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */