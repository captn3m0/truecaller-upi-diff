package com.truecaller.consentrefresh;

import android.os.Bundle;
import android.support.v4.app.e;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import c.g.b.k;
import com.truecaller.R.id;
import java.util.HashMap;

public final class i
  extends e
{
  j a;
  private HashMap b;
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setStyle(1, 2131952437);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    paramLayoutInflater = paramLayoutInflater.inflate(2131558564, paramViewGroup, false);
    int i = R.id.deactivateButton;
    paramViewGroup = (Button)paramLayoutInflater.findViewById(i);
    paramBundle = new com/truecaller/consentrefresh/i$a;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramViewGroup.setOnClickListener(paramBundle);
    i = R.id.downloadDataButton;
    paramViewGroup = (Button)paramLayoutInflater.findViewById(i);
    paramBundle = new com/truecaller/consentrefresh/i$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramViewGroup.setOnClickListener(paramBundle);
    i = R.id.cancelButton;
    paramViewGroup = (Button)paramLayoutInflater.findViewById(i);
    paramBundle = new com/truecaller/consentrefresh/i$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramViewGroup.setOnClickListener(paramBundle);
    return paramLayoutInflater;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.consentrefresh.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */