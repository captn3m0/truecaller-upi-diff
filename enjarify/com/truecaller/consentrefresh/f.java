package com.truecaller.consentrefresh;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AlertDialog.Builder;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import c.g.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.R.id;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.ui.o;
import com.truecaller.utils.extensions.p;
import com.truecaller.utils.extensions.p.b;
import java.util.HashMap;

public final class f
  extends o
  implements g.c
{
  public g.a a;
  private final f.a b;
  private HashMap c;
  
  public f()
  {
    f.a locala = new com/truecaller/consentrefresh/f$a;
    locala.<init>(this);
    b = locala;
  }
  
  private final void a(TextView paramTextView)
  {
    Object localObject = LinkMovementMethod.getInstance();
    paramTextView.setMovementMethod((MovementMethod)localObject);
    localObject = new com/truecaller/consentrefresh/f$b;
    ((f.b)localObject).<init>(this, paramTextView);
    localObject = (m)localObject;
    p.a(paramTextView, (m)localObject);
  }
  
  public final g.a F_()
  {
    g.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void b()
  {
    i locali = new com/truecaller/consentrefresh/i;
    locali.<init>();
    Object localObject = (j)b;
    a = ((j)localObject);
    localObject = getChildFragmentManager();
    locali.show((android.support.v4.app.j)localObject, "moreInfo");
  }
  
  public final void c()
  {
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "context ?: return");
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    localBuilder.<init>((Context)localObject1);
    localObject1 = localBuilder.setTitle(2131887107).setMessage(2131887105).setNegativeButton(2131887214, null);
    Object localObject2 = new com/truecaller/consentrefresh/f$f;
    ((f.f)localObject2).<init>(this);
    localObject2 = (DialogInterface.OnClickListener)localObject2;
    ((AlertDialog.Builder)localObject1).setPositiveButton(2131887235, (DialogInterface.OnClickListener)localObject2).show();
  }
  
  public final void d()
  {
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "context ?: return");
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    localBuilder.<init>((Context)localObject1);
    localObject1 = localBuilder.setMessage(2131887104).setNegativeButton(2131887214, null);
    Object localObject2 = new com/truecaller/consentrefresh/f$g;
    ((f.g)localObject2).<init>(this);
    localObject2 = (DialogInterface.OnClickListener)localObject2;
    ((AlertDialog.Builder)localObject1).setPositiveButton(2131887235, (DialogInterface.OnClickListener)localObject2).show();
  }
  
  public final void g()
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    k.a(localContext, "context ?: return");
    Toast.makeText(localContext, 2131886714, 1).show();
  }
  
  public final void h()
  {
    d_(false);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getContext();
    if (paramBundle != null) {
      paramBundle = paramBundle.getApplicationContext();
    } else {
      paramBundle = null;
    }
    if (paramBundle != null)
    {
      ((bk)paramBundle).a().cx().a(this);
      paramBundle = getActivity();
      if (paramBundle != null)
      {
        paramBundle = (g.b)paramBundle;
        g.a locala = a;
        if (locala == null)
        {
          String str = "presenter";
          k.a(str);
        }
        locala.b(paramBundle);
        return;
      }
      paramBundle = new c/u;
      paramBundle.<init>("null cannot be cast to non-null type com.truecaller.consentrefresh.ConsentRefreshMvp.Router");
      throw paramBundle;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    paramLayoutInflater = paramLayoutInflater.inflate(2131558676, paramViewGroup, false);
    int i = R.id.reminderText;
    paramViewGroup = (TextView)paramLayoutInflater.findViewById(i);
    k.a(paramViewGroup, "reminderText");
    int j = 2;
    Object localObject1 = new Object[j];
    localObject1[0] = "https://privacy.truecaller.com/privacy-policy-eu";
    int k = 1;
    localObject1[k] = "https://www.truecaller.com/terms-of-service#eu";
    paramViewGroup = p.a(paramViewGroup, 2131886400, (Object[])localObject1);
    a(paramViewGroup);
    i = R.id.dataUsedText;
    paramViewGroup = (TextView)paramLayoutInflater.findViewById(i);
    k.b(paramViewGroup, "receiver$0");
    int m = paramViewGroup.getResources().getColor(2131100661);
    Object localObject2 = new com/truecaller/utils/extensions/p$b;
    ((p.b)localObject2).<init>(m);
    localObject2 = (m)localObject2;
    p.a(paramViewGroup, (m)localObject2);
    localObject1 = new com/truecaller/consentrefresh/f$c;
    ((f.c)localObject1).<init>(this);
    localObject1 = (View.OnClickListener)localObject1;
    paramViewGroup.setOnClickListener((View.OnClickListener)localObject1);
    i = R.id.moreInfoButton;
    paramViewGroup = (Button)paramLayoutInflater.findViewById(i);
    localObject1 = new com/truecaller/consentrefresh/f$d;
    ((f.d)localObject1).<init>(this);
    localObject1 = (View.OnClickListener)localObject1;
    paramViewGroup.setOnClickListener((View.OnClickListener)localObject1);
    i = R.id.agreeButton;
    paramViewGroup = (Button)paramLayoutInflater.findViewById(i);
    localObject1 = new com/truecaller/consentrefresh/f$e;
    ((f.e)localObject1).<init>(this);
    localObject1 = (View.OnClickListener)localObject1;
    paramViewGroup.setOnClickListener((View.OnClickListener)localObject1);
    i = R.id.legalFooterText;
    paramViewGroup = (TextView)paramLayoutInflater.findViewById(i);
    k.a(paramViewGroup, "legalFooterText");
    Object[] arrayOfObject = new Object[j];
    arrayOfObject[0] = "https://privacy.truecaller.com/privacy-policy-eu";
    arrayOfObject[k] = "https://www.truecaller.com/terms-of-service#eu";
    paramViewGroup = p.a(paramViewGroup, 2131886919, arrayOfObject);
    a(paramViewGroup);
    return paramLayoutInflater;
  }
  
  public final void onDestroy()
  {
    g.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.x_();
    super.onDestroy();
  }
  
  public final void onDestroyView()
  {
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((g.a)localObject).y_();
    super.onDestroyView();
    localObject = c;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.consentrefresh.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */