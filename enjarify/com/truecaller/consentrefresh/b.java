package com.truecaller.consentrefresh;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.ui.o;
import com.truecaller.wizard.adschoices.h.a;
import com.truecaller.wizard.adschoices.h.b;
import com.truecaller.wizard.adschoices.h.c;
import com.truecaller.wizard.adschoices.h.d;
import java.util.HashMap;

public final class b
  extends o
  implements h.a
{
  public h.b a;
  public h.c b;
  private HashMap c;
  
  public final void E_()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = ((f)localObject).getSupportFragmentManager();
      if (localObject != null)
      {
        bool = ((j)localObject).d();
        localObject = Boolean.valueOf(bool);
        break label35;
      }
    }
    boolean bool = false;
    localObject = null;
    label35:
    bool = com.truecaller.utils.extensions.c.a((Boolean)localObject);
    if (!bool)
    {
      localObject = getActivity();
      if (localObject != null)
      {
        ((f)localObject).finish();
        return;
      }
    }
  }
  
  public final void b()
  {
    d_(false);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getContext();
    if (paramBundle != null) {
      paramBundle = paramBundle.getApplicationContext();
    } else {
      paramBundle = null;
    }
    if (paramBundle != null)
    {
      paramBundle = ((bk)paramBundle).a();
      c localc = new com/truecaller/consentrefresh/c;
      Object localObject = this;
      localObject = (h.a)this;
      localc.<init>((h.a)localObject);
      paramBundle.a(localc).a(this);
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    paramBundle = b;
    if (paramBundle == null)
    {
      String str = "view";
      k.a(str);
    }
    return paramBundle.d().a(paramLayoutInflater, paramViewGroup, false);
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject = b;
    if (localObject == null)
    {
      String str = "view";
      k.a(str);
    }
    ((h.c)localObject).d().f();
    localObject = c;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "v";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = b;
    if (paramView == null)
    {
      paramBundle = "view";
      k.a(paramBundle);
    }
    paramView.d().e();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.consentrefresh.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */