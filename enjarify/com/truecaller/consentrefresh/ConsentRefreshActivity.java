package com.truecaller.consentrefresh;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.o;
import android.support.v7.app.AppCompatActivity;
import c.g.b.k;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.util.dh;
import com.truecaller.utils.extensions.c;

public final class ConsentRefreshActivity
  extends AppCompatActivity
  implements g.b
{
  public static final ConsentRefreshActivity.a a;
  
  static
  {
    ConsentRefreshActivity.a locala = new com/truecaller/consentrefresh/ConsentRefreshActivity$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public static final void a(Context paramContext)
  {
    ConsentRefreshActivity.a.a(paramContext, true);
  }
  
  private final void a(boolean paramBoolean)
  {
    o localo = getSupportFragmentManager().a();
    Object localObject = new com/truecaller/consentrefresh/b;
    ((b)localObject).<init>();
    localObject = (Fragment)localObject;
    int i = 16908290;
    localo = localo.a(i, (Fragment)localObject, null);
    if (paramBoolean)
    {
      String str = "adsChoices";
      localo.a(str);
    }
    else
    {
      localo.a();
    }
    localo.c();
  }
  
  public static final void b(Context paramContext)
  {
    ConsentRefreshActivity.a.a(paramContext, false);
  }
  
  public final void a()
  {
    a(true);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "url");
    dh.a((Context)this, paramString, false);
  }
  
  public final void b()
  {
    TruecallerInit.b((Context)this, null);
  }
  
  public final void c()
  {
    com.truecaller.ui.j localj = new com/truecaller/ui/j;
    Object localObject = this;
    localObject = (Context)this;
    localj.<init>((Context)localObject);
    localj.show();
  }
  
  public final void d()
  {
    finish();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getIntent();
    if (paramBundle != null)
    {
      String str = "adsChoicesOnly";
      bool = paramBundle.hasExtra(str);
      paramBundle = Boolean.valueOf(bool);
    }
    else
    {
      bool = false;
      paramBundle = null;
    }
    boolean bool = c.a(paramBundle);
    if (bool)
    {
      a(false);
      return;
    }
    paramBundle = getSupportFragmentManager().a();
    Object localObject = new com/truecaller/consentrefresh/f;
    ((f)localObject).<init>();
    localObject = (Fragment)localObject;
    paramBundle.a(16908290, (Fragment)localObject, null).a().c();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.consentrefresh.ConsentRefreshActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */