package com.truecaller.consentrefresh;

import c.d.f;
import com.truecaller.ads.provider.fetch.AdsConfigurationManager;
import com.truecaller.ads.provider.fetch.AdsConfigurationManager.PromotionState;
import com.truecaller.ads.provider.fetch.AdsConfigurationManager.TargetingState;
import com.truecaller.common.h.ac;
import com.truecaller.common.network.optout.a;
import com.truecaller.wizard.adschoices.AdsChoice;
import com.truecaller.wizard.adschoices.h.c;
import com.truecaller.wizard.adschoices.j;

public final class k
  extends j
{
  private final boolean e;
  private final AdsConfigurationManager f;
  
  public k(a parama, f paramf1, f paramf2, AdsConfigurationManager paramAdsConfigurationManager, ac paramac)
  {
    super(parama, paramf1, paramf2, paramac);
    f = paramAdsConfigurationManager;
    e = true;
  }
  
  public final void a(AdsChoice paramAdsChoice, boolean paramBoolean1, boolean paramBoolean2)
  {
    Object localObject = "choice";
    c.g.b.k.b(paramAdsChoice, (String)localObject);
    super.a(paramAdsChoice, paramBoolean1, paramBoolean2);
    if (paramBoolean2)
    {
      localObject = AdsChoice.PERSONALIZED_ADS;
      if (paramAdsChoice == localObject)
      {
        if (paramBoolean1) {
          paramAdsChoice = AdsConfigurationManager.TargetingState.TARGETING;
        } else {
          paramAdsChoice = AdsConfigurationManager.TargetingState.NON_TARGETING;
        }
        f.a(paramAdsChoice);
        return;
      }
    }
    if (paramBoolean2)
    {
      AdsChoice localAdsChoice = AdsChoice.DIRECT_MARKETING;
      if (paramAdsChoice == localAdsChoice)
      {
        if (paramBoolean1) {
          paramAdsChoice = AdsConfigurationManager.PromotionState.OPT_IN;
        } else {
          paramAdsChoice = AdsConfigurationManager.PromotionState.OPT_OUT;
        }
        AdsConfigurationManager localAdsConfigurationManager = f;
        localAdsConfigurationManager.a(paramAdsChoice);
      }
    }
  }
  
  public final boolean a()
  {
    return e;
  }
  
  public final void b()
  {
    h.c localc = (h.c)b;
    if (localc != null)
    {
      localc.a();
      return;
    }
  }
  
  public final void y_()
  {
    super.y_();
    f.f();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.consentrefresh.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */