package com.truecaller.consentrefresh;

import com.truecaller.analytics.bb;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.bd;
import com.truecaller.common.account.r;

public final class h
  extends bd
  implements g.a
{
  private com.truecaller.androidactors.a a;
  private final r d;
  private final com.truecaller.common.g.a e;
  private final f f;
  private final com.truecaller.androidactors.k g;
  private final f h;
  
  public h(r paramr, com.truecaller.common.g.a parama, f paramf1, com.truecaller.androidactors.k paramk, f paramf2)
  {
    d = paramr;
    e = parama;
    f = paramf1;
    g = paramk;
    h = paramf2;
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "url");
    g.b localb = (g.b)c;
    if (localb != null)
    {
      localb.a(paramString);
      return;
    }
  }
  
  public final void b()
  {
    Object localObject = e;
    boolean bool = true;
    ((com.truecaller.common.g.a)localObject).b("core_agreed_region_1", bool);
    localObject = h;
    String str1 = "consentUpgrade";
    String str2 = "accepted";
    bb.a((f)localObject, str1, str2);
    localObject = (g.b)c;
    if (localObject != null) {
      ((g.b)localObject).b();
    }
    localObject = (g.b)c;
    if (localObject != null)
    {
      ((g.b)localObject).d();
      return;
    }
  }
  
  public final void c()
  {
    g.c localc = (g.c)b;
    if (localc != null)
    {
      localc.b();
      return;
    }
  }
  
  public final void e()
  {
    g.c localc = (g.c)b;
    if (localc != null)
    {
      localc.d();
      return;
    }
  }
  
  public final void f()
  {
    Object localObject1 = (g.c)b;
    if (localObject1 != null) {
      ((g.c)localObject1).h();
    }
    localObject1 = a;
    if (localObject1 != null) {
      ((com.truecaller.androidactors.a)localObject1).a();
    }
    localObject1 = ((com.truecaller.network.a.a)f.a()).a(true);
    i locali = g.a();
    Object localObject2 = new com/truecaller/consentrefresh/h$a;
    ((h.a)localObject2).<init>(this);
    localObject2 = (ac)localObject2;
    localObject1 = ((w)localObject1).a(locali, (ac)localObject2);
    a = ((com.truecaller.androidactors.a)localObject1);
  }
  
  public final void g()
  {
    g.b localb = (g.b)c;
    if (localb != null)
    {
      localb.c();
      return;
    }
  }
  
  public final void h()
  {
    g.b localb = (g.b)c;
    if (localb != null)
    {
      localb.a();
      return;
    }
  }
  
  public final void i()
  {
    g.c localc = (g.c)b;
    if (localc != null)
    {
      localc.c();
      return;
    }
  }
  
  public final void y_()
  {
    super.y_();
    com.truecaller.androidactors.a locala = a;
    if (locala != null) {
      locala.a();
    }
    a = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.consentrefresh.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */