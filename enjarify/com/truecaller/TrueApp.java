package com.truecaller;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import c.a.ag;
import c.a.an;
import com.d.b.ac;
import com.d.b.aj;
import com.d.b.w.a;
import com.truecaller.ads.provider.fetch.AdsConfigurationManager;
import com.truecaller.analytics.AppHeartBeatTask;
import com.truecaller.analytics.AppSettingsTask;
import com.truecaller.analytics.as.a;
import com.truecaller.analytics.e.a;
import com.truecaller.android.truemoji.Emoji;
import com.truecaller.android.truemoji.f.e;
import com.truecaller.backup.bb;
import com.truecaller.backup.bb.a;
import com.truecaller.common.account.r;
import com.truecaller.common.h.ai;
import com.truecaller.common.h.am;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.tag.TagService;
import com.truecaller.content.TruecallerContentProvider;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.filters.FilterManager;
import com.truecaller.filters.FilterManager.FilterAction;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.FlashContact;
import com.truecaller.incallui.a.i.a;
import com.truecaller.log.AssertionUtil;
import com.truecaller.notificationchannels.n.a;
import com.truecaller.old.data.access.Settings;
import com.truecaller.presence.AvailabilityTrigger;
import com.truecaller.presence.RingerModeListenerWorker;
import com.truecaller.profile.BusinessProfileOnboardingActivity;
import com.truecaller.sdk.ag.a;
import com.truecaller.service.AlarmReceiver;
import com.truecaller.service.CallStateService;
import com.truecaller.service.ClipboardService;
import com.truecaller.service.RefreshContactIndexingService.a;
import com.truecaller.service.RefreshT9MappingService;
import com.truecaller.tcpermissions.c.a;
import com.truecaller.tcpermissions.f.a;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import com.truecaller.truepay.PayTempTokenCallBack;
import com.truecaller.truepay.TcPaySDKListener;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.a.b.cx;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.WizardActivity;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.util.al;
import com.truecaller.util.bv;
import com.truecaller.util.cm;
import com.truecaller.util.co;
import com.truecaller.util.df;
import com.truecaller.utils.t.a;
import com.truecaller.voip.d.a;
import com.truecaller.voip.j.a;
import com.truecaller.voip.util.af;
import com.truecaller.voip.util.at;
import com.truecaller.voip.util.z;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

public class TrueApp
  extends com.truecaller.common.b.a
  implements com.truecaller.analytics.a, bk, com.truecaller.flashsdk.core.i, com.truecaller.sdk.ae, ag.a, TcPaySDKListener
{
  public static final long a = ;
  public final com.truecaller.common.a b;
  private com.truecaller.analytics.b d;
  private com.truecaller.analytics.t e;
  private final com.truecaller.utils.t f;
  private final com.truecaller.analytics.d g;
  private final com.truecaller.backup.c h;
  private final com.truecaller.tcpermissions.f i;
  private final com.truecaller.voip.j j;
  private final com.truecaller.insights.a.a.b k;
  private final com.truecaller.incallui.a.i l;
  private final bp m;
  private final BroadcastReceiver n;
  
  public TrueApp()
  {
    Object localObject1 = new com/truecaller/analytics/t;
    ((com.truecaller.analytics.t)localObject1).<init>("Truecaller", "10.41.6");
    e = ((com.truecaller.analytics.t)localObject1);
    localObject1 = com.truecaller.utils.c.a().a(this).a();
    f = ((com.truecaller.utils.t)localObject1);
    localObject1 = com.truecaller.analytics.aa.a().a(this).a(this).b();
    Object localObject2 = e;
    localObject1 = ((as.a)localObject1).a((com.truecaller.analytics.s)localObject2);
    localObject2 = f;
    localObject1 = ((as.a)localObject1).a((com.truecaller.utils.t)localObject2).a();
    g = ((com.truecaller.analytics.d)localObject1);
    localObject1 = com.truecaller.common.b.x();
    localObject2 = new com/truecaller/common/b/a$a;
    ((com.truecaller.common.b.a.a)localObject2).<init>(this);
    localObject2 = (com.truecaller.common.b.a.a)dagger.a.g.a(localObject2);
    a = ((com.truecaller.common.b.a.a)localObject2);
    localObject2 = (com.truecaller.utils.t)dagger.a.g.a(f);
    g = ((com.truecaller.utils.t)localObject2);
    localObject2 = (com.truecaller.analytics.d)dagger.a.g.a(g);
    h = ((com.truecaller.analytics.d)localObject2);
    localObject2 = new com/truecaller/common/g/c;
    ((com.truecaller.common.g.c)localObject2).<init>(this, "tc.settings");
    localObject2 = (com.truecaller.common.g.c)dagger.a.g.a(localObject2);
    b = ((com.truecaller.common.g.c)localObject2);
    dagger.a.g.a(a, com.truecaller.common.b.a.a.class);
    localObject2 = b;
    Object localObject3 = com.truecaller.common.g.c.class;
    dagger.a.g.a(localObject2, (Class)localObject3);
    localObject2 = c;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/common/edge/c;
      ((com.truecaller.common.edge.c)localObject2).<init>();
      c = ((com.truecaller.common.edge.c)localObject2);
    }
    localObject2 = d;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/common/h/x;
      ((com.truecaller.common.h.x)localObject2).<init>();
      d = ((com.truecaller.common.h.x)localObject2);
    }
    localObject2 = e;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/content/a;
      ((com.truecaller.content.a)localObject2).<init>();
      e = ((com.truecaller.content.a)localObject2);
    }
    localObject2 = f;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/common/d/a;
      ((com.truecaller.common.d.a)localObject2).<init>();
      f = ((com.truecaller.common.d.a)localObject2);
    }
    dagger.a.g.a(g, com.truecaller.utils.t.class);
    dagger.a.g.a(h, com.truecaller.analytics.d.class);
    localObject2 = new com/truecaller/common/b;
    com.truecaller.common.b.a.a locala = a;
    Object localObject4 = b;
    Object localObject5 = c;
    Object localObject6 = d;
    Object localObject7 = e;
    Object localObject8 = f;
    Object localObject9 = g;
    Object localObject10 = h;
    com.truecaller.filters.h localh = null;
    Object localObject11 = localObject2;
    ((com.truecaller.common.b)localObject2).<init>(locala, (com.truecaller.common.g.c)localObject4, (com.truecaller.common.edge.c)localObject5, (com.truecaller.common.h.x)localObject6, (com.truecaller.content.a)localObject7, (com.truecaller.common.d.a)localObject8, (com.truecaller.utils.t)localObject9, (com.truecaller.analytics.d)localObject10, (byte)0);
    b = ((com.truecaller.common.a)localObject2);
    localObject1 = bb.e();
    localObject2 = (com.truecaller.utils.t)dagger.a.g.a(f);
    b = ((com.truecaller.utils.t)localObject2);
    localObject2 = (com.truecaller.common.a)dagger.a.g.a(b);
    a = ((com.truecaller.common.a)localObject2);
    localObject2 = (com.truecaller.analytics.d)dagger.a.g.a(g);
    c = ((com.truecaller.analytics.d)localObject2);
    dagger.a.g.a(a, com.truecaller.common.a.class);
    dagger.a.g.a(b, com.truecaller.utils.t.class);
    dagger.a.g.a(c, com.truecaller.analytics.d.class);
    localObject2 = new com/truecaller/backup/bb;
    localObject3 = a;
    localObject11 = b;
    localObject1 = c;
    locala = null;
    ((bb)localObject2).<init>((com.truecaller.common.a)localObject3, (com.truecaller.utils.t)localObject11, (com.truecaller.analytics.d)localObject1, (byte)0);
    h = ((com.truecaller.backup.c)localObject2);
    localObject1 = com.truecaller.tcpermissions.c.a();
    localObject2 = f;
    localObject1 = ((c.a)localObject1).a((com.truecaller.utils.t)localObject2);
    localObject2 = b;
    localObject1 = ((c.a)localObject1).a((com.truecaller.common.a)localObject2).a();
    i = ((com.truecaller.tcpermissions.f)localObject1);
    localObject1 = com.truecaller.voip.a.a();
    localObject2 = (com.truecaller.common.a)dagger.a.g.a(b);
    a = ((com.truecaller.common.a)localObject2);
    localObject2 = (com.truecaller.utils.t)dagger.a.g.a(f);
    b = ((com.truecaller.utils.t)localObject2);
    localObject2 = com.truecaller.notificationchannels.n.a;
    localObject2 = (com.truecaller.notificationchannels.n)dagger.a.g.a(n.a.a(this));
    c = ((com.truecaller.notificationchannels.n)localObject2);
    localObject2 = (com.truecaller.tcpermissions.e)dagger.a.g.a(i);
    d = ((com.truecaller.tcpermissions.e)localObject2);
    localObject2 = (com.truecaller.analytics.d)dagger.a.g.a(g);
    e = ((com.truecaller.analytics.d)localObject2);
    dagger.a.g.a(a, com.truecaller.common.a.class);
    dagger.a.g.a(b, com.truecaller.utils.t.class);
    dagger.a.g.a(c, com.truecaller.notificationchannels.n.class);
    dagger.a.g.a(d, com.truecaller.tcpermissions.e.class);
    dagger.a.g.a(e, com.truecaller.analytics.d.class);
    localObject2 = new com/truecaller/voip/a;
    localObject5 = a;
    localObject6 = b;
    localObject7 = c;
    localObject8 = d;
    localObject9 = e;
    localObject10 = null;
    localObject4 = localObject2;
    ((com.truecaller.voip.a)localObject2).<init>((com.truecaller.common.a)localObject5, (com.truecaller.utils.t)localObject6, (com.truecaller.notificationchannels.n)localObject7, (com.truecaller.tcpermissions.e)localObject8, (com.truecaller.analytics.d)localObject9, (byte)0);
    j = ((com.truecaller.voip.j)localObject2);
    localObject1 = com.truecaller.insights.a.a.a.a();
    localObject2 = g;
    localObject1 = ((com.truecaller.insights.a.a.b.a)localObject1).a((com.truecaller.analytics.d)localObject2);
    localObject2 = b;
    localObject1 = ((com.truecaller.insights.a.a.b.a)localObject1).a((com.truecaller.common.a)localObject2).a();
    k = ((com.truecaller.insights.a.a.b)localObject1);
    localObject1 = com.truecaller.incallui.a.a.a();
    localObject2 = (com.truecaller.common.a)dagger.a.g.a(b);
    a = ((com.truecaller.common.a)localObject2);
    localObject2 = com.truecaller.notificationchannels.n.a;
    localObject2 = (com.truecaller.notificationchannels.n)dagger.a.g.a(n.a.a(this));
    b = ((com.truecaller.notificationchannels.n)localObject2);
    localObject2 = (com.truecaller.utils.t)dagger.a.g.a(f);
    c = ((com.truecaller.utils.t)localObject2);
    dagger.a.g.a(a, com.truecaller.common.a.class);
    dagger.a.g.a(b, com.truecaller.notificationchannels.n.class);
    dagger.a.g.a(c, com.truecaller.utils.t.class);
    localObject2 = new com/truecaller/incallui/a/a;
    localObject3 = a;
    localObject11 = b;
    localObject1 = c;
    ((com.truecaller.incallui.a.a)localObject2).<init>((com.truecaller.common.a)localObject3, (com.truecaller.notificationchannels.n)localObject11, (com.truecaller.utils.t)localObject1, (byte)0);
    l = ((com.truecaller.incallui.a.i)localObject2);
    localObject1 = be.a();
    localObject2 = (com.truecaller.utils.t)dagger.a.g.a(f);
    z = ((com.truecaller.utils.t)localObject2);
    localObject2 = (com.truecaller.analytics.d)dagger.a.g.a(g);
    D = ((com.truecaller.analytics.d)localObject2);
    localObject2 = (com.truecaller.common.a)dagger.a.g.a(b);
    y = ((com.truecaller.common.a)localObject2);
    localObject2 = (com.truecaller.backup.a)dagger.a.g.a(h);
    A = ((com.truecaller.backup.a)localObject2);
    localObject2 = (com.truecaller.tcpermissions.e)dagger.a.g.a(i);
    B = ((com.truecaller.tcpermissions.e)localObject2);
    localObject2 = (com.truecaller.voip.j)dagger.a.g.a(j);
    C = ((com.truecaller.voip.j)localObject2);
    localObject2 = (com.truecaller.insights.a.a.b)dagger.a.g.a(k);
    E = ((com.truecaller.insights.a.a.b)localObject2);
    localObject2 = (com.truecaller.incallui.a.i)dagger.a.g.a(l);
    F = ((com.truecaller.incallui.a.i)localObject2);
    localObject2 = new com/truecaller/c;
    ((c)localObject2).<init>(localTrueApp);
    localObject2 = (c)dagger.a.g.a(localObject2);
    d = ((c)localObject2);
    localObject2 = a;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/engagementrewards/m;
      ((com.truecaller.engagementrewards.m)localObject2).<init>();
      a = ((com.truecaller.engagementrewards.m)localObject2);
    }
    localObject2 = b;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/messaging/l;
      ((com.truecaller.messaging.l)localObject2).<init>();
      b = ((com.truecaller.messaging.l)localObject2);
    }
    localObject2 = c;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/messaging/data/e;
      ((com.truecaller.messaging.data.e)localObject2).<init>();
      c = ((com.truecaller.messaging.data.e)localObject2);
    }
    localObject2 = d;
    localObject3 = c.class;
    dagger.a.g.a(localObject2, (Class)localObject3);
    localObject2 = e;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/messaging/transport/o;
      ((com.truecaller.messaging.transport.o)localObject2).<init>();
      e = ((com.truecaller.messaging.transport.o)localObject2);
    }
    localObject2 = f;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/messaging/transport/sms/c;
      ((com.truecaller.messaging.transport.sms.c)localObject2).<init>();
      f = ((com.truecaller.messaging.transport.sms.c)localObject2);
    }
    localObject2 = g;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/filters/h;
      ((com.truecaller.filters.h)localObject2).<init>();
      g = ((com.truecaller.filters.h)localObject2);
    }
    localObject2 = h;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/network/util/k;
      ((com.truecaller.network.util.k)localObject2).<init>();
      h = ((com.truecaller.network.util.k)localObject2);
    }
    localObject2 = i;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/smsparser/d;
      ((com.truecaller.smsparser.d)localObject2).<init>();
      i = ((com.truecaller.smsparser.d)localObject2);
    }
    localObject2 = j;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/truepay/app/a/b/cx;
      ((cx)localObject2).<init>();
      j = ((cx)localObject2);
    }
    localObject2 = k;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/messaging/transport/mms/n;
      ((com.truecaller.messaging.transport.mms.n)localObject2).<init>();
      k = ((com.truecaller.messaging.transport.mms.n)localObject2);
    }
    localObject2 = l;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/network/d/g;
      ((com.truecaller.network.d.g)localObject2).<init>();
      l = ((com.truecaller.network.d.g)localObject2);
    }
    localObject2 = m;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/presence/g;
      ((com.truecaller.presence.g)localObject2).<init>();
      m = ((com.truecaller.presence.g)localObject2);
    }
    localObject2 = n;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/callhistory/g;
      ((com.truecaller.callhistory.g)localObject2).<init>();
      n = ((com.truecaller.callhistory.g)localObject2);
    }
    localObject2 = o;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/i/h;
      ((com.truecaller.i.h)localObject2).<init>();
      o = ((com.truecaller.i.h)localObject2);
    }
    localObject2 = p;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/data/entity/c;
      ((com.truecaller.data.entity.c)localObject2).<init>();
      p = ((com.truecaller.data.entity.c)localObject2);
    }
    localObject2 = q;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/tag/f;
      ((com.truecaller.tag.f)localObject2).<init>();
      q = ((com.truecaller.tag.f)localObject2);
    }
    localObject2 = r;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/clevertap/g;
      ((com.truecaller.clevertap.g)localObject2).<init>();
      r = ((com.truecaller.clevertap.g)localObject2);
    }
    localObject2 = s;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/ads/installedapps/a;
      ((com.truecaller.ads.installedapps.a)localObject2).<init>();
      s = ((com.truecaller.ads.installedapps.a)localObject2);
    }
    localObject2 = t;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/config/e;
      ((com.truecaller.config.e)localObject2).<init>();
      t = ((com.truecaller.config.e)localObject2);
    }
    localObject2 = u;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/flash/f;
      ((com.truecaller.flash.f)localObject2).<init>();
      u = ((com.truecaller.flash.f)localObject2);
    }
    localObject2 = v;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/b/a;
      ((com.truecaller.b.a)localObject2).<init>();
      v = ((com.truecaller.b.a)localObject2);
    }
    localObject2 = w;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/sdk/push/d;
      ((com.truecaller.sdk.push.d)localObject2).<init>();
      w = ((com.truecaller.sdk.push.d)localObject2);
    }
    localObject2 = x;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/messaging/g/g;
      ((com.truecaller.messaging.g.g)localObject2).<init>();
      x = ((com.truecaller.messaging.g.g)localObject2);
    }
    dagger.a.g.a(y, com.truecaller.common.a.class);
    dagger.a.g.a(z, com.truecaller.utils.t.class);
    dagger.a.g.a(A, com.truecaller.backup.a.class);
    dagger.a.g.a(B, com.truecaller.tcpermissions.e.class);
    dagger.a.g.a(C, com.truecaller.voip.j.class);
    dagger.a.g.a(D, com.truecaller.analytics.d.class);
    dagger.a.g.a(E, com.truecaller.insights.a.a.b.class);
    dagger.a.g.a(F, com.truecaller.incallui.a.i.class);
    localObject2 = new com/truecaller/be;
    localObject4 = localObject2;
    localObject5 = a;
    localObject6 = b;
    localObject7 = c;
    localObject8 = d;
    localObject9 = e;
    localObject10 = f;
    localh = g;
    com.truecaller.network.util.k localk = h;
    com.truecaller.smsparser.d locald = i;
    cx localcx = j;
    com.truecaller.messaging.transport.mms.n localn = k;
    com.truecaller.network.d.g localg = l;
    com.truecaller.presence.g localg1 = m;
    com.truecaller.callhistory.g localg2 = n;
    com.truecaller.i.h localh1 = o;
    com.truecaller.data.entity.c localc = p;
    com.truecaller.tag.f localf = q;
    com.truecaller.clevertap.g localg3 = r;
    com.truecaller.ads.installedapps.a locala1 = s;
    com.truecaller.config.e locale = t;
    com.truecaller.flash.f localf1 = u;
    com.truecaller.b.a locala2 = v;
    com.truecaller.sdk.push.d locald1 = w;
    com.truecaller.messaging.g.g localg4 = x;
    com.truecaller.common.a locala3 = y;
    com.truecaller.utils.t localt = z;
    com.truecaller.backup.a locala4 = A;
    com.truecaller.tcpermissions.e locale1 = B;
    com.truecaller.voip.j localj = C;
    com.truecaller.analytics.d locald2 = D;
    localObject3 = E;
    localObject1 = F;
    ((be)localObject2).<init>((com.truecaller.engagementrewards.m)localObject5, (com.truecaller.messaging.l)localObject6, (com.truecaller.messaging.data.e)localObject7, (c)localObject8, (com.truecaller.messaging.transport.o)localObject9, (com.truecaller.messaging.transport.sms.c)localObject10, localh, localk, locald, localcx, localn, localg, localg1, localg2, localh1, localc, localf, localg3, locala1, locale, localf1, locala2, locald1, localg4, locala3, localt, locala4, locale1, localj, locald2, (com.truecaller.insights.a.a.b)localObject3, (com.truecaller.incallui.a.i)localObject1, (byte)0);
    m = ((bp)localObject2);
    localObject1 = new com/truecaller/TrueApp$a;
    ((TrueApp.a)localObject1).<init>(localTrueApp, (byte)0);
    n = ((BroadcastReceiver)localObject1);
    localObject1 = com.truecaller.tcpermissions.f.a;
    f.a.a(i);
    localObject1 = com.truecaller.voip.d.a;
    localObject1 = new com/truecaller/voip/f;
    ((com.truecaller.voip.f)localObject1).<init>(localTrueApp);
    d.a.a((com.truecaller.voip.util.y)localObject1);
    localObject1 = com.truecaller.voip.d.a;
    localObject1 = new com/truecaller/voip/s;
    ((com.truecaller.voip.s)localObject1).<init>(localTrueApp);
    d.a.a((af)localObject1);
    localObject1 = com.truecaller.voip.d.a;
    localObject1 = new com/truecaller/voip/i;
    ((com.truecaller.voip.i)localObject1).<init>(localTrueApp);
    d.a.a((com.truecaller.voip.util.aa)localObject1);
    localObject1 = com.truecaller.voip.d.a;
    localObject1 = new com/truecaller/voip/h;
    ((com.truecaller.voip.h)localObject1).<init>(localTrueApp);
    d.a.a((z)localObject1);
    localObject1 = com.truecaller.voip.d.a;
    localObject1 = new com/truecaller/voip/b;
    ((com.truecaller.voip.b)localObject1).<init>(localTrueApp);
    d.a.a((com.truecaller.voip.util.l)localObject1);
    localObject1 = com.truecaller.voip.d.a;
    localObject1 = new com/truecaller/voip/ah;
    ((com.truecaller.voip.ah)localObject1).<init>(localTrueApp);
    d.a.a((at)localObject1);
    localObject1 = com.truecaller.voip.j.a;
    j.a.a(j);
    localObject1 = com.truecaller.incallui.a.a;
    localObject1 = new com/truecaller/incallui/b;
    ((com.truecaller.incallui.b)localObject1).<init>(localTrueApp);
    com.truecaller.incallui.a.a.a((com.truecaller.incallui.a.g)localObject1);
    localObject1 = com.truecaller.incallui.a.a;
    localObject1 = new com/truecaller/incallui/f;
    ((com.truecaller.incallui.f)localObject1).<init>(localTrueApp);
    com.truecaller.incallui.a.a.a((com.truecaller.incallui.a.o)localObject1);
    localObject1 = com.truecaller.incallui.a.a;
    localObject1 = new com/truecaller/incallui/e;
    ((com.truecaller.incallui.e)localObject1).<init>(localTrueApp);
    com.truecaller.incallui.a.a.a((com.truecaller.incallui.a.n)localObject1);
    localObject1 = com.truecaller.incallui.a.i.a;
    i.a.a(l);
  }
  
  public static Context x()
  {
    return F();
  }
  
  public static TrueApp y()
  {
    return (TrueApp)F();
  }
  
  public final String A()
  {
    return m.by().e().toString();
  }
  
  public final List B()
  {
    return m.bj().a();
  }
  
  public final FlashContact C()
  {
    Object localObject = m.I();
    String str1 = ((com.truecaller.common.g.a)localObject).a("profileFirstName");
    String str2 = ((com.truecaller.common.g.a)localObject).a("profileNumber");
    boolean bool = TextUtils.isEmpty(str1);
    if (!bool)
    {
      bool = TextUtils.isEmpty(str2);
      if (!bool)
      {
        localObject = ((com.truecaller.common.g.a)localObject).a("profileLastName");
        FlashContact localFlashContact = new com/truecaller/flashsdk/models/FlashContact;
        localFlashContact.<init>(str2, str1, (String)localObject);
        return localFlashContact;
      }
    }
    return null;
  }
  
  public final boolean D()
  {
    return m.ao().a("truecaller_sdk_sign_in_flow_17858").equalsIgnoreCase("Loader First");
  }
  
  public final boolean E()
  {
    return m.aF().r().a();
  }
  
  public final Intent a(Context paramContext)
  {
    return BusinessProfileOnboardingActivity.a(paramContext, true);
  }
  
  public final bp a()
  {
    bp localbp = m;
    String[] arrayOfString = new String[0];
    AssertionUtil.isNotNull(localbp, arrayOfString);
    return m;
  }
  
  public final void a(int paramInt, String paramString1, String paramString2)
  {
    if (paramInt != 0)
    {
      String str;
      List localList;
      Object localObject2;
      Object localObject3;
      switch (paramInt)
      {
      default: 
        break;
      case 5: 
        paramString2 = String.valueOf(paramString1);
        str = "+".concat(paramString2);
        localObject1 = com.truecaller.common.h.h.c(str);
        if (localObject1 != null)
        {
          localObject1 = c;
        }
        else
        {
          paramInt = 0;
          localObject1 = null;
        }
        localList = null;
        localObject2 = DetailsFragment.SourceType.External;
        boolean bool1 = true;
        boolean bool2 = true;
        int i1 = 10;
        localObject3 = this;
        localObject1 = DetailsFragment.a(this, null, null, str, paramString1, (String)localObject1, (DetailsFragment.SourceType)localObject2, bool1, bool2, i1).addFlags(268435456);
        int i2 = 536870912;
        localObject1 = ((Intent)localObject1).addFlags(i2);
        startActivity((Intent)localObject1);
        return;
      case 4: 
        localObject3 = m.P();
        paramString1 = String.valueOf(paramString1);
        localList = Collections.singletonList("+".concat(paramString1));
        localObject2 = TruecallerContract.Filters.WildCardType.NONE;
        TruecallerContract.Filters.EntityType localEntityType = TruecallerContract.Filters.EntityType.PERSON;
        str = paramString2;
        ((FilterManager)localObject3).a(localList, "PHONE_NUMBER", paramString2, "quickReply", false, (TruecallerContract.Filters.WildCardType)localObject2, localEntityType);
        return;
      }
      Object localObject1 = getSharedPreferences("callMeBackNotifications", 0).edit();
      long l1 = System.currentTimeMillis();
      ((SharedPreferences.Editor)localObject1).putLong(paramString1, l1).apply();
      return;
    }
    m.D().a("key_last_call_origin", "callMeBackNotification");
  }
  
  public final void a(Activity paramActivity)
  {
    boolean bool = com.truecaller.wizard.b.c.g();
    Object localObject1;
    Object localObject2;
    if (bool)
    {
      localObject1 = paramActivity.getPackageManager();
      localObject2 = new android/content/ComponentName;
      Class localClass = com.truecaller.ui.ah.class;
      ((ComponentName)localObject2).<init>(paramActivity, localClass);
      int i1 = 2;
      int i2 = 1;
      ((PackageManager)localObject1).setComponentEnabledSetting((ComponentName)localObject2, i1, i2);
      com.truecaller.wizard.b.c.h();
    }
    else
    {
      localObject1 = getPackageManager();
      localObject2 = getPackageName();
      localObject1 = ((PackageManager)localObject1).getLaunchIntentForPackage((String)localObject2);
      paramActivity.startActivity((Intent)localObject1);
    }
    paramActivity.finish();
  }
  
  public final void a(Flash paramFlash)
  {
    m.aY().a(paramFlash);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    boolean bool = isTcPayEnabled();
    if (bool) {
      TransactionActivity.startForRequest(this, paramString2, paramString4, paramString1, paramString3, paramString5);
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    com.truecaller.common.g.a locala = m.I();
    String str1 = locala.a("profileNumber");
    String str2 = locala.a("profileCountryIso");
    Object localObject1 = m.d().a();
    Object localObject2 = locala.a("profileAvatar");
    if (localObject2 != null) {
      com.truecaller.util.aw.a(this, (String)localObject2);
    }
    localObject2 = new com/truecaller/old/data/access/e;
    ((com.truecaller.old.data.access.e)localObject2).<init>(this);
    ((com.truecaller.old.data.access.e)localObject2).b();
    localObject2 = new com/truecaller/old/data/access/f;
    ((com.truecaller.old.data.access.f)localObject2).<init>(this);
    ((com.truecaller.old.data.access.f)localObject2).k();
    com.truecaller.common.b.e.a.edit().clear().apply();
    Settings.c();
    localObject2 = new com/truecaller/old/data/access/i;
    ((com.truecaller.old.data.access.i)localObject2).<init>(this);
    ((com.truecaller.old.data.access.i)localObject2).b();
    co.a(this);
    com.truecaller.ads.campaigns.f.a(this).b();
    Settings.b(this);
    localObject2 = m.be();
    ThemeManager.Theme localTheme = ThemeManager.Theme.DEFAULT;
    ((com.truecaller.flash.o)localObject2).a(localTheme);
    localObject2 = m.d();
    ((com.truecaller.analytics.a.a)localObject2).a((String)localObject1);
    localObject1 = m.cb();
    ((com.truecaller.voip.d)localObject1).b(str1);
    if (!paramBoolean)
    {
      localObject1 = "profileNumber";
      locala.a((String)localObject1, str1);
      str1 = "profileCountryIso";
      locala.a(str1, str2);
    }
    m.ce().b(this);
    super.a(paramBoolean);
  }
  
  public final boolean a(android.support.v4.app.j paramj)
  {
    boolean bool = Settings.a(this);
    if (bool)
    {
      com.truecaller.ui.dialogs.l locall = new com/truecaller/ui/dialogs/l;
      locall.<init>();
      locall.show(paramj, "QaDialog");
      return true;
    }
    return false;
  }
  
  public final boolean a(String paramString)
  {
    Object localObject1 = com.truecaller.common.b.a.F();
    boolean bool1 = ((com.truecaller.common.b.a)localObject1).p();
    boolean bool2 = true;
    if (!bool1) {
      return bool2;
    }
    localObject1 = (TelephonyManager)getSystemService("phone");
    String str;
    if (localObject1 != null)
    {
      str = ((TelephonyManager)localObject1).getNetworkCountryIso();
      localObject2 = Locale.ENGLISH;
      str = am.c(str, (Locale)localObject2);
      localObject1 = ((TelephonyManager)localObject1).getSimCountryIso();
      localObject2 = Locale.ENGLISH;
      localObject1 = am.c((String)localObject1, (Locale)localObject2);
    }
    else
    {
      bool1 = false;
      localObject1 = null;
      str = null;
    }
    Object localObject2 = m.P();
    localObject1 = (String)am.e(str, (CharSequence)localObject1);
    paramString = ah;
    localObject1 = FilterManager.FilterAction.FILTER_BLACKLISTED;
    if (paramString == localObject1) {
      return bool2;
    }
    return false;
  }
  
  public final boolean a(String paramString1, boolean paramBoolean, String paramString2)
  {
    return a(paramString1, paramBoolean, false, paramString2);
  }
  
  public final boolean a(String paramString1, boolean paramBoolean1, boolean paramBoolean2, String paramString2)
  {
    boolean bool1 = o();
    boolean bool2 = super.a(paramString1, paramBoolean1, paramString2);
    m.aw().e();
    Object localObject = com.truecaller.old.data.a.d.a;
    int i1 = localObject.length;
    int i2 = 0;
    while (i2 < i1)
    {
      int i3 = localObject[i2];
      com.truecaller.util.e.g localg = com.truecaller.util.e.g.a(this, i3);
      localg.b();
      i2 += 1;
    }
    localObject = m.ah();
    paramBoolean1 = ((com.truecaller.util.b)localObject).a();
    if (paramBoolean1) {
      if (!bool1)
      {
        paramBoolean1 = false;
        localObject = null;
        TruecallerInit.c(this, null);
      }
      else if (!paramBoolean2)
      {
        localObject = WizardActivity.class;
        com.truecaller.wizard.b.c.a(this, (Class)localObject);
      }
      else
      {
        localObject = WizardActivity.class;
        com.truecaller.wizard.b.c.b(this, (Class)localObject);
      }
    }
    return bool2;
  }
  
  protected void attachBaseContext(Context paramContext)
  {
    super.attachBaseContext(paramContext);
    android.support.multidex.a.a(this);
  }
  
  public final com.truecaller.common.profile.e b()
  {
    return m.bf();
  }
  
  public final boolean b(String paramString)
  {
    boolean bool = org.c.a.a.a.k.b(paramString);
    if (bool) {
      return Settings.g();
    }
    com.truecaller.flashsdk.core.b localb = m.aV();
    paramString = paramString.replace("+", "");
    return hc;
  }
  
  public final com.truecaller.common.f.c c()
  {
    return m.ai();
  }
  
  public void createShortcut(int paramInt)
  {
    a().aA().a(paramInt);
  }
  
  public final com.truecaller.common.f.b d()
  {
    return m.an();
  }
  
  public final com.truecaller.common.h.c e()
  {
    return m.aI();
  }
  
  public final com.truecaller.featuretoggles.e f()
  {
    return m.aF();
  }
  
  public void fetchTempToken(PayTempTokenCallBack paramPayTempTokenCallBack)
  {
    boolean bool = isTcPayEnabled();
    if (bool)
    {
      com.truecaller.androidactors.i locali = m.m().a();
      com.truecaller.androidactors.w localw = ((com.truecaller.payments.network.b)m.aR().a()).a();
      -..Lambda.TrueApp.UvHmEQe10W-5wmy6u2xWbg5DgN8 localUvHmEQe10W-5wmy6u2xWbg5DgN8 = new com/truecaller/-$$Lambda$TrueApp$UvHmEQe10W-5wmy6u2xWbg5DgN8;
      localUvHmEQe10W-5wmy6u2xWbg5DgN8.<init>(paramPayTempTokenCallBack);
      localw.a(locali, localUvHmEQe10W-5wmy6u2xWbg5DgN8);
    }
  }
  
  public final com.truecaller.content.d.a g()
  {
    return m.cl();
  }
  
  public Uri getTcPayNotificationTone()
  {
    return m.by().d();
  }
  
  public final Boolean h()
  {
    return Boolean.valueOf(Truepay.getInstance().isRegistrationComplete());
  }
  
  public final void i()
  {
    Object localObject2;
    try
    {
      Locale localLocale = Locale.getDefault();
      localObject2 = getResources();
      if (localObject2 != null)
      {
        localObject2 = ((Resources)localObject2).getConfiguration();
        if (localObject2 != null) {
          localLocale = locale;
        }
      }
      localObject2 = com.truecaller.old.data.access.d.d();
      if (localObject2 == null) {
        com.truecaller.old.data.access.d.b(localLocale);
      }
    }
    catch (RuntimeException localRuntimeException)
    {
      com.truecaller.log.d.a(localRuntimeException);
    }
    super.i();
    Settings.b(this);
    Object localObject1 = com.crashlytics.android.a.d();
    if (localObject1 != null)
    {
      localObject2 = n();
      com.crashlytics.android.a.a("language", (String)localObject2);
      localObject2 = a().aI().f();
      com.crashlytics.android.a.a("buildName", (String)localObject2);
      localObject2 = m.bx();
      int i1 = ((com.truecaller.utils.d)localObject2).o();
      com.crashlytics.android.a.a("googlePlayServicesVersion", i1);
      localObject1 = m.d().a();
      com.crashlytics.android.a.a((String)localObject1);
    }
  }
  
  public boolean isTcPayEnabled()
  {
    com.truecaller.featuretoggles.b localb = m.aF().n();
    boolean bool = localb.a();
    if (bool)
    {
      bool = p();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public void j()
  {
    super.j();
    m.ca().b();
    AlarmReceiver.a(this, false);
  }
  
  public final String k()
  {
    return "Truecaller";
  }
  
  public final String l()
  {
    return "10.41.6";
  }
  
  public void logTcPayCleverTapEvent(String paramString, Map paramMap)
  {
    m.bp().a(paramString, paramMap);
  }
  
  public void logTcPayEvent(String paramString1, String paramString2, Map paramMap1, Map paramMap2)
  {
    paramString1 = ao.b().a(paramMap1).b(paramMap2).b(paramString2).a(paramString1).a();
    ((com.truecaller.analytics.ae)m.f().a()).a(paramString1);
  }
  
  public void logTcPayFacebookEvent(String paramString, Map paramMap)
  {
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>(paramString);
    paramString = paramMap.entrySet().iterator();
    for (;;)
    {
      boolean bool = paramString.hasNext();
      if (!bool) {
        break;
      }
      paramMap = (Map.Entry)paramString.next();
      Object localObject = paramMap.getKey();
      if (localObject != null)
      {
        localObject = paramMap.getValue();
        if (localObject != null)
        {
          localObject = ((CharSequence)paramMap.getKey()).toString();
          Locale localLocale = Locale.ENGLISH;
          localObject = ((String)localObject).toLowerCase(localLocale);
          paramMap = ((CharSequence)paramMap.getValue()).toString();
          locala.a((String)localObject, paramMap);
        }
      }
    }
    paramString = d;
    paramMap = locala.a();
    paramString.a(paramMap);
  }
  
  public final boolean m()
  {
    return Settings.i();
  }
  
  public final String n()
  {
    try
    {
      Object localObject = getResources();
      localObject = ((Resources)localObject).getConfiguration();
      localObject = locale;
      return ((Locale)localObject).getLanguage();
    }
    catch (NullPointerException localNullPointerException) {}
    return Settings.b("language");
  }
  
  public final boolean o()
  {
    com.truecaller.i.c localc = m.D();
    String str = "hasNativeDialerCallerId";
    boolean bool = localc.b(str);
    return !bool;
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    Settings.c(this);
    paramConfiguration = getResources().getConfiguration();
    super.onConfigurationChanged(paramConfiguration);
  }
  
  public void onCreate()
  {
    int i1 = 1;
    com.truecaller.log.d.a = i1;
    try
    {
      localObject1 = new io.fabric.sdk.android.i[i1];
      localObject2 = new com/crashlytics/android/a;
      ((com.crashlytics.android.a)localObject2).<init>();
      localObject1[0] = localObject2;
      io.fabric.sdk.android.c.a(this, (io.fabric.sdk.android.i[])localObject1);
      localObject1 = Thread.getDefaultUncaughtExceptionHandler();
      localObject2 = new com/truecaller/common/b/g;
      ((com.truecaller.common.b.g)localObject2).<init>((Thread.UncaughtExceptionHandler)localObject1);
      Thread.setDefaultUncaughtExceptionHandler((Thread.UncaughtExceptionHandler)localObject2);
    }
    catch (RuntimeException localRuntimeException)
    {
      Object localObject1;
      Object localObject2;
      Object localObject3;
      Object localObject4;
      int i2;
      int i3;
      int i5;
      Object localObject5;
      Object localObject6;
      String str;
      boolean bool5;
      int i6;
      int i9;
      boolean bool6;
      for (;;) {}
    }
    com.google.firebase.b.a(this);
    localObject1 = e;
    localObject2 = a().T();
    localObject3 = b.c();
    localObject4 = a().aI().f();
    c.g.b.k.b(localObject2, "accountManager");
    c.g.b.k.b(localObject3, "coreSettings");
    c.g.b.k.b(localObject4, "buildName");
    b = ((r)localObject2);
    c = ((com.truecaller.common.g.a)localObject3);
    localObject4 = (CharSequence)localObject4;
    c.g.b.k.b(localObject4, "<set-?>");
    a = ((CharSequence)localObject4);
    localObject1 = m;
    localObject2 = com.truecaller.common.network.util.d.d;
    ((bp)localObject1).co();
    localObject1 = com.truecaller.android.truemoji.f.b;
    localObject1 = new com/truecaller/android/truemoji/b/i;
    ((com.truecaller.android.truemoji.b.i)localObject1).<init>();
    c.g.b.k.b(localObject1, "provider");
    com.truecaller.android.truemoji.f.a(com.truecaller.android.truemoji.f.c()).clear();
    localObject2 = com.truecaller.android.truemoji.f.c();
    localObject3 = ((com.truecaller.android.truemoji.i)localObject1).a();
    com.truecaller.android.truemoji.f.a((com.truecaller.android.truemoji.f)localObject2, (com.truecaller.android.truemoji.b[])localObject3);
    localObject2 = com.truecaller.android.truemoji.f.c();
    localObject3 = (f.e)com.truecaller.android.truemoji.f.d();
    com.truecaller.android.truemoji.f.a((com.truecaller.android.truemoji.f)localObject2, (f.e)localObject3);
    localObject2 = new java/util/ArrayList;
    i2 = 3000;
    ((ArrayList)localObject2).<init>(i2);
    localObject1 = ((com.truecaller.android.truemoji.i)localObject1).a();
    localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    localObject3 = (Collection)localObject3;
    i3 = localObject1.length;
    i5 = 0;
    localObject5 = null;
    while (i5 < i3)
    {
      localObject6 = localObject1[i5].a();
      str = "it.emojis";
      c.g.b.k.a(localObject6, str);
      localObject6 = (Iterable)c.a.f.f((Object[])localObject6);
      c.a.m.a((Collection)localObject3, (Iterable)localObject6);
      i5 += 1;
    }
    localObject1 = c.a.m.d((Collection)localObject3);
    localObject3 = localObject1;
    localObject3 = (Iterable)localObject1;
    localObject4 = new java/util/ArrayList;
    ((ArrayList)localObject4).<init>();
    localObject4 = (Collection)localObject4;
    localObject5 = ((Iterable)localObject3).iterator();
    for (;;)
    {
      boolean bool4 = ((Iterator)localObject5).hasNext();
      if (!bool4) {
        break;
      }
      localObject6 = (Emoji)((Iterator)localObject5).next();
      str = "it";
      c.g.b.k.a(localObject6, str);
      localObject6 = (Iterable)((Emoji)localObject6).b();
      c.a.m.a((Collection)localObject4, (Iterable)localObject6);
    }
    localObject4 = (Collection)localObject4;
    ((List)localObject1).addAll((Collection)localObject4);
    localObject1 = com.truecaller.android.truemoji.f.c();
    i3 = ag.a(c.a.m.a((Iterable)localObject3, 10));
    i5 = 16;
    i3 = c.k.i.c(i3, i5);
    localObject5 = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject5).<init>(i3);
    localObject5 = (Map)localObject5;
    localObject3 = ((Iterable)localObject3).iterator();
    for (;;)
    {
      boolean bool3 = ((Iterator)localObject3).hasNext();
      if (!bool3) {
        break;
      }
      localObject4 = (Emoji)((Iterator)localObject3).next();
      c.g.b.k.a(localObject4, "it");
      localObject6 = a;
      ((Map)localObject5).put(localObject6, localObject4);
    }
    localObject5 = (HashMap)localObject5;
    com.truecaller.android.truemoji.f.a((com.truecaller.android.truemoji.f)localObject1, (HashMap)localObject5);
    localObject1 = (Collection)com.truecaller.android.truemoji.f.a(com.truecaller.android.truemoji.f.c()).keySet();
    ((ArrayList)localObject2).addAll((Collection)localObject1);
    bool5 = ((ArrayList)localObject2).isEmpty();
    if (!bool5)
    {
      localObject2 = (Iterable)localObject2;
      localObject1 = com.truecaller.android.truemoji.f.e();
      localObject1 = c.a.m.a((Iterable)localObject2, (Comparator)localObject1);
      localObject2 = new java/lang/StringBuilder;
      i2 = 12000;
      ((StringBuilder)localObject2).<init>(i2);
      localObject1 = ((Iterable)localObject1).iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        localObject3 = Pattern.quote((String)((Iterator)localObject1).next());
        ((StringBuilder)localObject2).append((String)localObject3);
        char c = '|';
        ((StringBuilder)localObject2).append(c);
      }
      i6 = ((StringBuilder)localObject2).length() - i1;
      localObject1 = ((StringBuilder)localObject2).deleteCharAt(i6).toString();
      c.g.b.k.a(localObject1, "patternBuilder.deleteCha…er.length - 1).toString()");
      localObject2 = com.truecaller.android.truemoji.f.c();
      localObject3 = Pattern.compile((String)localObject1);
      com.truecaller.android.truemoji.f.a((com.truecaller.android.truemoji.f)localObject2, (Pattern)localObject3);
      localObject2 = com.truecaller.android.truemoji.f.c();
      localObject3 = new java/lang/StringBuilder;
      localObject4 = "(";
      ((StringBuilder)localObject3).<init>((String)localObject4);
      ((StringBuilder)localObject3).append((String)localObject1);
      ((StringBuilder)localObject3).append(")+");
      localObject1 = Pattern.compile(((StringBuilder)localObject3).toString());
      com.truecaller.android.truemoji.f.b((com.truecaller.android.truemoji.f)localObject2, (Pattern)localObject1);
      com.facebook.k.a(this);
      localObject1 = getString(2131886539);
      com.facebook.appevents.g.a(this, (String)localObject1);
      localObject1 = ai.a(this);
      com.truecaller.debug.log.a.a(this, (String)localObject1);
      localObject1 = g.c();
      d = ((com.truecaller.analytics.b)localObject1);
      localObject1 = new androidx/work/b$a;
      ((androidx.work.b.a)localObject1).<init>();
      d = 20000;
      e = 30000;
      i9 = 50;
      i9 = Math.min(i9, i9);
      f = i9;
      localObject2 = new androidx/work/b;
      ((androidx.work.b)localObject2).<init>((androidx.work.b.a)localObject1);
      androidx.work.impl.h.a(this, (androidx.work.b)localObject2);
      localObject1 = new com/truecaller/util/cm;
      localObject2 = getAssets();
      ((cm)localObject1).<init>((AssetManager)localObject2);
      com.google.c.a.k.a((com.google.c.a.c)localObject1);
      super.onCreate();
      localObject1 = m.t();
      localObject2 = m;
      ((bp)localObject2).bx();
      bool6 = ((al)localObject1).e();
      if (bool6)
      {
        localObject1 = m.aB();
        ((com.truecaller.notificationchannels.p)localObject1).b();
      }
      else
      {
        localObject1 = m.aB();
        ((com.truecaller.notificationchannels.p)localObject1).a();
      }
      localObject1 = TruecallerContract.a;
      if (localObject1 == null)
      {
        localObject1 = com.truecaller.common.c.b.b.a(this, TruecallerContentProvider.class);
        TruecallerContract.a((String)localObject1);
      }
      Settings.c(this);
      localObject1 = "qaServer";
      bool6 = Settings.e((String)localObject1);
      if (bool6) {
        KnownEndpoints.switchToStaging();
      }
      net.danlew.android.joda.a.a(this);
      localObject1 = new com/d/b/aj;
      ((aj)localObject1).<init>();
      localObject2 = new com/d/b/w$a;
      ((w.a)localObject2).<init>(this);
      localObject2 = ((w.a)localObject2).a((ac)localObject1);
      localObject3 = new com/truecaller/util/df;
      ((df)localObject3).<init>(this);
      localObject2 = ((w.a)localObject2).a((ac)localObject3);
      localObject3 = new com/c/a/a;
      localObject4 = com.truecaller.util.aw.a(this);
      ((com.c.a.a)localObject3).<init>((okhttp3.y)localObject4);
      localObject2 = ((w.a)localObject2).a((com.d.b.j)localObject3).a();
      com.d.b.w.a((com.d.b.w)localObject2);
      ((aj)localObject1).a((com.d.b.w)localObject2);
      CallStateService.a(this);
      com.truecaller.util.o.a(this);
    }
    try
    {
      localObject1 = new android/content/Intent;
      localObject2 = ClipboardService.class;
      ((Intent)localObject1).<init>(this, (Class)localObject2);
      startService((Intent)localObject1);
    }
    catch (IllegalStateException localIllegalStateException)
    {
      int i7;
      boolean bool7;
      int i4;
      int i8;
      boolean bool8;
      boolean bool1;
      Object localObject7;
      long l1;
      long l2;
      long l3;
      for (;;) {}
    }
    i7 = Build.VERSION.SDK_INT;
    i9 = 26;
    if (i7 >= i9) {
      RingerModeListenerWorker.b();
    }
    localObject1 = m.ah();
    registerActivityLifecycleCallbacks((Application.ActivityLifecycleCallbacks)localObject1);
    localObject1 = new com/truecaller/analytics/x;
    localObject2 = m.ag();
    ((com.truecaller.analytics.x)localObject1).<init>((com.truecaller.analytics.w)localObject2);
    registerActivityLifecycleCallbacks((Application.ActivityLifecycleCallbacks)localObject1);
    localObject1 = new com/truecaller/messaging/transport/im/aw;
    ((com.truecaller.messaging.transport.im.aw)localObject1).<init>();
    registerActivityLifecycleCallbacks((Application.ActivityLifecycleCallbacks)localObject1);
    localObject1 = getContentResolver();
    localObject2 = TruecallerContract.ah.a();
    localObject3 = new com/truecaller/data/b;
    ((com.truecaller.data.b)localObject3).<init>();
    ((ContentResolver)localObject1).registerContentObserver((Uri)localObject2, i1, (ContentObserver)localObject3);
    bool7 = p();
    if (bool7)
    {
      RefreshT9MappingService.b(this);
      localObject1 = new com/truecaller/service/RefreshContactIndexingService$a;
      ((RefreshContactIndexingService.a)localObject1).<init>(this);
      ((RefreshContactIndexingService.a)localObject1).a();
    }
    TagService.a(this);
    localObject1 = new android/content/IntentFilter;
    ((IntentFilter)localObject1).<init>("com.truecaller.ACTION_PERMISSIONS_CHANGED");
    ((IntentFilter)localObject1).addAction("com.truecaller.wizard.verification.action.PHONE_VERIFIED");
    localObject2 = android.support.v4.content.d.a(this);
    localObject3 = new com/truecaller/TrueApp$1;
    ((TrueApp.1)localObject3).<init>(this);
    ((android.support.v4.content.d)localObject2).a((BroadcastReceiver)localObject3, (IntentFilter)localObject1);
    bool7 = Settings.e() ^ i1;
    localObject2 = getPackageManager();
    localObject3 = new android/content/ComponentName;
    localObject4 = getPackageName();
    localObject5 = "com.truecaller.TruecallerInitAlias";
    ((ComponentName)localObject3).<init>((String)localObject4, (String)localObject5);
    i4 = 2;
    if (bool7) {
      i5 = 1;
    } else {
      i5 = 2;
    }
    ((PackageManager)localObject2).setComponentEnabledSetting((ComponentName)localObject3, i5, i1);
    localObject3 = new android/content/ComponentName;
    localObject5 = getPackageName();
    localObject6 = "com.truecaller.DialerActivityAlias";
    ((ComponentName)localObject3).<init>((String)localObject5, (String)localObject6);
    if (bool7) {
      bool7 = true;
    } else {
      i8 = 2;
    }
    ((PackageManager)localObject2).setComponentEnabledSetting((ComponentName)localObject3, i8, i1);
    localObject1 = com.truecaller.flashsdk.core.c.b;
    com.truecaller.flashsdk.core.c.a(this);
    localObject1 = m.be();
    localObject2 = ThemeManager.a();
    ((com.truecaller.flash.o)localObject1).a((ThemeManager.Theme)localObject2);
    localObject1 = m.aV();
    localObject2 = m.aW();
    ((com.truecaller.flashsdk.core.b)localObject1).a((com.truecaller.flashsdk.core.s)localObject2);
    m.aV().a(this);
    m.aV().b();
    localObject1 = n;
    localObject2 = new String[] { "com.truecaller.datamanager.STATUSES_CHANGED" };
    com.truecaller.utils.extensions.i.a(this, (BroadcastReceiver)localObject1, (String[])localObject2);
    i8 = 1041006;
    localObject2 = "VERSION_CODE";
    i9 = Settings.a((String)localObject2, 0);
    if (i8 == i9)
    {
      localObject1 = Build.VERSION.RELEASE;
      localObject2 = Settings.b("osVersion");
      bool8 = org.c.a.a.a.k.a((CharSequence)localObject1, (CharSequence)localObject2);
      if (bool8)
      {
        bool1 = false;
        localObject7 = null;
        break label1918;
      }
    }
    a().aB().c();
    localObject1 = m.aF().n();
    bool8 = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool8)
    {
      localObject1 = a().aC();
      ((com.truecaller.notificationchannels.e)localObject1).l();
    }
    else
    {
      localObject1 = a().aC();
      ((com.truecaller.notificationchannels.e)localObject1).m();
    }
    localObject1 = m.aF().A();
    bool8 = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool8)
    {
      localObject1 = m;
      ((bp)localObject1).bA();
    }
    m.aC().n();
    localObject1 = m.aF().t();
    bool8 = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool8)
    {
      localObject1 = m.aC();
      ((com.truecaller.notificationchannels.e)localObject1).p();
    }
    else
    {
      localObject1 = m.aC();
      ((com.truecaller.notificationchannels.e)localObject1).q();
    }
    localObject1 = m.j();
    bool8 = ((com.truecaller.engagementrewards.c)localObject1).b();
    if (bool8)
    {
      localObject1 = m.aC();
      ((com.truecaller.notificationchannels.e)localObject1).r();
    }
    else
    {
      localObject1 = m.aC();
      ((com.truecaller.notificationchannels.e)localObject1).s();
    }
    l1 = 1041006L;
    Settings.a("VERSION_CODE", l1);
    localObject2 = Build.VERSION.RELEASE;
    Settings.b("osVersion", (String)localObject2);
    localObject1 = this.c;
    ((com.truecaller.common.background.b)localObject1).a();
    AppSettingsTask.a((com.truecaller.common.background.b)localObject1);
    AppHeartBeatTask.a((com.truecaller.common.background.b)localObject1);
    ((com.truecaller.config.a)m.aZ().a()).b().c();
    localObject1 = a().F();
    localObject2 = "key_upgrade_timestamp";
    l2 = System.currentTimeMillis();
    ((com.truecaller.i.e)localObject1).b((String)localObject2, l2);
    label1918:
    localObject1 = (com.truecaller.presence.c)m.ae().a();
    localObject2 = AvailabilityTrigger.USER_ACTION;
    ((com.truecaller.presence.c)localObject1).a((AvailabilityTrigger)localObject2, false);
    localObject1 = m.aF().n();
    bool8 = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool8)
    {
      m.aC().l();
      Truepay.initialize(this);
      localObject1 = Truepay.getInstance();
      ((Truepay)localObject1).handleAppUpdate(bool1);
    }
    else
    {
      localObject1 = m.aC();
      ((com.truecaller.notificationchannels.e)localObject1).m();
    }
    localObject1 = m.aF().A();
    bool8 = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool8)
    {
      localObject1 = m;
      ((bp)localObject1).bA();
    }
    m.bp().a();
    localObject1 = m.bu();
    localObject2 = -..Lambda.TrueApp.lcucToFgVpCwHsQbhyCC-rdsEa4.INSTANCE;
    ((com.truecaller.tcpermissions.o)localObject1).a((c.g.a.b)localObject2);
    bool8 = E();
    if (bool8)
    {
      m.bO().a(this);
      localObject1 = Truepay.getInstance();
      localObject2 = m.bO();
      ((Truepay)localObject1).setCreditHelper((com.truecaller.truepay.c)localObject2);
    }
    ((com.truecaller.messaging.data.t)a().p().a()).b(i4);
    ((com.truecaller.messaging.data.t)a().p().a()).c(0);
    m.cb().b();
    localObject1 = m.cb();
    bool8 = ((com.truecaller.voip.d)localObject1).a();
    if (bool8)
    {
      localObject1 = m.aD();
      ((com.truecaller.notificationchannels.b)localObject1).k();
    }
    else
    {
      localObject1 = m.aD();
      ((com.truecaller.notificationchannels.b)localObject1).l();
    }
    m.ce().a(this);
    m.cg().a(bool1);
    localObject7 = m.cq();
    localObject1 = c.e();
    bool8 = ((com.truecaller.featuretoggles.b)localObject1).a();
    i9 = 5;
    if (bool8)
    {
      localObject1 = b;
      l3 = ((com.truecaller.messaging.h)localObject1).a();
      l2 = 0L;
      bool8 = l3 < l2;
      if (bool8)
      {
        localObject1 = b;
        bool8 = ((com.truecaller.messaging.h)localObject1).aa();
        if (!bool8)
        {
          localObject7 = (com.truecaller.messaging.data.t)a.a();
          localObject1 = an.a(Integer.valueOf(i9));
          ((com.truecaller.messaging.data.t)localObject7).a(false, (Set)localObject1);
        }
      }
    }
    else
    {
      localObject1 = b;
      bool8 = ((com.truecaller.messaging.h)localObject1).aa();
      if (bool8)
      {
        localObject7 = (com.truecaller.messaging.data.t)a.a();
        localObject1 = an.a(Integer.valueOf(i9));
        ((com.truecaller.messaging.data.t)localObject7).a(false, (Set)localObject1);
      }
    }
    return;
    localObject7 = new java/lang/IllegalArgumentException;
    ((IllegalArgumentException)localObject7).<init>("Your EmojiProvider must at least have one category with at least one emoji.");
    throw ((Throwable)localObject7);
  }
  
  public void onLowMemory()
  {
    super.onLowMemory();
    com.truecaller.util.aw.a();
  }
  
  public void onTrimMemory(int paramInt)
  {
    super.onTrimMemory(paramInt);
    Object localObject1 = m.ag();
    ((com.truecaller.analytics.w)localObject1).a(paramInt);
    int i1 = 60;
    if (paramInt >= i1)
    {
      com.truecaller.util.aw.a();
      localObject1 = m.aq();
      ((com.truecaller.ads.provider.f)localObject1).d();
    }
    i1 = 10;
    if (paramInt > i1)
    {
      Object localObject2 = com.truecaller.ads.i.a();
      paramInt = ((c.f)localObject2).a();
      if (paramInt != 0)
      {
        ((com.d.b.p)com.truecaller.ads.i.a().b()).a(-1);
        return;
      }
      localObject2 = new java/lang/AssertionError;
      localObject1 = "adsImageCache is not initialized";
      ((AssertionError)localObject2).<init>(localObject1);
      localObject2 = (Throwable)localObject2;
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject2);
    }
  }
  
  public final boolean p()
  {
    Object localObject = m.T();
    boolean bool1 = ((r)localObject).c();
    boolean bool2 = com.truecaller.wizard.b.c.e();
    int i1 = 1;
    boolean bool3;
    if ((bool1) && (bool2)) {
      bool3 = true;
    } else {
      bool3 = false;
    }
    if (!bool3)
    {
      boolean bool4 = o();
      if (bool4)
      {
        new String[1][0] = "***********************************************************";
        new String[1][0] = "    PROFILE IS NOT VALID";
        String[] arrayOfString = new String[i1];
        String str1 = "        - hasValidAccount=";
        localObject = String.valueOf(bool1);
        localObject = str1.concat((String)localObject);
        arrayOfString[0] = localObject;
        localObject = new String[i1];
        String str2 = "        - isWizardCompleted=";
        String str3 = String.valueOf(bool2);
        str3 = str2.concat(str3);
        localObject[0] = str3;
        localObject = "***********************************************************";
        new String[1][0] = localObject;
      }
    }
    return bool3;
  }
  
  public final boolean q()
  {
    boolean bool = o();
    if (!bool)
    {
      bool = p();
      if (!bool)
      {
        r localr = b.k();
        bool = localr.c();
        if (bool) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final com.truecaller.androidactors.f r()
  {
    return m.f();
  }
  
  public final com.truecaller.analytics.b s()
  {
    return d;
  }
  
  public final String t()
  {
    CountryListDto.a locala = com.truecaller.common.h.h.c(this);
    if (locala == null) {
      return null;
    }
    return a;
  }
  
  public final com.truecaller.common.a u()
  {
    return b;
  }
  
  public void updateCleverTapProfile(Map paramMap)
  {
    m.bp().a(paramMap);
  }
  
  public final com.truecaller.analytics.d v()
  {
    return g;
  }
  
  public final String w()
  {
    return "tc.settings";
  }
  
  public final com.truecaller.analytics.b z()
  {
    return d;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.TrueApp
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */