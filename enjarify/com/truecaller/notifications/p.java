package com.truecaller.notifications;

import com.truecaller.notificationchannels.b;
import com.truecaller.notificationchannels.n;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class p
  implements d
{
  private final Provider a;
  
  private p(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static b a(n paramn)
  {
    return (b)g.a(paramn.d(), "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static p a(Provider paramProvider)
  {
    p localp = new com/truecaller/notifications/p;
    localp.<init>(paramProvider);
    return localp;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */