package com.truecaller.notifications;

import android.content.Context;
import android.service.notification.StatusBarNotification;
import c.d.a.a;
import c.n;
import c.o.b;
import c.x;
import com.truecaller.common.h.t;
import com.truecaller.data.access.i;
import com.truecaller.data.entity.Contact;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ar;

final class al$a
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag d;
  
  al$a(al paramal, StatusBarNotification paramStatusBarNotification, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/notifications/al$a;
    al localal = b;
    StatusBarNotification localStatusBarNotification = c;
    locala.<init>(localal, localStatusBarNotification, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    int j = 1;
    boolean bool1;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label598;
      }
      long l = 500L;
      a = j;
      paramObject = ar.a(l, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = c;
    localObject1 = b.b;
    paramObject = am.a((StatusBarNotification)paramObject, (Context)localObject1);
    localObject1 = b.a;
    boolean bool2 = ((Stack)localObject1).isEmpty();
    if (bool2) {
      return x.a;
    }
    localObject1 = (ak)c.a.m.d((List)b.a);
    ak localak = (ak)b.a.pop();
    Object localObject2 = b.a;
    ((Stack)localObject2).clear();
    boolean bool3 = c.g.b.k.a(localak, paramObject) ^ j;
    if (bool3) {
      return x.a;
    }
    paramObject = "firstPostedNotification";
    c.g.b.k.a(localObject1, (String)paramObject);
    int k = c;
    int m = 2;
    if (k >= m)
    {
      k = c;
      if (k == j)
      {
        k = 1;
      }
      else
      {
        k = 0;
        paramObject = null;
      }
      if (k != 0) {
        k = 1;
      } else {
        k = 3;
      }
    }
    else
    {
      k = 2;
    }
    localObject2 = b;
    localObject1 = a;
    Object localObject3 = c;
    int n = 100;
    Object localObject4 = Integer.valueOf(n);
    localObject3 = (Iterable)((i)localObject3).a((String)localObject1, (Integer)localObject4);
    localObject4 = new java/util/ArrayList;
    ((ArrayList)localObject4).<init>();
    localObject4 = (Collection)localObject4;
    localObject3 = ((Iterable)localObject3).iterator();
    for (;;)
    {
      boolean bool4 = ((Iterator)localObject3).hasNext();
      if (!bool4) {
        break;
      }
      Object localObject5 = ((Iterator)localObject3).next();
      Object localObject6 = localObject5;
      localObject6 = (String)b;
      boolean bool5 = c.g.b.k.a(localObject6, localObject1);
      if (bool5) {
        ((Collection)localObject4).add(localObject5);
      }
    }
    localObject4 = (List)localObject4;
    int i1 = ((List)localObject4).size();
    if (i1 <= j)
    {
      i1 = ((List)localObject4).size();
      if (i1 == j)
      {
        localObject1 = (Contact)da;
        break label550;
      }
      localObject1 = f.a((String)localObject1);
      c.g.b.k.a(localObject1, "phoneNumbers");
      localObject3 = localObject1;
      localObject3 = (Collection)localObject1;
      int i2 = ((Collection)localObject3).isEmpty();
      j ^= i2;
      if (j != 0)
      {
        localObject7 = b;
        localObject1 = (String)c.a.m.d((List)localObject1);
        localObject1 = al.a((Context)localObject7, (String)localObject1);
        break label550;
      }
    }
    bool2 = false;
    localObject1 = null;
    label550:
    Object localObject7 = d;
    localObject1 = ((com.truecaller.data.access.c)localObject7).a((Contact)localObject1);
    if (localObject1 != null)
    {
      localObject7 = b.e;
      bool1 = b;
      ((com.truecaller.calling.e.c)localObject7).a(k, bool1, (Contact)localObject1);
    }
    return x.a;
    label598:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.al.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */