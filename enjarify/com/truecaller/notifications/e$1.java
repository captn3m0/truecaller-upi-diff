package com.truecaller.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.util.Iterator;
import java.util.LinkedHashSet;

final class e$1
  extends BroadcastReceiver
{
  e$1(e parame) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    paramContext = paramIntent.getLongArrayExtra("ids");
    int i = 0;
    paramIntent = null;
    if (paramContext != null)
    {
      int j = paramContext.length;
      if (j > 0)
      {
        Iterator localIterator = a.b.iterator();
        int k = 1;
        int m = 0;
        Object localObject1 = null;
        label250:
        for (;;)
        {
          boolean bool1 = localIterator.hasNext();
          if (!bool1) {
            break;
          }
          Object localObject2 = (SourcedContact)localIterator.next();
          int n = paramContext.length;
          int i1 = 0;
          String str = null;
          for (;;)
          {
            if (i1 >= n) {
              break label250;
            }
            long l1 = paramContext[i1];
            Long localLong = c;
            if (localLong != null)
            {
              long l2 = localLong.longValue();
              boolean bool2 = l2 < l1;
              if (!bool2)
              {
                localObject1 = a;
                Object localObject3 = f;
                localObject1 = a.edit();
                long l3 = System.currentTimeMillis();
                ((SharedPreferences.Editor)localObject1).putLong((String)localObject3, l3).apply();
                localObject1 = new String[k];
                localObject3 = new java/lang/StringBuilder;
                str = "A notification containing ";
                ((StringBuilder)localObject3).<init>(str);
                localObject2 = e;
                ((StringBuilder)localObject3).append((String)localObject2);
                ((StringBuilder)localObject3).append(" was removed");
                localObject2 = ((StringBuilder)localObject3).toString();
                localObject1[0] = localObject2;
                localIterator.remove();
                m = 1;
                break;
              }
            }
            i1 += 1;
          }
        }
        i = m;
      }
    }
    if (i != 0)
    {
      paramContext = a;
      e.a(paramContext);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.e.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */