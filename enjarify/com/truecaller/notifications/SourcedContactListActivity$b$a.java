package com.truecaller.notifications;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.ui.components.AvatarView;

final class SourcedContactListActivity$b$a
{
  final AvatarView a;
  final ImageView b;
  final TextView c;
  final TextView d;
  
  SourcedContactListActivity$b$a(View paramView)
  {
    Object localObject = (AvatarView)paramView.findViewById(2131362068);
    a = ((AvatarView)localObject);
    localObject = (ImageView)paramView.findViewById(2131362036);
    b = ((ImageView)localObject);
    localObject = (TextView)paramView.findViewById(2131362544);
    c = ((TextView)localObject);
    paramView = (TextView)paramView.findViewById(2131362032);
    d = paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.SourcedContactListActivity.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */