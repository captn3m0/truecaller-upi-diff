package com.truecaller.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;

public final class AnalyticsNotificationReceiver
  extends BroadcastReceiver
{
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    String str = "context";
    k.b(paramContext, str);
    if (paramIntent == null) {
      return;
    }
    paramContext = paramContext.getApplicationContext();
    if (paramContext != null)
    {
      ((bk)paramContext).a().W().a(paramIntent);
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.AnalyticsNotificationReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */