package com.truecaller.notifications;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.d.b.ab;
import com.d.b.w;
import com.d.b.w.a;
import com.truecaller.ui.components.AvatarView;
import com.truecaller.ui.components.b;
import java.util.List;

final class SourcedContactListActivity$b
  extends ArrayAdapter
{
  private final w a;
  
  SourcedContactListActivity$b(Context paramContext, List paramList)
  {
    super(paramContext, 0, paramList);
    paramList = new com/d/b/w$a;
    paramList.<init>(paramContext);
    SourcedContactListActivity.a locala = new com/truecaller/notifications/SourcedContactListActivity$a;
    locala.<init>(paramContext);
    paramContext = paramList.a(locala).a();
    a = paramContext;
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    w localw = null;
    if (paramView == null)
    {
      paramView = LayoutInflater.from(getContext());
      int i = 2131559036;
      paramView = paramView.inflate(i, paramViewGroup, false);
      paramViewGroup = new com/truecaller/notifications/SourcedContactListActivity$b$a;
      paramViewGroup.<init>(paramView);
      paramView.setTag(paramViewGroup);
    }
    Object localObject1 = (SourcedContact)getItem(paramInt);
    paramViewGroup = (SourcedContactListActivity.b.a)paramView.getTag();
    Object localObject2 = c;
    Object localObject3 = e;
    ((TextView)localObject2).setText((CharSequence)localObject3);
    localObject2 = d;
    localObject3 = getContext();
    Object[] arrayOfObject = new Object[2];
    String str1 = b;
    arrayOfObject[0] = str1;
    String str2 = f;
    arrayOfObject[1] = str2;
    localObject3 = ((Context)localObject3).getString(2131886524, arrayOfObject);
    ((TextView)localObject2).setText((CharSequence)localObject3);
    localObject2 = new com/truecaller/ui/components/b;
    localObject3 = h;
    Uri localUri = g;
    ((b)localObject2).<init>((Uri)localObject3, localUri, false, false);
    a.a((b)localObject2);
    localw = a;
    localObject2 = b;
    localw.d(localObject2);
    localw = a;
    localObject1 = String.valueOf(a);
    localObject1 = Uri.parse("appicon:".concat((String)localObject1));
    localObject1 = localw.a((Uri)localObject1);
    paramViewGroup = b;
    ((ab)localObject1).a(paramViewGroup, null);
    return paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.SourcedContactListActivity.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */