package com.truecaller.notifications;

public enum RegistrationNudgeTask$TaskState
{
  private final long interval;
  private final int text;
  private final int title;
  
  static
  {
    TaskState[] arrayOfTaskState = new TaskState[5];
    TaskState localTaskState1 = new com/truecaller/notifications/RegistrationNudgeTask$TaskState;
    TaskState localTaskState2 = localTaskState1;
    localTaskState1.<init>("INIT", 0, 0L, 2131886635, 2131886634);
    INIT = localTaskState1;
    arrayOfTaskState[0] = localTaskState1;
    localTaskState2 = new com/truecaller/notifications/RegistrationNudgeTask$TaskState;
    localTaskState2.<init>("FIRST", 1, 3600L, 2131886635, 2131886634);
    FIRST = localTaskState2;
    arrayOfTaskState[1] = localTaskState2;
    localTaskState2 = new com/truecaller/notifications/RegistrationNudgeTask$TaskState;
    localTaskState2.<init>("SECOND", 2, 86400L, 2131886637, 2131886636);
    SECOND = localTaskState2;
    arrayOfTaskState[2] = localTaskState2;
    localTaskState2 = new com/truecaller/notifications/RegistrationNudgeTask$TaskState;
    localTaskState2.<init>("THIRD", 3, 604800L, 2131886639, 2131886638);
    THIRD = localTaskState2;
    arrayOfTaskState[3] = localTaskState2;
    localTaskState2 = new com/truecaller/notifications/RegistrationNudgeTask$TaskState;
    localTaskState2.<init>("DONE", 4, -1, 2131886635, 2131886634);
    DONE = localTaskState2;
    arrayOfTaskState[4] = localTaskState2;
    $VALUES = arrayOfTaskState;
  }
  
  private RegistrationNudgeTask$TaskState(long paramLong, int paramInt2, int paramInt3)
  {
    interval = paramLong;
    title = paramInt2;
    text = paramInt3;
  }
  
  public final long getInterval()
  {
    return interval;
  }
  
  public final int getText()
  {
    return text;
  }
  
  public final int getTitle()
  {
    return title;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.RegistrationNudgeTask.TaskState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */