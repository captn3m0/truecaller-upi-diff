package com.truecaller.notifications;

import android.app.Notification;
import android.content.ComponentName;
import android.content.Context;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.utils.h;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import me.leolin.shortcutbadger.a;
import me.leolin.shortcutbadger.b;
import me.leolin.shortcutbadger.c;

public final class af
  implements ae
{
  private final a a;
  
  public af(Context paramContext)
  {
    String[] arrayOfString = null;
    Object localObject1 = c.class;
    Object localObject2 = "a";
    Object localObject3;
    try
    {
      localObject1 = ((Class)localObject1).getDeclaredField((String)localObject2);
      boolean bool = true;
      ((Field)localObject1).setAccessible(bool);
      localObject3 = ((Field)localObject1).get(null);
      localObject3 = (a)localObject3;
      if (localObject3 == null)
      {
        Object localObject4 = c.class;
        String str = "a";
        try
        {
          Class[] arrayOfClass = new Class[bool];
          Class localClass = Context.class;
          arrayOfClass[0] = localClass;
          localObject4 = ((Class)localObject4).getDeclaredMethod(str, arrayOfClass);
          ((Method)localObject4).setAccessible(bool);
          localObject2 = new Object[bool];
          localObject2[0] = paramContext;
          ((Method)localObject4).invoke(null, (Object[])localObject2);
          paramContext = ((Field)localObject1).get(null);
          paramContext = (a)paramContext;
        }
        catch (InvocationTargetException paramContext)
        {
          break label155;
        }
        catch (NoSuchMethodException paramContext)
        {
          break label177;
        }
        catch (IllegalAccessException paramContext)
        {
          break label199;
        }
        catch (NoSuchFieldException paramContext)
        {
          break label221;
        }
      }
      else
      {
        paramContext = (Context)localObject3;
      }
      localObject3 = paramContext;
    }
    catch (InvocationTargetException paramContext)
    {
      localObject3 = null;
      arrayOfString = new String[] { "Could not access badger init method" };
      AssertionUtil.OnlyInDebug.shouldNeverHappen(paramContext, arrayOfString);
    }
    catch (NoSuchMethodException paramContext)
    {
      localObject3 = null;
      arrayOfString = new String[] { "Could not access badger init method" };
      AssertionUtil.OnlyInDebug.shouldNeverHappen(paramContext, arrayOfString);
    }
    catch (IllegalAccessException paramContext)
    {
      localObject3 = null;
      arrayOfString = new String[] { "Could not access badger" };
      AssertionUtil.OnlyInDebug.shouldNeverHappen(paramContext, arrayOfString);
    }
    catch (NoSuchFieldException paramContext)
    {
      label155:
      label177:
      label199:
      localObject3 = null;
      label221:
      arrayOfString = new String[] { "Could not access badger" };
      AssertionUtil.OnlyInDebug.shouldNeverHappen(paramContext, arrayOfString);
    }
    a = ((a)localObject3);
  }
  
  public static void a(Context paramContext, Notification paramNotification, String paramString, int paramInt)
  {
    boolean bool = h.a();
    if ((bool) && (paramNotification != null)) {
      c.a(paramString, paramContext, paramNotification, paramInt);
    }
  }
  
  public final void a(Context paramContext, Class paramClass, int paramInt)
  {
    a locala = a;
    if (locala != null) {
      try
      {
        ComponentName localComponentName = new android/content/ComponentName;
        localComponentName.<init>(paramContext, paramClass);
        locala.a(paramContext, localComponentName, paramInt);
        return;
      }
      catch (Exception paramContext)
      {
        AssertionUtil.reportThrowableButNeverCrash(paramContext);
      }
      catch (b localb) {}
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.af
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */