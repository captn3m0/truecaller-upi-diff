package com.truecaller.notifications;

import android.content.Context;
import android.os.Bundle;
import com.truecaller.common.b.a;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.background.e;
import com.truecaller.common.background.e.a;
import com.truecaller.util.NotificationUtil;
import java.util.concurrent.TimeUnit;

public class NotificationUpdateTask
  extends PersistentBackgroundTask
{
  public final int a()
  {
    return 10008;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    paramBundle = (a)paramContext.getApplicationContext();
    boolean bool = paramBundle.p();
    if (bool)
    {
      NotificationUtil.b(paramContext);
      return PersistentBackgroundTask.RunResult.Success;
    }
    return PersistentBackgroundTask.RunResult.FailedSkip;
  }
  
  public final boolean a(Context paramContext)
  {
    return e(paramContext);
  }
  
  public final e b()
  {
    e.a locala = new com/truecaller/common/background/e$a;
    int i = 1;
    locala.<init>(i);
    TimeUnit localTimeUnit = TimeUnit.HOURS;
    locala = locala.a(6, localTimeUnit);
    localTimeUnit = TimeUnit.HOURS;
    long l = 1L;
    locala = locala.b(l, localTimeUnit);
    localTimeUnit = TimeUnit.HOURS;
    locala = locala.c(l, localTimeUnit);
    b = i;
    c = false;
    return locala.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.NotificationUpdateTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */