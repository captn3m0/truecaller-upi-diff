package com.truecaller.notifications;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.app.af;
import android.support.v4.app.z.b;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.g;
import android.webkit.URLUtil;
import com.truecaller.TrueApp;
import com.truecaller.analytics.e.a;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.am;
import com.truecaller.network.notification.NotificationType;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.util.ap;

public class GenericWebViewNotificationService
  extends af
{
  private static final String j = "com.truecaller.intent.action.CUSTOM_WEB_VIEW_NOTIFICATION_RECEIVED";
  private static final String k = "com.truecaller.intent.action.WEB_VIEW_NOTIFICATION_DISMISSED";
  private static final String l = "com.truecaller.intent.action.DEEPLINK_NOTIFICATION_RECEIVED";
  private static final String m = "com.truecaller.intent.action.DEEPLINK_NOTIFICATION_DISMISSED";
  private com.truecaller.analytics.b n;
  private final ah o;
  
  public GenericWebViewNotificationService()
  {
    ah localah = TrueApp.y().a().aK();
    o = localah;
  }
  
  private PendingIntent a(NotificationType paramNotificationType, Uri paramUri, String paramString)
  {
    Object localObject1 = NotificationType.GENERIC_WEBVIEW;
    Object localObject3;
    if (paramNotificationType == localObject1)
    {
      localObject1 = new android/content/Intent;
      Object localObject2 = GenericWebViewNotificationService.class;
      ((Intent)localObject1).<init>("com.truecaller.intent.action.CUSTOM_WEB_VIEW_DISPLAY", null, this, (Class)localObject2);
      localObject3 = o;
      paramUri = paramUri.toString();
      paramUri = ((ah)localObject3).b(paramUri);
      if (paramUri == null)
      {
        paramUri = n;
        localObject1 = new com/truecaller/analytics/e$a;
        ((e.a)localObject1).<init>("ANDROID_webview_notification_no_html");
        localObject3 = "campaign";
        localObject2 = am.a(paramString);
        localObject1 = ((e.a)localObject1).a((String)localObject3, (String)localObject2).a();
        paramUri.a((com.truecaller.analytics.e)localObject1);
        localObject1 = null;
      }
      else
      {
        localObject3 = "HTML_PAGE";
        localObject2 = o;
        paramUri = ((ah)localObject2).a(paramUri);
        ((Intent)localObject1).putExtra((String)localObject3, paramUri);
      }
      if (localObject1 == null) {
        return null;
      }
    }
    else
    {
      localObject1 = new android/content/Intent;
      ((Intent)localObject1).<init>();
      localObject3 = "android.intent.action.VIEW";
      ((Intent)localObject1).setAction((String)localObject3);
      ((Intent)localObject1).setData(paramUri);
    }
    ((Intent)localObject1).putExtra("CAMPAIGN_ID", paramString);
    int i = 268468224;
    ((Intent)localObject1).addFlags(i);
    paramUri = NotificationType.GENERIC_WEBVIEW;
    paramString = null;
    if (paramNotificationType == paramUri) {
      return PendingIntent.getService(this, 0, (Intent)localObject1, 134217728);
    }
    return PendingIntent.getActivity(this, 0, (Intent)localObject1, 0);
  }
  
  public static void a(Context paramContext, Notification paramNotification)
  {
    int i = 1;
    Object localObject1 = new String[i];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("onNotificationReceived() called with: context = [");
    ((StringBuilder)localObject2).append(paramContext);
    ((StringBuilder)localObject2).append("], notification = [");
    ((StringBuilder)localObject2).append(paramNotification);
    ((StringBuilder)localObject2).append("]");
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    localObject1 = new android/content/Intent;
    ((Intent)localObject1).<init>(paramContext, GenericWebViewNotificationService.class);
    localObject2 = paramNotification.b();
    Object localObject3 = NotificationType.GENERIC_WEBVIEW;
    if (localObject2 == localObject3) {
      localObject2 = j;
    } else {
      localObject2 = l;
    }
    ((Intent)localObject1).setAction((String)localObject2);
    localObject3 = paramNotification.a("u");
    ((Intent)localObject1).putExtra("PAGE_URL", (String)localObject3);
    localObject3 = paramNotification.a(paramContext);
    ((Intent)localObject1).putExtra("TITLE", (String)localObject3);
    localObject3 = paramNotification.b(paramContext);
    ((Intent)localObject1).putExtra("SUBTITLE", (String)localObject3);
    localObject3 = paramNotification.a("i");
    ((Intent)localObject1).putExtra("IMAGE_PAGE", (String)localObject3);
    localObject3 = paramNotification.a("ci");
    ((Intent)localObject1).putExtra("CAMPAIGN_ID", (String)localObject3);
    paramNotification = paramNotification.a("bi");
    ((Intent)localObject1).putExtra("BIG_IMAGE", paramNotification);
    a(paramContext, GenericWebViewNotificationService.class, 2131363174, (Intent)localObject1);
  }
  
  private void a(Intent paramIntent, NotificationType paramNotificationType)
  {
    Object localObject1 = Uri.parse(paramIntent.getStringExtra("PAGE_URL"));
    Object localObject2 = o;
    Object localObject3 = paramIntent.getStringExtra("TITLE");
    localObject2 = ((ah)localObject2).a((String)localObject3);
    localObject3 = o;
    Object localObject4 = paramIntent.getStringExtra("SUBTITLE");
    localObject3 = ((ah)localObject3).a((String)localObject4);
    localObject4 = paramIntent.getStringExtra("IMAGE_PAGE");
    Object localObject5 = paramIntent.getStringExtra("CAMPAIGN_ID");
    Object localObject6 = paramIntent.getStringExtra("BIG_IMAGE");
    boolean bool = am.b((CharSequence)localObject3);
    if (!bool)
    {
      bool = am.b((CharSequence)localObject2);
      if (!bool)
      {
        bool = a((Uri)localObject1);
        if (bool)
        {
          Object localObject7 = new android/content/Intent;
          Object localObject8 = NotificationType.GENERIC_WEBVIEW;
          if (paramNotificationType == localObject8) {
            localObject8 = k;
          } else {
            localObject8 = m;
          }
          Bitmap localBitmap = null;
          ((Intent)localObject7).<init>((String)localObject8, null, this, GenericWebViewNotificationService.class);
          localObject8 = "CAMPAIGN_ID";
          paramIntent = paramIntent.getStringExtra("CAMPAIGN_ID");
          ((Intent)localObject7).putExtra((String)localObject8, paramIntent);
          paramIntent = getApplicationContext();
          int i = 134217728;
          paramIntent = PendingIntent.getService(paramIntent, 0, (Intent)localObject7, i);
          paramNotificationType = a(paramNotificationType, (Uri)localObject1, (String)localObject5);
          if (paramNotificationType == null) {
            return;
          }
          if (localObject4 != null) {
            localObject1 = ap.a(getApplicationContext(), (String)localObject4, false);
          } else {
            localObject1 = null;
          }
          if (localObject6 != null)
          {
            localObject4 = getApplicationContext();
            localBitmap = ap.a((Context)localObject4, (String)localObject6, false);
          }
          if (localBitmap != null)
          {
            localObject4 = new android/support/v4/app/z$b;
            ((z.b)localObject4).<init>();
            a = localBitmap;
          }
          else
          {
            localObject4 = new android/support/v4/app/z$c;
            ((z.c)localObject4).<init>();
            localObject4 = ((z.c)localObject4).b((CharSequence)localObject3);
          }
          localObject5 = ((bk)getApplicationContext()).a();
          localObject6 = ((bp)localObject5).aC();
          localObject7 = new android/support/v4/app/z$d;
          localObject6 = ((com.truecaller.notificationchannels.e)localObject6).a();
          ((z.d)localObject7).<init>(this, (String)localObject6);
          localObject1 = ((z.d)localObject7).a((CharSequence)localObject2).b((CharSequence)localObject3).a((Bitmap)localObject1).a((z.g)localObject4);
          int i1 = android.support.v4.content.b.c(this, 2131100594);
          C = i1;
          localObject1 = ((z.d)localObject1).c(-1).a(2131234787);
          f = paramNotificationType;
          paramIntent = ((z.d)localObject1).b(paramIntent);
          paramIntent.d(16);
          paramNotificationType = ((bp)localObject5).W();
          paramIntent = paramIntent.h();
          paramNotificationType.a(2131362649, paramIntent, "notificationGenericWebView");
          return;
        }
      }
    }
    new String[1][0] = "Invalid notification message.";
  }
  
  private void a(String paramString, Intent paramIntent)
  {
    com.truecaller.analytics.b localb = n;
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>(paramString);
    paramIntent = am.a(paramIntent.getStringExtra("CAMPAIGN_ID"));
    paramString = locala.a("campaign", paramIntent).a();
    localb.a(paramString);
  }
  
  private boolean a(Uri paramUri)
  {
    if (paramUri != null)
    {
      Object localObject = paramUri.toString();
      boolean bool1 = URLUtil.isValidUrl((String)localObject);
      if (!bool1)
      {
        localObject = o;
        boolean bool2 = ((ah)localObject).a(paramUri);
        if (!bool2) {}
      }
      else
      {
        return true;
      }
    }
    return false;
  }
  
  public final void a(Intent paramIntent)
  {
    Object localObject1 = paramIntent.getAction();
    int i = ((String)localObject1).hashCode();
    Object localObject2;
    boolean bool2;
    switch (i)
    {
    default: 
      break;
    case 2019924160: 
      localObject2 = "com.truecaller.intent.action.DEEPLINK_NOTIFICATION_DISMISSED";
      boolean bool1 = ((String)localObject1).equals(localObject2);
      if (bool1) {
        int i1 = 4;
      }
      break;
    case 1568820902: 
      localObject2 = "com.truecaller.intent.action.CUSTOM_WEB_VIEW_NOTIFICATION_RECEIVED";
      bool2 = ((String)localObject1).equals(localObject2);
      if (bool2)
      {
        bool2 = false;
        localObject1 = null;
      }
      break;
    case 1468052170: 
      localObject2 = "com.truecaller.intent.action.DEEPLINK_NOTIFICATION_RECEIVED";
      bool2 = ((String)localObject1).equals(localObject2);
      if (bool2) {
        int i2 = 3;
      }
      break;
    case 226941846: 
      localObject2 = "com.truecaller.intent.action.WEB_VIEW_NOTIFICATION_DISMISSED";
      boolean bool3 = ((String)localObject1).equals(localObject2);
      if (bool3) {
        int i3 = 2;
      }
      break;
    case -560149933: 
      localObject2 = "com.truecaller.intent.action.CUSTOM_WEB_VIEW_DISPLAY";
      boolean bool4 = ((String)localObject1).equals(localObject2);
      if (bool4) {
        bool4 = true;
      }
      break;
    }
    int i4 = -1;
    switch (i4)
    {
    default: 
      break;
    case 4: 
      localObject1 = "ANDROID_deeplink_notification_canceled";
      a((String)localObject1, paramIntent);
      break;
    case 3: 
      a("ANDROID_deeplink_notification_received", paramIntent);
      localObject1 = NotificationType.GENERIC_DEEPLINK;
      a(paramIntent, (NotificationType)localObject1);
      return;
    case 2: 
      a("ANDROID_webview_notification_canceled", paramIntent);
      return;
    case 1: 
      a("ANDROID_webview_notification_shown", paramIntent);
      localObject1 = new android/content/Intent;
      localObject2 = getApplicationContext();
      ((Intent)localObject1).<init>((Context)localObject2, TruecallerInit.class);
      paramIntent = paramIntent.getStringExtra("HTML_PAGE");
      ((Intent)localObject1).putExtra("HTML_PAGE", paramIntent);
      ((Intent)localObject1).setAction("com.truecaller.intent.action.CUSTOM_WEB_VIEW_MAIN_DISPLAY");
      ((Intent)localObject1).addFlags(268468224);
      startActivity((Intent)localObject1);
      return;
    case 0: 
      a("ANDROID_webview_notification_received", paramIntent);
      localObject1 = NotificationType.GENERIC_WEBVIEW;
      a(paramIntent, (NotificationType)localObject1);
      return;
    }
  }
  
  public void onCreate()
  {
    super.onCreate();
    com.truecaller.analytics.b localb = TrueApp.y().a().c();
    n = localb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.GenericWebViewNotificationService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */