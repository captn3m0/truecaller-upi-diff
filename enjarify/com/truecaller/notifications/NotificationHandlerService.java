package com.truecaller.notifications;

import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.log.d;
import com.truecaller.presence.AvailabilityTrigger;
import com.truecaller.service.MissedCallsNotificationService;
import com.truecaller.util.al;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

public class NotificationHandlerService
  extends NotificationListenerService
{
  public static final Set a;
  private static final Collection b;
  private static final Collection c;
  private static final Collection d;
  private static int e;
  private Set f;
  private Looper g;
  private Handler h;
  private com.truecaller.i.c i;
  private al j;
  
  static
  {
    String[] tmp5_2 = new String[7];
    String[] tmp6_5 = tmp5_2;
    String[] tmp6_5 = tmp5_2;
    tmp6_5[0] = "com.android.server.telecom";
    tmp6_5[1] = "com.android.phone";
    String[] tmp15_6 = tmp6_5;
    String[] tmp15_6 = tmp6_5;
    tmp15_6[2] = "com.google.android.dialer";
    tmp15_6[3] = "com.android.dialer";
    String[] tmp24_15 = tmp15_6;
    String[] tmp24_15 = tmp15_6;
    tmp24_15[4] = "com.android.contacts";
    tmp24_15[5] = "com.samsung.android.contacts";
    tmp24_15[6] = "com.samsung.android.dialer";
    b = Arrays.asList(tmp24_15);
    Object localObject1 = new Integer[3];
    int k = 1;
    Integer localInteger = Integer.valueOf(k);
    localObject1[0] = localInteger;
    localInteger = Integer.valueOf(6001);
    localObject1[k] = localInteger;
    Object localObject2 = Integer.valueOf(10001);
    localObject1[2] = localObject2;
    c = Arrays.asList((Object[])localObject1);
    d = Arrays.asList(new String[] { "missedcall", "missed_call" });
    e = 0;
    localObject1 = new java/util/HashSet;
    String[] tmp121_118 = new String[4];
    String[] tmp122_121 = tmp121_118;
    String[] tmp122_121 = tmp121_118;
    tmp122_121[0] = "com.whatsapp";
    tmp122_121[1] = "com.viber.voip";
    tmp122_121[2] = "jp.naver.line.android";
    String[] tmp135_122 = tmp122_121;
    tmp135_122[3] = "org.telegram.messenger";
    localObject2 = Arrays.asList(tmp135_122);
    ((HashSet)localObject1).<init>((Collection)localObject2);
    a = Collections.unmodifiableSet((Set)localObject1);
  }
  
  public static int a()
  {
    return e;
  }
  
  private boolean a(StatusBarNotification paramStatusBarNotification)
  {
    boolean bool1 = paramStatusBarNotification.isClearable();
    String str1 = null;
    if (!bool1) {
      return false;
    }
    Object localObject1 = b;
    String str2 = paramStatusBarNotification.getPackageName();
    bool1 = ((Collection)localObject1).contains(str2);
    if (!bool1) {
      return false;
    }
    localObject1 = i;
    str2 = "showMissedCallsNotifications";
    bool1 = ((com.truecaller.i.c)localObject1).b(str2);
    if (!bool1) {
      return false;
    }
    localObject1 = paramStatusBarNotification.getTag();
    boolean bool2 = true;
    if (localObject1 != null)
    {
      localObject1 = paramStatusBarNotification.getTag();
      localObject2 = Locale.ROOT;
      localObject1 = ((String)localObject1).toLowerCase((Locale)localObject2);
      localObject2 = "voicemail";
      bool1 = ((String)localObject1).contains((CharSequence)localObject2);
      if (bool1)
      {
        bool1 = true;
        break label126;
      }
    }
    bool1 = false;
    localObject1 = null;
    label126:
    if (bool1) {
      return false;
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramStatusBarNotification.getId());
    bool1 = ((Collection)localObject1).contains(localObject2);
    int m = 21;
    if (!bool1)
    {
      n = Build.VERSION.SDK_INT;
      if (n >= m)
      {
        str3 = paramStatusBarNotification.getGroupKey();
        if (str3 != null)
        {
          str3 = paramStatusBarNotification.getGroupKey().toLowerCase();
          Iterator localIterator = d.iterator();
          boolean bool3;
          do
          {
            bool3 = localIterator.hasNext();
            if (!bool3) {
              break;
            }
            String str4 = (String)localIterator.next();
            bool3 = str3.contains(str4);
          } while (!bool3);
          n = 1;
          break label258;
        }
      }
    }
    int n = 0;
    String str3 = null;
    label258:
    if ((!bool1) && (n == 0)) {
      return false;
    }
    int k = Build.VERSION.SDK_INT;
    if (k < m)
    {
      localObject1 = paramStatusBarNotification.getPackageName();
      str1 = paramStatusBarNotification.getTag();
      int i1 = paramStatusBarNotification.getId();
      cancelNotification((String)localObject1, str1, i1);
    }
    else
    {
      paramStatusBarNotification = paramStatusBarNotification.getKey();
      cancelNotification(paramStatusBarNotification);
    }
    return bool2;
  }
  
  private void b()
  {
    Handler localHandler = h;
    -..Lambda.NotificationHandlerService.3R4eoT46X068fylmv5S2Mq1WDBA local3R4eoT46X068fylmv5S2Mq1WDBA = new com/truecaller/notifications/-$$Lambda$NotificationHandlerService$3R4eoT46X068fylmv5S2Mq1WDBA;
    local3R4eoT46X068fylmv5S2Mq1WDBA.<init>(this);
    localHandler.post(local3R4eoT46X068fylmv5S2Mq1WDBA);
  }
  
  private void c()
  {
    Object localObject = getApplicationContext();
    MissedCallsNotificationService.a((Context)localObject);
    int k = Build.VERSION.SDK_INT;
    int m = 21;
    if (k >= m)
    {
      k = getCurrentInterruptionFilter();
      onInterruptionFilterChanged(k);
    }
    try
    {
      localObject = getActiveNotifications();
      if (localObject == null) {
        return;
      }
      m = localObject.length;
      int n = 0;
      while (n < m)
      {
        StatusBarNotification localStatusBarNotification = localObject[n];
        onNotificationPosted(localStatusBarNotification);
        n += 1;
      }
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      d.a(localRuntimeException, "Could not list active notifications");
    }
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    String[] arrayOfString = new String[1];
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("onBind() on ");
    String str = Thread.currentThread().getName();
    ((StringBuilder)localObject).append(str);
    localObject = ((StringBuilder)localObject).toString();
    str = null;
    arrayOfString[0] = localObject;
    paramIntent = super.onBind(paramIntent);
    int k = Build.VERSION.SDK_INT;
    int m = 21;
    if (k < m)
    {
      b();
    }
    else
    {
      k = getCurrentInterruptionFilter();
      e = k;
    }
    return paramIntent;
  }
  
  public void onCreate()
  {
    super.onCreate();
    Object localObject1 = TrueApp.y().a();
    Object localObject2 = ((bp)localObject1).D();
    i = ((com.truecaller.i.c)localObject2);
    localObject2 = ((bp)localObject1).E();
    f = ((Set)localObject2);
    localObject1 = ((bp)localObject1).t();
    j = ((al)localObject1);
    localObject1 = new android/os/HandlerThread;
    ((HandlerThread)localObject1).<init>("NotificationHandlerService", 10);
    ((HandlerThread)localObject1).start();
    localObject1 = ((HandlerThread)localObject1).getLooper();
    g = ((Looper)localObject1);
    localObject1 = new android/os/Handler;
    localObject2 = g;
    ((Handler)localObject1).<init>((Looper)localObject2);
    h = ((Handler)localObject1);
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    int k = 1;
    Object localObject1 = new String[k];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("onDestroy() on ");
    String str = Thread.currentThread().getName();
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    str = null;
    localObject1[0] = localObject2;
    localObject1 = f.iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (h)((Iterator)localObject1).next();
      ((h)localObject2).a();
    }
    g.quit();
  }
  
  public void onInterruptionFilterChanged(int paramInt)
  {
    e = paramInt;
    new String[1][0] = "Do not disturb mode changed, triggering a presence update";
    com.truecaller.presence.c localc = (com.truecaller.presence.c)((bk)getApplicationContext()).a().ae().a();
    AvailabilityTrigger localAvailabilityTrigger = AvailabilityTrigger.USER_ACTION;
    localc.a(localAvailabilityTrigger, true);
  }
  
  public void onListenerConnected()
  {
    String[] arrayOfString = new String[1];
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("onListenerConnected() on ");
    String str = Thread.currentThread().getName();
    ((StringBuilder)localObject).append(str);
    localObject = ((StringBuilder)localObject).toString();
    arrayOfString[0] = localObject;
    b();
  }
  
  public void onNotificationPosted(StatusBarNotification paramStatusBarNotification)
  {
    if (paramStatusBarNotification == null) {
      return;
    }
    Object localObject1 = new String[1];
    -..Lambda.NotificationHandlerService.3OI-qM1f1t61oaBAgxTBTFfoSxk local3OI-qM1f1t61oaBAgxTBTFfoSxk = null;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("onNotificationPosted(");
    String str = paramStatusBarNotification.getPackageName();
    ((StringBuilder)localObject2).append(str);
    ((StringBuilder)localObject2).append(") on ");
    str = Thread.currentThread().getName();
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    localObject1 = j;
    boolean bool = ((al)localObject1).b();
    if (bool)
    {
      localObject1 = j;
      bool = ((al)localObject1).a();
      if (!bool)
      {
        new String[1][0] = "Invalid account, not canceling missed call notification";
        return;
      }
    }
    bool = a(paramStatusBarNotification);
    if (bool)
    {
      new String[1][0] = "Canceled missed call notification";
      return;
    }
    localObject1 = h;
    local3OI-qM1f1t61oaBAgxTBTFfoSxk = new com/truecaller/notifications/-$$Lambda$NotificationHandlerService$3OI-qM1f1t61oaBAgxTBTFfoSxk;
    local3OI-qM1f1t61oaBAgxTBTFfoSxk.<init>(this, paramStatusBarNotification);
    ((Handler)localObject1).post(local3OI-qM1f1t61oaBAgxTBTFfoSxk);
  }
  
  public void onNotificationRemoved(StatusBarNotification paramStatusBarNotification)
  {
    if (paramStatusBarNotification == null) {
      return;
    }
    Object localObject1 = new String[1];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("onNotificationRemoved(");
    String str = paramStatusBarNotification.getPackageName();
    ((StringBuilder)localObject2).append(str);
    ((StringBuilder)localObject2).append(")");
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    localObject1 = h;
    -..Lambda.NotificationHandlerService.LoQoknE-4-dYknUh5UOu7HSHEPQ localLoQoknE-4-dYknUh5UOu7HSHEPQ = new com/truecaller/notifications/-$$Lambda$NotificationHandlerService$LoQoknE-4-dYknUh5UOu7HSHEPQ;
    localLoQoknE-4-dYknUh5UOu7HSHEPQ.<init>(this, paramStatusBarNotification);
    ((Handler)localObject1).post(localLoQoknE-4-dYknUh5UOu7HSHEPQ);
  }
  
  public boolean onUnbind(Intent paramIntent)
  {
    String[] arrayOfString = new String[1];
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("onUnbind() on ");
    String str = Thread.currentThread().getName();
    ((StringBuilder)localObject).append(str);
    localObject = ((StringBuilder)localObject).toString();
    str = null;
    arrayOfString[0] = localObject;
    int k = Build.VERSION.SDK_INT;
    int m = 21;
    if (k >= m) {
      e = 0;
    }
    return super.onUnbind(paramIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.NotificationHandlerService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */