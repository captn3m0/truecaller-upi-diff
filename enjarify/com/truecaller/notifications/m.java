package com.truecaller.notifications;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog.Builder;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;

public final class m
  extends DialogFragment
{
  public static m a(String paramString)
  {
    m localm = new com/truecaller/notifications/m;
    localm.<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("html", paramString);
    localm.setArguments(localBundle);
    return localm;
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = getArguments().getString("html");
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject = getActivity();
    localBuilder.<init>((Context)localObject);
    localObject = LayoutInflater.from(getActivity()).inflate(2131558565, null);
    ((WebView)((View)localObject).findViewById(2131362649)).loadData(paramBundle, "text/html", "utf-8");
    return localBuilder.setView((View)localObject).create();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */