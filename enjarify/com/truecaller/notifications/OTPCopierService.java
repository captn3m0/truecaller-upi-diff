package com.truecaller.notifications;

import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;
import c.g.b.k;
import c.u;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.notifications.NotificationIdentifier;
import com.truecaller.utils.n;

public final class OTPCopierService
  extends Service
{
  public n a;
  public a b;
  public b c;
  public f d;
  public ClipboardManager e;
  
  private final void a(String paramString)
  {
    b localb = c;
    if (localb == null)
    {
      localObject = "analytics";
      k.a((String)localObject);
    }
    Object localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>("ViewAction");
    paramString = ((e.a)localObject).a("Action", paramString).a();
    k.a(paramString, "AnalyticsEvent.Builder(A…\n                .build()");
    localb.b(paramString);
  }
  
  public final IBinder onBind(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    return null;
  }
  
  public final void onCreate()
  {
    super.onCreate();
    Object localObject = getApplication();
    if (localObject != null)
    {
      ((bk)localObject).a().a(this);
      return;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw ((Throwable)localObject);
  }
  
  public final int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    k.b(paramIntent, "intent");
    Object localObject1 = paramIntent.getAction();
    Object localObject2 = { "OTPCopierService action should not be null" };
    AssertionUtil.isNotNull(localObject1, (String[])localObject2);
    localObject2 = "OTP_NOTIFICATION_ID";
    int i = -1;
    paramInt2 = paramIntent.getIntExtra((String)localObject2, i);
    Object localObject3;
    int j;
    if (paramInt2 != i)
    {
      localObject3 = new com/truecaller/messaging/notifications/NotificationIdentifier;
      j = 4;
      ((NotificationIdentifier)localObject3).<init>(paramInt2, 0, j);
    }
    else
    {
      localObject2 = paramIntent.getParcelableExtra("OTP_NOTIFICATION_ID");
      k.a(localObject2, "intent.getParcelableExtra(KEY_OTP_NOTIFICATION_ID)");
      localObject3 = localObject2;
      localObject3 = (NotificationIdentifier)localObject2;
    }
    localObject2 = "MESSAGE_ID";
    long l1 = -1;
    long l2 = paramIntent.getLongExtra((String)localObject2, l1);
    paramInt2 = l2 < l1;
    Object localObject4;
    if (paramInt2 != 0)
    {
      localObject2 = d;
      if (localObject2 == null)
      {
        localObject4 = "messageStorageRef";
        k.a((String)localObject4);
      }
      localObject2 = (t)((f)localObject2).a();
      j = 1;
      localObject4 = new long[j];
      localObject4[0] = l2;
      ((t)localObject2).c((long[])localObject4);
    }
    localObject2 = new String[0];
    AssertionUtil.isNotNull(localObject3, (String[])localObject2);
    localObject2 = b;
    if (localObject2 != null)
    {
      localObject2 = b;
      if (localObject2 == null)
      {
        localObject4 = "analyticsNotificationManager";
        k.a((String)localObject4);
      }
      localObject4 = b;
      i = a;
      ((a)localObject2).a((String)localObject4, i);
    }
    else
    {
      localObject2 = b;
      if (localObject2 == null)
      {
        localObject4 = "analyticsNotificationManager";
        k.a((String)localObject4);
      }
      i = a;
      ((a)localObject2).a(i);
    }
    if (localObject1 != null)
    {
      paramInt2 = ((String)localObject1).hashCode();
      i = -648928470;
      if (paramInt2 != i)
      {
        int k = 299469613;
        boolean bool;
        if (paramInt2 != k)
        {
          k = 2031677783;
          if (paramInt2 == k)
          {
            paramIntent = "ACTION_MARK_MESSAGE_READ";
            bool = ((String)localObject1).equals(paramIntent);
            if (bool)
            {
              paramIntent = "otpMarkedReadFromNotif";
              a(paramIntent);
            }
          }
        }
        else
        {
          paramIntent = "ACTION_DISMISS_OTP";
          bool = ((String)localObject1).equals(paramIntent);
          if (bool)
          {
            paramIntent = "otpDismissedFromNotif";
            a(paramIntent);
          }
        }
      }
      else
      {
        localObject2 = "ACTION_COPY_OTP";
        paramInt1 = ((String)localObject1).equals(localObject2);
        if (paramInt1 != 0)
        {
          localObject1 = new android/content/Intent;
          localObject2 = "android.intent.action.CLOSE_SYSTEM_DIALOGS";
          ((Intent)localObject1).<init>((String)localObject2);
          sendBroadcast((Intent)localObject1);
          paramIntent = paramIntent.getStringExtra("OTP");
          localObject1 = (CharSequence)"com.truecaller.OTP";
          paramIntent = (CharSequence)paramIntent;
          paramIntent = ClipData.newPlainText((CharSequence)localObject1, paramIntent);
          localObject1 = e;
          if (localObject1 != null)
          {
            if (localObject1 != null) {
              ((ClipboardManager)localObject1).setPrimaryClip(paramIntent);
            }
            paramIntent = this;
            paramIntent = (Context)this;
            localObject1 = a;
            if (localObject1 == null)
            {
              localObject2 = "resourceProvider";
              k.a((String)localObject2);
            }
            paramInt2 = 2131888469;
            localObject3 = new Object[0];
            localObject1 = (CharSequence)((n)localObject1).a(paramInt2, (Object[])localObject3);
            Toast.makeText(paramIntent, (CharSequence)localObject1, 0).show();
            paramIntent = "otpCopiedFromNotif";
            a(paramIntent);
          }
          else
          {
            paramIntent = new String[] { "Clipboard manager is null." };
            AssertionUtil.report(paramIntent);
          }
        }
      }
    }
    return 2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.OTPCopierService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */