package com.truecaller.notifications;

import android.app.Notification;
import android.content.Context;
import android.service.notification.StatusBarNotification;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.common.h.t;
import com.truecaller.data.access.i;
import java.util.Stack;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;

public final class al
  extends h
{
  final Stack a;
  final Context b;
  final i c;
  final com.truecaller.data.access.c d;
  final com.truecaller.calling.e.c e;
  final t f;
  private bn g;
  private final com.truecaller.calling.e.e h;
  private final f i;
  
  public al(Context paramContext, com.truecaller.calling.e.e parame, i parami, com.truecaller.data.access.c paramc, com.truecaller.calling.e.c paramc1, f paramf, t paramt)
  {
    b = paramContext;
    h = parame;
    c = parami;
    d = paramc;
    e = paramc1;
    i = paramf;
    f = paramt;
    paramContext = new java/util/Stack;
    paramContext.<init>();
    a = paramContext;
  }
  
  private final boolean c(StatusBarNotification paramStatusBarNotification)
  {
    Object localObject = h;
    boolean bool1 = ((com.truecaller.calling.e.e)localObject).b();
    if (!bool1) {
      return false;
    }
    bool1 = paramStatusBarNotification.isClearable();
    if (!bool1)
    {
      localObject = "com.whatsapp";
      String str = paramStatusBarNotification.getPackageName();
      bool1 = k.a(localObject, str);
      boolean bool2 = true;
      bool1 ^= bool2;
      if (!bool1)
      {
        paramStatusBarNotification = getNotificationcategory;
        localObject = "call";
        boolean bool3 = k.a(paramStatusBarNotification, localObject) ^ bool2;
        if (bool3) {
          return false;
        }
        return bool2;
      }
    }
    return false;
  }
  
  public final void a(StatusBarNotification paramStatusBarNotification)
  {
    Object localObject = "statusBarNotification";
    k.b(paramStatusBarNotification, (String)localObject);
    boolean bool = c(paramStatusBarNotification);
    if (!bool) {
      return;
    }
    localObject = g;
    if (localObject != null) {
      ((bn)localObject).n();
    }
    localObject = b;
    paramStatusBarNotification = am.a(paramStatusBarNotification, (Context)localObject);
    a.push(paramStatusBarNotification);
  }
  
  public final void b(StatusBarNotification paramStatusBarNotification)
  {
    Object localObject1 = "statusBarNotification";
    k.b(paramStatusBarNotification, (String)localObject1);
    boolean bool = c(paramStatusBarNotification);
    if (!bool) {
      return;
    }
    localObject1 = g;
    if (localObject1 != null) {
      ((bn)localObject1).n();
    }
    localObject1 = (ag)bg.a;
    f localf = i;
    Object localObject2 = new com/truecaller/notifications/al$a;
    ((al.a)localObject2).<init>(this, paramStatusBarNotification, null);
    localObject2 = (m)localObject2;
    paramStatusBarNotification = kotlinx.coroutines.e.b((ag)localObject1, localf, (m)localObject2, 2);
    g = paramStatusBarNotification;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.al
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */