package com.truecaller.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import c.n.m;
import com.truecaller.TrueApp;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.bb;
import com.truecaller.androidactors.w;
import com.truecaller.bp;
import com.truecaller.callerid.ai;
import com.truecaller.calling.ar;
import com.truecaller.common.h.aa;
import com.truecaller.flashsdk.core.b;
import com.truecaller.log.AssertionUtil;
import com.truecaller.multisim.h;
import com.truecaller.tracking.events.f.a;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.util.al;
import com.truecaller.util.bl;
import org.apache.a.d.d;

public final class MissedCallsNotificationActionReceiver
  extends BroadcastReceiver
{
  public static final MissedCallsNotificationActionReceiver.a a;
  private Context b;
  private bp c;
  private Intent d;
  
  static
  {
    MissedCallsNotificationActionReceiver.a locala = new com/truecaller/notifications/MissedCallsNotificationActionReceiver$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  private final void a(boolean paramBoolean)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "graph";
      c.g.b.k.a((String)localObject2);
    }
    localObject1 = ((bp)localObject1).ad();
    c.g.b.k.a(localObject1, "graph.callHistoryManager()");
    Object localObject2 = d;
    if (localObject2 == null)
    {
      str = "intent";
      c.g.b.k.a(str);
    }
    String str = "lastTimestamp";
    long l1 = Long.MAX_VALUE;
    long l2 = ((Intent)localObject2).getLongExtra(str, l1);
    w localw = ((com.truecaller.callhistory.a)((com.truecaller.androidactors.f)localObject1).a()).a(l2);
    Object localObject3 = new com/truecaller/notifications/MissedCallsNotificationActionReceiver$b;
    ((MissedCallsNotificationActionReceiver.b)localObject3).<init>(this);
    localObject3 = (com.truecaller.androidactors.ac)localObject3;
    localw.a((com.truecaller.androidactors.ac)localObject3);
    localObject1 = (com.truecaller.callhistory.a)((com.truecaller.androidactors.f)localObject1).a();
    ((com.truecaller.callhistory.a)localObject1).b(l2);
    if (!paramBoolean)
    {
      Context localContext = b;
      if (localContext == null)
      {
        localObject1 = "context";
        c.g.b.k.a((String)localObject1);
      }
      bl.a(localContext);
    }
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramContext == null) {
      return;
    }
    b = paramContext;
    if (paramIntent == null) {
      return;
    }
    d = paramIntent;
    Object localObject1 = paramContext.getApplicationContext();
    boolean bool1 = localObject1 instanceof TrueApp;
    boolean bool2 = false;
    Object localObject2 = null;
    boolean bool3;
    if (!bool1)
    {
      bool3 = false;
      localObject1 = null;
    }
    localObject1 = (TrueApp)localObject1;
    if (localObject1 != null)
    {
      localObject1 = ((TrueApp)localObject1).a();
      if (localObject1 != null)
      {
        c = ((bp)localObject1);
        paramIntent = paramIntent.getAction();
        if (paramIntent == null) {
          return;
        }
        localObject1 = c;
        if (localObject1 == null)
        {
          localObject3 = "graph";
          c.g.b.k.a((String)localObject3);
        }
        localObject1 = ((bp)localObject1).t();
        Object localObject3 = "graph.deviceManager()";
        c.g.b.k.a(localObject1, (String)localObject3);
        bool3 = ((al)localObject1).b();
        if (bool3)
        {
          localObject1 = c;
          if (localObject1 == null)
          {
            localObject3 = "graph";
            c.g.b.k.a((String)localObject3);
          }
          localObject1 = ((bp)localObject1).T();
          bool3 = ((com.truecaller.common.account.r)localObject1).c();
          if (!bool3) {
            return;
          }
        }
        int j = paramIntent.hashCode();
        int i = 268435456;
        boolean bool5 = true;
        long l1 = -1;
        boolean bool6;
        Object localObject4;
        long l2;
        boolean bool4;
        Object localObject5;
        String str1;
        String str2;
        boolean bool7;
        switch (j)
        {
        default: 
          break;
        case 2097706097: 
          localObject1 = "com.truecaller.SMS";
          bool6 = paramIntent.equals(localObject1);
          if (bool6)
          {
            paramIntent = c;
            if (paramIntent == null)
            {
              localObject1 = "graph";
              c.g.b.k.a((String)localObject1);
            }
            paramIntent = paramIntent.ad();
            c.g.b.k.a(paramIntent, "graph.callHistoryManager()");
            localObject1 = d;
            if (localObject1 == null)
            {
              localObject4 = "intent";
              c.g.b.k.a((String)localObject4);
            }
            localObject4 = "callLogId";
            l2 = ((Intent)localObject1).getLongExtra((String)localObject4, l1);
            bool4 = l2 < l1;
            if (bool4)
            {
              paramIntent = (com.truecaller.callhistory.a)paramIntent.a();
              paramIntent.c(l2);
            }
            paramIntent = b;
            if (paramIntent == null)
            {
              localObject1 = "context";
              c.g.b.k.a((String)localObject1);
            }
            localObject1 = new android/content/Intent;
            ((Intent)localObject1).<init>("android.intent.action.CLOSE_SYSTEM_DIALOGS");
            paramIntent.sendBroadcast((Intent)localObject1);
            paramIntent = new android/content/Intent;
            localObject1 = "android.intent.action.SENDTO";
            localObject4 = "smsto";
            localObject5 = d;
            if (localObject5 == null)
            {
              str1 = "intent";
              c.g.b.k.a(str1);
            }
            str1 = "number";
            localObject5 = ((Intent)localObject5).getStringExtra(str1);
            localObject2 = Uri.fromParts((String)localObject4, (String)localObject5, null);
            paramIntent.<init>((String)localObject1, (Uri)localObject2);
            paramIntent.setFlags(i);
            localObject1 = b;
            if (localObject1 == null)
            {
              localObject3 = "context";
              c.g.b.k.a((String)localObject3);
            }
            ((Context)localObject1).startActivity(paramIntent);
          }
          break;
        case 1543847176: 
          localObject1 = "com.truecaller.FLASH";
          bool6 = paramIntent.equals(localObject1);
          if (bool6)
          {
            paramIntent = d;
            if (paramIntent == null)
            {
              localObject1 = "intent";
              c.g.b.k.a((String)localObject1);
            }
            paramIntent = paramIntent.getExtras();
            localObject1 = "callLogId";
            long l3 = paramIntent.getLong((String)localObject1, l1);
            bool2 = l3 < l1;
            if (bool2)
            {
              localObject2 = c;
              if (localObject2 == null)
              {
                localObject4 = "graph";
                c.g.b.k.a((String)localObject4);
              }
              localObject2 = (com.truecaller.callhistory.a)((bp)localObject2).ad().a();
              ((com.truecaller.callhistory.a)localObject2).c(l3);
            }
            localObject1 = b;
            if (localObject1 == null)
            {
              localObject3 = "context";
              c.g.b.k.a((String)localObject3);
            }
            localObject3 = new android/content/Intent;
            localObject2 = "android.intent.action.CLOSE_SYSTEM_DIALOGS";
            ((Intent)localObject3).<init>((String)localObject2);
            ((Context)localObject1).sendBroadcast((Intent)localObject3);
            localObject1 = paramIntent.getString("number");
            localObject3 = "name";
            str2 = paramIntent.getString((String)localObject3, (String)localObject1);
            paramIntent = (Intent)localObject1;
            paramIntent = (CharSequence)localObject1;
            bool6 = org.c.a.a.a.k.b(paramIntent);
            if (!bool6)
            {
              if (localObject1 == null) {
                c.g.b.k.a();
              }
              localObject3 = "";
              paramIntent = m.a((String)localObject1, "+", (String)localObject3);
              l1 = Long.parseLong(paramIntent);
              localObject2 = com.truecaller.flashsdk.core.c.a();
              localObject4 = b;
              if (localObject4 == null)
              {
                paramIntent = "context";
                c.g.b.k.a(paramIntent);
              }
              String str3 = "notification";
              ((b)localObject2).a((Context)localObject4, l1, str2, str3);
            }
          }
          break;
        case 603891238: 
          localObject1 = "com.truecaller.CALL";
          bool6 = paramIntent.equals(localObject1);
          if (bool6)
          {
            paramIntent = c;
            if (paramIntent == null)
            {
              localObject1 = "graph";
              c.g.b.k.a((String)localObject1);
            }
            paramIntent = paramIntent.U();
            c.g.b.k.a(paramIntent, "graph.multiSimManager()");
            localObject1 = d;
            if (localObject1 == null)
            {
              str2 = "intent";
              c.g.b.k.a(str2);
            }
            str2 = "callLogId";
            l2 = ((Intent)localObject1).getLongExtra(str2, l1);
            bool4 = l2 < l1;
            if (bool4)
            {
              localObject1 = c;
              if (localObject1 == null)
              {
                localObject5 = "graph";
                c.g.b.k.a((String)localObject5);
              }
              localObject1 = (com.truecaller.callhistory.a)((bp)localObject1).ad().a();
              ((com.truecaller.callhistory.a)localObject1).c(l2);
            }
            localObject1 = b;
            if (localObject1 == null)
            {
              localObject5 = "context";
              c.g.b.k.a((String)localObject5);
            }
            localObject5 = new android/content/Intent;
            str1 = "android.intent.action.CLOSE_SYSTEM_DIALOGS";
            ((Intent)localObject5).<init>(str1);
            ((Context)localObject1).sendBroadcast((Intent)localObject5);
            localObject1 = d;
            if (localObject1 == null)
            {
              localObject5 = "intent";
              c.g.b.k.a((String)localObject5);
            }
            localObject5 = "number";
            localObject1 = ((Intent)localObject1).getStringExtra((String)localObject5);
            boolean bool8 = paramIntent.j();
            if (bool8)
            {
              localObject5 = c;
              if (localObject5 == null)
              {
                str1 = "graph";
                c.g.b.k.a(str1);
              }
              localObject5 = ((bp)localObject5).bv().e();
              str1 = "-1";
              boolean bool9 = c.g.b.k.a(localObject5, str1) ^ bool5;
              if (bool9) {
                localObject2 = localObject5;
              }
              if (localObject2 != null)
              {
                localObject5 = d;
                if (localObject5 == null)
                {
                  str1 = "intent";
                  c.g.b.k.a(str1);
                }
                paramIntent.a((Intent)localObject5, (String)localObject2);
              }
            }
            paramIntent = c;
            if (paramIntent == null)
            {
              localObject2 = "graph";
              c.g.b.k.a((String)localObject2);
            }
            paramIntent = paramIntent.D();
            c.g.b.k.a(paramIntent, "graph.callingSettings()");
            localObject5 = "missedCallNotification";
            paramIntent.a("key_last_call_origin", (String)localObject5);
            paramIntent.b("key_temp_latest_call_made_with_tc", bool5);
            localObject2 = "lastCallMadeWithTcTime";
            long l4 = System.currentTimeMillis();
            paramIntent.b((String)localObject2, l4);
            paramIntent = b;
            if (paramIntent == null)
            {
              localObject2 = "context";
              c.g.b.k.a((String)localObject2);
            }
            paramIntent = paramIntent.getPackageManager();
            localObject2 = "android.permission.CALL_PRIVILEGED";
            localObject4 = b;
            if (localObject4 == null)
            {
              localObject5 = "context";
              c.g.b.k.a((String)localObject5);
            }
            localObject4 = ((Context)localObject4).getPackageName();
            int k = paramIntent.checkPermission((String)localObject2, (String)localObject4);
            if (k == 0) {
              paramIntent = "android.intent.action.CALL_PRIVILEGED";
            } else {
              paramIntent = "android.intent.action.CALL";
            }
            localObject2 = new android/content/Intent;
            c.g.b.k.a(localObject1, "number");
            localObject4 = com.truecaller.utils.extensions.r.a((String)localObject1);
            ((Intent)localObject2).<init>(paramIntent, (Uri)localObject4);
            paramIntent = ((Intent)localObject2).setFlags(i);
            localObject3 = b;
            if (localObject3 == null)
            {
              localObject2 = "context";
              c.g.b.k.a((String)localObject2);
            }
            ((Context)localObject3).startActivity(paramIntent);
            paramIntent = b;
            if (paramIntent == null)
            {
              localObject3 = "context";
              c.g.b.k.a((String)localObject3);
            }
            paramIntent = com.truecaller.common.h.k.f(paramIntent);
            paramIntent = aa.c((String)localObject1, paramIntent);
            localObject1 = "PhoneNumberNormalizer.no…tPlus(number, countryIso)";
            c.g.b.k.a(paramIntent, (String)localObject1);
            try
            {
              localObject1 = c;
              if (localObject1 == null)
              {
                localObject3 = "graph";
                c.g.b.k.a((String)localObject3);
              }
              localObject1 = ((bp)localObject1).f();
              localObject1 = ((com.truecaller.androidactors.f)localObject1).a();
              localObject1 = (ae)localObject1;
              localObject3 = com.truecaller.tracking.events.f.b();
              localObject2 = "notification";
              localObject2 = (CharSequence)localObject2;
              localObject3 = ((f.a)localObject3).b((CharSequence)localObject2);
              paramIntent = (CharSequence)paramIntent;
              paramIntent = ((f.a)localObject3).a(paramIntent);
              localObject3 = ai.a();
              localObject3 = (CharSequence)localObject3;
              paramIntent = paramIntent.c((CharSequence)localObject3);
              paramIntent = paramIntent.a();
              paramIntent = (d)paramIntent;
              ((ae)localObject1).a(paramIntent);
            }
            catch (org.apache.a.a locala)
            {
              paramIntent = (Throwable)locala;
              AssertionUtil.reportThrowableButNeverCrash(paramIntent);
            }
          }
          break;
        case 112535124: 
          localObject1 = "com.truecaller.OPEN_APP";
          bool7 = paramIntent.equals(localObject1);
          if (bool7)
          {
            paramIntent = b;
            if (paramIntent == null)
            {
              localObject1 = "context";
              c.g.b.k.a((String)localObject1);
            }
            paramIntent = TruecallerInit.a(paramIntent, "calls", "notificationCalls");
            c.g.b.k.a(paramIntent, "intent");
            paramIntent.setAction("android.intent.action.MAIN");
            localObject3 = "openApp";
            bb.a(paramIntent, "notification", (String)localObject3);
            localObject1 = paramIntent.getExtras();
            if (localObject1 != null) {
              paramIntent.putExtras((Bundle)localObject1);
            }
            localObject1 = b;
            if (localObject1 == null)
            {
              localObject3 = "context";
              c.g.b.k.a((String)localObject3);
            }
            ((Context)localObject1).startActivity(paramIntent);
          }
          break;
        case -152353365: 
          localObject1 = "com.truecaller.CLEAR_MISSED_CALLS";
          bool7 = paramIntent.equals(localObject1);
          if (bool7)
          {
            bool7 = false;
            paramIntent = null;
            a(false);
          }
          break;
        case -502740451: 
          localObject1 = "com.truecaller.CLEAR_ALTERNATIVE_MISSED_CALLS";
          bool7 = paramIntent.equals(localObject1);
          if (bool7) {
            a(bool5);
          }
          break;
        }
        paramContext = android.support.v4.app.ac.a(paramContext);
        c.g.b.k.a(paramContext, "NotificationManagerCompat.from(context)");
        paramContext.a("missedCall", 12345);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.MissedCallsNotificationActionReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */