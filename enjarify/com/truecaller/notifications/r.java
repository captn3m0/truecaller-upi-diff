package com.truecaller.notifications;

import com.truecaller.notificationchannels.j;
import com.truecaller.notificationchannels.n;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class r
  implements d
{
  private final Provider a;
  
  private r(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static j a(n paramn)
  {
    return (j)g.a(paramn.e(), "Cannot return null from a non-@Nullable @Provides method");
  }
  
  public static r a(Provider paramProvider)
  {
    r localr = new com/truecaller/notifications/r;
    localr.<init>(paramProvider);
    return localr;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */