package com.truecaller.notifications;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.ac;
import c.g.b.k;
import com.truecaller.analytics.e.a;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

public final class b
  implements a
{
  private final Random a;
  private final Context b;
  private final ac c;
  private final com.truecaller.analytics.b d;
  
  public b(Context paramContext, ac paramac, com.truecaller.analytics.b paramb)
  {
    b = paramContext;
    c = paramac;
    d = paramb;
    paramContext = new java/util/Random;
    paramContext.<init>();
    a = paramContext;
  }
  
  private final Intent a(String paramString1, PendingIntent paramPendingIntent, String paramString2, Bundle paramBundle)
  {
    Intent localIntent = new android/content/Intent;
    Context localContext = b;
    localIntent.<init>(localContext, AnalyticsNotificationReceiver.class);
    localIntent.putExtra("notification_type", paramString1);
    paramPendingIntent = (Parcelable)paramPendingIntent;
    localIntent.putExtra("original_pending_intent", paramPendingIntent);
    localIntent.putExtra("notification_status", paramString2);
    localIntent.putExtra("additional_params", paramBundle);
    return localIntent;
  }
  
  private final void a(String paramString1, String paramString2, Bundle paramBundle)
  {
    Object localObject1 = new com/truecaller/analytics/e$a;
    ((e.a)localObject1).<init>("Notification");
    Object localObject2 = "Type";
    paramString1 = ((e.a)localObject1).a((String)localObject2, paramString1);
    localObject1 = "Status";
    paramString1 = paramString1.a((String)localObject1, paramString2);
    if (paramBundle != null)
    {
      paramString2 = paramBundle.keySet().iterator();
      for (;;)
      {
        boolean bool = paramString2.hasNext();
        if (!bool) {
          break;
        }
        localObject1 = (String)paramString2.next();
        localObject2 = paramBundle.get((String)localObject1);
        if (localObject2 != null) {
          localObject2 = localObject2.toString();
        } else {
          localObject2 = null;
        }
        if (localObject2 == null) {
          localObject2 = "";
        }
        paramString1.a((String)localObject1, (String)localObject2);
      }
    }
    paramString2 = d;
    paramString1 = paramString1.a();
    k.a(paramString1, "event.build()");
    paramString2.b(paramString1);
  }
  
  public final PendingIntent a(PendingIntent paramPendingIntent, String paramString1, String paramString2)
  {
    k.b(paramString1, "type");
    k.b(paramString2, "notificationStatus");
    paramPendingIntent = a(paramString1, paramPendingIntent, paramString2, null);
    paramString1 = b;
    int i = a.nextInt();
    paramPendingIntent = PendingIntent.getBroadcast(paramString1, i, paramPendingIntent, 268435456);
    k.a(paramPendingIntent, "PendingIntent.getBroadca…_CANCEL_CURRENT\n        )");
    return paramPendingIntent;
  }
  
  public final void a(int paramInt)
  {
    c.a(paramInt);
  }
  
  public final void a(int paramInt, Notification paramNotification, String paramString)
  {
    k.b(paramNotification, "notification");
    a(null, paramInt, paramNotification, paramString, null);
  }
  
  public final void a(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    Object localObject = "original_pending_intent";
    try
    {
      localObject = paramIntent.getParcelableExtra((String)localObject);
      localObject = (PendingIntent)localObject;
      if (localObject != null) {
        ((PendingIntent)localObject).send();
      }
    }
    catch (PendingIntent.CanceledException localCanceledException) {}
    localObject = paramIntent.getStringExtra("notification_type");
    if (localObject == null) {
      return;
    }
    String str = paramIntent.getStringExtra("notification_status");
    if (str == null) {
      return;
    }
    paramIntent = (Bundle)paramIntent.getParcelableExtra("additional_params");
    a((String)localObject, str, paramIntent);
  }
  
  public final void a(String paramString, int paramInt)
  {
    k.b(paramString, "tag");
    c.a(paramString, paramInt);
  }
  
  public final void a(String paramString1, int paramInt, Notification paramNotification, String paramString2)
  {
    k.b(paramNotification, "notification");
    a(paramString1, paramInt, paramNotification, paramString2, null);
  }
  
  public final void a(String paramString1, int paramInt, Notification paramNotification, String paramString2, Bundle paramBundle)
  {
    Object localObject1 = "notification";
    k.b(paramNotification, (String)localObject1);
    if (paramString2 != null)
    {
      a(paramString2, "Shown", paramBundle);
      localObject1 = contentIntent;
      localObject1 = a(paramString2, (PendingIntent)localObject1, "Opened", paramBundle);
      Object localObject2 = deleteIntent;
      String str = "Dismissed";
      paramString2 = a(paramString2, (PendingIntent)localObject2, str, paramBundle);
      paramBundle = b;
      localObject2 = a;
      int i = ((Random)localObject2).nextInt();
      int j = 268435456;
      paramBundle = PendingIntent.getBroadcast(paramBundle, i, (Intent)localObject1, j);
      contentIntent = paramBundle;
      paramBundle = b;
      localObject1 = a;
      int k = ((Random)localObject1).nextInt();
      paramString2 = PendingIntent.getBroadcast(paramBundle, k, paramString2, j);
      deleteIntent = paramString2;
    }
    c.a(paramString1, paramInt, paramNotification);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */