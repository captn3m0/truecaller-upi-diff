package com.truecaller.notifications;

import dagger.a.d;
import javax.inject.Provider;

public final class s
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private s(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static s a(Provider paramProvider1, Provider paramProvider2)
  {
    s locals = new com/truecaller/notifications/s;
    locals.<init>(paramProvider1, paramProvider2);
    return locals;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */