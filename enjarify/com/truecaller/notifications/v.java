package com.truecaller.notifications;

import dagger.a.d;
import javax.inject.Provider;

public final class v
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private v(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static v a(Provider paramProvider1, Provider paramProvider2)
  {
    v localv = new com/truecaller/notifications/v;
    localv.<init>(paramProvider1, paramProvider2);
    return localv;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */