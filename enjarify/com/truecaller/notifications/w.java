package com.truecaller.notifications;

import dagger.a.d;
import javax.inject.Provider;

public final class w
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private w(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static w a(Provider paramProvider1, Provider paramProvider2)
  {
    w localw = new com/truecaller/notifications/w;
    localw.<init>(paramProvider1, paramProvider2);
    return localw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */