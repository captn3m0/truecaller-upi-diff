package com.truecaller.notifications;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.analytics.bc;
import com.truecaller.analytics.e;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.k;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.util.at;
import com.truecaller.utils.extensions.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

public class SourcedContactListActivity
  extends AppCompatActivity
{
  private f a;
  
  public static Intent a(Context paramContext, LinkedHashSet paramLinkedHashSet)
  {
    Intent localIntent = new android/content/Intent;
    Object localObject = SourcedContactListActivity.class;
    localIntent.<init>(paramContext, (Class)localObject);
    localIntent.addFlags(268435456);
    localIntent.addFlags(32768);
    localIntent.addFlags(8388608);
    boolean bool = k.d();
    int i = 524288;
    if (bool) {
      localIntent.addFlags(i);
    } else {
      localIntent.addFlags(i);
    }
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>(paramLinkedHashSet);
    localIntent.putParcelableArrayListExtra("sourcedContacts", (ArrayList)localObject);
    return localIntent;
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    a.a(this);
    Object localObject1 = getTheme();
    Object localObject2 = ThemeManager.a();
    int i = resId;
    int k = 0;
    String str = null;
    ((Resources.Theme)localObject1).applyStyle(i, false);
    localObject1 = ((bk)getApplication()).a().f();
    a = ((f)localObject1);
    localObject1 = getIntent();
    int m;
    if (localObject1 != null)
    {
      localObject2 = "sourcedContacts";
      localObject1 = ((Intent)localObject1).getParcelableArrayListExtra((String)localObject2);
    }
    else
    {
      m = 0;
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      boolean bool1 = ((ArrayList)localObject1).isEmpty();
      if (!bool1)
      {
        if (paramBundle == null)
        {
          n = ((List)localObject1).size();
          paramBundle = new long[n];
          localObject2 = ((List)localObject1).iterator();
          for (;;)
          {
            boolean bool2 = ((Iterator)localObject2).hasNext();
            if (!bool2) {
              break;
            }
            Long localLong = nextc;
            int i1 = k + 1;
            long l;
            if (localLong == null) {
              l = 0L;
            } else {
              l = localLong.longValue();
            }
            paramBundle[k] = l;
            k = i1;
          }
          localObject2 = new android/content/Intent;
          ((Intent)localObject2).<init>("com.truecaller.ACTION_ENHANCED_NOTIFICATION_DELETED");
          str = "ids";
          ((Intent)localObject2).putExtra(str, paramBundle);
          sendBroadcast((Intent)localObject2);
        }
        setContentView(2131559227);
        int n = 2131362507;
        paramBundle = findViewById(n);
        localObject2 = getResources();
        k = 2131166117;
        int j = ((Resources)localObject2).getDimensionPixelSize(k);
        at.b(paramBundle, j, j);
        localObject2 = new com/truecaller/notifications/-$$Lambda$SourcedContactListActivity$xAMwryusO4BOKtNSVzmYjCDVFGA;
        ((-..Lambda.SourcedContactListActivity.xAMwryusO4BOKtNSVzmYjCDVFGA)localObject2).<init>(this);
        paramBundle.setOnClickListener((View.OnClickListener)localObject2);
        paramBundle = new com/truecaller/notifications/SourcedContactListActivity$b;
        paramBundle.<init>(this, (List)localObject1);
        m = 16908298;
        localObject1 = (ListView)findViewById(m);
        ((ListView)localObject1).setAdapter(paramBundle);
        paramBundle = new com/truecaller/notifications/-$$Lambda$SourcedContactListActivity$W3XZggwKwjnpZQJ5PlCdSKd0YGs;
        paramBundle.<init>(this);
        ((ListView)localObject1).setOnItemClickListener(paramBundle);
        break label325;
      }
    }
    finish();
    label325:
    paramBundle = TrueApp.y().a().c();
    localObject1 = new com/truecaller/analytics/bc;
    ((bc)localObject1).<init>("sourcedContactList");
    paramBundle.a((e)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.SourcedContactListActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */