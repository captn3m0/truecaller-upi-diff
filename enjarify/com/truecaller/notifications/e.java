package com.truecaller.notifications;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.z.d;
import android.support.v4.content.b;
import android.text.TextUtils;
import com.truecaller.common.h.t;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.d;
import com.truecaller.old.data.access.Settings;
import com.truecaller.search.f;
import com.truecaller.util.al;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

final class e
  extends h
{
  final SharedPreferences a;
  final LinkedHashSet b;
  private final Context c;
  private final al d;
  private final Handler e;
  private final BroadcastReceiver f;
  private final t g;
  private final com.truecaller.notificationchannels.e h;
  private final a i;
  
  e(Context paramContext, t paramt, al paramal, com.truecaller.notificationchannels.e parame, a parama)
  {
    LinkedHashSet localLinkedHashSet = new java/util/LinkedHashSet;
    localLinkedHashSet.<init>();
    b = localLinkedHashSet;
    c = paramContext;
    d = paramal;
    h = parame;
    i = parama;
    paramal = new android/os/Handler;
    parame = Looper.getMainLooper();
    paramal.<init>(parame);
    e = paramal;
    g = paramt;
    paramContext = paramContext.getSharedPreferences("enhancedNumbers", 0);
    a = paramContext;
    paramContext = new com/truecaller/notifications/e$1;
    paramContext.<init>(this);
    f = paramContext;
    paramContext = c;
    paramt = f;
    paramal = new android/content/IntentFilter;
    paramal.<init>("com.truecaller.ACTION_ENHANCED_NOTIFICATION_DELETED");
    paramContext.registerReceiver(paramt, paramal, "com.truecaller.permission.ENHANCED_NOTIFICATION", null);
  }
  
  private void a(StatusBarNotification paramStatusBarNotification, Collection paramCollection, String paramString)
  {
    e locale = this;
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (bool1) {
      return;
    }
    Object localObject1 = g;
    Object localObject2 = paramString;
    localObject1 = ((t)localObject1).a(paramString);
    boolean bool2 = true;
    Object localObject3 = new String[bool2];
    localObject2 = String.valueOf(paramString);
    localObject2 = "input: ".concat((String)localObject2);
    localObject3[0] = localObject2;
    localObject2 = new String[bool2];
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("resolved numbers: [");
    Object localObject4 = TextUtils.join(", ", (Iterable)localObject1);
    ((StringBuilder)localObject3).append((String)localObject4);
    localObject4 = "]";
    ((StringBuilder)localObject3).append((String)localObject4);
    localObject3 = ((StringBuilder)localObject3).toString();
    localObject2[0] = localObject3;
    localObject1 = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool3 = ((Iterator)localObject1).hasNext();
      if (!bool3) {
        break;
      }
      localObject2 = ((Iterator)localObject1).next();
      Object localObject5 = localObject2;
      localObject5 = (String)localObject2;
      localObject2 = a;
      long l1 = 0L;
      long l2 = ((SharedPreferences)localObject2).getLong((String)localObject5, l1);
      bool3 = l2 < l1;
      if (bool3)
      {
        l1 = System.currentTimeMillis();
        long l3 = 86400000L;
        l1 -= l3;
        bool3 = l1 < l2;
        if (!bool3)
        {
          bool3 = false;
          localObject2 = null;
          break label246;
        }
      }
      bool3 = true;
      label246:
      localObject3 = c;
      boolean bool4 = f.a((Context)localObject3, (String)localObject5);
      localObject4 = new String[bool2];
      Object localObject6 = new java/lang/StringBuilder;
      ((StringBuilder)localObject6).<init>();
      ((StringBuilder)localObject6).append((String)localObject5);
      ((StringBuilder)localObject6).append(": isNewSearch = ");
      ((StringBuilder)localObject6).append(bool3);
      String str1 = ", isInPhoneBook = ";
      ((StringBuilder)localObject6).append(str1);
      ((StringBuilder)localObject6).append(bool4);
      localObject6 = ((StringBuilder)localObject6).toString();
      localObject4[0] = localObject6;
      if ((bool3) && (!bool4))
      {
        localObject2 = new String[bool2];
        localObject4 = String.valueOf(localObject5);
        localObject3 = "going to add contact for number: ".concat((String)localObject4);
        localObject2[0] = localObject3;
        localObject2 = a(c, (String)localObject5);
        if (localObject2 != null)
        {
          localObject3 = ((Contact)localObject2).t();
          bool4 = TextUtils.isEmpty((CharSequence)localObject3);
          if (!bool4)
          {
            localObject3 = new String[bool2];
            localObject6 = String.valueOf(localObject5);
            localObject4 = "Contact resolved ".concat((String)localObject6);
            localObject3[0] = localObject4;
            localObject3 = new com/truecaller/notifications/SourcedContact;
            localObject6 = paramStatusBarNotification.getPackageName();
            str1 = d(paramStatusBarNotification);
            Long localLong = ((Contact)localObject2).getId();
            String str2 = ((Contact)localObject2).getTcId();
            String str3 = ((Contact)localObject2).t();
            Uri localUri1 = ((Contact)localObject2).a(false);
            Uri localUri2 = ((Contact)localObject2).a(bool2);
            localObject4 = localObject3;
            ((SourcedContact)localObject3).<init>((String)localObject6, str1, localLong, str2, str3, (String)localObject5, localUri1, localUri2);
            localObject4 = paramCollection;
            paramCollection.add(localObject3);
            continue;
          }
        }
        localObject4 = paramCollection;
        localObject3 = new String[bool2];
        localObject6 = new java/lang/StringBuilder;
        str1 = "Contact resolution failed: ";
        ((StringBuilder)localObject6).<init>(str1);
        if (localObject2 != null)
        {
          localObject2 = ((Contact)localObject2).t();
        }
        else
        {
          bool3 = false;
          localObject2 = null;
        }
        ((StringBuilder)localObject6).append((String)localObject2);
        localObject2 = ((StringBuilder)localObject6).toString();
        localObject3[0] = localObject2;
      }
      else
      {
        localObject4 = paramCollection;
      }
    }
  }
  
  private static boolean a(String paramString)
  {
    return NotificationHandlerService.a.contains(paramString);
  }
  
  private void b()
  {
    int j = 1;
    Object localObject1 = new String[j];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("displayContactsNotification, mDisplayedContacts.size()=");
    int k = b.size();
    ((StringBuilder)localObject2).append(k);
    ((StringBuilder)localObject2).append(" on ");
    Object localObject3 = Thread.currentThread().getName();
    ((StringBuilder)localObject2).append((String)localObject3);
    localObject2 = ((StringBuilder)localObject2).toString();
    k = 0;
    localObject3 = null;
    localObject1[0] = localObject2;
    localObject1 = b;
    boolean bool1 = ((LinkedHashSet)localObject1).isEmpty();
    int n = 2131363819;
    if (bool1)
    {
      i.a(n);
      return;
    }
    int m = b.size();
    localObject1 = new long[m];
    Object localObject4 = b.iterator();
    int i1 = 0;
    Object localObject5 = null;
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject4).hasNext();
      if (!bool2) {
        break;
      }
      localObject6 = nextc;
      i3 = i1 + 1;
      long l;
      if (localObject6 == null) {
        l = 0L;
      } else {
        l = ((Long)localObject6).longValue();
      }
      localObject1[i1] = l;
      i1 = i3;
    }
    localObject4 = new android/content/Intent;
    ((Intent)localObject4).<init>("com.truecaller.ACTION_ENHANCED_NOTIFICATION_DELETED");
    ((Intent)localObject4).putExtra("ids", (long[])localObject1);
    localObject1 = c;
    localObject5 = b;
    localObject1 = SourcedContactListActivity.a((Context)localObject1, (LinkedHashSet)localObject5);
    localObject5 = c.getResources();
    int i2 = b.size();
    Object localObject7 = new Object[j];
    Object localObject8 = Integer.valueOf(b.size());
    localObject7[0] = localObject8;
    int i4 = 2131755015;
    Object localObject6 = ((Resources)localObject5).getQuantityString(i4, i2, (Object[])localObject7);
    localObject7 = b;
    int i3 = ((LinkedHashSet)localObject7).size();
    int i5;
    if (i3 == j)
    {
      localObject7 = (SourcedContact)b.iterator().next();
      localObject8 = e;
      i5 = 2131886525;
      localObject9 = new Object[j];
      localObject7 = b;
      localObject9[0] = localObject7;
      localObject9 = ((Resources)localObject5).getString(i5, (Object[])localObject9);
    }
    else
    {
      localObject7 = b;
      i3 = ((LinkedHashSet)localObject7).size();
      localObject9 = new Object[j];
      i5 = b.size();
      Integer localInteger = Integer.valueOf(i5);
      localObject9[0] = localInteger;
      localObject8 = ((Resources)localObject5).getQuantityString(i4, i3, (Object[])localObject9);
      j = 2131886526;
      localObject9 = ((Resources)localObject5).getString(j);
    }
    localObject3 = new android/support/v4/app/z$d;
    localObject5 = c;
    localObject7 = h.a();
    ((z.d)localObject3).<init>((Context)localObject5, (String)localObject7);
    ((z.d)localObject3).d((CharSequence)localObject6);
    ((z.d)localObject3).a((CharSequence)localObject8);
    ((z.d)localObject3).b((CharSequence)localObject9);
    Object localObject9 = c;
    i2 = 268435456;
    localObject9 = PendingIntent.getBroadcast((Context)localObject9, 2131364143, (Intent)localObject4, i2);
    ((z.d)localObject3).b((PendingIntent)localObject9);
    ((z.d)localObject3).a(2131234787);
    ((z.d)localObject3).d(16);
    localObject9 = PendingIntent.getActivity(c, 2131364144, (Intent)localObject1, i2);
    f = ((PendingIntent)localObject9);
    j = b.c(c, 2131099685);
    C = j;
    localObject9 = i;
    localObject1 = ((z.d)localObject3).h();
    ((a)localObject9).a(n, (Notification)localObject1, "enhanceNotification");
  }
  
  private void c(StatusBarNotification paramStatusBarNotification)
  {
    Object localObject1 = paramStatusBarNotification.getNotification();
    if (localObject1 == null) {
      return;
    }
    LinkedHashSet localLinkedHashSet = new java/util/LinkedHashSet;
    localLinkedHashSet.<init>();
    Object localObject2 = tickerText;
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject2);
    if (!bool1)
    {
      localObject2 = tickerText.toString();
      a(paramStatusBarNotification, localLinkedHashSet, (String)localObject2);
    }
    localObject2 = extras;
    String str1 = "android.title";
    localObject2 = ((Bundle)localObject2).getString(str1);
    a(paramStatusBarNotification, localLinkedHashSet, (String)localObject2);
    localObject1 = extras;
    localObject2 = "android.people";
    localObject1 = ((Bundle)localObject1).getStringArray((String)localObject2);
    if (localObject1 != null)
    {
      int j = localObject1.length;
      int k = 0;
      str1 = null;
      while (k < j)
      {
        String str2 = localObject1[k];
        a(paramStatusBarNotification, localLinkedHashSet, str2);
        k += 1;
      }
    }
    boolean bool2 = localLinkedHashSet.isEmpty();
    if (!bool2)
    {
      paramStatusBarNotification = e;
      localObject1 = new com/truecaller/notifications/-$$Lambda$e$IyB5-ZLWOtA620VC1yGnJb-d2FI;
      ((-..Lambda.e.IyB5-ZLWOtA620VC1yGnJb-d2FI)localObject1).<init>(this, localLinkedHashSet);
      paramStatusBarNotification.post((Runnable)localObject1);
    }
  }
  
  private String d(StatusBarNotification paramStatusBarNotification)
  {
    Object localObject1 = "";
    try
    {
      Object localObject2 = c;
      localObject2 = ((Context)localObject2).getPackageManager();
      paramStatusBarNotification = paramStatusBarNotification.getPackageName();
      paramStatusBarNotification = ((PackageManager)localObject2).getPackageInfo(paramStatusBarNotification, 0);
      paramStatusBarNotification = applicationInfo;
      localObject1 = paramStatusBarNotification.loadLabel((PackageManager)localObject2);
    }
    catch (PackageManager.NameNotFoundException|RuntimeException localNameNotFoundException)
    {
      for (;;) {}
    }
    return String.valueOf(localObject1);
  }
  
  public final void a()
  {
    Context localContext = c;
    BroadcastReceiver localBroadcastReceiver = f;
    localContext.unregisterReceiver(localBroadcastReceiver);
  }
  
  public final void a(StatusBarNotification paramStatusBarNotification)
  {
    int j = 1;
    Object localObject1 = new String[j];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("shouldHandle() enabled=");
    boolean bool1 = Settings.e("enhancedNotificationsEnabled");
    ((StringBuilder)localObject2).append(bool1);
    ((StringBuilder)localObject2).append(", hasValidAccount=");
    bool1 = d.a();
    ((StringBuilder)localObject2).append(bool1);
    ((StringBuilder)localObject2).append(", validPackage=");
    bool1 = a(paramStatusBarNotification.getPackageName());
    ((StringBuilder)localObject2).append(bool1);
    localObject2 = ((StringBuilder)localObject2).toString();
    bool1 = false;
    localObject1[0] = localObject2;
    localObject1 = "enhancedNotificationsEnabled";
    boolean bool2 = Settings.e((String)localObject1);
    if (bool2)
    {
      localObject1 = d;
      bool2 = ((al)localObject1).a();
      if (bool2)
      {
        localObject1 = paramStatusBarNotification.getPackageName();
        bool2 = a((String)localObject1);
        if (bool2) {
          break label159;
        }
      }
    }
    j = 0;
    label159:
    if (j == 0) {
      return;
    }
    try
    {
      c(paramStatusBarNotification);
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      d.a(localRuntimeException, "Error handling notification");
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */