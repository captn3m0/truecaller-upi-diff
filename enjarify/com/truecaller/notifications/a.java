package com.truecaller.notifications;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;

public abstract interface a
{
  public abstract PendingIntent a(PendingIntent paramPendingIntent, String paramString1, String paramString2);
  
  public abstract void a(int paramInt);
  
  public abstract void a(int paramInt, Notification paramNotification, String paramString);
  
  public abstract void a(Intent paramIntent);
  
  public abstract void a(String paramString, int paramInt);
  
  public abstract void a(String paramString1, int paramInt, Notification paramNotification, String paramString2);
  
  public abstract void a(String paramString1, int paramInt, Notification paramNotification, String paramString2, Bundle paramBundle);
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */