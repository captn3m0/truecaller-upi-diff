package com.truecaller.notifications;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

public final class g
{
  private final com.truecaller.utils.d a;
  private Handler b;
  
  public g(com.truecaller.utils.d paramd)
  {
    a = paramd;
    paramd = new android/os/Handler;
    paramd.<init>();
    b = paramd;
  }
  
  public final boolean a(Context paramContext, int paramInt)
  {
    Object localObject = a;
    int i = ((com.truecaller.utils.d)localObject).h();
    int j = 22;
    String str;
    if (i >= j)
    {
      localObject = new android/content/Intent;
      str = "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS";
      ((Intent)localObject).<init>(str);
    }
    else
    {
      localObject = new android/content/Intent;
      str = "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS";
      ((Intent)localObject).<init>(str);
    }
    try
    {
      paramContext.startActivity((Intent)localObject);
      paramContext = b;
      localObject = new com/truecaller/notifications/-$$Lambda$g$fY73_hTVk42V30FUs7CHe_60z_Y;
      ((-..Lambda.g.fY73_hTVk42V30FUs7CHe_60z_Y)localObject).<init>(paramInt);
      long l = 500L;
      paramContext.postDelayed((Runnable)localObject, l);
      return true;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      com.truecaller.log.d.a(localActivityNotFoundException, "Cannot start activity");
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */