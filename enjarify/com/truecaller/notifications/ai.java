package com.truecaller.notifications;

import android.net.Uri;
import c.g.b.k;
import com.google.firebase.perf.network.FirebasePerfOkHttpClient;
import com.truecaller.common.h.am;
import com.truecaller.old.data.access.Settings;
import com.truecaller.profile.c;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import okhttp3.ab.a;
import okhttp3.ad;
import okhttp3.ae;
import okhttp3.y;

public final class ai
  implements ah
{
  private final String a;
  private final String b;
  private final String c;
  private final com.truecaller.common.g.a d;
  
  public ai(com.truecaller.common.g.a parama)
  {
    d = parama;
    a = "profile_name";
    b = "profile_country";
    c = "truecaller";
  }
  
  public final String a(String paramString)
  {
    k.b(paramString, "html");
    Object localObject1 = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject1).<init>();
    localObject1 = (Map)localObject1;
    Object localObject2 = a;
    Object localObject3 = c.b(d);
    ((Map)localObject1).put(localObject2, localObject3);
    localObject2 = b;
    localObject3 = d;
    String str = "profileCountryIso";
    localObject3 = ((com.truecaller.common.g.a)localObject3).a(str);
    ((Map)localObject1).put(localObject2, localObject3);
    localObject2 = new org/c/a/a/a/c/d;
    ((org.c.a.a.a.c.d)localObject2).<init>((Map)localObject1);
    if (paramString == null)
    {
      paramString = null;
    }
    else
    {
      localObject1 = new org/c/a/a/a/c/a;
      ((org.c.a.a.a.c.a)localObject1).<init>(paramString);
      int i = paramString.length();
      boolean bool = ((org.c.a.a.a.c.d)localObject2).a((org.c.a.a.a.c.a)localObject1, i);
      if (bool) {
        paramString = ((org.c.a.a.a.c.a)localObject1).toString();
      }
    }
    k.a(paramString, "htmlSubstitute.replace(html)");
    return paramString;
  }
  
  public final boolean a(Uri paramUri)
  {
    k.b(paramUri, "url");
    String str = c;
    paramUri = paramUri.getScheme();
    return k.a(str, paramUri);
  }
  
  public final String b(String paramString)
  {
    k.b(paramString, "url");
    try
    {
      y localy = com.truecaller.common.network.util.d.a();
      Object localObject1 = "language";
      localObject1 = Settings.b((String)localObject1);
      Object localObject2 = "Settings.get(Settings.LANGUAGE_ISO)";
      k.a(localObject1, (String)localObject2);
      localObject2 = new java/lang/StringBuilder;
      Object localObject3 = "en;q=0.5";
      ((StringBuilder)localObject2).<init>((String)localObject3);
      localObject3 = localObject1;
      localObject3 = (CharSequence)localObject1;
      boolean bool = am.b((CharSequence)localObject3);
      if (!bool)
      {
        localObject3 = ",";
        ((StringBuilder)localObject2).append((String)localObject3);
        ((StringBuilder)localObject2).append((String)localObject1);
      }
      localObject1 = new okhttp3/ab$a;
      ((ab.a)localObject1).<init>();
      paramString = ((ab.a)localObject1).a(paramString);
      localObject1 = "Accept-Language";
      localObject2 = ((StringBuilder)localObject2).toString();
      paramString = paramString.b((String)localObject1, (String)localObject2);
      paramString = paramString.a();
      paramString = localy.a(paramString);
      paramString = FirebasePerfOkHttpClient.execute(paramString);
      paramString = paramString.d();
      if (paramString != null) {
        return paramString.g();
      }
      return null;
    }
    catch (IOException localIOException) {}
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.ai
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */