package com.truecaller.notifications;

import android.content.Context;
import com.truecaller.common.h.t;
import com.truecaller.util.al;
import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  public static e a(Context paramContext, t paramt, al paramal, com.truecaller.notificationchannels.e parame, a parama)
  {
    e locale = new com/truecaller/notifications/e;
    locale.<init>(paramContext, paramt, paramal, parame, parama);
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */