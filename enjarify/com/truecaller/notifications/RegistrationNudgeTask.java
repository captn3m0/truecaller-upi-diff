package com.truecaller.notifications;

import android.content.Context;
import android.os.Bundle;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.h.am;
import com.truecaller.util.b.af;
import com.truecaller.util.b.at;
import com.truecaller.util.f;
import java.util.concurrent.TimeUnit;

public final class RegistrationNudgeTask
  extends PersistentBackgroundTask
{
  public static final RegistrationNudgeTask.a e;
  public com.truecaller.abtest.c a;
  public com.truecaller.analytics.b b;
  public aa c;
  public com.truecaller.featuretoggles.e d;
  
  static
  {
    RegistrationNudgeTask.a locala = new com/truecaller/notifications/RegistrationNudgeTask$a;
    locala.<init>((byte)0);
    e = locala;
  }
  
  public RegistrationNudgeTask()
  {
    new String[1][0] = "RegistrationNudgeTask: Init";
    Object localObject = "regNudgeLastShown";
    long l1 = 0L;
    long l2 = com.truecaller.common.b.e.a((String)localObject, l1);
    boolean bool = l2 < l1;
    if (!bool)
    {
      l1 = System.currentTimeMillis();
      com.truecaller.common.b.e.b("regNudgeLastShown", l1);
      localObject = "regNudgeBadgeStartTime";
      l1 = System.currentTimeMillis();
      com.truecaller.common.b.e.b((String)localObject, l1);
    }
    localObject = TrueApp.y();
    String str = "TrueApp.getApp()";
    c.g.b.k.a(localObject, str);
    ((TrueApp)localObject).a().a(this);
    localObject = a;
    if (localObject == null)
    {
      str = "mFirebaseRemoteConfig";
      c.g.b.k.a(str);
    }
    ((com.truecaller.abtest.c)localObject).a();
  }
  
  private static boolean a(RegistrationNudgeTask.TaskState paramTaskState)
  {
    RegistrationNudgeTask.TaskState localTaskState1 = b(paramTaskState);
    RegistrationNudgeTask.TaskState localTaskState2 = RegistrationNudgeTask.TaskState.DONE;
    if (localTaskState1 != localTaskState2)
    {
      localTaskState1 = RegistrationNudgeTask.TaskState.DONE;
      if (paramTaskState != localTaskState1) {
        return false;
      }
    }
    return true;
  }
  
  private static RegistrationNudgeTask.TaskState b(RegistrationNudgeTask.TaskState paramTaskState)
  {
    int[] arrayOfInt = ad.a;
    int i = paramTaskState.ordinal();
    i = arrayOfInt[i];
    switch (i)
    {
    default: 
      return RegistrationNudgeTask.TaskState.DONE;
    case 4: 
      return RegistrationNudgeTask.TaskState.DONE;
    case 3: 
      return RegistrationNudgeTask.TaskState.THIRD;
    case 2: 
      return RegistrationNudgeTask.TaskState.SECOND;
    }
    return RegistrationNudgeTask.TaskState.FIRST;
  }
  
  public final int a()
  {
    return 10010;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    paramBundle = "serviceContext";
    c.g.b.k.b(paramContext, paramBundle);
    boolean bool1 = e(paramContext);
    if (bool1)
    {
      new String[1][0] = "RegistrationNudgeTask: run without notification";
      return PersistentBackgroundTask.RunResult.FailedSkip;
    }
    paramBundle = paramContext.getApplicationContext();
    if (paramBundle != null)
    {
      paramBundle = ((bk)paramBundle).a().D();
      c.g.b.k.a(paramBundle, "(context.applicationCont…tsGraph.callingSettings()");
      bool1 = paramBundle.b("hasNativeDialerCallerId");
      boolean bool2 = true;
      if (bool1)
      {
        paramBundle = "RegistrationNudgeTask: Native dialer";
        new String[1][0] = paramBundle;
        bool1 = true;
      }
      else
      {
        new String[1][0] = "RegistrationNudgeTask: Not a native dialer";
        bool1 = false;
        paramBundle = null;
      }
      if (!bool1)
      {
        paramBundle = TimeUnit.MILLISECONDS;
        l1 = System.currentTimeMillis();
        localObject1 = "regNudgeBadgeStartTime";
        long l2 = System.currentTimeMillis();
        l3 = 60000L;
        l2 -= l3;
        l4 = com.truecaller.common.b.e.a((String)localObject1, l2);
        l1 -= l4;
        l1 = paramBundle.toDays(l1);
        int i = (int)l1;
        if (i > 0)
        {
          localObject2 = com.facebook.k.f();
          f.a((Context)localObject2, i);
          paramBundle = "regNudgeBadgeSet";
          com.truecaller.common.b.e.b(paramBundle, bool2);
        }
      }
      Object localObject2 = RegistrationNudgeTask.TaskState.INIT.toString();
      paramBundle = com.truecaller.common.b.e.a("registrationNotificationState", (String)localObject2);
      c.g.b.k.a(paramBundle, "CommonSettings.getString…askState.INIT.toString())");
      paramBundle = RegistrationNudgeTask.TaskState.valueOf(paramBundle);
      localObject2 = new String[bool2];
      Object localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("Current State: ");
      Object localObject1 = paramBundle.toString();
      ((StringBuilder)localObject3).append((String)localObject1);
      localObject3 = ((StringBuilder)localObject3).toString();
      localObject2[0] = localObject3;
      boolean bool3 = a(paramBundle);
      if (bool3)
      {
        new String[1][0] = "RegistrationNudgeTask: run without notification";
        return PersistentBackgroundTask.RunResult.FailedSkip;
      }
      localObject2 = "regNudgeLastShown";
      long l5 = 0L;
      long l1 = com.truecaller.common.b.e.a((String)localObject2, l5);
      localObject1 = b(paramBundle);
      long l4 = ((RegistrationNudgeTask.TaskState)localObject1).getInterval();
      Object localObject4 = new org/a/a/b;
      ((org.a.a.b)localObject4).<init>(l1);
      long l3 = 1000L * l4;
      localObject4 = ((org.a.a.b)localObject4).b(l3);
      l3 = System.currentTimeMillis();
      boolean bool4 = ((org.a.a.b)localObject4).d(l3);
      Object localObject5;
      if (bool4)
      {
        localObject1 = new String[bool2];
        localObject5 = new java/lang/StringBuilder;
        localObject4 = "RegistrationNudgeTask: Time past: ";
        ((StringBuilder)localObject5).<init>((String)localObject4);
        long l6 = System.currentTimeMillis() - l1;
        ((StringBuilder)localObject5).append(l6);
        localObject2 = ((StringBuilder)localObject5).toString();
        localObject1[0] = localObject2;
        bool3 = true;
      }
      else
      {
        localObject2 = new String[bool2];
        localObject1 = String.valueOf(l4);
        localObject3 = "RegistrationNudgeTask: Wait for next iteration ".concat((String)localObject1);
        localObject2[0] = localObject3;
        bool3 = false;
        localObject2 = null;
      }
      if (bool3)
      {
        new String[1][0] = "RegistrationNudgeTask: show notification";
        paramBundle = b(paramBundle);
        localObject2 = c;
        if (localObject2 == null)
        {
          localObject3 = "mRegistrationNudgeHelper";
          c.g.b.k.a((String)localObject3);
        }
        int j = paramBundle.getTitle();
        int k = paramBundle.getText();
        localObject5 = paramBundle.toString();
        ((aa)localObject2).a(paramContext, j, k, (String)localObject5);
        localObject2 = paramBundle.toString();
        com.truecaller.common.b.e.b("registrationNotificationState", (String)localObject2);
        l1 = System.currentTimeMillis();
        com.truecaller.common.b.e.b("regNudgeLastShown", l1);
        paramContext = b;
        if (paramContext == null)
        {
          localObject2 = "mAnalytics";
          c.g.b.k.a((String)localObject2);
        }
        localObject2 = new com/truecaller/analytics/e$a;
        ((com.truecaller.analytics.e.a)localObject2).<init>("Notification");
        localObject2 = ((com.truecaller.analytics.e.a)localObject2).a("Type", "regNudge");
        localObject1 = am.a(paramBundle.toString());
        localObject2 = ((com.truecaller.analytics.e.a)localObject2).a("Status", (String)localObject1).a();
        localObject3 = "AnalyticsEvent.Builder(A…tate.toString())).build()";
        c.g.b.k.a(localObject2, (String)localObject3);
        paramContext.a((com.truecaller.analytics.e)localObject2);
        paramContext = new String[bool2];
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localObject2 = "RegistrationNudgeTask: Moved to State: ";
        localStringBuilder.<init>((String)localObject2);
        paramBundle = paramBundle.toString();
        localStringBuilder.append(paramBundle);
        paramBundle = localStringBuilder.toString();
        paramContext[0] = paramBundle;
      }
      return PersistentBackgroundTask.RunResult.Success;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramContext;
  }
  
  public final boolean a(Context paramContext)
  {
    c.g.b.k.b(paramContext, "serviceContext");
    String str = RegistrationNudgeTask.TaskState.INIT.toString();
    Object localObject = com.truecaller.common.b.e.a("registrationNotificationState", str);
    str = "CommonSettings.getString…askState.INIT.toString())";
    c.g.b.k.a(localObject, str);
    localObject = RegistrationNudgeTask.TaskState.valueOf((String)localObject);
    boolean bool1 = e(paramContext);
    if (!bool1)
    {
      boolean bool2 = a((RegistrationNudgeTask.TaskState)localObject);
      if (!bool2)
      {
        paramContext = at.a(paramContext);
        boolean bool3 = paramContext instanceof af;
        if (!bool3) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final com.truecaller.common.background.e b()
  {
    Object localObject = new com/truecaller/common/background/e$a;
    int i = 1;
    ((com.truecaller.common.background.e.a)localObject).<init>(i);
    TimeUnit localTimeUnit = TimeUnit.MINUTES;
    localObject = ((com.truecaller.common.background.e.a)localObject).a(60, localTimeUnit);
    localTimeUnit = TimeUnit.MINUTES;
    localObject = ((com.truecaller.common.background.e.a)localObject).c(10, localTimeUnit).a(i).b();
    c.g.b.k.a(localObject, "TaskConfiguration.Builde…ANY)\n            .build()");
    return (com.truecaller.common.background.e)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.RegistrationNudgeTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */