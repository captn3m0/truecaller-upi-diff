package com.truecaller.notifications;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.g;
import android.support.v4.content.b;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.notificationchannels.e;
import com.truecaller.ui.TruecallerInit;

public final class ab
  implements aa
{
  private final a a;
  
  public ab(a parama)
  {
    a = parama;
  }
  
  public final void a(Context paramContext, int paramInt1, int paramInt2, String paramString)
  {
    k.b(paramContext, "context");
    k.b(paramString, "type");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, TruecallerInit.class);
    int i = 268468224;
    localIntent.addFlags(i);
    localIntent.putExtra("EXTRA_REG_NUDGE", paramString);
    paramString = new android/support/v4/app/z$d;
    Object localObject1 = paramContext.getApplicationContext();
    if (localObject1 != null)
    {
      localObject1 = ((bk)localObject1).a().aC().a();
      paramString.<init>(paramContext, (String)localObject1);
      Object localObject2 = (CharSequence)paramContext.getString(paramInt1);
      localObject2 = paramString.a((CharSequence)localObject2);
      paramString = (CharSequence)paramContext.getString(paramInt2);
      localObject2 = ((z.d)localObject2).b(paramString);
      paramString = new android/support/v4/app/z$c;
      paramString.<init>();
      Object localObject3 = (CharSequence)paramContext.getString(paramInt2);
      localObject3 = (z.g)paramString.b((CharSequence)localObject3);
      localObject2 = ((z.d)localObject2).a((z.g)localObject3);
      paramInt2 = b.c(paramContext, 2131100594);
      localObject2 = ((z.d)localObject2).f(paramInt2).c(-1).a(2131234787);
      paramContext = PendingIntent.getActivity(paramContext, 0, localIntent, 0);
      paramContext = ((z.d)localObject2).a(paramContext).e();
      localObject2 = a;
      paramContext = paramContext.h();
      k.a(paramContext, "builder.build()");
      ((a)localObject2).a(2131362836, paramContext, "notificationRegistrationNudge");
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */