package com.truecaller.notifications;

import android.content.Context;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import com.google.c.a.g;
import com.truecaller.common.h.aa;
import com.truecaller.data.entity.Contact;
import com.truecaller.network.search.j;
import com.truecaller.network.search.n;
import java.io.IOException;
import java.util.UUID;

public abstract class h
{
  static Contact a(Context paramContext, String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool) {
      return null;
    }
    try
    {
      aa.a(paramString);
      try
      {
        j localj = new com/truecaller/network/search/j;
        UUID localUUID = UUID.randomUUID();
        String str = "notification";
        localj.<init>(paramContext, localUUID, str);
        i = paramString;
        paramContext = localj.a();
        int i = 1;
        b = i;
        d = i;
        e = i;
        i = 0;
        paramString = null;
        f = false;
        i = 19;
        h = i;
        paramContext = paramContext.f();
        if (paramContext != null) {
          return paramContext.a();
        }
      }
      catch (IOException|RuntimeException localIOException) {}
      return null;
    }
    catch (g localg) {}
    return null;
  }
  
  public void a() {}
  
  public void a(StatusBarNotification paramStatusBarNotification) {}
  
  public void b(StatusBarNotification paramStatusBarNotification) {}
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */