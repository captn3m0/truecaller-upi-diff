package com.truecaller.notifications;

import c.g.b.k;

public final class ak
{
  final String a;
  final boolean b;
  final int c;
  
  public ak(String paramString, boolean paramBoolean, int paramInt)
  {
    a = paramString;
    b = paramBoolean;
    c = paramInt;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof ak;
      if (bool2)
      {
        paramObject = (ak)paramObject;
        String str1 = a;
        String str2 = a;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          bool2 = b;
          boolean bool3 = b;
          if (bool2 == bool3)
          {
            bool2 = true;
          }
          else
          {
            bool2 = false;
            str1 = null;
          }
          if (bool2)
          {
            int i = c;
            int j = c;
            if (i == j)
            {
              j = 1;
            }
            else
            {
              j = 0;
              paramObject = null;
            }
            if (j != 0) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str = a;
    if (str != null)
    {
      i = str.hashCode();
    }
    else
    {
      i = 0;
      str = null;
    }
    i *= 31;
    int j = b;
    if (j != 0) {
      j = 1;
    }
    int i = (i + j) * 31;
    int k = c;
    return i + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("WhatsAppNotification(title=");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append(", isVideo=");
    boolean bool = b;
    localStringBuilder.append(bool);
    localStringBuilder.append(", actionsSize=");
    int i = c;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.ak
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */