package com.truecaller.notifications;

import c.a.al;
import c.u;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.f;
import com.truecaller.log.AssertionUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class y
{
  public static final y.a a;
  private final e b;
  
  static
  {
    y.a locala = new com/truecaller/notifications/y$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public y(e parame)
  {
    b = parame;
  }
  
  public final y.b a(String paramString)
  {
    String str1 = paramString;
    c.g.b.k.b(paramString, "messageBody");
    Object localObject1 = paramString;
    localObject1 = (CharSequence)paramString;
    Object localObject2 = { "." };
    localObject1 = c.n.m.c((CharSequence)localObject1, (String[])localObject2, false, 6);
    boolean bool1 = ((List)localObject1).isEmpty();
    int j = 1;
    if (!bool1)
    {
      int i = ((List)localObject1).size();
      localObject2 = ((List)localObject1).listIterator(i);
      int k;
      do
      {
        boolean bool2 = ((ListIterator)localObject2).hasPrevious();
        if (!bool2) {
          break;
        }
        localObject3 = (CharSequence)((ListIterator)localObject2).previous();
        k = ((CharSequence)localObject3).length();
        if (k == 0)
        {
          k = 1;
        }
        else
        {
          k = 0;
          localObject3 = null;
        }
      } while (k != 0);
      localObject1 = (Iterable)localObject1;
      i = ((ListIterator)localObject2).nextIndex() + j;
      localObject1 = c.a.m.d((Iterable)localObject1, i);
    }
    else
    {
      localObject1 = (List)c.a.y.a;
    }
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    Object localObject3 = localObject1;
    localObject3 = ((Iterable)localObject1).iterator();
    CharSequence localCharSequence;
    Object localObject7;
    boolean bool4;
    Object localObject8;
    String str2;
    for (;;)
    {
      bool3 = ((Iterator)localObject3).hasNext();
      if (!bool3) {
        break;
      }
      localObject4 = (CharSequence)((Iterator)localObject3).next();
      localObject5 = new c/n/k;
      ((c.n.k)localObject5).<init>("\\s");
      localObject4 = ((c.n.k)localObject5).a((CharSequence)localObject4, 0);
      c.g.b.k.b(localObject4, "receiver$0");
      localObject6 = new c/a/al;
      ((al)localObject6).<init>((List)localObject4);
      localObject6 = (List)localObject6;
      localObject5 = localObject6;
      localObject5 = (Iterable)localObject6;
      localCharSequence = (CharSequence)" ";
      localObject7 = null;
      bool4 = false;
      localObject8 = null;
      str2 = null;
      int i1 = 62;
      localObject4 = c.a.m.a((Iterable)localObject5, localCharSequence, null, null, 0, null, null, i1);
      ((ArrayList)localObject2).add(localObject4);
    }
    localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    localObject1 = (Collection)localObject1;
    ((ArrayList)localObject3).addAll((Collection)localObject1);
    localObject2 = (Collection)localObject2;
    ((ArrayList)localObject3).addAll((Collection)localObject2);
    localObject2 = this;
    localObject1 = (CharSequence)b.aj().e();
    Object localObject4 = (CharSequence)"(?:(from)?.*\\b(?:ending|PAYTMWALLETLOADING|PAYUZOMATOMEDIAPVTLT|one.time.password|your.code|slack|Billdk|netbanking).*\\b.*(is|code):?\\s+)\\b(\\d{3,8})/.*?(?:verification|security|confirmation|validation).(?:code|otp).*(?:is|:).*?(\\b\\d{3,8}\\d)/(?:.*otp.*?is\\s)(?=.*?\\btr?xn\\b.*?)\\b(\\d{3,8})\\b/(?:otp is\\s)\\b(\\d{3,8})\\b/(?=.*?\\botp\\b.*?)(?:[\\s.,:\\-\\[]|^)(\\d{3,8})(?:[\\s.,\\]]|$)/(\\b[\\d-]{3,6}\\d).*?(?:verification|security|validation|twitter.login|login|password.reset).code/.*?(?:verification|security|confirmation|validation).code.*?(\\b\\d{3,8}\\d)/(?=.*?\\b(?:one.time.*?password|password.time.one|password.one-time|facebook)\\b.*?)\\b\\d{3,8}\\b/(?=.*?\\b(?:code.telegram|code..?imo|code.auto-verify|verify.*?grofers|key.v-authentication|banking.*?mpin|to.proceed.with.your.transaction|account.*?verify)\\b.*?)\\b\\d{3,8}\\b/(?:whatsapp|snapchat|uber|eventshigh|truecaller|signal.verification|discord.verification).code.*?([\\d-\\s]{3,8})/(?:enter|use).*?(?:code)*.*?(\\d{3,8}).(?:(?:to|as).*?(?:activate|allow|verify|confirm|sign.in))/to.*?enter.pin..\\b(\\d{3,8}\\b)/(?:otp|code|tiptapp.pin|pin|transaction.password|URN)[-:]?\\s?(is)?\\s?<?(\\b\\d{4,8}\\b)>?(?: ShareChat.*)?$/([\\d-]{3,8})\\b\\n?.*code.*(?:Microsoft).(?:verification)";
    localObject1 = (String)org.c.a.a.a.k.d((CharSequence)localObject1, (CharSequence)localObject4);
    c.g.b.k.a(localObject1, "regexString");
    localObject4 = localObject1;
    localObject4 = (CharSequence)localObject1;
    boolean bool6 = c.n.m.a((CharSequence)localObject4);
    Object localObject5 = { "Regex string is empty or null" };
    AssertionUtil.isFalse(bool6, (String[])localObject5);
    boolean bool3 = c.n.m.a((CharSequence)localObject4);
    bool6 = false;
    Object localObject6 = null;
    boolean bool7;
    if (bool3)
    {
      bool7 = false;
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      localObject1 = (CharSequence)localObject1;
      localObject4 = "/";
      localObject5 = new c/n/k;
      ((c.n.k)localObject5).<init>((String)localObject4);
      localObject1 = ((c.n.k)localObject5).a((CharSequence)localObject1, 0);
      bool3 = ((List)localObject1).isEmpty();
      int i2;
      if (!bool3)
      {
        int m = ((List)localObject1).size();
        localObject4 = ((List)localObject1).listIterator(m);
        do
        {
          boolean bool8 = ((ListIterator)localObject4).hasPrevious();
          if (!bool8) {
            break;
          }
          localObject5 = (CharSequence)((ListIterator)localObject4).previous();
          i2 = ((CharSequence)localObject5).length();
          if (i2 == 0)
          {
            i2 = 1;
          }
          else
          {
            i2 = 0;
            localObject5 = null;
          }
        } while (i2 != 0);
        localObject1 = (Iterable)localObject1;
        m = ((ListIterator)localObject4).nextIndex() + j;
        localObject1 = c.a.m.d((Iterable)localObject1, m);
      }
      else
      {
        localObject1 = (List)c.a.y.a;
      }
      localObject1 = (Collection)localObject1;
      if (localObject1 != null)
      {
        Object localObject9 = new String[0];
        localObject1 = ((Collection)localObject1).toArray((Object[])localObject9);
        if (localObject1 != null)
        {
          localObject9 = localObject1;
          localObject9 = (String[])localObject1;
          localObject3 = ((Iterable)localObject3).iterator();
          for (;;)
          {
            bool7 = ((Iterator)localObject3).hasNext();
            if (!bool7) {
              break;
            }
            localObject1 = ((Iterator)localObject3).next();
            localObject4 = localObject1;
            localObject4 = (String)localObject1;
            i2 = localObject9.length;
            int i3 = 0;
            localCharSequence = null;
            while (i3 < i2)
            {
              localObject1 = localObject9[i3];
              localObject7 = "(?i)";
              try
              {
                localObject8 = String.valueOf(localObject1);
                localObject7 = ((String)localObject7).concat((String)localObject8);
                localObject7 = Pattern.compile((String)localObject7);
                localObject8 = localObject4;
                localObject8 = (CharSequence)localObject4;
                localObject7 = ((Pattern)localObject7).matcher((CharSequence)localObject8);
                bool4 = ((Matcher)localObject7).find();
                if (bool4)
                {
                  int n = ((Matcher)localObject7).groupCount();
                  localObject7 = ((Matcher)localObject7).group(n);
                  localObject8 = "group";
                  c.g.b.k.a(localObject7, (String)localObject8);
                  localObject8 = "-";
                  str2 = "";
                  localObject7 = c.n.m.b((String)localObject7, (String)localObject8, str2);
                  if (localObject7 != null)
                  {
                    localObject7 = (CharSequence)localObject7;
                    localObject7 = c.n.m.b((CharSequence)localObject7);
                    localObject7 = localObject7.toString();
                    localObject8 = " ";
                    str2 = "";
                    localObject7 = c.n.m.b((String)localObject7, (String)localObject8, str2);
                    localObject8 = localObject7;
                    localObject8 = (CharSequence)localObject7;
                    boolean bool5 = org.c.a.a.a.k.g((CharSequence)localObject8);
                    if (bool5)
                    {
                      localObject8 = new com/truecaller/notifications/y$b;
                      ((y.b)localObject8).<init>(str1, (String)localObject1, (String)localObject7);
                      return (y.b)localObject8;
                    }
                  }
                  else
                  {
                    localObject1 = new c/u;
                    localObject7 = "null cannot be cast to non-null type kotlin.CharSequence";
                    ((u)localObject1).<init>((String)localObject7);
                    throw ((Throwable)localObject1);
                  }
                }
              }
              catch (Exception localException)
              {
                localObject1 = (Throwable)localException;
                AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
                i3 += 1;
              }
            }
          }
        }
        localObject1 = new c/u;
        ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.Array<T>");
        throw ((Throwable)localObject1);
      }
      localObject1 = new c/u;
      ((u)localObject1).<init>("null cannot be cast to non-null type java.util.Collection<T>");
      throw ((Throwable)localObject1);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */