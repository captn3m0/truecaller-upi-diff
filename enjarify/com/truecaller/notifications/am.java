package com.truecaller.notifications;

import android.app.Notification;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import c.g.b.k;
import c.n.m;

public final class am
{
  public static final ak a(StatusBarNotification paramStatusBarNotification, Context paramContext)
  {
    k.b(paramStatusBarNotification, "receiver$0");
    k.b(paramContext, "context");
    Object localObject1 = getNotificationextras;
    String str1 = "android.title";
    localObject1 = ((Bundle)localObject1).getString(str1);
    if (localObject1 == null) {
      localObject1 = "";
    }
    paramContext = a(paramContext);
    int i = 0;
    str1 = null;
    if (paramContext != null)
    {
      Object localObject2 = getNotificationextras;
      String str2 = "android.text";
      localObject2 = ((Bundle)localObject2).getString(str2);
      if (localObject2 != null)
      {
        localObject2 = (CharSequence)localObject2;
        paramContext = (CharSequence)paramContext;
        boolean bool1 = true;
        bool2 = m.a((CharSequence)localObject2, paramContext, bool1);
        paramContext = Boolean.valueOf(bool2);
      }
      else
      {
        bool2 = false;
        paramContext = null;
      }
      if (paramContext != null)
      {
        bool2 = paramContext.booleanValue();
        break label134;
      }
    }
    boolean bool2 = false;
    paramContext = null;
    label134:
    paramStatusBarNotification = getNotificationactions;
    if (paramStatusBarNotification != null) {
      i = paramStatusBarNotification.length;
    }
    paramStatusBarNotification = new com/truecaller/notifications/ak;
    paramStatusBarNotification.<init>((String)localObject1, bool2, i);
    return paramStatusBarNotification;
  }
  
  private static String a(Context paramContext)
  {
    k.b(paramContext, "receiver$0");
    paramContext = paramContext.getPackageManager().getResourcesForApplication("com.whatsapp");
    int i = paramContext.getIdentifier("video_ongoing_call", "string", "com.whatsapp");
    return paramContext.getString(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.am
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */