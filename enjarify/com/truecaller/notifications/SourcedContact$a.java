package com.truecaller.notifications;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class SourcedContact$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    SourcedContact localSourcedContact = new com/truecaller/notifications/SourcedContact;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    int i = paramParcel.readInt();
    if (i != 0)
    {
      long l = paramParcel.readLong();
      localObject1 = Long.valueOf(l);
    }
    else
    {
      i = 0;
      localObject1 = null;
    }
    Object localObject2 = localObject1;
    String str3 = paramParcel.readString();
    String str4 = paramParcel.readString();
    String str5 = paramParcel.readString();
    Object localObject1 = SourcedContact.class.getClassLoader();
    localObject1 = paramParcel.readParcelable((ClassLoader)localObject1);
    Object localObject3 = localObject1;
    localObject3 = (Uri)localObject1;
    localObject1 = SourcedContact.class.getClassLoader();
    paramParcel = paramParcel.readParcelable((ClassLoader)localObject1);
    Object localObject4 = paramParcel;
    localObject4 = (Uri)paramParcel;
    localObject1 = localSourcedContact;
    localSourcedContact.<init>(str1, str2, (Long)localObject2, str3, str4, str5, (Uri)localObject3, (Uri)localObject4);
    return localSourcedContact;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new SourcedContact[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.SourcedContact.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */