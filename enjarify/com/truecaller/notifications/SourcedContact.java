package com.truecaller.notifications;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class SourcedContact
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  final String a;
  final String b;
  final Long c;
  final String d;
  final String e;
  public final String f;
  final Uri g;
  final Uri h;
  
  static
  {
    SourcedContact.a locala = new com/truecaller/notifications/SourcedContact$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public SourcedContact(String paramString1, String paramString2, Long paramLong, String paramString3, String paramString4, String paramString5, Uri paramUri1, Uri paramUri2)
  {
    a = paramString1;
    b = paramString2;
    c = paramLong;
    d = paramString3;
    e = paramString4;
    f = paramString5;
    g = paramUri1;
    h = paramUri2;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    Object localObject = this;
    localObject = (SourcedContact)this;
    boolean bool1 = true;
    if (localObject == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof SourcedContact;
    if (!bool2) {
      return false;
    }
    localObject = a;
    paramObject = (SourcedContact)paramObject;
    String str = a;
    bool2 = k.a(localObject, str) ^ bool1;
    if (bool2) {
      return false;
    }
    localObject = c;
    paramObject = c;
    boolean bool3 = k.a(localObject, paramObject) ^ bool1;
    if (bool3) {
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str = a;
    int i = str.hashCode() * 31;
    Long localLong = c;
    int j;
    if (localLong != null)
    {
      j = localLong.hashCode();
    }
    else
    {
      j = 0;
      localLong = null;
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SourcedContact(packageName=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", label=");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", id=");
    localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", tcId=");
    localObject = d;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", name=");
    localObject = e;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", number=");
    localObject = f;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", photoUri=");
    localObject = g;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", thumbnailPhotoUri=");
    localObject = h;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    Object localObject = a;
    paramParcel.writeString((String)localObject);
    localObject = b;
    paramParcel.writeString((String)localObject);
    localObject = c;
    if (localObject != null)
    {
      int i = 1;
      paramParcel.writeInt(i);
      long l = ((Long)localObject).longValue();
      paramParcel.writeLong(l);
    }
    else
    {
      localObject = null;
      paramParcel.writeInt(0);
    }
    localObject = d;
    paramParcel.writeString((String)localObject);
    localObject = e;
    paramParcel.writeString((String)localObject);
    localObject = f;
    paramParcel.writeString((String)localObject);
    localObject = g;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    localObject = h;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.SourcedContact
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */