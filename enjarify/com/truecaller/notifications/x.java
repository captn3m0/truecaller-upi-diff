package com.truecaller.notifications;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import c.g.b.k;
import com.truecaller.messaging.notifications.NotificationIdentifier;

public final class x
{
  public static final Intent a(Context paramContext, long paramLong, NotificationIdentifier paramNotificationIdentifier)
  {
    k.b(paramContext, "context");
    k.b(paramNotificationIdentifier, "otpNotificationId");
    return b(paramContext, "ACTION_MARK_MESSAGE_READ", paramLong, paramNotificationIdentifier);
  }
  
  public static final Intent a(Context paramContext, String paramString, long paramLong, NotificationIdentifier paramNotificationIdentifier)
  {
    k.b(paramContext, "context");
    k.b(paramString, "otp");
    k.b(paramNotificationIdentifier, "otpNotificationId");
    String[] arrayOfString = new String[1];
    String str1 = String.valueOf(paramNotificationIdentifier);
    String str2 = "notificationid: ".concat(str1);
    arrayOfString[0] = str2;
    paramContext = b(paramContext, "ACTION_COPY_OTP", paramLong, paramNotificationIdentifier);
    paramContext.putExtra("OTP", paramString);
    return paramContext;
  }
  
  public static final Intent b(Context paramContext, long paramLong, NotificationIdentifier paramNotificationIdentifier)
  {
    k.b(paramContext, "context");
    k.b(paramNotificationIdentifier, "otpNotificationId");
    return b(paramContext, "ACTION_DISMISS_OTP", paramLong, paramNotificationIdentifier);
  }
  
  private static final Intent b(Context paramContext, String paramString, long paramLong, NotificationIdentifier paramNotificationIdentifier)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, OTPCopierService.class);
    localIntent.putExtra("MESSAGE_ID", paramLong);
    paramNotificationIdentifier = (Parcelable)paramNotificationIdentifier;
    localIntent.putExtra("OTP_NOTIFICATION_ID", paramNotificationIdentifier);
    localIntent.setAction(paramString);
    return localIntent;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */