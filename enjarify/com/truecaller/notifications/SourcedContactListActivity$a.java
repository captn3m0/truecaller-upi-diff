package com.truecaller.notifications;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import com.d.b.aa;
import com.d.b.ac;
import com.d.b.ac.a;
import com.d.b.w.d;
import java.io.IOException;

final class SourcedContactListActivity$a
  extends ac
{
  private final PackageManager a;
  
  SourcedContactListActivity$a(Context paramContext)
  {
    paramContext = paramContext.getPackageManager();
    a = paramContext;
  }
  
  public final ac.a a(aa paramaa, int paramInt)
  {
    try
    {
      Object localObject1 = a;
      localObject2 = d;
      localObject2 = ((Uri)localObject2).getSchemeSpecificPart();
      w.d locald = null;
      localObject1 = ((PackageManager)localObject1).getApplicationInfo((String)localObject2, 0);
      localObject2 = a;
      localObject1 = ((ApplicationInfo)localObject1).loadIcon((PackageManager)localObject2);
      boolean bool = localObject1 instanceof BitmapDrawable;
      if (bool)
      {
        localObject2 = new com/d/b/ac$a;
        localObject1 = (BitmapDrawable)localObject1;
        localObject1 = ((BitmapDrawable)localObject1).getBitmap();
        locald = w.d.b;
        ((ac.a)localObject2).<init>((Bitmap)localObject1, locald);
        return (ac.a)localObject2;
      }
      int i = ((Drawable)localObject1).getIntrinsicWidth();
      int j = ((Drawable)localObject1).getIntrinsicHeight();
      Bitmap.Config localConfig = Bitmap.Config.ARGB_8888;
      localObject2 = Bitmap.createBitmap(i, j, localConfig);
      Canvas localCanvas = new android/graphics/Canvas;
      localCanvas.<init>((Bitmap)localObject2);
      int k = localCanvas.getWidth();
      int m = localCanvas.getHeight();
      ((Drawable)localObject1).setBounds(0, 0, k, m);
      ((Drawable)localObject1).draw(localCanvas);
      localObject1 = new com/d/b/ac$a;
      locald = w.d.b;
      ((ac.a)localObject1).<init>((Bitmap)localObject2, locald);
      return (ac.a)localObject1;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      Object localObject2 = new java/io/IOException;
      paramaa = String.valueOf(d);
      ((IOException)localObject2).<init>(paramaa);
      ((IOException)localObject2).initCause(localNameNotFoundException);
      throw ((Throwable)localObject2);
    }
  }
  
  public final boolean a(aa paramaa)
  {
    Object localObject = d;
    if (localObject != null)
    {
      localObject = "appicon";
      paramaa = d.getScheme();
      boolean bool = TextUtils.equals((CharSequence)localObject, paramaa);
      if (bool) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.notifications.SourcedContactListActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */