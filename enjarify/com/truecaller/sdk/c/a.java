package com.truecaller.sdk.c;

import android.util.Base64;
import com.truecaller.log.d;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public final class a
{
  /* Error */
  public static boolean a(android.content.Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore 4
    //   3: aconst_null
    //   4: astore 5
    //   6: ldc 7
    //   8: astore 6
    //   10: aload_0
    //   11: aload 6
    //   13: invokevirtual 13	android/content/Context:openFileInput	(Ljava/lang/String;)Ljava/io/FileInputStream;
    //   16: astore 5
    //   18: new 15	java/io/BufferedReader
    //   21: astore_0
    //   22: new 17	java/io/InputStreamReader
    //   25: astore 6
    //   27: aload 6
    //   29: aload 5
    //   31: invokespecial 21	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   34: aload_0
    //   35: aload 6
    //   37: invokespecial 24	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   40: aload_0
    //   41: invokevirtual 28	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   44: astore 6
    //   46: aload 6
    //   48: ifnull +79 -> 127
    //   51: ldc 30
    //   53: astore 7
    //   55: iconst_m1
    //   56: istore 8
    //   58: aload 6
    //   60: aload 7
    //   62: iload 8
    //   64: invokevirtual 36	java/lang/String:split	(Ljava/lang/String;I)[Ljava/lang/String;
    //   67: astore 7
    //   69: aload 7
    //   71: arraylength
    //   72: istore 8
    //   74: iconst_2
    //   75: istore 9
    //   77: iload 8
    //   79: iload 9
    //   81: if_icmpne +46 -> 127
    //   84: aload 7
    //   86: iconst_0
    //   87: aaload
    //   88: astore 10
    //   90: aload 7
    //   92: iload 4
    //   94: aaload
    //   95: astore 7
    //   97: aload_1
    //   98: aload_2
    //   99: aload_3
    //   100: aload 7
    //   102: aload 10
    //   104: invokestatic 41	com/truecaller/sdk/c/a:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    //   107: istore 11
    //   109: iload 11
    //   111: ifeq +16 -> 127
    //   114: aload 5
    //   116: ifnull +8 -> 124
    //   119: aload 5
    //   121: invokevirtual 47	java/io/FileInputStream:close	()V
    //   124: iload 4
    //   126: ireturn
    //   127: aload 6
    //   129: ifnonnull -89 -> 40
    //   132: aload 5
    //   134: ifnull +63 -> 197
    //   137: aload 5
    //   139: invokevirtual 47	java/io/FileInputStream:close	()V
    //   142: goto +55 -> 197
    //   145: astore_0
    //   146: goto +53 -> 199
    //   149: astore_0
    //   150: iload 4
    //   152: anewarray 32	java/lang/String
    //   155: astore_1
    //   156: new 49	java/lang/StringBuilder
    //   159: astore_2
    //   160: ldc 51
    //   162: astore_3
    //   163: aload_2
    //   164: aload_3
    //   165: invokespecial 54	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   168: aload_0
    //   169: invokevirtual 59	java/io/IOException:getMessage	()Ljava/lang/String;
    //   172: astore_0
    //   173: aload_2
    //   174: aload_0
    //   175: invokevirtual 63	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   178: pop
    //   179: aload_2
    //   180: invokevirtual 66	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   183: astore_0
    //   184: aload_1
    //   185: iconst_0
    //   186: aload_0
    //   187: aastore
    //   188: aload 5
    //   190: ifnull +7 -> 197
    //   193: goto -56 -> 137
    //   196: pop
    //   197: iconst_0
    //   198: ireturn
    //   199: aload 5
    //   201: ifnull +8 -> 209
    //   204: aload 5
    //   206: invokevirtual 47	java/io/FileInputStream:close	()V
    //   209: aload_0
    //   210: athrow
    //   211: pop
    //   212: goto -88 -> 124
    //   215: pop
    //   216: goto -7 -> 209
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	219	0	paramContext	android.content.Context
    //   0	219	1	paramString1	String
    //   0	219	2	paramString2	String
    //   0	219	3	paramString3	String
    //   1	150	4	bool1	boolean
    //   4	201	5	localFileInputStream	java.io.FileInputStream
    //   8	120	6	localObject1	Object
    //   53	48	7	localObject2	Object
    //   56	26	8	i	int
    //   75	7	9	j	int
    //   88	15	10	str	String
    //   107	3	11	bool2	boolean
    //   196	1	12	localIOException1	java.io.IOException
    //   211	1	13	localIOException2	java.io.IOException
    //   215	1	14	localIOException3	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   11	16	145	finally
    //   18	21	145	finally
    //   22	25	145	finally
    //   29	34	145	finally
    //   35	40	145	finally
    //   40	44	145	finally
    //   62	67	145	finally
    //   69	72	145	finally
    //   86	88	145	finally
    //   92	95	145	finally
    //   102	107	145	finally
    //   150	155	145	finally
    //   156	159	145	finally
    //   164	168	145	finally
    //   168	172	145	finally
    //   174	179	145	finally
    //   179	183	145	finally
    //   186	188	145	finally
    //   11	16	149	java/io/IOException
    //   18	21	149	java/io/IOException
    //   22	25	149	java/io/IOException
    //   29	34	149	java/io/IOException
    //   35	40	149	java/io/IOException
    //   40	44	149	java/io/IOException
    //   62	67	149	java/io/IOException
    //   69	72	149	java/io/IOException
    //   86	88	149	java/io/IOException
    //   92	95	149	java/io/IOException
    //   102	107	149	java/io/IOException
    //   137	142	196	java/io/IOException
    //   119	124	211	java/io/IOException
    //   204	209	215	java/io/IOException
  }
  
  private static boolean a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    paramString4 = Base64.decode(paramString4.getBytes(), 0);
    X509EncodedKeySpec localX509EncodedKeySpec = new java/security/spec/X509EncodedKeySpec;
    localX509EncodedKeySpec.<init>(paramString4);
    try
    {
      paramString4 = KeyFactory.getInstance(paramString5);
      paramString4 = paramString4.generatePublic(localX509EncodedKeySpec);
      return a(paramString1, paramString2, paramString3, paramString4);
    }
    catch (InvalidKeySpecException|NoSuchAlgorithmException localInvalidKeySpecException) {}
    return false;
  }
  
  private static boolean a(String paramString1, String paramString2, String paramString3, PublicKey paramPublicKey)
  {
    boolean bool = false;
    try
    {
      paramString2 = Base64.decode(paramString2, 0);
      paramString1 = paramString1.getBytes();
      paramString3 = Signature.getInstance(paramString3);
      paramString3.initVerify(paramPublicKey);
      paramString3.update(paramString1);
      bool = paramString3.verify(paramString2);
    }
    catch (InvalidKeyException paramString1) {}catch (NoSuchAlgorithmException paramString1) {}catch (SignatureException paramString1) {}
    d.a(paramString1);
    return bool;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */