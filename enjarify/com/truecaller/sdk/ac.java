package com.truecaller.sdk;

import dagger.a.d;
import javax.inject.Provider;

public final class ac
  implements d
{
  private final h a;
  private final Provider b;
  
  private ac(h paramh, Provider paramProvider)
  {
    a = paramh;
    b = paramProvider;
  }
  
  public static ac a(h paramh, Provider paramProvider)
  {
    ac localac = new com/truecaller/sdk/ac;
    localac.<init>(paramh, paramProvider);
    return localac;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */