package com.truecaller.sdk;

import dagger.a.d;
import javax.inject.Provider;

public final class z
  implements d
{
  private final h a;
  private final Provider b;
  
  private z(h paramh, Provider paramProvider)
  {
    a = paramh;
    b = paramProvider;
  }
  
  public static z a(h paramh, Provider paramProvider)
  {
    z localz = new com/truecaller/sdk/z;
    localz.<init>(paramh, paramProvider);
    return localz;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */