package com.truecaller.sdk.a;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.b;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.sdk.R.color;
import com.truecaller.sdk.R.dimen;
import com.truecaller.sdk.R.id;

public class d$b
  extends RecyclerView.ViewHolder
{
  final TextView a;
  
  public d$b(View paramView)
  {
    super(paramView);
    int i = R.id.text;
    paramView = paramView.findViewById(i);
    k.a(paramView, "itemView.findViewById(id.text)");
    paramView = (TextView)paramView;
    a = paramView;
  }
  
  public void a()
  {
    TextView localTextView = a;
    localTextView.setTextSize(12.0F);
    Object localObject = localTextView.getContext();
    int i = R.color.TextMain;
    int j = b.c((Context)localObject, i);
    localTextView.setTextColor(j);
    localObject = localTextView.getResources();
    i = R.dimen.info_item_vertical_padding;
    j = ((Resources)localObject).getDimensionPixelSize(i);
    localTextView.setPadding(0, 0, 0, j);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.a.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */