package com.truecaller.sdk.a;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.sdk.R.id;

public final class f$a
  extends RecyclerView.ViewHolder
{
  final TextView a;
  final ImageView b;
  
  public f$a(View paramView)
  {
    super(paramView);
    int i = R.id.textMain;
    Object localObject = paramView.findViewById(i);
    k.a(localObject, "itemView.findViewById(R.id.textMain)");
    localObject = (TextView)localObject;
    a = ((TextView)localObject);
    i = R.id.imageLeft;
    paramView = paramView.findViewById(i);
    k.a(paramView, "itemView.findViewById(R.id.imageLeft)");
    paramView = (ImageView)paramView;
    b = paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.a.f.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */