package com.truecaller.sdk.a;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.b;
import android.view.View;
import android.widget.TextView;
import com.truecaller.sdk.R.color;
import com.truecaller.sdk.R.dimen;

public final class d$a
  extends d.b
{
  public d$a(View paramView)
  {
    super(paramView);
  }
  
  public final void a()
  {
    TextView localTextView = a;
    Object localObject = localTextView.getContext();
    int i = R.color.truecaller_blue;
    int j = b.c((Context)localObject, i);
    localTextView.setTextColor(j);
    localTextView.setTextSize(12.0F);
    localObject = localTextView.getResources();
    i = R.dimen.info_item_vertical_padding;
    j = ((Resources)localObject).getDimensionPixelSize(i);
    localTextView.setPadding(0, 0, 0, j);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.a.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */