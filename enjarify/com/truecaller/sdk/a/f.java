package com.truecaller.sdk.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import java.util.List;

public final class f
  extends RecyclerView.Adapter
{
  private final Context a;
  private final List b;
  
  public f(Context paramContext, List paramList)
  {
    a = paramContext;
    b = paramList;
  }
  
  public final int getItemCount()
  {
    return b.size();
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */