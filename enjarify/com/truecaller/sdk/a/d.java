package com.truecaller.sdk.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import java.util.List;

public final class d
  extends RecyclerView.Adapter
{
  public List a;
  public int b;
  private final LayoutInflater c;
  
  private d(Context paramContext, List paramList)
  {
    a = paramList;
    b = 2;
    paramContext = LayoutInflater.from(paramContext);
    c = paramContext;
  }
  
  public d(Context paramContext, List paramList, char paramChar)
  {
    this(paramContext, paramList, (byte)0);
  }
  
  public final int getItemCount()
  {
    return b;
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public final int getItemViewType(int paramInt)
  {
    return ((c)a.get(paramInt)).b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */