package com.truecaller.sdk.a;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.b;
import android.view.View;
import android.widget.TextView;
import com.truecaller.sdk.R.color;
import com.truecaller.sdk.R.dimen;
import com.truecaller.sdk.R.drawable;

public final class d$c
  extends d.b
{
  public d$c(View paramView)
  {
    super(paramView);
  }
  
  public final void a()
  {
    TextView localTextView = a;
    Object localObject = localTextView.getContext();
    int i = R.color.TextTitle;
    int j = b.c((Context)localObject, i);
    localTextView.setTextColor(j);
    localTextView.setTextSize(14.0F);
    j = R.drawable.ic_sdk_verified;
    localTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, j, 0);
    localObject = localTextView.getResources();
    int k = R.dimen.truesdk_horizontal_padding;
    j = ((Resources)localObject).getDimensionPixelSize(k);
    localTextView.setCompoundDrawablePadding(j);
    localObject = localTextView.getResources();
    k = R.dimen.info_item_vertical_padding;
    j = ((Resources)localObject).getDimensionPixelSize(k);
    localTextView.setPadding(0, j, 0, j);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.a.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */