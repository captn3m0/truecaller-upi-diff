package com.truecaller.sdk;

import com.truecaller.android.sdk.PartnerInformation;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.h;
import com.truecaller.sdk.push.PushAppData;
import e.b;
import e.d;

public final class ao
{
  public static void a(PartnerInformation paramPartnerInformation, d paramd)
  {
    String str = partnerKey;
    paramPartnerInformation = reqNonce;
    ((ar.a)h.a(KnownEndpoints.API, ar.a.class)).a(str, paramPartnerInformation).a(paramd);
  }
  
  public static void a(PushAppData paramPushAppData, d paramd)
  {
    paramPushAppData = a;
    ((ar.f)h.a(KnownEndpoints.API, ar.f.class)).a(paramPushAppData).a(paramd);
  }
  
  public static void a(String paramString, d paramd)
  {
    ((ar.d)h.a(KnownEndpoints.API, ar.d.class)).a(paramString).a(paramd);
  }
  
  public final void a(PartnerInformation paramPartnerInformation)
  {
    Object localObject = partnerKey;
    paramPartnerInformation = reqNonce;
    paramPartnerInformation = ((ar.c)h.a(KnownEndpoints.API, ar.c.class)).a((String)localObject, paramPartnerInformation);
    localObject = new com/truecaller/sdk/ao$3;
    ((ao.3)localObject).<init>(this);
    paramPartnerInformation.a((d)localObject);
  }
  
  public final void a(PartnerInformation paramPartnerInformation, String paramString)
  {
    Object localObject1 = partnerKey;
    Object localObject2 = reqNonce;
    localObject1 = ((ar.b)h.a(KnownEndpoints.API, ar.b.class)).a((String)localObject1, (String)localObject2);
    localObject2 = new com/truecaller/sdk/ao$2;
    ((ao.2)localObject2).<init>(this, paramString, paramPartnerInformation);
    ((b)localObject1).a((d)localObject2);
  }
  
  public final void a(PushAppData paramPushAppData)
  {
    Object localObject = a;
    localObject = ((ar.g)h.a(KnownEndpoints.API, ar.g.class)).a((String)localObject);
    ao.1 local1 = new com/truecaller/sdk/ao$1;
    local1.<init>(this, paramPushAppData);
    ((b)localObject).a(local1);
  }
  
  public final void a(String paramString)
  {
    paramString = ((ar.e)h.a(KnownEndpoints.API, ar.e.class)).a(paramString);
    ao.4 local4 = new com/truecaller/sdk/ao$4;
    local4.<init>(this);
    paramString.a(local4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.ao
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */