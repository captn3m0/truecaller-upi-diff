package com.truecaller.sdk;

import dagger.a.d;
import javax.inject.Provider;

public final class w
  implements d
{
  private final h a;
  private final Provider b;
  
  private w(h paramh, Provider paramProvider)
  {
    a = paramh;
    b = paramProvider;
  }
  
  public static w a(h paramh, Provider paramProvider)
  {
    w localw = new com/truecaller/sdk/w;
    localw.<init>(paramh, paramProvider);
    return localw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */