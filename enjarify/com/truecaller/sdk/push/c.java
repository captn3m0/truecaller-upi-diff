package com.truecaller.sdk.push;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.z.d;
import android.text.TextUtils;
import c.g.b.k;
import c.u;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.notificationchannels.e;
import com.truecaller.notificationchannels.n;
import com.truecaller.notificationchannels.n.a;
import com.truecaller.sdk.ConfirmProfileActivity;
import com.truecaller.sdk.R.drawable;
import com.truecaller.sdk.R.string;
import com.truecaller.sdk.utils.c.a.a;
import com.truecaller.sdk.utils.c.a.a.b;
import com.truecaller.tracking.events.ai;
import com.truecaller.tracking.events.ai.a;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.a.d.d;

public final class c
  implements b
{
  public final void a(Context paramContext, Map paramMap)
  {
    k.b(paramContext, "context");
    k.b(paramMap, "data");
    paramMap = PushAppData.a(paramMap);
    k.a(paramMap, "pushAppData");
    Object localObject1 = new android/os/Bundle;
    ((Bundle)localObject1).<init>();
    Object localObject2 = paramMap;
    localObject2 = (Parcelable)paramMap;
    ((Bundle)localObject1).putParcelable("a", (Parcelable)localObject2);
    Object localObject3 = ConfirmProfileActivity.getLaunchIntent(paramContext, (Bundle)localObject1);
    int i = R.string.SdkNotificationWebSignInTitle;
    int j = 1;
    Object localObject4 = new Object[j];
    Object localObject5 = b;
    String str = null;
    localObject4[0] = localObject5;
    localObject2 = paramContext.getString(i, (Object[])localObject4);
    localObject4 = RingtoneManager.getDefaultUri(2);
    localObject5 = n.a;
    localObject5 = n.a.a(paramContext).c().i();
    int k = 134217728;
    localObject3 = PendingIntent.getActivity(paramContext, 0, (Intent)localObject3, k);
    Object localObject6 = new android/content/Intent;
    Class localClass = SdkActionReceiver.class;
    ((Intent)localObject6).<init>("com.truecaller.sdk.web_request_reject", null, paramContext, localClass);
    ((Intent)localObject6).putExtras((Bundle)localObject1);
    int m = 16;
    localObject6 = PendingIntent.getBroadcast(paramContext, m, (Intent)localObject6, k);
    z.d locald = new android/support/v4/app/z$d;
    locald.<init>(paramContext, (String)localObject5);
    int n = R.drawable.notification_logo;
    localObject5 = locald.a(n);
    localObject2 = (CharSequence)localObject2;
    localObject2 = ((z.d)localObject5).a((CharSequence)localObject2);
    localObject5 = new long[3];
    Object tmp230_228 = localObject5;
    tmp230_228[0] = 500L;
    Object tmp236_230 = tmp230_228;
    tmp236_230[1] = 100;
    tmp236_230[2] = 500L;
    localObject2 = ((z.d)localObject2).a((long[])localObject5).a((Uri)localObject4).a(-16776961, j, j).e(j).b();
    int i1 = R.drawable.ic_notification_done;
    n = R.string.SdkNotificationAccept;
    localObject5 = (CharSequence)paramContext.getString(n);
    localObject2 = ((z.d)localObject2).a(i1, (CharSequence)localObject5, (PendingIntent)localObject3);
    i1 = R.drawable.ic_notification_reject;
    n = R.string.SdkNotificationReject;
    localObject5 = (CharSequence)paramContext.getString(n);
    localObject2 = ((z.d)localObject2).a(i1, (CharSequence)localObject5, (PendingIntent)localObject6);
    i1 = R.string.SdkNotificationOneTapLogin;
    localObject4 = (CharSequence)paramContext.getString(i1);
    localObject3 = ((z.d)localObject2).b((CharSequence)localObject4).a((PendingIntent)localObject3).b((PendingIntent)localObject6);
    localObject2 = (NotificationManager)paramContext.getSystemService("notification");
    if (localObject2 != null)
    {
      localObject3 = ((z.d)localObject3).h();
      ((NotificationManager)localObject2).notify(m, (Notification)localObject3);
    }
    m = c;
    localObject3 = new android/content/Intent;
    ((Intent)localObject3).<init>(paramContext, a.class);
    ((Intent)localObject3).setAction("com.truecaller.sdk.clearnotification");
    localObject3 = PendingIntent.getBroadcast(paramContext, j, (Intent)localObject3, k);
    localObject2 = paramContext.getSystemService("alarm");
    if (localObject2 != null)
    {
      localObject2 = (AlarmManager)localObject2;
      long l1 = System.currentTimeMillis();
      localObject5 = TimeUnit.SECONDS;
      long l2 = m;
      l2 = ((TimeUnit)localObject5).toMillis(l2);
      l1 += l2;
      android.support.v4.app.b.a((AlarmManager)localObject2, 0, l1, (PendingIntent)localObject3);
      k.b(paramContext, "context");
      k.b(paramMap, "pushAppData");
      localObject1 = com.truecaller.common.b.a.F();
      if (localObject1 != null)
      {
        localObject1 = (com.truecaller.sdk.ae)localObject1;
        localObject3 = ai.b();
        localObject2 = new com/truecaller/analytics/e$a;
        Object localObject7 = c.a.a.a;
        ((e.a)localObject2).<init>((String)localObject7);
        localObject7 = new java/util/HashMap;
        ((HashMap)localObject7).<init>();
        localObject4 = c.a.a.b;
        localObject5 = c.a.a.b.a;
        ((e.a)localObject2).a((String)localObject4, (String)localObject5);
        localObject4 = a;
        if (localObject4 != null)
        {
          localObject4 = c.a.a.c;
          localObject5 = a;
          ((e.a)localObject2).a((String)localObject4, (String)localObject5);
          localObject4 = localObject7;
          localObject4 = (Map)localObject7;
          localObject5 = c.a.a.e;
          str = a;
          ((Map)localObject4).put(localObject5, str);
        }
        localObject4 = (CharSequence)b;
        boolean bool1 = TextUtils.isEmpty((CharSequence)localObject4);
        if (!bool1)
        {
          localObject4 = c.a.a.d;
          localObject5 = b;
          ((e.a)localObject2).a((String)localObject4, (String)localObject5);
          localObject4 = localObject7;
          localObject4 = (Map)localObject7;
          localObject5 = c.a.a.f;
          paramMap = b;
          str = "pushAppData.name";
          k.a(paramMap, str);
          ((Map)localObject4).put(localObject5, paramMap);
        }
        k.a(localObject3, "eventWeb");
        localObject7 = (Map)localObject7;
        ((ai.a)localObject3).a((Map)localObject7);
        paramMap = (com.truecaller.analytics.ae)((com.truecaller.sdk.ae)localObject1).r().a();
        localObject1 = (d)((ai.a)localObject3).a();
        paramMap.a((d)localObject1);
        paramContext = paramContext.getApplicationContext();
        boolean bool2 = paramContext instanceof com.truecaller.sdk.ae;
        if (bool2)
        {
          paramContext = ((com.truecaller.sdk.ae)paramContext).s();
          paramMap = ((e.a)localObject2).a();
          localObject1 = "event.build()";
          k.a(paramMap, (String)localObject1);
          paramContext.a(paramMap);
        }
        return;
      }
      paramContext = new c/u;
      paramContext.<init>("null cannot be cast to non-null type com.truecaller.sdk.EventsTrackerHolder");
      throw paramContext;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.app.AlarmManager");
    throw paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.push.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */