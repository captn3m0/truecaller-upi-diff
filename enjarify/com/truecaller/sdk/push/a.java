package com.truecaller.sdk.push;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import c.g.b.k;

public final class a
  extends BroadcastReceiver
{
  public static final a.a a;
  
  static
  {
    a.a locala = new com/truecaller/sdk/push/a$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    k.b(paramContext, "context");
    k.b(paramIntent, "intent");
    String str = "com.truecaller.sdk.clearnotification";
    paramIntent = paramIntent.getAction();
    boolean bool = k.a(str, paramIntent);
    if (bool)
    {
      paramIntent = "notification";
      paramContext = (NotificationManager)paramContext.getSystemService(paramIntent);
      if (paramContext != null)
      {
        paramContext.cancel(16);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.push.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */