package com.truecaller.sdk.push;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.sdk.ae;
import com.truecaller.sdk.ao;

public class SdkActionReceiver
  extends BroadcastReceiver
{
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    Object localObject = "a";
    boolean bool1 = paramIntent.hasExtra((String)localObject);
    if (bool1)
    {
      paramIntent = (PushAppData)paramIntent.getParcelableExtra("a");
      localObject = new android/content/Intent;
      String str1 = "android.intent.action.CLOSE_SYSTEM_DIALOGS";
      ((Intent)localObject).<init>(str1);
      paramContext.sendBroadcast((Intent)localObject);
      localObject = new com/truecaller/sdk/ao;
      ((ao)localObject).<init>();
      ((ao)localObject).a(paramIntent);
      localObject = (NotificationManager)paramContext.getSystemService("notification");
      if (localObject != null)
      {
        int i = 16;
        ((NotificationManager)localObject).cancel(i);
        localObject = new android/content/Intent;
        str1 = "android.intent.action.CLOSE_SYSTEM_DIALOGS";
        ((Intent)localObject).<init>(str1);
        paramContext.sendBroadcast((Intent)localObject);
      }
      localObject = new com/truecaller/analytics/e$a;
      ((e.a)localObject).<init>("TrueSDK_Notification");
      String str2 = "NotificationRejected";
      ((e.a)localObject).a("EventType", str2);
      str1 = a;
      if (str1 != null)
      {
        str1 = "WebRequestId";
        str2 = a;
        ((e.a)localObject).a(str1, str2);
      }
      str1 = b;
      boolean bool2 = TextUtils.isEmpty(str1);
      if (!bool2)
      {
        str1 = "PartnerName";
        paramIntent = b;
        ((e.a)localObject).a(str1, paramIntent);
      }
      paramContext = paramContext.getApplicationContext();
      boolean bool3 = paramContext instanceof ae;
      if (bool3)
      {
        paramContext = ((ae)paramContext).s();
        paramIntent = ((e.a)localObject).a();
        paramContext.a(paramIntent);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.push.SdkActionReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */