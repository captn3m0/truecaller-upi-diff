package com.truecaller.sdk.push;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.Map;
import org.json.JSONObject;

public class PushAppData
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final String a;
  public final String b;
  public final int c;
  
  static
  {
    PushAppData.1 local1 = new com/truecaller/sdk/push/PushAppData$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  private PushAppData(Parcel paramParcel)
  {
    String str = paramParcel.readString();
    a = str;
    str = paramParcel.readString();
    b = str;
    int i = paramParcel.readInt();
    c = i;
  }
  
  private PushAppData(String paramString1, String paramString2, int paramInt)
  {
    a = paramString1;
    b = paramString2;
    c = paramInt;
  }
  
  public static PushAppData a(Map paramMap)
  {
    String str1 = "requestId";
    boolean bool1 = paramMap.containsKey(str1);
    String str2 = null;
    if (bool1)
    {
      str1 = (String)paramMap.get("requestId");
    }
    else
    {
      bool1 = false;
      str1 = null;
    }
    Object localObject = "name";
    boolean bool2 = paramMap.containsKey(localObject);
    if (bool2) {
      str2 = (String)paramMap.get("name");
    }
    localObject = "ttl";
    bool2 = paramMap.containsKey(localObject);
    int i;
    if (bool2)
    {
      localObject = "ttl";
      paramMap = (String)paramMap.get(localObject);
      i = Integer.parseInt(paramMap);
    }
    else
    {
      i = 0;
      paramMap = null;
    }
    localObject = new com/truecaller/sdk/push/PushAppData;
    ((PushAppData)localObject).<init>(str1, str2, i);
    return (PushAppData)localObject;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String toString()
  {
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>();
      String str1 = "requestId";
      String str2 = a;
      localJSONObject.put(str1, str2);
      str1 = "name";
      str2 = b;
      localJSONObject.put(str1, str2);
      str1 = "ttl";
      int i = c;
      localJSONObject.put(str1, i);
      return localJSONObject.toString();
    }
    catch (Exception localException) {}
    return null;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    String str = a;
    paramParcel.writeString(str);
    str = b;
    paramParcel.writeString(str);
    paramInt = c;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.push.PushAppData
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */