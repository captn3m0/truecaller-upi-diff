package com.truecaller.sdk.utils;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal;
import c.g.b.k;
import c.l.g;
import com.truecaller.common.b.a;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;

public final class TcAccountStateProvider
  extends ContentProvider
{
  public Uri a;
  
  public final int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    k.b(paramUri, "uri");
    return 0;
  }
  
  public final String getType(Uri paramUri)
  {
    k.b(paramUri, "uri");
    return "vnd.android.cursor.item/";
  }
  
  public final Uri insert(Uri paramUri, ContentValues paramContentValues)
  {
    k.b(paramUri, "uri");
    return null;
  }
  
  public final boolean onCreate()
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("content://");
    Object localObject2 = getContext();
    Class localClass = getClass();
    localObject2 = com.truecaller.common.c.b.b.a((Context)localObject2, localClass);
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = Uri.withAppendedPath(Uri.parse(((StringBuilder)localObject1).toString()), "tcAccountState");
    k.a(localObject1, "Uri.withAppendedPath(Uri…this.javaClass)}\"), PATH)");
    a = ((Uri)localObject1);
    return true;
  }
  
  public final Cursor query(Uri paramUri, String[] paramArrayOfString, Bundle paramBundle, CancellationSignal paramCancellationSignal)
  {
    k.b(paramUri, "uri");
    return query(paramUri, paramArrayOfString, null, null, null);
  }
  
  public final Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
  {
    k.b(paramUri, "uri");
    paramArrayOfString1 = getContext();
    paramString1 = null;
    if (paramArrayOfString1 != null) {
      paramArrayOfString1 = paramArrayOfString1.getApplicationContext();
    } else {
      paramArrayOfString1 = null;
    }
    int i = paramArrayOfString1 instanceof a;
    if (i == 0) {
      paramArrayOfString1 = null;
    }
    paramArrayOfString1 = (a)paramArrayOfString1;
    if (paramArrayOfString1 == null) {
      return null;
    }
    paramArrayOfString2 = paramArrayOfString1.f();
    paramString2 = C;
    Object localObject = e.a;
    int j = 87;
    localObject = localObject[j];
    paramArrayOfString2 = paramString2.a(paramArrayOfString2, (g)localObject);
    i = paramArrayOfString2.a();
    if (i != 0)
    {
      paramArrayOfString2 = a;
      if (paramArrayOfString2 == null)
      {
        paramString2 = "contentUri";
        k.a(paramString2);
      }
      boolean bool = k.a(paramUri, paramArrayOfString2);
      if (bool)
      {
        paramUri = new android/database/MatrixCursor;
        paramString1 = new String[] { "accountState" };
        i = 1;
        paramUri.<init>(paramString1, i);
        paramString1 = new Integer[i];
        paramArrayOfString1 = Integer.valueOf(paramArrayOfString1.p());
        paramString1[0] = paramArrayOfString1;
        paramUri.addRow(paramString1);
        return (Cursor)paramUri;
      }
    }
    return null;
  }
  
  public final int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    k.b(paramUri, "uri");
    return 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.utils.TcAccountStateProvider
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */