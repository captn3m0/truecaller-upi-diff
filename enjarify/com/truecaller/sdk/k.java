package com.truecaller.sdk;

import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d
{
  private final h a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  private final Provider g;
  private final Provider h;
  private final Provider i;
  private final Provider j;
  private final Provider k;
  private final Provider l;
  private final Provider m;
  private final Provider n;
  private final Provider o;
  private final Provider p;
  private final Provider q;
  
  private k(h paramh, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7, Provider paramProvider8, Provider paramProvider9, Provider paramProvider10, Provider paramProvider11, Provider paramProvider12, Provider paramProvider13, Provider paramProvider14, Provider paramProvider15, Provider paramProvider16)
  {
    a = paramh;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
    l = paramProvider11;
    m = paramProvider12;
    n = paramProvider13;
    o = paramProvider14;
    p = paramProvider15;
    q = paramProvider16;
  }
  
  public static k a(h paramh, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7, Provider paramProvider8, Provider paramProvider9, Provider paramProvider10, Provider paramProvider11, Provider paramProvider12, Provider paramProvider13, Provider paramProvider14, Provider paramProvider15, Provider paramProvider16)
  {
    k localk = new com/truecaller/sdk/k;
    localk.<init>(paramh, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13, paramProvider14, paramProvider15, paramProvider16);
    return localk;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */