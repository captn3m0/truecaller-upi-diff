package com.truecaller.sdk;

import com.truecaller.android.sdk.PartnerInformation;
import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;

public final class ak
  implements aj
{
  private final v a;
  
  public ak(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return aj.class.equals(paramClass);
  }
  
  public final w a(PartnerInformation paramPartnerInformation)
  {
    v localv = a;
    ak.a locala = new com/truecaller/sdk/ak$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramPartnerInformation, (byte)0);
    return w.a(localv, locala);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.ak
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */