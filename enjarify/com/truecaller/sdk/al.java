package com.truecaller.sdk;

import android.content.Context;
import android.util.Base64;
import com.truecaller.android.sdk.PartnerInformation;
import com.truecaller.android.sdk.TrueError;
import com.truecaller.android.sdk.TrueProfile;
import com.truecaller.android.sdk.TrueResponse;
import com.truecaller.androidactors.w;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.h;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.d;
import com.truecaller.sdk.c.a;
import com.truecaller.sdk.c.c.a;
import e.b;
import e.r;
import java.io.IOException;
import okhttp3.ad;
import okhttp3.t;

final class al
  implements aj
{
  private final Context a;
  
  al(Context paramContext)
  {
    a = paramContext;
  }
  
  /* Error */
  private static TrueResponse a(Context paramContext, ap paramap, PartnerInformation paramPartnerInformation, String paramString)
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore 4
    //   3: invokestatic 18	com/truecaller/sdk/al:a	()Le/r;
    //   6: astore 5
    //   8: aload 5
    //   10: ifnonnull +24 -> 34
    //   13: new 20	com/truecaller/android/sdk/TrueResponse
    //   16: astore_0
    //   17: new 22	com/truecaller/android/sdk/TrueError
    //   20: astore_1
    //   21: aload_1
    //   22: iload 4
    //   24: invokespecial 25	com/truecaller/android/sdk/TrueError:<init>	(I)V
    //   27: aload_0
    //   28: aload_1
    //   29: invokespecial 28	com/truecaller/android/sdk/TrueResponse:<init>	(Lcom/truecaller/android/sdk/TrueError;)V
    //   32: aload_0
    //   33: areturn
    //   34: aload 5
    //   36: getfield 33	e/r:a	Lokhttp3/ad;
    //   39: astore 6
    //   41: aload 6
    //   43: invokevirtual 39	okhttp3/ad:c	()Z
    //   46: istore 4
    //   48: iload 4
    //   50: ifeq +212 -> 262
    //   53: aload 5
    //   55: getfield 43	e/r:b	Ljava/lang/Object;
    //   58: checkcast 45	java/util/List
    //   61: astore 6
    //   63: aload 6
    //   65: ifnull +197 -> 262
    //   68: new 47	java/io/File
    //   71: astore 5
    //   73: aload_0
    //   74: invokevirtual 53	android/content/Context:getFilesDir	()Ljava/io/File;
    //   77: astore 7
    //   79: ldc 55
    //   81: astore 8
    //   83: aload 5
    //   85: aload 7
    //   87: aload 8
    //   89: invokespecial 58	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   92: new 60	java/io/FileWriter
    //   95: astore 7
    //   97: aload 7
    //   99: aload 5
    //   101: invokespecial 63	java/io/FileWriter:<init>	(Ljava/io/File;)V
    //   104: new 65	java/io/BufferedWriter
    //   107: astore 5
    //   109: aload 5
    //   111: aload 7
    //   113: invokespecial 68	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
    //   116: aload 6
    //   118: invokeinterface 72 1 0
    //   123: astore 6
    //   125: aload 6
    //   127: invokeinterface 77 1 0
    //   132: istore 9
    //   134: iload 9
    //   136: ifeq +71 -> 207
    //   139: aload 6
    //   141: invokeinterface 81 1 0
    //   146: astore 8
    //   148: aload 8
    //   150: checkcast 83	com/truecaller/sdk/c/b
    //   153: astore 8
    //   155: aload 8
    //   157: getfield 86	com/truecaller/sdk/c/b:a	Ljava/lang/String;
    //   160: astore 10
    //   162: aload 5
    //   164: aload 10
    //   166: invokevirtual 90	java/io/BufferedWriter:write	(Ljava/lang/String;)V
    //   169: ldc 92
    //   171: astore 10
    //   173: aload 5
    //   175: aload 10
    //   177: invokevirtual 90	java/io/BufferedWriter:write	(Ljava/lang/String;)V
    //   180: aload 8
    //   182: getfield 94	com/truecaller/sdk/c/b:b	Ljava/lang/String;
    //   185: astore 8
    //   187: aload 5
    //   189: aload 8
    //   191: invokevirtual 90	java/io/BufferedWriter:write	(Ljava/lang/String;)V
    //   194: aload 5
    //   196: invokevirtual 97	java/io/BufferedWriter:newLine	()V
    //   199: aload 5
    //   201: invokevirtual 100	java/io/BufferedWriter:flush	()V
    //   204: goto -79 -> 125
    //   207: aload 7
    //   209: invokevirtual 103	java/io/FileWriter:close	()V
    //   212: goto +50 -> 262
    //   215: pop
    //   216: goto +46 -> 262
    //   219: astore 6
    //   221: goto +15 -> 236
    //   224: astore_0
    //   225: aconst_null
    //   226: astore 7
    //   228: goto +22 -> 250
    //   231: astore 6
    //   233: aconst_null
    //   234: astore 7
    //   236: aload 6
    //   238: invokestatic 108	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   241: aload 7
    //   243: ifnull +19 -> 262
    //   246: goto -39 -> 207
    //   249: astore_0
    //   250: aload 7
    //   252: ifnull +8 -> 260
    //   255: aload 7
    //   257: invokevirtual 103	java/io/FileWriter:close	()V
    //   260: aload_0
    //   261: athrow
    //   262: aload_1
    //   263: getfield 111	com/truecaller/sdk/ap:b	Ljava/lang/String;
    //   266: astore 6
    //   268: aload_1
    //   269: getfield 112	com/truecaller/sdk/ap:a	Ljava/lang/String;
    //   272: astore_1
    //   273: aload_0
    //   274: aload 6
    //   276: aload_1
    //   277: aload_3
    //   278: invokestatic 117	com/truecaller/sdk/c/a:a	(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    //   281: istore 11
    //   283: iload 11
    //   285: ifne +67 -> 352
    //   288: new 119	java/lang/StringBuilder
    //   291: astore_0
    //   292: aload_0
    //   293: ldc 121
    //   295: invokespecial 123	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   298: aload_2
    //   299: getfield 128	com/truecaller/android/sdk/PartnerInformation:packageName	Ljava/lang/String;
    //   302: astore_1
    //   303: aload_0
    //   304: aload_1
    //   305: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   308: pop
    //   309: aload_0
    //   310: ldc -122
    //   312: invokevirtual 132	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   315: pop
    //   316: aload_0
    //   317: invokevirtual 138	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   320: astore_0
    //   321: iconst_0
    //   322: anewarray 140	java/lang/String
    //   325: astore_1
    //   326: aload_0
    //   327: aload_1
    //   328: invokestatic 146	com/truecaller/log/AssertionUtil:reportWithSummary	(Ljava/lang/String;[Ljava/lang/String;)V
    //   331: new 20	com/truecaller/android/sdk/TrueResponse
    //   334: astore_0
    //   335: new 22	com/truecaller/android/sdk/TrueError
    //   338: astore_1
    //   339: aload_1
    //   340: bipush 8
    //   342: invokespecial 25	com/truecaller/android/sdk/TrueError:<init>	(I)V
    //   345: aload_0
    //   346: aload_1
    //   347: invokespecial 28	com/truecaller/android/sdk/TrueResponse:<init>	(Lcom/truecaller/android/sdk/TrueError;)V
    //   350: aload_0
    //   351: areturn
    //   352: aconst_null
    //   353: areturn
    //   354: invokestatic 108	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   357: new 20	com/truecaller/android/sdk/TrueResponse
    //   360: astore_0
    //   361: new 22	com/truecaller/android/sdk/TrueError
    //   364: astore_1
    //   365: aload_1
    //   366: iload 4
    //   368: invokespecial 25	com/truecaller/android/sdk/TrueError:<init>	(I)V
    //   371: aload_0
    //   372: aload_1
    //   373: invokespecial 28	com/truecaller/android/sdk/TrueResponse:<init>	(Lcom/truecaller/android/sdk/TrueError;)V
    //   376: aload_0
    //   377: areturn
    //   378: pop
    //   379: goto -119 -> 260
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	382	0	paramContext	Context
    //   0	382	1	paramap	ap
    //   0	382	2	paramPartnerInformation	PartnerInformation
    //   0	382	3	paramString	String
    //   1	22	4	i	int
    //   46	321	4	j	int
    //   6	194	5	localObject1	Object
    //   39	101	6	localObject2	Object
    //   219	1	6	localException1	Exception
    //   231	6	6	localException2	Exception
    //   266	9	6	str1	String
    //   77	179	7	localObject3	Object
    //   81	109	8	localObject4	Object
    //   132	3	9	bool1	boolean
    //   160	16	10	str2	String
    //   281	3	11	bool2	boolean
    //   215	1	16	localIOException1	IOException
    //   354	1	17	localIOException2	IOException
    //   378	1	18	localIOException3	IOException
    // Exception table:
    //   from	to	target	type
    //   207	212	215	java/io/IOException
    //   104	107	219	java/lang/Exception
    //   111	116	219	java/lang/Exception
    //   116	123	219	java/lang/Exception
    //   125	132	219	java/lang/Exception
    //   139	146	219	java/lang/Exception
    //   148	153	219	java/lang/Exception
    //   155	160	219	java/lang/Exception
    //   164	169	219	java/lang/Exception
    //   175	180	219	java/lang/Exception
    //   180	185	219	java/lang/Exception
    //   189	194	219	java/lang/Exception
    //   194	199	219	java/lang/Exception
    //   199	204	219	java/lang/Exception
    //   92	95	224	finally
    //   99	104	224	finally
    //   92	95	231	java/lang/Exception
    //   99	104	231	java/lang/Exception
    //   104	107	249	finally
    //   111	116	249	finally
    //   116	123	249	finally
    //   125	132	249	finally
    //   139	146	249	finally
    //   148	153	249	finally
    //   155	160	249	finally
    //   164	169	249	finally
    //   175	180	249	finally
    //   180	185	249	finally
    //   189	194	249	finally
    //   194	199	249	finally
    //   199	204	249	finally
    //   236	241	249	finally
    //   3	6	354	java/io/IOException
    //   255	260	378	java/io/IOException
  }
  
  private static r a()
  {
    Object localObject1 = null;
    int i = 0;
    r localr = null;
    Object localObject2 = null;
    for (;;)
    {
      int j = 2;
      long l;
      if (i < j) {
        try
        {
          localObject2 = KnownEndpoints.API;
          Class localClass = c.a.class;
          localObject2 = h.a((KnownEndpoints)localObject2, localClass);
          localObject2 = (c.a)localObject2;
          localObject2 = ((c.a)localObject2).a();
          localr = ((b)localObject2).c();
        }
        catch (IOException localIOException)
        {
          l = 500L;
        }
      }
      try
      {
        Thread.sleep(l);
        i += 1;
        continue;
        i = 0;
        localr = null;
        localObject1 = localIOException;
        if (localObject1 == null) {
          return localr;
        }
        throw ((Throwable)localObject1);
      }
      catch (InterruptedException localInterruptedException)
      {
        for (;;) {}
      }
    }
  }
  
  private static r b(PartnerInformation paramPartnerInformation)
  {
    Object localObject1 = null;
    int i = 0;
    Object localObject2 = null;
    for (;;)
    {
      int j = 2;
      long l;
      if (i < j) {
        try
        {
          localObject2 = partnerKey;
          String str1 = packageName;
          String str2 = appFingerprint;
          String str3 = reqNonce;
          Object localObject3 = KnownEndpoints.API;
          Class localClass = aq.a.class;
          localObject3 = h.a((KnownEndpoints)localObject3, localClass);
          localObject3 = (aq.a)localObject3;
          localObject2 = ((aq.a)localObject3).a((String)localObject2, str1, str2, str3);
          paramPartnerInformation = ((b)localObject2).c();
        }
        catch (IOException localIOException)
        {
          l = 500L;
        }
      }
      try
      {
        Thread.sleep(l);
        i += 1;
        continue;
        paramPartnerInformation = null;
        localObject1 = localIOException;
        if (localObject1 == null) {
          return paramPartnerInformation;
        }
        throw ((Throwable)localObject1);
      }
      catch (InterruptedException localInterruptedException)
      {
        for (;;) {}
      }
    }
  }
  
  public final w a(PartnerInformation paramPartnerInformation)
  {
    int i = 1;
    try
    {
      localObject1 = b(paramPartnerInformation);
      if (localObject1 == null)
      {
        paramPartnerInformation = new com/truecaller/android/sdk/TrueResponse;
        localObject1 = new com/truecaller/android/sdk/TrueError;
        ((TrueError)localObject1).<init>(i);
        paramPartnerInformation.<init>((TrueError)localObject1);
        return w.b(paramPartnerInformation);
      }
      Object localObject2 = a;
      boolean bool1 = ((ad)localObject2).c();
      if (!bool1)
      {
        localObject2 = a;
        j = c;
        int k = 401;
        int n;
        if (j != k)
        {
          k = 404;
          if (j != k)
          {
            localObject2 = new java/lang/StringBuilder;
            localObject3 = "TrueSDK - Partner: ";
            ((StringBuilder)localObject2).<init>((String)localObject3);
            paramPartnerInformation = packageName;
            ((StringBuilder)localObject2).append(paramPartnerInformation);
            ((StringBuilder)localObject2).append(" - User profile request failed with code: ");
            int m = a.c;
            ((StringBuilder)localObject2).append(m);
            paramPartnerInformation = ((StringBuilder)localObject2).toString();
            localObject2 = new String[0];
            AssertionUtil.reportWithSummary(paramPartnerInformation, (String[])localObject2);
            paramPartnerInformation = new com/truecaller/android/sdk/TrueResponse;
            localObject2 = new com/truecaller/android/sdk/TrueError;
            ((TrueError)localObject2).<init>(0);
            paramPartnerInformation.<init>((TrueError)localObject2);
          }
          else
          {
            paramPartnerInformation = new com/truecaller/android/sdk/TrueResponse;
            localObject2 = new com/truecaller/android/sdk/TrueError;
            n = 3;
            ((TrueError)localObject2).<init>(n);
            paramPartnerInformation.<init>((TrueError)localObject2);
          }
        }
        else
        {
          paramPartnerInformation = new com/truecaller/android/sdk/TrueResponse;
          localObject2 = new com/truecaller/android/sdk/TrueError;
          n = 4;
          ((TrueError)localObject2).<init>(n);
          paramPartnerInformation.<init>((TrueError)localObject2);
        }
        return w.b(paramPartnerInformation);
      }
      localObject2 = (ap)b;
      if (localObject2 == null)
      {
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("TrueSDK - Partner: ");
        paramPartnerInformation = packageName;
        ((StringBuilder)localObject2).append(paramPartnerInformation);
        ((StringBuilder)localObject2).append(" - User profile request returned empty body");
        paramPartnerInformation = ((StringBuilder)localObject2).toString();
        localObject2 = new String[0];
        AssertionUtil.reportWithSummary(paramPartnerInformation, (String[])localObject2);
        paramPartnerInformation = new com/truecaller/android/sdk/TrueResponse;
        localObject2 = new com/truecaller/android/sdk/TrueError;
        ((TrueError)localObject2).<init>(0);
        paramPartnerInformation.<init>((TrueError)localObject2);
        return w.b(paramPartnerInformation);
      }
      localObject1 = a.f;
      Object localObject3 = "Signature-Algorithm";
      localObject1 = ((t)localObject1).a((String)localObject3);
      if (localObject1 == null)
      {
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("TrueSDK - Partner: ");
        paramPartnerInformation = packageName;
        ((StringBuilder)localObject2).append(paramPartnerInformation);
        ((StringBuilder)localObject2).append(" - User profile request didn't return the signature algorithm");
        paramPartnerInformation = ((StringBuilder)localObject2).toString();
        localObject2 = new String[0];
        AssertionUtil.reportWithSummary(paramPartnerInformation, (String[])localObject2);
        paramPartnerInformation = new com/truecaller/android/sdk/TrueResponse;
        localObject2 = new com/truecaller/android/sdk/TrueError;
        ((TrueError)localObject2).<init>(0);
        paramPartnerInformation.<init>((TrueError)localObject2);
        return w.b(paramPartnerInformation);
      }
      localObject3 = a;
      Object localObject4 = b;
      String str = a;
      boolean bool2 = a.a((Context)localObject3, (String)localObject4, str, (String)localObject1);
      if (!bool2)
      {
        localObject3 = a(a, (ap)localObject2, paramPartnerInformation, (String)localObject1);
        if (localObject3 != null) {
          return w.b(localObject3);
        }
      }
      localObject3 = new java/lang/String;
      localObject4 = Base64.decode(b, 0);
      ((String)localObject3).<init>((byte[])localObject4);
      localObject4 = b;
      localObject2 = a;
      localObject2 = ConfirmProfileActivity.b((String)localObject3, (String)localObject4, (String)localObject2, (String)localObject1);
      localObject1 = reqNonce;
      localObject3 = requestNonce;
      boolean bool3 = ((String)localObject1).equals(localObject3);
      if (!bool3)
      {
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("TrueSDK - Partner: ");
        localObject3 = packageName;
        ((StringBuilder)localObject1).append((String)localObject3);
        ((StringBuilder)localObject1).append(" - Nonce missmatch - req: ");
        paramPartnerInformation = reqNonce;
        ((StringBuilder)localObject1).append(paramPartnerInformation);
        ((StringBuilder)localObject1).append(", resp: ");
        paramPartnerInformation = requestNonce;
        ((StringBuilder)localObject1).append(paramPartnerInformation);
        paramPartnerInformation = ((StringBuilder)localObject1).toString();
        localObject2 = new String[0];
        AssertionUtil.reportWithSummary(paramPartnerInformation, (String[])localObject2);
        paramPartnerInformation = new com/truecaller/android/sdk/TrueResponse;
        localObject2 = new com/truecaller/android/sdk/TrueError;
        ((TrueError)localObject2).<init>(9);
        paramPartnerInformation.<init>((TrueError)localObject2);
        return w.b(paramPartnerInformation);
      }
      paramPartnerInformation = new com/truecaller/android/sdk/TrueResponse;
      paramPartnerInformation.<init>((TrueProfile)localObject2);
      return w.b(paramPartnerInformation);
    }
    catch (IOException localIOException)
    {
      int j;
      d.a(localIOException);
      paramPartnerInformation = new com/truecaller/android/sdk/TrueResponse;
      Object localObject1 = new com/truecaller/android/sdk/TrueError;
      ((TrueError)localObject1).<init>(j);
      paramPartnerInformation.<init>((TrueError)localObject1);
    }
    return w.b(paramPartnerInformation);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.al
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */