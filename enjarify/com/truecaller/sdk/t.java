package com.truecaller.sdk;

import dagger.a.d;
import javax.inject.Provider;

public final class t
  implements d
{
  private final h a;
  private final Provider b;
  
  private t(h paramh, Provider paramProvider)
  {
    a = paramh;
    b = paramProvider;
  }
  
  public static t a(h paramh, Provider paramProvider)
  {
    t localt = new com/truecaller/sdk/t;
    localt.<init>(paramh, paramProvider);
    return localt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */