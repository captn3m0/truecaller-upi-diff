package com.truecaller.sdk;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.telephony.TelephonyManager;
import com.d.b.w;
import com.d.b.w.a;
import com.truecaller.analytics.d;
import com.truecaller.androidactors.aa;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import com.truecaller.androidactors.m;
import com.truecaller.androidactors.q;
import com.truecaller.common.h.g;
import com.truecaller.sdk.utils.b;
import com.truecaller.utils.n;

class h
{
  final Activity a;
  
  h(Activity paramActivity)
  {
    a = paramActivity;
  }
  
  static com.truecaller.androidactors.f a(aj paramaj, i parami)
  {
    return parami.a(aj.class, paramaj);
  }
  
  static i a(k paramk, Context paramContext)
  {
    return paramk.a(paramContext, ConfirmProfileService.class, 10);
  }
  
  static aj a(Context paramContext)
  {
    al localal = new com/truecaller/sdk/al;
    localal.<init>(paramContext);
    return localal;
  }
  
  static ao a()
  {
    ao localao = new com/truecaller/sdk/ao;
    localao.<init>();
    return localao;
  }
  
  static e a(Context paramContext, k paramk, com.truecaller.androidactors.f paramf, TelephonyManager paramTelephonyManager, PackageManager paramPackageManager, NotificationManager paramNotificationManager, ae paramae, ao paramao, ah paramah, com.truecaller.common.g.a parama, ag.a parama1, am paramam, n paramn, com.truecaller.analytics.a.f paramf1, com.truecaller.sdk.utils.a parama2, a parama3)
  {
    f localf = new com/truecaller/sdk/f;
    c.d.f localf1 = g.a(paramContext).r();
    i locali = paramk.a();
    localf.<init>(localf1, locali, paramf, paramTelephonyManager, paramPackageManager, paramNotificationManager, paramae, paramao, paramah, parama, parama1, paramam, paramn, paramf1, parama2, parama3);
    return localf;
  }
  
  static w b(Context paramContext)
  {
    w.a locala = new com/d/b/w$a;
    locala.<init>(paramContext);
    b = false;
    c = false;
    return locala.a();
  }
  
  static k b()
  {
    Object localObject1 = new com/truecaller/sdk/c;
    ((c)localObject1).<init>();
    Object localObject2 = a;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/androidactors/m;
      ((m)localObject2).<init>();
      a = ((q)localObject2);
    }
    localObject2 = b;
    if (localObject2 == null)
    {
      localObject2 = new com/truecaller/sdk/c$b;
      localaa = null;
      ((c.b)localObject2).<init>((byte)0);
      b = ((aa)localObject2);
    }
    localObject2 = new com/truecaller/sdk/c$a;
    aa localaa = b;
    localObject1 = a;
    ((c.a)localObject2).<init>(localaa, (q)localObject1);
    return (k)localObject2;
  }
  
  static e b(Context paramContext, k paramk, com.truecaller.androidactors.f paramf, TelephonyManager paramTelephonyManager, PackageManager paramPackageManager, NotificationManager paramNotificationManager, ae paramae, ao paramao, ah paramah, com.truecaller.common.g.a parama, ag.a parama1, am paramam, n paramn, com.truecaller.analytics.a.f paramf1, com.truecaller.sdk.utils.a parama2, a parama3)
  {
    af localaf = new com/truecaller/sdk/af;
    c.d.f localf = g.a(paramContext).r();
    i locali = paramk.a();
    localaf.<init>(localf, locali, paramf, paramTelephonyManager, paramPackageManager, paramNotificationManager, paramae, paramao, paramah, parama, parama1, paramam, paramn, paramf1, parama2, parama3);
    return localaf;
  }
  
  static NotificationManager c(Context paramContext)
  {
    return (NotificationManager)paramContext.getSystemService("notification");
  }
  
  static ah c()
  {
    ah localah = new com/truecaller/sdk/ah;
    localah.<init>();
    return localah;
  }
  
  static PackageManager d(Context paramContext)
  {
    return paramContext.getPackageManager();
  }
  
  static TelephonyManager e(Context paramContext)
  {
    return (TelephonyManager)paramContext.getSystemService("phone");
  }
  
  static ae f(Context paramContext)
  {
    return (ae)((Activity)paramContext).getApplication();
  }
  
  static com.truecaller.common.g.a g(Context paramContext)
  {
    return ((com.truecaller.common.b.a)((Activity)paramContext).getApplication()).u().c();
  }
  
  static com.truecaller.analytics.a.f h(Context paramContext)
  {
    return ((com.truecaller.common.b.a)((Activity)paramContext).getApplication()).v().b();
  }
  
  static am i(Context paramContext)
  {
    an localan = new com/truecaller/sdk/an;
    localan.<init>(paramContext);
    return localan;
  }
  
  static com.truecaller.sdk.utils.a j(Context paramContext)
  {
    b localb = new com/truecaller/sdk/utils/b;
    localb.<init>(paramContext);
    return localb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */