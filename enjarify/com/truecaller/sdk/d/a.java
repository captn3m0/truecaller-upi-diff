package com.truecaller.sdk.d;

import android.content.Intent;
import android.text.SpannableStringBuilder;
import com.truecaller.android.sdk.TrueProfile;

public abstract interface a
{
  public abstract String a(int paramInt);
  
  public abstract void a();
  
  public abstract void a(int paramInt, Intent paramIntent);
  
  public abstract void a(SpannableStringBuilder paramSpannableStringBuilder);
  
  public abstract void a(TrueProfile paramTrueProfile);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString1, String paramString2, String paramString3, String paramString4);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b();
  
  public abstract void b(String paramString);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract boolean c();
  
  public abstract void e();
  
  public abstract void f();
  
  public abstract void g();
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */