package com.truecaller.sdk;

import dagger.a.d;
import javax.inject.Provider;

public final class n
  implements d
{
  private final h a;
  private final Provider b;
  
  private n(h paramh, Provider paramProvider)
  {
    a = paramh;
    b = paramProvider;
  }
  
  public static n a(h paramh, Provider paramProvider)
  {
    n localn = new com/truecaller/sdk/n;
    localn.<init>(paramh, paramProvider);
    return localn;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */