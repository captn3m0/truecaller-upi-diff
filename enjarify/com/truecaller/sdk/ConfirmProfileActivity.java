package com.truecaller.sdk;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.transition.n;
import android.support.transition.p;
import android.support.transition.s;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import c.a.m;
import c.g.b.k;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.google.gson.f;
import com.truecaller.android.sdk.TrueProfile;
import com.truecaller.common.h.aq.d;
import com.truecaller.utils.t;
import com.truecaller.utils.t.a;
import java.util.List;

public class ConfirmProfileActivity
  extends AppCompatActivity
  implements View.OnClickListener, com.truecaller.sdk.d.c
{
  e a;
  w b;
  private ProgressBar c;
  private RecyclerView d;
  private TextView e;
  private TextView f;
  private TextView g;
  
  static TrueProfile b(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    com.google.gson.g localg = new com/google/gson/g;
    localg.<init>();
    paramString1 = (TrueProfile)localg.a().a(paramString1, TrueProfile.class);
    payload = paramString2;
    signature = paramString3;
    signatureAlgorithm = paramString4;
    return paramString1;
  }
  
  public static Intent getLaunchIntent(Context paramContext, Bundle paramBundle)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, ConfirmProfileActivity.class);
    localIntent.addFlags(268468224);
    localIntent.putExtras(paramBundle);
    return localIntent;
  }
  
  public final String a(int paramInt)
  {
    return getString(paramInt);
  }
  
  public final void a()
  {
    a.g();
  }
  
  public final void a(int paramInt, Intent paramIntent)
  {
    setResult(paramInt, paramIntent);
  }
  
  public final void a(SpannableStringBuilder paramSpannableStringBuilder)
  {
    int i = R.id.tcBrandingText;
    ((TextView)findViewById(i)).setText(paramSpannableStringBuilder);
  }
  
  public final void a(TrueProfile paramTrueProfile)
  {
    a.a(paramTrueProfile);
  }
  
  public final void a(String paramString)
  {
    int i = R.id.legalTextDivider;
    findViewById(i).setVisibility(0);
    f.setText(paramString);
    f.setVisibility(0);
    f.setOnClickListener(this);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    paramString1 = e;
    int i = R.string.SdkProfileShareTerms;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramString2;
    paramString2 = Html.fromHtml(getString(i, arrayOfObject));
    paramString1.setText(paramString2);
    int j = R.id.partnerLoginIntentText;
    ((TextView)findViewById(j)).setText(paramString4);
    paramString1 = g;
    int k = R.string.SdkProfileContinue;
    paramString2 = getString(k);
    paramString1.setText(paramString2);
    paramString1 = f;
    k = R.string.SdkContinueWithDifferentNumber;
    paramString2 = getString(k);
    paramString1.setText(paramString2);
  }
  
  public final void a(List paramList)
  {
    com.truecaller.sdk.a.d locald = new com/truecaller/sdk/a/d;
    locald.<init>(this, paramList, '\000');
    d.setItemAnimator(null);
    d.setAdapter(locald);
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject = c;
    int i = 0;
    int j;
    if (paramBoolean) {
      j = 0;
    } else {
      j = 8;
    }
    ((ProgressBar)localObject).setVisibility(j);
    int k = R.id.ctaContainer;
    localObject = findViewById(k);
    if (paramBoolean) {
      i = 8;
    }
    ((View)localObject).setVisibility(i);
  }
  
  public final void b()
  {
    int i = R.id.confirmProgressBar;
    Object localObject = (ProgressBar)findViewById(i);
    c = ((ProgressBar)localObject);
    i = R.id.profileInfo;
    localObject = (RecyclerView)findViewById(i);
    d = ((RecyclerView)localObject);
    i = R.id.legalText;
    localObject = (TextView)findViewById(i);
    e = ((TextView)localObject);
    i = R.id.continueWithDifferentNumber;
    localObject = (TextView)findViewById(i);
    f = ((TextView)localObject);
    i = R.id.confirm;
    localObject = (TextView)findViewById(i);
    g = ((TextView)localObject);
    g.setOnClickListener(this);
  }
  
  public final void b(String paramString)
  {
    int i = R.id.profileImage;
    ImageView localImageView = (ImageView)findViewById(i);
    Object localObject = b;
    paramString = Uri.parse(paramString);
    paramString = ((w)localObject).a(paramString);
    localObject = aq.d.b();
    paramString = paramString.a((ai)localObject);
    int j = R.drawable.ic_sdk_empty_avatar;
    paramString = paramString.a(j);
    j = R.drawable.ic_sdk_empty_avatar;
    paramString.b(j).a(localImageView, null);
  }
  
  public final void b(boolean paramBoolean)
  {
    Object localObject1 = (com.truecaller.sdk.a.d)d.getAdapter();
    int i = 2;
    int j;
    if (paramBoolean)
    {
      localObject2 = a;
      j = ((List)localObject2).size() - i;
      ((com.truecaller.sdk.a.d)localObject1).notifyItemRangeInserted(i, j);
      localObject3 = a;
      i = ((List)localObject3).size();
    }
    else
    {
      localObject2 = a;
      j = ((List)localObject2).size() - i;
      ((com.truecaller.sdk.a.d)localObject1).notifyItemRangeRemoved(i, j);
    }
    b = i;
    int k = R.id.rootView;
    localObject1 = (ViewGroup)findViewById(k);
    Object localObject3 = new android/support/transition/s;
    ((s)localObject3).<init>();
    Object localObject2 = new android/support/transition/d;
    ((android.support.transition.d)localObject2).<init>();
    int m = R.id.ctaContainer;
    localObject2 = ((android.support.transition.d)localObject2).a(m);
    m = R.id.containerView;
    localObject2 = ((n)localObject2).a(m);
    ConfirmProfileActivity.1 local1 = new com/truecaller/sdk/ConfirmProfileActivity$1;
    local1.<init>(this, paramBoolean);
    Object localObject4 = ((n)localObject2).a(local1);
    localObject4 = ((s)localObject3).a((n)localObject4).c(300L);
    p.a((ViewGroup)localObject1, (n)localObject4);
  }
  
  public final boolean c()
  {
    String str = "android.permission.READ_PHONE_STATE";
    int i = android.support.v4.app.a.a(this, str);
    return i == 0;
  }
  
  public final void d()
  {
    TextView localTextView = e;
    int i = R.drawable.ic_sdk_arrow_down;
    localTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, i, 0);
    e.setOnClickListener(this);
  }
  
  public final void e()
  {
    finish();
    overridePendingTransition(0, 0);
  }
  
  public final void f()
  {
    a.c();
  }
  
  public final void g()
  {
    int i = R.id.rootView;
    Object localObject1 = (ViewGroup)findViewById(i);
    Object localObject2 = new android/support/transition/c;
    ((android.support.transition.c)localObject2).<init>();
    ConfirmProfileActivity.2 local2 = new com/truecaller/sdk/ConfirmProfileActivity$2;
    local2.<init>(this);
    localObject2 = ((android.support.transition.c)localObject2).c(local2);
    p.a((ViewGroup)localObject1, (n)localObject2);
    i = R.id.inProgressIndicator;
    findViewById(i).setVisibility(0);
    i = R.id.ctaContainer;
    localObject1 = findViewById(i);
    int j = 8;
    ((View)localObject1).setVisibility(j);
    localObject1 = (com.truecaller.sdk.a.d)d.getAdapter();
    int k = R.string.sdkLoggingYouIn;
    String str = getString(k);
    k.b(str, "inProgressText");
    com.truecaller.sdk.a.c[] arrayOfc = new com.truecaller.sdk.a.c[2];
    com.truecaller.sdk.a.c localc = (com.truecaller.sdk.a.c)a.get(0);
    arrayOfc[0] = localc;
    localObject2 = new com/truecaller/sdk/a/a;
    ((com.truecaller.sdk.a.a)localObject2).<init>(str);
    localObject2 = (com.truecaller.sdk.a.c)localObject2;
    arrayOfc[1] = localObject2;
    localObject2 = m.b(arrayOfc);
    a = ((List)localObject2);
    int m = a.size();
    b = m;
    ((com.truecaller.sdk.a.d)localObject1).notifyDataSetChanged();
    i = R.id.topContainer;
    findViewById(i).setVisibility(j);
  }
  
  public void onBackPressed()
  {
    a.b();
  }
  
  public void onClick(View paramView)
  {
    int i = paramView.getId();
    int j = R.id.confirm;
    if (i == j)
    {
      a.e();
      return;
    }
    j = R.id.continueWithDifferentNumber;
    if (i == j)
    {
      a.h();
      return;
    }
    j = R.id.legalText;
    if (i == j)
    {
      paramView = a;
      paramView.d();
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Object localObject1 = ad.a();
    Object localObject2 = com.truecaller.utils.c.a().a(this).a();
    localObject1 = ((ad.a)localObject1).a((t)localObject2);
    localObject2 = new com/truecaller/sdk/h;
    ((h)localObject2).<init>(this);
    ((ad.a)localObject1).a((h)localObject2).a().a(this);
    int i = R.layout.activity_confirm_profile;
    setContentView(i);
    localObject1 = a;
    boolean bool = ((e)localObject1).a(paramBundle);
    if (bool)
    {
      a.a(this);
      return;
    }
    finish();
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    a.a();
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    a.b(paramBundle);
  }
  
  public void onStop()
  {
    super.onStop();
    a.f();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.ConfirmProfileActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */