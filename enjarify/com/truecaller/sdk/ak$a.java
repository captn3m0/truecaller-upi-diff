package com.truecaller.sdk;

import com.truecaller.android.sdk.PartnerInformation;
import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class ak$a
  extends u
{
  private final PartnerInformation b;
  
  private ak$a(e parame, PartnerInformation paramPartnerInformation)
  {
    super(parame);
    b = paramPartnerInformation;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".getTrueProfile(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.ak.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */