package com.truecaller.sdk;

import android.content.Context;
import c.g.b.k;
import com.truecaller.common.e.f;
import java.util.Locale;

public final class an
  implements am
{
  private final Context a;
  
  public an(Context paramContext)
  {
    a = paramContext;
  }
  
  public final Locale a()
  {
    Locale localLocale = f.a();
    k.a(localLocale, "LocaleManager.getAppLocale()");
    return localLocale;
  }
  
  public final void a(Locale paramLocale)
  {
    k.b(paramLocale, "locale");
    f.a(a, paramLocale);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.an
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */