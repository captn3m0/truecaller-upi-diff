package com.truecaller.sdk;

import dagger.a.d;
import javax.inject.Provider;

public final class v
  implements d
{
  private final h a;
  private final Provider b;
  
  private v(h paramh, Provider paramProvider)
  {
    a = paramh;
    b = paramProvider;
  }
  
  public static v a(h paramh, Provider paramProvider)
  {
    v localv = new com/truecaller/sdk/v;
    localv.<init>(paramh, paramProvider);
    return localv;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */