package com.truecaller.sdk;

import com.airbnb.deeplinkdispatch.DeepLinkEntry;
import com.airbnb.deeplinkdispatch.DeepLinkEntry.Type;
import com.airbnb.deeplinkdispatch.Parser;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class ai
  implements Parser
{
  public static final List a;
  
  static
  {
    DeepLinkEntry[] arrayOfDeepLinkEntry = new DeepLinkEntry[1];
    DeepLinkEntry localDeepLinkEntry = new com/airbnb/deeplinkdispatch/DeepLinkEntry;
    DeepLinkEntry.Type localType = DeepLinkEntry.Type.METHOD;
    localDeepLinkEntry.<init>("truecallersdk://truesdk/web_verify", localType, ConfirmProfileActivity.class, "getLaunchIntent");
    arrayOfDeepLinkEntry[0] = localDeepLinkEntry;
    a = Collections.unmodifiableList(Arrays.asList(arrayOfDeepLinkEntry));
  }
  
  public final DeepLinkEntry parseUri(String paramString)
  {
    Iterator localIterator = a.iterator();
    DeepLinkEntry localDeepLinkEntry;
    boolean bool2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localDeepLinkEntry = (DeepLinkEntry)localIterator.next();
      bool2 = localDeepLinkEntry.matches(paramString);
    } while (!bool2);
    return localDeepLinkEntry;
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.ai
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */