package com.truecaller.sdk;

import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d
{
  private final h a;
  private final Provider b;
  private final Provider c;
  
  private p(h paramh, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramh;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static p a(h paramh, Provider paramProvider1, Provider paramProvider2)
  {
    p localp = new com/truecaller/sdk/p;
    localp.<init>(paramh, paramProvider1, paramProvider2);
    return localp;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */