package com.truecaller.sdk;

import com.truecaller.utils.t;

public final class ad$a
{
  private h a;
  private t b;
  
  public final a a(h paramh)
  {
    paramh = (h)dagger.a.g.a(paramh);
    a = paramh;
    return this;
  }
  
  public final a a(t paramt)
  {
    paramt = (t)dagger.a.g.a(paramt);
    b = paramt;
    return this;
  }
  
  public final g a()
  {
    dagger.a.g.a(a, h.class);
    dagger.a.g.a(b, t.class);
    ad localad = new com/truecaller/sdk/ad;
    h localh = a;
    t localt = b;
    localad.<init>(localh, localt, (byte)0);
    return localad;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.ad.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */