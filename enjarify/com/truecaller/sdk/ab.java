package com.truecaller.sdk;

import dagger.a.d;
import javax.inject.Provider;

public final class ab
  implements d
{
  private final h a;
  private final Provider b;
  
  private ab(h paramh, Provider paramProvider)
  {
    a = paramh;
    b = paramProvider;
  }
  
  public static ab a(h paramh, Provider paramProvider)
  {
    ab localab = new com/truecaller/sdk/ab;
    localab.<init>(paramh, paramProvider);
    return localab;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */