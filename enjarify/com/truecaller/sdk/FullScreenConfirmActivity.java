package com.truecaller.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Group;
import android.support.transition.d;
import android.support.transition.n;
import android.support.transition.p;
import android.support.transition.s;
import android.support.v4.app.a;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import c.g.b.k;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.android.sdk.TrueProfile;
import com.truecaller.common.h.aq.d;
import com.truecaller.utils.c;
import com.truecaller.utils.t;
import com.truecaller.utils.t.a;
import java.util.HashMap;
import java.util.List;

public final class FullScreenConfirmActivity
  extends AppCompatActivity
  implements View.OnClickListener, com.truecaller.sdk.d.b
{
  public e a;
  public w b;
  private ConstraintLayout c;
  private View d;
  private HashMap e;
  
  private View b(int paramInt)
  {
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      e = ((HashMap)localObject1);
    }
    localObject1 = e;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = e;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  private static Spanned c(String paramString)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 24;
    if (i >= j)
    {
      paramString = Html.fromHtml(paramString, 0);
      k.a(paramString, "Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY)");
      return paramString;
    }
    paramString = Html.fromHtml(paramString);
    k.a(paramString, "Html.fromHtml(text)");
    return paramString;
  }
  
  public final String a(int paramInt)
  {
    String str = getString(paramInt);
    k.a(str, "getString(resId)");
    return str;
  }
  
  public final void a()
  {
    e locale = a;
    if (locale == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locale.g();
  }
  
  public final void a(int paramInt, Intent paramIntent)
  {
    k.b(paramIntent, "result");
    setResult(paramInt, paramIntent);
  }
  
  public final void a(Drawable paramDrawable)
  {
    k.b(paramDrawable, "partnerAppIcon");
    int i = R.id.partnerAppImage;
    ((ImageView)b(i)).setImageDrawable(paramDrawable);
  }
  
  public final void a(SpannableStringBuilder paramSpannableStringBuilder)
  {
    int i = R.id.tcBrandingText;
    TextView localTextView = (TextView)b(i);
    k.a(localTextView, "tcBrandingText");
    paramSpannableStringBuilder = (CharSequence)paramSpannableStringBuilder;
    localTextView.setText(paramSpannableStringBuilder);
  }
  
  public final void a(TrueProfile paramTrueProfile)
  {
    k.b(paramTrueProfile, "trueProfile");
    e locale = a;
    if (locale == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locale.a(paramTrueProfile);
  }
  
  public final void a(String paramString)
  {
    int i = R.id.continueWithDifferentNumber;
    Object localObject = (TextView)b(i);
    k.a(localObject, "continueWithDifferentNumber");
    paramString = (CharSequence)paramString;
    ((TextView)localObject).setText(paramString);
    int j = R.id.continueWithDifferentNumber;
    paramString = (TextView)b(j);
    k.a(paramString, "continueWithDifferentNumber");
    paramString.setVisibility(0);
    j = R.id.continueWithDifferentNumber;
    paramString = (TextView)b(j);
    localObject = this;
    localObject = (View.OnClickListener)this;
    paramString.setOnClickListener((View.OnClickListener)localObject);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    k.b(paramString1, "phoneNumber");
    k.b(paramString2, "partnerAppName");
    k.b(paramString3, "fullName");
    k.b(paramString4, "partnerIntentText");
    int i = R.id.partnerLoginIntentText;
    Object localObject1 = (TextView)b(i);
    String str = "partnerLoginIntentText";
    k.a(localObject1, str);
    paramString4 = (CharSequence)paramString4;
    ((TextView)localObject1).setText(paramString4);
    int j = R.id.partnerSecondaryText;
    paramString4 = (TextView)b(j);
    k.a(paramString4, "partnerSecondaryText");
    i = R.string.SdkSecondaryTitleText;
    int k = 1;
    Object localObject2 = new Object[k];
    localObject2[0] = paramString2;
    localObject1 = getString(i, (Object[])localObject2);
    localObject2 = "getString(R.string.SdkSe…itleText, partnerAppName)";
    k.a(localObject1, (String)localObject2);
    localObject1 = (CharSequence)c((String)localObject1);
    paramString4.setText((CharSequence)localObject1);
    j = R.id.userName;
    paramString4 = (TextView)b(j);
    localObject1 = "userName";
    k.a(paramString4, (String)localObject1);
    paramString3 = (CharSequence)paramString3;
    paramString4.setText(paramString3);
    int m = R.id.userPhone;
    paramString3 = (TextView)b(m);
    k.a(paramString3, "userPhone");
    paramString1 = (CharSequence)paramString1;
    paramString3.setText(paramString1);
    int n = R.id.legalText;
    paramString1 = (TextView)b(n);
    k.a(paramString1, "legalText");
    m = R.string.SdkProfileShareTerms;
    paramString4 = new Object[k];
    paramString4[0] = paramString2;
    paramString2 = getString(m, paramString4);
    paramString3 = "getString(R.string.SdkPr…areTerms, partnerAppName)";
    k.a(paramString2, paramString3);
    paramString2 = (CharSequence)c(paramString2);
    paramString1.setText(paramString2);
    paramString1 = this;
    paramString1 = (Context)this;
    int i1 = R.drawable.ic_sdk_terms;
    paramString2 = android.support.v4.content.b.a(paramString1, i1);
    if (paramString2 != null)
    {
      paramString2 = paramString2.mutate();
      if (paramString2 != null)
      {
        m = R.color.full_screen_secondary_text;
        n = android.support.v4.content.b.c(paramString1, m);
        paramString3 = PorterDuff.Mode.SRC_IN;
        paramString2.setColorFilter(n, paramString3);
        n = R.id.legalText;
        ((TextView)b(n)).setCompoundDrawablesWithIntrinsicBounds(paramString2, null, null, null);
        return;
      }
    }
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "profileInfoList");
    Object localObject1 = new com/truecaller/sdk/a/f;
    Object localObject2 = this;
    localObject2 = (Context)this;
    ((com.truecaller.sdk.a.f)localObject1).<init>((Context)localObject2, paramList);
    int i = R.id.profileInfoListView;
    paramList = (RecyclerView)b(i);
    k.a(paramList, "profileInfoListView");
    localObject1 = (RecyclerView.Adapter)localObject1;
    paramList.setAdapter((RecyclerView.Adapter)localObject1);
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = 8;
    int j = 0;
    String str1 = null;
    if (!paramBoolean)
    {
      paramBoolean = R.id.progressBar;
      localObject = (ProgressBar)b(paramBoolean);
      str2 = "progressBar";
      k.a(localObject, str2);
      ((ProgressBar)localObject).setVisibility(i);
      paramBoolean = R.id.footerContainer;
      localObject = b(paramBoolean);
      String str3 = "footerContainer";
      k.a(localObject, str3);
      ((View)localObject).setVisibility(0);
      localObject = d;
      if (localObject != null) {
        ((View)localObject).setVisibility(0);
      }
      return;
    }
    paramBoolean = R.id.progressBar;
    Object localObject = (ProgressBar)b(paramBoolean);
    String str2 = "progressBar";
    k.a(localObject, str2);
    ((ProgressBar)localObject).setVisibility(0);
    paramBoolean = R.id.footerContainer;
    localObject = b(paramBoolean);
    str1 = "footerContainer";
    k.a(localObject, str1);
    j = 4;
    ((View)localObject).setVisibility(j);
    localObject = d;
    if (localObject != null)
    {
      ((View)localObject).setVisibility(i);
      return;
    }
  }
  
  public final void b()
  {
    int i = R.id.rootContainer;
    Object localObject1 = findViewById(i);
    k.a(localObject1, "findViewById(R.id.rootContainer)");
    localObject1 = (ConstraintLayout)localObject1;
    c = ((ConstraintLayout)localObject1);
    i = R.id.expander;
    localObject1 = (ImageView)b(i);
    Object localObject2 = this;
    localObject2 = (View.OnClickListener)this;
    ((ImageView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    i = R.id.confirmText;
    ((TextView)b(i)).setOnClickListener((View.OnClickListener)localObject2);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "avatarUrl");
    Object localObject = b;
    if (localObject == null)
    {
      String str = "picasso";
      k.a(str);
    }
    paramString = Uri.parse(paramString);
    paramString = ((w)localObject).a(paramString);
    localObject = (ai)aq.d.b();
    paramString = paramString.a((ai)localObject);
    int i = R.drawable.ic_sdk_empty_avatar;
    paramString = paramString.a(i);
    i = R.drawable.ic_sdk_empty_avatar;
    paramString = paramString.b(i);
    i = R.id.profileImage;
    localObject = (ImageView)b(i);
    paramString.a((ImageView)localObject, null);
  }
  
  public final void b(boolean paramBoolean)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "rootContainer";
      k.a((String)localObject2);
    }
    localObject1 = (ViewGroup)localObject1;
    Object localObject2 = new android/support/transition/s;
    ((s)localObject2).<init>();
    Object localObject3 = new android/support/transition/g;
    ((android.support.transition.g)localObject3).<init>(1);
    localObject3 = (n)localObject3;
    localObject2 = ((s)localObject2).a((n)localObject3);
    localObject3 = new android/support/transition/g;
    int i = 2;
    ((android.support.transition.g)localObject3).<init>(i);
    long l = 0L;
    localObject3 = ((android.support.transition.g)localObject3).a(l);
    localObject2 = ((s)localObject2).a((n)localObject3);
    localObject3 = new android/support/transition/d;
    ((d)localObject3).<init>();
    localObject3 = (n)localObject3;
    localObject2 = (n)((s)localObject2).a((n)localObject3);
    p.a((ViewGroup)localObject1, (n)localObject2);
    int j = R.id.profileInfoListView;
    localObject1 = (RecyclerView)b(j);
    k.a(localObject1, "profileInfoListView");
    int k = 0;
    localObject2 = null;
    if (paramBoolean)
    {
      i = 0;
      str = null;
    }
    else
    {
      i = 8;
    }
    ((RecyclerView)localObject1).setVisibility(i);
    j = R.id.collapsableContentDivider;
    localObject1 = b(j);
    String str = "collapsableContentDivider";
    k.a(localObject1, str);
    if (!paramBoolean) {
      k = 8;
    }
    ((View)localObject1).setVisibility(k);
    j = R.id.expander;
    localObject1 = (ImageView)b(j);
    localObject2 = "expander";
    k.a(localObject1, (String)localObject2);
    float f;
    if (paramBoolean)
    {
      paramBoolean = 1127481344;
      f = 180.0F;
    }
    else
    {
      paramBoolean = false;
      f = 0.0F;
    }
    ((ImageView)localObject1).setRotation(f);
  }
  
  public final void c(boolean paramBoolean)
  {
    int i = R.id.continueWithDifferentNumber;
    TextView localTextView = (TextView)b(i);
    String str = "continueWithDifferentNumber";
    k.a(localTextView, str);
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localTextView.setVisibility(paramBoolean);
  }
  
  public final boolean c()
  {
    Object localObject = this;
    localObject = (Context)this;
    String str = "android.permission.READ_PHONE_STATE";
    int i = a.a((Context)localObject, str);
    return i == 0;
  }
  
  public final void e()
  {
    finish();
  }
  
  public final void f()
  {
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((e)localObject).c();
    int i = R.id.containerLayoutGroup;
    localObject = (Group)b(i);
    k.a(localObject, "containerLayoutGroup");
    ((Group)localObject).setVisibility(0);
  }
  
  public final void g() {}
  
  public final void onBackPressed()
  {
    e locale = a;
    if (locale == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locale.b();
  }
  
  public final void onClick(View paramView)
  {
    String str = "v";
    k.b(paramView, str);
    int i = paramView.getId();
    int j = R.id.confirmText;
    if (i == j)
    {
      paramView = a;
      if (paramView == null)
      {
        str = "presenter";
        k.a(str);
      }
      paramView.e();
      return;
    }
    j = R.id.expander;
    if (i == j)
    {
      paramView = a;
      if (paramView == null)
      {
        str = "presenter";
        k.a(str);
      }
      paramView.d();
      return;
    }
    j = R.id.continueWithDifferentNumber;
    if (i == j)
    {
      paramView = a;
      if (paramView == null)
      {
        str = "presenter";
        k.a(str);
      }
      paramView.b();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Object localObject1 = ad.a();
    Object localObject2 = c.a();
    Object localObject3 = this;
    localObject3 = (Context)this;
    localObject2 = ((t.a)localObject2).a((Context)localObject3).a();
    localObject1 = ((ad.a)localObject1).a((t)localObject2);
    localObject2 = new com/truecaller/sdk/h;
    localObject3 = this;
    localObject3 = (Activity)this;
    ((h)localObject2).<init>((Activity)localObject3);
    ((ad.a)localObject1).a((h)localObject2).a().a(this);
    int i = R.layout.activity_confirm_profile_full;
    setContentView(i);
    localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    boolean bool = ((e)localObject1).a(paramBundle);
    if (bool)
    {
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject1 = "presenter";
        k.a((String)localObject1);
      }
      paramBundle.a(this);
      return;
    }
    finish();
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    e locale = a;
    if (locale == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locale.a();
  }
  
  public final void onSaveInstanceState(Bundle paramBundle)
  {
    k.b(paramBundle, "outState");
    super.onSaveInstanceState(paramBundle);
    e locale = a;
    if (locale == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locale.b(paramBundle);
  }
  
  public final void onStop()
  {
    super.onStop();
    e locale = a;
    if (locale == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locale.f();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.FullScreenConfirmActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */