package com.truecaller.sdk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import c.g.b.k;

public final class b
  implements a
{
  private final Activity a;
  
  public b(Activity paramActivity)
  {
    a = paramActivity;
  }
  
  public final boolean a()
  {
    String str1 = a.getPackageName();
    String str2 = a.getCallingPackage();
    return k.a(str1, str2);
  }
  
  public final Bundle b()
  {
    Intent localIntent = a.getIntent();
    if (localIntent != null) {
      return localIntent.getExtras();
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */