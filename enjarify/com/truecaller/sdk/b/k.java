package com.truecaller.sdk.b;

import android.app.NotificationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import com.truecaller.sdk.ae;
import com.truecaller.sdk.ah;
import com.truecaller.sdk.ao;
import com.truecaller.sdk.push.PushAppData;
import e.d;

public final class k
  extends b
{
  private final PushAppData e;
  private final NotificationManager f;
  private final ao g;
  private final Handler h;
  
  private k(Bundle paramBundle, NotificationManager paramNotificationManager, ao paramao, com.truecaller.common.g.a parama, boolean paramBoolean, ae paramae, ah paramah, Handler paramHandler)
  {
    super(paramBundle, parama, paramBoolean, paramae, paramah);
    f = paramNotificationManager;
    g = paramao;
    h = paramHandler;
    paramBundle = (PushAppData)paramBundle.getParcelable("a");
    e = paramBundle;
  }
  
  private final void r()
  {
    Object localObject = e;
    if (localObject != null)
    {
      ao localao = g;
      localao.a((PushAppData)localObject);
    }
    localObject = a;
    if (localObject != null)
    {
      ((com.truecaller.sdk.d.a)localObject).e();
      return;
    }
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    PushAppData localPushAppData = e;
    if (localPushAppData != null)
    {
      int i = -1;
      if (paramInt1 == i)
      {
        Object localObject = this;
        localObject = (d)this;
        ao.a(localPushAppData, (d)localObject);
        return;
      }
      c.a(paramInt2);
      g.a(localPushAppData);
      return;
    }
  }
  
  public final boolean b()
  {
    PushAppData localPushAppData = e;
    return localPushAppData != null;
  }
  
  public final String c()
  {
    Object localObject = e;
    if (localObject != null)
    {
      localObject = b;
      if (localObject != null) {}
    }
    else
    {
      localObject = "";
    }
    return (String)localObject;
  }
  
  public final String d()
  {
    return c();
  }
  
  public final String e()
  {
    return "web_api";
  }
  
  public final void f()
  {
    super.f();
    Object localObject1 = a;
    if (localObject1 == null) {
      return;
    }
    ((com.truecaller.sdk.d.a)localObject1).f();
    localObject1 = f;
    int i = 16;
    ((NotificationManager)localObject1).cancel(i);
    localObject1 = j();
    String str = "timout_left";
    long l1 = SystemClock.elapsedRealtime();
    long l2 = ((Bundle)localObject1).getLong(str, l1);
    l1 = SystemClock.elapsedRealtime() - l2;
    localObject1 = e;
    long l3 = 0L;
    if (localObject1 != null)
    {
      int j = c;
      l2 = j;
      long l4 = 1000L;
      l2 = l2 * l4 - l1;
    }
    else
    {
      l2 = l3;
    }
    boolean bool = l2 < l3;
    if (bool)
    {
      h.removeCallbacksAndMessages(null);
      Handler localHandler = h;
      Object localObject2 = new com/truecaller/sdk/b/k$a;
      Object localObject3 = this;
      localObject3 = (k)this;
      ((k.a)localObject2).<init>((k)localObject3);
      localObject2 = (c.g.a.a)localObject2;
      localObject3 = new com/truecaller/sdk/b/l;
      ((l)localObject3).<init>((c.g.a.a)localObject2);
      localObject3 = (Runnable)localObject3;
      localHandler.postDelayed((Runnable)localObject3, l2);
      return;
    }
    r();
  }
  
  public final void h()
  {
    super.h();
    boolean bool = d;
    if (bool)
    {
      int i = -1;
      a(i, i);
      localObject1 = a;
      if (localObject1 != null) {
        ((com.truecaller.sdk.d.a)localObject1).e();
      }
      return;
    }
    Object localObject1 = e;
    if (localObject1 != null)
    {
      Object localObject2 = this;
      localObject2 = (d)this;
      ao.a((PushAppData)localObject1, (d)localObject2);
      localObject1 = a;
      if (localObject1 != null)
      {
        ((com.truecaller.sdk.d.a)localObject1).g();
        return;
      }
      return;
    }
  }
  
  public final com.truecaller.android.sdk.clients.a i()
  {
    com.truecaller.android.sdk.clients.a locala = new com/truecaller/android/sdk/clients/a;
    locala.<init>(0, 0);
    return locala;
  }
  
  public final void m()
  {
    super.m();
    h.removeCallbacksAndMessages(null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.b.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */