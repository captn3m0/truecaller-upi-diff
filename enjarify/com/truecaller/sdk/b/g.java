package com.truecaller.sdk.b;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.android.sdk.PartnerInformation;
import com.truecaller.android.sdk.TrueError;
import com.truecaller.android.sdk.TrueResponse;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.sdk.ae;
import com.truecaller.sdk.ah;
import com.truecaller.sdk.aj;
import com.truecaller.sdk.d.b;
import java.util.Locale;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class g
  extends j
  implements ac, f
{
  private final PartnerInformation e;
  private final com.truecaller.android.sdk.clients.a f;
  private com.truecaller.androidactors.a g;
  private TrueResponse h;
  private boolean i;
  private final c.d.f j;
  private final com.truecaller.androidactors.f k;
  private final i l;
  private final PackageManager m;
  
  public g(c.d.f paramf, Bundle paramBundle, com.truecaller.common.g.a parama, com.truecaller.androidactors.f paramf1, i parami, PackageManager paramPackageManager, boolean paramBoolean, ae paramae, ah paramah)
  {
    super(paramBundle, parama, paramBoolean, paramae, paramah);
    j = paramf;
    k = paramf1;
    l = parami;
    m = paramPackageManager;
    paramf = new com/truecaller/android/sdk/PartnerInformation;
    paramf.<init>(paramBundle);
    e = paramf;
    paramf = new com/truecaller/android/sdk/clients/a;
    int n = paramBundle.getInt("truesdk flags", 0);
    int i1 = paramBundle.getInt("truesdk_consent_title", 0);
    paramf.<init>(n, i1);
    f = paramf;
  }
  
  private final void a(ac paramac)
  {
    Object localObject1 = g;
    if (localObject1 == null)
    {
      localObject1 = (aj)k.a();
      Object localObject2 = e;
      localObject1 = ((aj)localObject1).a((PartnerInformation)localObject2);
      localObject2 = l;
      paramac = ((w)localObject1).a((i)localObject2, paramac);
      g = paramac;
    }
  }
  
  private final void r()
  {
    boolean bool1 = b;
    if (bool1)
    {
      Object localObject = h;
      if (localObject != null)
      {
        boolean bool2 = i;
        if (!bool2)
        {
          if (localObject != null)
          {
            localObject = trueProfile;
          }
          else
          {
            bool1 = false;
            localObject = null;
          }
          bool2 = false;
          if (localObject != null)
          {
            bool1 = true;
          }
          else
          {
            bool1 = false;
            localObject = null;
          }
          int i1;
          if (bool1) {
            i1 = -1;
          } else {
            i1 = 0;
          }
          int n;
          if (bool1)
          {
            n = -1;
          }
          else
          {
            localObject = h;
            if (localObject != null)
            {
              localObject = trueError;
              if (localObject != null) {
                n = ((TrueError)localObject).getErrorType();
              }
            }
          }
          a(i1, n);
          localObject = a;
          if (localObject != null)
          {
            ((com.truecaller.sdk.d.a)localObject).e();
            return;
          }
        }
      }
    }
  }
  
  public final void a()
  {
    boolean bool = i;
    if (!bool)
    {
      bool = b;
      TrueError localTrueError;
      int i1;
      if (bool)
      {
        localObject = h;
        if (localObject == null)
        {
          localObject = new com/truecaller/android/sdk/TrueResponse;
          localTrueError = new com/truecaller/android/sdk/TrueError;
          i1 = 13;
          localTrueError.<init>(i1);
          ((TrueResponse)localObject).<init>(localTrueError);
          h = ((TrueResponse)localObject);
          a(0, i1);
        }
        else
        {
          int n = -1;
          a(n, n);
        }
      }
      else
      {
        localObject = new com/truecaller/android/sdk/TrueResponse;
        localTrueError = new com/truecaller/android/sdk/TrueError;
        i1 = 2;
        localTrueError.<init>(i1);
        ((TrueResponse)localObject).<init>(localTrueError);
        h = ((TrueResponse)localObject);
        a(0, i1);
      }
      Object localObject = a;
      if (localObject != null)
      {
        ((com.truecaller.sdk.d.a)localObject).e();
        return;
      }
    }
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    TrueResponse localTrueResponse = h;
    if (localTrueResponse != null)
    {
      Object localObject = c;
      ((com.truecaller.sdk.b.a.a)localObject).a(paramInt2);
      com.truecaller.sdk.d.a locala = a;
      if (locala != null)
      {
        localObject = new android/content/Intent;
        ((Intent)localObject).<init>();
        Bundle localBundle = new android/os/Bundle;
        localBundle.<init>();
        localTrueResponse.writeToBundle(localBundle);
        ((Intent)localObject).putExtras(localBundle);
        locala.a(paramInt1, (Intent)localObject);
        return;
      }
      return;
    }
  }
  
  public final void ab_()
  {
    i = false;
    ag localag = (ag)bg.a;
    c.d.f localf = j;
    Object localObject = new com/truecaller/sdk/b/g$a;
    ((g.a)localObject).<init>(this, null);
    localObject = (m)localObject;
    e.b(localag, localf, (m)localObject, 2);
  }
  
  public final boolean b()
  {
    Object localObject1 = j();
    Object localObject2 = "PARTNERINFO_TRUESDK_VERSION";
    localObject1 = ((Bundle)localObject1).getString((String)localObject2);
    if (localObject1 == null) {
      localObject1 = "0";
    }
    localObject2 = "0.5";
    int n = ((String)localObject1).compareTo((String)localObject2);
    if (n < 0)
    {
      localObject1 = new com/truecaller/android/sdk/TrueResponse;
      localObject2 = new com/truecaller/android/sdk/TrueError;
      ((TrueError)localObject2).<init>(6);
      ((TrueResponse)localObject1).<init>((TrueError)localObject2);
      h = ((TrueResponse)localObject1);
      return false;
    }
    return true;
  }
  
  public final String c()
  {
    Object localObject1;
    try
    {
      localObject1 = m;
      localObject2 = e;
      localObject2 = packageName;
      localObject1 = ((PackageManager)localObject1).getApplicationInfo((String)localObject2, 0);
      localObject2 = m;
      localObject1 = ((PackageManager)localObject2).getApplicationLabel((ApplicationInfo)localObject1);
      localObject1 = localObject1.toString();
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      localObject1 = e.packageName;
      Object localObject2 = "partnerInformation.packageName";
      k.a(localObject1, (String)localObject2);
    }
    return (String)localObject1;
  }
  
  public final String d()
  {
    String str = e.partnerKey;
    k.a(str, "partnerInformation.partnerKey");
    return str;
  }
  
  public final String e()
  {
    return "android";
  }
  
  public final void f()
  {
    super.f();
    Object localObject = a;
    if (localObject == null) {
      return;
    }
    boolean bool1 = d;
    boolean bool2 = true;
    if (!bool1)
    {
      bool1 = localObject instanceof b;
      if (!bool1)
      {
        bool1 = false;
        break label44;
      }
    }
    bool1 = true;
    label44:
    d = bool1;
    ((com.truecaller.sdk.d.a)localObject).f();
    bool1 = d;
    if (bool1) {
      ((com.truecaller.sdk.d.a)localObject).a(bool2);
    }
    localObject = this;
    localObject = (ac)this;
    a((ac)localObject);
  }
  
  public final Locale g()
  {
    return e.locale;
  }
  
  public final void h()
  {
    super.h();
    boolean bool = d;
    if (bool)
    {
      n = -1;
      a(n, n);
      localObject = a;
      if (localObject != null) {
        ((com.truecaller.sdk.d.a)localObject).e();
      }
      return;
    }
    int n = 1;
    i = n;
    Object localObject = a;
    if (localObject != null) {
      ((com.truecaller.sdk.d.a)localObject).g();
    }
    localObject = this;
    localObject = (ac)this;
    a((ac)localObject);
  }
  
  public final com.truecaller.android.sdk.clients.a i()
  {
    return f;
  }
  
  protected final boolean k()
  {
    boolean bool = super.k();
    if (!bool)
    {
      TrueResponse localTrueResponse = new com/truecaller/android/sdk/TrueResponse;
      TrueError localTrueError = new com/truecaller/android/sdk/TrueError;
      int n = 10;
      localTrueError.<init>(n);
      localTrueResponse.<init>(localTrueError);
      h = localTrueResponse;
    }
    return bool;
  }
  
  public final void l()
  {
    TrueResponse localTrueResponse = new com/truecaller/android/sdk/TrueResponse;
    TrueError localTrueError = new com/truecaller/android/sdk/TrueError;
    localTrueError.<init>(14);
    localTrueResponse.<init>(localTrueError);
    h = localTrueResponse;
    super.l();
  }
  
  public final void m()
  {
    super.m();
    a = null;
    com.truecaller.androidactors.a locala = g;
    if (locala != null)
    {
      locala.a();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */