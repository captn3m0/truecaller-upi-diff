package com.truecaller.sdk.b;

import android.os.Bundle;
import c.g.b.k;
import com.truecaller.android.sdk.TrueProfile;
import com.truecaller.sdk.ae;
import com.truecaller.sdk.ah;
import com.truecaller.sdk.b.a.a.b;
import java.util.Locale;

public abstract class j
  implements i
{
  com.truecaller.sdk.d.a a;
  boolean b;
  com.truecaller.sdk.b.a.a c;
  boolean d;
  private final Bundle e;
  private final com.truecaller.common.g.a f;
  private final ae g;
  private final ah h;
  
  public j(Bundle paramBundle, com.truecaller.common.g.a parama, boolean paramBoolean, ae paramae, ah paramah)
  {
    e = paramBundle;
    f = parama;
    d = paramBoolean;
    g = paramae;
    h = paramah;
    paramBundle = new com/truecaller/sdk/b/a/b;
    parama = g;
    Object localObject = this;
    localObject = (a.b)this;
    paramBundle.<init>(parama, (a.b)localObject);
    paramBundle = (com.truecaller.sdk.b.a.a)paramBundle;
    c = paramBundle;
  }
  
  public final void a(Bundle paramBundle)
  {
    k.b(paramBundle, "outState");
    Bundle localBundle = j();
    paramBundle.putBundle("keySaveInstance", localBundle);
  }
  
  public final void a(com.truecaller.sdk.d.a parama)
  {
    k.b(parama, "presenterView");
    a = parama;
    com.truecaller.sdk.b.a.a locala = c;
    locala.a();
    boolean bool = b();
    if (!bool)
    {
      a(0, 12);
      parama.e();
      return;
    }
    bool = k();
    if (!bool)
    {
      a(0, 10);
      parama.e();
      return;
    }
    parama.a();
  }
  
  public final void a(boolean paramBoolean)
  {
    c.a(paramBoolean);
  }
  
  public abstract boolean b();
  
  public void f()
  {
    c.b();
  }
  
  public Locale g()
  {
    return null;
  }
  
  public void h()
  {
    b = true;
  }
  
  public Bundle j()
  {
    return e;
  }
  
  protected boolean k()
  {
    com.truecaller.common.b.a locala = com.truecaller.common.b.a.F();
    k.a(locala, "ApplicationBase.getAppBase()");
    return locala.p();
  }
  
  public void l()
  {
    int i = 14;
    a(0, i);
    com.truecaller.sdk.d.a locala = a;
    if (locala != null)
    {
      locala.e();
      return;
    }
  }
  
  public void m()
  {
    a = null;
  }
  
  public final boolean n()
  {
    return j().getBoolean("PARTNERINFO_OTHER_NUMBER", false);
  }
  
  public final TrueProfile o()
  {
    Object localObject1 = f;
    k.b(localObject1, "receiver$0");
    TrueProfile localTrueProfile = new com/truecaller/android/sdk/TrueProfile;
    localTrueProfile.<init>();
    Object localObject2 = ((com.truecaller.common.g.a)localObject1).a("profileNumber");
    phoneNumber = ((String)localObject2);
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    String str = ((com.truecaller.common.g.a)localObject1).a("profileFirstName");
    ((StringBuilder)localObject2).append(str);
    ((StringBuilder)localObject2).append(' ');
    str = ((com.truecaller.common.g.a)localObject1).a("profileLastName");
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    firstName = ((String)localObject2);
    localObject2 = ((com.truecaller.common.g.a)localObject1).a("profileCompanyJob");
    jobTitle = ((String)localObject2);
    localObject2 = ((com.truecaller.common.g.a)localObject1).a("profileCompanyName");
    companyName = ((String)localObject2);
    localObject2 = ((com.truecaller.common.g.a)localObject1).a("profileEmail");
    email = ((String)localObject2);
    localObject2 = ((com.truecaller.common.g.a)localObject1).a("profileStreet");
    street = ((String)localObject2);
    localObject2 = ((com.truecaller.common.g.a)localObject1).a("profileZip");
    zipcode = ((String)localObject2);
    localObject2 = ((com.truecaller.common.g.a)localObject1).a("profileCity");
    city = ((String)localObject2);
    localObject2 = ((com.truecaller.common.g.a)localObject1).a("profileFacebook");
    facebookId = ((String)localObject2);
    localObject2 = ((com.truecaller.common.g.a)localObject1).a("profileTwitter");
    twitterId = ((String)localObject2);
    localObject2 = ((com.truecaller.common.g.a)localObject1).a("profileWeb");
    url = ((String)localObject2);
    localObject2 = ((com.truecaller.common.g.a)localObject1).a("profileGender");
    gender = ((String)localObject2);
    localObject1 = ((com.truecaller.common.g.a)localObject1).a("profileAvatar");
    avatarUrl = ((String)localObject1);
    return localTrueProfile;
  }
  
  public final String p()
  {
    com.truecaller.sdk.d.a locala = a;
    if (locala != null)
    {
      boolean bool = locala instanceof com.truecaller.sdk.d.b;
      if (bool) {
        return "FullScreen";
      }
    }
    return "Popup";
  }
  
  public final String q()
  {
    com.truecaller.android.sdk.clients.a locala = i();
    boolean bool = locala.a();
    if (bool) {
      return "skip";
    }
    locala = i();
    bool = locala.b();
    if (bool) {
      return "None";
    }
    return "uan";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.b.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */