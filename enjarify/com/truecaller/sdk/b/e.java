package com.truecaller.sdk.b;

import android.content.Intent;
import android.os.Bundle;
import c.g.b.k;
import com.truecaller.sdk.ae;
import com.truecaller.sdk.ah;

public final class e
  extends j
{
  private final com.truecaller.android.sdk.clients.a e;
  private final Bundle f;
  
  public e(Bundle paramBundle, com.truecaller.common.g.a parama, boolean paramBoolean, ae paramae, ah paramah)
  {
    super(paramBundle, parama, paramBoolean, paramae, paramah);
    f = paramBundle;
    paramBundle = new com/truecaller/android/sdk/clients/a;
    paramBundle.<init>(0, 0);
    e = paramBundle;
  }
  
  public final void a()
  {
    a(0, 2);
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    Object localObject = c;
    ((com.truecaller.sdk.b.a.a)localObject).a(paramInt2);
    com.truecaller.sdk.d.a locala1 = a;
    if (locala1 != null)
    {
      localObject = new android/content/Intent;
      ((Intent)localObject).<init>();
      locala1.a(paramInt1, (Intent)localObject);
    }
    com.truecaller.sdk.d.a locala2 = a;
    if (locala2 != null)
    {
      locala2.e();
      return;
    }
  }
  
  public final boolean b()
  {
    return true;
  }
  
  public final String c()
  {
    String str = f.getString("partnerName", "");
    k.a(str, "extras.getString(PARTNER_NAME, \"\")");
    return str;
  }
  
  public final String d()
  {
    return "inAppKey";
  }
  
  public final String e()
  {
    return "in_app";
  }
  
  public final void f()
  {
    super.f();
    com.truecaller.sdk.d.a locala = a;
    if (locala != null)
    {
      locala.f();
      return;
    }
  }
  
  public final void h()
  {
    super.h();
    int i = -1;
    a(i, i);
  }
  
  public final com.truecaller.android.sdk.clients.a i()
  {
    return e;
  }
  
  public final Bundle j()
  {
    return f;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */