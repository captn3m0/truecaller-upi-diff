package com.truecaller.sdk.b;

import android.os.Bundle;
import com.truecaller.android.sdk.TrueProfile;
import com.truecaller.sdk.b.a.a.b;
import java.util.Locale;

public abstract interface i
  extends a.b
{
  public abstract void a();
  
  public abstract void a(int paramInt1, int paramInt2);
  
  public abstract void a(Bundle paramBundle);
  
  public abstract void a(com.truecaller.sdk.d.a parama);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void f();
  
  public abstract Locale g();
  
  public abstract void h();
  
  public abstract com.truecaller.android.sdk.clients.a i();
  
  public abstract void l();
  
  public abstract void m();
  
  public abstract boolean n();
  
  public abstract TrueProfile o();
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.b.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */