package com.truecaller.sdk.b;

import android.os.Bundle;
import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.android.sdk.PartnerInformation;
import com.truecaller.sdk.ae;
import com.truecaller.sdk.ah;
import com.truecaller.sdk.ao;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public final class c
  extends b
{
  private final PartnerInformation e;
  private final com.truecaller.android.sdk.clients.a f;
  private final ao g;
  
  public c(Bundle paramBundle, com.truecaller.common.g.a parama, ao paramao, boolean paramBoolean, ae paramae, ah paramah)
  {
    super(paramBundle, parama, paramBoolean, paramae, paramah);
    g = paramao;
    paramBundle = j();
    localObject2 = paramBundle.getString("partnerKey");
    boolean bool1 = true;
    int i = 0;
    parama = null;
    int j = 0;
    paramao = null;
    if (localObject2 != null)
    {
      localObject5 = j();
      paramae = "requestNonce";
      localObject4 = ((Bundle)localObject5).getString(paramae);
      if (localObject4 != null)
      {
        localObject5 = j();
        paramae = "partnerName";
        localObject5 = ((Bundle)localObject5).getString(paramae);
        if (localObject5 != null)
        {
          paramae = (ae)localObject2;
          paramae = (CharSequence)localObject2;
          boolean bool2 = m.a(paramae);
          if (!bool2)
          {
            paramae = (ae)localObject4;
            paramae = (CharSequence)localObject4;
            bool2 = m.a(paramae);
            if (!bool2)
            {
              localObject5 = (CharSequence)localObject5;
              paramBoolean = m.a((CharSequence)localObject5);
              if (!paramBoolean)
              {
                localObject5 = j();
                paramae = "lang";
                localObject5 = (CharSequence)((Bundle)localObject5).getString(paramae);
                if (localObject5 != null)
                {
                  paramBoolean = m.a((CharSequence)localObject5);
                  if (!paramBoolean)
                  {
                    paramBoolean = false;
                    localObject5 = null;
                    break label270;
                  }
                }
                paramBoolean = true;
                label270:
                Object localObject6;
                if (!paramBoolean)
                {
                  localObject5 = new java/util/Locale;
                  paramae = j();
                  paramah = "lang";
                  paramae = paramae.getString(paramah);
                  ((Locale)localObject5).<init>(paramae);
                  localObject6 = localObject5;
                }
                else
                {
                  localObject6 = null;
                }
                localObject5 = new com/truecaller/android/sdk/PartnerInformation;
                localObject1 = "0.8";
                String str = "";
                localObject3 = "";
                ((PartnerInformation)localObject5).<init>((String)localObject1, (String)localObject2, str, (String)localObject3, (String)localObject4, (Locale)localObject6);
                break label359;
              }
            }
          }
        }
      }
    }
    paramBoolean = false;
    Object localObject5 = null;
    label359:
    e = ((PartnerInformation)localObject5);
    localObject5 = j();
    paramae = "title";
    localObject5 = ((Bundle)localObject5).getString(paramae);
    if (localObject5 != null) {
      if (localObject5 != null)
      {
        parama = ((String)localObject5).toLowerCase();
        localObject5 = "(this as java.lang.String).toLowerCase()";
        k.a(parama, (String)localObject5);
      }
      else
      {
        paramBundle = new c/u;
        paramBundle.<init>("null cannot be cast to non-null type java.lang.String");
        throw paramBundle;
      }
    }
    if (parama != null)
    {
      localObject5 = d.a();
      paramBoolean = ((List)localObject5).contains(parama);
      if (paramBoolean)
      {
        localObject5 = d.a();
        i = ((List)localObject5).indexOf(parama);
      }
      else
      {
        i = 0;
        parama = null;
      }
    }
    else
    {
      i = 0;
      parama = null;
    }
    localObject5 = j();
    paramae = "skipOption";
    localObject5 = ((Bundle)localObject5).getString(paramae);
    if (localObject5 != null) {
      if (localObject5 != null)
      {
        localObject5 = ((String)localObject5).toLowerCase();
        paramae = "(this as java.lang.String).toLowerCase()";
        k.a(localObject5, paramae);
        if (localObject5 != null)
        {
          paramae = d.b();
          localObject5 = (Integer)paramae.get(localObject5);
          if (localObject5 != null)
          {
            paramae = (ae)localObject5;
            ((Number)localObject5).intValue();
            paramae = j();
            paramah = "PARTNERINFO_OTHER_NUMBER";
            paramae.putBoolean(paramah, bool1);
            if (localObject5 != null) {
              j = ((Integer)localObject5).intValue();
            }
          }
        }
      }
      else
      {
        paramBundle = new c/u;
        paramBundle.<init>("null cannot be cast to non-null type java.lang.String");
        throw paramBundle;
      }
    }
    paramBundle = new com/truecaller/android/sdk/clients/a;
    paramBundle.<init>(j, i);
    f = paramBundle;
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    PartnerInformation localPartnerInformation = e;
    if (localPartnerInformation != null)
    {
      int i = -1;
      if (paramInt1 == i)
      {
        localObject = this;
        localObject = (e.d)this;
        ao.a(localPartnerInformation, (e.d)localObject);
        return;
      }
      Object localObject = c;
      ((com.truecaller.sdk.b.a.a)localObject).a(paramInt2);
      paramInt1 = 14;
      if (paramInt2 == paramInt1)
      {
        g.a(localPartnerInformation);
        return;
      }
      localObject = g;
      String str = c();
      ((ao)localObject).a(localPartnerInformation, str);
      return;
    }
  }
  
  public final boolean b()
  {
    PartnerInformation localPartnerInformation = e;
    return localPartnerInformation != null;
  }
  
  public final String c()
  {
    String str = j().getString("partnerName", "");
    k.a(str, "extras.getString(PARTNER_NAME, \"\")");
    return str;
  }
  
  public final String d()
  {
    Object localObject = e;
    if (localObject != null)
    {
      localObject = partnerKey;
      if (localObject != null) {}
    }
    else
    {
      localObject = "";
    }
    return (String)localObject;
  }
  
  public final String e()
  {
    return "mobile_web";
  }
  
  public final void f()
  {
    super.f();
    com.truecaller.sdk.d.a locala = a;
    if (locala != null)
    {
      locala.f();
      return;
    }
  }
  
  public final Locale g()
  {
    PartnerInformation localPartnerInformation = e;
    if (localPartnerInformation != null) {
      return locale;
    }
    return null;
  }
  
  public final void h()
  {
    super.h();
    boolean bool = d;
    if (bool)
    {
      int i = -1;
      a(i, i);
      localObject1 = a;
      if (localObject1 != null) {
        ((com.truecaller.sdk.d.a)localObject1).e();
      }
      return;
    }
    Object localObject1 = e;
    if (localObject1 != null)
    {
      Object localObject2 = this;
      localObject2 = (e.d)this;
      ao.a((PartnerInformation)localObject1, (e.d)localObject2);
      localObject1 = a;
      if (localObject1 != null)
      {
        ((com.truecaller.sdk.d.a)localObject1).g();
        return;
      }
      return;
    }
  }
  
  public final com.truecaller.android.sdk.clients.a i()
  {
    return f;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */