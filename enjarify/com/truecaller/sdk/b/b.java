package com.truecaller.sdk.b;

import android.os.Bundle;
import c.g.b.k;
import com.truecaller.sdk.ae;
import com.truecaller.sdk.ah;
import e.r;

public abstract class b
  extends j
  implements a
{
  public b(Bundle paramBundle, com.truecaller.common.g.a parama, boolean paramBoolean, ae paramae, ah paramah)
  {
    super(paramBundle, parama, paramBoolean, paramae, paramah);
  }
  
  public final void a()
  {
    boolean bool = b;
    int i;
    if (!bool)
    {
      bool = false;
      localObject = null;
      i = 2;
      a(0, i);
    }
    else
    {
      localObject = c;
      i = -1;
      ((com.truecaller.sdk.b.a.a)localObject).a(i);
    }
    Object localObject = a;
    if (localObject != null)
    {
      ((com.truecaller.sdk.d.a)localObject).e();
      return;
    }
  }
  
  public void onFailure(e.b paramb, Throwable paramThrowable)
  {
    String str = "call";
    k.b(paramb, str);
    k.b(paramThrowable, "t");
    paramb = c;
    int i = -1;
    paramb.a(i);
    paramb = a;
    if (paramb != null)
    {
      paramb.e();
      return;
    }
  }
  
  public void onResponse(e.b paramb, r paramr)
  {
    String str = "call";
    k.b(paramb, str);
    k.b(paramr, "response");
    paramb = c;
    int i = -1;
    paramb.a(i);
    paramb = a;
    if (paramb != null)
    {
      paramb.e();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */