package com.truecaller.sdk.b.a;

import c.a.ag;
import c.g.b.k;
import c.g.b.y;
import c.n;
import c.t;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import java.util.ArrayList;
import org.apache.a.d.d;

public final class b
  implements a
{
  private final com.truecaller.sdk.ae a;
  private final a.c b;
  private final a.a c;
  private final a.d d;
  
  private b(com.truecaller.sdk.ae paramae, a.b paramb, a.c paramc, a.a parama, a.d paramd)
  {
    a = paramae;
    b = paramc;
    c = parama;
    d = paramd;
  }
  
  private final void a(e parame)
  {
    a.s().b(parame);
  }
  
  private final void a(String paramString1, String paramString2)
  {
    Object localObject = c().a(paramString1, paramString2).a();
    k.a(localObject, "analyticsEvent");
    a((e)localObject);
    localObject = new n[1];
    paramString1 = t.a(paramString1, paramString2);
    localObject[0] = paramString1;
    a((n[])localObject);
  }
  
  private final void a(n... paramVarArgs)
  {
    Object localObject1 = ao.b();
    Object localObject2 = (CharSequence)"TruecallerSDK_Popup";
    localObject1 = ((ao.a)localObject1).a((CharSequence)localObject2);
    localObject2 = new c/g/b/y;
    ((y)localObject2).<init>(6);
    String str = b.d();
    n localn = t.a("PartnerKey", str);
    ((y)localObject2).b(localn);
    str = b.c();
    localn = t.a("PartnerName", str);
    ((y)localObject2).b(localn);
    str = d.p();
    localn = t.a("ConsentUI", str);
    ((y)localObject2).b(localn);
    str = c.e();
    localn = t.a("IntegrationType", str);
    ((y)localObject2).b(localn);
    str = d.q();
    localn = t.a("AdditionalCta", str);
    ((y)localObject2).b(localn);
    ((y)localObject2).a(paramVarArgs);
    paramVarArgs = new n[a.size()];
    paramVarArgs = ag.a((n[])((y)localObject2).a(paramVarArgs));
    paramVarArgs = ((ao.a)localObject1).a(paramVarArgs).a();
    localObject1 = (com.truecaller.analytics.ae)a.r().a();
    paramVarArgs = (d)paramVarArgs;
    ((com.truecaller.analytics.ae)localObject1).a(paramVarArgs);
  }
  
  private final e.a c()
  {
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("TruecallerSDK_Popup");
    String str = c.e();
    locala = locala.a("IntegrationType", str);
    str = b.c();
    locala = locala.a("PartnerName", str);
    str = d.p();
    locala = locala.a("ConsentUI", str);
    str = d.q();
    locala = locala.a("AdditionalCta", str);
    k.a(locala, "Builder(TcSdkEvent.NAME)…r.getAdditionalCtaText())");
    return locala;
  }
  
  public final void a()
  {
    a("PopupState", "requested");
  }
  
  public final void a(int paramInt)
  {
    Object localObject1 = c().a("PopupState", "dismissed").a("DismissReason", paramInt).a();
    k.a(localObject1, "analyticsEvent");
    a((e)localObject1);
    localObject1 = new n[2];
    n localn = t.a("PopupState", "dismissed");
    localObject1[0] = localn;
    Object localObject2 = String.valueOf(paramInt);
    localObject2 = t.a("DismissReason", localObject2);
    localObject1[1] = localObject2;
    a((n[])localObject1);
  }
  
  public final void a(boolean paramBoolean)
  {
    String str = String.valueOf(paramBoolean);
    a("InfoExpanded", str);
  }
  
  public final void b()
  {
    a("PopupState", "shown");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.b.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */