package com.truecaller.sdk.b;

import android.os.Bundle;
import c.g.b.k;
import c.n.m;
import com.truecaller.sdk.ae;
import com.truecaller.sdk.ah;
import com.truecaller.sdk.ao;
import e.d;

public final class h
  extends b
{
  private final Bundle e;
  private final com.truecaller.android.sdk.clients.a f;
  private final ao g;
  
  private h(Bundle paramBundle, com.truecaller.android.sdk.clients.a parama, com.truecaller.common.g.a parama1, ao paramao, boolean paramBoolean, ae paramae, ah paramah)
  {
    super(paramBundle, parama1, paramBoolean, paramae, paramah);
    e = paramBundle;
    f = parama;
    g = paramao;
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    Object localObject1 = e;
    String str = "qr_scan_code";
    localObject1 = ((Bundle)localObject1).getString(str);
    if (localObject1 != null)
    {
      int i = -1;
      if (paramInt1 == i)
      {
        Object localObject2 = this;
        localObject2 = (d)this;
        ao.a((String)localObject1, (d)localObject2);
        return;
      }
      c.a(paramInt2);
      g.a((String)localObject1);
      return;
    }
  }
  
  public final boolean b()
  {
    return e.containsKey("qr_partner_name");
  }
  
  public final String c()
  {
    String str = e.getString("qr_partner_name", "");
    k.a(str, "extras.getString(QR_PARTNER_NAME, \"\")");
    return str;
  }
  
  public final String d()
  {
    Object localObject = e;
    String str = "qr_scan_code";
    localObject = ((Bundle)localObject).getString(str);
    if (localObject != null)
    {
      str = "|";
      localObject = m.a((String)localObject, str);
      if (localObject != null) {}
    }
    else
    {
      localObject = "";
    }
    return (String)localObject;
  }
  
  public final String e()
  {
    return "qr_code";
  }
  
  public final void f()
  {
    super.f();
    com.truecaller.sdk.d.a locala = a;
    if (locala != null)
    {
      locala.f();
      return;
    }
  }
  
  public final void h()
  {
    super.h();
    boolean bool = d;
    if (bool)
    {
      int i = -1;
      a(i, i);
      localObject1 = a;
      if (localObject1 != null) {
        ((com.truecaller.sdk.d.a)localObject1).e();
      }
      return;
    }
    Object localObject1 = e;
    Object localObject2 = "qr_scan_code";
    localObject1 = ((Bundle)localObject1).getString((String)localObject2);
    if (localObject1 != null)
    {
      localObject2 = this;
      localObject2 = (d)this;
      ao.a((String)localObject1, (d)localObject2);
      localObject1 = a;
      if (localObject1 != null)
      {
        ((com.truecaller.sdk.d.a)localObject1).g();
        return;
      }
      return;
    }
  }
  
  public final com.truecaller.android.sdk.clients.a i()
  {
    return f;
  }
  
  public final Bundle j()
  {
    return e;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.b.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */