package com.truecaller.sdk;

import android.app.NotificationManager;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import com.truecaller.android.sdk.TrueProfile;
import com.truecaller.sdk.b.h;
import com.truecaller.utils.n;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

class f
  extends e
{
  protected final PackageManager b;
  com.truecaller.android.sdk.clients.a c;
  protected boolean d = false;
  com.truecaller.sdk.b.i e;
  private final c.d.f g;
  private final com.truecaller.androidactors.f h;
  private final com.truecaller.androidactors.i i;
  private final NotificationManager j;
  private final ao k;
  private final ah l;
  private final am m;
  private final com.truecaller.common.g.a n;
  private final n o;
  private final ae p;
  private ag.a q;
  private final TelephonyManager r;
  private Locale s;
  private final com.truecaller.analytics.a.f t;
  private final com.truecaller.sdk.utils.a u;
  private final a v;
  
  f(c.d.f paramf, com.truecaller.androidactors.i parami, com.truecaller.androidactors.f paramf1, TelephonyManager paramTelephonyManager, PackageManager paramPackageManager, NotificationManager paramNotificationManager, ae paramae, ao paramao, ah paramah, com.truecaller.common.g.a parama, ag.a parama1, am paramam, n paramn, com.truecaller.analytics.a.f paramf2, com.truecaller.sdk.utils.a parama2, a parama3)
  {
    g = paramf;
    h = paramf1;
    i = parami;
    r = paramTelephonyManager;
    b = paramPackageManager;
    j = paramNotificationManager;
    p = paramae;
    k = paramao;
    l = paramah;
    n = parama;
    q = parama1;
    m = paramam;
    o = paramn;
    t = paramf2;
    u = parama2;
    v = parama3;
  }
  
  private static String b(TrueProfile paramTrueProfile)
  {
    CharSequence[] arrayOfCharSequence = new CharSequence[2];
    String str = firstName;
    arrayOfCharSequence[0] = str;
    paramTrueProfile = lastName;
    arrayOfCharSequence[1] = paramTrueProfile;
    return com.truecaller.common.h.am.a(" ", arrayOfCharSequence);
  }
  
  public final void a()
  {
    super.a();
    com.truecaller.sdk.b.i locali = e;
    if (locali != null) {
      locali.m();
    }
  }
  
  final void a(TrueProfile paramTrueProfile)
  {
    Object localObject = n;
    long l1 = 0L;
    long l2 = ((com.truecaller.common.g.a)localObject).a("profileVerificationDate", l1);
    verificationTimestamp = l2;
    localObject = n;
    String str = "profileVerificationMode";
    localObject = ((com.truecaller.common.g.a)localObject).a(str);
    verificationMode = ((String)localObject);
    boolean bool = false;
    isSimChanged = false;
    localObject = s;
    if (localObject != null) {
      userLocale = ((Locale)localObject);
    }
    localObject = (com.truecaller.sdk.d.a)a;
    bool = ((com.truecaller.sdk.d.a)localObject).c();
    if (bool)
    {
      localObject = n;
      str = "profileSimNumber";
      localObject = ((com.truecaller.common.g.a)localObject).a(str);
      bool = com.truecaller.common.h.am.b((CharSequence)localObject);
      if (!bool)
      {
        localObject = r;
        if (localObject != null)
        {
          localObject = ((TelephonyManager)localObject).getSimSerialNumber();
          bool = com.truecaller.common.h.am.b((CharSequence)localObject);
          if (!bool)
          {
            localObject = n.a("profileSimNumber");
            str = r.getSimSerialNumber();
            bool = ((String)localObject).equals(str);
            if (!bool)
            {
              bool = true;
              isSimChanged = bool;
            }
          }
        }
      }
    }
  }
  
  public final boolean a(Bundle paramBundle)
  {
    if (paramBundle == null) {
      paramBundle = v.b();
    }
    if (paramBundle == null) {
      return false;
    }
    Object localObject1 = g;
    Object localObject2 = j;
    Object localObject3 = k;
    Object localObject4 = h;
    Object localObject5 = i;
    Object localObject6 = n;
    Object localObject7 = b;
    boolean bool1 = q.D();
    Object localObject8 = p;
    ah localah = l;
    Object localObject9 = v;
    boolean bool2 = ((a)localObject9).a();
    c.g.b.k.b(localObject1, "uiContext");
    c.g.b.k.b(paramBundle, "extras");
    c.g.b.k.b(localObject2, "notificationManager");
    c.g.b.k.b(localObject3, "sdkRepository");
    c.g.b.k.b(localObject4, "sdkHelper");
    c.g.b.k.b(localObject5, "uiThread");
    c.g.b.k.b(localObject6, "coreSettings");
    c.g.b.k.b(localObject7, "packageManager");
    c.g.b.k.b(localObject8, "eventsTrackerHolder");
    c.g.b.k.b(localah, "sdkAccountManager");
    Object localObject10 = "PARTNERINFO_TRUESDK_VERSION";
    boolean bool3 = paramBundle.containsKey((String)localObject10);
    if (bool3)
    {
      localObject10 = new com/truecaller/sdk/b/g;
      localObject9 = localObject10;
      localObject2 = paramBundle;
      localObject3 = localObject6;
      localObject6 = localObject7;
      Object localObject11 = localObject8;
      localObject8 = localah;
      ((com.truecaller.sdk.b.g)localObject10).<init>((c.d.f)localObject1, paramBundle, (com.truecaller.common.g.a)localObject3, (com.truecaller.androidactors.f)localObject4, (com.truecaller.androidactors.i)localObject5, (PackageManager)localObject7, bool1, (ae)localObject11, localah);
      localObject10 = (com.truecaller.sdk.b.i)localObject10;
    }
    else
    {
      localObject1 = "a";
      boolean bool4 = paramBundle.containsKey((String)localObject1);
      if (bool4)
      {
        localObject10 = new com/truecaller/sdk/b/k;
        localObject9 = localObject10;
        localObject1 = paramBundle;
        localObject4 = localObject6;
        localObject6 = localObject8;
        localObject7 = localah;
        ((com.truecaller.sdk.b.k)localObject10).<init>(paramBundle, (NotificationManager)localObject2, (ao)localObject3, (com.truecaller.common.g.a)localObject4, bool1, (ae)localObject8, localah);
        localObject10 = (com.truecaller.sdk.b.i)localObject10;
      }
      else
      {
        localObject1 = "qr_scan_code";
        bool4 = paramBundle.containsKey((String)localObject1);
        if (bool4)
        {
          localObject7 = new com/truecaller/sdk/b/h;
          localObject9 = localObject7;
          localObject1 = paramBundle;
          localObject2 = localObject6;
          localObject5 = localObject8;
          localObject6 = localah;
          ((h)localObject7).<init>(paramBundle, (com.truecaller.common.g.a)localObject2, (ao)localObject3, bool1, (ae)localObject8, localah);
          localObject10 = localObject7;
          localObject10 = (com.truecaller.sdk.b.i)localObject7;
        }
        else if (bool2)
        {
          localObject7 = new com/truecaller/sdk/b/e;
          localObject9 = localObject7;
          localObject1 = paramBundle;
          localObject2 = localObject6;
          localObject4 = localObject8;
          localObject5 = localah;
          ((com.truecaller.sdk.b.e)localObject7).<init>(paramBundle, (com.truecaller.common.g.a)localObject6, bool1, (ae)localObject8, localah);
          localObject10 = localObject7;
          localObject10 = (com.truecaller.sdk.b.i)localObject7;
        }
        else
        {
          localObject7 = new com/truecaller/sdk/b/c;
          localObject9 = localObject7;
          localObject1 = paramBundle;
          localObject2 = localObject6;
          localObject5 = localObject8;
          localObject6 = localah;
          ((com.truecaller.sdk.b.c)localObject7).<init>(paramBundle, (com.truecaller.common.g.a)localObject2, (ao)localObject3, bool1, (ae)localObject8, localah);
          localObject10 = localObject7;
          localObject10 = (com.truecaller.sdk.b.i)localObject7;
        }
      }
    }
    e = ((com.truecaller.sdk.b.i)localObject10);
    paramBundle = e.i();
    c = paramBundle;
    return true;
  }
  
  public final void b()
  {
    e.a();
  }
  
  public final void b(Bundle paramBundle)
  {
    e.a(paramBundle);
  }
  
  public void c()
  {
    Object localObject1 = a;
    if (localObject1 != null)
    {
      localObject1 = e.o();
      Object localObject2 = n;
      long l1 = 0L;
      long l2 = ((com.truecaller.common.g.a)localObject2).a("profileVerificationDate", l1);
      verificationTimestamp = l2;
      localObject2 = n.a("profileVerificationMode");
      verificationMode = ((String)localObject2);
      int i1 = 0;
      localObject2 = null;
      isSimChanged = false;
      Object localObject3 = s;
      if (localObject3 != null) {
        userLocale = ((Locale)localObject3);
      }
      localObject3 = (com.truecaller.sdk.d.a)a;
      boolean bool1 = ((com.truecaller.sdk.d.a)localObject3).c();
      int i2 = 1;
      if (bool1)
      {
        localObject3 = n;
        localObject4 = "profileSimNumber";
        localObject3 = ((com.truecaller.common.g.a)localObject3).a((String)localObject4);
        bool1 = com.truecaller.common.h.am.b((CharSequence)localObject3);
        if (!bool1)
        {
          localObject3 = r;
          if (localObject3 != null)
          {
            localObject3 = ((TelephonyManager)localObject3).getSimSerialNumber();
            bool1 = com.truecaller.common.h.am.b((CharSequence)localObject3);
            if (!bool1)
            {
              localObject3 = n.a("profileSimNumber");
              localObject4 = r.getSimSerialNumber();
              bool1 = ((String)localObject3).equals(localObject4);
              if (!bool1) {
                isSimChanged = i2;
              }
            }
          }
        }
      }
      localObject3 = b((TrueProfile)localObject1);
      Object localObject4 = e.c();
      Object localObject5 = (com.truecaller.sdk.d.a)a;
      String str1 = phoneNumber;
      Object localObject6 = o;
      int i3 = R.array.SdkPartnerLoginIntentOptionsArray;
      localObject6 = ((n)localObject6).a(i3);
      i3 = c.c();
      localObject6 = localObject6[i3];
      Object localObject7 = new Object[i2];
      localObject7[0] = localObject4;
      localObject6 = String.format((String)localObject6, (Object[])localObject7);
      ((com.truecaller.sdk.d.a)localObject5).a(str1, (String)localObject4, (String)localObject3, (String)localObject6);
      bool1 = f;
      if (!bool1)
      {
        localObject3 = c;
        if (localObject3 == null)
        {
          localObject1 = new java/lang/AssertionError;
          ((AssertionError)localObject1).<init>();
          throw ((Throwable)localObject1);
        }
      }
      localObject3 = c;
      bool1 = ((com.truecaller.android.sdk.clients.a)localObject3).b();
      int i5;
      if (!bool1)
      {
        localObject3 = e;
        bool1 = ((com.truecaller.sdk.b.i)localObject3).n();
        if (bool1)
        {
          localObject3 = o;
          localObject4 = c;
          boolean bool3 = ((com.truecaller.android.sdk.clients.a)localObject4).a();
          if (bool3) {
            i5 = R.string.SdkSkip;
          } else {
            i5 = R.string.SdkUseDifferentNumber;
          }
          localObject5 = new Object[0];
          localObject3 = ((n)localObject3).a(i5, (Object[])localObject5);
          localObject4 = (com.truecaller.sdk.d.a)a;
          ((com.truecaller.sdk.d.a)localObject4).a((String)localObject3);
        }
      }
      localObject3 = avatarUrl;
      bool1 = com.truecaller.common.h.am.b((CharSequence)localObject3);
      if (!bool1)
      {
        localObject3 = (com.truecaller.sdk.d.a)a;
        localObject4 = avatarUrl;
        ((com.truecaller.sdk.d.a)localObject3).b((String)localObject4);
      }
      localObject3 = a;
      if (localObject3 != null)
      {
        localObject3 = a;
        bool1 = localObject3 instanceof com.truecaller.sdk.d.c;
        i5 = 77;
        int i6 = 70;
        int i7 = 3;
        i3 = 2;
        String str3;
        label903:
        String str4;
        if (bool1)
        {
          localObject3 = new java/util/ArrayList;
          ((ArrayList)localObject3).<init>();
          localObject8 = new com/truecaller/sdk/a/g;
          str2 = phoneNumber;
          ((com.truecaller.sdk.a.g)localObject8).<init>(str2);
          ((List)localObject3).add(localObject8);
          localObject8 = new com/truecaller/sdk/a/b;
          str2 = b((TrueProfile)localObject1);
          ((com.truecaller.sdk.a.b)localObject8).<init>(str2);
          ((List)localObject3).add(localObject8);
          localObject8 = jobTitle;
          boolean bool5 = com.truecaller.common.h.am.b((CharSequence)localObject8);
          if (bool5)
          {
            localObject8 = companyName;
            bool5 = com.truecaller.common.h.am.b((CharSequence)localObject8);
            if (bool5) {}
          }
          else
          {
            localObject8 = new com/truecaller/sdk/a/b;
            localObject9 = new CharSequence[i3];
            str3 = jobTitle;
            localObject9[0] = str3;
            str3 = companyName;
            localObject9[i2] = str3;
            str2 = com.truecaller.common.h.am.a(" @ ", (CharSequence[])localObject9);
            ((com.truecaller.sdk.a.b)localObject8).<init>(str2);
            ((List)localObject3).add(localObject8);
          }
          localObject8 = email;
          bool5 = com.truecaller.common.h.am.b((CharSequence)localObject8);
          if (!bool5)
          {
            localObject8 = new com/truecaller/sdk/a/b;
            str2 = email;
            ((com.truecaller.sdk.a.b)localObject8).<init>(str2);
            ((List)localObject3).add(localObject8);
          }
          localObject8 = street;
          bool5 = com.truecaller.common.h.am.b((CharSequence)localObject8);
          if (bool5)
          {
            localObject8 = zipcode;
            bool5 = com.truecaller.common.h.am.b((CharSequence)localObject8);
            if (bool5)
            {
              localObject8 = city;
              bool5 = com.truecaller.common.h.am.b((CharSequence)localObject8);
              if (bool5) {
                break label903;
              }
            }
          }
          localObject8 = new com/truecaller/sdk/a/b;
          str2 = ", ";
          localObject6 = new CharSequence[i7];
          localObject9 = street;
          localObject6[0] = localObject9;
          localObject9 = city;
          localObject6[i2] = localObject9;
          localObject9 = zipcode;
          localObject6[i3] = localObject9;
          localObject6 = com.truecaller.common.h.am.a(str2, (CharSequence[])localObject6);
          ((com.truecaller.sdk.a.b)localObject8).<init>((String)localObject6);
          ((List)localObject3).add(localObject8);
          localObject6 = facebookId;
          bool4 = com.truecaller.common.h.am.b((CharSequence)localObject6);
          if (!bool4)
          {
            localObject6 = new com/truecaller/sdk/a/b;
            localObject8 = facebookId;
            ((com.truecaller.sdk.a.b)localObject6).<init>((String)localObject8);
            ((List)localObject3).add(localObject6);
          }
          localObject6 = twitterId;
          bool4 = com.truecaller.common.h.am.b((CharSequence)localObject6);
          if (!bool4)
          {
            localObject6 = new com/truecaller/sdk/a/b;
            localObject8 = twitterId;
            ((com.truecaller.sdk.a.b)localObject6).<init>((String)localObject8);
            ((List)localObject3).add(localObject6);
          }
          localObject6 = url;
          bool4 = com.truecaller.common.h.am.b((CharSequence)localObject6);
          if (!bool4)
          {
            localObject6 = new com/truecaller/sdk/a/b;
            localObject8 = url;
            ((com.truecaller.sdk.a.b)localObject6).<init>((String)localObject8);
            ((List)localObject3).add(localObject6);
          }
          localObject6 = "";
          localObject8 = a;
          if (localObject8 != null)
          {
            localObject8 = gender;
            bool5 = TextUtils.isEmpty((CharSequence)localObject8);
            if (!bool5)
            {
              localObject1 = gender;
              int i8 = ((String)localObject1).hashCode();
              if (i8 != i6)
              {
                if (i8 == i5)
                {
                  str4 = "M";
                  bool7 = ((String)localObject1).equals(str4);
                  if (bool7) {
                    break label1149;
                  }
                }
              }
              else
              {
                localObject2 = "F";
                bool7 = ((String)localObject1).equals(localObject2);
                if (bool7)
                {
                  i1 = 1;
                  break label1149;
                }
              }
              i1 = -1;
              switch (i1)
              {
              default: 
                break;
              case 1: 
                localObject1 = (com.truecaller.sdk.d.a)a;
                i1 = R.string.ProfileEditGenderFemale;
                localObject6 = ((com.truecaller.sdk.d.a)localObject1).a(i1);
                break;
              case 0: 
                label1149:
                localObject1 = (com.truecaller.sdk.d.a)a;
                i1 = R.string.ProfileEditGenderMale;
                localObject6 = ((com.truecaller.sdk.d.a)localObject1).a(i1);
              }
            }
          }
          boolean bool7 = com.truecaller.common.h.am.b((CharSequence)localObject6);
          if (!bool7)
          {
            localObject1 = new com/truecaller/sdk/a/b;
            ((com.truecaller.sdk.a.b)localObject1).<init>((String)localObject6);
            ((List)localObject3).add(localObject1);
          }
          localObject1 = (com.truecaller.sdk.d.c)a;
          ((com.truecaller.sdk.d.c)localObject1).a((List)localObject3);
          int i10 = ((List)localObject3).size();
          if (i10 > i3)
          {
            localObject1 = (com.truecaller.sdk.d.c)a;
            ((com.truecaller.sdk.d.c)localObject1).d();
          }
          return;
        }
        localObject3 = new java/util/ArrayList;
        ((ArrayList)localObject3).<init>();
        Object localObject8 = new com/truecaller/sdk/a/e;
        String str2 = phoneNumber;
        int i11 = R.drawable.ic_sdk_phone;
        ((com.truecaller.sdk.a.e)localObject8).<init>(str2, i11);
        ((List)localObject3).add(localObject8);
        localObject8 = jobTitle;
        boolean bool6 = com.truecaller.common.h.am.b((CharSequence)localObject8);
        if (bool6)
        {
          localObject8 = companyName;
          bool6 = com.truecaller.common.h.am.b((CharSequence)localObject8);
          if (bool6) {}
        }
        else
        {
          localObject8 = new com/truecaller/sdk/a/e;
          localObject9 = new CharSequence[i3];
          str3 = jobTitle;
          localObject9[0] = str3;
          str3 = companyName;
          localObject9[i2] = str3;
          str2 = com.truecaller.common.h.am.a(" @ ", (CharSequence[])localObject9);
          i11 = R.drawable.ic_sdk_work;
          ((com.truecaller.sdk.a.e)localObject8).<init>(str2, i11);
          ((List)localObject3).add(localObject8);
        }
        localObject8 = email;
        bool6 = com.truecaller.common.h.am.b((CharSequence)localObject8);
        if (!bool6)
        {
          localObject8 = new com/truecaller/sdk/a/e;
          str2 = email;
          i11 = R.drawable.ic_sdk_mail;
          ((com.truecaller.sdk.a.e)localObject8).<init>(str2, i11);
          ((List)localObject3).add(localObject8);
        }
        localObject8 = street;
        bool6 = com.truecaller.common.h.am.b((CharSequence)localObject8);
        if (bool6)
        {
          localObject8 = zipcode;
          bool6 = com.truecaller.common.h.am.b((CharSequence)localObject8);
          if (bool6)
          {
            localObject8 = city;
            bool6 = com.truecaller.common.h.am.b((CharSequence)localObject8);
            if (bool6) {
              break label1649;
            }
          }
        }
        localObject8 = new com/truecaller/sdk/a/e;
        str2 = ", ";
        localObject6 = new CharSequence[bool4];
        Object localObject9 = street;
        localObject6[0] = localObject9;
        localObject9 = city;
        localObject6[i2] = localObject9;
        localObject9 = zipcode;
        localObject6[i3] = localObject9;
        localObject6 = com.truecaller.common.h.am.a(str2, (CharSequence[])localObject6);
        i3 = R.drawable.ic_sdk_address;
        ((com.truecaller.sdk.a.e)localObject8).<init>((String)localObject6, i3);
        ((List)localObject3).add(localObject8);
        label1649:
        localObject6 = facebookId;
        boolean bool4 = com.truecaller.common.h.am.b((CharSequence)localObject6);
        int i9;
        if (!bool4)
        {
          localObject6 = new com/truecaller/sdk/a/e;
          localObject7 = facebookId;
          i9 = R.drawable.ic_sdk_facebook;
          ((com.truecaller.sdk.a.e)localObject6).<init>((String)localObject7, i9);
          ((List)localObject3).add(localObject6);
        }
        localObject6 = twitterId;
        bool4 = com.truecaller.common.h.am.b((CharSequence)localObject6);
        if (!bool4)
        {
          localObject6 = new com/truecaller/sdk/a/e;
          localObject7 = twitterId;
          i9 = R.drawable.ic_sdk_twitter;
          ((com.truecaller.sdk.a.e)localObject6).<init>((String)localObject7, i9);
          ((List)localObject3).add(localObject6);
        }
        localObject6 = url;
        bool4 = com.truecaller.common.h.am.b((CharSequence)localObject6);
        if (!bool4)
        {
          localObject6 = new com/truecaller/sdk/a/e;
          localObject7 = url;
          i9 = R.drawable.ic_sdk_link;
          ((com.truecaller.sdk.a.e)localObject6).<init>((String)localObject7, i9);
          ((List)localObject3).add(localObject6);
        }
        bool4 = false;
        localObject6 = null;
        localObject7 = a;
        if (localObject7 != null)
        {
          localObject7 = gender;
          boolean bool2 = TextUtils.isEmpty((CharSequence)localObject7);
          if (!bool2)
          {
            localObject1 = gender;
            int i4 = ((String)localObject1).hashCode();
            boolean bool8;
            if (i4 != i6)
            {
              if (i4 == i5)
              {
                str4 = "M";
                bool8 = ((String)localObject1).equals(str4);
                if (bool8)
                {
                  i2 = 0;
                  str4 = null;
                  break label1922;
                }
              }
            }
            else
            {
              localObject4 = "F";
              bool8 = ((String)localObject1).equals(localObject4);
              if (bool8) {
                break label1922;
              }
            }
            i2 = -1;
            switch (i2)
            {
            default: 
              break;
            case 1: 
              i1 = R.drawable.ic_sdk_female;
              localObject1 = (com.truecaller.sdk.d.a)a;
              i2 = R.string.ProfileEditGenderFemale;
              localObject6 = ((com.truecaller.sdk.d.a)localObject1).a(i2);
              break;
            case 0: 
              label1922:
              i1 = R.drawable.ic_sdk_male;
              localObject1 = (com.truecaller.sdk.d.a)a;
              i2 = R.string.ProfileEditGenderMale;
              localObject6 = ((com.truecaller.sdk.d.a)localObject1).a(i2);
            }
          }
        }
        if (i1 != 0)
        {
          localObject1 = new com/truecaller/sdk/a/e;
          ((com.truecaller.sdk.a.e)localObject1).<init>((String)localObject6, i1);
          ((List)localObject3).add(localObject1);
        }
        localObject1 = (com.truecaller.sdk.d.b)a;
        ((com.truecaller.sdk.d.b)localObject1).a((List)localObject3);
      }
    }
  }
  
  public void d()
  {
    Object localObject = a;
    if (localObject != null)
    {
      boolean bool1 = d ^ true;
      d = bool1;
      localObject = (com.truecaller.sdk.d.a)a;
      boolean bool2 = d;
      ((com.truecaller.sdk.d.a)localObject).b(bool2);
      localObject = e;
      bool2 = d;
      ((com.truecaller.sdk.b.i)localObject).a(bool2);
    }
  }
  
  public final void e()
  {
    t.a("ab_test_sdk_confirm_17858_converted");
    e.h();
  }
  
  final void f()
  {
    Locale localLocale = s;
    if (localLocale != null)
    {
      am localam = m;
      localam.a(localLocale);
    }
  }
  
  final void g()
  {
    Object localObject1 = a;
    if (localObject1 != null)
    {
      localObject1 = e.g();
      if (localObject1 != null)
      {
        localObject1 = m.a();
        s = ((Locale)localObject1);
        localObject1 = m;
        localObject2 = e.g();
        ((am)localObject1).a((Locale)localObject2);
      }
      ((com.truecaller.sdk.d.a)a).b();
      localObject1 = (com.truecaller.sdk.d.a)a;
      Object localObject2 = new android/text/SpannableStringBuilder;
      Object localObject3 = o;
      int i1 = R.string.SdkVerifiedBy;
      Object[] arrayOfObject = new Object[0];
      localObject3 = ((n)localObject3).a(i1, arrayOfObject);
      ((SpannableStringBuilder)localObject2).<init>((CharSequence)localObject3);
      localObject3 = o;
      i1 = R.drawable.ic_sdk_tc_logo;
      localObject3 = ((n)localObject3).c(i1).mutate();
      i1 = ((Drawable)localObject3).getIntrinsicWidth();
      int i2 = ((Drawable)localObject3).getIntrinsicHeight();
      ((Drawable)localObject3).setBounds(0, 0, i1, i2);
      Object localObject4 = u;
      int i3 = R.color.truecaller_blue;
      i1 = ((com.truecaller.sdk.utils.a)localObject4).a(i3);
      Object localObject5 = PorterDuff.Mode.SRC_ATOP;
      ((Drawable)localObject3).setColorFilter(i1, (PorterDuff.Mode)localObject5);
      localObject4 = ((SpannableStringBuilder)localObject2).toString();
      localObject5 = "**";
      i1 = ((String)localObject4).indexOf((String)localObject5);
      i3 = -1;
      i2 = 18;
      int i4;
      if (i1 == i3)
      {
        localObject4 = ((SpannableStringBuilder)localObject2).append(" ");
        localObject5 = new android/text/style/ImageSpan;
        ((ImageSpan)localObject5).<init>((Drawable)localObject3);
        i4 = ((SpannableStringBuilder)localObject2).length() + -1;
        int i5 = ((SpannableStringBuilder)localObject2).length();
        ((SpannableStringBuilder)localObject4).setSpan(localObject5, i4, i5, i2);
      }
      else
      {
        localObject5 = new android/text/style/ImageSpan;
        ((ImageSpan)localObject5).<init>((Drawable)localObject3);
        i4 = i1 + 2;
        ((SpannableStringBuilder)localObject2).setSpan(localObject5, i1, i4, i2);
      }
      ((com.truecaller.sdk.d.a)localObject1).a((SpannableStringBuilder)localObject2);
      localObject1 = t;
      localObject2 = "ab_test_sdk_confirm_17858_seen";
      ((com.truecaller.analytics.a.f)localObject1).a((String)localObject2);
      localObject1 = e;
      ((com.truecaller.sdk.b.i)localObject1).f();
    }
  }
  
  final void h()
  {
    e.l();
  }
  
  final void i()
  {
    Object localObject = e;
    boolean bool = localObject instanceof com.truecaller.sdk.b.f;
    if (bool)
    {
      localObject = (com.truecaller.sdk.b.f)localObject;
      ((com.truecaller.sdk.b.f)localObject).ab_();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.sdk.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */