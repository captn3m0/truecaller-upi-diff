package com.truecaller.payments.network;

class RestModel$TcTokenResponse
{
  private String token;
  
  public RestModel$TcTokenResponse(String paramString)
  {
    token = paramString;
  }
  
  public String getToken()
  {
    return token;
  }
  
  public void setToken(String paramString)
  {
    token = paramString;
  }
  
  public String toString()
  {
    String str = token;
    if (str == null) {
      return "null";
    }
    return "<non-null token>";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.payments.network.RestModel.TcTokenResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */