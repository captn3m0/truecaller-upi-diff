package com.truecaller.payments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.truecaller.ui.TruecallerInit;

public final class BankingActivity
  extends Activity
{
  protected final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = this;
    paramBundle = TruecallerInit.a((Context)this, "banking", "homescreenShortcut");
    startActivity(paramBundle);
    finish();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.payments.BankingActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */