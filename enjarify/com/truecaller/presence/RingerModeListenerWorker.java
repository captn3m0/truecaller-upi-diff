package com.truecaller.presence;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;

public final class RingerModeListenerWorker
  extends Worker
{
  public static final RingerModeListenerWorker.a c;
  public c b;
  
  static
  {
    RingerModeListenerWorker.a locala = new com/truecaller/presence/RingerModeListenerWorker$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public RingerModeListenerWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
  }
  
  public static final void b() {}
  
  public final ListenableWorker.a a()
  {
    new String[1][0] = "Ringer mode has changed. Reporting presence...";
    Object localObject1 = getApplicationContext();
    if (localObject1 != null)
    {
      ((bk)localObject1).a().a(this);
      localObject1 = b;
      if (localObject1 == null)
      {
        localObject2 = "presenceManager";
        k.a((String)localObject2);
      }
      Object localObject2 = AvailabilityTrigger.USER_ACTION;
      ((c)localObject1).a((AvailabilityTrigger)localObject2, true);
      localObject1 = ListenableWorker.a.a();
      k.a(localObject1, "Result.success()");
      return (ListenableWorker.a)localObject1;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.RingerModeListenerWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */