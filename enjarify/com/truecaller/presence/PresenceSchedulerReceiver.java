package com.truecaller.presence;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;

public class PresenceSchedulerReceiver
  extends BroadcastReceiver
  implements l
{
  private Context a;
  
  public PresenceSchedulerReceiver() {}
  
  public PresenceSchedulerReceiver(Context paramContext)
  {
    a = paramContext;
  }
  
  private Intent a(String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramString);
    paramString = a;
    return localIntent.setClass(paramString, PresenceSchedulerReceiver.class);
  }
  
  public final void a()
  {
    new String[1][0] = "cancelPendingAndScheduleSetLastSeen with delay 180000";
    AlarmManager localAlarmManager = (AlarmManager)a.getSystemService("alarm");
    Object localObject = a;
    Intent localIntent = a("com.truecaller.action.ACTION_SET_LAST_SEEN");
    localObject = PendingIntent.getBroadcast((Context)localObject, 2131364157, localIntent, 0);
    long l = SystemClock.elapsedRealtime() + 180000L;
    localAlarmManager.set(2, l, (PendingIntent)localObject);
  }
  
  public final void a(long paramLong)
  {
    AlarmManager localAlarmManager = (AlarmManager)a.getSystemService("alarm");
    Object localObject = a;
    Intent localIntent = a("com.truecaller.action.ACTION_UPDATE_PRESENCE_FOR_CURRENT_USER");
    localObject = PendingIntent.getBroadcast((Context)localObject, 2131364154, localIntent, 0);
    long l = SystemClock.elapsedRealtime() + paramLong;
    localAlarmManager.set(2, l, (PendingIntent)localObject);
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent == null) {
      return;
    }
    paramContext = ((bk)paramContext.getApplicationContext()).a();
    paramIntent = paramIntent.getAction();
    int i = -1;
    int j = paramIntent.hashCode();
    int k = -135745394;
    boolean bool1 = true;
    String str;
    boolean bool2;
    if (j != k)
    {
      k = 190343278;
      if (j != k)
      {
        k = 798292259;
        if (j == k)
        {
          str = "android.intent.action.BOOT_COMPLETED";
          bool2 = paramIntent.equals(str);
          if (bool2) {
            i = 0;
          }
        }
      }
      else
      {
        str = "com.truecaller.action.ACTION_SET_LAST_SEEN";
        bool2 = paramIntent.equals(str);
        if (bool2) {
          i = 1;
        }
      }
    }
    else
    {
      str = "com.truecaller.action.ACTION_UPDATE_PRESENCE_FOR_CURRENT_USER";
      bool2 = paramIntent.equals(str);
      if (bool2) {
        i = 2;
      }
    }
    switch (i)
    {
    default: 
      break;
    case 2: 
      new String[1][0] = "Delayed alarm, triggering a presence update";
      paramContext = (c)paramContext.ae().a();
      paramIntent = AvailabilityTrigger.RECURRING_TASK;
      paramContext.a(paramIntent, false);
      break;
    case 1: 
      ((c)paramContext.ae().a()).b();
      return;
    case 0: 
      new String[1][0] = "ACTION_BOOT_COMPLETED, triggering a presence update";
      paramContext = (c)paramContext.ae().a();
      paramIntent = AvailabilityTrigger.RECURRING_TASK;
      paramContext.a(paramIntent, bool1);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.PresenceSchedulerReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */