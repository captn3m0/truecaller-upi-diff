package com.truecaller.presence;

import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d
{
  private final g a;
  private final Provider b;
  private final Provider c;
  
  private k(g paramg, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramg;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static k a(g paramg, Provider paramProvider1, Provider paramProvider2)
  {
    k localk = new com/truecaller/presence/k;
    localk.<init>(paramg, paramProvider1, paramProvider2);
    return localk;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */