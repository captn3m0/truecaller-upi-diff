package com.truecaller.presence;

import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  private final Provider g;
  private final Provider h;
  private final Provider i;
  private final Provider j;
  private final Provider k;
  
  private p(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7, Provider paramProvider8, Provider paramProvider9, Provider paramProvider10, Provider paramProvider11)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
    e = paramProvider5;
    f = paramProvider6;
    g = paramProvider7;
    h = paramProvider8;
    i = paramProvider9;
    j = paramProvider10;
    k = paramProvider11;
  }
  
  public static p a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7, Provider paramProvider8, Provider paramProvider9, Provider paramProvider10, Provider paramProvider11)
  {
    p localp = new com/truecaller/presence/p;
    localp.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11);
    return localp;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */