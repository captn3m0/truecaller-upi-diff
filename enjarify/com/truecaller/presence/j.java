package com.truecaller.presence;

import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d
{
  private final g a;
  private final Provider b;
  
  private j(g paramg, Provider paramProvider)
  {
    a = paramg;
    b = paramProvider;
  }
  
  public static j a(g paramg, Provider paramProvider)
  {
    j localj = new com/truecaller/presence/j;
    localj.<init>(paramg, paramProvider);
    return localj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */