package com.truecaller.presence;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import java.util.Collection;

public final class d
  implements c
{
  private final v a;
  
  public d(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return c.class.equals(paramClass);
  }
  
  public final w a()
  {
    v localv = a;
    d.d locald = new com/truecaller/presence/d$d;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locald.<init>(locale, (byte)0);
    return w.a(localv, locald);
  }
  
  public final void a(AvailabilityTrigger paramAvailabilityTrigger, boolean paramBoolean)
  {
    v localv = a;
    d.c localc = new com/truecaller/presence/d$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, paramAvailabilityTrigger, paramBoolean, (byte)0);
    localv.a(localc);
  }
  
  public final void a(Collection paramCollection)
  {
    v localv = a;
    d.a locala = new com/truecaller/presence/d$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramCollection, (byte)0);
    localv.a(locala);
  }
  
  public final void b()
  {
    v localv = a;
    d.b localb = new com/truecaller/presence/d$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, (byte)0);
    localv.a(localb);
  }
  
  public final w c()
  {
    v localv = a;
    d.e locale = new com/truecaller/presence/d$e;
    e locale1 = new com/truecaller/androidactors/e;
    locale1.<init>();
    locale.<init>(locale1, (byte)0);
    return w.a(localv, locale);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */