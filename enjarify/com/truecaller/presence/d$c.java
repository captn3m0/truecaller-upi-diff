package com.truecaller.presence;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class d$c
  extends u
{
  private final AvailabilityTrigger b;
  private final boolean c;
  
  private d$c(e parame, AvailabilityTrigger paramAvailabilityTrigger, boolean paramBoolean)
  {
    super(parame);
    b = paramAvailabilityTrigger;
    c = paramBoolean;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".reportPresence(");
    Object localObject = b;
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Boolean.valueOf(c), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.d.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */