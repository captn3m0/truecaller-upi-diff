package com.truecaller.presence;

import com.truecaller.common.g.a;

public final class s
  implements r
{
  private final a a;
  
  public s(a parama)
  {
    a = parama;
  }
  
  public final long a()
  {
    a locala = a;
    long l1 = t.b();
    long l2 = locala.a("presence_interval", l1);
    l1 = t.c();
    return Math.max(l2, l1);
  }
  
  public final long b()
  {
    a locala = a;
    long l = t.a();
    return locala.a("presence_initial_delay", l);
  }
  
  public final long c()
  {
    a locala = a;
    long l = t.d();
    return locala.a("presence_stop_time", l);
  }
  
  public final long d()
  {
    a locala = a;
    long l = t.e();
    return locala.a("presence_recheck_time", l);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */