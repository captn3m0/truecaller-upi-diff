package com.truecaller.presence;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.Collection;

final class d$a
  extends u
{
  private final Collection b;
  
  private d$a(e parame, Collection paramCollection)
  {
    super(parame);
    b = paramCollection;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".getPresenceForNumbers(");
    String str = a(b, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */