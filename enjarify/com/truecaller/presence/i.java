package com.truecaller.presence;

import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d
{
  private final g a;
  private final Provider b;
  private final Provider c;
  
  private i(g paramg, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramg;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static i a(g paramg, Provider paramProvider1, Provider paramProvider2)
  {
    i locali = new com/truecaller/presence/i;
    locali.<init>(paramg, paramProvider1, paramProvider2);
    return locali;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */