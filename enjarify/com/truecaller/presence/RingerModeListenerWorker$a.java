package com.truecaller.presence;

import android.arch.lifecycle.LiveData;
import android.net.Uri;
import android.provider.Settings.Global;
import androidx.work.c;
import androidx.work.c.a;
import androidx.work.g;
import androidx.work.j;
import androidx.work.k.a;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public final class RingerModeListenerWorker$a
{
  public static void a()
  {
    new String[1][0] = "Enqueuing ringer mode listener worker.";
    androidx.work.p localp = androidx.work.p.a();
    c.g.b.k.a(localp, "WorkManager.getInstance()");
    Object localObject1 = new androidx/work/c$a;
    ((c.a)localObject1).<init>();
    Object localObject2 = j.b;
    localObject1 = ((c.a)localObject1).a((j)localObject2);
    localObject2 = TimeUnit.MILLISECONDS;
    localObject1 = ((c.a)localObject1).b((TimeUnit)localObject2);
    localObject2 = TimeUnit.MILLISECONDS;
    localObject1 = ((c.a)localObject1).a((TimeUnit)localObject2);
    localObject2 = Settings.Global.getUriFor("mode_ringer");
    localObject1 = ((c.a)localObject1).a((Uri)localObject2).a();
    c.g.b.k.a(localObject1, "Constraints.Builder()\n  …\n                .build()");
    localObject2 = new androidx/work/k$a;
    ((k.a)localObject2).<init>(RingerModeListenerWorker.class);
    localObject1 = ((k.a)((k.a)localObject2).a((c)localObject1)).c();
    c.g.b.k.a(localObject1, "OneTimeWorkRequest.Build…\n                .build()");
    localObject1 = (androidx.work.k)localObject1;
    localObject2 = ((androidx.work.k)localObject1).a();
    localObject2 = localp.a((UUID)localObject2);
    Object localObject3 = (android.arch.lifecycle.p)RingerModeListenerWorker.a.a.a;
    ((LiveData)localObject2).a((android.arch.lifecycle.p)localObject3);
    localObject3 = g.a;
    localp.a("com.truecaller.presence.RingerModeListenerWorker", (g)localObject3, (androidx.work.k)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.RingerModeListenerWorker.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */