package com.truecaller.presence;

public enum AvailabilityTrigger
{
  static
  {
    Object localObject = new com/truecaller/presence/AvailabilityTrigger;
    ((AvailabilityTrigger)localObject).<init>("RECURRING_TASK", 0);
    RECURRING_TASK = (AvailabilityTrigger)localObject;
    localObject = new com/truecaller/presence/AvailabilityTrigger;
    int i = 1;
    ((AvailabilityTrigger)localObject).<init>("USER_ACTION", i);
    USER_ACTION = (AvailabilityTrigger)localObject;
    localObject = new AvailabilityTrigger[2];
    AvailabilityTrigger localAvailabilityTrigger = RECURRING_TASK;
    localObject[0] = localAvailabilityTrigger;
    localAvailabilityTrigger = USER_ACTION;
    localObject[i] = localAvailabilityTrigger;
    $VALUES = (AvailabilityTrigger[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.AvailabilityTrigger
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */