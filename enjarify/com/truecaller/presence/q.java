package com.truecaller.presence;

import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.Availability.Context;
import com.truecaller.api.services.presence.v1.models.Availability.Status;
import com.truecaller.api.services.presence.v1.models.Availability.a;
import com.truecaller.api.services.presence.v1.models.b;
import com.truecaller.api.services.presence.v1.models.b.a;
import com.truecaller.api.services.presence.v1.models.d;
import com.truecaller.api.services.presence.v1.models.d.a;
import com.truecaller.api.services.presence.v1.models.f;
import com.truecaller.api.services.presence.v1.models.f.a;
import com.truecaller.api.services.presence.v1.models.i;
import com.truecaller.api.services.presence.v1.models.i.a;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import org.c.a.a.a.k;

public final class q
{
  private static a a;
  
  static Availability a(String paramString)
  {
    boolean bool = k.b(paramString);
    if (bool) {
      return null;
    }
    Availability.a locala = Availability.c();
    Object localObject1 = paramString.split(",");
    int i = localObject1.length;
    int j = 1;
    Object localObject2 = null;
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject3 = null;
    }
    String[] arrayOfString = new String[0];
    AssertionUtil.OnlyInDebug.isTrue(i, arrayOfString);
    Object localObject3 = localObject1[0];
    localObject2 = AvailabilityStatus.UNKNOWN;
    localObject3 = AvailabilityStatus.fromString((String)localObject3, (AvailabilityStatus)localObject2).toGrpcStatus();
    locala.a((Availability.Status)localObject3);
    localObject2 = Availability.Context.NOTSET;
    int k = localObject1.length;
    if (k > j)
    {
      localObject1 = localObject1[j];
      AvailabilityContext localAvailabilityContext = AvailabilityContext.UNKNOWN;
      localObject1 = AvailabilityContext.fromString((String)localObject1, localAvailabilityContext);
      localObject2 = ((AvailabilityContext)localObject1).toGrpcContext();
    }
    locala.a((Availability.Context)localObject2);
    localObject1 = Availability.Status.BUSY;
    if (localObject3 == localObject1)
    {
      localObject1 = Availability.Context.UNRECOGNIZED;
      if (localObject2 != localObject1)
      {
        localObject1 = Availability.Context.NOTSET;
        if (localObject2 != localObject1) {}
      }
      else
      {
        paramString = String.valueOf(paramString);
        AssertionUtil.reportWeirdnessButNeverCrash("Invalid availability string, BUSY state requires a Reason: ".concat(paramString));
        return null;
      }
    }
    return (Availability)locala.build();
  }
  
  public static a a()
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/presence/a$a;
      ((a.a)localObject1).<init>("");
      Object localObject2 = Availability.c();
      Availability.Status localStatus = Availability.Status.UNKNOWN;
      localObject2 = (Availability)((Availability.a)localObject2).a(localStatus).build();
      a = ((Availability)localObject2);
      localObject2 = b.c();
      localStatus = null;
      localObject2 = (b)((b.a)localObject2).a(false).build();
      b = ((b)localObject2);
      localObject2 = i.c();
      boolean bool = true;
      localObject2 = (i)((i.a)localObject2).a(bool).build();
      e = ((i)localObject2);
      localObject2 = (d)d.a().a(bool).build();
      d = ((d)localObject2);
      localObject2 = (f)f.d().a(false).build();
      f = ((f)localObject2);
      localObject1 = ((a.a)localObject1).a();
      a = (a)localObject1;
    }
    return a;
  }
  
  public static a a(a parama1, a parama2)
  {
    Object localObject = b;
    Availability localAvailability = b;
    if (localAvailability != null)
    {
      Availability.Status localStatus1 = localAvailability.a();
      Availability.Status localStatus2 = Availability.Status.BUSY;
      if (localStatus1 != localStatus2)
      {
        localStatus1 = localAvailability.a();
        localStatus2 = Availability.Status.AVAILABLE;
        if (localStatus1 != localStatus2) {}
      }
      else
      {
        if (localObject != null)
        {
          localObject = ((Availability)localObject).a();
          localStatus1 = Availability.Status.BUSY;
          if (localObject == localStatus1) {
            return parama1;
          }
        }
        parama1 = localAvailability.a();
        localObject = Availability.Status.BUSY;
        if (parama1 == localObject) {
          return parama2;
        }
        return parama2;
      }
    }
    return parama1;
  }
  
  static String a(Availability paramAvailability)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = AvailabilityStatus.fromGrpsStatus(paramAvailability.a()).name();
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    paramAvailability = AvailabilityContext.fromGrpcContext(paramAvailability.b()).name();
    localStringBuilder.append(paramAvailability);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */