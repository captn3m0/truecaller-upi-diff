package com.truecaller.presence;

import android.content.Context;
import c.a.ag;
import c.a.y;
import c.g.b.k;
import c.m.l;
import c.n.m;
import com.google.f.am;
import com.truecaller.api.services.presence.v1.c;
import com.truecaller.api.services.presence.v1.c.c;
import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.Availability.Context;
import com.truecaller.api.services.presence.v1.models.Availability.Status;
import com.truecaller.api.services.presence.v1.models.d;
import com.truecaller.api.services.presence.v1.models.f;
import com.truecaller.common.h.j;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import org.a.a.a.g;

public final class a
  implements Serializable
{
  public static final a.b h;
  public final String a;
  public final Availability b;
  public final com.truecaller.api.services.presence.v1.models.b c;
  public final org.a.a.b d;
  public final d e;
  public final com.truecaller.api.services.presence.v1.models.i f;
  final transient org.a.a.b g;
  private final f i;
  
  static
  {
    a.b localb = new com/truecaller/presence/a$b;
    localb.<init>((byte)0);
    h = localb;
  }
  
  private a(a.a parama)
  {
    Object localObject = g;
    a = ((String)localObject);
    localObject = a;
    b = ((Availability)localObject);
    localObject = b;
    c = ((com.truecaller.api.services.presence.v1.models.b)localObject);
    localObject = c;
    d = ((org.a.a.b)localObject);
    localObject = org.a.a.b.ay_();
    k.a(localObject, "DateTime.now()");
    g = ((org.a.a.b)localObject);
    localObject = d;
    e = ((d)localObject);
    localObject = e;
    f = ((com.truecaller.api.services.presence.v1.models.i)localObject);
    parama = f;
    i = parama;
  }
  
  public static final Collection a(c paramc, boolean paramBoolean)
  {
    if (paramc == null) {
      return (Collection)y.a;
    }
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (List)localObject1;
    paramc = paramc.a();
    if (paramc != null)
    {
      paramc = ag.c(paramc);
      if (paramc != null)
      {
        Object localObject2 = (c.g.a.b)a.b.a.a;
        paramc = l.a(paramc, (c.g.a.b)localObject2).a();
        for (;;)
        {
          boolean bool1 = paramc.hasNext();
          if (!bool1) {
            break;
          }
          localObject2 = paramc.next();
          Object localObject3 = localObject1;
          localObject3 = (Collection)localObject1;
          localObject2 = (Map.Entry)localObject2;
          Object localObject4 = (String)((Map.Entry)localObject2).getKey();
          localObject2 = (c.c)((Map.Entry)localObject2).getValue();
          a.a locala = new com/truecaller/presence/a$a;
          k.a(localObject4, "phoneNumber");
          locala.<init>((String)localObject4);
          boolean bool2 = ((c.c)localObject2).a();
          f localf = null;
          if (bool2)
          {
            k.a(localObject2, "presenceData");
            localObject4 = ((c.c)localObject2).b();
          }
          else
          {
            bool2 = false;
            localObject4 = null;
          }
          a = ((Availability)localObject4);
          bool2 = ((c.c)localObject2).c();
          if (bool2)
          {
            k.a(localObject2, "presenceData");
            localObject4 = ((c.c)localObject2).d();
            String str = "presenceData.lastSeen";
            k.a(localObject4, str);
            localObject4 = j.a(((am)localObject4).getValue());
          }
          else
          {
            bool2 = false;
            localObject4 = null;
          }
          c = ((org.a.a.b)localObject4);
          if (paramBoolean)
          {
            bool2 = ((c.c)localObject2).e();
            if (bool2)
            {
              k.a(localObject2, "presenceData");
              localObject4 = ((c.c)localObject2).f();
            }
            else
            {
              bool2 = false;
              localObject4 = null;
            }
            b = ((com.truecaller.api.services.presence.v1.models.b)localObject4);
            k.a(localObject2, "presenceData");
            localObject4 = ((c.c)localObject2).i();
            e = ((com.truecaller.api.services.presence.v1.models.i)localObject4);
            bool2 = ((c.c)localObject2).g();
            if (bool2)
            {
              localObject4 = ((c.c)localObject2).h();
            }
            else
            {
              bool2 = false;
              localObject4 = null;
            }
            d = ((d)localObject4);
            bool2 = ((c.c)localObject2).j();
            if (bool2) {
              localf = ((c.c)localObject2).k();
            }
            f = localf;
          }
          localObject2 = locala.a();
          ((Collection)localObject3).add(localObject2);
        }
      }
    }
    return (Collection)localObject1;
  }
  
  public final String a(Context paramContext, boolean paramBoolean)
  {
    k.b(paramContext, "context");
    Object localObject1 = b;
    if (localObject1 != null)
    {
      localObject1 = ((Availability)localObject1).a();
      if (localObject1 != null)
      {
        localObject1 = b.a();
        if (localObject1 != null)
        {
          int[] arrayOfInt = b.b;
          int j = ((Availability.Status)localObject1).ordinal();
          j = arrayOfInt[j];
          arrayOfInt = null;
          int k = 1;
          int m = 2131887460;
          Object localObject3;
          long l;
          switch (j)
          {
          default: 
            break;
          case 2: 
            localObject1 = b.b();
            if (localObject1 != null)
            {
              Object localObject2 = b.a;
              j = ((Availability.Context)localObject1).ordinal();
              j = localObject2[j];
              switch (j)
              {
              default: 
                break;
              case 2: 
                localObject1 = new java/lang/StringBuilder;
                ((StringBuilder)localObject1).<init>();
                int n = 2131887463;
                localObject2 = paramContext.getString(n);
                ((StringBuilder)localObject1).append((String)localObject2);
                if (paramBoolean)
                {
                  localObject3 = d;
                  if (localObject3 != null)
                  {
                    ((StringBuilder)localObject1).append(". ");
                    localObject3 = new Object[k];
                    l = d.a;
                    String str = j.a(paramContext, l);
                    localObject3[0] = str;
                    paramContext = paramContext.getString(m, (Object[])localObject3);
                    ((StringBuilder)localObject1).append(paramContext);
                  }
                }
                paramContext = ((StringBuilder)localObject1).toString();
                break;
              case 1: 
                paramContext = paramContext.getString(2131887462);
                k.a(paramContext, "context.getString(R.string.availability_busy_call)");
                return paramContext;
              }
            }
            paramBoolean = 2131887461;
            paramContext = paramContext.getString(paramBoolean);
            k.a(paramContext, "when (availability.conte…ility_busy)\n            }");
            return paramContext;
          case 1: 
            if (paramBoolean)
            {
              localObject3 = d;
              if (localObject3 != null)
              {
                localObject1 = new Object[k];
                l = a;
                localObject3 = j.a(paramContext, l);
                localObject1[0] = localObject3;
                paramContext = paramContext.getString(m, (Object[])localObject1);
                break label359;
              }
            }
            paramBoolean = 2131887459;
            paramContext = paramContext.getString(paramBoolean);
            label359:
            k.a(paramContext, "if (showLastSeen && last…g.availability_available)");
            return paramContext;
          }
        }
        return "";
      }
    }
    return "";
  }
  
  public final boolean a()
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = ((Availability)localObject).a();
      Availability.Status localStatus = Availability.Status.AVAILABLE;
      if (localObject != localStatus)
      {
        localObject = b.a();
        localStatus = Availability.Status.BUSY;
        if (localObject != localStatus) {}
      }
      else
      {
        return true;
      }
    }
    return false;
  }
  
  public final a.a b()
  {
    a.a locala = new com/truecaller/presence/a$a;
    locala.<init>(this);
    return locala;
  }
  
  public final boolean equals(Object paramObject)
  {
    Object localObject = this;
    localObject = (a)this;
    boolean bool1 = true;
    if (localObject == paramObject) {
      return bool1;
    }
    if (paramObject != null)
    {
      localObject = getClass();
      Class localClass = paramObject.getClass();
      boolean bool2 = k.a(localObject, localClass) ^ bool1;
      if (!bool2)
      {
        bool2 = paramObject instanceof a;
        bool1 = false;
        String str = null;
        if (!bool2) {
          paramObject = null;
        }
        paramObject = (a)paramObject;
        localObject = a;
        if (paramObject != null) {
          str = a;
        }
        return k.a(localObject, str);
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    return a.hashCode();
  }
  
  public final String toString()
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("Presence{\nNumber=");
    Object localObject2 = a;
    if (localObject2 != null) {
      localObject2 = "null";
    } else {
      localObject2 = "<non-null number>";
    }
    ((StringBuilder)localObject1).append((String)localObject2);
    k.a(localObject1, "append(value)");
    m.a((StringBuilder)localObject1);
    ((StringBuilder)localObject1).append("Availability");
    localObject2 = b;
    if (localObject2 == null)
    {
      ((StringBuilder)localObject1).append("=null");
      localObject2 = "append(value)";
      k.a(localObject1, (String)localObject2);
      m.a((StringBuilder)localObject1);
    }
    else
    {
      ((StringBuilder)localObject1).append(".Status=");
      k.a(localObject1, "append(\".Status=\")");
      localObject2 = b.a().name();
      ((StringBuilder)localObject1).append((String)localObject2);
      k.a(localObject1, "append(value)");
      m.a((StringBuilder)localObject1);
      ((StringBuilder)localObject1).append("Availability.Context=");
      k.a(localObject1, "append(\"Availability.Context=\")");
      localObject2 = b.b().name();
      ((StringBuilder)localObject1).append((String)localObject2);
      localObject2 = "append(value)";
      k.a(localObject1, (String)localObject2);
      m.a((StringBuilder)localObject1);
    }
    localObject2 = c;
    boolean bool1;
    if (localObject2 != null)
    {
      ((StringBuilder)localObject1).append("Flash.isEnabled=");
      k.a(localObject1, "append(\"Flash.isEnabled=\")");
      bool1 = c.a();
      ((StringBuilder)localObject1).append(bool1);
      k.a(localObject1, "append(value)");
      m.a((StringBuilder)localObject1);
      ((StringBuilder)localObject1).append("Flash.version=");
      k.a(localObject1, "append(\"Flash.version=\")");
      localObject2 = c.b();
      ((StringBuilder)localObject1).append(localObject2);
      localObject2 = "append(value)";
      k.a(localObject1, (String)localObject2);
      m.a((StringBuilder)localObject1);
    }
    localObject2 = d;
    if (localObject2 != null)
    {
      ((StringBuilder)localObject1).append("LastSeen=");
      k.a(localObject1, "append(\"LastSeen=\")");
      long l1 = d.a;
      ((StringBuilder)localObject1).append(l1);
      localObject2 = "append(value)";
      k.a(localObject1, (String)localObject2);
      m.a((StringBuilder)localObject1);
    }
    localObject2 = f;
    if (localObject2 != null)
    {
      ((StringBuilder)localObject1).append("VoIP.isDisabled=");
      k.a(localObject1, "append(\"VoIP.isDisabled=\")");
      bool1 = f.a();
      ((StringBuilder)localObject1).append(bool1);
      k.a(localObject1, "append(value)");
      m.a((StringBuilder)localObject1);
      ((StringBuilder)localObject1).append("VoIP.version=");
      k.a(localObject1, "append(\"VoIP.version=\")");
      int j = f.b();
      ((StringBuilder)localObject1).append(j);
      localObject2 = "append(value)";
      k.a(localObject1, (String)localObject2);
      m.a((StringBuilder)localObject1);
    }
    localObject2 = i;
    if (localObject2 != null)
    {
      ((StringBuilder)localObject1).append("Payment.isEnabled=");
      k.a(localObject1, "append(\"Payment.isEnabled=\")");
      boolean bool2 = i.a();
      ((StringBuilder)localObject1).append(bool2);
      k.a(localObject1, "append(value)");
      m.a((StringBuilder)localObject1);
      ((StringBuilder)localObject1).append("Payment.lastTxnTimeSeconds=");
      k.a(localObject1, "append(\"Payment.lastTxnTimeSeconds=\")");
      int k = i.c();
      ((StringBuilder)localObject1).append(k);
      k.a(localObject1, "append(value)");
      m.a((StringBuilder)localObject1);
      ((StringBuilder)localObject1).append("Payment.version=");
      k.a(localObject1, "append(\"Payment.version=\")");
      k = i.b();
      ((StringBuilder)localObject1).append(k);
      localObject2 = "append(value)";
      k.a(localObject1, (String)localObject2);
      m.a((StringBuilder)localObject1);
    }
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("CheckTime=");
    long l2 = g.a;
    ((StringBuilder)localObject2).append(l2);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((StringBuilder)localObject1).append((String)localObject2);
    k.a(localObject1, "append(value)");
    m.a((StringBuilder)localObject1);
    ((StringBuilder)localObject1).append("}");
    localObject1 = ((StringBuilder)localObject1).toString();
    k.a(localObject1, "toString()");
    k.a(localObject1, "with(StringBuilder(\"Pres…\n        toString()\n    }");
    return (String)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */