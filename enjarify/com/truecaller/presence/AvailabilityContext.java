package com.truecaller.presence;

import com.truecaller.api.services.presence.v1.models.Availability.Context;

 enum AvailabilityContext
{
  private final Availability.Context mGrpcContext;
  
  static
  {
    Object localObject = new com/truecaller/presence/AvailabilityContext;
    Availability.Context localContext = Availability.Context.CALL;
    ((AvailabilityContext)localObject).<init>("CALL", 0, localContext);
    CALL = (AvailabilityContext)localObject;
    localObject = new com/truecaller/presence/AvailabilityContext;
    localContext = Availability.Context.MEETING;
    int i = 1;
    ((AvailabilityContext)localObject).<init>("MEETING", i, localContext);
    MEETING = (AvailabilityContext)localObject;
    localObject = new com/truecaller/presence/AvailabilityContext;
    localContext = Availability.Context.SLEEP;
    int j = 2;
    ((AvailabilityContext)localObject).<init>("SLEEP", j, localContext);
    SLEEP = (AvailabilityContext)localObject;
    localObject = new com/truecaller/presence/AvailabilityContext;
    localContext = Availability.Context.NOTSET;
    int k = 3;
    ((AvailabilityContext)localObject).<init>("UNKNOWN", k, localContext);
    UNKNOWN = (AvailabilityContext)localObject;
    localObject = new AvailabilityContext[4];
    AvailabilityContext localAvailabilityContext = CALL;
    localObject[0] = localAvailabilityContext;
    localAvailabilityContext = MEETING;
    localObject[i] = localAvailabilityContext;
    localAvailabilityContext = SLEEP;
    localObject[j] = localAvailabilityContext;
    localAvailabilityContext = UNKNOWN;
    localObject[k] = localAvailabilityContext;
    $VALUES = (AvailabilityContext[])localObject;
  }
  
  private AvailabilityContext(Availability.Context paramContext)
  {
    mGrpcContext = paramContext;
  }
  
  static AvailabilityContext fromGrpcContext(Availability.Context paramContext)
  {
    if (paramContext != null)
    {
      int[] arrayOfInt = AvailabilityContext.1.a;
      int i = paramContext.ordinal();
      i = arrayOfInt[i];
      switch (i)
      {
      default: 
        break;
      case 4: 
      case 5: 
        return UNKNOWN;
      case 3: 
        return SLEEP;
      case 2: 
        return MEETING;
      case 1: 
        return CALL;
      }
    }
    return UNKNOWN;
  }
  
  static AvailabilityContext fromString(String paramString, AvailabilityContext paramAvailabilityContext)
  {
    if (paramString == null) {
      return paramAvailabilityContext;
    }
    int i = -1;
    int j = paramString.hashCode();
    int k = 2060894;
    String str;
    boolean bool;
    if (j != k)
    {
      k = 78984887;
      if (j != k)
      {
        k = 433141802;
        if (j != k)
        {
          k = 1660016155;
          if (j == k)
          {
            str = "MEETING";
            bool = paramString.equals(str);
            if (bool) {
              i = 1;
            }
          }
        }
        else
        {
          str = "UNKNOWN";
          bool = paramString.equals(str);
          if (bool) {
            i = 3;
          }
        }
      }
      else
      {
        str = "SLEEP";
        bool = paramString.equals(str);
        if (bool) {
          i = 2;
        }
      }
    }
    else
    {
      str = "CALL";
      bool = paramString.equals(str);
      if (bool) {
        i = 0;
      }
    }
    switch (i)
    {
    default: 
      return paramAvailabilityContext;
    case 3: 
      return UNKNOWN;
    case 2: 
      return SLEEP;
    case 1: 
      return MEETING;
    }
    return CALL;
  }
  
  final Availability.Context toGrpcContext()
  {
    return mGrpcContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.AvailabilityContext
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */