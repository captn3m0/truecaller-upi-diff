package com.truecaller.presence;

import com.truecaller.androidactors.w;
import java.util.Collection;

public abstract interface c
{
  public abstract w a();
  
  public abstract void a(AvailabilityTrigger paramAvailabilityTrigger, boolean paramBoolean);
  
  public abstract void a(Collection paramCollection);
  
  public abstract void b();
  
  public abstract w c();
}

/* Location:
 * Qualified Name:     com.truecaller.presence.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */