package com.truecaller.presence;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.common.background.TrackedWorker;
import com.truecaller.log.AssertionUtil;

public final class SendPresenceSettingWorker
  extends TrackedWorker
{
  public static final SendPresenceSettingWorker.a e;
  public r b;
  public f c;
  public b d;
  
  static
  {
    SendPresenceSettingWorker.a locala = new com/truecaller/presence/SendPresenceSettingWorker$a;
    locala.<init>((byte)0);
    e = locala;
  }
  
  public SendPresenceSettingWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final b b()
  {
    b localb = d;
    if (localb == null)
    {
      String str = "analytics";
      k.a(str);
    }
    return localb;
  }
  
  public final boolean c()
  {
    r localr = b;
    if (localr == null)
    {
      String str = "accountManager";
      k.a(str);
    }
    return localr.c();
  }
  
  public final ListenableWorker.a d()
  {
    Object localObject1;
    try
    {
      localObject1 = c;
      if (localObject1 == null)
      {
        localObject2 = "presenceManager";
        k.a((String)localObject2);
      }
      localObject1 = ((f)localObject1).a();
      localObject1 = (c)localObject1;
      localObject1 = ((c)localObject1).a();
      localObject1 = ((w)localObject1).d();
      localObject1 = (Boolean)localObject1;
      Object localObject2 = Boolean.TRUE;
      boolean bool = k.a(localObject1, localObject2);
      if (bool)
      {
        localObject1 = ListenableWorker.a.a();
        localObject2 = "Result.success()";
        k.a(localObject1, (String)localObject2);
        return (ListenableWorker.a)localObject1;
      }
    }
    catch (InterruptedException localInterruptedException)
    {
      localObject1 = (Throwable)localInterruptedException;
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject1);
      localObject1 = ListenableWorker.a.b();
      k.a(localObject1, "Result.retry()");
    }
    return (ListenableWorker.a)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.SendPresenceSettingWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */