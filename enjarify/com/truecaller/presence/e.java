package com.truecaller.presence;

import com.google.f.am;
import com.google.f.am.a;
import com.google.f.r.a;
import com.google.f.t;
import com.google.f.t.a;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.presence.v1.h.a;
import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.Availability.Context;
import com.truecaller.api.services.presence.v1.models.Availability.Status;
import com.truecaller.api.services.presence.v1.models.Availability.a;
import com.truecaller.api.services.presence.v1.models.b.a;
import com.truecaller.api.services.presence.v1.models.d.a;
import com.truecaller.api.services.presence.v1.models.i.a;
import com.truecaller.common.h.u;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.transport.im.bp;
import com.truecaller.search.local.model.g;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.utils.PaymentPresence;
import com.truecaller.util.af;
import com.truecaller.util.al;
import io.grpc.ap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class e
  implements c
{
  private final com.truecaller.common.account.r a;
  private final n b;
  private final com.truecaller.common.g.a c;
  private final l d;
  private final al e;
  private final af f;
  private final com.truecaller.utils.i g;
  private final dagger.a h;
  private final dagger.a i;
  private final dagger.a j;
  private final r k;
  private final com.truecaller.androidactors.f l;
  private final bw m;
  private final com.truecaller.messaging.h n;
  private final com.truecaller.voip.d o;
  private final dagger.a p;
  
  e(com.truecaller.common.account.r paramr, n paramn, com.truecaller.common.g.a parama, l paraml, al paramal, com.truecaller.utils.i parami, dagger.a parama1, dagger.a parama2, dagger.a parama3, af paramaf, r paramr1, com.truecaller.androidactors.f paramf, bw parambw, com.truecaller.messaging.h paramh, com.truecaller.voip.d paramd, dagger.a parama4)
  {
    a = paramr;
    b = paramn;
    c = parama;
    d = paraml;
    e = paramal;
    g = parami;
    h = parama1;
    i = parama2;
    j = parama3;
    f = paramaf;
    k = paramr1;
    l = paramf;
    m = parambw;
    n = paramh;
    o = paramd;
    p = parama4;
  }
  
  private long a(String paramString)
  {
    com.truecaller.common.g.a locala = c;
    long l1 = 0L;
    long l2 = locala.a(paramString, l1);
    long l3 = System.currentTimeMillis();
    boolean bool = l2 < l3;
    if (bool) {
      return l1;
    }
    return l2;
  }
  
  private com.truecaller.api.services.presence.v1.h a(AvailabilityTrigger paramAvailabilityTrigger, Availability paramAvailability, boolean paramBoolean)
  {
    paramAvailability = com.truecaller.api.services.presence.v1.h.a().a(paramAvailability);
    Object localObject = e();
    paramAvailability = paramAvailability.a((com.truecaller.api.services.presence.v1.models.b)localObject);
    localObject = f();
    paramAvailability = paramAvailability.a((com.truecaller.api.services.presence.v1.models.i)localObject);
    localObject = am.newBuilder();
    paramAvailabilityTrigger = paramAvailabilityTrigger.name();
    paramAvailabilityTrigger = ((am.a)localObject).setValue(paramAvailabilityTrigger);
    paramAvailabilityTrigger = paramAvailability.a(paramAvailabilityTrigger);
    paramAvailability = g();
    paramAvailabilityTrigger = paramAvailabilityTrigger.a(paramAvailability);
    paramAvailability = h();
    return (com.truecaller.api.services.presence.v1.h)paramAvailabilityTrigger.a(paramAvailability).a(paramBoolean).build();
  }
  
  private void a(Availability paramAvailability)
  {
    com.truecaller.common.g.a locala = c;
    paramAvailability = q.a(paramAvailability);
    locala.a("last_availability_update_success", paramAvailability);
    paramAvailability = c;
    long l1 = System.currentTimeMillis();
    paramAvailability.b("last_successful_availability_update_time", l1);
  }
  
  private static void a(u paramu, Collection paramCollection)
  {
    paramCollection = paramCollection.iterator();
    for (;;)
    {
      boolean bool = paramCollection.hasNext();
      if (!bool) {
        break;
      }
      String str = (String)paramCollection.next();
      int i1 = paramu.d(str);
      int i2 = 2;
      if (i1 != i2) {
        paramCollection.remove();
      }
    }
  }
  
  /* Error */
  private void a(com.truecaller.common.network.e parame, Collection paramCollection)
  {
    // Byte code:
    //   0: aload_2
    //   1: ifnonnull +4 -> 5
    //   4: return
    //   5: aload_0
    //   6: getfield 55	com/truecaller/presence/e:i	Ldagger/a;
    //   9: invokeinterface 191 1 0
    //   14: checkcast 179	com/truecaller/common/h/u
    //   17: astore_3
    //   18: aload_3
    //   19: aload_2
    //   20: invokestatic 194	com/truecaller/presence/e:a	(Lcom/truecaller/common/h/u;Ljava/util/Collection;)V
    //   23: aload_2
    //   24: invokeinterface 197 1 0
    //   29: istore 4
    //   31: iload 4
    //   33: ifeq +4 -> 37
    //   36: return
    //   37: aload_0
    //   38: aload_2
    //   39: invokespecial 200	com/truecaller/presence/e:c	(Ljava/util/Collection;)V
    //   42: aload_2
    //   43: invokeinterface 197 1 0
    //   48: istore 4
    //   50: iload 4
    //   52: ifeq +4 -> 56
    //   55: return
    //   56: aload_2
    //   57: invokeinterface 165 1 0
    //   62: astore_2
    //   63: new 202	java/util/ArrayList
    //   66: astore_3
    //   67: bipush 50
    //   69: istore 5
    //   71: aload_3
    //   72: iload 5
    //   74: invokespecial 206	java/util/ArrayList:<init>	(I)V
    //   77: aload_2
    //   78: invokeinterface 171 1 0
    //   83: istore 6
    //   85: iload 6
    //   87: ifeq +235 -> 322
    //   90: iconst_0
    //   91: istore 6
    //   93: aconst_null
    //   94: astore 7
    //   96: iload 6
    //   98: iload 5
    //   100: if_icmpge +42 -> 142
    //   103: aload_2
    //   104: invokeinterface 171 1 0
    //   109: istore 8
    //   111: iload 8
    //   113: ifeq +29 -> 142
    //   116: aload_2
    //   117: invokeinterface 175 1 0
    //   122: astore 9
    //   124: aload_3
    //   125: aload 9
    //   127: invokeinterface 212 2 0
    //   132: pop
    //   133: iload 6
    //   135: iconst_1
    //   136: iadd
    //   137: istore 6
    //   139: goto -43 -> 96
    //   142: aload_3
    //   143: invokeinterface 213 1 0
    //   148: istore 6
    //   150: iload 6
    //   152: ifne +170 -> 322
    //   155: invokestatic 218	com/truecaller/api/services/presence/v1/a:a	()Lcom/truecaller/api/services/presence/v1/a$a;
    //   158: aload_3
    //   159: invokevirtual 223	com/truecaller/api/services/presence/v1/a$a:a	(Ljava/lang/Iterable;)Lcom/truecaller/api/services/presence/v1/a$a;
    //   162: invokevirtual 224	com/truecaller/api/services/presence/v1/a$a:build	()Lcom/google/f/q;
    //   165: checkcast 215	com/truecaller/api/services/presence/v1/a
    //   168: astore 7
    //   170: aload_0
    //   171: getfield 43	com/truecaller/presence/e:b	Lcom/truecaller/presence/n;
    //   174: astore 9
    //   176: aload 9
    //   178: aload_1
    //   179: invokeinterface 229 2 0
    //   184: astore 9
    //   186: aload 9
    //   188: checkcast 231	com/truecaller/api/services/presence/v1/e$a
    //   191: astore 9
    //   193: aload 9
    //   195: ifnonnull +12 -> 207
    //   198: aload_3
    //   199: invokeinterface 234 1 0
    //   204: goto -127 -> 77
    //   207: aload 9
    //   209: aload 7
    //   211: invokevirtual 237	com/truecaller/api/services/presence/v1/e$a:a	(Lcom/truecaller/api/services/presence/v1/a;)Lcom/truecaller/api/services/presence/v1/c;
    //   214: astore 7
    //   216: aload 7
    //   218: ifnull +61 -> 279
    //   221: aload_1
    //   222: instanceof 239
    //   225: istore 8
    //   227: aload 7
    //   229: iload 8
    //   231: invokestatic 244	com/truecaller/presence/a:a	(Lcom/truecaller/api/services/presence/v1/c;Z)Ljava/util/Collection;
    //   234: astore 7
    //   236: iload 8
    //   238: ifeq +9 -> 247
    //   241: aload_0
    //   242: aload 7
    //   244: invokespecial 246	com/truecaller/presence/e:b	(Ljava/util/Collection;)V
    //   247: aload_0
    //   248: getfield 53	com/truecaller/presence/e:h	Ldagger/a;
    //   251: astore 9
    //   253: aload 9
    //   255: invokeinterface 191 1 0
    //   260: astore 9
    //   262: aload 9
    //   264: checkcast 248	com/truecaller/search/local/model/g
    //   267: astore 9
    //   269: aload 9
    //   271: aload 7
    //   273: invokevirtual 250	com/truecaller/search/local/model/g:a	(Ljava/util/Collection;)V
    //   276: goto +29 -> 305
    //   279: ldc -4
    //   281: astore 7
    //   283: iconst_1
    //   284: anewarray 177	java/lang/String
    //   287: iconst_0
    //   288: aload 7
    //   290: aastore
    //   291: goto +14 -> 305
    //   294: astore_1
    //   295: goto +19 -> 314
    //   298: astore 7
    //   300: aload 7
    //   302: invokestatic 258	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   305: aload_3
    //   306: invokeinterface 234 1 0
    //   311: goto -234 -> 77
    //   314: aload_3
    //   315: invokeinterface 234 1 0
    //   320: aload_1
    //   321: athrow
    //   322: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	323	0	this	e
    //   0	323	1	parame	com.truecaller.common.network.e
    //   0	323	2	paramCollection	Collection
    //   17	298	3	localObject1	Object
    //   29	22	4	bool1	boolean
    //   69	32	5	i1	int
    //   83	54	6	i2	int
    //   137	1	6	i3	int
    //   148	3	6	bool2	boolean
    //   94	195	7	localObject2	Object
    //   298	3	7	localRuntimeException	RuntimeException
    //   109	128	8	bool3	boolean
    //   122	148	9	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   170	174	294	finally
    //   178	184	294	finally
    //   186	191	294	finally
    //   209	214	294	finally
    //   229	234	294	finally
    //   242	247	294	finally
    //   247	251	294	finally
    //   253	260	294	finally
    //   262	267	294	finally
    //   271	276	294	finally
    //   283	291	294	finally
    //   300	305	294	finally
    //   170	174	298	java/lang/RuntimeException
    //   178	184	298	java/lang/RuntimeException
    //   186	191	298	java/lang/RuntimeException
    //   209	214	298	java/lang/RuntimeException
    //   229	234	298	java/lang/RuntimeException
    //   242	247	298	java/lang/RuntimeException
    //   247	251	298	java/lang/RuntimeException
    //   253	260	298	java/lang/RuntimeException
    //   262	267	298	java/lang/RuntimeException
    //   271	276	298	java/lang/RuntimeException
    //   283	291	298	java/lang/RuntimeException
  }
  
  private boolean a(a parama)
  {
    parama = g;
    long l1 = k.d();
    parama = parama.b(l1);
    l1 = org.a.a.e.a();
    return parama.d(l1);
  }
  
  private static int b(Availability paramAvailability)
  {
    int[] arrayOfInt = e.1.b;
    Availability.Status localStatus = paramAvailability.a();
    int i1 = localStatus.ordinal();
    int i2 = arrayOfInt[i1];
    switch (i2)
    {
    default: 
      break;
    case 2: 
      arrayOfInt = e.1.a;
      paramAvailability = paramAvailability.b();
      int i3 = paramAvailability.ordinal();
      i3 = arrayOfInt[i3];
      switch (i3)
      {
      default: 
        break;
      case 2: 
        return 7200000;
      case 1: 
        return 600000;
      }
      break;
    case 1: 
      return 432000000;
    }
    return -1 << -1;
  }
  
  private void b(Collection paramCollection)
  {
    Object localObject1 = m;
    boolean bool1 = ((bw)localObject1).a();
    if (!bool1) {
      return;
    }
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    paramCollection = paramCollection.iterator();
    boolean bool2;
    Object localObject2;
    for (;;)
    {
      bool2 = paramCollection.hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = (a)paramCollection.next();
      com.truecaller.api.services.presence.v1.models.d locald = e;
      if (locald != null)
      {
        boolean bool3 = a;
        if (!bool3)
        {
          localObject2 = a;
          ((List)localObject1).add(localObject2);
        }
      }
    }
    boolean bool4 = ((List)localObject1).isEmpty();
    if (!bool4)
    {
      paramCollection = (bp)l.a();
      bool2 = false;
      localObject2 = null;
      paramCollection = paramCollection.a((Collection)localObject1, false);
      paramCollection.c();
    }
  }
  
  private void c(Collection paramCollection)
  {
    paramCollection = paramCollection.iterator();
    for (;;)
    {
      boolean bool = paramCollection.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = (String)paramCollection.next();
      g localg = (g)h.get();
      localObject = localg.b((String)localObject);
      if (localObject != null)
      {
        bool = a((a)localObject);
        if (!bool) {
          paramCollection.remove();
        }
      }
    }
  }
  
  private boolean d()
  {
    Object localObject1 = f.a();
    try
    {
      Object localObject2 = b;
      Object localObject3 = com.truecaller.common.network.e.a.a;
      localObject2 = ((n)localObject2).b((com.truecaller.common.network.e)localObject3);
      localObject2 = (com.truecaller.api.services.presence.v1.e.a)localObject2;
      if (localObject2 != null)
      {
        localObject3 = com.truecaller.api.services.presence.v1.f.a();
        localObject1 = ((com.truecaller.api.services.presence.v1.f.a)localObject3).a((String)localObject1);
        localObject1 = ((com.truecaller.api.services.presence.v1.f.a)localObject1).build();
        localObject1 = (com.truecaller.api.services.presence.v1.f)localObject1;
        localObject3 = ((com.truecaller.api.services.presence.v1.e.a)localObject2).getChannel();
        ap localap = com.truecaller.api.services.presence.v1.e.a();
        localObject2 = ((com.truecaller.api.services.presence.v1.e.a)localObject2).getCallOptions();
        io.grpc.c.d.a((io.grpc.f)localObject3, localap, (io.grpc.e)localObject2, localObject1);
        return true;
      }
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localRuntimeException);
    }
    return false;
  }
  
  private com.truecaller.api.services.presence.v1.models.b e()
  {
    b.a locala = com.truecaller.api.services.presence.v1.models.b.c();
    boolean bool = i();
    locala = locala.a(bool);
    r.a locala1 = com.google.f.r.newBuilder().setValue(6);
    return (com.truecaller.api.services.presence.v1.models.b)locala.a(locala1).build();
  }
  
  private com.truecaller.api.services.presence.v1.models.i f()
  {
    com.truecaller.voip.d locald = o;
    boolean bool1 = locald.a();
    i.a locala = com.truecaller.api.services.presence.v1.models.i.c();
    boolean bool2 = bool1 ^ true;
    locala = locala.a(bool2);
    if (bool1) {
      locala.a();
    }
    return (com.truecaller.api.services.presence.v1.models.i)locala.build();
  }
  
  private com.truecaller.api.services.presence.v1.models.d g()
  {
    Object localObject = m;
    boolean bool1 = ((bw)localObject).a();
    boolean bool2 = true;
    if (bool1)
    {
      localObject = n.G();
      if (localObject != null)
      {
        bool1 = true;
        break label41;
      }
    }
    bool1 = false;
    localObject = null;
    label41:
    d.a locala = com.truecaller.api.services.presence.v1.models.d.a();
    bool1 ^= bool2;
    return (com.truecaller.api.services.presence.v1.models.d)locala.a(bool1).build();
  }
  
  private com.truecaller.api.services.presence.v1.models.f h()
  {
    Object localObject = ((com.truecaller.featuretoggles.e)p.get()).n();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject).a();
    if (!bool1) {
      return (com.truecaller.api.services.presence.v1.models.f)com.truecaller.api.services.presence.v1.models.f.d().a(false).build();
    }
    localObject = Truepay.getInstance().getPresenceInfo();
    com.truecaller.api.services.presence.v1.models.f.a locala = com.truecaller.api.services.presence.v1.models.f.d();
    boolean bool2 = ((PaymentPresence)localObject).isEnabled();
    locala = locala.a(bool2);
    int i2 = ((PaymentPresence)localObject).getLastTxnTs();
    locala = locala.b(i2);
    int i1 = ((PaymentPresence)localObject).getVersion();
    return (com.truecaller.api.services.presence.v1.models.f)locala.a(i1).build();
  }
  
  private boolean i()
  {
    com.truecaller.common.g.a locala1 = c;
    boolean bool1 = locala1.b("flash_enabled");
    com.truecaller.common.g.a locala2 = c;
    boolean bool2 = locala2.b("featureFlash");
    com.truecaller.common.account.r localr = a;
    boolean bool3 = localr.c();
    return (bool3) && (bool1) && (bool2);
  }
  
  private boolean j()
  {
    com.truecaller.common.g.a locala1 = c;
    boolean bool1 = locala1.b("availability_enabled");
    com.truecaller.common.g.a locala2 = c;
    boolean bool2 = locala2.b("featureAvailability");
    com.truecaller.common.account.r localr = a;
    boolean bool3 = localr.c();
    return (bool3) && (bool1) && (bool2);
  }
  
  private Availability k()
  {
    boolean bool1 = j();
    if (!bool1)
    {
      localObject1 = Availability.c();
      localObject2 = Availability.Status.UNKNOWN;
      return (Availability)((Availability.a)localObject1).a((Availability.Status)localObject2).build();
    }
    Object localObject1 = e;
    bool1 = ((al)localObject1).h();
    Object localObject2 = e;
    int i2 = ((al)localObject2).g();
    int i3 = 1;
    if (i2 == 0)
    {
      i2 = 1;
    }
    else
    {
      i2 = 0;
      localObject2 = null;
    }
    al localal = e;
    boolean bool2 = localal.i();
    Object localObject3;
    if ((i2 == 0) && (!bool2))
    {
      i3 = 0;
      localObject3 = null;
    }
    localObject2 = Availability.c();
    if ((!bool1) && (i3 == 0))
    {
      localObject1 = Availability.Status.AVAILABLE;
      ((Availability.a)localObject2).a((Availability.Status)localObject1);
    }
    else
    {
      localObject3 = Availability.Status.BUSY;
      ((Availability.a)localObject2).a((Availability.Status)localObject3);
      if (bool1) {
        localObject3 = Availability.Context.CALL;
      } else {
        localObject3 = Availability.Context.SLEEP;
      }
      ((Availability.a)localObject2).a((Availability.Context)localObject3);
      localObject3 = t.newBuilder();
      long l1 = System.currentTimeMillis();
      int i1;
      if (bool1) {
        i1 = 600000;
      } else {
        i1 = 7200000;
      }
      long l2 = i1;
      l1 += l2;
      localObject1 = ((t.a)localObject3).setValue(l1);
      ((Availability.a)localObject2).a((t.a)localObject1);
    }
    return (Availability)((Availability.a)localObject2).build();
  }
  
  public final w a()
  {
    Object localObject1 = g;
    boolean bool1 = ((com.truecaller.utils.i)localObject1).a();
    if (!bool1)
    {
      new String[1][0] = "No network, no point in trying to send settings.";
      return w.b(Boolean.FALSE);
    }
    localObject1 = k();
    Object localObject2 = AvailabilityTrigger.USER_ACTION;
    boolean bool2 = true;
    localObject2 = a((AvailabilityTrigger)localObject2, (Availability)localObject1, bool2);
    try
    {
      Object localObject3 = b;
      com.truecaller.common.network.e.a locala = com.truecaller.common.network.e.a.a;
      localObject3 = ((n)localObject3).b(locala);
      localObject3 = (com.truecaller.api.services.presence.v1.e.a)localObject3;
      if (localObject3 == null)
      {
        localObject1 = Boolean.FALSE;
        return w.b(localObject1);
      }
      ((com.truecaller.api.services.presence.v1.e.a)localObject3).a((com.truecaller.api.services.presence.v1.h)localObject2);
      a((Availability)localObject1);
      return w.b(Boolean.TRUE);
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localRuntimeException;
    }
    return w.b(Boolean.FALSE);
  }
  
  public final void a(AvailabilityTrigger paramAvailabilityTrigger, boolean paramBoolean)
  {
    e locale = this;
    boolean bool1 = j();
    if (!bool1) {
      return;
    }
    Object localObject1 = k();
    Object localObject2 = q.a((Availability)localObject1);
    Object localObject3 = c.a("last_availability_update_success");
    Object localObject4 = q.a((String)localObject3);
    int i1 = b((Availability)localObject1);
    int i2 = 1;
    if (localObject4 != null)
    {
      String str = "last_successful_availability_update_time";
      l1 = a(str);
      l2 = System.currentTimeMillis();
      int i3 = b((Availability)localObject4);
      int[] arrayOfInt = e.1.b;
      Object localObject5 = ((Availability)localObject4).a();
      int i4 = ((Availability.Status)localObject5).ordinal();
      int i5 = arrayOfInt[i4];
      switch (i5)
      {
      default: 
        break;
      case 2: 
        arrayOfInt = e.1.a;
        localObject5 = ((Availability)localObject4).b();
        i4 = ((Availability.Context)localObject5).ordinal();
        i5 = arrayOfInt[i4];
        switch (i5)
        {
        default: 
          break;
        case 2: 
          i5 = 1200000;
          break;
        case 1: 
          i5 = 60000;
        }
        break;
      case 1: 
        i5 = 10800000;
        break;
      }
      i5 = 0;
      arrayOfInt = null;
      long l3 = i3;
      l1 += l3;
      long l4 = i5;
      l1 -= l4;
      boolean bool2 = l2 < l1;
      int i6;
      if (bool2)
      {
        i6 = 1;
      }
      else
      {
        i6 = 0;
        str = null;
      }
      Availability.Status localStatus1 = Availability.Status.AVAILABLE;
      Availability.Status localStatus2 = ((Availability)localObject1).a();
      boolean bool3 = localStatus1.equals(localStatus2);
      if (bool3)
      {
        localStatus1 = Availability.Status.AVAILABLE;
        localObject4 = ((Availability)localObject4).a();
        bool4 = localStatus1.equals(localObject4);
        if (!bool4)
        {
          bool4 = true;
          break label333;
        }
      }
      bool4 = false;
      localObject4 = null;
      label333:
      if ((bool4) && (i6 != 0))
      {
        localObject3 = new String[i2];
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>("State (");
        ((StringBuilder)localObject4).append((String)localObject2);
        ((StringBuilder)localObject4).append(") will expire on server soon (meaning it will go back to AVAILABLE), no need for a request. Scheduling for Available though, in ");
        ((StringBuilder)localObject4).append(i1);
        ((StringBuilder)localObject4).append("ms");
        localObject2 = ((StringBuilder)localObject4).toString();
        localObject3[0] = localObject2;
        localObject2 = d;
        l5 = i1;
        ((l)localObject2).a(l5);
        locale.a((Availability)localObject1);
        return;
      }
      bool5 = ((String)localObject2).equals(localObject3);
      if ((bool5) && (i6 == 0))
      {
        localObject1 = new String[i2];
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("State hasn't changed (");
        ((StringBuilder)localObject2).append((String)localObject3);
        ((StringBuilder)localObject2).append("), and is not about to expire, no need for a request. Still rescheduling, to be sure that if for some reason the alarm was triggered too early, we still have a new one scheduled.");
        localObject2 = ((StringBuilder)localObject2).toString();
        localObject1[0] = localObject2;
        l6 = System.currentTimeMillis();
        long l7 = locale.a("last_successful_availability_update_time");
        l6 -= l7;
        long l8 = i1;
        l6 += l8;
        d.a(l6);
        return;
      }
    }
    localObject2 = "key_last_set_status_time";
    long l6 = locale.a((String)localObject2);
    long l1 = 15000L;
    l6 += l1;
    long l2 = System.currentTimeMillis();
    boolean bool4 = l6 < l2;
    if (bool4)
    {
      new String[1][0] = "Not enough time has passed since last update, delay the update";
      d.a(l1);
      return;
    }
    localObject2 = c;
    l1 = System.currentTimeMillis();
    ((com.truecaller.common.g.a)localObject2).b("key_last_set_status_time", l1);
    localObject2 = new String[i2];
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("Scheduling next reportPresence in ");
    ((StringBuilder)localObject3).append(i1);
    localObject4 = "ms";
    ((StringBuilder)localObject3).append((String)localObject4);
    localObject3 = ((StringBuilder)localObject3).toString();
    localObject2[0] = localObject3;
    localObject2 = d;
    long l5 = i1;
    ((l)localObject2).a(l5);
    localObject2 = g;
    boolean bool5 = ((com.truecaller.utils.i)localObject2).a();
    if (!bool5)
    {
      new String[1][0] = "No network, no point in trying to send presence.";
      return;
    }
    localObject2 = paramAvailabilityTrigger;
    localObject2 = locale.a(paramAvailabilityTrigger, (Availability)localObject1, paramBoolean);
    try
    {
      localObject3 = b;
      localObject4 = com.truecaller.common.network.e.a.a;
      localObject3 = ((n)localObject3).b((com.truecaller.common.network.e)localObject4);
      localObject3 = (com.truecaller.api.services.presence.v1.e.a)localObject3;
      if (localObject3 != null)
      {
        ((com.truecaller.api.services.presence.v1.e.a)localObject3).a((com.truecaller.api.services.presence.v1.h)localObject2);
        locale.a((Availability)localObject1);
      }
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localRuntimeException;
    }
  }
  
  public final void a(Collection paramCollection)
  {
    boolean bool = j();
    if (!bool)
    {
      bool = i();
      if (!bool)
      {
        localObject1 = m;
        bool = ((bw)localObject1).a();
        if (!bool)
        {
          localObject1 = o;
          bool = ((com.truecaller.voip.d)localObject1).a();
          if (!bool) {
            return;
          }
        }
      }
    }
    bool = paramCollection.isEmpty();
    if (bool) {
      return;
    }
    Object localObject1 = g;
    bool = ((com.truecaller.utils.i)localObject1).a();
    if (!bool)
    {
      new String[1][0] = "No network, no point in trying to get presence.";
      return;
    }
    localObject1 = (u)i.get();
    Object localObject2 = (com.truecaller.common.h.r)j.get();
    paramCollection = ((u)localObject1).a(paramCollection);
    paramCollection = ((com.truecaller.common.h.r)localObject2).a(paramCollection);
    localObject1 = ((com.truecaller.featuretoggles.e)p.get()).f();
    bool = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool)
    {
      paramCollection = paramCollection.entrySet().iterator();
      for (;;)
      {
        bool = paramCollection.hasNext();
        if (!bool) {
          break;
        }
        localObject1 = (Map.Entry)paramCollection.next();
        localObject2 = (com.truecaller.common.network.e)((Map.Entry)localObject1).getKey();
        localObject1 = (Collection)((Map.Entry)localObject1).getValue();
        a((com.truecaller.common.network.e)localObject2, (Collection)localObject1);
      }
      return;
    }
    localObject1 = com.truecaller.common.network.e.a.a;
    localObject2 = com.truecaller.common.network.e.a.a;
    paramCollection = (Collection)paramCollection.get(localObject2);
    a((com.truecaller.common.network.e)localObject1, paramCollection);
  }
  
  public final void b()
  {
    boolean bool1 = j();
    if (!bool1)
    {
      new String[1][0] = "isAvailabilityEnabled returned false";
      return;
    }
    Object localObject = g;
    bool1 = ((com.truecaller.utils.i)localObject).a();
    if (!bool1)
    {
      new String[1][0] = "No network, no point in trying to send last seen.";
      return;
    }
    localObject = "key_last_set_last_seen_time";
    long l1 = a((String)localObject) + 180000L;
    long l2 = System.currentTimeMillis();
    boolean bool2 = l1 < l2;
    if (bool2)
    {
      new String[1][0] = "Not enough time has passed since last set last seen, delay the update";
      d.a();
      return;
    }
    localObject = c;
    l2 = System.currentTimeMillis();
    ((com.truecaller.common.g.a)localObject).b("key_last_set_last_seen_time", l2);
    d();
  }
  
  public final w c()
  {
    Object localObject1 = g;
    boolean bool1 = ((com.truecaller.utils.i)localObject1).a();
    if (!bool1)
    {
      new String[1][0] = "No network, no point in trying to send settings.";
      return w.b(Boolean.FALSE);
    }
    try
    {
      localObject1 = b;
      Object localObject2 = com.truecaller.common.network.e.a.a;
      localObject1 = ((n)localObject1).b((com.truecaller.common.network.e)localObject2);
      localObject1 = (com.truecaller.api.services.presence.v1.e.a)localObject1;
      if (localObject1 == null)
      {
        localObject1 = Boolean.FALSE;
        return w.b(localObject1);
      }
      localObject2 = com.truecaller.api.services.presence.v1.h.a();
      Object localObject3 = com.truecaller.api.services.presence.v1.models.i.c();
      boolean bool2 = true;
      localObject3 = ((i.a)localObject3).a(bool2);
      localObject3 = ((i.a)localObject3).build();
      localObject3 = (com.truecaller.api.services.presence.v1.models.i)localObject3;
      localObject2 = ((h.a)localObject2).a((com.truecaller.api.services.presence.v1.models.i)localObject3);
      localObject2 = ((h.a)localObject2).build();
      localObject2 = (com.truecaller.api.services.presence.v1.h)localObject2;
      ((com.truecaller.api.services.presence.v1.e.a)localObject1).a((com.truecaller.api.services.presence.v1.h)localObject2);
      return w.b(Boolean.TRUE);
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localRuntimeException;
    }
    return w.b(Boolean.FALSE);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */