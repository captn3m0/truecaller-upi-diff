package com.truecaller.presence;

import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d
{
  private final g a;
  private final Provider b;
  
  private h(g paramg, Provider paramProvider)
  {
    a = paramg;
    b = paramProvider;
  }
  
  public static h a(g paramg, Provider paramProvider)
  {
    h localh = new com/truecaller/presence/h;
    localh.<init>(paramg, paramProvider);
    return localh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */