package com.truecaller.presence;

import com.truecaller.api.services.presence.v1.models.Availability.Status;

 enum AvailabilityStatus
{
  private final Availability.Status mGrpcStatus;
  
  static
  {
    Object localObject = new com/truecaller/presence/AvailabilityStatus;
    Availability.Status localStatus = Availability.Status.AVAILABLE;
    ((AvailabilityStatus)localObject).<init>("AVAILABLE", 0, localStatus);
    AVAILABLE = (AvailabilityStatus)localObject;
    localObject = new com/truecaller/presence/AvailabilityStatus;
    localStatus = Availability.Status.BUSY;
    int i = 1;
    ((AvailabilityStatus)localObject).<init>("BUSY", i, localStatus);
    BUSY = (AvailabilityStatus)localObject;
    localObject = new com/truecaller/presence/AvailabilityStatus;
    localStatus = Availability.Status.UNKNOWN;
    int j = 2;
    ((AvailabilityStatus)localObject).<init>("UNKNOWN", j, localStatus);
    UNKNOWN = (AvailabilityStatus)localObject;
    localObject = new AvailabilityStatus[3];
    AvailabilityStatus localAvailabilityStatus = AVAILABLE;
    localObject[0] = localAvailabilityStatus;
    localAvailabilityStatus = BUSY;
    localObject[i] = localAvailabilityStatus;
    localAvailabilityStatus = UNKNOWN;
    localObject[j] = localAvailabilityStatus;
    $VALUES = (AvailabilityStatus[])localObject;
  }
  
  private AvailabilityStatus(Availability.Status paramStatus)
  {
    mGrpcStatus = paramStatus;
  }
  
  static AvailabilityStatus fromGrpsStatus(Availability.Status paramStatus)
  {
    if (paramStatus != null)
    {
      int[] arrayOfInt = AvailabilityStatus.1.a;
      int i = paramStatus.ordinal();
      i = arrayOfInt[i];
      switch (i)
      {
      default: 
        break;
      case 3: 
      case 4: 
      case 5: 
        return UNKNOWN;
      case 2: 
        return BUSY;
      case 1: 
        return AVAILABLE;
      }
    }
    return UNKNOWN;
  }
  
  static AvailabilityStatus fromString(String paramString, AvailabilityStatus paramAvailabilityStatus)
  {
    if (paramString == null) {
      return paramAvailabilityStatus;
    }
    int i = -1;
    int j = paramString.hashCode();
    int k = 2050553;
    String str;
    boolean bool;
    if (j != k)
    {
      k = 433141802;
      if (j != k)
      {
        k = 2052692649;
        if (j == k)
        {
          str = "AVAILABLE";
          bool = paramString.equals(str);
          if (bool) {
            i = 1;
          }
        }
      }
      else
      {
        str = "UNKNOWN";
        bool = paramString.equals(str);
        if (bool) {
          i = 2;
        }
      }
    }
    else
    {
      str = "BUSY";
      bool = paramString.equals(str);
      if (bool) {
        i = 0;
      }
    }
    switch (i)
    {
    default: 
      return paramAvailabilityStatus;
    case 2: 
      return UNKNOWN;
    case 1: 
      return AVAILABLE;
    }
    return BUSY;
  }
  
  final Availability.Status toGrpcStatus()
  {
    return mGrpcStatus;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.presence.AvailabilityStatus
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */