package com.truecaller.old.data.access;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.provider.Settings.System;
import android.text.TextUtils;
import com.truecaller.TrueApp;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.callhistory.ae;
import com.truecaller.common.enhancedsearch.EnhancedSearchStateWorker;
import com.truecaller.common.h.am;
import com.truecaller.common.h.h;
import com.truecaller.common.tag.sync.AvailableTagsDownloadWorker;
import com.truecaller.filters.p;
import com.truecaller.filters.sync.FilterUploadWorker;
import com.truecaller.filters.v;
import com.truecaller.service.RefreshT9MappingService;
import com.truecaller.service.SyncPhoneBookService;
import com.truecaller.utils.l;
import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class Settings
{
  private static volatile boolean a = false;
  private static volatile SharedPreferences b;
  private static SharedPreferences.Editor c;
  
  public static int a(String paramString, int paramInt)
  {
    SharedPreferences localSharedPreferences = b;
    long l = paramInt;
    return (int)localSharedPreferences.getLong(paramString, l);
  }
  
  public static String a(String paramString1, String paramString2)
  {
    return b.getString(paramString1, paramString2);
  }
  
  public static void a(long paramLong)
  {
    String[] arrayOfString = new String[1];
    String str1 = String.valueOf(paramLong);
    String str2 = "disable ads until ".concat(str1);
    arrayOfString[0] = str2;
    a("adsDisabledUntil", paramLong);
  }
  
  public static void a(Context paramContext, com.truecaller.old.data.entity.b paramb)
  {
    paramb = a.b;
    b("language", paramb);
    c(paramContext);
  }
  
  public static void a(String paramString, long paramLong)
  {
    SharedPreferences.Editor localEditor = c;
    localEditor.putLong(paramString, paramLong);
    boolean bool = a;
    if (bool) {
      return;
    }
    n();
  }
  
  public static void a(String paramString, boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = c;
    localEditor.putBoolean(paramString, paramBoolean);
    boolean bool = a;
    if (bool) {
      return;
    }
    n();
  }
  
  public static boolean a()
  {
    return false;
  }
  
  public static boolean a(int paramInt)
  {
    paramInt &= 0x8;
    return paramInt == 0;
  }
  
  public static boolean a(Context paramContext)
  {
    boolean bool = com.truecaller.common.h.k.a(paramContext);
    return bool;
  }
  
  public static boolean a(String paramString)
  {
    return b.contains(paramString);
  }
  
  public static String b(String paramString)
  {
    return a(paramString, "");
  }
  
  /* Error */
  public static void b(Context paramContext)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aload_0
    //   3: ldc 93
    //   5: iconst_0
    //   6: invokevirtual 99	android/content/Context:getSharedPreferences	(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    //   9: astore_2
    //   10: aload_2
    //   11: putstatic 14	com/truecaller/old/data/access/Settings:b	Landroid/content/SharedPreferences;
    //   14: aload_2
    //   15: invokeinterface 103 1 0
    //   20: putstatic 62	com/truecaller/old/data/access/Settings:c	Landroid/content/SharedPreferences$Editor;
    //   23: invokestatic 109	com/truecaller/TrueApp:y	()Lcom/truecaller/TrueApp;
    //   26: invokevirtual 112	com/truecaller/TrueApp:a	()Lcom/truecaller/bp;
    //   29: astore_2
    //   30: aload_2
    //   31: invokeinterface 118 1 0
    //   36: astore_3
    //   37: aload_2
    //   38: invokeinterface 122 1 0
    //   43: astore 4
    //   45: aload_2
    //   46: invokeinterface 126 1 0
    //   51: astore 5
    //   53: ldc -128
    //   55: astore 6
    //   57: aload 6
    //   59: invokestatic 131	com/truecaller/old/data/access/Settings:e	(Ljava/lang/String;)Z
    //   62: istore 7
    //   64: iload 7
    //   66: ifeq +8 -> 74
    //   69: aload_0
    //   70: invokestatic 134	com/truecaller/old/data/access/Settings:f	(Landroid/content/Context;)V
    //   73: return
    //   74: aload_2
    //   75: invokeinterface 138 1 0
    //   80: invokeinterface 143 1 0
    //   85: ldc -111
    //   87: ldc2_w 146
    //   90: invokestatic 42	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;J)V
    //   93: getstatic 156	android/os/Build$VERSION:RELEASE	Ljava/lang/String;
    //   96: astore 6
    //   98: ldc -105
    //   100: aload 6
    //   102: invokestatic 57	com/truecaller/old/data/access/Settings:b	(Ljava/lang/String;Ljava/lang/String;)V
    //   105: ldc -98
    //   107: bipush 31
    //   109: i2l
    //   110: invokestatic 42	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;J)V
    //   113: ldc -94
    //   115: astore_2
    //   116: invokestatic 168	java/lang/System:currentTimeMillis	()J
    //   119: lstore 8
    //   121: aload 4
    //   123: aload_2
    //   124: lload 8
    //   126: invokeinterface 172 4 0
    //   131: aload_0
    //   132: invokestatic 175	com/truecaller/old/data/access/Settings:g	(Landroid/content/Context;)Z
    //   135: istore 10
    //   137: iconst_1
    //   138: istore 7
    //   140: iload 10
    //   142: ifeq +12 -> 154
    //   145: ldc -79
    //   147: astore_2
    //   148: aload_2
    //   149: iload 7
    //   151: invokestatic 180	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   154: invokestatic 184	com/truecaller/common/h/k:d	()Z
    //   157: istore 10
    //   159: iload 10
    //   161: ifeq +23 -> 184
    //   164: ldc -70
    //   166: astore_2
    //   167: aload_0
    //   168: invokestatic 191	com/truecaller/callerid/b/b:a	(Landroid/content/Context;)I
    //   171: istore 11
    //   173: iload 11
    //   175: i2l
    //   176: lstore 12
    //   178: aload_2
    //   179: lload 12
    //   181: invokestatic 42	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;J)V
    //   184: aload_3
    //   185: ldc -63
    //   187: iload 7
    //   189: invokeinterface 197 3 0
    //   194: aload_3
    //   195: ldc -57
    //   197: iload 7
    //   199: invokeinterface 197 3 0
    //   204: ldc -55
    //   206: iload 7
    //   208: invokestatic 180	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   211: invokestatic 206	com/truecaller/old/data/access/d:d	()Ljava/util/Locale;
    //   214: invokestatic 209	com/truecaller/old/data/access/d:a	(Ljava/util/Locale;)Lcom/truecaller/old/data/entity/b;
    //   217: astore_2
    //   218: aload_0
    //   219: aload_2
    //   220: invokestatic 212	com/truecaller/old/data/access/Settings:a	(Landroid/content/Context;Lcom/truecaller/old/data/entity/b;)V
    //   223: ldc -42
    //   225: iload 7
    //   227: invokestatic 180	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   230: aload 4
    //   232: ldc -40
    //   234: bipush 100
    //   236: invokeinterface 220 3 0
    //   241: ldc -34
    //   243: ldc -32
    //   245: invokestatic 57	com/truecaller/old/data/access/Settings:b	(Ljava/lang/String;Ljava/lang/String;)V
    //   248: invokestatic 168	java/lang/System:currentTimeMillis	()J
    //   251: lstore 12
    //   253: ldc -30
    //   255: lload 12
    //   257: invokestatic 42	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;J)V
    //   260: aload 4
    //   262: ldc -28
    //   264: bipush 21
    //   266: invokeinterface 220 3 0
    //   271: invokestatic 168	java/lang/System:currentTimeMillis	()J
    //   274: lstore 14
    //   276: ldc2_w 232
    //   279: lstore 12
    //   281: lload 14
    //   283: lload 12
    //   285: ladd
    //   286: lstore 14
    //   288: ldc -25
    //   290: lload 14
    //   292: invokestatic 42	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;J)V
    //   295: ldc -19
    //   297: iload 7
    //   299: invokestatic 180	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   302: ldc -17
    //   304: iload 7
    //   306: invokestatic 180	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   309: ldc -15
    //   311: iload 7
    //   313: invokestatic 180	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   316: ldc -13
    //   318: iload 7
    //   320: invokestatic 180	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   323: ldc -11
    //   325: iload 7
    //   327: invokestatic 180	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   330: aload 5
    //   332: ldc -9
    //   334: iload 7
    //   336: invokeinterface 250 3 0
    //   341: ldc -4
    //   343: iload 7
    //   345: invokestatic 180	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   348: ldc -2
    //   350: ldc_w 256
    //   353: invokestatic 57	com/truecaller/old/data/access/Settings:b	(Ljava/lang/String;Ljava/lang/String;)V
    //   356: ldc_w 258
    //   359: iconst_0
    //   360: invokestatic 180	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   363: getstatic 14	com/truecaller/old/data/access/Settings:b	Landroid/content/SharedPreferences;
    //   366: astore_2
    //   367: aload_0
    //   368: ldc_w 260
    //   371: invokevirtual 264	android/content/Context:getDatabasePath	(Ljava/lang/String;)Ljava/io/File;
    //   374: astore_3
    //   375: aload_3
    //   376: invokevirtual 269	java/io/File:exists	()Z
    //   379: istore 16
    //   381: iload 16
    //   383: ifeq +503 -> 886
    //   386: aload_2
    //   387: invokeinterface 103 1 0
    //   392: astore_2
    //   393: iconst_0
    //   394: istore 16
    //   396: aconst_null
    //   397: astore_3
    //   398: ldc_w 260
    //   401: astore 4
    //   403: aload_0
    //   404: aload 4
    //   406: iconst_0
    //   407: aconst_null
    //   408: invokevirtual 273	android/content/Context:openOrCreateDatabase	(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;
    //   411: astore 4
    //   413: ldc_w 275
    //   416: astore 17
    //   418: ldc_w 277
    //   421: astore 5
    //   423: ldc_w 279
    //   426: astore 18
    //   428: ldc_w 281
    //   431: astore 19
    //   433: iconst_3
    //   434: anewarray 27	java/lang/String
    //   437: dup
    //   438: dup2
    //   439: iconst_0
    //   440: aload 5
    //   442: aastore
    //   443: iconst_1
    //   444: aload 18
    //   446: aastore
    //   447: iconst_2
    //   448: aload 19
    //   450: aastore
    //   451: astore 19
    //   453: aconst_null
    //   454: astore 20
    //   456: aconst_null
    //   457: astore 21
    //   459: aload 4
    //   461: astore 18
    //   463: aload 4
    //   465: aload 17
    //   467: aload 19
    //   469: aconst_null
    //   470: aconst_null
    //   471: aconst_null
    //   472: aconst_null
    //   473: aconst_null
    //   474: invokevirtual 287	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   477: astore_3
    //   478: aload_3
    //   479: invokeinterface 292 1 0
    //   484: istore 22
    //   486: iload 22
    //   488: ifeq +327 -> 815
    //   491: aload_3
    //   492: iconst_0
    //   493: invokeinterface 295 2 0
    //   498: astore 5
    //   500: aload_3
    //   501: iload 7
    //   503: invokeinterface 299 2 0
    //   508: astore 18
    //   510: iconst_2
    //   511: istore 23
    //   513: aload_3
    //   514: iload 23
    //   516: invokeinterface 304 2 0
    //   521: istore 24
    //   523: new 306	java/io/DataInputStream
    //   526: astore 20
    //   528: new 308	java/io/ByteArrayInputStream
    //   531: astore 21
    //   533: aload 21
    //   535: aload 18
    //   537: invokespecial 312	java/io/ByteArrayInputStream:<init>	([B)V
    //   540: aload 20
    //   542: aload 21
    //   544: invokespecial 315	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
    //   547: iload 24
    //   549: iload 23
    //   551: if_icmpeq +153 -> 704
    //   554: iconst_4
    //   555: istore 11
    //   557: ldc_w 316
    //   560: fstore 25
    //   562: iload 24
    //   564: iload 11
    //   566: if_icmpeq +117 -> 683
    //   569: bipush 8
    //   571: istore 11
    //   573: ldc_w 317
    //   576: fstore 25
    //   578: iload 24
    //   580: iload 11
    //   582: if_icmpeq +80 -> 662
    //   585: bipush 16
    //   587: istore 11
    //   589: ldc_w 318
    //   592: fstore 25
    //   594: iload 24
    //   596: iload 11
    //   598: if_icmpeq +43 -> 641
    //   601: bipush 32
    //   603: istore 11
    //   605: ldc_w 319
    //   608: fstore 25
    //   610: iload 24
    //   612: iload 11
    //   614: if_icmpeq +6 -> 620
    //   617: goto +110 -> 727
    //   620: aload 20
    //   622: invokevirtual 323	java/io/DataInputStream:readUTF	()Ljava/lang/String;
    //   625: astore 18
    //   627: aload_2
    //   628: aload 5
    //   630: aload 18
    //   632: invokeinterface 327 3 0
    //   637: pop
    //   638: goto +89 -> 727
    //   641: aload 20
    //   643: invokevirtual 330	java/io/DataInputStream:readBoolean	()Z
    //   646: istore 11
    //   648: aload_2
    //   649: aload 5
    //   651: iload 11
    //   653: invokeinterface 78 3 0
    //   658: pop
    //   659: goto +68 -> 727
    //   662: aload 20
    //   664: invokevirtual 334	java/io/DataInputStream:readFloat	()F
    //   667: fstore 25
    //   669: aload_2
    //   670: aload 5
    //   672: fload 25
    //   674: invokeinterface 338 3 0
    //   679: pop
    //   680: goto +47 -> 727
    //   683: aload 20
    //   685: invokevirtual 341	java/io/DataInputStream:readLong	()J
    //   688: lstore 12
    //   690: aload_2
    //   691: aload 5
    //   693: lload 12
    //   695: invokeinterface 68 4 0
    //   700: pop
    //   701: goto +26 -> 727
    //   704: aload 20
    //   706: invokevirtual 345	java/io/DataInputStream:readInt	()I
    //   709: istore 11
    //   711: iload 11
    //   713: i2l
    //   714: lstore 12
    //   716: aload_2
    //   717: aload 5
    //   719: lload 12
    //   721: invokeinterface 68 4 0
    //   726: pop
    //   727: aload 20
    //   729: invokestatic 350	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   732: goto -254 -> 478
    //   735: astore_1
    //   736: goto +72 -> 808
    //   739: astore 18
    //   741: new 352	java/io/IOException
    //   744: astore 17
    //   746: new 354	java/lang/StringBuilder
    //   749: astore 19
    //   751: ldc_w 356
    //   754: astore 21
    //   756: aload 19
    //   758: aload 21
    //   760: invokespecial 359	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   763: aload 19
    //   765: aload 5
    //   767: invokevirtual 363	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   770: pop
    //   771: ldc_w 365
    //   774: astore 5
    //   776: aload 19
    //   778: aload 5
    //   780: invokevirtual 363	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   783: pop
    //   784: aload 19
    //   786: invokevirtual 368	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   789: astore 5
    //   791: aload 17
    //   793: aload 5
    //   795: aload 18
    //   797: invokespecial 371	java/io/IOException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   800: aload 17
    //   802: invokestatic 376	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   805: goto -78 -> 727
    //   808: aload 20
    //   810: invokestatic 350	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   813: aload_1
    //   814: athrow
    //   815: aload_2
    //   816: invokeinterface 379 1 0
    //   821: pop
    //   822: aload_3
    //   823: invokestatic 382	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   826: aload 4
    //   828: invokestatic 385	com/truecaller/util/q:a	(Landroid/database/sqlite/SQLiteDatabase;)V
    //   831: ldc_w 260
    //   834: astore_2
    //   835: aload_0
    //   836: aload_2
    //   837: invokevirtual 388	android/content/Context:deleteDatabase	(Ljava/lang/String;)Z
    //   840: pop
    //   841: aload_0
    //   842: invokestatic 134	com/truecaller/old/data/access/Settings:f	(Landroid/content/Context;)V
    //   845: goto +41 -> 886
    //   848: astore_1
    //   849: goto +7 -> 856
    //   852: astore_1
    //   853: aconst_null
    //   854: astore 4
    //   856: aload_2
    //   857: invokeinterface 379 1 0
    //   862: pop
    //   863: aload_3
    //   864: invokestatic 382	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   867: aload 4
    //   869: invokestatic 385	com/truecaller/util/q:a	(Landroid/database/sqlite/SQLiteDatabase;)V
    //   872: aload_0
    //   873: ldc_w 260
    //   876: invokevirtual 388	android/content/Context:deleteDatabase	(Ljava/lang/String;)Z
    //   879: pop
    //   880: aload_0
    //   881: invokestatic 134	com/truecaller/old/data/access/Settings:f	(Landroid/content/Context;)V
    //   884: aload_1
    //   885: athrow
    //   886: ldc -128
    //   888: iload 7
    //   890: invokestatic 180	com/truecaller/old/data/access/Settings:a	(Ljava/lang/String;Z)V
    //   893: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	894	0	paramContext	Context
    //   1	1	1	localObject1	Object
    //   735	79	1	localObject2	Object
    //   848	1	1	localObject3	Object
    //   852	33	1	localObject4	Object
    //   9	848	2	localObject5	Object
    //   36	828	3	localObject6	Object
    //   43	825	4	localObject7	Object
    //   51	743	5	localObject8	Object
    //   55	46	6	str	String
    //   62	827	7	i	int
    //   119	6	8	l1	long
    //   135	25	10	bool1	boolean
    //   171	444	11	j	int
    //   646	6	11	bool2	boolean
    //   709	3	11	k	int
    //   176	544	12	l2	long
    //   274	17	14	l3	long
    //   379	16	16	bool3	boolean
    //   416	385	17	localObject9	Object
    //   426	205	18	localObject10	Object
    //   739	57	18	localIOException	java.io.IOException
    //   431	354	19	localObject11	Object
    //   454	355	20	localDataInputStream	java.io.DataInputStream
    //   457	302	21	localObject12	Object
    //   484	3	22	bool4	boolean
    //   511	41	23	m	int
    //   521	94	24	n	int
    //   560	113	25	f	float
    // Exception table:
    //   from	to	target	type
    //   620	625	735	finally
    //   630	638	735	finally
    //   641	646	735	finally
    //   651	659	735	finally
    //   662	667	735	finally
    //   672	680	735	finally
    //   683	688	735	finally
    //   693	701	735	finally
    //   704	709	735	finally
    //   719	727	735	finally
    //   741	744	735	finally
    //   746	749	735	finally
    //   758	763	735	finally
    //   765	771	735	finally
    //   778	784	735	finally
    //   784	789	735	finally
    //   795	800	735	finally
    //   800	805	735	finally
    //   620	625	739	java/io/IOException
    //   630	638	739	java/io/IOException
    //   641	646	739	java/io/IOException
    //   651	659	739	java/io/IOException
    //   662	667	739	java/io/IOException
    //   672	680	739	java/io/IOException
    //   683	688	739	java/io/IOException
    //   693	701	739	java/io/IOException
    //   704	709	739	java/io/IOException
    //   719	727	739	java/io/IOException
    //   433	451	848	finally
    //   473	477	848	finally
    //   478	484	848	finally
    //   492	498	848	finally
    //   501	508	848	finally
    //   514	521	848	finally
    //   523	526	848	finally
    //   528	531	848	finally
    //   535	540	848	finally
    //   542	547	848	finally
    //   727	732	848	finally
    //   808	813	848	finally
    //   813	815	848	finally
    //   407	411	852	finally
  }
  
  public static void b(String paramString1, String paramString2)
  {
    SharedPreferences.Editor localEditor = c;
    localEditor.putString(paramString1, paramString2);
    boolean bool = a;
    if (bool) {
      return;
    }
    n();
  }
  
  static boolean b()
  {
    return false;
  }
  
  public static boolean b(int paramInt)
  {
    int i = 8;
    return paramInt == i;
  }
  
  public static boolean b(String paramString, long paramLong)
  {
    long l1 = System.currentTimeMillis();
    paramString = d(paramString);
    long l2 = paramString.longValue();
    l1 -= l2;
    boolean bool = l1 < paramLong;
    return bool;
  }
  
  public static boolean b(String paramString, boolean paramBoolean)
  {
    return b.getBoolean(paramString, paramBoolean);
  }
  
  public static int c(String paramString)
  {
    return (int)b.getLong(paramString, 0L);
  }
  
  public static void c()
  {
    SharedPreferences.Editor localEditor = c;
    localEditor.clear();
    boolean bool = a;
    if (bool) {
      return;
    }
    n();
  }
  
  public static void c(int paramInt)
  {
    long l = paramInt;
    a("contact_count", l);
  }
  
  public static void c(Context paramContext)
  {
    Object localObject1 = "languageAuto";
    boolean bool1 = true;
    boolean bool2 = b((String)localObject1, bool1);
    if (bool2)
    {
      paramContext = Locale.getDefault();
      if (paramContext != null)
      {
        paramContext = d.a(paramContext);
        localObject1 = "language";
        paramContext = a.b;
        b((String)localObject1, paramContext);
      }
      return;
    }
    localObject1 = a("language", "");
    String[] arrayOfString = ((String)localObject1).split("_");
    int i = arrayOfString.length;
    int j = 2;
    Object localObject2;
    if (i == j)
    {
      localObject1 = new java/util/Locale;
      i = 0;
      String str = arrayOfString[0];
      localObject2 = arrayOfString[bool1];
      ((Locale)localObject1).<init>(str, (String)localObject2);
    }
    else
    {
      localObject2 = new java/util/Locale;
      ((Locale)localObject2).<init>((String)localObject1);
      localObject1 = localObject2;
    }
    com.truecaller.common.e.f.a(paramContext, (Locale)localObject1);
  }
  
  public static boolean c(String paramString, long paramLong)
  {
    paramString = d(paramString);
    long l = paramString.longValue();
    boolean bool = l < paramLong;
    return !bool;
  }
  
  public static Long d(String paramString)
  {
    return Long.valueOf(b.getLong(paramString, 0L));
  }
  
  public static void d(String paramString, long paramLong)
  {
    a(o(paramString), paramLong);
    a(p(paramString), false);
  }
  
  public static boolean d()
  {
    Object localObject = "alwaysDownloadImages";
    boolean bool = e((String)localObject);
    if (!bool)
    {
      localObject = TrueApp.y().a().v();
      bool = ((com.truecaller.utils.i)localObject).d();
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  public static boolean d(Context paramContext)
  {
    int i = h(paramContext);
    int j = 1;
    i &= j;
    if (i != 0) {
      return j;
    }
    return false;
  }
  
  public static boolean e()
  {
    return e("hasTruedialerIntegration");
  }
  
  public static boolean e(Context paramContext)
  {
    Object localObject = ((bk)paramContext.getApplicationContext()).a().D();
    boolean bool = ((com.truecaller.i.c)localObject).b("hasNativeDialerCallerId");
    int i = 1;
    if (bool)
    {
      paramContext = paramContext.getContentResolver();
      localObject = "dtmf_tone";
      int j = Settings.System.getInt(paramContext, (String)localObject, i);
      if (j == i) {
        return i;
      }
      return false;
    }
    int k = h(paramContext) & 0x2;
    if (k != 0) {
      return i;
    }
    return false;
  }
  
  public static boolean e(String paramString)
  {
    return b.getBoolean(paramString, false);
  }
  
  private static void f(Context paramContext)
  {
    Object localObject1 = paramContext;
    String str1 = "global_settings_ver";
    boolean bool1 = false;
    Object localObject2 = null;
    int i7 = a(str1, 0);
    int i8 = 31;
    boolean bool14 = true;
    int i11;
    if (i7 < i8)
    {
      i11 = 1;
    }
    else
    {
      i11 = 0;
      localObject3 = null;
    }
    if (i11 == 0) {
      return;
    }
    Object localObject3 = (com.truecaller.common.b.a)paramContext.getApplicationContext();
    Object localObject4 = localObject3;
    localObject4 = ((bk)localObject3).a();
    Object localObject5 = a("version", "7.60");
    String str2 = "VERSION_CODE";
    int i13 = a(str2, 0);
    com.truecaller.i.c localc = ((bp)localObject4).D();
    Object localObject6 = ((bp)localObject4).F();
    com.truecaller.common.g.a locala = ((bp)localObject4).I();
    Object localObject7 = "2.99";
    int i14 = ((String)localObject5).compareTo((String)localObject7);
    if (i14 < 0)
    {
      localObject7 = "GOOGLE_REVIEW_ASK_TIMESTAMP";
      f((String)localObject7);
    }
    localObject7 = "3.0";
    i14 = ((String)localObject5).compareTo((String)localObject7);
    if (i14 < 0)
    {
      localObject7 = "clearTCHistory";
      a((String)localObject7, bool14);
    }
    localObject7 = "3.32";
    i14 = ((String)localObject5).compareTo((String)localObject7);
    if (i14 < 0)
    {
      localObject7 = "backupBatchSize";
      int i16 = 100;
      ((com.truecaller.i.e)localObject6).b((String)localObject7, i16);
    }
    localObject6 = "4.0";
    int i18 = ((String)localObject5).compareTo((String)localObject6);
    if (i18 < 0)
    {
      localObject6 = "notificationPush";
      a((String)localObject6, bool14);
    }
    localObject6 = "4.04";
    i18 = ((String)localObject5).compareTo((String)localObject6);
    label340:
    Object localObject8;
    boolean bool16;
    if (i18 < 0)
    {
      l1 = System.currentTimeMillis();
      a("collaborativeUserTimestamp", l1);
      localObject6 = ((bp)localObject4).R();
      localObject7 = "CALL_FILTER_TOP";
      boolean bool15 = e((String)localObject7);
      if (!bool15)
      {
        localObject7 = "SMS_FILTER_TOP";
        bool15 = e((String)localObject7);
        if (!bool15)
        {
          bool15 = false;
          localObject7 = null;
          break label340;
        }
      }
      bool15 = true;
      localObject8 = "CALL_FILTER_UNKNOWN";
      bool16 = e((String)localObject8);
      if (!bool16)
      {
        localObject8 = "SMS_FILTER_UNKNOWN";
        bool16 = e((String)localObject8);
        if (!bool16)
        {
          bool16 = false;
          localObject8 = null;
          break label389;
        }
      }
      bool16 = true;
      label389:
      ((p)localObject6).f(bool15);
      ((p)localObject6).a(bool16);
    }
    localObject6 = "4.10";
    i18 = ((String)localObject5).compareTo((String)localObject6);
    long l1 = 0L;
    if (i18 < 0)
    {
      localObject6 = "profileVerified";
      boolean bool17 = e((String)localObject6);
      if (bool17)
      {
        localObject6 = h.c(paramContext);
        if (localObject6 != null)
        {
          com.truecaller.wizard.b.c.a(bool14);
          break label477;
        }
      }
      localObject6 = "wizardStep";
      a((String)localObject6, l1);
    }
    label477:
    localObject6 = "4.34";
    int i19 = ((String)localObject5).compareTo((String)localObject6);
    String str3;
    if (i19 < 0)
    {
      str3 = "";
      localObject6 = a("language", str3);
      boolean bool18 = am.a((CharSequence)localObject6);
      if (!bool18)
      {
        localObject6 = d.a(d.d());
        a((Context)localObject1, (com.truecaller.old.data.entity.b)localObject6);
      }
    }
    localObject6 = "4.40";
    int i20 = ((String)localObject5).compareTo((String)localObject6);
    if (i20 < 0)
    {
      a("hasShownWelcome", bool14);
      localObject6 = "countryHash";
      str3 = "37e8d09fd4a669e5d4b3337e926b76ce";
      b((String)localObject6, str3);
    }
    localObject6 = "5.10";
    i20 = ((String)localObject5).compareTo((String)localObject6);
    Object localObject9;
    if (i20 < 0)
    {
      localObject6 = new com/truecaller/old/data/access/f;
      ((f)localObject6).<init>((Context)localObject1);
      str3 = "notificationsSeenCount";
      Collection localCollection = f.b(((f)localObject6).l());
      int i21 = localCollection.size();
      i20 = ((f)localObject6).f();
      i21 -= i20;
      long l2 = i21;
      a(str3, l2);
      f("certValidationError");
      f("toast");
      f("theme_name");
      f("toastDuration");
      a("FEEDBACK_DISMISSED_COUNT", l1);
      localObject2 = "GOOGLE_REVIEW_DONE";
      bool1 = e((String)localObject2);
      if (bool1)
      {
        localObject2 = "FEEDBACK_LIKES_TRUECALLER";
        a((String)localObject2, bool14);
        i8 = 0;
        localObject9 = null;
      }
      else
      {
        localObject2 = "FEEDBACK_LIKES_TRUECALLER";
        i8 = 0;
        localObject9 = null;
        a((String)localObject2, false);
      }
      localObject2 = "HAS_SHARED";
      a((String)localObject2, false);
    }
    localObject2 = "5.30";
    int i = ((String)localObject5).compareTo((String)localObject2);
    if (i < 0)
    {
      localObject2 = "clipboardSearchEnabled";
      localc.b((String)localObject2, bool14);
    }
    localObject2 = "5.40";
    i = ((String)localObject5).compareTo((String)localObject2);
    if (i < 0)
    {
      ((Context)localObject1).deleteDatabase("truecaller.data.History.s3db");
      localObject2 = "truecaller.data.CallersPb.s3db";
      ((Context)localObject1).deleteDatabase((String)localObject2);
    }
    localObject2 = "5.81";
    i = ((String)localObject5).compareTo((String)localObject2);
    if (i < 0)
    {
      localObject2 = "TC.logview.3.11.s3db";
      ((Context)localObject1).deleteDatabase((String)localObject2);
    }
    localObject2 = "6.03";
    i = ((String)localObject5).compareTo((String)localObject2);
    if (i < 0)
    {
      a("alwaysDownloadImages", bool14);
      ((Context)localObject1).deleteDatabase("BlockedSms.s3db");
      localObject2 = ((bp)localObject4).R();
      localObject9 = "TOP_SPAMMERS_SETTINGS";
      i8 = c((String)localObject9);
      if (i8 > 0)
      {
        i8 = 1;
      }
      else
      {
        i8 = 0;
        localObject9 = null;
      }
      ((p)localObject2).f(i8);
      localObject9 = "UNKNOWN_SETTINGS";
      int i9 = c((String)localObject9);
      if (i9 > 0)
      {
        i9 = 1;
      }
      else
      {
        i9 = 0;
        localObject9 = null;
      }
      ((p)localObject2).a(i9);
      localObject2 = com.truecaller.common.b.a.F();
      boolean bool2 = ((com.truecaller.common.b.a)localObject2).p();
      if (bool2) {
        AvailableTagsDownloadWorker.e();
      }
    }
    localObject2 = "6.09";
    int j = ((String)localObject5).compareTo((String)localObject2);
    if (j < 0)
    {
      boolean bool12 = false;
      localObject9 = null;
      localObject2 = ((Context)localObject1).getSharedPreferences("TC.settings.3.0.beta5", 0).getAll().entrySet().iterator();
      for (;;)
      {
        bool12 = ((Iterator)localObject2).hasNext();
        if (!bool12) {
          break;
        }
        localObject9 = (Map.Entry)((Iterator)localObject2).next();
        localObject7 = (String)((Map.Entry)localObject9).getKey();
        localObject9 = ((Map.Entry)localObject9).getValue();
        bool16 = localObject9 instanceof String;
        if (bool16)
        {
          localObject9 = (String)localObject9;
          b((String)localObject7, (String)localObject9);
        }
        else
        {
          bool16 = localObject9 instanceof Boolean;
          if (bool16)
          {
            localObject9 = (Boolean)localObject9;
            bool12 = ((Boolean)localObject9).booleanValue();
            a((String)localObject7, bool12);
          }
          else
          {
            bool16 = localObject9 instanceof Long;
            long l3;
            if (bool16)
            {
              localObject9 = (Long)localObject9;
              l3 = ((Long)localObject9).longValue();
              a((String)localObject7, l3);
            }
            else
            {
              bool16 = localObject9 instanceof Integer;
              if (bool16)
              {
                localObject9 = (Integer)localObject9;
                i10 = ((Integer)localObject9).intValue();
                l3 = i10;
                a((String)localObject7, l3);
              }
            }
          }
        }
      }
    }
    localObject2 = "6.17";
    j = ((String)localObject5).compareTo((String)localObject2);
    int i10 = 3;
    if (j < 0)
    {
      localObject2 = "wizardStep";
      j = c((String)localObject2);
      if (j >= i10) {
        com.truecaller.wizard.b.c.a(bool14);
      }
      ((Context)localObject1).deleteDatabase("truecaller.data.NameSuggestion.s3db");
      ((com.truecaller.common.b.a)localObject3).u().w();
      localObject2 = com.truecaller.common.e.e.c();
      if (localObject2 != null)
      {
        localObject2 = aa.b;
        localObject7 = "";
        localObject3 = a("language", (String)localObject7);
        bool3 = am.a((CharSequence)localObject2, (CharSequence)localObject3);
        if (bool3)
        {
          bool3 = true;
          break label1342;
        }
      }
      boolean bool3 = false;
      localObject2 = null;
      label1342:
      localObject3 = "languageAuto";
      a((String)localObject3, bool3);
      localObject2 = com.truecaller.common.b.a.F();
      bool3 = ((com.truecaller.common.b.a)localObject2).p();
      if (bool3)
      {
        localObject2 = ((bp)localObject4).Q();
        ((v)localObject2).b();
      }
    }
    localObject2 = "6.21";
    int k = ((String)localObject5).compareTo((String)localObject2);
    if (k < 0)
    {
      localObject2 = "truecaller.data.cms.s3db";
      ((Context)localObject1).deleteDatabase((String)localObject2);
    }
    localObject2 = "6.24";
    k = ((String)localObject5).compareTo((String)localObject2);
    if (k < 0) {
      AvailableTagsDownloadWorker.e();
    }
    localObject2 = "6.40";
    k = ((String)localObject5).compareTo((String)localObject2);
    if (k < 0)
    {
      localObject2 = "hasNativeDialerCallerId";
      boolean bool4 = localc.b((String)localObject2);
      if (bool4)
      {
        localObject2 = ((bp)localObject4).aI();
        ((com.truecaller.common.h.c)localObject2).h();
      }
      localObject2 = "clipboardSearchTimeout";
      f((String)localObject2);
    }
    localObject2 = "6.50";
    int m = ((String)localObject5).compareTo((String)localObject2);
    if (m < 0)
    {
      localObject2 = locala.a("profileAcceptAuto");
      localObject3 = "1";
      boolean bool5 = TextUtils.equals((CharSequence)localObject2, (CharSequence)localObject3);
      if (bool5)
      {
        localObject2 = "profileAcceptAuto";
        localObject3 = "0";
        locala.a((String)localObject2, (String)localObject3);
      }
      n = am.f(a("blockCallMode", ""));
      localObject3 = "blockCallMethod";
      localc.b((String)localObject3, n);
      localObject2 = "blockCallMode";
      f((String)localObject2);
    }
    localObject2 = "7.00";
    int n = ((String)localObject5).compareTo((String)localObject2);
    if (n < 0)
    {
      f("DISPLAY_CALL_TAB");
      localObject2 = "availability_enabled";
      a((String)localObject2, bool14);
    }
    localObject2 = "7.01";
    n = ((String)localObject5).compareTo((String)localObject2);
    if (n < 0)
    {
      localObject2 = "CHECK_DEVICE_ID";
      f((String)localObject2);
    }
    localObject2 = "7.10";
    n = ((String)localObject5).compareTo((String)localObject2);
    if (n <= 0)
    {
      localObject2 = "IS_PREALOAD_BUILD";
      localObject3 = ((bp)localObject4).aI().i();
      if (localObject3 != null)
      {
        i11 = 1;
      }
      else
      {
        i11 = 0;
        localObject3 = null;
      }
      locala.b((String)localObject2, i11);
    }
    localObject2 = "7.20";
    n = ((String)localObject5).compareTo((String)localObject2);
    if (n < 0)
    {
      boolean bool6 = g(paramContext);
      if (bool6)
      {
        localObject2 = "hasTruedialerIntegration";
        a((String)localObject2, bool14);
      }
    }
    localObject2 = "7.28";
    int i1 = ((String)localObject5).compareTo((String)localObject2);
    if (i1 < 0)
    {
      localObject2 = locala.a("profileCountryIso");
      localObject7 = "";
      localObject3 = a("codeName", (String)localObject7);
      boolean bool7 = TextUtils.isEmpty((CharSequence)localObject2);
      if (bool7)
      {
        bool7 = TextUtils.isEmpty((CharSequence)localObject3);
        if (!bool7)
        {
          localObject2 = "profileCountryIso";
          locala.a((String)localObject2, (String)localObject3);
        }
      }
      localObject2 = "codeName";
      f((String)localObject2);
    }
    localObject2 = "7.30";
    int i2 = ((String)localObject5).compareTo((String)localObject2);
    if (i2 < 0)
    {
      ((Context)localObject1).deleteDatabase("adKeywords.db");
      localObject2 = "hasShownWelcome";
      i11 = 0;
      localObject3 = null;
      a((String)localObject2, false);
    }
    else
    {
      i11 = 0;
      localObject3 = null;
    }
    localObject2 = "7.50";
    i2 = ((String)localObject5).compareTo((String)localObject2);
    if (i2 < 0)
    {
      f("INMOBI_ID");
      f("INVITE_PEOPLE_LAST_DISMISSED");
      f("INVITE_PEOPLE_DISMISSED");
      f("clearNativeCallLog");
      f("nudgeEnableTopSpammersCounter");
      f("blockHintCounter");
      f("updatePhonebookJobLastRun");
      f("linkedinLoggedIn");
      f("firstSearchDone");
      f("counterLoyalUser");
      f("dualSimSlotId");
      f("dualSimProviderField");
      f("dualSimProviderIndexing");
      f("ui_lang");
      f("counterLoyalUser");
      f("click_item_action_dialer");
      f("multi_sim_call_log_sim_field");
      f("multi_sim_call_log_sim_indexing");
      f("selected_theme");
      f("has_cleared_using_backspace_count");
      f("hasShownRatingDialog");
      f("ratingDialogDate");
      f("hasShownInviteDialog");
      f("inviteDialogDate");
      f("hasPlusOned");
      f("plusOneDialogDate");
      f("force_show_rate");
      f("force_show_invite");
      f("force_show_google_plus");
      ((Context)localObject1).deleteDatabase("truecaller.data.CommonConnectionsListDao.s3db");
      localObject2 = "truecaller.data.Whitelist.s3db";
      ((Context)localObject1).deleteDatabase((String)localObject2);
    }
    i2 = 435;
    if (i13 <= i2)
    {
      b("version", "10.41.6");
      ((Context)localObject1).deleteDatabase("truecaller.data.LogCounterEvent.s3db");
      ((Context)localObject1).deleteDatabase("truecaller.data.LogEvent.s3db");
      n("batchLoggingBatchId");
      n("batchLoggingBatchSize");
      n("checkIfLogEventCountersLastRun");
      n("key_show_ringtone_onboarding");
      a("enhancedNotificationsEnabled", bool14);
      localObject5 = "call";
      b("callLogTapBehavior", (String)localObject5);
      localc.d("lastCallMadeWithTcTime");
      localc.d("lastDialerPromotionTime");
      localObject2 = "dialerTipsShownCount";
      n((String)localObject2);
    }
    i2 = 450;
    if (i13 < i2)
    {
      localObject2 = TrueApp.y().a().bw();
      localObject5 = "showMissedCallsNotifications";
      boolean bool8 = ((l)localObject2).d();
      a((String)localObject5, bool8);
      n("showAlternativeMissedCallNotification");
      localObject2 = "removeDoubleMissedCallNotifications";
      n((String)localObject2);
    }
    int i3 = 454;
    if (i13 < i3)
    {
      localObject2 = "showMissedCallReminders";
      a((String)localObject2, bool14);
    }
    i3 = 1300;
    if (i13 < i3)
    {
      f("blockUpdateLastPressed");
      f("blockUpdateLastPerformed");
      f("blockUpdateCount");
      f("blockUpdateCountLastIncremented");
      f("regionCode");
      f("TC_SEARCH_TIMESTAMP");
      localObject2 = "callerIdTheme";
      f((String)localObject2);
      boolean bool9 = ae.b(paramContext);
      if (bool9)
      {
        localObject2 = (com.truecaller.callhistory.a)((bp)localObject4).ad().a();
        ((com.truecaller.callhistory.a)localObject2).g();
      }
    }
    int i4 = 1314;
    if (i13 < i4)
    {
      f("last_successful_availability_update");
      f("last_successful_time_zone_update");
      localObject2 = "key_busy_reason";
      f((String)localObject2);
    }
    i4 = 1318;
    if (i13 < i4) {
      SyncPhoneBookService.a((Context)localObject1, bool14);
    }
    i4 = 1335;
    if (i13 < i4)
    {
      localObject6 = "availability_enabled";
      boolean bool19 = j((String)localObject6);
      a("flash_enabled", bool19);
      localObject5 = ((Context)localObject1).getDatabasePath("missed_calls.db");
      bool19 = ((File)localObject5).exists();
      if (bool19) {
        ((File)localObject5).delete();
      }
      localObject5 = ((Context)localObject1).getDatabasePath("missed_calls.db-journal");
      bool19 = ((File)localObject5).exists();
      if (bool19) {
        ((File)localObject5).delete();
      }
    }
    int i22 = 2;
    if (i13 != i4)
    {
      i4 = 1336;
      if (i13 != i4) {}
    }
    else
    {
      localObject6 = "afterCallPromoteTcCounter";
      localObject2 = new String[] { "afterCallWarnFriends", localObject6 };
      while (i11 < i22)
      {
        localObject6 = localObject2[i11];
        try
        {
          localObject7 = b;
          int i17 = -1 << -1;
          int i15 = ((SharedPreferences)localObject7).getInt((String)localObject6, i17);
          if (i15 != i17)
          {
            localObject8 = b;
            localObject8 = ((SharedPreferences)localObject8).edit();
            long l4 = i15;
            localObject6 = ((SharedPreferences.Editor)localObject8).putLong((String)localObject6, l4);
            ((SharedPreferences.Editor)localObject6).apply();
          }
        }
        catch (ClassCastException localClassCastException)
        {
          int i12;
          boolean bool10;
          int i5;
          boolean bool13;
          boolean bool11;
          int i6;
          int i23;
          int i24;
          int i25;
          int i26;
          int i27;
          int i28;
          int i29;
          int i30;
          int i31;
          int i32;
          int i33;
          int i34;
          int i35;
          boolean bool20;
          int i36;
          int i37;
          int i38;
          for (;;) {}
        }
        i11 += 1;
      }
    }
    i4 = 1340;
    if (i13 <= i4)
    {
      localObject2 = "blockCount";
      f((String)localObject2);
    }
    i4 = 1358;
    if (i13 <= i4)
    {
      f("FEEDBACK_PLUS_ONE_FIRST_CHECKED");
      f("FEEDBACK_PLUS_ONE_DONE");
      localObject2 = "FEEDBACK_PLUS_ONE_DISMISS_COUNT";
      f((String)localObject2);
    }
    if (i7 <= 0)
    {
      localObject2 = new com/truecaller/old/data/access/i;
      ((i)localObject2).<init>((Context)localObject1);
      ((i)localObject2).b();
      f("PROFILE_MANUALLY_DEACTIVATED");
      f("updatePhonebookTimestamp");
      f("updatePhonebookEnabled");
      f("syncPictures");
      f("syncPicturesOverwrite");
      f("facebookFriendsTimestamp");
      f("linkedinFriendsTimestamp");
      f("googleFriendsTimestamp");
      f("twitterFriendsTimestamp");
      f("whatsNewDialogShownTimestamp");
      f("whatsNewDialogShownTimes");
      f("key_has_shown_default_dialer_sticky");
      f("showDefaultDialerPopupAfterDial");
      f("forceDefaultDialerPopup");
      f("key_has_shown_truecaller_notification");
      f("key_force_show_truecaller_notification");
      f("key_truecaller_notification_click_count");
      f("key_has_shown_identify_unknown_senders");
      f("last_banner_dismiss_timestamp");
      ((Context)localObject1).deleteDatabase("TC.friend.2.90.s3db");
      ((Context)localObject1).deleteDatabase("truecaller.data.automataStorage.s3db");
      f("featureDisableOnboarding");
      f("dialerPromotionStartTime");
      localObject2 = "callerIdHintCount";
      f((String)localObject2);
    }
    if (i7 < i22)
    {
      f("suppressAftercall");
      f("callerIdDialerPromoFirstShow");
      localObject2 = "callerIdDialerPromoLastShow";
      f((String)localObject2);
    }
    if (i7 < i10)
    {
      localObject2 = "referralsDisabledUntil";
      f((String)localObject2);
    }
    i4 = 4;
    if (i7 < i4)
    {
      localObject2 = "alphaRelease";
      localObject9 = "release";
      bool10 = c.g.b.k.a(localObject2, localObject9);
      if (bool10)
      {
        localObject2 = "android.permission.WRITE_EXTERNAL_STORAGE";
        com.truecaller.common.b.e.b((String)localObject2);
      }
    }
    i5 = 5;
    if (i7 < i5)
    {
      com.truecaller.common.b.e.b("shortcutsInboxShownTimes");
      localObject2 = "general_requestPinMessagesShortcutShown";
      com.truecaller.common.b.e.b((String)localObject2);
    }
    i5 = 6;
    if (i7 < i5)
    {
      localObject2 = "HAS_INVITED";
      com.truecaller.common.b.e.b((String)localObject2);
    }
    i5 = 7;
    if (i7 < i5)
    {
      i10 = c("Promo{Referral}DismissCount");
      com.truecaller.common.b.e.b("PromoReferralDismissCount", i10);
      i10 = c("Promo{Defaultsms}DismissCount");
      com.truecaller.common.b.e.b("PromoDefaultsmsDismissCount", i10);
      localObject9 = "Promo{Buypro}DismissCount";
      i10 = c((String)localObject9);
      com.truecaller.common.b.e.b("PromoBuyproDismissCount", i10);
      com.truecaller.common.b.e.b("Promo{Referral}DismissCount");
      com.truecaller.common.b.e.b("Promo{Defaultsms}DismissCount");
      com.truecaller.common.b.e.b("Promo{Buypro}DismissCount");
      localObject2 = "home_screen_banner_close_count";
      com.truecaller.common.b.e.b((String)localObject2);
    }
    i5 = 8;
    if (i7 < i5)
    {
      n("lastDialerPromotionInteractionTime_onboarding");
      n("lastDialerPromotionInteractionTime_frequentlyCalled");
      n("lastDialerPromotionInteractionTime_missed");
      n("lastDialerPromotionInteractionTime_outgoingUnanswered");
      localObject2 = "lastDialerPromotionInteractionTime_incoming";
      n((String)localObject2);
    }
    i5 = 9;
    if (i7 < i5)
    {
      localObject2 = ((bp)localObject4).cv().a();
      ((com.truecaller.backup.e)localObject2).d();
    }
    i5 = 10;
    if (i7 < i5)
    {
      locala.d("edgeLocationsLastRequestTime");
      locala.d("edgeLocationsExpiration");
      localObject2 = "profileNumberBackEnd";
      n((String)localObject2);
    }
    i5 = 11;
    if (i7 < i5)
    {
      localObject2 = com.truecaller.common.b.e.a("wizard_StartPage");
      localObject9 = "Page_CallVerification";
      bool13 = ((String)localObject2).equals(localObject9);
      if (!bool13)
      {
        localObject9 = "Page_SmsVerification";
        bool11 = ((String)localObject2).equals(localObject9);
        if (!bool11) {}
      }
      else
      {
        localObject2 = "wizard_StartPage";
        com.truecaller.common.b.e.b((String)localObject2);
      }
    }
    i6 = 12;
    if (i7 < i6)
    {
      locala.d("featureAdUnifiedSearchHistory");
      locala.d("featureAdUnifiedBlock");
      locala.d("featureAdUnifiedCallLog");
      localObject2 = "featureAdUnifiedInbox";
      locala.d((String)localObject2);
    }
    i6 = 13;
    if (i7 < i6)
    {
      localObject2 = "presenceSettingNeedSync";
      locala.d((String)localObject2);
    }
    i6 = 15;
    if (i7 < i6) {
      RefreshT9MappingService.a(paramContext);
    }
    i23 = 16;
    if (i7 < i23)
    {
      localObject1 = "featureBusinessSuggestion";
      locala.d((String)localObject1);
    }
    i24 = 17;
    if (i7 < i24)
    {
      localObject1 = "HeartBeatLastTime";
      n((String)localObject1);
    }
    i25 = 18;
    if (i7 < i25)
    {
      locala.d("filter_scheduledFilterSyncingEnabled");
      localObject1 = "filter_settingsLastVisitTimestamp";
      locala.d((String)localObject1);
    }
    i26 = 19;
    if (i7 < i26)
    {
      localObject1 = "whatsAppCallsEnabled";
      localc.b((String)localObject1, bool14);
    }
    i27 = 20;
    if (i7 < i27)
    {
      localObject1 = "filter_filterJustActivated";
      locala.d((String)localObject1);
    }
    i28 = 21;
    if (i7 < i28)
    {
      n("call_counter");
      n("lastCallMeBackTime");
      localObject1 = "MsgMastSyncTime";
      n((String)localObject1);
    }
    i29 = 22;
    if (i7 < i29)
    {
      localObject1 = "smart_notifications";
      locala.b((String)localObject1, bool14);
    }
    i30 = 23;
    if (i7 < i30)
    {
      localObject1 = "backupWhatsNewShown";
      n((String)localObject1);
    }
    i31 = 24;
    if (i7 < i31)
    {
      localObject1 = "featureShowOptInReadMore";
      n((String)localObject1);
    }
    i32 = 25;
    if (i7 < i32)
    {
      n("debugLoggingUploadTriggered");
      localObject1 = "lastTracingFeatureTime";
      n((String)localObject1);
    }
    i33 = 26;
    if (i7 < i33)
    {
      localObject1 = "initializeJobLastRun";
      n((String)localObject1);
    }
    i34 = 27;
    if (i7 < i34)
    {
      n("EmojiBarTipWasShown");
      n("EmojiBarEverUsed");
      localObject1 = "ConversationScreenOpenCount";
      n((String)localObject1);
    }
    i35 = 28;
    if (i7 < i35)
    {
      localObject1 = "backupNeedsSync";
      bool20 = j((String)localObject1);
      if (bool20)
      {
        localObject1 = "backupSyncValue";
        bool20 = j((String)localObject1);
        EnhancedSearchStateWorker.a(bool20);
      }
      n("backupNeedsSync");
      localObject1 = "backupSyncValue";
      n((String)localObject1);
    }
    i36 = 29;
    if (i7 < i36)
    {
      localObject1 = "UNUSED_DIRECTORIES_DELETED_ON_UPGRADE";
      n((String)localObject1);
    }
    i37 = 30;
    if (i7 < i37)
    {
      FilterUploadWorker.b();
      localObject1 = "filter_filtersRestored";
      n((String)localObject1);
      i38 = 31;
    }
    else
    {
      i38 = 31;
    }
    if (i7 < i38)
    {
      localObject1 = "imTooltipShown";
      n((String)localObject1);
    }
    a("global_settings_ver", 31);
  }
  
  public static void f(String paramString)
  {
    SharedPreferences.Editor localEditor = c;
    localEditor.remove(paramString);
    boolean bool = a;
    if (bool) {
      return;
    }
    n();
  }
  
  public static boolean f()
  {
    Object localObject = ((bk)com.truecaller.common.b.a.F()).a().I();
    String str = "featureAvailability";
    boolean bool = ((com.truecaller.common.g.a)localObject).a(str, false);
    if (bool)
    {
      localObject = "availability_enabled";
      bool = e((String)localObject);
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public static void g(String paramString)
  {
    long l = System.currentTimeMillis();
    a(paramString, l);
  }
  
  public static boolean g()
  {
    Object localObject = ((bk)com.truecaller.common.b.a.F()).a().I();
    String str = "featureFlash";
    boolean bool = ((com.truecaller.common.g.a)localObject).a(str, false);
    if (bool)
    {
      localObject = "flash_enabled";
      bool = e((String)localObject);
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  private static boolean g(Context paramContext)
  {
    Object localObject = Settings.BuildName.toBuildName(((bk)paramContext.getApplicationContext()).a().aI().f());
    if (localObject != null)
    {
      String str = ((Settings.BuildName)localObject).getPackageName();
      boolean bool = TextUtils.isEmpty(str);
      if (!bool)
      {
        localObject = ((Settings.BuildName)localObject).getPackageName().replace("truecaller", "truedialer");
        return paramContext.getPackageManager().hasSystemFeature((String)localObject);
      }
    }
    return false;
  }
  
  private static int h(Context paramContext)
  {
    String str = "";
    Object localObject = a("dialpad_feedback_index_str", str);
    boolean bool = TextUtils.isEmpty((CharSequence)localObject);
    if (bool) {
      localObject = "-1";
    }
    localObject = Integer.valueOf((String)localObject);
    int j = ((Integer)localObject).intValue();
    int i = -1;
    if (j == i)
    {
      paramContext = paramContext.getContentResolver();
      localObject = "haptic_feedback_enabled";
      i = 1;
      j = Settings.System.getInt(paramContext, (String)localObject, i);
      paramContext = "dialpad_feedback_index_str";
      str = String.valueOf(j);
      b(paramContext, str);
    }
    return j;
  }
  
  public static void h(String paramString)
  {
    a(paramString, 0L);
  }
  
  public static boolean h()
  {
    Object localObject = ((bk)com.truecaller.common.b.a.F()).a().ai();
    String str = "qaForceAds";
    boolean bool1 = e(str);
    boolean bool2 = true;
    if (!bool1)
    {
      boolean bool3 = ((com.truecaller.common.f.c)localObject).d();
      bool1 = false;
      str = null;
      if (!bool3)
      {
        localObject = d("adsDisabledUntil");
        long l1 = ((Long)localObject).longValue();
        long l2 = 0L;
        bool3 = l1 < l2;
        if (bool3)
        {
          long l3 = System.currentTimeMillis() - l1;
          bool3 = l3 < l2;
          if (bool3)
          {
            bool3 = true;
          }
          else
          {
            bool3 = false;
            localObject = null;
          }
        }
        else
        {
          bool3 = false;
          localObject = null;
        }
        if (!bool3)
        {
          localObject = com.truecaller.common.b.a.F();
          bool3 = ((com.truecaller.common.b.a)localObject).p();
          if (bool3)
          {
            bool3 = i();
            if (!bool3) {
              return bool2;
            }
          }
        }
      }
      return false;
    }
    return bool2;
  }
  
  public static void i(String paramString)
  {
    long l = d(paramString).longValue() + 1L;
    a(paramString, l);
  }
  
  public static boolean i()
  {
    Settings.BuildName localBuildName1 = Settings.BuildName.toBuildName(com.truecaller.common.b.a.F().e().f());
    Settings.BuildName localBuildName2 = Settings.BuildName.WILEYFOX;
    return localBuildName1 == localBuildName2;
  }
  
  public static long j()
  {
    return d("blockCallCounter").longValue();
  }
  
  public static boolean j(String paramString)
  {
    return b.getBoolean(paramString, false);
  }
  
  public static String k()
  {
    String str1 = l();
    String str2 = "auto";
    boolean bool = TextUtils.equals(str1, str2);
    if (bool) {
      return com.truecaller.common.e.f.a().getLanguage();
    }
    return str1;
  }
  
  public static boolean k(String paramString)
  {
    return e(p(paramString));
  }
  
  public static long l(String paramString)
  {
    return d(o(paramString)).longValue();
  }
  
  public static String l()
  {
    return b.getString("t9_lang", "auto");
  }
  
  public static int m()
  {
    return a("contact_count", -1);
  }
  
  public static void m(String paramString)
  {
    a(p(paramString), true);
  }
  
  private static void n()
  {
    a = false;
    c.apply();
  }
  
  public static void n(String paramString)
  {
    Object localObject = b;
    boolean bool1 = ((SharedPreferences)localObject).contains(paramString);
    if (!bool1) {
      return;
    }
    localObject = c;
    ((SharedPreferences.Editor)localObject).remove(paramString);
    boolean bool2 = a;
    if (bool2) {
      return;
    }
    n();
  }
  
  private static String o(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("truecaller.alarm.notification.");
    localStringBuilder.append(paramString);
    localStringBuilder.append(".set");
    return localStringBuilder.toString();
  }
  
  private static String p(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("truecaller.alarm.notification.");
    localStringBuilder.append(paramString);
    localStringBuilder.append(".fired");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.Settings
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */