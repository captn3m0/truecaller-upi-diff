package com.truecaller.old.data.access;

import android.text.TextUtils;
import com.truecaller.common.e.c;
import com.truecaller.common.e.e;
import com.truecaller.old.data.entity.b;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class d
{
  private static volatile b a;
  private static volatile List b;
  private static volatile List c;
  private static volatile Locale d;
  private static final e e;
  private static final com.truecaller.common.e.d f = com.truecaller.common.e.d.a;
  
  static
  {
    e locale = new com/truecaller/common/e/e;
    locale.<init>();
    e = locale;
  }
  
  public static b a(String paramString)
  {
    Iterator localIterator = a(false).iterator();
    b localb;
    boolean bool2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localb = (b)localIterator.next();
      String str = a.b;
      bool2 = str.equalsIgnoreCase(paramString);
    } while (!bool2);
    return localb;
    return a;
  }
  
  public static b a(Locale paramLocale)
  {
    Object localObject1 = a(false);
    if (paramLocale != null)
    {
      String str1 = paramLocale.getLanguage();
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append(str1);
      ((StringBuilder)localObject2).append("_");
      Object localObject3 = paramLocale.getCountry();
      ((StringBuilder)localObject2).append((String)localObject3);
      localObject2 = ((StringBuilder)localObject2).toString();
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      ((StringBuilder)localObject3).append(str1);
      ((StringBuilder)localObject3).append("_");
      Object localObject4 = paramLocale.getVariant();
      ((StringBuilder)localObject3).append((String)localObject4);
      localObject3 = ((StringBuilder)localObject3).toString();
      paramLocale = paramLocale.toString();
      localObject1 = ((List)localObject1).iterator();
      boolean bool2;
      do
      {
        boolean bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break label231;
        }
        localObject4 = (b)((Iterator)localObject1).next();
        String str2 = a.b;
        bool2 = str2.equalsIgnoreCase(str1);
        if (bool2) {
          break;
        }
        str2 = a.b;
        bool2 = str2.equalsIgnoreCase((String)localObject2);
        if (bool2) {
          break;
        }
        str2 = a.b;
        bool2 = str2.equalsIgnoreCase((String)localObject3);
        if (bool2) {
          break;
        }
        str2 = a.b;
        bool2 = str2.equalsIgnoreCase(paramLocale);
      } while (!bool2);
      return (b)localObject4;
    }
    label231:
    return a;
  }
  
  public static List a()
  {
    return a(false);
  }
  
  /* Error */
  private static List a(boolean paramBoolean)
  {
    // Byte code:
    //   0: iload_0
    //   1: ifeq +10 -> 11
    //   4: getstatic 94	com/truecaller/old/data/access/d:c	Ljava/util/List;
    //   7: astore_1
    //   8: goto +7 -> 15
    //   11: getstatic 96	com/truecaller/old/data/access/d:b	Ljava/util/List;
    //   14: astore_1
    //   15: aload_1
    //   16: ifnull +5 -> 21
    //   19: aload_1
    //   20: areturn
    //   21: ldc 2
    //   23: astore_2
    //   24: aload_2
    //   25: monitorenter
    //   26: aload_1
    //   27: ifnull +7 -> 34
    //   30: aload_2
    //   31: monitorexit
    //   32: aload_1
    //   33: areturn
    //   34: new 51	com/truecaller/old/data/entity/b
    //   37: astore_1
    //   38: invokestatic 99	com/truecaller/common/e/d:a	()Lcom/truecaller/common/e/c;
    //   41: astore_3
    //   42: aload_1
    //   43: aload_3
    //   44: invokespecial 102	com/truecaller/old/data/entity/b:<init>	(Lcom/truecaller/common/e/c;)V
    //   47: aload_1
    //   48: putstatic 67	com/truecaller/old/data/access/d:a	Lcom/truecaller/old/data/entity/b;
    //   51: new 104	java/util/ArrayList
    //   54: astore_1
    //   55: aload_1
    //   56: invokespecial 105	java/util/ArrayList:<init>	()V
    //   59: iload_0
    //   60: ifeq +10 -> 70
    //   63: invokestatic 108	com/truecaller/common/e/d:f	()Ljava/util/List;
    //   66: astore_3
    //   67: goto +7 -> 74
    //   70: invokestatic 111	com/truecaller/common/e/d:g	()Ljava/util/List;
    //   73: astore_3
    //   74: aload_3
    //   75: invokeinterface 39 1 0
    //   80: astore_3
    //   81: aload_3
    //   82: invokeinterface 45 1 0
    //   87: istore 4
    //   89: iload 4
    //   91: ifeq +42 -> 133
    //   94: aload_3
    //   95: invokeinterface 49 1 0
    //   100: astore 5
    //   102: aload 5
    //   104: checkcast 56	com/truecaller/common/e/c
    //   107: astore 5
    //   109: new 51	com/truecaller/old/data/entity/b
    //   112: astore 6
    //   114: aload 6
    //   116: aload 5
    //   118: invokespecial 102	com/truecaller/old/data/entity/b:<init>	(Lcom/truecaller/common/e/c;)V
    //   121: aload_1
    //   122: aload 6
    //   124: invokeinterface 115 2 0
    //   129: pop
    //   130: goto -49 -> 81
    //   133: invokestatic 118	com/truecaller/common/e/e:a	()Ljava/util/Set;
    //   136: astore_3
    //   137: getstatic 120	com/truecaller/old/data/access/d:d	Ljava/util/Locale;
    //   140: astore 5
    //   142: aload 5
    //   144: ifnull +188 -> 332
    //   147: aload 5
    //   149: invokevirtual 73	java/util/Locale:getLanguage	()Ljava/lang/String;
    //   152: astore 6
    //   154: aload 6
    //   156: invokevirtual 123	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   159: astore 6
    //   161: aload_3
    //   162: aload 6
    //   164: invokeinterface 126 2 0
    //   169: istore 7
    //   171: iload 7
    //   173: ifeq +159 -> 332
    //   176: aload 5
    //   178: invokevirtual 85	java/util/Locale:getCountry	()Ljava/lang/String;
    //   181: astore 8
    //   183: aload 8
    //   185: invokevirtual 123	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   188: astore 8
    //   190: aload 5
    //   192: invokevirtual 91	java/util/Locale:getVariant	()Ljava/lang/String;
    //   195: astore 5
    //   197: aload 5
    //   199: invokevirtual 123	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   202: astore 5
    //   204: aload 8
    //   206: invokevirtual 130	java/lang/String:length	()I
    //   209: istore 9
    //   211: iconst_2
    //   212: istore 10
    //   214: iload 9
    //   216: iload 10
    //   218: if_icmple +60 -> 278
    //   221: new 75	java/lang/StringBuilder
    //   224: astore 8
    //   226: aload 8
    //   228: invokespecial 76	java/lang/StringBuilder:<init>	()V
    //   231: aload 8
    //   233: aload 6
    //   235: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   238: pop
    //   239: ldc 82
    //   241: astore 6
    //   243: aload 8
    //   245: aload 6
    //   247: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   250: pop
    //   251: aload 8
    //   253: aload 5
    //   255: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   258: pop
    //   259: aload 8
    //   261: invokevirtual 88	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   264: astore 5
    //   266: aload_3
    //   267: aload 5
    //   269: invokeinterface 126 2 0
    //   274: pop
    //   275: goto +57 -> 332
    //   278: new 75	java/lang/StringBuilder
    //   281: astore 5
    //   283: aload 5
    //   285: invokespecial 76	java/lang/StringBuilder:<init>	()V
    //   288: aload 5
    //   290: aload 6
    //   292: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   295: pop
    //   296: ldc 82
    //   298: astore 6
    //   300: aload 5
    //   302: aload 6
    //   304: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   307: pop
    //   308: aload 5
    //   310: aload 8
    //   312: invokevirtual 80	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   315: pop
    //   316: aload 5
    //   318: invokevirtual 88	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   321: astore 5
    //   323: aload_3
    //   324: aload 5
    //   326: invokeinterface 126 2 0
    //   331: pop
    //   332: ldc -123
    //   334: astore 5
    //   336: aload 5
    //   338: invokestatic 138	com/truecaller/old/data/access/Settings:b	(Ljava/lang/String;)Ljava/lang/String;
    //   341: astore 5
    //   343: aload 5
    //   345: invokestatic 144	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   348: istore 11
    //   350: iload 11
    //   352: ifne +27 -> 379
    //   355: aload_3
    //   356: aload 5
    //   358: invokeinterface 147 2 0
    //   363: istore 11
    //   365: iload 11
    //   367: ifne +12 -> 379
    //   370: aload_3
    //   371: aload 5
    //   373: invokeinterface 126 2 0
    //   378: pop
    //   379: new 104	java/util/ArrayList
    //   382: astore 5
    //   384: aload 5
    //   386: invokespecial 105	java/util/ArrayList:<init>	()V
    //   389: aload_1
    //   390: invokeinterface 39 1 0
    //   395: astore_1
    //   396: aload_1
    //   397: invokeinterface 45 1 0
    //   402: istore 11
    //   404: iload 11
    //   406: ifeq +79 -> 485
    //   409: aload_1
    //   410: invokeinterface 49 1 0
    //   415: astore 6
    //   417: aload 6
    //   419: checkcast 51	com/truecaller/old/data/entity/b
    //   422: astore 6
    //   424: aload 6
    //   426: getfield 54	com/truecaller/old/data/entity/b:a	Lcom/truecaller/common/e/c;
    //   429: astore 8
    //   431: aload 8
    //   433: getfield 59	com/truecaller/common/e/c:b	Ljava/lang/String;
    //   436: astore 8
    //   438: aload 8
    //   440: invokevirtual 123	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   443: astore 8
    //   445: aload_3
    //   446: aload 8
    //   448: invokeinterface 147 2 0
    //   453: istore 9
    //   455: iload 9
    //   457: ifeq -61 -> 396
    //   460: aload 8
    //   462: invokestatic 149	com/truecaller/common/e/e:a	(Ljava/lang/String;)Z
    //   465: istore 7
    //   467: iload 7
    //   469: ifeq -73 -> 396
    //   472: aload 5
    //   474: aload 6
    //   476: invokeinterface 115 2 0
    //   481: pop
    //   482: goto -86 -> 396
    //   485: iload_0
    //   486: ifeq +18 -> 504
    //   489: aload 5
    //   491: invokestatic 155	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
    //   494: astore 12
    //   496: aload 12
    //   498: putstatic 94	com/truecaller/old/data/access/d:c	Ljava/util/List;
    //   501: goto +15 -> 516
    //   504: aload 5
    //   506: invokestatic 155	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
    //   509: astore 12
    //   511: aload 12
    //   513: putstatic 96	com/truecaller/old/data/access/d:b	Ljava/util/List;
    //   516: aload_2
    //   517: monitorexit
    //   518: aload 5
    //   520: areturn
    //   521: astore 12
    //   523: aload_2
    //   524: monitorexit
    //   525: aload 12
    //   527: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	528	0	paramBoolean	boolean
    //   7	403	1	localObject1	Object
    //   23	501	2	localClass	Class
    //   41	405	3	localObject2	Object
    //   87	3	4	bool1	boolean
    //   100	419	5	localObject3	Object
    //   112	363	6	localObject4	Object
    //   169	299	7	bool2	boolean
    //   181	280	8	localObject5	Object
    //   209	10	9	i	int
    //   453	3	9	bool3	boolean
    //   212	7	10	j	int
    //   348	57	11	bool4	boolean
    //   494	18	12	localList	List
    //   521	5	12	localObject6	Object
    // Exception table:
    //   from	to	target	type
    //   30	32	521	finally
    //   34	37	521	finally
    //   38	41	521	finally
    //   43	47	521	finally
    //   47	51	521	finally
    //   51	54	521	finally
    //   55	59	521	finally
    //   63	66	521	finally
    //   70	73	521	finally
    //   74	80	521	finally
    //   81	87	521	finally
    //   94	100	521	finally
    //   102	107	521	finally
    //   109	112	521	finally
    //   116	121	521	finally
    //   122	130	521	finally
    //   133	136	521	finally
    //   137	140	521	finally
    //   147	152	521	finally
    //   154	159	521	finally
    //   162	169	521	finally
    //   176	181	521	finally
    //   183	188	521	finally
    //   190	195	521	finally
    //   197	202	521	finally
    //   204	209	521	finally
    //   221	224	521	finally
    //   226	231	521	finally
    //   233	239	521	finally
    //   245	251	521	finally
    //   253	259	521	finally
    //   259	264	521	finally
    //   267	275	521	finally
    //   278	281	521	finally
    //   283	288	521	finally
    //   290	296	521	finally
    //   302	308	521	finally
    //   310	316	521	finally
    //   316	321	521	finally
    //   324	332	521	finally
    //   336	341	521	finally
    //   343	348	521	finally
    //   356	363	521	finally
    //   371	379	521	finally
    //   379	382	521	finally
    //   384	389	521	finally
    //   389	395	521	finally
    //   396	402	521	finally
    //   409	415	521	finally
    //   417	422	521	finally
    //   424	429	521	finally
    //   431	436	521	finally
    //   438	443	521	finally
    //   446	453	521	finally
    //   460	465	521	finally
    //   474	482	521	finally
    //   489	494	521	finally
    //   496	501	521	finally
    //   504	509	521	finally
    //   511	516	521	finally
    //   516	518	521	finally
    //   523	525	521	finally
  }
  
  public static List b()
  {
    return a(true);
  }
  
  public static void b(Locale paramLocale)
  {
    d = paramLocale;
  }
  
  public static boolean c()
  {
    Object localObject = Locale.getDefault();
    if (localObject != null)
    {
      localObject = ((Locale)localObject).getDisplayName((Locale)localObject);
      boolean bool = TextUtils.isEmpty((CharSequence)localObject);
      if (!bool)
      {
        int i = Character.getDirectionality(((String)localObject).charAt(0));
        int j = 1;
        if (i != j)
        {
          int k = Character.getDirectionality(((String)localObject).charAt(0));
          i = 2;
          if (k != i) {
            return false;
          }
        }
        return j;
      }
    }
    return false;
  }
  
  public static Locale d()
  {
    return d;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */