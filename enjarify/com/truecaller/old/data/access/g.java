package com.truecaller.old.data.access;

import android.content.Context;
import com.truecaller.old.data.entity.e;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class g
  extends b
{
  public g(Context paramContext)
  {
    super(paramContext);
  }
  
  protected final String d()
  {
    return "PhoneNotification";
  }
  
  public final List f()
  {
    Object localObject = a();
    if (localObject == null) {
      return Collections.emptyList();
    }
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    localObject = ((List)localObject).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject).hasNext();
      if (!bool1) {
        break;
      }
      e locale = (e)((Iterator)localObject).next();
      boolean bool2 = d;
      if (bool2) {
        localArrayList.add(locale);
      }
    }
    return localArrayList;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */