package com.truecaller.old.data.access;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.truecaller.content.c.v;
import com.truecaller.content.c.w;
import com.truecaller.log.AssertionUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class a
{
  protected final Context a;
  private boolean b;
  
  public a(Context paramContext)
  {
    boolean bool = Settings.b();
    b = bool;
    a = paramContext;
  }
  
  public final com.truecaller.old.data.entity.d a(Class paramClass, int paramInt)
  {
    Object localObject = null;
    try
    {
      paramClass = paramClass.newInstance();
      paramClass = (com.truecaller.old.data.entity.d)paramClass;
    }
    catch (IllegalAccessException paramClass)
    {
      localObject = new String[0];
      AssertionUtil.shouldNeverHappen(paramClass, (String[])localObject);
    }
    catch (InstantiationException paramClass)
    {
      localObject = new String[0];
      AssertionUtil.shouldNeverHappen(paramClass, (String[])localObject);
    }
    paramClass = null;
    localObject = d();
    String str = Integer.toString(paramInt % (-1 >>> 1));
    str = ((SharedPreferences)localObject).getString(str, "");
    paramClass.a(str);
    return paramClass;
  }
  
  protected abstract String a();
  
  public final List a(Class paramClass)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    int i = c();
    int j = e();
    int k = 0;
    for (;;)
    {
      if (k < i)
      {
        int m = j - k;
        try
        {
          com.truecaller.old.data.entity.d locald = a(paramClass, m);
          localArrayList.add(locald);
          k += 1;
        }
        catch (Exception paramClass)
        {
          String str = "In Dao - getAllForSuggestions exception";
          com.truecaller.log.d.a(paramClass, str);
          localArrayList.clear();
        }
      }
    }
    return localArrayList;
  }
  
  final void a(SharedPreferences.Editor paramEditor)
  {
    boolean bool = b;
    if (bool)
    {
      paramEditor.commit();
      return;
    }
    paramEditor.apply();
  }
  
  public void a(com.truecaller.old.data.entity.d paramd)
  {
    Object localObject = d();
    int i = ((SharedPreferences)localObject).getInt("size", 0);
    localObject = ((SharedPreferences)localObject).edit();
    i += 1;
    String str = Integer.toString(i % (-1 >>> 1));
    paramd = paramd.a();
    ((SharedPreferences.Editor)localObject).putString(str, paramd);
    ((SharedPreferences.Editor)localObject).putInt("size", i);
    a((SharedPreferences.Editor)localObject);
  }
  
  public void a(List paramList)
  {
    Object localObject1 = d();
    String str1 = "size";
    boolean bool = false;
    Object localObject2 = null;
    int i = ((SharedPreferences)localObject1).getInt(str1, 0);
    localObject1 = ((SharedPreferences)localObject1).edit();
    if (paramList != null)
    {
      paramList = paramList.iterator();
      for (;;)
      {
        bool = paramList.hasNext();
        if (!bool) {
          break;
        }
        localObject2 = (com.truecaller.old.data.entity.d)paramList.next();
        i += 1;
        int j = i % (-1 >>> 1);
        String str2 = Integer.toString(j);
        localObject2 = ((com.truecaller.old.data.entity.d)localObject2).a();
        ((SharedPreferences.Editor)localObject1).putString(str2, (String)localObject2);
      }
    }
    ((SharedPreferences.Editor)localObject1).putInt("size", i);
    a((SharedPreferences.Editor)localObject1);
  }
  
  public void a(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = d().edit();
    localEditor.clear();
    a(localEditor);
  }
  
  public void b()
  {
    a(true);
  }
  
  public int c()
  {
    return Math.min(e(), -1 >>> 1);
  }
  
  protected final SharedPreferences d()
  {
    Object localObject = a();
    SharedPreferences localSharedPreferences = v.a(a, (String)localObject);
    Context localContext = a;
    String str = "TC.settings.3.0.beta5";
    boolean bool = w.a(localContext, str);
    if (bool)
    {
      localContext = a;
      str = null;
      localObject = localContext.getSharedPreferences((String)localObject, 0);
      w.a((SharedPreferences)localObject, localSharedPreferences);
      localObject = ((SharedPreferences)localObject).edit().clear();
      ((SharedPreferences.Editor)localObject).commit();
    }
    return localSharedPreferences;
  }
  
  protected final int e()
  {
    return d().getInt("size", 0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */