package com.truecaller.old.data.access;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.log.d;
import com.truecaller.old.data.entity.c;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class e
  extends a
{
  private HashMap b;
  
  public e(Context paramContext)
  {
    super(paramContext);
  }
  
  private void a(c paramc)
  {
    super.a(paramc);
    HashMap localHashMap = null;
    Object localObject1 = new String[0];
    boolean bool1 = true;
    AssertionUtil.OnlyInDebug.isTrue(bool1, (String[])localObject1);
    try
    {
      int i = e();
      Object localObject2 = c.class;
      localObject1 = a((Class)localObject2, i);
      localObject1 = (c)localObject1;
      long l1 = a;
      long l2 = a;
      boolean bool2 = l1 < l2;
      if (!bool2)
      {
        bool2 = true;
      }
      else
      {
        bool2 = false;
        localObject1 = null;
      }
      localObject2 = new String[0];
      AssertionUtil.OnlyInDebug.isTrue(bool2, (String[])localObject2);
      localObject1 = c.class;
      int k = e();
      localObject1 = a((Class)localObject1, k);
      localObject1 = (c)localObject1;
      l1 = a;
      l2 = a;
      bool2 = l1 < l2;
      if (bool2) {
        bool1 = false;
      }
      localObject1 = new String[0];
      AssertionUtil.OnlyInDebug.isTrue(bool1, (String[])localObject1);
    }
    catch (Exception localException)
    {
      d.a(localException);
      localObject1 = new String[0];
      AssertionUtil.OnlyInDebug.isTrue(false, (String[])localObject1);
    }
    localHashMap = b;
    if (localHashMap != null)
    {
      long l3 = a;
      paramc = Long.valueOf(l3);
      int j = e();
      localObject1 = Integer.valueOf(j);
      localHashMap.put(paramc, localObject1);
    }
  }
  
  private HashMap f()
  {
    HashMap localHashMap1 = b;
    if (localHashMap1 == null)
    {
      int i = e();
      HashMap localHashMap2 = new java/util/HashMap;
      localHashMap2.<init>();
      b = localHashMap2;
      int j = 1;
      for (;;)
      {
        int k = i + 1;
        if (j >= k) {
          break;
        }
        Object localObject = (c)a(c.class, j);
        if (localObject != null)
        {
          HashMap localHashMap3 = b;
          long l = a;
          localObject = Long.valueOf(l);
          Integer localInteger = Integer.valueOf(j);
          localHashMap3.put(localObject, localInteger);
        }
        j += 1;
      }
    }
    return b;
  }
  
  public final c a(long paramLong)
  {
    HashMap localHashMap = f();
    Object localObject = Long.valueOf(paramLong);
    localObject = (Integer)localHashMap.get(localObject);
    c localc;
    if (localObject != null)
    {
      int i = ((Integer)localObject).intValue();
      localc = (c)a(c.class, i);
    }
    else
    {
      localc = null;
    }
    if (localc != null)
    {
      int j = ((Integer)localObject).intValue();
      p = j;
    }
    return localc;
  }
  
  public final c a(long paramLong, String paramString, int paramInt)
  {
    Object localObject1 = d();
    try
    {
      Object localObject2 = f();
      Object localObject3 = Long.valueOf(paramLong);
      localObject2 = ((HashMap)localObject2).get(localObject3);
      localObject2 = (Integer)localObject2;
      localObject3 = null;
      if (localObject2 != null)
      {
        localObject3 = c.class;
        int i = ((Integer)localObject2).intValue();
        localObject3 = a((Class)localObject3, i);
        localObject3 = (c)localObject3;
      }
      if (localObject3 != null)
      {
        n = paramString;
        o = paramInt;
        localObject1 = ((SharedPreferences)localObject1).edit();
        int j = ((Integer)localObject2).intValue();
        localObject2 = Integer.toString(j);
        String str = ((c)localObject3).a();
        ((SharedPreferences.Editor)localObject1).putString((String)localObject2, str);
        a((SharedPreferences.Editor)localObject1);
        return (c)localObject3;
      }
    }
    catch (Exception localException)
    {
      d.a(localException);
      c localc = new com/truecaller/old/data/entity/c;
      localc.<init>();
      a = paramLong;
      b = "";
      c = "";
      j = "";
      k = "";
      l = "";
      n = paramString;
      o = paramInt;
      m = "";
      a(localc);
      return localc;
    }
  }
  
  protected final String a()
  {
    return "TC.meta.2.90";
  }
  
  public final void a(List paramList)
  {
    int i = e();
    super.a(paramList);
    Object localObject = b;
    if (localObject != null)
    {
      paramList = paramList.iterator();
      for (;;)
      {
        bool = paramList.hasNext();
        if (!bool) {
          break;
        }
        localObject = (c)paramList.next();
        i += 1;
        HashMap localHashMap = b;
        long l = a;
        localObject = Long.valueOf(l);
        Integer localInteger = Integer.valueOf(i);
        localHashMap.put(localObject, localInteger);
      }
      int j = e();
      boolean bool = false;
      localObject = null;
      if (j == i)
      {
        j = 1;
      }
      else
      {
        j = 0;
        paramList = null;
      }
      String[] arrayOfString = new String[0];
      AssertionUtil.OnlyInDebug.isTrue(j, arrayOfString);
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    super.a(paramBoolean);
    b = null;
  }
  
  public final void b()
  {
    super.b();
    b = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */