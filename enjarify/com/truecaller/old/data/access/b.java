package com.truecaller.old.data.access;

import android.content.Context;
import android.text.TextUtils;
import com.google.gson.i;
import com.google.gson.l;
import com.google.gson.o;
import com.google.gson.q;
import com.truecaller.log.d;
import com.truecaller.old.data.entity.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public abstract class b
  extends c
{
  public static final Object a;
  private static final Object c;
  private static final Map d;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    c = localObject;
    localObject = new java/lang/Object;
    localObject.<init>();
    a = localObject;
    localObject = new java/util/TreeMap;
    ((TreeMap)localObject).<init>();
    d = (Map)localObject;
  }
  
  public b(Context arg1)
  {
    super(???);
    synchronized (c)
    {
      Object localObject1 = d;
      Object localObject3 = d();
      boolean bool = ((Map)localObject1).containsKey(localObject3);
      if (!bool)
      {
        localObject1 = f();
        localObject3 = d;
        String str = d();
        ((Map)localObject3).put(str, localObject1);
      }
      return;
    }
  }
  
  private List f()
  {
    Object localObject1 = "LIST";
    try
    {
      localObject1 = a((String)localObject1);
      boolean bool = TextUtils.isEmpty((CharSequence)localObject1);
      if (!bool)
      {
        localObject1 = q.a((String)localObject1);
        localObject1 = ((l)localObject1).j();
        localObject2 = new java/util/ArrayList;
        ((ArrayList)localObject2).<init>();
        int i = 0;
        int j = ((i)localObject1).a();
        while (i < j)
        {
          Object localObject3 = ((i)localObject1).a(i);
          localObject3 = ((l)localObject3).i();
          localObject3 = a((o)localObject3);
          ((List)localObject2).add(localObject3);
          i += 1;
        }
        return (List)localObject2;
      }
    }
    catch (Exception localException)
    {
      Object localObject2 = "DAO Error on reading";
      d.a(localException, (String)localObject2);
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      return localArrayList;
    }
  }
  
  protected abstract a a(o paramo);
  
  public final List a()
  {
    Map localMap = d;
    String str = d();
    return (List)localMap.get(str);
  }
  
  public final void a(List paramList)
  {
    synchronized (a)
    {
      List localList = a();
      localList.removeAll(paramList);
      c();
      return;
    }
  }
  
  public final void b()
  {
    synchronized (a)
    {
      List localList = a();
      localList.clear();
      e();
      return;
    }
  }
  
  public final void c()
  {
    Object localObject = a();
    i locali = new com/google/gson/i;
    locali.<init>();
    localObject = ((List)localObject).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject).hasNext();
      if (!bool) {
        break;
      }
      o localo = ((a)((Iterator)localObject).next()).a();
      locali.a(localo);
    }
    localObject = locali.toString();
    a("LIST", (String)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */