package com.truecaller.old.data.access;

import android.text.TextUtils;
import org.c.a.a.a.e;

public enum Settings$BuildName
{
  public static final String NATIVE_INTEGRATION_SUFFIX = "_NATIVE";
  public final String packageName;
  
  static
  {
    Object localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("GOOGLE_PLAY", 0, "");
    GOOGLE_PLAY = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    int i = 1;
    ((BuildName)localObject).<init>("SAMSUNG", i, "");
    SAMSUNG = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    int j = 2;
    ((BuildName)localObject).<init>("AMAZON", j, "");
    AMAZON = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    int k = 3;
    ((BuildName)localObject).<init>("TC_SHARED", k, "");
    TC_SHARED = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    int m = 4;
    ((BuildName)localObject).<init>("ASUSTEK", m, "com.truecaller.partner.asustek");
    ASUSTEK = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    int n = 5;
    ((BuildName)localObject).<init>("ASUSPAD", n, "com.truecaller.partner.asuspad");
    ASUSPAD = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    int i1 = 6;
    ((BuildName)localObject).<init>("ALCATEL", i1, "com.truecaller.partner.alcatel");
    ALCATEL = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    int i2 = 7;
    ((BuildName)localObject).<init>("APPLOADED", i2, "com.truecaller.partner.apploaded");
    APPLOADED = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    int i3 = 8;
    ((BuildName)localObject).<init>("CONPLEX", i3, "com.truecaller.partner.conplex");
    CONPLEX = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    int i4 = 9;
    ((BuildName)localObject).<init>("ETISALAT", i4, "com.truecaller.partner.etisalat");
    ETISALAT = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    int i5 = 10;
    ((BuildName)localObject).<init>("XTOUCH", i5, "com.truecaller.partner.xtouch");
    XTOUCH = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    int i6 = 11;
    ((BuildName)localObject).<init>("FLY", i6, "com.truecaller.partner.fly");
    FLY = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("PRESTIGIO", 12, "com.truecaller.partner.prestigio");
    PRESTIGIO = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("OLKYA", 13, "com.truecaller.partner.olkya");
    OLKYA = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("HUAWEI", 14, "com.truecaller.partner.huawei");
    HUAWEI = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("WILEYFOX", 15, "com.truecaller.partner.wileyfox");
    WILEYFOX = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("MICROMAX_INDIA", 16, "com.truecaller.partner.micromax_india");
    MICROMAX_INDIA = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("YU_INDIA", 17, "com.truecaller.partner.yu_india");
    YU_INDIA = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("MICROMAX_RUSSIA", 18, "com.truecaller.partner.micromax_russia");
    MICROMAX_RUSSIA = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("KARBONN", 19, "com.truecaller.partner.karbonn");
    KARBONN = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("KARBONN_UTL", 20, "com.truecaller.partner.karbonnutl");
    KARBONN_UTL = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("CELKON", 21, "com.truecaller.partner.celkon");
    CELKON = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("GIONEE", 22, "com.truecaller.partner.gionee");
    GIONEE = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("GIONEE_NEW", 23, "com.truecaller.partner.gionee_new");
    GIONEE_NEW = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("LENOVO", 24, "com.truecaller.partner.lenovo");
    LENOVO = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("LENOVO_NEW", 25, "com.truecaller.partner.lenovo_new");
    LENOVO_NEW = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("INTEX", 26, "com.truecaller.partner.intex");
    INTEX = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("OBI", 27, "com.truecaller.partner.obi");
    OBI = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("TECNO", 28, "com.truecaller.partner.tecno");
    TECNO = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("PANASONIC", 29, "com.truecaller.partner.panasonic");
    PANASONIC = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("LAVA", 30, "com.truecaller.partner.lava");
    LAVA = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("LG", 31, "com.truecaller.partner.lg");
    LG = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("AIRTEL", 32, "com.truecaller.partner.airtel");
    AIRTEL = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("MOVICEL", 33, "com.truecaller.partner.movicel");
    MOVICEL = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("BLU", 34, "com.truecaller.partner.blu");
    BLU = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("HTC", 35, "com.truecaller.partner.htc");
    HTC = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("ACER", 36, "com.truecaller.partner.acer");
    ACER = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("ZTE", 37, "com.truecaller.partner.zte");
    ZTE = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("VIVO", 38, "com.truecaller.partner.vivo");
    VIVO = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("KAZAM", 39, "com.truecaller.partner.kazam");
    KAZAM = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("IBALL", 40, "com.truecaller.partner.iball");
    IBALL = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("PHICOMM", 41, "com.truecaller.partner.phicomm");
    PHICOMM = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("SONY", 42, "com.truecaller.partner.sony");
    SONY = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("SKY", 43, "com.truecaller.partner.sky");
    SKY = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("POSH", 44, "com.truecaller.partner.posh");
    POSH = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("INFINIX", 45, "com.truecaller.partner.infinix");
    INFINIX = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("INFOCUS", 46, "com.truecaller.partner.infocus");
    INFOCUS = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("FOX_MOBILES", 47, "com.truecaller.partner.fox_mobiles");
    FOX_MOBILES = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("SWIPE", 48, "com.truecaller.partner.swipe");
    SWIPE = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("DATAWIND", 49, "com.truecaller.partner.datawind");
    DATAWIND = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("IMG", 50, "com.truecaller.partner.img");
    IMG = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("DAHL", 51, "com.truecaller.partner.dahl");
    DAHL = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("IVOOMI", 52, "com.truecaller.partner.ivoomi");
    IVOOMI = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("ZEN", 53, "com.truecaller.partner.zen");
    ZEN = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("OS_360", 54, "com.truecaller.partner.os360");
    OS_360 = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("ARCHOS", 55, "com.truecaller.partner.archos");
    ARCHOS = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("DIGITAL_TURBINE", 56, "com.truecaller.partner.digital_turbine");
    DIGITAL_TURBINE = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("MOBIISTAR", 57, "com.truecaller.partner.mobiistar");
    MOBIISTAR = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("HOMTOM_INDIA", 58, "com.truecaller.partner.homtom_india");
    HOMTOM_INDIA = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("ENTEL", 59, "com.truecaller.partner.entel");
    ENTEL = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("CARRIER_OI", 60, "com.truecaller.partner.carrier_oi");
    CARRIER_OI = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("CARRIER_VIVO", 61, "com.truecaller.partner.carrier_vivo");
    CARRIER_VIVO = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("CARRIER_TELENOR", 62, "com.truecaller.partner.carrier_telenor");
    CARRIER_TELENOR = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("CARRIER_TIM", 63, "com.truecaller.partner.carrier_tim");
    CARRIER_TIM = (BuildName)localObject;
    localObject = new com/truecaller/old/data/access/Settings$BuildName;
    ((BuildName)localObject).<init>("CARRIER_CLARO", 64, "com.truecaller.partner.carrier_claro");
    CARRIER_CLARO = (BuildName)localObject;
    localObject = new BuildName[65];
    BuildName localBuildName = GOOGLE_PLAY;
    localObject[0] = localBuildName;
    localBuildName = SAMSUNG;
    localObject[i] = localBuildName;
    localBuildName = AMAZON;
    localObject[j] = localBuildName;
    localBuildName = TC_SHARED;
    localObject[k] = localBuildName;
    localBuildName = ASUSTEK;
    localObject[m] = localBuildName;
    localBuildName = ASUSPAD;
    localObject[n] = localBuildName;
    localBuildName = ALCATEL;
    localObject[i1] = localBuildName;
    localBuildName = APPLOADED;
    localObject[i2] = localBuildName;
    localBuildName = CONPLEX;
    localObject[i3] = localBuildName;
    localBuildName = ETISALAT;
    localObject[i4] = localBuildName;
    localBuildName = XTOUCH;
    localObject[i5] = localBuildName;
    localBuildName = FLY;
    localObject[i6] = localBuildName;
    localBuildName = PRESTIGIO;
    localObject[12] = localBuildName;
    localBuildName = OLKYA;
    localObject[13] = localBuildName;
    localBuildName = HUAWEI;
    localObject[14] = localBuildName;
    localBuildName = WILEYFOX;
    localObject[15] = localBuildName;
    localBuildName = MICROMAX_INDIA;
    localObject[16] = localBuildName;
    localBuildName = YU_INDIA;
    localObject[17] = localBuildName;
    localBuildName = MICROMAX_RUSSIA;
    localObject[18] = localBuildName;
    localBuildName = KARBONN;
    localObject[19] = localBuildName;
    localBuildName = KARBONN_UTL;
    localObject[20] = localBuildName;
    localBuildName = CELKON;
    localObject[21] = localBuildName;
    localBuildName = GIONEE;
    localObject[22] = localBuildName;
    localBuildName = GIONEE_NEW;
    localObject[23] = localBuildName;
    localBuildName = LENOVO;
    localObject[24] = localBuildName;
    localBuildName = LENOVO_NEW;
    localObject[25] = localBuildName;
    localBuildName = INTEX;
    localObject[26] = localBuildName;
    localBuildName = OBI;
    localObject[27] = localBuildName;
    localBuildName = TECNO;
    localObject[28] = localBuildName;
    localBuildName = PANASONIC;
    localObject[29] = localBuildName;
    localBuildName = LAVA;
    localObject[30] = localBuildName;
    localBuildName = LG;
    localObject[31] = localBuildName;
    localBuildName = AIRTEL;
    localObject[32] = localBuildName;
    localBuildName = MOVICEL;
    localObject[33] = localBuildName;
    localBuildName = BLU;
    localObject[34] = localBuildName;
    localBuildName = HTC;
    localObject[35] = localBuildName;
    localBuildName = ACER;
    localObject[36] = localBuildName;
    localBuildName = ZTE;
    localObject[37] = localBuildName;
    localBuildName = VIVO;
    localObject[38] = localBuildName;
    localBuildName = KAZAM;
    localObject[39] = localBuildName;
    localBuildName = IBALL;
    localObject[40] = localBuildName;
    localBuildName = PHICOMM;
    localObject[41] = localBuildName;
    localBuildName = SONY;
    localObject[42] = localBuildName;
    localBuildName = SKY;
    localObject[43] = localBuildName;
    localBuildName = POSH;
    localObject[44] = localBuildName;
    localBuildName = INFINIX;
    localObject[45] = localBuildName;
    localBuildName = INFOCUS;
    localObject[46] = localBuildName;
    localBuildName = FOX_MOBILES;
    localObject[47] = localBuildName;
    localBuildName = SWIPE;
    localObject[48] = localBuildName;
    localBuildName = DATAWIND;
    localObject[49] = localBuildName;
    localBuildName = IMG;
    localObject[50] = localBuildName;
    localBuildName = DAHL;
    localObject[51] = localBuildName;
    localBuildName = IVOOMI;
    localObject[52] = localBuildName;
    localBuildName = ZEN;
    localObject[53] = localBuildName;
    localBuildName = OS_360;
    localObject[54] = localBuildName;
    localBuildName = ARCHOS;
    localObject[55] = localBuildName;
    localBuildName = DIGITAL_TURBINE;
    localObject[56] = localBuildName;
    localBuildName = MOBIISTAR;
    localObject[57] = localBuildName;
    localBuildName = HOMTOM_INDIA;
    localObject[58] = localBuildName;
    localBuildName = ENTEL;
    localObject[59] = localBuildName;
    localBuildName = CARRIER_OI;
    localObject[60] = localBuildName;
    localBuildName = CARRIER_VIVO;
    localObject[61] = localBuildName;
    localBuildName = CARRIER_TELENOR;
    localObject[62] = localBuildName;
    localBuildName = CARRIER_TIM;
    localObject[63] = localBuildName;
    localBuildName = CARRIER_CLARO;
    localObject[64] = localBuildName;
    $VALUES = (BuildName[])localObject;
  }
  
  private Settings$BuildName(String paramString1)
  {
    packageName = paramString1;
  }
  
  public static BuildName toBuildName(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool) {
      return null;
    }
    String str = "";
    paramString = paramString.replace("_NATIVE", str);
    Class localClass = BuildName.class;
    bool = e.a(localClass, paramString);
    if (!bool) {
      return null;
    }
    return valueOf(paramString);
  }
  
  public final String getPackageName()
  {
    return packageName;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.Settings.BuildName
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */