package com.truecaller.old.data.access;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.truecaller.content.c.v;
import com.truecaller.content.c.w;

public abstract class c
{
  private boolean a;
  protected final Context b;
  
  public c(Context paramContext)
  {
    boolean bool = Settings.b();
    a = bool;
    paramContext = paramContext.getApplicationContext();
    b = paramContext;
  }
  
  private SharedPreferences a()
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("truecaller.data.");
    Object localObject2 = d();
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localObject2 = v.a(b, (String)localObject1);
    Context localContext = b;
    String str = "TC.settings.3.0.beta5";
    boolean bool = w.a(localContext, str);
    if (bool)
    {
      localContext = b;
      str = null;
      localObject1 = localContext.getSharedPreferences((String)localObject1, 0);
      w.a((SharedPreferences)localObject1, (SharedPreferences)localObject2);
      localObject1 = ((SharedPreferences)localObject1).edit().clear();
      ((SharedPreferences.Editor)localObject1).commit();
    }
    return (SharedPreferences)localObject2;
  }
  
  private void a(SharedPreferences.Editor paramEditor)
  {
    boolean bool = a;
    if (bool)
    {
      paramEditor.commit();
      return;
    }
    paramEditor.apply();
  }
  
  protected final String a(String paramString)
  {
    return a().getString(paramString, "");
  }
  
  protected final void a(String paramString, long paramLong)
  {
    SharedPreferences.Editor localEditor = a().edit();
    localEditor.putLong(paramString, paramLong);
    a(localEditor);
  }
  
  protected final void a(String paramString1, String paramString2)
  {
    SharedPreferences.Editor localEditor = a().edit();
    localEditor.putString(paramString1, paramString2);
    a(localEditor);
  }
  
  protected final Long b(String paramString)
  {
    return Long.valueOf(a().getLong(paramString, 0L));
  }
  
  protected abstract String d();
  
  protected final void e()
  {
    SharedPreferences.Editor localEditor = a().edit();
    localEditor.clear();
    a(localEditor);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */