package com.truecaller.old.data.access;

import android.content.Context;
import com.google.gson.o;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.log.d;
import com.truecaller.network.notification.NotificationScope;
import com.truecaller.network.notification.NotificationType;
import com.truecaller.network.notification.c.a;
import com.truecaller.network.notification.c.a.b;
import com.truecaller.notifications.ah;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.old.data.entity.Notification.NotificationState;
import com.truecaller.service.WidgetListProvider;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

public final class f
  extends h
{
  public f(Context paramContext)
  {
    super(paramContext);
  }
  
  private static String a(NotificationScope paramNotificationScope)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("LAST_ID_");
    int i = value;
    localStringBuilder.append(i);
    return localStringBuilder.toString();
  }
  
  private void a(Map paramMap)
  {
    paramMap = paramMap.entrySet().iterator();
    for (;;)
    {
      boolean bool = paramMap.hasNext();
      if (!bool) {
        break;
      }
      Object localObject1 = (Map.Entry)paramMap.next();
      Object localObject2 = (NotificationScope)((Map.Entry)localObject1).getKey();
      localObject1 = (Long)((Map.Entry)localObject1).getValue();
      localObject2 = a((NotificationScope)localObject2);
      long l = ((Long)localObject1).longValue();
      a((String)localObject2, l);
    }
  }
  
  private static Notification b(o paramo)
  {
    Notification localNotification;
    try
    {
      localNotification = new com/truecaller/old/data/entity/Notification;
      localNotification.<init>(paramo);
    }
    finally
    {
      d.a(paramo);
      localNotification = null;
    }
    return localNotification;
  }
  
  public static Collection b(Collection paramCollection)
  {
    TreeSet localTreeSet = new java/util/TreeSet;
    localTreeSet.<init>();
    paramCollection = paramCollection.iterator();
    for (;;)
    {
      boolean bool1 = paramCollection.hasNext();
      if (!bool1) {
        break;
      }
      Notification localNotification = (Notification)paramCollection.next();
      boolean bool2 = localNotification.r();
      if (bool2) {
        localTreeSet.add(localNotification);
      }
    }
    return localTreeSet;
  }
  
  protected final int a()
  {
    return 100;
  }
  
  public final int a(Collection paramCollection, Boolean paramBoolean)
  {
    boolean bool1 = paramBoolean.booleanValue();
    if (bool1)
    {
      paramBoolean = new java/util/HashMap;
      paramBoolean.<init>();
      Iterator localIterator = paramCollection.iterator();
      for (;;)
      {
        boolean bool2 = localIterator.hasNext();
        if (!bool2) {
          break;
        }
        Object localObject1 = (Notification)localIterator.next();
        Object localObject2 = a.a.c;
        localObject2 = (Long)paramBoolean.get(localObject2);
        if (localObject2 != null)
        {
          long l1 = ((Long)localObject2).longValue();
          c.a.b localb = a.a;
          long l2 = a;
          boolean bool3 = l1 < l2;
          if (!bool3) {}
        }
        else
        {
          localObject2 = a.a.c;
          long l3 = a.a.a;
          localObject1 = Long.valueOf(l3);
          paramBoolean.put(localObject2, localObject1);
        }
      }
      a(paramBoolean);
    }
    return super.f(paramCollection);
  }
  
  public final Collection a(Collection paramCollection)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    int i = paramCollection.size();
    localArrayList.<init>(i);
    ah localah = TrueApp.y().a().aK();
    paramCollection = paramCollection.iterator();
    for (;;)
    {
      boolean bool = paramCollection.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = (c.a)paramCollection.next();
      Notification localNotification = new com/truecaller/old/data/entity/Notification;
      localNotification.<init>((c.a)localObject);
      localObject = localNotification.b();
      NotificationType localNotificationType = NotificationType.GENERIC_WEBVIEW;
      if (localObject == localNotificationType)
      {
        localObject = localNotification.a("u");
        localObject = localah.b((String)localObject);
        if (localObject != null)
        {
          localObject = localah.a((String)localObject);
          d = ((String)localObject);
          localArrayList.add(localNotification);
        }
      }
      else
      {
        localArrayList.add(localNotification);
      }
    }
    return d(localArrayList);
  }
  
  public final void a(Collection paramCollection, Notification.NotificationState paramNotificationState)
  {
    Iterator localIterator = paramCollection.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Notification localNotification = (Notification)localIterator.next();
      b = paramNotificationState;
    }
    e(paramCollection);
  }
  
  protected final Collection b()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    f.1 local1 = new com/truecaller/old/data/access/f$1;
    local1.<init>(this);
    localArrayList.add(local1);
    return localArrayList;
  }
  
  protected final h.b c()
  {
    return -..Lambda.f.J9ZUvgGuOH0ZHSidmjIaJfX-tzU.INSTANCE;
  }
  
  public final void c(Collection paramCollection)
  {
    Notification.NotificationState localNotificationState = Notification.NotificationState.VIEWED;
    a(paramCollection, localNotificationState);
  }
  
  protected final String d()
  {
    return "Notifications";
  }
  
  public final int f()
  {
    Iterator localIterator = b(l()).iterator();
    int i = 0;
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Notification.NotificationState localNotificationState1 = nextb;
      Notification.NotificationState localNotificationState2 = Notification.NotificationState.NEW;
      if (localNotificationState1 == localNotificationState2) {
        i += 1;
      }
    }
    return i;
  }
  
  public final Map g()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    NotificationScope[] arrayOfNotificationScope = NotificationScope.values();
    int i = arrayOfNotificationScope.length;
    int j = 0;
    while (j < i)
    {
      NotificationScope localNotificationScope = arrayOfNotificationScope[j];
      Object localObject = a(localNotificationScope);
      localObject = b((String)localObject);
      localHashMap.put(localNotificationScope, localObject);
      j += 1;
    }
    return localHashMap;
  }
  
  public final Collection h()
  {
    return b(l());
  }
  
  public final Notification i()
  {
    NotificationType localNotificationType1 = NotificationType.SOFTWARE_UPDATE;
    List localList = l();
    Iterator localIterator = localList.iterator();
    ArrayList localArrayList = null;
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Notification localNotification = (Notification)localIterator.next();
      NotificationType localNotificationType2 = localNotification.b();
      if (localNotificationType2 == localNotificationType1)
      {
        if (localArrayList == null)
        {
          localArrayList = new java/util/ArrayList;
          int i = localList.size();
          localArrayList.<init>(i);
        }
        localArrayList.add(localNotification);
      }
    }
    if (localArrayList != null)
    {
      int j = localArrayList.size();
      if (j > 0) {
        return (Notification)localArrayList.get(0);
      }
    }
    return null;
  }
  
  protected final void j()
  {
    WidgetListProvider.a(b);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */