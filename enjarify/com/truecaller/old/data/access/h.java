package com.truecaller.old.data.access;

import android.content.Context;
import android.text.TextUtils;
import com.google.gson.i;
import com.google.gson.l;
import com.google.gson.o;
import com.google.gson.q;
import com.truecaller.log.d;
import com.truecaller.old.data.entity.f;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

public abstract class h
  extends c
{
  private static final Object a;
  private static final Object c;
  private static Map d;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    a = localObject;
    localObject = new java/lang/Object;
    localObject.<init>();
    c = localObject;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    d = (Map)localObject;
  }
  
  public h(Context arg1)
  {
    super(???);
    synchronized (a)
    {
      Map localMap = d;
      String str = d();
      boolean bool = localMap.containsKey(str);
      if (!bool)
      {
        localMap = d;
        str = d();
        TreeSet localTreeSet = f();
        localMap.put(str, localTreeSet);
      }
      return;
    }
  }
  
  private static Collection a(Collection paramCollection1, Collection paramCollection2)
  {
    TreeSet localTreeSet = new java/util/TreeSet;
    localTreeSet.<init>();
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    Object localObject1 = paramCollection2.iterator();
    boolean bool1;
    Object localObject2;
    Object localObject3;
    for (;;)
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (h.a)((Iterator)localObject1).next();
      localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>();
      localHashMap.put(localObject2, localObject3);
    }
    paramCollection1 = paramCollection1.iterator();
    for (;;)
    {
      boolean bool2 = paramCollection1.hasNext();
      if (!bool2) {
        break;
      }
      localObject1 = (f)paramCollection1.next();
      bool1 = false;
      localObject2 = null;
      localObject3 = paramCollection2.iterator();
      h.a locala;
      boolean bool4;
      do
      {
        boolean bool3 = ((Iterator)localObject3).hasNext();
        if (!bool3) {
          break;
        }
        locala = (h.a)((Iterator)localObject3).next();
        bool4 = locala.a(localObject1);
      } while (!bool4);
      localObject2 = (ArrayList)localHashMap.get(locala);
      ((ArrayList)localObject2).add(localObject1);
      bool1 = true;
      if (!bool1) {
        localTreeSet.add(localObject1);
      }
    }
    paramCollection1 = paramCollection2.iterator();
    for (;;)
    {
      boolean bool5 = paramCollection1.hasNext();
      if (!bool5) {
        break;
      }
      paramCollection2 = (h.a)paramCollection1.next();
      localObject1 = (ArrayList)localHashMap.get(paramCollection2);
      bool1 = ((ArrayList)localObject1).isEmpty();
      if (!bool1)
      {
        paramCollection2 = (f)paramCollection2.a((Collection)localObject1);
        localTreeSet.add(paramCollection2);
      }
    }
    return localTreeSet;
  }
  
  private TreeSet a(i parami)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    if (parami != null)
    {
      int i = 0;
      int j = parami.a();
      while (i < j)
      {
        Object localObject = parami.a(i).i();
        localObject = a((o)localObject);
        if (localObject != null) {
          localArrayList.add(localObject);
        }
        i += 1;
      }
    }
    parami = new java/util/TreeSet;
    parami.<init>(localArrayList);
    return parami;
  }
  
  private TreeSet f()
  {
    TreeSet localTreeSet = new java/util/TreeSet;
    localTreeSet.<init>();
    Object localObject = "LIST";
    try
    {
      localObject = a((String)localObject);
      boolean bool = TextUtils.isEmpty((CharSequence)localObject);
      if (!bool)
      {
        localObject = q.a((String)localObject);
        localObject = ((l)localObject).j();
        localTreeSet = a((i)localObject);
      }
    }
    catch (Exception localException)
    {
      d.a(localException);
    }
    return localTreeSet;
  }
  
  private TreeSet g()
  {
    synchronized (c)
    {
      Object localObject2 = d;
      String str = d();
      localObject2 = ((Map)localObject2).get(str);
      localObject2 = (TreeSet)localObject2;
      return (TreeSet)localObject2;
    }
  }
  
  protected int a()
  {
    return 0;
  }
  
  protected abstract f a(o paramo);
  
  protected Collection b()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    return localArrayList;
  }
  
  protected h.b c()
  {
    h.1 local1 = new com/truecaller/old/data/access/h$1;
    local1.<init>(this);
    return local1;
  }
  
  protected final Collection d(Collection paramCollection)
  {
    h.b localb = c();
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramCollection = paramCollection.iterator();
    for (;;)
    {
      boolean bool1 = paramCollection.hasNext();
      if (!bool1) {
        break;
      }
      f localf = (f)paramCollection.next();
      boolean bool2 = localb.apply(localf);
      if (bool2) {
        localArrayList.add(localf);
      }
    }
    paramCollection = b();
    return a(localArrayList, paramCollection);
  }
  
  public final void e(Collection paramCollection)
  {
    synchronized (c)
    {
      TreeSet localTreeSet = g();
      localTreeSet.removeAll(paramCollection);
      f(paramCollection);
      return;
    }
  }
  
  public final int f(Collection paramCollection)
  {
    Object localObject1 = c;
    int i = 0;
    try
    {
      ??? = g();
      Object localObject3 = paramCollection.iterator();
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject3).hasNext();
        if (!bool1) {
          break;
        }
        Object localObject4 = ((Iterator)localObject3).next();
        bool1 = ((TreeSet)???).contains(localObject4);
        if (!bool1) {
          i += 1;
        }
      }
      if (i > 0)
      {
        ((TreeSet)???).addAll(paramCollection);
        paramCollection = new java/util/TreeSet;
        ??? = g();
        ??? = d((Collection)???);
        paramCollection.<init>((Collection)???);
        for (;;)
        {
          int j = a();
          if (j <= 0) {
            break;
          }
          j = paramCollection.size();
          int k = a();
          if (j <= k) {
            break;
          }
          paramCollection.pollLast();
        }
        synchronized (c)
        {
          localObject3 = g();
          ((TreeSet)localObject3).clear();
          ((TreeSet)localObject3).addAll(paramCollection);
          ??? = new com/google/gson/i;
          ((i)???).<init>();
          paramCollection = paramCollection.iterator();
          for (;;)
          {
            boolean bool2 = paramCollection.hasNext();
            if (!bool2) {
              break;
            }
            localObject3 = paramCollection.next();
            localObject3 = (f)localObject3;
            try
            {
              localObject3 = ((f)localObject3).s();
              ((i)???).a((l)localObject3);
            }
            finally
            {
              d.a(localThrowable);
            }
          }
          paramCollection = "LIST";
          ??? = ((i)???).toString();
          a(paramCollection, (String)???);
          j();
        }
      }
      return i;
    }
    finally {}
  }
  
  protected void j() {}
  
  public final void k()
  {
    synchronized (c)
    {
      TreeSet localTreeSet = g();
      localTreeSet.clear();
      e();
      j();
      return;
    }
  }
  
  public final List l()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    TreeSet localTreeSet = g();
    localArrayList.<init>(localTreeSet);
    return localArrayList;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */