package com.truecaller.old.data.access;

import com.truecaller.TrueApp;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.bp;
import com.truecaller.common.b.e;
import com.truecaller.common.tag.sync.AvailableTagsDownloadWorker;
import com.truecaller.log.d;

public final class d$a
  implements Runnable
{
  private final TrueApp a;
  
  public d$a(com.truecaller.common.b.a parama)
  {
    parama = (TrueApp)parama;
    a = parama;
  }
  
  public final void run()
  {
    try
    {
      Object localObject1 = a;
      boolean bool = ((TrueApp)localObject1).p();
      if (!bool) {
        return;
      }
      localObject1 = a;
      localObject1 = ((TrueApp)localObject1).a();
      localObject1 = ((bp)localObject1).aZ();
      localObject1 = ((f)localObject1).a();
      localObject1 = (com.truecaller.config.a)localObject1;
      localObject1 = ((com.truecaller.config.a)localObject1).b();
      ((w)localObject1).d();
      localObject1 = "tagsEntityTag";
      localObject2 = null;
      e.b((String)localObject1, null);
      AvailableTagsDownloadWorker.e();
      return;
    }
    catch (InterruptedException localInterruptedException) {}catch (RuntimeException localRuntimeException) {}
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Error updating language, language=");
    String str = Settings.b("language");
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    d.a(localRuntimeException, (String)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.access.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */