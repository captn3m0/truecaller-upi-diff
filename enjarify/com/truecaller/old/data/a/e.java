package com.truecaller.old.data.a;

import android.text.TextUtils;
import com.google.gson.f;
import com.google.gson.g;
import com.google.gson.i;
import com.google.gson.l;
import com.truecaller.common.h.ab;
import com.truecaller.common.tag.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public final class e
{
  private static final Comparator l = -..Lambda.e.fXgX5Izgy6M2xGnmw4Vp4pPj7co.INSTANCE;
  public final int a;
  public final int b;
  public final String c;
  public final String d;
  public final String e;
  public final String f;
  public final long g;
  public final Set h;
  public final Set i;
  public final Set j;
  public final Set k;
  private final String m;
  private final ArrayList n;
  
  public e(b paramb, com.truecaller.common.tag.b paramb1, boolean paramBoolean1, boolean paramBoolean2)
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    n = ((ArrayList)localObject);
    localObject = new java/util/TreeSet;
    ((TreeSet)localObject).<init>();
    h = ((Set)localObject);
    localObject = new java/util/TreeSet;
    ((TreeSet)localObject).<init>();
    i = ((Set)localObject);
    localObject = new java/util/TreeSet;
    ((TreeSet)localObject).<init>();
    j = ((Set)localObject);
    localObject = new java/util/TreeSet;
    ((TreeSet)localObject).<init>();
    k = ((Set)localObject);
    localObject = c;
    if (localObject != null)
    {
      localObject = c;
      i1 = o;
    }
    else
    {
      i1 = -1;
    }
    b = i1;
    int i1 = b;
    android.support.v4.f.o localo = new android/support/v4/f/o;
    localo.<init>();
    String str = b;
    paramb1.a(str, i1, localo);
    a(localo);
    int i2 = localo.c();
    if (i2 != 0)
    {
      i2 = 0;
      paramb1 = (a)localo.d(0);
      i1 = b;
    }
    a = i1;
    a(paramb);
    if (paramBoolean2)
    {
      paramb1 = e;
      if (paramb1 != null)
      {
        paramb1 = k;
        List localList = e;
        paramb1.addAll(localList);
      }
    }
    long l1 = a;
    g = l1;
    if (paramBoolean1)
    {
      paramb1 = b;
    }
    else
    {
      i2 = 0;
      paramb1 = null;
    }
    c = paramb1;
    paramb1 = c;
    if (paramb1 != null) {
      paramb1 = c.b;
    } else {
      paramb1 = "";
    }
    d = paramb1;
    paramb1 = c;
    if (paramb1 != null) {
      paramb = c.e;
    } else {
      paramb = "";
    }
    e = paramb;
    paramb = new java/lang/StringBuilder;
    paramb.<init>();
    paramb1 = c;
    paramb.append(paramb1);
    paramb.append("§");
    paramb1 = d;
    paramb.append(paramb1);
    paramb.append("§");
    paramb1 = e;
    paramb.append(paramb1);
    paramb.append("§");
    Set localSet = j;
    paramb1 = TextUtils.join(",", localSet);
    paramb.append(paramb1);
    paramb.append("§");
    localSet = i;
    paramb1 = TextUtils.join(",", localSet);
    paramb.append(paramb1);
    paramb.append("§");
    localSet = h;
    paramb1 = TextUtils.join(",", localSet);
    paramb.append(paramb1);
    paramb.append("§");
    localSet = k;
    paramb1 = TextUtils.join(",", localSet);
    paramb.append(paramb1);
    paramb.append("§");
    i2 = a;
    paramb.append(i2);
    paramb = paramb.toString();
    m = paramb;
    paramb = com.truecaller.utils.extensions.o.b(m);
    f = paramb;
  }
  
  private void a(android.support.v4.f.o paramo)
  {
    int i1 = 0;
    ArrayList localArrayList = null;
    for (;;)
    {
      int i2 = paramo.c();
      if (i1 >= i2) {
        break;
      }
      localObject1 = n;
      Object localObject2 = paramo.d(i1);
      ((ArrayList)localObject1).add(localObject2);
      i1 += 1;
    }
    localArrayList = n;
    Object localObject1 = l;
    Collections.sort(localArrayList, (Comparator)localObject1);
    i1 = Math.min(paramo.c(), 3);
    localObject1 = n;
    int i3 = paramo.c();
    ((ArrayList)localObject1).subList(i1, i3).clear();
  }
  
  public static void a(com.google.gson.o paramo, String paramString1, String paramString2)
  {
    boolean bool = TextUtils.isEmpty(paramString2);
    if (!bool) {
      paramo.a(paramString1, paramString2);
    }
  }
  
  public static void a(com.google.gson.o paramo, String paramString, ArrayList paramArrayList)
  {
    boolean bool = paramArrayList.isEmpty();
    if (!bool)
    {
      Object localObject = new com/google/gson/g;
      ((g)localObject).<init>();
      localObject = ((g)localObject).a();
      paramArrayList = ((f)localObject).a(paramArrayList).j();
      paramo.a(paramString, paramArrayList);
    }
  }
  
  private void a(b paramb)
  {
    Object localObject1 = d;
    if (localObject1 != null)
    {
      paramb = d.iterator();
      for (;;)
      {
        boolean bool1 = paramb.hasNext();
        if (!bool1) {
          break;
        }
        localObject1 = (c)paramb.next();
        Object localObject2 = b;
        boolean bool2 = ab.f((String)localObject2);
        if (bool2)
        {
          int i1 = c;
          int i2 = 1;
          if (i1 == i2)
          {
            localObject2 = h;
            localObject1 = b;
            ((Set)localObject2).add(localObject1);
          }
          else
          {
            i1 = c;
            i2 = 2;
            if (i1 == i2)
            {
              localObject2 = i;
              localObject1 = b;
              ((Set)localObject2).add(localObject1);
            }
            else
            {
              i1 = c;
              i2 = 3;
              if (i1 == i2)
              {
                localObject2 = j;
                localObject1 = b;
                ((Set)localObject2).add(localObject1);
              }
            }
          }
        }
      }
    }
  }
  
  public final void a(com.google.gson.o paramo)
  {
    Object localObject1 = n;
    int i1 = ((ArrayList)localObject1).size();
    if (i1 > 0)
    {
      localObject1 = new com/google/gson/i;
      ((i)localObject1).<init>();
      Object localObject2 = n.iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject2).hasNext();
        if (!bool) {
          break;
        }
        localObject3 = (a)((Iterator)localObject2).next();
        localObject4 = new com/google/gson/o;
        ((com.google.gson.o)localObject4).<init>();
        int i2 = a;
        Integer localInteger = Integer.valueOf(i2);
        ((com.google.gson.o)localObject4).a("tagId", localInteger);
        String str = "score";
        double d1 = c;
        localObject3 = Double.valueOf(d1);
        ((com.google.gson.o)localObject4).a(str, (Number)localObject3);
        ((i)localObject1).a((l)localObject4);
      }
      localObject2 = new com/google/gson/o;
      ((com.google.gson.o)localObject2).<init>();
      int i3 = a;
      Object localObject4 = Integer.valueOf(i3);
      ((com.google.gson.o)localObject2).a("version", (Number)localObject4);
      Object localObject3 = "tags";
      ((com.google.gson.o)localObject2).a((String)localObject3, (l)localObject1);
      localObject1 = "autoTag";
      paramo.a((String)localObject1, (l)localObject2);
    }
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Ugc{mAutoTagVersion=");
    int i1 = a;
    localStringBuilder.append(i1);
    localStringBuilder.append(", mMetaDataAutoTagVersion=");
    i1 = b;
    localStringBuilder.append(i1);
    localStringBuilder.append(", mUgcId='");
    String str = f;
    localStringBuilder.append(str);
    localStringBuilder.append('\'');
    localStringBuilder.append(", mContactId=");
    long l1 = g;
    localStringBuilder.append(l1);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */