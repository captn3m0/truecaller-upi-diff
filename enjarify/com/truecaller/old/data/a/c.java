package com.truecaller.old.data.a;

import com.truecaller.common.h.am;

public final class c
{
  public String a;
  public String b;
  public int c;
  public String d;
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    if (paramObject != null)
    {
      boolean bool2 = paramObject instanceof c;
      if (bool2)
      {
        paramObject = (c)paramObject;
        int i = c;
        int j = c;
        if (i != j) {
          return false;
        }
        String str1 = d;
        if (str1 != null)
        {
          String str2 = d;
          boolean bool3 = str1.equals(str2);
          if (bool3) {
            break label94;
          }
        }
        else
        {
          str1 = d;
          if (str1 == null) {
            break label94;
          }
        }
        return false;
        label94:
        str1 = b;
        if (str1 != null)
        {
          str1 = am.d(str1);
          paramObject = am.d(b);
          boolean bool4 = str1.equals(paramObject);
          if (bool4) {
            break label147;
          }
        }
        else
        {
          paramObject = b;
          if (paramObject == null) {
            break label147;
          }
        }
        return false;
        label147:
        return bool1;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    String str1 = b;
    int i = 0;
    if (str1 != null)
    {
      str1 = am.d(str1);
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    int j = (j + 527) * 31;
    int k = c;
    j = (j + k) * 31;
    String str2 = d;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Phone{rawId='");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append('\'');
    localStringBuilder.append(", type=");
    int i = c;
    localStringBuilder.append(i);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */