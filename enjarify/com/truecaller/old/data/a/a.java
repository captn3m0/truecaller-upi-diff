package com.truecaller.old.data.a;

import android.content.Context;
import android.graphics.Bitmap;
import com.truecaller.common.h.am;
import com.truecaller.old.data.entity.c;
import com.truecaller.ui.components.n;
import com.truecaller.util.t;

public abstract class a
  extends n
{
  public long a;
  public String b;
  public c c;
  
  public final String a(Context paramContext)
  {
    return b;
  }
  
  public Bitmap c(Context paramContext)
  {
    long l = a;
    return t.b(paramContext, l);
  }
  
  public final Object q()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    long l = a;
    localStringBuilder.append(l);
    return am.a(localStringBuilder.toString());
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */