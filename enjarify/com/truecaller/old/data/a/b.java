package com.truecaller.old.data.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import com.truecaller.log.d;
import com.truecaller.ui.components.n.c;
import com.truecaller.util.t;
import java.util.List;

public final class b
  extends a
  implements n.c
{
  public List d;
  public List e;
  
  public final String a()
  {
    return b;
  }
  
  public final String b(Context paramContext)
  {
    Object localObject = d;
    if (localObject != null)
    {
      int i = ((List)localObject).size();
      if (i > 0)
      {
        localObject = d;
        bool = false;
        localObject = get0b;
        break label48;
      }
    }
    localObject = "";
    label48:
    boolean bool = TextUtils.isEmpty((CharSequence)localObject);
    if (!bool) {
      return (String)localObject;
    }
    return super.b(paramContext);
  }
  
  public final Bitmap c(Context paramContext)
  {
    long l1 = a;
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    if (!bool1)
    {
      Object localObject = d;
      if (localObject != null)
      {
        boolean bool2 = ((List)localObject).isEmpty();
        if (!bool2)
        {
          localObject = d;
          String str = null;
          localObject = get0b;
          localObject = t.a(paramContext, (String)localObject);
          if (localObject != null) {
            try
            {
              localObject = Long.valueOf((String)localObject);
              l1 = ((Long)localObject).longValue();
              a = l1;
            }
            catch (NumberFormatException localNumberFormatException)
            {
              str = "OldContact.getImage error";
              d.a(localNumberFormatException, str);
            }
          }
        }
      }
    }
    l1 = a;
    return t.b(paramContext, l1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */