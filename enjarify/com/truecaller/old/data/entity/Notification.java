package com.truecaller.old.data.entity;

import android.content.Context;
import android.graphics.Bitmap;
import com.google.gson.b.h;
import com.google.gson.i;
import com.google.gson.l;
import com.google.gson.o;
import com.google.gson.q;
import com.google.gson.r;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.h.am;
import com.truecaller.network.notification.NotificationScope;
import com.truecaller.network.notification.NotificationType;
import com.truecaller.network.notification.c.a;
import com.truecaller.network.notification.c.a.a;
import com.truecaller.network.notification.c.a.b;
import com.truecaller.notifications.ah;
import com.truecaller.ui.components.n.a;
import com.truecaller.ui.components.n.d;
import com.truecaller.util.ap;
import com.truecaller.util.ay;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class Notification
  extends com.truecaller.ui.components.n
  implements f, n.d, Comparable
{
  public c.a a;
  public Notification.NotificationState b;
  public Integer c;
  public String d;
  public List e;
  
  public Notification(o paramo)
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    e = ((List)localObject);
    localObject = a(paramo.c("d"));
    a = ((c.a)localObject);
    localObject = Notification.NotificationState.getFromValue(Integer.valueOf(ay.b("s", paramo)));
    b = ((Notification.NotificationState)localObject);
    localObject = "m";
    boolean bool1 = paramo.a((String)localObject);
    int i;
    if (bool1)
    {
      localObject = "m";
      i = ay.b((String)localObject, paramo);
    }
    else
    {
      i = 1;
    }
    localObject = Integer.valueOf(i);
    c = ((Integer)localObject);
    localObject = "b";
    boolean bool2 = paramo.a((String)localObject);
    if (bool2)
    {
      localObject = paramo.b("b");
      bool2 = localObject instanceof com.google.gson.n;
      if (!bool2)
      {
        localObject = paramo.b("b").c();
        d = ((String)localObject);
      }
    }
    localObject = "a";
    paramo = paramo.b((String)localObject);
    if (paramo != null)
    {
      bool2 = paramo instanceof i;
      if (bool2)
      {
        paramo = a(((l)paramo).j());
        e = paramo;
        return;
      }
      bool2 = paramo instanceof r;
      if (bool2)
      {
        paramo = q.a(((l)paramo).c());
        if (paramo != null)
        {
          bool2 = paramo instanceof i;
          if (bool2)
          {
            paramo = a(((l)paramo).j());
            e = paramo;
            return;
          }
        }
      }
    }
    paramo = new java/util/ArrayList;
    paramo.<init>();
    e = paramo;
  }
  
  public Notification(o paramo, Notification.NotificationState paramNotificationState)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    e = localArrayList;
    paramo = a(paramo);
    a = paramo;
    b = paramNotificationState;
    paramo = Integer.valueOf(1);
    c = paramo;
  }
  
  public Notification(c.a parama)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    e = localArrayList;
    a = parama;
    parama = Notification.NotificationState.NEW;
    b = parama;
    parama = Integer.valueOf(1);
    c = parama;
  }
  
  public Notification(Collection paramCollection, Map paramMap)
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    e = ((List)localObject1);
    boolean bool1 = paramCollection.isEmpty();
    if (!bool1)
    {
      bool1 = false;
      localObject1 = null;
      int i = 0;
      Integer localInteger = Integer.valueOf(0);
      paramCollection = paramCollection.iterator();
      Object localObject2;
      for (;;)
      {
        boolean bool2 = paramCollection.hasNext();
        if (!bool2) {
          break;
        }
        localObject2 = (Notification)paramCollection.next();
        if (localObject1 != null)
        {
          int k = ((Notification)localObject1).a((Notification)localObject2);
          if (k <= 0) {}
        }
        else
        {
          localObject1 = localObject2;
        }
        i = localInteger.intValue();
        localObject2 = c;
        int j;
        if (localObject2 != null) {
          j = ((Integer)localObject2).intValue();
        } else {
          j = 1;
        }
        i += j;
        localInteger = Integer.valueOf(i);
      }
      paramCollection = a;
      a = paramCollection;
      paramCollection = paramMap.entrySet().iterator();
      for (;;)
      {
        boolean bool3 = paramCollection.hasNext();
        if (!bool3) {
          break;
        }
        paramMap = (Map.Entry)paramCollection.next();
        localObject2 = a.b;
        Object localObject3 = paramMap.getKey();
        paramMap = paramMap.getValue();
        ((Map)localObject2).put(localObject3, paramMap);
      }
      paramCollection = b;
      b = paramCollection;
      c = localInteger;
      return;
    }
    paramCollection = new java/lang/IllegalArgumentException;
    paramCollection.<init>("Empty collection");
    throw paramCollection;
  }
  
  private int a(Notification paramNotification)
  {
    if (paramNotification != null)
    {
      c.a locala = a;
      paramNotification = a;
      return locala.a(paramNotification);
    }
    paramNotification = new java/lang/NullPointerException;
    paramNotification.<init>();
    throw paramNotification;
  }
  
  private static c.a a(o paramo)
  {
    if (paramo == null) {
      return null;
    }
    c.a locala = new com/truecaller/network/notification/c$a;
    locala.<init>();
    Object localObject = b(paramo.c("e"));
    a = ((c.a.b)localObject);
    localObject = c(paramo.c("a"));
    b = ((Map)localObject);
    paramo = d(paramo.c("c"));
    c = paramo;
    return locala;
  }
  
  private static List a(i parami)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    int i = 0;
    for (;;)
    {
      int j = parami.a();
      if (i >= j) {
        break;
      }
      Object localObject1 = (o)parami.a(i);
      Notification.NotificationActionHistoryItem localNotificationActionHistoryItem = new com/truecaller/old/data/entity/Notification$NotificationActionHistoryItem;
      localNotificationActionHistoryItem.<init>();
      Object localObject2 = "iurl";
      boolean bool = ((o)localObject1).a((String)localObject2);
      if (bool)
      {
        localObject2 = ay.a("iurl", (o)localObject1);
        c = ((String)localObject2);
      }
      localObject2 = "time";
      bool = ((o)localObject1).a((String)localObject2);
      long l;
      if (bool)
      {
        l = ay.c("time", (o)localObject1);
        localObject2 = Long.valueOf(l);
        a = ((Long)localObject2);
      }
      localObject2 = "act";
      bool = ((o)localObject1).a((String)localObject2);
      if (bool)
      {
        localObject2 = "act";
        l = ay.c((String)localObject2, (o)localObject1);
        j = (int)l;
        switch (j)
        {
        default: 
          break;
        case 4: 
          localObject1 = Notification.NotificationActionHistoryItem.Action.REPLIED;
          b = ((Notification.NotificationActionHistoryItem.Action)localObject1);
          break;
        case 3: 
          localObject1 = Notification.NotificationActionHistoryItem.Action.DENIED;
          b = ((Notification.NotificationActionHistoryItem.Action)localObject1);
          break;
        case 2: 
          localObject1 = Notification.NotificationActionHistoryItem.Action.ACCEPTED;
          b = ((Notification.NotificationActionHistoryItem.Action)localObject1);
          break;
        case 1: 
          localObject1 = Notification.NotificationActionHistoryItem.Action.VIEWED;
          b = ((Notification.NotificationActionHistoryItem.Action)localObject1);
          break;
        case 0: 
          localObject1 = Notification.NotificationActionHistoryItem.Action.NONE;
          b = ((Notification.NotificationActionHistoryItem.Action)localObject1);
        }
      }
      localArrayList.add(localNotificationActionHistoryItem);
      i += 1;
    }
    return localArrayList;
  }
  
  private static c.a.b b(o paramo)
  {
    c.a.b localb = new com/truecaller/network/notification/c$a$b;
    localb.<init>();
    long l = ay.c("i", paramo);
    a = l;
    Object localObject = NotificationType.valueOf(ay.b("t", paramo));
    b = ((NotificationType)localObject);
    localObject = NotificationScope.valueOf(ay.b("s", paramo));
    c = ((NotificationScope)localObject);
    l = ay.c("c", paramo);
    d = l;
    return localb;
  }
  
  private static Map c(o paramo)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    paramo = a.entrySet().iterator();
    for (;;)
    {
      boolean bool1 = paramo.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject1 = (Map.Entry)paramo.next();
      Object localObject2 = (l)((Map.Entry)localObject1).getValue();
      localObject1 = ((Map.Entry)localObject1).getKey();
      boolean bool2 = localObject2 instanceof com.google.gson.n;
      if (bool2) {
        localObject2 = "";
      } else {
        localObject2 = ((l)localObject2).c();
      }
      localHashMap.put(localObject1, localObject2);
    }
    return localHashMap;
  }
  
  private static c.a.a d(o paramo)
  {
    c.a.a locala;
    if (paramo != null)
    {
      locala = new com/truecaller/network/notification/c$a$a;
      locala.<init>();
      int i = ay.b("d", paramo);
      i = Math.max(0, i);
      a = i;
      String str = "o";
      int j = ay.b(str, paramo);
      j = Math.max(0, j);
      b = j;
    }
    else
    {
      locala = null;
    }
    return locala;
  }
  
  public final String a()
  {
    return a("n");
  }
  
  public final String a(Context paramContext)
  {
    paramContext = a("t");
    boolean bool = am.a(paramContext);
    if (bool) {
      return paramContext;
    }
    return "No Title";
  }
  
  public final String a(String paramString)
  {
    Map localMap = a.b;
    if (localMap == null) {
      return null;
    }
    return (String)localMap.get(paramString);
  }
  
  public final NotificationType b()
  {
    return a.a.b;
  }
  
  public final String b(Context paramContext)
  {
    paramContext = a("s");
    if (paramContext != null)
    {
      ah localah = TrueApp.y().a().aK();
      paramContext = localah.a(paramContext);
    }
    boolean bool = am.a(paramContext);
    if (bool) {
      return paramContext;
    }
    return "";
  }
  
  public final Bitmap c(Context paramContext)
  {
    String str = a("i");
    return ap.a(paramContext, str);
  }
  
  public final Long d()
  {
    return Long.valueOf(a.a.a);
  }
  
  public final void d(Context paramContext)
  {
    Object localObject = a("f");
    boolean bool1 = am.a((CharSequence)localObject);
    if (bool1)
    {
      localObject = a("f");
      paramContext = a(paramContext);
    }
    else
    {
      localObject = a(paramContext);
      paramContext = b(paramContext);
    }
    boolean bool2 = this instanceof n.a;
    localObject = a(bool2, (String)localObject);
    boolean bool3 = true;
    localObject = b(bool3, (String)localObject);
    boolean bool4 = am.a(paramContext);
    if (bool4)
    {
      paramContext = a(bool2, paramContext);
      paramContext = b(bool3, paramContext);
    }
    a((CharSequence)localObject, paramContext);
  }
  
  public final String e()
  {
    return a("fn");
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    boolean bool = paramObject instanceof Notification;
    if (!bool) {
      return false;
    }
    paramObject = (Notification)paramObject;
    c.a locala = a;
    paramObject = a;
    return locala.equals(paramObject);
  }
  
  public final String f()
  {
    return a("ln");
  }
  
  public final Long g()
  {
    return Long.valueOf(a.a.d);
  }
  
  public final String h()
  {
    return a("ac");
  }
  
  public final int hashCode()
  {
    return a.hashCode() + 31;
  }
  
  public final String i()
  {
    return a("cid");
  }
  
  public final String j()
  {
    return a("ch");
  }
  
  public final String k()
  {
    return a("rtm");
  }
  
  public final String l()
  {
    return a("rtc");
  }
  
  public final String m()
  {
    return a("cide");
  }
  
  public final String n()
  {
    return a("uid");
  }
  
  public final int o()
  {
    Object localObject1 = Notification.1.a;
    Object localObject2 = b();
    int i = ((NotificationType)localObject2).ordinal();
    int j = localObject1[i];
    i = 2131234315;
    int k = 2131234333;
    Notification.NotificationState localNotificationState;
    switch (j)
    {
    default: 
      localObject1 = b;
      localNotificationState = Notification.NotificationState.VIEWED;
      if (localObject1 == localNotificationState) {
        return k;
      }
      break;
    case 5: 
      localObject1 = b;
      localObject2 = Notification.NotificationState.VIEWED;
      if (localObject1 == localObject2) {
        return k;
      }
      return 2131234332;
    case 2: 
    case 3: 
    case 4: 
      localObject1 = b;
      localObject2 = Notification.NotificationState.VIEWED;
      if (localObject1 == localObject2) {
        return 2131234325;
      }
      return 2131234324;
    case 1: 
      localObject1 = b;
      localNotificationState = Notification.NotificationState.VIEWED;
      if (localObject1 == localNotificationState) {
        return k;
      }
      return i;
    }
    return i;
  }
  
  public final String p()
  {
    return a("u");
  }
  
  public final Object q()
  {
    return a("i");
  }
  
  public final boolean r()
  {
    NotificationType localNotificationType1 = b();
    NotificationType localNotificationType2 = NotificationType.PROMO_DOWNLOAD_URL;
    if (localNotificationType1 != localNotificationType2)
    {
      localNotificationType1 = b();
      localNotificationType2 = NotificationType.PROMO_OPEN_URL;
      if (localNotificationType1 != localNotificationType2)
      {
        localNotificationType1 = b();
        localNotificationType2 = NotificationType.UNSUPPORTED;
        if (localNotificationType1 != localNotificationType2) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final o s()
  {
    o localo1 = new com/google/gson/o;
    localo1.<init>();
    Object localObject1 = "d";
    Object localObject2 = a;
    i locali = null;
    o localo2;
    Object localObject3;
    Object localObject4;
    int k;
    if (localObject2 == null)
    {
      localo2 = null;
    }
    else
    {
      localo2 = new com/google/gson/o;
      localo2.<init>();
      localObject3 = a;
      Object localObject5;
      Object localObject7;
      if (localObject3 != null)
      {
        localObject3 = "e";
        localObject4 = a;
        localObject5 = new com/google/gson/o;
        ((o)localObject5).<init>();
        Object localObject6 = Long.valueOf(a);
        ((o)localObject5).a("i", (Number)localObject6);
        localObject6 = Integer.valueOf(c.value);
        ((o)localObject5).a("s", (Number)localObject6);
        int i = b.value;
        localObject6 = Integer.valueOf(i);
        ((o)localObject5).a("t", (Number)localObject6);
        localObject7 = "c";
        long l = d;
        localObject4 = Long.valueOf(l);
        ((o)localObject5).a((String)localObject7, (Number)localObject4);
        localo2.a((String)localObject3, (l)localObject5);
      }
      localObject3 = b;
      if (localObject3 != null)
      {
        localObject3 = new com/google/gson/o;
        ((o)localObject3).<init>();
        localObject4 = b.keySet().iterator();
        for (;;)
        {
          boolean bool1 = ((Iterator)localObject4).hasNext();
          if (!bool1) {
            break;
          }
          localObject5 = (String)((Iterator)localObject4).next();
          localObject7 = (String)b.get(localObject5);
          ((o)localObject3).a((String)localObject5, (String)localObject7);
        }
        localObject4 = "a";
        localo2.a((String)localObject4, (l)localObject3);
      }
      localObject3 = c;
      if (localObject3 != null)
      {
        localObject3 = "c";
        localObject2 = c;
        localObject4 = new com/google/gson/o;
        ((o)localObject4).<init>();
        int j = a;
        localObject7 = Integer.valueOf(j);
        ((o)localObject4).a("d", (Number)localObject7);
        localObject5 = "o";
        k = b;
        localObject2 = Integer.valueOf(k);
        ((o)localObject4).a((String)localObject5, (Number)localObject2);
        localo2.a((String)localObject3, (l)localObject4);
      }
    }
    localo1.a((String)localObject1, localo2);
    localObject2 = b.getValue();
    localo1.a("s", (Number)localObject2);
    localObject2 = c;
    localo1.a("m", (Number)localObject2);
    localObject2 = d;
    localo1.a("b", (String)localObject2);
    localObject1 = e;
    if (localObject1 != null)
    {
      k = ((List)localObject1).size();
      if (k != 0)
      {
        locali = new com/google/gson/i;
        locali.<init>();
        localObject1 = ((List)localObject1).iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          localObject2 = (Notification.NotificationActionHistoryItem)((Iterator)localObject1).next();
          localo2 = new com/google/gson/o;
          localo2.<init>();
          localObject3 = a;
          if (localObject3 != null)
          {
            localObject3 = "time";
            localObject4 = a;
            localo2.a((String)localObject3, (Number)localObject4);
          }
          localObject3 = c;
          if (localObject3 != null)
          {
            localObject3 = "iurl";
            localObject4 = c;
            localo2.a((String)localObject3, (String)localObject4);
          }
          localObject3 = "act";
          localObject2 = b.getVal();
          localo2.a((String)localObject3, (Number)localObject2);
          locali.a(localo2);
        }
      }
    }
    if (locali != null)
    {
      int m = locali.a();
      if (m > 0)
      {
        localObject1 = "a";
        localo1.a((String)localObject1, locali);
      }
    }
    return localo1;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("{d:");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", a:");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.entity.Notification
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */