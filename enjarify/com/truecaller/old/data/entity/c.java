package com.truecaller.old.data.entity;

import android.text.TextUtils;
import com.truecaller.log.AssertionUtil;

public class c
  implements d
{
  public long a = 0L;
  public String b = "";
  public String c = "";
  public String d = "";
  public String e = "";
  public String f = "";
  public String g = "";
  public String h = "";
  public String i = "";
  public String j = "";
  public String k = "";
  public String l = "";
  public String m = "";
  public String n = "";
  public int o = -1;
  public int p;
  private int q = 0;
  
  private static int b(String paramString)
  {
    try
    {
      return Integer.parseInt(paramString);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      com.truecaller.log.d.a(localNumberFormatException;
    }
    return 0;
  }
  
  private static long c(String paramString)
  {
    try
    {
      return Long.parseLong(paramString);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      com.truecaller.log.d.a(localNumberFormatException;
    }
    return 0L;
  }
  
  public final String a()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(512);
    long l1 = a;
    localStringBuilder.append(l1);
    localStringBuilder.append("§");
    String str = b;
    localStringBuilder.append(str);
    localStringBuilder.append("§");
    str = c;
    localStringBuilder.append(str);
    localStringBuilder.append("§§§");
    str = j;
    localStringBuilder.append(str);
    localStringBuilder.append("§");
    str = k;
    localStringBuilder.append(str);
    localStringBuilder.append("§");
    str = l;
    localStringBuilder.append(str);
    localStringBuilder.append("§");
    str = n;
    localStringBuilder.append(str);
    localStringBuilder.append("§");
    str = d;
    localStringBuilder.append(str);
    localStringBuilder.append("§");
    int i1 = q;
    localStringBuilder.append(i1);
    localStringBuilder.append("§");
    str = m;
    localStringBuilder.append(str);
    localStringBuilder.append("§");
    str = e;
    localStringBuilder.append(str);
    localStringBuilder.append("§");
    str = f;
    localStringBuilder.append(str);
    localStringBuilder.append("§");
    str = g;
    localStringBuilder.append(str);
    localStringBuilder.append("§§§§");
    i1 = o;
    localStringBuilder.append(i1);
    return localStringBuilder.toString();
  }
  
  public final void a(String paramString)
  {
    int i1 = -1;
    String[] arrayOfString = paramString.split("§", i1);
    int i2 = arrayOfString.length;
    int i3 = 6;
    int i4 = 9;
    if (i2 < i4)
    {
      String str = "meta_deserialize_string";
      com.truecaller.log.d.a(i3, str, paramString);
    }
    long l1 = c(arrayOfString[0]);
    a = l1;
    paramString = arrayOfString[1];
    b = paramString;
    paramString = arrayOfString[2];
    c = paramString;
    paramString = arrayOfString[3];
    h = paramString;
    paramString = arrayOfString[4];
    i = paramString;
    paramString = arrayOfString[5];
    j = paramString;
    paramString = arrayOfString[i3];
    k = paramString;
    paramString = arrayOfString[7];
    l = paramString;
    paramString = arrayOfString[8];
    n = paramString;
    int i5 = arrayOfString.length;
    i2 = 10;
    if (i5 > i4)
    {
      paramString = arrayOfString[i4];
      d = paramString;
      paramString = arrayOfString[i2];
      i5 = b(paramString);
      q = i5;
    }
    i5 = arrayOfString.length;
    if (i5 > i2)
    {
      i5 = 11;
      paramString = arrayOfString[i5];
      m = paramString;
    }
    i5 = arrayOfString.length;
    i2 = 13;
    if (i5 > i2)
    {
      paramString = arrayOfString[12];
      e = paramString;
      paramString = arrayOfString[i2];
      f = paramString;
      i5 = 14;
      paramString = arrayOfString[i5];
      g = paramString;
    }
    o = i1;
    i5 = arrayOfString.length;
    i1 = 18;
    if (i5 > i1)
    {
      paramString = arrayOfString[i1];
      boolean bool = TextUtils.isEmpty(paramString);
      if (!bool) {
        try
        {
          paramString = arrayOfString[i1];
          int i6 = Integer.parseInt(paramString);
          o = i6;
          return;
        }
        catch (NumberFormatException paramString)
        {
          AssertionUtil.reportThrowableButNeverCrash(paramString);
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.entity.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */