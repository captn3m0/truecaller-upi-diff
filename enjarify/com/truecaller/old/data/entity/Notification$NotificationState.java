package com.truecaller.old.data.entity;

public enum Notification$NotificationState
{
  private final Integer value;
  
  static
  {
    Object localObject = new com/truecaller/old/data/entity/Notification$NotificationState;
    int i = 1;
    Integer localInteger1 = Integer.valueOf(i);
    ((NotificationState)localObject).<init>("NEW", 0, localInteger1);
    NEW = (NotificationState)localObject;
    localObject = new com/truecaller/old/data/entity/Notification$NotificationState;
    int j = 2;
    Integer localInteger2 = Integer.valueOf(j);
    ((NotificationState)localObject).<init>("VIEWED", i, localInteger2);
    VIEWED = (NotificationState)localObject;
    localObject = new NotificationState[j];
    NotificationState localNotificationState = NEW;
    localObject[0] = localNotificationState;
    localNotificationState = VIEWED;
    localObject[i] = localNotificationState;
    $VALUES = (NotificationState[])localObject;
  }
  
  private Notification$NotificationState(Integer paramInteger)
  {
    value = paramInteger;
  }
  
  public static NotificationState getFromValue(Integer paramInteger)
  {
    Object localObject = values();
    int i = localObject.length;
    int j = 0;
    while (j < i)
    {
      NotificationState localNotificationState = localObject[j];
      Integer localInteger = localNotificationState.getValue();
      if (localInteger == paramInteger) {
        return localNotificationState;
      }
      j += 1;
    }
    localObject = new java/lang/Exception;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Invalid NotificationState value '");
    localStringBuilder.append(paramInteger);
    localStringBuilder.append("'");
    paramInteger = localStringBuilder.toString();
    ((Exception)localObject).<init>(paramInteger);
    throw ((Throwable)localObject);
  }
  
  public final Integer getValue()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.entity.Notification.NotificationState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */