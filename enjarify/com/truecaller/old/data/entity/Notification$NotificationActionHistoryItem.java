package com.truecaller.old.data.entity;

public final class Notification$NotificationActionHistoryItem
{
  public Long a;
  public Notification.NotificationActionHistoryItem.Action b;
  public String c;
  
  public Notification$NotificationActionHistoryItem()
  {
    Notification.NotificationActionHistoryItem.Action localAction = Notification.NotificationActionHistoryItem.Action.NONE;
    b = localAction;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.entity.Notification.NotificationActionHistoryItem
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */