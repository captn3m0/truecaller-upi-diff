package com.truecaller.old.data.entity;

import android.text.TextUtils;
import com.google.gson.o;
import com.truecaller.util.ay;

public final class e
  implements a
{
  public long a;
  public String b;
  public String c;
  public boolean d;
  private String e;
  private boolean f;
  
  public e(long paramLong, String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    c = paramString2;
    b = paramString1;
    a = paramLong;
    e = paramString3;
    d = true;
    f = paramBoolean;
  }
  
  public e(o paramo)
  {
    String str = ay.a("n", paramo);
    b = str;
    long l = ay.c("ts", paramo);
    a = l;
    str = ay.a("na", paramo);
    c = str;
    str = ay.a("t", paramo);
    e = str;
    boolean bool1 = ay.d("b", paramo);
    d = bool1;
    boolean bool2 = ay.d("h", paramo);
    f = bool2;
  }
  
  public final o a()
  {
    o localo = new com/google/gson/o;
    localo.<init>();
    Object localObject = b;
    localo.a("n", (String)localObject);
    localObject = Long.valueOf(a);
    localo.a("ts", (Number)localObject);
    localObject = c;
    localo.a("na", (String)localObject);
    localObject = e;
    localo.a("t", (String)localObject);
    localObject = Boolean.valueOf(d);
    localo.a("b", (Boolean)localObject);
    localObject = Boolean.valueOf(f);
    localo.a("h", (Boolean)localObject);
    return localo;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    boolean bool2 = paramObject instanceof e;
    if (bool2)
    {
      paramObject = (e)paramObject;
      long l1 = a;
      long l2 = a;
      l1 = Math.abs(l1 - l2);
      l2 = 15000L;
      bool2 = l1 < l2;
      if (bool2)
      {
        String str = b;
        paramObject = b;
        boolean bool3 = TextUtils.equals(str, (CharSequence)paramObject);
        if (bool3) {
          return bool1;
        }
      }
      return false;
    }
    return false;
  }
  
  public final int hashCode()
  {
    long l1 = a;
    int i = 32;
    long l2 = l1 >>> i;
    l1 ^= l2;
    int j = ((int)l1 + 403) * 31;
    String str = b;
    int k;
    if (str == null)
    {
      k = 0;
      str = null;
    }
    else
    {
      k = str.hashCode();
    }
    return j + k;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.entity.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */