package com.truecaller.old.data.entity;

public enum Notification$NotificationActionHistoryItem$Action
{
  private long val = 0L;
  
  static
  {
    Object localObject = new com/truecaller/old/data/entity/Notification$NotificationActionHistoryItem$Action;
    ((Action)localObject).<init>("NONE", 0, 0L);
    NONE = (Action)localObject;
    localObject = new com/truecaller/old/data/entity/Notification$NotificationActionHistoryItem$Action;
    int i = 1;
    ((Action)localObject).<init>("VIEWED", i, 1L);
    VIEWED = (Action)localObject;
    localObject = new com/truecaller/old/data/entity/Notification$NotificationActionHistoryItem$Action;
    int j = 2;
    ((Action)localObject).<init>("ACCEPTED", j, 2);
    ACCEPTED = (Action)localObject;
    localObject = new com/truecaller/old/data/entity/Notification$NotificationActionHistoryItem$Action;
    int k = 3;
    ((Action)localObject).<init>("DENIED", k, 3);
    DENIED = (Action)localObject;
    localObject = new com/truecaller/old/data/entity/Notification$NotificationActionHistoryItem$Action;
    int m = 4;
    ((Action)localObject).<init>("REPLIED", m, 4);
    REPLIED = (Action)localObject;
    localObject = new Action[5];
    Action localAction = NONE;
    localObject[0] = localAction;
    localAction = VIEWED;
    localObject[i] = localAction;
    localAction = ACCEPTED;
    localObject[j] = localAction;
    localAction = DENIED;
    localObject[k] = localAction;
    localAction = REPLIED;
    localObject[m] = localAction;
    $VALUES = (Action[])localObject;
  }
  
  private Notification$NotificationActionHistoryItem$Action(long paramLong)
  {
    val = paramLong;
  }
  
  public final Long getVal()
  {
    return Long.valueOf(val);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.entity.Notification.NotificationActionHistoryItem.Action
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */