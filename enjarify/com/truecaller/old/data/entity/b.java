package com.truecaller.old.data.entity;

import android.content.Context;
import com.truecaller.common.e.c;
import com.truecaller.ui.components.n;

public final class b
  extends n
{
  public final c a;
  
  public b(c paramc)
  {
    a = paramc;
  }
  
  public b(String paramString1, String paramString2, String paramString3)
  {
    c localc = new com/truecaller/common/e/c;
    localc.<init>(paramString1, paramString2, paramString3);
    a = localc;
  }
  
  public final String a()
  {
    return a.b;
  }
  
  public final String a(Context paramContext)
  {
    return a.a;
  }
  
  public final String b()
  {
    return a.c;
  }
  
  public final int c()
  {
    return 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.data.entity.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */