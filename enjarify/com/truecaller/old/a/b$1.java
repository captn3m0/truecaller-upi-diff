package com.truecaller.old.a;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

final class b$1
  implements ThreadFactory
{
  private final AtomicInteger a;
  
  b$1()
  {
    AtomicInteger localAtomicInteger = new java/util/concurrent/atomic/AtomicInteger;
    localAtomicInteger.<init>(1);
    a = localAtomicInteger;
  }
  
  public final Thread newThread(Runnable paramRunnable)
  {
    Thread localThread = new java/lang/Thread;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Network AsyncTask #");
    int i = a.getAndIncrement();
    ((StringBuilder)localObject).append(i);
    localObject = ((StringBuilder)localObject).toString();
    localThread.<init>(paramRunnable, (String)localObject);
    return localThread;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.a.b.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */