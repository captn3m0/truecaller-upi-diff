package com.truecaller.old.a;

import android.os.AsyncTask;

public abstract class a
  extends AsyncTask
{
  private boolean a;
  protected final c b;
  private final Object[] c;
  
  public a()
  {
    this(null);
  }
  
  private a(c paramc)
  {
    this(paramc, true, null);
  }
  
  public a(c paramc, byte paramByte)
  {
    this(paramc);
  }
  
  public a(c paramc, boolean paramBoolean, Object... paramVarArgs)
  {
    b = paramc;
    paramc = null;
    a = false;
    c = paramVarArgs;
    if (paramBoolean)
    {
      paramc = new Object[0];
      b.c(this, paramc);
    }
  }
  
  public final a a()
  {
    Object[] arrayOfObject = c;
    b.a(this, arrayOfObject);
    return this;
  }
  
  protected abstract void a(Object paramObject);
  
  protected void onPostExecute(Object paramObject)
  {
    boolean bool = a;
    if (!bool)
    {
      c localc = b;
      if (localc != null)
      {
        bool = localc.isFinishing();
        if (!bool)
        {
          localc = b;
          localc.e();
        }
      }
    }
    a(paramObject);
  }
  
  protected void onPreExecute()
  {
    boolean bool = a;
    if (!bool)
    {
      c localc = b;
      if (localc != null)
      {
        bool = localc.isFinishing();
        if (!bool)
        {
          localc = b;
          localc.d_(false);
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */