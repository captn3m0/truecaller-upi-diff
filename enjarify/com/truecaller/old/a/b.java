package com.truecaller.old.a;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public abstract class b
{
  public static final Executor a;
  private static final Handler b;
  private static int c;
  private static final ThreadFactory d;
  private static final BlockingQueue e;
  
  static
  {
    Object localObject = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    ((Handler)localObject).<init>(localLooper);
    b = (Handler)localObject;
    c = 4;
    localObject = new com/truecaller/old/a/b$1;
    ((b.1)localObject).<init>();
    d = (ThreadFactory)localObject;
    localObject = new java/util/concurrent/LinkedBlockingQueue;
    ((LinkedBlockingQueue)localObject).<init>(128);
    e = (BlockingQueue)localObject;
    localObject = new java/util/concurrent/ThreadPoolExecutor;
    int i = c;
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    BlockingQueue localBlockingQueue = e;
    ThreadFactory localThreadFactory = d;
    ((ThreadPoolExecutor)localObject).<init>(1, i, 1L, localTimeUnit, localBlockingQueue, localThreadFactory);
    a = (Executor)localObject;
  }
  
  public static AsyncTask a(AsyncTask paramAsyncTask, Object... paramVarArgs)
  {
    if (paramAsyncTask == null) {
      return null;
    }
    int i = 1;
    Object localObject1 = new String[i];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("executeOnThreadPool(");
    String str = paramAsyncTask.getClass().getName();
    ((StringBuilder)localObject2).append(str);
    ((StringBuilder)localObject2).append(", ");
    str = Arrays.toString(paramVarArgs);
    ((StringBuilder)localObject2).append(str);
    str = ")";
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    try
    {
      localObject1 = AsyncTask.THREAD_POOL_EXECUTOR;
      return paramAsyncTask.executeOnExecutor((Executor)localObject1, paramVarArgs);
    }
    catch (Exception localException) {}
    return paramAsyncTask;
  }
  
  public static AsyncTask b(AsyncTask paramAsyncTask, Object... paramVarArgs)
  {
    if (paramAsyncTask == null) {
      return null;
    }
    int i = 1;
    Object localObject1 = new String[i];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("executeOnNetworkThreadPool(");
    String str = paramAsyncTask.getClass().getName();
    ((StringBuilder)localObject2).append(str);
    ((StringBuilder)localObject2).append(", ");
    str = Arrays.toString(paramVarArgs);
    ((StringBuilder)localObject2).append(str);
    str = ")";
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    try
    {
      localObject1 = a;
      return paramAsyncTask.executeOnExecutor((Executor)localObject1, paramVarArgs);
    }
    catch (Exception localException) {}
    return paramAsyncTask;
  }
  
  public static void c(AsyncTask paramAsyncTask, Object... paramVarArgs)
  {
    Object localObject1 = Looper.myLooper();
    if (localObject1 != null)
    {
      localObject2 = Looper.getMainLooper();
      if (localObject1 != localObject2)
      {
        localObject2 = new android/os/Handler;
        ((Handler)localObject2).<init>((Looper)localObject1);
        break label36;
      }
    }
    Object localObject2 = b;
    label36:
    localObject1 = new com/truecaller/old/a/b$2;
    ((b.2)localObject1).<init>(paramAsyncTask, paramVarArgs);
    ((Handler)localObject2).post((Runnable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */