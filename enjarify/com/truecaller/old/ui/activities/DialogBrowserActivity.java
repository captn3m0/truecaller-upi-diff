package com.truecaller.old.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import com.truecaller.log.AssertionUtil;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.utils.extensions.a;

public class DialogBrowserActivity
  extends AppCompatActivity
  implements View.OnClickListener
{
  protected WebView a;
  protected View b;
  
  public static void a(Context paramContext, String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, DialogBrowserActivity.class);
    paramString = localIntent.addFlags(268435456).putExtra("ARG_URL", paramString);
    paramContext.startActivity(paramString);
  }
  
  public void onClick(View paramView)
  {
    finish();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    a.a(this);
    paramBundle = getIntent();
    Object localObject = "ARG_URL";
    paramBundle = paramBundle.getStringExtra((String)localObject);
    boolean bool = TextUtils.isEmpty(paramBundle);
    if (bool)
    {
      AssertionUtil.reportWeirdnessButNeverCrash("Empty URL for DialogBrowserActivity");
      finish();
      return;
    }
    localObject = getTheme();
    int i = aresId;
    ((Resources.Theme)localObject).applyStyle(i, false);
    setContentView(2131559183);
    localObject = (WebView)findViewById(2131365530);
    a = ((WebView)localObject);
    localObject = findViewById(2131365529);
    b = ((View)localObject);
    findViewById(2131362837).setOnClickListener(this);
    localObject = a;
    DialogBrowserActivity.1 local1 = new com/truecaller/old/ui/activities/DialogBrowserActivity$1;
    local1.<init>(this);
    ((WebView)localObject).setWebViewClient(local1);
    a.loadUrl(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.ui.activities.DialogBrowserActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */