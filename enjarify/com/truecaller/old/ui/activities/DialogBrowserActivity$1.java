package com.truecaller.old.ui.activities;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.truecaller.util.at;
import com.truecaller.util.dh;

final class DialogBrowserActivity$1
  extends WebViewClient
{
  DialogBrowserActivity$1(DialogBrowserActivity paramDialogBrowserActivity) {}
  
  public final void onPageFinished(WebView paramWebView, String paramString)
  {
    super.onPageFinished(paramWebView, paramString);
    at.a(a.b, false);
  }
  
  public final void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
  {
    super.onPageStarted(paramWebView, paramString, paramBitmap);
    at.a(a.b, true);
  }
  
  public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    dh.a(a, paramString, false);
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.ui.activities.DialogBrowserActivity.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */