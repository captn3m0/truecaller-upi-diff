package com.truecaller.old.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import com.truecaller.util.at;
import com.truecaller.utils.extensions.a;

public class DialogActivity
  extends Activity
{
  public static void a(Context paramContext, String paramString1, String paramString2)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, DialogActivity.class);
    paramString1 = localIntent.addFlags(268435456).putExtra("ARG_TITLE", paramString1).putExtra("ARG_TEXT", paramString2);
    paramContext.startActivity(paramString1);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    a.a(this);
    getTheme().applyStyle(2131952145, false);
    paramBundle = (LinearLayout)at.b(this, 2131558593);
    Object localObject1 = at.b(this, 2131558573);
    paramBundle.addView((View)localObject1);
    setContentView(paramBundle);
    paramBundle = findViewById(16908290);
    localObject1 = getIntent().getStringExtra("ARG_TITLE");
    at.a(paramBundle, 2131362847, (CharSequence)localObject1);
    localObject1 = getIntent().getStringExtra("ARG_TEXT");
    at.a(paramBundle, 2131362838, (CharSequence)localObject1);
    at.a(paramBundle, 2131362849, false);
    at.a(paramBundle, 2131362844, false);
    Object localObject2 = getString(2131887217);
    int i = 2131362843;
    at.a(paramBundle, i, (CharSequence)localObject2);
    paramBundle = at.b(paramBundle, i);
    localObject2 = new com/truecaller/old/ui/activities/-$$Lambda$DialogActivity$mPDD9ESIbrGDV4QqW9rPCoEeE2s;
    ((-..Lambda.DialogActivity.mPDD9ESIbrGDV4QqW9rPCoEeE2s)localObject2).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.old.ui.activities.DialogActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */