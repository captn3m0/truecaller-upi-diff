package com.truecaller;

import dagger.a.d;
import javax.inject.Provider;

public final class aq
  implements d
{
  private final c a;
  private final Provider b;
  
  private aq(c paramc, Provider paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static aq a(c paramc, Provider paramProvider)
  {
    aq localaq = new com/truecaller/aq;
    localaq.<init>(paramc, paramProvider);
    return localaq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.aq
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */