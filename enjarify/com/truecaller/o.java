package com.truecaller;

import com.truecaller.androidactors.f;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.h;
import com.truecaller.util.al;
import javax.inject.Provider;

public final class o
  implements dagger.a.d
{
  private final c a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  private final Provider g;
  private final Provider h;
  private final Provider i;
  
  private o(c paramc, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7, Provider paramProvider8)
  {
    a = paramc;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
  }
  
  public static o a(c paramc, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7, Provider paramProvider8)
  {
    o localo = new com/truecaller/o;
    localo.<init>(paramc, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8);
    return localo;
  }
  
  public static com.truecaller.search.local.model.c a(com.truecaller.search.local.model.g paramg, com.truecaller.common.account.r paramr, al paramal, h paramh, f paramf, com.truecaller.presence.r paramr1, bw parambw, com.truecaller.voip.d paramd)
  {
    return (com.truecaller.search.local.model.c)dagger.a.g.a(c.a(paramg, paramr, paramal, paramh, paramf, paramr1, parambw, paramd), "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */