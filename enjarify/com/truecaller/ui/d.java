package com.truecaller.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.m;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.TextView;
import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.Availability.Status;
import com.truecaller.old.data.access.Settings;
import com.truecaller.presence.a;
import com.truecaller.search.local.model.a.n;
import com.truecaller.search.local.model.a.o;
import com.truecaller.search.local.model.a.q;
import com.truecaller.search.local.model.g;
import com.truecaller.search.local.model.j;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.util.ck;
import com.truecaller.utils.ui.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class d
  extends RecyclerView.Adapter
{
  public final RecyclerView.OnScrollListener a;
  protected final ArrayList b;
  protected final g c;
  protected Context d;
  protected LayoutInflater e;
  protected long f;
  protected d.c g;
  protected d.d h;
  protected final c i;
  protected final c j;
  private final d.e k;
  private long l;
  private final Object m;
  
  public d(Context paramContext)
  {
    this(paramContext, localArrayList);
  }
  
  public d(Context paramContext, ArrayList paramArrayList)
  {
    Object localObject = new com/truecaller/ui/d$b;
    ((d.b)localObject).<init>(this, (byte)0);
    a = ((RecyclerView.OnScrollListener)localObject);
    localObject = new com/truecaller/ui/d$e;
    ((d.e)localObject).<init>();
    k = ((d.e)localObject);
    l = 0L;
    localObject = new java/lang/Object;
    localObject.<init>();
    m = localObject;
    d = paramContext;
    b = paramArrayList;
    paramArrayList = g.a(paramContext);
    c = paramArrayList;
    paramArrayList = LayoutInflater.from(paramContext);
    e = paramArrayList;
    paramArrayList = new com/truecaller/ui/c$a;
    paramArrayList.<init>(paramContext);
    a = true;
    b = false;
    int n = 6;
    d = n;
    int i1 = 9;
    e = i1;
    c.a locala = new com/truecaller/ui/c$a;
    locala.<init>(paramContext);
    a = false;
    b = false;
    d = n;
    e = i1;
    a(paramArrayList, locala);
    paramContext = paramArrayList.a();
    i = paramContext;
    paramContext = locala.a();
    j = paramContext;
  }
  
  protected static CharSequence a(j paramj)
  {
    if (paramj == null) {
      return "";
    }
    boolean bool1 = paramj.d();
    if (!bool1)
    {
      localObject = (o)paramj.a(o.class);
      if (localObject != null) {
        return ((o)localObject).j();
      }
    }
    Object localObject = (n)paramj.a(n.class);
    int n = 0;
    String str = null;
    if (localObject != null) {
      str = ((n)localObject).e();
    }
    bool1 = TextUtils.isEmpty(str);
    if (bool1)
    {
      localObject = q.class;
      paramj = (q)paramj.a((Class)localObject);
      if (paramj != null)
      {
        paramj = paramj.d();
        bool1 = false;
        localObject = null;
        n = paramj.length();
        int i1 = 50;
        n = Math.min(n, i1);
        str = paramj.substring(0, n);
      }
    }
    boolean bool2 = TextUtils.isEmpty(str);
    if (bool2) {
      return "";
    }
    return str;
  }
  
  private void b(d.g paramg, int paramInt)
  {
    long l1 = l;
    long l2 = 1L;
    l1 += l2;
    l = l1;
    a = l1;
    b = paramInt;
    Object localObject1 = a(paramInt);
    Object localObject2 = itemView.getBackground();
    Object localObject3;
    if (localObject2 == null)
    {
      localObject2 = itemView;
      localObject3 = d;
      n = 2130969534;
      i1 = b.d((Context)localObject3, n);
      ((View)localObject2).setBackgroundResource(i1);
    }
    localObject2 = g;
    int i1 = 1;
    int n = 0;
    int i2;
    if (localObject2 != null)
    {
      i2 = 1;
    }
    else
    {
      i2 = 0;
      localObject2 = null;
    }
    if (i2 != 0)
    {
      localObject2 = itemView;
      -..Lambda.d.j3YZ98KTayxVfiskXjcf4L7lO-w localj3YZ98KTayxVfiskXjcf4L7lO-w = new com/truecaller/ui/-$$Lambda$d$j3YZ98KTayxVfiskXjcf4L7lO-w;
      localj3YZ98KTayxVfiskXjcf4L7lO-w.<init>(this, paramg);
      ((View)localObject2).setOnClickListener(localj3YZ98KTayxVfiskXjcf4L7lO-w);
    }
    else
    {
      itemView.setOnClickListener(null);
      localObject2 = itemView;
      ((View)localObject2).setClickable(false);
    }
    localObject2 = h;
    if (localObject2 == null)
    {
      i1 = 0;
      localObject3 = null;
    }
    if (i1 != 0)
    {
      localObject2 = itemView;
      localObject3 = new com/truecaller/ui/-$$Lambda$d$WyHfl19FKlS1CLIvZbiNOBKWQtk;
      ((-..Lambda.d.WyHfl19FKlS1CLIvZbiNOBKWQtk)localObject3).<init>(this, paramg);
      ((View)localObject2).setOnLongClickListener((View.OnLongClickListener)localObject3);
    }
    else
    {
      itemView.setOnLongClickListener(null);
      localObject2 = itemView;
      ((View)localObject2).setLongClickable(false);
    }
    a(paramg, localObject1, paramInt);
  }
  
  private boolean b(d.a parama)
  {
    ArrayList localArrayList = b;
    return parama.a(this, localArrayList);
  }
  
  public final Object a(int paramInt)
  {
    if (paramInt >= 0)
    {
      ArrayList localArrayList = b;
      int n = localArrayList.size();
      if (paramInt < n) {
        return b.get(paramInt);
      }
    }
    return null;
  }
  
  public final List a(RecyclerView paramRecyclerView)
  {
    if (paramRecyclerView == null) {
      return Collections.emptyList();
    }
    paramRecyclerView = (LinearLayoutManager)paramRecyclerView.getLayoutManager();
    int n = paramRecyclerView.findFirstVisibleItemPosition();
    int i1 = paramRecyclerView.findLastVisibleItemPosition();
    ArrayList localArrayList = new java/util/ArrayList;
    int i2 = i1 - n;
    localArrayList.<init>(i2);
    while (n <= i1)
    {
      Object localObject = a(n);
      localArrayList.add(localObject);
      n += 1;
    }
    return localArrayList;
  }
  
  protected void a() {}
  
  protected final void a(TextView paramTextView, j paramj)
  {
    boolean bool1 = Settings.f();
    int i1 = 8;
    if (bool1)
    {
      paramj = paramj.g();
      Object localObject = d.1.a;
      int i2 = b.a().ordinal();
      int n = localObject[i2];
      i2 = 0;
      boolean bool2 = true;
      switch (n)
      {
      default: 
        paramTextView.setVisibility(i1);
        return;
      case 2: 
        localObject = j;
        m.a(paramTextView, (Drawable)localObject, null);
        localObject = d;
        paramj = paramj.a((Context)localObject, bool2);
        paramTextView.setText(paramj);
        paramTextView.setVisibility(0);
        return;
      }
      localObject = i;
      m.a(paramTextView, (Drawable)localObject, null);
      localObject = d;
      paramj = paramj.a((Context)localObject, bool2);
      paramTextView.setText(paramj);
      paramTextView.setVisibility(0);
      return;
    }
    paramTextView.setVisibility(i1);
  }
  
  protected void a(c.a parama1, c.a parama2) {}
  
  public final void a(d.a parama)
  {
    ck.b("You must call this method on the main thread");
    long l1 = System.currentTimeMillis();
    f = l1;
    b(parama);
    a();
  }
  
  public final void a(d.c paramc)
  {
    g = paramc;
    paramc = k;
    a(paramc);
  }
  
  public final void a(d.g paramg, int paramInt)
  {
    Collections.emptyList();
    b(paramg, paramInt);
  }
  
  protected abstract void a(d.g paramg, Object paramObject, int paramInt);
  
  protected final void a(ContactPhoto paramContactPhoto, Object paramObject)
  {
    Object localObject = m;
    paramContactPhoto.a(paramObject, localObject);
  }
  
  public int getItemCount()
  {
    return b.size();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */