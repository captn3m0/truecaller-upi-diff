package com.truecaller.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.truecaller.util.at;
import com.truecaller.utils.ui.b;

public final class c
  extends Drawable
{
  private final Paint a;
  private final Paint b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  
  private c(Context paramContext, c.a parama)
  {
    boolean bool1 = a;
    if (bool1)
    {
      i = 2130968653;
      f1 = 1.7545966E38F;
    }
    else
    {
      i = 2130968654;
      f1 = 1.7545968E38F;
    }
    int i = b.a(paramContext, i);
    Paint localPaint1 = new android/graphics/Paint;
    int k = 1;
    localPaint1.<init>(k);
    a = localPaint1;
    localPaint1 = a;
    localPaint1.setColor(i);
    Paint localPaint2 = new android/graphics/Paint;
    localPaint2.<init>(k);
    b = localPaint2;
    localPaint2 = b;
    int m = -1;
    localPaint2.setColor(m);
    float f1 = d;
    i = at.a(paramContext, f1);
    c = i;
    boolean bool2 = c;
    if (bool2)
    {
      f1 = d + k;
      j = at.a(paramContext, f1);
      d = j;
    }
    else
    {
      d = 0;
    }
    int j = e;
    if (j > 0)
    {
      j = e;
      f1 = j;
      m = at.a(paramContext, f1);
    }
    e = m;
    boolean bool3 = b;
    if (bool3)
    {
      float f2 = f;
      int n = at.a(paramContext, f2);
      f = n;
      return;
    }
    f = 0;
  }
  
  public final void draw(Canvas paramCanvas)
  {
    Rect localRect = getBounds();
    int i = localRect.centerY();
    int j = f;
    i -= j;
    j = c / 2;
    int k = d;
    if (k > 0)
    {
      k = localRect.centerX();
      float f1 = k;
      float f2 = i;
      int m = d / 2;
      float f3 = m;
      Paint localPaint1 = b;
      paramCanvas.drawCircle(f1, f2, f3, localPaint1);
    }
    float f4 = localRect.centerX();
    float f5 = i;
    float f6 = j;
    Paint localPaint2 = a;
    paramCanvas.drawCircle(f4, f5, f6, localPaint2);
  }
  
  public final int getIntrinsicHeight()
  {
    int i = e;
    if (i > 0) {
      return i;
    }
    i = c;
    int j = f * 2;
    return i + j;
  }
  
  public final int getIntrinsicWidth()
  {
    int i = e;
    if (i > 0) {
      return i;
    }
    return c;
  }
  
  public final int getOpacity()
  {
    return -1;
  }
  
  public final void setAlpha(int paramInt)
  {
    a.setAlpha(paramInt);
  }
  
  public final void setColorFilter(ColorFilter paramColorFilter)
  {
    a.setColorFilter(paramColorFilter);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */