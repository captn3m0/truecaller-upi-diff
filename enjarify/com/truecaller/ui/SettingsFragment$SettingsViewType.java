package com.truecaller.ui;

public enum SettingsFragment$SettingsViewType
{
  static
  {
    Object localObject = new com/truecaller/ui/SettingsFragment$SettingsViewType;
    ((SettingsViewType)localObject).<init>("SETTINGS_GENERAL", 0);
    SETTINGS_GENERAL = (SettingsViewType)localObject;
    localObject = new com/truecaller/ui/SettingsFragment$SettingsViewType;
    int i = 1;
    ((SettingsViewType)localObject).<init>("SETTINGS_CALLERID", i);
    SETTINGS_CALLERID = (SettingsViewType)localObject;
    localObject = new com/truecaller/ui/SettingsFragment$SettingsViewType;
    int j = 2;
    ((SettingsViewType)localObject).<init>("SETTINGS_PRIVACY", j);
    SETTINGS_PRIVACY = (SettingsViewType)localObject;
    localObject = new com/truecaller/ui/SettingsFragment$SettingsViewType;
    int k = 3;
    ((SettingsViewType)localObject).<init>("SETTINGS_ABOUT", k);
    SETTINGS_ABOUT = (SettingsViewType)localObject;
    localObject = new com/truecaller/ui/SettingsFragment$SettingsViewType;
    int m = 4;
    ((SettingsViewType)localObject).<init>("SETTINGS_MAIN", m);
    SETTINGS_MAIN = (SettingsViewType)localObject;
    localObject = new com/truecaller/ui/SettingsFragment$SettingsViewType;
    int n = 5;
    ((SettingsViewType)localObject).<init>("SETTINGS_APPEARANCE", n);
    SETTINGS_APPEARANCE = (SettingsViewType)localObject;
    localObject = new com/truecaller/ui/SettingsFragment$SettingsViewType;
    int i1 = 6;
    ((SettingsViewType)localObject).<init>("SETTINGS_LANGUAGE", i1);
    SETTINGS_LANGUAGE = (SettingsViewType)localObject;
    localObject = new com/truecaller/ui/SettingsFragment$SettingsViewType;
    int i2 = 7;
    ((SettingsViewType)localObject).<init>("SETTINGS_RINGTONE", i2);
    SETTINGS_RINGTONE = (SettingsViewType)localObject;
    localObject = new com/truecaller/ui/SettingsFragment$SettingsViewType;
    int i3 = 8;
    ((SettingsViewType)localObject).<init>("SETTINGS_BLOCK", i3);
    SETTINGS_BLOCK = (SettingsViewType)localObject;
    localObject = new com/truecaller/ui/SettingsFragment$SettingsViewType;
    int i4 = 9;
    ((SettingsViewType)localObject).<init>("SETTINGS_MESSAGING", i4);
    SETTINGS_MESSAGING = (SettingsViewType)localObject;
    localObject = new com/truecaller/ui/SettingsFragment$SettingsViewType;
    int i5 = 10;
    ((SettingsViewType)localObject).<init>("SETTINGS_BACKUP", i5);
    SETTINGS_BACKUP = (SettingsViewType)localObject;
    localObject = new com/truecaller/ui/SettingsFragment$SettingsViewType;
    int i6 = 11;
    ((SettingsViewType)localObject).<init>("SETTINGS_CALL_RECORDING", i6);
    SETTINGS_CALL_RECORDING = (SettingsViewType)localObject;
    localObject = new com/truecaller/ui/SettingsFragment$SettingsViewType;
    int i7 = 12;
    ((SettingsViewType)localObject).<init>("SETTINGS_LANGUAGE_SELECTOR", i7);
    SETTINGS_LANGUAGE_SELECTOR = (SettingsViewType)localObject;
    localObject = new com/truecaller/ui/SettingsFragment$SettingsViewType;
    int i8 = 13;
    ((SettingsViewType)localObject).<init>("SETTINGS_NOTIFICATION_ACCESS", i8);
    SETTINGS_NOTIFICATION_ACCESS = (SettingsViewType)localObject;
    localObject = new SettingsViewType[14];
    SettingsViewType localSettingsViewType = SETTINGS_GENERAL;
    localObject[0] = localSettingsViewType;
    localSettingsViewType = SETTINGS_CALLERID;
    localObject[i] = localSettingsViewType;
    localSettingsViewType = SETTINGS_PRIVACY;
    localObject[j] = localSettingsViewType;
    localSettingsViewType = SETTINGS_ABOUT;
    localObject[k] = localSettingsViewType;
    localSettingsViewType = SETTINGS_MAIN;
    localObject[m] = localSettingsViewType;
    localSettingsViewType = SETTINGS_APPEARANCE;
    localObject[n] = localSettingsViewType;
    localSettingsViewType = SETTINGS_LANGUAGE;
    localObject[i1] = localSettingsViewType;
    localSettingsViewType = SETTINGS_RINGTONE;
    localObject[i2] = localSettingsViewType;
    localSettingsViewType = SETTINGS_BLOCK;
    localObject[i3] = localSettingsViewType;
    localSettingsViewType = SETTINGS_MESSAGING;
    localObject[i4] = localSettingsViewType;
    localSettingsViewType = SETTINGS_BACKUP;
    localObject[i5] = localSettingsViewType;
    localSettingsViewType = SETTINGS_CALL_RECORDING;
    localObject[i6] = localSettingsViewType;
    localSettingsViewType = SETTINGS_LANGUAGE_SELECTOR;
    localObject[i7] = localSettingsViewType;
    localSettingsViewType = SETTINGS_NOTIFICATION_ACCESS;
    localObject[i8] = localSettingsViewType;
    $VALUES = (SettingsViewType[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.SettingsFragment.SettingsViewType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */