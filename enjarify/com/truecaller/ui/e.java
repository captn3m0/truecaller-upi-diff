package com.truecaller.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.content.d;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.old.data.access.Settings;
import com.truecaller.presence.r;
import com.truecaller.search.local.model.a.o;
import com.truecaller.search.local.model.j;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public abstract class e
  extends Fragment
  implements ab
{
  Handler a;
  private Runnable b;
  private long c;
  private long d;
  private boolean e;
  private final BroadcastReceiver f;
  private f g;
  private r h;
  private com.truecaller.utils.i i;
  private bw j;
  
  public e()
  {
    Object localObject = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    ((Handler)localObject).<init>(localLooper);
    a = ((Handler)localObject);
    localObject = new com/truecaller/ui/e$a;
    ((e.a)localObject).<init>(this, (byte)0);
    f = ((BroadcastReceiver)localObject);
  }
  
  private static Collection a(Collection paramCollection)
  {
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    paramCollection = paramCollection.iterator();
    for (;;)
    {
      boolean bool1 = paramCollection.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject = (j)paramCollection.next();
      int k = 0;
      localObject = ((j)localObject).b().iterator();
      int m;
      do
      {
        boolean bool2 = ((Iterator)localObject).hasNext();
        if (!bool2) {
          break;
        }
        String str = ((o)((Iterator)localObject).next()).a();
        localHashSet.add(str);
        k += 1;
        m = 10;
      } while (k < m);
    }
    return localHashSet;
  }
  
  private void a(long paramLong)
  {
    Object localObject1 = b;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = a;
      ((Handler)localObject2).removeCallbacks((Runnable)localObject1);
    }
    boolean bool = isAdded();
    if (bool)
    {
      localObject1 = i;
      bool = ((com.truecaller.utils.i)localObject1).a();
      if (bool)
      {
        bool = d();
        if (bool)
        {
          localObject1 = new com/truecaller/ui/e$1;
          ((e.1)localObject1).<init>(this);
          b = ((Runnable)localObject1);
          localObject1 = a;
          localObject2 = b;
          ((Handler)localObject1).postDelayed((Runnable)localObject2, paramLong);
          return;
        }
      }
    }
  }
  
  private boolean d()
  {
    boolean bool = e;
    if (bool)
    {
      bool = Settings.f();
      if (!bool)
      {
        bool = Settings.g();
        if (!bool)
        {
          bw localbw = j;
          bool = localbw.a();
          if (!bool) {
            break label43;
          }
        }
      }
      return true;
    }
    label43:
    return false;
  }
  
  protected final void a()
  {
    long l = h.b();
    a(l);
  }
  
  public final void a(boolean paramBoolean)
  {
    e = false;
  }
  
  protected abstract Collection b();
  
  protected abstract void c();
  
  public void m() {}
  
  public void n()
  {
    e = true;
    long l = h.b();
    a(l);
  }
  
  public void onAttach(Context paramContext)
  {
    Object localObject1 = ((bk)paramContext.getApplicationContext()).a();
    Object localObject2 = ((bp)localObject1).ae();
    g = ((f)localObject2);
    localObject2 = ((bp)localObject1).af();
    h = ((r)localObject2);
    localObject2 = ((bp)localObject1).v();
    i = ((com.truecaller.utils.i)localObject2);
    localObject1 = ((bp)localObject1).aG();
    j = ((bw)localObject1);
    super.onAttach(paramContext);
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    a.removeCallbacksAndMessages(null);
  }
  
  public void onResume()
  {
    super.onResume();
    long l = SystemClock.elapsedRealtime();
    c = l;
  }
  
  public void onStart()
  {
    super.onStart();
    Context localContext = requireContext();
    BroadcastReceiver localBroadcastReceiver = f;
    String[] arrayOfString = { "com.truecaller.datamanager.STATUSES_CHANGED" };
    com.truecaller.utils.extensions.i.a(localContext, localBroadcastReceiver, arrayOfString);
  }
  
  public void onStop()
  {
    super.onStop();
    long l = SystemClock.elapsedRealtime();
    d = l;
    d locald = d.a(getActivity());
    BroadcastReceiver localBroadcastReceiver = f;
    locald.a(localBroadcastReceiver);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */