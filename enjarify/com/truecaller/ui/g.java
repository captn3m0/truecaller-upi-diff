package com.truecaller.ui;

import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.view.View;
import android.widget.ListPopupWindow;
import android.widget.ListView;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.calling.dialer.suggested_contacts.f;
import com.truecaller.common.g.a;
import com.truecaller.i.c;
import com.truecaller.util.at;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class g
{
  List a;
  private final g.f b;
  private g.c c;
  private final ListPopupWindow d;
  private final c e;
  
  public g(Context paramContext, View paramView)
  {
    Object localObject1 = ((TrueApp)paramContext.getApplicationContext()).a();
    Object localObject2 = ((bp)localObject1).I();
    String str = "featureFlash";
    boolean bool1 = ((a)localObject2).b(str);
    boolean bool3 = true;
    if (bool1)
    {
      localObject2 = ((bp)localObject1).I();
      localObject3 = "flash_enabled";
      bool1 = ((a)localObject2).b((String)localObject3);
      if (bool1)
      {
        bool1 = true;
        break label86;
      }
    }
    bool1 = false;
    localObject2 = null;
    label86:
    Object localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    Object localObject4 = new com/truecaller/ui/g$g;
    ((g.g)localObject4).<init>((byte)0);
    ((ArrayList)localObject3).add(localObject4);
    localObject4 = new com/truecaller/ui/g$d;
    ((g.d)localObject4).<init>(3, 2131886594, 2131234126);
    ((ArrayList)localObject3).add(localObject4);
    localObject4 = new com/truecaller/ui/g$d;
    ((g.d)localObject4).<init>(4, 2131886596, 2131234128);
    ((ArrayList)localObject3).add(localObject4);
    localObject4 = new com/truecaller/ui/g$d;
    ((g.d)localObject4).<init>(5, 2131886595, 2131234127);
    ((ArrayList)localObject3).add(localObject4);
    localObject4 = new com/truecaller/ui/g$d;
    long l1 = 6;
    int k = 2131886593;
    int m = 2131233879;
    ((g.d)localObject4).<init>(l1, k, m);
    ((ArrayList)localObject3).add(localObject4);
    if (bool1)
    {
      localObject4 = new com/truecaller/ui/g$d;
      l1 = 11;
      k = 2131888106;
      m = 2131234137;
      ((g.d)localObject4).<init>(l1, k, m);
      ((ArrayList)localObject3).add(localObject4);
    }
    localObject4 = new com/truecaller/ui/g$g;
    ((g.g)localObject4).<init>((byte)0);
    ((ArrayList)localObject3).add(localObject4);
    localObject4 = new com/truecaller/ui/g$b;
    ((g.b)localObject4).<init>((byte)0);
    ((ArrayList)localObject3).add(localObject4);
    localObject4 = new com/truecaller/ui/g$g;
    ((g.g)localObject4).<init>((byte)0);
    ((ArrayList)localObject3).add(localObject4);
    localObject4 = new com/truecaller/ui/g$a;
    ((g.a)localObject4).<init>();
    ((ArrayList)localObject3).add(localObject4);
    localObject4 = new com/truecaller/ui/g$d;
    l1 = 7;
    k = 2131888341;
    m = 2131234623;
    ((g.d)localObject4).<init>(l1, k, m);
    ((ArrayList)localObject3).add(localObject4);
    long l2;
    int n;
    if (bool1)
    {
      localObject2 = new com/truecaller/ui/g$d;
      l2 = 12;
      n = 2131888342;
      ((g.d)localObject2).<init>(l2, n, m);
      ((ArrayList)localObject3).add(localObject2);
    }
    localObject2 = new com/truecaller/ui/g$g;
    ((g.g)localObject2).<init>((byte)0);
    ((ArrayList)localObject3).add(localObject2);
    localObject1 = ((bp)localObject1).bq();
    boolean bool4 = ((f)localObject1).b();
    if (bool4)
    {
      localObject1 = new com/truecaller/ui/g$b;
      ((g.b)localObject1).<init>((byte)0);
      ((ArrayList)localObject3).add(localObject1);
      localObject1 = new com/truecaller/ui/g$g;
      ((g.g)localObject1).<init>((byte)0);
      ((ArrayList)localObject3).add(localObject1);
      localObject1 = new com/truecaller/ui/g$d;
      l2 = 9;
      int i = 2131887243;
      n = 2131234665;
      ((g.d)localObject1).<init>(l2, i, n);
      ((ArrayList)localObject3).add(localObject1);
      localObject1 = new com/truecaller/ui/g$g;
      ((g.g)localObject1).<init>((byte)0);
      ((ArrayList)localObject3).add(localObject1);
    }
    localObject1 = (ClipboardManager)paramContext.getSystemService("clipboard");
    boolean bool2 = ((ClipboardManager)localObject1).hasPrimaryClip();
    if (bool2)
    {
      localObject1 = ((ClipboardManager)localObject1).getPrimaryClipDescription();
      localObject2 = "text/plain";
      bool4 = ((ClipDescription)localObject1).hasMimeType((String)localObject2);
      if (bool4)
      {
        localObject1 = new com/truecaller/ui/g$b;
        ((g.b)localObject1).<init>((byte)0);
        ((ArrayList)localObject3).add(localObject1);
        localObject1 = new com/truecaller/ui/g$g;
        ((g.g)localObject1).<init>((byte)0);
        ((ArrayList)localObject3).add(localObject1);
        localObject1 = new com/truecaller/ui/g$d;
        l2 = 10;
        int j = 2131888346;
        n = 2131234361;
        ((g.d)localObject1).<init>(l2, j, n);
        ((ArrayList)localObject3).add(localObject1);
        localObject1 = new com/truecaller/ui/g$g;
        ((g.g)localObject1).<init>((byte)0);
        ((ArrayList)localObject3).add(localObject1);
      }
    }
    localObject1 = Collections.unmodifiableList((List)localObject3);
    a = ((List)localObject1);
    int i1 = at.a(paramContext, 220.0F);
    localObject2 = new com/truecaller/ui/g$f;
    ((g.f)localObject2).<init>(this, paramContext, (byte)0);
    b = ((g.f)localObject2);
    localObject2 = new android/widget/ListPopupWindow;
    ((ListPopupWindow)localObject2).<init>(paramContext);
    d = ((ListPopupWindow)localObject2);
    d.setAnchorView(paramView);
    d.setContentWidth(i1);
    paramContext = d;
    int i2 = -paramView.getHeight();
    paramContext.setVerticalOffset(i2);
    d.setModal(bool3);
    paramContext = d;
    paramView = b;
    paramContext.setAdapter(paramView);
    paramContext = d;
    paramView = new com/truecaller/ui/-$$Lambda$g$bzrc6MxpVcTkhVYc-2GDKs_CtcQ;
    paramView.<init>(this);
    paramContext.setOnItemClickListener(paramView);
    paramContext = TrueApp.y().a().D();
    e = paramContext;
  }
  
  public final void a()
  {
    d.show();
    ListView localListView = d.getListView();
    localListView.setChoiceMode(2);
    localListView.setBackgroundColor(0);
    ListPopupWindow localListPopupWindow = d;
    Object localObject1 = b;
    long l1 = a;
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    String str;
    int j;
    long l3;
    if (bool1)
    {
      i = 0;
      str = null;
      for (;;)
      {
        Object localObject2 = b.a;
        j = ((List)localObject2).size();
        if (i >= j) {
          break;
        }
        localObject2 = b.a.get(i);
        boolean bool2 = localObject2 instanceof g.d;
        if (bool2)
        {
          localObject2 = (g.d)localObject2;
          l3 = a;
          long l4 = a;
          boolean bool3 = l3 < l4;
          if (!bool3) {
            break label162;
          }
        }
        i += 1;
      }
    }
    int i = -1;
    label162:
    localListPopupWindow.setSelection(i);
    int k = 0;
    localListPopupWindow = null;
    for (;;)
    {
      localObject1 = a;
      int m = ((List)localObject1).size();
      if (k >= m) {
        break;
      }
      localObject1 = a.get(k);
      boolean bool4 = localObject1 instanceof g.a;
      if (bool4)
      {
        localObject1 = (g.a)a.get(k);
        long l5 = a;
        l3 = 8;
        boolean bool5 = l5 < l3;
        if (!bool5)
        {
          localObject1 = e;
          str = "merge_by";
          j = 3;
          int n = ((c)localObject1).a(str, j);
          if (n == j)
          {
            n = 1;
          }
          else
          {
            n = 0;
            localObject1 = null;
          }
          localListView.setItemChecked(k, n);
        }
      }
      k += 1;
    }
  }
  
  public final void a(g.c paramc)
  {
    c = paramc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */