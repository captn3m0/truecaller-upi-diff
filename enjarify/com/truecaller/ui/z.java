package com.truecaller.ui;

import android.view.View;
import com.truecaller.adapter_delegates.h;

public final class z
  implements aa.a
{
  public com.truecaller.adapter_delegates.k a;
  
  public z(com.truecaller.adapter_delegates.k paramk)
  {
    a = paramk;
  }
  
  private final boolean a(String paramString, int paramInt, View paramView)
  {
    com.truecaller.adapter_delegates.k localk = a;
    h localh = new com/truecaller/adapter_delegates/h;
    Object localObject = paramView.getTag();
    localh.<init>(paramString, paramInt, -1, paramView, localObject);
    return localk.a(localh);
  }
  
  public final void a(int paramInt, boolean paramBoolean, View paramView)
  {
    String str1 = "view";
    c.g.b.k.b(paramView, str1);
    String str2;
    if (paramBoolean) {
      str2 = "ItemEvent.SWIPE_COMPLETED_FROM_START";
    } else {
      str2 = "ItemEvent.SWIPE_COMPLETED_FROM_END";
    }
    a(str2, paramInt, paramView);
  }
  
  public final boolean a(int paramInt, View paramView)
  {
    c.g.b.k.b(paramView, "view");
    return a("ItemEvent.SWIPE_START", paramInt, paramView);
  }
  
  public final void d() {}
}

/* Location:
 * Qualified Name:     com.truecaller.ui.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */