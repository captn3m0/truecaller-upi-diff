package com.truecaller.ui;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.flashsdk.ui.CompoundFlashButton;
import com.truecaller.old.data.access.Settings;
import com.truecaller.search.local.model.j;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.util.at;
import com.truecaller.utils.ui.b;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class v
  extends d.g
{
  public final Context c;
  public final View d;
  public final ContactPhoto e;
  public final TextView f;
  public final TextView g;
  public final TextView h;
  public final CompoundFlashButton i;
  public final View j;
  public final ImageView k;
  private WeakReference l;
  private a m;
  private a n;
  
  public v(View paramView, ViewGroup paramViewGroup)
  {
    super(paramView, paramViewGroup);
    paramViewGroup = paramView.getContext();
    c = paramViewGroup;
    paramViewGroup = (TextView)paramView.findViewById(2131363718);
    f = paramViewGroup;
    paramViewGroup = (TextView)paramView.findViewById(2131364281);
    g = paramViewGroup;
    paramViewGroup = (TextView)paramView.findViewById(2131362066);
    h = paramViewGroup;
    paramViewGroup = paramView.findViewById(2131362071);
    d = paramViewGroup;
    paramViewGroup = (CompoundFlashButton)paramView.findViewById(2131363109);
    i = paramViewGroup;
    paramViewGroup = (ImageView)paramView.findViewById(2131361918);
    k = paramViewGroup;
    int i1 = 2131361842;
    paramViewGroup = paramView.findViewById(i1);
    j = paramViewGroup;
    paramViewGroup = j;
    Object localObject;
    if (paramViewGroup != null)
    {
      localObject = new com/truecaller/ui/-$$Lambda$v$NZyic9q0R7qD76UQ506iFJ4QZjI;
      ((-..Lambda.v.NZyic9q0R7qD76UQ506iFJ4QZjI)localObject).<init>(this);
      paramViewGroup.setOnClickListener((View.OnClickListener)localObject);
    }
    i1 = 2131362554;
    paramViewGroup = paramView.findViewById(i1);
    boolean bool = paramViewGroup instanceof ContactPhoto;
    if (bool)
    {
      paramViewGroup = (ContactPhoto)paramViewGroup;
      e = paramViewGroup;
      paramViewGroup = e;
      localObject = new com/truecaller/ui/-$$Lambda$v$ZcxU7I2x7BgQHp5BJnjWwdQic2w;
      ((-..Lambda.v.ZcxU7I2x7BgQHp5BJnjWwdQic2w)localObject).<init>(this);
      paramViewGroup.setOnClickListener((View.OnClickListener)localObject);
    }
    else
    {
      i1 = 0;
      paramViewGroup = null;
      e = null;
    }
    paramViewGroup = b.c(paramView.getContext(), 2130969534);
    paramView.setBackgroundDrawable(paramViewGroup);
  }
  
  private void a(List paramList, String paramString1, String paramString2)
  {
    int i1 = 8;
    if (paramList != null)
    {
      boolean bool1 = Settings.g();
      if (bool1)
      {
        ArrayList localArrayList = new java/util/ArrayList;
        localArrayList.<init>();
        try
        {
          paramList = paramList.iterator();
          for (;;)
          {
            boolean bool2 = paramList.hasNext();
            if (!bool2) {
              break;
            }
            Object localObject = paramList.next();
            localObject = (String)localObject;
            String str1 = "+";
            String str2 = "";
            localObject = ((String)localObject).replace(str1, str2);
            long l1 = Long.parseLong((String)localObject);
            localObject = Long.valueOf(l1);
            localArrayList.add(localObject);
          }
          i.b();
        }
        catch (NumberFormatException localNumberFormatException)
        {
          i.a(localArrayList, paramString1, paramString2);
          i.setVisibility(0);
          j.setVisibility(0);
          k.setVisibility(i1);
          return;
        }
      }
    }
    i.setVisibility(i1);
    b();
  }
  
  private void b()
  {
    Object localObject = i;
    int i1 = ((CompoundFlashButton)localObject).getVisibility();
    if (i1 != 0)
    {
      localObject = k;
      a locala = m;
      int i2 = 0;
      int i3;
      if (locala != null)
      {
        i3 = 0;
        locala = null;
      }
      else
      {
        i3 = 8;
      }
      ((ImageView)localObject).setVisibility(i3);
      localObject = j;
      locala = m;
      if (locala == null) {
        i2 = 8;
      }
      ((View)localObject).setVisibility(i2);
    }
  }
  
  public final Object a()
  {
    WeakReference localWeakReference = l;
    if (localWeakReference == null) {
      return null;
    }
    return localWeakReference.get();
  }
  
  protected final void a(j paramj, String paramString)
  {
    if (paramj == null)
    {
      a(null, null, paramString);
      return;
    }
    List localList = paramj.h();
    paramj = f;
    a(localList, paramj, paramString);
  }
  
  public final void a(a parama)
  {
    m = parama;
    b();
  }
  
  public final void a(CharSequence paramCharSequence)
  {
    boolean bool = TextUtils.isEmpty(paramCharSequence);
    if (bool)
    {
      g.setVisibility(8);
      g.setText(null);
      return;
    }
    g.setVisibility(0);
    TextView localTextView = g;
    paramCharSequence = at.a(paramCharSequence);
    localTextView.setText(paramCharSequence);
  }
  
  public final void a(Object paramObject)
  {
    if (paramObject == null)
    {
      l = null;
      return;
    }
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramObject);
    l = localWeakReference;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */