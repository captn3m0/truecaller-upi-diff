package com.truecaller.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;
import com.truecaller.common.b.a;
import com.truecaller.log.d;
import com.truecaller.old.a.c;
import com.truecaller.ui.dialogs.d.a;
import com.truecaller.ui.dialogs.g;
import com.truecaller.util.dh;
import com.truecaller.util.r;
import com.truecaller.wizard.utils.i;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class o
  extends Fragment
  implements c, d.a, p
{
  private final AtomicBoolean a;
  protected Dialog j;
  public boolean k;
  
  public o()
  {
    AtomicBoolean localAtomicBoolean = new java/util/concurrent/atomic/AtomicBoolean;
    localAtomicBoolean.<init>(false);
    a = localAtomicBoolean;
  }
  
  public static void a(Context paramContext, String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramString);
    paramContext.sendBroadcast(localIntent);
  }
  
  public boolean W_()
  {
    return false;
  }
  
  protected void a() {}
  
  public final void ac_()
  {
    b(2131886537);
  }
  
  public final void b(int paramInt)
  {
    Context localContext = getContext();
    if (localContext != null)
    {
      Toast localToast = Toast.makeText(localContext, paramInt, 0);
      localToast.show();
    }
  }
  
  public final void c(String paramString)
  {
    try
    {
      Context localContext = getContext();
      if (localContext != null)
      {
        paramString = Toast.makeText(localContext, paramString, 0);
        paramString.show();
      }
      return;
    }
    finally {}
  }
  
  public final void d(String paramString)
  {
    dh.a(getActivity(), paramString, false);
  }
  
  public void d_(boolean paramBoolean)
  {
    boolean bool = isFinishing();
    if (bool) {
      return;
    }
    try
    {
      Object localObject = j;
      if (localObject == null)
      {
        localObject = new com/truecaller/ui/dialogs/g;
        f localf = getActivity();
        ((g)localObject).<init>(localf, paramBoolean);
        j = ((Dialog)localObject);
      }
      Dialog localDialog = j;
      localDialog.show();
      return;
    }
    catch (Exception localException)
    {
      d.a(localException, "TCActivity Exception while showing loading dialog");
    }
  }
  
  public void e()
  {
    boolean bool = isFinishing();
    if (bool) {
      return;
    }
    try
    {
      Dialog localDialog = j;
      if (localDialog != null)
      {
        localDialog = j;
        localDialog.dismiss();
      }
      return;
    }
    catch (Exception localException)
    {
      d.a(localException, "TCActivity Exception while dismissing loading dialog");
    }
  }
  
  protected final void e(String paramString)
  {
    r.a(getActivity(), paramString, null);
    Toast.makeText(getContext(), 2131887203, 0).show();
  }
  
  public boolean isFinishing()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf = getActivity();
      boolean bool = localf.isFinishing();
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  protected final ActionBar k()
  {
    return ((n)getActivity()).getSupportActionBar();
  }
  
  protected final Toolbar l()
  {
    return getActivityh;
  }
  
  protected final View o()
  {
    f localf = getActivity();
    if (localf == null) {
      return null;
    }
    return getActivity().findViewById(16908290);
  }
  
  public void onAttach(Activity paramActivity)
  {
    boolean bool = ((a)paramActivity.getApplication()).p();
    k = bool;
    super.onAttach(paramActivity);
    a.set(true);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    a();
  }
  
  public void onDetach()
  {
    super.onDetach();
    new String[1][0] = "FragmentBase onDetach";
    a.set(false);
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    i.a(paramArrayOfString, paramArrayOfInt);
  }
  
  public void onResume()
  {
    super.onResume();
    a locala = (a)getActivity().getApplication();
    boolean bool1 = locala.p();
    k = bool1;
    boolean bool2 = locala.o();
    if (bool2)
    {
      bool2 = k;
      if (!bool2)
      {
        new String[1][0] = "CLEAR onResume";
        new String[1][0] = "CLEAR reset";
        TruecallerInit.b(getActivity(), "search", null);
        getActivity().finish();
        return;
      }
    }
    p();
  }
  
  public void onStart()
  {
    super.onStart();
  }
  
  public void onStop()
  {
    e();
    j = null;
    super.onStop();
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    boolean bool = isAdded();
    if (bool)
    {
      paramView = a;
      bool = paramView.get();
      if (bool) {}
    }
    else
    {
      new String[1][0] = "FragmentBase onViewCreated - Fragment Not Attached";
      paramView = getActivity();
      paramBundle = "search";
      TruecallerInit.b(paramView, paramBundle, null);
      paramView = getActivity();
      paramView.finish();
    }
  }
  
  public void p() {}
  
  public void q() {}
  
  public final void r()
  {
    new String[1][0] = "FragmentBase --> DialogBase.onDialogNo clicked.";
  }
  
  public final boolean s()
  {
    f localf = getActivity();
    if (localf != null)
    {
      boolean bool = isDetached();
      if (!bool) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */