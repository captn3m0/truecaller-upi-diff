package com.truecaller.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.view.r;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.State;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;
import com.truecaller.common.e.f;
import com.truecaller.utils.ui.b;

public final class q
  extends RecyclerView.ItemDecoration
{
  public boolean a;
  private final int b;
  private final Drawable c;
  private final Paint d;
  private Paint e;
  private View f;
  private TextView g;
  
  public q(Context paramContext, int paramInt)
  {
    this(paramContext, paramInt, i);
  }
  
  public q(Context paramContext, int paramInt1, int paramInt2)
  {
    int i = paramContext.getResources().getDimensionPixelSize(2131165866);
    b = i;
    Object localObject1 = b.c(paramContext, 16843284);
    c = ((Drawable)localObject1);
    localObject1 = new android/graphics/Paint;
    ((Paint)localObject1).<init>();
    d = ((Paint)localObject1);
    d.setColor(paramInt2);
    Object localObject2 = d;
    localObject1 = Paint.Style.FILL;
    ((Paint)localObject2).setStyle((Paint.Style)localObject1);
    Object localObject3 = LayoutInflater.from(paramContext).inflate(paramInt1, null);
    f = ((View)localObject3);
    localObject3 = f;
    localObject2 = new android/support/v7/widget/RecyclerView$LayoutParams;
    i = b;
    ((RecyclerView.LayoutParams)localObject2).<init>(-1, i);
    ((View)localObject3).setLayoutParams((ViewGroup.LayoutParams)localObject2);
    localObject3 = f;
    paramInt2 = f.b();
    r.b((View)localObject3, paramInt2);
    localObject3 = (TextView)f.findViewById(2131363268);
    g = ((TextView)localObject3);
    localObject3 = new android/graphics/Paint;
    ((Paint)localObject3).<init>();
    localObject2 = new android/graphics/PorterDuffColorFilter;
    int j = b.a(paramContext, 16842808);
    localObject1 = PorterDuff.Mode.SRC_IN;
    ((PorterDuffColorFilter)localObject2).<init>(j, (PorterDuff.Mode)localObject1);
    ((Paint)localObject3).setColorFilter((ColorFilter)localObject2);
    g.setLayerType(2, (Paint)localObject3);
  }
  
  private void a(View paramView)
  {
    int i = paramView.getWidth();
    int j = 1073741824;
    i = View.MeasureSpec.makeMeasureSpec(i, j);
    j = View.MeasureSpec.makeMeasureSpec(b, j);
    f.measure(i, j);
  }
  
  public final void a()
  {
    Paint localPaint1 = new android/graphics/Paint;
    Paint localPaint2 = d;
    localPaint1.<init>(localPaint2);
    e = localPaint1;
    e.setColor(0);
  }
  
  public final void b()
  {
    a = false;
  }
  
  public final void getItemOffsets(Rect paramRect, View paramView, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    super.getItemOffsets(paramRect, paramView, paramRecyclerView, paramState);
    paramView = paramRecyclerView.getChildViewHolder(paramView);
    boolean bool = paramView instanceof q.a;
    if (!bool) {
      return;
    }
    paramView = (q.a)paramView;
    paramRect.setEmpty();
    paramView = paramView.a();
    if (paramView != null)
    {
      a(paramRecyclerView);
      int i = top;
      paramRecyclerView = f;
      int j = paramRecyclerView.getMeasuredHeight();
      i += j;
      top = i;
    }
  }
  
  public final void onDraw(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    q localq = this;
    Canvas localCanvas = paramCanvas;
    RecyclerView localRecyclerView = paramRecyclerView;
    super.onDraw(paramCanvas, paramRecyclerView, paramState);
    a(paramRecyclerView);
    Object localObject1 = f;
    int i = ((View)localObject1).getMeasuredWidth();
    View localView1 = f;
    int j = localView1.getMeasuredHeight();
    ((View)localObject1).layout(0, 0, i, j);
    int k = 0;
    for (;;)
    {
      int m = paramRecyclerView.getChildCount();
      if (k >= m) {
        break;
      }
      View localView2 = localRecyclerView.getChildAt(k);
      Object localObject2 = localRecyclerView.getChildViewHolder(localView2);
      boolean bool1 = localObject2 instanceof q.a;
      if (bool1)
      {
        paramCanvas.save();
        Object localObject3 = localObject2;
        localObject3 = (q.a)localObject2;
        String str = ((q.a)localObject3).a();
        float f1 = localView2.getLeft();
        float f2 = localView2.getTranslationX() + f1;
        float f3 = localView2.getTop();
        f1 = localView2.getRight();
        float f4 = localView2.getTranslationX() + f1;
        float f5 = localView2.getBottom();
        Object localObject4 = d;
        localObject1 = paramCanvas;
        paramCanvas.drawRect(f2, f3, f4, f5, (Paint)localObject4);
        bool1 = ((q.a)localObject3).b();
        if (!bool1)
        {
          bool1 = a;
          if (!bool1) {}
        }
        else
        {
          localObject1 = c;
          i = localView2.getLeft();
          j = localView2.getBottom();
          int i1 = localView2.getRight();
          int i2 = localView2.getBottom();
          localObject4 = c;
          int i3 = ((Drawable)localObject4).getIntrinsicHeight();
          i2 += i3;
          ((Drawable)localObject1).setBounds(i, j, i1, i2);
          localObject1 = c;
          ((Drawable)localObject1).draw(localCanvas);
        }
        if (str != null)
        {
          f1 = 0.0F;
          i = localView2.getTop();
          j = b;
          i -= j;
          f2 = i;
          localCanvas.translate(0.0F, f2);
          f2 = localView2.getLeft();
          j = 0;
          f3 = 0.0F;
          localView1 = null;
          f4 = localView2.getRight();
          int n = b;
          f5 = n;
          localObject1 = e;
          if (localObject1 == null) {
            localObject1 = d;
          }
          localObject4 = localObject1;
          localObject1 = paramCanvas;
          paramCanvas.drawRect(f2, 0.0F, f4, f5, (Paint)localObject4);
          localObject1 = g;
          ((TextView)localObject1).setText(str);
          boolean bool2 = localObject2 instanceof q.c;
          if (bool2)
          {
            localObject1 = localObject2;
            localObject1 = (q.c)localObject2;
            TextView localTextView = g;
            localObject1 = ((q.c)localObject1).c();
            j = 0;
            f3 = 0.0F;
            localView1 = null;
            localTextView.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable)localObject1, null, null, null);
          }
          bool2 = localObject2 instanceof q.b;
          if (bool2)
          {
            localObject2 = (q.b)localObject2;
            localObject1 = g;
            i = ((q.b)localObject2).c();
            ((TextView)localObject1).setCompoundDrawablesRelativeWithIntrinsicBounds(i, 0, 0, 0);
          }
          g.destroyDrawingCache();
          localObject1 = f;
          ((View)localObject1).draw(localCanvas);
        }
        paramCanvas.restore();
      }
      k += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */