package com.truecaller.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.truecaller.old.data.access.Settings;
import com.truecaller.search.local.b.c;
import com.truecaller.search.local.b.f;
import com.truecaller.search.local.model.j;
import com.truecaller.ui.components.SideIndexScroller.a;
import java.util.HashMap;
import java.util.Map;

public final class h
  extends d
  implements SideIndexScroller.a
{
  a k;
  private final Map l;
  private final Map m;
  private final boolean n;
  
  public h(Context paramContext, boolean paramBoolean)
  {
    super(paramContext);
    paramContext = new java/util/HashMap;
    paramContext.<init>();
    l = paramContext;
    paramContext = akb;
    m = paramContext;
    n = paramBoolean;
  }
  
  public final int a(char paramChar)
  {
    Map localMap = l;
    Object localObject = Character.valueOf(paramChar);
    localObject = (Integer)localMap.get(localObject);
    if (localObject == null) {
      return -1;
    }
    return ((Integer)localObject).intValue();
  }
  
  public final h.a a(ViewGroup paramViewGroup)
  {
    h.a locala = new com/truecaller/ui/h$a;
    LayoutInflater localLayoutInflater = e;
    locala.<init>(localLayoutInflater, paramViewGroup);
    return locala;
  }
  
  protected final void a()
  {
    super.a();
    Map localMap1 = l;
    localMap1.clear();
    int i = getItemCount();
    int j = 0;
    while (j < i)
    {
      char c = b(j);
      Map localMap2 = l;
      Object localObject = Character.valueOf(c);
      boolean bool = localMap2.containsKey(localObject);
      if (!bool)
      {
        localMap2 = l;
        Character localCharacter = Character.valueOf(c);
        localObject = Integer.valueOf(j);
        localMap2.put(localCharacter, localObject);
      }
      j += 1;
    }
  }
  
  protected final void a(c.a parama1, c.a parama2)
  {
    e = 0;
    e = 0;
  }
  
  public final char b(int paramInt)
  {
    Object localObject = a(paramInt);
    int i = 63;
    if (localObject == null) {
      return i;
    }
    localObject = (j)localObject;
    boolean bool = ((j)localObject).e();
    if (bool)
    {
      i = c.c(f.trim());
      localObject = m;
      Character localCharacter = Character.valueOf(i);
      localObject = (Character)((Map)localObject).get(localCharacter);
      if (localObject != null) {
        i = ((Character)localObject).charValue();
      }
    }
    paramInt = 43;
    if (i != paramInt)
    {
      paramInt = 48;
      if (paramInt <= i)
      {
        paramInt = 57;
        if (i <= paramInt) {}
      }
      else
      {
        return i;
      }
    }
    return '#';
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */