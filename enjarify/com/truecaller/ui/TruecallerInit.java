package com.truecaller.ui;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract.Contacts;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.BaseBehavior.a;
import android.support.design.widget.AppBarLayout.Behavior;
import android.support.design.widget.AppBarLayout.b;
import android.support.design.widget.AppBarLayout.c;
import android.support.design.widget.CoordinatorLayout.b;
import android.support.design.widget.CoordinatorLayout.e;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.a;
import android.support.v4.app.Fragment;
import android.support.v4.view.r;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.c;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.graphics.drawable.DrawerArrowDrawable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import c.l.g;
import com.google.android.gms.appinvite.AppInvite;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.truecaller.TrueApp;
import com.truecaller.analytics.TimingEvent;
import com.truecaller.analytics.au;
import com.truecaller.analytics.az;
import com.truecaller.analytics.bb;
import com.truecaller.analytics.bc;
import com.truecaller.bp;
import com.truecaller.calling.dialer.aj.b;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.calling.recorder.CallRecordingOnBoardingLaunchContext;
import com.truecaller.calling.recorder.CallRecordingOnBoardingState;
import com.truecaller.calling.recorder.aj;
import com.truecaller.calling.recorder.bj;
import com.truecaller.common.h.ac;
import com.truecaller.common.h.am;
import com.truecaller.common.ui.b.a;
import com.truecaller.consentrefresh.ConsentRefreshActivity;
import com.truecaller.engagementrewards.EngagementRewardActionType;
import com.truecaller.engagementrewards.EngagementRewardState;
import com.truecaller.filters.blockedevents.BlockedEventsActivity;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.data.ak;
import com.truecaller.messaging.data.al;
import com.truecaller.messaging.data.al.a;
import com.truecaller.old.data.access.Settings;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.ba;
import com.truecaller.premium.ba.b;
import com.truecaller.premium.br;
import com.truecaller.profile.EditMeFormFragment;
import com.truecaller.profile.ProfileActivity;
import com.truecaller.profile.business.CreateBusinessProfileActivity;
import com.truecaller.referral.ReferralManager;
import com.truecaller.referral.ReferralManager.ReferralLaunchContext;
import com.truecaller.referral.ai;
import com.truecaller.scanner.NumberDetectorProcessor.ScanType;
import com.truecaller.scanner.NumberScannerActivity;
import com.truecaller.scanner.barcode.BarcodeCaptureActivity;
import com.truecaller.scanner.f.a;
import com.truecaller.scanner.s;
import com.truecaller.sdk.ConfirmProfileActivity;
import com.truecaller.service.AlarmReceiver;
import com.truecaller.service.DataManagerService;
import com.truecaller.service.SyncPhoneBookService;
import com.truecaller.startup_dialogs.StartupDialogDismissReason;
import com.truecaller.startup_dialogs.StartupDialogType;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.base.views.fragments.TcPayOnFragmentInteractionListener;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import com.truecaller.ui.components.DrawerHeaderView;
import com.truecaller.ui.components.DrawerHeaderView.a;
import com.truecaller.ui.components.FloatingActionButton.a;
import com.truecaller.ui.components.FloatingActionButton.c;
import com.truecaller.ui.components.FloatingActionButton.c.a;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.ui.view.BadgeDrawerArrowDrawable;
import com.truecaller.ui.view.BadgeDrawerArrowDrawable.1;
import com.truecaller.ui.view.BadgeDrawerArrowDrawable.BadgeTag;
import com.truecaller.ui.view.BottomBar;
import com.truecaller.ui.view.BottomBar.a;
import com.truecaller.update.ForcedUpdate;
import com.truecaller.util.b.e.1;
import com.truecaller.util.b.j.a;
import com.truecaller.util.co;
import com.truecaller.whoviewedme.WhoViewedMeActivity;
import com.truecaller.whoviewedme.WhoViewedMeLaunchContext;
import com.truecaller.wizard.utils.PermissionPoller;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TruecallerInit
  extends n
  implements GoogleApiClient.OnConnectionFailedListener, com.truecaller.common.ui.a, b.a, al.a, f.a, com.truecaller.startup_dialogs.a, TcPayOnFragmentInteractionListener, ae.a, ag.a, DrawerHeaderView.a, FloatingActionButton.c.a, BottomBar.a
{
  private AppBarLayout.c A;
  private String B;
  private int C;
  private boolean D;
  private boolean E;
  private boolean F;
  private boolean G;
  private boolean H;
  private com.truecaller.service.h I;
  private PermissionPoller J;
  private String K;
  private BroadcastReceiver L;
  private android.support.v4.app.j M;
  private ReferralManager N;
  private GoogleApiClient O;
  private com.truecaller.featuretoggles.e P;
  private com.truecaller.utils.d Q;
  private com.truecaller.i.e R;
  private com.truecaller.utils.l S;
  private com.truecaller.scanner.n T;
  private al U;
  private final BroadcastReceiver V;
  private final BroadcastReceiver W;
  private final BroadcastReceiver X;
  private Handler Y;
  final bp a;
  protected Toolbar b;
  protected View c;
  protected AppBarLayout d;
  protected DrawerLayout i;
  protected NavigationView j;
  protected DrawerHeaderView k;
  protected ActionBarDrawerToggle l;
  protected BottomBar m;
  protected BadgeDrawerArrowDrawable n;
  public com.truecaller.startup_dialogs.c o;
  public com.truecaller.g.a p;
  public au q;
  public br r;
  public com.truecaller.analytics.a.f s;
  public ag t;
  com.truecaller.common.g.a u;
  private TrueApp v;
  private TruecallerInit.b w;
  private boolean x;
  private com.truecaller.ui.view.c y;
  private com.truecaller.ui.view.c z;
  
  public TruecallerInit()
  {
    Object localObject = TrueApp.y();
    v = ((TrueApp)localObject);
    localObject = v.a();
    a = ((bp)localObject);
    boolean bool = true;
    x = bool;
    C = 0;
    D = false;
    E = false;
    F = false;
    G = bool;
    H = false;
    K = null;
    localObject = new com/truecaller/ui/TruecallerInit$1;
    ((TruecallerInit.1)localObject).<init>(this);
    V = ((BroadcastReceiver)localObject);
    localObject = new com/truecaller/ui/TruecallerInit$2;
    ((TruecallerInit.2)localObject).<init>(this);
    W = ((BroadcastReceiver)localObject);
    localObject = new com/truecaller/ui/TruecallerInit$3;
    ((TruecallerInit.3)localObject).<init>(this);
    X = ((BroadcastReceiver)localObject);
    localObject = new com/truecaller/ui/TruecallerInit$4;
    ((TruecallerInit.4)localObject).<init>(this);
    Y = ((Handler)localObject);
  }
  
  public static Intent a(Context paramContext, String paramString)
  {
    return a(paramContext, "calls", paramString, null);
  }
  
  public static Intent a(Context paramContext, String paramString1, String paramString2)
  {
    return a(paramContext, paramString1, paramString2, null);
  }
  
  public static Intent a(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, TruecallerInit.class);
    paramContext = localIntent.putExtra("ARG_FRAGMENT", paramString1).setFlags(335609856);
    bb.a(paramContext, paramString2, paramString3);
    return paramContext;
  }
  
  public static void a(Activity paramActivity, String paramString1, String paramString2)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramActivity, TruecallerInit.class);
    paramString1 = localIntent.putExtra("ARG_FRAGMENT", paramString1).setFlags(67174400);
    bb.a(paramString1, paramString2, null);
    paramActivity.startActivity(paramString1);
  }
  
  private static void a(Context paramContext)
  {
    Intent localIntent = com.truecaller.common.h.o.a(paramContext).addFlags(268435456);
    com.truecaller.common.h.o.a(paramContext, localIntent);
  }
  
  public static void a(Context paramContext, String paramString1, boolean paramBoolean, String paramString2)
  {
    paramString1 = a(paramContext, paramString1, paramString2, null);
    int i1 = 268435456;
    paramString1.addFlags(i1);
    if (paramBoolean)
    {
      paramBoolean = 32768;
      paramString1.addFlags(paramBoolean);
    }
    paramContext.startActivity(paramString1);
  }
  
  private void a(Intent paramIntent)
  {
    String str = "android.permission.CALL_PHONE";
    int i1 = android.support.v4.app.a.a(this, str);
    if (i1 != 0)
    {
      paramIntent = new String[] { "android.permission.CALL_PHONE" };
      android.support.v4.app.a.a(this, paramIntent, 7004);
      return;
    }
    com.truecaller.util.a.a(this, paramIntent);
  }
  
  private void a(Fragment paramFragment)
  {
    boolean bool = paramFragment instanceof com.truecaller.common.ui.b;
    if (bool)
    {
      paramFragment = (com.truecaller.common.ui.b)paramFragment;
      int i1 = paramFragment.f();
      View localView = c;
      localView.setVisibility(i1);
    }
  }
  
  private void a(Menu paramMenu)
  {
    paramMenu = paramMenu.findItem(2131362904);
    boolean bool = a.bh().a();
    paramMenu.setVisible(bool);
  }
  
  private void a(Integer paramInteger)
  {
    BottomBar localBottomBar = m;
    if ((localBottomBar != null) && (paramInteger != null))
    {
      String str = "calls";
      int i1 = paramInteger.intValue();
      localBottomBar.a(str, i1);
    }
  }
  
  public static void b(Context paramContext, String paramString)
  {
    a(paramContext, "calls", false, paramString);
  }
  
  public static void b(Context paramContext, String paramString1, String paramString2)
  {
    a(paramContext, paramString1, false, paramString2);
  }
  
  private void b(Intent paramIntent)
  {
    paramIntent = paramIntent.getData();
    Object localObject1;
    int i3;
    Object localObject2;
    Object localObject3;
    Object localObject4;
    String str2;
    if (paramIntent != null)
    {
      localObject1 = paramIntent.getHost();
      boolean bool1 = am.b((CharSequence)localObject1);
      if (!bool1)
      {
        localObject1 = paramIntent.getHost();
        i3 = 2131888543;
        localObject2 = getString(i3);
        bool1 = ((String)localObject1).equals(localObject2);
        if (bool1)
        {
          localObject1 = v;
          bool1 = ((TrueApp)localObject1).isTcPayEnabled();
          if (bool1)
          {
            localObject1 = getString(2131888546);
            localObject2 = paramIntent.getPath();
            bool1 = ((String)localObject1).equals(localObject2);
            if (bool1)
            {
              localObject1 = getString(2131888554);
              localObject3 = paramIntent.getQueryParameter((String)localObject1);
              localObject1 = getString(2131888541);
              String str1 = paramIntent.getQueryParameter((String)localObject1);
              int i1 = 2131888539;
              localObject2 = getString(i1);
              localObject4 = paramIntent.getQueryParameter((String)localObject2);
              localObject1 = getString(i1);
              str2 = paramIntent.getQueryParameter((String)localObject1);
              localObject1 = getString(2131888545);
              String str3 = paramIntent.getQueryParameter((String)localObject1);
              localObject2 = this;
              TransactionActivity.startForSend(this, (String)localObject4, (String)localObject3, str3, str1, str2);
              return;
            }
          }
          Toast.makeText(this, 2131888542, 1).show();
          return;
        }
      }
    }
    if (paramIntent != null)
    {
      localObject1 = paramIntent.getHost();
      boolean bool2 = am.b((CharSequence)localObject1);
      if (!bool2)
      {
        localObject1 = paramIntent.getHost();
        i3 = 2131888080;
        localObject2 = getString(i3);
        bool2 = ((String)localObject1).equals(localObject2);
        if (bool2)
        {
          bool2 = Settings.g();
          if (bool2)
          {
            int i2 = 2131888108;
            localObject1 = getString(i2);
            localObject1 = paramIntent.getQueryParameter((String)localObject1);
            i3 = 2131888107;
            localObject2 = getString(i3);
            str2 = paramIntent.getQueryParameter((String)localObject2);
            boolean bool3 = am.b((CharSequence)localObject1);
            if (!bool3)
            {
              int i4 = ((String)localObject1).length();
              i3 = 7;
              if (i4 > i3)
              {
                paramIntent = new java/lang/StringBuilder;
                paramIntent.<init>("+");
                localObject2 = ((String)localObject1).trim();
                paramIntent.append((String)localObject2);
                paramIntent = paramIntent.toString();
                localObject2 = v;
                boolean bool4 = ((TrueApp)localObject2).b(paramIntent);
                if (bool4) {
                  try
                  {
                    localObject4 = com.truecaller.flashsdk.core.c.a();
                    paramIntent = ((String)localObject1).trim();
                    long l1 = Long.parseLong(paramIntent);
                    String str4 = "deepLink";
                    localObject3 = this;
                    ((com.truecaller.flashsdk.core.b)localObject4).a(this, l1, str2, str4);
                    return;
                  }
                  catch (NumberFormatException localNumberFormatException) {}
                }
              }
            }
            int i5 = 2131888457;
            i2 = 0;
            localObject1 = null;
            paramIntent = Toast.makeText(this, i5, 0);
            paramIntent.show();
          }
        }
      }
    }
  }
  
  public static void c(Context paramContext, String paramString)
  {
    a(paramContext, "calls", true, paramString);
  }
  
  private boolean c(Intent paramIntent)
  {
    try
    {
      Context localContext = getApplicationContext();
      com.truecaller.common.h.ab.a(paramIntent, localContext);
      return true;
    }
    catch (SecurityException localSecurityException)
    {
      new String[1][0] = "Failed to parse phone number";
    }
    return false;
  }
  
  private void d(Intent paramIntent)
  {
    paramIntent = com.truecaller.notifications.m.a(paramIntent.getStringExtra("HTML_PAGE"));
    FragmentManager localFragmentManager = getFragmentManager();
    paramIntent.show(localFragmentManager, "dialog");
  }
  
  private void d(String paramString)
  {
    Object localObject1 = getIntent().getExtras();
    if (localObject1 != null)
    {
      localObject1 = "app_shortcut";
      localObject2 = getIntent().getExtras();
      str = "deeplink_source";
      localObject2 = ((Bundle)localObject2).getString(str);
      boolean bool = ((String)localObject1).equals(localObject2);
      if (bool)
      {
        localObject1 = "homescreenShortcut";
        K = ((String)localObject1);
      }
    }
    localObject1 = TrueApp.y().a().c();
    Object localObject2 = new com/truecaller/analytics/bc;
    String str = K;
    ((bc)localObject2).<init>(paramString, str);
    ((com.truecaller.analytics.b)localObject1).a((com.truecaller.analytics.e)localObject2);
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    localObject2 = "ViewId";
    ((Map)localObject1).put(localObject2, paramString);
    paramString = K;
    if (paramString != null)
    {
      localObject2 = "Context";
      ((Map)localObject1).put(localObject2, paramString);
    }
    paramString = ao.b().a("ViewVisited").a((Map)localObject1);
    localObject1 = com.truecaller.truepay.data.b.a.a();
    paramString = paramString.b((CharSequence)localObject1).a();
    ((com.truecaller.analytics.ae)a.f().a()).a(paramString);
  }
  
  private void n()
  {
    Object localObject1 = getIntent();
    Object localObject2 = ((Intent)localObject1).getExtras();
    if (localObject2 == null) {
      return;
    }
    String str1 = ((Bundle)localObject2).getString("view");
    String str2 = SettingsFragment.SettingsViewType.SETTINGS_MAIN.name();
    String str3 = ((Bundle)localObject2).getString("subview", str2);
    str2 = ((Bundle)localObject2).getString("c");
    int i1 = 1;
    Object localObject3 = new String[i1];
    Object localObject4 = new java/lang/StringBuilder;
    ((StringBuilder)localObject4).<init>("processDeepLinks:: Action: ");
    String str4 = ((Intent)localObject1).getAction();
    ((StringBuilder)localObject4).append(str4);
    ((StringBuilder)localObject4).append("Screen: ");
    ((StringBuilder)localObject4).append(str1);
    ((StringBuilder)localObject4).append(" sub screen: ");
    ((StringBuilder)localObject4).append(str3);
    localObject4 = ((StringBuilder)localObject4).toString();
    int i2 = 0;
    str4 = null;
    localObject3[0] = localObject4;
    localObject1 = ((Intent)localObject1).getAction();
    localObject3 = "android.intent.action.VIEW";
    boolean bool1 = am.a((CharSequence)localObject1, (CharSequence)localObject3);
    if ((bool1) && (str1 != null))
    {
      int i3 = str1.hashCode();
      int i14 = 4;
      int i15 = 5;
      boolean bool3;
      boolean bool8;
      switch (i3)
      {
      default: 
        break;
      case 1449811358: 
        localObject1 = "defaultdialer";
        boolean bool2 = str1.equals(localObject1);
        if (bool2) {
          int i4 = 10;
        }
        break;
      case 1434631203: 
        localObject1 = "settings";
        bool3 = str1.equals(localObject1);
        if (bool3) {
          bool3 = true;
        }
        break;
      case 1101712723: 
        localObject1 = "callrecording";
        bool3 = str1.equals(localObject1);
        if (bool3) {
          int i5 = 9;
        }
        break;
      case 85982298: 
        localObject1 = "whoviewedme";
        boolean bool4 = str1.equals(localObject1);
        if (bool4) {
          int i6 = 8;
        }
        break;
      case 3552126: 
        localObject1 = "tabs";
        boolean bool5 = str1.equals(localObject1);
        if (bool5) {
          int i7 = 4;
        }
        break;
      case -21437972: 
        localObject1 = "blocked";
        boolean bool6 = str1.equals(localObject1);
        if (bool6) {
          int i8 = 2;
        }
        break;
      case -91022241: 
        localObject1 = "editprofile";
        boolean bool7 = str1.equals(localObject1);
        if (bool7) {
          int i9 = 7;
        }
        break;
      case -722568291: 
        localObject1 = "referral";
        bool8 = str1.equals(localObject1);
        if (bool8)
        {
          bool8 = false;
          localObject1 = null;
        }
        break;
      case -874822710: 
        localObject1 = "themes";
        bool8 = str1.equals(localObject1);
        if (bool8) {
          int i10 = 5;
        }
        break;
      case -966943889: 
        localObject1 = "qamenu";
        boolean bool9 = str1.equals(localObject1);
        if (bool9) {
          int i11 = 6;
        }
        break;
      case -1035287946: 
        localObject1 = "detailview";
        boolean bool10 = str1.equals(localObject1);
        if (bool10) {
          i12 = 3;
        }
        break;
      }
      int i12 = -1;
      int i16;
      switch (i12)
      {
      default: 
        break;
      case 10: 
        a(this);
        break;
      case 9: 
        i12 = str3.hashCode();
        i16 = -1323191193;
        boolean bool11;
        if (i12 == i16)
        {
          localObject1 = "onboard";
          bool11 = str3.equals(localObject1);
          if (bool11) {}
        }
        else
        {
          i2 = -1;
        }
        if (i2 != 0)
        {
          localObject1 = a.bh();
          bool11 = ((com.truecaller.calling.recorder.h)localObject1).a();
          if (bool11) {
            bj.a(this);
          }
        }
        else
        {
          localObject1 = a.bh();
          bool11 = ((com.truecaller.calling.recorder.h)localObject1).a();
          if (bool11)
          {
            localObject1 = a.bg();
            bool11 = ((CallRecordingManager)localObject1).a();
            if (bool11) {
              localObject1 = CallRecordingOnBoardingState.POST_ENABLE;
            } else {
              localObject1 = CallRecordingOnBoardingState.WHATS_NEW;
            }
            localObject2 = CallRecordingOnBoardingLaunchContext.DEEP_LINK;
            aj.a(this, (CallRecordingOnBoardingState)localObject1, (CallRecordingOnBoardingLaunchContext)localObject2);
          }
        }
        break;
      case 8: 
        localObject1 = WhoViewedMeLaunchContext.DEEPLINK;
        localObject1 = WhoViewedMeActivity.a(this, (WhoViewedMeLaunchContext)localObject1);
        startActivity((Intent)localObject1);
        break;
      case 7: 
        t();
        break;
      case 6: 
        localObject1 = v;
        localObject2 = M;
        ((TrueApp)localObject1).a((android.support.v4.app.j)localObject2);
        break;
      case 5: 
        localObject1 = SingleActivity.FragmentSingle.THEME_SELECTOR;
        localObject1 = SingleActivity.a(this, (SingleActivity.FragmentSingle)localObject1);
        startActivity((Intent)localObject1);
        break;
      case 4: 
        int i13 = str3.hashCode();
        boolean bool12;
        switch (i13)
        {
        default: 
          break;
        case 1382682413: 
          localObject1 = "payments";
          bool12 = str3.equals(localObject1);
          if (!bool12) {
            break;
          }
          break;
        case 3045982: 
          localObject1 = "call";
          bool12 = str3.equals(localObject1);
          if (bool12) {
            i14 = 1;
          }
          break;
        case -318452137: 
          localObject1 = "premium";
          bool12 = str3.equals(localObject1);
          if (bool12) {
            i14 = 6;
          }
          break;
        case -337045466: 
          localObject1 = "banking";
          bool12 = str3.equals(localObject1);
          if (bool12) {
            i14 = 3;
          }
          break;
        case -462094004: 
          localObject1 = "messages";
          bool12 = str3.equals(localObject1);
          if (bool12)
          {
            i14 = 0;
            localObject3 = null;
          }
          break;
        case -567451565: 
          localObject1 = "contacts";
          bool12 = str3.equals(localObject1);
          if (bool12) {
            i14 = 2;
          }
          break;
        case -664572875: 
          localObject1 = "blocking";
          bool12 = str3.equals(localObject1);
          if (bool12) {
            i14 = 5;
          }
          break;
        }
        i14 = -1;
        switch (i14)
        {
        default: 
          i1 = 0;
          break;
        case 6: 
          localObject1 = getIntent();
          localObject2 = "ARG_FRAGMENT";
          str1 = "premium";
          ((Intent)localObject1).putExtra((String)localObject2, str1);
          break;
        case 5: 
          localObject1 = getIntent();
          localObject2 = "ARG_FRAGMENT";
          str1 = "blocking";
          ((Intent)localObject1).putExtra((String)localObject2, str1);
          break;
        case 4: 
          localObject1 = getIntent();
          localObject2 = "ARG_FRAGMENT";
          str1 = "payments";
          ((Intent)localObject1).putExtra((String)localObject2, str1);
          break;
        case 3: 
          localObject1 = getIntent();
          localObject2 = "ARG_FRAGMENT";
          str1 = "banking";
          ((Intent)localObject1).putExtra((String)localObject2, str1);
          break;
        case 2: 
          localObject1 = getIntent();
          localObject2 = "ARG_FRAGMENT";
          str1 = "contacts";
          ((Intent)localObject1).putExtra((String)localObject2, str1);
          break;
        case 1: 
          localObject1 = getIntent();
          localObject2 = "ARG_FRAGMENT";
          str1 = "calls";
          ((Intent)localObject1).putExtra((String)localObject2, str1);
          break;
        case 0: 
          localObject1 = getIntent();
          localObject2 = "ARG_FRAGMENT";
          str1 = "messages";
          ((Intent)localObject1).putExtra((String)localObject2, str1);
        }
        break;
      case 3: 
        localObject1 = "tel";
        str3 = ((Bundle)localObject2).getString((String)localObject1);
        if (str3 != null)
        {
          i16 = 0;
          localObject2 = null;
          str1 = null;
          str2 = null;
          i14 = 0;
          localObject3 = null;
          localObject4 = DetailsFragment.SourceType.DeepLink;
          i2 = 1;
          i15 = 4;
          localObject1 = this;
          DetailsFragment.a(this, null, null, str3, null, null, (DetailsFragment.SourceType)localObject4, i2, i15);
        }
        break;
      case 2: 
        localObject1 = new android/content/Intent;
        localObject2 = BlockedEventsActivity.class;
        ((Intent)localObject1).<init>(this, (Class)localObject2);
        startActivity((Intent)localObject1);
        break;
      case 1: 
        localObject1 = SettingsFragment.SettingsViewType.valueOf(am.l(str3));
        localObject2 = new String[i1];
        str3 = String.valueOf(localObject1);
        str1 = "processDeepLinks:: Settings screen: ".concat(str3);
        localObject2[0] = str1;
        SettingsFragment.b(this, (SettingsFragment.SettingsViewType)localObject1);
        break;
      case 0: 
        localObject1 = N;
        if (localObject1 == null) {
          break label1418;
        }
        ((ReferralManager)localObject1).b(str2);
        break;
      }
      i1 = 0;
      label1418:
      if (i1 != 0)
      {
        localObject1 = getIntent();
        i16 = 0;
        localObject2 = null;
        ((Intent)localObject1).setAction(null);
      }
      return;
    }
  }
  
  private void o()
  {
    Object localObject1 = v;
    boolean bool1 = ((TrueApp)localObject1).isTcPayEnabled();
    if (!bool1) {
      return;
    }
    localObject1 = getIntent().getExtras();
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = ((Bundle)localObject1).getString("bank_symbol");
    String str = ((Bundle)localObject1).getString("acc_number");
    boolean bool2 = TextUtils.isEmpty((CharSequence)localObject2);
    if (!bool2)
    {
      getIntent().setAction(null);
      a("banking");
      Truepay localTruepay = Truepay.getInstance();
      bool2 = localTruepay.isRegistrationComplete();
      if (bool2) {
        localObject2 = com.truecaller.truepay.app.ui.dashboard.views.b.d.a((String)localObject2, str);
      } else {
        localObject2 = Truepay.getInstance().getBankingFragment();
      }
      replaceFragment((Fragment)localObject2);
      localObject2 = Boolean.TRUE;
    }
    else
    {
      localObject2 = Boolean.FALSE;
    }
    boolean bool3 = ((Boolean)localObject2).booleanValue();
    if (!bool3)
    {
      localObject2 = "show_instant_reward";
      bool3 = ((Bundle)localObject1).getBoolean((String)localObject2);
      if (bool3)
      {
        localObject1 = ((Bundle)localObject1).getString("instant_reward_content");
        a("banking");
        localObject2 = Truepay.getInstance();
        bool3 = ((Truepay)localObject2).isRegistrationComplete();
        if ((bool3) && (localObject1 != null))
        {
          localObject2 = Boolean.TRUE;
          localObject1 = com.truecaller.truepay.app.ui.dashboard.views.b.d.a((String)localObject1, (Boolean)localObject2);
        }
        else
        {
          localObject1 = Truepay.getInstance().getBankingFragment();
        }
        replaceFragment((Fragment)localObject1);
      }
    }
  }
  
  private void p()
  {
    Object localObject1 = P.e();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
    Object localObject2;
    if (bool1) {
      localObject2 = "messages";
    } else {
      localObject2 = "calls";
    }
    Intent localIntent = getIntent();
    int i1 = 0;
    if (localIntent != null)
    {
      String str1 = localIntent.getAction();
      Object localObject3 = "android.intent.action.DIAL";
      boolean bool2 = ((String)localObject3).equals(str1);
      if (!bool2)
      {
        localObject3 = "android.intent.action.VIEW";
        bool2 = ((String)localObject3).equals(str1);
        if (!bool2)
        {
          localObject3 = localIntent.getExtras();
          if (localObject3 == null) {
            break label372;
          }
          String str2 = "ARG_FRAGMENT";
          localObject2 = ((Bundle)localObject3).getString(str2, (String)localObject2);
          localObject3 = "ARG_FRAGMENT";
          localIntent.removeExtra((String)localObject3);
          if (bool1)
          {
            localObject1 = "calls";
            bool1 = ((String)localObject1).equals(localObject2);
            if (bool1) {
              localObject2 = "messages";
            }
          }
          localObject1 = "banking";
          bool1 = ((String)localObject1).equals(localObject2);
          if (!bool1)
          {
            localObject1 = "payments";
            bool1 = ((String)localObject1).equals(localObject2);
            if (!bool1)
            {
              bool1 = false;
              localObject1 = null;
              break label185;
            }
          }
          bool1 = true;
          label185:
          localObject3 = "blocking";
          bool2 = ((String)localObject3).equals(localObject2);
          if (!bool2)
          {
            localObject3 = "premium";
            bool2 = ((String)localObject3).equals(localObject2);
            if (!bool2)
            {
              bool2 = false;
              localObject3 = null;
              break label236;
            }
          }
          bool2 = true;
          label236:
          if (bool1)
          {
            localObject1 = P.n();
            bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
            if (!bool1) {
              localObject2 = "calls";
            }
          }
          if (bool2)
          {
            bool1 = H;
            if (bool1)
            {
              localObject1 = v;
              bool1 = ((TrueApp)localObject1).isTcPayEnabled();
              if (!bool1) {}
            }
            else
            {
              localObject1 = "calls";
              localObject2 = localObject1;
            }
          }
          localObject1 = "search";
          bool1 = ((String)localObject1).equals(localObject2);
          if (!bool1) {
            break label372;
          }
          localObject2 = "calls";
          i1 = 1;
          break label372;
        }
      }
      localObject1 = "android.intent.action.DIAL";
      bool1 = ((String)localObject1).equals(str1);
      if (bool1)
      {
        localObject1 = "calls";
        localObject2 = localObject1;
      }
      else
      {
        localObject1 = "android.intent.action.VIEW";
        bool1 = ((String)localObject1).equals(str1);
        if (bool1)
        {
          bool1 = c(localIntent);
          if (bool1)
          {
            localObject1 = "calls";
            localObject2 = localObject1;
          }
        }
      }
      label372:
      localObject1 = "com.truecaller.intent.action.CUSTOM_WEB_VIEW_MAIN_DISPLAY";
      bool1 = ((String)localObject1).equals(str1);
      if (bool1) {
        d(localIntent);
      }
    }
    localObject1 = m;
    ((BottomBar)localObject1).a((String)localObject2);
    if (i1 != 0) {
      com.truecaller.search.global.n.a(this);
    }
    o();
  }
  
  private void q()
  {
    com.truecaller.ui.components.FloatingActionButton localFloatingActionButton = f();
    if (localFloatingActionButton == null) {
      return;
    }
    Object localObject1 = e;
    boolean bool = localObject1 instanceof FloatingActionButton.c;
    if (bool)
    {
      localObject1 = (FloatingActionButton.c)e;
      bool = ((FloatingActionButton.c)localObject1).l();
      if (bool)
      {
        localObject1 = (FloatingActionButton.c)e;
        Object localObject2 = ((FloatingActionButton.c)localObject1).k();
        localFloatingActionButton.setFabActionListener((FloatingActionButton.a)localObject2);
        int i1 = ((FloatingActionButton.c)localObject1).i();
        localObject2 = com.truecaller.utils.ui.b.a(this, i1, 2130968965);
        int i2 = com.truecaller.utils.ui.b.a(this, 2130968964);
        localFloatingActionButton.setDrawable((Drawable)localObject2);
        localFloatingActionButton.setBackgroundColor(i2);
        localObject1 = ((FloatingActionButton.c)localObject1).j();
        localFloatingActionButton.setMenuItems((com.truecaller.ui.components.i[])localObject1);
        localFloatingActionButton.a(true);
        return;
      }
    }
    localFloatingActionButton.a(false);
  }
  
  private void r()
  {
    Object localObject1 = "calls";
    Object localObject2 = B;
    boolean bool = ((String)localObject1).equalsIgnoreCase((String)localObject2);
    if (!bool)
    {
      localObject1 = m;
      if (localObject1 != null)
      {
        localObject1 = ((com.truecaller.callhistory.a)a.ad().a()).e();
        localObject2 = a.m().a();
        -..Lambda.TruecallerInit.qPWuhqV4mnv5MaVuefYRqvDyB-E localqPWuhqV4mnv5MaVuefYRqvDyB-E = new com/truecaller/ui/-$$Lambda$TruecallerInit$qPWuhqV4mnv5MaVuefYRqvDyB-E;
        localqPWuhqV4mnv5MaVuefYRqvDyB-E.<init>(this);
        ((com.truecaller.androidactors.w)localObject1).a((com.truecaller.androidactors.i)localObject2, localqPWuhqV4mnv5MaVuefYRqvDyB-E);
        return;
      }
    }
  }
  
  private void s()
  {
    Object localObject1 = e;
    boolean bool = localObject1 instanceof ae;
    int i1 = 0;
    float f = 0.0F;
    if (bool)
    {
      localObject1 = (ae)e;
      bool = ((ae)localObject1).g();
      if (bool)
      {
        bool = true;
        break label47;
      }
    }
    bool = false;
    localObject1 = null;
    label47:
    AppBarLayout.b localb = (AppBarLayout.b)c.getLayoutParams();
    Object localObject2 = getResources();
    int i2;
    if (bool) {
      i2 = 2131165489;
    } else {
      i2 = 2131165496;
    }
    int i3 = ((Resources)localObject2).getDimensionPixelSize(i2);
    bottomMargin = i3;
    localObject2 = c;
    ((View)localObject2).setLayoutParams(localb);
    if (!bool)
    {
      localObject1 = getResources();
      f = 1.7946777E38F;
      i1 = ((Resources)localObject1).getDimensionPixelSize(2131166268);
    }
    localObject1 = d;
    f = i1;
    r.b((View)localObject1, f);
  }
  
  private void t()
  {
    boolean bool = u();
    int i1 = 7001;
    Object localObject;
    if (bool)
    {
      localObject = P.ar();
      bool = ((com.truecaller.featuretoggles.b)localObject).a();
      if (!bool)
      {
        localObject = CreateBusinessProfileActivity.b(this);
        startActivityForResult((Intent)localObject, i1);
      }
    }
    else
    {
      localObject = P.j();
      bool = ((com.truecaller.featuretoggles.b)localObject).a();
      if (bool)
      {
        localObject = new android/content/Intent;
        ((Intent)localObject).<init>(this, ProfileActivity.class);
        startActivityForResult((Intent)localObject, i1);
        return;
      }
      localObject = com.truecaller.profile.a.b(this);
      startActivityForResult((Intent)localObject, i1);
    }
  }
  
  private boolean u()
  {
    return u.a("profileBusiness", false);
  }
  
  private void v()
  {
    Object localObject1 = P;
    Object localObject2 = n;
    Object localObject3 = com.truecaller.featuretoggles.e.a;
    int i1 = 41;
    localObject3 = localObject3[i1];
    localObject1 = ((com.truecaller.featuretoggles.e.a)localObject2).a((com.truecaller.featuretoggles.e)localObject1, (g)localObject3);
    boolean bool = ((com.truecaller.featuretoggles.b)localObject1).a();
    int i2;
    if (bool)
    {
      localObject1 = new android/content/Intent;
      localObject2 = BarcodeCaptureActivity.class;
      ((Intent)localObject1).<init>(this, (Class)localObject2);
      i2 = 7005;
      startActivityForResult((Intent)localObject1, i2);
    }
    else
    {
      localObject1 = NumberDetectorProcessor.ScanType.SCAN_PHONE;
      localObject1 = NumberScannerActivity.a(this, (NumberDetectorProcessor.ScanType)localObject1);
      i2 = 7003;
      startActivityForResult((Intent)localObject1, i2);
    }
    i.c();
  }
  
  private void w()
  {
    boolean bool = G;
    if (bool)
    {
      ag localag = t;
      WeakReference localWeakReference = new java/lang/ref/WeakReference;
      localWeakReference.<init>(this);
      localag.a(localWeakReference);
    }
  }
  
  private void x()
  {
    Object localObject1 = v;
    boolean bool = ((TrueApp)localObject1).isTcPayEnabled();
    Object localObject2;
    if (bool)
    {
      localObject1 = "banking";
      localObject2 = B;
      bool = ((String)localObject1).equals(localObject2);
      if (bool)
      {
        localObject1 = "banking";
        d((String)localObject1);
      }
      else
      {
        localObject1 = "payments";
        localObject2 = B;
        bool = ((String)localObject1).equals(localObject2);
        if (bool)
        {
          localObject1 = "payments";
          d((String)localObject1);
        }
      }
    }
    localObject1 = Y;
    int i1 = 1;
    ((Handler)localObject1).removeMessages(i1);
    localObject1 = e;
    bool = localObject1 instanceof az;
    if (bool)
    {
      localObject1 = Y;
      Fragment localFragment = e;
      localObject1 = ((Handler)localObject1).obtainMessage(i1, localFragment);
      localObject2 = Y;
      long l1 = 1000L;
      ((Handler)localObject2).sendMessageDelayed((Message)localObject1, l1);
    }
  }
  
  private void y()
  {
    Object localObject1 = j;
    int i1 = ((NavigationView)localObject1).getHeaderCount();
    int i3 = 2;
    if (i1 < i3) {
      return;
    }
    localObject1 = a.ai();
    Object localObject2 = j;
    int i4 = 1;
    localObject2 = ((NavigationView)localObject2).a(i4);
    int i6 = 2131362084;
    Object localObject3 = (ImageView)((View)localObject2).findViewById(i6);
    int i7 = 2131364884;
    TextView localTextView1 = (TextView)((View)localObject2).findViewById(i7);
    int i8 = 2131363752;
    TextView localTextView2 = (TextView)((View)localObject2).findViewById(i8);
    int i9 = 2131363301;
    ImageView localImageView1 = (ImageView)((View)localObject2).findViewById(i9);
    int i10 = 2131362085;
    ImageView localImageView2 = (ImageView)((View)localObject2).findViewById(i10);
    boolean bool3 = ((com.truecaller.common.f.c)localObject1).o();
    int i11 = 8;
    Object localObject4;
    if (bool3)
    {
      localObject4 = new com/truecaller/ui/view/c;
      int i12 = 2130969559;
      ((com.truecaller.ui.view.c)localObject4).<init>(this, false, false, i12);
      ((ImageView)localObject3).setImageDrawable((Drawable)localObject4);
      ((com.truecaller.ui.view.c)localObject4).a(i4);
      ((ImageView)localObject3).setVisibility(0);
    }
    else
    {
      ((ImageView)localObject3).setVisibility(i11);
      bool3 = false;
      localObject4 = null;
      ((ImageView)localObject3).setImageDrawable(null);
    }
    localObject3 = a.j();
    boolean bool2 = ((com.truecaller.engagementrewards.c)localObject3).b();
    if (bool2)
    {
      bool2 = ((com.truecaller.common.f.c)localObject1).d();
      if (bool2)
      {
        localObject3 = a.j();
        localObject4 = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
        localObject3 = ((com.truecaller.engagementrewards.c)localObject3).a((EngagementRewardActionType)localObject4);
        localObject4 = EngagementRewardState.COMPLETED;
        if (localObject3 == localObject4) {
          break label289;
        }
      }
    }
    int i5 = 0;
    label289:
    bool2 = ((com.truecaller.common.f.c)localObject1).d();
    if (!bool2)
    {
      i1 = 2131886314;
      localTextView1.setText(i1);
    }
    else
    {
      localObject3 = ((com.truecaller.common.f.c)localObject1).k();
      localObject4 = "regular";
      bool2 = ((String)localObject3).equals(localObject4);
      if (bool2)
      {
        localTextView1.setText(2131886824);
        i1 = 2131234161;
        localImageView1.setImageResource(i1);
        if (i5 != 0)
        {
          i1 = 0;
          localObject1 = null;
        }
        else
        {
          i1 = 8;
        }
        localImageView2.setVisibility(i1);
        if (i5 != 0) {
          i11 = 0;
        }
        localTextView2.setVisibility(i11);
      }
      else
      {
        localObject1 = ((com.truecaller.common.f.c)localObject1).k();
        localObject3 = "gold";
        boolean bool1 = ((String)localObject1).equals(localObject3);
        if (bool1)
        {
          int i2 = 2131886823;
          localTextView1.setText(i2);
        }
      }
    }
    if (i5 != 0)
    {
      localObject1 = new com/truecaller/ui/-$$Lambda$TruecallerInit$7vGYRsSgfCAgLjSNgCiabfnJEVU;
      ((-..Lambda.TruecallerInit.7vGYRsSgfCAgLjSNgCiabfnJEVU)localObject1).<init>(this);
      ((View)localObject2).setOnClickListener((View.OnClickListener)localObject1);
      return;
    }
    localObject1 = new com/truecaller/ui/-$$Lambda$TruecallerInit$d5jW3thcXpyo3WWRQlDFMCDqCeo;
    ((-..Lambda.TruecallerInit.d5jW3thcXpyo3WWRQlDFMCDqCeo)localObject1).<init>(this);
    ((View)localObject2).setOnClickListener((View.OnClickListener)localObject1);
  }
  
  private void z()
  {
    Object localObject1 = k;
    if (localObject1 != null)
    {
      localObject1 = v;
      boolean bool1 = ((TrueApp)localObject1).p();
      if (!bool1)
      {
        k.b();
        return;
      }
      localObject1 = com.truecaller.profile.c.a(u);
      Object localObject2 = u;
      String str = "profileAvatar";
      localObject2 = ((com.truecaller.common.g.a)localObject2).a(str);
      boolean bool2 = u();
      Uri localUri = null;
      if (bool2)
      {
        str = com.truecaller.profile.c.c(u);
      }
      else
      {
        bool2 = false;
        str = null;
      }
      boolean bool3 = am.d(str);
      if (bool3) {
        str = com.truecaller.profile.c.b(u);
      }
      DrawerHeaderView localDrawerHeaderView = k;
      if (localObject1 == null) {
        localObject1 = "";
      }
      boolean bool4 = am.d((CharSequence)localObject2);
      if (!bool4) {
        localUri = Uri.parse((String)localObject2);
      }
      localDrawerHeaderView.a(str, (String)localObject1, localUri);
    }
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    Object localObject = n;
    if (localObject != null)
    {
      int i1 = paramInt1 + paramInt2;
      com.truecaller.ui.components.c localc = b;
      int i2 = b;
      if (i2 != i1)
      {
        localc = b;
        b = i1;
        ((BadgeDrawerArrowDrawable)localObject).invalidateSelf();
      }
    }
    localObject = y;
    if (localObject != null) {
      ((com.truecaller.ui.view.c)localObject).a(paramInt1);
    }
    com.truecaller.ui.view.c localc1 = z;
    if (localc1 != null) {
      localc1.a(paramInt2);
    }
  }
  
  public final void a(AppBarLayout.c paramc)
  {
    AppBarLayout.c localc = A;
    if (localc != null)
    {
      AppBarLayout localAppBarLayout = d;
      localAppBarLayout.b(localc);
    }
    A = paramc;
    d.a(paramc);
  }
  
  public final void a(ak paramak)
  {
    int i1 = a;
    int i2 = b;
    i1 += i2;
    paramak = Integer.valueOf(i1);
    Object localObject1 = m;
    if (localObject1 != null)
    {
      localObject1 = Q;
      boolean bool1 = ((com.truecaller.utils.d)localObject1).d();
      boolean bool2 = true;
      int i3 = 0;
      Context localContext = null;
      if (bool1)
      {
        localObject1 = S;
        localObject2 = new String[] { "android.permission.READ_SMS" };
        bool1 = ((com.truecaller.utils.l)localObject1).a((String[])localObject2);
        if (bool1)
        {
          bool1 = true;
          break label99;
        }
      }
      bool1 = false;
      localObject1 = null;
      label99:
      Object localObject2 = a.aG();
      boolean bool3 = ((bw)localObject2).a();
      Object localObject3 = R;
      boolean bool4 = ((com.truecaller.i.e)localObject3).a("notDefaultSmsBadgeShown", false);
      BottomBar localBottomBar = m;
      String str1 = "messages";
      Object localObject4;
      if ((bool4) || (bool1) || (bool3))
      {
        bool2 = false;
        localObject4 = null;
      }
      int i4 = -1;
      int i5 = str1.hashCode();
      int i6 = -462094004;
      if (i5 == i6)
      {
        String str2 = "messages";
        boolean bool5 = str1.equals(str2);
        if (bool5) {}
      }
      else
      {
        i3 = -1;
      }
      if (i3 == 0)
      {
        localContext = localBottomBar.getContext();
        localObject3 = a.getDrawable();
        com.truecaller.util.at.a(localContext, (Drawable)localObject3, bool2);
        a.refreshDrawableState();
        localObject4 = a;
        ((ImageView)localObject4).invalidate();
      }
      if ((bool1) || (bool3))
      {
        localObject1 = m;
        localObject4 = "messages";
        i2 = paramak.intValue();
        ((BottomBar)localObject1).a((String)localObject4, i2);
      }
    }
  }
  
  public final void a(StartupDialogType paramStartupDialogType, StartupDialogDismissReason paramStartupDialogDismissReason)
  {
    o.a(paramStartupDialogType, paramStartupDialogDismissReason);
  }
  
  public final void a(String paramString)
  {
    m.a(paramString);
  }
  
  public final void ag_()
  {
    Fragment localFragment = e;
    a(localFragment);
  }
  
  public final void ah_()
  {
    v();
  }
  
  protected final void b(o paramo)
  {
    AssertionUtil.OnlyInDebug.fail(new String[] { "switchFragment() is unavailable for TruecallerInit" });
  }
  
  public final void b(String paramString)
  {
    android.support.v4.app.o localo = M.a();
    boolean bool1 = true;
    localo.a(bool1);
    localo.a(0);
    Object localObject1 = M;
    Object localObject2 = String.valueOf(paramString);
    localObject1 = ((android.support.v4.app.j)localObject1).a((String)localObject2);
    int i1;
    Object localObject3;
    Object localObject4;
    if (localObject1 != null)
    {
      boolean bool2 = localObject1 instanceof com.truecaller.truepay.app.ui.base.views.fragments.b;
      if (!bool2) {}
    }
    else
    {
      i1 = -1;
      int i2 = paramString.hashCode();
      switch (i2)
      {
      default: 
        break;
      case 1382682413: 
        localObject3 = "payments";
        bool4 = paramString.equals(localObject3);
        if (bool4) {
          i1 = 4;
        }
        break;
      case 94425557: 
        localObject3 = "calls";
        bool4 = paramString.equals(localObject3);
        if (bool4)
        {
          i1 = 0;
          localObject2 = null;
        }
        break;
      case -318452137: 
        localObject3 = "premium";
        bool4 = paramString.equals(localObject3);
        if (bool4) {
          i1 = 6;
        }
        break;
      case -337045466: 
        localObject3 = "banking";
        bool4 = paramString.equals(localObject3);
        if (bool4) {
          i1 = 3;
        }
        break;
      case -462094004: 
        localObject3 = "messages";
        bool4 = paramString.equals(localObject3);
        if (bool4) {
          i1 = 1;
        }
        break;
      case -567451565: 
        localObject3 = "contacts";
        bool4 = paramString.equals(localObject3);
        if (bool4) {
          i1 = 2;
        }
        break;
      case -664572875: 
        localObject3 = "blocking";
        bool4 = paramString.equals(localObject3);
        if (bool4) {
          i1 = 5;
        }
        break;
      }
      boolean bool4 = false;
      localObject3 = null;
      int i6;
      switch (i1)
      {
      default: 
        break;
      case 6: 
        int i5 = getResources().getDimensionPixelSize(2131165352);
        localObject2 = getResources();
        i6 = 2131165495;
        i1 = ((Resources)localObject2).getDimensionPixelSize(i6);
        i5 += i1;
        localObject2 = PremiumPresenterView.LaunchContext.BOTTOM_BAR;
        localObject4 = new com/truecaller/premium/ba$b;
        int i7 = 2131233992;
        float f = 16.0F;
        int i8 = com.truecaller.util.at.a(this, f);
        ((ba.b)localObject4).<init>(null, i7, i8, i5);
        localObject1 = ba.a((PremiumPresenterView.LaunchContext)localObject2, (ba.b)localObject4);
        break;
      case 5: 
        localObject1 = new com/truecaller/filters/blockedevents/e;
        ((com.truecaller.filters.blockedevents.e)localObject1).<init>();
        break;
      case 4: 
        localObject1 = Truepay.getInstance().getPaymentsHomeFragment();
        break;
      case 3: 
        localObject1 = Truepay.getInstance().getBankingFragment();
        break;
      case 2: 
        localObject1 = P;
        localObject2 = e;
        localObject3 = com.truecaller.featuretoggles.e.a;
        i6 = 16;
        localObject3 = localObject3[i6];
        localObject1 = ((com.truecaller.featuretoggles.e.a)localObject2).a((com.truecaller.featuretoggles.e)localObject1, (g)localObject3);
        boolean bool7 = ((com.truecaller.featuretoggles.b)localObject1).a();
        if (bool7)
        {
          localObject1 = new com/truecaller/calling/contacts_list/k;
          ((com.truecaller.calling.contacts_list.k)localObject1).<init>();
        }
        else
        {
          localObject1 = new com/truecaller/ui/i;
          ((i)localObject1).<init>();
        }
        break;
      case 1: 
        localObject1 = new com/truecaller/messaging/conversationlist/h;
        ((com.truecaller.messaging.conversationlist.h)localObject1).<init>();
        break;
      case 0: 
        localObject1 = q;
        localObject2 = TimingEvent.CALL_LOG_STARTUP;
        localObject4 = "fragment:V2";
        ((au)localObject1).a((TimingEvent)localObject2, null, (String)localObject4);
        localObject1 = new com/truecaller/calling/dialer/l;
        ((com.truecaller.calling.dialer.l)localObject1).<init>();
        localObject2 = localObject1;
        localObject2 = (com.truecaller.calling.dialer.l)localObject1;
        localObject3 = N;
        o = ((ReferralManager)localObject3);
      }
      i1 = 2131363156;
      localObject3 = String.valueOf(paramString);
      localo.a(i1, (Fragment)localObject1, (String)localObject3);
    }
    if (localObject1 != null)
    {
      localObject2 = Q;
      i1 = ((com.truecaller.utils.d)localObject2).h();
      int i3 = 21;
      if (i1 >= i3)
      {
        i1 = 1;
      }
      else
      {
        i1 = 0;
        localObject2 = null;
      }
      localObject3 = "banking";
      boolean bool5 = ((String)localObject3).equals(paramString);
      if (!bool5)
      {
        localObject3 = "payments";
        bool5 = ((String)localObject3).equals(paramString);
        if (!bool5)
        {
          bool5 = false;
          localObject3 = null;
          break label760;
        }
      }
      bool5 = true;
      label760:
      localObject4 = v;
      boolean bool8 = ((TrueApp)localObject4).isTcPayEnabled();
      boolean bool3;
      if ((bool8) && (bool5))
      {
        if (i1 != 0)
        {
          localObject2 = getWindow();
          int i4 = android.support.v4.content.b.c(this, 2131100521);
          ((Window)localObject2).setStatusBarColor(i4);
        }
        localObject2 = "banking";
        bool3 = ((String)localObject2).equals(paramString);
        if (bool3)
        {
          localObject2 = Truepay.getInstance();
          ((Truepay)localObject2).incrementBankingClicked();
        }
        else
        {
          localObject2 = "payments";
          bool3 = ((String)localObject2).equals(paramString);
          if (bool3)
          {
            localObject2 = Truepay.getInstance();
            ((Truepay)localObject2).incrementPaymentsClicked();
          }
        }
      }
      localObject2 = e;
      if (localObject2 != null)
      {
        bool3 = D;
        if (bool3)
        {
          localObject2 = e;
          bool3 = localObject2 instanceof ab;
          if (bool3)
          {
            localObject2 = (ab)e;
            ((ab)localObject2).a(bool1);
          }
        }
      }
      localObject2 = M.f();
      if (localObject2 != null)
      {
        localObject2 = ((List)localObject2).iterator();
        for (;;)
        {
          boolean bool6 = ((Iterator)localObject2).hasNext();
          if (!bool6) {
            break;
          }
          localObject3 = (Fragment)((Iterator)localObject2).next();
          if (localObject3 != null)
          {
            bool8 = ((Fragment)localObject3).isHidden();
            if (!bool8)
            {
              bool8 = localObject3 instanceof android.support.v4.app.e;
              if (bool8) {
                localo.a((Fragment)localObject3);
              } else {
                localo.b((Fragment)localObject3);
              }
            }
          }
        }
      }
      localObject2 = b.getMenu();
      if (localObject2 != null) {
        ((Menu)localObject2).close();
      }
      localo.c((Fragment)localObject1);
      localo.d();
      B = paramString;
      e = ((Fragment)localObject1);
      x();
      paramString = M;
      boolean bool9 = paramString.b();
      if (bool9)
      {
        paramString = d;
        if (paramString != null) {
          paramString.a(bool1, false, bool1);
        }
        q();
      }
      bool9 = D;
      if (bool9)
      {
        paramString = e;
        bool9 = paramString instanceof ab;
        if (bool9)
        {
          paramString = (ab)e;
          paramString.n();
        }
      }
      paramString = e;
      a(paramString);
      s();
    }
  }
  
  protected final boolean b()
  {
    Object localObject = i;
    boolean bool1 = ((DrawerLayout)localObject).d();
    boolean bool2 = true;
    if (bool1)
    {
      i.c();
      return bool2;
    }
    localObject = f();
    if (localObject != null)
    {
      boolean bool3 = a;
      if (bool3)
      {
        ((com.truecaller.ui.components.FloatingActionButton)localObject).c();
        return bool2;
      }
    }
    return false;
  }
  
  public final void c(String paramString)
  {
    Object localObject = d;
    boolean bool1 = true;
    ((AppBarLayout)localObject).a(bool1, bool1, bool1);
    localObject = v;
    boolean bool2 = ((TrueApp)localObject).isTcPayEnabled();
    if (bool2)
    {
      localObject = "payments";
      bool2 = paramString.equals(localObject);
      if (!bool2)
      {
        localObject = "banking";
        bool3 = paramString.equals(localObject);
        if (!bool3) {}
      }
      else
      {
        return;
      }
    }
    paramString = e;
    boolean bool3 = paramString instanceof ab;
    if (bool3)
    {
      paramString = (ab)e;
      paramString.m();
    }
  }
  
  public final void d()
  {
    s();
  }
  
  public final PermissionPoller e()
  {
    PermissionPoller localPermissionPoller = J;
    if (localPermissionPoller == null)
    {
      localPermissionPoller = new com/truecaller/wizard/utils/PermissionPoller;
      TrueApp localTrueApp = v;
      Handler localHandler = Y;
      Intent localIntent1 = a(this, null);
      localPermissionPoller.<init>(localTrueApp, localHandler, localIntent1);
      J = localPermissionPoller;
      localPermissionPoller = new com/truecaller/wizard/utils/PermissionPoller;
      localTrueApp = v;
      localHandler = Y;
      Intent localIntent2 = a(this, null);
      localPermissionPoller.<init>(localTrueApp, localHandler, localIntent2);
      J = localPermissionPoller;
    }
    return J;
  }
  
  public final com.truecaller.ui.components.FloatingActionButton f()
  {
    int i1 = 2131363120;
    View localView = findViewById(i1);
    boolean bool = localView instanceof com.truecaller.ui.components.FloatingActionButton;
    if (bool) {
      return (com.truecaller.ui.components.FloatingActionButton)localView;
    }
    return null;
  }
  
  protected final int g()
  {
    return 2130969581;
  }
  
  public final void h()
  {
    q();
  }
  
  public final void i()
  {
    aj.b localb = e).f;
    if (localb == null)
    {
      String str = "dialpadPresenter";
      c.g.b.k.a(str);
    }
    localb.g();
  }
  
  public final void j()
  {
    android.support.v4.app.o localo = M.a();
    Fragment localFragment = M.a("payments");
    Object localObject = M;
    String str = "premium";
    localObject = ((android.support.v4.app.j)localObject).a(str);
    if (localFragment != null) {
      localo.a(localFragment);
    }
    if (localObject != null) {
      localo.a((Fragment)localObject);
    }
    localo.d();
  }
  
  public final void k()
  {
    t();
  }
  
  public final void l()
  {
    boolean bool = u();
    int i1 = 7001;
    Object localObject;
    if (bool)
    {
      localObject = P.ar();
      bool = ((com.truecaller.featuretoggles.b)localObject).a();
      if (!bool)
      {
        localObject = CreateBusinessProfileActivity.b(this);
        startActivityForResult((Intent)localObject, i1);
      }
    }
    else
    {
      localObject = EditMeFormFragment.a(this);
      startActivityForResult((Intent)localObject, i1);
    }
  }
  
  public final void m()
  {
    com.truecaller.wizard.b.c.a(this, WizardActivity.class, "sideBar");
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    Object localObject1 = o;
    boolean bool1 = ((com.truecaller.startup_dialogs.c)localObject1).a(paramInt1);
    boolean bool2 = false;
    Fragment localFragment = null;
    int i2 = 1;
    Object localObject2;
    Object localObject3;
    if (bool1)
    {
      localObject2 = new String[i2];
      localObject3 = String.valueOf(paramInt1);
      localObject3 = "onActivityResult:: Handled by startup dialog resolver. Req code: ".concat((String)localObject3);
      localObject2[0] = localObject3;
      return;
    }
    int i1 = 7001;
    int i3 = -1;
    if ((paramInt1 == i1) && (paramInt2 == i3))
    {
      localObject1 = k;
      if (localObject1 != null)
      {
        z();
        return;
      }
    }
    i1 = 7003;
    if ((paramInt1 == i1) && (paramInt2 == i3))
    {
      localObject3 = paramIntent.getExtras();
      if (localObject3 != null)
      {
        localObject2 = "extra_results";
        localObject3 = ((Bundle)localObject3).getStringArrayList((String)localObject2);
        if (localObject3 != null)
        {
          paramInt2 = ((List)localObject3).size();
          if (paramInt2 > i2)
          {
            localObject2 = new android/support/v7/app/AlertDialog$Builder;
            ((AlertDialog.Builder)localObject2).<init>(this);
            localObject2 = ((AlertDialog.Builder)localObject2).setTitle(2131888737);
            paramIntent = new com/truecaller/scanner/s;
            paramIntent.<init>(this, (List)localObject3);
            localObject1 = new com/truecaller/ui/-$$Lambda$TruecallerInit$jp0IW3rAM69h-eCVmwD4P8zbeNs;
            ((-..Lambda.TruecallerInit.jp0IW3rAM69h-eCVmwD4P8zbeNs)localObject1).<init>(this, (List)localObject3);
            ((AlertDialog.Builder)localObject2).setAdapter(paramIntent, (DialogInterface.OnClickListener)localObject1).create().show();
            return;
          }
          localObject3 = (String)((List)localObject3).get(0);
          com.truecaller.scanner.f.a((String)localObject3, this);
        }
      }
      return;
    }
    i1 = 7005;
    if ((paramInt1 == i1) && (paramInt2 == i3))
    {
      localObject3 = paramIntent.getExtras();
      if (localObject3 != null)
      {
        localObject2 = "extra_barcode_value";
        localObject3 = ((Bundle)localObject3).getString((String)localObject2);
        if (localObject3 != null)
        {
          localObject3 = T.b((String)localObject3);
          localObject2 = new android/os/Bundle;
          ((Bundle)localObject2).<init>();
          localObject1 = (String)a;
          ((Bundle)localObject2).putString("qr_scan_code", (String)localObject1);
          localObject3 = (String)b;
          ((Bundle)localObject2).putString("qr_partner_name", (String)localObject3);
          localObject3 = new android/content/Intent;
          paramIntent = ConfirmProfileActivity.class;
          ((Intent)localObject3).<init>(this, paramIntent);
          ((Intent)localObject3).putExtras((Bundle)localObject2);
          startActivity((Intent)localObject3);
        }
      }
      return;
    }
    localObject1 = M.f().iterator();
    for (;;)
    {
      bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localFragment = (Fragment)((Iterator)localObject1).next();
      if (localFragment != null)
      {
        boolean bool3 = localFragment.isHidden();
        if (!bool3) {
          localFragment.onActivityResult(paramInt1, paramInt2, paramIntent);
        }
      }
    }
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    l.onConfigurationChanged(paramConfiguration);
  }
  
  public void onConnectionFailed(ConnectionResult paramConnectionResult) {}
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = v;
    boolean bool1 = paramBundle.o();
    int i3 = 1;
    String str = null;
    if (bool1)
    {
      paramBundle = v;
      bool1 = paramBundle.p();
      if (bool1)
      {
        paramBundle = "wizard_FullyCompleted";
        bool1 = com.truecaller.common.b.e.a(paramBundle, false);
        if (bool1) {}
      }
      else
      {
        paramBundle = "silentLoginFailed";
        bool1 = com.truecaller.common.b.e.a(paramBundle, false);
        if (bool1)
        {
          paramBundle = v;
          paramBundle.a(false);
        }
        paramBundle = getIntent();
        localObject1 = "EXTRA_REG_NUDGE";
        bool1 = paramBundle.hasExtra((String)localObject1);
        if (bool1)
        {
          paramBundle = WizardActivity.class;
          localObject1 = getIntent().getExtras();
          com.truecaller.wizard.b.c.a(this, paramBundle, (Bundle)localObject1, false);
        }
        else
        {
          paramBundle = WizardActivity.class;
          i5 = 0;
          localObject1 = null;
          com.truecaller.wizard.b.c.a(this, paramBundle, null, false);
        }
        bool1 = true;
        break label187;
      }
    }
    bool1 = ForcedUpdate.a(this, false);
    if (bool1)
    {
      bool1 = true;
    }
    else
    {
      paramBundle = f;
      bool1 = paramBundle.f();
      if (!bool1)
      {
        RequiredPermissionsActivity.a(this);
        bool1 = true;
      }
      else
      {
        bool1 = false;
        paramBundle = null;
      }
    }
    label187:
    if (bool1)
    {
      overridePendingTransition(0, 0);
      finish();
      return;
    }
    int i1 = aresId;
    setTheme(i1);
    a.cs().a(this);
    paramBundle = a.aF();
    P = paramBundle;
    paramBundle = a.bx();
    Q = paramBundle;
    paramBundle = a.I();
    u = paramBundle;
    paramBundle = a.F();
    R = paramBundle;
    paramBundle = a.bw();
    S = paramBundle;
    paramBundle = a.bX();
    T = paramBundle;
    paramBundle = a.bY();
    U = paramBundle;
    paramBundle = getSupportFragmentManager();
    M = paramBundle;
    p.a(this);
    a.ao().a();
    paramBundle = GoogleApiAvailability.a();
    i1 = paramBundle.a(this);
    if (i1 == 0)
    {
      paramBundle = com.truecaller.referral.w.a(this, "ReferralManagerImpl");
      N = paramBundle;
      paramBundle = N;
      if (paramBundle != null)
      {
        paramBundle = new com/google/android/gms/common/api/GoogleApiClient$Builder;
        paramBundle.<init>(this);
        paramBundle = paramBundle.a(this, this);
        localObject1 = AppInvite.a;
        paramBundle = paramBundle.a((Api)localObject1).b();
        O = paramBundle;
        paramBundle = N;
        localObject1 = getIntent().getData();
        paramBundle.a((Uri)localObject1);
      }
    }
    paramBundle = P.ap();
    boolean bool2 = paramBundle.a();
    if (bool2)
    {
      paramBundle = v;
      bool2 = paramBundle.p();
      if (bool2)
      {
        bool2 = true;
        break label504;
      }
    }
    bool2 = false;
    paramBundle = null;
    label504:
    H = bool2;
    paramBundle = getIntent();
    Object localObject1 = paramBundle.getStringExtra("AppUserInteraction.Context");
    K = ((String)localObject1);
    a(paramBundle);
    localObject1 = Q;
    int i5 = ((com.truecaller.utils.d)localObject1).h();
    int i9 = 21;
    if (i5 >= i9)
    {
      localObject1 = getWindow();
      i9 = -1 << -1;
      ((Window)localObject1).addFlags(i9);
      ((Window)localObject1).setStatusBarColor(0);
    }
    localObject1 = a.J();
    Object localObject2 = v;
    boolean bool8 = ((TrueApp)localObject2).p();
    if (bool8)
    {
      bool4 = ((ac)localObject1).a();
      if (bool4)
      {
        localObject1 = u;
        localObject2 = "core_agreed_region_1";
        bool4 = ((com.truecaller.common.g.a)localObject1).b((String)localObject2);
        if (!bool4)
        {
          ConsentRefreshActivity.b(this);
          overridePendingTransition(0, 0);
          finish();
          return;
        }
      }
    }
    o.a(this);
    o.a();
    setContentView(2131558477);
    localObject1 = (Toolbar)findViewById(2131363715);
    b = ((Toolbar)localObject1);
    localObject1 = findViewById(2131364917);
    c = ((View)localObject1);
    localObject1 = (AppBarLayout)findViewById(2131362038);
    d = ((AppBarLayout)localObject1);
    localObject1 = (DrawerLayout)findViewById(2131362909);
    i = ((DrawerLayout)localObject1);
    localObject1 = (NavigationView)findViewById(2131363793);
    j = ((NavigationView)localObject1);
    localObject1 = (BottomBar)findViewById(2131362140);
    m = ((BottomBar)localObject1);
    localObject1 = v;
    boolean bool4 = ((TrueApp)localObject1).p();
    if (!bool4) {
      G = false;
    }
    localObject1 = new com/truecaller/service/h;
    ((com.truecaller.service.h)localObject1).<init>(this, DataManagerService.class);
    I = ((com.truecaller.service.h)localObject1);
    AlarmReceiver.a(paramBundle);
    AlarmReceiver.a(this, false);
    localObject1 = new com/truecaller/ui/TruecallerInit$b;
    ((TruecallerInit.b)localObject1).<init>(this);
    w = ((TruecallerInit.b)localObject1);
    localObject1 = getContentResolver();
    localObject2 = ContactsContract.Contacts.CONTENT_URI;
    Object localObject3 = w;
    ((ContentResolver)localObject1).registerContentObserver((Uri)localObject2, i3, (ContentObserver)localObject3);
    n();
    localObject1 = new com/truecaller/ui/TruecallerInit$5;
    ((TruecallerInit.5)localObject1).<init>(this);
    localObject2 = new com/truecaller/ui/TruecallerInit$6;
    ((TruecallerInit.6)localObject2).<init>(this);
    ((AppBarLayout.Behavior)localObject1).a((AppBarLayout.BaseBehavior.a)localObject2);
    localObject2 = (CoordinatorLayout.e)d.getLayoutParams();
    ((CoordinatorLayout.e)localObject2).a((CoordinatorLayout.b)localObject1);
    int i6 = 2131364985;
    localObject1 = (ImageView)findViewById(i6);
    bool8 = co.c(this);
    if (bool8)
    {
      i10 = 2131234510;
      ((ImageView)localObject1).setImageResource(i10);
    }
    int i10 = 2130969581;
    int i12 = com.truecaller.utils.ui.b.a(this, i10);
    com.truecaller.utils.ui.b.a((ImageView)localObject1, i12);
    com.truecaller.util.at.a(b);
    localObject1 = b;
    localObject3 = new com/truecaller/ui/-$$Lambda$TruecallerInit$OPGNt9NxO81Yh3bayuiHiKEM2gA;
    ((-..Lambda.TruecallerInit.OPGNt9NxO81Yh3bayuiHiKEM2gA)localObject3).<init>(this);
    ((Toolbar)localObject1).setOnClickListener((View.OnClickListener)localObject3);
    localObject1 = b;
    setSupportActionBar((Toolbar)localObject1);
    localObject1 = getSupportActionBar();
    if (localObject1 != null)
    {
      ((ActionBar)localObject1).setDisplayHomeAsUpEnabled(i3);
      ((ActionBar)localObject1).setHomeButtonEnabled(i3);
      ((ActionBar)localObject1).setTitle(0);
    }
    localObject1 = new com/truecaller/ui/TruecallerInit$7;
    localObject3 = i;
    Object localObject4 = b;
    ((TruecallerInit.7)localObject1).<init>(this, this, (DrawerLayout)localObject3, (Toolbar)localObject4);
    l = ((ActionBarDrawerToggle)localObject1);
    localObject1 = new com/truecaller/ui/view/BadgeDrawerArrowDrawable;
    ((BadgeDrawerArrowDrawable)localObject1).<init>(this);
    n = ((BadgeDrawerArrowDrawable)localObject1);
    localObject1 = n;
    i10 = com.truecaller.utils.ui.b.a(this, i10);
    ((BadgeDrawerArrowDrawable)localObject1).setColor(i10);
    localObject1 = l;
    localObject2 = n;
    ((ActionBarDrawerToggle)localObject1).setDrawerArrowDrawable((DrawerArrowDrawable)localObject2);
    localObject1 = i;
    localObject2 = l;
    ((DrawerLayout)localObject1).a((DrawerLayout.c)localObject2);
    localObject1 = i;
    localObject2 = android.support.v4.content.b.a(((DrawerLayout)localObject1).getContext(), 2131231159);
    boolean bool10 = DrawerLayout.c;
    if (!bool10)
    {
      m = ((Drawable)localObject2);
      ((DrawerLayout)localObject1).a();
      ((DrawerLayout)localObject1).invalidate();
    }
    l.syncState();
    m.setup(this);
    localObject1 = m.getFab();
    localObject2 = new com/truecaller/ui/-$$Lambda$TruecallerInit$KnohTY0PNh-K6mDnqHnzlwBAoWo;
    ((-..Lambda.TruecallerInit.KnohTY0PNh-K6mDnqHnzlwBAoWo)localObject2).<init>(this);
    ((android.support.design.widget.FloatingActionButton)localObject1).setOnLongClickListener((View.OnLongClickListener)localObject2);
    localObject1 = m.b("messages");
    localObject2 = new com/truecaller/ui/-$$Lambda$TruecallerInit$03x4bTudoIqpVz1CZkaBr8i4NdY;
    ((-..Lambda.TruecallerInit.03x4bTudoIqpVz1CZkaBr8i4NdY)localObject2).<init>(this);
    ((ImageView)localObject1).setOnLongClickListener((View.OnLongClickListener)localObject2);
    localObject1 = m.b("contacts");
    localObject2 = new com/truecaller/ui/-$$Lambda$TruecallerInit$V2KbhKI5Y66X8wypkZB1hSiXOHU;
    ((-..Lambda.TruecallerInit.V2KbhKI5Y66X8wypkZB1hSiXOHU)localObject2).<init>(this);
    ((ImageView)localObject1).setOnLongClickListener((View.OnLongClickListener)localObject2);
    p();
    localObject1 = v;
    boolean bool5 = ((TrueApp)localObject1).p();
    localObject2 = j.getMenu();
    if (bool5)
    {
      localObject3 = getLayoutInflater();
      int i16 = 2131558832;
      localObject5 = j;
      localObject3 = ((LayoutInflater)localObject3).inflate(i16, (ViewGroup)localObject5, false);
      localObject4 = j.c;
      ((android.support.design.internal.c)localObject4).a((View)localObject3);
      y();
    }
    int i13 = 2131362903;
    localObject3 = ((Menu)localObject2).findItem(i13);
    boolean bool14 = H;
    if (bool14)
    {
      localObject4 = v;
      bool14 = ((TrueApp)localObject4).isTcPayEnabled();
      if (!bool14)
      {
        bool14 = false;
        localObject4 = null;
        break label1477;
      }
    }
    bool14 = true;
    label1477:
    ((MenuItem)localObject3).setVisible(bool14);
    i13 = 2131362910;
    localObject3 = ((Menu)localObject2).findItem(i13);
    localObject4 = (ImageView)((MenuItem)localObject3).getActionView();
    Object localObject5 = new com/truecaller/ui/view/c;
    int i18 = 2130969528;
    ((com.truecaller.ui.view.c)localObject5).<init>(this, false, false, i18);
    y = ((com.truecaller.ui.view.c)localObject5);
    localObject5 = y;
    ((ImageView)localObject4).setImageDrawable((Drawable)localObject5);
    ((MenuItem)localObject3).setVisible(bool5);
    localObject3 = (DrawerHeaderView)j.a(0);
    k = ((DrawerHeaderView)localObject3);
    k.setDrawerHeaderListener(this);
    localObject3 = j;
    localObject4 = new com/truecaller/ui/-$$Lambda$TruecallerInit$sk2X065LqlaYETShpqc1T8pVtL4;
    ((-..Lambda.TruecallerInit.sk2X065LqlaYETShpqc1T8pVtL4)localObject4).<init>(this);
    ((NavigationView)localObject3).setNavigationItemSelectedListener((NavigationView.a)localObject4);
    localObject3 = i;
    localObject4 = new com/truecaller/ui/TruecallerInit$a;
    ((TruecallerInit.a)localObject4).<init>(this, (byte)0);
    ((DrawerLayout)localObject3).a((DrawerLayout.c)localObject4);
    localObject3 = N;
    if (localObject3 != null)
    {
      localObject4 = ReferralManager.ReferralLaunchContext.NAVIGATION_DRAWER;
      bool11 = ((ReferralManager)localObject3).c((ReferralManager.ReferralLaunchContext)localObject4);
      if (bool11)
      {
        bool11 = true;
        break label1684;
      }
    }
    boolean bool11 = false;
    localObject3 = null;
    label1684:
    localObject4 = v;
    bool14 = ((TrueApp)localObject4).o();
    if ((bool14) && (!bool11))
    {
      bool14 = true;
    }
    else
    {
      bool14 = false;
      localObject4 = null;
    }
    localObject5 = ((Menu)localObject2).findItem(2131362916);
    boolean bool15 = a.bk().a();
    ((MenuItem)localObject5).setVisible(bool15);
    localObject5 = (ImageView)((MenuItem)localObject5).getActionView();
    com.truecaller.ui.view.c localc = new com/truecaller/ui/view/c;
    localc.<init>(this, false, false, i18);
    z = localc;
    Object localObject6 = z;
    ((ImageView)localObject5).setImageDrawable((Drawable)localObject6);
    int i19 = 2131362913;
    localObject5 = ((Menu)localObject2).findItem(i19);
    ((MenuItem)localObject5).setVisible(bool5);
    ((Menu)localObject2).findItem(2131362915).setVisible(bool14);
    int i7 = 2131362912;
    localObject1 = ((Menu)localObject2).findItem(i7);
    ((MenuItem)localObject1).setVisible(bool11);
    if (bool11)
    {
      localObject3 = a.ao().a("referalInvite_18335");
      localObject4 = "inviteEarn";
      bool14 = ((String)localObject4).equalsIgnoreCase((String)localObject3);
      if (bool14)
      {
        int i14 = 2131888644;
        ((MenuItem)localObject1).setTitle(i14);
      }
      else
      {
        localObject4 = "referEarn";
        boolean bool12 = ((String)localObject4).equalsIgnoreCase((String)localObject3);
        if (bool12)
        {
          i15 = 2131888645;
          ((MenuItem)localObject1).setTitle(i15);
        }
        else
        {
          i15 = 2131888643;
          ((MenuItem)localObject1).setTitle(i15);
        }
      }
    }
    boolean bool6 = u.a("featureOfflineDirectory", false);
    localObject3 = j.getMenu();
    int i17 = 2131362905;
    localObject3 = ((Menu)localObject3).findItem(i17);
    ((MenuItem)localObject3).setVisible(bool6);
    a((Menu)localObject2);
    localObject1 = v;
    bool6 = ((TrueApp)localObject1).isTcPayEnabled();
    if (bool6)
    {
      localObject1 = Truepay.getInstance();
      bool6 = ((Truepay)localObject1).shouldShowBankingAsNew();
      localObject2 = Truepay.getInstance();
      boolean bool9 = ((Truepay)localObject2).shouldShowPaymentsAsNew();
      localObject3 = m;
      localObject4 = b;
      if (localObject4 != null)
      {
        localObject4 = c;
        if (localObject4 != null)
        {
          i17 = 8;
          if (bool6)
          {
            b.setVisibility(0);
            localObject1 = c;
            ((ImageView)localObject1).setVisibility(i17);
          }
          else
          {
            if (bool9)
            {
              bool6 = d;
              if (!bool6)
              {
                b.setVisibility(i17);
                localObject1 = c;
                ((ImageView)localObject1).setVisibility(0);
                break label2219;
              }
            }
            b.setVisibility(i17);
            localObject1 = c;
            ((ImageView)localObject1).setVisibility(i17);
          }
        }
      }
    }
    label2219:
    localObject1 = a.aI();
    localObject2 = Q;
    int i11 = ((com.truecaller.utils.d)localObject2).h();
    int i15 = 26;
    if (i11 >= i15)
    {
      i11 = 1;
    }
    else
    {
      i11 = 0;
      localObject2 = null;
    }
    boolean bool13 = ((com.truecaller.common.h.c)localObject1).a();
    if ((i11 == 0) && (!bool13))
    {
      localObject2 = a.aA();
      bool6 = ((com.truecaller.common.h.c)localObject1).b();
      if (!bool6)
      {
        localObject1 = "dialerShortcutInstalled";
        bool6 = Settings.e((String)localObject1);
        if (!bool6)
        {
          ((com.truecaller.util.d.a)localObject2).a(0);
          localObject1 = "dialerShortcutInstalled";
          Settings.a((String)localObject1, i3);
        }
      }
      localObject1 = "messagesShortcutInstalled";
      bool6 = Settings.e((String)localObject1);
      if (!bool6)
      {
        ((com.truecaller.util.d.a)localObject2).a(i3);
        localObject1 = "messagesShortcutInstalled";
        Settings.a((String)localObject1, i3);
      }
      localObject1 = "shortcutInstalled";
      bool6 = Settings.e((String)localObject1);
      if (!bool6)
      {
        int i8 = 2;
        ((com.truecaller.util.d.a)localObject2).a(i8);
        localObject1 = "shortcutInstalled";
        Settings.a((String)localObject1, i3);
      }
    }
    localObject1 = S;
    localObject2 = new String[] { "android.permission.READ_SMS" };
    boolean bool7 = ((com.truecaller.utils.l)localObject1).a((String[])localObject2);
    if (bool7)
    {
      localObject1 = a.C();
      ((com.truecaller.messaging.h)localObject1).k(false);
    }
    F = i3;
    b(paramBundle);
    Object localObject7 = N;
    if (localObject7 != null) {
      ai.a(paramBundle, (ReferralManager)localObject7);
    }
    paramBundle = a.C();
    int i2 = paramBundle.t();
    if (i2 == 0)
    {
      paramBundle = v;
      localObject7 = new int[0];
      paramBundle.a((int[])localObject7);
    }
    paramBundle = a.aq();
    boolean bool3 = paramBundle.a();
    if (bool3)
    {
      paramBundle = a.at();
      str = "NOTIFICATIONS";
      localObject1 = "SEARCHRESULTS";
      localObject2 = "BLOCK";
      localObject3 = "BLOCK_UPDATE";
      localObject4 = "CALLLOG";
      localObject5 = "CONTACTS";
      localObject6 = "INBOX";
      String[] tmp2615_2612 = new String[8];
      String[] tmp2616_2615 = tmp2615_2612;
      String[] tmp2616_2615 = tmp2615_2612;
      tmp2616_2615[0] = "HISTORY";
      tmp2616_2615[1] = str;
      String[] tmp2626_2616 = tmp2616_2615;
      String[] tmp2626_2616 = tmp2616_2615;
      tmp2626_2616[2] = localObject1;
      tmp2626_2616[3] = localObject2;
      String[] tmp2635_2626 = tmp2626_2616;
      String[] tmp2635_2626 = tmp2626_2616;
      tmp2635_2626[4] = localObject3;
      tmp2635_2626[5] = localObject4;
      tmp2635_2626[6] = localObject5;
      String[] tmp2649_2635 = tmp2635_2626;
      tmp2649_2635[7] = localObject6;
      localObject7 = tmp2649_2635;
      paramBundle.a((String[])localObject7);
    }
    paramBundle = P.l();
    bool3 = paramBundle.a();
    if (bool3)
    {
      paramBundle = a.W();
      int i4 = 2131364166;
      paramBundle.a(i4);
    }
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    Object localObject = getMenuInflater();
    int i1 = 2131623958;
    ((MenuInflater)localObject).inflate(i1, paramMenu);
    int i2 = 2131361846;
    localObject = paramMenu.findItem(i2);
    j.a locala = com.truecaller.util.b.at.a(this).a();
    boolean bool = com.truecaller.util.b.j.a(locala);
    if (bool)
    {
      ((MenuItem)localObject).setVisible(true);
      int i3 = b;
      ((MenuItem)localObject).setIcon(i3);
      i1 = d;
      ((MenuItem)localObject).setTitle(i1);
    }
    return super.onCreateOptionsMenu(paramMenu);
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    boolean bool = F;
    if (!bool) {
      return;
    }
    Object localObject1 = L;
    if (localObject1 != null)
    {
      localObject1 = android.support.v4.content.d.a(this);
      localObject2 = L;
      ((android.support.v4.content.d)localObject1).a((BroadcastReceiver)localObject2);
    }
    bool = false;
    localObject1 = null;
    com.truecaller.util.b.at.a = null;
    Object localObject2 = getContentResolver();
    TruecallerInit.b localb = w;
    ((ContentResolver)localObject2).unregisterContentObserver(localb);
    w = null;
    localObject2 = Y;
    if (localObject2 != null)
    {
      int i1 = 1;
      ((Handler)localObject2).removeMessages(i1);
      Y = null;
    }
    localObject1 = I;
    if (localObject1 != null) {
      ((com.truecaller.service.h)localObject1).b();
    }
    localObject1 = J;
    if (localObject1 != null) {
      ((PermissionPoller)localObject1).a();
    }
  }
  
  public void onHamburgerClicked()
  {
    Object localObject = v;
    boolean bool = ((TrueApp)localObject).isTcPayEnabled();
    if (bool)
    {
      localObject = B;
      String str = "banking";
      bool = ((String)localObject).equals(str);
      if (!bool)
      {
        localObject = B;
        str = "payments";
        bool = ((String)localObject).equals(str);
        if (!bool) {}
      }
      else
      {
        localObject = i;
        ((DrawerLayout)localObject).b();
      }
    }
  }
  
  public void onNewIntent(Intent paramIntent)
  {
    super.onNewIntent(paramIntent);
    setIntent(paramIntent);
    n();
    Object localObject1 = paramIntent.getStringExtra("AppUserInteraction.Context");
    K = ((String)localObject1);
    a(paramIntent);
    localObject1 = m;
    if (localObject1 == null) {
      return;
    }
    p();
    b(paramIntent);
    localObject1 = N;
    if (localObject1 != null)
    {
      Object localObject2 = O;
      if (localObject2 != null)
      {
        localObject2 = paramIntent.getData();
        ((ReferralManager)localObject1).a((Uri)localObject2);
      }
      localObject1 = N;
      ai.a(paramIntent, (ReferralManager)localObject1);
    }
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    Object localObject1 = l;
    boolean bool1 = ((ActionBarDrawerToggle)localObject1).onOptionsItemSelected(paramMenuItem);
    if (bool1) {
      return true;
    }
    int i1 = paramMenuItem.getItemId();
    int i2 = 2131361846;
    if (i1 == i2)
    {
      localObject1 = com.truecaller.util.b.at.a(this).a();
      boolean bool2 = com.truecaller.util.b.j.a((j.a)localObject1);
      if (bool2)
      {
        int i3 = 2131559161;
        Object localObject2 = View.inflate(this, i3, null);
        Object localObject3 = new android/support/v7/app/AlertDialog$Builder;
        ((AlertDialog.Builder)localObject3).<init>(this);
        localObject3 = ((AlertDialog.Builder)localObject3).setView((View)localObject2).create();
        Object localObject4 = (TextView)((View)localObject2).findViewById(2131364807);
        int i4 = c;
        ((TextView)localObject4).setCompoundDrawablesWithIntrinsicBounds(0, 0, i4, 0);
        i4 = d;
        ((TextView)localObject4).setText(i4);
        int i5 = 2131363665;
        localObject2 = (ListView)((View)localObject2).findViewById(i5);
        localObject4 = new com/truecaller/util/b/e$a;
        Object localObject5 = getResources();
        int i6 = e;
        localObject5 = ((Resources)localObject5).getStringArray(i6);
        ((com.truecaller.util.b.e.a)localObject4).<init>(this, (String[])localObject5);
        ((ListView)localObject2).setAdapter((ListAdapter)localObject4);
        localObject4 = new com/truecaller/util/b/e$1;
        ((e.1)localObject4).<init>((AlertDialog)localObject3, this, (j.a)localObject1);
        ((ListView)localObject2).setOnItemClickListener((AdapterView.OnItemClickListener)localObject4);
        ((AlertDialog)localObject3).show();
        localObject2 = TrueApp.y().a().c();
        localObject3 = new com/truecaller/analytics/e$a;
        ((com.truecaller.analytics.e.a)localObject3).<init>("CARRIER_Menu_Opened");
        localObject4 = "Partner";
        localObject1 = a;
        localObject1 = ((com.truecaller.analytics.e.a)localObject3).a((String)localObject4, (String)localObject1).a();
        ((com.truecaller.analytics.b)localObject2).a((com.truecaller.analytics.e)localObject1);
      }
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  public void onPause()
  {
    super.onPause();
    Object localObject = e;
    boolean bool = localObject instanceof ab;
    if (bool)
    {
      localObject = (ab)e;
      ((ab)localObject).a(false);
    }
    D = false;
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    int i1 = 7004;
    if (paramInt == i1)
    {
      paramInt = paramArrayOfInt.length;
      if (paramInt > 0)
      {
        Intent localIntent = null;
        paramInt = paramArrayOfInt[0];
        if (paramInt == 0)
        {
          localIntent = getIntent();
          com.truecaller.util.a.a(this, localIntent);
        }
      }
    }
  }
  
  public void onResume()
  {
    super.onResume();
    boolean bool1 = false;
    Object localObject1 = null;
    boolean bool2 = ForcedUpdate.a(this, false);
    if (bool2)
    {
      overridePendingTransition(0, 0);
      finish();
      return;
    }
    Object localObject2 = v;
    bool2 = ((TrueApp)localObject2).o();
    boolean bool3 = true;
    if (bool2)
    {
      localObject2 = v;
      bool2 = ((TrueApp)localObject2).p();
      if (!bool2)
      {
        v.a(false);
        com.truecaller.wizard.b.c.a(this, WizardActivity.class, null, bool3);
        return;
      }
    }
    localObject2 = v;
    bool2 = ((TrueApp)localObject2).q();
    if (bool2)
    {
      localObject2 = new com/truecaller/ui/TruecallerInit$8;
      ((TruecallerInit.8)localObject2).<init>(this);
      L = ((BroadcastReceiver)localObject2);
      localObject2 = android.support.v4.content.d.a(this);
      Object localObject3 = L;
      Object localObject4 = new android/content/IntentFilter;
      String str = "com.truecaller.wizard.ACTION_AUTO_LOGIN";
      ((IntentFilter)localObject4).<init>(str);
      ((android.support.v4.content.d)localObject2).a((BroadcastReceiver)localObject3, (IntentFilter)localObject4);
      localObject2 = v;
      localObject3 = new Object[bool3];
      localObject4 = "Wizard auto login";
      localObject3[0] = localObject4;
      com.truecaller.debug.log.a.a((Object[])localObject3);
      localObject1 = new com/truecaller/wizard/utils/b;
      ((com.truecaller.wizard.utils.b)localObject1).<init>((Context)localObject2);
      ((com.truecaller.wizard.utils.b)localObject1).h();
    }
    w();
    localObject1 = J;
    if (localObject1 != null) {
      ((PermissionPoller)localObject1).a();
    }
    a.o().a(bool3);
    r();
    localObject1 = e;
    bool1 = localObject1 instanceof ab;
    if (bool1)
    {
      localObject1 = (ab)e;
      ((ab)localObject1).n();
    }
    s();
    D = bool3;
    localObject1 = j.getMenu();
    a((Menu)localObject1);
    ((com.truecaller.presence.c)a.ae().a()).b();
    a.cb().b();
    a.ce().a(this);
    m.a();
  }
  
  public void onStart()
  {
    super.onStart();
    Object localObject1 = I;
    if (localObject1 != null) {
      ((com.truecaller.service.h)localObject1).a();
    }
    z();
    boolean bool1 = E;
    int i1 = 1;
    if (!bool1)
    {
      localObject1 = (com.truecaller.callhistory.a)a.ad().a();
      ((com.truecaller.callhistory.a)localObject1).a();
      E = i1;
    }
    bool1 = x;
    com.truecaller.ui.components.c localc1 = null;
    if (bool1)
    {
      x = false;
      SyncPhoneBookService.a(this);
      localObject1 = a.aF().A();
      bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
      if (bool1)
      {
        localObject1 = a;
        ((bp)localObject1).bA();
      }
    }
    localObject1 = V;
    Object localObject2 = { "com.truecaller.notification.action.NOTIFICATIONS_UPDATED" };
    com.truecaller.utils.extensions.i.a(this, (BroadcastReceiver)localObject1, (String[])localObject2);
    localObject1 = W;
    localObject2 = new String[] { "com.truecaller.action.UPDATE_CALL_BADGE" };
    com.truecaller.utils.extensions.i.a(this, (BroadcastReceiver)localObject1, (String[])localObject2);
    localObject1 = X;
    localObject2 = new String[i1];
    Object localObject3 = com.truecaller.common.network.g.c.a;
    localObject2[0] = localObject3;
    com.truecaller.utils.extensions.i.a(this, (BroadcastReceiver)localObject1, (String[])localObject2);
    U.a(this);
    U.b();
    a.o().a(false);
    x();
    localObject1 = N;
    if (localObject1 != null) {
      ((ReferralManager)localObject1).b();
    }
    localObject1 = n;
    localObject2 = a.ai();
    boolean bool2 = ((com.truecaller.common.f.c)localObject2).o();
    if (bool2)
    {
      localObject2 = u;
      localObject3 = "subscriptionPaymentFailedViewShownOnce";
      bool2 = ((com.truecaller.common.g.a)localObject2).b((String)localObject3);
      if (!bool2)
      {
        localObject2 = BadgeDrawerArrowDrawable.BadgeTag.ERROR;
        break label318;
      }
    }
    localObject2 = BadgeDrawerArrowDrawable.BadgeTag.NONE;
    label318:
    a = ((BadgeDrawerArrowDrawable.BadgeTag)localObject2);
    localObject3 = BadgeDrawerArrowDrawable.1.a;
    int i2 = ((BadgeDrawerArrowDrawable.BadgeTag)localObject2).ordinal();
    i2 = localObject3[i2];
    if (i2 != i1)
    {
      com.truecaller.ui.components.c localc2 = b;
      i2 = d;
      localc2.a(i2);
      b.a = false;
      return;
    }
    localc1 = b;
    i2 = com.truecaller.utils.ui.b.a(c, 2130969559);
    localc1.a(i2);
    b.a = i1;
    ((BadgeDrawerArrowDrawable)localObject1).invalidateSelf();
  }
  
  public void onStop()
  {
    super.onStop();
    Object localObject = I;
    if (localObject != null) {
      ((com.truecaller.service.h)localObject).b();
    }
    Y.removeMessages(1);
    localObject = android.support.v4.content.d.a(this);
    BroadcastReceiver localBroadcastReceiver = V;
    ((android.support.v4.content.d)localObject).a(localBroadcastReceiver);
    localObject = android.support.v4.content.d.a(this);
    localBroadcastReceiver = W;
    ((android.support.v4.content.d)localObject).a(localBroadcastReceiver);
    localObject = android.support.v4.content.d.a(this);
    localBroadcastReceiver = X;
    ((android.support.v4.content.d)localObject).a(localBroadcastReceiver);
    U.c();
    U.b(this);
  }
  
  public void replaceFragment(Fragment paramFragment)
  {
    Object localObject1 = v;
    boolean bool = ((TrueApp)localObject1).isTcPayEnabled();
    if (bool)
    {
      localObject1 = B;
      Object localObject2 = "banking";
      bool = ((String)localObject1).equals(localObject2);
      if (!bool)
      {
        localObject1 = B;
        localObject2 = "payments";
        bool = ((String)localObject1).equals(localObject2);
        if (!bool) {}
      }
      else
      {
        localObject1 = M.a();
        ((android.support.v4.app.o)localObject1).a(false);
        localObject2 = M;
        int i1 = 2131363156;
        localObject2 = ((android.support.v4.app.j)localObject2).a(i1);
        if (localObject2 == null) {
          ((android.support.v4.app.o)localObject1).a(i1, paramFragment);
        } else {
          ((android.support.v4.app.o)localObject1).b(i1, paramFragment);
        }
        ((android.support.v4.app.o)localObject1).c(paramFragment);
        ((android.support.v4.app.o)localObject1).d();
        localObject1 = M;
        ((android.support.v4.app.j)localObject1).b();
        a(paramFragment);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.TruecallerInit
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */