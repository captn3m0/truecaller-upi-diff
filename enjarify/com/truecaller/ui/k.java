package com.truecaller.ui;

import android.content.Context;
import com.truecaller.search.local.model.j;
import com.truecaller.ui.components.SideIndexScroller.a;

final class k
  extends m
  implements SideIndexScroller.a
{
  private final h n;
  
  k(Context paramContext, h paramh)
  {
    super(paramContext);
    n = paramh;
  }
  
  public final int a(char paramChar)
  {
    char c1 = '￿';
    char c2 = ' ';
    if (paramChar == c2) {
      return c1;
    }
    c2 = '★';
    if (paramChar == c2)
    {
      paramChar = super.getItemCount();
      if (paramChar > 0) {
        return 0;
      }
      return c1;
    }
    h localh = n;
    paramChar = localh.a(paramChar);
    if (paramChar < 0) {
      return paramChar;
    }
    c1 = super.getItemCount();
    return paramChar + c1;
  }
  
  final char b(int paramInt)
  {
    int i = super.getItemCount();
    if ((i != 0) && (paramInt < i))
    {
      j localj = (j)a(paramInt);
      if (localj != null)
      {
        paramInt = k;
        i = -1;
        if (paramInt == i) {
          return ' ';
        }
      }
      return '★';
    }
    h localh = n;
    paramInt -= i;
    return localh.b(paramInt);
  }
  
  final int b()
  {
    return super.getItemCount();
  }
  
  public final int getItemCount()
  {
    int i = super.getItemCount();
    int j = n.getItemCount();
    return i + j;
  }
  
  public final int getItemViewType(int paramInt)
  {
    int i = super.getItemCount();
    if (paramInt < i) {
      return 2131365486;
    }
    h localh = n;
    paramInt -= i;
    return localh.getItemViewType(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */