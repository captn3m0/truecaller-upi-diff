package com.truecaller.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import com.truecaller.analytics.bb;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.components.FeedbackItemView;
import com.truecaller.ui.components.FeedbackItemView.DisplaySource;
import com.truecaller.ui.components.FeedbackItemView.FeedbackItem;
import com.truecaller.ui.components.FeedbackItemView.FeedbackItem.FeedbackItemState;
import com.truecaller.ui.components.FeedbackItemView.a;
import com.truecaller.ui.dialogs.c;
import com.truecaller.ui.dialogs.d;
import com.truecaller.ui.dialogs.d.c;
import com.truecaller.utils.extensions.a;

public class FeedbackDialogActivity
  extends AppCompatActivity
  implements FeedbackItemView.a
{
  private FeedbackItemView a;
  private c b;
  private f c;
  
  private void a()
  {
    boolean bool1 = isFinishing();
    if (bool1) {
      return;
    }
    Object localObject1 = getIntent();
    if (localObject1 == null)
    {
      bool1 = false;
      localObject1 = null;
    }
    else
    {
      localObject1 = ((Intent)localObject1).getExtras();
    }
    if (localObject1 == null)
    {
      localObject1 = new android/os/Bundle;
      ((Bundle)localObject1).<init>();
    }
    if (localObject1 != null)
    {
      Object localObject2 = new com/truecaller/ui/dialogs/d$c;
      ((d.c)localObject2).<init>(this);
      i = 2131558567;
      localObject2 = d.a((d.c)localObject2);
      b = ((c)localObject2);
      b.a();
      localObject2 = b.e;
      boolean bool2 = true;
      ((Dialog)localObject2).setCancelable(bool2);
      b.e.setCanceledOnTouchOutside(bool2);
      localObject2 = b.e;
      Object localObject3 = new com/truecaller/ui/-$$Lambda$FeedbackDialogActivity$-dyTqKhgk1sh4jqIwAp2FEy-ECU;
      ((-..Lambda.FeedbackDialogActivity.-dyTqKhgk1sh4jqIwAp2FEy-ECU)localObject3).<init>(this);
      ((Dialog)localObject2).setOnCancelListener((DialogInterface.OnCancelListener)localObject3);
      localObject2 = (FeedbackItemView)b.f;
      localObject3 = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem;
      Object localObject4 = FeedbackItemView.DisplaySource.values();
      int j = ((Bundle)localObject1).getInt("FeedbackDialogActivity.EXTRA_SOURCE", 0);
      localObject4 = localObject4[j];
      FeedbackItemView.FeedbackItem.FeedbackItemState[] arrayOfFeedbackItemState = FeedbackItemView.FeedbackItem.FeedbackItemState.values();
      int i = ((Bundle)localObject1).getInt("FeedbackDialogActivity.EXTRA_STATE", 0);
      localObject1 = arrayOfFeedbackItemState[i];
      ((FeedbackItemView.FeedbackItem)localObject3).<init>((FeedbackItemView.DisplaySource)localObject4, (FeedbackItemView.FeedbackItem.FeedbackItemState)localObject1);
      ((FeedbackItemView)localObject2).setFeedbackItem((FeedbackItemView.FeedbackItem)localObject3);
      ((FeedbackItemView)localObject2).setFeedbackItemListener(this);
      ((FeedbackItemView)localObject2).setDialogStyle(bool2);
      Settings.g("GOOGLE_REVIEW_ASK_TIMESTAMP");
      return;
    }
    new String[1][0] = "Invalid Intent, cannot show feedback dialog";
    finish();
  }
  
  public static void a(Context paramContext, FeedbackItemView.DisplaySource paramDisplaySource, FeedbackItemView.FeedbackItem.FeedbackItemState paramFeedbackItemState)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, FeedbackDialogActivity.class);
    int i = paramDisplaySource.ordinal();
    paramDisplaySource = localIntent.putExtra("FeedbackDialogActivity.EXTRA_SOURCE", i);
    int j = paramFeedbackItemState.ordinal();
    paramDisplaySource = paramDisplaySource.putExtra("FeedbackDialogActivity.EXTRA_STATE", j);
    paramDisplaySource.setFlags(268435456);
    paramContext.startActivity(paramDisplaySource);
  }
  
  public final void a(FeedbackItemView.FeedbackItem paramFeedbackItem)
  {
    paramFeedbackItem = b;
    Object localObject = FeedbackItemView.DisplaySource.BLOCKED_CALL;
    if (paramFeedbackItem == localObject)
    {
      paramFeedbackItem = c;
      localObject = "rateUs";
      String str = "positiveButton";
      bb.a(paramFeedbackItem, (String)localObject, str);
    }
  }
  
  public final void a(FeedbackItemView paramFeedbackItemView)
  {
    a = paramFeedbackItemView;
  }
  
  public final void b(FeedbackItemView.FeedbackItem paramFeedbackItem)
  {
    paramFeedbackItem = b;
    Object localObject = FeedbackItemView.DisplaySource.BLOCKED_CALL;
    if (paramFeedbackItem == localObject)
    {
      paramFeedbackItem = c;
      localObject = "rateUs";
      String str = "negativeButton";
      bb.a(paramFeedbackItem, (String)localObject, str);
    }
  }
  
  public final void c(FeedbackItemView.FeedbackItem paramFeedbackItem)
  {
    paramFeedbackItem = b;
    if (paramFeedbackItem != null)
    {
      paramFeedbackItem = (FeedbackItemView)f;
      if (paramFeedbackItem != null)
      {
        boolean bool = paramFeedbackItem.c();
        if (bool) {}
      }
      else
      {
        paramFeedbackItem = b;
        paramFeedbackItem.b();
        finish();
      }
    }
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (FeedbackItemView)f;
      if (localObject != null)
      {
        paramInt1 = ((FeedbackItemView)localObject).c();
        if (paramInt1 == 0) {}
      }
      else
      {
        localObject = b;
        ((c)localObject).b();
        finish();
      }
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    a.a(this);
    paramBundle = getTheme();
    int i = aresId;
    paramBundle.applyStyle(i, false);
    paramBundle = ((bk)getApplication()).a().f();
    c = paramBundle;
    paramBundle = new android/os/Handler;
    Object localObject = getMainLooper();
    paramBundle.<init>((Looper)localObject);
    localObject = new com/truecaller/ui/-$$Lambda$FeedbackDialogActivity$CnaxZL11SuW3bT6CDevXxMUYY4o;
    ((-..Lambda.FeedbackDialogActivity.CnaxZL11SuW3bT6CDevXxMUYY4o)localObject).<init>(this);
    paramBundle.postDelayed((Runnable)localObject, 2000L);
  }
  
  public void onResume()
  {
    super.onResume();
    FeedbackItemView localFeedbackItemView = a;
    if (localFeedbackItemView != null)
    {
      localFeedbackItemView.b();
      localFeedbackItemView = null;
      a = null;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.FeedbackDialogActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */