package com.truecaller.ui;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.a;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import com.truecaller.common.h.k;
import com.truecaller.ui.components.CircularImageView;
import com.truecaller.ui.components.CyclicProgressBar;
import com.truecaller.ui.components.ScrimInsetsFrameLayout;
import com.truecaller.ui.components.SnappingRelativeLayout;
import com.truecaller.ui.components.r;
import com.truecaller.util.at;
import com.truecaller.util.aw;
import com.truecaller.util.aw.e;
import com.truecaller.utils.ui.b;

public abstract class w
  extends x
  implements View.OnClickListener, aw.e
{
  private boolean a = false;
  private boolean b = false;
  protected SnappingRelativeLayout c;
  protected ViewGroup d;
  protected ImageView e;
  protected View f;
  protected CircularImageView g;
  protected CyclicProgressBar h;
  private boolean i = false;
  private Drawable l;
  private Drawable m;
  private Drawable n;
  private r o;
  private int p;
  private int q;
  private int r;
  
  private void b(boolean paramBoolean)
  {
    boolean bool = s();
    if (!bool) {
      return;
    }
    if (paramBoolean)
    {
      h.a();
      return;
    }
    h.a(false);
  }
  
  private void d(int paramInt)
  {
    Object localObject1 = l;
    float f1;
    int k;
    float f2;
    if (localObject1 == null)
    {
      localObject1 = new android/graphics/drawable/ColorDrawable;
      j = q;
      ((ColorDrawable)localObject1).<init>(j);
      m = ((Drawable)localObject1);
      localObject1 = a.a(getActivity(), 2131230873);
      n = ((Drawable)localObject1);
      localObject1 = new android/graphics/drawable/LayerDrawable;
      j = 2;
      f1 = 2.8E-45F;
      localObject2 = new Drawable[j];
      Drawable localDrawable = m;
      localObject2[0] = localDrawable;
      k = 1;
      f2 = Float.MIN_VALUE;
      localDrawable = n;
      localObject2[k] = localDrawable;
      ((LayerDrawable)localObject1).<init>((Drawable[])localObject2);
      l = ((Drawable)localObject1);
    }
    m.setAlpha(paramInt);
    localObject1 = n;
    int j = 255 - paramInt;
    ((Drawable)localObject1).setAlpha(j);
    localObject1 = l();
    Object localObject2 = l;
    ((Toolbar)localObject1).setBackgroundDrawable((Drawable)localObject2);
    boolean bool = a;
    if (bool)
    {
      bool = k.d();
      j = 1132396544;
      f1 = 255.0F;
      if (bool)
      {
        int i1 = r;
        k = q;
        float f3 = paramInt / f1;
        i1 = at.a(i1, k, f3);
        localObject2 = getActivity().getWindow();
        ((Window)localObject2).setStatusBarColor(i1);
      }
      else
      {
        localObject1 = o;
        k = q;
        ((r)localObject1).a(k);
        localObject1 = o;
        f2 = paramInt / f1;
        ((r)localObject1).a(f2);
      }
    }
    localObject1 = l();
    paramInt <<= 24;
    j = p & 0xFFFFFF;
    paramInt |= j;
    ((Toolbar)localObject1).setTitleTextColor(paramInt);
  }
  
  public static String f(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool) {
      return null;
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString);
    localStringBuilder.append("+blurred");
    return localStringBuilder.toString();
  }
  
  protected void a()
  {
    o = null;
  }
  
  protected final void a(Bitmap paramBitmap)
  {
    boolean bool = s();
    if (bool)
    {
      ImageView localImageView = e;
      if (localImageView != null)
      {
        View localView = f;
        if (localView != null)
        {
          localImageView.setImageBitmap(paramBitmap);
          f.setVisibility(0);
          return;
        }
      }
    }
  }
  
  public final void a(ImageView paramImageView)
  {
    b(true);
  }
  
  public final void a(ImageView paramImageView, Bitmap paramBitmap, String paramString)
  {
    boolean bool = s();
    if (!bool) {
      return;
    }
    if (paramBitmap != null)
    {
      bool = a(paramString);
      if (!bool) {
        a(paramString, paramBitmap);
      }
    }
    else
    {
      bool = false;
      a(null);
    }
    b(false);
  }
  
  protected final void a(String paramString, Bitmap paramBitmap)
  {
    new w.6(this, paramString, paramBitmap);
  }
  
  protected boolean a(String paramString)
  {
    paramString = f(paramString);
    if (paramString == null) {
      paramString = null;
    } else {
      paramString = aw.a(paramString);
    }
    if (paramString != null)
    {
      a(paramString);
      return true;
    }
    return false;
  }
  
  public final void b(ImageView paramImageView)
  {
    b(false);
  }
  
  public final void d_(boolean paramBoolean)
  {
    b(true);
  }
  
  public final void e()
  {
    b(false);
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = (SnappingRelativeLayout)paramView.findViewById(2131363849);
    c = paramBundle;
    paramBundle = (ViewGroup)paramView.findViewById(2131363992);
    d = paramBundle;
    paramBundle = (ImageView)paramView.findViewById(2131363990);
    e = paramBundle;
    paramBundle = paramView.findViewById(2131363991);
    f = paramBundle;
    int j = 2131363998;
    Object localObject1 = (CircularImageView)paramView.findViewById(j);
    g = ((CircularImageView)localObject1);
    localObject1 = (CyclicProgressBar)paramView.findViewById(2131363997);
    h = ((CyclicProgressBar)localObject1);
    int k = b.a(getContext(), 2130969588);
    p = k;
    k = b.a(getContext(), 2130968594);
    q = k;
    localObject1 = getResources();
    k = ((Resources)localObject1).getColor(2131100525);
    r = k;
    paramView.findViewById(j).setOnClickListener(this);
    paramBundle = getActivity().getWindow();
    k = 1;
    a = k;
    int i2 = Build.VERSION.SDK_INT;
    int i4 = 21;
    Object localObject2;
    if (i2 >= i4)
    {
      localObject2 = paramBundle.getDecorView();
      i4 = 1024;
      ((View)localObject2).setSystemUiVisibility(i4);
      i2 = r;
      paramBundle.setStatusBarColor(i2);
    }
    else
    {
      i2 = 67108864;
      paramBundle.setFlags(i2, i2);
      paramBundle = new com/truecaller/ui/components/r;
      localObject2 = getActivity();
      paramBundle.<init>((Activity)localObject2);
      o = paramBundle;
      paramBundle = o;
      boolean bool1 = a;
      if (bool1)
      {
        paramBundle = b;
        paramBundle.setVisibility(0);
      }
    }
    paramBundle = getActivity();
    int i3 = 2131362435;
    paramBundle = (ScrimInsetsFrameLayout)paramBundle.findViewById(i3);
    if (paramBundle != null)
    {
      localObject2 = new android/graphics/drawable/ColorDrawable;
      ((ColorDrawable)localObject2).<init>(0);
      paramBundle.setInsetForeground((Drawable)localObject2);
    }
    paramBundle = (AppCompatActivity)getActivity();
    i3 = 2131364907;
    paramView = (Toolbar)paramView.findViewById(i3);
    paramBundle.setSupportActionBar(paramView);
    paramView = k();
    if (paramView != null)
    {
      j = 0;
      paramBundle = null;
      paramView.setTitle(null);
      paramView.setHomeButtonEnabled(false);
      paramView.setDisplayHomeAsUpEnabled(k);
      paramView.setDisplayShowHomeEnabled(false);
      paramView.setDisplayShowTitleEnabled(false);
    }
    boolean bool2 = a;
    if (bool2)
    {
      paramView = d.getLayoutParams();
      paramBundle = getResources();
      int i1 = 2131165380;
      j = paramBundle.getDimensionPixelSize(i1);
      height = j;
    }
    d(0);
    paramView = c;
    paramBundle = new com/truecaller/ui/w$1;
    paramBundle.<init>(this);
    paramView.setOnSnapListener(paramBundle);
    paramView = g;
    if (paramView != null) {
      paramView.setListener(this);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */