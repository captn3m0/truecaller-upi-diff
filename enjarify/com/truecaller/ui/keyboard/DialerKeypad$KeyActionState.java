package com.truecaller.ui.keyboard;

public enum DialerKeypad$KeyActionState
{
  static
  {
    Object localObject = new com/truecaller/ui/keyboard/DialerKeypad$KeyActionState;
    ((KeyActionState)localObject).<init>("DOWN", 0);
    DOWN = (KeyActionState)localObject;
    localObject = new com/truecaller/ui/keyboard/DialerKeypad$KeyActionState;
    int i = 1;
    ((KeyActionState)localObject).<init>("UP", i);
    UP = (KeyActionState)localObject;
    localObject = new com/truecaller/ui/keyboard/DialerKeypad$KeyActionState;
    int j = 2;
    ((KeyActionState)localObject).<init>("CANCEL", j);
    CANCEL = (KeyActionState)localObject;
    localObject = new KeyActionState[3];
    KeyActionState localKeyActionState = DOWN;
    localObject[0] = localKeyActionState;
    localKeyActionState = UP;
    localObject[i] = localKeyActionState;
    localKeyActionState = CANCEL;
    localObject[j] = localKeyActionState;
    $VALUES = (KeyActionState[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.keyboard.DialerKeypad.KeyActionState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */