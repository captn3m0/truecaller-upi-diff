package com.truecaller.ui.keyboard;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import com.truecaller.common.e.f;
import com.truecaller.util.at;
import com.truecaller.utils.ui.b;
import java.util.List;

public final class a
  extends View
{
  private static Typeface a;
  private final Paint b;
  private final Paint c;
  private final Rect d;
  private final Rect e;
  private final int f;
  private final int g;
  private boolean h;
  private CharSequence i;
  private Drawable j;
  private CharSequence k;
  private Drawable l;
  private CharSequence m;
  
  private a(Context paramContext)
  {
    super(paramContext);
    Object localObject1 = new android/graphics/Rect;
    ((Rect)localObject1).<init>();
    d = ((Rect)localObject1);
    localObject1 = new android/graphics/Rect;
    ((Rect)localObject1).<init>();
    e = ((Rect)localObject1);
    i = "";
    localObject1 = new android/graphics/Paint;
    int n = 1;
    ((Paint)localObject1).<init>(n);
    b = ((Paint)localObject1);
    localObject1 = b;
    int i1 = b.a(paramContext, 2130969087);
    ((Paint)localObject1).setColor(i1);
    localObject1 = b;
    float f1 = at.b(paramContext, 30.0F);
    ((Paint)localObject1).setTextSize(f1);
    localObject1 = b;
    Object localObject2 = Paint.Align.CENTER;
    ((Paint)localObject1).setTextAlign((Paint.Align)localObject2);
    localObject1 = new android/graphics/Paint;
    ((Paint)localObject1).<init>(n);
    c = ((Paint)localObject1);
    localObject1 = c;
    i1 = b.a(paramContext, 2130969089);
    ((Paint)localObject1).setColor(i1);
    localObject1 = c;
    i1 = at.b(paramContext, 10.0F);
    f1 = i1;
    ((Paint)localObject1).setTextSize(f1);
    localObject1 = c;
    localObject2 = Paint.Align.LEFT;
    ((Paint)localObject1).setTextAlign((Paint.Align)localObject2);
    localObject1 = a;
    if (localObject1 == null)
    {
      i1 = 0;
      f1 = 0.0F;
      localObject2 = null;
      localObject1 = Typeface.create("sans-serif-medium", 0);
      a = (Typeface)localObject1;
    }
    localObject1 = c;
    localObject2 = a;
    ((Paint)localObject1).setTypeface((Typeface)localObject2);
    int i2 = at.a(getContext(), 3.0F);
    f = i2;
    i2 = at.a(getContext(), 24.0F);
    g = i2;
    paramContext = b.c(paramContext, 2130969082);
    setBackgroundDrawable(paramContext);
    setClickable(n);
    boolean bool = f.b();
    h = bool;
  }
  
  public static a a(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    a locala = new com/truecaller/ui/keyboard/a;
    locala.<init>(paramContext);
    int n = 0;
    paramContext = null;
    int i1 = paramString1.charAt(0);
    int i2 = 2130969088;
    int i3 = 42;
    if (i1 == i3)
    {
      paramContext = locala.getContext();
      i1 = 2131234054;
      paramContext = at.b(paramContext, i1, i2);
      j = paramContext;
    }
    else
    {
      n = paramString1.charAt(0);
      i1 = 35;
      if (n == i1)
      {
        paramContext = locala.getContext();
        i1 = 2131234053;
        paramContext = at.b(paramContext, i1, i2);
        j = paramContext;
      }
    }
    i = paramString1;
    k = paramString2;
    m = paramString3;
    paramContext = "+";
    boolean bool = paramContext.equals(paramString2);
    if (bool)
    {
      paramContext = c;
      paramString1 = locala.getContext();
      float f1 = 14.0F;
      int i4 = at.b(paramString1, f1);
      float f2 = i4;
      paramContext.setTextSize(f2);
    }
    return locala;
  }
  
  private int getMainTextX()
  {
    Rect localRect = d;
    int n = localRect.centerX();
    boolean bool = h;
    int i1;
    if (bool) {
      i1 = g;
    } else {
      i1 = -g;
    }
    i1 /= 2;
    return n + i1;
  }
  
  private CharSequence getTertiaryText()
  {
    Object localObject = m;
    if (localObject == null) {
      localObject = "";
    }
    return (CharSequence)localObject;
  }
  
  public final CharSequence getMainText()
  {
    return i;
  }
  
  public final CharSequence getSecondaryText()
  {
    Object localObject = k;
    if (localObject == null) {
      localObject = "";
    }
    return (CharSequence)localObject;
  }
  
  protected final void onDraw(Canvas paramCanvas)
  {
    Object localObject1 = j;
    CharSequence localCharSequence1 = null;
    Object localObject2;
    Object localObject3;
    int n;
    Object localObject4;
    Object localObject5;
    CharSequence localCharSequence2;
    int i1;
    float f2;
    Object localObject6;
    if (localObject1 == null)
    {
      localObject1 = b;
      localObject2 = getMainText().toString();
      localObject3 = getMainText();
      n = ((CharSequence)localObject3).length();
      localObject4 = e;
      ((Paint)localObject1).getTextBounds((String)localObject2, 0, n, (Rect)localObject4);
      localObject5 = getMainText();
      localCharSequence2 = null;
      i1 = getMainText().length();
      float f1 = getMainTextX();
      localObject1 = d;
      int i2 = ((Rect)localObject1).centerY();
      localObject2 = e;
      int i4 = ((Rect)localObject2).height() / 2;
      i2 += i4;
      f2 = i2;
      Paint localPaint1 = b;
      localObject6 = paramCanvas;
      paramCanvas.drawText((CharSequence)localObject5, 0, i1, f1, f2, localPaint1);
    }
    else
    {
      ((Drawable)localObject1).draw(paramCanvas);
    }
    localObject1 = k;
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1);
    int i3;
    int i7;
    int i8;
    float f3;
    Paint localPaint2;
    if (bool1)
    {
      localObject1 = l;
      if (localObject1 != null) {
        ((Drawable)localObject1).draw(paramCanvas);
      }
    }
    else
    {
      localObject1 = c;
      localObject2 = getSecondaryText().toString();
      localObject3 = getSecondaryText();
      n = ((CharSequence)localObject3).length();
      localObject4 = e;
      ((Paint)localObject1).getTextBounds((String)localObject2, 0, n, (Rect)localObject4);
      localObject1 = d;
      i3 = ((Rect)localObject1).centerX();
      boolean bool2 = h;
      if (bool2)
      {
        i5 = -f;
        localObject3 = e;
        n = ((Rect)localObject3).width();
        i5 -= n;
      }
      else
      {
        i5 = f;
      }
      i3 += i5;
      localObject2 = b;
      localObject3 = getMainText().toString();
      localObject4 = getMainText();
      i7 = ((CharSequence)localObject4).length();
      localObject6 = e;
      ((Paint)localObject2).getTextBounds((String)localObject3, 0, i7, (Rect)localObject6);
      localCharSequence2 = getSecondaryText();
      i1 = 0;
      i8 = getSecondaryText().length();
      f2 = i3;
      localObject1 = d;
      i3 = ((Rect)localObject1).centerY();
      localObject2 = e;
      int i5 = ((Rect)localObject2).height() / 2;
      i3 += i5;
      f3 = i3;
      localPaint2 = c;
      localObject5 = paramCanvas;
      paramCanvas.drawText(localCharSequence2, 0, i8, f2, f3, localPaint2);
    }
    localObject1 = m;
    if (localObject1 != null)
    {
      localObject1 = c;
      localObject2 = getTertiaryText().toString();
      localObject3 = getTertiaryText();
      n = ((CharSequence)localObject3).length();
      localObject4 = e;
      ((Paint)localObject1).getTextBounds((String)localObject2, 0, n, (Rect)localObject4);
      localObject1 = d;
      i3 = ((Rect)localObject1).centerX();
      boolean bool3 = h;
      int i6;
      if (bool3)
      {
        i6 = -f;
        localObject3 = e;
        n = ((Rect)localObject3).width();
        i6 -= n;
      }
      else
      {
        i6 = f;
      }
      i3 += i6;
      localObject2 = b;
      localObject3 = getMainText().toString();
      localObject4 = getMainText();
      i7 = ((CharSequence)localObject4).length();
      localObject6 = e;
      ((Paint)localObject2).getTextBounds((String)localObject3, 0, i7, (Rect)localObject6);
      localCharSequence2 = getTertiaryText();
      i1 = 0;
      localCharSequence1 = getTertiaryText();
      i8 = localCharSequence1.length();
      f2 = i3;
      localObject1 = d;
      i3 = ((Rect)localObject1).centerY();
      f3 = i3;
      localPaint2 = c;
      localObject5 = paramCanvas;
      paramCanvas.drawText(localCharSequence2, 0, i8, f2, f3, localPaint2);
    }
  }
  
  public final void onPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
  {
    super.onPopulateAccessibilityEvent(paramAccessibilityEvent);
    Object localObject = paramAccessibilityEvent.getText();
    CharSequence localCharSequence = getMainText();
    ((List)localObject).add(localCharSequence);
    localObject = paramAccessibilityEvent.getText();
    localCharSequence = getSecondaryText();
    ((List)localObject).add(localCharSequence);
    paramAccessibilityEvent = paramAccessibilityEvent.getText();
    localObject = getTertiaryText();
    paramAccessibilityEvent.add(localObject);
  }
  
  protected final void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    Object localObject1 = d;
    paramInt4 = 0;
    Drawable localDrawable = null;
    ((Rect)localObject1).set(0, 0, paramInt1, paramInt2);
    paramInt1 = Build.VERSION.SDK_INT;
    paramInt2 = 21;
    Object localObject3;
    Object localObject4;
    int n;
    Object localObject5;
    int i1;
    Object localObject6;
    int i2;
    if (paramInt1 >= paramInt2)
    {
      localObject2 = getBackground();
      localObject3 = getContext();
      float f1 = 24.0F;
      paramInt2 = at.a((Context)localObject3, f1);
      localObject1 = d;
      paramInt3 = ((Rect)localObject1).centerX() - paramInt2;
      localObject4 = d;
      n = ((Rect)localObject4).centerY() - paramInt2;
      localObject5 = d;
      i1 = ((Rect)localObject5).centerX() + paramInt2;
      localObject6 = d;
      i2 = ((Rect)localObject6).centerY() + paramInt2;
      ((Drawable)localObject2).setHotspotBounds(paramInt3, n, i1, i2);
    }
    Object localObject2 = j;
    if (localObject2 != null)
    {
      localObject2 = d;
      paramInt1 = ((Rect)localObject2).centerX();
      paramInt2 = h;
      if (paramInt2 != 0)
      {
        paramInt2 = 0;
        localObject3 = null;
      }
      else
      {
        localObject3 = j;
        paramInt2 = -((Drawable)localObject3).getIntrinsicWidth();
      }
      paramInt1 += paramInt2;
      localObject3 = j;
      paramInt2 = ((Drawable)localObject3).getIntrinsicWidth() + paramInt1;
      localObject1 = j;
      localObject4 = d;
      n = ((Rect)localObject4).centerY();
      i1 = j.getIntrinsicHeight() / 2;
      n -= i1;
      localObject5 = d;
      i1 = ((Rect)localObject5).centerY();
      localObject6 = j;
      i2 = ((Drawable)localObject6).getIntrinsicHeight() / 2;
      i1 += i2;
      ((Drawable)localObject1).setBounds(paramInt1, n, paramInt2, i1);
    }
    localObject2 = l;
    if (localObject2 != null)
    {
      localObject2 = d;
      paramInt1 = ((Rect)localObject2).centerX();
      paramInt2 = h;
      if (paramInt2 != 0)
      {
        paramInt2 = -f;
        localObject1 = l;
        paramInt3 = ((Drawable)localObject1).getIntrinsicWidth();
        paramInt2 -= paramInt3;
      }
      else
      {
        paramInt2 = f;
      }
      paramInt1 += paramInt2;
      localObject3 = l;
      paramInt2 = ((Drawable)localObject3).getIntrinsicWidth() + paramInt1;
      localObject1 = b;
      localObject4 = getMainText().toString();
      localObject5 = getMainText();
      i1 = ((CharSequence)localObject5).length();
      localObject6 = e;
      ((Paint)localObject1).getTextBounds((String)localObject4, 0, i1, (Rect)localObject6);
      localObject1 = d;
      paramInt3 = ((Rect)localObject1).centerY();
      paramInt4 = e.height() / 2;
      paramInt3 += paramInt4;
      localDrawable = l;
      paramInt4 = localDrawable.getIntrinsicHeight();
      paramInt4 = paramInt3 - paramInt4;
      localObject4 = l;
      ((Drawable)localObject4).setBounds(paramInt1, paramInt4, paramInt2, paramInt3);
    }
  }
  
  public final void setSecondaryImage(int paramInt)
  {
    Object localObject = l;
    if (localObject == null)
    {
      localObject = getContext();
      int n = c.getColor();
      ColorStateList localColorStateList = ColorStateList.valueOf(n);
      Drawable localDrawable = at.a((Context)localObject, paramInt, localColorStateList);
      l = localDrawable;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.keyboard.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */