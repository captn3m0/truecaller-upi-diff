package com.truecaller.ui.keyboard;

import android.content.Context;
import android.graphics.Rect;
import android.media.AudioManager;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import com.a.a.l;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.old.data.access.Settings;
import com.truecaller.search.local.b.f;
import com.truecaller.ui.view.d;
import com.truecaller.util.an;

public class DialerKeypad
  extends d
  implements View.OnLongClickListener, View.OnTouchListener
{
  public static final boolean a;
  private static final char[] b;
  private String c;
  private Rect[] d;
  private c e;
  private an f;
  private b g;
  private com.truecaller.i.c h;
  
  static
  {
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i >= j)
    {
      i = 1;
    }
    else
    {
      i = 0;
      arrayOfChar = null;
    }
    a = i;
    char[] arrayOfChar = new char[12];
    char[] tmp31_30 = arrayOfChar;
    char[] tmp32_31 = tmp31_30;
    char[] tmp32_31 = tmp31_30;
    tmp32_31[0] = 49;
    tmp32_31[1] = 50;
    char[] tmp41_32 = tmp32_31;
    char[] tmp41_32 = tmp32_31;
    tmp41_32[2] = 51;
    tmp41_32[3] = 52;
    char[] tmp50_41 = tmp41_32;
    char[] tmp50_41 = tmp41_32;
    tmp50_41[4] = 53;
    tmp50_41[5] = 54;
    char[] tmp59_50 = tmp50_41;
    char[] tmp59_50 = tmp50_41;
    tmp59_50[6] = 55;
    tmp59_50[7] = 56;
    char[] tmp70_59 = tmp59_50;
    char[] tmp70_59 = tmp59_50;
    tmp70_59[8] = 57;
    tmp70_59[9] = 42;
    tmp70_59[10] = 48;
    tmp70_59[11] = 35;
    b = arrayOfChar;
  }
  
  public DialerKeypad(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramAttributeSet = new Rect[5];
    d = paramAttributeSet;
    paramContext = ((bk)paramContext.getApplicationContext()).a().D();
    h = paramContext;
  }
  
  private static void a(a parama)
  {
    boolean bool = a;
    if ((!bool) && (parama != null))
    {
      bool = false;
      parama.setPressed(false);
    }
  }
  
  private void a(CharSequence paramCharSequence, DialerKeypad.KeyActionState paramKeyActionState)
  {
    c localc = e;
    if (localc != null)
    {
      char c1 = paramCharSequence.charAt(0);
      localc.a(c1, paramKeyActionState);
    }
  }
  
  private boolean a(int paramInt1, int paramInt2)
  {
    b localb = g;
    if (localb != null) {
      return localb.a(paramInt1, paramInt2);
    }
    return false;
  }
  
  private void b()
  {
    f localf = f.a(c);
    int i = 0;
    for (;;)
    {
      char[] arrayOfChar = b;
      int j = arrayOfChar.length;
      if (i >= j) {
        break;
      }
      char c1 = arrayOfChar[i];
      Object localObject1 = localf.b(c1);
      Object localObject2 = f.a.b(c1);
      boolean bool = TextUtils.equals((CharSequence)localObject1, (CharSequence)localObject2);
      if (bool)
      {
        c2 = '\000';
        localObject2 = null;
      }
      Context localContext = getContext();
      String str = Character.toString(c1);
      localObject1 = a.a(localContext, str, (String)localObject1, (String)localObject2);
      ((a)localObject1).setOnLongClickListener(this);
      ((a)localObject1).setOnTouchListener(this);
      localObject2 = new android/view/ViewGroup$LayoutParams;
      int m = -1;
      ((ViewGroup.LayoutParams)localObject2).<init>(m, m);
      ((a)localObject1).setLayoutParams((ViewGroup.LayoutParams)localObject2);
      char c2 = '1';
      if (c1 == c2)
      {
        int k = 2131234236;
        ((a)localObject1).setSecondaryImage(k);
      }
      addView((View)localObject1, i);
      i += 1;
    }
  }
  
  private void c()
  {
    an localan = f;
    if (localan != null) {
      localan.b();
    }
  }
  
  public final void a()
  {
    boolean bool1 = isInEditMode();
    String str1;
    if (bool1) {
      str1 = "auto";
    } else {
      str1 = Settings.l();
    }
    String str2 = c;
    boolean bool2 = str1.equals(str2);
    if (!bool2)
    {
      c = str1;
      removeAllViews();
      b();
    }
  }
  
  public int getColumnCount()
  {
    return 3;
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
    boolean bool = isInEditMode();
    if (!bool)
    {
      String str = c;
      if (str == null)
      {
        str = Settings.l();
        c = str;
      }
      b();
    }
  }
  
  public boolean onLongClick(View paramView)
  {
    boolean bool1 = paramView instanceof a;
    if (bool1)
    {
      localObject = paramView;
      localObject = ((a)paramView).getMainText().toString();
      boolean bool3 = TextUtils.isEmpty((CharSequence)localObject);
      if (!bool3)
      {
        int k = ((String)localObject).charAt(0);
        int m = 42;
        if (k == m)
        {
          bool1 = a(60819, 0);
          break label200;
        }
        k = ((String)localObject).charAt(0);
        m = 35;
        if (k == m)
        {
          bool1 = a(60820, 0);
          break label200;
        }
        try
        {
          int i = Integer.parseInt((String)localObject);
          int j;
          switch (i)
          {
          default: 
            i = 0;
            localObject = null;
            break;
          case 1: 
          case 2: 
          case 3: 
          case 4: 
          case 5: 
          case 6: 
          case 7: 
          case 8: 
          case 9: 
            k = 60824;
            j = a(k, i);
            break;
          case 0: 
            k = 60822;
            bool2 = a(k, j);
          }
        }
        catch (NumberFormatException localNumberFormatException) {}
      }
    }
    boolean bool2 = false;
    Object localObject = null;
    label200:
    if (bool2) {
      paramView.setPressed(false);
    }
    return bool2;
  }
  
  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    boolean bool1 = paramView instanceof a;
    if (!bool1) {
      return false;
    }
    Object localObject1 = paramView;
    localObject1 = (a)paramView;
    Object localObject2 = ((a)localObject1).getMainText();
    int i = ((CharSequence)localObject2).length();
    int j = 1;
    float f1 = Float.MIN_VALUE;
    if (i != j) {
      return false;
    }
    i = paramMotionEvent.getActionIndex();
    i = paramMotionEvent.getPointerId(i);
    int m = paramMotionEvent.findPointerIndex(i);
    int i1 = 5;
    if (i >= i1) {
      return false;
    }
    i1 = paramMotionEvent.getAction();
    int i5;
    switch (i1)
    {
    default: 
      break;
    case 2: 
      localObject2 = d[i];
      if (localObject2 != null)
      {
        i = paramView.getLeft();
        f1 = paramMotionEvent.getX(m);
        j = (int)f1;
        i += j;
        int i3 = paramView.getTop();
        float f2 = paramMotionEvent.getY(m);
        i5 = (int)f2;
        i3 += i5;
        boolean bool3 = ((Rect)localObject2).contains(i, i3);
        if (!bool3)
        {
          a((a)localObject1);
          c();
          paramView = ((a)localObject1).getMainText();
          paramMotionEvent = DialerKeypad.KeyActionState.CANCEL;
          a(paramView, paramMotionEvent);
        }
      }
      break;
    case 1: 
      paramView = ((a)localObject1).getMainText();
      paramMotionEvent = DialerKeypad.KeyActionState.UP;
      a(paramView, paramMotionEvent);
    case 3: 
      a((a)localObject1);
      c();
      break;
    case 0: 
      i5 = ((CharSequence)localObject2).charAt(0);
      localObject2 = f;
      if (localObject2 != null)
      {
        boolean bool2 = c;
        if (bool2)
        {
          Object localObject3 = b;
          String str = "audio";
          localObject3 = (AudioManager)((Context)localObject3).getSystemService(str);
          i1 = ((AudioManager)localObject3).getRingerMode();
          if ((i1 != 0) && (i1 != j))
          {
            bool2 = ((AudioManager)localObject3).isBluetoothA2dpOn();
            if (!bool2)
            {
              localObject3 = an.a;
              int i6 = ((l)localObject3).a(i5);
              n = -1;
              if (i6 != n)
              {
                localObject2 = d;
                if (localObject2 != null)
                {
                  ((Handler)localObject2).removeMessages(j);
                  n = 2000;
                  paramMotionEvent = Message.obtain((Handler)localObject2, 0, i6, n);
                  paramMotionEvent.sendToTarget();
                }
              }
            }
          }
        }
        paramMotionEvent = h;
        localObject2 = "hasNativeDialerCallerId";
        bool4 = paramMotionEvent.b((String)localObject2);
        if (bool4)
        {
          ((a)localObject1).performHapticFeedback(j);
        }
        else
        {
          paramMotionEvent = getContext();
          bool4 = Settings.d(paramMotionEvent);
          if (bool4)
          {
            paramMotionEvent = f.d;
            if (paramMotionEvent != null)
            {
              int i7 = 2;
              n = 30;
              paramMotionEvent = Message.obtain(paramMotionEvent, i7, n, 0);
              paramMotionEvent.sendToTarget();
            }
          }
        }
      }
      boolean bool4 = a;
      if ((!bool4) && (localObject1 != null)) {
        ((a)localObject1).setPressed(j);
      }
      paramMotionEvent = d;
      localObject2 = new android/graphics/Rect;
      int k = paramView.getLeft();
      int n = paramView.getTop();
      int i2 = paramView.getRight();
      int i4 = paramView.getBottom();
      ((Rect)localObject2).<init>(k, n, i2, i4);
      paramMotionEvent[i] = localObject2;
      paramView = ((a)localObject1).getMainText();
      paramMotionEvent = DialerKeypad.KeyActionState.DOWN;
      a(paramView, paramMotionEvent);
    }
    return false;
  }
  
  public void setActionsListener(b paramb)
  {
    g = paramb;
  }
  
  public void setDialpadListener(c paramc)
  {
    e = paramc;
  }
  
  public void setFeedback(an paraman)
  {
    f = paraman;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.keyboard.DialerKeypad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */