package com.truecaller.ui;

import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.RelativeLayout.LayoutParams;
import com.truecaller.ui.components.SnappingRelativeLayout;

final class w$3
  extends Animation
{
  w$3(w paramw, int paramInt) {}
  
  protected final void applyTransformation(float paramFloat, Transformation paramTransformation)
  {
    paramTransformation = (RelativeLayout.LayoutParams)b.d.getLayoutParams();
    int i = (int)(a * paramFloat);
    paramTransformation.setMargins(0, i, 0, 0);
    b.c.forceLayout();
    b.c.requestLayout();
    b.c.invalidate();
    int j = (int)(paramFloat * 255.0F);
    w.a(b, j);
  }
  
  public final boolean willChangeBounds()
  {
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.w.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */