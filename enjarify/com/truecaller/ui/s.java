package com.truecaller.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;
import c.g.b.k;
import com.truecaller.common.network.userarchive.DownloadDto;
import e.b;
import e.d;
import e.r;
import java.lang.ref.WeakReference;

public final class s
  implements d
{
  private final WeakReference a;
  
  public s(Dialog paramDialog)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramDialog);
    a = localWeakReference;
  }
  
  private final void a(String paramString)
  {
    Dialog localDialog = (Dialog)a.get();
    if (localDialog != null)
    {
      boolean bool = localDialog.isShowing();
      if (!bool) {
        return;
      }
      Intent localIntent;
      if (paramString == null)
      {
        k.a(localDialog, "this");
        paramString = localDialog.getContext();
        int i = 2131886537;
        localIntent = null;
        paramString = Toast.makeText(paramString, i, 0);
        paramString.show();
      }
      else
      {
        Context localContext = localDialog.getContext();
        localIntent = new android/content/Intent;
        String str = "android.intent.action.VIEW";
        paramString = Uri.parse(paramString);
        localIntent.<init>(str, paramString);
        localContext.startActivity(localIntent);
      }
      localDialog.dismiss();
      return;
    }
  }
  
  public final void onFailure(b paramb, Throwable paramThrowable)
  {
    a(null);
  }
  
  public final void onResponse(b paramb, r paramr)
  {
    if (paramr != null)
    {
      paramb = (DownloadDto)paramr.e();
      if (paramb != null)
      {
        paramb = url;
        break label26;
      }
    }
    paramb = null;
    label26:
    a(paramb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */