package com.truecaller.ui.details;

import android.os.AsyncTask;
import android.text.TextUtils;
import com.truecaller.data.entity.Contact;
import com.truecaller.old.a.a;
import com.truecaller.old.a.b;

abstract class DetailsFragment$a
  extends a
{
  Contact a;
  
  private DetailsFragment$a(DetailsFragment paramDetailsFragment) {}
  
  private boolean c()
  {
    Object localObject1 = c;
    boolean bool1 = DetailsFragment.s((DetailsFragment)localObject1);
    boolean bool2 = true;
    if (bool1)
    {
      localObject1 = DetailsFragment.a(c);
      bool1 = ((Contact)localObject1).V();
      if (!bool1)
      {
        localObject1 = DetailsFragment.o(c);
        bool1 = TextUtils.isEmpty((CharSequence)localObject1);
        if (bool1)
        {
          localObject1 = DetailsFragment.a(c);
          bool1 = ((Contact)localObject1).S();
          if (bool1) {
            break label103;
          }
        }
        else
        {
          localObject1 = DetailsFragment.a(c);
          localObject2 = DetailsFragment.o(c);
          bool1 = ((Contact)localObject1).p((String)localObject2);
          if (bool1) {
            break label103;
          }
        }
      }
      bool1 = true;
      break label107;
    }
    label103:
    bool1 = false;
    localObject1 = null;
    label107:
    Object localObject2 = c;
    boolean bool3 = DetailsFragment.t((DetailsFragment)localObject2);
    if (!bool3)
    {
      localObject2 = c;
      bool3 = DetailsFragment.u((DetailsFragment)localObject2);
      if ((!bool3) || (!bool1)) {
        return false;
      }
    }
    return bool2;
  }
  
  public final void a(Object paramObject)
  {
    DetailsFragment localDetailsFragment = c;
    DetailsFragment.d(localDetailsFragment);
    if (paramObject != null)
    {
      localDetailsFragment = c;
      boolean bool1 = localDetailsFragment.s();
      if (bool1)
      {
        localDetailsFragment = c;
        paramObject = (Contact)paramObject;
        DetailsFragment.a(localDetailsFragment, (Contact)paramObject);
        boolean bool2 = c();
        if (bool2)
        {
          paramObject = c;
          bool1 = true;
          DetailsFragment.a((DetailsFragment)paramObject, bool1);
        }
        paramObject = c;
        ((DetailsFragment)paramObject).p();
      }
    }
  }
  
  abstract Contact b();
  
  protected final Object doInBackground(Object... paramVarArgs)
  {
    paramVarArgs = c;
    Contact localContact = b();
    return DetailsFragment.c(paramVarArgs, localContact);
  }
  
  public void onPostExecute(Object paramObject)
  {
    super.onPostExecute(paramObject);
    paramObject = DetailsFragment.a(c);
    if (paramObject != null)
    {
      boolean bool = c();
      Object localObject1 = null;
      Object localObject2;
      Object localObject3;
      if (bool)
      {
        paramObject = DetailsFragment.n(c);
        bool = TextUtils.isEmpty((CharSequence)paramObject);
        if (bool)
        {
          paramObject = DetailsFragment.o(c);
          bool = TextUtils.isEmpty((CharSequence)paramObject);
          if (bool) {}
        }
        else
        {
          bool = true;
          localObject2 = c;
          localObject3 = DetailsFragment.o((DetailsFragment)localObject2);
          String str1 = DetailsFragment.n(c);
          String str2 = DetailsFragment.p(c);
          DetailsFragment.a((DetailsFragment)localObject2, (String)localObject3, str1, str2);
          break label144;
        }
      }
      paramObject = c;
      bool = DetailsFragment.c((DetailsFragment)paramObject);
      if (bool)
      {
        paramObject = c;
        localObject2 = DetailsFragment.a((DetailsFragment)paramObject);
        DetailsFragment.b((DetailsFragment)paramObject, (Contact)localObject2);
      }
      bool = false;
      paramObject = null;
      label144:
      if (!bool)
      {
        paramObject = c;
        bool = ((DetailsFragment)paramObject).s();
        if (bool)
        {
          paramObject = c;
          bool = DetailsFragment.q((DetailsFragment)paramObject);
          if (!bool)
          {
            DetailsFragment.r(c);
            paramObject = new com/truecaller/ui/details/DetailsFragment$g;
            localObject2 = c;
            localObject3 = DetailsFragment.a((DetailsFragment)localObject2);
            ((DetailsFragment.g)paramObject).<init>((DetailsFragment)localObject2, (Contact)localObject3);
            localObject1 = new Object[0];
            b.b((AsyncTask)paramObject, (Object[])localObject1);
          }
        }
      }
      paramObject = a;
      if (paramObject != null)
      {
        localObject1 = new com/truecaller/data/entity/Contact;
        ((Contact)localObject1).<init>((Contact)paramObject);
        paramObject = new com/truecaller/ui/details/DetailsFragment$a$1;
        ((DetailsFragment.a.1)paramObject).<init>(this, (Contact)localObject1);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsFragment.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */