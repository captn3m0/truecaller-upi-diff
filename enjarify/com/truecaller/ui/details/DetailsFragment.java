package com.truecaller.ui.details;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.c;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutCompat.LayoutParams;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.ads.AdSize;
import com.google.c.a.k.d;
import com.truecaller.TrueApp;
import com.truecaller.a.f.b;
import com.truecaller.ads.CustomTemplate;
import com.truecaller.ads.j.a;
import com.truecaller.ads.k.a;
import com.truecaller.ads.k.b;
import com.truecaller.ads.k.c;
import com.truecaller.ads.ui.AdsSwitchView;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.bb;
import com.truecaller.analytics.bc;
import com.truecaller.androidactors.ac;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.calling.ar;
import com.truecaller.calling.initiate_call.b.a.a;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.calling.recorder.CallRecordingOnBoardingLaunchContext;
import com.truecaller.common.h.aa;
import com.truecaller.common.h.am;
import com.truecaller.common.h.n;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Link;
import com.truecaller.data.entity.Note;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.Tag;
import com.truecaller.filters.FilterManager;
import com.truecaller.filters.FilterManager.ActionSource;
import com.truecaller.filters.FilterManager.FilterAction;
import com.truecaller.filters.p;
import com.truecaller.flashsdk.models.FlashContact;
import com.truecaller.log.AssertionUtil;
import com.truecaller.multisim.SimInfo;
import com.truecaller.network.search.j.b;
import com.truecaller.old.data.access.Settings;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.ShineView;
import com.truecaller.profile.data.dto.OpenHours;
import com.truecaller.referral.ReferralManager;
import com.truecaller.referral.ReferralManager.ReferralLaunchContext;
import com.truecaller.swish.SwishInputActivity;
import com.truecaller.swish.g.a;
import com.truecaller.tag.NameSuggestionActivity;
import com.truecaller.tag.TagPickActivity;
import com.truecaller.tracking.events.ao;
import com.truecaller.tracking.events.ao.a;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import com.truecaller.ui.SingleActivity;
import com.truecaller.ui.SingleActivity.FragmentSingle;
import com.truecaller.ui.components.CallerButtonBase;
import com.truecaller.ui.f.a;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.util.at;
import com.truecaller.util.br.a;
import com.truecaller.util.cb;
import com.truecaller.util.cf;
import com.truecaller.voip.ai;
import com.truecaller.whoviewedme.ProfileViewService;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SortedSet;

public final class DetailsFragment
  extends com.truecaller.ui.o
  implements AppBarLayout.c, View.OnClickListener, DetailsActionBar.a, k
{
  public static final List a;
  private static final Object b;
  private View A;
  private TextView B;
  private TextView C;
  private TextView D;
  private AdsSwitchView E;
  private TextView F;
  private TextView G;
  private CallerButtonBase H;
  private CallerButtonBase I;
  private View J;
  private AppBarLayout K;
  private Toolbar L;
  private View M;
  private CardView N;
  private RecyclerView O;
  private TextView P;
  private TextView Q;
  private View R;
  private RelativeLayout S;
  private RecyclerView T;
  private TintedImageView U;
  private int V;
  private ShineView W;
  private View X;
  private Drawable Y;
  private bp Z;
  private boolean aA;
  private boolean aB;
  private boolean aC;
  private boolean aD;
  private boolean aE;
  private boolean aF;
  private DetailsFragment.b aG;
  private boolean aH;
  private ReferralManager aI;
  private boolean aJ;
  private com.truecaller.data.entity.g aK;
  private com.truecaller.androidactors.i aL;
  private com.truecaller.androidactors.f aM;
  private String aN;
  private boolean aO;
  private com.truecaller.common.h.u aP;
  private com.truecaller.data.access.c aQ;
  private cf aR;
  private com.truecaller.calling.initiate_call.b aS;
  private ai aT;
  private com.truecaller.premium.b.a aU;
  private com.truecaller.ads.provider.f aV;
  private com.truecaller.i.a aW;
  private final HashMap aX;
  private int aY;
  private int aZ;
  private Contact aa;
  private String ab;
  private String ac;
  private String ad;
  private ContentObserver ae;
  private com.truecaller.ui.f af;
  private FilterManager ag;
  private com.truecaller.androidactors.f ah;
  private DetailsFragment.SourceType ai;
  private com.truecaller.abtest.c aj;
  private com.truecaller.premium.br ak;
  private String al;
  private AlertDialog am;
  private AlertDialog an;
  private com.truecaller.ui.dialogs.g ao;
  private com.truecaller.search.local.model.g ap;
  private com.truecaller.featuretoggles.e aq;
  private boolean ar;
  private boolean as;
  private boolean at;
  private boolean au;
  private boolean av;
  private boolean aw;
  private boolean ax;
  private boolean ay;
  private boolean az;
  private final Handler ba;
  private final Runnable bb;
  private String bc;
  private com.truecaller.androidactors.a bd;
  private final j.b be;
  private com.truecaller.ads.provider.o bf;
  private e bg;
  private f bh;
  private final ArrayList c;
  private AppCompatActivity d;
  private View e;
  private DetailsHeaderView f;
  private DetailsActionBar g;
  private View h;
  private View i;
  private TextView l;
  private ViewGroup m;
  private ViewGroup n;
  private ViewGroup o;
  private ViewGroup p;
  private View q;
  private ViewGroup r;
  private ViewGroup s;
  private View t;
  private View u;
  private TextView v;
  private View w;
  private TextView x;
  private TextView y;
  private TextView z;
  
  static
  {
    String[] tmp4_1 = new String[5];
    String[] tmp5_4 = tmp4_1;
    String[] tmp5_4 = tmp4_1;
    tmp5_4[0] = "com.whatsapp";
    tmp5_4[1] = "com.facebook.orca";
    String[] tmp14_5 = tmp5_4;
    String[] tmp14_5 = tmp5_4;
    tmp14_5[2] = "com.viber.voip";
    tmp14_5[3] = "com.skype.raider";
    tmp14_5[4] = "org.telegram.messenger";
    a = Arrays.asList(tmp14_5);
    Object localObject = new java/lang/Object;
    localObject.<init>();
    b = localObject;
  }
  
  public DetailsFragment()
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>(2);
    c = ((ArrayList)localObject);
    aB = false;
    aD = false;
    aE = false;
    aF = false;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    aX = ((HashMap)localObject);
    aZ = 10;
    localObject = new android/os/Handler;
    ((Handler)localObject).<init>();
    ba = ((Handler)localObject);
    localObject = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$jYyyXoZr4jTqB4WdqCfV4NJaXMQ;
    ((-..Lambda.DetailsFragment.jYyyXoZr4jTqB4WdqCfV4NJaXMQ)localObject).<init>(this);
    bb = ((Runnable)localObject);
    localObject = new com/truecaller/ui/details/DetailsFragment$1;
    ((DetailsFragment.1)localObject).<init>(this);
    be = ((j.b)localObject);
    bf = null;
  }
  
  private void A()
  {
    Object localObject = Z.aF().y();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool)
    {
      localObject = aa;
      bool = ((Contact)localObject).N();
      if (bool)
      {
        M.setVisibility(0);
        M.setOnClickListener(this);
        return;
      }
    }
    M.setVisibility(8);
  }
  
  private int a(View paramView)
  {
    ViewParent localViewParent = paramView.getParent();
    View localView = paramView.getRootView();
    if (localViewParent == localView) {
      return paramView.getTop();
    }
    int j = paramView.getTop();
    paramView = (View)paramView.getParent();
    int k = a(paramView);
    return j + k;
  }
  
  public static Intent a(Context paramContext, Contact paramContact, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2)
  {
    SingleActivity.FragmentSingle localFragmentSingle = SingleActivity.FragmentSingle.CALLER;
    paramContext = SingleActivity.c(paramContext, localFragmentSingle);
    paramContext.putExtra("ARG_CONTACT", paramContact);
    int j = paramSourceType.ordinal();
    paramContext.putExtra("ARG_SOURCE_TYPE", j);
    paramContext.putExtra("SHOULD_SAVE", paramBoolean1);
    paramContext.putExtra("SHOULD_FETCH_MORE_IF_NEEDED", paramBoolean2);
    return paramContext;
  }
  
  public static Intent a(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2, int paramInt)
  {
    SingleActivity.FragmentSingle localFragmentSingle = SingleActivity.FragmentSingle.CALLER;
    paramContext = SingleActivity.c(paramContext, localFragmentSingle);
    paramContext.putExtra("ARG_TC_ID", paramString1);
    paramContext.putExtra("NAME", paramString2);
    paramContext.putExtra("NORMALIZED_NUMBER", paramString3);
    paramContext.putExtra("RAW_NUMBER", paramString4);
    paramContext.putExtra("COUNTRY_CODE", paramString5);
    int j = paramSourceType.ordinal();
    paramContext.putExtra("ARG_SOURCE_TYPE", j);
    paramContext.putExtra("SHOULD_SAVE", paramBoolean1);
    paramContext.putExtra("SHOULD_FETCH_MORE_IF_NEEDED", paramBoolean2);
    paramContext.putExtra("SEARCH_TYPE", paramInt);
    return paramContext;
  }
  
  public static Bundle a(String paramString, DetailsFragment.SourceType paramSourceType)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("ARG_TC_ID", null);
    localBundle.putString("NORMALIZED_NUMBER", paramString);
    int j = paramSourceType.ordinal();
    localBundle.putInt("ARG_SOURCE_TYPE", j);
    j = 1;
    localBundle.putBoolean("SHOULD_SAVE", j);
    localBundle.putBoolean("SHOULD_FETCH_MORE_IF_NEEDED", j);
    return localBundle;
  }
  
  private Contact a(Contact paramContact)
  {
    DetailsFragment localDetailsFragment = this;
    boolean bool1 = isAdded();
    if ((bool1) && (paramContact != null))
    {
      bool1 = false;
      aw = false;
      Object localObject1 = paramContact.A().iterator();
      int j = 0;
      String[] arrayOfString1 = null;
      int k = 0;
      String[] arrayOfString2 = null;
      int i1 = 0;
      Object localObject2 = null;
      int i2 = 0;
      int i3 = 0;
      String[] arrayOfString3 = null;
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject1).hasNext();
        boolean bool3 = true;
        if (!bool2) {
          break;
        }
        Object localObject3 = (Number)((Iterator)localObject1).next();
        boolean bool4 = false;
        Object localObject4 = null;
        ((Number)localObject3).setTag(null);
        Object localObject5 = ((Number)localObject3).a();
        boolean bool5 = TextUtils.isEmpty((CharSequence)localObject5);
        if (!bool5)
        {
          Object localObject6 = (Collection)aX.get(localObject5);
          if (localObject6 == null)
          {
            localObject6 = ag;
            localObject7 = ((Number)localObject3).d();
            localObject6 = ((FilterManager)localObject6).a((String)localObject7, (String)localObject5, bool3);
            localObject7 = aX;
            ((HashMap)localObject7).put(localObject5, localObject6);
          }
          localObject6 = ((Collection)localObject6).iterator();
          Object localObject8;
          do
          {
            for (;;)
            {
              boolean bool6 = ((Iterator)localObject6).hasNext();
              if (!bool6) {
                break label625;
              }
              localObject7 = (com.truecaller.filters.g)((Iterator)localObject6).next();
              localObject8 = h;
              Object localObject9 = FilterManager.FilterAction.FILTER_BLACKLISTED;
              if (localObject8 == localObject9)
              {
                localObject8 = j;
                localObject9 = FilterManager.ActionSource.CUSTOM_BLACKLIST;
                if (localObject8 == localObject9)
                {
                  localObject7 = n;
                  localObject8 = TruecallerContract.Filters.WildCardType.NONE;
                  if (localObject7 != localObject8)
                  {
                    arrayOfString2 = new String[bool3];
                    localObject7 = new java/lang/StringBuilder;
                    ((StringBuilder)localObject7).<init>();
                    ((StringBuilder)localObject7).append((String)localObject5);
                    localObject8 = " found in wildcard user filters";
                    ((StringBuilder)localObject7).append((String)localObject8);
                    localObject7 = ((StringBuilder)localObject7).toString();
                    arrayOfString2[0] = localObject7;
                    k = 1;
                  }
                  else
                  {
                    arrayOfString1 = new String[bool3];
                    localObject7 = new java/lang/StringBuilder;
                    ((StringBuilder)localObject7).<init>();
                    ((StringBuilder)localObject7).append((String)localObject5);
                    localObject8 = " found in user filters";
                    ((StringBuilder)localObject7).append((String)localObject8);
                    localObject7 = ((StringBuilder)localObject7).toString();
                    arrayOfString1[0] = localObject7;
                    j = 1;
                  }
                  localObject7 = b;
                  ((Number)localObject3).setTag(localObject7);
                  continue;
                }
              }
              localObject8 = j;
              localObject9 = FilterManager.ActionSource.TOP_SPAMMER;
              if (localObject8 != localObject9) {
                break;
              }
              localObject2 = new String[bool3];
              localObject8 = new java/lang/StringBuilder;
              ((StringBuilder)localObject8).<init>();
              ((StringBuilder)localObject8).append((String)localObject5);
              localObject9 = " found in top spammers";
              ((StringBuilder)localObject8).append((String)localObject9);
              localObject8 = ((StringBuilder)localObject8).toString();
              localObject2[0] = localObject8;
              localObject2 = h;
              localObject8 = FilterManager.FilterAction.FILTER_BLACKLISTED;
              if (localObject2 == localObject8)
              {
                localObject2 = b;
                ((Number)localObject3).setTag(localObject2);
              }
              i1 = ((Number)localObject3).h();
              int i4 = m;
              if (i1 < i4)
              {
                i1 = m;
                ((Number)localObject3).a(i1);
                i1 = 1;
              }
              else
              {
                i1 = 1;
              }
            }
            localObject7 = h;
            localObject8 = FilterManager.FilterAction.ALLOW_WHITELISTED;
          } while (localObject7 != localObject8);
          arrayOfString3 = new String[bool3];
          localObject6 = new java/lang/StringBuilder;
          ((StringBuilder)localObject6).<init>();
          ((StringBuilder)localObject6).append((String)localObject5);
          Object localObject7 = " is white-listed";
          ((StringBuilder)localObject6).append((String)localObject7);
          localObject6 = ((StringBuilder)localObject6).toString();
          arrayOfString3[0] = localObject6;
          i3 = 1;
          label625:
          if (i3 != 0)
          {
            ((Number)localObject3).setTag(null);
            i2 += 1;
            j = 0;
            arrayOfString1 = null;
            k = 0;
            arrayOfString2 = null;
            i1 = 0;
            localObject2 = null;
          }
          localObject3 = ((Number)localObject3).d();
          if (localObject3 == null) {
            localObject3 = localObject5;
          }
          bool4 = aw;
          if (!bool4)
          {
            localObject4 = Z.l();
            bool4 = com.truecaller.search.f.a((Context)localObject4, (String)localObject3);
            if (bool4)
            {
              localObject4 = new String[bool3];
              localObject5 = new java/lang/StringBuilder;
              ((StringBuilder)localObject5).<init>();
              ((StringBuilder)localObject5).append((String)localObject3);
              ((StringBuilder)localObject5).append(" exists in the device phonebook");
              localObject3 = ((StringBuilder)localObject5).toString();
              localObject4[0] = localObject3;
              aw = bool3;
            }
          }
        }
      }
      localObject1 = Z.R();
      boolean bool7 = ((p)localObject1).g();
      if ((j == 0) && (k == 0) && ((i1 == 0) || (!bool7)))
      {
        bool7 = false;
        localObject1 = null;
      }
      else
      {
        bool7 = true;
      }
      as = bool7;
      if (i2 > 0)
      {
        localObject1 = paramContact.A();
        int i5 = ((List)localObject1).size();
        if (i2 == i5) {
          bool1 = true;
        }
      }
      at = bool1;
      av = i1;
      bool1 = paramContact.Y();
      ax = bool1;
      return paramContact;
    }
    return paramContact;
  }
  
  private CallerButtonBase a(String paramString, int paramInt)
  {
    CallerButtonBase localCallerButtonBase = new com/truecaller/ui/components/CallerButtonBase;
    AppCompatActivity localAppCompatActivity = d;
    localCallerButtonBase.<init>(localAppCompatActivity);
    localCallerButtonBase.setShowFullDivider(false);
    localCallerButtonBase.setHeadingText(paramString);
    localCallerButtonBase.setDetailsTextStyle(2131952074);
    localCallerButtonBase.setLeftImage(paramInt);
    localCallerButtonBase.setDetailsMaxLines(1);
    localCallerButtonBase.setHeaderGravity(8388627);
    int j = getResources().getDimensionPixelSize(2131165270);
    localCallerButtonBase.setLeftImageWidth(j);
    j = getResources().getDimensionPixelSize(2131165489);
    localCallerButtonBase.setHeaderDrawablePadding(j);
    return localCallerButtonBase;
  }
  
  private CallerButtonBase a(String paramString, int paramInt, PremiumPresenterView.LaunchContext paramLaunchContext)
  {
    Object localObject1 = getString(2131886332);
    paramString = a(paramString, (String)localObject1, paramInt);
    Object localObject2 = paramString.getDetailsTextView();
    localObject1 = Y;
    ((TextView)localObject2).setCompoundDrawablesWithIntrinsicBounds((Drawable)localObject1, null, null, null);
    localObject2 = paramString.getDetailsTextView();
    int j = getResources().getDimensionPixelSize(2131165495);
    ((TextView)localObject2).setCompoundDrawablePadding(j);
    localObject2 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$bbjSjsWK8oXTH6ujinEayImDZYk;
    ((-..Lambda.DetailsFragment.bbjSjsWK8oXTH6ujinEayImDZYk)localObject2).<init>(this, paramLaunchContext);
    paramString.setOnClickListener((View.OnClickListener)localObject2);
    return paramString;
  }
  
  private CallerButtonBase a(String paramString1, String paramString2, int paramInt)
  {
    CallerButtonBase localCallerButtonBase = new com/truecaller/ui/components/CallerButtonBase;
    AppCompatActivity localAppCompatActivity = d;
    localCallerButtonBase.<init>(localAppCompatActivity);
    localCallerButtonBase.setShowFullDivider(false);
    localCallerButtonBase.setHeadingText(paramString1);
    localCallerButtonBase.setDetailsText(paramString2);
    localCallerButtonBase.setDetailsTextStyle(2131952074);
    localCallerButtonBase.setLeftImage(paramInt);
    int j = aY;
    localCallerButtonBase.setImageTint(j);
    return localCallerButtonBase;
  }
  
  private void a(long paramLong, String paramString, int paramInt)
  {
    boolean bool1 = s();
    if (!bool1) {
      return;
    }
    com.truecaller.ui.dialogs.g localg = ao;
    if (localg != null)
    {
      localg.dismiss();
      ao = null;
    }
    long l1 = 1L;
    int j = 2131887217;
    int k = 2131886326;
    boolean bool2 = true;
    boolean bool3 = paramLong < l1;
    int i1;
    int i2;
    if (!bool3)
    {
      i1 = 2131886328;
      i2 = 2;
      localObject1 = new Object[i2];
      localObject1[0] = paramString;
      paramString = new Object[bool2];
      String str = String.valueOf(paramInt);
      paramString[0] = str;
      paramString = getString(k, paramString);
      localObject1[bool2] = paramString;
      localObject2 = getString(i1, (Object[])localObject1);
      localObject1 = new android/support/v7/app/AlertDialog$Builder;
      paramString = d;
      ((AlertDialog.Builder)localObject1).<init>(paramString);
      int i3 = 2131886327;
      localObject1 = ((AlertDialog.Builder)localObject1).setTitle(i3);
      localObject2 = ((AlertDialog.Builder)localObject1).setMessage((CharSequence)localObject2).setPositiveButton(j, null);
      ((AlertDialog.Builder)localObject2).show();
    }
    else
    {
      long l2 = 2;
      boolean bool4 = paramLong < l2;
      if (!bool4)
      {
        i1 = 2131886322;
        b(i1);
      }
      else
      {
        l2 = 3;
        bool4 = paramLong < l2;
        if (!bool4)
        {
          localObject2 = new android/support/v7/app/AlertDialog$Builder;
          localObject1 = d;
          ((AlertDialog.Builder)localObject2).<init>((Context)localObject1);
          i2 = 2131886325;
          localObject2 = ((AlertDialog.Builder)localObject2).setTitle(i2);
          localObject1 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$w9LmOPqIu5-LcgmQEd4q2WWk0FA;
          ((-..Lambda.DetailsFragment.w9LmOPqIu5-LcgmQEd4q2WWk0FA)localObject1).<init>(this);
          localObject2 = ((AlertDialog.Builder)localObject2).setPositiveButton(j, (DialogInterface.OnClickListener)localObject1).setCancelable(bool2);
          ((AlertDialog.Builder)localObject2).show();
        }
        else
        {
          i1 = 2131886324;
          b(i1);
        }
      }
    }
    Object localObject2 = l;
    Object localObject1 = new Object[bool2];
    paramString = String.valueOf(paramInt);
    localObject1[0] = paramString;
    localObject1 = getString(k, (Object[])localObject1);
    at.b((TextView)localObject2, (CharSequence)localObject1);
  }
  
  public static void a(Activity paramActivity, String paramString1, String paramString2, String paramString3, String paramString4, DetailsFragment.SourceType paramSourceType, Long paramLong)
  {
    long l1 = paramLong.longValue();
    Object localObject = paramActivity;
    localObject = a(paramActivity, paramString1, paramString2, paramString3, paramString4, null, paramSourceType, false, true, 20);
    ((Intent)localObject).putExtra("CONVERSATION_ID", l1);
    paramActivity.startActivityForResult((Intent)localObject, 2);
  }
  
  public static void a(Context paramContext, Contact paramContact, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    paramContact = a(paramContext, paramContact, paramSourceType, paramBoolean1, paramBoolean2);
    paramContact.putExtra("SHOULD_FORCE_SEARCH", paramBoolean3);
    paramContext.startActivity(paramContact);
  }
  
  public static void a(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, DetailsFragment.SourceType paramSourceType, boolean paramBoolean, int paramInt)
  {
    Object localObject = paramContext;
    localObject = a(paramContext, paramString1, paramString2, paramString3, paramString4, paramString5, paramSourceType, false, paramBoolean, paramInt);
    paramContext.startActivity((Intent)localObject);
  }
  
  public static void a(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2)
  {
    Object localObject = paramContext;
    localObject = a(paramContext, paramString1, paramString2, paramString3, paramString4, paramString5, paramSourceType, paramBoolean1, paramBoolean2, 10);
    paramContext.startActivity((Intent)localObject);
  }
  
  private static void a(View paramView, int paramInt1, int paramInt2)
  {
    int[] arrayOfInt = new int[2];
    arrayOfInt[0] = paramInt1;
    arrayOfInt[1] = paramInt2;
    ValueAnimator localValueAnimator = ValueAnimator.ofInt(arrayOfInt);
    -..Lambda.DetailsFragment.xe5h4HyxRDdceq1sG2QC7WI9YoY localxe5h4HyxRDdceq1sG2QC7WI9YoY = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$xe5h4HyxRDdceq1sG2QC7WI9YoY;
    localxe5h4HyxRDdceq1sG2QC7WI9YoY.<init>(paramView);
    localValueAnimator.addUpdateListener(localxe5h4HyxRDdceq1sG2QC7WI9YoY);
    paramView = new android/view/animation/DecelerateInterpolator;
    paramView.<init>();
    localValueAnimator.setInterpolator(paramView);
    localValueAnimator.start();
  }
  
  private void a(ViewGroup paramViewGroup)
  {
    Object localObject1 = aa.g();
    if (localObject1 != null)
    {
      Object localObject2 = ((Address)localObject1).getDisplayableAddress();
      boolean bool1 = TextUtils.isEmpty((CharSequence)localObject2);
      if (!bool1)
      {
        localObject2 = aU;
        Contact localContact = aa;
        boolean bool2 = true;
        bool1 = ((com.truecaller.premium.b.a)localObject2).a(localContact, bool2);
        int j = 2131234381;
        if (bool1)
        {
          int k = 2131886330;
          localObject1 = getString(k);
          localObject2 = PremiumPresenterView.LaunchContext.CONTACT_DETAILS_ADDRESS;
          localObject1 = a((String)localObject1, j, (PremiumPresenterView.LaunchContext)localObject2);
        }
        else
        {
          localObject1 = ((Address)localObject1).getDisplayableAddress();
          bool1 = false;
          localObject1 = a((String)localObject1, null, j);
          localObject2 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$Hn6LgIGZX6Ztot1idvtGrIHGRzY;
          ((-..Lambda.DetailsFragment.Hn6LgIGZX6Ztot1idvtGrIHGRzY)localObject2).<init>(this);
          ((CallerButtonBase)localObject1).setOnClickListener((View.OnClickListener)localObject2);
        }
        paramViewGroup.addView((View)localObject1);
        return;
      }
    }
  }
  
  private void a(ViewGroup paramViewGroup, String paramString)
  {
    Object localObject = I;
    if (localObject == null)
    {
      int j = 2131888549;
      localObject = getString(j);
      int k = 2131234461;
      localObject = a((String)localObject, k);
      I = ((CallerButtonBase)localObject);
      I.setTag(paramString);
      localObject = I;
      -..Lambda.DetailsFragment.KebEUsbktR3JKBgy6HDCm6oXVyk localKebEUsbktR3JKBgy6HDCm6oXVyk = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$KebEUsbktR3JKBgy6HDCm6oXVyk;
      localKebEUsbktR3JKBgy6HDCm6oXVyk.<init>(this);
      ((CallerButtonBase)localObject).setOnClickListener(localKebEUsbktR3JKBgy6HDCm6oXVyk);
      localObject = I.getRightImage();
      ((ImageView)localObject).setTag(paramString);
      paramString = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$KebEUsbktR3JKBgy6HDCm6oXVyk;
      paramString.<init>(this);
      ((ImageView)localObject).setOnClickListener(paramString);
    }
    paramString = new android/support/v7/widget/LinearLayoutCompat$LayoutParams;
    paramString.<init>(0, -1, 1.0F);
    localObject = I;
    paramViewGroup.addView((View)localObject, paramString);
  }
  
  private void a(ViewGroup paramViewGroup, String paramString1, String paramString2, int paramInt, Runnable paramRunnable, String paramString3)
  {
    boolean bool1 = TextUtils.isEmpty(paramString1);
    if (bool1) {
      return;
    }
    bool1 = am.b(paramString1);
    String str;
    if (bool1) {
      str = aa.t();
    } else {
      str = paramString1;
    }
    com.truecaller.premium.b.a locala = aU;
    Contact localContact = aa;
    Object localObject1 = a;
    com.truecaller.featuretoggles.e.a locala1 = y;
    Object localObject2 = com.truecaller.featuretoggles.e.a;
    int j = 76;
    localObject2 = localObject2[j];
    localObject1 = locala1.a((com.truecaller.featuretoggles.e)localObject1, (c.l.g)localObject2);
    boolean bool2 = ((com.truecaller.featuretoggles.b)localObject1).a();
    locala1 = null;
    boolean bool3 = true;
    if (bool2)
    {
      bool4 = locala.b(localContact, bool3);
      if (bool4)
      {
        bool4 = true;
        break label139;
      }
    }
    boolean bool4 = false;
    locala = null;
    label139:
    if (bool4)
    {
      int k = 2131886334;
      paramRunnable = new Object[bool3];
      paramRunnable[0] = paramString2;
      paramString1 = getString(k, paramRunnable);
      paramString2 = PremiumPresenterView.LaunchContext.CONTACT_DETAILS_SOCIAL;
      paramString1 = a(paramString1, paramInt, paramString2);
    }
    else
    {
      paramString2 = a(str, paramString2, paramInt);
      -..Lambda.DetailsFragment.MuAi-3-iJkfp1RXuQFv_8iMiaWE localMuAi-3-iJkfp1RXuQFv_8iMiaWE = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$MuAi-3-iJkfp1RXuQFv_8iMiaWE;
      localMuAi-3-iJkfp1RXuQFv_8iMiaWE.<init>(this, paramRunnable, paramString1, paramString3);
      paramString2.setOnClickListener(localMuAi-3-iJkfp1RXuQFv_8iMiaWE);
      paramString1 = paramString2;
    }
    paramViewGroup.addView(paramString1);
  }
  
  private void a(CallRecording paramCallRecording)
  {
    Object localObject = Z.bg();
    CallRecordingOnBoardingLaunchContext localCallRecordingOnBoardingLaunchContext = CallRecordingOnBoardingLaunchContext.UNKNOWN;
    boolean bool = ((CallRecordingManager)localObject).a(localCallRecordingOnBoardingLaunchContext);
    if (bool) {
      return;
    }
    localObject = Z.bQ();
    paramCallRecording = c;
    paramCallRecording = ((com.truecaller.calling.recorder.u)localObject).a(paramCallRecording);
    ((com.truecaller.calling.recorder.u)localObject).a(paramCallRecording);
  }
  
  private void a(Contact paramContact, boolean paramBoolean)
  {
    DetailsFragment localDetailsFragment = this;
    boolean bool1 = b(paramContact, paramBoolean);
    Object localObject1 = bh.a(paramContact, bool1);
    bg = ((e)localObject1);
    Object localObject2 = K;
    Object localObject3 = b;
    ((AppBarLayout)localObject2).setBackground((Drawable)localObject3);
    localObject2 = g;
    localObject3 = e.c;
    ((DetailsActionBar)localObject2).setBackground((Drawable)localObject3);
    localObject2 = h;
    localObject3 = e.d;
    ((View)localObject2).setBackground((Drawable)localObject3);
    localObject2 = F;
    int j = d.a;
    ((TextView)localObject2).setTextColor(j);
    n();
    localObject2 = f;
    localObject3 = d;
    ((DetailsHeaderView)localObject2).setAppearance((i)localObject3);
    int i1 = Build.VERSION.SDK_INT;
    j = 21;
    if (i1 >= j)
    {
      localObject2 = requireActivity().getWindow();
      j = a;
      ((Window)localObject2).setStatusBarColor(j);
    }
    String str = paramContact.s();
    localObject2 = F;
    ((TextView)localObject2).setText(str);
    boolean bool3 = paramContact.ac();
    int i3 = 1;
    boolean bool5 = false;
    if (bool3)
    {
      bool3 = k;
      if (bool3)
      {
        bool3 = true;
        break label241;
      }
    }
    bool3 = false;
    localObject2 = null;
    label241:
    aD = bool3;
    localObject2 = f;
    boolean bool2 = aw;
    boolean bool6 = aD;
    ((DetailsHeaderView)localObject2).b(paramContact, bool2, bool1, bool6);
    bool3 = ar;
    if (bool3)
    {
      localObject2 = aa;
      if (localObject2 != null)
      {
        bool3 = true;
        break label311;
      }
    }
    bool3 = false;
    localObject2 = null;
    label311:
    if (bool3)
    {
      localObject2 = o();
      j((View)localObject2);
      e((View)localObject2);
    }
    localObject2 = Z.ai();
    bool3 = ((com.truecaller.common.f.c)localObject2).d();
    if (!bool3)
    {
      bool3 = paramContact.R();
      if (bool3)
      {
        X.setVisibility(0);
        localObject1 = e;
        int i2 = ((View)localObject1).getPaddingLeft();
        localObject3 = e;
        k = ((View)localObject3).getPaddingRight();
        localObject4 = e;
        i4 = ((View)localObject4).getPaddingBottom();
        ((View)localObject1).setPadding(i2, 0, k, i4);
        break label570;
      }
    }
    X.setVisibility(8);
    localObject2 = e;
    int k = ((View)localObject2).getPaddingLeft();
    Object localObject4 = getResources();
    int i4 = ((Resources)localObject4).getDimensionPixelSize(2131165606);
    View localView1 = e;
    int i5 = localView1.getPaddingRight();
    View localView2 = e;
    int i7 = localView2.getPaddingBottom();
    ((View)localObject2).setPadding(k, i4, i5, i7);
    localObject2 = g;
    localObject3 = e;
    boolean bool7 = as;
    boolean bool8 = at;
    boolean bool9 = k;
    boolean bool10 = au;
    localObject1 = localObject2;
    localObject2 = paramContact;
    ((DetailsActionBar)localObject1).a(paramContact, (h)localObject3, bool7, bool8, bool9, paramBoolean, bool10);
    label570:
    localObject1 = (TextView)J.findViewById(2131364098);
    localObject3 = new Object[i3];
    localObject3[0] = str;
    localObject2 = getString(2131888636, (Object[])localObject3);
    ((TextView)localObject1).setText((CharSequence)localObject2);
    localObject1 = getContext();
    boolean bool4 = com.truecaller.data.access.c.b(paramContact);
    if (bool4) {
      localObject2 = paramContact.getId();
    } else {
      localObject2 = paramContact.k();
    }
    if ((localObject1 != null) && (localObject2 != null))
    {
      long l1 = ((Long)localObject2).longValue();
      bool7 = paramContact.Z();
      int i6 = aZ;
      ProfileViewService.a((Context)localObject1, l1, bool7, i6);
      localObject1 = W;
      bool4 = paramContact.a(32);
      if ((bool4) && (!bool1)) {
        bool5 = true;
      }
      at.a((View)localObject1, bool5);
      return;
    }
  }
  
  private void a(Number paramNumber, int paramInt)
  {
    paramNumber = paramNumber.e();
    if (paramNumber != null)
    {
      boolean bool = com.truecaller.common.h.ab.a(paramNumber);
      if (!bool)
      {
        a("call", "button");
        b.a.a locala = new com/truecaller/calling/initiate_call/b$a$a;
        locala.<init>(paramNumber, "detailView");
        paramNumber = aa.s();
        paramNumber = locala.a(paramNumber);
        Object localObject = Integer.valueOf(paramInt);
        paramNumber = paramNumber.a((Integer)localObject);
        localObject = aS;
        paramNumber = paramNumber.a();
        ((com.truecaller.calling.initiate_call.b)localObject).a(paramNumber);
        return;
      }
    }
  }
  
  private void a(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool) {
      paramString = aa.t();
    }
    com.truecaller.ui.dialogs.o localo = new com/truecaller/ui/dialogs/o;
    AppCompatActivity localAppCompatActivity = d;
    String str = (String)g().get(0);
    localo.<init>(localAppCompatActivity, paramString, str, null);
    localo.show();
  }
  
  private void a(String paramString, Contact paramContact)
  {
    AppCompatActivity localAppCompatActivity = d;
    if (localAppCompatActivity == null) {
      return;
    }
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(localAppCompatActivity, SwishInputActivity.class);
    localIntent.putExtra("payee_number", paramString);
    localIntent.putExtra("payee_contact", paramContact);
    startActivity(localIntent);
  }
  
  private void a(String paramString1, String paramString2)
  {
    Object localObject = new com/truecaller/analytics/e$a;
    ((com.truecaller.analytics.e.a)localObject).<init>("ViewAction");
    String str1 = "detailView";
    localObject = ((com.truecaller.analytics.e.a)localObject).a("Context", str1);
    String str2 = "Action";
    paramString1 = ((com.truecaller.analytics.e.a)localObject).a(str2, paramString1);
    if (paramString2 != null)
    {
      localObject = "SubAction";
      paramString1.a((String)localObject, paramString2);
    }
    paramString2 = Z.c();
    paramString1 = paramString1.a();
    paramString2.a(paramString1);
  }
  
  private void a(String paramString, List paramList)
  {
    boolean bool1 = am.c(paramString);
    if (bool1)
    {
      localObject1 = new com/truecaller/ads/j$a;
      localObject2 = "DETAILS";
      ((j.a)localObject1).<init>((String)localObject2);
      a = paramString;
      int j = aa.I();
      b = j;
      paramString = aa.t();
      e = paramString;
      paramString = ad;
      f = paramString;
      g = paramList;
      paramString = ((j.a)localObject1).a();
    }
    else
    {
      paramString = new com/truecaller/ads/j$a;
      paramList = "DETAILS";
      paramString.<init>(paramList);
      paramString = paramString.a();
    }
    paramList = aq.U();
    boolean bool2 = paramList.a();
    bool1 = true;
    Object localObject2 = null;
    Object localObject3;
    if (bool2)
    {
      paramString = com.truecaller.ads.k.a().a("/43067329/A*Detailed_view_2*Unified*GPS").a(paramString);
      paramList = new AdSize[2];
      localObject3 = AdSize.a;
      paramList[0] = localObject3;
      localObject2 = AdSize.c;
      paramList[bool1] = localObject2;
      paramString = paramString.a(paramList);
      int k = 3;
      paramString = paramString.a(k).d();
    }
    else
    {
      paramList = aW;
      localObject3 = "adsFeatureUnifiedAdsDetails";
      boolean bool3 = paramList.b((String)localObject3);
      if (bool3)
      {
        paramString = com.truecaller.ads.k.a().a("/43067329/A*Detailed_view*Unified*GPS").a(paramString);
        paramList = new AdSize[bool1];
        localObject3 = AdSize.a;
        paramList[0] = localObject3;
        paramString = paramString.a(paramList);
        paramList = new CustomTemplate[bool1];
        localObject1 = CustomTemplate.NATIVE_CONTENT_DUAL_TRACKER;
        paramList[0] = localObject1;
        paramString = paramString.a(paramList);
      }
      else
      {
        paramList = com.truecaller.ads.k.a();
        localObject3 = "/43067329/A*Detailed_view*Native*GPS";
        paramString = paramList.a((String)localObject3).a(paramString);
        paramList = new CustomTemplate[bool1];
        localObject1 = CustomTemplate.NATIVE_CONTENT_DUAL_TRACKER;
        paramList[0] = localObject1;
        paramString = paramString.a(paramList);
      }
    }
    paramString.c("detailView");
    paramString = paramString.e();
    paramList = new com/truecaller/ui/details/DetailsFragment$6;
    Object localObject1 = aV;
    paramList.<init>(this, (com.truecaller.ads.provider.f)localObject1, paramString);
    bf = paramList;
  }
  
  private void a(List paramList)
  {
    Object localObject1 = Calendar.getInstance();
    Object localObject2 = paramList.iterator();
    boolean bool1 = ((Iterator)localObject2).hasNext();
    int j = 7;
    boolean bool2 = true;
    Object localObject3;
    Object localObject4;
    Object localObject5;
    if (bool1)
    {
      localObject3 = (OpenHours)((Iterator)localObject2).next();
      localObject4 = ((OpenHours)localObject3).getWeekday().iterator();
      com.truecaller.profile.business.openHours.b localb2;
      boolean bool4;
      do
      {
        int k;
        com.truecaller.profile.business.openHours.b localb1;
        int i2;
        do
        {
          do
          {
            boolean bool3 = ((Iterator)localObject4).hasNext();
            if (!bool3) {
              break;
            }
            localObject5 = (Integer)((Iterator)localObject4).next();
            k = ((Integer)localObject5).intValue();
            localb1 = com.truecaller.profile.business.openHours.a.a((OpenHours)localObject3);
            localb2 = com.truecaller.profile.business.openHours.a.b((OpenHours)localObject3);
          } while ((localb1 == null) || (localb2 == null));
          i2 = ((Calendar)localObject1).get(j);
        } while (k != i2);
        localObject5 = ((Calendar)localObject1).getTime();
        bool4 = a((Date)localObject5, localb1, localb2);
      } while (!bool4);
      localObject1 = new Object[bool2];
      localObject2 = localb2.b();
      localObject1[0] = localObject2;
      paramList = getString(2131886227, (Object[])localObject1);
      a(bool2, paramList);
      return;
    }
    paramList = paramList.iterator();
    boolean bool5 = paramList.hasNext();
    if (bool5)
    {
      localObject2 = (OpenHours)paramList.next();
      localObject3 = ((OpenHours)localObject2).getWeekday().iterator();
      int i3;
      do
      {
        do
        {
          int i1;
          do
          {
            boolean bool6 = ((Iterator)localObject3).hasNext();
            if (!bool6) {
              break;
            }
            localObject4 = (Integer)((Iterator)localObject3).next();
            i3 = ((Integer)localObject4).intValue();
            i1 = ((Calendar)localObject1).get(j);
          } while (i3 != i1);
          localObject4 = com.truecaller.profile.business.openHours.b.a((Calendar)localObject1);
          localObject5 = com.truecaller.profile.business.openHours.a.a((OpenHours)localObject2);
        } while (localObject5 == null);
        i3 = ((com.truecaller.profile.business.openHours.b)localObject5).a((com.truecaller.profile.business.openHours.b)localObject4);
      } while (i3 <= 0);
      localObject1 = new Object[bool2];
      localObject2 = ((com.truecaller.profile.business.openHours.b)localObject5).b();
      localObject1[0] = localObject2;
      paramList = getString(2131886263, (Object[])localObject1);
      a(false, paramList);
      return;
    }
    a(false, null);
  }
  
  private void a(boolean paramBoolean, String paramString)
  {
    Object localObject1 = R;
    int j = 2131363494;
    localObject1 = (TextView)((View)localObject1).findViewById(j);
    Object localObject2 = R;
    int k = 2131364593;
    localObject2 = (TextView)((View)localObject2).findViewById(k);
    View localView = R;
    int i1 = 2131363833;
    localView = localView.findViewById(i1);
    Resources localResources;
    if (paramBoolean)
    {
      ((TextView)localObject1).setText(2131886258);
      localResources = getResources();
      i1 = 2131100075;
      paramBoolean = localResources.getColor(i1);
      ((TextView)localObject1).setTextColor(paramBoolean);
    }
    else
    {
      ((TextView)localObject1).setText(2131886225);
      localResources = getResources();
      i1 = 2131100475;
      paramBoolean = localResources.getColor(i1);
      ((TextView)localObject1).setTextColor(paramBoolean);
    }
    if (paramString != null)
    {
      ((TextView)localObject2).setText(paramString);
      ((TextView)localObject2).setVisibility(0);
      localView.setVisibility(0);
      return;
    }
    paramBoolean = true;
    ((TextView)localObject2).setVisibility(paramBoolean);
    localView.setVisibility(paramBoolean);
  }
  
  private static boolean a(Date paramDate, com.truecaller.profile.business.openHours.b paramb1, com.truecaller.profile.business.openHours.b paramb2)
  {
    boolean bool1 = false;
    try
    {
      Object localObject1 = new java/text/SimpleDateFormat;
      Object localObject2 = "HH:mm";
      Object localObject3 = Locale.US;
      ((SimpleDateFormat)localObject1).<init>((String)localObject2, (Locale)localObject3);
      paramb1 = paramb1.a();
      paramb1 = ((SimpleDateFormat)localObject1).parse(paramb1);
      localObject2 = Calendar.getInstance();
      ((Calendar)localObject2).setTime(paramb1);
      paramDate = ((SimpleDateFormat)localObject1).format(paramDate);
      paramDate = ((SimpleDateFormat)localObject1).parse(paramDate);
      localObject3 = Calendar.getInstance();
      ((Calendar)localObject3).setTime(paramDate);
      paramb2 = paramb2.a();
      paramb2 = ((SimpleDateFormat)localObject1).parse(paramb2);
      localObject1 = Calendar.getInstance();
      ((Calendar)localObject1).setTime(paramb2);
      boolean bool2 = paramDate.before(paramb2);
      int j = 1;
      int k = 5;
      if (bool2)
      {
        ((Calendar)localObject3).add(k, j);
        paramDate = ((Calendar)localObject3).getTime();
      }
      boolean bool3 = paramb1.before(paramb2);
      if (bool3)
      {
        ((Calendar)localObject2).add(k, j);
        paramb1 = ((Calendar)localObject2).getTime();
      }
      boolean bool4 = paramDate.after(paramb1);
      if (bool4)
      {
        bool4 = paramDate.after(paramb2);
        if (bool4)
        {
          ((Calendar)localObject1).add(k, j);
          paramb2 = ((Calendar)localObject1).getTime();
        }
        bool1 = paramDate.before(paramb2);
      }
    }
    catch (ParseException paramDate)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramDate);
    }
    return bool1;
  }
  
  private List b(List paramList)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    int j = 1;
    int k = 1;
    Object localObject1;
    Object localObject2;
    Object localObject3;
    for (;;)
    {
      int i1 = 7;
      if (k > i1) {
        break;
      }
      localObject1 = new com/truecaller/ui/details/b;
      localObject2 = c(k);
      localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>();
      ((b)localObject1).<init>((String)localObject2, (List)localObject3);
      localArrayList.add(localObject1);
      k += 1;
    }
    paramList = paramList.iterator();
    boolean bool1 = paramList.hasNext();
    if (bool1)
    {
      OpenHours localOpenHours = (OpenHours)paramList.next();
      localObject1 = localOpenHours.getWeekday().iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        localObject2 = (Integer)((Iterator)localObject1).next();
        int i2 = ((Integer)localObject2).intValue();
        localObject3 = com.truecaller.profile.business.openHours.a.a(localOpenHours);
        com.truecaller.profile.business.openHours.b localb = com.truecaller.profile.business.openHours.a.b(localOpenHours);
        if ((localObject3 != null) && (localb != null))
        {
          int i3 = 2131886260;
          int i4 = 2;
          Object[] arrayOfObject = new Object[i4];
          localObject3 = ((com.truecaller.profile.business.openHours.b)localObject3).b();
          arrayOfObject[0] = localObject3;
          localObject3 = localb.b();
          arrayOfObject[j] = localObject3;
          localObject3 = getString(i3, arrayOfObject);
          i2 -= j;
          localObject2 = getb;
          ((List)localObject2).add(localObject3);
        }
      }
    }
    return localArrayList;
  }
  
  private void b()
  {
    boolean bool = aJ;
    int j = 0;
    if (bool)
    {
      localReferralManager = aI;
      if (localReferralManager != null)
      {
        localObject = aa;
        bool = localReferralManager.b((Contact)localObject);
        if (bool)
        {
          bool = true;
          break label48;
        }
      }
    }
    bool = false;
    ReferralManager localReferralManager = null;
    label48:
    Object localObject = J;
    if (!bool) {
      j = 8;
    }
    ((View)localObject).setVisibility(j);
  }
  
  public static void b(Context paramContext, Contact paramContact, DetailsFragment.SourceType paramSourceType, boolean paramBoolean1, boolean paramBoolean2)
  {
    paramContact = a(paramContext, paramContact, paramSourceType, paramBoolean1, paramBoolean2);
    paramContext.startActivity(paramContact);
  }
  
  private void b(View paramView)
  {
    Object localObject = c;
    int j = ((ArrayList)localObject).size();
    int k = 1;
    if (j == k)
    {
      paramView = (String)c.get(0);
      g(paramView);
      return;
    }
    localObject = c;
    j = ((ArrayList)localObject).size();
    if (j > k)
    {
      localObject = am;
      if (localObject == null)
      {
        localObject = new android/support/v7/app/AlertDialog$Builder;
        paramView = paramView.getContext();
        ((AlertDialog.Builder)localObject).<init>(paramView);
        int i1 = 2131888530;
        paramView = ((AlertDialog.Builder)localObject).setTitle(i1);
        localObject = new com/truecaller/ui/details/DetailsFragment$l;
        ((DetailsFragment.l)localObject).<init>(this);
        -..Lambda.DetailsFragment.DwtWwjN8j17s6N9GIe6lZU0rZN4 localDwtWwjN8j17s6N9GIe6lZU0rZN4 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$DwtWwjN8j17s6N9GIe6lZU0rZN4;
        localDwtWwjN8j17s6N9GIe6lZU0rZN4.<init>(this);
        paramView = paramView.setAdapter((ListAdapter)localObject, localDwtWwjN8j17s6N9GIe6lZU0rZN4).create();
        am = paramView;
      }
      am.show();
      return;
    }
    g(null);
  }
  
  private void b(String paramString)
  {
    Object localObject1 = d;
    if (localObject1 == null) {
      return;
    }
    localObject1 = Z.c();
    Object localObject2 = new com/truecaller/analytics/e$a;
    ((com.truecaller.analytics.e.a)localObject2).<init>("Swish");
    String str1 = "detailView";
    localObject2 = ((com.truecaller.analytics.e.a)localObject2).a("Context", str1);
    String str2 = "Status";
    localObject2 = ((com.truecaller.analytics.e.a)localObject2).a(str2, paramString).a();
    ((com.truecaller.analytics.b)localObject1).a((com.truecaller.analytics.e)localObject2);
    localObject1 = "Clicked";
    boolean bool = paramString.equals(localObject1);
    if (bool)
    {
      paramString = new java/util/HashMap;
      paramString.<init>();
      paramString.put("Context", "detailView");
      localObject1 = ao.b();
      localObject2 = "Swish_Tapped";
      paramString = ((ao.a)localObject1).a((CharSequence)localObject2).a(paramString);
      localObject1 = com.truecaller.swish.c.a;
      localObject1 = com.truecaller.swish.c.b();
      paramString = paramString.b((CharSequence)localObject1).a();
      localObject1 = (ae)ah.a();
      ((ae)localObject1).a(paramString);
    }
  }
  
  private void b(boolean paramBoolean)
  {
    aX.clear();
    String str1 = "unblockQuery";
    a(str1, null);
    List localList = g();
    String str2;
    if (paramBoolean) {
      str2 = "notspam";
    } else {
      str2 = "unblock";
    }
    String str3 = str2;
    com.truecaller.ui.f localf = af;
    paramBoolean = localList.isEmpty();
    if (paramBoolean) {
      str2 = "OTHER";
    } else {
      str2 = "PHONE_NUMBER";
    }
    f.a locala = d();
    boolean bool = aD;
    localf.a(localList, str2, "detailView", str3, locala, bool);
  }
  
  private boolean b(ViewGroup paramViewGroup)
  {
    Object localObject1 = aa;
    boolean bool1 = ((Contact)localObject1).R();
    if (!bool1)
    {
      localObject1 = aa;
      bool1 = ((Contact)localObject1).O();
      if (bool1)
      {
        localObject1 = Z.U();
        boolean bool2 = ((com.truecaller.multisim.h)localObject1).j();
        boolean bool3 = true;
        if (bool2)
        {
          bool2 = ((com.truecaller.multisim.h)localObject1).e();
          if (bool2)
          {
            localObject2 = Z.bv().e();
            localObject1 = ((com.truecaller.multisim.h)localObject1).b((String)localObject2);
            if (localObject1 == null)
            {
              bool1 = false;
              localObject1 = null;
            }
            else
            {
              j = a;
            }
            localObject3 = "-1";
            bool2 = ((String)localObject3).equals(localObject2) ^ bool3;
            break label141;
          }
        }
        int j = 0;
        localObject1 = null;
        bool2 = false;
        Object localObject2 = null;
        label141:
        Object localObject3 = aa.A().iterator();
        Object localObject4;
        for (;;)
        {
          boolean bool4 = ((Iterator)localObject3).hasNext();
          if (!bool4) {
            break;
          }
          localObject4 = (Number)((Iterator)localObject3).next();
          Object localObject5 = ((Number)localObject4).n();
          CallerButtonBase localCallerButtonBase = new com/truecaller/ui/components/CallerButtonBase;
          Object localObject6 = d;
          localCallerButtonBase.<init>((Context)localObject6);
          localCallerButtonBase.setShowFullDivider(false);
          localCallerButtonBase.setDetailsTextStyle(2131952074);
          int k = aY;
          localCallerButtonBase.setImageTint(k);
          localObject6 = ((Number)localObject4).getTag();
          Object localObject7 = b;
          int i2;
          if (localObject6 == localObject7)
          {
            k = 2131886352;
            localObject6 = getString(k);
            localCallerButtonBase.setDetailsText((CharSequence)localObject6);
          }
          else
          {
            i2 = 2;
            localObject7 = new CharSequence[i2];
            String str = aK.a((Number)localObject4);
            localObject7[0] = str;
            str = ((Number)localObject4).f();
            localObject7[bool3] = str;
            localObject6 = am.a(" - ", (CharSequence[])localObject7);
            localCallerButtonBase.setDetailsText((CharSequence)localObject6);
          }
          localObject6 = at.a((CharSequence)localObject5);
          localCallerButtonBase.setHeadingText((CharSequence)localObject6);
          localCallerButtonBase.setRightImage(2131234560);
          boolean bool5 = Settings.f();
          if (bool5)
          {
            localObject6 = ap;
            localObject7 = ((Number)localObject4).a();
            localObject6 = ((com.truecaller.search.local.model.g)localObject6).b((String)localObject7);
            localObject7 = d;
            localCallerButtonBase.a((Context)localObject7, (com.truecaller.presence.a)localObject6);
          }
          int i1;
          if (bool2)
          {
            i1 = 2131233935;
            i2 = 2131233936;
            int i3;
            if (j == 0) {
              i3 = 2131233935;
            } else {
              i3 = 2131233936;
            }
            localCallerButtonBase.setLeftImage(i3);
            if (j == 0) {
              i1 = 2131233936;
            }
            localCallerButtonBase.setRightImageSecondary(i1);
            localObject6 = localCallerButtonBase.getRightImageSecondary();
            localObject7 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$gE3ql6L6P6Raxjb_5zVSIhYitnU;
            ((-..Lambda.DetailsFragment.gE3ql6L6P6Raxjb_5zVSIhYitnU)localObject7).<init>(this, (Number)localObject4, j);
            ((ImageView)localObject6).setOnClickListener((View.OnClickListener)localObject7);
            localObject6 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$XxmQ8S5xODCjRu-BOUwC1Ch7chU;
            ((-..Lambda.DetailsFragment.XxmQ8S5xODCjRu-BOUwC1Ch7chU)localObject6).<init>(this, (Number)localObject4, j);
            localCallerButtonBase.setOnClickListener((View.OnClickListener)localObject6);
          }
          else
          {
            i1 = 2131233928;
            localCallerButtonBase.setLeftImage(i1);
            localObject6 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$p9rMgv-X605BOjstokR-BCDcvNM;
            ((-..Lambda.DetailsFragment.p9rMgv-X605BOjstokR-BCDcvNM)localObject6).<init>(this, (Number)localObject4);
            localCallerButtonBase.setOnClickListener((View.OnClickListener)localObject6);
          }
          localObject6 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$kcSj7lEt3mEoUbXFxzIx3AWgZ9U;
          ((-..Lambda.DetailsFragment.kcSj7lEt3mEoUbXFxzIx3AWgZ9U)localObject6).<init>(this, (String)localObject5);
          localCallerButtonBase.setOnLongClickListener((View.OnLongClickListener)localObject6);
          localObject5 = localCallerButtonBase.getRightImage();
          localObject6 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$9oofOa7qiNLysi4m-VQjZwrTj7g;
          ((-..Lambda.DetailsFragment.9oofOa7qiNLysi4m-VQjZwrTj7g)localObject6).<init>(this, (Number)localObject4);
          ((ImageView)localObject5).setOnClickListener((View.OnClickListener)localObject6);
          paramViewGroup.addView(localCallerButtonBase);
        }
        localObject1 = aa.getTcId();
        if (localObject1 != null)
        {
          localObject1 = ((com.truecaller.callhistory.a)aM.a()).d((String)localObject1);
          localObject2 = aL;
          localObject3 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$HWTq-HNUaDvFBxgXapYGyblf0iQ;
          ((-..Lambda.DetailsFragment.HWTq-HNUaDvFBxgXapYGyblf0iQ)localObject3).<init>(this, paramViewGroup);
          ((com.truecaller.androidactors.w)localObject1).a((com.truecaller.androidactors.i)localObject2, (ac)localObject3);
        }
        j = 2131886384;
        localObject1 = getString(j);
        bool2 = false;
        int i4 = 2131233932;
        localObject1 = a((String)localObject1, null, i4);
        localObject2 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$gfl7OZ1nLdK9lWSTTFtWqlI1BTQ;
        ((-..Lambda.DetailsFragment.gfl7OZ1nLdK9lWSTTFtWqlI1BTQ)localObject2).<init>(this);
        ((CallerButtonBase)localObject1).setOnClickListener((View.OnClickListener)localObject2);
        localObject2 = aa.getTcId();
        if (localObject2 != null)
        {
          localObject3 = aN;
          if (localObject3 == null)
          {
            localObject2 = ((com.truecaller.callhistory.a)aM.a()).d((String)localObject2);
            localObject3 = aL;
            localObject4 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$sB4ApGb3BGdEkVxP7KE-Xwr_RX8;
            ((-..Lambda.DetailsFragment.sB4ApGb3BGdEkVxP7KE-Xwr_RX8)localObject4).<init>(this, (CallerButtonBase)localObject1);
            ((com.truecaller.androidactors.w)localObject2).a((com.truecaller.androidactors.i)localObject3, (ac)localObject4);
          }
          else
          {
            ((CallerButtonBase)localObject1).setDetailsText((CharSequence)localObject3);
          }
        }
        localObject2 = ((CallerButtonBase)localObject1).getRightImage();
        localObject3 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$xBlniXjqYuxYTkDqbhLvWFYVSXg;
        ((-..Lambda.DetailsFragment.xBlniXjqYuxYTkDqbhLvWFYVSXg)localObject3).<init>(this);
        ((ImageView)localObject2).setOnClickListener((View.OnClickListener)localObject3);
        ((CallerButtonBase)localObject1).setShowButtonDividers(false);
        paramViewGroup.addView((View)localObject1);
        return bool3;
      }
    }
    return false;
  }
  
  private boolean b(Contact paramContact, boolean paramBoolean)
  {
    boolean bool1 = at;
    if (!bool1)
    {
      boolean bool2 = paramContact.U();
      if (!bool2)
      {
        bool2 = as;
        if ((!bool2) && (!paramBoolean)) {}
      }
      else
      {
        return true;
      }
    }
    return false;
  }
  
  private static String c(int paramInt)
  {
    DateFormatSymbols localDateFormatSymbols = new java/text/DateFormatSymbols;
    Locale localLocale = Locale.getDefault();
    localDateFormatSymbols.<init>(localLocale);
    return localDateFormatSymbols.getShortWeekdays()[paramInt];
  }
  
  private void c()
  {
    aX.clear();
    com.truecaller.ui.f localf = af;
    List localList = g();
    Contact localContact = aa;
    f.a locala = d();
    boolean bool = aD;
    localf.a(localList, "OTHER", localContact, "detailView", locala, bool);
  }
  
  private void c(View paramView)
  {
    Object localObject = new com/truecaller/analytics/e$a;
    ((com.truecaller.analytics.e.a)localObject).<init>("PayPromo");
    localObject = ((com.truecaller.analytics.e.a)localObject).a("Context", "detailView").a("Status", "opened");
    String str = (String)paramView.getTag();
    localObject = ((com.truecaller.analytics.e.a)localObject).a("Text", str).a();
    Z.c().a((com.truecaller.analytics.e)localObject);
    b(paramView);
  }
  
  private void c(ViewGroup paramViewGroup)
  {
    Object localObject = H;
    CallerButtonBase localCallerButtonBase = null;
    if (localObject == null)
    {
      int j = 2131888550;
      localObject = getString(j);
      localObject = a((String)localObject, 2131234530);
      H = ((CallerButtonBase)localObject);
      localObject = H;
      -..Lambda.DetailsFragment.qVYWPi83NLy3phiIPWoJcq-8VOQ localqVYWPi83NLy3phiIPWoJcq-8VOQ = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$qVYWPi83NLy3phiIPWoJcq-8VOQ;
      localqVYWPi83NLy3phiIPWoJcq-8VOQ.<init>(this);
      ((CallerButtonBase)localObject).setOnClickListener(localqVYWPi83NLy3phiIPWoJcq-8VOQ);
      localObject = H;
      int k = 2131362875;
      ((CallerButtonBase)localObject).findViewById(k).setVisibility(0);
      localObject = H.getRightImage();
      localqVYWPi83NLy3phiIPWoJcq-8VOQ = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$qVYWPi83NLy3phiIPWoJcq-8VOQ;
      localqVYWPi83NLy3phiIPWoJcq-8VOQ.<init>(this);
      ((ImageView)localObject).setOnClickListener(localqVYWPi83NLy3phiIPWoJcq-8VOQ);
    }
    localObject = new android/support/v7/widget/LinearLayoutCompat$LayoutParams;
    ((LinearLayoutCompat.LayoutParams)localObject).<init>(0, -1, 1.0F);
    localCallerButtonBase = H;
    paramViewGroup.addView(localCallerButtonBase, (ViewGroup.LayoutParams)localObject);
  }
  
  private void c(boolean paramBoolean)
  {
    a("call", "header");
    AppCompatActivity localAppCompatActivity = d;
    Contact localContact = aa;
    List localList = localContact.A();
    com.truecaller.calling.c.c.a(localAppCompatActivity, localContact, localList, true, paramBoolean, false, "detailViewHeader");
  }
  
  private f.a d()
  {
    DetailsFragment.7 local7 = new com/truecaller/ui/details/DetailsFragment$7;
    local7.<init>(this);
    return local7;
  }
  
  private void d(View paramView)
  {
    Object localObject1 = ah;
    Object localObject2 = "requestMoney";
    bb.a((com.truecaller.androidactors.f)localObject1, "detailView", (String)localObject2);
    localObject1 = al;
    boolean bool1 = am.b((CharSequence)localObject1);
    String str = null;
    if (!bool1)
    {
      localObject1 = al;
      j = ((String)localObject1).length();
      int k = 2;
      if (j > k)
      {
        localObject1 = al.substring(k);
        break label75;
      }
    }
    int j = 0;
    localObject1 = null;
    label75:
    localObject2 = aa;
    if (localObject2 != null)
    {
      localObject2 = ((Contact)localObject2).t();
      boolean bool2 = am.b((CharSequence)localObject2);
      if (!bool2) {
        str = aa.t();
      }
    }
    localObject2 = c;
    int i1 = ((ArrayList)localObject2).size();
    int i2 = 1;
    if (i1 > i2)
    {
      localObject1 = an;
      if (localObject1 == null)
      {
        localObject1 = new android/support/v7/app/AlertDialog$Builder;
        paramView = paramView.getContext();
        ((AlertDialog.Builder)localObject1).<init>(paramView);
        int i3 = 2131888879;
        paramView = ((AlertDialog.Builder)localObject1).setTitle(i3);
        localObject1 = new com/truecaller/ui/details/DetailsFragment$l;
        ((DetailsFragment.l)localObject1).<init>(this);
        localObject2 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$p4HhfCtIwItlX0vrs3a4hDC-ihI;
        ((-..Lambda.DetailsFragment.p4HhfCtIwItlX0vrs3a4hDC-ihI)localObject2).<init>(this, str);
        paramView = paramView.setAdapter((ListAdapter)localObject1, (DialogInterface.OnClickListener)localObject2).create();
        an = paramView;
      }
      an.show();
      return;
    }
    TransactionActivity.startForRequest(d, (String)localObject1, str);
  }
  
  private void d(ViewGroup paramViewGroup)
  {
    Object localObject1 = aa;
    boolean bool1 = ((Contact)localObject1).R();
    if (!bool1)
    {
      localObject1 = com.truecaller.util.w.a(aa).iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        Object localObject2 = (String)((Iterator)localObject1).next();
        boolean bool3 = TextUtils.isEmpty((CharSequence)localObject2);
        if (!bool3)
        {
          Object localObject3 = aU;
          Object localObject4 = aa;
          bool3 = ((com.truecaller.premium.b.a)localObject3).a((Contact)localObject4);
          int k = 2131234042;
          if (bool3)
          {
            int j = 2131886331;
            localObject2 = getString(j);
            localObject3 = PremiumPresenterView.LaunchContext.CONTACT_DETAILS_EMAIL;
            localObject2 = a((String)localObject2, k, (PremiumPresenterView.LaunchContext)localObject3);
          }
          else
          {
            bool3 = false;
            localObject3 = a((String)localObject2, null, k);
            localObject4 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$pu-2t1uwaYVOAsL-driYEychxYU;
            ((-..Lambda.DetailsFragment.pu-2t1uwaYVOAsL-driYEychxYU)localObject4).<init>(this, (String)localObject2);
            ((CallerButtonBase)localObject3).setOnClickListener((View.OnClickListener)localObject4);
            localObject4 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$BDXnLD_jqxeLx3TxmDxnfa98n9o;
            ((-..Lambda.DetailsFragment.BDXnLD_jqxeLx3TxmDxnfa98n9o)localObject4).<init>(this, (String)localObject2);
            ((CallerButtonBase)localObject3).setOnLongClickListener((View.OnLongClickListener)localObject4);
            localObject2 = localObject3;
          }
          paramViewGroup.addView((View)localObject2);
        }
      }
    }
  }
  
  private void e(View paramView)
  {
    boolean bool1 = aa.R();
    int k = 1;
    bool1 ^= k;
    at.a(paramView, 2131362390, bool1);
    Object localObject1 = aa;
    boolean bool3 = ((Contact)localObject1).R() ^ k;
    at.a(paramView, 2131362387, bool3);
    v();
    f(paramView);
    h(paramView);
    z();
    A();
    Object localObject2 = Z.aF().y();
    bool1 = ((com.truecaller.featuretoggles.b)localObject2).a();
    int i1 = 8;
    int i3;
    if (bool1)
    {
      localObject2 = aa;
      bool1 = ((Contact)localObject2).N();
      if (bool1)
      {
        v.setVisibility(0);
        localObject2 = v;
        i3 = 2131886204;
        ((TextView)localObject2).setText(i3);
        break label298;
      }
    }
    bool1 = aw;
    if (bool1)
    {
      v.setVisibility(0);
      localObject2 = v;
      i3 = 2131886616;
      ((TextView)localObject2).setText(i3);
    }
    else
    {
      bool1 = ax;
      if (bool1)
      {
        v.setVisibility(0);
        localObject2 = v;
        i3 = 2131886998;
        ((TextView)localObject2).setText(i3);
      }
      else
      {
        localObject2 = aa.t();
        bool1 = TextUtils.isEmpty((CharSequence)localObject2);
        if (!bool1)
        {
          localObject2 = aa;
          bool1 = ((Contact)localObject2).R();
          if (!bool1)
          {
            v.setVisibility(0);
            localObject2 = v;
            i3 = 2131886597;
            ((TextView)localObject2).setText(i3);
            break label298;
          }
        }
        localObject2 = v;
        ((TextView)localObject2).setVisibility(i1);
      }
    }
    label298:
    m.removeAllViews();
    r.removeAllViews();
    localObject2 = aa.C();
    Object localObject3 = "yelp";
    bool1 = am.c((CharSequence)localObject2, (CharSequence)localObject3);
    boolean bool6 = u();
    if (bool6) {
      if (bool1)
      {
        localObject3 = m;
        f((ViewGroup)localObject3);
      }
      else
      {
        localObject3 = m;
        e((ViewGroup)localObject3);
      }
    }
    localObject3 = m;
    bool6 = b((ViewGroup)localObject3);
    if (!bool6)
    {
      g.a(0);
      localObject3 = g;
      ((DetailsActionBar)localObject3).a(k);
    }
    h();
    localObject3 = TrueApp.y();
    bool6 = ((TrueApp)localObject3).isTcPayEnabled();
    boolean bool7;
    Object localObject5;
    if (bool6)
    {
      q.setVisibility(0);
      localObject3 = o;
      i4 = ((ViewGroup)localObject3).getChildCount();
      if (i4 == 0)
      {
        c.clear();
        localObject3 = i();
        bool7 = am.c((CharSequence)localObject3);
        if (bool7)
        {
          localObject4 = aq.af();
          bool7 = ((com.truecaller.featuretoggles.f)localObject4).a();
          localObject5 = aa.t();
          if (bool7)
          {
            bool7 = am.c((CharSequence)localObject5);
            if (bool7)
            {
              localObject4 = Truepay.getInstance();
              bool7 = ((Truepay)localObject4).isRegistrationComplete();
              if (!bool7)
              {
                f((String)localObject5);
                break label780;
              }
            }
          }
          p.setVisibility(i1);
          localObject5 = "";
          localObject4 = ((String)localObject3).replace("+", (CharSequence)localObject5);
          al = ((String)localObject4);
          bool7 = aw;
          if (bool7)
          {
            bool7 = as;
            if (!bool7)
            {
              au = k;
              localObject3 = o;
              ((ViewGroup)localObject3).setVisibility(i1);
              break label780;
            }
          }
          bool7 = aw;
          if (bool7)
          {
            bool7 = as;
            if (bool7)
            {
              au = false;
              localObject3 = o;
              ((ViewGroup)localObject3).setVisibility(i1);
              break label780;
            }
          }
          au = false;
          o.setVisibility(0);
          localObject4 = o;
          c((ViewGroup)localObject4);
          localObject4 = o;
          a((ViewGroup)localObject4, (String)localObject3);
          break label780;
        }
        localObject3 = p;
        ((ViewGroup)localObject3).setVisibility(i1);
      }
      else
      {
        localObject4 = q;
        if (i4 > 0)
        {
          i4 = 0;
          localObject3 = null;
        }
        else
        {
          i4 = 8;
        }
        ((View)localObject4).setVisibility(i4);
        break label780;
      }
    }
    localObject3 = q;
    ((View)localObject3).setVisibility(i1);
    label780:
    localObject3 = r;
    d((ViewGroup)localObject3);
    localObject3 = r;
    a((ViewGroup)localObject3);
    localObject3 = new com/truecaller/ui/details/DetailsFragment$j;
    ((DetailsFragment.j)localObject3).<init>(this, (byte)0);
    Object localObject4 = new Void[0];
    ((DetailsFragment.j)localObject3).execute((Object[])localObject4);
    localObject3 = com.truecaller.util.w.c(aa).iterator();
    for (;;)
    {
      bool7 = ((Iterator)localObject3).hasNext();
      if (!bool7) {
        break;
      }
      localObject4 = (Link)((Iterator)localObject3).next();
      String str1 = ((Link)localObject4).getInfo();
      localObject5 = "facebook";
      Object localObject6 = ((Link)localObject4).getService();
      boolean bool8 = ((String)localObject5).equals(localObject6);
      int i7;
      String str2;
      int i8;
      Object localObject7;
      String str3;
      if (bool8)
      {
        localObject4 = ((Link)localObject4).getInfo();
        localObject6 = r;
        localObject5 = getResources();
        i7 = 2131886336;
        str2 = ((Resources)localObject5).getString(i7);
        i8 = 2131234043;
        localObject7 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$kLX6-4q7eDkk72Zac4yPory91Qc;
        ((-..Lambda.DetailsFragment.kLX6-4q7eDkk72Zac4yPory91Qc)localObject7).<init>(this, (String)localObject4);
        str3 = "facebook";
        localObject5 = this;
        a((ViewGroup)localObject6, str1, str2, i8, (Runnable)localObject7, str3);
      }
      else
      {
        localObject5 = "skype";
        localObject6 = ((Link)localObject4).getService();
        bool8 = ((String)localObject5).equals(localObject6);
        if (bool8)
        {
          localObject6 = r;
          localObject4 = getResources();
          int i5 = 2131886377;
          str2 = ((Resources)localObject4).getString(i5);
          i8 = 2131234046;
          localObject7 = null;
          str3 = "skype";
          localObject5 = this;
          a((ViewGroup)localObject6, str1, str2, i8, null, str3);
        }
        else
        {
          localObject5 = "googleplus";
          localObject6 = ((Link)localObject4).getService();
          boolean bool9 = ((String)localObject5).equals(localObject6);
          if (bool9)
          {
            localObject6 = r;
            localObject4 = getResources();
            int i6 = 2131886340;
            str2 = ((Resources)localObject4).getString(i6);
            i8 = 2131234044;
            localObject7 = null;
            str3 = "googlePlus";
            localObject5 = this;
            a((ViewGroup)localObject6, str1, str2, i8, null, str3);
          }
          else
          {
            localObject5 = "twitter";
            localObject6 = ((Link)localObject4).getService();
            boolean bool10 = ((String)localObject5).equals(localObject6);
            if (bool10)
            {
              localObject4 = ((Link)localObject4).getInfo();
              localObject6 = r;
              localObject5 = getResources();
              i7 = 2131886381;
              str2 = ((Resources)localObject5).getString(i7);
              i8 = 2131234633;
              localObject7 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$2Qa8wMankpNR587SQqC0y_f1f_c;
              ((-..Lambda.DetailsFragment.2Qa8wMankpNR587SQqC0y_f1f_c)localObject7).<init>(this, (String)localObject4);
              str3 = "twitter";
              localObject5 = this;
              a((ViewGroup)localObject6, str1, str2, i8, (Runnable)localObject7, str3);
            }
          }
        }
      }
    }
    if (!bool1)
    {
      localObject2 = r;
      f((ViewGroup)localObject2);
    }
    localObject2 = t;
    localObject3 = r;
    int i4 = ((ViewGroup)localObject3).getChildCount();
    if (i4 > 0)
    {
      i1 = 0;
      localObject1 = null;
    }
    ((View)localObject2).setVisibility(i1);
    localObject2 = D;
    localObject1 = aa;
    boolean bool4 = ((Contact)localObject1).a(k);
    int i2;
    if (bool4) {
      i2 = 2131886382;
    } else {
      i2 = 2131886364;
    }
    ((TextView)localObject2).setText(i2);
    localObject2 = d;
    ((AppCompatActivity)localObject2).supportInvalidateOptionsMenu();
    int j = 2131363971;
    localObject1 = aa;
    boolean bool5 = ((Contact)localObject1).R();
    boolean bool2;
    if (bool5)
    {
      localObject1 = Z.ai();
      bool5 = ((com.truecaller.common.f.c)localObject1).d();
      if (!bool5) {}
    }
    else
    {
      bool2 = false;
    }
    at.a(paramView, j, bool2);
  }
  
  private void e(ViewGroup paramViewGroup)
  {
    CallerButtonBase localCallerButtonBase = new com/truecaller/ui/components/CallerButtonBase;
    Object localObject1 = d;
    localCallerButtonBase.<init>((Context)localObject1);
    localCallerButtonBase.setShowFullDivider(false);
    localCallerButtonBase.setLeftImage(2131234360);
    int j = aY;
    localCallerButtonBase.setImageTint(j);
    localCallerButtonBase.setSingleLine(true);
    Object localObject2 = t();
    localCallerButtonBase.setHeadingText((CharSequence)localObject2);
    localCallerButtonBase.setShowButtonDividers(false);
    localObject1 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$Sk69SuRQBQ2B5n3E_WXDZflhfCc;
    ((-..Lambda.DetailsFragment.Sk69SuRQBQ2B5n3E_WXDZflhfCc)localObject1).<init>(this);
    localCallerButtonBase.setOnClickListener((View.OnClickListener)localObject1);
    localObject1 = localCallerButtonBase.getRightImage();
    localObject2 = ImageView.ScaleType.CENTER_INSIDE;
    ((ImageView)localObject1).setScaleType((ImageView.ScaleType)localObject2);
    localObject2 = com.d.b.w.a(d);
    String str = aa.B();
    ((com.d.b.w)localObject2).a(str).a((ImageView)localObject1, null);
    paramViewGroup.addView(localCallerButtonBase);
  }
  
  private void f(View paramView)
  {
    Object localObject = Z.aF().y();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool)
    {
      localObject = aa;
      bool = ((Contact)localObject).N();
      if (bool)
      {
        w();
        x();
        at.a(paramView, 2131361813, false);
        return;
      }
    }
    g(paramView);
    N.setVisibility(8);
  }
  
  private void f(ViewGroup paramViewGroup)
  {
    Object localObject1 = aa.C();
    boolean bool1 = u();
    Object localObject2;
    if (bool1)
    {
      localObject2 = "yelp";
      bool1 = am.c((CharSequence)localObject1, (CharSequence)localObject2);
      if (bool1)
      {
        localObject2 = "Yelp";
      }
      else
      {
        localObject2 = "zomato";
        bool1 = am.c((CharSequence)localObject1, (CharSequence)localObject2);
        if (bool1) {
          localObject2 = "Zomato";
        }
      }
    }
    else
    {
      bool1 = false;
      localObject2 = null;
    }
    Object localObject3 = aa.D();
    boolean bool2 = am.a((CharSequence)localObject3);
    if (bool2) {
      localObject3 = aa.D();
    } else {
      localObject3 = com.truecaller.util.w.d(aa);
    }
    if (localObject2 == null)
    {
      bool3 = TextUtils.isEmpty((CharSequence)localObject3);
      if (bool3) {
        return;
      }
    }
    Object localObject4 = "itesco";
    boolean bool3 = am.c((CharSequence)localObject1, (CharSequence)localObject4);
    int i1 = 1;
    if (bool3)
    {
      localObject1 = am.a((String)localObject1);
    }
    else if (localObject2 == null)
    {
      localObject1 = ((String)localObject3).replace("https://", "");
      localObject4 = "http://";
      localObject5 = "";
      localObject1 = ((String)localObject1).replace((CharSequence)localObject4, (CharSequence)localObject5);
    }
    else
    {
      localObject1 = getResources();
      int j = 2131886351;
      localObject5 = new Object[i1];
      localObject5[0] = localObject2;
      localObject1 = ((Resources)localObject1).getString(j, (Object[])localObject5);
    }
    localObject4 = "www";
    boolean bool4 = org.c.a.a.a.k.a((CharSequence)localObject1, (CharSequence)localObject4, i1);
    if (bool4)
    {
      localObject4 = "www\\.";
      localObject5 = "";
      localObject1 = ((String)localObject1).replaceFirst((String)localObject4, (String)localObject5);
    }
    int k = 2131234045;
    if (localObject2 == null)
    {
      localObject5 = aU;
      Contact localContact = aa;
      Object localObject6 = a;
      com.truecaller.featuretoggles.e.a locala = x;
      Object localObject7 = com.truecaller.featuretoggles.e.a;
      int i3 = 75;
      localObject7 = localObject7[i3];
      localObject6 = locala.a((com.truecaller.featuretoggles.e)localObject6, (c.l.g)localObject7);
      boolean bool5 = ((com.truecaller.featuretoggles.b)localObject6).a();
      if (bool5)
      {
        bool6 = ((com.truecaller.premium.b.a)localObject5).b(localContact, i1);
        if (bool6)
        {
          bool6 = true;
          break label375;
        }
      }
      bool6 = false;
      localObject5 = null;
      label375:
      if (bool6)
      {
        int i4 = 2131886335;
        localObject1 = getString(i4);
        localObject2 = PremiumPresenterView.LaunchContext.CONTACT_DETAILS_WEBSITE;
        localObject1 = a((String)localObject1, k, (PremiumPresenterView.LaunchContext)localObject2);
        break label598;
      }
    }
    Object localObject5 = "Yelp";
    boolean bool6 = ((String)localObject5).equals(localObject2);
    if (bool6)
    {
      localObject5 = t();
      localObject1 = a((String)localObject5, (String)localObject1, k);
    }
    else
    {
      localObject5 = getResources();
      int i5 = 2131886365;
      localObject5 = ((Resources)localObject5).getString(i5);
      localObject1 = a((String)localObject1, (String)localObject5, k);
    }
    ((CallerButtonBase)localObject1).setShowButtonDividers(false);
    ((CallerButtonBase)localObject1).setHeadingMaxLines(i1);
    localObject4 = getResources();
    int i2 = 2131165384;
    k = ((Resources)localObject4).getDimensionPixelSize(i2);
    ((CallerButtonBase)localObject1).setMinimumHeight(k);
    localObject4 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$pF4kb3w1cZG-MTB1OOo0Qq2h4Ng;
    ((-..Lambda.DetailsFragment.pF4kb3w1cZG-MTB1OOo0Qq2h4Ng)localObject4).<init>(this, (String)localObject3);
    ((CallerButtonBase)localObject1).setOnClickListener((View.OnClickListener)localObject4);
    if (localObject2 != null)
    {
      localObject2 = ((CallerButtonBase)localObject1).getRightImage();
      localObject3 = ImageView.ScaleType.CENTER_INSIDE;
      ((ImageView)localObject2).setScaleType((ImageView.ScaleType)localObject3);
      ((ImageView)localObject2).setVisibility(0);
      localObject3 = com.d.b.w.a(d);
      localObject4 = aa.B();
      localObject3 = ((com.d.b.w)localObject3).a((String)localObject4);
      ((com.d.b.ab)localObject3).a((ImageView)localObject2, null);
    }
    label598:
    paramViewGroup.addView((View)localObject1);
  }
  
  private void f(String paramString)
  {
    String str1 = " ";
    paramString = paramString.split(str1);
    int j = paramString.length;
    -..Lambda.DetailsFragment.LB_N5kGxscihjx2jzMO3UmT1iQw localLB_N5kGxscihjx2jzMO3UmT1iQw = null;
    if (j > 0) {
      paramString = paramString[0];
    } else {
      paramString = aa.z();
    }
    str1 = aj.a("payPromoTitle");
    String str2 = aj.a("payPromoSubtitle");
    String str3 = aj.a("payPromoButtonText");
    p.setVisibility(0);
    o.setVisibility(8);
    p.setTag(str1);
    Object localObject1 = p;
    Object localObject2 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$LB_N5kGxscihjx2jzMO3UmT1iQw;
    ((-..Lambda.DetailsFragment.LB_N5kGxscihjx2jzMO3UmT1iQw)localObject2).<init>(this);
    ((ViewGroup)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    ((TextView)p.findViewById(2131364029)).setText(str1);
    localObject1 = (TextView)p.findViewById(2131364027);
    localObject2 = new Object[1];
    localObject2[0] = paramString;
    paramString = String.format(str2, (Object[])localObject2);
    ((TextView)localObject1).setText(paramString);
    paramString = (Button)p.findViewById(2131363898);
    paramString.setTag(str1);
    paramString.setText(str3);
    localLB_N5kGxscihjx2jzMO3UmT1iQw = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$LB_N5kGxscihjx2jzMO3UmT1iQw;
    localLB_N5kGxscihjx2jzMO3UmT1iQw.<init>(this);
    paramString.setOnClickListener(localLB_N5kGxscihjx2jzMO3UmT1iQw);
    paramString = new com/truecaller/analytics/e$a;
    paramString.<init>("PayPromo");
    paramString = paramString.a("Context", "detailView").a("Status", "shown").a("Text", str1).a();
    Z.c().a(paramString);
  }
  
  private List g()
  {
    Object localObject = aa.A();
    ArrayList localArrayList = new java/util/ArrayList;
    int j = ((List)localObject).size();
    localArrayList.<init>(j);
    localObject = ((List)localObject).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject).hasNext();
      if (!bool1) {
        break;
      }
      String str = ((Number)((Iterator)localObject).next()).a();
      boolean bool2 = TextUtils.isEmpty(str);
      if (!bool2) {
        localArrayList.add(str);
      }
    }
    return localArrayList;
  }
  
  private void g(View paramView)
  {
    Object localObject1 = aa.h();
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1);
    Object localObject2 = null;
    Object localObject3 = null;
    int j;
    if (!bool1)
    {
      A.setVisibility(0);
      localObject1 = aU;
      localObject4 = aa;
      bool1 = ((com.truecaller.premium.b.a)localObject1).c((Contact)localObject4);
      at.a(C, bool1);
      localObject4 = A;
      -..Lambda.DetailsFragment.y0gf5lGKa10K2KBsO-9dtGzpd8Y localy0gf5lGKa10K2KBsO-9dtGzpd8Y;
      if (bool1)
      {
        localy0gf5lGKa10K2KBsO-9dtGzpd8Y = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$y0gf5lGKa10K2KBsO-9dtGzpd8Y;
        localy0gf5lGKa10K2KBsO-9dtGzpd8Y.<init>(this);
      }
      else
      {
        localy0gf5lGKa10K2KBsO-9dtGzpd8Y = null;
      }
      ((View)localObject4).setOnClickListener(localy0gf5lGKa10K2KBsO-9dtGzpd8Y);
      localObject4 = B;
      if (bool1)
      {
        j = 2131886329;
        localObject1 = getString(j);
      }
      else
      {
        localObject1 = aa.h();
      }
      ((TextView)localObject4).setText((CharSequence)localObject1);
      j = 1;
    }
    else
    {
      j = 0;
      localObject1 = null;
    }
    Object localObject4 = aa.w();
    boolean bool3 = TextUtils.isEmpty((CharSequence)localObject4);
    int k;
    if (!bool3)
    {
      w.setVisibility(0);
      localObject1 = aU;
      localObject3 = aa;
      boolean bool2 = ((com.truecaller.premium.b.a)localObject1).b((Contact)localObject3);
      at.a(y, bool2);
      localObject3 = w;
      if (bool2)
      {
        localObject2 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$rYYA93HM4jpqhr4XH9hKzLYfiyw;
        ((-..Lambda.DetailsFragment.rYYA93HM4jpqhr4XH9hKzLYfiyw)localObject2).<init>(this);
      }
      ((View)localObject3).setOnClickListener((View.OnClickListener)localObject2);
      localObject2 = x;
      if (bool2)
      {
        k = 2131886333;
        localObject1 = getString(k);
      }
      else
      {
        localObject1 = aa.w();
      }
      ((TextView)localObject2).setText((CharSequence)localObject1);
      k = 1;
    }
    at.a(paramView, 2131361813, k);
  }
  
  private void g(String paramString)
  {
    boolean bool = am.b(paramString);
    String str1 = null;
    if (!bool)
    {
      localObject = "+";
      String str2 = "";
      paramString = paramString.replace((CharSequence)localObject, str2);
    }
    else
    {
      paramString = null;
    }
    Object localObject = aa;
    if (localObject != null)
    {
      localObject = ((Contact)localObject).t();
      bool = am.b((CharSequence)localObject);
      if (!bool)
      {
        localObject = aa;
        str1 = ((Contact)localObject).t();
      }
    }
    TransactionActivity.startForSend(d, paramString, str1);
  }
  
  private void h()
  {
    Object localObject = d;
    if (localObject != null)
    {
      localObject = aa;
      if (localObject != null)
      {
        localObject = Z.aU().a().a();
        boolean bool1 = ((com.truecaller.swish.k)localObject).a();
        int j = 8;
        if (!bool1)
        {
          n.setVisibility(j);
          return;
        }
        ArrayList localArrayList = new java/util/ArrayList;
        localArrayList.<init>();
        Iterator localIterator = aa.A().iterator();
        for (;;)
        {
          boolean bool2 = localIterator.hasNext();
          if (!bool2) {
            break;
          }
          Number localNumber = (Number)localIterator.next();
          String str = localNumber.a();
          if (str != null)
          {
            str = ((com.truecaller.swish.k)localObject).a(str);
            if (str != null) {
              localArrayList.add(localNumber);
            }
          }
        }
        boolean bool3 = localArrayList.isEmpty();
        if (bool3)
        {
          n.setVisibility(j);
          n.setOnClickListener(null);
          return;
        }
        localObject = n;
        j = 0;
        ((ViewGroup)localObject).setVisibility(0);
        localObject = n;
        -..Lambda.DetailsFragment.Azc6ogMkTX88pADutER3u5sqR5Y localAzc6ogMkTX88pADutER3u5sqR5Y = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$Azc6ogMkTX88pADutER3u5sqR5Y;
        localAzc6ogMkTX88pADutER3u5sqR5Y.<init>(this, localArrayList);
        ((ViewGroup)localObject).setOnClickListener(localAzc6ogMkTX88pADutER3u5sqR5Y);
        bool3 = aO;
        if (!bool3)
        {
          localObject = "Shown";
          b((String)localObject);
          bool3 = true;
          aO = bool3;
        }
        return;
      }
    }
  }
  
  private void h(View paramView)
  {
    boolean bool = y();
    int j = 2131363817;
    if (bool)
    {
      bool = true;
      at.a(paramView, j, bool);
      TextView localTextView = z;
      String str = aa.h.getValue();
      localTextView.setText(str);
      at.a(paramView, 2131361813, bool);
      return;
    }
    at.a(paramView, j, false);
  }
  
  private String i()
  {
    Iterator localIterator = aa.A().iterator();
    String str = null;
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject1 = (Number)localIterator.next();
      Object localObject2 = ((Number)localObject1).l();
      if (localObject2 != null)
      {
        localObject2 = ((Number)localObject1).l();
        Object localObject3 = "IN";
        boolean bool2 = ((String)localObject2).equals(localObject3);
        if (bool2)
        {
          localObject2 = ((Number)localObject1).m();
          localObject3 = k.d.h;
          if (localObject2 != localObject3)
          {
            localObject2 = ((Number)localObject1).m();
            localObject3 = k.d.b;
            if (localObject2 != localObject3)
            {
              localObject2 = ((Number)localObject1).m();
              localObject3 = k.d.c;
              if (localObject2 != localObject3) {
                continue;
              }
            }
          }
          str = ((Number)localObject1).a();
          localObject1 = " ";
          localObject2 = "";
          str = str.replaceAll((String)localObject1, (String)localObject2);
          int j = str.length();
          int k = 10;
          if (j >= k)
          {
            localObject1 = c;
            int i1 = str.length() - k;
            localObject2 = str.substring(i1);
            ((ArrayList)localObject1).add(localObject2);
          }
        }
      }
    }
    boolean bool3 = am.b(str);
    if (bool3) {
      return null;
    }
    return str;
  }
  
  private void i(View paramView)
  {
    int j = paramView.getHeight();
    if (j == 0)
    {
      paramView.measure(-1, -2);
      j = paramView.getHeight();
      int k = paramView.getMeasuredHeight();
      a(paramView, j, k);
      U.setImageResource(2131230967);
      a("businessHoursExpanded", null);
      return;
    }
    j = paramView.getHeight();
    a(paramView, j, 0);
    U.setImageResource(2131230969);
  }
  
  private void j(View paramView)
  {
    boolean bool1 = ar;
    if (!bool1) {
      return;
    }
    Object localObject = aa;
    bool1 = ((Contact)localObject).R();
    if (bool1)
    {
      localObject = Z.ai();
      bool1 = ((com.truecaller.common.f.c)localObject).d();
      if (bool1)
      {
        i.setVisibility(0);
        localObject = l;
        boolean bool2 = true;
        Object[] arrayOfObject = new Object[bool2];
        String str1 = String.valueOf(Z.ai().i());
        arrayOfObject[0] = str1;
        String str2 = getString(2131886326, arrayOfObject);
        at.b((TextView)localObject, str2);
        at.a(paramView, 2131363975, bool2);
        m.setBackgroundResource(2131230996);
        paramView = m;
        int j = getResources().getDimensionPixelSize(2131165377);
        paramView.setPadding(0, j, 0, 0);
        return;
      }
    }
    m.setBackgroundResource(2131230873);
  }
  
  private void m()
  {
    AppCompatActivity localAppCompatActivity = d;
    if (localAppCompatActivity != null)
    {
      Contact localContact = aa;
      d.a(localAppCompatActivity, localContact);
    }
    a("callHistory", null);
  }
  
  private void n()
  {
    Object localObject = bg;
    if (localObject == null) {
      return;
    }
    localObject = L.getOverflowIcon();
    int j;
    Toolbar localToolbar;
    if (localObject != null)
    {
      localObject = android.support.v4.graphics.drawable.a.e((Drawable)localObject).mutate();
      j = bg.c;
      android.support.v4.graphics.drawable.a.a((Drawable)localObject, j);
      localToolbar = L;
      localToolbar.setOverflowIcon((Drawable)localObject);
    }
    localObject = L.getNavigationIcon();
    if (localObject != null)
    {
      localObject = android.support.v4.graphics.drawable.a.e((Drawable)localObject).mutate();
      j = bg.c;
      android.support.v4.graphics.drawable.a.a((Drawable)localObject, j);
      localToolbar = L;
      localToolbar.setNavigationIcon((Drawable)localObject);
    }
  }
  
  private String t()
  {
    String str1 = aa.C();
    boolean bool1 = TextUtils.isEmpty(str1);
    if (!bool1)
    {
      String str2 = "yelp";
      bool1 = am.c(str1, str2);
      if (bool1)
      {
        str2 = "(";
        int j = str1.indexOf(str2);
        String str3 = ")";
        int k = str1.indexOf(str3);
        if (k <= j) {
          return "0";
        }
        j += 1;
        return str1.substring(j, k).trim();
      }
      str2 = "zomato";
      boolean bool2 = am.c(str1, str2);
      if (bool2) {
        return "Zomato";
      }
    }
    return str1;
  }
  
  private boolean u()
  {
    return am.a(aa.B());
  }
  
  private void v()
  {
    Object localObject1 = Z.aF().y();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool1)
    {
      localObject1 = com.truecaller.util.w.f(aa);
      boolean bool2 = ((List)localObject1).isEmpty();
      if (!bool2)
      {
        N.setVisibility(0);
        O.setVisibility(0);
        Object localObject2 = O;
        boolean bool3 = true;
        ((RecyclerView)localObject2).setHasFixedSize(bool3);
        localObject2 = new com/truecaller/ui/details/l;
        ((l)localObject2).<init>(this);
        RecyclerView localRecyclerView = O;
        localRecyclerView.setAdapter((RecyclerView.Adapter)localObject2);
        ((l)localObject2).a((List)localObject1);
      }
      return;
    }
    localObject1 = O;
    int j = 8;
    ((RecyclerView)localObject1).setVisibility(j);
    N.setVisibility(j);
  }
  
  private void w()
  {
    String str = aa.h();
    if (str != null)
    {
      boolean bool = str.isEmpty();
      if (!bool)
      {
        N.setVisibility(0);
        P.setVisibility(0);
        N.findViewById(2131361811).setVisibility(0);
        Object localObject = N;
        int j = 2131361809;
        ((CardView)localObject).findViewById(j).setVisibility(0);
        localObject = P;
        ((TextView)localObject).setText(str);
      }
    }
  }
  
  private void x()
  {
    Object localObject1 = Z.aF().y();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool1)
    {
      localObject1 = aa.z();
      if (localObject1 != null)
      {
        boolean bool2 = ((String)localObject1).isEmpty();
        if (!bool2)
        {
          N.setVisibility(0);
          Q.setVisibility(0);
          Object localObject2 = N;
          int j = 2131362548;
          ((CardView)localObject2).findViewById(j).setVisibility(0);
          localObject2 = aa.x();
          if (localObject2 != null)
          {
            boolean bool3 = ((String)localObject2).isEmpty();
            if (!bool3)
            {
              int k = 2131886230;
              int i1 = 2;
              Object[] arrayOfObject = new Object[i1];
              arrayOfObject[0] = localObject1;
              bool1 = true;
              arrayOfObject[bool1] = localObject2;
              localObject1 = getString(k, arrayOfObject);
            }
          }
          localObject2 = Q;
          ((TextView)localObject2).setText((CharSequence)localObject1);
        }
      }
    }
  }
  
  private boolean y()
  {
    Object localObject = aa.h;
    if (localObject != null)
    {
      String str = ((Note)localObject).getValue();
      boolean bool1 = TextUtils.isEmpty(str);
      if (!bool1)
      {
        str = aa.h();
        bool1 = TextUtils.isEmpty(str);
        if (!bool1)
        {
          localObject = ((Note)localObject).getValue();
          str = aa.h();
          boolean bool2 = ((String)localObject).equals(str);
          if (bool2) {
            return false;
          }
        }
        return true;
      }
    }
    return false;
  }
  
  private void z()
  {
    Object localObject1 = Z.aF().y();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
    if (bool1)
    {
      localObject1 = com.truecaller.util.w.e(aa);
      boolean bool2 = ((List)localObject1).isEmpty();
      if (!bool2)
      {
        R.setVisibility(0);
        Object localObject2 = R;
        Object localObject3 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$JCjpI1t_125XmFHpdZ1VFpICPNQ;
        ((-..Lambda.DetailsFragment.JCjpI1t_125XmFHpdZ1VFpICPNQ)localObject3).<init>(this);
        ((View)localObject2).setOnClickListener((View.OnClickListener)localObject3);
        localObject2 = T;
        localObject3 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$lMCCDy-pZARBnsEmqH6iIQvsb6Y;
        ((-..Lambda.DetailsFragment.lMCCDy-pZARBnsEmqH6iIQvsb6Y)localObject3).<init>(this);
        ((RecyclerView)localObject2).setOnTouchListener((View.OnTouchListener)localObject3);
        localObject2 = -..Lambda.DetailsFragment.nlnaaFPBDSmVf5cEciMmnMtWW40.INSTANCE;
        Collections.sort((List)localObject1, (Comparator)localObject2);
        a((List)localObject1);
        T.setHasFixedSize(false);
        T.setNestedScrollingEnabled(false);
        localObject2 = new com/truecaller/ui/details/c;
        ((c)localObject2).<init>();
        RecyclerView localRecyclerView = T;
        localRecyclerView.setAdapter((RecyclerView.Adapter)localObject2);
        localObject1 = b((List)localObject1);
        ((c)localObject2).a((List)localObject1);
      }
      return;
    }
    R.setVisibility(8);
  }
  
  public final void a()
  {
    af = null;
  }
  
  public final void a(int paramInt)
  {
    DetailsFragment localDetailsFragment = this;
    Object localObject1 = aa;
    if (localObject1 == null) {
      return;
    }
    boolean bool1 = false;
    localObject1 = null;
    int k = 1;
    Object localObject2 = null;
    Object localObject3;
    Object localObject4;
    Object localObject5;
    switch (paramInt)
    {
    case 3: 
    case 9: 
    default: 
      break;
    case 11: 
      localObject1 = aT;
      localObject3 = getActivity();
      localObject2 = aa;
      ((ai)localObject1).a((android.support.v4.app.f)localObject3, (Contact)localObject2, "detailView");
      return;
    case 10: 
      a("pay", null);
      localObject1 = o();
      b((View)localObject1);
      return;
    case 8: 
      localObject1 = Z.ai();
      bool1 = ((com.truecaller.common.f.c)localObject1).d();
      if (bool1)
      {
        localObject1 = new com/truecaller/ui/dialogs/g;
        localObject3 = d;
        ((com.truecaller.ui.dialogs.g)localObject1).<init>((Context)localObject3, false);
        ao = ((com.truecaller.ui.dialogs.g)localObject1);
        ao.show();
        localObject1 = Z.aX();
        localObject3 = aa.getTcId();
        localObject2 = aa.t();
        localObject4 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$tWDcUOZan4ndBujGfntNHgprlMw;
        ((-..Lambda.DetailsFragment.tWDcUOZan4ndBujGfntNHgprlMw)localObject4).<init>(this);
        ((com.truecaller.a.f)localObject1).a((String)localObject3, (String)localObject2, (f.b)localObject4);
        return;
      }
      localObject1 = d;
      localObject3 = PremiumPresenterView.LaunchContext.CONTACT_DETAIL_CONTACT_REQ;
      com.truecaller.premium.br.a((Context)localObject1, (PremiumPresenterView.LaunchContext)localObject3);
      return;
    case 7: 
      localObject1 = d;
      localObject3 = PremiumPresenterView.LaunchContext.CONTACT_DETAIL_CONTACT_REQ;
      com.truecaller.premium.br.a((Context)localObject1, (PremiumPresenterView.LaunchContext)localObject3);
      break;
    case 6: 
      b(k);
      return;
    case 5: 
      b(false);
      return;
    case 4: 
      c();
      return;
    case 2: 
      a("flash", null);
      localObject4 = aa.A();
      localObject5 = new java/util/ArrayList;
      ((ArrayList)localObject5).<init>();
      String str1 = aa.t();
      localObject4 = ((List)localObject4).iterator();
      String str2;
      Object localObject6;
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject4).hasNext();
        if (!bool2) {
          break;
        }
        str2 = ((Number)((Iterator)localObject4).next()).a();
        boolean bool3 = am.b(str2);
        if (!bool3)
        {
          String str3 = str2.replace("+", "");
          localObject6 = Z.aV().h(str3);
          boolean bool4 = c;
          if (bool4)
          {
            bool4 = am.b(str1);
            if (!bool4) {
              str2 = str1;
            }
            localObject6 = new com/truecaller/flashsdk/models/FlashContact;
            ((FlashContact)localObject6).<init>(str3, str2, null);
            ((ArrayList)localObject5).add(localObject6);
          }
        }
      }
      int j = ((ArrayList)localObject5).size();
      int i1;
      if (j == k)
      {
        localObject1 = (FlashContact)((ArrayList)localObject5).get(0);
        localObject4 = Z.aV();
        localObject5 = a;
        localObject4 = ((com.truecaller.flashsdk.core.b)localObject4).g((String)localObject5);
        long l1 = System.currentTimeMillis();
        long l2 = b;
        l1 -= l2;
        l2 = 60000L;
        boolean bool5 = l1 < l2;
        if (bool5)
        {
          i1 = 0;
          localObject3 = null;
        }
        if (i1 != 0)
        {
          localObject3 = new android/os/Bundle;
          ((Bundle)localObject3).<init>();
          ((Bundle)localObject3).putString("flash_context", "detailView");
          com.truecaller.flashsdk.core.c.a().a("ANDROID_FLASH_TAPPED", (Bundle)localObject3);
          localObject2 = Z.aV();
          localObject4 = getContext();
          l1 = Long.parseLong(a);
          str2 = b;
          ((com.truecaller.flashsdk.core.b)localObject2).a((Context)localObject4, l1, str2, "detailView");
          return;
        }
        localObject6 = Z.aV();
        Context localContext = getContext();
        long l3 = Long.parseLong(a);
        String str4 = b;
        long l4 = l2 - l1;
        ((com.truecaller.flashsdk.core.b)localObject6).a(localContext, l3, str4, "detailView", l4);
        return;
      }
      j = ((ArrayList)localObject5).size();
      if (j > i1)
      {
        localObject1 = Z.aV();
        localObject3 = getContext();
        localObject2 = "detailView";
        ((com.truecaller.flashsdk.core.b)localObject1).a((Context)localObject3, (ArrayList)localObject5, (String)localObject2);
      }
      return;
    case 1: 
      a("message", "header");
      localObject2 = d;
      localObject4 = aa;
      localObject5 = ((Contact)localObject4).A();
      com.truecaller.calling.c.c.a((android.support.v4.app.f)localObject2, (Contact)localObject4, (List)localObject5, false, false, true, "detailView");
      return;
    case 0: 
      c(false);
      return;
    }
  }
  
  public final void a(AppBarLayout paramAppBarLayout, int paramInt)
  {
    Object localObject = G;
    if (localObject != null)
    {
      int j = a((View)localObject);
      TextView localTextView = G;
      int k = localTextView.getHeight() / 2;
      j += k;
      k = V;
      int i1 = 4;
      if (j <= k)
      {
        bool = aB;
        if (!bool)
        {
          F.setVisibility(0);
          localObject = f;
          ((DetailsHeaderView)localObject).setVisibility(i1);
          bool = true;
          aB = bool;
        }
      }
      else
      {
        bool = aB;
        if (bool)
        {
          F.setVisibility(i1);
          localObject = f;
          ((DetailsHeaderView)localObject).setVisibility(0);
          aB = false;
        }
      }
      boolean bool = com.truecaller.common.h.k.d();
      if (bool)
      {
        localObject = d;
        if (localObject != null)
        {
          localObject = bg;
          if (localObject != null)
          {
            paramInt = -paramInt;
            int i2 = paramAppBarLayout.getTotalScrollRange();
            if (paramInt == i2)
            {
              paramAppBarLayout = g;
              localDrawable = bg.e.b;
              paramAppBarLayout.setBackground(localDrawable);
              return;
            }
            paramAppBarLayout = g.getBackground();
            Drawable localDrawable = bg.e.b;
            if (paramAppBarLayout == localDrawable)
            {
              paramAppBarLayout = g;
              localDrawable = bg.e.c;
              paramAppBarLayout.setBackground(localDrawable);
            }
          }
        }
      }
    }
  }
  
  public final void a(ImageView paramImageView, String paramString)
  {
    Context localContext = getContext();
    if (localContext != null)
    {
      com.truecaller.ui.dialogs.j localj = new com/truecaller/ui/dialogs/j;
      paramString = Uri.parse(paramString);
      localj.<init>(localContext, paramString, paramImageView);
      localj.show();
      paramImageView = "businessImageOpened";
      paramString = null;
      a(paramImageView, null);
    }
  }
  
  public final void d_(boolean paramBoolean)
  {
    paramBoolean = s();
    if (paramBoolean)
    {
      DetailsHeaderView localDetailsHeaderView = f;
      boolean bool = true;
      localDetailsHeaderView.a(bool);
    }
  }
  
  public final void e()
  {
    boolean bool = s();
    if (bool)
    {
      DetailsHeaderView localDetailsHeaderView = f;
      localDetailsHeaderView.a(false);
    }
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Object localObject1 = new String[1];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    Object localObject3 = getClass().getSimpleName();
    ((StringBuilder)localObject2).append((String)localObject3);
    ((StringBuilder)localObject2).append(" - onActivityResult, requestsCode: ");
    ((StringBuilder)localObject2).append(paramInt1);
    ((StringBuilder)localObject2).append(", resultCode: ");
    ((StringBuilder)localObject2).append(paramInt2);
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject3 = null;
    localObject1[0] = localObject2;
    int j = 21;
    if (paramInt1 == j)
    {
      localObject1 = aa;
      if (localObject1 != null)
      {
        j = -1;
        if (paramInt2 == j)
        {
          if (paramIntent != null)
          {
            localObject1 = paramIntent.getData();
            if (localObject1 != null)
            {
              localObject1 = paramIntent.getData();
              localObject2 = new com/truecaller/ui/details/DetailsFragment$2;
              localObject3 = aa;
              ((DetailsFragment.2)localObject2).<init>(this, (Contact)localObject3, (Uri)localObject1);
              break label196;
            }
          }
          return;
        }
        localObject1 = new com/truecaller/ui/details/DetailsFragment$3;
        ((DetailsFragment.3)localObject1).<init>(this);
        break label196;
      }
    }
    j = 31;
    if (paramInt1 == j)
    {
      localObject1 = bc;
      a((String)localObject1);
    }
    label196:
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
  }
  
  public final void onAttach(Activity paramActivity)
  {
    super.onAttach(paramActivity);
    bp localbp = ((bk)paramActivity.getApplicationContext()).a();
    Z = localbp;
    paramActivity = (AppCompatActivity)paramActivity;
    d = paramActivity;
  }
  
  public final void onClick(View paramView)
  {
    Object localObject1 = aa;
    if (localObject1 == null) {
      return;
    }
    int j = paramView.getId();
    int i1 = 2131364653;
    int i5;
    int k;
    if (j == i1)
    {
      paramView = d;
      localObject1 = aa;
      i4 = 2;
      i5 = 4;
      paramView = TagPickActivity.a(paramView, (Contact)localObject1, i4, i5);
      startActivity(paramView);
      paramView = aa.a;
      k = paramView.isEmpty() ^ true;
      if (k != 0)
      {
        a("tag", "edit");
        return;
      }
      a("tag", "add");
      return;
    }
    i1 = 2131363970;
    if (k == i1)
    {
      paramView = d;
      localObject1 = PremiumPresenterView.LaunchContext.CONTACT_DETAIL_CONTACT_REQ;
      com.truecaller.premium.br.a(paramView, (PremiumPresenterView.LaunchContext)localObject1);
      return;
    }
    int i2 = 2131364612;
    int i4 = 2131361985;
    int i3;
    if (k != i4)
    {
      i5 = 2131364614;
      if ((k != i5) && (k != i2))
      {
        i3 = 2131364099;
        if (k == i3)
        {
          paramView = aI;
          if (paramView != null)
          {
            localObject1 = ReferralManager.ReferralLaunchContext.CONTACT_DETAILS;
            Contact localContact1 = aa;
            paramView.a((ReferralManager.ReferralLaunchContext)localObject1, localContact1);
          }
        }
        return;
      }
    }
    Object localObject2 = d;
    Contact localContact2 = aa;
    String str = "details";
    localObject2 = NameSuggestionActivity.a((Context)localObject2, localContact2, str);
    startActivity((Intent)localObject2);
    if (k == i4) {
      paramView = "addName";
    } else if (k == i3) {
      paramView = "notBusiness";
    } else {
      paramView = "suggestName";
    }
    localObject1 = new com/truecaller/analytics/e$a;
    ((com.truecaller.analytics.e.a)localObject1).<init>("ViewAction");
    paramView = ((com.truecaller.analytics.e.a)localObject1).a("Context", "detailView").a("Action", paramView);
    localObject1 = Z.c();
    paramView = paramView.a();
    ((com.truecaller.analytics.b)localObject1).a(paramView);
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    Object localObject1 = aa;
    if (localObject1 != null)
    {
      boolean bool1 = s();
      if ((bool1) && (paramMenuInflater != null) && (paramMenu != null))
      {
        localObject1 = aa.q();
        bool1 = am.a((String)localObject1, 3);
        com.truecaller.utils.l locall = Z.bw();
        Object localObject2 = { "android.permission.WRITE_CONTACTS" };
        boolean bool2 = locall.a((String[])localObject2);
        boolean bool3 = aw;
        boolean bool4 = true;
        if (!bool3)
        {
          bool3 = aF;
          if (!bool3)
          {
            localObject2 = DetailsFragment.SourceType.Contacts;
            localObject3 = ai;
            if (localObject2 == localObject3)
            {
              bool3 = aE;
              if (!bool3)
              {
                bool3 = true;
                break label143;
              }
            }
          }
        }
        bool3 = false;
        localObject2 = null;
        label143:
        paramMenuInflater.inflate(2131623948, paramMenu);
        paramMenuInflater = paramMenu.findItem(2131361943);
        Object localObject3 = aR;
        boolean bool5 = ((cf)localObject3).a();
        paramMenuInflater.setVisible(bool5);
        paramMenu.findItem(2131361856).setVisible(bool1);
        paramMenuInflater = paramMenu.findItem(2131361854);
        bool1 = aa.P();
        paramMenuInflater.setVisible(bool1);
        paramMenuInflater = paramMenu.findItem(2131361855);
        bool1 = aa.P();
        paramMenuInflater.setVisible(bool1);
        paramMenuInflater = paramMenu.findItem(2131361935);
        localObject1 = aa;
        bool1 = ((Contact)localObject1).R() ^ bool4;
        paramMenuInflater.setVisible(bool1);
        int j = 2131361924;
        paramMenuInflater = paramMenu.findItem(j);
        bool1 = aw;
        if (!bool1)
        {
          bool1 = ax;
          if (!bool1)
          {
            bool1 = false;
            localObject1 = null;
            break label341;
          }
        }
        bool1 = true;
        label341:
        paramMenuInflater.setVisible(bool1);
        paramMenuInflater = paramMenu.findItem(2131361835);
        bool1 = au;
        paramMenuInflater.setVisible(bool1);
        j = 2131361930;
        paramMenuInflater = paramMenu.findItem(j);
        if (bool2)
        {
          bool1 = aw;
          if (!bool1)
          {
            bool1 = true;
            break label416;
          }
        }
        bool1 = false;
        localObject1 = null;
        label416:
        paramMenuInflater.setVisible(bool1);
        j = 2131361864;
        paramMenuInflater = paramMenu.findItem(j);
        if (bool2)
        {
          bool1 = aw;
          if (bool1)
          {
            bool1 = true;
            break label466;
          }
        }
        bool1 = false;
        localObject1 = null;
        label466:
        paramMenuInflater.setVisible(bool1);
        paramMenuInflater = paramMenu.findItem(2131361925);
        paramMenuInflater.setVisible(bool3);
        paramMenu = paramMenu.findItem(2131361865);
        boolean bool6 = aw;
        if (bool6)
        {
          paramMenuInflater = aa;
          bool6 = paramMenuInflater.X();
          int k;
          if (bool6) {
            k = 2131886359;
          } else {
            k = 2131886353;
          }
          paramMenu.setTitle(k);
          paramMenu.setVisible(bool4);
          return;
        }
        paramMenu.setVisible(false);
      }
    }
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    setHasOptionsMenu(true);
    return paramLayoutInflater.inflate(2131559175, paramViewGroup, false);
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject1 = ae;
    if (localObject1 != null)
    {
      localObject1 = d.getContentResolver();
      localObject2 = ae;
      ((ContentResolver)localObject1).unregisterContentObserver((ContentObserver)localObject2);
      ae = null;
    }
    localObject1 = bf;
    if (localObject1 != null)
    {
      ((com.truecaller.ads.provider.o)localObject1).b();
      bf = null;
    }
    localObject1 = ba;
    Object localObject2 = bb;
    ((Handler)localObject1).removeCallbacks((Runnable)localObject2);
    localObject1 = ao;
    if (localObject1 != null)
    {
      ((com.truecaller.ui.dialogs.g)localObject1).dismiss();
      ao = null;
    }
  }
  
  public final void onDetach()
  {
    super.onDetach();
    com.truecaller.androidactors.a locala = bd;
    if (locala != null) {
      locala.a();
    }
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    boolean bool1 = super.onOptionsItemSelected(paramMenuItem);
    boolean bool2 = true;
    if (bool1) {
      return bool2;
    }
    Object localObject1 = aa;
    if (localObject1 == null) {
      return bool2;
    }
    int k = paramMenuItem.getItemId();
    int j = 2131887197;
    int i3 = 2131887217;
    Object localObject2 = null;
    int i4 = 0;
    Object localObject3 = null;
    Object localObject4;
    int i1;
    int i2;
    switch (k)
    {
    default: 
      return false;
    case 2131361943: 
      c(bool2);
      break;
    case 2131361935: 
      boolean bool3 = aa.R() ^ bool2;
      localObject1 = new String[0];
      AssertionUtil.isTrue(bool3, (String[])localObject1);
      paramMenuItem = aa;
      bool3 = paramMenuItem.R();
      if (!bool3)
      {
        paramMenuItem = d;
        if (paramMenuItem != null)
        {
          a("share", null);
          paramMenuItem = d;
          localObject1 = Z.bB();
          localObject4 = aa;
          com.truecaller.util.w.a(paramMenuItem, (cb)localObject1, (Contact)localObject4);
        }
      }
      break;
    case 2131361933: 
      paramMenuItem = d;
      localObject1 = aa;
      com.truecaller.util.w.a(paramMenuItem, (Contact)localObject1);
      paramMenuItem = "browser";
      localObject1 = "search";
      a(paramMenuItem, (String)localObject1);
      break;
    case 2131361930: 
      paramMenuItem = aa;
      localObject1 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$3Tsj5gls2WyElFrDjNZwMoCaJoY;
      ((-..Lambda.DetailsFragment.3Tsj5gls2WyElFrDjNZwMoCaJoY)localObject1).<init>(this);
      paramMenuItem = com.truecaller.util.br.a(paramMenuItem, (br.a)localObject1);
      localObject1 = getFragmentManager().a();
      localObject4 = com.truecaller.util.br.a;
      ((android.support.v4.app.o)localObject1).a(paramMenuItem, (String)localObject4).d();
      paramMenuItem = "save";
      a(paramMenuItem, null);
      break;
    case 2131361925: 
      paramMenuItem = d;
      if (paramMenuItem != null)
      {
        localObject3 = new android/support/v7/app/AlertDialog$Builder;
        ((AlertDialog.Builder)localObject3).<init>(paramMenuItem);
        i1 = 2131886375;
        paramMenuItem = ((AlertDialog.Builder)localObject3).setTitle(i1);
        i4 = 2131886372;
        paramMenuItem = paramMenuItem.setMessage(i4);
        localObject3 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$JJJN2NWZKkAsEvHMgdraWcoQ6Rc;
        ((-..Lambda.DetailsFragment.JJJN2NWZKkAsEvHMgdraWcoQ6Rc)localObject3).<init>(this);
        paramMenuItem = paramMenuItem.setPositiveButton(i3, (DialogInterface.OnClickListener)localObject3).setNegativeButton(j, null);
        paramMenuItem.show();
      }
      break;
    case 2131361924: 
      paramMenuItem = d;
      if (paramMenuItem != null)
      {
        localObject3 = new android/support/v7/app/AlertDialog$Builder;
        ((AlertDialog.Builder)localObject3).<init>(paramMenuItem);
        i1 = 2131886374;
        paramMenuItem = ((AlertDialog.Builder)localObject3).setTitle(i1);
        i4 = 2131886370;
        paramMenuItem = paramMenuItem.setMessage(i4);
        localObject3 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$WkKgCqCjFyHIdbu3c2bUp1Sk4P4;
        ((-..Lambda.DetailsFragment.WkKgCqCjFyHIdbu3c2bUp1Sk4P4)localObject3).<init>(this);
        paramMenuItem.setPositiveButton(i3, (DialogInterface.OnClickListener)localObject3).setNegativeButton(j, null).show();
        paramMenuItem = "delete";
        a(paramMenuItem, null);
      }
      break;
    case 2131361865: 
      paramMenuItem = aa;
      boolean bool4 = paramMenuItem.X() ^ bool2;
      aa.b(bool4);
      d.supportInvalidateOptionsMenu();
      localObject1 = new com/truecaller/ui/details/DetailsFragment$d;
      localObject4 = d.getApplicationContext();
      localObject2 = aa;
      ((DetailsFragment.d)localObject1).<init>((Context)localObject4, (Contact)localObject2, bool4);
      localObject4 = new Void[0];
      com.truecaller.old.a.b.a((AsyncTask)localObject1, (Object[])localObject4);
      if (bool4) {
        i2 = 2131886337;
      } else {
        i2 = 2131886338;
      }
      localObject1 = new Object[bool2];
      localObject4 = aa.s();
      localObject1[0] = localObject4;
      paramMenuItem = getString(i2, (Object[])localObject1);
      localObject1 = d;
      paramMenuItem = Toast.makeText((Context)localObject1, paramMenuItem, 0);
      paramMenuItem.show();
      break;
    case 2131361864: 
      paramMenuItem = aa.a(bool2);
      localObject1 = d;
      n.a(paramMenuItem, (Context)localObject1);
      paramMenuItem = aa.a(false);
      localObject1 = d;
      n.a(paramMenuItem, (Context)localObject1);
      paramMenuItem = d;
      localObject1 = aa;
      paramMenuItem = com.truecaller.util.d.a(paramMenuItem, (Contact)localObject1);
      j = 21;
      com.truecaller.common.h.o.a(this, paramMenuItem, j);
      paramMenuItem = "edit";
      a(paramMenuItem, null);
      break;
    case 2131361856: 
      paramMenuItem = aa.q();
      e(paramMenuItem);
      paramMenuItem = "copy";
      localObject1 = "number";
      a(paramMenuItem, (String)localObject1);
      break;
    case 2131361855: 
      paramMenuItem = aa.t();
      e(paramMenuItem);
      paramMenuItem = "copy";
      localObject1 = "name";
      a(paramMenuItem, (String)localObject1);
      break;
    case 2131361854: 
      paramMenuItem = aa.z();
      localObject1 = aa.w();
      localObject4 = aa.q();
      String str1 = aa.a();
      Object localObject5 = aa.h();
      boolean bool5 = TextUtils.isEmpty((CharSequence)localObject5);
      if (!bool5)
      {
        localObject2 = new java/lang/StringBuilder;
        String str2 = "\"";
        ((StringBuilder)localObject2).<init>(str2);
        ((StringBuilder)localObject2).append((String)localObject5);
        localObject5 = "\"";
        ((StringBuilder)localObject2).append((String)localObject5);
        localObject2 = ((StringBuilder)localObject2).toString();
      }
      int i5 = 5;
      localObject5 = new String[i5];
      localObject5[0] = paramMenuItem;
      localObject5[bool2] = localObject1;
      localObject5[2] = localObject4;
      localObject5[3] = str1;
      i2 = 4;
      localObject5[i2] = localObject2;
      paramMenuItem = am.a((String[])localObject5);
      e(paramMenuItem);
      paramMenuItem = "copy";
      localObject1 = "contact";
      a(paramMenuItem, (String)localObject1);
      break;
    case 2131361835: 
      c();
    }
    return bool2;
  }
  
  public final void onResume()
  {
    aN = null;
    super.onResume();
  }
  
  public final void onStart()
  {
    super.onStart();
    DetailsFragment.b localb = new com/truecaller/ui/details/DetailsFragment$b;
    DetailsFragment.SourceType localSourceType = ai;
    com.truecaller.analytics.b localb1 = Z.c();
    localb.<init>(localSourceType, localb1);
    aG = localb;
  }
  
  public final void onStop()
  {
    super.onStop();
    DetailsFragment.b localb = aG;
    boolean bool = c;
    if (!bool)
    {
      com.truecaller.analytics.b localb1 = b;
      bc localbc = new com/truecaller/analytics/bc;
      String str1 = "detailView";
      String str2 = a;
      localbc.<init>(str1, str2);
      localb1.a(localbc);
      bool = true;
      c = bool;
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = Z.ao();
    aj = paramBundle;
    paramBundle = Z.aq();
    aV = paramBundle;
    paramBundle = Z.as();
    aW = paramBundle;
    paramBundle = Z.aF();
    aq = paramBundle;
    paramBundle = Z.bF();
    ak = paramBundle;
    paramBundle = Z.bG();
    aU = paramBundle;
    paramBundle = com.truecaller.referral.w.a(this, "ReferralManagerImpl");
    aI = paramBundle;
    paramBundle = aI;
    boolean bool1 = true;
    if (paramBundle != null)
    {
      localObject1 = ReferralManager.ReferralLaunchContext.CONTACT_DETAILS;
      bool2 = paramBundle.c((ReferralManager.ReferralLaunchContext)localObject1);
      if (bool2)
      {
        bool2 = true;
        break label151;
      }
    }
    boolean bool2 = false;
    paramBundle = null;
    label151:
    aJ = bool2;
    paramBundle = paramView.findViewById(2131362828);
    e = paramBundle;
    paramBundle = (DetailsHeaderView)paramView.findViewById(2131362829);
    f = paramBundle;
    paramBundle = (DetailsActionBar)paramView.findViewById(2131361822);
    g = paramBundle;
    paramBundle = paramView.findViewById(2131362345);
    h = paramBundle;
    paramBundle = requireContext();
    Object localObject2 = requireContext();
    int i1 = 2131100398;
    int i2 = android.support.v4.content.b.c((Context)localObject2, i1);
    paramBundle = at.a(paramBundle, 2131234408, i2);
    Y = paramBundle;
    paramBundle = paramView.findViewById(2131363970);
    i = paramBundle;
    paramBundle = (TextView)i.findViewById(2131363975);
    l = paramBundle;
    paramBundle = (ViewGroup)paramView.findViewById(2131362389);
    m = paramBundle;
    paramBundle = (ViewGroup)paramView.findViewById(2131362394);
    n = paramBundle;
    paramBundle = (ViewGroup)paramView.findViewById(2131362391);
    o = paramBundle;
    paramBundle = (ViewGroup)paramView.findViewById(2131362393);
    p = paramBundle;
    paramBundle = paramView.findViewById(2131362392);
    q = paramBundle;
    paramBundle = (ViewGroup)paramView.findViewById(2131362395);
    s = paramBundle;
    paramBundle = paramView.findViewById(2131362396);
    u = paramBundle;
    paramBundle = (ViewGroup)paramView.findViewById(2131362386);
    r = paramBundle;
    paramBundle = paramView.findViewById(2131362387);
    t = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131363310);
    v = paramBundle;
    paramBundle = paramView.findViewById(2131363571);
    w = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131363570);
    x = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131363572);
    y = paramBundle;
    paramBundle = y;
    Object localObject1 = Y;
    i2 = 0;
    localObject2 = null;
    paramBundle.setCompoundDrawablesWithIntrinsicBounds((Drawable)localObject1, null, null, null);
    paramBundle = (TextView)paramView.findViewById(2131363816);
    z = paramBundle;
    paramBundle = paramView.findViewById(2131365410);
    A = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131361812);
    B = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131361810);
    C = paramBundle;
    paramBundle = C;
    localObject1 = Y;
    paramBundle.setCompoundDrawablesWithIntrinsicBounds((Drawable)localObject1, null, null, null);
    paramBundle = (TextView)paramView.findViewById(2131363773);
    D = paramBundle;
    paramBundle = (AdsSwitchView)paramView.findViewById(2131362827);
    E = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131363790);
    G = paramBundle;
    paramBundle = paramView.findViewById(2131364099);
    J = paramBundle;
    J.setOnClickListener(this);
    paramBundle = paramView.findViewById(2131364612);
    M = paramBundle;
    paramBundle = (CardView)paramView.findViewById(2131362269);
    N = paramBundle;
    paramBundle = (RecyclerView)paramView.findViewById(2131363943);
    O = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131361808);
    P = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131362270);
    Q = paramBundle;
    paramBundle = paramView.findViewById(2131363832);
    R = paramBundle;
    paramBundle = (RelativeLayout)paramView.findViewById(2131363837);
    S = paramBundle;
    paramBundle = (RecyclerView)paramView.findViewById(2131363836);
    T = paramBundle;
    paramBundle = (TintedImageView)paramView.findViewById(2131363009);
    U = paramBundle;
    paramBundle = (ShineView)paramView.findViewById(2131363196);
    W = paramBundle;
    W.setLifecycleOwner(this);
    paramBundle = paramView.findViewById(2131363969);
    X = paramBundle;
    int j = Build.VERSION.SDK_INT;
    int i3 = 21;
    if (j < i3)
    {
      j = 2131363988;
      paramBundle = (TintedImageView)paramView.findViewById(j);
      localObject1 = requireContext();
      i2 = 2131100364;
      i3 = android.support.v4.content.b.c((Context)localObject1, i2);
      paramBundle.setTint(i3);
    }
    paramBundle = d;
    i3 = 2130969592;
    j = com.truecaller.utils.ui.b.a(paramBundle, i3);
    aY = j;
    paramBundle = new com/truecaller/ui/details/g;
    localObject1 = requireContext();
    localObject2 = aq.y();
    boolean bool4 = ((com.truecaller.featuretoggles.b)localObject2).a();
    paramBundle.<init>((Context)localObject1, bool4);
    bh = paramBundle;
    j = 2131364907;
    paramBundle = (Toolbar)paramView.findViewById(j);
    L = paramBundle;
    paramBundle = d;
    localObject1 = L;
    paramBundle.setSupportActionBar((Toolbar)localObject1);
    paramBundle = d.getSupportActionBar();
    if (paramBundle != null)
    {
      paramBundle.setDisplayShowTitleEnabled(false);
      paramBundle.setDisplayHomeAsUpEnabled(bool1);
      paramBundle.setDisplayShowHomeEnabled(bool1);
    }
    paramBundle = (AppBarLayout)paramView.findViewById(2131362037);
    K = paramBundle;
    K.a(this);
    j = 2131364927;
    paramView = (TextView)paramView.findViewById(j);
    F = paramView;
    paramView = F.getViewTreeObserver();
    paramBundle = new com/truecaller/ui/details/DetailsFragment$4;
    paramBundle.<init>(this);
    paramView.addOnPreDrawListener(paramBundle);
    paramView = ae;
    if (paramView == null)
    {
      paramView = new com/truecaller/ui/details/DetailsFragment$c;
      paramView.<init>(this);
      ae = paramView;
      paramView = d.getContentResolver();
      paramBundle = TruecallerContract.b;
      localObject1 = ae;
      paramView.registerContentObserver(paramBundle, bool1, (ContentObserver)localObject1);
    }
    i.setOnClickListener(this);
    g.setEventListener(this);
    paramView = Z.P();
    ag = paramView;
    paramView = Z.f();
    ah = paramView;
    paramView = Z.aa();
    aK = paramView;
    paramView = Z.ad();
    aM = paramView;
    paramView = Z.K();
    ap = paramView;
    paramView = Z.m().a();
    aL = paramView;
    paramView = Z.V();
    aP = paramView;
    paramView = Z.bo();
    aQ = paramView;
    paramView = Z.bM();
    aS = paramView;
    paramView = Z.bz();
    aR = paramView;
    paramView = new com/truecaller/ui/details/DetailsFragment$5;
    paramBundle = ag;
    paramView.<init>(this, this, paramBundle);
    af = paramView;
    paramView = Z.cc();
    aT = paramView;
    f.setOnTagClickListener(this);
    f.setOnAddNameClickListener(this);
    f.setOnSuggestNameButtonClickListener(this);
    paramView = aj;
    if (paramView != null)
    {
      paramView = aq.ag().e();
      paramBundle = "detailView";
      boolean bool6 = paramView.contains(paramBundle);
      if (bool6)
      {
        paramView = E.findViewById(2131362001);
        j = 2131362825;
        paramView = paramView.findViewById(j);
        paramView.setVisibility(0);
        localObject1 = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$-fXu2yIEAfu1x96XxPIpWJojjuM;
        ((-..Lambda.DetailsFragment.-fXu2yIEAfu1x96XxPIpWJojjuM)localObject1).<init>(this);
        paramView.setOnClickListener((View.OnClickListener)localObject1);
        paramView = E;
        i3 = 2131362000;
        paramView = paramView.findViewById(i3).findViewById(j);
        paramView.setVisibility(0);
        paramBundle = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$9hcTzxoehEPvrYr5_7QEF4L04JE;
        paramBundle.<init>(this);
        paramView.setOnClickListener(paramBundle);
      }
    }
    paramView = d.getIntent();
    ar = false;
    paramBundle = DetailsFragment.SourceType.values();
    localObject1 = "ARG_SOURCE_TYPE";
    i3 = paramView.getIntExtra((String)localObject1, 0);
    paramBundle = paramBundle[i3];
    ai = paramBundle;
    paramView.removeExtra("ARG_SOURCE_TYPE");
    boolean bool3 = paramView.getBooleanExtra("SHOULD_SAVE", false);
    ay = bool3;
    paramView.removeExtra("SHOULD_SAVE");
    bool3 = paramView.getBooleanExtra("SHOULD_FETCH_MORE_IF_NEEDED", false);
    az = bool3;
    paramView.removeExtra("SHOULD_FETCH_MORE_IF_NEEDED");
    bool3 = paramView.getBooleanExtra("SHOULD_FORCE_SEARCH", false);
    aC = bool3;
    paramView.removeExtra("SHOULD_FORCE_SEARCH");
    paramBundle = "ARG_CONTACT";
    bool3 = paramView.hasExtra(paramBundle);
    Object localObject3;
    if (bool3)
    {
      paramBundle = (Contact)paramView.getParcelableExtra("ARG_CONTACT");
      if (paramBundle != null)
      {
        boolean bool5 = az;
        if (!bool5)
        {
          localObject1 = paramBundle.getTcId();
          if (localObject1 != null)
          {
            bool1 = false;
            localObject3 = null;
          }
        }
        az = bool1;
        localObject3 = com.truecaller.util.w.b(paramBundle);
        if (localObject3 != null)
        {
          localObject1 = ((Number)localObject3).d();
          ac = ((String)localObject1);
          localObject3 = ((Number)localObject3).a();
          ab = ((String)localObject3);
        }
        a(paramBundle);
        a(paramBundle, false);
        localObject3 = new com/truecaller/ui/details/DetailsFragment$k;
        ((DetailsFragment.k)localObject3).<init>(this, paramBundle);
      }
    }
    else
    {
      paramBundle = "ARG_TC_ID";
      bool3 = paramView.hasExtra(paramBundle);
      if (bool3)
      {
        String str1 = paramView.getStringExtra("ARG_TC_ID");
        paramBundle = "NAME";
        String str2 = paramView.getStringExtra(paramBundle);
        bool3 = az;
        if ((!bool3) && (str1 != null))
        {
          bool1 = false;
          localObject3 = null;
        }
        az = bool1;
        paramBundle = paramView.getStringExtra("NORMALIZED_NUMBER");
        ab = paramBundle;
        paramBundle = paramView.getStringExtra("RAW_NUMBER");
        ac = paramBundle;
        paramBundle = paramView.getStringExtra("COUNTRY_CODE");
        ad = paramBundle;
        localObject1 = new com/truecaller/ui/details/DetailsFragment$e;
        String str3 = ab;
        String str4 = ac;
        String str5 = ad;
        localObject2 = this;
        ((DetailsFragment.e)localObject1).<init>(this, str1, str2, str3, str4, str5);
      }
      else
      {
        int k = 2131886586;
        b(k);
        paramBundle = d;
        paramBundle.finish();
      }
    }
    int i4 = paramView.getIntExtra("SEARCH_TYPE", 10);
    aZ = i4;
  }
  
  public final void p()
  {
    boolean bool1 = s();
    if (bool1)
    {
      bool1 = isAdded();
      if (bool1)
      {
        Object localObject1 = aa;
        if (localObject1 != null)
        {
          localObject1 = ba;
          Object localObject2 = bb;
          ((Handler)localObject1).removeCallbacks((Runnable)localObject2);
          localObject1 = ba;
          localObject2 = bb;
          long l1 = 1000L;
          ((Handler)localObject1).postDelayed((Runnable)localObject2, l1);
          localObject1 = e;
          boolean bool2 = ar;
          boolean bool4 = false;
          Object localObject3 = null;
          int j;
          if (bool2)
          {
            bool2 = false;
            localObject2 = null;
          }
          else
          {
            j = 8;
          }
          ((View)localObject1).setVisibility(j);
          localObject1 = aa;
          boolean bool3 = av;
          a((Contact)localObject1, bool3);
          bool1 = aH;
          if (!bool1)
          {
            localObject1 = bf;
            if (localObject1 == null)
            {
              localObject1 = aa;
              bool1 = ((Contact)localObject1).R();
              bool3 = false;
              localObject2 = null;
              if (!bool1)
              {
                bool1 = ar;
                if (bool1)
                {
                  localObject1 = aa;
                  bool1 = ((Contact)localObject1).O();
                  if (bool1)
                  {
                    localObject1 = ((Number)aa.A().get(0)).a();
                  }
                  else
                  {
                    bool1 = false;
                    localObject1 = null;
                  }
                  bool4 = TextUtils.isEmpty((CharSequence)localObject1);
                  if (bool4) {
                    localObject1 = ab;
                  }
                  bool4 = TextUtils.isEmpty((CharSequence)localObject1);
                  if (bool4) {
                    localObject1 = aa.b(ac);
                  }
                  bool4 = TextUtils.isEmpty((CharSequence)localObject1);
                  if (bool4)
                  {
                    a(null, null);
                    return;
                  }
                  localObject3 = aa.J();
                  boolean bool5 = ((List)localObject3).isEmpty();
                  if (!bool5)
                  {
                    localObject2 = new java/util/ArrayList;
                    int k = ((List)localObject3).size();
                    ((ArrayList)localObject2).<init>(k);
                    localObject3 = ((List)localObject3).iterator();
                    for (;;)
                    {
                      boolean bool6 = ((Iterator)localObject3).hasNext();
                      if (!bool6) {
                        break;
                      }
                      String str = ((Tag)((Iterator)localObject3).next()).a();
                      boolean bool7 = TextUtils.isEmpty(str);
                      if (!bool7) {
                        ((List)localObject2).add(str);
                      }
                    }
                  }
                  a((String)localObject1, (List)localObject2);
                  return;
                }
              }
              a(null, null);
              return;
            }
          }
          return;
        }
      }
    }
  }
  
  public final void q()
  {
    DetailsFragment.f localf = new com/truecaller/ui/details/DetailsFragment$f;
    Contact localContact = aa;
    localf.<init>(this, localContact);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsFragment
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */