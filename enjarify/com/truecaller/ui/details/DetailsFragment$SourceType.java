package com.truecaller.ui.details;

public enum DetailsFragment$SourceType
{
  static
  {
    Object localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    ((SourceType)localObject).<init>("SearchResult", 0);
    SearchResult = (SourceType)localObject;
    localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    int i = 1;
    ((SourceType)localObject).<init>("AfterCall", i);
    AfterCall = (SourceType)localObject;
    localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    int j = 2;
    ((SourceType)localObject).<init>("CallLog", j);
    CallLog = (SourceType)localObject;
    localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    int k = 3;
    ((SourceType)localObject).<init>("Contacts", k);
    Contacts = (SourceType)localObject;
    localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    int m = 4;
    ((SourceType)localObject).<init>("SearchHistory", m);
    SearchHistory = (SourceType)localObject;
    localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    int n = 5;
    ((SourceType)localObject).<init>("Notification", n);
    Notification = (SourceType)localObject;
    localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    int i1 = 6;
    ((SourceType)localObject).<init>("ClipboardSearch", i1);
    ClipboardSearch = (SourceType)localObject;
    localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    int i2 = 7;
    ((SourceType)localObject).<init>("SpammersList", i2);
    SpammersList = (SourceType)localObject;
    localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    int i3 = 8;
    ((SourceType)localObject).<init>("External", i3);
    External = (SourceType)localObject;
    localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    int i4 = 9;
    ((SourceType)localObject).<init>("MissedCallReminder", i4);
    MissedCallReminder = (SourceType)localObject;
    localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    int i5 = 10;
    ((SourceType)localObject).<init>("Conversation", i5);
    Conversation = (SourceType)localObject;
    localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    int i6 = 11;
    ((SourceType)localObject).<init>("BlockedEvents", i6);
    BlockedEvents = (SourceType)localObject;
    localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    int i7 = 12;
    ((SourceType)localObject).<init>("DeepLink", i7);
    DeepLink = (SourceType)localObject;
    localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    ((SourceType)localObject).<init>("ScannedNumber", 13);
    ScannedNumber = (SourceType)localObject;
    localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    ((SourceType)localObject).<init>("TruecallerContacts", 14);
    TruecallerContacts = (SourceType)localObject;
    localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    ((SourceType)localObject).<init>("EssentialNumber", 15);
    EssentialNumber = (SourceType)localObject;
    localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    ((SourceType)localObject).<init>("WhoViewedMe", 16);
    WhoViewedMe = (SourceType)localObject;
    localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    ((SourceType)localObject).<init>("CallRecording", 17);
    CallRecording = (SourceType)localObject;
    localObject = new com/truecaller/ui/details/DetailsFragment$SourceType;
    ((SourceType)localObject).<init>("BulkSmsReferral", 18);
    BulkSmsReferral = (SourceType)localObject;
    localObject = new SourceType[19];
    SourceType localSourceType = SearchResult;
    localObject[0] = localSourceType;
    localSourceType = AfterCall;
    localObject[i] = localSourceType;
    localSourceType = CallLog;
    localObject[j] = localSourceType;
    localSourceType = Contacts;
    localObject[k] = localSourceType;
    localSourceType = SearchHistory;
    localObject[m] = localSourceType;
    localSourceType = Notification;
    localObject[n] = localSourceType;
    localSourceType = ClipboardSearch;
    localObject[i1] = localSourceType;
    localSourceType = SpammersList;
    localObject[i2] = localSourceType;
    localSourceType = External;
    localObject[i3] = localSourceType;
    localSourceType = MissedCallReminder;
    localObject[i4] = localSourceType;
    localSourceType = Conversation;
    localObject[i5] = localSourceType;
    localSourceType = BlockedEvents;
    localObject[i6] = localSourceType;
    localSourceType = DeepLink;
    localObject[i7] = localSourceType;
    localSourceType = ScannedNumber;
    localObject[13] = localSourceType;
    localSourceType = TruecallerContacts;
    localObject[14] = localSourceType;
    localSourceType = EssentialNumber;
    localObject[15] = localSourceType;
    localSourceType = WhoViewedMe;
    localObject[16] = localSourceType;
    localSourceType = CallRecording;
    localObject[17] = localSourceType;
    localSourceType = BulkSmsReferral;
    localObject[18] = localSourceType;
    $VALUES = (SourceType[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsFragment.SourceType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */