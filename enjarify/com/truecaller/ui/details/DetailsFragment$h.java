package com.truecaller.ui.details;

import android.content.Context;
import com.truecaller.data.access.m;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.old.a.a;
import com.truecaller.search.local.model.g;

final class DetailsFragment$h
  extends a
{
  private final Context c;
  private final Contact d;
  
  DetailsFragment$h(DetailsFragment paramDetailsFragment, Context paramContext, Contact paramContact)
  {
    c = paramContext;
    d = paramContact;
  }
  
  public final void a(Object paramObject)
  {
    DetailsFragment localDetailsFragment = a;
    boolean bool1 = localDetailsFragment.s();
    if (!bool1) {
      return;
    }
    DetailsFragment.i(a);
    paramObject = (Boolean)paramObject;
    boolean bool2 = ((Boolean)paramObject).booleanValue();
    localDetailsFragment = a;
    int i;
    if (bool2) {
      i = 2131886373;
    } else {
      i = 2131886371;
    }
    DetailsFragment.b(localDetailsFragment, i);
  }
  
  protected final Object doInBackground(Object... paramVarArgs)
  {
    paramVarArgs = Boolean.FALSE;
    Object localObject1 = d;
    boolean bool1 = ((Contact)localObject1).Z();
    int i = 1;
    Object localObject2;
    boolean bool2;
    if (bool1)
    {
      paramVarArgs = c;
      localObject1 = d.E();
      localObject2 = d.F();
      if (localObject1 != null)
      {
        paramVarArgs = g.a(paramVarArgs);
        long l = ((Long)localObject1).longValue();
        bool2 = paramVarArgs.a(l, (String)localObject2);
        if (bool2)
        {
          bool2 = true;
          break label84;
        }
      }
      bool2 = false;
      paramVarArgs = null;
      label84:
      paramVarArgs = Boolean.valueOf(bool2);
    }
    localObject1 = d;
    bool1 = ((Contact)localObject1).Y();
    if (bool1)
    {
      localObject1 = new com/truecaller/data/access/m;
      localObject2 = c;
      ((m)localObject1).<init>((Context)localObject2);
      localObject2 = d.r();
      if (localObject2 != null)
      {
        paramVarArgs = ((Number)localObject2).a();
        int[] arrayOfInt = new int[i];
        int j = 32;
        arrayOfInt[0] = j;
        bool2 = ((m)localObject1).a(paramVarArgs, arrayOfInt);
        paramVarArgs = Boolean.valueOf(bool2);
      }
    }
    return paramVarArgs;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsFragment.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */