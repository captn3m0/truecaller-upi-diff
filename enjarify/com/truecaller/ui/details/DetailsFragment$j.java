package com.truecaller.ui.details;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.truecaller.data.entity.e;
import com.truecaller.ui.components.CallerButtonBase;

final class DetailsFragment$j
  extends AsyncTask
{
  private DetailsFragment$j(DetailsFragment paramDetailsFragment) {}
  
  private CallerButtonBase a(e parame, int paramInt)
  {
    CallerButtonBase localCallerButtonBase = new com/truecaller/ui/components/CallerButtonBase;
    Object localObject = DetailsFragment.b(a);
    localCallerButtonBase.<init>((Context)localObject);
    localCallerButtonBase.setShowFullDivider(false);
    localObject = a;
    localCallerButtonBase.setHeadingText((CharSequence)localObject);
    localCallerButtonBase.setDetailsText(null);
    localObject = b;
    localCallerButtonBase.setLeftImage((Drawable)localObject);
    localObject = localCallerButtonBase.getLeftImage();
    ImageView.ScaleType localScaleType = ImageView.ScaleType.CENTER_INSIDE;
    ((ImageView)localObject).setScaleType(localScaleType);
    localCallerButtonBase.getLeftImage().setPadding(paramInt, paramInt, paramInt, paramInt);
    -..Lambda.DetailsFragment.j.mx1pvq7uchkI4nbbq07OGvXqS1U localmx1pvq7uchkI4nbbq07OGvXqS1U = new com/truecaller/ui/details/-$$Lambda$DetailsFragment$j$mx1pvq7uchkI4nbbq07OGvXqS1U;
    localmx1pvq7uchkI4nbbq07OGvXqS1U.<init>(this, parame);
    localCallerButtonBase.setOnClickListener(localmx1pvq7uchkI4nbbq07OGvXqS1U);
    DetailsFragment.k(a).addView(localCallerButtonBase);
    return localCallerButtonBase;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsFragment.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */