package com.truecaller.ui.details;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.data.entity.e;
import java.util.List;

final class j$a
  extends BaseAdapter
{
  private final List a;
  
  public j$a(List paramList)
  {
    a = paramList;
  }
  
  public final int getCount()
  {
    return a.size();
  }
  
  public final Object getItem(int paramInt)
  {
    return a.get(paramInt);
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    int i = 2131364664;
    if (paramView != null)
    {
      paramView = (j.b)paramView.getTag(i);
    }
    else
    {
      paramView = new com/truecaller/ui/details/j$b;
      LayoutInflater localLayoutInflater = LayoutInflater.from(paramViewGroup.getContext());
      int j = 2131559041;
      paramViewGroup = localLayoutInflater.inflate(j, paramViewGroup, false);
      paramView.<init>(paramViewGroup);
      paramViewGroup = a;
      paramViewGroup.setTag(i, paramView);
    }
    Object localObject = (e)a.get(paramInt);
    paramViewGroup = b;
    Drawable localDrawable = b;
    paramViewGroup.setImageDrawable(localDrawable);
    paramViewGroup = c;
    localObject = a;
    paramViewGroup.setText((CharSequence)localObject);
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.j.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */