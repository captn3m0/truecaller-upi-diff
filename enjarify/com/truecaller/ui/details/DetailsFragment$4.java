package com.truecaller.ui.details;

import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.TextView;

final class DetailsFragment$4
  implements ViewTreeObserver.OnPreDrawListener
{
  DetailsFragment$4(DetailsFragment paramDetailsFragment) {}
  
  public final boolean onPreDraw()
  {
    DetailsFragment.e(a).getViewTreeObserver().removeOnPreDrawListener(this);
    DetailsFragment localDetailsFragment = a;
    TextView localTextView = DetailsFragment.e(localDetailsFragment);
    int i = DetailsFragment.a(localDetailsFragment, localTextView);
    int j = DetailsFragment.e(a).getHeight() / 2;
    i += j;
    DetailsFragment.a(localDetailsFragment, i);
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsFragment.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */