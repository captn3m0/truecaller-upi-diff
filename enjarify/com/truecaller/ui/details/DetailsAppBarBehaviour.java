package com.truecaller.ui.details;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import c.a.m;
import c.g.b.k;
import com.truecaller.ui.components.FlingBehavior;
import com.truecaller.voip.incall.ui.VoipInAppNotificationView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class DetailsAppBarBehaviour
  extends FlingBehavior
{
  public DetailsAppBarBehaviour() {}
  
  public DetailsAppBarBehaviour(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  private static boolean a(AppBarLayout paramAppBarLayout, VoipInAppNotificationView paramVoipInAppNotificationView)
  {
    ViewGroup.LayoutParams localLayoutParams = paramAppBarLayout.getLayoutParams();
    if (localLayoutParams != null)
    {
      boolean bool = localLayoutParams instanceof ViewGroup.MarginLayoutParams;
      if (bool)
      {
        int i = paramVoipInAppNotificationView.getVisibility();
        int j = 8;
        int k;
        if (i == j)
        {
          k = 0;
          paramVoipInAppNotificationView = null;
        }
        else
        {
          k = paramVoipInAppNotificationView.getHeight();
        }
        Object localObject = localLayoutParams;
        localObject = (ViewGroup.MarginLayoutParams)localLayoutParams;
        j = topMargin;
        if (j == k) {
          return false;
        }
        topMargin = k;
        paramAppBarLayout.setLayoutParams((ViewGroup.LayoutParams)localLayoutParams);
        return true;
      }
    }
    return false;
  }
  
  public final boolean a(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, int paramInt)
  {
    k.b(paramCoordinatorLayout, "parent");
    k.b(paramAppBarLayout, "abl");
    Object localObject1 = paramAppBarLayout;
    localObject1 = (View)paramAppBarLayout;
    localObject1 = paramCoordinatorLayout.b((View)localObject1);
    k.a(localObject1, "parent.getDependencies(abl)");
    localObject1 = (Iterable)localObject1;
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (Collection)localObject2;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      Object localObject3 = ((Iterator)localObject1).next();
      boolean bool2 = localObject3 instanceof VoipInAppNotificationView;
      if (bool2) {
        ((Collection)localObject2).add(localObject3);
      }
    }
    localObject2 = (List)localObject2;
    localObject1 = (VoipInAppNotificationView)m.e((List)localObject2);
    if (localObject1 == null) {
      return super.a(paramCoordinatorLayout, paramAppBarLayout, paramInt);
    }
    int i = ((VoipInAppNotificationView)localObject1).getVisibility();
    if (i == 0)
    {
      ((VoipInAppNotificationView)localObject1).f();
      ((VoipInAppNotificationView)localObject1).bringToFront();
    }
    a(paramAppBarLayout, (VoipInAppNotificationView)localObject1);
    return super.a(paramCoordinatorLayout, paramAppBarLayout, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsAppBarBehaviour
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */