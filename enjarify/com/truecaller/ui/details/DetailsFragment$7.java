package com.truecaller.ui.details;

import android.content.Intent;
import android.support.v4.app.f;
import com.truecaller.ui.f.a;

final class DetailsFragment$7
  implements f.a
{
  DetailsFragment$7(DetailsFragment paramDetailsFragment) {}
  
  private void a(boolean paramBoolean)
  {
    f localf = a.getActivity();
    if (localf != null)
    {
      Intent localIntent1 = localf.getIntent();
      long l1 = -1;
      long l2 = localIntent1.getLongExtra("CONVERSATION_ID", l1);
      int i = -1;
      Intent localIntent2 = new android/content/Intent;
      localIntent2.<init>();
      String str1 = "CONVERSATION_ID";
      localIntent1 = localIntent2.putExtra(str1, l2);
      String str2 = "RESULT_NUMBER_BLOCKED";
      Intent localIntent3 = localIntent1.putExtra(str2, paramBoolean);
      localf.setResult(i, localIntent3);
    }
  }
  
  public final void a()
  {
    a(true);
  }
  
  public final void b()
  {
    a(false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsFragment.7
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */