package com.truecaller.ui.details;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.f.c;
import com.truecaller.data.entity.Contact;
import com.truecaller.util.at;
import com.truecaller.voip.ai;
import com.truecaller.voip.util.ak;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DetailsActionBar
  extends LinearLayout
  implements View.OnClickListener
{
  private final View a;
  private final View b;
  private final List c;
  private final LayoutInflater d;
  private DetailsActionBar.a e;
  private h f;
  private final int g;
  private final c h;
  private final com.truecaller.flashsdk.core.b i;
  private final ai j;
  private final ak k;
  private Runnable l;
  
  public DetailsActionBar(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private DetailsActionBar(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet, 0);
    paramAttributeSet = new java/util/ArrayList;
    paramAttributeSet.<init>();
    c = paramAttributeSet;
    l = null;
    paramContext = LayoutInflater.from(paramContext);
    d = paramContext;
    d.inflate(2131559177, this);
    paramContext = findViewById(2131362557);
    a = paramContext;
    paramContext = findViewById(2131363175);
    b = paramContext;
    a.setOnClickListener(this);
    b.setOnClickListener(this);
    int m = com.truecaller.utils.ui.b.a(getContext(), 2130969585);
    g = m;
    paramContext = ((bk)getContext().getApplicationContext()).a();
    paramAttributeSet = paramContext.ai();
    h = paramAttributeSet;
    paramAttributeSet = paramContext.aV();
    i = paramAttributeSet;
    paramAttributeSet = paramContext.cc();
    j = paramAttributeSet;
    paramContext = paramContext.cd();
    k = paramContext;
  }
  
  private TextView a(int paramInt1, int paramInt2, int paramInt3)
  {
    int m = f.a;
    return a(paramInt1, paramInt2, m, paramInt3);
  }
  
  private TextView a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    Object localObject = d;
    int m = 2131559176;
    localObject = (TextView)((LayoutInflater)localObject).inflate(m, this, false);
    ((TextView)localObject).setOnClickListener(this);
    Integer localInteger = Integer.valueOf(paramInt4);
    ((TextView)localObject).setTag(localInteger);
    ((TextView)localObject).setText(paramInt1);
    ((TextView)localObject).setTextColor(paramInt3);
    Drawable localDrawable = android.support.v4.content.b.a(getContext(), paramInt2);
    if (localDrawable != null)
    {
      localDrawable = localDrawable.mutate();
      PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
      localDrawable.setColorFilter(paramInt3, localMode);
      paramInt2 = 0;
      localMode = null;
      ((TextView)localObject).setCompoundDrawablesWithIntrinsicBounds(null, localDrawable, null, null);
    }
    return (TextView)localObject;
  }
  
  private void a()
  {
    int m = 11;
    Object localObject = Integer.valueOf(m);
    localObject = findViewWithTag(localObject);
    if (localObject != null)
    {
      ak localak = k;
      Activity localActivity = (Activity)getContext();
      -..Lambda.DetailsActionBar._mRRkgL1a5WyCNSJrhIT--CPO6g local_mRRkgL1a5WyCNSJrhIT--CPO6g = new com/truecaller/ui/details/-$$Lambda$DetailsActionBar$_mRRkgL1a5WyCNSJrhIT--CPO6g;
      local_mRRkgL1a5WyCNSJrhIT--CPO6g.<init>(this);
      localak.a(localActivity, (View)localObject, local_mRRkgL1a5WyCNSJrhIT--CPO6g);
    }
  }
  
  private void a(List paramList, boolean paramBoolean)
  {
    Object localObject1 = k;
    boolean bool1 = ((ak)localObject1).a();
    if (bool1)
    {
      localObject1 = new com/truecaller/ui/details/-$$Lambda$DetailsActionBar$7YAsuEBdC-Hs5MvqfIHdRsWXgyw;
      ((-..Lambda.DetailsActionBar.7YAsuEBdC-Hs5MvqfIHdRsWXgyw)localObject1).<init>(this, paramList, paramBoolean);
      l = ((Runnable)localObject1);
      return;
    }
    if (!paramBoolean)
    {
      localList = c;
      paramBoolean = localList.containsAll(paramList);
      if (paramBoolean)
      {
        localList = c;
        paramBoolean = paramList.containsAll(localList);
        if (paramBoolean) {
          return;
        }
      }
    }
    removeAllViews();
    c.clear();
    paramBoolean = false;
    List localList = null;
    for (;;)
    {
      boolean bool2 = paramList.size();
      if (paramBoolean >= bool2) {
        break;
      }
      int m = ((Integer)paramList.get(paramBoolean)).intValue();
      TextView localTextView = b(m);
      Object localObject2 = Integer.valueOf(m);
      localTextView.setTag(localObject2);
      localObject2 = c;
      localObject1 = Integer.valueOf(m);
      ((List)localObject2).add(localObject1);
      addView(localTextView, paramBoolean);
      paramBoolean += true;
    }
    a();
  }
  
  private TextView b(int paramInt)
  {
    int m = 2131233879;
    switch (paramInt)
    {
    case 3: 
    case 7: 
    case 8: 
    case 9: 
    default: 
      IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
      String str = String.valueOf(paramInt);
      str = "Unsupported button ".concat(str);
      localIllegalStateException.<init>(str);
      throw localIllegalStateException;
    case 11: 
      return a(2131887305, 2131234671, paramInt);
    case 10: 
      return a(2131886788, 2131234364, paramInt);
    case 6: 
      return a(2131886105, 2131234313, paramInt);
    case 5: 
      int n = g;
      return a(2131886115, m, n, paramInt);
    case 4: 
      return a(2131886094, m, paramInt);
    case 2: 
      return a(2131888106, 2131234137, paramInt);
    case 1: 
      return a(2131887768, 2131234560, paramInt);
    }
    return a(2131886319, 2131233928, paramInt);
  }
  
  public final void a(int paramInt)
  {
    List localList = c;
    Object localObject1 = Integer.valueOf(paramInt);
    int m = localList.indexOf(localObject1);
    int n = -1;
    if (m != n)
    {
      localObject1 = c;
      ((List)localObject1).remove(m);
      removeViewAt(m);
    }
    Object localObject2 = Integer.valueOf(paramInt);
    localObject2 = findViewWithTag(localObject2);
    if (localObject2 != null) {
      removeView((View)localObject2);
    }
  }
  
  public final void a(Contact paramContact, h paramh, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5)
  {
    DetailsActionBar localDetailsActionBar = this;
    h localh = paramh;
    setVisibility(0);
    boolean bool1 = paramContact.R();
    Object localObject1 = h;
    boolean bool2 = ((c)localObject1).d();
    View localView = a;
    int m = a;
    localView.setBackgroundColor(m);
    localView = a;
    m = 1;
    if ((bool1) && (bool2)) {
      bool4 = true;
    } else {
      bool4 = false;
    }
    at.a(localView, bool4);
    localView = b;
    if ((!bool1) || (bool2)) {
      m = 0;
    }
    at.a(localView, m);
    if (bool1)
    {
      localObject2 = Collections.emptyList();
      a((List)localObject2, false);
      f = localh;
      return;
    }
    ai localai = j;
    -..Lambda.DetailsActionBar.TyRjfXDXYcvYKQUgF0l5PNSB3vs localTyRjfXDXYcvYKQUgF0l5PNSB3vs = new com/truecaller/ui/details/-$$Lambda$DetailsActionBar$TyRjfXDXYcvYKQUgF0l5PNSB3vs;
    Object localObject2 = this;
    localObject1 = paramContact;
    boolean bool3 = paramBoolean3;
    boolean bool4 = paramBoolean2;
    localh = paramh;
    localTyRjfXDXYcvYKQUgF0l5PNSB3vs.<init>(this, paramContact, paramBoolean4, paramBoolean3, paramBoolean2, paramBoolean1, paramBoolean5, paramh);
    localai.a(paramContact, localTyRjfXDXYcvYKQUgF0l5PNSB3vs);
  }
  
  public void onClick(View paramView)
  {
    DetailsActionBar.a locala = e;
    if (locala == null) {
      return;
    }
    int m = paramView.getId();
    int n = 2131362557;
    if (m != n)
    {
      n = 2131363175;
      if (m != n)
      {
        int i1 = ((Integer)paramView.getTag()).intValue();
        e.a(i1);
        return;
      }
      e.a(7);
      return;
    }
    e.a(8);
  }
  
  public void setEventListener(DetailsActionBar.a parama)
  {
    e = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsActionBar
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */