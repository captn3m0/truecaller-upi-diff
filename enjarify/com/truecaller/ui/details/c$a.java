package com.truecaller.ui.details;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import java.util.HashMap;
import kotlinx.a.a.a;

public final class c$a
  extends RecyclerView.ViewHolder
  implements a
{
  private final View b;
  private HashMap c;
  
  public c$a(c paramc, View paramView)
  {
    super(paramView);
    b = paramView;
  }
  
  public final View a()
  {
    return b;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */