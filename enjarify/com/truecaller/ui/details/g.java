package com.truecaller.ui.details;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import c.g.b.k;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Style;
import com.truecaller.log.AssertionUtil;
import com.truecaller.util.at;

public final class g
  implements f
{
  private final Context a;
  private final boolean b;
  
  public g(Context paramContext, boolean paramBoolean)
  {
    a = paramContext;
    b = paramBoolean;
  }
  
  private final e a()
  {
    e locale = new com/truecaller/ui/details/e;
    int i = com.truecaller.utils.ui.b.a(a, 2130969586);
    Object localObject1 = new android/graphics/drawable/ColorDrawable;
    Object localObject2 = a;
    int j = 2130969548;
    int k = com.truecaller.utils.ui.b.a((Context)localObject2, j);
    ((ColorDrawable)localObject1).<init>(k);
    localObject2 = localObject1;
    localObject2 = (Drawable)localObject1;
    localObject1 = a;
    int m = 2130969592;
    int n = com.truecaller.utils.ui.b.a((Context)localObject1, m);
    i locali = new com/truecaller/ui/details/i;
    int i1 = com.truecaller.utils.ui.b.a(a, 2130969591);
    int i2 = com.truecaller.utils.ui.b.a(a, m);
    Context localContext = a;
    int i3 = 2130968887;
    int i4 = com.truecaller.utils.ui.b.a(localContext, i3);
    m = com.truecaller.utils.ui.b.a(a, m);
    locali.<init>(i1, i2, i4, m);
    h localh = new com/truecaller/ui/details/h;
    i1 = com.truecaller.utils.ui.b.a(a, i3);
    Object localObject3 = new android/graphics/drawable/ColorDrawable;
    j = com.truecaller.utils.ui.b.a(a, j);
    ((ColorDrawable)localObject3).<init>(j);
    localObject3 = (Drawable)localObject3;
    Object localObject4 = a;
    i4 = com.truecaller.utils.ui.b.d((Context)localObject4, 2130968886);
    localObject4 = at.a((Context)localObject4, i4);
    k.a(localObject4, "GUIUtils.getDrawable(\n  …Background)\n            )");
    localh.<init>(i1, (Drawable)localObject3, (Drawable)localObject4);
    localObject1 = locale;
    j = n;
    localObject3 = locali;
    locale.<init>(i, (Drawable)localObject2, n, locali, localh);
    return locale;
  }
  
  private static e a(Style paramStyle)
  {
    int i;
    try
    {
      paramStyle = paramStyle.getBackgroundColor();
      i = Color.parseColor(paramStyle);
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localIllegalArgumentException);
      paramStyle = "#F2F5F7";
      i = Color.parseColor(paramStyle);
    }
    double d1 = com.truecaller.utils.extensions.f.a(i);
    double d2 = 85.0D;
    boolean bool = d1 < d2;
    int j;
    if (bool) {
      j = 1;
    } else {
      j = 0;
    }
    int k;
    if (j != 0) {
      k = -14208456;
    } else {
      k = -1;
    }
    int m = 855638016;
    if (j != 0) {
      j = 855638016;
    } else {
      j = 872415231;
    }
    int n = com.truecaller.utils.extensions.f.b(i);
    e locale = new com/truecaller/ui/details/e;
    Object localObject1 = new android/graphics/drawable/ColorDrawable;
    ((ColorDrawable)localObject1).<init>(i);
    Object localObject2 = localObject1;
    localObject2 = (Drawable)localObject1;
    i locali = new com/truecaller/ui/details/i;
    Object localObject3 = Integer.valueOf(k);
    Integer localInteger = Integer.valueOf(m);
    Object localObject4 = locali;
    bool = k;
    locali.<init>(k, k, k, k, (Integer)localObject3, localInteger);
    localObject3 = new com/truecaller/ui/details/h;
    localObject4 = new android/graphics/drawable/ColorDrawable;
    ((ColorDrawable)localObject4).<init>(i);
    localObject4 = (Drawable)localObject4;
    localObject1 = new android/graphics/drawable/ColorDrawable;
    ((ColorDrawable)localObject1).<init>(i);
    localObject1 = (Drawable)localObject1;
    paramStyle = new android/graphics/drawable/ColorDrawable;
    paramStyle.<init>(j);
    paramStyle = (Drawable)paramStyle;
    ((h)localObject3).<init>(k, (Drawable)localObject4, (Drawable)localObject1, paramStyle);
    localObject4 = locale;
    locale.<init>(n, (Drawable)localObject2, k, locali, (h)localObject3);
    return locale;
  }
  
  public final e a(Contact paramContact, boolean paramBoolean)
  {
    k.b(paramContact, "contact");
    Object localObject1 = paramContact.f();
    if (paramBoolean) {
      return a();
    }
    boolean bool = paramContact.M();
    if (bool)
    {
      paramContact = new com/truecaller/ui/details/e;
      int j = android.support.v4.content.b.c(a, 2131100386);
      Drawable localDrawable = at.a(a, 2131231130);
      k.a(localDrawable, "GUIUtils.getDrawable(con…drawable.details_gold_bg)");
      int k = android.support.v4.content.b.c(a, 2131100638);
      i locali = new com/truecaller/ui/details/i;
      int m = android.support.v4.content.b.c(a, 2131100383);
      int n = android.support.v4.content.b.c(a, 2131100384);
      localObject1 = a;
      int i1 = 2131100385;
      int i2 = android.support.v4.content.b.c((Context)localObject1, i1);
      localObject1 = a;
      int i3 = 2131100382;
      int i4 = android.support.v4.content.b.c((Context)localObject1, i3);
      Integer localInteger1 = Integer.valueOf(android.support.v4.content.b.c(a, i3));
      Integer localInteger2 = Integer.valueOf(android.support.v4.content.b.c(a, 2131100378));
      Object localObject2 = locali;
      locali.<init>(m, n, i2, i4, localInteger1, localInteger2);
      h localh = new com/truecaller/ui/details/h;
      int i5 = android.support.v4.content.b.c(a, i1);
      Object localObject3 = at.a(a, 2131231126);
      k.a(localObject3, "GUIUtils.getDrawable(con…ar_border_gold_collapsed)");
      localObject2 = at.a(a, 2131231125);
      k.a(localObject2, "GUIUtils.getDrawable(con…s_action_bar_border_gold)");
      localh.<init>(i5, (Drawable)localObject3, (Drawable)localObject2);
      localObject3 = paramContact;
      localObject2 = locali;
      paramContact.<init>(j, localDrawable, k, locali, localh);
      return paramContact;
    }
    bool = b;
    if ((bool) && (localObject1 != null))
    {
      paramContact = (CharSequence)((Style)localObject1).getBackgroundColor();
      if (paramContact != null)
      {
        i = paramContact.length();
        if (i != 0)
        {
          i = 0;
          paramContact = null;
          break label325;
        }
      }
      int i = 1;
      label325:
      if (i == 0) {
        return a((Style)localObject1);
      }
    }
    return a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */