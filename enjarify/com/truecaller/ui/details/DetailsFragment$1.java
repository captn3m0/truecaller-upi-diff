package com.truecaller.ui.details;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import com.truecaller.data.entity.Contact;
import com.truecaller.network.search.j.c;

final class DetailsFragment$1
  extends j.c
{
  DetailsFragment$1(DetailsFragment paramDetailsFragment) {}
  
  public final void a(Contact paramContact)
  {
    Object localObject1 = a;
    boolean bool = ((DetailsFragment)localObject1).s();
    if (bool)
    {
      localObject1 = DetailsFragment.a(a).E();
      Object localObject2 = paramContact.E();
      if ((localObject1 != null) && (localObject2 != null))
      {
        bool = ((Long)localObject1).equals(localObject2);
        if (!bool) {
          return;
        }
      }
      DetailsFragment.a(a, paramContact);
      localObject1 = new android/content/Intent;
      ((Intent)localObject1).<init>();
      Contact localContact = DetailsFragment.a(a);
      ((Intent)localObject1).putExtra("ARG_CONTACT", localContact);
      localObject2 = DetailsFragment.b(a);
      if (localObject2 != null)
      {
        localObject2 = DetailsFragment.b(a);
        int i = -1;
        ((AppCompatActivity)localObject2).setResult(i, (Intent)localObject1);
      }
      localObject1 = a;
      localObject2 = null;
      DetailsFragment.a((DetailsFragment)localObject1, false);
      a.p();
      localObject1 = a;
      bool = DetailsFragment.c((DetailsFragment)localObject1);
      if (bool)
      {
        localObject1 = a;
        DetailsFragment.b((DetailsFragment)localObject1, paramContact);
      }
    }
  }
  
  public final void a(Throwable paramThrowable)
  {
    paramThrowable = a;
    boolean bool = DetailsFragment.c(paramThrowable);
    if (bool)
    {
      paramThrowable = DetailsFragment.a(a);
      if (paramThrowable != null)
      {
        paramThrowable = a;
        Contact localContact = DetailsFragment.a(paramThrowable);
        DetailsFragment.b(paramThrowable, localContact);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsFragment.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */