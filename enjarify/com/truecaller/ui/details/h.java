package com.truecaller.ui.details;

import android.graphics.drawable.Drawable;
import c.g.b.k;

public final class h
{
  final int a;
  final Drawable b;
  final Drawable c;
  final Drawable d;
  
  public h(int paramInt, Drawable paramDrawable1, Drawable paramDrawable2, Drawable paramDrawable3)
  {
    a = paramInt;
    b = paramDrawable1;
    c = paramDrawable2;
    d = paramDrawable3;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof h;
      if (bool2)
      {
        paramObject = (h)paramObject;
        int i = a;
        int j = a;
        Drawable localDrawable1;
        if (i == j)
        {
          i = 1;
        }
        else
        {
          i = 0;
          localDrawable1 = null;
        }
        if (i != 0)
        {
          localDrawable1 = b;
          Drawable localDrawable2 = b;
          boolean bool3 = k.a(localDrawable1, localDrawable2);
          if (bool3)
          {
            localDrawable1 = c;
            localDrawable2 = c;
            bool3 = k.a(localDrawable1, localDrawable2);
            if (bool3)
            {
              localDrawable1 = d;
              paramObject = d;
              boolean bool4 = k.a(localDrawable1, paramObject);
              if (bool4) {
                return bool1;
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = a * 31;
    Drawable localDrawable = b;
    int j = 0;
    int k;
    if (localDrawable != null)
    {
      k = localDrawable.hashCode();
    }
    else
    {
      k = 0;
      localDrawable = null;
    }
    i = (i + k) * 31;
    localDrawable = c;
    if (localDrawable != null)
    {
      k = localDrawable.hashCode();
    }
    else
    {
      k = 0;
      localDrawable = null;
    }
    i = (i + k) * 31;
    localDrawable = d;
    if (localDrawable != null) {
      j = localDrawable.hashCode();
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("DetailsButtonsAppearance(actionColor=");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(", collapsedBackground=");
    Drawable localDrawable = b;
    localStringBuilder.append(localDrawable);
    localStringBuilder.append(", background=");
    localDrawable = c;
    localStringBuilder.append(localDrawable);
    localStringBuilder.append(", separator=");
    localDrawable = d;
    localStringBuilder.append(localDrawable);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */