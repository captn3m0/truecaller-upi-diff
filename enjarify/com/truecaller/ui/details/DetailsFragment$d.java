package com.truecaller.ui.details;

import android.content.Context;
import android.os.AsyncTask;
import com.truecaller.data.entity.Contact;

final class DetailsFragment$d
  extends AsyncTask
{
  final Context a;
  final Contact b;
  final boolean c;
  
  DetailsFragment$d(Context paramContext, Contact paramContact, boolean paramBoolean)
  {
    a = paramContext;
    b = paramContact;
    c = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsFragment.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */