package com.truecaller.ui.details;

import android.support.v7.widget.RecyclerView.Adapter;
import c.a.y;
import c.g.b.k;
import java.util.List;

public final class c
  extends RecyclerView.Adapter
{
  private List a;
  
  public c()
  {
    List localList = (List)y.a;
    a = localList;
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "openHours");
    a = paramList;
    notifyDataSetChanged();
  }
  
  public final int getItemCount()
  {
    return a.size();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */