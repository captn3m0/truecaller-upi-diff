package com.truecaller.ui.details;

import android.net.Uri;
import com.truecaller.data.entity.Contact;
import com.truecaller.util.w.a;

final class DetailsFragment$2
  extends w.a
{
  DetailsFragment$2(DetailsFragment paramDetailsFragment, Contact paramContact, Uri paramUri)
  {
    super(paramContact, paramUri);
  }
  
  public final void a(Object paramObject)
  {
    DetailsFragment localDetailsFragment = a;
    DetailsFragment.d(localDetailsFragment);
    if (paramObject != null)
    {
      localDetailsFragment = a;
      paramObject = (Contact)paramObject;
      DetailsFragment.a(localDetailsFragment, (Contact)paramObject);
      paramObject = a;
      ((DetailsFragment)paramObject).p();
    }
  }
  
  public final Object doInBackground(Object... paramVarArgs)
  {
    paramVarArgs = super.doInBackground(paramVarArgs);
    DetailsFragment localDetailsFragment = a;
    paramVarArgs = (Contact)paramVarArgs;
    return DetailsFragment.c(localDetailsFragment, paramVarArgs);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsFragment.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */