package com.truecaller.ui.details;

import com.truecaller.bp;
import com.truecaller.filters.FilterManager;
import com.truecaller.messaging.j;
import com.truecaller.ui.f.b;
import com.truecaller.ui.o;

final class DetailsFragment$5
  extends f.b
{
  DetailsFragment$5(DetailsFragment paramDetailsFragment, o paramo, FilterManager paramFilterManager)
  {
    super(paramo, paramFilterManager);
  }
  
  public final void a(String paramString)
  {
    j localj = DetailsFragment.f(a).bV();
    boolean bool = localj.a();
    if (bool)
    {
      DetailsFragment.a(a, paramString);
      return;
    }
    DetailsFragment.b(a, paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsFragment.5
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */