package com.truecaller.ui.details;

import com.truecaller.data.access.c;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.util.w;
import java.util.Iterator;
import java.util.List;

final class DetailsFragment$k
  extends DetailsFragment.a
{
  private final Contact e;
  
  DetailsFragment$k(DetailsFragment paramDetailsFragment, Contact paramContact)
  {
    super(paramDetailsFragment, (byte)0);
    e = paramContact;
  }
  
  final Contact b()
  {
    Object localObject1 = DetailsFragment.v(d);
    Object localObject2 = e;
    localObject1 = ((c)localObject1).a((Contact)localObject2);
    boolean bool1 = false;
    localObject2 = null;
    boolean bool2;
    if (localObject1 == null)
    {
      localObject1 = DetailsFragment.v(d);
      localObject3 = DetailsFragment.o(d);
      localObject1 = ((c)localObject1).b((String)localObject3);
      if (localObject1 == null)
      {
        bool2 = false;
        localObject1 = null;
      }
    }
    Object localObject3 = e;
    boolean bool3 = w.a((Contact)localObject3, (Contact)localObject1);
    if (bool3)
    {
      localObject1 = DetailsFragment.a(d);
      if (localObject1 == null) {
        return e;
      }
      return null;
    }
    localObject3 = e;
    bool3 = ((Contact)localObject3).R();
    if ((!bool3) && (localObject1 != null))
    {
      bool3 = ((Contact)localObject1).R();
      if (bool3)
      {
        DetailsFragment.x(d);
        localObject1 = DetailsFragment.a(d);
        if (localObject1 == null) {
          return e;
        }
        return null;
      }
    }
    localObject3 = e;
    bool3 = d;
    if ((!bool3) && (localObject1 != null))
    {
      bool3 = ((Contact)localObject1).S();
      if (bool3)
      {
        localObject3 = e;
        bool3 = ((Contact)localObject3).S();
        if (bool3)
        {
          localObject3 = e;
          long l1 = ((Contact)localObject3).H();
          long l2 = ((Contact)localObject1).H();
          boolean bool4 = l1 < l2;
          if (bool4)
          {
            localObject3 = e;
            bool3 = ((Contact)localObject3).R();
            if (bool3)
            {
              bool3 = ((Contact)localObject1).R();
              if (!bool3) {}
            }
            else
            {
              localObject1 = e;
              a = ((Contact)localObject1);
              localObject1 = DetailsFragment.a(d);
              if (localObject1 == null) {
                return e;
              }
              return null;
            }
          }
        }
      }
    }
    if (localObject1 == null)
    {
      localObject1 = e;
      bool2 = ((Contact)localObject1).S();
      if (!bool2)
      {
        localObject1 = new com/truecaller/data/entity/Contact;
        ((Contact)localObject1).<init>();
        bool1 = true;
        d = bool1;
        localObject2 = e.A().iterator();
        for (;;)
        {
          bool3 = ((Iterator)localObject2).hasNext();
          if (!bool3) {
            break;
          }
          localObject3 = (Number)((Iterator)localObject2).next();
          ((Contact)localObject1).a((Number)localObject3);
        }
        return (Contact)localObject1;
      }
      return e;
    }
    return (Contact)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsFragment.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */