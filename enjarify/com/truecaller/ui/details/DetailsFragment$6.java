package com.truecaller.ui.details;

import com.truecaller.ads.AdLayoutType;
import com.truecaller.ads.k;
import com.truecaller.ads.provider.f;
import com.truecaller.ads.provider.o;
import com.truecaller.ads.ui.AdsSwitchView;
import com.truecaller.featuretoggles.b;

final class DetailsFragment$6
  extends o
{
  DetailsFragment$6(DetailsFragment paramDetailsFragment, f paramf, k paramk)
  {
    super(paramf, paramk);
  }
  
  public final void a(com.truecaller.ads.provider.holders.e parame)
  {
    Object localObject = DetailsFragment.b(a);
    if (localObject != null)
    {
      localObject = DetailsFragment.g(a);
      AdLayoutType localAdLayoutType = null;
      ((AdsSwitchView)localObject).setVisibility(0);
      localObject = DetailsFragment.h(a).U();
      boolean bool = ((b)localObject).a();
      if (bool)
      {
        localObject = DetailsFragment.g(a);
        localAdLayoutType = AdLayoutType.DETAILS;
        ((AdsSwitchView)localObject).a(parame, localAdLayoutType);
        return;
      }
      localObject = DetailsFragment.g(a);
      localAdLayoutType = AdLayoutType.LARGE;
      ((AdsSwitchView)localObject).a(parame, localAdLayoutType);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsFragment.6
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */