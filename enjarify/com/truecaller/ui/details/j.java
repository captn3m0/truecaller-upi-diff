package com.truecaller.ui.details;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.ListPopupWindow;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.bp;
import com.truecaller.log.AssertionUtil;
import com.truecaller.util.at;
import java.util.List;

public final class j
  implements AdapterView.OnItemClickListener
{
  final ListPopupWindow a;
  
  public j(Context paramContext, List paramList, View paramView)
  {
    Object localObject = new android/support/v7/widget/ListPopupWindow;
    ((ListPopupWindow)localObject).<init>(paramContext);
    a = ((ListPopupWindow)localObject);
    a.setAnchorView(paramView);
    paramView = a;
    localObject = new com/truecaller/ui/details/j$a;
    ((j.a)localObject).<init>(paramList);
    paramView.setAdapter((ListAdapter)localObject);
    a.setOnItemClickListener(this);
    paramList = a;
    int i = at.a(paramContext, 240.0F);
    paramList.setContentWidth(i);
    a.setModal(true);
  }
  
  public final void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    paramView = paramAdapterView.getContext();
    paramAdapterView = (com.truecaller.data.entity.e)paramAdapterView.getItemAtPosition(paramInt);
    Object localObject1 = new com/truecaller/analytics/e$a;
    ((e.a)localObject1).<init>("ViewAction");
    localObject1 = ((e.a)localObject1).a("Context", "detailView").a("Action", "externalApp");
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    String str = d;
    ((StringBuilder)localObject2).append(str);
    ((StringBuilder)localObject2).append("/");
    str = c.getType();
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1 = ((e.a)localObject1).a("SubAction", (String)localObject2).a();
    b localb = TrueApp.y().a().c();
    localb.a((com.truecaller.analytics.e)localObject1);
    try
    {
      paramAdapterView = c;
      paramView.startActivity(paramAdapterView);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localActivityNotFoundException;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */