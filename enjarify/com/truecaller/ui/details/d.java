package com.truecaller.ui.details;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import com.truecaller.androidactors.w;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.callhistory.z;
import com.truecaller.calling.initiate_call.b;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.ui.SingleActivity;
import com.truecaller.ui.SingleActivity.FragmentSingle;
import com.truecaller.ui.components.DropdownMenuTextView;
import com.truecaller.ui.components.h;
import com.truecaller.ui.r;
import com.truecaller.util.at;
import com.truecaller.util.cf;
import com.truecaller.voip.ai;

public class d
  extends r
  implements ActionMode.Callback
{
  private ActionMode a;
  private DropdownMenuTextView b;
  private h c;
  private Contact d;
  private final ContentObserver e;
  private k f;
  private com.truecaller.androidactors.f g;
  private com.truecaller.androidactors.a h;
  private CallRecordingManager i;
  private cf l;
  private b m;
  private ai n;
  
  public d()
  {
    d.1 local1 = new com/truecaller/ui/details/d$1;
    Handler localHandler = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    localHandler.<init>(localLooper);
    local1.<init>(this, localHandler);
    e = local1;
  }
  
  private void a(int paramInt1, int paramInt2)
  {
    if (paramInt2 > 0)
    {
      AlertDialog.Builder localBuilder1 = new android/support/v7/app/AlertDialog$Builder;
      Object localObject1 = getActivity();
      localBuilder1.<init>((Context)localObject1);
      localObject1 = getResources();
      int j = 2131755016;
      int k = 1;
      Object[] arrayOfObject = new Object[k];
      Integer localInteger = Integer.valueOf(paramInt2);
      arrayOfObject[0] = localInteger;
      Object localObject2 = ((Resources)localObject1).getQuantityString(j, paramInt2, arrayOfObject);
      localObject2 = localBuilder1.setMessage((CharSequence)localObject2);
      localObject1 = new com/truecaller/ui/details/-$$Lambda$d$S8fFG2wTGyO4jZeRBOUhDwtW8lU;
      ((-..Lambda.d.S8fFG2wTGyO4jZeRBOUhDwtW8lU)localObject1).<init>(this, paramInt1);
      AlertDialog.Builder localBuilder2 = ((AlertDialog.Builder)localObject2).setPositiveButton(2131887217, (DialogInterface.OnClickListener)localObject1);
      paramInt2 = 2131887197;
      localBuilder1 = null;
      localBuilder2 = localBuilder2.setNegativeButton(paramInt2, null);
      localBuilder2.show();
    }
  }
  
  public static void a(Context paramContext, Contact paramContact)
  {
    Object localObject = SingleActivity.FragmentSingle.DETAILS_CALL_LOG;
    localObject = SingleActivity.a(paramContext, (SingleActivity.FragmentSingle)localObject);
    ((Intent)localObject).putExtra("ARG_CONTACT", paramContact);
    paramContext.startActivity((Intent)localObject);
  }
  
  private void a(z paramz)
  {
    Object localObject = c.getCursor();
    if (localObject != null)
    {
      ContentObserver localContentObserver = e;
      ((Cursor)localObject).unregisterContentObserver(localContentObserver);
    }
    if (paramz != null)
    {
      localObject = e;
      paramz.registerContentObserver((ContentObserver)localObject);
    }
    c.swapCursor(paramz);
    paramz = c;
    a(paramz);
    C_();
  }
  
  private void b(int paramInt1, int paramInt2)
  {
    DropdownMenuTextView localDropdownMenuTextView = b;
    Object localObject = getResources();
    int j = 1;
    Object[] arrayOfObject = new Object[j];
    Integer localInteger = Integer.valueOf(paramInt2);
    int k = 0;
    arrayOfObject[0] = localInteger;
    int i1 = 2131755018;
    localObject = ((Resources)localObject).getQuantityString(i1, paramInt2, arrayOfObject);
    localDropdownMenuTextView.setText((CharSequence)localObject);
    localDropdownMenuTextView = b;
    if (paramInt1 == paramInt2) {
      k = 8;
    }
    localDropdownMenuTextView.setVisibility(k);
  }
  
  private void g()
  {
    Object localObject1 = d.getId();
    Object localObject2;
    -..Lambda.d.gcYcebq3BXs35m2md31I36skjaw localgcYcebq3BXs35m2md31I36skjaw;
    if (localObject1 != null)
    {
      localObject1 = (com.truecaller.callhistory.a)g.a();
      localObject2 = d;
      localObject1 = ((com.truecaller.callhistory.a)localObject1).a((Contact)localObject2);
      localObject2 = f.a();
      localgcYcebq3BXs35m2md31I36skjaw = new com/truecaller/ui/details/-$$Lambda$d$gcYcebq3BXs35m2md31I36skjaw;
      localgcYcebq3BXs35m2md31I36skjaw.<init>(this);
      localObject1 = ((w)localObject1).a((i)localObject2, localgcYcebq3BXs35m2md31I36skjaw);
      h = ((com.truecaller.androidactors.a)localObject1);
    }
    else
    {
      localObject1 = d.r();
      if (localObject1 != null)
      {
        localObject2 = (com.truecaller.callhistory.a)g.a();
        localObject1 = ((Number)localObject1).a();
        localObject1 = ((com.truecaller.callhistory.a)localObject2).a((String)localObject1);
        localObject2 = f.a();
        localgcYcebq3BXs35m2md31I36skjaw = new com/truecaller/ui/details/-$$Lambda$d$gcYcebq3BXs35m2md31I36skjaw;
        localgcYcebq3BXs35m2md31I36skjaw.<init>(this);
        localObject1 = ((w)localObject1).a((i)localObject2, localgcYcebq3BXs35m2md31I36skjaw);
        h = ((com.truecaller.androidactors.a)localObject1);
      }
    }
    C_();
  }
  
  private void h()
  {
    ActionMode localActionMode = a;
    if (localActionMode != null) {
      localActionMode.finish();
    }
  }
  
  public final void C_()
  {
    Object localObject = v();
    if (localObject != null)
    {
      localObject = ((ListView)localObject).getAdapter();
      boolean bool1 = true;
      View localView = null;
      boolean bool2;
      if (localObject == null)
      {
        bool2 = true;
      }
      else
      {
        bool2 = false;
        localObject = null;
      }
      if (!bool2)
      {
        h localh = c;
        boolean bool3 = localh.isEmpty();
        if (bool3) {}
      }
      else
      {
        bool1 = false;
      }
      localView = o();
      if (localView != null)
      {
        int j = 2131363685;
        localView = localView.findViewById(j);
      }
      else
      {
        localView = null;
      }
      at.a(localView, bool2);
      at.a(t(), bool1);
      localObject = b();
      at.a((View)localObject, bool1);
    }
  }
  
  public final boolean W_()
  {
    ActionMode localActionMode = a;
    if (localActionMode != null)
    {
      h();
      return true;
    }
    return super.W_();
  }
  
  public final void a()
  {
    super.a();
    Object localObject = c;
    if (localObject != null)
    {
      localObject = ((h)localObject).getCursor();
      if (localObject != null)
      {
        localContentObserver = e;
        ((Cursor)localObject).unregisterContentObserver(localContentObserver);
      }
      localObject = c;
      ContentObserver localContentObserver = null;
      ((h)localObject).swapCursor(null);
    }
    localObject = h;
    if (localObject != null) {
      ((com.truecaller.androidactors.a)localObject).a();
    }
  }
  
  public boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
  {
    int j = paramMenuItem.getItemId();
    int k = 2131361857;
    boolean bool = true;
    if (j != k)
    {
      k = 2131361934;
      int i1 = 0;
      if (j != k) {
        return false;
      }
      paramActionMode = v();
      if (paramActionMode != null)
      {
        k = paramActionMode.getCount();
        while (i1 < k)
        {
          paramActionMode.setItemChecked(i1, bool);
          i1 += 1;
        }
        b(k, k);
      }
      return bool;
    }
    paramActionMode = v();
    if (paramActionMode != null)
    {
      k = 2131362852;
      j = paramActionMode.getCheckedItemCount();
      a(k, j);
    }
    return bool;
  }
  
  public boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    Object localObject1 = getResources();
    Object localObject2 = getActivity();
    if ((localObject1 != null) && (localObject2 != null))
    {
      boolean bool = ((Activity)localObject2).isFinishing();
      if (!bool)
      {
        localObject1 = v();
        if (localObject1 != null)
        {
          int j = 2;
          ((ListView)localObject1).setChoiceMode(j);
          ((ListView)localObject1).clearChoices();
          localObject1 = c;
          ((h)localObject1).notifyDataSetChanged();
        }
        paramActionMode.getMenuInflater().inflate(2131623955, paramMenu);
        paramMenu = LayoutInflater.from((Context)localObject2).inflate(2131558430, null);
        localObject1 = (DropdownMenuTextView)paramMenu.findViewById(2131361913);
        b = ((DropdownMenuTextView)localObject1);
        localObject1 = b;
        localObject2 = new com/truecaller/ui/details/-$$Lambda$d$AQ5L4TmQJNVqeGrba-aal1WR4XM;
        ((-..Lambda.d.AQ5L4TmQJNVqeGrba-aal1WR4XM)localObject2).<init>(this);
        ((DropdownMenuTextView)localObject1).setOnMenuItemClickListener((PopupMenu.OnMenuItemClickListener)localObject2);
        paramActionMode.setCustomView(paramMenu);
        return true;
      }
    }
    return false;
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
    paramMenuInflater.inflate(2131623947, paramMenu);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramBundle = getActivity();
    try
    {
      Object localObject = paramBundle.getIntent();
      if (localObject != null)
      {
        String str = "ARG_CONTACT";
        localObject = ((Intent)localObject).getParcelableExtra(str);
        localObject = (Contact)localObject;
        d = ((Contact)localObject);
      }
    }
    catch (RuntimeException localRuntimeException)
    {
      com.truecaller.log.d.a(localRuntimeException);
    }
    Contact localContact = d;
    if (localContact == null) {
      paramBundle.finish();
    }
    return paramLayoutInflater.inflate(2131559178, paramViewGroup, false);
  }
  
  public void onDestroyActionMode(ActionMode paramActionMode)
  {
    Object localObject = a;
    if ((localObject == paramActionMode) && (localObject != null))
    {
      b = null;
      ((ActionMode)localObject).setCustomView(null);
      a = null;
      paramActionMode = v();
      if (paramActionMode != null)
      {
        localObject = paramActionMode.getCheckedItemPositions();
        int j = 0;
        for (;;)
        {
          int k = ((SparseBooleanArray)localObject).size();
          if (j >= k) {
            break;
          }
          k = ((SparseBooleanArray)localObject).keyAt(j);
          paramActionMode.setItemChecked(k, false);
          j += 1;
        }
        paramActionMode.clearChoices();
        localObject = new com/truecaller/ui/details/-$$Lambda$d$S7gNJQ48-gLZztZzpFIaGrHtdF0;
        ((-..Lambda.d.S7gNJQ48-gLZztZzpFIaGrHtdF0)localObject).<init>(paramActionMode);
        paramActionMode.post((Runnable)localObject);
      }
      return;
    }
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    int j = paramMenuItem.getItemId();
    int k = 2131361858;
    if (j == k)
    {
      paramMenuItem = v();
      if (paramMenuItem != null)
      {
        j = 2131362851;
        int i1 = paramMenuItem.getCount();
        a(j, i1);
      }
      return true;
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  public boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    return false;
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramView = ((bk)getActivity().getApplication()).a();
    paramBundle = paramView.m();
    f = paramBundle;
    paramBundle = paramView.ad();
    g = paramBundle;
    paramBundle = paramView.bg();
    i = paramBundle;
    paramBundle = paramView.bz();
    l = paramBundle;
    paramBundle = paramView.bM();
    m = paramBundle;
    paramView = paramView.cc();
    n = paramView;
    paramView = d;
    if (paramView != null)
    {
      paramView = getActivity();
      int j = 2131886494;
      paramView.setTitle(j);
      boolean bool1 = true;
      setHasOptionsMenu(bool1);
      paramBundle = d.t();
      boolean bool2 = TextUtils.isEmpty(paramBundle);
      if (bool2) {
        paramBundle = d.q();
      }
      int k = 2131886380;
      paramView = new Object[bool1];
      paramView[0] = paramBundle;
      paramView = getString(k, paramView);
      a(paramView, 0);
      paramView = v();
      if (paramView != null)
      {
        paramBundle = new com/truecaller/ui/details/-$$Lambda$d$F3QnnLTvnjY3h0vZ-Smuy7iMsEE;
        paramBundle.<init>(this);
        paramView.setOnItemClickListener(paramBundle);
        paramBundle = new com/truecaller/ui/details/-$$Lambda$d$9_h9IB9uEfUDCk_Hxocs5Dkr4bk;
        paramBundle.<init>(this);
        paramView.setOnItemLongClickListener(paramBundle);
      }
      paramView = new com/truecaller/ui/components/h;
      paramBundle = getActivity();
      CallRecordingManager localCallRecordingManager = i;
      paramView.<init>(paramBundle, localCallRecordingManager);
      c = paramView;
      paramView = c;
      paramBundle = new com/truecaller/ui/details/d$3;
      paramBundle.<init>(this);
      paramView.registerDataSetObserver(paramBundle);
      g();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */