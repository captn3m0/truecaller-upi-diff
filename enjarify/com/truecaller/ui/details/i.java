package com.truecaller.ui.details;

import c.g.b.k;

public final class i
{
  final int a;
  final int b;
  final int c;
  final int d;
  final Integer e;
  final Integer f;
  
  public i(int paramInt1, int paramInt2, int paramInt3, int paramInt4, Integer paramInteger1, Integer paramInteger2)
  {
    a = paramInt1;
    b = paramInt2;
    c = paramInt3;
    d = paramInt4;
    e = paramInteger1;
    f = paramInteger2;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof i;
      if (bool2)
      {
        paramObject = (i)paramObject;
        int i = a;
        int j = a;
        Integer localInteger1;
        if (i == j)
        {
          i = 1;
        }
        else
        {
          i = 0;
          localInteger1 = null;
        }
        if (i != 0)
        {
          i = b;
          j = b;
          if (i == j)
          {
            i = 1;
          }
          else
          {
            i = 0;
            localInteger1 = null;
          }
          if (i != 0)
          {
            i = c;
            j = c;
            if (i == j)
            {
              i = 1;
            }
            else
            {
              i = 0;
              localInteger1 = null;
            }
            if (i != 0)
            {
              i = d;
              j = d;
              if (i == j)
              {
                i = 1;
              }
              else
              {
                i = 0;
                localInteger1 = null;
              }
              if (i != 0)
              {
                localInteger1 = e;
                Integer localInteger2 = e;
                boolean bool3 = k.a(localInteger1, localInteger2);
                if (bool3)
                {
                  localInteger1 = f;
                  paramObject = f;
                  boolean bool4 = k.a(localInteger1, paramObject);
                  if (bool4) {
                    return bool1;
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = a * 31;
    int j = b;
    i = (i + j) * 31;
    j = c;
    i = (i + j) * 31;
    j = d;
    i = (i + j) * 31;
    Integer localInteger = e;
    int k = 0;
    if (localInteger != null)
    {
      j = localInteger.hashCode();
    }
    else
    {
      j = 0;
      localInteger = null;
    }
    i = (i + j) * 31;
    localInteger = f;
    if (localInteger != null) {
      k = localInteger.hashCode();
    }
    return i + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("DetailsHeaderAppearance(primaryTextColor=");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(", secondaryTextColor=");
    i = b;
    localStringBuilder.append(i);
    localStringBuilder.append(", suggestNameColor=");
    i = c;
    localStringBuilder.append(i);
    localStringBuilder.append(", tagIconColor=");
    i = d;
    localStringBuilder.append(i);
    localStringBuilder.append(", tagTextColor=");
    Integer localInteger = e;
    localStringBuilder.append(localInteger);
    localStringBuilder.append(", tagBackgroundColor=");
    localInteger = f;
    localStringBuilder.append(localInteger);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */