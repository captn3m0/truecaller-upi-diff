package com.truecaller.ui.details;

import com.truecaller.common.h.u;
import com.truecaller.data.access.c;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;

final class DetailsFragment$e
  extends DetailsFragment.a
{
  private final String e;
  private final String f;
  private final String g;
  private final String h;
  private final String i;
  
  DetailsFragment$e(DetailsFragment paramDetailsFragment, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    super(paramDetailsFragment, (byte)0);
    e = paramString1;
    f = paramString2;
    g = paramString3;
    h = paramString4;
    i = paramString5;
  }
  
  final Contact b()
  {
    Object localObject1 = e;
    if (localObject1 != null)
    {
      localObject1 = DetailsFragment.v(d);
      localObject2 = e;
      localObject1 = ((c)localObject1).a((String)localObject2);
      if (localObject1 != null) {}
    }
    else
    {
      localObject1 = h;
      if (localObject1 != null)
      {
        localObject1 = DetailsFragment.v(d);
        localObject2 = DetailsFragment.w(d);
        str1 = h;
        localObject2 = ((u)localObject2).b(str1);
        localObject1 = ((c)localObject1).b((String)localObject2);
        if (localObject1 != null) {}
      }
      else
      {
        localObject1 = null;
      }
    }
    if (localObject1 != null) {
      return (Contact)localObject1;
    }
    localObject1 = new com/truecaller/data/entity/Contact;
    ((Contact)localObject1).<init>();
    Object localObject2 = e;
    ((Contact)localObject1).setTcId((String)localObject2);
    localObject2 = f;
    ((Contact)localObject1).l((String)localObject2);
    boolean bool = true;
    d = bool;
    localObject2 = g;
    String str1 = h;
    String str2 = i;
    localObject2 = Number.a((String)localObject2, str1, str2);
    if (localObject2 != null)
    {
      str1 = e;
      ((Number)localObject2).setTcId(str1);
      str1 = ((Number)localObject2).a();
      ((Contact)localObject1).g(str1);
      ((Contact)localObject1).a((Number)localObject2);
    }
    return (Contact)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsFragment.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */