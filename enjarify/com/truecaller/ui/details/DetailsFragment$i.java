package com.truecaller.ui.details;

import android.support.v7.app.AppCompatActivity;
import com.truecaller.calling.a.e;

final class DetailsFragment$i
  extends com.truecaller.old.a.a
{
  private final e c;
  private final String d;
  
  DetailsFragment$i(DetailsFragment paramDetailsFragment, String paramString, e parame)
  {
    d = paramString;
    c = parame;
  }
  
  public final void a(Object paramObject)
  {
    DetailsFragment localDetailsFragment = a;
    boolean bool1 = localDetailsFragment.s();
    if (!bool1) {
      return;
    }
    localDetailsFragment = a;
    boolean bool2 = ((Boolean)paramObject).booleanValue();
    DetailsFragment.b(localDetailsFragment, bool2);
    paramObject = a;
    bool1 = DetailsFragment.j((DetailsFragment)paramObject);
    int i;
    if (bool1) {
      i = 2131886373;
    } else {
      i = 2131886371;
    }
    DetailsFragment.c((DetailsFragment)paramObject, i);
    paramObject = DetailsFragment.b(a);
    if (paramObject != null)
    {
      paramObject = DetailsFragment.b(a);
      ((AppCompatActivity)paramObject).supportInvalidateOptionsMenu();
    }
  }
  
  protected final Object doInBackground(Object... paramVarArgs)
  {
    paramVarArgs = d;
    if (paramVarArgs == null) {
      return Boolean.FALSE;
    }
    e locale = c;
    paramVarArgs = locale.a(paramVarArgs);
    boolean bool = true;
    if (paramVarArgs != null)
    {
      b = bool;
      locale = c;
      locale.b(paramVarArgs);
    }
    else
    {
      paramVarArgs = c;
      com.truecaller.calling.a.a.a locala = new com/truecaller/calling/a/a/a;
      String str = d;
      locala.<init>(str, bool);
      paramVarArgs.a(locala);
    }
    return Boolean.TRUE;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsFragment.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */