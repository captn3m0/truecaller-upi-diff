package com.truecaller.ui.details;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.truecaller.common.h.am;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.util.at;

public final class a
  extends AlertDialog
{
  public a.a a;
  public a.a b;
  private final AppCompatEditText c;
  private final RadioGroup d;
  private final TextView e;
  
  public a(Context paramContext, String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    super((Context)localObject1);
    paramContext = LayoutInflater.from(getContext());
    i = 0;
    Object[] arrayOfObject = null;
    paramContext = paramContext.inflate(2131558562, null);
    localObject1 = (TextView)paramContext.findViewById(2131364884);
    e = ((TextView)localObject1);
    localObject1 = (AppCompatEditText)paramContext.findViewById(2131363463);
    c = ((AppCompatEditText)localObject1);
    int j = 2131364061;
    localObject1 = (RadioGroup)paramContext.findViewById(j);
    d = ((RadioGroup)localObject1);
    localObject1 = d;
    at.a((View)localObject1, paramBoolean2);
    Object localObject2 = c;
    at.a((View)localObject2, paramBoolean1);
    paramBoolean1 = am.b(paramString);
    if (!paramBoolean1)
    {
      localObject3 = e;
      localObject2 = getContext();
      j = 2131886136;
      i = 1;
      arrayOfObject = new Object[i];
      arrayOfObject[0] = paramString;
      paramString = ((Context)localObject2).getString(j, arrayOfObject);
      ((TextView)localObject3).setText(paramString);
    }
    setView(paramContext);
    paramString = getContext().getString(2131886094);
    Object localObject3 = new com/truecaller/ui/details/-$$Lambda$a$9swKkCRyY4UgQaB6FqgeZtWAuJE;
    ((-..Lambda.a.9swKkCRyY4UgQaB6FqgeZtWAuJE)localObject3).<init>(this);
    setButton(-1, paramString, (DialogInterface.OnClickListener)localObject3);
    paramString = getContext().getString(2131887197);
    localObject3 = new com/truecaller/ui/details/-$$Lambda$a$UTs5TOjOgTB8nz9ISE_ACLrOHCk;
    ((-..Lambda.a.UTs5TOjOgTB8nz9ISE_ACLrOHCk)localObject3).<init>(this);
    setButton(-2, paramString, (DialogInterface.OnClickListener)localObject3);
  }
  
  private String a()
  {
    return c.getText().toString().trim();
  }
  
  private TruecallerContract.Filters.EntityType b()
  {
    RadioGroup localRadioGroup = d;
    int i = localRadioGroup.getVisibility();
    if (i == 0)
    {
      localRadioGroup = d;
      i = localRadioGroup.getCheckedRadioButtonId();
      int j = 2131362274;
      if (i != j)
      {
        j = 2131363928;
        if (i == j) {
          return TruecallerContract.Filters.EntityType.PERSON;
        }
      }
      else
      {
        return TruecallerContract.Filters.EntityType.BUSINESS;
      }
    }
    return TruecallerContract.Filters.EntityType.UNKNOWN;
  }
  
  public final a a(a.a parama)
  {
    a = parama;
    return this;
  }
  
  public final a b(a.a parama)
  {
    b = parama;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */