package com.truecaller.ui.details;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.widget.m;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.Availability.Status;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.tag.TagView;
import com.truecaller.common.tag.d;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil;
import com.truecaller.ui.c.a;
import com.truecaller.ui.components.AvatarView;
import com.truecaller.ui.components.k;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.util.at;
import com.truecaller.util.ce;

public class DetailsHeaderView
  extends k
{
  private TextView a;
  private TextView b;
  private TextView d;
  private ViewGroup e;
  private ViewGroup f;
  private AvatarView g;
  private View h;
  private View i;
  private ImageButton j;
  private TextView k;
  private TintedImageView l;
  private Drawable m;
  private Drawable n;
  private Drawable o;
  private bp p;
  private i q;
  
  public DetailsHeaderView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private DetailsHeaderView(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet);
  }
  
  private void a(ViewGroup paramViewGroup, int paramInt)
  {
    paramViewGroup.removeAllViews();
    TagView localTagView = new com/truecaller/common/tag/TagView;
    Context localContext = getContext();
    localTagView.<init>(localContext, false, false);
    Object localObject = getResources().getString(paramInt);
    localTagView.setText((CharSequence)localObject);
    localObject = o;
    localContext = null;
    m.a(localTagView, null, (Drawable)localObject);
    localObject = q.e;
    if (localObject != null)
    {
      localObject = q.e;
      paramInt = ((Integer)localObject).intValue();
      localTagView.setTextColor(paramInt);
    }
    localObject = q.f;
    if (localObject != null)
    {
      localObject = q.f;
      paramInt = ((Integer)localObject).intValue();
      localTagView.setBackgroundColor(paramInt);
    }
    paramViewGroup.addView(localTagView);
    paramViewGroup.setVisibility(0);
  }
  
  public final void a() {}
  
  public final void a(Context paramContext)
  {
    Object localObject = ((bk)paramContext.getApplicationContext()).a();
    p = ((bp)localObject);
    inflate(paramContext, 2131559179, this);
    localObject = (TextView)findViewById(2131363790);
    a = ((TextView)localObject);
    localObject = (TextView)findViewById(2131364559);
    b = ((TextView)localObject);
    localObject = (TextView)findViewById(2131362066);
    d = ((TextView)localObject);
    localObject = (ViewGroup)findViewById(2131361985);
    e = ((ViewGroup)localObject);
    localObject = (ViewGroup)findViewById(2131364653);
    f = ((ViewGroup)localObject);
    localObject = (ImageButton)findViewById(2131364614);
    j = ((ImageButton)localObject);
    localObject = findViewById(2131363984);
    i = ((View)localObject);
    localObject = (TextView)findViewById(2131363451);
    k = ((TextView)localObject);
    localObject = (TintedImageView)findViewById(2131363295);
    l = ((TintedImageView)localObject);
    localObject = (AvatarView)findViewById(2131362068);
    g = ((AvatarView)localObject);
    localObject = findViewById(2131362073);
    h = ((View)localObject);
    g.setVisibility(8);
    localObject = a;
    boolean bool = true;
    ((TextView)localObject).setSelected(bool);
    localObject = new com/truecaller/ui/c$a;
    ((c.a)localObject).<init>(paramContext);
    a = bool;
    b = false;
    int i1 = 6;
    d = i1;
    int i2 = 16;
    e = i2;
    c = bool;
    localObject = ((c.a)localObject).a();
    m = ((Drawable)localObject);
    localObject = new com/truecaller/ui/c$a;
    ((c.a)localObject).<init>(paramContext);
    a = false;
    b = false;
    d = i1;
    e = i2;
    c = bool;
    paramContext = ((c.a)localObject).a();
    n = paramContext;
  }
  
  public final void a(Uri paramUri, boolean paramBoolean) {}
  
  public final void a(Contact paramContact, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    Object localObject1 = q;
    Object localObject2 = new String[0];
    AssertionUtil.isNotNull(localObject1, (String[])localObject2);
    g.setVisibility(0);
    localObject1 = a;
    localObject2 = at.a(paramContact.s());
    ((TextView)localObject1).setText((CharSequence)localObject2);
    localObject1 = a;
    localObject2 = q;
    int i1 = a;
    ((TextView)localObject1).setTextColor(i1);
    boolean bool2 = paramContact.a(2);
    Object localObject3;
    if ((bool2) && (!paramBoolean1))
    {
      localObject3 = a;
      i3 = 2131233985;
      m.a((TextView)localObject3, 0, i3);
    }
    else
    {
      localObject3 = a;
      m.a((TextView)localObject3, 0, 0);
    }
    paramBoolean1 = true;
    if (paramBoolean3)
    {
      localObject4 = paramContact.t();
      paramBoolean3 = TextUtils.isEmpty((CharSequence)localObject4);
      if (!paramBoolean3)
      {
        j.setVisibility(0);
        localObject4 = j;
        localObject1 = q;
        i3 = c;
        localObject2 = PorterDuff.Mode.SRC_IN;
        ((ImageButton)localObject4).setColorFilter(i3, (PorterDuff.Mode)localObject2);
        break label207;
      }
    }
    Object localObject4 = j;
    ((ImageButton)localObject4).setVisibility(paramBoolean1);
    label207:
    localObject4 = com.truecaller.common.b.a.F();
    paramBoolean3 = ((com.truecaller.common.b.a)localObject4).p();
    int i3 = 1;
    Object localObject5;
    Object localObject6;
    if (paramBoolean2)
    {
      i1 = paramContact.I();
      localObject5 = b;
      boolean bool3;
      if (i1 > 0)
      {
        bool3 = true;
      }
      else
      {
        bool3 = false;
        localObject6 = null;
      }
      at.a((View)localObject5, bool3);
      if (i1 > 0)
      {
        localObject5 = b;
        localObject6 = getResources();
        int i4 = 2131886146;
        Object[] arrayOfObject = new Object[i3];
        localObject2 = Integer.valueOf(i1);
        arrayOfObject[0] = localObject2;
        localObject2 = ((Resources)localObject6).getString(i4, arrayOfObject);
        ((TextView)localObject5).setText((CharSequence)localObject2);
      }
    }
    else
    {
      localObject2 = b;
      ((TextView)localObject2).setVisibility(paramBoolean1);
    }
    localObject2 = ce.a(paramContact.J());
    int i5;
    if (localObject2 != null)
    {
      f.removeAllViews();
      localObject5 = new com/truecaller/common/tag/TagView;
      localObject6 = getContext();
      ((TagView)localObject5).<init>((Context)localObject6, false, i3);
      ((TagView)localObject5).setTag((com.truecaller.common.tag.c)localObject2);
      localObject2 = q.e;
      if (localObject2 != null)
      {
        localObject2 = q.e;
        i1 = ((Integer)localObject2).intValue();
        ((TagView)localObject5).setTextColor(i1);
      }
      localObject2 = q.f;
      if (localObject2 != null)
      {
        localObject2 = q.f;
        i1 = ((Integer)localObject2).intValue();
        ((TagView)localObject5).setBackgroundColor(i1);
      }
      f.addView((View)localObject5);
      localObject2 = f;
      ((ViewGroup)localObject2).setVisibility(0);
    }
    else
    {
      boolean bool1 = paramContact.R();
      if ((!bool1) && (paramBoolean3))
      {
        bool1 = d.a();
        if (bool1)
        {
          localObject2 = f;
          i5 = 2131886311;
          a((ViewGroup)localObject2, i5);
        }
      }
    }
    if (!paramBoolean2)
    {
      localObject7 = p.N().a(paramContact);
      if (localObject7 != null)
      {
        localObject2 = DetailsHeaderView.1.a;
        i5 = b.a().ordinal();
        i2 = localObject2[i5];
        i5 = 0;
        localObject5 = null;
        switch (i2)
        {
        default: 
          break;
        case 2: 
          localObject2 = d;
          localObject6 = n;
          m.a((TextView)localObject2, (Drawable)localObject6, null);
          localObject2 = d;
          localObject5 = getContext();
          localObject7 = ((com.truecaller.presence.a)localObject7).a((Context)localObject5, i3);
          ((TextView)localObject2).setText((CharSequence)localObject7);
          paramBoolean2 = false;
          localObject7 = null;
          break;
        case 1: 
          localObject2 = d;
          localObject6 = m;
          m.a((TextView)localObject2, (Drawable)localObject6, null);
          localObject2 = d;
          localObject5 = getContext();
          localObject7 = ((com.truecaller.presence.a)localObject7).a((Context)localObject5, i3);
          ((TextView)localObject2).setText((CharSequence)localObject7);
          paramBoolean2 = false;
          localObject7 = null;
          break;
        }
      }
    }
    paramBoolean2 = true;
    localObject1 = d;
    localObject2 = q;
    int i2 = b;
    ((TextView)localObject1).setTextColor(i2);
    localObject1 = d;
    ((TextView)localObject1).setVisibility(paramBoolean2);
    Object localObject7 = paramContact.t();
    paramBoolean2 = TextUtils.isEmpty((CharSequence)localObject7);
    if (paramBoolean2)
    {
      localObject7 = h;
      ((View)localObject7).setVisibility(paramBoolean1);
      paramBoolean1 = paramContact.O();
      if ((paramBoolean1) && (paramBoolean3))
      {
        localObject3 = e;
        paramBoolean2 = 2131886310;
        a((ViewGroup)localObject3, paramBoolean2);
      }
    }
    else
    {
      h.setVisibility(0);
      localObject7 = e;
      ((ViewGroup)localObject7).setVisibility(paramBoolean1);
    }
    paramBoolean1 = paramContact.R();
    if (paramBoolean1)
    {
      paramContact = paramContact.a(false);
      if (paramContact == null)
      {
        paramContact = p.ai();
        boolean bool4 = paramContact.d();
        if (!bool4)
        {
          g.setPrivateAvatar(2131233894);
          paramContact = l;
          paramBoolean1 = q.b;
          paramContact.setTint(paramBoolean1);
          paramContact = k;
          localObject3 = q;
          paramBoolean1 = b;
          paramContact.setTextColor(paramBoolean1);
          paramContact = i;
          paramContact.setVisibility(0);
        }
      }
    }
  }
  
  public final void b() {}
  
  public final void c() {}
  
  public final void d() {}
  
  public void setAppearance(i parami)
  {
    q = parami;
    Context localContext = getContext();
    int i1 = d;
    parami = at.a(localContext, 2131233810, i1);
    o = parami;
  }
  
  public void setOnAddNameClickListener(View.OnClickListener paramOnClickListener)
  {
    e.setOnClickListener(paramOnClickListener);
  }
  
  public void setOnSuggestNameButtonClickListener(View.OnClickListener paramOnClickListener)
  {
    j.setOnClickListener(paramOnClickListener);
  }
  
  public void setOnTagClickListener(View.OnClickListener paramOnClickListener)
  {
    f.setOnClickListener(paramOnClickListener);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsHeaderView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */