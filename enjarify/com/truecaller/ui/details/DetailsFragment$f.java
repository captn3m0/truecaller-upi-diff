package com.truecaller.ui.details;

import com.truecaller.data.entity.Contact;

final class DetailsFragment$f
  extends DetailsFragment.a
{
  private final Contact e;
  
  DetailsFragment$f(DetailsFragment paramDetailsFragment, Contact paramContact)
  {
    super(paramDetailsFragment, (byte)0);
    e = paramContact;
  }
  
  final Contact b()
  {
    return e;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsFragment.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */