package com.truecaller.ui.details;

import android.graphics.drawable.Drawable;
import c.g.b.k;

public final class e
{
  final int a;
  final Drawable b;
  final int c;
  final i d;
  final h e;
  
  public e(int paramInt1, Drawable paramDrawable, int paramInt2, i parami, h paramh)
  {
    a = paramInt1;
    b = paramDrawable;
    c = paramInt2;
    d = parami;
    e = paramh;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof e;
      if (bool2)
      {
        paramObject = (e)paramObject;
        int i = a;
        int k = a;
        Object localObject1;
        if (i == k)
        {
          i = 1;
        }
        else
        {
          i = 0;
          localObject1 = null;
        }
        if (i != 0)
        {
          localObject1 = b;
          Object localObject2 = b;
          boolean bool3 = k.a(localObject1, localObject2);
          if (bool3)
          {
            int j = c;
            k = c;
            if (j == k)
            {
              j = 1;
            }
            else
            {
              j = 0;
              localObject1 = null;
            }
            if (j != 0)
            {
              localObject1 = d;
              localObject2 = d;
              boolean bool4 = k.a(localObject1, localObject2);
              if (bool4)
              {
                localObject1 = e;
                paramObject = e;
                boolean bool5 = k.a(localObject1, paramObject);
                if (bool5) {
                  return bool1;
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = a * 31;
    Object localObject = b;
    int j = 0;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    i = (i + k) * 31;
    int k = c;
    i = (i + k) * 31;
    localObject = d;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    i = (i + k) * 31;
    localObject = e;
    if (localObject != null) {
      j = localObject.hashCode();
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("DetailsAppearance(statusBarColor=");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(", appBarBackground=");
    Object localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", toolbarIconColor=");
    i = c;
    localStringBuilder.append(i);
    localStringBuilder.append(", headerAppearance=");
    localObject = d;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", buttonsAppearance=");
    localObject = e;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */