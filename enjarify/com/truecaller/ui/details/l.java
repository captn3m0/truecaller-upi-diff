package com.truecaller.ui.details;

import android.support.v7.widget.RecyclerView.Adapter;
import c.a.y;
import java.util.List;

public final class l
  extends RecyclerView.Adapter
{
  private List a;
  private final k b;
  
  public l(k paramk)
  {
    b = paramk;
    paramk = (List)y.a;
    a = paramk;
  }
  
  private final String a(int paramInt)
  {
    return (String)a.get(paramInt);
  }
  
  public final void a(List paramList)
  {
    c.g.b.k.b(paramList, "pictures");
    a = paramList;
    notifyDataSetChanged();
  }
  
  public final int getItemCount()
  {
    return a.size();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */