package com.truecaller.ui.details;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import com.truecaller.R.id;
import java.util.HashMap;
import kotlinx.a.a.a;

public final class l$a
  extends RecyclerView.ViewHolder
  implements View.OnClickListener, a
{
  private final View b;
  private HashMap c;
  
  public l$a(l paraml, View paramView)
  {
    super(paramView);
    b = paramView;
    int i = R.id.rootView;
    paraml = (CardView)a(i);
    paramView = this;
    paramView = (View.OnClickListener)this;
    paraml.setOnClickListener(paramView);
  }
  
  public final View a()
  {
    return b;
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void onClick(View paramView)
  {
    c.g.b.k.b(paramView, "view");
    paramView = l.a(a);
    int i = R.id.pictureImageView;
    ImageView localImageView = (ImageView)a(i);
    c.g.b.k.a(localImageView, "pictureImageView");
    Object localObject = a;
    int j = getAdapterPosition();
    localObject = l.a((l)localObject, j);
    paramView.a(localImageView, (String)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.l.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */