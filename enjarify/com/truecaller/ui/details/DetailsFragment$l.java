package com.truecaller.ui.details;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.common.h.am;
import java.util.ArrayList;

final class DetailsFragment$l
  extends BaseAdapter
{
  private final LayoutInflater b;
  
  DetailsFragment$l(DetailsFragment paramDetailsFragment)
  {
    paramDetailsFragment = LayoutInflater.from(DetailsFragment.b(paramDetailsFragment));
    b = paramDetailsFragment;
  }
  
  public final int getCount()
  {
    return DetailsFragment.B(a).size();
  }
  
  public final long getItemId(int paramInt)
  {
    return 0L;
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null)
    {
      paramView = b;
      int i = 2131559040;
      bool = false;
      paramView = paramView.inflate(i, paramViewGroup, false);
      paramViewGroup = new com/truecaller/ui/details/DetailsFragment$l$a;
      paramViewGroup.<init>(this, paramView);
      paramView.setTag(paramViewGroup);
    }
    else
    {
      paramViewGroup = (DetailsFragment.l.a)paramView.getTag();
    }
    String str = (String)DetailsFragment.B(a).get(paramInt);
    ImageView localImageView = b;
    boolean bool = am.b(str);
    int j;
    if (bool) {
      j = 2131233928;
    } else {
      j = 2131234474;
    }
    localImageView.setImageResource(j);
    a.setText(str);
    return paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsFragment.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */