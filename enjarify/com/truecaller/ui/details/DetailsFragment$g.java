package com.truecaller.ui.details;

import android.support.v7.app.AppCompatActivity;
import com.truecaller.androidactors.f;
import com.truecaller.data.entity.Contact;
import com.truecaller.filters.FilterManager;
import com.truecaller.search.b;
import com.truecaller.search.g;
import java.util.UUID;

final class DetailsFragment$g
  extends g
{
  DetailsFragment$g(DetailsFragment paramDetailsFragment, Contact paramContact)
  {
    super(localAppCompatActivity, paramDetailsFragment, localFilterManager, localf, paramContact, 20, "detailView", localUUID, i, localb);
  }
  
  public final void a(Object paramObject)
  {
    DetailsFragment localDetailsFragment = a;
    DetailsFragment.d(localDetailsFragment);
    if (paramObject != null)
    {
      localDetailsFragment = a;
      boolean bool = localDetailsFragment.s();
      if (bool)
      {
        localDetailsFragment = a;
        paramObject = (Contact)paramObject;
        DetailsFragment.a(localDetailsFragment, (Contact)paramObject);
        paramObject = a;
        ((DetailsFragment)paramObject).p();
      }
    }
  }
  
  public final Object doInBackground(Object... paramVarArgs)
  {
    paramVarArgs = super.doInBackground(paramVarArgs);
    boolean bool = paramVarArgs instanceof Contact;
    if (bool)
    {
      DetailsFragment localDetailsFragment = a;
      paramVarArgs = (Contact)paramVarArgs;
      return DetailsFragment.c(localDetailsFragment, paramVarArgs);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.details.DetailsFragment.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */