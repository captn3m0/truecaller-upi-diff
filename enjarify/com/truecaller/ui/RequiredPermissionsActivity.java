package com.truecaller.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.analytics.bc;
import com.truecaller.bl;
import com.truecaller.bp;
import com.truecaller.common.a;
import com.truecaller.common.h.ac;
import com.truecaller.common.h.c;
import com.truecaller.wizard.utils.i;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RequiredPermissionsActivity
  extends AppCompatActivity
  implements View.OnClickListener
{
  private com.truecaller.utils.l a;
  private com.truecaller.tcpermissions.l b;
  
  private void a()
  {
    finish();
    Object localObject = getIntent();
    String str = "return_to_tab";
    boolean bool = ((Intent)localObject).hasExtra(str);
    if (bool)
    {
      localObject = getIntent().getStringExtra("return_to_tab");
      TruecallerInit.b(this, (String)localObject, "requiredPermission");
      return;
    }
    TruecallerInit.b(this, "requiredPermission");
  }
  
  public static void a(Context paramContext)
  {
    a(paramContext, null);
  }
  
  public static void a(Context paramContext, String paramString)
  {
    Intent localIntent = new android/content/Intent;
    Object localObject = RequiredPermissionsActivity.class;
    localIntent.<init>(paramContext, (Class)localObject);
    int i = 268468224;
    localIntent.setFlags(i);
    if (paramString != null)
    {
      localObject = "return_to_tab";
      localIntent.putExtra((String)localObject, paramString);
    }
    paramContext.startActivity(localIntent);
  }
  
  private boolean a(List paramList, int paramInt, String... paramVarArgs)
  {
    com.truecaller.utils.l locall = a;
    boolean bool1 = locall.a(paramVarArgs);
    if (!bool1)
    {
      int i = paramVarArgs.length;
      int j = 0;
      while (j < i)
      {
        String str = paramVarArgs[j];
        boolean bool2 = i.a(this, str);
        if (bool2)
        {
          paramList = new com/truecaller/bl;
          paramList.<init>(this, paramInt);
          paramList.show();
          return false;
        }
        j += 1;
      }
      List localList = Arrays.asList(paramVarArgs);
      paramList.addAll(localList);
    }
    return true;
  }
  
  public void onClick(View paramView)
  {
    int i = paramView.getId();
    int j = 2131362303;
    if (i == j)
    {
      paramView = new java/util/ArrayList;
      paramView.<init>();
      String[] arrayOfString1 = b.a();
      int k = a(paramView, 2131233931, arrayOfString1);
      if (k != 0)
      {
        arrayOfString1 = b.b();
        k = a(paramView, 2131234010, arrayOfString1);
        if (k != 0)
        {
          k = paramView.isEmpty();
          if (k != 0)
          {
            a();
            return;
          }
          String[] arrayOfString2 = new String[paramView.size()];
          paramView = (String[])paramView.toArray(arrayOfString2);
          k = 1;
          requestPermissions(paramView, k);
        }
      }
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = aresId;
    setTheme(i);
    i = 2131558465;
    setContentView(i);
    paramBundle = (TrueApp)getApplicationContext();
    Object localObject1 = paramBundle.a();
    Object localObject2 = ((bp)localObject1).bw();
    a = ((com.truecaller.utils.l)localObject2);
    localObject2 = ((bp)localObject1).bt();
    b = ((com.truecaller.tcpermissions.l)localObject2);
    localObject1 = ((bp)localObject1).aI();
    boolean bool2 = ((c)localObject1).c();
    if (!bool2)
    {
      paramBundle = b.n();
      boolean bool1 = paramBundle.a();
      if (bool1)
      {
        int j = 2131363938;
        paramBundle = (TextView)findViewById(j);
        int k = 2131886813;
        paramBundle.setText(k);
      }
    }
    findViewById(2131362303).setOnClickListener(this);
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    i.a(paramArrayOfString, paramArrayOfInt);
  }
  
  public void onResume()
  {
    super.onResume();
    Object localObject = b;
    boolean bool = ((com.truecaller.tcpermissions.l)localObject).f();
    if (bool)
    {
      a();
      return;
    }
    localObject = TrueApp.y().a().c();
    bc localbc = new com/truecaller/analytics/bc;
    localbc.<init>("requiredPermission");
    ((b)localObject).a(localbc);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.RequiredPermissionsActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */