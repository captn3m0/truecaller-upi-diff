package com.truecaller.ui;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.truecaller.ui.view.ThemeSelectorView;

final class ad$a$a
  extends RecyclerView.ViewHolder
  implements View.OnClickListener
{
  TextView a;
  ThemeSelectorView b;
  int c;
  int d;
  
  ad$a$a(ad.a parama, View paramView)
  {
    super(paramView);
    parama = (TextView)paramView.findViewById(2131364826);
    a = parama;
    parama = (ThemeSelectorView)paramView.findViewById(2131364291);
    b = parama;
    paramView.setOnClickListener(this);
    c = -1;
    d = -16777216;
  }
  
  public final void onClick(View paramView)
  {
    int i = getLayoutPosition();
    if (i > 0)
    {
      ad.a locala = e;
      int j = ad.a.a(locala);
      locala.notifyItemChanged(j);
      locala = e;
      ad.a.a(locala, i);
      paramView = e;
      int k = ad.a.a(paramView);
      paramView.notifyItemChanged(k);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.ad.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */