package com.truecaller.ui;

import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.RelativeLayout.LayoutParams;
import com.truecaller.ui.components.SnappingRelativeLayout;

final class w$4
  implements Animation.AnimationListener
{
  w$4(w paramw) {}
  
  public final void onAnimationEnd(Animation paramAnimation)
  {
    w.e(a);
    a.d.setAnimation(null);
    ((RelativeLayout.LayoutParams)a.d.getLayoutParams()).setMargins(0, 0, 0, 0);
    a.c.forceLayout();
    a.c.requestLayout();
    a.c.invalidate();
    w.a(a, false);
  }
  
  public final void onAnimationRepeat(Animation paramAnimation) {}
  
  public final void onAnimationStart(Animation paramAnimation) {}
}

/* Location:
 * Qualified Name:     com.truecaller.ui.w.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */