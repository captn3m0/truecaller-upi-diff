package com.truecaller.ui;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import com.truecaller.ui.components.FloatingActionButton;
import com.truecaller.util.at;

final class i$4
  implements View.OnTouchListener
{
  private final int b;
  private int[] c;
  
  i$4(i parami)
  {
    int i = at.a(a.getContext(), 16.0F);
    b = i;
    parami = new int[2];
    c = parami;
  }
  
  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    int i = paramMotionEvent.getActionMasked();
    switch (i)
    {
    default: 
      break;
    case 1: 
    case 3: 
      paramView = i.e(a);
      if (paramView != null)
      {
        paramView = i.e(a);
        paramView.setVisibility(0);
      }
      break;
    case 0: 
    case 2: 
      paramView = i.e(a);
      if (paramView != null)
      {
        paramView = i.e(a).getButtonView();
        Object localObject = c;
        paramView.getLocationOnScreen((int[])localObject);
        float f1 = paramMotionEvent.getRawY();
        paramMotionEvent = c;
        int k = paramMotionEvent[1];
        int m = b;
        k -= m;
        localObject = i.e(a);
        float f2 = k;
        boolean bool = f1 < f2;
        int j;
        if (bool)
        {
          j = 4;
          f1 = 5.6E-45F;
        }
        else
        {
          j = 0;
          f1 = 0.0F;
          paramView = null;
        }
        ((FloatingActionButton)localObject).setVisibility(j);
      }
      break;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.i.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */