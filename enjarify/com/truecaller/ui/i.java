package com.truecaller.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.TextView;
import com.truecaller.TrueApp;
import com.truecaller.ads.AdLayoutType;
import com.truecaller.ads.CustomTemplate;
import com.truecaller.ads.k.a;
import com.truecaller.ads.k.b;
import com.truecaller.ads.k.c;
import com.truecaller.analytics.az;
import com.truecaller.analytics.bc;
import com.truecaller.analytics.e.a;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.account.r;
import com.truecaller.data.entity.Contact;
import com.truecaller.search.local.model.a.o;
import com.truecaller.search.local.model.j;
import com.truecaller.search.local.model.j.a;
import com.truecaller.ui.components.FloatingActionButton;
import com.truecaller.ui.components.FloatingActionButton.a;
import com.truecaller.ui.components.FloatingActionButton.c;
import com.truecaller.ui.components.SideIndexScroller;
import com.truecaller.ui.components.SideIndexScroller.a;
import com.truecaller.util.at;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

public final class i
  extends e
  implements az, a, aa.a, FloatingActionButton.c, d.c
{
  private SideIndexScroller b;
  private h c;
  private k d;
  private com.truecaller.search.local.model.g e;
  private boolean f = false;
  private boolean g = false;
  private com.truecaller.analytics.b h;
  private com.truecaller.ads.a.a i;
  private com.truecaller.ads.a.e j;
  private RecyclerView k;
  private View l;
  private FloatingActionButton m;
  private final Handler n;
  private final BroadcastReceiver o;
  private final com.truecaller.ads.h p;
  
  public i()
  {
    Object localObject = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    ((Handler)localObject).<init>(localLooper);
    n = ((Handler)localObject);
    localObject = new com/truecaller/ui/i$a;
    ((i.a)localObject).<init>(this, (byte)0);
    o = ((BroadcastReceiver)localObject);
    i.1 local1 = new com/truecaller/ui/i$1;
    local1.<init>(this);
    p = local1;
  }
  
  private j a(int paramInt)
  {
    Object localObject1 = i;
    int i1 = ((com.truecaller.ads.a.a)localObject1).getItemViewType(paramInt);
    if (i1 != 0)
    {
      int i2 = 2131365486;
      if (i1 != i2) {
        return null;
      }
    }
    localObject1 = k;
    Object localObject2 = ((RecyclerView)localObject1).findViewHolderForAdapterPosition(paramInt);
    boolean bool = localObject2 instanceof d.g;
    if (!bool) {
      return null;
    }
    localObject2 = ((h.a)localObject2).a();
    bool = localObject2 instanceof j;
    if (bool) {
      return (j)localObject2;
    }
    bool = localObject2 instanceof com.truecaller.search.local.model.a.i;
    if (bool) {
      return ((com.truecaller.search.local.model.a.i)localObject2).b();
    }
    return null;
  }
  
  private List b(boolean paramBoolean)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Iterator localIterator = e.c().iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      j localj = (j)localIterator.next();
      boolean bool2 = localj.e();
      String str;
      if (bool2)
      {
        str = f;
      }
      else
      {
        bool2 = false;
        str = null;
      }
      if (str != null)
      {
        Object localObject1 = localj.b();
        boolean bool3 = ((Set)localObject1).isEmpty();
        if (!bool3)
        {
          int i1 = g;
          int i2 = 32;
          i1 &= i2;
          int i3 = 1;
          if (i1 != i2) {
            i1 = 1;
          } else {
            i1 = 0;
          }
          if (i1 != 0)
          {
            localObject1 = ((Set)localObject1).iterator();
            boolean bool4;
            do
            {
              bool4 = ((Iterator)localObject1).hasNext();
              if (!bool4) {
                break;
              }
              Object localObject2 = (o)((Iterator)localObject1).next();
              int i4 = ((o)localObject2).h();
              int i5 = 10;
              if (i4 >= i5) {
                break label225;
              }
              localObject2 = ((o)localObject2).e();
              bool4 = str.equalsIgnoreCase((String)localObject2);
            } while (!bool4);
            break label225;
            i3 = 0;
            label225:
            if (i3 != 0) {}
          }
          else
          {
            localObject1 = "Truecaller Verification";
            bool2 = str.equalsIgnoreCase((String)localObject1);
            if (!bool2)
            {
              bool2 = localArrayList.contains(localj);
              if ((!bool2) && ((!paramBoolean) || (i1 == 0))) {
                localArrayList.add(localj);
              }
            }
          }
        }
      }
    }
    return localArrayList;
  }
  
  private void e()
  {
    boolean bool = f;
    if (!bool)
    {
      localObject1 = j;
      bool = ((com.truecaller.ads.a.e)localObject1).f();
      if (!bool)
      {
        localObject1 = i;
        localObject2 = new com/truecaller/ads/a/c;
        ((com.truecaller.ads.a.c)localObject2).<init>();
        ((com.truecaller.ads.a.a)localObject1).a((com.truecaller.ads.a.b)localObject2);
        return;
      }
    }
    Object localObject1 = i;
    Object localObject2 = new com/truecaller/ads/a/d;
    int i1 = d.b();
    ((com.truecaller.ads.a.d)localObject2).<init>(i1, 0);
    ((com.truecaller.ads.a.a)localObject1).a((com.truecaller.ads.a.b)localObject2);
  }
  
  private void g()
  {
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = e.f();
    Object localObject3 = e.b();
    ((SortedSet)localObject2).addAll((Collection)localObject3);
    boolean bool1 = g;
    boolean bool3 = true;
    Object localObject4;
    if (bool1)
    {
      localObject3 = b(false);
      ((SortedSet)localObject2).addAll((Collection)localObject3);
      localObject1 = ((AppCompatActivity)localObject1).getSupportActionBar();
      if (localObject1 != null)
      {
        int i1 = 2131887468;
        localObject4 = new Object[bool3];
        int i3 = ((SortedSet)localObject2).size();
        Integer localInteger = Integer.valueOf(i3);
        localObject4[0] = localInteger;
        localObject3 = getString(i1, (Object[])localObject4);
        ((ActionBar)localObject1).setTitle((CharSequence)localObject3);
      }
    }
    else
    {
      localObject1 = ((bk)((Activity)localObject1).getApplicationContext()).a();
      localObject3 = ((bp)localObject1).aF();
      localObject4 = ((bp)localObject1).T();
      localObject1 = ((bp)localObject1).I();
      localObject3 = ((com.truecaller.featuretoggles.e)localObject3).l();
      boolean bool2 = ((com.truecaller.featuretoggles.b)localObject3).a();
      if (bool2)
      {
        bool2 = ((r)localObject4).c();
        if (bool2)
        {
          localObject3 = "backup_enabled";
          bool4 = ((com.truecaller.common.g.a)localObject1).b((String)localObject3);
          if (bool4)
          {
            bool4 = true;
            break label220;
          }
        }
      }
      boolean bool4 = false;
      localObject1 = null;
      label220:
      if (bool4)
      {
        localObject3 = b(bool3);
        ((SortedSet)localObject2).addAll((Collection)localObject3);
      }
      bool2 = ((SortedSet)localObject2).isEmpty();
      int i4;
      if (bool2)
      {
        b.a(false);
        at.a(k, false);
        localObject2 = l;
        bool2 = localObject2 instanceof ViewStub;
        if (bool2)
        {
          localObject2 = ((ViewStub)localObject2).inflate();
          l = ((View)localObject2);
          localObject2 = l;
          int i2 = 2131361984;
          localObject2 = (Button)((View)localObject2).findViewById(i2);
          if (!bool4)
          {
            i4 = 2131886401;
            ((Button)localObject2).setText(i4);
            localObject1 = new com/truecaller/ui/-$$Lambda$i$5I44xdT7aOjwWMtFWFO9ZRXEmx8;
            ((-..Lambda.i.5I44xdT7aOjwWMtFWFO9ZRXEmx8)localObject1).<init>(this);
            ((Button)localObject2).setOnClickListener((View.OnClickListener)localObject1);
          }
          else
          {
            i4 = 2131887500;
            ((Button)localObject2).setText(i4);
            localObject1 = new com/truecaller/ui/-$$Lambda$i$lxxOuPoi2BITkTaEE9TPgLI5VJk;
            ((-..Lambda.i.lxxOuPoi2BITkTaEE9TPgLI5VJk)localObject1).<init>(this);
            ((Button)localObject2).setOnClickListener((View.OnClickListener)localObject1);
          }
        }
        at.a(l, bool3);
        return;
      }
      localObject3 = new java/util/ArrayList;
      localObject4 = e.d();
      ((ArrayList)localObject3).<init>((Collection)localObject4);
      if (i4 != 0)
      {
        localObject1 = new com/truecaller/search/local/model/j$a;
        localObject4 = e;
        ((j.a)localObject1).<init>((com.truecaller.search.local.model.g)localObject4);
        localObject4 = getString(2131887467);
        d = ((String)localObject4);
        int i5 = -1;
        p = i5;
        localObject1 = ((j.a)localObject1).a();
        ((List)localObject3).add(0, localObject1);
      }
      localObject1 = d;
      localObject4 = new com/truecaller/ui/d$f;
      ((d.f)localObject4).<init>((Collection)localObject3);
      ((k)localObject1).a((d.a)localObject4);
    }
    localObject1 = c;
    localObject3 = new com/truecaller/ui/d$f;
    ((d.f)localObject3).<init>((Collection)localObject2);
    ((h)localObject1).a((d.a)localObject3);
    b.a(bool3);
    b.invalidate();
    at.a(k, bool3);
    at.a(l, false);
    e();
    a();
  }
  
  private void h()
  {
    com.truecaller.analytics.b localb = h;
    Object localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>("ViewAction");
    localObject = ((e.a)localObject).a("Action", "message").a("Context", "contacts").a();
    localb.a((com.truecaller.analytics.e)localObject);
  }
  
  public final void a(int paramInt, boolean paramBoolean, View paramView)
  {
    paramView = b;
    boolean bool = true;
    paramView.a(bool);
    Object localObject1 = a(paramInt);
    if (localObject1 == null) {
      return;
    }
    paramView = new com/truecaller/ui/y;
    Object localObject2 = getActivity();
    int i1 = paramBoolean ^ true;
    localObject1 = ((j)localObject1).f();
    String str = "contacts";
    paramView.<init>((android.support.v4.app.f)localObject2, i1, (Contact)localObject1, str);
    localObject1 = new com/truecaller/analytics/e$a;
    ((e.a)localObject1).<init>("ANDROID_Swipes");
    localObject2 = "Contacts";
    localObject1 = ((e.a)localObject1).a("Source", (String)localObject2);
    paramView = "Action";
    if (paramBoolean) {
      localObject2 = "Call";
    } else {
      localObject2 = "SMS";
    }
    ((e.a)localObject1).a(paramView, (String)localObject2);
    paramView = h;
    localObject1 = ((e.a)localObject1).a();
    paramView.a((com.truecaller.analytics.e)localObject1);
    if (!paramBoolean) {
      h();
    }
  }
  
  public final void a(Object paramObject)
  {
    boolean bool = paramObject instanceof j;
    if (bool)
    {
      paramObject = (j)paramObject;
    }
    else
    {
      bool = paramObject instanceof com.truecaller.search.local.model.a.i;
      if (bool) {
        paramObject = ((com.truecaller.search.local.model.a.i)paramObject).b();
      } else {
        paramObject = null;
      }
    }
    if (paramObject != null)
    {
      h();
      y localy = new com/truecaller/ui/y;
      android.support.v4.app.f localf = getActivity();
      int i1 = 1;
      paramObject = ((j)paramObject).f();
      String str = "contacts";
      localy.<init>(localf, i1, (Contact)paramObject, str);
    }
  }
  
  public final void a(String paramString)
  {
    paramString = h;
    bc localbc = new com/truecaller/analytics/bc;
    localbc.<init>("contacts");
    paramString.a(localbc);
  }
  
  public final boolean a(int paramInt, View paramView)
  {
    paramView = (j)d.a(paramInt);
    if (paramView != null)
    {
      int i1 = k;
      int i2 = -1;
      if (i1 == i2) {
        return false;
      }
    }
    paramView = b;
    paramView.a(false);
    j localj = a(paramInt);
    return localj != null;
  }
  
  protected final Collection b()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject = c;
    RecyclerView localRecyclerView = k;
    localObject = ((h)localObject).a(localRecyclerView);
    localArrayList.addAll((Collection)localObject);
    localObject = d;
    localRecyclerView = k;
    localObject = ((k)localObject).a(localRecyclerView);
    localArrayList.addAll((Collection)localObject);
    return localArrayList;
  }
  
  protected final void c()
  {
    k.getAdapter().notifyDataSetChanged();
  }
  
  public final void d()
  {
    b.a(true);
  }
  
  public final int f()
  {
    return 0;
  }
  
  public final int i()
  {
    return 2131233811;
  }
  
  public final com.truecaller.ui.components.i[] j()
  {
    return null;
  }
  
  public final FloatingActionButton.a k()
  {
    i.6 local6 = new com/truecaller/ui/i$6;
    local6.<init>(this);
    return local6;
  }
  
  public final boolean l()
  {
    return true;
  }
  
  public final void m()
  {
    RecyclerView localRecyclerView = k;
    if (localRecyclerView != null) {
      localRecyclerView.smoothScrollToPosition(0);
    }
  }
  
  public final void n()
  {
    super.n();
    Object localObject = ((bk)getActivity().getApplicationContext()).a();
    com.truecaller.i.e locale = ((bp)localObject).F();
    localObject = ((bp)localObject).aI();
    int i1 = Build.VERSION.SDK_INT;
    int i2 = 26;
    if (i1 >= i2)
    {
      boolean bool = ((com.truecaller.common.h.c)localObject).a();
      if (!bool)
      {
        localObject = "general_requestPinContactsShortcutShown";
        bool = locale.b((String)localObject);
        if (!bool)
        {
          localObject = "shortcutsContactListShownTimes";
          int i4 = locale.a_((String)localObject);
          int i5 = 2;
          if (i4 == i5)
          {
            localObject = ((bk)getActivity().getApplicationContext()).a();
            locale = ((bp)localObject).F();
            localObject = ((bp)localObject).aA();
            AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
            android.support.v4.app.f localf = getActivity();
            localBuilder.<init>(localf);
            localBuilder = localBuilder.setMessage(2131887401).setCancelable(false);
            -..Lambda.i.noNrs4fT8u1Ae6qTlZqoZPUPpWw localnoNrs4fT8u1Ae6qTlZqoZPUPpWw = new com/truecaller/ui/-$$Lambda$i$noNrs4fT8u1Ae6qTlZqoZPUPpWw;
            localnoNrs4fT8u1Ae6qTlZqoZPUPpWw.<init>((com.truecaller.util.d.a)localObject);
            localObject = localBuilder.setPositiveButton(2131887400, localnoNrs4fT8u1Ae6qTlZqoZPUPpWw);
            int i3 = 0;
            localf = null;
            ((AlertDialog.Builder)localObject).setNegativeButton(2131888828, null).create().show();
            localObject = "general_requestPinContactsShortcutShown";
            i1 = 1;
            locale.b((String)localObject, i1);
          }
        }
      }
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    boolean bool1 = true;
    setHasOptionsMenu(bool1);
    Object localObject = n;
    a = ((Handler)localObject);
    localObject = com.truecaller.search.local.model.g.a(getContext());
    e = ((com.truecaller.search.local.model.g)localObject);
    localObject = TrueApp.y().a().c();
    h = ((com.truecaller.analytics.b)localObject);
    localObject = getArguments();
    String str1 = null;
    String str2;
    if (localObject != null)
    {
      str2 = "extraBackedUpContacts";
      boolean bool2 = ((Bundle)localObject).getBoolean(str2, false);
      if (bool2) {}
    }
    else
    {
      bool1 = false;
      paramBundle = null;
    }
    g = bool1;
    bool1 = g;
    if (bool1)
    {
      paramBundle = h;
      localObject = new com/truecaller/analytics/bc;
      str1 = "truecallerContacts";
      str2 = "contacts";
      ((bc)localObject).<init>(str1, str2);
      paramBundle.a((com.truecaller.analytics.e)localObject);
    }
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(2131558677, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    com.truecaller.ads.a.e locale = j;
    if (locale != null) {
      locale.d();
    }
  }
  
  public final void onResume()
  {
    super.onResume();
    com.truecaller.search.local.model.g localg = e;
    -..Lambda.i.NNwqTdz4UPywFA6jCWgcs1Uxj0Y localNNwqTdz4UPywFA6jCWgcs1Uxj0Y = new com/truecaller/ui/-$$Lambda$i$NNwqTdz4UPywFA6jCWgcs1Uxj0Y;
    localNNwqTdz4UPywFA6jCWgcs1Uxj0Y.<init>(this);
    localg.a(localNNwqTdz4UPywFA6jCWgcs1Uxj0Y);
  }
  
  public final void onStart()
  {
    super.onStart();
    Context localContext = requireContext();
    BroadcastReceiver localBroadcastReceiver = o;
    String[] arrayOfString = { "com.truecaller.datamanager.DATA_CHANGED" };
    com.truecaller.utils.extensions.i.a(localContext, localBroadcastReceiver, arrayOfString);
  }
  
  public final void onStop()
  {
    super.onStop();
    android.support.v4.content.d locald = android.support.v4.content.d.a(requireContext());
    BroadcastReceiver localBroadcastReceiver = o;
    locald.a(localBroadcastReceiver);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = getActivity();
    Object localObject1 = ((bk)paramBundle.getApplicationContext()).a();
    Object localObject2 = paramView.findViewById(2131362962);
    l = ((View)localObject2);
    localObject2 = new com/truecaller/ui/h;
    Context localContext = getContext();
    int i1 = g;
    ((h)localObject2).<init>(localContext, i1);
    c = ((h)localObject2);
    c.a(this);
    c.k = this;
    localObject2 = new com/truecaller/ui/k;
    localContext = getContext();
    h localh = c;
    ((k)localObject2).<init>(localContext, localh);
    d = ((k)localObject2);
    localObject2 = d;
    ((k)localObject2).a(this);
    boolean bool1 = g;
    boolean bool2 = false;
    localContext = null;
    i1 = 1;
    if (bool1)
    {
      localObject1 = new com/truecaller/ads/a/h;
      ((com.truecaller.ads.a.h)localObject1).<init>();
      j = ((com.truecaller.ads.a.e)localObject1);
    }
    else
    {
      localObject2 = new com/truecaller/ads/a/f;
      localObject3 = ((bp)localObject1).aq();
      Object localObject4 = com.truecaller.ads.k.a().a("/43067329/A*Contacts*Native*GPS").b("CONTACTS").c("contacts");
      localObject5 = new CustomTemplate[i1];
      CustomTemplate localCustomTemplate = CustomTemplate.NATIVE_CONTENT_DUAL_TRACKER;
      localObject5[0] = localCustomTemplate;
      localObject4 = ((k.b)localObject4).a((CustomTemplate[])localObject5).b().c().e();
      localObject1 = ((bp)localObject1).bl();
      ((com.truecaller.ads.a.f)localObject2).<init>((com.truecaller.ads.provider.f)localObject3, (com.truecaller.ads.k)localObject4, (c.d.f)localObject1);
      j = ((com.truecaller.ads.a.e)localObject2);
    }
    localObject1 = new com/truecaller/ads/a/g;
    int i3 = 2131558488;
    k localk = d;
    AdLayoutType localAdLayoutType = AdLayoutType.SMALL;
    com.truecaller.ads.a.c localc = new com/truecaller/ads/a/c;
    localc.<init>();
    com.truecaller.ads.a.e locale = j;
    Object localObject5 = localObject1;
    ((com.truecaller.ads.a.g)localObject1).<init>(i3, localk, localAdLayoutType, localc, locale);
    i = ((com.truecaller.ads.a.a)localObject1);
    localObject1 = (RecyclerView)paramView.findViewById(2131362561);
    k = ((RecyclerView)localObject1);
    localObject1 = k;
    localObject2 = new android/support/v7/widget/LinearLayoutManager;
    Object localObject3 = getContext();
    ((LinearLayoutManager)localObject2).<init>((Context)localObject3, i1, false);
    ((RecyclerView)localObject1).setLayoutManager((RecyclerView.LayoutManager)localObject2);
    localObject1 = k;
    localObject2 = i;
    ((RecyclerView)localObject1).setAdapter((RecyclerView.Adapter)localObject2);
    localObject1 = k;
    localObject2 = new com/truecaller/ui/i$2;
    ((i.2)localObject2).<init>(this);
    ((RecyclerView)localObject1).addOnScrollListener((RecyclerView.OnScrollListener)localObject2);
    int i4 = 2131364498;
    localObject1 = (SideIndexScroller)paramView.findViewById(i4);
    b = ((SideIndexScroller)localObject1);
    localObject1 = b;
    int i2 = 2131365374;
    paramView = (TextView)paramView.findViewById(i2);
    ((SideIndexScroller)localObject1).setFloatingLabel(paramView);
    paramView = b;
    localObject1 = k;
    paramView.setRecyclerView((RecyclerView)localObject1);
    paramView = b;
    localObject1 = new com/truecaller/ui/i$3;
    ((i.3)localObject1).<init>(this);
    paramView.setAdapter((SideIndexScroller.a)localObject1);
    paramView = b;
    localObject1 = new com/truecaller/ui/i$4;
    ((i.4)localObject1).<init>(this);
    paramView.setOnTouchListener((View.OnTouchListener)localObject1);
    paramView = new com/truecaller/ui/-$$Lambda$i$e6vL4tP0-cxtg-tVsClyyAczKOo;
    paramView.<init>(this);
    localObject1 = new com/truecaller/ui/i$5;
    localObject2 = getContext();
    bool2 = com.truecaller.common.e.f.b();
    ((i.5)localObject1).<init>(this, (Context)localObject2, paramView, bool2);
    boolean bool3 = paramBundle instanceof TruecallerInit;
    if (bool3)
    {
      paramBundle = (TruecallerInit)paramBundle;
      paramView = paramBundle.f();
      m = paramView;
    }
    paramView = new com/truecaller/ui/aa;
    paramBundle = getContext();
    paramView.<init>(paramBundle, null, this);
    k.addOnItemTouchListener(paramView);
    k.addItemDecoration((RecyclerView.ItemDecoration)localObject1);
    k.addItemDecoration(paramView);
    paramView = j;
    paramBundle = p;
    paramView.a(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */