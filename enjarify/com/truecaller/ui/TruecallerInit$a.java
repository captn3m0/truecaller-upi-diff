package com.truecaller.ui;

import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.e;
import android.support.v7.app.AlertDialog.Builder;
import android.util.SparseArray;
import android.view.View;
import com.truecaller.TrueApp;
import com.truecaller.analytics.ChosenComponentReceiver;
import com.truecaller.analytics.ChosenComponentReceiver.a;
import com.truecaller.analytics.a.f;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.bp;
import com.truecaller.calling.recorder.bj;
import com.truecaller.common.h.ai;
import com.truecaller.essentialnumber.LocalServicesCategoryActivity;
import com.truecaller.filters.blockedevents.BlockedEventsActivity;
import com.truecaller.referral.ReferralManager;
import com.truecaller.referral.ReferralManager.ReferralLaunchContext;
import com.truecaller.util.dh;
import com.truecaller.utils.d;
import com.truecaller.whoviewedme.WhoViewedMeActivity;
import com.truecaller.whoviewedme.WhoViewedMeLaunchContext;
import com.truecaller.wizard.b.c;

final class TruecallerInit$a
  extends DrawerLayout.e
{
  boolean a = false;
  boolean b = false;
  private SparseArray d;
  
  private TruecallerInit$a(TruecallerInit paramTruecallerInit)
  {
    paramTruecallerInit = new android/util/SparseArray;
    paramTruecallerInit.<init>();
    d = paramTruecallerInit;
    d.put(2131362910, "Notifications");
    d.put(2131362916, "WhoViewedMe");
    d.put(2131362908, "Help");
    d.put(2131362915, "ShareTruecaller");
    d.put(2131362914, "Settings");
    d.put(2131362905, "EssentialNumbers");
    d.put(2131362903, "Blocking");
  }
  
  public final void onDrawerClosed(View paramView)
  {
    super.onDrawerClosed(paramView);
    Object localObject1 = c;
    int i = TruecallerInit.k((TruecallerInit)localObject1);
    Object localObject2;
    int j;
    Object localObject3;
    String str;
    switch (i)
    {
    case 2131362906: 
    case 2131362907: 
    case 2131362909: 
    default: 
      break;
    case 2131362916: 
      paramView = c;
      localObject2 = WhoViewedMeLaunchContext.NAVIGATION_DRAWER;
      localObject1 = WhoViewedMeActivity.a((Context)localObject1, (WhoViewedMeLaunchContext)localObject2);
      paramView.startActivity((Intent)localObject1);
      break;
    case 2131362915: 
      i = 0;
      paramView = null;
      localObject2 = TruecallerInit.m(c);
      j = ((d)localObject2).h();
      int k = 22;
      View localView;
      if (j >= k)
      {
        paramView = ChosenComponentReceiver.a;
        paramView = ChosenComponentReceiver.a.a((Context)localObject1, "Drawer").getIntentSender();
        localView = paramView;
      }
      else
      {
        localView = null;
      }
      paramView = c.getResources();
      j = 2131886653;
      localObject2 = paramView.getString(j);
      paramView = c.getResources();
      k = 2131887144;
      localObject3 = paramView.getString(k);
      paramView = c.getResources();
      int m = 2131887143;
      str = paramView.getString(m);
      ai.a((Context)localObject1, (String)localObject2, (String)localObject3, str, null, localView);
      paramView = TrueApp.y().a().c();
      localObject1 = new com/truecaller/analytics/e$a;
      ((e.a)localObject1).<init>("ViewAction");
      localObject1 = ((e.a)localObject1).a("Context", "sideBar");
      localObject2 = "Action";
      localObject3 = "share";
      localObject1 = ((e.a)localObject1).a((String)localObject2, (String)localObject3).a();
      paramView.a((e)localObject1);
      break;
    case 2131362914: 
      paramView = SettingsFragment.SettingsViewType.SETTINGS_MAIN;
      SettingsFragment.b((Context)localObject1, paramView);
      break;
    case 2131362913: 
      paramView = TrueApp.y().a().c();
      localObject2 = new com/truecaller/analytics/e$a;
      ((e.a)localObject2).<init>("ViewAction");
      localObject2 = ((e.a)localObject2).a("Context", "sideBar");
      localObject3 = "Action";
      str = "feedback";
      localObject2 = ((e.a)localObject2).a((String)localObject3, str).a();
      paramView.a((e)localObject2);
      paramView = SingleActivity.FragmentSingle.FEEDBACK_FORM;
      paramView = SingleActivity.a((Context)localObject1, paramView);
      localObject1 = c;
      ((TruecallerInit)localObject1).startActivity(paramView);
      break;
    case 2131362912: 
      paramView = TruecallerInit.g(c);
      if (paramView != null)
      {
        c.s.a("ab_test_invite_referral_18335_converted");
        paramView = TruecallerInit.g(c);
        localObject1 = ReferralManager.ReferralLaunchContext.NAVIGATION_DRAWER;
        paramView.a((ReferralManager.ReferralLaunchContext)localObject1);
      }
      break;
    case 2131362911: 
      paramView = c;
      TruecallerInit.l(paramView);
      break;
    case 2131362910: 
      u.b((Context)localObject1);
      break;
    case 2131362908: 
      paramView = "https://support.truecaller.com/hc/en-us/categories/201513109-Android";
      j = 0;
      localObject2 = null;
      dh.a((Context)localObject1, paramView, false);
      break;
    case 2131362905: 
      paramView = c;
      localObject2 = "sideBar";
      localObject1 = LocalServicesCategoryActivity.a((Context)localObject1, (String)localObject2);
      paramView.startActivity((Intent)localObject1);
      break;
    case 2131362904: 
      bj.a((Context)localObject1);
      break;
    case 2131362903: 
      paramView = TruecallerInit.h(c);
      boolean bool = paramView.p();
      if (bool)
      {
        bool = c.f();
        if (bool)
        {
          paramView = c;
          localObject2 = new android/content/Intent;
          localObject3 = BlockedEventsActivity.class;
          ((Intent)localObject2).<init>((Context)localObject1, (Class)localObject3);
          paramView.startActivity((Intent)localObject2);
          break;
        }
      }
      paramView = new android/support/v7/app/AlertDialog$Builder;
      paramView.<init>((Context)localObject1);
      paramView = paramView.setTitle(2131887146).setMessage(2131888397);
      j = 2131888396;
      localObject3 = new com/truecaller/ui/-$$Lambda$TruecallerInit$a$3D7xQTKB687_Op13lL-d9dOOsIk;
      ((-..Lambda.TruecallerInit.a.3D7xQTKB687_Op13lL-d9dOOsIk)localObject3).<init>(this, (TruecallerInit)localObject1);
      paramView = paramView.setPositiveButton(j, (DialogInterface.OnClickListener)localObject3);
      paramView.show();
    }
    paramView = d;
    localObject1 = c;
    int n = TruecallerInit.k((TruecallerInit)localObject1);
    paramView = (String)paramView.get(n);
    if (paramView != null)
    {
      localObject1 = new com/truecaller/analytics/e$a;
      ((e.a)localObject1).<init>("ANDROID_MAIN_Menu_Clicked");
      localObject2 = "Item";
      ((e.a)localObject1).a((String)localObject2, paramView);
      paramView = TrueApp.y().a().c();
      localObject1 = ((e.a)localObject1).a();
      paramView.a((e)localObject1);
    }
    TruecallerInit.n(c);
  }
  
  public final void onDrawerOpened(View paramView)
  {
    super.onDrawerOpened(paramView);
    TruecallerInit.o(c);
  }
  
  public final void onDrawerStateChanged(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      break;
    case 1: 
      b = true;
      return;
    case 0: 
      NavigationView localNavigationView = c.j;
      paramInt = DrawerLayout.f(localNavigationView);
      if (paramInt != 0)
      {
        bool1 = a;
        if (!bool1)
        {
          localObject1 = new com/truecaller/analytics/e$a;
          Object localObject2 = "ANDROID_MAIN_Menu_Opened";
          ((e.a)localObject1).<init>((String)localObject2);
          boolean bool2 = b;
          String str;
          if (bool2)
          {
            localObject2 = "Method";
            str = "Swipe";
            ((e.a)localObject1).a((String)localObject2, str);
          }
          else
          {
            localObject2 = "Method";
            str = "MenuButton";
            ((e.a)localObject1).a((String)localObject2, str);
          }
          localObject2 = TrueApp.y().a().c();
          localObject1 = ((e.a)localObject1).a();
          ((b)localObject2).a((e)localObject1);
        }
      }
      boolean bool1 = false;
      Object localObject1 = null;
      b = false;
      a = paramInt;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.TruecallerInit.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */