package com.truecaller.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.e;
import android.support.v7.app.AlertDialog.Builder;
import android.view.LayoutInflater;
import android.view.View;
import com.truecaller.analytics.at;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.old.data.access.Settings;

public final class ac
  extends e
  implements DialogInterface.OnClickListener
{
  private f a;
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = ((bk)paramContext.getApplicationContext()).a().f();
    a = paramContext;
  }
  
  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    String str1;
    String str2;
    switch (paramInt)
    {
    default: 
      break;
    case 1: 
      Settings.b("callLogTapBehavior", "profile");
      localObject = a;
      str1 = "openDetailView";
      str2 = "callHistory";
      at.a((f)localObject, str1, str2);
      break;
    case 0: 
      Settings.b("callLogTapBehavior", "call");
      localObject = a;
      str1 = "initiateCall";
      str2 = "callHistory";
      at.a((f)localObject, str1, str2);
    }
    Object localObject = getParentFragment();
    boolean bool = localObject instanceof DialogInterface.OnClickListener;
    if (bool)
    {
      bool = ((Fragment)localObject).isAdded();
      if (bool)
      {
        localObject = (DialogInterface.OnClickListener)localObject;
        ((DialogInterface.OnClickListener)localObject).onClick(paramDialogInterface, paramInt);
      }
    }
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    Object localObject1 = Settings.b("callLogTapBehavior");
    int i = "call".equals(localObject1);
    localObject1 = new android/support/v7/app/AlertDialog$Builder;
    Object localObject2 = getContext();
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    localObject2 = LayoutInflater.from(getContext()).inflate(2131558598, null, false);
    localObject1 = ((AlertDialog.Builder)localObject1).setCustomTitle((View)localObject2);
    i ^= 0x1;
    return ((AlertDialog.Builder)localObject1).setSingleChoiceItems(2130903047, i, this).setPositiveButton(2131887202, this).create();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */