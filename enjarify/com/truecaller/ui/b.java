package com.truecaller.ui;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;
import java.util.List;

public abstract class b
  extends RecyclerView.Adapter
{
  private RecyclerView.Adapter a;
  
  public b(RecyclerView.Adapter paramAdapter)
  {
    a = paramAdapter;
    paramAdapter = a;
    b.a locala = new com/truecaller/ui/b$a;
    locala.<init>(this, (byte)0);
    paramAdapter.registerAdapterDataObserver(locala);
    boolean bool = a.hasStableIds();
    super.setHasStableIds(bool);
  }
  
  public int b(int paramInt)
  {
    return paramInt;
  }
  
  public int c(int paramInt)
  {
    return paramInt;
  }
  
  public boolean d(int paramInt)
  {
    return false;
  }
  
  public int getItemCount()
  {
    return a.getItemCount();
  }
  
  public long getItemId(int paramInt)
  {
    RecyclerView.Adapter localAdapter = a;
    paramInt = b(paramInt);
    return localAdapter.getItemId(paramInt);
  }
  
  public int getItemViewType(int paramInt)
  {
    RecyclerView.Adapter localAdapter = a;
    paramInt = b(paramInt);
    return localAdapter.getItemViewType(paramInt);
  }
  
  public void onAttachedToRecyclerView(RecyclerView paramRecyclerView)
  {
    a.onAttachedToRecyclerView(paramRecyclerView);
  }
  
  public void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    RecyclerView.Adapter localAdapter = a;
    paramInt = b(paramInt);
    localAdapter.onBindViewHolder(paramViewHolder, paramInt);
  }
  
  public void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt, List paramList)
  {
    RecyclerView.Adapter localAdapter = a;
    paramInt = b(paramInt);
    localAdapter.onBindViewHolder(paramViewHolder, paramInt, paramList);
  }
  
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    return a.onCreateViewHolder(paramViewGroup, paramInt);
  }
  
  public void onDetachedFromRecyclerView(RecyclerView paramRecyclerView)
  {
    a.onDetachedFromRecyclerView(paramRecyclerView);
  }
  
  public final boolean onFailedToRecycleView(RecyclerView.ViewHolder paramViewHolder)
  {
    int i = paramViewHolder.getItemViewType();
    boolean bool1 = d(i);
    if (!bool1)
    {
      RecyclerView.Adapter localAdapter = a;
      boolean bool2 = localAdapter.onFailedToRecycleView(paramViewHolder);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public void onViewAttachedToWindow(RecyclerView.ViewHolder paramViewHolder)
  {
    int i = paramViewHolder.getItemViewType();
    boolean bool = d(i);
    if (!bool)
    {
      RecyclerView.Adapter localAdapter = a;
      localAdapter.onViewAttachedToWindow(paramViewHolder);
    }
  }
  
  public void onViewDetachedFromWindow(RecyclerView.ViewHolder paramViewHolder)
  {
    int i = paramViewHolder.getItemViewType();
    boolean bool = d(i);
    if (!bool)
    {
      RecyclerView.Adapter localAdapter = a;
      localAdapter.onViewDetachedFromWindow(paramViewHolder);
    }
  }
  
  public final void onViewRecycled(RecyclerView.ViewHolder paramViewHolder)
  {
    int i = paramViewHolder.getItemViewType();
    boolean bool = d(i);
    if (!bool)
    {
      RecyclerView.Adapter localAdapter = a;
      localAdapter.onViewRecycled(paramViewHolder);
    }
  }
  
  public void setHasStableIds(boolean paramBoolean)
  {
    super.setHasStableIds(paramBoolean);
    a.setHasStableIds(paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */