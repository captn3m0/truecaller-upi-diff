package com.truecaller.ui;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import java.lang.ref.WeakReference;
import kotlinx.coroutines.g;

final class ag$b
  extends c.d.b.a.k
  implements m
{
  int a;
  int b;
  private kotlinx.coroutines.ag e;
  
  ag$b(ag paramag, WeakReference paramWeakReference, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/ui/ag$b;
    ag localag = c;
    WeakReference localWeakReference = d;
    localb.<init>(localag, localWeakReference, paramc);
    paramObject = (kotlinx.coroutines.ag)paramObject;
    e = ((kotlinx.coroutines.ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = b;
    int j = 0;
    boolean bool;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      k = a;
      bool = paramObject instanceof o.b;
      if (!bool) {
        break label237;
      }
      throw a;
    case 1: 
      bool = paramObject instanceof o.b;
      if (bool) {
        throw a;
      }
      break;
    case 0: 
      bool = paramObject instanceof o.b;
      if (bool) {
        break label280;
      }
      paramObject = c.c;
      localObject1 = new com/truecaller/ui/ag$b$b;
      ((ag.b.b)localObject1).<init>(this, null);
      localObject1 = (m)localObject1;
      int m = 1;
      b = m;
      paramObject = g.a((f)paramObject, (m)localObject1, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    paramObject = (Number)paramObject;
    int n = ((Number)paramObject).intValue();
    Object localObject1 = c.c;
    Object localObject2 = new com/truecaller/ui/ag$b$a;
    ((ag.b.a)localObject2).<init>(this, null);
    localObject2 = (m)localObject2;
    a = n;
    j = 2;
    b = j;
    localObject1 = g.a((f)localObject1, (m)localObject2, this);
    if (localObject1 == locala) {
      return locala;
    }
    int k = n;
    paramObject = localObject1;
    label237:
    paramObject = (Number)paramObject;
    n = ((Number)paramObject).intValue();
    localObject1 = (ag.a)d.get();
    if (localObject1 != null) {
      ((ag.a)localObject1).a(n, k);
    }
    return x.a;
    label280:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.ag.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */