package com.truecaller.ui;

import android.view.View;
import com.truecaller.common.e.f;
import com.truecaller.old.a.a;
import com.truecaller.old.data.access.Settings;
import com.truecaller.old.data.access.d;
import com.truecaller.old.data.entity.b;
import com.truecaller.ui.components.ComboBase;
import com.truecaller.ui.components.ComboBase.a;
import com.truecaller.ui.components.n;
import com.truecaller.utils.extensions.t;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

final class SettingsFragment$2
  extends a
{
  List a;
  b c;
  b d;
  String e;
  
  SettingsFragment$2(SettingsFragment paramSettingsFragment, com.truecaller.old.a.c paramc, ComboBase paramComboBase)
  {
    super(paramc, (byte)0);
  }
  
  public final void a(Object paramObject)
  {
    paramObject = g;
    boolean bool1 = ((SettingsFragment)paramObject).s();
    if (bool1)
    {
      paramObject = f;
      boolean bool2 = true;
      float f1 = 0.5F;
      t.a((View)paramObject, bool2, f1);
      paramObject = f;
      Object localObject = a;
      ((ComboBase)paramObject).setData((List)localObject);
      paramObject = f;
      localObject = c;
      ((ComboBase)paramObject).setSelection((n)localObject);
      f.a();
      paramObject = f;
      localObject = new com/truecaller/ui/-$$Lambda$SettingsFragment$2$Bb6aXqZ7FaeH5y2J250CZ7Y5-ds;
      ((-..Lambda.SettingsFragment.2.Bb6aXqZ7FaeH5y2J250CZ7Y5-ds)localObject).<init>(this);
      ((ComboBase)paramObject).a((ComboBase.a)localObject);
    }
  }
  
  protected final Object doInBackground(Object... paramVarArgs)
  {
    paramVarArgs = new java/util/ArrayList;
    paramVarArgs.<init>();
    a = paramVarArgs;
    paramVarArgs = f.a();
    if (paramVarArgs != null)
    {
      paramVarArgs = d.a(paramVarArgs);
      localObject1 = new com/truecaller/old/data/entity/b;
      String str1 = e;
      int i = 1;
      Object localObject2 = new Object[i];
      String str2 = a.a;
      localObject2[0] = str2;
      str1 = String.format(str1, (Object[])localObject2);
      localObject2 = a.b;
      paramVarArgs = a.c;
      ((b)localObject1).<init>(str1, (String)localObject2, paramVarArgs);
      d = ((b)localObject1);
      paramVarArgs = Settings.l();
      localObject1 = "auto";
      boolean bool = paramVarArgs.equals(localObject1);
      if (bool)
      {
        paramVarArgs = d;
        c = paramVarArgs;
      }
      paramVarArgs = a;
      localObject1 = d;
      paramVarArgs.add(localObject1);
    }
    paramVarArgs = a;
    Object localObject1 = d.b();
    paramVarArgs.addAll((Collection)localObject1);
    paramVarArgs = c;
    if (paramVarArgs == null)
    {
      paramVarArgs = d.a(Settings.b("t9_lang"));
      c = paramVarArgs;
    }
    return null;
  }
  
  public final void onPreExecute()
  {
    t.a(f, false, 0.5F);
    String str = g.getString(2131887073);
    e = str;
    super.onPreExecute();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.SettingsFragment.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */