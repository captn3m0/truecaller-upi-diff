package com.truecaller.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore.Audio.Media;
import android.provider.Settings.System;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.truecaller.TrueApp;
import com.truecaller.analytics.bc;
import com.truecaller.analytics.e.a;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.calling.recorder.CallRecordingSettingsActivity;
import com.truecaller.common.account.r;
import com.truecaller.common.h.am;
import com.truecaller.common.h.o;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.filters.blockedlist.BlockedListActivity;
import com.truecaller.filters.s;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.multisim.SimInfo;
import com.truecaller.old.data.access.Settings;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.br;
import com.truecaller.profile.business.CreateBusinessProfileActivity;
import com.truecaller.service.RefreshT9MappingService;
import com.truecaller.swish.g.a;
import com.truecaller.truepay.Truepay;
import com.truecaller.truepay.app.ui.dashboard.views.activities.SettingsActivity;
import com.truecaller.ui.components.ComboBase;
import com.truecaller.ui.components.ComboBase.a;
import com.truecaller.ui.components.n;
import com.truecaller.util.RingtoneUtils.Ringtone;
import com.truecaller.util.RingtoneUtils.a;
import com.truecaller.util.RingtoneUtils.c;
import com.truecaller.util.at;
import com.truecaller.util.bv;
import com.truecaller.utils.extensions.t;
import com.truecaller.utils.l;
import com.truecaller.wizard.utils.PermissionPoller;
import com.truecaller.wizard.utils.PermissionPoller.Permission;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class SettingsFragment
  extends x
  implements View.OnClickListener, RingtoneUtils.c
{
  private com.truecaller.calling.e.e A;
  private bw B;
  private final List C;
  private Handler D;
  private PermissionPoller E;
  private android.support.v4.app.j F;
  private boolean G;
  private boolean H;
  private boolean I;
  private com.truecaller.notificationchannels.j J;
  private boolean K;
  private boolean L;
  private boolean M;
  private String N;
  private String O;
  private com.truecaller.common.profile.e P;
  private com.truecaller.androidactors.k Q;
  private com.truecaller.androidactors.f R;
  private com.truecaller.androidactors.a S;
  private com.truecaller.androidactors.f T;
  private com.truecaller.androidactors.a U;
  private com.truecaller.calling.recorder.h V;
  private com.truecaller.common.h.c W;
  private com.truecaller.common.e.e X;
  private BroadcastReceiver Y;
  private CompoundButton.OnCheckedChangeListener Z;
  protected SettingsFragment.SettingsViewType a;
  private CompoundButton.OnCheckedChangeListener aa;
  private CompoundButton.OnCheckedChangeListener ab;
  private CompoundButton.OnCheckedChangeListener ac;
  private CompoundButton.OnCheckedChangeListener ad;
  protected ComboBase b;
  protected ComboBase c;
  LinearLayout d;
  private ViewStub f;
  private ComboBase g;
  private TextView h;
  private com.truecaller.messaging.h i;
  private com.truecaller.i.c l;
  private com.truecaller.common.g.a m;
  private com.truecaller.utils.d n;
  private bv o;
  private com.truecaller.analytics.a.a p;
  private com.truecaller.filters.p q;
  private com.truecaller.androidactors.f r;
  private com.truecaller.featuretoggles.e s;
  private com.truecaller.flashsdk.core.b t;
  private com.truecaller.analytics.b u;
  private r v;
  private com.truecaller.common.f.c w;
  private com.truecaller.whoviewedme.w x;
  private l y;
  private br z;
  
  public SettingsFragment()
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    C = ((List)localObject);
    localObject = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    ((Handler)localObject).<init>(localLooper);
    D = ((Handler)localObject);
    L = false;
    M = true;
    N = null;
    O = null;
    U = null;
    localObject = new com/truecaller/ui/SettingsFragment$1;
    ((SettingsFragment.1)localObject).<init>(this);
    Y = ((BroadcastReceiver)localObject);
  }
  
  private void A()
  {
    Object localObject1 = o();
    int j = 2131364360;
    localObject1 = at.g((View)localObject1, j);
    boolean bool1 = false;
    Object localObject2 = null;
    ((SwitchCompat)localObject1).setOnCheckedChangeListener(null);
    localObject1 = o();
    boolean bool3 = q.g();
    at.c((View)localObject1, j, bool3);
    localObject1 = o();
    Object localObject3 = s.L();
    bool3 = ((com.truecaller.featuretoggles.b)localObject3).a();
    int i4 = 1;
    if (bool3)
    {
      localObject3 = w;
      bool3 = ((com.truecaller.common.f.c)localObject3).d();
      if (!bool3)
      {
        localObject3 = q;
        bool3 = ((com.truecaller.filters.p)localObject3).g();
        if (!bool3)
        {
          bool3 = true;
          break label130;
        }
      }
    }
    bool3 = false;
    localObject3 = null;
    label130:
    at.a((View)localObject1, 2131364359, bool3);
    localObject1 = at.g(o(), j);
    Object localObject4 = Z;
    ((SwitchCompat)localObject1).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject4);
    localObject1 = o();
    j = 2131364347;
    at.g((View)localObject1, j).setOnCheckedChangeListener(null);
    localObject1 = o();
    localObject3 = q;
    bool3 = ((com.truecaller.filters.p)localObject3).a();
    at.c((View)localObject1, j, bool3);
    localObject1 = o();
    int i1 = 2131364346;
    Object localObject5 = s.K();
    boolean bool7 = ((com.truecaller.featuretoggles.b)localObject5).a();
    if (bool7)
    {
      localObject5 = w;
      bool7 = ((com.truecaller.common.f.c)localObject5).d();
      if (!bool7)
      {
        localObject5 = q;
        bool7 = ((com.truecaller.filters.p)localObject5).a();
        if (!bool7)
        {
          bool7 = true;
          break label288;
        }
      }
    }
    bool7 = false;
    localObject5 = null;
    label288:
    at.a((View)localObject1, i1, bool7);
    localObject1 = at.g(o(), j);
    localObject4 = aa;
    ((SwitchCompat)localObject1).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject4);
    localObject1 = o();
    boolean bool4 = l.a("blockCallNotification", i4);
    at.c((View)localObject1, 2131364341, bool4);
    localObject1 = o();
    bool4 = i.i();
    at.c((View)localObject1, 2131364352, bool4);
    localObject1 = o();
    j = 2131364356;
    at.g((View)localObject1, j).setOnCheckedChangeListener(null);
    localObject1 = o();
    localObject3 = q;
    bool4 = ((com.truecaller.filters.p)localObject3).b();
    at.c((View)localObject1, j, bool4);
    localObject1 = o();
    int i2 = 2131364355;
    localObject5 = s.N();
    bool7 = ((com.truecaller.featuretoggles.b)localObject5).a();
    if (bool7)
    {
      localObject5 = w;
      bool7 = ((com.truecaller.common.f.c)localObject5).d();
      if (!bool7)
      {
        localObject5 = q;
        bool7 = ((com.truecaller.filters.p)localObject5).b();
        if (!bool7)
        {
          bool7 = true;
          break label498;
        }
      }
    }
    bool7 = false;
    localObject5 = null;
    label498:
    at.a((View)localObject1, i2, bool7);
    localObject1 = at.g(o(), j);
    localObject4 = ab;
    ((SwitchCompat)localObject1).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject4);
    localObject1 = o();
    j = 2131364344;
    at.g((View)localObject1, j).setOnCheckedChangeListener(null);
    localObject1 = o();
    localObject3 = q;
    boolean bool5 = ((com.truecaller.filters.p)localObject3).c();
    at.c((View)localObject1, j, bool5);
    localObject1 = o();
    int i3 = 2131364343;
    localObject5 = s.P();
    bool7 = ((com.truecaller.featuretoggles.b)localObject5).a();
    if (bool7)
    {
      localObject5 = w;
      bool7 = ((com.truecaller.common.f.c)localObject5).d();
      if (!bool7)
      {
        localObject5 = q;
        bool7 = ((com.truecaller.filters.p)localObject5).c();
        if (!bool7)
        {
          bool7 = true;
          break label656;
        }
      }
    }
    bool7 = false;
    localObject5 = null;
    label656:
    at.a((View)localObject1, i3, bool7);
    localObject1 = at.g(o(), j);
    localObject4 = ac;
    ((SwitchCompat)localObject1).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject4);
    localObject1 = o();
    j = 2131364350;
    at.g((View)localObject1, j).setOnCheckedChangeListener(null);
    localObject1 = o();
    localObject2 = q;
    bool1 = ((com.truecaller.filters.p)localObject2).e();
    at.c((View)localObject1, j, bool1);
    localObject1 = o();
    int k = 2131364349;
    localObject3 = s.S();
    boolean bool6 = ((com.truecaller.featuretoggles.b)localObject3).a();
    if (bool6)
    {
      localObject3 = w;
      bool6 = ((com.truecaller.common.f.c)localObject3).d();
      if (!bool6)
      {
        localObject3 = q;
        bool6 = ((com.truecaller.filters.p)localObject3).e();
        if (!bool6)
        {
          bool6 = true;
          break label811;
        }
      }
    }
    bool6 = false;
    localObject3 = null;
    label811:
    at.a((View)localObject1, k, bool6);
    localObject1 = at.g(o(), j);
    localObject4 = ad;
    ((SwitchCompat)localObject1).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject4);
    localObject1 = o();
    j = 2131364353;
    localObject1 = at.h((View)localObject1, j);
    if (localObject1 != null)
    {
      localObject4 = ((ComboBase)localObject1).getItems();
      localObject2 = l;
      localObject3 = "blockCallMethod";
      boolean bool2 = Settings.a(((com.truecaller.i.c)localObject2).a((String)localObject3, 0));
      if (bool2) {
        localObject4 = ((List)localObject4).get(0);
      } else {
        localObject4 = ((List)localObject4).get(i4);
      }
      localObject4 = (n)localObject4;
      ((ComboBase)localObject1).setSelection((n)localObject4);
    }
  }
  
  private void B()
  {
    SettingsFragment.SettingsViewType localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_GENERAL;
    a(2131887081, localSettingsViewType, 0);
  }
  
  private void C()
  {
    SettingsFragment.SettingsViewType localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_CALLERID;
    a(2131887080, localSettingsViewType, 0);
  }
  
  private void D()
  {
    SettingsFragment.SettingsViewType localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_ABOUT;
    a(2131887079, localSettingsViewType, 0);
  }
  
  private void E()
  {
    SettingsFragment.SettingsViewType localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_APPEARANCE;
    a(2131887060, localSettingsViewType, 0);
  }
  
  private void F()
  {
    y();
    SettingsFragment.SettingsViewType localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_RINGTONE;
    a(2131887114, localSettingsViewType, 0);
  }
  
  private void G()
  {
    z();
    SettingsFragment.SettingsViewType localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_MESSAGING;
    a(2131887093, localSettingsViewType, 0);
  }
  
  private void H()
  {
    A();
    SettingsFragment.SettingsViewType localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_BLOCK;
    a(2131887055, localSettingsViewType, 0);
  }
  
  private void I()
  {
    SettingsFragment.SettingsViewType localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_BACKUP;
    a(2131887054, localSettingsViewType, 0);
  }
  
  private void J()
  {
    Object localObject1 = y;
    Object localObject2 = "android.permission.WRITE_EXTERNAL_STORAGE";
    Object localObject3 = { "android.permission.READ_EXTERNAL_STORAGE", localObject2 };
    boolean bool = ((l)localObject1).a((String[])localObject3);
    if (bool)
    {
      localObject1 = getActivitygetApplicationc;
      localObject3 = new com/truecaller/util/RingtoneUtils$a;
      localObject2 = getContext();
      RingtoneUtils.Ringtone localRingtone = RingtoneUtils.Ringtone.Ringtone;
      -..Lambda.SettingsFragment.l6DtwaXI7VTrSVUxpeZlUptFO8s locall6DtwaXI7VTrSVUxpeZlUptFO8s = new com/truecaller/ui/-$$Lambda$SettingsFragment$l6DtwaXI7VTrSVUxpeZlUptFO8s;
      locall6DtwaXI7VTrSVUxpeZlUptFO8s.<init>(this);
      ((RingtoneUtils.a)localObject3).<init>((Context)localObject2, localRingtone, locall6DtwaXI7VTrSVUxpeZlUptFO8s, this);
      ((com.truecaller.common.background.b)localObject1).a((Runnable)localObject3);
      return;
    }
    localObject1 = y;
    localObject3 = new String[] { "android.permission.READ_EXTERNAL_STORAGE" };
    bool = ((l)localObject1).a((String[])localObject3);
    int j = 5;
    if (!bool)
    {
      a("android.permission.READ_EXTERNAL_STORAGE", j);
      return;
    }
    localObject1 = y;
    localObject2 = new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" };
    bool = ((l)localObject1).a((String[])localObject2);
    if (!bool)
    {
      localObject1 = "android.permission.WRITE_EXTERNAL_STORAGE";
      a((String)localObject1, j);
    }
  }
  
  private void K()
  {
    Object localObject1 = y;
    Object localObject2 = "android.permission.WRITE_EXTERNAL_STORAGE";
    Object localObject3 = { "android.permission.READ_EXTERNAL_STORAGE", localObject2 };
    boolean bool = ((l)localObject1).a((String[])localObject3);
    if (bool)
    {
      localObject1 = getActivitygetApplicationc;
      localObject3 = new com/truecaller/util/RingtoneUtils$a;
      localObject2 = getContext();
      RingtoneUtils.Ringtone localRingtone = RingtoneUtils.Ringtone.Message;
      -..Lambda.SettingsFragment.rfg1jKCKirsyIkyJpOed3h3MIeg localrfg1jKCKirsyIkyJpOed3h3MIeg = new com/truecaller/ui/-$$Lambda$SettingsFragment$rfg1jKCKirsyIkyJpOed3h3MIeg;
      localrfg1jKCKirsyIkyJpOed3h3MIeg.<init>(this);
      ((RingtoneUtils.a)localObject3).<init>((Context)localObject2, localRingtone, localrfg1jKCKirsyIkyJpOed3h3MIeg, this);
      ((com.truecaller.common.background.b)localObject1).a((Runnable)localObject3);
      return;
    }
    localObject1 = y;
    localObject3 = new String[] { "android.permission.READ_EXTERNAL_STORAGE" };
    bool = ((l)localObject1).a((String[])localObject3);
    int j = 6;
    if (!bool)
    {
      a("android.permission.READ_EXTERNAL_STORAGE", j);
      return;
    }
    localObject1 = y;
    localObject2 = new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" };
    bool = ((l)localObject1).a((String[])localObject2);
    if (!bool)
    {
      localObject1 = "android.permission.WRITE_EXTERNAL_STORAGE";
      a((String)localObject1, j);
    }
  }
  
  private void L()
  {
    Object localObject1 = y;
    Object localObject2 = "android.permission.WRITE_EXTERNAL_STORAGE";
    Object localObject3 = { "android.permission.READ_EXTERNAL_STORAGE", localObject2 };
    boolean bool = ((l)localObject1).a((String[])localObject3);
    if (bool)
    {
      localObject1 = getActivitygetApplicationc;
      localObject3 = new com/truecaller/util/RingtoneUtils$a;
      localObject2 = getContext();
      RingtoneUtils.Ringtone localRingtone = RingtoneUtils.Ringtone.FlashRingtone;
      -..Lambda.SettingsFragment.bCfn67n3XQFMOxJ4KbHoYhIPvAE localbCfn67n3XQFMOxJ4KbHoYhIPvAE = new com/truecaller/ui/-$$Lambda$SettingsFragment$bCfn67n3XQFMOxJ4KbHoYhIPvAE;
      localbCfn67n3XQFMOxJ4KbHoYhIPvAE.<init>(this);
      ((RingtoneUtils.a)localObject3).<init>((Context)localObject2, localRingtone, localbCfn67n3XQFMOxJ4KbHoYhIPvAE, this);
      ((com.truecaller.common.background.b)localObject1).a((Runnable)localObject3);
      return;
    }
    localObject1 = y;
    localObject3 = new String[] { "android.permission.READ_EXTERNAL_STORAGE" };
    bool = ((l)localObject1).a((String[])localObject3);
    int j = 7;
    if (!bool)
    {
      a("android.permission.READ_EXTERNAL_STORAGE", j);
      return;
    }
    localObject1 = y;
    localObject2 = new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" };
    bool = ((l)localObject1).a((String[])localObject2);
    if (!bool)
    {
      localObject1 = "android.permission.WRITE_EXTERNAL_STORAGE";
      a((String)localObject1, j);
    }
  }
  
  private static ContentValues M()
  {
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    Boolean localBoolean = Boolean.TRUE;
    localContentValues.put("is_ringtone", localBoolean);
    localBoolean = Boolean.TRUE;
    localContentValues.put("is_notification", localBoolean);
    return localContentValues;
  }
  
  public static Intent a(Context paramContext, SettingsFragment.SettingsViewType paramSettingsViewType)
  {
    SingleActivity.FragmentSingle localFragmentSingle = SingleActivity.FragmentSingle.SETTINGS_MAIN;
    paramContext = SingleActivity.a(paramContext, localFragmentSingle).setFlags(67108864);
    paramSettingsViewType = paramSettingsViewType.toString();
    return paramContext.putExtra("ARG_SUBVIEW", paramSettingsViewType);
  }
  
  private void a(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    AlertDialog.Builder localBuilder1 = new android/support/v7/app/AlertDialog$Builder;
    Context localContext = getContext();
    localBuilder1.<init>(localContext);
    AlertDialog.Builder localBuilder2 = localBuilder1.setTitle(paramInt1).setMessage(paramInt2);
    -..Lambda.SettingsFragment.QWW1rtOtfcjxO-RQLEzlYtLIGTU localQWW1rtOtfcjxO-RQLEzlYtLIGTU = new com/truecaller/ui/-$$Lambda$SettingsFragment$QWW1rtOtfcjxO-RQLEzlYtLIGTU;
    localQWW1rtOtfcjxO-RQLEzlYtLIGTU.<init>(this, paramBoolean);
    localBuilder2.setPositiveButton(2131887235, localQWW1rtOtfcjxO-RQLEzlYtLIGTU).setNegativeButton(2131887214, null).show();
  }
  
  private void a(int paramInt, Uri paramUri)
  {
    Object localObject = o();
    TextView localTextView = (TextView)((View)localObject).findViewById(paramInt);
    if (paramUri != null)
    {
      localObject = o.c();
      boolean bool = paramUri.equals(localObject);
      if (bool)
      {
        paramUri = RingtoneUtils.Ringtone.Message.getTitle();
        localTextView.setText(paramUri);
        return;
      }
      localObject = o.e();
      bool = paramUri.equals(localObject);
      if (bool)
      {
        paramUri = RingtoneUtils.Ringtone.FlashRingtone.getTitle();
        localTextView.setText(paramUri);
        return;
      }
      localObject = getActivity();
      paramUri = RingtoneManager.getRingtone((Context)localObject, paramUri);
      if (paramUri != null)
      {
        paramUri = paramUri.getTitle((Context)localObject);
        localTextView.setText(paramUri);
        return;
      }
    }
    localTextView.setText(2131886999);
  }
  
  private void a(int paramInt1, SettingsFragment.SettingsViewType paramSettingsViewType, int paramInt2)
  {
    View localView = o();
    -..Lambda.SettingsFragment.t2k62N34vy8qzDBLcvFXVHwiQ7A localt2k62N34vy8qzDBLcvFXVHwiQ7A = new com/truecaller/ui/-$$Lambda$SettingsFragment$t2k62N34vy8qzDBLcvFXVHwiQ7A;
    localt2k62N34vy8qzDBLcvFXVHwiQ7A.<init>(this, paramSettingsViewType, paramInt1, paramInt2);
    SettingsFragment.SettingsViewType localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_MAIN;
    long l1;
    if (paramSettingsViewType == localSettingsViewType) {
      l1 = 0L;
    } else {
      l1 = 250L;
    }
    localView.postDelayed(localt2k62N34vy8qzDBLcvFXVHwiQ7A, l1);
  }
  
  private void a(Uri paramUri)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.RINGTONE_PICKER");
    String str = "android.intent.extra.ringtone.DEFAULT_URI";
    localIntent.putExtra(str, paramUri);
    int j = 1;
    localIntent.putExtra("android.intent.extra.ringtone.SHOW_SILENT", j);
    localIntent.putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", j);
    Object localObject = getActivity();
    int k = 2131887116;
    localObject = ((android.support.v4.app.f)localObject).getString(k);
    localIntent.putExtra("android.intent.extra.ringtone.TITLE", (String)localObject);
    paramUri = RingtoneManager.getActualDefaultRingtoneUri(getActivity(), j);
    if (paramUri != null)
    {
      localObject = "android.intent.extra.ringtone.EXISTING_URI";
      localIntent.putExtra((String)localObject, paramUri);
    }
    localIntent.putExtra("android.intent.extra.ringtone.TYPE", j);
    startActivityForResult(localIntent, j);
  }
  
  private static void a(TextView paramTextView, int paramInt)
  {
    Drawable localDrawable = com.truecaller.utils.ui.b.a(paramTextView.getContext(), paramInt, 2130969592);
    android.support.v4.widget.m.a(paramTextView, localDrawable, null);
  }
  
  private void a(Integer paramInteger)
  {
    U = null;
    if (paramInteger == null) {
      paramInteger = Integer.valueOf(0);
    }
    TextView localTextView = h;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramInteger;
    paramInteger = getString(2131887301, arrayOfObject);
    localTextView.setText(paramInteger);
  }
  
  private void a(String paramString)
  {
    Object localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>("PermissionChanged");
    ((e.a)localObject).a("Context", "settings_screen").a("Permission", paramString).a("State", "Asked");
    paramString = u;
    localObject = ((e.a)localObject).a();
    paramString.a((com.truecaller.analytics.e)localObject);
  }
  
  private void a(String paramString1, String paramString2, String paramString3)
  {
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("SettingChanged");
    locala.a("Context", paramString1).a("Setting", paramString2).a("State", paramString3);
    paramString1 = u;
    paramString2 = locala.a();
    paramString1.a(paramString2);
  }
  
  private void a(boolean paramBoolean, int paramInt)
  {
    com.truecaller.messaging.h localh = i;
    localh.a(paramInt, paramBoolean);
    String str;
    if (paramBoolean) {
      str = "Enabled";
    } else {
      str = "Disabled";
    }
    a("Setting", "SmsDeliveryReport", str);
  }
  
  private boolean a(String paramString, int paramInt)
  {
    int j = C.size();
    paramInt += j;
    return com.truecaller.wizard.utils.i.a(this, paramString, paramInt, true);
  }
  
  public static void b(Context paramContext, SettingsFragment.SettingsViewType paramSettingsViewType)
  {
    paramSettingsViewType = a(paramContext, paramSettingsViewType);
    paramContext.startActivity(paramSettingsViewType);
  }
  
  private void b(Uri paramUri)
  {
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = i;
    boolean bool1 = ((com.truecaller.messaging.h)localObject2).q();
    if (!bool1)
    {
      localObject2 = i;
      localObject3 = paramUri.toString();
      ((com.truecaller.messaging.h)localObject2).a((String)localObject3);
    }
    localObject2 = o.a();
    Object localObject3 = o.c();
    boolean bool2 = ((Uri)localObject3).equals(localObject2);
    if (!bool2) {
      paramUri = (Uri)localObject2;
    }
    localObject2 = new android/content/Intent;
    ((Intent)localObject2).<init>("android.intent.action.RINGTONE_PICKER");
    ((Intent)localObject2).putExtra("android.intent.extra.ringtone.EXISTING_URI", paramUri);
    bool2 = true;
    ((Intent)localObject2).putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", bool2);
    ((Intent)localObject2).putExtra("android.intent.extra.ringtone.SHOW_SILENT", bool2);
    int j = 2;
    ((Intent)localObject2).putExtra("android.intent.extra.ringtone.DEFAULT_URI", j);
    ((Intent)localObject2).putExtra("android.intent.extra.ringtone.TYPE", j);
    localObject1 = ((Activity)localObject1).getString(2131887089);
    ((Intent)localObject2).putExtra("android.intent.extra.ringtone.TITLE", (String)localObject1);
    startActivityForResult((Intent)localObject2, j);
  }
  
  private void b(String paramString1, String paramString2, String paramString3)
  {
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>(paramString1);
    locala.a(paramString2, paramString3);
    paramString1 = u;
    paramString2 = locala.a();
    paramString1.b(paramString2);
  }
  
  private static boolean b()
  {
    Object localObject = com.truecaller.common.h.h.b();
    if (localObject != null)
    {
      String str = c;
      if (str != null)
      {
        str = "br";
        localObject = c;
        Locale localLocale = Locale.ENGLISH;
        localObject = ((String)localObject).toLowerCase(localLocale);
        boolean bool = str.equals(localObject);
        if (bool) {
          return true;
        }
      }
      return false;
    }
    return false;
  }
  
  private void c()
  {
    boolean bool = y.d();
    K = bool;
  }
  
  public static void c(Context paramContext, SettingsFragment.SettingsViewType paramSettingsViewType)
  {
    SettingsFragment.SettingsViewType localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_MAIN;
    boolean bool;
    if (localSettingsViewType == paramSettingsViewType)
    {
      bool = true;
    }
    else
    {
      bool = false;
      localSettingsViewType = null;
    }
    String[] arrayOfString = new String[0];
    AssertionUtil.isFalse(bool, arrayOfString);
    paramSettingsViewType = a(paramContext, paramSettingsViewType).putExtra("returnToMain", false);
    paramContext.startActivity(paramSettingsViewType);
  }
  
  private void c(Uri paramUri)
  {
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    localObject1 = t;
    boolean bool1 = ((com.truecaller.flashsdk.core.b)localObject1).c();
    if (!bool1)
    {
      localObject1 = t;
      localObject2 = paramUri.toString();
      ((com.truecaller.flashsdk.core.b)localObject1).d((String)localObject2);
    }
    localObject1 = t.d();
    Object localObject2 = o.e();
    boolean bool2 = ((Uri)localObject2).equals(localObject1);
    if (!bool2) {
      paramUri = (Uri)localObject1;
    }
    localObject1 = new android/content/Intent;
    ((Intent)localObject1).<init>("android.intent.action.RINGTONE_PICKER");
    ((Intent)localObject1).putExtra("android.intent.extra.ringtone.SHOW_SILENT", false);
    int j = 1;
    ((Intent)localObject1).putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", j);
    String str = getActivity().getString(2131887066);
    ((Intent)localObject1).putExtra("android.intent.extra.ringtone.TITLE", str);
    ((Intent)localObject1).putExtra("android.intent.extra.ringtone.EXISTING_URI", paramUri);
    ((Intent)localObject1).putExtra("android.intent.extra.ringtone.TYPE", j);
    startActivityForResult((Intent)localObject1, 3);
  }
  
  private void c(boolean paramBoolean)
  {
    d_(false);
    Object localObject = ((com.truecaller.network.a.a)R.a()).a(paramBoolean);
    com.truecaller.androidactors.i locali = Q.a();
    -..Lambda.SettingsFragment.WX1t6l1NSWAnbLK6dsh80gwPCvU localWX1t6l1NSWAnbLK6dsh80gwPCvU = new com/truecaller/ui/-$$Lambda$SettingsFragment$WX1t6l1NSWAnbLK6dsh80gwPCvU;
    localWX1t6l1NSWAnbLK6dsh80gwPCvU.<init>(this);
    localObject = ((com.truecaller.androidactors.w)localObject).a(locali, localWX1t6l1NSWAnbLK6dsh80gwPCvU);
    S = ((com.truecaller.androidactors.a)localObject);
  }
  
  private Uri d(Uri paramUri)
  {
    ContentResolver localContentResolver = getContext().getContentResolver();
    paramUri = paramUri.getPath();
    Object localObject1 = new java/io/File;
    ((File)localObject1).<init>(paramUri);
    boolean bool = ((File)localObject1).exists();
    Cursor localCursor = null;
    if (!bool) {
      return null;
    }
    Uri localUri = MediaStore.Audio.Media.getContentUriForPath(paramUri);
    localObject1 = "_id";
    Object localObject2 = "is_notification";
    Object localObject3 = "is_ringtone";
    try
    {
      String[] tmp62_59 = new String[3];
      String[] tmp63_62 = tmp62_59;
      String[] tmp63_62 = tmp62_59;
      tmp63_62[0] = localObject1;
      tmp63_62[1] = localObject2;
      tmp63_62[2] = localObject3;
      localObject3 = tmp63_62;
      String str = "_data=? ";
      int k = 1;
      String[] arrayOfString = new String[k];
      arrayOfString[0] = paramUri;
      localObject1 = localContentResolver;
      localObject2 = localUri;
      localCursor = localContentResolver.query(localUri, (String[])localObject3, str, arrayOfString, null);
      if (localCursor != null)
      {
        bool = localCursor.moveToFirst();
        if (bool)
        {
          localObject1 = "is_notification";
          int j = localCursor.getColumnIndexOrThrow((String)localObject1);
          localObject2 = "is_ringtone";
          int i1 = localCursor.getColumnIndexOrThrow((String)localObject2);
          j = localCursor.getInt(j);
          if (j > 0)
          {
            j = localCursor.getInt(i1);
            if (j > 0)
            {
              j = 1;
              break label210;
            }
          }
          j = 0;
          localObject1 = null;
          label210:
          if (j == 0)
          {
            localObject1 = M();
            localObject2 = "_data=?";
            localObject3 = new String[k];
            localObject3[0] = paramUri;
            localContentResolver.update(localUri, (ContentValues)localObject1, (String)localObject2, (String[])localObject3);
          }
          paramUri = "_id";
          int i2 = localCursor.getColumnIndex(paramUri);
          long l1 = localCursor.getLong(i2);
          paramUri = ContentUris.withAppendedId(localUri, l1);
          return paramUri;
        }
      }
      if (localCursor != null) {
        localCursor.close();
      }
      localObject1 = M();
      ((ContentValues)localObject1).put("_data", paramUri);
      return localContentResolver.insert(localUri, (ContentValues)localObject1);
    }
    finally
    {
      if (localCursor != null) {
        localCursor.close();
      }
    }
  }
  
  private void d(int paramInt)
  {
    Object localObject1 = TrueApp.y().a().B();
    Object localObject2 = getActivity();
    paramInt = ((com.truecaller.notifications.g)localObject1).a((Context)localObject2, paramInt);
    if (paramInt != 0)
    {
      PermissionPoller localPermissionPoller = E;
      if (localPermissionPoller != null) {
        localPermissionPoller.a();
      }
      localPermissionPoller = new com/truecaller/wizard/utils/PermissionPoller;
      localObject1 = getContext();
      localObject2 = D;
      Object localObject3 = getContext();
      SettingsFragment.SettingsViewType localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_GENERAL;
      localObject3 = a((Context)localObject3, localSettingsViewType);
      localPermissionPoller.<init>((Context)localObject1, (Handler)localObject2, (Intent)localObject3);
      E = localPermissionPoller;
      localPermissionPoller = E;
      localObject1 = PermissionPoller.Permission.NOTIFICATION_ACCESS;
      localPermissionPoller.a((PermissionPoller.Permission)localObject1);
    }
  }
  
  private void e(int paramInt)
  {
    View localView = o();
    SwitchCompat localSwitchCompat = at.g(localView, paramInt);
    if (localSwitchCompat != null) {
      localSwitchCompat.toggle();
    }
  }
  
  private boolean f(int paramInt)
  {
    Object localObject = l;
    String str = "blockCallMethod";
    int j = ((com.truecaller.i.c)localObject).a(str, 0);
    int i1 = 4;
    boolean bool2 = true;
    int k;
    if (paramInt != i1)
    {
      i1 = 8;
      if (paramInt != i1) {
        return false;
      }
      k = Settings.b(j);
      if (k != 0) {
        return bool2;
      }
      localObject = y;
      k = ((l)localObject).e();
      if (k == 0)
      {
        L = bool2;
        n();
        return false;
      }
      localObject = "RingSilent";
    }
    else
    {
      boolean bool1 = Settings.a(k);
      if (bool1) {
        return bool2;
      }
      localObject = "RejectAutomatically";
    }
    b("BLOCKSETTINGS_BlockMethod", "BlocktabSettings_Action", (String)localObject);
    l.b("blockCallMethod", paramInt);
    return bool2;
  }
  
  private void g()
  {
    SettingsFragment.SettingsViewType localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_MAIN;
    a(2131886648, localSettingsViewType, 0);
  }
  
  private void h()
  {
    SettingsFragment.SettingsViewType localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_LANGUAGE;
    a(2131887072, localSettingsViewType, 0);
  }
  
  private void i()
  {
    Object localObject = getView();
    if (localObject == null) {
      return;
    }
    localObject = at.c(getView(), 2131364413);
    com.truecaller.utils.d locald = n;
    boolean bool = locald.d();
    int k;
    if (bool) {
      k = 2131887124;
    } else {
      k = 2131887083;
    }
    at.a((TextView)localObject, k);
    int j;
    if (bool) {
      j = 2131233984;
    } else {
      j = 2131234439;
    }
    at.c((TextView)localObject, j);
  }
  
  private void m()
  {
    com.truecaller.utils.d locald1 = n;
    int j = locald1.h();
    boolean bool1 = true;
    int k = 24;
    if (j >= k)
    {
      j = 1;
    }
    else
    {
      j = 0;
      locald1 = null;
    }
    com.truecaller.utils.d locald2 = n;
    boolean bool2 = locald2.c();
    int i1 = 2131364382;
    int i2 = 2131362359;
    if ((j != 0) && (!bool2))
    {
      at.a(o(), i2, bool1);
      at.a(o(), i1, bool1);
      return;
    }
    at.a(o(), i2, false);
    at.a(o(), i1, false);
  }
  
  private void n()
  {
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject = getActivity();
    localBuilder.<init>((Context)localObject);
    localBuilder = localBuilder.setTitle(2131886808).setMessage(2131886804).setNegativeButton(2131886797, null);
    localObject = new com/truecaller/ui/-$$Lambda$SettingsFragment$adlpz45jaxJJWbhqQvSwpJW_-J8;
    ((-..Lambda.SettingsFragment.adlpz45jaxJJWbhqQvSwpJW_-J8)localObject).<init>(this);
    localBuilder.setPositiveButton(2131886809, (DialogInterface.OnClickListener)localObject).show();
  }
  
  private void t()
  {
    boolean bool = n.f();
    View localView = o();
    bool ^= true;
    at.a(localView, 2131364378, bool);
  }
  
  private void u()
  {
    v();
    Handler localHandler = D;
    -..Lambda.SettingsFragment.4goyPBHAvu1km3X2uSueitZVxp4 local4goyPBHAvu1km3X2uSueitZVxp4 = new com/truecaller/ui/-$$Lambda$SettingsFragment$4goyPBHAvu1km3X2uSueitZVxp4;
    local4goyPBHAvu1km3X2uSueitZVxp4.<init>(this);
    long l1 = TimeUnit.SECONDS.toMillis(5);
    localHandler.postDelayed(local4goyPBHAvu1km3X2uSueitZVxp4, l1);
  }
  
  private void v()
  {
    android.support.transition.p.a((ViewGroup)o().findViewById(2131364375));
    boolean bool = y.a();
    View localView = o();
    bool ^= true;
    at.a(localView, 2131364380, bool);
  }
  
  private void w()
  {
    boolean bool1 = K;
    int j = 2131364460;
    int k = 2131364540;
    if (!bool1)
    {
      at.a(o(), k, 2131887077);
      at.c(o(), j, false);
      return;
    }
    at.a(o(), k, 2131887078);
    View localView = o();
    boolean bool2 = Settings.e("enhancedNotificationsEnabled");
    at.c(localView, j, bool2);
  }
  
  private void x()
  {
    boolean bool1 = K;
    int j = 2131364473;
    int k = 2131364472;
    if (!bool1)
    {
      at.a(o(), k, 2131887135);
      at.c(o(), j, false);
      return;
    }
    at.a(o(), k, 2131887136);
    View localView = o();
    boolean bool2 = Settings.e("whatsAppCallsEnabled");
    at.c(localView, j, bool2);
  }
  
  private void y()
  {
    Object localObject1 = getActivity();
    boolean bool1 = com.truecaller.common.h.k.f();
    int j = 1;
    int i1 = 0;
    if (bool1)
    {
      bool1 = Settings.System.canWrite((Context)localObject1);
      if (!bool1)
      {
        bool1 = false;
        break label37;
      }
    }
    bool1 = true;
    label37:
    int i2 = 2131364438;
    Object localObject2;
    try
    {
      localObject1 = RingtoneManager.getActualDefaultRingtoneUri((Context)localObject1, j);
      a(i2, (Uri)localObject1);
    }
    catch (SecurityException localSecurityException1)
    {
      localObject2 = o();
      int i3 = 2131364439;
      at.b((View)localObject2, i3, false);
      com.truecaller.log.d.a(localSecurityException1);
    }
    int i4 = 2131364416;
    Object localObject3;
    try
    {
      localObject2 = J;
      localObject2 = ((com.truecaller.notificationchannels.j)localObject2).h();
      a(i4, (Uri)localObject2);
    }
    catch (SecurityException localSecurityException3)
    {
      localObject3 = o();
      at.b((View)localObject3, i4, false);
      com.truecaller.log.d.a(localSecurityException3);
    }
    boolean bool2 = H;
    int i6 = 2131364398;
    View localView1;
    if (!bool2)
    {
      localView1 = o().findViewById(i6);
      at.a(localView1, false);
    }
    else
    {
      localView1 = o().findViewById(i6);
      at.a(localView1, j);
      int i5 = 2131364397;
      try
      {
        Object localObject4 = t;
        localObject4 = ((com.truecaller.flashsdk.core.b)localObject4).d();
        localObject3 = t;
        str = ((Uri)localObject4).toString();
        ((com.truecaller.flashsdk.core.b)localObject3).d(str);
        a(i5, (Uri)localObject4);
      }
      catch (SecurityException localSecurityException4)
      {
        localObject3 = t;
        String str = null;
        ((com.truecaller.flashsdk.core.b)localObject3).d(null);
        try
        {
          localObject3 = o;
          localObject3 = ((bv)localObject3).e();
          a(i5, (Uri)localObject3);
        }
        catch (SecurityException localSecurityException2)
        {
          AssertionUtil.reportThrowableButNeverCrash(localSecurityException2);
        }
        localView2 = o();
        at.b(localView2, i6, false);
        com.truecaller.log.d.a(localSecurityException4);
      }
    }
    t.a(o().findViewById(i2), bool1, 0.5F);
    View localView2 = o();
    int k = 2131364440;
    localView2 = localView2.findViewById(k);
    if (bool1) {
      i1 = 8;
    }
    localView2.setVisibility(i1);
  }
  
  private void z()
  {
    View localView = o();
    if (localView != null)
    {
      Object localObject1 = i.A();
      boolean bool1 = false;
      Object localObject2 = null;
      if (localObject1 == null)
      {
        localObject1 = "";
      }
      else
      {
        boolean bool2 = org.c.a.a.a.k.b((CharSequence)localObject1);
        if (!bool2)
        {
          int k;
          if (localObject1 != null)
          {
            j = ((String)localObject1).length();
            if (j != 0)
            {
              k = 0;
              localObject3 = null;
              while (k != j)
              {
                boolean bool4 = Character.isWhitespace(((String)localObject1).charAt(k));
                if (!bool4) {
                  break;
                }
                k += 1;
              }
              localObject1 = ((String)localObject1).substring(k);
            }
          }
          if (localObject1 != null)
          {
            j = ((String)localObject1).length();
            if (j != 0)
            {
              while (j != 0)
              {
                k = j + -1;
                bool3 = Character.isWhitespace(((String)localObject1).charAt(k));
                if (!bool3) {
                  break;
                }
                j += -1;
              }
              localObject1 = ((String)localObject1).substring(0, j);
            }
          }
        }
      }
      localObject1 = ((String)localObject1).replace('\n', ' ');
      Object localObject3 = getString(2131887086);
      localObject1 = am.d((CharSequence)localObject1, (CharSequence)localObject3);
      at.a(localView, 2131364409, (CharSequence)localObject1);
      localObject1 = (SwitchCompat)localView.findViewById(2131364511);
      ((SwitchCompat)localObject1).setOnCheckedChangeListener(null);
      boolean bool3 = i.b(0);
      ((SwitchCompat)localObject1).setChecked(bool3);
      localObject3 = new com/truecaller/ui/-$$Lambda$SettingsFragment$T4_mEiPPddCthsfnSbQpL_RKgL4;
      ((-..Lambda.SettingsFragment.T4_mEiPPddCthsfnSbQpL_RKgL4)localObject3).<init>(this);
      ((SwitchCompat)localObject1).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject3);
      bool3 = i.c(0);
      at.c(localView, 2131364508, bool3);
      localObject3 = i;
      bool1 = ((com.truecaller.messaging.h)localObject3).d(0);
      at.c(localView, 2131364509, bool1);
      localObject1 = (SwitchCompat)localView.findViewById(2131364518);
      ((SwitchCompat)localObject1).setOnCheckedChangeListener(null);
      localObject2 = i;
      int j = 1;
      bool1 = ((com.truecaller.messaging.h)localObject2).b(j);
      ((SwitchCompat)localObject1).setChecked(bool1);
      localObject2 = new com/truecaller/ui/-$$Lambda$SettingsFragment$skZwFqyTKkGYP_oI--wGc9nrvrM;
      ((-..Lambda.SettingsFragment.skZwFqyTKkGYP_oI--wGc9nrvrM)localObject2).<init>(this);
      ((SwitchCompat)localObject1).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject2);
      bool1 = i.c(j);
      at.c(localView, 2131364515, bool1);
      int i1 = 2131364516;
      localObject2 = i;
      bool1 = ((com.truecaller.messaging.h)localObject2).d(j);
      at.c(localView, i1, bool1);
    }
  }
  
  public final boolean W_()
  {
    LinearLayout localLinearLayout = d;
    int j = localLinearLayout.getVisibility();
    if (j != 0)
    {
      boolean bool = M;
      if (bool)
      {
        g();
        return true;
      }
    }
    return false;
  }
  
  protected final void a()
  {
    d = null;
  }
  
  public final void b(boolean paramBoolean)
  {
    if (!paramBoolean)
    {
      Object localObject = getContext();
      int j = 2131887115;
      int k = 1;
      localObject = Toast.makeText((Context)localObject, j, k);
      ((Toast)localObject).show();
    }
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Object localObject1 = C;
    int j = ((List)localObject1).size();
    j = paramInt1 - j;
    int k = 0;
    String str1 = null;
    int i1 = -1;
    int i2 = 1;
    Object localObject2;
    Object localObject3;
    if (j == i2)
    {
      if (paramInt2 == i1)
      {
        localObject2 = (Uri)paramIntent.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
        localObject3 = RingtoneManager.getActualDefaultRingtoneUri(getActivity(), i2);
        int i3 = 2131364438;
        if (localObject2 != null)
        {
          RingtoneManager.setActualDefaultRingtoneUri(getActivity(), i2, (Uri)localObject2);
          a(i3, (Uri)localObject2);
          return;
        }
        if (localObject3 != null)
        {
          localObject2 = getActivity();
          RingtoneManager.setActualDefaultRingtoneUri((Context)localObject2, i2, null);
          a(i3, null);
        }
      }
      return;
    }
    i2 = 2;
    if (i2 == j)
    {
      if (i1 == paramInt2)
      {
        localObject2 = (Uri)paramIntent.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
        if (localObject2 != null)
        {
          localObject3 = ((Uri)localObject2).getScheme();
          paramIntent = "file";
          paramInt2 = ((String)localObject3).equals(paramIntent);
          if (paramInt2 != 0) {
            localObject2 = d((Uri)localObject2);
          }
        }
        localObject3 = i;
        if (localObject2 != null) {
          str1 = ((Uri)localObject2).toString();
        }
        ((com.truecaller.messaging.h)localObject3).a(str1);
        paramInt1 = Build.VERSION.SDK_INT;
        paramInt2 = 26;
        if (paramInt1 >= paramInt2)
        {
          localObject2 = J;
          ((com.truecaller.notificationchannels.j)localObject2).j();
        }
        y();
      }
      return;
    }
    k = 3;
    if (j == k)
    {
      if (paramInt2 == i1)
      {
        localObject1 = (Uri)paramIntent.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
        k = 2131364397;
        com.truecaller.flashsdk.core.b localb;
        String str2;
        if (localObject1 != null)
        {
          localb = t;
          str2 = ((Uri)localObject1).toString();
          localb.d(str2);
          a(k, (Uri)localObject1);
        }
        else
        {
          localObject1 = o.e();
          localb = t;
          str2 = ((Uri)localObject1).toString();
          localb.d(str2);
          a(k, (Uri)localObject1);
        }
      }
    }
    else
    {
      k = 8;
      if ((j == k) && (paramInt2 == i1)) {
        i();
      }
    }
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
  }
  
  public void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = (com.truecaller.common.b.a)paramContext.getApplicationContext();
    boolean bool1 = paramContext.o();
    G = bool1;
    bool1 = paramContext.p();
    H = bool1;
    Object localObject1 = paramContext;
    localObject1 = ((bk)paramContext).a();
    Object localObject2 = ((bp)localObject1).C();
    i = ((com.truecaller.messaging.h)localObject2);
    localObject2 = ((bp)localObject1).bx();
    n = ((com.truecaller.utils.d)localObject2);
    localObject2 = ((bp)localObject1).by();
    o = ((bv)localObject2);
    localObject2 = ((bp)localObject1).D();
    l = ((com.truecaller.i.c)localObject2);
    localObject2 = ((bp)localObject1).I();
    m = ((com.truecaller.common.g.a)localObject2);
    localObject2 = ((bp)localObject1).d();
    p = ((com.truecaller.analytics.a.a)localObject2);
    localObject2 = ((bp)localObject1).R();
    q = ((com.truecaller.filters.p)localObject2);
    localObject2 = ((bp)localObject1).f();
    r = ((com.truecaller.androidactors.f)localObject2);
    localObject2 = ((bp)localObject1).aF();
    s = ((com.truecaller.featuretoggles.e)localObject2);
    localObject2 = getChildFragmentManager();
    F = ((android.support.v4.app.j)localObject2);
    localObject2 = ((bp)localObject1).aV();
    t = ((com.truecaller.flashsdk.core.b)localObject2);
    localObject2 = ((bp)localObject1).T();
    v = ((r)localObject2);
    localObject2 = ((bp)localObject1).aG();
    B = ((bw)localObject2);
    localObject2 = ((bp)localObject1).bf();
    P = ((com.truecaller.common.profile.e)localObject2);
    localObject2 = ((bp)localObject1).bi();
    R = ((com.truecaller.androidactors.f)localObject2);
    localObject2 = ((bp)localObject1).m();
    Q = ((com.truecaller.androidactors.k)localObject2);
    localObject2 = ((bp)localObject1).S();
    T = ((com.truecaller.androidactors.f)localObject2);
    boolean bool2 = paramContext.u().n().a();
    I = bool2;
    localObject2 = ((bp)localObject1).ai();
    w = ((com.truecaller.common.f.c)localObject2);
    localObject2 = ((bp)localObject1).bk();
    x = ((com.truecaller.whoviewedme.w)localObject2);
    localObject2 = ((bp)localObject1).bh();
    V = ((com.truecaller.calling.recorder.h)localObject2);
    localObject2 = ((bp)localObject1).c();
    u = ((com.truecaller.analytics.b)localObject2);
    localObject2 = ((bp)localObject1).bw();
    y = ((l)localObject2);
    localObject2 = ((bp)localObject1).aE();
    J = ((com.truecaller.notificationchannels.j)localObject2);
    localObject2 = ((bp)localObject1).bF();
    z = ((br)localObject2);
    localObject2 = ((bp)localObject1).n();
    A = ((com.truecaller.calling.e.e)localObject2);
    localObject1 = ((bp)localObject1).aI();
    W = ((com.truecaller.common.h.c)localObject1);
    paramContext = paramContext.u().w();
    X = paramContext;
  }
  
  public void onClick(View paramView)
  {
    int j = paramView.getId();
    int i9 = 2131364468;
    int k;
    if (j == i9)
    {
      k = I;
      if (k != 0)
      {
        paramView = "https://www.truecaller.com/terms-of-service#eu";
      }
      else
      {
        k = b();
        if (k != 0) {
          paramView = "https://www.truecaller.com/pt-BR/terms-of-service";
        } else {
          paramView = "https://www.truecaller.com/terms-of-service#row";
        }
      }
      d(paramView);
      return;
    }
    i9 = 2131364429;
    if (k == i9)
    {
      k = I;
      if (k != 0)
      {
        paramView = "https://privacy.truecaller.com/privacy-policy-eu";
      }
      else
      {
        k = b();
        if (k != 0) {
          paramView = "https://www.truecaller.com/pt-BR/privacy-policy";
        } else {
          paramView = "https://privacy.truecaller.com/privacy-policy";
        }
      }
      d(paramView);
      return;
    }
    int i10 = 2131364318;
    if (k == i10)
    {
      d("file:///android_asset/third-party-acknowledgement.html");
      return;
    }
    int i11 = 2131364364;
    if (k == i11)
    {
      d("https://blog.truecaller.com");
      return;
    }
    int i12 = 2131364431;
    if (k == i12)
    {
      o.b(getActivity());
      boolean bool1 = true;
      Settings.a("GOOGLE_REVIEW_DONE", bool1);
      Settings.a("FEEDBACK_LIKES_TRUECALLER", bool1);
      return;
    }
    int i13 = 2131364442;
    Object localObject2;
    if (k == i13)
    {
      localObject1 = new com/truecaller/analytics/e$a;
      ((e.a)localObject1).<init>("ViewAction");
      ((e.a)localObject1).a("Context", "settings_screen").a("Action", "feedback");
      paramView = u;
      localObject2 = ((e.a)localObject1).a();
      paramView.a((com.truecaller.analytics.e)localObject2);
      paramView = getActivity();
      localObject2 = SingleActivity.FragmentSingle.FEEDBACK_FORM;
      paramView = SingleActivity.a(paramView, (SingleActivity.FragmentSingle)localObject2);
      getActivity().startActivity(paramView);
      return;
    }
    int i14 = 2131364432;
    int i52 = 0;
    Object localObject1 = null;
    if (k == i14)
    {
      paramView = m;
      localObject2 = "profileBusiness";
      k = paramView.a((String)localObject2, false);
      if (k != 0)
      {
        paramView = s.ar();
        k = paramView.a();
        if (k == 0)
        {
          paramView = CreateBusinessProfileActivity.a(getActivity());
          startActivity(paramView);
        }
      }
      else
      {
        paramView = getContext();
        localObject2 = SingleActivity.FragmentSingle.EDIT_ME;
        paramView = SingleActivity.c(paramView, (SingleActivity.FragmentSingle)localObject2);
        startActivity(paramView);
      }
      return;
    }
    int i15 = 2131364386;
    int i54 = 2131887106;
    int i1;
    if (k == i15)
    {
      i1 = 2131887107;
      boolean bool2 = I;
      if (bool2) {
        i54 = 2131887105;
      }
      bool2 = I;
      a(i1, i54, bool2);
      return;
    }
    int i16 = 2131364436;
    if (i1 == i16)
    {
      i1 = 2131887108;
      boolean bool3 = I;
      if (bool3) {
        i54 = 2131887103;
      }
      a(i1, i54, false);
      return;
    }
    int i17 = 2131364407;
    if (i1 == i17)
    {
      B();
      return;
    }
    i17 = 2131364427;
    if (i1 == i17)
    {
      paramView = new android/content/Intent;
      localObject2 = "android.intent.action.MAIN";
      paramView.<init>((String)localObject2);
      i17 = Build.VERSION.SDK_INT;
      i52 = 26;
      if (i17 >= i52)
      {
        localObject2 = ComponentName.unflattenFromString("com.android.dialer/.app.settings.DialerSettingsActivity");
        paramView.setComponent((ComponentName)localObject2);
      }
      else
      {
        i17 = Build.VERSION.SDK_INT;
        int i53 = 21;
        if (i17 >= i53)
        {
          localObject2 = ComponentName.unflattenFromString("com.android.dialer/.settings.DialerSettingsActivity");
          paramView.setComponent((ComponentName)localObject2);
        }
        else
        {
          localObject2 = ComponentName.unflattenFromString("com.android.phone/.CallFeaturesSetting");
          paramView.setComponent((ComponentName)localObject2);
        }
      }
      try
      {
        startActivity(paramView);
        return;
      }
      catch (SecurityException localSecurityException)
      {
        AssertionUtil.reportThrowableButNeverCrash(localSecurityException);
        return;
      }
    }
    i17 = 2131364404;
    if (i1 == i17)
    {
      h();
      return;
    }
    i17 = 2131364383;
    if (i1 == i17)
    {
      C();
      return;
    }
    i17 = 2131364441;
    if (i1 == i17)
    {
      F();
      return;
    }
    i17 = 2131364422;
    if (i1 == i17)
    {
      G();
      return;
    }
    i17 = 2131364467;
    if (i1 == i17)
    {
      paramView = new android/content/Intent;
      localObject2 = getActivity();
      paramView.<init>((Context)localObject2, SettingsActivity.class);
      startActivity(paramView);
      return;
    }
    i17 = 2131364361;
    if (i1 == i17)
    {
      H();
      return;
    }
    i17 = 2131364327;
    if (i1 == i17)
    {
      E();
      return;
    }
    i17 = 2131364338;
    if (i1 == i17)
    {
      I();
      return;
    }
    i17 = 2131364430;
    if (i1 == i17)
    {
      paramView = SettingsFragment.SettingsViewType.SETTINGS_PRIVACY;
      a(2131887113, paramView, 0);
      return;
    }
    i17 = 2131364366;
    if (i1 == i17)
    {
      paramView = getActivity();
      if (paramView != null)
      {
        paramView = getActivity();
        localObject2 = new android/content/Intent;
        localObject1 = getActivity();
        ((Intent)localObject2).<init>((Context)localObject1, CallRecordingSettingsActivity.class);
        paramView.startActivity((Intent)localObject2);
      }
    }
    else
    {
      i17 = 2131364319;
      if (i1 == i17)
      {
        D();
        return;
      }
      i17 = 2131364461;
      if (i1 == i17)
      {
        com.truecaller.calling.d.m.a(getActivity());
        return;
      }
      i17 = 2131364325;
      if (i1 == i17)
      {
        paramView = getActivity();
        localObject2 = getActivity();
        localObject1 = SingleActivity.FragmentSingle.THEME_SELECTOR;
        localObject2 = SingleActivity.a((Context)localObject2, (SingleActivity.FragmentSingle)localObject1);
        paramView.startActivity((Intent)localObject2);
        return;
      }
      i17 = 2131364459;
      int i2;
      if (i1 == i17)
      {
        i2 = K;
        if (i2 == 0)
        {
          d(2131886528);
          return;
        }
        e(2131364460);
        return;
      }
      i17 = 2131364471;
      if (i2 == i17)
      {
        i2 = K;
        if (i2 == 0)
        {
          d(2131887137);
          return;
        }
        e(2131364473);
        return;
      }
      int i18 = 2131364330;
      if (i2 == i18)
      {
        e(2131364331);
        return;
      }
      int i19 = 2131364336;
      if (i2 == i19)
      {
        e(2131364337);
        return;
      }
      int i20 = 2131364395;
      int i4;
      if (i2 == i20)
      {
        int i3 = 2131364399;
        e(i3);
        localObject2 = o();
        paramView = at.g((View)localObject2, i3);
        i4 = paramView.isChecked();
        if (i4 != 0) {
          paramView = "Enabled";
        } else {
          paramView = "Disabled";
        }
        a("settings_screen", "flashEnabled", paramView);
        return;
      }
      int i21 = 2131364462;
      if (i4 == i21)
      {
        e(2131364463);
        return;
      }
      int i22 = 2131364455;
      int i6;
      if (i4 == i22)
      {
        int i5 = 2131364456;
        e(i5);
        localObject2 = o();
        paramView = at.g((View)localObject2, i5);
        i6 = paramView.isChecked();
        if (i6 != 0) {
          paramView = "Enabled";
        } else {
          paramView = "Disabled";
        }
        a("settings_screen", "smartNotifications", paramView);
        return;
      }
      int i23 = 2131364391;
      if (i6 == i23)
      {
        e(2131364393);
        return;
      }
      int i24 = 2131364333;
      if (i6 == i24)
      {
        e(2131364335);
        return;
      }
      int i25 = 2131364376;
      if (i6 == i25)
      {
        e(2131364377);
        return;
      }
      int i26 = 2131364323;
      if (i6 == i26)
      {
        e(2131364324);
        return;
      }
      int i27 = 2131364345;
      if (i6 == i27)
      {
        e(2131364347);
        return;
      }
      int i28 = 2131364354;
      if (i6 == i28)
      {
        e(2131364356);
        return;
      }
      int i29 = 2131364342;
      if (i6 == i29)
      {
        e(2131364344);
        return;
      }
      int i30 = 2131364348;
      if (i6 == i30)
      {
        e(2131364350);
        return;
      }
      int i31 = 2131364358;
      if (i6 == i31)
      {
        e(2131364360);
        return;
      }
      int i32 = 2131364340;
      if (i6 == i32)
      {
        e(2131364341);
        return;
      }
      int i33 = 2131364351;
      if (i6 == i33)
      {
        e(2131364352);
        return;
      }
      int i34 = 2131364470;
      if (i6 == i34)
      {
        paramView = new android/content/Intent;
        localObject2 = getContext();
        paramView.<init>((Context)localObject2, BlockedListActivity.class);
        startActivity(paramView);
        return;
      }
      int i35 = 2131364438;
      if (i6 == i35)
      {
        J();
        return;
      }
      int i36 = 2131364397;
      if (i6 == i36)
      {
        L();
        return;
      }
      int i37 = 2131364416;
      if (i6 == i37)
      {
        K();
        return;
      }
      int i38 = 2131364418;
      if (i6 == i38)
      {
        e(2131364419);
        return;
      }
      int i39 = 2131364380;
      Object localObject3;
      SettingsFragment.SettingsViewType localSettingsViewType;
      if (i6 == i39)
      {
        paramView = y;
        i6 = paramView.a();
        if (i6 != 0) {
          return;
        }
        a("DrawOnTop");
        paramView = new com/truecaller/wizard/utils/PermissionPoller;
        localObject2 = getContext();
        localObject1 = D;
        localObject3 = getContext();
        localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_CALLERID;
        localObject3 = a((Context)localObject3, localSettingsViewType);
        paramView.<init>((Context)localObject2, (Handler)localObject1, (Intent)localObject3);
        E = paramView;
        paramView = E;
        localObject2 = PermissionPoller.Permission.DRAW_OVERLAY;
        paramView.a((PermissionPoller.Permission)localObject2);
        com.truecaller.wizard.utils.i.a(getActivity());
        return;
      }
      int i40 = 2131364378;
      if (i6 == i40)
      {
        paramView = n;
        i6 = paramView.f();
        if (i6 != 0) {
          return;
        }
        a("BatteryOptimization");
        paramView = new com/truecaller/wizard/utils/PermissionPoller;
        localObject2 = getContext();
        localObject1 = D;
        localObject3 = getContext();
        localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_CALLERID;
        localObject3 = a((Context)localObject3, localSettingsViewType);
        paramView.<init>((Context)localObject2, (Handler)localObject1, (Intent)localObject3);
        E = paramView;
        paramView = E;
        localObject2 = PermissionPoller.Permission.BATTERY_OPTIMISATIONS;
        paramView.a((PermissionPoller.Permission)localObject2);
        com.truecaller.wizard.utils.i.b(getActivity());
        return;
      }
      int i41 = 2131364362;
      if (i6 == i41)
      {
        paramView = n;
        i6 = paramView.c();
        if (i6 != 0) {
          return;
        }
        a("DialerApp");
        paramView = getActivity();
        localObject2 = o.a(getActivity());
        o.a(paramView, (Intent)localObject2);
        return;
      }
      int i42 = 2131364425;
      if (i6 == i42)
      {
        paramView = new com/truecaller/analytics/e$a;
        paramView.<init>("ViewAction");
        paramView.a("Context", "settings_screen").a("Action", "help").a("SubAction", "callerId");
        localObject2 = u;
        paramView = paramView.a();
        ((com.truecaller.analytics.b)localObject2).a(paramView);
        d("https://support.truecaller.com/hc/en-us/articles/212028169-Live-Caller-ID-is-not-working-All-devices-");
        return;
      }
      int i43 = 2131364448;
      if (i6 == i43)
      {
        paramView = at.g(o(), 2131364449);
        paramView.toggle();
        localObject2 = l;
        i6 = paramView.isChecked();
        ((com.truecaller.i.c)localObject2).b("showIncomingCallNotifications", i6);
        return;
      }
      int i44 = 2131364450;
      if (i6 == i44)
      {
        i6 = K;
        if (i6 == 0)
        {
          d(2131888934);
          return;
        }
        paramView = at.g(o(), 2131364451);
        paramView.toggle();
        i6 = paramView.isChecked();
        Settings.a("showMissedCallsNotifications", i6);
        return;
      }
      int i45 = 2131364476;
      if (i6 == i45)
      {
        paramView = at.g(o(), 2131364478);
        paramView.toggle();
        i6 = paramView.isChecked();
        Settings.a("showProfileViewNotifications", i6);
        return;
      }
      int i46 = 2131364503;
      if (i6 == i46)
      {
        com.truecaller.wizard.b.c.a(getContext(), WizardActivity.class, "settings_screen");
        return;
      }
      int i47 = 2131364440;
      int i8;
      if (i6 == i47)
      {
        int i7 = Build.VERSION.SDK_INT;
        i48 = 23;
        if (i7 >= i48)
        {
          paramView = getContext();
          i8 = Settings.System.canWrite(paramView);
          if (i8 == 0)
          {
            paramView = new android/content/Intent;
            paramView.<init>("android.settings.action.MANAGE_WRITE_SETTINGS");
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>("package:");
            localObject1 = getContext().getPackageName();
            ((StringBuilder)localObject2).append((String)localObject1);
            localObject2 = Uri.parse(((StringBuilder)localObject2).toString());
            paramView.setData((Uri)localObject2);
            startActivity(paramView);
          }
        }
        return;
      }
      int i48 = 2131364452;
      if (i8 == i48)
      {
        paramView = "showMissedCallReminders";
        i8 = Settings.e(paramView);
        if (i8 != 0)
        {
          paramView = TrueApp.y().a().Z();
          paramView.a();
        }
        e(2131364454);
        return;
      }
      int i49 = 2131364457;
      if (i8 == i49)
      {
        e(2131364458);
        return;
      }
      int i50 = 2131364475;
      if (i8 == i50)
      {
        paramView = w;
        i8 = paramView.d();
        if (i8 == 0)
        {
          paramView = requireContext();
          localObject2 = PremiumPresenterView.LaunchContext.WHO_VIEWED_ME_INCOGNITO;
          br.a(paramView, (PremiumPresenterView.LaunchContext)localObject2, "premiumIncognitoMode");
          return;
        }
        e(2131364479);
        return;
      }
      int i51 = 2131364382;
      if (i8 == i51)
      {
        paramView = n;
        i8 = paramView.c();
        if (i8 != 0) {
          return;
        }
        a("DialerApp");
        paramView = getActivity();
        localObject2 = o.a(getActivity());
        o.a(paramView, (Intent)localObject2);
      }
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = u;
    Object localObject = new com/truecaller/analytics/bc;
    ((bc)localObject).<init>("settings_screen");
    paramBundle.a((com.truecaller.analytics.e)localObject);
    paramBundle = android.support.v4.content.d.a(getContext());
    localObject = Y;
    IntentFilter localIntentFilter = new android/content/IntentFilter;
    localIntentFilter.<init>("com.truecaller.ACTION_FINISH");
    paramBundle.a((BroadcastReceiver)localObject, localIntentFilter);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(2131559101, paramViewGroup, false);
  }
  
  public void onDestroy()
  {
    Object localObject = D;
    BroadcastReceiver localBroadcastReceiver = null;
    ((Handler)localObject).removeCallbacksAndMessages(null);
    localObject = E;
    if (localObject != null) {
      ((PermissionPoller)localObject).a();
    }
    localObject = android.support.v4.content.d.a(getContext());
    localBroadcastReceiver = Y;
    ((android.support.v4.content.d)localObject).a(localBroadcastReceiver);
    super.onDestroy();
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    com.truecaller.androidactors.a locala = S;
    if (locala != null)
    {
      locala.a();
      S = null;
    }
    locala = U;
    if (locala != null)
    {
      locala.a();
      U = null;
    }
  }
  
  public void onPause()
  {
    super.onPause();
    String str1 = N;
    if (str1 != null)
    {
      String str2 = "t9_lang";
      Settings.b(str2, str1);
      com.truecaller.search.local.b.d.b(N);
      com.truecaller.search.local.b.c.a();
      RefreshT9MappingService.a(getContext());
      str1 = null;
      N = null;
    }
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    int j = paramArrayOfInt.length;
    int k = 0;
    List localList = null;
    if (j == 0) {
      j = -1;
    } else {
      j = paramArrayOfInt[0];
    }
    Object localObject = C;
    int i1 = ((List)localObject).size();
    if (paramInt > i1)
    {
      localList = C;
      k = localList.size();
      k = paramInt - k;
      switch (k)
      {
      default: 
        break;
      case 7: 
        if (j != 0) {
          break;
        }
        L();
        break;
      case 6: 
        if (j != 0) {
          break;
        }
        K();
        break;
      case 5: 
        if (j != 0) {
          break;
        }
        J();
        break;
      }
    }
    else
    {
      localObject = C;
      i1 = ((List)localObject).size();
      if (paramInt < i1)
      {
        i1 = ((Integer)C.get(paramInt)).intValue();
        View localView = o();
        localObject = at.g(localView, i1);
        if (j == 0) {
          k = 1;
        }
        ((SwitchCompat)localObject).setChecked(k);
      }
    }
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
  }
  
  public void onResume()
  {
    super.onResume();
    Object localObject1 = E;
    if (localObject1 != null) {
      ((PermissionPoller)localObject1).a();
    }
    localObject1 = U;
    if (localObject1 != null) {
      ((com.truecaller.androidactors.a)localObject1).a();
    }
    localObject1 = ((s)T.a()).b();
    com.truecaller.androidactors.i locali = Q.a();
    Object localObject2 = new com/truecaller/ui/-$$Lambda$SettingsFragment$6HEEligzKYPb_dwuNOxJcpR1s7k;
    ((-..Lambda.SettingsFragment.6HEEligzKYPb_dwuNOxJcpR1s7k)localObject2).<init>(this);
    localObject1 = ((com.truecaller.androidactors.w)localObject1).a(locali, (com.truecaller.androidactors.ac)localObject2);
    U = ((com.truecaller.androidactors.a)localObject1);
    boolean bool1 = K;
    c();
    boolean bool2 = true;
    if (!bool1)
    {
      bool1 = K;
      if (bool1)
      {
        Settings.a("enhancedNotificationsEnabled", bool2);
        localObject1 = "showMissedCallsNotifications";
        Settings.a((String)localObject1, bool2);
      }
    }
    w();
    x();
    y();
    localObject1 = o();
    int i1 = 2131364448;
    boolean bool3 = H;
    if (bool3)
    {
      localb = s.V();
      bool3 = localb.a();
      if (bool3)
      {
        bool3 = true;
        break label196;
      }
    }
    bool3 = false;
    com.truecaller.featuretoggles.b localb = null;
    label196:
    at.a((View)localObject1, i1, bool3);
    bool1 = l.a("showIncomingCallNotifications", bool2);
    at.c(o(), 2131364449, bool1);
    localObject1 = "showMissedCallsNotifications";
    bool1 = Settings.e((String)localObject1);
    localObject2 = o();
    int i2 = 2131364451;
    if (bool1)
    {
      bool1 = K;
      if (bool1)
      {
        bool1 = true;
        break label280;
      }
    }
    bool1 = false;
    localObject1 = null;
    label280:
    at.c((View)localObject2, i2, bool1);
    localObject1 = "showProfileViewNotifications";
    bool1 = Settings.a((String)localObject1);
    if (bool1)
    {
      localObject1 = "showProfileViewNotifications";
      bool1 = Settings.e((String)localObject1);
      if (!bool1)
      {
        bool2 = false;
        locali = null;
      }
    }
    localObject1 = o();
    i1 = 2131364478;
    at.c((View)localObject1, i1, bool2);
    bool1 = L;
    if (bool1)
    {
      localObject1 = y;
      bool1 = ((l)localObject1).e();
      if (bool1)
      {
        localObject1 = getActivity();
        int k = 2131886159;
        localObject1 = Toast.makeText((Context)localObject1, k, 0);
        ((Toast)localObject1).show();
        int j = 8;
        f(j);
      }
    }
    A();
    L = false;
    u();
    t();
    m();
    i();
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    String str = O;
    paramBundle.putString("subView", str);
    super.onSaveInstanceState(paramBundle);
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    SettingsFragment localSettingsFragment = this;
    Object localObject1 = paramView;
    Object localObject2 = paramBundle;
    super.onViewCreated(paramView, paramBundle);
    c();
    Object localObject3 = (ViewStub)paramView.findViewById(2131364405);
    f = ((ViewStub)localObject3);
    localObject3 = (ComboBase)paramView.findViewById(2131364469);
    b = ((ComboBase)localObject3);
    localObject3 = (ComboBase)paramView.findViewById(2131364433);
    c = ((ComboBase)localObject3);
    localObject3 = (ComboBase)paramView.findViewById(2131364388);
    g = ((ComboBase)localObject3);
    localObject3 = (TextView)paramView.findViewById(2131364357);
    h = ((TextView)localObject3);
    localObject3 = o();
    Object localObject4 = "alwaysDownloadImages";
    at.a((View)localObject3, (String)localObject4);
    boolean bool1 = G;
    int i1 = 2131364389;
    String str1 = null;
    if (!bool1)
    {
      localObject3 = o();
      at.a((View)localObject3, i1, false);
    }
    else
    {
      localObject3 = getResources();
      i8 = 2130903055;
      localObject3 = ((Resources)localObject3).getStringArray(i8);
      localObject5 = new java/util/ArrayList;
      int i12 = localObject3.length;
      ((ArrayList)localObject5).<init>(i12);
      i12 = 0;
      localObject6 = null;
      for (;;)
      {
        int i14 = localObject3.length;
        if (i12 >= i14) {
          break;
        }
        localObject7 = new com/truecaller/ui/components/n;
        localObject8 = localObject3[i12];
        localObject9 = "";
        localObject10 = String.valueOf(i12);
        ((n)localObject7).<init>((String)localObject8, (String)localObject9, localObject10);
        ((List)localObject5).add(localObject7);
        i12 += 1;
      }
      localObject3 = o();
      localObject6 = "dialpad_feedback_index_str";
      at.a((View)localObject3, i1, (List)localObject5, (String)localObject6);
    }
    bool1 = G;
    i1 = 2131364450;
    int i8 = 2131364424;
    if (!bool1)
    {
      localObject3 = o().findViewById(i8);
      boolean bool13 = H;
      if (!bool13)
      {
        at.a((View)localObject3, false);
      }
      else
      {
        at.a((View)localObject3, i1, false);
        i13 = 2131364453;
        at.a((View)localObject3, i13, false);
      }
    }
    localObject3 = o();
    Object localObject7 = m;
    boolean bool14 = ((com.truecaller.common.g.a)localObject7).b("backup");
    Object localObject8 = new com/truecaller/ui/-$$Lambda$SettingsFragment$Very7E_j-8EkzeE4GvXyb7ol1Bc;
    ((-..Lambda.SettingsFragment.Very7E_j-8EkzeE4GvXyb7ol1Bc)localObject8).<init>(localSettingsFragment);
    at.a((View)localObject3, 2131364393, bool14, (CompoundButton.OnCheckedChangeListener)localObject8);
    localObject3 = (TextView)o().findViewById(2131364394);
    Object localObject6 = LinkMovementMethod.getInstance();
    ((TextView)localObject3).setMovementMethod((MovementMethod)localObject6);
    localObject3 = W;
    bool1 = ((com.truecaller.common.h.c)localObject3).c();
    int i13 = 1;
    if (bool1)
    {
      bool1 = I;
      if (!bool1)
      {
        bool1 = true;
        break label493;
      }
    }
    bool1 = false;
    localObject3 = null;
    label493:
    localObject7 = o();
    int i17 = 2131364392;
    at.a((View)localObject7, i17, bool1);
    bool1 = H;
    int i15 = 2131364336;
    if (!bool1)
    {
      at.a(o(), i15, false);
      localObject3 = o();
      int i23 = 2131364396;
      at.a((View)localObject3, i23, false);
      at.a(o(), i17, false);
      at.a(o(), 2131364334, false);
      localObject3 = o();
      i17 = 2131364329;
      at.a((View)localObject3, i17, false);
    }
    bool1 = H;
    if (bool1)
    {
      localObject3 = s.t();
      bool1 = ((com.truecaller.featuretoggles.b)localObject3).a();
      if (bool1)
      {
        j = Build.VERSION.SDK_INT;
        i17 = 26;
        if (j < i17)
        {
          j = 1;
          break label666;
        }
      }
    }
    int j = 0;
    localObject3 = null;
    label666:
    localObject8 = o();
    at.a((View)localObject8, 2131364477, j);
    localObject3 = o();
    boolean bool21 = Settings.e("showMissedCallReminders");
    Object localObject10 = -..Lambda.SettingsFragment.aw0_aNb9gAvRksYyO3RlxxxOzPY.INSTANCE;
    at.a((View)localObject3, 2131364454, bool21, (CompoundButton.OnCheckedChangeListener)localObject10);
    localObject3 = getView();
    Object localObject9 = new com/truecaller/ui/-$$Lambda$SettingsFragment$hTiKIi8kPAZ8-i7-JkzBOtVgn0w;
    ((-..Lambda.SettingsFragment.hTiKIi8kPAZ8-i7-JkzBOtVgn0w)localObject9).<init>(localSettingsFragment);
    at.a((View)localObject3, 2131364445, (View.OnClickListener)localObject9);
    localObject3 = getView();
    int i18 = 2131364444;
    localObject9 = new com/truecaller/ui/-$$Lambda$SettingsFragment$h2j-ZTRaQFZwVRQQMFfIiGCylRU;
    ((-..Lambda.SettingsFragment.h2j-ZTRaQFZwVRQQMFfIiGCylRU)localObject9).<init>(localSettingsFragment);
    at.a((View)localObject3, i18, (View.OnClickListener)localObject9);
    localObject3 = TrueApp.y();
    boolean bool2 = ((TrueApp)localObject3).isTcPayEnabled();
    if (bool2)
    {
      localObject3 = Truepay.getInstance();
      bool2 = ((Truepay)localObject3).isRegistrationComplete();
      if (bool2)
      {
        localObject3 = s.ab();
        bool2 = ((com.truecaller.featuretoggles.b)localObject3).a();
        if (bool2)
        {
          localObject3 = getView();
          localObject8 = new com/truecaller/ui/-$$Lambda$SettingsFragment$CE1opBJEXe349nww3vP9_CVZyNU;
          ((-..Lambda.SettingsFragment.CE1opBJEXe349nww3vP9_CVZyNU)localObject8).<init>(localSettingsFragment);
          i24 = 2131364443;
          at.a((View)localObject3, i24, (View.OnClickListener)localObject8);
          at.a(o(), i24, i13);
          localObject3 = o();
          i18 = 2131364306;
          at.a((View)localObject3, i18, i13);
        }
      }
    }
    bool2 = H;
    if (bool2)
    {
      localObject3 = o().findViewById(i15);
      localObject8 = m;
      localObject9 = "featureAvailability";
      boolean bool16 = ((com.truecaller.common.g.a)localObject8).a((String)localObject9, false);
      at.a((View)localObject3, bool16);
      localObject3 = o();
      int i19 = 2131364337;
      localObject3 = at.g((View)localObject3, i19);
      if (localObject3 != null)
      {
        boolean bool17 = Settings.e("availability_enabled");
        ((SwitchCompat)localObject3).setChecked(bool17);
        localObject8 = -..Lambda.SettingsFragment.FflsOXiyfIQs2cqYRwj7VQogxbo.INSTANCE;
        ((SwitchCompat)localObject3).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject8);
      }
    }
    bool2 = H;
    int i20 = 2131364462;
    int i24 = 2131364395;
    int i25 = 8;
    Object localObject12;
    if (bool2)
    {
      localObject3 = o();
      if (localObject3 != null)
      {
        localObject11 = ((bk)getActivity().getApplication()).a();
        localObject12 = ((View)localObject3).findViewById(i24);
        localObject13 = ((bp)localObject11).I();
        localObject14 = "featureFlash";
        bool22 = ((com.truecaller.common.g.a)localObject13).a((String)localObject14, false);
        at.a((View)localObject12, bool22);
        localObject12 = at.g((View)localObject3, 2131364399);
        bool22 = Settings.e("flash_enabled");
        ((SwitchCompat)localObject12).setChecked(bool22);
        localObject13 = -..Lambda.SettingsFragment.882FSXH7Y-84XFU6fRpljGFkxKM.INSTANCE;
        ((SwitchCompat)localObject12).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject13);
        i26 = 2131364463;
        localObject12 = at.g((View)localObject3, i26);
        localObject3 = ((View)localObject3).findViewById(i20);
        localObject11 = ((bp)localObject11).aU().a().a();
        bool22 = ((com.truecaller.swish.k)localObject11).a();
        if (bool22)
        {
          ((View)localObject3).setVisibility(0);
          bool2 = ((com.truecaller.swish.k)localObject11).b();
          ((SwitchCompat)localObject12).setChecked(bool2);
          localObject3 = new com/truecaller/ui/-$$Lambda$SettingsFragment$525Mlo6iPvLza1GU2eWNHGRSNI0;
          ((-..Lambda.SettingsFragment.525Mlo6iPvLza1GU2eWNHGRSNI0)localObject3).<init>((com.truecaller.swish.k)localObject11);
          ((SwitchCompat)localObject12).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject3);
        }
        else
        {
          ((View)localObject3).setVisibility(i25);
        }
      }
    }
    bool2 = H;
    if (bool2)
    {
      localObject3 = o();
      if (localObject3 != null)
      {
        localObject11 = ((View)localObject3).findViewById(2131364455);
        localObject12 = ((bk)getActivity().getApplication()).a().I();
        localObject13 = "featureSmartNotifications";
        bool22 = ((com.truecaller.common.g.a)localObject12).b((String)localObject13);
        at.a((View)localObject11, bool22);
        localObject3 = at.g((View)localObject3, 2131364456);
        boolean bool23 = ((com.truecaller.common.g.a)localObject12).b("smart_notifications");
        ((SwitchCompat)localObject3).setChecked(bool23);
        localObject11 = new com/truecaller/ui/-$$Lambda$SettingsFragment$ebmG2Opt29cd8KnjOMHkfjpl9Qk;
        ((-..Lambda.SettingsFragment.ebmG2Opt29cd8KnjOMHkfjpl9Qk)localObject11).<init>((com.truecaller.common.g.a)localObject12);
        ((SwitchCompat)localObject3).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject11);
      }
    }
    localObject3 = getResources();
    int i27 = 2130903047;
    localObject3 = ((Resources)localObject3).getStringArray(i27);
    Object localObject11 = new java/util/ArrayList;
    int i26 = 2;
    ((ArrayList)localObject11).<init>(i26);
    Object localObject13 = new com/truecaller/ui/components/n;
    Object localObject14 = localObject3[0];
    i1 = 0;
    localObject4 = null;
    ((n)localObject13).<init>((String)localObject14, null, "call");
    ((List)localObject11).add(localObject13);
    Object localObject5 = new com/truecaller/ui/components/n;
    localObject3 = localObject3[i13];
    ((n)localObject5).<init>((String)localObject3, null, "profile");
    ((List)localObject11).add(localObject5);
    localObject3 = o();
    localObject13 = "callLogTapBehavior";
    localObject3 = at.a((View)localObject3, 2131364466, (List)localObject11, (String)localObject13);
    boolean bool10 = e;
    if ((!bool10) && (localObject3 == null))
    {
      localObject1 = new java/lang/AssertionError;
      ((AssertionError)localObject1).<init>();
      throw ((Throwable)localObject1);
    }
    localObject5 = new com/truecaller/ui/-$$Lambda$SettingsFragment$P1R84FIwxTh92vZ44I1g51NSS2E;
    ((-..Lambda.SettingsFragment.P1R84FIwxTh92vZ44I1g51NSS2E)localObject5).<init>(localSettingsFragment);
    ((ComboBase)localObject3).a((ComboBase.a)localObject5);
    android.support.transition.p.a((ViewGroup)o().findViewById(2131364375));
    t();
    u();
    localObject3 = o();
    boolean bool24 = l.b("afterCall");
    localObject13 = new com/truecaller/ui/-$$Lambda$SettingsFragment$s5GNgCRwKMPwoWQ_xLZxqBdaMpw;
    ((-..Lambda.SettingsFragment.s5GNgCRwKMPwoWQ_xLZxqBdaMpw)localObject13).<init>(localSettingsFragment);
    at.a((View)localObject3, 2131364324, bool24, (CompoundButton.OnCheckedChangeListener)localObject13);
    int k = C.size();
    localObject5 = C;
    int i28 = 2131364377;
    localObject13 = Integer.valueOf(i28);
    ((List)localObject5).add(localObject13);
    localObject5 = o();
    boolean bool22 = l.b("enabledCallerIDforPB");
    localObject14 = new com/truecaller/ui/-$$Lambda$SettingsFragment$UwFcuO2WtrXOlRIM5G4xxMIogo8;
    ((-..Lambda.SettingsFragment.UwFcuO2WtrXOlRIM5G4xxMIogo8)localObject14).<init>(localSettingsFragment, k);
    at.a((View)localObject5, i28, bool22, (CompoundButton.OnCheckedChangeListener)localObject14);
    localObject3 = o();
    bool10 = G;
    at.a((View)localObject3, i28, bool10);
    localObject3 = o();
    localObject11 = l;
    boolean bool25 = ((com.truecaller.i.c)localObject11).b("clipboardSearchEnabled");
    localObject13 = new com/truecaller/ui/-$$Lambda$SettingsFragment$wlzDKM8sEqh1DckjHZzAH9xSfAU;
    ((-..Lambda.SettingsFragment.wlzDKM8sEqh1DckjHZzAH9xSfAU)localObject13).<init>(localSettingsFragment);
    at.a((View)localObject3, 2131364335, bool25, (CompoundButton.OnCheckedChangeListener)localObject13);
    w();
    localObject3 = o();
    int i9 = 2131364460;
    localObject3 = at.g((View)localObject3, i9);
    if (localObject3 != null)
    {
      localObject5 = -..Lambda.SettingsFragment.w67N5ElldZIm7g9izQpXPS1Y5nQ.INSTANCE;
      ((SwitchCompat)localObject3).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject5);
    }
    localObject3 = A;
    boolean bool3 = ((com.truecaller.calling.e.e)localObject3).a();
    if (!bool3)
    {
      localObject3 = o();
      i9 = 2131364471;
      localObject3 = ((View)localObject3).findViewById(i9);
      at.a((View)localObject3, false);
    }
    else
    {
      localObject3 = o();
      i9 = 2131364473;
      localObject11 = A;
      bool25 = ((com.truecaller.calling.e.e)localObject11).b();
      localObject13 = new com/truecaller/ui/-$$Lambda$SettingsFragment$6ZgqwYfmd6w2xWJApoYSmXlnq4g;
      ((-..Lambda.SettingsFragment.6ZgqwYfmd6w2xWJApoYSmXlnq4g)localObject13).<init>(localSettingsFragment);
      at.a((View)localObject3, i9, bool25, (CompoundButton.OnCheckedChangeListener)localObject13);
      x();
    }
    localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    localObject5 = new com/truecaller/ui/components/n;
    localObject11 = localSettingsFragment.getString(2131887098);
    ((n)localObject5).<init>((String)localObject11, "", "2");
    ((List)localObject3).add(localObject5);
    localObject5 = new com/truecaller/ui/components/n;
    int i29 = 2131887100;
    localObject11 = localSettingsFragment.getString(i29);
    localObject13 = "";
    localObject14 = "0";
    ((n)localObject5).<init>((String)localObject11, (String)localObject13, localObject14);
    ((List)localObject3).add(localObject5);
    localObject5 = o();
    localObject11 = "profileAcceptAuto";
    localObject3 = at.a((View)localObject5, (List)localObject3, (String)localObject11);
    boolean bool11 = e;
    if ((!bool11) && (localObject3 == null))
    {
      localObject1 = new java/lang/AssertionError;
      ((AssertionError)localObject1).<init>();
      throw ((Throwable)localObject1);
    }
    localObject5 = new com/truecaller/ui/-$$Lambda$SettingsFragment$elC33WyNN7y5BDHw7z1RVuYG7S0;
    ((-..Lambda.SettingsFragment.elC33WyNN7y5BDHw7z1RVuYG7S0)localObject5).<init>(localSettingsFragment);
    ((ComboBase)localObject3).a((ComboBase.a)localObject5);
    bool3 = H;
    if (bool3)
    {
      localObject3 = m;
      localObject5 = "profileBusiness";
      bool3 = ((com.truecaller.common.g.a)localObject3).a((String)localObject5, false);
      if (!bool3)
      {
        bool3 = true;
        break label2132;
      }
    }
    bool3 = false;
    localObject3 = null;
    label2132:
    at.a(o(), 2131364474, bool3);
    bool3 = Settings.i() ^ i13;
    localObject5 = o();
    at.a((View)localObject5, 2131364321, bool3);
    localObject3 = o();
    boolean bool26 = I;
    at.a((View)localObject3, 2131364423, bool26);
    localObject3 = o();
    bool26 = H;
    at.a((View)localObject3, 2131364387, bool26);
    localObject3 = o();
    localObject11 = ThemeManager.a();
    int i30 = displayName;
    at.a((View)localObject3, 2131364385, i30);
    localObject3 = at.g(o(), 2131364458);
    int i10 = 3;
    if (localObject3 != null)
    {
      localObject11 = l;
      localObject13 = "merge_by";
      i30 = ((com.truecaller.i.c)localObject11).a((String)localObject13, i10);
      if (i30 == i10)
      {
        i30 = 1;
      }
      else
      {
        i30 = 0;
        localObject11 = null;
      }
      ((SwitchCompat)localObject3).setChecked(i30);
      localObject11 = new com/truecaller/ui/-$$Lambda$SettingsFragment$_VPGrxLDCQfguw83LgiDd0q_fb8;
      ((-..Lambda.SettingsFragment._VPGrxLDCQfguw83LgiDd0q_fb8)localObject11).<init>(localSettingsFragment);
      ((SwitchCompat)localObject3).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject11);
    }
    localObject3 = o();
    int i31 = 2131364447;
    localObject3 = at.g((View)localObject3, i31);
    if (localObject3 != null)
    {
      localObject11 = l;
      localObject13 = "showFrequentlyCalledContacts";
      bool27 = ((com.truecaller.i.c)localObject11).a((String)localObject13, i13);
      ((SwitchCompat)localObject3).setChecked(bool27);
      localObject11 = new com/truecaller/ui/-$$Lambda$SettingsFragment$-T63IJFXMD3XUceq6CS1ulyLHsg;
      ((-..Lambda.SettingsFragment.-T63IJFXMD3XUceq6CS1ulyLHsg)localObject11).<init>(localSettingsFragment);
      ((SwitchCompat)localObject3).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject11);
    }
    localObject3 = Locale.US;
    localObject13 = new Object[i13];
    localObject14 = "10.41.6";
    localObject13[0] = localObject14;
    localObject3 = String.format((Locale)localObject3, "v%s", (Object[])localObject13);
    localObject11 = "1114.0dbb949";
    boolean bool27 = TextUtils.isEmpty((CharSequence)localObject11);
    if (!bool27)
    {
      localObject11 = new java/lang/StringBuilder;
      ((StringBuilder)localObject11).<init>();
      ((StringBuilder)localObject11).append((String)localObject3);
      localObject3 = Locale.US;
      localObject13 = " (%s)";
      localObject14 = new Object[i13];
      String str2 = "1114.0dbb949";
      localObject14[0] = str2;
      localObject3 = String.format((Locale)localObject3, (String)localObject13, (Object[])localObject14);
      ((StringBuilder)localObject11).append((String)localObject3);
      localObject3 = ((StringBuilder)localObject11).toString();
    }
    long l1 = m.a("profileUserId", 0L);
    localObject11 = new com/truecaller/ui/-$$Lambda$SettingsFragment$RQZ5hcOT5ciVoyEqR6CeHaUgMKA;
    ((-..Lambda.SettingsFragment.RQZ5hcOT5ciVoyEqR6CeHaUgMKA)localObject11).<init>(localSettingsFragment, (String)localObject3, l1);
    localObject13 = new java/util/ArrayList;
    ((ArrayList)localObject13).<init>();
    localObject14 = new com/truecaller/ui/components/n;
    ((n)localObject14).<init>((String)localObject3, "");
    ((List)localObject13).add(localObject14);
    b.setData((List)localObject13);
    b.setOnLongClickListener((View.OnLongClickListener)localObject11);
    b.setOnClickListener(null);
    localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    localObject7 = new com/truecaller/ui/components/n;
    localObject8 = String.valueOf(l1);
    ((n)localObject7).<init>((String)localObject8, "");
    ((List)localObject3).add(localObject7);
    c.setData((List)localObject3);
    c.setOnLongClickListener((View.OnLongClickListener)localObject11);
    c.setOnClickListener(null);
    localObject3 = p.a();
    localObject7 = g;
    localObject8 = new com/truecaller/ui/components/n;
    ((n)localObject8).<init>((String)localObject3, "");
    localObject8 = Collections.singletonList(localObject8);
    ((ComboBase)localObject7).setData((List)localObject8);
    g.setOnClickListener(null);
    localObject7 = g;
    localObject8 = new com/truecaller/ui/-$$Lambda$SettingsFragment$Rdzk1Ld45E3ogvYpYfeeW135yX8;
    ((-..Lambda.SettingsFragment.Rdzk1Ld45E3ogvYpYfeeW135yX8)localObject8).<init>(localSettingsFragment, (String)localObject3);
    ((ComboBase)localObject7).setOnLongClickListener((View.OnLongClickListener)localObject8);
    localObject3 = o();
    boolean bool18 = H;
    at.a((View)localObject3, 2131364062, bool18);
    localObject3 = o();
    bool18 = H;
    at.a((View)localObject3, 2131364434, bool18);
    localObject3 = (ComboBase)getActivity().findViewById(2131364402);
    new SettingsFragment.3(localSettingsFragment, localSettingsFragment, (ComboBase)localObject3);
    localObject3 = (ComboBase)getActivity().findViewById(2131364464);
    new SettingsFragment.2(localSettingsFragment, localSettingsFragment, (ComboBase)localObject3);
    o().findViewById(2131364438).setOnClickListener(localSettingsFragment);
    o().findViewById(2131364416).setOnClickListener(localSettingsFragment);
    o().findViewById(2131364440).setOnClickListener(localSettingsFragment);
    o().findViewById(2131364397).setOnClickListener(localSettingsFragment);
    o().findViewById(2131364418).setOnClickListener(localSettingsFragment);
    localObject3 = at.g(o(), 2131364419);
    boolean bool15 = J.i();
    ((SwitchCompat)localObject3).setChecked(bool15);
    localObject7 = new com/truecaller/ui/-$$Lambda$SettingsFragment$CTp37yZPcfhC7ccxXJZiYP3g0VE;
    ((-..Lambda.SettingsFragment.CTp37yZPcfhC7ccxXJZiYP3g0VE)localObject7).<init>(localSettingsFragment);
    ((SwitchCompat)localObject3).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject7);
    localObject3 = ((bk)getActivity().getApplication()).a().U();
    localObject7 = getView();
    int i21 = 2131364408;
    localObject9 = new com/truecaller/ui/-$$Lambda$SettingsFragment$cjwjMK6dV1GPQpzkvlAT3EAMfQ8;
    ((-..Lambda.SettingsFragment.cjwjMK6dV1GPQpzkvlAT3EAMfQ8)localObject9).<init>(localSettingsFragment);
    at.a((View)localObject7, i21, (View.OnClickListener)localObject9);
    localObject7 = y;
    localObject8 = new String[] { "android.permission.READ_SMS" };
    bool15 = ((l)localObject7).a((String[])localObject8);
    if (bool15)
    {
      localObject7 = n;
      bool15 = ((com.truecaller.utils.d)localObject7).a();
      if (bool15)
      {
        bool15 = true;
        break label3103;
      }
    }
    bool15 = false;
    localObject7 = null;
    label3103:
    localObject8 = ((com.truecaller.multisim.h)localObject3).a(0);
    int i32;
    if (localObject8 != null)
    {
      localObject9 = o();
      i32 = 2131364420;
      at.a((View)localObject9, i32, i13);
      localObject8 = b;
      localObject8 = ((com.truecaller.multisim.h)localObject3).c((String)localObject8);
      boolean bool19 = ((com.truecaller.multisim.a)localObject8).b();
      if (bool19)
      {
        at.a(o(), 2131364510, i13);
        localObject8 = o();
        i24 = 2131364511;
        localObject8 = at.g((View)localObject8, i24);
        localObject9 = new com/truecaller/ui/-$$Lambda$SettingsFragment$iwxYMgtzOtRqxOgl9gKmjYR2SkE;
        ((-..Lambda.SettingsFragment.iwxYMgtzOtRqxOgl9gKmjYR2SkE)localObject9).<init>(localSettingsFragment);
        ((SwitchCompat)localObject8).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject9);
      }
      localObject8 = o();
      i24 = 2131364508;
      localObject8 = at.g((View)localObject8, i24);
      localObject9 = new com/truecaller/ui/-$$Lambda$SettingsFragment$dIH1JxV-_LJ_lXQEsztRCNIZ79U;
      ((-..Lambda.SettingsFragment.dIH1JxV-_LJ_lXQEsztRCNIZ79U)localObject9).<init>(localSettingsFragment);
      ((SwitchCompat)localObject8).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject9);
      if (bool15)
      {
        i24 = 0;
        localObject9 = null;
      }
      else
      {
        i24 = 8;
      }
      ((SwitchCompat)localObject8).setVisibility(i24);
      localObject8 = o();
      i24 = 2131364509;
      localObject8 = at.g((View)localObject8, i24);
      localObject9 = new com/truecaller/ui/-$$Lambda$SettingsFragment$VMLZJYYZB-qw2VGm2GZ4-1TyJeo;
      ((-..Lambda.SettingsFragment.VMLZJYYZB-qw2VGm2GZ4-1TyJeo)localObject9).<init>(localSettingsFragment);
      ((SwitchCompat)localObject8).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject9);
      if (bool15)
      {
        i24 = 0;
        localObject9 = null;
      }
      else
      {
        i24 = 8;
      }
      ((SwitchCompat)localObject8).setVisibility(i24);
    }
    else
    {
      localObject8 = o();
      i24 = 2131364420;
      at.a((View)localObject8, i24, false);
    }
    localObject8 = ((com.truecaller.multisim.h)localObject3).a(i13);
    if (localObject8 != null)
    {
      localObject9 = o();
      i32 = 2131364421;
      at.a((View)localObject9, i32, i13);
      localObject8 = b;
      localObject3 = ((com.truecaller.multisim.h)localObject3).c((String)localObject8);
      bool3 = ((com.truecaller.multisim.a)localObject3).b();
      if (bool3)
      {
        at.a(o(), 2131364517, i13);
        localObject3 = o();
        i22 = 2131364518;
        localObject3 = at.g((View)localObject3, i22);
        localObject8 = new com/truecaller/ui/-$$Lambda$SettingsFragment$Heh_0O9fs33IbkDqGd830XECuY4;
        ((-..Lambda.SettingsFragment.Heh_0O9fs33IbkDqGd830XECuY4)localObject8).<init>(localSettingsFragment);
        ((SwitchCompat)localObject3).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject8);
      }
      localObject3 = o();
      int i22 = 2131364515;
      localObject3 = at.g((View)localObject3, i22);
      localObject8 = new com/truecaller/ui/-$$Lambda$SettingsFragment$AZu2C95h6ghM0v99g2bmuwP18VI;
      ((-..Lambda.SettingsFragment.AZu2C95h6ghM0v99g2bmuwP18VI)localObject8).<init>(localSettingsFragment);
      ((SwitchCompat)localObject3).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject8);
      if (bool15)
      {
        i22 = 0;
        localObject8 = null;
      }
      else
      {
        i22 = 8;
      }
      ((SwitchCompat)localObject3).setVisibility(i22);
      localObject3 = o();
      i22 = 2131364516;
      localObject3 = at.g((View)localObject3, i22);
      localObject8 = new com/truecaller/ui/-$$Lambda$SettingsFragment$U-RcNI9aOWlF9GekrunW-CoVsms;
      ((-..Lambda.SettingsFragment.U-RcNI9aOWlF9GekrunW-CoVsms)localObject8).<init>(localSettingsFragment);
      ((SwitchCompat)localObject3).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject8);
      if (bool15)
      {
        bool15 = false;
        localObject7 = null;
      }
      else
      {
        i16 = 8;
      }
      ((SwitchCompat)localObject3).setVisibility(i16);
    }
    else
    {
      localObject3 = o();
      i16 = 2131364421;
      at.a((View)localObject3, i16, false);
    }
    localObject3 = o();
    int i16 = 2131364435;
    localObject3 = at.h((View)localObject3, i16);
    if (localObject3 != null)
    {
      localObject7 = new java/util/ArrayList;
      ((ArrayList)localObject7).<init>(i26);
      localObject8 = new com/truecaller/ui/components/n;
      localObject11 = Boolean.TRUE;
      ((n)localObject8).<init>(2131887036, localObject11);
      ((List)localObject7).add(localObject8);
      localObject8 = new com/truecaller/ui/components/n;
      i24 = 2131887035;
      localObject11 = Boolean.FALSE;
      ((n)localObject8).<init>(i24, localObject11);
      ((List)localObject7).add(localObject8);
      ((ComboBase)localObject3).setData((List)localObject7);
      localObject8 = i;
      bool20 = ((com.truecaller.messaging.h)localObject8).y();
      if (bool20)
      {
        localObject7 = (n)((List)localObject7).get(0);
        ((ComboBase)localObject3).setSelection((n)localObject7);
      }
      else
      {
        localObject7 = (n)((List)localObject7).get(i13);
        ((ComboBase)localObject3).setSelection((n)localObject7);
      }
      localObject7 = new com/truecaller/ui/-$$Lambda$SettingsFragment$_H4ZNuiFArorx_ZiM2vJ1hXCUoE;
      ((-..Lambda.SettingsFragment._H4ZNuiFArorx_ZiM2vJ1hXCUoE)localObject7).<init>(localSettingsFragment);
      ((ComboBase)localObject3).a((ComboBase.a)localObject7);
    }
    i();
    localObject3 = getView();
    localObject8 = new com/truecaller/ui/-$$Lambda$SettingsFragment$S7QnhOAqQGEuyeSxWpgKCdHgx9g;
    ((-..Lambda.SettingsFragment.S7QnhOAqQGEuyeSxWpgKCdHgx9g)localObject8).<init>(localSettingsFragment);
    at.a((View)localObject3, 2131364415, (View.OnClickListener)localObject8);
    localObject3 = getView();
    i16 = 2131364426;
    localObject8 = new com/truecaller/ui/-$$Lambda$SettingsFragment$b3EMRftcyrB4GI44wOVakKUQnh4;
    ((-..Lambda.SettingsFragment.b3EMRftcyrB4GI44wOVakKUQnh4)localObject8).<init>(localSettingsFragment);
    at.a((View)localObject3, i16, (View.OnClickListener)localObject8);
    localObject3 = B;
    bool3 = ((bw)localObject3).a();
    if (!bool3)
    {
      localObject3 = o();
      i10 = 2131364411;
      at.a((View)localObject3, i10, false);
    }
    else
    {
      at.a(o(), 2131364411, i13);
      localObject3 = o();
      i16 = 2131364384;
      localObject3 = at.h((View)localObject3, i16);
      if (localObject3 != null)
      {
        localObject7 = new java/util/ArrayList;
        ((ArrayList)localObject7).<init>(i10);
        localObject5 = new com/truecaller/ui/components/n;
        ((n)localObject5).<init>(2131887032, "wifi");
        ((List)localObject7).add(localObject5);
        localObject5 = new com/truecaller/ui/components/n;
        ((n)localObject5).<init>(2131887034, "wifiOrMobile");
        ((List)localObject7).add(localObject5);
        localObject5 = new com/truecaller/ui/components/n;
        localObject9 = "never";
        ((n)localObject5).<init>(2131887031, localObject9);
        ((List)localObject7).add(localObject5);
        ((ComboBase)localObject3).setData((List)localObject7);
        localObject5 = i.B();
        localObject8 = "wifi";
        bool20 = ((String)localObject8).equals(localObject5);
        if (bool20)
        {
          localObject5 = (n)((List)localObject7).get(0);
          ((ComboBase)localObject3).setSelection((n)localObject5);
        }
        else
        {
          localObject8 = "wifiOrMobile";
          bool12 = ((String)localObject8).equals(localObject5);
          if (bool12)
          {
            localObject5 = (n)((List)localObject7).get(i13);
            ((ComboBase)localObject3).setSelection((n)localObject5);
          }
          else
          {
            localObject5 = (n)((List)localObject7).get(i26);
            ((ComboBase)localObject3).setSelection((n)localObject5);
          }
        }
        localObject5 = new com/truecaller/ui/-$$Lambda$SettingsFragment$hpM_gSwCKjLaJGOq2e9GGfRk8nw;
        ((-..Lambda.SettingsFragment.hpM_gSwCKjLaJGOq2e9GGfRk8nw)localObject5).<init>(localSettingsFragment);
        ((ComboBase)localObject3).a((ComboBase.a)localObject5);
      }
    }
    android.support.transition.p.a((ViewGroup)o().findViewById(2131364339));
    m();
    localObject3 = com.truecaller.utils.ui.b.a(requireContext(), 2131234408, 2130969592);
    ((TextView)o().findViewById(2131364359)).setCompoundDrawablesWithIntrinsicBounds((Drawable)localObject3, null, null, null);
    localObject5 = new com/truecaller/ui/-$$Lambda$SettingsFragment$KwNE5leXVQc9NwvSXPONuHlix7A;
    ((-..Lambda.SettingsFragment.KwNE5leXVQc9NwvSXPONuHlix7A)localObject5).<init>(localSettingsFragment);
    Z = ((CompoundButton.OnCheckedChangeListener)localObject5);
    localObject5 = at.g(o(), 2131364360);
    localObject7 = Z;
    ((SwitchCompat)localObject5).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject7);
    ((TextView)o().findViewById(2131364346)).setCompoundDrawablesWithIntrinsicBounds((Drawable)localObject3, null, null, null);
    localObject5 = new com/truecaller/ui/-$$Lambda$SettingsFragment$njXnc2klUKdQ6k4CMm2uIo9ZFLA;
    ((-..Lambda.SettingsFragment.njXnc2klUKdQ6k4CMm2uIo9ZFLA)localObject5).<init>(localSettingsFragment);
    aa = ((CompoundButton.OnCheckedChangeListener)localObject5);
    localObject5 = at.g(o(), 2131364347);
    localObject7 = aa;
    ((SwitchCompat)localObject5).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject7);
    ((TextView)o().findViewById(2131364355)).setCompoundDrawablesWithIntrinsicBounds((Drawable)localObject3, null, null, null);
    localObject5 = o();
    boolean bool20 = s.M().a();
    at.a((View)localObject5, 2131364354, bool20);
    localObject5 = new com/truecaller/ui/-$$Lambda$SettingsFragment$0qsqxq_yo5LmLGLw8sZJjaFOQ8Y;
    ((-..Lambda.SettingsFragment.0qsqxq_yo5LmLGLw8sZJjaFOQ8Y)localObject5).<init>(localSettingsFragment);
    ab = ((CompoundButton.OnCheckedChangeListener)localObject5);
    localObject5 = at.g(o(), 2131364356);
    localObject7 = ab;
    ((SwitchCompat)localObject5).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject7);
    ((TextView)o().findViewById(2131364343)).setCompoundDrawablesWithIntrinsicBounds((Drawable)localObject3, null, null, null);
    localObject5 = o();
    localObject8 = s.O();
    bool20 = ((com.truecaller.featuretoggles.b)localObject8).a();
    at.a((View)localObject5, 2131364342, bool20);
    localObject5 = new com/truecaller/ui/-$$Lambda$SettingsFragment$mUWFC4dBrD2xaf-oV-ylKPFSn-Q;
    ((-..Lambda.SettingsFragment.mUWFC4dBrD2xaf-oV-ylKPFSn-Q)localObject5).<init>(localSettingsFragment);
    ac = ((CompoundButton.OnCheckedChangeListener)localObject5);
    localObject5 = at.g(o(), 2131364344);
    localObject7 = ac;
    ((SwitchCompat)localObject5).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject7);
    ((TextView)o().findViewById(2131364349)).setCompoundDrawablesWithIntrinsicBounds((Drawable)localObject3, null, null, null);
    localObject3 = o();
    localObject5 = s.S();
    boolean bool12 = ((com.truecaller.featuretoggles.b)localObject5).a();
    at.a((View)localObject3, 2131364348, bool12);
    localObject3 = new com/truecaller/ui/-$$Lambda$SettingsFragment$YBhqETaMRjy_9ccXAT8BPyDDVWs;
    ((-..Lambda.SettingsFragment.YBhqETaMRjy_9ccXAT8BPyDDVWs)localObject3).<init>(localSettingsFragment);
    ad = ((CompoundButton.OnCheckedChangeListener)localObject3);
    localObject3 = o();
    i1 = 2131364350;
    localObject3 = at.g((View)localObject3, i1);
    localObject4 = ad;
    ((SwitchCompat)localObject3).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject4);
    localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    localObject4 = new com/truecaller/ui/components/n;
    i16 = 4;
    localObject7 = Integer.valueOf(i16);
    ((n)localObject4).<init>(2131886157, localObject7);
    ((List)localObject3).add(localObject4);
    localObject4 = new com/truecaller/ui/components/n;
    localObject7 = Integer.valueOf(i25);
    ((n)localObject4).<init>(2131886158, localObject7);
    ((List)localObject3).add(localObject4);
    localObject4 = o();
    int i11 = 2131364353;
    localObject4 = at.h((View)localObject4, i11);
    if (localObject4 != null)
    {
      ((ComboBase)localObject4).setData((List)localObject3);
      localObject5 = new com/truecaller/ui/-$$Lambda$SettingsFragment$Awohq_-vY5xlyIBosI0-hpnRXRc;
      ((-..Lambda.SettingsFragment.Awohq_-vY5xlyIBosI0-hpnRXRc)localObject5).<init>(localSettingsFragment, (ComboBase)localObject4, (List)localObject3);
      ((ComboBase)localObject4).a((ComboBase.a)localObject5);
    }
    localObject3 = at.g(o(), 2131364341);
    localObject4 = new com/truecaller/ui/-$$Lambda$SettingsFragment$KkcTFqnMzJxsfP9FA2Z-c1krXTw;
    ((-..Lambda.SettingsFragment.KkcTFqnMzJxsfP9FA2Z-c1krXTw)localObject4).<init>(localSettingsFragment);
    ((SwitchCompat)localObject3).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject4);
    localObject3 = o();
    i1 = 2131364352;
    localObject3 = at.g((View)localObject3, i1);
    localObject4 = new com/truecaller/ui/-$$Lambda$SettingsFragment$ysajJ-6wiORHt3ZHZv2npui3zVg;
    ((-..Lambda.SettingsFragment.ysajJ-6wiORHt3ZHZv2npui3zVg)localObject4).<init>(localSettingsFragment);
    ((SwitchCompat)localObject3).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject4);
    localObject3 = x;
    bool3 = ((com.truecaller.whoviewedme.w)localObject3).a();
    if (bool3)
    {
      localObject3 = w;
      bool3 = ((com.truecaller.common.f.c)localObject3).d();
      if (!bool3)
      {
        localObject3 = x;
        ((com.truecaller.whoviewedme.w)localObject3).a(false);
      }
      localObject3 = o().findViewById(2131364475);
      localObject4 = x;
      boolean bool4 = ((com.truecaller.whoviewedme.w)localObject4).c();
      at.a((View)localObject3, bool4);
      int i2 = 2131364479;
      localObject3 = at.g((View)localObject3, i2);
      if (localObject3 != null)
      {
        bool5 = x.e();
        ((SwitchCompat)localObject3).setChecked(bool5);
        localObject4 = new com/truecaller/ui/-$$Lambda$SettingsFragment$Slmqfvbb4izvLqjoo2f1xKtu6t0;
        ((-..Lambda.SettingsFragment.Slmqfvbb4izvLqjoo2f1xKtu6t0)localObject4).<init>(localSettingsFragment);
        ((SwitchCompat)localObject3).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject4);
      }
    }
    localObject3 = (LinearLayout)f.inflate();
    d = ((LinearLayout)localObject3);
    a((TextView)d.findViewById(2131364407), 2131234159);
    localObject3 = (TextView)d.findViewById(2131364427);
    boolean bool5 = G;
    if (!bool5)
    {
      ((TextView)localObject3).setVisibility(0);
      int i3 = 2131234374;
      a((TextView)localObject3, i3);
    }
    else
    {
      ((TextView)localObject3).setVisibility(i25);
    }
    a((TextView)d.findViewById(2131364404), 2131234246);
    localObject3 = (TextView)d.findViewById(2131364383);
    a((TextView)localObject3, 2131233937);
    ((TextView)localObject3).setVisibility(0);
    a((TextView)d.findViewById(2131364441), 2131234471);
    a((TextView)d.findViewById(2131364327), 2131233833);
    a((TextView)d.findViewById(2131364422), 2131234560);
    localObject3 = (TextView)d.findViewById(2131364467);
    localObject4 = TrueApp.y();
    boolean bool6 = ((TrueApp)localObject4).isTcPayEnabled();
    if (bool6)
    {
      localObject4 = Truepay.getInstance();
      bool6 = ((Truepay)localObject4).isRegistrationComplete();
      if (bool6)
      {
        ((TextView)localObject3).setVisibility(0);
        int i4 = 2131233867;
        a((TextView)localObject3, i4);
      }
    }
    a((TextView)d.findViewById(2131364361), 2131233891);
    localObject3 = (TextView)d.findViewById(2131364430);
    boolean bool7 = H;
    if (bool7)
    {
      int i5 = 2131234261;
      a((TextView)localObject3, i5);
    }
    else
    {
      ((TextView)localObject3).setVisibility(i25);
    }
    localObject3 = (TextView)d.findViewById(2131364338);
    localObject4 = s.l();
    boolean bool8 = ((com.truecaller.featuretoggles.b)localObject4).a();
    if (bool8)
    {
      bool8 = H;
      if (bool8)
      {
        localObject4 = n.l();
        str1 = "kenzo";
        bool8 = ((String)localObject4).equals(str1);
        if (!bool8)
        {
          int i6 = 2131234004;
          a((TextView)localObject3, i6);
          break label5431;
        }
      }
    }
    ((TextView)localObject3).setVisibility(i25);
    label5431:
    localObject3 = (TextView)d.findViewById(2131364366);
    localObject4 = V;
    boolean bool9 = ((com.truecaller.calling.recorder.h)localObject4).a();
    if (!bool9)
    {
      ((TextView)localObject3).setVisibility(i25);
    }
    else
    {
      i7 = 2131234539;
      a((TextView)localObject3, i7);
    }
    localObject3 = (TextView)d.findViewById(2131364319);
    int i7 = 2131233787;
    a((TextView)localObject3, i7);
    ((View)localObject1).findViewById(2131364468).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364429).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364318).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364364).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364431).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364442).setOnClickListener(localSettingsFragment);
    localObject3 = ((View)localObject1).findViewById(2131364322);
    localObject4 = new com/truecaller/ui/-$$Lambda$SettingsFragment$jlCkh6P1ojxReJVMuJubE9dJgBg;
    ((-..Lambda.SettingsFragment.jlCkh6P1ojxReJVMuJubE9dJgBg)localObject4).<init>(localSettingsFragment);
    ((View)localObject3).setOnClickListener((View.OnClickListener)localObject4);
    localObject3 = ((View)localObject1).findViewById(2131364320);
    localObject4 = new com/truecaller/ui/-$$Lambda$SettingsFragment$5eDnwJqeQKOs2VHtFqTQlDNAc3c;
    ((-..Lambda.SettingsFragment.5eDnwJqeQKOs2VHtFqTQlDNAc3c)localObject4).<init>(localSettingsFragment);
    ((View)localObject3).setOnClickListener((View.OnClickListener)localObject4);
    localObject3 = ((View)localObject1).findViewById(2131364390);
    localObject4 = new com/truecaller/ui/-$$Lambda$SettingsFragment$ON5zneMc5wDHqfHhTWHdUwTQx0A;
    ((-..Lambda.SettingsFragment.ON5zneMc5wDHqfHhTWHdUwTQx0A)localObject4).<init>(localSettingsFragment);
    ((View)localObject3).setOnClickListener((View.OnClickListener)localObject4);
    ((View)localObject1).findViewById(2131364432).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364436).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364386).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364407).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364380).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364378).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364425).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364362).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364427).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364404).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364383).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364441).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364422).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364467).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364358).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364345).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364354).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364342).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364348).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364340).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364351).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364470).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364361).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364327).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364319).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364330).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364461).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364325).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364336).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364395).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364462).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364455).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364391).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364333).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364459).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364471).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364376).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364323).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364448).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364450).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364476).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364503).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364452).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364457).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364338).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364430).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364366).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364475).setOnClickListener(localSettingsFragment);
    ((View)localObject1).findViewById(2131364382).setOnClickListener(localSettingsFragment);
    localObject1 = getActivity().getIntent();
    bool3 = ((Intent)localObject1).getBooleanExtra("returnToMain", i13);
    M = bool3;
    localObject3 = "returnToMain";
    ((Intent)localObject1).removeExtra((String)localObject3);
    if (localObject2 != null)
    {
      localObject1 = ((Bundle)localObject2).getString("subView");
      O = ((String)localObject1);
    }
    else
    {
      localObject2 = ((Intent)localObject1).getStringExtra("ARG_SUBVIEW");
      O = ((String)localObject2);
      localObject2 = "ARG_SUBVIEW";
      ((Intent)localObject1).removeExtra((String)localObject2);
    }
    localObject1 = O;
    boolean bool28 = am.a((CharSequence)localObject1);
    if (bool28)
    {
      localObject1 = SettingsFragment.SettingsViewType.valueOf(O);
      localObject2 = SettingsFragment.5.a;
      int i33 = ((SettingsFragment.SettingsViewType)localObject1).ordinal();
      i33 = localObject2[i33];
      switch (i33)
      {
      default: 
        break;
      case 13: 
        localObject1 = V;
        boolean bool29 = ((com.truecaller.calling.recorder.h)localObject1).a();
        if (bool29)
        {
          g();
          localObject1 = getActivity();
          if (localObject1 == null) {
            return;
          }
          localObject1 = getActivity();
          localObject2 = new android/content/Intent;
          localObject3 = getActivity();
          ((Intent)localObject2).<init>((Context)localObject3, CallRecordingSettingsActivity.class);
          ((android.support.v4.app.f)localObject1).startActivity((Intent)localObject2);
          return;
        }
        break;
      case 12: 
        I();
        return;
      case 11: 
        H();
        return;
      case 10: 
        G();
        return;
      case 9: 
        localObject2 = SettingsFragment.SettingsViewType.SETTINGS_GENERAL;
        localSettingsFragment.a(2131887081, (SettingsFragment.SettingsViewType)localObject2, 2131364424);
        return;
      case 8: 
        F();
        return;
      case 7: 
        E();
        return;
      case 6: 
        h();
        localObject1 = getActivity();
        int i34 = 2131364402;
        localObject1 = (ComboBase)((android.support.v4.app.f)localObject1).findViewById(i34);
        if (localObject1 != null)
        {
          localObject2 = ((ComboBase)localObject1).getItems();
          if (localObject2 != null)
          {
            ((ComboBase)localObject1).callOnClick();
            return;
          }
          localObject2 = new com/truecaller/ui/SettingsFragment$4;
          ((SettingsFragment.4)localObject2).<init>(localSettingsFragment);
          ((ComboBase)localObject1).a((ComboBase.a)localObject2);
        }
        return;
      case 5: 
        h();
        return;
      case 4: 
        g();
        return;
      case 3: 
        D();
        return;
      case 2: 
        C();
        return;
      case 1: 
        B();
        return;
      }
      localObject1 = getActivity();
      ((android.support.v4.app.f)localObject1).finish();
    }
  }
  
  public final void p()
  {
    w();
    x();
  }
  
  public void startActivityForResult(Intent paramIntent, int paramInt)
  {
    int j = C.size();
    paramInt += j;
    super.startActivityForResult(paramIntent, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.SettingsFragment
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */