package com.truecaller.ui;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.a;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.util.at;
import com.truecaller.utils.ui.b;
import java.util.List;

final class g$f
  extends BaseAdapter
{
  long a;
  private final LayoutInflater c;
  
  private g$f(g paramg, Context paramContext)
  {
    paramg = LayoutInflater.from(paramContext);
    c = paramg;
  }
  
  public final int getCount()
  {
    return b.a.size();
  }
  
  public final Object getItem(int paramInt)
  {
    return b.a.get(paramInt);
  }
  
  public final long getItemId(int paramInt)
  {
    Object localObject = getItem(paramInt);
    boolean bool = localObject instanceof g.d;
    if (bool) {
      return a;
    }
    return -1;
  }
  
  public final int getItemViewType(int paramInt)
  {
    Object localObject = getItem(paramInt);
    boolean bool = localObject instanceof g.g;
    if (bool) {
      return 0;
    }
    paramInt = localObject instanceof g.b;
    if (paramInt != 0) {
      return 1;
    }
    return 2;
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    Object localObject1 = getItem(paramInt);
    boolean bool = localObject1 instanceof g.g;
    int j = 0;
    Context localContext = null;
    int k;
    if (bool)
    {
      if (paramView == null)
      {
        paramView = c;
        k = 2131558559;
        paramView = paramView.inflate(k, paramViewGroup, false);
      }
    }
    else
    {
      bool = localObject1 instanceof g.b;
      if (bool)
      {
        if (paramView == null)
        {
          paramView = c;
          k = 2131558557;
          paramView = paramView.inflate(k, paramViewGroup, false);
        }
      }
      else
      {
        localObject1 = (g.d)localObject1;
        int i = 2131364664;
        if (paramView == null)
        {
          paramView = c;
          int m = 2131558558;
          paramView = paramView.inflate(m, paramViewGroup, false);
          paramViewGroup = new com/truecaller/ui/g$e;
          paramViewGroup.<init>(paramView, (byte)0);
          paramView.setTag(i, paramViewGroup);
        }
        else
        {
          paramView = paramView.getTag(i);
          paramViewGroup = paramView;
          paramViewGroup = (g.e)paramView;
        }
        paramView = c;
        Object localObject2;
        if (paramView != null)
        {
          paramView = c;
          localObject2 = c;
          paramView.setText((CharSequence)localObject2);
        }
        else
        {
          paramView = c;
          i = b;
          paramView.setText(i);
        }
        paramView = b;
        i = d;
        if (i == 0)
        {
          k = 0;
          localObject1 = null;
        }
        else
        {
          localObject2 = b.getContext();
          j = d;
          localObject2 = a.e(at.a((Context)localObject2, j).mutate());
          localContext = b.getContext();
          k = e;
          localObject1 = b.b(localContext, k);
          a.a((Drawable)localObject2, (ColorStateList)localObject1);
          localObject1 = localObject2;
        }
        paramView.setImageDrawable((Drawable)localObject1);
        paramView = a;
      }
    }
    paramInt = isEnabled(paramInt);
    paramView.setEnabled(paramInt);
    return paramView;
  }
  
  public final int getViewTypeCount()
  {
    return 3;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.g.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */