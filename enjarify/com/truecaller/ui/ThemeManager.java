package com.truecaller.ui;

import android.text.TextUtils;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.flash.o;
import com.truecaller.old.data.access.Settings;

public final class ThemeManager
{
  public static ThemeManager.Theme a()
  {
    String str = Settings.b("currentTheme");
    boolean bool = TextUtils.isEmpty(str);
    if (bool) {
      return ThemeManager.Theme.DEFAULT;
    }
    return ThemeManager.Theme.valueOf(str);
  }
  
  public static void a(ThemeManager.Theme paramTheme)
  {
    String str = paramTheme.name();
    Settings.b("currentTheme", str);
    TrueApp.y().a().be().a(paramTheme);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.ThemeManager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */