package com.truecaller.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.google.gson.l;
import com.google.gson.q;
import com.truecaller.common.h.am;
import com.truecaller.common.h.aq.d;
import com.truecaller.common.h.j;
import com.truecaller.network.notification.NotificationType;
import com.truecaller.old.data.entity.Notification;
import com.truecaller.old.data.entity.Notification.NotificationActionHistoryItem;
import com.truecaller.old.data.entity.Notification.NotificationActionHistoryItem.Action;
import com.truecaller.ui.components.n;
import com.truecaller.util.at;
import java.util.List;
import java.util.concurrent.TimeUnit;

public final class t
  extends o
{
  private Button a;
  private Button b;
  private TextView c;
  private TextView d;
  private ImageView e;
  private TextView f;
  private Notification g;
  private final View.OnClickListener h;
  private final View.OnClickListener i;
  
  public t()
  {
    Object localObject = new com/truecaller/ui/t$1;
    ((t.1)localObject).<init>(this);
    h = ((View.OnClickListener)localObject);
    localObject = new com/truecaller/ui/t$2;
    ((t.2)localObject).<init>(this);
    i = ((View.OnClickListener)localObject);
  }
  
  private static void a(Button paramButton, String paramString, View.OnClickListener paramOnClickListener)
  {
    paramButton.setText(paramString);
    paramButton.setOnClickListener(paramOnClickListener);
    boolean bool = TextUtils.isEmpty(paramString);
    int j;
    if (bool)
    {
      j = 8;
    }
    else
    {
      j = 0;
      paramString = null;
    }
    paramButton.setVisibility(j);
  }
  
  private void b()
  {
    Object localObject1 = g;
    Object localObject2 = getContext();
    ((Notification)localObject1).d((Context)localObject2);
    localObject1 = c;
    localObject2 = g.m;
    at.b((TextView)localObject1, (CharSequence)localObject2);
    localObject1 = d;
    localObject2 = g.n;
    at.b((TextView)localObject1, (CharSequence)localObject2);
    localObject1 = g.g();
    f.setVisibility(0);
    localObject2 = f;
    Context localContext = getContext();
    Object localObject3 = TimeUnit.SECONDS;
    long l1 = ((Long)localObject1).longValue();
    long l2 = ((TimeUnit)localObject3).toMillis(l1);
    localObject1 = j.b(localContext, l2);
    ((TextView)localObject2).setText((CharSequence)localObject1);
    localObject1 = g;
    int j = ((Notification)localObject1).o();
    localObject2 = g.a("i");
    boolean bool = am.a((CharSequence)localObject2);
    localContext = null;
    if (bool)
    {
      localObject2 = w.a(getContext());
      localObject3 = g.a("i");
      localObject2 = ((w)localObject2).a((String)localObject3);
      localObject3 = aq.d.b();
      localObject1 = ((ab)localObject2).a((ai)localObject3).b(j);
      c = true;
      localObject1 = ((ab)localObject1).c();
      localObject2 = e;
      ((ab)localObject1).a((ImageView)localObject2, null);
      return;
    }
    localObject1 = w.a(getContext()).a(j);
    localObject2 = e;
    ((ab)localObject1).a((ImageView)localObject2, null);
  }
  
  private void b(boolean paramBoolean)
  {
    boolean bool = s();
    if (bool)
    {
      a.setEnabled(paramBoolean);
      Button localButton = b;
      localButton.setEnabled(paramBoolean);
    }
  }
  
  private void c()
  {
    Object localObject1 = g.b();
    Object localObject2 = NotificationType.CONTACT_REQUEST;
    int j = 2131886764;
    String str = null;
    if (localObject1 == localObject2)
    {
      localObject1 = c;
      localObject2 = h;
      ((TextView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
      localObject1 = e;
      localObject2 = h;
      ((ImageView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
      localObject1 = g.e;
      k = ((List)localObject1).size();
      if (k > 0)
      {
        localObject2 = a;
        localObject3 = getString(j);
        View.OnClickListener localOnClickListener = h;
        a((Button)localObject2, (String)localObject3, localOnClickListener);
        a(b, null, null);
        localObject2 = g.e;
        k += -1;
        localObject1 = getb;
        localObject2 = Notification.NotificationActionHistoryItem.Action.ACCEPTED;
        if (localObject1 == localObject2)
        {
          localObject1 = d;
          localObject2 = getString(2131886762);
          ((TextView)localObject1).setText((CharSequence)localObject2);
          return;
        }
        localObject2 = Notification.NotificationActionHistoryItem.Action.DENIED;
        if (localObject1 == localObject2)
        {
          localObject1 = d;
          int m = 2131886758;
          localObject2 = getString(m);
          ((TextView)localObject1).setText((CharSequence)localObject2);
        }
        return;
      }
      localObject1 = a;
      localObject2 = getString(2131886321);
      Object localObject3 = i;
      a((Button)localObject1, (String)localObject2, (View.OnClickListener)localObject3);
      localObject1 = b;
      localObject2 = getString(2131886323);
      localObject3 = i;
      a((Button)localObject1, (String)localObject2, (View.OnClickListener)localObject3);
      return;
    }
    localObject2 = t.3.a;
    int k = ((NotificationType)localObject1).ordinal();
    k = localObject2[k];
    switch (k)
    {
    default: 
      break;
    case 6: 
    case 7: 
    case 8: 
      str = getString(j);
      break;
    case 5: 
      k = 2131886760;
      str = getString(k);
      break;
    case 2: 
    case 3: 
    case 4: 
      k = 2131886759;
      str = getString(k);
      break;
    case 1: 
      k = 2131886763;
      str = getString(k);
    }
    localObject1 = a;
    localObject2 = h;
    a((Button)localObject1, str, (View.OnClickListener)localObject2);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(2131559213, paramViewGroup, false);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = getActivity().getIntent();
    Object localObject = "arg_notification";
    paramBundle = q.a(paramBundle.getStringExtra((String)localObject)).i();
    try
    {
      localObject = new com/truecaller/old/data/entity/Notification;
      ((Notification)localObject).<init>(paramBundle);
      g = ((Notification)localObject);
      paramBundle = (Button)paramView.findViewById(2131363058);
      a = paramBundle;
      paramBundle = (Button)paramView.findViewById(2131363057);
      b = paramBundle;
      paramBundle = (TextView)paramView.findViewById(2131363657);
      c = paramBundle;
      paramBundle = (TextView)paramView.findViewById(2131363653);
      d = paramBundle;
      paramBundle = (ImageView)paramView.findViewById(2131363654);
      e = paramBundle;
      int j = 2131363656;
      paramView = (TextView)paramView.findViewById(j);
      f = paramView;
      paramView = g;
      paramBundle = "f";
      paramView = paramView.a(paramBundle);
      boolean bool = am.a(paramView);
      if (bool)
      {
        paramView = k();
        paramBundle = g;
        localObject = "f";
        paramBundle = paramBundle.a((String)localObject);
        paramView.setTitle(paramBundle);
      }
      else
      {
        paramView = k();
        j = 2131887270;
        paramView.setTitle(j);
      }
      b();
      c();
      return;
    }
    catch (Exception localException)
    {
      getActivity().finish();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */