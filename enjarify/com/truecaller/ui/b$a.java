package com.truecaller.ui;

import android.support.v7.widget.RecyclerView.AdapterDataObserver;

final class b$a
  extends RecyclerView.AdapterDataObserver
{
  private b$a(b paramb) {}
  
  private int a(int paramInt1, int paramInt2)
  {
    int i = a.c(paramInt1);
    b localb = a;
    paramInt1 += paramInt2;
    return localb.c(paramInt1) - i;
  }
  
  public final void onChanged()
  {
    a.notifyDataSetChanged();
  }
  
  public final void onItemRangeChanged(int paramInt1, int paramInt2)
  {
    b localb = a;
    int i = localb.c(paramInt1);
    paramInt1 = a(paramInt1, paramInt2);
    localb.notifyItemRangeChanged(i, paramInt1);
  }
  
  public final void onItemRangeChanged(int paramInt1, int paramInt2, Object paramObject)
  {
    b localb = a;
    int i = localb.c(paramInt1);
    paramInt1 = a(paramInt1, paramInt2);
    localb.notifyItemRangeChanged(i, paramInt1, paramObject);
  }
  
  public final void onItemRangeInserted(int paramInt1, int paramInt2)
  {
    b localb = a;
    int i = localb.c(paramInt1);
    paramInt1 = a(paramInt1, paramInt2);
    localb.notifyItemRangeInserted(i, paramInt1);
  }
  
  public final void onItemRangeMoved(int paramInt1, int paramInt2, int paramInt3)
  {
    int i = 1;
    if (paramInt3 != i)
    {
      a.notifyDataSetChanged();
      return;
    }
    b localb = a;
    paramInt1 = localb.c(paramInt1);
    paramInt2 = a.c(paramInt2);
    localb.notifyItemMoved(paramInt1, paramInt2);
  }
  
  public final void onItemRangeRemoved(int paramInt1, int paramInt2)
  {
    b localb = a;
    int i = localb.c(paramInt1);
    paramInt1 = a(paramInt1, paramInt2);
    localb.notifyItemRangeRemoved(i, paramInt1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */