package com.truecaller.ui.components;

import android.widget.TextView;
import com.truecaller.common.h.j;
import java.lang.ref.WeakReference;

public final class AfterCallHeaderView$a
  implements Runnable
{
  private final WeakReference a;
  private final WeakReference b;
  private String c;
  private long d;
  
  private AfterCallHeaderView$a(TextView paramTextView, AfterCallBusinessView paramAfterCallBusinessView)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramTextView);
    a = localWeakReference;
    paramTextView = new java/lang/ref/WeakReference;
    paramTextView.<init>(paramAfterCallBusinessView);
    b = paramTextView;
  }
  
  public final void a(String paramString, long paramLong)
  {
    c = paramString;
    d = paramLong;
    AfterCallBusinessView localAfterCallBusinessView = (AfterCallBusinessView)b.get();
    if (localAfterCallBusinessView != null)
    {
      localAfterCallBusinessView.setLegendFormatString(paramString);
      paramString = Long.valueOf(paramLong);
      localAfterCallBusinessView.setEventTimestamp(paramString);
    }
  }
  
  public final void run()
  {
    Object localObject1 = (TextView)a.get();
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = c;
      int i = 1;
      Object[] arrayOfObject = new Object[i];
      long l1 = d;
      String str = j.a(l1);
      arrayOfObject[0] = str;
      localObject2 = String.format((String)localObject2, arrayOfObject);
      ((TextView)localObject1).setText((CharSequence)localObject2);
      long l2 = 60000L;
      ((TextView)localObject1).postDelayed(this, l2);
    }
    localObject1 = (AfterCallBusinessView)b.get();
    if (localObject1 != null)
    {
      localObject2 = ((AfterCallBusinessView)localObject1).getListener();
      if (localObject2 != null) {
        ((AfterCallBusinessView)localObject1).b();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.AfterCallHeaderView.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */