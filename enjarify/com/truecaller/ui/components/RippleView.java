package com.truecaller.ui.components;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;

public class RippleView
  extends View
{
  public int a;
  public int b;
  private final ArrayList c;
  private int d;
  private boolean e;
  private Paint f;
  private final Interpolator g;
  private AnimatorSet h;
  
  public RippleView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramContext = new java/util/ArrayList;
    paramContext.<init>();
    c = paramContext;
    e = false;
    paramContext = new android/view/animation/AccelerateDecelerateInterpolator;
    paramContext.<init>();
    g = paramContext;
    paramContext = new android/graphics/Paint;
    paramContext.<init>();
    f = paramContext;
    f.setColor(-1);
    paramContext = f;
    paramAttributeSet = Paint.Style.FILL;
    paramContext.setStyle(paramAttributeSet);
    paramContext = f;
    boolean bool = true;
    paramContext.setAntiAlias(bool);
    paramContext = new android/animation/AnimatorSet;
    paramContext.<init>();
    h = paramContext;
    int i = getVisibility();
    if (i == 0) {
      a();
    }
  }
  
  private void a()
  {
    boolean bool1 = e;
    if (bool1) {
      return;
    }
    ArrayList localArrayList = c;
    Object localObject = new com/truecaller/ui/components/RippleView$Ripple;
    ObjectAnimator localObjectAnimator = null;
    ((RippleView.Ripple)localObject).<init>(this, 0, 0.5F);
    localArrayList.add(localObject);
    localArrayList = c;
    localObject = new com/truecaller/ui/components/RippleView$Ripple;
    int i = 300;
    float f1 = 0.4F;
    ((RippleView.Ripple)localObject).<init>(this, i, f1);
    localArrayList.add(localObject);
    localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    localObject = c.iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject).hasNext();
      if (!bool2) {
        break;
      }
      localObjectAnimator = nextc;
      long l = 2000L;
      localObjectAnimator.setDuration(l);
      localArrayList.add(localObjectAnimator);
    }
    h.playTogether(localArrayList);
    h.start();
    e = true;
  }
  
  private void b()
  {
    boolean bool = e;
    if (bool)
    {
      h.end();
      c.clear();
      bool = false;
      e = false;
    }
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    a();
  }
  
  protected void onDetachedFromWindow()
  {
    b();
    super.onDetachedFromWindow();
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    Iterator localIterator = c.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      RippleView.Ripple localRipple = (RippleView.Ripple)localIterator.next();
      Paint localPaint1 = f;
      int i = b;
      localPaint1.setAlpha(i);
      int j = a;
      float f1 = j;
      i = b;
      float f2 = i;
      float f3 = a;
      Paint localPaint2 = f;
      paramCanvas.drawCircle(f1, f2, f3, localPaint2);
    }
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    double d1 = Math.max(paramInt1, paramInt2);
    Double.isNaN(d1);
    paramInt1 = (int)(d1 * 1.4D);
    d = paramInt1;
  }
  
  protected void onVisibilityChanged(View paramView, int paramInt)
  {
    super.onVisibilityChanged(paramView, paramInt);
    if (paramInt == 0)
    {
      a();
      return;
    }
    b();
  }
  
  public void setVisibility(int paramInt)
  {
    int i = getVisibility();
    if (i != paramInt)
    {
      super.setVisibility(paramInt);
      if (paramInt == 0)
      {
        a();
        return;
      }
      b();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.RippleView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */