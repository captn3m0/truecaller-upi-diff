package com.truecaller.ui.components;

import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

final class f$a
{
  public final TextView a;
  public final TextView b;
  public final ImageView c;
  public final RadioButton d;
  final View e;
  
  f$a(View paramView)
  {
    e = paramView;
    Object localObject = (TextView)paramView.findViewById(2131363657);
    a = ((TextView)localObject);
    localObject = (TextView)paramView.findViewById(2131363653);
    b = ((TextView)localObject);
    localObject = (ImageView)paramView.findViewById(2131363654);
    c = ((ImageView)localObject);
    localObject = (RadioButton)paramView.findViewById(2131363655);
    d = ((RadioButton)localObject);
    paramView.setTag(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.f.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */