package com.truecaller.ui.components;

import android.content.Context;

public final class FeedbackItemView$FeedbackItem
  extends n
{
  public FeedbackItemView.FeedbackItem.FeedbackItemState a;
  public FeedbackItemView.DisplaySource b;
  
  public FeedbackItemView$FeedbackItem(FeedbackItemView.DisplaySource paramDisplaySource, FeedbackItemView.FeedbackItem.FeedbackItemState paramFeedbackItemState)
  {
    Object localObject = FeedbackItemView.FeedbackItem.FeedbackItemState.QUESTION_ENJOYING;
    a = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject);
    localObject = FeedbackItemView.DisplaySource.OTHER;
    b = ((FeedbackItemView.DisplaySource)localObject);
    b = paramDisplaySource;
    a = paramFeedbackItemState;
  }
  
  public final void a()
  {
    Object localObject = FeedbackItemView.5.a;
    FeedbackItemView.FeedbackItem.FeedbackItemState localFeedbackItemState = a;
    int i = localFeedbackItemState.ordinal();
    int j = localObject[i];
    switch (j)
    {
    default: 
      break;
    case 10: 
      localObject = FeedbackItemView.FeedbackItem.FeedbackItemState.INVITE_YES;
      a = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject);
      break;
    case 7: 
    case 8: 
    case 9: 
      localObject = FeedbackItemView.FeedbackItem.FeedbackItemState.SHARE_YES;
      a = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject);
      return;
    case 6: 
      localObject = FeedbackItemView.FeedbackItem.FeedbackItemState.RATE_YES_THANKS;
      a = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject);
      return;
    case 4: 
      localObject = FeedbackItemView.FeedbackItem.FeedbackItemState.FEEDBACK_YES;
      a = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject);
      return;
    case 2: 
    case 3: 
    case 5: 
      localObject = FeedbackItemView.FeedbackItem.FeedbackItemState.RATE_YES;
      a = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject);
      return;
    case 1: 
      localObject = FeedbackItemView.FeedbackItem.FeedbackItemState.QUESTION_RATE;
      a = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject);
      return;
    }
  }
  
  public final FeedbackItemView.DisplaySource b()
  {
    return b;
  }
  
  public final FeedbackItemView.FeedbackItem.FeedbackItemState d()
  {
    return a;
  }
  
  public final String d(Context paramContext)
  {
    FeedbackItemView.FeedbackItem.FeedbackItemState localFeedbackItemState = a;
    int i = localFeedbackItemState.getMessageId();
    int j = -1;
    if (i == j) {
      return "";
    }
    return paramContext.getString(i);
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool = paramObject instanceof FeedbackItem;
    if (bool)
    {
      FeedbackItemView.FeedbackItem.FeedbackItemState localFeedbackItemState = a;
      paramObject = a;
      if (localFeedbackItemState == paramObject) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.FeedbackItemView.FeedbackItem
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */