package com.truecaller.ui.components;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.content.b.f;
import android.support.v4.widget.m;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.truecaller.R.styleable;
import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.Availability.Status;
import com.truecaller.common.tag.TagView;
import com.truecaller.common.tag.c;
import com.truecaller.ui.c.a;
import com.truecaller.util.at;

public class CallerButtonBase
  extends RelativeLayout
{
  private int a = 0;
  private final TagView b;
  
  public CallerButtonBase(Context paramContext)
  {
    this(paramContext, null, (byte)0);
  }
  
  public CallerButtonBase(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private CallerButtonBase(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet, 0);
    Context localContext = getContext();
    int i = getLayout();
    inflate(localContext, i, this);
    boolean bool = true;
    setClickable(bool);
    Object localObject = (TagView)findViewById(2131364650);
    b = ((TagView)localObject);
    localObject = R.styleable.CallerButtonBase;
    paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject);
    i = 0;
    localObject = null;
    int k = 0;
    Drawable localDrawable = null;
    for (;;)
    {
      int m = paramAttributeSet.getIndexCount();
      if (i >= m) {
        break;
      }
      m = paramAttributeSet.getIndex(i);
      int i4;
      float f;
      String str;
      int n;
      int i1;
      int i2;
      int i3;
      switch (m)
      {
      default: 
        break;
      case 13: 
        i4 = 2131952076;
        f = 1.9540585E38F;
        m = paramAttributeSet.getResourceId(m, i4);
        setHeadingTextStyle(m);
        break;
      case 12: 
        str = paramAttributeSet.getString(m);
        setHeadingText(str);
        break;
      case 11: 
        str = paramAttributeSet.getString(m);
        setFooterText(str);
        break;
      case 10: 
        i4 = 2131952074;
        f = 1.954058E38F;
        m = paramAttributeSet.getResourceId(m, i4);
        setDetailsTextStyle(m);
        break;
      case 9: 
        str = paramAttributeSet.getString(m);
        setDetailsText(str);
        break;
      case 8: 
        n = paramAttributeSet.getBoolean(m, false);
        setSingleLine(n);
        break;
      case 7: 
        i1 = paramAttributeSet.getBoolean(n, bool);
        setShowPartialDivider(i1);
        break;
      case 6: 
        i2 = paramAttributeSet.getBoolean(i1, bool);
        setShowFullDivider(i2);
        break;
      case 5: 
        i3 = paramAttributeSet.getResourceId(i2, 0);
        setRightImageSecondary(i3);
        break;
      case 4: 
        i3 = paramAttributeSet.getResourceId(i3, 0);
        setRightImage(i3);
        break;
      case 3: 
        i3 = paramAttributeSet.getResourceId(i3, 0);
        setLeftImage(i3);
        break;
      case 2: 
        Resources localResources = getResources();
        int i5 = 2131165378;
        f = localResources.getDimension(i5);
        i4 = (int)f;
        i3 = paramAttributeSet.getDimensionPixelSize(i3, i4);
        setButtonHeight(i3);
        break;
      case 1: 
        i3 = paramAttributeSet.getColor(i3, 0);
        a = i3;
        i3 = a;
        setImageTint(i3);
        break;
      case 0: 
        localDrawable = paramAttributeSet.getDrawable(i3);
        at.a(this, localDrawable);
        k = 1;
      }
      i += 1;
    }
    paramAttributeSet.recycle();
    if (k == 0)
    {
      paramAttributeSet = new int[bool];
      int j = 2130969388;
      paramAttributeSet[0] = j;
      paramContext = paramContext.obtainStyledAttributes(paramAttributeSet);
      int i6 = paramContext.getResourceId(0, 0);
      setBackgroundResource(i6);
      paramContext.recycle();
    }
    paramContext = new com/truecaller/ui/components/-$$Lambda$CallerButtonBase$CcEu4QWbCv1A5QUavSh8YfxyFno;
    paramContext.<init>(this);
    getRightImage().setOnClickListener(paramContext);
    getRightImageSecondary().setOnClickListener(paramContext);
  }
  
  private void a(int paramInt, Drawable paramDrawable)
  {
    if (paramDrawable != null)
    {
      paramDrawable = android.support.v4.graphics.drawable.a.e(paramDrawable);
      int i = a;
      if (i != 0) {
        android.support.v4.graphics.drawable.a.a(paramDrawable, i);
      }
    }
    at.a(this, paramInt, paramDrawable);
  }
  
  private static void a(ImageView paramImageView, int paramInt)
  {
    if (paramImageView != null)
    {
      paramImageView = paramImageView.getDrawable();
      if (paramImageView != null)
      {
        android.support.v4.graphics.drawable.a.a(paramImageView, paramInt);
        return;
      }
    }
  }
  
  public final void a(Context paramContext, com.truecaller.presence.a parama)
  {
    TextView localTextView = getHeadingTextView();
    if (parama != null)
    {
      Object localObject = b;
      if (localObject != null)
      {
        boolean bool1 = parama.a();
        if (bool1)
        {
          parama = new com/truecaller/ui/c$a;
          parama.<init>(paramContext);
          boolean bool2 = false;
          paramContext = null;
          b = false;
          int i = 6;
          d = i;
          e = 0;
          localObject = ((Availability)localObject).a();
          Availability.Status localStatus = Availability.Status.AVAILABLE;
          if (localObject == localStatus) {
            bool2 = true;
          }
          a = bool2;
          paramContext = parama.a();
          m.a(localTextView, paramContext, null);
          return;
        }
      }
    }
    localTextView.setCompoundDrawables(null, null, null, null);
  }
  
  public TextView getDetailsTextView()
  {
    return (TextView)findViewById(2131362299);
  }
  
  protected TextView getFooterTextView()
  {
    return (TextView)findViewById(2131362300);
  }
  
  protected TextView getHeadingTextView()
  {
    return (TextView)findViewById(2131362301);
  }
  
  protected int getLayout()
  {
    return 2131558521;
  }
  
  public ImageView getLeftImage()
  {
    return (ImageView)findViewById(2131362286);
  }
  
  public ImageView getRightImage()
  {
    return (ImageView)findViewById(2131362287);
  }
  
  public ImageView getRightImageSecondary()
  {
    return (ImageView)findViewById(2131362288);
  }
  
  public void setButtonHeight(int paramInt)
  {
    getLayoutParamsheight = paramInt;
    findViewById2131364706getLayoutParamsheight = paramInt;
  }
  
  public void setDetailsMaxLines(int paramInt)
  {
    getDetailsTextView().setMaxLines(paramInt);
  }
  
  public void setDetailsText(CharSequence paramCharSequence)
  {
    at.b(getDetailsTextView(), paramCharSequence);
  }
  
  public void setDetailsTextStyle(int paramInt)
  {
    TextView localTextView = getDetailsTextView();
    Context localContext = getContext();
    localTextView.setTextAppearance(localContext, paramInt);
  }
  
  public void setFooterText(CharSequence paramCharSequence)
  {
    at.b(getFooterTextView(), paramCharSequence);
  }
  
  public void setHeaderDrawablePadding(int paramInt)
  {
    getHeadingTextView().setCompoundDrawablePadding(paramInt);
  }
  
  public void setHeaderGravity(int paramInt)
  {
    getHeadingTextView().setGravity(paramInt);
  }
  
  public void setHeadingMaxLines(int paramInt)
  {
    getHeadingTextView().setMaxLines(paramInt);
  }
  
  public void setHeadingText(CharSequence paramCharSequence)
  {
    at.b(getHeadingTextView(), paramCharSequence);
  }
  
  public void setHeadingTextStyle(int paramInt)
  {
    TextView localTextView = getHeadingTextView();
    Context localContext = getContext();
    localTextView.setTextAppearance(localContext, paramInt);
  }
  
  public void setImageTint(int paramInt)
  {
    a = paramInt;
    a(getRightImage(), paramInt);
    a(getRightImageSecondary(), paramInt);
    a(getLeftImage(), paramInt);
  }
  
  public void setLeftImage(int paramInt)
  {
    Drawable localDrawable = f.a(getResources(), paramInt, null);
    a(2131362286, localDrawable);
  }
  
  public void setLeftImage(Drawable paramDrawable)
  {
    a(2131362286, paramDrawable);
  }
  
  public void setLeftImageWidth(int paramInt)
  {
    ViewGroup.LayoutParams localLayoutParams = getLeftImage().getLayoutParams();
    if (localLayoutParams != null)
    {
      width = paramInt;
      ImageView localImageView = getLeftImage();
      localImageView.setLayoutParams(localLayoutParams);
    }
  }
  
  public void setMaxLines(int paramInt)
  {
    getHeadingTextView().setMaxLines(paramInt);
  }
  
  public void setRightImage(int paramInt)
  {
    Object localObject = f.a(getResources(), paramInt, null);
    int i = 2131362287;
    a(i, (Drawable)localObject);
    localObject = findViewById(i);
    if (paramInt != 0) {
      paramInt = 0;
    } else {
      paramInt = 8;
    }
    ((View)localObject).setVisibility(paramInt);
  }
  
  public void setRightImageSecondary(int paramInt)
  {
    Object localObject = f.a(getResources(), paramInt, null);
    int i = 2131362288;
    a(i, (Drawable)localObject);
    localObject = findViewById(i);
    if (paramInt != 0) {
      paramInt = 0;
    } else {
      paramInt = 8;
    }
    ((View)localObject).setVisibility(paramInt);
  }
  
  public void setRightImageTint(int paramInt)
  {
    a(getRightImage(), paramInt);
  }
  
  public void setShowButtonDividers(boolean paramBoolean)
  {
    int i = 2131364177;
    LinearLayout localLinearLayout = (LinearLayout)findViewById(i);
    if (paramBoolean) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    }
    localLinearLayout.setShowDividers(paramBoolean);
  }
  
  public void setShowFullDivider(boolean paramBoolean)
  {
    int i = 2131362867;
    View localView = findViewById(i);
    if (paramBoolean)
    {
      getLayoutParamsleftMargin = 0;
      localView.setVisibility(0);
      return;
    }
    localView.setVisibility(8);
  }
  
  public void setShowPartialDivider(boolean paramBoolean)
  {
    int i = 2131362867;
    View localView = findViewById(i);
    if (paramBoolean)
    {
      ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)localView.getLayoutParams();
      int j = getResources().getDimensionPixelSize(2131165872);
      leftMargin = j;
      localView.setVisibility(0);
      return;
    }
    localView.setVisibility(8);
  }
  
  public void setSingleLine(boolean paramBoolean)
  {
    int i;
    if (paramBoolean)
    {
      i = 8;
    }
    else
    {
      i = 0;
      localTextView1 = null;
    }
    getDetailsTextView().setVisibility(i);
    TextView localTextView2 = getFooterTextView();
    localTextView2.setVisibility(i);
    TextView localTextView1 = getHeadingTextView();
    if (paramBoolean) {
      paramBoolean = true;
    } else {
      paramBoolean = true;
    }
    localTextView1.setMaxLines(paramBoolean);
  }
  
  public void setTag(c paramc)
  {
    TagView localTagView = b;
    boolean bool;
    if (paramc != null) {
      bool = true;
    } else {
      bool = false;
    }
    at.a(localTagView, bool);
    if (paramc != null)
    {
      localTagView = b;
      localTagView.setTag(paramc);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.CallerButtonBase
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */