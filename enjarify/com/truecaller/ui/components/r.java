package com.truecaller.ui.components;

import android.app.Activity;
import android.content.res.TypedArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout.LayoutParams;
import java.lang.reflect.Method;

public final class r
{
  private static String c;
  public boolean a;
  public View b;
  private final r.a d;
  private boolean e;
  private View f;
  
  static
  {
    Object localObject1 = "android.os.SystemProperties";
    try
    {
      localObject1 = Class.forName((String)localObject1);
      Object localObject2 = "get";
      boolean bool = true;
      Class[] arrayOfClass = new Class[bool];
      Class localClass = String.class;
      arrayOfClass[0] = localClass;
      localObject1 = ((Class)localObject1).getDeclaredMethod((String)localObject2, arrayOfClass);
      ((Method)localObject1).setAccessible(bool);
      localObject2 = new Object[bool];
      String str = "qemu.hw.mainkeys";
      localObject2[0] = str;
      localObject1 = ((Method)localObject1).invoke(null, (Object[])localObject2);
      localObject1 = (String)localObject1;
      return;
    }
    finally
    {
      c = null;
    }
  }
  
  public r(Activity paramActivity)
  {
    Object localObject1 = paramActivity.getWindow();
    ViewGroup localViewGroup = (ViewGroup)((Window)localObject1).getDecorView();
    int i = 2;
    Object localObject2 = new int[i];
    Object tmp28_26 = localObject2;
    tmp28_26[0] = 16843759;
    tmp28_26[1] = 16843760;
    localObject2 = paramActivity.obtainStyledAttributes((int[])localObject2);
    int k = 0;
    try
    {
      int m = ((TypedArray)localObject2).getBoolean(0, false);
      a = m;
      m = 1;
      boolean bool3 = ((TypedArray)localObject2).getBoolean(m, false);
      e = bool3;
      ((TypedArray)localObject2).recycle();
      localObject1 = ((Window)localObject1).getAttributes();
      i = flags;
      int i1 = 67108864;
      i &= i1;
      if (i != 0) {
        a = m;
      }
      int i3 = flags;
      i = 134217728;
      i3 &= i;
      if (i3 != 0) {
        e = m;
      }
      localObject1 = new com/truecaller/ui/components/r$a;
      boolean bool1 = a;
      boolean bool2 = e;
      ((r.a)localObject1).<init>(paramActivity, bool1, bool2, (byte)0);
      d = ((r.a)localObject1);
      localObject1 = d;
      boolean bool5 = b;
      if (!bool5) {
        e = false;
      }
      bool5 = a;
      int j = 8;
      k = -1728053248;
      int n = -1;
      if (bool5)
      {
        localObject1 = new android/view/View;
        ((View)localObject1).<init>(paramActivity);
        b = ((View)localObject1);
        localObject1 = new android/widget/FrameLayout$LayoutParams;
        Object localObject3 = d;
        i1 = a;
        ((FrameLayout.LayoutParams)localObject1).<init>(n, i1);
        gravity = 48;
        boolean bool4 = e;
        if (bool4)
        {
          localObject3 = d;
          bool4 = ((r.a)localObject3).a();
          if (!bool4)
          {
            localObject3 = d;
            int i2 = d;
            rightMargin = i2;
          }
        }
        localObject3 = b;
        ((View)localObject3).setLayoutParams((ViewGroup.LayoutParams)localObject1);
        b.setBackgroundColor(k);
        b.setVisibility(j);
        localObject1 = b;
        localViewGroup.addView((View)localObject1);
      }
      bool5 = e;
      if (bool5)
      {
        localObject1 = new android/view/View;
        ((View)localObject1).<init>(paramActivity);
        f = ((View)localObject1);
        paramActivity = d;
        boolean bool6 = paramActivity.a();
        int i4;
        if (bool6)
        {
          paramActivity = new android/widget/FrameLayout$LayoutParams;
          localObject1 = d;
          i4 = c;
          paramActivity.<init>(n, i4);
          i4 = 80;
          gravity = i4;
        }
        else
        {
          paramActivity = new android/widget/FrameLayout$LayoutParams;
          localObject1 = d;
          i4 = d;
          paramActivity.<init>(i4, n);
          i4 = 8388613;
          gravity = i4;
        }
        localObject1 = f;
        ((View)localObject1).setLayoutParams(paramActivity);
        f.setBackgroundColor(k);
        f.setVisibility(j);
        paramActivity = f;
        localViewGroup.addView(paramActivity);
      }
      return;
    }
    finally
    {
      ((TypedArray)localObject2).recycle();
    }
  }
  
  public final void a(float paramFloat)
  {
    boolean bool = a;
    if (bool)
    {
      View localView = b;
      localView.setAlpha(paramFloat);
    }
  }
  
  public final void a(int paramInt)
  {
    boolean bool = a;
    if (bool)
    {
      View localView = b;
      localView.setBackgroundColor(paramInt);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */