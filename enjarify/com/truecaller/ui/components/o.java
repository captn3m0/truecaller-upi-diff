package com.truecaller.ui.components;

import android.content.Context;
import com.d.b.w;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class o
  extends d
{
  public List c;
  private final w d;
  private final Context e;
  
  public o(Context paramContext)
  {
    e = paramContext;
    paramContext = w.a(paramContext);
    d = paramContext;
  }
  
  public final void a(Collection paramCollection)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(paramCollection);
    c = localArrayList;
    notifyDataSetChanged();
  }
  
  public final int getItemCount()
  {
    List localList = c;
    if (localList == null) {
      return 0;
    }
    return localList.size();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */