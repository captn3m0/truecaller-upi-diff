package com.truecaller.ui.components;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.widget.m;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.TouchDelegate;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.e;
import com.d.b.t;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.aa;
import com.truecaller.common.h.am;
import com.truecaller.common.h.aq.a;
import com.truecaller.common.h.aq.b;
import com.truecaller.common.h.aq.c;
import com.truecaller.common.h.j;
import com.truecaller.common.tag.TagView;
import com.truecaller.common.tag.c;
import com.truecaller.common.tag.d;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.Number;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.util.at;
import com.truecaller.util.ce;

public class AfterCallHeaderView
  extends k
{
  private int A;
  private int B;
  private boolean C;
  public long a = -1;
  public AfterCallHeaderView.a b;
  private View d;
  private TextView e;
  private TextView f;
  private TextView g;
  private TextView h;
  private TextView i;
  private TintedImageView j;
  private ImageView k;
  private ImageView l;
  private ViewGroup m;
  private ImageButton n;
  private TextView o;
  private TextView p;
  private TextView q;
  private AfterCallBusinessView r;
  private TextView s;
  private Uri t;
  private ai u;
  private ai v;
  private ai w;
  private boolean x;
  private int y;
  private boolean z;
  
  public AfterCallHeaderView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private AfterCallHeaderView(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet);
    paramContext = new com/truecaller/common/h/aq$b;
    paramContext.<init>();
    u = paramContext;
    float f1 = getResources().getDimensionPixelSize(2131166170) * 1.0F / 2.0F;
    paramAttributeSet = new com/truecaller/common/h/aq$c;
    paramAttributeSet.<init>(f1, f1, 0.0F, 0.0F);
    v = paramAttributeSet;
    paramContext = new com/truecaller/common/h/aq$a;
    int i1 = getResources().getColor(2131100502);
    paramContext.<init>(i1);
    w = paramContext;
  }
  
  private int a(int paramInt1, int paramInt2)
  {
    int i1 = 2131886103;
    int i2 = 2131886102;
    int i3 = 2131886104;
    int i4 = 2;
    int i5 = 1;
    if (paramInt2 == i5)
    {
      paramInt2 = 5;
      if (paramInt1 == paramInt2) {
        return i3;
      }
      if (paramInt1 == i4)
      {
        paramInt1 = y;
        if (paramInt1 == i5) {
          return i1;
        }
        return i2;
      }
      return 2131886098;
    }
    int i6 = 3;
    if (paramInt2 == i6) {
      return 2131886101;
    }
    if (paramInt1 == i5) {
      return 2131886099;
    }
    if (paramInt1 == i4)
    {
      paramInt1 = y;
      if (paramInt1 == i5) {
        return i1;
      }
      return i2;
    }
    if (paramInt1 == i6) {
      return 2131886100;
    }
    return i3;
  }
  
  private void b(Uri paramUri, boolean paramBoolean)
  {
    t = paramUri;
    boolean bool = x;
    Object localObject1;
    if ((!bool) && (!paramBoolean))
    {
      paramBoolean = false;
      localObject1 = null;
    }
    else
    {
      paramBoolean = true;
    }
    x = paramBoolean;
    paramBoolean = getWidth();
    if (paramBoolean)
    {
      paramBoolean = getHeight();
      if (paramBoolean)
      {
        paramBoolean = C;
        if (paramBoolean) {
          return;
        }
        paramBoolean = z;
        if (paramBoolean)
        {
          b();
          return;
        }
        paramUri = com.d.b.w.a(getContext()).a(paramUri).a();
        paramBoolean = getWidth() / 2;
        int i1 = getHeight() / 2;
        paramUri = paramUri.b(paramBoolean, i1).b();
        localObject1 = u;
        paramUri = paramUri.a((ai)localObject1);
        localObject1 = w;
        paramUri = paramUri.a((ai)localObject1);
        localObject1 = v;
        paramUri = paramUri.a((ai)localObject1);
        paramBoolean = x;
        if (!paramBoolean)
        {
          paramBoolean = Settings.d();
          if (!paramBoolean)
          {
            localObject1 = t.c;
            localObject2 = new t[0];
            paramUri.a((t)localObject1);
          }
        }
        localObject1 = l;
        Object localObject2 = new com/truecaller/ui/components/AfterCallHeaderView$1;
        ((AfterCallHeaderView.1)localObject2).<init>(this);
        paramUri.a((ImageView)localObject1, (e)localObject2);
        return;
      }
    }
  }
  
  private void b(boolean paramBoolean)
  {
    boolean bool = z;
    if (bool) {
      return;
    }
    C = paramBoolean;
    Object localObject1 = getContext();
    int i1 = android.support.v4.content.b.c((Context)localObject1, 2131100374);
    Context localContext1 = getContext();
    int i2 = android.support.v4.content.b.c(localContext1, 2131100638);
    Object localObject2 = getContext();
    int i3 = android.support.v4.content.b.c((Context)localObject2, 2131100383);
    Context localContext2 = getContext();
    int i4 = android.support.v4.content.b.c(localContext2, 2131100384);
    Context localContext3 = getContext();
    int i5 = com.truecaller.utils.ui.b.a(localContext3, 2130969588);
    Context localContext4 = getContext();
    int i6 = 2131099996;
    float f1 = 1.781236E38F;
    int i7 = android.support.v4.content.b.c(localContext4, i6);
    if (paramBoolean)
    {
      f();
      localObject3 = l;
      i8 = 0;
      f2 = 0.0F;
      localContext5 = null;
      ((ImageView)localObject3).setBackground(null);
    }
    else
    {
      localObject3 = l;
      i8 = 2131230850;
      f2 = 1.8077764E38F;
      ((ImageView)localObject3).setBackgroundResource(i8);
    }
    i6 = 2131362343;
    f1 = 1.8344464E38F;
    Object localObject3 = (CardView)findViewById(i6);
    if (paramBoolean) {
      i8 = i2;
    } else {
      i8 = i5;
    }
    ((CardView)localObject3).setCardBackgroundColor(i8);
    localObject3 = p;
    if (paramBoolean)
    {
      i8 = i3;
    }
    else
    {
      localContext5 = getContext();
      i9 = 2130968626;
      f3 = 1.754591E38F;
      i8 = com.truecaller.utils.ui.b.a(localContext5, i9);
    }
    ((TextView)localObject3).setTextColor(i8);
    localObject3 = n;
    if (paramBoolean) {
      i8 = i1;
    } else {
      i8 = i2;
    }
    com.truecaller.utils.ui.b.a((ImageView)localObject3, i8);
    localObject3 = findViewById(2131362882);
    int i8 = 8;
    float f2 = 1.1E-44F;
    ((View)localObject3).setVisibility(i8);
    i6 = 2131362867;
    f1 = 1.8345527E38F;
    localObject3 = findViewById(i6);
    if (paramBoolean)
    {
      localContext5 = getContext();
      i9 = 2131100373;
      f3 = 1.7813126E38F;
      i8 = android.support.v4.content.b.c(localContext5, i9);
    }
    else
    {
      i8 = i7;
    }
    ((View)localObject3).setBackgroundColor(i8);
    localObject3 = getContext();
    f2 = 1.0F;
    i6 = at.a((Context)localObject3, f2);
    Context localContext5 = getContext();
    int i9 = 2131100381;
    float f3 = 1.7813142E38F;
    i8 = android.support.v4.content.b.c(localContext5, i9);
    Object localObject4 = f;
    if (!paramBoolean) {
      i3 = i5;
    }
    ((TextView)localObject4).setTextColor(i3);
    localObject2 = f;
    if (paramBoolean) {
      localObject4 = "sans-serif-medium";
    } else {
      localObject4 = "sans-serif";
    }
    int i10 = 0;
    localObject4 = Typeface.create((String)localObject4, 0);
    ((TextView)localObject2).setTypeface((Typeface)localObject4);
    localObject2 = f;
    f3 = paramBoolean;
    f1 = i6;
    ((TextView)localObject2).setShadowLayer(f3, 0.0F, f1, i8);
    localObject2 = g;
    int i11;
    if (paramBoolean) {
      i11 = i4;
    } else {
      i11 = i5;
    }
    ((TextView)localObject2).setTextColor(i11);
    g.setShadowLayer(f3, 0.0F, f1, i8);
    localObject2 = s;
    if (paramBoolean) {
      i11 = i4;
    } else {
      i11 = i5;
    }
    ((TextView)localObject2).setTextColor(i11);
    s.setShadowLayer(f3, 0.0F, f1, i8);
    localObject2 = o;
    if (paramBoolean) {
      i11 = i4;
    } else {
      i11 = i5;
    }
    ((TextView)localObject2).setTextColor(i11);
    o.setShadowLayer(f3, 0.0F, f1, i8);
    localObject2 = e;
    if (paramBoolean) {
      i11 = i4;
    } else {
      i11 = i7;
    }
    ((TextView)localObject2).setTextColor(i11);
    e.setShadowLayer(f3, 0.0F, f1, i8);
    localObject2 = j;
    if (!paramBoolean) {
      i1 = i7;
    }
    ((TintedImageView)localObject2).setTint(i1);
    localObject1 = h;
    if (!paramBoolean) {
      i4 = i5;
    }
    ((TextView)localObject1).setTextColor(i4);
    h.setShadowLayer(f3, 0.0F, f1, i8);
    localObject1 = getContext();
    i3 = 2131100382;
    i1 = android.support.v4.content.b.c((Context)localObject1, i3);
    for (;;)
    {
      localObject2 = m;
      i3 = ((ViewGroup)localObject2).getChildCount();
      if (i10 >= i3) {
        break;
      }
      localObject2 = (TagView)m.getChildAt(i10);
      if (paramBoolean) {
        i4 = i1;
      } else {
        i4 = i2;
      }
      ((TagView)localObject2).setTint(i4);
      i10 += 1;
    }
    i1 = 2131362507;
    localObject1 = (TintedImageView)findViewById(i1);
    localObject2 = r;
    i3 = ((AfterCallBusinessView)localObject2).getVisibility();
    i4 = 2130968623;
    Context localContext6;
    if (i3 == 0)
    {
      localObject2 = r;
      if (paramBoolean)
      {
        localContext3 = getContext();
        i7 = 2131100372;
        i5 = android.support.v4.content.b.c(localContext3, i7);
      }
      else
      {
        localContext3 = getContext();
        i7 = 2130968622;
        i5 = com.truecaller.utils.ui.b.a(localContext3, i7);
      }
      ((AfterCallBusinessView)localObject2).c(i5);
      if (!paramBoolean)
      {
        localContext6 = getContext();
        i2 = com.truecaller.utils.ui.b.a(localContext6, i4);
      }
      ((TintedImageView)localObject1).setTint(i2);
      return;
    }
    if (paramBoolean)
    {
      localContext6 = getContext();
      i2 = 2131100380;
      paramBoolean = android.support.v4.content.b.c(localContext6, i2);
    }
    else
    {
      localContext6 = getContext();
      paramBoolean = com.truecaller.utils.ui.b.a(localContext6, i4);
    }
    ((TintedImageView)localObject1).setTint(paramBoolean);
  }
  
  private void f()
  {
    ImageView localImageView1 = null;
    t = null;
    Object localObject = com.d.b.w.a(getContext());
    ImageView localImageView2 = l;
    ((com.d.b.w)localObject).d(localImageView2);
    boolean bool = z;
    if (bool)
    {
      localImageView1 = l;
      localObject = new android/graphics/drawable/ColorDrawable;
      int i1 = B;
      ((ColorDrawable)localObject).<init>(i1);
      localImageView1.setImageDrawable((Drawable)localObject);
      return;
    }
    l.setImageDrawable(null);
  }
  
  private void setUpSearchPartnerLogo(Contact paramContact)
  {
    paramContact = paramContact.B();
    boolean bool = TextUtils.isEmpty(paramContact);
    if (bool)
    {
      k.setVisibility(8);
      return;
    }
    k.setVisibility(0);
    paramContact = com.d.b.w.a(getContext()).a(paramContact);
    ImageView localImageView = k;
    paramContact.a(localImageView, null);
  }
  
  protected final void a()
  {
    b(false);
    Object localObject = l;
    int i1 = 2130969585;
    com.truecaller.utils.ui.b.a((View)localObject, i1);
    boolean bool = z;
    Context localContext;
    if (!bool)
    {
      localObject = p;
      localContext = getContext();
      i1 = com.truecaller.utils.ui.b.a(localContext, i1);
      ((TextView)localObject).setTextColor(i1);
    }
    else
    {
      localObject = c;
      localContext = getContext();
      i1 = com.truecaller.utils.ui.b.a(localContext, i1);
      ((AvatarView)localObject).setTintColor(i1);
    }
    f();
  }
  
  public final void a(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    boolean bool = C;
    if (bool) {
      return;
    }
    z = true;
    A = paramInt2;
    B = paramInt1;
    f();
    int i1 = 2131362343;
    ((CardView)findViewById(i1)).setCardBackgroundColor(paramInt2);
    TextView localTextView = p;
    localTextView.setTextColor(paramInt1);
    com.truecaller.utils.ui.b.a(n, paramInt2);
    Object localObject1 = r;
    paramInt1 = ((AfterCallBusinessView)localObject1).getVisibility();
    if (paramInt1 == 0)
    {
      localObject1 = r;
      ((AfterCallBusinessView)localObject1).c(paramInt2);
    }
    else
    {
      paramInt1 = 2131362507;
      localObject1 = (TintedImageView)findViewById(paramInt1);
      ((TintedImageView)localObject1).setTint(paramInt2);
    }
    localObject1 = getContext();
    i1 = 2130969585;
    paramInt1 = com.truecaller.utils.ui.b.a((Context)localObject1, i1);
    if (paramBoolean)
    {
      localObject2 = c;
      ((AvatarView)localObject2).setTintColor(paramInt1);
    }
    else
    {
      localObject2 = c;
      ((AvatarView)localObject2).setTintColor(paramInt2);
    }
    Object localObject2 = f;
    i1 = -16777216;
    ((TextView)localObject2).setTextColor(i1);
    g.setTextColor(i1);
    o.setTextColor(i1);
    e.setTextColor(i1);
    j.setTint(i1);
    h.setTextColor(i1);
    i.setTextColor(paramInt1);
    paramInt1 = 0xFFFFFF & paramInt2 | 0x33000000;
    findViewById(2131362867).setBackgroundColor(paramInt1);
    paramBoolean = 2131362882;
    localObject2 = findViewById(paramBoolean);
    ((View)localObject2).setBackgroundColor(paramInt1);
    paramInt1 = 0;
    localObject1 = null;
    ((View)localObject2).setVisibility(0);
    for (;;)
    {
      localObject2 = m;
      paramBoolean = ((ViewGroup)localObject2).getChildCount();
      if (paramInt1 >= paramBoolean) {
        break;
      }
      localObject2 = (TagView)m.getChildAt(paramInt1);
      ((TagView)localObject2).setTint(paramInt2);
      paramInt1 += 1;
    }
  }
  
  protected final void a(Context paramContext)
  {
    paramContext = LayoutInflater.from(paramContext);
    int i1 = 2131559148;
    paramContext.inflate(i1, this);
    paramContext = findViewById(2131362594);
    d = paramContext;
    paramContext = (TextView)findViewById(2131363786);
    f = paramContext;
    paramContext = (TextView)findViewById(2131362019);
    g = paramContext;
    paramContext = (TextView)findViewById(2131363634);
    e = paramContext;
    paramContext = (TintedImageView)findViewById(2131364520);
    j = paramContext;
    paramContext = (TextView)findViewById(2131361989);
    h = paramContext;
    paramContext = (TextView)findViewById(2131364558);
    i = paramContext;
    paramContext = (ImageView)findViewById(2131362080);
    l = paramContext;
    paramContext = (ViewGroup)findViewById(2131364653);
    m = paramContext;
    paramContext = (ImageView)findViewById(2131364265);
    k = paramContext;
    paramContext = (ImageButton)findViewById(2131364613);
    n = paramContext;
    paramContext = (TextView)findViewById(2131363824);
    o = paramContext;
    paramContext = (TextView)findViewById(2131362344);
    p = paramContext;
    paramContext = (TextView)findViewById(2131363982);
    q = paramContext;
    paramContext = (AfterCallBusinessView)findViewById(2131362275);
    r = paramContext;
    paramContext = (TextView)findViewById(2131362271);
    s = paramContext;
    paramContext = new com/truecaller/ui/components/AfterCallHeaderView$a;
    Object localObject = e;
    AfterCallBusinessView localAfterCallBusinessView = r;
    paramContext.<init>((TextView)localObject, localAfterCallBusinessView, (byte)0);
    b = paramContext;
    boolean bool = isInEditMode();
    if (bool)
    {
      e.setText("Missed call from (yesterday)");
      h.setText("200 Eastern Pkwy Brooklyn, NY 11288 United States");
      paramContext = i;
      localObject = "Over 9000 people marked this as spam";
      paramContext.setText((CharSequence)localObject);
      paramContext = k;
      i1 = 2131689472;
      paramContext.setImageResource(i1);
    }
  }
  
  protected final void a(Uri paramUri, boolean paramBoolean)
  {
    b(paramUri, paramBoolean);
    boolean bool = z;
    if (!bool)
    {
      paramUri = p;
      Context localContext = getContext();
      int i1 = 2130968626;
      paramBoolean = com.truecaller.utils.ui.b.a(localContext, i1);
      paramUri.setTextColor(paramBoolean);
    }
  }
  
  public final void a(Contact paramContact, HistoryEvent paramHistoryEvent, boolean paramBoolean1, boolean paramBoolean2, int paramInt, boolean paramBoolean3)
  {
    y = paramInt;
    b(paramContact, paramBoolean1, paramBoolean2, paramBoolean3);
    paramBoolean1 = true;
    String str1;
    if (paramHistoryEvent != null)
    {
      str1 = c;
      Object localObject1 = b;
      str1 = (String)am.e(str1, (CharSequence)localObject1);
      localObject1 = getResources();
      paramBoolean3 = o;
      int i1 = p;
      paramBoolean3 = a(paramBoolean3, i1);
      localObject1 = ((Resources)localObject1).getString(paramBoolean3);
      long l1 = h;
      a = l1;
      Object localObject2 = b;
      l1 = a;
      ((AfterCallHeaderView.a)localObject2).a((String)localObject1, l1);
      localObject2 = e;
      i1 = 1;
      Object[] arrayOfObject = new Object[i1];
      long l2 = h;
      String str2 = j.a(l2);
      arrayOfObject[0] = str2;
      localObject1 = String.format((String)localObject1, arrayOfObject);
      ((TextView)localObject2).setText((CharSequence)localObject1);
      localObject1 = ((bk)getContext().getApplicationContext()).a().U();
      paramBoolean3 = ((h)localObject1).j();
      if (paramBoolean3)
      {
        localObject2 = j;
        localObject1 = ((h)localObject1).b((String)localObject2);
        if (localObject1 != null)
        {
          paramBoolean3 = a;
          if (!paramBoolean3)
          {
            localObject3 = j;
            paramInt = 2131234549;
            ((TintedImageView)localObject3).setImageResource(paramInt);
            localObject3 = j;
            ((TintedImageView)localObject3).setVisibility(0);
            break label345;
          }
          paramInt = a;
          if (paramInt == i1)
          {
            localObject3 = j;
            paramInt = 2131234550;
            ((TintedImageView)localObject3).setImageResource(paramInt);
            localObject3 = j;
            ((TintedImageView)localObject3).setVisibility(0);
            break label345;
          }
          localObject1 = j;
          ((TintedImageView)localObject1).setVisibility(paramBoolean1);
          break label345;
        }
      }
      localObject1 = j;
      ((TintedImageView)localObject1).setVisibility(paramBoolean1);
      label345:
      Object localObject3 = android.support.v4.e.a.a();
      localObject1 = c;
      paramHistoryEvent = b;
      paramHistoryEvent = (String)am.e((CharSequence)localObject1, paramHistoryEvent);
      localObject1 = com.truecaller.common.h.k.f(getContext());
      paramHistoryEvent = aa.d(paramHistoryEvent, (String)localObject1);
      paramHistoryEvent = ((android.support.v4.e.a)localObject3).a(paramHistoryEvent);
      paramContact = com.truecaller.util.w.a(paramContact, str1);
      if (paramContact != null)
      {
        localObject3 = o;
        localObject1 = ", ";
        paramBoolean3 = true;
        localObject2 = new CharSequence[paramBoolean3];
        localObject2[0] = paramHistoryEvent;
        paramContact = paramContact.g();
        localObject2[i1] = paramContact;
        paramContact = am.a((String)localObject1, (CharSequence[])localObject2);
        ((TextView)localObject3).setText(paramContact);
      }
      else
      {
        paramContact = o;
        paramContact.setText(paramHistoryEvent);
      }
      paramContact = r;
      paramContact.setPhoneNumber(paramHistoryEvent);
    }
    else
    {
      e.setVisibility(paramBoolean1);
      paramHistoryEvent = o;
      paramHistoryEvent.setVisibility(paramBoolean1);
      str1 = paramContact.q();
      long l3 = -1;
      a = l3;
      paramContact = b;
      removeCallbacks(paramContact);
    }
    paramContact = f;
    int i2 = paramContact.length();
    if (i2 == 0)
    {
      paramContact = f;
      paramHistoryEvent = com.truecaller.common.h.k.f(getContext());
      paramHistoryEvent = at.a(aa.d(str1, paramHistoryEvent));
      paramContact.setText(paramHistoryEvent);
    }
  }
  
  protected final void a(Contact paramContact, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    Object localObject1 = f;
    String str1 = paramContact.K();
    boolean bool1 = TextUtils.isEmpty(str1);
    if (bool1) {
      str1 = paramContact.t();
    } else {
      str1 = paramContact.K();
    }
    ((TextView)localObject1).setText(str1);
    boolean bool2 = paramContact.N();
    int i1 = 8;
    boolean bool4 = true;
    Object localObject2;
    Object localObject3;
    if (!bool2)
    {
      bool2 = paramContact.Z();
      if (!bool2)
      {
        localObject1 = paramContact.l();
        bool2 = TextUtils.isEmpty((CharSequence)localObject1);
        if (!bool2)
        {
          g.setVisibility(0);
          localObject1 = g;
          localObject2 = getContext();
          int i5 = 2131886093;
          localObject3 = new Object[bool4];
          String str2 = paramContact.l();
          localObject3[0] = str2;
          localObject2 = ((Context)localObject2).getString(i5, (Object[])localObject3);
          ((TextView)localObject1).setText((CharSequence)localObject2);
          break label171;
        }
      }
    }
    localObject1 = g;
    ((TextView)localObject1).setVisibility(i1);
    label171:
    bool2 = paramContact.a(2);
    if ((bool2) && (!paramBoolean1))
    {
      paramBoolean1 = true;
    }
    else
    {
      paramBoolean1 = false;
      localObject4 = null;
    }
    localObject1 = f;
    if (paramBoolean1)
    {
      paramBoolean1 = 2131233985;
    }
    else
    {
      paramBoolean1 = false;
      localObject4 = null;
    }
    m.a((TextView)localObject1, 0, paramBoolean1);
    at.a(n, paramBoolean3);
    Object localObject4 = paramContact.g();
    paramBoolean3 = false;
    Context localContext1 = null;
    if (localObject4 != null)
    {
      localObject4 = ((Address)localObject4).getShortDisplayableAddress();
    }
    else
    {
      paramBoolean1 = false;
      localObject4 = null;
    }
    bool2 = TextUtils.isEmpty((CharSequence)localObject4);
    if (!bool2)
    {
      h.setVisibility(0);
      localObject1 = h;
      ((TextView)localObject1).setText((CharSequence)localObject4);
    }
    else
    {
      localObject4 = h;
      int i2 = 4;
      ((TextView)localObject4).setVisibility(i2);
    }
    paramBoolean1 = paramContact.I();
    int i6;
    if ((paramBoolean2) && (paramBoolean1))
    {
      i.setVisibility(0);
      localObject4 = i;
      localObject1 = getContext();
      i6 = 2131886146;
      Object[] arrayOfObject = new Object[bool4];
      int i7 = paramContact.I();
      localObject3 = Integer.valueOf(i7);
      arrayOfObject[0] = localObject3;
      localObject1 = ((Context)localObject1).getString(i6, arrayOfObject);
      ((TextView)localObject4).setText((CharSequence)localObject1);
    }
    else
    {
      localObject4 = i;
      ((TextView)localObject4).setVisibility(i1);
    }
    setUpSearchPartnerLogo(paramContact);
    localObject4 = ce.a(paramContact.J());
    TagView localTagView;
    if (localObject4 != null)
    {
      m.removeAllViews();
      localTagView = new com/truecaller/common/tag/TagView;
      localContext1 = getContext();
      localTagView.<init>(localContext1, bool4, bool4);
      localTagView.setTag((c)localObject4);
      paramBoolean1 = z;
      if (paramBoolean1)
      {
        paramBoolean1 = A;
        localTagView.setTint(paramBoolean1);
      }
      m.addView(localTagView);
      localObject4 = m;
      ((ViewGroup)localObject4).setVisibility(0);
    }
    else
    {
      paramBoolean1 = paramContact.R();
      if (!paramBoolean1)
      {
        localObject4 = com.truecaller.common.b.a.F();
        paramBoolean1 = ((com.truecaller.common.b.a)localObject4).p();
        if (paramBoolean1)
        {
          paramBoolean1 = d.a();
          if (paramBoolean1)
          {
            localObject4 = m;
            boolean bool3 = paramContact.a(32);
            Context localContext2;
            if ((!bool3) || (paramBoolean2))
            {
              bool4 = false;
              localContext2 = null;
            }
            ((ViewGroup)localObject4).removeAllViews();
            localTagView = new com/truecaller/common/tag/TagView;
            localObject1 = getContext();
            localTagView.<init>((Context)localObject1, false, false);
            localObject1 = getResources();
            i6 = 2131886311;
            localObject1 = ((Resources)localObject1).getString(i6);
            localTagView.setText((CharSequence)localObject1);
            int i3 = 2131233810;
            if (bool4)
            {
              int i4 = android.support.v4.content.b.c(getContext(), 2131100382);
              localObject2 = getContext();
              localObject1 = at.a((Context)localObject2, i3, i4);
              localTagView.setTextColor(i4);
              localContext2 = getContext();
              i6 = 2131100378;
              i4 = android.support.v4.content.b.c(localContext2, i6);
              localTagView.setBackgroundColor(i4);
            }
            else
            {
              localContext2 = getContext();
              i6 = 2130969592;
              localObject1 = com.truecaller.utils.ui.b.a(localContext2, i3, i6);
            }
            m.a(localTagView, null, (Drawable)localObject1);
            ((ViewGroup)localObject4).addView(localTagView);
            ((ViewGroup)localObject4).setVisibility(0);
            break label768;
          }
        }
      }
      localObject4 = m;
      ((ViewGroup)localObject4).setVisibility(i1);
    }
    label768:
    localObject4 = s;
    boolean bool5 = paramContact.N();
    if (bool5)
    {
      i1 = 0;
      str1 = null;
    }
    ((TextView)localObject4).setVisibility(i1);
  }
  
  public final void a(a parama)
  {
    r.setVisibility(0);
    d.setVisibility(4);
    r.setListener(parama);
    r.b();
  }
  
  protected final void b()
  {
    b(false);
    Object localObject = l;
    int i1 = 2130968622;
    com.truecaller.utils.ui.b.a((View)localObject, i1);
    boolean bool = z;
    if (!bool)
    {
      localObject = p;
      i1 = com.truecaller.utils.ui.b.a(getContext(), 2130968626);
      ((TextView)localObject).setTextColor(i1);
      return;
    }
    localObject = c;
    i1 = A;
    ((AvatarView)localObject).setTintColor(i1);
  }
  
  protected final void c()
  {
    Object localObject1 = null;
    b(false);
    Object localObject2 = l;
    int i1 = 2130969576;
    com.truecaller.utils.ui.b.a((View)localObject2, i1);
    localObject2 = q;
    ((TextView)localObject2).setVisibility(0);
    boolean bool = z;
    int i2;
    if (!bool)
    {
      localObject1 = p;
      localObject2 = getContext();
      i2 = com.truecaller.utils.ui.b.a((Context)localObject2, i1);
      ((TextView)localObject1).setTextColor(i2);
    }
    else
    {
      localObject1 = c;
      i2 = A;
      ((AvatarView)localObject1).setTintColor(i2);
    }
    f();
  }
  
  protected final void d()
  {
    b(true);
  }
  
  public final void e()
  {
    r.setVisibility(8);
    d.setVisibility(0);
    Object localObject = r;
    Context localContext = null;
    ((AfterCallBusinessView)localObject).setListener(null);
    int i1 = 2131362507;
    localObject = (TintedImageView)findViewById(i1);
    boolean bool1 = z;
    int i3;
    if (bool1)
    {
      int i2 = A;
    }
    else
    {
      boolean bool2 = C;
      int i4;
      if (bool2)
      {
        localContext = getContext();
        i4 = 2131100380;
        i3 = android.support.v4.content.b.c(localContext, i4);
      }
      else
      {
        localContext = getContext();
        i4 = 2130968623;
        i3 = com.truecaller.utils.ui.b.a(localContext, i4);
      }
    }
    ((TintedImageView)localObject).setTint(i3);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    Object localObject = n;
    paramBoolean = ((ImageButton)localObject).getVisibility();
    if (!paramBoolean)
    {
      paramBoolean = getResources().getDimensionPixelSize(2131165276);
      localObject = at.c(n, paramBoolean, paramBoolean);
      ((ViewGroup)n.getParent()).setTouchDelegate((TouchDelegate)localObject);
      return;
    }
    ((ViewGroup)n.getParent()).setTouchDelegate(null);
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    Uri localUri = t;
    if (localUri != null)
    {
      paramInt2 = x;
      b(localUri, paramInt2);
    }
  }
  
  public void setOnSuggestNameClickListener(View.OnClickListener paramOnClickListener)
  {
    n.setOnClickListener(paramOnClickListener);
  }
  
  public void setOnTagClickListener(View.OnClickListener paramOnClickListener)
  {
    m.setOnClickListener(paramOnClickListener);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.AfterCallHeaderView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */