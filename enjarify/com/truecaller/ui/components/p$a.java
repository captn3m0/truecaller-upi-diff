package com.truecaller.ui.components;

import android.view.View;
import android.widget.TextView;
import com.truecaller.ui.q.a;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.util.at;

public final class p$a
  extends d.c
  implements q.a
{
  TextView a;
  TextView c;
  TextView d;
  String e;
  boolean f;
  ContactPhoto g;
  
  p$a(View paramView)
  {
    super(paramView);
    TextView localTextView = at.c(paramView, 2131363718);
    a = localTextView;
    localTextView = at.c(paramView, 2131364281);
    c = localTextView;
    localTextView = (TextView)paramView.findViewById(2131364559);
    d = localTextView;
    paramView = (ContactPhoto)paramView.findViewById(2131362554);
    g = paramView;
  }
  
  public final String a()
  {
    return e;
  }
  
  public final boolean b()
  {
    return f;
  }
  
  public final void c_(String paramString)
  {
    e = paramString;
  }
  
  public final void c_(boolean paramBoolean)
  {
    f = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.p.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */