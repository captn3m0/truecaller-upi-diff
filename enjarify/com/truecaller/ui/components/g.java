package com.truecaller.ui.components;

import android.database.Cursor;

public abstract class g
  extends d
{
  public Cursor c = null;
  private int d;
  
  public Cursor a(Cursor paramCursor)
  {
    if (paramCursor != null)
    {
      localObject = "_id";
      int i = paramCursor.getColumnIndex((String)localObject);
      d = i;
    }
    Object localObject = c;
    c = paramCursor;
    notifyDataSetChanged();
    return (Cursor)localObject;
  }
  
  public final void a(d.c paramc, int paramInt)
  {
    c.moveToPosition(paramInt);
    Cursor localCursor = c;
    a(paramc, localCursor);
  }
  
  protected abstract void a(d.c paramc, Cursor paramCursor);
  
  public int getItemCount()
  {
    Cursor localCursor = c;
    if (localCursor != null) {
      return localCursor.getCount();
    }
    return 0;
  }
  
  public long getItemId(int paramInt)
  {
    int i = d;
    if (i >= 0)
    {
      c.moveToPosition(paramInt);
      Cursor localCursor = c;
      i = d;
      return localCursor.getLong(i);
    }
    return -1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */