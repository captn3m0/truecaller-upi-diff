package com.truecaller.ui.components;

import android.content.Context;
import android.support.design.widget.AppBarLayout.Behavior;
import android.util.AttributeSet;

public class FlingBehavior
  extends AppBarLayout.Behavior
{
  public FlingBehavior() {}
  
  public FlingBehavior(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.FlingBehavior
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */