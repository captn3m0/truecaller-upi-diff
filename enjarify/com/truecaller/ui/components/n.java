package com.truecaller.ui.components;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.support.v4.e.a;
import android.text.Html;
import java.util.ArrayList;
import java.util.List;

public class n
{
  int f;
  String g;
  String h;
  Object i = "";
  public int j;
  int k;
  boolean l;
  public CharSequence m;
  public CharSequence n;
  
  public n()
  {
    this("", "");
  }
  
  public n(int paramInt)
  {
    this(0, paramInt, -1, "");
  }
  
  public n(int paramInt1, int paramInt2, int paramInt3, Object paramObject)
  {
    int i1 = -1;
    j = i1;
    k = i1;
    f = paramInt1;
    j = paramInt2;
    k = paramInt3;
    i = paramObject;
  }
  
  public n(int paramInt, Object paramObject)
  {
    int i1 = -1;
    j = i1;
    k = i1;
    f = 0;
    j = paramInt;
    h = null;
    i = paramObject;
  }
  
  public n(String paramString1, String paramString2)
  {
    this(paramString1, paramString2, "");
  }
  
  public n(String paramString1, String paramString2, Object paramObject)
  {
    int i1 = -1;
    j = i1;
    k = i1;
    f = 0;
    g = paramString1;
    h = paramString2;
    i = paramObject;
  }
  
  public static String a(boolean paramBoolean, String paramString)
  {
    if ((paramBoolean) && (paramString != null))
    {
      a locala = a.a();
      paramString = locala.a(paramString);
    }
    return paramString;
  }
  
  public static List a(n.b[] paramArrayOfb)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    int i1 = paramArrayOfb.length;
    int i2 = 0;
    while (i2 < i1)
    {
      n.b localb = paramArrayOfb[i2];
      n localn = new com/truecaller/ui/components/n;
      int i3 = localb.getImageId();
      int i4 = localb.getNameId();
      int i5 = localb.getDetailsId();
      localn.<init>(i3, i4, i5, localb);
      localArrayList.add(localn);
      i2 += 1;
    }
    return localArrayList;
  }
  
  public static CharSequence b(boolean paramBoolean, String paramString)
  {
    if (paramString == null) {
      return null;
    }
    if (paramBoolean) {
      paramString = Html.fromHtml(paramString);
    }
    return paramString;
  }
  
  public String a(Context paramContext)
  {
    String str = g;
    if (str == null)
    {
      int i1 = j;
      int i2 = -1;
      if (i1 != i2)
      {
        paramContext = paramContext.getResources();
        i1 = j;
        paramContext = paramContext.getString(i1);
        g = paramContext;
      }
    }
    return g;
  }
  
  public final void a(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
  {
    l = true;
    m = paramCharSequence1;
    n = paramCharSequence2;
  }
  
  public String b(Context paramContext)
  {
    String str = h;
    if (str == null)
    {
      int i1 = k;
      int i2 = -1;
      if (i1 != i2)
      {
        paramContext = paramContext.getResources();
        i1 = k;
        paramContext = paramContext.getString(i1);
        h = paramContext;
      }
    }
    return h;
  }
  
  public int c()
  {
    return f;
  }
  
  public Bitmap c(Context paramContext)
  {
    return null;
  }
  
  public Object q()
  {
    return i;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */