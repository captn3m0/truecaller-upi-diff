package com.truecaller.ui.components;

import android.content.Context;
import android.support.v4.view.r;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;

public class FlowLayout
  extends ViewGroup
{
  private final FlowLayout.a a;
  
  public FlowLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramContext = new com/truecaller/ui/components/FlowLayout$a;
    paramContext.<init>(this, (byte)0);
    a = paramContext;
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return paramLayoutParams instanceof ViewGroup.MarginLayoutParams;
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams()
  {
    ViewGroup.MarginLayoutParams localMarginLayoutParams = new android/view/ViewGroup$MarginLayoutParams;
    ViewGroup.LayoutParams localLayoutParams = new android/view/ViewGroup$LayoutParams;
    int i = -2;
    localLayoutParams.<init>(i, i);
    localMarginLayoutParams.<init>(localLayoutParams);
    return localMarginLayoutParams;
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    ViewGroup.MarginLayoutParams localMarginLayoutParams = new android/view/ViewGroup$MarginLayoutParams;
    Context localContext = getContext();
    localMarginLayoutParams.<init>(localContext, paramAttributeSet);
    return localMarginLayoutParams;
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    ViewGroup.MarginLayoutParams localMarginLayoutParams = new android/view/ViewGroup$MarginLayoutParams;
    localMarginLayoutParams.<init>(paramLayoutParams);
    return localMarginLayoutParams;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    Object localObject = a;
    paramInt3 -= paramInt1;
    paramInt1 = getPaddingLeft();
    paramInt3 -= paramInt1;
    paramInt1 = getPaddingRight();
    paramInt3 -= paramInt1;
    ((FlowLayout.a)localObject).a(paramInt3);
    paramBoolean = r.g(this);
    paramInt1 = 0;
    paramInt2 = 1;
    if (paramBoolean != paramInt2) {
      paramInt2 = 0;
    }
    for (;;)
    {
      paramBoolean = getChildCount();
      if (paramInt1 >= paramBoolean) {
        break;
      }
      localObject = getChildAt(paramInt1);
      a.a((View)localObject);
      FlowLayout.a locala1 = a;
      paramInt3 = f;
      if (paramInt2 != 0) {
        paramInt4 = getPaddingRight();
      } else {
        paramInt4 = getPaddingLeft();
      }
      paramInt3 += paramInt4;
      paramInt4 = getPaddingTop();
      FlowLayout.a locala2 = a;
      int i = g;
      paramInt4 += i;
      i = ((View)localObject).getMeasuredWidth() + paramInt3;
      int j = ((View)localObject).getMeasuredHeight() + paramInt4;
      ((View)localObject).layout(paramInt3, paramInt4, i, j);
      paramInt1 += 1;
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i = View.MeasureSpec.getMode(paramInt1);
    int j = -1 << -1;
    int k = -1 >>> 1;
    Object localObject1;
    if (i != j) {
      if (i != 0)
      {
        j = 1073741824;
        if (i != j)
        {
          localObject1 = new java/lang/AssertionError;
          ((AssertionError)localObject1).<init>();
          throw ((Throwable)localObject1);
        }
      }
      else
      {
        i = -1 >>> 1;
        break label63;
      }
    }
    i = View.MeasureSpec.getSize(paramInt1);
    label63:
    a.a(i);
    j = 0;
    for (;;)
    {
      int m = getChildCount();
      if (j >= m) {
        break;
      }
      View localView = getChildAt(j);
      Object localObject2 = this;
      measureChildWithMargins(localView, paramInt1, 0, paramInt2, 0);
      localObject2 = a;
      ((FlowLayout.a)localObject2).a(localView);
      j += 1;
    }
    if (i == k)
    {
      localObject1 = a;
      i = c;
    }
    paramInt1 = a.d;
    paramInt2 = a.e;
    paramInt1 += paramInt2;
    paramInt2 = getPaddingTop();
    paramInt1 += paramInt2;
    paramInt2 = getPaddingBottom();
    paramInt1 += paramInt2;
    paramInt2 = getPaddingLeft();
    j = getPaddingRight();
    paramInt2 += j;
    i += paramInt2;
    setMeasuredDimension(i, paramInt1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.FlowLayout
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */