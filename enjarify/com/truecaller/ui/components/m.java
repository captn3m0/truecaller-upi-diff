package com.truecaller.ui.components;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.truecaller.util.at;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;

public final class m
  extends l
{
  private List a;
  
  public m(Context paramContext, List paramList, m.a parama)
  {
    super(paramContext, paramList, 1);
    paramContext = new java/util/ArrayList;
    paramContext.<init>();
    a = paramContext;
    a.add(parama);
  }
  
  private static boolean a(PackageManager paramPackageManager, String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool) {
      return false;
    }
    try
    {
      paramPackageManager.getPackageInfo(paramString, 0);
      return true;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
    return false;
  }
  
  public final View a(int paramInt, ViewGroup paramViewGroup)
  {
    if (paramInt == 0) {
      return at.b(getContext(), 2131559034);
    }
    paramInt += -1;
    return super.a(paramInt, paramViewGroup);
  }
  
  public final void a(View paramView, n paramn, int paramInt1, int paramInt2)
  {
    int i = 1;
    if (paramInt1 == 0)
    {
      int j = 2131559034;
      Object localObject1 = (m.c)paramView.getTag(j);
      if (localObject1 == null)
      {
        localObject1 = new com/truecaller/ui/components/m$c;
        paramInt2 = 0;
        ((m.c)localObject1).<init>((byte)0);
        int k = 2131363027;
        LinearLayout localLinearLayout = (LinearLayout)at.b(paramView, k);
        a = localLinearLayout;
        int m = 2131363198;
        Object localObject2 = (LinearLayout)at.b(paramView, m);
        b = ((LinearLayout)localObject2);
        paramView.setTag(j, localObject1);
        paramn = a;
        localObject2 = new com/truecaller/ui/components/-$$Lambda$m$7JMxyj5AEiIUwl75_TbQRmclnXg;
        ((-..Lambda.m.7JMxyj5AEiIUwl75_TbQRmclnXg)localObject2).<init>(this);
        paramn.setOnClickListener((View.OnClickListener)localObject2);
        paramn = b;
        localObject1 = new com/truecaller/ui/components/-$$Lambda$m$Jj_AThHuDru6Ue2aY7m76LdS9mM;
        ((-..Lambda.m.Jj_AThHuDru6Ue2aY7m76LdS9mM)localObject1).<init>(this);
        paramn.setOnClickListener((View.OnClickListener)localObject1);
        paramn = getContext();
        localObject1 = EnumSet.allOf(m.b.class);
        paramn = paramn.getPackageManager();
        localObject2 = ((EnumSet)localObject1).iterator();
        for (;;)
        {
          boolean bool1 = ((Iterator)localObject2).hasNext();
          if (!bool1) {
            break;
          }
          m.b localb = (m.b)((Iterator)localObject2).next();
          String str = d;
          boolean bool2 = a(paramn, str);
          if (bool2) {
            e = i;
          }
        }
        paramn = ((EnumSet)localObject1).iterator();
        for (;;)
        {
          boolean bool3 = paramn.hasNext();
          if (!bool3) {
            break;
          }
          localObject2 = (m.b)paramn.next();
          int n = c;
          if (n == 0)
          {
            bool3 = e;
            if (bool3) {
              break label506;
            }
          }
        }
        paramn = ((EnumSet)localObject1).iterator();
        int i1;
        do
        {
          do
          {
            paramInt1 = paramn.hasNext();
            if (paramInt1 == 0) {
              break;
            }
            localObject1 = (m.b)paramn.next();
            i1 = c;
          } while (i1 != i);
          paramInt1 = e;
        } while (paramInt1 == 0);
        paramn = (LinearLayout)paramView.findViewById(k);
        paramView = (LinearLayout)paramView.findViewById(m);
        if ((paramn != null) && (paramView != null))
        {
          localObject1 = (ViewGroup)paramn.getParent();
          i = 0;
          for (;;)
          {
            i1 = ((ViewGroup)localObject1).getChildCount();
            if (i >= i1) {
              break;
            }
            localObject2 = ((ViewGroup)localObject1).getChildAt(i);
            i1 = ((View)localObject2).getId();
            if (i1 == k)
            {
              ((ViewGroup)localObject1).removeView(paramn);
              break;
            }
            i += 1;
          }
          ViewGroup localViewGroup = (ViewGroup)paramView.getParent();
          for (;;)
          {
            i1 = localViewGroup.getChildCount();
            if (paramInt2 >= i1) {
              break;
            }
            localObject2 = localViewGroup.getChildAt(paramInt2);
            i1 = ((View)localObject2).getId();
            if (i1 == m)
            {
              localViewGroup.removeView(paramView);
              break;
            }
            paramInt2 += 1;
          }
          ((ViewGroup)localObject1).addView(paramView, i);
          localViewGroup.addView(paramn);
          return;
        }
        return;
      }
      label506:
      return;
    }
    paramInt1 -= i;
    super.a(paramView, paramn, paramInt1, paramInt2);
  }
  
  public final boolean areAllItemsEnabled()
  {
    return false;
  }
  
  public final int b(int paramInt)
  {
    if (paramInt == 0) {
      return 0;
    }
    return super.b(paramInt) + 1;
  }
  
  public final boolean isEnabled(int paramInt)
  {
    paramInt = b(paramInt);
    return paramInt != 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */