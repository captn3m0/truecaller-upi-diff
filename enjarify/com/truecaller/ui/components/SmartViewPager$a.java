package com.truecaller.ui.components;

import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;

final class SmartViewPager$a
  extends GestureDetector.SimpleOnGestureListener
{
  private SmartViewPager$a(SmartViewPager paramSmartViewPager) {}
  
  public final boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    float f1 = Math.abs(paramFloat1);
    float f2 = Math.abs(paramFloat2);
    boolean bool = f1 < f2;
    return bool;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.SmartViewPager.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */