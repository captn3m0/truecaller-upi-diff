package com.truecaller.ui.components;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.util.at;

public final class h$a
{
  public final TextView a;
  public final TextView b;
  public final ImageView c;
  public final ImageView d;
  public final ImageView e;
  
  public h$a(View paramView)
  {
    Object localObject = at.c(paramView, 2131363657);
    a = ((TextView)localObject);
    localObject = at.c(paramView, 2131363653);
    b = ((TextView)localObject);
    localObject = at.d(paramView, 2131361795);
    c = ((ImageView)localObject);
    localObject = at.d(paramView, 2131363658);
    d = ((ImageView)localObject);
    paramView = (ImageView)paramView.findViewById(2131364084);
    e = paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.h.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */