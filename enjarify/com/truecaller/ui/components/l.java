package com.truecaller.ui.components;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.content.b;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.common.h.am;
import com.truecaller.old.data.access.d;
import com.truecaller.util.at;
import com.truecaller.util.aw;
import java.util.List;

public class l
  extends ArrayAdapter
  implements AbsListView.OnScrollListener
{
  private final LayoutInflater a;
  private final int b = 2131559038;
  private aw c;
  private boolean d;
  private int e;
  private Filter f;
  private int g;
  private final l h;
  
  public l(Context paramContext, List paramList)
  {
    this(paramContext, paramList, 0);
  }
  
  public l(Context paramContext, List paramList, int paramInt)
  {
    super(paramContext, 0, paramList);
    paramList = aw.b(paramContext);
    c = paramList;
    paramContext = LayoutInflater.from(paramContext);
    a = paramContext;
    d = false;
    paramInt += 1;
    g = paramInt;
    h = this;
  }
  
  private n c(int paramInt)
  {
    return (n)super.getItem(paramInt);
  }
  
  public View a(int paramInt, ViewGroup paramViewGroup)
  {
    LayoutInflater localLayoutInflater = a;
    int i = b;
    return localLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public final n a(int paramInt)
  {
    return h.c(paramInt);
  }
  
  public void a(View paramView, n paramn, int paramInt1, int paramInt2)
  {
    Object localObject1 = paramView.getTag();
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/ui/components/l$b;
      ((l.b)localObject1).<init>(paramView);
      paramView.setTag(localObject1);
    }
    else
    {
      localObject1 = (l.b)localObject1;
    }
    int i = e;
    if (i == paramInt2)
    {
      boolean bool1 = f;
      boolean bool3 = d;
      if (bool1 == bool3)
      {
        j = g;
        k = e;
        if (j == k) {
          break label681;
        }
      }
    }
    int j = e;
    int k = g;
    boolean bool5 = true;
    Object localObject2;
    if (j != k)
    {
      j = 1;
    }
    else
    {
      j = 0;
      localObject2 = null;
    }
    e = paramInt2;
    boolean bool4 = d;
    f = bool4;
    int m = e;
    g = m;
    m = 0;
    if (j == 0)
    {
      bool2 = l;
      if (bool2) {}
    }
    else
    {
      localObject2 = getContext();
      localObject2 = paramn.a((Context)localObject2);
      Object localObject3 = getContext();
      localObject3 = paramn.b((Context)localObject3);
      boolean bool6 = paramn instanceof n.d;
      boolean bool7 = paramn instanceof n.a;
      localObject2 = n.a(bool7, (String)localObject2);
      localObject2 = n.b(bool6, (String)localObject2);
      boolean bool8 = am.a((CharSequence)localObject3);
      if (bool8)
      {
        localObject3 = n.a(bool7, (String)localObject3);
        localObject3 = n.b(bool6, (String)localObject3);
      }
      else
      {
        localObject3 = null;
      }
      paramn.a((CharSequence)localObject2, (CharSequence)localObject3);
    }
    boolean bool2 = l;
    if (bool2)
    {
      Object localObject4 = b;
      if (localObject4 != null)
      {
        localObject4 = b;
        localObject2 = m;
        ((TextView)localObject4).setText((CharSequence)localObject2);
      }
      localObject4 = c;
      TextView localTextView;
      if (localObject4 != null)
      {
        localObject4 = c;
        localObject2 = n;
        ((TextView)localObject4).setText((CharSequence)localObject2);
        localObject4 = c;
        localObject2 = n;
        if (localObject2 == null)
        {
          bool5 = false;
          localTextView = null;
        }
        at.a((View)localObject4, bool5);
      }
      getContext();
      paramInt2 = paramn.c();
      localObject2 = a;
      int n = -1;
      if (localObject2 == null)
      {
        if ((paramInt2 != n) && (paramInt2 != 0))
        {
          localObject2 = getContext();
          localObject4 = b.a((Context)localObject2, paramInt2);
        }
        else
        {
          paramInt2 = 0;
          localObject4 = null;
        }
        bool2 = d.c();
        if (bool2)
        {
          localObject2 = localObject4;
          paramInt2 = 0;
          localObject4 = null;
        }
        else
        {
          bool2 = false;
          localObject2 = null;
        }
        localTextView = b;
        localTextView.setCompoundDrawablesWithIntrinsicBounds((Drawable)localObject4, null, (Drawable)localObject2, null);
      }
      else if (paramInt2 != n)
      {
        localObject2 = a;
        at.a((ImageView)localObject2, paramInt2);
      }
      else
      {
        paramInt2 = d;
        if (paramInt2 != 0)
        {
          localObject4 = aw.a(am.a(paramn));
          if (localObject4 == null)
          {
            localObject4 = c;
            paramInt2 = a;
            if (paramInt2 != 0)
            {
              localObject2 = a;
              ((ImageView)localObject2).setImageResource(paramInt2);
            }
            else
            {
              localObject4 = a;
              ((ImageView)localObject4).setImageBitmap(null);
            }
          }
          else
          {
            localObject2 = a;
            ((ImageView)localObject2).setImageBitmap((Bitmap)localObject4);
          }
        }
        else
        {
          localObject4 = c;
          localObject2 = a;
          ((aw)localObject4).a(paramn, (ImageView)localObject2);
        }
      }
      paramInt2 = 2131364656;
      paramView.setTag(paramInt2, paramn);
      paramView.setTag(localObject1);
      int i1 = 2131364665;
      paramInt1 = b;
      localObject1 = Integer.valueOf(paramInt1);
      paramView.setTag(i1, localObject1);
      label681:
      return;
    }
    paramView = new java/lang/IllegalStateException;
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("You did not update the presentation of ");
    paramn = paramn.getClass().getSimpleName();
    ((StringBuilder)localObject1).append(paramn);
    ((StringBuilder)localObject1).append(" at position ");
    ((StringBuilder)localObject1).append(paramInt2);
    paramn = ((StringBuilder)localObject1).toString();
    paramView.<init>(paramn);
    throw paramView;
  }
  
  public int b(int paramInt)
  {
    return 0;
  }
  
  public Filter getFilter()
  {
    Filter localFilter = f;
    if (localFilter == null)
    {
      int i = getCount();
      if (i > 0)
      {
        i = 0;
        localFilter = null;
        Object localObject = a(0);
        boolean bool = localObject instanceof n.c;
        if (bool)
        {
          localObject = new com/truecaller/ui/components/l$a;
          ((l.a)localObject).<init>(this, (byte)0);
          f = ((Filter)localObject);
        }
      }
    }
    localFilter = f;
    if (localFilter != null) {
      return localFilter;
    }
    return super.getFilter();
  }
  
  public int getItemViewType(int paramInt)
  {
    return h.b(paramInt);
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramInt >= 0)
    {
      int i = getCount();
      if (paramInt < i)
      {
        if (paramView == null)
        {
          paramView = h;
          i = paramView.b(paramInt);
          paramView = paramView.a(i, paramViewGroup);
        }
        paramViewGroup = h.c(paramInt);
        i = h.b(paramInt);
        h.a(paramView, paramViewGroup, i, paramInt);
        return paramView;
      }
    }
    return paramView;
  }
  
  public int getViewTypeCount()
  {
    return g + 2;
  }
  
  public void notifyDataSetChanged()
  {
    int i = e + 1;
    e = i;
    super.notifyDataSetChanged();
  }
  
  public void onScroll(AbsListView paramAbsListView, int paramInt1, int paramInt2, int paramInt3) {}
  
  public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt)
  {
    int i = 1;
    if (paramInt != 0) {
      paramInt = 1;
    } else {
      paramInt = 0;
    }
    boolean bool = d;
    if ((!bool) || (paramInt != 0)) {
      i = 0;
    }
    d = paramInt;
    if (i != 0) {
      notifyDataSetChanged();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */