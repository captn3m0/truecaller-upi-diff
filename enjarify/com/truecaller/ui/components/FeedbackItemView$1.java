package com.truecaller.ui.components;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.TextView;

final class FeedbackItemView$1
  extends AnimatorListenerAdapter
{
  FeedbackItemView$1(FeedbackItemView paramFeedbackItemView, String paramString, Drawable paramDrawable) {}
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    paramAnimator = FeedbackItemView.a(c);
    Object localObject = a;
    paramAnimator.setText((CharSequence)localObject);
    paramAnimator = FeedbackItemView.b(c);
    localObject = b;
    paramAnimator.setImageDrawable((Drawable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.FeedbackItemView.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */