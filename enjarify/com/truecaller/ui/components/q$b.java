package com.truecaller.ui.components;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.internal.ads.zzly;
import com.truecaller.TrueApp;
import com.truecaller.ads.CustomTemplate;
import com.truecaller.ads.campaigns.AdCampaigns;
import com.truecaller.ads.g;
import com.truecaller.ads.j;
import com.truecaller.ads.k;
import com.truecaller.ads.k.a;
import com.truecaller.ads.k.b;
import com.truecaller.ads.k.c;
import com.truecaller.ads.provider.a;
import com.truecaller.ads.provider.f;
import com.truecaller.ads.provider.fetch.AdsConfigurationManager;
import com.truecaller.ads.provider.fetch.l;
import com.truecaller.ads.provider.holders.e;
import com.truecaller.bp;
import com.truecaller.log.AssertionUtil;
import java.util.LinkedList;
import java.util.Queue;

public final class q$b
{
  public PublisherAdView a;
  public View b;
  g c;
  e d;
  private final Queue f;
  private final String[] g;
  private final AdCampaigns h;
  private final ViewGroup i;
  private q.c j;
  
  q$b(q paramq, ViewGroup paramViewGroup, AdCampaigns paramAdCampaigns)
  {
    paramq = new java/util/LinkedList;
    paramq.<init>();
    f = paramq;
    i = paramViewGroup;
    h = paramAdCampaigns;
    paramq = paramAdCampaigns.a();
    g = paramq;
    paramq = paramAdCampaigns.d();
    int k = paramq.length;
    paramAdCampaigns = null;
    int m = 0;
    while (m < k)
    {
      Object localObject = paramq[m];
      q.c[] arrayOfc = q.a;
      int n = arrayOfc.length;
      int i1 = 0;
      while (i1 < n)
      {
        q.c localc = arrayOfc[i1];
        String str = a;
        boolean bool = str.equals(localObject);
        if (bool)
        {
          localObject = f;
          ((Queue)localObject).add(localc);
          break;
        }
        i1 += 1;
      }
      m += 1;
    }
  }
  
  private void a(String paramString)
  {
    int k = 3;
    Object localObject1 = new String[k];
    Object localObject2 = "ACS-ads";
    Object localObject3 = null;
    localObject1[0] = localObject2;
    boolean bool = true;
    localObject1[bool] = paramString;
    Object localObject4 = "loadBannerAd";
    int m = 2;
    localObject1[m] = localObject4;
    a();
    localObject1 = d();
    if (localObject1 != null)
    {
      localObject2 = new AdSize[bool];
      localObject4 = AdSize.c;
      localObject2[0] = localObject4;
      ((PublisherAdView)localObject1).setAdSizes((AdSize[])localObject2);
      ((PublisherAdView)localObject1).setAdUnitId(paramString);
      localObject2 = q.h(e);
      ((PublisherAdView)localObject1).setAdListener((AdListener)localObject2);
      localObject2 = TrueApp.y().a();
      localObject3 = ((bp)localObject2).av();
      localObject2 = ((bp)localObject2).aw();
      localObject4 = e.getContext();
      String[] arrayOfString = g;
      bool = ((AdsConfigurationManager)localObject2).a();
      localObject2 = ((l)localObject3).a((Context)localObject4, arrayOfString, bool);
      localObject3 = e;
      q.a((q)localObject3, paramString);
      try
      {
        ((PublisherAdView)localObject1).a((PublisherAdRequest)localObject2);
        localObject1 = e;
        localObject1 = q.d((q)localObject1);
        localObject2 = AdSize.c;
        ((a)localObject1).a(paramString, (AdSize)localObject2);
        return;
      }
      catch (Exception paramString)
      {
        AssertionUtil.reportThrowableButNeverCrash(paramString);
        a();
      }
    }
  }
  
  private void b(String paramString)
  {
    int k = 3;
    Object localObject1 = new String[k];
    Object localObject2 = "ACS-ads";
    CustomTemplate localCustomTemplate = null;
    localObject1[0] = localObject2;
    boolean bool = true;
    localObject1[bool] = paramString;
    Object localObject3 = "loadUnifiedAd";
    int m = 2;
    localObject1[m] = localObject3;
    a();
    localObject1 = q.i(e);
    if (localObject1 == null) {
      return;
    }
    localObject1 = k.a().a(paramString);
    localObject3 = q.j(e);
    localObject1 = ((k.c)localObject1).a((j)localObject3);
    localObject3 = new AdSize[k];
    Object localObject4 = AdSize.a;
    localObject3[0] = localObject4;
    localObject4 = AdSize.c;
    localObject3[bool] = localObject4;
    localObject4 = new com/google/android/gms/ads/AdSize;
    ((AdSize)localObject4).<init>(320, 140);
    localObject3[m] = localObject4;
    ((k.b)localObject1).a((AdSize[])localObject3);
    localObject3 = new CustomTemplate[5];
    localObject4 = CustomTemplate.NATIVE_BANNER;
    localObject3[0] = localObject4;
    localCustomTemplate = CustomTemplate.NATIVE_BANNER_DUAL_TRACKER;
    localObject3[bool] = localCustomTemplate;
    localCustomTemplate = CustomTemplate.CLICK_TO_PLAY_VIDEO;
    localObject3[m] = localCustomTemplate;
    localCustomTemplate = CustomTemplate.VIDEO_WITH_FALLBACK_IMAGE;
    localObject3[k] = localCustomTemplate;
    localCustomTemplate = CustomTemplate.NATIVE_CONTENT_DUAL_TRACKER;
    localObject3[4] = localCustomTemplate;
    ((k.b)localObject1).a((CustomTemplate[])localObject3);
    h = bool;
    i = bool;
    ((k.b)localObject1).c("afterCall");
    k localk = ((k.b)localObject1).e();
    q.a(e, localk);
    localObject1 = new com/truecaller/ui/components/q$b$1;
    ((q.b.1)localObject1).<init>(this, localk);
    c = ((g)localObject1);
    localObject1 = q.g(e);
    localObject2 = c;
    ((f)localObject1).a(localk, (g)localObject2);
    q.a(e, paramString);
  }
  
  private PublisherAdView d()
  {
    PublisherAdView localPublisherAdView = a;
    if (localPublisherAdView == null) {
      try
      {
        localPublisherAdView = new com/google/android/gms/ads/doubleclick/PublisherAdView;
        Object localObject = i;
        localObject = ((ViewGroup)localObject).getContext();
        localPublisherAdView.<init>((Context)localObject);
        a = localPublisherAdView;
        localPublisherAdView = a;
        int k = 4;
        localPublisherAdView.setVisibility(k);
        localPublisherAdView = a;
        localObject = q.h(e);
        localPublisherAdView.setAdListener((AdListener)localObject);
        i.removeAllViews();
        localPublisherAdView = a;
        a(localPublisherAdView);
      }
      catch (Exception localException)
      {
        AssertionUtil.reportThrowableButNeverCrash(localException);
        return null;
      }
    }
    return a;
  }
  
  public final void a()
  {
    Object localObject = a;
    if (localObject != null)
    {
      b((View)localObject);
      localObject = a.a;
      ((zzly)localObject).destroy();
      a = null;
    }
    localObject = c;
    if (localObject != null)
    {
      localObject = q.f(e);
      if (localObject != null)
      {
        localObject = q.g(e);
        k localk = q.f(e);
        g localg = c;
        ((f)localObject).b(localk, localg);
      }
    }
    c = null;
    localObject = b;
    if (localObject != null)
    {
      b((View)localObject);
      b = null;
    }
    localObject = d;
    if (localObject != null)
    {
      ((e)localObject).d();
      d = null;
    }
  }
  
  final void a(View paramView)
  {
    ViewGroup localViewGroup = i;
    ViewGroup.LayoutParams localLayoutParams = new android/view/ViewGroup$LayoutParams;
    localLayoutParams.<init>(-1, -2);
    localViewGroup.addView(paramView, localLayoutParams);
  }
  
  public final void b()
  {
    Object localObject1 = j;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = a;
    int k = -1;
    int m = ((String)localObject2).hashCode();
    int n = -1422244343;
    String str;
    boolean bool;
    if (m != n)
    {
      n = -286932590;
      if (m == n)
      {
        str = "unified";
        bool = ((String)localObject2).equals(str);
        if (bool) {
          k = 0;
        }
      }
    }
    else
    {
      str = "banner-320x100";
      bool = ((String)localObject2).equals(str);
      if (bool) {
        k = 1;
      }
    }
    switch (k)
    {
    default: 
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Unsupported type of Ad Unit: ");
      localObject1 = a;
      ((StringBuilder)localObject2).append((String)localObject1);
      AssertionUtil.reportWeirdnessButNeverCrash(((StringBuilder)localObject2).toString());
      return;
    case 1: 
      localObject1 = b;
      a((String)localObject1);
      return;
    }
    localObject1 = b;
    b((String)localObject1);
  }
  
  final void b(View paramView)
  {
    i.removeView(paramView);
  }
  
  final void c()
  {
    q.c localc = (q.c)f.poll();
    j = localc;
    b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.q.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */