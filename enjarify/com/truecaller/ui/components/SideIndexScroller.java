package com.truecaller.ui.components;

import android.animation.TimeInterpolator;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.d;
import com.truecaller.util.at;
import com.truecaller.utils.extensions.t;
import com.truecaller.utils.ui.b;
import java.util.ArrayList;
import java.util.List;

public class SideIndexScroller
  extends View
{
  private SideIndexScroller.a a;
  private TextView b;
  private RecyclerView c;
  private List d;
  private Paint e;
  private final Rect f;
  private SpannableString g;
  private int h;
  private float i;
  
  public SideIndexScroller(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private SideIndexScroller(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet, 0);
    paramContext = new android/graphics/Rect;
    paramContext.<init>();
    f = paramContext;
    boolean bool1 = isInEditMode();
    if (bool1) {
      paramContext = "en";
    } else {
      paramContext = Settings.k();
    }
    paramContext = com.truecaller.search.local.b.f.a(paramContext);
    paramAttributeSet = new java/util/ArrayList;
    paramContext = c;
    paramAttributeSet.<init>(paramContext);
    d = paramAttributeSet;
    paramContext = d;
    paramAttributeSet = Character.valueOf('★');
    paramContext.add(0, paramAttributeSet);
    paramContext = d;
    paramAttributeSet = Character.valueOf('#');
    paramByte = 1;
    paramContext.add(paramByte, paramAttributeSet);
    int j = b.a(getContext(), 2130969592);
    paramAttributeSet = new android/graphics/PorterDuffColorFilter;
    PorterDuff.Mode localMode = PorterDuff.Mode.SRC_ATOP;
    paramAttributeSet.<init>(j, localMode);
    paramContext = new android/graphics/Paint;
    paramContext.<init>(paramByte);
    e = paramContext;
    e.setColorFilter(paramAttributeSet);
    paramContext = e;
    paramAttributeSet = getContext();
    float f1 = 12.0F;
    float f2 = at.b(paramAttributeSet, f1);
    paramContext.setTextSize(f2);
    paramContext = e;
    paramAttributeSet = Paint.Align.CENTER;
    paramContext.setTextAlign(paramAttributeSet);
    j = getResources().getInteger(17694720);
    h = j;
    paramContext = getResources();
    f2 = 1.7946612E38F;
    float f3 = paramContext.getDimension(2131166187);
    boolean bool2 = com.truecaller.common.e.f.b();
    if (bool2) {
      paramByte = -1;
    }
    f2 = paramByte;
    f3 *= f2;
    i = f3;
  }
  
  private void a(int paramInt)
  {
    Object localObject = a;
    boolean bool = localObject instanceof d;
    if (bool)
    {
      localObject = a;
      RecyclerView localRecyclerView = c;
      ((RecyclerView.OnScrollListener)localObject).onScrollStateChanged(localRecyclerView, paramInt);
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    float f1 = 0.0F;
    float f2;
    boolean bool;
    if (paramBoolean)
    {
      f2 = getTranslationX();
      bool = f2 < 0.0F;
      if (!bool) {}
    }
    else
    {
      if (paramBoolean) {
        break label41;
      }
      f2 = getTranslationX();
      bool = f2 < 0.0F;
      if (!bool) {
        break label41;
      }
    }
    return;
    label41:
    if (paramBoolean)
    {
      f2 = i;
    }
    else
    {
      bool = false;
      f2 = 0.0F;
      localViewPropertyAnimator = null;
    }
    if (!paramBoolean) {
      f1 = i;
    }
    setTranslationX(f2);
    ViewPropertyAnimator localViewPropertyAnimator = animate();
    int j = h;
    long l = j;
    localViewPropertyAnimator = localViewPropertyAnimator.setDuration(l);
    Object localObject;
    if (paramBoolean)
    {
      localObject = new android/view/animation/DecelerateInterpolator;
      ((DecelerateInterpolator)localObject).<init>();
    }
    else
    {
      localObject = new android/view/animation/AccelerateInterpolator;
      ((AccelerateInterpolator)localObject).<init>();
    }
    localViewPropertyAnimator.setInterpolator((TimeInterpolator)localObject).translationX(f1);
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    int j = getHeight();
    int k = d.size();
    j /= k;
    Object localObject1 = e;
    Object localObject2 = "Q";
    Object localObject3 = f;
    int m = 1;
    ((Paint)localObject1).getTextBounds((String)localObject2, 0, m, (Rect)localObject3);
    localObject1 = f;
    k = ((Rect)localObject1).height();
    if (j < k) {
      return;
    }
    k = 0;
    localObject1 = null;
    for (;;)
    {
      localObject2 = d;
      int n = ((List)localObject2).size();
      if (k >= n) {
        break;
      }
      localObject2 = (Character)d.get(k);
      n = ((Character)localObject2).charValue();
      localObject3 = a;
      int i1 = ((SideIndexScroller.a)localObject3).a(n);
      if (i1 < 0)
      {
        i1 = 64;
        f1 = 9.0E-44F;
      }
      else
      {
        i1 = 255;
        f1 = 3.57E-43F;
      }
      localObject2 = String.valueOf(n);
      Paint localPaint1 = e;
      Object localObject4 = f;
      localPaint1.getTextBounds((String)localObject2, 0, m, (Rect)localObject4);
      int i2 = getWidth() / 2;
      int i3 = f.height();
      int i4 = f.height();
      i4 = (j - i4) / 2;
      i3 += i4;
      i4 = j * k;
      i3 += i4;
      Paint localPaint2 = e;
      localPaint2.setAlpha(i1);
      float f1 = i2;
      float f2 = i3;
      localObject4 = e;
      paramCanvas.drawText((String)localObject2, f1, f2, (Paint)localObject4);
      k += 1;
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    int j = paramMotionEvent.getActionMasked();
    int n = 1;
    switch (j)
    {
    default: 
      break;
    case 2: 
      localObject1 = b;
      ((TextView)localObject1).setVisibility(0);
      a(n);
      break;
    case 1: 
    case 3: 
      paramMotionEvent = b;
      j = 8;
      paramMotionEvent.setVisibility(j);
      a(0);
      break;
    }
    Object localObject1 = getContext();
    boolean bool1 = localObject1 instanceof Activity;
    if (bool1)
    {
      k = 2;
      t.a(this, false, k);
    }
    int k = getHeight();
    Object localObject2;
    float f1;
    int i2;
    if (k > 0)
    {
      localObject2 = d;
      int i1 = ((List)localObject2).size();
      k /= i1;
      f1 = paramMotionEvent.getY(0);
      i2 = (int)f1 / k;
    }
    else
    {
      i2 = -1;
      f1 = 0.0F / 0.0F;
    }
    if (i2 >= 0)
    {
      localObject1 = d;
      k = ((List)localObject1).size();
      if (i2 < k)
      {
        localObject1 = (Character)d.get(i2);
        k = ((Character)localObject1).charValue();
        f1 = paramMotionEvent.getY();
        Object localObject3 = b;
        int i4 = ((TextView)localObject3).getHeight();
        float f2 = i4;
        boolean bool2 = f1 < f2;
        int i3;
        if (bool2)
        {
          localObject2 = b;
          i4 = 2131234975;
          f2 = 1.808613E38F;
          ((TextView)localObject2).setBackgroundResource(i4);
          f3 = paramMotionEvent.getY();
          i3 = getTop();
          f1 = i3;
          f3 += f1;
        }
        else
        {
          localObject2 = b;
          i4 = 2131234974;
          f2 = 1.8086129E38F;
          ((TextView)localObject2).setBackgroundResource(i4);
          f3 = paramMotionEvent.getY();
          localObject2 = b;
          f1 = ((TextView)localObject2).getHeight();
          f3 -= f1;
          i3 = getTop();
          f1 = i3;
          f3 += f1;
        }
        localObject2 = b.getBackground();
        localObject3 = getContext();
        i4 = b.a((Context)localObject3, 2130969528);
        PorterDuff.Mode localMode = PorterDuff.Mode.SRC_ATOP;
        ((Drawable)localObject2).setColorFilter(i4, localMode);
        localObject2 = b;
        i4 = getTop();
        int i5 = getHeight();
        i4 += i5;
        f2 = i4;
        i5 = getTop();
        float f4 = i5;
        float f3 = Math.max(f3, f4);
        f3 = Math.min(f2, f3);
        ((TextView)localObject2).setTranslationY(f3);
        int i6 = 9733;
        f3 = 1.3639E-41F;
        if (k == i6)
        {
          paramMotionEvent = g;
          if (paramMotionEvent == null)
          {
            paramMotionEvent = new android/text/SpannableString;
            paramMotionEvent.<init>(" ");
            g = paramMotionEvent;
            paramMotionEvent = g;
            localObject2 = new android/text/style/ImageSpan;
            localObject3 = getContext();
            i5 = 2131234571;
            f4 = 1.8085311E38F;
            ((ImageSpan)localObject2).<init>((Context)localObject3, i5);
            paramMotionEvent.setSpan(localObject2, 0, n, 0);
          }
          paramMotionEvent = b;
          localObject2 = g;
          paramMotionEvent.setText((CharSequence)localObject2);
        }
        else
        {
          paramMotionEvent = b;
          localObject2 = String.valueOf(k);
          paramMotionEvent.setText((CharSequence)localObject2);
        }
        paramMotionEvent = a;
        int i7 = paramMotionEvent.a(k);
        if (i7 >= 0)
        {
          localObject1 = a;
          int m = ((SideIndexScroller.a)localObject1).getItemCount();
          if (i7 < m)
          {
            localObject1 = (LinearLayoutManager)c.getLayoutManager();
            ((LinearLayoutManager)localObject1).scrollToPositionWithOffset(i7, 0);
          }
        }
      }
    }
    return n;
  }
  
  public void setAdapter(SideIndexScroller.a parama)
  {
    a = parama;
  }
  
  public void setFloatingLabel(TextView paramTextView)
  {
    b = paramTextView;
  }
  
  public void setRecyclerView(RecyclerView paramRecyclerView)
  {
    c = paramRecyclerView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.SideIndexScroller
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */