package com.truecaller.ui.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

public class CallerIdView
  extends LinearLayout
{
  private static int[] a;
  
  static
  {
    int[] arrayOfInt = new int[4];
    arrayOfInt[0] = 2131362414;
    arrayOfInt[1] = 2131363887;
    arrayOfInt[2] = 2131362400;
    arrayOfInt[3] = 2131362384;
    a = arrayOfInt;
  }
  
  public CallerIdView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    setChildrenDrawingOrderEnabled(true);
  }
  
  protected int getChildDrawingOrder(int paramInt1, int paramInt2)
  {
    Object localObject = a;
    int i = localObject.length;
    if (paramInt2 < i)
    {
      int j = localObject[paramInt2];
      localObject = findViewById(j);
      j = indexOfChild((View)localObject);
      if (j >= 0) {
        return j;
      }
    }
    return super.getChildDrawingOrder(paramInt1, paramInt2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.CallerIdView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */