package com.truecaller.ui.components;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.app.a;
import android.support.v4.view.r;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.truecaller.R.styleable;
import com.truecaller.common.h.k;
import com.truecaller.util.at;
import com.truecaller.utils.ui.b;

public class FloatingActionButton
  extends FrameLayout
  implements View.OnClickListener, View.OnLongClickListener
{
  public boolean a = false;
  private i[] b;
  private FloatingActionButton.a c;
  private boolean d = false;
  private FloatingActionButton.d e;
  private ImageView f;
  private LinearLayout g;
  private View h;
  private int i;
  private int j;
  private boolean k = true;
  private boolean l;
  
  public FloatingActionButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramAttributeSet);
  }
  
  private void a(int paramInt1, int paramInt2)
  {
    int m = Build.VERSION.SDK_INT;
    int n = 21;
    if (m >= n)
    {
      b(paramInt1, paramInt2);
      return;
    }
    f();
  }
  
  private void a(int paramInt1, int paramInt2, Animator.AnimatorListener paramAnimatorListener)
  {
    int m = Build.VERSION.SDK_INT;
    int n = 21;
    if (m >= n)
    {
      b(paramInt1, paramInt2, paramAnimatorListener);
      return;
    }
    a(paramAnimatorListener);
  }
  
  private void a(Animator.AnimatorListener paramAnimatorListener)
  {
    h.setAlpha(1.0F);
    ViewPropertyAnimator localViewPropertyAnimator = h.animate().alpha(0.0F);
    AccelerateDecelerateInterpolator localAccelerateDecelerateInterpolator = new android/view/animation/AccelerateDecelerateInterpolator;
    localAccelerateDecelerateInterpolator.<init>();
    localViewPropertyAnimator.setInterpolator(localAccelerateDecelerateInterpolator).setDuration(300L).setListener(paramAnimatorListener);
  }
  
  private void a(AttributeSet paramAttributeSet)
  {
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    LayoutInflater.from((Context)localObject1).inflate(2131558635, this);
    boolean bool = false;
    setClipChildren(false);
    Object localObject2 = (ImageView)findViewById(2131363022);
    f = ((ImageView)localObject2);
    localObject2 = (LinearLayout)findViewById(2131363023);
    g = ((LinearLayout)localObject2);
    int m = 2131363021;
    float f1 = 1.834584E38F;
    localObject2 = findViewById(m);
    h = ((View)localObject2);
    if (paramAttributeSet != null)
    {
      localObject2 = R.styleable.FloatingActionButton;
      paramAttributeSet = ((Context)localObject1).obtainStyledAttributes(paramAttributeSet, (int[])localObject2);
      if (paramAttributeSet != null)
      {
        localObject2 = (FrameLayout.LayoutParams)f.getLayoutParams();
        int n = -1;
        int i1 = paramAttributeSet.getDimensionPixelSize(7, n);
        if (i1 != n) {
          bottomMargin = i1;
        }
        i1 = paramAttributeSet.getDimensionPixelSize(8, n);
        if (i1 != n) {
          ((FrameLayout.LayoutParams)localObject2).setMarginEnd(i1);
        }
        i1 = paramAttributeSet.getDimensionPixelSize(6, n);
        if (i1 != n) {
          width = i1;
        }
        i1 = paramAttributeSet.getDimensionPixelSize(5, n);
        if (i1 != n) {
          height = i1;
        }
        Object localObject3 = f;
        ((ImageView)localObject3).setLayoutParams((ViewGroup.LayoutParams)localObject2);
        localObject2 = (FrameLayout.LayoutParams)g.getLayoutParams();
        i1 = paramAttributeSet.getDimensionPixelSize(11, n);
        if (i1 != n) {
          bottomMargin = i1;
        }
        i1 = paramAttributeSet.getDimensionPixelSize(12, n);
        if (i1 != n) {
          ((FrameLayout.LayoutParams)localObject2).setMarginEnd(i1);
        }
        g.setLayoutParams((ViewGroup.LayoutParams)localObject2);
        m = paramAttributeSet.getResourceId(10, 2131558613);
        i = m;
        f1 = 1.3E-44F;
        localObject3 = getResources();
        n = 2131165754;
        i1 = ((Resources)localObject3).getDimensionPixelSize(n);
        m = paramAttributeSet.getDimensionPixelSize(9, i1);
        j = m;
        paramAttributeSet.recycle();
      }
    }
    paramAttributeSet = h;
    localObject2 = new com/truecaller/ui/components/-$$Lambda$FloatingActionButton$2fm0Hq9reveNfAmx3TlbeSIs0nU;
    ((-..Lambda.FloatingActionButton.2fm0Hq9reveNfAmx3TlbeSIs0nU)localObject2).<init>(this);
    paramAttributeSet.setOnClickListener((View.OnClickListener)localObject2);
    int i2 = k.d();
    int i4;
    if (i2 != 0)
    {
      paramAttributeSet = f;
      m = 1086324736;
      f1 = 6.0F;
      i4 = at.a((Context)localObject1, f1);
      f2 = i4;
      paramAttributeSet.setElevation(f2);
    }
    else
    {
      paramAttributeSet = new com/truecaller/ui/components/FloatingActionButton$d;
      paramAttributeSet.<init>((Context)localObject1);
      e = paramAttributeSet;
      i2 = 0;
      paramAttributeSet = null;
      for (;;)
      {
        i4 = getChildCount();
        if (i2 >= i4) {
          break;
        }
        localObject1 = getChildAt(i2);
        localObject2 = f;
        if (localObject1 == localObject2) {
          break;
        }
        i2 += 1;
      }
      localObject1 = e;
      localObject2 = new android/widget/FrameLayout$LayoutParams;
      ((FrameLayout.LayoutParams)localObject2).<init>(0, 0);
      addView((View)localObject1, i3, (ViewGroup.LayoutParams)localObject2);
    }
    paramAttributeSet = getResources().getConfiguration();
    int i3 = orientation;
    int i5 = 2;
    float f2 = 2.8E-45F;
    if (i3 == i5) {
      bool = true;
    }
    l = bool;
    f.setOnClickListener(this);
    f.setOnLongClickListener(this);
  }
  
  private void a(boolean paramBoolean, View paramView, int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null) {
      return;
    }
    localObject1 = paramView.animate();
    int m = 0;
    FloatingActionButton.1 local1 = null;
    int n = 1;
    if (paramBoolean)
    {
      arrayOfi1 = b;
      i1 = arrayOfi1.length - n;
      if (paramInt != i1) {
        break label61;
      }
    }
    else
    {
      if (paramInt != 0) {
        break label61;
      }
    }
    m = 1;
    label61:
    if (m != 0)
    {
      local1 = new com/truecaller/ui/components/FloatingActionButton$1;
      local1.<init>(this);
      ((ViewPropertyAnimator)localObject1).setListener(local1);
    }
    m = 2131363024;
    n = 2131363025;
    int i1 = 0;
    i[] arrayOfi1 = null;
    long l1 = 300L;
    float f1;
    Object localObject2;
    Object localObject3;
    if (paramBoolean)
    {
      paramBoolean = j;
      i[] arrayOfi2 = b;
      boolean bool1 = arrayOfi2.length - paramInt;
      f1 = paramBoolean * bool1;
      paramView.setTranslationY(f1);
      paramView.findViewById(n).setAlpha(0.0F);
      long l2 = paramInt * 300 / 8;
      ((ViewPropertyAnimator)localObject1).setStartDelay(l2);
      localObject2 = ((ViewPropertyAnimator)localObject1).translationY(0.0F);
      localObject3 = new android/view/animation/OvershootInterpolator;
      float f2 = 1.5F;
      ((OvershootInterpolator)localObject3).<init>(f2);
      ((ViewPropertyAnimator)localObject2).setInterpolator((TimeInterpolator)localObject3).setDuration(l1);
      localObject2 = paramView.findViewById(n).animate();
      ((ViewPropertyAnimator)localObject2).setStartDelay(l2);
      paramInt = 1065353216;
      float f3 = 1.0F;
      localObject2 = ((ViewPropertyAnimator)localObject2).alpha(f3);
      localObject1 = new android/view/animation/DecelerateInterpolator;
      ((DecelerateInterpolator)localObject1).<init>();
      localObject2 = ((ViewPropertyAnimator)localObject2).setInterpolator((TimeInterpolator)localObject1);
      ((ViewPropertyAnimator)localObject2).setDuration(l1);
      paramBoolean = l;
      if (paramBoolean)
      {
        localObject2 = paramView.findViewById(m);
        ((View)localObject2).setAlpha(0.0F);
        localObject2 = ((View)localObject2).animate();
        ((ViewPropertyAnimator)localObject2).setStartDelay(l2);
        localObject2 = ((ViewPropertyAnimator)localObject2).alpha(f3);
        paramView = new android/view/animation/DecelerateInterpolator;
        paramView.<init>();
        ((ViewPropertyAnimator)localObject2).setInterpolator(paramView).setDuration(l1);
      }
    }
    else
    {
      paramBoolean = b.length;
      boolean bool2 = paramInt + 1;
      long l3 = (paramBoolean - bool2) * 300 / 8;
      ((ViewPropertyAnimator)localObject1).setStartDelay(l3);
      paramBoolean = j;
      i[] arrayOfi3 = b;
      boolean bool3 = arrayOfi3.length - paramInt;
      f1 = paramBoolean * bool3;
      localObject2 = ((ViewPropertyAnimator)localObject1).translationY(f1);
      localObject3 = new android/view/animation/AccelerateInterpolator;
      ((AccelerateInterpolator)localObject3).<init>();
      ((ViewPropertyAnimator)localObject2).setInterpolator((TimeInterpolator)localObject3).setDuration(l1);
      localObject2 = paramView.findViewById(n).animate();
      paramInt = (b.length - bool2) * 300 / 8;
      l3 = paramInt;
      ((ViewPropertyAnimator)localObject2).setStartDelay(l3);
      localObject2 = ((ViewPropertyAnimator)localObject2).alpha(0.0F);
      localObject3 = new android/view/animation/AccelerateInterpolator;
      ((AccelerateInterpolator)localObject3).<init>();
      localObject2 = ((ViewPropertyAnimator)localObject2).setInterpolator((TimeInterpolator)localObject3);
      ((ViewPropertyAnimator)localObject2).setDuration(l1);
      paramBoolean = l;
      if (paramBoolean)
      {
        localObject2 = paramView.findViewById(m).animate();
        int i2 = (b.length - bool2) * 300 / 8;
        long l4 = i2;
        ((ViewPropertyAnimator)localObject2).setStartDelay(l4);
        localObject2 = ((ViewPropertyAnimator)localObject2).alpha(0.0F);
        paramView = new android/view/animation/AccelerateInterpolator;
        paramView.<init>();
        localObject2 = ((ViewPropertyAnimator)localObject2).setInterpolator(paramView);
        ((ViewPropertyAnimator)localObject2).setDuration(l1);
      }
    }
  }
  
  private void b(int paramInt1, int paramInt2)
  {
    View localView = h;
    int m = localView.getHeight();
    int n = h.getWidth();
    float f1 = Math.max(m, n);
    Animator localAnimator = ViewAnimationUtils.createCircularReveal(localView, paramInt1, paramInt2, 0.0F, f1);
    h.setVisibility(0);
    localAnimator.start();
  }
  
  private void b(int paramInt1, int paramInt2, Animator.AnimatorListener paramAnimatorListener)
  {
    View localView = h;
    int m = localView.getWidth();
    int n = h.getHeight();
    float f1 = Math.max(m, n);
    Animator localAnimator = ViewAnimationUtils.createCircularReveal(localView, paramInt1, paramInt2, f1, 0.0F);
    localAnimator.addListener(paramAnimatorListener);
    localAnimator.start();
  }
  
  private void b(boolean paramBoolean)
  {
    ImageView localImageView1 = f;
    int m = localImageView1.getLeft();
    int n = f.getRight();
    m = (m + n) / 2;
    ImageView localImageView2 = f;
    n = localImageView2.getTop();
    ImageView localImageView3 = f;
    int i1 = localImageView3.getBottom();
    n = (n + i1) / 2;
    if (paramBoolean)
    {
      a(m, n);
      return;
    }
    FloatingActionButton.2 local2 = new com/truecaller/ui/components/FloatingActionButton$2;
    local2.<init>(this);
    a(m, n, local2);
  }
  
  private void d()
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    localObject = (FrameLayout.LayoutParams)g.getLayoutParams();
    int m = j;
    i[] arrayOfi = b;
    int n = arrayOfi.length;
    m *= n;
    height = m;
    g.setLayoutParams((ViewGroup.LayoutParams)localObject);
    boolean bool = l;
    m = 0;
    float f1 = 0.0F;
    if (bool)
    {
      g.setTranslationY(0.0F);
      localObject = g;
      f1 = -f.getMeasuredWidth();
      ((LinearLayout)localObject).setTranslationX(f1);
      return;
    }
    localObject = g;
    float f2 = -f.getMeasuredHeight();
    ((LinearLayout)localObject).setTranslationY(f2);
    g.setTranslationX(0.0F);
  }
  
  private void e()
  {
    boolean bool = a;
    if (!bool) {
      return;
    }
    LinearLayout localLinearLayout = g;
    int m = 4;
    if (localLinearLayout != null)
    {
      localLinearLayout.removeAllViews();
      localLinearLayout = g;
      localLinearLayout.setVisibility(m);
    }
    h.animate().cancel();
    h.setVisibility(m);
    d = false;
    a = false;
  }
  
  private void f()
  {
    h.setAlpha(0.0F);
    h.setVisibility(0);
    ViewPropertyAnimator localViewPropertyAnimator = h.animate().alpha(1.0F);
    AccelerateDecelerateInterpolator localAccelerateDecelerateInterpolator = new android/view/animation/AccelerateDecelerateInterpolator;
    localAccelerateDecelerateInterpolator.<init>();
    localViewPropertyAnimator.setInterpolator(localAccelerateDecelerateInterpolator).setDuration(300L).setListener(null);
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    setVisibility(paramBoolean);
    e();
  }
  
  public final boolean a()
  {
    return a;
  }
  
  public final void b()
  {
    i[] arrayOfi = b;
    if (arrayOfi != null)
    {
      boolean bool1 = d;
      if (!bool1)
      {
        bool1 = true;
        d = bool1;
        LayoutInflater localLayoutInflater = LayoutInflater.from(getContext());
        Context localContext = getContext();
        int m = b.a(localContext, 2130969592);
        g.removeAllViews();
        int n = 0;
        for (;;)
        {
          Object localObject1 = b;
          int i1 = localObject1.length;
          if (n >= i1) {
            break;
          }
          localObject1 = localObject1[n];
          i1 = i;
          Object localObject2 = g;
          View localView = localLayoutInflater.inflate(i1, (ViewGroup)localObject2, false);
          int i2 = 2131363024;
          localObject2 = (ImageView)localView.findViewById(i2);
          Object localObject3 = (TextView)localView.findViewById(2131363025);
          int i3 = c;
          ((TextView)localObject3).setText(i3);
          int i4 = b;
          ((ImageView)localObject2).setImageResource(i4);
          localObject3 = e;
          if (localObject3 != null)
          {
            localObject3 = e;
            ((ImageView)localObject2).setBackground((Drawable)localObject3);
          }
          i4 = f;
          if (i4 != 0)
          {
            i4 = f;
            localObject3 = ColorStateList.valueOf(i4);
            r.a((View)localObject2, (ColorStateList)localObject3);
          }
          boolean bool2 = ((i)localObject1).a();
          int i5;
          if (bool2) {
            i5 = d;
          } else {
            i5 = m;
          }
          localObject3 = PorterDuff.Mode.SRC_IN;
          ((ImageView)localObject2).setColorFilter(i5, (PorterDuff.Mode)localObject3);
          localObject1 = new com/truecaller/ui/components/-$$Lambda$FloatingActionButton$aj5I9m9piJB5H6gfYXbB6RNvRyc;
          ((-..Lambda.FloatingActionButton.aj5I9m9piJB5H6gfYXbB6RNvRyc)localObject1).<init>(this, n);
          localView.setOnClickListener((View.OnClickListener)localObject1);
          localObject1 = g;
          ((LinearLayout)localObject1).addView(localView);
          a(bool1, localView, n);
          n += 1;
        }
        d();
        g.setVisibility(0);
        b(bool1);
        a = bool1;
        return;
      }
    }
  }
  
  public final void c()
  {
    boolean bool = a;
    if (bool)
    {
      i[] arrayOfi = b;
      if (arrayOfi != null)
      {
        bool = d;
        if (!bool)
        {
          d = true;
          bool = false;
          arrayOfi = null;
          int m = 0;
          Object localObject1 = null;
          for (;;)
          {
            localObject2 = g;
            int n = ((LinearLayout)localObject2).getChildCount();
            if (m >= n) {
              break;
            }
            localObject2 = g.getChildAt(m);
            a(false, (View)localObject2, m);
            m += 1;
          }
          localObject1 = new com/truecaller/ui/components/-$$Lambda$FloatingActionButton$uYf5U7YFH4WDRdMOVGGuEiZzQkM;
          ((-..Lambda.FloatingActionButton.uYf5U7YFH4WDRdMOVGGuEiZzQkM)localObject1).<init>(this);
          long l1 = b.length * 300 / 8 + 300;
          postDelayed((Runnable)localObject1, l1);
          b(false);
          localObject1 = f.animate().rotation(0.0F);
          Object localObject2 = new android/view/animation/OvershootInterpolator;
          ((OvershootInterpolator)localObject2).<init>();
          ((ViewPropertyAnimator)localObject1).setInterpolator((TimeInterpolator)localObject2).setDuration(300L);
          a = false;
          return;
        }
      }
    }
  }
  
  public View getButtonView()
  {
    return f;
  }
  
  public void onClick(View paramView)
  {
    boolean bool1 = k;
    if (bool1)
    {
      paramView = b;
      if (paramView != null)
      {
        int m = paramView.length;
        if (m != 0)
        {
          boolean bool2 = a;
          if (bool2)
          {
            c();
            return;
          }
          b();
          return;
        }
      }
    }
    paramView = c;
    if (paramView != null)
    {
      paramView.a();
      return;
    }
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    int m = orientation;
    int n = 2;
    if (m == n)
    {
      m = 1;
    }
    else
    {
      m = 0;
      paramConfiguration = null;
    }
    l = m;
    boolean bool = a;
    if (bool) {
      d();
    }
    requestLayout();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    FloatingActionButton.d locald1 = e;
    if (locald1 != null)
    {
      paramBoolean = a;
      FloatingActionButton.d locald2 = e;
      ImageView localImageView1 = f;
      paramInt2 = localImageView1.getLeft() - paramBoolean;
      ImageView localImageView2 = f;
      paramInt3 = localImageView2.getTop() - paramBoolean;
      ImageView localImageView3 = f;
      paramInt4 = localImageView3.getRight() + paramBoolean;
      ImageView localImageView4 = f;
      int m = localImageView4.getBottom() + paramBoolean;
      locald2.layout(paramInt2, paramInt3, paramInt4, m);
    }
  }
  
  public boolean onLongClick(View paramView)
  {
    boolean bool = a;
    if (!bool)
    {
      bool = d;
      if (!bool)
      {
        paramView = c;
        if (paramView != null) {
          paramView.p();
        }
      }
    }
    return true;
  }
  
  public void setBackgroundColor(int paramInt)
  {
    Drawable localDrawable = a.a(getContext(), 2131233762);
    PorterDuff.Mode localMode = PorterDuff.Mode.MULTIPLY;
    localDrawable.setColorFilter(paramInt, localMode);
    at.a(f, localDrawable);
  }
  
  public void setDrawable(Drawable paramDrawable)
  {
    ImageView localImageView = f;
    if (localImageView != null) {
      localImageView.setImageDrawable(paramDrawable);
    }
  }
  
  public void setFabActionListener(FloatingActionButton.a parama)
  {
    c = parama;
  }
  
  public void setMenuItemLayout(int paramInt)
  {
    i = paramInt;
  }
  
  public void setMenuItems(i[] paramArrayOfi)
  {
    b = paramArrayOfi;
  }
  
  public void setOnClickListener(View.OnClickListener paramOnClickListener) {}
  
  public void setOpenMenuOnClick(boolean paramBoolean)
  {
    k = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.FloatingActionButton
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */