package com.truecaller.ui.components;

import android.text.Editable;
import android.text.TextWatcher;

final class EditBase$1
  implements TextWatcher
{
  EditBase$1(EditBase paramEditBase) {}
  
  public final void afterTextChanged(Editable paramEditable) {}
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    EditBase.a(a);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.EditBase.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */