package com.truecaller.ui.components;

import android.content.Context;
import android.content.res.Resources;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.common.h.j;
import java.util.Arrays;
import java.util.HashMap;

public final class AfterCallBusinessView
  extends ConstraintLayout
{
  private String k;
  private Long l;
  private String m;
  private a n;
  private HashMap o;
  
  public AfterCallBusinessView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private AfterCallBusinessView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, (byte)0);
    paramContext = LayoutInflater.from(paramContext);
    paramAttributeSet = this;
    paramAttributeSet = (ViewGroup)this;
    paramContext.inflate(2131559147, paramAttributeSet);
    int i = R.id.yesButton;
    paramContext = (CardView)d(i);
    paramAttributeSet = new com/truecaller/ui/components/AfterCallBusinessView$1;
    paramAttributeSet.<init>(this);
    paramAttributeSet = (View.OnClickListener)paramAttributeSet;
    paramContext.setOnClickListener(paramAttributeSet);
    i = R.id.noButton;
    paramContext = (CardView)d(i);
    paramAttributeSet = new com/truecaller/ui/components/AfterCallBusinessView$2;
    paramAttributeSet.<init>(this);
    paramAttributeSet = (View.OnClickListener)paramAttributeSet;
    paramContext.setOnClickListener(paramAttributeSet);
  }
  
  private View d(int paramInt)
  {
    Object localObject1 = o;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      o = ((HashMap)localObject1);
    }
    localObject1 = o;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = o;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void b()
  {
    Object localObject1 = m;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = l;
    if (localObject2 != null)
    {
      long l1 = ((Long)localObject2).longValue();
      String str1 = k;
      if (str1 == null) {
        return;
      }
      try
      {
        int i = R.id.infoText;
        Object localObject3 = d(i);
        localObject3 = (TextView)localObject3;
        Object localObject4 = "infoText";
        k.a(localObject3, (String)localObject4);
        localObject4 = getResources();
        int j = 2;
        Object[] arrayOfObject1 = new Object[j];
        arrayOfObject1[0] = localObject1;
        int i1 = 1;
        Object[] arrayOfObject2 = new Object[i1];
        localObject2 = j.a(l1);
        arrayOfObject2[0] = localObject2;
        localObject2 = Arrays.copyOf(arrayOfObject2, i1);
        localObject2 = String.format(str1, (Object[])localObject2);
        String str2 = "java.lang.String.format(this, *args)";
        k.a(localObject2, str2);
        int i2 = 2131886095;
        arrayOfObject1[i1] = localObject2;
        localObject1 = ((Resources)localObject4).getString(i2, arrayOfObject1);
        localObject1 = (CharSequence)localObject1;
        ((TextView)localObject3).setText((CharSequence)localObject1);
        return;
      }
      catch (NullPointerException localNullPointerException)
      {
        localObject1 = n;
        if (localObject1 != null)
        {
          ((a)localObject1).a();
          return;
        }
        return;
      }
    }
  }
  
  public final void c(int paramInt)
  {
    int i = R.id.container;
    ((ConstraintLayout)d(i)).setBackgroundColor(paramInt);
    i = R.id.innerNoButton;
    ((CardView)d(i)).setCardBackgroundColor(paramInt);
    i = R.id.yesText;
    ((TextView)d(i)).setTextColor(paramInt);
  }
  
  public final Long getEventTimestamp()
  {
    return l;
  }
  
  public final String getLegendFormatString()
  {
    return k;
  }
  
  public final a getListener()
  {
    return n;
  }
  
  public final String getPhoneNumber()
  {
    return m;
  }
  
  public final void setEventTimestamp(Long paramLong)
  {
    l = paramLong;
    b();
  }
  
  public final void setLegendFormatString(String paramString)
  {
    k = paramString;
    b();
  }
  
  public final void setListener(a parama)
  {
    n = parama;
  }
  
  public final void setPhoneNumber(String paramString)
  {
    m = paramString;
    b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.AfterCallBusinessView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */