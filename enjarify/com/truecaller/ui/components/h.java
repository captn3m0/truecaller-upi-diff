package com.truecaller.ui.components;

import android.content.Context;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.a;
import android.support.v4.widget.f;
import android.support.v4.widget.m;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.callhistory.z;
import com.truecaller.calling.ah;
import com.truecaller.calling.dialer.ax;
import com.truecaller.calling.recorder.CallRecordingManager;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.am;
import com.truecaller.data.entity.CallRecording;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.Number;
import com.truecaller.data.entity.g;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.ae;
import com.truecaller.util.af;
import com.truecaller.util.at;
import com.truecaller.util.cf;
import com.truecaller.utils.n;
import com.truecaller.utils.ui.b;

public final class h
  extends f
{
  private final LayoutInflater a;
  private final Drawable b;
  private final Drawable c;
  private final Drawable d;
  private final Drawable e;
  private final Drawable f;
  private final Drawable g;
  private final Drawable h;
  private final Drawable i;
  private final Drawable j;
  private final Drawable k;
  private final Drawable l;
  private final Drawable m;
  private final int n;
  private final int o;
  private final ColorStateList p;
  private final ColorStateList q;
  private final boolean r;
  private final cf s;
  private final CallRecordingManager t;
  private final n u;
  private final ax v;
  private final g w;
  private final af x;
  private final ae y;
  
  public h(Context paramContext, CallRecordingManager paramCallRecordingManager)
  {
    super(paramContext, null, false);
    Object localObject = LayoutInflater.from(paramContext);
    a = ((LayoutInflater)localObject);
    t = paramCallRecordingManager;
    paramCallRecordingManager = ((bk)paramContext.getApplicationContext()).a();
    localObject = paramCallRecordingManager.ax();
    y = ((ae)localObject);
    boolean bool = paramCallRecordingManager.U().j();
    r = bool;
    localObject = paramCallRecordingManager.bz();
    s = ((cf)localObject);
    localObject = paramCallRecordingManager.r();
    u = ((n)localObject);
    localObject = paramCallRecordingManager.s();
    v = ((ax)localObject);
    localObject = paramCallRecordingManager.aa();
    w = ((g)localObject);
    paramCallRecordingManager = paramCallRecordingManager.aJ();
    x = paramCallRecordingManager;
    int i1 = b.a(paramContext, 2130969585);
    n = i1;
    i1 = b.a(paramContext, 2130969591);
    o = i1;
    paramCallRecordingManager = b.b(paramContext, 2130969183);
    p = paramCallRecordingManager;
    paramCallRecordingManager = b.b(paramContext, 2130968889);
    q = paramCallRecordingManager;
    paramCallRecordingManager = a.e(at.a(paramContext, 2131234205)).mutate();
    b = paramCallRecordingManager;
    paramCallRecordingManager = b;
    localObject = p;
    a.a(paramCallRecordingManager, (ColorStateList)localObject);
    i1 = 2131234275;
    localObject = a.e(at.a(paramContext, i1)).mutate();
    e = ((Drawable)localObject);
    localObject = e;
    ColorStateList localColorStateList = q;
    a.a((Drawable)localObject, localColorStateList);
    paramCallRecordingManager = a.e(at.a(paramContext, i1)).mutate();
    f = paramCallRecordingManager;
    paramCallRecordingManager = f;
    localObject = q;
    a.a(paramCallRecordingManager, (ColorStateList)localObject);
    i1 = 2131234352;
    localObject = a.e(at.a(paramContext, i1)).mutate();
    c = ((Drawable)localObject);
    localObject = c;
    localColorStateList = p;
    a.a((Drawable)localObject, localColorStateList);
    paramCallRecordingManager = a.e(at.a(paramContext, i1)).mutate();
    d = paramCallRecordingManager;
    paramCallRecordingManager = d;
    localObject = p;
    a.a(paramCallRecordingManager, (ColorStateList)localObject);
    paramCallRecordingManager = a.e(at.a(paramContext, 2131233892)).mutate();
    g = paramCallRecordingManager;
    paramCallRecordingManager = g;
    localObject = q;
    a.a(paramCallRecordingManager, (ColorStateList)localObject);
    paramCallRecordingManager = a.e(at.a(paramContext, 2131234295)).mutate();
    h = paramCallRecordingManager;
    paramCallRecordingManager = h;
    localObject = q;
    a.a(paramCallRecordingManager, (ColorStateList)localObject);
    i1 = 2131234549;
    localObject = a.e(at.a(paramContext, i1)).mutate();
    i = ((Drawable)localObject);
    localObject = i;
    localColorStateList = p;
    a.a((Drawable)localObject, localColorStateList);
    paramCallRecordingManager = a.e(at.a(paramContext, i1)).mutate();
    j = paramCallRecordingManager;
    paramCallRecordingManager = j;
    localObject = q;
    a.a(paramCallRecordingManager, (ColorStateList)localObject);
    i1 = 2131234550;
    localObject = a.e(at.a(paramContext, i1)).mutate();
    k = ((Drawable)localObject);
    localObject = k;
    localColorStateList = p;
    a.a((Drawable)localObject, localColorStateList);
    paramCallRecordingManager = a.e(at.a(paramContext, i1)).mutate();
    l = paramCallRecordingManager;
    paramCallRecordingManager = l;
    localObject = q;
    a.a(paramCallRecordingManager, (ColorStateList)localObject);
    paramContext = a.e(at.a(paramContext, 2131234658)).mutate();
    m = paramContext;
    paramContext = m;
    paramCallRecordingManager = p;
    a.a(paramContext, paramCallRecordingManager);
  }
  
  public final void bindView(View paramView, Context paramContext, Cursor paramCursor)
  {
    h localh = this;
    Object localObject1 = paramView;
    Object localObject2 = paramCursor;
    localObject2 = ((z)paramCursor).d();
    Object localObject3 = paramView.getTag();
    if (localObject3 == null)
    {
      localObject3 = new com/truecaller/ui/components/h$a;
      ((h.a)localObject3).<init>(paramView);
      paramView.setTag(localObject3);
    }
    else
    {
      localObject3 = (h.a)localObject3;
    }
    int i1 = 8;
    if (localObject2 == null)
    {
      a.setText("");
      b.setText("");
      e.setVisibility(i1);
      d.setImageDrawable(null);
      c.setImageDrawable(null);
      return;
    }
    Object localObject4 = s;
    localObject4 = com.truecaller.calling.dialer.h.a((HistoryEvent)localObject2, (cf)localObject4);
    int i2 = p;
    int i3 = 2;
    int i4 = 3;
    int i5 = 1;
    if ((i2 != i5) && (i2 != i3) && (i2 != i4)) {
      i2 = 0;
    } else {
      i2 = 1;
    }
    TextView localTextView = a;
    Object localObject5 = f;
    if (localObject5 != null) {
      localObject5 = ((Contact)localObject5).q();
    } else {
      localObject5 = c;
    }
    at.a(localTextView, (CharSequence)localObject5);
    localTextView = b;
    localObject5 = f;
    Object localObject6 = c;
    boolean bool1 = ab.a((String)localObject6);
    if (!bool1)
    {
      localObject6 = b;
      bool1 = am.c((CharSequence)localObject6);
      if (bool1)
      {
        localObject6 = b;
        break label293;
      }
    }
    localObject6 = c;
    label293:
    Object localObject7;
    if (localObject6 != null)
    {
      localObject7 = u;
      localObject7 = ((com.truecaller.calling.dialer.h)localObject4).a((n)localObject7);
      if (localObject7 == null)
      {
        localObject7 = u;
        localObject8 = v;
        localObject8 = com.truecaller.calling.r.a((Contact)localObject5, (String)localObject6, (n)localObject7, (ax)localObject8);
        localObject7 = localObject8;
      }
      if (localObject7 == null)
      {
        localObject8 = w;
        localObject5 = new String[i5];
        localObject5[0] = localObject6;
        localObject8 = ((g)localObject8).a((String[])localObject5);
        if (localObject8 != null)
        {
          localObject5 = u;
          localObject6 = v;
          localObject8 = ah.a((Number)localObject8, (n)localObject5, (ax)localObject6);
          localObject7 = localObject8;
        }
      }
    }
    else
    {
      localObject7 = null;
    }
    Object localObject8 = new java/lang/StringBuilder;
    ((StringBuilder)localObject8).<init>();
    ((StringBuilder)localObject8).append((CharSequence)localObject7);
    ((StringBuilder)localObject8).append(", ");
    localObject5 = x;
    long l1 = h;
    localObject5 = ((af)localObject5).g(l1);
    ((StringBuilder)localObject8).append((CharSequence)localObject5);
    long l2 = i;
    long l3 = 0L;
    boolean bool2 = l2 < l3;
    if (bool2)
    {
      ((StringBuilder)localObject8).append(" (");
      localObject7 = x;
      localObject5 = ((af)localObject7).i(l2);
      ((StringBuilder)localObject8).append((String)localObject5);
      localObject5 = ")";
      ((StringBuilder)localObject8).append((String)localObject5);
    }
    localObject8 = ((StringBuilder)localObject8).toString();
    at.b(localTextView, (CharSequence)localObject8);
    m.a(b, 0, 0);
    localObject8 = b;
    localObject1 = paramView.getContext();
    float f1 = 4.0F;
    int i6 = at.a((Context)localObject1, f1);
    android.support.v4.view.r.a((View)localObject8, i6, 0, 0, 0);
    boolean bool3 = r;
    if (bool3)
    {
      localObject1 = j;
      localObject8 = y;
      localObject1 = ((ae)localObject8).a((String)localObject1);
      if (localObject1 != null)
      {
        i1 = a;
        if (i1 != 0)
        {
          i1 = a;
          if (i1 != i5) {}
        }
        else
        {
          if (i2 == 0)
          {
            i1 = o;
            if (i1 != i4)
            {
              i1 = 0;
              localObject8 = null;
              break label719;
            }
          }
          i1 = 1;
          label719:
          localTextView = b;
          int i7 = a;
          if (i7 == 0)
          {
            if (i1 != 0) {
              localObject1 = j;
            } else {
              localObject1 = i;
            }
          }
          else if (i1 != 0) {
            localObject1 = l;
          } else {
            localObject1 = k;
          }
          m.a(localTextView, (Drawable)localObject1, null);
          localObject1 = b;
          android.support.v4.view.r.a((View)localObject1, 0, 0, 0, 0);
        }
      }
    }
    localObject1 = a;
    if (i2 != 0) {
      i1 = n;
    } else {
      i1 = o;
    }
    at.b((TextView)localObject1, i1);
    localObject1 = c;
    i1 = p;
    i2 = o;
    if (i1 == i5)
    {
      localObject8 = g;
    }
    else if (i1 == i4)
    {
      localObject8 = h;
    }
    else if (i2 == i5)
    {
      localObject8 = b;
    }
    else if (i2 == i3)
    {
      localObject8 = c;
    }
    else if (i2 == i4)
    {
      localObject8 = e;
    }
    else
    {
      i1 = 0;
      localObject8 = null;
    }
    ((ImageView)localObject1).setImageDrawable((Drawable)localObject8);
    boolean bool4 = a;
    if (bool4)
    {
      localObject1 = d;
      localObject8 = m;
      ((ImageView)localObject1).setImageDrawable((Drawable)localObject8);
    }
    else
    {
      localObject1 = d;
      ((ImageView)localObject1).setImageDrawable(null);
    }
    localObject1 = m;
    if (localObject1 != null)
    {
      e.setVisibility(0);
      localObject2 = e;
      localObject3 = new com/truecaller/ui/components/-$$Lambda$h$zJW6KVecy5oa6oA4ESRmimoIi6M;
      ((-..Lambda.h.zJW6KVecy5oa6oA4ESRmimoIi6M)localObject3).<init>(localh, (CallRecording)localObject1);
      ((ImageView)localObject2).setOnClickListener((View.OnClickListener)localObject3);
      return;
    }
    e.setOnClickListener(null);
    e.setVisibility(8);
  }
  
  public final FilterQueryProvider getFilterQueryProvider()
  {
    return null;
  }
  
  public final View newView(Context paramContext, Cursor paramCursor, ViewGroup paramViewGroup)
  {
    return a.inflate(2131559007, paramViewGroup, false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */