package com.truecaller.ui.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.widget.RelativeLayout;

public class SnappingRelativeLayout
  extends RelativeLayout
{
  private SnappingRelativeLayout.a a;
  private float b;
  private float c;
  private final float d;
  private final float e;
  
  public SnappingRelativeLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    float f = ViewConfiguration.get(paramContext).getScaledPagingTouchSlop() / 2;
    d = f;
    f = d * 2.0F;
    e = f;
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    SnappingRelativeLayout.a locala = a;
    if (locala == null) {
      return false;
    }
    int i = paramMotionEvent.getAction();
    float f1;
    float f2;
    switch (i)
    {
    default: 
      break;
    case 2: 
      f1 = paramMotionEvent.getX();
      f2 = paramMotionEvent.getY();
      f1 = Math.abs(b - f1);
      float f3 = c - f2;
      f2 = Math.abs(f3);
      float f4 = e;
      boolean bool1 = f1 < f4;
      if (bool1) {
        return false;
      }
      f1 = d;
      boolean bool2 = f2 < f1;
      if (bool2)
      {
        bool2 = false;
        f2 = 0.0F;
        paramMotionEvent = null;
        bool1 = f3 < 0.0F;
        if (bool1)
        {
          locala = a;
          locala.b();
        }
        bool2 = f3 < 0.0F;
        if (bool2)
        {
          paramMotionEvent = a;
          paramMotionEvent.a();
        }
      }
      break;
    case 0: 
      f1 = paramMotionEvent.getX();
      b = f1;
      f2 = paramMotionEvent.getY();
      c = f2;
    }
    return false;
  }
  
  public void setOnSnapListener(SnappingRelativeLayout.a parama)
  {
    a = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.SnappingRelativeLayout
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */