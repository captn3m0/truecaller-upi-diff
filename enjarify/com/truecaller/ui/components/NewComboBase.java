package com.truecaller.ui.components;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog.Builder;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import com.truecaller.R.styleable;
import com.truecaller.util.at;
import com.truecaller.utils.ui.b;
import java.util.List;

public class NewComboBase
  extends LinearLayout
  implements View.OnClickListener
{
  private String a;
  private n b;
  private List c;
  private NewComboBase.a d;
  
  public NewComboBase(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    LinearLayout.LayoutParams localLayoutParams = new android/widget/LinearLayout$LayoutParams;
    localLayoutParams.<init>(-1, -2);
    Object localObject = at.b(getContext(), 2131558538);
    addView((View)localObject, localLayoutParams);
    setOnClickListener(this);
    boolean bool1 = true;
    setClickable(bool1);
    boolean bool2 = isEnabled();
    setEnabled(bool2);
    localObject = getContext();
    int j = 2130969528;
    localObject = b.a((Context)localObject, 2131234007, j);
    int k = 2131362920;
    ImageView localImageView = (ImageView)findViewById(k);
    localImageView.setImageDrawable((Drawable)localObject);
    localObject = R.styleable.ComboBase;
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject);
    if (paramContext != null)
    {
      int m = 0;
      paramAttributeSet = null;
      for (;;)
      {
        int i = paramContext.getIndexCount();
        if (m >= i) {
          break;
        }
        i = paramContext.getIndex(m);
        if (i == 0)
        {
          localObject = paramContext.getString(i);
          localObject = n.a(bool1, (String)localObject);
          setTitle((String)localObject);
        }
        m += 1;
      }
      paramContext.recycle();
    }
  }
  
  public n getSelection()
  {
    return b;
  }
  
  public void onClick(View paramView)
  {
    paramView = new android/support/v7/app/AlertDialog$Builder;
    Object localObject1 = getContext();
    paramView.<init>((Context)localObject1);
    localObject1 = a;
    paramView = paramView.setTitle((CharSequence)localObject1);
    localObject1 = new com/truecaller/ui/components/f;
    Object localObject2 = c;
    ((f)localObject1).<init>((List)localObject2);
    localObject2 = new com/truecaller/ui/components/-$$Lambda$NewComboBase$gyDDSs9_BJqe04520jHejQeJEqU;
    ((-..Lambda.NewComboBase.gyDDSs9_BJqe04520jHejQeJEqU)localObject2).<init>(this);
    paramView.setAdapter((ListAdapter)localObject1, (DialogInterface.OnClickListener)localObject2).show();
  }
  
  public void setData(List paramList)
  {
    c = paramList;
    paramList = c;
    if (paramList != null)
    {
      int i = paramList.size();
      if (i > 0)
      {
        paramList = (n)c.get(0);
        setSelection(paramList);
      }
    }
  }
  
  public void setObserver(NewComboBase.a parama)
  {
    d = parama;
  }
  
  public void setSelection(n paramn)
  {
    b = paramn;
    if (paramn == null)
    {
      paramn = "";
    }
    else
    {
      paramn = b;
      Context localContext = getContext();
      paramn = paramn.a(localContext);
    }
    at.a(this, 2131363653, paramn);
  }
  
  public void setTitle(String paramString)
  {
    paramString = n.a(true, paramString);
    a = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.NewComboBase
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */