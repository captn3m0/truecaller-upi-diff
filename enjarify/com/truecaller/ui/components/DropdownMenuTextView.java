package com.truecaller.ui.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View.OnClickListener;
import com.truecaller.R.styleable;

public class DropdownMenuTextView
  extends AppCompatTextView
{
  private PopupMenu b;
  private PopupMenu.OnMenuItemClickListener c;
  
  public DropdownMenuTextView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    Object localObject1 = new android/support/v7/widget/PopupMenu;
    ((PopupMenu)localObject1).<init>(paramContext, this);
    b = ((PopupMenu)localObject1);
    localObject1 = b;
    Object localObject2 = new com/truecaller/ui/components/DropdownMenuTextView$1;
    ((DropdownMenuTextView.1)localObject2).<init>(this);
    ((PopupMenu)localObject1).setOnMenuItemClickListener((PopupMenu.OnMenuItemClickListener)localObject2);
    localObject1 = new com/truecaller/ui/components/DropdownMenuTextView$2;
    ((DropdownMenuTextView.2)localObject1).<init>(this);
    setOnClickListener((View.OnClickListener)localObject1);
    localObject1 = R.styleable.DropdownMenuTextView;
    localObject2 = null;
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject1, 0, 0);
    int i = paramContext.getResourceId(0, 0);
    if (i != 0)
    {
      localObject1 = b.getMenuInflater();
      localObject2 = b.getMenu();
      ((MenuInflater)localObject1).inflate(i, (Menu)localObject2);
    }
    paramContext.recycle();
  }
  
  public void setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener paramOnMenuItemClickListener)
  {
    c = paramOnMenuItemClickListener;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.DropdownMenuTextView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */