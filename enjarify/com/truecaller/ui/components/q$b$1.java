package com.truecaller.ui.components;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.truecaller.ads.AdLayoutType;
import com.truecaller.ads.CustomTemplate;
import com.truecaller.ads.h;
import com.truecaller.ads.k;
import com.truecaller.ads.provider.f;
import com.truecaller.ads.provider.holders.AdHolderType;
import com.truecaller.ads.provider.holders.e;

final class q$b$1
  extends h
{
  q$b$1(q.b paramb, k paramk) {}
  
  public final void a()
  {
    Object localObject1 = b.c;
    if (localObject1 == this)
    {
      localObject1 = b;
      Object localObject2 = null;
      c = null;
      localObject1 = q.g(e);
      Object localObject3 = a;
      ((f)localObject1).b((k)localObject3, this);
      localObject1 = b;
      localObject3 = a;
      Object localObject4 = q.a(e);
      if (localObject4 != null)
      {
        localObject4 = q.g(e);
        localObject3 = ((f)localObject4).a((k)localObject3, 0);
        d = ((e)localObject3);
        localObject3 = d;
        if (localObject3 != null)
        {
          localObject3 = q.i(e);
          if (localObject3 != null)
          {
            q.a(e).a();
            localObject4 = b;
            if (localObject4 != null)
            {
              localObject4 = b;
              ((q.b)localObject1).b((View)localObject4);
              b = null;
            }
            localObject2 = AdLayoutType.LARGE;
            localObject4 = d;
            localObject2 = com.truecaller.ads.d.a((Activity)localObject3, (com.truecaller.ads.a)localObject2, (e)localObject4);
            localObject3 = AdHolderType.NATIVE_AD;
            localObject4 = d.a();
            if (localObject3 != localObject4)
            {
              localObject3 = d;
              boolean bool = com.truecaller.ads.c.a.a((e)localObject3);
              if (!bool)
              {
                b = ((View)localObject2);
                localObject2 = AdHolderType.CUSTOM_AD;
                localObject3 = d.a();
                if (localObject2 != localObject3) {
                  break label353;
                }
                localObject2 = (NativeCustomTemplateAd)((com.truecaller.ads.provider.holders.d)d).g();
                localObject3 = VIDEO_WITH_FALLBACK_IMAGEtemplateId;
                localObject4 = ((NativeCustomTemplateAd)localObject2).getCustomTemplateId();
                bool = ((String)localObject3).equals(localObject4);
                if (!bool) {
                  break label353;
                }
                localObject2 = ((NativeCustomTemplateAd)localObject2).getVideoController();
                bool = ((VideoController)localObject2).f();
                if (!bool) {
                  break label353;
                }
                bool = true;
                ((VideoController)localObject2).a(bool);
                break label353;
              }
            }
            localObject3 = LayoutInflater.from(e.getContext());
            int i = 2131558490;
            q localq = e;
            localObject3 = (ViewGroup)((LayoutInflater)localObject3).inflate(i, localq, false);
            ((ViewGroup)localObject3).addView((View)localObject2);
            b = ((View)localObject3);
            label353:
            localObject2 = b;
            ((q.b)localObject1).a((View)localObject2);
          }
        }
      }
    }
  }
  
  public final void a(int paramInt)
  {
    Object localObject = b.c;
    if (localObject == this)
    {
      localObject = b;
      c = null;
      localObject = q.g(e);
      k localk = a;
      ((f)localObject).b(localk, this);
      localObject = b.e.b;
      if (localObject != null) {
        ((q.b)localObject).c();
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.q.b.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */