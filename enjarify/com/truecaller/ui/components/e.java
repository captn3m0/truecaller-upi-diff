package com.truecaller.ui.components;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.aj;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.analytics.bb;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.o;
import com.truecaller.data.entity.Address;
import com.truecaller.data.entity.Contact;
import com.truecaller.filters.g;
import com.truecaller.i.c;
import com.truecaller.log.d;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.AfterClipboardSearchActivity;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.util.at;
import com.truecaller.util.bj;
import com.truecaller.util.co;
import com.truecaller.util.w;

public final class e
  extends FloatingWindow
{
  public Contact a;
  public String b;
  public g c;
  private AvatarView k;
  private TextView l;
  private TextView m;
  private View n;
  private View o;
  private View p;
  private ImageView q;
  private final f r;
  private final c s;
  
  public e(Context paramContext)
  {
    super(paramContext, View.class);
    paramContext = ((bk)paramContext.getApplicationContext()).a();
    f localf = paramContext.f();
    r = localf;
    paramContext = paramContext.D();
    s = paramContext;
  }
  
  private void a(Intent paramIntent)
  {
    int i = 268468224;
    try
    {
      paramIntent.addFlags(i);
      Context localContext = d;
      localContext.startActivity(paramIntent);
      return;
    }
    finally
    {
      d.a(finally;
    }
  }
  
  private void d()
  {
    Object localObject1 = s;
    Object localObject2 = "clipboardSearchHaveAskedOnDismiss";
    boolean bool = ((c)localObject1).b((String)localObject2);
    if (!bool)
    {
      localObject1 = new android/content/Intent;
      localObject2 = d;
      Class localClass = AfterClipboardSearchActivity.class;
      ((Intent)localObject1).<init>((Context)localObject2, localClass);
      a((Intent)localObject1);
    }
  }
  
  protected final int a()
  {
    return Settings.c("clipboardSearchLastYPosition");
  }
  
  protected final void a(int paramInt)
  {
    long l1 = paramInt;
    Settings.a("clipboardSearchLastYPosition", l1);
  }
  
  protected final void a(View paramView)
  {
    Object localObject = (AvatarView)paramView.findViewById(2131362407);
    k = ((AvatarView)localObject);
    localObject = (TextView)paramView.findViewById(2131362398);
    l = ((TextView)localObject);
    localObject = (TextView)paramView.findViewById(2131362397);
    m = ((TextView)localObject);
    localObject = paramView.findViewById(2131364253);
    n = ((View)localObject);
    localObject = paramView.findViewById(2131364256);
    o = ((View)localObject);
    localObject = paramView.findViewById(2131364255);
    p = ((View)localObject);
    localObject = (ImageView)paramView.findViewById(2131364254);
    q = ((ImageView)localObject);
    localObject = q;
    Context localContext = d;
    int i = 2130969592;
    int j = com.truecaller.utils.ui.b.a(localContext, i);
    com.truecaller.utils.ui.b.a((ImageView)localObject, j);
    paramView = (ImageView)paramView.findViewById(2131362062);
    localObject = d;
    boolean bool = co.c((Context)localObject);
    if (bool)
    {
      int i1 = 2131234510;
      paramView.setImageResource(i1);
    }
    n.setOnClickListener(this);
    o.setOnClickListener(this);
    p.setOnClickListener(this);
    q.setOnClickListener(this);
  }
  
  protected final void a(FloatingWindow.DismissCause paramDismissCause)
  {
    super.a(paramDismissCause);
    FloatingWindow.DismissCause localDismissCause = FloatingWindow.DismissCause.MANUAL;
    if (paramDismissCause == localDismissCause) {
      d();
    }
  }
  
  public final void a(String paramString, Contact paramContact, g paramg)
  {
    a = paramContact;
    b = paramString;
    c = paramg;
    paramString = l;
    Object localObject = paramContact.u();
    at.b(paramString, (CharSequence)localObject);
    paramString = paramContact.a();
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (bool1)
    {
      paramString = paramContact.g();
      if (paramString != null)
      {
        localObject = paramString.getCountryCode();
        if (localObject != null)
        {
          localObject = m;
          paramString = paramString.getCountryName();
          at.b((TextView)localObject, paramString);
        }
      }
    }
    else
    {
      paramString = m;
      localObject = paramContact.a();
      at.b(paramString, (CharSequence)localObject);
    }
    bool1 = w.a(paramContact, paramg);
    if (bool1)
    {
      k.a();
      return;
    }
    paramString = new com/truecaller/ui/components/b;
    paramg = paramContact.a(true);
    localObject = paramContact.a(false);
    boolean bool2 = paramContact.a(32);
    boolean bool3 = paramContact.a(4);
    paramString.<init>(paramg, (Uri)localObject, bool2, bool3);
    k.a(paramString);
  }
  
  protected final void b()
  {
    a(6000L);
  }
  
  public final void onClick(View paramView)
  {
    Object localObject1 = q;
    if (paramView == localObject1)
    {
      d();
    }
    else
    {
      localObject1 = s;
      String str1 = "lastCopied";
      ((c)localObject1).d(str1);
      localObject1 = n;
      boolean bool1 = true;
      Object localObject2;
      if (paramView == localObject1)
      {
        paramView = ((bk)d.getApplicationContext()).a().D();
        localObject2 = "clipboard";
        paramView.a("key_last_call_origin", (String)localObject2);
        paramView.b("key_temp_latest_call_made_with_tc", bool1);
        localObject1 = "lastCallMadeWithTcTime";
        long l1 = System.currentTimeMillis();
        paramView.b((String)localObject1, l1);
        paramView = a.q();
        if (paramView != null)
        {
          paramView = o.b(paramView);
          a(paramView);
          paramView = r;
          localObject1 = "autoSearch";
          str1 = "called";
          bb.a(paramView, (String)localObject1, str1);
        }
      }
      else
      {
        localObject1 = o;
        if (paramView == localObject1)
        {
          paramView = d;
          localObject1 = a.q();
          bj.a(paramView, (String)localObject1);
          paramView = r;
          localObject1 = "autoSearch";
          str1 = "sms";
          bb.a(paramView, (String)localObject1, str1);
        }
        else
        {
          localObject1 = p;
          if (paramView == localObject1)
          {
            paramView = d;
            localObject1 = a;
            localObject2 = DetailsFragment.SourceType.ClipboardSearch;
            Contact localContact = a;
            String str2 = b;
            boolean bool2 = localContact.p(str2) ^ bool1;
            paramView = DetailsFragment.a(paramView, (Contact)localObject1, (DetailsFragment.SourceType)localObject2, bool1, bool2);
            localObject1 = aj.a(d);
            ((aj)localObject1).b(paramView);
            ((aj)localObject1).a();
            paramView = r;
            localObject1 = "autoSearch";
            str1 = "openedDetailView";
            bb.a(paramView, (String)localObject1, str1);
          }
        }
      }
    }
    a(100);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */