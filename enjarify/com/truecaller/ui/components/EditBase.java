package com.truecaller.ui.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.text.Editable;
import android.util.AttributeSet;
import com.truecaller.R.styleable;
import com.truecaller.common.e.f;
import com.truecaller.common.h.am;
import com.truecaller.util.at;
import com.truecaller.utils.ui.b;

public class EditBase
  extends AppCompatAutoCompleteTextView
{
  private final Drawable a;
  private Drawable b = null;
  private boolean c = false;
  private boolean d = false;
  private EditBase.a e;
  
  public EditBase(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private EditBase(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet, 0);
    Object localObject = R.styleable.EditBase;
    paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject);
    int i = paramAttributeSet.getResourceId(0, 2131233794);
    int j = -1;
    int k = 1;
    int m = paramAttributeSet.getResourceId(k, j);
    int n = 2130969592;
    localObject = b.a(paramContext, i, n);
    a = ((Drawable)localObject);
    if (m != j)
    {
      d = k;
      paramContext = b.a(paramContext, m, n);
      b = paramContext;
    }
    paramAttributeSet.recycle();
    boolean bool1 = isInEditMode();
    if (!bool1)
    {
      bool1 = f.b();
      if (bool1) {}
    }
    else
    {
      k = 0;
    }
    c = k;
    paramContext = "Ay";
    float f1 = getTextSize();
    localObject = getTypeface();
    float f2 = at.a(paramContext, f1, (Typeface)localObject);
    int i2 = 1061997773;
    f1 = 0.8F;
    f2 *= f1;
    int i1 = Math.round(f2);
    paramAttributeSet = a;
    paramAttributeSet.setBounds(0, 0, i1, i1);
    boolean bool2 = d;
    if (bool2)
    {
      paramContext = b;
      i2 = paramContext.getIntrinsicWidth();
      localObject = b;
      i = ((Drawable)localObject).getIntrinsicHeight();
      paramContext.setBounds(0, 0, i2, i);
    }
    paramContext = new com/truecaller/ui/components/-$$Lambda$EditBase$HeasBOl9j1SVO486ZmpmJO6b7Ug;
    paramContext.<init>(this);
    setOnTouchListener(paramContext);
    paramContext = new com/truecaller/ui/components/EditBase$1;
    paramContext.<init>(this);
    addTextChangedListener(paramContext);
    bool2 = d;
    if (bool2) {
      a();
    }
  }
  
  private void a()
  {
    Editable localEditable = getText();
    boolean bool = am.b(localEditable);
    if (bool)
    {
      bool = d;
      if (bool)
      {
        c();
        return;
      }
      d();
      return;
    }
    b();
  }
  
  private void b()
  {
    boolean bool = c;
    int i = 3;
    int j = 1;
    if (bool)
    {
      localDrawable1 = a;
      localDrawable2 = getCompoundDrawables()[j];
      localDrawable3 = getCompoundDrawables()[2];
      localDrawable4 = getCompoundDrawables()[i];
      setCompoundDrawables(localDrawable1, localDrawable2, localDrawable3, localDrawable4);
      return;
    }
    Drawable localDrawable1 = getCompoundDrawables()[0];
    Drawable localDrawable2 = getCompoundDrawables()[j];
    Drawable localDrawable3 = a;
    Drawable localDrawable4 = getCompoundDrawables()[i];
    setCompoundDrawables(localDrawable1, localDrawable2, localDrawable3, localDrawable4);
  }
  
  private void c()
  {
    boolean bool = c;
    int i = 3;
    int j = 1;
    if (bool)
    {
      localDrawable1 = b;
      localDrawable2 = getCompoundDrawables()[j];
      localDrawable3 = getCompoundDrawables()[2];
      localDrawable4 = getCompoundDrawables()[i];
      setCompoundDrawables(localDrawable1, localDrawable2, localDrawable3, localDrawable4);
      return;
    }
    Drawable localDrawable1 = getCompoundDrawables()[0];
    Drawable localDrawable2 = getCompoundDrawables()[j];
    Drawable localDrawable3 = b;
    Drawable localDrawable4 = getCompoundDrawables()[i];
    setCompoundDrawables(localDrawable1, localDrawable2, localDrawable3, localDrawable4);
  }
  
  private void d()
  {
    boolean bool = c;
    int i = 3;
    int j = 1;
    if (bool)
    {
      localDrawable1 = getCompoundDrawables()[j];
      localDrawable2 = getCompoundDrawables()[2];
      localDrawable3 = getCompoundDrawables()[i];
      setCompoundDrawables(null, localDrawable1, localDrawable2, localDrawable3);
      return;
    }
    Drawable localDrawable1 = getCompoundDrawables()[0];
    Drawable localDrawable2 = getCompoundDrawables()[j];
    Drawable localDrawable3 = getCompoundDrawables()[i];
    setCompoundDrawables(localDrawable1, localDrawable2, null, localDrawable3);
  }
  
  public void setIsScannerEnabled(boolean paramBoolean)
  {
    d = paramBoolean;
    a();
  }
  
  public void setOnScannerClickListener(EditBase.a parama)
  {
    e = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.EditBase
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */