package com.truecaller.ui.components;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.truecaller.ads.CustomTemplate;
import com.truecaller.ads.campaigns.AdCampaign;
import com.truecaller.ads.campaigns.AdCampaigns;
import com.truecaller.ads.j;
import com.truecaller.ads.j.a;
import com.truecaller.ads.provider.a.b;
import com.truecaller.ads.provider.holders.AdHolderType;
import com.truecaller.ads.provider.holders.d;
import com.truecaller.ads.provider.holders.e;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.Tag;
import com.truecaller.log.AssertionUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class q
  extends FrameLayout
{
  public static final q.c[] a;
  public q.b b;
  private final String c;
  private q.a d;
  private boolean e = false;
  private AdCampaigns f = null;
  private String g = null;
  private final com.truecaller.androidactors.f h;
  private com.truecaller.androidactors.a i = null;
  private j j = null;
  private com.truecaller.ads.k k = null;
  private final com.truecaller.ads.provider.f l;
  private final i m;
  private final com.truecaller.ads.provider.a n;
  private final com.truecaller.androidactors.f o;
  private final com.truecaller.common.g.a p;
  private final AdListener q;
  
  static
  {
    q.c[] arrayOfc = new q.c[2];
    q.c localc = new com/truecaller/ui/components/q$c;
    localc.<init>("banner-320x100", "/43067329/A*ACS*Banner*320x100*GPS", false);
    arrayOfc[0] = localc;
    localc = new com/truecaller/ui/components/q$c;
    boolean bool = true;
    localc.<init>("unified", "/43067329/A*ACS*Unified*GPS", bool);
    arrayOfc[bool] = localc;
    a = arrayOfc;
  }
  
  public q(Context paramContext, String paramString)
  {
    super(paramContext);
    Object localObject1 = new com/truecaller/ui/components/q$1;
    ((q.1)localObject1).<init>(this);
    q = ((AdListener)localObject1);
    localObject1 = ((bk)paramContext.getApplicationContext()).a();
    Object localObject2 = ((bp)localObject1).au();
    h = ((com.truecaller.androidactors.f)localObject2);
    localObject2 = ((bp)localObject1).aq();
    l = ((com.truecaller.ads.provider.f)localObject2);
    localObject2 = ((bp)localObject1).m().a();
    m = ((i)localObject2);
    c = paramString;
    paramString = ((bp)localObject1).ar();
    n = paramString;
    paramString = ((bp)localObject1).f();
    o = paramString;
    paramString = ((bp)localObject1).I();
    p = paramString;
    int i1 = 2131559225;
    inflate(paramContext, i1, this);
    try
    {
      paramString = new android/webkit/WebView;
      paramString.<init>(paramContext);
      paramContext = generateDefaultLayoutParams();
      height = 0;
      width = 0;
      addView(paramString, 0, paramContext);
      return;
    }
    catch (RuntimeException localRuntimeException) {}
  }
  
  private void b(AdCampaigns paramAdCampaigns)
  {
    Object localObject1 = paramAdCampaigns.b();
    if (localObject1 != null)
    {
      localObject2 = d;
      if (localObject2 != null) {
        ((q.a)localObject2).a((AdCampaign)localObject1);
      }
    }
    localObject1 = new com/truecaller/ui/components/q$b;
    Object localObject2 = (ViewGroup)findViewById(2131362832);
    ((q.b)localObject1).<init>(this, (ViewGroup)localObject2, paramAdCampaigns);
    b = ((q.b)localObject1);
    b.c();
  }
  
  private Activity getActivity()
  {
    for (Context localContext = getContext();; localContext = ((ContextWrapper)localContext).getBaseContext())
    {
      boolean bool = localContext instanceof ContextWrapper;
      if (!bool) {
        break;
      }
      bool = localContext instanceof Activity;
      if (bool) {
        return (Activity)localContext;
      }
    }
    return null;
  }
  
  public final void a(AdCampaigns paramAdCampaigns)
  {
    i = null;
    boolean bool = e;
    if (!bool)
    {
      f = paramAdCampaigns;
      return;
    }
    if (paramAdCampaigns == null)
    {
      AssertionUtil.reportWeirdnessButNeverCrash("Got null AdCampaigns on AfterCall");
      return;
    }
    b(paramAdCampaigns);
  }
  
  public final void a(Contact paramContact, HistoryEvent paramHistoryEvent)
  {
    Object localObject1 = paramContact.J();
    boolean bool1 = ((List)localObject1).isEmpty();
    if (!bool1)
    {
      localArrayList = new java/util/ArrayList;
      int i1 = ((List)localObject1).size();
      localArrayList.<init>(i1);
      localObject1 = ((List)localObject1).iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        localObject2 = ((Tag)((Iterator)localObject1).next()).a();
        boolean bool3 = TextUtils.isEmpty((CharSequence)localObject2);
        if (!bool3) {
          localArrayList.add(localObject2);
        }
      }
    }
    bool1 = false;
    ArrayList localArrayList = null;
    f = null;
    localObject1 = new com/truecaller/ads/j$a;
    Object localObject2 = c;
    ((j.a)localObject1).<init>((String)localObject2);
    localObject2 = b;
    a = ((String)localObject2);
    int i2 = paramContact.I();
    b = i2;
    i2 = paramHistoryEvent.g();
    localObject2 = Integer.valueOf(i2);
    d = ((Integer)localObject2);
    paramContact = paramContact.t();
    e = paramContact;
    paramContact = d;
    f = paramContact;
    g = localArrayList;
    paramContact = ((j.a)localObject1).a();
    j = paramContact;
    paramContact = i;
    if (paramContact != null)
    {
      paramContact.a();
      i = null;
    }
    paramContact = (b)h.a();
    paramHistoryEvent = j;
    paramContact = paramContact.a(paramHistoryEvent);
    paramHistoryEvent = m;
    localObject1 = new com/truecaller/ui/components/-$$Lambda$YC81vRqtTKjM3qHDmNndGGwCaqE;
    ((-..Lambda.YC81vRqtTKjM3qHDmNndGGwCaqE)localObject1).<init>(this);
    paramContact = paramContact.a(paramHistoryEvent, (ac)localObject1);
    i = paramContact;
  }
  
  public final String getAdSubType()
  {
    q.b localb = b;
    if (localb == null) {
      return null;
    }
    e locale = d;
    if (locale != null) {
      return d.c();
    }
    return null;
  }
  
  public final AdHolderType getAdType()
  {
    q.b localb = b;
    if (localb == null) {
      return null;
    }
    Object localObject = a;
    if (localObject != null) {
      return AdHolderType.PUBLISHER_VIEW;
    }
    localObject = d;
    if (localObject != null) {
      return d.a();
    }
    return null;
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    boolean bool = true;
    e = bool;
    AdCampaigns localAdCampaigns = f;
    if (localAdCampaigns != null)
    {
      b(localAdCampaigns);
      bool = false;
      localAdCampaigns = null;
      f = null;
    }
  }
  
  protected final void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    Object localObject1 = i;
    if (localObject1 != null)
    {
      ((com.truecaller.androidactors.a)localObject1).a();
      localObject1 = null;
      i = null;
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      Object localObject2 = d;
      if (localObject2 != null)
      {
        localObject2 = d.a();
        Object localObject3 = AdHolderType.CUSTOM_AD;
        if (localObject2 == localObject3)
        {
          localObject2 = e.p;
          localObject3 = "featureAdCtpRotation";
          boolean bool = ((com.truecaller.common.g.a)localObject2).b((String)localObject3);
          if (bool)
          {
            localObject2 = ((NativeCustomTemplateAd)((d)d).g()).getCustomTemplateId();
            localObject3 = CLICK_TO_PLAY_VIDEOtemplateId;
            bool = ((String)localObject2).equals(localObject3);
            if (bool) {
              break label131;
            }
          }
        }
      }
      ((q.b)localObject1).a();
    }
    label131:
    e = false;
  }
  
  public final void setAdListener(q.a parama)
  {
    d = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */