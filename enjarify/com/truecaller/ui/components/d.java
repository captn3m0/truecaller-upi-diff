package com.truecaller.ui.components;

import android.support.v7.widget.RecyclerView.Adapter;
import android.view.ViewGroup;

public abstract class d
  extends RecyclerView.Adapter
{
  public d.a a;
  public d.b b;
  
  public abstract d.c a(ViewGroup paramViewGroup, int paramInt);
  
  public abstract void a(d.c paramc, int paramInt);
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */