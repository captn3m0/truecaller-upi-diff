package com.truecaller.ui.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.b.e;
import com.truecaller.R.styleable;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.util.w;

public class AvatarView
  extends FrameLayout
{
  private final e a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private int f;
  private ContactPhoto g;
  private TextView h;
  private Uri i;
  private Uri j;
  private AvatarView.a k;
  private boolean l;
  private boolean m;
  private boolean n;
  private boolean o;
  private boolean p;
  private long q;
  
  public AvatarView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private AvatarView(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet, 0);
    Object localObject = new com/truecaller/ui/components/AvatarView$1;
    ((AvatarView.1)localObject).<init>(this);
    a = ((e)localObject);
    p = false;
    long l1 = Long.MIN_VALUE;
    q = l1;
    localObject = R.styleable.AvatarView;
    paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject);
    int i1 = paramAttributeSet.getResourceId(4, 2131559150);
    boolean bool1 = paramAttributeSet.getBoolean(0, false);
    int i2 = paramAttributeSet.getResourceId(7, 0);
    b = i2;
    i2 = 1;
    int i3 = paramAttributeSet.getColor(i2, -1);
    c = i3;
    i3 = paramAttributeSet.getResourceId(2, 2131886765);
    d = i3;
    int i4 = -16777216;
    i3 = paramAttributeSet.getResourceId(3, i4);
    e = i3;
    paramAttributeSet.recycle();
    boolean bool2 = isInEditMode();
    if (!bool2)
    {
      int i5 = d;
      if (i5 != 0)
      {
        i5 = 1;
      }
      else
      {
        i5 = 0;
        paramAttributeSet = null;
      }
      String[] arrayOfString = new String[0];
      AssertionUtil.isTrue(i5, arrayOfString);
      int i6 = e;
      if (i6 != 0)
      {
        i6 = 1;
      }
      else
      {
        i6 = 0;
        paramAttributeSet = null;
      }
      arrayOfString = new String[0];
      AssertionUtil.isTrue(i6, arrayOfString);
    }
    if (bool1)
    {
      paramAttributeSet = new com/truecaller/ui/components/-$$Lambda$AvatarView$1lOD_rBtQYJGR1Y3EzEpm2bxFAQ;
      paramAttributeSet.<init>(this);
      setOnClickListener(paramAttributeSet);
    }
    paramContext = LayoutInflater.from(paramContext).inflate(i1, this);
    paramAttributeSet = (ContactPhoto)paramContext.findViewById(2131362076);
    g = paramAttributeSet;
    paramAttributeSet = g;
    localObject = new com/truecaller/ui/components/AvatarView$2;
    ((AvatarView.2)localObject).<init>(this);
    paramAttributeSet.setCallback((e)localObject);
    int i7 = 2131365367;
    paramContext = (TextView)paramContext.findViewById(i7);
    h = paramContext;
    paramContext = g;
    if (paramContext == null) {
      i2 = 0;
    }
    paramContext = new String[0];
    AssertionUtil.isTrue(i2, paramContext);
    boolean bool3 = isInEditMode();
    if (bool3)
    {
      paramContext = g;
      i7 = 2131233847;
      paramContext.setImageResource(i7);
    }
  }
  
  private void a(Uri paramUri1, Uri paramUri2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    b();
    n = paramBoolean2;
    o = paramBoolean3;
    i = paramUri1;
    if (paramUri2 == null) {
      paramUri2 = paramUri1;
    }
    j = paramUri2;
    g.setIsSpam(false);
    paramUri2 = g;
    paramBoolean2 = n;
    paramUri2.setIsGold(paramBoolean2);
    if (paramUri1 != null)
    {
      a(paramUri1, paramBoolean1);
      return;
    }
    long l1 = q;
    long l2 = Long.MIN_VALUE;
    paramBoolean3 = l1 < l2;
    if (paramBoolean3)
    {
      paramBoolean1 = m;
      if (!paramBoolean1)
      {
        ContactPhoto localContactPhoto = g;
        int i1 = (int)l1;
        com.truecaller.utils.ui.b.a(localContactPhoto, i1);
      }
    }
  }
  
  private void a(Uri paramUri, boolean paramBoolean)
  {
    if (!paramBoolean)
    {
      paramBoolean = Settings.d();
      if (!paramBoolean)
      {
        paramBoolean = true;
        break label21;
      }
    }
    paramBoolean = false;
    ContactPhoto localContactPhoto = null;
    label21:
    l = paramBoolean;
    localContactPhoto = g;
    boolean bool = l;
    localContactPhoto.setOfflineMode(bool);
    g.a(paramUri, null);
  }
  
  private void b()
  {
    g.setIsDownload(false);
    i = null;
    j = null;
    l = false;
    m = false;
    p = false;
    g.b();
    TextView localTextView = h;
    if (localTextView != null)
    {
      int i1 = 8;
      localTextView.setVisibility(i1);
    }
    g.a(null, null);
  }
  
  public final void a()
  {
    b();
    boolean bool = true;
    m = bool;
    g.setIsSpam(bool);
  }
  
  public final void a(Contact paramContact)
  {
    int i1 = w.g(paramContact);
    f = i1;
    ContactPhoto localContactPhoto = g;
    int i2 = w.a(f, true);
    localContactPhoto.setContactBadgeDrawable(i2);
  }
  
  public final void a(b paramb)
  {
    Uri localUri1 = a;
    Uri localUri2 = b;
    boolean bool1 = c;
    boolean bool2 = d;
    boolean bool3 = e;
    a(localUri1, localUri2, bool1, bool2, bool3);
  }
  
  void setForcedLoadListener(AvatarView.a parama)
  {
    k = parama;
  }
  
  public void setPrivateAvatar(int paramInt)
  {
    g.setPrivateAvatar(paramInt);
  }
  
  public void setTintColor(int paramInt)
  {
    long l1 = paramInt;
    q = l1;
    Object localObject = i;
    if (localObject == null)
    {
      localObject = g;
      com.truecaller.utils.ui.b.a((ImageView)localObject, paramInt);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.AvatarView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */