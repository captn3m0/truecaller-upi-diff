package com.truecaller.ui.components;

import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;

public final class j
  extends RecyclerView.Adapter
  implements FeedbackItemView.a
{
  public FeedbackItemView.FeedbackItem a;
  public FeedbackItemView b;
  public FeedbackItemView.a c;
  private final RecyclerView.Adapter d;
  
  public j(RecyclerView.Adapter paramAdapter)
  {
    d = paramAdapter;
    paramAdapter = d;
    j.1 local1 = new com/truecaller/ui/components/j$1;
    local1.<init>(this);
    paramAdapter.registerAdapterDataObserver(local1);
  }
  
  private int a(int paramInt)
  {
    FeedbackItemView.FeedbackItem localFeedbackItem = a;
    if ((localFeedbackItem != null) && (paramInt > 0)) {
      return paramInt + -1;
    }
    return paramInt;
  }
  
  public final void a(FeedbackItemView.FeedbackItem paramFeedbackItem)
  {
    FeedbackItemView.a locala = c;
    if (locala != null) {
      locala.a(paramFeedbackItem);
    }
  }
  
  public final void a(FeedbackItemView paramFeedbackItemView)
  {
    b = paramFeedbackItemView;
  }
  
  public final void b(FeedbackItemView.FeedbackItem paramFeedbackItem)
  {
    FeedbackItemView.a locala = c;
    if (locala != null) {
      locala.b(paramFeedbackItem);
    }
  }
  
  public final void c(FeedbackItemView.FeedbackItem paramFeedbackItem)
  {
    Object localObject = a;
    if (paramFeedbackItem == localObject)
    {
      localObject = null;
      d(null);
    }
    localObject = c;
    if (localObject != null) {
      ((FeedbackItemView.a)localObject).c(paramFeedbackItem);
    }
  }
  
  public final void d(FeedbackItemView.FeedbackItem paramFeedbackItem)
  {
    if (paramFeedbackItem == null)
    {
      localFeedbackItem = a;
      if (localFeedbackItem != null)
      {
        notifyItemRemoved(0);
        break label57;
      }
    }
    if (paramFeedbackItem != null)
    {
      localFeedbackItem = a;
      if (localFeedbackItem == null)
      {
        notifyItemInserted(0);
        break label57;
      }
    }
    FeedbackItemView.FeedbackItem localFeedbackItem = a;
    if (paramFeedbackItem != localFeedbackItem) {
      notifyItemChanged(0);
    }
    label57:
    a = paramFeedbackItem;
  }
  
  public final int getItemCount()
  {
    RecyclerView.Adapter localAdapter = d;
    int i = localAdapter.getItemCount();
    int j = 0;
    if (i == 0) {
      return 0;
    }
    localAdapter = d;
    i = localAdapter.getItemCount();
    FeedbackItemView.FeedbackItem localFeedbackItem = a;
    if (localFeedbackItem != null) {
      j = 1;
    }
    return i + j;
  }
  
  public final int getItemViewType(int paramInt)
  {
    if (paramInt == 0)
    {
      localObject = a;
      if (localObject != null) {
        return 2131365487;
      }
    }
    Object localObject = d;
    paramInt = a(paramInt);
    return ((RecyclerView.Adapter)localObject).getItemViewType(paramInt);
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    if (paramInt == 0)
    {
      localObject = a;
      if (localObject != null)
      {
        paramViewHolder = a;
        FeedbackItemView.FeedbackItem localFeedbackItem = a;
        paramViewHolder.setFeedbackItem(localFeedbackItem);
        return;
      }
    }
    Object localObject = d;
    paramInt = a(paramInt);
    ((RecyclerView.Adapter)localObject).onBindViewHolder(paramViewHolder, paramInt);
  }
  
  public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    int i = 2131365487;
    if (paramInt == i)
    {
      FeedbackItemView localFeedbackItemView = new com/truecaller/ui/components/FeedbackItemView;
      paramViewGroup = paramViewGroup.getContext();
      localFeedbackItemView.<init>(paramViewGroup);
      localFeedbackItemView.setFeedbackItemListener(this);
      paramViewGroup = new com/truecaller/ui/components/j$a;
      paramViewGroup.<init>(this, localFeedbackItemView);
    }
    else
    {
      RecyclerView.Adapter localAdapter = d;
      paramViewGroup = localAdapter.onCreateViewHolder(paramViewGroup, paramInt);
    }
    return paramViewGroup;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */