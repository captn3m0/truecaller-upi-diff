package com.truecaller.ui.components;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

final class FloatingWindow$b
  implements View.OnTouchListener
{
  private final float b;
  private final float c;
  private float d;
  private float e;
  private int f;
  private float g;
  private float h;
  private VelocityTracker i;
  
  FloatingWindow$b(FloatingWindow paramFloatingWindow)
  {
    VelocityTracker localVelocityTracker = VelocityTracker.obtain();
    i = localVelocityTracker;
    float f1 = agetResourcesgetDisplayMetricsdensity;
    float f2 = 25.0F * f1;
    c = f2;
    f1 *= 400.0F;
    b = f1;
  }
  
  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    paramView = i;
    paramView.addMovement(paramMotionEvent);
    int j = paramMotionEvent.getAction();
    boolean bool2 = true;
    int m = 0;
    float f1 = 0.0F;
    Object localObject1 = null;
    float f3;
    float f4;
    Object localObject2;
    float f5;
    boolean bool5;
    switch (j)
    {
    default: 
      return false;
    case 2: 
      f2 = paramMotionEvent.getRawX();
      g = f2;
      f2 = paramMotionEvent.getRawY();
      h = f2;
      f2 = g;
      f3 = d;
      f2 -= f3;
      f3 = h;
      f4 = e;
      f3 -= f4;
      localObject2 = a;
      boolean bool3 = FloatingWindow.e((FloatingWindow)localObject2);
      if (!bool3)
      {
        localObject2 = a;
        bool3 = FloatingWindow.g((FloatingWindow)localObject2);
        if (!bool3)
        {
          f4 = Math.abs(f3);
          FloatingWindow localFloatingWindow = a;
          int i2 = FloatingWindow.h(localFloatingWindow);
          f5 = i2;
          bool3 = f4 < f5;
          if (bool3)
          {
            localObject2 = a;
            FloatingWindow.b((FloatingWindow)localObject2, bool2);
          }
          else
          {
            f4 = Math.abs(f2);
            localFloatingWindow = a;
            i2 = FloatingWindow.h(localFloatingWindow);
            f5 = i2;
            bool3 = f4 < f5;
            if (bool3)
            {
              localObject2 = a;
              FloatingWindow.a((FloatingWindow)localObject2, bool2);
            }
          }
        }
      }
      localObject2 = a;
      bool3 = FloatingWindow.g((FloatingWindow)localObject2);
      int n;
      if (bool3)
      {
        n = f;
        f4 = n + f3;
        int i3 = (int)f4;
        if (i3 < 0)
        {
          paramMotionEvent = FloatingWindow.b(a);
          y = 0;
        }
        else
        {
          localObject1 = a;
          m = FloatingWindow.c((FloatingWindow)localObject1);
          localObject2 = FloatingWindow.d(a);
          n = ((View)localObject2).getHeight();
          m -= n;
          if (i3 > m)
          {
            paramMotionEvent = FloatingWindow.b(a);
            localObject1 = a;
            m = FloatingWindow.c((FloatingWindow)localObject1);
            localObject2 = FloatingWindow.d(a);
            n = ((View)localObject2).getHeight();
            m -= n;
            y = m;
          }
          else
          {
            localObject1 = FloatingWindow.b(a);
            y = i3;
          }
        }
        paramMotionEvent = FloatingWindow.j(a);
        localObject1 = FloatingWindow.i(a);
        localObject2 = FloatingWindow.b(a);
        paramMotionEvent.updateViewLayout((View)localObject1, (ViewGroup.LayoutParams)localObject2);
      }
      paramMotionEvent = a;
      bool5 = FloatingWindow.e(paramMotionEvent);
      if (bool5)
      {
        bool5 = false;
        f1 = Math.abs(f2);
        localObject2 = a;
        f4 = FloatingWindow.f((FloatingWindow)localObject2);
        f1 /= f4;
        n = 1065353216;
        f4 = 1.0F;
        f1 = f4 - f1;
        f1 = Math.min(f4, f1);
        f3 = Math.max(0.0F, f1);
        localObject1 = FloatingWindow.d(a);
        ((View)localObject1).setAlpha(f3);
        paramMotionEvent = FloatingWindow.d(a);
        paramMotionEvent.setTranslationX(f2);
      }
      return bool2;
    case 1: 
      paramView = a;
      boolean bool1 = FloatingWindow.e(paramView);
      if (bool1)
      {
        i.computeCurrentVelocity(1000);
        paramView = i;
        f2 = paramView.getXVelocity();
        f4 = Math.abs(f2);
        f5 = b;
        boolean bool4 = f4 < f5;
        if (bool4)
        {
          f4 = d;
          f3 = paramMotionEvent.getRawX();
          f3 = Math.abs(f4 - f3);
          f4 = c;
          bool5 = f3 < f4;
          if (bool5) {}
        }
        else
        {
          paramMotionEvent = FloatingWindow.d(a);
          f3 = Math.abs(paramMotionEvent.getTranslationX());
          localObject2 = a;
          i1 = FloatingWindow.f((FloatingWindow)localObject2) / 2;
          f4 = i1;
          bool5 = f3 < f4;
          if (bool5) {
            break label828;
          }
        }
        paramMotionEvent = FloatingWindow.d(a);
        f3 = Math.abs(paramMotionEvent.getTranslationX());
        localObject2 = a;
        int i1 = FloatingWindow.f((FloatingWindow)localObject2) / 2;
        f4 = i1;
        bool5 = f3 < f4;
        if (!bool5)
        {
          paramView = FloatingWindow.d(a);
          f2 = paramView.getTranslationX();
        }
        paramMotionEvent = a;
        i1 = FloatingWindow.f(paramMotionEvent);
        f4 = i1;
        f2 = Math.copySign(f4, f2);
        k = (int)f2;
        FloatingWindow.a(paramMotionEvent, k);
        break label838;
        label828:
        paramView = a;
        FloatingWindow.a(paramView, 0);
        label838:
        paramView = a;
        FloatingWindow.a(paramView, false);
      }
      FloatingWindow.b(a, false);
      return bool2;
    }
    float f2 = paramMotionEvent.getRawX();
    g = f2;
    d = f2;
    f2 = paramMotionEvent.getRawY();
    h = f2;
    e = f2;
    paramView = FloatingWindow.b(a);
    int k = y;
    f = k;
    k = f;
    paramMotionEvent = a;
    int i4 = FloatingWindow.c(paramMotionEvent);
    localObject1 = FloatingWindow.d(a);
    m = ((View)localObject1).getHeight();
    i4 -= m;
    if (k > i4)
    {
      paramView = a;
      k = FloatingWindow.c(paramView);
      paramMotionEvent = FloatingWindow.d(a);
      i4 = paramMotionEvent.getHeight();
      k -= i4;
      f = k;
    }
    return bool2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.FloatingWindow.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */