package com.truecaller.ui.components;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import com.truecaller.common.h.am;
import java.util.List;

public final class f
  extends BaseAdapter
{
  private final int a;
  private n b;
  private final f.b c;
  private List d;
  
  f(List paramList)
  {
    this(paramList, 0, null, null);
  }
  
  f(List paramList, int paramInt, n paramn, f.b paramb)
  {
    d = paramList;
    if (paramInt == 0) {
      paramInt = 2131559038;
    }
    a = paramInt;
    b = paramn;
    c = paramb;
  }
  
  private void a(int paramInt)
  {
    Object localObject = (n)getItem(paramInt);
    b = ((n)localObject);
    localObject = c;
    if (localObject != null)
    {
      n localn = b;
      ((f.b)localObject).onComboItemSelected(localn);
    }
    notifyDataSetChanged();
  }
  
  public final int getCount()
  {
    return d.size();
  }
  
  public final Object getItem(int paramInt)
  {
    return d.get(paramInt);
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    Object localObject1 = paramViewGroup.getContext();
    boolean bool1 = false;
    -..Lambda.f.7WRmJlzJo6QzDnwXyGYfijypnpc local7WRmJlzJo6QzDnwXyGYfijypnpc = null;
    if (paramView != null)
    {
      paramViewGroup = (f.a)paramView.getTag();
    }
    else
    {
      paramView = LayoutInflater.from((Context)localObject1);
      int i = a;
      paramView = paramView.inflate(i, paramViewGroup, false);
      paramViewGroup = new com/truecaller/ui/components/f$a;
      paramViewGroup.<init>(paramView);
    }
    Object localObject2 = (n)getItem(paramInt);
    if (localObject2 != null)
    {
      int j = ((n)localObject2).c();
      int k = 8;
      if (j != 0)
      {
        c.setVisibility(0);
        localObject3 = c;
        ((ImageView)localObject3).setImageResource(j);
      }
      else
      {
        localObject4 = ((n)localObject2).c((Context)localObject1);
        if (localObject4 != null)
        {
          c.setVisibility(0);
          localObject3 = c;
          ((ImageView)localObject3).setImageBitmap((Bitmap)localObject4);
        }
        else
        {
          localObject4 = c;
          ((ImageView)localObject4).setVisibility(k);
        }
      }
      Object localObject4 = a;
      Object localObject3 = ((n)localObject2).a((Context)localObject1);
      ((TextView)localObject4).setText((CharSequence)localObject3);
      localObject4 = b;
      localObject3 = ((n)localObject2).b((Context)localObject1);
      boolean bool2 = am.b((CharSequence)localObject3);
      if (!bool2) {
        k = 0;
      }
      ((TextView)localObject4).setVisibility(k);
      localObject4 = b;
      localObject1 = ((n)localObject2).b((Context)localObject1);
      ((TextView)localObject4).setText((CharSequence)localObject1);
      localObject1 = d;
      if (localObject1 != null)
      {
        localObject1 = b;
        if (localObject1 != null)
        {
          localObject1 = d;
          j = 0;
          ((RadioButton)localObject1).setOnCheckedChangeListener(null);
          localObject1 = d;
          localObject2 = ((n)localObject2).q();
          localObject4 = b.q();
          if (localObject2 == localObject4) {
            bool1 = true;
          }
          ((RadioButton)localObject1).setChecked(bool1);
          localObject1 = e;
          local7WRmJlzJo6QzDnwXyGYfijypnpc = new com/truecaller/ui/components/-$$Lambda$f$7WRmJlzJo6QzDnwXyGYfijypnpc;
          local7WRmJlzJo6QzDnwXyGYfijypnpc.<init>(this, paramInt);
          ((View)localObject1).setOnClickListener(local7WRmJlzJo6QzDnwXyGYfijypnpc);
          paramViewGroup = d;
          localObject1 = new com/truecaller/ui/components/-$$Lambda$f$nvc0odARnq2sc08mBskyjura9yU;
          ((-..Lambda.f.nvc0odARnq2sc08mBskyjura9yU)localObject1).<init>(this, paramInt);
          paramViewGroup.setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject1);
        }
      }
    }
    return paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */