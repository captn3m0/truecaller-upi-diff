package com.truecaller.ui.components;

import android.support.v4.view.r;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;

final class FlowLayout$a
{
  public int a;
  public boolean b;
  public int c;
  public int d;
  public int e;
  public int f;
  public int g;
  
  private FlowLayout$a(FlowLayout paramFlowLayout) {}
  
  public final void a(int paramInt)
  {
    a = paramInt;
    FlowLayout localFlowLayout = h;
    paramInt = r.g(localFlowLayout);
    int i = 1;
    if (paramInt != i) {
      i = 0;
    }
    b = i;
    c = 0;
    d = 0;
    e = 0;
  }
  
  final void a(View paramView)
  {
    ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)paramView.getLayoutParams();
    boolean bool1 = b;
    int i;
    if (bool1) {
      i = rightMargin;
    } else {
      i = leftMargin;
    }
    boolean bool2 = b;
    if (bool2) {
      j = leftMargin;
    } else {
      j = rightMargin;
    }
    int k = paramView.getMeasuredWidth();
    int m = k + i + j;
    int n = paramView.getMeasuredHeight();
    int j = bottomMargin;
    n += j;
    j = topMargin;
    n += j;
    j = Math.max(e, n);
    e = j;
    j = a;
    int i2;
    if (k > j)
    {
      j = c;
      if (j != 0)
      {
        j = d;
        i2 = e;
        j += i2;
        d = j;
        e = n;
        c = 0;
      }
    }
    else
    {
      i2 = c + m;
      if (i2 > j)
      {
        j = d;
        i2 = e;
        j += i2;
        d = j;
        e = n;
        c = 0;
      }
    }
    boolean bool3 = b;
    if (bool3)
    {
      i1 = a;
      j = c + k + i;
      i1 -= j;
      f = i1;
    }
    else
    {
      i1 = c + i;
      f = i1;
    }
    int i1 = d;
    int i3 = topMargin;
    i1 += i3;
    g = i1;
    i1 = c + m;
    c = i1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.FlowLayout.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */