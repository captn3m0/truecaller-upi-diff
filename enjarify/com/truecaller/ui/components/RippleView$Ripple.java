package com.truecaller.ui.components;

import android.animation.ObjectAnimator;
import android.view.animation.Interpolator;

class RippleView$Ripple
{
  float a;
  int b;
  final ObjectAnimator c;
  private final float e;
  
  public RippleView$Ripple(RippleView paramRippleView, int paramInt, float paramFloat)
  {
    e = paramFloat;
    int[] arrayOfInt = new int[2];
    int[] tmp21_19 = arrayOfInt;
    tmp21_19[0] = 0;
    tmp21_19[1] = 'ߐ';
    paramRippleView = ObjectAnimator.ofInt(this, "time", arrayOfInt);
    c = paramRippleView;
    paramRippleView = c;
    long l = paramInt;
    paramRippleView.setStartDelay(l);
    c.setDuration(2000L);
    c.setRepeatCount(-1);
  }
  
  public void setTime(int paramInt)
  {
    int i = 900;
    float f1 = 1.261E-42F;
    if (paramInt <= i)
    {
      float f2 = paramInt / 900.0F;
      Interpolator localInterpolator = RippleView.a(d);
      float f3 = 1.0F - f2;
      float f4 = localInterpolator.getInterpolation(f3) * 255.0F;
      f1 = e;
      f4 *= f1;
      i = (int)f4;
      b = i;
      RippleView localRippleView = d;
      i = RippleView.b(localRippleView);
      f1 = i;
      f2 *= f1;
      a = f2;
    }
    d.invalidate();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.RippleView.Ripple
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */