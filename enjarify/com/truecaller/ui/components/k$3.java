package com.truecaller.ui.components;

import android.net.Uri;

final class k$3
  implements AvatarView.a
{
  k$3(k paramk) {}
  
  public final void a()
  {
    a.a(true);
  }
  
  public final void a(Uri paramUri)
  {
    a.a(false);
    a.a(paramUri, true);
  }
  
  public final void b()
  {
    a.a(false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.k.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */