package com.truecaller.ui.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowInsets;
import android.widget.FrameLayout;
import c.g.b.k;

public final class WindowInsetsFrameLayout
  extends FrameLayout
{
  public WindowInsetsFrameLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public final WindowInsets onApplyWindowInsets(WindowInsets paramWindowInsets)
  {
    String str = "insets";
    k.b(paramWindowInsets, str);
    int i = getChildCount();
    int j = 0;
    while (j < i)
    {
      View localView = getChildAt(j);
      localView.dispatchApplyWindowInsets(paramWindowInsets);
      j += 1;
    }
    return paramWindowInsets;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.WindowInsetsFrameLayout
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */