package com.truecaller.ui.components;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.app.f;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.truecaller.R.styleable;
import com.truecaller.TrueApp;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.bp;
import com.truecaller.common.b.a;
import com.truecaller.common.h.ai;
import com.truecaller.common.h.am;
import com.truecaller.common.h.o;
import com.truecaller.old.data.access.Settings;
import com.truecaller.referral.ReferralManager;
import com.truecaller.referral.ReferralManager.ReferralLaunchContext;
import com.truecaller.referral.w;
import com.truecaller.ui.SingleActivity;
import com.truecaller.ui.SingleActivity.FragmentSingle;
import com.truecaller.util.at;
import com.truecaller.util.bk;

public class FeedbackItemView
  extends RelativeLayout
  implements View.OnClickListener
{
  private FeedbackItemView.FeedbackItem a;
  private ImageView b;
  private TextView c;
  private Button d;
  private Button e;
  private Button f;
  private boolean g;
  private float h;
  private boolean i;
  private FeedbackItemView.a j;
  private boolean k;
  private int l;
  
  public FeedbackItemView(Context paramContext)
  {
    super(paramContext);
    a(null);
  }
  
  public FeedbackItemView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramAttributeSet);
  }
  
  public static FeedbackItemView.FeedbackItem a(FeedbackItemView.DisplaySource paramDisplaySource, Context paramContext)
  {
    FeedbackItemView.FeedbackItem.FeedbackItemState localFeedbackItemState = null;
    if (paramContext == null) {
      return null;
    }
    boolean bool = paramDisplaySource.shouldShowInviteFriends();
    if (bool)
    {
      paramContext = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem;
      localFeedbackItemState = paramDisplaySource.getInitialInviteState();
      paramContext.<init>(paramDisplaySource, localFeedbackItemState);
      return paramContext;
    }
    bool = paramDisplaySource.shouldShowShare();
    if (bool)
    {
      paramContext = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem;
      localFeedbackItemState = paramDisplaySource.getInitialShareState();
      paramContext.<init>(paramDisplaySource, localFeedbackItemState);
      return paramContext;
    }
    bool = paramDisplaySource.shouldShowFeedback();
    if (bool)
    {
      paramContext = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem;
      localFeedbackItemState = paramDisplaySource.getInitialFeedbackState();
      paramContext.<init>(paramDisplaySource, localFeedbackItemState);
      return paramContext;
    }
    return null;
  }
  
  private void a(AttributeSet paramAttributeSet)
  {
    Context localContext = getContext();
    int[] arrayOfInt = R.styleable.FeedbackItemView;
    paramAttributeSet = localContext.obtainStyledAttributes(paramAttributeSet, arrayOfInt, 0, 0);
    try
    {
      localContext = getContext();
      float f1 = 32.0F;
      int m = at.a(localContext, f1);
      m = paramAttributeSet.getDimensionPixelSize(0, m);
      l = m;
      return;
    }
    finally
    {
      paramAttributeSet.recycle();
    }
  }
  
  private static void a(Button paramButton, int paramInt)
  {
    int m = -1;
    if (paramInt == m)
    {
      paramButton.setVisibility(4);
      return;
    }
    paramButton.setText(paramInt);
  }
  
  protected static boolean a()
  {
    Object localObject = a.F();
    boolean bool = ((a)localObject).o();
    if (!bool) {
      return false;
    }
    localObject = "GOOGLE_REVIEW_DONE";
    bool = Settings.e((String)localObject);
    if (bool) {
      return false;
    }
    localObject = "FEEDBACK_SENT";
    bool = Settings.e((String)localObject);
    if (bool) {
      return false;
    }
    localObject = "FEEDBACK_DISMISSED_COUNT";
    long l1 = 2;
    bool = Settings.c((String)localObject, l1);
    return !bool;
  }
  
  private void d()
  {
    Object localObject = getContext();
    FeedbackItemView.FeedbackItem.FeedbackItemState localFeedbackItemState = a.a;
    e();
    boolean bool1 = localFeedbackItemState.isInviteState();
    if (bool1)
    {
      Settings.g("INVITE_LAST_DISMISSED");
      return;
    }
    Settings.g("FEEDBACK_LAST_DISMISSED");
    Settings.i("FEEDBACK_DISMISSED_COUNT");
    String str = "FEEDBACK_DISMISSED_COUNT";
    long l1 = 2;
    bool1 = Settings.c(str, l1);
    if (bool1)
    {
      boolean bool2 = localFeedbackItemState.isShareState();
      int m;
      if (bool2) {
        m = 2131886577;
      } else {
        m = 2131886542;
      }
      bool1 = false;
      str = null;
      localObject = Toast.makeText((Context)localObject, m, 0);
      ((Toast)localObject).show();
    }
  }
  
  private void e()
  {
    i = true;
    Object localObject1 = a;
    Object localObject2 = FeedbackItemView.FeedbackItem.FeedbackItemState.DUMMY_FINAL;
    a = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject2);
    boolean bool = g;
    int m = 2;
    long l1 = 200L;
    if (!bool)
    {
      localObject1 = new android/animation/AnimatorSet;
      ((AnimatorSet)localObject1).<init>();
      int n = 0;
      ViewGroup.LayoutParams localLayoutParams = null;
      for (;;)
      {
        i1 = getChildCount();
        if (n >= i1) {
          break;
        }
        Object localObject3 = getChildAt(n);
        float[] arrayOfFloat = new float[m];
        float[] tmp79_77 = arrayOfFloat;
        tmp79_77[0] = 1.0F;
        tmp79_77[1] = 0.0F;
        localObject3 = bk.a(localObject3, "alpha", arrayOfFloat);
        localObject4 = new android/view/animation/AccelerateDecelerateInterpolator;
        ((AccelerateDecelerateInterpolator)localObject4).<init>();
        localObject3 = aba;
        ((AnimatorSet)localObject1).play((Animator)localObject3);
        n += 1;
      }
      localLayoutParams = getLayoutParams();
      int i1 = getHeight();
      Object localObject4 = new android/animation/ValueAnimator;
      ((ValueAnimator)localObject4).<init>();
      localObject2 = new float[m];
      Object tmp169_168 = localObject2;
      tmp169_168[0] = 1.0F;
      tmp169_168[1] = 0.0F;
      ((ValueAnimator)localObject4).setFloatValues((float[])localObject2);
      ((ValueAnimator)localObject4).setDuration(l1);
      ((ValueAnimator)localObject4).setStartDelay(l1);
      localObject2 = new android/view/animation/AccelerateDecelerateInterpolator;
      ((AccelerateDecelerateInterpolator)localObject2).<init>();
      ((ValueAnimator)localObject4).setInterpolator((TimeInterpolator)localObject2);
      localObject2 = new com/truecaller/ui/components/-$$Lambda$FeedbackItemView$dIDTK8Ei2gDQRdZxFVMYIOftkvg;
      ((-..Lambda.FeedbackItemView.dIDTK8Ei2gDQRdZxFVMYIOftkvg)localObject2).<init>(this, localLayoutParams, i1);
      ((ValueAnimator)localObject4).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject2);
      ((AnimatorSet)localObject1).play((Animator)localObject4);
      localObject2 = new com/truecaller/ui/components/FeedbackItemView$3;
      ((FeedbackItemView.3)localObject2).<init>(this);
      ((AnimatorSet)localObject1).addListener((Animator.AnimatorListener)localObject2);
      ((AnimatorSet)localObject1).start();
      return;
    }
    localObject2 = new float[m];
    Object tmp263_262 = localObject2;
    tmp263_262[0] = 1.0F;
    tmp263_262[1] = 0.0F;
    localObject1 = bk.a(this, "alpha", (float[])localObject2);
    localObject2 = new android/view/animation/AccelerateDecelerateInterpolator;
    ((AccelerateDecelerateInterpolator)localObject2).<init>();
    localObject1 = ((bk)localObject1).a((TimeInterpolator)localObject2).b(l1);
    localObject2 = new com/truecaller/ui/components/FeedbackItemView$4;
    ((FeedbackItemView.4)localObject2).<init>(this);
    aa.start();
  }
  
  private void setIconDrawable(FeedbackItemView.FeedbackItem paramFeedbackItem)
  {
    Context localContext = getContext();
    int m = a.getIconId();
    paramFeedbackItem = com.truecaller.utils.ui.b.a(localContext, m, 2130969592);
    b.setImageDrawable(paramFeedbackItem);
  }
  
  public final void b()
  {
    Object localObject1 = a.a;
    Object localObject2 = FeedbackItemView.FeedbackItem.FeedbackItemState.RATE_YES;
    if (localObject1 == localObject2)
    {
      a.a();
      localObject1 = f;
      int m = 8;
      ((Button)localObject1).setVisibility(m);
      d.setVisibility(m);
      e.setVisibility(m);
      setMinimumHeight(0);
      localObject1 = c;
      localObject2 = a;
      Object localObject3 = getContext();
      localObject2 = ((FeedbackItemView.FeedbackItem)localObject2).d((Context)localObject3);
      ((TextView)localObject1).setText((CharSequence)localObject2);
      localObject1 = a;
      setIconDrawable((FeedbackItemView.FeedbackItem)localObject1);
      localObject1 = b;
      localObject3 = new float[2];
      Object tmp108_106 = localObject3;
      tmp108_106[0] = 0.0F;
      tmp108_106[1] = 360.0F;
      localObject1 = bk.a(localObject1, "rotation", (float[])localObject3);
      localObject2 = new android/view/animation/OvershootInterpolator;
      float f1 = 1.5F;
      ((OvershootInterpolator)localObject2).<init>(f1);
      localObject1 = ((bk)localObject1).a((TimeInterpolator)localObject2);
      long l1 = 500L;
      aba.start();
      localObject1 = new com/truecaller/ui/components/-$$Lambda$FeedbackItemView$cOOIi6O-P4wgdRS25VlW6WrzxiU;
      ((-..Lambda.FeedbackItemView.cOOIi6O-P4wgdRS25VlW6WrzxiU)localObject1).<init>(this);
      l1 = 2000L;
      postDelayed((Runnable)localObject1, l1);
    }
  }
  
  public final boolean c()
  {
    FeedbackItemView.FeedbackItem.FeedbackItemState localFeedbackItemState = a.a;
    boolean bool = localFeedbackItemState.shouldShare();
    if (bool)
    {
      bool = k;
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public void onClick(View paramView)
  {
    boolean bool1 = i;
    if (bool1) {
      return;
    }
    Object localObject1 = getContext();
    int m = paramView.getId();
    boolean bool4 = true;
    Object localObject2;
    switch (m)
    {
    default: 
      return;
    case 2131363058: 
      paramView = j;
      if (paramView != null)
      {
        localObject2 = a;
        paramView.a((FeedbackItemView.FeedbackItem)localObject2);
      }
      a.a();
      paramView = a.a;
      localObject2 = FeedbackItemView.FeedbackItem.FeedbackItemState.QUESTION_RATE;
      if (paramView != localObject2)
      {
        paramView = a.a;
        localObject2 = FeedbackItemView.FeedbackItem.FeedbackItemState.RATE_YES;
        if (paramView != localObject2) {
          break;
        }
      }
      else
      {
        paramView = "FEEDBACK_LIKES_TRUECALLER";
        Settings.a(paramView, bool4);
      }
      break;
    case 2131363057: 
      paramView = j;
      if (paramView != null)
      {
        localObject2 = a;
        paramView.b((FeedbackItemView.FeedbackItem)localObject2);
      }
      paramView = a;
      localObject2 = FeedbackItemView.5.a;
      Object localObject3 = a;
      int i1 = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject3).ordinal();
      int i3 = localObject2[i1];
      switch (i3)
      {
      default: 
        break;
      case 10: 
        localObject2 = FeedbackItemView.FeedbackItem.FeedbackItemState.INVITE_NO;
        a = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject2);
        break;
      case 7: 
      case 8: 
      case 9: 
        localObject2 = FeedbackItemView.FeedbackItem.FeedbackItemState.SHARE_NO;
        a = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject2);
        break;
      case 6: 
        localObject2 = FeedbackItemView.FeedbackItem.FeedbackItemState.RATE_YES_THANKS;
        a = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject2);
        break;
      case 5: 
        localObject2 = FeedbackItemView.FeedbackItem.FeedbackItemState.RATE_NO;
        a = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject2);
        break;
      case 3: 
      case 4: 
        localObject2 = FeedbackItemView.FeedbackItem.FeedbackItemState.FEEDBACK_NO;
        a = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject2);
        break;
      case 1: 
      case 2: 
        localObject2 = FeedbackItemView.FeedbackItem.FeedbackItemState.QUESTION_GIVE_FEEDBACK;
        a = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject2);
      }
      paramView = a;
      localObject2 = getContext();
      paramView = paramView.d((Context)localObject2);
      localObject2 = a.a;
      i3 = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject2).getIconId();
      boolean bool5 = am.a(paramView);
      int i2;
      int i4;
      Object localObject4;
      if ((bool5) && (i3 >= 0))
      {
        localObject2 = getContext();
        i2 = a.a.getIconId();
        i4 = 2130969592;
        localObject2 = com.truecaller.utils.ui.b.a((Context)localObject2, i2, i4);
        i = bool4;
        localObject3 = c;
        int i5 = 2;
        float[] arrayOfFloat1 = new float[i5];
        float[] tmp438_436 = arrayOfFloat1;
        tmp438_436[0] = 1.0F;
        tmp438_436[1] = 0.0F;
        localObject3 = bk.a(localObject3, "alpha", arrayOfFloat1);
        long l1 = 100;
        localObject3 = ((bk)localObject3).b(l1);
        localObject4 = new android/view/animation/AccelerateDecelerateInterpolator;
        ((AccelerateDecelerateInterpolator)localObject4).<init>();
        aa.start();
        localObject3 = b;
        Object localObject5 = new float[i5];
        Object tmp508_506 = localObject5;
        tmp508_506[0] = 1.0F;
        tmp508_506[1] = 0.0F;
        localObject3 = bk.a(localObject3, "alpha", (float[])localObject5).b(l1);
        localObject4 = new android/view/animation/AccelerateDecelerateInterpolator;
        ((AccelerateDecelerateInterpolator)localObject4).<init>();
        localObject3 = ((bk)localObject3).a((TimeInterpolator)localObject4);
        localObject4 = new com/truecaller/ui/components/FeedbackItemView$1;
        ((FeedbackItemView.1)localObject4).<init>(this, paramView, (Drawable)localObject2);
        aa.start();
        paramView = new android/animation/AnimatorSet;
        paramView.<init>();
        localObject2 = new android/view/animation/DecelerateInterpolator;
        ((DecelerateInterpolator)localObject2).<init>();
        paramView.setInterpolator((TimeInterpolator)localObject2);
        localObject2 = new com/truecaller/ui/components/FeedbackItemView$2;
        ((FeedbackItemView.2)localObject2).<init>(this);
        paramView.addListener((Animator.AnimatorListener)localObject2);
        localObject2 = c;
        localObject4 = new float[i5];
        float f1 = -h;
        localObject4[0] = f1;
        f1 = 0.0F;
        localObject4[bool4] = 0.0F;
        localObject2 = a"translationX"aba;
        localObject3 = c;
        Object localObject6 = new float[i5];
        Object tmp692_690 = localObject6;
        tmp692_690[0] = 0.0F;
        tmp692_690[1] = 1.0F;
        localObject3 = a"alpha"aba;
        localObject4 = b;
        localObject6 = "translationX";
        float[] arrayOfFloat2 = new float[i5];
        float f2 = -h;
        arrayOfFloat2[0] = f2;
        arrayOfFloat2[bool4] = 0.0F;
        localObject4 = aaba;
        localObject5 = b;
        String str = "alpha";
        Object localObject7 = new float[i5];
        Object tmp802_800 = localObject7;
        tmp802_800[0] = 0.0F;
        tmp802_800[1] = 1.0F;
        localObject7 = aaba;
        paramView.play((Animator)localObject2);
        paramView.play((Animator)localObject3);
        paramView.play((Animator)localObject4);
        paramView.play((Animator)localObject7);
        paramView.start();
      }
      paramView = a.a;
      boolean bool2 = paramView.shouldGiveFeedback();
      Object localObject8;
      if (bool2)
      {
        paramView = TrueApp.y().a().c();
        localObject8 = new com/truecaller/analytics/e$a;
        ((e.a)localObject8).<init>("ViewAction");
        localObject3 = a.b.asAnalyticsContext();
        localObject8 = ((e.a)localObject8).a("Context", (String)localObject3);
        localObject2 = "Action";
        localObject3 = "feedback";
        localObject8 = ((e.a)localObject8).a((String)localObject2, (String)localObject3).a();
        paramView.a((e)localObject8);
        paramView = SingleActivity.FragmentSingle.FEEDBACK_FORM;
        paramView = SingleActivity.a((Context)localObject1, paramView);
        ((Context)localObject1).startActivity(paramView);
      }
      else
      {
        paramView = a.a;
        bool2 = paramView.shouldRate();
        if (bool2)
        {
          paramView = TrueApp.y().a().c();
          localObject2 = new com/truecaller/analytics/e$a;
          ((e.a)localObject2).<init>("ViewAction");
          localObject4 = a.b.asAnalyticsContext();
          localObject2 = ((e.a)localObject2).a("Context", (String)localObject4);
          localObject3 = "Action";
          localObject4 = "rate";
          localObject2 = ((e.a)localObject2).a((String)localObject3, (String)localObject4).a();
          paramView.a((e)localObject2);
          o.b((Context)localObject1);
          paramView = j;
          if (paramView != null)
          {
            paramView.a(this);
          }
          else
          {
            paramView = a;
            localObject1 = FeedbackItemView.FeedbackItem.FeedbackItemState.DUMMY_FINAL;
            a = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject1);
          }
          Settings.a("GOOGLE_REVIEW_DONE", bool4);
          paramView = "FEEDBACK_DISMISSED_COUNT";
          Settings.h(paramView);
        }
        else
        {
          paramView = a.a;
          bool2 = paramView.shouldShare();
          if (bool2)
          {
            paramView = TrueApp.y().a().c();
            localObject2 = new com/truecaller/analytics/e$a;
            ((e.a)localObject2).<init>("ViewAction");
            localObject4 = a.b.asAnalyticsContext();
            localObject2 = ((e.a)localObject2).a("Context", (String)localObject4).a("Action", "share").a();
            paramView.a((e)localObject2);
            int n = 2131886653;
            paramView = ((Context)localObject1).getString(n);
            i3 = 2131887144;
            localObject2 = ((Context)localObject1).getString(i3);
            i2 = 2131887143;
            localObject3 = ((Context)localObject1).getString(i2);
            i4 = 0;
            localObject4 = null;
            ai.a((Context)localObject1, paramView, (String)localObject2, (CharSequence)localObject3, null);
            k = bool4;
          }
          else
          {
            paramView = a.a;
            bool3 = paramView.shouldInvite();
            if (bool3)
            {
              paramView = TrueApp.y().a().c();
              localObject8 = new com/truecaller/analytics/e$a;
              ((e.a)localObject8).<init>("ViewAction");
              localObject3 = a.b.asAnalyticsContext();
              localObject8 = ((e.a)localObject8).a("Context", (String)localObject3);
              localObject2 = "Action";
              localObject3 = "invite";
              localObject8 = ((e.a)localObject8).a((String)localObject2, (String)localObject3).a();
              paramView.a((e)localObject8);
              localObject1 = (f)localObject1;
              paramView = w.a((f)localObject1, "ReferralManagerImpl");
              if (paramView != null)
              {
                localObject1 = ReferralManager.ReferralLaunchContext.SEARCH_SCREEN_PROMO;
                paramView.a((ReferralManager.ReferralLaunchContext)localObject1);
              }
            }
          }
        }
      }
      paramView = a.a;
      boolean bool3 = paramView.shouldClose();
      if (bool3)
      {
        paramView = a.a;
        localObject1 = FeedbackItemView.FeedbackItem.FeedbackItemState.RATE_NO;
        if (paramView != localObject1)
        {
          paramView = a.a;
          localObject1 = FeedbackItemView.FeedbackItem.FeedbackItemState.FEEDBACK_NO;
          if (paramView != localObject1)
          {
            paramView = a.a;
            localObject1 = FeedbackItemView.FeedbackItem.FeedbackItemState.SHARE_NO;
            if (paramView != localObject1)
            {
              paramView = a.a;
              localObject1 = FeedbackItemView.FeedbackItem.FeedbackItemState.INVITE_NO;
              if (paramView != localObject1)
              {
                e();
                break label1521;
              }
            }
          }
        }
        d();
        return;
      }
      label1521:
      return;
    }
    d();
  }
  
  public void setDialogStyle(boolean paramBoolean)
  {
    g = paramBoolean;
  }
  
  public void setFeedbackItem(FeedbackItemView.FeedbackItem paramFeedbackItem)
  {
    Object localObject1 = a;
    if (localObject1 == paramFeedbackItem) {
      return;
    }
    a = paramFeedbackItem;
    localObject1 = a.a;
    boolean bool1 = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject1).isFeedbackState();
    boolean bool2 = true;
    if (bool1)
    {
      localObject1 = a.b;
      localObject2 = FeedbackItemView.DisplaySource.GLOBAL_SEARCH_HISTORY;
      if (localObject1 == localObject2)
      {
        localObject1 = "FEEDBACK_HAS_ASKED_SEARCH";
        Settings.a((String)localObject1, bool2);
      }
      else
      {
        localObject1 = a.b;
        localObject2 = FeedbackItemView.DisplaySource.AFTERCALL;
        if (localObject1 == localObject2)
        {
          localObject1 = "FEEDBACK_HAS_ASKED_AFTERCALL";
          Settings.a((String)localObject1, bool2);
        }
      }
    }
    localObject1 = a;
    bool1 = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject1).shouldClose();
    if (bool1)
    {
      setVisibility(8);
      return;
    }
    int m = com.truecaller.utils.ui.b.a(getContext(), 2130969548);
    setBackgroundColor(m);
    localObject1 = LayoutInflater.from(getContext());
    int i1 = 2131559199;
    float f1 = 1.8743735E38F;
    ((LayoutInflater)localObject1).inflate(i1, this, bool2);
    localObject1 = getLayoutParams();
    int n = -1;
    if (localObject1 != null)
    {
      i1 = -2;
      f1 = 0.0F / 0.0F;
      height = i1;
      width = n;
    }
    m = at.a(getContext(), 96.0F);
    setMinimumHeight(m);
    localObject1 = getContext();
    i1 = 1090519040;
    f1 = 8.0F;
    float f2 = at.a((Context)localObject1, f1);
    h = f2;
    localObject1 = (Button)findViewById(2131363057);
    d = ((Button)localObject1);
    d.setOnClickListener(this);
    localObject1 = (Button)findViewById(2131363058);
    e = ((Button)localObject1);
    e.setOnClickListener(this);
    localObject1 = (Button)findViewById(2131363056);
    f = ((Button)localObject1);
    f.setOnClickListener(this);
    localObject1 = (ImageView)findViewById(2131363068);
    b = ((ImageView)localObject1);
    f2 = 1.8345936E38F;
    localObject1 = (TextView)findViewById(2131363069);
    c = ((TextView)localObject1);
    localObject1 = getContext();
    localObject1 = paramFeedbackItem.d((Context)localObject1);
    Object localObject2 = c;
    ((TextView)localObject2).setText((CharSequence)localObject1);
    localObject1 = a;
    m = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject1).getIconId();
    if (m != n) {
      setIconDrawable(paramFeedbackItem);
    }
    localObject1 = f;
    n = a.getDismissId();
    a((Button)localObject1, n);
    localObject1 = d;
    n = a.getNegativeId();
    a((Button)localObject1, n);
    localObject1 = e;
    int i2 = a.getPositiveId();
    a((Button)localObject1, i2);
    i2 = getPaddingLeft();
    m = getPaddingTop();
    n = l;
    i1 = getPaddingBottom();
    setPadding(i2, m, n, i1);
  }
  
  public void setFeedbackItemListener(FeedbackItemView.a parama)
  {
    j = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.FeedbackItemView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */