package com.truecaller.ui.components;

import android.net.Uri;
import c.g.b.k;

public final class b
{
  final Uri a;
  final Uri b;
  final boolean c;
  final boolean d;
  final boolean e;
  
  public b(Uri paramUri1, Uri paramUri2, boolean paramBoolean1, boolean paramBoolean2)
  {
    a = paramUri1;
    b = paramUri2;
    c = false;
    d = paramBoolean1;
    e = paramBoolean2;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof b;
      if (bool2)
      {
        paramObject = (b)paramObject;
        Uri localUri1 = a;
        Uri localUri2 = a;
        bool2 = k.a(localUri1, localUri2);
        if (bool2)
        {
          localUri1 = b;
          localUri2 = b;
          bool2 = k.a(localUri1, localUri2);
          if (bool2)
          {
            bool2 = c;
            boolean bool3 = c;
            if (bool2 == bool3)
            {
              bool2 = true;
            }
            else
            {
              bool2 = false;
              localUri1 = null;
            }
            if (bool2)
            {
              bool2 = d;
              bool3 = d;
              if (bool2 == bool3)
              {
                bool2 = true;
              }
              else
              {
                bool2 = false;
                localUri1 = null;
              }
              if (bool2)
              {
                bool2 = e;
                boolean bool4 = e;
                if (bool2 == bool4)
                {
                  bool4 = true;
                }
                else
                {
                  bool4 = false;
                  paramObject = null;
                }
                if (bool4) {
                  return bool1;
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    Uri localUri1 = a;
    int i = 0;
    if (localUri1 != null)
    {
      k = localUri1.hashCode();
    }
    else
    {
      k = 0;
      localUri1 = null;
    }
    k *= 31;
    Uri localUri2 = b;
    if (localUri2 != null) {
      i = localUri2.hashCode();
    }
    int k = (k + i) * 31;
    int j = c;
    if (j != 0) {
      j = 1;
    }
    int m = (k + j) * 31;
    j = d;
    if (j != 0) {
      j = 1;
    }
    int n = (m + j) * 31;
    j = e;
    if (j != 0) {
      j = 1;
    }
    return n + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("AvatarConfig(thumbnailUri=");
    Uri localUri = a;
    localStringBuilder.append(localUri);
    localStringBuilder.append(", bigAvatarUri=");
    localUri = b;
    localStringBuilder.append(localUri);
    localStringBuilder.append(", forceDownload=");
    boolean bool = c;
    localStringBuilder.append(bool);
    localStringBuilder.append(", isGold=");
    bool = d;
    localStringBuilder.append(bool);
    localStringBuilder.append(", isPremium=");
    bool = e;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */