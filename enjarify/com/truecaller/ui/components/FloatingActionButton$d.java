package com.truecaller.ui.components;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.view.View;
import com.truecaller.util.at;

final class FloatingActionButton$d
  extends View
{
  final int a;
  private Paint b;
  
  public FloatingActionButton$d(Context paramContext)
  {
    super(paramContext);
    int i = at.a(getContext(), 4.0F);
    int j = at.a(getContext(), 0.0F);
    int k = at.a(getContext(), 2.0F);
    int m = Math.max(j, k) + i;
    a = m;
    Paint localPaint = new android/graphics/Paint;
    localPaint.<init>();
    b = localPaint;
    b.setStrokeWidth(1.0F);
    localPaint = b;
    int n = 855638016;
    localPaint.setColor(n);
    localPaint = b;
    Paint.Style localStyle = Paint.Style.FILL;
    localPaint.setStyle(localStyle);
    m = 1;
    setLayerType(m, null);
    b.setAntiAlias(m);
    localPaint = b;
    float f1 = i;
    float f2 = j;
    float f3 = k;
    localPaint.setShadowLayer(f1, f2, f3, n);
  }
  
  protected final void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    int i = paramCanvas.save();
    float f1 = getWidth() / 2;
    float f2 = getHeight() / 2;
    paramCanvas.translate(f1, f2);
    int j = getWidth() / 2;
    int k = a;
    f1 = j - k;
    Paint localPaint = b;
    paramCanvas.drawCircle(0.0F, 0.0F, f1, localPaint);
    paramCanvas.restoreToCount(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.FloatingActionButton.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */