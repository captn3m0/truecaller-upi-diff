package com.truecaller.ui.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import com.d.b.w;
import com.truecaller.callhistory.z;

public final class p
  extends g
  implements AbsListView.OnScrollListener
{
  private final w d;
  private final Object e;
  private final Context f;
  private final int g;
  
  public p(Context paramContext)
  {
    f = paramContext;
    a();
    Object localObject = w.a(paramContext);
    d = ((w)localObject);
    localObject = new java/lang/Object;
    localObject.<init>();
    e = localObject;
    boolean bool = true;
    int[] arrayOfInt = new int[bool];
    arrayOfInt[0] = 16842829;
    paramContext = paramContext.obtainStyledAttributes(arrayOfInt);
    int i = paramContext.getDimensionPixelSize(0, -2);
    g = i;
    paramContext.recycle();
    setHasStableIds(bool);
  }
  
  private void a()
  {
    w localw = d;
    if (localw != null)
    {
      Object localObject = e;
      localw.a(localObject);
    }
  }
  
  public final z a(z paramz)
  {
    a();
    return (z)super.a(paramz);
  }
  
  public final d.c a(ViewGroup paramViewGroup, int paramInt)
  {
    LayoutInflater localLayoutInflater = LayoutInflater.from(paramViewGroup.getContext());
    p.a locala = new com/truecaller/ui/components/p$a;
    paramViewGroup = localLayoutInflater.inflate(2131559017, paramViewGroup, false);
    locala.<init>(paramViewGroup);
    return locala;
  }
  
  public final int getItemCount()
  {
    int i = super.getItemCount();
    if (i == 0) {
      return 0;
    }
    return Math.max(3, i);
  }
  
  public final int getItemViewType(int paramInt)
  {
    return 2131365488;
  }
  
  public final void onScroll(AbsListView paramAbsListView, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void onScrollStateChanged(AbsListView paramAbsListView, int paramInt)
  {
    int i;
    if (paramInt == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      paramAbsListView = null;
    }
    if (i != 0)
    {
      paramAbsListView = d;
      localObject = e;
      paramAbsListView.c(localObject);
      return;
    }
    paramAbsListView = d;
    Object localObject = e;
    paramAbsListView.b(localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */