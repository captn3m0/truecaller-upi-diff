package com.truecaller.ui.components;

import com.truecaller.common.b.a;
import com.truecaller.old.data.access.Settings;

public enum FeedbackItemView$DisplaySource
{
  static
  {
    Object localObject = new com/truecaller/ui/components/FeedbackItemView$DisplaySource;
    ((DisplaySource)localObject).<init>("AFTERCALL", 0);
    AFTERCALL = (DisplaySource)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$DisplaySource;
    int i = 1;
    ((DisplaySource)localObject).<init>("BLOCKED_CALL", i);
    BLOCKED_CALL = (DisplaySource)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$DisplaySource;
    int j = 2;
    ((DisplaySource)localObject).<init>("GLOBAL_SEARCH_HISTORY", j);
    GLOBAL_SEARCH_HISTORY = (DisplaySource)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$DisplaySource;
    int k = 3;
    ((DisplaySource)localObject).<init>("OTHER", k);
    OTHER = (DisplaySource)localObject;
    localObject = new DisplaySource[4];
    DisplaySource localDisplaySource = AFTERCALL;
    localObject[0] = localDisplaySource;
    localDisplaySource = BLOCKED_CALL;
    localObject[i] = localDisplaySource;
    localDisplaySource = GLOBAL_SEARCH_HISTORY;
    localObject[j] = localDisplaySource;
    localDisplaySource = OTHER;
    localObject[k] = localDisplaySource;
    $VALUES = (DisplaySource[])localObject;
  }
  
  public final String asAnalyticsContext()
  {
    int[] arrayOfInt = FeedbackItemView.5.b;
    int i = ordinal();
    int j = arrayOfInt[i];
    switch (j)
    {
    default: 
      return "undefined";
    case 3: 
      return "callBlocked";
    case 2: 
      return "afterCall";
    }
    return "searchHistory";
  }
  
  public final FeedbackItemView.FeedbackItem.FeedbackItemState getInitialFeedbackState()
  {
    DisplaySource localDisplaySource = AFTERCALL;
    if (this == localDisplaySource) {
      return FeedbackItemView.FeedbackItem.FeedbackItemState.QUESTION_ENJOYING_CALLER_ID;
    }
    localDisplaySource = BLOCKED_CALL;
    if (this == localDisplaySource) {
      return FeedbackItemView.FeedbackItem.FeedbackItemState.QUESTION_ENJOYING_BLOCKED;
    }
    return FeedbackItemView.FeedbackItem.FeedbackItemState.QUESTION_ENJOYING;
  }
  
  public final FeedbackItemView.FeedbackItem.FeedbackItemState getInitialInviteState()
  {
    return FeedbackItemView.FeedbackItem.FeedbackItemState.QUESTION_INVITE_FRIENDS;
  }
  
  public final FeedbackItemView.FeedbackItem.FeedbackItemState getInitialShareState()
  {
    DisplaySource localDisplaySource = AFTERCALL;
    if (this == localDisplaySource) {
      return FeedbackItemView.FeedbackItem.FeedbackItemState.QUESTION_SHARE_CALLER_ID;
    }
    localDisplaySource = BLOCKED_CALL;
    if (this == localDisplaySource) {
      return FeedbackItemView.FeedbackItem.FeedbackItemState.QUESTION_SHARE_BLOCKED;
    }
    return FeedbackItemView.FeedbackItem.FeedbackItemState.QUESTION_SHARE;
  }
  
  public final boolean shouldShowFeedback()
  {
    boolean bool1 = FeedbackItemView.a();
    if (!bool1) {
      return false;
    }
    Object localObject = FeedbackItemView.5.b;
    int j = ordinal();
    int i = localObject[j];
    j = 1;
    long l1;
    switch (i)
    {
    default: 
      return false;
    case 3: 
      l1 = Settings.j();
      long l2 = 1L;
      bool2 = l1 < l2;
      if (!bool2) {
        return j;
      }
      return false;
    case 2: 
      localObject = "FEEDBACK_HAS_ASKED_AFTERCALL";
      bool2 = Settings.e((String)localObject);
      if (!bool2) {
        return j;
      }
      return false;
    }
    localObject = "FEEDBACK_HAS_ASKED_SEARCH";
    boolean bool2 = Settings.e((String)localObject);
    if (!bool2)
    {
      localObject = "GOOGLE_REVIEW_ASK_TIMESTAMP";
      bool2 = Settings.a((String)localObject);
      if (bool2)
      {
        localObject = "GOOGLE_REVIEW_ASK_TIMESTAMP";
        l1 = 259200000L;
        bool2 = Settings.b((String)localObject, l1);
        if (!bool2)
        {
          localObject = "INVITE_LAST_ASKED";
          bool2 = Settings.a((String)localObject);
          if (bool2)
          {
            localObject = "INVITE_LAST_ASKED";
            bool2 = Settings.b((String)localObject, l1);
            if (!bool2) {}
          }
        }
        else
        {
          localObject = "FEEDBACK_LAST_DISMISSED";
          l1 = 2592000000L;
          bool2 = Settings.b((String)localObject, l1);
          if (bool2) {
            return j;
          }
        }
      }
    }
    return false;
  }
  
  public final boolean shouldShowInviteFriends()
  {
    Object localObject = a.F();
    boolean bool = ((a)localObject).o();
    if (!bool) {
      return false;
    }
    localObject = Settings.d("INVITE_PEOPLE_FIRST_CHECKED");
    long l1 = ((Long)localObject).longValue();
    long l2 = 0L;
    bool = l1 < l2;
    if (!bool)
    {
      localObject = "INVITE_PEOPLE_FIRST_CHECKED";
      Settings.g((String)localObject);
    }
    localObject = GLOBAL_SEARCH_HISTORY;
    if (this == localObject)
    {
      localObject = "counterFacebookInvite";
      l1 = 3;
      bool = Settings.c((String)localObject, l1);
      if (bool)
      {
        localObject = "INVITE_LAST_ASKED";
        l1 = 86400000L;
        bool = Settings.b((String)localObject, l1);
        if (bool)
        {
          localObject = "INVITE_LAST_DISMISSED";
          l1 = 1209600000L;
          bool = Settings.b((String)localObject, l1);
          if (bool) {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  public final boolean shouldShowShare()
  {
    Object localObject = a.F();
    boolean bool = ((a)localObject).o();
    if (!bool) {
      return false;
    }
    localObject = "FEEDBACK_LIKES_TRUECALLER";
    bool = Settings.e((String)localObject);
    if (bool)
    {
      localObject = "FEEDBACK_DISMISSED_COUNT";
      long l = 2;
      bool = Settings.c((String)localObject, l);
      if (!bool)
      {
        localObject = "HAS_SHARED";
        bool = Settings.e((String)localObject);
        if (!bool)
        {
          localObject = "GOOGLE_REVIEW_ASK_TIMESTAMP";
          l = 604800000L;
          bool = Settings.b((String)localObject, l);
          if (bool) {
            return true;
          }
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.FeedbackItemView.DisplaySource
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */