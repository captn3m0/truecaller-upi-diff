package com.truecaller.ui.components;

import android.content.Context;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.os.SystemClock;
import android.support.v4.view.r;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import com.truecaller.R.styleable;
import com.truecaller.util.at;
import java.util.Stack;

public class CyclicProgressBar
  extends View
{
  private static final Interpolator a;
  private static boolean b = true;
  private float c;
  private float d;
  private float e;
  private float f;
  private RectF g;
  private Paint h;
  private float i;
  private long j;
  private boolean k;
  private boolean l;
  private boolean m;
  private final Runnable n;
  private final Runnable o;
  private Stack p;
  
  static
  {
    AccelerateDecelerateInterpolator localAccelerateDecelerateInterpolator = new android/view/animation/AccelerateDecelerateInterpolator;
    localAccelerateDecelerateInterpolator.<init>();
    a = localAccelerateDecelerateInterpolator;
  }
  
  public CyclicProgressBar(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    Object localObject1 = new android/graphics/RectF;
    ((RectF)localObject1).<init>();
    g = ((RectF)localObject1);
    long l1 = -1;
    j = l1;
    int i1 = 0;
    float f1 = 0.0F;
    localObject1 = null;
    k = false;
    l = false;
    m = false;
    Object localObject2 = new com/truecaller/ui/components/CyclicProgressBar$1;
    ((CyclicProgressBar.1)localObject2).<init>(this);
    n = ((Runnable)localObject2);
    localObject2 = new com/truecaller/ui/components/CyclicProgressBar$2;
    ((CyclicProgressBar.2)localObject2).<init>(this);
    o = ((Runnable)localObject2);
    localObject2 = new java/util/Stack;
    ((Stack)localObject2).<init>();
    p = ((Stack)localObject2);
    boolean bool = isInEditMode();
    float f2 = 4.0F;
    int i2;
    float f3;
    if (bool)
    {
      i2 = -7829368;
      f3 = 0.0F / 0.0F;
      i = f2;
    }
    else
    {
      localObject2 = paramContext.getTheme();
      int[] arrayOfInt = R.styleable.CyclicProgressBar;
      paramAttributeSet = ((Resources.Theme)localObject2).obtainStyledAttributes(paramAttributeSet, arrayOfInt, 0, 0);
      i1 = 3;
      f1 = 4.2E-45F;
    }
    try
    {
      i2 = at.a(paramContext, f2);
      f3 = i2;
      f3 = paramAttributeSet.getDimension(i1, f3);
      i = f3;
      i2 = 2;
      f3 = 2.8E-45F;
      i1 = -1;
      f1 = 0.0F / 0.0F;
      i2 = paramAttributeSet.getColor(i2, i1);
      paramAttributeSet.recycle();
      paramAttributeSet = new android/graphics/Paint;
      paramAttributeSet.<init>();
      h = paramAttributeSet;
      paramAttributeSet = h;
      localObject1 = Paint.Style.STROKE;
      paramAttributeSet.setStyle((Paint.Style)localObject1);
      paramAttributeSet = h;
      f1 = i;
      paramAttributeSet.setStrokeWidth(f1);
      paramAttributeSet = h;
      localObject1 = Paint.Cap.ROUND;
      paramAttributeSet.setStrokeCap((Paint.Cap)localObject1);
      h.setColor(i2);
      h.setAntiAlias(true);
      return;
    }
    finally
    {
      paramAttributeSet.recycle();
    }
  }
  
  public static void setAnimationEnabled(boolean paramBoolean)
  {
    b = paramBoolean;
  }
  
  public final void a()
  {
    Object localObject = p;
    int i1 = ((Stack)localObject).size();
    if (i1 == 0)
    {
      long l1 = -1;
      j = l1;
      m = false;
      k = false;
      localObject = o;
      removeCallbacks((Runnable)localObject);
      boolean bool = l;
      if (!bool)
      {
        localObject = n;
        long l2 = 500L;
        postDelayed((Runnable)localObject, l2);
        bool = true;
        l = bool;
      }
    }
    p.push(null);
  }
  
  public final void a(boolean paramBoolean)
  {
    Stack localStack1 = p;
    int i1 = localStack1.size();
    int i2;
    if ((i1 > 0) || (paramBoolean))
    {
      Stack localStack2;
      if (paramBoolean)
      {
        localStack2 = p;
        localStack2.clear();
      }
      else
      {
        localStack2 = p;
        localStack2.pop();
      }
      i2 = 1;
      if ((i1 == i2) || (paramBoolean))
      {
        m = i2;
        l = false;
        localRunnable = n;
        removeCallbacks(localRunnable);
        long l1 = System.currentTimeMillis();
        long l2 = j;
        l1 -= l2;
        long l3 = 600L;
        paramBoolean = l1 < l3;
        if (!paramBoolean) {
          break label173;
        }
        long l4 = -1;
        paramBoolean = l2 < l4;
        if (!paramBoolean) {
          break label173;
        }
        paramBoolean = k;
        if (!paramBoolean)
        {
          localRunnable = o;
          l3 -= l1;
          postDelayed(localRunnable, l3);
          k = i2;
        }
      }
    }
    return;
    label173:
    Runnable localRunnable = o;
    removeCallbacks(localRunnable);
    localRunnable = o;
    postDelayed(localRunnable, 100);
    k = i2;
  }
  
  public void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    Runnable localRunnable = o;
    removeCallbacks(localRunnable);
    localRunnable = n;
    removeCallbacks(localRunnable);
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    long l1 = SystemClock.elapsedRealtime();
    long l2 = 2000L;
    long l3 = l1 % l2;
    float f1 = (float)l3;
    float f2 = f1 / 2000.0F;
    int i1 = 1135869952;
    float f3 = 360.0F;
    float f4 = f2 * f3;
    c = f4;
    l1 /= l2;
    float f5 = (float)l1;
    float f6 = 225.0F;
    f5 *= f6;
    float f7 = (int)(f5 / f3) * 360;
    f5 -= f7;
    f = f5;
    int i2 = 1133903872;
    f5 = 300.0F;
    f7 = 270.0F;
    float f8 = 0.85F;
    boolean bool3 = f2 < f8;
    Interpolator localInterpolator;
    if (!bool3)
    {
      int i3 = 1154777088;
      f8 = 1700.0F;
      f1 = (f1 - f8) / f5;
      localInterpolator = a;
      f5 = localInterpolator.getInterpolation(f1) * f6;
      f5 = f7 - f5;
      d = f5;
      f5 = d;
      f7 -= f5;
      e = f7;
    }
    else
    {
      f8 = 0.5F;
      boolean bool4 = f2 < f8;
      if (!bool4)
      {
        d = f7;
      }
      else
      {
        f7 = 45.0F;
        int i4 = 1051931443;
        f8 = 0.35F;
        boolean bool1 = f2 < f8;
        if (!bool1)
        {
          i4 = 1143930880;
          f8 = 700.0F;
          f1 = (f1 - f8) / f5;
          localInterpolator = a;
          f5 = localInterpolator.getInterpolation(f1) * f6 + f7;
          d = f5;
        }
        else
        {
          boolean bool2 = f2 < f8;
          if (bool2)
          {
            d = f7;
            bool2 = false;
            f5 = 0.0F;
            localInterpolator = null;
            e = 0.0F;
          }
        }
      }
    }
    RectF localRectF = g;
    f5 = c;
    f6 = f;
    f5 += f6;
    f6 = e;
    f8 = f5 + f6;
    f1 = d;
    f2 = 0.0F;
    Paint localPaint = h;
    paramCanvas.drawArc(localRectF, f8, f1, false, localPaint);
    boolean bool5 = b;
    if (bool5) {
      r.e(this);
    }
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    RectF localRectF = g;
    float f1 = i;
    float f2 = 0.5F;
    float f3 = f1 * f2;
    float f4 = f1 * f2;
    float f5 = paramInt1;
    float f6 = f1 * f2;
    f5 -= f6;
    float f7 = paramInt2;
    f1 *= f2;
    f7 -= f1;
    localRectF.set(f3, f4, f5, f7);
    g.inset(f2, f2);
  }
  
  public void setStrokeColor(int paramInt)
  {
    Paint localPaint = h;
    if (localPaint != null) {
      localPaint.setColor(paramInt);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.CyclicProgressBar
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */