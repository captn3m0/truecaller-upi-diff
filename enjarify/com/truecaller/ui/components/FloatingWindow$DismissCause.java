package com.truecaller.ui.components;

public enum FloatingWindow$DismissCause
{
  static
  {
    Object localObject = new com/truecaller/ui/components/FloatingWindow$DismissCause;
    ((DismissCause)localObject).<init>("UNDEFINED", 0);
    UNDEFINED = (DismissCause)localObject;
    localObject = new com/truecaller/ui/components/FloatingWindow$DismissCause;
    int i = 1;
    ((DismissCause)localObject).<init>("MANUAL", i);
    MANUAL = (DismissCause)localObject;
    localObject = new com/truecaller/ui/components/FloatingWindow$DismissCause;
    int j = 2;
    ((DismissCause)localObject).<init>("AUTOMATIC", j);
    AUTOMATIC = (DismissCause)localObject;
    localObject = new DismissCause[3];
    DismissCause localDismissCause = UNDEFINED;
    localObject[0] = localDismissCause;
    localDismissCause = MANUAL;
    localObject[i] = localDismissCause;
    localDismissCause = AUTOMATIC;
    localObject[j] = localDismissCause;
    $VALUES = (DismissCause[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.FloatingWindow.DismissCause
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */