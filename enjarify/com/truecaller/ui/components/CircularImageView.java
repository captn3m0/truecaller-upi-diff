package com.truecaller.ui.components;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.app.a;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.truecaller.R.styleable;
import com.truecaller.common.h.am;
import com.truecaller.data.entity.Contact;
import com.truecaller.util.at;
import com.truecaller.util.aw;
import com.truecaller.util.aw.e;

public class CircularImageView
  extends AppCompatImageView
  implements aw.e
{
  private boolean a;
  private int b;
  private String c;
  private int d;
  private float e;
  private Typeface f;
  private BitmapShader g;
  private Bitmap h;
  private Paint i;
  private Paint j;
  private Contact k;
  private aw l;
  private aw.e m;
  
  public CircularImageView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private CircularImageView(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet, paramByte);
    Object localObject = aw.b(paramContext).a(0, 0);
    l = ((aw)localObject);
    localObject = new android/graphics/Paint;
    ((Paint)localObject).<init>();
    i = ((Paint)localObject);
    localObject = i;
    int n = 1;
    ((Paint)localObject).setAntiAlias(n);
    localObject = R.styleable.CircularImageView;
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject, paramByte, 0);
    int i1 = getResources().getColor(17170443);
    i1 = paramContext.getColor(n, i1);
    d = i1;
    paramAttributeSet = paramContext.getString(0);
    c = paramAttributeSet;
    paramAttributeSet = getResources();
    float f1 = paramAttributeSet.getDimension(2131165383);
    paramByte = 2;
    f1 = paramContext.getDimension(paramByte, f1);
    e = f1;
    a = false;
    paramContext.recycle();
    paramContext = Typeface.create("sans-serif-light", 0);
    f = paramContext;
    paramContext = f;
    if (paramContext == null)
    {
      paramContext = Typeface.create("sans-serif", 0);
      f = paramContext;
    }
  }
  
  private void a()
  {
    int n = b;
    if (n > 0) {
      b();
    }
  }
  
  private void b()
  {
    Object localObject1 = h;
    if (localObject1 == null)
    {
      g = null;
      return;
    }
    localObject1 = getScaleType();
    Object localObject2 = ImageView.ScaleType.CENTER_CROP;
    if (localObject1 != localObject2)
    {
      localObject1 = new android/graphics/BitmapShader;
      localObject2 = h;
      int n = b;
      localObject2 = Bitmap.createScaledBitmap((Bitmap)localObject2, n, n, false);
      localTileMode = Shader.TileMode.CLAMP;
      ((BitmapShader)localObject1).<init>((Bitmap)localObject2, localTileMode, localTileMode);
      g = ((BitmapShader)localObject1);
      return;
    }
    localObject1 = new android/graphics/BitmapShader;
    localObject2 = h;
    Shader.TileMode localTileMode = Shader.TileMode.CLAMP;
    ((BitmapShader)localObject1).<init>((Bitmap)localObject2, localTileMode, localTileMode);
    g = ((BitmapShader)localObject1);
  }
  
  private void b(Contact paramContact)
  {
    k = paramContact;
    l.a(this);
    setImageBitmap(null);
    setBackgroundResource(2131233847);
  }
  
  public final void a(ImageView paramImageView)
  {
    aw.e locale = m;
    if (locale != null) {
      locale.a(paramImageView);
    }
  }
  
  public final void a(ImageView paramImageView, Bitmap paramBitmap, String paramString)
  {
    if (paramBitmap != null)
    {
      setText("");
      locale = null;
      setBackgroundResource(0);
    }
    aw.e locale = m;
    if (locale != null) {
      locale.a(paramImageView, paramBitmap, paramString);
    }
  }
  
  public final void a(Contact paramContact)
  {
    k = paramContact;
    l.a(this);
    Object localObject = k;
    boolean bool = ((Contact)localObject).U();
    if (bool)
    {
      setBackgroundResource(0);
      setImageResource(2131233883);
      setText("");
      return;
    }
    b(paramContact);
    localObject = k.z();
    bool = am.b((CharSequence)localObject);
    if (bool) {
      return;
    }
    localObject = ImageView.ScaleType.FIT_CENTER;
    setScaleType((ImageView.ScaleType)localObject);
    l.a(paramContact, this, this);
  }
  
  public final void b(ImageView paramImageView)
  {
    aw.e locale = m;
    if (locale != null) {
      locale.b(paramImageView);
    }
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool = isClickable();
    if (!bool) {
      return super.onTouchEvent(paramMotionEvent);
    }
    paramMotionEvent.getAction();
    invalidate();
    return super.dispatchTouchEvent(paramMotionEvent);
  }
  
  public void invalidate()
  {
    super.invalidate();
    a();
  }
  
  public void invalidate(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.invalidate(paramInt1, paramInt2, paramInt3, paramInt4);
    a();
  }
  
  public void invalidate(Rect paramRect)
  {
    super.invalidate(paramRect);
    a();
  }
  
  public void onDraw(Canvas paramCanvas)
  {
    Object localObject1 = c;
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1);
    if (bool1)
    {
      localObject1 = h;
      if (localObject1 != null)
      {
        n = ((Bitmap)localObject1).getHeight();
        if (n != 0)
        {
          localObject1 = h;
          n = ((Bitmap)localObject1).getWidth();
          if (n != 0) {
            break label47;
          }
        }
      }
      return;
    }
    label47:
    int n = b;
    int i1 = getWidth();
    b = i1;
    i1 = getHeight();
    int i2 = b;
    if (i1 < i2)
    {
      i1 = getHeight();
      b = i1;
    }
    i1 = b;
    if (n != i1) {
      b();
    }
    n = 0;
    float f1 = 0.0F;
    localObject1 = null;
    i1 = b / 2;
    boolean bool2 = a;
    float f2;
    Object localObject2;
    if (bool2)
    {
      localObject1 = j;
      f1 = ((Paint)localObject1).getStrokeWidth();
      n = (int)f1;
      f2 = i1;
      i4 = b / 2 - n;
      f3 = i4;
      localObject2 = j;
      paramCanvas.drawCircle(f2, f2, f3, (Paint)localObject2);
    }
    Object localObject3 = h;
    int i4 = 0;
    float f3 = 0.0F;
    Object localObject4 = null;
    int i5;
    float f4;
    int i3;
    if (localObject3 != null)
    {
      localObject3 = getScaleType();
      localObject2 = ImageView.ScaleType.CENTER_CROP;
      Object localObject5;
      if (localObject3 != localObject2)
      {
        localObject3 = i;
        localObject2 = g;
        ((Paint)localObject3).setShader((Shader)localObject2);
        f2 = i1;
        i5 = b / 2;
        f4 = i5;
        localObject5 = i;
        paramCanvas.drawCircle(f2, f2, f4, (Paint)localObject5);
      }
      else
      {
        localObject3 = i;
        ((Paint)localObject3).setShader(null);
        i3 = i1 + n;
        localObject2 = h;
        i5 = ((Bitmap)localObject2).getWidth() / 2;
        int i6 = h.getHeight() / 2;
        i5 = i3 - i5;
        i3 -= i6;
        localObject5 = h;
        f4 = i5;
        f2 = i3;
        Paint localPaint = i;
        paramCanvas.drawBitmap((Bitmap)localObject5, f4, f2, localPaint);
      }
    }
    localObject3 = c;
    if (localObject3 != null)
    {
      i3 = ((String)localObject3).length();
      if (i3 > 0)
      {
        i.setShader(null);
        localObject3 = i;
        localObject4 = f;
        ((Paint)localObject3).setTypeface((Typeface)localObject4);
        i1 += n;
        localObject1 = i;
        f2 = e;
        ((Paint)localObject1).setTextSize(f2);
        localObject1 = i;
        i3 = d;
        ((Paint)localObject1).setColor(i3);
        localObject1 = i;
        localObject3 = c;
        f1 = ((Paint)localObject1).measureText((String)localObject3);
        f2 = i.getFontMetrics().top;
        localObject4 = c;
        float f5 = i1;
        f1 /= 2.0F;
        f1 = f5 - f1;
        i5 = 1077936128;
        f4 = 3.0F;
        f2 /= f4;
        f5 -= f2;
        localObject3 = i;
        paramCanvas.drawText((String)localObject4, f1, f5, (Paint)localObject3);
      }
    }
  }
  
  public void setBorderColor(int paramInt)
  {
    Paint localPaint = j;
    if (localPaint != null) {
      localPaint.setColor(paramInt);
    }
  }
  
  public void setBorderWidth(int paramInt)
  {
    Paint localPaint = j;
    if (localPaint != null)
    {
      float f1 = paramInt;
      localPaint.setStrokeWidth(f1);
    }
    requestLayout();
  }
  
  public void setHasBorder(boolean paramBoolean)
  {
    a = paramBoolean;
    Paint localPaint = j;
    if (localPaint == null)
    {
      localPaint = new android/graphics/Paint;
      localPaint.<init>();
      j = localPaint;
      localPaint = j;
      boolean bool = true;
      localPaint.setAntiAlias(bool);
      localPaint = j;
      Paint.Style localStyle = Paint.Style.STROKE;
      localPaint.setStyle(localStyle);
    }
  }
  
  public void setImageBitmap(Bitmap paramBitmap)
  {
    if (paramBitmap != null)
    {
      String str = "";
      setText(str);
    }
    h = paramBitmap;
    invalidate();
  }
  
  public void setImageDrawable(Drawable paramDrawable)
  {
    paramDrawable = at.a(paramDrawable);
    setImageBitmap(paramDrawable);
  }
  
  public void setImageResource(int paramInt)
  {
    Bitmap localBitmap = at.a(a.a(getContext(), paramInt));
    setImageBitmap(localBitmap);
  }
  
  public void setListener(aw.e parame)
  {
    m = parame;
  }
  
  public void setText(String paramString)
  {
    c = paramString;
    invalidate();
  }
  
  public void setTextColor(int paramInt)
  {
    d = paramInt;
  }
  
  public void setTypeface(Typeface paramTypeface)
  {
    f = paramTypeface;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.CircularImageView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */