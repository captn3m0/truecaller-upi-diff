package com.truecaller.ui.components;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

final class FeedbackItemView$3
  extends AnimatorListenerAdapter
{
  FeedbackItemView$3(FeedbackItemView paramFeedbackItemView) {}
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    FeedbackItemView.c(a);
    paramAnimator = FeedbackItemView.d(a);
    if (paramAnimator != null)
    {
      paramAnimator = FeedbackItemView.d(a);
      FeedbackItemView.FeedbackItem localFeedbackItem = FeedbackItemView.e(a);
      paramAnimator.c(localFeedbackItem);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.FeedbackItemView.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */