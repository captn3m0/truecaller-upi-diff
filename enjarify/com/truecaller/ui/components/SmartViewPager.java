package com.truecaller.ui.components;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ViewParent;

public class SmartViewPager
  extends ViewPager
{
  private GestureDetector p;
  private boolean q = false;
  
  public SmartViewPager(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    GestureDetector localGestureDetector = new android/view/GestureDetector;
    SmartViewPager.a locala = new com/truecaller/ui/components/SmartViewPager$a;
    locala.<init>(this, (byte)0);
    localGestureDetector.<init>(paramContext, locala);
    p = localGestureDetector;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool1 = q;
    if (!bool1)
    {
      localObject = p;
      bool1 = ((GestureDetector)localObject).onTouchEvent(paramMotionEvent);
      q = bool1;
    }
    int i = paramMotionEvent.getAction();
    int j = 1;
    if (i == j)
    {
      i = 0;
      localObject = null;
      q = false;
    }
    Object localObject = getParent();
    boolean bool2 = q;
    ((ViewParent)localObject).requestDisallowInterceptTouchEvent(bool2);
    return super.onTouchEvent(paramMotionEvent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.SmartViewPager
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */