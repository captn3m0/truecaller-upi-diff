package com.truecaller.ui.components;

import android.text.TextUtils;
import android.widget.Filter;
import android.widget.Filter.FilterResults;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class l$a
  extends Filter
{
  private List b;
  
  private l$a(l paraml) {}
  
  private void a()
  {
    Object localObject = b;
    if (localObject == null)
    {
      localObject = new java/util/ArrayList;
      ((ArrayList)localObject).<init>();
      b = ((List)localObject);
      int i = 0;
      localObject = null;
      l locall = a;
      int j = locall.getCount();
      while (i < j)
      {
        n localn = a.a(i);
        if (localn != null)
        {
          List localList = b;
          localList.add(localn);
        }
        i += 1;
      }
    }
  }
  
  protected final Filter.FilterResults performFiltering(CharSequence paramCharSequence)
  {
    Filter.FilterResults localFilterResults = new android/widget/Filter$FilterResults;
    localFilterResults.<init>();
    a();
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    boolean bool1 = TextUtils.isEmpty(paramCharSequence);
    if (bool1)
    {
      paramCharSequence = b;
    }
    else
    {
      Object localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("(");
      Object localObject2 = Pattern.quote(paramCharSequence.toString());
      ((StringBuilder)localObject1).append((String)localObject2);
      ((StringBuilder)localObject1).append(")");
      localObject1 = ((StringBuilder)localObject1).toString();
      int i = 2;
      localObject1 = Pattern.compile((String)localObject1, i);
      localObject2 = new java/util/ArrayList;
      ((ArrayList)localObject2).<init>();
      int j = 0;
      List localList = b;
      int k = localList.size();
      while (j < k)
      {
        n localn = (n)b.get(j);
        Object localObject3 = localn;
        localObject3 = ((n.c)localn).a();
        Object localObject4 = ((Pattern)localObject1).matcher((CharSequence)localObject3);
        boolean bool2 = ((Matcher)localObject4).find();
        boolean bool3;
        if (bool2)
        {
          localObject3 = ((String)localObject3).toLowerCase();
          localObject4 = paramCharSequence.toString().toLowerCase();
          bool3 = ((String)localObject3).startsWith((String)localObject4);
          if (bool3) {
            localArrayList.add(localn);
          } else {
            ((List)localObject2).add(localn);
          }
        }
        else
        {
          localObject3 = localn.toString();
          bool3 = ((String)localObject3).contains(paramCharSequence);
          if (bool3) {
            ((List)localObject2).add(localn);
          }
        }
        j += 1;
      }
      paramCharSequence = (CharSequence)localObject2;
    }
    localArrayList.addAll(paramCharSequence);
    int m = localArrayList.size();
    count = m;
    values = localArrayList;
    return localFilterResults;
  }
  
  protected final void publishResults(CharSequence paramCharSequence, Filter.FilterResults paramFilterResults)
  {
    if (paramFilterResults != null)
    {
      paramCharSequence = a;
      l locall = null;
      paramCharSequence.setNotifyOnChange(false);
      a.clear();
      paramCharSequence = (List)values;
      if (paramCharSequence != null)
      {
        int i = paramCharSequence.size();
        if (i > 0)
        {
          paramCharSequence = paramCharSequence.iterator();
          for (;;)
          {
            boolean bool = paramCharSequence.hasNext();
            if (!bool) {
              break;
            }
            paramFilterResults = (n)paramCharSequence.next();
            locall = a;
            locall.add(paramFilterResults);
          }
        }
      }
      a.setNotifyOnChange(true);
      a.notifyDataSetChanged();
      return;
    }
    a.notifyDataSetInvalidated();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.l.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */