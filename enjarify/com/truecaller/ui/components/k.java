package com.truecaller.ui.components;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil.AlwaysFatal;

public abstract class k
  extends RelativeLayout
{
  private final Runnable a;
  private final Runnable b;
  protected final AvatarView c;
  private final View d;
  
  public k(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet, 0);
    paramAttributeSet = new com/truecaller/ui/components/k$1;
    paramAttributeSet.<init>(this);
    a = paramAttributeSet;
    paramAttributeSet = new com/truecaller/ui/components/k$2;
    paramAttributeSet.<init>(this);
    b = paramAttributeSet;
    a(paramContext);
    paramContext = (AvatarView)findViewById(2131362068);
    c = paramContext;
    int i = 2131362071;
    paramContext = findViewById(i);
    d = paramContext;
    paramContext = c;
    if (paramContext != null)
    {
      i = 1;
    }
    else
    {
      i = 0;
      paramContext = null;
    }
    paramAttributeSet = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(i, paramAttributeSet);
    paramContext = c;
    paramAttributeSet = new com/truecaller/ui/components/k$3;
    paramAttributeSet.<init>(this);
    paramContext.setForcedLoadListener(paramAttributeSet);
  }
  
  protected abstract void a();
  
  protected abstract void a(Context paramContext);
  
  protected abstract void a(Uri paramUri, boolean paramBoolean);
  
  protected abstract void a(Contact paramContact, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3);
  
  public final void a(boolean paramBoolean)
  {
    View localView = d;
    if (localView != null)
    {
      long l = 200L;
      if (paramBoolean)
      {
        localRunnable = b;
        removeCallbacks(localRunnable);
        localRunnable = a;
        postDelayed(localRunnable, l);
        return;
      }
      Runnable localRunnable = a;
      removeCallbacks(localRunnable);
      localRunnable = b;
      postDelayed(localRunnable, l);
    }
  }
  
  protected abstract void b();
  
  public final void b(Contact paramContact, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    boolean bool1 = paramContact.a(16);
    c.a(paramContact);
    a(paramContact, paramBoolean1, paramBoolean2, paramBoolean3);
    b localb = new com/truecaller/ui/components/b;
    paramBoolean3 = true;
    Uri localUri1 = paramContact.a(paramBoolean3);
    Uri localUri2 = paramContact.a(false);
    int i = 32;
    boolean bool2 = paramContact.a(i);
    boolean bool3 = paramContact.a(4);
    localb.<init>(localUri1, localUri2, bool2, bool3);
    boolean bool4 = paramContact.a(i);
    if ((bool4) && (!paramBoolean2))
    {
      d();
      c.a(localb);
      return;
    }
    if (bool1)
    {
      c();
      c.a(localb);
      return;
    }
    if (paramBoolean2)
    {
      c.a();
      a();
      return;
    }
    paramContact = paramContact.a(paramBoolean3);
    AvatarView localAvatarView = c;
    localAvatarView.a(localb);
    if (paramContact != null)
    {
      a(paramContact, false);
      return;
    }
    b();
  }
  
  protected abstract void c();
  
  protected abstract void d();
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */