package com.truecaller.ui.components;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.log.AssertionUtil;
import com.truecaller.tracking.events.b;
import com.truecaller.tracking.events.b.a;
import org.apache.a.d.d;

final class q$1
  extends AdListener
{
  q$1(q paramq) {}
  
  public final void onAdFailedToLoad(int paramInt)
  {
    int i = 3;
    Object localObject1 = new String[i];
    com.truecaller.ads.provider.a locala = null;
    localObject1[0] = "ACS-ads";
    localObject1[1] = "bannerAdFailed";
    Object localObject2 = String.valueOf(paramInt);
    int j = 2;
    localObject1[j] = localObject2;
    localObject1 = q.b(a);
    if (localObject1 == null) {
      return;
    }
    localObject2 = a;
    if (localObject2 != null)
    {
      locala = q.d(a);
      String str = ((PublisherAdView)localObject2).getAdUnitId();
      localObject2 = ((PublisherAdView)localObject2).getAdSize();
      locala.a(str, (AdSize)localObject2, paramInt);
    }
    ((q.b)localObject1).c();
  }
  
  public final void onAdLeftApplication()
  {
    Object localObject1 = q.a(a);
    if (localObject1 != null)
    {
      localObject1 = q.b(a);
      if (localObject1 != null)
      {
        localObject1 = ba).a;
        if (localObject1 != null)
        {
          Object localObject2 = ((PublisherAdView)localObject1).getAdSize();
          localObject1 = ((PublisherAdView)localObject1).getAdUnitId();
          q.d(a).c((String)localObject1, (AdSize)localObject2);
          f localf = q.e(a);
          String str = "banner";
          localObject2 = ((AdSize)localObject2).toString();
          b.a locala1 = b.b();
          localObject1 = locala1.b("afterCall").a((CharSequence)localObject1);
          localObject1 = ((b.a)localObject1).a(null).c(str).d((CharSequence)localObject2);
          try
          {
            localObject2 = localf.a();
            localObject2 = (ae)localObject2;
            localObject1 = ((b.a)localObject1).a();
            ((ae)localObject2).a((d)localObject1);
            return;
          }
          catch (org.apache.a.a locala)
          {
            AssertionUtil.reportThrowableButNeverCrash(locala);
          }
        }
      }
    }
  }
  
  public final void onAdLoaded()
  {
    String str = "bannerAdLoaded";
    { "ACS-ads" }[1] = str;
    Object localObject = q.a(a);
    if (localObject != null)
    {
      localObject = q.b(a);
      if (localObject != null)
      {
        localObject = ba).a;
        if (localObject != null)
        {
          { "ACS-ads" }[1] = "bannerAdDisplayed";
          ba).a.setVisibility(0);
          localObject = q.d(a);
          str = q.c(a);
          AdSize localAdSize = ba).a.getAdSize();
          ((com.truecaller.ads.provider.a)localObject).b(str, localAdSize);
          localObject = q.a(a);
          ((q.a)localObject).a();
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.q.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */