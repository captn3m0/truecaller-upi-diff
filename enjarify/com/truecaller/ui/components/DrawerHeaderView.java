package com.truecaller.ui.components;

import android.content.Context;
import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Group;
import android.support.v4.view.n;
import android.support.v4.view.r;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.TrueApp;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.util.at;
import com.truecaller.utils.extensions.t;
import com.truecaller.utils.ui.b;
import java.util.HashMap;

public final class DrawerHeaderView
  extends ConstraintLayout
  implements View.OnClickListener
{
  private DrawerHeaderView.a k;
  private HashMap l;
  
  public DrawerHeaderView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private DrawerHeaderView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, (byte)0);
    paramAttributeSet = this;
    paramAttributeSet = (ViewGroup)this;
    ConstraintLayout.inflate(paramContext, 2131558606, paramAttributeSet);
    int i = b.a(paramContext, 2130968790);
    setBackgroundColor(i);
    paramContext = this;
    paramContext = (View)this;
    paramAttributeSet = new com/truecaller/ui/components/DrawerHeaderView$1;
    paramAttributeSet.<init>(this);
    paramAttributeSet = (n)paramAttributeSet;
    r.a(paramContext, paramAttributeSet);
  }
  
  private View c(int paramInt)
  {
    Object localObject1 = l;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      l = ((HashMap)localObject1);
    }
    localObject1 = l;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = l;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a(String paramString1, String paramString2, Uri paramUri)
  {
    k.b(paramString1, "name");
    k.b(paramString2, "number");
    int i = R.id.groupProfile;
    Object localObject = (Group)c(i);
    k.a(localObject, "groupProfile");
    t.a((View)localObject);
    i = R.id.groupNoProfile;
    localObject = (Group)c(i);
    k.a(localObject, "groupNoProfile");
    t.a((View)localObject, false);
    i = R.id.name;
    localObject = (TextView)c(i);
    String str = "this.name";
    k.a(localObject, str);
    paramString1 = (CharSequence)paramString1;
    ((TextView)localObject).setText(paramString1);
    int j = R.id.number;
    paramString1 = (TextView)c(j);
    localObject = "this.number";
    k.a(paramString1, (String)localObject);
    paramString2 = (CharSequence)at.a((CharSequence)paramString2);
    paramString1.setText(paramString2);
    j = 0;
    paramString1 = null;
    if (paramUri == null)
    {
      m = R.id.avatar;
      ((ContactPhoto)c(m)).setImageDrawable(null);
      m = R.id.avatarAddPhoto;
      paramString2 = (ImageView)c(m);
      paramUri = "avatarAddPhoto";
      k.a(paramString2, paramUri);
      paramString2 = (View)paramString2;
      t.a(paramString2);
    }
    else
    {
      m = R.id.avatar;
      ((ContactPhoto)c(m)).a(paramUri, null);
      m = R.id.avatarAddPhoto;
      paramString2 = (ImageView)c(m);
      paramUri = "avatarAddPhoto";
      k.a(paramString2, paramUri);
      paramString2 = (View)paramString2;
      t.a(paramString2, false);
    }
    int m = R.id.avatar;
    paramString2 = (ContactPhoto)c(m);
    paramUri = this;
    paramUri = (View.OnClickListener)this;
    paramString2.setOnClickListener(paramUri);
    m = R.id.edit;
    ((LinearLayout)c(m)).setOnClickListener(paramUri);
    setOnClickListener(null);
    j = R.id.editText;
    ((TextView)c(j)).setText(2131886964);
  }
  
  public final void b()
  {
    int i = R.id.groupProfile;
    Object localObject1 = (Group)c(i);
    k.a(localObject1, "groupProfile");
    t.a((View)localObject1, false);
    i = R.id.avatarAddPhoto;
    localObject1 = (ImageView)c(i);
    k.a(localObject1, "avatarAddPhoto");
    t.a((View)localObject1, false);
    i = R.id.groupNoProfile;
    localObject1 = (Group)c(i);
    k.a(localObject1, "groupNoProfile");
    t.a((View)localObject1);
    i = R.id.avatar;
    ((ContactPhoto)c(i)).setOnClickListener(null);
    i = R.id.edit;
    localObject1 = (LinearLayout)c(i);
    Object localObject2 = this;
    localObject2 = (View.OnClickListener)this;
    ((LinearLayout)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    setOnClickListener((View.OnClickListener)localObject2);
    i = R.id.editText;
    ((TextView)c(i)).setText(2131886644);
  }
  
  public final void onClick(View paramView)
  {
    k.b(paramView, "v");
    TrueApp localTrueApp = TrueApp.y();
    boolean bool = localTrueApp.p();
    int j = paramView.getId();
    int m = 2131362926;
    if ((j == m) && (bool))
    {
      paramView = k;
      if (paramView != null) {
        paramView.k();
      }
      return;
    }
    j = paramView.getId();
    if ((j == m) && (!bool))
    {
      paramView = k;
      if (paramView != null) {
        paramView.m();
      }
      return;
    }
    int n = paramView.getId();
    int i = 2131362068;
    if (n == i)
    {
      paramView = k;
      if (paramView != null) {
        paramView.l();
      }
      return;
    }
    paramView = k;
    if (paramView != null)
    {
      paramView.m();
      return;
    }
  }
  
  public final void setDrawerHeaderListener(DrawerHeaderView.a parama)
  {
    k.b(parama, "drawerHeaderListener");
    k = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.DrawerHeaderView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */