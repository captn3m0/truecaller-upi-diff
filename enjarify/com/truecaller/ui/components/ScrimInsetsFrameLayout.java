package com.truecaller.ui.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.view.r;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.truecaller.R.styleable;

public class ScrimInsetsFrameLayout
  extends FrameLayout
{
  private Drawable a;
  private Rect b;
  private Rect c;
  private ScrimInsetsFrameLayout.a d;
  
  public ScrimInsetsFrameLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    Object localObject = new android/graphics/Rect;
    ((Rect)localObject).<init>();
    c = ((Rect)localObject);
    localObject = R.styleable.ScrimInsetsView;
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject, 0, 0);
    if (paramContext != null)
    {
      paramAttributeSet = paramContext.getDrawable(0);
      a = paramAttributeSet;
      paramContext.recycle();
      boolean bool = true;
      setWillNotDraw(bool);
    }
  }
  
  public void draw(Canvas paramCanvas)
  {
    super.draw(paramCanvas);
    int i = getWidth();
    int j = getHeight();
    Object localObject1 = b;
    if (localObject1 != null)
    {
      localObject1 = a;
      if (localObject1 != null)
      {
        int k = paramCanvas.save();
        int m = getScrollX();
        float f1 = m;
        float f2 = getScrollY();
        paramCanvas.translate(f1, f2);
        Object localObject2 = c;
        int n = b.top;
        ((Rect)localObject2).set(0, 0, i, n);
        localObject2 = a;
        Rect localRect1 = c;
        ((Drawable)localObject2).setBounds(localRect1);
        a.draw(paramCanvas);
        localObject2 = c;
        n = b.bottom;
        n = j - n;
        ((Rect)localObject2).set(0, n, i, j);
        localObject2 = a;
        localRect1 = c;
        ((Drawable)localObject2).setBounds(localRect1);
        a.draw(paramCanvas);
        localObject2 = c;
        n = b.top;
        int i1 = b.left;
        Rect localRect2 = b;
        int i2 = bottom;
        i2 = j - i2;
        ((Rect)localObject2).set(0, n, i1, i2);
        localObject2 = a;
        localRect1 = c;
        ((Drawable)localObject2).setBounds(localRect1);
        a.draw(paramCanvas);
        localObject2 = c;
        localRect1 = b;
        n = right;
        n = i - n;
        Rect localRect3 = b;
        int i3 = top;
        Rect localRect4 = b;
        i1 = bottom;
        j -= i1;
        ((Rect)localObject2).set(n, i3, i, j);
        Drawable localDrawable = a;
        Rect localRect5 = c;
        localDrawable.setBounds(localRect5);
        localDrawable = a;
        localDrawable.draw(paramCanvas);
        paramCanvas.restoreToCount(k);
      }
    }
  }
  
  protected boolean fitSystemWindows(Rect paramRect)
  {
    Object localObject = new android/graphics/Rect;
    ((Rect)localObject).<init>(paramRect);
    b = ((Rect)localObject);
    localObject = a;
    boolean bool1 = true;
    boolean bool2;
    if (localObject == null)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      localObject = null;
    }
    setWillNotDraw(bool2);
    r.e(this);
    localObject = d;
    if (localObject != null) {
      ((ScrimInsetsFrameLayout.a)localObject).a(paramRect);
    }
    return bool1;
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    Drawable localDrawable = a;
    if (localDrawable != null) {
      localDrawable.setCallback(this);
    }
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    Drawable localDrawable = a;
    if (localDrawable != null) {
      localDrawable.setCallback(null);
    }
  }
  
  public void setInsetForeground(Drawable paramDrawable)
  {
    a = paramDrawable;
  }
  
  public void setOnInsetsCallback(ScrimInsetsFrameLayout.a parama)
  {
    d = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.ScrimInsetsFrameLayout
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */