package com.truecaller.ui.components;

public enum FeedbackItemView$FeedbackItem$FeedbackItemState
{
  private final int mDismissId;
  private final boolean mFinalState;
  private final int mIconId;
  private final int mMessageId;
  private final int mNegativeId;
  private final int mPositiveId;
  
  static
  {
    Object localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    ((FeedbackItemState)localObject).<init>("QUESTION_ENJOYING", 0, 2131886567, 2131234113);
    QUESTION_ENJOYING = (FeedbackItemState)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    int i = 1;
    int j = 2131234433;
    ((FeedbackItemState)localObject).<init>("QUESTION_ENJOYING_CALLER_ID", i, 2131886569, j);
    QUESTION_ENJOYING_CALLER_ID = (FeedbackItemState)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    int k = 2;
    ((FeedbackItemState)localObject).<init>("QUESTION_ENJOYING_BLOCKED", k, 2131886568, j);
    QUESTION_ENJOYING_BLOCKED = (FeedbackItemState)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    int m = 3;
    ((FeedbackItemState)localObject).<init>("QUESTION_RATE", m, 2131886572, j);
    QUESTION_RATE = (FeedbackItemState)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    j = 4;
    ((FeedbackItemState)localObject).<init>("QUESTION_GIVE_FEEDBACK", j, 2131886570, 2131234195);
    QUESTION_GIVE_FEEDBACK = (FeedbackItemState)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    int n = 5;
    ((FeedbackItemState)localObject).<init>("FEEDBACK_NO", n);
    FEEDBACK_NO = (FeedbackItemState)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    int i1 = 6;
    ((FeedbackItemState)localObject).<init>("RATE_NO", i1);
    RATE_NO = (FeedbackItemState)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    int i2 = 7;
    ((FeedbackItemState)localObject).<init>("FEEDBACK_YES", i2);
    FEEDBACK_YES = (FeedbackItemState)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    int i3 = 8;
    ((FeedbackItemState)localObject).<init>("RATE_YES", i3, false);
    RATE_YES = (FeedbackItemState)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    int i4 = 9;
    ((FeedbackItemState)localObject).<init>("RATE_YES_THANKS", i4, 2131886576, 2131234612);
    RATE_YES_THANKS = (FeedbackItemState)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    int i5 = 10;
    int i6 = 2131234540;
    ((FeedbackItemState)localObject).<init>("QUESTION_SHARE", i5, 2131886573, i6);
    QUESTION_SHARE = (FeedbackItemState)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    ((FeedbackItemState)localObject).<init>("QUESTION_SHARE_CALLER_ID", 11, 2131886575, i6);
    QUESTION_SHARE_CALLER_ID = (FeedbackItemState)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    int i7 = 12;
    ((FeedbackItemState)localObject).<init>("QUESTION_SHARE_BLOCKED", i7, 2131886574, i6);
    QUESTION_SHARE_BLOCKED = (FeedbackItemState)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    ((FeedbackItemState)localObject).<init>("SHARE_NO", 13);
    SHARE_NO = (FeedbackItemState)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    ((FeedbackItemState)localObject).<init>("SHARE_YES", 14);
    SHARE_YES = (FeedbackItemState)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    ((FeedbackItemState)localObject).<init>("QUESTION_INVITE_FRIENDS", 15, 2131886571, 2131234219, 2131886566, -1, 2131886565, false);
    QUESTION_INVITE_FRIENDS = (FeedbackItemState)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    ((FeedbackItemState)localObject).<init>("INVITE_YES", 16);
    INVITE_YES = (FeedbackItemState)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    ((FeedbackItemState)localObject).<init>("INVITE_NO", 17);
    INVITE_NO = (FeedbackItemState)localObject;
    localObject = new com/truecaller/ui/components/FeedbackItemView$FeedbackItem$FeedbackItemState;
    ((FeedbackItemState)localObject).<init>("DUMMY_FINAL", 18);
    DUMMY_FINAL = (FeedbackItemState)localObject;
    localObject = new FeedbackItemState[19];
    FeedbackItemState localFeedbackItemState = QUESTION_ENJOYING;
    localObject[0] = localFeedbackItemState;
    localFeedbackItemState = QUESTION_ENJOYING_CALLER_ID;
    localObject[i] = localFeedbackItemState;
    localFeedbackItemState = QUESTION_ENJOYING_BLOCKED;
    localObject[k] = localFeedbackItemState;
    localFeedbackItemState = QUESTION_RATE;
    localObject[m] = localFeedbackItemState;
    localFeedbackItemState = QUESTION_GIVE_FEEDBACK;
    localObject[j] = localFeedbackItemState;
    localFeedbackItemState = FEEDBACK_NO;
    localObject[n] = localFeedbackItemState;
    localFeedbackItemState = RATE_NO;
    localObject[i1] = localFeedbackItemState;
    localFeedbackItemState = FEEDBACK_YES;
    localObject[i2] = localFeedbackItemState;
    localFeedbackItemState = RATE_YES;
    localObject[i3] = localFeedbackItemState;
    localFeedbackItemState = RATE_YES_THANKS;
    localObject[i4] = localFeedbackItemState;
    localFeedbackItemState = QUESTION_SHARE;
    localObject[i5] = localFeedbackItemState;
    localFeedbackItemState = QUESTION_SHARE_CALLER_ID;
    localObject[11] = localFeedbackItemState;
    localFeedbackItemState = QUESTION_SHARE_BLOCKED;
    localObject[i7] = localFeedbackItemState;
    localFeedbackItemState = SHARE_NO;
    localObject[13] = localFeedbackItemState;
    localFeedbackItemState = SHARE_YES;
    localObject[14] = localFeedbackItemState;
    localFeedbackItemState = QUESTION_INVITE_FRIENDS;
    localObject[15] = localFeedbackItemState;
    localFeedbackItemState = INVITE_YES;
    localObject[16] = localFeedbackItemState;
    localFeedbackItemState = INVITE_NO;
    localObject[17] = localFeedbackItemState;
    localFeedbackItemState = DUMMY_FINAL;
    localObject[18] = localFeedbackItemState;
    $VALUES = (FeedbackItemState[])localObject;
  }
  
  private FeedbackItemView$FeedbackItem$FeedbackItemState()
  {
    this(true);
  }
  
  private FeedbackItemView$FeedbackItem$FeedbackItemState(int paramInt2, int paramInt3)
  {
    this(paramInt2, paramInt3, 2131886564, 2131887214, 2131887235, false);
  }
  
  private FeedbackItemView$FeedbackItem$FeedbackItemState(int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, boolean paramBoolean)
  {
    mMessageId = paramInt2;
    mIconId = paramInt3;
    mDismissId = paramInt4;
    mNegativeId = paramInt5;
    mPositiveId = paramInt6;
    mFinalState = paramBoolean;
  }
  
  private FeedbackItemView$FeedbackItem$FeedbackItemState(boolean paramBoolean)
  {
    this(-1, -1, -1, -1, -1, paramBoolean);
  }
  
  public final int getDismissId()
  {
    return mDismissId;
  }
  
  public final int getIconId()
  {
    return mIconId;
  }
  
  public final int getMessageId()
  {
    return mMessageId;
  }
  
  public final int getNegativeId()
  {
    return mNegativeId;
  }
  
  public final int getPositiveId()
  {
    return mPositiveId;
  }
  
  public final boolean isFeedbackState()
  {
    FeedbackItemState localFeedbackItemState = QUESTION_ENJOYING;
    if (this != localFeedbackItemState)
    {
      localFeedbackItemState = QUESTION_ENJOYING_CALLER_ID;
      if (this != localFeedbackItemState)
      {
        localFeedbackItemState = QUESTION_ENJOYING_BLOCKED;
        if (this != localFeedbackItemState)
        {
          localFeedbackItemState = QUESTION_RATE;
          if (this != localFeedbackItemState)
          {
            localFeedbackItemState = QUESTION_GIVE_FEEDBACK;
            if (this != localFeedbackItemState) {
              return false;
            }
          }
        }
      }
    }
    return true;
  }
  
  public final boolean isInviteState()
  {
    FeedbackItemState localFeedbackItemState = QUESTION_INVITE_FRIENDS;
    if (this != localFeedbackItemState)
    {
      localFeedbackItemState = INVITE_YES;
      if (this != localFeedbackItemState)
      {
        localFeedbackItemState = INVITE_NO;
        if (this != localFeedbackItemState) {
          return false;
        }
      }
    }
    return true;
  }
  
  public final boolean isShareState()
  {
    FeedbackItemState localFeedbackItemState = QUESTION_SHARE;
    if (this != localFeedbackItemState)
    {
      localFeedbackItemState = QUESTION_SHARE_BLOCKED;
      if (this != localFeedbackItemState)
      {
        localFeedbackItemState = QUESTION_SHARE_CALLER_ID;
        if (this != localFeedbackItemState)
        {
          localFeedbackItemState = SHARE_NO;
          if (this != localFeedbackItemState)
          {
            localFeedbackItemState = SHARE_YES;
            if (this != localFeedbackItemState) {
              return false;
            }
          }
        }
      }
    }
    return true;
  }
  
  public final boolean shouldClose()
  {
    return mFinalState;
  }
  
  public final boolean shouldGiveFeedback()
  {
    FeedbackItemState localFeedbackItemState = FEEDBACK_YES;
    return this == localFeedbackItemState;
  }
  
  public final boolean shouldInvite()
  {
    FeedbackItemState localFeedbackItemState = INVITE_YES;
    return this == localFeedbackItemState;
  }
  
  public final boolean shouldRate()
  {
    FeedbackItemState localFeedbackItemState = RATE_YES;
    return this == localFeedbackItemState;
  }
  
  public final boolean shouldShare()
  {
    FeedbackItemState localFeedbackItemState = SHARE_YES;
    return this == localFeedbackItemState;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.FeedbackItemView.FeedbackItem.FeedbackItemState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */