package com.truecaller.ui.components;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;

public final class c
{
  public boolean a;
  public int b;
  public final RectF c;
  private final Paint d;
  private final int e;
  private final int f;
  private final int g;
  private Paint h;
  private final Paint i;
  private final Rect j;
  private final RectF k;
  private final int l;
  
  public c(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, float paramFloat, int paramInt6, int paramInt7, int paramInt8)
  {
    Object localObject1 = new android/graphics/Rect;
    ((Rect)localObject1).<init>();
    j = ((Rect)localObject1);
    b = 0;
    localObject1 = new android/graphics/Paint;
    ((Paint)localObject1).<init>();
    d = ((Paint)localObject1);
    localObject1 = d;
    ((Paint)localObject1).setColor(paramInt4);
    Paint localPaint = d;
    boolean bool = true;
    localPaint.setAntiAlias(bool);
    localPaint = d;
    Paint.Style localStyle1 = Paint.Style.FILL;
    localPaint.setStyle(localStyle1);
    if (paramInt6 > 0)
    {
      localPaint = new android/graphics/Paint;
      localPaint.<init>();
      h = localPaint;
      h.setColor(paramInt7);
      h.setAntiAlias(bool);
      localPaint = h;
      Paint.Style localStyle2 = Paint.Style.STROKE;
      localPaint.setStyle(localStyle2);
      localPaint = h;
      float f1 = paramInt6;
      localPaint.setStrokeWidth(f1);
    }
    localPaint = new android/graphics/Paint;
    localPaint.<init>();
    i = localPaint;
    i.setColor(paramInt5);
    localPaint = i;
    Object localObject2 = Typeface.DEFAULT_BOLD;
    localPaint.setTypeface((Typeface)localObject2);
    i.setTextSize(paramFloat);
    i.setAntiAlias(bool);
    localPaint = i;
    localObject2 = Paint.Align.CENTER;
    localPaint.setTextAlign((Paint.Align)localObject2);
    e = paramInt1;
    f = paramInt2;
    g = paramInt3;
    RectF localRectF = new android/graphics/RectF;
    localRectF.<init>();
    k = localRectF;
    localRectF = new android/graphics/RectF;
    localRectF.<init>();
    c = localRectF;
    l = paramInt8;
  }
  
  public final void a(int paramInt)
  {
    d.setColor(paramInt);
  }
  
  public final void a(Canvas paramCanvas, RectF paramRectF)
  {
    boolean bool1 = a;
    if (!bool1)
    {
      int m = b;
      if (m <= 0) {
        return;
      }
    }
    boolean bool2 = a;
    String str;
    if (bool2)
    {
      str = "!";
    }
    else
    {
      int n = b;
      int i1 = 99;
      f1 = 1.39E-43F;
      if (n <= i1) {
        str = String.valueOf(n);
      } else {
        str = "99+";
      }
    }
    boolean bool3 = a;
    if (bool3) {}
    do
    {
      i2 = f;
      break;
      i2 = b;
      i3 = 10;
      f2 = 1.4E-44F;
    } while (i2 < i3);
    int i2 = g;
    float f2 = paramRectF.width();
    float f3 = paramRectF.height();
    int i4 = l;
    float f4 = 2.0F;
    RectF localRectF;
    float f5;
    float f6;
    switch (i4)
    {
    default: 
      return;
    case 1: 
      localRectF = k;
      f5 = left + f2;
      f1 = i2;
      f5 -= f1;
      i2 = 1092616192;
      f1 = 10.0F;
      f5 += f1;
      f2 = top - f1;
      f6 = right + f1;
      f7 = bottom - f3;
      int i5 = e;
      f3 = i5;
      f7 = f7 + f3 - f1;
      localRectF.set(f5, f2, f6, f7);
      break;
    case 0: 
      localRectF = k;
      f5 = left;
      f1 = i2;
      f2 = (f2 - f1) / f4;
      f5 += f2;
      f1 = top;
      int i6 = e;
      f6 = i6;
      f6 = (f3 - f6) / f4;
      f1 += f6;
      f6 = right - f2;
      f7 = bottom;
      i3 = e;
      f2 = i3;
      f3 = (f3 - f2) / f4;
      f7 -= f3;
      localRectF.set(f5, f1, f6, f7);
    }
    paramRectF = k;
    float f7 = paramRectF.width();
    float f1 = k.height();
    f7 = Math.min(f7, f1);
    Object localObject1 = k;
    Object localObject2 = d;
    paramCanvas.drawRoundRect((RectF)localObject1, f7, f7, (Paint)localObject2);
    localObject1 = h;
    if (localObject1 != null)
    {
      f1 = ((Paint)localObject1).getStrokeWidth() / f4;
      i3 = -1082130432;
      f2 = -1.0F;
      f1 *= f2;
      k.inset(f1, f1);
      localObject2 = k;
      f7 -= f1;
      localObject1 = h;
      paramCanvas.drawRoundRect((RectF)localObject2, f7, f7, (Paint)localObject1);
    }
    paramRectF = i;
    int i3 = str.length();
    Rect localRect = j;
    paramRectF.getTextBounds(str, 0, i3, localRect);
    f7 = k.left;
    f1 = k.right;
    f7 = (f7 + f1) / f4;
    f1 = k.top;
    f2 = k.bottom;
    f1 = (f1 + f2) / f4;
    f2 = j.height() / 2;
    f1 += f2;
    localObject2 = i;
    paramCanvas.drawText(str, f7, f1, (Paint)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */