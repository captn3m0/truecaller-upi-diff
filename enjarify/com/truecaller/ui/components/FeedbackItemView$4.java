package com.truecaller.ui.components;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

final class FeedbackItemView$4
  extends AnimatorListenerAdapter
{
  FeedbackItemView$4(FeedbackItemView paramFeedbackItemView) {}
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    FeedbackItemView.c(a);
    paramAnimator = FeedbackItemView.d(a);
    if (paramAnimator != null)
    {
      paramAnimator = FeedbackItemView.d(a);
      FeedbackItemView.FeedbackItem localFeedbackItem = FeedbackItemView.e(a);
      paramAnimator.c(localFeedbackItem);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.FeedbackItemView.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */