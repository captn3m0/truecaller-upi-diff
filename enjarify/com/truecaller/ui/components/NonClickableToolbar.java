package com.truecaller.ui.components;

import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class NonClickableToolbar
  extends Toolbar
{
  public NonClickableToolbar(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.NonClickableToolbar
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */