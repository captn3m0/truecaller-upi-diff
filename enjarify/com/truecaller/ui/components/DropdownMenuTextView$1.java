package com.truecaller.ui.components;

import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.view.MenuItem;

final class DropdownMenuTextView$1
  implements PopupMenu.OnMenuItemClickListener
{
  DropdownMenuTextView$1(DropdownMenuTextView paramDropdownMenuTextView) {}
  
  public final boolean onMenuItemClick(MenuItem paramMenuItem)
  {
    PopupMenu.OnMenuItemClickListener localOnMenuItemClickListener = DropdownMenuTextView.a(a);
    if (localOnMenuItemClickListener != null) {
      return localOnMenuItemClickListener.onMenuItemClick(paramMenuItem);
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.DropdownMenuTextView.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */