package com.truecaller.ui.components;

import android.graphics.drawable.Drawable;

public final class i
{
  final int a;
  final int b;
  final int c;
  final int d;
  final Drawable e;
  final int f;
  
  public i(int paramInt1, int paramInt2, int paramInt3)
  {
    this(paramInt1, paramInt2, paramInt3, 0, 0, 56);
  }
  
  private i(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    a = paramInt1;
    b = paramInt2;
    c = paramInt3;
    d = paramInt4;
    e = null;
    f = paramInt5;
  }
  
  public final boolean a()
  {
    int i = d;
    return i != 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */