package com.truecaller.ui.components.a;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;
import c.g.b.k;

public final class c
  extends RecyclerView.ItemDecoration
{
  public static final c.a a;
  private final int b;
  private final int c;
  private final boolean d;
  private final boolean e;
  
  static
  {
    c.a locala = new com/truecaller/ui/components/a/c$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  private c(int paramInt)
  {
    b = paramInt;
    c = 1;
    d = false;
    e = false;
  }
  
  public static final c a(int paramInt)
  {
    c localc = new com/truecaller/ui/components/a/c;
    localc.<init>(paramInt, (byte)0);
    return localc;
  }
  
  public final void getItemOffsets(Rect paramRect, View paramView, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    k.b(paramRect, "outRect");
    k.b(paramView, "view");
    k.b(paramRecyclerView, "parent");
    String str = "state";
    k.b(paramState, str);
    int i = c;
    int k = 1;
    int m;
    if (i == k)
    {
      boolean bool2 = e;
      if (bool2)
      {
        m = b;
        left = m;
        right = m;
      }
      m = b;
      bottom = m;
      return;
    }
    if (i == 0)
    {
      boolean bool1 = d;
      int j;
      if (bool1)
      {
        j = b;
        left = j;
        m = paramRecyclerView.getChildAdapterPosition(paramView);
        if (m == 0)
        {
          m = b;
          right = m;
        }
      }
      else
      {
        j = b;
        right = j;
        m = paramRecyclerView.getChildAdapterPosition(paramView);
        if (m == 0)
        {
          m = b;
          left = m;
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */