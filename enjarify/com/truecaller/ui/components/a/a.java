package com.truecaller.ui.components.a;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.view.View;

public abstract class a
  extends RecyclerView.ItemDecoration
{
  private final a.a a;
  
  public a(a.a parama)
  {
    a = parama;
  }
  
  protected final boolean a(RecyclerView paramRecyclerView, View paramView)
  {
    a.a locala = a;
    RecyclerView.Adapter localAdapter = paramRecyclerView.getAdapter();
    int i = paramRecyclerView.getChildAdapterPosition(paramView);
    return locala.isFirstInGroup(localAdapter, i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */