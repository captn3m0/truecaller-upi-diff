package com.truecaller.ui.components.a;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;

public abstract class b
  extends a
{
  private final int a;
  private final boolean b;
  private final int c;
  private final int d;
  private final Drawable e;
  private final Rect f;
  private final Paint g;
  
  public b(Context paramContext, a.a parama, boolean paramBoolean)
  {
    super(parama);
    parama = new android/graphics/Rect;
    parama.<init>();
    f = parama;
    parama = paramContext.getResources();
    int i = parama.getDimensionPixelSize(2131165555);
    a = i;
    i = parama.getDimensionPixelOffset(2131165497);
    int j = parama.getDimensionPixelOffset(2131165495) + i;
    c = j;
    d = i;
    j = 2130969178;
    i = 2131234571;
    Object localObject = com.truecaller.utils.ui.b.a(paramContext, i, j);
    e = ((Drawable)localObject);
    localObject = new android/graphics/Paint;
    int k = 1;
    ((Paint)localObject).<init>(k);
    g = ((Paint)localObject);
    localObject = g;
    j = com.truecaller.utils.ui.b.a(paramContext, j);
    ((Paint)localObject).setColor(j);
    paramContext = paramContext.getResources();
    j = 2131165556;
    int m = paramContext.getDimensionPixelSize(j);
    parama = g;
    float f1 = m;
    parama.setTextSize(f1);
    paramContext = g;
    if (paramBoolean) {
      parama = Paint.Align.RIGHT;
    } else {
      parama = Paint.Align.LEFT;
    }
    paramContext.setTextAlign(parama);
    b = paramBoolean;
  }
  
  public abstract char a(int paramInt);
  
  public void getItemOffsets(Rect paramRect, View paramView, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    paramRect.set(0, 0, 0, 0);
    boolean bool = a(paramRecyclerView, paramView);
    if (bool)
    {
      int i = a;
      top = i;
    }
  }
  
  public void onDraw(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    int i = paramRecyclerView.getChildCount();
    int j = 0;
    while (j < i)
    {
      Object localObject1 = paramRecyclerView.getChildAt(j);
      boolean bool1 = a(paramRecyclerView, (View)localObject1);
      if (bool1)
      {
        int k = paramRecyclerView.getChildAdapterPosition((View)localObject1);
        k = a(k);
        int m = 9733;
        float f1 = 1.3639E-41F;
        float f3;
        Object localObject2;
        int i2;
        int i4;
        int i1;
        Object localObject3;
        Object localObject4;
        int i5;
        if (k == m)
        {
          float f2 = ((View)localObject1).getY();
          int n = a;
          f3 = n;
          f2 -= f3;
          localObject2 = e;
          i2 = ((Drawable)localObject2).getIntrinsicHeight();
          f1 = (n - i2) / 2;
          f2 += f1;
          k = (int)f2;
          boolean bool2 = b;
          if (bool2)
          {
            i4 = ((View)localObject1).getRight();
            i1 = d;
            i4 -= i1;
            localObject3 = e;
            i1 = ((Drawable)localObject3).getIntrinsicWidth();
            i4 -= i1;
          }
          else
          {
            i4 = d;
          }
          localObject3 = e;
          i2 = ((Drawable)localObject3).getIntrinsicWidth() + i4;
          localObject4 = e;
          i5 = ((Drawable)localObject4).getIntrinsicHeight() + k;
          ((Drawable)localObject3).setBounds(i4, k, i2, i5);
          localObject1 = e;
          ((Drawable)localObject1).draw(paramCanvas);
        }
        else
        {
          String str = String.valueOf(k);
          localObject3 = g;
          i2 = str.length();
          localObject4 = f;
          ((Paint)localObject3).getTextBounds(str, 0, i2, (Rect)localObject4);
          f1 = ((View)localObject1).getY();
          i2 = a;
          localObject4 = f;
          i5 = ((Rect)localObject4).height();
          f3 = (i2 - i5) / 2;
          f1 -= f3;
          i1 = (int)f1;
          boolean bool3 = b;
          if (bool3)
          {
            i4 = ((View)localObject1).getRight();
            int i3 = c;
            i4 -= i3;
          }
          else
          {
            i4 = c;
          }
          float f4 = i4;
          f1 = i1;
          localObject2 = g;
          paramCanvas.drawText(str, f4, f1, (Paint)localObject2);
        }
      }
      j += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */