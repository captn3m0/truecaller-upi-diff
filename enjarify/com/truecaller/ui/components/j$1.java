package com.truecaller.ui.components;

import android.support.v7.widget.RecyclerView.AdapterDataObserver;

final class j$1
  extends RecyclerView.AdapterDataObserver
{
  j$1(j paramj) {}
  
  public final void onChanged()
  {
    a.notifyDataSetChanged();
  }
  
  public final void onItemRangeChanged(int paramInt1, int paramInt2)
  {
    a.notifyDataSetChanged();
  }
  
  public final void onItemRangeChanged(int paramInt1, int paramInt2, Object paramObject)
  {
    a.notifyDataSetChanged();
  }
  
  public final void onItemRangeInserted(int paramInt1, int paramInt2)
  {
    a.notifyDataSetChanged();
  }
  
  public final void onItemRangeMoved(int paramInt1, int paramInt2, int paramInt3)
  {
    a.notifyDataSetChanged();
  }
  
  public final void onItemRangeRemoved(int paramInt1, int paramInt2)
  {
    a.notifyDataSetChanged();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.j.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */