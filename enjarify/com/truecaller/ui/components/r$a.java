package com.truecaller.ui.components;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.ViewConfiguration;
import android.view.WindowManager;

public final class r$a
{
  final int a;
  final boolean b;
  final int c;
  final int d;
  private final boolean e;
  private final boolean f;
  private final int g;
  private final boolean h;
  private final float i;
  
  private r$a(Activity paramActivity, boolean paramBoolean1, boolean paramBoolean2)
  {
    Object localObject1 = paramActivity.getResources();
    Object localObject2 = ((Resources)localObject1).getConfiguration();
    int j = orientation;
    boolean bool2 = false;
    int k = 1;
    if (j == k)
    {
      j = 1;
      f1 = Float.MIN_VALUE;
    }
    else
    {
      j = 0;
      f1 = 0.0F;
      localObject2 = null;
    }
    h = j;
    localObject2 = new android/util/DisplayMetrics;
    ((DisplayMetrics)localObject2).<init>();
    Display localDisplay = paramActivity.getWindowManager().getDefaultDisplay();
    localDisplay.getRealMetrics((DisplayMetrics)localObject2);
    float f2 = widthPixels;
    float f3 = density;
    f2 /= f3;
    int m = heightPixels;
    f3 = m;
    float f1 = density;
    f3 /= f1;
    f1 = Math.min(f2, f3);
    i = f1;
    int n = a((Resources)localObject1, "status_bar_height");
    a = n;
    localObject1 = new android/util/TypedValue;
    ((TypedValue)localObject1).<init>();
    localObject2 = paramActivity.getTheme();
    int i1 = 16843499;
    f2 = 2.3695652E-38F;
    ((Resources.Theme)localObject2).resolveAttribute(i1, (TypedValue)localObject1, k);
    n = data;
    localObject2 = paramActivity.getResources().getDisplayMetrics();
    n = TypedValue.complexToDimensionPixelSize(n, (DisplayMetrics)localObject2);
    g = n;
    localObject1 = paramActivity.getResources();
    boolean bool1 = a(paramActivity);
    if (bool1)
    {
      bool1 = h;
      if (bool1) {
        localObject2 = "navigation_bar_height";
      } else {
        localObject2 = "navigation_bar_height_landscape";
      }
      n = a((Resources)localObject1, (String)localObject2);
    }
    else
    {
      n = 0;
      localObject1 = null;
    }
    c = n;
    localObject1 = paramActivity.getResources();
    boolean bool3 = a(paramActivity);
    if (bool3)
    {
      paramActivity = "navigation_bar_width";
      i2 = a((Resources)localObject1, paramActivity);
    }
    else
    {
      i2 = 0;
      paramActivity = null;
    }
    d = i2;
    int i2 = c;
    if (i2 > 0) {
      bool2 = true;
    }
    b = bool2;
    e = paramBoolean1;
    f = paramBoolean2;
  }
  
  private static int a(Resources paramResources, String paramString)
  {
    String str1 = "dimen";
    String str2 = "android";
    int j = paramResources.getIdentifier(paramString, str1, str2);
    int k;
    if (j > 0)
    {
      k = paramResources.getDimensionPixelSize(j);
    }
    else
    {
      k = 0;
      paramResources = null;
    }
    return k;
  }
  
  private static boolean a(Context paramContext)
  {
    Object localObject = paramContext.getResources();
    String str1 = "config_showNavigationBar";
    String str2 = "bool";
    int j = ((Resources)localObject).getIdentifier(str1, str2, "android");
    boolean bool1 = true;
    if (j != 0)
    {
      bool2 = ((Resources)localObject).getBoolean(j);
      localObject = "1";
      str1 = r.a();
      boolean bool3 = ((String)localObject).equals(str1);
      if (bool3)
      {
        bool2 = false;
        paramContext = null;
      }
      else
      {
        localObject = "0";
        str1 = r.a();
        bool3 = ((String)localObject).equals(str1);
        if (bool3) {
          bool2 = true;
        }
      }
      return bool2;
    }
    paramContext = ViewConfiguration.get(paramContext);
    boolean bool2 = paramContext.hasPermanentMenuKey();
    if (!bool2) {
      return bool1;
    }
    return false;
  }
  
  public final boolean a()
  {
    float f1 = i;
    float f2 = 600.0F;
    boolean bool = f1 < f2;
    if (bool)
    {
      bool = h;
      if (!bool) {
        return false;
      }
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.r.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */