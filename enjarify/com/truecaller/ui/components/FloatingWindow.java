package com.truecaller.ui.components;

import android.animation.Animator.AnimatorListener;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewPropertyAnimator;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import com.truecaller.common.h.k;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.util.at;

public abstract class FloatingWindow
  implements View.OnClickListener
{
  private static final FloatingWindow.a a = -..Lambda.FloatingWindow.E5aG50wEr2bO1NY8Bx-AH8rGvco.INSTANCE;
  private final FloatingWindow.a b;
  private final Class c;
  public final Context d;
  public WindowManager e;
  public FrameLayout f;
  public Handler g;
  public int h;
  public int i;
  public boolean j;
  private WindowManager.LayoutParams k;
  private int l;
  private boolean m;
  private boolean n;
  private int o;
  private View p;
  
  FloatingWindow(Context paramContext, Class paramClass)
  {
    Object localObject = new android/view/ContextThemeWrapper;
    ThemeManager.Theme localTheme = ThemeManager.a();
    int i1 = resId;
    ((ContextThemeWrapper)localObject).<init>(paramContext, i1);
    d = ((Context)localObject);
    paramContext = a;
    b = paramContext;
    c = paramClass;
    paramContext = new android/os/Handler;
    paramClass = new com/truecaller/ui/components/-$$Lambda$FloatingWindow$146P-xG6kOlfEITt6gu5StQVsaw;
    paramClass.<init>(this);
    paramContext.<init>(paramClass);
    g = paramContext;
    paramContext = d.getResources();
    int i2 = 17694720;
    int i3 = paramContext.getInteger(i2);
    l = i3;
    paramContext = ViewConfiguration.get(d);
    i3 = paramContext.getScaledTouchSlop();
    o = i3;
    boolean bool = k.h();
    int i4;
    int i5;
    if (bool)
    {
      i4 = 2038;
      i5 = 2038;
    }
    else
    {
      i4 = 2010;
      i5 = 2010;
    }
    paramContext = LayoutInflater.from(d);
    paramClass = (WindowManager)d.getSystemService("window");
    e = paramClass;
    paramClass = d.getResources().getDisplayMetrics();
    int i6 = widthPixels;
    h = i6;
    i2 = heightPixels;
    i6 = at.a(d.getResources());
    i2 -= i6;
    i = i2;
    paramClass = new android/view/WindowManager$LayoutParams;
    localObject = paramClass;
    paramClass.<init>(-1, -2, i5, 8, -3);
    k = paramClass;
    paramClass = k;
    gravity = 49;
    dimAmount = 0.6F;
    i6 = a();
    y = i6;
    paramClass = c;
    paramContext = paramContext.inflate(2131559220, null);
    paramContext = (View)paramClass.cast(paramContext);
    p = paramContext;
    paramContext = new android/widget/FrameLayout;
    paramClass = d;
    paramContext.<init>(paramClass);
    f = paramContext;
    f.setVisibility(8);
    paramContext = f;
    paramClass = p;
    paramContext.addView(paramClass);
    paramContext = e;
    paramClass = f;
    localObject = k;
    paramContext.addView(paramClass, (ViewGroup.LayoutParams)localObject);
    paramContext = f;
    paramClass = new com/truecaller/ui/components/FloatingWindow$b;
    paramClass.<init>(this);
    paramContext.setOnTouchListener(paramClass);
    paramContext = p;
    a(paramContext);
  }
  
  private void b(int paramInt)
  {
    int i1;
    float f1;
    if (paramInt == 0)
    {
      i1 = 1065353216;
      f1 = 1.0F;
      localObject = new android/view/animation/AccelerateDecelerateInterpolator;
      ((AccelerateDecelerateInterpolator)localObject).<init>();
    }
    else
    {
      localObject = new android/view/animation/AccelerateInterpolator;
      ((AccelerateInterpolator)localObject).<init>();
      i1 = h;
      int i2 = -i1;
      if ((paramInt == i2) || (paramInt == i1))
      {
        i1 = 0;
        f1 = 0.0F;
        localViewPropertyAnimator1 = null;
        j = false;
      }
      i1 = 0;
      f1 = 0.0F;
      localViewPropertyAnimator1 = null;
    }
    ViewPropertyAnimator localViewPropertyAnimator2 = p.animate();
    float f2 = paramInt;
    ViewPropertyAnimator localViewPropertyAnimator1 = localViewPropertyAnimator2.translationX(f2).alpha(f1);
    long l1 = l;
    localViewPropertyAnimator1 = localViewPropertyAnimator1.setDuration(l1).setInterpolator((TimeInterpolator)localObject);
    Object localObject = new com/truecaller/ui/components/FloatingWindow$2;
    ((FloatingWindow.2)localObject).<init>(this, paramInt);
    localViewPropertyAnimator1.setListener((Animator.AnimatorListener)localObject);
  }
  
  protected abstract int a();
  
  protected abstract void a(int paramInt);
  
  final void a(long paramLong)
  {
    Handler localHandler = g;
    if (localHandler != null)
    {
      int i1 = 2;
      localHandler.removeMessages(i1);
      localHandler = g;
      localHandler.sendEmptyMessageDelayed(i1, paramLong);
    }
  }
  
  protected abstract void a(View paramView);
  
  protected void a(FloatingWindow.DismissCause paramDismissCause)
  {
    boolean bool = j;
    WindowManager.LayoutParams localLayoutParams;
    if (!bool)
    {
      localLayoutParams = k;
      int i2 = -2;
      height = i2;
    }
    try
    {
      WindowManager localWindowManager = e;
      FrameLayout localFrameLayout = f;
      localWindowManager.updateViewLayout(localFrameLayout, localLayoutParams);
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      int i1;
      for (;;) {}
    }
    i1 = k.y;
    a(i1);
    f.setVisibility(8);
    b.onDismissed(paramDismissCause);
    return;
    c();
  }
  
  protected abstract void b();
  
  public final void b(FloatingWindow.DismissCause paramDismissCause)
  {
    j = false;
    Object localObject1 = g;
    if (localObject1 != null)
    {
      ((Handler)localObject1).removeMessages(2);
      localObject1 = p.animate().alpha(0.0F);
      int i1 = l;
      long l1 = i1;
      localObject1 = ((ViewPropertyAnimator)localObject1).setDuration(l1);
      Object localObject2 = new android/view/animation/LinearInterpolator;
      ((LinearInterpolator)localObject2).<init>();
      localObject1 = ((ViewPropertyAnimator)localObject1).setInterpolator((TimeInterpolator)localObject2);
      localObject2 = new com/truecaller/ui/components/FloatingWindow$1;
      ((FloatingWindow.1)localObject2).<init>(this, paramDismissCause);
      ((ViewPropertyAnimator)localObject1).setListener((Animator.AnimatorListener)localObject2);
    }
  }
  
  public final void c()
  {
    j = true;
    f.setVisibility(0);
    p.setAlpha(0.0F);
    View localView = p;
    float f1 = h;
    localView.setTranslationX(f1);
    b(0);
    b();
  }
  
  public void onClick(View paramView) {}
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.FloatingWindow
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */