package com.truecaller.ui.components;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.res.TypedArray;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import com.truecaller.R.styleable;
import com.truecaller.util.at;
import java.util.ArrayList;
import java.util.List;

public class ComboBase
  extends LinearLayout
  implements View.OnClickListener
{
  public List a;
  private String b;
  private n c;
  private List d;
  private int e;
  private AlertDialog f;
  
  public ComboBase(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    int i = 0;
    Context localContext = null;
    e = 0;
    Object localObject = R.styleable.ComboBase;
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject);
    int j = 2131558537;
    localObject = null;
    if (paramContext != null)
    {
      for (;;)
      {
        k = paramContext.getIndexCount();
        if (i >= k) {
          break;
        }
        k = paramContext.getIndex(i);
        switch (k)
        {
        default: 
          break;
        case 1: 
          j = paramContext.getResourceId(k, j);
          break;
        case 0: 
          localObject = paramContext.getString(k);
        }
        i += 1;
      }
      paramContext.recycle();
    }
    paramContext = new android/widget/LinearLayout$LayoutParams;
    i = -1;
    int k = -2;
    paramContext.<init>(i, k);
    localContext = getContext();
    paramAttributeSet = at.b(localContext, j);
    addView(paramAttributeSet, paramContext);
    setOnClickListener(this);
    boolean bool2 = true;
    setClickable(bool2);
    boolean bool1 = isEnabled();
    setEnabled(bool1);
    if (localObject != null)
    {
      paramContext = n.a(bool2, (String)localObject);
      setTitle(paramContext);
    }
  }
  
  public final void a()
  {
    List localList = a;
    if (localList != null)
    {
      int i = localList.size() + -1;
      while (i >= 0)
      {
        ComboBase.a locala = (ComboBase.a)a.get(i);
        locala.onSelectionChanged(this);
        i += -1;
      }
    }
  }
  
  public final void a(ComboBase.a parama)
  {
    Object localObject = a;
    if (localObject == null)
    {
      localObject = new java/util/ArrayList;
      int i = 1;
      ((ArrayList)localObject).<init>(i);
      a = ((List)localObject);
    }
    a.add(parama);
  }
  
  public List getItems()
  {
    return d;
  }
  
  public n getSelection()
  {
    return c;
  }
  
  public String getTitle()
  {
    return b;
  }
  
  public void onClick(View paramView)
  {
    paramView = d;
    if (paramView != null)
    {
      paramView = new android/support/v7/app/AlertDialog$Builder;
      Object localObject = getContext();
      paramView.<init>((Context)localObject);
      localObject = b;
      paramView = paramView.setTitle((CharSequence)localObject);
      localObject = c;
      if (localObject != null)
      {
        int i = e;
        if (i != 0)
        {
          localf = new com/truecaller/ui/components/f;
          List localList = d;
          -..Lambda.ComboBase.uxzxDENvlInWJtsc_8X2kFd6qZQ localuxzxDENvlInWJtsc_8X2kFd6qZQ = new com/truecaller/ui/components/-$$Lambda$ComboBase$uxzxDENvlInWJtsc_8X2kFd6qZQ;
          localuxzxDENvlInWJtsc_8X2kFd6qZQ.<init>(this);
          localf.<init>(localList, i, (n)localObject, localuxzxDENvlInWJtsc_8X2kFd6qZQ);
          break label104;
        }
      }
      f localf = new com/truecaller/ui/components/f;
      localObject = d;
      localf.<init>((List)localObject);
      label104:
      localObject = new com/truecaller/ui/components/-$$Lambda$ComboBase$fQm3CtNPtzvjsFV9T54OLn1TWf8;
      ((-..Lambda.ComboBase.fQm3CtNPtzvjsFV9T54OLn1TWf8)localObject).<init>(this);
      paramView = paramView.setAdapter(localf, (DialogInterface.OnClickListener)localObject).show();
      f = paramView;
    }
  }
  
  public void setData(List paramList)
  {
    d = paramList;
    paramList = d;
    if (paramList != null)
    {
      int i = paramList.size();
      if (i > 0)
      {
        paramList = (n)d.get(0);
        setSelection(paramList);
      }
    }
  }
  
  public void setListItemLayoutRes(int paramInt)
  {
    e = paramInt;
  }
  
  public void setSelection(n paramn)
  {
    c = paramn;
    Object localObject1;
    Object localObject2;
    if (paramn == null)
    {
      localObject1 = "";
    }
    else
    {
      localObject1 = c;
      localObject2 = getContext();
      localObject1 = ((n)localObject1).a((Context)localObject2);
    }
    if (paramn == null)
    {
      localObject2 = "";
    }
    else
    {
      localObject2 = c;
      Context localContext = getContext();
      localObject2 = ((n)localObject2).b(localContext);
    }
    int i;
    if (paramn == null)
    {
      i = 0;
      paramn = null;
    }
    else
    {
      i = f;
    }
    at.e(this, i);
    at.a(this, 2131363657, (CharSequence)localObject1);
    at.a(this, 2131363653, (CharSequence)localObject2);
  }
  
  public void setTitle(String paramString)
  {
    paramString = n.a(true, paramString);
    b = paramString;
    paramString = b;
    at.a(this, 2131362535, paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.components.ComboBase
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */