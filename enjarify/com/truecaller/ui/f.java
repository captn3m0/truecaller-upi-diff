package com.truecaller.ui;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.support.v7.app.AlertDialog.Builder;
import android.widget.Toast;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.content.TruecallerContract.Filters.WildCardType;
import com.truecaller.data.entity.Contact;
import com.truecaller.filters.FilterManager;
import com.truecaller.ui.details.a;
import com.truecaller.ui.details.a.a;
import java.util.Collections;
import java.util.List;

public abstract class f
{
  public final FilterManager b;
  
  public f(FilterManager paramFilterManager)
  {
    b = paramFilterManager;
  }
  
  private void a(List paramList, String paramString1, Contact paramContact, String paramString2, String paramString3, TruecallerContract.Filters.WildCardType paramWildCardType, f.a parama, boolean paramBoolean)
  {
    String str = paramString3;
    Object localObject1 = c();
    if (localObject1 == null) {
      return;
    }
    int i = -1;
    int j = paramString3.hashCode();
    int m = -293212780;
    boolean bool4 = false;
    int n = 1;
    boolean bool1;
    if (j != m)
    {
      m = 93832333;
      if (j != m)
      {
        m = 2129658012;
        if (j == m)
        {
          localObject2 = "notspam";
          bool1 = paramString3.equals(localObject2);
          if (bool1) {
            i = 2;
          }
        }
      }
      else
      {
        localObject2 = "block";
        bool1 = paramString3.equals(localObject2);
        if (bool1)
        {
          i = 0;
          localObject1 = null;
        }
      }
    }
    else
    {
      localObject2 = "unblock";
      bool1 = paramString3.equals(localObject2);
      if (bool1) {
        i = 1;
      }
    }
    int k;
    int i1;
    switch (i)
    {
    default: 
      return;
    case 2: 
      i = 2131886198;
      k = 2131886105;
      i1 = 2131886105;
      break;
    case 1: 
      i = 2131886194;
      k = 2131886115;
      i1 = 2131886115;
      break;
    case 0: 
      i = 2131886135;
      k = 2131886094;
      i1 = 2131886094;
    }
    Object localObject3;
    if (paramContact == null)
    {
      localObject3 = null;
    }
    else
    {
      localObject2 = paramContact.t();
      localObject3 = localObject2;
    }
    Object localObject2 = "block";
    boolean bool2 = ((String)localObject2).equalsIgnoreCase(str);
    if (bool2)
    {
      localObject1 = new com/truecaller/ui/details/a;
      localObject2 = c();
      m = paramList.size();
      boolean bool3;
      if (m == n)
      {
        bool3 = paramBoolean;
        bool4 = true;
      }
      else
      {
        bool3 = paramBoolean;
      }
      ((a)localObject1).<init>((Context)localObject2, (String)localObject3, bool3, bool4);
      localObject2 = new com/truecaller/ui/-$$Lambda$f$O5HcVJjbDPr21JMT56TWbxmdIlc;
      localObject4 = localObject2;
      localObject5 = this;
      ((-..Lambda.f.O5HcVJjbDPr21JMT56TWbxmdIlc)localObject2).<init>(this, paramContact, (String)localObject3, paramList, paramString1, paramString2, true, paramWildCardType, parama);
      a = ((a.a)localObject2);
      ((a)localObject1).show();
      return;
    }
    localObject2 = new android/support/v7/app/AlertDialog$Builder;
    Object localObject6 = c();
    ((AlertDialog.Builder)localObject2).<init>((Context)localObject6);
    Object localObject4 = ((AlertDialog.Builder)localObject2).setMessage(i);
    Object localObject5 = new com/truecaller/ui/-$$Lambda$f$AMV-Smj4LTD5rhTTzSWZkJj3CNs;
    localObject1 = localObject5;
    localObject2 = this;
    localObject6 = paramList;
    str = paramString3;
    ((-..Lambda.f.AMV-Smj4LTD5rhTTzSWZkJj3CNs)localObject5).<init>(this, paramList, paramString1, (String)localObject3, paramString2, paramString3, true, parama);
    ((AlertDialog.Builder)localObject4).setPositiveButton(i1, (DialogInterface.OnClickListener)localObject5).setNegativeButton(2131887197, null).show();
  }
  
  public void a() {}
  
  public void a(int paramInt)
  {
    Context localContext = c();
    if (localContext != null)
    {
      Toast localToast = Toast.makeText(localContext, paramInt, 0);
      localToast.show();
    }
  }
  
  protected void a(String paramString) {}
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean)
  {
    List localList = Collections.singletonList(paramString1);
    a(localList, paramString2, paramString3, paramString4, null, paramBoolean);
  }
  
  public final void a(List paramList, String paramString1, Contact paramContact, String paramString2, f.a parama, boolean paramBoolean)
  {
    TruecallerContract.Filters.WildCardType localWildCardType = TruecallerContract.Filters.WildCardType.NONE;
    a(paramList, paramString1, paramContact, paramString2, "block", localWildCardType, parama, paramBoolean);
  }
  
  public final void a(List paramList, String paramString1, String paramString2, String paramString3, f.a parama, boolean paramBoolean)
  {
    TruecallerContract.Filters.WildCardType localWildCardType = TruecallerContract.Filters.WildCardType.NONE;
    a(paramList, paramString1, null, paramString2, paramString3, localWildCardType, parama, paramBoolean);
  }
  
  public final void a(List paramList, String paramString1, String paramString2, String paramString3, boolean paramBoolean, TruecallerContract.Filters.WildCardType paramWildCardType, TruecallerContract.Filters.EntityType paramEntityType)
  {
    new f.1(this, paramList, paramString1, paramString2, paramString3, paramBoolean, paramWildCardType, paramEntityType);
  }
  
  public void b() {}
  
  protected abstract Context c();
}

/* Location:
 * Qualified Name:     com.truecaller.ui.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */