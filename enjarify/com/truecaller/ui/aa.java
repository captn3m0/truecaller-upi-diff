package com.truecaller.ui;

import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.OnItemTouchListener;
import android.support.v7.widget.RecyclerView.State;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import com.truecaller.common.e.f;
import com.truecaller.utils.ui.b;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class aa
  extends RecyclerView.ItemDecoration
  implements RecyclerView.OnItemTouchListener
{
  private final float a;
  private final Drawable b;
  private final Map c;
  private final Drawable d;
  private final Paint e;
  private final Drawable f;
  private final aa.a g;
  private Drawable h;
  private boolean i;
  private float j;
  private float k;
  private View l;
  private int m;
  private aa.b n;
  private boolean o;
  
  public aa(Context paramContext, Map paramMap, aa.a parama)
  {
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    c = ((Map)localObject1);
    m = -1;
    localObject1 = aa.b.a;
    n = ((aa.b)localObject1);
    g = parama;
    parama = ViewConfiguration.get(paramContext);
    float f1 = parama.getScaledTouchSlop() * 6.0F;
    a = f1;
    int i1 = 2130969588;
    f1 = 1.7547862E38F;
    localObject1 = b.a(paramContext, 2131234012, i1);
    b = ((Drawable)localObject1);
    localObject1 = b;
    int i2 = -((Drawable)localObject1).getIntrinsicWidth() / 2;
    int i3 = -b.getIntrinsicHeight() / 2;
    int i4 = b.getIntrinsicWidth() / 2;
    int i5 = b.getIntrinsicHeight() / 2;
    ((Drawable)localObject1).setBounds(i2, i3, i4, i5);
    int i6 = 2131234013;
    localObject1 = b.a(paramContext, i6, i1);
    d = ((Drawable)localObject1);
    localObject1 = d;
    i2 = -((Drawable)localObject1).getIntrinsicWidth() / 2;
    Object localObject2 = d;
    i3 = -((Drawable)localObject2).getIntrinsicHeight() / 2;
    Drawable localDrawable1 = d;
    i4 = localDrawable1.getIntrinsicWidth() / 2;
    Drawable localDrawable2 = d;
    i5 = localDrawable2.getIntrinsicHeight() / 2;
    ((Drawable)localObject1).setBounds(i2, i3, i4, i5);
    if (paramMap != null)
    {
      paramMap = paramMap.entrySet().iterator();
      for (;;)
      {
        boolean bool = paramMap.hasNext();
        if (!bool) {
          break;
        }
        localObject1 = (Map.Entry)paramMap.next();
        i2 = ((Integer)((Map.Entry)localObject1).getValue()).intValue();
        Drawable localDrawable3 = b.a(paramContext, i2, i1);
        i3 = -localDrawable3.getIntrinsicWidth() / 2;
        i4 = -localDrawable3.getIntrinsicHeight() / 2;
        i5 = localDrawable3.getIntrinsicWidth() / 2;
        int i7 = localDrawable3.getIntrinsicHeight() / 2;
        localDrawable3.setBounds(i3, i4, i5, i7);
        localObject2 = c;
        localObject1 = ((Map.Entry)localObject1).getKey();
        ((Map)localObject2).put(localObject1, localDrawable3);
      }
    }
    paramMap = new android/graphics/Paint;
    paramMap.<init>();
    e = paramMap;
    paramMap = e;
    parama = Paint.Style.FILL;
    paramMap.setStyle(parama);
    paramMap = e;
    i1 = b.a(paramContext, 2130969528);
    paramMap.setColor(i1);
    paramContext = b.c(paramContext, 2130969548);
    f = paramContext;
  }
  
  private View a(RecyclerView paramRecyclerView)
  {
    int i1 = m;
    Object localObject = paramRecyclerView.findViewHolderForAdapterPosition(i1);
    if (localObject == null) {
      return null;
    }
    localObject = itemView;
    View localView = l;
    if (localView == null)
    {
      l = ((View)localObject);
    }
    else if (localView != localObject)
    {
      localView = ((ViewGroup)localView).getChildAt(0);
      localView.setTranslationX(0.0F);
      l = ((View)localObject);
      paramRecyclerView.invalidateItemDecorations();
    }
    return (View)localObject;
  }
  
  private void a(int paramInt, float paramFloat, View paramView)
  {
    aa.a locala = g;
    boolean bool1 = paramFloat < 0.0F;
    if (bool1)
    {
      bool1 = o;
      if (!bool1) {}
    }
    else
    {
      bool2 = paramFloat < 0.0F;
      if (!bool2) {
        break label57;
      }
      bool2 = o;
      if (!bool2) {
        break label57;
      }
    }
    boolean bool2 = true;
    paramFloat = Float.MIN_VALUE;
    break label62;
    label57:
    bool2 = false;
    paramFloat = 0.0F;
    label62:
    locala.a(paramInt, bool2, paramView);
  }
  
  private void a(RecyclerView paramRecyclerView, boolean paramBoolean)
  {
    int i1 = m;
    int i2 = -1;
    if (i1 != i2)
    {
      Object localObject1 = n;
      Object localObject2 = aa.b.c;
      if (localObject1 == localObject2)
      {
        localObject1 = a(paramRecyclerView);
        if (paramBoolean)
        {
          localObject3 = aa.b.d;
          n = ((aa.b)localObject3);
          localObject3 = new float[2];
          float f1 = ((View)localObject1).getTranslationX();
          localObject3[0] = f1;
          localObject3[1] = 0.0F;
          localObject3 = ValueAnimator.ofFloat((float[])localObject3);
          ((ValueAnimator)localObject3).setDuration(200L);
          localObject2 = new com/truecaller/ui/-$$Lambda$aa$rjR-Ik_1dtDMOxhBAf4oLgylmQ8;
          ((-..Lambda.aa.rjR-Ik_1dtDMOxhBAf4oLgylmQ8)localObject2).<init>((View)localObject1, paramRecyclerView);
          ((ValueAnimator)localObject3).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject2);
          localObject1 = new com/truecaller/ui/aa$2;
          ((aa.2)localObject1).<init>(this, paramRecyclerView);
          ((ValueAnimator)localObject3).addListener((Animator.AnimatorListener)localObject1);
          ((ValueAnimator)localObject3).start();
          return;
        }
        Object localObject3 = new com/truecaller/ui/-$$Lambda$aa$F1CnJflIRtgLKcuzGI2ouh76HlM;
        ((-..Lambda.aa.F1CnJflIRtgLKcuzGI2ouh76HlM)localObject3).<init>(this, (View)localObject1, paramRecyclerView);
        paramRecyclerView.postDelayed((Runnable)localObject3, 500L);
        return;
      }
    }
    b(paramRecyclerView);
  }
  
  private void a(View paramView)
  {
    if (paramView != null)
    {
      boolean bool = i;
      if (bool)
      {
        Drawable localDrawable = h;
        paramView.setBackground(localDrawable);
        paramView = null;
        i = false;
      }
    }
  }
  
  private void b(RecyclerView paramRecyclerView)
  {
    paramRecyclerView = a(paramRecyclerView);
    a(paramRecyclerView);
    paramRecyclerView = aa.b.a;
    n = paramRecyclerView;
    m = -1;
  }
  
  public final void onDraw(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    super.onDraw(paramCanvas, paramRecyclerView, paramState);
    paramState = n;
    Object localObject1 = aa.b.c;
    if (paramState != localObject1)
    {
      paramState = n;
      localObject1 = aa.b.d;
      if (paramState != localObject1) {}
    }
    else
    {
      paramState = a(paramRecyclerView);
      paramRecyclerView = a(paramRecyclerView);
      if ((paramState != null) && (paramRecyclerView != null))
      {
        localObject1 = c;
        Object localObject2 = paramState.getTag();
        localObject1 = (Drawable)((Map)localObject1).get(localObject2);
        boolean bool1 = false;
        localObject2 = null;
        float f1 = paramRecyclerView.getTranslationX();
        int i1 = paramState.getLeft();
        float f2 = i1;
        float f3 = 0.0F;
        boolean bool2 = f1 < f2;
        float f5;
        if (!bool2)
        {
          f2 = paramState.getLeft();
          f3 = paramState.getTop();
          float f4 = paramRecyclerView.getTranslationX();
          f5 = paramState.getBottom();
          Paint localPaint1 = e;
          paramCanvas.drawRect(f2, f3, f4, f5, localPaint1);
          bool1 = o;
          if (bool1) {
            localObject1 = d;
          }
          for (;;)
          {
            localObject2 = localObject1;
            break;
            if (localObject1 == null) {
              localObject1 = b;
            }
          }
          i3 = paramState.getHeight() / 2;
          f3 = i3;
        }
        else
        {
          f1 = paramRecyclerView.getTranslationX();
          bool2 = f1 < 0.0F;
          if (bool2)
          {
            bool1 = o;
            if (bool1)
            {
              if (localObject1 == null) {
                localObject1 = b;
              }
            }
            else {
              localObject1 = d;
            }
            localObject2 = localObject1;
            i3 = paramState.getWidth();
            i2 = paramState.getHeight() / 2;
            f3 = i3 - i2;
            float f6 = paramState.getRight();
            f1 = paramRecyclerView.getTranslationX();
            f5 = f6 + f1;
            float f7 = paramState.getTop();
            float f8 = paramState.getRight();
            i3 = paramState.getBottom();
            float f9 = i3;
            Paint localPaint2 = e;
            paramCanvas.drawRect(f5, f7, f8, f9, localPaint2);
          }
        }
        if (localObject2 == null) {
          return;
        }
        int i3 = paramCanvas.save();
        int i2 = paramState.getTop();
        i1 = paramState.getHeight() / 2;
        i2 += i1;
        f1 = i2;
        paramCanvas.translate(f3, f1);
        float f10 = paramRecyclerView.getTranslationX();
        float f11 = paramState.getHeight();
        f10 = Math.min(Math.abs(f10 / f11), 1.0F);
        paramCanvas.scale(f10, f10);
        f11 = 255.0F;
        f10 *= f11;
        int i4 = (int)f10;
        ((Drawable)localObject2).setAlpha(i4);
        ((Drawable)localObject2).draw(paramCanvas);
        paramCanvas.restoreToCount(i3);
      }
    }
  }
  
  public final boolean onInterceptTouchEvent(RecyclerView paramRecyclerView, MotionEvent paramMotionEvent)
  {
    Object localObject = n;
    aa.b localb1 = aa.b.d;
    boolean bool1 = true;
    float f1 = Float.MIN_VALUE;
    if (localObject == localb1) {
      return bool1;
    }
    int i1 = paramMotionEvent.getActionMasked();
    localb1 = null;
    float f2;
    float f4;
    label289:
    int i3;
    switch (i1)
    {
    default: 
      break;
    case 2: 
      localObject = n;
      aa.b localb2 = aa.b.b;
      if (localObject == localb2)
      {
        f2 = j;
        float f3 = paramMotionEvent.getRawX();
        f2 = Math.abs(f2 - f3);
        f3 = a;
        boolean bool2 = f2 < f3;
        boolean bool3;
        if (bool2)
        {
          paramMotionEvent = a(paramRecyclerView);
          int i2 = paramRecyclerView.getChildAdapterPosition(paramMotionEvent);
          if (i2 >= 0)
          {
            localObject = g;
            bool3 = ((aa.a)localObject).a(i2, paramMotionEvent);
            if (bool3)
            {
              bool3 = f.b();
              o = bool3;
              paramRecyclerView = aa.b.c;
              n = paramRecyclerView;
              if (paramMotionEvent == null) {
                break label289;
              }
              paramRecyclerView = f;
              if (paramRecyclerView == null) {
                break label289;
              }
              paramRecyclerView = paramMotionEvent.getBackground();
              h = paramRecyclerView;
              paramRecyclerView = f;
              paramMotionEvent.setBackground(paramRecyclerView);
              i = bool1;
              break label289;
            }
          }
          paramRecyclerView = aa.b.a;
          n = paramRecyclerView;
        }
        else
        {
          f4 = k;
          float f5 = paramMotionEvent.getRawY();
          f4 = Math.abs(f4 - f5);
          f5 = a;
          bool3 = f4 < f5;
          if (bool3)
          {
            paramRecyclerView = aa.b.a;
            n = paramRecyclerView;
          }
        }
      }
      paramRecyclerView = n;
      paramMotionEvent = aa.b.c;
      if (paramRecyclerView == paramMotionEvent) {
        return bool1;
      }
      break;
    case 1: 
    case 3: 
      paramRecyclerView = n;
      paramMotionEvent = aa.b.c;
      if (paramRecyclerView == paramMotionEvent) {
        return bool1;
      }
      paramRecyclerView = n;
      paramMotionEvent = aa.b.b;
      if (paramRecyclerView == paramMotionEvent)
      {
        paramRecyclerView = aa.b.a;
        n = paramRecyclerView;
        i3 = -1;
        f4 = 0.0F / 0.0F;
        m = i3;
      }
      break;
    case 0: 
      f2 = paramMotionEvent.getX();
      f1 = paramMotionEvent.getY();
      localObject = paramRecyclerView.findChildViewUnder(f2, f1);
      i3 = paramRecyclerView.getChildAdapterPosition((View)localObject);
      m = i3;
      boolean bool4 = localObject instanceof ViewGroup;
      if (bool4)
      {
        localObject = (ViewGroup)localObject;
        paramRecyclerView = ((ViewGroup)localObject).getChildAt(0);
        if (paramRecyclerView != null)
        {
          paramRecyclerView = aa.b.b;
          n = paramRecyclerView;
          f4 = paramMotionEvent.getRawX();
          j = f4;
          f4 = paramMotionEvent.getRawY();
          k = f4;
        }
      }
      break;
    }
    return false;
  }
  
  public final void onRequestDisallowInterceptTouchEvent(boolean paramBoolean) {}
  
  public final void onTouchEvent(RecyclerView paramRecyclerView, MotionEvent paramMotionEvent)
  {
    View localView = a(paramRecyclerView);
    Object localObject1 = n;
    Object localObject2 = aa.b.d;
    if (localObject1 != localObject2)
    {
      int i1 = m;
      int i3 = -1;
      float f1 = 0.0F / 0.0F;
      if ((i1 != i3) && (localView != null))
      {
        i1 = paramMotionEvent.getActionMasked();
        boolean bool3 = true;
        float f2 = 0.5F;
        float f3 = 1.0F;
        float f4;
        float f5;
        int i4;
        int i5;
        switch (i1)
        {
        default: 
          break;
        case 3: 
          paramMotionEvent = g;
          paramMotionEvent.d();
          a(paramRecyclerView, bool3);
          break;
        case 2: 
          localObject1 = n;
          localObject2 = aa.b.c;
          if (localObject1 == localObject2)
          {
            f4 = paramMotionEvent.getRawX();
            f1 = j;
            f4 = (f4 - f1) * f2;
            f1 = Math.abs(f4);
            boolean bool2 = f1 < f3;
            if (bool2)
            {
              f5 = paramMotionEvent.getRawX();
              f1 = j;
              f5 = Math.abs(f5 - f1);
              i4 = localView.getWidth();
              i5 = localView.getHeight();
              f1 = i4 - i5;
              f5 /= f1;
              i4 = 1073741824;
              f1 = 2.0F;
              f5 /= f1;
              f3 -= f5;
            }
            f4 *= f3;
            localView.setTranslationX(f4);
            paramRecyclerView.invalidateItemDecorations();
            return;
          }
          break;
        case 1: 
          f5 = paramMotionEvent.getRawX();
          f4 = j;
          f5 = (f5 - f4) * f2;
          f4 = localView.getHeight();
          f4 = Math.abs(f5 / f4);
          boolean bool1 = f4 < f3;
          if (bool1)
          {
            int i2 = paramRecyclerView.getChildAdapterPosition(localView);
            if (i2 == i4)
            {
              localObject1 = paramRecyclerView.getViewTreeObserver();
              localObject2 = new com/truecaller/ui/aa$1;
              ((aa.1)localObject2).<init>(this, paramRecyclerView, localView, f5);
              ((ViewTreeObserver)localObject1).addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)localObject2);
            }
            else
            {
              a(i2, f5, localView);
            }
            a(paramRecyclerView, false);
            return;
          }
          g.d();
          a(paramRecyclerView, i5);
          return;
        }
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */