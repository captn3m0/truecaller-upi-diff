package com.truecaller.ui.view;

import android.content.Context;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import c.g.b.k;

public final class VerticalNestedScrollView
  extends NestedScrollView
{
  private int a;
  private float b;
  private float c;
  private float d;
  private float e;
  private final int f;
  
  public VerticalNestedScrollView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private VerticalNestedScrollView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, (byte)0);
    f = paramInt;
  }
  
  public final boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    String str = "motion";
    k.b(paramMotionEvent, str);
    int i = paramMotionEvent.getAction();
    int j = -1;
    float f1 = 0.0F / 0.0F;
    int k;
    float f2;
    float f4;
    float f5;
    if (i != 0)
    {
      k = 2;
      f2 = 2.8E-45F;
      if (i == k)
      {
        f3 = paramMotionEvent.getX();
        f2 = paramMotionEvent.getY();
        f4 = b;
        f5 = d;
        f5 = Math.abs(f3 - f5);
        f4 += f5;
        b = f4;
        f4 = c;
        f5 = e;
        f5 = Math.abs(f2 - f5);
        f4 += f5;
        c = f4;
        d = f3;
        e = f2;
      }
    }
    else
    {
      i = 0;
      str = null;
      b = 0.0F;
      c = 0.0F;
      f3 = paramMotionEvent.getX();
      d = f3;
      f3 = paramMotionEvent.getY();
      e = f3;
      a = j;
    }
    boolean bool1 = super.onInterceptTouchEvent(paramMotionEvent);
    i = 0;
    float f3 = 0.0F;
    str = null;
    if (bool1)
    {
      float f6 = c;
      k = f;
      f4 = k;
      boolean bool2 = true;
      f5 = Float.MIN_VALUE;
      bool1 = f6 < f4;
      if (!bool1)
      {
        f6 = b;
        f2 = k;
        bool1 = f6 < f2;
        if (!bool1) {}
      }
      else
      {
        int m = a;
        if (m == j)
        {
          f6 = Math.abs(c);
          f1 = Math.abs(b);
          int n = f6 < f1;
          if (n > 0)
          {
            n = 0;
            f6 = 0.0F;
            paramMotionEvent = null;
          }
          else
          {
            n = 1;
            f6 = Float.MIN_VALUE;
          }
          a = n;
        }
      }
      int i1 = a;
      if (i1 == 0)
      {
        i1 = 1;
        f6 = Float.MIN_VALUE;
      }
      else
      {
        i1 = 0;
        f6 = 0.0F;
        paramMotionEvent = null;
      }
      if (i1 != 0) {
        return bool2;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.VerticalNestedScrollView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */