package com.truecaller.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.widget.ImageView.ScaleType;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.e;
import com.d.b.t;
import com.d.b.w;
import com.truecaller.common.h.aq.d;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil;
import com.truecaller.old.data.access.Settings;
import com.truecaller.search.local.model.CallCache.Call;
import com.truecaller.search.local.model.a.i;
import com.truecaller.search.local.model.j;
import com.truecaller.util.at;
import com.truecaller.util.bj;

public class ContactPhoto
  extends AppCompatImageView
{
  public Object a;
  private final RectF b;
  private Object c;
  private e d;
  private final SparseArray e;
  private int f;
  private boolean g;
  private boolean h;
  private boolean i;
  private boolean j;
  private boolean k;
  private boolean l;
  private boolean m;
  private int n;
  private int o;
  private int p;
  private final int q;
  private final int r;
  private final int s;
  private final int t;
  private final int u;
  private final int v;
  private final Paint w;
  private final Paint x;
  private int y;
  
  public ContactPhoto(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private ContactPhoto(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet, 0);
    paramAttributeSet = new android/graphics/RectF;
    paramAttributeSet.<init>();
    b = paramAttributeSet;
    paramAttributeSet = new android/util/SparseArray;
    paramAttributeSet.<init>();
    e = paramAttributeSet;
    boolean bool1 = isInEditMode();
    int i1;
    if (bool1)
    {
      i1 = -12303292;
    }
    else
    {
      paramAttributeSet = getContext();
      i4 = 2130969536;
      i1 = com.truecaller.utils.ui.b.a(paramAttributeSet, i4);
    }
    p = i1;
    boolean bool2 = isInEditMode();
    int i4 = -65536;
    int i2;
    if (bool2)
    {
      i2 = -65536;
    }
    else
    {
      paramAttributeSet = getContext();
      i5 = 2130969538;
      i2 = com.truecaller.utils.ui.b.a(paramAttributeSet, i5);
    }
    r = i2;
    boolean bool3 = isInEditMode();
    int i5 = -16776961;
    if (bool3)
    {
      i3 = -16776961;
    }
    else
    {
      paramAttributeSet = getContext();
      int i6 = 2130969548;
      i3 = com.truecaller.utils.ui.b.a(paramAttributeSet, i6);
    }
    s = i3;
    int i3 = 2131100375;
    int i7 = android.support.v4.content.b.c(paramContext, i3);
    q = i7;
    boolean bool4 = isInEditMode();
    int i8;
    if (bool4)
    {
      i8 = -7829368;
    }
    else
    {
      paramContext = getContext();
      i3 = 2130969537;
      i8 = com.truecaller.utils.ui.b.a(paramContext, i3);
    }
    t = i8;
    boolean bool5 = isInEditMode();
    if (!bool5)
    {
      paramContext = getContext();
      i3 = 2130969539;
      i4 = com.truecaller.utils.ui.b.a(paramContext, i3);
    }
    u = i4;
    bool5 = isInEditMode();
    if (!bool5)
    {
      paramContext = getContext();
      i3 = 2130969528;
      i5 = com.truecaller.utils.ui.b.a(paramContext, i3);
    }
    v = i5;
    paramContext = new android/graphics/Paint;
    i3 = 1;
    paramContext.<init>(i3);
    w = paramContext;
    paramContext = w;
    Paint.Style localStyle = Paint.Style.FILL;
    paramContext.setStyle(localStyle);
    paramContext = new android/graphics/Paint;
    paramContext.<init>(i3);
    x = paramContext;
    paramContext = x;
    localStyle = Paint.Style.FILL;
    paramContext.setStyle(localStyle);
    paramContext = x;
    i4 = -1;
    paramContext.setColor(i4);
    bool5 = isInEditMode();
    if (!bool5)
    {
      bool5 = Settings.d();
      if (bool5)
      {
        i3 = 0;
        paramAttributeSet = null;
      }
    }
    k = i3;
    y = 0;
  }
  
  private Drawable a(int paramInt)
  {
    Drawable localDrawable = (Drawable)e.get(paramInt);
    if (localDrawable == null)
    {
      localDrawable = android.support.v4.content.b.a(getContext(), paramInt).mutate();
      a(localDrawable);
      SparseArray localSparseArray = e;
      localSparseArray.put(paramInt, localDrawable);
    }
    return localDrawable;
  }
  
  private void a(Drawable paramDrawable)
  {
    if (paramDrawable == null) {
      return;
    }
    int i1 = paramDrawable.getIntrinsicWidth();
    int i2 = getWidth();
    i1 = Math.min(i1, i2) / 2;
    i2 = paramDrawable.getIntrinsicHeight();
    int i3 = getHeight();
    i2 = Math.min(i2, i3) / 2;
    i3 = -i1;
    int i4 = -i2;
    paramDrawable.setBounds(i3, i4, i1, i2);
  }
  
  private boolean a(String paramString)
  {
    try
    {
      Context localContext = getContext();
      return bj.b(localContext, paramString);
    }
    catch (SecurityException paramString)
    {
      String[] arrayOfString = new String[0];
      AssertionUtil.shouldNeverHappen(paramString, arrayOfString);
    }
    return false;
  }
  
  private boolean c()
  {
    int i1 = y;
    return i1 != 0;
  }
  
  public final void a()
  {
    p = 0;
  }
  
  public final void a(Object paramObject1, Object paramObject2)
  {
    a = paramObject1;
    c = paramObject2;
    Object localObject1 = b;
    float f1 = ((RectF)localObject1).width();
    boolean bool1 = false;
    Object localObject2 = null;
    boolean bool2 = f1 < 0.0F;
    if (bool2)
    {
      localObject1 = b;
      f1 = ((RectF)localObject1).height();
      bool2 = f1 < 0.0F;
      if (bool2)
      {
        w.a(getContext()).d(this);
        bool2 = false;
        f1 = 0.0F;
        localObject1 = null;
        i = false;
        bool1 = paramObject1 instanceof i;
        boolean bool3 = true;
        int i1;
        if (bool1)
        {
          paramObject1 = ((i)paramObject1).b();
        }
        else
        {
          bool1 = paramObject1 instanceof Integer;
          if (bool1)
          {
            localObject2 = getContext();
            i1 = ((Integer)paramObject1).intValue();
            paramObject1 = at.a((Context)localObject2, i1);
          }
          else
          {
            bool1 = paramObject1 instanceof CallCache.Call;
            if (bool1)
            {
              localObject2 = paramObject1;
              localObject2 = (CallCache.Call)paramObject1;
              str = g;
              bool4 = TextUtils.isEmpty(str);
              if (!bool4)
              {
                localObject2 = h;
                bool1 = a((String)localObject2);
                if (bool1) {
                  i = bool3;
                }
              }
            }
            else
            {
              bool1 = paramObject1 instanceof String;
              if (bool1)
              {
                localObject2 = paramObject1;
                localObject2 = (String)paramObject1;
                bool1 = a((String)localObject2);
                if (bool1) {
                  i = bool3;
                }
              }
            }
          }
        }
        invalidate();
        bool1 = paramObject1 instanceof Drawable;
        if (bool1)
        {
          paramObject2 = ImageView.ScaleType.FIT_XY;
          setScaleType((ImageView.ScaleType)paramObject2);
          paramObject1 = (Drawable)paramObject1;
          setImageDrawable((Drawable)paramObject1);
          return;
        }
        bool1 = paramObject1 instanceof j;
        boolean bool4 = false;
        String str = null;
        if (bool1)
        {
          paramObject1 = ((j)paramObject1).c();
        }
        else
        {
          bool1 = paramObject1 instanceof Contact;
          if (bool1)
          {
            paramObject1 = ((Contact)paramObject1).a(bool3);
          }
          else
          {
            bool1 = paramObject1 instanceof String;
            if (bool1)
            {
              paramObject1 = Uri.parse((String)paramObject1);
            }
            else
            {
              bool1 = paramObject1 instanceof Uri;
              if (bool1)
              {
                paramObject1 = (Uri)paramObject1;
              }
              else
              {
                i1 = 0;
                paramObject1 = null;
              }
            }
          }
        }
        localObject2 = ImageView.ScaleType.FIT_CENTER;
        setScaleType((ImageView.ScaleType)localObject2);
        setImageDrawable(null);
        if (paramObject1 != null)
        {
          paramObject1 = w.a(getContext()).a((Uri)paramObject1);
          c = bool3;
          paramObject1 = ((ab)paramObject1).b();
          localObject2 = aq.d.b();
          paramObject1 = ((ab)paramObject1).a((ai)localObject2);
          if (paramObject2 != null) {
            ((ab)paramObject1).a(paramObject2);
          }
          boolean bool5 = k;
          if (bool5)
          {
            paramObject2 = t.c;
            localObject1 = new t[0];
            ((ab)paramObject1).a((t)paramObject2);
          }
          paramObject2 = d;
          ((ab)paramObject1).a(this, (e)paramObject2);
        }
        return;
      }
    }
  }
  
  public final void b()
  {
    w.a(getContext()).d(this);
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    int i1 = getWidth();
    int i2 = getHeight();
    int i3 = f;
    Drawable localDrawable;
    int i16;
    if (i3 != 0)
    {
      localDrawable = a(i3);
    }
    else
    {
      boolean bool1 = isActivated();
      if (bool1)
      {
        int i4 = 2131233797;
        localDrawable = a(i4);
      }
      else
      {
        boolean bool2 = h;
        if (bool2)
        {
          int i5 = 2131234172;
          localDrawable = a(i5);
        }
        else
        {
          boolean bool3 = i;
          if (bool3)
          {
            int i6 = 2131234670;
            localDrawable = a(i6);
          }
          else
          {
            boolean bool4 = j;
            if (bool4)
            {
              int i7 = 2131233850;
              localDrawable = a(i7);
            }
            else
            {
              boolean bool5 = m;
              int i9;
              if (bool5)
              {
                int i8 = 2131234163;
              }
              else
              {
                boolean bool6 = c();
                if (bool6) {
                  i9 = y;
                } else {
                  i9 = 2131233852;
                }
              }
              localDrawable = a(i9);
              int i11 = -i1 / 2;
              i16 = -i2 / 2;
              i18 = i1 / 2;
              i19 = i2 / 2;
              localDrawable.setBounds(i11, i16, i18, i19);
            }
          }
        }
      }
    }
    boolean bool8 = isActivated();
    Paint localPaint1;
    Object localObject1;
    if (bool8)
    {
      localPaint1 = w;
      i16 = s;
      localPaint1.setColor(i16);
      int i12 = v;
      localObject1 = PorterDuff.Mode.SRC_IN;
      localDrawable.setColorFilter(i12, (PorterDuff.Mode)localObject1);
    }
    else
    {
      boolean bool9 = g;
      if (bool9)
      {
        localPaint1 = w;
        i16 = r;
        localPaint1.setColor(i16);
        int i13 = u;
        localObject1 = PorterDuff.Mode.SRC_IN;
        localDrawable.setColorFilter(i13, (PorterDuff.Mode)localObject1);
      }
      else
      {
        boolean bool10 = j;
        if (bool10)
        {
          localPaint1 = w;
          i16 = p;
          localPaint1.setColor(i16);
          f1 = 0.0F / 0.0F;
          localObject1 = PorterDuff.Mode.SRC_IN;
          localDrawable.setColorFilter(-1, (PorterDuff.Mode)localObject1);
          int i14 = localDrawable.getIntrinsicWidth() / 2;
          i16 = localDrawable.getIntrinsicHeight() / 2;
          i18 = -i14;
          i19 = -i16;
          localDrawable.setBounds(i18, i19, i14, i16);
        }
        else
        {
          boolean bool11 = m;
          if (bool11)
          {
            localPaint1 = w;
            i16 = q;
            localPaint1.setColor(i16);
            bool11 = false;
            f1 = 0.0F;
            localPaint1 = null;
            localDrawable.setColorFilter(null);
          }
          else
          {
            bool11 = c();
            if (!bool11)
            {
              localPaint1 = w;
              i16 = p;
              localPaint1.setColor(i16);
              i15 = t;
              localObject1 = PorterDuff.Mode.SRC_IN;
              localDrawable.setColorFilter(i15, (PorterDuff.Mode)localObject1);
            }
          }
        }
      }
    }
    int i15 = paramCanvas.save();
    float f2 = i1 / 2;
    int i18 = i2 / 2;
    float f3 = i18;
    paramCanvas.translate(f2, f3);
    f2 = Math.min(i1, i2) / 2;
    Paint localPaint2 = w;
    int i19 = 0;
    paramCanvas.drawCircle(0.0F, 0.0F, f2, localPaint2);
    boolean bool12 = l;
    if (!bool12) {
      localDrawable.draw(paramCanvas);
    }
    paramCanvas.restoreToCount(i15);
    boolean bool7 = isActivated();
    if (!bool7) {
      super.onDraw(paramCanvas);
    }
    int i10 = n;
    i15 = 1077936128;
    float f1 = 3.0F;
    int i17;
    float f4;
    float f5;
    if (i10 != 0)
    {
      i17 = paramCanvas.save();
      localDrawable = a(i10);
      i15 = (int)(i1 / f1) / 2;
      i18 = -i15;
      localDrawable.setBounds(i18, i18, i15, i15);
      f4 = i1 - i15;
      f5 = i2 - i15;
      paramCanvas.translate(f4, f5);
      localDrawable.draw(paramCanvas);
      paramCanvas.restoreToCount(i17);
      return;
    }
    i10 = o;
    if (i10 != 0)
    {
      i10 = paramCanvas.save();
      i17 = o;
      i18 = 1;
      f3 = Float.MIN_VALUE;
      if (i17 == i18)
      {
        i17 = 2131234003;
        f2 = 1.808416E38F;
      }
      else
      {
        i17 = 2131234582;
        f2 = 1.8085334E38F;
      }
      localObject1 = a(i17);
      int i20 = o;
      if (i20 == i18)
      {
        i18 = 1082549862;
        f3 = 4.2F;
      }
      else
      {
        i18 = 1080452710;
        f3 = 3.6F;
      }
      float f6 = i1;
      f1 = f6 / f1;
      i15 = (int)f1 / 2;
      f6 /= f3;
      i18 = (int)f6 / 2;
      i20 = -i18;
      ((Drawable)localObject1).setBounds(i20, i20, i18, i18);
      f4 = i1 - i15;
      i2 -= i15;
      f5 = i2;
      paramCanvas.translate(f4, f5);
      i1 = v;
      Object localObject2 = PorterDuff.Mode.SRC_IN;
      ((Drawable)localObject1).setColorFilter(i1, (PorterDuff.Mode)localObject2);
      f4 = i15;
      localObject2 = x;
      paramCanvas.drawCircle(0.0F, 0.0F, f4, (Paint)localObject2);
      ((Drawable)localObject1).draw(paramCanvas);
      paramCanvas.restoreToCount(i10);
    }
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
    boolean bool = isInEditMode();
    if (bool)
    {
      ImageView.ScaleType localScaleType = ImageView.ScaleType.CENTER;
      setScaleType(localScaleType);
      int i1 = 2131233849;
      setImageResource(i1);
    }
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    RectF localRectF = b;
    float f1 = paramInt1;
    float f2 = paramInt2;
    paramInt4 = 0;
    localRectF.set(0.0F, 0.0F, f1, f2);
    paramInt1 = isInEditMode();
    Object localObject2;
    if (paramInt1 == 0)
    {
      localObject1 = a;
      localObject2 = c;
      a(localObject1, localObject2);
    }
    paramInt1 = 0;
    f1 = 0.0F;
    Object localObject1 = null;
    for (;;)
    {
      localObject2 = e;
      paramInt2 = ((SparseArray)localObject2).size();
      if (paramInt1 >= paramInt2) {
        break;
      }
      localObject2 = (Drawable)e.valueAt(paramInt1);
      a((Drawable)localObject2);
      paramInt1 += 1;
    }
  }
  
  public void setAvatarIconHidden(boolean paramBoolean)
  {
    l = paramBoolean;
  }
  
  public void setBackupBadge(int paramInt)
  {
    o = paramInt;
    invalidate();
  }
  
  public void setCallback(e parame)
  {
    d = parame;
  }
  
  public void setContactBadgeDrawable(int paramInt)
  {
    n = paramInt;
    invalidate();
  }
  
  public void setDrawableRes(int paramInt)
  {
    f = paramInt;
  }
  
  public void setIsDownload(boolean paramBoolean)
  {
    j = paramBoolean;
    invalidate();
  }
  
  public void setIsGold(boolean paramBoolean)
  {
    m = paramBoolean;
    invalidate();
  }
  
  public void setIsGroup(boolean paramBoolean)
  {
    h = paramBoolean;
    invalidate();
  }
  
  public void setIsSpam(boolean paramBoolean)
  {
    g = paramBoolean;
    invalidate();
  }
  
  public void setOfflineMode(boolean paramBoolean)
  {
    k = paramBoolean;
  }
  
  public void setPrivateAvatar(int paramInt)
  {
    y = paramInt;
    invalidate();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.ContactPhoto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */