package com.truecaller.ui.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

final class h$3
  extends AnimatorListenerAdapter
{
  h$3(h paramh) {}
  
  public final void onAnimationCancel(Animator paramAnimator)
  {
    h.f(a);
  }
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    paramAnimator = a;
    boolean bool = h.g(paramAnimator);
    if (!bool)
    {
      paramAnimator = a.a;
      paramAnimator.start();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.h.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */