package com.truecaller.ui.view;

import android.content.Context;
import android.content.res.Resources.Theme;
import android.graphics.Canvas;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v4.content.b;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.MeasureSpec;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;

public class ThemePreviewView
  extends View
{
  public ContextThemeWrapper a;
  private Drawable b;
  private Drawable c;
  private Drawable d;
  private Drawable e;
  private Drawable f;
  private Drawable g;
  private Drawable h;
  private int i;
  private int j;
  private int k;
  private int l;
  private int m;
  
  public ThemePreviewView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private ThemePreviewView(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet, 0);
    paramContext = new android/view/ContextThemeWrapper;
    paramAttributeSet = getContext();
    boolean bool = isInEditMode();
    ThemeManager.Theme localTheme;
    if (bool) {
      localTheme = ThemeManager.Theme.DEFAULT;
    } else {
      localTheme = ThemeManager.a();
    }
    int n = resId;
    paramContext.<init>(paramAttributeSet, n);
    a = paramContext;
    paramContext = b.a(getContext(), 2131234980);
    b = paramContext;
    paramContext = b.a(getContext(), 2131234976);
    c = paramContext;
    paramContext = b.a(getContext(), 2131234981);
    d = paramContext;
    paramContext = b.a(getContext(), 2131234982);
    e = paramContext;
    paramContext = b.a(getContext(), 2131234978);
    f = paramContext;
    paramContext = b.a(getContext(), 2131234977);
    g = paramContext;
    paramContext = b.a(getContext(), 2131234979);
    h = paramContext;
    paramContext = b;
    int i1 = paramContext.getIntrinsicWidth();
    n = b.getIntrinsicHeight();
    paramContext.setBounds(0, 0, i1, n);
    paramContext = c;
    i1 = paramContext.getIntrinsicWidth();
    n = c.getIntrinsicHeight();
    paramContext.setBounds(0, 0, i1, n);
    paramContext = d;
    i1 = paramContext.getIntrinsicWidth();
    n = d.getIntrinsicHeight();
    paramContext.setBounds(0, 0, i1, n);
    paramContext = e;
    i1 = paramContext.getIntrinsicWidth();
    n = e.getIntrinsicHeight();
    paramContext.setBounds(0, 0, i1, n);
    paramContext = f;
    i1 = paramContext.getIntrinsicWidth();
    n = f.getIntrinsicHeight();
    paramContext.setBounds(0, 0, i1, n);
    paramContext = g;
    i1 = paramContext.getIntrinsicWidth();
    n = g.getIntrinsicHeight();
    paramContext.setBounds(0, 0, i1, n);
    paramContext = h;
    i1 = paramContext.getIntrinsicWidth();
    n = h.getIntrinsicHeight();
    paramContext.setBounds(0, 0, i1, n);
    a();
  }
  
  private void a(Resources.Theme paramTheme)
  {
    TypedValue localTypedValue = new android/util/TypedValue;
    localTypedValue.<init>();
    boolean bool = true;
    paramTheme.resolveAttribute(2130969528, localTypedValue, bool);
    Context localContext = getContext();
    int n = resourceId;
    int i1 = b.c(localContext, n);
    i = i1;
    paramTheme.resolveAttribute(2130969591, localTypedValue, bool);
    localContext = getContext();
    n = resourceId;
    i1 = b.c(localContext, n);
    j = i1;
    paramTheme.resolveAttribute(2130969593, localTypedValue, bool);
    localContext = getContext();
    n = resourceId;
    i1 = b.c(localContext, n);
    k = i1;
    paramTheme.resolveAttribute(2130969536, localTypedValue, bool);
    localContext = getContext();
    n = resourceId;
    i1 = b.c(localContext, n);
    m = i1;
    paramTheme.resolveAttribute(2130969548, localTypedValue, bool);
    paramTheme = getContext();
    int i2 = resourceId;
    int i3 = b.c(paramTheme, i2);
    l = i3;
  }
  
  private void b()
  {
    Drawable localDrawable = c;
    int n = m;
    PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
    localDrawable.setColorFilter(n, localMode);
    localDrawable = d;
    n = l;
    localMode = PorterDuff.Mode.SRC_IN;
    localDrawable.setColorFilter(n, localMode);
    localDrawable = e;
    n = l;
    localMode = PorterDuff.Mode.SRC_IN;
    localDrawable.setColorFilter(n, localMode);
    localDrawable = f;
    n = k;
    localMode = PorterDuff.Mode.SRC_IN;
    localDrawable.setColorFilter(n, localMode);
    localDrawable = h;
    n = j;
    localMode = PorterDuff.Mode.SRC_IN;
    localDrawable.setColorFilter(n, localMode);
    localDrawable = g;
    n = i;
    localMode = PorterDuff.Mode.SRC_IN;
    localDrawable.setColorFilter(n, localMode);
  }
  
  public final void a()
  {
    Resources.Theme localTheme = a.getTheme();
    if (localTheme != null)
    {
      a(localTheme);
      b();
    }
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    b.draw(paramCanvas);
    c.draw(paramCanvas);
    d.draw(paramCanvas);
    f.draw(paramCanvas);
    e.draw(paramCanvas);
    g.draw(paramCanvas);
    h.draw(paramCanvas);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    paramInt1 = b.getIntrinsicWidth();
    paramInt2 = 1073741824;
    paramInt1 = View.MeasureSpec.makeMeasureSpec(paramInt1, paramInt2);
    paramInt2 = View.MeasureSpec.makeMeasureSpec(b.getIntrinsicHeight(), paramInt2);
    super.onMeasure(paramInt1, paramInt2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.ThemePreviewView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */