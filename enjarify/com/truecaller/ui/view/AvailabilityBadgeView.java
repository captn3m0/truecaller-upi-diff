package com.truecaller.ui.view;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.view.r;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.Availability.Context;
import com.truecaller.api.services.presence.v1.models.Availability.Status;
import com.truecaller.search.local.model.c.a;
import com.truecaller.search.local.model.c.b;
import com.truecaller.utils.extensions.t;

public final class AvailabilityBadgeView
  extends View
  implements c.b
{
  public c.a a;
  private final int b;
  
  public AvailabilityBadgeView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private AvailabilityBadgeView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    int i = getResources().getDimensionPixelSize(2131165301);
    b = i;
  }
  
  private final void setAvailability(com.truecaller.presence.a parama)
  {
    ViewGroup.LayoutParams localLayoutParams = getLayoutParams();
    int i = b;
    height = i;
    width = i;
    setBackgroundResource(0);
    t.a(this);
    i = 0;
    Availability.Context localContext = null;
    if (parama != null)
    {
      localObject = b;
      if (localObject != null)
      {
        localObject = ((Availability)localObject).a();
        break label65;
      }
    }
    int j = 0;
    Object localObject = null;
    label65:
    if (parama != null)
    {
      parama = b;
      if (parama != null) {
        localContext = parama.b();
      }
    }
    if (localObject != null)
    {
      parama = a.b;
      j = ((Availability.Status)localObject).ordinal();
      int k = parama[j];
      switch (k)
      {
      default: 
        break;
      case 2: 
        if (localContext != null)
        {
          parama = a.a;
          i = localContext.ordinal();
          k = parama[i];
          switch (k)
          {
          default: 
            break;
          case 2: 
            k = 2131230890;
            setBackgroundResource(k);
            break;
          case 1: 
            k = 2131230889;
            setBackgroundResource(k);
            break;
          }
        }
        k = 2131230888;
        setBackgroundResource(k);
        break;
      case 1: 
        k = 2131230887;
        setBackgroundResource(k);
        break;
      }
    }
    t.b(this);
    setLayoutParams(localLayoutParams);
  }
  
  public final void a()
  {
    c.a locala = a;
    if (locala != null)
    {
      boolean bool = locala.a();
      if (!bool)
      {
        Object localObject = this;
        localObject = (View)this;
        bool = r.H((View)localObject);
        if (bool)
        {
          bool = false;
          setAvailability(null);
          localObject = this;
          localObject = (c.b)this;
          locala.a((c.b)localObject);
        }
      }
      return;
    }
  }
  
  public final void a(com.truecaller.presence.a parama)
  {
    setAvailability(parama);
  }
  
  public final void b()
  {
    c.a locala = a;
    if (locala != null)
    {
      boolean bool = locala.a();
      if (bool) {
        locala.b();
      }
    }
    setAvailability(null);
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    a();
  }
  
  protected final void onDetachedFromWindow()
  {
    b();
    super.onDetachedFromWindow();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.AvailabilityBadgeView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */