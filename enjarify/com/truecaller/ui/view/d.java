package com.truecaller.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;

public abstract class d
  extends ViewGroup
{
  public d(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  protected abstract int getColumnCount();
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    paramInt3 -= paramInt1;
    paramInt4 -= paramInt2;
    paramBoolean = getColumnCount();
    paramInt1 = getChildCount();
    float f1 = paramInt1;
    float f2 = 1.0F;
    f1 *= f2;
    float f3 = paramBoolean;
    f1 /= f3;
    int i = 1056964608;
    float f4 = 0.5F;
    paramInt2 = (int)(f1 + f4);
    float f5 = paramInt3 * f2 / f3;
    float f6 = paramInt4 * f2;
    f1 = paramInt2;
    f6 /= f1;
    paramInt2 = (int)f5;
    int j = 1073741824;
    f2 = 2.0F;
    paramInt2 = View.MeasureSpec.makeMeasureSpec(paramInt2, j);
    int k = (int)f6;
    j = View.MeasureSpec.makeMeasureSpec(k, j);
    measureChildren(paramInt2, j);
    paramInt2 = 0;
    f1 = 0.0F;
    while (paramInt2 < paramInt1)
    {
      View localView = getChildAt(paramInt2);
      k = paramInt2 / paramBoolean;
      f4 = paramInt2 % paramBoolean * f5;
      i = Math.round(f4);
      float f7 = i + f5;
      int m = Math.round(f7);
      f3 = k * f6;
      k = Math.round(f3);
      float f8 = k + f6;
      int n = Math.round(f8);
      localView.layout(i, k, m, n);
      paramInt2 += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */