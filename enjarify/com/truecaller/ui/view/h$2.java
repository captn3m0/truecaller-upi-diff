package com.truecaller.ui.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.drawable.Drawable;

final class h$2
  extends AnimatorListenerAdapter
{
  h$2(h paramh) {}
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    paramAnimator = a;
    Drawable[] arrayOfDrawable = h.d(paramAnimator);
    h.a(paramAnimator, arrayOfDrawable);
    a.invalidateSelf();
  }
  
  public final void onAnimationStart(Animator paramAnimator)
  {
    paramAnimator = a;
    Drawable[] arrayOfDrawable = h.c(paramAnimator);
    h.a(paramAnimator, arrayOfDrawable);
    paramAnimator = h.d(a);
    int i = paramAnimator.length;
    int j = 0;
    while (j < i)
    {
      Drawable localDrawable = paramAnimator[j];
      h localh = a;
      int k = h.e(localh);
      h.a(localh, localDrawable, k);
      j += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.h.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */