package com.truecaller.ui.view;

import android.animation.ArgbEvaluator;
import android.animation.TimeInterpolator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.f.c;
import com.truecaller.featuretoggles.e;
import com.truecaller.truepay.Truepay;
import com.truecaller.util.at;

public class BottomBar
  extends FrameLayout
  implements View.OnClickListener
{
  private boolean A;
  public ImageView a;
  public ImageView b;
  public ImageView c;
  public boolean d;
  private BottomBar.a e;
  private FloatingActionButton f;
  private TextView g;
  private TextView h;
  private TextView i;
  private TextView j;
  private TextView k;
  private TextView l;
  private ImageView m;
  private ImageView n;
  private ImageView o;
  private ImageView p;
  private int q;
  private int r;
  private SparseIntArray s;
  private SparseArray t;
  private View u;
  private BottomBar.DialpadState v;
  private Object w;
  private ImageView x;
  private boolean y;
  private boolean z;
  
  public BottomBar(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private BottomBar(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet, 0);
    paramAttributeSet = BottomBar.DialpadState.DIALPAD_DOWN;
    v = paramAttributeSet;
    y = false;
    z = false;
    A = false;
    paramAttributeSet = TrueApp.y();
    Object localObject2 = paramAttributeSet.f();
    boolean bool1 = paramAttributeSet.isTcPayEnabled();
    y = bool1;
    com.truecaller.featuretoggles.b localb = ((e)localObject2).ap();
    bool1 = localb.a();
    int i2 = 1;
    if (bool1)
    {
      bool2 = paramAttributeSet.p();
      if (bool2)
      {
        bool2 = true;
        break label104;
      }
    }
    boolean bool2 = false;
    paramAttributeSet = null;
    label104:
    z = bool2;
    bool2 = ((e)localObject2).e().a();
    A = bool2;
    int i3 = com.truecaller.utils.ui.b.d(paramContext, 2130969389);
    r = i3;
    paramAttributeSet = getResources();
    int i4 = 17694720;
    i3 = paramAttributeSet.getInteger(i4);
    q = i3;
    boolean bool3 = y;
    if (!bool3)
    {
      bool3 = z;
      if (!bool3) {}
    }
    else
    {
      paramByte = 1;
    }
    if (paramByte != 0) {
      b1 = 2131559152;
    } else {
      b1 = 2131559153;
    }
    localObject1 = getContext();
    inflate((Context)localObject1, b1, this);
    paramAttributeSet = (FloatingActionButton)findViewById(2131363019);
    f = paramAttributeSet;
    paramAttributeSet = f;
    paramAttributeSet.setOnClickListener(this);
    byte b1 = Build.VERSION.SDK_INT;
    paramByte = 21;
    if (b1 < paramByte)
    {
      paramAttributeSet = (FrameLayout.LayoutParams)f.getLayoutParams();
      paramByte = leftMargin;
      i4 = topMargin;
      i1 = rightMargin;
      i2 = bottomMargin;
      float f1 = 20.0F;
      int i5 = at.a(paramContext, f1);
      i2 -= i5;
      paramAttributeSet.setMargins(paramByte, i4, i1, i2);
      localObject1 = f;
      ((FloatingActionButton)localObject1).setLayoutParams(paramAttributeSet);
    }
    paramAttributeSet = new android/util/SparseIntArray;
    paramByte = 2;
    paramAttributeSet.<init>(paramByte);
    s = paramAttributeSet;
    paramAttributeSet = s;
    i4 = DIALPAD_DOWNcolorAttr;
    int i1 = DIALPAD_DOWNcolorAttr;
    i1 = com.truecaller.utils.ui.b.a(paramContext, i1);
    paramAttributeSet.put(i4, i1);
    paramAttributeSet = s;
    i4 = NUMBER_ENTEREDcolorAttr;
    i1 = NUMBER_ENTEREDcolorAttr;
    i1 = com.truecaller.utils.ui.b.a(paramContext, i1);
    paramAttributeSet.put(i4, i1);
    paramAttributeSet = new android/util/SparseArray;
    paramAttributeSet.<init>(paramByte);
    t = paramAttributeSet;
    paramAttributeSet = t;
    paramByte = DIALPAD_DOWNdrawable;
    i4 = DIALPAD_DOWNdrawable;
    i1 = 2130969588;
    localObject2 = com.truecaller.utils.ui.b.a(paramContext, i4, i1);
    paramAttributeSet.put(paramByte, localObject2);
    paramAttributeSet = t;
    paramByte = NUMBER_ENTEREDdrawable;
    i4 = NUMBER_ENTEREDdrawable;
    paramContext = com.truecaller.utils.ui.b.a(paramContext, i4, i1);
    paramAttributeSet.put(paramByte, paramContext);
    paramContext = findViewById(2131364634);
    u = paramContext;
  }
  
  private ImageView a(int paramInt1, String paramString, int paramInt2, int paramInt3)
  {
    ImageView localImageView = (ImageView)findViewById(paramInt1);
    localImageView.setImageResource(paramInt2);
    com.truecaller.utils.ui.b.a(getContext(), localImageView, 2130968688);
    String str = getResources().getString(paramInt3);
    localImageView.setContentDescription(str);
    localImageView.setOnClickListener(this);
    localImageView.setTag(paramString);
    return localImageView;
  }
  
  private TextView a(int paramInt1, int paramInt2)
  {
    TextView localTextView = (TextView)findViewById(paramInt1);
    localTextView.setText(paramInt2);
    return localTextView;
  }
  
  private void a(View paramView)
  {
    Object localObject1 = "calls";
    Object localObject2 = paramView.getTag();
    boolean bool1 = ((String)localObject1).equals(localObject2);
    int i1 = 1065353216;
    float f1 = 1.0F;
    boolean bool2 = true;
    Object localObject3;
    float f2;
    if (bool1)
    {
      f();
      localObject1 = f;
      localObject3 = v;
      f2 = scale;
      a((View)localObject1, bool2, f2);
      localObject1 = n;
      a((View)localObject1, false, f1);
      localObject1 = j;
      if (localObject1 != null)
      {
        i1 = 8;
        f1 = 1.1E-44F;
        ((TextView)localObject1).setVisibility(i1);
      }
      localObject1 = "calls";
      a((String)localObject1, 0);
    }
    else
    {
      localObject1 = "calls";
      localObject3 = w;
      bool1 = ((String)localObject1).equals(localObject3);
      if (!bool1)
      {
        localObject1 = w;
        if (localObject1 != null) {}
      }
      else
      {
        f();
        localObject1 = n;
        if (localObject1 != null)
        {
          int i2 = r;
          at.a((View)localObject1, i2);
        }
        localObject1 = f;
        localObject3 = v;
        f2 = scale;
        a((View)localObject1, false, f2);
        localObject1 = n;
        a((View)localObject1, bool2, f1);
        localObject1 = j;
        if (localObject1 != null) {
          ((TextView)localObject1).setVisibility(0);
        }
      }
    }
    localObject1 = paramView.getTag();
    w = localObject1;
    localObject1 = e;
    if (localObject1 != null)
    {
      paramView = (String)paramView.getTag();
      bool1 = y;
      if (!bool1)
      {
        localObject1 = "banking";
        bool1 = paramView.equals(localObject1);
        if (!bool1)
        {
          localObject1 = "payments";
          bool1 = paramView.equals(localObject1);
          if (!bool1) {}
        }
        else
        {
          return;
        }
      }
      bool1 = z;
      if (!bool1)
      {
        bool1 = d;
        if (!bool1)
        {
          localObject1 = "blocking";
          bool1 = paramView.equals(localObject1);
          if (!bool1)
          {
            localObject1 = "premium";
            bool1 = paramView.equals(localObject1);
            if (!bool1) {}
          }
          else
          {
            return;
          }
        }
      }
      localObject1 = e;
      ((BottomBar.a)localObject1).b(paramView);
    }
  }
  
  private void a(View paramView, boolean paramBoolean, float paramFloat)
  {
    if (paramBoolean)
    {
      f1 = 0.0F;
      localObject = null;
      paramView.setVisibility(0);
    }
    float f1 = 0.0F;
    Object localObject = null;
    float f2;
    if (paramBoolean)
    {
      f2 = 0.0F;
      localw6x6Egr20wTyyjKrbn4mZe1zx0w = null;
    }
    else
    {
      f2 = 1.0F;
    }
    paramView.setAlpha(f2);
    if (paramBoolean)
    {
      f2 = 0.0F;
      localw6x6Egr20wTyyjKrbn4mZe1zx0w = null;
    }
    else
    {
      f2 = paramFloat;
    }
    paramView.setScaleX(f2);
    if (!paramBoolean) {
      f1 = paramFloat;
    }
    paramView.setScaleY(f1);
    localObject = new float[2];
    Object tmp90_88 = localObject;
    tmp90_88[0] = 0.0F;
    tmp90_88[1] = 1.0F;
    localObject = ValueAnimator.ofFloat((float[])localObject);
    long l1 = q;
    localObject = ((ValueAnimator)localObject).setDuration(l1);
    -..Lambda.BottomBar.w6x6Egr20wTyyjKrbn4mZe1zx0w localw6x6Egr20wTyyjKrbn4mZe1zx0w = new com/truecaller/ui/view/-$$Lambda$BottomBar$w6x6Egr20wTyyjKrbn4mZe1zx0w;
    localw6x6Egr20wTyyjKrbn4mZe1zx0w.<init>(paramBoolean, paramView, paramFloat);
    ((ValueAnimator)localObject).addUpdateListener(localw6x6Egr20wTyyjKrbn4mZe1zx0w);
    BottomBar.1 local1 = new com/truecaller/ui/view/BottomBar$1;
    local1.<init>(this, paramView, paramBoolean);
    ((ValueAnimator)localObject).addListener(local1);
    ((ValueAnimator)localObject).start();
  }
  
  private void a(ImageView paramImageView, TextView paramTextView)
  {
    Object localObject = x;
    if (localObject == paramImageView)
    {
      b((View)localObject);
      return;
    }
    boolean bool1 = false;
    if (localObject != null) {
      ((ImageView)localObject).setSelected(false);
    }
    x = paramImageView;
    localObject = x;
    boolean bool2 = true;
    ((ImageView)localObject).setSelected(bool2);
    localObject = x;
    a((View)localObject);
    if (paramTextView != null)
    {
      localObject = g;
      if (localObject != null) {
        ((TextView)localObject).setSelected(false);
      }
      g = paramTextView;
      paramTextView = g;
      paramTextView.setSelected(bool2);
    }
    paramTextView = n;
    if (paramImageView == paramTextView)
    {
      paramImageView = v;
      paramTextView = BottomBar.DialpadState.DIALPAD_DOWN;
      if (paramImageView == paramTextView) {
        bool1 = true;
      }
      setShadowVisibility(bool1);
      return;
    }
    boolean bool3 = z;
    if (!bool3)
    {
      bool3 = d;
      if (!bool3) {}
    }
    else
    {
      paramTextView = p;
      if (paramImageView == paramTextView)
      {
        setShadowVisibility(false);
        return;
      }
    }
    setShadowVisibility(bool2);
  }
  
  private ImageView b(int paramInt1, int paramInt2)
  {
    ImageView localImageView = (ImageView)findViewById(paramInt1);
    String str = getResources().getString(paramInt2);
    localImageView.setContentDescription(str);
    return localImageView;
  }
  
  private void b()
  {
    e();
    d();
    c();
  }
  
  private void b(View paramView)
  {
    Object localObject1 = "calls";
    Object localObject2 = paramView.getTag();
    boolean bool = ((String)localObject1).equals(localObject2);
    if (bool)
    {
      localObject1 = n;
      if (localObject1 != null)
      {
        localObject2 = null;
        at.a((View)localObject1, 0);
      }
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      paramView = (String)paramView.getTag();
      ((BottomBar.a)localObject1).c(paramView);
    }
  }
  
  private void c()
  {
    boolean bool1 = y;
    if (bool1) {
      i1 = 2131887263;
    } else {
      i1 = 2131887265;
    }
    ImageView localImageView = b(2131363152, i1);
    b = localImageView;
    int i1 = 2131363073;
    boolean bool2 = y;
    if (bool2)
    {
      bool2 = d;
      if (!bool2)
      {
        i2 = 2131887271;
        break label67;
      }
    }
    int i2 = 2131887272;
    label67:
    localImageView = b(i1, i2);
    c = localImageView;
  }
  
  private void d()
  {
    boolean bool1 = y;
    if (bool1) {
      localObject = "banking";
    } else {
      localObject = "blocking";
    }
    boolean bool2 = y;
    int i2;
    if (bool2) {
      i2 = 2131234584;
    } else {
      i2 = 2131234585;
    }
    boolean bool4 = y;
    int i3;
    if (bool4) {
      i3 = 2131887263;
    } else {
      i3 = 2131887265;
    }
    int i5 = 2131363151;
    Object localObject = a(i5, (String)localObject, i2, i3);
    o = ((ImageView)localObject);
    int i1 = 2131363072;
    boolean bool3 = y;
    if (bool3)
    {
      bool3 = d;
      if (!bool3)
      {
        str = "payments";
        break label120;
      }
    }
    String str = "premium";
    label120:
    boolean bool5 = y;
    if (bool5)
    {
      bool5 = d;
      if (!bool5)
      {
        i4 = 2131234590;
        break label155;
      }
    }
    int i4 = 2131234591;
    label155:
    boolean bool6 = y;
    if (bool6)
    {
      bool6 = d;
      if (!bool6)
      {
        i6 = 2131887271;
        break label190;
      }
    }
    int i6 = 2131887272;
    label190:
    localObject = a(i1, str, i4, i6);
    p = ((ImageView)localObject);
  }
  
  private void e()
  {
    boolean bool1 = y;
    if (bool1) {
      i1 = 2131887263;
    } else {
      i1 = 2131887265;
    }
    TextView localTextView = a(2131363153, i1);
    k = localTextView;
    int i1 = 2131363074;
    boolean bool2 = y;
    if (bool2)
    {
      bool2 = d;
      if (!bool2)
      {
        i2 = 2131887271;
        break label67;
      }
    }
    int i2 = 2131887272;
    label67:
    localTextView = a(i1, i2);
    l = localTextView;
  }
  
  private void f()
  {
    f.clearAnimation();
    n.clearAnimation();
  }
  
  private void setFabScale(Float paramFloat)
  {
    FloatingActionButton localFloatingActionButton = f;
    float f1 = paramFloat.floatValue();
    localFloatingActionButton.setScaleX(f1);
    localFloatingActionButton = f;
    float f2 = paramFloat.floatValue();
    localFloatingActionButton.setScaleY(f2);
  }
  
  public final void a()
  {
    boolean bool1 = y;
    if (!bool1)
    {
      bool1 = z;
      if (!bool1) {}
    }
    else
    {
      bool1 = d;
      boolean bool2 = y;
      if (bool2)
      {
        localObject1 = TrueApp.y().f().ac();
        bool2 = ((com.truecaller.featuretoggles.b)localObject1).a();
        if (bool2)
        {
          localObject1 = TrueApp.y().a().ai();
          bool2 = ((c)localObject1).d();
          if (!bool2)
          {
            localObject1 = Truepay.getApplicationComponent();
            if (localObject1 != null)
            {
              localObject1 = Truepay.getApplicationComponent().h().c();
              if (localObject1 == null)
              {
                bool2 = true;
                break label109;
              }
            }
          }
        }
      }
      bool2 = false;
      Object localObject1 = null;
      label109:
      d = bool2;
      b();
      bool2 = d;
      if (bool1 != bool2)
      {
        e.j();
        Object localObject2 = x;
        localObject1 = p;
        if (localObject2 == localObject1)
        {
          bool1 = false;
          x = null;
          localObject2 = l;
          a((ImageView)localObject1, (TextView)localObject2);
        }
      }
    }
  }
  
  public final void a(String paramString)
  {
    int i1 = paramString.hashCode();
    Object localObject;
    boolean bool2;
    boolean bool4;
    switch (i1)
    {
    default: 
      break;
    case 1382682413: 
      localObject = "payments";
      boolean bool1 = paramString.equals(localObject);
      if (bool1) {
        int i2 = 5;
      }
      break;
    case 94425557: 
      localObject = "calls";
      bool2 = paramString.equals(localObject);
      if (bool2)
      {
        bool2 = false;
        paramString = null;
      }
      break;
    case -318452137: 
      localObject = "premium";
      bool2 = paramString.equals(localObject);
      if (bool2) {
        int i3 = 6;
      }
      break;
    case -337045466: 
      localObject = "banking";
      boolean bool3 = paramString.equals(localObject);
      if (bool3) {
        int i4 = 3;
      }
      break;
    case -462094004: 
      localObject = "messages";
      bool4 = paramString.equals(localObject);
      if (bool4) {
        bool4 = true;
      }
      break;
    case -567451565: 
      localObject = "contacts";
      bool4 = paramString.equals(localObject);
      if (bool4) {
        int i5 = 2;
      }
      break;
    case -664572875: 
      localObject = "blocking";
      boolean bool5 = paramString.equals(localObject);
      if (bool5) {
        i6 = 4;
      }
      break;
    }
    int i6 = -1;
    switch (i6)
    {
    default: 
      return;
    case 5: 
    case 6: 
      paramString = p;
      localObject = l;
      break;
    case 3: 
    case 4: 
      paramString = o;
      localObject = k;
      break;
    case 2: 
      paramString = m;
      localObject = i;
      break;
    case 1: 
      paramString = a;
      localObject = h;
      break;
    case 0: 
      paramString = n;
      localObject = j;
    }
    a(paramString, (TextView)localObject);
  }
  
  public final void a(String paramString, int paramInt)
  {
    int i1 = paramString.hashCode();
    int i2 = -462094004;
    Object localObject;
    boolean bool;
    if (i1 != i2)
    {
      i2 = 94425557;
      if (i1 == i2)
      {
        localObject = "calls";
        bool = paramString.equals(localObject);
        if (bool)
        {
          bool = false;
          paramString = null;
          break label83;
        }
      }
    }
    else
    {
      localObject = "messages";
      bool = paramString.equals(localObject);
      if (bool)
      {
        bool = true;
        break label83;
      }
    }
    int i3 = -1;
    switch (i3)
    {
    default: 
      break;
    case 1: 
      paramString = getContext();
      localObject = a.getDrawable();
      at.a(paramString, (Drawable)localObject, paramInt);
      a.refreshDrawableState();
      paramString = a;
      paramString.invalidate();
      break;
    case 0: 
      label83:
      paramString = getContext();
      localObject = n.getDrawable();
      at.a(paramString, (Drawable)localObject, paramInt);
      n.refreshDrawableState();
      n.invalidate();
      return;
    }
  }
  
  public final ImageView b(String paramString)
  {
    int i1 = paramString.hashCode();
    String str;
    boolean bool4;
    switch (i1)
    {
    default: 
      break;
    case 1382682413: 
      str = "payments";
      boolean bool1 = paramString.equals(str);
      if (bool1) {
        int i2 = 4;
      }
      break;
    case -318452137: 
      str = "premium";
      boolean bool2 = paramString.equals(str);
      if (bool2) {
        int i3 = 5;
      }
      break;
    case -337045466: 
      str = "banking";
      boolean bool3 = paramString.equals(str);
      if (bool3) {
        int i4 = 2;
      }
      break;
    case -462094004: 
      str = "messages";
      bool4 = paramString.equals(str);
      if (bool4)
      {
        bool4 = false;
        paramString = null;
      }
      break;
    case -567451565: 
      str = "contacts";
      bool4 = paramString.equals(str);
      if (bool4) {
        bool4 = true;
      }
      break;
    case -664572875: 
      str = "blocking";
      bool4 = paramString.equals(str);
      if (bool4) {
        i5 = 3;
      }
      break;
    }
    int i5 = -1;
    switch (i5)
    {
    default: 
      return n;
    case 4: 
    case 5: 
      return p;
    case 2: 
    case 3: 
      return o;
    case 1: 
      return m;
    }
    return a;
  }
  
  public FloatingActionButton getFab()
  {
    return f;
  }
  
  public void onClick(View paramView)
  {
    int i1 = paramView.getId();
    boolean bool;
    switch (i1)
    {
    default: 
      break;
    case 2131363762: 
      a("messages");
      return;
    case 2131363151: 
      bool = y;
      if (bool)
      {
        a("banking");
        return;
      }
      bool = z;
      if (bool)
      {
        a("blocking");
        return;
      }
      break;
    case 2131363072: 
      bool = y;
      if (bool)
      {
        bool = d;
        if (bool) {
          paramView = "premium";
        } else {
          paramView = "payments";
        }
        a(paramView);
        return;
      }
      bool = z;
      if (bool)
      {
        paramView = "premium";
        a(paramView);
      }
      break;
    case 2131363019: 
      a("calls");
      paramView = e;
      if (paramView != null)
      {
        paramView.i();
        return;
      }
      break;
    case 2131362559: 
      a("contacts");
      return;
    case 2131362415: 
      a("calls");
      return;
    }
  }
  
  public void setDialpadState(BottomBar.DialpadState paramDialpadState)
  {
    Object localObject1 = v;
    if (paramDialpadState != localObject1)
    {
      float f1 = scale;
      Object localObject2 = v;
      float f2 = scale;
      int i1 = 1;
      int i2 = 2;
      boolean bool = f1 < f2;
      Object localObject3;
      if (bool)
      {
        f1 = scale;
        localObject2 = new float[i2];
        localObject3 = f;
        float f3 = ((FloatingActionButton)localObject3).getScaleX();
        localObject2[0] = f3;
        localObject2[i1] = f1;
        localObject1 = ValueAnimator.ofFloat((float[])localObject2);
        i4 = q;
        long l1 = i4;
        localObject1 = ((ValueAnimator)localObject1).setDuration(l1);
        localObject2 = new android/view/animation/AccelerateDecelerateInterpolator;
        ((AccelerateDecelerateInterpolator)localObject2).<init>();
        ((ValueAnimator)localObject1).setInterpolator((TimeInterpolator)localObject2);
        localObject2 = new com/truecaller/ui/view/-$$Lambda$BottomBar$jqesarmdRk0QrUaGSStbMCJ6PVI;
        ((-..Lambda.BottomBar.jqesarmdRk0QrUaGSStbMCJ6PVI)localObject2).<init>(this);
        ((ValueAnimator)localObject1).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject2);
        ((ValueAnimator)localObject1).start();
      }
      int i3 = colorAttr;
      localObject2 = v;
      int i4 = colorAttr;
      if (i3 != i4)
      {
        i3 = colorAttr;
        i4 = f.getBackgroundTintList().getDefaultColor();
        i3 = s.get(i3);
        localObject3 = new android/animation/ArgbEvaluator;
        ((ArgbEvaluator)localObject3).<init>();
        Object[] arrayOfObject = new Object[i2];
        localObject2 = Integer.valueOf(i4);
        arrayOfObject[0] = localObject2;
        localObject1 = Integer.valueOf(i3);
        arrayOfObject[i1] = localObject1;
        localObject1 = ValueAnimator.ofObject((TypeEvaluator)localObject3, arrayOfObject);
        localObject2 = new com/truecaller/ui/view/-$$Lambda$BottomBar$9N5-DbszZso_zGrP-QrUf7XelF0;
        ((-..Lambda.BottomBar.9N5-DbszZso_zGrP-QrUf7XelF0)localObject2).<init>(this);
        ((ValueAnimator)localObject1).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject2);
        ((ValueAnimator)localObject1).start();
      }
      i3 = drawable;
      localObject2 = v;
      i4 = drawable;
      if (i3 != i4)
      {
        localObject1 = f;
        localObject2 = t;
        i1 = drawable;
        localObject2 = (Drawable)((SparseArray)localObject2).get(i1);
        ((FloatingActionButton)localObject1).setImageDrawable((Drawable)localObject2);
      }
      v = paramDialpadState;
    }
  }
  
  public void setShadowVisibility(boolean paramBoolean)
  {
    View localView = u;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localView.setVisibility(paramBoolean);
  }
  
  public void setup(BottomBar.a parama)
  {
    e = parama;
    int i1 = 2131887268;
    Object localObject1 = a(2131363763, i1);
    h = ((TextView)localObject1);
    int i2 = 2131887267;
    TextView localTextView = a(2131362560, i2);
    i = localTextView;
    int i3 = 2131887266;
    Object localObject2 = a(2131362416, i3);
    j = ((TextView)localObject2);
    localObject2 = "messages";
    int i4 = 2131234589;
    parama = a(2131363762, (String)localObject2, i4, i1);
    a = parama;
    int i5 = 2131234587;
    parama = a(2131362559, "contacts", i5, i2);
    m = parama;
    i2 = 2131362415;
    int i6 = 2131234586;
    parama = a(i2, "calls", i6, i3);
    n = parama;
    a();
    boolean bool = A;
    if (bool)
    {
      a.setImageResource(2131234588);
      parama = h;
      i2 = 2131887269;
      parama.setText(i2);
      parama = getContext();
      localObject1 = a;
      i3 = 2130968688;
      com.truecaller.utils.ui.b.a(parama, (ImageView)localObject1, i3);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.BottomBar
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */