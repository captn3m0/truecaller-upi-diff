package com.truecaller.ui.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff.Mode;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import com.truecaller.R.styleable;

public class TintedImageView
  extends AppCompatImageView
{
  private ColorStateList a;
  
  public TintedImageView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    int[] arrayOfInt = R.styleable.TintedImageView;
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, arrayOfInt, 0, 0);
    paramAttributeSet = paramContext.getColorStateList(0);
    a = paramAttributeSet;
    paramContext.recycle();
  }
  
  private void a()
  {
    ColorStateList localColorStateList = a;
    if (localColorStateList == null)
    {
      clearColorFilter();
      return;
    }
    Object localObject = getDrawableState();
    int i = localColorStateList.getColorForState((int[])localObject, 0);
    localObject = PorterDuff.Mode.SRC_IN;
    setColorFilter(i, (PorterDuff.Mode)localObject);
  }
  
  public void drawableStateChanged()
  {
    super.drawableStateChanged();
    a();
  }
  
  public void setTint(int paramInt)
  {
    ColorStateList localColorStateList = new android/content/res/ColorStateList;
    int i = 1;
    int[][] arrayOfInt = new int[i][];
    int[] arrayOfInt1 = new int[0];
    arrayOfInt[0] = arrayOfInt1;
    int[] arrayOfInt2 = new int[i];
    arrayOfInt2[0] = paramInt;
    localColorStateList.<init>(arrayOfInt, arrayOfInt2);
    setTint(localColorStateList);
  }
  
  public void setTint(ColorStateList paramColorStateList)
  {
    a = paramColorStateList;
    a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.TintedImageView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */