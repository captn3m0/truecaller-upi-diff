package com.truecaller.ui.view;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.b;
import android.view.animation.AnticipateInterpolator;
import com.truecaller.util.at;

public final class h
  extends Drawable
{
  public Animator a;
  private final int b;
  private final int c;
  private final Drawable d;
  private final Drawable[] e;
  private final Drawable[] f;
  private final Drawable[] g;
  private Drawable[] h;
  private final Rect i;
  private boolean j;
  
  public h(Context paramContext)
  {
    Rect localRect = new android/graphics/Rect;
    localRect.<init>();
    i = localRect;
    int k = at.a(paramContext, 285.0F);
    b = k;
    k = at.a(paramContext, 209.0F);
    c = k;
    k = at.a(paramContext, 38.0F);
    int m = at.a(paramContext, 62.0F);
    int n = at.a(paramContext, 105.0F);
    int i1 = at.a(paramContext, 149.0F);
    Drawable localDrawable1 = b.a(paramContext, 2131234792);
    d = localDrawable1;
    localDrawable1 = d;
    int i2 = localDrawable1.getIntrinsicWidth();
    Drawable localDrawable2 = d;
    int i3 = localDrawable2.getIntrinsicHeight();
    int i4 = 0;
    localDrawable1.setBounds(0, 0, i2, i3);
    int i5 = 2131234344;
    localDrawable1 = b.a(paramContext, i5);
    i2 = b;
    i3 = k + m;
    int i6 = k + i2;
    localDrawable1.setBounds(m, i2, i3, i6);
    i2 = 2131234348;
    Drawable localDrawable3 = b.a(paramContext, i2);
    i6 = b;
    int i7 = k + n;
    int i8 = k + i6;
    localDrawable3.setBounds(n, i6, i7, i8);
    i6 = 2131234346;
    Drawable localDrawable4 = b.a(paramContext, i6);
    i8 = b;
    int i9 = k + i1;
    int i10 = k + i8;
    localDrawable4.setBounds(i1, i8, i9, i10);
    i8 = 2131234345;
    Drawable localDrawable5 = b.a(paramContext, i8);
    i10 = b;
    int i11 = k + i10;
    localDrawable5.setBounds(m, i10, i3, i11);
    m = 2131234349;
    Drawable localDrawable6 = b.a(paramContext, m);
    i3 = b;
    i10 = k + i3;
    localDrawable6.setBounds(n, i3, i7, i10);
    paramContext = b.a(paramContext, 2131234347);
    n = b;
    k += n;
    paramContext.setBounds(i1, n, i9, k);
    k = 3;
    Drawable[] arrayOfDrawable = new Drawable[k];
    arrayOfDrawable[0] = localDrawable1;
    i1 = 1;
    arrayOfDrawable[i1] = localDrawable3;
    i3 = 2;
    arrayOfDrawable[i3] = localDrawable4;
    e = arrayOfDrawable;
    arrayOfDrawable = new Drawable[k];
    arrayOfDrawable[0] = paramContext;
    arrayOfDrawable[i1] = localDrawable6;
    arrayOfDrawable[i3] = localDrawable5;
    f = arrayOfDrawable;
    n = 6;
    arrayOfDrawable = new Drawable[n];
    arrayOfDrawable[0] = localDrawable1;
    arrayOfDrawable[i1] = localDrawable3;
    arrayOfDrawable[i3] = localDrawable4;
    arrayOfDrawable[k] = localDrawable5;
    arrayOfDrawable[4] = localDrawable6;
    arrayOfDrawable[5] = paramContext;
    g = arrayOfDrawable;
    paramContext = e;
    h = paramContext;
    paramContext = g;
    k = paramContext.length;
    while (i4 < k)
    {
      localDrawable6 = paramContext[i4];
      n = 255;
      localDrawable6.setAlpha(n);
      i4 += 1;
    }
    a();
  }
  
  private void a()
  {
    h localh = this;
    int k = e.length;
    Object localObject1 = new Animator[k];
    h.3 local3 = null;
    int m = 0;
    AnimatorSet localAnimatorSet = null;
    long l1;
    long l2;
    long l3;
    int i1;
    int i2;
    long l4;
    Object localObject4;
    for (;;)
    {
      localObject2 = e;
      int n = localObject2.length;
      l1 = 800L;
      l2 = 2000L;
      l3 = 500L;
      i1 = 2;
      i2 = 1;
      if (m >= n) {
        break;
      }
      localObject2 = localObject2[m];
      l4 = m * l3 + l2;
      localObject3 = new int[i1];
      int i3 = b;
      localObject3[0] = i3;
      i3 = c;
      localObject3[i2] = i3;
      localObject3 = ValueAnimator.ofInt((int[])localObject3);
      ((ValueAnimator)localObject3).setDuration(l1);
      ((ValueAnimator)localObject3).setStartDelay(l4);
      localObject4 = new android/view/animation/AnticipateInterpolator;
      ((AnticipateInterpolator)localObject4).<init>();
      ((ValueAnimator)localObject3).setInterpolator((TimeInterpolator)localObject4);
      localObject4 = new com/truecaller/ui/view/-$$Lambda$h$aCKYed5SmSJ0-4CI9RbBZZAeYnQ;
      ((-..Lambda.h.aCKYed5SmSJ0-4CI9RbBZZAeYnQ)localObject4).<init>(localh, (Drawable)localObject2);
      ((ValueAnimator)localObject3).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject4);
      localObject1[m] = localObject3;
      m += 1;
    }
    localAnimatorSet = new android/animation/AnimatorSet;
    localAnimatorSet.<init>();
    localAnimatorSet.playTogether((Animator[])localObject1);
    k = f.length;
    localObject1 = new Animator[k];
    int i4 = 0;
    Object localObject2 = null;
    for (;;)
    {
      localObject3 = f;
      int i5 = localObject3.length;
      if (i4 >= i5) {
        break;
      }
      localObject3 = localObject3[i4];
      l4 = 1000L;
      l2 = i4 * l3 + l4;
      Object localObject5 = new int[i1];
      int i6 = c;
      localObject5[0] = i6;
      i6 = b;
      localObject5[i2] = i6;
      localObject5 = ValueAnimator.ofInt((int[])localObject5);
      ((ValueAnimator)localObject5).setDuration(l1);
      ((ValueAnimator)localObject5).setStartDelay(l2);
      -..Lambda.h.3al0cu9vajrvKl1pZO9fzTgqRNQ local3al0cu9vajrvKl1pZO9fzTgqRNQ = new com/truecaller/ui/view/-$$Lambda$h$3al0cu9vajrvKl1pZO9fzTgqRNQ;
      local3al0cu9vajrvKl1pZO9fzTgqRNQ.<init>(localh, (Drawable)localObject3);
      ((ValueAnimator)localObject5).addUpdateListener(local3al0cu9vajrvKl1pZO9fzTgqRNQ);
      localObject1[i4] = localObject5;
      i4 += 1;
      l2 = 2000L;
    }
    localObject2 = new android/animation/AnimatorSet;
    ((AnimatorSet)localObject2).<init>();
    ((AnimatorSet)localObject2).playTogether((Animator[])localObject1);
    localObject1 = new com/truecaller/ui/view/h$1;
    ((h.1)localObject1).<init>(localh);
    ((AnimatorSet)localObject2).addListener((Animator.AnimatorListener)localObject1);
    localObject1 = f;
    Object localObject3 = e;
    int i7 = localObject1.length;
    int i8 = localObject3.length;
    if (i7 == i8)
    {
      localObject4 = new float[i1];
      Object tmp417_415 = localObject4;
      tmp417_415[0] = 0.0F;
      tmp417_415[1] = 1.0F;
      localObject4 = ValueAnimator.ofFloat((float[])localObject4);
      ((ValueAnimator)localObject4).setDuration(l3);
      -..Lambda.h.5IIHRK4n9QYak-z24smfqaDoGUo local5IIHRK4n9QYak-z24smfqaDoGUo = new com/truecaller/ui/view/-$$Lambda$h$5IIHRK4n9QYak-z24smfqaDoGUo;
      local5IIHRK4n9QYak-z24smfqaDoGUo.<init>(localh, (Drawable[])localObject1, (Drawable[])localObject3);
      ((ValueAnimator)localObject4).addUpdateListener(local5IIHRK4n9QYak-z24smfqaDoGUo);
      ((Animator)localObject4).setStartDelay(2000L);
      localObject1 = new com/truecaller/ui/view/h$2;
      ((h.2)localObject1).<init>(localh);
      ((Animator)localObject4).addListener((Animator.AnimatorListener)localObject1);
      localObject1 = new android/animation/AnimatorSet;
      ((AnimatorSet)localObject1).<init>();
      localObject3 = new Animator[3];
      localObject3[0] = localAnimatorSet;
      localObject3[i2] = localObject2;
      localObject3[i1] = localObject4;
      ((AnimatorSet)localObject1).playSequentially((Animator[])localObject3);
      local3 = new com/truecaller/ui/view/h$3;
      local3.<init>(localh);
      ((AnimatorSet)localObject1).addListener(local3);
      a = ((Animator)localObject1);
      return;
    }
    localObject1 = new java/lang/IllegalArgumentException;
    ((IllegalArgumentException)localObject1).<init>("Arrays must have equal length");
    throw ((Throwable)localObject1);
  }
  
  private void a(Drawable paramDrawable, int paramInt)
  {
    Rect localRect1 = i;
    paramDrawable.copyBounds(localRect1);
    localRect1 = i;
    int k = left;
    localRect1.offsetTo(k, paramInt);
    Rect localRect2 = i;
    paramDrawable.setBounds(localRect2);
  }
  
  public final void draw(Canvas paramCanvas)
  {
    d.draw(paramCanvas);
    Drawable[] arrayOfDrawable = h;
    int k = arrayOfDrawable.length;
    int m = 0;
    while (m < k)
    {
      Drawable localDrawable = arrayOfDrawable[m];
      localDrawable.draw(paramCanvas);
      m += 1;
    }
  }
  
  public final int getIntrinsicHeight()
  {
    Drawable localDrawable = d;
    if (localDrawable != null) {
      return localDrawable.getIntrinsicHeight();
    }
    return 0;
  }
  
  public final int getIntrinsicWidth()
  {
    Drawable localDrawable = d;
    if (localDrawable != null) {
      return localDrawable.getIntrinsicWidth();
    }
    return 0;
  }
  
  public final int getOpacity()
  {
    return -3;
  }
  
  public final void setAlpha(int paramInt)
  {
    d.setAlpha(paramInt);
    Drawable[] arrayOfDrawable = g;
    int k = arrayOfDrawable.length;
    int m = 0;
    while (m < k)
    {
      Drawable localDrawable = arrayOfDrawable[m];
      localDrawable.setAlpha(paramInt);
      m += 1;
    }
  }
  
  public final void setColorFilter(ColorFilter paramColorFilter)
  {
    d.setColorFilter(paramColorFilter);
    Drawable[] arrayOfDrawable = g;
    int k = arrayOfDrawable.length;
    int m = 0;
    while (m < k)
    {
      Drawable localDrawable = arrayOfDrawable[m];
      localDrawable.setColorFilter(paramColorFilter);
      m += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */