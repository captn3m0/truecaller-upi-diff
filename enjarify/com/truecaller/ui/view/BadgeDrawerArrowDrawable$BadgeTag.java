package com.truecaller.ui.view;

public enum BadgeDrawerArrowDrawable$BadgeTag
{
  static
  {
    Object localObject = new com/truecaller/ui/view/BadgeDrawerArrowDrawable$BadgeTag;
    ((BadgeTag)localObject).<init>("ERROR", 0);
    ERROR = (BadgeTag)localObject;
    localObject = new com/truecaller/ui/view/BadgeDrawerArrowDrawable$BadgeTag;
    int i = 1;
    ((BadgeTag)localObject).<init>("WARNING", i);
    WARNING = (BadgeTag)localObject;
    localObject = new com/truecaller/ui/view/BadgeDrawerArrowDrawable$BadgeTag;
    int j = 2;
    ((BadgeTag)localObject).<init>("NONE", j);
    NONE = (BadgeTag)localObject;
    localObject = new BadgeTag[3];
    BadgeTag localBadgeTag = ERROR;
    localObject[0] = localBadgeTag;
    localBadgeTag = WARNING;
    localObject[i] = localBadgeTag;
    localBadgeTag = NONE;
    localObject[j] = localBadgeTag;
    $VALUES = (BadgeTag[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.BadgeDrawerArrowDrawable.BadgeTag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */