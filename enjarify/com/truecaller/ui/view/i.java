package com.truecaller.ui.view;

import android.animation.Animator.AnimatorListener;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.Shape;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.R.styleable;
import com.truecaller.android.truemoji.Emoji;
import com.truecaller.android.truemoji.f;
import com.truecaller.utils.ui.b;
import java.util.Iterator;
import java.util.List;

public final class i
  extends FrameLayout
{
  private LinearLayout a;
  private g b;
  private final AttributeSet c = null;
  private final int d = 0;
  private List e;
  private String f;
  private String g;
  
  public i(Context paramContext, List paramList, String paramString1, String paramString2)
  {
    super(paramContext, null, 0);
    e = paramList;
    f = paramString1;
    g = paramString2;
    paramContext = c;
    if (paramContext != null)
    {
      paramContext = getContext();
      paramList = c;
      paramString1 = R.styleable.ReactionPicker;
      int i = d;
      paramContext = paramContext.obtainStyledAttributes(paramList, paramString1, i, 0);
      paramList = "context.obtainStyledAttr…ctionPicker, defStyle, 0)";
      k.a(paramContext, paramList);
      try
      {
        paramList = paramContext.getString(0);
        if (paramList == null) {
          paramList = "👍,🤣,😮,😍,😠,😢,👎";
        }
        paramList = (CharSequence)paramList;
        paramString1 = ",";
        paramString1 = new String[] { paramString1 };
        i = 6;
        paramList = m.c(paramList, paramString1, false, i);
        e = paramList;
        int j = 1;
        paramList = paramContext.getString(j);
        f = paramList;
        j = 2;
        paramList = paramContext.getString(j);
        g = paramList;
      }
      finally
      {
        paramContext.recycle();
      }
    }
    a();
  }
  
  private final void a()
  {
    Object localObject1 = LayoutInflater.from(getContext());
    Object localObject2 = this;
    localObject2 = (ViewGroup)this;
    boolean bool1 = true;
    ((LayoutInflater)localObject1).inflate(2131559080, (ViewGroup)localObject2, bool1);
    int i = 2131234359;
    float f1 = 1.8084882E38F;
    setBackgroundResource(i);
    localObject1 = new android/widget/FrameLayout$LayoutParams;
    int j = -2;
    ((FrameLayout.LayoutParams)localObject1).<init>(j, j);
    localObject1 = (ViewGroup.LayoutParams)localObject1;
    setLayoutParams((ViewGroup.LayoutParams)localObject1);
    localObject1 = (CharSequence)g;
    j = 0;
    if (localObject1 != null)
    {
      i = ((CharSequence)localObject1).length();
      if (i != 0)
      {
        bool1 = false;
        localCharSequence = null;
      }
    }
    if (!bool1)
    {
      i = 2131364882;
      f1 = 1.8349614E38F;
      localObject1 = (TextView)findViewById(i);
      localCharSequence = (CharSequence)g;
      ((TextView)localObject1).setText(localCharSequence);
      ((TextView)localObject1).setVisibility(0);
    }
    i = 2131362956;
    f1 = 1.8345707E38F;
    localObject1 = findViewById(i);
    k.a(localObject1, "findViewById(R.id.emojis_holder)");
    localObject1 = (LinearLayout)localObject1;
    a = ((LinearLayout)localObject1);
    localObject1 = e;
    bool1 = false;
    CharSequence localCharSequence = null;
    if (localObject1 != null)
    {
      localObject1 = ((Iterable)localObject1).iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break label452;
        }
        String str = (String)((Iterator)localObject1).next();
        Object localObject3 = f.b;
        localObject3 = f.c();
        Object localObject4 = str;
        localObject4 = (CharSequence)str;
        localObject3 = ((f)localObject3).b((CharSequence)localObject4);
        int k;
        if (localObject3 != null)
        {
          k = ((Emoji)localObject3).a();
        }
        else
        {
          k = 0;
          localObject3 = null;
        }
        localObject4 = getEmojiBgDrawable();
        if (k != 0)
        {
          LinearLayout localLinearLayout = a;
          if (localLinearLayout == null)
          {
            localObject5 = "emojiContainer";
            k.a((String)localObject5);
          }
          Object localObject5 = LayoutInflater.from(getContext());
          int m = 2131559078;
          localObject5 = ((LayoutInflater)localObject5).inflate(m, (ViewGroup)localObject2, false);
          if (localObject5 == null) {
            break;
          }
          localObject5 = (ImageView)localObject5;
          ((ImageView)localObject5).setImageResource(k);
          ((ImageView)localObject5).setBackground((Drawable)localObject4);
          boolean bool3 = k.a(f, str);
          ((ImageView)localObject5).setSelected(bool3);
          Object localObject6 = new com/truecaller/ui/view/i$a;
          ((i.a)localObject6).<init>(this, k, (Drawable)localObject4, str);
          localObject6 = (View.OnClickListener)localObject6;
          ((ImageView)localObject5).setOnClickListener((View.OnClickListener)localObject6);
          ((ImageView)localObject5).setTag(str);
          ((ImageView)localObject5).setScaleX(0.0F);
          ((ImageView)localObject5).setScaleY(0.0F);
          localObject5 = (View)localObject5;
          localLinearLayout.addView((View)localObject5);
        }
      }
      localObject1 = new c/u;
      ((u)localObject1).<init>("null cannot be cast to non-null type android.widget.ImageView");
      throw ((Throwable)localObject1);
    }
    label452:
    setAlpha(0.0F);
    localObject1 = getContext();
    k.a(localObject1, "context");
    f1 = ((Context)localObject1).getResources().getDimensionPixelSize(2131166123) * 2.0F;
    setTranslationY(f1);
  }
  
  private final Drawable getEmojiBgDrawable()
  {
    StateListDrawable localStateListDrawable = new android/graphics/drawable/StateListDrawable;
    localStateListDrawable.<init>();
    int i = b.a(getContext(), 2130969358);
    int j = b.a(getContext(), 2130969359);
    int k = 1;
    Object localObject1 = new int[k];
    localObject1[0] = 16842919;
    Object localObject2 = new android/graphics/drawable/ShapeDrawable;
    Object localObject3 = new android/graphics/drawable/shapes/OvalShape;
    ((OvalShape)localObject3).<init>();
    localObject3 = (Shape)localObject3;
    ((ShapeDrawable)localObject2).<init>((Shape)localObject3);
    localObject3 = ((ShapeDrawable)localObject2).getPaint();
    k.a(localObject3, "paint");
    ((Paint)localObject3).setColor(i);
    localObject2 = (Drawable)localObject2;
    localStateListDrawable.addState((int[])localObject1, (Drawable)localObject2);
    int[] arrayOfInt = new int[k];
    arrayOfInt[0] = 16842913;
    Object localObject4 = new android/graphics/drawable/ShapeDrawable;
    localObject1 = new android/graphics/drawable/shapes/OvalShape;
    ((OvalShape)localObject1).<init>();
    localObject1 = (Shape)localObject1;
    ((ShapeDrawable)localObject4).<init>((Shape)localObject1);
    localObject1 = ((ShapeDrawable)localObject4).getPaint();
    k.a(localObject1, "paint");
    ((Paint)localObject1).setColor(j);
    localObject4 = (Drawable)localObject4;
    localStateListDrawable.addState(arrayOfInt, (Drawable)localObject4);
    return (Drawable)localStateListDrawable;
  }
  
  public final AttributeSet getAttrs()
  {
    return c;
  }
  
  public final int getDefStyle()
  {
    return d;
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    ViewPropertyAnimator localViewPropertyAnimator = animate().setDuration(200L).alpha(1.0F).translationY(0.0F);
    Object localObject = new android/view/animation/AccelerateInterpolator;
    ((AccelerateInterpolator)localObject).<init>();
    localObject = (TimeInterpolator)localObject;
    localViewPropertyAnimator = localViewPropertyAnimator.setInterpolator((TimeInterpolator)localObject);
    localObject = new com/truecaller/ui/view/i$c;
    ((i.c)localObject).<init>(this);
    localObject = (Animator.AnimatorListener)localObject;
    localViewPropertyAnimator.setListener((Animator.AnimatorListener)localObject).start();
  }
  
  protected final void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    int i = 0;
    b = null;
    LinearLayout localLinearLayout = a;
    if (localLinearLayout == null)
    {
      str1 = "emojiContainer";
      k.a(str1);
    }
    i = localLinearLayout.getChildCount();
    int j = 0;
    String str1 = null;
    while (j < i)
    {
      Object localObject = a;
      if (localObject == null)
      {
        String str2 = "emojiContainer";
        k.a(str2);
      }
      localObject = ((LinearLayout)localObject).getChildAt(j);
      ((View)localObject).clearAnimation();
      j += 1;
    }
    clearAnimation();
  }
  
  public final void setOnReactionPickListener(g paramg)
  {
    k.b(paramg, "listener");
    b = paramg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */