package com.truecaller.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.truecaller.util.at;

public class ThemeSelectorView
  extends View
{
  private final Paint a;
  private final Paint b;
  private final RectF c;
  private int d;
  
  public ThemeSelectorView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramContext = new android/graphics/Paint;
    paramContext.<init>();
    a = paramContext;
    paramContext = new android/graphics/Paint;
    paramContext.<init>();
    b = paramContext;
    paramContext = new android/graphics/RectF;
    paramContext.<init>();
    c = paramContext;
    a.setColor(-65536);
    paramContext = a;
    int i = 1;
    paramContext.setAntiAlias(i);
    paramContext = getContext();
    float f1 = 4.0F;
    int j = at.a(paramContext, f1);
    int k = at.a(getContext(), 0.0F);
    int m = at.a(getContext(), f1);
    int n = Math.max(k, m) + j;
    d = n;
    Paint localPaint = a;
    float f2 = j;
    float f3 = k;
    f1 = m;
    localPaint.setShadowLayer(f2, f3, f1, 805306368);
    paramContext = a;
    setLayerType(i, paramContext);
    b.setColor(-16776961);
    b.setAntiAlias(i);
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    int i = getWidth();
    int j = getHeight();
    int k = getPaddingLeft();
    int m = getPaddingRight();
    int n = getPaddingTop();
    int i1 = getPaddingBottom();
    m += k;
    i -= m;
    i1 += n;
    j -= i1;
    m = Math.min(i, j) / 2;
    i1 = d;
    m -= i1;
    i /= 2;
    k += i;
    j /= 2;
    n += j;
    float f1 = k;
    float f2 = n;
    float f3 = m;
    Object localObject = a;
    paramCanvas.drawCircle(f1, f2, f3, (Paint)localObject);
    RectF localRectF1 = c;
    f2 = k - m;
    f3 = n - m;
    float f4 = k + m;
    float f5 = n + m;
    localRectF1.set(f2, f3, f4, f5);
    RectF localRectF2 = c;
    Paint localPaint = b;
    localObject = paramCanvas;
    paramCanvas.drawArc(localRectF2, -90.0F, 180.0F, true, localPaint);
  }
  
  public void setLeftColor(int paramInt)
  {
    a.setColor(paramInt);
  }
  
  public void setRightColor(int paramInt)
  {
    b.setColor(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.ThemeSelectorView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */