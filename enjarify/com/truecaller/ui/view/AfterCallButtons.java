package com.truecaller.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.truecaller.aftercall.PromoBadgeView;

public class AfterCallButtons
  extends LinearLayout
  implements View.OnClickListener
{
  public View a;
  public PromoBadgeView b;
  public PromoBadgeView c;
  public boolean d = false;
  public int e = 0;
  private View f;
  private ImageView g;
  private TextView h;
  private View i;
  private ImageView j;
  private TextView k;
  private View l;
  private View m;
  private View n;
  private View o;
  private View p;
  private boolean q;
  private AfterCallButtons.a r;
  
  public AfterCallButtons(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    inflate(paramContext, 2131558535, this);
    paramContext = findViewById(2131361876);
    f = paramContext;
    paramContext = (ImageView)findViewById(2131361886);
    g = paramContext;
    paramContext = (TextView)findViewById(2131361887);
    h = paramContext;
    paramContext = findViewById(2131361885);
    i = paramContext;
    paramContext = findViewById(2131361895);
    a = paramContext;
    paramContext = (ImageView)findViewById(2131361874);
    j = paramContext;
    paramContext = (TextView)findViewById(2131361875);
    k = paramContext;
    paramContext = findViewById(2131361879);
    l = paramContext;
    paramContext = findViewById(2131361873);
    m = paramContext;
    paramContext = findViewById(2131361891);
    n = paramContext;
    paramContext = findViewById(2131361888);
    o = paramContext;
    paramContext = findViewById(2131361882);
    p = paramContext;
    paramContext = (PromoBadgeView)findViewById(2131361893);
    b = paramContext;
    paramContext = (PromoBadgeView)findViewById(2131361897);
    c = paramContext;
    f.setOnClickListener(this);
    i.setOnClickListener(this);
    a.setOnClickListener(this);
    l.setOnClickListener(this);
    m.setOnClickListener(this);
    n.setOnClickListener(this);
    o.setOnClickListener(this);
    p.setOnClickListener(this);
    int i1 = com.truecaller.utils.ui.b.a(getContext(), 2130968620);
    com.truecaller.utils.ui.b.a((ImageView)findViewById(2131361877), i1);
    com.truecaller.utils.ui.b.a(g, i1);
    com.truecaller.utils.ui.b.a((ImageView)findViewById(2131361896), i1);
    com.truecaller.utils.ui.b.a((ImageView)findViewById(2131361880), i1);
    com.truecaller.utils.ui.b.a(j, i1);
    com.truecaller.utils.ui.b.a((ImageView)findViewById(2131361892), i1);
    com.truecaller.utils.ui.b.a((ImageView)findViewById(2131361883), i1);
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    Object localObject = g;
    ((ImageView)localObject).setImageResource(paramInt1);
    paramInt1 = q;
    Context localContext1;
    int i2;
    if (paramInt1 != 0)
    {
      localContext1 = getContext();
      paramInt1 = android.support.v4.content.b.c(localContext1, 2131100374);
      localObject = getContext();
      int i1 = 2131100383;
      i2 = android.support.v4.content.b.c((Context)localObject, i1);
    }
    else
    {
      paramInt1 = d;
      if (paramInt1 != 0)
      {
        paramInt1 = e;
        i2 = paramInt1;
      }
      else
      {
        localContext1 = getContext();
        i2 = 2130968620;
        paramInt1 = com.truecaller.utils.ui.b.a(localContext1, i2);
        Context localContext2 = getContext();
        i2 = com.truecaller.utils.ui.b.a(localContext2, i2);
      }
    }
    com.truecaller.utils.ui.b.a(g, paramInt1);
    h.setTextColor(i2);
    h.setText(paramInt2);
  }
  
  public final void a(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
  {
    TextView localTextView = k;
    localTextView.setText(paramInt3);
    paramInt3 = d;
    if ((paramInt3 != 0) && (!paramBoolean))
    {
      Object localObject = k;
      paramInt2 = e;
      ((TextView)localObject).setTextColor(paramInt2);
      localObject = j;
      paramInt2 = e;
      com.truecaller.utils.ui.b.a((ImageView)localObject, paramInt2);
      return;
    }
    k.setTextColor(paramInt2);
    com.truecaller.utils.ui.b.a(j, paramInt1);
  }
  
  public final void a(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    com.truecaller.utils.ui.b.a((ImageView)findViewById(2131361877), paramInt1);
    ((TextView)findViewById(2131361878)).setTextColor(paramInt2);
    com.truecaller.utils.ui.b.a(g, paramInt1);
    h.setTextColor(paramInt2);
    com.truecaller.utils.ui.b.a((ImageView)findViewById(2131361896), paramInt1);
    ((TextView)findViewById(2131361898)).setTextColor(paramInt2);
    com.truecaller.utils.ui.b.a((ImageView)findViewById(2131361880), paramInt1);
    TextView localTextView = (TextView)findViewById(2131361881);
    localTextView.setTextColor(paramInt2);
    int i1 = 2130969585;
    int i2;
    if (paramBoolean)
    {
      Context localContext1 = getContext();
      i2 = com.truecaller.utils.ui.b.a(localContext1, i1);
    }
    else
    {
      i2 = paramInt1;
    }
    if (paramBoolean)
    {
      Context localContext2 = getContext();
      paramBoolean = com.truecaller.utils.ui.b.a(localContext2, i1);
    }
    else
    {
      paramBoolean = paramInt2;
    }
    com.truecaller.utils.ui.b.a(j, i2);
    k.setTextColor(paramBoolean);
    com.truecaller.utils.ui.b.a((ImageView)findViewById(2131361892), paramInt1);
    ((TextView)findViewById(2131361894)).setTextColor(paramInt2);
    com.truecaller.utils.ui.b.a((ImageView)findViewById(2131361889), paramInt1);
    ((TextView)findViewById(2131361890)).setTextColor(paramInt2);
    com.truecaller.utils.ui.b.a((ImageView)findViewById(2131361883), paramInt1);
    ((TextView)findViewById(2131361884)).setTextColor(paramInt2);
    setShowDividers(0);
  }
  
  public final void a(boolean paramBoolean)
  {
    View localView = o;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localView.setVisibility(paramBoolean);
  }
  
  public final void a(boolean paramBoolean1, boolean paramBoolean2)
  {
    boolean bool = d;
    if (bool) {
      return;
    }
    q = paramBoolean1;
    if ((paramBoolean1) && (!paramBoolean2))
    {
      paramBoolean1 = android.support.v4.content.b.c(getContext(), 2131100374);
      int i1 = android.support.v4.content.b.c(getContext(), 2131100383);
      a(paramBoolean1, i1, paramBoolean2);
      return;
    }
    paramBoolean1 = com.truecaller.utils.ui.b.a(getContext(), 2130968620);
    a(paramBoolean1, paramBoolean1, paramBoolean2);
  }
  
  public void onClick(View paramView)
  {
    int i1 = paramView.getId();
    int i2 = -1;
    switch (i1)
    {
    default: 
      i1 = -1;
      break;
    case 2131361895: 
      i1 = 7;
      break;
    case 2131361891: 
      i1 = 4;
      break;
    case 2131361888: 
      i1 = 5;
      break;
    case 2131361885: 
      i1 = 1;
      break;
    case 2131361882: 
      i1 = 6;
      break;
    case 2131361879: 
      i1 = 2;
      break;
    case 2131361876: 
      i1 = 0;
      paramView = null;
      break;
    case 2131361873: 
      i1 = 3;
    }
    if (i1 != i2)
    {
      AfterCallButtons.a locala = r;
      if (locala != null) {
        locala.a(i1);
      }
    }
  }
  
  public void setBlockButtonAvailable(boolean paramBoolean)
  {
    View localView = m;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localView.setVisibility(paramBoolean);
  }
  
  public void setCallButtonAvailable(boolean paramBoolean)
  {
    View localView = f;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localView.setVisibility(paramBoolean);
  }
  
  public void setMessageButtonText(int paramInt)
  {
    ((TextView)findViewById(2131361894)).setText(paramInt);
  }
  
  public void setOnButtonClickListener(AfterCallButtons.a parama)
  {
    r = parama;
  }
  
  public void setPayButtonAvailable(boolean paramBoolean)
  {
    View localView = p;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localView.setVisibility(paramBoolean);
  }
  
  public void setPhoneBookAvailable(boolean paramBoolean)
  {
    View localView = i;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localView.setVisibility(paramBoolean);
  }
  
  public void setReferralButtonLabel(String paramString)
  {
    ((TextView)o.findViewById(2131361890)).setText(paramString);
  }
  
  public void setSaveToPhoneBookButton(boolean paramBoolean)
  {
    View localView = i;
    if (paramBoolean) {
      paramBoolean = true;
    } else {
      paramBoolean = false;
    }
    localView.setVisibility(paramBoolean);
  }
  
  public void setSpam(boolean paramBoolean)
  {
    View localView = l;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localView.setVisibility(paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.AfterCallButtons
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */