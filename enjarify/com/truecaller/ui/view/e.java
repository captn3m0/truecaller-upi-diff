package com.truecaller.ui.view;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.ui.components.n;
import com.truecaller.ui.components.n.d;
import java.util.Iterator;
import java.util.List;

public class e
  extends n
  implements n.d
{
  protected final Contact a;
  
  public e(Contact paramContact)
  {
    super(str, "");
    a = paramContact;
  }
  
  public final Contact a()
  {
    return a;
  }
  
  public String a(Context paramContext)
  {
    paramContext = a.t();
    boolean bool = TextUtils.isEmpty(paramContext);
    if (bool) {
      return a.q();
    }
    return a.t();
  }
  
  public String b(Context paramContext)
  {
    Object localObject1 = a;
    boolean bool1 = ((Contact)localObject1).Z();
    if (bool1)
    {
      localObject1 = a;
      bool1 = ((Contact)localObject1).O();
      if (bool1)
      {
        localObject1 = a.A();
        int i = ((List)localObject1).size();
        int j = 1;
        i -= j;
        if (i == 0) {
          return a.q();
        }
        paramContext = paramContext.getResources();
        int k = 2131755036;
        int m = 2;
        Object[] arrayOfObject = new Object[m];
        Iterator localIterator = a.A().iterator();
        Number localNumber;
        int n;
        do
        {
          boolean bool2 = localIterator.hasNext();
          if (!bool2) {
            break;
          }
          localNumber = (Number)localIterator.next();
          n = localNumber.i();
        } while (n != m);
        Object localObject2 = localNumber.n();
        break label155;
        localObject2 = a.q();
        label155:
        arrayOfObject[0] = localObject2;
        localObject2 = Integer.valueOf(i);
        arrayOfObject[j] = localObject2;
        return paramContext.getQuantityString(k, i, arrayOfObject);
      }
    }
    paramContext = a;
    boolean bool3 = paramContext.R();
    if (bool3) {
      return null;
    }
    return a.q();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */