package com.truecaller.ui.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.drawable.Drawable;

final class h$1
  extends AnimatorListenerAdapter
{
  h$1(h paramh) {}
  
  public final void onAnimationStart(Animator paramAnimator)
  {
    paramAnimator = a;
    Drawable[] arrayOfDrawable = h.a(paramAnimator);
    h.a(paramAnimator, arrayOfDrawable);
    paramAnimator = h.a(a);
    int i = paramAnimator.length;
    int j = 0;
    while (j < i)
    {
      Drawable localDrawable = paramAnimator[j];
      h localh = a;
      int k = h.b(localh);
      h.a(localh, localDrawable, k);
      localDrawable.setAlpha(0);
      j += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.h.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */