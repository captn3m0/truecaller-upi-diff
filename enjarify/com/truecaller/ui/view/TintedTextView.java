package com.truecaller.ui.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class TintedTextView
  extends AppCompatTextView
{
  private int b = 0;
  
  public TintedTextView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    int i = getCurrentTextColor();
    setupPaint(i);
  }
  
  private void setupPaint(int paramInt)
  {
    int i = b;
    if (i == paramInt) {
      return;
    }
    Paint localPaint = new android/graphics/Paint;
    localPaint.<init>(1);
    PorterDuffColorFilter localPorterDuffColorFilter = new android/graphics/PorterDuffColorFilter;
    PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
    localPorterDuffColorFilter.<init>(paramInt, localMode);
    localPaint.setColorFilter(localPorterDuffColorFilter);
    b = paramInt;
    setLayerType(2, localPaint);
  }
  
  public void setTextColor(int paramInt)
  {
    int i = Color.red(paramInt);
    int j = Color.blue(paramInt);
    int k = Color.green(paramInt);
    i = Color.argb(255, i, j, k);
    super.setTextColor(i);
    setupPaint(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.TintedTextView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */