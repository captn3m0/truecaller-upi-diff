package com.truecaller.ui.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

public final class c
  extends Drawable
{
  private final com.truecaller.ui.components.c a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  
  private c(Context paramContext, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8)
  {
    Resources localResources = paramContext.getResources();
    int m = localResources.getDimensionPixelSize(paramInt3);
    int n;
    if ((paramInt6 != 0) && (paramInt7 != 0))
    {
      j = localResources.getDimensionPixelSize(paramInt6);
      n = com.truecaller.utils.ui.b.a(paramContext, paramInt7);
      k = j;
      j = paramInt5;
    }
    else
    {
      j = paramInt5;
      k = 0;
      n = 0;
    }
    j = com.truecaller.utils.ui.b.a(localContext, j);
    d = j;
    j = android.support.v4.content.b.c(localContext, 2131100636);
    e = j;
    com.truecaller.ui.components.c localc1 = new com/truecaller/ui/components/c;
    int i1 = localResources.getDimensionPixelSize(i);
    int i2 = localResources.getDimensionPixelSize(paramInt2);
    int i3 = d;
    int i4 = com.truecaller.utils.ui.b.a(localContext, 2130969588);
    int i5 = paramInt4;
    float f = localResources.getDimension(paramInt4);
    localc1.<init>(i1, i2, m, i3, i4, f, k, n, paramInt8);
    a = localc1;
    i5 = localResources.getDimensionPixelSize(i);
    b = i5;
    k *= 2;
    i5 = m + k;
    c = i5;
  }
  
  public c(Context paramContext, boolean paramBoolean1, boolean paramBoolean2, int paramInt)
  {
    this(paramContext, i, j, k, m, paramInt, n, i1, paramBoolean2);
  }
  
  public final void a(int paramInt)
  {
    com.truecaller.ui.components.c localc = a;
    int i = b;
    if (i != paramInt)
    {
      localc = a;
      b = paramInt;
      invalidateSelf();
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    com.truecaller.ui.components.c localc = a;
    a = paramBoolean;
    if (paramBoolean) {
      paramBoolean = e;
    } else {
      paramBoolean = d;
    }
    localc.a(paramBoolean);
    invalidateSelf();
  }
  
  public final void draw(Canvas paramCanvas)
  {
    com.truecaller.ui.components.c localc = a;
    Object localObject = getBounds();
    c.set((Rect)localObject);
    localObject = c;
    localc.a(paramCanvas, (RectF)localObject);
  }
  
  public final int getIntrinsicHeight()
  {
    return b;
  }
  
  public final int getIntrinsicWidth()
  {
    return c;
  }
  
  public final int getOpacity()
  {
    return -1;
  }
  
  public final void setAlpha(int paramInt) {}
  
  public final void setColorFilter(ColorFilter paramColorFilter) {}
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */