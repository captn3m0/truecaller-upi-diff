package com.truecaller.ui.view;

import android.content.Context;
import com.truecaller.data.access.c;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;

public final class f
  extends e
{
  public final HistoryEvent b;
  private c c;
  
  public f(Contact paramContact, HistoryEvent paramHistoryEvent)
  {
    super(paramContact);
    b = paramHistoryEvent;
  }
  
  public f(HistoryEvent paramHistoryEvent)
  {
    this(localContact, paramHistoryEvent);
  }
  
  public final String a(Context paramContext)
  {
    return super.a(paramContext);
  }
  
  public final String b(Context paramContext)
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = ((HistoryEvent)localObject).getId();
      if (localObject != null)
      {
        localObject = a;
        if (localObject != null)
        {
          localObject = a;
          boolean bool = ((Contact)localObject).R();
          if (!bool) {
            return a.q();
          }
          localObject = c;
          if (localObject == null)
          {
            localObject = new com/truecaller/data/access/c;
            ((c)localObject).<init>(paramContext);
            c = ((c)localObject);
          }
          paramContext = c;
          localObject = b.getId();
          long l = ((Long)localObject).longValue();
          paramContext = paramContext.b(l);
          if (paramContext != null) {
            return paramContext.b();
          }
          return null;
        }
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */