package com.truecaller.ui.view;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.support.v4.view.ViewPager.f;
import android.util.AttributeSet;
import android.view.View;
import com.truecaller.R.styleable;
import com.truecaller.util.at;

public class DotPagerIndicator
  extends View
  implements ViewPager.f
{
  private Paint a;
  private int b;
  private int c;
  private float d;
  private int e;
  private int f;
  private float g = 6.0F;
  private float h;
  private float i;
  private float j;
  private int k;
  private ArgbEvaluator l;
  
  public DotPagerIndicator(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    Object localObject = new android/animation/ArgbEvaluator;
    ((ArgbEvaluator)localObject).<init>();
    l = ((ArgbEvaluator)localObject);
    localObject = paramContext.getTheme();
    int[] arrayOfInt = R.styleable.DotPagerIndicator;
    paramAttributeSet = ((Resources.Theme)localObject).obtainStyledAttributes(paramAttributeSet, arrayOfInt, 0, 0);
    localObject = new android/graphics/Paint;
    int m = 1;
    float f1 = Float.MIN_VALUE;
    ((Paint)localObject).<init>(m);
    a = ((Paint)localObject);
    localObject = a;
    Paint.Style localStyle = Paint.Style.FILL;
    ((Paint)localObject).setStyle(localStyle);
    int n = 4;
    try
    {
      n = paramAttributeSet.getInteger(n, 0);
      b = n;
      localObject = paramContext.getResources();
      int i1 = 2131100594;
      float f2 = 1.7813574E38F;
      n = ((Resources)localObject).getColor(i1);
      n = paramAttributeSet.getColor(0, n);
      e = n;
      n = 3;
      i1 = paramAttributeSet.getColor(n, 0);
      f = i1;
      i1 = 6;
      f2 = 8.4E-45F;
      m = paramAttributeSet.getDimensionPixelSize(m, i1);
      f1 = m;
      g = f1;
      m = 2;
      f1 = 2.8E-45F;
      i1 = 1090519040;
      f2 = 8.0F;
      int i2 = at.a(paramContext, f2);
      float f3 = i2;
      f3 = paramAttributeSet.getDimension(m, f3);
      h = f3;
      paramAttributeSet.recycle();
      c = 0;
      boolean bool = isInEditMode();
      if (bool)
      {
        setNumberOfPages(n);
        setFirstPage(0);
      }
      return;
    }
    finally
    {
      paramAttributeSet.recycle();
    }
  }
  
  public int getMinimumHeight()
  {
    return (int)(g * 2.0F);
  }
  
  public int getMinimumWidth()
  {
    float f1 = g * 2.0F;
    int m = b;
    float f2 = m;
    f1 *= f2;
    float f3 = m + -1;
    f2 = h;
    f3 *= f2;
    return (int)(f1 + f3);
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    float f1 = i;
    float f2 = g;
    f1 += f2;
    float f3 = j + f2;
    int m = 0;
    f2 = 0.0F;
    for (;;)
    {
      int n = b;
      if (m >= n) {
        break;
      }
      n = c;
      Paint localPaint;
      float f4;
      int i1;
      Integer localInteger1;
      int i2;
      Integer localInteger2;
      int i3;
      if (m == n)
      {
        localPaint = a;
        localObject = l;
        f4 = d;
        i1 = e;
        localInteger1 = Integer.valueOf(i1);
        i2 = f;
        localInteger2 = Integer.valueOf(i2);
        localObject = (Integer)((ArgbEvaluator)localObject).evaluate(f4, localInteger1, localInteger2);
        i3 = ((Integer)localObject).intValue();
        localPaint.setColor(i3);
      }
      else
      {
        n += 1;
        if (m == n)
        {
          localPaint = a;
          localObject = l;
          f4 = d;
          i1 = f;
          localInteger1 = Integer.valueOf(i1);
          i2 = e;
          localInteger2 = Integer.valueOf(i2);
          localObject = (Integer)((ArgbEvaluator)localObject).evaluate(f4, localInteger1, localInteger2);
          i3 = ((Integer)localObject).intValue();
          localPaint.setColor(i3);
        }
        else
        {
          localPaint = a;
          i3 = f;
          localPaint.setColor(i3);
        }
      }
      float f5 = g;
      Object localObject = a;
      paramCanvas.drawCircle(f1, f3, f5, (Paint)localObject);
      f5 = h;
      float f6 = g;
      f5 = f5 + f6 + f6;
      f1 += f5;
      m += 1;
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int m = getPaddingLeft();
    int n = getPaddingRight();
    m += n;
    n = getMinimumWidth();
    m += n;
    n = 1;
    paramInt1 = resolveSizeAndState(m, paramInt1, n);
    m = getPaddingTop();
    int i1 = getPaddingBottom();
    m += i1;
    i1 = getMinimumHeight();
    paramInt2 = resolveSizeAndState(m + i1, paramInt2, n);
    setMeasuredDimension(paramInt1, paramInt2);
  }
  
  public void onPageScrollStateChanged(int paramInt) {}
  
  public void onPageScrolled(int paramInt1, float paramFloat, int paramInt2)
  {
    d = paramFloat;
    int m = k;
    paramInt1 -= m;
    c = paramInt1;
    invalidate();
  }
  
  public void onPageSelected(int paramInt)
  {
    int m = k;
    paramInt -= m;
    c = paramInt;
    invalidate();
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    float f1 = getPaddingLeft();
    i = f1;
    f1 = getPaddingTop();
    j = f1;
  }
  
  public void setActiveColor(int paramInt)
  {
    e = paramInt;
    invalidate();
  }
  
  public void setFirstPage(int paramInt)
  {
    k = paramInt;
  }
  
  public void setNumberOfPages(int paramInt)
  {
    b = paramInt;
    invalidate();
    requestLayout();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.DotPagerIndicator
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */