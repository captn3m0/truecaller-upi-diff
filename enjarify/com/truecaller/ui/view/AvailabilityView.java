package com.truecaller.ui.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.r;
import android.support.v4.widget.m;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.Availability.Status;
import com.truecaller.presence.a;
import com.truecaller.search.local.model.c.b;
import com.truecaller.utils.extensions.t;

public final class AvailabilityView
  extends AppCompatTextView
  implements c.b
{
  private com.truecaller.search.local.model.c.a b;
  private c.g.a.b c;
  
  public AvailabilityView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  private final void a()
  {
    com.truecaller.search.local.model.c.a locala = b;
    if (locala != null)
    {
      boolean bool = locala.a();
      if (!bool)
      {
        Object localObject = this;
        localObject = (View)this;
        bool = r.H((View)localObject);
        if (bool)
        {
          bool = false;
          setAvailability(null);
          localObject = this;
          localObject = (c.b)this;
          locala.a((c.b)localObject);
        }
      }
      return;
    }
  }
  
  private final void b()
  {
    com.truecaller.search.local.model.c.a locala = b;
    if (locala != null)
    {
      boolean bool = locala.a();
      if (bool) {
        locala.b();
      }
    }
    setAvailability(null);
  }
  
  private final void setAvailability(a parama)
  {
    c.g.a.b localb = null;
    if (parama != null)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        localObject1 = ((Availability)localObject1).a();
        break label28;
      }
    }
    boolean bool = false;
    Object localObject1 = null;
    label28:
    t.a(this);
    if (localObject1 != null)
    {
      Object localObject2 = b.a;
      int i = ((Availability.Status)localObject1).ordinal();
      int j = localObject2[i];
      switch (j)
      {
      default: 
        break;
      case 1: 
      case 2: 
        localObject2 = c;
        if (localObject2 != null)
        {
          localObject2 = (CharSequence)((c.g.a.b)localObject2).invoke(parama);
          if (localObject2 != null) {}
        }
        else
        {
          localObject2 = getContext();
          String str = "context";
          k.a(localObject2, str);
          parama = a.a(parama, (Context)localObject2);
          localObject2 = parama;
          localObject2 = (CharSequence)parama;
        }
        setText((CharSequence)localObject2);
        parama = new com/truecaller/ui/c$a;
        localObject2 = getContext();
        parama.<init>((Context)localObject2);
        parama = parama.b().c().d();
        localObject2 = Availability.Status.AVAILABLE;
        if (localObject1 == localObject2)
        {
          bool = true;
        }
        else
        {
          bool = false;
          localObject1 = null;
        }
        parama = parama.a(bool);
        localObject1 = this;
        localObject1 = (TextView)this;
        parama = (Drawable)parama.a();
        m.a((TextView)localObject1, parama, null);
        return;
      }
    }
    setCompoundDrawables(null, null, null, null);
    localb = c;
    if (localb != null)
    {
      parama = (CharSequence)localb.invoke(parama);
      setText(parama);
      return;
    }
    t.b(this);
  }
  
  public final void a(a parama)
  {
    setAvailability(parama);
  }
  
  public final void a(com.truecaller.search.local.model.c.a parama)
  {
    b();
    b = parama;
    a();
  }
  
  public final c.g.a.b getCustomTextProvider()
  {
    return c;
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    a();
  }
  
  protected final void onDetachedFromWindow()
  {
    b();
    super.onDetachedFromWindow();
  }
  
  public final void setCustomTextProvider(c.g.a.b paramb)
  {
    c = paramb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.AvailabilityView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */