package com.truecaller.ui.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

final class BottomBar$1
  extends AnimatorListenerAdapter
{
  private boolean d = false;
  
  BottomBar$1(BottomBar paramBottomBar, View paramView, boolean paramBoolean) {}
  
  public final void onAnimationCancel(Animator paramAnimator)
  {
    d = true;
  }
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    boolean bool1 = d;
    int i = 0;
    if (bool1)
    {
      paramAnimator = a;
      bool2 = b;
      if (bool2) {
        i = 8;
      }
      paramAnimator.setVisibility(i);
      return;
    }
    paramAnimator = a;
    boolean bool2 = b;
    if (!bool2) {
      i = 8;
    }
    paramAnimator.setVisibility(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.BottomBar.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */