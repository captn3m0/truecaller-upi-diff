package com.truecaller.ui.view;

public enum BottomBar$DialpadState
{
  public final int colorAttr;
  public final int drawable;
  public final float scale;
  
  static
  {
    DialpadState localDialpadState = new com/truecaller/ui/view/BottomBar$DialpadState;
    Object localObject1 = localDialpadState;
    localDialpadState.<init>("DIALPAD_DOWN", 0, 0.85F, 2130969528, 2131234051);
    DIALPAD_DOWN = localDialpadState;
    localObject1 = new com/truecaller/ui/view/BottomBar$DialpadState;
    ((DialpadState)localObject1).<init>("DIALPAD_UP", 1, 1.0F, 2130969528, 2131234051);
    DIALPAD_UP = (DialpadState)localObject1;
    localObject1 = new com/truecaller/ui/view/BottomBar$DialpadState;
    Object localObject2 = localObject1;
    ((DialpadState)localObject1).<init>("NUMBER_ENTERED", 2, 1.0F, 2130969533, 2131233928);
    NUMBER_ENTERED = (DialpadState)localObject1;
    localObject1 = new DialpadState[3];
    localObject2 = DIALPAD_DOWN;
    localObject1[0] = localObject2;
    localObject2 = DIALPAD_UP;
    localObject1[1] = localObject2;
    localObject2 = NUMBER_ENTERED;
    localObject1[2] = localObject2;
    $VALUES = (DialpadState[])localObject1;
  }
  
  private BottomBar$DialpadState(float paramFloat, int paramInt2, int paramInt3)
  {
    scale = paramFloat;
    colorAttr = paramInt2;
    drawable = paramInt3;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.BottomBar.DialpadState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */