package com.truecaller.ui.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.OvershootInterpolator;
import android.widget.LinearLayout;
import c.g.b.k;

public final class i$c
  extends AnimatorListenerAdapter
{
  i$c(i parami) {}
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    k.b(paramAnimator, "animation");
    paramAnimator = i.d(a);
    int i = paramAnimator.getChildCount();
    int j = 0;
    while (j < i)
    {
      ViewPropertyAnimator localViewPropertyAnimator = i.d(a).getChildAt(j).animate();
      float f = 1.0F;
      localViewPropertyAnimator = localViewPropertyAnimator.scaleX(f).scaleY(f);
      long l1 = j;
      long l2 = 50;
      l1 *= l2;
      localViewPropertyAnimator = localViewPropertyAnimator.setStartDelay(l1);
      Object localObject = new android/view/animation/OvershootInterpolator;
      ((OvershootInterpolator)localObject).<init>();
      localObject = (TimeInterpolator)localObject;
      localViewPropertyAnimator = localViewPropertyAnimator.setInterpolator((TimeInterpolator)localObject);
      l1 = 200L;
      localViewPropertyAnimator = localViewPropertyAnimator.setDuration(l1);
      localViewPropertyAnimator.start();
      j += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.i.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */