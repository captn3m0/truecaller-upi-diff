package com.truecaller.ui.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v7.graphics.drawable.DrawerArrowDrawable;
import com.truecaller.ui.components.c;
import com.truecaller.utils.ui.b;

public final class BadgeDrawerArrowDrawable
  extends DrawerArrowDrawable
{
  public BadgeDrawerArrowDrawable.BadgeTag a;
  public final c b;
  public final Context c;
  public final int d;
  
  public BadgeDrawerArrowDrawable(Context paramContext)
  {
    this(paramContext, (byte)0);
  }
  
  private BadgeDrawerArrowDrawable(Context paramContext, byte paramByte)
  {
    this(paramContext, '\000');
  }
  
  private BadgeDrawerArrowDrawable(Context paramContext, char paramChar)
  {
    super(paramContext);
    Object localObject = BadgeDrawerArrowDrawable.BadgeTag.NONE;
    a = ((BadgeDrawerArrowDrawable.BadgeTag)localObject);
    localObject = paramContext.getResources();
    c = paramContext;
    int i = b.a(paramContext, 2130969541);
    d = i;
    c localc = new com/truecaller/ui/components/c;
    int j = ((Resources)localObject).getDimensionPixelSize(2131165302);
    int k = ((Resources)localObject).getDimensionPixelSize(2131165303);
    int m = ((Resources)localObject).getDimensionPixelSize(2131165304);
    int n = d;
    int i1 = b.a(paramContext, 2130969588);
    float f = ((Resources)localObject).getDimension(2131165306);
    localc.<init>(j, k, m, n, i1, f, 0, -1, 0);
    b = localc;
  }
  
  public final void draw(Canvas paramCanvas)
  {
    super.draw(paramCanvas);
    RectF localRectF = new android/graphics/RectF;
    Rect localRect = getBounds();
    localRectF.<init>(localRect);
    float f1 = left;
    float f2 = localRectF.width();
    float f3 = 2.0F;
    f2 /= f3;
    f1 += f2;
    left = f1;
    f1 = bottom;
    f2 = localRectF.height() / f3;
    f1 -= f2;
    bottom = f1;
    b.a(paramCanvas, localRectF);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.BadgeDrawerArrowDrawable
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */