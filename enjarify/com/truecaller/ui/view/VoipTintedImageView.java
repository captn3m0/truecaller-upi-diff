package com.truecaller.ui.view;

import android.content.Context;
import android.support.v4.view.r;
import android.util.AttributeSet;
import android.view.View;
import c.g.a.b;
import c.g.b.k;
import c.u;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.calling.be;
import com.truecaller.calling.contacts_list.data.g;
import com.truecaller.calling.contacts_list.j;
import com.truecaller.data.entity.Contact;
import com.truecaller.voip.ai;
import com.truecaller.voip.d;
import com.truecaller.voip.e;

public final class VoipTintedImageView
  extends TintedImageView
  implements be, e
{
  public ai a;
  public d b;
  private Contact c;
  private b d;
  private g e;
  
  public VoipTintedImageView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramContext = getContext();
    if (paramContext != null) {
      paramContext = paramContext.getApplicationContext();
    } else {
      paramContext = null;
    }
    if (paramContext != null)
    {
      ((bk)paramContext).a().ct().a(this);
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramContext;
  }
  
  private final void a()
  {
    Object localObject1 = this;
    localObject1 = (View)this;
    boolean bool = r.H((View)localObject1);
    if (bool)
    {
      localObject1 = c;
      if (localObject1 != null)
      {
        Object localObject2 = e;
        if (localObject2 != null) {
          localObject2 = ((g)localObject2).a((Contact)localObject1);
        } else {
          localObject2 = null;
        }
        if (localObject2 != null)
        {
          bool = ((Boolean)localObject2).booleanValue();
          onVoipAvailabilityLoaded(bool);
          return;
        }
        localObject2 = a;
        if (localObject2 == null)
        {
          localObject3 = "voipUtil";
          k.a((String)localObject3);
        }
        Object localObject3 = this;
        localObject3 = (e)this;
        ((ai)localObject2).a((Contact)localObject1, (e)localObject3);
        return;
      }
    }
  }
  
  private final void setVoipEnabled(boolean paramBoolean)
  {
    Object localObject = this;
    localObject = (View)this;
    boolean bool = r.H((View)localObject);
    if (bool)
    {
      localObject = d;
      if (localObject != null)
      {
        Boolean localBoolean = Boolean.valueOf(paramBoolean);
        ((b)localObject).invoke(localBoolean);
        return;
      }
    }
  }
  
  public final void a(g paramg, Contact paramContact, b paramb)
  {
    k.b(paramg, "voipAvailabilityCache");
    k.b(paramContact, "contact");
    k.b(paramb, "callback");
    e = paramg;
    c = paramContact;
    d = paramb;
    a();
  }
  
  public final d getVoip()
  {
    d locald = b;
    if (locald == null)
    {
      String str = "voip";
      k.a(str);
    }
    return locald;
  }
  
  public final ai getVoipUtil()
  {
    ai localai = a;
    if (localai == null)
    {
      String str = "voipUtil";
      k.a(str);
    }
    return localai;
  }
  
  protected final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    a();
  }
  
  public final void onVoipAvailabilityLoaded(boolean paramBoolean)
  {
    g localg = e;
    if (localg != null)
    {
      Contact localContact = c;
      if (localContact != null)
      {
        Boolean localBoolean = localg.a(localContact);
        if (localBoolean == null) {
          localg.a(localContact, paramBoolean);
        }
      }
    }
    setVoipEnabled(paramBoolean);
  }
  
  public final void setVoip(d paramd)
  {
    k.b(paramd, "<set-?>");
    b = paramd;
  }
  
  public final void setVoipUtil(ai paramai)
  {
    k.b(paramai, "<set-?>");
    a = paramai;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.view.VoipTintedImageView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */