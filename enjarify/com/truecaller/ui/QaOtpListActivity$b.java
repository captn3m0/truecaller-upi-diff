package com.truecaller.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;
import com.truecaller.notifications.y.b;
import java.util.ArrayList;
import java.util.List;

final class QaOtpListActivity$b
  extends RecyclerView.Adapter
{
  List a;
  List b;
  Context c;
  
  QaOtpListActivity$b(List paramList, Context paramContext)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    a = localArrayList;
    localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    b = localArrayList;
    a = paramList;
    c = paramContext;
  }
  
  public final int getItemCount()
  {
    return a.size();
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    y.b localb = (y.b)a.get(paramInt);
    int i = 3;
    Object localObject1 = new Object[i];
    String str = a;
    localObject1[0] = str;
    str = c;
    localObject1[1] = str;
    str = b;
    int j = 2;
    localObject1[j] = str;
    Object localObject2 = String.format("\n%s\n\nOTP: %s\nMatcher: %s\n", (Object[])localObject1);
    localObject1 = (TextView)itemView;
    ((TextView)localObject1).setText((CharSequence)localObject2);
    localObject2 = b;
    boolean bool = ((List)localObject2).contains(localb);
    if (bool)
    {
      localObject2 = itemView;
      i = 65280;
      ((View)localObject2).setBackgroundColor(i);
    }
    else
    {
      localObject2 = itemView;
      ((View)localObject2).setBackgroundColor(0);
    }
    paramViewHolder = itemView;
    localObject2 = new com/truecaller/ui/-$$Lambda$QaOtpListActivity$b$Sll644ZDDiOU8VVTcBjqKXbzGQo;
    ((-..Lambda.QaOtpListActivity.b.Sll644ZDDiOU8VVTcBjqKXbzGQo)localObject2).<init>(this, localb);
    paramViewHolder.setOnClickListener((View.OnClickListener)localObject2);
  }
  
  public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    paramViewGroup = new android/widget/TextView;
    Object localObject = c;
    paramViewGroup.<init>((Context)localObject);
    localObject = new android/view/ViewGroup$LayoutParams;
    ((ViewGroup.LayoutParams)localObject).<init>(-1, -2);
    paramViewGroup.setLayoutParams((ViewGroup.LayoutParams)localObject);
    localObject = new com/truecaller/ui/QaOtpListActivity$b$1;
    ((QaOtpListActivity.b.1)localObject).<init>(this, paramViewGroup);
    return (RecyclerView.ViewHolder)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.QaOtpListActivity.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */