package com.truecaller.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import c.g.b.k;
import com.truecaller.wizard.utils.PermissionPoller;
import com.truecaller.wizard.utils.PermissionPoller.Permission;

public final class NotificationAccessActivity
  extends AppCompatActivity
{
  public static final NotificationAccessActivity.a a;
  private boolean b;
  
  static
  {
    NotificationAccessActivity.a locala = new com/truecaller/ui/NotificationAccessActivity$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public static final Intent a(Context paramContext, Intent paramIntent)
  {
    return NotificationAccessActivity.a.a(paramContext, paramIntent);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    boolean bool;
    if (paramBundle != null)
    {
      String str = "hasOpenedNotificationAccessSetting";
      bool = paramBundle.getBoolean(str);
    }
    else
    {
      bool = false;
      paramBundle = null;
    }
    b = bool;
  }
  
  public final void onResume()
  {
    super.onResume();
    Object localObject1 = getIntent();
    k.a(localObject1, "intent");
    localObject1 = ((Intent)localObject1).getExtras();
    String str = "goBackIntent";
    localObject1 = (Intent)((Bundle)localObject1).get(str);
    int i = b;
    if (i == 0)
    {
      i = 1;
      b = i;
      if (localObject1 != null)
      {
        ((Intent)localObject1).setAction("android.intent.action.MAIN");
        int j = 335609856;
        ((Intent)localObject1).setFlags(j);
        localObject2 = new com/truecaller/wizard/utils/PermissionPoller;
        Object localObject3 = this;
        localObject3 = (Context)this;
        Handler localHandler = new android/os/Handler;
        Looper localLooper = Looper.getMainLooper();
        localHandler.<init>(localLooper);
        ((PermissionPoller)localObject2).<init>((Context)localObject3, localHandler, (Intent)localObject1);
        localObject1 = PermissionPoller.Permission.NOTIFICATION_ACCESS;
        ((PermissionPoller)localObject2).a((PermissionPoller.Permission)localObject1);
      }
      int k = getIntent().getIntExtra("toastMessage", 2131888934);
      Object localObject2 = this;
      Toast.makeText((Context)this, k, i).show();
      localObject1 = new android/content/Intent;
      ((Intent)localObject1).<init>("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
      startActivity((Intent)localObject1);
      return;
    }
    if (localObject1 != null) {
      startActivity((Intent)localObject1);
    }
    finish();
  }
  
  public final void onSaveInstanceState(Bundle paramBundle)
  {
    if (paramBundle != null)
    {
      String str = "hasOpenedNotificationAccessSetting";
      boolean bool = b;
      paramBundle.putBoolean(str, bool);
    }
    super.onSaveInstanceState(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.NotificationAccessActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */