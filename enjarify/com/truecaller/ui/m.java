package com.truecaller.ui;

import android.content.Context;
import android.text.style.ForegroundColorSpan;
import com.truecaller.utils.ui.b;
import java.util.ArrayList;

public abstract class m
  extends d
{
  protected String k = "";
  protected final int l;
  protected ForegroundColorSpan m;
  
  public m(Context paramContext)
  {
    this(paramContext, localArrayList);
  }
  
  private m(Context paramContext, ArrayList paramArrayList)
  {
    super(paramContext, paramArrayList);
    int i = b.a(paramContext, 2130968888);
    l = i;
    paramContext = new android/text/style/ForegroundColorSpan;
    int j = l;
    paramContext.<init>(j);
    m = paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */