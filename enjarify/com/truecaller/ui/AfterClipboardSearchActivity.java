package com.truecaller.ui;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.analytics.bc;
import com.truecaller.analytics.e;
import com.truecaller.bp;
import com.truecaller.i.c;
import com.truecaller.utils.extensions.a;

public class AfterClipboardSearchActivity
  extends AppCompatActivity
{
  private AlertDialog a;
  private c b;
  
  void a()
  {
    Object localObject1 = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject1).<init>(this);
    Object localObject2 = new com/truecaller/ui/-$$Lambda$GNwkSGDDjd3sec6h8pSc_HlIPu0;
    ((-..Lambda.GNwkSGDDjd3sec6h8pSc_HlIPu0)localObject2).<init>(this);
    localObject1 = ((AlertDialog.Builder)localObject1).setPositiveButton(2131887235, (DialogInterface.OnClickListener)localObject2);
    localObject2 = new com/truecaller/ui/-$$Lambda$GNwkSGDDjd3sec6h8pSc_HlIPu0;
    ((-..Lambda.GNwkSGDDjd3sec6h8pSc_HlIPu0)localObject2).<init>(this);
    localObject1 = ((AlertDialog.Builder)localObject1).setNegativeButton(2131887214, (DialogInterface.OnClickListener)localObject2).setMessage(2131886390);
    localObject2 = new com/truecaller/ui/-$$Lambda$AfterClipboardSearchActivity$CuLQLdEAOBvJpi7tbsDQjmWB86A;
    ((-..Lambda.AfterClipboardSearchActivity.CuLQLdEAOBvJpi7tbsDQjmWB86A)localObject2).<init>(this);
    localObject1 = ((AlertDialog.Builder)localObject1).setOnCancelListener((DialogInterface.OnCancelListener)localObject2).setCancelable(true).show();
    a = ((AlertDialog)localObject1);
    localObject1 = TrueApp.y().a().c();
    localObject2 = new com/truecaller/analytics/bc;
    ((bc)localObject2).<init>("afterClipboardSearch");
    ((b)localObject1).a((e)localObject2);
  }
  
  public void a(DialogInterface paramDialogInterface, int paramInt)
  {
    int i = -2;
    if (paramInt == i)
    {
      paramDialogInterface = b;
      String str = "clipboardSearchEnabled";
      paramDialogInterface.b(str, false);
    }
    b.b("clipboardSearchHaveAskedOnDismiss", true);
    finish();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Object localObject1 = TrueApp.y().a().D();
    b = ((c)localObject1);
    a.a(this);
    localObject1 = getTheme();
    ThemeManager.Theme localTheme = ThemeManager.a();
    int i = resId;
    Object localObject2 = null;
    ((Resources.Theme)localObject1).applyStyle(i, false);
    long l;
    if (paramBundle == null) {
      l = 200L;
    } else {
      l = 0L;
    }
    paramBundle = new android/os/Handler;
    localObject2 = getMainLooper();
    paramBundle.<init>((Looper)localObject2);
    localObject2 = new com/truecaller/ui/-$$Lambda$N1XsvC5vZHeN1NzwJCtDUcnmk-4;
    ((-..Lambda.N1XsvC5vZHeN1NzwJCtDUcnmk-4)localObject2).<init>(this);
    paramBundle.postDelayed((Runnable)localObject2, l);
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    AlertDialog localAlertDialog = a;
    if (localAlertDialog != null) {
      localAlertDialog.dismiss();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.AfterClipboardSearchActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */