package com.truecaller.ui;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.truecaller.TrueApp;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.at;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.view.ThemePreviewView;
import java.util.ArrayList;
import java.util.List;

public final class ad
  extends o
{
  private ThemePreviewView a;
  private ContextThemeWrapper b;
  private ad.a c;
  
  private void a(ThemeManager.Theme paramTheme)
  {
    Object localObject1 = getContext().getApplicationContext();
    b localb = TrueApp.y().a().c();
    Object localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("SettingChanged");
    localObject2 = ((e.a)localObject2).a("Setting", "Theme");
    String str = paramTheme.toString();
    localObject2 = ((e.a)localObject2).a("State", str).a();
    localb.a((e)localObject2);
    localObject1 = (ae)((bk)localObject1).a().f().a();
    paramTheme = at.a(paramTheme.toString(), "settings_screen");
    ((ae)localObject1).a(paramTheme);
  }
  
  private void b()
  {
    ThemeManager.Theme localTheme = c.a();
    ThemeManager.a(localTheme);
    a(localTheme);
    TruecallerInit.c(getContext(), "settings_screen");
  }
  
  public final boolean W_()
  {
    Object localObject1 = c.a();
    Object localObject2 = ThemeManager.a();
    if (localObject1 != localObject2)
    {
      localObject1 = new android/support/v7/app/AlertDialog$Builder;
      localObject2 = getActivity();
      ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
      localObject1 = ((AlertDialog.Builder)localObject1).setMessage(2131887127);
      Object localObject3 = new com/truecaller/ui/-$$Lambda$ad$7tS7v7doe3wheDFOJAeRUyWms7c;
      ((-..Lambda.ad.7tS7v7doe3wheDFOJAeRUyWms7c)localObject3).<init>(this);
      localObject1 = ((AlertDialog.Builder)localObject1).setPositiveButton(2131887235, (DialogInterface.OnClickListener)localObject3);
      localObject3 = new com/truecaller/ui/-$$Lambda$ad$kPNI_FxZ0RLuE5F17eugeoi3Jy0;
      ((-..Lambda.ad.kPNI_FxZ0RLuE5F17eugeoi3Jy0)localObject3).<init>(this);
      ((AlertDialog.Builder)localObject1).setNegativeButton(2131887214, (DialogInterface.OnClickListener)localObject3).setCancelable(false).show();
      return true;
    }
    return false;
  }
  
  public final void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    getActivity().setTitle(2131887051);
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
    paramMenuInflater.inflate(2131623978, paramMenu);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    boolean bool1 = true;
    setHasOptionsMenu(bool1);
    paramBundle = ThemeManager.a();
    Object localObject1 = ThemeManager.Theme.values();
    ArrayList localArrayList = new java/util/ArrayList;
    int i = localObject1.length;
    localArrayList.<init>(i);
    i = 0;
    int j = 0;
    int k = 0;
    for (;;)
    {
      int m = localObject1.length;
      if (j >= m) {
        break;
      }
      Object localObject2 = localObject1[j];
      Object localObject3 = ThemeManager.Theme.DEBUG;
      if (localObject2 == localObject3)
      {
        localObject3 = getContext();
        boolean bool2 = Settings.a((Context)localObject3);
        if (!bool2) {}
      }
      else
      {
        localArrayList.add(localObject2);
      }
      if (localObject2 == paramBundle) {
        k = j + 1;
      }
      j += 1;
    }
    paramBundle = new android/view/ContextThemeWrapper;
    localObject1 = getContext();
    j = aresId;
    paramBundle.<init>((Context)localObject1, j);
    b = paramBundle;
    paramBundle = (FrameLayout)paramLayoutInflater.inflate(2131559232, paramViewGroup, false);
    localObject1 = (ThemePreviewView)paramBundle.findViewById(2131364830);
    a = ((ThemePreviewView)localObject1);
    paramLayoutInflater = (RecyclerView)paramLayoutInflater.inflate(2131559231, paramViewGroup, false);
    paramViewGroup = new com/truecaller/ui/ad$a;
    paramViewGroup.<init>(this, paramBundle, localArrayList, k);
    c = paramViewGroup;
    paramViewGroup = c;
    paramLayoutInflater.setAdapter(paramViewGroup);
    paramViewGroup = new android/support/v7/widget/GridLayoutManager;
    paramBundle = getContext();
    paramViewGroup.<init>(paramBundle, 3);
    paramBundle = new com/truecaller/ui/ad$1;
    paramBundle.<init>(this, paramViewGroup);
    paramViewGroup.setSpanSizeLookup(paramBundle);
    paramLayoutInflater.setLayoutManager(paramViewGroup);
    return paramLayoutInflater;
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    int i = paramMenuItem.getItemId();
    int j = 2131361827;
    if (i != j) {
      return super.onOptionsItemSelected(paramMenuItem);
    }
    b();
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */