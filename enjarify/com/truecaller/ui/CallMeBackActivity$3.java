package com.truecaller.ui;

import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;

final class CallMeBackActivity$3
  implements ViewTreeObserver.OnPreDrawListener
{
  CallMeBackActivity$3(CallMeBackActivity paramCallMeBackActivity) {}
  
  public final boolean onPreDraw()
  {
    float f = CallMeBackActivity.c(a).getTop() * 1.5F;
    CallMeBackActivity.d(a).setTranslationY(f);
    ValueAnimator localValueAnimator = CallMeBackActivity.e(a);
    float[] arrayOfFloat = new float[2];
    arrayOfFloat[0] = f;
    arrayOfFloat[1] = 0.0F;
    localValueAnimator.setFloatValues(arrayOfFloat);
    CallMeBackActivity.e(a).start();
    CallMeBackActivity.d(a).getViewTreeObserver().removeOnPreDrawListener(this);
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.CallMeBackActivity.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */