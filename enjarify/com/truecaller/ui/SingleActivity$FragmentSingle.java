package com.truecaller.ui;

public enum SingleActivity$FragmentSingle
{
  static
  {
    Object localObject = new com/truecaller/ui/SingleActivity$FragmentSingle;
    ((FragmentSingle)localObject).<init>("CALLER", 0);
    CALLER = (FragmentSingle)localObject;
    localObject = new com/truecaller/ui/SingleActivity$FragmentSingle;
    int i = 1;
    ((FragmentSingle)localObject).<init>("EDIT_ME", i);
    EDIT_ME = (FragmentSingle)localObject;
    localObject = new com/truecaller/ui/SingleActivity$FragmentSingle;
    int j = 2;
    ((FragmentSingle)localObject).<init>("EDIT_ME_FORM", j);
    EDIT_ME_FORM = (FragmentSingle)localObject;
    localObject = new com/truecaller/ui/SingleActivity$FragmentSingle;
    int k = 3;
    ((FragmentSingle)localObject).<init>("NOTIFICATION_MESSAGES", k);
    NOTIFICATION_MESSAGES = (FragmentSingle)localObject;
    localObject = new com/truecaller/ui/SingleActivity$FragmentSingle;
    int m = 4;
    ((FragmentSingle)localObject).<init>("SETTINGS_MAIN", m);
    SETTINGS_MAIN = (FragmentSingle)localObject;
    localObject = new com/truecaller/ui/SingleActivity$FragmentSingle;
    int n = 5;
    ((FragmentSingle)localObject).<init>("FEEDBACK_FORM", n);
    FEEDBACK_FORM = (FragmentSingle)localObject;
    localObject = new com/truecaller/ui/SingleActivity$FragmentSingle;
    int i1 = 6;
    ((FragmentSingle)localObject).<init>("NOTIFICATIONS", i1);
    NOTIFICATIONS = (FragmentSingle)localObject;
    localObject = new com/truecaller/ui/SingleActivity$FragmentSingle;
    int i2 = 7;
    ((FragmentSingle)localObject).<init>("DETAILS_CALL_LOG", i2);
    DETAILS_CALL_LOG = (FragmentSingle)localObject;
    localObject = new com/truecaller/ui/SingleActivity$FragmentSingle;
    int i3 = 8;
    ((FragmentSingle)localObject).<init>("SPEED_DIAL", i3);
    SPEED_DIAL = (FragmentSingle)localObject;
    localObject = new com/truecaller/ui/SingleActivity$FragmentSingle;
    int i4 = 9;
    ((FragmentSingle)localObject).<init>("THEME_SELECTOR", i4);
    THEME_SELECTOR = (FragmentSingle)localObject;
    localObject = new com/truecaller/ui/SingleActivity$FragmentSingle;
    int i5 = 10;
    ((FragmentSingle)localObject).<init>("CALL_RECORDINGS", i5);
    CALL_RECORDINGS = (FragmentSingle)localObject;
    localObject = new FragmentSingle[11];
    FragmentSingle localFragmentSingle = CALLER;
    localObject[0] = localFragmentSingle;
    localFragmentSingle = EDIT_ME;
    localObject[i] = localFragmentSingle;
    localFragmentSingle = EDIT_ME_FORM;
    localObject[j] = localFragmentSingle;
    localFragmentSingle = NOTIFICATION_MESSAGES;
    localObject[k] = localFragmentSingle;
    localFragmentSingle = SETTINGS_MAIN;
    localObject[m] = localFragmentSingle;
    localFragmentSingle = FEEDBACK_FORM;
    localObject[n] = localFragmentSingle;
    localFragmentSingle = NOTIFICATIONS;
    localObject[i1] = localFragmentSingle;
    localFragmentSingle = DETAILS_CALL_LOG;
    localObject[i2] = localFragmentSingle;
    localFragmentSingle = SPEED_DIAL;
    localObject[i3] = localFragmentSingle;
    localFragmentSingle = THEME_SELECTOR;
    localObject[i4] = localFragmentSingle;
    localFragmentSingle = CALL_RECORDINGS;
    localObject[i5] = localFragmentSingle;
    $VALUES = (FragmentSingle[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.SingleActivity.FragmentSingle
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */