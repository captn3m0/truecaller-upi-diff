package com.truecaller.ui;

public enum ThemeManager$Theme
{
  public final int displayName;
  public final int resId;
  
  static
  {
    Object localObject = new com/truecaller/ui/ThemeManager$Theme;
    ((Theme)localObject).<init>("DEFAULT", 0, 2131887131, 2131952145);
    DEFAULT = (Theme)localObject;
    localObject = new com/truecaller/ui/ThemeManager$Theme;
    int i = 1;
    ((Theme)localObject).<init>("DARK", i, 2131887129, 2131952149);
    DARK = (Theme)localObject;
    localObject = new com/truecaller/ui/ThemeManager$Theme;
    int j = 2;
    ((Theme)localObject).<init>("COFFEE", j, 2131887128, 2131952147);
    COFFEE = (Theme)localObject;
    localObject = new com/truecaller/ui/ThemeManager$Theme;
    int k = 3;
    ((Theme)localObject).<init>("RAMADAN", k, 2131887134, 2131952180);
    RAMADAN = (Theme)localObject;
    localObject = new com/truecaller/ui/ThemeManager$Theme;
    int m = 4;
    ((Theme)localObject).<init>("PITCH_BLACK", m, 2131887133, 2131952161);
    PITCH_BLACK = (Theme)localObject;
    localObject = new com/truecaller/ui/ThemeManager$Theme;
    int n = 5;
    ((Theme)localObject).<init>("LIGHT_GRAY", n, 2131887132, 2131952160);
    LIGHT_GRAY = (Theme)localObject;
    localObject = new com/truecaller/ui/ThemeManager$Theme;
    int i1 = 6;
    ((Theme)localObject).<init>("DEBUG", i1, 2131887130, 2131952151);
    DEBUG = (Theme)localObject;
    localObject = new Theme[7];
    Theme localTheme = DEFAULT;
    localObject[0] = localTheme;
    localTheme = DARK;
    localObject[i] = localTheme;
    localTheme = COFFEE;
    localObject[j] = localTheme;
    localTheme = RAMADAN;
    localObject[k] = localTheme;
    localTheme = PITCH_BLACK;
    localObject[m] = localTheme;
    localTheme = LIGHT_GRAY;
    localObject[n] = localTheme;
    localTheme = DEBUG;
    localObject[i1] = localTheme;
    $VALUES = (Theme[])localObject;
  }
  
  private ThemeManager$Theme(int paramInt2, int paramInt3)
  {
    displayName = paramInt2;
    resId = paramInt3;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.ThemeManager.Theme
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */