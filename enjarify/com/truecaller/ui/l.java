package com.truecaller.ui;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.k;
import com.truecaller.androidactors.w;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.am;
import com.truecaller.featuretoggles.e;
import com.truecaller.ui.components.NewComboBase;
import com.truecaller.ui.components.NewComboBase.a;
import com.truecaller.ui.components.n;
import com.truecaller.util.at;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class l
  extends o
{
  private static final List w;
  protected TextView a;
  protected EditText b;
  protected EditText c;
  protected TextView d;
  protected EditText e;
  protected TextView f;
  protected NewComboBase g;
  protected TextView h;
  protected EditText i;
  private Bundle l;
  private boolean m = false;
  private MenuItem n;
  private View o;
  private Paint p;
  private int q;
  private int r;
  private com.truecaller.androidactors.f s;
  private k t;
  private com.truecaller.utils.i u;
  private com.truecaller.common.g.a v;
  private final List x;
  
  static
  {
    n[] arrayOfn = new n[8];
    n localn = new com/truecaller/ui/components/n;
    localn.<init>(2131886555);
    arrayOfn[0] = localn;
    localn = new com/truecaller/ui/components/n;
    localn.<init>(2131886562);
    arrayOfn[1] = localn;
    localn = new com/truecaller/ui/components/n;
    localn.<init>(2131886558);
    arrayOfn[2] = localn;
    localn = new com/truecaller/ui/components/n;
    localn.<init>(2131886556);
    arrayOfn[3] = localn;
    localn = new com/truecaller/ui/components/n;
    localn.<init>(2131886557);
    arrayOfn[4] = localn;
    localn = new com/truecaller/ui/components/n;
    localn.<init>(2131886554);
    arrayOfn[5] = localn;
    localn = new com/truecaller/ui/components/n;
    localn.<init>(2131886561);
    arrayOfn[6] = localn;
    localn = new com/truecaller/ui/components/n;
    localn.<init>(2131886559);
    arrayOfn[7] = localn;
    w = Arrays.asList(arrayOfn);
  }
  
  public l()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    List localList = w;
    localArrayList.<init>(localList);
    x = localArrayList;
  }
  
  private void a(TextView paramTextView, boolean paramBoolean)
  {
    int j;
    if (paramBoolean) {
      j = 2131234675;
    } else {
      j = 0;
    }
    paramTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, j, 0);
    if (paramBoolean) {
      paramBoolean = r;
    } else {
      paramBoolean = q;
    }
    paramTextView.setTextColor(paramBoolean);
  }
  
  private boolean a(boolean paramBoolean, int paramInt)
  {
    boolean bool = true;
    int j = 100;
    if (paramInt < j)
    {
      if (paramBoolean)
      {
        paramBoolean = 2131886544;
        Object[] arrayOfObject = new Object[bool];
        Integer localInteger = Integer.valueOf(paramInt);
        arrayOfObject[0] = localInteger;
        localObject = getString(paramBoolean, arrayOfObject);
        c((String)localObject);
        localObject = h;
        a((TextView)localObject, bool);
        localObject = i;
        ((EditText)localObject).requestFocus();
      }
      return false;
    }
    Object localObject = h;
    a((TextView)localObject, false);
    return bool;
  }
  
  private boolean b(boolean paramBoolean)
  {
    String str = e.getText().toString();
    boolean bool1 = am.c(str);
    boolean bool2 = true;
    if (!bool1)
    {
      if (paramBoolean)
      {
        paramBoolean = 2131886543;
        b(paramBoolean);
      }
      localTextView = d;
      a(localTextView, bool2);
      e.requestFocus();
      return false;
    }
    TextView localTextView = d;
    a(localTextView, false);
    return bool2;
  }
  
  private boolean c(boolean paramBoolean)
  {
    Editable localEditable = b.getText();
    int j = localEditable.length();
    boolean bool = true;
    if (j == 0)
    {
      if (paramBoolean)
      {
        paramBoolean = 2131886545;
        b(paramBoolean);
      }
      localTextView = a;
      a(localTextView, bool);
      b.requestFocus();
      return false;
    }
    TextView localTextView = a;
    a(localTextView, false);
    return bool;
  }
  
  private void d(boolean paramBoolean)
  {
    b.setFocusableInTouchMode(paramBoolean);
    b.setFocusable(paramBoolean);
    c.setFocusableInTouchMode(paramBoolean);
    c.setFocusable(paramBoolean);
    e.setFocusableInTouchMode(paramBoolean);
    e.setFocusable(paramBoolean);
    i.setFocusableInTouchMode(paramBoolean);
    i.setFocusable(paramBoolean);
    g.setFocusableInTouchMode(paramBoolean);
    g.setFocusable(paramBoolean);
    g.setClickable(paramBoolean);
  }
  
  protected final void a()
  {
    b = null;
    e = null;
    g = null;
    i = null;
    c = null;
    l = null;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    l = paramBundle;
    paramBundle = yb.v().n();
    boolean bool = paramBundle.a();
    if (bool)
    {
      paramBundle = x;
      int j = paramBundle.size() + -1;
      n localn = new com/truecaller/ui/components/n;
      int k = 2131886560;
      localn.<init>(k);
      paramBundle.add(j, localn);
    }
    paramBundle = new android/graphics/Paint;
    paramBundle.<init>();
    p = paramBundle;
    paramBundle = p;
    LightingColorFilter localLightingColorFilter = new android/graphics/LightingColorFilter;
    localLightingColorFilter.<init>(0, 16777215);
    paramBundle.setColorFilter(localLightingColorFilter);
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    boolean bool = s();
    if (bool)
    {
      int j = 2131623953;
      paramMenuInflater.inflate(j, paramMenu);
      int k = 2131363070;
      paramMenu = paramMenu.findItem(k);
      n = paramMenu;
    }
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    int j = 1;
    setHasOptionsMenu(j);
    getActivity().setTitle(2131886563);
    Object localObject = l();
    localObject = paramLayoutInflater.inflate(2131558429, (ViewGroup)localObject, false);
    o = ((View)localObject);
    localObject = o;
    Paint localPaint = p;
    ((View)localObject).setLayerType(j, localPaint);
    return paramLayoutInflater.inflate(2131559198, paramViewGroup, false);
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    boolean bool1 = super.onOptionsItemSelected(paramMenuItem);
    boolean bool3 = true;
    if (bool1) {
      return bool3;
    }
    int k = paramMenuItem.getItemId();
    int j = 2131363070;
    Object localObject1 = null;
    if (k == j)
    {
      boolean bool4 = m;
      if (!bool4)
      {
        bool4 = c(bool3);
        if (bool4)
        {
          bool4 = b(bool3);
          if (bool4)
          {
            paramMenuItem = g.getSelection();
            getActivity();
            int i1 = j;
            j = 2131886555;
            if (i1 == j)
            {
              b(2131886549);
              paramMenuItem = f;
              a(paramMenuItem, bool3);
              g.requestFocus();
              i1 = 0;
              paramMenuItem = null;
            }
            else
            {
              paramMenuItem = f;
              a(paramMenuItem, false);
              i1 = 1;
            }
            if (i1 != 0)
            {
              paramMenuItem = i;
              i1 = paramMenuItem.length();
              bool5 = a(bool3, i1);
              if (bool5)
              {
                bool5 = true;
                break label187;
              }
            }
          }
        }
        boolean bool5 = false;
        paramMenuItem = null;
        label187:
        if (bool5)
        {
          paramMenuItem = getActivity();
          Object localObject2 = u;
          boolean bool2 = ((com.truecaller.utils.i)localObject2).a();
          if (!bool2)
          {
            paramMenuItem = requireContext();
            com.truecaller.utils.extensions.i.a(paramMenuItem);
          }
          else
          {
            m = bool3;
            d(false);
            localObject2 = n;
            localObject1 = o;
            ((MenuItem)localObject2).setActionView((View)localObject1);
            localObject2 = new android/os/Bundle;
            ((Bundle)localObject2).<init>();
            onSaveInstanceState((Bundle)localObject2);
            localObject1 = ((bk)getActivity().getApplication()).a().ai();
            Object localObject3 = s.a();
            Object localObject4 = localObject3;
            localObject4 = (com.truecaller.network.util.f)localObject3;
            String str1 = ((Bundle)localObject2).getString("FeedbackFormFragment.STATE_NAME");
            String str2 = ((Bundle)localObject2).getString("FeedbackFormFragment.STATE_EMAIL");
            String str3 = ((Bundle)localObject2).getString("FeedbackFormFragment.STATE_SUBJECT");
            String str4 = ((Bundle)localObject2).getString("FeedbackFormFragment.STATE_FEEDBACK");
            String str5 = ((com.truecaller.common.f.c)localObject1).k();
            localObject2 = ((com.truecaller.network.util.f)localObject4).a(str1, str2, str3, str4, str5);
            localObject1 = t.a();
            localObject3 = new com/truecaller/ui/-$$Lambda$l$pMdxBasbDIb5gH7IYwpAUmlRCfA;
            ((-..Lambda.l.pMdxBasbDIb5gH7IYwpAUmlRCfA)localObject3).<init>(this, paramMenuItem);
            ((w)localObject2).a((com.truecaller.androidactors.i)localObject1, (ac)localObject3);
          }
        }
      }
      return bool3;
    }
    return false;
  }
  
  public final void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    Object localObject = b.getText().toString();
    paramBundle.putString("FeedbackFormFragment.STATE_NAME", (String)localObject);
    localObject = e.getText().toString();
    paramBundle.putString("FeedbackFormFragment.STATE_EMAIL", (String)localObject);
    localObject = i.getText().toString();
    paramBundle.putString("FeedbackFormFragment.STATE_FEEDBACK", (String)localObject);
    localObject = g.getSelection();
    android.support.v4.app.f localf = getActivity();
    localObject = ((n)localObject).a(localf);
    paramBundle.putString("FeedbackFormFragment.STATE_SUBJECT", (String)localObject);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = (TextView)paramView.findViewById(2131363064);
    a = paramBundle;
    paramBundle = (EditText)paramView.findViewById(2131363063);
    b = paramBundle;
    paramBundle = (EditText)paramView.findViewById(2131363067);
    c = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131363060);
    d = paramBundle;
    paramBundle = (EditText)paramView.findViewById(2131363059);
    e = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131363066);
    f = paramBundle;
    paramBundle = (NewComboBase)paramView.findViewById(2131363065);
    g = paramBundle;
    paramBundle = (TextView)paramView.findViewById(2131363062);
    h = paramBundle;
    int j = 2131363061;
    paramView = (EditText)paramView.findViewById(j);
    i = paramView;
    paramView = getActivity();
    paramBundle = com.truecaller.common.b.a.F();
    boolean bool1 = paramBundle.p();
    if (!bool1)
    {
      paramView.finish();
      return;
    }
    paramView = ((bk)paramBundle).a();
    paramBundle = paramView.m();
    t = paramBundle;
    paramBundle = paramView.az();
    s = paramBundle;
    paramBundle = paramView.v();
    u = paramBundle;
    paramView = paramView.I();
    v = paramView;
    int i1 = a.getTextColors().getDefaultColor();
    q = i1;
    i1 = getResources().getColor(2131100636);
    r = i1;
    paramView = getContext();
    i1 = com.truecaller.utils.ui.b.a(paramView, 2130969591);
    paramBundle = getContext();
    int k = 2130969592;
    j = com.truecaller.utils.ui.b.a(paramBundle, k);
    Object localObject1 = l;
    int i2 = 2131363653;
    Object localObject3;
    if (localObject1 == null)
    {
      localObject1 = b;
      localObject2 = com.truecaller.profile.c.b(v);
      ((EditText)localObject1).setText((CharSequence)localObject2);
      localObject1 = e;
      localObject2 = v;
      localObject3 = "profileEmail";
      localObject2 = ((com.truecaller.common.g.a)localObject2).a((String)localObject3);
      ((EditText)localObject1).setText((CharSequence)localObject2);
      localObject1 = at.c(g, i2);
      ((TextView)localObject1).setTextColor(j);
    }
    else
    {
      localObject2 = b;
      localObject1 = ((Bundle)localObject1).getString("FeedbackFormFragment.STATE_NAME");
      ((EditText)localObject2).setText((CharSequence)localObject1);
      localObject1 = e;
      localObject2 = l.getString("FeedbackFormFragment.STATE_EMAIL");
      ((EditText)localObject1).setText((CharSequence)localObject2);
      localObject1 = i;
      localObject2 = l.getString("FeedbackFormFragment.STATE_FEEDBACK");
      ((EditText)localObject1).setText((CharSequence)localObject2);
      localObject1 = l.getString("FeedbackFormFragment.STATE_SUBJECT");
      localObject2 = g;
      localObject3 = new com/truecaller/ui/components/n;
      ((n)localObject3).<init>((String)localObject1, null);
      ((NewComboBase)localObject2).setSelection((n)localObject3);
      localObject2 = (n)x.get(0);
      localObject3 = getActivity();
      localObject2 = ((n)localObject2).a((Context)localObject3);
      boolean bool2 = ((String)localObject2).equals(localObject1);
      if (bool2)
      {
        localObject1 = at.c(g, i2);
        ((TextView)localObject1).setTextColor(j);
      }
    }
    long l1 = ((bk)getActivity().getApplication()).a().I().a("profileUserId", 0L);
    Object localObject2 = c;
    localObject1 = String.valueOf(l1);
    ((EditText)localObject2).setText((CharSequence)localObject1);
    localObject1 = g;
    Object localObject4 = x;
    ((NewComboBase)localObject1).setData((List)localObject4);
    g.setFocusableInTouchMode(true);
    g.requestFocus();
    localObject1 = g;
    localObject4 = new com/truecaller/ui/-$$Lambda$l$epmMsMMAf5iJbHw6f37gsbIceH0;
    ((-..Lambda.l.epmMsMMAf5iJbHw6f37gsbIceH0)localObject4).<init>(this, j, i1);
    ((NewComboBase)localObject1).setObserver((NewComboBase.a)localObject4);
    paramView = b;
    paramBundle = new com/truecaller/ui/l$1;
    paramBundle.<init>(this);
    paramView.addTextChangedListener(paramBundle);
    paramView = e;
    paramBundle = new com/truecaller/ui/l$2;
    paramBundle.<init>(this);
    paramView.addTextChangedListener(paramBundle);
    paramView = i;
    paramBundle = new com/truecaller/ui/l$3;
    paramBundle.<init>(this);
    paramView.addTextChangedListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */