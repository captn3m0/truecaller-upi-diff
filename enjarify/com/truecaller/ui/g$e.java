package com.truecaller.ui;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

final class g$e
{
  public final View a;
  public final ImageView b;
  public final TextView c;
  
  private g$e(View paramView)
  {
    a = paramView;
    ImageView localImageView = (ImageView)paramView.findViewById(2131363301);
    b = localImageView;
    paramView = (TextView)paramView.findViewById(2131363578);
    c = paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.g.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */