package com.truecaller.ui;

import java.util.ArrayList;
import java.util.Collection;

public final class d$f
  extends d.a
{
  public final Collection a;
  
  public d$f(Collection paramCollection)
  {
    a = paramCollection;
  }
  
  public final boolean a(d paramd, ArrayList paramArrayList)
  {
    paramArrayList.clear();
    Collection localCollection = a;
    paramArrayList.addAll(localCollection);
    paramd.notifyDataSetChanged();
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.d.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */