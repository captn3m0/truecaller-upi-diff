package com.truecaller.ui;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.widget.TextView;
import com.truecaller.analytics.b;
import com.truecaller.content.c.ab;
import com.truecaller.content.c.ag;
import com.truecaller.notifications.y;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

final class QaOtpListActivity$a
  extends AsyncTask
{
  WeakReference a;
  private y b;
  private int c;
  
  QaOtpListActivity$a(QaOtpListActivity paramQaOtpListActivity, y paramy)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramQaOtpListActivity);
    a = localWeakReference;
    b = paramy;
  }
  
  private List a()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = (QaOtpListActivity)a.get();
    Object localObject2 = ag.b();
    Object localObject3 = g;
    localObject2 = ag.a((Context)localObject1, (ab[])localObject2, (b)localObject3).getWritableDatabase();
    localObject3 = "msg_entities";
    String str = "message_id desc";
    localObject1 = ((SQLiteDatabase)localObject2).query((String)localObject3, null, null, null, null, null, str);
    if (localObject1 != null) {
      try
      {
        boolean bool1 = ((Cursor)localObject1).moveToFirst();
        if (bool1)
        {
          int i = ((Cursor)localObject1).getCount();
          c = i;
          boolean bool2;
          do
          {
            localObject2 = "content";
            i = ((Cursor)localObject1).getColumnIndex((String)localObject2);
            localObject2 = ((Cursor)localObject1).getString(i);
            localObject3 = b;
            localObject2 = ((y)localObject3).a((String)localObject2);
            if (localObject2 != null) {
              localArrayList.add(localObject2);
            }
            bool2 = ((Cursor)localObject1).moveToNext();
          } while (bool2);
        }
      }
      finally
      {
        if (localObject1 != null) {
          ((Cursor)localObject1).close();
        }
      }
    }
    if (localObject1 != null) {
      ((Cursor)localObject1).close();
    }
    return localList;
  }
  
  protected final void onPreExecute()
  {
    super.onPreExecute();
    Object localObject = (QaOtpListActivity)a.get();
    if (localObject != null)
    {
      localObject = QaOtpListActivity.a((QaOtpListActivity)localObject);
      String str = "Loading...";
      ((TextView)localObject).setText(str);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.QaOtpListActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */