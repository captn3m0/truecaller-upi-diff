package com.truecaller.ui.dialogs;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import c.d.c;
import c.g.b.v.b;
import c.n;
import c.o.b;
import c.t;
import c.x;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.o;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Participant.a;
import com.truecaller.messaging.transport.im.a.g;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.e;

final class m$b
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag e;
  
  m$b(m paramm, int paramInt, ProgressDialog paramProgressDialog, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/ui/dialogs/m$b;
    m localm = b;
    int i = c;
    ProgressDialog localProgressDialog = d;
    localb.<init>(localm, i, localProgressDialog, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = this;
    Object localObject2 = paramObject;
    Object localObject6 = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        localObject2 = e;
        localObject6 = b.getContext();
        int k = 0;
        Object localObject7 = null;
        if (localObject6 != null)
        {
          ContentResolver localContentResolver = ((Context)localObject6).getContentResolver();
          if (localContentResolver != null)
          {
            ArrayList localArrayList = new java/util/ArrayList;
            localArrayList.<init>();
            localObject6 = b.c;
            Object localObject8;
            if (localObject6 == null)
            {
              localObject8 = "messageSettings";
              c.g.b.k.a((String)localObject8);
            }
            String str = ((com.truecaller.messaging.h)localObject6).G();
            if (str == null) {
              return Integer.valueOf(0);
            }
            v.b localb = new c/g/b/v$b;
            localb.<init>();
            a = 0;
            localObject6 = b.a;
            if (localObject6 == null)
            {
              localObject8 = "fetchMessageStorage";
              c.g.b.k.a((String)localObject8);
            }
            localObject6 = (o)((com.truecaller.androidactors.f)localObject6).a();
            Object localObject9 = null;
            localObject6 = (com.truecaller.messaging.data.a.a)((o)localObject6).a(null).d();
            if (localObject6 != null)
            {
              Object localObject10 = localObject6;
              localObject10 = (Cursor)localObject6;
              localObject6 = new java/util/ArrayList;
              ((ArrayList)localObject6).<init>();
              Object localObject11 = localObject6;
              localObject11 = (Collection)localObject6;
              for (;;)
              {
                bool1 = ((Cursor)localObject10).moveToNext();
                if (!bool1) {
                  break;
                }
                localObject6 = localObject10;
                localObject6 = ((com.truecaller.messaging.data.a.a)localObject10).b();
                localObject8 = l;
                Object localObject12 = "conversation.participants";
                c.g.b.k.a(localObject8, (String)localObject12);
                boolean bool3 = com.truecaller.messaging.i.h.b((Participant[])localObject8);
                Object localObject13;
                Object localObject14;
                Object localObject15;
                Object localObject16;
                int i2;
                int i3;
                int n;
                Message localMessage;
                Integer localInteger;
                Long localLong1;
                Long localLong2;
                int i4;
                if (bool3)
                {
                  localObject8 = b.b;
                  if (localObject8 == null)
                  {
                    localObject13 = "imGroupManager";
                    c.g.b.k.a((String)localObject13);
                  }
                  localObject8 = (com.truecaller.messaging.transport.im.a.a)((com.truecaller.androidactors.f)localObject8).a();
                  localObject13 = l[0].f;
                  localObject14 = "conversation.participants[0].normalizedAddress";
                  c.g.b.k.a(localObject13, (String)localObject14);
                  localObject8 = (g)((com.truecaller.messaging.transport.im.a.a)localObject8).a((String)localObject13).d();
                  if (localObject8 != null)
                  {
                    localObject8 = (Cursor)localObject8;
                    localObject13 = localObject8;
                    localObject13 = (Closeable)localObject8;
                    try
                    {
                      localObject14 = new java/util/ArrayList;
                      ((ArrayList)localObject14).<init>();
                      localObject14 = (Collection)localObject14;
                      for (;;)
                      {
                        boolean bool4 = ((Cursor)localObject8).moveToNext();
                        if (!bool4) {
                          break;
                        }
                        localObject15 = localObject8;
                        localObject15 = (g)localObject8;
                        localObject15 = ((g)localObject15).a();
                        localObject16 = new com/truecaller/messaging/data/types/Participant$a;
                        k = 3;
                        ((Participant.a)localObject16).<init>(k);
                        localObject7 = a;
                        localObject16 = ((Participant.a)localObject16).b((String)localObject7);
                        localObject15 = a;
                        localObject15 = ((Participant.a)localObject16).d((String)localObject15);
                        localObject15 = ((Participant.a)localObject15).a();
                        localObject16 = "Participant.Builder(Part…                 .build()";
                        c.g.b.k.a(localObject15, (String)localObject16);
                        int i1 = localArrayList.size();
                        localObject7 = localArrayList;
                        localObject7 = (List)localArrayList;
                        com.truecaller.messaging.data.b.a((List)localObject7, (Participant)localObject15);
                        localObject16 = Integer.valueOf(i1);
                        localObject15 = t.a(localObject15, localObject16);
                        ((Collection)localObject14).add(localObject15);
                        k = 0;
                        localObject7 = null;
                      }
                      localObject14 = (List)localObject14;
                      c.f.b.a((Closeable)localObject13, null);
                      localObject14 = (Iterable)localObject14;
                      localObject8 = c.a.m.g((Iterable)localObject14);
                      if (localObject8 == null)
                      {
                        localObject2 = null;
                        break label835;
                      }
                      boolean bool5 = ((List)localObject8).isEmpty();
                      if (!bool5)
                      {
                        i2 = c;
                        i3 = 0;
                        localObject14 = null;
                        while (i3 < i2)
                        {
                          localObject15 = (n)m.a(b, (List)localObject8);
                          localObject16 = (Participant)a;
                          localObject15 = (Number)b;
                          n = ((Number)localObject15).intValue();
                          localObject7 = f;
                          bool2 = c.g.b.k.a(localObject7, str);
                          localMessage = m.a(b, (Participant)localObject16, bool2);
                          localObject16 = localArrayList;
                          localObject16 = (List)localArrayList;
                          localInteger = Integer.valueOf(n);
                          bool2 = i2;
                          long l1 = a;
                          localLong1 = c.d.b.a.b.a(l1);
                          localLong2 = null;
                          i4 = 36;
                          i5 = m.a((List)localObject16, localMessage, null, localLong1, localInteger, null, i4);
                          localObject13 = m.e(b);
                          m.a((List)localObject16, (Entity)localObject13, i5);
                          i5 = a + 1;
                          a = i5;
                          i3 += 1;
                          localObject9 = null;
                        }
                      }
                      localObject1 = localObject11;
                    }
                    finally
                    {
                      localObject9 = localObject3;
                      try
                      {
                        throw ((Throwable)localObject3);
                      }
                      finally
                      {
                        c.f.b.a((Closeable)localObject13, (Throwable)localObject9);
                      }
                    }
                  }
                  localObject5 = null;
                  label835:
                  return Integer.valueOf(0);
                }
                localObject8 = l;
                int m = localObject8.length;
                int i5 = 1;
                if (m == i5)
                {
                  localObject8 = l;
                  localObject12 = "conversation.participants";
                  c.g.b.k.a(localObject8, (String)localObject12);
                  localObject8 = (CharSequence)bd;
                  if (localObject8 != null)
                  {
                    m = ((CharSequence)localObject8).length();
                    if (m != 0)
                    {
                      m = 0;
                      localObject8 = null;
                      break label930;
                    }
                  }
                  m = 1;
                  label930:
                  if (m == 0)
                  {
                    localObject8 = l;
                    localObject12 = "conversation.participants";
                    c.g.b.k.a(localObject8, (String)localObject12);
                    localObject8 = (Participant)c.a.f.b((Object[])localObject8);
                    i5 = c;
                    i2 = 0;
                    localObject13 = null;
                    while (i2 < i5)
                    {
                      localObject14 = b;
                      c.g.b.k.a(localObject8, "participant");
                      localMessage = m.a((m)localObject14, (Participant)localObject8);
                      localObject14 = localArrayList;
                      localObject14 = (List)localArrayList;
                      localLong2 = c.d.b.a.b.a(b);
                      long l2 = a;
                      localLong1 = c.d.b.a.b.a(l2);
                      localInteger = null;
                      i4 = 20;
                      n = m.a((List)localObject14, localMessage, null, localLong1, null, localLong2, i4);
                      localObject16 = m.e(b);
                      m.a((List)localObject14, (Entity)localObject16, n);
                      i3 = a;
                      n = 1;
                      i3 += n;
                      a = i3;
                      i2 += 1;
                    }
                  }
                }
                int j = localArrayList.size();
                m = 100;
                if (j >= m)
                {
                  m.a(localContentResolver, localArrayList);
                  localObject7 = m.b(b);
                  localObject9 = new com/truecaller/ui/dialogs/m$b$a;
                  m = 0;
                  localObject8 = null;
                  localObject6 = localObject9;
                  localObject12 = this;
                  localObject13 = localObject5;
                  localObject14 = localArrayList;
                  localObject15 = str;
                  localObject16 = localb;
                  localObject1 = localObject11;
                  localObject11 = localContentResolver;
                  ((m.b.a)localObject9).<init>(null, this, (ag)localObject5, localArrayList, str, localb, localContentResolver);
                  localObject9 = (c.g.a.m)localObject9;
                  j = 2;
                  e.b((ag)localObject5, (c.d.f)localObject7, (c.g.a.m)localObject9, j);
                }
                else
                {
                  localObject1 = localObject11;
                }
                localObject6 = x.a;
                ((Collection)localObject1).add(localObject6);
                localObject11 = localObject1;
                localObject1 = this;
                boolean bool2 = false;
                localObject7 = null;
                localObject9 = null;
              }
            }
            m.a(localContentResolver, localArrayList);
            return Integer.valueOf(a);
          }
        }
        return Integer.valueOf(0);
      }
      throw a;
    }
    Object localObject5 = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject5).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)localObject5);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.m.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */