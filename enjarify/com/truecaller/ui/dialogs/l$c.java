package com.truecaller.ui.dialogs;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.google.c.a.g;
import com.truecaller.common.h.aa;
import com.truecaller.data.entity.Contact;
import com.truecaller.network.search.j;
import com.truecaller.network.search.j.b;
import com.truecaller.notifications.SourcedContact;
import com.truecaller.notifications.SourcedContactListActivity;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.UUID;

class l$c
{
  final Map b;
  final String[] c;
  int d;
  
  private l$c(l paraml, String... paramVarArgs)
  {
    paraml = new java/util/LinkedHashMap;
    paraml.<init>();
    b = paraml;
    d = 0;
    c = paramVarArgs;
  }
  
  private void a(Context paramContext, int paramInt)
  {
    if (paramInt >= 0)
    {
      localObject = c;
      int i = localObject.length;
      if (paramInt < i) {}
    }
    else
    {
      a(null);
    }
    Object localObject = c;
    CharSequence localCharSequence = localObject[paramInt];
    boolean bool = TextUtils.isEmpty(localCharSequence);
    if (bool) {
      a(null);
    }
    try
    {
      aa.a(localCharSequence);
    }
    catch (g localg)
    {
      a(null);
    }
    localObject = new com/truecaller/network/search/j;
    UUID localUUID = UUID.randomUUID();
    ((j)localObject).<init>(paramContext, localUUID, "notification");
    i = localCharSequence;
    paramContext = ((j)localObject).a();
    paramInt = 1;
    b = paramInt;
    d = paramInt;
    e = paramInt;
    f = false;
    h = 19;
    localObject = new com/truecaller/ui/dialogs/l$c$1;
    ((l.c.1)localObject).<init>(this);
    paramContext.a(null, false, false, (j.b)localObject);
  }
  
  final void a()
  {
    int i = d;
    String[] arrayOfString = c;
    int j = arrayOfString.length;
    if (i < j)
    {
      localObject = e.getContext();
      j = d;
      a((Context)localObject, j);
      return;
    }
    Object localObject = b;
    a((Map)localObject);
  }
  
  final void a(Contact paramContact)
  {
    int i = 1;
    if (paramContact != null)
    {
      Object localObject1 = paramContact.t();
      boolean bool = TextUtils.isEmpty((CharSequence)localObject1);
      if (!bool)
      {
        localObject1 = new com/truecaller/notifications/SourcedContact;
        String str1 = "org.telegram.messenger";
        String str2 = "Random label";
        Long localLong = paramContact.getId();
        String str3 = paramContact.getTcId();
        String str4 = paramContact.t();
        Object localObject2 = c;
        int j = d;
        String str5 = localObject2[j];
        Uri localUri1 = paramContact.a(false);
        Uri localUri2 = paramContact.a(i);
        localObject2 = localObject1;
        ((SourcedContact)localObject1).<init>(str1, str2, localLong, str3, str4, str5, localUri1, localUri2);
        localObject2 = b;
        ((Map)localObject2).put(localObject1, paramContact);
      }
    }
    int k = d + i;
    d = k;
    a();
  }
  
  void a(Map paramMap)
  {
    l locall = e;
    Context localContext = locall.getContext();
    LinkedHashSet localLinkedHashSet = new java/util/LinkedHashSet;
    paramMap = paramMap.keySet();
    localLinkedHashSet.<init>(paramMap);
    paramMap = SourcedContactListActivity.a(localContext, localLinkedHashSet);
    locall.startActivity(paramMap);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.l.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */