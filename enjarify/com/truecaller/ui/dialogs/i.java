package com.truecaller.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.util.al;
import java.util.Map;

public final class i
  extends AppCompatDialog
{
  private final al a;
  private final Map b;
  
  public i(Context paramContext, al paramal, Map paramMap)
  {
    super(paramContext);
    a = paramal;
    b = paramMap;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = 1;
    supportRequestWindowFeature(i);
    setCanceledOnTouchOutside(i);
    setContentView(2131558577);
    int j = 2131364884;
    Object localObject1 = (TextView)findViewById(j);
    Context localContext = null;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = getContext();
      int k = 2131888600;
      Object[] arrayOfObject = new Object[i];
      int m = b.size();
      Integer localInteger = Integer.valueOf(m);
      arrayOfObject[0] = localInteger;
      localObject2 = (CharSequence)((Context)localObject2).getString(k, arrayOfObject);
      ((TextView)localObject1).setText((CharSequence)localObject2);
    }
    j = 2131364093;
    localObject1 = (RecyclerView)findViewById(j);
    if (localObject1 != null)
    {
      k.a(localObject1, "recyclerView");
      localObject2 = new android/support/v7/widget/LinearLayoutManager;
      Object localObject3 = getContext();
      ((LinearLayoutManager)localObject2).<init>((Context)localObject3, i, false);
      localObject2 = (RecyclerView.LayoutManager)localObject2;
      ((RecyclerView)localObject1).setLayoutManager((RecyclerView.LayoutManager)localObject2);
      paramBundle = new com/truecaller/ui/dialogs/h;
      localContext = getContext();
      k.a(localContext, "context");
      localObject2 = a;
      localObject3 = b;
      paramBundle.<init>(localContext, (al)localObject2, (Map)localObject3);
      paramBundle = (RecyclerView.Adapter)paramBundle;
      ((RecyclerView)localObject1).setAdapter(paramBundle);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */