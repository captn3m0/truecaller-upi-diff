package com.truecaller.ui.dialogs;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;
import c.d.a.a;
import c.d.c;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

final class m$a$1
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag d;
  
  m$a$1(m.a parama, int paramInt, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/ui/dialogs/m$a$1;
    m.a locala = b;
    int i = c;
    local1.<init>(locala, i, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        b.f.dismiss();
        paramObject = b.c.getContext();
        if (paramObject != null)
        {
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          int j = c;
          ((StringBuilder)localObject).append(j);
          String str = " messages added";
          ((StringBuilder)localObject).append(str);
          localObject = (CharSequence)((StringBuilder)localObject).toString();
          j = 1;
          paramObject = Toast.makeText((Context)paramObject, (CharSequence)localObject, j);
          ((Toast)paramObject).show();
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.m.a.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */