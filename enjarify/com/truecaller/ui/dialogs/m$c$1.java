package com.truecaller.ui.dialogs;

import android.app.ProgressDialog;
import c.d.c;
import c.g.a.m;
import c.g.b.v.b;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

final class m$c$1
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  m$c$1(m.c paramc, v.b paramb, c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/ui/dialogs/m$c$1;
    m.c localc = b;
    v.b localb = c;
    local1.<init>(localc, localb, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = b.e;
        int j = c.a;
        ((ProgressDialog)paramObject).setProgress(j);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.m.c.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */