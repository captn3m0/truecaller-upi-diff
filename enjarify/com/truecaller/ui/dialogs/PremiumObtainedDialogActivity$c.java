package com.truecaller.ui.dialogs;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.truecaller.common.h.ai;

final class PremiumObtainedDialogActivity$c
  implements View.OnClickListener
{
  PremiumObtainedDialogActivity$c(PremiumObtainedDialogActivity paramPremiumObtainedDialogActivity, String paramString) {}
  
  public final void onClick(View paramView)
  {
    paramView = a;
    Object localObject = paramView.getString(2131886653);
    String str = a.getString(2131887144);
    CharSequence localCharSequence = (CharSequence)b;
    localObject = ai.a((String)localObject, str, localCharSequence);
    paramView.startActivity((Intent)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.PremiumObtainedDialogActivity.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */