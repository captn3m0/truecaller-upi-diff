package com.truecaller.ui.dialogs;

import android.content.Context;
import com.truecaller.data.entity.Contact;
import com.truecaller.notifications.SourcedContact;
import com.truecaller.ui.CallMeBackActivity;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

final class l$2
  extends l.c
{
  l$2(l paraml, String... paramVarArgs)
  {
    super(paraml, paramVarArgs, (byte)0);
  }
  
  final void a(Map paramMap)
  {
    boolean bool = paramMap.isEmpty();
    if (!bool)
    {
      Object localObject = (SourcedContact)paramMap.keySet().iterator().next();
      paramMap = (Contact)paramMap.get(localObject);
      l locall = a;
      Context localContext = locall.getContext();
      localObject = f;
      int i = 1;
      String str = "callMeBackPopupOutApp";
      paramMap = CallMeBackActivity.a(localContext, paramMap, (String)localObject, i, str);
      locall.startActivity(paramMap);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.l.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */