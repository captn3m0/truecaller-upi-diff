package com.truecaller.ui.dialogs;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.ai;
import com.truecaller.util.at;
import com.truecaller.util.cb;
import org.c.a.a.a.k;

public final class o
  extends AppCompatDialog
  implements View.OnClickListener
{
  public boolean a = false;
  private Button b;
  private Uri c;
  private String d;
  private final String e;
  private final String f;
  private final String g;
  
  public o(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    super(paramContext);
    e = paramString1;
    f = paramString2;
    g = paramString3;
  }
  
  public final void onClick(View paramView)
  {
    int i = paramView.getId();
    int j = 2131362844;
    Uri localUri = null;
    if (i != j)
    {
      j = 2131362849;
      if (i == j)
      {
        i = 1;
        a = i;
        Context localContext = getContext();
        String str1 = localContext.getString(2131887308);
        paramView = new Object[i];
        String str2 = d;
        paramView[0] = str2;
        paramView = localContext.getString(2131887307, paramView);
        localUri = c;
        ai.a(localContext, null, str1, paramView, localUri);
        dismiss();
      }
    }
    else
    {
      a = false;
      dismiss();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2131558602);
    paramBundle = (Button)findViewById(2131362849);
    b = paramBundle;
    int i = 2131363824;
    Object localObject1 = (TextView)findViewById(i);
    int k = 2131363786;
    Object localObject2 = (TextView)findViewById(k);
    int m = 2131362844;
    Button localButton = (Button)findViewById(m);
    b.setEnabled(false);
    String str1 = k.l(e);
    String str2 = f;
    at.a((TextView)localObject1, str2);
    at.b((TextView)localObject2, str1);
    localObject1 = getLayoutInflater();
    int n = 2131559234;
    str2 = null;
    localObject1 = ((LayoutInflater)localObject1).inflate(n, null);
    localObject2 = f;
    at.a((View)localObject1, i, (CharSequence)localObject2);
    at.a((View)localObject1, k, str1);
    paramBundle = g;
    boolean bool = TextUtils.isEmpty(paramBundle);
    if (!bool)
    {
      int j = 2131363761;
      Object localObject3 = (TextView)findViewById(j);
      localObject2 = g;
      ((TextView)localObject3).setText((CharSequence)localObject2);
      localObject3 = g;
      at.a((View)localObject1, j, (CharSequence)localObject3);
    }
    new o.1(this, (View)localObject1);
    b.setOnClickListener(this);
    localButton.setOnClickListener(this);
    paramBundle = ((bk)getContext().getApplicationContext()).a().bB();
    localObject1 = f;
    paramBundle = paramBundle.a((String)localObject1);
    d = paramBundle;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */