package com.truecaller.ui.dialogs;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnCloseListener;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.libraries.nbu.engagementrewards.api.EngagementRewardsConstants.RewardsEnvironment;
import com.inmobi.sdk.InMobiSdk;
import com.inmobi.sdk.InMobiSdk.LogLevel;
import com.truecaller.TrueApp;
import com.truecaller.ads.qa.QaCampaignsActivity;
import com.truecaller.ads.qa.QaKeywordsActivity;
import com.truecaller.aftercall.AfterCallPromotionActivity;
import com.truecaller.aftercall.PromotionType;
import com.truecaller.analytics.AppHeartBeatTask;
import com.truecaller.analytics.AppSettingsTask;
import com.truecaller.analytics.InstalledAppsHeartbeatWorker;
import com.truecaller.analytics.InstalledAppsHeartbeatWorker.a;
import com.truecaller.analytics.ae;
import com.truecaller.backup.BackupLogWorker;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.edge.EdgeLocationsWorker;
import com.truecaller.common.edge.EdgeLocationsWorker.a;
import com.truecaller.common.h.u;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.tag.sync.AvailableTagsDownloadWorker;
import com.truecaller.common.tag.sync.TagKeywordsDownloadWorker;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.n;
import com.truecaller.content.c.ab;
import com.truecaller.content.c.ad;
import com.truecaller.content.c.ag;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.HistoryEvent;
import com.truecaller.data.entity.Number;
import com.truecaller.engagementrewards.EngagementRewardActionType;
import com.truecaller.engagementrewards.EngagementRewardState;
import com.truecaller.feature_toggles.control_panel.FeaturesControlPanelActivity;
import com.truecaller.filters.FilterManager;
import com.truecaller.log.UnmutedException.e;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.Conversation.a;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.old.data.access.Settings;
import com.truecaller.old.data.access.Settings.BuildName;
import com.truecaller.old.data.entity.Notification.NotificationState;
import com.truecaller.presence.AvailabilityTrigger;
import com.truecaller.profile.BusinessProfileOnboardingActivity;
import com.truecaller.referral.ReferralManager;
import com.truecaller.referral.am;
import com.truecaller.referral.an;
import com.truecaller.remote_explorer.activities.PreferenceClientActivity;
import com.truecaller.remote_explorer.server.HttpServerService;
import com.truecaller.service.AlarmReceiver;
import com.truecaller.service.AlarmReceiver.AlarmType;
import com.truecaller.service.MissedCallsNotificationService;
import com.truecaller.service.RefreshContactIndexingService;
import com.truecaller.service.RefreshContactIndexingService.a;
import com.truecaller.service.RefreshT9MappingService;
import com.truecaller.service.SyncPhoneBookService;
import com.truecaller.ui.QaOtpListActivity;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.components.ComboBase;
import com.truecaller.ui.components.ComboBase.a;
import com.truecaller.ui.components.n;
import com.truecaller.util.ai;
import com.truecaller.util.at;
import com.truecaller.util.bl;
import com.truecaller.util.bt;
import com.truecaller.util.co;
import java.io.File;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Executor;
import kotlinx.coroutines.ao;
import okhttp3.y;

public final class l
  extends AppCompatDialogFragment
  implements View.OnClickListener
{
  private final com.truecaller.premium.data.m A;
  private final com.truecaller.androidactors.f B;
  private final com.truecaller.calling.a.e C;
  private final com.truecaller.androidactors.f D;
  private final com.truecaller.engagementrewards.ui.d E;
  private final com.truecaller.filters.v a;
  private final FilterManager b;
  private final com.truecaller.data.entity.g c;
  private final com.truecaller.i.c d;
  private final com.truecaller.i.e e;
  private final com.truecaller.common.g.a f;
  private final com.truecaller.androidactors.f g;
  private final com.truecaller.notificationchannels.e h;
  private final com.truecaller.notificationchannels.b i;
  private final com.truecaller.notifications.a j;
  private com.truecaller.aftercall.a k;
  private ReferralManager l;
  private final com.truecaller.f.a m;
  private final com.truecaller.flashsdk.core.b n;
  private final com.truecaller.common.f.c o;
  private final com.truecaller.common.edge.a p;
  private final com.truecaller.common.h.ac q;
  private final com.truecaller.whoviewedme.w r;
  private final com.truecaller.notificationchannels.p s;
  private final com.truecaller.ads.provider.f t;
  private final com.truecaller.i.a u;
  private final com.truecaller.messaging.h v;
  private final com.truecaller.utils.l w;
  private final com.truecaller.featuretoggles.e x;
  private final com.truecaller.clevertap.l y;
  private final bt z;
  
  public l()
  {
    Object localObject1 = TrueApp.y().a();
    Object localObject2 = yb;
    Object localObject3 = ((bp)localObject1).Q();
    a = ((com.truecaller.filters.v)localObject3);
    localObject3 = ((bp)localObject1).P();
    b = ((FilterManager)localObject3);
    localObject3 = ((bp)localObject1).aa();
    c = ((com.truecaller.data.entity.g)localObject3);
    localObject3 = ((bp)localObject1).D();
    d = ((com.truecaller.i.c)localObject3);
    localObject3 = ((bp)localObject1).F();
    e = ((com.truecaller.i.e)localObject3);
    localObject3 = ((bp)localObject1).I();
    f = ((com.truecaller.common.g.a)localObject3);
    localObject3 = ((bp)localObject1).Y();
    k = ((com.truecaller.aftercall.a)localObject3);
    localObject3 = ((bp)localObject1).p();
    g = ((com.truecaller.androidactors.f)localObject3);
    localObject3 = ((bp)localObject1).aC();
    h = ((com.truecaller.notificationchannels.e)localObject3);
    localObject3 = ((bp)localObject1).aD();
    i = ((com.truecaller.notificationchannels.b)localObject3);
    localObject3 = ((bp)localObject1).W();
    j = ((com.truecaller.notifications.a)localObject3);
    localObject3 = ((bp)localObject1).aS();
    m = ((com.truecaller.f.a)localObject3);
    localObject3 = ((bp)localObject1).aV();
    n = ((com.truecaller.flashsdk.core.b)localObject3);
    localObject3 = ((bp)localObject1).ai();
    o = ((com.truecaller.common.f.c)localObject3);
    localObject3 = ((com.truecaller.common.a)localObject2).e();
    p = ((com.truecaller.common.edge.a)localObject3);
    localObject2 = ((com.truecaller.common.a)localObject2).n();
    q = ((com.truecaller.common.h.ac)localObject2);
    localObject2 = ((bp)localObject1).bk();
    r = ((com.truecaller.whoviewedme.w)localObject2);
    localObject2 = ((bp)localObject1).aB();
    s = ((com.truecaller.notificationchannels.p)localObject2);
    localObject2 = ((bp)localObject1).aq();
    t = ((com.truecaller.ads.provider.f)localObject2);
    localObject2 = ((bp)localObject1).as();
    u = ((com.truecaller.i.a)localObject2);
    localObject2 = ((bp)localObject1).C();
    v = ((com.truecaller.messaging.h)localObject2);
    localObject2 = ((bp)localObject1).bw();
    w = ((com.truecaller.utils.l)localObject2);
    localObject2 = ((bp)localObject1).aF();
    x = ((com.truecaller.featuretoggles.e)localObject2);
    localObject2 = ((bp)localObject1).aM();
    y = ((com.truecaller.clevertap.l)localObject2);
    localObject2 = ((bp)localObject1).bC();
    z = ((bt)localObject2);
    localObject2 = ((bp)localObject1).al();
    A = ((com.truecaller.premium.data.m)localObject2);
    localObject2 = ((bp)localObject1).aZ();
    B = ((com.truecaller.androidactors.f)localObject2);
    localObject2 = ((bp)localObject1).ct().a();
    C = ((com.truecaller.calling.a.e)localObject2);
    localObject2 = ((bp)localObject1).ae();
    D = ((com.truecaller.androidactors.f)localObject2);
    localObject1 = ((bp)localObject1).k();
    E = ((com.truecaller.engagementrewards.ui.d)localObject1);
  }
  
  private com.truecaller.callerid.i a(String paramString, int paramInt1, int paramInt2)
  {
    Contact localContact = new com/truecaller/data/entity/Contact;
    localContact.<init>();
    localContact.l("Sample contact");
    Object localObject = new com/truecaller/data/entity/Number;
    ((Number)localObject).<init>(paramString);
    localContact.a((Number)localObject);
    localObject = c;
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramString;
    Number localNumber = ((com.truecaller.data.entity.g)localObject).b(arrayOfString);
    com.truecaller.callerid.i locali = new com/truecaller/callerid/i;
    long l1 = System.currentTimeMillis();
    com.truecaller.filters.g localg = b.a(paramString);
    localObject = locali;
    locali.<init>(paramInt1, paramInt2, localNumber, 0, false, l1, localContact, null, localg);
    return locali;
  }
  
  private static String a()
  {
    try
    {
      Object localObject1 = NetworkInterface.getNetworkInterfaces();
      localObject1 = Collections.list((Enumeration)localObject1);
      localObject1 = ((List)localObject1).iterator();
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (bool1)
      {
        Object localObject2 = ((Iterator)localObject1).next();
        localObject2 = (NetworkInterface)localObject2;
        localObject2 = ((NetworkInterface)localObject2).getInetAddresses();
        localObject2 = Collections.list((Enumeration)localObject2);
        localObject2 = ((List)localObject2).iterator();
        Object localObject3;
        int i1;
        do
        {
          boolean bool3;
          do
          {
            boolean bool2 = ((Iterator)localObject2).hasNext();
            if (!bool2) {
              break;
            }
            localObject3 = ((Iterator)localObject2).next();
            localObject3 = (InetAddress)localObject3;
            bool3 = ((InetAddress)localObject3).isLoopbackAddress();
          } while (bool3);
          localObject3 = ((InetAddress)localObject3).getHostAddress();
          i1 = 58;
          i1 = ((String)localObject3).indexOf(i1);
          if (i1 < 0) {
            i1 = 1;
          } else {
            i1 = 0;
          }
        } while (i1 == 0);
        return (String)localObject3;
      }
    }
    catch (Exception localException) {}
    return "";
  }
  
  private static void a(boolean paramBoolean)
  {
    an localan = new com/truecaller/referral/an;
    localan.<init>();
    String str1 = "featureSearchBarIcon";
    String str2 = "featureInboxOverflow";
    String str3 = "featureContactDetail";
    String str4 = "featureContacts";
    String str5 = "featureUserBusyPrompt";
    String str6 = "featureAftercall";
    String str7 = "featureAftercallSaveContact";
    String str8 = "featureGoPro";
    String str9 = "featurePushNotification";
    String str10 = "featureReferralDeeplink";
    String str11 = "featureReferralAfterCallPromo";
    String[] tmp66_63 = new String[13];
    String[] tmp67_66 = tmp66_63;
    String[] tmp67_66 = tmp66_63;
    tmp67_66[0] = "featureReferralNavigationDrawer";
    tmp67_66[1] = "featureReferralDeeplink";
    String[] tmp78_67 = tmp67_66;
    String[] tmp78_67 = tmp67_66;
    tmp78_67[2] = str1;
    tmp78_67[3] = str2;
    String[] tmp85_78 = tmp78_67;
    String[] tmp85_78 = tmp78_67;
    tmp85_78[4] = str3;
    tmp85_78[5] = str4;
    String[] tmp94_85 = tmp85_78;
    String[] tmp94_85 = tmp85_78;
    tmp94_85[6] = str5;
    tmp94_85[7] = str6;
    String[] tmp105_94 = tmp94_85;
    String[] tmp105_94 = tmp94_85;
    tmp105_94[8] = str7;
    tmp105_94[9] = str8;
    String[] tmp116_105 = tmp105_94;
    String[] tmp116_105 = tmp105_94;
    tmp116_105[10] = str9;
    tmp116_105[11] = str10;
    tmp116_105[12] = str11;
    String[] arrayOfString = tmp116_105;
    int i1 = 0;
    for (;;)
    {
      int i2 = 13;
      if (i1 >= i2) {
        break;
      }
      str1 = arrayOfString[i1];
      localan.a(str1, paramBoolean);
      i1 += 1;
    }
  }
  
  private void b()
  {
    d.d("lastCallMadeWithTcTime");
    d.d("lastDialerPromotionTime");
  }
  
  private void c()
  {
    try
    {
      Object localObject1 = getContext();
      Object localObject2 = "test.db";
      localObject1 = ((Context)localObject1).getDatabasePath((String)localObject2);
      localObject1 = ((File)localObject1).getParentFile();
      localObject2 = "android.os.FileUtils";
      localObject2 = Class.forName((String)localObject2);
      Object localObject3 = "setPermissions";
      int i1 = 4;
      Class[] arrayOfClass = new Class[i1];
      Class localClass = String.class;
      arrayOfClass[0] = localClass;
      localClass = Integer.TYPE;
      int i2 = 1;
      arrayOfClass[i2] = localClass;
      localClass = Integer.TYPE;
      int i3 = 2;
      arrayOfClass[i3] = localClass;
      localClass = Integer.TYPE;
      int i4 = 3;
      arrayOfClass[i4] = localClass;
      localObject2 = ((Class)localObject2).getMethod((String)localObject3, arrayOfClass);
      localObject3 = new java/util/TreeSet;
      ((TreeSet)localObject3).<init>();
      localObject1 = ((File)localObject1).listFiles();
      int i5 = localObject1.length;
      int i6 = 0;
      localClass = null;
      while (i6 < i5)
      {
        Object localObject4 = localObject1[i6];
        try
        {
          Object localObject6 = ((File)localObject4).getName();
          Object localObject7 = ".db";
          boolean bool1 = ((String)localObject6).endsWith((String)localObject7);
          if (bool1)
          {
            bool1 = false;
            localObject6 = null;
            localObject7 = new Object[i1];
            Object localObject8 = ((File)localObject4).getAbsolutePath();
            localObject7[0] = localObject8;
            int i7 = 420;
            localObject8 = Integer.valueOf(i7);
            localObject7[i2] = localObject8;
            i7 = -1;
            Integer localInteger = Integer.valueOf(i7);
            localObject7[i3] = localInteger;
            localObject8 = Integer.valueOf(i7);
            localObject7[i4] = localObject8;
            ((Method)localObject2).invoke(null, (Object[])localObject7);
            localObject6 = ((File)localObject4).getAbsolutePath();
            ((TreeSet)localObject3).add(localObject6);
            localObject6 = new String[i2];
            localObject7 = "File permissions changed for ";
            localObject4 = String.valueOf(localObject4);
            localObject4 = ((String)localObject7).concat((String)localObject4);
            localObject6[0] = localObject4;
          }
        }
        finally
        {
          ((Throwable)localObject5).printStackTrace();
        }
        i6 += 1;
      }
      boolean bool2 = ((TreeSet)localObject3).isEmpty();
      if (!bool2)
      {
        localObject1 = getContext();
        localObject2 = new java/lang/StringBuilder;
        String str = "Permissions changed for: ";
        ((StringBuilder)localObject2).<init>(str);
        str = ",";
        localObject3 = TextUtils.join(str, (Iterable)localObject3);
        ((StringBuilder)localObject2).append((String)localObject3);
        localObject2 = ((StringBuilder)localObject2).toString();
        localObject1 = Toast.makeText((Context)localObject1, (CharSequence)localObject2, i2);
        ((Toast)localObject1).show();
      }
      return;
    }
    finally
    {
      finally;
    }
  }
  
  private void d()
  {
    AlarmReceiver.AlarmType[] arrayOfAlarmType = AlarmReceiver.AlarmType.values();
    int i1 = arrayOfAlarmType.length;
    int i2 = 0;
    while (i2 < i1)
    {
      String str = arrayOfAlarmType[i2].name();
      long l1 = 0L;
      Settings.d(str, l1);
      i2 += 1;
    }
    AlarmReceiver.a(getContext(), false);
  }
  
  private void e()
  {
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = w;
    boolean bool = ((com.truecaller.utils.l)localObject2).c();
    if (!bool)
    {
      localObject1 = new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" };
      requestPermissions((String[])localObject1, 1);
      return;
    }
    Toast.makeText((Context)localObject1, "App is exporting DB Schema...", 0).show();
    localObject2 = ai.a((Context)localObject1);
    -..Lambda.l.RXOpnNj1_UpFWeBtcoixF1oEjqE localRXOpnNj1_UpFWeBtcoixF1oEjqE = new com/truecaller/ui/dialogs/-$$Lambda$l$RXOpnNj1_UpFWeBtcoixF1oEjqE;
    localRXOpnNj1_UpFWeBtcoixF1oEjqE.<init>((Context)localObject1);
    ((ao)localObject2).a_(localRXOpnNj1_UpFWeBtcoixF1oEjqE);
  }
  
  private void f()
  {
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = w;
    boolean bool = ((com.truecaller.utils.l)localObject2).c();
    if (!bool)
    {
      localObject1 = new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" };
      requestPermissions((String[])localObject1, 2);
      return;
    }
    Toast.makeText((Context)localObject1, "App is exporting logs...", 0).show();
    localObject2 = com.truecaller.debug.log.a.a((Context)localObject1);
    -..Lambda.l.4uNUEC9kKe3ZbMNfktnEgZ2a3_g local4uNUEC9kKe3ZbMNfktnEgZ2a3_g = new com/truecaller/ui/dialogs/-$$Lambda$l$4uNUEC9kKe3ZbMNfktnEgZ2a3_g;
    local4uNUEC9kKe3ZbMNfktnEgZ2a3_g.<init>((Context)localObject1);
    ((ao)localObject2).a_(local4uNUEC9kKe3ZbMNfktnEgZ2a3_g);
  }
  
  private void g()
  {
    int i1 = Build.VERSION.SDK_INT;
    int i2 = 1;
    int i3 = 26;
    if (i1 < i3)
    {
      Toast.makeText(getContext(), "Method tracing requires Android O or above", i2).show();
      return;
    }
    Toast.makeText(getContext(), "Method tracing requires a debuggable build", i2).show();
  }
  
  private void h()
  {
    Object localObject = w;
    boolean bool = ((com.truecaller.utils.l)localObject).c();
    -..Lambda.l.Uk7KctX9iZGGfPfgfY6ovWtmN0A localUk7KctX9iZGGfPfgfY6ovWtmN0A = null;
    if (!bool)
    {
      localObject = new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" };
      requestPermissions((String[])localObject, 0);
      return;
    }
    Toast.makeText(getContext(), "App is dumping its heap. It will cause UI freeze.", 0).show();
    localObject = getView();
    localUk7KctX9iZGGfPfgfY6ovWtmN0A = new com/truecaller/ui/dialogs/-$$Lambda$l$Uk7KctX9iZGGfPfgfY6ovWtmN0A;
    localUk7KctX9iZGGfPfgfY6ovWtmN0A.<init>(this);
    ((View)localObject).postDelayed(localUk7KctX9iZGGfPfgfY6ovWtmN0A, 100);
  }
  
  public final void onClick(View paramView)
  {
    l locall = this;
    Object localObject1 = TrueApp.y().a();
    Object localObject2 = com.truecaller.common.b.a.F();
    int i1 = paramView.getId();
    int i4 = 0;
    Object localObject3 = null;
    int i5;
    int i6;
    int i7;
    Object localObject4;
    long l1;
    int i8;
    boolean bool3;
    Object localObject7;
    int i2;
    int i10;
    switch (i1)
    {
    default: 
      i5 = 2131887217;
      i6 = 2131887197;
      i7 = 0;
      localObject4 = null;
      switch (i1)
      {
      default: 
        l1 = 0L;
        double d1 = 0.0D;
        i8 = 1;
        switch (i1)
        {
        default: 
          switch (i1)
          {
          default: 
            switch (i1)
            {
            default: 
              switch (i1)
              {
              default: 
                switch (i1)
                {
                default: 
                  switch (i1)
                  {
                  default: 
                    int i9 = 2131559075;
                    switch (i1)
                    {
                    default: 
                      switch (i1)
                      {
                      default: 
                        switch (i1)
                        {
                        default: 
                          switch (i1)
                          {
                          default: 
                            boolean bool4;
                            switch (i1)
                            {
                            default: 
                              int i11 = 10015;
                              switch (i1)
                              {
                              default: 
                                switch (i1)
                                {
                                default: 
                                  break;
                                case 2131363838: 
                                  TruecallerInit.a(getActivity(), "banking", "banking");
                                  return;
                                case 2131363071: 
                                  c.b(10009);
                                  return;
                                case 2131363054: 
                                  localObject1 = new android/content/Intent;
                                  localObject2 = getContext();
                                  ((Intent)localObject1).<init>((Context)localObject2, FeaturesControlPanelActivity.class);
                                  startActivity((Intent)localObject1);
                                  return;
                                case 2131363014: 
                                  e();
                                  return;
                                case 2131362731: 
                                  localObject1 = LayoutInflater.from(getContext()).inflate(2131559077, null);
                                  localObject2 = (EditText)((View)localObject1).findViewById(2131362754);
                                  localObject5 = Settings.b("qa_voip_notification_rtm_token");
                                  ((EditText)localObject2).setText((CharSequence)localObject5);
                                  localObject5 = new android/support/v7/app/AlertDialog$Builder;
                                  localObject3 = getContext();
                                  ((AlertDialog.Builder)localObject5).<init>((Context)localObject3);
                                  localObject1 = ((AlertDialog.Builder)localObject5).setTitle("New RTM token").setView((View)localObject1);
                                  localObject5 = new com/truecaller/ui/dialogs/-$$Lambda$l$o1-9hr6nBsWnb7crVLy06DTUzsw;
                                  ((-..Lambda.l.o1-9hr6nBsWnb7crVLy06DTUzsw)localObject5).<init>(this, (EditText)localObject2);
                                  localObject1 = ((AlertDialog.Builder)localObject1).setPositiveButton(i5, (DialogInterface.OnClickListener)localObject5).setNegativeButton(i6, null);
                                  localObject5 = new com/truecaller/ui/dialogs/-$$Lambda$l$_MDFmz1ryJGgriIMIPRr2Q37txY;
                                  ((-..Lambda.l._MDFmz1ryJGgriIMIPRr2Q37txY)localObject5).<init>(this);
                                  ((AlertDialog.Builder)localObject1).setNeutralButton("Clear", (DialogInterface.OnClickListener)localObject5).show();
                                  return;
                                case 2131362701: 
                                  localObject2 = ((bp)localObject1).j();
                                  localObject5 = EngagementRewardActionType.BUY_PREMIUM_ANNUAL;
                                  localObject3 = EngagementRewardState.COMPLETED;
                                  ((com.truecaller.engagementrewards.c)localObject2).a((EngagementRewardActionType)localObject5, (EngagementRewardState)localObject3);
                                  localObject1 = ((bp)localObject1).k();
                                  ((com.truecaller.engagementrewards.ui.d)localObject1).a();
                                  break;
                                case 2131362698: 
                                  m.f("KeyCallLogPromoDisabledUntil");
                                  a(i8);
                                  return;
                                case 2131362696: 
                                  localObject1 = getContext();
                                  if (localObject1 != null)
                                  {
                                    localObject1 = "qaEnableHttpServer";
                                    bool3 = Settings.b((String)localObject1, false);
                                    if (bool3)
                                    {
                                      HttpServerService.a(getContext());
                                      return;
                                    }
                                    localObject1 = getContext();
                                    localObject2 = new android/content/Intent;
                                    localObject5 = getContext();
                                    ((Intent)localObject2).<init>((Context)localObject5, HttpServerService.class);
                                    ((Context)localObject1).stopService((Intent)localObject2);
                                    return;
                                  }
                                  break;
                                case 2131362686: 
                                  a(false);
                                  return;
                                }
                                break;
                              case 2131362791: 
                                e.b("whatsNewDialogShownRevision", 0);
                                e.b("mdauPromoShownTimes", 0);
                                e.b("mdauPromoShownTimestamp", l1);
                                e.d("whatsNewShownTimestamp");
                                return;
                              case 2131362790: 
                                localObject1 = getActivity().getSupportFragmentManager().a();
                                localObject2 = new com/truecaller/startup_dialogs/fragments/l;
                                ((com.truecaller.startup_dialogs.fragments.l)localObject2).<init>();
                                localObject5 = com.truecaller.startup_dialogs.fragments.l.class.getSimpleName();
                                ((android.support.v4.app.o)localObject1).a((Fragment)localObject2, (String)localObject5).f();
                                return;
                              case 2131362789: 
                                localObject1 = getActivity().getSupportFragmentManager().a();
                                localObject2 = new com/truecaller/startup_dialogs/fragments/g;
                                ((com.truecaller.startup_dialogs.fragments.g)localObject2).<init>();
                                localObject5 = com.truecaller.startup_dialogs.fragments.g.class.getSimpleName();
                                ((android.support.v4.app.o)localObject1).a((Fragment)localObject2, (String)localObject5).d();
                                return;
                              case 2131362788: 
                                localObject1 = new com/truecaller/old/data/access/i;
                                localObject5 = getContext();
                                ((com.truecaller.old.data.access.i)localObject1).<init>((Context)localObject5);
                                ((com.truecaller.old.data.access.i)localObject1).b();
                                c.b(i11);
                                return;
                              case 2131362787: 
                                localObject1 = com.truecaller.common.network.util.d.c();
                                ((ae)((bk)getActivity().getApplication()).a().f().a()).a((y)localObject1).c();
                                return;
                              case 2131362786: 
                                c.b(i11);
                                return;
                              case 2131362785: 
                                com.truecaller.common.b.e.b("tagsPhonebookForcedUpload", i8);
                                c.b(i11);
                                return;
                              case 2131362784: 
                                localObject1 = new android/content/Intent;
                                ((Intent)localObject1).<init>("com.google.android.gms.gcm.ACTION_TASK_READY");
                                localObject2 = getContext().getPackageName();
                                ((Intent)localObject1).setPackage((String)localObject2);
                                localObject2 = getContext().getPackageManager();
                                localObject1 = ((PackageManager)localObject2).queryIntentServices((Intent)localObject1, 0);
                                if (localObject1 != null)
                                {
                                  bool4 = ((List)localObject1).isEmpty();
                                  if (!bool4)
                                  {
                                    localObject2 = new java/util/ArrayList;
                                    ((ArrayList)localObject2).<init>();
                                    localObject1 = ((List)localObject1).iterator();
                                    for (;;)
                                    {
                                      boolean bool1 = ((Iterator)localObject1).hasNext();
                                      if (!bool1) {
                                        break;
                                      }
                                      localObject5 = (ResolveInfo)((Iterator)localObject1).next();
                                      localObject6 = new android/content/Intent;
                                      ((Intent)localObject6).<init>("com.google.android.gms.gcm.SERVICE_ACTION_INITIALIZE");
                                      localObject7 = serviceInfo.packageName;
                                      localObject5 = serviceInfo.name;
                                      ((Intent)localObject6).setClassName((String)localObject7, (String)localObject5);
                                      localObject5 = getContext().startService((Intent)localObject6);
                                      if (localObject5 == null) {
                                        i4 += 1;
                                      } else {
                                        ((List)localObject2).add(localObject5);
                                      }
                                    }
                                    localObject1 = getContext();
                                    localObject5 = new java/lang/StringBuilder;
                                    localObject6 = "Started ";
                                    ((StringBuilder)localObject5).<init>((String)localObject6);
                                    ((StringBuilder)localObject5).append(localObject2);
                                    ((StringBuilder)localObject5).append(", failed to start ");
                                    ((StringBuilder)localObject5).append(i4);
                                    ((StringBuilder)localObject5).append(" services");
                                    localObject2 = ((StringBuilder)localObject5).toString();
                                    localObject1 = Toast.makeText((Context)localObject1, (CharSequence)localObject2, i8);
                                    ((Toast)localObject1).show();
                                  }
                                }
                                return;
                              case 2131362783: 
                                localObject1 = EdgeLocationsWorker.f.a().b();
                                androidx.work.p.a().a((androidx.work.q)localObject1);
                                return;
                              case 2131362782: 
                                localObject1 = new android/support/v7/app/AlertDialog$Builder;
                                localObject2 = getContext();
                                ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
                                localObject1 = ((AlertDialog.Builder)localObject1).setTitle("Enter background task ID").setView(i9);
                                localObject2 = new com/truecaller/ui/dialogs/-$$Lambda$l$bWOt0585QvJ8i-EstHzi2R3Kovg;
                                ((-..Lambda.l.bWOt0585QvJ8i-EstHzi2R3Kovg)localObject2).<init>(this);
                                ((AlertDialog.Builder)localObject1).setPositiveButton(i5, (DialogInterface.OnClickListener)localObject2).setNegativeButton(i6, null).show();
                                return;
                              case 2131362781: 
                                AvailableTagsDownloadWorker.e();
                                return;
                              case 2131362780: 
                                TagKeywordsDownloadWorker.e();
                                return;
                              case 2131362779: 
                                ((t)g.a()).a(i8);
                                return;
                              }
                              break;
                            case 2131362775: 
                              localObject1 = new com/truecaller/ui/dialogs/l$c;
                              String[] tmp1685_1682 = new String[3];
                              String[] tmp1686_1685 = tmp1685_1682;
                              String[] tmp1686_1685 = tmp1685_1682;
                              tmp1686_1685[0] = "0731256247";
                              tmp1686_1685[1] = "0761840301";
                              tmp1686_1685[2] = "+911244130150";
                              localObject2 = tmp1686_1685;
                              ((l.c)localObject1).<init>(this, (String[])localObject2, (byte)0);
                              ((l.c)localObject1).a();
                              return;
                            case 2131362774: 
                              localObject1 = TrueApp.y().a().M();
                              localObject2 = new com/truecaller/messaging/data/types/Message$a;
                              ((Message.a)localObject2).<init>();
                              localObject5 = Entity.a("text/plain", "Your otp is 767676");
                              localObject2 = ((Message.a)localObject2).a((Entity)localObject5);
                              localObject3 = TrueApp.y().a().V();
                              localObject5 = Participant.b("46763185096", (u)localObject3, "-1");
                              c = ((Participant)localObject5);
                              localObject2 = ((Message.a)localObject2).b();
                              localObject5 = new com/truecaller/messaging/data/types/Conversation$a;
                              ((Conversation.a)localObject5).<init>();
                              localObject3 = c;
                              l.add(localObject3);
                              localObject5 = ((Conversation.a)localObject5).a();
                              localObject1 = (com.truecaller.messaging.notifications.a)((com.truecaller.androidactors.f)localObject1).a();
                              localObject2 = Collections.singletonList(localObject2);
                              localObject2 = Collections.singletonMap(localObject5, localObject2);
                              ((com.truecaller.messaging.notifications.a)localObject1).a((Map)localObject2);
                              return;
                            case 2131362773: 
                              localObject1 = new com/truecaller/callerid/g;
                              Context localContext = getContext();
                              com.truecaller.i.c localc = d;
                              com.truecaller.notificationchannels.e locale = h;
                              com.truecaller.notificationchannels.b localb = i;
                              localObject2 = j;
                              ((com.truecaller.callerid.g)localObject1).<init>(localContext, localc, locale, localb, (com.truecaller.notifications.a)localObject2);
                              localObject2 = "+123456789";
                              double d2 = Math.random();
                              l1 = 4602678819172646912L;
                              d1 = 0.5D;
                              i2 = 3;
                              i10 = d2 < d1;
                              if (i10 > 0) {
                                i5 = 1;
                              } else {
                                i5 = 3;
                              }
                              localObject2 = locall.a((String)localObject2, i2, i5);
                              ((com.truecaller.callerid.e)localObject1).a((com.truecaller.callerid.i)localObject2);
                              d2 = Math.random();
                              bool4 = d2 < d1;
                              if (bool4)
                              {
                                localObject2 = "+198765432";
                                d2 = Math.random();
                                i10 = d2 < d1;
                                if (i10 > 0) {
                                  i5 = 1;
                                } else {
                                  i5 = 3;
                                }
                                localObject2 = locall.a((String)localObject2, i2, i5);
                                ((com.truecaller.callerid.e)localObject1).a((com.truecaller.callerid.i)localObject2);
                              }
                              localObject1 = "{\n   \"e\": {\"s\":2,\"c\":1443107255,\"t\":2,\"i\":391912021},\n   \"a\": {\"v\":\"10.00\",\"u\":\"http://truecaller.com\"}\n }";
                            }
                            break;
                          }
                          break;
                        }
                        break;
                      }
                      break;
                    }
                    break;
                  }
                  break;
                }
                break;
              }
              break;
            }
            break;
          }
          break;
        }
        break;
      }
      break;
    }
    try
    {
      localObject1 = com.google.gson.q.a((String)localObject1);
      localObject1 = ((com.google.gson.l)localObject1).i();
      localObject2 = new com/truecaller/old/data/entity/Notification;
      localObject5 = Notification.NotificationState.NEW;
      ((com.truecaller.old.data.entity.Notification)localObject2).<init>((com.google.gson.o)localObject1, (Notification.NotificationState)localObject5);
      localObject1 = getContext();
      bl.a((Context)localObject1, (com.truecaller.old.data.entity.Notification)localObject2);
    }
    catch (Exception localException)
    {
      int i13;
      boolean bool5;
      int i14;
      long l2;
      boolean bool2;
      int i12;
      int i3;
      boolean bool6;
      for (;;) {}
    }
    localObject1 = new android/content/ContentValues;
    ((ContentValues)localObject1).<init>();
    Object localObject5 = Integer.valueOf(i8);
    ((ContentValues)localObject1).put("new", (Integer)localObject5);
    localObject5 = Integer.valueOf(0);
    ((ContentValues)localObject1).put("is_read", (Integer)localObject5);
    localObject2 = getContext().getContentResolver();
    localObject5 = TruecallerContract.n.a();
    Object localObject6 = "_id= (SELECT MAX(_id) FROM history WHERE type=3)";
    ((ContentResolver)localObject2).update((Uri)localObject5, (ContentValues)localObject1, (String)localObject6, null);
    MissedCallsNotificationService.a(getContext());
    localObject1 = getContext();
    i13 = 4;
    localObject5 = "qa";
    bl.a((Context)localObject1, i13, false, (String)localObject5);
    localObject1 = AlarmReceiver.AlarmType.TYPE_NOTIFICATION_ACCESS;
    localObject2 = getContext();
    localObject1 = ((AlarmReceiver.AlarmType)localObject1).getNotification((Context)localObject2);
    if (localObject1 != null)
    {
      i13 = flags & 0xFFFFFFFD;
      flags = i13;
      localObject2 = android.support.v4.app.ac.a(getContext());
      localObject5 = AlarmReceiver.AlarmType.TYPE_NOTIFICATION_ACCESS;
      i2 = ((AlarmReceiver.AlarmType)localObject5).getNotificationId();
      ((android.support.v4.app.ac)localObject2).a(null, i2, (android.app.Notification)localObject1);
    }
    localObject1 = AlarmReceiver.AlarmType.TYPE_UPDATE_SPAM;
    localObject2 = getContext();
    localObject1 = ((AlarmReceiver.AlarmType)localObject1).getNotification((Context)localObject2);
    if (localObject1 != null)
    {
      localObject2 = android.support.v4.app.ac.a(getContext());
      i2 = AlarmReceiver.AlarmType.TYPE_UPDATE_SPAM.getNotificationId();
      ((android.support.v4.app.ac)localObject2).a(null, i2, (android.app.Notification)localObject1);
      return;
      b();
      localObject1 = a("0000000000", 0, 0);
      localObject2 = com.truecaller.callerid.j.b((com.truecaller.callerid.i)localObject1);
      localObject1 = k.a((com.truecaller.callerid.i)localObject1, (HistoryEvent)localObject2);
      localObject5 = paramView.getContext();
      localObject3 = d;
      if (localObject1 == null) {
        localObject1 = PromotionType.SIGN_UP;
      }
      AfterCallPromotionActivity.a((Context)localObject5, (com.truecaller.i.c)localObject3, (PromotionType)localObject1, (HistoryEvent)localObject2);
      return;
      d.b("afterCallPromoteContactsPermissionTimestamp", l1);
      localObject1 = getContext();
      localObject2 = PromotionType.CONTACT_PERMISSION;
      AfterCallPromotionActivity.a((Context)localObject1, (PromotionType)localObject2);
      return;
      localObject1 = com.truecaller.common.b.e.a("forcedUpdate_updateType");
      bool5 = TextUtils.isEmpty((CharSequence)localObject1);
      if (bool5) {
        localObject1 = "NO_UPDATE";
      }
      localObject2 = new android/support/v7/app/AlertDialog$Builder;
      localObject5 = getContext();
      ((AlertDialog.Builder)localObject2).<init>((Context)localObject5);
      localObject1 = String.valueOf(localObject1);
      localObject1 = "Current: ".concat((String)localObject1);
      localObject1 = ((AlertDialog.Builder)localObject2).setTitle((CharSequence)localObject1);
      String[] tmp2470_2467 = new String[4];
      String[] tmp2471_2470 = tmp2470_2467;
      String[] tmp2471_2470 = tmp2470_2467;
      tmp2471_2470[0] = "No update";
      tmp2471_2470[1] = "Optional update";
      tmp2471_2470[2] = "Required update";
      String[] tmp2487_2471 = tmp2471_2470;
      tmp2487_2471[3] = "Version discontinued";
      localObject2 = tmp2487_2471;
      localObject5 = -..Lambda.l.UhErUcYo-yuXJCrxlboUx7PiN_A.INSTANCE;
      ((AlertDialog.Builder)localObject1).setItems((CharSequence[])localObject2, (DialogInterface.OnClickListener)localObject5).show();
      return;
      localObject1 = com.truecaller.common.h.o.a((Context)localObject2);
      com.truecaller.common.h.o.a((Context)localObject2, (Intent)localObject1);
      return;
      localObject1 = ThemeManager.Theme.values();
      i14 = localObject1.length;
      localObject2 = new String[i14];
      localObject5 = ThemeManager.a();
      i5 = 0;
      localObject6 = null;
      for (;;)
      {
        i6 = localObject1.length;
        if (i4 >= i6) {
          break;
        }
        localObject7 = getContext();
        localObject4 = localObject1[i4];
        i7 = displayName;
        localObject7 = ((Context)localObject7).getString(i7);
        localObject2[i4] = localObject7;
        localObject7 = localObject1[i4];
        if (localObject7 == localObject5) {
          i5 = i4;
        }
        i4 += 1;
      }
      localObject5 = new android/support/v7/app/AlertDialog$Builder;
      localObject3 = getContext();
      ((AlertDialog.Builder)localObject5).<init>((Context)localObject3);
      localObject3 = new com/truecaller/ui/dialogs/-$$Lambda$l$S7JmBLJ3ff6IUkTnAL-C8u5vHWo;
      ((-..Lambda.l.S7JmBLJ3ff6IUkTnAL-C8u5vHWo)localObject3).<init>(locall, (ThemeManager.Theme[])localObject1);
      ((AlertDialog.Builder)localObject5).setSingleChoiceItems((CharSequence[])localObject2, i5, (DialogInterface.OnClickListener)localObject3).show();
      return;
      Settings.a("madeCallsFromCallLog", false);
      return;
      localObject1 = TrueApp.y();
      bool3 = ((TrueApp)localObject1).E();
      if (bool3)
      {
        localObject1 = getContext();
        if (localObject1 != null)
        {
          localObject1 = TrueApp.y().a().bO();
          localObject2 = getContext();
          ((com.truecaller.credit.e)localObject1).a((Context)localObject2);
          return;
          c.b(20001);
          return;
          localObject1 = TrueApp.y();
          bool3 = ((TrueApp)localObject1).E();
          if (bool3)
          {
            localObject1 = getContext();
            if (localObject1 != null)
            {
              TrueApp.y().a().bO().a();
              return;
              com.truecaller.truepay.app.fcm.a.a(getContext());
              return;
              c.b(20003);
              return;
              ((com.truecaller.config.a)B.a()).b().c();
              return;
              localObject1 = TrueApp.y();
              bool3 = ((TrueApp)localObject1).E();
              if (bool3)
              {
                TrueApp.y().a().bO().b();
                return;
                localObject1 = TrueApp.y();
                bool3 = ((TrueApp)localObject1).E();
                if (bool3)
                {
                  localObject1 = getContext();
                  if (localObject1 != null)
                  {
                    localObject1 = TrueApp.y().a().bO();
                    localObject2 = getContext();
                    ((com.truecaller.credit.e)localObject1).b((Context)localObject2);
                    return;
                    c.b(20004);
                    return;
                    e.b("backupOnboardingAvailable", i8);
                    e.b("backupOnboardingShown", false);
                    localObject1 = f;
                    l2 = System.currentTimeMillis();
                    ((com.truecaller.common.g.a)localObject1).b("key_backup_fetched_timestamp", l2);
                    return;
                    f.d("featureRegion1_qa");
                    localObject1 = getView();
                    i14 = 2131362778;
                    localObject5 = q;
                    bool2 = ((com.truecaller.common.h.ac)localObject5).a();
                    at.c((View)localObject1, i14, bool2);
                    localObject1 = f;
                    localObject2 = "featureRegion1_qa";
                    ((com.truecaller.common.g.a)localObject1).d((String)localObject2);
                    break label3233;
                    localObject1 = getContext();
                    bool3 = co.a((Context)localObject1);
                    if (bool3)
                    {
                      localObject1 = TrueApp.y().a();
                      ((com.truecaller.callhistory.a)((bp)localObject1).ad().a()).a();
                      localObject2 = getContext();
                      SyncPhoneBookService.a((Context)localObject2);
                      localObject1 = (t)((bp)localObject1).p().a();
                      ((t)localObject1).a(i8);
                      bool3 = true;
                    }
                    else
                    {
                      bool3 = false;
                      localObject1 = null;
                    }
                    localObject2 = com.truecaller.ads.campaigns.f.a(getContext());
                    ((com.truecaller.ads.campaigns.e)localObject2).b();
                    if (bool3)
                    {
                      Toast.makeText(getContext(), "Provider has been reset, syncing call log and phone book", 0).show();
                      return;
                    }
                    Toast.makeText(getContext(), "Could not reset provider", i8).show();
                    return;
                    n.j();
                    return;
                    b();
                    Toast.makeText(getContext(), "Dialer promotions reset", 0).show();
                    return;
                    C.a();
                    Toast.makeText(getContext(), "Contacts settings reseted", i8).show();
                    return;
                    label3233:
                    f.d("core_agreed_region_1");
                    return;
                    d();
                    return;
                    localObject1 = new com/truecaller/ui/dialogs/l$b;
                    localObject2 = getActivity();
                    ((l.b)localObject1).<init>((Activity)localObject2);
                    localObject2 = AsyncTask.THREAD_POOL_EXECUTOR;
                    localObject5 = new Void[0];
                    ((l.b)localObject1).executeOnExecutor((Executor)localObject2, (Object[])localObject5);
                    return;
                    f.b("key_last_set_status_time", l1);
                    f.a("last_availability_update_success", null);
                    localObject1 = (com.truecaller.presence.c)D.a();
                    localObject2 = AvailabilityTrigger.USER_ACTION;
                    ((com.truecaller.presence.c)localObject1).a((AvailabilityTrigger)localObject2, i8);
                    return;
                    Settings.f("initialCallLogSyncComplete");
                    return;
                    WebView.setWebContentsDebuggingEnabled(i8);
                    Toast.makeText(getContext(), "Remote WebView debugging enabled", 0).show();
                    return;
                    localObject1 = getContext();
                    if (localObject1 != null)
                    {
                      localObject1 = new android/support/v7/app/AlertDialog$Builder;
                      localObject2 = getContext();
                      ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
                      localObject1 = ((AlertDialog.Builder)localObject1).setTitle("How to edit preferences remotely ?");
                      localObject2 = new java/lang/StringBuilder;
                      ((StringBuilder)localObject2).<init>("Connect your phone and your laptop on the same wifi.\nEnter 'http://");
                      localObject5 = a();
                      ((StringBuilder)localObject2).append((String)localObject5);
                      localObject5 = ":8080/' in your browser search bar.\nClick on the connect button in your browser.\nAccept the connection on your phone.";
                      ((StringBuilder)localObject2).append((String)localObject5);
                      localObject2 = ((StringBuilder)localObject2).toString();
                      localObject1 = ((AlertDialog.Builder)localObject1).setMessage((CharSequence)localObject2);
                      localObject2 = "OK";
                      localObject1 = ((AlertDialog.Builder)localObject1).setPositiveButton((CharSequence)localObject2, null).create();
                      ((AlertDialog)localObject1).show();
                      break label4384;
                      RefreshT9MappingService.a(paramView.getContext());
                      return;
                      s.c();
                      Toast.makeText(getContext(), "Notification channels are recreated", 0).show();
                      return;
                      localObject1 = TrueApp.y().a().c();
                      localObject5 = ag.b();
                      localObject1 = ag.a((Context)localObject2, (ab[])localObject5, (com.truecaller.analytics.b)localObject1).getWritableDatabase();
                      localObject2 = new com/truecaller/content/c/ad;
                      ((ad)localObject2).<init>();
                      ((ad)localObject2).a((SQLiteDatabase)localObject1);
                      return;
                      localObject1 = new com/truecaller/service/RefreshContactIndexingService$a;
                      localObject2 = paramView.getContext();
                      ((RefreshContactIndexingService.a)localObject1).<init>((Context)localObject2);
                      localObject2 = new android/content/Intent;
                      localObject5 = a;
                      ((Intent)localObject2).<init>((Context)localObject5, RefreshContactIndexingService.class);
                      localObject2 = ((Intent)localObject2).setAction("RefreshContactIndexingService.action.sync").putExtra("RefreshContactIndexingService.extra.rebuild_all", i8);
                      android.support.v4.app.v.a(a, RefreshContactIndexingService.class, 2131364100, (Intent)localObject2);
                      return;
                      c();
                      return;
                      c.b(10024);
                      return;
                      AsyncTask.execute(-..Lambda.l.k6EOQyAa571SG-2NmskBR3aHxd4.INSTANCE);
                      return;
                      localObject1 = PreferenceClientActivity.a(getContext());
                      startActivity((Intent)localObject1);
                      return;
                      TrueApp.y().a().B().a((Context)localObject2, 2131888934);
                      return;
                      com.truecaller.wizard.utils.i.a(getActivity());
                      return;
                      localObject1 = new android/content/Intent;
                      localObject2 = getContext();
                      ((Intent)localObject1).<init>((Context)localObject2, QaOtpListActivity.class);
                      startActivity((Intent)localObject1);
                      return;
                      localObject1 = (TelephonyManager)getContext().getSystemService("phone");
                      i12 = ((TelephonyManager)localObject1).getCallState();
                      localObject2 = "";
                      switch (i12)
                      {
                      default: 
                        break;
                      case 2: 
                        localObject2 = "Offhook";
                        break;
                      case 1: 
                        localObject2 = "Ringing";
                        break;
                      case 0: 
                        localObject2 = "Idle";
                      }
                      localObject1 = getContext();
                      localObject2 = String.valueOf(localObject2);
                      localObject2 = "Current native call state is: ".concat((String)localObject2);
                      Toast.makeText((Context)localObject1, (CharSequence)localObject2, 0).show();
                      return;
                      localObject1 = new com/truecaller/ui/dialogs/m;
                      ((m)localObject1).<init>();
                      localObject2 = getFragmentManager();
                      ((m)localObject1).show((android.support.v4.app.j)localObject2, "qa_mock_im");
                      return;
                      g();
                      return;
                      localObject1 = new android/support/v7/app/AlertDialog$Builder;
                      localObject2 = getContext();
                      ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
                      localObject1 = ((AlertDialog.Builder)localObject1).setTitle("Enter LeadGen ID").setView(i10);
                      localObject2 = new com/truecaller/ui/dialogs/-$$Lambda$l$Ba3y2NT9lE0MuQrLRj0OTYHVldU;
                      ((-..Lambda.l.Ba3y2NT9lE0MuQrLRj0OTYHVldU)localObject2).<init>(this);
                      ((AlertDialog.Builder)localObject1).setPositiveButton(i5, (DialogInterface.OnClickListener)localObject2).setNegativeButton(i6, null).show();
                      return;
                      QaKeywordsActivity.a(getActivity());
                      return;
                      localObject1 = androidx.work.p.a();
                      localObject2 = InstalledAppsHeartbeatWorker.f.a().b();
                      ((androidx.work.p)localObject1).a((androidx.work.q)localObject2);
                      return;
                      localObject1 = ((bk)getActivity().getApplicationContext()).a().aA();
                      ((com.truecaller.util.d.a)localObject1).a(2);
                      ((com.truecaller.util.d.a)localObject1).a(i8);
                      ((com.truecaller.util.d.a)localObject1).a(0);
                      return;
                      AppSettingsTask.a(c);
                      AppHeartBeatTask.c(c);
                      return;
                      f();
                      return;
                      u.b("adsFeatureHouseAdsTimeout", 1L);
                      return;
                      f.b("featureAdCtpRotation", i8);
                      Toast.makeText(getContext(), "Rotation forced until feature flag sync", 0).show();
                      return;
                      localObject2 = n;
                      localObject5 = getContext();
                      localObject1 = ((com.truecaller.flashsdk.core.b)localObject2).a((Context)localObject5, null, null, null, null, false, null);
                      startActivity((Intent)localObject1);
                      return;
                      localObject1 = getActivity().getSupportFragmentManager().a();
                      localObject2 = new com/truecaller/startup_dialogs/fragments/d;
                      ((com.truecaller.startup_dialogs.fragments.d)localObject2).<init>();
                      localObject5 = com.truecaller.startup_dialogs.fragments.d.class.getSimpleName();
                      ((android.support.v4.app.o)localObject1).a((Fragment)localObject2, (String)localObject5).d();
                      return;
                      localObject1 = getContext().getContentResolver();
                      localObject2 = TruecallerContract.b;
                      ((ContentResolver)localObject1).call((Uri)localObject2, "dump", null, null);
                      return;
                      h();
                      return;
                      f.b("core_enhancedSearchReported", false);
                      return;
                      t.d();
                      return;
                      localObject1 = new com/truecaller/log/UnmutedException$e;
                      ((UnmutedException.e)localObject1).<init>(i8, 0);
                      throw ((Throwable)localObject1);
                      "".substring(0, i8);
                      return;
                      localObject1 = paramView.getContext();
                      localObject2 = AccountManager.get((Context)localObject1);
                      i3 = 2131887458;
                      localObject5 = ((Context)localObject1).getString(i3);
                      localObject6 = ((AccountManager)localObject2).getAccountsByType((String)localObject5);
                      i6 = localObject6.length;
                      if (i6 == 0)
                      {
                        Toast.makeText((Context)localObject1, "System account does not exist", 0).show();
                        return;
                      }
                      i6 = org.c.a.a.a.j.a();
                      switch (i6)
                      {
                      default: 
                        break;
                      case 1: 
                        localObject5 = localObject6[0];
                        localObject6 = "countryCode";
                        ((AccountManager)localObject2).setUserData((Account)localObject5, (String)localObject6, null);
                        localObject2 = "country code has been removed";
                        localObject1 = Toast.makeText((Context)localObject1, (CharSequence)localObject2, 0);
                        ((Toast)localObject1).show();
                        break;
                      case 0: 
                        localObject6 = localObject6[0];
                        localObject6 = ((AccountManager)localObject2).peekAuthToken((Account)localObject6, "installation_id");
                        ((AccountManager)localObject2).invalidateAuthToken((String)localObject5, (String)localObject6);
                        Toast.makeText((Context)localObject1, "installation ID has been removed", 0).show();
                        return;
                      }
                      return;
                    }
                    label4384:
                    y.a();
                    return;
                    localObject1 = l;
                    if (localObject1 != null)
                    {
                      ((ReferralManager)localObject1).e();
                      return;
                      r.g();
                      return;
                      p.c();
                      Toast.makeText(getContext(), "Edge locations cleared", 0).show();
                      return;
                      getContext().getSharedPreferences("callMeBackNotifications", 0).edit().clear().apply();
                      return;
                      c.b(10032);
                      return;
                      QaCampaignsActivity.a(getActivity());
                      return;
                      localObject2 = ((bp)localObject1).ao();
                      localObject5 = ((bp)localObject1).bx();
                      localObject3 = new java/lang/StringBuilder;
                      ((StringBuilder)localObject3).<init>("Device: ");
                      localObject6 = ((com.truecaller.utils.d)localObject5).l();
                      ((StringBuilder)localObject3).append((String)localObject6);
                      localObject6 = "\nManufacturer: ";
                      ((StringBuilder)localObject3).append((String)localObject6);
                      localObject5 = ((com.truecaller.utils.d)localObject5).m();
                      ((StringBuilder)localObject3).append((String)localObject5);
                      ((StringBuilder)localObject3).append("\nAndroid version: ");
                      i3 = Build.VERSION.SDK_INT;
                      ((StringBuilder)localObject3).append(i3);
                      ((StringBuilder)localObject3).append("\nDevice blacklist: ");
                      localObject5 = x.ak().d();
                      localObject5 = ((com.truecaller.abtest.c)localObject2).a((String)localObject5);
                      ((StringBuilder)localObject3).append((String)localObject5);
                      ((StringBuilder)localObject3).append("\nDevice blacklist regex: ");
                      localObject5 = x.am().d();
                      localObject5 = ((com.truecaller.abtest.c)localObject2).a((String)localObject5);
                      ((StringBuilder)localObject3).append((String)localObject5);
                      ((StringBuilder)localObject3).append("\nManufacturer blacklist: ");
                      localObject5 = x.al().d();
                      localObject2 = ((com.truecaller.abtest.c)localObject2).a((String)localObject5);
                      ((StringBuilder)localObject3).append((String)localObject2);
                      ((StringBuilder)localObject3).append("\nSupported Android version: >=21\n\nFeature supported: ");
                      bool6 = ((bp)localObject1).bh().a();
                      ((StringBuilder)localObject3).append(bool6);
                      ((StringBuilder)localObject3).append("\n\n");
                      localObject1 = ((bp)localObject1).bh().toString();
                      ((StringBuilder)localObject3).append((String)localObject1);
                      localObject1 = ((StringBuilder)localObject3).toString();
                      localObject2 = getContext();
                      if (localObject2 != null)
                      {
                        localObject2 = new android/support/v7/app/AlertDialog$Builder;
                        localObject5 = getContext();
                        ((AlertDialog.Builder)localObject2).<init>((Context)localObject5);
                        ((AlertDialog.Builder)localObject2).setMessage((CharSequence)localObject1).show();
                        return;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return;
    localObject1 = new com/truecaller/ui/dialogs/l$2;
    localObject2 = new String[] { "0735342770" };
    ((l.2)localObject1).<init>(this, (String[])localObject2);
    ((l.2)localObject1).a();
    return;
    localObject1 = BusinessProfileOnboardingActivity.a(getActivity(), false);
    startActivity((Intent)localObject1);
    return;
    e.b("backupOnboardingAvailable", i8);
    e.b("backupOnboardingShown", false);
    f.b("key_backup_fetched_timestamp", l1);
    return;
    BackupLogWorker.e();
    return;
    InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);
    return;
    localObject1 = new android/support/v7/app/AlertDialog$Builder;
    localObject2 = getContext();
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    localObject1 = ((AlertDialog.Builder)localObject1).setTitle("Add top spammer").setView(2131559076);
    localObject5 = new com/truecaller/ui/dialogs/-$$Lambda$l$WJ7UBcBuN6zwRLFDjX5l-_rgpn4;
    ((-..Lambda.l.WJ7UBcBuN6zwRLFDjX5l-_rgpn4)localObject5).<init>(this);
    ((AlertDialog.Builder)localObject1).setPositiveButton(2131887235, (DialogInterface.OnClickListener)localObject5).setNegativeButton(i6, null).show();
    return;
    localObject1 = new android/support/v7/app/AlertDialog$Builder;
    localObject2 = getContext();
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    localObject1 = ((AlertDialog.Builder)localObject1).setTitle("Add edge end-point").setView(2131559074);
    localObject2 = new com/truecaller/ui/dialogs/-$$Lambda$l$_yvCF_mjlrjIxe-re9dbyGeSAj8;
    ((-..Lambda.l._yvCF_mjlrjIxe-re9dbyGeSAj8)localObject2).<init>(this);
    ((AlertDialog.Builder)localObject1).setPositiveButton(i5, (DialogInterface.OnClickListener)localObject2).setNegativeButton(i6, null).show();
    return;
    localObject1 = new com/truecaller/abtest/b;
    ((com.truecaller.abtest.b)localObject1).<init>();
    localObject2 = getFragmentManager();
    ((com.truecaller.abtest.b)localObject1).show((android.support.v4.app.j)localObject2, "");
    return;
    b.d();
    return;
    Settings.f("premiumTimestamp");
    Settings.f("premiumRenewable");
    Settings.f("premiumGraceExpiration");
    Settings.f("premiumLevel");
    f.d("premiumDuration");
    f.d("premiumLastFetchDate");
    o.a(0);
    A.a();
    return;
    f.d("profileLastName");
    return;
    f.d("profileFirstName");
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.referral.w.a(this, "ReferralManagerImpl");
    l = paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramBundle = new android/view/ContextThemeWrapper;
    Context localContext = paramLayoutInflater.getContext();
    int i1 = aresId;
    paramBundle.<init>(localContext, i1);
    return paramLayoutInflater.cloneInContext(paramBundle).inflate(2131558584, paramViewGroup, false);
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    paramArrayOfString = null;
    int i1 = 1;
    if (paramInt == 0)
    {
      paramInt = paramArrayOfInt[0];
      if (paramInt == 0)
      {
        h();
        return;
      }
      Toast.makeText(getContext(), "We need media storage access for saving heap info file. Please try again and grant permission when android will ask about it", i1).show();
      return;
    }
    if (paramInt == i1)
    {
      paramInt = paramArrayOfInt[0];
      if (paramInt == 0)
      {
        e();
        return;
      }
      Toast.makeText(getContext(), "We need media storage access for exporting DB Schema. Please try again and grant permission when android will ask about it", i1).show();
      return;
    }
    int i2 = 2;
    if (paramInt == i2)
    {
      paramInt = paramArrayOfInt[0];
      if (paramInt == 0)
      {
        f();
        return;
      }
      Toast.makeText(getContext(), "We need media storage access for exporting logs. Please try again and grant permission when android will ask about it", i1).show();
      return;
    }
    i2 = 3;
    if (paramInt == i2)
    {
      paramInt = paramArrayOfInt[0];
      if (paramInt == 0)
      {
        g();
        return;
      }
      Object localObject = getContext();
      paramArrayOfString = "We need media storage access for method tracing. Please try again and grant permission when android will ask about it";
      localObject = Toast.makeText((Context)localObject, paramArrayOfString, i1);
      ((Toast)localObject).show();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    paramBundle = new java/util/ArrayList;
    paramBundle.<init>();
    Object localObject1 = Settings.BuildName.values();
    int i1 = localObject1.length;
    Object localObject2 = null;
    int i2 = 0;
    Object localObject3 = null;
    while (i2 < i1)
    {
      localObject4 = localObject1[i2];
      localObject5 = new com/truecaller/ui/components/n;
      localObject6 = ((Settings.BuildName)localObject4).name();
      localObject7 = "";
      localObject4 = ((Settings.BuildName)localObject4).name();
      ((n)localObject5).<init>((String)localObject6, (String)localObject7, localObject4);
      paramBundle.add(localObject5);
      i2 += 1;
    }
    int i4 = 2131362732;
    Object localObject8 = "BUILD_KEY";
    paramBundle = at.a(paramView, i4, paramBundle, (String)localObject8);
    localObject1 = -..Lambda.l.WDZeFYiq-gYI2PLNzAkDXhUWmbM.INSTANCE;
    paramBundle.a((ComboBase.a)localObject1);
    paramBundle = new java/util/ArrayList;
    paramBundle.<init>();
    localObject1 = EngagementRewardState.values();
    i1 = localObject1.length;
    i2 = 0;
    localObject3 = null;
    while (i2 < i1)
    {
      localObject4 = localObject1[i2];
      localObject5 = new com/truecaller/ui/components/n;
      localObject6 = ((EngagementRewardState)localObject4).name();
      localObject7 = "";
      localObject4 = ((EngagementRewardState)localObject4).name();
      ((n)localObject5).<init>((String)localObject6, (String)localObject7, localObject4);
      paramBundle.add(localObject5);
      i2 += 1;
    }
    i4 = 2131362703;
    paramBundle = at.a(paramView, i4, paramBundle, "qa_engagement_reward_state");
    localObject1 = -..Lambda.l.x54uZRTInzYOLsjSe1_f69OwX5g.INSTANCE;
    paramBundle.a((ComboBase.a)localObject1);
    paramBundle = new com/truecaller/engagementrewards/ui/g;
    localObject1 = getContext();
    localObject8 = getFragmentManager();
    localObject3 = E;
    paramBundle.<init>((Context)localObject1, (android.support.v4.app.j)localObject8, (com.truecaller.engagementrewards.ui.d)localObject3);
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject8 = a.keySet().iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject8).hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = (String)((Iterator)localObject8).next();
      localObject4 = new com/truecaller/ui/components/n;
      localObject5 = "";
      ((n)localObject4).<init>((String)localObject3, (String)localObject5, localObject3);
      ((List)localObject1).add(localObject4);
    }
    localObject8 = at.h(paramView, 2131362702);
    ((ComboBase)localObject8).setData((List)localObject1);
    localObject1 = new com/truecaller/ui/dialogs/-$$Lambda$l$_451E4Kd0OpqmwrXiOzarjsexac;
    ((-..Lambda.l._451E4Kd0OpqmwrXiOzarjsexac)localObject1).<init>(paramBundle);
    ((ComboBase)localObject8).a((ComboBase.a)localObject1);
    paramBundle = new java/util/ArrayList;
    paramBundle.<init>();
    localObject1 = new com/truecaller/ui/components/n;
    localObject8 = EngagementRewardsConstants.RewardsEnvironment.PROD_ENVIRONMENT.name();
    Object localObject4 = EngagementRewardsConstants.RewardsEnvironment.PROD_ENVIRONMENT;
    ((n)localObject1).<init>((String)localObject8, "", localObject4);
    paramBundle.add(localObject1);
    localObject1 = new com/truecaller/ui/components/n;
    localObject8 = EngagementRewardsConstants.RewardsEnvironment.STAGING_ENVIRONMENT.name();
    localObject4 = EngagementRewardsConstants.RewardsEnvironment.STAGING_ENVIRONMENT;
    ((n)localObject1).<init>((String)localObject8, "", localObject4);
    paramBundle.add(localObject1);
    paramBundle = at.a(paramView, 2131362700, paramBundle, "qaEngagementRewardEnv");
    localObject1 = -..Lambda.l.8O0EhynwNAWI_xEzZh8SMWUYduc.INSTANCE;
    paramBundle.a((ComboBase.a)localObject1);
    boolean bool2 = Settings.e("qaForceAds");
    localObject8 = -..Lambda.l.4GlBxExP6MMmv9v2EozZqiT9Ars.INSTANCE;
    at.a(paramView, 2131362707, bool2, (CompoundButton.OnCheckedChangeListener)localObject8);
    bool2 = u.b("adsQaDisableRequests");
    localObject8 = new com/truecaller/ui/dialogs/-$$Lambda$l$_TqKXfgaVd5kVPCOWvu-WSYHlMc;
    ((-..Lambda.l._TqKXfgaVd5kVPCOWvu-WSYHlMc)localObject8).<init>(this);
    at.a(paramView, 2131362685, bool2, (CompoundButton.OnCheckedChangeListener)localObject8);
    bool2 = Settings.e("qaServer");
    localObject8 = -..Lambda.l.P-uXlQNHUz3l9w9b34gBx8XeBR8.INSTANCE;
    at.a(paramView, 2131362768, bool2, (CompoundButton.OnCheckedChangeListener)localObject8);
    bool2 = q.a();
    localObject8 = new com/truecaller/ui/dialogs/-$$Lambda$l$tBuYrs9xmoonpuhnIhfdY4eJT7c;
    ((-..Lambda.l.tBuYrs9xmoonpuhnIhfdY4eJT7c)localObject8).<init>(this);
    at.a(paramView, 2131362778, bool2, (CompoundButton.OnCheckedChangeListener)localObject8);
    bool2 = Settings.e("qaReferralFakeSendSms");
    localObject8 = -..Lambda.l.s4YmF-HLlg5xElTfp3X4dKyoTSg.INSTANCE;
    at.a(paramView, 2131362704, bool2, (CompoundButton.OnCheckedChangeListener)localObject8);
    bool2 = Settings.e("qaEnableLogging");
    localObject8 = -..Lambda.l.5Q-pE0UuGJworg7jr3x6iPz3eWQ.INSTANCE;
    at.a(paramView, 2131362710, bool2, (CompoundButton.OnCheckedChangeListener)localObject8);
    bool2 = Settings.e("qaForceShowReferral");
    localObject8 = -..Lambda.l.5KYvnD8X0QMxj44rG4OpE9k_FLM.INSTANCE;
    at.a(paramView, 2131362711, bool2, (CompoundButton.OnCheckedChangeListener)localObject8);
    bool2 = Settings.e("qaEnableDomainFronting");
    localObject8 = -..Lambda.l.rKQ9E16r1qV_NGT_aJM66vXyMuI.INSTANCE;
    at.a(paramView, 2131362695, bool2, (CompoundButton.OnCheckedChangeListener)localObject8);
    boolean bool3 = Settings.e("qaEnableHttpServer");
    localObject1 = -..Lambda.l.1AOC0BIhx2c-Lu1dW_e4ZqDlShQ.INSTANCE;
    i1 = 2131362696;
    at.a(paramView, i1, bool3, (CompoundButton.OnCheckedChangeListener)localObject1);
    bool2 = f.b("backupForceRootFolder");
    localObject3 = new com/truecaller/ui/dialogs/-$$Lambda$l$K1AiKlRzdZVx3X5XpeJ6afFZW9g;
    ((-..Lambda.l.K1AiKlRzdZVx3X5XpeJ6afFZW9g)localObject3).<init>(this);
    at.a(paramView, 2131362669, bool2, (CompoundButton.OnCheckedChangeListener)localObject3);
    bool2 = f.b("whoViewedMePBContactEnabled");
    localObject3 = new com/truecaller/ui/dialogs/-$$Lambda$l$sMz-0iMoSZChlVb4nJGwnUdiWqs;
    ((-..Lambda.l.sMz-0iMoSZChlVb4nJGwnUdiWqs)localObject3).<init>(this);
    at.a(paramView, 2131362793, bool2, (CompoundButton.OnCheckedChangeListener)localObject3);
    bool2 = f.b("whoViewedMeACSEnabled");
    localObject3 = new com/truecaller/ui/dialogs/-$$Lambda$l$xCkFe4ER4dLKVTEq4LUu6SZopls;
    ((-..Lambda.l.xCkFe4ER4dLKVTEq4LUu6SZopls)localObject3).<init>(this);
    at.a(paramView, 2131362792, bool2, (CompoundButton.OnCheckedChangeListener)localObject3);
    bool2 = f.b("featureCleverTap");
    localObject3 = new com/truecaller/ui/dialogs/-$$Lambda$l$F9bh7NsYSt64SnJ9hrXtOKixc-A;
    ((-..Lambda.l.F9bh7NsYSt64SnJ9hrXtOKixc-A)localObject3).<init>(this);
    at.a(paramView, 2131362694, bool2, (CompoundButton.OnCheckedChangeListener)localObject3);
    bool2 = z.c();
    localObject3 = new com/truecaller/ui/dialogs/-$$Lambda$l$vNuGhakMUJiO6LfYDM_LsCcsbuw;
    ((-..Lambda.l.vNuGhakMUJiO6LfYDM_LsCcsbuw)localObject3).<init>(this);
    at.a(paramView, 2131362716, bool2, (CompoundButton.OnCheckedChangeListener)localObject3);
    bool2 = z.d();
    localObject3 = new com/truecaller/ui/dialogs/-$$Lambda$l$Pue1mB6Nzs2yl0oa4reTG4_wN3Q;
    ((-..Lambda.l.Pue1mB6Nzs2yl0oa4reTG4_wN3Q)localObject3).<init>(this);
    at.a(paramView, 2131362714, bool2, (CompoundButton.OnCheckedChangeListener)localObject3);
    bool2 = z.e();
    localObject3 = new com/truecaller/ui/dialogs/-$$Lambda$l$DLCEsP32SDJ5JqGGTTyvyIXFJzk;
    ((-..Lambda.l.DLCEsP32SDJ5JqGGTTyvyIXFJzk)localObject3).<init>(this);
    at.a(paramView, 2131362717, bool2, (CompoundButton.OnCheckedChangeListener)localObject3);
    localObject3 = p;
    localObject4 = "eu";
    Object localObject5 = KnownEndpoints.MESSENGER.getKey();
    localObject3 = ((com.truecaller.common.edge.a)localObject3).a((String)localObject4, (String)localObject5);
    bool2 = "messenger-dev-se1.truecaller.com".equals(localObject3);
    localObject3 = new com/truecaller/ui/dialogs/-$$Lambda$l$4zuRlfXRpOILUlHWSlNTg-CnpR0;
    ((-..Lambda.l.4zuRlfXRpOILUlHWSlNTg-CnpR0)localObject3).<init>(this);
    at.a(paramView, 2131362715, bool2, (CompoundButton.OnCheckedChangeListener)localObject3);
    bool2 = Settings.e("qaEnableRecorderLeak");
    localObject3 = -..Lambda.l.89qNr2PHVT5lMQOOwOl1KSGLxN8.INSTANCE;
    at.a(paramView, 2131362697, bool2, (CompoundButton.OnCheckedChangeListener)localObject3);
    bool2 = Settings.e("qaEnableRewardForMonthlySubs");
    localObject3 = -..Lambda.l.oeaoYaOvfijSO2aR-oxZ8cHZeok.INSTANCE;
    at.a(paramView, 2131362699, bool2, (CompoundButton.OnCheckedChangeListener)localObject3);
    at.a(paramView, 2131362788, this);
    at.a(paramView, 2131362786, this);
    at.a(paramView, 2131362785, this);
    at.a(paramView, 2131362758, this);
    at.a(paramView, 2131362762, this);
    at.a(paramView, 2131362759, this);
    at.a(paramView, 2131362755, this);
    at.a(paramView, 2131362760, this);
    at.a(paramView, 2131362756, this);
    at.a(paramView, 2131362763, this);
    at.a(paramView, 2131362761, this);
    at.a(paramView, 2131362757, this);
    at.a(paramView, 2131362683, this);
    at.a(paramView, 2131362684, this);
    at.a(paramView, 2131362745, this);
    at.a(paramView, 2131362748, this);
    at.a(paramView, 2131362782, this);
    at.a(paramView, 2131362784, this);
    at.a(paramView, 2131362675, this);
    at.a(paramView, 2131362722, this);
    at.a(paramView, 2131362723, this);
    at.a(paramView, 2131362742, this);
    at.a(paramView, 2131362708, this);
    at.a(paramView, 2131362688, this);
    at.a(paramView, 2131362709, this);
    at.a(paramView, 2131362668, this);
    at.a(paramView, 2131363014, this);
    at.a(paramView, 2131362751, this);
    at.a(paramView, 2131362691, this);
    at.a(paramView, 2131362736, this);
    at.a(paramView, 2131362746, this);
    at.a(paramView, 2131362666, this);
    at.a(paramView, 2131362678, this);
    at.a(paramView, 2131362783, this);
    at.a(paramView, 2131362743, this);
    at.a(paramView, 2131362689, this);
    at.a(paramView, 2131362667, this);
    at.a(paramView, 2131362498, this);
    at.a(paramView, 2131362787, this);
    at.a(paramView, 2131362781, this);
    at.a(paramView, 2131362780, this);
    at.a(paramView, 2131362670, this);
    at.a(paramView, 2131362740, this);
    at.a(paramView, 2131362737, this);
    at.a(paramView, 2131362790, this);
    at.a(paramView, 2131362791, this);
    at.a(paramView, 2131362671, this);
    at.a(paramView, 2131362753, this);
    at.a(paramView, 2131362789, this);
    at.a(paramView, 2131362495, this);
    at.a(paramView, 2131362496, this);
    at.a(paramView, 2131362705, this);
    at.a(paramView, 2131362773, this);
    at.a(paramView, 2131362735, this);
    at.a(paramView, 2131362734, this);
    at.a(paramView, 2131362771, this);
    at.a(paramView, 2131362682, this);
    at.a(paramView, 2131362749, this);
    at.a(paramView, 2131362772, this);
    at.a(paramView, 2131362770, this);
    at.a(paramView, 2131362713, this);
    at.a(paramView, 2131362720, this);
    at.a(paramView, 2131362766, this);
    at.a(paramView, 2131362719, this);
    at.a(paramView, 2131362712, this);
    at.a(paramView, 2131362724, this);
    at.a(paramView, 2131362744, this);
    at.a(paramView, 2131362752, this);
    at.a(paramView, 2131362747, this);
    at.a(paramView, 2131362779, this);
    at.a(paramView, 2131362738, this);
    at.a(paramView, 2131362767, this);
    at.a(paramView, 2131362672, this);
    at.a(paramView, 2131362775, this);
    at.a(paramView, 2131362673, this);
    at.a(paramView, 2131362677, this);
    at.a(paramView, 2131363838, this);
    at.a(paramView, 2131362680, this);
    at.a(paramView, 2131362686, this);
    at.a(paramView, 2131362698, this);
    at.a(paramView, 2131362665, this);
    at.a(paramView, 2131362497, this);
    at.a(paramView, 2131363071, this);
    at.a(paramView, 2131362690, this);
    at.a(paramView, 2131363054, this);
    at.a(paramView, 2131362774, this);
    at.a(paramView, 2131362727, this);
    at.a(paramView, 2131362725, this);
    at.a(paramView, 2131362750, this);
    at.a(paramView, 2131362706, this);
    at.a(paramView, 2131362679, this);
    at.a(paramView, 2131362739, this);
    at.a(paramView, 2131362728, this);
    at.a(paramView, 2131362729, this);
    at.a(paramView, 2131362769, this);
    at.a(paramView, 2131362674, this);
    at.a(paramView, 2131362733, this);
    at.a(paramView, i1, this);
    at.a(paramView, 2131362741, this);
    at.a(paramView, 2131362681, this);
    at.a(paramView, 2131362676, this);
    at.a(paramView, 2131362731, this);
    at.a(paramView, 2131362726, this);
    int i6 = 2131362701;
    at.a(paramView, i6, this);
    paramBundle = Locale.US;
    i1 = 4;
    localObject8 = new Object[i1];
    Settings.a();
    localObject8[0] = "Release";
    int i7 = 1;
    localObject8[i7] = "10.41.6";
    int i3 = 1041006;
    localObject3 = Integer.valueOf(i3);
    int i8 = 2;
    localObject8[i8] = localObject3;
    localObject3 = f;
    long l1 = 0L;
    long l2 = ((com.truecaller.common.g.a)localObject3).a("profileUserId", l1);
    localObject3 = Long.valueOf(l2);
    int i9 = 3;
    localObject8[i9] = localObject3;
    paramBundle = String.format(paramBundle, "%s v%s(%d) | %d", (Object[])localObject8);
    int i5 = 2131362847;
    localObject1 = (TextView)paramView.findViewById(i5);
    ((TextView)localObject1).setText(paramBundle);
    paramBundle = co.b(requireContext());
    localObject8 = "Build time: %s\nDevice: %s%s";
    localObject3 = new Object[i9];
    Object localObject6 = requireContext();
    Object localObject7 = "COMPILE_TIME";
    localObject6 = com.truecaller.util.k.a((Context)localObject6, (String)localObject7);
    if (localObject6 != null)
    {
      localObject6 = new java/util/Date;
      l1 = ((Long)com.truecaller.util.k.a(requireContext(), "COMPILE_TIME")).longValue();
      ((Date)localObject6).<init>(l1);
      localObject7 = new java/text/SimpleDateFormat;
      String str = "HH:mm dd/MM/yyyy";
      Locale localLocale = Locale.US;
      ((SimpleDateFormat)localObject7).<init>(str, localLocale);
      localObject6 = ((SimpleDateFormat)localObject7).format((Date)localObject6);
    }
    else
    {
      localObject6 = "Not Available";
    }
    localObject3[0] = localObject6;
    localObject2 = com.truecaller.common.h.k.c();
    localObject3[i7] = localObject2;
    if (paramBundle == null)
    {
      paramBundle = "";
    }
    else
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("\nLat: ");
      double d1 = paramBundle.getLatitude();
      ((StringBuilder)localObject2).append(d1);
      localObject4 = "\nLon: ";
      ((StringBuilder)localObject2).append((String)localObject4);
      d1 = paramBundle.getLongitude();
      ((StringBuilder)localObject2).append(d1);
      paramBundle = ((StringBuilder)localObject2).toString();
    }
    localObject3[i8] = paramBundle;
    paramBundle = String.format((String)localObject8, (Object[])localObject3);
    ((TextView)paramView.findViewById(2131362838)).setText(paramBundle);
    paramBundle = (TextView)paramView.findViewById(2131362721);
    localObject8 = new java/lang/StringBuilder;
    ((StringBuilder)localObject8).<init>("Address : ");
    localObject2 = a();
    ((StringBuilder)localObject8).append((String)localObject2);
    ((StringBuilder)localObject8).append(":8080");
    localObject8 = ((StringBuilder)localObject8).toString();
    paramBundle.setText((CharSequence)localObject8);
    paramBundle = new com/truecaller/ui/dialogs/l$a;
    localObject8 = (ViewGroup)paramView.findViewById(2131362730);
    paramBundle.<init>(this, (ViewGroup)localObject8);
    localObject8 = (SearchView)paramView.findViewById(2131362765);
    localObject2 = new com/truecaller/ui/dialogs/-$$Lambda$l$drCTXwn42Bx4PLoqR3CB3-Tp6sk;
    ((-..Lambda.l.drCTXwn42Bx4PLoqR3CB3-Tp6sk)localObject2).<init>((TextView)localObject1);
    ((SearchView)localObject8).setOnSearchClickListener((View.OnClickListener)localObject2);
    localObject2 = new com/truecaller/ui/dialogs/-$$Lambda$l$pfRe1B8ovAi4d82zkoeMvtEbjuk;
    ((-..Lambda.l.pfRe1B8ovAi4d82zkoeMvtEbjuk)localObject2).<init>(paramBundle, (TextView)localObject1);
    ((SearchView)localObject8).setOnCloseListener((SearchView.OnCloseListener)localObject2);
    localObject1 = new com/truecaller/ui/dialogs/l$1;
    ((l.1)localObject1).<init>(this, paramBundle);
    ((SearchView)localObject8).setOnQueryTextListener((SearchView.OnQueryTextListener)localObject1);
    paramView = (NestedScrollView)paramView.findViewById(2131362764);
    paramBundle = new com/truecaller/ui/dialogs/-$$Lambda$l$6GVD6UJpx9Uo-BVlb20OP1otlHI;
    paramBundle.<init>((SearchView)localObject8);
    paramView.setOnScrollChangeListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */