package com.truecaller.ui.dialogs;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import c.g.b.k;

final class j$c
  implements View.OnTouchListener
{
  j$c(j paramj) {}
  
  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    paramView = "motionEvent";
    k.a(paramMotionEvent, paramView);
    int i = paramMotionEvent.getAction();
    if (i == 0)
    {
      a.dismiss();
      return true;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.j.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */