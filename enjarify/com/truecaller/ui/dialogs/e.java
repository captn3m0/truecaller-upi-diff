package com.truecaller.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v7.app.AlertDialog.Builder;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

public class e
  extends a
{
  protected EditText b;
  
  public final String a()
  {
    return b.getText().toString();
  }
  
  public final void a(String paramString1, String paramString2)
  {
    Bundle localBundle = getArguments();
    localBundle.putInt("layout_resource", 2131558596);
    localBundle.putInt("title_resource", 0);
    localBundle.putString("title", paramString1);
    localBundle.putInt("hint_resource", 0);
    localBundle.putString("initial_text", paramString2);
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = getArguments();
    int i = paramBundle.getInt("layout_resource");
    String str1 = "title_resource";
    int j = paramBundle.getInt(str1);
    paramBundle = paramBundle.getString("title");
    Object localObject1 = View.inflate(getActivity(), i, null);
    Object localObject2 = (EditText)((View)localObject1).findViewById(2131363827);
    b = ((EditText)localObject2);
    localObject2 = getArguments();
    Object localObject3 = b;
    String str2 = ((Bundle)localObject2).getString("initial_text");
    ((EditText)localObject3).setText(str2);
    localObject3 = b;
    boolean bool = true;
    ((EditText)localObject3).setSelectAllOnFocus(bool);
    localObject3 = "hint_resource";
    int k = ((Bundle)localObject2).getInt((String)localObject3);
    if (k > 0)
    {
      localObject3 = b;
      ((EditText)localObject3).setHint(k);
    }
    localObject2 = new android/support/v7/app/AlertDialog$Builder;
    localObject3 = getActivity();
    ((AlertDialog.Builder)localObject2).<init>((Context)localObject3);
    localObject1 = ((AlertDialog.Builder)localObject2).setView((View)localObject1);
    localObject3 = new com/truecaller/ui/dialogs/-$$Lambda$e$42NBOhAKXvNk37I7DhdMO77IM5M;
    ((-..Lambda.e.42NBOhAKXvNk37I7DhdMO77IM5M)localObject3).<init>(this);
    localObject1 = ((AlertDialog.Builder)localObject1).setNegativeButton(2131887197, (DialogInterface.OnClickListener)localObject3);
    k = 2131887217;
    localObject3 = new com/truecaller/ui/dialogs/-$$Lambda$e$nMFKRqqQNdKnvrtS6WL3GQwzbHA;
    ((-..Lambda.e.nMFKRqqQNdKnvrtS6WL3GQwzbHA)localObject3).<init>(this);
    localObject1 = ((AlertDialog.Builder)localObject1).setPositiveButton(k, (DialogInterface.OnClickListener)localObject3);
    if (j > 0) {
      ((AlertDialog.Builder)localObject1).setTitle(j);
    } else if (paramBundle != null) {
      ((AlertDialog.Builder)localObject1).setTitle(paramBundle);
    }
    paramBundle = ((AlertDialog.Builder)localObject1).create();
    paramBundle.getWindow().setSoftInputMode(5);
    return paramBundle;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */