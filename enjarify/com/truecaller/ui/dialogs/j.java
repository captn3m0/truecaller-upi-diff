package com.truecaller.ui.dialogs;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.c;

public final class j
  extends Dialog
  implements DialogInterface.OnShowListener, View.OnClickListener
{
  private Animator a;
  private final Uri b;
  private final ImageView c;
  
  public j(Context paramContext, Uri paramUri, ImageView paramImageView)
  {
    super(paramContext);
    b = paramUri;
    c = paramImageView;
  }
  
  private static Animator a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    Object localObject1 = new float[1];
    localObject1[0] = 0.0F;
    localObject1 = ValueAnimator.ofFloat((float[])localObject1);
    Object localObject2 = new com/truecaller/ui/dialogs/j$a;
    ((j.a)localObject2).<init>(paramView, paramInt1, paramInt2, paramInt3, paramInt4);
    localObject2 = (ValueAnimator.AnimatorUpdateListener)localObject2;
    ((ValueAnimator)localObject1).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject2);
    c.g.b.k.a(localObject1, "animator");
    return (Animator)localObject1;
  }
  
  private final void a(View paramView, boolean paramBoolean, Animator.AnimatorListener paramAnimatorListener)
  {
    int i = paramView.getWidth();
    int j = paramView.getHeight();
    int k = 2;
    Object localObject1 = new int[k];
    paramView.getLocationOnScreen((int[])localObject1);
    ImageView localImageView1 = c;
    int m = localImageView1.getWidth();
    Object localObject2 = c;
    int n = ((ImageView)localObject2).getHeight();
    int[] arrayOfInt = new int[k];
    ImageView localImageView2 = c;
    localImageView2.getLocationOnScreen(arrayOfInt);
    int i1 = i * i / 4;
    int i2 = j * j / 4;
    i1 += i2;
    double d = Math.sqrt(i1);
    float f1 = (float)d;
    i2 = 0;
    int i3 = arrayOfInt[0];
    m /= k;
    i3 += m;
    int i4 = localObject1[0];
    i /= k;
    i4 += i;
    i3 -= i4;
    i4 = 1;
    int i5 = arrayOfInt[i4];
    n /= k;
    i5 += n;
    int i6 = localObject1[i4];
    j /= k;
    i6 += j;
    i5 -= i6;
    float f2;
    if (paramBoolean)
    {
      localObject1 = new float[k];
      Object tmp224_222 = localObject1;
      tmp224_222[0] = 0.2F;
      tmp224_222[1] = 1.0F;
      localObject3 = ObjectAnimator.ofFloat(paramView, "alpha", (float[])localObject1);
      c.g.b.k.a(localObject3, "ObjectAnimator.ofFloat(view, \"alpha\", 0.2f, 1.0f)");
      localObject3 = (Animator)localObject3;
      localObject1 = new android/view/animation/AccelerateInterpolator;
      ((AccelerateInterpolator)localObject1).<init>();
      localObject1 = (TimeInterpolator)localObject1;
      ((Animator)localObject3).setInterpolator((TimeInterpolator)localObject1);
      localObject1 = a(paramView, i3, i5, 0, 0);
      localObject2 = new android/view/animation/DecelerateInterpolator;
      ((DecelerateInterpolator)localObject2).<init>();
      localObject2 = (TimeInterpolator)localObject2;
      ((Animator)localObject1).setInterpolator((TimeInterpolator)localObject2);
      f2 = m;
      paramView = ViewAnimationUtils.createCircularReveal(paramView, i, j, f2, f1);
      c.g.b.k.a(paramView, "ViewAnimationUtils.creat… 2).toFloat(), maxRadius)");
      localObject4 = new android/view/animation/AccelerateInterpolator;
      j = 1073741824;
      f3 = 2.0F;
      ((AccelerateInterpolator)localObject4).<init>(f3);
      localObject4 = (TimeInterpolator)localObject4;
      paramView.setInterpolator((TimeInterpolator)localObject4);
    }
    else
    {
      localObject1 = new float[k];
      Object tmp385_383 = localObject1;
      tmp385_383[0] = 1.0F;
      tmp385_383[1] = 0.2F;
      localObject3 = ObjectAnimator.ofFloat(paramView, "alpha", (float[])localObject1);
      c.g.b.k.a(localObject3, "ObjectAnimator.ofFloat(view, \"alpha\", 1.0f, 0.2f)");
      localObject3 = (Animator)localObject3;
      localObject1 = new android/view/animation/DecelerateInterpolator;
      ((DecelerateInterpolator)localObject1).<init>();
      localObject1 = (TimeInterpolator)localObject1;
      ((Animator)localObject3).setInterpolator((TimeInterpolator)localObject1);
      localObject1 = a(paramView, 0, 0, i3, i5);
      localObject2 = new android/view/animation/AccelerateInterpolator;
      ((AccelerateInterpolator)localObject2).<init>();
      localObject2 = (TimeInterpolator)localObject2;
      ((Animator)localObject1).setInterpolator((TimeInterpolator)localObject2);
      f2 = m;
      paramView = ViewAnimationUtils.createCircularReveal(paramView, i, j, f1, f2);
      c.g.b.k.a(paramView, "ViewAnimationUtils.creat…dius, (aw / 2).toFloat())");
      localObject4 = new android/view/animation/DecelerateInterpolator;
      ((DecelerateInterpolator)localObject4).<init>();
      localObject4 = (TimeInterpolator)localObject4;
      paramView.setInterpolator((TimeInterpolator)localObject4);
    }
    Object localObject4 = new android/animation/AnimatorSet;
    ((AnimatorSet)localObject4).<init>();
    j = 3;
    float f3 = 4.2E-45F;
    Animator[] arrayOfAnimator = new Animator[j];
    arrayOfAnimator[0] = localObject1;
    arrayOfAnimator[i4] = localObject3;
    arrayOfAnimator[k] = paramView;
    ((AnimatorSet)localObject4).playTogether(arrayOfAnimator);
    paramView = getContext();
    Object localObject3 = "context";
    c.g.b.k.a(paramView, (String)localObject3);
    paramView = paramView.getResources();
    paramBoolean = 17694720;
    int i7 = paramView.getInteger(paramBoolean);
    long l = i7;
    ((AnimatorSet)localObject4).setDuration(l);
    if (paramAnimatorListener != null) {
      ((AnimatorSet)localObject4).addListener(paramAnimatorListener);
    }
    ((AnimatorSet)localObject4).start();
    localObject4 = (Animator)localObject4;
    a = ((Animator)localObject4);
  }
  
  public final void dismiss()
  {
    boolean bool = com.truecaller.common.h.k.d();
    if (bool)
    {
      Object localObject1 = a;
      if (localObject1 != null)
      {
        if (localObject1 == null) {
          c.g.b.k.a();
        }
        bool = ((Animator)localObject1).isStarted();
        if (bool) {
          return;
        }
      }
      localObject1 = findViewById(2131363323);
      c.g.b.k.a(localObject1, "findViewById(R.id.image)");
      Object localObject2 = new com/truecaller/ui/dialogs/j$b;
      ((j.b)localObject2).<init>(this);
      localObject2 = (Animator.AnimatorListener)localObject2;
      a((View)localObject1, false, (Animator.AnimatorListener)localObject2);
      return;
    }
    super.dismiss();
  }
  
  public final void onClick(View paramView)
  {
    String str = "v";
    c.g.b.k.b(paramView, str);
    int i = paramView.getId();
    int j = 2131363323;
    if (i == j) {
      dismiss();
    }
  }
  
  protected final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = 1;
    requestWindowFeature(i);
    Object localObject1 = getWindow();
    if (localObject1 != null)
    {
      localObject2 = new android/graphics/drawable/ColorDrawable;
      f = 0.0F;
      localObject3 = null;
      ((ColorDrawable)localObject2).<init>(0);
      localObject2 = (Drawable)localObject2;
      ((Window)localObject1).setBackgroundDrawable((Drawable)localObject2);
    }
    setCanceledOnTouchOutside(i);
    localObject1 = getWindow();
    if (localObject1 != null)
    {
      localObject1 = ((Window)localObject1).getDecorView();
      if (localObject1 != null)
      {
        localObject2 = new com/truecaller/ui/dialogs/j$c;
        ((j.c)localObject2).<init>(this);
        localObject2 = (View.OnTouchListener)localObject2;
        ((View)localObject1).setOnTouchListener((View.OnTouchListener)localObject2);
      }
    }
    setContentView(2131558579);
    int k = 2131363323;
    localObject1 = (ImageView)findViewById(k);
    boolean bool = com.truecaller.common.h.k.d();
    if (bool)
    {
      localObject2 = c.getDrawable();
      if (localObject2 != null) {
        ((ImageView)localObject1).setImageDrawable((Drawable)localObject2);
      }
    }
    Object localObject2 = w.a(getContext());
    Object localObject3 = b;
    localObject2 = ((w)localObject2).a((Uri)localObject3);
    c = i;
    int j = 2131230956;
    paramBundle = ((ab)localObject2).a(j).b(j).b();
    localObject2 = new com/truecaller/common/h/aq$c;
    localObject3 = getContext();
    c.g.b.k.a(localObject3, "context");
    float f = ((Context)localObject3).getResources().getDimension(2131166170);
    ((aq.c)localObject2).<init>(f);
    localObject2 = (ai)localObject2;
    paramBundle.a((ai)localObject2).a((ImageView)localObject1, null);
    paramBundle = this;
    paramBundle = (DialogInterface.OnShowListener)this;
    setOnShowListener(paramBundle);
    paramBundle = this;
    paramBundle = (View.OnClickListener)this;
    ((ImageView)localObject1).setOnClickListener(paramBundle);
  }
  
  public final void onShow(DialogInterface paramDialogInterface)
  {
    String str = "dialog";
    c.g.b.k.b(paramDialogInterface, str);
    boolean bool1 = com.truecaller.common.h.k.d();
    if (bool1)
    {
      int i = 2131363323;
      paramDialogInterface = findViewById(i);
      str = "findViewById(R.id.image)";
      c.g.b.k.a(paramDialogInterface, str);
      boolean bool2 = true;
      a(paramDialogInterface, bool2, null);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */