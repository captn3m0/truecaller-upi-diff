package com.truecaller.ui.dialogs;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.view.View;

final class b$a
  implements ValueAnimator.AnimatorUpdateListener
{
  private final View a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  
  b$a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    a = paramView;
    b = paramInt1;
    c = paramInt2;
    d = paramInt3;
    e = paramInt4;
  }
  
  public final void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    float f1 = paramValueAnimator.getAnimatedFraction();
    View localView = a;
    int i = b;
    float f2 = i;
    float f3 = (d - i) * f1;
    f2 += f3;
    localView.setTranslationX(f2);
    localView = a;
    i = c;
    f2 = i;
    f3 = (e - i) * f1;
    f2 += f3;
    localView.setTranslationY(f2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */