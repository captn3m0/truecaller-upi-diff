package com.truecaller.ui.dialogs;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filter.FilterResults;
import android.widget.TextView;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import com.truecaller.common.h.am;
import com.truecaller.ui.components.ComboBase;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.c.a.a.a.k;

final class l$a
  extends Filter
{
  final List a;
  private final List c;
  private final Multimap d;
  private final Map e;
  
  l$a(l paraml, ViewGroup paramViewGroup)
  {
    paraml = new java/util/ArrayList;
    int i = paramViewGroup.getChildCount();
    paraml.<init>(i);
    a = paraml;
    paraml = new java/util/ArrayList;
    i = paramViewGroup.getChildCount();
    paraml.<init>(i);
    c = paraml;
    paraml = LinkedListMultimap.create();
    d = paraml;
    paraml = new java/util/HashMap;
    paraml.<init>();
    e = paraml;
    boolean bool1 = false;
    paraml = null;
    i = 0;
    for (;;)
    {
      int k = paramViewGroup.getChildCount();
      if (i >= k) {
        break;
      }
      Object localObject1 = paramViewGroup.getChildAt(i);
      int m = ((View)localObject1).getVisibility();
      if (m == 0)
      {
        boolean bool2 = localObject1 instanceof TextView;
        Object localObject2;
        if (bool2)
        {
          localObject2 = localObject1;
          localObject2 = ((TextView)localObject1).getText().toString();
        }
        else
        {
          bool2 = localObject1 instanceof ComboBase;
          if (!bool2) {
            break label394;
          }
          localObject2 = localObject1;
          localObject2 = ((ComboBase)localObject1).getTitle();
        }
        a.add(localObject1);
        c.add(localObject2);
        Object localObject3 = (String)((View)localObject1).getTag();
        boolean bool3 = am.b((CharSequence)localObject3);
        if (!bool3)
        {
          String str = "Group";
          bool3 = ((String)localObject3).startsWith(str);
          if (bool3)
          {
            paraml = "Group";
            bool3 = k.b((CharSequence)localObject3);
            if (!bool3)
            {
              bool3 = k.b(paraml);
              if (!bool3)
              {
                bool1 = ((String)localObject3).startsWith(paraml);
                if (bool1)
                {
                  int j = 5;
                  paraml = ((String)localObject3).substring(j);
                  break label293;
                }
              }
            }
            paraml = (l)localObject3;
            label293:
            localObject3 = d;
            ((Multimap)localObject3).put(paraml, localObject1);
            localObject1 = e;
            ((Map)localObject1).put(localObject2, paraml);
          }
          else
          {
            localObject2 = "Child";
            bool2 = ((String)localObject3).startsWith((String)localObject2);
            if (bool2)
            {
              str = String.valueOf(paraml);
              localObject2 = "Child".concat(str);
              bool2 = ((String)localObject3).equals(localObject2);
              if (bool2)
              {
                localObject2 = d;
                ((Multimap)localObject2).put(paraml, localObject1);
              }
            }
          }
        }
      }
      label394:
      i += 1;
    }
  }
  
  protected final Filter.FilterResults performFiltering(CharSequence paramCharSequence)
  {
    LinkedHashSet localLinkedHashSet = new java/util/LinkedHashSet;
    localLinkedHashSet.<init>();
    Iterator localIterator = e.entrySet().iterator();
    Object localObject1;
    Object localObject2;
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (Map.Entry)localIterator.next();
      localObject2 = (String)((Map.Entry)localObject1).getKey();
      Locale localLocale = Locale.ENGLISH;
      localObject2 = ((String)localObject2).toLowerCase(localLocale);
      boolean bool3 = ((String)localObject2).contains(paramCharSequence);
      if (bool3)
      {
        localObject2 = d;
        localObject1 = ((Map.Entry)localObject1).getValue();
        localObject1 = ((Multimap)localObject2).get(localObject1);
        localLinkedHashSet.addAll((Collection)localObject1);
      }
    }
    int j = 0;
    localIterator = null;
    for (;;)
    {
      localObject1 = c;
      int i = ((List)localObject1).size();
      if (j >= i) {
        break;
      }
      localObject1 = (String)c.get(j);
      localObject2 = Locale.ENGLISH;
      localObject1 = ((String)localObject1).toLowerCase((Locale)localObject2);
      boolean bool2 = ((String)localObject1).contains(paramCharSequence);
      if (bool2)
      {
        localObject1 = a.get(j);
        localLinkedHashSet.add(localObject1);
      }
      j += 1;
    }
    paramCharSequence = new android/widget/Filter$FilterResults;
    paramCharSequence.<init>();
    j = localLinkedHashSet.size();
    count = j;
    values = localLinkedHashSet;
    return paramCharSequence;
  }
  
  protected final void publishResults(CharSequence paramCharSequence, Filter.FilterResults paramFilterResults)
  {
    paramCharSequence = new java/util/ArrayList;
    Object localObject = a;
    paramCharSequence.<init>((Collection)localObject);
    paramFilterResults = (Set)values;
    paramCharSequence.removeAll(paramFilterResults);
    paramCharSequence = paramCharSequence.iterator();
    boolean bool1;
    for (;;)
    {
      bool1 = paramCharSequence.hasNext();
      if (!bool1) {
        break;
      }
      localObject = (View)paramCharSequence.next();
      int i = 8;
      ((View)localObject).setVisibility(i);
    }
    paramCharSequence = paramFilterResults.iterator();
    for (;;)
    {
      boolean bool2 = paramCharSequence.hasNext();
      if (!bool2) {
        break;
      }
      paramFilterResults = (View)paramCharSequence.next();
      bool1 = false;
      localObject = null;
      paramFilterResults.setVisibility(0);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.l.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */