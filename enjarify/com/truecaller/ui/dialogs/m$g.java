package com.truecaller.ui.dialogs;

import android.support.design.widget.TextInputEditText;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import c.g.b.k;
import com.truecaller.R.id;

final class m$g
  implements CompoundButton.OnCheckedChangeListener
{
  m$g(m paramm) {}
  
  public final void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean)
  {
    paramCompoundButton = a;
    int i = R.id.conversations_text;
    paramCompoundButton = (TextInputEditText)paramCompoundButton.a(i);
    k.a(paramCompoundButton, "conversations_text");
    paramBoolean ^= true;
    paramCompoundButton.setEnabled(paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.m.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */