package com.truecaller.ui.dialogs;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import c.d.c;
import c.g.a.m;
import c.g.b.v.b;
import c.o.b;
import c.x;
import java.util.ArrayList;
import kotlinx.coroutines.ag;

final class m$b$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag h;
  
  m$b$a(c paramc, m.b paramb, ag paramag, ArrayList paramArrayList, String paramString, v.b paramb1, ContentResolver paramContentResolver)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/ui/dialogs/m$b$a;
    m.b localb = b;
    ag localag = c;
    ArrayList localArrayList = d;
    String str = e;
    v.b localb1 = f;
    ContentResolver localContentResolver = g;
    locala.<init>(paramc, localb, localag, localArrayList, str, localb1, localContentResolver);
    paramObject = (ag)paramObject;
    h = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = b.d;
        int j = f.a;
        ((ProgressDialog)paramObject).setProgress(j);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.m.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */