package com.truecaller.ui.dialogs;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import c.a.an;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.b.v.b;
import c.o.b;
import c.x;
import com.truecaller.messaging.data.b;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.e;

final class m$c
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag f;
  
  m$c(m paramm, int paramInt1, int paramInt2, ProgressDialog paramProgressDialog, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/ui/dialogs/m$c;
    m localm = b;
    int i = c;
    int j = d;
    ProgressDialog localProgressDialog = e;
    localc.<init>(localm, i, j, localProgressDialog, paramc);
    paramObject = (ag)paramObject;
    f = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    c localc = this;
    Object localObject1 = paramObject;
    Object localObject2 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        localObject1 = f;
        m.c(b);
        localObject2 = b.getContext();
        int j = 0;
        Object localObject3 = null;
        if (localObject2 != null)
        {
          localObject2 = ((Context)localObject2).getContentResolver();
          if (localObject2 != null)
          {
            ArrayList localArrayList = new java/util/ArrayList;
            localArrayList.<init>();
            v.b localb = new c/g/b/v$b;
            localb.<init>();
            a = 0;
            int k = c;
            int m = 0;
            while (m < k)
            {
              Object localObject4 = m.d(b);
              Object localObject5 = localArrayList;
              localObject5 = (List)localArrayList;
              int n = b.a((List)localObject5, (Participant)localObject4);
              Object localObject6 = an.a(localObject4);
              int i1 = b.a((List)localObject5, (Set)localObject6);
              int i2 = d;
              int i3 = 0;
              int i5;
              while (i3 < i2)
              {
                Object localObject7 = m.a(b, (Participant)localObject4);
                Integer localInteger1 = Integer.valueOf(n);
                Integer localInteger2 = Integer.valueOf(i1);
                int i4 = 40;
                localObject6 = localObject5;
                localObject3 = localObject5;
                i5 = m.a((List)localObject5, (Message)localObject7, localInteger2, null, localInteger1, null, i4);
                localObject7 = m.e(b);
                m.a((List)localObject5, (Entity)localObject7, i5);
                i5 = a + 1;
                a = i5;
                i3 += 1;
                j = 0;
                localObject3 = null;
              }
              j = localArrayList.size();
              int i6 = 100;
              if (j >= i6)
              {
                m.a((ContentResolver)localObject2, localArrayList);
                localObject3 = m.b(b);
                localObject4 = new com/truecaller/ui/dialogs/m$c$1;
                localObject6 = null;
                ((m.c.1)localObject4).<init>(localc, localb, null);
                localObject4 = (c.g.a.m)localObject4;
                i5 = 2;
                e.b((ag)localObject1, (f)localObject3, (c.g.a.m)localObject4, i5);
              }
              m += 1;
              j = 0;
              localObject3 = null;
            }
            m.a((ContentResolver)localObject2, localArrayList);
            return Integer.valueOf(a);
          }
        }
        return Integer.valueOf(0);
      }
      throw a;
    }
    localObject1 = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject1).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)localObject1);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.m.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */