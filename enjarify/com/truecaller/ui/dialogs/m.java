package com.truecaller.ui.dialogs;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton.OnCheckedChangeListener;
import c.g.a.b;
import c.k.i;
import com.google.c.a.g;
import com.google.c.a.m.a;
import com.truecaller.R.id;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.f;
import com.truecaller.bp;
import com.truecaller.content.TruecallerContract.u;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Reaction;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.im.ImTransportInfo.a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public final class m
  extends AppCompatDialogFragment
{
  public f a;
  public f b;
  public h c;
  private final Random d;
  private final com.google.c.a.k e;
  private final List f;
  private HashMap g;
  
  public m()
  {
    Object localObject = new java/util/Random;
    ((Random)localObject).<init>();
    d = ((Random)localObject);
    localObject = com.google.c.a.k.a();
    e = ((com.google.c.a.k)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    localObject = (List)localObject;
    f = ((List)localObject);
  }
  
  private final Message a(Participant paramParticipant, boolean paramBoolean)
  {
    Object localObject1 = paramParticipant;
    int i = paramBoolean;
    Message.a locala = new com/truecaller/messaging/data/types/Message$a;
    locala.<init>();
    locala = locala.a(paramParticipant);
    int j = 1;
    locala = locala.a(j).b(j);
    long l1 = System.currentTimeMillis();
    locala = locala.d(l1);
    l1 = System.currentTimeMillis();
    locala = locala.c(l1);
    Object localObject2 = e;
    locala = locala.c((String)localObject2);
    String str1 = d;
    int k = d.nextInt();
    localObject1 = String.valueOf(k);
    localObject2 = new com/truecaller/messaging/transport/im/ImTransportInfo$a;
    ((ImTransportInfo.a)localObject2).<init>();
    localObject1 = ((ImTransportInfo.a)localObject2).a((String)localObject1);
    b = paramBoolean;
    int m = 3;
    c = m;
    d = m;
    m = 4;
    e = m;
    f = m;
    int n = 2;
    Object localObject3;
    if (str1 == null)
    {
      j = 0;
      localObject3 = null;
    }
    else
    {
      m = d.nextInt();
      String str2 = String.valueOf(m);
      localObject2 = (CharSequence)"👍,🤣,😮,😍,😠,😢,👎";
      Object localObject4 = { "," };
      localObject2 = c.n.m.c((CharSequence)localObject2, (String[])localObject4, false, 6);
      localObject4 = d;
      int i1 = ((List)localObject2).size() - j;
      int i2 = ((Random)localObject4).nextInt(i1);
      localObject4 = ((List)localObject2).get(i2);
      Object localObject5 = localObject4;
      localObject5 = (String)localObject4;
      localObject4 = d;
      i1 = ((List)localObject2).size() - j;
      i2 = ((Random)localObject4).nextInt(i1);
      localObject2 = ((List)localObject2).get(i2);
      Object localObject6 = localObject2;
      localObject6 = (String)localObject2;
      Reaction[] arrayOfReaction = new Reaction[n];
      Object localObject7 = new com/truecaller/messaging/data/types/Reaction;
      long l2 = -1;
      long l3 = -1;
      long l4 = System.currentTimeMillis();
      localObject2 = localObject7;
      ((Reaction)localObject7).<init>(l2, l3, str1, (String)localObject5, l4, 0);
      arrayOfReaction[0] = localObject7;
      localObject2 = new com/truecaller/messaging/data/types/Reaction;
      l4 = -1;
      long l5 = -1;
      long l6 = System.currentTimeMillis();
      localObject7 = localObject2;
      ((Reaction)localObject2).<init>(l4, l5, str2, (String)localObject6, l6, 0);
      arrayOfReaction[j] = localObject2;
      localObject3 = arrayOfReaction;
    }
    localObject1 = (TransportInfo)((ImTransportInfo.a)localObject1).a((Reaction[])localObject3).a();
    localObject1 = locala.a(n, (TransportInfo)localObject1).a(i).b();
    c.g.b.k.a(localObject1, "Message.Builder()\n      …e 0)\n            .build()");
    return (Message)localObject1;
  }
  
  private final Object a(List paramList)
  {
    Random localRandom = d;
    int i = paramList.size();
    int j = localRandom.nextInt(i);
    return paramList.get(j);
  }
  
  private final String a()
  {
    for (;;)
    {
      Object localObject1 = f;
      localObject1 = (String)a((List)localObject1);
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      int i = 4;
      Object localObject3 = c.n.m.a((String)localObject1, i);
      ((StringBuilder)localObject2).append((String)localObject3);
      int j = ((String)localObject1).length() - i;
      localObject1 = i.b(0, j);
      Object localObject4 = localObject1;
      localObject4 = (Iterable)localObject1;
      localObject3 = (CharSequence)"";
      localObject1 = new com/truecaller/ui/dialogs/m$d;
      ((m.d)localObject1).<init>(this);
      Object localObject5 = localObject1;
      localObject5 = (b)localObject1;
      int k = 30;
      localObject1 = c.a.m.a((Iterable)localObject4, (CharSequence)localObject3, null, null, 0, null, (b)localObject5, k);
      ((StringBuilder)localObject2).append((String)localObject1);
      localObject1 = ((StringBuilder)localObject2).toString();
      try
      {
        localObject2 = e;
        localObject4 = e;
        localObject3 = localObject1;
        localObject3 = (CharSequence)localObject1;
        localObject4 = ((com.google.c.a.k)localObject4).a((CharSequence)localObject3, null);
        boolean bool = ((com.google.c.a.k)localObject2).c((m.a)localObject4);
        if (bool) {
          return (String)localObject1;
        }
      }
      catch (g localg) {}
    }
  }
  
  private static void a(List paramList, Reaction[] paramArrayOfReaction, int paramInt)
  {
    if (paramArrayOfReaction != null)
    {
      int i = paramArrayOfReaction.length;
      int j = 0;
      while (j < i)
      {
        Object localObject1 = paramArrayOfReaction[j];
        Object localObject2 = ContentProviderOperation.newInsert(TruecallerContract.u.a()).withValueBackReference("message_id", paramInt);
        Object localObject3 = d;
        localObject2 = ((ContentProviderOperation.Builder)localObject2).withValue("emoji", localObject3);
        long l = e;
        localObject3 = Long.valueOf(l);
        localObject2 = ((ContentProviderOperation.Builder)localObject2).withValue("send_date", localObject3);
        String str = "from_peer_id";
        localObject1 = c;
        localObject1 = ((ContentProviderOperation.Builder)localObject2).withValue(str, localObject1).build();
        localObject2 = "it";
        c.g.b.k.a(localObject1, (String)localObject2);
        paramList.add(localObject1);
        j += 1;
      }
      return;
    }
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = g;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      g = ((HashMap)localObject1);
    }
    localObject1 = g;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = g;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = TrueApp.y();
    c.g.b.k.a(paramBundle, "TrueApp.getApp()");
    paramBundle.a().a(this);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558585, paramViewGroup, false);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    c.g.b.k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.add_button;
    paramView = (Button)a(i);
    paramBundle = new com/truecaller/ui/dialogs/m$f;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    i = R.id.existing_conversations_check_box;
    paramView = (CheckBox)a(i);
    paramBundle = new com/truecaller/ui/dialogs/m$g;
    paramBundle.<init>(this);
    paramBundle = (CompoundButton.OnCheckedChangeListener)paramBundle;
    paramView.setOnCheckedChangeListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */