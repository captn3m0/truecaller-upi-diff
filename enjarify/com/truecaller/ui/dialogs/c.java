package com.truecaller.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnShowListener;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.truecaller.common.h.am;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import java.util.ArrayList;
import java.util.List;

public class c
  implements DialogInterface.OnCancelListener, DialogInterface.OnShowListener, View.OnClickListener
{
  protected final Context a;
  String b;
  String c;
  boolean d;
  public Dialog e;
  public View f;
  Object g;
  int h;
  int i;
  int j;
  int k;
  int l;
  int m;
  private final int n;
  
  public c(Context paramContext, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    this(paramContext, paramInt2, paramBoolean);
    k = paramInt1;
  }
  
  private c(Context paramContext, int paramInt, boolean paramBoolean)
  {
    a = paramContext;
    b = null;
    n = paramInt;
    d = paramBoolean;
  }
  
  private void a(int paramInt1, int paramInt2)
  {
    String str = a.getString(paramInt2);
    a(paramInt1, str);
  }
  
  private void a(int paramInt, String paramString)
  {
    View localView = f;
    TextView localTextView = (TextView)localView.findViewById(paramInt);
    if (localTextView != null)
    {
      localTextView.setText(paramString);
      boolean bool = am.a(paramString);
      int i1;
      if (bool)
      {
        bool = false;
        paramString = null;
      }
      else
      {
        i1 = 8;
      }
      localTextView.setVisibility(i1);
    }
  }
  
  private void d()
  {
    int i1 = 3;
    Object localObject = new int[i1];
    Object tmp7_6 = localObject;
    tmp7_6[0] = 2131362844;
    Object tmp12_7 = tmp7_6;
    tmp12_7[1] = 2131362843;
    tmp12_7[2] = 2131362849;
    int[] arrayOfInt = new int[i1];
    int i2 = i;
    arrayOfInt[0] = i2;
    i2 = j;
    int i3 = 1;
    arrayOfInt[i3] = i2;
    i2 = h;
    arrayOfInt[2] = i2;
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    i1 = Math.min(i1, i1);
    int i4 = 0;
    while (i4 < i1)
    {
      int i5 = localObject[i4];
      int i6 = arrayOfInt[i4];
      Button localButton1 = (Button)f.findViewById(i5);
      Button localButton2 = null;
      if (localButton1 != null)
      {
        if (i6 > 0)
        {
          localButton1.setText(i6);
          localButton1.setOnClickListener(this);
        }
        int i7;
        if (i6 > 0) {
          i7 = 0;
        } else {
          i7 = 8;
        }
        localButton1.setVisibility(i7);
        if (i6 > 0) {
          localButton2 = localButton1;
        }
      }
      if (localButton2 != null) {
        localArrayList.add(localButton2);
      }
      i4 += 1;
    }
    i1 = localArrayList.size();
    if (i1 == i3)
    {
      ((View)localArrayList.get(0)).setBackgroundResource(2131230979);
      return;
    }
    if (i1 > i3)
    {
      ((View)localArrayList.get(0)).setBackgroundResource(2131230981);
      i1 -= i3;
      localObject = (View)localArrayList.get(i1);
      int i8 = 2131230982;
      ((View)localObject).setBackgroundResource(i8);
      while (i3 < i1)
      {
        localObject = (View)localArrayList.get(i3);
        i8 = 2131230980;
        ((View)localObject).setBackgroundResource(i8);
        i3 += 1;
      }
    }
  }
  
  public final void a()
  {
    b();
    Object localObject1 = a;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = new android/app/Dialog;
    int i1 = aresId;
    ((Dialog)localObject2).<init>((Context)localObject1, i1);
    e = ((Dialog)localObject2);
    localObject1 = e.getLayoutInflater();
    localObject2 = (LinearLayout)((LayoutInflater)localObject1).inflate(2131558593, null);
    i1 = n;
    localObject1 = ((LayoutInflater)localObject1).inflate(i1, (ViewGroup)localObject2, false);
    f = ((View)localObject1);
    localObject1 = f;
    ((LinearLayout)localObject2).addView((View)localObject1);
    localObject1 = new com/truecaller/ui/dialogs/c$1;
    Context localContext = a;
    ((c.1)localObject1).<init>(this, localContext);
    e = ((Dialog)localObject1);
    localObject1 = e;
    i1 = 1;
    ((Dialog)localObject1).requestWindowFeature(i1);
    e.setContentView((View)localObject2);
    localObject1 = e.getWindow();
    localObject2 = new android/graphics/drawable/ColorDrawable;
    ((ColorDrawable)localObject2).<init>(0);
    ((Window)localObject1).setBackgroundDrawable((Drawable)localObject2);
    e.setOnCancelListener(this);
    e.setOnShowListener(this);
    localObject1 = e;
    boolean bool = d;
    ((Dialog)localObject1).setCancelable(bool);
    localObject1 = e;
    bool = d;
    ((Dialog)localObject1).setCanceledOnTouchOutside(bool);
    localObject1 = e.getWindow();
    ((Window)localObject1).setSoftInputMode(16);
    int i3 = k;
    int i2 = 2131362847;
    if (i3 != 0)
    {
      a(i2, i3);
    }
    else
    {
      localObject1 = b;
      a(i2, (String)localObject1);
    }
    i3 = l;
    i2 = 2131362838;
    if (i3 != 0)
    {
      a(i2, i3);
    }
    else
    {
      localObject1 = c;
      a(i2, (String)localObject1);
    }
    d();
    e.show();
  }
  
  public final void b()
  {
    Dialog localDialog = e;
    if (localDialog != null)
    {
      localDialog.dismiss();
      localDialog = null;
      e = null;
      f = null;
    }
  }
  
  protected void c() {}
  
  public void onCancel(DialogInterface paramDialogInterface)
  {
    if (paramDialogInterface != null)
    {
      Dialog localDialog = e;
      paramDialogInterface.equals(localDialog);
    }
  }
  
  public void onClick(View paramView)
  {
    int i1 = paramView.getId();
    int i2 = 2131362849;
    if (i1 != i2)
    {
      i2 = 2131362844;
      if (i1 == i2) {
        c();
      }
    }
    b();
  }
  
  public void onShow(DialogInterface paramDialogInterface) {}
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */