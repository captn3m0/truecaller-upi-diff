package com.truecaller.ui.dialogs;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.m;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.b.ab;
import com.d.b.ai;
import com.truecaller.TrueApp;
import com.truecaller.analytics.e.a;
import com.truecaller.bp;
import com.truecaller.common.h.aq.c;
import com.truecaller.common.h.k;
import com.truecaller.premium.PremiumPresenterView.LaunchContext;
import com.truecaller.premium.br;

public final class b
  extends Dialog
  implements DialogInterface.OnShowListener, View.OnClickListener
{
  private final int a;
  private final Uri b;
  private final ImageView c;
  private final com.d.b.e d;
  private final boolean e;
  private Animator f;
  
  public b(Context paramContext, Uri paramUri, ImageView paramImageView, int paramInt, com.d.b.e parame, boolean paramBoolean)
  {
    super(paramContext, 2131951633);
    b = paramUri;
    c = paramImageView;
    a = paramInt;
    d = parame;
    e = paramBoolean;
  }
  
  private static Animator a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    Object localObject = new float[1];
    localObject[0] = 0.0F;
    localObject = ValueAnimator.ofFloat((float[])localObject);
    b.a locala = new com/truecaller/ui/dialogs/b$a;
    locala.<init>(paramView, paramInt1, paramInt2, paramInt3, paramInt4);
    ((ValueAnimator)localObject).addUpdateListener(locala);
    return (Animator)localObject;
  }
  
  private void a(View paramView, boolean paramBoolean, Animator.AnimatorListener paramAnimatorListener)
  {
    int i = paramView.getWidth();
    int j = paramView.getHeight();
    int k = 2;
    Object localObject1 = new int[k];
    paramView.getLocationOnScreen((int[])localObject1);
    ImageView localImageView1 = c;
    int m = localImageView1.getWidth();
    Object localObject2 = c;
    int n = ((ImageView)localObject2).getHeight();
    int[] arrayOfInt = new int[k];
    ImageView localImageView2 = c;
    localImageView2.getLocationOnScreen(arrayOfInt);
    int i1 = i * i / 4;
    int i2 = j * j / 4;
    i1 += i2;
    double d1 = Math.sqrt(i1);
    float f1 = (float)d1;
    i2 = 0;
    int i3 = arrayOfInt[0];
    m /= k;
    i3 += m;
    int i4 = localObject1[0];
    i /= k;
    i4 += i;
    i3 -= i4;
    i4 = 1;
    int i5 = arrayOfInt[i4];
    n /= k;
    i5 += n;
    int i6 = localObject1[i4];
    j /= k;
    i6 += j;
    i5 -= i6;
    ObjectAnimator localObjectAnimator;
    float f2;
    if (paramBoolean)
    {
      localObject1 = new float[k];
      Object tmp224_222 = localObject1;
      tmp224_222[0] = 0.2F;
      tmp224_222[1] = 1.0F;
      localObjectAnimator = ObjectAnimator.ofFloat(paramView, "alpha", (float[])localObject1);
      localObject1 = new android/view/animation/AccelerateInterpolator;
      ((AccelerateInterpolator)localObject1).<init>();
      localObjectAnimator.setInterpolator((TimeInterpolator)localObject1);
      localObject1 = a(paramView, i3, i5, 0, 0);
      localObject2 = new android/view/animation/DecelerateInterpolator;
      ((DecelerateInterpolator)localObject2).<init>();
      ((Animator)localObject1).setInterpolator((TimeInterpolator)localObject2);
      f2 = m;
      paramView = ViewAnimationUtils.createCircularReveal(paramView, i, j, f2, f1);
      localObject3 = new android/view/animation/AccelerateInterpolator;
      j = 1073741824;
      f3 = 2.0F;
      ((AccelerateInterpolator)localObject3).<init>(f3);
      paramView.setInterpolator((TimeInterpolator)localObject3);
    }
    else
    {
      localObject1 = new float[k];
      Object tmp344_342 = localObject1;
      tmp344_342[0] = 1.0F;
      tmp344_342[1] = 0.2F;
      localObjectAnimator = ObjectAnimator.ofFloat(paramView, "alpha", (float[])localObject1);
      localObject1 = new android/view/animation/DecelerateInterpolator;
      ((DecelerateInterpolator)localObject1).<init>();
      localObjectAnimator.setInterpolator((TimeInterpolator)localObject1);
      localObject1 = a(paramView, 0, 0, i3, i5);
      localObject2 = new android/view/animation/AccelerateInterpolator;
      ((AccelerateInterpolator)localObject2).<init>();
      ((Animator)localObject1).setInterpolator((TimeInterpolator)localObject2);
      f2 = m;
      paramView = ViewAnimationUtils.createCircularReveal(paramView, i, j, f1, f2);
      localObject3 = new android/view/animation/DecelerateInterpolator;
      ((DecelerateInterpolator)localObject3).<init>();
      paramView.setInterpolator((TimeInterpolator)localObject3);
    }
    Object localObject3 = new android/animation/AnimatorSet;
    ((AnimatorSet)localObject3).<init>();
    j = 3;
    float f3 = 4.2E-45F;
    Animator[] arrayOfAnimator = new Animator[j];
    arrayOfAnimator[0] = localObject1;
    arrayOfAnimator[i4] = localObjectAnimator;
    arrayOfAnimator[k] = paramView;
    ((AnimatorSet)localObject3).playTogether(arrayOfAnimator);
    paramView = getContext().getResources();
    paramBoolean = 17694720;
    int i7 = paramView.getInteger(paramBoolean);
    long l = i7;
    ((AnimatorSet)localObject3).setDuration(l);
    if (paramAnimatorListener != null) {
      ((AnimatorSet)localObject3).addListener(paramAnimatorListener);
    }
    ((AnimatorSet)localObject3).start();
    f = ((Animator)localObject3);
  }
  
  public final void dismiss()
  {
    boolean bool = k.d();
    if (bool)
    {
      Object localObject = f;
      if (localObject != null)
      {
        bool = ((Animator)localObject).isStarted();
        if (bool) {
          return;
        }
      }
      localObject = findViewById(2131362074);
      b.1 local1 = new com/truecaller/ui/dialogs/b$1;
      local1.<init>(this);
      a((View)localObject, false, local1);
      return;
    }
    super.dismiss();
  }
  
  public final void onClick(View paramView)
  {
    int i = paramView.getId();
    int j = 2131363323;
    if (i == j) {
      dismiss();
    }
    Object localObject1 = getContext();
    int k = paramView.getId();
    j = 2131362086;
    if ((k == j) && (localObject1 != null))
    {
      TrueApp.y().a();
      paramView = TrueApp.y().a().c();
      j = a;
      int m = 4;
      int n = 32;
      Object localObject2;
      if (j == n)
      {
        localObject2 = PremiumPresenterView.LaunchContext.GOLD_BADGE;
        String str1 = "gold";
        br.a((Context)localObject1, (PremiumPresenterView.LaunchContext)localObject2, str1);
      }
      else if (j == m)
      {
        localObject2 = PremiumPresenterView.LaunchContext.PREMIUM_BADGE;
        br.a((Context)localObject1, (PremiumPresenterView.LaunchContext)localObject2);
      }
      i = a;
      if ((i == n) || (i == m))
      {
        localObject1 = new com/truecaller/analytics/e$a;
        ((e.a)localObject1).<init>("ViewAction");
        localObject1 = ((e.a)localObject1).a("Action", "details");
        localObject2 = "SubAction";
        String str2 = "premiumBadge";
        localObject1 = ((e.a)localObject1).a((String)localObject2, str2).a();
        paramView.b((com.truecaller.analytics.e)localObject1);
      }
    }
  }
  
  protected final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = 1;
    requestWindowFeature(i);
    Object localObject1 = getWindow();
    Object localObject2 = new android/graphics/drawable/ColorDrawable;
    int k = 0;
    ((ColorDrawable)localObject2).<init>(0);
    ((Window)localObject1).setBackgroundDrawable((Drawable)localObject2);
    setCanceledOnTouchOutside(i);
    localObject1 = getWindow().getDecorView();
    localObject2 = new com/truecaller/ui/dialogs/-$$Lambda$b$cLhswllxwvT-mSDikkQ1Mmf9u1I;
    ((-..Lambda.b.cLhswllxwvT-mSDikkQ1Mmf9u1I)localObject2).<init>(this);
    ((View)localObject1).setOnTouchListener((View.OnTouchListener)localObject2);
    setContentView(2131558560);
    int m = 2131363323;
    localObject1 = (ImageView)findViewById(m);
    int n = 2131362084;
    localObject2 = (TextView)findViewById(n);
    int i1 = 2131362086;
    View localView = findViewById(i1);
    boolean bool1 = k.d();
    if (bool1)
    {
      localObject3 = c.getDrawable();
      if (localObject3 != null) {
        ((ImageView)localObject1).setImageDrawable((Drawable)localObject3);
      }
    }
    Object localObject3 = getContext();
    boolean bool2 = e;
    if (bool2) {
      i3 = 2131233857;
    } else {
      i3 = 2131233849;
    }
    Object localObject4 = android.support.v4.content.b.a((Context)localObject3, i3);
    Object localObject5 = b;
    int i4;
    if (localObject5 != null)
    {
      localObject5 = com.d.b.w.a((Context)localObject3);
      Uri localUri = b;
      localObject5 = ((com.d.b.w)localObject5).a(localUri);
      c = i;
      localObject4 = ((ab)localObject5).a((Drawable)localObject4).b((Drawable)localObject4).b();
      localObject5 = new com/truecaller/common/h/aq$c;
      localObject3 = ((Context)localObject3).getResources();
      i4 = 2131166170;
      float f1 = ((Resources)localObject3).getDimension(i4);
      ((aq.c)localObject5).<init>(f1);
      localObject3 = ((ab)localObject4).a((ai)localObject5);
      localObject4 = d;
      ((ab)localObject3).a((ImageView)localObject1, (com.d.b.e)localObject4);
    }
    else
    {
      localObject3 = d;
      if (localObject3 != null) {
        ((com.d.b.e)localObject3).onError();
      }
      ((ImageView)localObject1).setImageDrawable((Drawable)localObject4);
    }
    int i2 = com.truecaller.util.w.a(a, i);
    int i3 = 8;
    if (i2 != 0)
    {
      localView.setVisibility(0);
      int i5 = a;
      i4 = 32;
      int i6 = 4;
      if (i5 != i)
      {
        if (i5 != i6)
        {
          if (i5 != i3)
          {
            if (i5 != i4)
            {
              j = 64;
              if (i5 != j)
              {
                j = 0;
                paramBundle = null;
              }
              else
              {
                j = 2131886313;
              }
            }
            else
            {
              j = 2131886316;
            }
          }
          else {
            j = 2131886312;
          }
        }
        else {
          j = 2131886317;
        }
      }
      else {
        j = 2131886318;
      }
      ((TextView)localObject2).setText(j);
      int j = a;
      if ((j == i6) || (j == i4)) {
        k = 2131234430;
      }
      m.a((TextView)localObject2, i2, k);
    }
    else
    {
      localView.setVisibility(i3);
    }
    setOnShowListener(this);
    ((ImageView)localObject1).setOnClickListener(this);
    localView.setOnClickListener(this);
  }
  
  public final void onShow(DialogInterface paramDialogInterface)
  {
    boolean bool1 = k.d();
    if (bool1)
    {
      int i = 2131362074;
      paramDialogInterface = findViewById(i);
      boolean bool2 = true;
      a(paramDialogInterface, bool2, null);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */