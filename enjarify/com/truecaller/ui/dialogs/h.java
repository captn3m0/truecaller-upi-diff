package com.truecaller.ui.dialogs;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import com.truecaller.util.al;
import java.util.Map;

public final class h
  extends RecyclerView.Adapter
{
  private final Context a;
  private final al b;
  private final Map c;
  
  public h(Context paramContext, al paramal, Map paramMap)
  {
    a = paramContext;
    b = paramal;
    c = paramMap;
  }
  
  public final int getItemCount()
  {
    return c.size();
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */