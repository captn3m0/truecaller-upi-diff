package com.truecaller.ui.dialogs;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Debug;
import android.os.Parcelable;
import android.support.v4.content.FileProvider;
import android.widget.Toast;
import c.g.b.k;
import com.truecaller.common.h.ai;
import java.io.File;

public final class TracingBroadcastReceiver
  extends BroadcastReceiver
{
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    k.b(paramContext, "context");
    k.b(paramIntent, "intent");
    Object localObject = paramIntent.getAction();
    String str1 = "com.truecaller.qa.STOP_METHOD_TRACING";
    boolean bool = k.a(localObject, str1);
    int i = 1;
    bool ^= i;
    if (bool) {
      return;
    }
    Debug.stopMethodTracing();
    localObject = "path";
    paramIntent = paramIntent.getStringExtra((String)localObject);
    if (paramIntent == null) {
      return;
    }
    String str2 = String.valueOf(paramIntent);
    localObject = (CharSequence)"Trace saved to ".concat(str2);
    Toast.makeText(paramContext, (CharSequence)localObject, i).show();
    localObject = new java/io/File;
    ((File)localObject).<init>(paramIntent);
    paramIntent = new android/content/Intent;
    paramIntent.<init>("android.intent.action.SEND");
    str2 = ai.a(paramContext);
    localObject = (Parcelable)FileProvider.a(paramContext, str2, (File)localObject);
    paramIntent = paramIntent.putExtra("android.intent.extra.STREAM", (Parcelable)localObject).setType("application/binary").setFlags(268435456);
    paramContext.startActivity(paramIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.TracingBroadcastReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */