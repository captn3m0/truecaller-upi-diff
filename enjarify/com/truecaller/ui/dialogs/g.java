package com.truecaller.ui.dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.view.Window;

public final class g
  extends AppCompatDialog
{
  public g(Context paramContext, boolean paramBoolean)
  {
    super(paramContext);
    setCancelable(paramBoolean);
    setCanceledOnTouchOutside(paramBoolean);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = 1;
    requestWindowFeature(i);
    paramBundle = getWindow();
    if (paramBundle != null)
    {
      ColorDrawable localColorDrawable = new android/graphics/drawable/ColorDrawable;
      localColorDrawable.<init>(0);
      paramBundle.setBackgroundDrawable(localColorDrawable);
    }
    setContentView(2131558504);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */