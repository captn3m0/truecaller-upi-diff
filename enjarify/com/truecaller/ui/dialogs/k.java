package com.truecaller.ui.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public final class k
  extends a
{
  protected TextView a;
  
  public static k c(int paramInt)
  {
    k localk = new com/truecaller/ui/dialogs/k;
    localk.<init>();
    localk.getArguments().putInt("message", paramInt);
    localk.setCancelable(false);
    return localk;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2131558583, paramViewGroup);
    paramViewGroup = (TextView)paramLayoutInflater.findViewById(2131363752);
    a = paramViewGroup;
    paramViewGroup = getArguments();
    paramBundle = a;
    int i = paramViewGroup.getInt("message");
    paramBundle.setText(i);
    return paramLayoutInflater;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */