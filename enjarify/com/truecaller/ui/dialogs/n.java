package com.truecaller.ui.dialogs;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.ui.view.ContactPhoto;

public final class n
  extends RecyclerView.ViewHolder
{
  final ContactPhoto a;
  final TextView b;
  final ImageView c;
  
  public n(View paramView)
  {
    super(paramView);
    Object localObject = paramView.findViewById(2131362554);
    k.a(localObject, "itemView.findViewById(R.id.contact_photo)");
    localObject = (ContactPhoto)localObject;
    a = ((ContactPhoto)localObject);
    localObject = paramView.findViewById(2131363786);
    k.a(localObject, "itemView.findViewById(R.id.name)");
    localObject = (TextView)localObject;
    b = ((TextView)localObject);
    paramView = paramView.findViewById(2131362948);
    k.a(paramView, "itemView.findViewById(R.id.emoji)");
    paramView = (ImageView)paramView;
    c = paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */