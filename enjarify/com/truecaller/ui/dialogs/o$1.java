package com.truecaller.ui.dialogs;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import com.truecaller.common.h.ai;
import com.truecaller.old.a.a;

final class o$1
  extends a
{
  private Bitmap d;
  
  o$1(o paramo, View paramView) {}
  
  public final void a(Object paramObject)
  {
    o localo = c;
    paramObject = (Uri)paramObject;
    o.a(localo, (Uri)paramObject);
    o.a(c).setEnabled(true);
  }
  
  protected final Object doInBackground(Object... paramVarArgs)
  {
    paramVarArgs = c.getContext();
    Bitmap localBitmap = d;
    return ai.a(paramVarArgs, localBitmap);
  }
  
  public final void onPostExecute(Object paramObject)
  {
    super.onPostExecute(paramObject);
    d.recycle();
  }
  
  public final void onPreExecute()
  {
    super.onPreExecute();
    View localView = a;
    ViewGroup.LayoutParams localLayoutParams = new android/view/ViewGroup$LayoutParams;
    int i = -2;
    localLayoutParams.<init>(i, i);
    localView.setLayoutParams(localLayoutParams);
    localView.measure(0, 0);
    i = localView.getMeasuredWidth();
    int j = localView.getMeasuredHeight();
    Object localObject = Bitmap.Config.ARGB_8888;
    localObject = Bitmap.createBitmap(i, j, (Bitmap.Config)localObject);
    Canvas localCanvas = new android/graphics/Canvas;
    localCanvas.<init>((Bitmap)localObject);
    localView.layout(0, 0, i, j);
    localView.draw(localCanvas);
    d = ((Bitmap)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.o.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */