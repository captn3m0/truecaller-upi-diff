package com.truecaller.ui.dialogs;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.support.v4.app.f;
import android.support.v4.content.b;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import c.n.m;
import com.truecaller.R.id;
import com.truecaller.utils.extensions.a;
import java.util.HashMap;

public final class PremiumObtainedDialogActivity
  extends AppCompatActivity
{
  public static final PremiumObtainedDialogActivity.a a;
  private HashMap b;
  
  static
  {
    PremiumObtainedDialogActivity.a locala = new com/truecaller/ui/dialogs/PremiumObtainedDialogActivity$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      b = ((HashMap)localObject1);
    }
    localObject1 = b;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = b;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public static final void a(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    k.b(paramContext, "context");
    k.b(paramString1, "title");
    k.b(paramString2, "text");
    k.b(paramString3, "level");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, PremiumObtainedDialogActivity.class);
    paramString1 = localIntent.addFlags(268435456).putExtra("ARG_TITLE", paramString1).putExtra("ARG_TEXT", paramString2).putExtra("ARG_LEVEL", paramString3);
    paramContext.startActivity(paramString1);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    a.a(this);
    paramBundle = getTheme();
    int i = 0;
    TextView localTextView = null;
    int j = 2131952145;
    paramBundle.applyStyle(j, false);
    setContentView(2131558581);
    int k = R.id.dialogTitle;
    paramBundle = (TextView)a(k);
    k.a(paramBundle, "dialogTitle");
    Object localObject = (CharSequence)getIntent().getStringExtra("ARG_TITLE");
    paramBundle.setText((CharSequence)localObject);
    k = R.id.dialogText;
    paramBundle = (TextView)a(k);
    k.a(paramBundle, "dialogText");
    localObject = getIntent();
    String str = "ARG_TEXT";
    localObject = (CharSequence)((Intent)localObject).getStringExtra(str);
    paramBundle.setText((CharSequence)localObject);
    paramBundle = getIntent().getStringExtra("ARG_LEVEL");
    localObject = "gold";
    boolean bool2 = true;
    boolean bool1 = m.a(paramBundle, (String)localObject, bool2);
    int m;
    if (bool1)
    {
      m = R.id.image;
      ((ImageView)a(m)).setImageResource(2131234168);
      m = R.id.dialogTitle;
      paramBundle = (TextView)a(m);
      localObject = this;
      localObject = (Context)this;
      int n = 2131100389;
      j = b.c((Context)localObject, n);
      paramBundle.setTextColor(j);
      m = 2131886881;
      localObject = new Object[bool2];
      str = "https://tclr.se/2OGXxcW";
      localObject[0] = str;
      paramBundle = getString(m, (Object[])localObject);
    }
    else
    {
      m = 2131886883;
      localObject = new Object[bool2];
      str = "https://tclr.se/2OGXxcW";
      localObject[0] = str;
      paramBundle = getString(m, (Object[])localObject);
    }
    i = R.id.gotItButton;
    localTextView = (TextView)a(i);
    localObject = new com/truecaller/ui/dialogs/PremiumObtainedDialogActivity$b;
    ((PremiumObtainedDialogActivity.b)localObject).<init>(this);
    localObject = (View.OnClickListener)localObject;
    localTextView.setOnClickListener((View.OnClickListener)localObject);
    i = R.id.shareButton;
    localTextView = (TextView)a(i);
    localObject = new com/truecaller/ui/dialogs/PremiumObtainedDialogActivity$c;
    ((PremiumObtainedDialogActivity.c)localObject).<init>(this, paramBundle);
    localObject = (View.OnClickListener)localObject;
    localTextView.setOnClickListener((View.OnClickListener)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.PremiumObtainedDialogActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */