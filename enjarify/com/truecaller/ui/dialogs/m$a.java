package com.truecaller.ui.dialogs;

import android.app.ProgressDialog;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ax;
import kotlinx.coroutines.e;
import kotlinx.coroutines.g;

final class m$a
  extends c.d.b.a.k
  implements c.g.a.m
{
  Object a;
  int b;
  private ag h;
  
  m$a(m paramm, boolean paramBoolean, int paramInt1, ProgressDialog paramProgressDialog, int paramInt2, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/ui/dialogs/m$a;
    m localm = c;
    boolean bool = d;
    int i = e;
    ProgressDialog localProgressDialog = f;
    int j = g;
    locala.<init>(localm, bool, i, localProgressDialog, j, paramc);
    paramObject = (ag)paramObject;
    h = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    int j = 2;
    boolean bool;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      localObject1 = (ag)a;
      bool = paramObject instanceof o.b;
      if (!bool) {
        break label328;
      }
      throw a;
    case 1: 
      localObject1 = (ag)a;
      bool = paramObject instanceof o.b;
      if (bool) {
        throw a;
      }
      break;
    case 0: 
      bool = paramObject instanceof o.b;
      if (bool) {
        break label384;
      }
      paramObject = h;
      bool = d;
      if (!bool) {
        break label232;
      }
      localObject2 = c;
      int k = e;
      localObject3 = f;
      a = paramObject;
      m = 1;
      b = m;
      f localf = (f)ax.b();
      Object localObject4 = new com/truecaller/ui/dialogs/m$b;
      ((m.b)localObject4).<init>((m)localObject2, k, (ProgressDialog)localObject3, null);
      localObject4 = (c.g.a.m)localObject4;
      localObject2 = g.a(localf, (c.g.a.m)localObject4, this);
      if (localObject2 == localObject1) {
        return localObject1;
      }
      localObject1 = paramObject;
      paramObject = localObject2;
    }
    paramObject = (Number)paramObject;
    int n = ((Number)paramObject).intValue();
    break label339;
    label232:
    Object localObject3 = c;
    int m = g;
    int i1 = e;
    ProgressDialog localProgressDialog = f;
    a = paramObject;
    b = j;
    Object localObject2 = (f)ax.b();
    Object localObject5 = new com/truecaller/ui/dialogs/m$c;
    Object localObject6 = localObject5;
    ((m.c)localObject5).<init>((m)localObject3, m, i1, localProgressDialog, null);
    localObject5 = (c.g.a.m)localObject5;
    localObject2 = g.a((f)localObject2, (c.g.a.m)localObject5, this);
    if (localObject2 == localObject1) {
      return localObject1;
    }
    localObject1 = paramObject;
    paramObject = localObject2;
    label328:
    paramObject = (Number)paramObject;
    n = ((Number)paramObject).intValue();
    label339:
    localObject2 = m.b(c);
    localObject6 = new com/truecaller/ui/dialogs/m$a$1;
    ((m.a.1)localObject6).<init>(this, n, null);
    localObject6 = (c.g.a.m)localObject6;
    e.b((ag)localObject1, (f)localObject2, (c.g.a.m)localObject6, j);
    return x.a;
    label384:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.m.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */