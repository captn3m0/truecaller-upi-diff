package com.truecaller.ui.dialogs;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.View;
import c.g.b.k;
import java.util.HashMap;

public abstract class a
  extends AppCompatDialogFragment
{
  private HashMap a;
  
  public a()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    setArguments(localBundle);
  }
  
  private void a(f paramf, String paramString)
  {
    if (paramf == null) {
      return;
    }
    boolean bool = paramf.isFinishing();
    if (bool) {
      return;
    }
    try
    {
      paramf = paramf.getSupportFragmentManager();
      if (paramString != null)
      {
        Fragment localFragment = paramf.a(paramString);
        if (localFragment != null) {}
      }
      else
      {
        super.show(paramf, paramString);
        paramf.b();
        return;
      }
    }
    catch (Exception localException) {}
  }
  
  public View a(int paramInt)
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      a = ((HashMap)localObject1);
    }
    localObject1 = a;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = a;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  protected final void a(int paramInt, Intent paramIntent)
  {
    Fragment localFragment = getTargetFragment();
    if (localFragment != null)
    {
      int i = getTargetRequestCode();
      localFragment.onActivityResult(i, paramInt, paramIntent);
      return;
    }
  }
  
  public void b()
  {
    HashMap localHashMap = a;
    if (localHashMap != null) {
      localHashMap.clear();
    }
  }
  
  protected void b(int paramInt)
  {
    getActivity();
  }
  
  public void dismiss()
  {
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    String str = "activity ?: return";
    k.a(localf, str);
    boolean bool = localf.isFinishing();
    if (bool) {
      return;
    }
    super.dismiss();
  }
  
  public void dismissAllowingStateLoss()
  {
    f localf = getActivity();
    if (localf == null) {
      return;
    }
    String str = "activity ?: return";
    k.a(localf, str);
    boolean bool = localf.isFinishing();
    if (bool) {
      return;
    }
    super.dismissAllowingStateLoss();
  }
  
  public void onCancel(DialogInterface paramDialogInterface)
  {
    super.onCancel(paramDialogInterface);
    getActivity();
    a(0, null);
  }
  
  public void onDismiss(DialogInterface paramDialogInterface)
  {
    super.onDismiss(paramDialogInterface);
    getActivity();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */