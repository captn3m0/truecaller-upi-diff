package com.truecaller.ui.dialogs;

import android.content.Context;
import c.g.b.k;

public final class f$a
{
  public static f a(Context paramContext)
  {
    k.b(paramContext, "context");
    f localf = new com/truecaller/ui/dialogs/f;
    localf.<init>(paramContext);
    localf.setCancelable(false);
    localf.show();
    return localf;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.dialogs.f.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */