package com.truecaller.ui;

import android.content.Context;
import android.support.v4.app.f;
import com.truecaller.TrueApp;
import com.truecaller.data.entity.Contact;
import com.truecaller.old.a.a;
import java.lang.ref.WeakReference;
import java.util.List;

public final class y
  extends a
{
  private Contact a;
  private int c;
  private WeakReference d;
  private final String e;
  
  public y(f paramf, int paramInt, Contact paramContact, String paramString)
  {
    c = paramInt;
    a = paramContact;
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramf);
    d = localWeakReference;
    e = paramString;
  }
  
  public final void a(Object paramObject)
  {
    Object localObject1 = d.get();
    Object localObject2 = localObject1;
    localObject2 = (f)localObject1;
    if ((localObject2 != null) && (paramObject != null))
    {
      Object localObject3 = paramObject;
      localObject3 = (Contact)paramObject;
      List localList = ((Contact)localObject3).A();
      int i = c;
      localObject1 = null;
      int j = 1;
      if (i == 0)
      {
        i = 1;
      }
      else
      {
        i = 0;
        paramObject = null;
      }
      int k = c;
      if (k == j) {
        k = 1;
      } else {
        k = 0;
      }
      String str = e;
      j = i;
      com.truecaller.calling.c.c.a((f)localObject2, (Contact)localObject3, localList, i, false, k, str);
      return;
    }
  }
  
  protected final Object doInBackground(Object... paramVarArgs)
  {
    paramVarArgs = new com/truecaller/data/access/c;
    Object localObject = TrueApp.x();
    paramVarArgs.<init>((Context)localObject);
    localObject = a;
    return paramVarArgs.a((Contact)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */