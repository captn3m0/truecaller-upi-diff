package com.truecaller.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.truecaller.TrueApp;
import com.truecaller.ads.AdLayoutType;
import com.truecaller.ads.CustomTemplate;
import com.truecaller.ads.a.d;
import com.truecaller.ads.k;
import com.truecaller.ads.k.a;
import com.truecaller.ads.k.b;
import com.truecaller.ads.k.c;
import com.truecaller.analytics.b;
import com.truecaller.analytics.bc;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.old.data.entity.Notification.NotificationState;
import com.truecaller.ui.components.o;
import com.truecaller.util.at;
import com.truecaller.wizard.b.c;
import java.util.Collection;
import java.util.List;

public final class u
  extends r
{
  private RecyclerView a;
  private o b;
  private com.truecaller.ads.a.a c;
  private com.truecaller.ads.a.e d;
  private com.truecaller.old.data.access.f e;
  
  public static Intent a(Context paramContext)
  {
    SingleActivity.FragmentSingle localFragmentSingle = SingleActivity.FragmentSingle.NOTIFICATIONS;
    return SingleActivity.b(paramContext, localFragmentSingle);
  }
  
  public static void b(Context paramContext)
  {
    Object localObject = SingleActivity.FragmentSingle.NOTIFICATIONS;
    localObject = SingleActivity.b(paramContext, (SingleActivity.FragmentSingle)localObject);
    paramContext.startActivity((Intent)localObject);
  }
  
  private void g()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      boolean bool = isFinishing();
      if (!bool)
      {
        localObject = com.truecaller.old.data.access.f.b(e.l());
        b.a((Collection)localObject);
        C_();
        return;
      }
    }
  }
  
  protected final void C_()
  {
    o localo = b;
    if (localo != null)
    {
      i = localo.getItemCount();
      if (i != 0)
      {
        i = 0;
        localo = null;
        break label30;
      }
    }
    int i = 1;
    label30:
    at.a(t(), i);
    at.a(b(), i);
  }
  
  protected final void a()
  {
    super.a();
    e = null;
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getActivity();
    Object localObject1 = (com.truecaller.common.b.a)paramBundle.getApplicationContext();
    Object localObject2 = localObject1;
    localObject2 = ((bk)localObject1).a();
    boolean bool = ((com.truecaller.common.b.a)localObject1).p();
    Object localObject3 = null;
    if (bool)
    {
      bool = c.f();
      if (bool)
      {
        localObject1 = new com/truecaller/ui/components/o;
        Object localObject4 = getActivity();
        ((o)localObject1).<init>((Context)localObject4);
        b = ((o)localObject1);
        localObject1 = k.a().a("/43067329/A*Notifications*Native*GPS").b("NOTIFICATIONS").c("notificationsList").a(0);
        localObject4 = new CustomTemplate[1];
        Object localObject5 = CustomTemplate.NATIVE_CONTENT_DUAL_TRACKER;
        localObject4[0] = localObject5;
        localObject1 = ((k.b)localObject1).a((CustomTemplate[])localObject4).a().b().c().d().e();
        localObject3 = new com/truecaller/ads/a/f;
        localObject4 = ((bp)localObject2).aq();
        localObject5 = ((bp)localObject2).bl();
        ((com.truecaller.ads.a.f)localObject3).<init>((com.truecaller.ads.provider.f)localObject4, (k)localObject1, (c.d.f)localObject5);
        d = ((com.truecaller.ads.a.e)localObject3);
        localObject3 = new com/truecaller/ads/a/a;
        localObject4 = b;
        localObject5 = AdLayoutType.MEGA_VIDEO;
        d locald = new com/truecaller/ads/a/d;
        locald.<init>(2);
        com.truecaller.ads.a.e locale = d;
        ((com.truecaller.ads.a.a)localObject3).<init>((RecyclerView.Adapter)localObject4, (com.truecaller.ads.a)localObject5, locald, locale);
        c = ((com.truecaller.ads.a.a)localObject3);
        localObject3 = new com/truecaller/old/data/access/f;
        ((com.truecaller.old.data.access.f)localObject3).<init>(paramBundle);
        e = ((com.truecaller.old.data.access.f)localObject3);
        paramBundle = TrueApp.y().a().c();
        localObject3 = new com/truecaller/analytics/bc;
        ((bc)localObject3).<init>("notificationsList");
        paramBundle.a((com.truecaller.analytics.e)localObject3);
        ((bp)localObject2).aq().b((k)localObject1);
        return;
      }
    }
    c.a(paramBundle, WizardActivity.class, "widget");
    paramBundle.overridePendingTransition(0, 0);
    paramBundle.finish();
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    boolean bool = s();
    if (bool)
    {
      int i = 2131623972;
      paramMenuInflater.inflate(i, paramMenu);
    }
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    setHasOptionsMenu(true);
    paramLayoutInflater = paramLayoutInflater.inflate(2131559214, paramViewGroup, false);
    paramViewGroup = (RecyclerView)paramLayoutInflater.findViewById(2131364092);
    a = paramViewGroup;
    return paramLayoutInflater;
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    com.truecaller.ads.a.e locale = d;
    if (locale != null) {
      locale.d();
    }
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    boolean bool1 = super.onOptionsItemSelected(paramMenuItem);
    boolean bool2 = true;
    if (bool1) {
      return bool2;
    }
    int j = paramMenuItem.getItemId();
    int i = 2131361922;
    if (j != i)
    {
      switch (j)
      {
      default: 
        return false;
      case 2131361905: 
        paramMenuItem = e;
        localList = paramMenuItem.l();
        Notification.NotificationState localNotificationState = Notification.NotificationState.NEW;
        paramMenuItem.a(localList, localNotificationState);
        b.notifyDataSetChanged();
        return bool2;
      }
      paramMenuItem = e;
      List localList = paramMenuItem.l();
      paramMenuItem.c(localList);
      b.notifyDataSetChanged();
      return bool2;
    }
    c(bool2);
    new u.2(this);
    return bool2;
  }
  
  public final void onResume()
  {
    super.onResume();
    o localo = b;
    if (localo != null) {
      g();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramView = b;
    if (paramView == null) {
      return;
    }
    getActivity().setTitle(2131887270);
    paramView = getString(2131886769);
    a(paramView, 2130969278);
    C_();
    paramView = a;
    paramBundle = new android/support/v7/widget/LinearLayoutManager;
    Context localContext = getContext();
    paramBundle.<init>(localContext);
    paramView.setLayoutManager(paramBundle);
    paramView = new com/truecaller/ui/u$1;
    paramView.<init>(this);
    b.registerAdapterDataObserver(paramView);
    paramView = b;
    paramBundle = new com/truecaller/ui/-$$Lambda$u$oeZTlFwjd911MlV6hBsV3KlIoYs;
    paramBundle.<init>(this);
    a = paramBundle;
    paramView = a;
    paramBundle = c;
    paramView.setAdapter(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */