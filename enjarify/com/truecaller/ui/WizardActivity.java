package com.truecaller.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ac;
import androidx.work.p;
import androidx.work.q;
import com.truecaller.TrueApp;
import com.truecaller.ads.provider.fetch.AdsConfigurationManager;
import com.truecaller.analytics.AppHeartBeatTask;
import com.truecaller.analytics.AppSettingsTask;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.bb;
import com.truecaller.backup.BackupLogWorker;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.background.g;
import com.truecaller.common.background.h;
import com.truecaller.common.tag.TagService;
import com.truecaller.common.tag.sync.AvailableTagsDownloadWorker;
import com.truecaller.common.tag.sync.TagKeywordsDownloadWorker;
import com.truecaller.f.a.j;
import com.truecaller.filters.sync.FilterRestoreWorker;
import com.truecaller.filters.sync.TopSpammersSyncRecurringWorker;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.transport.im.FetchImContactsWorker;
import com.truecaller.network.spamUrls.FetchSpamLinksWhiteListWorker;
import com.truecaller.old.data.access.Settings;
import com.truecaller.old.data.access.d.a;
import com.truecaller.presence.SendPresenceSettingWorker;
import com.truecaller.referral.ReferralManager;
import com.truecaller.referral.w;
import com.truecaller.tracking.events.k;
import com.truecaller.tracking.events.k.a;
import com.truecaller.wizard.TruecallerWizard;
import java.util.concurrent.TimeUnit;
import org.apache.a.d.d;

public class WizardActivity
  extends TruecallerWizard
{
  private bp c;
  private final com.truecaller.common.e.e d;
  
  public WizardActivity()
  {
    com.truecaller.common.e.e locale = new com/truecaller/common/e/e;
    locale.<init>();
    d = locale;
  }
  
  public final void a()
  {
    super.a();
    TagService.a(this);
    ac.a(this).a(null, 2131362836);
  }
  
  public final void b()
  {
    setResult(-1);
    super.b();
    Object localObject1 = "languageAuto";
    boolean bool1 = true;
    boolean bool2 = com.truecaller.common.b.e.a((String)localObject1, bool1);
    int k = 0;
    if (!bool2)
    {
      localObject1 = TrueApp.x();
      localObject3 = new com/truecaller/old/data/entity/b;
      localObject4 = com.truecaller.common.e.e.b(com.truecaller.common.b.e.a("language"));
      ((com.truecaller.old.data.entity.b)localObject3).<init>((com.truecaller.common.e.c)localObject4);
      Settings.a("languageAuto", false);
      Settings.a((Context)localObject1, (com.truecaller.old.data.entity.b)localObject3);
      localObject1 = (com.truecaller.common.b.a)getApplication();
      localObject3 = c;
      localObject4 = new com/truecaller/old/data/access/d$a;
      ((d.a)localObject4).<init>((com.truecaller.common.b.a)localObject1);
      ((com.truecaller.common.background.b)localObject3).a((Runnable)localObject4);
    }
    long l1 = System.currentTimeMillis();
    long l2 = TimeUnit.DAYS.toMillis(1L);
    l1 += l2;
    Settings.a(l1);
    localObject1 = c.aS();
    long l3 = System.currentTimeMillis();
    long l4 = j.a();
    l3 += l4;
    ((com.truecaller.f.a)localObject1).a("KeyCallLogPromoDisabledUntil", l3);
    localObject1 = com.truecaller.common.b.a.F();
    AppSettingsTask.a(c);
    Object localObject3 = c;
    AppHeartBeatTask.b((com.truecaller.common.background.b)localObject3);
    ((com.truecaller.common.b.a)localObject1).J();
    com.truecaller.util.f.a(getApplicationContext(), 0);
    localObject1 = getIntent();
    String str;
    try
    {
      localObject3 = c;
      localObject3 = ((bp)localObject3).f();
      localObject4 = "EXTRA_REG_NUDGE";
      localObject1 = ((Intent)localObject1).getStringExtra((String)localObject4);
      if (localObject1 != null)
      {
        localObject3 = ((com.truecaller.androidactors.f)localObject3).a();
        localObject3 = (ae)localObject3;
        localObject4 = k.b();
        str = "RegistrationNudge";
        localObject4 = ((k.a)localObject4).a(str);
        localObject1 = ((k.a)localObject4).b((CharSequence)localObject1);
        localObject1 = ((k.a)localObject1).a();
        ((ae)localObject3).a((d)localObject1);
      }
      else
      {
        localObject1 = "regNudgeBadgeSet";
        bool2 = com.truecaller.common.b.e.a((String)localObject1, false);
        if (bool2)
        {
          localObject1 = getApplicationContext();
          com.truecaller.util.f.a((Context)localObject1, 0);
          localObject1 = ((com.truecaller.androidactors.f)localObject3).a();
          localObject1 = (ae)localObject1;
          localObject3 = k.b();
          localObject4 = "RegistrationNudge";
          localObject3 = ((k.a)localObject3).a((CharSequence)localObject4);
          localObject4 = "Badge";
          localObject3 = ((k.a)localObject3).b((CharSequence)localObject4);
          localObject3 = ((k.a)localObject3).a();
          ((ae)localObject1).a((d)localObject3);
        }
      }
    }
    catch (org.apache.a.a locala)
    {
      AssertionUtil.reportThrowableButNeverCrash(locala);
    }
    Object localObject2 = c.I();
    localObject3 = c.f();
    Object localObject4 = "core_viewed_region_1";
    boolean bool4 = ((com.truecaller.common.g.a)localObject2).b((String)localObject4);
    if (bool4)
    {
      str = "viewed";
      bb.a((com.truecaller.androidactors.f)localObject3, "consentWizard", str);
      localObject4 = "core_viewed_region_1";
      ((com.truecaller.common.g.a)localObject2).d((String)localObject4);
    }
    localObject4 = "core_accepted_region_1";
    bool4 = ((com.truecaller.common.g.a)localObject2).b((String)localObject4);
    if (bool4)
    {
      localObject4 = "consentWizard";
      str = "accepted";
      bb.a((com.truecaller.androidactors.f)localObject3, (String)localObject4, str);
      localObject3 = "core_accepted_region_1";
      ((com.truecaller.common.g.a)localObject2).d((String)localObject3);
    }
    localObject2 = c.aw();
    ((AdsConfigurationManager)localObject2).f();
    int j = 7;
    localObject3 = new h[j];
    localObject4 = FetchImContactsWorker.d;
    localObject3[0] = localObject4;
    localObject4 = SendPresenceSettingWorker.e;
    localObject3[bool1] = localObject4;
    localObject4 = AvailableTagsDownloadWorker.d;
    localObject3[2] = localObject4;
    localObject4 = TagKeywordsDownloadWorker.e;
    localObject3[3] = localObject4;
    localObject4 = FetchSpamLinksWhiteListWorker.e;
    localObject3[4] = localObject4;
    localObject4 = TopSpammersSyncRecurringWorker.e;
    localObject3[5] = localObject4;
    int i = 6;
    localObject4 = BackupLogWorker.d;
    localObject3[i] = localObject4;
    Object localObject5 = p.a();
    while (k < j)
    {
      localObject4 = localObject3[k].a().b();
      ((p)localObject5).a((q)localObject4);
      k += 1;
    }
    FilterRestoreWorker.b();
    localObject2 = getIntent();
    if (localObject2 != null)
    {
      localObject2 = getIntent();
      localObject5 = "extraRequestCode";
      boolean bool3 = ((Intent)localObject2).hasExtra((String)localObject5);
      if (bool3) {}
    }
    else
    {
      localObject2 = "calls";
      localObject5 = "wizard";
      TruecallerInit.a(this, (String)localObject2, (String)localObject5);
    }
  }
  
  public final com.truecaller.wizard.d.c c()
  {
    WizardActivity.a locala = new com/truecaller/ui/WizardActivity$a;
    com.truecaller.androidactors.f localf = c.ad();
    locala.<init>(localf);
    return locala;
  }
  
  public void onCreate(Bundle paramBundle)
  {
    Object localObject = ((bk)getApplication()).a();
    c = ((bp)localObject);
    super.onCreate(paramBundle);
    setResult(0);
    paramBundle = w.a(this, "ReferralManagerImpl");
    if (paramBundle != null)
    {
      localObject = getApplicationContext();
      paramBundle.a((Context)localObject);
    }
    paramBundle = getIntent();
    localObject = "EXTRA_REG_NUDGE";
    paramBundle = paramBundle.getStringExtra((String)localObject);
    if (paramBundle != null)
    {
      paramBundle = "signUpOrigin";
      localObject = "notificationRegNudge";
      com.truecaller.common.b.e.b(paramBundle, (String)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.WizardActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */