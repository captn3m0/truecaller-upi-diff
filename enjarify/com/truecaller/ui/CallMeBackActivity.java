package com.truecaller.ui;

import android.animation.Animator.AnimatorListener;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.truecaller.TrueApp;
import com.truecaller.analytics.bc;
import com.truecaller.analytics.e.a;
import com.truecaller.api.services.presence.v1.models.Availability;
import com.truecaller.api.services.presence.v1.models.Availability.Status;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.calling.initiate_call.b.a;
import com.truecaller.calling.initiate_call.b.a.a;
import com.truecaller.common.h.aa;
import com.truecaller.common.h.k;
import com.truecaller.data.entity.Contact;
import com.truecaller.search.local.model.g;
import com.truecaller.ui.components.AvatarView;
import com.truecaller.util.co;

public class CallMeBackActivity
  extends AppCompatActivity
  implements View.OnClickListener
{
  private ColorDrawable a;
  private View b;
  private View c;
  private String d;
  private String e;
  private Contact f;
  private String g;
  private ValueAnimator h;
  private ValueAnimator i;
  private SharedPreferences j;
  private bp k;
  private e.a l;
  
  public CallMeBackActivity()
  {
    Object localObject = new android/graphics/drawable/ColorDrawable;
    int m = Color.argb(178, 0, 0, 0);
    ((ColorDrawable)localObject).<init>(m);
    a = ((ColorDrawable)localObject);
    localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>("ANDROID_CallMeBack_DialogShown");
    localObject = ((e.a)localObject).a("Action", "Dismiss");
    l = ((e.a)localObject);
  }
  
  public static Intent a(Context paramContext, Contact paramContact, String paramString1, int paramInt, String paramString2)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, CallMeBackActivity.class);
    localIntent.addFlags(268435456);
    localIntent.addFlags(8388608);
    localIntent.addFlags(65536);
    localIntent.putExtra("ARG_CONTACT", paramContact);
    localIntent.putExtra("ARG_NUMBER", paramString1);
    localIntent.putExtra("ARG_REASON", paramInt);
    localIntent.putExtra("ARG_ANALYTICS_CONTEXT", paramString2);
    return localIntent;
  }
  
  private static SpannableStringBuilder a(c.a parama, String paramString)
  {
    parama = parama.a();
    int m = parama.getIntrinsicWidth();
    int n = parama.getIntrinsicHeight();
    parama.setBounds(0, 0, m, n);
    SpannableStringBuilder localSpannableStringBuilder = new android/text/SpannableStringBuilder;
    localSpannableStringBuilder.<init>(paramString);
    localSpannableStringBuilder.insert(0, "# ");
    paramString = new android/text/style/ImageSpan;
    paramString.<init>(parama);
    localSpannableStringBuilder.setSpan(paramString, 0, 1, 18);
    return localSpannableStringBuilder;
  }
  
  private void a()
  {
    int m = 1;
    Object localObject = new Object[m];
    String str = f.t();
    localObject[0] = str;
    localObject = getString(2131886298, (Object[])localObject);
    Toast.makeText(this, (CharSequence)localObject, m).show();
  }
  
  private void a(String paramString)
  {
    SharedPreferences.Editor localEditor = j.edit();
    long l1 = System.currentTimeMillis();
    localEditor.putLong(paramString, l1).apply();
  }
  
  private void b()
  {
    finish();
    overridePendingTransition(0, 0);
  }
  
  public void finish()
  {
    ValueAnimator localValueAnimator1 = i;
    boolean bool = localValueAnimator1.isRunning();
    if (!bool)
    {
      int m = c.getTop();
      float f1 = m;
      float f2 = 1.5F;
      f1 *= f2;
      ValueAnimator localValueAnimator2 = i;
      int n = 2;
      float[] arrayOfFloat = new float[n];
      arrayOfFloat[0] = 0.0F;
      int i1 = 1;
      arrayOfFloat[i1] = f1;
      localValueAnimator2.setFloatValues(arrayOfFloat);
      localValueAnimator1 = i;
      localValueAnimator1.start();
    }
  }
  
  public void onClick(View paramView)
  {
    int m = paramView.getId();
    int n = 2131362050;
    String str2;
    if (m != n)
    {
      n = 2131362372;
      if (m != n)
      {
        n = 2131362594;
        if (m == n) {
          b();
        }
      }
      else
      {
        l.a("Action", "Call");
        paramView = ((bk)getApplicationContext()).a().bM();
        localObject = new com/truecaller/calling/initiate_call/b$a$a;
        str1 = d;
        str2 = g;
        ((b.a.a)localObject).<init>(str1, str2);
        boolean bool2 = true;
        localObject = ((b.a.a)localObject).b(bool2).a();
        paramView.a((b.a)localObject);
        b();
      }
      return;
    }
    paramView = l;
    String str1 = "AskToCallBack";
    paramView.a("Action", str1);
    paramView = k.aV();
    Object localObject = e;
    paramView = paramView.h((String)localObject);
    boolean bool1 = c;
    if (bool1)
    {
      i1 = b;
      int i2 = 2;
      if (i1 < i2) {}
    }
    try
    {
      m = b;
      i1 = 4;
      long l1;
      int i3;
      if (m >= i1)
      {
        paramView = k;
        paramView = paramView.aV();
        localObject = e;
        l1 = Long.parseLong((String)localObject);
        i3 = 2131886301;
        str2 = getString(i3);
        paramView.a(l1, str2);
      }
      else
      {
        paramView = k;
        paramView = paramView.aV();
        localObject = e;
        l1 = Long.parseLong((String)localObject);
        i3 = 2131887552;
        str2 = getString(i3);
        paramView.b(l1, str2);
        paramView = e;
        a(paramView);
        a();
      }
    }
    catch (NumberFormatException localNumberFormatException)
    {
      for (;;) {}
    }
    paramView = new com/truecaller/ui/CallMeBackActivity$1;
    localObject = com.truecaller.network.notification.a.a(d);
    paramView.<init>(this, (e.b)localObject);
    int i1 = 0;
    localObject = new Void[0];
    paramView.b((Object[])localObject);
    a();
    b();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    com.truecaller.utils.extensions.a.a(this);
    paramBundle = getTheme();
    int m = aresId;
    paramBundle.applyStyle(m, false);
    setContentView(2131559154);
    paramBundle = getWindow();
    Object localObject1 = a;
    paramBundle.setBackgroundDrawable((Drawable)localObject1);
    int n = 2;
    localObject1 = new float[n];
    Object tmp58_57 = localObject1;
    tmp58_57[0] = 0.0F;
    tmp58_57[1] = 1.0F;
    localObject1 = ValueAnimator.ofFloat((float[])localObject1);
    h = ((ValueAnimator)localObject1);
    localObject1 = h;
    long l1 = getResources().getInteger(17694722);
    ((ValueAnimator)localObject1).setDuration(l1);
    localObject1 = h;
    Object localObject2 = new android/view/animation/DecelerateInterpolator;
    float f1 = 3.0F;
    ((DecelerateInterpolator)localObject2).<init>(f1);
    ((ValueAnimator)localObject1).setInterpolator((TimeInterpolator)localObject2);
    localObject1 = h;
    localObject2 = new com/truecaller/ui/-$$Lambda$CallMeBackActivity$eyPc_HDmR-Vj4az6a_wER66m57w;
    ((-..Lambda.CallMeBackActivity.eyPc_HDmR-Vj4az6a_wER66m57w)localObject2).<init>(this);
    ((ValueAnimator)localObject1).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject2);
    paramBundle = new float[n];
    Bundle tmp157_156 = paramBundle;
    tmp157_156[0] = 0.0F;
    tmp157_156[1] = 1.0F;
    paramBundle = ValueAnimator.ofFloat(paramBundle);
    i = paramBundle;
    paramBundle = i;
    long l2 = 300L;
    paramBundle.setDuration(l2);
    paramBundle = i;
    localObject1 = new android/view/animation/AccelerateInterpolator;
    ((AccelerateInterpolator)localObject1).<init>(f1);
    paramBundle.setInterpolator((TimeInterpolator)localObject1);
    paramBundle = i;
    localObject1 = new com/truecaller/ui/-$$Lambda$CallMeBackActivity$CdGGIUP9fOS5EBfPvXtyY9-J5qo;
    ((-..Lambda.CallMeBackActivity.CdGGIUP9fOS5EBfPvXtyY9-J5qo)localObject1).<init>(this);
    paramBundle.addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject1);
    paramBundle = i;
    localObject1 = new com/truecaller/ui/CallMeBackActivity$2;
    ((CallMeBackActivity.2)localObject1).<init>(this);
    paramBundle.addListener((Animator.AnimatorListener)localObject1);
    paramBundle = getSharedPreferences("callMeBackNotifications", 0);
    j = paramBundle;
    paramBundle = ((TrueApp)getApplicationContext()).a();
    k = paramBundle;
    n = 2131364884;
    paramBundle = (TextView)findViewById(n);
    m = 2131364609;
    localObject1 = (TextView)findViewById(m);
    int i2 = 2131362068;
    localObject2 = (AvatarView)findViewById(i2);
    int i3 = 2131362050;
    f1 = 1.834387E38F;
    TextView localTextView = (TextView)findViewById(i3);
    int i4 = 2131362372;
    Object localObject3 = (TextView)findViewById(i4);
    int i5 = 2131363714;
    Object localObject4 = findViewById(i5);
    Object localObject5 = findViewById(2131364675);
    c = ((View)localObject5);
    localObject5 = findViewById(2131362594);
    b = ((View)localObject5);
    boolean bool3 = co.c(this);
    int i7;
    if (bool3)
    {
      localObject5 = (ImageView)c;
      i7 = 2131234759;
      ((ImageView)localObject5).setImageResource(i7);
    }
    b.setOnClickListener(this);
    ((View)localObject4).setOnClickListener(this);
    localTextView.setOnClickListener(this);
    ((TextView)localObject3).setOnClickListener(this);
    localObject4 = getIntent();
    localObject5 = (Contact)((Intent)localObject4).getParcelableExtra("ARG_CONTACT");
    f = ((Contact)localObject5);
    localObject5 = ((Intent)localObject4).getStringExtra("ARG_NUMBER");
    d = ((String)localObject5);
    localObject5 = ((Intent)localObject4).getStringExtra("ARG_ANALYTICS_CONTEXT");
    g = ((String)localObject5);
    localObject5 = k.f(this);
    Object localObject6 = f;
    if (localObject6 != null)
    {
      localObject6 = d;
      if (localObject6 != null)
      {
        localObject5 = aa.c((String)localObject6, (String)localObject5);
        e = ((String)localObject5);
        i7 = 1;
        i5 = ((Intent)localObject4).getIntExtra("ARG_REASON", i7);
        localObject5 = new com/truecaller/ui/c$a;
        ((c.a)localObject5).<init>(this);
        b = i7;
        int i9 = 10;
        e = i9;
        d = i9;
        Object localObject7 = f.t();
        paramBundle.setText((CharSequence)localObject7);
        switch (i5)
        {
        default: 
          break;
        case 1: 
          i2 = 2131886296;
          localObject2 = getString(i2);
          ((TextView)localObject1).setText((CharSequence)localObject2);
          localObject2 = g.a(this);
          localObject4 = d;
          localObject2 = ((g)localObject2).b((String)localObject4);
          if (localObject2 != null)
          {
            localObject4 = b;
            if (localObject4 != null)
            {
              boolean bool2 = ((com.truecaller.presence.a)localObject2).a();
              if (bool2)
              {
                ((TextView)localObject1).setVisibility(0);
                localObject4 = b.a();
                localObject7 = Availability.Status.AVAILABLE;
                if (localObject4 == localObject7)
                {
                  bool2 = true;
                }
                else
                {
                  bool2 = false;
                  localObject4 = null;
                }
                a = bool2;
                int i6 = 6;
                e = i6;
                d = i6;
                localObject4 = f.t();
                localObject4 = a((c.a)localObject5, (String)localObject4);
                paramBundle.setText((CharSequence)localObject4);
                paramBundle = ((com.truecaller.presence.a)localObject2).a(this, i7);
                ((TextView)localObject1).setText(paramBundle);
              }
            }
          }
          n = 2131886294;
          paramBundle = getString(n);
          ((TextView)localObject3).setText(paramBundle);
          paramBundle = l;
          localObject2 = "Trigger";
          localObject3 = "CallIncomplete";
          paramBundle.a((String)localObject2, (String)localObject3);
          break;
        case 0: 
          a = false;
          localObject4 = f.t();
          localObject4 = a((c.a)localObject5, (String)localObject4);
          paramBundle.setText((CharSequence)localObject4);
          localObject4 = new Object[i7];
          localObject4[0] = "";
          paramBundle = getString(2131886297, (Object[])localObject4);
          ((TextView)localObject1).setText(paramBundle);
          paramBundle = new com/truecaller/ui/components/b;
          localObject4 = f.a(i7);
          bool3 = false;
          localObject5 = null;
          localObject7 = f;
          boolean bool4 = ((Contact)localObject7).a(32);
          Contact localContact = f;
          int i10 = 4;
          boolean bool5 = localContact.a(i10);
          paramBundle.<init>((Uri)localObject4, null, bool4, bool5);
          ((AvatarView)localObject2).a(paramBundle);
          n = 2131886295;
          paramBundle = getString(n);
          ((TextView)localObject3).setText(paramBundle);
          paramBundle = l;
          localObject2 = "Trigger";
          localObject3 = "Busy";
          paramBundle.a((String)localObject2, (String)localObject3);
        }
        paramBundle = e;
        l2 = System.currentTimeMillis();
        localObject2 = j;
        long l3 = ((SharedPreferences)localObject2).getLong(paramBundle, 0L);
        l2 -= l3;
        l3 = 3600000L;
        boolean bool1 = l2 < l3;
        int i8;
        if (!bool1)
        {
          i8 = 0;
          localObject6 = null;
        }
        if (i8 != 0)
        {
          paramBundle = getString(2131886302);
          ((TextView)localObject1).setText(paramBundle);
          ((TextView)localObject1).setVisibility(0);
          int i1 = 8;
          localTextView.setVisibility(i1);
        }
        paramBundle = k.c();
        localObject1 = new com/truecaller/analytics/bc;
        ((bc)localObject1).<init>("callMeBack");
        paramBundle.a((com.truecaller.analytics.e)localObject1);
        return;
      }
    }
    super.finish();
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    boolean bool = isFinishing();
    if (bool)
    {
      com.truecaller.analytics.b localb = k.c();
      com.truecaller.analytics.e locale = l.a();
      localb.a(locale);
    }
  }
  
  public void onStart()
  {
    super.onStart();
    ViewTreeObserver localViewTreeObserver = b.getViewTreeObserver();
    CallMeBackActivity.3 local3 = new com/truecaller/ui/CallMeBackActivity$3;
    local3.<init>(this);
    localViewTreeObserver.addOnPreDrawListener(local3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.CallMeBackActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */