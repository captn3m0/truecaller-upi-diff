package com.truecaller.ui;

import android.content.Intent;
import android.os.Bundle;
import com.truecaller.common.h.o;
import com.truecaller.old.data.access.Settings;

public class ContactsActivity
  extends n
{
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = "android.intent.action.CREATE_SHORTCUT";
    Object localObject = getIntent().getAction();
    boolean bool1 = paramBundle.equals(localObject);
    if (bool1)
    {
      paramBundle = "shortcutInstalled";
      boolean bool2 = true;
      Settings.a(paramBundle, bool2);
      int i = -1;
      localObject = new android/content/Intent;
      ((Intent)localObject).<init>(this, ContactsActivity.class);
      ((Intent)localObject).setAction("android.intent.action.MAIN");
      String str = "android.intent.category.DEFAULT";
      ((Intent)localObject).addCategory(str);
      ((Intent)localObject).addFlags(335544320);
      int j = 2131888872;
      int k = 2131689473;
      localObject = o.a(this, j, k, (Intent)localObject);
      setResult(i, (Intent)localObject);
    }
    else
    {
      localObject = "homescreenShortcut";
      paramBundle = TruecallerInit.a(this, "contacts", (String)localObject);
      startActivity(paramBundle);
    }
    finish();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.ContactsActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */