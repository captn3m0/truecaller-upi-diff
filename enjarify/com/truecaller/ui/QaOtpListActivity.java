package com.truecaller.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.g.a;
import com.truecaller.common.h.am;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.f;
import com.truecaller.notifications.y;
import java.util.concurrent.Executor;

public class QaOtpListActivity
  extends AppCompatActivity
{
  RecyclerView a;
  TextView b;
  QaOtpListActivity.b c;
  public y d;
  public a e;
  public e f;
  public b g;
  private AsyncTask h;
  private EditText i;
  private Button j;
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    ((bk)TrueApp.x()).a().a(this);
    setContentView(2131558462);
    paramBundle = (RecyclerView)findViewById(2131364199);
    a = paramBundle;
    paramBundle = (EditText)findViewById(2131364102);
    i = paramBundle;
    paramBundle = (TextView)findViewById(2131364988);
    b = paramBundle;
    int k = 2131364227;
    paramBundle = (Button)findViewById(k);
    j = paramBundle;
    paramBundle = a;
    Object localObject = new android/support/v7/widget/LinearLayoutManager;
    Void[] arrayOfVoid = null;
    int m = 1;
    ((LinearLayoutManager)localObject).<init>(this, m, false);
    paramBundle.setLayoutManager((RecyclerView.LayoutManager)localObject);
    paramBundle = a;
    localObject = new android/support/v7/widget/DividerItemDecoration;
    ((DividerItemDecoration)localObject).<init>(this, m);
    paramBundle.addItemDecoration((RecyclerView.ItemDecoration)localObject);
    paramBundle = f.aj().e();
    boolean bool = am.b(paramBundle);
    if (!bool)
    {
      localObject = i;
      String str1 = "/";
      String str2 = "\n";
      paramBundle = paramBundle.replaceAll(str1, str2);
      ((EditText)localObject).setText(paramBundle);
    }
    paramBundle = j;
    localObject = new com/truecaller/ui/-$$Lambda$QaOtpListActivity$qhr2XREyJni6cA-KUtZqgZQoJ4M;
    ((-..Lambda.QaOtpListActivity.qhr2XREyJni6cA-KUtZqgZQoJ4M)localObject).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject);
    paramBundle = new com/truecaller/ui/QaOtpListActivity$a;
    localObject = d;
    paramBundle.<init>(this, (y)localObject);
    localObject = AsyncTask.THREAD_POOL_EXECUTOR;
    arrayOfVoid = new Void[0];
    paramBundle = paramBundle.executeOnExecutor((Executor)localObject, arrayOfVoid);
    h = paramBundle;
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    paramMenu.add("Report Errors");
    paramMenu.add("Edit regex");
    return true;
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    AsyncTask localAsyncTask = h;
    if (localAsyncTask != null)
    {
      boolean bool1 = localAsyncTask.isCancelled();
      if (!bool1)
      {
        localAsyncTask = h;
        boolean bool2 = true;
        localAsyncTask.cancel(bool2);
      }
    }
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    boolean bool1 = true;
    Object localObject1 = new String[bool1];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("onOptionsItemSelected:: ");
    CharSequence localCharSequence = paramMenuItem.getTitle();
    ((StringBuilder)localObject2).append(localCharSequence);
    localObject2 = ((StringBuilder)localObject2).toString();
    localCharSequence = null;
    localObject1[0] = localObject2;
    localObject1 = paramMenuItem.getTitle();
    localObject2 = "Report Errors";
    boolean bool2 = localObject1.equals(localObject2);
    if (bool2)
    {
      paramMenuItem = new android/content/Intent;
      paramMenuItem.<init>("android.intent.action.SEND");
      paramMenuItem.setType("text/html");
      paramMenuItem.putExtra("android.intent.extra.EMAIL", "supreeth.surendrababu@truecaller.com");
      paramMenuItem.putExtra("android.intent.extra.SUBJECT", "OTP parsing error");
      localObject2 = c.b.toString();
      paramMenuItem.putExtra("android.intent.extra.TEXT", (String)localObject2);
      localObject1 = "Send Email";
      paramMenuItem = Intent.createChooser(paramMenuItem, (CharSequence)localObject1);
      startActivity(paramMenuItem);
    }
    else
    {
      paramMenuItem = paramMenuItem.getTitle();
      localObject1 = "Edit regex";
      boolean bool3 = paramMenuItem.equals(localObject1);
      if (bool3)
      {
        new String[1][0] = "onOptionsItemSelected:: Changing visibility";
        i.setVisibility(0);
        paramMenuItem = j;
        paramMenuItem.setVisibility(0);
      }
    }
    return bool1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.QaOtpListActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */