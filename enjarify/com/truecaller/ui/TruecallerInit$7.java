package com.truecaller.ui;

import android.app.Activity;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import com.truecaller.analytics.a.f;
import com.truecaller.referral.ReferralManager;
import com.truecaller.referral.ReferralManager.ReferralLaunchContext;

final class TruecallerInit$7
  extends ActionBarDrawerToggle
{
  TruecallerInit$7(TruecallerInit paramTruecallerInit, Activity paramActivity, DrawerLayout paramDrawerLayout, Toolbar paramToolbar)
  {
    super(paramActivity, paramDrawerLayout, paramToolbar, 0, 0);
  }
  
  public final void onDrawerOpened(View paramView)
  {
    super.onDrawerOpened(paramView);
    paramView = TruecallerInit.g(a);
    if (paramView != null)
    {
      paramView = TruecallerInit.g(a);
      Object localObject = ReferralManager.ReferralLaunchContext.NAVIGATION_DRAWER;
      boolean bool = paramView.c((ReferralManager.ReferralLaunchContext)localObject);
      if (bool)
      {
        paramView = a.s;
        localObject = "ab_test_invite_referral_18335_seen";
        paramView.a((String)localObject);
      }
    }
  }
  
  public final void onDrawerSlide(View paramView, float paramFloat)
  {
    super.onDrawerSlide(paramView, 0.0F);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.TruecallerInit.7
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */