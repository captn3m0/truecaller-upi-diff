package com.truecaller.ui;

import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;
import java.util.List;

final class ad$a
  extends RecyclerView.Adapter
{
  private List b;
  private int c = 0;
  private final View d;
  
  ad$a(ad paramad, View paramView, List paramList, int paramInt)
  {
    d = paramView;
    b = paramList;
    c = paramInt;
  }
  
  static boolean a(int paramInt)
  {
    return paramInt == 0;
  }
  
  final ThemeManager.Theme a()
  {
    List localList = b;
    int i = c + -1;
    return (ThemeManager.Theme)localList.get(i);
  }
  
  public final int getItemCount()
  {
    return b.size() + 1;
  }
  
  public final int getItemViewType(int paramInt)
  {
    int i = 1;
    if (paramInt == 0) {
      paramInt = 1;
    } else {
      paramInt = 0;
    }
    if (paramInt != 0) {
      return 0;
    }
    return i;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.ad.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */