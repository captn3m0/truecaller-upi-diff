package com.truecaller.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup.MarginLayoutParams;
import com.truecaller.TrueApp;
import com.truecaller.analytics.bb;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.b.a;
import com.truecaller.log.d;
import com.truecaller.old.data.access.Settings;
import com.truecaller.tcpermissions.l;
import com.truecaller.ui.components.ScrimInsetsFrameLayout;
import com.truecaller.ui.components.ScrimInsetsFrameLayout.a;
import com.truecaller.util.at;

public abstract class n
  extends AppCompatActivity
  implements ScrimInsetsFrameLayout.a
{
  protected Fragment e;
  protected l f;
  public boolean g;
  Toolbar h;
  
  public final void a(Rect paramRect)
  {
    Toolbar localToolbar = h;
    if (localToolbar != null)
    {
      ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)localToolbar.getLayoutParams();
      int i = top;
      topMargin = i;
      localToolbar.setLayoutParams(localMarginLayoutParams);
    }
  }
  
  protected final void a(o paramo)
  {
    b(paramo);
  }
  
  protected boolean a()
  {
    return true;
  }
  
  protected void b(o paramo)
  {
    getSupportFragmentManager().a().b(2131362594, paramo, null).c();
    e = paramo;
  }
  
  protected boolean b()
  {
    return false;
  }
  
  protected void c() {}
  
  protected int g()
  {
    return 2130969592;
  }
  
  public void onBackPressed()
  {
    boolean bool1 = b();
    if (bool1) {
      return;
    }
    Object localObject = e;
    boolean bool2 = localObject instanceof p;
    if (bool2)
    {
      localObject = (p)localObject;
      bool1 = ((p)localObject).W_();
      if (bool1) {
        return;
      }
    }
    try
    {
      super.onBackPressed();
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      d.a(localIllegalStateException;
    }
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    Settings.c(this);
    paramConfiguration = getResources().getConfiguration();
    super.onConfigurationChanged(paramConfiguration);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Settings.c(this);
    if (paramBundle == null)
    {
      paramBundle = a.F();
      Object localObject = getIntent();
      if (localObject != null)
      {
        paramBundle = ((bk)paramBundle.getApplicationContext()).a().f();
        String str1 = ((Intent)localObject).getStringExtra("AppUserInteraction.Context");
        String str2 = ((Intent)localObject).getStringExtra("AppUserInteraction.Action");
        boolean bool1 = TextUtils.isEmpty(str1);
        if (!bool1)
        {
          bool1 = TextUtils.isEmpty(str2);
          if (!bool1)
          {
            bb.a(paramBundle, str1, str2);
            break label188;
          }
        }
        str1 = "android.intent.action.MAIN";
        str2 = ((Intent)localObject).getAction();
        boolean bool2 = str1.equals(str2);
        if (bool2)
        {
          str1 = "android.intent.category.LAUNCHER";
          bool2 = ((Intent)localObject).hasCategory(str1);
          if (bool2)
          {
            localObject = ((Intent)localObject).getComponent();
            if (localObject != null)
            {
              localObject = ((ComponentName)localObject).getClassName();
              str1 = ContactsActivity.class.getName();
              boolean bool3 = ((String)localObject).equals(str1);
              if (bool3)
              {
                localObject = "contactsIcon";
                str1 = "openApp";
                bb.a(paramBundle, (String)localObject, str1);
                break label188;
              }
            }
            localObject = "appIcon";
            str1 = "openApp";
            bb.a(paramBundle, (String)localObject, str1);
          }
        }
      }
    }
    label188:
    paramBundle = TrueApp.y().a().bt();
    f = paramBundle;
  }
  
  public void onDestroy()
  {
    boolean bool = true;
    g = bool;
    super.onDestroy();
    String[] arrayOfString = new String[bool];
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str = getClass().getSimpleName();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append("#onDestroy()");
    localObject = ((StringBuilder)localObject).toString();
    arrayOfString[0] = localObject;
  }
  
  public void onPause()
  {
    super.onPause();
    String[] arrayOfString = new String[1];
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str = getClass().getSimpleName();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append("#onPause()");
    localObject = ((StringBuilder)localObject).toString();
    arrayOfString[0] = localObject;
  }
  
  public void onPostCreate(Bundle paramBundle)
  {
    super.onPostCreate(paramBundle);
    int i = 2131362435;
    paramBundle = (ScrimInsetsFrameLayout)findViewById(i);
    if (paramBundle != null) {
      paramBundle.setOnInsetsCallback(this);
    }
  }
  
  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    int i = g();
    int j = 0;
    for (;;)
    {
      int k = paramMenu.size();
      if (j >= k) {
        break;
      }
      MenuItem localMenuItem = paramMenu.getItem(j);
      at.a(this, localMenuItem, i);
      j += 1;
    }
    return super.onPrepareOptionsMenu(paramMenu);
  }
  
  public void onResume()
  {
    super.onResume();
    Object localObject1 = new String[1];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    String str = getClass().getSimpleName();
    ((StringBuilder)localObject2).append(str);
    ((StringBuilder)localObject2).append("#onResume()");
    localObject2 = ((StringBuilder)localObject2).toString();
    str = null;
    localObject1[0] = localObject2;
    boolean bool = a();
    if (bool)
    {
      localObject1 = f;
      bool = ((l)localObject1).f();
      if (!bool)
      {
        c();
        RequiredPermissionsActivity.a(this);
        finish();
        return;
      }
    }
    supportInvalidateOptionsMenu();
  }
  
  public void onStart()
  {
    super.onStart();
    String[] arrayOfString = new String[1];
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str = getClass().getSimpleName();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append("#onStart()");
    localObject = ((StringBuilder)localObject).toString();
    arrayOfString[0] = localObject;
  }
  
  public void onStop()
  {
    super.onStop();
    String[] arrayOfString = new String[1];
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str = getClass().getSimpleName();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append("#onStop()");
    localObject = ((StringBuilder)localObject).toString();
    arrayOfString[0] = localObject;
  }
  
  public void setSupportActionBar(Toolbar paramToolbar)
  {
    h = paramToolbar;
    super.setSupportActionBar(paramToolbar);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */