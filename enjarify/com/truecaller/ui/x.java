package com.truecaller.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.a.a.a.h;
import com.a.a.ai;
import com.a.a.u;
import com.a.a.v;
import com.truecaller.old.data.a.d;
import com.truecaller.util.e.e;
import com.truecaller.util.e.g;
import java.util.Iterator;

public abstract class x
  extends o
  implements com.truecaller.util.e.f
{
  private final v a;
  
  public x()
  {
    u localu = new com/a/a/u;
    localu.<init>();
    a = localu;
  }
  
  public void a(int paramInt)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putInt("socialType", paramInt);
    android.support.v4.app.f localf = getActivity();
    String str = "com.truecaller.EVENT_APP_SOCIAL_SIGN_IN";
    if (localf != null)
    {
      Intent localIntent = new android/content/Intent;
      localIntent.<init>(str);
      localIntent.putExtras(localBundle);
      localf.sendBroadcast(localIntent);
    }
  }
  
  public final e c(int paramInt)
  {
    return (e)a.a(paramInt);
  }
  
  public void d() {}
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Iterator localIterator = a.a().iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      e locale = (e)nextb;
      locale.a(paramInt1, paramInt2, paramIntent);
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int[] arrayOfInt = d.a;
    int i = arrayOfInt.length;
    int j = 0;
    while (j < i)
    {
      int k = arrayOfInt[j];
      e locale = g.a(getActivity().getApplication(), k).a(this, this);
      locale.a(paramBundle);
      v localv = a;
      localv.a(k, locale);
      j += 1;
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    Iterator localIterator = a.a().iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localIterator.next();
    }
  }
  
  public void onPause()
  {
    super.onPause();
    Iterator localIterator = a.a().iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localIterator.next();
    }
  }
  
  public void onResume()
  {
    super.onResume();
    Iterator localIterator = a.a().iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      e locale = (e)nextb;
      locale.a();
    }
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    Iterator localIterator = a.a().iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      e locale = (e)nextb;
      locale.b(paramBundle);
    }
  }
  
  public void onStart()
  {
    super.onStart();
    Iterator localIterator = a.a().iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localIterator.next();
    }
  }
  
  public void onStop()
  {
    e();
    j = null;
    super.onStop();
    Iterator localIterator = a.a().iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localIterator.next();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */