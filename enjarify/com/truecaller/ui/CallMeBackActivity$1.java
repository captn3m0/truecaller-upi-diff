package com.truecaller.ui;

import android.widget.Toast;

final class CallMeBackActivity$1
  extends com.truecaller.network.util.b
{
  CallMeBackActivity$1(CallMeBackActivity paramCallMeBackActivity, e.b paramb)
  {
    super(paramb);
  }
  
  public final void a(Exception paramException, int paramInt)
  {
    super.a(paramException, paramInt);
    int i = 405;
    if (paramInt != i)
    {
      i = 409;
      if (paramInt != i) {
        i = 2131886537;
      } else {
        i = 2131886300;
      }
    }
    else
    {
      i = 2131886299;
    }
    CallMeBackActivity localCallMeBackActivity = b;
    paramException = localCallMeBackActivity.getString(i);
    Toast.makeText(localCallMeBackActivity, paramException, 1).show();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.CallMeBackActivity.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */