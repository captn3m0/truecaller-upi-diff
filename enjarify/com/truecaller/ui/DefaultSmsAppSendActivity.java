package com.truecaller.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.defaultsms.DefaultSmsActivity;
import com.truecaller.utils.extensions.a;
import com.truecaller.utils.l;

public class DefaultSmsAppSendActivity
  extends Activity
{
  private String a;
  
  private static void a(Context paramContext, String paramString)
  {
    try
    {
      Intent localIntent = new android/content/Intent;
      String str1 = "android.intent.action.SENDTO";
      String str2 = "smsto";
      paramString = Uri.fromParts(str2, paramString, null);
      localIntent.<init>(str1, paramString);
      paramContext.startActivity(localIntent);
      return;
    }
    catch (Exception localException)
    {
      com.truecaller.log.d.a(localException, "Failed to send SMS");
    }
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    paramInt2 = 1;
    if (paramInt1 == paramInt2)
    {
      finish();
      String str = a;
      a(this, str);
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    a.a(this);
    paramBundle = getIntent().getStringExtra("number");
    a = paramBundle;
    paramBundle = getIntent().getStringExtra("AppUserInteraction.Context");
    int i = 0;
    Object localObject1 = new String[0];
    AssertionUtil.isNotNull(paramBundle, (String[])localObject1);
    localObject1 = a;
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1);
    if (bool1)
    {
      finish();
      return;
    }
    localObject1 = TrueApp.y().a().bw();
    Object localObject2 = TrueApp.y().a().bx();
    boolean bool2 = ((com.truecaller.utils.d)localObject2).d();
    int j = 1;
    if (bool2)
    {
      localObject2 = new String[] { "android.permission.SEND_SMS" };
      bool1 = ((l)localObject1).a((String[])localObject2);
      if (bool1) {
        i = 1;
      }
    }
    if (i == 0)
    {
      paramBundle = DefaultSmsActivity.a(this, paramBundle);
      startActivityForResult(paramBundle, j);
      return;
    }
    finish();
    paramBundle = a;
    a(this, paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.DefaultSmsAppSendActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */