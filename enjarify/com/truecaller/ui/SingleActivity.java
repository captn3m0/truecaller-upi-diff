package com.truecaller.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.truecaller.calling.d.f;
import com.truecaller.calling.recorder.bj;
import com.truecaller.profile.EditMeFormFragment;
import com.truecaller.profile.a;
import com.truecaller.service.AlarmReceiver;
import com.truecaller.service.DataManagerService;
import com.truecaller.service.h;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.ui.details.d;
import com.truecaller.wizard.utils.i;

public class SingleActivity
  extends n
{
  private SingleActivity.FragmentSingle a;
  private h b;
  
  public static Intent a(Context paramContext, SingleActivity.FragmentSingle paramFragmentSingle)
  {
    return b(paramContext, paramFragmentSingle);
  }
  
  public static Intent b(Context paramContext, SingleActivity.FragmentSingle paramFragmentSingle)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, SingleActivity.class);
    paramFragmentSingle = paramFragmentSingle.name();
    localIntent.putExtra("ARG_FRAGMENT", paramFragmentSingle);
    localIntent.putExtra("ARG_ACTIONBAR_OVERLAY", false);
    return localIntent;
  }
  
  public static Intent c(Context paramContext, SingleActivity.FragmentSingle paramFragmentSingle)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, SingleActivity.class);
    paramFragmentSingle = paramFragmentSingle.name();
    localIntent.putExtra("ARG_FRAGMENT", paramFragmentSingle);
    localIntent.putExtra("ARG_LAYOUT_RES", 2131559224);
    return localIntent;
  }
  
  public void onCreate(Bundle paramBundle)
  {
    Object localObject1 = getIntent();
    Object localObject2 = "ARG_ACTIONBAR_OVERLAY";
    int i = 0;
    String str1 = null;
    boolean bool1 = ((Intent)localObject1).getBooleanExtra((String)localObject2, false);
    Object localObject3 = new com/truecaller/service/h;
    Class localClass = DataManagerService.class;
    ((h)localObject3).<init>(this, localClass);
    b = ((h)localObject3);
    localObject3 = null;
    boolean bool2 = true;
    if (paramBundle == null)
    {
      Object localObject4 = "android.intent.action.DIAL";
      Object localObject5 = ((Intent)localObject1).getAction();
      boolean bool3 = ((String)localObject4).equals(localObject5);
      if (!bool3)
      {
        localObject4 = "android.intent.action.VIEW";
        localObject5 = ((Intent)localObject1).getAction();
        bool3 = ((String)localObject4).equals(localObject5);
        if (!bool3)
        {
          localObject4 = "ARG_FRAGMENT";
          bool3 = ((Intent)localObject1).hasExtra((String)localObject4);
          if (!bool3) {
            break label241;
          }
          localObject4 = "ARG_FRAGMENT";
          try
          {
            localObject4 = ((Intent)localObject1).getStringExtra((String)localObject4);
            localObject4 = SingleActivity.FragmentSingle.valueOf((String)localObject4);
            a = ((SingleActivity.FragmentSingle)localObject4);
          }
          catch (IllegalArgumentException localIllegalArgumentException)
          {
            localObject4 = new String[bool2];
            localObject5 = new java/lang/StringBuilder;
            ((StringBuilder)localObject5).<init>("Invalid fragment type, ARG_FRAGMENT=");
            String str2 = ((Intent)localObject1).getStringExtra("ARG_FRAGMENT");
            ((StringBuilder)localObject5).append(str2);
            localObject5 = ((StringBuilder)localObject5).toString();
            localObject4[0] = localObject5;
          }
        }
      }
      localObject2 = SingleActivity.FragmentSingle.CALLER;
      a = ((SingleActivity.FragmentSingle)localObject2);
      localObject2 = ((Intent)localObject1).getData().getSchemeSpecificPart();
      localObject4 = DetailsFragment.SourceType.External;
      localObject2 = DetailsFragment.a((String)localObject2, (DetailsFragment.SourceType)localObject4);
      ((Intent)localObject1).putExtras((Bundle)localObject2);
      bool1 = true;
      label241:
      localObject4 = a;
      if (localObject4 == null)
      {
        paramBundle = new String[bool2];
        localObject1 = String.valueOf(localObject1);
        localObject1 = "Invalid intent, no fragment type, intent=".concat((String)localObject1);
        paramBundle[0] = localObject1;
        super.onCreate(null);
        finish();
        return;
      }
    }
    str1 = "ARG_LAYOUT_RES";
    int k = 2131559222;
    i = ((Intent)localObject1).getIntExtra(str1, k);
    if (bool1) {
      i = 2131559223;
    }
    int j = aresId;
    setTheme(j);
    super.onCreate(paramBundle);
    setContentView(i);
    j = 2131364907;
    localObject2 = (Toolbar)findViewById(j);
    if (localObject2 != null) {
      setSupportActionBar((Toolbar)localObject2);
    }
    if (paramBundle == null)
    {
      paramBundle = a;
      localObject2 = SingleActivity.1.a;
      int m = paramBundle.ordinal();
      m = localObject2[m];
      switch (m)
      {
      default: 
        break;
      case 11: 
        localObject3 = new com/truecaller/calling/recorder/bj;
        ((bj)localObject3).<init>();
        break;
      case 10: 
        localObject3 = new com/truecaller/ui/ad;
        ((ad)localObject3).<init>();
        break;
      case 9: 
        localObject3 = new com/truecaller/calling/d/f;
        ((f)localObject3).<init>();
        break;
      case 8: 
        localObject3 = new com/truecaller/ui/details/d;
        ((d)localObject3).<init>();
        break;
      case 7: 
        localObject3 = new com/truecaller/ui/l;
        ((l)localObject3).<init>();
        break;
      case 6: 
        localObject3 = new com/truecaller/ui/SettingsFragment;
        ((SettingsFragment)localObject3).<init>();
        break;
      case 5: 
        localObject3 = new com/truecaller/ui/u;
        ((u)localObject3).<init>();
        break;
      case 4: 
        localObject3 = new com/truecaller/ui/t;
        ((t)localObject3).<init>();
        break;
      case 3: 
        localObject3 = new com/truecaller/profile/EditMeFormFragment;
        ((EditMeFormFragment)localObject3).<init>();
        break;
      case 2: 
        localObject3 = new com/truecaller/profile/a;
        ((a)localObject3).<init>();
        break;
      case 1: 
        localObject3 = new com/truecaller/ui/details/DetailsFragment;
        ((DetailsFragment)localObject3).<init>();
      }
      paramBundle = ((Intent)localObject1).getExtras();
      ((o)localObject3).setArguments(paramBundle);
      a((o)localObject3);
    }
    paramBundle = getSupportActionBar();
    if (paramBundle != null) {
      paramBundle.setDisplayHomeAsUpEnabled(bool2);
    }
    AlarmReceiver.a(getIntent());
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    h localh = b;
    if (localh != null) {
      localh.b();
    }
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    boolean bool1 = super.onOptionsItemSelected(paramMenuItem);
    boolean bool2 = true;
    if (bool1) {
      return bool2;
    }
    int j = paramMenuItem.getItemId();
    int i = 16908332;
    if (j == i)
    {
      onBackPressed();
      return bool2;
    }
    return false;
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    i.a(paramArrayOfString, paramArrayOfInt);
  }
  
  public void onStart()
  {
    super.onStart();
    b.a();
  }
  
  public void onStop()
  {
    super.onStop();
    b.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.SingleActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */