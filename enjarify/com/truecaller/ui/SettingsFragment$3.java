package com.truecaller.ui;

import android.view.View;
import com.truecaller.common.e.e;
import com.truecaller.old.data.access.Settings;
import com.truecaller.old.data.access.d;
import com.truecaller.old.data.entity.b;
import com.truecaller.ui.components.ComboBase;
import com.truecaller.ui.components.ComboBase.a;
import com.truecaller.ui.components.n;
import com.truecaller.utils.extensions.t;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

final class SettingsFragment$3
  extends com.truecaller.old.a.a
{
  List a;
  b c;
  Locale d;
  b e;
  String f;
  
  SettingsFragment$3(SettingsFragment paramSettingsFragment, com.truecaller.old.a.c paramc, ComboBase paramComboBase)
  {
    super(paramc, (byte)0);
  }
  
  public final void a(Object paramObject)
  {
    paramObject = h;
    boolean bool1 = ((SettingsFragment)paramObject).s();
    if (bool1)
    {
      paramObject = g;
      boolean bool2 = true;
      float f1 = 0.5F;
      t.a((View)paramObject, bool2, f1);
      paramObject = g;
      Object localObject = a;
      ((ComboBase)paramObject).setData((List)localObject);
      paramObject = g;
      localObject = c;
      ((ComboBase)paramObject).setSelection((n)localObject);
      g.a();
      paramObject = g;
      localObject = new com/truecaller/ui/-$$Lambda$SettingsFragment$3$JRYrdN79Nez4PDcuFODyv9LOSko;
      ((-..Lambda.SettingsFragment.3.JRYrdN79Nez4PDcuFODyv9LOSko)localObject).<init>(this);
      ((ComboBase)paramObject).a((ComboBase.a)localObject);
    }
  }
  
  protected final Object doInBackground(Object... paramVarArgs)
  {
    paramVarArgs = new java/util/ArrayList;
    paramVarArgs.<init>();
    a = paramVarArgs;
    SettingsFragment.a(h);
    paramVarArgs = e.c();
    d = paramVarArgs;
    paramVarArgs = d;
    Object localObject2;
    boolean bool1;
    Object localObject3;
    if (paramVarArgs != null)
    {
      paramVarArgs = d.a(paramVarArgs);
      localObject1 = new com/truecaller/old/data/entity/b;
      localObject2 = f;
      bool1 = true;
      localObject3 = new Object[bool1];
      String str = a.a;
      localObject3[0] = str;
      localObject2 = String.format((String)localObject2, (Object[])localObject3);
      localObject3 = a.b;
      paramVarArgs = a.c;
      ((b)localObject1).<init>((String)localObject2, (String)localObject3, paramVarArgs);
      e = ((b)localObject1);
      paramVarArgs = "languageAuto";
      boolean bool2 = Settings.e(paramVarArgs);
      if (bool2)
      {
        paramVarArgs = e;
        c = paramVarArgs;
      }
      paramVarArgs = a;
      localObject1 = e;
      paramVarArgs.add(localObject1);
    }
    paramVarArgs = new com/truecaller/common/e/a;
    paramVarArgs.<init>();
    Object localObject1 = d.a().iterator();
    for (;;)
    {
      boolean bool3 = ((Iterator)localObject1).hasNext();
      if (!bool3) {
        break;
      }
      localObject2 = (b)((Iterator)localObject1).next();
      localObject3 = a.a;
      bool1 = paramVarArgs.a((String)localObject3);
      if (bool1)
      {
        localObject3 = a;
        ((List)localObject3).add(localObject2);
      }
    }
    paramVarArgs = c;
    if (paramVarArgs == null)
    {
      paramVarArgs = d.a(Settings.b("language"));
      c = paramVarArgs;
    }
    return null;
  }
  
  public final void onPreExecute()
  {
    t.a(g, false, 0.5F);
    String str = h.getString(2131887073);
    f = str;
    super.onPreExecute();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.SettingsFragment.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */