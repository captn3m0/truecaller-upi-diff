package com.truecaller.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import c.g.b.k;

public final class NotificationAccessActivity$a
{
  public static Intent a(Context paramContext, Intent paramIntent)
  {
    k.b(paramContext, "context");
    Object localObject = new android/content/Intent;
    Class localClass = NotificationAccessActivity.class;
    ((Intent)localObject).<init>(paramContext, localClass);
    int i = 2131886633;
    paramContext = ((Intent)localObject).putExtra("toastMessage", i);
    if (paramIntent != null)
    {
      localObject = "goBackIntent";
      paramIntent = (Parcelable)paramIntent;
      paramContext.putExtra((String)localObject, paramIntent);
    }
    paramContext = paramContext.setFlags(335609856);
    k.a(paramContext, "notificationAccessActivi…t.FLAG_ACTIVITY_NEW_TASK)");
    return paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.NotificationAccessActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */