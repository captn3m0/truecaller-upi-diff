package com.truecaller.ui.clicktocall;

import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.util.at;

public class CallConfirmationActivity
  extends AppCompatActivity
  implements g
{
  e a;
  private TextView b;
  private TextView c;
  
  public final void a(String paramString1, String paramString2)
  {
    c.setText(paramString1);
    b.setText(paramString2);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    com.truecaller.utils.extensions.a.a(this);
    paramBundle = getTheme();
    Object localObject1 = ThemeManager.a();
    int i = resId;
    paramBundle.applyStyle(i, false);
    paramBundle = e.a(getIntent());
    if (paramBundle == null)
    {
      finish();
      return;
    }
    localObject1 = new com/truecaller/ui/clicktocall/h$a;
    ((h.a)localObject1).<init>((byte)0);
    Object localObject2 = (bp)dagger.a.g.a(((bk)getApplicationContext()).a());
    b = ((bp)localObject2);
    localObject2 = new com/truecaller/ui/clicktocall/b;
    ((b)localObject2).<init>(paramBundle);
    paramBundle = (b)dagger.a.g.a(localObject2);
    a = paramBundle;
    dagger.a.g.a(a, b.class);
    dagger.a.g.a(b, bp.class);
    paramBundle = new com/truecaller/ui/clicktocall/h;
    localObject2 = a;
    localObject1 = b;
    paramBundle.<init>((b)localObject2, (bp)localObject1, (byte)0);
    paramBundle.a(this);
    setContentView(2131558520);
    paramBundle = (TextView)findViewById(2131365291);
    c = paramBundle;
    paramBundle = (TextView)findViewById(2131365225);
    b = paramBundle;
    paramBundle = (Button)findViewById(2131362308);
    localObject1 = new com/truecaller/ui/clicktocall/-$$Lambda$CallConfirmationActivity$yfXh22hWkb67YKoW54NnQpdDdcY;
    ((-..Lambda.CallConfirmationActivity.yfXh22hWkb67YKoW54NnQpdDdcY)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    at.d(paramBundle, 2130969528);
    paramBundle = (Button)findViewById(2131362310);
    localObject1 = new com/truecaller/ui/clicktocall/-$$Lambda$CallConfirmationActivity$ueoyzx6FKKTpJulG1kROGGEqOco;
    ((-..Lambda.CallConfirmationActivity.ueoyzx6FKKTpJulG1kROGGEqOco)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    at.d(paramBundle, 2130969592);
    a.a(this);
  }
  
  public void onDestroy()
  {
    e locale = a;
    if (locale != null) {
      locale.y_();
    }
    super.onDestroy();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.clicktocall.CallConfirmationActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */