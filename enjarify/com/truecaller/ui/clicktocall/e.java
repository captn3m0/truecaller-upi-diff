package com.truecaller.ui.clicktocall;

import android.content.Intent;
import android.net.Uri;
import com.truecaller.bb;

abstract class e
  extends bb
{
  static String a(Intent paramIntent)
  {
    paramIntent = paramIntent.getData();
    if (paramIntent != null)
    {
      String str1 = "truecaller";
      try
      {
        String str2 = paramIntent.getScheme();
        boolean bool = str1.equals(str2);
        if (bool)
        {
          str1 = "call_confirmation";
          str2 = paramIntent.getHost();
          bool = str1.equals(str2);
          if (bool)
          {
            str1 = "tel";
            return paramIntent.getQueryParameter(str1);
          }
        }
      }
      catch (UnsupportedOperationException|NullPointerException localUnsupportedOperationException) {}
    }
    return null;
  }
  
  abstract void a();
}

/* Location:
 * Qualified Name:     com.truecaller.ui.clicktocall.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */