package com.truecaller.ui.clicktocall;

import android.os.AsyncTask;
import com.truecaller.calling.initiate_call.b.a;
import com.truecaller.calling.initiate_call.b.a.a;
import com.truecaller.network.search.j;

final class f
  extends e
{
  final String a;
  private final j c;
  private final String d;
  private AsyncTask e;
  private final com.truecaller.analytics.b f;
  private final com.truecaller.calling.initiate_call.b g;
  
  f(String paramString1, j paramj, String paramString2, com.truecaller.analytics.b paramb, com.truecaller.calling.initiate_call.b paramb1)
  {
    c = paramj;
    d = paramString2;
    a = paramString1;
    f = paramb;
    g = paramb1;
  }
  
  public final void a()
  {
    Object localObject1 = new com/truecaller/calling/initiate_call/b$a$a;
    Object localObject2 = a;
    String str = d;
    ((b.a.a)localObject1).<init>((String)localObject2, str);
    localObject2 = g;
    localObject1 = ((b.a.a)localObject1).a();
    ((com.truecaller.calling.initiate_call.b)localObject2).a((b.a)localObject1);
  }
  
  public final void y_()
  {
    super.y_();
    AsyncTask localAsyncTask = e;
    if (localAsyncTask != null)
    {
      boolean bool = true;
      localAsyncTask.cancel(bool);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.clicktocall.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */