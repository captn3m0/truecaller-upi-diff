package com.truecaller.ui;

import android.os.AsyncTask;
import com.truecaller.common.account.r;
import java.lang.ref.WeakReference;

final class SettingsFragment$a
  extends AsyncTask
{
  private final WeakReference a;
  private final r b;
  
  SettingsFragment$a(SettingsFragment paramSettingsFragment, r paramr)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramSettingsFragment);
    a = localWeakReference;
    b = paramr;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.SettingsFragment.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */