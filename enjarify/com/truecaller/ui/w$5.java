package com.truecaller.ui;

import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.RelativeLayout.LayoutParams;
import com.truecaller.ui.components.SnappingRelativeLayout;

final class w$5
  extends Animation
{
  w$5(w paramw, int paramInt) {}
  
  protected final void applyTransformation(float paramFloat, Transformation paramTransformation)
  {
    paramTransformation = (RelativeLayout.LayoutParams)b.d.getLayoutParams();
    int i = a;
    int j = (int)(i * paramFloat);
    i -= j;
    paramTransformation.setMargins(0, i, 0, 0);
    b.c.forceLayout();
    b.c.requestLayout();
    b.c.invalidate();
    float f = 255.0F;
    paramFloat *= f;
    int k = (int)(f - paramFloat);
    w.a(b, k);
  }
  
  public final boolean willChangeBounds()
  {
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.w.5
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */