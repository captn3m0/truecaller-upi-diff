package com.truecaller.ui;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.Behavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;

final class TruecallerInit$5
  extends AppBarLayout.Behavior
{
  TruecallerInit$5(TruecallerInit paramTruecallerInit) {}
  
  public final void a(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, View paramView, int paramInt1, int paramInt2, int[] paramArrayOfInt, int paramInt3)
  {
    boolean bool = paramView instanceof RecyclerView;
    if (bool)
    {
      localObject = paramView;
      localObject = (RecyclerView)paramView;
      int i = 1;
      bool = ((RecyclerView)localObject).canScrollVertically(i);
      if (!bool)
      {
        paramInt2 = 0;
        j = 0;
        break label50;
      }
    }
    int j = paramInt2;
    label50:
    Object localObject = this;
    super.a(paramCoordinatorLayout, paramAppBarLayout, (View)paramView, paramInt1, j, paramArrayOfInt, paramInt3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.TruecallerInit.5
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */