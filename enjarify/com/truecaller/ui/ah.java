package com.truecaller.ui;

import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ac;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.b.e;
import com.truecaller.wizard.TruecallerWizard;
import com.truecaller.wizard.d.c;

public class ah
  extends TruecallerWizard
{
  public final void b()
  {
    super.b();
    e.b("wizard_OEMMode", false);
    PackageManager localPackageManager = getPackageManager();
    ComponentName localComponentName = new android/content/ComponentName;
    localComponentName.<init>(this, ah.class);
    localPackageManager.setComponentEnabledSetting(localComponentName, 2, 1);
    ac.a(this).a(null, 2131362836);
  }
  
  public final c c()
  {
    Object localObject = ((bk)getApplication()).a();
    WizardActivity.a locala = new com/truecaller/ui/WizardActivity$a;
    localObject = ((bp)localObject).ad();
    locala.<init>((f)localObject);
    return locala;
  }
  
  public void onCreate(Bundle paramBundle)
  {
    e.b("wizard_OEMMode", true);
    e.b("signUpOrigin", "deviceSetup");
    super.onCreate(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */