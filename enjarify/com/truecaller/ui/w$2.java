package com.truecaller.ui;

import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.RelativeLayout.LayoutParams;
import com.truecaller.ui.components.SnappingRelativeLayout;

final class w$2
  implements Animation.AnimationListener
{
  w$2(w paramw, int paramInt) {}
  
  public final void onAnimationEnd(Animation paramAnimation)
  {
    w.e(b);
    b.d.setAnimation(null);
    paramAnimation = (RelativeLayout.LayoutParams)b.d.getLayoutParams();
    int i = a;
    paramAnimation.setMargins(0, i, 0, 0);
    b.c.forceLayout();
    b.c.requestLayout();
    b.c.invalidate();
    w.a(b, true);
  }
  
  public final void onAnimationRepeat(Animation paramAnimation) {}
  
  public final void onAnimationStart(Animation paramAnimation) {}
}

/* Location:
 * Qualified Name:     com.truecaller.ui.w.2
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */