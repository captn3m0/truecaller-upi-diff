package com.truecaller.ui;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.truecaller.old.data.access.Settings;
import com.truecaller.ui.components.FeedbackItemView;
import com.truecaller.ui.components.FeedbackItemView.DisplaySource;
import com.truecaller.ui.components.FeedbackItemView.FeedbackItem;
import com.truecaller.ui.components.FeedbackItemView.FeedbackItem.FeedbackItemState;
import com.truecaller.ui.components.j;
import com.truecaller.util.at;
import com.truecaller.utils.ui.b;

public abstract class r
  extends o
{
  protected void C_() {}
  
  protected final void a(ListAdapter paramListAdapter)
  {
    ListView localListView = v();
    if (localListView != null) {
      localListView.setAdapter(paramListAdapter);
    }
  }
  
  protected final void a(j paramj)
  {
    Object localObject1 = d();
    if ((paramj != null) && (localObject1 != null))
    {
      Object localObject2 = a;
      if (localObject2 == null)
      {
        int i = paramj.getItemCount();
        if (i >= 0)
        {
          localObject2 = getActivity();
          localObject1 = FeedbackItemView.a((FeedbackItemView.DisplaySource)localObject1, (Context)localObject2);
          if (localObject1 == null) {
            return;
          }
          localObject2 = a;
          boolean bool = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject2).shouldClose();
          if (bool)
          {
            paramj.d(null);
            return;
          }
          localObject2 = a;
          bool = ((FeedbackItemView.FeedbackItem.FeedbackItemState)localObject2).isInviteState();
          if (bool)
          {
            localObject2 = "INVITE_LAST_ASKED";
            Settings.g((String)localObject2);
          }
          else
          {
            localObject2 = "GOOGLE_REVIEW_ASK_TIMESTAMP";
            Settings.g((String)localObject2);
          }
          paramj.d((FeedbackItemView.FeedbackItem)localObject1);
          return;
        }
      }
    }
  }
  
  protected final void a(CharSequence paramCharSequence, int paramInt)
  {
    a(paramCharSequence, null, paramInt);
  }
  
  protected final void a(CharSequence paramCharSequence, String paramString, int paramInt)
  {
    TextView localTextView = b();
    at.b(localTextView, paramCharSequence);
    at.b(u(), paramString);
    paramCharSequence = t();
    if ((paramCharSequence != null) && (paramInt != 0))
    {
      paramString = getContext();
      int i = 2130969591;
      int j = b.a(paramString, i);
      b.a(paramCharSequence, j);
      paramString = b.c(getContext(), paramInt);
      at.a(paramCharSequence, paramString);
    }
  }
  
  protected TextView b()
  {
    View localView = getView();
    if (localView == null) {
      return null;
    }
    return (TextView)localView.findViewById(2131363652);
  }
  
  protected final void c(boolean paramBoolean)
  {
    Object localObject = getActivity();
    if (localObject == null) {
      return;
    }
    localObject = getView();
    if (localObject == null)
    {
      localObject = null;
    }
    else
    {
      int i = 2131363685;
      localObject = ((View)localObject).findViewById(i);
    }
    if (localObject != null)
    {
      if (paramBoolean) {
        paramBoolean = false;
      } else {
        paramBoolean = true;
      }
      ((View)localObject).setVisibility(paramBoolean);
    }
  }
  
  protected FeedbackItemView.DisplaySource d()
  {
    return FeedbackItemView.DisplaySource.OTHER;
  }
  
  protected final ImageView t()
  {
    View localView = getView();
    if (localView == null) {
      return null;
    }
    return (ImageView)localView.findViewById(2131363650);
  }
  
  protected final TextView u()
  {
    View localView = getView();
    if (localView == null) {
      return null;
    }
    return (TextView)localView.findViewById(2131363651);
  }
  
  protected final ListView v()
  {
    View localView = getView();
    if (localView == null)
    {
      localView = null;
    }
    else
    {
      int i = 2131363647;
      localView = localView.findViewById(i);
    }
    return (ListView)localView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */