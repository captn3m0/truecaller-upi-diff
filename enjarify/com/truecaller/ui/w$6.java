package com.truecaller.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.log.f;
import com.truecaller.old.a.a;
import com.truecaller.util.aw;
import com.truecaller.utils.extensions.b;

final class w$6
  extends a
{
  w$6(w paramw, String paramString, Bitmap paramBitmap) {}
  
  public final void a(Object paramObject)
  {
    if (paramObject != null)
    {
      w localw = d;
      paramObject = (Bitmap)paramObject;
      localw.a((Bitmap)paramObject);
    }
  }
  
  protected final Object doInBackground(Object... paramVarArgs)
  {
    int i = 1;
    try
    {
      String str1 = a;
      str1 = w.f(str1);
      Object localObject1 = c;
      Object localObject2 = TrueApp.x();
      int j = 600;
      localObject1 = b.a((Bitmap)localObject1, (Context)localObject2, j);
      localObject2 = TrueApp.x();
      Object localObject3 = "receiver$0";
      k.b(localObject1, (String)localObject3);
      localObject3 = "context";
      k.b(localObject2, (String)localObject3);
      localObject2 = RenderScript.create((Context)localObject2);
      localObject3 = Allocation.createFromBitmap((RenderScript)localObject2, (Bitmap)localObject1);
      Object localObject4 = Element.U8_4((RenderScript)localObject2);
      localObject4 = ScriptIntrinsicBlur.create((RenderScript)localObject2, (Element)localObject4);
      int k = 1101004800;
      float f = 20.0F;
      ((ScriptIntrinsicBlur)localObject4).setRadius(f);
      ((ScriptIntrinsicBlur)localObject4).setInput((Allocation)localObject3);
      j = ((Bitmap)localObject1).getWidth();
      k = ((Bitmap)localObject1).getHeight();
      localObject1 = b.a((Bitmap)localObject1);
      localObject1 = Bitmap.createBitmap(j, k, (Bitmap.Config)localObject1);
      localObject3 = Allocation.createFromBitmap((RenderScript)localObject2, (Bitmap)localObject1);
      ((ScriptIntrinsicBlur)localObject4).forEach((Allocation)localObject3);
      ((Allocation)localObject3).copyTo((Bitmap)localObject1);
      ((RenderScript)localObject2).destroy();
      localObject2 = "result";
      k.a(localObject1, (String)localObject2);
      if ((localObject1 != null) && (str1 != null))
      {
        localObject2 = d;
        localObject2 = ((w)localObject2).getActivity();
        aw.b((Context)localObject2);
        aw.a(str1, (Bitmap)localObject1);
      }
      return localObject1;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      arrayOfString = new String[i];
      String str2 = f.a(localOutOfMemoryError);
      arrayOfString[0] = str2;
    }
    catch (Exception localException)
    {
      String[] arrayOfString = new String[i];
      String str3 = f.a(localException);
      arrayOfString[0] = str3;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.w.6
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */