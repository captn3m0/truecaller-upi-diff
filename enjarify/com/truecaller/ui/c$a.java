package com.truecaller.ui;

import android.content.Context;

public final class c$a
{
  public boolean a;
  public boolean b;
  public boolean c;
  public int d = 10;
  public int e = -1;
  int f = 6;
  private final Context g;
  
  public c$a(Context paramContext)
  {
    g = paramContext;
  }
  
  public final a a(boolean paramBoolean)
  {
    a = paramBoolean;
    return this;
  }
  
  public final c a()
  {
    c localc = new com/truecaller/ui/c;
    Context localContext = g;
    localc.<init>(localContext, this, (byte)0);
    return localc;
  }
  
  public final a b()
  {
    b = false;
    return this;
  }
  
  public final a c()
  {
    d = 6;
    return this;
  }
  
  public final a d()
  {
    e = 12;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.ui.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */