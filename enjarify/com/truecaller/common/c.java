package com.truecaller.common;

import c.f;
import c.g.a.a;
import c.g.b.k;
import c.g.b.r;
import c.g.b.s;
import c.g.b.w;
import c.n.m;
import com.truecaller.common.network.KnownDomain;
import java.util.Iterator;
import java.util.List;

public final class c
{
  private static final f b = c.g.a((a)c.a.a);
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/s;
    c.l.c localc = w.a(c.class, "common_release");
    ((s)localObject).<init>(localc, "region1CountryList", "getRegion1CountryList()Ljava/util/List;");
    localObject = (c.l.g)w.a((r)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public static final KnownDomain a(String paramString)
  {
    k.b(paramString, "receiver$0");
    Iterator localIterator = ((Iterable)a()).iterator();
    boolean bool3;
    do
    {
      bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = localIterator.next();
      Object localObject2 = localObject1;
      localObject2 = (String)localObject1;
      boolean bool2 = true;
      bool3 = m.a((String)localObject2, paramString, bool2);
    } while (!bool3);
    break label70;
    boolean bool1 = false;
    Object localObject1 = null;
    label70:
    localObject1 = (String)localObject1;
    if (localObject1 != null)
    {
      paramString = KnownDomain.DOMAIN_REGION_1;
      if (paramString != null) {}
    }
    else
    {
      paramString = KnownDomain.DOMAIN_OTHER_REGIONS;
    }
    return paramString;
  }
  
  public static final List a()
  {
    return (List)b.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */