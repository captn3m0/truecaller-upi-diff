package com.truecaller.common.b;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public final class e
{
  public static SharedPreferences a;
  
  public static int a(String paramString, int paramInt)
  {
    SharedPreferences localSharedPreferences = a;
    long l = paramInt;
    return (int)localSharedPreferences.getLong(paramString, l);
  }
  
  public static long a(String paramString, long paramLong)
  {
    return a.getLong(paramString, paramLong);
  }
  
  public static String a(String paramString)
  {
    return a.getString(paramString, "");
  }
  
  public static String a(String paramString1, String paramString2)
  {
    return a.getString(paramString1, paramString2);
  }
  
  public static boolean a(String paramString, boolean paramBoolean)
  {
    return a.getBoolean(paramString, paramBoolean);
  }
  
  public static void b(String paramString)
  {
    a.edit().remove(paramString).apply();
  }
  
  public static void b(String paramString, int paramInt)
  {
    SharedPreferences.Editor localEditor = a.edit();
    long l = paramInt;
    localEditor.putLong(paramString, l).apply();
  }
  
  public static void b(String paramString, long paramLong)
  {
    a.edit().putLong(paramString, paramLong).apply();
  }
  
  public static void b(String paramString1, String paramString2)
  {
    a.edit().putString(paramString1, paramString2).apply();
  }
  
  public static void b(String paramString, boolean paramBoolean)
  {
    a.edit().putBoolean(paramString, paramBoolean).apply();
  }
  
  public static boolean c(String paramString)
  {
    return a.contains(paramString);
  }
  
  public static boolean c(String paramString, long paramLong)
  {
    long l1 = System.currentTimeMillis();
    long l2 = a(paramString, 0L);
    l1 -= l2;
    boolean bool = l1 < paramLong;
    return bool;
  }
  
  public static void d(String paramString)
  {
    long l = System.currentTimeMillis();
    b(paramString, l);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */