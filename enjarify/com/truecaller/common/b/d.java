package com.truecaller.common.b;

import javax.inject.Provider;

public final class d
  implements dagger.a.d
{
  private final a.a a;
  private final Provider b;
  private final Provider c;
  
  private d(a.a parama, Provider paramProvider1, Provider paramProvider2)
  {
    a = parama;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static d a(a.a parama, Provider paramProvider1, Provider paramProvider2)
  {
    d locald = new com/truecaller/common/b/d;
    locald.<init>(parama, paramProvider1, paramProvider2);
    return locald;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */