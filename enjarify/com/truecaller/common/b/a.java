package com.truecaller.common.b;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.j;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;
import com.truecaller.common.account.r;
import com.truecaller.common.h.am;
import com.truecaller.common.h.h;
import com.truecaller.log.AssertionUtil;

public abstract class a
  extends Application
{
  private static volatile a a;
  private static boolean b;
  public com.truecaller.common.background.b c;
  private boolean d = false;
  
  public a()
  {
    b = false;
    a = this;
    AssertionUtil.setIsDebugBuild(false);
  }
  
  public static a F()
  {
    a locala = a;
    String[] arrayOfString = null;
    boolean bool;
    if (locala != null)
    {
      bool = true;
    }
    else
    {
      bool = false;
      locala = null;
    }
    arrayOfString = new String[0];
    AssertionUtil.isTrue(bool, arrayOfString);
    return a;
  }
  
  public static boolean G()
  {
    return b;
  }
  
  public final String H()
  {
    return am.n(u().k().a());
  }
  
  public final com.truecaller.common.background.b I()
  {
    return c;
  }
  
  public final void J()
  {
    c.a();
  }
  
  public abstract Intent a(Context paramContext);
  
  public void a(Activity paramActivity) {}
  
  public void a(boolean paramBoolean)
  {
    u().e().c();
    J();
  }
  
  public final void a(int... paramVarArgs)
  {
    c.a(10004, paramVarArgs);
  }
  
  public abstract boolean a(j paramj);
  
  public boolean a(String paramString1, boolean paramBoolean, String paramString2)
  {
    boolean bool1 = true;
    if (paramString1 != null)
    {
      r localr = u().k();
      bool2 = localr.a(paramString1, paramString2);
      if (bool2)
      {
        bool2 = true;
        break label45;
      }
    }
    boolean bool2 = false;
    paramString1 = null;
    label45:
    if ((!bool2) && (!paramBoolean)) {
      return false;
    }
    a(paramBoolean);
    return bool1;
  }
  
  public abstract com.truecaller.common.profile.e b();
  
  public abstract com.truecaller.common.f.c c();
  
  public abstract com.truecaller.common.f.b d();
  
  public abstract com.truecaller.common.h.c e();
  
  public abstract com.truecaller.featuretoggles.e f();
  
  public abstract com.truecaller.content.d.a g();
  
  public abstract Boolean h();
  
  protected void i()
  {
    String str = w();
    e.a = getSharedPreferences(str, 0);
  }
  
  protected void j()
  {
    h.a(getApplicationContext());
  }
  
  public abstract String k();
  
  public abstract String l();
  
  public abstract boolean m();
  
  public abstract String n();
  
  public boolean o()
  {
    return true;
  }
  
  public void onCreate()
  {
    Object localObject = new com/truecaller/common/background/d;
    ((com.truecaller.common.background.d)localObject).<init>(this);
    c = ((com.truecaller.common.background.b)localObject);
    super.onCreate();
    boolean bool1 = true;
    -..Lambda.LM_tgxF0BTlT3KWXc2q6MILp_BM localLM_tgxF0BTlT3KWXc2q6MILp_BM = null;
    try
    {
      ProviderInstaller.a(this);
      d = bool1;
    }
    catch (GooglePlayServicesRepairableException|GooglePlayServicesNotAvailableException localGooglePlayServicesRepairableException)
    {
      d = false;
    }
    i();
    boolean bool2 = b;
    if (!bool2)
    {
      String str = "qaEnableLogging";
      bool2 = e.a(str, false);
      if (!bool2)
      {
        bool1 = false;
        localObject = null;
      }
    }
    com.truecaller.log.f.a = bool1;
    localObject = c;
    localLM_tgxF0BTlT3KWXc2q6MILp_BM = new com/truecaller/common/b/-$$Lambda$LM_tgxF0BTlT3KWXc2q6MILp_BM;
    localLM_tgxF0BTlT3KWXc2q6MILp_BM.<init>(this);
    ((com.truecaller.common.background.b)localObject).a(localLM_tgxF0BTlT3KWXc2q6MILp_BM);
    c.e();
  }
  
  public abstract boolean p();
  
  public boolean q()
  {
    return false;
  }
  
  public abstract com.truecaller.common.a u();
  
  public abstract com.truecaller.analytics.d v();
  
  public abstract String w();
}

/* Location:
 * Qualified Name:     com.truecaller.common.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */