package com.truecaller.common.b;

import c.g.b.k;
import com.truecaller.log.b;

public final class g
  implements Thread.UncaughtExceptionHandler
{
  private final Thread.UncaughtExceptionHandler a;
  
  public g(Thread.UncaughtExceptionHandler paramUncaughtExceptionHandler)
  {
    a = paramUncaughtExceptionHandler;
  }
  
  public final void uncaughtException(Thread paramThread, Throwable paramThrowable)
  {
    Object localObject = "thread";
    k.b(paramThread, (String)localObject);
    boolean bool = paramThrowable instanceof VirtualMachineError;
    if (!bool)
    {
      localObject = b.a(paramThrowable);
      if (localObject == null)
      {
        localObject = new com/truecaller/common/b/f;
        if (paramThrowable != null) {
          paramThrowable = paramThrowable.getClass();
        } else {
          paramThrowable = null;
        }
        ((f)localObject).<init>(paramThrowable);
        paramThrowable = (Throwable)localObject;
        paramThrowable = (Throwable)localObject;
      }
      else
      {
        paramThrowable = (Throwable)localObject;
      }
    }
    localObject = a;
    if (localObject != null)
    {
      ((Thread.UncaughtExceptionHandler)localObject).uncaughtException(paramThread, (Throwable)paramThrowable);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */