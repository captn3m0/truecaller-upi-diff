package com.truecaller.common.b;

import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d
{
  private final a.a a;
  private final Provider b;
  private final Provider c;
  
  private c(a.a parama, Provider paramProvider1, Provider paramProvider2)
  {
    a = parama;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static c a(a.a parama, Provider paramProvider1, Provider paramProvider2)
  {
    c localc = new com/truecaller/common/b/c;
    localc.<init>(parama, paramProvider1, paramProvider2);
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */