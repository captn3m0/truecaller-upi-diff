package com.truecaller.common.account;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.text.TextUtils;
import com.truecaller.common.R.string;

public final class n
  extends AbstractAccountAuthenticator
{
  protected final Context a;
  
  public n(Context paramContext)
  {
    super(paramContext);
    a = paramContext;
  }
  
  public final Bundle addAccount(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, String paramString1, String paramString2, String[] paramArrayOfString, Bundle paramBundle)
  {
    paramAccountAuthenticatorResponse = new android/content/Intent;
    paramAccountAuthenticatorResponse.<init>("android.intent.action.MAIN");
    paramString1 = a.getApplicationInfo().packageName;
    paramAccountAuthenticatorResponse.setPackage(paramString1);
    paramAccountAuthenticatorResponse.addCategory("android.intent.category.DEFAULT");
    paramAccountAuthenticatorResponse.setFlags(268435456);
    paramString1 = new android/os/Bundle;
    paramString1.<init>(1);
    paramString1.putParcelable("intent", paramAccountAuthenticatorResponse);
    return paramString1;
  }
  
  public final Bundle confirmCredentials(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, Account paramAccount, Bundle paramBundle)
  {
    paramAccountAuthenticatorResponse = new android/os/Bundle;
    int i = 1;
    paramAccountAuthenticatorResponse.<init>(i);
    paramAccountAuthenticatorResponse.putBoolean("booleanResult", i);
    return paramAccountAuthenticatorResponse;
  }
  
  public final Bundle editProperties(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, String paramString)
  {
    return null;
  }
  
  public final Bundle getAuthToken(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, Account paramAccount, String paramString, Bundle paramBundle)
  {
    paramAccountAuthenticatorResponse = "com.truecaller.auth_token_default";
    boolean bool1 = paramAccountAuthenticatorResponse.equals(paramString);
    int i = 7;
    int j = 2;
    if (!bool1)
    {
      paramAccountAuthenticatorResponse = "installation_id";
      bool1 = paramAccountAuthenticatorResponse.equals(paramString);
      if (!bool1)
      {
        paramAccountAuthenticatorResponse = new android/os/Bundle;
        paramAccountAuthenticatorResponse.<init>(j);
        paramAccountAuthenticatorResponse.putInt("errorCode", i);
        paramAccountAuthenticatorResponse.putString("errorMessage", "Unsupported auth token type");
        return paramAccountAuthenticatorResponse;
      }
    }
    paramAccountAuthenticatorResponse = a;
    int k = R.string.authenticator_account_type;
    paramAccountAuthenticatorResponse = paramAccountAuthenticatorResponse.getString(k);
    String str = type;
    boolean bool3 = paramAccountAuthenticatorResponse.equals(str);
    if (!bool3)
    {
      paramAccountAuthenticatorResponse = new android/os/Bundle;
      paramAccountAuthenticatorResponse.<init>(j);
      paramAccountAuthenticatorResponse.putInt("errorCode", i);
      paramAccountAuthenticatorResponse.putString("errorMessage", "Wrong account type");
      return paramAccountAuthenticatorResponse;
    }
    paramBundle = AccountManager.get(a);
    paramString = paramBundle.peekAuthToken(paramAccount, paramString);
    boolean bool2 = TextUtils.isEmpty(paramString);
    if (!bool2)
    {
      paramBundle = new android/os/Bundle;
      paramBundle.<init>(3);
      paramAccount = name;
      paramBundle.putString("authAccount", paramAccount);
      paramBundle.putString("accountType", paramAccountAuthenticatorResponse);
      paramBundle.putString("authtoken", paramString);
      return paramBundle;
    }
    paramAccountAuthenticatorResponse = new android/os/Bundle;
    paramAccountAuthenticatorResponse.<init>(j);
    paramAccountAuthenticatorResponse.putInt("errorCode", 5);
    paramAccountAuthenticatorResponse.putString("errorMessage", "Missing register ID");
    return paramAccountAuthenticatorResponse;
  }
  
  public final String getAuthTokenLabel(String paramString)
  {
    String str = "installation_id";
    boolean bool = str.equals(paramString);
    if (bool)
    {
      paramString = a;
      int i = R.string.authenticator_account_name;
      return paramString.getString(i);
    }
    return null;
  }
  
  public final Bundle hasFeatures(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, Account paramAccount, String[] paramArrayOfString)
  {
    return null;
  }
  
  public final Bundle updateCredentials(AccountAuthenticatorResponse paramAccountAuthenticatorResponse, Account paramAccount, String paramString, Bundle paramBundle)
  {
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */