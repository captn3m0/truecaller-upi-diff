package com.truecaller.common.account;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.backup.BackupManager;
import c.a.f;
import c.f.b;
import c.g.b.k;
import com.truecaller.common.g.a;
import com.truecaller.log.AssertionUtil;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class p
  implements o
{
  private final String a;
  private final String b;
  private final File c;
  private final AccountManager d;
  private final BackupManager e;
  private final a f;
  
  public p(String paramString1, String paramString2, File paramFile, AccountManager paramAccountManager, BackupManager paramBackupManager, a parama)
  {
    a = paramString1;
    b = paramString2;
    c = paramFile;
    d = paramAccountManager;
    e = paramBackupManager;
    f = parama;
  }
  
  private static j a(File paramFile)
  {
    boolean bool = paramFile.exists();
    localObject1 = null;
    if (!bool) {
      return null;
    }
    try
    {
      Object localObject2 = new java/io/FileInputStream;
      ((FileInputStream)localObject2).<init>(paramFile);
      localObject2 = (InputStream)localObject2;
      paramFile = new java/io/DataInputStream;
      paramFile.<init>((InputStream)localObject2);
      paramFile = (Closeable)paramFile;
      localObject2 = paramFile;
      try
      {
        localObject2 = (DataInputStream)paramFile;
        int i = ((DataInputStream)localObject2).readInt();
        int j = 2;
        if (i != j)
        {
          b.a(paramFile, null);
          return null;
        }
        localObject5 = new com/truecaller/common/account/j;
        String str1 = ((DataInputStream)localObject2).readUTF();
        String str2 = "it.readUTF()";
        k.a(str1, str2);
        str2 = ((DataInputStream)localObject2).readUTF();
        String str3 = "it.readUTF()";
        k.a(str2, str3);
        localObject2 = ((DataInputStream)localObject2).readUTF();
        str3 = "it.readUTF()";
        k.a(localObject2, str3);
        ((j)localObject5).<init>(str1, (String)localObject2, str2);
        b.a(paramFile, null);
        localObject1 = localObject5;
      }
      finally
      {
        try
        {
          throw ((Throwable)localObject3);
        }
        finally
        {
          Object localObject5 = localObject3;
          Object localObject4 = localObject6;
          b.a(paramFile, (Throwable)localObject5);
        }
      }
      return (j)localObject1;
    }
    catch (IOException localIOException)
    {
      paramFile = (Throwable)localIOException;
      AssertionUtil.reportThrowableButNeverCrash(paramFile);
    }
  }
  
  private final Account b()
  {
    Object localObject = d;
    String str = b;
    localObject = ((AccountManager)localObject).getAccountsByType(str);
    k.a(localObject, "accountManager.getAccountsByType(accountType)");
    return (Account)f.c((Object[])localObject);
  }
  
  public final j a()
  {
    Object localObject1 = b();
    boolean bool = false;
    a locala = null;
    String str1;
    String str2;
    if (localObject1 != null)
    {
      str1 = d.peekAuthToken((Account)localObject1, "installation_id_backup");
      str2 = d.getUserData((Account)localObject1, "normalized_number_backup");
      Object localObject2 = d;
      String str3 = "country_code_backup";
      localObject1 = ((AccountManager)localObject2).getUserData((Account)localObject1, str3);
      if ((str1 != null) && (str2 != null) && (localObject1 != null))
      {
        localObject2 = new com/truecaller/common/account/j;
        ((j)localObject2).<init>(str1, str2, (String)localObject1);
      }
      else
      {
        localObject2 = null;
      }
      if (localObject2 != null)
      {
        f.a("accountRestorationSource", "restored_from_account_manager");
        return (j)localObject2;
      }
    }
    localObject1 = a(c);
    if (localObject1 != null)
    {
      locala = f;
      str1 = "accountFileWasRestoredByAutobackup";
      bool = locala.b(str1);
      if (bool)
      {
        locala = f;
        str2 = "restored_from_autobackup";
        locala.a("accountRestorationSource", str2);
        locala = f;
        str1 = "accountFileWasRestoredByAutobackup";
        locala.d(str1);
      }
      else
      {
        locala = f;
        str1 = "accountRestorationSource";
        str2 = "restored_from_file";
        locala.a(str1, str2);
      }
      return (j)localObject1;
    }
    return null;
  }
  
  /* Error */
  public final void a(j paramj)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc -96
    //   3: invokestatic 24	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_0
    //   7: invokespecial 121	com/truecaller/common/account/p:b	()Landroid/accounts/Account;
    //   10: astore_2
    //   11: aconst_null
    //   12: astore_3
    //   13: aload_2
    //   14: ifnonnull +56 -> 70
    //   17: aload_0
    //   18: getfield 46	com/truecaller/common/account/p:d	Landroid/accounts/AccountManager;
    //   21: astore 4
    //   23: new 118	android/accounts/Account
    //   26: astore 5
    //   28: aload_0
    //   29: getfield 40	com/truecaller/common/account/p:a	Ljava/lang/String;
    //   32: astore 6
    //   34: aload_0
    //   35: getfield 42	com/truecaller/common/account/p:b	Ljava/lang/String;
    //   38: astore 7
    //   40: aload 5
    //   42: aload 6
    //   44: aload 7
    //   46: invokespecial 162	android/accounts/Account:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   49: aload 4
    //   51: aload 5
    //   53: aconst_null
    //   54: aconst_null
    //   55: invokevirtual 166	android/accounts/AccountManager:addAccountExplicitly	(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z
    //   58: istore 8
    //   60: iload 8
    //   62: ifeq +8 -> 70
    //   65: aload_0
    //   66: invokespecial 121	com/truecaller/common/account/p:b	()Landroid/accounts/Account;
    //   69: astore_2
    //   70: aload_2
    //   71: ifnull +73 -> 144
    //   74: aload_0
    //   75: getfield 46	com/truecaller/common/account/p:d	Landroid/accounts/AccountManager;
    //   78: astore 4
    //   80: aload_1
    //   81: getfield 167	com/truecaller/common/account/j:a	Ljava/lang/String;
    //   84: astore 6
    //   86: aload 4
    //   88: aload_2
    //   89: ldc 123
    //   91: aload 6
    //   93: invokevirtual 171	android/accounts/AccountManager:setAuthToken	(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    //   96: aload_0
    //   97: getfield 46	com/truecaller/common/account/p:d	Landroid/accounts/AccountManager;
    //   100: astore 4
    //   102: aload_1
    //   103: getfield 172	com/truecaller/common/account/j:b	Ljava/lang/String;
    //   106: astore 6
    //   108: aload 4
    //   110: aload_2
    //   111: ldc -127
    //   113: aload 6
    //   115: invokevirtual 175	android/accounts/AccountManager:setUserData	(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    //   118: aload_0
    //   119: getfield 46	com/truecaller/common/account/p:d	Landroid/accounts/AccountManager;
    //   122: astore 4
    //   124: ldc -122
    //   126: astore 5
    //   128: aload_1
    //   129: getfield 177	com/truecaller/common/account/j:c	Ljava/lang/String;
    //   132: astore 6
    //   134: aload 4
    //   136: aload_2
    //   137: aload 5
    //   139: aload 6
    //   141: invokevirtual 175	android/accounts/AccountManager:setUserData	(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    //   144: aload_0
    //   145: getfield 44	com/truecaller/common/account/p:c	Ljava/io/File;
    //   148: astore_2
    //   149: new 179	java/io/FileOutputStream
    //   152: astore 4
    //   154: aload 4
    //   156: aload_2
    //   157: invokespecial 180	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   160: aload 4
    //   162: checkcast 182	java/io/OutputStream
    //   165: astore 4
    //   167: new 184	java/io/DataOutputStream
    //   170: astore_2
    //   171: aload_2
    //   172: aload 4
    //   174: invokespecial 187	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   177: aload_2
    //   178: checkcast 70	java/io/Closeable
    //   181: astore_2
    //   182: aload_2
    //   183: astore 4
    //   185: aload_2
    //   186: checkcast 184	java/io/DataOutputStream
    //   189: astore 4
    //   191: iconst_2
    //   192: istore 9
    //   194: aload 4
    //   196: iload 9
    //   198: invokevirtual 191	java/io/DataOutputStream:writeInt	(I)V
    //   201: aload_1
    //   202: getfield 167	com/truecaller/common/account/j:a	Ljava/lang/String;
    //   205: astore 5
    //   207: aload 4
    //   209: aload 5
    //   211: invokevirtual 194	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   214: aload_1
    //   215: getfield 177	com/truecaller/common/account/j:c	Ljava/lang/String;
    //   218: astore 5
    //   220: aload 4
    //   222: aload 5
    //   224: invokevirtual 194	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   227: aload_1
    //   228: getfield 172	com/truecaller/common/account/j:b	Ljava/lang/String;
    //   231: astore_1
    //   232: aload 4
    //   234: aload_1
    //   235: invokevirtual 194	java/io/DataOutputStream:writeUTF	(Ljava/lang/String;)V
    //   238: getstatic 199	c/x:a	Lc/x;
    //   241: astore_1
    //   242: aload_2
    //   243: aconst_null
    //   244: invokestatic 80	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   247: goto +27 -> 274
    //   250: astore_1
    //   251: goto +8 -> 259
    //   254: astore_1
    //   255: aload_1
    //   256: astore_3
    //   257: aload_1
    //   258: athrow
    //   259: aload_2
    //   260: aload_3
    //   261: invokestatic 80	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   264: aload_1
    //   265: athrow
    //   266: checkcast 95	java/lang/Throwable
    //   269: astore_1
    //   270: aload_1
    //   271: invokestatic 101	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   274: aload_0
    //   275: getfield 48	com/truecaller/common/account/p:e	Landroid/app/backup/BackupManager;
    //   278: invokevirtual 204	android/app/backup/BackupManager:dataChanged	()V
    //   281: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	282	0	this	p
    //   0	282	1	paramj	j
    //   10	250	2	localObject1	Object
    //   12	249	3	localj	j
    //   21	212	4	localObject2	Object
    //   26	197	5	localObject3	Object
    //   32	108	6	str1	String
    //   38	7	7	str2	String
    //   58	3	8	bool	boolean
    //   192	5	9	i	int
    //   266	1	10	localIOException	IOException
    // Exception table:
    //   from	to	target	type
    //   257	259	250	finally
    //   185	189	254	finally
    //   196	201	254	finally
    //   201	205	254	finally
    //   209	214	254	finally
    //   214	218	254	finally
    //   222	227	254	finally
    //   227	231	254	finally
    //   234	238	254	finally
    //   238	241	254	finally
    //   149	152	266	java/io/IOException
    //   156	160	266	java/io/IOException
    //   160	165	266	java/io/IOException
    //   167	170	266	java/io/IOException
    //   172	177	266	java/io/IOException
    //   177	181	266	java/io/IOException
    //   243	247	266	java/io/IOException
    //   260	264	266	java/io/IOException
    //   264	266	266	java/io/IOException
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "installationId");
    AccountManager localAccountManager = d;
    String str = b;
    localAccountManager.invalidateAuthToken(str, paramString);
    c.delete();
    e.dataChanged();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */