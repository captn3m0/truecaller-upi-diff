package com.truecaller.common.account;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class TruecallerAccountAuthenticatorService
  extends Service
{
  private n a;
  
  public IBinder onBind(Intent paramIntent)
  {
    if (paramIntent == null) {
      return null;
    }
    String str = "android.accounts.AccountAuthenticator";
    paramIntent = paramIntent.getAction();
    boolean bool = str.equals(paramIntent);
    if (bool) {
      return a.getIBinder();
    }
    return null;
  }
  
  public void onCreate()
  {
    super.onCreate();
    n localn = new com/truecaller/common/account/n;
    localn.<init>(this);
    a = localn;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.TruecallerAccountAuthenticatorService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */