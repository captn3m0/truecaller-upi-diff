package com.truecaller.common.account;

import java.util.concurrent.TimeUnit;

public final class u
{
  private static final long a = TimeUnit.SECONDS.toMillis(2);
  private static final long b;
  private static final long c;
  
  static
  {
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    long l = 10;
    b = localTimeUnit.toMillis(l);
    c = TimeUnit.MINUTES.toMillis(l);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */