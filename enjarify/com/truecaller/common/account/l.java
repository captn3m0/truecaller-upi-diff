package com.truecaller.common.account;

import com.truecaller.common.account.b.a;

public final class l
  implements k
{
  private final a a;
  private final g b;
  
  public l(a parama, g paramg)
  {
    a = parama;
    b = paramg;
  }
  
  public final String a()
  {
    try
    {
      Object localObject1 = a;
      String str = "auth_token_cross_domain";
      localObject1 = ((a)localObject1).a(str);
      if (localObject1 == null) {
        localObject1 = b();
      }
      return (String)localObject1;
    }
    finally {}
  }
  
  /* Error */
  public final String b()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aconst_null
    //   3: astore_1
    //   4: aload_0
    //   5: getfield 27	com/truecaller/common/account/l:b	Lcom/truecaller/common/account/g;
    //   8: astore_2
    //   9: aload_2
    //   10: invokeinterface 42 1 0
    //   15: astore_2
    //   16: aload_2
    //   17: invokevirtual 48	e/r:d	()Z
    //   20: istore_3
    //   21: iload_3
    //   22: ifne +121 -> 143
    //   25: aload_2
    //   26: invokevirtual 52	e/r:f	()Lokhttp3/ae;
    //   29: astore 4
    //   31: aload 4
    //   33: ifnull +296 -> 329
    //   36: aload 4
    //   38: invokevirtual 57	okhttp3/ae:f	()Ljava/io/Reader;
    //   41: astore 4
    //   43: aload 4
    //   45: ifnull +284 -> 329
    //   48: new 59	com/google/gson/f
    //   51: astore 5
    //   53: aload 5
    //   55: invokespecial 60	com/google/gson/f:<init>	()V
    //   58: ldc 62
    //   60: astore 6
    //   62: aload 5
    //   64: aload 4
    //   66: aload 6
    //   68: invokevirtual 65	com/google/gson/f:a	(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;
    //   71: astore 4
    //   73: aload 4
    //   75: checkcast 62	com/truecaller/common/network/account/TokenErrorResponseDto
    //   78: astore 4
    //   80: aload 4
    //   82: ifnull +247 -> 329
    //   85: new 67	com/truecaller/log/UnmutedException$a
    //   88: astore 5
    //   90: aload_2
    //   91: invokevirtual 70	e/r:b	()I
    //   94: istore 7
    //   96: aload 4
    //   98: invokevirtual 73	com/truecaller/common/network/account/TokenErrorResponseDto:getStatus	()I
    //   101: istore 8
    //   103: iload 8
    //   105: invokestatic 79	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   108: astore 6
    //   110: aload 4
    //   112: invokevirtual 82	com/truecaller/common/network/account/TokenErrorResponseDto:getMessage	()Ljava/lang/String;
    //   115: astore 4
    //   117: aload 5
    //   119: iload 7
    //   121: aload 6
    //   123: aload 4
    //   125: invokespecial 85	com/truecaller/log/UnmutedException$a:<init>	(ILjava/lang/Integer;Ljava/lang/String;)V
    //   128: aload 5
    //   130: checkcast 87	java/lang/Throwable
    //   133: astore 5
    //   135: aload 5
    //   137: invokestatic 92	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   140: goto +189 -> 329
    //   143: aload_2
    //   144: invokevirtual 96	e/r:e	()Ljava/lang/Object;
    //   147: astore_2
    //   148: aload_2
    //   149: checkcast 98	com/truecaller/common/network/account/TemporaryTokenDto
    //   152: astore_2
    //   153: aload_2
    //   154: ifnull +175 -> 329
    //   157: aload_2
    //   158: invokevirtual 101	com/truecaller/common/network/account/TemporaryTokenDto:getToken	()Ljava/lang/String;
    //   161: astore_2
    //   162: aload_2
    //   163: ifnull +166 -> 329
    //   166: aload_2
    //   167: astore 4
    //   169: aload_2
    //   170: checkcast 103	java/lang/CharSequence
    //   173: astore 4
    //   175: aload 4
    //   177: invokestatic 108	c/n/m:a	(Ljava/lang/CharSequence;)Z
    //   180: iconst_1
    //   181: ixor
    //   182: istore_3
    //   183: iload_3
    //   184: ifeq +6 -> 190
    //   187: goto +8 -> 195
    //   190: iconst_0
    //   191: istore 7
    //   193: aconst_null
    //   194: astore_2
    //   195: aload_2
    //   196: ifnull +133 -> 329
    //   199: aload_0
    //   200: getfield 25	com/truecaller/common/account/l:a	Lcom/truecaller/common/account/b/a;
    //   203: astore 4
    //   205: ldc 29
    //   207: astore 5
    //   209: aload 4
    //   211: aload 5
    //   213: aload_2
    //   214: invokeinterface 111 3 0
    //   219: aload_2
    //   220: astore_1
    //   221: goto +108 -> 329
    //   224: astore_1
    //   225: goto +108 -> 333
    //   228: astore_2
    //   229: aload_2
    //   230: instanceof 113
    //   233: istore_3
    //   234: iload_3
    //   235: ifne +34 -> 269
    //   238: aload_2
    //   239: instanceof 115
    //   242: istore_3
    //   243: iload_3
    //   244: ifeq +6 -> 250
    //   247: goto +22 -> 269
    //   250: aload_2
    //   251: instanceof 117
    //   254: istore_3
    //   255: iload_3
    //   256: ifeq +6 -> 262
    //   259: goto +70 -> 329
    //   262: aload_2
    //   263: checkcast 87	java/lang/Throwable
    //   266: astore_2
    //   267: aload_2
    //   268: athrow
    //   269: new 67	com/truecaller/log/UnmutedException$a
    //   272: astore 4
    //   274: new 119	java/lang/StringBuilder
    //   277: astore 5
    //   279: ldc 121
    //   281: astore 6
    //   283: aload 5
    //   285: aload 6
    //   287: invokespecial 124	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   290: aload_2
    //   291: checkcast 126	java/lang/Exception
    //   294: invokevirtual 127	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   297: astore_2
    //   298: aload 5
    //   300: aload_2
    //   301: invokevirtual 131	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   304: pop
    //   305: aload 5
    //   307: invokevirtual 134	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   310: astore_2
    //   311: aload 4
    //   313: aload_2
    //   314: invokespecial 135	com/truecaller/log/UnmutedException$a:<init>	(Ljava/lang/String;)V
    //   317: aload 4
    //   319: checkcast 87	java/lang/Throwable
    //   322: astore 4
    //   324: aload 4
    //   326: invokestatic 92	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   329: aload_0
    //   330: monitorexit
    //   331: aload_1
    //   332: areturn
    //   333: aload_0
    //   334: monitorexit
    //   335: aload_1
    //   336: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	337	0	this	l
    //   3	218	1	localObject1	Object
    //   224	112	1	str	String
    //   8	212	2	localObject2	Object
    //   228	35	2	localException	Exception
    //   266	48	2	localObject3	Object
    //   20	236	3	bool	boolean
    //   29	296	4	localObject4	Object
    //   51	255	5	localObject5	Object
    //   60	226	6	localObject6	Object
    //   94	98	7	i	int
    //   101	3	8	j	int
    // Exception table:
    //   from	to	target	type
    //   4	8	224	finally
    //   9	15	224	finally
    //   16	20	224	finally
    //   25	29	224	finally
    //   36	41	224	finally
    //   48	51	224	finally
    //   53	58	224	finally
    //   66	71	224	finally
    //   73	78	224	finally
    //   85	88	224	finally
    //   90	94	224	finally
    //   96	101	224	finally
    //   103	108	224	finally
    //   110	115	224	finally
    //   123	128	224	finally
    //   128	133	224	finally
    //   135	140	224	finally
    //   143	147	224	finally
    //   148	152	224	finally
    //   157	161	224	finally
    //   169	173	224	finally
    //   175	180	224	finally
    //   199	203	224	finally
    //   213	219	224	finally
    //   262	266	224	finally
    //   267	269	224	finally
    //   269	272	224	finally
    //   274	277	224	finally
    //   285	290	224	finally
    //   290	297	224	finally
    //   300	305	224	finally
    //   305	310	224	finally
    //   313	317	224	finally
    //   317	322	224	finally
    //   324	329	224	finally
    //   4	8	228	java/lang/Exception
    //   9	15	228	java/lang/Exception
    //   16	20	228	java/lang/Exception
    //   25	29	228	java/lang/Exception
    //   36	41	228	java/lang/Exception
    //   48	51	228	java/lang/Exception
    //   53	58	228	java/lang/Exception
    //   66	71	228	java/lang/Exception
    //   73	78	228	java/lang/Exception
    //   85	88	228	java/lang/Exception
    //   90	94	228	java/lang/Exception
    //   96	101	228	java/lang/Exception
    //   103	108	228	java/lang/Exception
    //   110	115	228	java/lang/Exception
    //   123	128	228	java/lang/Exception
    //   128	133	228	java/lang/Exception
    //   135	140	228	java/lang/Exception
    //   143	147	228	java/lang/Exception
    //   148	152	228	java/lang/Exception
    //   157	161	228	java/lang/Exception
    //   169	173	228	java/lang/Exception
    //   175	180	228	java/lang/Exception
    //   199	203	228	java/lang/Exception
    //   213	219	228	java/lang/Exception
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */