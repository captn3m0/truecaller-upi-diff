package com.truecaller.common.account;

import c.g.b.k;
import c.x;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e.a;
import com.truecaller.common.network.account.ExchangeCredentialsResponseDto;
import com.truecaller.common.network.c;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public final class s
  implements r
{
  private long a;
  private int b;
  private final Object c;
  private final Object d;
  private final com.truecaller.common.g.a e;
  private final com.truecaller.utils.a f;
  private final c g;
  private final o h;
  private final b i;
  private final com.truecaller.common.account.a.a j;
  private final g k;
  
  public s(com.truecaller.common.g.a parama, com.truecaller.utils.a parama1, c paramc, o paramo, b paramb, com.truecaller.common.account.a.a parama2, g paramg)
  {
    e = parama;
    f = parama1;
    g = paramc;
    h = paramo;
    i = paramb;
    j = parama2;
    k = paramg;
    parama = new java/lang/Object;
    parama.<init>();
    c = parama;
    parama = new java/lang/Object;
    parama.<init>();
    d = parama;
  }
  
  private final String a(String paramString)
  {
    try
    {
      localObject1 = k;
      localObject1 = ((g)localObject1).a(paramString);
      Object localObject2 = ((e.r)localObject1).e();
      localObject2 = (ExchangeCredentialsResponseDto)localObject2;
      boolean bool1 = ((e.r)localObject1).d();
      Object localObject3;
      if ((bool1) && (localObject2 != null))
      {
        localObject1 = ((ExchangeCredentialsResponseDto)localObject2).getDomain();
        if (localObject1 != null)
        {
          localObject3 = g;
          ((c)localObject3).a((String)localObject1);
        }
        l1 = 0L;
        a = l1;
        boolean bool2 = false;
        localObject1 = null;
        b = 0;
        localObject1 = TimeUnit.SECONDS;
        l1 = ((ExchangeCredentialsResponseDto)localObject2).getTtl();
        l1 = ((TimeUnit)localObject1).toMillis(l1);
        localObject1 = ((ExchangeCredentialsResponseDto)localObject2).getState();
        String str = "exchanged";
        bool2 = k.a(localObject1, str);
        if (bool2)
        {
          localObject1 = ((ExchangeCredentialsResponseDto)localObject2).getInstallationId();
          if (localObject1 != null)
          {
            localObject1 = ((ExchangeCredentialsResponseDto)localObject2).getInstallationId();
            a((String)localObject1, l1);
            return ((ExchangeCredentialsResponseDto)localObject2).getInstallationId();
          }
        }
        a(paramString, l1);
      }
      else
      {
        int n = ((e.r)localObject1).b();
        int i1 = 401;
        if (n == i1)
        {
          localObject1 = "ExchangeCredentials";
          a(paramString, (String)localObject1);
          return null;
        }
        l2 = u.a();
        int m = b;
        l2 <<= m;
        l1 = u.b();
        l2 = Math.min(l2, l1);
        localObject3 = f;
        l1 = ((com.truecaller.utils.a)localObject3).b() + l2;
        a = l1;
        n = b + 1;
        b = n;
      }
    }
    catch (IOException localIOException)
    {
      Object localObject1 = f;
      long l2 = ((com.truecaller.utils.a)localObject1).b();
      long l1 = u.c();
      l2 += l1;
      a = l2;
    }
    return paramString;
  }
  
  private final void a(j paramj)
  {
    String str1 = a;
    String str2 = c;
    String str3 = b;
    a(str1, 0L, str2, str3);
  }
  
  private final j f()
  {
    synchronized (c)
    {
      Object localObject2 = e;
      Object localObject4 = "installationId";
      localObject2 = ((com.truecaller.common.g.a)localObject2).a((String)localObject4);
      localObject4 = e;
      Object localObject5 = "profileNumber";
      localObject4 = ((com.truecaller.common.g.a)localObject4).a((String)localObject5);
      localObject5 = e;
      Object localObject6 = "profileCountryIso";
      localObject5 = ((com.truecaller.common.g.a)localObject5).a((String)localObject6);
      if ((localObject2 != null) && (localObject5 != null) && (localObject4 != null))
      {
        localObject6 = new com/truecaller/common/account/j;
        ((j)localObject6).<init>((String)localObject2, (String)localObject4, (String)localObject5);
        return (j)localObject6;
      }
      localObject2 = j;
      localObject2 = ((com.truecaller.common.account.a.a)localObject2).a();
      localObject4 = null;
      if (localObject2 != null)
      {
        a((j)localObject2);
        localObject5 = j;
        ((com.truecaller.common.account.a.a)localObject5).b();
      }
      else
      {
        localObject2 = null;
      }
      if (localObject2 == null)
      {
        localObject2 = h;
        localObject2 = ((o)localObject2).a();
        if (localObject2 != null) {
          a((j)localObject2);
        } else {
          localObject2 = null;
        }
      }
      return (j)localObject2;
    }
  }
  
  public final String a()
  {
    j localj = f();
    if (localj != null) {
      return c;
    }
    return null;
  }
  
  public final void a(String paramString, long paramLong)
  {
    k.b(paramString, "newInstallationId");
    synchronized (c)
    {
      Object localObject2 = e;
      Object localObject3 = "installationId";
      ((com.truecaller.common.g.a)localObject2).a((String)localObject3, paramString);
      localObject2 = e;
      localObject3 = "installationIdFetchTime";
      com.truecaller.utils.a locala = f;
      long l = locala.a();
      ((com.truecaller.common.g.a)localObject2).b((String)localObject3, l);
      localObject2 = e;
      localObject3 = "installationIdTtl";
      ((com.truecaller.common.g.a)localObject2).b((String)localObject3, paramLong);
      Object localObject4 = e;
      Object localObject5 = "profileNumber";
      localObject4 = ((com.truecaller.common.g.a)localObject4).a((String)localObject5);
      if (localObject4 == null) {
        return;
      }
      localObject5 = e;
      localObject2 = "profileCountryIso";
      localObject5 = ((com.truecaller.common.g.a)localObject5).a((String)localObject2);
      if (localObject5 == null) {
        return;
      }
      localObject2 = h;
      localObject3 = new com/truecaller/common/account/j;
      ((j)localObject3).<init>(paramString, (String)localObject4, (String)localObject5);
      ((o)localObject2).a((j)localObject3);
      paramString = x.a;
      return;
    }
  }
  
  public final void a(String paramString1, long paramLong, String paramString2, String paramString3)
  {
    k.b(paramString1, "installationId");
    k.b(paramString2, "countryIso");
    k.b(paramString3, "normalizedNumber");
    synchronized (c)
    {
      Object localObject2 = e;
      String str = "installationId";
      ((com.truecaller.common.g.a)localObject2).a(str, paramString1);
      localObject2 = e;
      str = "installationIdTtl";
      ((com.truecaller.common.g.a)localObject2).b(str, paramLong);
      Object localObject3 = e;
      Object localObject4 = "installationIdFetchTime";
      localObject2 = f;
      long l = ((com.truecaller.utils.a)localObject2).a();
      ((com.truecaller.common.g.a)localObject3).b((String)localObject4, l);
      localObject3 = e;
      localObject4 = "profileCountryIso";
      ((com.truecaller.common.g.a)localObject3).a((String)localObject4, paramString2);
      localObject3 = e;
      localObject4 = "profileNumber";
      ((com.truecaller.common.g.a)localObject3).a((String)localObject4, paramString3);
      localObject3 = h;
      localObject4 = new com/truecaller/common/account/j;
      ((j)localObject4).<init>(paramString1, paramString3, paramString2);
      ((o)localObject3).a((j)localObject4);
      paramString1 = x.a;
      return;
    }
  }
  
  public final boolean a(String paramString1, String paramString2)
  {
    k.b(paramString1, "installationId");
    k.b(paramString2, "context");
    synchronized (c)
    {
      Object localObject2 = e;
      String str1 = "installationId";
      localObject2 = ((com.truecaller.common.g.a)localObject2).a(str1);
      boolean bool1 = k.a(localObject2, paramString1);
      boolean bool2 = true;
      bool1 ^= bool2;
      if (bool1) {
        return false;
      }
      localObject2 = e;
      String str2 = "installationId";
      ((com.truecaller.common.g.a)localObject2).d(str2);
      localObject2 = e;
      str2 = "installationIdFetchTime";
      ((com.truecaller.common.g.a)localObject2).d(str2);
      localObject2 = e;
      str2 = "installationIdTtl";
      ((com.truecaller.common.g.a)localObject2).d(str2);
      localObject2 = h;
      ((o)localObject2).a(paramString1);
      paramString1 = i;
      localObject2 = new com/truecaller/analytics/e$a;
      str2 = "Logout";
      ((e.a)localObject2).<init>(str2);
      str2 = "Context";
      paramString2 = ((e.a)localObject2).a(str2, paramString2);
      paramString2 = paramString2.a();
      localObject2 = "AnalyticsEvent.Builder(L…CONTEXT, context).build()";
      k.a(paramString2, (String)localObject2);
      paramString1.b(paramString2);
      return bool2;
    }
  }
  
  public final String b()
  {
    j localj = f();
    if (localj != null) {
      return b;
    }
    return null;
  }
  
  public final boolean c()
  {
    j localj = f();
    return localj != null;
  }
  
  public final String d()
  {
    j localj = f();
    if (localj != null) {
      return a;
    }
    return null;
  }
  
  public final String e()
  {
    synchronized (d)
    {
      Object localObject2 = f();
      if (localObject2 != null)
      {
        localObject2 = a;
        if (localObject2 != null)
        {
          com.truecaller.common.g.a locala = e;
          String str1 = "installationIdFetchTime";
          long l1 = 0L;
          long l2 = locala.a(str1, l1);
          Object localObject4 = e;
          String str2 = "installationIdTtl";
          l1 = ((com.truecaller.common.g.a)localObject4).a(str2, l1);
          localObject4 = f;
          long l3 = ((com.truecaller.utils.a)localObject4).a();
          l1 += l2;
          boolean bool1 = l1 < l3;
          if (bool1)
          {
            boolean bool2 = l2 < l3;
            if (bool2) {}
          }
          else
          {
            l2 = a;
            com.truecaller.utils.a locala1 = f;
            l1 = locala1.b();
            boolean bool3 = l2 < l1;
            if (!bool3) {
              localObject2 = a((String)localObject2);
            }
          }
          return (String)localObject2;
        }
      }
      return null;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */