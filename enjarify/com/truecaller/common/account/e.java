package com.truecaller.common.account;

import dagger.a.d;
import javax.inject.Provider;

public final class e
  implements d
{
  private final Provider a;
  
  private e(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static e a(Provider paramProvider)
  {
    e locale = new com/truecaller/common/account/e;
    locale.<init>(paramProvider);
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */