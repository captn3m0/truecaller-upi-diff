package com.truecaller.common.account.a;

import android.accounts.Account;
import android.accounts.AccountManager;
import c.a.f;
import c.g.b.k;
import com.truecaller.common.account.j;

public final class b
  implements a
{
  private final AccountManager a;
  private final String b;
  
  public b(AccountManager paramAccountManager, String paramString)
  {
    a = paramAccountManager;
    b = paramString;
  }
  
  private final Account c()
  {
    Object localObject = a;
    String str = b;
    localObject = ((AccountManager)localObject).getAccountsByType(str);
    k.a(localObject, "accountManager.getAccountsByType(accountType)");
    return (Account)f.c((Object[])localObject);
  }
  
  public final j a()
  {
    Object localObject1 = c();
    if (localObject1 == null) {
      return null;
    }
    Object localObject2 = a.getUserData((Account)localObject1, "isMigratedToSettings");
    Object localObject3 = "true";
    boolean bool = k.a(localObject2, localObject3);
    if (bool) {
      return null;
    }
    localObject2 = new com/truecaller/common/account/j;
    localObject3 = a;
    Object localObject4 = "installation_id";
    localObject3 = ((AccountManager)localObject3).peekAuthToken((Account)localObject1, (String)localObject4);
    if (localObject3 == null) {
      return null;
    }
    localObject4 = a;
    Object localObject5 = "phone_number";
    localObject4 = ((AccountManager)localObject4).getUserData((Account)localObject1, (String)localObject5);
    if (localObject4 == null) {
      return null;
    }
    localObject5 = a;
    String str = "country_code";
    localObject1 = ((AccountManager)localObject5).getUserData((Account)localObject1, str);
    if (localObject1 == null) {
      return null;
    }
    ((j)localObject2).<init>((String)localObject3, (String)localObject4, (String)localObject1);
    return (j)localObject2;
  }
  
  public final void b()
  {
    Account localAccount = c();
    if (localAccount != null)
    {
      a.setUserData(localAccount, "isMigratedToSettings", "true");
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */