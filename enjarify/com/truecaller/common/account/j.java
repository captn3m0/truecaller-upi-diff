package com.truecaller.common.account;

import c.g.b.k;

public final class j
{
  final String a;
  final String b;
  final String c;
  
  public j(String paramString1, String paramString2, String paramString3)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof j;
      if (bool1)
      {
        paramObject = (j)paramObject;
        String str1 = a;
        String str2 = a;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = b;
          str2 = b;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = c;
            paramObject = c;
            boolean bool2 = k.a(str1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final int hashCode()
  {
    String str1 = a;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = b;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = c;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("AccountState(installationId=");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append(", normalizedNumber=");
    str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(", countryIso=");
    str = c;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */