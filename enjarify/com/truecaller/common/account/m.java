package com.truecaller.common.account;

import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private m(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static m a(Provider paramProvider1, Provider paramProvider2)
  {
    m localm = new com/truecaller/common/account/m;
    localm.<init>(paramProvider1, paramProvider2);
    return localm;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.account.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */