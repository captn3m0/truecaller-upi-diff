package com.truecaller.common.payments;

public final class R$styleable
{
  public static final int[] FontFamily;
  public static final int[] FontFamilyFont;
  public static final int FontFamilyFont_android_font = 0;
  public static final int FontFamilyFont_android_fontStyle = 2;
  public static final int FontFamilyFont_android_fontVariationSettings = 4;
  public static final int FontFamilyFont_android_fontWeight = 1;
  public static final int FontFamilyFont_android_ttcIndex = 3;
  public static final int FontFamilyFont_font = 5;
  public static final int FontFamilyFont_fontStyle = 6;
  public static final int FontFamilyFont_fontVariationSettings = 7;
  public static final int FontFamilyFont_fontWeight = 8;
  public static final int FontFamilyFont_ttcIndex = 9;
  public static final int FontFamily_fontProviderAuthority = 0;
  public static final int FontFamily_fontProviderCerts = 1;
  public static final int FontFamily_fontProviderFetchStrategy = 2;
  public static final int FontFamily_fontProviderFetchTimeout = 3;
  public static final int FontFamily_fontProviderPackage = 4;
  public static final int FontFamily_fontProviderQuery = 5;
  
  static
  {
    int[] arrayOfInt = new int[6];
    arrayOfInt[0] = 2130969000;
    arrayOfInt[1] = 2130969001;
    arrayOfInt[2] = 2130969002;
    arrayOfInt[3] = 2130969003;
    arrayOfInt[4] = 2130969004;
    arrayOfInt[5] = 2130969005;
    FontFamily = arrayOfInt;
    arrayOfInt = new int[10];
    arrayOfInt[0] = 16844082;
    arrayOfInt[1] = 16844083;
    arrayOfInt[2] = 16844095;
    arrayOfInt[3] = 16844143;
    arrayOfInt[4] = 16844144;
    arrayOfInt[5] = 2130968998;
    arrayOfInt[6] = 2130969006;
    arrayOfInt[7] = 2130969007;
    arrayOfInt[8] = 2130969008;
    arrayOfInt[9] = 2130969645;
    FontFamilyFont = arrayOfInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.payments.R.styleable
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */