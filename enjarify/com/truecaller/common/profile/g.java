package com.truecaller.common.profile;

import c.a.m;
import c.g.b.k;
import c.l;
import com.truecaller.common.R.string;

public final class g
{
  public static final int a(f paramf)
  {
    k.b(paramf, "receiver$0");
    Object localObject = f.e.c;
    boolean bool = k.a(paramf, localObject);
    if (!bool)
    {
      localObject = f.b.c;
      bool = k.a(paramf, localObject);
      if (bool) {
        return R.string.profile_error_generic;
      }
      localObject = f.a.c;
      bool = k.a(paramf, localObject);
      if (bool) {
        return R.string.profile_error_generic;
      }
      localObject = f.c.c;
      bool = k.a(paramf, localObject);
      if (bool) {
        return R.string.profile_error_network;
      }
      localObject = f.d.c;
      bool = k.a(paramf, localObject);
      if (bool) {
        return R.string.profile_error_network;
      }
      bool = paramf instanceof f.f;
      if (bool) {
        return R.string.profile_error_network;
      }
      bool = paramf instanceof f.g;
      if (bool)
      {
        paramf = (ProfileSaveError)m.e(c);
        int j;
        if (paramf != null)
        {
          j = paramf.getErrorType();
          paramf = Integer.valueOf(j);
        }
        else
        {
          j = 0;
          paramf = null;
        }
        int i;
        if (paramf != null)
        {
          i = paramf.intValue();
          int k = 1;
          if (i == k) {
            return R.string.profile_error_validation_invalidCharacter;
          }
        }
        if (paramf != null)
        {
          j = paramf.intValue();
          i = 2;
          if (j == i) {
            return R.string.profile_error_validation_invalidWord;
          }
        }
        return R.string.profile_error_generic;
      }
      paramf = new c/l;
      paramf.<init>();
      throw paramf;
    }
    paramf = new java/lang/IllegalStateException;
    localObject = "Not an error".toString();
    paramf.<init>((String)localObject);
    throw ((Throwable)paramf);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.profile.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */