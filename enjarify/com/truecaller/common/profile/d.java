package com.truecaller.common.profile;

import javax.inject.Provider;

public final class d
  implements dagger.a.d
{
  private final Provider a;
  
  private d(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static d a(Provider paramProvider)
  {
    d locald = new com/truecaller/common/profile/d;
    locald.<init>(paramProvider);
    return locald;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.profile.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */