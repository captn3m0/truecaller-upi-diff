package com.truecaller.common.profile;

import c.g.b.k;

public final class ProfileSaveError
{
  private final String details;
  private final int errorType;
  private final String fieldName;
  
  public ProfileSaveError(String paramString1, int paramInt, String paramString2)
  {
    fieldName = paramString1;
    errorType = paramInt;
    details = paramString2;
  }
  
  public final String component1()
  {
    return fieldName;
  }
  
  public final int component2()
  {
    return errorType;
  }
  
  public final String component3()
  {
    return details;
  }
  
  public final ProfileSaveError copy(String paramString1, int paramInt, String paramString2)
  {
    k.b(paramString1, "fieldName");
    k.b(paramString2, "details");
    ProfileSaveError localProfileSaveError = new com/truecaller/common/profile/ProfileSaveError;
    localProfileSaveError.<init>(paramString1, paramInt, paramString2);
    return localProfileSaveError;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof ProfileSaveError;
      if (bool2)
      {
        paramObject = (ProfileSaveError)paramObject;
        String str1 = fieldName;
        String str2 = fieldName;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          int i = errorType;
          int j = errorType;
          if (i == j)
          {
            i = 1;
          }
          else
          {
            i = 0;
            str1 = null;
          }
          if (i != 0)
          {
            str1 = details;
            paramObject = details;
            boolean bool3 = k.a(str1, paramObject);
            if (bool3) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final String getDetails()
  {
    return details;
  }
  
  public final int getErrorType()
  {
    return errorType;
  }
  
  public final String getFieldName()
  {
    return fieldName;
  }
  
  public final int hashCode()
  {
    String str1 = fieldName;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    int k = errorType;
    int j = (j + k) * 31;
    String str2 = details;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ProfileSaveError(fieldName=");
    String str = fieldName;
    localStringBuilder.append(str);
    localStringBuilder.append(", errorType=");
    int i = errorType;
    localStringBuilder.append(i);
    localStringBuilder.append(", details=");
    str = details;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.profile.ProfileSaveError
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */