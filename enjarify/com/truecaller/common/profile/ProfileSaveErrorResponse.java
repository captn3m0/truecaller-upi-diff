package com.truecaller.common.profile;

import c.g.b.k;
import com.google.gson.c.a;
import com.google.gson.f;
import java.lang.reflect.Type;
import java.util.List;

public final class ProfileSaveErrorResponse
{
  public static final ProfileSaveErrorResponse.a Companion;
  private static final f gson;
  private static final Type type;
  private final List errors;
  
  static
  {
    Object localObject = new com/truecaller/common/profile/ProfileSaveErrorResponse$a;
    ((ProfileSaveErrorResponse.a)localObject).<init>((byte)0);
    Companion = (ProfileSaveErrorResponse.a)localObject;
    localObject = new com/google/gson/f;
    ((f)localObject).<init>();
    gson = (f)localObject;
    localObject = new com/truecaller/common/profile/ProfileSaveErrorResponse$b;
    ((ProfileSaveErrorResponse.b)localObject).<init>();
    type = b;
  }
  
  public ProfileSaveErrorResponse(List paramList)
  {
    errors = paramList;
  }
  
  public final List component1()
  {
    return errors;
  }
  
  public final ProfileSaveErrorResponse copy(List paramList)
  {
    ProfileSaveErrorResponse localProfileSaveErrorResponse = new com/truecaller/common/profile/ProfileSaveErrorResponse;
    localProfileSaveErrorResponse.<init>(paramList);
    return localProfileSaveErrorResponse;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof ProfileSaveErrorResponse;
      if (bool1)
      {
        paramObject = (ProfileSaveErrorResponse)paramObject;
        List localList = errors;
        paramObject = errors;
        boolean bool2 = k.a(localList, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final List getErrors()
  {
    return errors;
  }
  
  public final int hashCode()
  {
    List localList = errors;
    if (localList != null) {
      return localList.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ProfileSaveErrorResponse(errors=");
    List localList = errors;
    localStringBuilder.append(localList);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.profile.ProfileSaveErrorResponse
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */