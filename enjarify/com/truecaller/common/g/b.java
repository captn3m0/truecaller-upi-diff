package com.truecaller.common.g;

import android.content.Context;
import android.content.SharedPreferences;
import c.g.b.k;

public final class b
  extends com.truecaller.utils.a.a
  implements a
{
  private final int b = 2;
  private final String c = "core";
  
  public b(SharedPreferences paramSharedPreferences)
  {
    super(paramSharedPreferences);
  }
  
  public final int a()
  {
    return b;
  }
  
  public final void a(int paramInt, Context paramContext)
  {
    String str1 = "context";
    k.b(paramContext, str1);
    if (paramInt <= 0)
    {
      d("profileTrueName");
      paramContext = "profileAmbassador";
      d(paramContext);
    }
    int i = 2;
    if (paramInt < i)
    {
      d("key_pending_first_name");
      String str2 = "key_pending_last_name";
      d(str2);
    }
  }
  
  public final String b()
  {
    return c;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.g.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */