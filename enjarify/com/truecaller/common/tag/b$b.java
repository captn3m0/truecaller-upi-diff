package com.truecaller.common.tag;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class b$b
{
  final Map a;
  List b;
  
  b$b()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    a = localHashMap;
    b = null;
  }
  
  final b a(CharSequence paramCharSequence)
  {
    char c = Character.toLowerCase(paramCharSequence.charAt(0));
    Map localMap = a;
    Object localObject = Character.valueOf(c);
    localObject = (List)localMap.get(localObject);
    localMap = null;
    if (localObject == null) {
      return null;
    }
    int i = Collections.binarySearch((List)localObject, paramCharSequence);
    if (i < 0) {
      return null;
    }
    return getb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */