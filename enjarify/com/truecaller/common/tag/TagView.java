package com.truecaller.common.tag;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.a;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.View.MeasureSpec;
import com.d.b.ab;
import com.d.b.ag;
import com.d.b.w;
import com.truecaller.common.R.attr;
import com.truecaller.common.R.color;
import com.truecaller.common.R.dimen;
import com.truecaller.common.R.drawable;
import com.truecaller.common.R.styleable;
import com.truecaller.common.h.l;

public class TagView
  extends AppCompatTextView
  implements ValueAnimator.AnimatorUpdateListener, ag
{
  private static final int b = R.color.truecaller_blue_all_themes;
  private static final int c = R.attr.tagStyle;
  private final Paint d;
  private final Paint e;
  private final Paint f;
  private int g;
  private float h;
  private float i;
  private final RectF j;
  private final RectF k;
  private final RectF l;
  private final RectF m;
  private final RectF n;
  private float o;
  private Bitmap p;
  private Bitmap q;
  private ValueAnimator r;
  private boolean s;
  private final boolean t;
  private final boolean u;
  private c v;
  private int w;
  
  public TagView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, i1);
  }
  
  private TagView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    this(paramContext, paramAttributeSet, paramInt, false, true);
  }
  
  private TagView(Context paramContext, AttributeSet paramAttributeSet, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    super(paramContext, paramAttributeSet, paramInt);
    Object localObject1 = new android/graphics/RectF;
    ((RectF)localObject1).<init>();
    j = ((RectF)localObject1);
    localObject1 = new android/graphics/RectF;
    ((RectF)localObject1).<init>();
    k = ((RectF)localObject1);
    localObject1 = new android/graphics/RectF;
    ((RectF)localObject1).<init>();
    l = ((RectF)localObject1);
    localObject1 = new android/graphics/RectF;
    ((RectF)localObject1).<init>();
    m = ((RectF)localObject1);
    localObject1 = new android/graphics/RectF;
    ((RectF)localObject1).<init>();
    n = ((RectF)localObject1);
    o = 0.0F;
    t = paramBoolean1;
    u = paramBoolean2;
    localObject1 = getContext().getResources();
    paramBoolean1 = R.dimen.tag_view_stroke_width;
    paramInt = ((Resources)localObject1).getDimensionPixelSize(paramBoolean1);
    g = paramInt;
    localObject1 = getContext().getResources();
    paramBoolean1 = R.dimen.tag_view_close_btn_margin;
    paramInt = ((Resources)localObject1).getDimensionPixelSize(paramBoolean1);
    w = paramInt;
    localObject1 = new android/graphics/Paint;
    ((Paint)localObject1).<init>();
    e = ((Paint)localObject1);
    localObject1 = e;
    paramBoolean1 = true;
    float f1 = Float.MIN_VALUE;
    ((Paint)localObject1).setAntiAlias(paramBoolean1);
    localObject1 = new android/graphics/Paint;
    ((Paint)localObject1).<init>();
    f = ((Paint)localObject1);
    f.setAntiAlias(paramBoolean1);
    localObject1 = f;
    PorterDuffColorFilter localPorterDuffColorFilter = new android/graphics/PorterDuffColorFilter;
    PorterDuff.Mode localMode = PorterDuff.Mode.SRC_ATOP;
    int i1 = -1;
    localPorterDuffColorFilter.<init>(i1, localMode);
    ((Paint)localObject1).setColorFilter(localPorterDuffColorFilter);
    paramInt = t;
    Object localObject2;
    if (paramInt != 0)
    {
      localObject1 = e;
      d = ((Paint)localObject1);
      localObject2 = Paint.Style.STROKE;
      ((Paint)localObject1).setStyle((Paint.Style)localObject2);
      e.setColor(i1);
      localObject1 = e;
      paramBoolean1 = g;
      f1 = paramBoolean1;
      ((Paint)localObject1).setStrokeWidth(f1);
      setTextColor(i1);
    }
    else
    {
      localObject1 = new android/graphics/Paint;
      ((Paint)localObject1).<init>();
      d = ((Paint)localObject1);
      d.setAntiAlias(paramBoolean1);
      localObject1 = d;
      localObject2 = Paint.Style.FILL;
      ((Paint)localObject1).setStyle((Paint.Style)localObject2);
      localObject1 = e;
      localObject2 = getContext();
      paramBoolean2 = R.attr.tagBackground;
      paramBoolean1 = com.truecaller.utils.ui.b.a((Context)localObject2, paramBoolean2);
      ((Paint)localObject1).setColor(paramBoolean1);
      localObject1 = e;
      localObject2 = Paint.Style.FILL;
      ((Paint)localObject1).setStyle((Paint.Style)localObject2);
    }
    localObject1 = R.styleable.TagView;
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject1);
    int i2 = R.styleable.TagView_closeButtonVisible;
    paramInt = 0;
    localObject1 = null;
    boolean bool = paramContext.getBoolean(i2, false);
    paramContext.recycle();
    int i3;
    if (!bool)
    {
      i3 = 0;
      paramContext = null;
    }
    else
    {
      i3 = e.getColor();
      paramContext = a(i3);
    }
    p = paramContext;
    setGravity(16);
  }
  
  public TagView(Context paramContext, boolean paramBoolean1, boolean paramBoolean2)
  {
    this(paramContext, null, i1, paramBoolean1, paramBoolean2);
  }
  
  private Bitmap a(int paramInt)
  {
    Context localContext = getContext();
    if (localContext != null)
    {
      int i1 = R.drawable.ic_cancel_black_16dp;
      return l.a(l.a(localContext, i1, paramInt));
    }
    return null;
  }
  
  private void a()
  {
    RectF localRectF = m;
    float f1 = getHeight();
    float f2 = i;
    f1 -= f2;
    f2 = 2.0F;
    f1 /= f2;
    top = f1;
    localRectF = m;
    f1 = top;
    float f3 = i;
    f1 += f3;
    bottom = f1;
    localRectF = m;
    f1 = getHeight();
    f3 = h;
    f1 = (f1 - f3) / f2;
    left = f1;
    localRectF = m;
    f1 = left;
    f2 = h;
    f1 += f2;
    right = f1;
  }
  
  private void b()
  {
    RectF localRectF = l;
    float f1 = j.left;
    left = f1;
    localRectF = l;
    f1 = j.top;
    top = f1;
    localRectF = l;
    f1 = j.bottom;
    bottom = f1;
    localRectF = l;
    f1 = j.right;
    float f2 = o;
    float f3 = k.right;
    float f4 = j.right;
    f3 -= f4;
    f2 *= f3;
    f1 += f2;
    right = f1;
  }
  
  private int c()
  {
    Bitmap localBitmap = p;
    int i1;
    if (localBitmap != null)
    {
      i1 = localBitmap.getWidth();
      int i2 = w;
      i1 += i2;
    }
    else
    {
      i1 = 0;
      localBitmap = null;
    }
    return i1;
  }
  
  public final void a(Bitmap paramBitmap)
  {
    Resources localResources = getContext().getResources();
    int i1 = R.dimen.tag_view_icon_size;
    int i2 = localResources.getDimensionPixelSize(i1);
    i1 = paramBitmap.getWidth();
    int i3 = paramBitmap.getHeight();
    float f1;
    float f2;
    float f3;
    if (i1 > i3)
    {
      f1 = i2;
      h = f1;
      i1 = paramBitmap.getHeight();
      f2 = i1;
      i3 = paramBitmap.getWidth();
      f3 = i3;
      f2 /= f3;
      f1 *= f2;
      i = f1;
    }
    else
    {
      f1 = i2;
      i1 = paramBitmap.getWidth();
      f2 = i1;
      i3 = paramBitmap.getHeight();
      f3 = i3;
      f2 = f2 / f3 * f1;
      h = f2;
      i = f1;
    }
    a();
    q = paramBitmap;
    invalidate();
  }
  
  public final void a(Drawable paramDrawable) {}
  
  public final void a(boolean paramBoolean1, boolean paramBoolean2)
  {
    boolean bool1 = s;
    if (bool1 == paramBoolean1) {
      return;
    }
    s = paramBoolean1;
    paramBoolean1 = 1065353216;
    float f1 = 1.0F;
    bool1 = false;
    ValueAnimator localValueAnimator;
    if (paramBoolean2)
    {
      Object localObject = r;
      if (localObject != null) {
        ((ValueAnimator)localObject).cancel();
      }
      paramBoolean2 = true;
      localObject = new float[paramBoolean2];
      float f2 = o;
      localObject[0] = f2;
      int i1 = 1;
      boolean bool2 = s;
      if (!bool2)
      {
        paramBoolean1 = false;
        f1 = 0.0F;
        localValueAnimator = null;
      }
      localObject[i1] = f1;
      localValueAnimator = ValueAnimator.ofFloat((float[])localObject);
      r = localValueAnimator;
      r.setDuration(200L);
      r.addUpdateListener(this);
      r.start();
      return;
    }
    paramBoolean2 = s;
    if (!paramBoolean2)
    {
      paramBoolean1 = false;
      f1 = 0.0F;
      localValueAnimator = null;
    }
    o = f1;
    b();
    invalidate();
  }
  
  public final void b(Drawable paramDrawable) {}
  
  public void draw(Canvas paramCanvas)
  {
    Object localObject1 = k;
    float f1 = getHeight() / 2;
    int i1 = getHeight() / 2;
    float f2 = i1;
    Object localObject2 = e;
    paramCanvas.drawRoundRect((RectF)localObject1, f1, f2, (Paint)localObject2);
    float f3 = o;
    boolean bool1 = false;
    f1 = 0.0F;
    ColorStateList localColorStateList = null;
    boolean bool2 = f3 < 0.0F;
    int i4;
    float f4;
    Object localObject3;
    if (bool2)
    {
      localObject1 = l;
      i1 = getHeight() / 2;
      f2 = i1;
      i4 = getHeight() / 2;
      f4 = i4;
      localObject3 = d;
      paramCanvas.drawRoundRect((RectF)localObject1, f2, f4, (Paint)localObject3);
    }
    bool2 = u;
    i1 = 0;
    f2 = 0.0F;
    RectF localRectF = null;
    int i5;
    float f5;
    Paint localPaint;
    int i3;
    if (bool2)
    {
      localObject1 = l;
      i4 = getHeight() / 2;
      f4 = i4;
      i5 = getHeight() / 2;
      f5 = i5;
      localPaint = d;
      paramCanvas.drawRoundRect((RectF)localObject1, f4, f5, localPaint);
      localObject1 = q;
      if (localObject1 != null)
      {
        localObject2 = m;
        localObject3 = f;
        paramCanvas.drawBitmap((Bitmap)localObject1, null, (RectF)localObject2, (Paint)localObject3);
      }
      i3 = getHeight();
    }
    else
    {
      f3 = 0.0F;
      localObject1 = null;
      i4 = getPaddingRight();
      i5 = getPaddingLeft();
      i4 -= i5;
      i3 = Math.max(0, i4);
    }
    localObject2 = p;
    if (localObject2 != null)
    {
      localObject3 = n;
      localPaint = f;
      paramCanvas.drawBitmap((Bitmap)localObject2, null, (RectF)localObject3, localPaint);
    }
    f3 = i3;
    paramCanvas.translate(f3, 0.0F);
    super.draw(paramCanvas);
    f2 = o;
    bool1 = f2 < 0.0F;
    if (bool1)
    {
      int i2 = getCurrentTextColor();
      double d1 = a.a(i2);
      double d2 = 0.5D;
      boolean bool3 = d1 < d2;
      if (bool3)
      {
        localColorStateList = getTextColors();
        i1 = -1;
        setTextColor(i1);
        localRectF = l;
        f2 = left - f3;
        localObject2 = l;
        f4 = top;
        localObject3 = l;
        f5 = right - f3;
        localObject1 = l;
        f3 = bottom;
        paramCanvas.clipRect(f2, f4, f5, f3);
        super.draw(paramCanvas);
        setTextColor(localColorStateList);
      }
    }
  }
  
  public c getAvailableTag()
  {
    return v;
  }
  
  public long getParentTagId()
  {
    c localc = v;
    if (localc == null) {
      return 0L;
    }
    return c;
  }
  
  public long getTagId()
  {
    c localc = v;
    if (localc == null) {
      return 0L;
    }
    return a;
  }
  
  public void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    float f1 = ((Float)paramValueAnimator.getAnimatedValue()).floatValue();
    o = f1;
    b();
    invalidate();
  }
  
  public void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    int i1 = getMeasuredWidth();
    int i2 = c();
    i1 += i2;
    i2 = getMeasuredHeight();
    boolean bool = u;
    if (bool)
    {
      i1 += i2;
    }
    else
    {
      i4 = getPaddingRight();
      int i5 = getPaddingLeft();
      i4 -= i5;
      i3 = Math.max(0, i4);
      i1 += i3;
    }
    int i3 = View.MeasureSpec.getMode(paramInt1);
    int i4 = -1 << -1;
    if (i3 == i4)
    {
      paramInt1 = View.MeasureSpec.getSize(paramInt1);
      i3 = c();
      paramInt1 += i3;
      i1 = Math.min(paramInt1, i1);
    }
    paramInt1 = View.MeasureSpec.getMode(paramInt2);
    if (paramInt1 == i4)
    {
      paramInt1 = View.MeasureSpec.getSize(paramInt2);
      i2 = Math.min(paramInt1, i2);
    }
    setMeasuredDimension(i1, i2);
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    Object localObject1 = k;
    paramInt4 = g;
    float f1 = paramInt4 / 2;
    left = f1;
    int i1 = paramInt4 / 2;
    f1 = i1;
    top = f1;
    paramInt2 -= paramInt4;
    float f2 = paramInt2;
    bottom = f2;
    paramInt1 -= paramInt4;
    float f3 = paramInt1;
    right = f3;
    Object localObject2 = j;
    f2 = left;
    left = f2;
    localObject2 = j;
    f2 = k.top;
    top = f2;
    localObject2 = j;
    f2 = k.bottom;
    bottom = f2;
    localObject2 = j;
    RectF localRectF = k;
    f2 = bottom;
    right = f2;
    a();
    b();
    localObject2 = p;
    if (localObject2 != null)
    {
      localObject2 = n;
      localRectF = k;
      f2 = right;
      float f4 = w;
      f2 -= f4;
      right = f2;
      localObject2 = n;
      f2 = right;
      f4 = p.getWidth();
      f2 -= f4;
      left = f2;
      localObject2 = n;
      paramInt2 = getHeight();
      paramInt3 = p.getHeight();
      paramInt2 = (paramInt2 - paramInt3) / 2;
      f2 = paramInt2;
      top = f2;
      localObject2 = n;
      f2 = top;
      localObject1 = p;
      paramInt3 = ((Bitmap)localObject1).getHeight();
      f4 = paramInt3;
      f2 += f4;
      bottom = f2;
    }
  }
  
  public void setBackgroundColor(int paramInt)
  {
    e.setColor(paramInt);
    invalidate();
  }
  
  public void setTag(c paramc)
  {
    Object localObject1 = b;
    setText((CharSequence)localObject1);
    v = paramc;
    boolean bool1 = t;
    long l1;
    if (!bool1)
    {
      int i1 = d;
      Object localObject2;
      if (i1 != 0)
      {
        localObject1 = d;
        int i3 = d;
        localObject2 = localObject1;
        i1 = i3;
      }
      else
      {
        l1 = c;
        localObject1 = d.a(l1);
        localObject2 = d;
        if (localObject1 != null)
        {
          i4 = d;
          if (i4 != 0)
          {
            i1 = d;
            break label111;
          }
        }
        localObject1 = getContext();
        int i4 = b;
        i1 = android.support.v4.content.b.c((Context)localObject1, i4);
      }
      label111:
      ((Paint)localObject2).setColor(i1);
    }
    localObject1 = null;
    q = null;
    boolean bool2 = u;
    if (bool2)
    {
      int i2 = e;
      if (i2 == 0)
      {
        l1 = c;
        paramc = d.a(l1);
        if (paramc == null)
        {
          i5 = 0;
          paramc = null;
          break label172;
        }
      }
      int i5 = e;
      label172:
      if (i5 != 0)
      {
        localObject1 = w.a(getContext());
        paramc = ((w)localObject1).a(i5);
        paramc.a(this);
      }
    }
    requestLayout();
  }
  
  public void setTint(int paramInt)
  {
    boolean bool = t;
    if (bool)
    {
      d.setColor(paramInt);
      e.setColor(paramInt);
      Object localObject = f;
      PorterDuffColorFilter localPorterDuffColorFilter = new android/graphics/PorterDuffColorFilter;
      PorterDuff.Mode localMode = PorterDuff.Mode.SRC_ATOP;
      localPorterDuffColorFilter.<init>(paramInt, localMode);
      ((Paint)localObject).setColorFilter(localPorterDuffColorFilter);
      localObject = p;
      if (localObject != null)
      {
        localObject = a(paramInt);
        p = ((Bitmap)localObject);
      }
      setTextColor(paramInt);
      invalidate();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.TagView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */