package com.truecaller.common.tag;

public final class c
{
  public final long a;
  public final String b;
  public final long c;
  public final int d;
  public final int e;
  
  public c(long paramLong1, String paramString, long paramLong2, int paramInt1, int paramInt2)
  {
    a = paramLong1;
    b = paramString;
    c = paramLong2;
    d = paramInt1;
    e = paramInt2;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof c;
    if (!bool1) {
      return false;
    }
    paramObject = (c)paramObject;
    long l1 = a;
    long l2 = a;
    boolean bool2 = l1 < l2;
    return !bool2;
  }
  
  public final int hashCode()
  {
    return (int)a;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("AvailableTag{id=");
    long l = a;
    localStringBuilder.append(l);
    localStringBuilder.append(", name='");
    String str = b;
    localStringBuilder.append(str);
    localStringBuilder.append('\'');
    localStringBuilder.append(", parentId=");
    l = c;
    localStringBuilder.append(l);
    localStringBuilder.append(", color=");
    int i = d;
    localStringBuilder.append(i);
    localStringBuilder.append(", imageRes=");
    i = e;
    localStringBuilder.append(i);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */