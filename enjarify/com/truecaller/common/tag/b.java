package com.truecaller.common.tag;

import android.support.v4.f.o;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public final class b
{
  private static final String[] a;
  private static final b.b b;
  private final int c;
  private final b.b d;
  
  static
  {
    String[] tmp4_1 = new String[3];
    String[] tmp5_4 = tmp4_1;
    String[] tmp5_4 = tmp4_1;
    tmp5_4[0] = "term";
    tmp5_4[1] = "tag_id";
    tmp5_4[2] = "relevance";
    a = tmp5_4;
    b.b localb = new com/truecaller/common/tag/b$b;
    localb.<init>((byte)0);
    b = localb;
  }
  
  /* Error */
  public b(android.content.Context paramContext)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 32	java/lang/Object:<init>	()V
    //   4: new 23	com/truecaller/common/tag/b$b
    //   7: astore_2
    //   8: aconst_null
    //   9: astore_3
    //   10: aload_2
    //   11: iconst_0
    //   12: invokespecial 27	com/truecaller/common/tag/b$b:<init>	(B)V
    //   15: aload_0
    //   16: aload_2
    //   17: putfield 34	com/truecaller/common/tag/b:d	Lcom/truecaller/common/tag/b$b;
    //   20: ldc 36
    //   22: astore_2
    //   23: aload_2
    //   24: iconst_0
    //   25: invokestatic 41	com/truecaller/common/b/e:a	(Ljava/lang/String;Z)Z
    //   28: istore 4
    //   30: iconst_m1
    //   31: istore 5
    //   33: iload 4
    //   35: ifne +10 -> 45
    //   38: aload_0
    //   39: iload 5
    //   41: putfield 43	com/truecaller/common/tag/b:c	I
    //   44: return
    //   45: ldc 45
    //   47: astore_2
    //   48: aload_2
    //   49: iconst_m1
    //   50: i2l
    //   51: invokestatic 48	com/truecaller/common/b/e:a	(Ljava/lang/String;J)J
    //   54: lstore 6
    //   56: lload 6
    //   58: l2i
    //   59: istore 4
    //   61: aload_1
    //   62: invokestatic 53	com/truecaller/common/tag/a/d:a	(Landroid/content/Context;)Lcom/truecaller/common/c/b/a;
    //   65: invokevirtual 59	com/truecaller/common/c/b/a:getReadableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   68: astore 8
    //   70: ldc 61
    //   72: astore 9
    //   74: getstatic 21	com/truecaller/common/tag/b:a	[Ljava/lang/String;
    //   77: astore 10
    //   79: iconst_0
    //   80: istore 11
    //   82: aload 8
    //   84: aload 9
    //   86: aload 10
    //   88: aconst_null
    //   89: aconst_null
    //   90: aconst_null
    //   91: aconst_null
    //   92: aconst_null
    //   93: invokevirtual 67	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   96: astore_1
    //   97: aload_1
    //   98: ifnull +150 -> 248
    //   101: aload_1
    //   102: invokeinterface 73 1 0
    //   107: istore 12
    //   109: iload 12
    //   111: ifeq +63 -> 174
    //   114: new 75	com/truecaller/common/tag/b$a
    //   117: astore 8
    //   119: iconst_1
    //   120: istore 13
    //   122: aload_1
    //   123: iload 13
    //   125: invokeinterface 80 2 0
    //   130: istore 13
    //   132: aload_1
    //   133: iconst_0
    //   134: invokeinterface 84 2 0
    //   139: astore 10
    //   141: iconst_2
    //   142: istore 11
    //   144: aload_1
    //   145: iload 11
    //   147: invokeinterface 89 2 0
    //   152: dstore 14
    //   154: aload 8
    //   156: iload 13
    //   158: aload 10
    //   160: dload 14
    //   162: invokespecial 92	com/truecaller/common/tag/b$a:<init>	(ILjava/lang/String;D)V
    //   165: aload_0
    //   166: aload 8
    //   168: invokespecial 95	com/truecaller/common/tag/b:a	(Lcom/truecaller/common/tag/b$a;)V
    //   171: goto -70 -> 101
    //   174: aload_1
    //   175: invokeinterface 98 1 0
    //   180: goto +68 -> 248
    //   183: astore_2
    //   184: goto +56 -> 240
    //   187: astore_2
    //   188: aload_2
    //   189: invokestatic 104	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   192: aload_0
    //   193: getfield 34	com/truecaller/common/tag/b:d	Lcom/truecaller/common/tag/b$b;
    //   196: astore_2
    //   197: aload_2
    //   198: getfield 107	com/truecaller/common/tag/b$b:a	Ljava/util/Map;
    //   201: astore_3
    //   202: aload_3
    //   203: invokeinterface 112 1 0
    //   208: aload_2
    //   209: getfield 115	com/truecaller/common/tag/b$b:b	Ljava/util/List;
    //   212: astore_3
    //   213: aload_3
    //   214: ifnull +14 -> 228
    //   217: aload_2
    //   218: getfield 115	com/truecaller/common/tag/b$b:b	Ljava/util/List;
    //   221: astore_2
    //   222: aload_2
    //   223: invokeinterface 118 1 0
    //   228: aload_1
    //   229: invokeinterface 98 1 0
    //   234: iconst_m1
    //   235: istore 4
    //   237: goto +11 -> 248
    //   240: aload_1
    //   241: invokeinterface 98 1 0
    //   246: aload_2
    //   247: athrow
    //   248: aload_0
    //   249: iload 4
    //   251: putfield 43	com/truecaller/common/tag/b:c	I
    //   254: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	255	0	this	b
    //   0	255	1	paramContext	android.content.Context
    //   7	42	2	localObject1	Object
    //   183	1	2	localObject2	Object
    //   187	2	2	localSQLiteException	android.database.sqlite.SQLiteException
    //   196	51	2	localObject3	Object
    //   9	205	3	localObject4	Object
    //   28	6	4	bool1	boolean
    //   59	191	4	i	int
    //   31	9	5	j	int
    //   54	3	6	l	long
    //   68	99	8	localObject5	Object
    //   72	13	9	str	String
    //   77	82	10	localObject6	Object
    //   80	66	11	k	int
    //   107	3	12	bool2	boolean
    //   120	37	13	m	int
    //   152	9	14	d1	double
    // Exception table:
    //   from	to	target	type
    //   101	107	183	finally
    //   114	117	183	finally
    //   123	130	183	finally
    //   133	139	183	finally
    //   145	152	183	finally
    //   160	165	183	finally
    //   166	171	183	finally
    //   188	192	183	finally
    //   192	196	183	finally
    //   197	201	183	finally
    //   202	208	183	finally
    //   208	212	183	finally
    //   217	221	183	finally
    //   222	228	183	finally
    //   101	107	187	android/database/sqlite/SQLiteException
    //   114	117	187	android/database/sqlite/SQLiteException
    //   123	130	187	android/database/sqlite/SQLiteException
    //   133	139	187	android/database/sqlite/SQLiteException
    //   145	152	187	android/database/sqlite/SQLiteException
    //   160	165	187	android/database/sqlite/SQLiteException
    //   166	171	187	android/database/sqlite/SQLiteException
  }
  
  private void a(b.a parama)
  {
    Object localObject1 = new com/truecaller/common/tag/b$d;
    Object localObject2 = b;
    ((b.d)localObject1).<init>((CharSequence)localObject2);
    Object localObject5;
    for (localObject2 = d;; localObject2 = b)
    {
      boolean bool = ((b.d)localObject1).a();
      if (!bool) {
        break;
      }
      bool = false;
      Object localObject3 = null;
      char c1 = Character.toLowerCase(((CharSequence)localObject1).charAt(0));
      Object localObject4 = a;
      localObject5 = Character.valueOf(c1);
      localObject4 = (List)((Map)localObject4).get(localObject5);
      if (localObject4 == null)
      {
        localObject4 = new java/util/ArrayList;
        ((ArrayList)localObject4).<init>();
        localObject5 = new com/truecaller/common/tag/b$c;
        String str1 = ((CharSequence)localObject1).toString();
        ((b.c)localObject5).<init>(str1, (byte)0);
        localObject3 = new com/truecaller/common/tag/b$b;
        ((b.b)localObject3).<init>();
        b = ((b.b)localObject3);
        ((List)localObject4).add(localObject5);
        localObject2 = a;
        localObject3 = Character.valueOf(c1);
        ((Map)localObject2).put(localObject3, localObject4);
      }
      else
      {
        int i = Collections.binarySearch((List)localObject4, localObject1);
        if (i < 0)
        {
          i = -i + -1;
          localObject5 = new com/truecaller/common/tag/b$c;
          String str2 = ((CharSequence)localObject1).toString();
          ((b.c)localObject5).<init>(str2, (byte)0);
          localObject3 = new com/truecaller/common/tag/b$b;
          ((b.b)localObject3).<init>();
          b = ((b.b)localObject3);
          ((List)localObject4).add(i, localObject5);
        }
        else
        {
          localObject2 = ((List)localObject4).get(i);
          localObject5 = localObject2;
          localObject5 = (b.c)localObject2;
        }
      }
    }
    localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
      b = ((List)localObject1);
    }
    b.add(parama);
  }
  
  private void a(b.b paramb, o paramo)
  {
    List localList = b;
    int i = 0;
    int j;
    if (localList == null)
    {
      j = 0;
      localList = null;
    }
    else
    {
      localList = b;
      j = localList.size();
    }
    while (i < j)
    {
      b.a locala = (b.a)b.get(i);
      int k = a;
      a locala1 = null;
      a locala2 = (a)paramo.a(k, null);
      double d2;
      if (locala2 != null)
      {
        double d1 = c;
        d2 = c;
        d1 += d2;
        c = d1;
      }
      else
      {
        k = a;
        locala1 = new com/truecaller/common/tag/a;
        int m = a;
        d2 = c;
        int n = c;
        locala1.<init>(m, d2, n);
        paramo.c(k, locala1);
      }
      i += 1;
    }
  }
  
  public final boolean a(String paramString, int paramInt, o paramo)
  {
    int i = c;
    int j = -1;
    if ((i != j) && (paramInt != i))
    {
      paramInt = TextUtils.isEmpty(paramString);
      if (paramInt == 0)
      {
        b.d locald = new com/truecaller/common/tag/b$d;
        locald.<init>(paramString);
        paramString = new java/util/LinkedList;
        paramString.<init>();
        b.b localb1 = b;
        paramString.add(localb1);
        for (;;)
        {
          boolean bool = locald.a();
          if (!bool) {
            break;
          }
          for (;;)
          {
            localb1 = (b.b)paramString.poll();
            b.b localb2 = b;
            if (localb1 == localb2) {
              break;
            }
            localb1 = localb1.a(locald);
            if (localb1 != null)
            {
              a(localb1, paramo);
              paramString.add(localb1);
            }
          }
          localb1 = d.a(locald);
          if (localb1 != null)
          {
            a(localb1, paramo);
            paramString.add(localb1);
          }
          localb1 = b;
          paramString.add(localb1);
        }
        int k = paramo.c();
        return k != 0;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */