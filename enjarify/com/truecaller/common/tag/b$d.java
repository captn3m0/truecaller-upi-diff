package com.truecaller.common.tag;

final class b$d
  implements CharSequence
{
  private final CharSequence a;
  private final int b;
  private int c;
  private int d = -1;
  
  b$d(CharSequence paramCharSequence)
  {
    a = paramCharSequence;
    int i = paramCharSequence.length();
    b = i;
  }
  
  public final boolean a()
  {
    int i = d;
    int k = 1;
    i += k;
    d = i;
    i = d;
    c = i;
    int j;
    for (;;)
    {
      i = d;
      m = b;
      if (i >= m) {
        break;
      }
      CharSequence localCharSequence = a;
      boolean bool = Character.isWhitespace(localCharSequence.charAt(i));
      if (bool)
      {
        j = c;
        m = d;
        if (j != m) {
          return k;
        }
        m += 1;
        c = m;
      }
      j = d + k;
      d = j;
    }
    int m = c;
    if (m != j) {
      return k;
    }
    return false;
  }
  
  public final char charAt(int paramInt)
  {
    CharSequence localCharSequence = a;
    int i = c;
    paramInt += i;
    return localCharSequence.charAt(paramInt);
  }
  
  public final int length()
  {
    int i = d;
    int j = c;
    return i - j;
  }
  
  public final CharSequence subSequence(int paramInt1, int paramInt2)
  {
    CharSequence localCharSequence = a;
    int i = c;
    paramInt1 += i;
    i += paramInt2;
    return localCharSequence.subSequence(paramInt1, i);
  }
  
  public final String toString()
  {
    CharSequence localCharSequence = a;
    int i = c;
    int j = d;
    return localCharSequence.subSequence(i, j).toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */