package com.truecaller.common.tag.sync;

import androidx.work.a;
import androidx.work.j;
import androidx.work.p;
import c.g.b.w;
import c.l.b;

public final class AvailableTagsDownloadWorker$a
  implements com.truecaller.common.background.h
{
  public final com.truecaller.common.background.g a()
  {
    com.truecaller.common.background.g localg = new com/truecaller/common/background/g;
    Object localObject1 = w.a(AvailableTagsDownloadWorker.class);
    org.a.a.h localh = org.a.a.h.a(7);
    localg.<init>((b)localObject1, localh);
    long l = 1L;
    Object localObject2 = org.a.a.h.a(l);
    c.g.b.k.a(localObject2, "Duration.standardDays(1)");
    localg = localg.a((org.a.a.h)localObject2);
    localObject2 = a.a;
    localObject1 = org.a.a.h.b(l);
    c.g.b.k.a(localObject1, "Duration.standardHours(1)");
    localg = localg.a((a)localObject2, (org.a.a.h)localObject1);
    localObject1 = j.b;
    return localg.a((j)localObject1);
  }
  
  public final void b()
  {
    p localp = p.a();
    c.g.b.k.a(localp, "WorkManager.getInstance()");
    androidx.work.g localg = androidx.work.g.a;
    Object localObject = this;
    localObject = ((a)this).a().b();
    localp.a("AvailableTagsDownloadWorkerOneOff", localg, (androidx.work.k)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.sync.AvailableTagsDownloadWorker.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */