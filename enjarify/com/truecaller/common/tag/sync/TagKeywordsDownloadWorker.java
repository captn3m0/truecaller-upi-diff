package com.truecaller.common.tag.sync;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import androidx.work.p;
import com.truecaller.analytics.b;
import com.truecaller.common.account.r;
import com.truecaller.common.b.e;
import com.truecaller.common.background.TrackedWorker;
import com.truecaller.common.h.ac;
import com.truecaller.common.tag.d;

public final class TagKeywordsDownloadWorker
  extends TrackedWorker
{
  public static final TagKeywordsDownloadWorker.a e;
  public b b;
  public r c;
  public ac d;
  private final Context f;
  
  static
  {
    TagKeywordsDownloadWorker.a locala = new com/truecaller/common/tag/sync/TagKeywordsDownloadWorker$a;
    locala.<init>((byte)0);
    e = locala;
  }
  
  public TagKeywordsDownloadWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    f = paramContext;
    paramContext = com.truecaller.common.b.a.F();
    c.g.b.k.a(paramContext, "ApplicationBase.getAppBase()");
    paramContext.u().a(this);
  }
  
  public static final void e()
  {
    p localp = p.a();
    c.g.b.k.a(localp, "WorkManager.getInstance()");
    androidx.work.g localg = androidx.work.g.a;
    androidx.work.k localk = e.a().b();
    localp.a("TagKeywordsDownloadWorkerOneOff", localg, localk);
  }
  
  public final b b()
  {
    b localb = b;
    if (localb == null)
    {
      String str = "analytics";
      c.g.b.k.a(str);
    }
    return localb;
  }
  
  public final boolean c()
  {
    Object localObject = c;
    if (localObject == null)
    {
      str1 = "accountManager";
      c.g.b.k.a(str1);
    }
    boolean bool = ((r)localObject).c();
    String str1 = null;
    if (bool)
    {
      localObject = "featureAutoTagging";
      bool = e.a((String)localObject, false);
      if (bool)
      {
        localObject = d;
        if (localObject == null)
        {
          String str2 = "regionUtils";
          c.g.b.k.a(str2);
        }
        bool = ((ac)localObject).a();
        if (!bool) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final ListenableWorker.a d()
  {
    Object localObject = f;
    boolean bool1 = d.c((Context)localObject);
    if (bool1)
    {
      long l1 = 0L;
      long l2 = e.a("tagsKeywordsFeatureCurrentVersion", l1);
      localObject = "tagsKeywordsFeatureLastVersion";
      long l3 = e.a((String)localObject, l1);
      bool1 = true;
      boolean bool2 = l2 < l3;
      int i;
      String str;
      if (bool2)
      {
        i = 1;
      }
      else
      {
        i = 0;
        str = null;
      }
      if (i != 0)
      {
        str = "tagsPhonebookForcedUpload";
        e.b(str, bool1);
      }
      long l4 = e.a("tagsKeywordsFeatureCurrentVersion", l1);
      e.b("tagsKeywordsFeatureLastVersion", l4);
      localObject = ListenableWorker.a.a();
      c.g.b.k.a(localObject, "Result.success()");
      return (ListenableWorker.a)localObject;
    }
    localObject = ListenableWorker.a.b();
    c.g.b.k.a(localObject, "Result.retry()");
    return (ListenableWorker.a)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.sync.TagKeywordsDownloadWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */