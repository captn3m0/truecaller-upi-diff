package com.truecaller.common.tag.sync;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.common.account.r;
import com.truecaller.common.background.TrackedWorker;
import com.truecaller.common.tag.d;

public final class AvailableTagsDownloadWorker
  extends TrackedWorker
{
  public static final AvailableTagsDownloadWorker.a d;
  public b b;
  public r c;
  private final Context e;
  
  static
  {
    AvailableTagsDownloadWorker.a locala = new com/truecaller/common/tag/sync/AvailableTagsDownloadWorker$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  public AvailableTagsDownloadWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    e = paramContext;
    paramContext = com.truecaller.common.b.a.F();
    k.a(paramContext, "ApplicationBase.getAppBase()");
    paramContext.u().a(this);
  }
  
  public static final void e()
  {
    d.b();
  }
  
  public final b b()
  {
    b localb = b;
    if (localb == null)
    {
      String str = "analytics";
      k.a(str);
    }
    return localb;
  }
  
  public final boolean c()
  {
    r localr = c;
    if (localr == null)
    {
      String str = "accountManager";
      k.a(str);
    }
    return localr.c();
  }
  
  public final ListenableWorker.a d()
  {
    Object localObject = e;
    boolean bool = d.b((Context)localObject);
    if (bool) {
      localObject = ListenableWorker.a.a();
    } else {
      localObject = ListenableWorker.a.b();
    }
    k.a(localObject, "if (TagManager.fetchAvai…ess() else Result.retry()");
    return (ListenableWorker.a)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.sync.AvailableTagsDownloadWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */