package com.truecaller.common.tag.sync;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.c;
import androidx.work.c.a;
import androidx.work.g;
import androidx.work.j;
import androidx.work.k.a;
import androidx.work.p;
import com.truecaller.common.account.r;
import com.truecaller.common.tag.d;

public final class TagsUploadWorker
  extends Worker
{
  public static final TagsUploadWorker.a c;
  public r b;
  private final Context d;
  
  static
  {
    TagsUploadWorker.a locala = new com/truecaller/common/tag/sync/TagsUploadWorker$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public TagsUploadWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    d = paramContext;
    paramContext = com.truecaller.common.b.a.F();
    c.g.b.k.a(paramContext, "ApplicationBase.getAppBase()");
    paramContext.u().a(this);
  }
  
  public static final void b()
  {
    p localp = p.a();
    g localg = g.a;
    Object localObject1 = new androidx/work/k$a;
    ((k.a)localObject1).<init>(TagsUploadWorker.class);
    Object localObject2 = new androidx/work/c$a;
    ((c.a)localObject2).<init>();
    j localj = j.b;
    localObject2 = ((c.a)localObject2).a(localj).a();
    localObject1 = (androidx.work.k)((k.a)((k.a)localObject1).a((c)localObject2)).c();
    localp.a("TagsUploadWorker", localg, (androidx.work.k)localObject1);
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "accountManager";
      c.g.b.k.a((String)localObject2);
    }
    boolean bool1 = ((r)localObject1).c();
    if (!bool1)
    {
      localObject1 = ListenableWorker.a.a();
      c.g.b.k.a(localObject1, "Result.success()");
      return (ListenableWorker.a)localObject1;
    }
    localObject1 = d;
    bool1 = d.d((Context)localObject1);
    Object localObject2 = d;
    boolean bool2 = d.e((Context)localObject2);
    if ((bool1) && (bool2))
    {
      new String[1][0] = "Tags and name suggestions upload success!";
      localObject1 = ListenableWorker.a.a();
      c.g.b.k.a(localObject1, "Result.success()");
      return (ListenableWorker.a)localObject1;
    }
    new String[1][0] = "Tags and name suggestions upload upload failed";
    localObject1 = ListenableWorker.a.b();
    c.g.b.k.a(localObject1, "Result.retry()");
    return (ListenableWorker.a)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.sync.TagsUploadWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */