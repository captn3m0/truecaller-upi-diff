package com.truecaller.common.tag;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.truecaller.common.R.drawable;
import com.truecaller.common.h.ac;
import com.truecaller.common.tag.sync.TagsUploadWorker;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class d
{
  public static final Map a;
  public static final Map b;
  
  static
  {
    Object localObject = new java/util/concurrent/ConcurrentHashMap;
    ((ConcurrentHashMap)localObject).<init>();
    a = (Map)localObject;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    b = (Map)localObject;
    Long localLong = Long.valueOf(1L);
    d.a locala = new com/truecaller/common/tag/d$a;
    int i = R.drawable.ic_tag_education;
    locala.<init>(-12151053, i);
    ((Map)localObject).put(localLong, locala);
    localObject = b;
    localLong = Long.valueOf(2);
    locala = new com/truecaller/common/tag/d$a;
    i = R.drawable.ic_tag_entertainment_arts;
    locala.<init>(-12627531, i);
    ((Map)localObject).put(localLong, locala);
    localObject = b;
    localLong = Long.valueOf(3);
    locala = new com/truecaller/common/tag/d$a;
    i = R.drawable.ic_tag_finance_legal_insurance;
    locala.<init>(-10453621, i);
    ((Map)localObject).put(localLong, locala);
    localObject = b;
    localLong = Long.valueOf(4);
    locala = new com/truecaller/common/tag/d$a;
    i = R.drawable.ic_tag_governmental_public_services;
    locala.<init>(-12232092, i);
    ((Map)localObject).put(localLong, locala);
    localObject = b;
    localLong = Long.valueOf(5);
    locala = new com/truecaller/common/tag/d$a;
    i = R.drawable.ic_tag_health_wellness;
    locala.<init>(-769226, i);
    ((Map)localObject).put(localLong, locala);
    localObject = b;
    localLong = Long.valueOf(6);
    locala = new com/truecaller/common/tag/d$a;
    i = R.drawable.ic_tag_hotels_accomodation;
    locala.<init>(-8825528, i);
    ((Map)localObject).put(localLong, locala);
    localObject = b;
    localLong = Long.valueOf(7);
    locala = new com/truecaller/common/tag/d$a;
    i = R.drawable.ic_tag_nightlife_drinks;
    locala.<init>(-10011977, i);
    ((Map)localObject).put(localLong, locala);
    localObject = b;
    localLong = Long.valueOf(8);
    locala = new com/truecaller/common/tag/d$a;
    i = R.drawable.ic_tag_restaurants_cafe;
    locala.<init>(-11751600, i);
    ((Map)localObject).put(localLong, locala);
    localObject = b;
    localLong = Long.valueOf(9);
    locala = new com/truecaller/common/tag/d$a;
    i = R.drawable.ic_tag_services;
    locala.<init>(-6381922, i);
    ((Map)localObject).put(localLong, locala);
    localObject = b;
    localLong = Long.valueOf(10);
    locala = new com/truecaller/common/tag/d$a;
    i = R.drawable.ic_tag_shopping_convenience_stores;
    locala.<init>(-16738680, i);
    ((Map)localObject).put(localLong, locala);
    localObject = b;
    localLong = Long.valueOf(11);
    locala = new com/truecaller/common/tag/d$a;
    i = R.drawable.ic_tag_transportation;
    locala.<init>(45824, i);
    ((Map)localObject).put(localLong, locala);
    localObject = b;
    localLong = Long.valueOf(12);
    locala = new com/truecaller/common/tag/d$a;
    i = R.drawable.ic_tag_travel_tourism;
    locala.<init>(-43230, i);
    ((Map)localObject).put(localLong, locala);
  }
  
  public static c a(long paramLong)
  {
    Map localMap = a;
    Long localLong = Long.valueOf(paramLong);
    return (c)localMap.get(localLong);
  }
  
  /* Error */
  public static List a(Context paramContext, long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic 130	com/truecaller/common/tag/a/d:a	(Landroid/content/Context;)Lcom/truecaller/common/c/b/a;
    //   4: invokevirtual 136	com/truecaller/common/c/b/a:getReadableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   7: astore_3
    //   8: ldc -118
    //   10: astore 4
    //   12: ldc -116
    //   14: astore_0
    //   15: iconst_3
    //   16: anewarray 146	java/lang/String
    //   19: dup
    //   20: dup2
    //   21: iconst_0
    //   22: aload_0
    //   23: aastore
    //   24: iconst_1
    //   25: ldc -114
    //   27: aastore
    //   28: iconst_2
    //   29: ldc -112
    //   31: aastore
    //   32: astore 5
    //   34: ldc -108
    //   36: astore 6
    //   38: iconst_1
    //   39: istore 7
    //   41: iload 7
    //   43: anewarray 146	java/lang/String
    //   46: astore 8
    //   48: lload_1
    //   49: invokestatic 152	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   52: astore 9
    //   54: aload 8
    //   56: iconst_0
    //   57: aload 9
    //   59: aastore
    //   60: ldc -102
    //   62: astore 10
    //   64: aconst_null
    //   65: astore 11
    //   67: aload_3
    //   68: aload 4
    //   70: aload 5
    //   72: aload 6
    //   74: aload 8
    //   76: aconst_null
    //   77: aconst_null
    //   78: aload 10
    //   80: invokevirtual 160	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   83: astore 9
    //   85: new 162	java/util/ArrayList
    //   88: astore_3
    //   89: bipush 10
    //   91: istore 12
    //   93: aload_3
    //   94: iload 12
    //   96: invokespecial 166	java/util/ArrayList:<init>	(I)V
    //   99: aload 9
    //   101: ifnull +219 -> 320
    //   104: aload 9
    //   106: invokeinterface 172 1 0
    //   111: istore 12
    //   113: iload 12
    //   115: ifeq +174 -> 289
    //   118: aload 9
    //   120: iconst_0
    //   121: invokeinterface 176 2 0
    //   126: lstore 13
    //   128: aload 9
    //   130: iload 7
    //   132: invokeinterface 180 2 0
    //   137: astore 11
    //   139: iconst_2
    //   140: istore 12
    //   142: aload 9
    //   144: iload 12
    //   146: invokeinterface 176 2 0
    //   151: lstore 15
    //   153: getstatic 20	com/truecaller/common/tag/d:b	Ljava/util/Map;
    //   156: astore 4
    //   158: lconst_0
    //   159: lstore 17
    //   161: lload 15
    //   163: lload 17
    //   165: lcmp
    //   166: istore 19
    //   168: iload 19
    //   170: ifne +10 -> 180
    //   173: lload 13
    //   175: lstore 17
    //   177: goto +7 -> 184
    //   180: lload 15
    //   182: lstore 17
    //   184: lload 17
    //   186: invokestatic 28	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   189: astore 5
    //   191: aload 4
    //   193: aload 5
    //   195: invokeinterface 123 2 0
    //   200: astore 4
    //   202: aload 4
    //   204: checkcast 30	com/truecaller/common/tag/d$a
    //   207: astore 4
    //   209: aload 4
    //   211: ifnull +17 -> 228
    //   214: aload 4
    //   216: getfield 183	com/truecaller/common/tag/d$a:a	I
    //   219: istore 19
    //   221: iload 19
    //   223: istore 20
    //   225: goto +6 -> 231
    //   228: iconst_0
    //   229: istore 20
    //   231: aload 4
    //   233: ifnull +17 -> 250
    //   236: aload 4
    //   238: getfield 185	com/truecaller/common/tag/d$a:b	I
    //   241: istore 12
    //   243: iload 12
    //   245: istore 21
    //   247: goto +6 -> 253
    //   250: iconst_0
    //   251: istore 21
    //   253: new 125	com/truecaller/common/tag/c
    //   256: astore 4
    //   258: aload 4
    //   260: astore 5
    //   262: aload 4
    //   264: lload 13
    //   266: aload 11
    //   268: lload 15
    //   270: iload 20
    //   272: iload 21
    //   274: invokespecial 188	com/truecaller/common/tag/c:<init>	(JLjava/lang/String;JII)V
    //   277: aload_3
    //   278: aload 4
    //   280: invokeinterface 194 2 0
    //   285: pop
    //   286: goto -182 -> 104
    //   289: aload 9
    //   291: invokeinterface 197 1 0
    //   296: goto +24 -> 320
    //   299: astore_0
    //   300: goto +11 -> 311
    //   303: astore_0
    //   304: aload_0
    //   305: invokestatic 202	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   308: goto -19 -> 289
    //   311: aload 9
    //   313: invokeinterface 197 1 0
    //   318: aload_0
    //   319: athrow
    //   320: aload_3
    //   321: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	322	0	paramContext	Context
    //   0	322	1	paramLong	long
    //   7	314	3	localObject1	Object
    //   10	269	4	localObject2	Object
    //   32	229	5	localObject3	Object
    //   36	37	6	str1	String
    //   39	92	7	i	int
    //   46	29	8	arrayOfString	String[]
    //   52	260	9	localObject4	Object
    //   62	17	10	str2	String
    //   65	202	11	str3	String
    //   91	4	12	j	int
    //   111	3	12	bool1	boolean
    //   140	104	12	k	int
    //   126	139	13	l1	long
    //   151	118	15	l2	long
    //   159	26	17	l3	long
    //   166	3	19	bool2	boolean
    //   219	3	19	m	int
    //   223	48	20	n	int
    //   245	28	21	i1	int
    // Exception table:
    //   from	to	target	type
    //   104	111	299	finally
    //   120	126	299	finally
    //   130	137	299	finally
    //   144	151	299	finally
    //   153	156	299	finally
    //   184	189	299	finally
    //   193	200	299	finally
    //   202	207	299	finally
    //   214	219	299	finally
    //   236	241	299	finally
    //   253	256	299	finally
    //   272	277	299	finally
    //   278	286	299	finally
    //   304	308	299	finally
    //   104	111	303	android/database/sqlite/SQLiteException
    //   120	126	303	android/database/sqlite/SQLiteException
    //   130	137	303	android/database/sqlite/SQLiteException
    //   144	151	303	android/database/sqlite/SQLiteException
    //   153	156	303	android/database/sqlite/SQLiteException
    //   184	189	303	android/database/sqlite/SQLiteException
    //   193	200	303	android/database/sqlite/SQLiteException
    //   202	207	303	android/database/sqlite/SQLiteException
    //   214	219	303	android/database/sqlite/SQLiteException
    //   236	241	303	android/database/sqlite/SQLiteException
    //   253	256	303	android/database/sqlite/SQLiteException
    //   272	277	303	android/database/sqlite/SQLiteException
    //   278	286	303	android/database/sqlite/SQLiteException
  }
  
  /* Error */
  public static void a(Context paramContext)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aload_0
    //   3: invokestatic 130	com/truecaller/common/tag/a/d:a	(Landroid/content/Context;)Lcom/truecaller/common/c/b/a;
    //   6: astore_0
    //   7: aload_0
    //   8: invokevirtual 136	com/truecaller/common/c/b/a:getReadableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   11: astore_0
    //   12: ldc -118
    //   14: astore_2
    //   15: ldc -116
    //   17: astore_3
    //   18: ldc -114
    //   20: astore 4
    //   22: ldc -112
    //   24: astore 5
    //   26: iconst_3
    //   27: anewarray 146	java/lang/String
    //   30: dup
    //   31: dup2
    //   32: iconst_0
    //   33: aload_3
    //   34: aastore
    //   35: iconst_1
    //   36: aload 4
    //   38: aastore
    //   39: iconst_2
    //   40: aload 5
    //   42: aastore
    //   43: astore 4
    //   45: aconst_null
    //   46: astore 5
    //   48: aconst_null
    //   49: astore 6
    //   51: ldc -102
    //   53: astore 7
    //   55: aload_0
    //   56: astore_3
    //   57: aload_0
    //   58: aload_2
    //   59: aload 4
    //   61: aconst_null
    //   62: aconst_null
    //   63: aconst_null
    //   64: aconst_null
    //   65: aload 7
    //   67: invokevirtual 160	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   70: astore_1
    //   71: aload_1
    //   72: ifnull +178 -> 250
    //   75: getstatic 15	com/truecaller/common/tag/d:a	Ljava/util/Map;
    //   78: astore_3
    //   79: aload_3
    //   80: invokeinterface 207 1 0
    //   85: aload_1
    //   86: invokeinterface 172 1 0
    //   91: istore 8
    //   93: iload 8
    //   95: ifeq +155 -> 250
    //   98: iconst_0
    //   99: istore 8
    //   101: aconst_null
    //   102: astore_3
    //   103: aload_1
    //   104: iconst_0
    //   105: invokeinterface 176 2 0
    //   110: lstore 9
    //   112: iconst_1
    //   113: istore 11
    //   115: aload_1
    //   116: iload 11
    //   118: invokeinterface 180 2 0
    //   123: astore 6
    //   125: iconst_2
    //   126: istore 11
    //   128: aload_1
    //   129: iload 11
    //   131: invokeinterface 176 2 0
    //   136: lstore 12
    //   138: getstatic 20	com/truecaller/common/tag/d:b	Ljava/util/Map;
    //   141: astore_2
    //   142: lload 9
    //   144: invokestatic 28	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   147: astore 4
    //   149: aload_2
    //   150: aload 4
    //   152: invokeinterface 123 2 0
    //   157: astore_2
    //   158: aload_2
    //   159: checkcast 30	com/truecaller/common/tag/d$a
    //   162: astore_2
    //   163: aload_2
    //   164: ifnull +16 -> 180
    //   167: aload_2
    //   168: getfield 183	com/truecaller/common/tag/d$a:a	I
    //   171: istore 14
    //   173: iload 14
    //   175: istore 15
    //   177: goto +9 -> 186
    //   180: iconst_0
    //   181: istore 15
    //   183: aconst_null
    //   184: astore 7
    //   186: aload_2
    //   187: ifnull +16 -> 203
    //   190: aload_2
    //   191: getfield 185	com/truecaller/common/tag/d$a:b	I
    //   194: istore 8
    //   196: iload 8
    //   198: istore 16
    //   200: goto +6 -> 206
    //   203: iconst_0
    //   204: istore 16
    //   206: new 125	com/truecaller/common/tag/c
    //   209: astore_3
    //   210: aload_3
    //   211: astore_2
    //   212: aload_3
    //   213: lload 9
    //   215: aload 6
    //   217: lload 12
    //   219: iload 15
    //   221: iload 16
    //   223: invokespecial 188	com/truecaller/common/tag/c:<init>	(JLjava/lang/String;JII)V
    //   226: getstatic 15	com/truecaller/common/tag/d:a	Ljava/util/Map;
    //   229: astore_2
    //   230: lload 9
    //   232: invokestatic 28	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   235: astore 4
    //   237: aload_2
    //   238: aload 4
    //   240: aload_3
    //   241: invokeinterface 47 3 0
    //   246: pop
    //   247: goto -162 -> 85
    //   250: aload_1
    //   251: invokestatic 212	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   254: aload_0
    //   255: invokestatic 212	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   258: return
    //   259: astore_3
    //   260: goto +12 -> 272
    //   263: astore_3
    //   264: aconst_null
    //   265: astore_0
    //   266: goto +14 -> 280
    //   269: astore_3
    //   270: aconst_null
    //   271: astore_0
    //   272: aload_3
    //   273: invokestatic 202	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   276: goto -26 -> 250
    //   279: astore_3
    //   280: aload_1
    //   281: invokestatic 212	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   284: aload_0
    //   285: invokestatic 212	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   288: aload_3
    //   289: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	290	0	paramContext	Context
    //   1	280	1	localCursor	android.database.Cursor
    //   14	224	2	localObject1	Object
    //   17	224	3	localObject2	Object
    //   259	1	3	localSQLiteException1	SQLiteException
    //   263	1	3	localObject3	Object
    //   269	4	3	localSQLiteException2	SQLiteException
    //   279	10	3	localObject4	Object
    //   20	219	4	localObject5	Object
    //   24	23	5	str1	String
    //   49	167	6	str2	String
    //   53	132	7	str3	String
    //   91	9	8	bool	boolean
    //   194	3	8	i	int
    //   110	121	9	l1	long
    //   113	17	11	j	int
    //   136	82	12	l2	long
    //   171	3	14	k	int
    //   175	45	15	m	int
    //   198	24	16	n	int
    // Exception table:
    //   from	to	target	type
    //   26	43	259	android/database/sqlite/SQLiteException
    //   65	70	259	android/database/sqlite/SQLiteException
    //   75	78	259	android/database/sqlite/SQLiteException
    //   79	85	259	android/database/sqlite/SQLiteException
    //   85	91	259	android/database/sqlite/SQLiteException
    //   104	110	259	android/database/sqlite/SQLiteException
    //   116	123	259	android/database/sqlite/SQLiteException
    //   129	136	259	android/database/sqlite/SQLiteException
    //   138	141	259	android/database/sqlite/SQLiteException
    //   142	147	259	android/database/sqlite/SQLiteException
    //   150	157	259	android/database/sqlite/SQLiteException
    //   158	162	259	android/database/sqlite/SQLiteException
    //   167	171	259	android/database/sqlite/SQLiteException
    //   190	194	259	android/database/sqlite/SQLiteException
    //   206	209	259	android/database/sqlite/SQLiteException
    //   221	226	259	android/database/sqlite/SQLiteException
    //   226	229	259	android/database/sqlite/SQLiteException
    //   230	235	259	android/database/sqlite/SQLiteException
    //   240	247	259	android/database/sqlite/SQLiteException
    //   2	6	263	finally
    //   7	11	263	finally
    //   2	6	269	android/database/sqlite/SQLiteException
    //   7	11	269	android/database/sqlite/SQLiteException
    //   26	43	279	finally
    //   65	70	279	finally
    //   75	78	279	finally
    //   79	85	279	finally
    //   85	91	279	finally
    //   104	110	279	finally
    //   116	123	279	finally
    //   129	136	279	finally
    //   138	141	279	finally
    //   142	147	279	finally
    //   150	157	279	finally
    //   158	162	279	finally
    //   167	171	279	finally
    //   190	194	279	finally
    //   206	209	279	finally
    //   221	226	279	finally
    //   226	229	279	finally
    //   230	235	279	finally
    //   240	247	279	finally
    //   272	276	279	finally
  }
  
  public static void a(Context paramContext, String paramString, int paramInt)
  {
    Object localObject1 = ((com.truecaller.common.b.a)paramContext.getApplicationContext()).u().n();
    boolean bool1 = ((ac)localObject1).a();
    int i = 0;
    try
    {
      paramContext = com.truecaller.common.tag.a.d.a(paramContext);
      paramContext = paramContext.getWritableDatabase();
      paramContext.beginTransaction();
      try
      {
        boolean bool2 = a((ac)localObject1, bool1, paramString, paramInt);
        bool1 = true;
        if (bool2)
        {
          localObject1 = new android/content/ContentValues;
          ((ContentValues)localObject1).<init>();
          String str = "type";
          Object localObject2 = Integer.valueOf(paramInt);
          ((ContentValues)localObject1).put(str, (Integer)localObject2);
          localObject2 = "name_suggestions";
          str = "number=?";
          String[] arrayOfString = new String[bool1];
          arrayOfString[0] = paramString;
          paramInt = paramContext.update((String)localObject2, (ContentValues)localObject1, str, arrayOfString);
          if (paramInt == 0)
          {
            localObject2 = "number";
            ((ContentValues)localObject1).put((String)localObject2, paramString);
            paramString = "name_suggestions";
            paramInt = 0;
            localObject2 = null;
            long l1 = paramContext.insert(paramString, null, (ContentValues)localObject1);
            long l2 = 0L;
            bool2 = l1 < l2;
            if (bool2) {}
          }
          else
          {
            i = 1;
          }
        }
        paramContext.setTransactionSuccessful();
      }
      finally
      {
        paramContext.endTransaction();
      }
      return;
    }
    catch (SQLiteException paramContext)
    {
      com.truecaller.log.d.a(paramContext);
      if (i != 0) {
        TagsUploadWorker.b();
      }
    }
  }
  
  public static void a(Context paramContext, List paramList, long paramLong1, long paramLong2, int paramInt1, int paramInt2)
  {
    try
    {
      paramContext = com.truecaller.common.tag.a.d.a(paramContext);
      paramContext = paramContext.getWritableDatabase();
      paramContext.beginTransaction();
      try
      {
        paramList = paramList.iterator();
        for (;;)
        {
          boolean bool = paramList.hasNext();
          if (!bool) {
            break;
          }
          Object localObject1 = paramList.next();
          localObject1 = (Long)localObject1;
          localObject1 = String.valueOf(localObject1);
          Object localObject2 = "user_tags";
          Object localObject3 = "normalized_number=?";
          int i = 1;
          String[] arrayOfString = new String[i];
          arrayOfString[0] = localObject1;
          paramContext.delete((String)localObject2, (String)localObject3, arrayOfString);
          localObject2 = new android/content/ContentValues;
          ((ContentValues)localObject2).<init>();
          localObject3 = "normalized_number";
          ((ContentValues)localObject2).put((String)localObject3, (String)localObject1);
          localObject1 = "tag_id";
          localObject3 = Long.valueOf(paramLong1);
          ((ContentValues)localObject2).put((String)localObject1, (Long)localObject3);
          localObject1 = "tag_id_2";
          localObject3 = Long.valueOf(paramLong2);
          ((ContentValues)localObject2).put((String)localObject1, (Long)localObject3);
          if (paramInt1 > 0)
          {
            localObject1 = "context";
            localObject3 = Integer.valueOf(paramInt1);
            ((ContentValues)localObject2).put((String)localObject1, (Integer)localObject3);
          }
          if (paramInt2 > 0)
          {
            localObject1 = "search_type";
            localObject3 = Integer.valueOf(paramInt2);
            ((ContentValues)localObject2).put((String)localObject1, (Integer)localObject3);
          }
          localObject1 = "timestamp";
          long l = System.currentTimeMillis();
          localObject3 = Long.valueOf(l);
          ((ContentValues)localObject2).put((String)localObject1, (Long)localObject3);
          localObject1 = "user_tags";
          localObject3 = null;
          paramContext.insert((String)localObject1, null, (ContentValues)localObject2);
        }
        paramContext.setTransactionSuccessful();
        paramContext.endTransaction();
        TagsUploadWorker.b();
        return;
      }
      finally
      {
        paramContext.endTransaction();
      }
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      com.truecaller.log.d.a(localSQLiteException;
    }
  }
  
  /* Error */
  public static void a(Context paramContext, List paramList, String paramString, int paramInt)
  {
    // Byte code:
    //   0: aload_2
    //   1: invokestatic 334	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   4: istore 4
    //   6: iload 4
    //   8: ifeq +4 -> 12
    //   11: return
    //   12: aload_0
    //   13: invokevirtual 218	android/content/Context:getApplicationContext	()Landroid/content/Context;
    //   16: checkcast 220	com/truecaller/common/b/a
    //   19: invokevirtual 224	com/truecaller/common/b/a:u	()Lcom/truecaller/common/a;
    //   22: invokeinterface 230 1 0
    //   27: astore 5
    //   29: aload 5
    //   31: invokeinterface 234 1 0
    //   36: istore 6
    //   38: iconst_0
    //   39: istore 7
    //   41: aload_0
    //   42: invokestatic 130	com/truecaller/common/tag/a/d:a	(Landroid/content/Context;)Lcom/truecaller/common/c/b/a;
    //   45: astore_0
    //   46: aload_0
    //   47: invokevirtual 237	com/truecaller/common/c/b/a:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   50: astore_0
    //   51: aload_0
    //   52: invokevirtual 240	android/database/sqlite/SQLiteDatabase:beginTransaction	()V
    //   55: aload_1
    //   56: invokeinterface 287 1 0
    //   61: astore_1
    //   62: iconst_0
    //   63: istore 8
    //   65: aload_1
    //   66: invokeinterface 292 1 0
    //   71: istore 9
    //   73: iload 9
    //   75: ifeq +170 -> 245
    //   78: aload_1
    //   79: invokeinterface 296 1 0
    //   84: astore 10
    //   86: aload 10
    //   88: checkcast 146	java/lang/String
    //   91: astore 10
    //   93: ldc_w 258
    //   96: astore 11
    //   98: ldc_w 260
    //   101: astore 12
    //   103: iconst_1
    //   104: istore 13
    //   106: iload 13
    //   108: anewarray 146	java/lang/String
    //   111: astore 14
    //   113: aload 14
    //   115: iconst_0
    //   116: aload 10
    //   118: aastore
    //   119: aload_0
    //   120: aload 11
    //   122: aload 12
    //   124: aload 14
    //   126: invokevirtual 307	android/database/sqlite/SQLiteDatabase:delete	(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    //   129: pop
    //   130: aload 5
    //   132: iload 6
    //   134: aload 10
    //   136: iload_3
    //   137: invokestatic 243	com/truecaller/common/tag/d:a	(Lcom/truecaller/common/h/ac;ZLjava/lang/String;I)Z
    //   140: istore 15
    //   142: iload 15
    //   144: ifeq -79 -> 65
    //   147: new 245	android/content/ContentValues
    //   150: astore 11
    //   152: aload 11
    //   154: invokespecial 246	android/content/ContentValues:<init>	()V
    //   157: ldc_w 266
    //   160: astore 12
    //   162: aload 11
    //   164: aload 12
    //   166: aload 10
    //   168: invokevirtual 269	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   171: ldc -114
    //   173: astore 10
    //   175: aload 11
    //   177: aload 10
    //   179: aload_2
    //   180: invokevirtual 269	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   183: ldc -8
    //   185: astore 10
    //   187: iload_3
    //   188: invokestatic 253	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   191: astore 12
    //   193: aload 11
    //   195: aload 10
    //   197: aload 12
    //   199: invokevirtual 256	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   202: ldc_w 258
    //   205: astore 10
    //   207: aconst_null
    //   208: astore 12
    //   210: aload_0
    //   211: aload 10
    //   213: aconst_null
    //   214: aload 11
    //   216: invokevirtual 273	android/database/sqlite/SQLiteDatabase:insert	(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    //   219: lstore 16
    //   221: lconst_0
    //   222: lstore 18
    //   224: lload 16
    //   226: lload 18
    //   228: lcmp
    //   229: istore 20
    //   231: iload 20
    //   233: iflt -168 -> 65
    //   236: iload 8
    //   238: iconst_1
    //   239: iadd
    //   240: istore 8
    //   242: goto -177 -> 65
    //   245: aload_0
    //   246: invokevirtual 276	android/database/sqlite/SQLiteDatabase:setTransactionSuccessful	()V
    //   249: aload_0
    //   250: invokevirtual 279	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   253: goto +38 -> 291
    //   256: astore_0
    //   257: goto +30 -> 287
    //   260: astore_1
    //   261: iload 8
    //   263: istore 7
    //   265: goto +4 -> 269
    //   268: astore_1
    //   269: aload_0
    //   270: invokevirtual 279	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   273: aload_1
    //   274: athrow
    //   275: astore_0
    //   276: iload 7
    //   278: istore 8
    //   280: goto +7 -> 287
    //   283: astore_0
    //   284: iconst_0
    //   285: istore 8
    //   287: aload_0
    //   288: invokestatic 202	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   291: iload 8
    //   293: ifle +6 -> 299
    //   296: invokestatic 283	com/truecaller/common/tag/sync/TagsUploadWorker:b	()V
    //   299: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	300	0	paramContext	Context
    //   0	300	1	paramList	List
    //   0	300	2	paramString	String
    //   0	300	3	paramInt	int
    //   4	3	4	bool1	boolean
    //   27	104	5	localac	ac
    //   36	97	6	bool2	boolean
    //   39	238	7	i	int
    //   63	229	8	j	int
    //   71	3	9	bool3	boolean
    //   84	128	10	localObject1	Object
    //   96	119	11	localObject2	Object
    //   101	108	12	localObject3	Object
    //   104	3	13	k	int
    //   111	14	14	arrayOfString	String[]
    //   140	3	15	bool4	boolean
    //   219	6	16	l1	long
    //   222	5	18	l2	long
    //   229	3	20	bool5	boolean
    // Exception table:
    //   from	to	target	type
    //   249	253	256	android/database/sqlite/SQLiteException
    //   65	71	260	finally
    //   78	84	260	finally
    //   86	91	260	finally
    //   106	111	260	finally
    //   116	119	260	finally
    //   124	130	260	finally
    //   136	140	260	finally
    //   147	150	260	finally
    //   152	157	260	finally
    //   166	171	260	finally
    //   179	183	260	finally
    //   187	191	260	finally
    //   197	202	260	finally
    //   214	219	260	finally
    //   245	249	260	finally
    //   55	61	268	finally
    //   269	273	275	android/database/sqlite/SQLiteException
    //   273	275	275	android/database/sqlite/SQLiteException
    //   41	45	283	android/database/sqlite/SQLiteException
    //   46	50	283	android/database/sqlite/SQLiteException
    //   51	55	283	android/database/sqlite/SQLiteException
  }
  
  public static boolean a()
  {
    Map localMap = a;
    boolean bool = localMap.isEmpty();
    return !bool;
  }
  
  private static boolean a(ac paramac, boolean paramBoolean, String paramString, int paramInt)
  {
    int i = 2;
    if (paramInt != i) {
      if (!paramBoolean)
      {
        boolean bool = paramac.a(paramString);
        if (!bool) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  /* Error */
  public static boolean b(Context paramContext)
  {
    // Byte code:
    //   0: ldc_w 341
    //   3: astore_1
    //   4: aload_1
    //   5: invokestatic 346	com/truecaller/common/b/e:a	(Ljava/lang/String;)Ljava/lang/String;
    //   8: astore_1
    //   9: invokestatic 351	com/truecaller/common/tag/network/b:a	()Lcom/truecaller/common/tag/network/b$a;
    //   12: astore_2
    //   13: aload_2
    //   14: aload_1
    //   15: invokeinterface 356 2 0
    //   20: astore_1
    //   21: aload_1
    //   22: invokeinterface 362 1 0
    //   27: astore_1
    //   28: aload_1
    //   29: getfield 367	e/r:a	Lokhttp3/ad;
    //   32: astore_2
    //   33: aload_2
    //   34: getfield 371	okhttp3/ad:c	I
    //   37: istore_3
    //   38: sipush 304
    //   41: istore 4
    //   43: iconst_1
    //   44: istore 5
    //   46: iload_3
    //   47: iload 4
    //   49: if_icmpne +6 -> 55
    //   52: iload 5
    //   54: ireturn
    //   55: aload_1
    //   56: getfield 367	e/r:a	Lokhttp3/ad;
    //   59: astore_2
    //   60: aload_2
    //   61: invokevirtual 374	okhttp3/ad:c	()Z
    //   64: istore_3
    //   65: iload_3
    //   66: ifeq +271 -> 337
    //   69: aload_1
    //   70: getfield 377	e/r:b	Ljava/lang/Object;
    //   73: astore_2
    //   74: aload_2
    //   75: checkcast 379	com/truecaller/common/tag/network/TagRestModel$TagsResponse
    //   78: astore_2
    //   79: aload_2
    //   80: ifnull +257 -> 337
    //   83: aload_2
    //   84: getfield 383	com/truecaller/common/tag/network/TagRestModel$TagsResponse:data	Ljava/util/List;
    //   87: astore 6
    //   89: aload 6
    //   91: ifnonnull +6 -> 97
    //   94: goto +243 -> 337
    //   97: aload_0
    //   98: invokestatic 130	com/truecaller/common/tag/a/d:a	(Landroid/content/Context;)Lcom/truecaller/common/c/b/a;
    //   101: astore 6
    //   103: aload 6
    //   105: invokevirtual 237	com/truecaller/common/c/b/a:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   108: astore 6
    //   110: aload 6
    //   112: invokevirtual 240	android/database/sqlite/SQLiteDatabase:beginTransaction	()V
    //   115: ldc -118
    //   117: astore 7
    //   119: aload 6
    //   121: aload 7
    //   123: aconst_null
    //   124: aconst_null
    //   125: invokevirtual 307	android/database/sqlite/SQLiteDatabase:delete	(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    //   128: pop
    //   129: aload_2
    //   130: getfield 383	com/truecaller/common/tag/network/TagRestModel$TagsResponse:data	Ljava/util/List;
    //   133: astore_2
    //   134: aload_2
    //   135: invokeinterface 287 1 0
    //   140: astore_2
    //   141: aload_2
    //   142: invokeinterface 292 1 0
    //   147: istore 8
    //   149: iload 8
    //   151: ifeq +120 -> 271
    //   154: aload_2
    //   155: invokeinterface 296 1 0
    //   160: astore 7
    //   162: aload 7
    //   164: checkcast 385	com/truecaller/common/tag/network/TagRestModel$Tag
    //   167: astore 7
    //   169: new 245	android/content/ContentValues
    //   172: astore 9
    //   174: aload 9
    //   176: invokespecial 246	android/content/ContentValues:<init>	()V
    //   179: ldc -116
    //   181: astore 10
    //   183: aload 7
    //   185: getfield 388	com/truecaller/common/tag/network/TagRestModel$Tag:id	I
    //   188: istore 11
    //   190: iload 11
    //   192: invokestatic 253	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   195: astore 12
    //   197: aload 9
    //   199: aload 10
    //   201: aload 12
    //   203: invokevirtual 256	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   206: ldc -114
    //   208: astore 10
    //   210: aload 7
    //   212: getfield 391	com/truecaller/common/tag/network/TagRestModel$Tag:name	Ljava/lang/String;
    //   215: astore 12
    //   217: aload 9
    //   219: aload 10
    //   221: aload 12
    //   223: invokevirtual 269	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   226: ldc -112
    //   228: astore 10
    //   230: aload 7
    //   232: getfield 394	com/truecaller/common/tag/network/TagRestModel$Tag:parentId	I
    //   235: istore 8
    //   237: iload 8
    //   239: invokestatic 253	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   242: astore 7
    //   244: aload 9
    //   246: aload 10
    //   248: aload 7
    //   250: invokevirtual 256	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   253: ldc -118
    //   255: astore 7
    //   257: aload 6
    //   259: aload 7
    //   261: aconst_null
    //   262: aload 9
    //   264: invokevirtual 397	android/database/sqlite/SQLiteDatabase:replace	(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    //   267: pop2
    //   268: goto -127 -> 141
    //   271: aload 6
    //   273: invokevirtual 276	android/database/sqlite/SQLiteDatabase:setTransactionSuccessful	()V
    //   276: aload 6
    //   278: invokevirtual 279	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   281: aload_0
    //   282: invokestatic 400	com/truecaller/common/tag/d:a	(Landroid/content/Context;)V
    //   285: ldc_w 402
    //   288: astore_0
    //   289: aload_0
    //   290: iload 5
    //   292: invokestatic 405	com/truecaller/common/b/e:b	(Ljava/lang/String;Z)V
    //   295: ldc_w 341
    //   298: astore_0
    //   299: aload_1
    //   300: getfield 367	e/r:a	Lokhttp3/ad;
    //   303: astore_1
    //   304: aload_1
    //   305: getfield 409	okhttp3/ad:f	Lokhttp3/t;
    //   308: astore_1
    //   309: ldc_w 411
    //   312: astore_2
    //   313: aload_1
    //   314: aload_2
    //   315: invokevirtual 414	okhttp3/t:a	(Ljava/lang/String;)Ljava/lang/String;
    //   318: astore_1
    //   319: aload_0
    //   320: aload_1
    //   321: invokestatic 416	com/truecaller/common/b/e:b	(Ljava/lang/String;Ljava/lang/String;)V
    //   324: iload 5
    //   326: ireturn
    //   327: astore_0
    //   328: aload 6
    //   330: invokevirtual 279	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   333: aload_0
    //   334: athrow
    //   335: iconst_0
    //   336: ireturn
    //   337: iconst_0
    //   338: ireturn
    //   339: astore_0
    //   340: goto +4 -> 344
    //   343: astore_0
    //   344: aload_0
    //   345: invokestatic 202	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   348: iconst_0
    //   349: ireturn
    //   350: pop
    //   351: goto -16 -> 335
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	354	0	paramContext	Context
    //   3	318	1	localObject1	Object
    //   12	303	2	localObject2	Object
    //   37	13	3	i	int
    //   64	2	3	bool1	boolean
    //   41	9	4	j	int
    //   44	281	5	bool2	boolean
    //   87	242	6	localObject3	Object
    //   117	143	7	localObject4	Object
    //   147	3	8	bool3	boolean
    //   235	3	8	k	int
    //   172	91	9	localContentValues	ContentValues
    //   181	66	10	str	String
    //   188	3	11	m	int
    //   195	27	12	localObject5	Object
    //   350	1	15	localRuntimeException	RuntimeException
    // Exception table:
    //   from	to	target	type
    //   124	129	327	finally
    //   129	133	327	finally
    //   134	140	327	finally
    //   141	147	327	finally
    //   154	160	327	finally
    //   162	167	327	finally
    //   169	172	327	finally
    //   174	179	327	finally
    //   183	188	327	finally
    //   190	195	327	finally
    //   201	206	327	finally
    //   210	215	327	finally
    //   221	226	327	finally
    //   230	235	327	finally
    //   237	242	327	finally
    //   248	253	327	finally
    //   262	268	327	finally
    //   271	276	327	finally
    //   4	8	339	java/lang/RuntimeException
    //   9	12	339	java/lang/RuntimeException
    //   14	20	339	java/lang/RuntimeException
    //   21	27	339	java/lang/RuntimeException
    //   28	32	339	java/lang/RuntimeException
    //   33	37	339	java/lang/RuntimeException
    //   55	59	339	java/lang/RuntimeException
    //   60	64	339	java/lang/RuntimeException
    //   69	73	339	java/lang/RuntimeException
    //   74	78	339	java/lang/RuntimeException
    //   83	87	339	java/lang/RuntimeException
    //   97	101	339	java/lang/RuntimeException
    //   103	108	339	java/lang/RuntimeException
    //   281	285	339	java/lang/RuntimeException
    //   290	295	339	java/lang/RuntimeException
    //   299	303	339	java/lang/RuntimeException
    //   304	308	339	java/lang/RuntimeException
    //   314	318	339	java/lang/RuntimeException
    //   320	324	339	java/lang/RuntimeException
    //   4	8	343	java/io/IOException
    //   9	12	343	java/io/IOException
    //   14	20	343	java/io/IOException
    //   21	27	343	java/io/IOException
    //   28	32	343	java/io/IOException
    //   33	37	343	java/io/IOException
    //   55	59	343	java/io/IOException
    //   60	64	343	java/io/IOException
    //   69	73	343	java/io/IOException
    //   74	78	343	java/io/IOException
    //   83	87	343	java/io/IOException
    //   97	101	343	java/io/IOException
    //   103	108	343	java/io/IOException
    //   110	115	343	java/io/IOException
    //   276	281	343	java/io/IOException
    //   281	285	343	java/io/IOException
    //   290	295	343	java/io/IOException
    //   299	303	343	java/io/IOException
    //   304	308	343	java/io/IOException
    //   314	318	343	java/io/IOException
    //   320	324	343	java/io/IOException
    //   328	333	343	java/io/IOException
    //   333	335	343	java/io/IOException
    //   110	115	350	java/lang/RuntimeException
    //   276	281	350	java/lang/RuntimeException
    //   328	333	350	java/lang/RuntimeException
    //   333	335	350	java/lang/RuntimeException
  }
  
  /* Error */
  public static boolean c(Context paramContext)
  {
    // Byte code:
    //   0: ldc_w 422
    //   3: astore_1
    //   4: aload_1
    //   5: invokestatic 346	com/truecaller/common/b/e:a	(Ljava/lang/String;)Ljava/lang/String;
    //   8: astore_1
    //   9: invokestatic 351	com/truecaller/common/tag/network/b:a	()Lcom/truecaller/common/tag/network/b$a;
    //   12: astore_2
    //   13: aload_2
    //   14: aload_1
    //   15: invokeinterface 424 2 0
    //   20: astore_1
    //   21: aload_1
    //   22: invokeinterface 362 1 0
    //   27: astore_1
    //   28: aload_1
    //   29: getfield 367	e/r:a	Lokhttp3/ad;
    //   32: astore_2
    //   33: aload_2
    //   34: getfield 371	okhttp3/ad:c	I
    //   37: istore_3
    //   38: sipush 304
    //   41: istore 4
    //   43: iconst_1
    //   44: istore 5
    //   46: iload_3
    //   47: iload 4
    //   49: if_icmpne +6 -> 55
    //   52: iload 5
    //   54: ireturn
    //   55: aload_1
    //   56: getfield 367	e/r:a	Lokhttp3/ad;
    //   59: astore_2
    //   60: aload_2
    //   61: invokevirtual 374	okhttp3/ad:c	()Z
    //   64: istore_3
    //   65: iload_3
    //   66: ifeq +356 -> 422
    //   69: aload_1
    //   70: getfield 377	e/r:b	Ljava/lang/Object;
    //   73: astore_2
    //   74: aload_2
    //   75: checkcast 426	com/truecaller/common/tag/network/TagRestModel$KeywordsResponse
    //   78: astore_2
    //   79: aload_2
    //   80: ifnull +342 -> 422
    //   83: aload_2
    //   84: getfield 427	com/truecaller/common/tag/network/TagRestModel$KeywordsResponse:data	Ljava/util/List;
    //   87: astore 6
    //   89: aload 6
    //   91: ifnonnull +6 -> 97
    //   94: goto +328 -> 422
    //   97: aload_0
    //   98: invokestatic 130	com/truecaller/common/tag/a/d:a	(Landroid/content/Context;)Lcom/truecaller/common/c/b/a;
    //   101: astore_0
    //   102: aload_0
    //   103: invokevirtual 237	com/truecaller/common/c/b/a:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   106: astore_0
    //   107: aload_0
    //   108: invokevirtual 240	android/database/sqlite/SQLiteDatabase:beginTransaction	()V
    //   111: ldc_w 429
    //   114: astore 6
    //   116: aload_0
    //   117: aload 6
    //   119: aconst_null
    //   120: aconst_null
    //   121: invokevirtual 307	android/database/sqlite/SQLiteDatabase:delete	(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    //   124: pop
    //   125: aload_2
    //   126: getfield 427	com/truecaller/common/tag/network/TagRestModel$KeywordsResponse:data	Ljava/util/List;
    //   129: astore 6
    //   131: aload 6
    //   133: invokeinterface 287 1 0
    //   138: astore 6
    //   140: aload 6
    //   142: invokeinterface 292 1 0
    //   147: istore 7
    //   149: iload 7
    //   151: ifeq +201 -> 352
    //   154: aload 6
    //   156: invokeinterface 296 1 0
    //   161: astore 8
    //   163: aload 8
    //   165: checkcast 431	com/truecaller/common/tag/network/TagRestModel$TagKeywords
    //   168: astore 8
    //   170: aload 8
    //   172: getfield 434	com/truecaller/common/tag/network/TagRestModel$TagKeywords:keywords	Ljava/util/List;
    //   175: astore 9
    //   177: aload 9
    //   179: invokeinterface 287 1 0
    //   184: astore 9
    //   186: aload 9
    //   188: invokeinterface 292 1 0
    //   193: istore 10
    //   195: iload 10
    //   197: ifeq -57 -> 140
    //   200: aload 9
    //   202: invokeinterface 296 1 0
    //   207: astore 11
    //   209: aload 11
    //   211: checkcast 436	com/truecaller/common/tag/network/TagRestModel$Keyword
    //   214: astore 11
    //   216: aload 11
    //   218: getfield 439	com/truecaller/common/tag/network/TagRestModel$Keyword:term	Ljava/lang/String;
    //   221: astore 12
    //   223: aload 12
    //   225: invokestatic 334	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   228: istore 13
    //   230: iload 13
    //   232: ifne -46 -> 186
    //   235: new 245	android/content/ContentValues
    //   238: astore 12
    //   240: iconst_4
    //   241: istore 14
    //   243: aload 12
    //   245: iload 14
    //   247: invokespecial 441	android/content/ContentValues:<init>	(I)V
    //   250: ldc_w 442
    //   253: astore 15
    //   255: aload 11
    //   257: getfield 439	com/truecaller/common/tag/network/TagRestModel$Keyword:term	Ljava/lang/String;
    //   260: astore 16
    //   262: aload 16
    //   264: invokevirtual 446	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   267: astore 16
    //   269: aload 12
    //   271: aload 15
    //   273: aload 16
    //   275: invokevirtual 269	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   278: ldc_w 448
    //   281: astore 15
    //   283: aload 11
    //   285: getfield 451	com/truecaller/common/tag/network/TagRestModel$Keyword:relevance	D
    //   288: dstore 17
    //   290: dload 17
    //   292: invokestatic 456	java/lang/Double:valueOf	(D)Ljava/lang/Double;
    //   295: astore 11
    //   297: aload 12
    //   299: aload 15
    //   301: aload 11
    //   303: invokevirtual 459	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Double;)V
    //   306: ldc_w 311
    //   309: astore 11
    //   311: aload 8
    //   313: getfield 462	com/truecaller/common/tag/network/TagRestModel$TagKeywords:tagId	I
    //   316: istore 14
    //   318: iload 14
    //   320: invokestatic 253	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   323: astore 15
    //   325: aload 12
    //   327: aload 11
    //   329: aload 15
    //   331: invokevirtual 256	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   334: ldc_w 429
    //   337: astore 11
    //   339: aload_0
    //   340: aload 11
    //   342: aconst_null
    //   343: aload 12
    //   345: invokevirtual 273	android/database/sqlite/SQLiteDatabase:insert	(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    //   348: pop2
    //   349: goto -163 -> 186
    //   352: ldc_w 464
    //   355: astore 6
    //   357: aload_2
    //   358: getfield 467	com/truecaller/common/tag/network/TagRestModel$KeywordsResponse:version	I
    //   361: istore_3
    //   362: iload_3
    //   363: i2l
    //   364: lstore 19
    //   366: aload 6
    //   368: lload 19
    //   370: invokestatic 470	com/truecaller/common/b/e:b	(Ljava/lang/String;J)V
    //   373: aload_0
    //   374: invokevirtual 276	android/database/sqlite/SQLiteDatabase:setTransactionSuccessful	()V
    //   377: aload_0
    //   378: invokevirtual 279	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   381: ldc_w 422
    //   384: astore_0
    //   385: aload_1
    //   386: getfield 367	e/r:a	Lokhttp3/ad;
    //   389: astore_1
    //   390: aload_1
    //   391: getfield 409	okhttp3/ad:f	Lokhttp3/t;
    //   394: astore_1
    //   395: ldc_w 411
    //   398: astore_2
    //   399: aload_1
    //   400: aload_2
    //   401: invokevirtual 414	okhttp3/t:a	(Ljava/lang/String;)Ljava/lang/String;
    //   404: astore_1
    //   405: aload_0
    //   406: aload_1
    //   407: invokestatic 416	com/truecaller/common/b/e:b	(Ljava/lang/String;Ljava/lang/String;)V
    //   410: iload 5
    //   412: ireturn
    //   413: astore_1
    //   414: aload_0
    //   415: invokevirtual 279	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   418: aload_1
    //   419: athrow
    //   420: iconst_0
    //   421: ireturn
    //   422: iconst_0
    //   423: ireturn
    //   424: astore_0
    //   425: goto +4 -> 429
    //   428: astore_0
    //   429: aload_0
    //   430: invokestatic 202	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   433: iconst_0
    //   434: ireturn
    //   435: pop
    //   436: goto -16 -> 420
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	439	0	paramContext	Context
    //   3	404	1	localObject1	Object
    //   413	6	1	localObject2	Object
    //   12	389	2	localObject3	Object
    //   37	13	3	i	int
    //   64	2	3	bool1	boolean
    //   361	2	3	j	int
    //   41	9	4	k	int
    //   44	367	5	bool2	boolean
    //   87	280	6	localObject4	Object
    //   147	3	7	bool3	boolean
    //   161	151	8	localObject5	Object
    //   175	26	9	localObject6	Object
    //   193	3	10	bool4	boolean
    //   207	134	11	localObject7	Object
    //   221	123	12	localObject8	Object
    //   228	3	13	bool5	boolean
    //   241	78	14	m	int
    //   253	77	15	localObject9	Object
    //   260	14	16	str	String
    //   288	3	17	d	double
    //   364	5	19	l	long
    //   435	1	22	localRuntimeException	RuntimeException
    // Exception table:
    //   from	to	target	type
    //   120	125	413	finally
    //   125	129	413	finally
    //   131	138	413	finally
    //   140	147	413	finally
    //   154	161	413	finally
    //   163	168	413	finally
    //   170	175	413	finally
    //   177	184	413	finally
    //   186	193	413	finally
    //   200	207	413	finally
    //   209	214	413	finally
    //   216	221	413	finally
    //   223	228	413	finally
    //   235	238	413	finally
    //   245	250	413	finally
    //   255	260	413	finally
    //   262	267	413	finally
    //   273	278	413	finally
    //   283	288	413	finally
    //   290	295	413	finally
    //   301	306	413	finally
    //   311	316	413	finally
    //   318	323	413	finally
    //   329	334	413	finally
    //   343	349	413	finally
    //   357	361	413	finally
    //   368	373	413	finally
    //   373	377	413	finally
    //   4	8	424	java/lang/RuntimeException
    //   9	12	424	java/lang/RuntimeException
    //   14	20	424	java/lang/RuntimeException
    //   21	27	424	java/lang/RuntimeException
    //   28	32	424	java/lang/RuntimeException
    //   33	37	424	java/lang/RuntimeException
    //   55	59	424	java/lang/RuntimeException
    //   60	64	424	java/lang/RuntimeException
    //   69	73	424	java/lang/RuntimeException
    //   74	78	424	java/lang/RuntimeException
    //   83	87	424	java/lang/RuntimeException
    //   97	101	424	java/lang/RuntimeException
    //   102	106	424	java/lang/RuntimeException
    //   385	389	424	java/lang/RuntimeException
    //   390	394	424	java/lang/RuntimeException
    //   400	404	424	java/lang/RuntimeException
    //   406	410	424	java/lang/RuntimeException
    //   4	8	428	java/io/IOException
    //   9	12	428	java/io/IOException
    //   14	20	428	java/io/IOException
    //   21	27	428	java/io/IOException
    //   28	32	428	java/io/IOException
    //   33	37	428	java/io/IOException
    //   55	59	428	java/io/IOException
    //   60	64	428	java/io/IOException
    //   69	73	428	java/io/IOException
    //   74	78	428	java/io/IOException
    //   83	87	428	java/io/IOException
    //   97	101	428	java/io/IOException
    //   102	106	428	java/io/IOException
    //   107	111	428	java/io/IOException
    //   377	381	428	java/io/IOException
    //   385	389	428	java/io/IOException
    //   390	394	428	java/io/IOException
    //   400	404	428	java/io/IOException
    //   406	410	428	java/io/IOException
    //   414	418	428	java/io/IOException
    //   418	420	428	java/io/IOException
    //   107	111	435	java/lang/RuntimeException
    //   377	381	435	java/lang/RuntimeException
    //   414	418	435	java/lang/RuntimeException
    //   418	420	435	java/lang/RuntimeException
  }
  
  /* Error */
  public static boolean d(Context paramContext)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aload_0
    //   3: invokestatic 130	com/truecaller/common/tag/a/d:a	(Landroid/content/Context;)Lcom/truecaller/common/c/b/a;
    //   6: astore_0
    //   7: aload_0
    //   8: invokevirtual 237	com/truecaller/common/c/b/a:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   11: astore_0
    //   12: ldc_w 301
    //   15: astore_2
    //   16: ldc -116
    //   18: astore_3
    //   19: ldc_w 309
    //   22: astore 4
    //   24: ldc_w 311
    //   27: astore 5
    //   29: ldc_w 316
    //   32: astore 6
    //   34: ldc_w 318
    //   37: astore 7
    //   39: ldc_w 320
    //   42: astore 8
    //   44: ldc_w 322
    //   47: astore 9
    //   49: bipush 7
    //   51: anewarray 146	java/lang/String
    //   54: dup
    //   55: dup2
    //   56: iconst_0
    //   57: aload_3
    //   58: aastore
    //   59: iconst_1
    //   60: aload 4
    //   62: aastore
    //   63: dup2
    //   64: iconst_2
    //   65: aload 5
    //   67: aastore
    //   68: iconst_3
    //   69: aload 6
    //   71: aastore
    //   72: dup2
    //   73: iconst_4
    //   74: aload 7
    //   76: aastore
    //   77: iconst_5
    //   78: aload 8
    //   80: aastore
    //   81: bipush 6
    //   83: aload 9
    //   85: aastore
    //   86: astore_3
    //   87: iconst_0
    //   88: istore 10
    //   90: aconst_null
    //   91: astore 4
    //   93: iconst_0
    //   94: istore 11
    //   96: aconst_null
    //   97: astore 5
    //   99: iconst_0
    //   100: istore 12
    //   102: aconst_null
    //   103: astore 6
    //   105: iconst_0
    //   106: istore 13
    //   108: aconst_null
    //   109: astore 7
    //   111: iconst_0
    //   112: istore 14
    //   114: aconst_null
    //   115: astore 8
    //   117: aload_0
    //   118: astore 15
    //   120: aload_0
    //   121: aload_2
    //   122: aload_3
    //   123: aconst_null
    //   124: aconst_null
    //   125: aconst_null
    //   126: aconst_null
    //   127: aconst_null
    //   128: invokevirtual 160	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   131: astore_1
    //   132: aload_1
    //   133: ifnull +536 -> 669
    //   136: new 162	java/util/ArrayList
    //   139: astore 15
    //   141: aload_1
    //   142: invokeinterface 474 1 0
    //   147: istore 16
    //   149: aload 15
    //   151: iload 16
    //   153: invokespecial 166	java/util/ArrayList:<init>	(I)V
    //   156: new 162	java/util/ArrayList
    //   159: astore_2
    //   160: aload_1
    //   161: invokeinterface 474 1 0
    //   166: istore 17
    //   168: aload_2
    //   169: iload 17
    //   171: invokespecial 166	java/util/ArrayList:<init>	(I)V
    //   174: aload_1
    //   175: invokeinterface 172 1 0
    //   180: istore 17
    //   182: iconst_1
    //   183: istore 10
    //   185: iload 17
    //   187: ifeq +285 -> 472
    //   190: aload_1
    //   191: iload 10
    //   193: invokeinterface 180 2 0
    //   198: astore_3
    //   199: iconst_2
    //   200: istore 10
    //   202: aload_1
    //   203: iload 10
    //   205: invokeinterface 478 2 0
    //   210: istore 11
    //   212: iconst_3
    //   213: istore 12
    //   215: aload_1
    //   216: iload 12
    //   218: invokeinterface 478 2 0
    //   223: istore 12
    //   225: iconst_4
    //   226: istore 13
    //   228: aload_1
    //   229: iload 13
    //   231: invokeinterface 478 2 0
    //   236: istore 13
    //   238: iconst_5
    //   239: istore 14
    //   241: aload_1
    //   242: iload 14
    //   244: invokeinterface 478 2 0
    //   249: istore 14
    //   251: bipush 6
    //   253: istore 18
    //   255: aload_1
    //   256: iload 18
    //   258: invokeinterface 176 2 0
    //   263: lstore 19
    //   265: new 162	java/util/ArrayList
    //   268: astore 21
    //   270: aload 21
    //   272: iload 10
    //   274: invokespecial 166	java/util/ArrayList:<init>	(I)V
    //   277: iload 12
    //   279: ifle +20 -> 299
    //   282: iload 12
    //   284: invokestatic 253	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   287: astore 4
    //   289: aload 21
    //   291: aload 4
    //   293: invokeinterface 194 2 0
    //   298: pop
    //   299: iload 11
    //   301: ifle +20 -> 321
    //   304: iload 11
    //   306: invokestatic 253	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   309: astore 4
    //   311: aload 21
    //   313: aload 4
    //   315: invokeinterface 194 2 0
    //   320: pop
    //   321: ldc_w 483
    //   324: astore 4
    //   326: ldc_w 485
    //   329: astore 5
    //   331: aload_3
    //   332: aload 4
    //   334: aload 5
    //   336: invokevirtual 489	java/lang/String:replaceFirst	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   339: astore_3
    //   340: new 491	com/truecaller/common/tag/network/TagRestModel$SetTagsRequest
    //   343: astore 4
    //   345: aload 4
    //   347: invokespecial 492	com/truecaller/common/tag/network/TagRestModel$SetTagsRequest:<init>	()V
    //   350: aload_3
    //   351: invokestatic 495	java/lang/Long:valueOf	(Ljava/lang/String;)Ljava/lang/Long;
    //   354: astore_3
    //   355: aload_3
    //   356: invokevirtual 498	java/lang/Long:longValue	()J
    //   359: lstore 22
    //   361: aload 4
    //   363: lload 22
    //   365: putfield 502	com/truecaller/common/tag/network/TagRestModel$SetTagsRequest:phoneNumber	J
    //   368: aload 4
    //   370: aload 21
    //   372: putfield 505	com/truecaller/common/tag/network/TagRestModel$SetTagsRequest:tagIds	Ljava/util/List;
    //   375: iload 13
    //   377: ifle +15 -> 392
    //   380: iload 13
    //   382: invokestatic 253	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   385: astore_3
    //   386: aload 4
    //   388: aload_3
    //   389: putfield 508	com/truecaller/common/tag/network/TagRestModel$SetTagsRequest:context	Ljava/lang/Integer;
    //   392: iload 14
    //   394: ifle +15 -> 409
    //   397: iload 14
    //   399: invokestatic 253	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   402: astore_3
    //   403: aload 4
    //   405: aload_3
    //   406: putfield 511	com/truecaller/common/tag/network/TagRestModel$SetTagsRequest:searchType	Ljava/lang/Integer;
    //   409: lconst_0
    //   410: lstore 22
    //   412: lload 19
    //   414: lload 22
    //   416: lcmp
    //   417: istore 17
    //   419: iload 17
    //   421: ifle +15 -> 436
    //   424: lload 19
    //   426: invokestatic 28	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   429: astore_3
    //   430: aload 4
    //   432: aload_3
    //   433: putfield 514	com/truecaller/common/tag/network/TagRestModel$SetTagsRequest:timestamp	Ljava/lang/Long;
    //   436: aload 15
    //   438: aload 4
    //   440: invokeinterface 194 2 0
    //   445: pop
    //   446: aload_1
    //   447: iconst_0
    //   448: invokeinterface 176 2 0
    //   453: lstore 24
    //   455: lload 24
    //   457: invokestatic 152	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   460: astore_3
    //   461: aload_2
    //   462: aload_3
    //   463: invokeinterface 194 2 0
    //   468: pop
    //   469: goto -295 -> 174
    //   472: aload 15
    //   474: invokeinterface 515 1 0
    //   479: istore 17
    //   481: iload 17
    //   483: ifeq +16 -> 499
    //   486: aload_1
    //   487: ifnull +9 -> 496
    //   490: aload_1
    //   491: invokeinterface 197 1 0
    //   496: iload 10
    //   498: ireturn
    //   499: invokestatic 351	com/truecaller/common/tag/network/b:a	()Lcom/truecaller/common/tag/network/b$a;
    //   502: astore_3
    //   503: aload_3
    //   504: aload 15
    //   506: invokeinterface 518 2 0
    //   511: astore 15
    //   513: aload 15
    //   515: invokeinterface 362 1 0
    //   520: astore 15
    //   522: aload 15
    //   524: getfield 367	e/r:a	Lokhttp3/ad;
    //   527: astore 15
    //   529: aload 15
    //   531: invokevirtual 374	okhttp3/ad:c	()Z
    //   534: istore 26
    //   536: iload 26
    //   538: ifne +15 -> 553
    //   541: aload_1
    //   542: ifnull +9 -> 551
    //   545: aload_1
    //   546: invokeinterface 197 1 0
    //   551: iconst_0
    //   552: ireturn
    //   553: ldc_w 301
    //   556: astore 15
    //   558: ldc_w 520
    //   561: astore_3
    //   562: iload 10
    //   564: anewarray 4	java/lang/Object
    //   567: astore 5
    //   569: ldc_w 522
    //   572: astore 6
    //   574: aload_2
    //   575: invokeinterface 525 1 0
    //   580: istore 13
    //   582: ldc_w 527
    //   585: astore 8
    //   587: iload 13
    //   589: aload 8
    //   591: invokestatic 533	java/util/Collections:nCopies	(ILjava/lang/Object;)Ljava/util/List;
    //   594: astore 7
    //   596: aload 6
    //   598: aload 7
    //   600: invokestatic 537	android/text/TextUtils:join	(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;
    //   603: astore 6
    //   605: aload 5
    //   607: iconst_0
    //   608: aload 6
    //   610: aastore
    //   611: aload_3
    //   612: aload 5
    //   614: invokestatic 541	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   617: astore_3
    //   618: aload_2
    //   619: invokeinterface 525 1 0
    //   624: istore 11
    //   626: iload 11
    //   628: anewarray 146	java/lang/String
    //   631: astore 5
    //   633: aload_2
    //   634: aload 5
    //   636: invokeinterface 545 2 0
    //   641: astore_2
    //   642: aload_2
    //   643: checkcast 547	[Ljava/lang/String;
    //   646: astore_2
    //   647: aload_0
    //   648: aload 15
    //   650: aload_3
    //   651: aload_2
    //   652: invokevirtual 307	android/database/sqlite/SQLiteDatabase:delete	(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    //   655: pop
    //   656: aload_1
    //   657: ifnull +9 -> 666
    //   660: aload_1
    //   661: invokeinterface 197 1 0
    //   666: iload 10
    //   668: ireturn
    //   669: aload_1
    //   670: ifnull +29 -> 699
    //   673: goto +20 -> 693
    //   676: astore_0
    //   677: goto +24 -> 701
    //   680: astore_0
    //   681: goto +4 -> 685
    //   684: astore_0
    //   685: aload_0
    //   686: invokestatic 202	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   689: aload_1
    //   690: ifnull +9 -> 699
    //   693: aload_1
    //   694: invokeinterface 197 1 0
    //   699: iconst_0
    //   700: ireturn
    //   701: aload_1
    //   702: ifnull +9 -> 711
    //   705: aload_1
    //   706: invokeinterface 197 1 0
    //   711: aload_0
    //   712: athrow
    //   713: pop
    //   714: goto -540 -> 174
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	717	0	paramContext	Context
    //   1	705	1	localCursor	android.database.Cursor
    //   15	637	2	localObject1	Object
    //   18	633	3	localObject2	Object
    //   22	417	4	localObject3	Object
    //   27	608	5	localObject4	Object
    //   32	577	6	str1	String
    //   37	562	7	localObject5	Object
    //   42	548	8	str2	String
    //   47	37	9	str3	String
    //   88	579	10	i	int
    //   94	533	11	j	int
    //   100	183	12	k	int
    //   106	482	13	m	int
    //   112	286	14	n	int
    //   118	531	15	localObject6	Object
    //   147	5	16	i1	int
    //   166	4	17	i2	int
    //   180	302	17	bool1	boolean
    //   253	4	18	i3	int
    //   263	162	19	l1	long
    //   268	103	21	localArrayList	java.util.ArrayList
    //   359	56	22	l2	long
    //   453	3	24	l3	long
    //   713	1	25	localNumberFormatException	NumberFormatException
    //   534	3	26	bool2	boolean
    // Exception table:
    //   from	to	target	type
    //   2	6	676	finally
    //   7	11	676	finally
    //   49	86	676	finally
    //   127	131	676	finally
    //   136	139	676	finally
    //   141	147	676	finally
    //   151	156	676	finally
    //   156	159	676	finally
    //   160	166	676	finally
    //   169	174	676	finally
    //   174	180	676	finally
    //   191	198	676	finally
    //   203	210	676	finally
    //   216	223	676	finally
    //   229	236	676	finally
    //   242	249	676	finally
    //   256	263	676	finally
    //   265	268	676	finally
    //   272	277	676	finally
    //   282	287	676	finally
    //   291	299	676	finally
    //   304	309	676	finally
    //   313	321	676	finally
    //   334	339	676	finally
    //   340	343	676	finally
    //   345	350	676	finally
    //   350	354	676	finally
    //   355	359	676	finally
    //   363	368	676	finally
    //   370	375	676	finally
    //   380	385	676	finally
    //   388	392	676	finally
    //   397	402	676	finally
    //   405	409	676	finally
    //   424	429	676	finally
    //   432	436	676	finally
    //   438	446	676	finally
    //   447	453	676	finally
    //   455	460	676	finally
    //   462	469	676	finally
    //   472	479	676	finally
    //   499	502	676	finally
    //   504	511	676	finally
    //   513	520	676	finally
    //   522	527	676	finally
    //   529	534	676	finally
    //   562	567	676	finally
    //   574	580	676	finally
    //   589	594	676	finally
    //   598	603	676	finally
    //   608	611	676	finally
    //   612	617	676	finally
    //   618	624	676	finally
    //   626	631	676	finally
    //   634	641	676	finally
    //   642	646	676	finally
    //   651	656	676	finally
    //   685	689	676	finally
    //   2	6	680	java/lang/RuntimeException
    //   7	11	680	java/lang/RuntimeException
    //   49	86	680	java/lang/RuntimeException
    //   127	131	680	java/lang/RuntimeException
    //   136	139	680	java/lang/RuntimeException
    //   141	147	680	java/lang/RuntimeException
    //   151	156	680	java/lang/RuntimeException
    //   156	159	680	java/lang/RuntimeException
    //   160	166	680	java/lang/RuntimeException
    //   169	174	680	java/lang/RuntimeException
    //   174	180	680	java/lang/RuntimeException
    //   191	198	680	java/lang/RuntimeException
    //   203	210	680	java/lang/RuntimeException
    //   216	223	680	java/lang/RuntimeException
    //   229	236	680	java/lang/RuntimeException
    //   242	249	680	java/lang/RuntimeException
    //   256	263	680	java/lang/RuntimeException
    //   265	268	680	java/lang/RuntimeException
    //   272	277	680	java/lang/RuntimeException
    //   282	287	680	java/lang/RuntimeException
    //   291	299	680	java/lang/RuntimeException
    //   304	309	680	java/lang/RuntimeException
    //   313	321	680	java/lang/RuntimeException
    //   334	339	680	java/lang/RuntimeException
    //   340	343	680	java/lang/RuntimeException
    //   345	350	680	java/lang/RuntimeException
    //   350	354	680	java/lang/RuntimeException
    //   355	359	680	java/lang/RuntimeException
    //   363	368	680	java/lang/RuntimeException
    //   370	375	680	java/lang/RuntimeException
    //   380	385	680	java/lang/RuntimeException
    //   388	392	680	java/lang/RuntimeException
    //   397	402	680	java/lang/RuntimeException
    //   405	409	680	java/lang/RuntimeException
    //   424	429	680	java/lang/RuntimeException
    //   432	436	680	java/lang/RuntimeException
    //   438	446	680	java/lang/RuntimeException
    //   447	453	680	java/lang/RuntimeException
    //   455	460	680	java/lang/RuntimeException
    //   462	469	680	java/lang/RuntimeException
    //   472	479	680	java/lang/RuntimeException
    //   499	502	680	java/lang/RuntimeException
    //   504	511	680	java/lang/RuntimeException
    //   513	520	680	java/lang/RuntimeException
    //   522	527	680	java/lang/RuntimeException
    //   529	534	680	java/lang/RuntimeException
    //   562	567	680	java/lang/RuntimeException
    //   574	580	680	java/lang/RuntimeException
    //   589	594	680	java/lang/RuntimeException
    //   598	603	680	java/lang/RuntimeException
    //   608	611	680	java/lang/RuntimeException
    //   612	617	680	java/lang/RuntimeException
    //   618	624	680	java/lang/RuntimeException
    //   626	631	680	java/lang/RuntimeException
    //   634	641	680	java/lang/RuntimeException
    //   642	646	680	java/lang/RuntimeException
    //   651	656	680	java/lang/RuntimeException
    //   2	6	684	java/io/IOException
    //   7	11	684	java/io/IOException
    //   49	86	684	java/io/IOException
    //   127	131	684	java/io/IOException
    //   136	139	684	java/io/IOException
    //   141	147	684	java/io/IOException
    //   151	156	684	java/io/IOException
    //   156	159	684	java/io/IOException
    //   160	166	684	java/io/IOException
    //   169	174	684	java/io/IOException
    //   174	180	684	java/io/IOException
    //   191	198	684	java/io/IOException
    //   203	210	684	java/io/IOException
    //   216	223	684	java/io/IOException
    //   229	236	684	java/io/IOException
    //   242	249	684	java/io/IOException
    //   256	263	684	java/io/IOException
    //   265	268	684	java/io/IOException
    //   272	277	684	java/io/IOException
    //   282	287	684	java/io/IOException
    //   291	299	684	java/io/IOException
    //   304	309	684	java/io/IOException
    //   313	321	684	java/io/IOException
    //   334	339	684	java/io/IOException
    //   340	343	684	java/io/IOException
    //   345	350	684	java/io/IOException
    //   350	354	684	java/io/IOException
    //   355	359	684	java/io/IOException
    //   363	368	684	java/io/IOException
    //   370	375	684	java/io/IOException
    //   380	385	684	java/io/IOException
    //   388	392	684	java/io/IOException
    //   397	402	684	java/io/IOException
    //   405	409	684	java/io/IOException
    //   424	429	684	java/io/IOException
    //   432	436	684	java/io/IOException
    //   438	446	684	java/io/IOException
    //   447	453	684	java/io/IOException
    //   455	460	684	java/io/IOException
    //   462	469	684	java/io/IOException
    //   472	479	684	java/io/IOException
    //   499	502	684	java/io/IOException
    //   504	511	684	java/io/IOException
    //   513	520	684	java/io/IOException
    //   522	527	684	java/io/IOException
    //   529	534	684	java/io/IOException
    //   562	567	684	java/io/IOException
    //   574	580	684	java/io/IOException
    //   589	594	684	java/io/IOException
    //   598	603	684	java/io/IOException
    //   608	611	684	java/io/IOException
    //   612	617	684	java/io/IOException
    //   618	624	684	java/io/IOException
    //   626	631	684	java/io/IOException
    //   634	641	684	java/io/IOException
    //   642	646	684	java/io/IOException
    //   651	656	684	java/io/IOException
    //   350	354	713	java/lang/NumberFormatException
    //   355	359	713	java/lang/NumberFormatException
    //   363	368	713	java/lang/NumberFormatException
  }
  
  /* Error */
  public static boolean e(Context paramContext)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aload_0
    //   3: invokestatic 130	com/truecaller/common/tag/a/d:a	(Landroid/content/Context;)Lcom/truecaller/common/c/b/a;
    //   6: astore_0
    //   7: aload_0
    //   8: invokevirtual 237	com/truecaller/common/c/b/a:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   11: astore_0
    //   12: ldc_w 258
    //   15: astore_2
    //   16: ldc -116
    //   18: astore_3
    //   19: ldc_w 266
    //   22: astore 4
    //   24: ldc -114
    //   26: astore 5
    //   28: ldc -8
    //   30: astore 6
    //   32: iconst_4
    //   33: anewarray 146	java/lang/String
    //   36: dup
    //   37: dup2
    //   38: iconst_0
    //   39: aload_3
    //   40: aastore
    //   41: iconst_1
    //   42: aload 4
    //   44: aastore
    //   45: iconst_2
    //   46: aload 5
    //   48: aastore
    //   49: dup
    //   50: iconst_3
    //   51: aload 6
    //   53: aastore
    //   54: astore 4
    //   56: iconst_0
    //   57: istore 7
    //   59: aconst_null
    //   60: astore 5
    //   62: iconst_0
    //   63: istore 8
    //   65: aconst_null
    //   66: astore 6
    //   68: aconst_null
    //   69: astore 9
    //   71: iconst_0
    //   72: istore 10
    //   74: aconst_null
    //   75: astore 11
    //   77: iconst_0
    //   78: istore 12
    //   80: aconst_null
    //   81: astore 13
    //   83: aload_0
    //   84: astore_3
    //   85: aload_0
    //   86: aload_2
    //   87: aload 4
    //   89: aconst_null
    //   90: aconst_null
    //   91: aconst_null
    //   92: aconst_null
    //   93: aconst_null
    //   94: invokevirtual 160	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   97: astore_1
    //   98: aload_1
    //   99: ifnull +372 -> 471
    //   102: aload_1
    //   103: invokeinterface 474 1 0
    //   108: istore 14
    //   110: iconst_1
    //   111: istore 15
    //   113: iload 14
    //   115: ifne +16 -> 131
    //   118: aload_1
    //   119: ifnull +9 -> 128
    //   122: aload_1
    //   123: invokeinterface 197 1 0
    //   128: iload 15
    //   130: ireturn
    //   131: ldc -116
    //   133: astore_3
    //   134: aload_1
    //   135: aload_3
    //   136: invokeinterface 553 2 0
    //   141: istore 14
    //   143: ldc_w 266
    //   146: astore 4
    //   148: aload_1
    //   149: aload 4
    //   151: invokeinterface 553 2 0
    //   156: istore 16
    //   158: ldc -114
    //   160: astore 5
    //   162: aload_1
    //   163: aload 5
    //   165: invokeinterface 553 2 0
    //   170: istore 7
    //   172: ldc -8
    //   174: astore 6
    //   176: aload_1
    //   177: aload 6
    //   179: invokeinterface 553 2 0
    //   184: istore 8
    //   186: new 162	java/util/ArrayList
    //   189: astore 9
    //   191: aload 9
    //   193: invokespecial 554	java/util/ArrayList:<init>	()V
    //   196: aload_1
    //   197: invokeinterface 172 1 0
    //   202: istore 10
    //   204: iload 10
    //   206: ifeq +77 -> 283
    //   209: new 556	com/truecaller/common/tag/network/NameSuggestionRestModel$NameSuggestion
    //   212: astore 11
    //   214: aload 11
    //   216: invokespecial 557	com/truecaller/common/tag/network/NameSuggestionRestModel$NameSuggestion:<init>	()V
    //   219: aload_1
    //   220: iload 16
    //   222: invokeinterface 180 2 0
    //   227: astore 13
    //   229: aload 11
    //   231: aload 13
    //   233: putfield 559	com/truecaller/common/tag/network/NameSuggestionRestModel$NameSuggestion:phoneNumber	Ljava/lang/String;
    //   236: aload_1
    //   237: iload 7
    //   239: invokeinterface 180 2 0
    //   244: astore 13
    //   246: aload 11
    //   248: aload 13
    //   250: putfield 560	com/truecaller/common/tag/network/NameSuggestionRestModel$NameSuggestion:name	Ljava/lang/String;
    //   253: aload_1
    //   254: iload 8
    //   256: invokeinterface 478 2 0
    //   261: istore 12
    //   263: aload 11
    //   265: iload 12
    //   267: putfield 562	com/truecaller/common/tag/network/NameSuggestionRestModel$NameSuggestion:type	I
    //   270: aload 9
    //   272: aload 11
    //   274: invokeinterface 194 2 0
    //   279: pop
    //   280: goto -84 -> 196
    //   283: getstatic 568	com/truecaller/common/network/util/KnownEndpoints:CONTACT	Lcom/truecaller/common/network/util/KnownEndpoints;
    //   286: astore 4
    //   288: ldc_w 570
    //   291: astore 5
    //   293: aload 4
    //   295: aload 5
    //   297: invokestatic 575	com/truecaller/common/network/util/h:a	(Lcom/truecaller/common/network/util/KnownEndpoints;Ljava/lang/Class;)Ljava/lang/Object;
    //   300: astore 4
    //   302: aload 4
    //   304: checkcast 570	com/truecaller/common/tag/network/a$a
    //   307: astore 4
    //   309: aload 4
    //   311: aload 9
    //   313: invokeinterface 576 2 0
    //   318: astore 4
    //   320: aload 4
    //   322: invokeinterface 362 1 0
    //   327: astore 4
    //   329: aload 4
    //   331: getfield 367	e/r:a	Lokhttp3/ad;
    //   334: astore 4
    //   336: aload 4
    //   338: invokevirtual 374	okhttp3/ad:c	()Z
    //   341: istore 16
    //   343: iload 16
    //   345: ifne +15 -> 360
    //   348: aload_1
    //   349: ifnull +9 -> 358
    //   352: aload_1
    //   353: invokeinterface 197 1 0
    //   358: iconst_0
    //   359: ireturn
    //   360: iconst_m1
    //   361: istore 16
    //   363: aload_1
    //   364: iload 16
    //   366: invokeinterface 580 2 0
    //   371: pop
    //   372: aload_0
    //   373: invokevirtual 240	android/database/sqlite/SQLiteDatabase:beginTransaction	()V
    //   376: aload_1
    //   377: invokeinterface 172 1 0
    //   382: istore 16
    //   384: iload 16
    //   386: ifeq +57 -> 443
    //   389: ldc_w 258
    //   392: astore 4
    //   394: ldc_w 582
    //   397: astore 5
    //   399: iload 15
    //   401: anewarray 146	java/lang/String
    //   404: astore 6
    //   406: aload_1
    //   407: iload 14
    //   409: invokeinterface 176 2 0
    //   414: lstore 17
    //   416: lload 17
    //   418: invokestatic 152	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   421: astore 9
    //   423: aload 6
    //   425: iconst_0
    //   426: aload 9
    //   428: aastore
    //   429: aload_0
    //   430: aload 4
    //   432: aload 5
    //   434: aload 6
    //   436: invokevirtual 307	android/database/sqlite/SQLiteDatabase:delete	(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    //   439: pop
    //   440: goto -64 -> 376
    //   443: aload_0
    //   444: invokevirtual 276	android/database/sqlite/SQLiteDatabase:setTransactionSuccessful	()V
    //   447: aload_0
    //   448: invokevirtual 279	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   451: aload_1
    //   452: ifnull +9 -> 461
    //   455: aload_1
    //   456: invokeinterface 197 1 0
    //   461: iload 15
    //   463: ireturn
    //   464: astore_3
    //   465: aload_0
    //   466: invokevirtual 279	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   469: aload_3
    //   470: athrow
    //   471: aload_1
    //   472: ifnull +29 -> 501
    //   475: goto +20 -> 495
    //   478: astore_0
    //   479: goto +24 -> 503
    //   482: astore_0
    //   483: goto +4 -> 487
    //   486: astore_0
    //   487: aload_0
    //   488: invokestatic 202	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   491: aload_1
    //   492: ifnull +9 -> 501
    //   495: aload_1
    //   496: invokeinterface 197 1 0
    //   501: iconst_0
    //   502: ireturn
    //   503: aload_1
    //   504: ifnull +9 -> 513
    //   507: aload_1
    //   508: invokeinterface 197 1 0
    //   513: aload_0
    //   514: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	515	0	paramContext	Context
    //   1	507	1	localCursor	android.database.Cursor
    //   15	72	2	str1	String
    //   18	118	3	localObject1	Object
    //   464	6	3	localObject2	Object
    //   22	409	4	localObject3	Object
    //   26	407	5	localObject4	Object
    //   30	405	6	localObject5	Object
    //   57	181	7	i	int
    //   63	192	8	j	int
    //   69	358	9	localObject6	Object
    //   72	133	10	bool1	boolean
    //   75	198	11	localNameSuggestion	com.truecaller.common.tag.network.NameSuggestionRestModel.NameSuggestion
    //   78	188	12	k	int
    //   81	168	13	str2	String
    //   108	300	14	m	int
    //   111	351	15	bool2	boolean
    //   156	65	16	n	int
    //   341	3	16	bool3	boolean
    //   361	4	16	i1	int
    //   382	3	16	bool4	boolean
    //   414	3	17	l	long
    // Exception table:
    //   from	to	target	type
    //   376	382	464	finally
    //   399	404	464	finally
    //   407	414	464	finally
    //   416	421	464	finally
    //   426	429	464	finally
    //   434	440	464	finally
    //   443	447	464	finally
    //   2	6	478	finally
    //   7	11	478	finally
    //   32	54	478	finally
    //   93	97	478	finally
    //   102	108	478	finally
    //   135	141	478	finally
    //   149	156	478	finally
    //   163	170	478	finally
    //   177	184	478	finally
    //   186	189	478	finally
    //   191	196	478	finally
    //   196	202	478	finally
    //   209	212	478	finally
    //   214	219	478	finally
    //   220	227	478	finally
    //   231	236	478	finally
    //   237	244	478	finally
    //   248	253	478	finally
    //   254	261	478	finally
    //   265	270	478	finally
    //   272	280	478	finally
    //   283	286	478	finally
    //   295	300	478	finally
    //   302	307	478	finally
    //   311	318	478	finally
    //   320	327	478	finally
    //   329	334	478	finally
    //   336	341	478	finally
    //   364	372	478	finally
    //   372	376	478	finally
    //   447	451	478	finally
    //   465	469	478	finally
    //   469	471	478	finally
    //   487	491	478	finally
    //   2	6	482	java/lang/RuntimeException
    //   7	11	482	java/lang/RuntimeException
    //   32	54	482	java/lang/RuntimeException
    //   93	97	482	java/lang/RuntimeException
    //   102	108	482	java/lang/RuntimeException
    //   135	141	482	java/lang/RuntimeException
    //   149	156	482	java/lang/RuntimeException
    //   163	170	482	java/lang/RuntimeException
    //   177	184	482	java/lang/RuntimeException
    //   186	189	482	java/lang/RuntimeException
    //   191	196	482	java/lang/RuntimeException
    //   196	202	482	java/lang/RuntimeException
    //   209	212	482	java/lang/RuntimeException
    //   214	219	482	java/lang/RuntimeException
    //   220	227	482	java/lang/RuntimeException
    //   231	236	482	java/lang/RuntimeException
    //   237	244	482	java/lang/RuntimeException
    //   248	253	482	java/lang/RuntimeException
    //   254	261	482	java/lang/RuntimeException
    //   265	270	482	java/lang/RuntimeException
    //   272	280	482	java/lang/RuntimeException
    //   283	286	482	java/lang/RuntimeException
    //   295	300	482	java/lang/RuntimeException
    //   302	307	482	java/lang/RuntimeException
    //   311	318	482	java/lang/RuntimeException
    //   320	327	482	java/lang/RuntimeException
    //   329	334	482	java/lang/RuntimeException
    //   336	341	482	java/lang/RuntimeException
    //   364	372	482	java/lang/RuntimeException
    //   372	376	482	java/lang/RuntimeException
    //   447	451	482	java/lang/RuntimeException
    //   465	469	482	java/lang/RuntimeException
    //   469	471	482	java/lang/RuntimeException
    //   2	6	486	java/io/IOException
    //   7	11	486	java/io/IOException
    //   32	54	486	java/io/IOException
    //   93	97	486	java/io/IOException
    //   102	108	486	java/io/IOException
    //   135	141	486	java/io/IOException
    //   149	156	486	java/io/IOException
    //   163	170	486	java/io/IOException
    //   177	184	486	java/io/IOException
    //   186	189	486	java/io/IOException
    //   191	196	486	java/io/IOException
    //   196	202	486	java/io/IOException
    //   209	212	486	java/io/IOException
    //   214	219	486	java/io/IOException
    //   220	227	486	java/io/IOException
    //   231	236	486	java/io/IOException
    //   237	244	486	java/io/IOException
    //   248	253	486	java/io/IOException
    //   254	261	486	java/io/IOException
    //   265	270	486	java/io/IOException
    //   272	280	486	java/io/IOException
    //   283	286	486	java/io/IOException
    //   295	300	486	java/io/IOException
    //   302	307	486	java/io/IOException
    //   311	318	486	java/io/IOException
    //   320	327	486	java/io/IOException
    //   329	334	486	java/io/IOException
    //   336	341	486	java/io/IOException
    //   364	372	486	java/io/IOException
    //   372	376	486	java/io/IOException
    //   447	451	486	java/io/IOException
    //   465	469	486	java/io/IOException
    //   469	471	486	java/io/IOException
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */