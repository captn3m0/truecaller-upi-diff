package com.truecaller.common.tag.network;

import java.util.List;

public class TagRestModel$SetTagsRequest
{
  public Integer context;
  public long phoneNumber;
  public Integer searchType;
  public List tagIds;
  public Long timestamp;
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.network.TagRestModel.SetTagsRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */