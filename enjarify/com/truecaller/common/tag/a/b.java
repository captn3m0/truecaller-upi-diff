package com.truecaller.common.tag.a;

import android.database.sqlite.SQLiteDatabase;
import com.truecaller.common.c.b.c;
import com.truecaller.common.c.b.c.a;

final class b
  extends c
{
  private static final c.a[] c;
  
  static
  {
    c.a[] arrayOfa = new c.a[4];
    c.a locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("_id", "INTEGER PRIMARY KEY");
    arrayOfa[0] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("number", "TEXT", (byte)0);
    arrayOfa[1] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("name", "TEXT");
    arrayOfa[2] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("type", "INTEGER");
    arrayOfa[3] = locala;
    c = arrayOfa;
  }
  
  b()
  {
    super("name_suggestions", arrayOfa);
  }
  
  public final void a(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    paramInt2 = 2;
    if (paramInt1 < paramInt2) {
      a(paramSQLiteDatabase);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */