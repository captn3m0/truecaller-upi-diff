package com.truecaller.common.tag.a;

import android.database.sqlite.SQLiteDatabase;
import com.truecaller.common.c.b.c.a;

public final class c
  extends com.truecaller.common.c.b.c
{
  private static final c.a[] c;
  
  static
  {
    c.a[] arrayOfa = new c.a[4];
    c.a locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("_id", "INTEGER PRIMARY KEY");
    arrayOfa[0] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("term", "TEXT");
    arrayOfa[1] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("tag_id", "INTEGER");
    arrayOfa[2] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("relevance", "REAL");
    arrayOfa[3] = locala;
    c = arrayOfa;
  }
  
  c()
  {
    super("tag_keywords", arrayOfa);
  }
  
  public final void a(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    paramInt2 = 5;
    if (paramInt1 < paramInt2) {
      a(paramSQLiteDatabase);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */