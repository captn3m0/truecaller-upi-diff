package com.truecaller.common.tag.a;

import android.content.Context;

public final class d
{
  private static volatile com.truecaller.common.c.b.a a;
  private static final com.truecaller.common.c.b.c[] b;
  private static final com.truecaller.common.c.b.d[] c = new com.truecaller.common.c.b.d[0];
  
  static
  {
    com.truecaller.common.c.b.c[] arrayOfc = new com.truecaller.common.c.b.c[4];
    Object localObject = new com/truecaller/common/tag/a/a;
    ((a)localObject).<init>();
    arrayOfc[0] = localObject;
    localObject = new com/truecaller/common/tag/a/e;
    ((e)localObject).<init>();
    arrayOfc[1] = localObject;
    localObject = new com/truecaller/common/tag/a/b;
    ((b)localObject).<init>();
    arrayOfc[2] = localObject;
    localObject = new com/truecaller/common/tag/a/c;
    ((c)localObject).<init>();
    arrayOfc[3] = localObject;
    b = arrayOfc;
  }
  
  public static com.truecaller.common.c.b.a a(Context paramContext)
  {
    synchronized (d.class)
    {
      Object localObject = a;
      if (localObject == null)
      {
        localObject = "tags.db";
        int i = 5;
        com.truecaller.common.c.b.c[] arrayOfc = b;
        com.truecaller.common.c.b.d[] arrayOfd = c;
        paramContext = com.truecaller.common.c.b.a.a(paramContext, (String)localObject, i, arrayOfc, arrayOfd);
        a = paramContext;
      }
      paramContext = a;
      return paramContext;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */