package com.truecaller.common.tag.a;

import android.database.sqlite.SQLiteDatabase;
import com.truecaller.common.c.b.c;
import com.truecaller.common.c.b.c.a;

final class e
  extends c
{
  private static final c.a[] c;
  
  static
  {
    c.a[] arrayOfa = new c.a[7];
    c.a locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("_id", "INTEGER PRIMARY KEY");
    arrayOfa[0] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("normalized_number", "TEXT", (byte)0);
    arrayOfa[1] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("tag_id", "INTEGER");
    arrayOfa[2] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("tag_id_2", "INTEGER");
    arrayOfa[3] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("context", "INTEGER");
    arrayOfa[4] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("search_type", "INTEGER");
    arrayOfa[5] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("timestamp", "INTEGER");
    arrayOfa[6] = locala;
    c = arrayOfa;
  }
  
  e()
  {
    super("user_tags", arrayOfa);
  }
  
  public final void a(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    paramInt2 = 3;
    if (paramInt1 < paramInt2)
    {
      String str1 = "ALTER TABLE user_tags ADD COLUMN tag_id_2 INTEGER";
      paramSQLiteDatabase.execSQL(str1);
    }
    paramInt2 = 4;
    if (paramInt1 < paramInt2)
    {
      paramSQLiteDatabase.execSQL("ALTER TABLE user_tags ADD COLUMN context INTEGER");
      paramSQLiteDatabase.execSQL("ALTER TABLE user_tags ADD COLUMN search_type INTEGER");
      String str2 = "ALTER TABLE user_tags ADD COLUMN timestamp INTEGER";
      paramSQLiteDatabase.execSQL(str2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */