package com.truecaller.common.tag.a;

import android.database.sqlite.SQLiteDatabase;
import com.truecaller.common.c.b.c;
import com.truecaller.common.c.b.c.a;

final class a
  extends c
{
  private static final c.a[] c;
  
  static
  {
    c.a[] arrayOfa = new c.a[5];
    c.a locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("_id", "INTEGER PRIMARY KEY");
    arrayOfa[0] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("name", "TEXT");
    arrayOfa[1] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("parent_id", "INTEGER", (byte)0);
    arrayOfa[2] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("color", "INTEGER");
    arrayOfa[3] = locala;
    locala = new com/truecaller/common/c/b/c$a;
    locala.<init>("image", "TEXT");
    arrayOfa[4] = locala;
    c = arrayOfa;
  }
  
  a()
  {
    super("available_tags", arrayOfa);
  }
  
  public final void a(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2) {}
}

/* Location:
 * Qualified Name:     com.truecaller.common.tag.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */