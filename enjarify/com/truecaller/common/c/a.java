package com.truecaller.common.c;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.text.TextUtils;
import com.truecaller.common.c.a.a.a;
import com.truecaller.common.c.a.a.b;
import com.truecaller.common.c.a.a.c;
import com.truecaller.common.c.a.a.d;
import com.truecaller.common.c.a.a.e;
import com.truecaller.common.c.a.a.f;
import com.truecaller.common.c.a.a.g;
import com.truecaller.common.c.a.a.h;
import com.truecaller.common.c.a.e;
import com.truecaller.common.c.b.b;
import java.io.PrintStream;
import java.util.Collection;

public abstract class a
  extends c
{
  private volatile com.truecaller.common.c.a.c b;
  
  private com.truecaller.common.c.a.c e()
  {
    Object localObject = b;
    if (localObject == null) {
      try
      {
        localObject = b;
        if (localObject == null)
        {
          localObject = getContext();
          localObject = b((Context)localObject);
          b = ((com.truecaller.common.c.a.c)localObject);
        }
      }
      finally {}
    }
    return localc;
  }
  
  protected final int a(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    com.truecaller.common.c.a.a locala = e().b(paramUri);
    Object localObject1 = m;
    Object localObject2;
    Object localObject3;
    Object localObject4;
    Object localObject5;
    int i;
    if (localObject1 != null)
    {
      localObject2 = this;
      localObject3 = locala;
      localObject4 = paramUri;
      localObject5 = paramContentValues;
      i = ((a.h)localObject1).a(this, locala, paramUri, paramContentValues, paramString, paramArrayOfString);
      int j = -1;
      if (i != j)
      {
        localObject1 = q;
        if (localObject1 != null) {
          i = ((a.c)localObject1).a(this, locala, paramUri, paramContentValues, i);
        }
        paramUri = j;
        a(paramUri);
        return i;
      }
    }
    boolean bool1 = d;
    if (bool1)
    {
      bool1 = e;
      if (bool1)
      {
        paramString = android.support.v4.a.a.a(paramString, "_id=?");
        bool1 = true;
        localObject1 = new String[bool1];
        localObject2 = null;
        localObject3 = paramUri.getLastPathSegment();
        localObject1[0] = localObject3;
        paramArrayOfString = android.support.v4.a.a.a(paramArrayOfString, (String[])localObject1);
        localObject4 = paramString;
        localObject5 = paramArrayOfString;
      }
      else
      {
        localObject4 = paramString;
        localObject5 = paramArrayOfString;
      }
      localObject1 = c();
      localObject2 = g;
      i = b;
      localObject3 = paramContentValues;
      i = ((SQLiteDatabase)localObject1).updateWithOnConflict((String)localObject2, paramContentValues, (String)localObject4, (String[])localObject5, i);
      if (i > 0)
      {
        boolean bool2 = c;
        if (bool2)
        {
          paramString = i;
          a(paramString);
        }
        paramString = j;
        a(paramString);
      }
      localObject1 = q;
      if (localObject1 != null)
      {
        localObject2 = this;
        localObject3 = locala;
        localObject4 = paramUri;
        localObject5 = paramContentValues;
        i = ((a.c)localObject1).a(this, locala, paramUri, paramContentValues, i);
      }
      return i;
    }
    paramContentValues = new android/database/sqlite/SQLiteException;
    paramUri = String.valueOf(paramUri);
    paramUri = "Cannot update ".concat(paramUri);
    paramContentValues.<init>(paramUri);
    throw paramContentValues;
  }
  
  protected final int a(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    com.truecaller.common.c.a.a locala = e().b(paramUri);
    Object localObject1 = n;
    Object localObject2;
    String str;
    String[] arrayOfString;
    int i;
    if (localObject1 != null)
    {
      localObject2 = locala;
      str = paramString;
      arrayOfString = paramArrayOfString;
      i = ((a.e)localObject1).a(this, locala, paramUri, paramString, paramArrayOfString);
      int j = -1;
      if (i != j)
      {
        localObject1 = r;
        if (localObject1 != null) {
          i = ((a.a)localObject1).a(this, locala, paramUri, paramString, paramArrayOfString, i);
        }
        paramUri = j;
        a(paramUri);
        return i;
      }
    }
    boolean bool1 = d;
    if (bool1)
    {
      bool1 = e;
      if (bool1)
      {
        paramString = android.support.v4.a.a.a(paramString, "_id=?");
        bool1 = true;
        localObject1 = new String[bool1];
        localObject2 = paramUri.getLastPathSegment();
        localObject1[0] = localObject2;
        paramArrayOfString = android.support.v4.a.a.a(paramArrayOfString, (String[])localObject1);
        str = paramString;
        arrayOfString = paramArrayOfString;
      }
      else if (paramString == null)
      {
        paramString = "1";
        paramArrayOfString = null;
        str = paramString;
        arrayOfString = null;
      }
      else
      {
        str = paramString;
        arrayOfString = paramArrayOfString;
      }
      paramString = c();
      paramArrayOfString = g;
      i = paramString.delete(paramArrayOfString, str, arrayOfString);
      if (i > 0)
      {
        boolean bool2 = c;
        if (bool2)
        {
          paramString = i;
          a(paramString);
        }
        paramString = j;
        a(paramString);
      }
      localObject1 = r;
      if (localObject1 != null)
      {
        localObject2 = locala;
        i = ((a.a)localObject1).a(this, locala, paramUri, str, arrayOfString, i);
      }
      return i;
    }
    paramString = new android/database/sqlite/SQLiteException;
    paramUri = String.valueOf(paramUri);
    paramUri = "Cannot delete from ".concat(paramUri);
    paramString.<init>(paramUri);
    throw paramString;
  }
  
  protected final Cursor a(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal)
  {
    Object localObject1 = paramUri;
    Object localObject2 = e().b(paramUri);
    Object localObject3 = k;
    Object localObject5;
    Object localObject6;
    Object localObject7;
    Object localObject8;
    if (localObject3 != null)
    {
      localObject4 = this;
      localObject5 = paramArrayOfString1;
      localObject6 = paramString1;
      localObject7 = paramArrayOfString2;
      localObject8 = paramString2;
      return ((a.g)localObject3).a(this, (com.truecaller.common.c.a.a)localObject2, paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2, paramCancellationSignal);
    }
    boolean bool1 = c;
    if (bool1)
    {
      bool1 = e;
      boolean bool2;
      if (bool1)
      {
        localObject4 = paramString1;
        localObject3 = android.support.v4.a.a.a(paramString1, "_id=?");
        bool2 = true;
        localObject4 = new String[bool2];
        localObject6 = paramUri.getLastPathSegment();
        localObject4[0] = localObject6;
        localObject5 = paramArrayOfString2;
        localObject4 = android.support.v4.a.a.a(paramArrayOfString2, (String[])localObject4);
        localObject7 = localObject3;
        localObject8 = localObject4;
      }
      else
      {
        localObject4 = paramString1;
        localObject5 = paramArrayOfString2;
        localObject7 = paramString1;
        localObject8 = paramArrayOfString2;
      }
      bool1 = f;
      if (bool1)
      {
        localObject3 = c();
        bool2 = TextUtils.isEmpty((CharSequence)localObject7);
        if (bool2)
        {
          localObject4 = "";
        }
        else
        {
          localObject1 = String.valueOf(localObject7);
          localObject4 = " WHERE ".concat((String)localObject1);
        }
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("SELECT COUNT(*) AS _count FROM ");
        localObject5 = g;
        ((StringBuilder)localObject1).append((String)localObject5);
        ((StringBuilder)localObject1).append((String)localObject4);
        localObject4 = ((StringBuilder)localObject1).toString();
        localObject3 = ((SQLiteDatabase)localObject3).rawQuery((String)localObject4, (String[])localObject8);
      }
      else
      {
        localObject3 = new android/database/sqlite/SQLiteQueryBuilder;
        ((SQLiteQueryBuilder)localObject3).<init>();
        localObject4 = g;
        ((SQLiteQueryBuilder)localObject3).setTables((String)localObject4);
        localObject5 = c();
        localObject4 = "limit";
        String str = paramUri.getQueryParameter((String)localObject4);
        localObject1 = localObject3;
        localObject6 = paramArrayOfString1;
        localObject3 = ((SQLiteQueryBuilder)localObject3).query((SQLiteDatabase)localObject5, paramArrayOfString1, (String)localObject7, (String[])localObject8, null, null, paramString2, str, paramCancellationSignal);
      }
      if (localObject3 != null)
      {
        localObject4 = getContext();
        if (localObject4 != null)
        {
          localObject4 = ((Context)localObject4).getContentResolver();
          localObject2 = i;
          ((Cursor)localObject3).setNotificationUri((ContentResolver)localObject4, (Uri)localObject2);
        }
      }
      return (Cursor)localObject3;
    }
    localObject3 = new android/database/sqlite/SQLiteException;
    Object localObject4 = String.valueOf(paramUri);
    localObject4 = "Cannot read from ".concat((String)localObject4);
    ((SQLiteException)localObject3).<init>((String)localObject4);
    throw ((Throwable)localObject3);
  }
  
  protected final SQLiteDatabase a(Context paramContext)
  {
    com.truecaller.common.c.a.c localc = e();
    e locale = b;
    if (locale != null) {
      return b.a_(paramContext);
    }
    paramContext = new java/lang/IllegalStateException;
    paramContext.<init>("No SQLiteDatabaseFactory defined");
    throw paramContext;
  }
  
  protected final Uri a(Uri paramUri, ContentValues paramContentValues)
  {
    Object localObject1 = e().b(paramUri);
    Object localObject2 = l;
    Object localObject3;
    if (localObject2 != null)
    {
      localObject2 = ((a.f)localObject2).a(this, (com.truecaller.common.c.a.a)localObject1, paramUri, paramContentValues);
      if (localObject2 != null)
      {
        localObject3 = p;
        if (localObject3 != null) {
          localObject2 = ((a.b)localObject3).a(this, paramUri, paramContentValues, (Uri)localObject2);
        }
        boolean bool1 = c;
        if (bool1) {
          a((Uri)localObject2);
        }
        paramUri = j;
        a(paramUri);
        return (Uri)localObject2;
      }
    }
    boolean bool2 = d;
    if (bool2)
    {
      bool2 = e;
      if (!bool2)
      {
        localObject2 = paramContentValues.getAsLong("_id");
        if (localObject2 == null)
        {
          localObject2 = c();
          localObject3 = g;
          Object localObject4 = "_id";
          int i = a;
          long l1 = ((SQLiteDatabase)localObject2).insertWithOnConflict((String)localObject3, (String)localObject4, paramContentValues, i);
          long l2 = 0L;
          boolean bool3 = l1 < l2;
          if (bool3)
          {
            localObject4 = i;
            localObject2 = ContentUris.withAppendedId((Uri)localObject4, l1);
            boolean bool4 = c;
            if (bool4)
            {
              localObject3 = i;
              a((Uri)localObject3);
            }
            localObject3 = j;
            a((Collection)localObject3);
            localObject1 = p;
            if (localObject1 != null)
            {
              localObject2 = ((a.b)localObject1).a(this, paramUri, paramContentValues, (Uri)localObject2);
              if (localObject2 == null)
              {
                paramContentValues = new android/database/sqlite/SQLiteException;
                paramUri = String.valueOf(paramUri);
                paramUri = "Could not insert into ".concat(paramUri);
                paramContentValues.<init>(paramUri);
                throw paramContentValues;
              }
            }
            return (Uri)localObject2;
          }
          paramContentValues = new android/database/sqlite/SQLiteException;
          paramUri = String.valueOf(paramUri);
          paramUri = "Could not insert into ".concat(paramUri);
          paramContentValues.<init>(paramUri);
          throw paramContentValues;
        }
      }
      localObject1 = new android/database/sqlite/SQLiteException;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Cannot insert into a row, ");
      ((StringBuilder)localObject2).append(paramUri);
      ((StringBuilder)localObject2).append(", values=");
      ((StringBuilder)localObject2).append(paramContentValues);
      paramUri = ((StringBuilder)localObject2).toString();
      ((SQLiteException)localObject1).<init>(paramUri);
      throw ((Throwable)localObject1);
    }
    paramContentValues = new android/database/sqlite/SQLiteException;
    paramUri = String.valueOf(paramUri);
    paramUri = "Cannot insert into ".concat(paramUri);
    paramContentValues.<init>(paramUri);
    throw paramContentValues;
  }
  
  protected final Bundle a(String paramString1, String paramString2, Bundle paramBundle)
  {
    Object localObject1 = e();
    Object localObject2 = a.buildUpon().appendPath(paramString1).build();
    localObject1 = ((com.truecaller.common.c.a.c)localObject1).a((Uri)localObject2);
    if (localObject1 != null)
    {
      localObject1 = o;
      if (localObject1 != null) {
        return ((a.d)localObject1).a();
      }
    }
    localObject1 = "dump";
    boolean bool = paramString1.equals(localObject1);
    if (bool)
    {
      bool = TextUtils.isEmpty(paramString2);
      if (bool)
      {
        localObject1 = c();
        localObject2 = System.out;
        b.a((SQLiteDatabase)localObject1, (PrintStream)localObject2);
      }
      else
      {
        localObject1 = c();
        localObject2 = System.out;
        b.a((SQLiteDatabase)localObject1, paramString2, (PrintStream)localObject2);
      }
    }
    return super.call(paramString1, paramString2, paramBundle);
  }
  
  protected final void a()
  {
    super.a();
    Object localObject1 = null;
    try
    {
      b = null;
      return;
    }
    finally {}
  }
  
  protected abstract com.truecaller.common.c.a.c b(Context paramContext);
  
  public String getType(Uri paramUri)
  {
    return ebh;
  }
  
  public boolean onCreate()
  {
    return super.onCreate();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */