package com.truecaller.common.c;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteTransactionListener;
import android.net.Uri;
import android.net.Uri.Builder;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import org.c.a.a.a.k;

public abstract class c
  extends d
  implements SQLiteTransactionListener
{
  private static AtomicBoolean b;
  protected final String a;
  private final ThreadLocal c;
  private final ThreadLocal d;
  private volatile boolean e;
  private volatile SQLiteDatabase f;
  
  static
  {
    AtomicBoolean localAtomicBoolean = new java/util/concurrent/atomic/AtomicBoolean;
    localAtomicBoolean.<init>(false);
    b = localAtomicBoolean;
  }
  
  public c()
  {
    Object localObject = getClass().getSimpleName();
    a = ((String)localObject);
    localObject = new java/lang/ThreadLocal;
    ((ThreadLocal)localObject).<init>();
    c = ((ThreadLocal)localObject);
    localObject = new com/truecaller/common/c/c$1;
    ((c.1)localObject).<init>(this);
    d = ((ThreadLocal)localObject);
  }
  
  private static Uri b(Uri paramUri)
  {
    String str = paramUri.getLastPathSegment();
    boolean bool = k.g(str);
    if (!bool) {
      return paramUri;
    }
    str = paramUri.getPath();
    paramUri = paramUri.buildUpon();
    int i = str.lastIndexOf('/');
    str = str.substring(0, i);
    return paramUri.path(str).build();
  }
  
  public static void b()
  {
    b.set(true);
  }
  
  private boolean e()
  {
    Object localObject = c.get();
    Boolean localBoolean = Boolean.TRUE;
    return localObject == localBoolean;
  }
  
  protected abstract int a(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString);
  
  protected abstract int a(Uri paramUri, String paramString, String[] paramArrayOfString);
  
  /* Error */
  protected final int a(Uri paramUri, ContentValues[] paramArrayOfContentValues)
  {
    // Byte code:
    //   0: aload_2
    //   1: arraylength
    //   2: istore_3
    //   3: aload_0
    //   4: getfield 54	com/truecaller/common/c/c:d	Ljava/lang/ThreadLocal;
    //   7: invokevirtual 110	java/lang/ThreadLocal:remove	()V
    //   10: aload_0
    //   11: invokevirtual 113	com/truecaller/common/c/c:c	()Landroid/database/sqlite/SQLiteDatabase;
    //   14: astore 4
    //   16: aload 4
    //   18: aload_0
    //   19: invokevirtual 119	android/database/sqlite/SQLiteDatabase:beginTransactionWithListener	(Landroid/database/sqlite/SQLiteTransactionListener;)V
    //   22: aload_2
    //   23: arraylength
    //   24: istore 5
    //   26: iconst_0
    //   27: istore 6
    //   29: iconst_1
    //   30: istore 7
    //   32: iload 6
    //   34: iload 5
    //   36: if_icmpge +44 -> 80
    //   39: aload_2
    //   40: iload 6
    //   42: aaload
    //   43: astore 8
    //   45: aload_0
    //   46: aload_1
    //   47: aload 8
    //   49: invokevirtual 122	com/truecaller/common/c/c:a	(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    //   52: astore 8
    //   54: aload 8
    //   56: ifnull +9 -> 65
    //   59: aload_0
    //   60: iload 7
    //   62: putfield 124	com/truecaller/common/c/c:e	Z
    //   65: aload 4
    //   67: invokevirtual 128	android/database/sqlite/SQLiteDatabase:yieldIfContendedSafely	()Z
    //   70: pop
    //   71: iload 6
    //   73: iconst_1
    //   74: iadd
    //   75: istore 6
    //   77: goto -48 -> 29
    //   80: aload_0
    //   81: invokevirtual 130	com/truecaller/common/c/c:d	()V
    //   84: aload 4
    //   86: invokevirtual 133	android/database/sqlite/SQLiteDatabase:setTransactionSuccessful	()V
    //   89: aload 4
    //   91: invokevirtual 136	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   94: aload_0
    //   95: iload 7
    //   97: invokevirtual 138	com/truecaller/common/c/c:a	(Z)V
    //   100: iload_3
    //   101: ireturn
    //   102: astore_1
    //   103: goto +6 -> 109
    //   106: astore_1
    //   107: aload_1
    //   108: athrow
    //   109: aload 4
    //   111: invokevirtual 136	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   114: aload_0
    //   115: iconst_0
    //   116: invokevirtual 138	com/truecaller/common/c/c:a	(Z)V
    //   119: aload_1
    //   120: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	121	0	this	c
    //   0	121	1	paramUri	Uri
    //   0	121	2	paramArrayOfContentValues	ContentValues[]
    //   2	99	3	i	int
    //   14	96	4	localSQLiteDatabase	SQLiteDatabase
    //   24	13	5	j	int
    //   27	49	6	k	int
    //   30	66	7	bool	boolean
    //   43	12	8	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   22	24	102	finally
    //   40	43	102	finally
    //   47	52	102	finally
    //   60	65	102	finally
    //   65	71	102	finally
    //   80	84	102	finally
    //   84	89	102	finally
    //   107	109	102	finally
    //   22	24	106	java/lang/RuntimeException
    //   40	43	106	java/lang/RuntimeException
    //   47	52	106	java/lang/RuntimeException
    //   60	65	106	java/lang/RuntimeException
    //   65	71	106	java/lang/RuntimeException
    //   80	84	106	java/lang/RuntimeException
    //   84	89	106	java/lang/RuntimeException
  }
  
  protected abstract SQLiteDatabase a(Context paramContext);
  
  protected abstract Uri a(Uri paramUri, ContentValues paramContentValues);
  
  protected void a()
  {
    f = null;
  }
  
  public final void a(Uri paramUri)
  {
    if (paramUri != null)
    {
      Set localSet = (Set)d.get();
      paramUri = b(paramUri);
      localSet.add(paramUri);
    }
  }
  
  public final void a(Collection paramCollection)
  {
    if (paramCollection != null)
    {
      paramCollection = paramCollection.iterator();
      for (;;)
      {
        boolean bool = paramCollection.hasNext();
        if (!bool) {
          break;
        }
        Uri localUri = (Uri)paramCollection.next();
        a(localUri);
      }
    }
  }
  
  protected void a(boolean paramBoolean)
  {
    boolean bool1 = e;
    if ((bool1) && (paramBoolean))
    {
      paramBoolean = false;
      e = false;
      Object localObject1 = (Collection)d.get();
      if (localObject1 != null)
      {
        boolean bool2 = ((Collection)localObject1).isEmpty();
        if (!bool2)
        {
          localObject1 = ((Collection)localObject1).iterator();
          for (;;)
          {
            bool2 = ((Iterator)localObject1).hasNext();
            if (!bool2) {
              break;
            }
            Uri localUri = (Uri)((Iterator)localObject1).next();
            Object localObject2 = getContext();
            if (localObject2 != null)
            {
              localObject2 = ((Context)localObject2).getContentResolver();
              ((ContentResolver)localObject2).notifyChange(localUri, null, false);
            }
          }
        }
      }
    }
    d.remove();
  }
  
  /* Error */
  protected final android.content.ContentProviderResult[] a(java.util.ArrayList paramArrayList)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual 193	java/util/ArrayList:size	()I
    //   4: istore_2
    //   5: iload_2
    //   6: ifne +8 -> 14
    //   9: iconst_0
    //   10: anewarray 195	android/content/ContentProviderResult
    //   13: areturn
    //   14: aload_0
    //   15: invokevirtual 113	com/truecaller/common/c/c:c	()Landroid/database/sqlite/SQLiteDatabase;
    //   18: astore_3
    //   19: aload_3
    //   20: aload_0
    //   21: invokevirtual 119	android/database/sqlite/SQLiteDatabase:beginTransactionWithListener	(Landroid/database/sqlite/SQLiteTransactionListener;)V
    //   24: aload_0
    //   25: getfield 47	com/truecaller/common/c/c:c	Ljava/lang/ThreadLocal;
    //   28: astore 4
    //   30: getstatic 107	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   33: astore 5
    //   35: aload 4
    //   37: aload 5
    //   39: invokevirtual 198	java/lang/ThreadLocal:set	(Ljava/lang/Object;)V
    //   42: aload_0
    //   43: getfield 54	com/truecaller/common/c/c:d	Ljava/lang/ThreadLocal;
    //   46: astore 4
    //   48: aload 4
    //   50: invokevirtual 110	java/lang/ThreadLocal:remove	()V
    //   53: iload_2
    //   54: anewarray 195	android/content/ContentProviderResult
    //   57: astore 4
    //   59: iconst_0
    //   60: istore 6
    //   62: aconst_null
    //   63: astore 5
    //   65: iload 6
    //   67: iload_2
    //   68: if_icmpge +75 -> 143
    //   71: aload_1
    //   72: iload 6
    //   74: invokevirtual 201	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   77: astore 7
    //   79: aload 7
    //   81: checkcast 203	android/content/ContentProviderOperation
    //   84: astore 7
    //   86: iload 6
    //   88: ifle +27 -> 115
    //   91: aload 7
    //   93: invokevirtual 206	android/content/ContentProviderOperation:isYieldAllowed	()Z
    //   96: istore 8
    //   98: iload 8
    //   100: ifeq +15 -> 115
    //   103: ldc2_w 207
    //   106: lstore 9
    //   108: aload_3
    //   109: lload 9
    //   111: invokevirtual 213	android/database/sqlite/SQLiteDatabase:yieldIfContendedSafely	(J)Z
    //   114: pop
    //   115: aload 7
    //   117: aload_0
    //   118: aload 4
    //   120: iload 6
    //   122: invokevirtual 217	android/content/ContentProviderOperation:apply	(Landroid/content/ContentProvider;[Landroid/content/ContentProviderResult;I)Landroid/content/ContentProviderResult;
    //   125: astore 7
    //   127: aload 4
    //   129: iload 6
    //   131: aload 7
    //   133: aastore
    //   134: iload 6
    //   136: iconst_1
    //   137: iadd
    //   138: istore 6
    //   140: goto -75 -> 65
    //   143: aload_0
    //   144: invokevirtual 130	com/truecaller/common/c/c:d	()V
    //   147: aload_3
    //   148: invokevirtual 133	android/database/sqlite/SQLiteDatabase:setTransactionSuccessful	()V
    //   151: aload_0
    //   152: getfield 47	com/truecaller/common/c/c:c	Ljava/lang/ThreadLocal;
    //   155: astore_1
    //   156: getstatic 220	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   159: astore 11
    //   161: aload_1
    //   162: aload 11
    //   164: invokevirtual 198	java/lang/ThreadLocal:set	(Ljava/lang/Object;)V
    //   167: aload_3
    //   168: invokevirtual 136	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   171: aload_0
    //   172: iconst_1
    //   173: invokevirtual 138	com/truecaller/common/c/c:a	(Z)V
    //   176: aload 4
    //   178: areturn
    //   179: astore_1
    //   180: goto +6 -> 186
    //   183: astore_1
    //   184: aload_1
    //   185: athrow
    //   186: aload_0
    //   187: getfield 47	com/truecaller/common/c/c:c	Ljava/lang/ThreadLocal;
    //   190: astore 11
    //   192: getstatic 220	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   195: astore 4
    //   197: aload 11
    //   199: aload 4
    //   201: invokevirtual 198	java/lang/ThreadLocal:set	(Ljava/lang/Object;)V
    //   204: aload_3
    //   205: invokevirtual 136	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   208: aload_0
    //   209: iconst_0
    //   210: invokevirtual 138	com/truecaller/common/c/c:a	(Z)V
    //   213: aload_1
    //   214: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	215	0	this	c
    //   0	215	1	paramArrayList	java.util.ArrayList
    //   4	65	2	i	int
    //   18	187	3	localSQLiteDatabase	SQLiteDatabase
    //   28	172	4	localObject1	Object
    //   33	31	5	localBoolean	Boolean
    //   60	79	6	j	int
    //   77	55	7	localObject2	Object
    //   96	3	8	bool	boolean
    //   106	4	9	l	long
    //   159	39	11	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   24	28	179	finally
    //   30	33	179	finally
    //   37	42	179	finally
    //   42	46	179	finally
    //   48	53	179	finally
    //   53	57	179	finally
    //   72	77	179	finally
    //   79	84	179	finally
    //   91	96	179	finally
    //   109	115	179	finally
    //   120	125	179	finally
    //   131	134	179	finally
    //   143	147	179	finally
    //   147	151	179	finally
    //   184	186	179	finally
    //   24	28	183	java/lang/RuntimeException
    //   30	33	183	java/lang/RuntimeException
    //   37	42	183	java/lang/RuntimeException
    //   42	46	183	java/lang/RuntimeException
    //   48	53	183	java/lang/RuntimeException
    //   53	57	183	java/lang/RuntimeException
    //   72	77	183	java/lang/RuntimeException
    //   79	84	183	java/lang/RuntimeException
    //   91	96	183	java/lang/RuntimeException
    //   109	115	183	java/lang/RuntimeException
    //   120	125	183	java/lang/RuntimeException
    //   131	134	183	java/lang/RuntimeException
    //   143	147	183	java/lang/RuntimeException
    //   147	151	183	java/lang/RuntimeException
  }
  
  /* Error */
  protected final int b(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 222	com/truecaller/common/c/c:e	()Z
    //   4: istore 5
    //   6: aload_0
    //   7: invokevirtual 113	com/truecaller/common/c/c:c	()Landroid/database/sqlite/SQLiteDatabase;
    //   10: astore 6
    //   12: iconst_1
    //   13: istore 7
    //   15: iload 5
    //   17: ifne +84 -> 101
    //   20: aload_0
    //   21: getfield 54	com/truecaller/common/c/c:d	Ljava/lang/ThreadLocal;
    //   24: astore 8
    //   26: aload 8
    //   28: invokevirtual 110	java/lang/ThreadLocal:remove	()V
    //   31: aload 6
    //   33: aload_0
    //   34: invokevirtual 119	android/database/sqlite/SQLiteDatabase:beginTransactionWithListener	(Landroid/database/sqlite/SQLiteTransactionListener;)V
    //   37: aload_0
    //   38: aload_1
    //   39: aload_2
    //   40: aload_3
    //   41: aload 4
    //   43: invokevirtual 225	com/truecaller/common/c/c:a	(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   46: istore 9
    //   48: iload 9
    //   50: ifle +9 -> 59
    //   53: aload_0
    //   54: iload 7
    //   56: putfield 124	com/truecaller/common/c/c:e	Z
    //   59: aload_0
    //   60: invokevirtual 130	com/truecaller/common/c/c:d	()V
    //   63: aload 6
    //   65: invokevirtual 133	android/database/sqlite/SQLiteDatabase:setTransactionSuccessful	()V
    //   68: aload 6
    //   70: invokevirtual 136	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   73: aload_0
    //   74: iload 7
    //   76: invokevirtual 138	com/truecaller/common/c/c:a	(Z)V
    //   79: goto +44 -> 123
    //   82: astore_1
    //   83: goto +6 -> 89
    //   86: astore_1
    //   87: aload_1
    //   88: athrow
    //   89: aload 6
    //   91: invokevirtual 136	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   94: aload_0
    //   95: iconst_0
    //   96: invokevirtual 138	com/truecaller/common/c/c:a	(Z)V
    //   99: aload_1
    //   100: athrow
    //   101: aload_0
    //   102: aload_1
    //   103: aload_2
    //   104: aload_3
    //   105: aload 4
    //   107: invokevirtual 225	com/truecaller/common/c/c:a	(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   110: istore 9
    //   112: iload 9
    //   114: ifle +9 -> 123
    //   117: aload_0
    //   118: iload 7
    //   120: putfield 124	com/truecaller/common/c/c:e	Z
    //   123: iload 9
    //   125: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	126	0	this	c
    //   0	126	1	paramUri	Uri
    //   0	126	2	paramContentValues	ContentValues
    //   0	126	3	paramString	String
    //   0	126	4	paramArrayOfString	String[]
    //   4	12	5	bool1	boolean
    //   10	80	6	localSQLiteDatabase	SQLiteDatabase
    //   13	106	7	bool2	boolean
    //   24	3	8	localThreadLocal	ThreadLocal
    //   46	78	9	i	int
    // Exception table:
    //   from	to	target	type
    //   41	46	82	finally
    //   54	59	82	finally
    //   59	63	82	finally
    //   63	68	82	finally
    //   87	89	82	finally
    //   41	46	86	java/lang/RuntimeException
    //   54	59	86	java/lang/RuntimeException
    //   59	63	86	java/lang/RuntimeException
    //   63	68	86	java/lang/RuntimeException
  }
  
  /* Error */
  protected final int b(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 222	com/truecaller/common/c/c:e	()Z
    //   4: istore 4
    //   6: aload_0
    //   7: invokevirtual 113	com/truecaller/common/c/c:c	()Landroid/database/sqlite/SQLiteDatabase;
    //   10: astore 5
    //   12: iconst_1
    //   13: istore 6
    //   15: iload 4
    //   17: ifne +82 -> 99
    //   20: aload_0
    //   21: getfield 54	com/truecaller/common/c/c:d	Ljava/lang/ThreadLocal;
    //   24: astore 7
    //   26: aload 7
    //   28: invokevirtual 110	java/lang/ThreadLocal:remove	()V
    //   31: aload 5
    //   33: aload_0
    //   34: invokevirtual 119	android/database/sqlite/SQLiteDatabase:beginTransactionWithListener	(Landroid/database/sqlite/SQLiteTransactionListener;)V
    //   37: aload_0
    //   38: aload_1
    //   39: aload_2
    //   40: aload_3
    //   41: invokevirtual 228	com/truecaller/common/c/c:a	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   44: istore 8
    //   46: iload 8
    //   48: ifle +9 -> 57
    //   51: aload_0
    //   52: iload 6
    //   54: putfield 124	com/truecaller/common/c/c:e	Z
    //   57: aload_0
    //   58: invokevirtual 130	com/truecaller/common/c/c:d	()V
    //   61: aload 5
    //   63: invokevirtual 133	android/database/sqlite/SQLiteDatabase:setTransactionSuccessful	()V
    //   66: aload 5
    //   68: invokevirtual 136	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   71: aload_0
    //   72: iload 6
    //   74: invokevirtual 138	com/truecaller/common/c/c:a	(Z)V
    //   77: goto +42 -> 119
    //   80: astore_1
    //   81: goto +6 -> 87
    //   84: astore_1
    //   85: aload_1
    //   86: athrow
    //   87: aload 5
    //   89: invokevirtual 136	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   92: aload_0
    //   93: iconst_0
    //   94: invokevirtual 138	com/truecaller/common/c/c:a	(Z)V
    //   97: aload_1
    //   98: athrow
    //   99: aload_0
    //   100: aload_1
    //   101: aload_2
    //   102: aload_3
    //   103: invokevirtual 228	com/truecaller/common/c/c:a	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   106: istore 8
    //   108: iload 8
    //   110: ifle +9 -> 119
    //   113: aload_0
    //   114: iload 6
    //   116: putfield 124	com/truecaller/common/c/c:e	Z
    //   119: iload 8
    //   121: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	122	0	this	c
    //   0	122	1	paramUri	Uri
    //   0	122	2	paramString	String
    //   0	122	3	paramArrayOfString	String[]
    //   4	12	4	bool1	boolean
    //   10	78	5	localSQLiteDatabase	SQLiteDatabase
    //   13	102	6	bool2	boolean
    //   24	3	7	localThreadLocal	ThreadLocal
    //   44	76	8	i	int
    // Exception table:
    //   from	to	target	type
    //   40	44	80	finally
    //   52	57	80	finally
    //   57	61	80	finally
    //   61	66	80	finally
    //   85	87	80	finally
    //   40	44	84	java/lang/RuntimeException
    //   52	57	84	java/lang/RuntimeException
    //   57	61	84	java/lang/RuntimeException
    //   61	66	84	java/lang/RuntimeException
  }
  
  /* Error */
  protected final Uri b(Uri paramUri, ContentValues paramContentValues)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 113	com/truecaller/common/c/c:c	()Landroid/database/sqlite/SQLiteDatabase;
    //   4: astore_3
    //   5: aload_0
    //   6: invokespecial 222	com/truecaller/common/c/c:e	()Z
    //   9: istore 4
    //   11: iconst_1
    //   12: istore 5
    //   14: iload 4
    //   16: ifne +75 -> 91
    //   19: aload_0
    //   20: getfield 54	com/truecaller/common/c/c:d	Ljava/lang/ThreadLocal;
    //   23: astore 6
    //   25: aload 6
    //   27: invokevirtual 110	java/lang/ThreadLocal:remove	()V
    //   30: aload_3
    //   31: aload_0
    //   32: invokevirtual 119	android/database/sqlite/SQLiteDatabase:beginTransactionWithListener	(Landroid/database/sqlite/SQLiteTransactionListener;)V
    //   35: aload_0
    //   36: aload_1
    //   37: aload_2
    //   38: invokevirtual 122	com/truecaller/common/c/c:a	(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    //   41: astore_1
    //   42: aload_1
    //   43: ifnull +9 -> 52
    //   46: aload_0
    //   47: iload 5
    //   49: putfield 124	com/truecaller/common/c/c:e	Z
    //   52: aload_0
    //   53: invokevirtual 130	com/truecaller/common/c/c:d	()V
    //   56: aload_3
    //   57: invokevirtual 133	android/database/sqlite/SQLiteDatabase:setTransactionSuccessful	()V
    //   60: aload_3
    //   61: invokevirtual 136	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   64: aload_0
    //   65: iload 5
    //   67: invokevirtual 138	com/truecaller/common/c/c:a	(Z)V
    //   70: goto +38 -> 108
    //   73: astore_1
    //   74: goto +6 -> 80
    //   77: astore_1
    //   78: aload_1
    //   79: athrow
    //   80: aload_3
    //   81: invokevirtual 136	android/database/sqlite/SQLiteDatabase:endTransaction	()V
    //   84: aload_0
    //   85: iconst_0
    //   86: invokevirtual 138	com/truecaller/common/c/c:a	(Z)V
    //   89: aload_1
    //   90: athrow
    //   91: aload_0
    //   92: aload_1
    //   93: aload_2
    //   94: invokevirtual 122	com/truecaller/common/c/c:a	(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    //   97: astore_1
    //   98: aload_1
    //   99: ifnull +9 -> 108
    //   102: aload_0
    //   103: iload 5
    //   105: putfield 124	com/truecaller/common/c/c:e	Z
    //   108: aload_1
    //   109: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	110	0	this	c
    //   0	110	1	paramUri	Uri
    //   0	110	2	paramContentValues	ContentValues
    //   4	77	3	localSQLiteDatabase	SQLiteDatabase
    //   9	6	4	bool1	boolean
    //   12	92	5	bool2	boolean
    //   23	3	6	localThreadLocal	ThreadLocal
    // Exception table:
    //   from	to	target	type
    //   37	41	73	finally
    //   47	52	73	finally
    //   52	56	73	finally
    //   56	60	73	finally
    //   78	80	73	finally
    //   37	41	77	java/lang/RuntimeException
    //   47	52	77	java/lang/RuntimeException
    //   52	56	77	java/lang/RuntimeException
    //   56	60	77	java/lang/RuntimeException
  }
  
  public final SQLiteDatabase c()
  {
    Object localObject = b;
    boolean bool1 = true;
    boolean bool2 = ((AtomicBoolean)localObject).compareAndSet(bool1, false);
    if (bool2) {
      a();
    }
    localObject = f;
    if (localObject == null) {
      try
      {
        localObject = f;
        if (localObject == null)
        {
          localObject = getContext();
          localObject = a((Context)localObject);
          f = ((SQLiteDatabase)localObject);
        }
      }
      finally {}
    }
    return localSQLiteDatabase;
  }
  
  protected void d() {}
  
  public void onBegin() {}
  
  public void onCommit() {}
  
  public boolean onCreate()
  {
    return true;
  }
  
  public void onRollback() {}
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */