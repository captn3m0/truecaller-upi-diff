package com.truecaller.common.c.a;

import android.content.UriMatcher;
import android.net.Uri;
import android.text.TextUtils;
import android.util.SparseArray;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class b
{
  public String a;
  public boolean b;
  public boolean c;
  public boolean d;
  public int e;
  public a.g f;
  public a.f g;
  public a.h h;
  public a.e i;
  public a.b j;
  public a.c k;
  public a.a l;
  private final d m;
  private final String n;
  private final String o;
  private final int p;
  private int q;
  private Boolean r;
  private Boolean s;
  private Set t;
  private a.d u;
  
  protected b(d paramd, String paramString1, String paramString2, int paramInt)
  {
    m = paramd;
    n = paramString1;
    o = paramString2;
    p = paramInt;
    int i1 = 2;
    e = i1;
    q = i1;
  }
  
  private static void a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
      String str = String.valueOf(paramInt);
      str = "Invalid conflict resolution method, ".concat(str);
      localIllegalStateException.<init>(str);
      throw localIllegalStateException;
    }
  }
  
  public final b a(Collection paramCollection)
  {
    boolean bool = paramCollection.isEmpty();
    if (!bool)
    {
      Object localObject = t;
      if (localObject == null)
      {
        localObject = new java/util/HashSet;
        ((HashSet)localObject).<init>();
        t = ((Set)localObject);
      }
      localObject = t;
      ((Set)localObject).addAll(paramCollection);
    }
    return this;
  }
  
  public final b a(boolean paramBoolean)
  {
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    s = localBoolean;
    return this;
  }
  
  public final b a(Uri... paramVarArgs)
  {
    Object localObject = t;
    if (localObject == null)
    {
      localObject = new java/util/HashSet;
      ((HashSet)localObject).<init>();
      t = ((Set)localObject);
    }
    Collections.addAll(t, paramVarArgs);
    return this;
  }
  
  public final d a()
  {
    b localb = this;
    boolean bool1 = d;
    if (bool1)
    {
      bool1 = b;
      if (bool1)
      {
        localObject1 = new java/lang/IllegalStateException;
        ((IllegalStateException)localObject1).<init>("Cannot combine \"count()\" with \"row()\"");
        throw ((Throwable)localObject1);
      }
    }
    bool1 = false;
    Object localObject1 = null;
    Object localObject2 = t;
    Object localObject3;
    if (localObject2 != null)
    {
      bool1 = d;
      if (!bool1)
      {
        bool1 = c;
        if (!bool1)
        {
          localObject1 = new java/util/HashSet;
          ((HashSet)localObject1).<init>((Collection)localObject2);
          localObject3 = localObject1;
          break label99;
        }
      }
      localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("Cannot use \"alsoNotify(Uri)\" for views or counts");
      throw ((Throwable)localObject1);
    }
    else
    {
      localObject3 = null;
    }
    label99:
    localObject1 = a;
    if (localObject1 != null)
    {
      localObject1 = ((String)localObject1).trim();
      bool1 = TextUtils.isEmpty((CharSequence)localObject1);
      if (!bool1)
      {
        localObject1 = a;
        localObject2 = "#";
        bool1 = ((String)localObject1).contains((CharSequence)localObject2);
        if (!bool1)
        {
          localObject1 = a;
          localObject2 = "*";
          bool1 = ((String)localObject1).contains((CharSequence)localObject2);
          if (!bool1) {
            break label218;
          }
        }
      }
      localObject1 = new java/lang/IllegalStateException;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Bad path, '");
      localObject4 = a;
      ((StringBuilder)localObject2).append((String)localObject4);
      ((StringBuilder)localObject2).append("'");
      localObject2 = ((StringBuilder)localObject2).toString();
      ((IllegalStateException)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    }
    label218:
    a(e);
    int i1 = q;
    a(i1);
    localObject1 = a;
    if (localObject1 == null) {
      localObject1 = o;
    }
    boolean bool2 = d;
    if (bool2)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append("/count");
      localObject1 = ((StringBuilder)localObject2).toString();
    }
    bool2 = b;
    if (bool2)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append("/#");
      localObject1 = ((StringBuilder)localObject2).toString();
    }
    bool2 = b;
    Object localObject5;
    if (bool2)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("vnd.");
      localObject4 = n;
      ((StringBuilder)localObject2).append((String)localObject4);
      ((StringBuilder)localObject2).append(".cursor.item/");
      localObject4 = o;
      ((StringBuilder)localObject2).append((String)localObject4);
      localObject2 = ((StringBuilder)localObject2).toString();
      localObject5 = localObject2;
    }
    else
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("vnd.");
      localObject4 = n;
      ((StringBuilder)localObject2).append((String)localObject4);
      ((StringBuilder)localObject2).append(".cursor.dir/");
      localObject4 = o;
      ((StringBuilder)localObject2).append((String)localObject4);
      localObject2 = ((StringBuilder)localObject2).toString();
      localObject5 = localObject2;
    }
    localObject2 = r;
    int i2 = 1;
    boolean bool3;
    if (localObject2 == null)
    {
      bool3 = true;
    }
    else
    {
      bool2 = ((Boolean)localObject2).booleanValue();
      bool3 = bool2;
    }
    localObject2 = s;
    boolean bool4;
    if (localObject2 == null)
    {
      bool2 = d;
      if (!bool2)
      {
        bool2 = c;
        if (!bool2)
        {
          bool4 = true;
          break label557;
        }
      }
      bool2 = false;
      localObject2 = null;
      bool4 = false;
    }
    else
    {
      bool2 = ((Boolean)localObject2).booleanValue();
      bool4 = bool2;
    }
    label557:
    localObject2 = m;
    int i3 = p;
    Object localObject6 = new com/truecaller/common/c/a/a;
    Object localObject4 = localObject6;
    Object localObject7 = o;
    int i4 = e;
    int i5 = q;
    boolean bool5 = b;
    boolean bool6 = d;
    Object localObject8 = b();
    Object localObject9 = localObject1;
    localObject1 = localObject6;
    localObject6 = localObject8;
    a.g localg = f;
    i1 = i3;
    localObject8 = g;
    a.h localh = h;
    a.e locale = i;
    a.d locald = u;
    a.b localb1 = j;
    a.c localc = k;
    localObject1 = l;
    ((a)localObject4).<init>(i3, (String)localObject7, i4, i5, bool3, bool4, bool5, bool6, (String)localObject5, (Uri)localObject6, (Set)localObject3, localg, (a.f)localObject8, localh, locale, locald, localb1, localc, (a.a)localObject1);
    localObject1 = a;
    i2 = i3;
    i1 = ((SparseArray)localObject1).indexOfKey(i3);
    if (i1 < 0)
    {
      a.put(i3, localObject4);
      localObject1 = m.b;
      localObject2 = n;
      i2 = p;
      ((UriMatcher)localObject1).addURI((String)localObject2, (String)localObject9, i2);
      return m;
    }
    localObject1 = new java/lang/IllegalArgumentException;
    localObject7 = new java/lang/StringBuilder;
    ((StringBuilder)localObject7).<init>("Duplicated match, previous match=");
    localObject2 = a.get(i3);
    ((StringBuilder)localObject7).append(localObject2);
    ((StringBuilder)localObject7).append(", replaced by match=");
    ((StringBuilder)localObject7).append(localObject4);
    localObject2 = ((StringBuilder)localObject7).toString();
    ((IllegalArgumentException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public final Uri b()
  {
    String str1 = a;
    if (str1 == null) {
      str1 = o;
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("content://");
    String str2 = n;
    localStringBuilder.append(str2);
    localStringBuilder.append("/");
    localStringBuilder.append(str1);
    return Uri.parse(localStringBuilder.toString());
  }
  
  public final b b(boolean paramBoolean)
  {
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    r = localBoolean;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */