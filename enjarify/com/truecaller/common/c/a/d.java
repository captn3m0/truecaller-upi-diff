package com.truecaller.common.c.a;

import android.content.UriMatcher;
import android.net.Uri;
import android.text.TextUtils;
import android.util.SparseArray;

public final class d
{
  final SparseArray a;
  final UriMatcher b;
  public e c;
  public String d;
  public Uri e;
  private int f;
  
  public d()
  {
    Object localObject = new android/util/SparseArray;
    ((SparseArray)localObject).<init>();
    a = ((SparseArray)localObject);
    localObject = new android/content/UriMatcher;
    ((UriMatcher)localObject).<init>(-1);
    b = ((UriMatcher)localObject);
  }
  
  public final b a(String paramString)
  {
    Object localObject = d;
    boolean bool = TextUtils.isEmpty((CharSequence)localObject);
    if (!bool)
    {
      bool = TextUtils.isEmpty(paramString);
      if (!bool)
      {
        localObject = new com/truecaller/common/c/a/b;
        String str = d;
        int i = f + 1;
        f = i;
        ((b)localObject).<init>(this, str, paramString, i);
        return (b)localObject;
      }
    }
    paramString = new java/lang/IllegalArgumentException;
    paramString.<init>();
    throw paramString;
  }
  
  public final c a()
  {
    c localc = new com/truecaller/common/c/a/c;
    Uri localUri = e;
    SparseArray localSparseArray = a;
    UriMatcher localUriMatcher = b;
    e locale = c;
    localc.<init>(localUri, localSparseArray, localUriMatcher, locale);
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */