package com.truecaller.common.c.a;

import android.content.UriMatcher;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.util.SparseArray;

public final class c
{
  public final Uri a;
  public final e b;
  private final SparseArray c;
  private final UriMatcher d;
  
  protected c(Uri paramUri, SparseArray paramSparseArray, UriMatcher paramUriMatcher, e parame)
  {
    a = paramUri;
    c = paramSparseArray;
    d = paramUriMatcher;
    b = parame;
  }
  
  public final a a(Uri paramUri)
  {
    SparseArray localSparseArray = c;
    int i = d.match(paramUri);
    return (a)localSparseArray.get(i, null);
  }
  
  public final a b(Uri paramUri)
  {
    Object localObject = a(paramUri);
    if (localObject != null) {
      return (a)localObject;
    }
    localObject = new android/database/sqlite/SQLiteException;
    paramUri = String.valueOf(paramUri);
    paramUri = "Unsupported uri, uri=".concat(paramUri);
    ((SQLiteException)localObject).<init>(paramUri);
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */