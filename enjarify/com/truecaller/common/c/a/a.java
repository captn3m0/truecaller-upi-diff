package com.truecaller.common.c.a;

import android.content.ContentUris;
import android.net.Uri;
import android.text.TextUtils;
import java.util.Set;

public final class a
{
  public final int a;
  public final int b;
  public final boolean c;
  public final boolean d;
  public final boolean e;
  public final boolean f;
  public final String g;
  public final String h;
  public final Uri i;
  public final Set j;
  public final a.g k;
  public final a.f l;
  public final a.h m;
  public final a.e n;
  public final a.d o;
  public final a.b p;
  public final a.c q;
  public final a.a r;
  private final int s;
  
  protected a(int paramInt1, String paramString1, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, String paramString2, Uri paramUri, Set paramSet, a.g paramg, a.f paramf, a.h paramh, a.e parame, a.d paramd, a.b paramb, a.c paramc, a.a parama)
  {
    s = paramInt1;
    g = paramString1;
    a = paramInt2;
    b = paramInt3;
    c = paramBoolean1;
    d = paramBoolean2;
    e = paramBoolean3;
    f = paramBoolean4;
    h = paramString2;
    i = paramUri;
    j = paramSet;
    k = paramg;
    l = paramf;
    m = paramh;
    n = parame;
    o = paramd;
    p = paramb;
    q = paramc;
    r = parama;
  }
  
  public final Uri a(long paramLong)
  {
    return ContentUris.withAppendedId(i, paramLong);
  }
  
  public final String a()
  {
    return g;
  }
  
  public final Uri b()
  {
    return i;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof a;
    if (bool1)
    {
      bool1 = true;
      if (paramObject == this) {
        return bool1;
      }
      paramObject = (a)paramObject;
      int i1 = s;
      int i2 = s;
      if (i1 == i2)
      {
        String str1 = h;
        String str2 = h;
        boolean bool2 = TextUtils.equals(str1, str2);
        if (bool2)
        {
          str1 = g;
          paramObject = g;
          boolean bool3 = TextUtils.equals(str1, (CharSequence)paramObject);
          if (bool3) {
            return bool1;
          }
        }
      }
      return false;
    }
    return false;
  }
  
  public final int hashCode()
  {
    int i1 = s;
    int i2 = h.hashCode() * 13;
    i1 += i2;
    i2 = g.hashCode() * 27;
    return i1 + i2;
  }
  
  public final String toString()
  {
    Object[] arrayOfObject = new Object[7];
    Object localObject = Integer.valueOf(s);
    arrayOfObject[0] = localObject;
    localObject = g;
    arrayOfObject[1] = localObject;
    localObject = h;
    arrayOfObject[2] = localObject;
    localObject = j;
    arrayOfObject[3] = localObject;
    localObject = Boolean.valueOf(c);
    arrayOfObject[4] = localObject;
    localObject = Boolean.valueOf(d);
    arrayOfObject[5] = localObject;
    localObject = Boolean.valueOf(f);
    arrayOfObject[6] = localObject;
    return String.format("{match=0x%08X, table=%s, type=%s, alsoNotify=%s, r=%b, w=%b, c=%b}", arrayOfObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */