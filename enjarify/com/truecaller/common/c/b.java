package com.truecaller.common.c;

import android.database.ContentObserver;
import android.os.Handler;
import android.os.Looper;

public abstract class b
  extends ContentObserver
{
  private final Handler a;
  private final long b;
  private final Runnable c;
  
  public b(Handler paramHandler)
  {
    this(paramHandler, 300L);
  }
  
  public b(Handler paramHandler, long paramLong)
  {
    super(paramHandler);
    if (paramHandler == null)
    {
      paramHandler = new android/os/Handler;
      Looper localLooper = Looper.getMainLooper();
      paramHandler.<init>(localLooper);
    }
    a = paramHandler;
    b = paramLong;
    paramHandler = new com/truecaller/common/c/b$1;
    paramHandler.<init>(this);
    c = paramHandler;
  }
  
  public abstract void a();
  
  protected final void a(long paramLong)
  {
    Handler localHandler = a;
    Runnable localRunnable = c;
    localHandler.removeCallbacks(localRunnable);
    localHandler = a;
    localRunnable = c;
    localHandler.postDelayed(localRunnable, paramLong);
  }
  
  public final void onChange(boolean paramBoolean)
  {
    long l = b;
    a(l);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */