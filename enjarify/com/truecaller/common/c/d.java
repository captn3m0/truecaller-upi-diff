package com.truecaller.common.c;

import android.content.ContentProvider;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal;
import java.util.ArrayList;

public abstract class d
  extends ContentProvider
{
  private d.b a;
  
  public d()
  {
    d.b localb = d.a.a;
    a = localb;
  }
  
  protected abstract int a(Uri paramUri, ContentValues[] paramArrayOfContentValues);
  
  protected abstract Cursor a(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal);
  
  protected Bundle a(String paramString1, String paramString2, Bundle paramBundle)
  {
    return super.call(paramString1, paramString2, paramBundle);
  }
  
  protected abstract ContentProviderResult[] a(ArrayList paramArrayList);
  
  /* Error */
  public final ContentProviderResult[] applyBatch(ArrayList paramArrayList)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokevirtual 22	com/truecaller/common/c/d:a	(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    //   5: areturn
    //   6: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	7	0	this	d
    //   0	7	1	paramArrayList	ArrayList
    // Exception table:
    //   from	to	target	type
    //   1	5	6	finally
  }
  
  protected abstract int b(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString);
  
  protected abstract int b(Uri paramUri, String paramString, String[] paramArrayOfString);
  
  protected abstract Uri b(Uri paramUri, ContentValues paramContentValues);
  
  /* Error */
  public final int bulkInsert(Uri paramUri, ContentValues[] paramArrayOfContentValues)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: aload_2
    //   3: invokevirtual 25	com/truecaller/common/c/d:a	(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    //   6: ireturn
    //   7: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	8	0	this	d
    //   0	8	1	paramUri	Uri
    //   0	8	2	paramArrayOfContentValues	ContentValues[]
    // Exception table:
    //   from	to	target	type
    //   2	6	7	finally
  }
  
  /* Error */
  public final Bundle call(String paramString1, String paramString2, Bundle paramBundle)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: aload_2
    //   3: aload_3
    //   4: invokevirtual 27	com/truecaller/common/c/d:a	(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    //   7: areturn
    //   8: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	9	0	this	d
    //   0	9	1	paramString1	String
    //   0	9	2	paramString2	String
    //   0	9	3	paramBundle	Bundle
    // Exception table:
    //   from	to	target	type
    //   3	7	8	finally
  }
  
  /* Error */
  public final int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: aload_2
    //   3: aload_3
    //   4: invokevirtual 31	com/truecaller/common/c/d:b	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   7: ireturn
    //   8: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	9	0	this	d
    //   0	9	1	paramUri	Uri
    //   0	9	2	paramString	String
    //   0	9	3	paramArrayOfString	String[]
    // Exception table:
    //   from	to	target	type
    //   3	7	8	finally
  }
  
  /* Error */
  public final Uri insert(Uri paramUri, ContentValues paramContentValues)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: aload_2
    //   3: invokevirtual 34	com/truecaller/common/c/d:b	(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    //   6: areturn
    //   7: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	8	0	this	d
    //   0	8	1	paramUri	Uri
    //   0	8	2	paramContentValues	ContentValues
    // Exception table:
    //   from	to	target	type
    //   2	6	7	finally
  }
  
  public final Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
  {
    return query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2, null);
  }
  
  /* Error */
  public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: aload_2
    //   3: aload_3
    //   4: aload 4
    //   6: aload 5
    //   8: aload 6
    //   10: invokevirtual 40	com/truecaller/common/c/d:a	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    //   13: areturn
    //   14: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	15	0	this	d
    //   0	15	1	paramUri	Uri
    //   0	15	2	paramArrayOfString1	String[]
    //   0	15	3	paramString1	String
    //   0	15	4	paramArrayOfString2	String[]
    //   0	15	5	paramString2	String
    //   0	15	6	paramCancellationSignal	CancellationSignal
    // Exception table:
    //   from	to	target	type
    //   8	13	14	finally
  }
  
  /* Error */
  public final int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: aload_2
    //   3: aload_3
    //   4: aload 4
    //   6: invokevirtual 43	com/truecaller/common/c/d:b	(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   9: ireturn
    //   10: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	11	0	this	d
    //   0	11	1	paramUri	Uri
    //   0	11	2	paramContentValues	ContentValues
    //   0	11	3	paramString	String
    //   0	11	4	paramArrayOfString	String[]
    // Exception table:
    //   from	to	target	type
    //   4	9	10	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */