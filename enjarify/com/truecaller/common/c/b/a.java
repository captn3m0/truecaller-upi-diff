package com.truecaller.common.c.b;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.truecaller.common.c.a.e;
import java.util.Collection;

public final class a
  extends SQLiteOpenHelper
  implements e
{
  private final c[] a;
  private final d[] b;
  
  private a(Context paramContext, String paramString, int paramInt, c[] paramArrayOfc, d[] paramArrayOfd)
  {
    super(paramContext, paramString, null, paramInt, locala);
    a = paramArrayOfc;
    b = paramArrayOfd;
  }
  
  public static a a(Context paramContext, String paramString, int paramInt, c[] paramArrayOfc, d[] paramArrayOfd)
  {
    a locala = new com/truecaller/common/c/b/a;
    locala.<init>(paramContext, paramString, paramInt, paramArrayOfc, paramArrayOfd);
    return locala;
  }
  
  public static void a(Cursor paramCursor, Collection paramCollection)
  {
    if (paramCursor != null)
    {
      int i = paramCursor.getCount();
      if (i > 0)
      {
        i = -1;
        paramCursor.moveToPosition(i);
        for (;;)
        {
          boolean bool = paramCursor.moveToNext();
          if (!bool) {
            break;
          }
          ContentValues localContentValues = new android/content/ContentValues;
          localContentValues.<init>();
          DatabaseUtils.cursorRowToContentValues(paramCursor, localContentValues);
          int j = localContentValues.size();
          if (j > 0) {
            paramCollection.add(localContentValues);
          }
        }
        return;
      }
    }
  }
  
  public final SQLiteDatabase a_(Context paramContext)
  {
    return getWritableDatabase();
  }
  
  public final void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
    c[] arrayOfc = a;
    int i = arrayOfc.length;
    int j = 0;
    while (j < i)
    {
      c localc = arrayOfc[j];
      if (localc != null) {
        localc.a(paramSQLiteDatabase);
      }
      j += 1;
    }
  }
  
  public final void onOpen(SQLiteDatabase paramSQLiteDatabase)
  {
    super.onOpen(paramSQLiteDatabase);
    b.b(paramSQLiteDatabase);
    paramSQLiteDatabase.beginTransaction();
    String str = "view";
    try
    {
      b.a(paramSQLiteDatabase, str);
      paramSQLiteDatabase.setTransactionSuccessful();
      return;
    }
    finally
    {
      paramSQLiteDatabase.endTransaction();
    }
  }
  
  public final void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    c[] arrayOfc = a;
    int i = arrayOfc.length;
    int j = 0;
    while (j < i)
    {
      c localc = arrayOfc[j];
      if (localc != null) {
        localc.a(paramSQLiteDatabase, paramInt1, paramInt2);
      }
      j += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */