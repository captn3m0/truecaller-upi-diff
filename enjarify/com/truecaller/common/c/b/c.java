package com.truecaller.common.c.b;

import android.database.sqlite.SQLiteDatabase;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import java.util.ArrayList;
import java.util.List;

public abstract class c
{
  protected final String a;
  protected final c.a[] b;
  
  protected c(String paramString, c.a[] paramArrayOfa)
  {
    a = paramString;
    b = paramArrayOfa;
  }
  
  protected final void a(SQLiteDatabase paramSQLiteDatabase)
  {
    Object localObject1 = b;
    int i = localObject1.length;
    int j = 1;
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject1 = null;
    }
    Object localObject2 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(i, (String[])localObject2);
    boolean bool1 = b[0].a.equals("_id");
    localObject2 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(bool1, (String[])localObject2);
    bool1 = b[0].b.equals("INTEGER PRIMARY KEY");
    localObject2 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(bool1, (String[])localObject2);
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("CREATE TABLE '");
    Object localObject4 = a;
    ((StringBuilder)localObject3).append((String)localObject4);
    localObject4 = "' (";
    ((StringBuilder)localObject3).append((String)localObject4);
    localObject3 = ((StringBuilder)localObject3).toString();
    ((StringBuilder)localObject2).append((String)localObject3);
    int k = 0;
    localObject3 = null;
    Object localObject5;
    for (;;)
    {
      localObject4 = b;
      int m = localObject4.length;
      if (k >= m) {
        break;
      }
      if (k > 0)
      {
        localObject4 = ", ";
        ((StringBuilder)localObject2).append((String)localObject4);
      }
      localObject4 = b[k];
      boolean bool2 = c;
      if (bool2) {
        ((List)localObject1).add(localObject4);
      }
      ((StringBuilder)localObject2).append("'");
      localObject5 = a;
      ((StringBuilder)localObject2).append((String)localObject5);
      localObject5 = "' ";
      ((StringBuilder)localObject2).append((String)localObject5);
      localObject4 = b;
      ((StringBuilder)localObject2).append((String)localObject4);
      k += 1;
    }
    ((StringBuilder)localObject2).append(");");
    localObject3 = new String[j];
    localObject4 = ((StringBuilder)localObject2).toString();
    localObject3[0] = localObject4;
    localObject2 = ((StringBuilder)localObject2).toString();
    paramSQLiteDatabase.execSQL((String)localObject2);
    int n = 0;
    localObject2 = null;
    for (;;)
    {
      k = ((List)localObject1).size();
      if (n >= k) {
        break;
      }
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject4 = (c.a)((List)localObject1).get(n);
      ((StringBuilder)localObject3).append("CREATE INDEX ");
      localObject5 = new java/lang/StringBuilder;
      ((StringBuilder)localObject5).<init>();
      String str = a;
      ((StringBuilder)localObject5).append(str);
      ((StringBuilder)localObject5).append("_");
      str = a;
      ((StringBuilder)localObject5).append(str);
      str = "_idx";
      ((StringBuilder)localObject5).append(str);
      localObject5 = ((StringBuilder)localObject5).toString();
      ((StringBuilder)localObject3).append((String)localObject5);
      ((StringBuilder)localObject3).append(" ON ");
      localObject5 = a;
      ((StringBuilder)localObject3).append((String)localObject5);
      ((StringBuilder)localObject3).append(" (");
      localObject4 = a;
      ((StringBuilder)localObject3).append((String)localObject4);
      ((StringBuilder)localObject3).append(");");
      localObject4 = new String[j];
      localObject5 = ((StringBuilder)localObject3).toString();
      localObject4[0] = localObject5;
      localObject3 = ((StringBuilder)localObject3).toString();
      paramSQLiteDatabase.execSQL((String)localObject3);
      n += 1;
    }
  }
  
  protected void a(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    paramSQLiteDatabase = new String[1];
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("table: ");
    String str1 = a;
    localStringBuilder.append(str1);
    localStringBuilder.append(" unsupported onUpgrade(oldVersion: ");
    localStringBuilder.append(paramInt1);
    localStringBuilder.append(", newVersion: ");
    localStringBuilder.append(paramInt2);
    localStringBuilder.append(")");
    String str2 = localStringBuilder.toString();
    paramSQLiteDatabase[0] = str2;
    AssertionUtil.AlwaysFatal.isTrue(false, paramSQLiteDatabase);
  }
  
  protected final void b(SQLiteDatabase paramSQLiteDatabase)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("DROP TABLE IF EXISTS ");
    String str = a;
    ((StringBuilder)localObject).append(str);
    localObject = ((StringBuilder)localObject).toString();
    paramSQLiteDatabase.execSQL((String)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */