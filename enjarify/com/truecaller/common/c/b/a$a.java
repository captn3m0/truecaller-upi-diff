package com.truecaller.common.c.b;

import android.database.DatabaseErrorHandler;
import android.database.DefaultDatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import com.truecaller.log.AssertionUtil;

final class a$a
  implements DatabaseErrorHandler
{
  public final void onCorruption(SQLiteDatabase paramSQLiteDatabase)
  {
    Object localObject = new String[1];
    String str = paramSQLiteDatabase.toString();
    localObject[0] = str;
    AssertionUtil.isTrue(false, (String[])localObject);
    localObject = new android/database/DefaultDatabaseErrorHandler;
    ((DefaultDatabaseErrorHandler)localObject).<init>();
    ((DefaultDatabaseErrorHandler)localObject).onCorruption(paramSQLiteDatabase);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.b.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */