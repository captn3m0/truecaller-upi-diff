package com.truecaller.common.c.b;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.OperationCanceledException;
import android.text.TextUtils;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import java.io.PrintStream;

public final class b
{
  public static String a = null;
  private static final String[] b = { "_count" };
  
  public static int a(ContentResolver paramContentResolver, Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    return b(paramContentResolver, paramUri, paramString, paramArrayOfString);
  }
  
  private static Cursor a(SQLiteDatabase paramSQLiteDatabase, String paramString, String[] paramArrayOfString)
  {
    return paramSQLiteDatabase.query("sqlite_master", null, paramString, paramArrayOfString, null, null, null);
  }
  
  public static String a(Context paramContext, Class paramClass)
  {
    Object localObject = a;
    if (localObject != null) {
      return (String)localObject;
    }
    localObject = new android/content/ComponentName;
    ((ComponentName)localObject).<init>(paramContext, paramClass);
    paramClass = null;
    try
    {
      paramContext = paramContext.getPackageManager();
      paramContext = paramContext.getProviderInfo((ComponentName)localObject, 0);
      paramClass = authority;
    }
    catch (PackageManager.NameNotFoundException paramContext)
    {
      localObject = new String[0];
      AssertionUtil.shouldNeverHappen(paramContext, (String[])localObject);
    }
    boolean bool = TextUtils.isEmpty(paramClass) ^ true;
    localObject = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(bool, (String[])localObject);
    return paramClass;
  }
  
  public static String a(String paramString, String... paramVarArgs)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int i = 0;
    for (;;)
    {
      int j = paramVarArgs.length;
      if (i >= j) {
        break;
      }
      if (i > 0)
      {
        str1 = ", ";
        localStringBuilder.append(str1);
      }
      String str1 = paramVarArgs[i];
      localStringBuilder.append(paramString);
      char c = '.';
      localStringBuilder.append(c);
      localStringBuilder.append(str1);
      String str2 = " AS ";
      localStringBuilder.append(str2);
      localStringBuilder.append(str1);
      i += 1;
    }
    return localStringBuilder.toString();
  }
  
  public static void a(ContentValues paramContentValues, String paramString, Object paramObject)
  {
    if (paramObject == null)
    {
      paramContentValues.putNull(paramString);
      return;
    }
    boolean bool = paramObject instanceof String;
    if (bool)
    {
      paramObject = (String)paramObject;
      paramContentValues.put(paramString, (String)paramObject);
      return;
    }
    bool = paramObject instanceof Integer;
    if (bool)
    {
      paramObject = (Integer)paramObject;
      paramContentValues.put(paramString, (Integer)paramObject);
      return;
    }
    bool = paramObject instanceof Long;
    if (bool)
    {
      paramObject = (Long)paramObject;
      paramContentValues.put(paramString, (Long)paramObject);
      return;
    }
    bool = paramObject instanceof Boolean;
    if (bool)
    {
      paramObject = (Boolean)paramObject;
      paramContentValues.put(paramString, (Boolean)paramObject);
      return;
    }
    bool = paramObject instanceof Float;
    if (bool)
    {
      paramObject = (Float)paramObject;
      paramContentValues.put(paramString, (Float)paramObject);
      return;
    }
    bool = paramObject instanceof Double;
    if (bool)
    {
      paramObject = (Double)paramObject;
      paramContentValues.put(paramString, (Double)paramObject);
      return;
    }
    bool = paramObject instanceof byte[];
    if (bool)
    {
      paramObject = (byte[])paramObject;
      paramContentValues.put(paramString, (byte[])paramObject);
      return;
    }
    bool = paramObject instanceof Short;
    if (bool)
    {
      paramObject = (Short)paramObject;
      paramContentValues.put(paramString, (Short)paramObject);
      return;
    }
    paramContentValues = new java/lang/IllegalArgumentException;
    paramString = new java/lang/StringBuilder;
    paramString.<init>("Unknown value type, ");
    paramObject = paramObject.getClass();
    paramString.append(paramObject);
    paramString = paramString.toString();
    paramContentValues.<init>(paramString);
    throw paramContentValues;
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.enableWriteAheadLogging();
  }
  
  private static void a(SQLiteDatabase paramSQLiteDatabase, Cursor paramCursor, PrintStream paramPrintStream)
  {
    Object localObject1 = paramCursor;
    PrintStream localPrintStream = paramPrintStream;
    int i = paramCursor.getColumnIndexOrThrow("type");
    Object localObject3 = paramCursor.getString(i);
    int j = paramCursor.getColumnIndexOrThrow("name");
    String str1 = paramCursor.getString(j);
    int k = paramCursor.getColumnIndexOrThrow("sql");
    localObject1 = paramCursor.getString(k);
    Object localObject4 = "%s, name=%s, sql=%s";
    int n = 3;
    Object localObject5 = new Object[n];
    localObject5[0] = localObject3;
    int i1 = 1;
    localObject5[i1] = str1;
    if (localObject1 == null) {
      localObject1 = "<none>";
    }
    int i2 = 2;
    localObject5[i2] = localObject1;
    localObject1 = String.format((String)localObject4, (Object[])localObject5);
    localPrintStream.println((String)localObject1);
    localObject1 = "view";
    int i3 = ((String)localObject1).equals(localObject3);
    if (i3 == 0)
    {
      localObject1 = "table";
      i3 = ((String)localObject1).equals(localObject3);
      if (i3 == 0) {}
    }
    else
    {
      Object[] arrayOfObject = null;
      int i5 = 0;
      boolean bool2 = false;
      String str2 = null;
      localObject4 = paramSQLiteDatabase;
      localObject5 = str1;
      localObject3 = paramSQLiteDatabase.query(str1, null, null, null, null, null, null);
      if (localObject3 != null)
      {
        i3 = 0;
        localObject1 = null;
        try
        {
          for (;;)
          {
            boolean bool1 = ((Cursor)localObject3).moveToNext();
            if (!bool1) {
              break;
            }
            int m = ((Cursor)localObject3).getColumnCount();
            localObject5 = "%s row %d, %d columns";
            arrayOfObject = new Object[n];
            arrayOfObject[0] = str1;
            i5 = i3 + 1;
            localObject1 = Integer.valueOf(i3);
            arrayOfObject[i1] = localObject1;
            localObject1 = Integer.valueOf(m);
            arrayOfObject[i2] = localObject1;
            localObject1 = String.format((String)localObject5, arrayOfObject);
            localPrintStream.println((String)localObject1);
            int i4 = 0;
            localObject1 = null;
            while (i4 < m)
            {
              localObject5 = "%s=%s";
              arrayOfObject = new Object[i2];
              str2 = ((Cursor)localObject3).getColumnName(i4);
              arrayOfObject[0] = str2;
              bool2 = ((Cursor)localObject3).isNull(i4);
              if (bool2) {
                str2 = "NULL";
              } else {
                str2 = ((Cursor)localObject3).getString(i4);
              }
              arrayOfObject[i1] = str2;
              localObject5 = String.format((String)localObject5, arrayOfObject);
              localPrintStream.println((String)localObject5);
              i4 += 1;
            }
            i4 = i5;
          }
          return;
        }
        finally
        {
          ((Cursor)localObject3).close();
        }
      }
    }
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase, PrintStream paramPrintStream)
  {
    Cursor localCursor = a(paramSQLiteDatabase, null, null);
    try
    {
      for (;;)
      {
        boolean bool = localCursor.moveToNext();
        if (!bool) {
          break;
        }
        a(paramSQLiteDatabase, localCursor, paramPrintStream);
      }
      return;
    }
    finally
    {
      localCursor.close();
    }
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase, String paramString)
  {
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramString;
    b(paramSQLiteDatabase, "SELECT 'drop ' || type || ' ' || name || ';' FROM sqlite_master WHERE name !='android_metadata' AND type=?", arrayOfString);
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase, String paramString, PrintStream paramPrintStream)
  {
    String str = "name=?";
    int i = 1;
    String[] arrayOfString = new String[i];
    arrayOfString[0] = paramString;
    paramString = a(paramSQLiteDatabase, str, arrayOfString);
    try
    {
      for (;;)
      {
        boolean bool = paramString.moveToNext();
        if (!bool) {
          break;
        }
        a(paramSQLiteDatabase, paramString, paramPrintStream);
      }
      return;
    }
    finally
    {
      paramString.close();
    }
  }
  
  public static void a(SQLiteDatabase paramSQLiteDatabase, String paramString1, String paramString2)
  {
    String[] arrayOfString = new String[2];
    arrayOfString[0] = paramString1;
    arrayOfString[1] = paramString2;
    b(paramSQLiteDatabase, "SELECT 'drop ' || type || ' ' || name || ';' FROM sqlite_master WHERE name !='android_metadata' AND type=? and name=?", arrayOfString);
  }
  
  private static int b(ContentResolver paramContentResolver, Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    int i = -1;
    try
    {
      paramUri = paramUri.buildUpon();
      Object localObject = "count";
      paramUri = paramUri.appendPath((String)localObject);
      Uri localUri = paramUri.build();
      String[] arrayOfString = b;
      localObject = paramContentResolver;
      paramContentResolver = paramContentResolver.query(localUri, arrayOfString, paramString, paramArrayOfString, null, null);
      if (paramContentResolver != null) {
        try
        {
          boolean bool = paramContentResolver.moveToNext();
          if (bool)
          {
            bool = false;
            paramUri = null;
            int j = paramContentResolver.getInt(0);
            i = j;
          }
        }
        finally
        {
          paramContentResolver.close();
        }
      }
    }
    catch (SQLiteException|OperationCanceledException localSQLiteException)
    {
      for (;;) {}
    }
    return i;
  }
  
  public static String b(String paramString, String... paramVarArgs)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("CREATE INDEX IF NOT EXISTS idx_");
    localStringBuilder.append(paramString);
    localStringBuilder.append("_");
    String str = TextUtils.join("_", paramVarArgs);
    localStringBuilder.append(str);
    localStringBuilder.append(" ON ");
    localStringBuilder.append(paramString);
    localStringBuilder.append("(");
    paramString = TextUtils.join(",", paramVarArgs);
    localStringBuilder.append(paramString);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public static void b(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.setForeignKeyConstraintsEnabled(true);
  }
  
  private static void b(SQLiteDatabase paramSQLiteDatabase, String paramString, String[] paramArrayOfString)
  {
    paramString = paramSQLiteDatabase.rawQuery(paramString, paramArrayOfString);
    if (paramString != null) {
      try
      {
        for (;;)
        {
          boolean bool = paramString.moveToNext();
          if (!bool) {
            break;
          }
          bool = false;
          paramArrayOfString = null;
          paramArrayOfString = paramString.getString(0);
          paramSQLiteDatabase.execSQL(paramArrayOfString);
        }
        return;
      }
      finally
      {
        paramString.close();
      }
    }
  }
  
  /* Error */
  public static boolean b(SQLiteDatabase paramSQLiteDatabase, String paramString1, String paramString2)
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_3
    //   2: aconst_null
    //   3: astore 4
    //   5: new 72	java/lang/StringBuilder
    //   8: astore 5
    //   10: ldc_w 296
    //   13: astore 6
    //   15: aload 5
    //   17: aload 6
    //   19: invokespecial 142	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   22: aload 5
    //   24: aload_1
    //   25: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   28: pop
    //   29: ldc_w 298
    //   32: astore_1
    //   33: aload 5
    //   35: aload_1
    //   36: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   39: pop
    //   40: aload 5
    //   42: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   45: astore_1
    //   46: aload_0
    //   47: aload_1
    //   48: aconst_null
    //   49: invokevirtual 291	android/database/sqlite/SQLiteDatabase:rawQuery	(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    //   52: astore 4
    //   54: aload 4
    //   56: aload_2
    //   57: invokeinterface 301 2 0
    //   62: istore 7
    //   64: iconst_m1
    //   65: istore 8
    //   67: iload 7
    //   69: iload 8
    //   71: if_icmpeq +17 -> 88
    //   74: aload 4
    //   76: ifnull +10 -> 86
    //   79: aload 4
    //   81: invokeinterface 221 1 0
    //   86: iload_3
    //   87: ireturn
    //   88: aload 4
    //   90: ifnull +10 -> 100
    //   93: aload 4
    //   95: invokeinterface 221 1 0
    //   100: iconst_0
    //   101: ireturn
    //   102: astore_0
    //   103: goto +58 -> 161
    //   106: astore_0
    //   107: iload_3
    //   108: anewarray 12	java/lang/String
    //   111: astore_1
    //   112: new 72	java/lang/StringBuilder
    //   115: astore_2
    //   116: ldc_w 303
    //   119: astore 9
    //   121: aload_2
    //   122: aload 9
    //   124: invokespecial 142	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   127: aload_0
    //   128: invokevirtual 308	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   131: astore_0
    //   132: aload_2
    //   133: aload_0
    //   134: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   137: pop
    //   138: aload_2
    //   139: invokevirtual 91	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   142: astore_0
    //   143: aload_1
    //   144: iconst_0
    //   145: aload_0
    //   146: aastore
    //   147: aload 4
    //   149: ifnull +10 -> 159
    //   152: aload 4
    //   154: invokeinterface 221 1 0
    //   159: iconst_0
    //   160: ireturn
    //   161: aload 4
    //   163: ifnull +10 -> 173
    //   166: aload 4
    //   168: invokeinterface 221 1 0
    //   173: aload_0
    //   174: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	175	0	paramSQLiteDatabase	SQLiteDatabase
    //   0	175	1	paramString1	String
    //   0	175	2	paramString2	String
    //   1	107	3	bool	boolean
    //   3	164	4	localCursor	Cursor
    //   8	33	5	localStringBuilder	StringBuilder
    //   13	5	6	str1	String
    //   62	10	7	i	int
    //   65	7	8	j	int
    //   119	4	9	str2	String
    // Exception table:
    //   from	to	target	type
    //   5	8	102	finally
    //   17	22	102	finally
    //   24	29	102	finally
    //   35	40	102	finally
    //   40	45	102	finally
    //   48	52	102	finally
    //   56	62	102	finally
    //   107	111	102	finally
    //   112	115	102	finally
    //   122	127	102	finally
    //   127	131	102	finally
    //   133	138	102	finally
    //   138	142	102	finally
    //   145	147	102	finally
    //   5	8	106	java/lang/Exception
    //   17	22	106	java/lang/Exception
    //   24	29	106	java/lang/Exception
    //   35	40	106	java/lang/Exception
    //   40	45	106	java/lang/Exception
    //   48	52	106	java/lang/Exception
    //   56	62	106	java/lang/Exception
  }
  
  public static void c(SQLiteDatabase paramSQLiteDatabase)
  {
    b(paramSQLiteDatabase, "SELECT 'drop ' || type || ' ' || name || ';' FROM sqlite_master WHERE name !='android_metadata' AND type != 'index'", null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.c.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */