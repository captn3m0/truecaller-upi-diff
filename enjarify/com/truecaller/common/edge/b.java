package com.truecaller.common.edge;

import android.telephony.TelephonyManager;
import c.f;
import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.g;
import c.x;
import com.truecaller.common.network.edge.EdgeDto;
import com.truecaller.common.network.edge.EdgeDto.a;
import com.truecaller.log.UnmutedException.b;
import com.truecaller.log.d;
import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

public final class b
  implements a
{
  private final File b;
  private EdgeDto c;
  private final f d;
  private final com.truecaller.common.account.r e;
  private final com.truecaller.common.g.a f;
  private final TelephonyManager g;
  
  static
  {
    g[] arrayOfg = new g[1];
    Object localObject = new c/g/b/u;
    c.l.b localb = w.a(b.class);
    ((u)localObject).<init>(localb, "localEdgeDto", "getLocalEdgeDto()Lcom/truecaller/common/network/edge/EdgeDto;");
    localObject = (g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  /* Error */
  public b(com.truecaller.common.account.r paramr, com.truecaller.common.g.a parama, TelephonyManager paramTelephonyManager, File paramFile)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 45
    //   3: invokestatic 50	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aload_2
    //   7: ldc 52
    //   9: invokestatic 50	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   12: aload_3
    //   13: ldc 54
    //   15: invokestatic 50	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   18: ldc 56
    //   20: astore 5
    //   22: aload 4
    //   24: aload 5
    //   26: invokestatic 50	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   29: aload_0
    //   30: invokespecial 59	java/lang/Object:<init>	()V
    //   33: aload_0
    //   34: aload_1
    //   35: putfield 61	com/truecaller/common/edge/b:e	Lcom/truecaller/common/account/r;
    //   38: aload_0
    //   39: aload_2
    //   40: putfield 63	com/truecaller/common/edge/b:f	Lcom/truecaller/common/g/a;
    //   43: aload_0
    //   44: aload_3
    //   45: putfield 65	com/truecaller/common/edge/b:g	Landroid/telephony/TelephonyManager;
    //   48: new 67	java/io/File
    //   51: astore_1
    //   52: ldc 69
    //   54: astore_2
    //   55: aload_1
    //   56: aload 4
    //   58: aload_2
    //   59: invokespecial 72	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   62: aload_0
    //   63: aload_1
    //   64: putfield 74	com/truecaller/common/edge/b:b	Ljava/io/File;
    //   67: getstatic 79	com/truecaller/common/edge/b$a:a	Lcom/truecaller/common/edge/b$a;
    //   70: checkcast 81	c/g/a/a
    //   73: invokestatic 86	c/g:a	(Lc/g/a/a;)Lc/f;
    //   76: astore_1
    //   77: aload_0
    //   78: aload_1
    //   79: putfield 88	com/truecaller/common/edge/b:d	Lc/f;
    //   82: aload_0
    //   83: getfield 74	com/truecaller/common/edge/b:b	Ljava/io/File;
    //   86: astore_1
    //   87: aload_1
    //   88: invokevirtual 92	java/io/File:exists	()Z
    //   91: istore 6
    //   93: iload 6
    //   95: ifeq +188 -> 283
    //   98: aload_0
    //   99: monitorenter
    //   100: aload_0
    //   101: getfield 74	com/truecaller/common/edge/b:b	Ljava/io/File;
    //   104: astore_1
    //   105: getstatic 97	c/n/d:a	Ljava/nio/charset/Charset;
    //   108: astore_2
    //   109: new 99	java/io/FileInputStream
    //   112: astore_3
    //   113: aload_3
    //   114: aload_1
    //   115: invokespecial 102	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   118: aload_3
    //   119: checkcast 104	java/io/InputStream
    //   122: astore_3
    //   123: new 106	java/io/InputStreamReader
    //   126: astore_1
    //   127: aload_1
    //   128: aload_3
    //   129: aload_2
    //   130: invokespecial 109	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V
    //   133: aload_1
    //   134: checkcast 111	java/io/Closeable
    //   137: astore_1
    //   138: iconst_0
    //   139: istore 7
    //   141: aconst_null
    //   142: astore_2
    //   143: aload_1
    //   144: astore_3
    //   145: aload_1
    //   146: checkcast 106	java/io/InputStreamReader
    //   149: astore_3
    //   150: new 113	com/google/gson/f
    //   153: astore 4
    //   155: aload 4
    //   157: invokespecial 114	com/google/gson/f:<init>	()V
    //   160: aload_3
    //   161: checkcast 116	java/io/Reader
    //   164: astore_3
    //   165: ldc 118
    //   167: astore 5
    //   169: aload 4
    //   171: aload_3
    //   172: aload 5
    //   174: invokevirtual 121	com/google/gson/f:a	(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;
    //   177: astore_3
    //   178: aload_3
    //   179: checkcast 118	com/truecaller/common/network/edge/EdgeDto
    //   182: astore_3
    //   183: aload_1
    //   184: aconst_null
    //   185: invokestatic 126	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   188: aload_0
    //   189: aload_3
    //   190: invokespecial 129	com/truecaller/common/edge/b:b	(Lcom/truecaller/common/network/edge/EdgeDto;)Z
    //   193: pop
    //   194: aload_0
    //   195: monitorexit
    //   196: return
    //   197: astore_3
    //   198: goto +6 -> 204
    //   201: astore_2
    //   202: aload_2
    //   203: athrow
    //   204: aload_1
    //   205: aload_2
    //   206: invokestatic 126	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   209: aload_3
    //   210: athrow
    //   211: astore_1
    //   212: aload_0
    //   213: monitorexit
    //   214: aload_1
    //   215: athrow
    //   216: astore_1
    //   217: aload_1
    //   218: instanceof 131
    //   221: istore 7
    //   223: iload 7
    //   225: ifeq +49 -> 274
    //   228: new 133	com/truecaller/log/UnmutedException$b
    //   231: astore_2
    //   232: new 135	java/lang/StringBuilder
    //   235: astore_3
    //   236: aload_3
    //   237: ldc -119
    //   239: invokespecial 140	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   242: aload_1
    //   243: checkcast 142	java/lang/Exception
    //   246: invokevirtual 146	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   249: astore_1
    //   250: aload_3
    //   251: aload_1
    //   252: invokevirtual 150	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   255: pop
    //   256: aload_3
    //   257: invokevirtual 153	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   260: astore_1
    //   261: aload_2
    //   262: aload_1
    //   263: invokespecial 154	com/truecaller/log/UnmutedException$b:<init>	(Ljava/lang/String;)V
    //   266: aload_2
    //   267: checkcast 156	java/lang/Throwable
    //   270: invokestatic 161	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   273: return
    //   274: aload_1
    //   275: checkcast 156	java/lang/Throwable
    //   278: astore_1
    //   279: aload_1
    //   280: invokestatic 161	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   283: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	284	0	this	b
    //   0	284	1	paramr	com.truecaller.common.account.r
    //   0	284	2	parama	com.truecaller.common.g.a
    //   0	284	3	paramTelephonyManager	TelephonyManager
    //   0	284	4	paramFile	File
    //   20	153	5	localObject	Object
    //   91	3	6	bool1	boolean
    //   139	85	7	bool2	boolean
    // Exception table:
    //   from	to	target	type
    //   202	204	197	finally
    //   145	149	201	finally
    //   150	153	201	finally
    //   155	160	201	finally
    //   160	164	201	finally
    //   172	177	201	finally
    //   178	182	201	finally
    //   100	104	211	finally
    //   105	108	211	finally
    //   109	112	211	finally
    //   114	118	211	finally
    //   118	122	211	finally
    //   123	126	211	finally
    //   129	133	211	finally
    //   133	137	211	finally
    //   184	188	211	finally
    //   189	194	211	finally
    //   205	209	211	finally
    //   209	211	211	finally
    //   98	100	216	java/lang/Exception
    //   194	196	216	java/lang/Exception
    //   212	214	216	java/lang/Exception
    //   214	216	216	java/lang/Exception
  }
  
  private final String a(EdgeDto paramEdgeDto, String paramString1, String paramString2)
  {
    if (paramEdgeDto != null) {
      try
      {
        paramEdgeDto = paramEdgeDto.getData();
        if (paramEdgeDto != null)
        {
          paramEdgeDto = paramEdgeDto.get(paramString1);
          paramEdgeDto = (Map)paramEdgeDto;
          if (paramEdgeDto != null)
          {
            paramEdgeDto = paramEdgeDto.get(paramString2);
            paramEdgeDto = (EdgeDto.a)paramEdgeDto;
            if (paramEdgeDto != null) {
              paramEdgeDto = a;
            }
          }
        }
      }
      finally {}
    }
    paramEdgeDto = null;
    if (paramEdgeDto != null)
    {
      paramEdgeDto = (String)c.a.m.e(paramEdgeDto);
      if (paramEdgeDto != null)
      {
        paramString1 = paramEdgeDto;
        paramString1 = (CharSequence)paramEdgeDto;
        boolean bool = c.n.m.a(paramString1);
        if (!bool) {
          return paramEdgeDto;
        }
      }
    }
    return null;
  }
  
  /* Error */
  private final boolean a(EdgeDto paramEdgeDto)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 74	com/truecaller/common/edge/b:b	Ljava/io/File;
    //   4: astore_2
    //   5: getstatic 97	c/n/d:a	Ljava/nio/charset/Charset;
    //   8: astore_3
    //   9: new 192	java/io/FileOutputStream
    //   12: astore 4
    //   14: aload 4
    //   16: aload_2
    //   17: invokespecial 193	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   20: aload 4
    //   22: checkcast 195	java/io/OutputStream
    //   25: astore 4
    //   27: new 197	java/io/OutputStreamWriter
    //   30: astore_2
    //   31: aload_2
    //   32: aload 4
    //   34: aload_3
    //   35: invokespecial 200	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V
    //   38: aload_2
    //   39: checkcast 111	java/io/Closeable
    //   42: astore_2
    //   43: iconst_0
    //   44: istore 5
    //   46: aconst_null
    //   47: astore_3
    //   48: aload_2
    //   49: astore 4
    //   51: aload_2
    //   52: checkcast 197	java/io/OutputStreamWriter
    //   55: astore 4
    //   57: new 113	com/google/gson/f
    //   60: astore 6
    //   62: aload 6
    //   64: invokespecial 114	com/google/gson/f:<init>	()V
    //   67: aload 4
    //   69: checkcast 202	java/lang/Appendable
    //   72: astore 4
    //   74: aload 6
    //   76: aload_1
    //   77: aload 4
    //   79: invokevirtual 205	com/google/gson/f:a	(Ljava/lang/Object;Ljava/lang/Appendable;)V
    //   82: getstatic 210	c/x:a	Lc/x;
    //   85: astore 4
    //   87: aload_2
    //   88: aconst_null
    //   89: invokestatic 126	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   92: aload_1
    //   93: invokevirtual 214	com/truecaller/common/network/edge/EdgeDto:getTimeToLive	()I
    //   96: istore 7
    //   98: iconst_1
    //   99: istore 5
    //   101: iload 7
    //   103: ifle +121 -> 224
    //   106: invokestatic 220	java/lang/System:currentTimeMillis	()J
    //   109: lstore 8
    //   111: getstatic 226	java/util/concurrent/TimeUnit:SECONDS	Ljava/util/concurrent/TimeUnit;
    //   114: astore_2
    //   115: aload_1
    //   116: invokevirtual 214	com/truecaller/common/network/edge/EdgeDto:getTimeToLive	()I
    //   119: istore 10
    //   121: iload 10
    //   123: i2l
    //   124: lstore 11
    //   126: aload_2
    //   127: lload 11
    //   129: invokevirtual 230	java/util/concurrent/TimeUnit:toMillis	(J)J
    //   132: lstore 11
    //   134: lload 8
    //   136: lload 11
    //   138: ladd
    //   139: lstore 8
    //   141: iload 5
    //   143: anewarray 183	java/lang/String
    //   146: astore_2
    //   147: new 135	java/lang/StringBuilder
    //   150: astore 13
    //   152: ldc -24
    //   154: astore 14
    //   156: aload 13
    //   158: aload 14
    //   160: invokespecial 140	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   163: aload 13
    //   165: aload_1
    //   166: invokevirtual 235	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   169: pop
    //   170: ldc -19
    //   172: astore_1
    //   173: aload 13
    //   175: aload_1
    //   176: invokevirtual 150	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   179: pop
    //   180: new 239	java/util/Date
    //   183: astore_1
    //   184: aload_1
    //   185: lload 8
    //   187: invokespecial 242	java/util/Date:<init>	(J)V
    //   190: aload 13
    //   192: aload_1
    //   193: invokevirtual 235	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   196: pop
    //   197: aload 13
    //   199: invokevirtual 153	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   202: astore_1
    //   203: aload_2
    //   204: iconst_0
    //   205: aload_1
    //   206: aastore
    //   207: aload_0
    //   208: getfield 63	com/truecaller/common/edge/b:f	Lcom/truecaller/common/g/a;
    //   211: astore_1
    //   212: ldc -12
    //   214: astore_2
    //   215: aload_1
    //   216: aload_2
    //   217: lload 8
    //   219: invokeinterface 249 4 0
    //   224: iload 5
    //   226: ireturn
    //   227: astore_1
    //   228: goto +8 -> 236
    //   231: astore_1
    //   232: aload_1
    //   233: astore_3
    //   234: aload_1
    //   235: athrow
    //   236: aload_2
    //   237: aload_3
    //   238: invokestatic 126	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   241: aload_1
    //   242: athrow
    //   243: checkcast 156	java/lang/Throwable
    //   246: astore_1
    //   247: aload_1
    //   248: invokestatic 161	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   251: goto +11 -> 262
    //   254: checkcast 156	java/lang/Throwable
    //   257: astore_1
    //   258: aload_1
    //   259: invokestatic 161	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   262: iconst_0
    //   263: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	264	0	this	b
    //   0	264	1	paramEdgeDto	EdgeDto
    //   4	233	2	localObject1	Object
    //   8	230	3	localObject2	Object
    //   12	74	4	localObject3	Object
    //   44	181	5	bool	boolean
    //   60	15	6	localf	com.google.gson.f
    //   96	6	7	i	int
    //   109	109	8	l1	long
    //   119	3	10	j	int
    //   124	13	11	l2	long
    //   150	48	13	localStringBuilder	StringBuilder
    //   243	1	13	localRuntimeException	RuntimeException
    //   154	5	14	str	String
    //   254	1	14	localIOException	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   234	236	227	finally
    //   51	55	231	finally
    //   57	60	231	finally
    //   62	67	231	finally
    //   67	72	231	finally
    //   77	82	231	finally
    //   82	85	231	finally
    //   0	4	243	java/lang/RuntimeException
    //   5	8	243	java/lang/RuntimeException
    //   9	12	243	java/lang/RuntimeException
    //   16	20	243	java/lang/RuntimeException
    //   20	25	243	java/lang/RuntimeException
    //   27	30	243	java/lang/RuntimeException
    //   34	38	243	java/lang/RuntimeException
    //   38	42	243	java/lang/RuntimeException
    //   88	92	243	java/lang/RuntimeException
    //   92	96	243	java/lang/RuntimeException
    //   106	109	243	java/lang/RuntimeException
    //   111	114	243	java/lang/RuntimeException
    //   115	119	243	java/lang/RuntimeException
    //   127	132	243	java/lang/RuntimeException
    //   141	146	243	java/lang/RuntimeException
    //   147	150	243	java/lang/RuntimeException
    //   158	163	243	java/lang/RuntimeException
    //   165	170	243	java/lang/RuntimeException
    //   175	180	243	java/lang/RuntimeException
    //   180	183	243	java/lang/RuntimeException
    //   185	190	243	java/lang/RuntimeException
    //   192	197	243	java/lang/RuntimeException
    //   197	202	243	java/lang/RuntimeException
    //   205	207	243	java/lang/RuntimeException
    //   207	211	243	java/lang/RuntimeException
    //   217	224	243	java/lang/RuntimeException
    //   237	241	243	java/lang/RuntimeException
    //   241	243	243	java/lang/RuntimeException
    //   0	4	254	java/io/IOException
    //   5	8	254	java/io/IOException
    //   9	12	254	java/io/IOException
    //   16	20	254	java/io/IOException
    //   20	25	254	java/io/IOException
    //   27	30	254	java/io/IOException
    //   34	38	254	java/io/IOException
    //   38	42	254	java/io/IOException
    //   88	92	254	java/io/IOException
    //   92	96	254	java/io/IOException
    //   106	109	254	java/io/IOException
    //   111	114	254	java/io/IOException
    //   115	119	254	java/io/IOException
    //   127	132	254	java/io/IOException
    //   141	146	254	java/io/IOException
    //   147	150	254	java/io/IOException
    //   158	163	254	java/io/IOException
    //   165	170	254	java/io/IOException
    //   175	180	254	java/io/IOException
    //   180	183	254	java/io/IOException
    //   185	190	254	java/io/IOException
    //   192	197	254	java/io/IOException
    //   197	202	254	java/io/IOException
    //   205	207	254	java/io/IOException
    //   207	211	254	java/io/IOException
    //   217	224	254	java/io/IOException
    //   237	241	254	java/io/IOException
    //   241	243	254	java/io/IOException
  }
  
  private final boolean b(EdgeDto paramEdgeDto)
  {
    c = paramEdgeDto;
    if (paramEdgeDto != null) {
      paramEdgeDto = paramEdgeDto.getData();
    } else {
      paramEdgeDto = null;
    }
    return paramEdgeDto != null;
  }
  
  public final String a(String paramString1, String paramString2)
  {
    k.b(paramString1, "domain");
    k.b(paramString2, "edgeName");
    Object localObject = c;
    localObject = a((EdgeDto)localObject, paramString1, paramString2);
    if (localObject == null)
    {
      localObject = (EdgeDto)d.b();
      localObject = a((EdgeDto)localObject, paramString1, paramString2);
    }
    return (String)localObject;
  }
  
  public final boolean a()
  {
    Object localObject1 = e.b();
    if (localObject1 == null)
    {
      localObject1 = f;
      str1 = "profileNumber";
      localObject1 = ((com.truecaller.common.g.a)localObject1).a(str1);
    }
    boolean bool1 = false;
    String str1 = null;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/log/UnmutedException$b;
      ((UnmutedException.b)localObject1).<init>("Trying to call edge location without phone number");
      d.a((Throwable)localObject1);
      return false;
    }
    Object localObject3 = e.a();
    if (localObject3 == null)
    {
      localObject3 = f;
      localObject4 = "profileCountryIso";
      localObject3 = ((com.truecaller.common.g.a)localObject3).a((String)localObject4);
    }
    if (localObject3 == null)
    {
      localObject1 = new com/truecaller/log/UnmutedException$b;
      ((UnmutedException.b)localObject1).<init>("Trying to call edge location without profile country code");
      d.a((Throwable)localObject1);
      return false;
    }
    Object localObject4 = f;
    String str2 = "edgeLocationsLastRequestTime";
    long l = System.currentTimeMillis();
    ((com.truecaller.common.g.a)localObject4).b(str2, l);
    localObject4 = g.getNetworkCountryIso();
    localObject1 = com.truecaller.common.network.edge.a.a((String)localObject4, (String)localObject3, (String)localObject1).c();
    localObject3 = "response";
    k.a(localObject1, (String)localObject3);
    boolean bool2 = ((e.r)localObject1).d();
    if (!bool2) {
      return false;
    }
    localObject1 = (EdgeDto)((e.r)localObject1).e();
    if (localObject1 == null) {
      return false;
    }
    str1 = "response.body() ?: return false";
    k.a(localObject1, str1);
    try
    {
      bool1 = b((EdgeDto)localObject1);
      if (bool1)
      {
        boolean bool3 = a((EdgeDto)localObject1);
        return bool3;
      }
      localObject1 = x.a;
      return true;
    }
    finally {}
  }
  
  public final boolean a(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "domain");
    k.b(paramString2, "edgeName");
    Object localObject1 = "edgeHost";
    k.b(paramString3, (String)localObject1);
    try
    {
      localObject1 = c;
      if (localObject1 == null)
      {
        localObject1 = new com/truecaller/common/network/edge/EdgeDto;
        ((EdgeDto)localObject1).<init>();
      }
      Object localObject2 = ((EdgeDto)localObject1).getData();
      if (localObject2 == null)
      {
        localObject2 = new java/util/LinkedHashMap;
        ((LinkedHashMap)localObject2).<init>();
        localObject2 = (Map)localObject2;
        ((EdgeDto)localObject1).setData((Map)localObject2);
      }
      localObject2 = ((EdgeDto)localObject1).getData();
      if (localObject2 != null)
      {
        localObject2 = ((Map)localObject2).get(paramString1);
        localObject2 = (Map)localObject2;
        if (localObject2 != null) {}
      }
      else
      {
        localObject2 = new java/util/LinkedHashMap;
        ((LinkedHashMap)localObject2).<init>();
        localObject2 = (Map)localObject2;
      }
      EdgeDto.a locala = new com/truecaller/common/network/edge/EdgeDto$a;
      locala.<init>();
      int i = 1;
      String[] arrayOfString = new String[i];
      arrayOfString[0] = paramString3;
      paramString3 = c.a.m.c(arrayOfString);
      a = paramString3;
      ((Map)localObject2).put(paramString2, locala);
      paramString2 = ((EdgeDto)localObject1).getData();
      if (paramString2 != null) {
        paramString2.put(paramString1, localObject2);
      }
      c = ((EdgeDto)localObject1);
      boolean bool = a((EdgeDto)localObject1);
      return bool;
    }
    finally {}
  }
  
  public final void b(String paramString1, String paramString2)
  {
    k.b(paramString1, "domain");
    Object localObject = "edgeName";
    k.b(paramString2, (String)localObject);
    try
    {
      localObject = c;
      if (localObject != null)
      {
        Map localMap = ((EdgeDto)localObject).getData();
        if (localMap != null)
        {
          paramString1 = localMap.get(paramString1);
          paramString1 = (Map)paramString1;
          if (paramString1 != null)
          {
            paramString1 = paramString1.remove(paramString2);
            paramString1 = (EdgeDto.a)paramString1;
            break label74;
          }
        }
        paramString1 = null;
        label74:
        if (paramString1 != null) {
          a((EdgeDto)localObject);
        }
        paramString1 = x.a;
      }
      return;
    }
    finally {}
  }
  
  public final boolean b()
  {
    Object localObject = f;
    String str = "edgeLocationsLastRequestTime";
    long l1 = 0L;
    long l2 = ((com.truecaller.common.g.a)localObject).a(str, l1);
    boolean bool = l2 < l1;
    if (bool)
    {
      localObject = c;
      if (localObject != null) {
        return true;
      }
    }
    return false;
  }
  
  public final void c()
  {
    try
    {
      Object localObject1 = b;
      ((File)localObject1).delete();
      localObject1 = null;
      c = null;
      localObject1 = x.a;
      f.d("edgeLocationsExpiration");
      f.d("edgeLocationsLastRequestTime");
      return;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.edge.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */