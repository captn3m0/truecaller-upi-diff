package com.truecaller.common.edge;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.common.account.r;
import com.truecaller.common.background.TrackedWorker;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public final class EdgeLocationsWorker
  extends TrackedWorker
{
  public static final EdgeLocationsWorker.a f;
  public a b;
  public com.truecaller.common.g.a c;
  public b d;
  public r e;
  
  static
  {
    EdgeLocationsWorker.a locala = new com/truecaller/common/edge/EdgeLocationsWorker$a;
    locala.<init>((byte)0);
    f = locala;
  }
  
  public EdgeLocationsWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = com.truecaller.common.b.a.F();
    k.a(paramContext, "ApplicationBase.getAppBase()");
    paramContext.u().a(this);
  }
  
  public final b b()
  {
    b localb = d;
    if (localb == null)
    {
      String str = "analytics";
      k.a(str);
    }
    return localb;
  }
  
  public final boolean c()
  {
    r localr = e;
    if (localr == null)
    {
      String str = "accountManager";
      k.a(str);
    }
    return localr.c();
  }
  
  public final ListenableWorker.a d()
  {
    long l1 = System.currentTimeMillis();
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "coreSettings";
      k.a((String)localObject2);
    }
    long l2 = 0L;
    long l3 = ((com.truecaller.common.g.a)localObject1).a("edgeLocationsLastRequestTime", l2);
    localObject1 = Long.valueOf(l3);
    Object localObject2 = localObject1;
    localObject2 = (Number)localObject1;
    long l4 = ((Number)localObject2).longValue();
    int i = 1;
    boolean bool1 = l4 < l2;
    int j;
    String str1;
    if (bool1)
    {
      j = 1;
    }
    else
    {
      j = 0;
      str1 = null;
    }
    boolean bool2;
    if (j == 0)
    {
      bool2 = false;
      localObject1 = null;
    }
    String str3;
    if (localObject1 != null)
    {
      l4 = ((Long)localObject1).longValue();
      bool2 = l4 < l1;
      if (bool2)
      {
        localObject1 = b;
        if (localObject1 == null)
        {
          localObject2 = "edgeLocationsManager";
          k.a((String)localObject2);
        }
        ((a)localObject1).c();
        localObject1 = TimeUnit.MILLISECONDS;
        l4 -= l1;
        l1 = ((TimeUnit)localObject1).toDays(l4);
        double d1 = l1;
        localObject1 = b();
        localObject2 = new com/truecaller/analytics/e$a;
        String str2 = "IllegalEdgeLocationTtl";
        ((e.a)localObject2).<init>(str2);
        localObject3 = Double.valueOf(d1);
        localObject3 = ((e.a)localObject2).a((Double)localObject3).a();
        str3 = "AnalyticsEvent.Builder(C…ueToSum(daysDiff).build()";
        k.a(localObject3, str3);
        ((b)localObject1).b((e)localObject3);
      }
      else
      {
        localObject1 = c;
        if (localObject1 == null)
        {
          str1 = "coreSettings";
          k.a(str1);
        }
        str1 = "edgeLocationsExpiration";
        l2 = ((com.truecaller.common.g.a)localObject1).a(str1, l2);
        bool2 = l2 < l1;
        if (bool2) {
          break label281;
        }
      }
    }
    i = 0;
    localObject2 = null;
    label281:
    if (i != 0)
    {
      localObject3 = ListenableWorker.a.a();
      k.a(localObject3, "Result.success()");
      return (ListenableWorker.a)localObject3;
    }
    try
    {
      localObject3 = b;
      if (localObject3 == null)
      {
        str3 = "edgeLocationsManager";
        k.a(str3);
      }
      boolean bool3 = ((a)localObject3).a();
      if (bool3) {
        localObject3 = ListenableWorker.a.a();
      }
      for (str3 = "Result.success()";; str3 = "Result.failure()")
      {
        k.a(localObject3, str3);
        break;
        localObject3 = ListenableWorker.a.c();
      }
      return (ListenableWorker.a)localObject3;
    }
    catch (IOException localIOException)
    {
      localObject3 = ListenableWorker.a.c();
      str3 = "Result.failure()";
      k.a(localObject3, str3);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.edge.EdgeLocationsWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */