package com.truecaller.common.edge;

import javax.inject.Provider;

public final class d
  implements dagger.a.d
{
  private final c a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  private d(c paramc, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramc;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
  }
  
  public static d a(c paramc, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    d locald = new com/truecaller/common/edge/d;
    locald.<init>(paramc, paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return locald;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.edge.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */