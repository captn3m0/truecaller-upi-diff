package com.truecaller.common.ui.a;

import android.content.Context;
import android.widget.AdapterView.OnItemClickListener;

public final class b$a
{
  public Context a;
  public int b;
  public int c;
  public int d;
  public AdapterView.OnItemClickListener e;
  
  public b$a(Context paramContext)
  {
    a = paramContext;
  }
  
  public final a a()
  {
    b = 2131886275;
    return this;
  }
  
  public final a a(AdapterView.OnItemClickListener paramOnItemClickListener)
  {
    e = paramOnItemClickListener;
    return this;
  }
  
  public final a b()
  {
    d = 2131886274;
    return this;
  }
  
  public final void c()
  {
    b localb = new com/truecaller/common/ui/a/b;
    Context localContext = a;
    int i = b;
    int j = d;
    int k = c;
    AdapterView.OnItemClickListener localOnItemClickListener = e;
    localb.<init>(localContext, i, j, k, localOnItemClickListener, (byte)0);
    localb.show();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.ui.a.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */