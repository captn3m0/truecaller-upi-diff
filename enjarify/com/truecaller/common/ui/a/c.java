package com.truecaller.common.ui.a;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.m;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.app.AppCompatDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.truecaller.common.R.id;
import com.truecaller.common.R.layout;

public class c
  extends AppCompatDialog
  implements TextWatcher, AdapterView.OnItemClickListener
{
  AdapterView.OnItemClickListener a;
  int b;
  int c;
  private final ListAdapter d;
  private final boolean e;
  private int f;
  
  public c(Context paramContext, ListAdapter paramListAdapter)
  {
    super(paramContext, i);
    d = paramListAdapter;
    boolean bool = paramListAdapter instanceof Filterable;
    e = bool;
    getDelegate().requestWindowFeature(1);
  }
  
  private void a(int paramInt)
  {
    int i = R.id.title;
    TextView localTextView = (TextView)findViewById(i);
    if (localTextView == null) {
      return;
    }
    if (paramInt != 0)
    {
      localTextView.setText(paramInt);
      return;
    }
    localTextView.setVisibility(8);
  }
  
  public void afterTextChanged(Editable paramEditable)
  {
    boolean bool = e;
    if (bool)
    {
      Filter localFilter = ((Filterable)d).getFilter();
      localFilter.filter(paramEditable);
    }
  }
  
  public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = R.layout.dialog_filterable;
    setContentView(i);
    i = R.id.editor;
    paramBundle = (EditText)findViewById(i);
    boolean bool = e;
    int j;
    if (!bool)
    {
      j = 8;
      paramBundle.setVisibility(j);
    }
    else
    {
      paramBundle.addTextChangedListener(this);
      localListAdapter = null;
      int k = c;
      m.a(paramBundle, 0, k);
      j = b;
      if (j != 0) {
        paramBundle.setHint(j);
      }
    }
    i = f;
    a(i);
    i = R.id.list;
    paramBundle = (ListView)findViewById(i);
    ListAdapter localListAdapter = d;
    paramBundle.setAdapter(localListAdapter);
    paramBundle.setOnItemClickListener(this);
  }
  
  public void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    dismiss();
    AdapterView.OnItemClickListener localOnItemClickListener = a;
    if (localOnItemClickListener != null) {
      localOnItemClickListener.onItemClick(paramAdapterView, paramView, paramInt, paramLong);
    }
  }
  
  public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public void setTitle(int paramInt)
  {
    f = paramInt;
    a(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.ui.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */