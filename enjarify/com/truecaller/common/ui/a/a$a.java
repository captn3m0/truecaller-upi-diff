package com.truecaller.common.ui.a;

import android.widget.Filter;
import android.widget.Filter.FilterResults;
import com.truecaller.common.h.am;
import com.truecaller.common.network.country.CountryListDto.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.c.a.a.a.k;

abstract class a$a
  extends Filter
{
  private final List a;
  private final List b;
  private CharSequence c = null;
  
  public a$a(List paramList)
  {
    a = paramList;
    ArrayList localArrayList = new java/util/ArrayList;
    int i = paramList.size();
    localArrayList.<init>(i);
    b = localArrayList;
  }
  
  protected Filter.FilterResults performFiltering(CharSequence paramCharSequence)
  {
    Object localObject1 = c;
    boolean bool1 = am.c(paramCharSequence, (CharSequence)localObject1);
    boolean bool2;
    if (bool1)
    {
      localObject1 = b.iterator();
      for (;;)
      {
        bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        localObject2 = nextb;
        bool2 = am.c((CharSequence)localObject2, paramCharSequence);
        if (!bool2) {
          ((Iterator)localObject1).remove();
        }
      }
      c = paramCharSequence;
    }
    else
    {
      localObject1 = new java/util/ArrayList;
      localObject2 = a;
      ((ArrayList)localObject1).<init>((Collection)localObject2);
      b.clear();
      localObject2 = ((List)localObject1).iterator();
      boolean bool3;
      Object localObject3;
      for (;;)
      {
        bool3 = ((Iterator)localObject2).hasNext();
        if (!bool3) {
          break;
        }
        localObject3 = (CountryListDto.a)((Iterator)localObject2).next();
        Object localObject4 = b;
        boolean bool4 = true;
        boolean bool5 = k.a((CharSequence)localObject4, paramCharSequence, bool4);
        if (bool5)
        {
          localObject4 = b;
          ((List)localObject4).add(localObject3);
          ((Iterator)localObject2).remove();
        }
      }
      localObject1 = ((List)localObject1).iterator();
      for (;;)
      {
        bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        localObject2 = (CountryListDto.a)((Iterator)localObject1).next();
        localObject3 = b;
        bool3 = am.c((CharSequence)localObject3, paramCharSequence);
        if (bool3)
        {
          localObject3 = b;
          ((List)localObject3).add(localObject2);
        }
      }
    }
    paramCharSequence = new android/widget/Filter$FilterResults;
    paramCharSequence.<init>();
    localObject1 = new java/util/ArrayList;
    Object localObject2 = b;
    ((ArrayList)localObject1).<init>((Collection)localObject2);
    values = localObject1;
    int i = b.size();
    count = i;
    return paramCharSequence;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.ui.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */