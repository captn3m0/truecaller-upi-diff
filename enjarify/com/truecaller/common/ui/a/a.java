package com.truecaller.common.ui.a;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.truecaller.common.R.layout;
import com.truecaller.common.network.country.CountryListDto.a;
import java.util.ArrayList;
import java.util.List;

public final class a
  extends BaseAdapter
  implements Filterable
{
  private List a;
  private Filter b;
  
  public a(List paramList)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(paramList);
    a = localArrayList;
  }
  
  public final int getCount()
  {
    return a.size();
  }
  
  public final Filter getFilter()
  {
    Object localObject = b;
    if (localObject == null)
    {
      localObject = new com/truecaller/common/ui/a/a$1;
      List localList = a;
      ((a.1)localObject).<init>(this, localList);
      b = ((Filter)localObject);
    }
    return b;
  }
  
  public final Object getItem(int paramInt)
  {
    return a.get(paramInt);
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null)
    {
      paramView = LayoutInflater.from(paramViewGroup.getContext());
      int i = R.layout.item_country;
      paramView = paramView.inflate(i, paramViewGroup, false);
      paramViewGroup = new com/truecaller/common/ui/a/a$b;
      paramViewGroup.<init>(paramView);
      paramView.setTag(paramViewGroup);
    }
    else
    {
      paramViewGroup = (a.b)paramView.getTag();
    }
    Object localObject1 = (CountryListDto.a)a.get(paramInt);
    Object localObject2 = a;
    String str = b;
    ((TextView)localObject2).setText(str);
    paramViewGroup = b;
    localObject2 = android.support.v4.e.a.a();
    Object[] arrayOfObject = new Object[1];
    localObject1 = d;
    arrayOfObject[0] = localObject1;
    localObject1 = String.format("(+%s)", arrayOfObject);
    localObject1 = ((android.support.v4.e.a)localObject2).a((String)localObject1);
    paramViewGroup.setText((CharSequence)localObject1);
    return paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.ui.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */