package com.truecaller.common.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import com.truecaller.common.R.styleable;

public class RoundedCornerImageView
  extends AppCompatImageView
{
  private final Path a;
  private RectF b;
  private float[] c;
  
  public RoundedCornerImageView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramContext = new android/graphics/Path;
    paramContext.<init>();
    a = paramContext;
    paramContext = new float[8];
    c = paramContext;
    int i = 1;
    if (paramAttributeSet != null)
    {
      Object localObject = getContext();
      int[] arrayOfInt = R.styleable.RoundedCornerImageView;
      paramAttributeSet = ((Context)localObject).obtainStyledAttributes(paramAttributeSet, arrayOfInt, 0, 0);
      localObject = c;
      int j = R.styleable.RoundedCornerImageView_topLeftRadius;
      float f1 = paramAttributeSet.getDimensionPixelSize(j, 0);
      localObject[0] = f1;
      localObject = c;
      f1 = localObject[0];
      localObject[i] = f1;
      j = R.styleable.RoundedCornerImageView_topRightRadius;
      f1 = paramAttributeSet.getDimensionPixelSize(j, 0);
      int k = 2;
      localObject[k] = f1;
      localObject = c;
      float f2 = localObject[k];
      localObject[3] = f2;
      j = R.styleable.RoundedCornerImageView_bottomRightRadius;
      f1 = paramAttributeSet.getDimensionPixelSize(j, 0);
      k = 4;
      localObject[k] = f1;
      localObject = c;
      f2 = localObject[k];
      localObject[5] = f2;
      j = R.styleable.RoundedCornerImageView_bottomLeftRadius;
      f1 = paramAttributeSet.getDimensionPixelSize(j, 0);
      int m = 6;
      localObject[m] = f1;
      localObject = c;
      j = 7;
      f1 = 9.8E-45F;
      float f3 = localObject[m];
      localObject[j] = f3;
      paramAttributeSet.recycle();
    }
    setLayerType(i, null);
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    Path localPath = a;
    RectF localRectF = b;
    float[] arrayOfFloat = c;
    Path.Direction localDirection = Path.Direction.CW;
    localPath.addRoundRect(localRectF, arrayOfFloat, localDirection);
    localPath = a;
    paramCanvas.clipPath(localPath);
    super.onDraw(paramCanvas);
    a.rewind();
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    RectF localRectF = new android/graphics/RectF;
    float f1 = paramInt1;
    float f2 = paramInt2;
    localRectF.<init>(0.0F, 0.0F, f1, f2);
    b = localRectF;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.ui.view.RoundedCornerImageView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */