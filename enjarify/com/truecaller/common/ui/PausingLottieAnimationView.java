package com.truecaller.common.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import c.g.b.k;
import com.airbnb.lottie.LottieAnimationView;

public final class PausingLottieAnimationView
  extends LottieAnimationView
{
  private boolean a = true;
  private boolean b;
  
  public PausingLottieAnimationView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private PausingLottieAnimationView(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet, (byte)0);
  }
  
  public final void a()
  {
    boolean bool = isShown();
    if (bool)
    {
      super.a();
      return;
    }
    b = true;
  }
  
  public final void b()
  {
    boolean bool = isShown();
    if (bool)
    {
      super.b();
      return;
    }
    b = true;
  }
  
  public final void d()
  {
    b = false;
    super.d();
  }
  
  public final void e()
  {
    b = false;
    super.e();
  }
  
  protected final void onVisibilityChanged(View paramView, int paramInt)
  {
    String str = "changedView";
    k.b(paramView, str);
    boolean bool = a;
    if (!bool) {
      return;
    }
    bool = isShown();
    if (bool)
    {
      bool = b;
      if (bool)
      {
        b();
        b = false;
      }
    }
    else
    {
      bool = c();
      if (bool)
      {
        e();
        bool = true;
        b = bool;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.ui.PausingLottieAnimationView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */