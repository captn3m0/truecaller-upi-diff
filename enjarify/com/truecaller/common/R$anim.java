package com.truecaller.common;

public final class R$anim
{
  public static final int abc_fade_in = 2130771968;
  public static final int abc_fade_out = 2130771969;
  public static final int abc_grow_fade_in_from_bottom = 2130771970;
  public static final int abc_popup_enter = 2130771971;
  public static final int abc_popup_exit = 2130771972;
  public static final int abc_shrink_fade_out_from_bottom = 2130771973;
  public static final int abc_slide_in_bottom = 2130771974;
  public static final int abc_slide_in_top = 2130771975;
  public static final int abc_slide_out_bottom = 2130771976;
  public static final int abc_slide_out_top = 2130771977;
  public static final int abc_tooltip_enter = 2130771978;
  public static final int abc_tooltip_exit = 2130771979;
  public static final int design_bottom_sheet_slide_in = 2130771984;
  public static final int design_bottom_sheet_slide_out = 2130771985;
  public static final int design_snackbar_in = 2130771986;
  public static final int design_snackbar_out = 2130771987;
  public static final int fast_slide_in_up = 2130771991;
  public static final int fast_slide_out_down = 2130771992;
  public static final int slide_in_up = 2130772000;
  public static final int slide_out_down = 2130772001;
}

/* Location:
 * Qualified Name:     com.truecaller.common.R.anim
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */