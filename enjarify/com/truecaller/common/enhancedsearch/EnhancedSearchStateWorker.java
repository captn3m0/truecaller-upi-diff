package com.truecaller.common.enhancedsearch;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.e;
import c.g.b.k;
import c.n.m;
import com.truecaller.log.d;
import java.io.IOException;

public final class EnhancedSearchStateWorker
  extends Worker
{
  public static final EnhancedSearchStateWorker.a d;
  public com.truecaller.common.account.r b;
  public com.truecaller.common.g.a c;
  private final Context e;
  
  static
  {
    EnhancedSearchStateWorker.a locala = new com/truecaller/common/enhancedsearch/EnhancedSearchStateWorker$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  public EnhancedSearchStateWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    e = paramContext;
    paramContext = com.truecaller.common.b.a.F();
    k.a(paramContext, "ApplicationBase.getAppBase()");
    paramContext.u().a(this);
  }
  
  public static final void a(boolean paramBoolean)
  {
    EnhancedSearchStateWorker.a.a(paramBoolean);
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      str1 = "accountManager";
      k.a(str1);
    }
    boolean bool1 = ((com.truecaller.common.account.r)localObject1).c();
    if (!bool1)
    {
      localObject1 = ListenableWorker.a.a();
      k.a(localObject1, "Result.success()");
      return (ListenableWorker.a)localObject1;
    }
    localObject1 = getInputData();
    String str1 = "enhanced_search_value";
    bool1 = ((e)localObject1).a(str1);
    try
    {
      localObject1 = com.truecaller.common.network.c.a.a(bool1);
      localObject1 = ((e.b)localObject1).c();
      str1 = "response";
      k.a(localObject1, str1);
      boolean bool2 = ((e.r)localObject1).d();
      if (bool2)
      {
        localObject1 = ((e.r)localObject1).e();
        localObject1 = (com.truecaller.common.network.g.a)localObject1;
        bool2 = true;
        if (localObject1 != null)
        {
          localObject1 = a;
          if (localObject1 != null)
          {
            localObject1 = a;
            localObject2 = "ENABLED";
            bool1 = m.a((String)localObject1, (String)localObject2, bool2);
            break label135;
          }
        }
        bool1 = false;
        localObject1 = null;
        label135:
        Object localObject2 = c;
        if (localObject2 == null)
        {
          str2 = "coreSettings";
          k.a(str2);
        }
        String str2 = "backup";
        ((com.truecaller.common.g.a)localObject2).b(str2, bool1);
        localObject1 = c;
        if (localObject1 == null)
        {
          localObject2 = "coreSettings";
          k.a((String)localObject2);
        }
        localObject2 = "core_enhancedSearchReported";
        ((com.truecaller.common.g.a)localObject1).b((String)localObject2, bool2);
        localObject1 = ListenableWorker.a.a();
        str1 = "Result.success()";
        k.a(localObject1, str1);
        return (ListenableWorker.a)localObject1;
      }
    }
    catch (RuntimeException localRuntimeException)
    {
      localObject1 = (Throwable)localRuntimeException;
      d.a((Throwable)localObject1);
    }
    catch (IOException localIOException)
    {
      localObject1 = (Throwable)localIOException;
      d.a((Throwable)localObject1);
    }
    localObject1 = ListenableWorker.a.b();
    k.a(localObject1, "Result.retry()");
    return (ListenableWorker.a)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.enhancedsearch.EnhancedSearchStateWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */