package com.truecaller.common.enhancedsearch;

import androidx.work.c;
import androidx.work.c.a;
import androidx.work.e;
import androidx.work.e.a;
import androidx.work.g;
import androidx.work.j;
import androidx.work.k;
import androidx.work.k.a;
import androidx.work.p;

public final class EnhancedSearchStateWorker$a
{
  public static void a(boolean paramBoolean)
  {
    p localp = p.a();
    g localg = g.a;
    k.a locala = new androidx/work/k$a;
    locala.<init>(EnhancedSearchStateWorker.class);
    Object localObject1 = new androidx/work/c$a;
    ((c.a)localObject1).<init>();
    j localj = j.b;
    localObject1 = ((c.a)localObject1).a(localj).a();
    locala = (k.a)locala.a((c)localObject1);
    localObject1 = new androidx/work/e$a;
    ((e.a)localObject1).<init>();
    Object localObject2 = ((e.a)localObject1).a("enhanced_search_value", paramBoolean).a();
    localObject2 = (k)((k.a)locala.a((e)localObject2)).c();
    localp.a("EnhancedSearchStateWorker", localg, (k)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.enhancedsearch.EnhancedSearchStateWorker.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */