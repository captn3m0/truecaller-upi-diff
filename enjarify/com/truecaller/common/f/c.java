package com.truecaller.common.f;

import android.support.v4.f.j;

public abstract interface c
{
  public abstract int a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(String paramString1, String paramString2, c.b paramb);
  
  public abstract void a(String paramString1, String paramString2, c.c paramc);
  
  public abstract boolean a(long paramLong);
  
  public abstract j b();
  
  public abstract void b(String paramString1, String paramString2, c.c paramc);
  
  public abstract void c();
  
  public abstract boolean d();
  
  public abstract long e();
  
  public abstract String f();
  
  public abstract boolean g();
  
  public abstract long h();
  
  public abstract int i();
  
  public abstract boolean j();
  
  public abstract String k();
  
  public abstract boolean l();
  
  public abstract boolean m();
  
  public abstract boolean n();
  
  public abstract boolean o();
}

/* Location:
 * Qualified Name:     com.truecaller.common.f.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */