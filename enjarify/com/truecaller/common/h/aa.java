package com.truecaller.common.h;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import com.google.c.a.g;
import com.google.c.a.g.a;
import com.google.c.a.k.c;
import com.google.c.a.m.a;
import com.google.c.a.n;
import com.truecaller.common.b.a;
import com.truecaller.log.AssertionUtil;
import java.util.Locale;

public final class aa
{
  public static String a(String paramString)
  {
    String str = a.F().H();
    k.c localc = k.c.a;
    return a(paramString, str, localc);
  }
  
  public static String a(String paramString1, String paramString2)
  {
    k.c localc = k.c.a;
    return a(paramString1, paramString2, localc);
  }
  
  private static String a(String paramString1, String paramString2, k.c paramc)
  {
    boolean bool1 = am.b(paramString1);
    if (bool1)
    {
      try
      {
        synchronized (aa.class)
        {
          bool2 = PhoneNumberUtils.isEmergencyNumber(paramString1);
          if (bool2) {
            return paramString1;
          }
        }
      }
      catch (Exception localException)
      {
        AssertionUtil.reportThrowableButNeverCrash(localException);
        ??? = c(paramString2);
        boolean bool2 = TextUtils.isEmpty((CharSequence)???);
        if (!bool2)
        {
          paramString2 = com.google.c.a.k.a();
          n localn = n.a();
          try
          {
            ??? = paramString2.a(paramString1, (String)???);
            boolean bool3 = paramString2.e((m.a)???);
            if (!bool3) {
              return paramString1;
            }
            bool2 = localn.a((m.a)???);
            if (bool2) {
              return paramString1;
            }
            return paramString2.a((m.a)???, paramc);
          }
          catch (IllegalStateException localIllegalStateException)
          {
            return paramString1;
          }
        }
        paramString1 = new com/google/c/a/g;
        paramc = g.a.a;
        paramString2 = String.valueOf(paramString2);
        paramString2 = "Bad country ISO code, ".concat(paramString2);
        paramString1.<init>(paramc, paramString2);
        throw paramString1;
      }
      throw paramString1;
    }
    paramString2 = new com/google/c/a/g;
    paramc = g.a.b;
    paramString2.<init>(paramc, paramString1);
    throw paramString2;
  }
  
  public static String b(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool) {
      try
      {
        return a(paramString);
      }
      catch (g localg) {}
    }
    return paramString;
  }
  
  public static String b(String paramString1, String paramString2)
  {
    boolean bool = TextUtils.isEmpty(paramString1);
    if (!bool) {
      try
      {
        k.c localc = k.c.a;
        return a(paramString1, paramString2, localc);
      }
      catch (g localg) {}
    }
    return paramString1;
  }
  
  public static String c(String paramString)
  {
    paramString = f(paramString);
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (bool1)
    {
      paramString = a.F();
      String str = f(paramString.H());
      boolean bool2 = TextUtils.isEmpty(str);
      if (bool2) {
        str = f(k.f(paramString));
      }
      bool2 = TextUtils.isEmpty(str);
      if (bool2)
      {
        bool2 = paramString.o();
        if (!bool2) {
          return f(getResourcesgetConfigurationlocale.getCountry());
        }
      }
      paramString = str;
    }
    return paramString;
  }
  
  public static String c(String paramString1, String paramString2)
  {
    boolean bool = TextUtils.isEmpty(paramString2);
    if (bool) {
      paramString2 = b(paramString1);
    } else {
      paramString2 = b(paramString1, paramString2);
    }
    bool = TextUtils.isEmpty(paramString2);
    if (!bool) {
      paramString1 = paramString2;
    }
    paramString2 = "+";
    int i = paramString1.startsWith(paramString2);
    if (i != 0)
    {
      i = 1;
      paramString1 = paramString1.substring(i);
    }
    return paramString1;
  }
  
  public static String d(String paramString)
  {
    try
    {
      Object localObject = a.F();
      localObject = ((a)localObject).H();
      k.c localc = k.c.b;
      return a(paramString, (String)localObject, localc);
    }
    catch (g localg) {}
    return paramString;
  }
  
  public static String d(String paramString1, String paramString2)
  {
    boolean bool1 = TextUtils.isEmpty(paramString2);
    if (bool1) {
      paramString2 = a.F().H();
    }
    try
    {
      bool1 = TextUtils.isEmpty(paramString2);
      if (!bool1)
      {
        localObject = com.google.c.a.k.a();
        localObject = ((com.google.c.a.k)localObject).a(paramString1, paramString2);
        int i = b;
        localObject = String.valueOf(i);
        boolean bool2 = paramString2.equals(localObject);
        if (bool2)
        {
          localObject = k.c.c;
          return a(paramString1, paramString2, (k.c)localObject);
        }
        localObject = k.c.b;
        return a(paramString1, paramString2, (k.c)localObject);
      }
      Object localObject = new com/google/c/a/g;
      g.a locala = g.a.a;
      String str = "Bad country ISO code, ";
      paramString2 = String.valueOf(paramString2);
      paramString2 = str.concat(paramString2);
      ((g)localObject).<init>(locala, paramString2);
      throw ((Throwable)localObject);
    }
    catch (g localg)
    {
      for (;;) {}
    }
    return paramString1;
  }
  
  public static String e(String paramString)
  {
    return d(paramString, null);
  }
  
  private static String f(String paramString)
  {
    boolean bool = am.d(paramString);
    if (!bool)
    {
      int i = paramString.length();
      int j = 2;
      if (i == j)
      {
        Locale localLocale = Locale.ENGLISH;
        return am.c(paramString, localLocale);
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */