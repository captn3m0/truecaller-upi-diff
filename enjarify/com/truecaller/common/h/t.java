package com.truecaller.common.h;

import android.text.TextUtils;
import com.truecaller.common.account.r;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class t
{
  private static final Pattern a = Pattern.compile("(^|\\s)\\+?\\d([ -]?\\d){5,}");
  private final String b;
  
  public t(r paramr)
  {
    paramr = paramr.a();
    b = paramr;
  }
  
  public t(String paramString)
  {
    b = paramString;
  }
  
  public final List a(String paramString)
  {
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (!bool1)
    {
      Object localObject = b;
      if (localObject != null)
      {
        paramString = paramString.replace("‪", "").replace("‬", "");
        String str1 = "";
        paramString = paramString.replace("tel:", str1);
        paramString = a.matcher(paramString);
        localObject = new java/util/ArrayList;
        int i = paramString.groupCount();
        ((ArrayList)localObject).<init>(i);
        for (;;)
        {
          boolean bool2 = paramString.find();
          if (!bool2) {
            break;
          }
          str1 = paramString.group().trim();
          String str2 = b;
          boolean bool3 = ab.b(str1, str2);
          if (bool3) {
            ((List)localObject).add(str1);
          }
        }
        return (List)localObject;
      }
    }
    return Collections.emptyList();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */