package com.truecaller.common.h;

import c.n.m;
import com.google.c.a.m.a;
import com.truecaller.common.account.r;
import com.truecaller.common.c;
import com.truecaller.common.network.KnownDomain;
import java.util.Collection;
import java.util.Iterator;

public final class ad
  implements ac
{
  private final com.truecaller.common.g.a a;
  private final dagger.a b;
  
  public ad(com.truecaller.common.g.a parama, dagger.a parama1)
  {
    a = parama;
    b = parama1;
  }
  
  public final boolean a()
  {
    Object localObject = a;
    String str = "featureRegion1_qa";
    boolean bool1 = ((com.truecaller.common.g.a)localObject).e(str);
    if (bool1) {
      return a.b("featureRegion1_qa");
    }
    localObject = a;
    str = "key_region_1_timestamp";
    long l1 = 0L;
    long l2 = ((com.truecaller.common.g.a)localObject).a(str, l1);
    boolean bool2 = l2 < l1;
    if (bool2) {
      return a.b("featureRegion1");
    }
    localObject = ((r)b.get()).a();
    if (localObject == null)
    {
      localObject = a;
      str = "profileCountryIso";
      localObject = ((com.truecaller.common.g.a)localObject).a(str);
    }
    if (localObject != null) {
      return b((String)localObject);
    }
    return true;
  }
  
  public final boolean a(String paramString)
  {
    c.g.b.k.b(paramString, "normalizedNumber");
    com.google.c.a.k localk = com.google.c.a.k.a();
    String str = null;
    try
    {
      paramString = (CharSequence)paramString;
      paramString = localk.a(paramString, null);
      int i = b;
      paramString = localk.b(i);
      if (paramString != null)
      {
        boolean bool = b(paramString);
        paramString = Boolean.valueOf(bool);
        str = paramString;
      }
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    if (str != null) {
      return str.booleanValue();
    }
    return true;
  }
  
  public final KnownDomain b()
  {
    boolean bool = a();
    if (bool) {
      return KnownDomain.DOMAIN_REGION_1;
    }
    return KnownDomain.DOMAIN_OTHER_REGIONS;
  }
  
  public final boolean b(String paramString)
  {
    c.g.b.k.b(paramString, "countryIso");
    Object localObject1 = (Iterable)c.a();
    boolean bool1 = localObject1 instanceof Collection;
    Object localObject2;
    if (bool1)
    {
      localObject2 = localObject1;
      localObject2 = (Collection)localObject1;
      bool1 = ((Collection)localObject2).isEmpty();
      if (bool1) {}
    }
    else
    {
      localObject1 = ((Iterable)localObject1).iterator();
      boolean bool2;
      do
      {
        bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject2 = (String)((Iterator)localObject1).next();
        bool2 = true;
        bool1 = m.a((String)localObject2, paramString, bool2);
      } while (!bool1);
      return bool2;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */