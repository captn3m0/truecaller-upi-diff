package com.truecaller.common.h;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Build.VERSION;
import android.util.TypedValue;
import com.truecaller.common.e.f;

public class l
{
  private static final android.support.v4.e.a a = ;
  
  public static int a(Context paramContext)
  {
    return (int)paramContext.getResources().getDimension(2131165925);
  }
  
  public static int a(Context paramContext, float paramFloat)
  {
    paramContext = paramContext.getResources().getDisplayMetrics();
    return (int)TypedValue.applyDimension(1, paramFloat, paramContext);
  }
  
  public static Bitmap a(Drawable paramDrawable)
  {
    int i = 0;
    Bitmap localBitmap = null;
    if (paramDrawable == null) {
      return null;
    }
    boolean bool = paramDrawable instanceof BitmapDrawable;
    if (bool) {
      return ((BitmapDrawable)paramDrawable).getBitmap();
    }
    bool = paramDrawable instanceof ShapeDrawable;
    int k = 1;
    if (!bool)
    {
      bool = paramDrawable instanceof GradientDrawable;
      if (!bool)
      {
        int j = paramDrawable.getIntrinsicWidth();
        if (j > 0)
        {
          j = paramDrawable.getIntrinsicHeight();
          if (j > 0)
          {
            k = paramDrawable.getIntrinsicWidth();
            i = paramDrawable.getIntrinsicHeight();
            break label90;
          }
        }
        return null;
      }
    }
    i = 1;
    label90:
    Object localObject = Bitmap.Config.ARGB_8888;
    localBitmap = Bitmap.createBitmap(k, i, (Bitmap.Config)localObject);
    localObject = new android/graphics/Canvas;
    ((Canvas)localObject).<init>(localBitmap);
    k = ((Canvas)localObject).getWidth();
    int m = ((Canvas)localObject).getHeight();
    ((Drawable)paramDrawable).setBounds(0, 0, k, m);
    ((Drawable)paramDrawable).draw((Canvas)localObject);
    return localBitmap;
  }
  
  public static Drawable a(Context paramContext, int paramInt)
  {
    Resources localResources = paramContext.getResources();
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i >= j)
    {
      paramContext = paramContext.getTheme();
      return localResources.getDrawable(paramInt, paramContext);
    }
    return localResources.getDrawable(paramInt);
  }
  
  public static Drawable a(Context paramContext, int paramInt1, int paramInt2)
  {
    paramContext = android.support.v4.graphics.drawable.a.e(a(paramContext, paramInt1).mutate());
    android.support.v4.graphics.drawable.a.a(paramContext, paramInt2);
    return paramContext;
  }
  
  public static String a(CharSequence paramCharSequence)
  {
    boolean bool = am.b(paramCharSequence);
    if (bool) {
      return "";
    }
    bool = f.b();
    if (!bool) {
      return paramCharSequence.toString();
    }
    android.support.v4.e.a locala = a;
    paramCharSequence = paramCharSequence.toString();
    return locala.a(paramCharSequence);
  }
  
  public static int b(Context paramContext, float paramFloat)
  {
    paramContext = paramContext.getResources().getDisplayMetrics();
    return (int)TypedValue.applyDimension(2, paramFloat, paramContext);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */