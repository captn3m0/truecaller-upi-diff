package com.truecaller.common.h;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader.TileMode;
import com.d.b.ai;

public final class aq$a
  implements ai
{
  private final int a;
  private final float b;
  
  public aq$a(int paramInt)
  {
    this(paramInt, (byte)0);
  }
  
  private aq$a(int paramInt, byte paramByte)
  {
    a = paramInt;
    b = -1.0F;
  }
  
  public final Bitmap a(Bitmap paramBitmap)
  {
    int i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    Object localObject = Bitmap.Config.ARGB_8888;
    Bitmap localBitmap = Bitmap.createBitmap(i, j, (Bitmap.Config)localObject);
    Canvas localCanvas = new android/graphics/Canvas;
    localCanvas.<init>(localBitmap);
    localObject = new android/graphics/Paint;
    ((Paint)localObject).<init>();
    float f1 = b;
    LinearGradient localLinearGradient = null;
    boolean bool = f1 < 0.0F;
    if (!bool)
    {
      int k = localBitmap.getHeight();
      f1 = k;
    }
    localCanvas.drawBitmap(paramBitmap, 0.0F, 0.0F, (Paint)localObject);
    localLinearGradient = new android/graphics/LinearGradient;
    float f2 = localBitmap.getHeight() - f1;
    float f3 = localBitmap.getHeight();
    int m = a;
    Shader.TileMode localTileMode = Shader.TileMode.CLAMP;
    localLinearGradient.<init>(0.0F, f2, 0.0F, f3, 0, m, localTileMode);
    ((Paint)localObject).setShader(localLinearGradient);
    localCanvas.drawPaint((Paint)localObject);
    paramBitmap.recycle();
    return localBitmap;
  }
  
  public final String a()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = getClass().getSimpleName();
    localStringBuilder.append(str);
    localStringBuilder.append(" ");
    str = Integer.toHexString(a);
    localStringBuilder.append(str);
    float f = b;
    localStringBuilder.append(f);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.aq.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */