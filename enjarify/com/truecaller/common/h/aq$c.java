package com.truecaller.common.h;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;

public final class aq$c
  extends aq.d
{
  private final float a;
  private final float b;
  private final float c;
  private final float d;
  private final boolean e;
  
  public aq$c(float paramFloat)
  {
    this(paramFloat, paramFloat, paramFloat, paramFloat);
  }
  
  public aq$c(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    a = paramFloat1;
    b = paramFloat2;
    c = paramFloat3;
    d = paramFloat4;
    boolean bool = paramFloat1 < paramFloat2;
    if (!bool)
    {
      bool = paramFloat2 < paramFloat3;
      if (!bool)
      {
        bool = paramFloat3 < paramFloat4;
        if (!bool)
        {
          bool = true;
          paramFloat1 = Float.MIN_VALUE;
          break label70;
        }
      }
    }
    bool = false;
    paramFloat1 = 0.0F;
    label70:
    e = bool;
  }
  
  public final String a()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = super.a();
    localStringBuilder.append(str);
    float f = a;
    localStringBuilder.append(f);
    localStringBuilder.append("x");
    f = b;
    localStringBuilder.append(f);
    localStringBuilder.append("x");
    f = c;
    localStringBuilder.append(f);
    localStringBuilder.append("x");
    f = d;
    localStringBuilder.append(f);
    return localStringBuilder.toString();
  }
  
  final void a(Canvas paramCanvas, Bitmap paramBitmap, Paint paramPaint, RectF paramRectF)
  {
    Object localObject1 = new android/graphics/BitmapShader;
    Object localObject2 = Shader.TileMode.CLAMP;
    ((BitmapShader)localObject1).<init>(paramBitmap, (Shader.TileMode)localObject2, (Shader.TileMode)localObject2);
    paramPaint.setShader((Shader)localObject1);
    boolean bool = e;
    if (bool)
    {
      float f1 = a;
      paramCanvas.drawRoundRect(paramRectF, f1, f1, paramPaint);
      return;
    }
    paramBitmap = new float[8];
    float f2 = a;
    paramBitmap[0] = f2;
    float f3 = paramBitmap[0];
    paramBitmap[1] = f3;
    f2 = b;
    int i = 2;
    paramBitmap[i] = f2;
    f3 = paramBitmap[i];
    paramBitmap[3] = f3;
    f2 = c;
    i = 4;
    paramBitmap[i] = f2;
    f3 = paramBitmap[i];
    paramBitmap[5] = f3;
    f2 = d;
    i = 6;
    paramBitmap[i] = f2;
    f3 = paramBitmap[i];
    paramBitmap[7] = f3;
    localObject1 = new android/graphics/Path;
    ((Path)localObject1).<init>();
    localObject2 = Path.Direction.CW;
    ((Path)localObject1).addRoundRect(paramRectF, paramBitmap, (Path.Direction)localObject2);
    paramCanvas.drawPath((Path)localObject1, paramPaint);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.aq.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */