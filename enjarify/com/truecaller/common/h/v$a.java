package com.truecaller.common.h;

import c.g.b.k;
import com.google.c.a.m.a;

final class v$a
{
  final m.a a;
  final boolean b;
  
  public v$a(m.a parama, boolean paramBoolean)
  {
    a = parama;
    b = paramBoolean;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof a;
      if (bool2)
      {
        paramObject = (a)paramObject;
        m.a locala1 = a;
        m.a locala2 = a;
        bool2 = k.a(locala1, locala2);
        if (bool2)
        {
          bool2 = b;
          boolean bool3 = b;
          if (bool2 == bool3)
          {
            bool3 = true;
          }
          else
          {
            bool3 = false;
            paramObject = null;
          }
          if (bool3) {
            return bool1;
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    m.a locala = a;
    int i;
    if (locala != null)
    {
      i = locala.hashCode();
    }
    else
    {
      i = 0;
      locala = null;
    }
    i *= 31;
    int j = b;
    if (j != 0) {
      j = 1;
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Result(phoneNumber=");
    m.a locala = a;
    localStringBuilder.append(locala);
    localStringBuilder.append(", isValidNumber=");
    boolean bool = b;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.v.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */