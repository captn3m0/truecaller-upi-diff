package com.truecaller.common.h;

import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;
import com.google.gson.f;
import com.truecaller.common.network.country.CountryListDto;
import com.truecaller.common.network.country.CountryListDto.a;
import com.truecaller.common.network.country.CountryListDto.b;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.log.d;
import e.b;
import e.r;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import okhttp3.ad;

public final class h
{
  private static final String[] a;
  private static final String[] b;
  private static final Map c;
  private static final List d;
  private static final ReentrantLock e;
  private static final Condition f;
  private static volatile CountryListDto g;
  private static Map h;
  private static Map i;
  private static Map j;
  private static Map k;
  
  static
  {
    String[] tmp5_2 = new String[38];
    String[] tmp6_5 = tmp5_2;
    String[] tmp6_5 = tmp5_2;
    tmp6_5[0] = "1403";
    tmp6_5[1] = "1587";
    String[] tmp15_6 = tmp6_5;
    String[] tmp15_6 = tmp6_5;
    tmp15_6[2] = "1780";
    tmp15_6[3] = "1825";
    String[] tmp24_15 = tmp15_6;
    String[] tmp24_15 = tmp15_6;
    tmp24_15[4] = "1236";
    tmp24_15[5] = "1250";
    String[] tmp33_24 = tmp24_15;
    String[] tmp33_24 = tmp24_15;
    tmp33_24[6] = "1604";
    tmp33_24[7] = "1672";
    String[] tmp44_33 = tmp33_24;
    String[] tmp44_33 = tmp33_24;
    tmp44_33[8] = "1778";
    tmp44_33[9] = "1204";
    String[] tmp55_44 = tmp44_33;
    String[] tmp55_44 = tmp44_33;
    tmp55_44[10] = "1431";
    tmp55_44[11] = "1506";
    String[] tmp66_55 = tmp55_44;
    String[] tmp66_55 = tmp55_44;
    tmp66_55[12] = "1709";
    tmp66_55[13] = "1902";
    String[] tmp77_66 = tmp66_55;
    String[] tmp77_66 = tmp66_55;
    tmp77_66[14] = "1226";
    tmp77_66[15] = "1249";
    String[] tmp88_77 = tmp77_66;
    String[] tmp88_77 = tmp77_66;
    tmp88_77[16] = "1289";
    tmp88_77[17] = "1343";
    String[] tmp99_88 = tmp88_77;
    String[] tmp99_88 = tmp88_77;
    tmp99_88[18] = "1365";
    tmp99_88[19] = "1416";
    String[] tmp110_99 = tmp99_88;
    String[] tmp110_99 = tmp99_88;
    tmp110_99[20] = "1437";
    tmp110_99[21] = "1519";
    String[] tmp121_110 = tmp110_99;
    String[] tmp121_110 = tmp110_99;
    tmp121_110[22] = "1613";
    tmp121_110[23] = "1647";
    String[] tmp132_121 = tmp121_110;
    String[] tmp132_121 = tmp121_110;
    tmp132_121[24] = "1705";
    tmp132_121[25] = "1807";
    String[] tmp143_132 = tmp132_121;
    String[] tmp143_132 = tmp132_121;
    tmp143_132[26] = "1905";
    tmp143_132[27] = "1418";
    String[] tmp154_143 = tmp143_132;
    String[] tmp154_143 = tmp143_132;
    tmp154_143[28] = "1438";
    tmp154_143[29] = "1450";
    String[] tmp165_154 = tmp154_143;
    String[] tmp165_154 = tmp154_143;
    tmp165_154[30] = "1514";
    tmp165_154[31] = "1579";
    String[] tmp176_165 = tmp165_154;
    String[] tmp176_165 = tmp165_154;
    tmp176_165[32] = "1581";
    tmp176_165[33] = "1819";
    String[] tmp187_176 = tmp176_165;
    String[] tmp187_176 = tmp176_165;
    tmp187_176[34] = "1873";
    tmp187_176[35] = "1306";
    tmp187_176[36] = "1639";
    String[] tmp203_187 = tmp187_176;
    tmp203_187[37] = "1867";
    a = tmp203_187;
    String[] tmp216_213 = new String[3];
    String[] tmp217_216 = tmp216_213;
    String[] tmp217_216 = tmp216_213;
    tmp217_216[0] = "733622";
    tmp217_216[1] = "76";
    tmp217_216[2] = "77";
    b = tmp217_216;
    String[] tmp237_234 = new String[3];
    String[] tmp238_237 = tmp237_234;
    String[] tmp238_237 = tmp237_234;
    tmp238_237[0] = "tw";
    tmp238_237[1] = "hk";
    tmp238_237[2] = "mo";
    d = Arrays.asList(tmp238_237);
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    c = (Map)localObject;
    String[] arrayOfString = a;
    ((Map)localObject).put("ca", arrayOfString);
    localObject = c;
    arrayOfString = b;
    ((Map)localObject).put("kz", arrayOfString);
    localObject = new java/util/concurrent/locks/ReentrantLock;
    ((ReentrantLock)localObject).<init>();
    e = (ReentrantLock)localObject;
    f = ((ReentrantLock)localObject).newCondition();
  }
  
  public static CountryListDto.a a(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    Object localObject = e;
    ((ReentrantLock)localObject).lock();
    try
    {
      localObject = h;
      if (localObject != null) {}
    }
    finally
    {
      label32:
      ReentrantLock localReentrantLock;
      e.unlock();
    }
    try
    {
      localObject = f;
      ((Condition)localObject).await();
    }
    catch (InterruptedException localInterruptedException)
    {
      break label32;
    }
    localObject = h;
    localReentrantLock = e;
    localReentrantLock.unlock();
    if (localObject != null)
    {
      paramString = paramString.toLowerCase();
      return (CountryListDto.a)((Map)localObject).get(paramString);
    }
    return null;
  }
  
  private static Object a(Context paramContext, String paramString, Type paramType)
  {
    Object localObject1 = new java/io/File;
    Object localObject3 = paramContext.getFilesDir();
    ((File)localObject1).<init>((File)localObject3, paramString);
    boolean bool = ((File)localObject1).exists();
    if (bool) {
      try
      {
        localObject3 = new java/io/FileReader;
        ((FileReader)localObject3).<init>((File)localObject1);
        try
        {
          localObject1 = new com/google/gson/f;
          ((f)localObject1).<init>();
          localObject1 = ((f)localObject1).a((Reader)localObject3, paramType);
          return localObject1;
        }
        finally
        {
          ((Reader)localObject3).close();
        }
        return b(paramContext, paramString, paramType);
      }
      catch (Exception localException)
      {
        localObject3 = "Failed to read country list from file";
        d.a(localException, (String)localObject3);
      }
    }
  }
  
  public static List a()
  {
    CountryListDto localCountryListDto = c();
    boolean bool = a(localCountryListDto);
    if (!bool) {
      return Collections.emptyList();
    }
    return countryList.b;
  }
  
  private static List a(List paramList)
  {
    Object localObject1 = "HUAWEI_STORE";
    Object localObject2 = com.truecaller.common.b.a.F().e().f();
    boolean bool1 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
    if (bool1)
    {
      localObject1 = paramList.iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        localObject2 = (CountryListDto.a)((Iterator)localObject1).next();
        List localList = d;
        localObject2 = c;
        bool2 = localList.contains(localObject2);
        if (bool2) {
          ((Iterator)localObject1).remove();
        }
      }
    }
    return Collections.unmodifiableList(paramList);
  }
  
  public static void a(Context paramContext)
  {
    Object localObject1 = e;
    ((ReentrantLock)localObject1).lock();
    try
    {
      localObject1 = g;
      int m = 1;
      Object localObject2 = null;
      boolean bool1;
      if (localObject1 == null)
      {
        bool1 = true;
      }
      else
      {
        bool1 = false;
        localObject1 = null;
      }
      Object localObject3 = new String[0];
      AssertionUtil.OnlyInDebug.isTrue(bool1, (String[])localObject3);
      localObject1 = "countries.json";
      localObject3 = CountryListDto.class;
      localObject1 = a(paramContext, (String)localObject1, (Type)localObject3);
      localObject1 = (CountryListDto)localObject1;
      localObject3 = "countries_languages.json";
      Object localObject4 = new com/truecaller/common/h/h$1;
      ((h.1)localObject4).<init>();
      localObject4 = b;
      localObject3 = a(paramContext, (String)localObject3, (Type)localObject4);
      localObject3 = (Map)localObject3;
      k = (Map)localObject3;
      boolean bool2;
      if (localObject1 != null)
      {
        bool2 = true;
      }
      else
      {
        bool2 = false;
        localObject3 = null;
      }
      localObject4 = new String[0];
      AssertionUtil.isTrue(bool2, (String[])localObject4);
      b((CountryListDto)localObject1);
      if (localObject1 != null)
      {
        localObject3 = countryList;
        if (localObject3 != null)
        {
          localObject3 = countryList;
          localObject3 = b;
          if (localObject3 != null)
          {
            localObject3 = countryList;
            localObject4 = countryList;
            localObject4 = b;
            localObject4 = a((List)localObject4);
            b = ((List)localObject4);
          }
          localObject3 = countryList;
          localObject3 = a;
          if (localObject3 == null)
          {
            localObject3 = k.g(paramContext);
            boolean bool3 = TextUtils.isEmpty((CharSequence)localObject3);
            if (!bool3)
            {
              localObject3 = a((String)localObject3);
              if (localObject3 != null)
              {
                localObject2 = countryList;
                a = ((CountryListDto.a)localObject3);
                break label272;
              }
            }
          }
        }
      }
      m = 0;
      label272:
      g = (CountryListDto)localObject1;
      localObject2 = f;
      ((Condition)localObject2).signalAll();
      localObject2 = e;
      ((ReentrantLock)localObject2).unlock();
      if (m != 0) {
        a(paramContext, (CountryListDto)localObject1);
      }
      return;
    }
    finally
    {
      e.unlock();
    }
  }
  
  private static void a(Context paramContext, CountryListDto paramCountryListDto)
  {
    Object localObject = new java/io/File;
    paramContext = paramContext.getFilesDir();
    String str = "countries.json";
    ((File)localObject).<init>(paramContext, str);
    try
    {
      paramContext = new java/io/FileWriter;
      paramContext.<init>((File)localObject);
      try
      {
        localObject = new com/google/gson/f;
        ((f)localObject).<init>();
        ((f)localObject).a(paramCountryListDto, paramContext);
        return;
      }
      finally
      {
        paramContext.close();
      }
      return;
    }
    catch (Exception localException)
    {
      d.a(localException;
    }
  }
  
  private static boolean a(CountryListDto paramCountryListDto)
  {
    if (paramCountryListDto != null)
    {
      CountryListDto.b localb = countryList;
      if (localb != null)
      {
        paramCountryListDto = countryList.b;
        if (paramCountryListDto != null) {
          return true;
        }
      }
    }
    return false;
  }
  
  public static CountryListDto.a b()
  {
    CountryListDto localCountryListDto = c();
    boolean bool = a(localCountryListDto);
    if (!bool) {
      return null;
    }
    return countryList.a;
  }
  
  public static CountryListDto.a b(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    Object localObject = e;
    ((ReentrantLock)localObject).lock();
    try
    {
      localObject = i;
      if (localObject != null) {}
    }
    finally
    {
      label32:
      ReentrantLock localReentrantLock;
      e.unlock();
    }
    try
    {
      localObject = f;
      ((Condition)localObject).await();
    }
    catch (InterruptedException localInterruptedException)
    {
      break label32;
    }
    localObject = i;
    localReentrantLock = e;
    localReentrantLock.unlock();
    if (localObject != null)
    {
      paramString = am.m(am.i(paramString));
      return (CountryListDto.a)((Map)localObject).get(paramString);
    }
    return null;
  }
  
  private static Object b(Context paramContext, String paramString, Type paramType)
  {
    try
    {
      paramContext = paramContext.getAssets();
      paramContext = paramContext.open(paramString);
      try
      {
        paramString = new com/google/gson/f;
        paramString.<init>();
        InputStreamReader localInputStreamReader = new java/io/InputStreamReader;
        localInputStreamReader.<init>(paramContext);
        paramString = paramString.a(localInputStreamReader, paramType);
        return paramString;
      }
      finally
      {
        paramContext.close();
      }
      return null;
    }
    catch (Exception localException)
    {
      d.a(localException, "Failed to read countries from assets");
    }
  }
  
  public static void b(Context paramContext)
  {
    Object localObject1 = c();
    if (localObject1 == null) {
      localObject1 = "";
    }
    try
    {
      localObject1 = countryListChecksum;
      localObject1 = com.truecaller.common.network.country.a.a((String)localObject1);
      localObject1 = ((b)localObject1).c();
      Object localObject2 = a;
      boolean bool1 = ((ad)localObject2).c();
      if (bool1)
      {
        localObject1 = b;
        localObject1 = (CountryListDto)localObject1;
        if (localObject1 != null)
        {
          localObject2 = countryList;
          if (localObject2 != null)
          {
            localObject2 = e;
            ((ReentrantLock)localObject2).lock();
            try
            {
              localObject2 = g;
              bool1 = a((CountryListDto)localObject2);
              boolean bool2 = true;
              Object localObject3;
              if (!bool1)
              {
                localObject2 = countryList;
                if (localObject2 != null)
                {
                  localObject2 = countryList;
                  localObject2 = b;
                  if (localObject2 != null)
                  {
                    localObject2 = countryList;
                    localObject3 = countryList;
                    localObject3 = b;
                    localObject3 = a((List)localObject3);
                    b = ((List)localObject3);
                  }
                }
                g = (CountryListDto)localObject1;
                bool1 = true;
              }
              else
              {
                localObject2 = countryList;
                localObject2 = b;
                localObject3 = null;
                Object localObject4;
                boolean bool3;
                if (localObject2 == null)
                {
                  localObject2 = countryList;
                  localObject4 = g;
                  localObject4 = countryList;
                  localObject4 = b;
                  b = ((List)localObject4);
                  bool1 = false;
                  localObject2 = null;
                  bool3 = false;
                  localObject4 = null;
                }
                else
                {
                  localObject2 = countryList;
                  localObject4 = countryList;
                  localObject4 = b;
                  localObject4 = a((List)localObject4);
                  b = ((List)localObject4);
                  bool1 = true;
                  bool3 = true;
                }
                Object localObject5 = countryList;
                localObject5 = a;
                if (localObject5 == null)
                {
                  localObject5 = countryList;
                  localObject6 = g;
                  localObject6 = countryList;
                  localObject6 = a;
                  a = ((CountryListDto.a)localObject6);
                }
                else
                {
                  bool1 = true;
                }
                localObject5 = countryListChecksum;
                Object localObject6 = g;
                localObject6 = countryListChecksum;
                boolean bool4 = TextUtils.equals((CharSequence)localObject5, (CharSequence)localObject6);
                if (bool4)
                {
                  bool2 = false;
                  localReentrantLock = null;
                }
                bool2 |= bool1;
                g = (CountryListDto)localObject1;
                bool1 = bool2;
                bool2 = bool3;
              }
              if (bool2) {
                b((CountryListDto)localObject1);
              }
              ReentrantLock localReentrantLock = e;
              localReentrantLock.unlock();
              if (bool1) {
                a(paramContext, (CountryListDto)localObject1);
              }
              return;
            }
            finally
            {
              e.unlock();
            }
          }
        }
      }
      paramContext = "Unable to update country list from network";
      new String[1][0] = paramContext;
      return;
    }
    catch (RuntimeException paramContext) {}catch (IOException paramContext) {}
    d.a(paramContext, "Unable to load countries from network");
  }
  
  private static void b(CountryListDto paramCountryListDto)
  {
    HashMap localHashMap1 = new java/util/HashMap;
    localHashMap1.<init>();
    HashMap localHashMap2 = new java/util/HashMap;
    localHashMap2.<init>();
    HashMap localHashMap3 = new java/util/HashMap;
    localHashMap3.<init>();
    if (paramCountryListDto != null)
    {
      Object localObject1 = countryList;
      if (localObject1 != null)
      {
        localObject1 = countryList.b;
        if (localObject1 != null)
        {
          paramCountryListDto = countryList.b.iterator();
          for (;;)
          {
            boolean bool1 = paramCountryListDto.hasNext();
            if (!bool1) {
              break;
            }
            localObject1 = (CountryListDto.a)paramCountryListDto.next();
            Object localObject2 = c;
            boolean bool2 = TextUtils.isEmpty((CharSequence)localObject2);
            if (!bool2)
            {
              localObject2 = c;
              Object localObject3 = Locale.ENGLISH;
              localObject2 = am.d((String)localObject2, (Locale)localObject3);
              localHashMap1.put(localObject2, localObject1);
              localObject3 = am.m(am.i(b));
              localHashMap2.put(localObject3, localObject1);
              localObject3 = c;
              localObject2 = (String[])((Map)localObject3).get(localObject2);
              if (localObject2 != null)
              {
                int m = localObject2.length;
                if (m > 0)
                {
                  m = localObject2.length;
                  int n = 0;
                  while (n < m)
                  {
                    Object localObject4 = localObject2[n];
                    localHashMap3.put(localObject4, localObject1);
                    n += 1;
                  }
                  continue;
                }
              }
              localObject2 = d;
              localHashMap3.put(localObject2, localObject1);
            }
          }
        }
      }
    }
    h = Collections.unmodifiableMap(localHashMap1);
    i = Collections.unmodifiableMap(localHashMap2);
    j = Collections.unmodifiableMap(localHashMap3);
  }
  
  public static CountryListDto.a c(Context paramContext)
  {
    return a(((com.truecaller.common.b.a)paramContext.getApplicationContext()).H());
  }
  
  public static CountryListDto.a c(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    Object localObject1 = e;
    ((ReentrantLock)localObject1).lock();
    try
    {
      localObject1 = j;
      if (localObject1 != null) {}
    }
    finally
    {
      label32:
      Object localObject2;
      String str;
      boolean bool1;
      int n;
      int i1;
      int m;
      int i3;
      int i2;
      e.unlock();
    }
    try
    {
      localObject1 = f;
      ((Condition)localObject1).await();
    }
    catch (InterruptedException localInterruptedException)
    {
      break label32;
    }
    localObject1 = j;
    e.unlock();
    localObject2 = "";
    str = "+";
    bool1 = paramString.startsWith(str);
    n = 1;
    if (bool1)
    {
      localObject2 = paramString.substring(n);
    }
    else
    {
      str = "00";
      bool1 = paramString.startsWith(str);
      if (bool1)
      {
        i1 = 2;
        localObject2 = paramString.substring(i1);
      }
    }
    m = ((String)localObject2).length();
    i3 = Math.min(6, m);
    m = 0;
    str = null;
    for (paramString = ((String)localObject2).substring(0, i3);; paramString = paramString.substring(0, i2))
    {
      boolean bool2 = TextUtils.isEmpty(paramString);
      if (bool2) {
        break;
      }
      localObject2 = (CountryListDto.a)((Map)localObject1).get(paramString);
      if (localObject2 != null) {
        return (CountryListDto.a)localObject2;
      }
      i2 = paramString.length() - n;
    }
    return null;
  }
  
  private static CountryListDto c()
  {
    Object localObject1 = e;
    ((ReentrantLock)localObject1).lock();
    try
    {
      localObject1 = g;
      if (localObject1 == null) {
        try
        {
          localObject1 = f;
          ((Condition)localObject1).await();
        }
        catch (InterruptedException localInterruptedException)
        {
          localObject1 = "Interrupted, while was waiting for countries";
          new String[1][0] = localObject1;
        }
      }
      localObject1 = g;
      return (CountryListDto)localObject1;
    }
    finally
    {
      e.unlock();
    }
  }
  
  public static List d(String paramString)
  {
    Map localMap = k;
    boolean bool = localMap.containsKey(paramString);
    if (bool) {
      return (List)k.get(paramString);
    }
    return (List)k.get("ZZ");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */