package com.truecaller.common.h;

import android.content.Context;
import android.telecom.TelecomManager;
import c.u;
import com.truecaller.common.a.a;

public final class f
  implements d
{
  private final TelecomManager a;
  private final Context b;
  private final a c;
  
  public f(Context paramContext, a parama)
  {
    b = paramContext;
    c = parama;
    paramContext = b;
    parama = "telecom";
    paramContext = paramContext.getSystemService(parama);
    if (paramContext != null)
    {
      paramContext = (TelecomManager)paramContext;
      a = paramContext;
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.telecom.TelecomManager");
    throw paramContext;
  }
  
  public final boolean a()
  {
    boolean bool;
    try
    {
      TelecomManager localTelecomManager = a;
      bool = localTelecomManager.endCall();
    }
    catch (Exception localException)
    {
      a locala = c;
      int i = 2;
      locala.a(i, localException);
      bool = false;
      Object localObject = null;
    }
    return bool;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */