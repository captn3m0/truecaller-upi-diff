package com.truecaller.common.h;

import android.content.Context;
import com.truecaller.common.b.a;
import com.truecaller.featuretoggles.e;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class ar
  implements d
{
  private final Provider a;
  
  public static e a(Context paramContext)
  {
    return (e)g.a(((a)paramContext.getApplicationContext()).f(), "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.ar
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */