package com.truecaller.common.h;

import android.content.Context;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public final class a
  implements Iterable
{
  private final Class a;
  private final ClassLoader b;
  private final Set c;
  
  private a(Context paramContext, Class paramClass, ClassLoader paramClassLoader)
  {
    a = paramClass;
    b = paramClassLoader;
    paramClass = new java/util/HashSet;
    paramClass.<init>();
    c = paramClass;
    a(paramContext);
  }
  
  public static a a(Context paramContext, Class paramClass, ClassLoader paramClassLoader)
  {
    a locala = new com/truecaller/common/h/a;
    paramContext = paramContext.getApplicationContext();
    locala.<init>(paramContext, paramClass, paramClassLoader);
    return locala;
  }
  
  /* Error */
  private void a(Context paramContext)
  {
    // Byte code:
    //   0: iconst_2
    //   1: istore_2
    //   2: aconst_null
    //   3: astore_3
    //   4: aload_1
    //   5: invokevirtual 42	android/content/Context:getAssets	()Landroid/content/res/AssetManager;
    //   8: astore_1
    //   9: new 44	java/lang/StringBuilder
    //   12: astore 4
    //   14: ldc 46
    //   16: astore 5
    //   18: aload 4
    //   20: aload 5
    //   22: invokespecial 49	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   25: aload_0
    //   26: getfield 18	com/truecaller/common/h/a:a	Ljava/lang/Class;
    //   29: astore 5
    //   31: aload 5
    //   33: invokevirtual 55	java/lang/Class:getName	()Ljava/lang/String;
    //   36: astore 5
    //   38: aload 4
    //   40: aload 5
    //   42: invokevirtual 59	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   45: pop
    //   46: aload 4
    //   48: invokevirtual 62	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   51: astore 4
    //   53: aload_1
    //   54: aload 4
    //   56: iload_2
    //   57: invokevirtual 68	android/content/res/AssetManager:open	(Ljava/lang/String;I)Ljava/io/InputStream;
    //   60: astore_3
    //   61: new 70	java/io/BufferedReader
    //   64: astore_1
    //   65: new 72	java/io/InputStreamReader
    //   68: astore 4
    //   70: aload 4
    //   72: aload_3
    //   73: invokespecial 75	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   76: aload_1
    //   77: aload 4
    //   79: invokespecial 78	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   82: aload_1
    //   83: invokevirtual 81	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   86: astore 4
    //   88: aload 4
    //   90: ifnull +34 -> 124
    //   93: aload 4
    //   95: invokestatic 87	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   98: istore 6
    //   100: iload 6
    //   102: ifne -20 -> 82
    //   105: aload_0
    //   106: getfield 25	com/truecaller/common/h/a:c	Ljava/util/Set;
    //   109: astore 5
    //   111: aload 5
    //   113: aload 4
    //   115: invokeinterface 93 2 0
    //   120: pop
    //   121: goto -39 -> 82
    //   124: aload_3
    //   125: ifnull +89 -> 214
    //   128: aload_3
    //   129: invokevirtual 98	java/io/InputStream:close	()V
    //   132: return
    //   133: astore_1
    //   134: goto +81 -> 215
    //   137: pop
    //   138: iload_2
    //   139: anewarray 100	java/lang/String
    //   142: astore_1
    //   143: iconst_0
    //   144: istore_2
    //   145: ldc 102
    //   147: astore 4
    //   149: aload_1
    //   150: iconst_0
    //   151: aload 4
    //   153: aastore
    //   154: iconst_1
    //   155: istore_2
    //   156: new 44	java/lang/StringBuilder
    //   159: astore 4
    //   161: ldc 105
    //   163: astore 5
    //   165: aload 4
    //   167: aload 5
    //   169: invokespecial 49	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   172: aload_0
    //   173: getfield 18	com/truecaller/common/h/a:a	Ljava/lang/Class;
    //   176: astore 5
    //   178: aload 5
    //   180: invokevirtual 55	java/lang/Class:getName	()Ljava/lang/String;
    //   183: astore 5
    //   185: aload 4
    //   187: aload 5
    //   189: invokevirtual 59	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   192: pop
    //   193: aload 4
    //   195: invokevirtual 62	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   198: astore 4
    //   200: aload_1
    //   201: iload_2
    //   202: aload 4
    //   204: aastore
    //   205: aload_3
    //   206: ifnull +8 -> 214
    //   209: aload_3
    //   210: invokevirtual 98	java/io/InputStream:close	()V
    //   213: return
    //   214: return
    //   215: aload_3
    //   216: ifnull +7 -> 223
    //   219: aload_3
    //   220: invokevirtual 98	java/io/InputStream:close	()V
    //   223: aload_1
    //   224: athrow
    //   225: pop
    //   226: goto -94 -> 132
    //   229: pop
    //   230: goto -17 -> 213
    //   233: pop
    //   234: goto -11 -> 223
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	237	0	this	a
    //   0	237	1	paramContext	Context
    //   1	201	2	i	int
    //   3	217	3	localInputStream	java.io.InputStream
    //   12	191	4	localObject1	Object
    //   16	172	5	localObject2	Object
    //   98	3	6	bool	boolean
    //   137	1	7	localIOException1	java.io.IOException
    //   225	1	8	localIOException2	java.io.IOException
    //   229	1	9	localIOException3	java.io.IOException
    //   233	1	10	localIOException4	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   4	8	133	finally
    //   9	12	133	finally
    //   20	25	133	finally
    //   25	29	133	finally
    //   31	36	133	finally
    //   40	46	133	finally
    //   46	51	133	finally
    //   56	60	133	finally
    //   61	64	133	finally
    //   65	68	133	finally
    //   72	76	133	finally
    //   77	82	133	finally
    //   82	86	133	finally
    //   93	98	133	finally
    //   105	109	133	finally
    //   113	121	133	finally
    //   138	142	133	finally
    //   151	154	133	finally
    //   156	159	133	finally
    //   167	172	133	finally
    //   172	176	133	finally
    //   178	183	133	finally
    //   187	193	133	finally
    //   193	198	133	finally
    //   202	205	133	finally
    //   4	8	137	java/io/IOException
    //   9	12	137	java/io/IOException
    //   20	25	137	java/io/IOException
    //   25	29	137	java/io/IOException
    //   31	36	137	java/io/IOException
    //   40	46	137	java/io/IOException
    //   46	51	137	java/io/IOException
    //   56	60	137	java/io/IOException
    //   61	64	137	java/io/IOException
    //   65	68	137	java/io/IOException
    //   72	76	137	java/io/IOException
    //   77	82	137	java/io/IOException
    //   82	86	137	java/io/IOException
    //   93	98	137	java/io/IOException
    //   105	109	137	java/io/IOException
    //   113	121	137	java/io/IOException
    //   128	132	225	java/io/IOException
    //   209	213	229	java/io/IOException
    //   219	223	233	java/io/IOException
  }
  
  public final Iterator iterator()
  {
    a.a locala = new com/truecaller/common/h/a$a;
    locala.<init>(this, (byte)0);
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */