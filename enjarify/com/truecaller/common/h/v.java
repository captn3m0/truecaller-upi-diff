package com.truecaller.common.h;

import android.text.TextUtils;
import c.g.a.b;
import c.m.i;
import c.m.l;
import com.google.c.a.g;
import com.google.c.a.k.c;
import com.google.c.a.k.d;
import com.google.c.a.m.a;
import com.google.c.a.n;
import com.truecaller.common.account.r;
import com.truecaller.multisim.h;
import com.truecaller.utils.extensions.c;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;

public final class v
  implements u
{
  private final com.google.c.a.k a;
  private final h b;
  private final r c;
  private final aj d;
  
  public v(com.google.c.a.k paramk, h paramh, r paramr, aj paramaj)
  {
    a = paramk;
    b = paramh;
    c = paramr;
    d = paramaj;
  }
  
  private final String a(String paramString1, k.c paramc, String paramString2, String paramString3, boolean paramBoolean)
  {
    paramString1 = b(paramString1, paramString2, paramString3);
    paramString2 = null;
    if (paramString1 != null)
    {
      if (paramBoolean)
      {
        boolean bool = b;
        if (!bool) {
          return null;
        }
      }
      paramString2 = a;
      paramString1 = a;
      return paramString2.a(paramString1, paramc);
    }
    return null;
  }
  
  private final v.a d(String paramString1, String paramString2)
  {
    try
    {
      com.google.c.a.k localk = a;
      paramString1 = (CharSequence)paramString1;
      Locale localLocale = Locale.ENGLISH;
      paramString2 = org.c.a.a.a.k.c(paramString2, localLocale);
      paramString1 = localk.a(paramString1, paramString2);
      paramString2 = new com/truecaller/common/h/v$a;
      localk = a;
      boolean bool = localk.c(paramString1);
      paramString2.<init>(paramString1, bool);
    }
    catch (g localg)
    {
      paramString2 = null;
    }
    return paramString2;
  }
  
  public final String a()
  {
    String str = b.f();
    c.g.b.k.a(str, "multiSimManager.defaultSimToken");
    return str;
  }
  
  public final String a(String paramString)
  {
    c.g.b.k.b(paramString, "simToken");
    r localr = c;
    String str1 = localr.b();
    if (str1 != null)
    {
      k.c localc = k.c.a;
      String str2 = b();
      return a(this, str1, localc, str2, paramString, false, 8);
    }
    return null;
  }
  
  public final String a(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "number");
    k.c localc = k.c.c;
    return a(this, paramString1, localc, paramString2, null, false, 12);
  }
  
  public final String a(String paramString1, String paramString2, String paramString3)
  {
    c.g.b.k.b(paramString1, "number");
    c.g.b.k.b(paramString2, "simToken");
    k.c localc = k.c.a;
    return a(this, paramString1, localc, paramString3, paramString2, false, 8);
  }
  
  public final Collection a(Collection paramCollection)
  {
    c.g.b.k.b(paramCollection, "numbers");
    paramCollection = c.a.m.n((Iterable)paramCollection);
    Object localObject = (b)v.c.a;
    paramCollection = l.a(paramCollection, (b)localObject);
    localObject = new com/truecaller/common/h/v$d;
    ((v.d)localObject).<init>(this);
    localObject = (b)localObject;
    return (Collection)l.d(l.d(paramCollection, (b)localObject));
  }
  
  final v.a b(String paramString1, String paramString2, String paramString3)
  {
    if (paramString3 == null) {
      paramString3 = a();
    }
    Object localObject1 = b.d(paramString3);
    paramString3 = b.e(paramString3);
    Object localObject2 = b();
    int i = 4;
    Object localObject3 = new String[i];
    localObject3[0] = paramString2;
    int j = 1;
    localObject3[j] = paramString3;
    localObject3[2] = localObject1;
    int k = 3;
    localObject3[k] = localObject2;
    paramString3 = l.c(l.a((Object[])localObject3));
    localObject1 = (b)v.b.a;
    paramString3 = l.a(paramString3, (b)localObject1).a();
    localObject1 = null;
    localObject2 = null;
    for (;;)
    {
      boolean bool1 = paramString3.hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = (String)paramString3.next();
      Boolean localBoolean;
      if (localObject2 != null)
      {
        bool2 = b;
        localBoolean = Boolean.valueOf(bool2);
      }
      else
      {
        bool2 = false;
        localBoolean = null;
      }
      boolean bool2 = c.a(localBoolean);
      if (!bool2)
      {
        localObject3 = d(paramString1, (String)localObject3);
        if (localObject3 != null)
        {
          bool2 = b;
          if ((!bool2) && (localObject2 != null))
          {
            bool2 = false;
            localBoolean = null;
          }
          else
          {
            bool2 = true;
          }
          if (!bool2)
          {
            bool1 = false;
            localObject3 = null;
          }
          if (localObject3 != null) {
            localObject2 = localObject3;
          }
        }
      }
    }
    return (v.a)localObject2;
  }
  
  public final String b()
  {
    return c.a();
  }
  
  public final String b(String paramString)
  {
    c.g.b.k.b(paramString, "number");
    k.c localc = k.c.a;
    return a(this, paramString, localc, null, null, false, 14);
  }
  
  public final String b(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "number");
    c.g.b.k.b(paramString2, "simToken");
    k.c localc = k.c.a;
    return a(this, paramString1, localc, null, paramString2, false, 10);
  }
  
  public final m.a c(String paramString)
  {
    c.g.b.k.b(paramString, "number");
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    boolean bool = c.n.m.a((CharSequence)localObject);
    if (bool) {
      return null;
    }
    paramString = b(paramString, null, null);
    if (paramString != null) {
      return a;
    }
    return null;
  }
  
  public final String c(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "number");
    c.g.b.k.b(paramString2, "simToken");
    k.c localc = k.c.a;
    return a(this, paramString1, localc, null, paramString2, true, 2);
  }
  
  public final int d(String paramString)
  {
    c.g.b.k.b(paramString, "numberStr");
    Object localObject1 = k.d.l;
    Object localObject2 = n.a();
    String str1 = b();
    Object localObject3 = str1;
    localObject3 = (CharSequence)str1;
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject3);
    if (!bool1)
    {
      localObject3 = paramString;
      localObject3 = (CharSequence)paramString;
      boolean bool2 = ((n)localObject2).a((CharSequence)localObject3, str1);
      if (bool2) {
        localObject1 = k.d.c;
      } else {
        try
        {
          localObject2 = a;
          localObject3 = paramString;
          localObject3 = (CharSequence)paramString;
          localObject2 = ((com.google.c.a.k)localObject2).a((CharSequence)localObject3, str1);
          localObject3 = a;
          localObject2 = ((com.google.c.a.k)localObject3).b((m.a)localObject2);
          localObject3 = "phoneNumberUtil.getNumberType(parsedNumber)";
          c.g.b.k.a(localObject2, (String)localObject3);
          localObject1 = localObject2;
        }
        catch (g localg)
        {
          bool1 = true;
          localObject3 = new String[bool1];
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          String str2 = "Invalid number, cannot parse ";
          localStringBuilder.<init>(str2);
          localStringBuilder.append(paramString);
          localStringBuilder.append(" using ");
          localStringBuilder.append(str1);
          localStringBuilder.append(", ");
          paramString = localg.getMessage();
          localStringBuilder.append(paramString);
          paramString = localStringBuilder.toString();
          localObject3[0] = paramString;
        }
      }
    }
    return ab.a((k.d)localObject1);
  }
  
  public final String e(String paramString)
  {
    c.g.b.k.b(paramString, "phoneNumber");
    String str = null;
    try
    {
      com.google.c.a.k localk1 = a;
      com.google.c.a.k localk2 = a;
      paramString = (CharSequence)paramString;
      paramString = localk2.a(paramString, null);
      str = localk1.d(paramString);
    }
    catch (g localg)
    {
      for (;;) {}
    }
    return str;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */