package com.truecaller.common.h;

import c.g.b.k;

public final class au
{
  public final Integer a;
  public final Integer b;
  public final Integer c;
  
  public au(Integer paramInteger1, Integer paramInteger2, Integer paramInteger3)
  {
    a = paramInteger1;
    b = paramInteger2;
    c = paramInteger3;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof au;
      if (bool1)
      {
        paramObject = (au)paramObject;
        Integer localInteger1 = a;
        Integer localInteger2 = a;
        bool1 = k.a(localInteger1, localInteger2);
        if (bool1)
        {
          localInteger1 = b;
          localInteger2 = b;
          bool1 = k.a(localInteger1, localInteger2);
          if (bool1)
          {
            localInteger1 = c;
            paramObject = c;
            boolean bool2 = k.a(localInteger1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final int hashCode()
  {
    Integer localInteger1 = a;
    int i = 0;
    if (localInteger1 != null)
    {
      j = localInteger1.hashCode();
    }
    else
    {
      j = 0;
      localInteger1 = null;
    }
    j *= 31;
    Integer localInteger2 = b;
    int k;
    if (localInteger2 != null)
    {
      k = localInteger2.hashCode();
    }
    else
    {
      k = 0;
      localInteger2 = null;
    }
    int j = (j + k) * 31;
    localInteger2 = c;
    if (localInteger2 != null) {
      i = localInteger2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Version(major=");
    Integer localInteger = a;
    localStringBuilder.append(localInteger);
    localStringBuilder.append(", minor=");
    localInteger = b;
    localStringBuilder.append(localInteger);
    localStringBuilder.append(", build=");
    localInteger = c;
    localStringBuilder.append(localInteger);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.au
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */