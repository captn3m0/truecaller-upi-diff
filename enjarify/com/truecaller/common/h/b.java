package com.truecaller.common.h;

import c.a.f;
import c.u;
import com.bumptech.glide.load.d.a.e;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Arrays;

public abstract class b
  extends e
{
  private final byte[] b;
  private final String c;
  
  public b(String paramString)
  {
    c = paramString;
    paramString = c;
    Charset localCharset = Charset.forName("UTF-8");
    String str = "Charset.forName(\"UTF-8\")";
    c.g.b.k.a(localCharset, str);
    if (paramString != null)
    {
      paramString = paramString.getBytes(localCharset);
      c.g.b.k.a(paramString, "(this as java.lang.String).getBytes(charset)");
      b = paramString;
      return;
    }
    paramString = new c/u;
    paramString.<init>("null cannot be cast to non-null type java.lang.String");
    throw paramString;
  }
  
  public final int a(int... paramVarArgs)
  {
    Object localObject = "hashCodeValues";
    c.g.b.k.b(paramVarArgs, (String)localObject);
    int i = paramVarArgs.length;
    int j = 1;
    if (i == j) {
      return f.b(paramVarArgs);
    }
    i = f.b(paramVarArgs);
    int k = paramVarArgs.length;
    String str = "receiver$0";
    c.g.b.k.b(paramVarArgs, str);
    int m = paramVarArgs.length;
    if (k <= m)
    {
      paramVarArgs = Arrays.copyOfRange(paramVarArgs, j, k);
      c.g.b.k.a(paramVarArgs, "java.util.Arrays.copyOfR…this, fromIndex, toIndex)");
      j = paramVarArgs.length;
      paramVarArgs = Arrays.copyOf(paramVarArgs, j);
      int n = a(paramVarArgs);
      return com.bumptech.glide.g.k.b(i, n);
    }
    paramVarArgs = new java/lang/IndexOutOfBoundsException;
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("toIndex (");
    ((StringBuilder)localObject).append(k);
    ((StringBuilder)localObject).append(") is greater than size (");
    ((StringBuilder)localObject).append(m);
    ((StringBuilder)localObject).append(").");
    localObject = ((StringBuilder)localObject).toString();
    paramVarArgs.<init>((String)localObject);
    throw ((Throwable)paramVarArgs);
  }
  
  public void a(MessageDigest paramMessageDigest)
  {
    c.g.b.k.b(paramMessageDigest, "messageDigest");
    byte[] arrayOfByte = b;
    paramMessageDigest.update(arrayOfByte);
  }
  
  public boolean equals(Object paramObject)
  {
    return paramObject instanceof m;
  }
  
  public int hashCode()
  {
    return c.hashCode();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */