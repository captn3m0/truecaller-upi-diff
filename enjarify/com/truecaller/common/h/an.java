package com.truecaller.common.h;

import c.g.b.k;
import com.truecaller.utils.a;
import java.util.concurrent.TimeUnit;

public final class an
{
  private final a a;
  
  public an(a parama)
  {
    a = parama;
  }
  
  public final long a()
  {
    return a.a();
  }
  
  public final boolean a(long paramLong1, long paramLong2)
  {
    long l = a() - paramLong1;
    boolean bool = l < paramLong2;
    return bool;
  }
  
  public final boolean a(long paramLong1, long paramLong2, TimeUnit paramTimeUnit)
  {
    k.b(paramTimeUnit, "timeUnit");
    paramLong2 = paramTimeUnit.toMillis(paramLong2);
    return a(paramLong1, paramLong2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.an
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */