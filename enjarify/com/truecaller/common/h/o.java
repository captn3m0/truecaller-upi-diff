package com.truecaller.common.h;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipData.Item;
import android.content.Context;
import android.content.Intent;
import android.content.Intent.ShortcutIconResource;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.widget.Toast;
import com.truecaller.common.R.string;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.d;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.c.a.a.a.k;

public final class o
{
  private static final String[] a = { "android.intent.extra.TEXT", "sms_body" };
  
  public static Intent a(Context paramContext)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.telecom.action.CHANGE_DEFAULT_DIALER");
    paramContext = paramContext.getPackageName();
    localIntent.putExtra("android.telecom.extra.CHANGE_DEFAULT_DIALER_PACKAGE_NAME", paramContext);
    return localIntent;
  }
  
  public static Intent a(Context paramContext, int paramInt1, int paramInt2, Intent paramIntent)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    String str = paramContext.getString(paramInt1);
    localIntent.putExtra("android.intent.extra.shortcut.NAME", str);
    paramContext = Intent.ShortcutIconResource.fromContext(paramContext, paramInt2);
    localIntent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", paramContext);
    localIntent.putExtra("android.intent.extra.shortcut.INTENT", paramIntent);
    localIntent.putExtra("duplicate", false);
    return localIntent;
  }
  
  public static Intent a(Uri paramUri)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.VIEW", paramUri);
    return localIntent;
  }
  
  public static Intent a(String paramString)
  {
    return a(Uri.parse(paramString));
  }
  
  public static ArrayList a(Intent paramIntent)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject = paramIntent.getClipData();
    if (localObject != null)
    {
      int i = 0;
      str = null;
      for (;;)
      {
        int j = ((ClipData)localObject).getItemCount();
        if (i >= j) {
          break;
        }
        Uri localUri = ((ClipData)localObject).getItemAt(i).getUri();
        if (localUri != null) {
          localArrayList.add(localUri);
        }
        i += 1;
      }
    }
    localObject = "android.intent.action.SEND_MULTIPLE";
    String str = paramIntent.getAction();
    boolean bool2 = ((String)localObject).equals(str);
    if (bool2)
    {
      localObject = "android.intent.extra.STREAM";
      paramIntent = paramIntent.getParcelableArrayListExtra((String)localObject);
      if (paramIntent != null)
      {
        paramIntent = paramIntent.iterator();
        for (;;)
        {
          bool2 = paramIntent.hasNext();
          if (!bool2) {
            break;
          }
          localObject = (Uri)paramIntent.next();
          boolean bool1 = localArrayList.contains(localObject);
          if (!bool1) {
            localArrayList.add(localObject);
          }
        }
      }
    }
    else
    {
      localObject = "android.intent.action.SEND";
      str = paramIntent.getAction();
      bool2 = ((String)localObject).equals(str);
      if (bool2)
      {
        localObject = "android.intent.extra.STREAM";
        paramIntent = (Uri)paramIntent.getParcelableExtra((String)localObject);
        if (paramIntent != null)
        {
          bool2 = localArrayList.contains(paramIntent);
          if (!bool2) {
            localArrayList.add(paramIntent);
          }
        }
      }
    }
    boolean bool3 = localArrayList.isEmpty();
    if (bool3) {
      return null;
    }
    return localArrayList;
  }
  
  private static void a(Context paramContext, ActivityNotFoundException paramActivityNotFoundException)
  {
    d.a(paramActivityNotFoundException);
    int i = R.string.StrAppNotFound;
    Toast.makeText(paramContext, i, 0).show();
  }
  
  public static void a(Context paramContext, Intent paramIntent, String paramString)
  {
    String str = paramIntent.getAction();
    boolean bool1 = TextUtils.equals(str, paramString);
    if (bool1)
    {
      paramString = paramContext.getPackageName();
      paramContext = paramContext.getPackageManager();
      boolean bool2 = false;
      str = null;
      paramContext = paramContext.queryIntentActivities(paramIntent, 0).iterator();
      do
      {
        bool2 = paramContext.hasNext();
        if (!bool2) {
          break;
        }
        str = nextactivityInfo.packageName;
        bool2 = TextUtils.equals(str, paramString);
      } while (!bool2);
      paramIntent.setPackage(paramString);
      return;
    }
  }
  
  public static void a(Intent paramIntent1, Intent paramIntent2)
  {
    paramIntent1 = paramIntent1.getClipData();
    if (paramIntent1 != null)
    {
      int i = paramIntent1.getItemCount();
      if (i > 0)
      {
        paramIntent2.setClipData(paramIntent1);
        int j = 1;
        paramIntent2.addFlags(j);
      }
    }
  }
  
  public static boolean a(Activity paramActivity, Intent paramIntent, int paramInt)
  {
    try
    {
      paramActivity.startActivityForResult(paramIntent, paramInt);
      return true;
    }
    catch (ActivityNotFoundException paramIntent)
    {
      a(paramActivity, paramIntent);
    }
    return false;
  }
  
  public static boolean a(Context paramContext, Intent paramIntent)
  {
    try
    {
      paramContext.startActivity(paramIntent);
      return true;
    }
    catch (ActivityNotFoundException paramIntent)
    {
      a(paramContext, paramIntent);
    }
    return false;
  }
  
  public static boolean a(Context paramContext, String paramString)
  {
    paramString = String.valueOf(paramString);
    paramString = "tel:".concat(paramString);
    Intent localIntent = new android/content/Intent;
    paramString = Uri.parse(paramString);
    localIntent.<init>("android.intent.action.DIAL", paramString);
    return a(paramContext, localIntent);
  }
  
  public static boolean a(Fragment paramFragment, Intent paramIntent)
  {
    try
    {
      paramFragment.startActivity(paramIntent);
      return true;
    }
    catch (ActivityNotFoundException paramIntent)
    {
      paramFragment = paramFragment.getContext();
      if (paramFragment != null) {
        a(paramFragment, paramIntent);
      }
    }
    return false;
  }
  
  public static boolean a(Fragment paramFragment, Intent paramIntent, int paramInt)
  {
    try
    {
      paramFragment.startActivityForResult(paramIntent, paramInt);
      return true;
    }
    catch (SecurityException paramFragment)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramFragment);
    }
    catch (ActivityNotFoundException paramIntent)
    {
      paramFragment = paramFragment.getContext();
      if (paramFragment != null) {
        a(paramFragment, paramIntent);
      }
    }
    return false;
  }
  
  public static Intent b(Uri paramUri)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.SENDTO", paramUri);
    return localIntent;
  }
  
  public static Intent b(String paramString)
  {
    paramString = String.valueOf(paramString);
    return c("tel:".concat(paramString));
  }
  
  public static String b(Intent paramIntent)
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = a;
    int i = localObject2.length;
    int k = 0;
    int m = 0;
    while (m < i)
    {
      String str1 = localObject2[m];
      str1 = paramIntent.getStringExtra(str1);
      boolean bool2 = k.b(str1);
      if (!bool2)
      {
        int n = ((StringBuilder)localObject1).length();
        if (n > 0)
        {
          String str2 = "\n";
          ((StringBuilder)localObject1).append(str2);
        }
        ((StringBuilder)localObject1).append(str1);
      }
      m += 1;
    }
    localObject1 = ((StringBuilder)localObject1).toString();
    boolean bool3 = ((String)localObject1).isEmpty();
    if (!bool3) {
      return (String)localObject1;
    }
    localObject1 = "android.intent.action.SENDTO";
    localObject2 = paramIntent.getAction();
    boolean bool4 = ((String)localObject1).equals(localObject2);
    if (!bool4)
    {
      localObject1 = "android.intent.action.VIEW";
      localObject2 = paramIntent.getAction();
      bool4 = ((String)localObject1).equals(localObject2);
      if (!bool4) {}
    }
    else
    {
      paramIntent = paramIntent.getData();
      if (paramIntent != null)
      {
        paramIntent = paramIntent.getEncodedQuery();
        bool4 = TextUtils.isEmpty(paramIntent);
        if (!bool4)
        {
          paramIntent = k.a(paramIntent, '&');
          int i1 = paramIntent.length;
          while (k < i1)
          {
            localObject2 = paramIntent[k];
            String str3 = "body=";
            boolean bool1 = ((String)localObject2).startsWith(str3);
            if (bool1)
            {
              int j = 5;
              try
              {
                localObject2 = ((String)localObject2).substring(j);
                str3 = "UTF-8";
                return URLDecoder.decode((String)localObject2, str3);
              }
              catch (UnsupportedEncodingException localUnsupportedEncodingException) {}
            }
            k += 1;
          }
        }
      }
    }
    return null;
  }
  
  public static boolean b(Context paramContext)
  {
    String str = c(paramContext);
    return d(paramContext, str);
  }
  
  public static boolean b(Context paramContext, Intent paramIntent)
  {
    paramContext = paramContext.getPackageManager();
    int i = 65536;
    paramContext = paramContext.queryIntentActivities(paramIntent, i);
    int j = paramContext.size();
    return j > 0;
  }
  
  public static boolean b(Context paramContext, String paramString)
  {
    paramString = c(paramString);
    return a(paramContext, paramString);
  }
  
  public static Intent c(Uri paramUri)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.EDIT", paramUri);
    return localIntent;
  }
  
  private static Intent c(String paramString)
  {
    Intent localIntent = new android/content/Intent;
    paramString = Uri.parse(paramString);
    localIntent.<init>("android.intent.action.CALL", paramString);
    return localIntent;
  }
  
  public static String c(Context paramContext)
  {
    paramContext = paramContext.getPackageName();
    String str = ".debug";
    boolean bool = paramContext.endsWith(str);
    if (bool)
    {
      int i = paramContext.length() + -6;
      paramContext = paramContext.substring(0, i);
    }
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramContext;
    return String.format("market://details?id=%s", arrayOfObject);
  }
  
  public static boolean c(Context paramContext, String paramString)
  {
    Intent localIntent = new android/content/Intent;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("geo:0,0?q=");
    paramString = Uri.encode(paramString);
    localStringBuilder.append(paramString);
    paramString = Uri.parse(localStringBuilder.toString());
    localIntent.<init>("android.intent.action.VIEW", paramString);
    return a(paramContext, localIntent);
  }
  
  public static boolean d(Context paramContext)
  {
    Intent localIntent = a(Uri.parse("market://details?id=%s"));
    paramContext = paramContext.getPackageManager();
    paramContext = localIntent.resolveActivity(paramContext);
    return paramContext != null;
  }
  
  public static boolean d(Context paramContext, String paramString)
  {
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (!bool1)
    {
      paramString = a(Uri.parse(paramString));
      boolean bool2 = a(paramContext, paramString);
      if (!bool2) {
        return false;
      }
    }
    return true;
  }
  
  public static boolean e(Context paramContext, String paramString)
  {
    Intent localIntent = new android/content/Intent;
    paramString = Uri.fromParts("mailto", paramString, null);
    localIntent.<init>("android.intent.action.SENDTO", paramString);
    return a(paramContext, localIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */