package com.truecaller.common.h;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import c.g.b.k;
import com.bumptech.glide.load.b.a.e;

public final class m
  extends b
{
  public m()
  {
    super("com.truecaller.common.util.GradientTransformation");
  }
  
  public final Bitmap a(e parame, Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    k.b(parame, "pool");
    k.b(paramBitmap, "toTransform");
    int i = paramBitmap.getWidth();
    paramInt1 = paramBitmap.getHeight();
    Object localObject1 = Bitmap.Config.ARGB_8888;
    boolean bool = true;
    paramBitmap = paramBitmap.copy((Bitmap.Config)localObject1, bool);
    localObject1 = new android/graphics/Canvas;
    ((Canvas)localObject1).<init>(paramBitmap);
    int j = 2;
    int[] arrayOfInt = new int[j];
    int k = Color.argb(0, 0, 0, 0);
    arrayOfInt[0] = k;
    int m = Color.argb(51, 0, 0, 0);
    arrayOfInt[bool] = m;
    Object localObject2 = new android/graphics/LinearGradient;
    float f1 = i / j;
    float f2 = paramInt1 / 2;
    float f3 = paramInt1;
    Shader.TileMode localTileMode = Shader.TileMode.CLAMP;
    ((LinearGradient)localObject2).<init>(f1, f2, f1, f3, arrayOfInt, null, localTileMode);
    parame = new android/graphics/Paint;
    parame.<init>(4);
    parame.setDither(bool);
    parame.setFilterBitmap(bool);
    localObject2 = (Shader)localObject2;
    parame.setShader((Shader)localObject2);
    ((Canvas)localObject1).drawPaint(parame);
    k.a(paramBitmap, "gradientBitmap");
    return paramBitmap;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */