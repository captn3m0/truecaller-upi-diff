package com.truecaller.common.h;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.ServiceConfigurationError;

final class a$a
  implements Iterator
{
  private final Queue b;
  
  private a$a(a parama)
  {
    LinkedList localLinkedList = new java/util/LinkedList;
    parama = a.a(parama);
    localLinkedList.<init>(parama);
    b = localLinkedList;
  }
  
  public final boolean hasNext()
  {
    Queue localQueue = b;
    boolean bool = localQueue.isEmpty();
    return !bool;
  }
  
  public final Object next()
  {
    String str = (String)b.remove();
    try
    {
      Object localObject1 = a;
      localObject1 = a.c((a)localObject1);
      localObject2 = a;
      localObject2 = a.b((a)localObject2);
      localObject2 = ((ClassLoader)localObject2).loadClass(str);
      localObject2 = ((Class)localObject2).newInstance();
      return ((Class)localObject1).cast(localObject2);
    }
    catch (Exception localException)
    {
      Object localObject2 = new java/util/ServiceConfigurationError;
      str = String.valueOf(str);
      str = "Couldn't instantiate class ".concat(str);
      ((ServiceConfigurationError)localObject2).<init>(str, localException);
      throw ((Throwable)localObject2);
    }
  }
  
  public final void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>();
    throw localUnsupportedOperationException;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */