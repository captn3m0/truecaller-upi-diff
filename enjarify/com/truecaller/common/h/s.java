package com.truecaller.common.h;

import c.a.m;
import c.g.a.b;
import c.m.i;
import c.m.l;
import c.n;
import com.google.c.a.k.c;
import com.google.c.a.m.a;
import com.truecaller.common.c;
import com.truecaller.common.network.KnownDomain;
import com.truecaller.common.network.e;
import com.truecaller.common.network.e.a;
import com.truecaller.common.network.e.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class s
  implements r
{
  private final ac a;
  private final dagger.a b;
  
  public s(ac paramac, dagger.a parama)
  {
    a = paramac;
    b = parama;
  }
  
  public final e a(m.a parama)
  {
    c.g.b.k.b(parama, "number");
    Object localObject = (com.google.c.a.k)b.get();
    String str = ((com.google.c.a.k)localObject).d(parama);
    boolean bool = ((com.google.c.a.k)localObject).a(parama, str);
    if (!bool) {
      return (e)e.a.a;
    }
    c.g.b.k.a(str, "regionCodeForNumber");
    parama = c.a(str);
    localObject = a.b();
    if (parama != localObject)
    {
      localObject = new com/truecaller/common/network/e$b;
      ((e.b)localObject).<init>(parama);
      return (e)localObject;
    }
    return (e)e.a.a;
  }
  
  public final Map a(Iterable paramIterable)
  {
    c.g.b.k.b(paramIterable, "numbers");
    com.google.c.a.k localk = (com.google.c.a.k)b.get();
    Object localObject1 = a.b();
    paramIterable = m.n(paramIterable);
    Object localObject2 = new com/truecaller/common/h/s$a;
    ((s.a)localObject2).<init>(localk);
    localObject2 = (b)localObject2;
    paramIterable = l.c(paramIterable, (b)localObject2);
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    Object localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    paramIterable = paramIterable.a();
    Object localObject5;
    Object localObject6;
    boolean bool2;
    for (;;)
    {
      boolean bool1 = paramIterable.hasNext();
      if (!bool1) {
        break;
      }
      localObject4 = paramIterable.next();
      localObject5 = localObject4;
      localObject5 = (n)localObject4;
      localObject6 = (m.a)a;
      localObject5 = (String)b;
      bool2 = localk.a((m.a)localObject6, (String)localObject5);
      if (bool2) {
        ((ArrayList)localObject2).add(localObject4);
      } else {
        ((ArrayList)localObject3).add(localObject4);
      }
    }
    paramIterable = new c/n;
    paramIterable.<init>(localObject2, localObject3);
    localObject2 = (List)a;
    paramIterable = (List)b;
    localObject3 = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject3).<init>();
    localObject3 = (Map)localObject3;
    Object localObject4 = (c.g.a.a)s.b.a;
    localObject2 = ((Iterable)localObject2).iterator();
    for (;;)
    {
      bool2 = ((Iterator)localObject2).hasNext();
      if (!bool2) {
        break;
      }
      localObject5 = (n)((Iterator)localObject2).next();
      localObject6 = (m.a)a;
      localObject5 = (String)b;
      Object localObject7 = "regionCode";
      c.g.b.k.a(localObject5, (String)localObject7);
      localObject5 = c.a((String)localObject5);
      int i;
      if (localObject5 == localObject1)
      {
        i = 1;
      }
      else
      {
        i = 0;
        localObject7 = null;
      }
      if (i != 0)
      {
        bool2 = false;
        localObject5 = null;
      }
      if (localObject5 != null)
      {
        localObject7 = new com/truecaller/common/network/e$b;
        ((e.b)localObject7).<init>((KnownDomain)localObject5);
        localObject7 = (e)localObject7;
      }
      else
      {
        localObject5 = e.a.a;
        localObject7 = localObject5;
        localObject7 = (e)localObject5;
      }
      localObject5 = ((Map)localObject3).get(localObject7);
      if (localObject5 == null)
      {
        localObject5 = ((c.g.a.a)localObject4).invoke();
        ((Map)localObject3).put(localObject7, localObject5);
      }
      localObject5 = (List)localObject5;
      localObject7 = k.c.a;
      localObject6 = localk.a((m.a)localObject6, (k.c)localObject7);
      localObject7 = "phoneNumberUtil.format(number, E164)";
      c.g.b.k.a(localObject6, (String)localObject7);
      ((List)localObject5).add(localObject6);
    }
    paramIterable = ((Iterable)paramIterable).iterator();
    for (;;)
    {
      boolean bool3 = paramIterable.hasNext();
      if (!bool3) {
        break;
      }
      localObject1 = (m.a)nexta;
      localObject2 = e.a.a;
      localObject5 = ((Map)localObject3).get(localObject2);
      if (localObject5 == null)
      {
        localObject5 = ((c.g.a.a)localObject4).invoke();
        ((Map)localObject3).put(localObject2, localObject5);
      }
      localObject5 = (List)localObject5;
      localObject2 = k.c.a;
      localObject1 = localk.a((m.a)localObject1, (k.c)localObject2);
      localObject2 = "phoneNumberUtil.format(number, E164)";
      c.g.b.k.a(localObject1, (String)localObject2);
      ((List)localObject5).add(localObject1);
    }
    return (Map)localObject3;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */