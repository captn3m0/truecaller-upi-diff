package com.truecaller.common.h;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import com.d.b.ai;

public class aq$d
  implements ai
{
  private static final d a;
  
  static
  {
    d locald = new com/truecaller/common/h/aq$d;
    locald.<init>();
    a = locald;
  }
  
  public static d b()
  {
    return a;
  }
  
  public final Bitmap a(Bitmap paramBitmap)
  {
    int i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    Object localObject = Bitmap.Config.ARGB_8888;
    Bitmap localBitmap = Bitmap.createBitmap(i, j, (Bitmap.Config)localObject);
    Canvas localCanvas = new android/graphics/Canvas;
    localCanvas.<init>(localBitmap);
    localObject = new android/graphics/Paint;
    ((Paint)localObject).<init>();
    ((Paint)localObject).setAntiAlias(true);
    RectF localRectF = new android/graphics/RectF;
    float f1 = localBitmap.getWidth();
    float f2 = localBitmap.getHeight();
    localRectF.<init>(0.0F, 0.0F, f1, f2);
    a(localCanvas, paramBitmap, (Paint)localObject, localRectF);
    paramBitmap.recycle();
    return localBitmap;
  }
  
  public String a()
  {
    return "RoundedImageTransformation";
  }
  
  void a(Canvas paramCanvas, Bitmap paramBitmap, Paint paramPaint, RectF paramRectF)
  {
    BitmapShader localBitmapShader = new android/graphics/BitmapShader;
    Shader.TileMode localTileMode = Shader.TileMode.CLAMP;
    localBitmapShader.<init>(paramBitmap, localTileMode, localTileMode);
    paramPaint.setShader(localBitmapShader);
    paramCanvas.drawOval(paramRectF, paramPaint);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.aq.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */