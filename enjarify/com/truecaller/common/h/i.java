package com.truecaller.common.h;

import c.g.a.a;
import com.truecaller.common.network.e;
import com.truecaller.common.network.e.b;

public class i
{
  private final a a;
  
  public i(a parama)
  {
    a = parama;
  }
  
  private static boolean a(e parame, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      boolean bool = parame instanceof e.b;
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean a(e parame)
  {
    boolean bool = ((Boolean)a.invoke()).booleanValue();
    return a(parame, bool);
  }
  
  public final e.b b(e parame)
  {
    Boolean localBoolean = (Boolean)a.invoke();
    boolean bool = localBoolean.booleanValue();
    bool = a(parame, bool);
    if (!bool) {
      parame = null;
    }
    bool = parame instanceof e.b;
    if (!bool) {
      parame = null;
    }
    return (e.b)parame;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */