package com.truecaller.common.h;

import dagger.a.d;
import javax.inject.Provider;

public final class at
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private at(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static at a(Provider paramProvider1, Provider paramProvider2)
  {
    at localat = new com/truecaller/common/h/at;
    localat.<init>(paramProvider1, paramProvider2);
    return localat;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.at
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */