package com.truecaller.common.h;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Build.VERSION;
import com.truecaller.log.AssertionUtil;

public final class ai
{
  private static Intent a(Intent paramIntent, String paramString, IntentSender paramIntentSender)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 268435456;
    int k = 22;
    if ((i >= k) && (paramIntentSender != null)) {
      return Intent.createChooser(paramIntent, paramString, paramIntentSender).setFlags(j);
    }
    return Intent.createChooser(paramIntent, paramString).setFlags(j);
  }
  
  public static Intent a(String paramString1, String paramString2, CharSequence paramCharSequence)
  {
    return a(paramString1, paramString2, paramCharSequence, null, null, null);
  }
  
  public static Intent a(String paramString1, String paramString2, CharSequence paramCharSequence, Uri paramUri, String paramString3, IntentSender paramIntentSender)
  {
    Intent localIntent = new android/content/Intent;
    String str = "android.intent.action.SEND";
    localIntent.<init>(str);
    if (paramString2 != null)
    {
      str = "android.intent.extra.SUBJECT";
      localIntent.putExtra(str, paramString2);
    }
    paramString2 = "android.intent.extra.TEXT";
    localIntent.putExtra(paramString2, paramCharSequence);
    if (paramUri != null)
    {
      paramString2 = "file";
      paramCharSequence = paramUri.getScheme();
      boolean bool = paramString2.equalsIgnoreCase(paramCharSequence);
      if (bool)
      {
        paramString2 = new java/lang/StringBuilder;
        paramString2.<init>("File URI for intent: ");
        paramCharSequence = paramUri.toString();
        paramString2.append(paramCharSequence);
        paramString2 = paramString2.toString();
        AssertionUtil.reportWeirdnessButNeverCrash(paramString2);
      }
      paramString2 = localIntent.setType(paramString3);
      paramCharSequence = "android.intent.extra.STREAM";
      paramString2.putExtra(paramCharSequence, paramUri);
    }
    else
    {
      paramString2 = "text/plain";
      localIntent.setType(paramString2);
    }
    return a(localIntent, paramString1, paramIntentSender);
  }
  
  /* Error */
  public static Uri a(Context paramContext, android.graphics.Bitmap paramBitmap)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_1
    //   3: ifnonnull +5 -> 8
    //   6: aconst_null
    //   7: areturn
    //   8: ldc 95
    //   10: astore_3
    //   11: ldc 97
    //   13: astore 4
    //   15: invokestatic 102	android/os/Environment:getExternalStorageState	()Ljava/lang/String;
    //   18: astore 5
    //   20: aload 4
    //   22: aload 5
    //   24: invokevirtual 106	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   27: istore 6
    //   29: iload 6
    //   31: ifne +9 -> 40
    //   34: aconst_null
    //   35: astore 5
    //   37: goto +142 -> 179
    //   40: aload_0
    //   41: invokevirtual 112	android/content/Context:getExternalCacheDir	()Ljava/io/File;
    //   44: astore 4
    //   46: aload 4
    //   48: ifnonnull +9 -> 57
    //   51: aconst_null
    //   52: astore 5
    //   54: goto +125 -> 179
    //   57: new 114	com/truecaller/common/h/ai$1
    //   60: astore 5
    //   62: aload 5
    //   64: invokespecial 117	com/truecaller/common/h/ai$1:<init>	()V
    //   67: aload 4
    //   69: aload 5
    //   71: invokevirtual 123	java/io/File:listFiles	(Ljava/io/FilenameFilter;)[Ljava/io/File;
    //   74: astore 5
    //   76: aload 5
    //   78: arraylength
    //   79: istore 7
    //   81: iconst_0
    //   82: istore 8
    //   84: aconst_null
    //   85: astore 9
    //   87: iload 8
    //   89: iload 7
    //   91: if_icmpge +25 -> 116
    //   94: aload 5
    //   96: iload 8
    //   98: aaload
    //   99: astore 10
    //   101: aload 10
    //   103: invokevirtual 127	java/io/File:delete	()Z
    //   106: pop
    //   107: iload 8
    //   109: iconst_1
    //   110: iadd
    //   111: istore 8
    //   113: goto -26 -> 87
    //   116: new 119	java/io/File
    //   119: astore 5
    //   121: new 63	java/lang/StringBuilder
    //   124: astore 11
    //   126: aload 11
    //   128: ldc -127
    //   130: invokespecial 66	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   133: invokestatic 135	java/lang/System:currentTimeMillis	()J
    //   136: lstore 12
    //   138: aload 11
    //   140: lload 12
    //   142: invokevirtual 138	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   145: pop
    //   146: ldc -116
    //   148: astore 9
    //   150: aload 11
    //   152: aload 9
    //   154: invokevirtual 73	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   157: pop
    //   158: aload 11
    //   160: aload_3
    //   161: invokevirtual 73	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   164: pop
    //   165: aload 11
    //   167: invokevirtual 74	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   170: astore_3
    //   171: aload 5
    //   173: aload 4
    //   175: aload_3
    //   176: invokespecial 143	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   179: aload 5
    //   181: ifnonnull +5 -> 186
    //   184: aconst_null
    //   185: areturn
    //   186: new 145	java/io/BufferedOutputStream
    //   189: astore_3
    //   190: new 147	java/io/FileOutputStream
    //   193: astore 4
    //   195: aload 4
    //   197: aload 5
    //   199: invokespecial 150	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   202: aload_3
    //   203: aload 4
    //   205: invokespecial 153	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   208: getstatic 159	android/graphics/Bitmap$CompressFormat:JPEG	Landroid/graphics/Bitmap$CompressFormat;
    //   211: astore 4
    //   213: bipush 90
    //   215: istore 7
    //   217: aload_1
    //   218: aload 4
    //   220: iload 7
    //   222: aload_3
    //   223: invokevirtual 166	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   226: pop
    //   227: aload_0
    //   228: invokestatic 169	com/truecaller/common/h/ai:a	(Landroid/content/Context;)Ljava/lang/String;
    //   231: astore_1
    //   232: aload_0
    //   233: aload_1
    //   234: aload 5
    //   236: invokestatic 174	android/support/v4/content/FileProvider:a	(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;
    //   239: astore_0
    //   240: aload_3
    //   241: invokestatic 179	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   244: aload_0
    //   245: areturn
    //   246: astore_0
    //   247: aload_3
    //   248: astore_2
    //   249: goto +4 -> 253
    //   252: astore_0
    //   253: aload_2
    //   254: invokestatic 179	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   257: aload_0
    //   258: athrow
    //   259: pop
    //   260: aconst_null
    //   261: astore_3
    //   262: aload_3
    //   263: invokestatic 179	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   266: aconst_null
    //   267: areturn
    //   268: pop
    //   269: goto -7 -> 262
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	272	0	paramContext	Context
    //   0	272	1	paramBitmap	android.graphics.Bitmap
    //   1	253	2	localObject1	Object
    //   10	253	3	localObject2	Object
    //   13	206	4	localObject3	Object
    //   18	217	5	localObject4	Object
    //   27	3	6	bool	boolean
    //   79	142	7	i	int
    //   82	30	8	j	int
    //   85	68	9	str	String
    //   99	3	10	localObject5	Object
    //   124	42	11	localStringBuilder	StringBuilder
    //   136	5	12	l	long
    //   259	1	13	localIOException1	java.io.IOException
    //   268	1	14	localIOException2	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   208	211	246	finally
    //   222	227	246	finally
    //   227	231	246	finally
    //   234	239	246	finally
    //   186	189	252	finally
    //   190	193	252	finally
    //   197	202	252	finally
    //   203	208	252	finally
    //   186	189	259	java/io/IOException
    //   190	193	259	java/io/IOException
    //   197	202	259	java/io/IOException
    //   203	208	259	java/io/IOException
    //   208	211	268	java/io/IOException
    //   222	227	268	java/io/IOException
    //   227	231	268	java/io/IOException
    //   234	239	268	java/io/IOException
  }
  
  public static String a(Context paramContext)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramContext = paramContext.getApplicationContext().getPackageName();
    localStringBuilder.append(paramContext);
    localStringBuilder.append(".fileprovider");
    return localStringBuilder.toString();
  }
  
  public static boolean a(Context paramContext, String paramString1, String paramString2, CharSequence paramCharSequence, Uri paramUri)
  {
    return a(paramContext, paramString1, paramString2, paramCharSequence, paramUri, null);
  }
  
  public static boolean a(Context paramContext, String paramString1, String paramString2, CharSequence paramCharSequence, Uri paramUri, IntentSender paramIntentSender)
  {
    boolean bool = k.e();
    if ((bool) && (paramIntentSender != null))
    {
      paramString1 = a(paramString1, paramString2, paramCharSequence, paramUri, "image/jpeg", paramIntentSender);
      return o.a(paramContext, paramString1);
    }
    paramString1 = a(paramString1, paramString2, paramCharSequence, paramUri, "image/jpeg", null);
    return o.a(paramContext, paramString1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.ai
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */