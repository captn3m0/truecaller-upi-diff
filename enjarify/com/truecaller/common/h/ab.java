package com.truecaller.common.h;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build.VERSION;
import android.telephony.PhoneNumberUtils;
import com.google.c.a.g;
import com.google.c.a.g.a;
import com.google.c.a.k;
import com.google.c.a.k.b;
import com.google.c.a.k.d;
import com.truecaller.common.R.string;
import com.truecaller.utils.f;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ab
{
  private static final List a = Collections.singletonList("IT");
  private static final Pattern b = Pattern.compile("[a-zA-Z]{2}-?([a-zA-Z]{6})");
  private static final Pattern c = Pattern.compile("[,;pPwW*#]");
  
  public static int a()
  {
    return Resources.getSystem().getIdentifier("emergency_call_dialog_number_for_display", "string", "android");
  }
  
  public static int a(int paramInt, k.d paramd)
  {
    int i;
    switch (paramInt)
    {
    default: 
      i = 0;
      break;
    case 0: 
    case 1: 
    case 2: 
    case 3: 
    case 4: 
    case 5: 
    case 6: 
    case 7: 
    case 8: 
    case 9: 
    case 10: 
    case 11: 
    case 12: 
    case 13: 
    case 14: 
    case 15: 
    case 16: 
    case 17: 
    case 18: 
    case 19: 
    case 20: 
      i = 1;
    }
    if (i != 0) {
      return paramInt;
    }
    return a(paramd);
  }
  
  public static int a(k.d paramd)
  {
    int i = 10;
    int j;
    if (paramd != null)
    {
      int[] arrayOfInt = ab.1.b;
      j = paramd.ordinal();
      j = arrayOfInt[j];
    }
    switch (j)
    {
    default: 
      i = 7;
      break;
    case 11: 
      i = 7;
      break;
    case 9: 
      i = 6;
      break;
    case 8: 
      i = 2;
      break;
    case 7: 
      i = 7;
      break;
    case 3: 
      i = 2;
      break;
    case 2: 
      i = 2;
      break;
    case 1: 
      i = 1;
      break;
      i = 7;
    }
    return i;
  }
  
  public static k.d a(String paramString, k.d paramd)
  {
    if (paramString != null) {}
    try
    {
      paramString = paramString.toUpperCase();
      paramd = k.d.valueOf(paramString);
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;) {}
    }
    return paramd;
  }
  
  public static String a(Context paramContext, String paramString, boolean paramBoolean)
  {
    boolean bool = d(paramString);
    if (bool) {
      return paramString;
    }
    if (paramBoolean)
    {
      i = R.string.HistoryHiddenNumber;
      return paramContext.getString(i);
    }
    int i = R.string.HistoryCallerUnknown;
    return paramContext.getString(i);
  }
  
  public static String a(Intent paramIntent, Context paramContext)
  {
    Uri localUri = paramIntent.getData();
    int i = 0;
    paramIntent = null;
    if (localUri == null) {
      return null;
    }
    Object localObject1 = localUri.getScheme();
    Object localObject2 = "tel";
    boolean bool1 = ((String)localObject1).equals(localObject2);
    if (!bool1)
    {
      localObject2 = "sip";
      bool1 = ((String)localObject1).equals(localObject2);
      if (!bool1)
      {
        localObject2 = "truecaller";
        boolean bool2 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
        if (!bool2)
        {
          if (paramContext == null) {
            return null;
          }
          localObject1 = localUri.getAuthority();
          localObject2 = "contacts";
          bool1 = ((String)localObject2).equals(localObject1);
          Object localObject3;
          if (bool1)
          {
            localObject1 = "number";
            localObject3 = localObject1;
          }
          else
          {
            localObject2 = "com.android.contacts";
            bool2 = ((String)localObject2).equals(localObject1);
            if (!bool2) {
              break label229;
            }
            localObject1 = "data1";
            localObject3 = localObject1;
          }
          localObject1 = paramContext.getContentResolver();
          localObject2 = new String[1];
          localObject2[0] = localObject3;
          paramContext = ((ContentResolver)localObject1).query(localUri, (String[])localObject2, null, null, null);
          if (paramContext != null) {
            try
            {
              bool2 = paramContext.moveToFirst();
              if (bool2)
              {
                i = paramContext.getColumnIndex((String)localObject3);
                paramIntent = paramContext.getString(i);
              }
            }
            finally
            {
              paramContext.close();
            }
          }
          return paramIntent;
          label229:
          return null;
        }
      }
    }
    return localUri.getSchemeSpecificPart();
  }
  
  public static boolean a(int paramInt)
  {
    String str = k.a().b(paramInt);
    return a.contains(str);
  }
  
  public static boolean a(Context paramContext, String paramString)
  {
    boolean bool = f.a(paramString);
    if (!bool) {
      return false;
    }
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i >= j) {
      return PhoneNumberUtils.isLocalEmergencyNumber(paramContext, paramString);
    }
    return PhoneNumberUtils.isEmergencyNumber(paramString);
  }
  
  public static boolean a(CharSequence paramCharSequence)
  {
    int i = am.b(paramCharSequence);
    if (i != 0) {
      return false;
    }
    i = 0;
    for (;;)
    {
      int k = paramCharSequence.length();
      if (i >= k) {
        break;
      }
      int m = paramCharSequence.charAt(i);
      int n = 48;
      boolean bool = true;
      if (m >= n)
      {
        n = 57;
        if (m <= n) {}
      }
      else
      {
        n = 42;
        if (m != n)
        {
          n = 35;
          if (m != n)
          {
            n = 43;
            if (m != n)
            {
              m = 0;
              break label86;
            }
          }
        }
      }
      m = 1;
      label86:
      if (m != 0) {
        return bool;
      }
      int j;
      i += 1;
    }
    return b(paramCharSequence);
  }
  
  public static boolean a(String paramString)
  {
    boolean bool1 = am.b(paramString);
    if (!bool1)
    {
      String str = "-2";
      bool1 = paramString.equals(str);
      if (!bool1)
      {
        str = "-1";
        boolean bool2 = paramString.equals(str);
        if (!bool2) {
          return false;
        }
      }
    }
    return true;
  }
  
  public static boolean a(String paramString1, String paramString2)
  {
    return a(paramString1, paramString2, false);
  }
  
  public static boolean a(String paramString1, String paramString2, boolean paramBoolean)
  {
    boolean bool1 = am.a(paramString1, paramString2);
    boolean bool2 = true;
    if (bool1)
    {
      boolean bool3 = am.b(paramString1);
      if (!bool3) {
        return bool2;
      }
      return false;
    }
    k localk = k.a();
    paramString1 = localk.a(paramString1, paramString2);
    paramString2 = ab.1.a;
    int i = paramString1.ordinal();
    i = paramString2[i];
    switch (i)
    {
    default: 
      return false;
    case 2: 
      if (!paramBoolean) {
        return bool2;
      }
      return false;
    }
    return bool2;
  }
  
  public static boolean b(CharSequence paramCharSequence)
  {
    if (paramCharSequence == null) {
      return false;
    }
    paramCharSequence = paramCharSequence.toString();
    String str = "@";
    boolean bool1 = paramCharSequence.contains(str);
    if (!bool1)
    {
      str = "%40";
      boolean bool2 = paramCharSequence.contains(str);
      if (!bool2) {
        return false;
      }
    }
    return true;
  }
  
  public static boolean b(String paramString)
  {
    return c.matcher(paramString).find();
  }
  
  public static boolean b(String paramString1, String paramString2)
  {
    boolean bool = am.b(paramString1);
    if (bool) {
      return false;
    }
    try
    {
      bool = am.b(paramString2);
      if (!bool)
      {
        localObject = k.a();
        paramString1 = ((k)localObject).a(paramString1, paramString2);
        return ((k)localObject).c(paramString1);
      }
      paramString1 = new com/google/c/a/g;
      Object localObject = g.a.a;
      String str = "Bad country ISO code, ";
      paramString2 = String.valueOf(paramString2);
      paramString2 = str.concat(paramString2);
      paramString1.<init>((g.a)localObject, paramString2);
      throw paramString1;
    }
    catch (g localg)
    {
      for (;;) {}
    }
    return false;
  }
  
  public static String c(String paramString)
  {
    Matcher localMatcher = b.matcher(paramString);
    boolean bool = localMatcher.matches();
    if (!bool) {
      return paramString;
    }
    return localMatcher.group(1).toUpperCase();
  }
  
  public static boolean d(String paramString)
  {
    if (paramString != null)
    {
      boolean bool = am.b(paramString);
      if (bool)
      {
        int j = paramString.length();
        int i = 3;
        if (j >= i) {
          return true;
        }
      }
    }
    return false;
  }
  
  public static boolean e(String paramString)
  {
    if (paramString != null)
    {
      boolean bool = am.b(paramString);
      if (bool)
      {
        int j = paramString.length();
        int i = 6;
        if (j >= i) {
          return true;
        }
      }
    }
    return false;
  }
  
  public static boolean f(String paramString)
  {
    boolean bool = am.a(paramString);
    if (bool)
    {
      int j = paramString.length();
      int i = 20;
      if (j < i) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */