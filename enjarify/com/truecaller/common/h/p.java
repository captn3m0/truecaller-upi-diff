package com.truecaller.common.h;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import c.u;
import com.bumptech.glide.load.b.a.e;
import com.truecaller.common.R.dimen;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.MessageDigest;

public final class p
  extends b
{
  private final Context b;
  private final int c;
  private final int d;
  private final String e;
  
  public p(Context paramContext, int paramInt1, int paramInt2, String paramString)
  {
    super("com.truecaller.common.util.LabelTransformation");
    b = paramContext;
    c = paramInt1;
    d = paramInt2;
    e = paramString;
  }
  
  public final Bitmap a(e parame, Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    c.g.b.k.b(parame, "pool");
    c.g.b.k.b(paramBitmap, "toTransform");
    parame = b.getResources();
    paramInt1 = R.dimen.label_padding_small;
    int i = parame.getDimensionPixelSize(paramInt1);
    Resources localResources = b.getResources();
    paramInt2 = R.dimen.label_padding_medium;
    paramInt1 = localResources.getDimensionPixelSize(paramInt2);
    Paint localPaint = new android/graphics/Paint;
    localPaint.<init>();
    paramInt2 = c;
    localPaint.setColor(paramInt2);
    Object localObject1 = new android/text/TextPaint;
    ((TextPaint)localObject1).<init>();
    int j = d;
    ((TextPaint)localObject1).setColor(j);
    j = 1;
    ((TextPaint)localObject1).setAntiAlias(j);
    Object localObject2 = Typeface.create("sans-serif-medium", 0);
    ((TextPaint)localObject1).setTypeface((Typeface)localObject2);
    localObject2 = b.getResources();
    int m = R.dimen.label_text_size;
    float f1 = ((Resources)localObject2).getDimensionPixelSize(m);
    ((TextPaint)localObject1).setTextSize(f1);
    int n = paramBitmap.getWidth();
    m = paramInt1 * 2;
    m = n - m;
    CharSequence localCharSequence = (CharSequence)e;
    float f2 = m;
    Object localObject3 = TextUtils.TruncateAt.END;
    localObject3 = TextUtils.ellipsize(localCharSequence, (TextPaint)localObject1, f2, (TextUtils.TruncateAt)localObject3).toString();
    Rect localRect = new android/graphics/Rect;
    localRect.<init>();
    m = ((String)localObject3).length();
    ((TextPaint)localObject1).getTextBounds((String)localObject3, 0, m, localRect);
    Bitmap.Config localConfig = Bitmap.Config.ARGB_8888;
    paramBitmap = paramBitmap.copy(localConfig, j);
    Canvas localCanvas = new android/graphics/Canvas;
    localCanvas.<init>(paramBitmap);
    int k = localRect.height();
    int i1 = i * 2;
    k += i1;
    f2 = n;
    float f3 = k;
    localCanvas.drawRect(0.0F, 0.0F, f2, f3, localPaint);
    k = localRect.height() + i;
    float f4 = paramInt1;
    float f5 = k;
    localObject1 = (Paint)localObject1;
    localCanvas.drawText((String)localObject3, f4, f5, (Paint)localObject1);
    c.g.b.k.a(paramBitmap, "resultBitmap");
    return paramBitmap;
  }
  
  public final void a(MessageDigest paramMessageDigest)
  {
    c.g.b.k.b(paramMessageDigest, "messageDigest");
    super.a(paramMessageDigest);
    int i = 8;
    Object localObject = ByteBuffer.allocate(i);
    int j = c;
    localObject = ((ByteBuffer)localObject).putInt(j);
    j = d;
    localObject = ((ByteBuffer)localObject).putInt(j).array();
    paramMessageDigest.update((byte[])localObject);
    localObject = e;
    Charset localCharset = Charset.forName("UTF-8");
    String str = "Charset.forName(\"UTF-8\")";
    c.g.b.k.a(localCharset, str);
    if (localObject != null)
    {
      localObject = ((String)localObject).getBytes(localCharset);
      c.g.b.k.a(localObject, "(this as java.lang.String).getBytes(charset)");
      paramMessageDigest.update((byte[])localObject);
      return;
    }
    paramMessageDigest = new c/u;
    paramMessageDigest.<init>("null cannot be cast to non-null type java.lang.String");
    throw paramMessageDigest;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = super.equals(paramObject);
    if (bool1) {
      if (paramObject != null)
      {
        paramObject = (p)paramObject;
        int i = c;
        int j = c;
        if (i == j)
        {
          i = d;
          j = d;
          if (i == j)
          {
            paramObject = e;
            String str = e;
            boolean bool2 = c.g.b.k.a(paramObject, str);
            if (bool2) {
              return true;
            }
          }
        }
      }
      else
      {
        paramObject = new c/u;
        ((u)paramObject).<init>("null cannot be cast to non-null type com.truecaller.common.util.LabelTransformation");
        throw ((Throwable)paramObject);
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    int[] arrayOfInt = new int[4];
    int i = super.hashCode();
    arrayOfInt[0] = i;
    i = com.bumptech.glide.g.k.b(c);
    arrayOfInt[1] = i;
    i = com.bumptech.glide.g.k.b(d);
    arrayOfInt[2] = i;
    i = e.hashCode();
    arrayOfInt[3] = i;
    return a(arrayOfInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */