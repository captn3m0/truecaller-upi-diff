package com.truecaller.common.h;

import android.text.TextUtils;
import android.util.Patterns;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.c.a.a.a.k;

public final class am
  extends k
{
  private static final Set a;
  
  static
  {
    HashSet localHashSet = new java/util/HashSet;
    Object localObject = new Character[9];
    Character localCharacter = Character.valueOf(',');
    localObject[0] = localCharacter;
    localCharacter = Character.valueOf(';');
    localObject[1] = localCharacter;
    localCharacter = Character.valueOf('p');
    localObject[2] = localCharacter;
    localCharacter = Character.valueOf('P');
    localObject[3] = localCharacter;
    localCharacter = Character.valueOf('w');
    localObject[4] = localCharacter;
    localCharacter = Character.valueOf('W');
    localObject[5] = localCharacter;
    localCharacter = Character.valueOf('N');
    localObject[6] = localCharacter;
    localCharacter = Character.valueOf('*');
    localObject[7] = localCharacter;
    localCharacter = Character.valueOf('#');
    localObject[8] = localCharacter;
    localObject = Arrays.asList((Object[])localObject);
    localHashSet.<init>((Collection)localObject);
    a = Collections.unmodifiableSet(localHashSet);
  }
  
  public static int a(String paramString1, String paramString2, boolean paramBoolean)
  {
    int i = 1;
    int j;
    if (paramString1 == null) {
      j = 1;
    } else {
      j = 0;
    }
    int k;
    if (paramString2 == null) {
      k = 1;
    } else {
      k = 0;
    }
    j ^= k;
    if (j != 0)
    {
      if (paramString1 == null) {
        return -1;
      }
      return i;
    }
    if ((paramString1 == null) && (paramString2 == null)) {
      return 0;
    }
    if (paramBoolean) {
      return paramString1.compareToIgnoreCase(paramString2);
    }
    return paramString1.compareTo(paramString2);
  }
  
  public static String a(Object paramObject)
  {
    if (paramObject == null) {
      return null;
    }
    return paramObject.toString();
  }
  
  public static String a(String paramString)
  {
    boolean bool = a(paramString);
    if (bool) {
      return paramString;
    }
    return "";
  }
  
  public static String a(String paramString, Locale paramLocale)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      if (paramLocale == null) {
        paramLocale = Locale.ENGLISH;
      }
      return paramString.toUpperCase(paramLocale);
    }
    return paramString;
  }
  
  public static String a(String paramString, CharSequence... paramVarArgs)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int i = paramVarArgs.length;
    int j = 0;
    while (j < i)
    {
      CharSequence localCharSequence = paramVarArgs[j];
      boolean bool = b(localCharSequence);
      if (!bool)
      {
        bool = b(localStringBuilder);
        if (!bool) {
          localStringBuilder.append(paramString);
        }
        localStringBuilder.append(localCharSequence);
      }
      j += 1;
    }
    return localStringBuilder.toString();
  }
  
  public static String a(String... paramVarArgs)
  {
    return a(", ", paramVarArgs);
  }
  
  public static boolean a(CharSequence paramCharSequence)
  {
    boolean bool1 = e(paramCharSequence);
    if (bool1)
    {
      String str = "null";
      boolean bool2 = str.equals(paramCharSequence);
      if (!bool2) {
        return true;
      }
    }
    return false;
  }
  
  public static boolean a(String paramString, int paramInt)
  {
    boolean bool = b(paramString);
    if (!bool) {
      try
      {
        paramString = d(paramString);
        if (paramString != null)
        {
          BigInteger localBigInteger = new java/math/BigInteger;
          localBigInteger.<init>(paramString);
          int i = -1;
          if (paramInt != i)
          {
            int j = paramString.length();
            if (j < paramInt) {
              return false;
            }
          }
          return true;
        }
      }
      catch (NumberFormatException|NullPointerException localNumberFormatException) {}
    }
    return false;
  }
  
  public static String b(String paramString, Locale paramLocale)
  {
    int i = paramString.length();
    int j = 1;
    if (i <= j) {
      return paramString.toUpperCase(paramLocale);
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramLocale = paramString.substring(0, j).toUpperCase(paramLocale);
    localStringBuilder.append(paramLocale);
    paramString = paramString.substring(j);
    localStringBuilder.append(paramString);
    return localStringBuilder.toString();
  }
  
  public static String b(String... paramVarArgs)
  {
    int i = paramVarArgs.length;
    int j = 0;
    while (j < i)
    {
      String str = paramVarArgs[j];
      boolean bool = e(str);
      if (bool) {
        return str;
      }
      j += 1;
    }
    return null;
  }
  
  public static boolean b(String paramString)
  {
    return a(paramString, -1);
  }
  
  public static boolean c(String paramString)
  {
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (!bool1)
    {
      Pattern localPattern = Patterns.EMAIL_ADDRESS;
      paramString = localPattern.matcher(paramString);
      boolean bool2 = paramString.matches();
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public static String d(String paramString)
  {
    boolean bool1 = b(paramString);
    if (!bool1)
    {
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      int i = paramString.length();
      int j = 0;
      int k = 0;
      while (j < i)
      {
        char c1 = paramString.charAt(j);
        boolean bool2 = Character.isDigit(c1);
        if (bool2)
        {
          localStringBuilder.append(c1);
          k = 1;
        }
        else
        {
          char c2 = '+';
          if ((c1 == c2) && (k == 0))
          {
            localStringBuilder.append(c1);
            k = 1;
          }
          else
          {
            Set localSet = a;
            Character localCharacter = Character.valueOf(c1);
            boolean bool3 = localSet.contains(localCharacter);
            if (bool3) {
              localStringBuilder.append(c1);
            }
          }
        }
        j += 1;
      }
      paramString = localStringBuilder.toString();
    }
    return paramString;
  }
  
  public static String e(String paramString)
  {
    paramString = d(paramString);
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      String str1 = "+";
      String str2 = "00";
      paramString = paramString.replace(str1, str2);
    }
    return paramString;
  }
  
  public static int f(String paramString)
  {
    try
    {
      paramString = d(paramString);
      return Integer.parseInt(paramString);
    }
    catch (RuntimeException localRuntimeException) {}
    return 0;
  }
  
  public static int g(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool) {
      return 0;
    }
    try
    {
      return Integer.parseInt(paramString);
    }
    catch (NumberFormatException localNumberFormatException) {}
    return 0;
  }
  
  public static long h(String paramString)
  {
    try
    {
      paramString = d(paramString);
      return Long.parseLong(paramString);
    }
    catch (RuntimeException localRuntimeException) {}
    return 0L;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.am
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */