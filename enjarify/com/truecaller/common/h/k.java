package com.truecaller.common.h;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.truecaller.utils.c;
import com.truecaller.utils.t;
import com.truecaller.utils.t.a;
import java.util.Locale;
import org.c.a.a.a.c.e;

public final class k
{
  private static int a = 255;
  private static int b = 255;
  
  public static String a()
  {
    return am.n(Build.MODEL).trim();
  }
  
  public static boolean a(Context paramContext)
  {
    return c.a().a(paramContext).a().c().d("com.truecaller.qa");
  }
  
  public static long b(Context paramContext)
  {
    try
    {
      PackageManager localPackageManager = paramContext.getPackageManager();
      paramContext = paramContext.getPackageName();
      paramContext = localPackageManager.getPackageInfo(paramContext, 0);
      return firstInstallTime;
    }
    catch (Exception localException)
    {
      com.truecaller.log.d.a(localException;
    }
    return 0L;
  }
  
  public static String b()
  {
    return am.n(Build.MANUFACTURER).trim();
  }
  
  public static String c()
  {
    String str1 = a();
    String str2 = b();
    Object localObject1 = Locale.ENGLISH;
    localObject1 = str1.toLowerCase((Locale)localObject1);
    Object localObject2 = Locale.ENGLISH;
    localObject2 = str2.toLowerCase((Locale)localObject2);
    boolean bool1 = ((String)localObject1).startsWith((String)localObject2);
    if (!bool1)
    {
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      ((StringBuilder)localObject1).append(str2);
      str2 = " ";
      ((StringBuilder)localObject1).append(str2);
      ((StringBuilder)localObject1).append(str1);
      str1 = ((StringBuilder)localObject1).toString();
    }
    boolean bool2 = am.a(str1);
    if (bool2) {
      return e.a(str1);
    }
    return "Unknown";
  }
  
  public static String c(Context paramContext)
  {
    return ((TelephonyManager)paramContext.getSystemService("phone")).getLine1Number();
  }
  
  public static String d(Context paramContext)
  {
    return ((TelephonyManager)paramContext.getSystemService("phone")).getNetworkOperatorName();
  }
  
  public static boolean d()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    return i >= j;
  }
  
  public static String e(Context paramContext)
  {
    return ((TelephonyManager)paramContext.getSystemService("phone")).getSubscriberId();
  }
  
  public static boolean e()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 22;
    return i >= j;
  }
  
  public static String f(Context paramContext)
  {
    paramContext = (TelephonyManager)paramContext.getSystemService("phone");
    String str = paramContext.getNetworkCountryIso();
    boolean bool = TextUtils.isEmpty(str);
    if (bool) {
      str = paramContext.getSimCountryIso();
    }
    paramContext = Locale.ENGLISH;
    return am.c(str, paramContext);
  }
  
  public static boolean f()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 23;
    return i >= j;
  }
  
  public static String g(Context paramContext)
  {
    paramContext = (TelephonyManager)paramContext.getSystemService("phone");
    String str = paramContext.getSimCountryIso();
    boolean bool = TextUtils.isEmpty(str);
    if (bool) {
      str = paramContext.getNetworkCountryIso();
    }
    paramContext = Locale.ENGLISH;
    return am.c(str, paramContext);
  }
  
  public static boolean g()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 24;
    return i >= j;
  }
  
  public static int h(Context paramContext)
  {
    return ((ActivityManager)paramContext.getSystemService("activity")).getLargeMemoryClass();
  }
  
  public static boolean h()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 26;
    return i >= j;
  }
  
  public static String i(Context paramContext)
  {
    return Settings.Secure.getString(paramContext.getContentResolver(), "android_id");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */