package com.truecaller.common.h;

import android.content.Context;
import android.telephony.TelephonyManager;
import c.g.b.k;
import c.u;
import com.truecaller.common.a.a;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public final class e
  implements d
{
  private final TelephonyManager a;
  private final Context b;
  private final a c;
  
  public e(Context paramContext, a parama)
  {
    b = paramContext;
    c = parama;
    paramContext = b;
    parama = "phone";
    paramContext = paramContext.getSystemService(parama);
    if (paramContext != null)
    {
      paramContext = (TelephonyManager)paramContext;
      a = paramContext;
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type android.telephony.TelephonyManager");
    throw paramContext;
  }
  
  private static Object a(Object paramObject, String paramString)
  {
    Object localObject = paramObject.getClass();
    Class[] arrayOfClass = new Class[0];
    paramString = ((Class)localObject).getDeclaredMethod(paramString, arrayOfClass);
    paramString.setAccessible(true);
    localObject = new Object[0];
    return paramString.invoke(paramObject, (Object[])localObject);
  }
  
  private static Object a(Object paramObject, String paramString, Object... paramVarArgs)
  {
    Object localObject1 = paramObject.getClass();
    Object localObject2 = new java/util/ArrayList;
    int i = 1;
    ((ArrayList)localObject2).<init>(i);
    localObject2 = (Collection)localObject2;
    int j = 0;
    Class[] arrayOfClass = null;
    while (j <= 0)
    {
      Class localClass = paramVarArgs[0].getClass();
      ((Collection)localObject2).add(localClass);
      j += 1;
    }
    localObject2 = (Collection)localObject2;
    arrayOfClass = new Class[0];
    localObject2 = ((Collection)localObject2).toArray(arrayOfClass);
    if (localObject2 != null)
    {
      localObject2 = (Class[])localObject2;
      j = localObject2.length;
      localObject2 = (Class[])Arrays.copyOf((Object[])localObject2, j);
      paramString = ((Class)localObject1).getDeclaredMethod(paramString, (Class[])localObject2);
      paramString.setAccessible(i);
      localObject1 = new Object[i];
      localObject1[0] = paramVarArgs;
      return paramString.invoke(paramObject, (Object[])localObject1);
    }
    paramObject = new c/u;
    ((u)paramObject).<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw ((Throwable)paramObject);
  }
  
  public final boolean a()
  {
    int i = 1;
    try
    {
      Object localObject1 = a;
      localObject3 = "getITelephony";
      localObject1 = a(localObject1, (String)localObject3);
      if (localObject1 != null)
      {
        localObject3 = "endCall";
        a(localObject1, (String)localObject3);
      }
      return i;
    }
    catch (Exception localException1)
    {
      Object localObject3 = c;
      ((a)localObject3).a(0, localException1);
      try
      {
        Object localObject2 = a;
        localObject3 = "getITelephonyMSim";
        localObject2 = a(localObject2, (String)localObject3);
        if (localObject2 != null)
        {
          localObject3 = "endCall";
          Object localObject4 = new Object[i];
          Integer localInteger = Integer.valueOf(0);
          localObject4[0] = localInteger;
          localObject3 = a(localObject2, (String)localObject3, (Object[])localObject4);
          localObject4 = Boolean.FALSE;
          boolean bool = k.a(localObject3, localObject4);
          if (bool)
          {
            localObject3 = "endCall";
            localObject4 = new Object[i];
            localInteger = Integer.valueOf(i);
            localObject4[0] = localInteger;
            a(localObject2, (String)localObject3, (Object[])localObject4);
          }
          return i;
        }
      }
      catch (Exception localException2)
      {
        localObject3 = c;
        ((a)localObject3).a(i, localException2);
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */