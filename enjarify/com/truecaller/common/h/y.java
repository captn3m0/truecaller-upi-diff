package com.truecaller.common.h;

import dagger.a.d;
import javax.inject.Provider;

public final class y
  implements d
{
  private final x a;
  private final Provider b;
  
  private y(x paramx, Provider paramProvider)
  {
    a = paramx;
    b = paramProvider;
  }
  
  public static y a(x paramx, Provider paramProvider)
  {
    y localy = new com/truecaller/common/h/y;
    localy.<init>(paramx, paramProvider);
    return localy;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */