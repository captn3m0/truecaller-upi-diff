package com.truecaller.common.h;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import c.u;
import com.bumptech.glide.load.b.a.e;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

public final class ap
  extends b
{
  private final int b;
  
  public ap(int paramInt)
  {
    super("com.truecaller.common.util.TintTransformation");
    b = paramInt;
  }
  
  public final Bitmap a(e parame, Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    c.g.b.k.b(parame, "pool");
    c.g.b.k.b(paramBitmap, "toTransform");
    parame = Bitmap.Config.ARGB_8888;
    parame = paramBitmap.copy(parame, true);
    paramBitmap = new android/graphics/Canvas;
    paramBitmap.<init>(parame);
    paramInt1 = b;
    paramBitmap.drawColor(paramInt1);
    c.g.b.k.a(parame, "tintBitmap");
    return parame;
  }
  
  public final void a(MessageDigest paramMessageDigest)
  {
    c.g.b.k.b(paramMessageDigest, "messageDigest");
    super.a(paramMessageDigest);
    Object localObject = ByteBuffer.allocate(4);
    int i = b;
    localObject = ((ByteBuffer)localObject).putInt(i).array();
    paramMessageDigest.update((byte[])localObject);
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool = super.equals(paramObject);
    if (bool) {
      if (paramObject != null)
      {
        paramObject = (ap)paramObject;
        int j = b;
        int i = b;
        if (j == i) {
          return true;
        }
      }
      else
      {
        paramObject = new c/u;
        ((u)paramObject).<init>("null cannot be cast to non-null type com.truecaller.common.util.TintTransformation");
        throw ((Throwable)paramObject);
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    int[] arrayOfInt = new int[2];
    int i = super.hashCode();
    arrayOfInt[0] = i;
    i = com.bumptech.glide.g.k.b(b);
    arrayOfInt[1] = i;
    return a(arrayOfInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.ap
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */