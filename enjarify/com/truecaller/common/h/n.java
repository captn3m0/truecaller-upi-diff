package com.truecaller.common.h;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Parcelable;
import android.support.v4.content.FileProvider;
import com.d.b.d;
import com.d.b.w;
import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class n
{
  private static Uri a;
  private static Uri b;
  private static Set c;
  
  static
  {
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    c = localHashSet;
  }
  
  public static Intent a()
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    return localIntent.setType("image/*").setAction("android.intent.action.GET_CONTENT");
  }
  
  public static Intent a(Context paramContext)
  {
    Uri localUri = b(paramContext);
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.media.action.IMAGE_CAPTURE");
    String str = null;
    localIntent = localIntent.putExtra("return-data", false);
    Object localObject = "output";
    localIntent = localIntent.putExtra((String)localObject, localUri);
    int i = Build.VERSION.SDK_INT;
    int j = 21;
    if (i < j)
    {
      localObject = paramContext.getPackageManager();
      j = 65536;
      localObject = ((PackageManager)localObject).queryIntentActivities(localIntent, j).iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject).hasNext();
        if (!bool) {
          break;
        }
        str = nextactivityInfo.packageName;
        int k = 3;
        paramContext.grantUriPermission(str, localUri, k);
      }
    }
    return localIntent;
  }
  
  public static Intent a(Context paramContext, Uri paramUri)
  {
    Object localObject1 = new android/content/Intent;
    ((Intent)localObject1).<init>("com.android.camera.action.CROP");
    String str1 = "image/*";
    paramUri = ((Intent)localObject1).setDataAndType(paramUri, str1);
    int i = 800;
    paramUri = paramUri.putExtra("outputX", i).putExtra("outputY", i);
    i = 1;
    paramUri = paramUri.putExtra("aspectX", i).putExtra("aspectY", i).putExtra("scale", i).putExtra("scaleUpIfNeeded", i).putExtra("return-data", false);
    localObject1 = "output";
    Object localObject2 = b;
    Object localObject3;
    Object localObject4;
    if (localObject2 == null)
    {
      localObject2 = ai.a(paramContext);
      localObject3 = new java/io/File;
      localObject4 = paramContext.getCacheDir();
      String str2 = "crop.jpg";
      ((File)localObject3).<init>((File)localObject4, str2);
      localObject2 = FileProvider.a(paramContext, (String)localObject2, (File)localObject3);
      b = (Uri)localObject2;
    }
    localObject2 = b;
    paramUri = paramUri.putExtra((String)localObject1, (Parcelable)localObject2);
    paramUri.addFlags(i);
    int j = 2;
    paramUri.addFlags(j);
    localObject1 = paramContext.getPackageManager();
    int k = 65536;
    localObject1 = ((PackageManager)localObject1).queryIntentActivities(paramUri, k).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = nextactivityInfo.packageName;
      localObject3 = a;
      localObject4 = new String[i];
      localObject4[0] = localObject2;
      a(paramContext, (Uri)localObject3, (String[])localObject4);
      localObject3 = b;
      localObject4 = new String[i];
      localObject4[0] = localObject2;
      a(paramContext, (Uri)localObject3, (String[])localObject4);
    }
    return paramUri;
  }
  
  private static void a(Context paramContext, Uri paramUri, String... paramVarArgs)
  {
    int i = 0;
    while (i <= 0)
    {
      String str = paramVarArgs[0];
      int j = 3;
      paramContext.grantUriPermission(str, paramUri, j);
      i += 1;
    }
    c.add(paramUri);
  }
  
  public static void a(Uri paramUri, Context paramContext)
  {
    if (paramUri != null)
    {
      paramContext = w.a(paramContext);
      if (paramUri != null)
      {
        paramContext = g;
        paramUri = paramUri.toString();
        paramContext.b(paramUri);
      }
      else
      {
        paramUri = new java/lang/IllegalArgumentException;
        paramUri.<init>("uri == null");
        throw paramUri;
      }
    }
  }
  
  /* Error */
  public static byte[] a(android.content.ContentResolver paramContentResolver, Uri paramUri, int paramInt1, int paramInt2, int paramInt3, android.graphics.Bitmap.CompressFormat paramCompressFormat, int paramInt4)
  {
    // Byte code:
    //   0: new 194	android/graphics/BitmapFactory$Options
    //   3: astore 7
    //   5: aload 7
    //   7: invokespecial 195	android/graphics/BitmapFactory$Options:<init>	()V
    //   10: aload 7
    //   12: iconst_0
    //   13: putfield 199	android/graphics/BitmapFactory$Options:inScaled	Z
    //   16: aload 7
    //   18: iconst_0
    //   19: putfield 202	android/graphics/BitmapFactory$Options:inDensity	I
    //   22: aload 7
    //   24: iconst_0
    //   25: putfield 205	android/graphics/BitmapFactory$Options:inTargetDensity	I
    //   28: iload_2
    //   29: iload_3
    //   30: idiv
    //   31: istore_2
    //   32: aload 7
    //   34: iload_2
    //   35: putfield 208	android/graphics/BitmapFactory$Options:inSampleSize	I
    //   38: aload 7
    //   40: iconst_0
    //   41: putfield 211	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   44: aload 7
    //   46: iconst_0
    //   47: putfield 214	android/graphics/BitmapFactory$Options:inMutable	Z
    //   50: iconst_0
    //   51: istore_2
    //   52: aconst_null
    //   53: astore 8
    //   55: aload_0
    //   56: aload_1
    //   57: invokevirtual 220	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   60: astore_0
    //   61: aload_0
    //   62: aconst_null
    //   63: aload 7
    //   65: invokestatic 226	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   68: astore_1
    //   69: aload 7
    //   71: getfield 229	android/graphics/BitmapFactory$Options:outWidth	I
    //   74: istore_2
    //   75: iload_2
    //   76: iload_3
    //   77: if_icmpgt +15 -> 92
    //   80: aload 7
    //   82: getfield 232	android/graphics/BitmapFactory$Options:outHeight	I
    //   85: istore_2
    //   86: iload_2
    //   87: iload 4
    //   89: if_icmple +12 -> 101
    //   92: aload_1
    //   93: iload_3
    //   94: iload 4
    //   96: iconst_0
    //   97: invokestatic 238	android/graphics/Bitmap:createScaledBitmap	(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    //   100: astore_1
    //   101: new 240	java/io/ByteArrayOutputStream
    //   104: astore 8
    //   106: aload 8
    //   108: invokespecial 241	java/io/ByteArrayOutputStream:<init>	()V
    //   111: aload_1
    //   112: aload 5
    //   114: iload 6
    //   116: aload 8
    //   118: invokevirtual 245	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   121: pop
    //   122: aload 8
    //   124: invokevirtual 249	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   127: astore_1
    //   128: aload_0
    //   129: invokestatic 254	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   132: aload_1
    //   133: areturn
    //   134: astore_1
    //   135: goto +6 -> 141
    //   138: astore_1
    //   139: aconst_null
    //   140: astore_0
    //   141: aload_0
    //   142: invokestatic 254	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   145: aload_1
    //   146: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	147	0	paramContentResolver	android.content.ContentResolver
    //   0	147	1	paramUri	Uri
    //   0	147	2	paramInt1	int
    //   0	147	3	paramInt2	int
    //   0	147	4	paramInt3	int
    //   0	147	5	paramCompressFormat	android.graphics.Bitmap.CompressFormat
    //   0	147	6	paramInt4	int
    //   3	78	7	localOptions	android.graphics.BitmapFactory.Options
    //   53	70	8	localByteArrayOutputStream	java.io.ByteArrayOutputStream
    // Exception table:
    //   from	to	target	type
    //   63	68	134	finally
    //   69	74	134	finally
    //   80	85	134	finally
    //   96	100	134	finally
    //   101	104	134	finally
    //   106	111	134	finally
    //   116	122	134	finally
    //   122	127	134	finally
    //   56	60	138	finally
  }
  
  public static Uri b(Context paramContext)
  {
    Object localObject = a;
    if (localObject == null)
    {
      localObject = ai.a(paramContext);
      File localFile1 = new java/io/File;
      File localFile2 = paramContext.getCacheDir();
      String str = "capture.jpg";
      localFile1.<init>(localFile2, str);
      paramContext = FileProvider.a(paramContext, (String)localObject, localFile1);
      a = paramContext;
    }
    return a;
  }
  
  public static Uri c(Context paramContext)
  {
    File localFile = new java/io/File;
    paramContext = paramContext.getCacheDir();
    localFile.<init>(paramContext, "crop.jpg");
    return Uri.fromFile(localFile);
  }
  
  public static Uri d(Context paramContext)
  {
    File localFile = new java/io/File;
    paramContext = paramContext.getCacheDir();
    localFile.<init>(paramContext, "capture.jpg");
    return Uri.fromFile(localFile);
  }
  
  public static void e(Context paramContext)
  {
    Iterator localIterator = c.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Uri localUri = (Uri)localIterator.next();
      int i = 3;
      paramContext.revokeUriPermission(localUri, i);
    }
  }
  
  public static void f(Context paramContext)
  {
    File localFile1 = paramContext.getCacheDir();
    File localFile2 = new java/io/File;
    localFile2.<init>(localFile1, "capture.jpg");
    localFile2.delete();
    localFile2 = new java/io/File;
    localFile2.<init>(localFile1, "crop.jpg");
    localFile2.delete();
    e(paramContext);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */