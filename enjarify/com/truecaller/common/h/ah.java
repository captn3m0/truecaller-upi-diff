package com.truecaller.common.h;

import com.google.c.a.h;
import com.google.c.a.k;
import java.util.Iterator;
import java.util.Locale;

public final class ah
{
  static final String a = Character.toString('‎');
  static final String b = Character.toString('‬');
  
  public static String a(String paramString)
  {
    return a(paramString, null);
  }
  
  public static String a(String paramString1, String paramString2)
  {
    if (paramString1 != null)
    {
      boolean bool1 = paramString1.isEmpty();
      if (!bool1)
      {
        if (paramString2 == null)
        {
          paramString2 = null;
        }
        else
        {
          localObject1 = Locale.ENGLISH;
          paramString2 = paramString2.toUpperCase((Locale)localObject1);
        }
        Object localObject1 = k.a();
        paramString2 = ((k)localObject1).c(paramString1, paramString2).iterator();
        bool1 = paramString2.hasNext();
        if (!bool1) {
          return paramString1;
        }
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>(paramString1);
        int i = 0;
        paramString1 = null;
        for (;;)
        {
          boolean bool2 = paramString2.hasNext();
          if (!bool2) {
            break;
          }
          Object localObject2 = (h)paramString2.next();
          int k = a + i;
          String str1 = a;
          ((StringBuilder)localObject1).insert(k, str1);
          k = a.length();
          i += k;
          int j = ((h)localObject2).a() + i;
          String str2 = b;
          ((StringBuilder)localObject1).insert(j, str2);
          localObject2 = b;
          j = ((String)localObject2).length();
          i += j;
        }
        return ((StringBuilder)localObject1).toString();
      }
    }
    return "";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */