package com.truecaller.common.h;

import android.content.Context;
import c.f;
import c.g.a.a;
import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import c.l.b;
import com.truecaller.utils.n;

public final class ak
  implements aj
{
  private final f b;
  private final Context c;
  private final n d;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[1];
    Object localObject = new c/g/b/u;
    b localb = w.a(ak.class);
    ((u)localObject).<init>(localb, "voicemailNumber", "getVoicemailNumber()Ljava/lang/String;");
    localObject = (c.l.g)w.a((t)localObject);
    arrayOfg[0] = localObject;
    a = arrayOfg;
  }
  
  public ak(Context paramContext, n paramn)
  {
    c = paramContext;
    d = paramn;
    paramContext = new com/truecaller/common/h/ak$a;
    paramContext.<init>(this);
    paramContext = c.g.a((a)paramContext);
    b = paramContext;
  }
  
  private final String b()
  {
    return (String)b.b();
  }
  
  public final String a()
  {
    int i = ab.a();
    Integer localInteger = Integer.valueOf(i);
    Object localObject = localInteger;
    localObject = (Number)localInteger;
    int j = ((Number)localObject).intValue();
    Object[] arrayOfObject = null;
    if (j != 0)
    {
      j = 1;
    }
    else
    {
      j = 0;
      localObject = null;
    }
    if (j == 0)
    {
      i = 0;
      localInteger = null;
    }
    if (localInteger != null)
    {
      i = ((Number)localInteger).intValue();
      localObject = d;
      arrayOfObject = new Object[0];
      return ((n)localObject).a(i, arrayOfObject);
    }
    return null;
  }
  
  public final boolean a(CharSequence paramCharSequence)
  {
    k.b(paramCharSequence, "phoneNumber");
    return ab.b(paramCharSequence);
  }
  
  public final boolean a(String paramString)
  {
    return ab.a(c, paramString);
  }
  
  public final boolean a(String... paramVarArgs)
  {
    k.b(paramVarArgs, "phoneNumbers");
    CharSequence localCharSequence = (CharSequence)b();
    boolean bool1 = true;
    if (localCharSequence != null)
    {
      i = localCharSequence.length();
      if (i != 0)
      {
        i = 0;
        localCharSequence = null;
        break label47;
      }
    }
    int i = 1;
    label47:
    if (i == 0)
    {
      i = paramVarArgs.length;
      int j = 0;
      while (j < i)
      {
        String str1 = paramVarArgs[j];
        String str2 = b();
        boolean bool2 = k.a(str2, str1);
        if (bool2)
        {
          k = 1;
          break label112;
        }
        j += 1;
      }
      int k = 0;
      paramVarArgs = null;
      label112:
      if (k != 0) {
        return bool1;
      }
    }
    return false;
  }
  
  public final boolean b(String paramString)
  {
    k.b(paramString, "phoneNumber");
    return ab.a((CharSequence)paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.ak
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */