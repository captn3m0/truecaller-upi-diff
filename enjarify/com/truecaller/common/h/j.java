package com.truecaller.common.h;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.text.format.DateUtils;
import com.truecaller.common.R.string;
import com.truecaller.common.e.b.a;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.a.a.d.i.a;
import org.a.a.x;

public final class j
{
  protected static java.text.DateFormat a = null;
  protected static java.text.DateFormat b = null;
  private static final long c;
  private static final long d;
  private static final long e;
  private static final long f;
  private static final StringBuilder g;
  private static final Formatter h;
  private static final SimpleDateFormat i;
  private static final SimpleDateFormat j;
  private static final SimpleDateFormat k;
  private static final SimpleDateFormat l;
  
  static
  {
    Object localObject = TimeUnit.DAYS;
    long l1 = 1L;
    c = ((TimeUnit)localObject).toMillis(l1);
    d = TimeUnit.DAYS.toHours(l1);
    e = TimeUnit.MINUTES.toSeconds(l1);
    f = TimeUnit.HOURS.toSeconds(l1);
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>(32);
    g = (StringBuilder)localObject;
    localObject = new java/util/Formatter;
    StringBuilder localStringBuilder = g;
    Locale localLocale = com.truecaller.common.e.f.a();
    ((Formatter)localObject).<init>(localStringBuilder, localLocale);
    h = (Formatter)localObject;
    localObject = new java/text/SimpleDateFormat;
    ((SimpleDateFormat)localObject).<init>("yyyy-MM-dd");
    i = (SimpleDateFormat)localObject;
    localObject = new java/text/SimpleDateFormat;
    ((SimpleDateFormat)localObject).<init>("HH:mm");
    j = (SimpleDateFormat)localObject;
    localObject = new java/text/SimpleDateFormat;
    ((SimpleDateFormat)localObject).<init>("dd/MM");
    k = (SimpleDateFormat)localObject;
    localObject = new java/text/SimpleDateFormat;
    ((SimpleDateFormat)localObject).<init>("MM/dd");
    l = (SimpleDateFormat)localObject;
  }
  
  public static int a()
  {
    Calendar localCalendar = Calendar.getInstance();
    int m = localCalendar.get(11) * 60;
    int n = localCalendar.get(12);
    return m + n;
  }
  
  public static CharSequence a(Context paramContext, long paramLong, boolean paramBoolean)
  {
    synchronized (j.class)
    {
      Object localObject1 = g;
      ((StringBuilder)localObject1).setLength(0);
      long l1 = System.currentTimeMillis();
      localObject1 = TimeZone.getDefault();
      int m = ((TimeZone)localObject1).getOffset(l1);
      long l2 = m;
      l1 += l2;
      long l3 = c;
      l1 /= l3;
      l2 += paramLong;
      l3 = c;
      l2 /= l3;
      boolean bool = l1 < l2;
      if (!bool)
      {
        paramContext = f(paramContext, paramLong);
        return paramContext;
      }
      Object localObject2;
      if (!paramBoolean)
      {
        l3 = l1 - l2;
        long l4 = 1L;
        bool = l3 < l4;
        if (!bool)
        {
          localObject1 = paramContext.getResources();
          int n = R.string.yesterday;
          localObject1 = ((Resources)localObject1).getString(n);
          localObject2 = com.truecaller.common.e.f.a();
          localObject1 = am.b((String)localObject1, (Locale)localObject2);
          break label236;
        }
      }
      l1 -= l2;
      l2 = 7;
      bool = l1 < l2;
      if (!bool)
      {
        localObject1 = d(paramContext, paramLong);
      }
      else
      {
        Formatter localFormatter = h;
        int i1 = 32770;
        localObject2 = paramContext;
        l2 = paramLong;
        l3 = paramLong;
        localObject1 = DateUtils.formatDateRange(paramContext, localFormatter, paramLong, paramLong, i1);
        localObject1 = ((Formatter)localObject1).toString();
      }
      label236:
      if (paramBoolean)
      {
        paramContext = f(paramContext, paramLong);
        String str = "%s, %s";
        int i2 = 2;
        Object[] arrayOfObject = new Object[i2];
        arrayOfObject[0] = paramContext;
        int i3 = 1;
        arrayOfObject[i3] = localObject1;
        localObject1 = String.format(str, arrayOfObject);
      }
      return (CharSequence)localObject1;
    }
  }
  
  public static String a(long paramLong)
  {
    long l1 = 0L;
    boolean bool = paramLong < l1;
    if (!bool) {
      return "";
    }
    long l2 = System.currentTimeMillis();
    l1 = paramLong;
    return String.valueOf(DateUtils.getRelativeTimeSpanString(paramLong, l2, 60000L, 524288));
  }
  
  public static String a(Context paramContext, int paramInt)
  {
    if (paramInt < 0) {
      return "";
    }
    long l1 = paramInt;
    long l2 = e;
    int m = 1;
    boolean bool = l1 < l2;
    if (bool)
    {
      int n = R.string.duration_sec;
      Object[] arrayOfObject1 = new Object[m];
      Integer localInteger = Integer.valueOf(paramInt);
      arrayOfObject1[0] = localInteger;
      return paramContext.getString(n, arrayOfObject1);
    }
    long l3 = f;
    paramInt = l1 < l3;
    if (paramInt < 0)
    {
      l1 /= l2;
      paramInt = R.string.duration_min;
      arrayOfObject2 = new Object[m];
      localLong = Long.valueOf(l1);
      arrayOfObject2[0] = localLong;
      return paramContext.getString(paramInt, arrayOfObject2);
    }
    l1 /= l3;
    l2 = d;
    l1 %= l2;
    paramInt = R.string.duration_hour;
    Object[] arrayOfObject2 = new Object[m];
    Long localLong = Long.valueOf(l1);
    arrayOfObject2[0] = localLong;
    return paramContext.getString(paramInt, arrayOfObject2);
  }
  
  public static String a(Context paramContext, long paramLong)
  {
    Object localObject1 = Calendar.getInstance(com.truecaller.common.e.f.a());
    ((Calendar)localObject1).setTimeInMillis(paramLong);
    long l1 = (System.currentTimeMillis() - paramLong) / 1000L;
    Object localObject2 = TimeUnit.MINUTES;
    long l2 = 1L;
    paramLong = ((TimeUnit)localObject2).toSeconds(l2);
    boolean bool1 = l1 < paramLong;
    int m;
    if (bool1)
    {
      paramContext = paramContext.getResources();
      m = R.string.now;
      paramContext = paramContext.getString(m);
    }
    else
    {
      localObject2 = TimeUnit.MINUTES;
      l2 = 10;
      paramLong = ((TimeUnit)localObject2).toSeconds(l2);
      bool1 = false;
      int n = 1;
      boolean bool2 = l1 < paramLong;
      Object localObject3;
      if (bool2)
      {
        paramLong = TimeUnit.SECONDS.toMinutes(l1);
        paramContext = paramContext.getResources();
        int i1 = R.string.n_minutes_ago;
        localObject3 = new Object[n];
        localObject2 = Long.valueOf(paramLong);
        localObject3[0] = localObject2;
        paramContext = paramContext.getString(i1, (Object[])localObject3);
      }
      else
      {
        localObject2 = Calendar.getInstance(com.truecaller.common.e.f.a());
        Object localObject4 = Calendar.getInstance(com.truecaller.common.e.f.a());
        int i2 = -1;
        int i3 = 6;
        ((Calendar)localObject4).add(i3, i2);
        localObject3 = Calendar.getInstance(com.truecaller.common.e.f.a());
        ((Calendar)localObject3).add(i3, -7);
        bool2 = com.truecaller.common.e.f.c();
        int i4 = ((Calendar)localObject1).get(n);
        int i5 = ((Calendar)localObject2).get(n);
        int i6 = 3;
        if (i4 == i5)
        {
          i4 = ((Calendar)localObject1).get(i3);
          i5 = ((Calendar)localObject2).get(i3);
          if (i4 == i5)
          {
            paramContext = android.text.format.DateFormat.getTimeFormat(paramContext);
            if (paramContext == null)
            {
              paramContext = com.truecaller.common.e.f.a();
              paramContext = java.text.DateFormat.getTimeInstance(i6, paramContext);
            }
            localObject2 = ((Calendar)localObject1).getTime();
            return paramContext.format((Date)localObject2);
          }
        }
        i4 = ((Calendar)localObject1).get(i3);
        int i7 = ((Calendar)localObject4).get(i3);
        if (i4 == i7)
        {
          paramContext = paramContext.getResources();
          m = R.string.yesterday;
          paramContext = paramContext.getString(m);
        }
        else
        {
          boolean bool3 = ((Calendar)localObject1).after(localObject3);
          if (bool3)
          {
            paramContext = new java/text/SimpleDateFormat;
            localObject4 = com.truecaller.common.e.f.a();
            paramContext.<init>("EEEE", (Locale)localObject4);
            localObject2 = ((Calendar)localObject1).getTime();
            paramContext = paramContext.format((Date)localObject2);
          }
          else
          {
            int i8 = ((Calendar)localObject1).get(n);
            m = ((Calendar)localObject2).get(n);
            i7 = 5;
            i2 = 2;
            if (i8 == m)
            {
              if (bool2)
              {
                paramContext = new com/truecaller/common/e/b$a;
                m = ((Calendar)localObject1).get(n);
                i3 = ((Calendar)localObject1).get(i2);
                i7 = ((Calendar)localObject1).get(i7);
                paramContext.<init>(m, i3, i7);
                paramContext = com.truecaller.common.e.b.a(paramContext);
                localObject2 = paramContext.a();
                i8 = c;
                localObject4 = "%d %s";
                localObject1 = new Object[i2];
                paramContext = Integer.valueOf(i8);
                localObject1[0] = paramContext;
                localObject1[n] = localObject2;
                paramContext = String.format((String)localObject4, (Object[])localObject1);
              }
              else
              {
                paramContext = new java/text/SimpleDateFormat;
                localObject4 = com.truecaller.common.e.f.a();
                paramContext.<init>("dd MMM", (Locale)localObject4);
                paramContext.setCalendar((Calendar)localObject1);
                localObject2 = ((Calendar)localObject1).getTime();
                paramContext = paramContext.format((Date)localObject2);
              }
            }
            else if (bool2)
            {
              paramContext = new com/truecaller/common/e/b$a;
              m = ((Calendar)localObject1).get(n);
              i3 = ((Calendar)localObject1).get(i2);
              i7 = ((Calendar)localObject1).get(i7);
              paramContext.<init>(m, i3, i7);
              paramContext = com.truecaller.common.e.b.a(paramContext);
              localObject2 = paramContext.a();
              i7 = c;
              i8 = a;
              localObject1 = "%d %s %d";
              Object[] arrayOfObject = new Object[i6];
              localObject4 = Integer.valueOf(i7);
              arrayOfObject[0] = localObject4;
              arrayOfObject[n] = localObject2;
              paramContext = Integer.valueOf(i8);
              arrayOfObject[i2] = paramContext;
              paramContext = String.format((String)localObject1, arrayOfObject);
            }
            else
            {
              paramContext = com.truecaller.common.e.f.a();
              paramContext = java.text.DateFormat.getDateInstance(i2, paramContext);
              paramContext.setCalendar((Calendar)localObject1);
              localObject2 = ((Calendar)localObject1).getTime();
              paramContext = paramContext.format((Date)localObject2);
            }
          }
        }
      }
    }
    return paramContext;
  }
  
  public static org.a.a.b a(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool) {
      try
      {
        org.a.a.d.b localb = i.a.a();
        org.a.a.f localf = org.a.a.f.a;
        localb = localb.a(localf);
        paramString = localb.b(paramString);
      }
      catch (Exception localException) {}
    } else {
      paramString = null;
    }
    return paramString;
  }
  
  public static boolean a(long paramLong1, long paramLong2)
  {
    long l1 = System.currentTimeMillis() - paramLong1;
    boolean bool = l1 < paramLong2;
    return bool;
  }
  
  public static CharSequence b(Context paramContext, long paramLong)
  {
    synchronized (j.class)
    {
      Object localObject = g;
      Formatter localFormatter = null;
      ((StringBuilder)localObject).setLength(0);
      long l1 = System.currentTimeMillis() - paramLong;
      l1 = Math.abs(l1);
      long l2 = 60000L;
      l1 /= l2;
      l2 = 0L;
      boolean bool = l1 < l2;
      if (!bool)
      {
        int m = R.string.now;
        paramContext = paramContext.getString(m);
        return paramContext;
      }
      l2 = 10;
      bool = l1 < l2;
      if (!bool)
      {
        long l3 = System.currentTimeMillis();
        long l4 = 60000L;
        int n = 524288;
        paramContext = DateUtils.getRelativeTimeSpanString(paramLong, l3, l4, n);
        return paramContext;
      }
      localFormatter = h;
      int i1 = 524289;
      localObject = paramContext;
      l2 = paramLong;
      paramContext = DateUtils.formatDateRange(paramContext, localFormatter, paramLong, paramLong, i1);
      paramContext = paramContext.toString();
      return paramContext;
    }
  }
  
  public static void b()
  {
    a = null;
    b = null;
  }
  
  public static String c()
  {
    Object localObject1 = org.a.a.f.a();
    long l1 = org.a.a.b.ay_().a();
    int m = ((org.a.a.f)localObject1).b(l1);
    Object localObject2 = TimeUnit.MILLISECONDS;
    long l2 = m;
    int n = (int)((TimeUnit)localObject2).toHours(l2);
    l2 = TimeUnit.MILLISECONDS.toMinutes(l2);
    long l3 = n;
    long l4 = TimeUnit.HOURS.toMinutes(1L);
    l3 *= l4;
    m = Math.abs((int)(l2 - l3));
    Locale localLocale = Locale.ENGLISH;
    Object[] arrayOfObject = new Object[2];
    localObject2 = Integer.valueOf(n);
    arrayOfObject[0] = localObject2;
    localObject1 = Integer.valueOf(m);
    arrayOfObject[1] = localObject1;
    return String.format(localLocale, "GMT%+03d:%02d", arrayOfObject);
  }
  
  public static String c(Context paramContext, long paramLong)
  {
    long l1 = 0L;
    boolean bool1 = paramLong < l1;
    if (bool1) {
      return "";
    }
    l1 = e;
    bool1 = false;
    Long localLong1 = null;
    int m = 1;
    boolean bool2 = paramLong < l1;
    if (bool2)
    {
      n = R.string.duration_s;
      arrayOfObject = new Object[m];
      localLong2 = Long.valueOf(paramLong);
      arrayOfObject[0] = localLong2;
      return paramContext.getString(n, arrayOfObject);
    }
    long l2 = f;
    int i1 = 2;
    boolean bool3 = paramLong < l2;
    if (bool3)
    {
      l2 = paramLong / l1;
      paramLong %= l1;
      n = R.string.duration_ms;
      arrayOfObject = new Object[i1];
      localLong3 = Long.valueOf(l2);
      arrayOfObject[0] = localLong3;
      localLong2 = Long.valueOf(paramLong);
      arrayOfObject[m] = localLong2;
      return paramContext.getString(n, arrayOfObject);
    }
    l2 = paramLong / l2;
    long l3 = d;
    l2 %= l3;
    l3 = paramLong / l1 % l1;
    paramLong %= l1;
    int n = R.string.duration_hms;
    Object[] arrayOfObject = new Object[3];
    Long localLong3 = Long.valueOf(l2);
    arrayOfObject[0] = localLong3;
    localLong1 = Long.valueOf(l3);
    arrayOfObject[m] = localLong1;
    Long localLong2 = Long.valueOf(paramLong);
    arrayOfObject[i1] = localLong2;
    return paramContext.getString(n, arrayOfObject);
  }
  
  public static String d(Context paramContext, long paramLong)
  {
    try
    {
      synchronized (j.class)
      {
        localObject = a;
        if (localObject == null)
        {
          paramContext = android.text.format.DateFormat.getDateFormat(paramContext);
          a = paramContext;
        }
        paramContext = a;
        localObject = new java/util/Date;
        ((Date)localObject).<init>(paramLong);
        paramContext = paramContext.format((Date)localObject);
        return paramContext;
      }
    }
    catch (Exception localException1)
    {
      try
      {
        paramContext = i;
        Object localObject = new java/util/Date;
        ((Date)localObject).<init>(paramLong);
        paramContext = paramContext.format((Date)localObject);
        return paramContext;
      }
      catch (Exception localException2)
      {
        return "";
      }
    }
  }
  
  public static String e(Context paramContext, long paramLong)
  {
    int m = 77;
    int n = 100;
    try
    {
      paramContext = android.text.format.DateFormat.getDateFormatOrder(paramContext);
      int i1 = paramContext.length;
      int i2 = 0;
      while (i2 < i1)
      {
        int i3 = paramContext[i2];
        if ((i3 != n) && (i3 != m))
        {
          i2 += 1;
        }
        else
        {
          m = i3;
          break label70;
        }
      }
      m = 100;
    }
    catch (IllegalArgumentException localIllegalArgumentException) {}
    label70:
    if (m == n) {
      paramContext = k;
    } else {
      paramContext = l;
    }
    Date localDate = new java/util/Date;
    localDate.<init>(paramLong);
    return paramContext.format(localDate);
  }
  
  public static String f(Context paramContext, long paramLong)
  {
    try
    {
      synchronized (j.class)
      {
        localObject = b;
        if (localObject == null)
        {
          paramContext = android.text.format.DateFormat.getTimeFormat(paramContext);
          b = paramContext;
        }
        paramContext = b;
        localObject = new java/util/Date;
        ((Date)localObject).<init>(paramLong);
        paramContext = paramContext.format((Date)localObject);
        return paramContext;
      }
    }
    catch (Exception localException1)
    {
      try
      {
        paramContext = j;
        Object localObject = new java/util/Date;
        ((Date)localObject).<init>(paramLong);
        paramContext = paramContext.format((Date)localObject);
        return paramContext;
      }
      catch (Exception localException2)
      {
        return "";
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.h.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */