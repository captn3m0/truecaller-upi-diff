package com.truecaller.common.a;

import c.g.b.k;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.utils.d;
import java.lang.reflect.InvocationTargetException;

public final class b
  implements a
{
  private final com.truecaller.analytics.b a;
  private final d b;
  
  public b(com.truecaller.analytics.b paramb, d paramd)
  {
    a = paramb;
    b = paramd;
  }
  
  public final void a(int paramInt, Exception paramException)
  {
    k.b(paramException, "e");
    com.truecaller.analytics.b localb = a;
    Object localObject1 = new com/truecaller/analytics/e$a;
    ((e.a)localObject1).<init>("DeclineCallErrors");
    String str = "Method";
    switch (paramInt)
    {
    default: 
      localObject2 = "Default";
      break;
    case 2: 
      localObject2 = "TelecomManager";
      break;
    case 1: 
      localObject2 = "Msim";
    }
    Object localObject2 = ((e.a)localObject1).a(str, (String)localObject2);
    localObject1 = "Reason";
    boolean bool1 = paramException instanceof InvocationTargetException;
    boolean bool2;
    if (bool1)
    {
      paramException = ((InvocationTargetException)paramException).getTargetException();
      bool2 = paramException instanceof SecurityException;
      if (bool2) {
        paramException = "Security";
      } else {
        paramException = "Unknown";
      }
    }
    else
    {
      bool1 = paramException instanceof SecurityException;
      if (bool1)
      {
        paramException = "Security";
      }
      else
      {
        bool2 = paramException instanceof NoSuchMethodException;
        if (bool2) {
          paramException = "Reflection";
        } else {
          paramException = "Unknown";
        }
      }
    }
    localObject2 = ((e.a)localObject2).a((String)localObject1, paramException);
    paramException = "SecurityPatchVersion";
    localObject1 = b.i();
    if (localObject1 == null) {
      localObject1 = "";
    }
    localObject2 = ((e.a)localObject2).a(paramException, (String)localObject1).a();
    k.a(localObject2, "AnalyticsEvent.Builder(C…\n                .build()");
    localb.b((e)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */