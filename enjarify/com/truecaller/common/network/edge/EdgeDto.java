package com.truecaller.common.network.edge;

import java.util.Map;

public final class EdgeDto
{
  private Map data;
  private int timeToLive;
  
  public final Map getData()
  {
    return data;
  }
  
  public final int getTimeToLive()
  {
    return timeToLive;
  }
  
  public final void setData(Map paramMap)
  {
    data = paramMap;
  }
  
  public final void setTimeToLive(int paramInt)
  {
    timeToLive = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("EdgeDto(data=");
    Map localMap = data;
    localStringBuilder.append(localMap);
    localStringBuilder.append(", timeToLive=");
    int i = timeToLive;
    localStringBuilder.append(i);
    localStringBuilder.append(')');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.edge.EdgeDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */