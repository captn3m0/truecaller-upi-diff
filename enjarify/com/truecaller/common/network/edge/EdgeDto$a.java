package com.truecaller.common.network.edge;

import c.a.m;
import java.util.List;

public final class EdgeDto$a
{
  public List a;
  
  public EdgeDto$a() {}
  
  public EdgeDto$a(String paramString)
  {
    this();
    String[] arrayOfString = new String[1];
    arrayOfString[0] = paramString;
    paramString = m.c(arrayOfString);
    a = paramString;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Endpoint(edges=");
    List localList = a;
    localStringBuilder.append(localList);
    localStringBuilder.append(')');
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.edge.EdgeDto.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */