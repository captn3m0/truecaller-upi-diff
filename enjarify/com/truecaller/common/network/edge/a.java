package com.truecaller.common.network.edge;

import com.truecaller.common.network.util.AuthRequirement;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.d;
import okhttp3.y;

public final class a
{
  public static e.b a(String paramString1, String paramString2, String paramString3)
  {
    com.truecaller.common.network.util.a locala = new com/truecaller/common/network/util/a;
    locala.<init>();
    Object localObject = KnownEndpoints.EDGE;
    locala = locala.a((KnownEndpoints)localObject).a(a.a.class);
    localObject = new com/truecaller/common/network/a/b;
    ((com.truecaller.common.network.a.b)localObject).<init>();
    AuthRequirement localAuthRequirement = AuthRequirement.OPTIONAL;
    localObject = d.a(((com.truecaller.common.network.a.b)localObject).a(localAuthRequirement).a());
    return ((a.a)locala.a((y)localObject).b(a.a.class)).a(paramString1, paramString2, paramString3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.edge.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */