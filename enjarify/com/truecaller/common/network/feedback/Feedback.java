package com.truecaller.common.network.feedback;

import c.g.b.k;

public final class Feedback
{
  private String from;
  private String message;
  private String override;
  
  public Feedback(String paramString1, String paramString2)
  {
    from = paramString1;
    message = paramString2;
  }
  
  public final String getFrom()
  {
    return from;
  }
  
  public final String getMessage()
  {
    return message;
  }
  
  public final String getOverride()
  {
    return override;
  }
  
  public final void setFrom(String paramString)
  {
    k.b(paramString, "<set-?>");
    from = paramString;
  }
  
  public final void setMessage(String paramString)
  {
    k.b(paramString, "<set-?>");
    message = paramString;
  }
  
  public final void setOverride(String paramString)
  {
    override = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.feedback.Feedback
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */