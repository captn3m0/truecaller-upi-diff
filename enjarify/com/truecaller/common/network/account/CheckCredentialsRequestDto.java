package com.truecaller.common.network.account;

public final class CheckCredentialsRequestDto
{
  public static final CheckCredentialsRequestDto.a Companion;
  public static final String REASON_RECEIVED_UNAUTHORIZED = "received_unauthorized";
  public static final String REASON_RESTORED_FROM_ACCOUNT_MANAGER = "restored_from_account_manager";
  public static final String REASON_RESTORED_FROM_AUTOBACKUP = "restored_from_autobackup";
  public static final String REASON_RESTORED_FROM_FILE = "restored_from_file";
  private final CheckCredentialsDeviceDto device;
  private final String endpoint;
  private final String reason;
  
  static
  {
    CheckCredentialsRequestDto.a locala = new com/truecaller/common/network/account/CheckCredentialsRequestDto$a;
    locala.<init>((byte)0);
    Companion = locala;
  }
  
  public CheckCredentialsRequestDto(String paramString1, String paramString2, CheckCredentialsDeviceDto paramCheckCredentialsDeviceDto)
  {
    reason = paramString1;
    endpoint = paramString2;
    device = paramCheckCredentialsDeviceDto;
  }
  
  public final CheckCredentialsDeviceDto getDevice()
  {
    return device;
  }
  
  public final String getEndpoint()
  {
    return endpoint;
  }
  
  public final String getReason()
  {
    return reason;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.CheckCredentialsRequestDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */