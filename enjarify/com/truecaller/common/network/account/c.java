package com.truecaller.common.network.account;

import android.content.Context;
import android.os.Build.VERSION;
import c.u;
import com.truecaller.common.b.a;
import com.truecaller.common.h.au;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.h;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public final class c
  implements b
{
  private final Context a;
  private final com.truecaller.common.h.c b;
  private final h c;
  
  public c(Context paramContext, com.truecaller.common.h.c paramc, h paramh)
  {
    a = paramContext;
    b = paramc;
    c = paramh;
  }
  
  private final DeviceDto a(List paramList)
  {
    String str1 = com.truecaller.common.h.k.i(a);
    c.g.b.k.a(str1, "DeviceInfoUtils.getDeviceId(context)");
    String str2 = "Android";
    String str3 = Build.VERSION.RELEASE;
    String str4 = com.truecaller.common.h.k.b();
    String str5 = com.truecaller.common.h.k.a();
    Locale localLocale = Locale.getDefault();
    Object localObject1 = "Locale.getDefault()";
    c.g.b.k.a(localLocale, (String)localObject1);
    String str6 = localLocale.getLanguage();
    localLocale = null;
    boolean bool1;
    Object localObject2;
    if (paramList != null)
    {
      paramList = (Iterable)paramList;
      localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
      localObject1 = (Collection)localObject1;
      paramList = paramList.iterator();
      for (;;)
      {
        bool1 = paramList.hasNext();
        if (!bool1) {
          break;
        }
        localObject2 = nexth;
        if (localObject2 != null) {
          ((Collection)localObject1).add(localObject2);
        }
      }
      paramList = (List)localObject1;
      paramList = (List)localObject1;
      localObject1 = paramList;
      localObject1 = (Collection)paramList;
      boolean bool2 = ((Collection)localObject1).isEmpty() ^ true;
      if (!bool2) {
        paramList = null;
      }
      localObject2 = paramList;
    }
    else
    {
      bool1 = false;
      localObject2 = null;
    }
    paramList = new com/truecaller/common/network/account/DeviceDto;
    localObject1 = paramList;
    paramList.<init>(str1, str2, str3, str4, str5, str6, (List)localObject2);
    return paramList;
  }
  
  private static List b(List paramList)
  {
    if (paramList != null)
    {
      paramList = (Iterable)paramList;
      Object localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
      localObject1 = (Collection)localObject1;
      paramList = paramList.iterator();
      int i;
      for (;;)
      {
        boolean bool1 = paramList.hasNext();
        i = 1;
        if (!bool1) {
          break;
        }
        Object localObject2 = (SimInfo)paramList.next();
        Object localObject3 = (CharSequence)i;
        String str1 = null;
        if (localObject3 != null)
        {
          j = ((CharSequence)localObject3).length();
          if (j != 0)
          {
            j = 0;
            localObject3 = null;
            break label100;
          }
        }
        int j = 1;
        label100:
        int k = 4;
        label152:
        String str2;
        if (j != 0)
        {
          localObject3 = (CharSequence)d;
          if (localObject3 != null)
          {
            j = ((CharSequence)localObject3).length();
            if (j != 0)
            {
              j = 0;
              localObject3 = null;
              break label152;
            }
          }
          j = 1;
          if (j != 0)
          {
            localObject3 = e;
            j = ((String)localObject3).length();
            if (j < k)
            {
              i = 0;
              str2 = null;
            }
          }
        }
        if (i != 0)
        {
          String str3 = i;
          String str4 = d;
          str2 = e;
          i = str2.length();
          j = 3;
          String str5;
          if (i >= k)
          {
            str2 = e;
            localObject4 = "simInfo.mccMnc";
            c.g.b.k.a(str2, (String)localObject4);
            if (str2 != null)
            {
              str2 = str2.substring(0, j);
              str1 = "(this as java.lang.Strin…ing(startIndex, endIndex)";
              c.g.b.k.a(str2, str1);
              str5 = str2;
            }
            else
            {
              paramList = new c/u;
              paramList.<init>("null cannot be cast to non-null type java.lang.String");
              throw paramList;
            }
          }
          else
          {
            str5 = null;
          }
          str2 = e;
          i = str2.length();
          Object localObject5;
          if (i >= k)
          {
            localObject2 = e;
            str2 = "simInfo.mccMnc";
            c.g.b.k.a(localObject2, str2);
            if (localObject2 != null)
            {
              localObject2 = ((String)localObject2).substring(j);
              str2 = "(this as java.lang.String).substring(startIndex)";
              c.g.b.k.a(localObject2, str2);
              localObject5 = localObject2;
            }
            else
            {
              paramList = new c/u;
              paramList.<init>("null cannot be cast to non-null type java.lang.String");
              throw paramList;
            }
          }
          else
          {
            localObject5 = null;
          }
          localObject2 = new com/truecaller/common/network/account/SimDto;
          Object localObject4 = localObject2;
          ((SimDto)localObject2).<init>(str3, str4, str5, (String)localObject5, null);
        }
        else
        {
          bool1 = false;
          localObject2 = null;
        }
        if (localObject2 != null) {
          ((Collection)localObject1).add(localObject2);
        }
      }
      localObject1 = (List)localObject1;
      paramList = (List)localObject1;
      paramList = (Collection)localObject1;
      boolean bool2 = paramList.isEmpty() ^ i;
      if (bool2) {
        return (List)localObject1;
      }
    }
    return null;
  }
  
  public final InstallationDetailsDto a()
  {
    Object localObject1 = c.h();
    c.g.b.k.a(localObject1, "it");
    Object localObject2 = localObject1;
    localObject2 = (Collection)localObject1;
    boolean bool = ((Collection)localObject2).isEmpty() ^ true;
    if (!bool) {
      localObject1 = null;
    }
    localObject2 = new com/truecaller/common/network/account/InstallationDetailsDto;
    Object localObject3 = a.F();
    c.g.b.k.a(localObject3, "ApplicationBase.getAppBase()");
    localObject3 = ((a)localObject3).n();
    DeviceDto localDeviceDto = a((List)localObject1);
    AppDto localAppDto = new com/truecaller/common/network/account/AppDto;
    Integer localInteger1 = b.e().a;
    if (localInteger1 != null)
    {
      int i = localInteger1.intValue();
      Integer localInteger2 = b.e().b;
      if (localInteger2 != null)
      {
        int j = localInteger2.intValue();
        Integer localInteger3 = b.e().c;
        String str = b.f();
        localAppDto.<init>(i, j, localInteger3, str);
        localObject1 = b((List)localObject1);
        ((InstallationDetailsDto)localObject2).<init>((String)localObject3, localDeviceDto, localAppDto, (List)localObject1);
        return (InstallationDetailsDto)localObject2;
      }
      localObject1 = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject1).<init>("Minor build version is missing");
      throw ((Throwable)localObject1);
    }
    localObject1 = new java/lang/IllegalArgumentException;
    ((IllegalArgumentException)localObject1).<init>("Major build version is missing");
    throw ((Throwable)localObject1);
  }
  
  public final CheckCredentialsDeviceDto b()
  {
    CheckCredentialsDeviceDto localCheckCredentialsDeviceDto = new com/truecaller/common/network/account/CheckCredentialsDeviceDto;
    String str1 = com.truecaller.common.h.k.i(a);
    c.g.b.k.a(str1, "DeviceInfoUtils.getDeviceId(context)");
    String str2 = com.truecaller.common.h.k.a();
    String str3 = com.truecaller.common.h.k.b();
    localCheckCredentialsDeviceDto.<init>(str1, str2, str3);
    return localCheckCredentialsDeviceDto;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */