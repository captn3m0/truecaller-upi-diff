package com.truecaller.common.network.account;

public final class TokenErrorResponseDto
{
  public static final TokenErrorResponseDto.a Companion;
  public static final int STATUS_APPLICATION_NOT_SUPPORTED = 40002;
  public static final int STATUS_CLIENT_SECRET_WRONG = 40004;
  public static final int STATUS_INTERNAL_SERVER_ERROR = 50002;
  public static final int STATUS_INVALID_BODY_FORMAT = 40001;
  public static final int STATUS_INVALID_PHONE_NUMBER = 40003;
  private final String message;
  private final int status;
  
  static
  {
    TokenErrorResponseDto.a locala = new com/truecaller/common/network/account/TokenErrorResponseDto$a;
    locala.<init>((byte)0);
    Companion = locala;
  }
  
  public TokenErrorResponseDto(int paramInt, String paramString)
  {
    status = paramInt;
    message = paramString;
  }
  
  public final String getMessage()
  {
    return message;
  }
  
  public final int getStatus()
  {
    return status;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.TokenErrorResponseDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */