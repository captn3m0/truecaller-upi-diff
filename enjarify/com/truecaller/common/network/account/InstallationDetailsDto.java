package com.truecaller.common.network.account;

import java.util.List;

public final class InstallationDetailsDto
{
  private final AppDto app;
  private final DeviceDto device;
  private final String language;
  private final List sims;
  
  public InstallationDetailsDto(String paramString, DeviceDto paramDeviceDto, AppDto paramAppDto, List paramList)
  {
    language = paramString;
    device = paramDeviceDto;
    app = paramAppDto;
    sims = paramList;
  }
  
  public final AppDto getApp()
  {
    return app;
  }
  
  public final DeviceDto getDevice()
  {
    return device;
  }
  
  public final String getLanguage()
  {
    return language;
  }
  
  public final List getSims()
  {
    return sims;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.InstallationDetailsDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */