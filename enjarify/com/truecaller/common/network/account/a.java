package com.truecaller.common.network.account;

import c.g.b.k;
import com.truecaller.common.network.util.AuthRequirement;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.d;
import com.truecaller.common.network.util.h;
import java.util.ArrayList;
import java.util.List;
import okhttp3.v;
import okhttp3.y;

public final class a
{
  public static final a a;
  
  static
  {
    a locala = new com/truecaller/common/network/account/a;
    locala.<init>();
    a = locala;
  }
  
  public static a.a a(boolean paramBoolean)
  {
    Object localObject1 = new com/truecaller/common/network/util/a;
    ((com.truecaller.common.network.util.a)localObject1).<init>();
    Object localObject2 = KnownEndpoints.ACCOUNT;
    localObject1 = ((com.truecaller.common.network.util.a)localObject1).a((KnownEndpoints)localObject2).a(a.a.class);
    localObject2 = new com/truecaller/common/network/a/b;
    ((com.truecaller.common.network.a.b)localObject2).<init>();
    AuthRequirement localAuthRequirement = AuthRequirement.NONE;
    ((com.truecaller.common.network.a.b)localObject2).a(localAuthRequirement);
    ((com.truecaller.common.network.a.b)localObject2).a(paramBoolean);
    Object localObject3 = d.a((com.truecaller.common.network.a.b)localObject2);
    localObject3 = ((com.truecaller.common.network.util.a)localObject1).a((y)localObject3);
    localObject1 = (v)a.b.a;
    k.b(localObject1, "interceptor");
    localObject2 = b;
    if (localObject2 == null)
    {
      localObject2 = new java/util/ArrayList;
      ((ArrayList)localObject2).<init>();
      localObject2 = (List)localObject2;
      b = ((List)localObject2);
    }
    localObject2 = b;
    if (localObject2 != null) {
      ((List)localObject2).add(localObject1);
    }
    return (a.a)((com.truecaller.common.network.util.a)localObject3).b(a.a.class);
  }
  
  public static e.b a()
  {
    return ((a.a)h.a(KnownEndpoints.ACCOUNT, a.a.class)).b();
  }
  
  public static e.b a(CheckCredentialsRequestDto paramCheckCredentialsRequestDto)
  {
    k.b(paramCheckCredentialsRequestDto, "requestDto");
    com.truecaller.common.network.util.a locala = new com/truecaller/common/network/util/a;
    locala.<init>();
    Object localObject = KnownEndpoints.ACCOUNT;
    locala = locala.a((KnownEndpoints)localObject).a(a.a.class);
    localObject = new com/truecaller/common/network/a/b;
    ((com.truecaller.common.network.a.b)localObject).<init>();
    AuthRequirement localAuthRequirement = AuthRequirement.REQUIRED;
    ((com.truecaller.common.network.a.b)localObject).a(localAuthRequirement);
    ((com.truecaller.common.network.a.b)localObject).a(false);
    localObject = d.a((com.truecaller.common.network.a.b)localObject);
    return ((a.a)locala.a((y)localObject).b(a.a.class)).a(paramCheckCredentialsRequestDto);
  }
  
  public static e.b a(InstallationDetailsDto paramInstallationDetailsDto)
  {
    k.b(paramInstallationDetailsDto, "requestDto");
    return ((a.a)h.a(KnownEndpoints.ACCOUNT, a.a.class)).a(paramInstallationDetailsDto);
  }
  
  public static e.b b()
  {
    return ((a.a)h.a(KnownEndpoints.ACCOUNT, a.a.class)).a();
  }
  
  public static e.b c()
  {
    return ((a.a)h.a(KnownEndpoints.ACCOUNT, a.a.class)).c();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */