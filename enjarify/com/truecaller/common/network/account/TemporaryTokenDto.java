package com.truecaller.common.network.account;

public final class TemporaryTokenDto
{
  private final String token;
  
  public TemporaryTokenDto(String paramString)
  {
    token = paramString;
  }
  
  public final String getToken()
  {
    return token;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.TemporaryTokenDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */