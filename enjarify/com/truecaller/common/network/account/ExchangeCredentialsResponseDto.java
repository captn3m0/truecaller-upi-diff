package com.truecaller.common.network.account;

public final class ExchangeCredentialsResponseDto
{
  public static final ExchangeCredentialsResponseDto.a Companion;
  public static final String STATE_ACCEPTED = "accepted";
  public static final String STATE_EXCHANGED = "exchanged";
  private final String domain;
  private final String installationId;
  private final String state;
  private final long ttl;
  
  static
  {
    ExchangeCredentialsResponseDto.a locala = new com/truecaller/common/network/account/ExchangeCredentialsResponseDto$a;
    locala.<init>((byte)0);
    Companion = locala;
  }
  
  public ExchangeCredentialsResponseDto(String paramString1, String paramString2, String paramString3, long paramLong)
  {
    installationId = paramString1;
    state = paramString2;
    domain = paramString3;
    ttl = paramLong;
  }
  
  public final String getDomain()
  {
    return domain;
  }
  
  public final String getInstallationId()
  {
    return installationId;
  }
  
  public final String getState()
  {
    return state;
  }
  
  public final long getTtl()
  {
    return ttl;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.ExchangeCredentialsResponseDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */