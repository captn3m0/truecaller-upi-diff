package com.truecaller.common.network.account;

public final class ExchangeCredentialsRequestDto
{
  private final String installationId;
  
  public ExchangeCredentialsRequestDto(String paramString)
  {
    installationId = paramString;
  }
  
  public final String getInstallationId()
  {
    return installationId;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.account.ExchangeCredentialsRequestDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */