package com.truecaller.common.network;

import c.g.b.k;

public final class e$b
  extends e
{
  public final KnownDomain a;
  
  public e$b(KnownDomain paramKnownDomain)
  {
    super((byte)0);
    a = paramKnownDomain;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof b;
      if (bool1)
      {
        paramObject = (b)paramObject;
        KnownDomain localKnownDomain = a;
        paramObject = a;
        boolean bool2 = k.a(localKnownDomain, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    KnownDomain localKnownDomain = a;
    if (localKnownDomain != null) {
      return localKnownDomain.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Specific(domain=");
    KnownDomain localKnownDomain = a;
    localStringBuilder.append(localKnownDomain);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */