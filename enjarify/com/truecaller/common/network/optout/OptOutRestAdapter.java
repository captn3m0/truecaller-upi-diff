package com.truecaller.common.network.optout;

import c.f;
import c.g.a.a;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;

public final class OptOutRestAdapter
{
  public static final OptOutRestAdapter b;
  private static final f c = c.g.a((a)OptOutRestAdapter.b.a);
  
  static
  {
    Object localObject1 = new c.l.g[1];
    Object localObject2 = new c/g/b/u;
    c.l.b localb = w.a(OptOutRestAdapter.class);
    ((u)localObject2).<init>(localb, "endpoint", "getEndpoint()Lcom/truecaller/common/network/optout/OptOutRestAdapter$OptOutRestApi;");
    localObject2 = (c.l.g)w.a((t)localObject2);
    localObject1[0] = localObject2;
    a = (c.l.g[])localObject1;
    localObject1 = new com/truecaller/common/network/optout/OptOutRestAdapter;
    ((OptOutRestAdapter)localObject1).<init>();
    b = (OptOutRestAdapter)localObject1;
  }
  
  public static final e.b a()
  {
    return f().a("dm");
  }
  
  public static final e.b b()
  {
    return f().a("ads");
  }
  
  public static final e.b c()
  {
    return f().b("dm");
  }
  
  public static final e.b d()
  {
    return f().b("ads");
  }
  
  public static final e.b e()
  {
    return f().a();
  }
  
  private static OptOutRestAdapter.a f()
  {
    return (OptOutRestAdapter.a)c.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.optout.OptOutRestAdapter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */