package com.truecaller.common.network.optout;

import java.util.List;

public final class OptOutRestAdapter$OptOutsDto
{
  private final boolean consentRefresh;
  private final List optIns;
  private final List optOuts;
  
  public OptOutRestAdapter$OptOutsDto(List paramList1, List paramList2, boolean paramBoolean)
  {
    optOuts = paramList1;
    optIns = paramList2;
    consentRefresh = paramBoolean;
  }
  
  public final boolean getConsentRefresh()
  {
    return consentRefresh;
  }
  
  public final List getOptIns()
  {
    return optIns;
  }
  
  public final List getOptOuts()
  {
    return optOuts;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.optout.OptOutRestAdapter.OptOutsDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */