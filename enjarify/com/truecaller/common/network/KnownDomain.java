package com.truecaller.common.network;

public enum KnownDomain
{
  private final String value;
  
  static
  {
    KnownDomain[] arrayOfKnownDomain = new KnownDomain[2];
    KnownDomain localKnownDomain = new com/truecaller/common/network/KnownDomain;
    localKnownDomain.<init>("DOMAIN_REGION_1", 0, "eu");
    DOMAIN_REGION_1 = localKnownDomain;
    arrayOfKnownDomain[0] = localKnownDomain;
    localKnownDomain = new com/truecaller/common/network/KnownDomain;
    int i = 1;
    localKnownDomain.<init>("DOMAIN_OTHER_REGIONS", i, "noneu");
    DOMAIN_OTHER_REGIONS = localKnownDomain;
    arrayOfKnownDomain[i] = localKnownDomain;
    $VALUES = arrayOfKnownDomain;
  }
  
  private KnownDomain(String paramString1)
  {
    value = paramString1;
  }
  
  public final String getValue()
  {
    return value;
  }
  
  public final String toString()
  {
    return value;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.KnownDomain
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */