package com.truecaller.common.network.d;

import android.os.SystemClock;
import android.text.TextUtils;
import com.truecaller.common.network.util.KnownEndpoints;
import d.c;
import d.e;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import okhttp3.ab;
import okhttp3.ad;
import okhttp3.ae;
import okhttp3.t;
import okhttp3.u;
import okhttp3.v;
import okhttp3.v.a;
import okhttp3.w;

public final class a
  implements v
{
  private static void a(StringBuilder paramStringBuilder, t paramt)
  {
    if (paramt != null)
    {
      Object localObject1 = a;
      int i = localObject1.length / 2;
      if (i != 0)
      {
        localObject1 = new java/util/TreeSet;
        Object localObject2 = String.CASE_INSENSITIVE_ORDER;
        ((TreeSet)localObject1).<init>((Comparator)localObject2);
        int j = 0;
        localObject2 = null;
        Object localObject3 = a;
        int k = localObject3.length / 2;
        String str1;
        while (j < k)
        {
          str1 = paramt.a(j);
          ((TreeSet)localObject1).add(str1);
          j += 1;
        }
        localObject1 = Collections.unmodifiableSet((Set)localObject1).iterator();
        do
        {
          boolean bool1 = ((Iterator)localObject1).hasNext();
          if (!bool1) {
            break;
          }
          localObject2 = (String)((Iterator)localObject1).next();
          localObject3 = paramt.b((String)localObject2);
        } while (localObject3 == null);
        localObject3 = ((List)localObject3).iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject3).hasNext();
          if (!bool2) {
            break;
          }
          str1 = (String)((Iterator)localObject3).next();
          paramStringBuilder.append("\n    ");
          paramStringBuilder.append((String)localObject2);
          String str2 = ": ";
          paramStringBuilder.append(str2);
          paramStringBuilder.append(str1);
        }
        return;
      }
    }
  }
  
  private static void a(ab paramab, boolean paramBoolean, long paramLong)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append("--> ");
    Object localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(" ");
    localObject = a;
    localStringBuilder.append(localObject);
    localObject = " time spent: ";
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(paramLong);
    String str = "ms";
    localStringBuilder.append(str);
    if (paramBoolean)
    {
      paramab = c;
      a(localStringBuilder, paramab);
    }
    paramab = new Object[1];
    str = localStringBuilder.toString();
    paramab[0] = str;
    com.truecaller.debug.log.a.a(paramab);
  }
  
  public final ad intercept(v.a parama)
  {
    localObject1 = parama.a();
    localObject2 = REQUESTurlb;
    localObject3 = a.b;
    bool = TextUtils.equals((CharSequence)localObject2, (CharSequence)localObject3);
    l1 = SystemClock.elapsedRealtime();
    i = 1;
    for (;;)
    {
      try
      {
        parama = parama.a((ab)localObject1);
        localObject1 = a;
        l2 = SystemClock.elapsedRealtime() - l1;
        a((ab)localObject1, bool, l2);
        localObject4 = b;
        localObject5 = a;
        localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localObject6 = "<-- ";
        localStringBuilder.append((String)localObject6);
        localStringBuilder.append((String)localObject4);
        localObject4 = " ";
        localStringBuilder.append((String)localObject4);
        localStringBuilder.append(localObject5);
        localObject4 = " status code: ";
        localStringBuilder.append((String)localObject4);
        int j = c;
        localStringBuilder.append(j);
        if (bool)
        {
          localObject4 = f;
          a(localStringBuilder, (t)localObject4);
        }
      }
      catch (Exception parama)
      {
        Object localObject4;
        Object localObject5;
        StringBuilder localStringBuilder;
        Object localObject6;
        long l2 = SystemClock.elapsedRealtime() - l1;
        a((ab)localObject1, bool, l2);
        localObject2 = new Object[i];
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>("<-- ");
        String str = b;
        ((StringBuilder)localObject3).append(str);
        ((StringBuilder)localObject3).append(" ");
        localObject1 = a;
        ((StringBuilder)localObject3).append(localObject1);
        ((StringBuilder)localObject3).append(" error:");
        localObject1 = parama.toString();
        ((StringBuilder)localObject3).append((String)localObject1);
        localObject1 = ((StringBuilder)localObject3).toString();
        localObject2[0] = localObject1;
        com.truecaller.debug.log.a.a((Object[])localObject2);
        throw parama;
      }
      try
      {
        localObject4 = g;
        if (localObject4 != null)
        {
          localObject5 = ((ae)localObject4).c();
          long l3 = Long.MAX_VALUE;
          ((e)localObject5).b(l3);
          localObject5 = ((e)localObject5).b();
          localObject4 = ((ae)localObject4).a();
          localObject6 = "UTF-8";
          localObject6 = Charset.forName((String)localObject6);
          if (localObject4 != null) {
            localObject6 = ((w)localObject4).a((Charset)localObject6);
          }
          localObject4 = "\n    ";
          localStringBuilder.append((String)localObject4);
          localObject4 = ((c)localObject5).u();
          localObject4 = ((c)localObject4).a((Charset)localObject6);
          localStringBuilder.append((String)localObject4);
        }
      }
      catch (IOException|UnsupportedCharsetException localIOException) {}
    }
    localObject4 = new Object[i];
    localObject5 = localStringBuilder.toString();
    localObject4[0] = localObject5;
    com.truecaller.debug.log.a.a((Object[])localObject4);
    return parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */