package com.truecaller.common.network.d;

import android.os.Build.VERSION;
import okhttp3.ab;
import okhttp3.ab.a;
import okhttp3.ad;
import okhttp3.v;
import okhttp3.v.a;

public final class f
  implements v
{
  private final String a;
  private final String b;
  private final String c;
  
  public f(String paramString1, String paramString2)
  {
    b = paramString1;
    c = paramString2;
    paramString1 = Build.VERSION.RELEASE;
    c.g.b.k.a(paramString1, "Build.VERSION.RELEASE");
    paramString1 = (CharSequence)paramString1;
    c.n.k localk = new c/n/k;
    localk.<init>("[^\\x20-\\x7E]");
    paramString1 = localk.a(paramString1, "");
    a = paramString1;
  }
  
  public final ad intercept(v.a parama)
  {
    c.g.b.k.b(parama, "chain");
    Object localObject1 = parama.a().e().b("User-Agent");
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    String str = b;
    ((StringBuilder)localObject2).append(str);
    ((StringBuilder)localObject2).append('/');
    str = c;
    ((StringBuilder)localObject2).append(str);
    ((StringBuilder)localObject2).append(" (Android;");
    str = a;
    ((StringBuilder)localObject2).append(str);
    ((StringBuilder)localObject2).append(')');
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1 = ((ab.a)localObject1).b("User-Agent", (String)localObject2).a();
    parama = parama.a((ab)localObject1);
    c.g.b.k.a(parama, "chain.proceed(userAgent)");
    return parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.d.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */