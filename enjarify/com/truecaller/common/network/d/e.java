package com.truecaller.common.network.d;

import c.g.b.k;
import com.truecaller.common.network.util.b;
import com.truecaller.common.network.util.c;
import java.util.concurrent.TimeUnit;
import okhttp3.ab;
import okhttp3.ad;
import okhttp3.u;
import okhttp3.v;
import okhttp3.v.a;

public final class e
  implements v
{
  private final long a;
  
  public e(long paramLong)
  {
    a = paramLong;
  }
  
  public final ad intercept(v.a parama)
  {
    k.b(parama, "chain");
    Object localObject1 = parama.a();
    Object localObject2 = ((ab)localObject1).a();
    boolean bool;
    if (localObject2 != null)
    {
      localObject2 = ((u)localObject2).f();
    }
    else
    {
      bool = false;
      localObject2 = null;
    }
    long l1 = a;
    if (localObject2 != null)
    {
      long l2 = c.a((String)localObject2, l1);
      long l3 = 0L;
      bool = l2 < l3;
      if (bool) {
        l1 -= l2;
      }
      bool = l1 < l3;
      if (!bool)
      {
        parama = new com/truecaller/common/network/util/b;
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("Timed out after DNS lookup for ");
        long l4 = a;
        ((StringBuilder)localObject1).append(l4);
        ((StringBuilder)localObject1).append(" milliseconds");
        localObject1 = ((StringBuilder)localObject1).toString();
        parama.<init>((String)localObject1);
        throw ((Throwable)parama);
      }
    }
    int i = (int)l1;
    TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
    parama = parama.a(i, localTimeUnit).a((ab)localObject1);
    k.a(parama, "chain.withConnectTimeout…        .proceed(request)");
    return parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.d.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */