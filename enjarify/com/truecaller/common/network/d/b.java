package com.truecaller.common.network.d;

import c.g.b.k;
import c.n.m;
import okhttp3.ab;
import okhttp3.ab.a;
import okhttp3.ac;
import okhttp3.ad;
import okhttp3.v;
import okhttp3.v.a;

public final class b
  implements v
{
  public final ad intercept(v.a parama)
  {
    k.b(parama, "chain");
    Object localObject1 = parama.a();
    k.a(localObject1, "chain.request()");
    ac localac = ((ab)localObject1).c();
    if (localac != null)
    {
      Object localObject2 = "gzip";
      Object localObject3 = ((ab)localObject1).a("Content-Encoding");
      boolean bool1 = true;
      boolean bool2 = m.a((String)localObject2, (String)localObject3, bool1);
      if (!bool2) {
        localac = null;
      }
      if (localac != null)
      {
        localObject2 = ((ab)localObject1).e();
        localObject1 = ((ab)localObject1).b();
        localObject3 = new com/truecaller/common/network/d/b$a;
        ((b.a)localObject3).<init>(localac);
        localObject3 = (ac)localObject3;
        localObject1 = ((ab.a)localObject2).a((String)localObject1, (ac)localObject3).a();
        parama = parama.a((ab)localObject1);
        k.a(parama, "chain.proceed(it)");
        k.a(parama, "originalRequest.newBuild…let { chain.proceed(it) }");
        return parama;
      }
    }
    parama = parama.a((ab)localObject1);
    k.a(parama, "chain.proceed(originalRequest)");
    return parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.d.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */