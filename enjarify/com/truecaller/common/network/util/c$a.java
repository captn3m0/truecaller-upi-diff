package com.truecaller.common.network.util;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import java.net.InetAddress;
import kotlinx.coroutines.ag;

final class c$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  c$a(String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/common/network/util/c$a;
    String str = b;
    locala.<init>(str, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        try
        {
          paramObject = b;
          paramObject = InetAddress.getAllByName((String)paramObject);
        }
        catch (Exception localException)
        {
          paramObject = x.a;
        }
        return paramObject;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.util.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */