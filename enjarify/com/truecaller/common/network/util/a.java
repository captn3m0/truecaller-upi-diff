package com.truecaller.common.network.util;

import c.g.b.k;
import com.truecaller.common.network.b;
import com.truecaller.common.network.d.e;
import e.f.a;
import e.s;
import e.s.a;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import okhttp3.u;
import okhttp3.v;
import okhttp3.y;
import okhttp3.y.a;

public final class a
{
  public List a;
  public List b;
  private u c;
  private String d;
  private f.a e;
  private y f;
  private int g;
  private TimeUnit h;
  private boolean i;
  
  private final s.a a()
  {
    s.a locala = new e/s$a;
    locala.<init>();
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "endpoint";
      k.a((String)localObject2);
    }
    locala = locala.a((u)localObject1);
    localObject1 = e;
    if (localObject1 == null) {
      localObject1 = (f.a)e.a.a.a.a();
    }
    locala = locala.a((f.a)localObject1);
    k.a(locala, "Retrofit.Builder()\n     …onverterFactory.create())");
    localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = d.d;
      localObject1 = d.b();
    }
    int j = g;
    if (j > 0)
    {
      localObject2 = h;
      if (localObject2 != null)
      {
        j = 1;
        break label106;
      }
    }
    j = 0;
    Object localObject2 = null;
    label106:
    Object localObject3 = a;
    if (localObject3 == null)
    {
      localObject3 = b;
      if (localObject3 == null)
      {
        localObject3 = d;
        if ((localObject3 == null) && (j == 0)) {
          break label414;
        }
      }
    }
    localObject1 = ((y)localObject1).c();
    localObject3 = d;
    Object localObject4;
    if (localObject3 != null)
    {
      localObject3 = ((Iterable)b.a((String)localObject3)).iterator();
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject3).hasNext();
        if (!bool1) {
          break;
        }
        localObject4 = (v)((Iterator)localObject3).next();
        ((y.a)localObject1).a((v)localObject4);
      }
      localObject3 = new com/truecaller/common/network/d/a;
      ((com.truecaller.common.network.d.a)localObject3).<init>();
      localObject3 = (v)localObject3;
      ((y.a)localObject1).a((v)localObject3);
    }
    localObject3 = a;
    if (localObject3 != null)
    {
      localObject4 = ((y.a)localObject1).b();
      localObject3 = (Collection)localObject3;
      ((List)localObject4).addAll((Collection)localObject3);
    }
    localObject3 = b;
    if (localObject3 != null)
    {
      localObject4 = ((y.a)localObject1).c();
      localObject3 = (Collection)localObject3;
      ((List)localObject4).addAll((Collection)localObject3);
    }
    if (j != 0)
    {
      localObject2 = h;
      if (localObject2 != null)
      {
        boolean bool2 = i;
        if (bool2)
        {
          localObject3 = new com/truecaller/common/network/d/e;
          int k = g;
          long l1 = k;
          l1 = ((TimeUnit)localObject2).toMillis(l1);
          ((e)localObject3).<init>(l1);
          localObject3 = (v)localObject3;
          ((y.a)localObject1).a((v)localObject3);
        }
        else
        {
          int m = g;
          long l2 = m;
          ((y.a)localObject1).a(l2, (TimeUnit)localObject2);
        }
      }
    }
    localObject1 = ((y.a)localObject1).d();
    k.a(localObject1, "build()");
    localObject2 = "client.newBuilder().run …    build()\n            }";
    k.a(localObject1, (String)localObject2);
    label414:
    locala = locala.a((y)localObject1);
    k.a(locala, "builder.client(client)");
    return locala;
  }
  
  public final a a(int paramInt, TimeUnit paramTimeUnit, boolean paramBoolean)
  {
    g = paramInt;
    h = paramTimeUnit;
    i = paramBoolean;
    return this;
  }
  
  public final a a(KnownEndpoints paramKnownEndpoints)
  {
    k.b(paramKnownEndpoints, "endpoint");
    paramKnownEndpoints = paramKnownEndpoints.url();
    c = paramKnownEndpoints;
    return this;
  }
  
  public final a a(f.a parama)
  {
    k.b(parama, "factory");
    e = parama;
    return this;
  }
  
  public final a a(Class paramClass)
  {
    k.b(paramClass, "api");
    paramClass = paramClass.getSimpleName();
    k.a(paramClass, "api.simpleName");
    d = paramClass;
    return this;
  }
  
  public final a a(y paramy)
  {
    k.b(paramy, "client");
    f = paramy;
    return this;
  }
  
  public final Object b(Class paramClass)
  {
    k.b(paramClass, "api");
    return a().b().a(paramClass);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.util.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */