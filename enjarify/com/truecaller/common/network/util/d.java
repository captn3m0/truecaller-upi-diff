package com.truecaller.common.network.util;

import c.f;
import c.g.b.k;
import c.g.b.t;
import c.g.b.u;
import c.g.b.w;
import com.truecaller.common.network.d.c;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.concurrent.TimeUnit;
import okhttp3.q.a;
import okhttp3.v;
import okhttp3.y;
import okhttp3.y.a;

public final class d
{
  public static c b;
  public static com.truecaller.common.network.f.a c;
  public static final d d;
  private static final f e = c.g.a((c.g.a.a)d.b.a);
  private static final f f = c.g.a((c.g.a.a)d.a.a);
  
  static
  {
    Object localObject1 = new c.l.g[2];
    Object localObject2 = new c/g/b/u;
    c.l.b localb = w.a(d.class);
    ((u)localObject2).<init>(localb, "publicHttpClient", "getPublicHttpClient()Lokhttp3/OkHttpClient;");
    localObject2 = (c.l.g)w.a((t)localObject2);
    localObject1[0] = localObject2;
    localObject2 = new c/g/b/u;
    localb = w.a(d.class);
    ((u)localObject2).<init>(localb, "authorizedHttpClient", "getAuthorizedHttpClient$common_network_release()Lokhttp3/OkHttpClient;");
    localObject2 = (c.l.g)w.a((t)localObject2);
    localObject1[1] = localObject2;
    a = (c.l.g[])localObject1;
    localObject1 = new com/truecaller/common/network/util/d;
    ((d)localObject1).<init>();
    d = (d)localObject1;
  }
  
  public static final y a()
  {
    return (y)e.b();
  }
  
  public static final y a(com.truecaller.common.network.a.b paramb)
  {
    k.b(paramb, "config");
    paramb = b(paramb).d();
    k.a(paramb, "createOkHttpClientBuilder(config).build()");
    return paramb;
  }
  
  public static void a(c paramc)
  {
    k.b(paramc, "<set-?>");
    b = paramc;
  }
  
  public static void a(com.truecaller.common.network.f.a parama)
  {
    k.b(parama, "<set-?>");
    c = parama;
  }
  
  public static final y.a b(com.truecaller.common.network.a.b paramb)
  {
    k.b(paramb, "config");
    y.a locala = new okhttp3/y$a;
    locala.<init>();
    Object localObject1 = TimeUnit.SECONDS;
    locala.a(10, (TimeUnit)localObject1);
    localObject1 = TimeUnit.SECONDS;
    long l = 20;
    locala.b(l, (TimeUnit)localObject1);
    paramb = paramb.b().iterator();
    for (;;)
    {
      boolean bool1 = paramb.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (com.truecaller.common.network.a.a)paramb.next();
      Object localObject2 = b;
      if (localObject2 == null)
      {
        str = "interceptorFactory";
        k.a(str);
      }
      String str = "attr";
      k.a(localObject1, str);
      localObject2 = ((c)localObject2).a((com.truecaller.common.network.a.a)localObject1);
      if (localObject2 != null)
      {
        boolean bool2 = a;
        if (bool2) {
          locala.b((v)localObject2);
        } else {
          locala.a((v)localObject2);
        }
        localObject2 = b;
        if (localObject2 != null)
        {
          localObject1 = b;
          locala.a((List)localObject1);
        }
      }
    }
    paramb = new com/truecaller/common/network/d/d;
    paramb.<init>();
    paramb = (v)paramb;
    locala.a(paramb);
    paramb = c;
    if (paramb == null)
    {
      localObject1 = "httpAnalyticsLoggerFactory";
      k.a((String)localObject1);
    }
    paramb = (q.a)paramb;
    locala.a(paramb);
    return locala;
  }
  
  public static y b()
  {
    return (y)f.b();
  }
  
  public static final y c()
  {
    com.truecaller.common.network.a.b localb = new com/truecaller/common/network/a/b;
    localb.<init>();
    return a(localb);
  }
  
  public static final y.a d()
  {
    com.truecaller.common.network.a.b localb = new com/truecaller/common/network/a/b;
    localb.<init>();
    return b(localb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.util.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */