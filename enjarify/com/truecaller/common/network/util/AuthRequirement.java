package com.truecaller.common.network.util;

public enum AuthRequirement
{
  static
  {
    AuthRequirement[] arrayOfAuthRequirement = new AuthRequirement[3];
    AuthRequirement localAuthRequirement = new com/truecaller/common/network/util/AuthRequirement;
    localAuthRequirement.<init>("REQUIRED", 0);
    REQUIRED = localAuthRequirement;
    arrayOfAuthRequirement[0] = localAuthRequirement;
    localAuthRequirement = new com/truecaller/common/network/util/AuthRequirement;
    int i = 1;
    localAuthRequirement.<init>("OPTIONAL", i);
    OPTIONAL = localAuthRequirement;
    arrayOfAuthRequirement[i] = localAuthRequirement;
    localAuthRequirement = new com/truecaller/common/network/util/AuthRequirement;
    i = 2;
    localAuthRequirement.<init>("NONE", i);
    NONE = localAuthRequirement;
    arrayOfAuthRequirement[i] = localAuthRequirement;
    $VALUES = arrayOfAuthRequirement;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.util.AuthRequirement
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */