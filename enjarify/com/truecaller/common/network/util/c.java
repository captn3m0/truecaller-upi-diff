package com.truecaller.common.network.util;

import c.g.a.m;
import java.net.UnknownHostException;
import kotlinx.coroutines.f;

public final class c
{
  public static final c a;
  
  static
  {
    c localc = new com/truecaller/common/network/util/c;
    localc.<init>();
    a = localc;
  }
  
  public static long a(String paramString, long paramLong)
  {
    if (paramString != null) {
      try
      {
        long l = System.currentTimeMillis();
        Object localObject = new com/truecaller/common/network/util/c$b;
        ((c.b)localObject).<init>(paramLong, paramString, null);
        localObject = (m)localObject;
        f.a((m)localObject);
        return System.currentTimeMillis() - l;
      }
      catch (Exception paramString)
      {
        UnknownHostException localUnknownHostException = new java/net/UnknownHostException;
        localUnknownHostException.<init>();
        paramString = (Throwable)paramString;
        localUnknownHostException.initCause(paramString);
        throw ((Throwable)localUnknownHostException);
      }
    }
    paramString = new java/net/UnknownHostException;
    paramString.<init>("hostname == null");
    throw ((Throwable)paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.util.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */