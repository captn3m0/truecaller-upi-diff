package com.truecaller.common.network.util;

import c.g.b.k;

public final class h
{
  public static final h a;
  
  static
  {
    h localh = new com/truecaller/common/network/util/h;
    localh.<init>();
    a = localh;
  }
  
  public static final Object a(KnownEndpoints paramKnownEndpoints, Class paramClass)
  {
    k.b(paramKnownEndpoints, "endpoint");
    k.b(paramClass, "api");
    a locala = new com/truecaller/common/network/util/a;
    locala.<init>();
    return locala.a(paramKnownEndpoints).a(paramClass).b(paramClass);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.util.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */