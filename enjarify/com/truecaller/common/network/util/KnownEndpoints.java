package com.truecaller.common.network.util;

import c.g.b.k;
import c.l;
import com.truecaller.common.network.KnownDomain;
import okhttp3.u;
import okhttp3.u.a;

public enum KnownEndpoints
{
  public static final KnownEndpoints.a Companion;
  private static boolean isStaging;
  private final String euHost;
  private final String key;
  private final String nonEuHost;
  private final String stagingEndpoint;
  private final Integer stagingPort;
  
  static
  {
    Object localObject1 = new KnownEndpoints[34];
    Object localObject2 = new com/truecaller/common/network/util/KnownEndpoints;
    Integer localInteger = Integer.valueOf(16080);
    ((KnownEndpoints)localObject2).<init>("SEARCH", 0, "search5", "search5-eu", "search5-noneu", null, localInteger, 8, null);
    SEARCH = (KnownEndpoints)localObject2;
    localObject1[0] = localObject2;
    KnownEndpoints localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localKnownEndpoints.<init>("REQUEST", 1, "request3", "request3-eu", "request3-noneu", null, null, 24, null);
    REQUEST = localKnownEndpoints;
    localObject1[1] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    int i = 24;
    localKnownEndpoints.<init>("CONTACTREQUEST", 2, "contact-request", "contact-request-eu", "contact-request-noneu", null, null, i, null);
    CONTACTREQUEST = localKnownEndpoints;
    localObject1[2] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localKnownEndpoints.<init>("PREMIUM", 3, "premium", "premium-eu", "premium-noneu", null, null, i, null);
    PREMIUM = localKnownEndpoints;
    localObject1[3] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localObject2 = Integer.valueOf(19010);
    i = 8;
    localKnownEndpoints.<init>("CONTACT", 4, "contact-upload4", "contact-upload4-eu", "contact-upload4-noneu", null, (Integer)localObject2, i, null);
    CONTACT = localKnownEndpoints;
    localObject1[4] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localObject2 = Integer.valueOf(19000);
    localKnownEndpoints.<init>("NOTIFICATION", 5, "notifications5", "notifications5-eu", "notifications5-noneu", null, (Integer)localObject2, i, null);
    NOTIFICATION = localKnownEndpoints;
    localObject1[5] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localObject2 = Integer.valueOf(23250);
    localKnownEndpoints.<init>("BATCHLOG", 6, "batchlogging4", "batchlogging4-eu", "batchlogging4-noneu", null, (Integer)localObject2, i, null);
    BATCHLOG = localKnownEndpoints;
    localObject1[6] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localObject2 = Integer.valueOf(28220);
    localKnownEndpoints.<init>("PHONEBOOK", 7, "phonebook5", "phonebook5-eu", "phonebook5-noneu", null, (Integer)localObject2, i, null);
    PHONEBOOK = localKnownEndpoints;
    localObject1[7] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localObject2 = Integer.valueOf(20150);
    localKnownEndpoints.<init>("TAGGING", 8, "tagging5", "tagging5-eu", "tagging5-noneu", null, (Integer)localObject2, i, null);
    TAGGING = localKnownEndpoints;
    localObject1[8] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localObject2 = Integer.valueOf(16010);
    localKnownEndpoints.<init>("FILTER", 9, "filter-store4", "filter-store4-eu", "filter-store4-noneu", null, (Integer)localObject2, i, null);
    FILTER = localKnownEndpoints;
    localObject1[9] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localObject2 = Integer.valueOf(19050);
    localKnownEndpoints.<init>("EDGE", 10, "edge-locations5", "edge-locations5-eu", "edge-locations5-noneu", null, (Integer)localObject2, i, null);
    EDGE = localKnownEndpoints;
    localObject1[10] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localKnownEndpoints.<init>("FEEDBACK", 11, "feedback", "feedback-eu", "feedback-noneu", null, null, 24, null);
    FEEDBACK = localKnownEndpoints;
    localObject1[11] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localObject2 = Integer.valueOf(17010);
    i = 8;
    localKnownEndpoints.<init>("API", 12, "api4", "api4-eu", "api4-noneu", null, (Integer)localObject2, i, null);
    API = localKnownEndpoints;
    localObject1[12] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localObject2 = Integer.valueOf(19060);
    localKnownEndpoints.<init>("ADS", 13, "ads5", "ads5-eu", "ads5-noneu", null, (Integer)localObject2, i, null);
    ADS = localKnownEndpoints;
    localObject1[13] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localObject2 = Integer.valueOf(16020);
    localKnownEndpoints.<init>("CALLMEBACK", 14, "callmeback", "callmeback-eu", "callmeback-noneu", null, (Integer)localObject2, i, null);
    CALLMEBACK = localKnownEndpoints;
    localObject1[14] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localObject2 = Integer.valueOf(16050);
    localKnownEndpoints.<init>("USERAPPS", 15, "userapps", "userapps-eu", "userapps-noneu", null, (Integer)localObject2, i, null);
    USERAPPS = localKnownEndpoints;
    localObject1[15] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localObject2 = Integer.valueOf(19080);
    localKnownEndpoints.<init>("REFERRAL", 16, "referrals", "referrals-eu", "referrals-noneu", null, (Integer)localObject2, i, null);
    REFERRAL = localKnownEndpoints;
    localObject1[16] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localKnownEndpoints.<init>("PROFILE", 17, "profile4", "profile4-eu", "profile4-noneu", null, null, 24, null);
    PROFILE = localKnownEndpoints;
    localObject1[17] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localObject2 = Integer.valueOf(16350);
    localKnownEndpoints.<init>("LEADGEN", 18, "leadgen", "leadgen-eu", "leadgen-noneu", null, (Integer)localObject2, 8, null);
    LEADGEN = localKnownEndpoints;
    localObject1[18] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localKnownEndpoints.<init>("BACKUP", 19, "backup", "backup-eu", "backup-noneu", null, null, 24, null);
    BACKUP = localKnownEndpoints;
    localObject1[19] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localObject2 = Integer.valueOf(16610);
    localKnownEndpoints.<init>("TOPSPAMMERS", 20, "topspammers", "topspammers-eu", "topspammers-noneu", null, (Integer)localObject2, 8, null);
    TOPSPAMMERS = localKnownEndpoints;
    localObject1[20] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    i = 24;
    localKnownEndpoints.<init>("PUSHID", 21, "pushid", "pushid-eu", "pushid-noneu", null, null, i, null);
    PUSHID = localKnownEndpoints;
    localObject1[21] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localKnownEndpoints.<init>("IMAGES", 22, "images", "images-eu", "images-noneu", null, null, i, null);
    IMAGES = localKnownEndpoints;
    localObject1[22] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localKnownEndpoints.<init>("USERARCHIVE", 23, "user-archive", "user-archive-eu", "user-archive-noneu", null, null, i, null);
    USERARCHIVE = localKnownEndpoints;
    localObject1[23] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localObject2 = Integer.valueOf(18870);
    localKnownEndpoints.<init>("ACCOUNT", 24, "account", "account-eu", "account-noneu", null, (Integer)localObject2, 8, null);
    ACCOUNT = localKnownEndpoints;
    localObject1[24] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    i = 24;
    localKnownEndpoints.<init>("FLASH", 25, "flash", "flash-eu", "flash-noneu", null, null, i, null);
    FLASH = localKnownEndpoints;
    localObject1[25] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localKnownEndpoints.<init>("OPTOUT", 26, "opt-out", "opt-out-eu", "opt-out-noneu", null, null, i, null);
    OPTOUT = localKnownEndpoints;
    localObject1[26] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localKnownEndpoints.<init>("PRESENCE_GRPC", 27, "presence-grpc", "presence-grpc-eu", "presence-grpc-noneu", null, null, i, null);
    PRESENCE_GRPC = localKnownEndpoints;
    localObject1[27] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localKnownEndpoints.<init>("PROFILE_VIEW", 28, "profile-view", "profile-view-eu", "profile-view-noneu", null, null, i, null);
    PROFILE_VIEW = localKnownEndpoints;
    localObject1[28] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localKnownEndpoints.<init>("SPAM_URL", 29, "link-reports", "link-reports-eu", "link-reports-noneu", null, null, i, null);
    SPAM_URL = localKnownEndpoints;
    localObject1[29] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localKnownEndpoints.<init>("MESSENGER", 30, "messenger", "messenger-eu", "messenger-noneu", null, null, i, null);
    MESSENGER = localKnownEndpoints;
    localObject1[30] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localKnownEndpoints.<init>("CLIENT_SEARCH", 31, "client-search", "client-search-eu", "client-search-noneu", null, null, i, null);
    CLIENT_SEARCH = localKnownEndpoints;
    localObject1[31] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localKnownEndpoints.<init>("LAST_ACTIVITY", 32, "lastactivity", "lastactivity-eu", "lastactivity-noneu", null, null, i, null);
    LAST_ACTIVITY = localKnownEndpoints;
    localObject1[32] = localKnownEndpoints;
    localKnownEndpoints = new com/truecaller/common/network/util/KnownEndpoints;
    localKnownEndpoints.<init>("VOIP", 33, "voip", "voip-eu", "voip-noneu", null, null, i, null);
    VOIP = localKnownEndpoints;
    localObject1[33] = localKnownEndpoints;
    $VALUES = (KnownEndpoints[])localObject1;
    localObject1 = new com/truecaller/common/network/util/KnownEndpoints$a;
    ((KnownEndpoints.a)localObject1).<init>((byte)0);
    Companion = (KnownEndpoints.a)localObject1;
  }
  
  private KnownEndpoints(String paramString2, String paramString3, String paramString4, String paramString5, Integer paramInteger)
  {
    key = paramString2;
    euHost = paramString3;
    nonEuHost = paramString4;
    stagingEndpoint = paramString5;
    stagingPort = paramInteger;
  }
  
  public static final void switchToStaging()
  {
    access$setStaging$cp(true);
  }
  
  public final String getHost(KnownDomain paramKnownDomain)
  {
    k.b(paramKnownDomain, "domain");
    Object localObject = f.a;
    int i = paramKnownDomain.ordinal();
    i = localObject[i];
    switch (i)
    {
    default: 
      paramKnownDomain = new c/l;
      paramKnownDomain.<init>();
      throw paramKnownDomain;
    case 2: 
      paramKnownDomain = nonEuHost;
      break;
    case 1: 
      paramKnownDomain = euHost;
    }
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    ((StringBuilder)localObject).append(paramKnownDomain);
    ((StringBuilder)localObject).append(".truecaller.com");
    return ((StringBuilder)localObject).toString();
  }
  
  public final String getKey()
  {
    return key;
  }
  
  public final u url()
  {
    boolean bool = isStaging;
    if (bool)
    {
      localObject1 = stagingPort;
      if (localObject1 != null)
      {
        localObject1 = stagingEndpoint;
        if (localObject1 != null)
        {
          localObject1 = new okhttp3/u$a;
          ((u.a)localObject1).<init>();
          localObject1 = ((u.a)localObject1).a("http");
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          str = stagingEndpoint;
          ((StringBuilder)localObject2).append(str);
          ((StringBuilder)localObject2).append(".truecaller.net");
          localObject2 = ((StringBuilder)localObject2).toString();
          localObject1 = ((u.a)localObject1).b((String)localObject2);
          int i = stagingPort.intValue();
          localObject1 = ((u.a)localObject1).a(i).b();
          k.a(localObject1, "HttpUrl.Builder()\n      …\n                .build()");
          return (u)localObject1;
        }
      }
    }
    Object localObject1 = new okhttp3/u$a;
    ((u.a)localObject1).<init>();
    localObject1 = ((u.a)localObject1).a("https");
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    String str = key;
    ((StringBuilder)localObject2).append(str);
    ((StringBuilder)localObject2).append(".truecaller.com");
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1 = ((u.a)localObject1).b((String)localObject2).b();
    k.a(localObject1, "HttpUrl.Builder()\n      …\n                .build()");
    return (u)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.util.KnownEndpoints
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */