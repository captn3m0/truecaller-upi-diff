package com.truecaller.common.network.userarchive;

public class DownloadDto
{
  public long expiration;
  public String url;
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.userarchive.DownloadDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */