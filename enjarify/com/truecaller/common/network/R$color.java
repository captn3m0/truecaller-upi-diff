package com.truecaller.common.network;

public final class R$color
{
  public static final int abc_background_cache_hint_selector_material_dark = 2131099658;
  public static final int abc_background_cache_hint_selector_material_light = 2131099659;
  public static final int abc_btn_colored_borderless_text_material = 2131099660;
  public static final int abc_btn_colored_text_material = 2131099661;
  public static final int abc_color_highlight_material = 2131099662;
  public static final int abc_hint_foreground_material_dark = 2131099663;
  public static final int abc_hint_foreground_material_light = 2131099664;
  public static final int abc_input_method_navigation_guard = 2131099665;
  public static final int abc_primary_text_disable_only_material_dark = 2131099666;
  public static final int abc_primary_text_disable_only_material_light = 2131099667;
  public static final int abc_primary_text_material_dark = 2131099668;
  public static final int abc_primary_text_material_light = 2131099669;
  public static final int abc_search_url_text = 2131099670;
  public static final int abc_search_url_text_normal = 2131099671;
  public static final int abc_search_url_text_pressed = 2131099672;
  public static final int abc_search_url_text_selected = 2131099673;
  public static final int abc_secondary_text_material_dark = 2131099674;
  public static final int abc_secondary_text_material_light = 2131099675;
  public static final int abc_tint_btn_checkable = 2131099676;
  public static final int abc_tint_default = 2131099677;
  public static final int abc_tint_edittext = 2131099678;
  public static final int abc_tint_seek_thumb = 2131099679;
  public static final int abc_tint_spinner = 2131099680;
  public static final int abc_tint_switch_track = 2131099681;
  public static final int accent_material_dark = 2131099694;
  public static final int accent_material_light = 2131099695;
  public static final int background_floating_material_dark = 2131099774;
  public static final int background_floating_material_light = 2131099775;
  public static final int background_material_dark = 2131099777;
  public static final int background_material_light = 2131099778;
  public static final int bright_foreground_disabled_material_dark = 2131099817;
  public static final int bright_foreground_disabled_material_light = 2131099818;
  public static final int bright_foreground_inverse_material_dark = 2131099819;
  public static final int bright_foreground_inverse_material_light = 2131099820;
  public static final int bright_foreground_material_dark = 2131099821;
  public static final int bright_foreground_material_light = 2131099822;
  public static final int button_material_dark = 2131099832;
  public static final int button_material_light = 2131099833;
  public static final int common_google_signin_btn_text_dark = 2131099897;
  public static final int common_google_signin_btn_text_dark_default = 2131099898;
  public static final int common_google_signin_btn_text_dark_disabled = 2131099899;
  public static final int common_google_signin_btn_text_dark_focused = 2131099900;
  public static final int common_google_signin_btn_text_dark_pressed = 2131099901;
  public static final int common_google_signin_btn_text_light = 2131099902;
  public static final int common_google_signin_btn_text_light_default = 2131099903;
  public static final int common_google_signin_btn_text_light_disabled = 2131099904;
  public static final int common_google_signin_btn_text_light_focused = 2131099905;
  public static final int common_google_signin_btn_text_light_pressed = 2131099906;
  public static final int common_google_signin_btn_tint = 2131099907;
  public static final int dim_foreground_disabled_material_dark = 2131099997;
  public static final int dim_foreground_disabled_material_light = 2131099998;
  public static final int dim_foreground_material_dark = 2131099999;
  public static final int dim_foreground_material_light = 2131100000;
  public static final int error_color_material_dark = 2131100032;
  public static final int error_color_material_light = 2131100033;
  public static final int foreground_material_dark = 2131100054;
  public static final int foreground_material_light = 2131100055;
  public static final int highlighted_text_material_dark = 2131100080;
  public static final int highlighted_text_material_light = 2131100081;
  public static final int material_blue_grey_800 = 2131100159;
  public static final int material_blue_grey_900 = 2131100160;
  public static final int material_blue_grey_950 = 2131100161;
  public static final int material_deep_teal_200 = 2131100162;
  public static final int material_deep_teal_500 = 2131100163;
  public static final int material_grey_100 = 2131100164;
  public static final int material_grey_300 = 2131100165;
  public static final int material_grey_50 = 2131100166;
  public static final int material_grey_600 = 2131100167;
  public static final int material_grey_800 = 2131100168;
  public static final int material_grey_850 = 2131100169;
  public static final int material_grey_900 = 2131100170;
  public static final int notification_action_color_filter = 2131100296;
  public static final int notification_icon_bg_color = 2131100298;
  public static final int notification_material_background_media_default_color = 2131100299;
  public static final int primary_dark_material_dark = 2131100409;
  public static final int primary_dark_material_light = 2131100410;
  public static final int primary_material_dark = 2131100414;
  public static final int primary_material_light = 2131100415;
  public static final int primary_text_default_material_dark = 2131100417;
  public static final int primary_text_default_material_light = 2131100418;
  public static final int primary_text_disabled_material_dark = 2131100419;
  public static final int primary_text_disabled_material_light = 2131100420;
  public static final int ripple_material_dark = 2131100476;
  public static final int ripple_material_light = 2131100477;
  public static final int secondary_text_default_material_dark = 2131100487;
  public static final int secondary_text_default_material_light = 2131100488;
  public static final int secondary_text_disabled_material_dark = 2131100489;
  public static final int secondary_text_disabled_material_light = 2131100490;
  public static final int switch_thumb_disabled_material_dark = 2131100530;
  public static final int switch_thumb_disabled_material_light = 2131100531;
  public static final int switch_thumb_material_dark = 2131100532;
  public static final int switch_thumb_material_light = 2131100533;
  public static final int switch_thumb_normal_material_dark = 2131100534;
  public static final int switch_thumb_normal_material_light = 2131100535;
  public static final int tooltip_background_dark = 2131100582;
  public static final int tooltip_background_light = 2131100583;
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.R.color
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */