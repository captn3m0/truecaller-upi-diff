package com.truecaller.common.network.a;

public final class a$e
  extends a
{
  public final boolean c;
  
  public a$e(boolean paramBoolean)
  {
    super((byte)4, false, null, 6);
    c = paramBoolean;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof e;
      if (bool2)
      {
        paramObject = (e)paramObject;
        bool2 = c;
        boolean bool3 = c;
        if (bool2 == bool3)
        {
          bool3 = true;
        }
        else
        {
          bool3 = false;
          paramObject = null;
        }
        if (bool3) {}
      }
      else
      {
        return false;
      }
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = c;
    if (i != 0) {
      i = 1;
    }
    return i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("EdgeLocation(allowed=");
    boolean bool = c;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.a.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */