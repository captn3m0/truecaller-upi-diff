package com.truecaller.common.network.a;

import c.a.f;
import c.g.b.k;
import com.truecaller.common.network.util.AuthRequirement;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

public final class b
{
  private a.a a;
  private a.e b;
  private a.b c;
  
  public final b a()
  {
    a.e locale = new com/truecaller/common/network/a/a$e;
    locale.<init>(false);
    b = locale;
    return this;
  }
  
  public final b a(AuthRequirement paramAuthRequirement)
  {
    k.b(paramAuthRequirement, "authRequirement");
    a.a locala = new com/truecaller/common/network/a/a$a;
    locala.<init>(paramAuthRequirement);
    a = locala;
    return this;
  }
  
  public final b a(boolean paramBoolean)
  {
    a.b localb = new com/truecaller/common/network/a/a$b;
    localb.<init>(paramBoolean);
    c = localb;
    return this;
  }
  
  public final SortedSet b()
  {
    int i = 7;
    a[] arrayOfa = new a[i];
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/common/network/a/a$a;
      localObject2 = AuthRequirement.NONE;
      ((a.a)localObject1).<init>((AuthRequirement)localObject2);
    }
    localObject1 = (a)localObject1;
    Object localObject2 = null;
    arrayOfa[0] = localObject1;
    localObject1 = b;
    boolean bool = true;
    if (localObject1 == null)
    {
      localObject1 = new com/truecaller/common/network/a/a$e;
      ((a.e)localObject1).<init>(bool);
    }
    localObject1 = (a)localObject1;
    arrayOfa[bool] = localObject1;
    int j = 2;
    Object localObject3 = c;
    if (localObject3 == null)
    {
      localObject3 = new com/truecaller/common/network/a/a$b;
      ((a.b)localObject3).<init>(bool);
    }
    localObject3 = (a)localObject3;
    arrayOfa[j] = localObject3;
    localObject2 = (a)a.g.c;
    arrayOfa[3] = localObject2;
    localObject2 = (a)a.f.c;
    arrayOfa[4] = localObject2;
    localObject2 = (a)a.d.c;
    arrayOfa[5] = localObject2;
    localObject2 = (a)a.c.c;
    arrayOfa[6] = localObject2;
    k.b(arrayOfa, "elements");
    localObject1 = new java/util/TreeSet;
    ((TreeSet)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    return (SortedSet)f.b(arrayOfa, (Collection)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */