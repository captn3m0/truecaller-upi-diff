package com.truecaller.common.network.a;

import c.g.b.k;
import com.truecaller.common.network.util.AuthRequirement;

public final class a$a
  extends a
{
  public final AuthRequirement c;
  
  public a$a(AuthRequirement paramAuthRequirement)
  {
    super((byte)2, false, null, 6);
    c = paramAuthRequirement;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof a;
      if (bool1)
      {
        paramObject = (a)paramObject;
        AuthRequirement localAuthRequirement = c;
        paramObject = c;
        boolean bool2 = k.a(localAuthRequirement, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    AuthRequirement localAuthRequirement = c;
    if (localAuthRequirement != null) {
      return localAuthRequirement.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("AuthRequired(authReq=");
    AuthRequirement localAuthRequirement = c;
    localStringBuilder.append(localAuthRequirement);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */