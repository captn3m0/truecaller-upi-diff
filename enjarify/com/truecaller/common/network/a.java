package com.truecaller.common.network;

import com.truecaller.common.account.r;
import com.truecaller.common.h.i;
import com.truecaller.log.AssertionUtil;
import java.io.IOException;
import okhttp3.ab;
import okhttp3.ab.a;
import okhttp3.ad;
import okhttp3.u;
import okhttp3.u.a;
import okhttp3.v;
import okhttp3.v.a;

public final class a
  implements v
{
  private final boolean a;
  private final r b;
  private final dagger.a c;
  private final i d;
  
  public a(boolean paramBoolean, r paramr, dagger.a parama, i parami)
  {
    a = paramBoolean;
    b = paramr;
    c = parama;
    d = parami;
  }
  
  public final ad intercept(v.a parama)
  {
    Object localObject1 = "chain";
    c.g.b.k.b(parama, (String)localObject1);
    try
    {
      localObject1 = parama.a();
      Object localObject2 = d;
      Object localObject3 = "request";
      c.g.b.k.a(localObject1, (String)localObject3);
      localObject3 = f.a((ab)localObject1);
      boolean bool1 = ((i)localObject2).a((e)localObject3);
      boolean bool2;
      if (bool1)
      {
        localObject3 = c;
        localObject3 = ((dagger.a)localObject3).get();
        localObject3 = (com.truecaller.common.account.k)localObject3;
        localObject3 = ((com.truecaller.common.account.k)localObject3).a();
      }
      else
      {
        bool2 = a;
        if (bool2)
        {
          localObject3 = b;
          localObject3 = ((r)localObject3).e();
        }
        else
        {
          localObject3 = b;
          localObject3 = ((r)localObject3).d();
        }
      }
      if (localObject3 != null)
      {
        localObject2 = ((ab)localObject1).a();
        localObject2 = ((u)localObject2).j();
        String str1 = "encoding";
        String str2 = "json";
        localObject2 = ((u.a)localObject2).a(str1, str2);
        localObject2 = ((u.a)localObject2).b();
        localObject1 = ((ab)localObject1).e();
        str1 = "Authorization";
        str2 = "Bearer ";
        localObject3 = String.valueOf(localObject3);
        localObject3 = str2.concat((String)localObject3);
        localObject1 = ((ab.a)localObject1).b(str1, (String)localObject3);
        localObject1 = ((ab.a)localObject1).a((u)localObject2);
        localObject1 = ((ab.a)localObject1).a();
      }
      else
      {
        bool2 = a;
        if ((bool2) || (bool1)) {
          break label253;
        }
      }
      parama = parama.a((ab)localObject1);
      localObject1 = "chain.proceed(request)";
      c.g.b.k.a(parama, (String)localObject1);
      return parama;
      label253:
      parama = b;
      boolean bool3 = parama.c();
      if (!bool3)
      {
        bool3 = false;
        parama = null;
        localObject1 = "Bug in application code. You should not do these requests if not everything is initialized. This is to prevent that (potentially lots of) bad backend requests are made.";
        localObject1 = new String[] { localObject1 };
        AssertionUtil.isTrue(false, (String[])localObject1);
      }
      parama = new java/io/IOException;
      localObject1 = "Bug in application code. You should not do these requests if not everything is initialized. This is to prevent that (potentially lots of) bad backend requests are made.";
      parama.<init>((String)localObject1);
      parama = (Throwable)parama;
      throw parama;
    }
    catch (SecurityException parama)
    {
      localObject1 = new java/io/IOException;
      parama = (Throwable)parama;
      ((IOException)localObject1).<init>(parama);
      throw ((Throwable)localObject1);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */