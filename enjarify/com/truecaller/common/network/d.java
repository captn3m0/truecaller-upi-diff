package com.truecaller.common.network;

import c.g.b.k;
import c.n.m;
import com.truecaller.common.g.a;
import com.truecaller.common.h.ac;

public final class d
  implements c
{
  private final a a;
  private final ac b;
  
  public d(a parama, ac paramac)
  {
    a = parama;
    b = paramac;
  }
  
  public final String a()
  {
    Object localObject = a;
    String str = "networkDomain";
    localObject = ((a)localObject).a(str);
    if (localObject == null) {
      localObject = b();
    }
    return (String)localObject;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "value");
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    boolean bool = m.a((CharSequence)localObject);
    if (!bool)
    {
      localObject = paramString;
    }
    else
    {
      bool = false;
      localObject = null;
    }
    if (localObject != null)
    {
      localObject = a;
      String str = "networkDomain";
      ((a)localObject).a(str, paramString);
    }
  }
  
  public final String b()
  {
    Object localObject = b;
    boolean bool = ((ac)localObject).a();
    if (bool) {}
    for (localObject = KnownDomain.DOMAIN_REGION_1;; localObject = KnownDomain.DOMAIN_OTHER_REGIONS) {
      return ((KnownDomain)localObject).getValue();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */