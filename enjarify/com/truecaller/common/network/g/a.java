package com.truecaller.common.network.g;

import c.g.b.k;

public final class a
{
  public final b a;
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof a;
      if (bool1)
      {
        paramObject = (a)paramObject;
        b localb = a;
        paramObject = a;
        boolean bool2 = k.a(localb, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    b localb = a;
    if (localb != null) {
      return localb.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("EnhancedSearchBackupDto(backupService=");
    b localb = a;
    localStringBuilder.append(localb);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.g.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */