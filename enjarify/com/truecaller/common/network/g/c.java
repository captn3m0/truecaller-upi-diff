package com.truecaller.common.network.g;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class c
{
  public static final String a;
  private static final Map b;
  private static final Map c;
  
  static
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str = c.class.getName();
    ((StringBuilder)localObject).append(str);
    ((StringBuilder)localObject).append("#ACTION_PROFILE_REFRESHED");
    a = ((StringBuilder)localObject).toString();
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    b = (Map)localObject;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    c = (Map)localObject;
    b.put("profileFirstName", "first_name");
    b.put("profileLastName", "last_name");
    b.put("profileNumber", "phone_number");
    b.put("profileNationalNumber", "national_number");
    b.put("profileStatus", "status_message");
    b.put("profileCity", "city");
    b.put("profileStreet", "street");
    b.put("profileZip", "zipcode");
    b.put("profileEmail", "email");
    b.put("profileWeb", "url");
    b.put("profileFacebook", "facebook_id");
    b.put("profileTwitter", "twitter_id");
    b.put("profileGender", "gender");
    b.put("profileAvatar", "avatar_url");
    b.put("profileCompanyName", "w_company");
    b.put("profileCompanyJob", "w_title");
    b.put("profileAcceptAuto", "auto_accept");
    b.put("profileTag", "tag");
    c.put("profileBusiness", "w_is_business_number");
  }
  
  private c()
  {
    AssertionError localAssertionError = new java/lang/AssertionError;
    localAssertionError.<init>();
    throw localAssertionError;
  }
  
  public static String a(String paramString)
  {
    return (String)b.get(paramString);
  }
  
  public static void a(Map paramMap1, Map paramMap2)
  {
    paramMap1 = paramMap1.entrySet().iterator();
    for (;;)
    {
      boolean bool = paramMap1.hasNext();
      if (!bool) {
        break;
      }
      Object localObject1 = (Map.Entry)paramMap1.next();
      Object localObject2 = b;
      Object localObject3 = ((Map.Entry)localObject1).getKey();
      localObject2 = (String)((Map)localObject2).get(localObject3);
      if (localObject2 == null)
      {
        localObject2 = c;
        localObject3 = ((Map.Entry)localObject1).getKey();
        localObject2 = (String)((Map)localObject2).get(localObject3);
      }
      if (localObject2 != null)
      {
        localObject1 = ((Map.Entry)localObject1).getValue();
        paramMap2.put(localObject2, localObject1);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.g.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */