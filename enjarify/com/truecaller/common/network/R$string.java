package com.truecaller.common.network;

public final class R$string
{
  public static final int abc_action_bar_home_description = 2131887345;
  public static final int abc_action_bar_up_description = 2131887346;
  public static final int abc_action_menu_overflow_description = 2131887347;
  public static final int abc_action_mode_done = 2131887348;
  public static final int abc_activity_chooser_view_see_all = 2131887349;
  public static final int abc_activitychooserview_choose_application = 2131887350;
  public static final int abc_capital_off = 2131887351;
  public static final int abc_capital_on = 2131887352;
  public static final int abc_font_family_body_1_material = 2131887353;
  public static final int abc_font_family_body_2_material = 2131887354;
  public static final int abc_font_family_button_material = 2131887355;
  public static final int abc_font_family_caption_material = 2131887356;
  public static final int abc_font_family_display_1_material = 2131887357;
  public static final int abc_font_family_display_2_material = 2131887358;
  public static final int abc_font_family_display_3_material = 2131887359;
  public static final int abc_font_family_display_4_material = 2131887360;
  public static final int abc_font_family_headline_material = 2131887361;
  public static final int abc_font_family_menu_material = 2131887362;
  public static final int abc_font_family_subhead_material = 2131887363;
  public static final int abc_font_family_title_material = 2131887364;
  public static final int abc_menu_alt_shortcut_label = 2131887365;
  public static final int abc_menu_ctrl_shortcut_label = 2131887366;
  public static final int abc_menu_delete_shortcut_label = 2131887367;
  public static final int abc_menu_enter_shortcut_label = 2131887368;
  public static final int abc_menu_function_shortcut_label = 2131887369;
  public static final int abc_menu_meta_shortcut_label = 2131887370;
  public static final int abc_menu_shift_shortcut_label = 2131887371;
  public static final int abc_menu_space_shortcut_label = 2131887372;
  public static final int abc_menu_sym_shortcut_label = 2131887373;
  public static final int abc_prepend_shortcut_label = 2131887374;
  public static final int abc_search_hint = 2131887375;
  public static final int abc_searchview_description_clear = 2131887376;
  public static final int abc_searchview_description_query = 2131887377;
  public static final int abc_searchview_description_search = 2131887378;
  public static final int abc_searchview_description_submit = 2131887379;
  public static final int abc_searchview_description_voice = 2131887380;
  public static final int abc_shareactionprovider_share_with = 2131887381;
  public static final int abc_shareactionprovider_share_with_application = 2131887382;
  public static final int abc_toolbar_collapse_description = 2131887383;
  public static final int common_google_play_services_enable_button = 2131887712;
  public static final int common_google_play_services_enable_text = 2131887713;
  public static final int common_google_play_services_enable_title = 2131887714;
  public static final int common_google_play_services_install_button = 2131887715;
  public static final int common_google_play_services_install_text = 2131887716;
  public static final int common_google_play_services_install_title = 2131887717;
  public static final int common_google_play_services_notification_channel_name = 2131887718;
  public static final int common_google_play_services_notification_ticker = 2131887719;
  public static final int common_google_play_services_unknown_issue = 2131887720;
  public static final int common_google_play_services_unsupported_text = 2131887721;
  public static final int common_google_play_services_update_button = 2131887722;
  public static final int common_google_play_services_update_text = 2131887723;
  public static final int common_google_play_services_update_title = 2131887724;
  public static final int common_google_play_services_updating_text = 2131887725;
  public static final int common_google_play_services_wear_update_text = 2131887726;
  public static final int common_open_on_phone = 2131887727;
  public static final int common_signin_button_text = 2131887728;
  public static final int common_signin_button_text_long = 2131887729;
  public static final int search_menu_title = 2131888760;
  public static final int status_bar_notification_info_overflow = 2131888855;
}

/* Location:
 * Qualified Name:     com.truecaller.common.network.R.string
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */