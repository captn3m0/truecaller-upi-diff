package com.truecaller.common.background;

import android.content.Context;
import android.os.Bundle;
import com.truecaller.log.AssertionUtil;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import org.c.a.a.a.a.b;

public abstract class PersistentBackgroundTask
{
  private volatile FutureTask a = null;
  
  private void a(Context paramContext, long paramLong)
  {
    paramContext = ((com.truecaller.common.b.a)paramContext.getApplicationContext()).u().c();
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("recurring_task_last_run_time");
    int i = a();
    ((StringBuilder)localObject).append(i);
    localObject = ((StringBuilder)localObject).toString();
    paramContext.b((String)localObject, paramLong);
  }
  
  protected static boolean e(Context paramContext)
  {
    return ((com.truecaller.common.b.a)paramContext.getApplicationContext()).p();
  }
  
  protected abstract int a();
  
  protected abstract PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle);
  
  protected abstract boolean a(Context paramContext);
  
  final PersistentBackgroundTask.RunResult b(Context paramContext, Bundle paramBundle)
  {
    FutureTask localFutureTask = a;
    int i = 0;
    -..Lambda.PersistentBackgroundTask.ubcXw7gtjUMP5f_ygIcOmQ-H-zM localubcXw7gtjUMP5f_ygIcOmQ-H-zM = null;
    if (localFutureTask == null) {
      try
      {
        localFutureTask = a;
        if (localFutureTask == null)
        {
          localFutureTask = new java/util/concurrent/FutureTask;
          localubcXw7gtjUMP5f_ygIcOmQ-H-zM = new com/truecaller/common/background/-$$Lambda$PersistentBackgroundTask$ubcXw7gtjUMP5f_ygIcOmQ-H-zM;
          localubcXw7gtjUMP5f_ygIcOmQ-H-zM.<init>(this, paramContext, paramBundle);
          localFutureTask.<init>(localubcXw7gtjUMP5f_ygIcOmQ-H-zM);
          a = localFutureTask;
          i = 1;
        }
      }
      finally {}
    }
    if (i != 0)
    {
      localFutureTask.run();
      try
      {
        paramContext = a;
        if (localFutureTask == paramContext)
        {
          paramContext = null;
          a = null;
        }
      }
      finally {}
    }
    try
    {
      paramContext = localFutureTask.get();
      return (PersistentBackgroundTask.RunResult)paramContext;
    }
    catch (ExecutionException paramContext) {}catch (InterruptedException paramContext) {}
    boolean bool = paramContext instanceof ExecutionException;
    if (bool) {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)paramContext);
    }
    return PersistentBackgroundTask.RunResult.FailedSkip;
  }
  
  protected abstract e b();
  
  public final boolean b(Context paramContext)
  {
    e locale = b();
    int i = a;
    int j = 1;
    if (i == j)
    {
      long l1 = c(paramContext);
      long l2 = System.currentTimeMillis();
      paramContext = TimeUnit.MILLISECONDS;
      long l3 = locale.a(paramContext);
      paramContext = TimeUnit.MILLISECONDS;
      long l4 = locale.b(paramContext);
      l3 -= l4;
      l4 = 0L;
      paramContext = null;
      boolean bool1 = l1 < l4;
      if (bool1)
      {
        l3 += l1;
        bool2 = l3 < l2;
        if (!bool2)
        {
          bool2 = l2 < l1;
          if (bool2)
          {
            bool2 = true;
            break label119;
          }
        }
      }
      boolean bool2 = false;
      locale = null;
      label119:
      if (!bool2) {
        return j;
      }
      return false;
    }
    return j;
  }
  
  public final long c(Context paramContext)
  {
    paramContext = ((com.truecaller.common.b.a)paramContext.getApplicationContext()).u().c();
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("recurring_task_last_run_time");
    int i = a();
    ((StringBuilder)localObject).append(i);
    localObject = ((StringBuilder)localObject).toString();
    return paramContext.a((String)localObject, 0L);
  }
  
  public final void d(Context paramContext)
  {
    a(paramContext, 0L);
  }
  
  public String toString()
  {
    return b.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.PersistentBackgroundTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */