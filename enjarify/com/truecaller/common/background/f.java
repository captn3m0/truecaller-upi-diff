package com.truecaller.common.background;

import android.content.Context;
import android.util.SparseArray;
import com.truecaller.common.h.a;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import java.util.Iterator;

final class f
{
  private final Context a;
  private volatile SparseArray b = null;
  
  f(Context paramContext)
  {
    paramContext = paramContext.getApplicationContext();
    a = paramContext;
  }
  
  final SparseArray a()
  {
    SparseArray localSparseArray1 = b;
    if (localSparseArray1 == null) {
      synchronized (f.class)
      {
        localSparseArray1 = b;
        if (localSparseArray1 == null)
        {
          localSparseArray1 = new android/util/SparseArray;
          localSparseArray1.<init>();
          Object localObject1 = a;
          Object localObject2 = PersistentBackgroundTask.class;
          Object localObject3 = PersistentBackgroundTask.class;
          localObject3 = ((Class)localObject3).getClassLoader();
          localObject1 = a.a((Context)localObject1, (Class)localObject2, (ClassLoader)localObject3);
          localObject1 = ((a)localObject1).iterator();
          for (;;)
          {
            boolean bool = ((Iterator)localObject1).hasNext();
            if (!bool) {
              break;
            }
            localObject2 = ((Iterator)localObject1).next();
            localObject2 = (PersistentBackgroundTask)localObject2;
            int i = ((PersistentBackgroundTask)localObject2).a();
            int j = localSparseArray1.indexOfKey(i);
            if (j >= 0)
            {
              j = 1;
              String[] arrayOfString = new String[j];
              Object localObject4 = new java/lang/StringBuilder;
              Object localObject5 = "Background tasks [";
              ((StringBuilder)localObject4).<init>((String)localObject5);
              localObject5 = localObject2.getClass();
              localObject5 = ((Class)localObject5).getSimpleName();
              ((StringBuilder)localObject4).append((String)localObject5);
              localObject5 = "] and [";
              ((StringBuilder)localObject4).append((String)localObject5);
              localObject5 = localSparseArray1.get(i);
              localObject5 = (PersistentBackgroundTask)localObject5;
              localObject5 = localObject5.getClass();
              localObject5 = ((Class)localObject5).getSimpleName();
              ((StringBuilder)localObject4).append((String)localObject5);
              localObject5 = "] have te same id [";
              ((StringBuilder)localObject4).append((String)localObject5);
              ((StringBuilder)localObject4).append(i);
              localObject5 = "] ";
              ((StringBuilder)localObject4).append((String)localObject5);
              localObject4 = ((StringBuilder)localObject4).toString();
              arrayOfString[0] = localObject4;
              AssertionUtil.AlwaysFatal.fail(arrayOfString);
            }
            localSparseArray1.put(i, localObject2);
          }
          b = localSparseArray1;
        }
      }
    }
    return localSparseArray2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */