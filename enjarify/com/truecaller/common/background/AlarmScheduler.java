package com.truecaller.common.background;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.text.TextUtils;
import com.truecaller.common.b.a;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.c.a.a.a.j;

public final class AlarmScheduler
  extends Service
  implements b.a
{
  private static final long a = TimeUnit.MINUTES.toMillis(1L);
  private f b;
  private final Set c;
  private int d;
  
  public AlarmScheduler()
  {
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    c = localHashSet;
  }
  
  private static PendingIntent a(Context paramContext, int paramInt1, Bundle paramBundle, int paramInt2, boolean paramBoolean)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, AlarmScheduler.class);
    localIntent.setAction("com.truecaller.common.background.ACTION_FALLBACK_EXECUTE");
    Uri.Builder localBuilder = new android/net/Uri$Builder;
    localBuilder.<init>();
    String str = "job";
    localBuilder = localBuilder.scheme(str);
    Object localObject = String.valueOf(paramInt1);
    localObject = localBuilder.appendPath((String)localObject).build();
    localIntent.setData((Uri)localObject);
    localObject = "retries";
    localIntent.putExtra((String)localObject, paramInt2);
    if (paramBundle != null)
    {
      paramInt1 = paramBundle.isEmpty();
      if (paramInt1 == 0)
      {
        localObject = "task_params";
        localIntent.putExtra((String)localObject, paramBundle);
      }
    }
    paramInt1 = 0;
    localObject = null;
    int i;
    if (paramBoolean) {
      i = 134217728;
    } else {
      i = 536870912;
    }
    return PendingIntent.getService(paramContext, 0, localIntent, i);
  }
  
  private static void a(AlarmManager paramAlarmManager, e parame, long paramLong, PendingIntent paramPendingIntent)
  {
    int i = a;
    long l1 = 0L;
    long l2;
    if (i == 0)
    {
      boolean bool1 = paramLong < l1;
      if (!bool1)
      {
        TimeUnit localTimeUnit1 = TimeUnit.MILLISECONDS;
        paramLong = parame.c(localTimeUnit1);
        localTimeUnit2 = TimeUnit.MILLISECONDS;
        l2 = parame.d(localTimeUnit2);
        paramLong = j.a(paramLong, l2);
      }
      l2 = SystemClock.elapsedRealtime() + paramLong;
      paramAlarmManager.set(2, l2, paramPendingIntent);
      return;
    }
    TimeUnit localTimeUnit2 = TimeUnit.MILLISECONDS;
    long l3 = parame.a(localTimeUnit2);
    boolean bool2 = paramLong < l1;
    long l4;
    if (!bool2)
    {
      l2 = 300000L;
      l4 = j.a(1000L, l2);
    }
    else
    {
      l4 = Math.min(l3, paramLong);
    }
    long l5 = SystemClock.elapsedRealtime() + l4;
    paramAlarmManager.setInexactRepeating(2, l5, l3, paramPendingIntent);
  }
  
  private static void b(AlarmManager paramAlarmManager, Context paramContext, int paramInt)
  {
    paramContext = a(paramContext, paramInt, null, 0, false);
    if (paramContext == null) {
      return;
    }
    paramAlarmManager.cancel(paramContext);
  }
  
  public final IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public final void onCreate()
  {
    super.onCreate();
    f localf = new com/truecaller/common/background/f;
    localf.<init>(this);
    b = localf;
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    b = null;
  }
  
  public final int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    if (paramIntent != null)
    {
      ??? = "com.truecaller.common.background.ACTION_FALLBACK_EXECUTE";
      Object localObject2 = paramIntent.getAction();
      boolean bool1 = TextUtils.equals((CharSequence)???, (CharSequence)localObject2);
      if (bool1)
      {
        ??? = paramIntent.getData();
        if (??? != null) {
          try
          {
            ??? = paramIntent.getData();
            ??? = ((Uri)???).getPath();
            paramInt1 = Integer.parseInt((String)???);
            synchronized (c)
            {
              localObject2 = c;
              Integer localInteger = Integer.valueOf(paramInt2);
              ((Set)localObject2).add(localInteger);
              d = paramInt2;
              ??? = paramIntent.getBundleExtra("task_params");
              int i = paramIntent.getIntExtra("retries", 0);
              paramIntent = getApplicationc;
              AlarmScheduler.b localb = new com/truecaller/common/background/AlarmScheduler$b;
              localObject2 = localb;
              localb.<init>(paramInt1, (Bundle)???, i, paramInt2, (byte)0);
              paramIntent.a(paramInt1, (Bundle)???, this, localb);
              return 3;
            }
            boolean bool2;
            return super.onStartCommand(paramIntent, paramInt1, paramInt2);
          }
          catch (NumberFormatException localNumberFormatException)
          {
            synchronized (c)
            {
              localObject2 = c;
              bool2 = ((Set)localObject2).isEmpty();
              if (bool2) {
                stopSelf(paramInt2);
              }
              return super.onStartCommand(paramIntent, paramInt1, paramInt2);
            }
          }
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.AlarmScheduler
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */