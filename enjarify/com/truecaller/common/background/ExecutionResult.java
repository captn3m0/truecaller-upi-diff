package com.truecaller.common.background;

public enum ExecutionResult
{
  static
  {
    Object localObject = new com/truecaller/common/background/ExecutionResult;
    ((ExecutionResult)localObject).<init>("Success", 0);
    Success = (ExecutionResult)localObject;
    localObject = new com/truecaller/common/background/ExecutionResult;
    int i = 1;
    ((ExecutionResult)localObject).<init>("Skip", i);
    Skip = (ExecutionResult)localObject;
    localObject = new com/truecaller/common/background/ExecutionResult;
    int j = 2;
    ((ExecutionResult)localObject).<init>("Retry", j);
    Retry = (ExecutionResult)localObject;
    localObject = new com/truecaller/common/background/ExecutionResult;
    int k = 3;
    ((ExecutionResult)localObject).<init>("NotFound", k);
    NotFound = (ExecutionResult)localObject;
    localObject = new com/truecaller/common/background/ExecutionResult;
    int m = 4;
    ((ExecutionResult)localObject).<init>("Inactive", m);
    Inactive = (ExecutionResult)localObject;
    localObject = new ExecutionResult[5];
    ExecutionResult localExecutionResult = Success;
    localObject[0] = localExecutionResult;
    localExecutionResult = Skip;
    localObject[i] = localExecutionResult;
    localExecutionResult = Retry;
    localObject[j] = localExecutionResult;
    localExecutionResult = NotFound;
    localObject[k] = localExecutionResult;
    localExecutionResult = Inactive;
    localObject[m] = localExecutionResult;
    $VALUES = (ExecutionResult[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.ExecutionResult
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */