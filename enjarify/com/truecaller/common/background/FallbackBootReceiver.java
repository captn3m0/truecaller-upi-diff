package com.truecaller.common.background;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.truecaller.common.b.a;

public class FallbackBootReceiver
  extends BroadcastReceiver
{
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    new String[1][0] = "Boot receiver triggered";
    String str = "android.intent.action.BOOT_COMPLETED";
    paramIntent = paramIntent.getAction();
    boolean bool = TextUtils.equals(str, paramIntent);
    if (bool)
    {
      paramContext = getApplicationContextc;
      paramContext.d();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.FallbackBootReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */