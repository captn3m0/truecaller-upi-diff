package com.truecaller.common.background;

import android.os.Bundle;
import android.os.PersistableBundle;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public final class e
{
  final int a;
  final int b;
  final long c;
  final long d;
  final boolean e;
  final long f;
  private final Map g;
  
  private e(e.a parama)
  {
    int i = a;
    a = i;
    i = b;
    b = i;
    boolean bool1 = c;
    e = bool1;
    long l1 = h;
    f = l1;
    Map localMap = i;
    g = localMap;
    int j = a;
    if (j != 0)
    {
      l1 = d;
      c = l1;
      l1 = e;
      d = l1;
      return;
    }
    l1 = f;
    c = l1;
    l1 = g;
    d = l1;
    l1 = c;
    long l2 = d;
    parama = null;
    boolean bool2 = l1 < l2;
    if (bool2)
    {
      j = 1;
    }
    else
    {
      j = 0;
      localMap = null;
    }
    parama = new String[0];
    AssertionUtil.OnlyInDebug.isTrue(j, parama);
  }
  
  final long a(TimeUnit paramTimeUnit)
  {
    long l = c;
    TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
    return paramTimeUnit.convert(l, localTimeUnit);
  }
  
  final void a(Bundle paramBundle)
  {
    Object localObject1 = g;
    if (localObject1 != null)
    {
      boolean bool1 = ((Map)localObject1).isEmpty();
      if (!bool1)
      {
        localObject1 = g.entrySet().iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          Object localObject2 = (Map.Entry)((Iterator)localObject1).next();
          Object localObject3 = ((Map.Entry)localObject2).getValue();
          boolean bool3 = localObject3 instanceof String;
          if (bool3)
          {
            localObject2 = (String)((Map.Entry)localObject2).getKey();
            localObject3 = (String)localObject3;
            paramBundle.putString((String)localObject2, (String)localObject3);
          }
          else
          {
            bool3 = localObject3 instanceof Integer;
            if (bool3)
            {
              localObject2 = (String)((Map.Entry)localObject2).getKey();
              localObject3 = (Integer)localObject3;
              int i = ((Integer)localObject3).intValue();
              paramBundle.putInt((String)localObject2, i);
            }
          }
        }
        return;
      }
    }
  }
  
  final void a(PersistableBundle paramPersistableBundle)
  {
    Object localObject1 = g;
    if (localObject1 != null)
    {
      boolean bool1 = ((Map)localObject1).isEmpty();
      if (!bool1)
      {
        localObject1 = g.entrySet().iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          Object localObject2 = (Map.Entry)((Iterator)localObject1).next();
          Object localObject3 = ((Map.Entry)localObject2).getValue();
          boolean bool3 = localObject3 instanceof String;
          if (bool3)
          {
            localObject2 = (String)((Map.Entry)localObject2).getKey();
            localObject3 = (String)localObject3;
            paramPersistableBundle.putString((String)localObject2, (String)localObject3);
          }
          else
          {
            bool3 = localObject3 instanceof Integer;
            if (bool3)
            {
              localObject2 = (String)((Map.Entry)localObject2).getKey();
              localObject3 = (Integer)localObject3;
              int i = ((Integer)localObject3).intValue();
              paramPersistableBundle.putInt((String)localObject2, i);
            }
          }
        }
        return;
      }
    }
  }
  
  final long b(TimeUnit paramTimeUnit)
  {
    long l = d;
    TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
    return paramTimeUnit.convert(l, localTimeUnit);
  }
  
  final long c(TimeUnit paramTimeUnit)
  {
    long l = c;
    TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
    return paramTimeUnit.convert(l, localTimeUnit);
  }
  
  final long d(TimeUnit paramTimeUnit)
  {
    long l = d;
    TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
    return paramTimeUnit.convert(l, localTimeUnit);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */