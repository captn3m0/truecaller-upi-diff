package com.truecaller.common.background;

public enum PersistentBackgroundTask$RunResult
{
  static
  {
    Object localObject = new com/truecaller/common/background/PersistentBackgroundTask$RunResult;
    ((RunResult)localObject).<init>("FailedSkip", 0);
    FailedSkip = (RunResult)localObject;
    localObject = new com/truecaller/common/background/PersistentBackgroundTask$RunResult;
    int i = 1;
    ((RunResult)localObject).<init>("FailedRetry", i);
    FailedRetry = (RunResult)localObject;
    localObject = new com/truecaller/common/background/PersistentBackgroundTask$RunResult;
    int j = 2;
    ((RunResult)localObject).<init>("Success", j);
    Success = (RunResult)localObject;
    localObject = new RunResult[3];
    RunResult localRunResult = FailedSkip;
    localObject[0] = localRunResult;
    localRunResult = FailedRetry;
    localObject[i] = localRunResult;
    localRunResult = Success;
    localObject[j] = localRunResult;
    $VALUES = (RunResult[])localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.PersistentBackgroundTask.RunResult
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */