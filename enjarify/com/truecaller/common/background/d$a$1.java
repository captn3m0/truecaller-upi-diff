package com.truecaller.common.background;

import android.util.SparseArray;
import java.util.ArrayList;

final class d$a$1
  implements Runnable
{
  d$a$1(d.a parama) {}
  
  public final void run()
  {
    ArrayList localArrayList1 = new java/util/ArrayList;
    localArrayList1.<init>();
    ArrayList localArrayList2 = new java/util/ArrayList;
    localArrayList2.<init>();
    SparseArray localSparseArray = d.a.a(a).a();
    int i = localSparseArray.size();
    int j = 0;
    while (j < i)
    {
      d.a locala = a;
      PersistentBackgroundTask localPersistentBackgroundTask = (PersistentBackgroundTask)localSparseArray.valueAt(j);
      d.a.a(locala, localPersistentBackgroundTask, localArrayList1, localArrayList2);
      j += 1;
    }
    d.a.a(a, localArrayList1, localArrayList2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.d.a.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */