package com.truecaller.common.background;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import com.truecaller.common.b.a;
import com.truecaller.utils.i;
import com.truecaller.utils.t;
import com.truecaller.utils.t.a;
import java.util.Iterator;
import java.util.List;

final class AlarmScheduler$a
  implements c
{
  private final Context a;
  private final i b;
  
  AlarmScheduler$a(Context paramContext)
  {
    paramContext = paramContext.getApplicationContext();
    a = paramContext;
    paramContext = com.truecaller.utils.c.a();
    Context localContext = a;
    paramContext = paramContext.a(localContext).a().f();
    b = paramContext;
  }
  
  public final void a(List paramList)
  {
    Object localObject1 = a;
    Object localObject2 = "alarm";
    localObject1 = (AlarmManager)((Context)localObject1).getSystemService((String)localObject2);
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (PersistentBackgroundTask)paramList.next();
      Object localObject3 = ((PersistentBackgroundTask)localObject2).b();
      Object localObject4 = new android/os/Bundle;
      ((Bundle)localObject4).<init>();
      ((e)localObject3).a((Bundle)localObject4);
      Context localContext = a;
      int j = ((PersistentBackgroundTask)localObject2).a();
      localObject4 = AlarmScheduler.a(localContext, j, (Bundle)localObject4);
      AlarmScheduler.a((AlarmManager)localObject1, (e)localObject3, (PendingIntent)localObject4);
      localObject3 = a.getApplicationContext()).c;
      int i = ((PersistentBackgroundTask)localObject2).a();
      ((b)localObject3).b(i);
    }
  }
  
  public final boolean a()
  {
    return true;
  }
  
  public final boolean a(e parame)
  {
    int i = b;
    boolean bool2 = true;
    switch (i)
    {
    default: 
      return bool2;
    case 2: 
      parame = b;
      boolean bool1 = parame.a();
      if (bool1)
      {
        parame = b;
        bool1 = parame.d();
        if (bool1) {
          return bool2;
        }
      }
      return false;
    }
    return b.a();
  }
  
  public final void b(List paramList)
  {
    Object localObject1 = a;
    Object localObject2 = "alarm";
    localObject1 = (AlarmManager)((Context)localObject1).getSystemService((String)localObject2);
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (PersistentBackgroundTask)paramList.next();
      Context localContext = a;
      int i = ((PersistentBackgroundTask)localObject2).a();
      AlarmScheduler.a((AlarmManager)localObject1, localContext, i);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.AlarmScheduler.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */