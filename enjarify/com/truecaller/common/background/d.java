package com.truecaller.common.background;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.util.SparseArray;
import com.google.android.gms.common.GoogleApiAvailability;
import com.truecaller.analytics.e.a;
import com.truecaller.common.h.k;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public final class d
  implements b
{
  final d.a a;
  public com.truecaller.analytics.b b;
  private final Context c;
  private final f d;
  private final Executor e;
  private final c f;
  
  public d(Context paramContext)
  {
    Object localObject = Executors.newCachedThreadPool();
    e = ((Executor)localObject);
    localObject = paramContext.getApplicationContext();
    c = ((Context)localObject);
    localObject = new com/truecaller/common/background/f;
    ((f)localObject).<init>(paramContext);
    d = ((f)localObject);
    boolean bool = k.d();
    if (bool)
    {
      new String[1][0] = "Scheduler engine: JobScheduler";
      localObject = new com/truecaller/common/background/NativeScheduler$a;
      ((NativeScheduler.a)localObject).<init>(paramContext);
      f = ((c)localObject);
    }
    else
    {
      bool = a(paramContext);
      if (bool)
      {
        localObject = new com/truecaller/common/background/GcmScheduler$a;
        ((GcmScheduler.a)localObject).<init>(paramContext);
        f = ((c)localObject);
        localObject = "Scheduler engine: GcmScheduler";
        new String[1][0] = localObject;
      }
      else
      {
        localObject = new com/truecaller/common/background/AlarmScheduler$a;
        ((AlarmScheduler.a)localObject).<init>(paramContext);
        f = ((c)localObject);
        localObject = "Scheduler engine: AlarmManager";
        new String[1][0] = localObject;
      }
    }
    localObject = new android/os/HandlerThread;
    ((HandlerThread)localObject).<init>("Scheduler");
    ((HandlerThread)localObject).start();
    d.a locala = new com/truecaller/common/background/d$a;
    Looper localLooper = ((HandlerThread)localObject).getLooper();
    Context localContext = c;
    f localf = d;
    c localc = f;
    locala.<init>(localLooper, localContext, localf, localc, (byte)0);
    a = locala;
    ((com.truecaller.common.b.a)paramContext.getApplicationContext()).u().a(this);
  }
  
  static void a(PersistentBackgroundTask paramPersistentBackgroundTask, String paramString)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("[Task: ");
    paramPersistentBackgroundTask = paramPersistentBackgroundTask.getClass().getSimpleName();
    ((StringBuilder)localObject).append(paramPersistentBackgroundTask);
    ((StringBuilder)localObject).append("] ");
    ((StringBuilder)localObject).append(paramString);
    paramPersistentBackgroundTask = ((StringBuilder)localObject).toString();
    int i = 1;
    localObject = new Object[i];
    localObject[0] = paramPersistentBackgroundTask;
    com.truecaller.debug.log.a.a((Object[])localObject);
    new String[i][0] = paramPersistentBackgroundTask;
  }
  
  private static boolean a(Context paramContext)
  {
    GoogleApiAvailability localGoogleApiAvailability = GoogleApiAvailability.a();
    int i = localGoogleApiAvailability.a(paramContext);
    boolean bool = true;
    if (i != 0)
    {
      int j = -1;
      try
      {
        paramContext = paramContext.getPackageManager();
        String str = "com.google.android.gms";
        paramContext = paramContext.getPackageInfo(str, 0);
        j = versionCode;
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException)
      {
        paramContext = "Play services not installed";
        new String[1][0] = paramContext;
      }
      paramContext = new java/lang/StringBuilder;
      paramContext.<init>("Old/missing Play Services. isGooglePlayServicesAvailable: ");
      paramContext.append(i);
      paramContext.append(" version: ");
      paramContext.append(j);
      paramContext = paramContext.toString();
      new String[bool][0] = paramContext;
      return false;
    }
    return bool;
  }
  
  public final ExecutionResult a(int paramInt)
  {
    return c(paramInt, null);
  }
  
  public final ExecutionResult a(int paramInt, Bundle paramBundle)
  {
    return c(paramInt, paramBundle);
  }
  
  public final void a()
  {
    d.a locala = a;
    Runnable localRunnable = a;
    locala.post(localRunnable);
  }
  
  public final void a(int paramInt, Bundle paramBundle, b.a parama, Object paramObject)
  {
    d.2 local2 = new com/truecaller/common/background/d$2;
    local2.<init>(this, paramInt, paramBundle, parama, paramObject);
    a(local2);
  }
  
  public final void a(int paramInt, int... paramVarArgs)
  {
    a(0L, paramInt, paramVarArgs);
  }
  
  public final void a(long paramLong, int paramInt, int... paramVarArgs)
  {
    a.a(paramLong, paramInt, paramVarArgs);
  }
  
  public final void a(Runnable paramRunnable)
  {
    e.execute(paramRunnable);
  }
  
  public final void b()
  {
    Object localObject = d.a();
    int i = 10002;
    localObject = (PersistentBackgroundTask)((SparseArray)localObject).get(i);
    if (localObject != null)
    {
      Context localContext = c;
      ((PersistentBackgroundTask)localObject).d(localContext);
    }
  }
  
  public final void b(int paramInt)
  {
    b(paramInt, null);
  }
  
  public final void b(int paramInt, Bundle paramBundle)
  {
    d.1 local1 = new com/truecaller/common/background/d$1;
    local1.<init>(this, paramInt, paramBundle);
    a(local1);
  }
  
  final ExecutionResult c(int paramInt, Bundle paramBundle)
  {
    Object localObject1 = (PowerManager)c.getSystemService("power");
    int i = 1;
    localObject1 = ((PowerManager)localObject1).newWakeLock(i, "Task Scheduler");
    Object localObject2 = null;
    ((PowerManager.WakeLock)localObject1).setReferenceCounted(false);
    long l1 = 180000L;
    ((PowerManager.WakeLock)localObject1).acquire(l1);
    try
    {
      Object localObject3 = d;
      localObject3 = ((f)localObject3).a();
      Object localObject4 = ((SparseArray)localObject3).get(paramInt);
      localObject4 = (PersistentBackgroundTask)localObject4;
      if (localObject4 == null)
      {
        localObject4 = ExecutionResult.NotFound;
        paramBundle = (Bundle)localObject4;
      }
      else
      {
        localObject3 = c;
        boolean bool = ((PersistentBackgroundTask)localObject4).a((Context)localObject3);
        if (!bool)
        {
          paramBundle = ExecutionResult.Inactive;
          localObject2 = "Task is inactive";
          a((PersistentBackgroundTask)localObject4, (String)localObject2);
        }
        else
        {
          localObject3 = f;
          Object localObject6 = ((PersistentBackgroundTask)localObject4).b();
          bool = ((c)localObject3).a((e)localObject6);
          if (bool)
          {
            l1 = SystemClock.elapsedRealtime();
            Object localObject7 = "Execute task";
            a((PersistentBackgroundTask)localObject4, (String)localObject7);
            localObject7 = c;
            paramBundle = ((PersistentBackgroundTask)localObject4).b((Context)localObject7, paramBundle);
            localObject7 = d.3.a;
            int j = paramBundle.ordinal();
            int k = localObject7[j];
            switch (k)
            {
            default: 
              localObject8 = new String[i];
              break;
            case 3: 
              paramBundle = ExecutionResult.Skip;
              break;
            case 2: 
              paramBundle = ExecutionResult.Retry;
              break;
            case 1: 
              paramBundle = ExecutionResult.Success;
              break;
            }
            localObject7 = new java/lang/StringBuilder;
            ((StringBuilder)localObject7).<init>();
            Object localObject9 = localObject4.getClass();
            localObject9 = ((Class)localObject9).getSimpleName();
            ((StringBuilder)localObject7).append((String)localObject9);
            localObject9 = ": Incorrect result - ";
            ((StringBuilder)localObject7).append((String)localObject9);
            ((StringBuilder)localObject7).append(paramBundle);
            localObject7 = ((StringBuilder)localObject7).toString();
            localObject8[0] = localObject7;
            AssertionUtil.AlwaysFatal.fail((String[])localObject8);
            localObject2 = "Incorrect result: ";
            paramBundle = String.valueOf(paramBundle);
            paramBundle = ((String)localObject2).concat(paramBundle);
            a((PersistentBackgroundTask)localObject4, paramBundle);
            paramBundle = ExecutionResult.Inactive;
            long l2 = SystemClock.elapsedRealtime() - l1;
            double d1 = l2;
            localObject3 = new com/truecaller/analytics/e$a;
            localObject6 = "BackgroundTask";
            ((e.a)localObject3).<init>((String)localObject6);
            localObject6 = "TaskName";
            localObject7 = localObject4.getClass();
            localObject7 = ((Class)localObject7).getSimpleName();
            localObject3 = ((e.a)localObject3).a((String)localObject6, (String)localObject7);
            localObject6 = "Result";
            localObject7 = paramBundle.name();
            localObject3 = ((e.a)localObject3).a((String)localObject6, (String)localObject7);
            localObject2 = Double.valueOf(d1);
            a = ((Double)localObject2);
            localObject2 = ((e.a)localObject3).a();
            Object localObject8 = b;
            ((com.truecaller.analytics.b)localObject8).b((com.truecaller.analytics.e)localObject2);
            localObject2 = new java/lang/StringBuilder;
            localObject8 = "Execution result: ";
            ((StringBuilder)localObject2).<init>((String)localObject8);
            localObject8 = paramBundle.name();
            ((StringBuilder)localObject2).append((String)localObject8);
            localObject2 = ((StringBuilder)localObject2).toString();
            a((PersistentBackgroundTask)localObject4, (String)localObject2);
          }
          else
          {
            paramBundle = ExecutionResult.Retry;
            localObject2 = "No conditions to start task. Will retry it later.";
            a((PersistentBackgroundTask)localObject4, (String)localObject2);
          }
        }
      }
      return paramBundle;
    }
    finally
    {
      ((PowerManager.WakeLock)localObject1).release();
    }
  }
  
  public final void c()
  {
    Object localObject = d.a();
    int i = 10002;
    localObject = (PersistentBackgroundTask)((SparseArray)localObject).get(i);
    if (localObject != null)
    {
      c localc = f;
      localObject = Collections.singletonList(localObject);
      localc.b((List)localObject);
    }
  }
  
  public final void d()
  {
    c localc = f;
    boolean bool = localc.a();
    if (bool) {
      a();
    }
  }
  
  public final void e()
  {
    a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */