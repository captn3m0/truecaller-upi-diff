package com.truecaller.common.background;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;

public final class a
{
  public static void a(Context paramContext, Class paramClass, long paramLong, Bundle paramBundle, int paramInt)
  {
    Context localContext = paramContext.getApplicationContext();
    ComponentName localComponentName = new android/content/ComponentName;
    localComponentName.<init>(localContext, paramClass);
    paramClass = DelayedServiceBroadcastReceiver.a(paramContext, localComponentName, paramBundle);
    int i = 134217728;
    paramClass = PendingIntent.getBroadcast(paramContext, paramInt, paramClass, i);
    long l = SystemClock.elapsedRealtime() + paramLong;
    String str = "alarm";
    paramContext = (AlarmManager)paramContext.getSystemService(str);
    if (paramContext == null) {
      return;
    }
    paramContext.set(2, l, paramClass);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */