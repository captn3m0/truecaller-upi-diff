package com.truecaller.common.background;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.OneoffTask.Builder;
import com.google.android.gms.gcm.PeriodicTask.Builder;
import com.google.android.gms.gcm.Task;
import com.google.android.gms.gcm.Task.Builder;
import com.truecaller.common.b.a;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

final class GcmScheduler$a
  implements c
{
  private final SharedPreferences a;
  private final Context b;
  
  GcmScheduler$a(Context paramContext)
  {
    paramContext = paramContext.getApplicationContext();
    b = paramContext;
    paramContext = b.getSharedPreferences("gcm-scheduler-engine-initial", 0);
    a = paramContext;
  }
  
  public final void a(List paramList)
  {
    GcmNetworkManager localGcmNetworkManager = GcmNetworkManager.a(b);
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      PersistentBackgroundTask localPersistentBackgroundTask = (PersistentBackgroundTask)paramList.next();
      Object localObject1 = localPersistentBackgroundTask.b();
      int j = localPersistentBackgroundTask.a();
      int k = a;
      long l1;
      if (k != 0)
      {
        localObject2 = new com/google/android/gms/gcm/PeriodicTask$Builder;
        ((PeriodicTask.Builder)localObject2).<init>();
        localObject3 = TimeUnit.SECONDS;
        l1 = ((e)localObject1).a((TimeUnit)localObject3);
        a = l1;
        localObject3 = TimeUnit.SECONDS;
        l1 = ((e)localObject1).b((TimeUnit)localObject3);
        b = l1;
      }
      else
      {
        localObject2 = new com/google/android/gms/gcm/OneoffTask$Builder;
        ((OneoffTask.Builder)localObject2).<init>();
        localObject3 = TimeUnit.SECONDS;
        l1 = ((e)localObject1).c((TimeUnit)localObject3);
        TimeUnit localTimeUnit = TimeUnit.SECONDS;
        long l2 = ((e)localObject1).d(localTimeUnit);
        a = l1;
        b = l2;
      }
      int m = b;
      int n = 1;
      switch (m)
      {
      default: 
        m = 2;
        break;
      case 2: 
        m = 1;
        break;
      case 1: 
        m = 0;
        localObject3 = null;
      }
      ((Task.Builder)localObject2).a(m);
      boolean bool2 = e;
      ((Task.Builder)localObject2).a(bool2);
      Object localObject4 = String.valueOf(j);
      localObject4 = ((Task.Builder)localObject2).a((String)localObject4);
      Object localObject3 = GcmScheduler.class;
      ((Task.Builder)localObject4).a((Class)localObject3).c();
      localObject4 = new android/os/Bundle;
      ((Bundle)localObject4).<init>();
      ((e)localObject1).a((Bundle)localObject4);
      bool2 = ((Bundle)localObject4).isEmpty();
      if (!bool2) {
        ((Task.Builder)localObject2).a((Bundle)localObject4);
      }
      localObject4 = ((Task.Builder)localObject2).b();
      Object localObject2 = d;
      localObject3 = GcmScheduler.class;
      localGcmNetworkManager.a((String)localObject2, (Class)localObject3);
      localGcmNetworkManager.a((Task)localObject4);
      j = localPersistentBackgroundTask.a();
      localObject4 = String.valueOf(j);
      int i1 = a;
      if (i1 == n)
      {
        localObject1 = a;
        boolean bool3 = ((SharedPreferences)localObject1).getBoolean((String)localObject4, false);
        if (!bool3)
        {
          a.edit().putBoolean((String)localObject4, n).apply();
          localObject1 = b).c;
          int i = localPersistentBackgroundTask.a();
          ((b)localObject1).b(i);
        }
      }
    }
  }
  
  public final boolean a()
  {
    return false;
  }
  
  public final boolean a(e parame)
  {
    return true;
  }
  
  public final void b(List paramList)
  {
    GcmNetworkManager localGcmNetworkManager = GcmNetworkManager.a(b);
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      int i = ((PersistentBackgroundTask)paramList.next()).a();
      Object localObject = String.valueOf(i);
      localGcmNetworkManager.a((String)localObject, GcmScheduler.class);
      SharedPreferences.Editor localEditor = a.edit();
      localObject = localEditor.putBoolean((String)localObject, false);
      ((SharedPreferences.Editor)localObject).apply();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.GcmScheduler.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */