package com.truecaller.common.background;

import androidx.work.c;
import androidx.work.c.a;
import androidx.work.j;
import androidx.work.k.a;
import androidx.work.m;
import androidx.work.m.a;
import androidx.work.q.a;
import c.l.b;
import c.n;
import java.util.concurrent.TimeUnit;

public final class g
{
  private org.a.a.h a;
  private final c.a b;
  private n c;
  private final b d;
  private final org.a.a.h e;
  
  public g(b paramb, org.a.a.h paramh)
  {
    d = paramb;
    e = paramh;
    paramb = new androidx/work/c$a;
    paramb.<init>();
    b = paramb;
  }
  
  private final void a(q.a parama)
  {
    Object localObject = b.a();
    parama.a((c)localObject);
    localObject = c;
    if (localObject != null)
    {
      androidx.work.a locala = (androidx.work.a)a;
      long l = b).b;
      localObject = TimeUnit.MILLISECONDS;
      parama.a(locala, l, (TimeUnit)localObject);
      return;
    }
  }
  
  public final m a()
  {
    Object localObject1 = e;
    if (localObject1 != null)
    {
      localObject1 = a;
      org.a.a.h localh;
      if (localObject1 == null)
      {
        localObject1 = new androidx/work/m$a;
        localObject2 = c.g.a.a(d);
        localh = e;
        long l1 = b;
        TimeUnit localTimeUnit1 = TimeUnit.MILLISECONDS;
        ((m.a)localObject1).<init>((Class)localObject2, l1, localTimeUnit1);
      }
      else
      {
        localObject2 = new androidx/work/m$a;
        Class localClass = c.g.a.a(d);
        localh = e;
        long l2 = b;
        TimeUnit localTimeUnit2 = TimeUnit.MILLISECONDS;
        long l3 = b;
        TimeUnit localTimeUnit3 = TimeUnit.MILLISECONDS;
        ((m.a)localObject2).<init>(localClass, l2, localTimeUnit2, l3, localTimeUnit3);
        localObject1 = localObject2;
      }
      localObject2 = localObject1;
      localObject2 = (q.a)localObject1;
      a((q.a)localObject2);
      localObject1 = ((m.a)localObject1).c();
      c.g.b.k.a(localObject1, "when (val flex: Duration…t) }\n            .build()");
      return (m)localObject1;
    }
    localObject1 = new java/lang/IllegalStateException;
    Object localObject2 = "Interval of a periodic request can not be null".toString();
    ((IllegalStateException)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public final g a(androidx.work.a parama, org.a.a.h paramh)
  {
    c.g.b.k.b(parama, "backoffPolicy");
    c.g.b.k.b(paramh, "backoffDelay");
    n localn = new c/n;
    localn.<init>(parama, paramh);
    c = localn;
    return this;
  }
  
  public final g a(j paramj)
  {
    c.g.b.k.b(paramj, "networkType");
    b.a(paramj);
    return this;
  }
  
  public final g a(org.a.a.h paramh)
  {
    c.g.b.k.b(paramh, "interval");
    a = paramh;
    return this;
  }
  
  public final g a(boolean paramBoolean)
  {
    b.a(paramBoolean);
    return this;
  }
  
  public final androidx.work.k b()
  {
    Object localObject1 = new androidx/work/k$a;
    Object localObject2 = c.g.a.a(d);
    ((k.a)localObject1).<init>((Class)localObject2);
    localObject2 = localObject1;
    localObject2 = (q.a)localObject1;
    a((q.a)localObject2);
    localObject1 = ((k.a)localObject1).c();
    c.g.b.k.a(localObject1, "OneTimeWorkRequest.Build…t) }\n            .build()");
    return (androidx.work.k)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */