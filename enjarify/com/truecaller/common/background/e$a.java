package com.truecaller.common.background;

import com.truecaller.log.AssertionUtil.OnlyInDebug;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public final class e$a
{
  final int a;
  public int b = 0;
  public boolean c = false;
  long d;
  long e;
  long f;
  long g;
  long h;
  Map i;
  
  public e$a(int paramInt)
  {
    long l = 0L;
    d = l;
    e = l;
    f = l;
    g = l;
    l = TimeUnit.MINUTES.toMillis(5);
    h = l;
    a = paramInt;
  }
  
  public final a a()
  {
    c = false;
    return this;
  }
  
  public final a a(int paramInt)
  {
    b = paramInt;
    return this;
  }
  
  public final a a(long paramLong, TimeUnit paramTimeUnit)
  {
    int j = a;
    int k = 1;
    if (j != k) {
      k = 0;
    }
    String[] arrayOfString = { "Only periodic tasks can have period" };
    AssertionUtil.OnlyInDebug.isTrue(k, arrayOfString);
    paramLong = paramTimeUnit.toMillis(paramLong);
    d = paramLong;
    return this;
  }
  
  public final a a(String paramString1, String paramString2)
  {
    Object localObject = { "Can not save null as string parameter" };
    boolean bool = true;
    AssertionUtil.OnlyInDebug.isTrue(bool, (String[])localObject);
    localObject = i;
    if (localObject == null)
    {
      localObject = new java/util/HashMap;
      ((HashMap)localObject).<init>();
      i = ((Map)localObject);
    }
    i.put(paramString1, paramString2);
    return this;
  }
  
  public final a a(TimeUnit paramTimeUnit)
  {
    int j = a;
    if (j == 0) {
      j = 1;
    } else {
      j = 0;
    }
    String[] arrayOfString = { "Only one off tasks can have start deadline" };
    AssertionUtil.OnlyInDebug.isTrue(j, arrayOfString);
    long l = paramTimeUnit.toMillis(5);
    g = l;
    return this;
  }
  
  public final a b(long paramLong, TimeUnit paramTimeUnit)
  {
    int j = a;
    int k = 1;
    if (j != k) {
      k = 0;
    }
    String[] arrayOfString = { "Only periodic tasks can have flexibility" };
    AssertionUtil.OnlyInDebug.isTrue(k, arrayOfString);
    paramLong = paramTimeUnit.toMillis(paramLong);
    e = paramLong;
    return this;
  }
  
  public final e b()
  {
    int j = a;
    int k = 1;
    if (j == k)
    {
      long l1 = d;
      long l2 = 0L;
      boolean bool = l1 < l2;
      if (!bool) {
        k = 0;
      }
      localObject = new String[] { "Period not specified for periodic tasks" };
      AssertionUtil.OnlyInDebug.isTrue(k, (String[])localObject);
    }
    Object localObject = new com/truecaller/common/background/e;
    ((e)localObject).<init>(this, (byte)0);
    return (e)localObject;
  }
  
  public final a c(long paramLong, TimeUnit paramTimeUnit)
  {
    paramLong = paramTimeUnit.toMillis(paramLong);
    h = paramLong;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */