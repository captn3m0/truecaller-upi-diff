package com.truecaller.common.background;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.v;
import c.g.b.k;

public final class DelayedServiceBroadcastReceiver
  extends BroadcastReceiver
{
  public static final DelayedServiceBroadcastReceiver.a a;
  
  static
  {
    DelayedServiceBroadcastReceiver.a locala = new com/truecaller/common/background/DelayedServiceBroadcastReceiver$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public static final Intent a(Context paramContext, ComponentName paramComponentName, Bundle paramBundle)
  {
    k.b(paramContext, "context");
    k.b(paramComponentName, "componentName");
    Intent localIntent = new android/content/Intent;
    Class localClass = DelayedServiceBroadcastReceiver.class;
    localIntent.<init>(paramContext, localClass);
    paramComponentName = (Parcelable)paramComponentName;
    paramContext = localIntent.putExtra("component_name", paramComponentName);
    paramComponentName = "job_id";
    int i = 2131363822;
    paramContext = paramContext.putExtra(paramComponentName, i);
    if (paramBundle != null)
    {
      paramComponentName = "payload";
      paramContext.putExtra(paramComponentName, paramBundle);
    }
    k.a(paramContext, "intent");
    return paramContext;
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    k.b(paramContext, "context");
    k.b(paramIntent, "intent");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    ComponentName localComponentName = (ComponentName)paramIntent.getParcelableExtra("component_name");
    if (localComponentName == null) {
      return;
    }
    int i = -1;
    int j = paramIntent.getIntExtra("job_id", i);
    Integer localInteger = Integer.valueOf(j);
    Object localObject = localInteger;
    localObject = (Number)localInteger;
    int k = ((Number)localObject).intValue();
    String str;
    if (k != i)
    {
      i = 1;
    }
    else
    {
      i = 0;
      str = null;
    }
    if (i == 0)
    {
      j = 0;
      localInteger = null;
    }
    if (localInteger != null)
    {
      j = localInteger.intValue();
      str = "payload";
      paramIntent = (Bundle)paramIntent.getParcelableExtra(str);
      if (paramIntent != null) {
        localIntent.putExtras(paramIntent);
      }
      v.a(paramContext, localComponentName, j, localIntent);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.DelayedServiceBroadcastReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */