package com.truecaller.common.background;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.PersistableBundle;
import com.truecaller.common.b.a;
import com.truecaller.log.AssertionUtil;
import java.util.Iterator;
import java.util.List;

public final class NativeScheduler
  extends JobService
  implements b.a
{
  private JobScheduler a;
  
  public final void onCreate()
  {
    super.onCreate();
    JobScheduler localJobScheduler = (JobScheduler)getSystemService("jobscheduler");
    a = localJobScheduler;
  }
  
  public final boolean onStartJob(JobParameters paramJobParameters)
  {
    Object localObject1 = paramJobParameters.getExtras();
    int i = 0;
    a locala = null;
    boolean bool1 = true;
    try
    {
      int j = paramJobParameters.getJobId();
      int k = Build.VERSION.SDK_INT;
      int m = 24;
      Bundle localBundle = null;
      if (k >= m)
      {
        localObject2 = a;
        localObject3 = ((JobScheduler)localObject2).getPendingJob(j);
      }
      else
      {
        localObject2 = a;
        localObject2 = ((JobScheduler)localObject2).getAllPendingJobs();
        if (localObject2 != null)
        {
          localObject2 = ((List)localObject2).iterator();
          Object localObject4;
          int n;
          do
          {
            boolean bool4 = ((Iterator)localObject2).hasNext();
            if (!bool4) {
              break;
            }
            localObject4 = ((Iterator)localObject2).next();
            localObject4 = (JobInfo)localObject4;
            n = ((JobInfo)localObject4).getId();
          } while (j != n);
          localObject3 = localObject4;
        }
        else
        {
          j = 0;
          localObject3 = null;
        }
      }
      Object localObject2 = "tag";
      boolean bool3 = ((PersistableBundle)localObject1).containsKey((String)localObject2);
      if (bool3)
      {
        if (localObject3 != null)
        {
          boolean bool5 = ((JobInfo)localObject3).isPeriodic();
          if (bool5)
          {
            localObject1 = a;
            int i1 = paramJobParameters.getJobId();
            ((JobScheduler)localObject1).cancel(i1);
          }
        }
        return false;
      }
      locala = (a)getApplication();
      Object localObject3 = "params";
      boolean bool2 = ((PersistableBundle)localObject1).containsKey((String)localObject3);
      if (bool2)
      {
        localBundle = new android/os/Bundle;
        localObject3 = "params";
        localObject1 = ((PersistableBundle)localObject1).getPersistableBundle((String)localObject3);
        localBundle.<init>((PersistableBundle)localObject1);
      }
      localObject1 = c;
      i = paramJobParameters.getJobId();
      ((b)localObject1).a(i, localBundle, this, paramJobParameters);
      return bool1;
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localRuntimeException);
      jobFinished(paramJobParameters, bool1);
    }
    return false;
  }
  
  public final boolean onStopJob(JobParameters paramJobParameters)
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.NativeScheduler
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */