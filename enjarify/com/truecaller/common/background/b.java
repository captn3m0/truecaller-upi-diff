package com.truecaller.common.background;

import android.os.Bundle;

public abstract interface b
{
  public abstract ExecutionResult a(int paramInt);
  
  public abstract ExecutionResult a(int paramInt, Bundle paramBundle);
  
  public abstract void a();
  
  public abstract void a(int paramInt, Bundle paramBundle, b.a parama, Object paramObject);
  
  public abstract void a(int paramInt, int... paramVarArgs);
  
  public abstract void a(long paramLong, int paramInt, int... paramVarArgs);
  
  public abstract void a(Runnable paramRunnable);
  
  public abstract void b();
  
  public abstract void b(int paramInt);
  
  public abstract void b(int paramInt, Bundle paramBundle);
  
  public abstract void c();
  
  public abstract void d();
  
  public abstract void e();
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */