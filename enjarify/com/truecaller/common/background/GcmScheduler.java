package com.truecaller.common.background;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;
import com.truecaller.common.b.a;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;

public final class GcmScheduler
  extends GcmTaskService
{
  public final int a(TaskParams paramTaskParams)
  {
    int i = 1;
    Object localObject1 = new String[i];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("onRunTask() taskParams: ");
    String str = org.c.a.a.a.a.b.a(paramTaskParams);
    ((StringBuilder)localObject2).append(str);
    localObject2 = ((StringBuilder)localObject2).toString();
    str = null;
    localObject1[0] = localObject2;
    AssertionUtil.OnlyInDebug.notOnMainThread(new String[0]);
    localObject1 = a;
    int j = 2;
    try
    {
      int k = Integer.parseInt((String)localObject1);
      b localb = getApplicationc;
      paramTaskParams = b;
      paramTaskParams = localb.a(k, paramTaskParams);
      int[] arrayOfInt = GcmScheduler.1.a;
      int m = paramTaskParams.ordinal();
      k = arrayOfInt[m];
      switch (k)
      {
      default: 
        localObject1 = GcmScheduler.1.a;
        int n = paramTaskParams.ordinal();
        n = localObject1[n];
        switch (n)
        {
        default: 
          paramTaskParams = new String[0];
          AssertionUtil.isTrue(false, paramTaskParams);
          return j;
        }
        break;
      }
      GcmNetworkManager.a(this).a((String)localObject1, GcmScheduler.class);
      return j;
      return j;
      return i;
      return 0;
    }
    catch (NumberFormatException localNumberFormatException) {}
    return j;
  }
  
  public final void a()
  {
    new String[1][0] = "onInitializeTasks()";
    super.a();
    getApplicationc.a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.GcmScheduler
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */