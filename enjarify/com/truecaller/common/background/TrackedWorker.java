package com.truecaller.common.background;

import android.content.Context;
import android.os.SystemClock;
import androidx.work.ListenableWorker.a;
import androidx.work.ListenableWorker.a.a;
import androidx.work.ListenableWorker.a.b;
import androidx.work.ListenableWorker.a.c;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.debug.log.a;

public abstract class TrackedWorker
  extends Worker
{
  public TrackedWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
  }
  
  private static String a(ListenableWorker.a parama)
  {
    boolean bool1 = parama instanceof ListenableWorker.a.c;
    if (bool1) {
      return "Success";
    }
    bool1 = parama instanceof ListenableWorker.a.b;
    if (bool1) {
      return "Retry";
    }
    boolean bool2 = parama instanceof ListenableWorker.a.a;
    if (bool2) {
      return "Failure";
    }
    return "Unknown";
  }
  
  private static void a(String paramString)
  {
    int i = 1;
    new String[i][0] = paramString;
    Object[] arrayOfObject = new Object[i];
    arrayOfObject[0] = paramString;
    a.a(arrayOfObject);
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject1 = getClass().getSimpleName();
    Object localObject2 = "javaClass.simpleName";
    k.a(localObject1, (String)localObject2);
    boolean bool = c();
    if (!bool)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Worker ");
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(" was not run");
      a(((StringBuilder)localObject2).toString());
      localObject1 = ListenableWorker.a.a();
      k.a(localObject1, "Result.success()");
      return (ListenableWorker.a)localObject1;
    }
    long l1 = SystemClock.elapsedRealtime();
    ListenableWorker.a locala = d();
    long l2 = SystemClock.elapsedRealtime() - l1;
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Worker ");
    ((StringBuilder)localObject2).append((String)localObject1);
    ((StringBuilder)localObject2).append(" finished with result ");
    Object localObject3 = a(locala);
    ((StringBuilder)localObject2).append((String)localObject3);
    ((StringBuilder)localObject2).append(" after ");
    ((StringBuilder)localObject2).append(l2);
    ((StringBuilder)localObject2).append(" ms");
    a(((StringBuilder)localObject2).toString());
    localObject2 = b();
    localObject3 = new com/truecaller/analytics/e$a;
    ((e.a)localObject3).<init>("BackgroundWork");
    localObject1 = ((e.a)localObject3).a("WorkName", (String)localObject1);
    String str = a(locala);
    localObject1 = ((e.a)localObject1).a("Result", str);
    localObject3 = Double.valueOf(l2);
    localObject1 = ((e.a)localObject1).a((Double)localObject3).a();
    k.a(localObject1, "AnalyticsEvent.Builder(B…\n                .build()");
    ((b)localObject2).b((e)localObject1);
    return locala;
  }
  
  public abstract b b();
  
  public abstract boolean c();
  
  public abstract ListenableWorker.a d();
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.TrackedWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */