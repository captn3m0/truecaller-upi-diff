package com.truecaller.common.background;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.SparseArray;
import com.truecaller.log.AssertionUtil;
import java.util.ArrayList;
import java.util.List;
import org.c.a.a.a.a;

final class d$a
  extends Handler
{
  final Runnable a;
  private final int b = 0;
  private final int c = 1;
  private final Context d;
  private final c e;
  private final f f;
  
  private d$a(Looper paramLooper, Context paramContext, f paramf, c paramc)
  {
    super(paramLooper);
    paramLooper = new com/truecaller/common/background/d$a$1;
    paramLooper.<init>(this);
    a = paramLooper;
    e = paramc;
    f = paramf;
    d = paramContext;
  }
  
  private void a(PersistentBackgroundTask paramPersistentBackgroundTask, List paramList1, List paramList2)
  {
    if (paramPersistentBackgroundTask == null) {
      return;
    }
    Context localContext1 = d;
    boolean bool1 = paramPersistentBackgroundTask.b(localContext1);
    Context localContext2 = d;
    boolean bool2 = paramPersistentBackgroundTask.a(localContext2);
    if ((bool2) && (bool1))
    {
      paramList1.add(paramPersistentBackgroundTask);
      d.a(paramPersistentBackgroundTask, "was scheduled for launch");
      return;
    }
    paramList2.add(paramPersistentBackgroundTask);
    if (!bool1)
    {
      d.a(paramPersistentBackgroundTask, " was successfully executed recently and was NOT scheduled for launch");
      return;
    }
    d.a(paramPersistentBackgroundTask, "is disabled and was NOT scheduled for launch");
  }
  
  private void a(List paramList1, List paramList2)
  {
    try
    {
      boolean bool1 = paramList2.isEmpty();
      if (!bool1)
      {
        c localc = e;
        localc.b(paramList2);
      }
      boolean bool2 = paramList1.isEmpty();
      if (!bool2)
      {
        paramList2 = e;
        paramList2.a(paramList1);
      }
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localRuntimeException;
    }
  }
  
  final void a(long paramLong, int paramInt, int... paramVarArgs)
  {
    if (paramVarArgs != null)
    {
      localObject = a.b(paramVarArgs, paramInt);
    }
    else
    {
      int i = 1;
      paramVarArgs = new int[i];
      paramVarArgs[0] = paramInt;
      localObject = paramVarArgs;
    }
    Object localObject = obtainMessage(0, localObject);
    sendMessageDelayed((Message)localObject, paramLong);
  }
  
  public final void handleMessage(Message paramMessage)
  {
    int i = what;
    Object localObject1;
    Object localObject2;
    switch (i)
    {
    default: 
      break;
    case 1: 
      paramMessage = (d.b)obj;
      if (paramMessage != null)
      {
        localObject1 = b;
        localObject2 = a;
        paramMessage = c;
        ((b.a)localObject1).a((ExecutionResult)localObject2, paramMessage);
      }
      break;
    case 0: 
      paramMessage = (int[])obj;
      if (paramMessage != null)
      {
        localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>();
        localObject2 = new java/util/ArrayList;
        ((ArrayList)localObject2).<init>();
        int j = paramMessage.length;
        int k = 0;
        while (k < j)
        {
          int m = paramMessage[k];
          SparseArray localSparseArray = f.a();
          PersistentBackgroundTask localPersistentBackgroundTask = (PersistentBackgroundTask)localSparseArray.get(m);
          a(localPersistentBackgroundTask, (List)localObject1, (List)localObject2);
          k += 1;
        }
        a((List)localObject1, (List)localObject2);
        return;
      }
      break;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */