package com.truecaller.common.background;

import android.app.job.JobInfo;
import android.app.job.JobInfo.Builder;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.PersistableBundle;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

final class NativeScheduler$a
  implements c
{
  private final Context a;
  private final JobScheduler b;
  
  NativeScheduler$a(Context paramContext)
  {
    Context localContext = paramContext.getApplicationContext();
    a = localContext;
    paramContext = (JobScheduler)paramContext.getSystemService("jobscheduler");
    b = paramContext;
  }
  
  private static JobInfo.Builder a(PersistentBackgroundTask paramPersistentBackgroundTask, ComponentName paramComponentName, int paramInt)
  {
    paramPersistentBackgroundTask = paramPersistentBackgroundTask.b();
    PersistableBundle localPersistableBundle = new android/os/PersistableBundle;
    int i = 1;
    localPersistableBundle.<init>(i);
    Object localObject = new android/os/PersistableBundle;
    ((PersistableBundle)localObject).<init>();
    paramPersistentBackgroundTask.a((PersistableBundle)localObject);
    boolean bool1 = ((PersistableBundle)localObject).isEmpty();
    if (!bool1)
    {
      String str = "params";
      localPersistableBundle.putPersistableBundle(str, (PersistableBundle)localObject);
    }
    localObject = new android/app/job/JobInfo$Builder;
    ((JobInfo.Builder)localObject).<init>(paramInt, paramComponentName);
    boolean bool2 = e;
    paramComponentName = ((JobInfo.Builder)localObject).setRequiresCharging(bool2).setExtras(localPersistableBundle).setPersisted(i);
    paramInt = a;
    TimeUnit localTimeUnit;
    if (paramInt != 0)
    {
      localTimeUnit = TimeUnit.MILLISECONDS;
      l = paramPersistentBackgroundTask.a(localTimeUnit);
      paramComponentName.setPeriodic(l);
    }
    else
    {
      localTimeUnit = TimeUnit.MILLISECONDS;
      l = paramPersistentBackgroundTask.c(localTimeUnit);
      paramComponentName.setMinimumLatency(l);
      localTimeUnit = TimeUnit.MILLISECONDS;
      l = paramPersistentBackgroundTask.d(localTimeUnit);
      paramComponentName.setOverrideDeadline(l);
    }
    paramInt = b;
    switch (paramInt)
    {
    default: 
      paramInt = 0;
      localTimeUnit = null;
      break;
    case 2: 
      paramInt = 2;
      break;
    case 1: 
      paramInt = 1;
    }
    paramComponentName.setRequiredNetworkType(paramInt);
    long l = f;
    paramComponentName.setBackoffCriteria(l, i);
    return paramComponentName;
  }
  
  private JobInfo a(int paramInt)
  {
    Iterator localIterator = b.getAllPendingJobs().iterator();
    JobInfo localJobInfo;
    int i;
    do
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localJobInfo = (JobInfo)localIterator.next();
      i = localJobInfo.getId();
    } while (i != paramInt);
    return localJobInfo;
    return null;
  }
  
  public final void a(List paramList)
  {
    ComponentName localComponentName = new android/content/ComponentName;
    Object localObject1 = a.getPackageName();
    Object localObject2 = NativeScheduler.class.getName();
    localComponentName.<init>((String)localObject1, (String)localObject2);
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (PersistentBackgroundTask)paramList.next();
      int i = ((PersistentBackgroundTask)localObject1).a();
      localObject2 = a(i);
      if (localObject2 != null)
      {
        int j = ((JobInfo)localObject2).getId();
        localObject1 = a((PersistentBackgroundTask)localObject1, localComponentName, j).build();
        j = ((JobInfo)localObject1).getId();
        int n = ((JobInfo)localObject2).getId();
        if (j == n)
        {
          boolean bool2 = ((JobInfo)localObject1).isRequireCharging();
          boolean bool4 = ((JobInfo)localObject2).isRequireCharging();
          if (bool2 == bool4)
          {
            bool2 = ((JobInfo)localObject1).isRequireDeviceIdle();
            bool4 = ((JobInfo)localObject2).isRequireDeviceIdle();
            if (bool2 == bool4)
            {
              int k = ((JobInfo)localObject1).getNetworkType();
              int i1 = ((JobInfo)localObject2).getNetworkType();
              if (k == i1)
              {
                long l1 = ((JobInfo)localObject1).getMinLatencyMillis();
                long l2 = ((JobInfo)localObject2).getMinLatencyMillis();
                boolean bool6 = l1 < l2;
                if (!bool6)
                {
                  l1 = ((JobInfo)localObject1).getMaxExecutionDelayMillis();
                  l2 = ((JobInfo)localObject2).getMaxExecutionDelayMillis();
                  bool6 = l1 < l2;
                  if (!bool6)
                  {
                    boolean bool3 = ((JobInfo)localObject1).isPeriodic();
                    boolean bool5 = ((JobInfo)localObject2).isPeriodic();
                    if (bool3 == bool5)
                    {
                      bool3 = ((JobInfo)localObject1).isPersisted();
                      bool5 = ((JobInfo)localObject2).isPersisted();
                      if (bool3 == bool5)
                      {
                        l1 = ((JobInfo)localObject1).getIntervalMillis();
                        l2 = ((JobInfo)localObject2).getIntervalMillis();
                        bool6 = l1 < l2;
                        if (!bool6)
                        {
                          l1 = ((JobInfo)localObject1).getInitialBackoffMillis();
                          l2 = ((JobInfo)localObject2).getInitialBackoffMillis();
                          bool6 = l1 < l2;
                          if (!bool6)
                          {
                            int m = ((JobInfo)localObject1).getBackoffPolicy();
                            i = ((JobInfo)localObject2).getBackoffPolicy();
                            if (m == i)
                            {
                              i = 1;
                              break label345;
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        i = 0;
        localObject2 = null;
        label345:
        if (i == 0)
        {
          localObject2 = b;
          ((JobScheduler)localObject2).schedule((JobInfo)localObject1);
        }
      }
      else
      {
        i = ((PersistentBackgroundTask)localObject1).a();
        localObject1 = a((PersistentBackgroundTask)localObject1, localComponentName, i).build();
        localObject2 = b;
        ((JobScheduler)localObject2).schedule((JobInfo)localObject1);
      }
    }
  }
  
  public final boolean a()
  {
    return false;
  }
  
  public final boolean a(e parame)
  {
    return true;
  }
  
  public final void b(List paramList)
  {
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      PersistentBackgroundTask localPersistentBackgroundTask = (PersistentBackgroundTask)paramList.next();
      JobScheduler localJobScheduler = b;
      int i = localPersistentBackgroundTask.a();
      localJobScheduler.cancel(i);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.background.NativeScheduler.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */