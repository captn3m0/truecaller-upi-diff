package com.truecaller.common.e;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import c.n.m;
import c.u;
import com.truecaller.common.h.h;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public final class e
{
  public static c a(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "countryIso");
    String str = "usedLanguageISO";
    c.g.b.k.b(paramString2, str);
    paramString1 = h.d(paramString1).iterator();
    boolean bool1;
    boolean bool2;
    do
    {
      bool1 = paramString1.hasNext();
      bool2 = true;
      if (!bool1) {
        break;
      }
      str = (String)paramString1.next();
      bool2 = m.a(str, paramString2, bool2);
    } while (bool2);
    c.g.b.k.a(str, "lang");
    return b(str);
    paramString1 = d.a;
    paramString1 = h.d(d.b()).iterator();
    boolean bool3;
    do
    {
      bool1 = paramString1.hasNext();
      if (!bool1) {
        break;
      }
      str = (String)paramString1.next();
      bool3 = m.a(str, paramString2, bool2);
    } while (bool3);
    c.g.b.k.a(str, "lang");
    return b(str);
    paramString1 = d.a;
    return d.a();
  }
  
  public static Locale a(Context paramContext)
  {
    String str = "context";
    c.g.b.k.b(paramContext, str);
    try
    {
      paramContext = paramContext.getResources();
      str = "context.resources";
      c.g.b.k.a(paramContext, str);
      paramContext = paramContext.getConfiguration();
      paramContext = locale;
    }
    catch (NullPointerException localNullPointerException)
    {
      paramContext = null;
    }
    return paramContext;
  }
  
  public static Locale a(Locale paramLocale)
  {
    c.g.b.k.b(paramLocale, "defaultLocale");
    Locale localLocale = c();
    if (localLocale != null) {
      paramLocale = localLocale;
    }
    return paramLocale;
  }
  
  public static Set a()
  {
    Object localObject1 = Locale.getAvailableLocales();
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    int i = localObject1.length;
    int j = 0;
    while (j < i)
    {
      Object localObject2 = localObject1[j];
      c.g.b.k.a(localObject2, "locale");
      String str1 = ((Locale)localObject2).getLanguage();
      Object localObject3 = "locale.language";
      c.g.b.k.a(str1, (String)localObject3);
      if (str1 != null)
      {
        str1 = str1.toLowerCase();
        c.g.b.k.a(str1, "(this as java.lang.String).toLowerCase()");
        localObject3 = ((Locale)localObject2).getCountry();
        String str2 = "locale.country";
        c.g.b.k.a(localObject3, str2);
        if (localObject3 != null)
        {
          localObject3 = ((String)localObject3).toLowerCase();
          c.g.b.k.a(localObject3, "(this as java.lang.String).toLowerCase()");
          localObject2 = ((Locale)localObject2).getVariant();
          str2 = "locale.variant";
          c.g.b.k.a(localObject2, str2);
          if (localObject2 != null)
          {
            localObject2 = ((String)localObject2).toLowerCase();
            c.g.b.k.a(localObject2, "(this as java.lang.String).toLowerCase()");
            localHashSet.add(str1);
            str2 = "zh";
            boolean bool = c.g.b.k.a(str1, str2);
            if (bool)
            {
              int k = ((String)localObject3).length();
              int m = 2;
              if (k > m)
              {
                localObject3 = new java/lang/StringBuilder;
                ((StringBuilder)localObject3).<init>();
                ((StringBuilder)localObject3).append(str1);
                str1 = "_";
                ((StringBuilder)localObject3).append(str1);
                ((StringBuilder)localObject3).append((String)localObject2);
                localObject2 = ((StringBuilder)localObject3).toString();
                localHashSet.add(localObject2);
              }
              else
              {
                localObject2 = new java/lang/StringBuilder;
                ((StringBuilder)localObject2).<init>();
                ((StringBuilder)localObject2).append(str1);
                str1 = "_";
                ((StringBuilder)localObject2).append(str1);
                ((StringBuilder)localObject2).append((String)localObject3);
                localObject2 = ((StringBuilder)localObject2).toString();
                localHashSet.add(localObject2);
              }
            }
            j += 1;
          }
          else
          {
            localObject1 = new c/u;
            ((u)localObject1).<init>("null cannot be cast to non-null type java.lang.String");
            throw ((Throwable)localObject1);
          }
        }
        else
        {
          localObject1 = new c/u;
          ((u)localObject1).<init>("null cannot be cast to non-null type java.lang.String");
          throw ((Throwable)localObject1);
        }
      }
      else
      {
        localObject1 = new c/u;
        ((u)localObject1).<init>("null cannot be cast to non-null type java.lang.String");
        throw ((Throwable)localObject1);
      }
    }
    return (Set)localHashSet;
  }
  
  public static boolean a(String paramString)
  {
    c.g.b.k.b(paramString, "iso");
    Object localObject1 = d.a;
    localObject1 = d.c();
    int i = localObject1.length;
    int j = 0;
    while (j < i)
    {
      Object localObject2 = localObject1[j];
      String str1 = com.truecaller.common.h.k.b();
      c.g.b.k.a(str1, "DeviceInfoUtils.getDeviceManufacturer()");
      Locale localLocale = Locale.ENGLISH;
      String str2 = "Locale.ENGLISH";
      c.g.b.k.a(localLocale, str2);
      if (str1 != null)
      {
        str1 = str1.toLowerCase(localLocale);
        c.g.b.k.a(str1, "(this as java.lang.String).toLowerCase(locale)");
        localLocale = localObject2[0];
        boolean bool1 = m.b(str1, localLocale, false);
        if (bool1)
        {
          int k = localObject2.length;
          int m = 0;
          localLocale = null;
          while (m < k)
          {
            str2 = localObject2[m];
            boolean bool2 = c.g.b.k.a(str2, paramString);
            if (bool2) {
              return false;
            }
            m += 1;
          }
        }
        j += 1;
      }
      else
      {
        paramString = new c/u;
        paramString.<init>("null cannot be cast to non-null type java.lang.String");
        throw paramString;
      }
    }
    return true;
  }
  
  public static c b(String paramString)
  {
    c.g.b.k.b(paramString, "languageISO");
    Iterator localIterator = b().iterator();
    c localc;
    boolean bool3;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localc = (c)localIterator.next();
      String str = b;
      boolean bool2 = true;
      bool3 = m.a(paramString, str, bool2);
    } while (!bool3);
    return localc;
    paramString = d.a;
    return d.a();
  }
  
  public static List b()
  {
    Object localObject = d.a;
    localObject = d.g();
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Set localSet = a();
    localObject = ((List)localObject).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject).hasNext();
      if (!bool1) {
        break label128;
      }
      c localc = (c)((Iterator)localObject).next();
      String str1 = b;
      if (str1 == null) {
        break;
      }
      str1 = str1.toLowerCase();
      String str2 = "(this as java.lang.String).toLowerCase()";
      c.g.b.k.a(str1, str2);
      boolean bool2 = localSet.contains(str1);
      if (bool2)
      {
        boolean bool3 = a(str1);
        if (bool3) {
          localArrayList.add(localc);
        }
      }
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type java.lang.String");
    throw ((Throwable)localObject);
    label128:
    return (List)localArrayList;
  }
  
  public static Locale c()
  {
    Object localObject1 = "android.app.ActivityManagerNative";
    try
    {
      localObject1 = Class.forName((String)localObject1);
      Object localObject2 = "getDefault";
      Object[] arrayOfObject = null;
      Object localObject3 = new Class[0];
      localObject2 = ((Class)localObject1).getMethod((String)localObject2, (Class[])localObject3);
      localObject3 = "getDefault";
      c.g.b.k.a(localObject2, (String)localObject3);
      boolean bool = true;
      ((Method)localObject2).setAccessible(bool);
      localObject3 = new Object[0];
      localObject3 = ((Method)localObject2).invoke(localObject1, (Object[])localObject3);
      int i = Build.VERSION.SDK_INT;
      int j = 26;
      if (i >= j)
      {
        localObject1 = localObject3.getClass();
        localObject1 = ((Class)localObject1).getName();
        localObject1 = Class.forName((String)localObject1);
      }
      localObject3 = "getConfiguration";
      Class[] arrayOfClass = new Class[0];
      localObject1 = ((Class)localObject1).getMethod((String)localObject3, arrayOfClass);
      localObject3 = new Object[0];
      localObject2 = ((Method)localObject2).invoke(null, (Object[])localObject3);
      arrayOfObject = new Object[0];
      localObject1 = ((Method)localObject1).invoke(localObject2, arrayOfObject);
      if (localObject1 != null)
      {
        localObject1 = (Configuration)localObject1;
        return locale;
      }
      localObject1 = new c/u;
      localObject2 = "null cannot be cast to non-null type android.content.res.Configuration";
      ((u)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    }
    catch (Exception localException)
    {
      com.truecaller.log.d.a((Throwable)localException);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.e.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */