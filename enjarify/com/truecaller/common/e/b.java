package com.truecaller.common.e;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public final class b
  extends Calendar
{
  public static int[] a;
  public static int[] b;
  static final int[] c;
  static final int[] d;
  static final int[] e;
  private static TimeZone f;
  private static boolean g;
  private GregorianCalendar h;
  
  static
  {
    int i = 12;
    int[] arrayOfInt1 = new int[i];
    int[] tmp8_7 = arrayOfInt1;
    int[] tmp9_8 = tmp8_7;
    int[] tmp9_8 = tmp8_7;
    tmp9_8[0] = 31;
    tmp9_8[1] = 28;
    int[] tmp18_9 = tmp9_8;
    int[] tmp18_9 = tmp9_8;
    tmp18_9[2] = 31;
    tmp18_9[3] = 30;
    int[] tmp27_18 = tmp18_9;
    int[] tmp27_18 = tmp18_9;
    tmp27_18[4] = 31;
    tmp27_18[5] = 30;
    int[] tmp36_27 = tmp27_18;
    int[] tmp36_27 = tmp27_18;
    tmp36_27[6] = 31;
    tmp36_27[7] = 31;
    int[] tmp47_36 = tmp36_27;
    int[] tmp47_36 = tmp36_27;
    tmp47_36[8] = 30;
    tmp47_36[9] = 31;
    tmp47_36[10] = 30;
    tmp47_36[11] = 31;
    a = arrayOfInt1;
    int[] arrayOfInt2 = new int[i];
    int[] tmp77_76 = arrayOfInt2;
    int[] tmp78_77 = tmp77_76;
    int[] tmp78_77 = tmp77_76;
    tmp78_77[0] = 31;
    tmp78_77[1] = 31;
    int[] tmp87_78 = tmp78_77;
    int[] tmp87_78 = tmp78_77;
    tmp87_78[2] = 31;
    tmp87_78[3] = 31;
    int[] tmp96_87 = tmp87_78;
    int[] tmp96_87 = tmp87_78;
    tmp96_87[4] = 31;
    tmp96_87[5] = 31;
    int[] tmp105_96 = tmp96_87;
    int[] tmp105_96 = tmp96_87;
    tmp105_96[6] = 30;
    tmp105_96[7] = 30;
    int[] tmp116_105 = tmp105_96;
    int[] tmp116_105 = tmp105_96;
    tmp116_105[8] = 30;
    tmp116_105[9] = 30;
    tmp116_105[10] = 30;
    tmp116_105[11] = 29;
    b = arrayOfInt2;
    f = TimeZone.getDefault();
    g = false;
    i = 17;
    arrayOfInt1 = new int[i];
    int[] tmp159_158 = arrayOfInt1;
    int[] tmp160_159 = tmp159_158;
    int[] tmp160_159 = tmp159_158;
    tmp160_159[0] = 0;
    tmp160_159[1] = 1;
    int[] tmp167_160 = tmp160_159;
    int[] tmp167_160 = tmp160_159;
    tmp167_160[2] = 0;
    tmp167_160[3] = 1;
    int[] tmp174_167 = tmp167_160;
    int[] tmp174_167 = tmp167_160;
    tmp174_167[4] = 0;
    tmp174_167[5] = 1;
    int[] tmp181_174 = tmp174_167;
    int[] tmp181_174 = tmp174_167;
    tmp181_174[6] = 1;
    tmp181_174[7] = 7;
    int[] tmp191_181 = tmp181_174;
    int[] tmp191_181 = tmp181_174;
    tmp191_181[8] = 1;
    tmp191_181[9] = 0;
    int[] tmp200_191 = tmp191_181;
    int[] tmp200_191 = tmp191_181;
    tmp200_191[10] = 0;
    tmp200_191[11] = 0;
    int[] tmp209_200 = tmp200_191;
    int[] tmp209_200 = tmp200_191;
    tmp209_200[12] = 0;
    tmp209_200[13] = 0;
    tmp209_200[14] = 0;
    int[] tmp222_209 = tmp209_200;
    tmp222_209[15] = -46800000;
    tmp222_209[16] = 0;
    c = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp241_240 = arrayOfInt1;
    int[] tmp242_241 = tmp241_240;
    int[] tmp242_241 = tmp241_240;
    tmp242_241[0] = 1;
    tmp242_241[1] = 292269054;
    int[] tmp250_242 = tmp242_241;
    int[] tmp250_242 = tmp242_241;
    tmp250_242[2] = 11;
    tmp250_242[3] = 52;
    int[] tmp259_250 = tmp250_242;
    int[] tmp259_250 = tmp250_242;
    tmp259_250[4] = 4;
    tmp259_250[5] = 28;
    int[] tmp267_259 = tmp259_250;
    int[] tmp267_259 = tmp259_250;
    tmp267_259[6] = 'ŭ';
    tmp267_259[7] = 6;
    int[] tmp279_267 = tmp267_259;
    int[] tmp279_267 = tmp267_259;
    tmp279_267[8] = 4;
    tmp279_267[9] = 1;
    int[] tmp288_279 = tmp279_267;
    int[] tmp288_279 = tmp279_267;
    tmp288_279[10] = 11;
    tmp288_279[11] = 23;
    int[] tmp299_288 = tmp288_279;
    int[] tmp299_288 = tmp288_279;
    tmp299_288[12] = 59;
    tmp299_288[13] = 59;
    tmp299_288[14] = 'ϧ';
    int[] tmp316_299 = tmp299_288;
    tmp316_299[15] = 50400000;
    tmp316_299[16] = 1200000;
    d = arrayOfInt1;
    arrayOfInt2 = new int[i];
    int[] tmp336_335 = arrayOfInt2;
    int[] tmp337_336 = tmp336_335;
    int[] tmp337_336 = tmp336_335;
    tmp337_336[0] = 1;
    tmp337_336[1] = 292278994;
    int[] tmp345_337 = tmp337_336;
    int[] tmp345_337 = tmp337_336;
    tmp345_337[2] = 11;
    tmp345_337[3] = 53;
    int[] tmp354_345 = tmp345_337;
    int[] tmp354_345 = tmp345_337;
    tmp354_345[4] = 6;
    tmp354_345[5] = 31;
    int[] tmp363_354 = tmp354_345;
    int[] tmp363_354 = tmp354_345;
    tmp363_354[6] = 'Ů';
    tmp363_354[7] = 6;
    int[] tmp375_363 = tmp363_354;
    int[] tmp375_363 = tmp363_354;
    tmp375_363[8] = 6;
    tmp375_363[9] = 1;
    int[] tmp385_375 = tmp375_363;
    int[] tmp385_375 = tmp375_363;
    tmp385_375[10] = 11;
    tmp385_375[11] = 23;
    int[] tmp396_385 = tmp385_375;
    int[] tmp396_385 = tmp385_375;
    tmp396_385[12] = 59;
    tmp396_385[13] = 59;
    tmp396_385[14] = 'ϧ';
    int[] tmp413_396 = tmp396_385;
    tmp413_396[15] = 50400000;
    tmp413_396[16] = 7200000;
    e = arrayOfInt2;
  }
  
  private static int a(int paramInt1, int paramInt2)
  {
    b.a locala1 = new com/truecaller/common/e/b$a;
    int i = 1;
    locala1.<init>(paramInt2, 0, i);
    b.a locala2 = b(locala1);
    paramInt2 = c(locala2);
    switch (paramInt2)
    {
    default: 
      break;
    case 7: 
      paramInt1 += -1;
      break;
    case 6: 
      paramInt1 += 5;
      break;
    case 5: 
      paramInt1 += 4;
      break;
    case 4: 
      paramInt1 += 3;
      break;
    case 3: 
      paramInt1 += 2;
      break;
    case 2: 
      paramInt1 += 1;
    }
    return paramInt1 / 7 + i;
  }
  
  public static b.a a(b.a parama)
  {
    int i = b;
    int j = 11;
    if (i <= j)
    {
      i = b;
      int k = -11;
      if (i >= k)
      {
        i = a + 63936;
        a = i;
        i = c;
        k = 1;
        i -= k;
        c = i;
        i = a * 365;
        int m = (a + 3) / 4;
        i += m;
        m = (a + 99) / 100;
        i -= m;
        m = (a + 399) / 400;
        i += m;
        m = 0;
        int n = i;
        i = 0;
        Object localObject = null;
        int i1;
        for (;;)
        {
          i1 = b;
          if (i >= i1) {
            break;
          }
          int[] arrayOfInt = a;
          i1 = arrayOfInt[i];
          n += i1;
          i += 1;
        }
        i = b;
        if (i > k)
        {
          i = a % 4;
          if (i == 0)
          {
            i = a % 100;
            if (i != 0) {}
          }
          else
          {
            i = a % 400;
            if (i != 0) {
              break label216;
            }
          }
          n += 1;
        }
        label216:
        int i2 = c;
        n = n + i2 + -79;
        i2 = n / 12053;
        n %= 12053;
        i2 = i2 * 33 + 979;
        i = n / 1461 * 4;
        i2 += i;
        n %= 1461;
        i = 366;
        if (n >= i)
        {
          n += -1;
          i = n / 365;
          i2 += i;
          n %= 365;
        }
        while (m < j)
        {
          localObject = b;
          i1 = localObject[m];
          if (n < i1) {
            break;
          }
          i = localObject[m];
          n -= i;
          m += 1;
        }
        n += k;
        localObject = new com/truecaller/common/e/b$a;
        ((b.a)localObject).<init>(i2, m, n);
        return (b.a)localObject;
      }
    }
    parama = new java/lang/IllegalArgumentException;
    parama.<init>();
    throw parama;
  }
  
  private static boolean a(int paramInt)
  {
    paramInt %= 33;
    int i = 1;
    if (paramInt != i)
    {
      int j = 5;
      if (paramInt != j)
      {
        j = 9;
        if (paramInt != j)
        {
          j = 13;
          if (paramInt != j)
          {
            j = 17;
            if (paramInt != j)
            {
              j = 22;
              if (paramInt != j)
              {
                j = 26;
                if (paramInt != j)
                {
                  j = 30;
                  if (paramInt != j) {
                    return false;
                  }
                }
              }
            }
          }
        }
      }
    }
    return i;
  }
  
  private static b.a b(b.a parama)
  {
    int i = b;
    int j = 11;
    if (i <= j)
    {
      i = b;
      j = -11;
      if (i >= j)
      {
        i = a + 64557;
        a = i;
        i = c;
        j = 1;
        i -= j;
        c = i;
        i = a;
        int k = 365;
        i *= 365;
        int m = a / 33 * 8;
        i += m;
        m = (a % 33 + 3) / 4;
        i += m;
        m = 0;
        int n = i;
        i = 0;
        int[] arrayOfInt;
        for (;;)
        {
          i1 = b;
          if (i >= i1) {
            break;
          }
          arrayOfInt = b;
          i1 = arrayOfInt[i];
          n += i1;
          i += 1;
        }
        int i2 = c;
        n = n + i2 + 79;
        i2 = 146097;
        i = n / i2 * 400 + 1600;
        n %= i2;
        i2 = 36525;
        if (n >= i2)
        {
          n += -1;
          i2 = 36524;
          i1 = n / i2 * 100;
          i += i1;
          n %= i2;
          if (n >= k)
          {
            n += 1;
            i2 = 1;
          }
          else
          {
            i2 = 0;
            parama = null;
          }
        }
        else
        {
          i2 = 1;
        }
        int i1 = n / 1461 * 4;
        i += i1;
        n %= 1461;
        i1 = 366;
        if (n >= i1)
        {
          n += -1;
          i2 = n / 365;
          i += i2;
          n %= k;
          i2 = 0;
          parama = null;
        }
        k = 0;
        for (;;)
        {
          arrayOfInt = a;
          i1 = arrayOfInt[k];
          int i3;
          if ((k == j) && (i2 == j)) {
            i3 = k;
          } else {
            i3 = 0;
          }
          i1 += i3;
          if (n < i1) {
            break;
          }
          arrayOfInt = a;
          i1 = arrayOfInt[k];
          if ((k == j) && (i2 == j)) {
            i3 = k;
          } else {
            i3 = 0;
          }
          i1 += i3;
          n -= i1;
          k += 1;
        }
        n += j;
        parama = new com/truecaller/common/e/b$a;
        parama.<init>(i, k, n);
        return parama;
      }
    }
    parama = new java/lang/IllegalArgumentException;
    parama.<init>();
    throw parama;
  }
  
  private static int c(b.a parama)
  {
    GregorianCalendar localGregorianCalendar = new java/util/GregorianCalendar;
    int i = a;
    int j = b;
    int k = c;
    localGregorianCalendar.<init>(i, j, k);
    return localGregorianCalendar.get(7);
  }
  
  public final void add(int paramInt1, int paramInt2)
  {
    int i = paramInt1;
    int m = paramInt2;
    int n = 30;
    int i1 = 11;
    int i2 = 12;
    int i3 = 5;
    int i4 = 2;
    int i5 = 1;
    if (paramInt1 == i4)
    {
      i = get(i4) + paramInt2;
      m = i / 12;
      add(i5, m);
      i %= i2;
      super.set(i4, i);
      m = get(i3);
      int[] arrayOfInt = b;
      i6 = arrayOfInt[i];
      if (m > i6)
      {
        i = arrayOfInt[i];
        super.set(i3, i);
        i = get(i4);
        if (i == i1)
        {
          boolean bool1 = a(get(i5));
          if (bool1) {
            super.set(i3, n);
          }
        }
      }
      complete();
      return;
    }
    if (paramInt1 == i5)
    {
      int j = get(i5) + paramInt2;
      super.set(i5, j);
      j = get(i3);
      if (j == n)
      {
        j = get(i4);
        if (j == i1)
        {
          boolean bool2 = a(get(i5));
          if (!bool2)
          {
            k = 29;
            super.set(i3, k);
          }
        }
      }
      complete();
      return;
    }
    Object localObject = new com/truecaller/common/e/b$a;
    int i6 = get(i5);
    int i7 = get(i4);
    int i8 = get(i3);
    ((b.a)localObject).<init>(i6, i7, i8);
    localObject = b((b.a)localObject);
    GregorianCalendar localGregorianCalendar = new java/util/GregorianCalendar;
    i7 = a;
    i8 = b;
    int i9 = c;
    int i10 = get(i1);
    int i11 = get(i2);
    int i12 = get(13);
    localObject = localGregorianCalendar;
    localGregorianCalendar.<init>(i7, i8, i9, i10, i11, i12);
    localGregorianCalendar.add(paramInt1, paramInt2);
    b.a locala = new com/truecaller/common/e/b$a;
    m = localGregorianCalendar.get(i5);
    i6 = localGregorianCalendar.get(i4);
    i7 = localGregorianCalendar.get(i3);
    locala.<init>(m, i6, i7);
    locala = a(locala);
    m = a;
    super.set(i5, m);
    m = b;
    super.set(i4, m);
    int k = c;
    super.set(i3, k);
    k = localGregorianCalendar.get(i1);
    super.set(i1, k);
    k = localGregorianCalendar.get(i2);
    super.set(i2, k);
    k = 13;
    m = localGregorianCalendar.get(k);
    super.set(k, m);
    complete();
  }
  
  protected final void computeFields()
  {
    boolean bool = isTimeSet;
    int i = areFieldsSet;
    if (i == 0)
    {
      i = 1;
      setMinimalDaysInFirstWeek(i);
      int j = 7;
      setFirstDayOfWeek(j);
      int k = 0;
      int[] arrayOfInt1 = null;
      int m = 0;
      Object localObject = null;
      for (;;)
      {
        int[] arrayOfInt2 = fields;
        n = 2;
        i1 = arrayOfInt2[n];
        if (k >= i1) {
          break;
        }
        arrayOfInt2 = b;
        n = k + 1;
        k = arrayOfInt2[k];
        m += k;
        k = n;
      }
      arrayOfInt1 = fields;
      int i1 = 5;
      k = arrayOfInt1[i1];
      m += k;
      k = 6;
      super.set(k, m);
      localObject = new com/truecaller/common/e/b$a;
      int[] arrayOfInt3 = fields;
      int i2 = arrayOfInt3[i];
      int[] arrayOfInt4 = fields;
      int i3 = arrayOfInt4[n];
      int[] arrayOfInt5 = fields;
      int i4 = arrayOfInt5[i1];
      ((b.a)localObject).<init>(i2, i3, i4);
      m = c(b((b.a)localObject));
      super.set(j, m);
      localObject = fields;
      m = localObject[i1];
      i2 = 8;
      if (m > 0)
      {
        localObject = fields;
        m = localObject[i1];
        if (m < i2) {
          super.set(i2, i);
        }
      }
      localObject = fields;
      m = localObject[i1];
      if (j < m)
      {
        arrayOfInt6 = fields;
        j = arrayOfInt6[i1];
        m = 15;
        if (j < m) {
          super.set(i2, n);
        }
      }
      j = 14;
      localObject = fields;
      m = localObject[i1];
      int n = 3;
      if (j < m)
      {
        arrayOfInt6 = fields;
        j = arrayOfInt6[i1];
        m = 22;
        if (j < m) {
          super.set(i2, n);
        }
      }
      j = 21;
      localObject = fields;
      m = localObject[i1];
      i3 = 4;
      if (j < m)
      {
        arrayOfInt6 = fields;
        j = arrayOfInt6[i1];
        m = 29;
        if (j < m) {
          super.set(i2, i3);
        }
      }
      j = 28;
      localObject = fields;
      m = localObject[i1];
      if (j < m)
      {
        arrayOfInt6 = fields;
        j = arrayOfInt6[i1];
        m = 32;
        if (j < m) {
          super.set(i2, i1);
        }
      }
      j = fields[k];
      m = fields[i];
      j = a(j, m);
      super.set(n, j);
      int[] arrayOfInt6 = fields;
      j = arrayOfInt6[k];
      m = fields[i];
      j = a(j, m);
      k = fields[k];
      m = fields[i1];
      k -= m;
      localObject = fields;
      m = localObject[i];
      k = a(k, m);
      j = j - k + i;
      super.set(i3, j);
      isTimeSet = bool;
    }
  }
  
  protected final void computeTime()
  {
    b localb = this;
    boolean bool1 = isTimeSet;
    int j = 5;
    int k = 2;
    int m = 23;
    int n = 0;
    int i1 = 1;
    int i2 = 16;
    int i4 = 15;
    int i6 = 13;
    int i7 = 9;
    int i8 = 10;
    int i9 = 12;
    int i10 = 11;
    Object localObject1;
    int i5;
    int i3;
    label397:
    Object localObject2;
    long l;
    if (!bool1)
    {
      bool1 = g;
      if (!bool1)
      {
        localObject1 = GregorianCalendar.getInstance(f);
        boolean bool4 = isSet(i10);
        if (!bool4)
        {
          int i11 = ((Calendar)localObject1).get(i10);
          super.set(i10, i11);
        }
        boolean bool5 = localb.isSet(i8);
        if (!bool5)
        {
          int i12 = ((Calendar)localObject1).get(i8);
          localb.set(i8, i12);
        }
        boolean bool6 = localb.isSet(i9);
        if (!bool6)
        {
          int i13 = ((Calendar)localObject1).get(i9);
          localb.set(i9, i13);
        }
        boolean bool7 = localb.isSet(i6);
        if (!bool7)
        {
          i14 = ((Calendar)localObject1).get(i6);
          localb.set(i6, i14);
        }
        int i14 = 14;
        boolean bool9 = localb.isSet(i14);
        if (!bool9)
        {
          i16 = ((Calendar)localObject1).get(i14);
          localb.set(i14, i16);
        }
        boolean bool8 = localb.isSet(i4);
        if (!bool8)
        {
          int i15 = ((Calendar)localObject1).get(i4);
          localb.set(i4, i15);
        }
        boolean bool3 = localb.isSet(i2);
        if (!bool3)
        {
          i5 = ((Calendar)localObject1).get(i2);
          localb.set(i2, i5);
        }
        boolean bool2 = localb.isSet(i7);
        if (!bool2)
        {
          i3 = ((Calendar)localObject1).get(i7);
          localb.set(i7, i3);
        }
        i3 = localb.internalGet(i10);
        if (i3 >= i9)
        {
          i3 = localb.internalGet(i10);
          if (i3 <= m)
          {
            localb.set(i7, i1);
            m = localb.internalGet(i10) - i9;
            localb.set(i8, m);
            break label397;
          }
        }
        m = localb.internalGet(i10);
        localb.set(i8, m);
        localb.set(i7, 0);
        b.a locala = new com/truecaller/common/e/b$a;
        n = localb.internalGet(i1);
        k = localb.internalGet(k);
        j = localb.internalGet(j);
        locala.<init>(n, k, j);
        localObject2 = b(locala);
        int i16 = a;
        k = b;
        j = c;
        int i17 = localb.internalGet(i10);
        int i18 = localb.internalGet(i9);
        int i19 = localb.internalGet(i6);
        ((Calendar)localObject1).set(i16, k, j, i17, i18, i19);
        l = ((Calendar)localObject1).getTimeInMillis();
        time = l;
        return;
      }
    }
    bool1 = isTimeSet;
    if (!bool1)
    {
      bool1 = g;
      if (bool1)
      {
        int i = localb.internalGet(i10);
        if (i >= i9)
        {
          i = localb.internalGet(i10);
          if (i <= m)
          {
            localb.set(i7, i1);
            i = localb.internalGet(i10) - i9;
            localb.set(i8, i);
            break label607;
          }
        }
        i = localb.internalGet(i10);
        localb.set(i8, i);
        localb.set(i7, 0);
        label607:
        localObject1 = new java/util/GregorianCalendar;
        ((GregorianCalendar)localObject1).<init>();
        h = ((GregorianCalendar)localObject1);
        i = f.getRawOffset();
        localb.set(i5, i);
        i = f.getDSTSavings();
        localb.set(i3, i);
        localObject1 = new com/truecaller/common/e/b$a;
        m = localb.internalGet(i1);
        k = localb.internalGet(k);
        j = localb.internalGet(j);
        ((b.a)localObject1).<init>(m, k, j);
        localObject1 = b((b.a)localObject1);
        localObject2 = h;
        k = a;
        m = b;
        n = c;
        i1 = localb.internalGet(i10);
        i3 = localb.internalGet(i9);
        i5 = localb.internalGet(i6);
        ((GregorianCalendar)localObject2).set(k, m, n, i1, i3, i5);
        localObject1 = h;
        l = ((GregorianCalendar)localObject1).getTimeInMillis();
        time = l;
      }
    }
  }
  
  public final int getGreatestMinimum(int paramInt)
  {
    return c[paramInt];
  }
  
  public final int getLeastMaximum(int paramInt)
  {
    return d[paramInt];
  }
  
  public final int getMaximum(int paramInt)
  {
    return e[paramInt];
  }
  
  public final int getMinimum(int paramInt)
  {
    return c[paramInt];
  }
  
  public final void roll(int paramInt1, int paramInt2)
  {
    if (paramInt2 == 0) {
      return;
    }
    if (paramInt1 >= 0)
    {
      int i = 15;
      if (paramInt1 < i)
      {
        complete();
        i = 6;
        int j = 30;
        int k = 0;
        int m = 9;
        int n = 5;
        int i1 = 12;
        int i2 = 10;
        int i3 = 1;
        int i4 = 2;
        int i5 = 11;
        int[] arrayOfInt;
        switch (paramInt1)
        {
        case 4: 
        case 8: 
        default: 
          localObject = new java/lang/IllegalArgumentException;
          ((IllegalArgumentException)localObject).<init>();
          throw ((Throwable)localObject);
        case 14: 
          paramInt1 = 14;
          i = (internalGet(paramInt1) + paramInt2) % 1000;
          if (i < 0) {
            i += 1000;
          }
          super.set(paramInt1, i);
          return;
        case 13: 
          paramInt1 = 13;
          i = (internalGet(paramInt1) + paramInt2) % 60;
          if (i < 0) {
            i += 60;
          }
          super.set(paramInt1, i);
          return;
        case 12: 
          paramInt1 = (internalGet(i1) + paramInt2) % 60;
          if (paramInt1 < 0) {
            paramInt1 += 60;
          }
          super.set(i1, paramInt1);
          return;
        case 11: 
          localObject = fields;
          i = (internalGet(i5) + paramInt2) % 24;
          localObject[i5] = i;
          paramInt1 = internalGet(i5);
          if (paramInt1 < 0)
          {
            localObject = fields;
            i = localObject[i5] + 24;
            localObject[i5] = i;
          }
          paramInt1 = internalGet(i5);
          if (paramInt1 < i1)
          {
            fields[m] = 0;
            localObject = fields;
            i = internalGet(i5);
            localObject[i2] = i;
          }
          else
          {
            fields[m] = i3;
            localObject = fields;
            i = internalGet(i5) - i1;
            localObject[i2] = i;
          }
          break;
        case 10: 
          paramInt1 = (internalGet(i2) + paramInt2) % i1;
          super.set(i2, paramInt1);
          paramInt1 = internalGet(i2);
          if (paramInt1 < 0)
          {
            localObject = fields;
            paramInt2 = localObject[i2] + i1;
            localObject[i2] = paramInt2;
          }
          paramInt1 = internalGet(m);
          if (paramInt1 == 0)
          {
            paramInt1 = internalGet(i2);
            super.set(i5, paramInt1);
            return;
          }
          paramInt1 = internalGet(i2) + i1;
          super.set(i5, paramInt1);
          return;
        case 9: 
          paramInt2 %= i4;
          if (paramInt2 == 0) {
            break label1038;
          }
          paramInt1 = internalGet(m);
          if (paramInt1 == 0)
          {
            localObject = fields;
            localObject[m] = i3;
          }
          else
          {
            localObject = fields;
            localObject[m] = 0;
          }
          paramInt1 = get(m);
          if (paramInt1 == 0)
          {
            paramInt1 = get(i2);
            super.set(i5, paramInt1);
            return;
          }
          paramInt1 = get(i2) + i1;
          super.set(i5, paramInt1);
          return;
        case 7: 
          paramInt1 = 7;
          paramInt2 %= paramInt1;
          if (paramInt2 < 0) {
            paramInt2 += 7;
          }
          while (k != paramInt2)
          {
            j = internalGet(paramInt1);
            if (j == i)
            {
              j = -6;
              add(n, j);
            }
            else
            {
              add(n, i3);
            }
            k += 1;
          }
        case 6: 
          paramInt1 = a(internalGet(i3));
          if (paramInt1 != 0) {
            paramInt1 = 366;
          } else {
            paramInt1 = 365;
          }
          i = (internalGet(i) + paramInt2) % paramInt1;
          if (i <= 0) {
            i += paramInt1;
          }
          paramInt1 = 0;
          localObject = null;
          while (i > k)
          {
            arrayOfInt = b;
            j = paramInt1 + 1;
            paramInt1 = arrayOfInt[paramInt1];
            k += paramInt1;
            paramInt1 = j;
          }
          paramInt1 += -1;
          super.set(i4, paramInt1);
          localObject = b;
          paramInt2 = internalGet(i4);
          paramInt1 = localObject[paramInt2];
          k -= i;
          paramInt1 -= k;
          super.set(n, paramInt1);
          return;
        case 5: 
          paramInt1 = get(i4);
          if (paramInt1 >= 0)
          {
            paramInt1 = get(i4);
            if (paramInt1 <= n)
            {
              paramInt1 = 31;
              break label785;
            }
          }
          paramInt1 = 0;
          localObject = null;
          k = get(i4);
          if (i <= k)
          {
            i = get(i4);
            if (i <= i2) {
              paramInt1 = 30;
            }
          }
          i = get(i4);
          if (i == i5)
          {
            paramInt1 = a(get(i3));
            if (paramInt1 == 0) {
              j = 29;
            }
          }
          else
          {
            j = paramInt1;
          }
          paramInt1 = (get(n) + paramInt2) % j;
          if (paramInt1 < 0) {
            paramInt1 += j;
          }
          super.set(n, paramInt1);
          return;
        case 3: 
          return;
        case 2: 
          label785:
          paramInt1 = (internalGet(i4) + paramInt2) % i1;
          if (paramInt1 < 0) {
            paramInt1 += 12;
          }
          super.set(i4, paramInt1);
          arrayOfInt = b;
          paramInt1 = arrayOfInt[paramInt1];
          paramInt2 = internalGet(i4);
          if (paramInt2 == i5)
          {
            paramInt2 = a(internalGet(i3));
            if (paramInt2 != 0) {
              paramInt1 = 30;
            }
          }
          paramInt2 = internalGet(n);
          if (paramInt2 <= paramInt1) {
            break label1038;
          }
          super.set(n, paramInt1);
          return;
        }
        paramInt1 = internalGet(i3) + paramInt2;
        super.set(i3, paramInt1);
        paramInt1 = internalGet(i4);
        if (paramInt1 == i5)
        {
          paramInt1 = internalGet(n);
          if (paramInt1 == j)
          {
            paramInt1 = a(internalGet(i3));
            if (paramInt1 == 0)
            {
              super.set(n, 29);
              return;
            }
          }
        }
        label1038:
        return;
      }
    }
    Object localObject = new java/lang/IllegalArgumentException;
    ((IllegalArgumentException)localObject).<init>();
    throw ((Throwable)localObject);
  }
  
  public final void roll(int paramInt, boolean paramBoolean)
  {
    if (paramBoolean) {
      paramBoolean = true;
    } else {
      paramBoolean = true;
    }
    roll(paramInt, paramBoolean);
  }
  
  public final void set(int paramInt1, int paramInt2)
  {
    int i = paramInt1;
    int j = paramInt2;
    boolean bool1 = false;
    int i2 = 11;
    int i4 = 5;
    int i5 = 2;
    int i6 = 1;
    int i3;
    switch (paramInt1)
    {
    case 8: 
    case 9: 
    default: 
      super.set(paramInt1, paramInt2);
      return;
    case 10: 
    case 11: 
    case 12: 
    case 13: 
    case 14: 
    case 15: 
    case 16: 
      bool1 = isSet(i6);
      if (bool1)
      {
        bool1 = isSet(i5);
        if (bool1)
        {
          bool1 = isSet(i4);
          if (bool1)
          {
            bool1 = isSet(10);
            if (bool1)
            {
              bool1 = isSet(i2);
              if (bool1)
              {
                int k = 12;
                boolean bool6 = isSet(k);
                if (bool6)
                {
                  int i7 = 13;
                  boolean bool7 = isSet(i7);
                  if (bool7)
                  {
                    bool7 = isSet(14);
                    if (bool7)
                    {
                      Object localObject = new java/util/GregorianCalendar;
                      ((GregorianCalendar)localObject).<init>();
                      h = ((GregorianCalendar)localObject);
                      localObject = new com/truecaller/common/e/b$a;
                      int i9 = internalGet(i6);
                      int i10 = internalGet(i5);
                      int i11 = internalGet(i4);
                      ((b.a)localObject).<init>(i9, i10, i11);
                      localObject = b((b.a)localObject);
                      GregorianCalendar localGregorianCalendar = h;
                      i10 = a;
                      i11 = b;
                      int i12 = c;
                      int i13 = internalGet(i2);
                      int i14 = internalGet(k);
                      int i15 = internalGet(i7);
                      localGregorianCalendar.set(i10, i11, i12, i13, i14, i15);
                      h.set(paramInt1, paramInt2);
                      b.a locala = new com/truecaller/common/e/b$a;
                      j = h.get(i6);
                      int i8 = h.get(i5);
                      i9 = h.get(i4);
                      locala.<init>(j, i8, i9);
                      locala = a(locala);
                      j = a;
                      super.set(i6, j);
                      j = b;
                      super.set(i5, j);
                      i = c;
                      super.set(i4, i);
                      i = h.get(i2);
                      super.set(i2, i);
                      i = h.get(k);
                      super.set(k, i);
                      i = h.get(i7);
                      super.set(i7, i);
                      return;
                    }
                  }
                }
              }
            }
          }
        }
      }
      super.set(paramInt1, paramInt2);
      return;
    case 7: 
      boolean bool2 = isSet(i6);
      if (bool2)
      {
        bool2 = isSet(i5);
        if (bool2)
        {
          bool2 = isSet(i4);
          if (bool2)
          {
            i = 7;
            j = paramInt2 % i;
            int m = get(i);
            j -= m;
            add(i, j);
            return;
          }
        }
      }
      super.set(paramInt1, paramInt2);
      return;
    case 6: 
      boolean bool5 = isSet(i6);
      if (bool5)
      {
        bool5 = isSet(i5);
        if (bool5)
        {
          bool5 = isSet(i4);
          if (bool5)
          {
            i3 = internalGet(i6);
            super.set(i6, i3);
            super.set(i5, 0);
            super.set(i4, 0);
            add(paramInt1, paramInt2);
            return;
          }
        }
      }
      super.set(paramInt1, paramInt2);
      return;
    case 5: 
      super.set(paramInt1, 0);
      add(paramInt1, paramInt2);
      return;
    case 4: 
      boolean bool3 = isSet(i6);
      if (bool3)
      {
        bool3 = isSet(i5);
        if (bool3)
        {
          bool3 = isSet(i4);
          if (bool3)
          {
            int n = get(4);
            j = paramInt2 - n;
            add(paramInt1, j);
            return;
          }
        }
      }
      super.set(paramInt1, paramInt2);
      return;
    case 3: 
      boolean bool4 = isSet(i6);
      if (bool4)
      {
        bool4 = isSet(i5);
        if (bool4)
        {
          bool4 = isSet(i4);
          if (bool4)
          {
            int i1 = get(3);
            j = paramInt2 - i1;
            add(paramInt1, j);
            return;
          }
        }
      }
      super.set(paramInt1, paramInt2);
      return;
    }
    if (paramInt2 > i3)
    {
      super.set(paramInt1, i3);
      j = paramInt2 - i3;
      add(paramInt1, j);
      return;
    }
    if (paramInt2 < 0)
    {
      super.set(paramInt1, 0);
      add(paramInt1, paramInt2);
      return;
    }
    super.set(paramInt1, paramInt2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */