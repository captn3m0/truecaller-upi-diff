package com.truecaller.common.e;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public final class a
{
  private static final char[] a;
  private final Paint b;
  private final Rect c;
  
  static
  {
    char[] arrayOfChar = new char[1];
    arrayOfChar[0] = 64991;
    a = arrayOfChar;
  }
  
  public a()
  {
    this(localPaint);
  }
  
  private a(Paint paramPaint)
  {
    b = paramPaint;
    paramPaint = new android/graphics/Rect;
    paramPaint.<init>();
    c = paramPaint;
    paramPaint = b;
    char[] arrayOfChar = a;
    Rect localRect = c;
    paramPaint.getTextBounds(arrayOfChar, 0, 1, localRect);
  }
  
  private void a(char[] paramArrayOfChar, Bitmap paramBitmap)
  {
    paramBitmap.eraseColor(0);
    Canvas localCanvas = new android/graphics/Canvas;
    localCanvas.<init>(paramBitmap);
    int i = paramArrayOfChar.length;
    Paint localPaint = b;
    localCanvas.drawText(paramArrayOfChar, 0, i, 0.0F, 0.0F, localPaint);
  }
  
  private boolean a(char paramChar)
  {
    Object localObject1 = new android/graphics/Rect;
    ((Rect)localObject1).<init>();
    int i = 1;
    char[] arrayOfChar = new char[i];
    int j = 0;
    arrayOfChar[0] = paramChar;
    b.getTextBounds(arrayOfChar, 0, i, (Rect)localObject1);
    Object localObject2 = c;
    paramChar = ((Rect)localObject2).equals(localObject1);
    if (paramChar != 0)
    {
      paramChar = ((Rect)localObject1).width();
      if (paramChar == 0)
      {
        paramChar = ((Rect)localObject1).height();
        if (paramChar == 0) {
          return false;
        }
      }
      paramChar = ((Rect)localObject1).width();
      j = ((Rect)localObject1).height();
      Bitmap.Config localConfig = Bitmap.Config.ARGB_8888;
      localObject2 = Bitmap.createBitmap(paramChar, j, localConfig);
      j = ((Rect)localObject1).width();
      int k = ((Rect)localObject1).height();
      localConfig = Bitmap.Config.ARGB_8888;
      localObject1 = Bitmap.createBitmap(j, k, localConfig);
      try
      {
        a(arrayOfChar, (Bitmap)localObject2);
        arrayOfChar = a;
        a(arrayOfChar, (Bitmap)localObject1);
        int m = ((Bitmap)localObject2).sameAs((Bitmap)localObject1);
        i ^= m;
        return i;
      }
      finally
      {
        ((Bitmap)localObject2).recycle();
        ((Bitmap)localObject1).recycle();
      }
    }
    return i;
  }
  
  public final boolean a(String paramString)
  {
    paramString = paramString.toCharArray();
    int i = paramString.length;
    int j = 0;
    while (j < i)
    {
      char c1 = paramString[j];
      boolean bool2 = Character.isWhitespace(c1);
      if (!bool2)
      {
        boolean bool1 = a(c1);
        if (!bool1) {
          return false;
        }
      }
      j += 1;
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */