package com.truecaller.common.e;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.text.TextUtils;
import java.util.Locale;

public final class f
{
  private static Locale a;
  private static Locale b;
  
  static
  {
    Locale localLocale = Locale.getDefault();
    a = localLocale;
    b = localLocale;
  }
  
  public static Locale a()
  {
    return b;
  }
  
  public static boolean a(Context paramContext, Locale paramLocale)
  {
    if (paramLocale == null) {
      return false;
    }
    b = paramLocale;
    Resources localResources1 = paramContext.getApplicationContext().getResources();
    boolean bool1 = a(localResources1, paramLocale);
    Resources localResources2 = Resources.getSystem();
    boolean bool2 = a(localResources2, paramLocale);
    paramContext = paramContext.getResources();
    boolean bool3 = a(paramContext, paramLocale);
    Locale.setDefault(paramLocale);
    return (bool1) || (bool2) || (bool3);
  }
  
  private static boolean a(Resources paramResources, Locale paramLocale)
  {
    Configuration localConfiguration1 = null;
    if (paramLocale == null) {
      return false;
    }
    Configuration localConfiguration2 = paramResources.getConfiguration();
    Locale localLocale = locale;
    boolean bool = paramLocale.equals(localLocale);
    if (bool) {
      return false;
    }
    localConfiguration1 = new android/content/res/Configuration;
    localConfiguration1.<init>(localConfiguration2);
    locale = paramLocale;
    localConfiguration1.setLayoutDirection(paramLocale);
    paramLocale = paramResources.getDisplayMetrics();
    paramResources.updateConfiguration(localConfiguration1, paramLocale);
    return true;
  }
  
  public static boolean b()
  {
    Locale localLocale = Locale.getDefault();
    int i = TextUtils.getLayoutDirectionFromLocale(localLocale);
    int j = 1;
    if (i == j) {
      return j;
    }
    return false;
  }
  
  public static boolean c()
  {
    Object localObject = b;
    if (localObject != null)
    {
      String str = "fa";
      localObject = ((Locale)localObject).getLanguage();
      boolean bool = TextUtils.equals(str, (CharSequence)localObject);
      if (bool) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.e.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */