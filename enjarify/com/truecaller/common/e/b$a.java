package com.truecaller.common.e;

public final class b$a
{
  public int a;
  int b;
  public int c;
  
  public b$a(int paramInt1, int paramInt2, int paramInt3)
  {
    a = paramInt1;
    b = paramInt2;
    c = paramInt3;
  }
  
  public final String a()
  {
    int i = b;
    switch (i)
    {
    default: 
      return "";
    case 11: 
      return "اسفند";
    case 10: 
      return "بهمن";
    case 9: 
      return "دی";
    case 8: 
      return "آذر";
    case 7: 
      return "آبان";
    case 6: 
      return "مهر";
    case 5: 
      return "شهريور";
    case 4: 
      return "مرداد";
    case 3: 
      return "تير";
    case 2: 
      return "خرداد";
    case 1: 
      return "ارديبهشت";
    }
    return "فروردين";
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append("/");
    i = b;
    localStringBuilder.append(i);
    localStringBuilder.append("/");
    i = c;
    localStringBuilder.append(i);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.e.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */