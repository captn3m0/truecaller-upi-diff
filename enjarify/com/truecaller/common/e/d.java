package com.truecaller.common.e;

import c.a.m;
import java.util.List;

public final class d
{
  public static final d a;
  private static final c b;
  private static final String c = "zz";
  private static final String[][] d;
  private static final String[] e;
  private static final String[] f;
  private static final List g;
  private static final List h;
  
  static
  {
    Object localObject1 = new com/truecaller/common/e/d;
    ((d)localObject1).<init>();
    a = (d)localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("English", "en", "GB");
    b = (c)localObject1;
    c = "zz";
    int i = 1;
    String[][] arrayOfString = new String[i][];
    String[] tmp46_43 = new String[4];
    String[] tmp47_46 = tmp46_43;
    String[] tmp47_46 = tmp46_43;
    tmp47_46[0] = "lenovo";
    tmp47_46[1] = "pa";
    tmp47_46[2] = "gu";
    String[] tmp60_47 = tmp47_46;
    tmp60_47[3] = "si";
    Object localObject2 = tmp60_47;
    arrayOfString[0] = localObject2;
    d = (String[][])arrayOfString;
    e = new String[] { "zh_CN", "zh_TW" };
    f = new String[] { "ko" };
    int j = 11;
    localObject2 = new c[j];
    c localc1 = b;
    localObject2[0] = localc1;
    localc1 = new com/truecaller/common/e/c;
    localc1.<init>("العربية", "ar", "SA");
    localObject2[i] = localc1;
    localc1 = new com/truecaller/common/e/c;
    localc1.<init>("Български", "bg", "BG");
    int k = 2;
    localObject2[k] = localc1;
    localc1 = new com/truecaller/common/e/c;
    localc1.<init>("简中", "zh_CN", "CN");
    int m = 3;
    localObject2[m] = localc1;
    localc1 = new com/truecaller/common/e/c;
    localc1.<init>("繁中", "zh_TW", "CN");
    int n = 4;
    localObject2[n] = localc1;
    localc1 = new com/truecaller/common/e/c;
    localc1.<init>("ελληνικά", "el", "GR");
    int i1 = 5;
    localObject2[i1] = localc1;
    localc1 = new com/truecaller/common/e/c;
    localc1.<init>("עברית", "iw", "IL");
    int i2 = 6;
    localObject2[i2] = localc1;
    localc1 = new com/truecaller/common/e/c;
    localc1.<init>("한국어", "ko", "KR");
    int i3 = 7;
    localObject2[i3] = localc1;
    localc1 = new com/truecaller/common/e/c;
    localc1.<init>("فارسی", "fa", "IR");
    int i4 = 8;
    localObject2[i4] = localc1;
    localc1 = new com/truecaller/common/e/c;
    localc1.<init>("Русский", "ru", "RU");
    int i5 = 9;
    localObject2[i5] = localc1;
    localc1 = new com/truecaller/common/e/c;
    localc1.<init>("Українська", "uk", "UA");
    int i6 = 10;
    localObject2[i6] = localc1;
    g = m.b((Object[])localObject2);
    localObject2 = new c[47];
    localc1 = b;
    localObject2[0] = localc1;
    c localc2 = new com/truecaller/common/e/c;
    localc2.<init>("العربية", "ar", "SA");
    localObject2[i] = localc2;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("বাংলা", "bn", "IN");
    localObject2[k] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Български", "bg", "BG");
    localObject2[m] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("简中", "zh_CN", "CN");
    localObject2[n] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("繁中", "zh_TW", "CN");
    localObject2[i1] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Čeština", "cs", "CZ");
    localObject2[i2] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Dansk", "da", "DK");
    localObject2[i3] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Deutsch", "de", "DE");
    localObject2[i4] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("ગુજરાતી", "gu", "IN");
    localObject2[i5] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Español", "es", "ES");
    localObject2[i6] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Español (Latinoamericano)", "es", "MX");
    localObject2[j] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Suomi", "fi", "FI");
    localObject2[12] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Français", "fr", "FR");
    localObject2[13] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("ελληνικά", "el", "GR");
    localObject2[14] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("עברית", "iw", "IL");
    localObject2[15] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("हिंदी", "hi", "IN");
    localObject2[16] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Hrvatski", "hr", "HR");
    localObject2[17] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Indonesia", "in", "ID");
    localObject2[18] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Italiano", "it", "IT");
    localObject2[19] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("日本語", "ja", "JP");
    localObject2[20] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("ಕನ್ನಡ", "kn", "IN");
    localObject2[21] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Kiswahili", "sw", "KE");
    localObject2[22] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("한국어", "ko", "KR");
    localObject2[23] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("मराठी", "mr", "IN");
    localObject2[24] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Magyar", "hu", "HU");
    localObject2[25] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Melayu", "ms", "MY");
    localObject2[26] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("മലയാളം", "ml", "IN");
    localObject2[27] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Nederlands", "nl", "NL");
    localObject2[28] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("नेपाली", "ne", "NP");
    localObject2[29] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Norsk", "nb", "NO");
    localObject2[30] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("فارسی", "fa", "IR");
    localObject2[31] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Polski", "pl", "PL");
    localObject2[32] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Português (Brasil)", "pt", "BR");
    localObject2[33] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("ਪੰਜਾਬੀ", "pa", "IN");
    localObject2[34] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Română", "ro", "RO");
    localObject2[35] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Русский", "ru", "RU");
    localObject2[36] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("සිංහල", "si", "LK");
    localObject2[37] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Svenska", "sv", "SE");
    localObject2[38] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Tagalog", "tl", "PH");
    localObject2[39] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("தமிழ்", "ta", "IN");
    localObject2[40] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("తెలుగు", "te", "IN");
    localObject2[41] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("ภาษาไทย", "th", "TH");
    localObject2[42] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Türkçe", "tr", "TR");
    localObject2[43] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("Українська", "uk", "UA");
    localObject2[44] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("اردو", "ur", "PK");
    localObject2[45] = localObject1;
    localObject1 = new com/truecaller/common/e/c;
    ((c)localObject1).<init>("tiếng Việt", "vi", "VN");
    localObject2[46] = localObject1;
    h = m.b((Object[])localObject2);
  }
  
  public static c a()
  {
    return b;
  }
  
  public static String b()
  {
    return c;
  }
  
  public static String[][] c()
  {
    return d;
  }
  
  public static String[] d()
  {
    return e;
  }
  
  public static String[] e()
  {
    return f;
  }
  
  public static List f()
  {
    return g;
  }
  
  public static List g()
  {
    return h;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.e.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */