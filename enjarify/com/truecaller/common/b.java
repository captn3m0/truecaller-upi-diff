package com.truecaller.common;

import android.content.Context;
import com.truecaller.common.account.i;
import com.truecaller.common.account.k;
import com.truecaller.common.account.m;
import com.truecaller.common.account.q;
import com.truecaller.common.account.v;
import com.truecaller.common.b.a.a;
import com.truecaller.common.edge.EdgeLocationsWorker;
import com.truecaller.common.enhancedsearch.EnhancedSearchStateWorker;
import com.truecaller.common.h.ac;
import com.truecaller.common.h.ae;
import com.truecaller.common.h.aj;
import com.truecaller.common.h.an;
import com.truecaller.common.h.ar;
import com.truecaller.common.h.as;
import com.truecaller.common.h.at;
import com.truecaller.common.h.s;
import com.truecaller.common.h.w;
import com.truecaller.common.h.x;
import com.truecaller.common.h.y;
import com.truecaller.common.h.z;
import com.truecaller.common.tag.sync.AvailableTagsDownloadWorker;
import com.truecaller.common.tag.sync.TagKeywordsDownloadWorker;
import com.truecaller.common.tag.sync.TagsUploadWorker;
import com.truecaller.content.aa;
import com.truecaller.multisim.h;
import dagger.a.g;
import javax.inject.Provider;

public final class b
  implements a
{
  private Provider A;
  private Provider B;
  private Provider C;
  private Provider D;
  private Provider E;
  private Provider F;
  private Provider G;
  private Provider H;
  private Provider I;
  private Provider J;
  private Provider K;
  private Provider L;
  private Provider M;
  private Provider N;
  private Provider O;
  private Provider P;
  private Provider Q;
  private Provider R;
  private Provider S;
  private final a.a b;
  private final com.truecaller.analytics.d c;
  private final com.truecaller.utils.t d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  private Provider o;
  private Provider p;
  private Provider q;
  private Provider r;
  private Provider s;
  private Provider t;
  private Provider u;
  private Provider v;
  private Provider w;
  private Provider x;
  private Provider y;
  private Provider z;
  
  private b(a.a parama, com.truecaller.common.g.c paramc, com.truecaller.common.edge.c paramc1, x paramx, com.truecaller.content.a parama1, com.truecaller.common.d.a parama2, com.truecaller.utils.t paramt, com.truecaller.analytics.d paramd)
  {
    b = parama;
    c = paramd;
    d = paramt;
    Provider localProvider1 = dagger.a.c.a(com.truecaller.common.g.e.a(paramc));
    e = localProvider1;
    localProvider1 = e;
    paramc = dagger.a.c.a(com.truecaller.common.g.d.a(paramc, localProvider1));
    f = paramc;
    paramc = new com/truecaller/common/b$c;
    paramc.<init>(paramt);
    g = paramc;
    paramc = new dagger/a/b;
    paramc.<init>();
    h = paramc;
    paramc = f;
    localProvider1 = h;
    paramc = ae.a(paramc, localProvider1);
    i = paramc;
    paramc = dagger.a.c.a(i);
    j = paramc;
    paramc = f;
    localProvider1 = j;
    paramc = dagger.a.c.a(com.truecaller.common.b.c.a(parama, paramc, localProvider1));
    k = paramc;
    paramc = com.truecaller.common.b.b.a(parama);
    l = paramc;
    paramc = com.truecaller.common.account.b.a(l);
    m = paramc;
    paramc = com.truecaller.common.account.d.a(l);
    n = paramc;
    paramc = com.truecaller.common.account.e.a(l);
    o = paramc;
    paramc = com.truecaller.common.account.a.a(l);
    p = paramc;
    paramc = com.truecaller.common.account.f.a(l);
    q = paramc;
    localProvider1 = m;
    Provider localProvider2 = n;
    Provider localProvider3 = o;
    Provider localProvider4 = p;
    Provider localProvider5 = q;
    Provider localProvider6 = f;
    paramc = q.a(localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6);
    r = paramc;
    paramc = new com/truecaller/common/b$b;
    paramc.<init>(paramd);
    s = paramc;
    paramc = p;
    paramd = n;
    paramc = com.truecaller.common.account.a.c.a(paramc, paramd);
    t = paramc;
    localProvider1 = f;
    localProvider2 = g;
    localProvider3 = k;
    localProvider4 = r;
    localProvider5 = s;
    localProvider6 = t;
    i locali = i.a();
    paramc = com.truecaller.common.account.t.a(localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6, locali);
    u = paramc;
    paramc = h;
    paramd = dagger.a.c.a(u);
    dagger.a.b.a(paramc, paramd);
    paramc = com.truecaller.common.edge.e.a(l);
    v = paramc;
    paramc = h;
    paramd = f;
    localProvider1 = v;
    localProvider2 = l;
    paramc = dagger.a.c.a(com.truecaller.common.edge.d.a(paramc1, paramc, paramd, localProvider1, localProvider2));
    w = paramc;
    paramc = com.truecaller.common.profile.d.a(l);
    x = paramc;
    paramc = dagger.a.c.a(x);
    y = paramc;
    paramc = l;
    paramc = dagger.a.c.a(y.a(paramx, paramc));
    z = paramc;
    paramc = dagger.a.c.a(z.a(paramx));
    A = paramc;
    paramc = new com/truecaller/common/b$e;
    paramc.<init>(paramt);
    B = paramc;
    paramc = l;
    paramc1 = B;
    parama = dagger.a.c.a(com.truecaller.common.b.d.a(parama, paramc, paramc1));
    C = parama;
    parama = A;
    paramc = z;
    paramc1 = h;
    paramx = C;
    parama = w.a(parama, paramc, paramc1, paramx);
    D = parama;
    parama = dagger.a.c.a(D);
    E = parama;
    parama = dagger.a.c.a(com.truecaller.common.account.c.a(l));
    F = parama;
    parama = F;
    paramc = i.a();
    parama = m.a(parama, paramc);
    G = parama;
    parama = dagger.a.c.a(G);
    H = parama;
    parama = new com/truecaller/common/b$d;
    parama.<init>(paramt);
    I = parama;
    parama = s;
    paramc = I;
    parama = com.truecaller.common.a.c.a(parama, paramc);
    J = parama;
    parama = dagger.a.c.a(J);
    K = parama;
    parama = l;
    paramc = K;
    parama = dagger.a.c.a(at.a(parama, paramc));
    L = parama;
    parama = dagger.a.c.a(com.truecaller.common.d.b.a(parama2));
    M = parama;
    parama = new com/truecaller/common/b$f;
    parama.<init>(paramt);
    N = parama;
    parama = l;
    paramc = E;
    paramc1 = M;
    paramx = N;
    parama = dagger.a.c.a(com.truecaller.content.b.a(parama1, parama, paramc, paramc1, paramx));
    O = parama;
    parama = dagger.a.c.a(com.truecaller.common.d.e.a(parama2));
    P = parama;
    parama = dagger.a.c.a(com.truecaller.common.d.d.a(parama2));
    Q = parama;
    parama = dagger.a.c.a(com.truecaller.common.d.c.a(parama2));
    R = parama;
    parama = dagger.a.c.a(as.a());
    S = parama;
  }
  
  public static b.a x()
  {
    b.a locala = new com/truecaller/common/b$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final void a(com.truecaller.common.background.d paramd)
  {
    com.truecaller.analytics.b localb = (com.truecaller.analytics.b)g.a(c.c(), "Cannot return null from a non-@Nullable component method");
    b = localb;
  }
  
  public final void a(EdgeLocationsWorker paramEdgeLocationsWorker)
  {
    Object localObject = (com.truecaller.common.edge.a)w.get();
    b = ((com.truecaller.common.edge.a)localObject);
    localObject = (com.truecaller.common.g.a)f.get();
    c = ((com.truecaller.common.g.a)localObject);
    localObject = (com.truecaller.analytics.b)g.a(c.c(), "Cannot return null from a non-@Nullable component method");
    d = ((com.truecaller.analytics.b)localObject);
    localObject = (com.truecaller.common.account.r)h.get();
    e = ((com.truecaller.common.account.r)localObject);
  }
  
  public final void a(EnhancedSearchStateWorker paramEnhancedSearchStateWorker)
  {
    Object localObject = (com.truecaller.common.account.r)h.get();
    b = ((com.truecaller.common.account.r)localObject);
    localObject = (com.truecaller.common.g.a)f.get();
    c = ((com.truecaller.common.g.a)localObject);
  }
  
  public final void a(AvailableTagsDownloadWorker paramAvailableTagsDownloadWorker)
  {
    Object localObject = (com.truecaller.analytics.b)g.a(c.c(), "Cannot return null from a non-@Nullable component method");
    b = ((com.truecaller.analytics.b)localObject);
    localObject = (com.truecaller.common.account.r)h.get();
    c = ((com.truecaller.common.account.r)localObject);
  }
  
  public final void a(TagKeywordsDownloadWorker paramTagKeywordsDownloadWorker)
  {
    Object localObject = (com.truecaller.analytics.b)g.a(c.c(), "Cannot return null from a non-@Nullable component method");
    b = ((com.truecaller.analytics.b)localObject);
    localObject = (com.truecaller.common.account.r)h.get();
    c = ((com.truecaller.common.account.r)localObject);
    localObject = (ac)j.get();
    d = ((ac)localObject);
  }
  
  public final void a(TagsUploadWorker paramTagsUploadWorker)
  {
    com.truecaller.common.account.r localr = (com.truecaller.common.account.r)h.get();
    b = localr;
  }
  
  public final void a(com.truecaller.content.c.u paramu)
  {
    aa localaa = (aa)O.get();
    a = localaa;
  }
  
  public final boolean a()
  {
    return com.truecaller.common.b.a.a(b.a);
  }
  
  public final Context b()
  {
    return com.truecaller.common.b.b.b(b);
  }
  
  public final com.truecaller.common.g.a c()
  {
    return (com.truecaller.common.g.a)f.get();
  }
  
  public final an d()
  {
    an localan = new com/truecaller/common/h/an;
    com.truecaller.utils.a locala = (com.truecaller.utils.a)g.a(d.d(), "Cannot return null from a non-@Nullable component method");
    localan.<init>(locala);
    return localan;
  }
  
  public final com.truecaller.common.edge.a e()
  {
    return (com.truecaller.common.edge.a)w.get();
  }
  
  public final com.truecaller.common.profile.b f()
  {
    return (com.truecaller.common.profile.b)y.get();
  }
  
  public final h g()
  {
    return (h)z.get();
  }
  
  public final com.truecaller.common.h.u h()
  {
    return (com.truecaller.common.h.u)E.get();
  }
  
  public final com.truecaller.common.h.r i()
  {
    s locals = new com/truecaller/common/h/s;
    ac localac = (ac)j.get();
    dagger.a locala = dagger.a.c.b(A);
    locals.<init>(localac, locala);
    return locals;
  }
  
  public final com.truecaller.common.network.account.b j()
  {
    com.truecaller.common.network.account.c localc = new com/truecaller/common/network/account/c;
    Context localContext = com.truecaller.common.b.b.b(b);
    com.truecaller.common.h.c localc1 = v.a();
    h localh = (h)z.get();
    localc.<init>(localContext, localc1, localh);
    return localc;
  }
  
  public final com.truecaller.common.account.r k()
  {
    return (com.truecaller.common.account.r)h.get();
  }
  
  public final k l()
  {
    return (k)H.get();
  }
  
  public final com.truecaller.common.network.c m()
  {
    return (com.truecaller.common.network.c)k.get();
  }
  
  public final ac n()
  {
    return (ac)j.get();
  }
  
  public final com.truecaller.common.network.optout.a o()
  {
    com.truecaller.common.network.optout.b localb = new com/truecaller/common/network/optout/b;
    localb.<init>();
    return localb;
  }
  
  public final com.truecaller.common.h.d p()
  {
    return (com.truecaller.common.h.d)L.get();
  }
  
  public final aj q()
  {
    return (aj)C.get();
  }
  
  public final c.d.f r()
  {
    return (c.d.f)P.get();
  }
  
  public final c.d.f s()
  {
    return (c.d.f)M.get();
  }
  
  public final c.d.f t()
  {
    return (c.d.f)Q.get();
  }
  
  public final c.d.f u()
  {
    return (c.d.f)R.get();
  }
  
  public final com.truecaller.featuretoggles.e v()
  {
    return ar.a(com.truecaller.common.b.b.b(b));
  }
  
  public final com.truecaller.common.e.e w()
  {
    return (com.truecaller.common.e.e)S.get();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.common.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */