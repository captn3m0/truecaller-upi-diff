package com.truecaller.messaging.categorizer;

import android.content.Context;
import android.os.Bundle;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.f;
import com.truecaller.bp;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.background.e.a;
import com.truecaller.featuretoggles.b;
import com.truecaller.messaging.data.t;
import java.util.concurrent.TimeUnit;

public final class UnclassifiedMessagesTask
  extends PersistentBackgroundTask
{
  public com.truecaller.featuretoggles.e a;
  
  public UnclassifiedMessagesTask()
  {
    TrueApp localTrueApp = TrueApp.y();
    k.a(localTrueApp, "TrueApp.getApp()");
    localTrueApp.a().a(this);
  }
  
  public final int a()
  {
    return 10032;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    k.b(paramContext, "context");
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    ((t)paramContext.a().p().a()).a();
    return PersistentBackgroundTask.RunResult.Success;
  }
  
  public final boolean a(Context paramContext)
  {
    String str = "context";
    k.b(paramContext, str);
    paramContext = a;
    if (paramContext == null)
    {
      str = "featuresRegistry";
      k.a(str);
    }
    return paramContext.w().a();
  }
  
  public final com.truecaller.common.background.e b()
  {
    Object localObject = new com/truecaller/common/background/e$a;
    ((e.a)localObject).<init>(1);
    TimeUnit localTimeUnit = TimeUnit.HOURS;
    localObject = ((e.a)localObject).a(12, localTimeUnit);
    localTimeUnit = TimeUnit.HOURS;
    localObject = ((e.a)localObject).b(2, localTimeUnit).b();
    k.a(localObject, "TaskConfiguration.Builde…URS)\n            .build()");
    return (com.truecaller.common.background.e)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.categorizer.UnclassifiedMessagesTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */