package com.truecaller.messaging.categorizer;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TextEntity;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.transport.i;
import com.truecaller.messaging.transport.sms.SmsTransportInfo.a;
import java.util.ArrayList;
import java.util.Date;
import kotlinx.coroutines.ag;

final class d$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag l;
  
  d$a(int paramInt1, String paramString1, String paramString2, String paramString3, long paramLong1, long paramLong2, int paramInt2, c.d.c paramc, d paramd, ArrayList paramArrayList, com.truecaller.insights.database.c paramc1)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/messaging/categorizer/d$a;
    int m = b;
    String str1 = c;
    String str2 = d;
    String str3 = e;
    long l1 = f;
    long l2 = g;
    int n = h;
    d locald = i;
    ArrayList localArrayList = j;
    com.truecaller.insights.database.c localc = k;
    Object localObject = locala;
    locala.<init>(m, str1, str2, str3, l1, l2, n, paramc, locald, localArrayList, localc);
    localObject = paramObject;
    localObject = (ag)paramObject;
    l = ((ag)localObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int m = a;
    if (m == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = new com/truecaller/messaging/data/types/Message$a;
        ((Message.a)paramObject).<init>();
        int n = b;
        paramObject = ((Message.a)paramObject).a(n);
        Object localObject2 = c;
        localObject1 = TextEntity.a("text/plain", (String)localObject2);
        paramObject = ((Message.a)paramObject).a((Entity)localObject1);
        localObject1 = d;
        paramObject = ((Message.a)paramObject).a((String)localObject1);
        localObject1 = i.c;
        localObject2 = e;
        localObject1 = ((i)localObject1).a((String)localObject2);
        paramObject = ((Message.a)paramObject).a((Participant)localObject1);
        n = 0;
        localObject2 = new com/truecaller/messaging/transport/sms/SmsTransportInfo$a;
        ((SmsTransportInfo.a)localObject2).<init>();
        localObject2 = (TransportInfo)((SmsTransportInfo.a)localObject2).a();
        paramObject = ((Message.a)paramObject).a(0, (TransportInfo)localObject2).b();
        c.g.b.k.a(paramObject, "Message.Builder()\n      …                 .build()");
        localObject1 = (com.truecaller.insights.models.d)com.truecaller.insights.models.d.a.a;
        localObject2 = i.a.Y();
        boolean bool2 = ((com.truecaller.featuretoggles.b)localObject2).a();
        if (bool2)
        {
          localObject1 = i.d;
          localObject2 = k;
          com.truecaller.insights.models.c localc = new com/truecaller/insights/models/c;
          long l1 = f;
          String str1 = e;
          String str2 = ((Message)paramObject).j();
          c.g.b.k.a(str2, "message.buildMessageText()");
          Date localDate = new java/util/Date;
          long l2 = g;
          localDate.<init>(l2);
          localObject3 = localc;
          localc.<init>(l1, str1, str2, localDate);
          localObject1 = (com.truecaller.insights.models.d)((com.truecaller.insights.core.d.a)localObject1).a((com.truecaller.insights.database.c)localObject2, localc);
        }
        localObject2 = i;
        long l3 = f;
        int i1 = h;
        Object localObject3 = ContentProviderOperation.newUpdate(TruecallerContract.aa.a(l3));
        int i2 = 1;
        if (i1 != i2)
        {
          paramObject = new com/truecaller/messaging/categorizer/a;
          n = 2;
          ((a)paramObject).<init>(n, n);
        }
        else
        {
          localObject2 = b;
          paramObject = ((b)localObject2).a((Message)paramObject, (com.truecaller.insights.models.d)localObject1);
        }
        localObject2 = Integer.valueOf(a);
        ((ContentProviderOperation.Builder)localObject3).withValue("category", localObject2);
        paramObject = Integer.valueOf(b);
        ((ContentProviderOperation.Builder)localObject3).withValue("classification", paramObject);
        paramObject = ((ContentProviderOperation.Builder)localObject3).build();
        c.g.b.k.a(paramObject, "ContentProviderOperation…cation)\n        }.build()");
        return paramObject;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.categorizer.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */