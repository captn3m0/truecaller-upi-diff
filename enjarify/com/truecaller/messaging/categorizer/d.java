package com.truecaller.messaging.categorizer;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import c.g.a.m;
import c.x;
import com.truecaller.content.TruecallerContract.ap;
import com.truecaller.messaging.transport.i;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Set;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class d
{
  final com.truecaller.featuretoggles.e a;
  final b b;
  final i c;
  final com.truecaller.insights.core.d.a d;
  final com.truecaller.insights.core.c.a e;
  private final ContentResolver f;
  private final com.truecaller.insights.core.b.c g;
  private final c.d.f h;
  
  public d(com.truecaller.featuretoggles.e parame, ContentResolver paramContentResolver, b paramb, i parami, com.truecaller.insights.core.d.a parama, com.truecaller.insights.core.b.c paramc, com.truecaller.insights.core.c.a parama1, c.d.f paramf)
  {
    a = parame;
    f = paramContentResolver;
    b = paramb;
    c = parami;
    d = parama;
    g = paramc;
    e = parama1;
    h = paramf;
  }
  
  public final ArrayList a(Set paramSet)
  {
    Object localObject1 = this;
    Object localObject2 = paramSet;
    c.g.b.k.b(paramSet, "ids");
    Object localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    Object localObject11 = new java/util/ArrayList;
    ((ArrayList)localObject11).<init>();
    com.truecaller.insights.database.c localc = g.a();
    Object localObject12 = f;
    Object localObject13 = TruecallerContract.ap.a(paramSet);
    localObject2 = localObject12;
    localObject12 = localObject13;
    localObject13 = null;
    Object localObject14 = null;
    Object localObject15 = null;
    localObject2 = ((ContentResolver)localObject2).query((Uri)localObject12, null, null, null, null);
    if (localObject2 != null)
    {
      Object localObject16 = localObject2;
      localObject16 = (Closeable)localObject2;
      Object localObject17 = null;
      Object localObject18 = localObject16;
      try
      {
        localObject18 = (Cursor)localObject16;
        int i;
        for (;;)
        {
          boolean bool = ((Cursor)localObject18).moveToNext();
          if (bool)
          {
            localObject2 = "message_status";
            try
            {
              i = com.truecaller.utils.extensions.k.b((Cursor)localObject18, (String)localObject2);
              localObject2 = "message_id";
              long l1 = com.truecaller.utils.extensions.k.c((Cursor)localObject18, (String)localObject2);
              localObject2 = "conversation_split_criteria";
              int j = com.truecaller.utils.extensions.k.b((Cursor)localObject18, (String)localObject2);
              localObject2 = "message_content";
              localObject2 = com.truecaller.utils.extensions.k.a((Cursor)localObject18, (String)localObject2);
              if (localObject2 == null) {
                localObject2 = "";
              }
              localObject13 = localObject2;
              localObject2 = "message_sender_raw_address";
              localObject2 = com.truecaller.utils.extensions.k.a((Cursor)localObject18, (String)localObject2);
              if (localObject2 == null) {
                localObject2 = "";
              }
              localObject15 = localObject2;
              localObject2 = "message_sim_token";
              localObject2 = com.truecaller.utils.extensions.k.a((Cursor)localObject18, (String)localObject2);
              if (localObject2 == null) {
                localObject2 = "";
              }
              localObject14 = localObject2;
              localObject2 = "message_date";
              long l2 = com.truecaller.utils.extensions.k.c((Cursor)localObject18, (String)localObject2);
              localObject2 = bg.a;
              localObject2 = (ag)localObject2;
              paramSet = (Set)localObject18;
              localObject18 = h;
              d.a locala = new com/truecaller/messaging/categorizer/d$a;
              Object localObject19 = localObject2;
              localObject2 = locala;
              localObject20 = localObject3;
              localObject3 = localObject18;
              localObject18 = null;
              localObject17 = this;
              localObject21 = localObject16;
              localObject16 = localObject11;
              localObject1 = localObject11;
              localObject11 = localc;
              try
              {
                locala.<init>(i, (String)localObject13, (String)localObject14, (String)localObject15, l1, l2, j, null, this, (ArrayList)localObject16, localc);
                localObject2 = (m)locala;
                i = 2;
                localObject13 = localObject19;
                localObject3 = kotlinx.coroutines.e.a((ag)localObject19, (c.d.f)localObject3, (m)localObject2, i);
                ((ArrayList)localObject16).add(localObject3);
                localObject11 = localObject16;
                localObject18 = paramSet;
                localObject3 = localObject20;
                localObject16 = localObject21;
                localObject17 = null;
                localObject1 = this;
              }
              finally {}
            }
            finally
            {
              localObject17 = localObject5;
              localObject2 = localObject16;
              break label450;
            }
          }
        }
        Object localObject21 = localObject16;
        localObject1 = localObject11;
        try
        {
          ((Cursor)localObject18).close();
          localObject6 = x.a;
          localObject2 = localObject16;
          i = 0;
          localObject12 = null;
          c.f.b.a((Closeable)localObject16, null);
        }
        finally
        {
          Object localObject6;
          localObject2 = localObject21;
        }
      }
      finally
      {
        localObject2 = localObject16;
      }
      try
      {
        label450:
        throw ((Throwable)localObject17);
      }
      finally
      {
        localObject12 = localObject17;
        c.f.b.a((Closeable)localObject2, (Throwable)localObject17);
      }
    }
    Object localObject20 = localObject9;
    Object localObject10 = localObject11;
    c.d.f localf = h;
    Object localObject22 = new com/truecaller/messaging/categorizer/d$b;
    localObject2 = localObject22;
    localObject12 = this;
    localObject13 = localObject10;
    localObject14 = localObject20;
    localObject15 = localc;
    ((d.b)localObject22).<init>(this, (ArrayList)localObject10, (ArrayList)localObject20, localc, null);
    localObject22 = (m)localObject22;
    kotlinx.coroutines.f.a(localf, (m)localObject22);
    return (ArrayList)localObject20;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.categorizer.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */