package com.truecaller.messaging.categorizer;

import c.g.a.m;
import c.l.g;
import c.o.b;
import c.x;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;

final class d$b$1
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  Object c;
  Object d;
  Object e;
  int f;
  private ag h;
  
  d$b$1(d.b paramb, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/messaging/categorizer/d$b$1;
    d.b localb = g;
    local1.<init>(localb, paramc);
    paramObject = (ag)paramObject;
    h = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = f;
    Object localObject4;
    Object localObject5;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = this;
        break label347;
      }
      throw a;
    case 1: 
      localObject2 = (ArrayList)e;
      localObject3 = (Iterator)b;
      localObject4 = (Iterable)a;
      boolean bool2 = paramObject instanceof o.b;
      if (!bool2)
      {
        localObject5 = localObject1;
        localObject1 = this;
      }
      else
      {
        throw a;
      }
      break;
    case 0: 
      j = paramObject instanceof o.b;
      if (j != 0) {
        break label438;
      }
      paramObject = (Iterable)g.c;
      localObject2 = ((Iterable)paramObject).iterator();
      localObject4 = paramObject;
      localObject3 = localObject2;
      paramObject = this;
    }
    for (;;)
    {
      j = ((Iterator)localObject3).hasNext();
      if (j == 0) {
        break;
      }
      localObject2 = ((Iterator)localObject3).next();
      localObject5 = localObject2;
      localObject5 = (ao)localObject2;
      ArrayList localArrayList = g.d;
      a = localObject4;
      b = localObject3;
      c = localObject2;
      d = localObject5;
      e = localArrayList;
      j = 1;
      f = j;
      localObject2 = ((ao)localObject5).a((c.d.c)paramObject);
      if (localObject2 == localObject1) {
        return localObject1;
      }
      localObject5 = localObject1;
      localObject1 = paramObject;
      paramObject = localObject2;
      localObject2 = localArrayList;
      ((ArrayList)localObject2).add(paramObject);
      paramObject = localObject1;
      localObject1 = localObject5;
    }
    Object localObject2 = g.b.a.Z();
    int j = ((b)localObject2).a();
    if (j != 0)
    {
      localObject2 = g.e;
      int k = 2;
      f = k;
      localObject2 = ((com.truecaller.insights.database.c)localObject2).a((c.d.c)paramObject);
      if (localObject2 == localObject1) {
        return localObject1;
      }
    }
    label347:
    g.e.a.clear();
    localObject1 = g.b.a;
    localObject2 = A;
    Object localObject3 = e.a;
    int m = 80;
    localObject3 = localObject3[m];
    localObject1 = ((e.a)localObject2).a((e)localObject1, (g)localObject3);
    boolean bool1 = ((b)localObject1).a();
    if (bool1)
    {
      paramObject = g.b.e;
      ((com.truecaller.insights.core.c.a)paramObject).a();
    }
    return x.a;
    label438:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.categorizer.d.b.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */