package com.truecaller.messaging.categorizer;

import android.text.TextUtils;
import c.a.m;
import c.g.b.k;
import com.truecaller.analytics.ae;
import com.truecaller.androidactors.f;
import com.truecaller.common.h.ab;
import com.truecaller.insights.models.d.a;
import com.truecaller.insights.models.d.b;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.h;
import com.truecaller.payments.j;
import com.truecaller.tracking.events.ac;
import com.truecaller.tracking.events.ac.a;
import com.truecaller.tracking.events.al;
import com.truecaller.tracking.events.al.a;
import com.truecaller.tracking.events.ax;
import com.truecaller.tracking.events.ax.a;
import java.util.List;

public final class c
  implements b
{
  private final com.truecaller.featuretoggles.e a;
  private final com.truecaller.insights.core.smscategorizer.d b;
  private final j c;
  private final com.truecaller.insights.core.d.a d;
  private final h e;
  private final f f;
  
  public c(com.truecaller.featuretoggles.e parame, com.truecaller.insights.core.smscategorizer.d paramd, j paramj, com.truecaller.insights.core.d.a parama, h paramh, f paramf)
  {
    a = parame;
    b = paramd;
    c = paramj;
    d = parama;
    e = paramh;
    f = paramf;
  }
  
  private static ax a(Participant paramParticipant)
  {
    Object localObject1 = al.b();
    boolean bool1 = paramParticipant.d();
    localObject1 = ((al.a)localObject1).a(bool1);
    Object localObject2 = (CharSequence)paramParticipant.m;
    bool1 = TextUtils.isEmpty((CharSequence)localObject2);
    int n = 1;
    bool1 ^= n;
    localObject1 = ((al.a)localObject1).b(bool1);
    int i = paramParticipant.j;
    if (i == n)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject2 = null;
    }
    localObject2 = Boolean.valueOf(i);
    localObject1 = ((al.a)localObject1).a((Boolean)localObject2);
    localObject2 = Boolean.valueOf(paramParticipant.k);
    localObject1 = ((al.a)localObject1).b((Boolean)localObject2);
    int j = paramParticipant.j;
    int i1 = 2;
    if (j == i1)
    {
      j = 1;
    }
    else
    {
      j = 0;
      localObject2 = null;
    }
    localObject2 = Boolean.valueOf(j);
    localObject1 = ((al.a)localObject1).c((Boolean)localObject2);
    int k = q;
    int i2 = 10;
    if (k >= i2)
    {
      k = 1;
    }
    else
    {
      k = 0;
      localObject2 = null;
    }
    localObject2 = Boolean.valueOf(k);
    localObject1 = ((al.a)localObject1).d((Boolean)localObject2);
    int m = o & 0x40;
    boolean bool2;
    if (m == 0)
    {
      bool2 = false;
      localCharSequence = null;
    }
    localObject2 = Boolean.valueOf(bool2);
    localObject1 = ((al.a)localObject1).e((Boolean)localObject2);
    m = q;
    localObject2 = Integer.valueOf(Math.max(0, m));
    localObject1 = ((al.a)localObject1).a((Integer)localObject2).a();
    localObject2 = ax.b();
    CharSequence localCharSequence = (CharSequence)paramParticipant.j();
    localObject2 = ((ax.a)localObject2).a(localCharSequence);
    paramParticipant = (CharSequence)paramParticipant.k();
    paramParticipant = ((ax.a)localObject2).b(paramParticipant).a((al)localObject1).a();
    k.a(paramParticipant, "com.truecaller.tracking.…nfo)\n            .build()");
    return paramParticipant;
  }
  
  public final a a(Message paramMessage, com.truecaller.insights.models.d paramd)
  {
    k.b(paramMessage, "message");
    k.b(paramd, "insightsParseResponse");
    Object localObject1 = a.w();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
    boolean bool3 = false;
    int j = 2;
    if (!bool1)
    {
      paramMessage = new com/truecaller/messaging/categorizer/a;
      paramMessage.<init>(j, 0);
      return paramMessage;
    }
    String str = paramMessage.j();
    localObject1 = "message.buildMessageText()";
    k.a(str, (String)localObject1);
    int i = f;
    int k = 1;
    i &= k;
    if (i != 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject1 = null;
    }
    int m = j;
    Object localObject2;
    if (m != 0)
    {
      m = j;
      n = 4;
      if (m != n)
      {
        m = 0;
        localObject2 = null;
        break label141;
      }
    }
    m = 1;
    label141:
    Object localObject3 = str;
    localObject3 = (CharSequence)str;
    int n = ((CharSequence)localObject3).length();
    if (n == 0)
    {
      n = 1;
    }
    else
    {
      n = 0;
      localObject3 = null;
    }
    if ((n == 0) && (m != 0) && (i == 0))
    {
      localObject1 = e;
      long l1 = ((h)localObject1).a();
      long l2 = 0L;
      boolean bool2 = l1 < l2;
      if (bool2)
      {
        bool2 = true;
      }
      else
      {
        bool2 = false;
        localObject1 = null;
      }
      ac.a locala = ac.b();
      localObject2 = b;
      localObject3 = ab.c(c.e);
      k.a(localObject3, "PhoneNumberUtils.stripAl…e.participant.rawAddress)");
      Object localObject4 = c;
      k.a(localObject4, "message.participant");
      List localList = m.a(a((Participant)localObject4));
      localObject4 = "smsCategorizerCompareEvent";
      k.a(locala, (String)localObject4);
      m = ((com.truecaller.insights.core.smscategorizer.d)localObject2).a((String)localObject3, str, bool2, localList, locala);
      if (m == 0)
      {
        localObject3 = c;
        ((j)localObject3).a(paramMessage);
      }
      int i2 = 3;
      if (m == k)
      {
        localObject2 = new com/truecaller/messaging/categorizer/a;
        ((a)localObject2).<init>(i2, j);
      }
      else
      {
        localObject2 = new com/truecaller/messaging/categorizer/a;
        ((a)localObject2).<init>(j, j);
      }
      boolean bool4 = paramd instanceof d.a;
      if (!bool4)
      {
        paramd = (d.b)paramd;
        int i1 = a;
        if (i1 == i2)
        {
          paramMessage = d;
          localObject3 = paramd;
          localObject3 = (com.truecaller.insights.models.d)paramd;
          boolean bool5 = paramMessage.a((com.truecaller.insights.models.d)localObject3);
          if (bool5)
          {
            localObject2 = new com/truecaller/messaging/categorizer/a;
            ((a)localObject2).<init>(j, j);
            bool3 = true;
          }
        }
        if (bool2)
        {
          paramMessage = d;
          localObject1 = b;
          paramMessage.a((com.truecaller.insights.models.e)localObject1);
          paramMessage = (CharSequence)b.a();
          locala.b(paramMessage);
          locala.b();
          locala.a(bool3);
          paramMessage = (ae)f.a();
          paramd = (org.apache.a.d.d)locala.c();
          paramMessage.a(paramd);
        }
      }
      return (a)localObject2;
    }
    paramMessage = new com/truecaller/messaging/categorizer/a;
    paramMessage.<init>(j, j);
    return paramMessage;
  }
  
  public final void a(Message paramMessage, int paramInt1, int paramInt2)
  {
    k.b(paramMessage, "message");
    com.truecaller.insights.core.smscategorizer.d locald = b;
    String str1 = paramMessage.j();
    String str2 = "message.buildMessageText()";
    k.a(str1, str2);
    int i = 3;
    if (paramInt1 == i) {
      paramInt1 = 1;
    } else {
      paramInt1 = 2;
    }
    paramMessage = c;
    k.a(paramMessage, "message.participant");
    paramMessage = m.a(a(paramMessage));
    locald.a(str1, paramInt1, paramInt2, paramMessage);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.categorizer.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */