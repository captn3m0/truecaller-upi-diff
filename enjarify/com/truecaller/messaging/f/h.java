package com.truecaller.messaging.f;

import android.support.v4.app.ac;
import com.truecaller.analytics.ap;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.k;
import com.truecaller.messaging.d.a;
import com.truecaller.messaging.transport.m;
import dagger.a.g;
import javax.inject.Provider;

public final class h
  implements dagger.a.d
{
  private final e a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  private final Provider g;
  private final Provider h;
  private final Provider i;
  
  public static c a(e parame, f paramf1, k paramk, f paramf2, m paramm, a parama, ap paramap, ac paramac, com.truecaller.messaging.g.d paramd)
  {
    c localc = new com/truecaller/messaging/f/c;
    long l1 = a;
    long l2 = b;
    i locali = paramk.a();
    localc.<init>(l1, l2, paramf1, locali, paramf2, paramm, parama, paramap, paramac, paramd);
    return (c)g.a(localc, "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.f.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */