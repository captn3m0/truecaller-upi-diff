package com.truecaller.messaging.newconversation;

import android.net.Uri;
import c.o.b;
import c.x;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.d;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.util.ba;
import com.truecaller.util.bg;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;

final class NewConversationPresenter$d$a
  extends c.d.b.a.k
  implements c.g.a.m
{
  int a;
  private ag c;
  
  NewConversationPresenter$d$a(NewConversationPresenter.d paramd, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/messaging/newconversation/NewConversationPresenter$d$a;
    NewConversationPresenter.d locald = b;
    locala.<init>(locald, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = NewConversationPresenter.b(b.b);
        int j = 2;
        long l = ((bg)paramObject).a(j);
        paramObject = b.c;
        ba localba = (ba)NewConversationPresenter.c(b.b).a();
        Object localObject1 = d.c;
        paramObject = (Collection)com.truecaller.messaging.c.a((List)paramObject);
        c.g.b.k.b(paramObject, "entities");
        paramObject = (Iterable)paramObject;
        localObject1 = new java/util/ArrayList;
        int k = c.a.m.a((Iterable)paramObject, 10);
        ((ArrayList)localObject1).<init>(k);
        localObject1 = (Collection)localObject1;
        paramObject = ((Iterable)paramObject).iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)paramObject).hasNext();
          if (!bool2) {
            break;
          }
          Object localObject2 = (BinaryEntity)((Iterator)paramObject).next();
          d locald = new com/truecaller/messaging/d;
          Uri localUri = b;
          String str = "it.content";
          c.g.b.k.a(localUri, str);
          localObject2 = j;
          locald.<init>(localUri, (String)localObject2);
          ((Collection)localObject1).add(locald);
        }
        localObject1 = (Collection)localObject1;
        return localba.a((Collection)localObject1, l).d();
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.NewConversationPresenter.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */