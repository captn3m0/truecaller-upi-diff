package com.truecaller.messaging.newconversation;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.o;
import android.support.v7.app.AppCompatActivity;
import c.g.b.k;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;

public final class NewConversationActivity
  extends AppCompatActivity
{
  public static final NewConversationActivity.a a;
  
  static
  {
    NewConversationActivity.a locala = new com/truecaller/messaging/newconversation/NewConversationActivity$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public final void onBackPressed()
  {
    android.support.v4.app.j localj = getSupportFragmentManager();
    String str = "supportFragmentManager";
    k.a(localj, str);
    int i = localj.e();
    if (i > 0)
    {
      getSupportFragmentManager().c();
      return;
    }
    super.onBackPressed();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    ThemeManager.Theme localTheme = ThemeManager.a();
    int i = resId;
    setTheme(i);
    super.onCreate(paramBundle);
    if (paramBundle == null)
    {
      paramBundle = getSupportFragmentManager().a();
      i = 16908290;
      Object localObject = new com/truecaller/messaging/newconversation/j;
      ((j)localObject).<init>();
      localObject = (Fragment)localObject;
      paramBundle = paramBundle.b(i, (Fragment)localObject);
      paramBundle.c();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.NewConversationActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */