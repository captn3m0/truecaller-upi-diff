package com.truecaller.messaging.newconversation;

import c.a.ag;
import c.a.m;
import c.a.y;
import com.truecaller.adapter_delegates.j;
import com.truecaller.common.h.an;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.g;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.util.al;
import com.truecaller.utils.n;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class c
  extends t.a
  implements j
{
  private final String b;
  private final String c;
  private final String d;
  private final String e;
  private final String f;
  private t.h g;
  private com.truecaller.messaging.data.a.k h;
  private List i;
  private List j;
  private Integer k;
  private i l;
  private String m;
  private String n;
  private boolean o;
  private Map p;
  private final al q;
  private final g r;
  private final t.f s;
  private final com.truecaller.messaging.h t;
  private final an u;
  
  public c(al paramal, g paramg, t.f paramf, n paramn, com.truecaller.messaging.h paramh, an paraman)
  {
    q = paramal;
    r = paramg;
    s = paramf;
    t = paramh;
    u = paraman;
    paramg = new Object[0];
    paramg = paramn.a(2131886730, paramg);
    c.g.b.k.a(paramg, "resourceProvider.getStri…sationSectionRecentChats)");
    b = paramg;
    paramg = new Object[0];
    paramg = paramn.a(2131886728, paramg);
    c.g.b.k.a(paramg, "resourceProvider.getStri…rsationSectionOtherChats)");
    c = paramg;
    paramg = new Object[0];
    paramg = paramn.a(2131886729, paramg);
    c.g.b.k.a(paramg, "resourceProvider.getStri…tionSectionOtherContacts)");
    d = paramg;
    paramg = new Object[0];
    paramg = paramn.a(2131886727, paramg);
    c.g.b.k.a(paramg, "resourceProvider.getStri…rsationSectionImContacts)");
    e = paramg;
    paramal = new Object[0];
    paramal = paramn.a(2131886731, paramal);
    c.g.b.k.a(paramal, "resourceProvider.getStri…nSectionSmsConversations)");
    f = paramal;
    paramal = (List)y.a;
    j = paramal;
    m = "";
    n = "";
    paramal = ag.a();
    p = paramal;
  }
  
  private final i a(int paramInt)
  {
    com.truecaller.messaging.data.a.k localk = h;
    if (localk != null)
    {
      localk.moveToPosition(paramInt);
      return localk.a();
    }
    return l;
  }
  
  private final boolean a(i parami)
  {
    Object localObject1 = f;
    int i1 = ((List)localObject1).size();
    int i2 = 1;
    if (i1 == i2)
    {
      parami = ((com.truecaller.data.entity.Number)m.d(f)).a();
      localObject1 = (Iterable)j;
      boolean bool = localObject1 instanceof Collection;
      Object localObject2;
      if (bool)
      {
        localObject2 = localObject1;
        localObject2 = (Collection)localObject1;
        bool = ((Collection)localObject2).isEmpty();
        if (bool) {}
      }
      else
      {
        localObject1 = ((Iterable)localObject1).iterator();
        do
        {
          bool = ((Iterator)localObject1).hasNext();
          if (!bool) {
            break;
          }
          localObject2 = nextf;
          bool = c.g.b.k.a(localObject2, parami);
        } while (!bool);
        return i2;
        return false;
      }
    }
    return false;
  }
  
  public final i a()
  {
    Integer localInteger = k;
    if (localInteger != null)
    {
      int i1 = ((Number)localInteger).intValue();
      return a(i1);
    }
    return null;
  }
  
  public final void a(Contact paramContact)
  {
    c.g.b.k.b(paramContact, "contact");
    a(null, null);
    Object localObject = i.l;
    c.g.b.k.b(paramContact, "contact");
    localObject = new com/truecaller/messaging/newconversation/i;
    List localList1 = m.b(paramContact.t());
    List localList2 = m.b(paramContact.E());
    String str1 = paramContact.v();
    List localList3 = paramContact.A();
    c.g.b.k.a(localList3, "contact.numbers");
    String str2 = paramContact.j();
    ((i)localObject).<init>(null, localList1, localList2, str1, localList3, str2, 0L, 0L, 1, 4, null, null, null);
    l = ((i)localObject);
    m = "";
    n = "";
  }
  
  public final void a(com.truecaller.messaging.data.a.k paramk, List paramList)
  {
    com.truecaller.messaging.data.a.k localk = h;
    if (localk != null) {
      localk.close();
    }
    h = paramk;
    i = paramList;
    k = null;
    l = null;
    m = "";
    n = "";
  }
  
  public final void a(t.h paramh)
  {
    c.g.b.k.b(paramh, "router");
    g = paramh;
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "string");
    a(null, null);
    m = paramString;
    l = null;
    n = "";
  }
  
  public final void a(List paramList)
  {
    c.g.b.k.b(paramList, "participants");
    j = paramList;
  }
  
  public final void a(Map paramMap)
  {
    c.g.b.k.b(paramMap, "participants");
    p = paramMap;
  }
  
  public final void a(boolean paramBoolean)
  {
    o = paramBoolean;
  }
  
  public final boolean a(com.truecaller.adapter_delegates.h paramh)
  {
    c.g.b.k.b(paramh, "event");
    Object localObject = a;
    String str = "ItemEvent.CLICKED";
    boolean bool1 = c.g.b.k.a(localObject, str);
    boolean bool2 = true;
    bool1 ^= bool2;
    if (bool1) {
      return false;
    }
    localObject = s;
    bool1 = localObject instanceof t.f.c;
    label131:
    int i3;
    if (bool1)
    {
      localObject = k;
      int i2 = b;
      i locali = null;
      if (localObject != null)
      {
        i1 = ((Integer)localObject).intValue();
        if (i1 == i2)
        {
          i1 = 0;
          localObject = null;
          break label131;
        }
      }
      int i1 = b;
      locali = a(i1);
      if (locali == null) {
        return false;
      }
      i1 = b;
      localObject = Integer.valueOf(i1);
      k = ((Integer)localObject);
      localObject = g;
      if (localObject != null)
      {
        i3 = b;
        ((t.h)localObject).a(locali, i3);
      }
    }
    else
    {
      localObject = g;
      if (localObject == null) {
        break label196;
      }
      i3 = b;
      paramh = a(i3);
      ((t.h)localObject).a(paramh);
    }
    return bool2;
    label196:
    return false;
  }
  
  public final void b()
  {
    g = null;
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "error");
    a(null, null);
    m = "";
    l = null;
    n = paramString;
  }
  
  public final int getItemCount()
  {
    Object localObject = l;
    int i1 = 1;
    if (localObject == null)
    {
      localObject = (CharSequence)m;
      int i2 = ((CharSequence)localObject).length();
      if (i2 > 0)
      {
        i2 = 1;
      }
      else
      {
        i2 = 0;
        localObject = null;
      }
      if (i2 == 0)
      {
        localObject = (CharSequence)n;
        i2 = ((CharSequence)localObject).length();
        if (i2 > 0)
        {
          i2 = 1;
        }
        else
        {
          i2 = 0;
          localObject = null;
        }
        if (i2 == 0)
        {
          localObject = h;
          if (localObject != null) {
            return ((com.truecaller.messaging.data.a.k)localObject).getCount();
          }
          return 0;
        }
      }
    }
    return i1;
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */