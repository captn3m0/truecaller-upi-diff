package com.truecaller.messaging.newconversation;

import android.os.Bundle;
import com.truecaller.b;
import com.truecaller.bb;
import com.truecaller.messaging.data.types.Participant;
import java.util.List;

public abstract class t$c
  extends bb
  implements b
{
  public abstract void a(Bundle paramBundle);
  
  public abstract void a(Participant paramParticipant);
  
  public abstract void a(List paramList);
  
  public abstract void b(Bundle paramBundle);
  
  public abstract void b(List paramList);
  
  public abstract boolean b();
  
  public abstract boolean c();
  
  public abstract boolean e();
  
  public abstract String f();
  
  public abstract boolean g();
  
  public abstract List h();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.t.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */