package com.truecaller.messaging.newconversation;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.data.entity.Contact;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class NewConversationPresenter$e
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  NewConversationPresenter$e(NewConversationPresenter paramNewConversationPresenter, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    e locale = new com/truecaller/messaging/newconversation/NewConversationPresenter$e;
    NewConversationPresenter localNewConversationPresenter = b;
    String str = c;
    locale.<init>(localNewConversationPresenter, str, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locale;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label153;
      }
      paramObject = NewConversationPresenter.a(b);
      localObject2 = new com/truecaller/messaging/newconversation/NewConversationPresenter$e$a;
      ((NewConversationPresenter.e.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      int j = 1;
      a = j;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Contact)paramObject;
    localObject1 = b;
    Object localObject2 = c;
    NewConversationPresenter.a((NewConversationPresenter)localObject1, (Contact)paramObject, (String)localObject2);
    return x.a;
    label153:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (e)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((e)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.NewConversationPresenter.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */