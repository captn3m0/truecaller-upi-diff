package com.truecaller.messaging.newconversation;

import android.os.CancellationSignal;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

final class NewConversationPresenter$b$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  NewConversationPresenter$b$a(NewConversationPresenter.b paramb, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/messaging/newconversation/NewConversationPresenter$b$a;
    NewConversationPresenter.b localb = b;
    locala.<init>(localb, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = NewConversationPresenter.e(b.b);
        localObject = b.c;
        CancellationSignal localCancellationSignal = b.d;
        String str = NewConversationPresenter.f(b.b).f();
        return ((f)paramObject).a((String)localObject, localCancellationSignal, str);
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.NewConversationPresenter.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */