package com.truecaller.messaging.newconversation;

public enum NewConversationPresenter$SendType
{
  static
  {
    SendType[] arrayOfSendType = new SendType[2];
    SendType localSendType = new com/truecaller/messaging/newconversation/NewConversationPresenter$SendType;
    localSendType.<init>("IM", 0);
    IM = localSendType;
    arrayOfSendType[0] = localSendType;
    localSendType = new com/truecaller/messaging/newconversation/NewConversationPresenter$SendType;
    int i = 1;
    localSendType.<init>("SMS", i);
    SMS = localSendType;
    arrayOfSendType[i] = localSendType;
    $VALUES = arrayOfSendType;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.NewConversationPresenter.SendType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */