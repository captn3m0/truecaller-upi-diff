package com.truecaller.messaging.newconversation;

import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  
  private h(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
    e = paramProvider5;
  }
  
  public static h a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    h localh = new com/truecaller/messaging/newconversation/h;
    localh.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
    return localh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */