package com.truecaller.messaging.newconversation;

import android.os.CancellationSignal;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.n;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class NewConversationPresenter$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  NewConversationPresenter$b(NewConversationPresenter paramNewConversationPresenter, String paramString, CancellationSignal paramCancellationSignal, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/messaging/newconversation/NewConversationPresenter$b;
    NewConversationPresenter localNewConversationPresenter = b;
    String str = c;
    CancellationSignal localCancellationSignal = d;
    localb.<init>(localNewConversationPresenter, str, localCancellationSignal, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label186;
      }
      paramObject = NewConversationPresenter.a(b);
      Object localObject2 = new com/truecaller/messaging/newconversation/NewConversationPresenter$b$a;
      ((NewConversationPresenter.b.a)localObject2).<init>(this, null);
      localObject2 = (m)localObject2;
      int j = 1;
      a = j;
      paramObject = g.a((f)paramObject, (m)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (n)paramObject;
    localObject1 = d;
    boolean bool2 = ((CancellationSignal)localObject1).isCanceled();
    if (bool2)
    {
      if (paramObject != null)
      {
        paramObject = (com.truecaller.messaging.data.a.k)a;
        if (paramObject != null) {
          ((com.truecaller.messaging.data.a.k)paramObject).close();
        }
      }
    }
    else
    {
      localObject1 = b;
      NewConversationPresenter.a((NewConversationPresenter)localObject1, (n)paramObject);
    }
    return x.a;
    label186:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.NewConversationPresenter.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */