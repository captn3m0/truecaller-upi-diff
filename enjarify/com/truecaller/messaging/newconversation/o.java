package com.truecaller.messaging.newconversation;

import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d
{
  private final n a;
  private final Provider b;
  private final Provider c;
  
  private o(n paramn, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramn;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static o a(n paramn, Provider paramProvider1, Provider paramProvider2)
  {
    o localo = new com/truecaller/messaging/newconversation/o;
    localo.<init>(paramn, paramProvider1, paramProvider2);
    return localo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */