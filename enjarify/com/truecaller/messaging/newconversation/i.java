package com.truecaller.messaging.newconversation;

import c.a.m;
import c.g.a.b;
import c.g.b.k;
import c.m.l;
import java.util.List;

public final class i
{
  public static final i.a l;
  final boolean a;
  final String b;
  final List c;
  final List d;
  final String e;
  final List f;
  final String g;
  final long h;
  final String i;
  final String j;
  final String k;
  private final long m;
  private final int n;
  private final int o;
  
  static
  {
    i.a locala = new com/truecaller/messaging/newconversation/i$a;
    locala.<init>((byte)0);
    l = locala;
  }
  
  public i(String paramString1, List paramList1, List paramList2, String paramString2, List paramList3, String paramString3, long paramLong1, long paramLong2, int paramInt1, int paramInt2, String paramString4, String paramString5, String paramString6)
  {
    b = paramString1;
    c = paramList1;
    d = paramList2;
    e = paramString2;
    f = paramList3;
    g = paramString3;
    h = paramLong1;
    m = paramLong2;
    int i1 = paramInt1;
    n = paramInt1;
    i1 = paramInt2;
    o = paramInt2;
    i = paramString4;
    j = paramString5;
    k = paramString6;
    i1 = n;
    if (i1 == 0) {
      i1 = 1;
    } else {
      i1 = 0;
    }
    a = i1;
  }
  
  public final String a()
  {
    Object localObject1 = c;
    int i1 = ((List)localObject1).size();
    Object localObject2 = f;
    int i2 = ((List)localObject2).size();
    if (i1 == i2)
    {
      localObject1 = j;
      if (localObject1 != null) {
        return (String)localObject1;
      }
      localObject1 = (Iterable)c;
      localObject2 = (Iterable)f;
      localObject1 = m.n((Iterable)m.d((Iterable)localObject1, (Iterable)localObject2));
      localObject2 = (b)i.b.a;
      localObject1 = l.d((c.m.i)localObject1, (b)localObject2);
      localObject2 = (CharSequence)", ";
      return l.a((c.m.i)localObject1, (CharSequence)localObject2);
    }
    localObject1 = new java/lang/RuntimeException;
    ((RuntimeException)localObject1).<init>("List size should match");
    throw ((Throwable)localObject1);
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof i;
      if (bool2)
      {
        paramObject = (i)paramObject;
        Object localObject1 = b;
        Object localObject2 = b;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          localObject1 = c;
          localObject2 = c;
          bool2 = k.a(localObject1, localObject2);
          if (bool2)
          {
            localObject1 = d;
            localObject2 = d;
            bool2 = k.a(localObject1, localObject2);
            if (bool2)
            {
              localObject1 = e;
              localObject2 = e;
              bool2 = k.a(localObject1, localObject2);
              if (bool2)
              {
                localObject1 = f;
                localObject2 = f;
                bool2 = k.a(localObject1, localObject2);
                if (bool2)
                {
                  localObject1 = g;
                  localObject2 = g;
                  bool2 = k.a(localObject1, localObject2);
                  if (bool2)
                  {
                    long l1 = h;
                    long l2 = h;
                    bool2 = l1 < l2;
                    if (!bool2)
                    {
                      bool2 = true;
                    }
                    else
                    {
                      bool2 = false;
                      localObject1 = null;
                    }
                    if (bool2)
                    {
                      l1 = m;
                      l2 = m;
                      bool2 = l1 < l2;
                      if (!bool2)
                      {
                        bool2 = true;
                      }
                      else
                      {
                        bool2 = false;
                        localObject1 = null;
                      }
                      if (bool2)
                      {
                        int i1 = n;
                        int i2 = n;
                        if (i1 == i2)
                        {
                          i1 = 1;
                        }
                        else
                        {
                          i1 = 0;
                          localObject1 = null;
                        }
                        if (i1 != 0)
                        {
                          i1 = o;
                          i2 = o;
                          if (i1 == i2)
                          {
                            i1 = 1;
                          }
                          else
                          {
                            i1 = 0;
                            localObject1 = null;
                          }
                          if (i1 != 0)
                          {
                            localObject1 = i;
                            localObject2 = i;
                            boolean bool3 = k.a(localObject1, localObject2);
                            if (bool3)
                            {
                              localObject1 = j;
                              localObject2 = j;
                              bool3 = k.a(localObject1, localObject2);
                              if (bool3)
                              {
                                localObject1 = k;
                                paramObject = k;
                                boolean bool4 = k.a(localObject1, paramObject);
                                if (bool4) {
                                  return bool1;
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str = b;
    int i1 = 0;
    if (str != null)
    {
      i2 = str.hashCode();
    }
    else
    {
      i2 = 0;
      str = null;
    }
    i2 *= 31;
    Object localObject = c;
    if (localObject != null)
    {
      i3 = localObject.hashCode();
    }
    else
    {
      i3 = 0;
      localObject = null;
    }
    int i2 = (i2 + i3) * 31;
    localObject = d;
    if (localObject != null)
    {
      i3 = localObject.hashCode();
    }
    else
    {
      i3 = 0;
      localObject = null;
    }
    i2 = (i2 + i3) * 31;
    localObject = e;
    if (localObject != null)
    {
      i3 = localObject.hashCode();
    }
    else
    {
      i3 = 0;
      localObject = null;
    }
    i2 = (i2 + i3) * 31;
    localObject = f;
    if (localObject != null)
    {
      i3 = localObject.hashCode();
    }
    else
    {
      i3 = 0;
      localObject = null;
    }
    i2 = (i2 + i3) * 31;
    localObject = g;
    if (localObject != null)
    {
      i3 = localObject.hashCode();
    }
    else
    {
      i3 = 0;
      localObject = null;
    }
    i2 = (i2 + i3) * 31;
    long l1 = h;
    int i4 = 32;
    long l2 = l1 >>> i4;
    int i5 = (int)(l1 ^ l2);
    i2 = (i2 + i5) * 31;
    l1 = m;
    long l3 = l1 >>> i4;
    l1 ^= l3;
    i5 = (int)l1;
    i2 = (i2 + i5) * 31;
    int i3 = n;
    i2 = (i2 + i3) * 31;
    i3 = o;
    i2 = (i2 + i3) * 31;
    localObject = i;
    if (localObject != null)
    {
      i3 = localObject.hashCode();
    }
    else
    {
      i3 = 0;
      localObject = null;
    }
    i2 = (i2 + i3) * 31;
    localObject = j;
    if (localObject != null)
    {
      i3 = localObject.hashCode();
    }
    else
    {
      i3 = 0;
      localObject = null;
    }
    i2 = (i2 + i3) * 31;
    localObject = k;
    if (localObject != null) {
      i1 = localObject.hashCode();
    }
    return i2 + i1;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("NewConversationDestination(conversationId=");
    Object localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", names=");
    localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", phonebookIds=");
    localObject = d;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", imageUri=");
    localObject = e;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", normalizedNumbers=");
    localObject = f;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", contactImPeerId=");
    localObject = g;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", contactImRegistrationTimestamp=");
    long l1 = h;
    localStringBuilder.append(l1);
    localStringBuilder.append(", timestamp=");
    l1 = m;
    localStringBuilder.append(l1);
    localStringBuilder.append(", transportType=");
    int i1 = n;
    localStringBuilder.append(i1);
    localStringBuilder.append(", group=");
    i1 = o;
    localStringBuilder.append(i1);
    localStringBuilder.append(", imGroupId=");
    localObject = i;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", imGroupTitle=");
    localObject = j;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", imGroupAvatar=");
    localObject = k;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */