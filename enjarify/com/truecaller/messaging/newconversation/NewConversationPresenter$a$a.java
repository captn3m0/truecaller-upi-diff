package com.truecaller.messaging.newconversation;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.o;
import kotlinx.coroutines.ag;

final class NewConversationPresenter$a$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  NewConversationPresenter$a$a(long paramLong, c paramc, NewConversationPresenter.a parama, ag paramag)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/messaging/newconversation/NewConversationPresenter$a$a;
    long l = b;
    NewConversationPresenter.a locala1 = c;
    ag localag = d;
    locala.<init>(l, paramc, locala1, localag);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = (o)NewConversationPresenter.g(c.e).a();
        long l = b;
        return ((o)paramObject).a(l).d();
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.NewConversationPresenter.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */