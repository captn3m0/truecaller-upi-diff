package com.truecaller.messaging.newconversation;

import android.text.Editable;
import android.text.TextWatcher;
import c.g.b.k;

public final class j$i
  implements TextWatcher
{
  j$i(j paramj) {}
  
  public final void afterTextChanged(Editable paramEditable)
  {
    k.b(paramEditable, "editable");
    t.g localg = a.b();
    paramEditable = paramEditable.toString();
    localg.a(paramEditable);
  }
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    k.b(paramCharSequence, "charSequence");
  }
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    k.b(paramCharSequence, "charSequence");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.j.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */