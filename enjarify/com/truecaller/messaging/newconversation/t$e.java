package com.truecaller.messaging.newconversation;

import android.net.Uri;
import com.truecaller.ui.q.c;

public abstract interface t$e
  extends q.c
{
  public abstract void a(int paramInt);
  
  public abstract void a(Uri paramUri);
  
  public abstract void b(String paramString);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c(String paramString);
  
  public abstract void c(boolean paramBoolean);
  
  public abstract void e(boolean paramBoolean);
  
  public abstract void g(boolean paramBoolean);
  
  public abstract void h(boolean paramBoolean);
  
  public abstract void i(boolean paramBoolean);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.t.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */