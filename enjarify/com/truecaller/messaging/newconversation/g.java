package com.truecaller.messaging.newconversation;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.OperationCanceledException;
import com.truecaller.content.TruecallerContract.ad;
import com.truecaller.data.entity.Contact;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.data.c;
import com.truecaller.network.search.j;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class g
  implements f
{
  private final bw a;
  private final com.truecaller.network.search.l b;
  private final ContentResolver c;
  private final c d;
  private final t.f e;
  
  public g(bw parambw, com.truecaller.network.search.l paraml, ContentResolver paramContentResolver, c paramc, t.f paramf)
  {
    a = parambw;
    b = paraml;
    c = paramContentResolver;
    d = paramc;
    e = paramf;
  }
  
  private final com.truecaller.messaging.data.a.k a(Uri paramUri, CancellationSignal paramCancellationSignal)
  {
    try
    {
      ContentResolver localContentResolver = c;
      paramUri = localContentResolver.query(paramUri, null, null, null, null, paramCancellationSignal);
    }
    catch (OperationCanceledException localOperationCanceledException)
    {
      paramUri = null;
    }
    if (paramUri == null) {
      return null;
    }
    return d.h(paramUri);
  }
  
  public final c.n a(String paramString1, CancellationSignal paramCancellationSignal, String paramString2)
  {
    c.g.b.k.b(paramString1, "query");
    c.g.b.k.b(paramCancellationSignal, "cancellationSignal");
    c.g.b.k.b(paramString2, "conversationType");
    Object localObject = e;
    boolean bool1 = localObject instanceof t.f.a;
    if (!bool1)
    {
      bool1 = localObject instanceof t.f.d;
      if (!bool1)
      {
        boolean bool2 = localObject instanceof t.f.c;
        if (!bool2)
        {
          bool2 = localObject instanceof t.f.b;
          if (!bool2)
          {
            paramString1 = new c/l;
            paramString1.<init>();
            throw paramString1;
          }
        }
        bool2 = a.a();
        paramString1 = TruecallerContract.ad.a(bool2, paramString1);
        paramString2 = "NewConversationDestinati…VITEE_VALUE\n            )";
        c.g.b.k.a(paramString1, paramString2);
        break label148;
      }
    }
    localObject = a;
    boolean bool3 = ((bw)localObject).a();
    paramString1 = TruecallerContract.ad.a(bool3, paramString1, paramString2);
    paramString2 = "NewConversationDestinati… query, conversationType)";
    c.g.b.k.a(paramString1, paramString2);
    label148:
    paramString1 = a(paramString1, paramCancellationSignal);
    int k = 0;
    paramCancellationSignal = null;
    if (paramString1 == null) {
      return null;
    }
    paramString2 = new java/util/ArrayList;
    paramString2.<init>();
    paramString2 = (List)paramString2;
    for (;;)
    {
      bool3 = paramString1.moveToNext();
      if (!bool3) {
        break;
      }
      int j = paramString1.b();
      if (paramCancellationSignal != null)
      {
        int i = paramCancellationSignal.intValue();
        if (j == i) {}
      }
      else
      {
        k = paramString1.b();
        paramCancellationSignal = Integer.valueOf(k);
        j = paramString1.getPosition();
        localObject = Integer.valueOf(j);
        paramString2.add(localObject);
      }
    }
    paramCancellationSignal = new c/n;
    paramCancellationSignal.<init>(paramString1, paramString2);
    return paramCancellationSignal;
  }
  
  public final Contact a(String paramString)
  {
    c.g.b.k.b(paramString, "query");
    String str1 = null;
    try
    {
      Object localObject = b;
      UUID localUUID = UUID.randomUUID();
      String str2 = "UUID.randomUUID()";
      c.g.b.k.a(localUUID, str2);
      str2 = "newConversation";
      localObject = ((com.truecaller.network.search.l)localObject).a(localUUID, str2);
      localObject = ((j)localObject).b();
      paramString = ((j)localObject).a(paramString);
      paramString = paramString.a();
      int i = 4;
      paramString = paramString.a(i);
      paramString = paramString.f();
      if (paramString != null)
      {
        paramString = paramString.a();
        str1 = paramString;
      }
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
    return str1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */