package com.truecaller.messaging.newconversation;

import android.os.Bundle;
import c.a.m;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.h;
import com.truecaller.util.al;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class k
  extends t.c
{
  private final ArrayList a;
  private String c;
  private boolean d;
  private final t.f e;
  private final al f;
  private final e g;
  private final h h;
  
  public k(t.f paramf, al paramal, e parame, h paramh)
  {
    e = paramf;
    f = paramal;
    g = parame;
    h = paramh;
    paramf = new java/util/ArrayList;
    paramf.<init>();
    a = paramf;
    c = "one_to_one_type";
  }
  
  private final void i()
  {
    c = "im_group_type";
    k();
  }
  
  private final void j()
  {
    c = "mms_group_type";
    k();
  }
  
  private final void k()
  {
    t.d locald = (t.d)b;
    if (locald != null)
    {
      locald.i();
      locald.b(false);
      locald.a(false);
      boolean bool1 = a.isEmpty();
      locald.c(bool1);
      bool1 = ((Collection)a).isEmpty() ^ true;
      locald.d(bool1);
      Object localObject = e;
      bool1 = localObject instanceof t.f.d;
      if (bool1)
      {
        localObject = c;
        int k = ((String)localObject).hashCode();
        int m = -1481575691;
        String str;
        if (k != m)
        {
          m = -1152006682;
          if (k == m)
          {
            str = "mms_group_type";
            bool1 = ((String)localObject).equals(str);
            if (bool1)
            {
              int i = 2131886716;
              localObject = Integer.valueOf(i);
              locald.a((Integer)localObject);
            }
          }
        }
        else
        {
          str = "im_group_type";
          boolean bool2 = ((String)localObject).equals(str);
          if (bool2)
          {
            int j = 2131886715;
            localObject = Integer.valueOf(j);
            locald.a((Integer)localObject);
          }
        }
      }
      locald.e();
      return;
    }
  }
  
  public final int a()
  {
    return a.size();
  }
  
  public final int a(int paramInt)
  {
    return 0;
  }
  
  public final void a(Bundle paramBundle)
  {
    if (paramBundle != null)
    {
      Object localObject = paramBundle.getParcelableArrayList("group_participants");
      if (localObject != null)
      {
        localObject = (List)localObject;
        b((List)localObject);
      }
      localObject = paramBundle.getString("conversation_mode");
      if (localObject == null) {
        localObject = "one_to_one_type";
      }
      c = ((String)localObject);
      localObject = c;
      int i = ((String)localObject).hashCode();
      int j = -1481575691;
      String str;
      boolean bool1;
      if (i != j)
      {
        j = -1152006682;
        if (i == j)
        {
          str = "mms_group_type";
          bool1 = ((String)localObject).equals(str);
          if (bool1) {
            j();
          }
        }
      }
      else
      {
        str = "im_group_type";
        bool1 = ((String)localObject).equals(str);
        if (bool1) {
          i();
        }
      }
      boolean bool2 = paramBundle.getBoolean("is_in_multi_pick_mode");
      d = bool2;
      return;
    }
  }
  
  public final void a(Participant paramParticipant)
  {
    c.g.b.k.b(paramParticipant, "participant");
    ArrayList localArrayList = a;
    localArrayList.remove(paramParticipant);
    paramParticipant = (t.d)b;
    if (paramParticipant == null) {
      return;
    }
    paramParticipant.j();
    localArrayList = a;
    boolean bool = localArrayList.isEmpty();
    if (bool)
    {
      paramParticipant.c(true);
      bool = false;
      localArrayList = null;
      paramParticipant.d(false);
    }
    paramParticipant.f();
  }
  
  public final void a(List paramList)
  {
    if (paramList == null) {
      return;
    }
    b(paramList);
    d = true;
  }
  
  public final long b(int paramInt)
  {
    return -1;
  }
  
  public final void b(Bundle paramBundle)
  {
    c.g.b.k.b(paramBundle, "state");
    Object localObject = c;
    paramBundle.putString("conversation_mode", (String)localObject);
    boolean bool = d;
    paramBundle.putBoolean("is_in_multi_pick_mode", bool);
    localObject = a;
    paramBundle.putParcelableArrayList("group_participants", (ArrayList)localObject);
  }
  
  public final void b(List paramList)
  {
    Object localObject1 = "participantsToAdd";
    c.g.b.k.b(paramList, (String)localObject1);
    boolean bool1 = paramList.isEmpty();
    if (bool1) {
      return;
    }
    localObject1 = (t.d)b;
    if (localObject1 == null) {
      return;
    }
    paramList = (Iterable)paramList;
    Object localObject2 = (Iterable)a;
    localObject2 = m.c(paramList, (Iterable)localObject2);
    boolean bool2 = ((List)localObject2).isEmpty();
    if (bool2)
    {
      ((t.d)localObject1).b(2131888560);
      return;
    }
    Object localObject3 = a;
    int i = ((ArrayList)localObject3).size();
    int j = ((List)localObject2).size();
    i += j;
    h localh = h;
    j = localh.Z();
    int k;
    if (i > j)
    {
      k = h.Z();
      ((t.d)localObject1).c(k);
      return;
    }
    localObject3 = a;
    localObject2 = (Collection)localObject2;
    ((ArrayList)localObject3).addAll((Collection)localObject2);
    localObject2 = c;
    localObject3 = "one_to_one_type";
    boolean bool4 = c.g.b.k.a(localObject2, localObject3);
    i = 1;
    if (bool4)
    {
      localObject2 = a;
      int n = ((ArrayList)localObject2).size();
      if (n > i)
      {
        boolean bool5 = paramList instanceof Collection;
        if (bool5)
        {
          localObject2 = paramList;
          localObject2 = (Collection)paramList;
          bool5 = ((Collection)localObject2).isEmpty();
          if (bool5) {}
        }
        else
        {
          paramList = ((Iterable)paramList).iterator();
          do
          {
            bool5 = paramList.hasNext();
            if (!bool5) {
              break;
            }
            localObject2 = (Participant)paramList.next();
            bool5 = ((Participant)localObject2).f();
          } while (bool5);
          k = 0;
          paramList = null;
          break label308;
        }
        k = 1;
        label308:
        if (k != 0)
        {
          paramList = g.i();
          bool3 = paramList.a();
          if (bool3)
          {
            i();
            break label392;
          }
        }
        j();
        break label392;
      }
    }
    boolean bool3 = a.isEmpty();
    ((t.d)localObject1).c(bool3);
    paramList = (Collection)a;
    bool3 = paramList.isEmpty() ^ i;
    ((t.d)localObject1).d(bool3);
    label392:
    int m = a.size() - i;
    ((t.d)localObject1).d(m);
    ((t.d)localObject1).f();
  }
  
  public final boolean b()
  {
    i();
    return true;
  }
  
  public final boolean c()
  {
    j();
    return true;
  }
  
  public final boolean e()
  {
    String str1 = c;
    String str2 = "im_group_type";
    boolean bool = c.g.b.k.a(str1, str2);
    if (!bool)
    {
      str1 = c;
      str2 = "mms_group_type";
      bool = c.g.b.k.a(str1, str2);
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  public final String f()
  {
    return c;
  }
  
  public final boolean g()
  {
    return d;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */