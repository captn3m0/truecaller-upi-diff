package com.truecaller.messaging.newconversation;

import c.d.c;
import c.g.a.m;
import c.x;
import kotlinx.coroutines.ag;

final class NewConversationPresenter$c$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  NewConversationPresenter$c$a(NewConversationPresenter.c paramc, c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/messaging/newconversation/NewConversationPresenter$c$a;
    NewConversationPresenter.c localc = b;
    locala.<init>(localc, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  /* Error */
  public final Object a(Object paramObject)
  {
    // Byte code:
    //   0: getstatic 38	c/d/a/a:a	Lc/d/a/a;
    //   3: astore_2
    //   4: aload_0
    //   5: getfield 40	com/truecaller/messaging/newconversation/NewConversationPresenter$c$a:a	I
    //   8: istore_3
    //   9: iload_3
    //   10: ifne +201 -> 211
    //   13: aload_1
    //   14: instanceof 42
    //   17: istore_3
    //   18: iload_3
    //   19: ifne +184 -> 203
    //   22: aload_0
    //   23: getfield 14	com/truecaller/messaging/newconversation/NewConversationPresenter$c$a:b	Lcom/truecaller/messaging/newconversation/NewConversationPresenter$c;
    //   26: getfield 47	com/truecaller/messaging/newconversation/NewConversationPresenter$c:b	Lcom/truecaller/messaging/newconversation/NewConversationPresenter;
    //   29: invokestatic 53	com/truecaller/messaging/newconversation/NewConversationPresenter:i	(Lcom/truecaller/messaging/newconversation/NewConversationPresenter;)Lcom/truecaller/androidactors/f;
    //   32: invokeinterface 58 1 0
    //   37: checkcast 60	com/truecaller/messaging/transport/im/a/a
    //   40: astore_1
    //   41: aload_0
    //   42: getfield 14	com/truecaller/messaging/newconversation/NewConversationPresenter$c$a:b	Lcom/truecaller/messaging/newconversation/NewConversationPresenter$c;
    //   45: getfield 63	com/truecaller/messaging/newconversation/NewConversationPresenter$c:c	Ljava/lang/String;
    //   48: astore_2
    //   49: aload_1
    //   50: aload_2
    //   51: invokeinterface 66 2 0
    //   56: invokevirtual 71	com/truecaller/androidactors/w:d	()Ljava/lang/Object;
    //   59: checkcast 73	com/truecaller/messaging/transport/im/a/g
    //   62: astore_1
    //   63: iconst_0
    //   64: istore_3
    //   65: aconst_null
    //   66: astore_2
    //   67: aload_1
    //   68: ifnull +133 -> 201
    //   71: aload_1
    //   72: checkcast 75	android/database/Cursor
    //   75: astore_1
    //   76: aload_1
    //   77: astore 4
    //   79: aload_1
    //   80: checkcast 77	java/io/Closeable
    //   83: astore 4
    //   85: new 79	java/util/ArrayList
    //   88: astore 5
    //   90: aload 5
    //   92: invokespecial 82	java/util/ArrayList:<init>	()V
    //   95: aload 5
    //   97: checkcast 84	java/util/Collection
    //   100: astore 5
    //   102: aload_1
    //   103: invokeinterface 88 1 0
    //   108: istore 6
    //   110: iload 6
    //   112: ifeq +50 -> 162
    //   115: aload_1
    //   116: astore 7
    //   118: aload_1
    //   119: checkcast 73	com/truecaller/messaging/transport/im/a/g
    //   122: astore 7
    //   124: aload 7
    //   126: invokeinterface 91 1 0
    //   131: astore 7
    //   133: aload 7
    //   135: getfield 95	com/truecaller/messaging/transport/im/a/f:a	Ljava/lang/String;
    //   138: astore 8
    //   140: aload 8
    //   142: aload 7
    //   144: invokestatic 100	c/t:a	(Ljava/lang/Object;Ljava/lang/Object;)Lc/n;
    //   147: astore 7
    //   149: aload 5
    //   151: aload 7
    //   153: invokeinterface 104 2 0
    //   158: pop
    //   159: goto -57 -> 102
    //   162: aload 5
    //   164: checkcast 106	java/util/List
    //   167: astore 5
    //   169: aload 4
    //   171: aconst_null
    //   172: invokestatic 111	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   175: aload 5
    //   177: checkcast 113	java/lang/Iterable
    //   180: invokestatic 118	c/a/ag:a	(Ljava/lang/Iterable;)Ljava/util/Map;
    //   183: areturn
    //   184: astore_1
    //   185: goto +8 -> 193
    //   188: astore_1
    //   189: aload_1
    //   190: astore_2
    //   191: aload_1
    //   192: athrow
    //   193: aload 4
    //   195: aload_2
    //   196: invokestatic 111	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   199: aload_1
    //   200: athrow
    //   201: aconst_null
    //   202: areturn
    //   203: aload_1
    //   204: checkcast 42	c/o$b
    //   207: getfield 121	c/o$b:a	Ljava/lang/Throwable;
    //   210: athrow
    //   211: new 123	java/lang/IllegalStateException
    //   214: astore_1
    //   215: aload_1
    //   216: ldc 125
    //   218: invokespecial 128	java/lang/IllegalStateException:<init>	(Ljava/lang/String;)V
    //   221: aload_1
    //   222: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	223	0	this	a
    //   0	223	1	paramObject	Object
    //   3	193	2	localObject1	Object
    //   8	2	3	i	int
    //   17	48	3	bool1	boolean
    //   77	117	4	localObject2	Object
    //   88	88	5	localObject3	Object
    //   108	3	6	bool2	boolean
    //   116	36	7	localObject4	Object
    //   138	3	8	str	String
    // Exception table:
    //   from	to	target	type
    //   191	193	184	finally
    //   85	88	188	finally
    //   90	95	188	finally
    //   95	100	188	finally
    //   102	108	188	finally
    //   118	122	188	finally
    //   124	131	188	finally
    //   133	138	188	finally
    //   142	147	188	finally
    //   151	159	188	finally
    //   162	167	188	finally
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.NewConversationPresenter.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */