package com.truecaller.messaging.newconversation;

import android.os.Bundle;
import android.os.CancellationSignal;
import c.a.y;
import c.l;
import com.truecaller.analytics.ap;
import com.truecaller.featuretoggles.b;
import com.truecaller.messaging.ForwardContentItem;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Participant.a;
import com.truecaller.messaging.g.d;
import com.truecaller.multisim.h;
import com.truecaller.util.al;
import com.truecaller.util.bg;
import com.truecaller.utils.extensions.c;
import com.truecaller.utils.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.bn;

public final class NewConversationPresenter
  extends t.g
  implements t.h
{
  private final t.f A;
  private final com.truecaller.featuretoggles.e B;
  private final com.truecaller.androidactors.f C;
  private final c.n.k c;
  private String d;
  private boolean e;
  private CancellationSignal f;
  private bn g;
  private bn h;
  private NewConversationPresenter.SendType i;
  private ArrayList j;
  private final c.d.f k;
  private final c.d.f l;
  private final long m;
  private final com.truecaller.common.h.u n;
  private final al o;
  private final ap p;
  private final com.truecaller.androidactors.f q;
  private final d r;
  private final h s;
  private final f t;
  private final com.truecaller.messaging.f u;
  private final o v;
  private final bg w;
  private final com.truecaller.androidactors.f x;
  private final t.a y;
  private final t.c z;
  
  public NewConversationPresenter(c.d.f paramf1, c.d.f paramf2, long paramLong, com.truecaller.common.h.u paramu, al paramal, ap paramap, com.truecaller.androidactors.f paramf3, d paramd, h paramh, f paramf, com.truecaller.messaging.f paramf4, o paramo, bg parambg, com.truecaller.androidactors.f paramf5, t.a parama, t.c paramc, t.f paramf6, com.truecaller.featuretoggles.e parame, com.truecaller.androidactors.f paramf7)
  {
    super(paramf1);
    k = paramf1;
    l = paramf2;
    m = paramLong;
    n = paramu;
    o = paramal;
    p = paramap;
    q = paramf3;
    r = paramd;
    s = paramh;
    t = paramf;
    u = paramf4;
    v = paramo;
    w = parambg;
    x = paramf5;
    y = parama;
    z = paramc;
    A = paramf6;
    localObject = paramf7;
    B = parame;
    C = paramf7;
    localObject = new c/n/k;
    ((c.n.k)localObject).<init>("\\+?[\\d\\s()-]+");
    c = ((c.n.k)localObject);
    d = "";
  }
  
  private final void a(Participant paramParticipant, Long paramLong)
  {
    Object localObject = h;
    boolean bool1;
    if (localObject != null)
    {
      if (localObject != null)
      {
        bool1 = ((bn)localObject).j();
        localObject = Boolean.valueOf(bool1);
      }
      else
      {
        bool1 = false;
        localObject = null;
      }
      bool1 = c.a((Boolean)localObject);
      if (!bool1) {
        return;
      }
    }
    localObject = A;
    boolean bool2 = localObject instanceof t.f.c;
    if (!bool2)
    {
      bool1 = false;
      localObject = null;
    }
    localObject = (t.f.c)localObject;
    if (localObject != null)
    {
      ArrayList localArrayList = a;
      if (localArrayList != null)
      {
        if ((paramParticipant == null) && (paramLong == null))
        {
          paramParticipant = new java/lang/IllegalArgumentException;
          paramParticipant.<init>("Neither participant nor conversation is selected to forward to");
          throw ((Throwable)paramParticipant);
        }
        localObject = new com/truecaller/messaging/newconversation/NewConversationPresenter$a;
        ((NewConversationPresenter.a)localObject).<init>(this, paramLong, localArrayList, paramParticipant, null);
        localObject = (c.g.a.m)localObject;
        kotlinx.coroutines.e.b(this, null, (c.g.a.m)localObject, 3);
        return;
      }
    }
  }
  
  private final void a(Long paramLong, List paramList)
  {
    t.i locali = (t.i)b;
    if (locali == null) {
      return;
    }
    t.c localc = z;
    boolean bool1 = localc.g();
    if (bool1)
    {
      if (paramList == null)
      {
        paramLong = y.a;
        paramList = paramLong;
        paramList = (List)paramLong;
      }
      paramList = (Collection)paramList;
      paramLong = new java/util/ArrayList;
      paramLong.<init>(paramList);
      locali.a(paramLong);
      locali.g();
      return;
    }
    bool1 = false;
    localc = null;
    Object localObject1;
    if (paramList != null)
    {
      localObject1 = paramList;
      localObject1 = (Collection)paramList;
      if (localObject1 != null)
      {
        localObject2 = new Participant[0];
        localObject1 = ((Collection)localObject1).toArray((Object[])localObject2);
        if (localObject1 != null)
        {
          localObject1 = (Participant[])localObject1;
        }
        else
        {
          paramLong = new c/u;
          paramLong.<init>("null cannot be cast to non-null type kotlin.Array<T>");
          throw paramLong;
        }
      }
      else
      {
        paramLong = new c/u;
        paramLong.<init>("null cannot be cast to non-null type java.util.Collection<T>");
        throw paramLong;
      }
    }
    else
    {
      localObject1 = null;
    }
    Object localObject2 = A;
    boolean bool2 = localObject2 instanceof t.f.b;
    if (bool2)
    {
      localObject2 = (t.f.b)localObject2;
      paramList = a;
      locali.a(paramLong, (Participant[])localObject1, paramList);
    }
    else
    {
      bool2 = localObject2 instanceof t.f.c;
      if (bool2)
      {
        locali.a(paramLong, (Participant[])localObject1, null);
      }
      else
      {
        bool1 = localObject2 instanceof t.f.a;
        if (bool1)
        {
          if (paramList == null)
          {
            paramLong = y.a;
            paramList = paramLong;
            paramList = (List)paramLong;
          }
          paramList = (Collection)paramList;
          paramLong = new java/util/ArrayList;
          paramLong.<init>(paramList);
          locali.a(paramLong);
        }
        else
        {
          boolean bool3 = localObject2 instanceof t.f.d;
          if (bool3)
          {
            a(paramLong, (Participant[])localObject1);
            return;
          }
        }
      }
    }
    locali.g();
  }
  
  private final void a(Long paramLong, Participant[] paramArrayOfParticipant)
  {
    t.i locali = (t.i)b;
    if (locali == null) {
      return;
    }
    Object localObject = B.i();
    boolean bool = ((b)localObject).a();
    if (bool)
    {
      localObject = z.f();
      String str1 = "im_group_type";
      bool = c.g.b.k.a(localObject, str1);
      if ((bool) && (paramArrayOfParticipant != null))
      {
        int i1 = paramArrayOfParticipant.length;
        int i2 = 0;
        str1 = null;
        int i3 = 1;
        if (i1 == 0)
        {
          i1 = 1;
        }
        else
        {
          i1 = 0;
          localObject = null;
        }
        i1 ^= i3;
        if (i1 != 0)
        {
          i1 = paramArrayOfParticipant.length;
          int i4 = 0;
          while (i4 < i1)
          {
            String str2 = d;
            int i5;
            if (str2 != null)
            {
              i5 = 1;
            }
            else
            {
              i5 = 0;
              str2 = null;
            }
            if (i5 == 0) {
              break label170;
            }
            i4 += 1;
          }
          i2 = 1;
          label170:
          if (i2 != 0)
          {
            locali.a(paramArrayOfParticipant);
            return;
          }
        }
      }
    }
    locali.a(paramLong, paramArrayOfParticipant, null);
    locali.g();
  }
  
  private final void a(boolean paramBoolean)
  {
    Integer localInteger = l();
    if (localInteger != null)
    {
      int i1 = localInteger.intValue();
      com.truecaller.messaging.f localf;
      if (paramBoolean)
      {
        localf = u;
        paramBoolean = localf.b();
      }
      else
      {
        localf = u;
        paramBoolean = localf.a();
      }
      t.i locali = (t.i)b;
      if (locali != null)
      {
        int i2 = u.a(i1);
        i1 = u.b(i1);
        locali.a(i2, i1, paramBoolean);
        return;
      }
      return;
    }
  }
  
  private final void c(String paramString)
  {
    CancellationSignal localCancellationSignal = f;
    if (localCancellationSignal != null) {
      localCancellationSignal.cancel();
    }
    localCancellationSignal = new android/os/CancellationSignal;
    localCancellationSignal.<init>();
    f = localCancellationSignal;
    Object localObject = new com/truecaller/messaging/newconversation/NewConversationPresenter$b;
    ((NewConversationPresenter.b)localObject).<init>(this, paramString, localCancellationSignal, null);
    localObject = (c.g.a.m)localObject;
    kotlinx.coroutines.e.b(this, null, (c.g.a.m)localObject, 3);
  }
  
  private final void d(String paramString)
  {
    com.truecaller.common.h.u localu = n;
    String str = localu.a();
    paramString = Participant.a(paramString, localu, str);
    c.g.b.k.a(paramString, "Participant.buildFromAdd…per.getDefaultSimToken())");
    paramString = c.a.m.a(paramString);
    a(null, paramString);
  }
  
  private final boolean e(String paramString)
  {
    paramString = (CharSequence)paramString;
    c.n.k localk = c;
    int i1 = localk.a(paramString);
    if (i1 != 0)
    {
      i1 = 0;
      localk = null;
      boolean bool2;
      for (;;)
      {
        int i3 = paramString.length();
        bool2 = true;
        if (i1 >= i3) {
          break;
        }
        boolean bool1 = Character.isDigit(paramString.charAt(i1));
        if (bool1)
        {
          i4 = 1;
          break label76;
        }
        int i2;
        i1 += 1;
      }
      int i4 = 0;
      paramString = null;
      label76:
      if (i4 != 0) {
        return bool2;
      }
    }
    return false;
  }
  
  private final boolean k()
  {
    Object localObject1 = A;
    boolean bool1 = localObject1 instanceof t.f.c;
    Object localObject2 = null;
    if (!bool1) {
      localObject1 = null;
    }
    localObject1 = (t.f.c)localObject1;
    bool1 = false;
    if (localObject1 != null)
    {
      localObject1 = a;
      boolean bool2 = true;
      if (localObject1 != null)
      {
        localObject1 = ((Iterable)localObject1).iterator();
        Object localObject3;
        int i1;
        do
        {
          boolean bool3 = ((Iterator)localObject1).hasNext();
          if (!bool3) {
            break;
          }
          localObject3 = ((Iterator)localObject1).next();
          Object localObject4 = localObject3;
          localObject4 = b;
          if (localObject4 != null)
          {
            i1 = 1;
          }
          else
          {
            i1 = 0;
            localObject4 = null;
          }
        } while (i1 == 0);
        localObject2 = localObject3;
        localObject2 = (ForwardContentItem)localObject2;
      }
      if (localObject2 != null) {
        return bool2;
      }
      return false;
    }
    return false;
  }
  
  private final Integer l()
  {
    NewConversationPresenter.SendType localSendType1 = i;
    if (localSendType1 != null)
    {
      NewConversationPresenter.SendType localSendType2 = NewConversationPresenter.SendType.IM;
      if (localSendType1 == localSendType2) {
        return Integer.valueOf(2);
      }
      boolean bool = k();
      if (bool) {
        return Integer.valueOf(1);
      }
      return Integer.valueOf(0);
    }
    return null;
  }
  
  public final void a()
  {
    Object localObject1 = (t.i)b;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = o;
    boolean bool1 = ((al)localObject2).a();
    if (!bool1)
    {
      localObject2 = o;
      bool1 = ((al)localObject2).b();
      if (bool1)
      {
        ((t.i)localObject1).h();
        ((t.i)localObject1).g();
        return;
      }
    }
    localObject1 = A;
    bool1 = localObject1 instanceof t.f.c;
    int i1;
    if (!bool1)
    {
      i1 = 0;
      localObject1 = null;
    }
    localObject1 = (t.f.c)localObject1;
    if (localObject1 != null)
    {
      localObject1 = a;
      if (localObject1 != null)
      {
        if (localObject1 != null)
        {
          localObject2 = localObject1;
          localObject2 = (Collection)localObject1;
          Object localObject3 = "receiver$0";
          c.g.b.k.b(localObject2, (String)localObject3);
          localObject2 = ((Iterable)localObject2).iterator();
          boolean bool3;
          int i2;
          do
          {
            bool2 = ((Iterator)localObject2).hasNext();
            bool3 = true;
            if (!bool2) {
              break;
            }
            localObject3 = ((Iterator)localObject2).next();
            Object localObject4 = localObject3;
            localObject4 = b;
            if (localObject4 != null)
            {
              i2 = 1;
            }
            else
            {
              i2 = 0;
              localObject4 = null;
            }
          } while (i2 == 0);
          break label199;
          boolean bool2 = false;
          localObject3 = null;
          label199:
          if (localObject3 == null) {
            bool3 = false;
          }
          localObject2 = Boolean.valueOf(bool3);
        }
        else
        {
          bool1 = false;
          localObject2 = null;
        }
        bool1 = c.a((Boolean)localObject2);
        if (bool1)
        {
          localObject2 = j;
          if (localObject2 == null)
          {
            localObject2 = h;
            if (localObject2 == null)
            {
              localObject2 = new com/truecaller/messaging/newconversation/NewConversationPresenter$d;
              ((NewConversationPresenter.d)localObject2).<init>(this, (ArrayList)localObject1, null);
              localObject2 = (c.g.a.m)localObject2;
              i1 = 3;
              localObject1 = kotlinx.coroutines.e.b(this, null, (c.g.a.m)localObject2, i1);
              h = ((bn)localObject1);
            }
          }
        }
        return;
      }
    }
  }
  
  public final void a(int paramInt)
  {
    Object localObject1 = y.a();
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = NewConversationPresenter.SendType.values();
    Object localObject3 = localObject2[paramInt];
    i = ((NewConversationPresenter.SendType)localObject3);
    localObject3 = (t.i)b;
    if (localObject3 != null) {
      ((t.i)localObject3).o();
    }
    localObject3 = (t.i)b;
    boolean bool1 = false;
    localObject2 = null;
    boolean bool2 = true;
    if (localObject3 != null)
    {
      boolean bool3 = a;
      if (bool3)
      {
        localObject1 = i;
        NewConversationPresenter.SendType localSendType = NewConversationPresenter.SendType.IM;
        if (localObject1 == localSendType)
        {
          bool3 = true;
          break label109;
        }
      }
      bool3 = false;
      localObject1 = null;
      label109:
      int i2 = 12;
      t.i.a.a((t.i)localObject3, bool2, bool3, i2);
    }
    localObject3 = i;
    localObject1 = NewConversationPresenter.SendType.IM;
    if (localObject3 == localObject1) {
      bool1 = true;
    }
    a(bool1);
    localObject3 = p;
    localObject1 = i;
    if (localObject1 == null)
    {
      localObject1 = "Unknown";
    }
    else
    {
      localObject2 = u.a;
      int i1 = ((NewConversationPresenter.SendType)localObject1).ordinal();
      i1 = localObject2[i1];
      switch (i1)
      {
      default: 
        localObject3 = new c/l;
        ((l)localObject3).<init>();
        throw ((Throwable)localObject3);
      case 2: 
        localObject1 = "SMS";
        break;
      case 1: 
        localObject1 = "IM";
      }
    }
    ((ap)localObject3).a((String)localObject1, "newConversation");
  }
  
  public final void a(Bundle paramBundle)
  {
    c.g.b.k.b(paramBundle, "outState");
    ArrayList localArrayList = j;
    paramBundle.putParcelableArrayList("newconversation_copied_entities", localArrayList);
  }
  
  public final void a(i parami)
  {
    boolean bool1 = false;
    Object localObject1 = null;
    if (parami != null)
    {
      localObject2 = b;
      if (localObject2 != null)
      {
        long l1 = Long.parseLong((String)localObject2);
        localObject2 = Long.valueOf(l1);
        break label39;
      }
    }
    Object localObject2 = null;
    label39:
    if (localObject2 != null)
    {
      parami = null;
    }
    else
    {
      if (parami != null)
      {
        localObject3 = f;
        if (localObject3 != null)
        {
          localObject3 = (com.truecaller.data.entity.Number)c.a.m.e((List)localObject3);
          if (localObject3 != null)
          {
            localObject3 = ((com.truecaller.data.entity.Number)localObject3).a();
            if (localObject3 != null) {
              break label97;
            }
          }
        }
      }
      localObject3 = d;
      label97:
      Object localObject4 = n;
      String str = ((com.truecaller.common.h.u)localObject4).a();
      localObject3 = Participant.a((String)localObject3, (com.truecaller.common.h.u)localObject4, str);
      localObject4 = "Participant.buildFromAdd…per.getDefaultSimToken())";
      c.g.b.k.a(localObject3, (String)localObject4);
      if (parami != null)
      {
        localObject3 = ((Participant)localObject3).l();
        localObject4 = (Long)c.a.m.e(d);
        if (localObject4 != null)
        {
          localObject4 = (Number)localObject4;
          long l2 = ((Number)localObject4).longValue();
          ((Participant.a)localObject3).c(l2);
        }
        localObject4 = e;
        if (localObject4 != null) {
          ((Participant.a)localObject3).g((String)localObject4);
        }
        localObject4 = (String)c.a.m.e(c);
        if (localObject4 != null) {
          ((Participant.a)localObject3).f((String)localObject4);
        }
        parami = g;
        ((Participant.a)localObject3).d(parami);
        parami = ((Participant.a)localObject3).a();
        c.g.b.k.a(parami, "build()");
        localObject3 = "with(participant.buildUp…build()\n                }";
        c.g.b.k.a(parami, (String)localObject3);
      }
      else
      {
        parami = (i)localObject3;
      }
      localObject3 = z;
      bool2 = ((t.c)localObject3).e();
      if (bool2)
      {
        localObject1 = z.h();
        bool1 = ((List)localObject1).contains(parami);
        if (bool1)
        {
          z.a(parami);
          return;
        }
        localObject1 = z;
        parami = c.a.m.a(parami);
        ((t.c)localObject1).b(parami);
        return;
      }
    }
    Object localObject3 = A;
    boolean bool2 = localObject3 instanceof t.f.c;
    if (bool2)
    {
      a(parami, (Long)localObject2);
      return;
    }
    if (parami != null) {
      localObject1 = c.a.m.a(parami);
    }
    a((Long)localObject2, (List)localObject1);
  }
  
  public final void a(i parami, int paramInt)
  {
    Object localObject1 = (t.i)b;
    if (localObject1 != null) {
      ((t.i)localObject1).d();
    }
    localObject1 = null;
    if (parami == null)
    {
      parami = (t.i)b;
      if (parami != null)
      {
        paramInt = 14;
        t.i.a.a(parami, false, false, paramInt);
      }
      parami = (t.i)b;
      if (parami != null) {
        parami.l();
      }
      return;
    }
    t.i locali = (t.i)b;
    label116:
    Object localObject2;
    if (locali != null)
    {
      boolean bool1 = a;
      boolean bool2 = true;
      if (bool1)
      {
        str1 = i;
        if (str1 == null)
        {
          bool1 = true;
          break label116;
        }
      }
      bool1 = false;
      String str1 = null;
      o localo = v;
      int i1 = 2131886720;
      Object[] arrayOfObject = new Object[bool2];
      String str2 = parami.a();
      arrayOfObject[0] = str2;
      localObject1 = localo.a(i1, arrayOfObject);
      localObject2 = Integer.valueOf(paramInt);
      locali.a(bool2, bool1, (String)localObject1, (Integer)localObject2);
    }
    paramInt = a;
    if (paramInt != 0) {
      localObject2 = NewConversationPresenter.SendType.IM;
    } else {
      localObject2 = NewConversationPresenter.SendType.SMS;
    }
    i = ((NewConversationPresenter.SendType)localObject2);
    boolean bool3 = a;
    a(bool3);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "text");
    d = paramString;
    Object localObject1 = g;
    if (localObject1 != null) {
      ((bn)localObject1).n();
    }
    g = null;
    localObject1 = (t.i)b;
    if (localObject1 == null) {
      return;
    }
    c(paramString);
    t.i.a.a((t.i)localObject1, false, false, 14);
    Object localObject2 = A;
    t.f.d locald = t.f.d.a;
    boolean bool1 = c.g.b.k.a(localObject2, locald);
    if (bool1)
    {
      localObject2 = z;
      bool1 = ((t.c)localObject2).e();
      if (!bool1)
      {
        bool2 = e(paramString);
        ((t.i)localObject1).d(bool2);
        return;
      }
    }
    paramString = A;
    localObject2 = t.f.d.a;
    boolean bool2 = c.g.b.k.a(paramString, localObject2) ^ true;
    if (bool2) {
      ((t.i)localObject1).l();
    }
  }
  
  public final void b(Bundle paramBundle)
  {
    if (paramBundle != null)
    {
      String str = "newconversation_copied_entities";
      paramBundle = paramBundle.getParcelableArrayList(str);
    }
    else
    {
      paramBundle = null;
    }
    j = paramBundle;
  }
  
  public final boolean b(String paramString)
  {
    c.g.b.k.b(paramString, "text");
    Object localObject = A;
    t.f.d locald = t.f.d.a;
    boolean bool1 = c.g.b.k.a(localObject, locald);
    boolean bool2 = true;
    bool1 ^= bool2;
    if (!bool1)
    {
      localObject = z;
      bool1 = ((t.c)localObject).e();
      if (!bool1)
      {
        localObject = (t.i)b;
        if (localObject == null) {
          return false;
        }
        boolean bool3 = e(paramString);
        if (!bool3)
        {
          ((t.i)localObject).b(2131886723);
          return false;
        }
        d(paramString);
        return bool2;
      }
    }
    return false;
  }
  
  public final void e()
  {
    Object localObject = y;
    List localList = z.h();
    ((t.a)localObject).a(localList);
    localObject = (t.i)b;
    if (localObject != null)
    {
      ((t.i)localObject).d();
      return;
    }
  }
  
  public final void f()
  {
    t.i locali = (t.i)b;
    if (locali == null) {
      return;
    }
    boolean bool = e;
    if (bool)
    {
      e = false;
      locali.a("");
      locali.e(true);
      locali.f(false);
      return;
    }
    locali.q();
  }
  
  public final void g()
  {
    Object localObject = y.a();
    if (localObject != null)
    {
      a((i)localObject);
      return;
    }
    localObject = z;
    boolean bool = ((t.c)localObject).e();
    if (bool)
    {
      localObject = z.h();
      a(null, (List)localObject);
      return;
    }
    localObject = d;
    d((String)localObject);
  }
  
  public final void h()
  {
    Object localObject1 = y.a();
    if (localObject1 != null)
    {
      boolean bool1 = a;
      boolean bool2 = false;
      if (bool1)
      {
        localObject2 = i;
        if (localObject2 == null)
        {
          bool1 = true;
          break label44;
        }
      }
      bool1 = false;
      Object localObject2 = null;
      label44:
      if (!bool1) {
        localObject1 = null;
      }
      if (localObject1 != null)
      {
        localObject1 = i;
        localObject2 = NewConversationPresenter.SendType.IM;
        int i2;
        int i3;
        int i5;
        int i1;
        if (localObject1 == localObject2)
        {
          localObject1 = (t.i)b;
          if (localObject1 != null)
          {
            localObject2 = new com/truecaller/messaging/e;
            i2 = NewConversationPresenter.SendType.SMS.ordinal();
            i3 = u.a();
            Object localObject3 = u;
            int i4 = k();
            i5 = ((com.truecaller.messaging.f)localObject3).a(i4);
            localObject3 = u;
            int i6 = ((com.truecaller.messaging.f)localObject3).b(0);
            bool2 = k();
            int i7;
            if (bool2)
            {
              i1 = 2131888777;
              i7 = 2131888777;
            }
            else
            {
              i1 = 2131888778;
              i7 = 2131888778;
            }
            localObject3 = localObject2;
            ((com.truecaller.messaging.e)localObject2).<init>(i2, i3, i5, i6, i7);
            localObject2 = c.a.m.a(localObject2);
            ((t.i)localObject1).a((List)localObject2);
          }
          return;
        }
        localObject1 = (t.i)b;
        if (localObject1 != null)
        {
          com.truecaller.messaging.e locale = new com/truecaller/messaging/e;
          i1 = NewConversationPresenter.SendType.IM.ordinal();
          int i8 = u.b();
          localObject2 = u;
          i2 = 2;
          i3 = ((com.truecaller.messaging.f)localObject2).a(i2);
          i5 = u.b(i2);
          localObject2 = locale;
          i2 = i3;
          i3 = i5;
          i5 = 2131888776;
          locale.<init>(i1, i8, i2, i3, i5);
          localObject2 = c.a.m.a(locale);
          ((t.i)localObject1).a((List)localObject2);
          return;
        }
        return;
      }
    }
  }
  
  public final boolean i()
  {
    t.i locali = (t.i)b;
    boolean bool = true;
    if (locali != null)
    {
      e = bool;
      locali.e(false);
      locali.f(bool);
      locali.k();
    }
    return bool;
  }
  
  public final void j()
  {
    i locali = y.a();
    if (locali == null) {
      return;
    }
    a(locali);
  }
  
  public final void y_()
  {
    super.y_();
    y.b();
    y.a(null, null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.NewConversationPresenter
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */