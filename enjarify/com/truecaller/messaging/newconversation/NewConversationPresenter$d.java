package com.truecaller.messaging.newconversation;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import java.util.ArrayList;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.e;

final class NewConversationPresenter$d
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  NewConversationPresenter$d(NewConversationPresenter paramNewConversationPresenter, ArrayList paramArrayList, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/messaging/newconversation/NewConversationPresenter$d;
    NewConversationPresenter localNewConversationPresenter = b;
    ArrayList localArrayList = c;
    locald.<init>(localNewConversationPresenter, localArrayList, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool = paramObject instanceof o.b;
      if (bool) {
        throw a;
      }
      break;
    case 0: 
      int j = paramObject instanceof o.b;
      if (j != 0) {
        break label161;
      }
      paramObject = d;
      f localf = NewConversationPresenter.a(b);
      Object localObject = new com/truecaller/messaging/newconversation/NewConversationPresenter$d$a;
      ((NewConversationPresenter.d.a)localObject).<init>(this, null);
      localObject = (m)localObject;
      int k = 2;
      paramObject = e.a((ag)paramObject, localf, (m)localObject, k);
      j = 1;
      a = j;
      paramObject = ((ao)paramObject).a(this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    paramObject = (List)paramObject;
    NewConversationPresenter.a(b, (List)paramObject);
    return x.a;
    label161:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.NewConversationPresenter.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */