package com.truecaller.messaging.newconversation;

import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.b.v.c;
import c.n;
import c.o.b;
import c.x;
import com.truecaller.messaging.ForwardContentItem;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.Draft.a;
import com.truecaller.messaging.data.types.Participant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.e;

final class NewConversationPresenter$a
  extends c.d.b.a.k
  implements c.g.a.m
{
  Object a;
  Object b;
  long c;
  int d;
  private ag i;
  
  NewConversationPresenter$a(NewConversationPresenter paramNewConversationPresenter, Long paramLong, ArrayList paramArrayList, Participant paramParticipant, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/messaging/newconversation/NewConversationPresenter$a;
    NewConversationPresenter localNewConversationPresenter = e;
    Long localLong = f;
    ArrayList localArrayList = g;
    Participant localParticipant = h;
    locala.<init>(localNewConversationPresenter, localLong, localArrayList, localParticipant, paramc);
    paramObject = (ag)paramObject;
    i = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int j = d;
    List localList = null;
    int k;
    int m;
    Object localObject6;
    Object localObject7;
    switch (j)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      localObject1 = (v.c)b;
      localObject2 = (v.c)a;
      k = paramObject instanceof o.b;
      if (k == 0)
      {
        Object localObject3 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject3;
      }
      else
      {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label638;
      }
      paramObject = i;
      localObject2 = new c/g/b/v$c;
      ((v.c)localObject2).<init>();
      localObject4 = f;
      if (localObject4 == null) {
        break label254;
      }
      long l = ((Number)localObject4).longValue();
      f localf = NewConversationPresenter.a(e);
      Object localObject5 = new com/truecaller/messaging/newconversation/NewConversationPresenter$a$a;
      m = 0;
      localObject6 = null;
      localObject4 = localObject5;
      localObject7 = this;
      ((NewConversationPresenter.a.a)localObject5).<init>(l, null, this, (ag)paramObject);
      localObject5 = (c.g.a.m)localObject5;
      paramObject = e.a((ag)paramObject, localf, (c.g.a.m)localObject5, 2);
      a = localObject2;
      c = l;
      b = localObject2;
      k = 1;
      d = k;
      paramObject = ((ao)paramObject).a(this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      localObject1 = localObject2;
    }
    paramObject = (Conversation)paramObject;
    if (paramObject == null)
    {
      return x.a;
      label254:
      localObject1 = localObject2;
      n = 0;
      paramObject = null;
    }
    a = paramObject;
    int n = 0;
    paramObject = null;
    Object localObject2 = (Iterable)g;
    Object localObject4 = new java/util/ArrayList;
    int i1 = c.a.m.a((Iterable)localObject2, 10);
    ((ArrayList)localObject4).<init>(i1);
    localObject4 = (Collection)localObject4;
    localObject2 = ((Iterable)localObject2).iterator();
    for (;;)
    {
      boolean bool3 = ((Iterator)localObject2).hasNext();
      if (!bool3) {
        break;
      }
      Object localObject8 = (ForwardContentItem)((Iterator)localObject2).next();
      Object localObject9 = new com/truecaller/messaging/data/types/Draft$a;
      ((Draft.a)localObject9).<init>();
      localObject6 = (Conversation)a;
      if (localObject6 != null)
      {
        localObject6 = (Conversation)a;
        ((Draft.a)localObject9).a((Conversation)localObject6);
        localObject6 = a).l;
        localObject6 = ((Draft.a)localObject9).a((Participant[])localObject6);
        localObject7 = "addParticipants(conversation.participants)";
        c.g.b.k.a(localObject6, (String)localObject7);
      }
      else
      {
        localObject6 = h;
        if (localObject6 != null) {
          ((Draft.a)localObject9).a((Participant)localObject6);
        }
      }
      localObject6 = a;
      ((Draft.a)localObject9).a((String)localObject6);
      localObject9 = ((Draft.a)localObject9).d();
      c.g.b.k.a(localObject9, "build()");
      localObject6 = "Draft.Builder().run {\n  …build()\n                }";
      c.g.b.k.a(localObject9, (String)localObject6);
      localObject8 = b;
      if (localObject8 != null)
      {
        localObject8 = NewConversationPresenter.h(e);
        if (localObject8 != null)
        {
          localObject8 = (List)localObject8;
          m = n + 1;
          paramObject = (BinaryEntity)c.a.m.a((List)localObject8, n);
          break label542;
        }
      }
      m = n;
      n = 0;
      paramObject = null;
      label542:
      localObject8 = new c/n;
      paramObject = c.a.m.b(paramObject);
      ((n)localObject8).<init>(localObject9, paramObject);
      ((Collection)localObject4).add(localObject8);
      n = m;
    }
    localObject4 = (List)localObject4;
    paramObject = e;
    boolean bool2 = NewConversationPresenter.b((NewConversationPresenter)paramObject, (List)localObject4);
    if (bool2)
    {
      paramObject = e;
      localObject1 = h;
      if (localObject1 != null) {
        localList = c.a.m.a(localObject1);
      }
      localObject1 = f;
      NewConversationPresenter.a((NewConversationPresenter)paramObject, (Long)localObject1, localList);
    }
    return x.a;
    label638:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.NewConversationPresenter.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */