package com.truecaller.messaging.newconversation;

import android.content.Context;
import c.g.b.k;
import com.truecaller.analytics.ap;
import com.truecaller.analytics.aq;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.f;

public final class n
{
  final Context a;
  final t.f b;
  
  public n(Context paramContext, t.f paramf)
  {
    a = paramContext;
    b = paramf;
  }
  
  public static ap a(f paramf, b paramb)
  {
    k.b(paramf, "eventsTracker");
    k.b(paramb, "analytics");
    aq localaq = new com/truecaller/analytics/aq;
    localaq.<init>(paramf, paramb);
    return (ap)localaq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */