package com.truecaller.messaging.newconversation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import c.a.m;
import c.u;
import com.truecaller.adapter_delegates.p;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.messaging.conversation.ConversationActivity;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.newconversation.a.c;
import com.truecaller.tcpermissions.l;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.components.FloatingActionButton;
import com.truecaller.ui.components.FloatingActionButton.a;
import com.truecaller.ui.q;
import com.truecaller.util.at;
import com.truecaller.utils.extensions.t;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class j
  extends Fragment
  implements t.d, t.i, FloatingActionButton.a
{
  public static final j.a e;
  public t.g a;
  public t.a b;
  public t.c c;
  public l d;
  private p f;
  private com.truecaller.adapter_delegates.f g;
  private b h;
  private RecyclerView i;
  private Toolbar j;
  private EditText k;
  private RecyclerView l;
  private View m;
  private View n;
  private View o;
  private TextView p;
  private FloatingActionButton q;
  private View r;
  private TextView s;
  private TextView t;
  private MenuItem u;
  private MenuItem v;
  private HashMap w;
  
  static
  {
    j.a locala = new com/truecaller/messaging/newconversation/j$a;
    locala.<init>((byte)0);
    e = locala;
  }
  
  public static final Intent a(Context paramContext, ArrayList paramArrayList)
  {
    c.g.b.k.b(paramContext, "context");
    c.g.b.k.b(paramArrayList, "participants");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, NewConversationActivity.class);
    paramArrayList = (Serializable)paramArrayList;
    localIntent.putExtra("PRE_FILL_PARTICIPANTS", paramArrayList);
    return localIntent;
  }
  
  public static final List a(Intent paramIntent)
  {
    return j.a.a(paramIntent);
  }
  
  public final void a()
  {
    Object localObject = q;
    String str;
    if (localObject == null)
    {
      str = "fabContainer";
      c.g.b.k.a(str);
    }
    boolean bool = ((FloatingActionButton)localObject).a();
    if (bool)
    {
      localObject = q;
      if (localObject == null)
      {
        str = "fabContainer";
        c.g.b.k.a(str);
      }
      ((FloatingActionButton)localObject).c();
      return;
    }
    localObject = a;
    if (localObject == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    ((t.g)localObject).g();
  }
  
  public final void a(int paramInt)
  {
    t.g localg = a;
    if (localg == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    localg.a(paramInt);
  }
  
  public final void a(int paramInt1, int paramInt2, int paramInt3)
  {
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = ((Context)localObject1).getResources();
    localObject1 = ((Context)localObject1).getTheme();
    Drawable localDrawable = android.support.v4.content.b.f.a((Resources)localObject2, paramInt1, (Resources.Theme)localObject1);
    if (localDrawable == null) {
      return;
    }
    localObject1 = q;
    if (localObject1 == null)
    {
      localObject2 = "fabContainer";
      c.g.b.k.a((String)localObject2);
    }
    t.a((View)localObject1);
    localObject1 = q;
    if (localObject1 == null)
    {
      localObject2 = "fabContainer";
      c.g.b.k.a((String)localObject2);
    }
    ((FloatingActionButton)localObject1).setBackgroundColor(paramInt3);
    localDrawable = android.support.v4.graphics.drawable.a.e(localDrawable).mutate();
    android.support.v4.graphics.drawable.a.a(localDrawable, paramInt2);
    FloatingActionButton localFloatingActionButton = q;
    if (localFloatingActionButton == null)
    {
      String str = "fabContainer";
      c.g.b.k.a(str);
    }
    localFloatingActionButton.setDrawable(localDrawable);
  }
  
  public final void a(long paramLong1, long paramLong2, int paramInt)
  {
    com.truecaller.messaging.g.k.a((Fragment)this, paramLong1, paramLong2, paramInt, 2131886477);
  }
  
  public final void a(Integer paramInteger)
  {
    Toolbar localToolbar = j;
    if (localToolbar == null)
    {
      String str = "toolbar";
      c.g.b.k.a(str);
    }
    int i1;
    if (paramInteger != null)
    {
      i1 = ((Number)paramInteger).intValue();
      paramInteger = getString(i1);
    }
    else
    {
      i1 = 0;
      paramInteger = null;
    }
    paramInteger = (CharSequence)paramInteger;
    localToolbar.setTitle(paramInteger);
  }
  
  public final void a(Long paramLong, Participant[] paramArrayOfParticipant, Intent paramIntent)
  {
    Intent localIntent = new android/content/Intent;
    Context localContext = getContext();
    Class localClass = ConversationActivity.class;
    localIntent.<init>(localContext, localClass);
    if (paramLong != null)
    {
      long l1 = ((Number)paramLong).longValue();
      paramLong = "conversation_id";
      localIntent.putExtra(paramLong, l1);
    }
    if (paramArrayOfParticipant != null)
    {
      paramLong = "participants";
      paramArrayOfParticipant = (Parcelable[])paramArrayOfParticipant;
      localIntent.putExtra(paramLong, paramArrayOfParticipant);
    }
    if (paramIntent != null)
    {
      paramLong = "send_intent";
      paramArrayOfParticipant = paramIntent;
      paramArrayOfParticipant = (Parcelable)paramIntent;
      localIntent.putExtra(paramLong, paramArrayOfParticipant);
    }
    if (paramIntent != null) {
      com.truecaller.common.h.o.a(paramIntent, localIntent);
    }
    startActivity(localIntent);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "text");
    EditText localEditText = k;
    if (localEditText == null)
    {
      String str = "searchText";
      c.g.b.k.a(str);
    }
    paramString = (CharSequence)paramString;
    localEditText.setText(paramString);
  }
  
  public final void a(ArrayList paramArrayList)
  {
    c.g.b.k.b(paramArrayList, "participants");
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      Intent localIntent = new android/content/Intent;
      localIntent.<init>();
      paramArrayList = (Serializable)paramArrayList;
      paramArrayList = localIntent.putExtra("RESULT_PARTICIPANTS", paramArrayList);
      localf.setResult(-1, paramArrayList);
      return;
    }
  }
  
  public final void a(List paramList)
  {
    c.g.b.k.b(paramList, "items");
    paramList = (Iterable)paramList;
    Object localObject1 = new java/util/ArrayList;
    int i1 = m.a(paramList, 10);
    ((ArrayList)localObject1).<init>(i1);
    localObject1 = (Collection)localObject1;
    paramList = paramList.iterator();
    Object localObject2;
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      localObject2 = com.truecaller.messaging.i.k.a((com.truecaller.messaging.e)paramList.next());
      ((Collection)localObject1).add(localObject2);
    }
    localObject1 = (Collection)localObject1;
    paramList = new com.truecaller.ui.components.i[0];
    paramList = ((Collection)localObject1).toArray(paramList);
    if (paramList != null)
    {
      paramList = (com.truecaller.ui.components.i[])paramList;
      localObject1 = q;
      if (localObject1 == null)
      {
        localObject2 = "fabContainer";
        c.g.b.k.a((String)localObject2);
      }
      ((FloatingActionButton)localObject1).setMenuItems(paramList);
      paramList = q;
      if (paramList == null)
      {
        localObject1 = "fabContainer";
        c.g.b.k.a((String)localObject1);
      }
      paramList.b();
      return;
    }
    paramList = new c/u;
    paramList.<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw paramList;
  }
  
  public final void a(boolean paramBoolean)
  {
    MenuItem localMenuItem = u;
    if (localMenuItem == null)
    {
      String str = "groupModeMenuItem";
      c.g.b.k.a(str);
    }
    localMenuItem.setVisible(paramBoolean);
  }
  
  public final void a(boolean paramBoolean1, boolean paramBoolean2, String paramString, Integer paramInteger)
  {
    Object localObject1 = r;
    if (localObject1 == null)
    {
      String str = "bottomContainer";
      c.g.b.k.a(str);
    }
    t.a((View)localObject1, paramBoolean1);
    Object localObject2 = t;
    if (localObject2 == null)
    {
      localObject1 = "forwardDestinationSubtextView";
      c.g.b.k.a((String)localObject1);
    }
    localObject2 = (View)localObject2;
    t.a((View)localObject2, paramBoolean2);
    Object localObject3;
    if (paramString != null)
    {
      localObject2 = s;
      if (localObject2 == null)
      {
        localObject3 = "forwardDestinationTextView";
        c.g.b.k.a((String)localObject3);
      }
      paramString = (CharSequence)paramString;
      ((TextView)localObject2).setText(paramString);
    }
    if (paramInteger != null)
    {
      paramInteger = (Number)paramInteger;
      paramBoolean1 = paramInteger.intValue();
      localObject3 = l;
      if (localObject3 == null)
      {
        paramString = "recyclerView";
        c.g.b.k.a(paramString);
      }
      ((RecyclerView)localObject3).scrollToPosition(paramBoolean1);
      return;
    }
  }
  
  public final void a(Participant[] paramArrayOfParticipant)
  {
    c.g.b.k.b(paramArrayOfParticipant, "participants");
    Object localObject1 = getActivity();
    if (localObject1 == null) {
      return;
    }
    c.g.b.k.a(localObject1, "activity ?: return");
    localObject1 = ((android.support.v4.app.f)localObject1).getSupportFragmentManager().a();
    Object localObject2 = c.c;
    c.g.b.k.b(paramArrayOfParticipant, "participants");
    localObject2 = new com/truecaller/messaging/newconversation/a/c;
    ((c)localObject2).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramArrayOfParticipant = (Parcelable[])paramArrayOfParticipant;
    localBundle.putParcelableArray("participants", paramArrayOfParticipant);
    localBundle.putString("im_group_mode", "im_group_mode_create");
    ((c)localObject2).setArguments(localBundle);
    localObject2 = (Fragment)localObject2;
    ((android.support.v4.app.o)localObject1).a(16908290, (Fragment)localObject2).a(null).c();
  }
  
  public final t.g b()
  {
    t.g localg = a;
    if (localg == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    return localg;
  }
  
  public final void b(int paramInt)
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    Toast.makeText(localContext, paramInt, 0).show();
  }
  
  public final void b(boolean paramBoolean)
  {
    View localView = m;
    if (localView == null)
    {
      String str = "newImGroupView";
      c.g.b.k.a(str);
    }
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localView.setVisibility(paramBoolean);
  }
  
  public final t.c c()
  {
    t.c localc = c;
    if (localc == null)
    {
      String str = "groupPresenter";
      c.g.b.k.a(str);
    }
    return localc;
  }
  
  public final void c(int paramInt)
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    Object[] arrayOfObject = new Object[1];
    Object localObject = Integer.valueOf(paramInt);
    arrayOfObject[0] = localObject;
    localObject = (CharSequence)getString(2131886724, arrayOfObject);
    Toast.makeText(localContext, (CharSequence)localObject, 0).show();
  }
  
  public final void c(boolean paramBoolean)
  {
    TextView localTextView = p;
    if (localTextView == null)
    {
      String str = "emptyGroupView";
      c.g.b.k.a(str);
    }
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localTextView.setVisibility(paramBoolean);
  }
  
  public final void d()
  {
    com.truecaller.adapter_delegates.f localf = g;
    if (localf == null)
    {
      String str = "adapter";
      c.g.b.k.a(str);
    }
    localf.notifyDataSetChanged();
  }
  
  public final void d(int paramInt)
  {
    RecyclerView localRecyclerView = i;
    if (localRecyclerView == null)
    {
      String str = "groupParticipantsRecyclerView";
      c.g.b.k.a(str);
    }
    localRecyclerView.scrollToPosition(paramInt);
  }
  
  public final void d(boolean paramBoolean)
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    Object localObject = q;
    String str;
    if (localObject == null)
    {
      str = "fabContainer";
      c.g.b.k.a(str);
    }
    int i1;
    if (paramBoolean)
    {
      i1 = 0;
      str = null;
    }
    else
    {
      i1 = 8;
    }
    ((FloatingActionButton)localObject).setVisibility(i1);
    if (paramBoolean)
    {
      FloatingActionButton localFloatingActionButton = q;
      if (localFloatingActionButton == null)
      {
        localObject = "fabContainer";
        c.g.b.k.a((String)localObject);
      }
      localObject = localContext.getResources();
      i1 = 2131233836;
      Resources.Theme localTheme = localContext.getTheme();
      localObject = android.support.v4.content.b.f.a((Resources)localObject, i1, localTheme);
      localFloatingActionButton.setDrawable((Drawable)localObject);
      localFloatingActionButton = q;
      if (localFloatingActionButton == null)
      {
        localObject = "fabContainer";
        c.g.b.k.a((String)localObject);
      }
      int i2 = 2130969528;
      int i3 = com.truecaller.utils.ui.b.a(localContext, i2);
      localFloatingActionButton.setBackgroundColor(i3);
    }
  }
  
  public final void e()
  {
    Object localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      c.g.b.k.a((String)localObject2);
    }
    Object localObject2 = k;
    if (localObject2 == null)
    {
      String str = "searchText";
      c.g.b.k.a(str);
    }
    localObject2 = ((EditText)localObject2).getText().toString();
    ((t.g)localObject1).a((String)localObject2);
    boolean bool = true;
    e(bool);
    localObject1 = k;
    if (localObject1 == null)
    {
      localObject2 = "searchText";
      c.g.b.k.a((String)localObject2);
    }
    ((EditText)localObject1).setVisibility(8);
  }
  
  public final void e(boolean paramBoolean)
  {
    MenuItem localMenuItem = v;
    if (localMenuItem == null)
    {
      String str = "searchModeMenuItem";
      c.g.b.k.a(str);
    }
    localMenuItem.setVisible(paramBoolean);
  }
  
  public final void f()
  {
    t.g localg = a;
    if (localg == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    localg.e();
  }
  
  public final void f(boolean paramBoolean)
  {
    EditText localEditText = k;
    if (localEditText == null)
    {
      String str = "searchText";
      c.g.b.k.a(str);
    }
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localEditText.setVisibility(paramBoolean);
  }
  
  public final void g()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void h()
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    TruecallerInit.b(localContext, "messages", "newConversation");
  }
  
  public final void i()
  {
    View localView = o;
    if (localView == null)
    {
      String str = "groupView";
      c.g.b.k.a(str);
    }
    localView.setVisibility(0);
  }
  
  public final void j()
  {
    b localb = h;
    if (localb == null)
    {
      String str = "groupParticipantAdapter";
      c.g.b.k.a(str);
    }
    localb.notifyDataSetChanged();
  }
  
  public final void k()
  {
    EditText localEditText = k;
    String str;
    if (localEditText == null)
    {
      str = "searchText";
      c.g.b.k.a(str);
    }
    localEditText.requestFocus();
    localEditText = k;
    if (localEditText == null)
    {
      str = "searchText";
      c.g.b.k.a(str);
    }
    t.a((View)localEditText, true, 0L);
  }
  
  public final void l()
  {
    FloatingActionButton localFloatingActionButton = q;
    if (localFloatingActionButton == null)
    {
      String str = "fabContainer";
      c.g.b.k.a(str);
    }
    t.b((View)localFloatingActionButton);
  }
  
  public final void m()
  {
    com.truecaller.messaging.g.k.a((Fragment)this, "forwardMessages");
  }
  
  public final void n()
  {
    Object localObject = com.truecaller.messaging.g.k.a;
    localObject = this;
    localObject = (Fragment)this;
    l locall = d;
    if (locall == null)
    {
      String str = "tcPermissionsUtil";
      c.g.b.k.a(str);
    }
    com.truecaller.messaging.g.k.a((Fragment)localObject, locall);
  }
  
  public final void o()
  {
    FloatingActionButton localFloatingActionButton = q;
    if (localFloatingActionButton == null)
    {
      String str = "fabContainer";
      c.g.b.k.a(str);
    }
    localFloatingActionButton.c();
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    int i1 = -1;
    if (paramInt2 != i1) {
      return;
    }
    paramInt2 = 200;
    if (paramInt1 == paramInt2)
    {
      t.g localg = a;
      if (localg == null)
      {
        String str = "presenter";
        c.g.b.k.a(str);
      }
      localg.j();
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getActivity();
    if (paramBundle != null)
    {
      paramBundle = (Activity)paramBundle;
      Object localObject1 = paramBundle.getIntent();
      if (localObject1 == null) {
        return;
      }
      Object localObject2 = a.a();
      Object localObject3 = paramBundle.getApplicationContext();
      if (localObject3 != null)
      {
        localObject3 = ((bk)localObject3).a();
        localObject2 = ((a.a)localObject2).a((bp)localObject3);
        localObject3 = new com/truecaller/messaging/newconversation/n;
        paramBundle = (Context)paramBundle;
        c.g.b.k.b(localObject1, "intent");
        Object localObject4 = "send_intent";
        boolean bool1 = ((Intent)localObject1).hasExtra((String)localObject4);
        Object localObject5 = null;
        if (bool1)
        {
          localObject4 = "send_intent";
          localObject1 = (Intent)((Intent)localObject1).getParcelableExtra((String)localObject4);
          if (localObject1 != null)
          {
            localObject5 = new com/truecaller/messaging/newconversation/t$f$b;
            ((t.f.b)localObject5).<init>((Intent)localObject1);
          }
          localObject5 = (t.f)localObject5;
        }
        else
        {
          localObject4 = "forward_content";
          bool1 = ((Intent)localObject1).hasExtra((String)localObject4);
          if (bool1)
          {
            localObject4 = "forward_content";
            localObject1 = ((Intent)localObject1).getParcelableArrayListExtra((String)localObject4);
            if (localObject1 != null)
            {
              bool1 = ((ArrayList)localObject1).isEmpty();
              if (bool1)
              {
                bool2 = false;
                localObject1 = null;
              }
              if (localObject1 != null)
              {
                localObject5 = new com/truecaller/messaging/newconversation/t$f$c;
                ((t.f.c)localObject5).<init>((ArrayList)localObject1);
              }
            }
            localObject5 = (t.f)localObject5;
          }
          else
          {
            localObject4 = "im_group_info";
            bool1 = ((Intent)localObject1).hasExtra((String)localObject4);
            if (bool1)
            {
              localObject4 = "im_group_info";
              localObject1 = (ImGroupInfo)((Intent)localObject1).getParcelableExtra((String)localObject4);
              if (localObject1 != null)
              {
                localObject5 = new com/truecaller/messaging/newconversation/t$f$a;
                ((t.f.a)localObject5).<init>((ImGroupInfo)localObject1);
              }
              localObject5 = (t.f)localObject5;
            }
          }
        }
        if (localObject5 == null)
        {
          localObject1 = t.f.d.a;
          localObject5 = localObject1;
          localObject5 = (t.f)localObject1;
        }
        ((n)localObject3).<init>(paramBundle, (t.f)localObject5);
        ((a.a)localObject2).a((n)localObject3).a().a(this);
        paramBundle = new com/truecaller/adapter_delegates/p;
        localObject1 = b;
        if (localObject1 == null)
        {
          localObject2 = "adapterPresenter";
          c.g.b.k.a((String)localObject2);
        }
        localObject1 = (com.truecaller.adapter_delegates.b)localObject1;
        int i1 = 2131558883;
        localObject3 = new com/truecaller/messaging/newconversation/j$b;
        ((j.b)localObject3).<init>(this);
        localObject3 = (c.g.a.b)localObject3;
        localObject4 = (c.g.a.b)j.c.a;
        paramBundle.<init>((com.truecaller.adapter_delegates.b)localObject1, i1, (c.g.a.b)localObject3, (c.g.a.b)localObject4);
        f = paramBundle;
        paramBundle = new com/truecaller/adapter_delegates/f;
        localObject1 = f;
        if (localObject1 == null)
        {
          localObject2 = "adapterDelegate";
          c.g.b.k.a((String)localObject2);
        }
        localObject1 = (com.truecaller.adapter_delegates.a)localObject1;
        paramBundle.<init>((com.truecaller.adapter_delegates.a)localObject1);
        boolean bool2 = true;
        paramBundle.setHasStableIds(bool2);
        g = paramBundle;
        paramBundle = new com/truecaller/messaging/newconversation/b;
        localObject1 = c;
        if (localObject1 == null)
        {
          localObject2 = "groupPresenter";
          c.g.b.k.a((String)localObject2);
        }
        paramBundle.<init>((t.c)localObject1);
        h = paramBundle;
        return;
      }
      paramBundle = new c/u;
      paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
      throw paramBundle;
    }
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558733, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    Object localObject = a;
    String str;
    if (localObject == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    ((t.g)localObject).y_();
    localObject = c;
    if (localObject == null)
    {
      str = "groupPresenter";
      c.g.b.k.a(str);
    }
    ((t.c)localObject).y_();
    super.onDestroy();
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    c.g.b.k.b(paramArrayOfString, "permissions");
    String str = "grantResults";
    c.g.b.k.b(paramArrayOfInt, str);
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    com.truecaller.wizard.utils.i.a(paramArrayOfString, paramArrayOfInt);
    int i1 = 200;
    if (paramInt == i1)
    {
      paramInt = paramArrayOfInt.length;
      i1 = 0;
      paramArrayOfString = null;
      int i2 = 0;
      str = null;
      for (;;)
      {
        int i3 = 1;
        if (i2 >= paramInt) {
          break;
        }
        int i4 = paramArrayOfInt[i2];
        if (i4 != 0) {
          i3 = 0;
        }
        if (i3 == 0) {
          break label101;
        }
        i2 += 1;
      }
      i1 = 1;
      label101:
      if (i1 != 0)
      {
        t.g localg = a;
        if (localg == null)
        {
          paramArrayOfString = "presenter";
          c.g.b.k.a(paramArrayOfString);
        }
        localg.j();
      }
    }
  }
  
  public final void onResume()
  {
    super.onResume();
    t.g localg = a;
    if (localg == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    localg.a();
  }
  
  public final void onSaveInstanceState(Bundle paramBundle)
  {
    c.g.b.k.b(paramBundle, "outState");
    super.onSaveInstanceState(paramBundle);
    Object localObject = a;
    String str;
    if (localObject == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    ((t.g)localObject).a(paramBundle);
    localObject = c;
    if (localObject == null)
    {
      str = "groupPresenter";
      c.g.b.k.a(str);
    }
    ((t.c)localObject).b(paramBundle);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    c.g.b.k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    Object localObject1 = (AppCompatActivity)getActivity();
    if (localObject1 == null) {
      return;
    }
    int i1 = 2131364907;
    Object localObject2 = paramView.findViewById(i1);
    Object localObject3 = "view.findViewById(R.id.toolbar)";
    c.g.b.k.a(localObject2, (String)localObject3);
    localObject2 = (Toolbar)localObject2;
    j = ((Toolbar)localObject2);
    localObject2 = j;
    if (localObject2 == null)
    {
      localObject3 = "toolbar";
      c.g.b.k.a((String)localObject3);
    }
    localObject3 = new com/truecaller/messaging/newconversation/j$d;
    ((j.d)localObject3).<init>(this);
    localObject3 = (View.OnClickListener)localObject3;
    ((Toolbar)localObject2).setNavigationOnClickListener((View.OnClickListener)localObject3);
    localObject2 = j;
    if (localObject2 == null)
    {
      localObject3 = "toolbar";
      c.g.b.k.a((String)localObject3);
    }
    int i2 = 2131623970;
    ((Toolbar)localObject2).inflateMenu(i2);
    localObject2 = j;
    if (localObject2 == null)
    {
      localObject3 = "toolbar";
      c.g.b.k.a((String)localObject3);
    }
    localObject2 = ((Toolbar)localObject2).getMenu();
    c.g.b.k.a(localObject2, "toolbar.menu");
    localObject3 = ((Menu)localObject2).findItem(2131361869);
    Object localObject4 = "menu.findItem(R.id.action_group_mode)";
    c.g.b.k.a(localObject3, (String)localObject4);
    u = ((MenuItem)localObject3);
    i2 = 2131361932;
    localObject2 = ((Menu)localObject2).findItem(i2);
    localObject3 = "menu.findItem(R.id.action_search)";
    c.g.b.k.a(localObject2, (String)localObject3);
    v = ((MenuItem)localObject2);
    localObject2 = u;
    if (localObject2 == null)
    {
      localObject3 = "groupModeMenuItem";
      c.g.b.k.a((String)localObject3);
    }
    localObject3 = new com/truecaller/messaging/newconversation/j$e;
    ((j.e)localObject3).<init>(this);
    localObject3 = (MenuItem.OnMenuItemClickListener)localObject3;
    ((MenuItem)localObject2).setOnMenuItemClickListener((MenuItem.OnMenuItemClickListener)localObject3);
    localObject2 = v;
    if (localObject2 == null)
    {
      localObject3 = "searchModeMenuItem";
      c.g.b.k.a((String)localObject3);
    }
    localObject3 = new com/truecaller/messaging/newconversation/j$f;
    ((j.f)localObject3).<init>(this);
    localObject3 = (MenuItem.OnMenuItemClickListener)localObject3;
    ((MenuItem)localObject2).setOnMenuItemClickListener((MenuItem.OnMenuItemClickListener)localObject3);
    localObject2 = paramView.findViewById(2131363799);
    c.g.b.k.a(localObject2, "view.findViewById(R.id.new_im_group_view)");
    m = ((View)localObject2);
    i1 = 2131363770;
    localObject2 = paramView.findViewById(i1);
    localObject3 = "view.findViewById(R.id.mms_switch_view)";
    c.g.b.k.a(localObject2, (String)localObject3);
    n = ((View)localObject2);
    localObject2 = m;
    if (localObject2 == null)
    {
      localObject3 = "newImGroupView";
      c.g.b.k.a((String)localObject3);
    }
    localObject3 = new com/truecaller/messaging/newconversation/j$g;
    ((j.g)localObject3).<init>(this);
    localObject3 = (View.OnClickListener)localObject3;
    ((View)localObject2).setOnClickListener((View.OnClickListener)localObject3);
    localObject2 = n;
    if (localObject2 == null)
    {
      localObject3 = "switchToNewMmsGroupView";
      c.g.b.k.a((String)localObject3);
    }
    localObject3 = new com/truecaller/messaging/newconversation/j$h;
    ((j.h)localObject3).<init>(this);
    localObject3 = (View.OnClickListener)localObject3;
    ((View)localObject2).setOnClickListener((View.OnClickListener)localObject3);
    localObject2 = localObject1;
    localObject2 = (Context)localObject1;
    localObject3 = j;
    if (localObject3 == null)
    {
      localObject4 = "toolbar";
      c.g.b.k.a((String)localObject4);
    }
    at.a((Context)localObject2, (Toolbar)localObject3);
    localObject1 = ((AppCompatActivity)localObject1).getSupportActionBar();
    i2 = 0;
    localObject3 = null;
    if (localObject1 != null)
    {
      bool = true;
      ((ActionBar)localObject1).setDisplayHomeAsUpEnabled(bool);
      ((ActionBar)localObject1).setDisplayShowTitleEnabled(false);
    }
    localObject1 = paramView.findViewById(2131364268);
    c.g.b.k.a(localObject1, "view.findViewById(R.id.search_text)");
    localObject1 = (EditText)localObject1;
    k = ((EditText)localObject1);
    int i3 = 2131364093;
    localObject1 = paramView.findViewById(i3);
    localObject4 = "view.findViewById(R.id.recycler_view)";
    c.g.b.k.a(localObject1, (String)localObject4);
    localObject1 = (RecyclerView)localObject1;
    l = ((RecyclerView)localObject1);
    localObject1 = l;
    if (localObject1 == null)
    {
      localObject4 = "recyclerView";
      c.g.b.k.a((String)localObject4);
    }
    boolean bool = false;
    localObject4 = null;
    ((RecyclerView)localObject1).setItemAnimator(null);
    localObject1 = l;
    if (localObject1 == null)
    {
      localObject5 = "recyclerView";
      c.g.b.k.a((String)localObject5);
    }
    Object localObject5 = new com/truecaller/ui/q;
    int i4 = 2131559209;
    ((q)localObject5).<init>((Context)localObject2, i4);
    localObject5 = (RecyclerView.ItemDecoration)localObject5;
    ((RecyclerView)localObject1).addItemDecoration((RecyclerView.ItemDecoration)localObject5);
    localObject1 = l;
    if (localObject1 == null)
    {
      localObject5 = "recyclerView";
      c.g.b.k.a((String)localObject5);
    }
    localObject5 = new android/support/v7/widget/LinearLayoutManager;
    ((LinearLayoutManager)localObject5).<init>((Context)localObject2);
    localObject5 = (RecyclerView.LayoutManager)localObject5;
    ((RecyclerView)localObject1).setLayoutManager((RecyclerView.LayoutManager)localObject5);
    localObject1 = l;
    if (localObject1 == null)
    {
      localObject2 = "recyclerView";
      c.g.b.k.a((String)localObject2);
    }
    localObject2 = g;
    if (localObject2 == null)
    {
      localObject5 = "adapter";
      c.g.b.k.a((String)localObject5);
    }
    localObject2 = (RecyclerView.Adapter)localObject2;
    ((RecyclerView)localObject1).setAdapter((RecyclerView.Adapter)localObject2);
    localObject1 = k;
    if (localObject1 == null)
    {
      localObject2 = "searchText";
      c.g.b.k.a((String)localObject2);
    }
    localObject2 = new com/truecaller/messaging/newconversation/j$i;
    ((j.i)localObject2).<init>(this);
    localObject2 = (TextWatcher)localObject2;
    ((EditText)localObject1).addTextChangedListener((TextWatcher)localObject2);
    localObject1 = k;
    if (localObject1 == null)
    {
      localObject2 = "searchText";
      c.g.b.k.a((String)localObject2);
    }
    localObject2 = new com/truecaller/messaging/newconversation/j$j;
    ((j.j)localObject2).<init>(this);
    localObject2 = (TextView.OnEditorActionListener)localObject2;
    ((EditText)localObject1).setOnEditorActionListener((TextView.OnEditorActionListener)localObject2);
    i3 = 2131363219;
    localObject1 = paramView.findViewById(i3);
    localObject2 = "view.findViewById(R.id.g…rticipants_recycler_view)";
    c.g.b.k.a(localObject1, (String)localObject2);
    localObject1 = (RecyclerView)localObject1;
    i = ((RecyclerView)localObject1);
    localObject1 = i;
    if (localObject1 == null)
    {
      localObject2 = "groupParticipantsRecyclerView";
      c.g.b.k.a((String)localObject2);
    }
    localObject2 = h;
    if (localObject2 == null)
    {
      localObject5 = "groupParticipantAdapter";
      c.g.b.k.a((String)localObject5);
    }
    localObject2 = (RecyclerView.Adapter)localObject2;
    ((RecyclerView)localObject1).setAdapter((RecyclerView.Adapter)localObject2);
    localObject1 = paramView.findViewById(2131363224);
    c.g.b.k.a(localObject1, "view.findViewById(R.id.group_view)");
    o = ((View)localObject1);
    localObject1 = paramView.findViewById(2131362963);
    c.g.b.k.a(localObject1, "view.findViewById(R.id.empty_group_view)");
    localObject1 = (TextView)localObject1;
    p = ((TextView)localObject1);
    localObject1 = paramView.findViewById(2131362142);
    c.g.b.k.a(localObject1, "view.findViewById(R.id.bottom_container)");
    r = ((View)localObject1);
    localObject1 = paramView.findViewById(2131362824);
    c.g.b.k.a(localObject1, "view.findViewById(R.id.destination_text)");
    localObject1 = (TextView)localObject1;
    s = ((TextView)localObject1);
    localObject1 = paramView.findViewById(2131362823);
    localObject2 = "view.findViewById(R.id.destination_subtext)";
    c.g.b.k.a(localObject1, (String)localObject2);
    localObject1 = (TextView)localObject1;
    t = ((TextView)localObject1);
    i3 = 2131364590;
    paramView = paramView.findViewById(i3);
    localObject1 = "view.findViewById(R.id.start_conversation_button)";
    c.g.b.k.a(paramView, (String)localObject1);
    paramView = (FloatingActionButton)paramView;
    q = paramView;
    paramView = q;
    if (paramView == null)
    {
      localObject1 = "fabContainer";
      c.g.b.k.a((String)localObject1);
    }
    paramView.setOpenMenuOnClick(false);
    paramView = q;
    if (paramView == null)
    {
      localObject1 = "fabContainer";
      c.g.b.k.a((String)localObject1);
    }
    localObject1 = this;
    localObject1 = (FloatingActionButton.a)this;
    paramView.setFabActionListener((FloatingActionButton.a)localObject1);
    paramView = a;
    if (paramView == null)
    {
      localObject1 = "presenter";
      c.g.b.k.a((String)localObject1);
    }
    paramView.a(this);
    paramView = a;
    if (paramView == null)
    {
      localObject1 = "presenter";
      c.g.b.k.a((String)localObject1);
    }
    paramView.b(paramBundle);
    paramView = c;
    if (paramView == null)
    {
      localObject1 = "groupPresenter";
      c.g.b.k.a((String)localObject1);
    }
    paramView.a(this);
    paramView = c;
    if (paramView == null)
    {
      localObject1 = "groupPresenter";
      c.g.b.k.a((String)localObject1);
    }
    paramView.a(paramBundle);
    paramView = getActivity();
    if (paramView != null) {
      localObject4 = paramView.getIntent();
    }
    if (localObject4 != null)
    {
      paramView = ((Intent)localObject4).getExtras();
      if (paramView != null)
      {
        paramBundle = "PRE_FILL_PARTICIPANTS";
        paramView = paramView.getParcelableArrayList(paramBundle);
        if (paramView != null)
        {
          paramView = (List)paramView;
          paramBundle = c;
          if (paramBundle == null)
          {
            localObject1 = "groupPresenter";
            c.g.b.k.a((String)localObject1);
          }
          paramBundle.a(paramView);
          return;
        }
      }
    }
  }
  
  public final void p()
  {
    Object localObject = q;
    String str;
    if (localObject == null)
    {
      str = "fabContainer";
      c.g.b.k.a(str);
    }
    boolean bool = ((FloatingActionButton)localObject).a();
    if (bool)
    {
      localObject = q;
      if (localObject == null)
      {
        str = "fabContainer";
        c.g.b.k.a(str);
      }
      ((FloatingActionButton)localObject).c();
      return;
    }
    localObject = a;
    if (localObject == null)
    {
      str = "presenter";
      c.g.b.k.a(str);
    }
    ((t.g)localObject).h();
  }
  
  public final void p(int paramInt)
  {
    Context localContext = getContext();
    if (localContext != null)
    {
      Toast.makeText(localContext, paramInt, 0).show();
      return;
    }
  }
  
  public final void q()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      localf.onBackPressed();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */