package com.truecaller.messaging.newconversation.a;

import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.messaging.transport.im.bn;
import com.truecaller.util.g;
import kotlinx.coroutines.ag;

final class j$f
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  j$f(j paramj, Uri paramUri, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    f localf = new com/truecaller/messaging/newconversation/a/j$f;
    j localj = b;
    Uri localUri = c;
    localf.<init>(localj, localUri, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localf;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = j.e(b);
        localObject = c;
        paramObject = ((g)paramObject).d((Uri)localObject);
        localObject = j.g(b).a((Uri)paramObject);
        j.e(b).e((Uri)paramObject);
        return localObject;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (f)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((f)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.a.j.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */