package com.truecaller.messaging.newconversation.a;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import c.a.m;
import c.a.y;
import c.g.b.k;
import c.u;
import com.truecaller.R.id;
import com.truecaller.bg;
import com.truecaller.bi;
import com.truecaller.bj;
import com.truecaller.bk;
import com.truecaller.bl;
import com.truecaller.messaging.conversation.ConversationActivity;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.ui.components.CircularImageView;
import com.truecaller.utils.extensions.p;
import com.truecaller.utils.extensions.r;
import com.truecaller.utils.extensions.t;
import com.truecaller.wizard.utils.i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class c
  extends Fragment
  implements f.d
{
  public static final c.a c;
  public f.c a;
  public f.b b;
  private g d;
  private HashMap e;
  
  static
  {
    c.a locala = new com/truecaller/messaging/newconversation/a/c$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  private final String a(ResolveInfo paramResolveInfo)
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = ((f)localObject).getPackageManager();
      if (localObject != null)
      {
        paramResolveInfo = activityInfo.applicationInfo;
        paramResolveInfo = ((PackageManager)localObject).getApplicationLabel(paramResolveInfo);
        break label37;
      }
    }
    paramResolveInfo = null;
    label37:
    return String.valueOf(paramResolveInfo);
  }
  
  private static Intent b(Intent paramIntent, ResolveInfo paramResolveInfo)
  {
    ComponentName localComponentName = new android/content/ComponentName;
    String str = activityInfo.packageName;
    paramResolveInfo = activityInfo.name;
    localComponentName.<init>(str, paramResolveInfo);
    paramIntent.setComponent(localComponentName);
    return paramIntent;
  }
  
  private void b(Uri paramUri)
  {
    k.b(paramUri, "uri");
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "context ?: return");
    Object localObject2 = com.truecaller.common.h.n.b((Context)localObject1);
    localObject2 = com.truecaller.common.h.n.a((Context)localObject1, (Uri)localObject2);
    Object localObject3 = getActivity();
    Object localObject4 = null;
    if (localObject3 != null)
    {
      localObject3 = ((f)localObject3).getPackageManager();
      if (localObject3 != null)
      {
        localObject3 = ((PackageManager)localObject3).queryIntentActivities((Intent)localObject2, 0);
        if (localObject3 != null) {
          break label81;
        }
      }
    }
    localObject3 = (List)y.a;
    label81:
    int i = ((List)localObject3).size();
    switch (i)
    {
    default: 
      paramUri = (Uri)localObject3;
      paramUri = (Iterable)localObject3;
      localObject4 = new java/util/ArrayList;
      i = 10;
      int j = m.a(paramUri, i);
      ((ArrayList)localObject4).<init>(j);
      localObject4 = (Collection)localObject4;
      paramUri = paramUri.iterator();
      break;
    case 1: 
      paramUri = new android/content/Intent;
      paramUri.<init>((Intent)localObject2);
      localObject1 = (ResolveInfo)((List)localObject3).get(0);
      paramUri = b(paramUri, (ResolveInfo)localObject1);
      startActivityForResult(paramUri, 2);
      return;
    case 0: 
      localObject1 = a;
      if (localObject1 == null)
      {
        localObject2 = "presenter";
        k.a((String)localObject2);
      }
      ((f.c)localObject1).b(paramUri);
      return;
    }
    for (;;)
    {
      boolean bool2 = paramUri.hasNext();
      if (!bool2) {
        break;
      }
      Object localObject5 = (ResolveInfo)paramUri.next();
      c.n localn = new c/n;
      Intent localIntent = new android/content/Intent;
      localIntent.<init>((Intent)localObject2);
      localIntent = b(localIntent, (ResolveInfo)localObject5);
      localObject5 = a((ResolveInfo)localObject5);
      localn.<init>(localIntent, localObject5);
      ((Collection)localObject4).add(localn);
    }
    localObject4 = (List)localObject4;
    paramUri = (Uri)localObject4;
    paramUri = (Iterable)localObject4;
    localObject2 = new java/util/ArrayList;
    i = m.a(paramUri, i);
    ((ArrayList)localObject2).<init>(i);
    localObject2 = (Collection)localObject2;
    paramUri = paramUri.iterator();
    for (;;)
    {
      boolean bool1 = paramUri.hasNext();
      if (!bool1) {
        break;
      }
      String str = (String)nextb;
      ((Collection)localObject2).add(str);
    }
    localObject2 = (List)localObject2;
    paramUri = new android/widget/ArrayAdapter;
    paramUri.<init>((Context)localObject1, 17367043, (List)localObject2);
    localObject2 = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject2).<init>((Context)localObject1);
    localObject1 = ((AlertDialog.Builder)localObject2).setTitle(2131887194);
    paramUri = (ListAdapter)paramUri;
    localObject2 = new com/truecaller/messaging/newconversation/a/c$f;
    ((c.f)localObject2).<init>(this, (List)localObject4, (List)localObject3);
    localObject2 = (DialogInterface.OnClickListener)localObject2;
    ((AlertDialog.Builder)localObject1).setAdapter(paramUri, (DialogInterface.OnClickListener)localObject2).show();
  }
  
  private View c(int paramInt)
  {
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      e = ((HashMap)localObject1);
    }
    localObject1 = e;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = e;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final f.c a()
  {
    f.c localc = a;
    if (localc == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localc;
  }
  
  public final void a(int paramInt)
  {
    int i = R.id.participant_count;
    TextView localTextView = (TextView)c(i);
    k.a(localTextView, "participant_count");
    Resources localResources = getResources();
    Object[] arrayOfObject = new Object[1];
    Integer localInteger = Integer.valueOf(paramInt);
    arrayOfObject[0] = localInteger;
    CharSequence localCharSequence = (CharSequence)localResources.getQuantityString(2131755027, paramInt, arrayOfObject);
    localTextView.setText(localCharSequence);
  }
  
  public final void a(int paramInt1, int paramInt2, int paramInt3)
  {
    bl localbl = new com/truecaller/bl;
    Context localContext = (Context)requireActivity();
    localbl.<init>(localContext, paramInt1, paramInt2, paramInt3);
    localbl.show();
  }
  
  public final void a(Uri paramUri)
  {
    boolean bool1 = true;
    if (paramUri == null)
    {
      j = R.id.group_image_view;
      ((CircularImageView)c(j)).setImageBitmap(null);
      j = R.id.add_photo_icon_view;
      paramUri = (AppCompatImageView)c(j);
      k.a(paramUri, "add_photo_icon_view");
      t.a((View)paramUri, bool1);
      return;
    }
    paramUri = bg.a(requireContext()).b(paramUri);
    Object localObject1 = com.bumptech.glide.load.b.j.b;
    paramUri = paramUri.b((com.bumptech.glide.load.b.j)localObject1).c(bool1);
    int i = R.id.group_image_view;
    Object localObject2 = (CircularImageView)c(i);
    paramUri.a((ImageView)localObject2);
    int j = R.id.add_photo_icon_view;
    paramUri = (AppCompatImageView)c(j);
    k.a(paramUri, "add_photo_icon_view");
    paramUri = (View)paramUri;
    localObject2 = a;
    if (localObject2 == null)
    {
      localObject1 = "presenter";
      k.a((String)localObject1);
    }
    boolean bool2 = k.a(e, "im_group_mode_edit");
    t.a(paramUri, bool2);
  }
  
  public final void a(Participant paramParticipant)
  {
    k.b(paramParticipant, "participant");
    Intent localIntent = new android/content/Intent;
    Context localContext = getContext();
    localIntent.<init>(localContext, ConversationActivity.class);
    Object localObject = new Participant[1];
    localObject[0] = paramParticipant;
    localObject = (Parcelable[])localObject;
    localIntent.putExtra("participants", (Parcelable[])localObject);
    startActivity(localIntent);
  }
  
  public final void a(String paramString, int paramInt)
  {
    k.b(paramString, "permission");
    i.a((Fragment)this, paramString, paramInt);
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.done_button;
    FloatingActionButton localFloatingActionButton = (FloatingActionButton)c(i);
    k.a(localFloatingActionButton, "done_button");
    t.a((View)localFloatingActionButton, paramBoolean);
  }
  
  public final boolean a(String paramString)
  {
    k.b(paramString, "permission");
    return i.a((Activity)requireActivity(), paramString);
  }
  
  public final void b()
  {
    android.support.v4.app.j localj = getFragmentManager();
    if (localj != null)
    {
      localj.c();
      return;
    }
  }
  
  public final void b(int paramInt)
  {
    Toast.makeText(getContext(), paramInt, 0).show();
  }
  
  public final void b(String paramString)
  {
    int i = R.id.group_name_edit_text;
    EditText localEditText = (EditText)c(i);
    paramString = (CharSequence)paramString;
    localEditText.setText(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    int i = R.id.progress;
    Object localObject1 = (ProgressBar)c(i);
    Object localObject2 = "progress";
    k.a(localObject1, (String)localObject2);
    t.a((View)localObject1, paramBoolean);
    i = R.id.done_button;
    localObject1 = (FloatingActionButton)c(i);
    if (paramBoolean)
    {
      bool = false;
      localObject2 = null;
    }
    else
    {
      localObject2 = requireContext();
      int j = 2131233981;
      localObject2 = android.support.v4.content.b.a((Context)localObject2, j);
    }
    ((FloatingActionButton)localObject1).setImageDrawable((Drawable)localObject2);
    i = R.id.group_name_edit_text;
    localObject1 = (EditText)c(i);
    k.a(localObject1, "group_name_edit_text");
    boolean bool = paramBoolean ^ true;
    ((EditText)localObject1).setEnabled(bool);
    i = R.id.group_image_view;
    localObject1 = (CircularImageView)c(i);
    k.a(localObject1, "group_image_view");
    paramBoolean ^= true;
    ((CircularImageView)localObject1).setEnabled(paramBoolean);
  }
  
  public final void c()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void c(boolean paramBoolean)
  {
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "context ?: return");
    int i = 2;
    Object localObject2 = new String[i];
    Object localObject3 = null;
    String str = getString(2131886738);
    localObject2[0] = str;
    int j = 1;
    int k = 2131886739;
    str = getString(k);
    localObject2[j] = str;
    localObject2 = m.c((Object[])localObject2);
    if (paramBoolean)
    {
      paramBoolean = 2131886740;
      localObject4 = getString(paramBoolean);
      ((List)localObject2).add(localObject4);
    }
    Object localObject4 = new android/widget/ArrayAdapter;
    ((ArrayAdapter)localObject4).<init>((Context)localObject1, 17367043, (List)localObject2);
    localObject3 = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject3).<init>((Context)localObject1);
    localObject1 = ((AlertDialog.Builder)localObject3).setTitle(2131886741);
    localObject4 = (ListAdapter)localObject4;
    localObject3 = new com/truecaller/messaging/newconversation/a/c$g;
    ((c.g)localObject3).<init>(this, (List)localObject2);
    localObject3 = (DialogInterface.OnClickListener)localObject3;
    ((AlertDialog.Builder)localObject1).setAdapter((ListAdapter)localObject4, (DialogInterface.OnClickListener)localObject3).show();
  }
  
  public final void d()
  {
    Intent localIntent = com.truecaller.common.h.n.a(requireContext());
    startActivityForResult(localIntent, 0);
  }
  
  public final void e()
  {
    Intent localIntent = com.truecaller.common.h.n.a();
    CharSequence localCharSequence = (CharSequence)getString(2131887194);
    localIntent = Intent.createChooser(localIntent, localCharSequence);
    startActivityForResult(localIntent, 1);
  }
  
  public final void f()
  {
    g localg = d;
    if (localg == null)
    {
      String str = "groupParticipantAdapter";
      k.a(str);
    }
    localg.notifyDataSetChanged();
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    int i = -1;
    if (paramInt2 == i)
    {
      Object localObject1;
      Object localObject2;
      switch (paramInt1)
      {
      default: 
        break;
      case 2: 
        localObject1 = a;
        if (localObject1 == null)
        {
          localObject2 = "presenter";
          k.a((String)localObject2);
        }
        localObject2 = com.truecaller.common.h.n.c(getContext());
        ((f.c)localObject1).a((Uri)localObject2);
        break;
      case 1: 
        if (paramIntent != null)
        {
          localObject1 = paramIntent.getData();
          if (localObject1 != null)
          {
            localObject2 = requireContext();
            k.a(localObject2, "requireContext()");
            paramIntent = com.truecaller.common.h.n.b(requireContext());
            String str = "ImageUtils.getTempCaptureUri(requireContext())";
            k.a(paramIntent, str);
            localObject1 = r.a((Uri)localObject1, (Context)localObject2, paramIntent);
            if (localObject1 == null) {
              return;
            }
            b((Uri)localObject1);
          }
        }
        return;
      case 0: 
        localObject1 = com.truecaller.common.h.n.b(requireContext());
        k.a(localObject1, "destUri");
        b((Uri)localObject1);
        return;
      }
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getActivity();
    if (paramBundle != null)
    {
      paramBundle = (Activity)paramBundle;
      Object localObject = a.a();
      paramBundle = paramBundle.getApplicationContext();
      if (paramBundle != null)
      {
        paramBundle = ((bk)paramBundle).a();
        paramBundle = ((a.a)localObject).a(paramBundle);
        localObject = new com/truecaller/messaging/newconversation/a/d;
        ((d)localObject).<init>();
        paramBundle.a((d)localObject).a().a(this);
        return;
      }
      paramBundle = new c/u;
      paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
      throw paramBundle;
    }
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558734, paramViewGroup, false);
  }
  
  public final void onDestroyView()
  {
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((f.c)localObject).y_();
    com.truecaller.common.h.n.f(getContext());
    super.onDestroyView();
    localObject = e;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    k.b(paramArrayOfInt, "grantResults");
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    i.a(paramArrayOfString, paramArrayOfInt);
    f.c localc = a;
    if (localc == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localc.a(paramInt, paramArrayOfString, paramArrayOfInt);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject = "view";
    k.b(paramView, (String)localObject);
    super.onViewCreated(paramView, paramBundle);
    paramView = getArguments();
    paramBundle = null;
    int i;
    if (paramView != null)
    {
      localObject = "participants";
      paramView = paramView.getParcelableArray((String)localObject);
    }
    else
    {
      i = 0;
      paramView = null;
    }
    boolean bool = paramView instanceof Participant[];
    if (!bool)
    {
      i = 0;
      paramView = null;
    }
    paramView = (Participant[])paramView;
    localObject = b;
    String str;
    if (localObject == null)
    {
      str = "groupParticipantPresenter";
      k.a(str);
    }
    a = paramView;
    localObject = a;
    if (localObject == null)
    {
      str = "presenter";
      k.a(str);
    }
    c = paramView;
    paramView = a;
    if (paramView == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    localObject = getArguments();
    if (localObject != null) {
      paramBundle = (ImGroupInfo)((Bundle)localObject).getParcelable("im_group_info");
    }
    d = ((ImGroupInfo)paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramBundle = getArguments();
    if (paramBundle != null)
    {
      localObject = "im_group_mode";
      paramBundle = paramBundle.getString((String)localObject);
      if (paramBundle != null)
      {
        e = paramBundle;
        i = R.id.toolbar;
        paramView = (Toolbar)c(i);
        paramBundle = new com/truecaller/messaging/newconversation/a/c$b;
        paramBundle.<init>(this);
        paramBundle = (View.OnClickListener)paramBundle;
        paramView.setNavigationOnClickListener(paramBundle);
        i = R.id.toolbar;
        paramView = (Toolbar)c(i);
        k.a(paramView, "toolbar");
        paramBundle = requireContext();
        int j = 2131233834;
        int k = 2130969274;
        paramBundle = com.truecaller.utils.ui.b.a(paramBundle, j, k);
        paramView.setNavigationIcon(paramBundle);
        i = R.id.done_button;
        paramView = (FloatingActionButton)c(i);
        paramBundle = new com/truecaller/messaging/newconversation/a/c$c;
        paramBundle.<init>(this);
        paramBundle = (View.OnClickListener)paramBundle;
        paramView.setOnClickListener(paramBundle);
        paramView = new com/truecaller/messaging/newconversation/a/g;
        paramBundle = b;
        if (paramBundle == null)
        {
          localObject = "groupParticipantPresenter";
          k.a((String)localObject);
        }
        paramView.<init>(paramBundle);
        d = paramView;
        i = R.id.group_participants_recycler_view;
        paramView = (RecyclerView)c(i);
        k.a(paramView, "group_participants_recycler_view");
        paramBundle = d;
        if (paramBundle == null)
        {
          localObject = "groupParticipantAdapter";
          k.a((String)localObject);
        }
        paramBundle = (RecyclerView.Adapter)paramBundle;
        paramView.setAdapter(paramBundle);
        i = R.id.group_name_edit_text;
        paramView = (EditText)c(i);
        k.a(paramView, "group_name_edit_text");
        paramView = (TextView)paramView;
        paramBundle = new com/truecaller/messaging/newconversation/a/c$d;
        paramBundle.<init>(this);
        paramBundle = (c.g.a.b)paramBundle;
        p.a(paramView, paramBundle);
        i = R.id.group_image_view;
        paramView = (CircularImageView)c(i);
        paramBundle = new com/truecaller/messaging/newconversation/a/c$e;
        paramBundle.<init>(this);
        paramBundle = (View.OnClickListener)paramBundle;
        paramView.setOnClickListener(paramBundle);
        paramView = a;
        if (paramView == null)
        {
          paramBundle = "presenter";
          k.a(paramBundle);
        }
        paramView.a(this);
        return;
      }
    }
    paramView = new java/lang/IllegalArgumentException;
    paramView.<init>("imGroupMode must be defined");
    throw ((Throwable)paramView);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */