package com.truecaller.messaging.newconversation.a;

import android.net.Uri;
import c.g.b.k;
import c.u;
import com.truecaller.androidactors.a;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.transport.im.bn;
import com.truecaller.util.g;
import com.truecaller.utils.l;
import java.util.List;
import kotlinx.coroutines.e;

public final class j
  extends f.c
{
  private String f;
  private Uri g;
  private a h;
  private final c.d.f i;
  private final c.d.f j;
  private final com.truecaller.androidactors.i k;
  private final com.truecaller.androidactors.f l;
  private final l m;
  private final bn n;
  private final g o;
  private final com.truecaller.utils.i p;
  private final f.b q;
  
  public j(c.d.f paramf1, c.d.f paramf2, com.truecaller.androidactors.i parami, com.truecaller.androidactors.f paramf, l paraml, bn parambn, g paramg, com.truecaller.utils.i parami1, f.b paramb)
  {
    super(paramf2);
    i = paramf1;
    j = paramf2;
    k = parami;
    l = paramf;
    m = paraml;
    n = parambn;
    o = paramg;
    p = parami1;
    q = paramb;
  }
  
  private final void a(Boolean paramBoolean)
  {
    f.d locald = (f.d)b;
    if (locald == null) {
      return;
    }
    locald.b(false);
    Boolean localBoolean = Boolean.TRUE;
    boolean bool = k.a(paramBoolean, localBoolean);
    if (bool)
    {
      locald.c();
      return;
    }
    locald.b(2131886751);
  }
  
  private static boolean b(String paramString)
  {
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    boolean bool = true;
    if (localObject != null)
    {
      i1 = ((CharSequence)localObject).length();
      if (i1 != 0)
      {
        i1 = 0;
        localObject = null;
        break label36;
      }
    }
    int i1 = 1;
    label36:
    if (i1 == 0)
    {
      i1 = paramString.length();
      if (i1 > 0)
      {
        int i2 = paramString.length();
        i1 = 25;
        if (i2 <= i1) {
          return bool;
        }
      }
    }
    return false;
  }
  
  private final boolean i()
  {
    String str1 = e;
    if (str1 != null)
    {
      int i1 = str1.hashCode();
      int i2 = -1424083893;
      String str2;
      boolean bool;
      if (i1 != i2)
      {
        i2 = 1505483517;
        if (i1 == i2)
        {
          str2 = "im_group_mode_create";
          bool = str1.equals(str2);
          if (bool) {
            return b(f);
          }
        }
      }
      else
      {
        str2 = "im_group_mode_edit";
        bool = str1.equals(str2);
        if (bool)
        {
          bool = k();
          if (!bool)
          {
            bool = j();
            if (bool)
            {
              str1 = f;
              bool = b(str1);
              if (bool) {}
            }
            else
            {
              return false;
            }
          }
          return true;
        }
      }
    }
    return false;
  }
  
  private final boolean j()
  {
    Object localObject = d;
    if (localObject != null)
    {
      localObject = c;
      if (localObject != null)
      {
        localObject = Uri.parse((String)localObject);
        break label28;
      }
    }
    localObject = null;
    label28:
    Uri localUri = g;
    return k.a(localObject, localUri) ^ true;
  }
  
  private final boolean k()
  {
    Object localObject = d;
    if (localObject != null)
    {
      localObject = b;
    }
    else
    {
      bool1 = false;
      localObject = null;
    }
    String str = f;
    boolean bool1 = k.a(localObject, str);
    boolean bool2 = true;
    bool1 ^= bool2;
    if (bool1)
    {
      localObject = f;
      bool1 = b((String)localObject);
      if (bool1) {
        return bool2;
      }
    }
    return false;
  }
  
  public final void a()
  {
    Object localObject1 = (f.d)b;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = f;
    boolean bool1 = b((String)localObject2);
    if (!bool1)
    {
      ((f.d)localObject1).b(2131886748);
      return;
    }
    localObject2 = p;
    bool1 = ((com.truecaller.utils.i)localObject2).a();
    if (!bool1)
    {
      ((f.d)localObject1).b(2131886537);
      return;
    }
    bool1 = true;
    ((f.d)localObject1).b(bool1);
    localObject1 = e;
    if (localObject1 == null) {
      return;
    }
    int i1 = ((String)localObject1).hashCode();
    int i2 = -1424083893;
    int i3 = 3;
    boolean bool2;
    Object localObject3;
    if (i1 != i2)
    {
      i2 = 1505483517;
      if (i1 == i2)
      {
        localObject2 = "im_group_mode_create";
        bool2 = ((String)localObject1).equals(localObject2);
        if (bool2)
        {
          localObject1 = f;
          if (localObject1 == null) {
            return;
          }
          localObject2 = c;
          if (localObject2 != null)
          {
            localObject2 = c.a.f.a((Object[])localObject2);
            if (localObject2 != null)
            {
              localObject3 = new com/truecaller/messaging/newconversation/a/j$b;
              ((j.b)localObject3).<init>(this, (List)localObject2, (String)localObject1, null);
              localObject3 = (c.g.a.m)localObject3;
              e.b(this, null, (c.g.a.m)localObject3, i3);
              return;
            }
          }
        }
      }
    }
    else
    {
      localObject2 = "im_group_mode_edit";
      bool2 = ((String)localObject1).equals(localObject2);
      if (bool2)
      {
        bool2 = j();
        if (!bool2)
        {
          bool2 = k();
          if (!bool2)
          {
            localObject1 = Boolean.TRUE;
            a((Boolean)localObject1);
            return;
          }
        }
        localObject1 = f;
        if (localObject1 == null) {
          return;
        }
        localObject2 = d;
        if (localObject2 != null)
        {
          localObject2 = a;
          if (localObject2 != null)
          {
            localObject3 = new com/truecaller/messaging/newconversation/a/j$d;
            ((j.d)localObject3).<init>(this, (String)localObject2, (String)localObject1, null);
            localObject3 = (c.g.a.m)localObject3;
            e.b(this, null, (c.g.a.m)localObject3, i3);
            return;
          }
        }
        return;
      }
    }
  }
  
  public final void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    paramArrayOfString = "grantResults";
    k.b(paramArrayOfInt, paramArrayOfString);
    int i1 = 1;
    f.d locald;
    switch (paramInt)
    {
    default: 
      break;
    case 1: 
      paramInt = paramArrayOfInt.length;
      if (paramInt == 0)
      {
        paramInt = 1;
      }
      else
      {
        paramInt = 0;
        locald = null;
      }
      paramInt ^= i1;
      if (paramInt != 0)
      {
        paramInt = paramArrayOfInt[0];
        if (paramInt == 0)
        {
          locald = (f.d)b;
          if (locald != null)
          {
            locald.e();
            return;
          }
        }
      }
      break;
    case 0: 
      paramInt = paramArrayOfInt.length;
      if (paramInt == 0)
      {
        paramInt = 1;
      }
      else
      {
        paramInt = 0;
        locald = null;
      }
      paramInt ^= i1;
      if (paramInt != 0)
      {
        paramInt = paramArrayOfInt[0];
        if (paramInt == 0)
        {
          locald = (f.d)b;
          if (locald != null) {
            locald.d();
          }
          return;
        }
      }
      break;
    }
  }
  
  public final void a(Uri paramUri)
  {
    g = paramUri;
    f.d locald = (f.d)b;
    if (locald != null) {
      locald.a(paramUri);
    }
    paramUri = (f.d)b;
    if (paramUri != null)
    {
      boolean bool = i();
      paramUri.a(bool);
      return;
    }
  }
  
  public final void a(String paramString)
  {
    if (paramString != null)
    {
      if (paramString != null)
      {
        paramString = c.n.m.b((CharSequence)paramString).toString();
      }
      else
      {
        paramString = new c/u;
        paramString.<init>("null cannot be cast to non-null type kotlin.CharSequence");
        throw paramString;
      }
    }
    else {
      paramString = null;
    }
    f = paramString;
    paramString = (f.d)b;
    if (paramString != null)
    {
      boolean bool = i();
      paramString.a(bool);
      return;
    }
  }
  
  public final void b(Uri paramUri)
  {
    k.b(paramUri, "uri");
    Object localObject = new com/truecaller/messaging/newconversation/a/j$h;
    ((j.h)localObject).<init>(this, paramUri, null);
    localObject = (c.g.a.m)localObject;
    e.b(this, null, (c.g.a.m)localObject, 3);
  }
  
  public final void e()
  {
    Object localObject = e;
    if (localObject == null) {
      return;
    }
    int i1 = ((String)localObject).hashCode();
    int i2 = -1424083893;
    String str;
    boolean bool;
    if (i1 != i2)
    {
      i2 = 1505483517;
      if (i1 == i2)
      {
        str = "im_group_mode_create";
        bool = ((String)localObject).equals(str);
        if (bool)
        {
          localObject = (f.d)b;
          if (localObject != null) {
            ((f.d)localObject).b();
          }
        }
      }
    }
    else
    {
      str = "im_group_mode_edit";
      bool = ((String)localObject).equals(str);
      if (bool)
      {
        localObject = (f.d)b;
        if (localObject != null)
        {
          ((f.d)localObject).c();
          return;
        }
      }
    }
  }
  
  public final void f()
  {
    f.d locald = (f.d)b;
    if (locald != null)
    {
      Uri localUri = g;
      boolean bool;
      if (localUri != null)
      {
        bool = true;
      }
      else
      {
        bool = false;
        localUri = null;
      }
      locald.c(bool);
      return;
    }
  }
  
  public final void g()
  {
    f.d locald = (f.d)b;
    if (locald == null) {
      return;
    }
    Object localObject = m;
    String[] arrayOfString = { "android.permission.CAMERA" };
    boolean bool = ((l)localObject).a(arrayOfString);
    if (bool)
    {
      locald.d();
      return;
    }
    localObject = "android.permission.CAMERA";
    bool = locald.a((String)localObject);
    if (bool)
    {
      locald.a(2131886796, 2131886795, 2131233942);
      return;
    }
    locald.a("android.permission.CAMERA", 0);
  }
  
  public final void h()
  {
    f.d locald = (f.d)b;
    if (locald == null) {
      return;
    }
    Object localObject = m;
    String[] arrayOfString = { "android.permission.READ_EXTERNAL_STORAGE" };
    boolean bool = ((l)localObject).a(arrayOfString);
    if (bool)
    {
      locald.e();
      return;
    }
    localObject = "android.permission.READ_EXTERNAL_STORAGE";
    bool = locald.a((String)localObject);
    if (bool)
    {
      locald.a(2131886801, 2131886800, 2131234158);
      return;
    }
    locald.a("android.permission.READ_EXTERNAL_STORAGE", 1);
  }
  
  public final void y_()
  {
    super.y_();
    a locala = h;
    if (locala != null) {
      locala.a();
    }
    h = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.a.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */