package com.truecaller.messaging.newconversation.a;

import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import java.util.List;
import kotlinx.coroutines.ag;

final class j$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  j$b(j paramj, List paramList, String paramString, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/messaging/newconversation/a/j$b;
    j localj = b;
    List localList = c;
    String str = d;
    localb.<init>(localj, localList, str, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label162;
      }
      paramObject = j.c(b);
      if (paramObject == null) {
        break label130;
      }
      paramObject = b;
      localObject2 = j.c((j)paramObject);
      int j = 1;
      a = j;
      paramObject = ((j)paramObject).a((Uri)localObject2, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (String)paramObject;
    break label132;
    label130:
    paramObject = null;
    label132:
    localObject1 = b;
    Object localObject2 = c;
    String str = d;
    j.a((j)localObject1, (List)localObject2, str, (String)paramObject);
    return x.a;
    label162:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.a.j.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */