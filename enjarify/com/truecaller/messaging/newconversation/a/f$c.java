package com.truecaller.messaging.newconversation.a;

import android.net.Uri;
import c.d.f;
import com.truecaller.ba;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.Participant;

public abstract class f$c
  extends ba
{
  Participant[] c;
  ImGroupInfo d;
  String e;
  
  public f$c(f paramf)
  {
    super(paramf);
  }
  
  public abstract void a();
  
  public abstract void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt);
  
  public abstract void a(Uri paramUri);
  
  public abstract void a(String paramString);
  
  public abstract void b(Uri paramUri);
  
  public abstract void e();
  
  public abstract void f();
  
  public abstract void g();
  
  public abstract void h();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.a.f.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */