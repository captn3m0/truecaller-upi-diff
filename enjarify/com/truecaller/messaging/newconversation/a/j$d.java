package com.truecaller.messaging.newconversation.a;

import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.messaging.data.types.ImGroupInfo;
import kotlinx.coroutines.ag;

final class j$d
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  j$d(j paramj, String paramString1, String paramString2, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/messaging/newconversation/a/j$d;
    j localj = b;
    String str1 = c;
    String str2 = d;
    locald.<init>(localj, str1, str2, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    int j = 0;
    Object localObject2 = null;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label220;
      }
      paramObject = b;
      boolean bool3 = j.f((j)paramObject);
      if (!bool3) {
        break label158;
      }
      paramObject = j.c(b);
      if (paramObject == null) {
        break label158;
      }
      paramObject = b;
      localObject3 = j.c((j)paramObject);
      j = 1;
      a = j;
      paramObject = ((j)paramObject).a((Uri)localObject3, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    localObject2 = paramObject;
    localObject2 = (String)paramObject;
    break label191;
    label158:
    paramObject = j.c(b);
    if (paramObject != null)
    {
      paramObject = b.d;
      if (paramObject != null) {
        localObject2 = c;
      }
    }
    label191:
    paramObject = b;
    localObject1 = c;
    Object localObject3 = d;
    j.a((j)paramObject, (String)localObject1, (String)localObject3, (String)localObject2);
    return x.a;
    label220:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.a.j.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */