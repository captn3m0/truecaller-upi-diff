package com.truecaller.messaging.newconversation.a;

import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.util.g;
import kotlinx.coroutines.ag;

final class j$h$1
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  j$h$1(j.h paramh, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/messaging/newconversation/a/j$h$1;
    j.h localh = b;
    local1.<init>(localh, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = j.e(b.c);
        localObject = b.d;
        return ((g)paramObject).f((Uri)localObject);
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.a.j.h.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */