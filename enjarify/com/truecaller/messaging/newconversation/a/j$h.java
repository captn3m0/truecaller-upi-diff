package com.truecaller.messaging.newconversation.a;

import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.g;

final class j$h
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag e;
  
  j$h(j paramj, Uri paramUri, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    h localh = new com/truecaller/messaging/newconversation/a/j$h;
    j localj = c;
    Uri localUri = d;
    localh.<init>(localj, localUri, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localh;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    boolean bool;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      localObject1 = (j)a;
      bool = paramObject instanceof o.b;
      if (bool) {
        throw a;
      }
      break;
    case 0: 
      bool = paramObject instanceof o.b;
      if (bool) {
        break label193;
      }
      paramObject = c;
      Object localObject2 = j.d((j)paramObject);
      Object localObject3 = new com/truecaller/messaging/newconversation/a/j$h$1;
      ((j.h.1)localObject3).<init>(this, null);
      localObject3 = (m)localObject3;
      a = paramObject;
      int j = 1;
      b = j;
      localObject2 = g.a((f)localObject2, (m)localObject3, this);
      if (localObject2 == localObject1) {
        return localObject1;
      }
      localObject1 = paramObject;
      paramObject = localObject2;
    }
    paramObject = (Uri)paramObject;
    j.a((j)localObject1, (Uri)paramObject);
    paramObject = j.c(c);
    if (paramObject != null)
    {
      localObject1 = j.b(c);
      if (localObject1 != null) {
        ((f.d)localObject1).a((Uri)paramObject);
      }
    }
    return x.a;
    label193:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (h)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((h)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.a.j.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */