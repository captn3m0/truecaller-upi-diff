package com.truecaller.messaging.newconversation.a;

import javax.inject.Provider;

public final class e
  implements dagger.a.d
{
  private final d a;
  private final Provider b;
  
  private e(d paramd, Provider paramProvider)
  {
    a = paramd;
    b = paramProvider;
  }
  
  public static e a(d paramd, Provider paramProvider)
  {
    e locale = new com/truecaller/messaging/newconversation/a/e;
    locale.<init>(paramd, paramProvider);
    return locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */