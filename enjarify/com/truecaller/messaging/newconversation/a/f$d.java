package com.truecaller.messaging.newconversation.a;

import android.net.Uri;
import com.truecaller.messaging.data.types.Participant;

public abstract interface f$d
{
  public abstract void a(int paramInt);
  
  public abstract void a(int paramInt1, int paramInt2, int paramInt3);
  
  public abstract void a(Uri paramUri);
  
  public abstract void a(Participant paramParticipant);
  
  public abstract void a(String paramString, int paramInt);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract boolean a(String paramString);
  
  public abstract void b();
  
  public abstract void b(int paramInt);
  
  public abstract void b(String paramString);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c();
  
  public abstract void c(boolean paramBoolean);
  
  public abstract void d();
  
  public abstract void e();
  
  public abstract void f();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.a.f.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */