package com.truecaller.messaging.newconversation;

import android.content.Intent;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.g.j;
import java.util.ArrayList;
import java.util.List;

public abstract interface t$i
  extends j
{
  public abstract void a(int paramInt1, int paramInt2, int paramInt3);
  
  public abstract void a(Integer paramInteger);
  
  public abstract void a(Long paramLong, Participant[] paramArrayOfParticipant, Intent paramIntent);
  
  public abstract void a(String paramString);
  
  public abstract void a(ArrayList paramArrayList);
  
  public abstract void a(List paramList);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void a(boolean paramBoolean1, boolean paramBoolean2, String paramString, Integer paramInteger);
  
  public abstract void a(Participant[] paramArrayOfParticipant);
  
  public abstract void b(int paramInt);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void d();
  
  public abstract void d(boolean paramBoolean);
  
  public abstract void e(boolean paramBoolean);
  
  public abstract void f(boolean paramBoolean);
  
  public abstract void g();
  
  public abstract void h();
  
  public abstract void k();
  
  public abstract void l();
  
  public abstract void o();
  
  public abstract void q();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.t.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */