package com.truecaller.messaging.newconversation;

import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.ui.components.d.c;
import com.truecaller.ui.view.ContactPhoto;

public final class b$a
  extends d.c
  implements t.b
{
  private final ContactPhoto a;
  private final TextView c;
  
  public b$a(View paramView, t.c paramc)
  {
    super(paramView);
    Object localObject = paramView.findViewById(2131362554);
    k.a(localObject, "itemView.findViewById(R.id.contact_photo)");
    localObject = (ContactPhoto)localObject;
    a = ((ContactPhoto)localObject);
    localObject = paramView.findViewById(2131363791);
    k.a(localObject, "itemView.findViewById(R.id.name_text)");
    localObject = (TextView)localObject;
    c = ((TextView)localObject);
    paramView = paramView.findViewById(2131364114);
    localObject = new com/truecaller/messaging/newconversation/b$a$1;
    ((b.a.1)localObject).<init>(this, paramc);
    localObject = (View.OnClickListener)localObject;
    paramView.setOnClickListener((View.OnClickListener)localObject);
  }
  
  public final void a(Uri paramUri)
  {
    a.a(paramUri, null);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "name");
    TextView localTextView = c;
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */