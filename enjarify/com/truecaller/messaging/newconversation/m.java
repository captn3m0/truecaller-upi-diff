package com.truecaller.messaging.newconversation;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import com.truecaller.adapter_delegates.i;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.util.at;
import com.truecaller.utils.extensions.t;
import com.truecaller.utils.ui.b;

public final class m
  extends RecyclerView.ViewHolder
  implements t.e
{
  final TextView a;
  boolean b;
  String c;
  Drawable d;
  private final ContactPhoto e;
  private final View f;
  private final TextView g;
  private final TextView h;
  private final TextView i;
  
  public m(View paramView, com.truecaller.adapter_delegates.k paramk)
  {
    super(paramView);
    Object localObject = paramView.findViewById(2131362554);
    c.g.b.k.a(localObject, "itemView.findViewById(R.id.contact_photo)");
    localObject = (ContactPhoto)localObject;
    e = ((ContactPhoto)localObject);
    localObject = paramView.findViewById(2131364018);
    c.g.b.k.a(localObject, "itemView.findViewById(R.id.progress_bar)");
    f = ((View)localObject);
    localObject = paramView.findViewById(2131363791);
    c.g.b.k.a(localObject, "itemView.findViewById(R.id.name_text)");
    localObject = (TextView)localObject;
    g = ((TextView)localObject);
    localObject = paramView.findViewById(2131364281);
    c.g.b.k.a(localObject, "itemView.findViewById(R.id.secondary_text)");
    localObject = (TextView)localObject;
    h = ((TextView)localObject);
    localObject = paramView.findViewById(2131363797);
    c.g.b.k.a(localObject, "itemView.findViewById(R.id.new_badge_text)");
    localObject = (TextView)localObject;
    i = ((TextView)localObject);
    localObject = paramView.findViewById(2131362988);
    c.g.b.k.a(localObject, "itemView.findViewById(R.id.error_text)");
    localObject = (TextView)localObject;
    a = ((TextView)localObject);
    localObject = this;
    localObject = (RecyclerView.ViewHolder)this;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, null, 12);
  }
  
  public final String a()
  {
    return c;
  }
  
  public final void a(int paramInt)
  {
    h.setText(paramInt);
  }
  
  public final void a(Uri paramUri)
  {
    e.a(paramUri, null);
  }
  
  public final void b(String paramString)
  {
    TextView localTextView = g;
    paramString = (CharSequence)at.a((CharSequence)paramString);
    localTextView.setText(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    e.setIsGroup(paramBoolean);
  }
  
  public final boolean b()
  {
    return b;
  }
  
  public final Drawable c()
  {
    return d;
  }
  
  public final void c(String paramString)
  {
    c.g.b.k.b(paramString, "text");
    TextView localTextView = h;
    paramString = (CharSequence)at.a((CharSequence)paramString);
    localTextView.setText(paramString);
  }
  
  public final void c(boolean paramBoolean)
  {
    t.a(f, paramBoolean);
  }
  
  public final void c_(String paramString)
  {
    c = paramString;
  }
  
  public final void c_(boolean paramBoolean)
  {
    b = paramBoolean;
  }
  
  public final void d(boolean paramBoolean)
  {
    Object localObject = g.getContext();
    int j = 2131234626;
    localObject = b.a((Context)localObject, j, 2130969528);
    TextView localTextView = g;
    if (!paramBoolean) {
      localObject = null;
    }
    android.support.v4.widget.m.a(localTextView, null, (Drawable)localObject);
  }
  
  public final void e(boolean paramBoolean)
  {
    t.a((View)h, paramBoolean);
  }
  
  public final void f(boolean paramBoolean)
  {
    View localView = itemView;
    c.g.b.k.a(localView, "itemView");
    localView.setSelected(paramBoolean);
  }
  
  public final void g(boolean paramBoolean)
  {
    t.a((View)i, paramBoolean);
  }
  
  public final void h(boolean paramBoolean)
  {
    View localView = itemView;
    String str = "itemView";
    c.g.b.k.a(localView, str);
    float f1;
    if (paramBoolean)
    {
      paramBoolean = 1056964608;
      f1 = 0.5F;
    }
    else
    {
      paramBoolean = 1065353216;
      f1 = 1.0F;
    }
    localView.setAlpha(f1);
  }
  
  public final void i(boolean paramBoolean)
  {
    View localView = itemView;
    c.g.b.k.a(localView, "itemView");
    localView.setEnabled(paramBoolean);
  }
  
  public final void j(boolean paramBoolean)
  {
    t.a((View)a, paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.newconversation.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */