package com.truecaller.messaging;

public final class e
{
  public final int a;
  public final int b;
  public final int c;
  public final int d;
  public final int e;
  
  public e(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    a = paramInt1;
    b = paramInt2;
    c = paramInt3;
    d = paramInt4;
    e = paramInt5;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof e;
      if (bool2)
      {
        paramObject = (e)paramObject;
        int i = a;
        int j = a;
        if (i == j) {
          i = 1;
        } else {
          i = 0;
        }
        if (i != 0)
        {
          i = b;
          j = b;
          if (i == j) {
            i = 1;
          } else {
            i = 0;
          }
          if (i != 0)
          {
            i = c;
            j = c;
            if (i == j) {
              i = 1;
            } else {
              i = 0;
            }
            if (i != 0)
            {
              i = d;
              j = d;
              if (i == j) {
                i = 1;
              } else {
                i = 0;
              }
              if (i != 0)
              {
                i = e;
                int k = e;
                if (i == k)
                {
                  k = 1;
                }
                else
                {
                  k = 0;
                  paramObject = null;
                }
                if (k != 0) {
                  return bool1;
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = a * 31;
    int j = b;
    i = (i + j) * 31;
    j = c;
    i = (i + j) * 31;
    j = d;
    i = (i + j) * 31;
    j = e;
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("SendOptionItem(id=");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(", backgroundTint=");
    i = b;
    localStringBuilder.append(i);
    localStringBuilder.append(", icon=");
    i = c;
    localStringBuilder.append(i);
    localStringBuilder.append(", tintColor=");
    i = d;
    localStringBuilder.append(i);
    localStringBuilder.append(", title=");
    i = e;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */