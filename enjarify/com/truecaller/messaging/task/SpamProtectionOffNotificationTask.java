package com.truecaller.messaging.task;

import android.content.Context;
import android.os.Bundle;
import android.text.format.DateUtils;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.background.e;
import com.truecaller.common.background.e.a;
import com.truecaller.messaging.h;
import com.truecaller.messaging.notifications.a;
import com.truecaller.util.al;
import com.truecaller.utils.d;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class SpamProtectionOffNotificationTask
  extends PersistentBackgroundTask
{
  public final int a()
  {
    return 10012;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    paramBundle = ((bk)paramContext.getApplicationContext()).a();
    d locald = paramBundle.bx();
    boolean bool1 = locald.d();
    int j = 0;
    int k = 1;
    if (bool1)
    {
      paramBundle = "SpamProtectionOff: we are default!";
      new String[1][0] = paramBundle;
    }
    else
    {
      paramBundle = paramBundle.C();
      i = paramBundle.v();
      long l1 = paramBundle.x();
      paramBundle = new String[k];
      Object localObject1 = Locale.getDefault();
      String str = "SpamProtectionOff: timesToShow=%d, latestShowtime=%d, hourOfDay=%d";
      int m = 3;
      Object[] arrayOfObject = new Object[m];
      Object localObject2 = Integer.valueOf(i);
      arrayOfObject[0] = localObject2;
      localObject2 = Long.valueOf(l1);
      arrayOfObject[k] = localObject2;
      int n = 2;
      Object localObject3 = Calendar.getInstance();
      int i1 = 11;
      int i2 = ((Calendar)localObject3).get(i1);
      localObject3 = Integer.valueOf(i2);
      arrayOfObject[n] = localObject3;
      localObject1 = String.format((Locale)localObject1, str, arrayOfObject);
      paramBundle[0] = localObject1;
      if (i > 0)
      {
        boolean bool2 = DateUtils.isToday(l1);
        if (!bool2)
        {
          paramBundle = Calendar.getInstance();
          int i3 = paramBundle.get(i1);
          if (i1 <= i3)
          {
            i = 21;
            if (i3 < i) {
              j = 1;
            }
          }
        }
      }
    }
    if (j == 0)
    {
      new String[1][0] = "SpamProtectionOff: run without notification";
      return PersistentBackgroundTask.RunResult.FailedSkip;
    }
    new String[1][0] = "SpamProtectionOff: show notification";
    paramContext = ((bk)paramContext.getApplicationContext()).a();
    paramBundle = paramContext.C();
    int i = paramBundle.v() - k;
    paramBundle.f(i);
    long l2 = System.currentTimeMillis();
    paramBundle.c(l2);
    ((a)paramContext.M().a()).e();
    return PersistentBackgroundTask.RunResult.Success;
  }
  
  public final boolean a(Context paramContext)
  {
    paramContext = ((bk)paramContext.getApplicationContext()).a().t();
    boolean bool1 = paramContext.b();
    if (bool1)
    {
      boolean bool2 = paramContext.a();
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final e b()
  {
    e.a locala = new com/truecaller/common/background/e$a;
    locala.<init>(1);
    TimeUnit localTimeUnit = TimeUnit.HOURS;
    locala = locala.a(6, localTimeUnit);
    localTimeUnit = TimeUnit.HOURS;
    locala = locala.b(1L, localTimeUnit);
    c = false;
    return locala.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.task.SpamProtectionOffNotificationTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */