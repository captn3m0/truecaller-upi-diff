package com.truecaller.messaging.task;

import android.content.Context;
import android.os.Bundle;
import c.u;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.background.e;
import com.truecaller.common.background.e.a;
import java.util.concurrent.TimeUnit;

public final class ReportSpamUrlTask
  extends PersistentBackgroundTask
{
  public final int a()
  {
    return 10030;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    paramBundle = "serviceContext";
    c.g.b.k.b(paramContext, paramBundle);
    paramContext = paramContext.getApplicationContext();
    if (paramContext != null)
    {
      paramContext = ((TrueApp)paramContext).a().bN();
      c.g.b.k.a(paramContext, "(serviceContext.applicat…tsGraph.spamLinkManager()");
      boolean bool = paramContext.b();
      int i = 1;
      paramBundle = new String[i];
      String str1 = String.valueOf(bool);
      String str2 = "spam upload task result is ".concat(str1);
      paramBundle[0] = str2;
      if (bool) {
        return PersistentBackgroundTask.RunResult.Success;
      }
      return PersistentBackgroundTask.RunResult.FailedRetry;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.TrueApp");
    throw paramContext;
  }
  
  public final boolean a(Context paramContext)
  {
    c.g.b.k.b(paramContext, "serviceContext");
    return e(paramContext);
  }
  
  public final e b()
  {
    Object localObject = new com/truecaller/common/background/e$a;
    ((e.a)localObject).<init>(0);
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    localObject = ((e.a)localObject).a(localTimeUnit).a(1).b();
    c.g.b.k.a(localObject, "TaskConfiguration.Builde…ANY)\n            .build()");
    return (e)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.task.ReportSpamUrlTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */