package com.truecaller.messaging.c;

import c.g.b.k;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.common.h.an;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.multisim.h;
import java.util.concurrent.TimeUnit;

public final class b
  implements a
{
  private final d a;
  private final com.truecaller.analytics.b b;
  private final h c;
  private final an d;
  
  public b(d paramd, com.truecaller.analytics.b paramb, h paramh, an paraman)
  {
    a = paramd;
    b = paramb;
    c = paramh;
    d = paraman;
  }
  
  private static String a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return "Unknown";
    case 2: 
      return "im";
    case 1: 
      return "mms";
    }
    return "sms";
  }
  
  private static String a(long paramLong)
  {
    TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
    paramLong = localTimeUnit.toSeconds(paramLong);
    int i = (int)paramLong;
    int j = 2;
    if ((i >= 0) && (j >= i)) {
      return "0-2";
    }
    int k = 5;
    if ((j <= i) && (k >= i)) {
      return "2-5";
    }
    j = 10;
    if ((k <= i) && (j >= i)) {
      return "5-10";
    }
    k = 20;
    if ((j <= i) && (k >= i)) {
      return "10-20";
    }
    j = 30;
    if ((k <= i) && (j >= i)) {
      return "20-30";
    }
    k = 40;
    if ((j <= i) && (k >= i)) {
      return "30-40";
    }
    j = 50;
    if ((k <= i) && (j >= i)) {
      return "40-50";
    }
    k = 60;
    if ((j <= i) && (k >= i)) {
      return "50-60";
    }
    j = 90;
    if ((k <= i) && (j >= i)) {
      return "60-90";
    }
    k = 120;
    if ((j <= i) && (k >= i)) {
      return "90-120";
    }
    j = 180;
    if ((k <= i) && (j >= i)) {
      return "120-180";
    }
    k = 240;
    if ((j <= i) && (k >= i)) {
      return "180-240";
    }
    j = 480;
    if ((k <= i) && (j >= i)) {
      return "240-480";
    }
    return ">480";
  }
  
  public final void a(String paramString)
  {
    if (paramString != null)
    {
      a.b(paramString);
      return;
    }
  }
  
  public final void a(String paramString, Message paramMessage, int paramInt)
  {
    k.b(paramString, "status");
    k.b(paramMessage, "message");
    Object localObject1 = new com/truecaller/analytics/e$a;
    ((e.a)localObject1).<init>("MessageSendResult");
    Object localObject2 = a(paramInt);
    localObject1 = ((e.a)localObject1).a("Type", (String)localObject2).a("Status", paramString);
    Object localObject3 = "Sim";
    localObject2 = c;
    boolean bool1 = ((h)localObject2).j();
    if (bool1) {
      localObject2 = "Multi";
    } else {
      localObject2 = "Single";
    }
    localObject1 = ((e.a)localObject1).a((String)localObject3, (String)localObject2);
    localObject2 = l;
    localObject1 = ((e.a)localObject1).a("SimToken", (String)localObject2);
    localObject2 = c.a();
    localObject1 = ((e.a)localObject1).a("MultiSimConfig", (String)localObject2);
    int i = t;
    localObject1 = ((e.a)localObject1).a("RetryCount", i);
    k.a(localObject1, "AnalyticsEvent.Builder(A…OUNT, message.retryCount)");
    localObject3 = o;
    if (localObject3 != null)
    {
      localObject2 = a;
      long l1 = d.a();
      String str1 = "id";
      k.a(localObject3, str1);
      long l2 = ((d)localObject2).b(l1, (String)localObject3);
      localObject2 = Long.valueOf(l2);
      Object localObject4 = localObject2;
      localObject4 = (Number)localObject2;
      l1 = ((Number)localObject4).longValue();
      int j = 1;
      long l3 = 0L;
      boolean bool2 = l1 < l3;
      if (bool2)
      {
        bool3 = true;
      }
      else
      {
        bool3 = false;
        localObject4 = null;
      }
      String str2 = null;
      if (!bool3)
      {
        i = 0;
        localObject2 = null;
      }
      String str3;
      if (localObject2 != null)
      {
        l2 = ((Number)localObject2).longValue();
        str3 = "FullTimeInterval";
        localObject2 = a(l2);
        ((e.a)localObject1).a(str3, (String)localObject2);
      }
      l2 = a.e((String)localObject3);
      localObject2 = Long.valueOf(l2);
      localObject4 = localObject2;
      localObject4 = (Number)localObject2;
      long l4 = ((Number)localObject4).longValue();
      boolean bool3 = l4 < l3;
      if (bool3)
      {
        bool3 = true;
      }
      else
      {
        bool3 = false;
        localObject4 = null;
      }
      if (!bool3)
      {
        i = 0;
        localObject2 = null;
      }
      if (localObject2 != null)
      {
        l2 = ((Number)localObject2).longValue();
        str3 = "ScheduleTimeInterval";
        localObject2 = a(l2);
        ((e.a)localObject1).a(str3, (String)localObject2);
      }
      l2 = a.f((String)localObject3);
      localObject2 = Long.valueOf(l2);
      localObject4 = localObject2;
      localObject4 = (Number)localObject2;
      l4 = ((Number)localObject4).longValue();
      bool3 = l4 < l3;
      if (bool3)
      {
        bool3 = true;
      }
      else
      {
        bool3 = false;
        localObject4 = null;
      }
      if (!bool3)
      {
        i = 0;
        localObject2 = null;
      }
      if (localObject2 != null)
      {
        l2 = ((Number)localObject2).longValue();
        str3 = "EnqueueTimeInterval";
        localObject2 = a(l2);
        ((e.a)localObject1).a(str3, (String)localObject2);
      }
      localObject2 = a;
      l4 = d.a();
      l2 = ((d)localObject2).a(l4, (String)localObject3);
      localObject2 = Long.valueOf(l2);
      localObject4 = localObject2;
      localObject4 = (Number)localObject2;
      l4 = ((Number)localObject4).longValue();
      bool3 = l4 < l3;
      if (!bool3)
      {
        j = 0;
        str1 = null;
      }
      if (j == 0)
      {
        i = 0;
        localObject2 = null;
      }
      if (localObject2 != null)
      {
        l2 = ((Number)localObject2).longValue();
        str2 = "SendTimeInterval";
        str1 = a(l2);
        ((e.a)localObject1).a(str2, str1);
        double d1 = l2;
        localObject2 = Double.valueOf(d1);
        ((e.a)localObject1).a((Double)localObject2);
      }
      localObject2 = a;
      ((d)localObject2).g((String)localObject3);
    }
    localObject3 = b;
    localObject1 = ((e.a)localObject1).a();
    localObject2 = "event.build()";
    k.a(localObject1, (String)localObject2);
    ((com.truecaller.analytics.b)localObject3).a((e)localObject1);
    int k = 2;
    if (paramInt == k)
    {
      Object localObject5 = new com/truecaller/analytics/e$a;
      ((e.a)localObject5).<init>("IMMessage");
      paramString = ((e.a)localObject5).a("Status", paramString);
      localObject1 = "Sent";
      paramString = paramString.a("Action", (String)localObject1);
      int m = t;
      paramString = paramString.a("RetryCount", m);
      k.a(paramString, "AnalyticsEvent.Builder(A…OUNT, message.retryCount)");
      paramMessage = b;
      paramString = paramString.a();
      localObject5 = "imEvent.build()";
      k.a(paramString, (String)localObject5);
      paramMessage.a(paramString);
    }
  }
  
  public final void a(String paramString1, String paramString2, int paramInt, Participant[] paramArrayOfParticipant, String paramString3)
  {
    k.b(paramString2, "userInteraction");
    Object localObject = "participants";
    k.b(paramArrayOfParticipant, (String)localObject);
    if (paramString1 != null)
    {
      localObject = a;
      ((d)localObject).a(paramString1);
    }
    int i = paramArrayOfParticipant.length;
    int k = 1;
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      paramString1 = null;
    }
    if (i == 0)
    {
      i = paramArrayOfParticipant.length;
      if (i > k)
      {
        paramString1 = "Group";
        break label107;
      }
      paramString1 = paramArrayOfParticipant[0];
      boolean bool = paramString1.d();
      if (bool)
      {
        paramString1 = "Phonebook";
        break label107;
      }
    }
    paramString1 = "Unknown";
    label107:
    paramArrayOfParticipant = new com/truecaller/analytics/e$a;
    paramArrayOfParticipant.<init>("MessageInitiated");
    paramString2 = paramArrayOfParticipant.a("Context", paramString2);
    localObject = a(paramInt);
    paramString1 = paramString2.a("Type", (String)localObject).a("Participant", paramString1).a();
    paramString2 = b;
    paramArrayOfParticipant = "event";
    k.a(paramString1, paramArrayOfParticipant);
    paramString2.a(paramString1);
    int j = 2;
    if (paramInt == j)
    {
      paramString1 = new com/truecaller/analytics/e$a;
      paramString1.<init>("IMMessage");
      paramString2 = "Action";
      String str = "Initiated";
      paramString1 = paramString1.a(paramString2, str);
      if (paramString3 != null)
      {
        paramString2 = "InitiatedVia";
        paramString1.a(paramString2, paramString3);
      }
      paramString2 = b;
      paramString1 = paramString1.a();
      str = "imEvent.build()";
      k.a(paramString1, str);
      paramString2.a(paramString1);
    }
  }
  
  public final void b(String paramString)
  {
    if (paramString != null)
    {
      a.c(paramString);
      return;
    }
  }
  
  public final void c(String paramString)
  {
    if (paramString != null)
    {
      a.d(paramString);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */