package com.truecaller.messaging.c;

import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private c(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
  }
  
  public static c a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    c localc = new com/truecaller/messaging/c/c;
    localc.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */