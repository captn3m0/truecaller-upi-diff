package com.truecaller.messaging.data;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.net.Uri;
import android.net.Uri.Builder;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.content.TruecallerContract.ac;
import com.truecaller.content.TruecallerContract.ae;
import com.truecaller.content.TruecallerContract.aj;
import com.truecaller.content.TruecallerContract.al;
import com.truecaller.content.TruecallerContract.o;
import com.truecaller.content.TruecallerContract.w;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.transport.history.HistoryTransportInfo;
import com.truecaller.messaging.transport.im.ImTransportInfo;
import com.truecaller.messaging.transport.im.ImTransportInfo.a;
import com.truecaller.messaging.transport.mms.MmsTransportInfo;
import com.truecaller.messaging.transport.mms.MmsTransportInfo.a;
import com.truecaller.messaging.transport.sms.SmsTransportInfo;
import com.truecaller.messaging.transport.sms.SmsTransportInfo.a;
import com.truecaller.messaging.transport.status.StatusTransportInfo;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.a.a.a.g;

public final class b
{
  public static int a(List paramList, int paramInt, ImTransportInfo paramImTransportInfo)
  {
    Object localObject = ContentProviderOperation.newDelete(TruecallerContract.w.a());
    String[] arrayOfString = new String[1];
    ((ContentProviderOperation.Builder)localObject).withSelection("message_id=?", arrayOfString);
    ((ContentProviderOperation.Builder)localObject).withSelectionBackReference(0, paramInt);
    localObject = ((ContentProviderOperation.Builder)localObject).build();
    paramList.add(localObject);
    localObject = ContentProviderOperation.newInsert(TruecallerContract.w.a());
    a((ContentProviderOperation.Builder)localObject, paramImTransportInfo);
    ((ContentProviderOperation.Builder)localObject).withValueBackReference("message_id", paramInt);
    paramInt = paramList.size();
    paramImTransportInfo = ((ContentProviderOperation.Builder)localObject).build();
    paramList.add(paramImTransportInfo);
    return paramInt;
  }
  
  public static int a(List paramList, int paramInt, StatusTransportInfo paramStatusTransportInfo)
  {
    ContentProviderOperation.Builder localBuilder = ContentProviderOperation.newInsert(TruecallerContract.al.a());
    a(localBuilder, paramStatusTransportInfo);
    localBuilder.withValueBackReference("message_id", paramInt);
    paramInt = paramList.size();
    paramStatusTransportInfo = localBuilder.build();
    paramList.add(paramStatusTransportInfo);
    return paramInt;
  }
  
  public static int a(List paramList, Participant paramParticipant)
  {
    Object localObject = ContentProviderOperation.newInsert(TruecallerContract.ae.a());
    Integer localInteger = Integer.valueOf(c);
    ContentProviderOperation.Builder localBuilder = ((ContentProviderOperation.Builder)localObject).withValue("type", localInteger);
    String str = e;
    localBuilder = localBuilder.withValue("raw_destination", str);
    str = f;
    localBuilder = localBuilder.withValue("normalized_destination", str);
    str = g;
    localBuilder = localBuilder.withValue("country_code", str);
    str = d;
    localBuilder = localBuilder.withValue("tc_im_peer_id", str);
    paramParticipant = Long.valueOf(i);
    localBuilder.withValue("aggregated_contact_id", paramParticipant);
    int i = paramList.size();
    localObject = ((ContentProviderOperation.Builder)localObject).build();
    paramList.add(localObject);
    return i;
  }
  
  private static int a(List paramList, HistoryTransportInfo paramHistoryTransportInfo, long paramLong)
  {
    int i = 1;
    long l = 0L;
    boolean bool1 = paramLong < l;
    boolean bool2;
    if (bool1)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      localBuilder = null;
    }
    String[] arrayOfString = new String[0];
    AssertionUtil.isTrue(bool2, arrayOfString);
    ContentProviderOperation.Builder localBuilder = ContentProviderOperation.newDelete(TruecallerContract.o.a());
    Object localObject1 = new String[i];
    String str = String.valueOf(paramLong);
    localObject1[0] = str;
    localBuilder.withSelection("message_id=?", (String[])localObject1);
    localObject1 = localBuilder.build();
    paramList.add(localObject1);
    localObject1 = ContentProviderOperation.newInsert(TruecallerContract.o.a());
    a((ContentProviderOperation.Builder)localObject1, paramHistoryTransportInfo);
    Object localObject2 = Long.valueOf(paramLong);
    ((ContentProviderOperation.Builder)localObject1).withValue("message_id", localObject2);
    int j = paramList.size();
    localObject2 = ((ContentProviderOperation.Builder)localObject1).build();
    paramList.add(localObject2);
    return j;
  }
  
  public static int a(List paramList, ImTransportInfo paramImTransportInfo)
  {
    long l1 = a;
    int i = 1;
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    boolean bool2;
    if (bool1)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      localObject = null;
    }
    String[] arrayOfString1 = new String[0];
    AssertionUtil.isTrue(bool2, arrayOfString1);
    Object localObject = ContentProviderOperation.newDelete(TruecallerContract.w.a());
    String[] arrayOfString2 = new String[i];
    String str = String.valueOf(a);
    arrayOfString2[0] = str;
    ((ContentProviderOperation.Builder)localObject).withSelection("message_id=?", arrayOfString2);
    localObject = ((ContentProviderOperation.Builder)localObject).build();
    paramList.add(localObject);
    localObject = ContentProviderOperation.newInsert(TruecallerContract.w.a());
    a((ContentProviderOperation.Builder)localObject, paramImTransportInfo);
    paramImTransportInfo = Long.valueOf(a);
    ((ContentProviderOperation.Builder)localObject).withValue("message_id", paramImTransportInfo);
    int j = paramList.size();
    localObject = ((ContentProviderOperation.Builder)localObject).build();
    paramList.add(localObject);
    return j;
  }
  
  private static int a(List paramList, MmsTransportInfo paramMmsTransportInfo)
  {
    long l1 = a;
    int i = 1;
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    boolean bool2;
    if (bool1)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      localObject = null;
    }
    String[] arrayOfString1 = new String[0];
    AssertionUtil.isTrue(bool2, arrayOfString1);
    Object localObject = ContentProviderOperation.newDelete(TruecallerContract.ac.a());
    String[] arrayOfString2 = new String[i];
    String str = String.valueOf(a);
    arrayOfString2[0] = str;
    ((ContentProviderOperation.Builder)localObject).withSelection("message_id=?", arrayOfString2);
    localObject = ((ContentProviderOperation.Builder)localObject).build();
    paramList.add(localObject);
    localObject = ContentProviderOperation.newInsert(TruecallerContract.ac.a());
    a((ContentProviderOperation.Builder)localObject, paramMmsTransportInfo);
    paramMmsTransportInfo = Long.valueOf(a);
    ((ContentProviderOperation.Builder)localObject).withValue("message_id", paramMmsTransportInfo);
    int j = paramList.size();
    localObject = ((ContentProviderOperation.Builder)localObject).build();
    paramList.add(localObject);
    return j;
  }
  
  private static int a(List paramList, SmsTransportInfo paramSmsTransportInfo)
  {
    long l1 = a;
    int i = 1;
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    boolean bool2;
    if (bool1)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      localObject = null;
    }
    String[] arrayOfString1 = new String[0];
    AssertionUtil.isTrue(bool2, arrayOfString1);
    Object localObject = ContentProviderOperation.newDelete(TruecallerContract.aj.a());
    String[] arrayOfString2 = new String[i];
    String str = String.valueOf(a);
    arrayOfString2[0] = str;
    ((ContentProviderOperation.Builder)localObject).withSelection("message_id=?", arrayOfString2);
    localObject = ((ContentProviderOperation.Builder)localObject).build();
    paramList.add(localObject);
    localObject = ContentProviderOperation.newInsert(TruecallerContract.aj.a());
    a((ContentProviderOperation.Builder)localObject, paramSmsTransportInfo);
    paramSmsTransportInfo = Long.valueOf(a);
    ((ContentProviderOperation.Builder)localObject).withValue("message_id", paramSmsTransportInfo);
    int j = paramList.size();
    localObject = ((ContentProviderOperation.Builder)localObject).build();
    paramList.add(localObject);
    return j;
  }
  
  private static int a(List paramList, StatusTransportInfo paramStatusTransportInfo, long paramLong)
  {
    Object localObject1 = null;
    long l = 0L;
    boolean bool1 = paramLong < l;
    boolean bool2;
    if (bool1) {
      bool2 = true;
    } else {
      bool2 = false;
    }
    localObject1 = new String[0];
    AssertionUtil.isTrue(bool2, (String[])localObject1);
    localObject1 = ContentProviderOperation.newInsert(TruecallerContract.al.a());
    a((ContentProviderOperation.Builder)localObject1, paramStatusTransportInfo);
    Object localObject2 = Long.valueOf(paramLong);
    ((ContentProviderOperation.Builder)localObject1).withValue("message_id", localObject2);
    int i = paramList.size();
    localObject2 = ((ContentProviderOperation.Builder)localObject1).build();
    paramList.add(localObject2);
    return i;
  }
  
  public static int a(List paramList, Set paramSet)
  {
    boolean bool1 = paramSet.isEmpty();
    Object localObject1 = { "Participants set should never be empty" };
    AssertionUtil.AlwaysFatal.isFalse(bool1, (String[])localObject1);
    int i = paramSet.size();
    String[] arrayOfString = new String[i];
    localObject1 = paramSet.iterator();
    int j = 0;
    int k = 0;
    Object localObject2 = null;
    Object localObject3;
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = (Participant)((Iterator)localObject1).next();
      int m = k + 1;
      localObject3 = f;
      arrayOfString[k] = localObject3;
      k = m;
    }
    paramSet = (Participant)paramSet.iterator().next();
    int n = c;
    k = 4;
    if (n == k) {
      paramSet = f;
    } else {
      paramSet = null;
    }
    n = arrayOfString.length;
    if (n == 0)
    {
      n = 1;
    }
    else
    {
      n = 0;
      localObject1 = null;
    }
    localObject2 = new String[] { "Participants set should never be empty" };
    AssertionUtil.AlwaysFatal.isFalse(n, (String[])localObject2);
    localObject1 = TruecallerContract.b.buildUpon();
    localObject2 = "msg/msg_conversations";
    localObject1 = ((Uri.Builder)localObject1).appendEncodedPath((String)localObject2);
    k = arrayOfString.length;
    while (j < k)
    {
      localObject3 = arrayOfString[j];
      String str = "addr";
      ((Uri.Builder)localObject1).appendQueryParameter(str, (String)localObject3);
      j += 1;
    }
    paramSet = ContentProviderOperation.newInsert(((Uri.Builder)localObject1).build()).withValue("tc_group_id", paramSet);
    i = paramList.size();
    paramSet = paramSet.build();
    paramList.add(paramSet);
    return i;
  }
  
  private static void a(ContentProviderOperation.Builder paramBuilder, HistoryTransportInfo paramHistoryTransportInfo)
  {
    Object localObject = Long.valueOf(b);
    paramBuilder.withValue("raw_id", localObject);
    localObject = Integer.valueOf(c);
    paramBuilder.withValue("call_log_id", localObject);
    localObject = Integer.valueOf(e);
    paramBuilder.withValue("features", localObject);
    localObject = f;
    paramBuilder.withValue("number_type", localObject);
    paramHistoryTransportInfo = Integer.valueOf(d);
    paramBuilder.withValue("type", paramHistoryTransportInfo);
  }
  
  private static void a(ContentProviderOperation.Builder paramBuilder, ImTransportInfo paramImTransportInfo)
  {
    Object localObject = b;
    paramBuilder.withValue("raw_id", localObject);
    localObject = Integer.valueOf(c);
    paramBuilder.withValue("im_status", localObject);
    localObject = Integer.valueOf(d);
    paramBuilder.withValue("delivery_status", localObject);
    localObject = Integer.valueOf(e);
    paramBuilder.withValue("read_status", localObject);
    localObject = Integer.valueOf(f);
    paramBuilder.withValue("delivery_sync_status", localObject);
    localObject = Integer.valueOf(g);
    paramBuilder.withValue("read_sync_status", localObject);
    localObject = Integer.valueOf(h);
    paramBuilder.withValue("error_code", localObject);
    localObject = Integer.valueOf(i);
    paramBuilder.withValue("api_version", localObject);
    paramImTransportInfo = Long.valueOf(j);
    paramBuilder.withValue("random_id", paramImTransportInfo);
  }
  
  public static void a(ContentProviderOperation.Builder paramBuilder, MmsTransportInfo paramMmsTransportInfo)
  {
    Object localObject1 = e;
    Object localObject2 = new String[0];
    AssertionUtil.isNotNull(localObject1, (String[])localObject2);
    localObject2 = Long.valueOf(b);
    paramBuilder.withValue("raw_id", localObject2);
    localObject2 = Integer.valueOf(c);
    paramBuilder.withValue("raw_status", localObject2);
    localObject2 = e.toString();
    paramBuilder.withValue("message_uri", localObject2);
    long l1 = d;
    localObject2 = Long.valueOf(l1);
    paramBuilder.withValue("raw_thread_id", localObject2);
    localObject2 = Integer.valueOf(g);
    paramBuilder.withValue("type", localObject2);
    localObject2 = h;
    paramBuilder.withValue("subject", localObject2);
    int i = i;
    localObject2 = Integer.valueOf(i);
    paramBuilder.withValue("subject_charset", localObject2);
    localObject1 = l;
    if (localObject1 != null)
    {
      localObject1 = "content_location";
      localObject2 = l.toString();
      paramBuilder.withValue((String)localObject1, localObject2);
    }
    localObject2 = o;
    paramBuilder.withValue("transaction_id", localObject2);
    localObject1 = p;
    long l2 = a;
    long l3 = 0L;
    boolean bool = l2 < l3;
    if (bool)
    {
      String str = "expiry";
      long l4 = 1000L;
      l2 /= l4;
      localObject1 = Long.valueOf(l2);
      paramBuilder.withValue(str, localObject1);
    }
    localObject2 = Integer.valueOf(q);
    paramBuilder.withValue("priority", localObject2);
    localObject2 = Integer.valueOf(r);
    paramBuilder.withValue("retrieve_status", localObject2);
    localObject2 = Integer.valueOf(s);
    paramBuilder.withValue("response_status", localObject2);
    localObject2 = v;
    paramBuilder.withValue("mms_message_id", localObject2);
    localObject2 = Integer.valueOf(w);
    paramBuilder.withValue("message_box", localObject2);
    localObject2 = Integer.valueOf(x);
    paramBuilder.withValue("size", localObject2);
    localObject2 = Integer.valueOf(y);
    paramBuilder.withValue("delivery_report", localObject2);
    localObject2 = Long.valueOf(z);
    paramBuilder.withValue("delivery_time", localObject2);
    localObject2 = Integer.valueOf(f);
    paramBuilder.withValue("version", localObject2);
    localObject2 = j;
    paramBuilder.withValue("retrieve_text", localObject2);
    localObject2 = Integer.valueOf(k);
    paramBuilder.withValue("retrieve_text_charset", localObject2);
    localObject2 = m;
    paramBuilder.withValue("content_type", localObject2);
    localObject2 = Integer.valueOf(n);
    paramBuilder.withValue("content_class", localObject2);
    localObject2 = t;
    paramBuilder.withValue("response_text", localObject2);
    localObject2 = u;
    paramBuilder.withValue("message_class", localObject2);
    localObject2 = Integer.valueOf(A);
    paramBuilder.withValue("read_report", localObject2);
    localObject2 = Integer.valueOf(B);
    paramBuilder.withValue("read_status", localObject2);
    paramMmsTransportInfo = Boolean.valueOf(C);
    paramBuilder.withValue("report_allowed", paramMmsTransportInfo);
  }
  
  public static void a(ContentProviderOperation.Builder paramBuilder, SmsTransportInfo paramSmsTransportInfo)
  {
    Uri localUri = e;
    Object localObject = new String[0];
    AssertionUtil.isNotNull(localUri, (String[])localObject);
    localObject = Long.valueOf(b);
    paramBuilder.withValue("raw_id", localObject);
    localObject = Integer.valueOf(c);
    paramBuilder.withValue("raw_status", localObject);
    localObject = e.toString();
    paramBuilder.withValue("message_uri", localObject);
    localObject = Integer.valueOf(g);
    paramBuilder.withValue("protocol", localObject);
    localObject = Integer.valueOf(h);
    paramBuilder.withValue("type", localObject);
    localObject = i;
    paramBuilder.withValue("service_center", localObject);
    localObject = Integer.valueOf(j);
    paramBuilder.withValue("error_code", localObject);
    localObject = Boolean.valueOf(k);
    paramBuilder.withValue("reply_path_present", localObject);
    localObject = Long.valueOf(d);
    paramBuilder.withValue("raw_thread_id", localObject);
    localObject = f;
    paramBuilder.withValue("subject", localObject);
    paramSmsTransportInfo = l;
    paramBuilder.withValue("stripped_raw_address", paramSmsTransportInfo);
  }
  
  private static void a(ContentProviderOperation.Builder paramBuilder, StatusTransportInfo paramStatusTransportInfo)
  {
    paramStatusTransportInfo = a;
    paramBuilder.withValue("raw_id", paramStatusTransportInfo);
  }
  
  public static void a(List paramList, long paramLong)
  {
    ContentProviderOperation localContentProviderOperation = ContentProviderOperation.newDelete(TruecallerContract.aa.a(paramLong)).build();
    paramList.add(localContentProviderOperation);
  }
  
  public static void a(List paramList, Message paramMessage)
  {
    a(paramList, paramMessage, -1);
  }
  
  public static void a(List paramList, Message paramMessage, int paramInt)
  {
    boolean bool = paramMessage.b();
    Object localObject1 = { "Can update only already stored messages" };
    AssertionUtil.isTrue(bool, (String[])localObject1);
    int i = j;
    long l1;
    switch (i)
    {
    default: 
      paramList = new java/lang/RuntimeException;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("Unsupported transport for message: ");
      j = j;
      localStringBuilder.append(j);
      paramMessage = localStringBuilder.toString();
      paramList.<init>(paramMessage);
      throw paramList;
    case 6: 
      localObject2 = (StatusTransportInfo)m;
      l1 = a;
      a(paramList, (StatusTransportInfo)localObject2, l1);
      break;
    case 5: 
      localObject2 = (HistoryTransportInfo)m;
      l1 = a;
      a(paramList, (HistoryTransportInfo)localObject2, l1);
      break;
    case 2: 
      localObject2 = ((ImTransportInfo)m).g();
      l1 = a;
      a = l1;
      localObject2 = ((ImTransportInfo.a)localObject2).a();
      a(paramList, (ImTransportInfo)localObject2);
      break;
    case 1: 
      localObject2 = ((MmsTransportInfo)m).g();
      l1 = a;
      a = l1;
      localObject2 = ((MmsTransportInfo.a)localObject2).a();
      a(paramList, (MmsTransportInfo)localObject2);
      break;
    case 0: 
      localObject2 = ((SmsTransportInfo)m).g();
      l1 = a;
      a = l1;
      localObject2 = ((SmsTransportInfo.a)localObject2).a();
      a(paramList, (SmsTransportInfo)localObject2);
    }
    long l2 = a;
    Object localObject2 = ContentProviderOperation.newUpdate(TruecallerContract.aa.a(l2));
    Object localObject3 = Boolean.valueOf(h);
    ((ContentProviderOperation.Builder)localObject2).withValue("read", localObject3);
    localObject3 = Boolean.valueOf(g);
    ((ContentProviderOperation.Builder)localObject2).withValue("seen", localObject3);
    localObject3 = Boolean.valueOf(i);
    ((ContentProviderOperation.Builder)localObject2).withValue("locked", localObject3);
    localObject3 = Integer.valueOf(f);
    ((ContentProviderOperation.Builder)localObject2).withValue("status", localObject3);
    localObject3 = Long.valueOf(e.a);
    ((ContentProviderOperation.Builder)localObject2).withValue("date", localObject3);
    long l3 = d.a;
    localObject3 = Long.valueOf(l3);
    ((ContentProviderOperation.Builder)localObject2).withValue("date_sent", localObject3);
    int k = j;
    localObject3 = Integer.valueOf(k);
    ((ContentProviderOperation.Builder)localObject2).withValue("transport", localObject3);
    localObject1 = "retry_count";
    int j = t;
    paramMessage = Integer.valueOf(j);
    ((ContentProviderOperation.Builder)localObject2).withValue((String)localObject1, paramMessage);
    if (paramInt >= 0)
    {
      paramMessage = "conversation_id";
      ((ContentProviderOperation.Builder)localObject2).withValueBackReference(paramMessage, paramInt);
    }
    paramMessage = ((ContentProviderOperation.Builder)localObject2).build();
    paramList.add(paramMessage);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */