package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class u$u
  extends u
{
  private final long[] b;
  
  private u$u(e parame, long[] paramArrayOfLong)
  {
    super(parame);
    b = paramArrayOfLong;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".markMessagesRead(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */