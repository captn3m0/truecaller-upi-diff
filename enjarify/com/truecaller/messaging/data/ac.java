package com.truecaller.messaging.data;

import c.g.b.k;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;

public final class ac
  implements ab
{
  private final e a;
  
  public ac(e parame)
  {
    a = parame;
  }
  
  private static StringBuilder a()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("( ( spam_score<10 AND top_spammer_count = 0  AND blacklist_count = 0) OR white_list_count > 0)");
    k.a(localStringBuilder, "StringBuilder().append(\"…ST_COUNT).append(\" > 0)\")");
    return localStringBuilder;
  }
  
  public final String a(int paramInt)
  {
    Object localObject1 = a.w();
    boolean bool = ((b)localObject1).a();
    Object localObject2;
    if (bool)
    {
      switch (paramInt)
      {
      default: 
        localObject1 = new java/lang/IllegalArgumentException;
        localObject2 = String.valueOf(paramInt);
        localObject2 = "Unknown filter type ".concat((String)localObject2);
        ((IllegalArgumentException)localObject1).<init>((String)localObject2);
        throw ((Throwable)localObject1);
      case 4: 
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("\n                (outgoing_message_count > 0\n                OR phonebook_count > 0\n                OR tc_group_id IS NOT NULL)\n                AND ");
        localObject1 = a();
        ((StringBuilder)localObject2).append(localObject1);
        localObject1 = "\n            ";
        ((StringBuilder)localObject2).append((String)localObject1);
        localObject2 = (CharSequence)((StringBuilder)localObject2).toString();
        break;
      case 3: 
        localObject2 = (CharSequence)"\n            split_criteria = 1\n            OR blacklist_count > 0\n            AND white_list_count = 0\n    ";
        break;
      case 2: 
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("\n                outgoing_message_count = 0\n                AND phonebook_count = 0\n                AND tc_group_id IS NULL\n                AND latest_message_id IS NOT NULL\n                AND ");
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>("( blacklist_count= 0 OR white_list_count > 0 )");
        String str = "StringBuilder().append(\"…T_COUNT).append(\" > 0 )\")";
        k.a(localObject1, str);
        ((StringBuilder)localObject2).append(localObject1);
        localObject1 = "\n            ";
        ((StringBuilder)localObject2).append((String)localObject1);
        localObject2 = (CharSequence)((StringBuilder)localObject2).toString();
        break;
      case 1: 
        localObject2 = (CharSequence)"";
      }
      return localObject2.toString();
    }
    switch (paramInt)
    {
    default: 
      localObject1 = new java/lang/IllegalArgumentException;
      localObject2 = String.valueOf(paramInt);
      localObject2 = "Unknown filter type ".concat((String)localObject2);
      ((IllegalArgumentException)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    case 4: 
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("\n                (outgoing_message_count > 0\n                OR phonebook_count > 0\n                OR tc_group_id IS NOT NULL)\n                AND ");
      localObject1 = a();
      ((StringBuilder)localObject2).append(localObject1);
      localObject1 = "\n            ";
      ((StringBuilder)localObject2).append((String)localObject1);
      localObject2 = (CharSequence)((StringBuilder)localObject2).toString();
      break;
    case 3: 
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>(" ( ( spam_score>=10 OR top_spammer_count > 0  OR blacklist_count > 0) AND white_list_count = 0 )");
      localObject1 = "StringBuilder(\" ( ( \")\n …T_COUNT).append(\" = 0 )\")";
      k.a(localObject2, (String)localObject1);
      localObject2 = (CharSequence)localObject2;
      break;
    case 2: 
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("\n                outgoing_message_count = 0\n                AND phonebook_count = 0\n                AND tc_group_id IS NULL\n                AND ");
      localObject1 = a();
      ((StringBuilder)localObject2).append(localObject1);
      localObject1 = "\n            ";
      ((StringBuilder)localObject2).append((String)localObject1);
      localObject2 = (CharSequence)((StringBuilder)localObject2).toString();
      break;
    case 1: 
      localObject2 = (CharSequence)"";
    }
    return localObject2.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */