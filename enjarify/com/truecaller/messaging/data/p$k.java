package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class p$k
  extends u
{
  private final int b;
  
  private p$k(e parame, int paramInt)
  {
    super(parame);
    b = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".fetchNonBlockConversationCursor(");
    String str = a(Integer.valueOf(b), 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.p.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */