package com.truecaller.messaging.data;

import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.Draft;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import java.util.Set;
import org.a.a.b;

public abstract interface t
{
  public abstract w a(int paramInt);
  
  public abstract w a(long paramLong);
  
  public abstract w a(long paramLong, int paramInt1, int paramInt2, boolean paramBoolean);
  
  public abstract w a(Draft paramDraft);
  
  public abstract w a(Message paramMessage, int paramInt, String paramString);
  
  public abstract w a(Message paramMessage, long paramLong);
  
  public abstract w a(Message paramMessage, long paramLong, boolean paramBoolean);
  
  public abstract w a(Message paramMessage, Participant[] paramArrayOfParticipant, int paramInt);
  
  public abstract w a(String paramString);
  
  public abstract w a(b paramb);
  
  public abstract w a(boolean paramBoolean, long... paramVarArgs);
  
  public abstract w a(long[] paramArrayOfLong);
  
  public abstract w a(Conversation[] paramArrayOfConversation);
  
  public abstract w a(Conversation[] paramArrayOfConversation, boolean paramBoolean);
  
  public abstract void a();
  
  public abstract void a(int paramInt, b paramb);
  
  public abstract void a(int paramInt, b paramb, boolean paramBoolean);
  
  public abstract void a(long paramLong, int paramInt);
  
  public abstract void a(long paramLong, int paramInt1, int paramInt2);
  
  public abstract void a(t.a parama, int paramInt, Iterable paramIterable);
  
  public abstract void a(Message paramMessage);
  
  public abstract void a(Message paramMessage, String paramString1, String paramString2);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void a(boolean paramBoolean, Set paramSet);
  
  public abstract void a(Message[] paramArrayOfMessage, int paramInt);
  
  public abstract w b(b paramb);
  
  public abstract void b();
  
  public abstract void b(int paramInt);
  
  public abstract void b(long paramLong);
  
  public abstract void b(Message paramMessage);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void b(boolean paramBoolean, Set paramSet);
  
  public abstract void b(long[] paramArrayOfLong);
  
  public abstract w c(long paramLong);
  
  public abstract w c(Message paramMessage);
  
  public abstract void c(int paramInt);
  
  public abstract void c(long... paramVarArgs);
  
  public abstract w d(Message paramMessage);
  
  public abstract void d(long paramLong);
  
  public abstract w e(long paramLong);
  
  public abstract w e(Message paramMessage);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */