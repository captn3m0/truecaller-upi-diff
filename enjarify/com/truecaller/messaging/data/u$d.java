package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.messaging.data.types.Conversation;

final class u$d
  extends u
{
  private final Conversation[] b;
  private final boolean c;
  
  private u$d(e parame, Conversation[] paramArrayOfConversation, boolean paramBoolean)
  {
    super(parame);
    b = paramArrayOfConversation;
    c = paramBoolean;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".deleteConversations(");
    Object localObject = b;
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Boolean.valueOf(c), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */