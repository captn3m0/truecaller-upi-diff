package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import org.a.a.b;

final class u$ab
  extends u
{
  private final int b;
  private final b c;
  private final boolean d;
  
  private u$ab(e parame, int paramInt, b paramb, boolean paramBoolean)
  {
    super(parame);
    b = paramInt;
    c = paramb;
    d = paramBoolean;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".performPartialSync(");
    Object localObject = Integer.valueOf(b);
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Boolean.valueOf(d), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */