package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class u$f
  extends u
{
  private final boolean b;
  private final long[] c;
  
  private u$f(e parame, boolean paramBoolean, long[] paramArrayOfLong)
  {
    super(parame);
    b = paramBoolean;
    c = paramArrayOfLong;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".deleteMessages(");
    Object localObject = Boolean.valueOf(b);
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */