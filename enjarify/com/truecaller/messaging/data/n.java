package com.truecaller.messaging.data;

import android.database.Cursor;
import android.database.CursorWrapper;
import com.truecaller.messaging.data.a.g;
import com.truecaller.messaging.data.types.Entity;
import org.c.a.a.a.k;

final class n
  extends CursorWrapper
  implements g
{
  private final int a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  
  n(Cursor paramCursor)
  {
    super(paramCursor);
    int j = paramCursor.getColumnIndexOrThrow("_id");
    a = j;
    j = paramCursor.getColumnIndexOrThrow("type");
    b = j;
    j = paramCursor.getColumnIndexOrThrow("content");
    c = j;
    j = paramCursor.getColumnIndexOrThrow("width");
    d = j;
    j = paramCursor.getColumnIndexOrThrow("height");
    e = j;
    j = paramCursor.getColumnIndexOrThrow("duration");
    f = j;
    j = paramCursor.getColumnIndexOrThrow("status");
    g = j;
    j = paramCursor.getColumnIndexOrThrow("size");
    h = j;
    int k = paramCursor.getColumnIndexOrThrow("thumbnail");
    i = k;
  }
  
  public final Entity a()
  {
    int j = a;
    long l1 = getLong(j);
    j = b;
    String str1 = k.n(getString(j));
    j = g;
    int k = getInt(j);
    j = c;
    String str2 = k.n(getString(j));
    j = d;
    int m = getInt(j);
    j = e;
    int n = getInt(j);
    j = f;
    int i1 = getInt(j);
    j = h;
    long l2 = getLong(j);
    j = i;
    String str3 = k.n(getString(j));
    return Entity.a(l1, str1, k, str2, m, n, i1, l2, str3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */