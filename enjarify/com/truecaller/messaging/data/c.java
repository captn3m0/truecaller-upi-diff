package com.truecaller.messaging.data;

import android.database.Cursor;
import com.truecaller.messaging.data.a.a;
import com.truecaller.messaging.data.a.h;
import com.truecaller.messaging.data.a.j;
import com.truecaller.messaging.data.a.k;
import com.truecaller.messaging.data.a.m;
import com.truecaller.messaging.data.a.n;
import com.truecaller.messaging.data.a.p;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.a.s;

public abstract interface c
{
  public abstract a a(Cursor paramCursor);
  
  public abstract com.truecaller.messaging.data.a.c a(Cursor paramCursor, boolean paramBoolean);
  
  public abstract j b(Cursor paramCursor);
  
  public abstract m c(Cursor paramCursor);
  
  public abstract r d(Cursor paramCursor);
  
  public abstract s e(Cursor paramCursor);
  
  public abstract com.truecaller.messaging.data.a.g f(Cursor paramCursor);
  
  public abstract p g(Cursor paramCursor);
  
  public abstract k h(Cursor paramCursor);
  
  public abstract com.truecaller.messaging.transport.im.a.g i(Cursor paramCursor);
  
  public abstract h j(Cursor paramCursor);
  
  public abstract n k(Cursor paramCursor);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */