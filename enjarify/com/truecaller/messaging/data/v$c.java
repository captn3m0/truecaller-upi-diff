package com.truecaller.messaging.data;

import android.util.SparseArray;
import com.truecaller.messaging.transport.f;
import com.truecaller.messaging.transport.i;
import java.util.Locale;
import org.a.a.b;

final class v$c
  implements t.a
{
  final SparseArray a;
  private final f b;
  private final i c;
  private b d;
  private b e;
  private final boolean f;
  private final boolean g;
  
  v$c(f paramf, i parami, v.d paramd, boolean paramBoolean1, boolean paramBoolean2)
  {
    SparseArray localSparseArray = new android/util/SparseArray;
    localSparseArray.<init>();
    a = localSparseArray;
    b = paramf;
    c = parami;
    paramf = b;
    d = paramf;
    paramf = c;
    e = paramf;
    f = paramBoolean1;
    g = paramBoolean2;
  }
  
  public final f a()
  {
    return b;
  }
  
  public final b a(int paramInt)
  {
    return (b)a.get(paramInt);
  }
  
  public final void a(int paramInt, b paramb)
  {
    a.put(paramInt, paramb);
  }
  
  public final boolean a(b paramb1, b paramb2)
  {
    b localb = d;
    boolean bool1 = localb.c(paramb1);
    boolean bool2;
    if (bool1)
    {
      d = paramb1;
      paramb1 = a;
      paramb1.clear();
      bool2 = true;
    }
    else
    {
      bool2 = false;
      paramb1 = null;
    }
    localb = e;
    bool1 = paramb2.c(localb);
    if (bool1)
    {
      e = paramb2;
      bool2 = true;
    }
    return bool2;
  }
  
  public final i b()
  {
    return c;
  }
  
  public final b c()
  {
    return e;
  }
  
  public final b d()
  {
    return d;
  }
  
  public final boolean e()
  {
    return f;
  }
  
  public final boolean f()
  {
    return g;
  }
  
  public final String toString()
  {
    Locale localLocale = Locale.US;
    Object[] arrayOfObject = new Object[12];
    Integer localInteger = Integer.valueOf(d.j());
    arrayOfObject[0] = localInteger;
    localInteger = Integer.valueOf(d.k());
    arrayOfObject[1] = localInteger;
    localInteger = Integer.valueOf(d.l());
    arrayOfObject[2] = localInteger;
    localInteger = Integer.valueOf(d.i());
    arrayOfObject[3] = localInteger;
    localInteger = Integer.valueOf(d.h());
    arrayOfObject[4] = localInteger;
    localInteger = Integer.valueOf(d.g());
    arrayOfObject[5] = localInteger;
    localInteger = Integer.valueOf(e.j());
    arrayOfObject[6] = localInteger;
    localInteger = Integer.valueOf(e.k());
    arrayOfObject[7] = localInteger;
    localInteger = Integer.valueOf(e.l());
    arrayOfObject[8] = localInteger;
    localInteger = Integer.valueOf(e.i());
    arrayOfObject[9] = localInteger;
    localInteger = Integer.valueOf(e.h());
    arrayOfObject[10] = localInteger;
    localInteger = Integer.valueOf(e.g());
    arrayOfObject[11] = localInteger;
    return String.format(localLocale, "Batch from %1$02d:%2$02d:%3$02d %4$02d/%5$02d/%6$04d to %7$02d:%8$02d:%9$02d %10$02d/%11$02d/%12$04d", arrayOfObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.v.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */