package com.truecaller.messaging.data;

import com.truecaller.androidactors.h;
import java.util.concurrent.TimeUnit;

public class MessagesDataService
  extends h
{
  private static final long b = TimeUnit.SECONDS.toMillis(30);
  
  public MessagesDataService()
  {
    super("messages-storage", l, true);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.MessagesDataService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */