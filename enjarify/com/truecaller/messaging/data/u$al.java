package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.messaging.data.types.Message;

final class u$al
  extends u
{
  private final Message b;
  private final long c;
  
  private u$al(e parame, Message paramMessage, long paramLong)
  {
    super(parame);
    b = paramMessage;
    c = paramLong;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".updateMessageDate(");
    Object localObject = b;
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Long.valueOf(c), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u.al
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */