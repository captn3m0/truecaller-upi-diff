package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.List;

final class p$m
  extends u
{
  private final List b;
  
  private p$m(e parame, List paramList)
  {
    super(parame);
    b = paramList;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".hasMessageWithPublicEntities(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.p.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */