package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;

final class u$af
  extends u
{
  private final Message b;
  private final Participant[] c;
  private final int d;
  
  private u$af(e parame, Message paramMessage, Participant[] paramArrayOfParticipant, int paramInt)
  {
    super(parame);
    b = paramMessage;
    c = paramArrayOfParticipant;
    d = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".scheduleMessage(");
    Object localObject = b;
    int i = 1;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(d), 2);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u.af
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */