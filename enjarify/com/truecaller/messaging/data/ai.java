package com.truecaller.messaging.data;

import android.database.Cursor;
import android.database.CursorWrapper;
import com.truecaller.messaging.data.a.r;

final class ai
  extends CursorWrapper
  implements r
{
  static final String[] a = tmp82_66;
  
  static
  {
    String[] tmp5_2 = new String[16];
    String[] tmp6_5 = tmp5_2;
    String[] tmp6_5 = tmp5_2;
    tmp6_5[0] = "_id";
    tmp6_5[1] = "date";
    String[] tmp15_6 = tmp6_5;
    String[] tmp15_6 = tmp6_5;
    tmp15_6[2] = "raw_id";
    tmp15_6[3] = "raw_status";
    String[] tmp24_15 = tmp15_6;
    String[] tmp24_15 = tmp15_6;
    tmp24_15[4] = "seen";
    tmp24_15[5] = "read";
    String[] tmp33_24 = tmp24_15;
    String[] tmp33_24 = tmp24_15;
    tmp33_24[6] = "locked";
    tmp33_24[7] = "status";
    String[] tmp44_33 = tmp33_24;
    String[] tmp44_33 = tmp33_24;
    tmp44_33[8] = "response_status";
    tmp44_33[9] = "retrieve_status";
    String[] tmp55_44 = tmp44_33;
    String[] tmp55_44 = tmp44_33;
    tmp55_44[10] = "delivery_report";
    tmp55_44[11] = "raw_thread";
    String[] tmp66_55 = tmp55_44;
    String[] tmp66_55 = tmp55_44;
    tmp66_55[12] = "stripped_raw_address";
    tmp66_55[13] = "category";
    tmp66_55[14] = "sync_status";
    String[] tmp82_66 = tmp66_55;
    tmp82_66[15] = "classification";
  }
  
  ai(Cursor paramCursor)
  {
    super(paramCursor);
  }
  
  public final long a()
  {
    return getLong(0);
  }
  
  public final long b()
  {
    return getLong(2);
  }
  
  public final long c()
  {
    return getInt(3);
  }
  
  public final long d()
  {
    return getLong(11);
  }
  
  public final long e()
  {
    return getLong(1);
  }
  
  public final boolean f()
  {
    int i = getInt(4);
    return i != 0;
  }
  
  public final boolean g()
  {
    int i = getInt(5);
    return i != 0;
  }
  
  public final boolean h()
  {
    int i = getInt(6);
    return i != 0;
  }
  
  public final int i()
  {
    return getInt(7);
  }
  
  public final int j()
  {
    return getInt(8);
  }
  
  public final int k()
  {
    return getInt(9);
  }
  
  public final int l()
  {
    return getInt(10);
  }
  
  public final String m()
  {
    return getString(12);
  }
  
  public final int n()
  {
    return getInt(14);
  }
  
  public final int o()
  {
    return getInt(15);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.ai
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */