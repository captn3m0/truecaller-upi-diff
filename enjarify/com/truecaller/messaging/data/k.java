package com.truecaller.messaging.data;

import dagger.a.d;
import javax.inject.Provider;

public final class k
  implements d
{
  private final e a;
  private final Provider b;
  
  private k(e parame, Provider paramProvider)
  {
    a = parame;
    b = paramProvider;
  }
  
  public static k a(e parame, Provider paramProvider)
  {
    k localk = new com/truecaller/messaging/data/k;
    localk.<init>(parame, paramProvider);
    return localk;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */