package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.messaging.data.types.Draft;

final class u$ae
  extends u
{
  private final Draft b;
  
  private u$ae(e parame, Draft paramDraft)
  {
    super(parame);
    b = paramDraft;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".saveDraft(");
    String str = a(b, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u.ae
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */