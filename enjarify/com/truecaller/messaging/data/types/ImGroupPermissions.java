package com.truecaller.messaging.data.types;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class ImGroupPermissions
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final int a;
  public final int b;
  public final int c;
  public final int d;
  
  static
  {
    ImGroupPermissions.a locala = new com/truecaller/messaging/data/types/ImGroupPermissions$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public ImGroupPermissions(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    a = paramInt1;
    b = paramInt2;
    c = paramInt3;
    d = paramInt4;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof ImGroupPermissions;
      if (bool2)
      {
        paramObject = (ImGroupPermissions)paramObject;
        int i = a;
        int j = a;
        if (i == j) {
          i = 1;
        } else {
          i = 0;
        }
        if (i != 0)
        {
          i = b;
          j = b;
          if (i == j) {
            i = 1;
          } else {
            i = 0;
          }
          if (i != 0)
          {
            i = c;
            j = c;
            if (i == j) {
              i = 1;
            } else {
              i = 0;
            }
            if (i != 0)
            {
              i = d;
              int k = d;
              if (i == k)
              {
                k = 1;
              }
              else
              {
                k = 0;
                paramObject = null;
              }
              if (k != 0) {
                return bool1;
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = a * 31;
    int j = b;
    i = (i + j) * 31;
    j = c;
    i = (i + j) * 31;
    j = d;
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ImGroupPermissions(actions=");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(", roleUpdateRestrictionMask=");
    i = b;
    localStringBuilder.append(i);
    localStringBuilder.append(", roleUpdateMask=");
    i = c;
    localStringBuilder.append(i);
    localStringBuilder.append(", selfRoleUpdateMask=");
    i = d;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    paramInt = a;
    paramParcel.writeInt(paramInt);
    paramInt = b;
    paramParcel.writeInt(paramInt);
    paramInt = c;
    paramParcel.writeInt(paramInt);
    paramInt = d;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.ImGroupPermissions
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */