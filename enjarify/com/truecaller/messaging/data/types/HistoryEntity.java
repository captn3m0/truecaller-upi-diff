package com.truecaller.messaging.data.types;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class HistoryEntity
  extends Entity
{
  public static final Parcelable.Creator CREATOR;
  
  static
  {
    HistoryEntity.a locala = new com/truecaller/messaging/data/types/HistoryEntity$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public HistoryEntity()
  {
    super("history");
  }
  
  public final void a(ContentValues paramContentValues)
  {
    k.b(paramContentValues, "contentValues");
    String str = j;
    paramContentValues.put("type", str);
    paramContentValues.put("content", "");
  }
  
  public final boolean a()
  {
    return false;
  }
  
  public final boolean b()
  {
    return false;
  }
  
  public final boolean c()
  {
    return false;
  }
  
  public final boolean d()
  {
    return false;
  }
  
  public final boolean e()
  {
    return false;
  }
  
  public final boolean f()
  {
    return true;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    paramParcel.writeInt(1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.HistoryEntity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */