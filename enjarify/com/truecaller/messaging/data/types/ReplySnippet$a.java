package com.truecaller.messaging.data.types;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;
import java.util.ArrayList;

public final class ReplySnippet$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    ReplySnippet localReplySnippet = new com/truecaller/messaging/data/types/ReplySnippet;
    long l = paramParcel.readLong();
    int i = paramParcel.readInt();
    int j = paramParcel.readInt();
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(j);
    while (j != 0)
    {
      localObject = ReplySnippet.class.getClassLoader();
      localObject = (Entity)paramParcel.readParcelable((ClassLoader)localObject);
      localArrayList.add(localObject);
      j += -1;
    }
    Object localObject = paramParcel.readString();
    String str = paramParcel.readString();
    localReplySnippet.<init>(l, i, localArrayList, (String)localObject, str);
    return localReplySnippet;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new ReplySnippet[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.ReplySnippet.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */