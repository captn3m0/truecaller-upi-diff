package com.truecaller.messaging.data.types;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public final class TextEntity
  extends Entity
{
  public static final Parcelable.Creator CREATOR;
  public final String a;
  
  static
  {
    TextEntity.1 local1 = new com/truecaller/messaging/data/types/TextEntity$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  TextEntity(long paramLong, String paramString1, int paramInt, String paramString2)
  {
    super(paramLong, paramString1, paramInt);
    a = paramString2;
  }
  
  private TextEntity(Parcel paramParcel)
  {
    super(paramParcel);
    paramParcel = paramParcel.readString();
    a = paramParcel;
  }
  
  public final void a(ContentValues paramContentValues)
  {
    Object localObject = j;
    paramContentValues.put("type", (String)localObject);
    localObject = Integer.valueOf(this.i);
    paramContentValues.put("status", (Integer)localObject);
    localObject = a;
    paramContentValues.put("content", (String)localObject);
    int i = -1;
    Integer localInteger = Integer.valueOf(i);
    paramContentValues.put("width", localInteger);
    localInteger = Integer.valueOf(i);
    paramContentValues.put("height", localInteger);
    localObject = Integer.valueOf(i);
    paramContentValues.put("size", (Integer)localObject);
  }
  
  public final boolean a()
  {
    return true;
  }
  
  public final boolean b()
  {
    return false;
  }
  
  public final boolean c()
  {
    return false;
  }
  
  public final boolean d()
  {
    return false;
  }
  
  public final boolean e()
  {
    return false;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    String str = null;
    if (paramObject != null)
    {
      Class localClass1 = getClass();
      Class localClass2 = paramObject.getClass();
      if (localClass1 == localClass2)
      {
        boolean bool = super.equals(paramObject);
        if (!bool) {
          return false;
        }
        paramObject = (TextEntity)paramObject;
        str = a;
        paramObject = a;
        return str.equals(paramObject);
      }
    }
    return false;
  }
  
  public final boolean f()
  {
    return false;
  }
  
  public final int hashCode()
  {
    int i = super.hashCode() * 31;
    int j = a.hashCode();
    return i + j;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    String str = a;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.TextEntity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */