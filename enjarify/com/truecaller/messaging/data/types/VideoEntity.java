package com.truecaller.messaging.data.types;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class VideoEntity
  extends BinaryEntity
{
  public static final Parcelable.Creator CREATOR;
  public static final VideoEntity.a n;
  public final int a;
  public final int k;
  public final int l;
  public final Uri m;
  
  static
  {
    Object localObject = new com/truecaller/messaging/data/types/VideoEntity$a;
    ((VideoEntity.a)localObject).<init>((byte)0);
    n = (VideoEntity.a)localObject;
    localObject = new com/truecaller/messaging/data/types/VideoEntity$b;
    ((VideoEntity.b)localObject).<init>();
    CREATOR = (Parcelable.Creator)localObject;
  }
  
  public VideoEntity(long paramLong1, String paramString, int paramInt1, Uri paramUri1, boolean paramBoolean, long paramLong2, int paramInt2, int paramInt3, int paramInt4, Uri paramUri2)
  {
    super(paramLong1, paramString, paramInt1, paramUri1, paramBoolean, paramLong2);
    a = paramInt2;
    k = paramInt3;
    l = paramInt4;
    m = paramUri2;
  }
  
  public VideoEntity(long paramLong1, String paramString1, int paramInt1, String paramString2, long paramLong2, int paramInt2, int paramInt3, int paramInt4, String paramString3)
  {
    super(paramLong1, paramString1, paramInt1, paramString2, paramLong2);
    a = paramInt2;
    k = paramInt3;
    l = paramInt4;
    Uri localUri = Uri.parse(paramString3);
    k.a(localUri, "Uri.parse(thumbnail)");
    m = localUri;
  }
  
  public VideoEntity(Parcel paramParcel)
  {
    super(paramParcel);
    int i = paramParcel.readInt();
    a = i;
    i = paramParcel.readInt();
    k = i;
    i = paramParcel.readInt();
    l = i;
    paramParcel = Uri.parse(paramParcel.readString());
    k.a(paramParcel, "Uri.parse(source.readString())");
    m = paramParcel;
  }
  
  public final void a(ContentValues paramContentValues)
  {
    k.b(paramContentValues, "contentValues");
    Object localObject = j;
    paramContentValues.put("type", (String)localObject);
    localObject = Integer.valueOf(i);
    paramContentValues.put("status", (Integer)localObject);
    localObject = b.toString();
    paramContentValues.put("content", (String)localObject);
    localObject = Integer.valueOf(a);
    paramContentValues.put("width", (Integer)localObject);
    localObject = Integer.valueOf(k);
    paramContentValues.put("height", (Integer)localObject);
    localObject = Long.valueOf(d);
    paramContentValues.put("size", (Long)localObject);
    localObject = Integer.valueOf(l);
    paramContentValues.put("duration", (Integer)localObject);
    localObject = m.toString();
    paramContentValues.put("thumbnail", (String)localObject);
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    VideoEntity localVideoEntity = this;
    localVideoEntity = (VideoEntity)this;
    boolean bool1 = true;
    if (localVideoEntity == paramObject) {
      return bool1;
    }
    localVideoEntity = null;
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      boolean bool2 = k.a(localObject1, localObject2) ^ bool1;
      if (!bool2)
      {
        bool2 = super.equals(paramObject);
        if (!bool2) {
          return false;
        }
        bool2 = paramObject instanceof VideoEntity;
        if (bool2)
        {
          paramObject = (VideoEntity)paramObject;
          int i = a;
          int j = a;
          if (i == j)
          {
            i = k;
            j = k;
            if (i == j)
            {
              i = l;
              j = l;
              if (i == j)
              {
                localObject1 = b;
                localObject2 = b;
                boolean bool3 = k.a(localObject1, localObject2);
                if (bool3)
                {
                  paramObject = m;
                  localObject1 = m;
                  boolean bool4 = k.a(paramObject, localObject1);
                  if (bool4) {
                    return bool1;
                  }
                }
              }
            }
          }
        }
        return false;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    int i = super.hashCode() * 31;
    int j = b.hashCode();
    i = (i + j) * 31;
    j = a;
    i = (i + j) * 31;
    j = k;
    i = (i + j) * 31;
    j = l;
    i = (i + j) * 31;
    j = m.hashCode();
    return i + j;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    super.writeToParcel(paramParcel, paramInt);
    paramInt = a;
    paramParcel.writeInt(paramInt);
    paramInt = k;
    paramParcel.writeInt(paramInt);
    paramInt = l;
    paramParcel.writeInt(paramInt);
    String str = m.toString();
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.VideoEntity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */