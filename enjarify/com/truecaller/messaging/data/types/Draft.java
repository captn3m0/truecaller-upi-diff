package com.truecaller.messaging.data.types;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.truecaller.messaging.transport.NullTransportInfo;
import com.truecaller.messaging.transport.NullTransportInfo.a;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.a.a.b;
import org.c.a.a.a.k;

public final class Draft
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private static final BinaryEntity[] j = new BinaryEntity[0];
  public final long a;
  public final Conversation b;
  public final String c;
  public final Participant[] d;
  public final BinaryEntity[] e;
  public final boolean f;
  public final String g;
  public final long h;
  public final ReplySnippet i;
  
  static
  {
    Draft.1 local1 = new com/truecaller/messaging/data/types/Draft$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  private Draft(Parcel paramParcel)
  {
    long l = paramParcel.readLong();
    a = l;
    Object localObject = Conversation.class.getClassLoader();
    localObject = (Conversation)paramParcel.readParcelable((ClassLoader)localObject);
    b = ((Conversation)localObject);
    localObject = paramParcel.readString();
    c = ((String)localObject);
    localObject = Participant.CREATOR;
    localObject = (Participant[])paramParcel.createTypedArray((Parcelable.Creator)localObject);
    d = ((Participant[])localObject);
    localObject = Entity.class.getClassLoader();
    localObject = paramParcel.readParcelableArray((ClassLoader)localObject);
    BinaryEntity[] arrayOfBinaryEntity1 = new BinaryEntity[localObject.length];
    e = arrayOfBinaryEntity1;
    boolean bool = false;
    arrayOfBinaryEntity1 = null;
    int k = 0;
    for (;;)
    {
      BinaryEntity[] arrayOfBinaryEntity2 = e;
      int m = arrayOfBinaryEntity2.length;
      if (k >= m) {
        break;
      }
      BinaryEntity localBinaryEntity = (BinaryEntity)localObject[k];
      arrayOfBinaryEntity2[k] = localBinaryEntity;
      k += 1;
    }
    int n = paramParcel.readInt();
    if (n != 0) {
      bool = true;
    }
    f = bool;
    localObject = paramParcel.readString();
    g = ((String)localObject);
    localObject = ReplySnippet.class.getClassLoader();
    localObject = (ReplySnippet)paramParcel.readParcelable((ClassLoader)localObject);
    i = ((ReplySnippet)localObject);
    l = paramParcel.readLong();
    h = l;
  }
  
  private Draft(Draft.a parama)
  {
    long l = a;
    a = l;
    Object localObject1 = b;
    b = ((Conversation)localObject1);
    localObject1 = k.n(d);
    c = ((String)localObject1);
    localObject1 = c;
    int k = c.size();
    Object localObject2 = new Participant[k];
    localObject1 = (Participant[])((Set)localObject1).toArray((Object[])localObject2);
    d = ((Participant[])localObject1);
    localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = j;
      e = ((BinaryEntity[])localObject1);
    }
    else
    {
      localObject1 = e;
      k = e.size();
      localObject2 = new BinaryEntity[k];
      localObject1 = (BinaryEntity[])((List)localObject1).toArray((Object[])localObject2);
      e = ((BinaryEntity[])localObject1);
    }
    boolean bool = f;
    f = bool;
    localObject1 = UUID.randomUUID().toString();
    g = ((String)localObject1);
    localObject1 = h;
    i = ((ReplySnippet)localObject1);
    l = g;
    h = l;
  }
  
  public final Message a(String paramString)
  {
    Message.a locala = new com/truecaller/messaging/data/types/Message$a;
    locala.<init>();
    long l1 = a;
    long l2 = -1;
    boolean bool1 = l1 < l2;
    if (bool1) {
      a = l1;
    }
    Conversation localConversation = b;
    if (localConversation != null)
    {
      l1 = a;
      b = l1;
    }
    boolean bool2 = true;
    g = bool2;
    h = bool2;
    bool2 = false;
    localConversation = null;
    i = false;
    Object localObject = b.ay_();
    e = ((b)localObject);
    localObject = b.ay_();
    d = ((b)localObject);
    localObject = d[0];
    c = ((Participant)localObject);
    paramString = locala.a(paramString);
    localObject = g;
    q = ((String)localObject);
    int m = 3;
    f = m;
    bool1 = f;
    o = bool1;
    String str = d[0].e;
    p = str;
    int k = 2;
    r = k;
    long l3 = h;
    w = l3;
    l3 = a;
    boolean bool3 = l3 < l2;
    if (bool3)
    {
      paramString = new com/truecaller/messaging/transport/NullTransportInfo$a;
      paramString.<init>();
      l3 = a;
      a = l3;
      paramString = paramString.a();
    }
    else
    {
      paramString = NullTransportInfo.b;
    }
    locala.a(m, paramString);
    paramString = e;
    m = paramString.length;
    k = 0;
    str = null;
    while (k < m)
    {
      Entity localEntity = paramString[k];
      locala.a(localEntity);
      k += 1;
    }
    paramString = c;
    bool3 = TextUtils.isEmpty(paramString);
    if (!bool3)
    {
      localObject = c;
      paramString = Entity.a("text/plain", 0, (String)localObject, l2);
      locala.a(paramString);
    }
    return locala.b();
  }
  
  public final boolean a()
  {
    Object localObject = c;
    boolean bool = k.b((CharSequence)localObject);
    if (bool)
    {
      localObject = e;
      int k = localObject.length;
      if (k == 0) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean b()
  {
    long l1 = h;
    long l2 = -1;
    boolean bool = l1 < l2;
    return bool;
  }
  
  public final Draft.a c()
  {
    Draft.a locala = new com/truecaller/messaging/data/types/Draft$a;
    locala.<init>(this, (byte)0);
    return locala;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Draft{messageId=");
    long l = a;
    localStringBuilder.append(l);
    localStringBuilder.append(", conversation=");
    Object localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", participants=");
    localObject = Arrays.toString(d);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", hiddenNumber=");
    boolean bool = f;
    localStringBuilder.append(bool);
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    long l = a;
    paramParcel.writeLong(l);
    Object localObject = b;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    localObject = c;
    paramParcel.writeString((String)localObject);
    localObject = d;
    paramParcel.writeTypedArray((Parcelable[])localObject, paramInt);
    localObject = e;
    paramParcel.writeParcelableArray((Parcelable[])localObject, paramInt);
    int k = f;
    paramParcel.writeInt(k);
    localObject = g;
    paramParcel.writeString((String)localObject);
    localObject = i;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    l = h;
    paramParcel.writeLong(l);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.Draft
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */