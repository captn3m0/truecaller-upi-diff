package com.truecaller.messaging.data.types;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public class ImageEntity
  extends BinaryEntity
{
  public static final Parcelable.Creator CREATOR;
  public final int a;
  public final int k;
  
  static
  {
    ImageEntity.1 local1 = new com/truecaller/messaging/data/types/ImageEntity$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  ImageEntity(long paramLong1, String paramString, int paramInt1, Uri paramUri, int paramInt2, int paramInt3, boolean paramBoolean, long paramLong2)
  {
    super(paramLong1, paramString, paramInt1, paramUri, paramBoolean, paramLong2);
    a = paramInt2;
    k = paramInt3;
  }
  
  ImageEntity(long paramLong1, String paramString1, int paramInt1, String paramString2, int paramInt2, int paramInt3, long paramLong2)
  {
    super(paramLong1, paramString1, paramInt1, paramString2, paramLong2);
    a = paramInt2;
    k = paramInt3;
  }
  
  protected ImageEntity(Parcel paramParcel)
  {
    super(paramParcel);
    int i = paramParcel.readInt();
    a = i;
    int j = paramParcel.readInt();
    k = j;
  }
  
  public final void a(ContentValues paramContentValues)
  {
    Object localObject = j;
    paramContentValues.put("type", (String)localObject);
    localObject = Integer.valueOf(i);
    paramContentValues.put("status", (Integer)localObject);
    localObject = b.toString();
    paramContentValues.put("content", (String)localObject);
    localObject = Integer.valueOf(a);
    paramContentValues.put("width", (Integer)localObject);
    localObject = Integer.valueOf(k);
    paramContentValues.put("height", (Integer)localObject);
    localObject = Long.valueOf(d);
    paramContentValues.put("size", (Long)localObject);
  }
  
  public final boolean a()
  {
    return false;
  }
  
  public final boolean b()
  {
    return true;
  }
  
  public final boolean c()
  {
    return false;
  }
  
  public final boolean d()
  {
    return false;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public final boolean e()
  {
    return false;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    Uri localUri = null;
    if (paramObject != null)
    {
      Class localClass1 = getClass();
      Class localClass2 = paramObject.getClass();
      if (localClass1 == localClass2)
      {
        boolean bool = super.equals(paramObject);
        if (!bool) {
          return false;
        }
        paramObject = (ImageEntity)paramObject;
        int i = a;
        int j = a;
        if (i != j) {
          return false;
        }
        i = k;
        j = k;
        if (i != j) {
          return false;
        }
        localUri = b;
        paramObject = b;
        return localUri.equals(paramObject);
      }
    }
    return false;
  }
  
  public int hashCode()
  {
    int i = super.hashCode() * 31;
    int j = b.hashCode();
    i = (i + j) * 31;
    j = a;
    i = (i + j) * 31;
    j = k;
    return i + j;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    paramInt = a;
    paramParcel.writeInt(paramInt);
    paramInt = k;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.ImageEntity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */