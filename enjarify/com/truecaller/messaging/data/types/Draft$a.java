package com.truecaller.messaging.data.types;

import com.truecaller.log.AssertionUtil.AlwaysFatal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class Draft$a
{
  public long a;
  public Conversation b;
  Set c;
  public String d;
  List e;
  boolean f;
  public long g;
  public ReplySnippet h;
  
  public Draft$a()
  {
    long l = -1;
    a = l;
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    c = localHashSet;
    f = false;
    g = l;
  }
  
  private Draft$a(Draft paramDraft)
  {
    long l = -1;
    a = l;
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    c = localHashSet;
    localHashSet = null;
    f = false;
    g = l;
    l = a;
    a = l;
    Object localObject1 = b;
    b = ((Conversation)localObject1);
    localObject1 = c;
    d = ((String)localObject1);
    localObject1 = c;
    Object localObject2 = d;
    Collections.addAll((Collection)localObject1, (Object[])localObject2);
    localObject1 = e;
    int i = localObject1.length;
    if (i > 0)
    {
      localObject1 = new java/util/ArrayList;
      int j = e.length;
      ((ArrayList)localObject1).<init>(j);
      e = ((List)localObject1);
      localObject1 = e;
      localObject2 = e;
      Collections.addAll((Collection)localObject1, (Object[])localObject2);
    }
    localObject1 = i;
    h = ((ReplySnippet)localObject1);
    l = h;
    g = l;
  }
  
  public final a a()
  {
    String str = d;
    if (str != null)
    {
      str = null;
      d = null;
    }
    return this;
  }
  
  public final a a(long paramLong)
  {
    a = paramLong;
    return this;
  }
  
  public final a a(BinaryEntity paramBinaryEntity)
  {
    boolean bool = paramBinaryEntity.a();
    String[] arrayOfString = new String[0];
    AssertionUtil.AlwaysFatal.isFalse(bool, arrayOfString);
    Object localObject = e;
    if (localObject == null)
    {
      localObject = new java/util/ArrayList;
      ((ArrayList)localObject).<init>();
      e = ((List)localObject);
    }
    e.add(paramBinaryEntity);
    return this;
  }
  
  public final a a(Conversation paramConversation)
  {
    b = paramConversation;
    return this;
  }
  
  public final a a(Participant paramParticipant)
  {
    c.add(paramParticipant);
    return this;
  }
  
  public final a a(ReplySnippet paramReplySnippet)
  {
    h = paramReplySnippet;
    return this;
  }
  
  public final a a(String paramString)
  {
    d = paramString;
    return this;
  }
  
  public final a a(Collection paramCollection)
  {
    boolean bool = paramCollection.isEmpty();
    if (!bool)
    {
      Object localObject = e;
      if (localObject == null)
      {
        localObject = new java/util/ArrayList;
        int i = paramCollection.size();
        ((ArrayList)localObject).<init>(i);
        e = ((List)localObject);
      }
      localObject = e;
      ((List)localObject).addAll(paramCollection);
    }
    return this;
  }
  
  public final a a(Participant[] paramArrayOfParticipant)
  {
    Collections.addAll(c, paramArrayOfParticipant);
    return this;
  }
  
  public final a b()
  {
    List localList = e;
    if (localList != null) {
      localList.clear();
    }
    return this;
  }
  
  public final a b(long paramLong)
  {
    g = paramLong;
    return this;
  }
  
  public final a c()
  {
    h = null;
    g = -1;
    return this;
  }
  
  public final Draft d()
  {
    Draft localDraft = new com/truecaller/messaging/data/types/Draft;
    localDraft.<init>(this, (byte)0);
    return localDraft;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.Draft.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */