package com.truecaller.messaging.data.types;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Patterns;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.h.u;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.messaging.transport.im.a.i;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.c.a.a.a.k;

public class Participant
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public static final Participant a;
  public final long b;
  public final int c;
  public final String d;
  public final String e;
  public final String f;
  public final String g;
  public final String h;
  public final long i;
  public final int j;
  public final boolean k;
  public final int l;
  public final String m;
  public final String n;
  public final int o;
  public final long p;
  public final int q;
  public final int r;
  public final String s;
  public final long t;
  private final int u;
  
  static
  {
    Object localObject = new com/truecaller/messaging/data/types/Participant$a;
    ((Participant.a)localObject).<init>(3);
    e = "";
    a = ((Participant.a)localObject).a();
    localObject = new com/truecaller/messaging/data/types/Participant$1;
    ((Participant.1)localObject).<init>();
    CREATOR = (Parcelable.Creator)localObject;
  }
  
  private Participant(Parcel paramParcel)
  {
    long l1 = paramParcel.readLong();
    b = l1;
    int i1 = paramParcel.readInt();
    c = i1;
    String str = paramParcel.readString();
    d = str;
    str = paramParcel.readString();
    e = str;
    str = paramParcel.readString();
    f = str;
    str = paramParcel.readString();
    g = str;
    l1 = paramParcel.readLong();
    i = l1;
    str = paramParcel.readString();
    h = str;
    i1 = paramParcel.readInt();
    j = i1;
    i1 = paramParcel.readInt();
    int i2 = 1;
    if (i1 != i2) {
      i2 = 0;
    }
    k = i2;
    i1 = paramParcel.readInt();
    l = i1;
    str = paramParcel.readString();
    m = str;
    str = paramParcel.readString();
    n = str;
    i1 = paramParcel.readInt();
    o = i1;
    l1 = paramParcel.readLong();
    p = l1;
    i1 = paramParcel.readInt();
    q = i1;
    i1 = paramParcel.readInt();
    r = i1;
    str = paramParcel.readString();
    s = str;
    l1 = paramParcel.readLong();
    t = l1;
    paramParcel = new org/c/a/a/a/a/a;
    paramParcel.<init>();
    str = f;
    paramParcel = paramParcel.a(str);
    i1 = c;
    int i3 = aa;
    u = i3;
  }
  
  private Participant(Participant.a parama)
  {
    long l1 = b;
    b = l1;
    int i1 = a;
    c = i1;
    String str = c;
    d = str;
    str = k.n(d);
    e = str;
    str = k.n(e);
    f = str;
    str = k.n(f);
    g = str;
    l1 = h;
    i = l1;
    str = g;
    h = str;
    i1 = i;
    j = i1;
    boolean bool = j;
    k = bool;
    int i2 = k;
    l = i2;
    str = l;
    m = str;
    str = m;
    n = str;
    i2 = n;
    o = i2;
    l1 = o;
    p = l1;
    i2 = p;
    q = i2;
    i2 = s;
    r = i2;
    str = q;
    s = str;
    l1 = r;
    t = l1;
    parama = new org/c/a/a/a/a/a;
    parama.<init>();
    str = f;
    parama = parama.a(str);
    i2 = c;
    int i3 = aa;
    u = i3;
  }
  
  public static Participant a(Contact paramContact, String paramString, u paramu)
  {
    Participant.a locala = new com/truecaller/messaging/data/types/Participant$a;
    String str = null;
    locala.<init>(0);
    if (paramString != null)
    {
      e = paramString;
    }
    else
    {
      paramString = paramContact.r();
      if (paramString != null)
      {
        str = paramString.a();
        e = str;
        paramString = paramString.l();
        f = paramString;
      }
      else
      {
        paramString = new java/lang/IllegalArgumentException;
        str = "Normalized number cannot be null";
        paramString.<init>(str);
        AssertionUtil.reportThrowableButNeverCrash(paramString);
      }
    }
    if (paramu != null)
    {
      paramString = f;
      bool1 = k.b(paramString);
      if (bool1)
      {
        paramString = e;
        bool1 = k.d(paramString);
        if (!bool1)
        {
          paramString = e;
          paramString = paramu.e(paramString);
          boolean bool2 = k.d(paramString);
          if (!bool2) {
            f = paramString;
          }
        }
      }
    }
    paramString = paramContact.k();
    if (paramString != null)
    {
      paramString = paramContact.k();
      long l1 = paramString.longValue();
      h = l1;
    }
    paramString = paramContact.s();
    boolean bool1 = k.b(paramString);
    if (!bool1)
    {
      paramString = paramContact.s();
      l = paramString;
    }
    bool1 = true;
    paramContact = paramContact.a(bool1);
    if (paramContact != null)
    {
      paramContact = paramContact.toString();
      m = paramContact;
    }
    return locala.a();
  }
  
  public static Participant a(String paramString1, u paramu, String paramString2)
  {
    int i1 = paramString1.indexOf('@');
    if (i1 >= 0)
    {
      paramu = Patterns.EMAIL_ADDRESS.matcher(paramString1);
      boolean bool = paramu.matches();
      if (bool)
      {
        paramu = new com/truecaller/messaging/data/types/Participant$a;
        paramu.<init>(2);
        d = paramString1;
        e = paramString1;
        return paramu.a();
      }
      paramu = new com/truecaller/messaging/data/types/Participant$a;
      paramu.<init>(1);
      d = paramString1;
      e = paramString1;
      return paramu.a();
    }
    return b(paramString1, paramu, paramString2);
  }
  
  private boolean a(int paramInt)
  {
    int i1 = r;
    paramInt &= i1;
    return paramInt != 0;
  }
  
  public static Participant[] a(Uri paramUri, u paramu, String paramString)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    String str = paramUri.getScheme();
    Object localObject = "sms";
    boolean bool1 = ((String)localObject).equals(str);
    if (!bool1)
    {
      localObject = "smsto";
      boolean bool2 = ((String)localObject).equals(str);
      if (!bool2) {}
    }
    else
    {
      str = paramUri.getSchemeSpecificPart();
      paramUri = paramUri.getQuery();
      bool1 = TextUtils.isEmpty(paramUri);
      int i3 = 0;
      int i4 = 1;
      if (!bool1)
      {
        int i1 = str.length();
        int i5 = paramUri.length();
        i1 = i1 - i5 - i4;
        str = str.substring(0, i1);
      }
      paramUri = k.a(str, ",;");
      int i2 = paramUri.length;
      while (i3 < i2)
      {
        localObject = a(paramUri[i3], paramu, paramString);
        int i6 = c;
        if ((i6 == 0) || (i6 == i4)) {
          localArrayList.add(localObject);
        }
        i3 += 1;
      }
    }
    paramUri = new Participant[localArrayList.size()];
    return (Participant[])localArrayList.toArray(paramUri);
  }
  
  public static Participant b(String paramString1, u paramu, String paramString2)
  {
    paramString2 = paramu.c(paramString1, paramString2);
    if (paramString2 == null)
    {
      paramu = new com/truecaller/messaging/data/types/Participant$a;
      int i1 = 1;
      paramu.<init>(i1);
      e = paramString1;
    }
    else
    {
      Participant.a locala = new com/truecaller/messaging/data/types/Participant$a;
      locala.<init>(0);
      e = paramString2;
      paramu = paramu.e(paramString2);
      boolean bool = k.d(paramu);
      if (!bool) {
        f = paramu;
      }
      paramu = locala;
    }
    d = paramString1;
    return paramu.a();
  }
  
  private boolean m()
  {
    int i1 = o;
    int i2 = 32;
    i1 &= i2;
    return i1 == i2;
  }
  
  public final String a()
  {
    int i1 = r;
    int i2 = 64;
    boolean bool1 = com.truecaller.content.a.a.a(i1, i2);
    if (bool1)
    {
      localObject1 = s;
      bool1 = k.e((CharSequence)localObject1);
      if (bool1)
      {
        localObject1 = TrueApp.y().a().aF().y();
        bool1 = ((b)localObject1).a();
        if (bool1)
        {
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>();
          Object localObject2 = s;
          ((StringBuilder)localObject1).append((String)localObject2);
          localObject2 = m;
          boolean bool2 = k.e((CharSequence)localObject2);
          if (bool2)
          {
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>(" (");
            String str = m;
            ((StringBuilder)localObject2).append(str);
            str = ")";
            ((StringBuilder)localObject2).append(str);
            localObject2 = ((StringBuilder)localObject2).toString();
          }
          else
          {
            localObject2 = "";
          }
          ((StringBuilder)localObject1).append((String)localObject2);
          return ((StringBuilder)localObject1).toString();
        }
      }
    }
    Object localObject1 = m;
    bool1 = k.e((CharSequence)localObject1);
    if (bool1) {
      return m;
    }
    return b();
  }
  
  public final boolean a(boolean paramBoolean)
  {
    int i1 = j;
    boolean bool2 = true;
    if (i1 != bool2)
    {
      boolean bool1 = k;
      bool2 = true;
      if ((!bool1) || (!paramBoolean))
      {
        paramBoolean = j;
        if (paramBoolean != bool2) {}
      }
      else
      {
        return bool2;
      }
    }
    return false;
  }
  
  public final String b()
  {
    int i1 = c;
    if (i1 != 0) {
      switch (i1)
      {
      default: 
        return f;
      case 3: 
        localObject = TrueApp.y().a().cp();
        str = f;
        return ((i)localObject).a(str);
      }
    }
    Object localObject = android.support.v4.e.a.a();
    String str = f;
    return ((android.support.v4.e.a)localObject).a(str);
  }
  
  public final boolean c()
  {
    String str1 = f;
    boolean bool = k.d(str1);
    if (!bool)
    {
      str1 = "insert-address-token";
      String str2 = f;
      bool = k.a(str1, str2);
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  public final boolean d()
  {
    int i1 = o;
    int i2 = 2;
    i1 &= i2;
    return i1 == i2;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public final boolean e()
  {
    int i1 = c;
    if (i1 == 0)
    {
      i1 = o & 0x2;
      if (i1 == 0)
      {
        String str = m;
        int i2 = TextUtils.isEmpty(str);
        if (i2 == 0)
        {
          i2 = 1;
          boolean bool = a(i2);
          if (!bool) {
            return i2;
          }
        }
      }
    }
    return false;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof Participant;
    if (!bool1) {
      return false;
    }
    paramObject = (Participant)paramObject;
    int i1 = c;
    int i2 = c;
    if (i1 == i2)
    {
      String str = f;
      paramObject = f;
      boolean bool2 = str.equals(paramObject);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean f()
  {
    return k.c(d);
  }
  
  public final boolean g()
  {
    int i1 = j;
    int i2 = 2;
    if (i1 != i2)
    {
      boolean bool = k;
      int i4 = 1;
      if (!bool)
      {
        int i3 = q;
        int i5 = 10;
        if ((i3 < i5) && (i1 != i4)) {}
      }
      else
      {
        return i4;
      }
    }
    return false;
  }
  
  public final boolean h()
  {
    int i1 = c;
    int i2 = 1;
    if ((i1 != 0) && (i1 != i2)) {
      return false;
    }
    return i2;
  }
  
  public int hashCode()
  {
    return u;
  }
  
  public final boolean i()
  {
    boolean bool = d();
    if (!bool)
    {
      bool = a(2);
      if (!bool)
      {
        bool = m();
        if (!bool) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final String j()
  {
    int i1 = c;
    switch (i1)
    {
    default: 
      AssertionUtil.OnlyInDebug.fail(new String[] { "Should never happen" });
      return "unknwon";
    case 4: 
      return "im_group";
    case 3: 
      return "tc";
    case 2: 
      return "email";
    case 1: 
      return "alphanum";
    }
    return "phone_number";
  }
  
  public final String k()
  {
    int i1 = c;
    if (i1 == 0)
    {
      String str1 = f;
      String str2 = "+";
      boolean bool = str1.startsWith(str2);
      if (bool) {
        return f.substring(1);
      }
    }
    return f;
  }
  
  public final Participant.a l()
  {
    Participant.a locala = new com/truecaller/messaging/data/types/Participant$a;
    locala.<init>(this, (byte)0);
    return locala;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("{id : ");
    long l1 = b;
    localStringBuilder.append(l1);
    localStringBuilder.append(", type: ");
    String str = j();
    localStringBuilder.append(str);
    localStringBuilder.append(", source : \"");
    int i1 = o;
    localStringBuilder.append(i1);
    localStringBuilder.append("\"}");
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    long l1 = b;
    paramParcel.writeLong(l1);
    paramInt = c;
    paramParcel.writeInt(paramInt);
    String str = d;
    paramParcel.writeString(str);
    str = e;
    paramParcel.writeString(str);
    str = f;
    paramParcel.writeString(str);
    str = g;
    paramParcel.writeString(str);
    l1 = i;
    paramParcel.writeLong(l1);
    str = h;
    paramParcel.writeString(str);
    paramInt = j;
    paramParcel.writeInt(paramInt);
    paramInt = k;
    paramParcel.writeInt(paramInt);
    paramInt = l;
    paramParcel.writeInt(paramInt);
    str = m;
    paramParcel.writeString(str);
    str = n;
    paramParcel.writeString(str);
    paramInt = o;
    paramParcel.writeInt(paramInt);
    l1 = p;
    paramParcel.writeLong(l1);
    paramInt = q;
    paramParcel.writeInt(paramInt);
    paramInt = r;
    paramParcel.writeInt(paramInt);
    str = s;
    paramParcel.writeString(str);
    l1 = t;
    paramParcel.writeLong(l1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.Participant
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */