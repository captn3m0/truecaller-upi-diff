package com.truecaller.messaging.data.types;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.avito.konveyor.b.a;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.transport.im.ImTransportInfo;
import java.util.Arrays;
import java.util.List;
import org.a.a.a.g;
import org.c.a.a.a.k;

public final class Message
  implements Parcelable, a
{
  public static final Parcelable.Creator CREATOR;
  public final long a;
  public final long b;
  public final Participant c;
  public final org.a.a.b d;
  public final org.a.a.b e;
  public final int f;
  public final boolean g;
  public final boolean h;
  public final boolean i;
  public final int j;
  public final int k;
  public final String l;
  public final TransportInfo m;
  public final Entity[] n;
  public final String o;
  public final String p;
  public final int q;
  public final int r;
  public final int s;
  public final int t;
  public final org.a.a.b u;
  public final ReplySnippet v;
  public final boolean w;
  public final long x;
  
  static
  {
    Message.1 local1 = new com/truecaller/messaging/data/types/Message$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  private Message(Parcel paramParcel)
  {
    long l1 = paramParcel.readLong();
    a = l1;
    l1 = paramParcel.readLong();
    b = l1;
    Object localObject = Participant.class.getClassLoader();
    localObject = (Participant)paramParcel.readParcelable((ClassLoader)localObject);
    c = ((Participant)localObject);
    localObject = new org/a/a/b;
    long l2 = paramParcel.readLong();
    ((org.a.a.b)localObject).<init>(l2);
    e = ((org.a.a.b)localObject);
    localObject = new org/a/a/b;
    l2 = paramParcel.readLong();
    ((org.a.a.b)localObject).<init>(l2);
    d = ((org.a.a.b)localObject);
    int i1 = paramParcel.readInt();
    f = i1;
    i1 = paramParcel.readInt();
    boolean bool = true;
    if (i1 != 0)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      localObject = null;
    }
    g = i1;
    i1 = paramParcel.readInt();
    if (i1 != 0)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      localObject = null;
    }
    h = i1;
    i1 = paramParcel.readInt();
    if (i1 != 0)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      localObject = null;
    }
    i = i1;
    i1 = paramParcel.readInt();
    j = i1;
    i1 = paramParcel.readInt();
    k = i1;
    localObject = TransportInfo.class.getClassLoader();
    localObject = (TransportInfo)paramParcel.readParcelable((ClassLoader)localObject);
    m = ((TransportInfo)localObject);
    localObject = paramParcel.readString();
    l = ((String)localObject);
    localObject = Entity.class.getClassLoader();
    localObject = paramParcel.readParcelableArray((ClassLoader)localObject);
    if (localObject != null)
    {
      Entity[] arrayOfEntity1 = new Entity[localObject.length];
      n = arrayOfEntity1;
      int i2 = 0;
      arrayOfEntity1 = null;
      for (;;)
      {
        Entity[] arrayOfEntity2 = n;
        int i3 = arrayOfEntity2.length;
        if (i2 >= i3) {
          break;
        }
        Entity localEntity = (Entity)localObject[i2];
        arrayOfEntity2[i2] = localEntity;
        i2 += 1;
      }
    }
    localObject = new Entity[0];
    n = ((Entity[])localObject);
    localObject = paramParcel.readString();
    o = ((String)localObject);
    i1 = paramParcel.readInt();
    if (i1 == 0) {
      bool = false;
    }
    w = bool;
    localObject = paramParcel.readString();
    p = ((String)localObject);
    i1 = paramParcel.readInt();
    q = i1;
    i1 = paramParcel.readInt();
    r = i1;
    i1 = paramParcel.readInt();
    s = i1;
    i1 = paramParcel.readInt();
    t = i1;
    localObject = new org/a/a/b;
    l2 = paramParcel.readLong();
    ((org.a.a.b)localObject).<init>(l2);
    u = ((org.a.a.b)localObject);
    l1 = paramParcel.readLong();
    x = l1;
    localObject = ReplySnippet.class.getClassLoader();
    paramParcel = (ReplySnippet)paramParcel.readParcelable((ClassLoader)localObject);
    v = paramParcel;
  }
  
  private Message(Message.a parama)
  {
    long l1 = a;
    a = l1;
    l1 = b;
    b = l1;
    Object localObject = c;
    c = ((Participant)localObject);
    localObject = e;
    long l2 = 0L;
    if (localObject != null)
    {
      localObject = e;
    }
    else
    {
      localObject = new org/a/a/b;
      ((org.a.a.b)localObject).<init>(l2);
    }
    e = ((org.a.a.b)localObject);
    localObject = d;
    if (localObject != null)
    {
      localObject = d;
    }
    else
    {
      localObject = new org/a/a/b;
      ((org.a.a.b)localObject).<init>(l2);
    }
    d = ((org.a.a.b)localObject);
    int i1 = f;
    f = i1;
    boolean bool1 = g;
    g = bool1;
    bool1 = h;
    h = bool1;
    bool1 = i;
    i = bool1;
    int i2 = j;
    j = i2;
    localObject = m;
    m = ((TransportInfo)localObject);
    i2 = k;
    k = i2;
    localObject = l;
    l = ((String)localObject);
    localObject = q;
    o = ((String)localObject);
    boolean bool2 = o;
    w = bool2;
    localObject = p;
    p = ((String)localObject);
    int i3 = r;
    q = i3;
    i3 = s;
    r = i3;
    i3 = t;
    s = i3;
    i3 = u;
    t = i3;
    localObject = v;
    if (localObject != null)
    {
      localObject = v;
    }
    else
    {
      localObject = new org/a/a/b;
      ((org.a.a.b)localObject).<init>(l2);
    }
    u = ((org.a.a.b)localObject);
    l1 = w;
    x = l1;
    localObject = x;
    v = ((ReplySnippet)localObject);
    localObject = n;
    if (localObject == null)
    {
      parama = new Entity[0];
      n = parama;
      return;
    }
    localObject = n;
    parama = new Entity[n.size()];
    parama = (Entity[])((List)localObject).toArray(parama);
    n = parama;
  }
  
  public static String a(long paramLong, org.a.a.b paramb)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = Long.toHexString(paramLong);
    char c1 = '0';
    int i1 = 16;
    str = k.a(str, i1, c1);
    localStringBuilder.append(str);
    str = k.a(Long.toHexString(a), i1, c1);
    localStringBuilder.append(str);
    return localStringBuilder.toString();
  }
  
  public final long a()
  {
    return a;
  }
  
  public final boolean a(e parame)
  {
    int i1 = j;
    int i2 = 2;
    if (i1 == i2)
    {
      i1 = f;
      i2 = 1;
      if ((i1 == i2) || (i1 == 0))
      {
        boolean bool1 = d();
        if (bool1)
        {
          bool1 = h();
          if (!bool1) {}
        }
        else
        {
          com.truecaller.featuretoggles.b localb = parame.h();
          bool1 = localb.a();
          if (bool1)
          {
            parame = parame.b();
            boolean bool2 = parame.a();
            if (bool2) {
              return i2;
            }
          }
        }
      }
    }
    return false;
  }
  
  public final boolean b()
  {
    long l1 = a;
    long l2 = -1;
    boolean bool = l1 < l2;
    return bool;
  }
  
  public final boolean c()
  {
    Entity[] arrayOfEntity = n;
    int i1 = arrayOfEntity.length;
    return i1 != 0;
  }
  
  public final boolean d()
  {
    Entity[] arrayOfEntity = n;
    int i1 = arrayOfEntity.length;
    int i2 = 0;
    while (i2 < i1)
    {
      Entity localEntity = arrayOfEntity[i2];
      boolean bool1 = localEntity.a();
      if (!bool1)
      {
        boolean bool2 = localEntity.f();
        if (!bool2) {
          return true;
        }
      }
      i2 += 1;
    }
    return false;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean e()
  {
    int i1 = j;
    int i2 = 3;
    if (i1 == i2)
    {
      i1 = f;
      i2 = 17;
      i1 &= i2;
      if (i1 == i2) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    Entity[] arrayOfEntity = null;
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        paramObject = (Message)paramObject;
        long l1 = a;
        long l2 = a;
        boolean bool1 = l1 < l2;
        if (bool1) {
          return false;
        }
        l1 = b;
        l2 = b;
        bool1 = l1 < l2;
        if (bool1) {
          return false;
        }
        int i1 = f;
        int i4 = f;
        if (i1 != i4) {
          return false;
        }
        boolean bool2 = g;
        boolean bool5 = g;
        if (bool2 != bool5) {
          return false;
        }
        bool2 = h;
        bool5 = h;
        if (bool2 != bool5) {
          return false;
        }
        bool2 = i;
        bool5 = i;
        if (bool2 != bool5) {
          return false;
        }
        int i2 = j;
        int i5 = j;
        if (i2 != i5) {
          return false;
        }
        i2 = k;
        i5 = k;
        if (i2 != i5) {
          return false;
        }
        localObject1 = c;
        localObject2 = c;
        boolean bool3 = ((Participant)localObject1).equals(localObject2);
        if (!bool3) {
          return false;
        }
        localObject1 = d;
        localObject2 = d;
        bool3 = ((org.a.a.b)localObject1).equals(localObject2);
        if (!bool3) {
          return false;
        }
        localObject1 = e;
        localObject2 = e;
        bool3 = ((org.a.a.b)localObject1).equals(localObject2);
        if (!bool3) {
          return false;
        }
        localObject1 = m;
        localObject2 = m;
        bool3 = localObject1.equals(localObject2);
        if (!bool3) {
          return false;
        }
        localObject1 = l;
        localObject2 = l;
        bool3 = ((String)localObject1).equals(localObject2);
        if (!bool3) {
          return false;
        }
        int i3 = t;
        i5 = t;
        if (i3 != i5) {
          return false;
        }
        localObject1 = u;
        localObject2 = u;
        boolean bool4 = ((org.a.a.b)localObject1).equals(localObject2);
        if (!bool4) {
          return false;
        }
        l1 = x;
        l2 = x;
        bool1 = l1 < l2;
        if (bool1) {
          return false;
        }
        arrayOfEntity = n;
        paramObject = n;
        return Arrays.equals(arrayOfEntity, (Object[])paramObject);
      }
    }
    return false;
  }
  
  public final boolean f()
  {
    Object localObject = m;
    boolean bool = localObject instanceof ImTransportInfo;
    if (bool)
    {
      localObject = (ImTransportInfo)localObject;
      int i1 = i;
      if (i1 > 0) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean g()
  {
    long l1 = x;
    long l2 = -1;
    boolean bool = l1 < l2;
    return bool;
  }
  
  public final boolean h()
  {
    Entity[] arrayOfEntity = n;
    int i1 = arrayOfEntity.length;
    int i2 = 0;
    while (i2 < i1)
    {
      Entity localEntity = arrayOfEntity[i2];
      boolean bool = localEntity.a();
      if (!bool)
      {
        int i3 = i;
        if (i3 == 0) {
          return true;
        }
      }
      i2 += 1;
    }
    return false;
  }
  
  public final int hashCode()
  {
    long l1 = a;
    int i1 = 32;
    long l2 = l1 >>> i1;
    int i2 = (int)(l1 ^ l2) * 31;
    l2 = b;
    long l3 = l2 >>> i1;
    int i3 = (int)(l2 ^ l3);
    i2 = (i2 + i3) * 31;
    i3 = c.hashCode();
    i2 = (i2 + i3) * 31;
    i3 = d.hashCode();
    i2 = (i2 + i3) * 31;
    i3 = e.hashCode();
    i2 = (i2 + i3) * 31;
    i3 = f;
    i2 = (i2 + i3) * 31;
    int i4 = g;
    i2 = (i2 + i4) * 31;
    int i5 = h;
    i2 = (i2 + i5) * 31;
    int i6 = i;
    i2 = (i2 + i6) * 31;
    int i7 = j;
    i2 = (i2 + i7) * 31;
    int i8 = k;
    i2 = (i2 + i8) * 31;
    int i9 = m.hashCode();
    i2 = (i2 + i9) * 31;
    int i10 = l.hashCode();
    i2 = (i2 + i10) * 31;
    int i11 = t;
    i2 = (i2 + i11) * 31;
    int i12 = u.hashCode();
    i2 = (i2 + i12) * 31;
    l2 = x;
    l3 = l2 >>> i1;
    int i13 = (int)(l2 ^ l3);
    i2 = (i2 + i13) * 31;
    int i14 = Arrays.hashCode(n);
    return i2 + i14;
  }
  
  public final boolean i()
  {
    int i1 = f;
    int i2 = i1 & 0x1;
    if (i2 == 0)
    {
      i1 &= 0x4;
      if (i1 != 0)
      {
        i1 = j;
        i2 = 1;
        if (i1 == i2) {
          return i2;
        }
      }
    }
    return false;
  }
  
  public final String j()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Entity[] arrayOfEntity = n;
    int i1 = arrayOfEntity.length;
    int i2 = 0;
    while (i2 < i1)
    {
      Object localObject = arrayOfEntity[i2];
      boolean bool = ((Entity)localObject).a();
      if (bool)
      {
        localObject = (TextEntity)localObject;
        int i3 = localStringBuilder.length();
        if (i3 > 0)
        {
          i3 = 10;
          localStringBuilder.append(i3);
        }
        localObject = a;
        localStringBuilder.append((String)localObject);
      }
      i2 += 1;
    }
    return localStringBuilder.toString();
  }
  
  public final TransportInfo k()
  {
    return m;
  }
  
  public final String l()
  {
    long l1 = m.d();
    org.a.a.b localb = e;
    return a(l1, localb);
  }
  
  public final Message.a m()
  {
    Message.a locala = new com/truecaller/messaging/data/types/Message$a;
    locala.<init>(this, (byte)0);
    return locala;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("{");
    localStringBuilder.append("id : ");
    long l1 = a;
    localStringBuilder.append(l1);
    localStringBuilder.append(", conversation : ");
    l1 = b;
    localStringBuilder.append(l1);
    localStringBuilder.append(", status : ");
    int i1 = f;
    localStringBuilder.append(i1);
    localStringBuilder.append(", participant: ");
    Object localObject1 = c;
    localStringBuilder.append(localObject1);
    localStringBuilder.append(", date : ");
    localObject1 = e;
    localStringBuilder.append(localObject1);
    localStringBuilder.append(", dateSent : ");
    localObject1 = d;
    localStringBuilder.append(localObject1);
    localStringBuilder.append(", seen : ");
    boolean bool = g;
    localStringBuilder.append(bool);
    localStringBuilder.append(", read : ");
    bool = h;
    localStringBuilder.append(bool);
    localStringBuilder.append(", locked : ");
    bool = i;
    localStringBuilder.append(bool);
    localStringBuilder.append(", transport : ");
    int i2 = j;
    localStringBuilder.append(i2);
    localStringBuilder.append(", sim : ");
    localObject1 = l;
    localStringBuilder.append((String)localObject1);
    localStringBuilder.append(", scheduledTransport : ");
    i2 = k;
    localStringBuilder.append(i2);
    localStringBuilder.append(", transportInfo : ");
    localObject1 = m;
    localStringBuilder.append(localObject1);
    localStringBuilder.append(", rawAddress : ");
    localObject1 = p;
    localStringBuilder.append((String)localObject1);
    localObject1 = n;
    i2 = localObject1.length;
    if (i2 > 0)
    {
      localStringBuilder.append(", entities : [");
      localObject1 = n;
      int i3 = 0;
      Object localObject2 = null;
      localObject1 = localObject1[0];
      localStringBuilder.append(localObject1);
      i2 = 1;
      for (;;)
      {
        localObject2 = n;
        i3 = localObject2.length;
        if (i2 >= i3) {
          break;
        }
        localStringBuilder.append(", ");
        localObject2 = n[i2];
        localStringBuilder.append(localObject2);
        i2 += 1;
      }
      localObject1 = "]";
      localStringBuilder.append((String)localObject1);
    }
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    long l1 = a;
    paramParcel.writeLong(l1);
    l1 = b;
    paramParcel.writeLong(l1);
    Object localObject = c;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    l1 = e.a;
    paramParcel.writeLong(l1);
    l1 = d.a;
    paramParcel.writeLong(l1);
    int i1 = f;
    paramParcel.writeInt(i1);
    int i2 = g;
    paramParcel.writeInt(i2);
    int i3 = h;
    paramParcel.writeInt(i3);
    int i4 = i;
    paramParcel.writeInt(i4);
    int i5 = j;
    paramParcel.writeInt(i5);
    i5 = k;
    paramParcel.writeInt(i5);
    localObject = m;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    localObject = l;
    paramParcel.writeString((String)localObject);
    localObject = n;
    paramParcel.writeParcelableArray((Parcelable[])localObject, paramInt);
    localObject = o;
    paramParcel.writeString((String)localObject);
    int i6 = w;
    paramParcel.writeInt(i6);
    localObject = p;
    paramParcel.writeString((String)localObject);
    int i7 = q;
    paramParcel.writeInt(i7);
    i7 = r;
    paramParcel.writeInt(i7);
    i7 = s;
    paramParcel.writeInt(i7);
    i7 = t;
    paramParcel.writeInt(i7);
    l1 = u.a;
    paramParcel.writeLong(l1);
    l1 = x;
    paramParcel.writeLong(l1);
    localObject = v;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.Message
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */