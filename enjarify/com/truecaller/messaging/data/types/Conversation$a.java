package com.truecaller.messaging.data.types;

import com.truecaller.messaging.transport.history.HistoryTransportInfo;
import java.util.ArrayList;
import java.util.List;
import org.a.a.b;
import org.c.a.a.a.k;

public final class Conversation$a
{
  public long a;
  long b;
  int c;
  long d;
  int e;
  int f;
  String g;
  String h = "-1";
  b i;
  String j;
  int k;
  public List l;
  public boolean m;
  int n;
  int o;
  public int p = 0;
  public int q;
  int r = 3;
  int s;
  int t;
  int u;
  int v;
  HistoryTransportInfo w;
  ImGroupInfo x;
  
  public Conversation$a()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    l = localArrayList;
  }
  
  public final a a(int paramInt)
  {
    c = paramInt;
    return this;
  }
  
  public final a a(long paramLong)
  {
    a = paramLong;
    return this;
  }
  
  public final a a(ImGroupInfo paramImGroupInfo)
  {
    x = paramImGroupInfo;
    return this;
  }
  
  public final a a(HistoryTransportInfo paramHistoryTransportInfo)
  {
    w = paramHistoryTransportInfo;
    return this;
  }
  
  public final a a(String paramString)
  {
    g = paramString;
    return this;
  }
  
  public final a a(List paramList)
  {
    l.clear();
    l.addAll(paramList);
    return this;
  }
  
  public final a a(b paramb)
  {
    i = paramb;
    return this;
  }
  
  public final a a(boolean paramBoolean)
  {
    m = paramBoolean;
    return this;
  }
  
  public final Conversation a()
  {
    Conversation localConversation = new com/truecaller/messaging/data/types/Conversation;
    localConversation.<init>(this, (byte)0);
    return localConversation;
  }
  
  public final a b(int paramInt)
  {
    e = paramInt;
    return this;
  }
  
  public final a b(long paramLong)
  {
    b = paramLong;
    return this;
  }
  
  public final a b(String paramString)
  {
    paramString = (String)k.e(paramString, "-1");
    h = paramString;
    return this;
  }
  
  public final a c(int paramInt)
  {
    f = paramInt;
    return this;
  }
  
  public final a c(long paramLong)
  {
    d = paramLong;
    return this;
  }
  
  public final a c(String paramString)
  {
    j = paramString;
    return this;
  }
  
  public final a d(int paramInt)
  {
    k = paramInt;
    return this;
  }
  
  public final a e(int paramInt)
  {
    o = paramInt;
    return this;
  }
  
  public final a f(int paramInt)
  {
    n = paramInt;
    return this;
  }
  
  public final a g(int paramInt)
  {
    p = paramInt;
    return this;
  }
  
  public final a h(int paramInt)
  {
    q = paramInt;
    return this;
  }
  
  public final a i(int paramInt)
  {
    r = paramInt;
    return this;
  }
  
  public final a j(int paramInt)
  {
    u = paramInt;
    return this;
  }
  
  public final a k(int paramInt)
  {
    s = paramInt;
    return this;
  }
  
  public final a l(int paramInt)
  {
    t = paramInt;
    return this;
  }
  
  public final a m(int paramInt)
  {
    v = paramInt;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.Conversation.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */