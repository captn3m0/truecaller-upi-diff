package com.truecaller.messaging.data.types;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class ImGroupPermissions$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    ImGroupPermissions localImGroupPermissions = new com/truecaller/messaging/data/types/ImGroupPermissions;
    int i = paramParcel.readInt();
    int j = paramParcel.readInt();
    int k = paramParcel.readInt();
    int m = paramParcel.readInt();
    localImGroupPermissions.<init>(i, j, k, m);
    return localImGroupPermissions;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new ImGroupPermissions[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.ImGroupPermissions.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */