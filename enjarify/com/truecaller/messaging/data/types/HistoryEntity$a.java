package com.truecaller.messaging.data.types;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class HistoryEntity$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    new com/truecaller/messaging/data/types/HistoryEntity;
    int i = paramParcel.readInt();
    if (i != 0)
    {
      paramParcel = new com/truecaller/messaging/data/types/HistoryEntity;
      paramParcel.<init>();
      return paramParcel;
    }
    return null;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new HistoryEntity[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.HistoryEntity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */