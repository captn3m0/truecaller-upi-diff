package com.truecaller.messaging.data.types;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class ImGroupInfo$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    ImGroupInfo localImGroupInfo = new com/truecaller/messaging/data/types/ImGroupInfo;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    long l = paramParcel.readLong();
    String str4 = paramParcel.readString();
    int i = paramParcel.readInt();
    Object localObject1 = ImGroupPermissions.CREATOR.createFromParcel(paramParcel);
    Object localObject2 = localObject1;
    localObject2 = (ImGroupPermissions)localObject1;
    int j = paramParcel.readInt();
    localObject1 = localImGroupInfo;
    localImGroupInfo.<init>(str1, str2, str3, l, str4, i, (ImGroupPermissions)localObject2, j);
    return localImGroupInfo;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new ImGroupInfo[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.ImGroupInfo.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */