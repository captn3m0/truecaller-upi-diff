package com.truecaller.messaging.data.types;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import org.c.a.a.a.k;

public abstract class Entity
  implements Parcelable
{
  public static final String[] e;
  public static final String[] f = tmp44_31;
  public static final String[] g = { "video/3gpp", "video/mp4" };
  public final long h;
  public final int i;
  public final String j;
  
  static
  {
    String[] tmp4_1 = new String[4];
    String[] tmp5_4 = tmp4_1;
    String[] tmp5_4 = tmp4_1;
    tmp5_4[0] = "text/x-vcard";
    tmp5_4[1] = "text/vcard";
    tmp5_4[2] = "text/directory";
    String[] tmp18_5 = tmp5_4;
    tmp18_5[3] = "text/directory; profile=vcard";
    e = tmp18_5;
    String[] tmp30_27 = new String[4];
    String[] tmp31_30 = tmp30_27;
    String[] tmp31_30 = tmp30_27;
    tmp31_30[0] = "image/gif";
    tmp31_30[1] = "image/jpeg";
    tmp31_30[2] = "image/jpg";
    String[] tmp44_31 = tmp31_30;
    tmp44_31[3] = "image/png";
  }
  
  protected Entity(long paramLong, String paramString, int paramInt)
  {
    h = paramLong;
    String str = k.n(paramString);
    j = str;
    i = paramInt;
  }
  
  protected Entity(Parcel paramParcel)
  {
    long l = paramParcel.readLong();
    h = l;
    String str = paramParcel.readString();
    j = str;
    int k = paramParcel.readInt();
    i = k;
  }
  
  protected Entity(String paramString)
  {
    h = -1;
    paramString = k.n(paramString);
    j = paramString;
    i = 0;
  }
  
  public static BinaryEntity a(long paramLong1, String paramString, int paramInt1, Uri paramUri1, int paramInt2, int paramInt3, int paramInt4, boolean paramBoolean, long paramLong2, Uri paramUri2)
  {
    boolean bool1 = a(paramString);
    String[] arrayOfString = new String[0];
    AssertionUtil.AlwaysFatal.isFalse(bool1, arrayOfString);
    Object localObject1 = "image/";
    boolean bool2 = true;
    bool1 = k.a(paramString, (CharSequence)localObject1, bool2);
    if (bool1)
    {
      ImageEntity localImageEntity = new com/truecaller/messaging/data/types/ImageEntity;
      localObject1 = localImageEntity;
      localImageEntity.<init>(paramLong1, paramString, paramInt1, paramUri1, paramInt2, paramInt3, paramBoolean, paramLong2);
      return localImageEntity;
    }
    localObject1 = "video/";
    bool1 = k.a(paramString, (CharSequence)localObject1, bool2);
    if (bool1)
    {
      VideoEntity localVideoEntity = new com/truecaller/messaging/data/types/VideoEntity;
      localObject1 = localVideoEntity;
      localVideoEntity.<init>(paramLong1, paramString, paramInt1, paramUri1, paramBoolean, paramLong2, paramInt2, paramInt3, paramInt4, paramUri2);
      return localVideoEntity;
    }
    localObject1 = "audio/";
    bool1 = k.a(paramString, (CharSequence)localObject1, bool2);
    if (bool1)
    {
      localObject2 = new com/truecaller/messaging/data/types/AudioEntity;
      localObject1 = localObject2;
      ((AudioEntity)localObject2).<init>(paramLong1, paramString, paramInt1, paramUri1, paramLong2, paramInt4);
      return (BinaryEntity)localObject2;
    }
    Object localObject2 = new com/truecaller/messaging/data/types/BinaryEntity;
    localObject1 = localObject2;
    ((BinaryEntity)localObject2).<init>(paramLong1, paramString, paramInt1, paramUri1, paramBoolean, paramLong2);
    return (BinaryEntity)localObject2;
  }
  
  public static BinaryEntity a(long paramLong1, String paramString, int paramInt1, Uri paramUri, int paramInt2, int paramInt3, boolean paramBoolean, long paramLong2)
  {
    Uri localUri = Uri.EMPTY;
    return a(paramLong1, paramString, paramInt1, paramUri, paramInt2, paramInt3, -1, paramBoolean, paramLong2, localUri);
  }
  
  public static BinaryEntity a(long paramLong1, String paramString, int paramInt, Uri paramUri, long paramLong2)
  {
    return a(paramLong1, paramString, paramInt, paramUri, -1, -1, false, paramLong2);
  }
  
  public static BinaryEntity a(long paramLong1, String paramString, Uri paramUri1, int paramInt1, int paramInt2, int paramInt3, long paramLong2, Uri paramUri2)
  {
    return a(paramLong1, paramString, 0, paramUri1, paramInt1, paramInt2, paramInt3, false, paramLong2, paramUri2);
  }
  
  public static BinaryEntity a(String paramString, Uri paramUri, int paramInt1, int paramInt2, boolean paramBoolean, long paramLong)
  {
    return a(-1, paramString, 0, paramUri, paramInt1, paramInt2, paramBoolean, paramLong);
  }
  
  public static Entity a(long paramLong1, String paramString1, int paramInt1, String paramString2, int paramInt2, int paramInt3, int paramInt4, long paramLong2, String paramString3)
  {
    boolean bool1 = a(paramString1);
    if (bool1)
    {
      localObject1 = new com/truecaller/messaging/data/types/TextEntity;
      paramString3 = paramString2;
      ((TextEntity)localObject1).<init>(paramLong1, paramString1, paramInt1, paramString2);
      return (Entity)localObject1;
    }
    Object localObject1 = "image/";
    boolean bool2 = true;
    bool1 = k.a(paramString1, (CharSequence)localObject1, bool2);
    if (bool1)
    {
      ImageEntity localImageEntity = new com/truecaller/messaging/data/types/ImageEntity;
      localObject1 = localImageEntity;
      localObject2 = paramString2;
      localImageEntity.<init>(paramLong1, paramString1, paramInt1, paramString2, paramInt2, paramInt3, paramLong2);
      return localImageEntity;
    }
    localObject1 = "video/";
    bool1 = k.a(paramString1, (CharSequence)localObject1, bool2);
    if (bool1)
    {
      VideoEntity localVideoEntity = new com/truecaller/messaging/data/types/VideoEntity;
      localObject1 = localVideoEntity;
      localObject2 = paramString2;
      localVideoEntity.<init>(paramLong1, paramString1, paramInt1, paramString2, paramLong2, paramInt2, paramInt3, paramInt4, paramString3);
      return localVideoEntity;
    }
    localObject1 = "history";
    bool1 = paramString1.equals(localObject1);
    if (bool1)
    {
      localObject1 = new com/truecaller/messaging/data/types/HistoryEntity;
      ((HistoryEntity)localObject1).<init>();
      return (Entity)localObject1;
    }
    localObject1 = "audio/";
    bool1 = k.a(paramString1, (CharSequence)localObject1, bool2);
    if (bool1)
    {
      AudioEntity localAudioEntity = new com/truecaller/messaging/data/types/AudioEntity;
      localObject2 = Uri.parse(paramString2);
      localObject1 = localAudioEntity;
      localAudioEntity.<init>(paramLong1, paramString1, paramInt1, (Uri)localObject2, paramLong2, paramInt4);
      return localAudioEntity;
    }
    BinaryEntity localBinaryEntity = new com/truecaller/messaging/data/types/BinaryEntity;
    localObject1 = localBinaryEntity;
    Object localObject2 = paramString2;
    localBinaryEntity.<init>(paramLong1, paramString1, paramInt1, paramString2, paramLong2);
    return localBinaryEntity;
  }
  
  public static Entity a(String paramString1, int paramInt, String paramString2, long paramLong)
  {
    return a(-1, paramString1, paramInt, paramString2, -1, -1, -1, paramLong, "");
  }
  
  public static Entity a(String paramString1, String paramString2)
  {
    return a(paramString1, 0, paramString2, -1);
  }
  
  public static Entity a(String paramString1, String paramString2, int paramInt)
  {
    return a(-1, paramString1, 1, paramString2, -1, -1, paramInt, -1, "");
  }
  
  public static Entity a(String paramString1, String paramString2, long paramLong)
  {
    return a(-1, paramString1, 0, paramString2, -1, -1, -1, paramLong, "");
  }
  
  public static Entity a(String paramString1, String paramString2, String paramString3)
  {
    return a(-1, paramString1, 0, paramString2, -1, -1, -1, -1, paramString3);
  }
  
  public static boolean a(String paramString)
  {
    String str = "text/plain";
    boolean bool1 = str.equalsIgnoreCase(paramString);
    if (!bool1)
    {
      str = "text/html";
      boolean bool2 = str.equalsIgnoreCase(paramString);
      if (!bool2) {
        return false;
      }
    }
    return true;
  }
  
  public static boolean b(String paramString)
  {
    return "application/smil".equalsIgnoreCase(paramString);
  }
  
  public static boolean c(String paramString)
  {
    return k.a(paramString, "image/", true);
  }
  
  public static boolean d(String paramString)
  {
    return k.a(paramString, "video/", true);
  }
  
  public static boolean e(String paramString)
  {
    return k.a(paramString, "audio/", true);
  }
  
  public static boolean f(String paramString)
  {
    String[] arrayOfString = e;
    int k = arrayOfString.length;
    int m = 0;
    while (m < k)
    {
      String str = arrayOfString[m];
      boolean bool = k.b(str, paramString);
      if (bool) {
        return true;
      }
      m += 1;
    }
    return false;
  }
  
  public static boolean g(String paramString)
  {
    return "image/gif".equalsIgnoreCase(paramString);
  }
  
  public abstract void a(ContentValues paramContentValues);
  
  public abstract boolean a();
  
  public abstract boolean b();
  
  public abstract boolean c();
  
  public abstract boolean d();
  
  public int describeContents()
  {
    return 0;
  }
  
  public abstract boolean e();
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    if (paramObject != null)
    {
      Object localObject = getClass();
      Class localClass = paramObject.getClass();
      if (localObject == localClass)
      {
        paramObject = (Entity)paramObject;
        long l1 = h;
        long l2 = h;
        boolean bool2 = l1 < l2;
        if (!bool2)
        {
          localObject = j;
          paramObject = j;
          boolean bool3 = TextUtils.equals((CharSequence)localObject, (CharSequence)paramObject);
          if (bool3) {
            return bool1;
          }
        }
        return false;
      }
    }
    return false;
  }
  
  public abstract boolean f();
  
  public int hashCode()
  {
    long l1 = h;
    long l2 = l1 >>> 32;
    int k = (int)(l1 ^ l2) * 31;
    int m = j.hashCode();
    return k + m;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("{ id : ");
    long l = h;
    localStringBuilder.append(l);
    localStringBuilder.append(", type: \"");
    String str = j;
    localStringBuilder.append(str);
    localStringBuilder.append("\"\"}");
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    long l = h;
    paramParcel.writeLong(l);
    String str = j;
    paramParcel.writeString(str);
    paramInt = i;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.Entity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */