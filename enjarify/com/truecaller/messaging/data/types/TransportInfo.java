package com.truecaller.messaging.data.types;

import android.os.Parcelable;
import org.a.a.b;

public abstract interface TransportInfo
  extends Parcelable
{
  public abstract int a();
  
  public abstract String a(b paramb);
  
  public abstract int b();
  
  public abstract long c();
  
  public abstract long d();
  
  public abstract long e();
  
  public abstract boolean f();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.TransportInfo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */