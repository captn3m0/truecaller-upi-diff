package com.truecaller.messaging.data.types;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.a.m;
import c.g.a.b;
import c.g.b.k;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class ReplySnippet
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final long a;
  public final int b;
  public final List c;
  public final String d;
  public final String e;
  
  static
  {
    ReplySnippet.a locala = new com/truecaller/messaging/data/types/ReplySnippet$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public ReplySnippet(long paramLong, int paramInt, List paramList, String paramString1, String paramString2)
  {
    a = paramLong;
    b = paramInt;
    c = paramList;
    d = paramString1;
    e = paramString2;
  }
  
  public ReplySnippet(Message paramMessage)
  {
    this(l, i, localList, str1, str2);
  }
  
  public final String a()
  {
    Object localObject1 = (Iterable)c;
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (Collection)localObject2;
    localObject1 = ((Iterable)localObject1).iterator();
    boolean bool1;
    for (;;)
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = (Entity)((Iterator)localObject1).next();
      boolean bool2 = localObject3 instanceof TextEntity;
      if (!bool2)
      {
        bool1 = false;
        localObject3 = null;
      }
      localObject3 = (TextEntity)localObject3;
      if (localObject3 != null) {
        ((Collection)localObject2).add(localObject3);
      }
    }
    localObject2 = (Iterable)localObject2;
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    localObject2 = ((Iterable)localObject2).iterator();
    for (;;)
    {
      bool1 = ((Iterator)localObject2).hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = ((Iterator)localObject2).next();
      Object localObject4 = localObject3;
      localObject4 = a;
      String str = "it.content";
      k.a(localObject4, str);
      localObject4 = (CharSequence)localObject4;
      int i = ((CharSequence)localObject4).length();
      if (i > 0)
      {
        i = 1;
      }
      else
      {
        i = 0;
        localObject4 = null;
      }
      if (i != 0) {
        ((Collection)localObject1).add(localObject3);
      }
    }
    localObject1 = (List)localObject1;
    localObject2 = localObject1;
    localObject2 = (Iterable)localObject1;
    Object localObject3 = (CharSequence)"\n";
    localObject1 = ReplySnippet.b.a;
    Object localObject5 = localObject1;
    localObject5 = (b)localObject1;
    return m.a((Iterable)localObject2, (CharSequence)localObject3, null, null, 0, null, (b)localObject5, 30);
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof ReplySnippet;
      if (bool2)
      {
        paramObject = (ReplySnippet)paramObject;
        long l1 = a;
        long l2 = a;
        bool2 = l1 < l2;
        Object localObject1;
        if (!bool2)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          localObject1 = null;
        }
        if (bool2)
        {
          int i = b;
          int j = b;
          if (i == j)
          {
            i = 1;
          }
          else
          {
            i = 0;
            localObject1 = null;
          }
          if (i != 0)
          {
            localObject1 = c;
            Object localObject2 = c;
            boolean bool3 = k.a(localObject1, localObject2);
            if (bool3)
            {
              localObject1 = d;
              localObject2 = d;
              bool3 = k.a(localObject1, localObject2);
              if (bool3)
              {
                localObject1 = e;
                paramObject = e;
                boolean bool4 = k.a(localObject1, paramObject);
                if (bool4) {
                  return bool1;
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    long l1 = a;
    long l2 = l1 >>> 32;
    l1 ^= l2;
    int i = (int)l1 * 31;
    int j = b;
    i = (i + j) * 31;
    Object localObject = c;
    int k = 0;
    if (localObject != null)
    {
      j = localObject.hashCode();
    }
    else
    {
      j = 0;
      localObject = null;
    }
    i = (i + j) * 31;
    localObject = d;
    if (localObject != null)
    {
      j = localObject.hashCode();
    }
    else
    {
      j = 0;
      localObject = null;
    }
    i = (i + j) * 31;
    localObject = e;
    if (localObject != null) {
      k = localObject.hashCode();
    }
    return i + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ReplySnippet(id=");
    long l = a;
    localStringBuilder.append(l);
    localStringBuilder.append(", status=");
    int i = b;
    localStringBuilder.append(i);
    localStringBuilder.append(", entities=");
    Object localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", participantName=");
    localObject = d;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", participantNormalizedAddress=");
    localObject = e;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    long l = a;
    paramParcel.writeLong(l);
    int i = b;
    paramParcel.writeInt(i);
    Object localObject = c;
    int j = ((Collection)localObject).size();
    paramParcel.writeInt(j);
    localObject = ((Collection)localObject).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject).hasNext();
      if (!bool) {
        break;
      }
      Entity localEntity = (Entity)((Iterator)localObject).next();
      paramParcel.writeParcelable(localEntity, paramInt);
    }
    String str = d;
    paramParcel.writeString(str);
    str = e;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.ReplySnippet
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */