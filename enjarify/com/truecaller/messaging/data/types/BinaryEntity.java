package com.truecaller.messaging.data.types;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import org.c.a.a.a.k;

public class BinaryEntity
  extends Entity
{
  public static final Parcelable.Creator CREATOR;
  public final Uri b;
  public final boolean c;
  public final long d;
  
  static
  {
    BinaryEntity.1 local1 = new com/truecaller/messaging/data/types/BinaryEntity$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  protected BinaryEntity(long paramLong1, String paramString, int paramInt, Uri paramUri, boolean paramBoolean, long paramLong2)
  {
    super(paramLong1, paramString, paramInt);
    b = paramUri;
    c = paramBoolean;
    d = paramLong2;
  }
  
  protected BinaryEntity(long paramLong1, String paramString1, int paramInt, String paramString2, long paramLong2)
  {
    super(paramLong1, paramString1, paramInt);
    Uri localUri = Uri.parse(paramString2);
    b = localUri;
    c = false;
    d = paramLong2;
  }
  
  protected BinaryEntity(Parcel paramParcel)
  {
    super(paramParcel);
    Uri localUri = Uri.parse(paramParcel.readString());
    b = localUri;
    int i = paramParcel.readInt();
    int j = 1;
    if (i != j) {
      j = 0;
    }
    c = j;
    long l = paramParcel.readLong();
    d = l;
  }
  
  protected BinaryEntity(String paramString, Uri paramUri, long paramLong)
  {
    this(-1, paramString, 0, paramUri, false, paramLong);
  }
  
  public void a(ContentValues paramContentValues)
  {
    Object localObject = j;
    paramContentValues.put("type", (String)localObject);
    localObject = Integer.valueOf(this.i);
    paramContentValues.put("status", (Integer)localObject);
    localObject = b.toString();
    paramContentValues.put("content", (String)localObject);
    int i = -1;
    Integer localInteger = Integer.valueOf(i);
    paramContentValues.put("width", localInteger);
    localObject = Integer.valueOf(i);
    paramContentValues.put("height", (Integer)localObject);
    localObject = Long.valueOf(d);
    paramContentValues.put("size", (Long)localObject);
  }
  
  public boolean a()
  {
    return false;
  }
  
  public boolean b()
  {
    return false;
  }
  
  public boolean c()
  {
    return k.a(j, "video/", true);
  }
  
  public boolean d()
  {
    return f(j);
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean e()
  {
    return k.a(j, "audio/", true);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    Uri localUri = null;
    if (paramObject != null)
    {
      Class localClass1 = getClass();
      Class localClass2 = paramObject.getClass();
      if (localClass1 == localClass2)
      {
        boolean bool = super.equals(paramObject);
        if (!bool) {
          return false;
        }
        paramObject = (BinaryEntity)paramObject;
        localUri = b;
        paramObject = b;
        return localUri.equals(paramObject);
      }
    }
    return false;
  }
  
  public final boolean f()
  {
    return false;
  }
  
  public int hashCode()
  {
    int i = super.hashCode() * 31;
    int j = b.hashCode();
    return i + j;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    String str = b.toString();
    paramParcel.writeString(str);
    paramInt = c;
    paramParcel.writeInt(paramInt);
    long l = d;
    paramParcel.writeLong(l);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.BinaryEntity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */