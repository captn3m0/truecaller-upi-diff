package com.truecaller.messaging.data.types;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class ImGroupInfo
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final String a;
  public final String b;
  public final String c;
  public final long d;
  public final String e;
  public final int f;
  public final ImGroupPermissions g;
  public final int h;
  
  static
  {
    ImGroupInfo.a locala = new com/truecaller/messaging/data/types/ImGroupInfo$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public ImGroupInfo(String paramString1, String paramString2, String paramString3, long paramLong, String paramString4, int paramInt1, ImGroupPermissions paramImGroupPermissions, int paramInt2)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
    d = paramLong;
    e = paramString4;
    f = paramInt1;
    g = paramImGroupPermissions;
    h = paramInt2;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof ImGroupInfo;
      if (bool2)
      {
        paramObject = (ImGroupInfo)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          localObject1 = b;
          localObject2 = b;
          bool2 = k.a(localObject1, localObject2);
          if (bool2)
          {
            localObject1 = c;
            localObject2 = c;
            bool2 = k.a(localObject1, localObject2);
            if (bool2)
            {
              long l1 = d;
              long l2 = d;
              bool2 = l1 < l2;
              if (!bool2)
              {
                bool2 = true;
              }
              else
              {
                bool2 = false;
                localObject1 = null;
              }
              if (bool2)
              {
                localObject1 = e;
                localObject2 = e;
                bool2 = k.a(localObject1, localObject2);
                if (bool2)
                {
                  int i = f;
                  int k = f;
                  if (i == k)
                  {
                    i = 1;
                  }
                  else
                  {
                    i = 0;
                    localObject1 = null;
                  }
                  if (i != 0)
                  {
                    localObject1 = g;
                    localObject2 = g;
                    boolean bool3 = k.a(localObject1, localObject2);
                    if (bool3)
                    {
                      int j = h;
                      int m = h;
                      if (j == m)
                      {
                        m = 1;
                      }
                      else
                      {
                        m = 0;
                        paramObject = null;
                      }
                      if (m != 0) {
                        return bool1;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str = a;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = b;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = c;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    long l1 = d;
    int m = 32;
    long l2 = l1 >>> m;
    l1 ^= l2;
    int n = (int)l1;
    j = (j + n) * 31;
    localObject = e;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    int k = f;
    j = (j + k) * 31;
    localObject = g;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    j = (j + i) * 31;
    i = h;
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ImGroupInfo(groupId=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", title=");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", avatar=");
    localObject = c;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", invitedDate=");
    long l = d;
    localStringBuilder.append(l);
    localStringBuilder.append(", invitedBy=");
    localObject = e;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", roles=");
    int i = f;
    localStringBuilder.append(i);
    localStringBuilder.append(", permissions=");
    localObject = g;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", notificationSettings=");
    i = h;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = a;
    paramParcel.writeString(str);
    str = b;
    paramParcel.writeString(str);
    str = c;
    paramParcel.writeString(str);
    long l = d;
    paramParcel.writeLong(l);
    str = e;
    paramParcel.writeString(str);
    paramInt = f;
    paramParcel.writeInt(paramInt);
    g.writeToParcel(paramParcel, 0);
    paramInt = h;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.ImGroupInfo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */