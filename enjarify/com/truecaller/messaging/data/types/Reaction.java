package com.truecaller.messaging.data.types;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class Reaction
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public static final Reaction.a g;
  public final long a;
  public final long b;
  public final String c;
  public final String d;
  public final long e;
  public final int f;
  
  static
  {
    Object localObject = new com/truecaller/messaging/data/types/Reaction$a;
    ((Reaction.a)localObject).<init>((byte)0);
    g = (Reaction.a)localObject;
    localObject = new com/truecaller/messaging/data/types/Reaction$b;
    ((Reaction.b)localObject).<init>();
    CREATOR = (Parcelable.Creator)localObject;
  }
  
  public Reaction(long paramLong1, long paramLong2, String paramString1, String paramString2, long paramLong3, int paramInt)
  {
    a = paramLong1;
    b = paramLong2;
    c = paramString1;
    d = paramString2;
    e = paramLong3;
    f = paramInt;
  }
  
  public Reaction(Parcel paramParcel)
  {
    this(l1, l2, str1, str2, l3, i);
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    Reaction localReaction = this;
    localReaction = (Reaction)this;
    boolean bool1 = true;
    if (localReaction == paramObject) {
      return bool1;
    }
    localReaction = null;
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      boolean bool2 = k.a(localObject1, localObject2) ^ bool1;
      if (!bool2)
      {
        bool2 = super.equals(paramObject);
        if (!bool2) {
          return false;
        }
        bool2 = paramObject instanceof Reaction;
        if (bool2)
        {
          paramObject = (Reaction)paramObject;
          long l1 = a;
          long l2 = a;
          boolean bool3 = l1 < l2;
          if (!bool3)
          {
            l1 = b;
            l2 = b;
            bool3 = l1 < l2;
            if (!bool3)
            {
              localObject1 = c;
              localObject2 = c;
              bool2 = k.a(localObject1, localObject2);
              if (bool2)
              {
                localObject1 = d;
                localObject2 = d;
                bool2 = k.a(localObject1, localObject2);
                if (bool2)
                {
                  l1 = e;
                  l2 = e;
                  bool3 = l1 < l2;
                  if (!bool3)
                  {
                    int j = f;
                    int i = f;
                    if (j == i) {
                      return bool1;
                    }
                  }
                }
              }
            }
          }
        }
        return false;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    int i = super.hashCode() * 31;
    int j = Long.valueOf(a).hashCode();
    i = (i + j) * 31;
    long l = b;
    j = Long.valueOf(l).hashCode();
    i = (i + j) * 31;
    j = c.hashCode();
    i = (i + j) * 31;
    String str = d;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    i = (i + j) * 31;
    j = Long.valueOf(e).hashCode();
    i = (i + j) * 31;
    j = Integer.valueOf(f).hashCode();
    return i + j;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    long l = a;
    paramParcel.writeLong(l);
    l = b;
    paramParcel.writeLong(l);
    String str = c;
    paramParcel.writeString(str);
    str = d;
    paramParcel.writeString(str);
    l = e;
    paramParcel.writeLong(l);
    paramInt = f;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.Reaction
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */