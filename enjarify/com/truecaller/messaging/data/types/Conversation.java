package com.truecaller.messaging.data.types;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.truecaller.messaging.transport.history.HistoryTransportInfo;
import java.util.List;
import org.a.a.b;
import org.c.a.a.a.k;

public class Conversation
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final long a;
  public final long b;
  public final int c;
  public final long d;
  public final int e;
  public final int f;
  public final String g;
  public final String h;
  public final b i;
  public final String j;
  public final int k;
  public final Participant[] l;
  public final boolean m;
  public final int n;
  public final int o;
  public final int p;
  public final int q;
  public final int r;
  public final int s;
  public final int t;
  public final int u;
  public final int v;
  public HistoryTransportInfo w;
  public final ImGroupInfo x;
  private String y;
  
  static
  {
    Conversation.1 local1 = new com/truecaller/messaging/data/types/Conversation$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  private Conversation(Parcel paramParcel)
  {
    long l1 = paramParcel.readLong();
    a = l1;
    l1 = paramParcel.readLong();
    b = l1;
    int i1 = paramParcel.readInt();
    c = i1;
    l1 = paramParcel.readLong();
    d = l1;
    i1 = paramParcel.readInt();
    e = i1;
    i1 = paramParcel.readInt();
    f = i1;
    Object localObject = paramParcel.readString();
    g = ((String)localObject);
    localObject = paramParcel.readString();
    h = ((String)localObject);
    localObject = new org/a/a/b;
    long l2 = paramParcel.readLong();
    ((b)localObject).<init>(l2);
    i = ((b)localObject);
    localObject = paramParcel.readString();
    j = ((String)localObject);
    i1 = paramParcel.readInt();
    k = i1;
    localObject = new Participant[paramParcel.readInt()];
    l = ((Participant[])localObject);
    localObject = l;
    Parcelable.Creator localCreator = Participant.CREATOR;
    paramParcel.readTypedArray((Object[])localObject, localCreator);
    i1 = paramParcel.readByte();
    int i2 = 1;
    if (i1 != i2)
    {
      i2 = 0;
      localCreator = null;
    }
    m = i2;
    i1 = paramParcel.readInt();
    n = i1;
    i1 = paramParcel.readInt();
    o = i1;
    i1 = paramParcel.readInt();
    p = i1;
    i1 = paramParcel.readInt();
    q = i1;
    i1 = paramParcel.readInt();
    r = i1;
    i1 = paramParcel.readInt();
    s = i1;
    i1 = paramParcel.readInt();
    t = i1;
    i1 = paramParcel.readInt();
    v = i1;
    i1 = paramParcel.readInt();
    u = i1;
    localObject = HistoryTransportInfo.class.getClassLoader();
    localObject = (HistoryTransportInfo)paramParcel.readParcelable((ClassLoader)localObject);
    w = ((HistoryTransportInfo)localObject);
    localObject = ImGroupInfo.class.getClassLoader();
    paramParcel = (ImGroupInfo)paramParcel.readParcelable((ClassLoader)localObject);
    x = paramParcel;
  }
  
  private Conversation(Conversation.a parama)
  {
    long l1 = a;
    a = l1;
    l1 = b;
    b = l1;
    int i1 = c;
    c = i1;
    l1 = d;
    d = l1;
    i1 = e;
    e = i1;
    i1 = f;
    f = i1;
    Object localObject = g;
    g = ((String)localObject);
    localObject = h;
    h = ((String)localObject);
    localObject = i;
    if (localObject != null)
    {
      localObject = i;
    }
    else
    {
      localObject = new org/a/a/b;
      long l2 = 0L;
      ((b)localObject).<init>(l2);
    }
    i = ((b)localObject);
    localObject = k.n(j);
    j = ((String)localObject);
    i1 = k;
    k = i1;
    localObject = l;
    Participant[] arrayOfParticipant = new Participant[l.size()];
    localObject = (Participant[])((List)localObject).toArray(arrayOfParticipant);
    l = ((Participant[])localObject);
    boolean bool = m;
    m = bool;
    int i2 = n;
    n = i2;
    i2 = o;
    o = i2;
    i2 = p;
    p = i2;
    i2 = q;
    q = i2;
    i2 = r;
    r = i2;
    i2 = u;
    u = i2;
    i2 = s;
    s = i2;
    i2 = t;
    t = i2;
    i2 = v;
    v = i2;
    localObject = w;
    w = ((HistoryTransportInfo)localObject);
    parama = x;
    x = parama;
  }
  
  public final String a()
  {
    String str = y;
    if (str == null)
    {
      str = com.truecaller.messaging.i.g.a(l);
      y = str;
    }
    return y;
  }
  
  public final boolean a(boolean paramBoolean)
  {
    Participant[] arrayOfParticipant = l;
    int i1 = arrayOfParticipant.length;
    int i2 = 0;
    while (i2 < i1)
    {
      Participant localParticipant = arrayOfParticipant[i2];
      boolean bool = localParticipant.a(paramBoolean);
      if (bool) {
        return true;
      }
      i2 += 1;
    }
    return false;
  }
  
  public final String b()
  {
    Object localObject = l;
    boolean bool = com.truecaller.messaging.i.g.c((Participant[])localObject);
    if (bool)
    {
      localObject = x;
      if (localObject == null) {
        return "";
      }
      return k.n(b);
    }
    return a();
  }
  
  public final boolean c()
  {
    Participant[] arrayOfParticipant = l;
    int i1 = arrayOfParticipant.length;
    int i2 = 0;
    while (i2 < i1)
    {
      Participant localParticipant = arrayOfParticipant[i2];
      boolean bool = localParticipant.g();
      if (bool) {
        return true;
      }
      i2 += 1;
    }
    return false;
  }
  
  public final int d()
  {
    int i1 = n;
    if (i1 <= 0)
    {
      i1 = o;
      if (i1 > 0)
      {
        boolean bool = c();
        if (!bool) {}
      }
      else
      {
        return 2;
      }
    }
    return 4;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    long l1 = a;
    paramParcel.writeLong(l1);
    l1 = b;
    paramParcel.writeLong(l1);
    int i1 = c;
    paramParcel.writeInt(i1);
    l1 = d;
    paramParcel.writeLong(l1);
    i1 = e;
    paramParcel.writeInt(i1);
    i1 = f;
    paramParcel.writeInt(i1);
    Object localObject = g;
    paramParcel.writeString((String)localObject);
    localObject = h;
    paramParcel.writeString((String)localObject);
    l1 = i.a;
    paramParcel.writeLong(l1);
    localObject = j;
    paramParcel.writeString((String)localObject);
    i1 = k;
    paramParcel.writeInt(i1);
    i1 = l.length;
    paramParcel.writeInt(i1);
    localObject = l;
    paramParcel.writeTypedArray((Parcelable[])localObject, 0);
    int i2 = m;
    paramParcel.writeByte(i2);
    int i3 = n;
    paramParcel.writeInt(i3);
    i3 = o;
    paramParcel.writeInt(i3);
    i3 = p;
    paramParcel.writeInt(i3);
    i3 = q;
    paramParcel.writeInt(i3);
    i3 = r;
    paramParcel.writeInt(i3);
    i3 = s;
    paramParcel.writeInt(i3);
    i3 = t;
    paramParcel.writeInt(i3);
    i3 = v;
    paramParcel.writeInt(i3);
    i3 = u;
    paramParcel.writeInt(i3);
    localObject = w;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    localObject = x;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.Conversation
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */