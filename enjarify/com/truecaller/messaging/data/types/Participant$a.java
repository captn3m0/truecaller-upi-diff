package com.truecaller.messaging.data.types;

import com.truecaller.log.AssertionUtil.AlwaysFatal;

public final class Participant$a
{
  final int a;
  public long b;
  public String c;
  public String d;
  public String e;
  public String f;
  public String g;
  public long h;
  public int i;
  public boolean j;
  public int k;
  public String l;
  public String m;
  public int n;
  public long o;
  public int p;
  public String q;
  public long r;
  public int s;
  
  public Participant$a(int paramInt)
  {
    long l1 = -1;
    b = l1;
    h = l1;
    o = l1;
    s = 0;
    a = paramInt;
  }
  
  private Participant$a(Participant paramParticipant)
  {
    long l1 = -1;
    b = l1;
    h = l1;
    o = l1;
    s = 0;
    int i1 = c;
    a = i1;
    l1 = b;
    b = l1;
    String str = d;
    c = str;
    str = e;
    d = str;
    l1 = i;
    h = l1;
    str = f;
    e = str;
    str = g;
    f = str;
    str = h;
    g = str;
    i1 = j;
    i = i1;
    boolean bool = k;
    j = bool;
    int i2 = l;
    k = i2;
    str = m;
    l = str;
    str = n;
    m = str;
    i2 = o;
    n = i2;
    l1 = p;
    o = l1;
    i2 = q;
    p = i2;
    i2 = r;
    s = i2;
    str = s;
    q = str;
    l1 = t;
    r = l1;
  }
  
  public final a a(int paramInt)
  {
    i = paramInt;
    return this;
  }
  
  public final a a(long paramLong)
  {
    b = paramLong;
    return this;
  }
  
  public final a a(String paramString)
  {
    d = paramString;
    return this;
  }
  
  public final a a(boolean paramBoolean)
  {
    j = paramBoolean;
    return this;
  }
  
  public final Participant a()
  {
    Object localObject = e;
    String[] arrayOfString = new String[0];
    AssertionUtil.AlwaysFatal.isNotNull(localObject, arrayOfString);
    localObject = new com/truecaller/messaging/data/types/Participant;
    ((Participant)localObject).<init>(this, (byte)0);
    return (Participant)localObject;
  }
  
  public final a b(int paramInt)
  {
    k = paramInt;
    return this;
  }
  
  public final a b(long paramLong)
  {
    h = paramLong;
    return this;
  }
  
  public final a b(String paramString)
  {
    e = paramString;
    return this;
  }
  
  public final a c(int paramInt)
  {
    n = paramInt;
    return this;
  }
  
  public final a c(long paramLong)
  {
    o = paramLong;
    return this;
  }
  
  public final a c(String paramString)
  {
    f = paramString;
    return this;
  }
  
  public final a d(int paramInt)
  {
    p = paramInt;
    return this;
  }
  
  public final a d(long paramLong)
  {
    r = paramLong;
    return this;
  }
  
  public final a d(String paramString)
  {
    c = paramString;
    return this;
  }
  
  public final a e(int paramInt)
  {
    s = paramInt;
    return this;
  }
  
  public final a e(String paramString)
  {
    g = paramString;
    return this;
  }
  
  public final a f(String paramString)
  {
    l = paramString;
    return this;
  }
  
  public final a g(String paramString)
  {
    m = paramString;
    return this;
  }
  
  public final a h(String paramString)
  {
    q = paramString;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.Participant.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */