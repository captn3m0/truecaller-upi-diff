package com.truecaller.messaging.data.types;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Parcel;
import c.g.b.k;

public final class AudioEntity
  extends BinaryEntity
{
  public static final AudioEntity.a CREATOR;
  public final int a;
  
  static
  {
    AudioEntity.a locala = new com/truecaller/messaging/data/types/AudioEntity$a;
    locala.<init>((byte)0);
    CREATOR = locala;
  }
  
  public AudioEntity(long paramLong1, String paramString, int paramInt1, Uri paramUri, long paramLong2, int paramInt2)
  {
    super(paramLong1, paramString, paramInt1, paramUri, false, paramLong2);
    a = paramInt2;
  }
  
  public AudioEntity(Parcel paramParcel)
  {
    super(paramParcel);
    int i = paramParcel.readInt();
    a = i;
  }
  
  public final void a(ContentValues paramContentValues)
  {
    k.b(paramContentValues, "contentValues");
    Object localObject = j;
    paramContentValues.put("type", (String)localObject);
    localObject = Integer.valueOf(this.i);
    paramContentValues.put("status", (Integer)localObject);
    localObject = b.toString();
    paramContentValues.put("content", (String)localObject);
    int i = -1;
    Integer localInteger = Integer.valueOf(i);
    paramContentValues.put("width", localInteger);
    localObject = Integer.valueOf(i);
    paramContentValues.put("height", (Integer)localObject);
    localObject = Long.valueOf(d);
    paramContentValues.put("size", (Long)localObject);
    localObject = Integer.valueOf(a);
    paramContentValues.put("duration", (Integer)localObject);
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    super.writeToParcel(paramParcel, paramInt);
    paramInt = a;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.AudioEntity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */