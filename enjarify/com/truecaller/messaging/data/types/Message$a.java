package com.truecaller.messaging.data.types;

import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.transport.NullTransportInfo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.a.a.b;

public final class Message$a
{
  public long a;
  public long b;
  public Participant c;
  public b d;
  public b e;
  public int f;
  public boolean g;
  public boolean h;
  public boolean i;
  int j;
  public int k;
  String l;
  TransportInfo m;
  List n;
  boolean o;
  public String p;
  public String q;
  int r;
  public int s;
  int t;
  public int u;
  b v;
  long w;
  ReplySnippet x;
  
  public Message$a()
  {
    long l1 = -1;
    a = l1;
    b = l1;
    int i1 = 3;
    j = i1;
    k = i1;
    l = "-1";
    NullTransportInfo localNullTransportInfo = NullTransportInfo.b;
    m = localNullTransportInfo;
    o = false;
    w = l1;
  }
  
  private Message$a(Message paramMessage)
  {
    long l1 = -1;
    a = l1;
    b = l1;
    int i1 = 3;
    j = i1;
    k = i1;
    l = "-1";
    NullTransportInfo localNullTransportInfo = NullTransportInfo.b;
    m = localNullTransportInfo;
    i1 = 0;
    localNullTransportInfo = null;
    o = false;
    w = l1;
    l1 = Message.a(paramMessage);
    a = l1;
    l1 = b;
    b = l1;
    Object localObject = c;
    c = ((Participant)localObject);
    localObject = e;
    e = ((b)localObject);
    localObject = d;
    d = ((b)localObject);
    int i2 = f;
    f = i2;
    boolean bool1 = g;
    g = bool1;
    bool1 = h;
    h = bool1;
    bool1 = i;
    i = bool1;
    int i3 = j;
    j = i3;
    i3 = k;
    k = i3;
    localObject = m;
    m = ((TransportInfo)localObject);
    localObject = l;
    l = ((String)localObject);
    localObject = n;
    i3 = localObject.length;
    if (i3 > 0)
    {
      localObject = new java/util/ArrayList;
      ((ArrayList)localObject).<init>();
      n = ((List)localObject);
      localObject = n;
      Entity[] arrayOfEntity = n;
      Collections.addAll((Collection)localObject, arrayOfEntity);
    }
    localObject = p;
    p = ((String)localObject);
    boolean bool2 = w;
    o = bool2;
    int i4 = q;
    r = i4;
    i4 = r;
    s = i4;
    i4 = s;
    t = i4;
    i4 = t;
    u = i4;
    localObject = u;
    v = ((b)localObject);
    l1 = x;
    w = l1;
    localObject = o;
    q = ((String)localObject);
    paramMessage = v;
    x = paramMessage;
  }
  
  public final a a()
  {
    List localList = n;
    if (localList != null) {
      localList.clear();
    }
    return this;
  }
  
  public final a a(int paramInt)
  {
    f = paramInt;
    return this;
  }
  
  public final a a(int paramInt, TransportInfo paramTransportInfo)
  {
    j = paramInt;
    m = paramTransportInfo;
    return this;
  }
  
  public final a a(long paramLong)
  {
    a = paramLong;
    return this;
  }
  
  public final a a(Entity paramEntity)
  {
    Object localObject = n;
    if (localObject == null)
    {
      localObject = new java/util/ArrayList;
      ((ArrayList)localObject).<init>();
      n = ((List)localObject);
    }
    n.add(paramEntity);
    return this;
  }
  
  public final a a(Participant paramParticipant)
  {
    c = paramParticipant;
    return this;
  }
  
  public final a a(ReplySnippet paramReplySnippet)
  {
    x = paramReplySnippet;
    return this;
  }
  
  public final a a(String paramString)
  {
    String str = "-1";
    if (paramString == null) {
      paramString = str;
    }
    l = paramString;
    return this;
  }
  
  public final a a(Collection paramCollection)
  {
    Object localObject = n;
    if (localObject == null)
    {
      localObject = new java/util/ArrayList;
      ((ArrayList)localObject).<init>();
      n = ((List)localObject);
    }
    n.addAll(paramCollection);
    return this;
  }
  
  public final a a(b paramb)
  {
    d = paramb;
    return this;
  }
  
  public final a a(boolean paramBoolean)
  {
    g = paramBoolean;
    return this;
  }
  
  public final a b(int paramInt)
  {
    k = paramInt;
    return this;
  }
  
  public final a b(long paramLong)
  {
    b = paramLong;
    return this;
  }
  
  public final a b(String paramString)
  {
    q = paramString;
    return this;
  }
  
  public final a b(boolean paramBoolean)
  {
    h = paramBoolean;
    return this;
  }
  
  public final Message b()
  {
    Object localObject = c;
    String[] arrayOfString = new String[0];
    AssertionUtil.isNotNull(localObject, arrayOfString);
    localObject = new com/truecaller/messaging/data/types/Message;
    ((Message)localObject).<init>(this, (byte)0);
    return (Message)localObject;
  }
  
  public final a c(int paramInt)
  {
    r = paramInt;
    return this;
  }
  
  public final a c(long paramLong)
  {
    b localb = new org/a/a/b;
    localb.<init>(paramLong);
    e = localb;
    return this;
  }
  
  public final a c(String paramString)
  {
    p = paramString;
    return this;
  }
  
  public final a c(boolean paramBoolean)
  {
    i = paramBoolean;
    return this;
  }
  
  public final a d(int paramInt)
  {
    u = paramInt;
    return this;
  }
  
  public final a d(long paramLong)
  {
    b localb = new org/a/a/b;
    localb.<init>(paramLong);
    d = localb;
    return this;
  }
  
  public final a e(long paramLong)
  {
    b localb = new org/a/a/b;
    localb.<init>(paramLong);
    v = localb;
    return this;
  }
  
  public final a f(long paramLong)
  {
    w = paramLong;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.types.Message.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */