package com.truecaller.messaging.data;

import dagger.a.d;
import javax.inject.Provider;

public final class r
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private r(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
  }
  
  public static r a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    r localr = new com/truecaller/messaging/data/r;
    localr.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localr;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */