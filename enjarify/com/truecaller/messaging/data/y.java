package com.truecaller.messaging.data;

import android.content.ContentResolver;

public final class y
  implements x
{
  private final ContentResolver a;
  private final c b;
  
  public y(ContentResolver paramContentResolver, c paramc)
  {
    a = paramContentResolver;
    b = paramc;
  }
  
  /* Error */
  public final java.util.Map a(java.util.List paramList)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore_2
    //   2: aload_1
    //   3: astore_3
    //   4: aload_1
    //   5: ldc 29
    //   7: invokestatic 17	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   10: aload_1
    //   11: checkcast 31	java/lang/Iterable
    //   14: astore_3
    //   15: new 33	java/util/LinkedHashMap
    //   18: astore 4
    //   20: aload 4
    //   22: invokespecial 34	java/util/LinkedHashMap:<init>	()V
    //   25: aload 4
    //   27: checkcast 36	java/util/Map
    //   30: astore 4
    //   32: aload_3
    //   33: invokeinterface 40 1 0
    //   38: astore_3
    //   39: aload_3
    //   40: invokeinterface 46 1 0
    //   45: istore 5
    //   47: iload 5
    //   49: ifeq +90 -> 139
    //   52: aload_3
    //   53: invokeinterface 50 1 0
    //   58: astore 6
    //   60: aload 6
    //   62: astore 7
    //   64: aload 6
    //   66: checkcast 52	com/truecaller/messaging/data/types/Message
    //   69: getfield 55	com/truecaller/messaging/data/types/Message:b	J
    //   72: lstore 8
    //   74: lload 8
    //   76: invokestatic 61	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   79: astore 7
    //   81: aload 4
    //   83: aload 7
    //   85: invokeinterface 65 2 0
    //   90: astore 10
    //   92: aload 10
    //   94: ifnonnull +25 -> 119
    //   97: new 67	java/util/ArrayList
    //   100: astore 10
    //   102: aload 10
    //   104: invokespecial 68	java/util/ArrayList:<init>	()V
    //   107: aload 4
    //   109: aload 7
    //   111: aload 10
    //   113: invokeinterface 72 3 0
    //   118: pop
    //   119: aload 10
    //   121: checkcast 74	java/util/List
    //   124: astore 10
    //   126: aload 10
    //   128: aload 6
    //   130: invokeinterface 78 2 0
    //   135: pop
    //   136: goto -97 -> 39
    //   139: aload_2
    //   140: getfield 25	com/truecaller/messaging/data/y:a	Landroid/content/ContentResolver;
    //   143: astore 11
    //   145: invokestatic 83	com/truecaller/content/TruecallerContract$g:a	()Landroid/net/Uri;
    //   148: astore 12
    //   150: new 85	java/lang/StringBuilder
    //   153: astore_3
    //   154: aload_3
    //   155: ldc 87
    //   157: invokespecial 90	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   160: aload 4
    //   162: invokeinterface 94 1 0
    //   167: astore 6
    //   169: aload 6
    //   171: astore 13
    //   173: aload 6
    //   175: checkcast 31	java/lang/Iterable
    //   178: astore 13
    //   180: bipush 63
    //   182: istore 14
    //   184: aload 13
    //   186: aconst_null
    //   187: aconst_null
    //   188: aconst_null
    //   189: iconst_0
    //   190: aconst_null
    //   191: aconst_null
    //   192: iload 14
    //   194: invokestatic 100	c/a/m:a	(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lc/g/a/b;I)Ljava/lang/String;
    //   197: astore 6
    //   199: aload_3
    //   200: aload 6
    //   202: invokevirtual 104	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   205: pop
    //   206: bipush 41
    //   208: istore 5
    //   210: aload_3
    //   211: iload 5
    //   213: invokevirtual 108	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   216: pop
    //   217: aload_3
    //   218: invokevirtual 112	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   221: astore 13
    //   223: aload 11
    //   225: aload 12
    //   227: aconst_null
    //   228: aload 13
    //   230: aconst_null
    //   231: aconst_null
    //   232: invokevirtual 118	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   235: astore_3
    //   236: aload_2
    //   237: getfield 27	com/truecaller/messaging/data/y:b	Lcom/truecaller/messaging/data/c;
    //   240: astore 6
    //   242: aload 6
    //   244: aload_3
    //   245: invokeinterface 123 2 0
    //   250: astore_3
    //   251: aload_3
    //   252: ifnull +232 -> 484
    //   255: aload_3
    //   256: checkcast 125	android/database/Cursor
    //   259: astore_3
    //   260: aload_3
    //   261: astore 6
    //   263: aload_3
    //   264: checkcast 127	java/io/Closeable
    //   267: astore 6
    //   269: iconst_0
    //   270: istore 15
    //   272: aconst_null
    //   273: astore 7
    //   275: new 67	java/util/ArrayList
    //   278: astore 10
    //   280: aload 10
    //   282: invokespecial 68	java/util/ArrayList:<init>	()V
    //   285: aload 10
    //   287: checkcast 129	java/util/Collection
    //   290: astore 10
    //   292: aload_3
    //   293: invokeinterface 132 1 0
    //   298: istore 16
    //   300: iload 16
    //   302: ifeq +34 -> 336
    //   305: aload_3
    //   306: astore 11
    //   308: aload_3
    //   309: checkcast 134	com/truecaller/messaging/data/a/a
    //   312: astore 11
    //   314: aload 11
    //   316: invokeinterface 137 1 0
    //   321: astore 11
    //   323: aload 10
    //   325: aload 11
    //   327: invokeinterface 138 2 0
    //   332: pop
    //   333: goto -41 -> 292
    //   336: aload 10
    //   338: checkcast 74	java/util/List
    //   341: astore 10
    //   343: aload 6
    //   345: aconst_null
    //   346: invokestatic 143	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   349: aload 10
    //   351: checkcast 31	java/lang/Iterable
    //   354: astore 10
    //   356: aload 10
    //   358: bipush 10
    //   360: invokestatic 147	c/a/m:a	(Ljava/lang/Iterable;I)I
    //   363: invokestatic 152	c/a/ag:a	(I)I
    //   366: istore 17
    //   368: bipush 16
    //   370: istore 5
    //   372: iload 17
    //   374: iload 5
    //   376: invokestatic 159	c/k/i:c	(II)I
    //   379: istore 17
    //   381: new 33	java/util/LinkedHashMap
    //   384: astore 6
    //   386: aload 6
    //   388: iload 17
    //   390: invokespecial 162	java/util/LinkedHashMap:<init>	(I)V
    //   393: aload 6
    //   395: checkcast 36	java/util/Map
    //   398: astore 6
    //   400: aload 10
    //   402: invokeinterface 40 1 0
    //   407: astore_3
    //   408: aload_3
    //   409: invokeinterface 46 1 0
    //   414: istore 15
    //   416: iload 15
    //   418: ifeq +71 -> 489
    //   421: aload_3
    //   422: invokeinterface 50 1 0
    //   427: astore 7
    //   429: aload 7
    //   431: astore 10
    //   433: aload 7
    //   435: checkcast 164	com/truecaller/messaging/data/types/Conversation
    //   438: getfield 166	com/truecaller/messaging/data/types/Conversation:a	J
    //   441: lstore 18
    //   443: lload 18
    //   445: invokestatic 61	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   448: astore 10
    //   450: aload 6
    //   452: aload 10
    //   454: aload 7
    //   456: invokeinterface 72 3 0
    //   461: pop
    //   462: goto -54 -> 408
    //   465: astore_3
    //   466: goto +9 -> 475
    //   469: astore_3
    //   470: aload_3
    //   471: astore 7
    //   473: aload_3
    //   474: athrow
    //   475: aload 6
    //   477: aload 7
    //   479: invokestatic 143	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   482: aload_3
    //   483: athrow
    //   484: invokestatic 169	c/a/ag:a	()Ljava/util/Map;
    //   487: astore 6
    //   489: new 33	java/util/LinkedHashMap
    //   492: astore_3
    //   493: aload_3
    //   494: invokespecial 34	java/util/LinkedHashMap:<init>	()V
    //   497: aload 4
    //   499: invokeinterface 172 1 0
    //   504: invokeinterface 175 1 0
    //   509: astore 4
    //   511: aload 4
    //   513: invokeinterface 46 1 0
    //   518: istore 15
    //   520: iload 15
    //   522: ifeq +83 -> 605
    //   525: aload 4
    //   527: invokeinterface 50 1 0
    //   532: checkcast 177	java/util/Map$Entry
    //   535: astore 7
    //   537: aload 7
    //   539: invokeinterface 180 1 0
    //   544: checkcast 182	java/lang/Number
    //   547: invokevirtual 186	java/lang/Number:longValue	()J
    //   550: lstore 18
    //   552: lload 18
    //   554: invokestatic 61	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   557: astore 10
    //   559: aload 6
    //   561: aload 10
    //   563: invokeinterface 189 2 0
    //   568: istore 20
    //   570: iload 20
    //   572: ifeq -61 -> 511
    //   575: aload 7
    //   577: invokeinterface 180 1 0
    //   582: astore 10
    //   584: aload 7
    //   586: invokeinterface 192 1 0
    //   591: astore 7
    //   593: aload_3
    //   594: aload 10
    //   596: aload 7
    //   598: invokevirtual 193	java/util/LinkedHashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   601: pop
    //   602: goto -91 -> 511
    //   605: aload_3
    //   606: checkcast 36	java/util/Map
    //   609: astore_3
    //   610: new 33	java/util/LinkedHashMap
    //   613: astore 4
    //   615: aload_3
    //   616: invokeinterface 197 1 0
    //   621: invokestatic 152	c/a/ag:a	(I)I
    //   624: istore 15
    //   626: aload 4
    //   628: iload 15
    //   630: invokespecial 162	java/util/LinkedHashMap:<init>	(I)V
    //   633: aload 4
    //   635: checkcast 36	java/util/Map
    //   638: astore 4
    //   640: aload_3
    //   641: invokeinterface 172 1 0
    //   646: checkcast 31	java/lang/Iterable
    //   649: invokeinterface 40 1 0
    //   654: astore_3
    //   655: aload_3
    //   656: invokeinterface 46 1 0
    //   661: istore 15
    //   663: iload 15
    //   665: ifeq +72 -> 737
    //   668: aload_3
    //   669: invokeinterface 50 1 0
    //   674: checkcast 177	java/util/Map$Entry
    //   677: astore 7
    //   679: aload 7
    //   681: invokeinterface 180 1 0
    //   686: checkcast 182	java/lang/Number
    //   689: invokevirtual 186	java/lang/Number:longValue	()J
    //   692: lstore 18
    //   694: lload 18
    //   696: invokestatic 61	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   699: astore 10
    //   701: aload 6
    //   703: aload 10
    //   705: invokestatic 200	c/a/ag:b	(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
    //   708: checkcast 164	com/truecaller/messaging/data/types/Conversation
    //   711: astore 10
    //   713: aload 7
    //   715: invokeinterface 192 1 0
    //   720: astore 7
    //   722: aload 4
    //   724: aload 10
    //   726: aload 7
    //   728: invokeinterface 72 3 0
    //   733: pop
    //   734: goto -79 -> 655
    //   737: aload 4
    //   739: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	740	0	this	y
    //   0	740	1	paramList	java.util.List
    //   1	236	2	localy	y
    //   3	419	3	localObject1	Object
    //   465	1	3	localObject2	Object
    //   469	14	3	localObject3	Object
    //   492	177	3	localObject4	Object
    //   18	720	4	localObject5	Object
    //   45	3	5	bool1	boolean
    //   208	167	5	c	char
    //   58	644	6	localObject6	Object
    //   62	665	7	localObject7	Object
    //   72	3	8	l1	long
    //   90	635	10	localObject8	Object
    //   143	183	11	localObject9	Object
    //   148	78	12	localUri	android.net.Uri
    //   171	58	13	localObject10	Object
    //   182	11	14	i	int
    //   270	251	15	bool2	boolean
    //   624	5	15	j	int
    //   661	3	15	bool3	boolean
    //   298	3	16	bool4	boolean
    //   366	23	17	k	int
    //   441	254	18	l2	long
    //   568	3	20	bool5	boolean
    // Exception table:
    //   from	to	target	type
    //   473	475	465	finally
    //   275	278	469	finally
    //   280	285	469	finally
    //   285	290	469	finally
    //   292	298	469	finally
    //   308	312	469	finally
    //   314	321	469	finally
    //   325	333	469	finally
    //   336	341	469	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */