package com.truecaller.messaging.data;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.sqlite.SQLiteFullException;
import android.net.Uri;
import android.os.Environment;
import android.os.RemoteException;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import com.google.common.collect.Sets;
import com.truecaller.backup.bk;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.content.TruecallerContract.ab;
import com.truecaller.content.TruecallerContract.an;
import com.truecaller.content.TruecallerContract.g;
import com.truecaller.content.TruecallerContract.h;
import com.truecaller.content.TruecallerContract.i;
import com.truecaller.content.TruecallerContract.z;
import com.truecaller.filters.p;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.messaging.data.a.j;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.AudioEntity;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.Conversation.a;
import com.truecaller.messaging.data.types.Draft.a;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.ImageEntity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TextEntity;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.data.types.VideoEntity;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.NullTransportInfo;
import com.truecaller.messaging.transport.ad;
import com.truecaller.messaging.transport.i;
import com.truecaller.messaging.transport.im.as;
import com.truecaller.messaging.transport.l;
import com.truecaller.messaging.transport.l.a;
import com.truecaller.messaging.transport.l.b;
import com.truecaller.messaging.transport.m;
import com.truecaller.tracking.events.w.a;
import com.truecaller.util.ch;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.inject.Provider;
import org.c.a.a.a.k;

final class v
  implements t
{
  static final org.a.a.b a;
  private static final ContentProviderResult[] b = new ContentProviderResult[0];
  private final com.truecaller.messaging.categorizer.d A;
  private final al B;
  private final dagger.a C;
  private final x D;
  private final ContentResolver c;
  private final File d;
  private File[] e;
  private final dagger.a f;
  private final dagger.a g;
  private final Provider h;
  private final Provider i;
  private final com.truecaller.androidactors.f j;
  private final h k;
  private final c l;
  private final p m;
  private final ae n;
  private final com.truecaller.util.al o;
  private final ch p;
  private final com.truecaller.androidactors.f q;
  private final com.truecaller.messaging.c.a r;
  private v.d s = null;
  private final com.truecaller.utils.s t;
  private final o u;
  private final com.truecaller.androidactors.f v;
  private final ab w;
  private final com.truecaller.featuretoggles.e x;
  private final bk y;
  private final com.truecaller.messaging.categorizer.b z;
  
  static
  {
    org.a.a.b localb = new org/a/a/b;
    org.a.a.f localf = org.a.a.f.a;
    localb.<init>(localf);
    a = localb;
  }
  
  v(ContentResolver paramContentResolver, File paramFile, File[] paramArrayOfFile, h paramh, c paramc, o paramo, com.truecaller.androidactors.f paramf1, dagger.a parama1, dagger.a parama2, com.truecaller.androidactors.f paramf2, Provider paramProvider1, Provider paramProvider2, ae paramae, p paramp, com.truecaller.util.al paramal, ch paramch, com.truecaller.androidactors.f paramf3, com.truecaller.utils.s params, com.truecaller.messaging.c.a parama, ab paramab, com.truecaller.featuretoggles.e parame, bk parambk, com.truecaller.messaging.categorizer.b paramb, com.truecaller.messaging.categorizer.d paramd, al paramal1, dagger.a parama3, x paramx)
  {
    Object localObject = paramContentResolver;
    c = paramContentResolver;
    localObject = new java/io/File;
    ((File)localObject).<init>(paramFile, "msg_media");
    d = ((File)localObject);
    localObject = paramArrayOfFile;
    e = paramArrayOfFile;
    localObject = paramh;
    k = paramh;
    localObject = parama1;
    f = parama1;
    localObject = parama2;
    g = parama2;
    localObject = paramc;
    l = paramc;
    localObject = paramo;
    u = paramo;
    localObject = paramf1;
    v = paramf1;
    localObject = paramf2;
    j = paramf2;
    localObject = paramProvider1;
    i = paramProvider1;
    localObject = paramProvider2;
    h = paramProvider2;
    localObject = paramae;
    n = paramae;
    localObject = paramp;
    m = paramp;
    localObject = paramal;
    o = paramal;
    localObject = paramch;
    p = paramch;
    localObject = paramf3;
    q = paramf3;
    localObject = params;
    t = params;
    localObject = parama;
    r = parama;
    localObject = paramab;
    w = paramab;
    localObject = parame;
    x = parame;
    localObject = parambk;
    y = parambk;
    localObject = paramb;
    z = paramb;
    localObject = paramd;
    A = paramd;
    localObject = paramal1;
    B = paramal1;
    localObject = parama3;
    C = parama3;
    localObject = paramx;
    D = paramx;
  }
  
  private int a(t.a parama, com.truecaller.utils.q paramq, Iterable paramIterable)
  {
    Object localObject1 = this;
    t.a locala = parama;
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    int i1 = 1;
    Object localObject4 = new String[i1];
    Object localObject5 = new java/lang/StringBuilder;
    ((StringBuilder)localObject5).<init>("Staring sync batch ");
    ((StringBuilder)localObject5).append(parama);
    Object localObject6 = " with messages limit 40";
    ((StringBuilder)localObject5).append((String)localObject6);
    localObject5 = ((StringBuilder)localObject5).toString();
    localObject4[0] = localObject5;
    Object localObject7 = new android/util/SparseBooleanArray;
    ((SparseBooleanArray)localObject7).<init>();
    Iterator localIterator = paramIterable.iterator();
    int i2 = 0;
    int i3 = 0;
    Object localObject11;
    long l3;
    Object localObject14;
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localObject4 = (l)localIterator.next();
      int i5 = ((l)localObject4).f();
      Object localObject8 = parama.c();
      localObject6 = locala.a(i5);
      if (localObject6 == null)
      {
        localObject6 = a;
        localObject9 = localObject6;
      }
      else
      {
        localObject9 = localObject6;
      }
      boolean bool3 = ((org.a.a.b)localObject9).d((org.a.a.x)localObject8);
      if (!bool3)
      {
        int i8 = 3;
        try
        {
          Object localObject10 = new String[i8];
          localObject11 = String.valueOf(i5);
          localObject10[0] = localObject11;
          long l1 = a;
          localObject5 = String.valueOf(l1);
          localObject10[i1] = localObject5;
          l1 = a;
          localObject5 = String.valueOf(l1);
          int i6 = 2;
          localObject10[i6] = localObject5;
          localObject5 = c;
          localObject6 = TruecallerContract.b;
          localObject11 = "msg/msg_messages_by_transport_view";
          Uri localUri = Uri.withAppendedPath((Uri)localObject6, (String)localObject11);
          String[] arrayOfString = ai.a;
          String str1 = "transport=? AND date>=? AND date <=?";
          String str2 = "date DESC, raw_id DESC";
          localObject12 = localObject5;
          localObject5 = ((ContentResolver)localObject5).query(localUri, arrayOfString, str1, (String[])localObject10, str2);
          localObject6 = l;
          localObject12 = ((c)localObject6).d((Cursor)localObject5);
          if (localObject12 == null) {}
          try
          {
            locala.a(i5, (org.a.a.b)localObject8);
            com.truecaller.util.q.a((Cursor)localObject12);
            localObject1 = localObject7;
            i8 = 1;
          }
          finally
          {
            int i11;
            boolean bool5;
            boolean bool6;
            int i12;
            Object localObject13;
            long l2;
            int i7;
            long l4;
            boolean bool7;
            long l5;
            localObject14 = localObject12;
            break label705;
          }
          i10 = localArrayList.size();
          localObject5 = parama.a();
          localObject6 = parama.b();
          i11 = 40;
          bool5 = parama.e();
          bool6 = parama.f();
          localObject10 = localObject12;
          localObject11 = localObject8;
          i12 = i10;
          i10 = i11;
          localObject8 = localArrayList;
          localObject13 = localObject7;
          l2 = ((l)localObject4).a((com.truecaller.messaging.transport.f)localObject5, (i)localObject6, (r)localObject12, (org.a.a.b)localObject11, (org.a.a.b)localObject9, i11, localArrayList, paramq, bool5, bool6, localHashSet);
          i7 = l.b.b(l2);
          if (i7 != 0)
          {
            localObject10 = localObject11;
            i7 = i5;
            locala.a(i5, (org.a.a.b)localObject11);
            i4 = localArrayList.size();
            localObject1 = localArrayList.subList(i12, i4);
            ((List)localObject1).clear();
            localObject1 = localObject7;
            i8 = 1;
          }
          else
          {
            localObject10 = localObject11;
            i7 = i5;
            l3 = 0x8000000000000000 & l2;
            l4 = 0L;
            bool7 = l3 < l4;
            if (bool7)
            {
              bool7 = true;
            }
            else
            {
              bool7 = false;
              localObject1 = null;
            }
            if (bool7)
            {
              locala.a(i7, (org.a.a.b)localObject10);
              localObject1 = localObject13;
              i8 = 1;
              ((SparseBooleanArray)localObject13).put(i7, i8);
              i2 = 1;
            }
            else
            {
              localObject1 = localObject13;
              i9 = 1;
              localObject11 = new org/a/a/b;
              l5 = 4611686018427387903L;
              l2 &= l5;
              ((org.a.a.b)localObject11).<init>(l2);
              locala.a(i7, (org.a.a.b)localObject11);
              ((SparseBooleanArray)localObject13).put(i7, i9);
              i2 = 1;
              i3 = 1;
            }
          }
          com.truecaller.util.q.a((Cursor)localObject12);
          localObject7 = localObject1;
          localObject1 = this;
          i1 = 1;
          continue;
        }
        finally
        {
          Object localObject12;
          int i10 = 0;
          localObject14 = null;
        }
        label705:
        com.truecaller.util.q.a((Cursor)localObject14);
        throw ((Throwable)localObject3);
      }
      else
      {
        localObject1 = localObject7;
        i9 = 1;
      }
      localObject7 = localObject1;
      localObject1 = this;
      i1 = 1;
    }
    localObject1 = localObject7;
    int i9 = 1;
    if (i2 == 0) {
      return 2;
    }
    int i4 = 2;
    localObject5 = this;
    localObject6 = a(localArrayList);
    boolean bool8 = localArrayList.isEmpty();
    int i13;
    if (!bool8)
    {
      i13 = localObject6.length;
      if (i13 == 0) {
        return i4;
      }
    }
    localObject4 = x.w();
    boolean bool2 = ((com.truecaller.featuretoggles.b)localObject4).a();
    if (bool2)
    {
      l3 = -1;
      localObject4 = Long.valueOf(l3);
      bool2 = localHashSet.remove(localObject4);
      if (bool2)
      {
        localObject4 = a((ContentProviderResult[])localObject6);
        localHashSet.addAll((Collection)localObject4);
      }
      bool2 = localHashSet.isEmpty();
      if (!bool2)
      {
        localObject4 = A.a(localHashSet);
        ((v)localObject5).a((ArrayList)localObject4);
      }
    }
    localObject4 = paramIterable.iterator();
    for (;;)
    {
      boolean bool4 = ((Iterator)localObject4).hasNext();
      if (!bool4) {
        break;
      }
      localObject6 = (l)((Iterator)localObject4).next();
      i13 = ((l)localObject6).f();
      localObject9 = null;
      boolean bool9 = ((SparseBooleanArray)localObject1).get(i13, false);
      if (bool9)
      {
        localObject11 = ((l)localObject6).d();
        localObject14 = parama.d();
        bool9 = ((org.a.a.b)localObject11).c((org.a.a.x)localObject14);
        if (bool9)
        {
          localObject11 = parama.d();
          ((l)localObject6).a((org.a.a.b)localObject11);
        }
      }
    }
    Object localObject9 = null;
    if (i3 != 0) {
      return 0;
    }
    return i9;
  }
  
  private SparseBooleanArray a(String paramString, boolean paramBoolean)
  {
    v localv = this;
    Object localObject1 = new android/util/SparseArray;
    int i1 = 5;
    ((SparseArray)localObject1).<init>(i1);
    Object localObject3 = new android/util/SparseArray;
    ((SparseArray)localObject3).<init>(i1);
    Object localObject4 = new android/util/SparseArray;
    ((SparseArray)localObject4).<init>(i1);
    SparseBooleanArray localSparseBooleanArray = new android/util/SparseBooleanArray;
    localSparseBooleanArray.<init>(i1);
    Object localObject6;
    Object localObject7;
    if (!paramBoolean)
    {
      localObject5 = new java/lang/StringBuilder;
      ((StringBuilder)localObject5).<init>("(");
      localObject6 = paramString;
      ((StringBuilder)localObject5).append(paramString);
      localObject6 = ") AND (read = 0)";
      ((StringBuilder)localObject5).append((String)localObject6);
      localObject5 = ((StringBuilder)localObject5).toString();
      localObject7 = localObject5;
    }
    else
    {
      localObject6 = paramString;
      localObject7 = paramString;
    }
    i1 = 0;
    Object localObject5 = null;
    try
    {
      localObject6 = c;
      Object localObject8 = TruecallerContract.h.a();
      int i2 = 0;
      boolean bool1 = false;
      Object localObject9 = null;
      boolean bool3 = false;
      Object localObject10 = null;
      localObject6 = ((ContentResolver)localObject6).query((Uri)localObject8, null, (String)localObject7, null, null);
      int i5 = 0;
      localObject8 = null;
      i2 = 1;
      int i3;
      if (localObject6 != null)
      {
        localObject7 = l;
        localObject5 = ((c)localObject7).e((Cursor)localObject6);
        for (;;)
        {
          boolean bool4 = ((com.truecaller.messaging.data.a.s)localObject5).moveToNext();
          if (!bool4) {
            break;
          }
          i6 = ((com.truecaller.messaging.data.a.s)localObject5).d();
          Object localObject11 = ((com.truecaller.messaging.data.a.s)localObject5).e();
          localObject7 = f;
          localObject7 = ((dagger.a)localObject7).get();
          localObject7 = (m)localObject7;
          localObject7 = ((m)localObject7).b(i6);
          if (localObject7 == null)
          {
            localObject1 = new String[i2];
            localObject3 = "Unsupported transport type: ";
            localObject4 = String.valueOf(i6);
            localObject3 = ((String)localObject3).concat((String)localObject4);
            localObject1[0] = localObject3;
            AssertionUtil.OnlyInDebug.fail((String[])localObject1);
            return localSparseBooleanArray;
          }
          localObject9 = ((SparseArray)localObject1).get(i6);
          localObject9 = (ad)localObject9;
          Object localObject12;
          if (localObject9 == null)
          {
            localObject9 = ((l)localObject7).b();
            localObject10 = org.a.a.b.ay_();
            ((SparseArray)localObject3).put(i6, localObject10);
            localObject10 = new java/util/ArrayList;
            ((ArrayList)localObject10).<init>();
            ((SparseArray)localObject4).put(i6, localObject10);
            ((SparseArray)localObject1).put(i6, localObject9);
            localObject12 = localObject9;
          }
          else
          {
            localObject12 = localObject9;
          }
          long l1 = ((com.truecaller.messaging.data.a.s)localObject5).a();
          long l2 = ((com.truecaller.messaging.data.a.s)localObject5).b();
          localObject9 = localObject11;
          localObject8 = localObject11;
          localObject11 = localObject12;
          boolean bool5 = ((l)localObject7).a((TransportInfo)localObject9, l1, l2, (ad)localObject12);
          if (bool5)
          {
            localObject7 = new org/a/a/b;
            long l3 = ((com.truecaller.messaging.data.a.s)localObject5).b();
            ((org.a.a.b)localObject7).<init>(l3);
            localObject9 = ((SparseArray)localObject3).get(i6);
            localObject9 = (org.a.a.b)localObject9;
            bool1 = ((org.a.a.b)localObject9).b((org.a.a.x)localObject7);
            if (bool1) {
              ((SparseArray)localObject3).put(i6, localObject7);
            }
            i3 = ((com.truecaller.messaging.data.a.s)localObject5).c() & i2;
            if (i3 == 0)
            {
              localObject6 = ((SparseArray)localObject4).get(i6);
              localObject6 = (List)localObject6;
              localObject8 = ((TransportInfo)localObject8).a((org.a.a.b)localObject7);
              ((List)localObject6).add(localObject8);
            }
          }
          i5 = 0;
          localObject8 = null;
        }
      }
      com.truecaller.util.q.a((Cursor)localObject5);
      localObject5 = new java/util/HashSet;
      ((HashSet)localObject5).<init>();
      int i6 = ((SparseArray)localObject1).size();
      i5 = 0;
      localObject8 = null;
      while (i5 < i6)
      {
        localObject7 = (m)f.get();
        i3 = ((SparseArray)localObject1).keyAt(i5);
        localObject7 = ((m)localObject7).b(i3);
        localObject9 = new String[] { "Only known transports should by part of transactions" };
        AssertionUtil.AlwaysFatal.isNotNull(localObject7, (String[])localObject9);
        localObject9 = (ad)((SparseArray)localObject1).valueAt(i5);
        bool3 = ((l)localObject7).b((ad)localObject9);
        if (bool3)
        {
          bool2 = ((l)localObject7).a((ad)localObject9);
          if (bool2)
          {
            bool2 = true;
            break label709;
          }
        }
        boolean bool2 = false;
        localObject9 = null;
        label709:
        if (bool2)
        {
          int i4 = ((l)localObject7).f();
          localObject10 = (org.a.a.b)((SparseArray)localObject3).get(i4);
          ((l)localObject7).a((org.a.a.b)localObject10);
          localObject10 = (List)((SparseArray)localObject4).valueAt(i5);
          localv.a((List)localObject10);
          ((Set)localObject5).add(localObject7);
        }
        int i7 = ((l)localObject7).f();
        localSparseBooleanArray.put(i7, bool2);
        i5 += 1;
      }
      boolean bool6 = ((Set)localObject5).isEmpty();
      if (!bool6) {
        localv.b(i2, (Iterable)localObject5);
      }
      return localSparseBooleanArray;
    }
    finally
    {
      com.truecaller.util.q.a((Cursor)localObject5);
    }
  }
  
  private Message a(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    boolean bool1 = paramMessage.b();
    Object localObject2;
    if (bool1)
    {
      l1 = a;
      localObject1 = f(l1);
      localObject2 = localObject1;
    }
    else
    {
      localObject2 = null;
    }
    Object localObject1 = n;
    int i1 = 0;
    List localList = a((Entity[])localObject1, false);
    long l1 = a;
    int i2 = q;
    long l2 = x;
    Object localObject3 = paramArrayOfParticipant;
    Object localObject4 = localList;
    Object localObject5 = localArrayList;
    paramArrayOfParticipant = a(l1, paramArrayOfParticipant, localList, localArrayList, i2, l2);
    localObject1 = a(localArrayList);
    int i3 = localObject1.length;
    if (i3 == 0)
    {
      b(localList);
      new String[1][0] = "Operation failed. Draft was not saved";
      return null;
    }
    Object localObject6 = n;
    int i4 = localObject6.length;
    while (i1 < i4)
    {
      localObject4 = localObject6[i1];
      boolean bool3 = ((Entity)localObject4).a();
      if (!bool3)
      {
        localObject4 = (BinaryEntity)localObject4;
        bool3 = c;
        if (bool3)
        {
          localObject5 = p;
          ((ch)localObject5).a((BinaryEntity)localObject4);
        }
        else
        {
          localObject5 = b;
          bool3 = a((Uri)localObject5);
          if ((bool3) && (localObject2 != null))
          {
            localObject5 = new java/io/File;
            localObject4 = b.getPath();
            ((File)localObject5).<init>((String)localObject4);
            ((Set)localObject2).remove(localObject5);
          }
        }
      }
      i1 += 1;
    }
    if (localObject2 != null)
    {
      localObject6 = ((Set)localObject2).iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject6).hasNext();
        if (!bool2) {
          break;
        }
        localObject3 = (File)((Iterator)localObject6).next();
        boolean bool4 = ((File)localObject3).exists();
        if (bool4) {
          ((File)localObject3).delete();
        }
      }
    }
    i3 = b;
    long l3 = ContentUris.parseId(uri);
    int i5 = c;
    long l4 = ContentUris.parseId(uri);
    paramMessage = paramMessage.m();
    a = l4;
    b = l3;
    return paramMessage.a().a(localList).b();
  }
  
  private static v.a a(long paramLong1, Participant[] paramArrayOfParticipant, List paramList, ArrayList paramArrayList, int paramInt, long paramLong2)
  {
    Participant localParticipant = paramArrayOfParticipant[0];
    int i1 = b.a(paramArrayList, localParticipant);
    int i2 = 1;
    int i3 = 1;
    for (;;)
    {
      int i4 = paramArrayOfParticipant.length;
      if (i3 >= i4) {
        break;
      }
      localObject1 = paramArrayOfParticipant[i3];
      b.a(paramArrayList, (Participant)localObject1);
      i3 += 1;
    }
    paramArrayOfParticipant = Sets.newHashSet(paramArrayOfParticipant);
    int i5 = b.a(paramArrayList, paramArrayOfParticipant);
    ContentProviderOperation.Builder localBuilder1 = ContentProviderOperation.newInsert(TruecallerContract.aa.a());
    long l1 = -1;
    boolean bool1 = paramLong1 < l1;
    if (bool1)
    {
      localObject1 = "_id";
      localObject2 = Long.valueOf(paramLong1);
      localBuilder1.withValue((String)localObject1, localObject2);
    }
    localBuilder1.withValueBackReference("participant_id", i1);
    localBuilder1.withValueBackReference("conversation_id", i5);
    l1 = System.currentTimeMillis();
    Object localObject3 = Long.valueOf(l1);
    localBuilder1.withValue("date", localObject3);
    int i6 = 3;
    Object localObject1 = Integer.valueOf(i6);
    localBuilder1.withValue("status", localObject1);
    localObject1 = Integer.valueOf(i2);
    localBuilder1.withValue("seen", localObject1);
    localObject1 = Integer.valueOf(i2);
    localBuilder1.withValue("read", localObject1);
    localObject1 = Integer.valueOf(0);
    localBuilder1.withValue("locked", localObject1);
    localObject3 = Integer.valueOf(i6);
    localBuilder1.withValue("transport", localObject3);
    localObject3 = Integer.valueOf(paramInt);
    localBuilder1.withValue("category", localObject3);
    Object localObject2 = "reply_to_msg_id";
    localObject3 = Long.valueOf(paramLong2);
    localBuilder1.withValue((String)localObject2, localObject3);
    int i7 = paramArrayList.size();
    localObject3 = localBuilder1.build();
    paramArrayList.add(localObject3);
    localObject3 = ContentProviderOperation.newDelete(TruecallerContract.z.a());
    Object localObject4 = new String[i2];
    ContentProviderOperation.Builder localBuilder2 = ((ContentProviderOperation.Builder)localObject3).withSelection("message_id=?", (String[])localObject4);
    localBuilder2.withSelectionBackReference(0, i7);
    localObject3 = ((ContentProviderOperation.Builder)localObject3).build();
    paramArrayList.add(localObject3);
    localObject3 = paramList.iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject3).hasNext();
      if (!bool2) {
        break;
      }
      paramList = (Entity)((Iterator)localObject3).next();
      localBuilder2 = ContentProviderOperation.newInsert(TruecallerContract.z.a());
      localObject4 = "type";
      Object localObject5 = j;
      localBuilder2.withValue((String)localObject4, localObject5);
      boolean bool3 = paramList.a();
      if (bool3)
      {
        localObject4 = "content";
        paramList = a;
        localBuilder2.withValue((String)localObject4, paramList);
      }
      else
      {
        localObject5 = paramList;
        localObject5 = (BinaryEntity)paramList;
        String str = b.toString();
        localBuilder2.withValue("content", str);
        localObject4 = "size";
        long l2 = d;
        localObject5 = Long.valueOf(l2);
        localBuilder2.withValue((String)localObject4, localObject5);
        bool3 = paramList.b();
        int i9;
        int i8;
        if (bool3)
        {
          paramList = (ImageEntity)paramList;
          i9 = a;
          localObject5 = Integer.valueOf(i9);
          localBuilder2.withValue("width", localObject5);
          localObject4 = "height";
          i8 = k;
          paramList = Integer.valueOf(i8);
          localBuilder2.withValue((String)localObject4, paramList);
        }
        else
        {
          bool3 = paramList.c();
          if (bool3)
          {
            paramList = (VideoEntity)paramList;
            localObject5 = Integer.valueOf(a);
            localBuilder2.withValue("width", localObject5);
            localObject5 = Integer.valueOf(k);
            localBuilder2.withValue("height", localObject5);
            i9 = l;
            localObject5 = Integer.valueOf(i9);
            localBuilder2.withValue("duration", localObject5);
            localObject4 = "thumbnail";
            paramList = m.toString();
            localBuilder2.withValue((String)localObject4, paramList);
          }
          else
          {
            bool3 = paramList.e();
            if (bool3)
            {
              localObject4 = "duration";
              i8 = a;
              paramList = Integer.valueOf(i8);
              localBuilder2.withValue((String)localObject4, paramList);
            }
          }
        }
      }
      localBuilder2.withValueBackReference("message_id", i7);
      paramList = localBuilder2.build();
      paramArrayList.add(paramList);
    }
    localObject3 = new com/truecaller/messaging/data/v$a;
    ((v.a)localObject3).<init>(i1, i5, i7, (byte)0);
    return (v.a)localObject3;
  }
  
  private v.d a(org.a.a.b paramb1, org.a.a.b paramb2, int paramInt)
  {
    v.d locald = s;
    if (locald != null)
    {
      paramb1 = locald.a(paramb1, paramb2, paramInt);
      s = paramb1;
      return null;
    }
    locald = new com/truecaller/messaging/data/v$d;
    locald.<init>(paramb1, paramb2, paramInt);
    s = locald;
    return s;
  }
  
  /* Error */
  private List a(Entity[] paramArrayOfEntity, boolean paramBoolean)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore_3
    //   2: aload_1
    //   3: astore 4
    //   5: new 146	java/util/ArrayList
    //   8: astore 5
    //   10: aload 5
    //   12: invokespecial 147	java/util/ArrayList:<init>	()V
    //   15: aload_0
    //   16: getfield 94	com/truecaller/messaging/data/v:d	Ljava/io/File;
    //   19: astore 6
    //   21: aload 6
    //   23: invokevirtual 521	java/io/File:exists	()Z
    //   26: istore 7
    //   28: iload 7
    //   30: ifne +33 -> 63
    //   33: aload_0
    //   34: getfield 94	com/truecaller/messaging/data/v:d	Ljava/io/File;
    //   37: astore 6
    //   39: aload 6
    //   41: invokevirtual 702	java/io/File:mkdirs	()Z
    //   44: istore 7
    //   46: iload 7
    //   48: ifne +15 -> 63
    //   51: iconst_1
    //   52: anewarray 153	java/lang/String
    //   55: iconst_0
    //   56: ldc_w 704
    //   59: aastore
    //   60: aload 5
    //   62: areturn
    //   63: aload 4
    //   65: arraylength
    //   66: istore 8
    //   68: iconst_0
    //   69: istore 9
    //   71: iload 9
    //   73: iload 8
    //   75: if_icmpge +770 -> 845
    //   78: aload 4
    //   80: iload 9
    //   82: aaload
    //   83: astore 6
    //   85: aload 6
    //   87: invokevirtual 499	com/truecaller/messaging/data/types/Entity:a	()Z
    //   90: istore 10
    //   92: iload 10
    //   94: ifne +732 -> 826
    //   97: aload 6
    //   99: astore 11
    //   101: aload 6
    //   103: checkcast 501	com/truecaller/messaging/data/types/BinaryEntity
    //   106: astore 11
    //   108: aload 11
    //   110: getfield 504	com/truecaller/messaging/data/types/BinaryEntity:c	Z
    //   113: istore 12
    //   115: iload 12
    //   117: ifne +27 -> 144
    //   120: aload 11
    //   122: getfield 510	com/truecaller/messaging/data/types/BinaryEntity:b	Landroid/net/Uri;
    //   125: astore 13
    //   127: aload_3
    //   128: aload 13
    //   130: invokespecial 513	com/truecaller/messaging/data/v:a	(Landroid/net/Uri;)Z
    //   133: istore 12
    //   135: iload 12
    //   137: ifeq +7 -> 144
    //   140: iload_2
    //   141: ifeq +685 -> 826
    //   144: aload 11
    //   146: getfield 657	com/truecaller/messaging/data/types/BinaryEntity:d	J
    //   149: lstore 14
    //   151: invokestatic 710	android/os/Environment:getDataDirectory	()Ljava/io/File;
    //   154: invokevirtual 711	java/io/File:getPath	()Ljava/lang/String;
    //   157: astore 16
    //   159: aload 16
    //   161: invokestatic 716	com/truecaller/util/ao:a	(Ljava/lang/String;)J
    //   164: lstore 17
    //   166: lload 14
    //   168: lload 17
    //   170: lcmp
    //   171: istore 19
    //   173: iload 19
    //   175: ifge +9 -> 184
    //   178: iconst_1
    //   179: istore 12
    //   181: goto +9 -> 190
    //   184: iconst_0
    //   185: istore 12
    //   187: aconst_null
    //   188: astore 13
    //   190: aconst_null
    //   191: astore 20
    //   193: iload 12
    //   195: ifne +54 -> 249
    //   198: aload_0
    //   199: invokespecial 718	com/truecaller/messaging/data/v:c	()Ljava/io/File;
    //   202: astore 16
    //   204: aload 16
    //   206: ifnull +27 -> 233
    //   209: aload 16
    //   211: invokevirtual 521	java/io/File:exists	()Z
    //   214: istore 21
    //   216: iload 21
    //   218: ifne +34 -> 252
    //   221: aload 16
    //   223: invokevirtual 702	java/io/File:mkdirs	()Z
    //   226: istore 21
    //   228: iload 21
    //   230: ifne +22 -> 252
    //   233: ldc_w 720
    //   236: astore 6
    //   238: iconst_1
    //   239: anewarray 153	java/lang/String
    //   242: iconst_0
    //   243: aload 6
    //   245: aastore
    //   246: goto +590 -> 836
    //   249: aconst_null
    //   250: astore 16
    //   252: new 87	java/io/File
    //   255: astore 22
    //   257: iload 12
    //   259: ifeq +9 -> 268
    //   262: aload_3
    //   263: getfield 94	com/truecaller/messaging/data/v:d	Ljava/io/File;
    //   266: astore 16
    //   268: bipush 45
    //   270: istore 12
    //   272: iload 12
    //   274: invokestatic 725	org/c/a/a/a/i:b	(I)Ljava/lang/String;
    //   277: astore 13
    //   279: aload 22
    //   281: aload 16
    //   283: aload 13
    //   285: invokespecial 92	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   288: aload 22
    //   290: invokevirtual 728	java/io/File:createNewFile	()Z
    //   293: istore 12
    //   295: iload 12
    //   297: ifne +39 -> 336
    //   300: ldc_w 730
    //   303: astore 6
    //   305: iconst_1
    //   306: anewarray 153	java/lang/String
    //   309: iconst_0
    //   310: aload 6
    //   312: aastore
    //   313: aconst_null
    //   314: invokestatic 733	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   317: aconst_null
    //   318: invokestatic 733	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   321: aload 22
    //   323: invokevirtual 521	java/io/File:exists	()Z
    //   326: istore 7
    //   328: iload 7
    //   330: ifeq +506 -> 836
    //   333: goto +443 -> 776
    //   336: new 735	java/io/FileOutputStream
    //   339: astore 13
    //   341: aload 13
    //   343: aload 22
    //   345: invokespecial 738	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   348: aload_3
    //   349: getfield 85	com/truecaller/messaging/data/v:c	Landroid/content/ContentResolver;
    //   352: astore 16
    //   354: aload 11
    //   356: getfield 510	com/truecaller/messaging/data/types/BinaryEntity:b	Landroid/net/Uri;
    //   359: astore 11
    //   361: aload 16
    //   363: aload 11
    //   365: invokevirtual 742	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   368: astore 20
    //   370: aload 20
    //   372: ifnonnull +41 -> 413
    //   375: ldc_w 744
    //   378: astore 6
    //   380: iconst_1
    //   381: anewarray 153	java/lang/String
    //   384: iconst_0
    //   385: aload 6
    //   387: aastore
    //   388: aload 13
    //   390: invokestatic 733	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   393: aload 20
    //   395: invokestatic 733	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   398: aload 22
    //   400: invokevirtual 521	java/io/File:exists	()Z
    //   403: istore 7
    //   405: iload 7
    //   407: ifeq +429 -> 836
    //   410: goto +366 -> 776
    //   413: aload 20
    //   415: aload 13
    //   417: invokestatic 749	com/truecaller/utils/extensions/m:a	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   420: lstore 23
    //   422: aload 22
    //   424: invokestatic 753	android/net/Uri:fromFile	(Ljava/io/File;)Landroid/net/Uri;
    //   427: astore 25
    //   429: aload 6
    //   431: invokevirtual 658	com/truecaller/messaging/data/types/Entity:b	()Z
    //   434: istore 10
    //   436: iload 10
    //   438: ifeq +42 -> 480
    //   441: aload 6
    //   443: astore 11
    //   445: aload 6
    //   447: checkcast 662	com/truecaller/messaging/data/types/ImageEntity
    //   450: astore 11
    //   452: aload 11
    //   454: getfield 664	com/truecaller/messaging/data/types/ImageEntity:a	I
    //   457: istore 10
    //   459: aload 6
    //   461: astore 26
    //   463: aload 6
    //   465: checkcast 662	com/truecaller/messaging/data/types/ImageEntity
    //   468: astore 26
    //   470: aload 26
    //   472: getfield 668	com/truecaller/messaging/data/types/ImageEntity:k	I
    //   475: istore 19
    //   477: goto +9 -> 486
    //   480: iconst_m1
    //   481: istore 10
    //   483: iconst_m1
    //   484: istore 19
    //   486: getstatic 756	android/net/Uri:EMPTY	Landroid/net/Uri;
    //   489: astore 27
    //   491: aload 6
    //   493: invokevirtual 670	com/truecaller/messaging/data/types/Entity:c	()Z
    //   496: istore 28
    //   498: iload 28
    //   500: ifeq +86 -> 586
    //   503: aload 6
    //   505: astore 11
    //   507: aload 6
    //   509: checkcast 672	com/truecaller/messaging/data/types/VideoEntity
    //   512: astore 11
    //   514: aload 11
    //   516: getfield 673	com/truecaller/messaging/data/types/VideoEntity:a	I
    //   519: istore 10
    //   521: aload 6
    //   523: astore 26
    //   525: aload 6
    //   527: checkcast 672	com/truecaller/messaging/data/types/VideoEntity
    //   530: astore 26
    //   532: aload 26
    //   534: getfield 674	com/truecaller/messaging/data/types/VideoEntity:k	I
    //   537: istore 19
    //   539: aload 6
    //   541: astore 27
    //   543: aload 6
    //   545: checkcast 672	com/truecaller/messaging/data/types/VideoEntity
    //   548: astore 27
    //   550: aload 27
    //   552: getfield 678	com/truecaller/messaging/data/types/VideoEntity:l	I
    //   555: istore 29
    //   557: aload 6
    //   559: astore 30
    //   561: aload 6
    //   563: checkcast 672	com/truecaller/messaging/data/types/VideoEntity
    //   566: astore 30
    //   568: aload 30
    //   570: getfield 682	com/truecaller/messaging/data/types/VideoEntity:m	Landroid/net/Uri;
    //   573: astore 30
    //   575: iload 29
    //   577: istore 28
    //   579: aload 30
    //   581: astore 27
    //   583: goto +6 -> 589
    //   586: iconst_m1
    //   587: istore 28
    //   589: aload 6
    //   591: invokevirtual 683	com/truecaller/messaging/data/types/Entity:e	()Z
    //   594: istore 31
    //   596: iload 31
    //   598: ifeq +43 -> 641
    //   601: aload 6
    //   603: astore 11
    //   605: aload 6
    //   607: checkcast 685	com/truecaller/messaging/data/types/AudioEntity
    //   610: astore 11
    //   612: aload 11
    //   614: getfield 686	com/truecaller/messaging/data/types/AudioEntity:a	I
    //   617: istore 10
    //   619: getstatic 756	android/net/Uri:EMPTY	Landroid/net/Uri;
    //   622: astore 26
    //   624: iload 10
    //   626: istore 32
    //   628: aload 26
    //   630: astore 33
    //   632: iconst_m1
    //   633: istore 31
    //   635: iconst_m1
    //   636: istore 34
    //   638: goto +19 -> 657
    //   641: iload 10
    //   643: istore 31
    //   645: iload 19
    //   647: istore 34
    //   649: aload 27
    //   651: astore 33
    //   653: iload 28
    //   655: istore 32
    //   657: iconst_m1
    //   658: i2l
    //   659: lstore 35
    //   661: aload 6
    //   663: getfield 647	com/truecaller/messaging/data/types/Entity:j	Ljava/lang/String;
    //   666: astore 30
    //   668: lload 35
    //   670: aload 30
    //   672: aload 25
    //   674: iload 31
    //   676: iload 34
    //   678: iload 32
    //   680: lload 23
    //   682: aload 33
    //   684: invokestatic 759	com/truecaller/messaging/data/types/Entity:a	(JLjava/lang/String;Landroid/net/Uri;IIIJLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   687: astore 6
    //   689: aload 13
    //   691: invokestatic 733	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   694: aload 20
    //   696: invokestatic 733	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   699: goto +127 -> 826
    //   702: astore 6
    //   704: goto +91 -> 795
    //   707: astore 6
    //   709: goto +5 -> 714
    //   712: astore 6
    //   714: aload 20
    //   716: astore 11
    //   718: aload 13
    //   720: astore 20
    //   722: goto +27 -> 749
    //   725: astore 6
    //   727: iconst_0
    //   728: istore 12
    //   730: aconst_null
    //   731: astore 13
    //   733: goto +62 -> 795
    //   736: astore 6
    //   738: goto +5 -> 743
    //   741: astore 6
    //   743: iconst_0
    //   744: istore 10
    //   746: aconst_null
    //   747: astore 11
    //   749: aload 6
    //   751: invokestatic 765	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   754: aload 20
    //   756: invokestatic 733	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   759: aload 11
    //   761: invokestatic 733	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   764: aload 22
    //   766: invokevirtual 521	java/io/File:exists	()Z
    //   769: istore 7
    //   771: iload 7
    //   773: ifeq +63 -> 836
    //   776: aload 22
    //   778: invokevirtual 524	java/io/File:delete	()Z
    //   781: pop
    //   782: goto +54 -> 836
    //   785: astore 6
    //   787: aload 20
    //   789: astore 13
    //   791: aload 11
    //   793: astore 20
    //   795: aload 13
    //   797: invokestatic 733	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   800: aload 20
    //   802: invokestatic 733	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   805: aload 22
    //   807: invokevirtual 521	java/io/File:exists	()Z
    //   810: istore 37
    //   812: iload 37
    //   814: ifeq +9 -> 823
    //   817: aload 22
    //   819: invokevirtual 524	java/io/File:delete	()Z
    //   822: pop
    //   823: aload 6
    //   825: athrow
    //   826: aload 5
    //   828: aload 6
    //   830: invokeinterface 442 2 0
    //   835: pop
    //   836: iload 9
    //   838: iconst_1
    //   839: iadd
    //   840: istore 9
    //   842: goto -771 -> 71
    //   845: aload 5
    //   847: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	848	0	this	v
    //   0	848	1	paramArrayOfEntity	Entity[]
    //   0	848	2	paramBoolean	boolean
    //   1	348	3	localv	v
    //   3	76	4	arrayOfEntity	Entity[]
    //   8	838	5	localArrayList	ArrayList
    //   19	669	6	localObject1	Object
    //   702	1	6	localObject2	Object
    //   707	1	6	localSecurityException1	SecurityException
    //   712	1	6	localIOException1	java.io.IOException
    //   725	1	6	localObject3	Object
    //   736	1	6	localSecurityException2	SecurityException
    //   741	9	6	localIOException2	java.io.IOException
    //   785	44	6	localObject4	Object
    //   26	746	7	bool1	boolean
    //   66	10	8	i1	int
    //   69	772	9	i2	int
    //   90	347	10	bool2	boolean
    //   457	288	10	i3	int
    //   99	693	11	localObject5	Object
    //   113	145	12	bool3	boolean
    //   270	3	12	i4	int
    //   293	436	12	bool4	boolean
    //   125	671	13	localObject6	Object
    //   149	18	14	l1	long
    //   157	205	16	localObject7	Object
    //   164	5	17	l2	long
    //   171	3	19	bool5	boolean
    //   475	171	19	i5	int
    //   191	610	20	localObject8	Object
    //   214	15	21	bool6	boolean
    //   255	563	22	localFile	File
    //   420	261	23	l3	long
    //   427	246	25	localUri	Uri
    //   461	168	26	localObject9	Object
    //   489	161	27	localObject10	Object
    //   496	82	28	bool7	boolean
    //   587	67	28	i6	int
    //   555	21	29	i7	int
    //   559	112	30	localObject11	Object
    //   594	3	31	bool8	boolean
    //   633	42	31	i8	int
    //   626	53	32	i9	int
    //   630	53	33	localObject12	Object
    //   636	41	34	i10	int
    //   659	10	35	l4	long
    //   810	3	37	bool9	boolean
    // Exception table:
    //   from	to	target	type
    //   348	352	702	finally
    //   354	359	702	finally
    //   363	368	702	finally
    //   380	388	702	finally
    //   415	420	702	finally
    //   422	427	702	finally
    //   429	434	702	finally
    //   445	450	702	finally
    //   452	457	702	finally
    //   463	468	702	finally
    //   470	475	702	finally
    //   486	489	702	finally
    //   491	496	702	finally
    //   507	512	702	finally
    //   514	519	702	finally
    //   525	530	702	finally
    //   532	537	702	finally
    //   543	548	702	finally
    //   550	555	702	finally
    //   561	566	702	finally
    //   568	573	702	finally
    //   589	594	702	finally
    //   605	610	702	finally
    //   612	617	702	finally
    //   619	622	702	finally
    //   661	666	702	finally
    //   682	687	702	finally
    //   348	352	707	java/lang/SecurityException
    //   354	359	707	java/lang/SecurityException
    //   363	368	707	java/lang/SecurityException
    //   380	388	707	java/lang/SecurityException
    //   415	420	707	java/lang/SecurityException
    //   422	427	707	java/lang/SecurityException
    //   429	434	707	java/lang/SecurityException
    //   445	450	707	java/lang/SecurityException
    //   452	457	707	java/lang/SecurityException
    //   463	468	707	java/lang/SecurityException
    //   470	475	707	java/lang/SecurityException
    //   486	489	707	java/lang/SecurityException
    //   491	496	707	java/lang/SecurityException
    //   507	512	707	java/lang/SecurityException
    //   514	519	707	java/lang/SecurityException
    //   525	530	707	java/lang/SecurityException
    //   532	537	707	java/lang/SecurityException
    //   543	548	707	java/lang/SecurityException
    //   550	555	707	java/lang/SecurityException
    //   561	566	707	java/lang/SecurityException
    //   568	573	707	java/lang/SecurityException
    //   589	594	707	java/lang/SecurityException
    //   605	610	707	java/lang/SecurityException
    //   612	617	707	java/lang/SecurityException
    //   619	622	707	java/lang/SecurityException
    //   661	666	707	java/lang/SecurityException
    //   682	687	707	java/lang/SecurityException
    //   348	352	712	java/io/IOException
    //   354	359	712	java/io/IOException
    //   363	368	712	java/io/IOException
    //   380	388	712	java/io/IOException
    //   415	420	712	java/io/IOException
    //   422	427	712	java/io/IOException
    //   429	434	712	java/io/IOException
    //   445	450	712	java/io/IOException
    //   452	457	712	java/io/IOException
    //   463	468	712	java/io/IOException
    //   470	475	712	java/io/IOException
    //   486	489	712	java/io/IOException
    //   491	496	712	java/io/IOException
    //   507	512	712	java/io/IOException
    //   514	519	712	java/io/IOException
    //   525	530	712	java/io/IOException
    //   532	537	712	java/io/IOException
    //   543	548	712	java/io/IOException
    //   550	555	712	java/io/IOException
    //   561	566	712	java/io/IOException
    //   568	573	712	java/io/IOException
    //   589	594	712	java/io/IOException
    //   605	610	712	java/io/IOException
    //   612	617	712	java/io/IOException
    //   619	622	712	java/io/IOException
    //   661	666	712	java/io/IOException
    //   682	687	712	java/io/IOException
    //   288	293	725	finally
    //   305	313	725	finally
    //   336	339	725	finally
    //   343	348	725	finally
    //   288	293	736	java/lang/SecurityException
    //   305	313	736	java/lang/SecurityException
    //   336	339	736	java/lang/SecurityException
    //   343	348	736	java/lang/SecurityException
    //   288	293	741	java/io/IOException
    //   305	313	741	java/io/IOException
    //   336	339	741	java/io/IOException
    //   343	348	741	java/io/IOException
    //   749	754	785	finally
  }
  
  private Set a(Collection paramCollection)
  {
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    paramCollection = paramCollection.iterator();
    for (;;)
    {
      boolean bool = paramCollection.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = (Integer)paramCollection.next();
      m localm = (m)f.get();
      int i1 = ((Integer)localObject).intValue();
      localObject = localm.a(i1);
      localHashSet.add(localObject);
    }
    return localHashSet;
  }
  
  private static Set a(ContentProviderResult[] paramArrayOfContentProviderResult)
  {
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    int i1 = paramArrayOfContentProviderResult.length;
    int i2 = 0;
    while (i2 < i1)
    {
      Object localObject = uri;
      if (localObject != null)
      {
        String str1 = ((Uri)localObject).getPath();
        if (str1 != null)
        {
          str1 = ((Uri)localObject).getPath();
          String str2 = "msg_messages";
          boolean bool = str1.contains(str2);
          if (bool)
          {
            long l1 = ContentUris.parseId((Uri)localObject);
            localObject = Long.valueOf(l1);
            localHashSet.add(localObject);
          }
        }
      }
      i2 += 1;
    }
    return localHashSet;
  }
  
  private org.a.a.b a(Message paramMessage, int paramInt, List paramList)
  {
    int i1 = 1;
    Object localObject1 = new String[i1];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Enqueue for sending message: ");
    ((StringBuilder)localObject2).append(paramMessage);
    ((StringBuilder)localObject2).append(" by transport: ");
    ((StringBuilder)localObject2).append(paramInt);
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    localObject1 = e;
    localObject2 = (m)f.get();
    Object localObject3 = ((m)localObject2).b(paramInt);
    int i2 = 9;
    int i3 = -1;
    if (localObject3 != null)
    {
      i4 = 0;
      localObject4 = null;
    }
    try
    {
      Object localObject5 = u;
      long l1 = b;
      localObject5 = ((o)localObject5).a(l1);
      localObject5 = ((com.truecaller.androidactors.w)localObject5).d();
      localObject5 = (Conversation)localObject5;
      localObject4 = localObject5;
    }
    catch (InterruptedException localInterruptedException)
    {
      Object localObject6;
      for (;;) {}
    }
    if (localObject4 == null)
    {
      localObject3 = paramMessage.m();
      f = i2;
      g = i1;
      localObject3 = ((Message.a)localObject3).b();
      b.a(paramList, (Message)localObject3, i3);
      localObject3 = new String[i1];
      paramMessage = String.valueOf(paramMessage);
      paramMessage = "Can't fetch conversation for message: ".concat(paramMessage);
      localObject3[0] = paramMessage;
      AssertionUtil.reportWeirdnessButNeverCrash("Trying to enqueue a message but conversation doesn't exist");
      return (org.a.a.b)localObject1;
    }
    localObject6 = l;
    localObject6 = ((l)localObject3).a(paramMessage, (Participant[])localObject6);
    int i4 = a;
    switch (i4)
    {
    default: 
      break;
    case 2: 
      long l2 = a;
      b.a(paramList, l2);
      localObject3 = d;
      localObject1 = localObject3;
      break;
    case 1: 
      localObject1 = b;
      localObject2 = new String[0];
      AssertionUtil.AlwaysFatal.isNotNull(localObject1, (String[])localObject2);
      paramMessage = paramMessage.m();
      int i5 = 5;
      f = i5;
      paramInt = ((l)localObject3).f();
      localObject6 = b;
      paramMessage = paramMessage.a(paramInt, (TransportInfo)localObject6).b();
      b.a(paramList, paramMessage, i3);
      localObject3 = d;
      localObject1 = localObject3;
      break;
    case 0: 
      paramMessage = paramMessage.m();
      f = i2;
      paramMessage = paramMessage.b();
      b.a(paramList, paramMessage, i3);
      paramList = r;
      localObject6 = "Failure";
      paramInt = ((l)localObject3).f();
      paramList.a((String)localObject6, paramMessage, paramInt);
    }
    localObject3 = r;
    paramMessage = o;
    ((com.truecaller.messaging.c.a)localObject3).b(paramMessage);
    break label555;
    localObject3 = new String[i1];
    localObject6 = new java/lang/StringBuilder;
    Object localObject4 = "Unknown transport: ";
    ((StringBuilder)localObject6).<init>((String)localObject4);
    i4 = k;
    ((StringBuilder)localObject6).append(i4);
    localObject6 = ((StringBuilder)localObject6).toString();
    localObject3[0] = localObject6;
    paramMessage = paramMessage.m();
    f = i2;
    paramMessage = paramMessage.b();
    b.a(paramList, paramMessage, i3);
    localObject3 = r;
    paramList = "Failure";
    i1 = 3;
    ((com.truecaller.messaging.c.a)localObject3).a(paramList, paramMessage, i1);
    label555:
    return (org.a.a.b)localObject1;
  }
  
  private static void a(SparseBooleanArray paramSparseBooleanArray1, SparseBooleanArray paramSparseBooleanArray2)
  {
    int i1 = 0;
    for (;;)
    {
      int i2 = paramSparseBooleanArray2.size();
      if (i1 >= i2) {
        break;
      }
      i2 = paramSparseBooleanArray2.keyAt(i1);
      boolean bool1 = paramSparseBooleanArray2.valueAt(i1);
      boolean bool2 = true;
      if (bool1)
      {
        bool1 = paramSparseBooleanArray1.get(i2, bool2);
        if (bool1) {}
      }
      else
      {
        bool2 = false;
      }
      paramSparseBooleanArray1.put(i2, bool2);
      i1 += 1;
    }
  }
  
  private void a(List paramList)
  {
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      Object localObject1 = (String)paramList.next();
      try
      {
        Object localObject2 = com.truecaller.tracking.events.w.b();
        localObject1 = ((w.a)localObject2).a((CharSequence)localObject1);
        localObject1 = ((w.a)localObject1).a();
        localObject2 = q;
        localObject2 = ((com.truecaller.androidactors.f)localObject2).a();
        localObject2 = (com.truecaller.analytics.ae)localObject2;
        ((com.truecaller.analytics.ae)localObject2).a((org.apache.a.d.d)localObject1);
      }
      catch (org.apache.a.a locala) {}
    }
  }
  
  private void a(org.a.a.b paramb1, org.a.a.b paramb2, int paramInt, Iterable paramIterable)
  {
    paramb1 = a(paramb1, paramb2, paramInt);
    if (paramb1 == null) {
      return;
    }
    boolean bool1 = x.v().a();
    boolean bool2 = x.e().a();
    paramb2 = new com/truecaller/messaging/data/v$c;
    Object localObject1 = i.get();
    Object localObject2 = localObject1;
    localObject2 = (com.truecaller.messaging.transport.f)localObject1;
    localObject1 = h.get();
    Object localObject3 = localObject1;
    localObject3 = (i)localObject1;
    paramb2.<init>((com.truecaller.messaging.transport.f)localObject2, (i)localObject3, paramb1, bool1, bool2);
    int i1 = a;
    a(paramb2, i1, paramIterable);
  }
  
  private void a(boolean paramBoolean, Iterable paramIterable)
  {
    new String[1][0] = "Full message sync is requested";
    org.a.a.b localb1 = org.a.a.b.ay_();
    org.a.a.b localb2 = new org/a/a/b;
    localb2.<init>(0L);
    a(localb1, localb2, paramBoolean, paramIterable);
  }
  
  private void a(Conversation[] paramArrayOfConversation, v.b paramb)
  {
    int i1 = 0;
    for (;;)
    {
      int i2 = paramArrayOfConversation.length;
      if (i1 >= i2) {
        break;
      }
      i2 = i1 + 200;
      int i3 = paramArrayOfConversation.length;
      i2 = Math.min(i2, i3);
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      while (i1 < i2)
      {
        int i4 = localStringBuilder.length();
        if (i4 > 0)
        {
          localObject = " OR ";
          localStringBuilder.append((String)localObject);
        }
        localStringBuilder.append("conversation_id = ");
        long l1 = a;
        localStringBuilder.append(l1);
        localStringBuilder.append(" ");
        Object localObject = x;
        Conversation localConversation1 = paramArrayOfConversation[i1];
        int i5 = p;
        Conversation localConversation2 = paramArrayOfConversation[i1];
        int i6 = q;
        localObject = com.truecaller.content.c.a((com.truecaller.featuretoggles.e)localObject, i5, i6);
        localStringBuilder.append((String)localObject);
        i1 += 1;
      }
      String str = localStringBuilder.toString();
      paramb.onSelection(str);
    }
  }
  
  private boolean a(Uri paramUri)
  {
    Object localObject = "file";
    String str = paramUri.getScheme();
    boolean bool = ((String)localObject).equals(str);
    if (!bool) {
      return false;
    }
    localObject = new java/io/File;
    paramUri = paramUri.getPath();
    ((File)localObject).<init>(paramUri);
    paramUri = ((File)localObject).getParentFile();
    localObject = d;
    return paramUri.equals(localObject);
  }
  
  private ContentProviderResult[] a(ArrayList paramArrayList)
  {
    try
    {
      ContentResolver localContentResolver = c;
      String str = TruecallerContract.a;
      return localContentResolver.applyBatch(str, paramArrayList);
    }
    catch (SQLiteFullException paramArrayList) {}catch (OperationApplicationException paramArrayList) {}catch (RemoteException paramArrayList) {}
    AssertionUtil.reportThrowableButNeverCrash(paramArrayList);
    return b;
  }
  
  private SparseBooleanArray b(String paramString, boolean paramBoolean)
  {
    Object localObject1 = new android/util/SparseArray;
    boolean bool1 = true;
    ((SparseArray)localObject1).<init>(bool1);
    SparseArray localSparseArray = new android/util/SparseArray;
    localSparseArray.<init>(bool1);
    SparseBooleanArray localSparseBooleanArray = new android/util/SparseBooleanArray;
    localSparseBooleanArray.<init>(bool1);
    bool1 = false;
    com.truecaller.messaging.data.a.s locals = null;
    try
    {
      ContentResolver localContentResolver = c;
      Uri localUri = TruecallerContract.h.a();
      boolean bool2 = false;
      Object localObject2 = null;
      boolean bool3 = false;
      Object localObject3 = null;
      org.a.a.b localb = null;
      Object localObject4 = paramString;
      paramString = localContentResolver.query(localUri, null, paramString, null, null);
      localContentResolver = null;
      boolean bool4 = true;
      if (paramString != null)
      {
        localObject2 = l;
        locals = ((c)localObject2).e(paramString);
        for (;;)
        {
          boolean bool5 = locals.moveToNext();
          if (!bool5) {
            break;
          }
          int i4 = locals.d();
          localObject2 = locals.e();
          localObject4 = f;
          localObject4 = ((dagger.a)localObject4).get();
          localObject4 = (m)localObject4;
          localObject4 = ((m)localObject4).b(i4);
          if (localObject4 == null)
          {
            String[] arrayOfString = new String[bool4];
            localObject1 = "Unsupported transport type: ";
            paramString = String.valueOf(i4);
            paramString = ((String)localObject1).concat(paramString);
            arrayOfString[0] = paramString;
            AssertionUtil.OnlyInDebug.fail(arrayOfString);
            return localSparseBooleanArray;
          }
          localObject3 = ((SparseArray)localObject1).get(i4);
          localObject3 = (ad)localObject3;
          if (localObject3 == null)
          {
            localObject3 = ((l)localObject4).b();
            ((SparseArray)localObject1).put(i4, localObject3);
            localb = org.a.a.b.ay_();
            localSparseArray.put(i4, localb);
          }
          bool2 = ((l)localObject4).a((TransportInfo)localObject2, (ad)localObject3, paramBoolean);
          if (bool2)
          {
            localObject2 = new org/a/a/b;
            long l1 = locals.b();
            ((org.a.a.b)localObject2).<init>(l1);
            localObject4 = localSparseArray.get(i4);
            localObject4 = (org.a.a.b)localObject4;
            boolean bool6 = ((org.a.a.b)localObject4).b((org.a.a.x)localObject2);
            if (bool6) {
              localSparseArray.put(i4, localObject2);
            }
          }
        }
      }
      com.truecaller.util.q.a(locals);
      paramString = new java/util/HashSet;
      paramString.<init>();
      paramBoolean = ((SparseArray)localObject1).size();
      bool1 = false;
      locals = null;
      while (bool1 < paramBoolean)
      {
        localObject2 = (m)f.get();
        int i5 = ((SparseArray)localObject1).keyAt(bool1);
        localObject2 = ((m)localObject2).b(i5);
        localObject4 = new String[] { "Only known transports should by part of transactions" };
        AssertionUtil.AlwaysFatal.isNotNull(localObject2, (String[])localObject4);
        localObject4 = (ad)((SparseArray)localObject1).valueAt(bool1);
        bool3 = ((l)localObject2).b((ad)localObject4);
        if (bool3)
        {
          bool7 = ((l)localObject2).a((ad)localObject4);
          if (bool7)
          {
            bool7 = true;
            break label491;
          }
        }
        boolean bool7 = false;
        localObject4 = null;
        label491:
        if (bool7)
        {
          int i3 = ((l)localObject2).f();
          localObject3 = (org.a.a.b)localSparseArray.get(i3);
          ((l)localObject2).a((org.a.a.b)localObject3);
          paramString.add(localObject2);
        }
        int i2 = ((l)localObject2).f();
        localSparseBooleanArray.put(i2, bool7);
        int i1;
        bool1 += true;
      }
      paramBoolean = paramString.isEmpty();
      if (!paramBoolean) {
        b(bool4, paramString);
      }
      return localSparseBooleanArray;
    }
    finally
    {
      com.truecaller.util.q.a(locals);
    }
  }
  
  private void b(String paramString)
  {
    SparseArray localSparseArray1 = new android/util/SparseArray;
    int i1 = 5;
    localSparseArray1.<init>(i1);
    SparseArray localSparseArray2 = new android/util/SparseArray;
    localSparseArray2.<init>(i1);
    i1 = 0;
    com.truecaller.messaging.data.a.s locals = null;
    try
    {
      Object localObject1 = c;
      Object localObject2 = TruecallerContract.h.a();
      boolean bool1 = false;
      Object localObject3 = null;
      boolean bool2 = false;
      Object localObject4 = paramString;
      paramString = ((ContentResolver)localObject1).query((Uri)localObject2, null, paramString, null, null);
      if (paramString == null)
      {
        com.truecaller.util.q.a(null);
        return;
      }
      localObject1 = l;
      locals = ((c)localObject1).e(paramString);
      int i3;
      boolean bool4;
      for (;;)
      {
        boolean bool3 = locals.moveToNext();
        i3 = 0;
        localObject1 = null;
        int i4 = 1;
        if (!bool3) {
          break;
        }
        int i2 = locals.d();
        localObject3 = locals.e();
        localObject4 = f;
        localObject4 = ((dagger.a)localObject4).get();
        localObject4 = (m)localObject4;
        localObject4 = ((m)localObject4).b(i2);
        if (localObject4 == null)
        {
          localObject2 = new String[i4];
          localObject3 = "Unsupported transport type: ";
          paramString = String.valueOf(i2);
          paramString = ((String)localObject3).concat(paramString);
          localObject2[0] = paramString;
          AssertionUtil.OnlyInDebug.fail((String[])localObject2);
        }
        else
        {
          localObject1 = localSparseArray1.get(i2);
          localObject1 = (ad)localObject1;
          if (localObject1 == null)
          {
            localObject1 = ((l)localObject4).b();
            localSparseArray1.put(i2, localObject1);
            localObject2 = org.a.a.b.ay_();
            localSparseArray2.put(i2, localObject2);
          }
          i4 = ((ad)localObject1).b();
          bool1 = ((l)localObject4).a((TransportInfo)localObject3, (ad)localObject1);
          if (!bool1)
          {
            ((ad)localObject1).a(i4);
          }
          else
          {
            localObject1 = new org/a/a/b;
            long l1 = locals.b();
            ((org.a.a.b)localObject1).<init>(l1);
            localObject2 = localSparseArray2.get(i2);
            localObject2 = (org.a.a.b)localObject2;
            bool4 = ((org.a.a.b)localObject2).b((org.a.a.x)localObject1);
            if (bool4) {
              localSparseArray2.put(i2, localObject1);
            }
          }
        }
      }
      com.truecaller.util.q.a(locals);
      paramString = new java/util/HashSet;
      paramString.<init>();
      i1 = localSparseArray1.size();
      while (i3 < i1)
      {
        localObject3 = (m)f.get();
        int i5 = localSparseArray1.keyAt(i3);
        localObject3 = ((m)localObject3).b(i5);
        localObject4 = new String[] { "Only known transports should by part of transactions" };
        AssertionUtil.AlwaysFatal.isNotNull(localObject3, (String[])localObject4);
        localObject4 = (ad)localSparseArray1.valueAt(i3);
        bool2 = ((l)localObject3).b((ad)localObject4);
        if (bool2)
        {
          boolean bool5 = ((l)localObject3).a((ad)localObject4);
          if (bool5)
          {
            int i6 = ((l)localObject3).f();
            localObject4 = (org.a.a.b)localSparseArray2.get(i6);
            ((l)localObject3).a((org.a.a.b)localObject4);
            paramString.add(localObject3);
          }
        }
        i3 += 1;
      }
      boolean bool6 = paramString.isEmpty();
      if (!bool6) {
        b(bool4, paramString);
      }
      return;
    }
    finally
    {
      com.truecaller.util.q.a(locals);
    }
  }
  
  private void b(List paramList)
  {
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject1 = (Entity)paramList.next();
      boolean bool2 = ((Entity)localObject1).a();
      if (!bool2)
      {
        localObject1 = (BinaryEntity)localObject1;
        bool2 = c;
        Object localObject2;
        if (bool2)
        {
          localObject2 = p;
          localObject1 = b;
          ((ch)localObject2).a((Uri)localObject1);
        }
        else
        {
          localObject2 = b;
          bool2 = a((Uri)localObject2);
          if (bool2)
          {
            localObject2 = new java/io/File;
            localObject1 = b.getPath();
            ((File)localObject2).<init>((String)localObject1);
            bool1 = ((File)localObject2).exists();
            if (bool1) {
              ((File)localObject2).delete();
            }
          }
        }
      }
    }
  }
  
  /* Error */
  private void b(boolean paramBoolean, Iterable paramIterable)
  {
    // Byte code:
    //   0: ldc_w 992
    //   3: astore_3
    //   4: iconst_1
    //   5: anewarray 153	java/lang/String
    //   8: iconst_0
    //   9: aload_3
    //   10: aastore
    //   11: aload_0
    //   12: invokespecial 994	com/truecaller/messaging/data/v:d	()Z
    //   15: istore 4
    //   17: iload 4
    //   19: ifne +9 -> 28
    //   22: aload_0
    //   23: iload_1
    //   24: invokevirtual 997	com/truecaller/messaging/data/v:a	(Z)V
    //   27: return
    //   28: invokestatic 419	org/a/a/b:ay_	()Lorg/a/a/b;
    //   31: astore_3
    //   32: aload_2
    //   33: invokeinterface 182 1 0
    //   38: astore 5
    //   40: aload 5
    //   42: invokeinterface 188 1 0
    //   47: istore 6
    //   49: iload 6
    //   51: ifeq +54 -> 105
    //   54: aload 5
    //   56: invokeinterface 192 1 0
    //   61: astore 7
    //   63: aload 7
    //   65: checkcast 194	com/truecaller/messaging/transport/l
    //   68: astore 7
    //   70: aload 7
    //   72: invokeinterface 352 1 0
    //   77: astore 7
    //   79: aload 7
    //   81: invokestatic 1000	com/truecaller/messaging/data/v:c	(Lorg/a/a/b;)Lorg/a/a/b;
    //   84: astore 7
    //   86: aload 7
    //   88: aload_3
    //   89: invokevirtual 355	org/a/a/b:c	(Lorg/a/a/x;)Z
    //   92: istore 8
    //   94: iload 8
    //   96: ifeq -56 -> 40
    //   99: aload 7
    //   101: astore_3
    //   102: goto -62 -> 40
    //   105: invokestatic 419	org/a/a/b:ay_	()Lorg/a/a/b;
    //   108: astore 5
    //   110: iload_1
    //   111: ifeq +8 -> 119
    //   114: iconst_1
    //   115: istore_1
    //   116: goto +5 -> 121
    //   119: iconst_0
    //   120: istore_1
    //   121: aload_0
    //   122: aload 5
    //   124: aload_3
    //   125: iload_1
    //   126: aload_2
    //   127: invokespecial 921	com/truecaller/messaging/data/v:a	(Lorg/a/a/b;Lorg/a/a/b;ILjava/lang/Iterable;)V
    //   130: return
    //   131: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	132	0	this	v
    //   0	132	1	paramBoolean	boolean
    //   0	132	2	paramIterable	Iterable
    //   3	122	3	localObject1	Object
    //   15	3	4	bool1	boolean
    //   38	85	5	localObject2	Object
    //   47	3	6	bool2	boolean
    //   61	39	7	localObject3	Object
    //   92	3	8	bool3	boolean
    // Exception table:
    //   from	to	target	type
    //   11	15	131	finally
    //   23	27	131	finally
    //   28	31	131	finally
    //   32	38	131	finally
    //   40	47	131	finally
    //   54	61	131	finally
    //   63	68	131	finally
    //   70	77	131	finally
    //   79	84	131	finally
    //   88	92	131	finally
    //   105	108	131	finally
    //   126	130	131	finally
  }
  
  private File c()
  {
    Object localObject = e;
    if (localObject != null)
    {
      localObject = Environment.getExternalStorageState();
      String str1 = "mounted";
      bool1 = ((String)localObject).equals(str1);
      if (bool1)
      {
        localObject = e;
        int i1 = localObject.length;
        int i2 = 0;
        while (i2 < i1)
        {
          File localFile = localObject[i2];
          String str2 = localFile.getAbsolutePath();
          String str3 = "/emulated/";
          boolean bool2 = str2.contains(str3);
          if (!bool2)
          {
            localObject = new java/io/File;
            str1 = "msg_media";
            ((File)localObject).<init>(localFile, str1);
            return (File)localObject;
          }
          i2 += 1;
        }
      }
    }
    boolean bool1 = false;
    localObject = null;
    return (File)localObject;
  }
  
  private static org.a.a.b c(org.a.a.b paramb)
  {
    long l1 = org.a.a.e.a();
    boolean bool = paramb.c(l1);
    if (bool)
    {
      paramb = new org/a/a/b;
      paramb.<init>(0L);
      return paramb;
    }
    return paramb;
  }
  
  private static long[] c(List paramList)
  {
    Long[] arrayOfLong = new Long[paramList.size()];
    return org.c.a.a.a.a.a((Long[])paramList.toArray(arrayOfLong));
  }
  
  private static String d(long[] paramArrayOfLong)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("message_id");
    localStringBuilder.append(" IN (");
    long l1 = paramArrayOfLong[0];
    localStringBuilder.append(l1);
    int i1 = 1;
    for (;;)
    {
      int i2 = paramArrayOfLong.length;
      if (i1 >= i2) {
        break;
      }
      i2 = 44;
      localStringBuilder.append(i2);
      long l2 = paramArrayOfLong[i1];
      localStringBuilder.append(l2);
      i1 += 1;
    }
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  private boolean d()
  {
    h localh = k;
    long l1 = localh.a();
    long l2 = 0L;
    boolean bool = l1 < l2;
    return bool;
  }
  
  private Set f(long paramLong)
  {
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    com.truecaller.messaging.data.a.g localg = null;
    try
    {
      c localc = l;
      ContentResolver localContentResolver = c;
      Uri localUri = TruecallerContract.z.a();
      String str = "message_id=?";
      int i1 = 1;
      String[] arrayOfString = new String[i1];
      Object localObject1 = String.valueOf(paramLong);
      arrayOfString[0] = localObject1;
      localObject1 = localContentResolver.query(localUri, null, str, arrayOfString, null);
      localg = localc.f((Cursor)localObject1);
      if (localg != null) {
        for (;;)
        {
          boolean bool1 = localg.moveToNext();
          if (!bool1) {
            break;
          }
          localObject1 = localg.a();
          boolean bool2 = ((Entity)localObject1).a();
          if (!bool2)
          {
            localObject1 = (BinaryEntity)localObject1;
            localObject1 = b;
            bool2 = a((Uri)localObject1);
            if (bool2)
            {
              File localFile = new java/io/File;
              localObject1 = ((Uri)localObject1).getPath();
              localFile.<init>((String)localObject1);
              localHashSet.add(localFile);
            }
          }
        }
      }
      return localHashSet;
    }
    finally
    {
      com.truecaller.util.q.a(localg);
    }
  }
  
  private void f(Message paramMessage)
  {
    Object localObject = (m)f.get();
    int i1 = j;
    localObject = ((m)localObject).b(i1);
    i1 = 1;
    if (localObject == null)
    {
      localObject = new String[i1];
      paramMessage = String.valueOf(paramMessage);
      paramMessage = "Can not save message to system table: ".concat(paramMessage);
      localObject[0] = paramMessage;
      AssertionUtil.OnlyInDebug.fail((String[])localObject);
      return;
    }
    boolean bool = ((l)localObject).a(paramMessage);
    if (bool)
    {
      paramMessage = Collections.singleton(Integer.valueOf(((l)localObject).f()));
      b(i1, paramMessage);
      new String[1][0] = "Message saved";
      return;
    }
    new String[1][0] = "An error occurred while was saving message. Message have not been saved!";
  }
  
  public final com.truecaller.androidactors.w a(int paramInt)
  {
    String str = w.a(paramInt);
    Object localObject1 = c;
    Object localObject2 = TruecallerContract.g.a();
    String[] arrayOfString = { "_id", "split_criteria" };
    boolean bool = false;
    Object localObject3 = null;
    Cursor localCursor = ((ContentResolver)localObject1).query((Uri)localObject2, arrayOfString, str, null, null);
    localObject1 = new android/util/SparseBooleanArray;
    ((SparseBooleanArray)localObject1).<init>();
    localObject1 = com.truecaller.androidactors.w.b(localObject1);
    if (localCursor != null) {
      try
      {
        int i2 = localCursor.getCount();
        if (i2 > 0)
        {
          i2 = localCursor.getCount();
          localObject2 = new Conversation[i2];
          arrayOfString = null;
          int i3 = 0;
          str = null;
          for (;;)
          {
            bool = localCursor.moveToNext();
            if (!bool) {
              break;
            }
            int i1 = i3 + 1;
            localObject3 = new com/truecaller/messaging/data/types/Conversation$a;
            ((Conversation.a)localObject3).<init>();
            long l1 = localCursor.getLong(0);
            a = l1;
            int i4 = 1;
            i4 = localCursor.getInt(i4);
            q = i4;
            p = paramInt;
            localObject3 = ((Conversation.a)localObject3).a();
            localObject2[i3] = localObject3;
            i3 = i1;
          }
        }
        i2 = 0;
        localObject2 = null;
        localCursor.close();
        if (localObject2 != null) {
          localObject1 = a((Conversation[])localObject2);
        }
      }
      finally
      {
        localCursor.close();
      }
    }
    return (com.truecaller.androidactors.w)localObject1;
  }
  
  public final com.truecaller.androidactors.w a(long paramLong)
  {
    return u.b(paramLong);
  }
  
  public final com.truecaller.androidactors.w a(long paramLong, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("conversation_id=");
    localStringBuilder.append(paramLong);
    localStringBuilder.append(" ");
    String str = com.truecaller.content.c.a(x, paramInt1, paramInt2);
    localStringBuilder.append(str);
    str = localStringBuilder.toString();
    return com.truecaller.androidactors.w.b(b(str, paramBoolean));
  }
  
  /* Error */
  public final com.truecaller.androidactors.w a(com.truecaller.messaging.data.types.Draft paramDraft)
  {
    // Byte code:
    //   0: aload_1
    //   1: getfield 1123	com/truecaller/messaging/data/types/Draft:d	[Lcom/truecaller/messaging/data/types/Participant;
    //   4: astore_2
    //   5: iconst_0
    //   6: istore_3
    //   7: aconst_null
    //   8: astore 4
    //   10: iconst_0
    //   11: anewarray 153	java/lang/String
    //   14: astore 5
    //   16: aload_2
    //   17: aload 5
    //   19: invokestatic 455	com/truecaller/log/AssertionUtil$AlwaysFatal:isNotNull	(Ljava/lang/Object;[Ljava/lang/String;)V
    //   22: aload_1
    //   23: getfield 1123	com/truecaller/messaging/data/types/Draft:d	[Lcom/truecaller/messaging/data/types/Participant;
    //   26: astore_2
    //   27: aload_2
    //   28: arraylength
    //   29: istore 6
    //   31: iconst_1
    //   32: istore 7
    //   34: iload 6
    //   36: ifle +9 -> 45
    //   39: iconst_1
    //   40: istore 6
    //   42: goto +8 -> 50
    //   45: iconst_0
    //   46: istore 6
    //   48: aconst_null
    //   49: astore_2
    //   50: iconst_0
    //   51: anewarray 153	java/lang/String
    //   54: astore 8
    //   56: iload 6
    //   58: aload 8
    //   60: invokestatic 1127	com/truecaller/log/AssertionUtil$AlwaysFatal:isTrue	(Z[Ljava/lang/String;)V
    //   63: iload 7
    //   65: anewarray 153	java/lang/String
    //   68: astore_2
    //   69: aload_1
    //   70: invokestatic 817	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   73: astore 9
    //   75: ldc_w 1129
    //   78: aload 9
    //   80: invokevirtual 402	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   83: astore 8
    //   85: aload_2
    //   86: iconst_0
    //   87: aload 8
    //   89: aastore
    //   90: aload_1
    //   91: invokevirtual 1130	com/truecaller/messaging/data/types/Draft:a	()Z
    //   94: istore 6
    //   96: iload 6
    //   98: ifeq +270 -> 368
    //   101: aload_1
    //   102: getfield 1131	com/truecaller/messaging/data/types/Draft:a	J
    //   105: lstore 10
    //   107: iconst_m1
    //   108: i2l
    //   109: lstore 12
    //   111: lload 10
    //   113: lload 12
    //   115: lcmp
    //   116: istore 6
    //   118: iload 6
    //   120: ifeq +9 -> 129
    //   123: iconst_1
    //   124: istore 6
    //   126: goto +8 -> 134
    //   129: iconst_0
    //   130: istore 6
    //   132: aconst_null
    //   133: astore_2
    //   134: iload 6
    //   136: ifeq +227 -> 363
    //   139: aload_1
    //   140: getfield 1131	com/truecaller/messaging/data/types/Draft:a	J
    //   143: lstore 10
    //   145: new 146	java/util/ArrayList
    //   148: astore_2
    //   149: aload_2
    //   150: invokespecial 147	java/util/ArrayList:<init>	()V
    //   153: aload_0
    //   154: lload 10
    //   156: invokespecial 478	com/truecaller/messaging/data/v:f	(J)Ljava/util/Set;
    //   159: astore 14
    //   161: invokestatic 572	com/truecaller/content/TruecallerContract$aa:a	()Landroid/net/Uri;
    //   164: astore 15
    //   166: aload 15
    //   168: invokestatic 631	android/content/ContentProviderOperation:newDelete	(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;
    //   171: astore 15
    //   173: ldc_w 1133
    //   176: astore 16
    //   178: iload 7
    //   180: anewarray 153	java/lang/String
    //   183: astore 17
    //   185: lload 10
    //   187: invokestatic 221	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   190: astore 18
    //   192: aload 17
    //   194: iconst_0
    //   195: aload 18
    //   197: aastore
    //   198: aload 15
    //   200: aload 16
    //   202: aload 17
    //   204: invokevirtual 637	android/content/ContentProviderOperation$Builder:withSelection	(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;
    //   207: pop
    //   208: aload 15
    //   210: invokevirtual 624	android/content/ContentProviderOperation$Builder:build	()Landroid/content/ContentProviderOperation;
    //   213: astore 15
    //   215: aload_2
    //   216: aload 15
    //   218: invokevirtual 625	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   221: pop
    //   222: aload_0
    //   223: aload_2
    //   224: invokespecial 310	com/truecaller/messaging/data/v:a	(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    //   227: astore_2
    //   228: aload_2
    //   229: arraylength
    //   230: istore 19
    //   232: iload 19
    //   234: ifeq +81 -> 315
    //   237: aload_2
    //   238: iconst_0
    //   239: aaload
    //   240: astore_2
    //   241: aload_2
    //   242: getfield 1137	android/content/ContentProviderResult:count	Ljava/lang/Integer;
    //   245: astore_2
    //   246: aload_2
    //   247: invokevirtual 775	java/lang/Integer:intValue	()I
    //   250: istore 6
    //   252: iload 6
    //   254: ifne +6 -> 260
    //   257: goto +58 -> 315
    //   260: aload 14
    //   262: invokeinterface 518 1 0
    //   267: astore_2
    //   268: aload_2
    //   269: invokeinterface 188 1 0
    //   274: istore_3
    //   275: iload_3
    //   276: ifeq +71 -> 347
    //   279: aload_2
    //   280: invokeinterface 192 1 0
    //   285: astore 4
    //   287: aload 4
    //   289: checkcast 87	java/io/File
    //   292: astore 4
    //   294: aload 4
    //   296: invokevirtual 521	java/io/File:exists	()Z
    //   299: istore 7
    //   301: iload 7
    //   303: ifeq -35 -> 268
    //   306: aload 4
    //   308: invokevirtual 524	java/io/File:delete	()Z
    //   311: pop
    //   312: goto -44 -> 268
    //   315: iload 7
    //   317: anewarray 153	java/lang/String
    //   320: astore_2
    //   321: ldc_w 1139
    //   324: astore 5
    //   326: lload 10
    //   328: invokestatic 221	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   331: astore 8
    //   333: aload 5
    //   335: aload 8
    //   337: invokevirtual 402	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   340: astore 5
    //   342: aload_2
    //   343: iconst_0
    //   344: aload 5
    //   346: aastore
    //   347: aload_1
    //   348: invokevirtual 1142	com/truecaller/messaging/data/types/Draft:c	()Lcom/truecaller/messaging/data/types/Draft$a;
    //   351: astore_1
    //   352: aload_1
    //   353: lload 12
    //   355: putfield 1145	com/truecaller/messaging/data/types/Draft$a:a	J
    //   358: aload_1
    //   359: invokevirtual 1148	com/truecaller/messaging/data/types/Draft$a:d	()Lcom/truecaller/messaging/data/types/Draft;
    //   362: astore_1
    //   363: aload_1
    //   364: invokestatic 1087	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   367: areturn
    //   368: ldc_w 1150
    //   371: astore_2
    //   372: aload_1
    //   373: aload_2
    //   374: invokevirtual 1153	com/truecaller/messaging/data/types/Draft:a	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Message;
    //   377: astore_2
    //   378: aload_1
    //   379: getfield 1123	com/truecaller/messaging/data/types/Draft:d	[Lcom/truecaller/messaging/data/types/Participant;
    //   382: astore 8
    //   384: aload_0
    //   385: aload_2
    //   386: aload 8
    //   388: invokespecial 1156	com/truecaller/messaging/data/v:a	(Lcom/truecaller/messaging/data/types/Message;[Lcom/truecaller/messaging/data/types/Participant;)Lcom/truecaller/messaging/data/types/Message;
    //   391: astore_2
    //   392: aload_2
    //   393: ifnonnull +8 -> 401
    //   396: aload_1
    //   397: invokestatic 1087	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   400: areturn
    //   401: aconst_null
    //   402: astore 8
    //   404: aload_0
    //   405: getfield 104	com/truecaller/messaging/data/v:l	Lcom/truecaller/messaging/data/c;
    //   408: astore 9
    //   410: aload_0
    //   411: getfield 85	com/truecaller/messaging/data/v:c	Landroid/content/ContentResolver;
    //   414: astore 20
    //   416: aload_2
    //   417: getfield 794	com/truecaller/messaging/data/types/Message:b	J
    //   420: lstore 21
    //   422: lload 21
    //   424: invokestatic 1159	com/truecaller/content/TruecallerContract$g:a	(J)Landroid/net/Uri;
    //   427: astore 23
    //   429: iconst_0
    //   430: istore 24
    //   432: aconst_null
    //   433: astore 14
    //   435: iconst_0
    //   436: istore 19
    //   438: aconst_null
    //   439: astore 15
    //   441: aconst_null
    //   442: astore 16
    //   444: aconst_null
    //   445: astore 17
    //   447: aload 20
    //   449: aload 23
    //   451: aconst_null
    //   452: aconst_null
    //   453: aconst_null
    //   454: aconst_null
    //   455: invokevirtual 250	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   458: astore 20
    //   460: aload 9
    //   462: aload 20
    //   464: invokeinterface 1162 2 0
    //   469: astore 9
    //   471: aload 9
    //   473: ifnull +37 -> 510
    //   476: aload 9
    //   478: invokeinterface 1167 1 0
    //   483: istore 25
    //   485: iload 25
    //   487: ifeq +23 -> 510
    //   490: aload 9
    //   492: invokeinterface 1169 1 0
    //   497: astore 8
    //   499: goto +11 -> 510
    //   502: astore_1
    //   503: aload 9
    //   505: astore 8
    //   507: goto +212 -> 719
    //   510: aload 9
    //   512: invokestatic 263	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   515: new 1144	com/truecaller/messaging/data/types/Draft$a
    //   518: astore 9
    //   520: aload 9
    //   522: invokespecial 1170	com/truecaller/messaging/data/types/Draft$a:<init>	()V
    //   525: aload_2
    //   526: getfield 475	com/truecaller/messaging/data/types/Message:a	J
    //   529: lstore 12
    //   531: aload 9
    //   533: lload 12
    //   535: putfield 1145	com/truecaller/messaging/data/types/Draft$a:a	J
    //   538: aload_1
    //   539: getfield 1123	com/truecaller/messaging/data/types/Draft:d	[Lcom/truecaller/messaging/data/types/Participant;
    //   542: astore 20
    //   544: aload 9
    //   546: aload 20
    //   548: invokevirtual 1173	com/truecaller/messaging/data/types/Draft$a:a	([Lcom/truecaller/messaging/data/types/Participant;)Lcom/truecaller/messaging/data/types/Draft$a;
    //   551: pop
    //   552: aload_1
    //   553: getfield 1175	com/truecaller/messaging/data/types/Draft:h	J
    //   556: lstore 12
    //   558: aload 9
    //   560: lload 12
    //   562: putfield 1177	com/truecaller/messaging/data/types/Draft$a:g	J
    //   565: aload_1
    //   566: getfield 1180	com/truecaller/messaging/data/types/Draft:i	Lcom/truecaller/messaging/data/types/ReplySnippet;
    //   569: astore_1
    //   570: aload 9
    //   572: aload_1
    //   573: putfield 1182	com/truecaller/messaging/data/types/Draft$a:h	Lcom/truecaller/messaging/data/types/ReplySnippet;
    //   576: aload_2
    //   577: getfield 481	com/truecaller/messaging/data/types/Message:n	[Lcom/truecaller/messaging/data/types/Entity;
    //   580: astore_1
    //   581: aload_1
    //   582: arraylength
    //   583: istore 6
    //   585: iconst_0
    //   586: istore 25
    //   588: aconst_null
    //   589: astore 20
    //   591: iload 25
    //   593: iload 6
    //   595: if_icmpge +69 -> 664
    //   598: aload_1
    //   599: iload 25
    //   601: aaload
    //   602: astore 23
    //   604: aload 23
    //   606: invokevirtual 499	com/truecaller/messaging/data/types/Entity:a	()Z
    //   609: istore 24
    //   611: iload 24
    //   613: ifeq +27 -> 640
    //   616: aload 23
    //   618: checkcast 651	com/truecaller/messaging/data/types/TextEntity
    //   621: astore 23
    //   623: aload 23
    //   625: getfield 653	com/truecaller/messaging/data/types/TextEntity:a	Ljava/lang/String;
    //   628: astore 23
    //   630: aload 9
    //   632: aload 23
    //   634: putfield 1184	com/truecaller/messaging/data/types/Draft$a:d	Ljava/lang/String;
    //   637: goto +18 -> 655
    //   640: aload 23
    //   642: checkcast 501	com/truecaller/messaging/data/types/BinaryEntity
    //   645: astore 23
    //   647: aload 9
    //   649: aload 23
    //   651: invokevirtual 1187	com/truecaller/messaging/data/types/Draft$a:a	(Lcom/truecaller/messaging/data/types/BinaryEntity;)Lcom/truecaller/messaging/data/types/Draft$a;
    //   654: pop
    //   655: iload 25
    //   657: iconst_1
    //   658: iadd
    //   659: istore 25
    //   661: goto -70 -> 591
    //   664: aload 8
    //   666: ifnull +10 -> 676
    //   669: aload 9
    //   671: aload 8
    //   673: putfield 1190	com/truecaller/messaging/data/types/Draft$a:b	Lcom/truecaller/messaging/data/types/Conversation;
    //   676: aload 9
    //   678: invokevirtual 1148	com/truecaller/messaging/data/types/Draft$a:d	()Lcom/truecaller/messaging/data/types/Draft;
    //   681: astore_1
    //   682: iload 7
    //   684: anewarray 153	java/lang/String
    //   687: astore_2
    //   688: ldc_w 1192
    //   691: astore 5
    //   693: aload_1
    //   694: invokestatic 817	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   697: astore 8
    //   699: aload 5
    //   701: aload 8
    //   703: invokevirtual 402	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   706: astore 5
    //   708: aload_2
    //   709: iconst_0
    //   710: aload 5
    //   712: aastore
    //   713: aload_1
    //   714: invokestatic 1087	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   717: areturn
    //   718: astore_1
    //   719: aload 8
    //   721: invokestatic 263	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   724: aload_1
    //   725: athrow
    //   726: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	727	0	this	v
    //   0	727	1	paramDraft	com.truecaller.messaging.data.types.Draft
    //   4	705	2	localObject1	Object
    //   6	270	3	bool1	boolean
    //   8	299	4	localObject2	Object
    //   14	697	5	localObject3	Object
    //   29	28	6	i1	int
    //   94	41	6	bool2	boolean
    //   250	346	6	i2	int
    //   32	651	7	bool3	boolean
    //   54	666	8	localObject4	Object
    //   73	604	9	localObject5	Object
    //   105	222	10	l1	long
    //   109	452	12	l2	long
    //   159	275	14	localSet	Set
    //   164	276	15	localObject6	Object
    //   176	267	16	str1	String
    //   183	263	17	arrayOfString	String[]
    //   190	6	18	str2	String
    //   230	207	19	i3	int
    //   414	176	20	localObject7	Object
    //   420	3	21	l3	long
    //   427	223	23	localObject8	Object
    //   430	182	24	bool4	boolean
    //   483	176	25	i4	int
    //   659	1	25	i5	int
    // Exception table:
    //   from	to	target	type
    //   476	483	502	finally
    //   490	497	502	finally
    //   404	408	718	finally
    //   410	414	718	finally
    //   416	420	718	finally
    //   422	427	718	finally
    //   454	458	718	finally
    //   462	469	718	finally
    //   90	94	726	finally
    //   101	105	726	finally
    //   139	143	726	finally
    //   145	148	726	finally
    //   149	153	726	finally
    //   154	159	726	finally
    //   161	164	726	finally
    //   166	171	726	finally
    //   178	183	726	finally
    //   185	190	726	finally
    //   195	198	726	finally
    //   202	208	726	finally
    //   208	213	726	finally
    //   216	222	726	finally
    //   223	227	726	finally
    //   228	230	726	finally
    //   238	240	726	finally
    //   241	245	726	finally
    //   246	250	726	finally
    //   260	267	726	finally
    //   268	274	726	finally
    //   279	285	726	finally
    //   287	292	726	finally
    //   294	299	726	finally
    //   306	312	726	finally
    //   315	320	726	finally
    //   326	331	726	finally
    //   335	340	726	finally
    //   344	347	726	finally
    //   347	351	726	finally
    //   353	358	726	finally
    //   358	362	726	finally
    //   363	367	726	finally
    //   373	377	726	finally
    //   378	382	726	finally
    //   386	391	726	finally
    //   396	400	726	finally
    //   510	515	726	finally
    //   515	518	726	finally
    //   520	525	726	finally
    //   525	529	726	finally
    //   533	538	726	finally
    //   538	542	726	finally
    //   546	552	726	finally
    //   552	556	726	finally
    //   560	565	726	finally
    //   565	569	726	finally
    //   572	576	726	finally
    //   576	580	726	finally
    //   581	583	726	finally
    //   599	602	726	finally
    //   604	609	726	finally
    //   616	621	726	finally
    //   623	628	726	finally
    //   632	637	726	finally
    //   640	645	726	finally
    //   649	655	726	finally
    //   671	676	726	finally
    //   676	681	726	finally
    //   682	687	726	finally
    //   693	697	726	finally
    //   701	706	726	finally
    //   710	713	726	finally
    //   713	717	726	finally
    //   719	724	726	finally
    //   724	726	726	finally
  }
  
  public final com.truecaller.androidactors.w a(Message paramMessage, int paramInt, String paramString)
  {
    Object localObject1 = n;
    localObject1 = a((Entity[])localObject1, true);
    paramMessage = paramMessage.m();
    a = -1;
    paramMessage = paramMessage.a().a((Collection)localObject1);
    int i1 = 3;
    f = i1;
    k = paramInt;
    Object localObject2 = NullTransportInfo.b;
    paramMessage = paramMessage.a(i1, (TransportInfo)localObject2);
    localObject2 = UUID.randomUUID().toString();
    q = ((String)localObject2);
    paramMessage = paramMessage.a(paramString);
    localObject2 = org.a.a.b.ay_();
    e = ((org.a.a.b)localObject2);
    localObject2 = org.a.a.b.ay_();
    d = ((org.a.a.b)localObject2);
    return com.truecaller.androidactors.w.b(paramMessage.b());
  }
  
  public final com.truecaller.androidactors.w a(Message paramMessage, long paramLong)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    long l1 = a;
    paramMessage = ContentProviderOperation.newUpdate(TruecallerContract.aa.a(l1));
    Long localLong1 = Long.valueOf(paramLong);
    paramMessage = paramMessage.withValue("date_sent", localLong1);
    String str = "date";
    Long localLong2 = Long.valueOf(paramLong);
    paramMessage = paramMessage.withValue(str, localLong2).build();
    localArrayList.add(paramMessage);
    paramMessage = a(localArrayList);
    int i1 = paramMessage.length;
    if (i1 > 0)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      paramMessage = null;
    }
    return com.truecaller.androidactors.w.b(Boolean.valueOf(i1));
  }
  
  public final com.truecaller.androidactors.w a(Message paramMessage, long paramLong, boolean paramBoolean)
  {
    boolean bool = paramMessage.b();
    int i3 = 0;
    Object localObject1 = null;
    String[] arrayOfString = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(bool, arrayOfString);
    int i1 = f;
    int i4 = 1;
    i1 &= i4;
    if (i1 != 0)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      localObject2 = null;
    }
    localObject1 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(i1, (String[])localObject1);
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject1 = ContentProviderOperation.newAssertQuery(TruecallerContract.aa.a(a));
    Integer localInteger = Integer.valueOf(t);
    localObject1 = ((ContentProviderOperation.Builder)localObject1).withValue("retry_count", localInteger).build();
    ((ArrayList)localObject2).add(localObject1);
    long l1 = a;
    localObject1 = ContentProviderOperation.newUpdate(TruecallerContract.aa.a(l1));
    localInteger = Integer.valueOf(69);
    localObject1 = ((ContentProviderOperation.Builder)localObject1).withValue("status", localInteger);
    int i5 = t + paramBoolean;
    Object localObject3 = Integer.valueOf(i5);
    localObject3 = ((ContentProviderOperation.Builder)localObject1).withValue("retry_count", localObject3);
    localObject1 = "retry_date";
    Long localLong = Long.valueOf(paramLong);
    localObject3 = ((ContentProviderOperation.Builder)localObject3).withValue((String)localObject1, localLong).build();
    ((ArrayList)localObject2).add(localObject3);
    localObject3 = (m)f.get();
    i3 = j;
    localObject3 = ((m)localObject3).b(i3);
    if (localObject3 != null)
    {
      localObject2 = a((ArrayList)localObject2);
      int i2 = localObject2.length;
      if (i2 != 0)
      {
        ((l)localObject3).b(paramLong);
        Message.a locala = paramMessage.m();
        int i6 = t + i4;
        u = i6;
        return com.truecaller.androidactors.w.b(locala.b());
      }
    }
    return com.truecaller.androidactors.w.b(null);
  }
  
  public final com.truecaller.androidactors.w a(Message paramMessage, Participant[] paramArrayOfParticipant, int paramInt)
  {
    int i1 = j;
    boolean bool2 = true;
    int i4 = 3;
    ArrayList localArrayList = null;
    if (i1 == i4)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      localObject1 = null;
    }
    Object localObject2 = new String[0];
    AssertionUtil.isTrue(i1, (String[])localObject2);
    int i2 = f;
    if (i2 != i4) {
      bool2 = false;
    }
    Object localObject1 = new String[0];
    AssertionUtil.isTrue(bool2, (String[])localObject1);
    boolean bool1 = paramMessage.c();
    bool2 = false;
    if (!bool1)
    {
      AssertionUtil.OnlyInDebug.fail(new String[] { "Can not schedule empty message for sending" });
      return com.truecaller.androidactors.w.b(null);
    }
    paramArrayOfParticipant = a(paramMessage, paramArrayOfParticipant);
    if (paramArrayOfParticipant == null) {
      return com.truecaller.androidactors.w.b(null);
    }
    localObject1 = ((m)f.get()).a(paramInt);
    localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    localObject2 = ContentProviderOperation.newAssertQuery(TruecallerContract.aa.a(a));
    Object localObject3 = Integer.valueOf(i4);
    ((ContentProviderOperation.Builder)localObject2).withValue("status", localObject3);
    localObject3 = ((ContentProviderOperation.Builder)localObject2).build();
    localArrayList.add(localObject3);
    localObject3 = ContentProviderOperation.newUpdate(TruecallerContract.aa.a(a));
    int i5 = 17;
    Integer localInteger = Integer.valueOf(i5);
    ((ContentProviderOperation.Builder)localObject3).withValue("status", localInteger);
    Object localObject4 = Integer.valueOf(paramInt);
    ((ContentProviderOperation.Builder)localObject3).withValue("scheduled_transport", localObject4);
    long l1 = d.a;
    localObject2 = Long.valueOf(((l)localObject1).a(l1));
    ((ContentProviderOperation.Builder)localObject3).withValue("date_sent", localObject2);
    l1 = e.a;
    l1 = ((l)localObject1).a(l1);
    localObject2 = Long.valueOf(l1);
    ((ContentProviderOperation.Builder)localObject3).withValue("date", localObject2);
    localObject2 = l;
    localObject1 = ((l)localObject1).a((String)localObject2);
    ((ContentProviderOperation.Builder)localObject3).withValue("sim_token", localObject1);
    int i3 = q;
    localObject1 = Integer.valueOf(i3);
    ((ContentProviderOperation.Builder)localObject3).withValue("category", localObject1);
    localObject1 = Boolean.FALSE;
    ((ContentProviderOperation.Builder)localObject3).withValue("seen", localObject1);
    localObject1 = Boolean.FALSE;
    ((ContentProviderOperation.Builder)localObject3).withValue("read", localObject1);
    localObject4 = "analytics_id";
    paramMessage = o;
    ((ContentProviderOperation.Builder)localObject3).withValue((String)localObject4, paramMessage);
    paramMessage = ((ContentProviderOperation.Builder)localObject3).build();
    localArrayList.add(paramMessage);
    paramMessage = a(localArrayList);
    int i6 = paramMessage.length;
    if (i6 == 0) {
      return com.truecaller.androidactors.w.b(null);
    }
    paramMessage = u;
    long l2 = a;
    return paramMessage.b(l2);
  }
  
  public final com.truecaller.androidactors.w a(String paramString)
  {
    return u.b(paramString);
  }
  
  /* Error */
  public final com.truecaller.androidactors.w a(org.a.a.b paramb)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_1
    //   3: ifnonnull +15 -> 18
    //   6: ldc_w 1265
    //   9: astore_1
    //   10: aload_1
    //   11: astore_3
    //   12: aconst_null
    //   13: astore 4
    //   15: goto +48 -> 63
    //   18: ldc_w 1267
    //   21: astore 5
    //   23: iconst_1
    //   24: istore 6
    //   26: iload 6
    //   28: anewarray 153	java/lang/String
    //   31: astore 7
    //   33: iconst_0
    //   34: istore 8
    //   36: aconst_null
    //   37: astore 9
    //   39: aload_1
    //   40: getfield 218	org/a/a/a/g:a	J
    //   43: lstore 10
    //   45: lload 10
    //   47: invokestatic 221	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   50: astore_1
    //   51: aload 7
    //   53: iconst_0
    //   54: aload_1
    //   55: aastore
    //   56: aload 5
    //   58: astore_3
    //   59: aload 7
    //   61: astore 4
    //   63: aload_0
    //   64: getfield 104	com/truecaller/messaging/data/v:l	Lcom/truecaller/messaging/data/c;
    //   67: astore_1
    //   68: aload_0
    //   69: getfield 85	com/truecaller/messaging/data/v:c	Landroid/content/ContentResolver;
    //   72: astore 12
    //   74: invokestatic 1270	com/truecaller/content/TruecallerContract$ab:a	()Landroid/net/Uri;
    //   77: astore 13
    //   79: ldc_w 1272
    //   82: astore 14
    //   84: aload 12
    //   86: aload 13
    //   88: aconst_null
    //   89: aload_3
    //   90: aload 4
    //   92: aload 14
    //   94: invokevirtual 250	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   97: astore 5
    //   99: aload_1
    //   100: aload 5
    //   102: invokeinterface 1275 2 0
    //   107: astore_2
    //   108: aload_2
    //   109: ifnonnull +18 -> 127
    //   112: getstatic 1258	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   115: astore_1
    //   116: aload_1
    //   117: invokestatic 1087	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   120: astore_1
    //   121: aload_2
    //   122: invokestatic 263	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   125: aload_1
    //   126: areturn
    //   127: invokestatic 419	org/a/a/b:ay_	()Lorg/a/a/b;
    //   130: astore_1
    //   131: new 146	java/util/ArrayList
    //   134: astore 5
    //   136: aload 5
    //   138: invokespecial 147	java/util/ArrayList:<init>	()V
    //   141: new 149	java/util/HashSet
    //   144: astore 7
    //   146: aload 7
    //   148: invokespecial 150	java/util/HashSet:<init>	()V
    //   151: aload_2
    //   152: invokeinterface 1278 1 0
    //   157: istore 8
    //   159: iload 8
    //   161: ifeq +99 -> 260
    //   164: aload_2
    //   165: invokeinterface 1279 1 0
    //   170: astore 9
    //   172: aload 9
    //   174: getfield 856	com/truecaller/messaging/data/types/Message:k	I
    //   177: istore 15
    //   179: aload_0
    //   180: aload 9
    //   182: iload 15
    //   184: aload 5
    //   186: invokespecial 1282	com/truecaller/messaging/data/v:a	(Lcom/truecaller/messaging/data/types/Message;ILjava/util/List;)Lorg/a/a/b;
    //   189: astore 12
    //   191: aload_0
    //   192: getfield 100	com/truecaller/messaging/data/v:f	Ldagger/a;
    //   195: astore 13
    //   197: aload 13
    //   199: invokeinterface 391 1 0
    //   204: astore 13
    //   206: aload 13
    //   208: checkcast 393	com/truecaller/messaging/transport/m
    //   211: astore 13
    //   213: aload 9
    //   215: getfield 856	com/truecaller/messaging/data/types/Message:k	I
    //   218: istore 8
    //   220: aload 13
    //   222: iload 8
    //   224: invokeinterface 777 2 0
    //   229: astore 9
    //   231: aload 7
    //   233: aload 9
    //   235: invokeinterface 467 2 0
    //   240: pop
    //   241: aload 12
    //   243: aload_1
    //   244: invokevirtual 355	org/a/a/b:c	(Lorg/a/a/x;)Z
    //   247: istore 8
    //   249: iload 8
    //   251: ifeq -100 -> 151
    //   254: aload 12
    //   256: astore_1
    //   257: goto -106 -> 151
    //   260: aload 5
    //   262: invokevirtual 313	java/util/ArrayList:isEmpty	()Z
    //   265: istore 8
    //   267: iload 8
    //   269: ifeq +18 -> 287
    //   272: getstatic 1285	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   275: astore_1
    //   276: aload_1
    //   277: invokestatic 1087	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   280: astore_1
    //   281: aload_2
    //   282: invokestatic 263	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   285: aload_1
    //   286: areturn
    //   287: aload_0
    //   288: aload 5
    //   290: invokespecial 310	com/truecaller/messaging/data/v:a	(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    //   293: astore 5
    //   295: aload 5
    //   297: arraylength
    //   298: istore 16
    //   300: iload 16
    //   302: ifne +18 -> 320
    //   305: getstatic 1258	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   308: astore_1
    //   309: aload_1
    //   310: invokestatic 1087	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   313: astore_1
    //   314: aload_2
    //   315: invokestatic 263	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   318: aload_1
    //   319: areturn
    //   320: invokestatic 419	org/a/a/b:ay_	()Lorg/a/a/b;
    //   323: astore 5
    //   325: iconst_2
    //   326: istore 8
    //   328: aload_0
    //   329: aload 5
    //   331: aload_1
    //   332: iload 8
    //   334: aload 7
    //   336: invokespecial 921	com/truecaller/messaging/data/v:a	(Lorg/a/a/b;Lorg/a/a/b;ILjava/lang/Iterable;)V
    //   339: getstatic 1285	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   342: astore_1
    //   343: aload_1
    //   344: invokestatic 1087	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   347: astore_1
    //   348: aload_2
    //   349: invokestatic 263	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   352: aload_1
    //   353: areturn
    //   354: astore_1
    //   355: goto +19 -> 374
    //   358: astore_1
    //   359: aload_1
    //   360: invokestatic 765	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   363: aload_2
    //   364: invokestatic 263	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   367: getstatic 1258	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   370: invokestatic 1087	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   373: areturn
    //   374: aload_2
    //   375: invokestatic 263	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   378: aload_1
    //   379: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	380	0	this	v
    //   0	380	1	paramb	org.a.a.b
    //   1	374	2	localj	j
    //   11	79	3	localObject1	Object
    //   13	78	4	localObject2	Object
    //   21	309	5	localObject3	Object
    //   24	3	6	i1	int
    //   31	304	7	localObject4	Object
    //   34	126	8	bool1	boolean
    //   218	5	8	i2	int
    //   247	21	8	bool2	boolean
    //   326	7	8	i3	int
    //   37	197	9	localObject5	Object
    //   43	3	10	l1	long
    //   72	183	12	localObject6	Object
    //   77	144	13	localObject7	Object
    //   82	11	14	str	String
    //   177	6	15	i4	int
    //   298	3	16	i5	int
    // Exception table:
    //   from	to	target	type
    //   26	31	354	finally
    //   39	43	354	finally
    //   45	50	354	finally
    //   54	56	354	finally
    //   63	67	354	finally
    //   68	72	354	finally
    //   74	77	354	finally
    //   92	97	354	finally
    //   100	107	354	finally
    //   112	115	354	finally
    //   116	120	354	finally
    //   127	130	354	finally
    //   131	134	354	finally
    //   136	141	354	finally
    //   141	144	354	finally
    //   146	151	354	finally
    //   151	157	354	finally
    //   164	170	354	finally
    //   172	177	354	finally
    //   184	189	354	finally
    //   191	195	354	finally
    //   197	204	354	finally
    //   206	211	354	finally
    //   213	218	354	finally
    //   222	229	354	finally
    //   233	241	354	finally
    //   243	247	354	finally
    //   260	265	354	finally
    //   272	275	354	finally
    //   276	280	354	finally
    //   288	293	354	finally
    //   295	298	354	finally
    //   305	308	354	finally
    //   309	313	354	finally
    //   320	323	354	finally
    //   334	339	354	finally
    //   339	342	354	finally
    //   343	347	354	finally
    //   359	363	354	finally
    //   26	31	358	java/lang/RuntimeException
    //   39	43	358	java/lang/RuntimeException
    //   45	50	358	java/lang/RuntimeException
    //   54	56	358	java/lang/RuntimeException
    //   63	67	358	java/lang/RuntimeException
    //   68	72	358	java/lang/RuntimeException
    //   74	77	358	java/lang/RuntimeException
    //   92	97	358	java/lang/RuntimeException
    //   100	107	358	java/lang/RuntimeException
    //   112	115	358	java/lang/RuntimeException
    //   116	120	358	java/lang/RuntimeException
    //   127	130	358	java/lang/RuntimeException
    //   131	134	358	java/lang/RuntimeException
    //   136	141	358	java/lang/RuntimeException
    //   141	144	358	java/lang/RuntimeException
    //   146	151	358	java/lang/RuntimeException
    //   151	157	358	java/lang/RuntimeException
    //   164	170	358	java/lang/RuntimeException
    //   172	177	358	java/lang/RuntimeException
    //   184	189	358	java/lang/RuntimeException
    //   191	195	358	java/lang/RuntimeException
    //   197	204	358	java/lang/RuntimeException
    //   206	211	358	java/lang/RuntimeException
    //   213	218	358	java/lang/RuntimeException
    //   222	229	358	java/lang/RuntimeException
    //   233	241	358	java/lang/RuntimeException
    //   243	247	358	java/lang/RuntimeException
    //   260	265	358	java/lang/RuntimeException
    //   272	275	358	java/lang/RuntimeException
    //   276	280	358	java/lang/RuntimeException
    //   288	293	358	java/lang/RuntimeException
    //   295	298	358	java/lang/RuntimeException
    //   305	308	358	java/lang/RuntimeException
    //   309	313	358	java/lang/RuntimeException
    //   320	323	358	java/lang/RuntimeException
    //   334	339	358	java/lang/RuntimeException
    //   339	342	358	java/lang/RuntimeException
    //   343	347	358	java/lang/RuntimeException
  }
  
  public final com.truecaller.androidactors.w a(boolean paramBoolean, long... paramVarArgs)
  {
    paramVarArgs = d(paramVarArgs);
    return com.truecaller.androidactors.w.b(b(paramVarArgs, paramBoolean));
  }
  
  public final com.truecaller.androidactors.w a(long[] paramArrayOfLong)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("conversation_id");
    localStringBuilder.append(" IN (");
    long l1 = paramArrayOfLong[0];
    localStringBuilder.append(l1);
    int i1 = 1;
    for (;;)
    {
      int i2 = paramArrayOfLong.length;
      if (i1 >= i2) {
        break;
      }
      i2 = 44;
      localStringBuilder.append(i2);
      long l2 = paramArrayOfLong[i1];
      localStringBuilder.append(l2);
      i1 += 1;
    }
    localStringBuilder.append(')');
    paramArrayOfLong = localStringBuilder.toString();
    return com.truecaller.androidactors.w.b(a(paramArrayOfLong, false));
  }
  
  public final com.truecaller.androidactors.w a(Conversation[] paramArrayOfConversation)
  {
    SparseBooleanArray localSparseBooleanArray = new android/util/SparseBooleanArray;
    localSparseBooleanArray.<init>();
    -..Lambda.v.bXMuDjRgAMAvY_MYIfd8FzIwVZw localbXMuDjRgAMAvY_MYIfd8FzIwVZw = new com/truecaller/messaging/data/-$$Lambda$v$bXMuDjRgAMAvY_MYIfd8FzIwVZw;
    localbXMuDjRgAMAvY_MYIfd8FzIwVZw.<init>(this, localSparseBooleanArray);
    a(paramArrayOfConversation, localbXMuDjRgAMAvY_MYIfd8FzIwVZw);
    return com.truecaller.androidactors.w.b(localSparseBooleanArray);
  }
  
  public final com.truecaller.androidactors.w a(Conversation[] paramArrayOfConversation, boolean paramBoolean)
  {
    SparseBooleanArray localSparseBooleanArray = new android/util/SparseBooleanArray;
    localSparseBooleanArray.<init>();
    -..Lambda.v.WCdhtL09afn-oOKkG6hWsnBLqKw localWCdhtL09afn-oOKkG6hWsnBLqKw = new com/truecaller/messaging/data/-$$Lambda$v$WCdhtL09afn-oOKkG6hWsnBLqKw;
    localWCdhtL09afn-oOKkG6hWsnBLqKw.<init>(this, localSparseBooleanArray, paramBoolean);
    a(paramArrayOfConversation, localWCdhtL09afn-oOKkG6hWsnBLqKw);
    return com.truecaller.androidactors.w.b(localSparseBooleanArray);
  }
  
  public final void a()
  {
    Object localObject1 = c;
    Object localObject2 = TruecallerContract.aa.a();
    String[] arrayOfString = { "_id" };
    Object localObject4 = "classification = 0 AND (status & 1) = 0";
    String str = "_id LIMIT 1";
    localObject1 = ((ContentResolver)localObject1).query((Uri)localObject2, arrayOfString, (String)localObject4, null, str);
    if (localObject1 != null) {
      try
      {
        int i1 = ((Cursor)localObject1).getCount();
        if (i1 > 0)
        {
          localObject2 = new java/util/HashSet;
          ((HashSet)localObject2).<init>();
          arrayOfString = null;
          localObject4 = Integer.valueOf(0);
          ((Set)localObject2).add(localObject4);
          int i2 = 4;
          localObject4 = Integer.valueOf(i2);
          ((Set)localObject2).add(localObject4);
          a(false, (Set)localObject2);
        }
        return;
      }
      finally
      {
        ((Cursor)localObject1).close();
      }
    }
  }
  
  public final void a(int paramInt, org.a.a.b paramb)
  {
    Object localObject1 = l;
    Object localObject2 = c;
    Object localObject3 = TruecallerContract.ab.a();
    String str1 = "(transport = ? OR scheduled_transport = ?) AND (status & 1) != 0 AND (status & 36) != 0";
    String[] arrayOfString = new String[2];
    String str2 = String.valueOf(paramInt);
    arrayOfString[0] = str2;
    Object localObject4 = String.valueOf(paramInt);
    arrayOfString[1] = localObject4;
    String str3 = "date_sent LIMIT 1";
    str2 = null;
    localObject4 = ((ContentResolver)localObject2).query((Uri)localObject3, null, str1, arrayOfString, str3);
    localObject4 = ((c)localObject1).b((Cursor)localObject4);
    if (localObject4 == null) {
      return;
    }
    try
    {
      boolean bool1 = ((j)localObject4).moveToFirst();
      if (!bool1) {
        return;
      }
      localObject1 = ((j)localObject4).b();
      int i1 = f & 0x20;
      if (i1 != 0) {
        return;
      }
      i1 = f & 0x40;
      if (i1 != 0)
      {
        localObject2 = u;
        boolean bool2 = ((org.a.a.b)localObject2).b(paramb);
        if (bool2) {
          return;
        }
      }
      paramb = new android/content/ContentValues;
      paramb.<init>();
      localObject2 = "status";
      int i3 = 33;
      localObject3 = Integer.valueOf(i3);
      paramb.put((String)localObject2, (Integer)localObject3);
      localObject2 = c;
      long l1 = a;
      localObject3 = TruecallerContract.aa.a(l1);
      str2 = null;
      int i2 = ((ContentResolver)localObject2).update((Uri)localObject3, paramb, null, null);
      if (i2 == 0) {
        return;
      }
      paramb = g;
      paramb = paramb.get();
      paramb = (com.truecaller.messaging.transport.d)paramb;
      paramb.b((Message)localObject1);
      return;
    }
    finally
    {
      ((j)localObject4).close();
    }
  }
  
  public final void a(int paramInt, org.a.a.b paramb, boolean paramBoolean)
  {
    ((m)f.get()).a(paramInt).a(paramb);
    Set localSet = Collections.singleton(Integer.valueOf(paramInt));
    b(paramBoolean, localSet);
  }
  
  public final void a(long paramLong, int paramInt)
  {
    String str = String.valueOf(paramLong);
    str = "_id = ".concat(str);
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>(1);
    Object localObject = Integer.valueOf(paramInt);
    localContentValues.put("preferred_transport", (Integer)localObject);
    localObject = c;
    Uri localUri = TruecallerContract.i.a();
    ((ContentResolver)localObject).update(localUri, localContentValues, str, null);
  }
  
  public final void a(long paramLong, int paramInt1, int paramInt2)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("conversation_id = ");
    localStringBuilder.append(paramLong);
    localStringBuilder.append(" ");
    String str = com.truecaller.content.c.a(x, paramInt1, paramInt2);
    localStringBuilder.append(str);
    str = localStringBuilder.toString();
    a(str, false);
  }
  
  public final void a(t.a parama, int paramInt, Iterable paramIterable)
  {
    v localv = this;
    Object localObject1 = parama;
    Object localObject5 = paramIterable;
    Object localObject6 = s;
    Object localObject7 = { "You can't trigger sync without setting its bounds" };
    AssertionUtil.AlwaysFatal.isNotNull(localObject6, (String[])localObject7);
    new String[1][0] = "Process next batch";
    localObject6 = t;
    localObject7 = "MessagesStorage.performBatch";
    localObject6 = ((com.truecaller.utils.s)localObject6).a((String)localObject7);
    try
    {
      localObject7 = s;
      localObject7 = b;
      Object localObject8 = s;
      localObject8 = c;
      boolean bool1 = parama.a((org.a.a.b)localObject7, (org.a.a.b)localObject8);
      int i1;
      if (bool1)
      {
        localObject7 = s;
        i1 = a;
        i1 = paramInt | i1;
      }
      else
      {
        i1 = paramInt;
      }
      int i2 = localv.a((t.a)localObject1, (com.truecaller.utils.q)localObject6, (Iterable)localObject5);
      long l1 = 0L;
      boolean bool5 = false;
      String str1 = null;
      Object localObject9 = null;
      int i6 = 1;
      Object localObject10;
      Object localObject11;
      switch (i2)
      {
      default: 
        break;
      case 2: 
        localObject1 = "Batch sync failed";
        new String[1][0] = localObject1;
        s = null;
        return;
      case 1: 
        localObject8 = k;
        long l2 = ((h)localObject8).a();
        localObject8 = parama.d();
        boolean bool3 = ((org.a.a.b)localObject8).c(l2);
        if (bool3)
        {
          bool3 = d();
          if (!bool3)
          {
            bool3 = true;
          }
          else
          {
            bool3 = false;
            localObject8 = null;
          }
          localObject10 = k;
          localObject11 = parama.d();
          long l3 = a;
          ((h)localObject10).a(l3);
          if (bool3)
          {
            localObject8 = o;
            ((com.truecaller.util.al)localObject8).j();
          }
        }
        localObject8 = parama.c();
        bool3 = ((org.a.a.b)localObject8).e(l1);
        if (bool3)
        {
          localObject8 = y;
          ((bk)localObject8).b();
        }
        s = null;
        localObject8 = f;
        localObject8 = ((dagger.a)localObject8).get();
        localObject8 = (m)localObject8;
        localObject8 = ((m)localObject8).a();
        localObject8 = ((Iterable)localObject8).iterator();
        int i7;
        for (;;)
        {
          boolean bool6 = ((Iterator)localObject8).hasNext();
          if (!bool6) {
            break;
          }
          localObject10 = ((Iterator)localObject8).next();
          localObject10 = (l)localObject10;
          boolean bool8 = ((l)localObject10).e();
          if (bool8)
          {
            i7 = ((l)localObject10).f();
            localObject11 = org.a.a.b.ay_();
            localv.a(i7, (org.a.a.b)localObject11);
          }
        }
        localObject5 = paramIterable.iterator();
        for (;;)
        {
          bool3 = ((Iterator)localObject5).hasNext();
          if (!bool3) {
            break;
          }
          localObject8 = ((Iterator)localObject5).next();
          localObject8 = (l)localObject8;
          int i3 = ((l)localObject8).f();
          i7 = 5;
          if (i3 == i7)
          {
            localObject8 = C;
            localObject8 = ((dagger.a)localObject8).get();
            localObject8 = (com.truecaller.messaging.h.c)localObject8;
            boolean bool7 = parama.f();
            localObject8 = b;
            ((h)localObject8).j(bool7);
          }
        }
        localObject1 = "Batch sync finished";
        new String[1][0] = localObject1;
        break;
      case 0: 
        localObject8 = n;
        ((ae)localObject8).a((t.a)localObject1, i1, (Iterable)localObject5);
        localObject5 = new String[i6];
        localObject8 = "Schedule next batch: ";
        localObject1 = String.valueOf(parama);
        localObject1 = ((String)localObject8).concat((String)localObject1);
        localObject5[0] = localObject1;
      }
      int i8 = i1 & 0x1;
      if (i8 == i6) {
        bool5 = true;
      }
      if (bool5)
      {
        localObject1 = f;
        localObject1 = ((dagger.a)localObject1).get();
        localObject1 = (m)localObject1;
        localObject5 = ((m)localObject1).b();
        boolean bool2 = ((List)localObject5).isEmpty();
        if (!bool2) {
          try
          {
            localObject10 = c;
            localObject11 = TruecallerContract.ab.a();
            localObject7 = "seen=0";
            localObject8 = x;
            localObject8 = ((com.truecaller.featuretoggles.e)localObject8).w();
            boolean bool4 = ((com.truecaller.featuretoggles.b)localObject8).a();
            if (bool4)
            {
              localObject8 = new java/lang/StringBuilder;
              ((StringBuilder)localObject8).<init>();
              ((StringBuilder)localObject8).append((String)localObject7);
              localObject7 = " AND category!=3";
              ((StringBuilder)localObject8).append((String)localObject7);
              localObject7 = ((StringBuilder)localObject8).toString();
            }
            localObject8 = k;
            long l4 = ((h)localObject8).c();
            int i4 = 2;
            bool5 = l4 < l1;
            Object localObject12;
            int i9;
            Object localObject14;
            if (bool5)
            {
              localObject12 = new java/util/ArrayList;
              ((ArrayList)localObject12).<init>();
              localObject5 = ((List)localObject5).iterator();
              for (;;)
              {
                boolean bool10 = ((Iterator)localObject5).hasNext();
                if (!bool10) {
                  break;
                }
                Object localObject13 = ((Iterator)localObject5).next();
                localObject13 = (Integer)localObject13;
                str1 = " (transport=";
                String str2 = String.valueOf(localObject13);
                str1 = str1.concat(str2);
                i9 = ((Integer)localObject13).intValue();
                if (i9 != i4)
                {
                  localObject13 = new java/lang/StringBuilder;
                  ((StringBuilder)localObject13).<init>();
                  ((StringBuilder)localObject13).append(str1);
                  str1 = " AND date> ";
                  ((StringBuilder)localObject13).append(str1);
                  ((StringBuilder)localObject13).append(l4);
                  str1 = ((StringBuilder)localObject13).toString();
                }
                localObject13 = new java/lang/StringBuilder;
                ((StringBuilder)localObject13).<init>();
                ((StringBuilder)localObject13).append(str1);
                str1 = " )";
                ((StringBuilder)localObject13).append(str1);
                localObject13 = ((StringBuilder)localObject13).toString();
                ((List)localObject12).add(localObject13);
                i6 = 1;
              }
              localObject5 = new java/lang/StringBuilder;
              ((StringBuilder)localObject5).<init>();
              ((StringBuilder)localObject5).append((String)localObject7);
              localObject7 = " AND (";
              ((StringBuilder)localObject5).append((String)localObject7);
              localObject7 = " OR ";
              localObject7 = k.a((Iterable)localObject12, (String)localObject7);
              ((StringBuilder)localObject5).append((String)localObject7);
              localObject7 = ")";
              ((StringBuilder)localObject5).append((String)localObject7);
              localObject5 = ((StringBuilder)localObject5).toString();
              localObject14 = localObject5;
            }
            else
            {
              localObject12 = new java/lang/StringBuilder;
              ((StringBuilder)localObject12).<init>();
              ((StringBuilder)localObject12).append((String)localObject7);
              localObject7 = " AND transport IN (";
              ((StringBuilder)localObject12).append((String)localObject7);
              localObject7 = ",";
              localObject5 = k.a((Iterable)localObject5, (String)localObject7);
              ((StringBuilder)localObject12).append((String)localObject5);
              localObject5 = ")";
              ((StringBuilder)localObject12).append((String)localObject5);
              localObject5 = ((StringBuilder)localObject12).toString();
              localObject14 = localObject5;
            }
            String str3 = "date DESC LIMIT 25";
            localObject5 = ((ContentResolver)localObject10).query((Uri)localObject11, null, (String)localObject14, null, str3);
            if (localObject5 == null) {}
            for (;;)
            {
              com.truecaller.util.q.a((Cursor)localObject9);
              break;
              localObject7 = new java/util/ArrayList;
              ((ArrayList)localObject7).<init>();
              localObject12 = l;
              localObject9 = ((c)localObject12).b((Cursor)localObject5);
              for (;;)
              {
                boolean bool11 = ((j)localObject9).moveToNext();
                if (!bool11) {
                  break;
                }
                localObject5 = ((j)localObject9).b();
                int i10 = f;
                i9 = 1;
                i10 &= i9;
                if (i10 == 0)
                {
                  i10 = ((m)localObject1).c((Message)localObject5);
                  if (i10 != i4) {
                    ((List)localObject7).add(localObject5);
                  }
                }
                else
                {
                  i10 = f;
                  int i5 = 8;
                  i10 &= i5;
                  if (i10 == i5)
                  {
                    long l5 = a;
                    localv.b(l5);
                    localObject12 = j;
                    localObject12 = ((com.truecaller.androidactors.f)localObject12).a();
                    localObject12 = (com.truecaller.messaging.notifications.a)localObject12;
                    ((com.truecaller.messaging.notifications.a)localObject12).b((Message)localObject5);
                  }
                }
              }
              localObject1 = j;
              localObject1 = ((com.truecaller.androidactors.f)localObject1).a();
              localObject1 = (com.truecaller.messaging.notifications.a)localObject1;
              localObject5 = D;
              localObject5 = ((x)localObject5).a((List)localObject7);
              ((com.truecaller.messaging.notifications.a)localObject1).a((Map)localObject5);
              localObject1 = B;
              boolean bool9 = ((al)localObject1).e();
              if (!bool9)
              {
                localObject1 = B;
                ((al)localObject1).d();
              }
            }
            localObject3 = v;
          }
          finally
          {
            com.truecaller.util.q.a((Cursor)localObject9);
          }
        }
        Object localObject3 = ((com.truecaller.androidactors.f)localObject3).a();
        localObject3 = (as)localObject3;
        ((as)localObject3).a();
      }
      return;
    }
    finally
    {
      ((com.truecaller.utils.q)localObject6).a();
    }
  }
  
  public final void a(Message paramMessage)
  {
    paramMessage = paramMessage.m();
    long l1 = System.currentTimeMillis();
    paramMessage = paramMessage.c(l1).b();
    f(paramMessage);
  }
  
  public final void a(Message paramMessage, String paramString1, String paramString2)
  {
    Object localObject = (m)f.get();
    int i1 = j;
    localObject = ((m)localObject).b(i1);
    if (localObject == null)
    {
      paramString1 = new String[1];
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("Unknown transport: ");
      int i2 = j;
      ((StringBuilder)localObject).append(i2);
      paramMessage = ((StringBuilder)localObject).toString();
      paramString1[0] = paramMessage;
      return;
    }
    ((l)localObject).a(paramMessage, paramString1, paramString2);
  }
  
  public final void a(boolean paramBoolean)
  {
    Iterable localIterable = ((m)f.get()).a();
    a(paramBoolean, localIterable);
  }
  
  public final void a(boolean paramBoolean, Set paramSet)
  {
    paramSet = a(paramSet);
    a(paramBoolean, paramSet);
  }
  
  public final void a(Message[] paramArrayOfMessage, int paramInt)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    int i1 = 1;
    int i2;
    if (paramInt == i1) {
      i2 = 3;
    } else {
      i2 = 2;
    }
    int i3 = paramArrayOfMessage.length;
    int i4 = 0;
    while (i4 < i3)
    {
      Message localMessage = paramArrayOfMessage[i4];
      long l1 = a;
      Object localObject = ContentProviderOperation.newUpdate(TruecallerContract.aa.a(l1));
      Integer localInteger = Integer.valueOf(i2);
      ((ContentProviderOperation.Builder)localObject).withValue("category", localInteger);
      String str = "classification";
      localInteger = Integer.valueOf(i1);
      ((ContentProviderOperation.Builder)localObject).withValue(str, localInteger);
      localObject = ((ContentProviderOperation.Builder)localObject).build();
      localArrayList.add(localObject);
      localObject = z;
      int i5 = q;
      ((com.truecaller.messaging.categorizer.b)localObject).a(localMessage, i5, paramInt);
      i4 += 1;
    }
    paramArrayOfMessage = a(localArrayList);
    int i6 = paramArrayOfMessage.length;
    if (i6 == 0)
    {
      paramArrayOfMessage = "Could not update message category";
      new String[1][0] = paramArrayOfMessage;
    }
  }
  
  public final com.truecaller.androidactors.w b(org.a.a.b paramb)
  {
    return u.a(paramb);
  }
  
  public final void b()
  {
    Object localObject1 = c;
    Object localObject2 = TruecallerContract.aa.a();
    String[] tmp13_10 = new String[3];
    String[] tmp14_13 = tmp13_10;
    String[] tmp14_13 = tmp13_10;
    tmp14_13[0] = "_id";
    tmp14_13[1] = "read";
    tmp14_13[2] = "seen";
    Object localObject4 = tmp14_13;
    Object localObject5 = "sync_status = 1";
    int i1 = 0;
    localObject1 = ((ContentResolver)localObject1).query((Uri)localObject2, (String[])localObject4, (String)localObject5, null, null);
    if (localObject1 != null) {
      try
      {
        localObject2 = new java/util/ArrayList;
        ((ArrayList)localObject2).<init>();
        localObject4 = new java/util/ArrayList;
        ((ArrayList)localObject4).<init>();
        for (;;)
        {
          bool1 = ((Cursor)localObject1).moveToNext();
          i1 = 1;
          if (!bool1) {
            break;
          }
          bool1 = false;
          localObject5 = null;
          long l1 = ((Cursor)localObject1).getLong(0);
          int i2 = ((Cursor)localObject1).getInt(i1);
          int i3;
          if (i2 == i1) {
            i3 = 1;
          } else {
            i3 = 0;
          }
          int i4 = 2;
          i4 = ((Cursor)localObject1).getInt(i4);
          if (i4 == i1) {
            bool1 = true;
          }
          if (i3 != 0)
          {
            localObject5 = Long.valueOf(l1);
            ((List)localObject2).add(localObject5);
          }
          else if (bool1)
          {
            localObject5 = Long.valueOf(l1);
            ((List)localObject4).add(localObject5);
          }
        }
        boolean bool1 = ((List)localObject2).isEmpty();
        if (!bool1)
        {
          localObject2 = c((List)localObject2);
          localObject2 = d((long[])localObject2);
          a((String)localObject2, i1);
        }
        boolean bool2 = ((List)localObject4).isEmpty();
        if (!bool2)
        {
          localObject2 = c((List)localObject4);
          localObject2 = d((long[])localObject2);
          b((String)localObject2);
        }
        return;
      }
      finally
      {
        ((Cursor)localObject1).close();
      }
    }
  }
  
  public final void b(int paramInt)
  {
    l locall = ((m)f.get()).b(paramInt);
    if (locall == null) {
      return;
    }
    Object localObject1 = l;
    Object localObject2 = c;
    Object localObject3 = TruecallerContract.ab.a();
    boolean bool1 = false;
    Message localMessage = null;
    Object localObject4 = "transport = ? AND (status & 33) = 33";
    int i1 = 1;
    String[] arrayOfString = new String[i1];
    String str = String.valueOf(paramInt);
    arrayOfString[0] = str;
    str = null;
    localObject2 = ((ContentResolver)localObject2).query((Uri)localObject3, null, (String)localObject4, arrayOfString, null);
    localObject1 = ((c)localObject1).b((Cursor)localObject2);
    if (localObject1 == null) {
      return;
    }
    localObject2 = locall.b();
    localObject3 = a;
    try
    {
      for (;;)
      {
        bool1 = ((j)localObject1).moveToNext();
        if (!bool1) {
          break;
        }
        localMessage = ((j)localObject1).b();
        locall.a(localMessage, (ad)localObject2);
        localObject4 = e;
        boolean bool2 = ((org.a.a.b)localObject4).c((org.a.a.x)localObject3);
        if (bool2) {
          localObject3 = e;
        }
      }
      ((j)localObject1).close();
      boolean bool3 = locall.b((ad)localObject2);
      if (bool3)
      {
        boolean bool4 = locall.a((ad)localObject2);
        if (bool4)
        {
          a(paramInt, (org.a.a.b)localObject3, false);
          return;
        }
      }
      return;
    }
    finally
    {
      ((j)localObject1).close();
    }
  }
  
  public final void b(long paramLong)
  {
    String str = String.valueOf(paramLong);
    str = "message_id=".concat(str);
    b(str);
  }
  
  public final void b(Message paramMessage)
  {
    f(paramMessage);
  }
  
  public final void b(boolean paramBoolean)
  {
    Iterable localIterable = ((m)f.get()).a();
    b(paramBoolean, localIterable);
  }
  
  public final void b(boolean paramBoolean, Set paramSet)
  {
    paramSet = a(paramSet);
    b(paramBoolean, paramSet);
  }
  
  public final void b(long[] paramArrayOfLong)
  {
    paramArrayOfLong = d(paramArrayOfLong);
    b(paramArrayOfLong);
  }
  
  public final com.truecaller.androidactors.w c(long paramLong)
  {
    String str = String.valueOf(paramLong);
    str = "message_id=".concat(str);
    return com.truecaller.androidactors.w.b(b(str, false));
  }
  
  public final com.truecaller.androidactors.w c(Message paramMessage)
  {
    boolean bool1 = paramMessage.b();
    Object localObject1 = { "You can re-schedule only saved messages" };
    AssertionUtil.AlwaysFatal.isTrue(bool1, (String[])localObject1);
    int i1 = f & 0x8;
    int i4 = 0;
    localObject1 = null;
    int i5 = 1;
    if (i1 != 0)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      localObject2 = null;
    }
    Object localObject3 = { "You can re-schedule only failed messages" };
    AssertionUtil.AlwaysFatal.isTrue(i1, (String[])localObject3);
    int i2 = f & i5;
    if (i2 != 0)
    {
      i2 = 1;
    }
    else
    {
      i2 = 0;
      localObject2 = null;
    }
    localObject3 = new String[] { "You can re-schedule only outgoing messages" };
    AssertionUtil.AlwaysFatal.isTrue(i2, (String[])localObject3);
    int i3 = 0;
    Object localObject2 = null;
    try
    {
      localObject3 = l;
      ContentResolver localContentResolver = c;
      Uri localUri = TruecallerContract.ab.a();
      String str = "_id=?";
      String[] arrayOfString = new String[i5];
      long l1 = a;
      paramMessage = String.valueOf(l1);
      arrayOfString[0] = paramMessage;
      paramMessage = localContentResolver.query(localUri, null, str, arrayOfString, null);
      localObject2 = ((c)localObject3).b(paramMessage);
      if (localObject2 != null)
      {
        boolean bool3 = ((j)localObject2).moveToNext();
        if (bool3)
        {
          paramMessage = ((j)localObject2).b();
          com.truecaller.util.q.a((Cursor)localObject2);
          i3 = f;
          i4 = 9;
          i3 &= i4;
          if (i3 != i4) {
            return com.truecaller.androidactors.w.b(Boolean.FALSE);
          }
          i3 = j;
          i4 = 3;
          if (i3 == i4) {
            i3 = k;
          }
          localObject1 = new java/util/ArrayList;
          ((ArrayList)localObject1).<init>();
          paramMessage = a(paramMessage, i3, (List)localObject1);
          boolean bool2 = ((ArrayList)localObject1).isEmpty();
          if (bool2) {
            return com.truecaller.androidactors.w.b(Boolean.TRUE);
          }
          localObject1 = a((ArrayList)localObject1);
          i4 = localObject1.length;
          if (i4 == 0) {
            return com.truecaller.androidactors.w.b(Boolean.FALSE);
          }
          localObject1 = org.a.a.b.ay_();
          localObject2 = Collections.singleton(((m)f.get()).a(i3));
          a((org.a.a.b)localObject1, paramMessage, 2, (Iterable)localObject2);
          return com.truecaller.androidactors.w.b(Boolean.TRUE);
        }
      }
      paramMessage = Boolean.FALSE;
      paramMessage = com.truecaller.androidactors.w.b(paramMessage);
      return paramMessage;
    }
    finally
    {
      com.truecaller.util.q.a((Cursor)localObject2);
    }
  }
  
  public final void c(int paramInt)
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Object localObject2 = c;
    Object localObject3 = TruecallerContract.aa.a();
    Object localObject4 = { "_id", "date" };
    String str1 = "scheduled_transport =? AND (status & 17) = 17";
    int i1 = 1;
    Object localObject5 = new String[i1];
    Object localObject6 = String.valueOf(paramInt);
    localObject5[0] = localObject6;
    localObject6 = null;
    localObject2 = ((ContentResolver)localObject2).query((Uri)localObject3, (String[])localObject4, str1, (String[])localObject5, null);
    if (localObject2 == null) {
      return;
    }
    localObject3 = a;
    try
    {
      for (;;)
      {
        boolean bool = ((Cursor)localObject2).moveToNext();
        if (!bool) {
          break;
        }
        long l1 = ((Cursor)localObject2).getLong(0);
        localObject5 = new org/a/a/b;
        long l2 = ((Cursor)localObject2).getLong(i1);
        ((org.a.a.b)localObject5).<init>(l2);
        localObject6 = TruecallerContract.aa.a();
        localObject6 = ContentProviderOperation.newUpdate((Uri)localObject6);
        String str2 = "status";
        int i2 = 9;
        Object localObject7 = Integer.valueOf(i2);
        ((ContentProviderOperation.Builder)localObject6).withValue(str2, localObject7);
        str2 = "seen";
        localObject7 = Integer.valueOf(i1);
        ((ContentProviderOperation.Builder)localObject6).withValue(str2, localObject7);
        str2 = "_id=? ";
        localObject7 = new String[i1];
        localObject4 = String.valueOf(l1);
        localObject7[0] = localObject4;
        ((ContentProviderOperation.Builder)localObject6).withSelection(str2, (String[])localObject7);
        localObject4 = ((ContentProviderOperation.Builder)localObject6).build();
        ((ArrayList)localObject1).add(localObject4);
        bool = ((org.a.a.b)localObject5).c((org.a.a.x)localObject3);
        if (bool) {
          localObject3 = localObject5;
        }
      }
      ((Cursor)localObject2).close();
      localObject1 = a((ArrayList)localObject1);
      int i3 = localObject1.length;
      if (i3 != 0) {
        a(paramInt, (org.a.a.b)localObject3, false);
      }
      return;
    }
    finally
    {
      ((Cursor)localObject2).close();
    }
  }
  
  public final void c(long... paramVarArgs)
  {
    paramVarArgs = d(paramVarArgs);
    a(paramVarArgs, false);
  }
  
  public final com.truecaller.androidactors.w d(Message paramMessage)
  {
    boolean bool1 = paramMessage.b();
    Object localObject1 = { "You can update status only for stored messages" };
    AssertionUtil.AlwaysFatal.isTrue(bool1, (String[])localObject1);
    int i1 = j;
    int i3 = 3;
    int i5 = 0;
    int i6 = 1;
    if (i1 == i3)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      localObject2 = null;
    }
    Object localObject3 = { "You can't cancel failed message" };
    AssertionUtil.AlwaysFatal.isTrue(i1, (String[])localObject3);
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject3 = ContentProviderOperation.newDelete(TruecallerContract.aa.a());
    Object localObject4 = new String[i6];
    long l1 = b;
    String str = String.valueOf(l1);
    localObject4[0] = str;
    ((ContentProviderOperation.Builder)localObject3).withSelection("conversation_id=? AND (status & 2) == 2", (String[])localObject4);
    localObject3 = ((ContentProviderOperation.Builder)localObject3).build();
    ((ArrayList)localObject2).add(localObject3);
    localObject3 = ContentProviderOperation.newUpdate(TruecallerContract.aa.a());
    localObject1 = Integer.valueOf(i3);
    ((ContentProviderOperation.Builder)localObject3).withValue("status", localObject1);
    String[] arrayOfString = new String[i6];
    long l2 = a;
    localObject4 = String.valueOf(l2);
    arrayOfString[0] = localObject4;
    ((ContentProviderOperation.Builder)localObject3).withSelection("_id=? AND status = 17", arrayOfString);
    localObject1 = ((ContentProviderOperation.Builder)localObject3).build();
    ((ArrayList)localObject2).add(localObject1);
    localObject2 = a((ArrayList)localObject2);
    int i4 = localObject2.length;
    localObject3 = null;
    if (i4 != 0)
    {
      localObject2 = count;
      int i2 = ((Integer)localObject2).intValue();
      if (i2 != 0)
      {
        try
        {
          localObject2 = u;
          long l3 = b;
          localObject2 = ((o)localObject2).a(l3);
          localObject2 = ((com.truecaller.androidactors.w)localObject2).d();
          localObject2 = (Conversation)localObject2;
        }
        catch (InterruptedException localInterruptedException)
        {
          i2 = 0;
          localObject2 = null;
        }
        if (localObject2 == null)
        {
          new String[1][0] = "Operation failed. Draft not saved";
          return com.truecaller.androidactors.w.b(null);
        }
        localObject1 = new com/truecaller/messaging/data/types/Draft$a;
        ((Draft.a)localObject1).<init>();
        Object localObject5 = l;
        localObject5 = ((Draft.a)localObject1).a((Participant[])localObject5);
        b = ((Conversation)localObject2);
        long l4 = a;
        a = l4;
        localObject2 = n;
        i6 = localObject2.length;
        while (i5 < i6)
        {
          localObject3 = localObject2[i5];
          boolean bool2 = ((Entity)localObject3).a();
          if (bool2)
          {
            localObject3 = a;
            d = ((String)localObject3);
          }
          else
          {
            localObject3 = (BinaryEntity)localObject3;
            ((Draft.a)localObject1).a((BinaryEntity)localObject3);
          }
          i5 += 1;
        }
        localObject2 = r;
        i6 = k;
        ((com.truecaller.messaging.c.a)localObject2).a("Cancel", paramMessage, i6);
        return com.truecaller.androidactors.w.b(((Draft.a)localObject1).d());
      }
    }
    new String[1][0] = "Operation failed. Draft not saved";
    return com.truecaller.androidactors.w.b(null);
  }
  
  public final void d(long paramLong)
  {
    ContentValues localContentValues = new android/content/ContentValues;
    int i1 = 1;
    localContentValues.<init>(i1);
    Object localObject = Integer.valueOf(i1);
    localContentValues.put("actions_dismissed", (Integer)localObject);
    localObject = c;
    Uri localUri = TruecallerContract.an.a(paramLong);
    ((ContentResolver)localObject).update(localUri, localContentValues, null, null);
  }
  
  public final com.truecaller.androidactors.w e(long paramLong)
  {
    return u.c(paramLong);
  }
  
  /* Error */
  public final com.truecaller.androidactors.w e(Message paramMessage)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual 474	com/truecaller/messaging/data/types/Message:b	()Z
    //   4: istore_2
    //   5: iconst_1
    //   6: anewarray 153	java/lang/String
    //   9: dup
    //   10: iconst_0
    //   11: ldc_w 1509
    //   14: aastore
    //   15: astore_3
    //   16: iload_2
    //   17: aload_3
    //   18: invokestatic 1127	com/truecaller/log/AssertionUtil$AlwaysFatal:isTrue	(Z[Ljava/lang/String;)V
    //   21: aload_1
    //   22: getfield 1528	com/truecaller/messaging/data/types/Message:m	Lcom/truecaller/messaging/data/types/TransportInfo;
    //   25: astore 4
    //   27: aload 4
    //   29: invokeinterface 1529 1 0
    //   34: lstore 5
    //   36: iconst_1
    //   37: istore 7
    //   39: iconst_m1
    //   40: i2l
    //   41: lstore 8
    //   43: lload 5
    //   45: lload 8
    //   47: lcmp
    //   48: istore 10
    //   50: iload 10
    //   52: ifeq +8 -> 60
    //   55: iconst_1
    //   56: istore_2
    //   57: goto +8 -> 65
    //   60: iconst_0
    //   61: istore_2
    //   62: aconst_null
    //   63: astore 4
    //   65: iconst_1
    //   66: anewarray 153	java/lang/String
    //   69: dup
    //   70: iconst_0
    //   71: ldc_w 1509
    //   74: aastore
    //   75: astore_3
    //   76: iload_2
    //   77: aload_3
    //   78: invokestatic 1127	com/truecaller/log/AssertionUtil$AlwaysFatal:isTrue	(Z[Ljava/lang/String;)V
    //   81: aload_1
    //   82: getfield 1224	com/truecaller/messaging/data/types/Message:f	I
    //   85: iload 7
    //   87: iand
    //   88: istore_2
    //   89: iload_2
    //   90: ifeq +8 -> 98
    //   93: iconst_1
    //   94: istore_2
    //   95: goto +8 -> 103
    //   98: iconst_0
    //   99: istore_2
    //   100: aconst_null
    //   101: astore 4
    //   103: iconst_0
    //   104: anewarray 153	java/lang/String
    //   107: astore_3
    //   108: iload_2
    //   109: aload_3
    //   110: invokestatic 1127	com/truecaller/log/AssertionUtil$AlwaysFatal:isTrue	(Z[Ljava/lang/String;)V
    //   113: iload 7
    //   115: anewarray 153	java/lang/String
    //   118: astore 4
    //   120: new 155	java/lang/StringBuilder
    //   123: astore_3
    //   124: aload_3
    //   125: ldc_w 1531
    //   128: invokespecial 160	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   131: aload_3
    //   132: aload_1
    //   133: invokevirtual 164	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   136: pop
    //   137: aload_3
    //   138: ldc_w 1533
    //   141: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   144: pop
    //   145: aload_1
    //   146: getfield 1224	com/truecaller/messaging/data/types/Message:f	I
    //   149: istore 11
    //   151: aload_3
    //   152: iload 11
    //   154: invokevirtual 790	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   157: pop
    //   158: ldc_w 1535
    //   161: astore 12
    //   163: aload_3
    //   164: aload 12
    //   166: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   169: pop
    //   170: aload_1
    //   171: getfield 1051	com/truecaller/messaging/data/types/Message:j	I
    //   174: istore 11
    //   176: aload_3
    //   177: iload 11
    //   179: invokevirtual 790	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   182: pop
    //   183: aload_3
    //   184: invokevirtual 173	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   187: astore_3
    //   188: aload 4
    //   190: iconst_0
    //   191: aload_3
    //   192: aastore
    //   193: aload_0
    //   194: getfield 100	com/truecaller/messaging/data/v:f	Ldagger/a;
    //   197: astore 4
    //   199: aload 4
    //   201: invokeinterface 391 1 0
    //   206: astore 4
    //   208: aload 4
    //   210: checkcast 393	com/truecaller/messaging/transport/m
    //   213: astore 4
    //   215: aload_1
    //   216: getfield 1051	com/truecaller/messaging/data/types/Message:j	I
    //   219: istore 13
    //   221: aload 4
    //   223: iload 13
    //   225: invokeinterface 396 2 0
    //   230: astore 4
    //   232: iconst_0
    //   233: istore 13
    //   235: aconst_null
    //   236: astore_3
    //   237: aload 4
    //   239: ifnonnull +57 -> 296
    //   242: iload 7
    //   244: anewarray 153	java/lang/String
    //   247: astore 4
    //   249: new 155	java/lang/StringBuilder
    //   252: astore 14
    //   254: ldc_w 855
    //   257: astore 12
    //   259: aload 14
    //   261: aload 12
    //   263: invokespecial 160	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   266: aload_1
    //   267: getfield 856	com/truecaller/messaging/data/types/Message:k	I
    //   270: istore 15
    //   272: aload 14
    //   274: iload 15
    //   276: invokevirtual 790	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   279: pop
    //   280: aload 14
    //   282: invokevirtual 173	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   285: astore_1
    //   286: aload 4
    //   288: iconst_0
    //   289: aload_1
    //   290: aastore
    //   291: aconst_null
    //   292: invokestatic 1087	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   295: areturn
    //   296: aload 4
    //   298: invokeinterface 416 1 0
    //   303: astore 12
    //   305: aload 4
    //   307: aload_1
    //   308: aload 12
    //   310: invokeinterface 1490 3 0
    //   315: istore 16
    //   317: iload 16
    //   319: ifne +8 -> 327
    //   322: aconst_null
    //   323: invokestatic 1087	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   326: areturn
    //   327: aload 4
    //   329: aload 12
    //   331: invokeinterface 463 2 0
    //   336: istore 11
    //   338: iload 11
    //   340: ifne +8 -> 348
    //   343: aconst_null
    //   344: invokestatic 1087	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   347: areturn
    //   348: aload_1
    //   349: getfield 792	com/truecaller/messaging/data/types/Message:e	Lorg/a/a/b;
    //   352: astore 12
    //   354: aload 4
    //   356: aload 12
    //   358: invokeinterface 358 2 0
    //   363: aload 4
    //   365: invokeinterface 197 1 0
    //   370: istore_2
    //   371: iload_2
    //   372: invokestatic 608	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   375: astore 4
    //   377: aload 4
    //   379: invokestatic 1062	java/util/Collections:singleton	(Ljava/lang/Object;)Ljava/util/Set;
    //   382: astore 4
    //   384: aload_0
    //   385: iload 7
    //   387: aload 4
    //   389: invokevirtual 1065	com/truecaller/messaging/data/v:b	(ZLjava/util/Set;)V
    //   392: new 1321	android/content/ContentValues
    //   395: astore 4
    //   397: aload 4
    //   399: invokespecial 1322	android/content/ContentValues:<init>	()V
    //   402: ldc_w 1229
    //   405: astore 14
    //   407: iconst_0
    //   408: invokestatic 608	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   411: astore 12
    //   413: aload 4
    //   415: aload 14
    //   417: aload 12
    //   419: invokevirtual 1326	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   422: aload_0
    //   423: getfield 85	com/truecaller/messaging/data/v:c	Landroid/content/ContentResolver;
    //   426: astore 14
    //   428: aload_1
    //   429: getfield 475	com/truecaller/messaging/data/types/Message:a	J
    //   432: lstore 8
    //   434: lload 8
    //   436: invokestatic 1213	com/truecaller/content/TruecallerContract$aa:a	(J)Landroid/net/Uri;
    //   439: astore 12
    //   441: aload 14
    //   443: aload 12
    //   445: aload 4
    //   447: aconst_null
    //   448: aconst_null
    //   449: invokevirtual 1330	android/content/ContentResolver:update	(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   452: pop
    //   453: aload_1
    //   454: invokevirtual 542	com/truecaller/messaging/data/types/Message:m	()Lcom/truecaller/messaging/data/types/Message$a;
    //   457: astore_1
    //   458: bipush 9
    //   460: istore_2
    //   461: aload_1
    //   462: iload_2
    //   463: putfield 807	com/truecaller/messaging/data/types/Message$a:f	I
    //   466: aload_1
    //   467: iconst_0
    //   468: putfield 1238	com/truecaller/messaging/data/types/Message$a:u	I
    //   471: aload_1
    //   472: invokevirtual 555	com/truecaller/messaging/data/types/Message$a:b	()Lcom/truecaller/messaging/data/types/Message;
    //   475: astore_1
    //   476: aload_1
    //   477: invokestatic 1087	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   480: areturn
    //   481: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	482	0	this	v
    //   0	482	1	paramMessage	Message
    //   4	73	2	bool1	boolean
    //   88	21	2	i1	int
    //   370	93	2	i2	int
    //   15	222	3	localObject1	Object
    //   25	421	4	localObject2	Object
    //   34	10	5	l1	long
    //   37	349	7	i3	int
    //   41	394	8	l2	long
    //   48	3	10	bool2	boolean
    //   149	29	11	i4	int
    //   336	3	11	bool3	boolean
    //   161	283	12	localObject3	Object
    //   219	15	13	i5	int
    //   252	190	14	localObject4	Object
    //   270	5	15	i6	int
    //   315	3	16	bool4	boolean
    // Exception table:
    //   from	to	target	type
    //   193	197	481	finally
    //   199	206	481	finally
    //   208	213	481	finally
    //   215	219	481	finally
    //   223	230	481	finally
    //   242	247	481	finally
    //   249	252	481	finally
    //   261	266	481	finally
    //   266	270	481	finally
    //   274	280	481	finally
    //   280	285	481	finally
    //   289	291	481	finally
    //   291	295	481	finally
    //   296	303	481	finally
    //   308	315	481	finally
    //   322	326	481	finally
    //   329	336	481	finally
    //   343	347	481	finally
    //   348	352	481	finally
    //   356	363	481	finally
    //   363	370	481	finally
    //   371	375	481	finally
    //   377	382	481	finally
    //   387	392	481	finally
    //   392	395	481	finally
    //   397	402	481	finally
    //   407	411	481	finally
    //   417	422	481	finally
    //   422	426	481	finally
    //   428	432	481	finally
    //   434	439	481	finally
    //   448	453	481	finally
    //   453	457	481	finally
    //   462	466	481	finally
    //   467	471	481	finally
    //   471	475	481	finally
    //   476	480	481	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */