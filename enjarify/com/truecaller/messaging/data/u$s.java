package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.messaging.data.types.Conversation;

final class u$s
  extends u
{
  private final Conversation[] b;
  
  private u$s(e parame, Conversation[] paramArrayOfConversation)
  {
    super(parame);
    b = paramArrayOfConversation;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".markConversationsRead(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */