package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class af$a
  extends u
{
  private final t.a b;
  private final int c;
  private final Iterable d;
  
  private af$a(e parame, t.a parama, int paramInt, Iterable paramIterable)
  {
    super(parame);
    b = parama;
    c = paramInt;
    d = paramIterable;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".performNextBatch(");
    Object localObject = b;
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(c), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(d, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.af.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */