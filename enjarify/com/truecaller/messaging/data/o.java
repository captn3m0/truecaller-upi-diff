package com.truecaller.messaging.data;

import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.types.Participant;
import java.util.List;
import org.a.a.b;

public abstract interface o
{
  public abstract w a();
  
  public abstract w a(int paramInt);
  
  public abstract w a(long paramLong);
  
  public abstract w a(long paramLong, int paramInt1, int paramInt2, Integer paramInteger);
  
  public abstract w a(Integer paramInteger);
  
  public abstract w a(String paramString);
  
  public abstract w a(List paramList);
  
  public abstract w a(b paramb);
  
  public abstract w a(Participant[] paramArrayOfParticipant, int paramInt);
  
  public abstract w b();
  
  public abstract w b(long paramLong);
  
  public abstract w b(String paramString);
  
  public abstract w c(long paramLong);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */