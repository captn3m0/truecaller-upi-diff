package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.Set;

final class u$aa
  extends u
{
  private final boolean b;
  private final Set c;
  
  private u$aa(e parame, boolean paramBoolean, Set paramSet)
  {
    super(parame);
    b = paramBoolean;
    c = paramSet;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".performPartialSync(");
    Object localObject = Boolean.valueOf(b);
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */