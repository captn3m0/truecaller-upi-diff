package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.messaging.data.types.Message;

final class u$ah
  extends u
{
  private final Message b;
  private final String c;
  private final String d;
  
  private u$ah(e parame, Message paramMessage, String paramString1, String paramString2)
  {
    super(parame);
    b = paramMessage;
    c = paramString1;
    d = paramString2;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".sendReaction(");
    Object localObject = b;
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(d, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */