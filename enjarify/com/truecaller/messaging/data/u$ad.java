package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.messaging.data.types.Message;

final class u$ad
  extends u
{
  private final Message b;
  private final long c;
  private final boolean d;
  
  private u$ad(e parame, Message paramMessage, long paramLong, boolean paramBoolean)
  {
    super(parame);
    b = paramMessage;
    c = paramLong;
    d = paramBoolean;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".retryMessage(");
    Object localObject = a(b, 1);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = Long.valueOf(c);
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Boolean.valueOf(d), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */