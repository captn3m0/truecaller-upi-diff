package com.truecaller.messaging.data;

import dagger.a.d;
import javax.inject.Provider;

public final class i
  implements d
{
  private final e a;
  private final Provider b;
  
  private i(e parame, Provider paramProvider)
  {
    a = parame;
    b = paramProvider;
  }
  
  public static i a(e parame, Provider paramProvider)
  {
    i locali = new com/truecaller/messaging/data/i;
    locali.<init>(parame, paramProvider);
    return locali;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */