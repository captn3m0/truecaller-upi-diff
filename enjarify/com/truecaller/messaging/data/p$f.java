package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class p$f
  extends u
{
  private final long b;
  private final int c;
  private final int d;
  private final Integer e;
  
  private p$f(e parame, long paramLong, int paramInt1, int paramInt2, Integer paramInteger)
  {
    super(parame);
    b = paramLong;
    c = paramInt1;
    d = paramInt2;
    e = paramInteger;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".fetchMessageCursor(");
    Object localObject = Long.valueOf(b);
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(c), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(d), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(e, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.p.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */