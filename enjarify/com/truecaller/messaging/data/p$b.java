package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class p$b
  extends u
{
  private final Integer b;
  
  private p$b(e parame, Integer paramInteger)
  {
    super(parame);
    b = paramInteger;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".fetchConversationCursor(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.p.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */