package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class u$c
  extends u
{
  private final long b;
  private final int c;
  private final int d;
  private final boolean e;
  
  private u$c(e parame, long paramLong, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    super(parame);
    b = paramLong;
    c = paramInt1;
    d = paramInt2;
    e = paramBoolean;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".deleteConversation(");
    Object localObject = Long.valueOf(b);
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(c), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(d), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Boolean.valueOf(e), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */