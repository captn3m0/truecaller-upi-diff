package com.truecaller.messaging.data;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.net.Uri;
import com.truecaller.backup.BackupTransportInfo;
import com.truecaller.common.h.am;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.data.a.s;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.transport.NullTransportInfo;
import com.truecaller.messaging.transport.NullTransportInfo.a;
import com.truecaller.messaging.transport.history.HistoryTransportInfo;
import com.truecaller.messaging.transport.im.ImTransportInfo.a;
import com.truecaller.messaging.transport.mms.MmsTransportInfo.a;
import com.truecaller.messaging.transport.sms.SmsTransportInfo.a;

final class aj
  extends CursorWrapper
  implements s
{
  private final int A;
  private final int B;
  private final int C;
  private final int D;
  private final int E;
  private final int F;
  private final int G;
  private final int H;
  private final int I;
  private final int J;
  private final int K;
  private final int L;
  private final int M;
  private final int N;
  private final int O;
  private final int P;
  private final int Q;
  private final int R;
  private final int S;
  private final int T;
  private final int U;
  private final int V;
  private final int W;
  private final int X;
  private final int Y;
  private final int Z;
  private final int a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  private final int j;
  private final int k;
  private final int l;
  private final int m;
  private final int n;
  private final int o;
  private final int p;
  private final int q;
  private final int r;
  private final int s;
  private final int t;
  private final int u;
  private final int v;
  private final int w;
  private final int x;
  private final int y;
  private final int z;
  
  aj(Cursor paramCursor)
  {
    super(paramCursor);
    int i1 = paramCursor.getColumnIndexOrThrow("message_id");
    a = i1;
    i1 = paramCursor.getColumnIndexOrThrow("conversation_id");
    b = i1;
    i1 = paramCursor.getColumnIndexOrThrow("seen");
    c = i1;
    i1 = paramCursor.getColumnIndexOrThrow("read");
    d = i1;
    i1 = paramCursor.getColumnIndexOrThrow("locked");
    e = i1;
    i1 = paramCursor.getColumnIndexOrThrow("status");
    f = i1;
    i1 = paramCursor.getColumnIndexOrThrow("date");
    h = i1;
    i1 = paramCursor.getColumnIndexOrThrow("transport");
    g = i1;
    i1 = paramCursor.getColumnIndexOrThrow("raw_id");
    i = i1;
    i1 = paramCursor.getColumnIndexOrThrow("raw_status");
    j = i1;
    i1 = paramCursor.getColumnIndexOrThrow("raw_thread");
    k = i1;
    i1 = paramCursor.getColumnIndexOrThrow("message_uri");
    l = i1;
    i1 = paramCursor.getColumnIndexOrThrow("sms_protocol");
    m = i1;
    i1 = paramCursor.getColumnIndexOrThrow("sms_type");
    n = i1;
    i1 = paramCursor.getColumnIndexOrThrow("sms_service_center");
    o = i1;
    i1 = paramCursor.getColumnIndexOrThrow("sms_subject");
    p = i1;
    i1 = paramCursor.getColumnIndexOrThrow("sms_error_code");
    q = i1;
    i1 = paramCursor.getColumnIndexOrThrow("sms_reply_path_present");
    r = i1;
    i1 = paramCursor.getColumnIndexOrThrow("sms_stripped_raw_address");
    s = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_type");
    t = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_subject");
    u = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_subject_charset");
    v = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_content_location");
    w = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_transaction_id");
    x = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_expiry");
    y = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_priority");
    z = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_retrieve_status");
    A = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_response_status");
    B = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_message_id");
    C = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_message_box");
    D = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_size");
    E = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_delivery_report");
    F = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_delivery_time");
    G = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_version");
    H = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_retrieve_text");
    I = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_retrieve_text_charset");
    J = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_content_type");
    K = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_content_type");
    L = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_response_text");
    M = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_message_class");
    N = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_read_report");
    O = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_read_status");
    P = i1;
    i1 = paramCursor.getColumnIndexOrThrow("mms_report_allowed");
    Q = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_status");
    R = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_delivery_status");
    S = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_read_status");
    T = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_read_sync_status");
    U = i1;
    i1 = paramCursor.getColumnIndexOrThrow("raw_id");
    V = i1;
    i1 = paramCursor.getColumnIndexOrThrow("h_type");
    W = i1;
    i1 = paramCursor.getColumnIndexOrThrow("h_number_type");
    X = i1;
    i1 = paramCursor.getColumnIndexOrThrow("h_call_log_id");
    Y = i1;
    int i2 = paramCursor.getColumnIndexOrThrow("h_features");
    Z = i2;
  }
  
  private long f()
  {
    int i1 = a;
    return getLong(i1);
  }
  
  private NullTransportInfo g()
  {
    NullTransportInfo.a locala = new com/truecaller/messaging/transport/NullTransportInfo$a;
    locala.<init>();
    long l1 = f();
    a = l1;
    return locala.a();
  }
  
  public final long a()
  {
    int i1 = b;
    return getLong(i1);
  }
  
  public final long b()
  {
    int i1 = h;
    return getLong(i1);
  }
  
  public final int c()
  {
    int i1 = f;
    return getInt(i1);
  }
  
  public final int d()
  {
    int i1 = g;
    i1 = getInt(i1);
    switch (i1)
    {
    default: 
      AssertionError localAssertionError = new java/lang/AssertionError;
      localAssertionError.<init>("Unsupported transport type");
      AssertionUtil.reportThrowableButNeverCrash(localAssertionError);
      throw localAssertionError;
    case 6: 
      return 6;
    case 5: 
      return 5;
    case 4: 
      return 4;
    case 3: 
      return 3;
    case 2: 
      return 2;
    case 1: 
      return 1;
    }
    return 0;
  }
  
  public final TransportInfo e()
  {
    int i1 = g;
    i1 = getInt(i1);
    int i5 = 1;
    Object localObject2;
    long l3;
    switch (i1)
    {
    default: 
      localObject1 = new java/lang/AssertionError;
      ((AssertionError)localObject1).<init>("Unsupported transport type");
      throw ((Throwable)localObject1);
    case 6: 
      return g();
    case 5: 
      localObject1 = new com/truecaller/messaging/transport/history/HistoryTransportInfo;
      long l1 = f();
      i5 = V;
      l2 = getLong(i5);
      i5 = Y;
      int i6 = getInt(i5);
      i5 = W;
      int i7 = getInt(i5);
      i5 = Z;
      int i8 = getInt(i5);
      i5 = X;
      String str1 = getString(i5);
      localObject2 = localObject1;
      ((HistoryTransportInfo)localObject1).<init>(l1, l2, i6, i7, i8, str1);
      return (TransportInfo)localObject1;
    case 4: 
      localObject1 = new com/truecaller/backup/BackupTransportInfo;
      l3 = f();
      ((BackupTransportInfo)localObject1).<init>(l3);
      return (TransportInfo)localObject1;
    case 3: 
      return g();
    case 2: 
      localObject1 = new com/truecaller/messaging/transport/im/ImTransportInfo$a;
      ((ImTransportInfo.a)localObject1).<init>();
      i5 = i;
      localObject2 = am.n(getString(i5));
      localObject1 = ((ImTransportInfo.a)localObject1).a((String)localObject2);
      l3 = f();
      a = l3;
      i5 = R;
      i5 = getInt(i5);
      b = i5;
      i5 = S;
      i5 = getInt(i5);
      c = i5;
      i5 = T;
      i5 = getInt(i5);
      d = i5;
      i5 = U;
      i5 = getInt(i5);
      f = i5;
      return ((ImTransportInfo.a)localObject1).a();
    case 1: 
      i1 = l;
      localObject1 = getString(i1);
      localObject3 = new String[0];
      AssertionUtil.AlwaysFatal.isNotNull(localObject1, (String[])localObject3);
      localObject3 = new com/truecaller/messaging/transport/mms/MmsTransportInfo$a;
      ((MmsTransportInfo.a)localObject3).<init>();
      i9 = i;
      l2 = getLong(i9);
      b = l2;
      i9 = j;
      i9 = getInt(i9);
      c = i9;
      i9 = k;
      l2 = getLong(i9);
      d = l2;
      localObject1 = Uri.parse((String)localObject1);
      e = ((Uri)localObject1);
      l2 = f();
      a = l2;
      i1 = t;
      i1 = getInt(i1);
      w = i1;
      i1 = x;
      localObject1 = getString(i1);
      p = ((String)localObject1);
      i1 = y;
      l2 = getLong(i1);
      localObject1 = ((MmsTransportInfo.a)localObject3).e(l2);
      i9 = z;
      i9 = getInt(i9);
      r = i9;
      i9 = A;
      i9 = getInt(i9);
      s = i9;
      i9 = B;
      i9 = getInt(i9);
      t = i9;
      i9 = C;
      String str2 = getString(i9);
      u = str2;
      i9 = D;
      i9 = getInt(i9);
      v = i9;
      i9 = E;
      i9 = getInt(i9);
      x = i9;
      i9 = F;
      i9 = getInt(i9);
      y = i9;
      i9 = G;
      l2 = getLong(i9);
      z = l2;
      i9 = H;
      i9 = getInt(i9);
      f = i9;
      i9 = K;
      str2 = getString(i9);
      l = str2;
      i9 = L;
      i9 = getInt(i9);
      m = i9;
      i9 = M;
      str2 = getString(i9);
      n = str2;
      i9 = N;
      str2 = getString(i9);
      o = str2;
      i9 = O;
      i9 = getInt(i9);
      A = i9;
      i9 = P;
      i9 = getInt(i9);
      B = i9;
      i9 = Q;
      i9 = getInt(i9);
      if (i9 == 0)
      {
        i5 = 0;
        localObject2 = null;
      }
      C = i5;
      i1 = u;
      boolean bool1 = isNull(i1);
      if (!bool1)
      {
        i2 = u;
        localObject1 = getString(i2);
        i5 = v;
        i5 = getInt(i5);
        ((MmsTransportInfo.a)localObject3).a((String)localObject1, i5);
      }
      int i2 = I;
      boolean bool2 = isNull(i2);
      if (!bool2)
      {
        i3 = I;
        localObject1 = getString(i3);
        i5 = J;
        i5 = getInt(i5);
        ((MmsTransportInfo.a)localObject3).a((String)localObject1, i5);
      }
      int i3 = w;
      boolean bool3 = isNull(i3);
      if (!bool3)
      {
        i4 = w;
        localObject1 = Uri.parse(getString(i4));
        k = ((Uri)localObject1);
      }
      return ((MmsTransportInfo.a)localObject3).a();
    }
    int i4 = l;
    Object localObject1 = getString(i4);
    Object localObject3 = new String[0];
    AssertionUtil.AlwaysFatal.isNotNull(localObject1, (String[])localObject3);
    localObject3 = new com/truecaller/messaging/transport/sms/SmsTransportInfo$a;
    ((SmsTransportInfo.a)localObject3).<init>();
    int i9 = i;
    long l2 = getLong(i9);
    b = l2;
    i9 = j;
    i9 = getInt(i9);
    c = i9;
    i9 = k;
    l2 = getLong(i9);
    d = l2;
    localObject1 = Uri.parse((String)localObject1);
    e = ((Uri)localObject1);
    l2 = f();
    a = l2;
    i4 = m;
    i4 = getInt(i4);
    f = i4;
    i4 = n;
    i4 = getInt(i4);
    g = i4;
    i4 = o;
    localObject1 = getString(i4);
    h = ((String)localObject1);
    i4 = q;
    i4 = getInt(i4);
    i = i4;
    i4 = p;
    localObject1 = getString(i4);
    k = ((String)localObject1);
    i4 = r;
    i4 = getInt(i4);
    if (i4 == 0)
    {
      i5 = 0;
      localObject2 = null;
    }
    j = i5;
    i4 = s;
    localObject1 = getString(i4);
    l = ((String)localObject1);
    return ((SmsTransportInfo.a)localObject3).a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.aj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */