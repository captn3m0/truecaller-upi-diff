package com.truecaller.messaging.data;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import c.g.a.b;
import c.g.b.k;
import com.truecaller.androidactors.a;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.content.TruecallerContract.g;
import com.truecaller.network.search.e;
import com.truecaller.network.search.e.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public final class am
  implements al, e.a
{
  final List a;
  ak b;
  boolean c;
  final f d;
  private List e;
  private a f;
  private boolean g;
  private final ContentObserver h;
  private final am.c i;
  private final ContentResolver j;
  private final f k;
  private final i l;
  private final com.truecaller.util.al m;
  private final e n;
  
  public am(ContentResolver paramContentResolver, f paramf1, i parami, com.truecaller.util.al paramal, e parame, f paramf2)
  {
    j = paramContentResolver;
    k = paramf1;
    l = parami;
    m = paramal;
    n = parame;
    d = paramf2;
    paramContentResolver = new java/util/ArrayList;
    paramContentResolver.<init>();
    paramContentResolver = (List)paramContentResolver;
    e = paramContentResolver;
    paramContentResolver = new java/util/ArrayList;
    paramContentResolver.<init>();
    paramContentResolver = (List)paramContentResolver;
    a = paramContentResolver;
    paramContentResolver = new com/truecaller/messaging/data/ak;
    paramContentResolver.<init>(0, 0, 0, 0L);
    b = paramContentResolver;
    paramContentResolver = new com/truecaller/messaging/data/am$a;
    paramf1 = new android/os/Handler;
    parami = Looper.getMainLooper();
    paramf1.<init>(parami);
    paramContentResolver.<init>(this, paramf1);
    paramContentResolver = (ContentObserver)paramContentResolver;
    h = paramContentResolver;
    paramContentResolver = new com/truecaller/messaging/data/am$c;
    paramContentResolver.<init>(this);
    i = paramContentResolver;
  }
  
  private final void f()
  {
    a locala = f;
    if (locala != null) {
      locala.a();
    }
    f = null;
    c = false;
  }
  
  public final List a()
  {
    return e;
  }
  
  public final void a(al.a parama)
  {
    String str = "observer";
    k.b(parama, str);
    boolean bool = g;
    if (bool)
    {
      bool = c;
      if (!bool)
      {
        bool = true;
        break label35;
      }
    }
    bool = false;
    str = null;
    label35:
    if (bool) {
      c(parama);
    }
    a.add(parama);
  }
  
  public final void a(Collection paramCollection)
  {
    k.b(paramCollection, "normalizedNumbers");
    d();
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "<set-?>");
    e = paramList;
  }
  
  public final void a(Set paramSet)
  {
    k.b(paramSet, "normalizedNumbers");
  }
  
  public final void b()
  {
    boolean bool1 = g;
    if (bool1) {
      return;
    }
    Object localObject1 = j;
    Object localObject2 = TruecallerContract.g.a();
    Object localObject3 = h;
    boolean bool2 = true;
    ((ContentResolver)localObject1).registerContentObserver((Uri)localObject2, bool2, (ContentObserver)localObject3);
    localObject1 = m;
    localObject2 = (BroadcastReceiver)i;
    localObject3 = new String[] { "com.truecaller.messaging.spam.SEARCH_COMPLETED" };
    ((com.truecaller.util.al)localObject1).a((BroadcastReceiver)localObject2, (String[])localObject3);
    localObject1 = n;
    localObject2 = this;
    localObject2 = (e.a)this;
    ((e)localObject1).a((e.a)localObject2);
    g = bool2;
    d();
  }
  
  public final void b(al.a parama)
  {
    k.b(parama, "observer");
    a.remove(parama);
  }
  
  public final void c()
  {
    Object localObject1 = j;
    Object localObject2 = h;
    ((ContentResolver)localObject1).unregisterContentObserver((ContentObserver)localObject2);
    localObject1 = m;
    localObject2 = (BroadcastReceiver)i;
    ((com.truecaller.util.al)localObject1).a((BroadcastReceiver)localObject2);
    localObject1 = n;
    localObject2 = this;
    localObject2 = (e.a)this;
    ((e)localObject1).b((e.a)localObject2);
    g = false;
  }
  
  final void c(al.a parama)
  {
    ak localak = b;
    parama.a(localak);
  }
  
  public final void d()
  {
    f();
    c = true;
    Object localObject1 = ((o)k.a()).b();
    i locali = l;
    Object localObject2 = new com/truecaller/messaging/data/am$b;
    Object localObject3 = this;
    localObject3 = (am)this;
    ((am.b)localObject2).<init>((am)localObject3);
    localObject2 = (b)localObject2;
    localObject3 = new com/truecaller/messaging/data/an;
    ((an)localObject3).<init>((b)localObject2);
    localObject3 = (ac)localObject3;
    localObject1 = ((w)localObject1).a(locali, (ac)localObject3);
    f = ((a)localObject1);
  }
  
  public final boolean e()
  {
    return g;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.am
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */