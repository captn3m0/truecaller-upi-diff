package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.Draft;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import java.util.Set;
import org.a.a.b;

public final class u
  implements t
{
  private final v a;
  
  public u(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return t.class.equals(paramClass);
  }
  
  public final w a(int paramInt)
  {
    v localv = a;
    u.o localo = new com/truecaller/messaging/data/u$o;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localo.<init>(locale, paramInt, (byte)0);
    return w.a(localv, localo);
  }
  
  public final w a(long paramLong)
  {
    v localv = a;
    u.m localm = new com/truecaller/messaging/data/u$m;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localm.<init>(locale, paramLong, (byte)0);
    return w.a(localv, localm);
  }
  
  public final w a(long paramLong, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    v localv = a;
    u.c localc = new com/truecaller/messaging/data/u$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, paramLong, paramInt1, paramInt2, paramBoolean, (byte)0);
    return w.a(localv, localc);
  }
  
  public final w a(Draft paramDraft)
  {
    v localv = a;
    u.ae localae = new com/truecaller/messaging/data/u$ae;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localae.<init>(locale, paramDraft, (byte)0);
    return w.a(localv, localae);
  }
  
  public final w a(Message paramMessage, int paramInt, String paramString)
  {
    v localv = a;
    u.ac localac = new com/truecaller/messaging/data/u$ac;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localac.<init>(locale, paramMessage, paramInt, paramString, (byte)0);
    return w.a(localv, localac);
  }
  
  public final w a(Message paramMessage, long paramLong)
  {
    v localv = a;
    u.al localal = new com/truecaller/messaging/data/u$al;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localal.<init>(locale, paramMessage, paramLong, (byte)0);
    return w.a(localv, localal);
  }
  
  public final w a(Message paramMessage, long paramLong, boolean paramBoolean)
  {
    v localv = a;
    u.ad localad = new com/truecaller/messaging/data/u$ad;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localad.<init>(locale, paramMessage, paramLong, paramBoolean, (byte)0);
    return w.a(localv, localad);
  }
  
  public final w a(Message paramMessage, Participant[] paramArrayOfParticipant, int paramInt)
  {
    v localv = a;
    u.af localaf = new com/truecaller/messaging/data/u$af;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localaf.<init>(locale, paramMessage, paramArrayOfParticipant, paramInt, (byte)0);
    return w.a(localv, localaf);
  }
  
  public final w a(String paramString)
  {
    v localv = a;
    u.l locall = new com/truecaller/messaging/data/u$l;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locall.<init>(locale, paramString, (byte)0);
    return w.a(localv, locall);
  }
  
  public final w a(b paramb)
  {
    v localv = a;
    u.h localh = new com/truecaller/messaging/data/u$h;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localh.<init>(locale, paramb, (byte)0);
    return w.a(localv, localh);
  }
  
  public final w a(boolean paramBoolean, long[] paramArrayOfLong)
  {
    v localv = a;
    u.f localf = new com/truecaller/messaging/data/u$f;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localf.<init>(locale, paramBoolean, paramArrayOfLong, (byte)0);
    return w.a(localv, localf);
  }
  
  public final w a(long[] paramArrayOfLong)
  {
    v localv = a;
    u.r localr = new com/truecaller/messaging/data/u$r;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localr.<init>(locale, paramArrayOfLong, (byte)0);
    return w.a(localv, localr);
  }
  
  public final w a(Conversation[] paramArrayOfConversation)
  {
    v localv = a;
    u.s locals = new com/truecaller/messaging/data/u$s;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locals.<init>(locale, paramArrayOfConversation, (byte)0);
    return w.a(localv, locals);
  }
  
  public final w a(Conversation[] paramArrayOfConversation, boolean paramBoolean)
  {
    v localv = a;
    u.d locald = new com/truecaller/messaging/data/u$d;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locald.<init>(locale, paramArrayOfConversation, paramBoolean, (byte)0);
    return w.a(localv, locald);
  }
  
  public final void a()
  {
    v localv = a;
    u.b localb = new com/truecaller/messaging/data/u$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, (byte)0);
    localv.a(localb);
  }
  
  public final void a(int paramInt, b paramb)
  {
    v localv = a;
    u.ag localag = new com/truecaller/messaging/data/u$ag;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localag.<init>(locale, paramInt, paramb, (byte)0);
    localv.a(localag);
  }
  
  public final void a(int paramInt, b paramb, boolean paramBoolean)
  {
    v localv = a;
    u.ab localab = new com/truecaller/messaging/data/u$ab;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localab.<init>(locale, paramInt, paramb, paramBoolean, (byte)0);
    localv.a(localab);
  }
  
  public final void a(long paramLong, int paramInt)
  {
    v localv = a;
    u.ak localak = new com/truecaller/messaging/data/u$ak;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localak.<init>(locale, paramLong, paramInt, (byte)0);
    localv.a(localak);
  }
  
  public final void a(long paramLong, int paramInt1, int paramInt2)
  {
    v localv = a;
    u.q localq = new com/truecaller/messaging/data/u$q;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localq.<init>(locale, paramLong, paramInt1, paramInt2, (byte)0);
    localv.a(localq);
  }
  
  public final void a(t.a parama, int paramInt, Iterable paramIterable)
  {
    v localv = a;
    u.y localy = new com/truecaller/messaging/data/u$y;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localy.<init>(locale, parama, paramInt, paramIterable, (byte)0);
    localv.a(localy);
  }
  
  public final void a(Message paramMessage)
  {
    v localv = a;
    u.ai localai = new com/truecaller/messaging/data/u$ai;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localai.<init>(locale, paramMessage, (byte)0);
    localv.a(localai);
  }
  
  public final void a(Message paramMessage, String paramString1, String paramString2)
  {
    v localv = a;
    u.ah localah = new com/truecaller/messaging/data/u$ah;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localah.<init>(locale, paramMessage, paramString1, paramString2, (byte)0);
    localv.a(localah);
  }
  
  public final void a(boolean paramBoolean)
  {
    v localv = a;
    u.w localw = new com/truecaller/messaging/data/u$w;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localw.<init>(locale, paramBoolean, (byte)0);
    localv.a(localw);
  }
  
  public final void a(boolean paramBoolean, Set paramSet)
  {
    v localv = a;
    u.x localx = new com/truecaller/messaging/data/u$x;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localx.<init>(locale, paramBoolean, paramSet, (byte)0);
    localv.a(localx);
  }
  
  public final void a(Message[] paramArrayOfMessage, int paramInt)
  {
    v localv = a;
    u.ao localao = new com/truecaller/messaging/data/u$ao;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localao.<init>(locale, paramArrayOfMessage, paramInt, (byte)0);
    localv.a(localao);
  }
  
  public final w b(b paramb)
  {
    v localv = a;
    u.k localk = new com/truecaller/messaging/data/u$k;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localk.<init>(locale, paramb, (byte)0);
    return w.a(localv, localk);
  }
  
  public final void b()
  {
    v localv = a;
    u.aj localaj = new com/truecaller/messaging/data/u$aj;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localaj.<init>(locale, (byte)0);
    localv.a(localaj);
  }
  
  public final void b(int paramInt)
  {
    v localv = a;
    u.i locali = new com/truecaller/messaging/data/u$i;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locali.<init>(locale, paramInt, (byte)0);
    localv.a(locali);
  }
  
  public final void b(long paramLong)
  {
    v localv = a;
    u.t localt = new com/truecaller/messaging/data/u$t;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localt.<init>(locale, paramLong, (byte)0);
    localv.a(localt);
  }
  
  public final void b(Message paramMessage)
  {
    v localv = a;
    u.am localam = new com/truecaller/messaging/data/u$am;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localam.<init>(locale, paramMessage, (byte)0);
    localv.a(localam);
  }
  
  public final void b(boolean paramBoolean)
  {
    v localv = a;
    u.z localz = new com/truecaller/messaging/data/u$z;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localz.<init>(locale, paramBoolean, (byte)0);
    localv.a(localz);
  }
  
  public final void b(boolean paramBoolean, Set paramSet)
  {
    v localv = a;
    u.aa localaa = new com/truecaller/messaging/data/u$aa;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localaa.<init>(locale, paramBoolean, paramSet, (byte)0);
    localv.a(localaa);
  }
  
  public final void b(long[] paramArrayOfLong)
  {
    v localv = a;
    u.v localv1 = new com/truecaller/messaging/data/u$v;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localv1.<init>(locale, paramArrayOfLong, (byte)0);
    localv.a(localv1);
  }
  
  public final w c(long paramLong)
  {
    v localv = a;
    u.e locale = new com/truecaller/messaging/data/u$e;
    e locale1 = new com/truecaller/androidactors/e;
    locale1.<init>();
    locale.<init>(locale1, paramLong, (byte)0);
    return w.a(localv, locale);
  }
  
  public final w c(Message paramMessage)
  {
    v localv = a;
    u.g localg = new com/truecaller/messaging/data/u$g;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localg.<init>(locale, paramMessage, (byte)0);
    return w.a(localv, localg);
  }
  
  public final void c(int paramInt)
  {
    v localv = a;
    u.j localj = new com/truecaller/messaging/data/u$j;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localj.<init>(locale, paramInt, (byte)0);
    localv.a(localj);
  }
  
  public final void c(long[] paramArrayOfLong)
  {
    v localv = a;
    u.u localu = new com/truecaller/messaging/data/u$u;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localu.<init>(locale, paramArrayOfLong, (byte)0);
    localv.a(localu);
  }
  
  public final w d(Message paramMessage)
  {
    v localv = a;
    u.a locala = new com/truecaller/messaging/data/u$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramMessage, (byte)0);
    return w.a(localv, locala);
  }
  
  public final void d(long paramLong)
  {
    v localv = a;
    u.p localp = new com/truecaller/messaging/data/u$p;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localp.<init>(locale, paramLong, (byte)0);
    localv.a(localp);
  }
  
  public final w e(long paramLong)
  {
    v localv = a;
    u.n localn = new com/truecaller/messaging/data/u$n;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localn.<init>(locale, paramLong, (byte)0);
    return w.a(localv, localn);
  }
  
  public final w e(Message paramMessage)
  {
    v localv = a;
    u.an localan = new com/truecaller/messaging/data/u$an;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localan.<init>(locale, paramMessage, (byte)0);
    return w.a(localv, localan);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */