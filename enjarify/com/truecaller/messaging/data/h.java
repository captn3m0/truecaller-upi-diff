package com.truecaller.messaging.data;

import dagger.a.d;
import javax.inject.Provider;

public final class h
  implements d
{
  private final e a;
  private final Provider b;
  private final Provider c;
  
  private h(e parame, Provider paramProvider1, Provider paramProvider2)
  {
    a = parame;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static h a(e parame, Provider paramProvider1, Provider paramProvider2)
  {
    h localh = new com/truecaller/messaging/data/h;
    localh.<init>(parame, paramProvider1, paramProvider2);
    return localh;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */