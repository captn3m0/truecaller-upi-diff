package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class u$ak
  extends u
{
  private final long b;
  private final int c;
  
  private u$ak(e parame, long paramLong, int paramInt)
  {
    super(parame);
    b = paramLong;
    c = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".updateConversationPreferredTransport(");
    Object localObject = Long.valueOf(b);
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(c), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u.ak
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */