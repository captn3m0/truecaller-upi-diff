package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.messaging.data.types.Message;

final class u$ao
  extends u
{
  private final Message[] b;
  private final int c;
  
  private u$ao(e parame, Message[] paramArrayOfMessage, int paramInt)
  {
    super(parame);
    b = paramArrayOfMessage;
    c = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".updateMessagesCategory(");
    Object localObject = b;
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(c), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u.ao
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */