package com.truecaller.messaging.data.providers;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.CancellationSignal;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.webkit.MimeTypeMap;
import c.l;
import c.u;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class AttachmentProvider
  extends ContentProvider
{
  private static File a()
  {
    File localFile1 = new java/io/File;
    File localFile2 = Environment.getExternalStorageDirectory();
    localFile1.<init>(localFile2, "Truecaller");
    return localFile1;
  }
  
  /* Error */
  private final Long a(Uri paramUri)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_0
    //   3: aload_1
    //   4: invokespecial 25	com/truecaller/messaging/data/providers/AttachmentProvider:b	(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;
    //   7: astore_1
    //   8: aload_1
    //   9: invokevirtual 31	android/os/ParcelFileDescriptor:getStatSize	()J
    //   12: lstore_3
    //   13: lload_3
    //   14: invokestatic 37	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   17: astore_2
    //   18: aload_1
    //   19: ifnull +37 -> 56
    //   22: aload_1
    //   23: invokevirtual 40	android/os/ParcelFileDescriptor:close	()V
    //   26: goto +30 -> 56
    //   29: astore_2
    //   30: goto +6 -> 36
    //   33: astore_2
    //   34: aconst_null
    //   35: astore_1
    //   36: aload_1
    //   37: ifnull +7 -> 44
    //   40: aload_1
    //   41: invokevirtual 40	android/os/ParcelFileDescriptor:close	()V
    //   44: aload_2
    //   45: athrow
    //   46: pop
    //   47: aconst_null
    //   48: astore_1
    //   49: aload_1
    //   50: ifnull +6 -> 56
    //   53: goto -31 -> 22
    //   56: aload_2
    //   57: areturn
    //   58: pop
    //   59: goto -10 -> 49
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	62	0	this	AttachmentProvider
    //   0	62	1	paramUri	Uri
    //   1	17	2	localLong1	Long
    //   29	1	2	localObject	Object
    //   33	24	2	localLong2	Long
    //   12	2	3	l	long
    //   46	1	6	localFileNotFoundException1	FileNotFoundException
    //   58	1	7	localFileNotFoundException2	FileNotFoundException
    // Exception table:
    //   from	to	target	type
    //   8	12	29	finally
    //   13	17	29	finally
    //   3	7	33	finally
    //   3	7	46	java/io/FileNotFoundException
    //   8	12	58	java/io/FileNotFoundException
    //   13	17	58	java/io/FileNotFoundException
  }
  
  /* Error */
  private static Long a(Uri paramUri, File paramFile)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokestatic 45	com/truecaller/messaging/data/providers/AttachmentProvider:b	(Landroid/net/Uri;Ljava/io/File;)Landroid/os/ParcelFileDescriptor;
    //   5: astore_0
    //   6: aload_0
    //   7: invokevirtual 31	android/os/ParcelFileDescriptor:getStatSize	()J
    //   10: lstore_2
    //   11: aload_0
    //   12: ifnull +7 -> 19
    //   15: aload_0
    //   16: invokevirtual 40	android/os/ParcelFileDescriptor:close	()V
    //   19: lload_2
    //   20: invokestatic 37	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   23: areturn
    //   24: astore_1
    //   25: goto +6 -> 31
    //   28: astore_1
    //   29: aconst_null
    //   30: astore_0
    //   31: aload_0
    //   32: ifnull +7 -> 39
    //   35: aload_0
    //   36: invokevirtual 40	android/os/ParcelFileDescriptor:close	()V
    //   39: aload_1
    //   40: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	41	0	paramUri	Uri
    //   0	41	1	paramFile	File
    //   10	10	2	l	long
    // Exception table:
    //   from	to	target	type
    //   6	10	24	finally
    //   1	5	28	finally
  }
  
  private final List a(Uri paramUri, Iterable paramIterable)
  {
    Object localObject1 = new java/util/ArrayList;
    int i = c.a.m.a(paramIterable, 10);
    ((ArrayList)localObject1).<init>(i);
    localObject1 = (Collection)localObject1;
    paramIterable = paramIterable.iterator();
    for (;;)
    {
      boolean bool = paramIterable.hasNext();
      if (!bool) {
        break;
      }
      Object localObject2 = (String)paramIterable.next();
      int j = ((String)localObject2).hashCode();
      int k = -488395321;
      String str;
      if (j != k)
      {
        k = -196041627;
        if (j != k)
        {
          k = 90810505;
          if (j != k)
          {
            k = 91265248;
            if (j == k)
            {
              str = "_size";
              bool = ((String)localObject2).equals(str);
              if (bool)
              {
                localObject2 = a(paramUri);
                break label224;
              }
            }
          }
          else
          {
            str = "_data";
            bool = ((String)localObject2).equals(str);
            if (bool)
            {
              localObject2 = paramUri.toString();
              break label224;
            }
          }
        }
        else
        {
          str = "mime_type";
          bool = ((String)localObject2).equals(str);
          if (bool)
          {
            localObject2 = getType(paramUri);
            break label224;
          }
        }
      }
      else
      {
        str = "_display_name";
        bool = ((String)localObject2).equals(str);
        if (bool)
        {
          localObject2 = paramUri.getLastPathSegment();
          break label224;
        }
      }
      bool = false;
      localObject2 = null;
      label224:
      ((Collection)localObject1).add(localObject2);
    }
    return (List)localObject1;
  }
  
  private static List a(Uri paramUri, Iterable paramIterable, File paramFile)
  {
    Object localObject1 = new java/util/ArrayList;
    int i = c.a.m.a(paramIterable, 10);
    ((ArrayList)localObject1).<init>(i);
    localObject1 = (Collection)localObject1;
    paramIterable = paramIterable.iterator();
    for (;;)
    {
      boolean bool = paramIterable.hasNext();
      if (!bool) {
        break;
      }
      Object localObject2 = (String)paramIterable.next();
      int j = ((String)localObject2).hashCode();
      int k = -488395321;
      Object localObject3;
      if (j != k)
      {
        k = -196041627;
        if (j != k)
        {
          k = 90810505;
          if (j != k)
          {
            k = 91265248;
            if (j == k)
            {
              localObject3 = "_size";
              bool = ((String)localObject2).equals(localObject3);
              if (bool)
              {
                localObject2 = a(paramUri, paramFile);
                break label277;
              }
            }
          }
          else
          {
            localObject3 = "_data";
            bool = ((String)localObject2).equals(localObject3);
            if (bool)
            {
              localObject2 = paramUri.toString();
              break label277;
            }
          }
        }
        else
        {
          localObject3 = "mime_type";
          bool = ((String)localObject2).equals(localObject3);
          if (bool)
          {
            localObject2 = paramUri.getQueryParameter("mime");
            break label277;
          }
        }
      }
      else
      {
        localObject3 = "_display_name";
        bool = ((String)localObject2).equals(localObject3);
        if (bool)
        {
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>("attachment.");
          localObject3 = MimeTypeMap.getSingleton();
          String str = paramUri.getQueryParameter("mime");
          localObject3 = ((MimeTypeMap)localObject3).getExtensionFromMimeType(str);
          if (localObject3 == null) {
            localObject3 = "bin";
          }
          ((StringBuilder)localObject2).append((String)localObject3);
          localObject2 = ((StringBuilder)localObject2).toString();
          break label277;
        }
      }
      bool = false;
      localObject2 = null;
      label277:
      ((Collection)localObject1).add(localObject2);
    }
    return (List)localObject1;
  }
  
  private final ParcelFileDescriptor b(Uri paramUri)
  {
    try
    {
      localObject1 = paramUri.getLastPathSegment();
      Object localObject2 = "uri.lastPathSegment";
      c.g.b.k.a(localObject1, (String)localObject2);
      localObject1 = (CharSequence)localObject1;
      boolean bool = true;
      localObject2 = new char[bool];
      int i = 46;
      localObject2[0] = i;
      i = 6;
      localObject1 = c.n.m.a((CharSequence)localObject1, (char[])localObject2, 0, i);
      localObject1 = c.a.m.e((List)localObject1);
      localObject1 = (String)localObject1;
      if (localObject1 != null)
      {
        localObject2 = localObject1;
        localObject2 = (CharSequence)localObject1;
        bool = org.c.a.a.a.k.g((CharSequence)localObject2);
        if (bool)
        {
          paramUri = b.b();
          paramUri = paramUri.buildUpon();
          paramUri = paramUri.appendPath((String)localObject1);
          paramUri = paramUri.build();
          localObject1 = getContext();
          localObject2 = "context";
          c.g.b.k.a(localObject1, (String)localObject2);
          localObject1 = ((Context)localObject1).getContentResolver();
          localObject2 = "r";
          paramUri = ((ContentResolver)localObject1).openFileDescriptor(paramUri, (String)localObject2);
          localObject1 = "context.contentResolver.…scriptor(attachment, \"r\")";
          c.g.b.k.a(paramUri, (String)localObject1);
          return paramUri;
        }
        localObject1 = new java/io/FileNotFoundException;
        paramUri = paramUri.toString();
        ((FileNotFoundException)localObject1).<init>(paramUri);
        localObject1 = (Throwable)localObject1;
        throw ((Throwable)localObject1);
      }
      localObject1 = new java/io/FileNotFoundException;
      paramUri = paramUri.toString();
      ((FileNotFoundException)localObject1).<init>(paramUri);
      localObject1 = (Throwable)localObject1;
      throw ((Throwable)localObject1);
    }
    catch (IOException paramUri)
    {
      Object localObject1 = new java/io/FileNotFoundException;
      paramUri = paramUri.getMessage();
      ((FileNotFoundException)localObject1).<init>(paramUri);
      throw ((Throwable)localObject1);
    }
  }
  
  private static ParcelFileDescriptor b(Uri paramUri, File paramFile)
  {
    paramFile = b.a(paramUri, paramFile);
    if (paramFile != null)
    {
      boolean bool = paramFile.exists();
      if (bool)
      {
        paramUri = ParcelFileDescriptor.open(paramFile, 268435456);
        c.g.b.k.a(paramUri, "ParcelFileDescriptor.ope…escriptor.MODE_READ_ONLY)");
        return paramUri;
      }
    }
    paramFile = new java/io/FileNotFoundException;
    paramUri = paramUri.toString();
    paramFile.<init>(paramUri);
    throw ((Throwable)paramFile);
  }
  
  private final File b()
  {
    File localFile = new java/io/File;
    Object localObject = getContext();
    c.g.b.k.a(localObject, "context");
    localObject = ((Context)localObject).getFilesDir();
    localFile.<init>((File)localObject, "media");
    return localFile;
  }
  
  private static f c(Uri paramUri)
  {
    paramUri = paramUri.getPathSegments();
    String str = "contentUri.pathSegments";
    c.g.b.k.a(paramUri, str);
    paramUri = (String)c.a.m.e(paramUri);
    if (paramUri != null)
    {
      int i = paramUri.hashCode();
      int j = 108243;
      boolean bool;
      if (i != j)
      {
        j = 870994574;
        if (i != j)
        {
          j = 959285800;
          if (i == j)
          {
            str = "private_media";
            bool = paramUri.equals(str);
            if (bool) {
              return f.b;
            }
          }
        }
        else
        {
          str = "public_media";
          bool = paramUri.equals(str);
          if (bool) {
            return f.c;
          }
        }
      }
      else
      {
        str = "mms";
        bool = paramUri.equals(str);
        if (bool) {
          return f.d;
        }
      }
    }
    return f.a;
  }
  
  public final int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    c.g.b.k.b(paramUri, "uri");
    paramString = paramUri.getPathSegments();
    paramArrayOfString = "uri.pathSegments";
    c.g.b.k.a(paramString, paramArrayOfString);
    paramString = (String)c.a.m.e(paramString);
    if (paramString != null)
    {
      int i = paramString.hashCode();
      int j = 870994574;
      if (i != j)
      {
        j = 959285800;
        if (i == j)
        {
          paramArrayOfString = "private_media";
          bool1 = paramString.equals(paramArrayOfString);
          if (bool1)
          {
            paramString = b();
            paramUri = b.a(paramUri, paramString);
            break label133;
          }
        }
      }
      else
      {
        paramArrayOfString = "public_media";
        bool1 = paramString.equals(paramArrayOfString);
        if (bool1)
        {
          paramString = a();
          paramUri = b.a(paramUri, paramString);
          break label133;
        }
      }
    }
    boolean bool2 = false;
    paramUri = null;
    label133:
    boolean bool1 = false;
    paramString = null;
    if (paramUri == null) {
      return 0;
    }
    bool2 = paramUri.delete();
    if (bool2) {
      return 1;
    }
    return 0;
  }
  
  public final String getType(Uri paramUri)
  {
    if (paramUri != null) {
      return paramUri.getQueryParameter("mime");
    }
    return null;
  }
  
  public final boolean onCreate()
  {
    return true;
  }
  
  public final ParcelFileDescriptor openFile(Uri paramUri, String paramString)
  {
    c.g.b.k.b(paramUri, "uri");
    c.g.b.k.b(paramString, "mode");
    Object localObject = "r";
    boolean bool = c.g.b.k.a(paramString, localObject) ^ true;
    if (!bool)
    {
      paramString = c(paramUri);
      localObject = a.b;
      int i = paramString.ordinal();
      i = localObject[i];
      switch (i)
      {
      default: 
        paramUri = new c/l;
        paramUri.<init>();
        throw paramUri;
      case 4: 
        paramString = new java/lang/IllegalArgumentException;
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>("Unsupported path: ");
        paramUri = paramUri.toString();
        ((StringBuilder)localObject).append(paramUri);
        paramUri = ((StringBuilder)localObject).toString();
        paramString.<init>(paramUri);
        throw ((Throwable)paramString);
      case 3: 
        paramString = a();
        return b(paramUri, paramString);
      case 2: 
        paramString = b();
        return b(paramUri, paramString);
      }
      return b(paramUri);
    }
    paramUri = new java/lang/IllegalArgumentException;
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>("Open mode \"");
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append("\" is not allowed");
    paramString = ((StringBuilder)localObject).toString();
    paramUri.<init>(paramString);
    throw ((Throwable)paramUri);
  }
  
  public final Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
  {
    c.g.b.k.b(paramUri, "uri");
    return query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2, null);
  }
  
  public final Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal)
  {
    c.g.b.k.b(paramUri, "uri");
    paramString1 = c(paramUri);
    if (paramArrayOfString1 == null) {
      paramString2 = new String[0];
    } else {
      paramString2 = paramArrayOfString1;
    }
    paramString2 = c.a.f.i(paramString2);
    paramCancellationSignal = b.a();
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    int i = paramCancellationSignal.length;
    int j = 0;
    int k;
    for (;;)
    {
      k = 1;
      if (j >= i) {
        break;
      }
      Object localObject2 = paramCancellationSignal[j];
      if (paramArrayOfString1 != null)
      {
        int m = c.a.f.b(paramArrayOfString1, localObject2);
        if (m == k) {}
      }
      else
      {
        k = 0;
      }
      if (k == 0) {
        ((Collection)localObject1).add(localObject2);
      }
      j += 1;
    }
    localObject1 = (Iterable)localObject1;
    c.g.b.k.b(paramString2, "receiver$0");
    paramArrayOfString1 = "elements";
    c.g.b.k.b(localObject1, paramArrayOfString1);
    boolean bool = paramString2 instanceof Collection;
    if (bool)
    {
      paramString2 = (Collection)paramString2;
      paramArrayOfString1 = c.a.m.c(paramString2, (Iterable)localObject1);
    }
    else
    {
      paramArrayOfString1 = new java/util/ArrayList;
      paramArrayOfString1.<init>();
      paramCancellationSignal = paramArrayOfString1;
      paramCancellationSignal = (Collection)paramArrayOfString1;
      c.a.m.a(paramCancellationSignal, paramString2);
      c.a.m.a(paramCancellationSignal, (Iterable)localObject1);
      paramArrayOfString1 = (List)paramArrayOfString1;
    }
    paramString2 = paramArrayOfString1;
    paramString2 = (Collection)paramArrayOfString1;
    paramCancellationSignal = new String[0];
    paramString2 = paramString2.toArray(paramCancellationSignal);
    if (paramString2 != null)
    {
      paramString2 = (String[])paramString2;
      paramCancellationSignal = new android/database/MatrixCursor;
      paramCancellationSignal.<init>(paramString2, k);
      paramString2 = a.a;
      int n = paramString1.ordinal();
      n = paramString2[n];
      switch (n)
      {
      default: 
        paramUri = new c/l;
        paramUri.<init>();
        throw paramUri;
      case 4: 
        paramArrayOfString1 = new java/lang/IllegalArgumentException;
        paramUri = String.valueOf(paramUri);
        paramUri = "Unsupported uri: ".concat(paramUri);
        paramArrayOfString1.<init>(paramUri);
        throw ((Throwable)paramArrayOfString1);
      case 3: 
        paramArrayOfString1 = (Iterable)paramArrayOfString1;
        paramString1 = a();
        paramUri = a(paramUri, paramArrayOfString1, paramString1);
        break;
      case 2: 
        paramArrayOfString1 = (Iterable)paramArrayOfString1;
        paramString1 = b();
        paramUri = a(paramUri, paramArrayOfString1, paramString1);
        break;
      case 1: 
        paramArrayOfString1 = (Iterable)paramArrayOfString1;
        paramUri = a(paramUri, paramArrayOfString1);
      }
      paramUri = (Collection)paramUri;
      paramArrayOfString1 = new Object[0];
      paramUri = paramUri.toArray(paramArrayOfString1);
      if (paramUri != null)
      {
        paramCancellationSignal.addRow(paramUri);
        return (Cursor)paramCancellationSignal;
      }
      paramUri = new c/u;
      paramUri.<init>("null cannot be cast to non-null type kotlin.Array<T>");
      throw paramUri;
    }
    paramUri = new c/u;
    paramUri.<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw paramUri;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.providers.AttachmentProvider
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */