package com.truecaller.messaging.data.providers;

import android.net.Uri;
import com.truecaller.messaging.data.types.BinaryEntity;
import java.io.File;
import java.util.List;

public abstract interface c
{
  public abstract Uri a(BinaryEntity paramBinaryEntity);
  
  public abstract Uri a(File paramFile, String paramString, boolean paramBoolean);
  
  public abstract File a(long paramLong);
  
  public abstract File a(long paramLong, String paramString);
  
  public abstract File a(Uri paramUri);
  
  public abstract void a(List paramList);
  
  public abstract boolean a();
  
  public abstract Iterable b(long paramLong);
  
  public abstract boolean b(Uri paramUri);
  
  public abstract boolean c(Uri paramUri);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.providers.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */