package com.truecaller.messaging.data.providers;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Environment;
import android.webkit.MimeTypeMap;
import c.a.an;
import c.a.f;
import c.a.m;
import c.a.y;
import c.g.b.k;
import c.u;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.utils.l;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.c.a.a.a.i;

public final class d
  implements c
{
  private final SimpleDateFormat a;
  private final Context b;
  private final l c;
  
  public d(Context paramContext, l paraml)
  {
    b = paramContext;
    c = paraml;
    paramContext = new java/text/SimpleDateFormat;
    Locale localLocale = Locale.US;
    paramContext.<init>("yyMMdd-HHmmss", localLocale);
    a = paramContext;
  }
  
  private static File b()
  {
    File localFile1 = new java/io/File;
    File localFile2 = Environment.getExternalStorageDirectory();
    localFile1.<init>(localFile2, "Truecaller");
    return localFile1;
  }
  
  private final File c()
  {
    File localFile1 = new java/io/File;
    File localFile2 = b.getFilesDir();
    localFile1.<init>(localFile2, "media");
    return localFile1;
  }
  
  public final Uri a(BinaryEntity paramBinaryEntity)
  {
    k.b(paramBinaryEntity, "entity");
    Object localObject1 = b;
    k.a(localObject1, "entity.content");
    localObject1 = ((Uri)localObject1).getScheme();
    Object localObject2 = "content";
    boolean bool1 = k.a(localObject1, localObject2);
    if (bool1)
    {
      localObject1 = b;
      k.a(localObject1, "entity.content");
      localObject1 = ((Uri)localObject1).getAuthority();
      localObject2 = "com.truecaller.attachmentprovider";
      bool1 = k.a(localObject1, localObject2);
      if (bool1) {
        return b;
      }
    }
    localObject1 = b.b();
    k.a(localObject1, "MMS_PART_URI");
    localObject1 = ((Uri)localObject1).getScheme();
    localObject2 = b;
    k.a(localObject2, "entity.content");
    localObject2 = ((Uri)localObject2).getScheme();
    bool1 = k.a(localObject1, localObject2);
    boolean bool2 = true;
    bool1 ^= bool2;
    StringBuilder localStringBuilder = null;
    if (!bool1)
    {
      localObject1 = b.b();
      k.a(localObject1, "MMS_PART_URI");
      localObject1 = ((Uri)localObject1).getAuthority();
      Object localObject3 = b;
      Object localObject4 = "entity.content";
      k.a(localObject3, (String)localObject4);
      localObject3 = ((Uri)localObject3).getAuthority();
      bool1 = k.a(localObject1, localObject3) ^ bool2;
      if (!bool1)
      {
        localObject1 = b.b();
        k.a(localObject1, "MMS_PART_URI");
        localObject1 = ((Uri)localObject1).getPathSegments().iterator();
        localObject3 = b;
        k.a(localObject3, "entity.content");
        localObject3 = ((Uri)localObject3).getPathSegments();
        k.a(localObject3, "entity.content.pathSegments");
        localObject3 = (Iterable)localObject3;
        localObject4 = new java/util/ArrayList;
        ((ArrayList)localObject4).<init>();
        localObject4 = (Collection)localObject4;
        localObject3 = ((Iterable)localObject3).iterator();
        boolean bool3;
        for (;;)
        {
          bool3 = ((Iterator)localObject3).hasNext();
          if (!bool3) {
            break;
          }
          Object localObject5 = ((Iterator)localObject3).next();
          Object localObject6 = localObject5;
          localObject6 = (String)localObject5;
          boolean bool4 = ((Iterator)localObject1).hasNext();
          if (bool4)
          {
            String str = (String)((Iterator)localObject1).next();
            bool5 = k.a(localObject6, str);
            if (bool5)
            {
              bool5 = true;
              break label351;
            }
          }
          boolean bool5 = false;
          localObject6 = null;
          label351:
          if (!bool5) {
            ((Collection)localObject4).add(localObject5);
          }
        }
        localObject4 = (Iterable)localObject4;
        localObject3 = ((Iterable)localObject4).iterator();
        do
        {
          bool6 = ((Iterator)localObject3).hasNext();
          if (!bool6) {
            break;
          }
          localObject4 = ((Iterator)localObject3).next();
          bool3 = ((Iterator)localObject1).hasNext() ^ bool2;
        } while (!bool3);
        break label436;
        boolean bool6 = false;
        localObject4 = null;
        label436:
        localObject4 = (String)localObject4;
        if (localObject4 == null) {
          return null;
        }
        localObject1 = MimeTypeMap.getSingleton();
        localObject2 = j;
        localObject1 = ((MimeTypeMap)localObject1).getExtensionFromMimeType((String)localObject2);
        if (localObject1 == null) {
          localObject1 = "bin";
        }
        localObject2 = new android/net/Uri$Builder;
        ((Uri.Builder)localObject2).<init>();
        ((Uri.Builder)localObject2).scheme("content");
        ((Uri.Builder)localObject2).authority("com.truecaller.attachmentprovider");
        ((Uri.Builder)localObject2).appendPath("mms");
        localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append((String)localObject4);
        localStringBuilder.append('.');
        localStringBuilder.append((String)localObject1);
        localObject1 = localStringBuilder.toString();
        ((Uri.Builder)localObject2).appendEncodedPath((String)localObject1);
        paramBinaryEntity = j;
        ((Uri.Builder)localObject2).appendQueryParameter("mime", paramBinaryEntity);
        return ((Uri.Builder)localObject2).build();
      }
    }
    return null;
  }
  
  public final Uri a(File paramFile, String paramString, boolean paramBoolean)
  {
    k.b(paramFile, "file");
    k.b(paramString, "contentType");
    Uri.Builder localBuilder = new android/net/Uri$Builder;
    localBuilder.<init>();
    localBuilder.scheme("content");
    String str = "com.truecaller.attachmentprovider";
    localBuilder.authority(str);
    if (paramBoolean) {
      localObject = "public_media";
    } else {
      localObject = "private_media";
    }
    localBuilder.appendPath((String)localObject);
    Object localObject = paramFile.getParentFile();
    k.a(localObject, "file.parentFile");
    localObject = ((File)localObject).getName();
    localBuilder.appendPath((String)localObject);
    paramFile = paramFile.getName();
    localBuilder.appendPath(paramFile);
    localBuilder.appendQueryParameter("mime", paramString);
    paramString = String.valueOf(System.currentTimeMillis());
    localBuilder.appendQueryParameter("tmp", paramString);
    return localBuilder.build();
  }
  
  public final File a(long paramLong)
  {
    File localFile1 = new java/io/File;
    File localFile2 = b.getFilesDir();
    String str1 = "media/";
    Object localObject = String.valueOf(paramLong);
    localObject = str1.concat((String)localObject);
    localFile1.<init>(localFile2, (String)localObject);
    boolean bool = localFile1.exists();
    if (!bool)
    {
      bool = localFile1.mkdirs();
      if (!bool) {
        return null;
      }
    }
    localObject = new java/io/File;
    String str2 = i.b(28);
    ((File)localObject).<init>(localFile1, str2);
    return (File)localObject;
  }
  
  public final File a(long paramLong, String paramString)
  {
    k.b(paramString, "contentType");
    File localFile1 = new java/io/File;
    File localFile2 = b();
    Object localObject1 = new java/lang/StringBuilder;
    Object localObject2 = "Truecaller ";
    ((StringBuilder)localObject1).<init>((String)localObject2);
    boolean bool1 = Entity.c(paramString);
    if (bool1)
    {
      localObject2 = "Images";
    }
    else
    {
      bool1 = Entity.d(paramString);
      if (bool1)
      {
        localObject2 = "Video";
      }
      else
      {
        bool1 = Entity.e(paramString);
        if (bool1) {
          localObject2 = "Audio";
        } else {
          localObject2 = "Documents";
        }
      }
    }
    ((StringBuilder)localObject1).append((String)localObject2);
    localObject1 = ((StringBuilder)localObject1).toString();
    localFile1.<init>(localFile2, (String)localObject1);
    boolean bool2 = localFile1.exists();
    if (!bool2)
    {
      bool2 = localFile1.mkdirs();
      if (!bool2) {
        return null;
      }
    }
    localFile2 = new java/io/File;
    boolean bool3 = Entity.c(paramString);
    if (bool3)
    {
      localObject1 = "IMG";
    }
    else
    {
      bool3 = Entity.d(paramString);
      if (bool3)
      {
        localObject1 = "VID";
      }
      else
      {
        bool3 = Entity.e(paramString);
        if (bool3) {
          localObject1 = "AUD";
        } else {
          localObject1 = "DOC";
        }
      }
    }
    localObject2 = MimeTypeMap.getSingleton();
    paramString = ((MimeTypeMap)localObject2).getExtensionFromMimeType(paramString);
    if (paramString == null)
    {
      paramString = "";
    }
    else
    {
      localObject2 = ".";
      paramString = String.valueOf(paramString);
      paramString = ((String)localObject2).concat(paramString);
    }
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    ((StringBuilder)localObject2).append((String)localObject1);
    char c1 = '-';
    ((StringBuilder)localObject2).append(c1);
    Object localObject3 = a;
    Date localDate = new java/util/Date;
    localDate.<init>();
    localObject3 = ((SimpleDateFormat)localObject3).format(localDate);
    ((StringBuilder)localObject2).append((String)localObject3);
    ((StringBuilder)localObject2).append(c1);
    ((StringBuilder)localObject2).append(paramLong);
    ((StringBuilder)localObject2).append(paramString);
    String str = ((StringBuilder)localObject2).toString();
    localFile2.<init>(localFile1, str);
    return localFile2;
  }
  
  public final File a(Uri paramUri)
  {
    k.b(paramUri, "uri");
    Object localObject = paramUri.getScheme();
    boolean bool = k.a(localObject, "content") ^ true;
    if (!bool)
    {
      localObject = paramUri.getAuthority();
      String str = "com.truecaller.attachmentprovider";
      bool = k.a(localObject, str) ^ true;
      if (!bool)
      {
        localObject = paramUri.getPathSegments();
        str = "uri.pathSegments";
        k.a(localObject, str);
        localObject = (String)m.e((List)localObject);
        if (localObject != null)
        {
          int i = ((String)localObject).hashCode();
          int j = 870994574;
          if (i != j)
          {
            j = 959285800;
            if (i == j)
            {
              str = "private_media";
              bool = ((String)localObject).equals(str);
              if (bool)
              {
                localObject = c();
                return b.a(paramUri, (File)localObject);
              }
            }
          }
          else
          {
            str = "public_media";
            bool = ((String)localObject).equals(str);
            if (bool)
            {
              localObject = b();
              return b.a(paramUri, (File)localObject);
            }
          }
        }
        return null;
      }
    }
    return null;
  }
  
  public final void a(List paramList)
  {
    Object localObject = "filePaths";
    k.b(paramList, (String)localObject);
    paramList = (Collection)paramList;
    boolean bool = paramList.isEmpty() ^ true;
    if (bool)
    {
      localObject = b;
      String[] arrayOfString = new String[0];
      paramList = paramList.toArray(arrayOfString);
      if (paramList != null)
      {
        paramList = (String[])paramList;
        arrayOfString = null;
        MediaScannerConnection.OnScanCompletedListener localOnScanCompletedListener = (MediaScannerConnection.OnScanCompletedListener)d.a.a;
        MediaScannerConnection.scanFile((Context)localObject, paramList, null, localOnScanCompletedListener);
      }
      else
      {
        paramList = new c/u;
        paramList.<init>("null cannot be cast to non-null type kotlin.Array<T>");
        throw paramList;
      }
    }
  }
  
  public final boolean a()
  {
    Object localObject = Environment.getExternalStorageState();
    String str = "mounted";
    boolean bool = k.a(localObject, str);
    if (bool)
    {
      localObject = c;
      bool = ((l)localObject).c();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public final Iterable b(long paramLong)
  {
    File localFile1 = new java/io/File;
    File localFile2 = c();
    Object localObject = String.valueOf(paramLong);
    localFile1.<init>(localFile2, (String)localObject);
    boolean bool = localFile1.exists();
    if (!bool) {
      return (Iterable)y.a;
    }
    localObject = localFile1.listFiles();
    if (localObject != null)
    {
      localObject = f.i((Object[])localObject);
      if (localObject != null) {}
    }
    else
    {
      localObject = (Iterable)y.a;
    }
    return (Iterable)localObject;
  }
  
  public final boolean b(Uri paramUri)
  {
    k.b(paramUri, "uri");
    Object localObject = paramUri.getScheme();
    String str = "content";
    boolean bool1 = k.a(localObject, str);
    if (bool1)
    {
      localObject = paramUri.getAuthority();
      str = "com.truecaller.attachmentprovider";
      bool1 = k.a(localObject, str);
      if (bool1)
      {
        localObject = (Iterable)an.a(new String[] { "private_media", "public_media" });
        paramUri = paramUri.getPathSegments();
        str = "uri.pathSegments";
        k.a(paramUri, str);
        paramUri = m.e(paramUri);
        boolean bool2 = m.a((Iterable)localObject, paramUri);
        if (bool2) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final boolean c(Uri paramUri)
  {
    k.b(paramUri, "uri");
    paramUri = paramUri.getPathSegments();
    k.a(paramUri, "uri.pathSegments");
    paramUri = (String)m.e(paramUri);
    return k.a("public_media", paramUri);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.providers.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */