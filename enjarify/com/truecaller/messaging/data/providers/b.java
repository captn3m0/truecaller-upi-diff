package com.truecaller.messaging.data.providers;

import android.net.Uri;
import c.g.b.k;
import java.io.File;

public final class b
{
  private static final Uri a = Uri.parse("content://mms/part");
  private static final String[] b = tmp26_13;
  
  static
  {
    String[] tmp12_9 = new String[4];
    String[] tmp13_12 = tmp12_9;
    String[] tmp13_12 = tmp12_9;
    tmp13_12[0] = "_display_name";
    tmp13_12[1] = "_size";
    tmp13_12[2] = "_data";
    String[] tmp26_13 = tmp13_12;
    tmp26_13[3] = "mime_type";
  }
  
  private static final boolean a(File paramFile1, File paramFile2)
  {
    while (paramFile1 != null)
    {
      boolean bool = k.a(paramFile2, paramFile1);
      if (bool) {
        return true;
      }
      paramFile1 = paramFile1.getParentFile();
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.providers.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */