package com.truecaller.messaging.data.providers;

import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;

final class d$a
  implements MediaScannerConnection.OnScanCompletedListener
{
  public static final a a;
  
  static
  {
    a locala = new com/truecaller/messaging/data/providers/d$a;
    locala.<init>();
    a = locala;
  }
  
  public final void onScanCompleted(String paramString, Uri paramUri)
  {
    paramUri = new String[1];
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Scanned path ");
    localStringBuilder.append(paramString);
    localStringBuilder.append(" completed");
    paramString = localStringBuilder.toString();
    paramUri[0] = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.providers.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */