package com.truecaller.messaging.data;

import android.database.Cursor;
import com.truecaller.messaging.data.a.a;
import com.truecaller.messaging.data.a.b;
import com.truecaller.messaging.data.a.e;
import com.truecaller.messaging.data.a.i;
import com.truecaller.messaging.data.a.j;
import com.truecaller.messaging.data.a.k;
import com.truecaller.messaging.data.a.l;
import com.truecaller.messaging.data.a.m;
import com.truecaller.messaging.data.a.o;
import com.truecaller.messaging.data.a.p;
import com.truecaller.messaging.data.a.q;
import com.truecaller.messaging.data.a.r;

final class d
  implements c
{
  public final a a(Cursor paramCursor)
  {
    if (paramCursor != null)
    {
      b localb = new com/truecaller/messaging/data/a/b;
      localb.<init>(paramCursor);
      return localb;
    }
    return null;
  }
  
  public final com.truecaller.messaging.data.a.c a(Cursor paramCursor, boolean paramBoolean)
  {
    if (paramCursor == null) {
      return null;
    }
    if (paramBoolean)
    {
      localObject = new com/truecaller/messaging/data/a/e;
      ((e)localObject).<init>(paramCursor);
      return (com.truecaller.messaging.data.a.c)localObject;
    }
    Object localObject = new com/truecaller/messaging/data/a/d;
    ((com.truecaller.messaging.data.a.d)localObject).<init>(paramCursor);
    return (com.truecaller.messaging.data.a.c)localObject;
  }
  
  public final j b(Cursor paramCursor)
  {
    if (paramCursor != null)
    {
      s locals = new com/truecaller/messaging/data/s;
      locals.<init>(paramCursor);
      return locals;
    }
    return null;
  }
  
  public final m c(Cursor paramCursor)
  {
    if (paramCursor != null)
    {
      aa localaa = new com/truecaller/messaging/data/aa;
      localaa.<init>(paramCursor);
      return localaa;
    }
    return null;
  }
  
  public final r d(Cursor paramCursor)
  {
    if (paramCursor != null)
    {
      ai localai = new com/truecaller/messaging/data/ai;
      localai.<init>(paramCursor);
      return localai;
    }
    return null;
  }
  
  public final com.truecaller.messaging.data.a.s e(Cursor paramCursor)
  {
    if (paramCursor != null)
    {
      aj localaj = new com/truecaller/messaging/data/aj;
      localaj.<init>(paramCursor);
      return localaj;
    }
    return null;
  }
  
  public final com.truecaller.messaging.data.a.g f(Cursor paramCursor)
  {
    if (paramCursor != null)
    {
      n localn = new com/truecaller/messaging/data/n;
      localn.<init>(paramCursor);
      return localn;
    }
    return null;
  }
  
  public final p g(Cursor paramCursor)
  {
    if (paramCursor != null)
    {
      q localq = new com/truecaller/messaging/data/a/q;
      localq.<init>(paramCursor);
      return localq;
    }
    return null;
  }
  
  public final k h(Cursor paramCursor)
  {
    if (paramCursor != null)
    {
      l locall = new com/truecaller/messaging/data/a/l;
      locall.<init>(paramCursor);
      return locall;
    }
    return null;
  }
  
  public final com.truecaller.messaging.transport.im.a.g i(Cursor paramCursor)
  {
    if (paramCursor != null)
    {
      com.truecaller.messaging.transport.im.a.h localh = new com/truecaller/messaging/transport/im/a/h;
      localh.<init>(paramCursor);
      return localh;
    }
    return null;
  }
  
  public final com.truecaller.messaging.data.a.h j(Cursor paramCursor)
  {
    if (paramCursor != null)
    {
      i locali = new com/truecaller/messaging/data/a/i;
      locali.<init>(paramCursor);
      return locali;
    }
    return null;
  }
  
  public final com.truecaller.messaging.data.a.n k(Cursor paramCursor)
  {
    if (paramCursor != null)
    {
      o localo = new com/truecaller/messaging/data/a/o;
      localo.<init>(paramCursor);
      return localo;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */