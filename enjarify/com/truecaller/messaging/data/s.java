package com.truecaller.messaging.data;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import c.a.ae;
import c.a.y;
import c.g.b.k;
import c.u;
import com.truecaller.backup.BackupTransportInfo;
import com.truecaller.messaging.data.a.j;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Participant.a;
import com.truecaller.messaging.data.types.Reaction;
import com.truecaller.messaging.data.types.ReplySnippet;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.transport.NullTransportInfo.a;
import com.truecaller.messaging.transport.history.HistoryTransportInfo;
import com.truecaller.messaging.transport.im.ImTransportInfo.a;
import com.truecaller.messaging.transport.mms.MmsTransportInfo.a;
import com.truecaller.messaging.transport.sms.SmsTransportInfo.a;
import com.truecaller.messaging.transport.status.StatusTransportInfo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public final class s
  extends a
  implements j
{
  private final int A;
  private final int B;
  private final int C;
  private final int D;
  private final int E;
  private final int F;
  private final int G;
  private final int H;
  private final int I;
  private final int J;
  private final int K;
  private final int L;
  private final int M;
  private final int N;
  private final int O;
  private final int P;
  private final int Q;
  private final int R;
  private final int S;
  private final int T;
  private final int U;
  private final int V;
  private final int W;
  private final int X;
  private final int Y;
  private final int Z;
  private final int a;
  private final int aA;
  private final int aB;
  private final int aC;
  private final int aD;
  private final int aE;
  private final int aF;
  private final int aG;
  private final int aH;
  private final int aI;
  private final int aJ;
  private final int aK;
  private final int aL;
  private final int aM;
  private final int aN;
  private final int aO;
  private final int aP;
  private final int aQ;
  private final int aR;
  private final int aS;
  private final int aT;
  private final int aU;
  private final int aa;
  private final int ab;
  private final int ac;
  private final int ad;
  private final int ae;
  private final int af;
  private final int ag;
  private final int ah;
  private final int ai;
  private final int aj;
  private final int ak;
  private final int al;
  private final int am;
  private final int an;
  private final int ao;
  private final int ap;
  private final int aq;
  private final int ar;
  private final int as;
  private final int at;
  private final int au;
  private final int av;
  private final int aw;
  private final int ax;
  private final int ay;
  private final int az;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  private final int j;
  private final int k;
  private final int l;
  private final int m;
  private final int n;
  private final int o;
  private final int p;
  private final int q;
  private final int r;
  private final int s;
  private final int t;
  private final int u;
  private final int v;
  private final int w;
  private final int x;
  private final int y;
  private final int z;
  
  public s(Cursor paramCursor)
  {
    super(paramCursor);
    int i1 = paramCursor.getColumnIndexOrThrow("_id");
    a = i1;
    i1 = paramCursor.getColumnIndexOrThrow("conversation_id");
    b = i1;
    i1 = paramCursor.getColumnIndexOrThrow("date");
    c = i1;
    i1 = paramCursor.getColumnIndexOrThrow("date_sent");
    d = i1;
    i1 = paramCursor.getColumnIndexOrThrow("status");
    e = i1;
    i1 = paramCursor.getColumnIndexOrThrow("seen");
    f = i1;
    i1 = paramCursor.getColumnIndexOrThrow("read");
    g = i1;
    i1 = paramCursor.getColumnIndexOrThrow("locked");
    h = i1;
    i1 = paramCursor.getColumnIndexOrThrow("transport");
    i = i1;
    i1 = paramCursor.getColumnIndexOrThrow("scheduled_transport");
    j = i1;
    i1 = paramCursor.getColumnIndexOrThrow("sim_token");
    k = i1;
    i1 = paramCursor.getColumnIndexOrThrow("raw_address");
    l = i1;
    i1 = paramCursor.getColumnIndexOrThrow("retry_count");
    m = i1;
    i1 = paramCursor.getColumnIndexOrThrow("retry_date");
    n = i1;
    i1 = paramCursor.getColumnIndexOrThrow("reply_to_msg_id");
    o = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participant_id");
    p = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_participant_type");
    q = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_participant_im_id");
    r = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_participant_raw_destination");
    s = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_participant_normalized_destination");
    t = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_participant_country_code");
    u = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_participant_tc_id");
    v = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_participant_aggregated_contact_id");
    w = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_participant_filter_action");
    x = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_participant_is_top_spammer");
    y = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_participant_top_spam_score");
    z = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_participant_name");
    A = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_participant_image_url");
    B = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_participant_source");
    C = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_participant_badges");
    D = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_participant_company_name");
    E = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_participant_search_time");
    F = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_participant_phonebook_id");
    G = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_participant_spam_score");
    H = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_entities_id");
    I = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_entities_type");
    J = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_entities_content");
    K = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_entities_width");
    L = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_entities_height");
    M = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_entities_duration");
    N = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_entities_status");
    O = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_entities_size");
    P = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_entities_thumnail");
    Q = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_raw_id");
    R = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_raw_status");
    S = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_raw_thread");
    T = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_message_uri");
    U = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_sms_protocol");
    V = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_sms_type");
    W = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_sms_service_center");
    X = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_sms_subject");
    Y = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_sms_error_code");
    Z = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_sms_reply_path_present");
    aa = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_sms_stripped_raw_address");
    ab = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_message_id");
    ac = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_transaction_id");
    ad = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_subject");
    ae = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_subject_charset");
    af = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_content_location");
    ag = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_expiry");
    ah = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_priority");
    ai = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_size");
    aj = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_retrieve_status");
    ak = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_response_status");
    al = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_message_box");
    am = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_type");
    an = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_delivery_report");
    ao = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_delivery_time");
    ap = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_version");
    aq = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_retrieve_text");
    ar = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_retrieve_text_charset");
    as = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_content_type");
    at = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_content_class");
    au = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_response_text");
    av = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_message_class");
    aw = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_read_report");
    ax = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_read_status");
    ay = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_mms_report_allowed");
    az = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_im_status");
    aA = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_im_delivery_status");
    aB = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_im_read_status");
    aC = i1;
    i1 = paramCursor.getColumnIndexOrThrow("me_im_read_sync_status");
    aD = i1;
    i1 = paramCursor.getColumnIndexOrThrow("analytics_id");
    aE = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_error_code");
    aF = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_api_version");
    aG = i1;
    i1 = paramCursor.getColumnIndexOrThrow("category");
    aH = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_random_id");
    aI = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_reactions");
    aJ = i1;
    i1 = paramCursor.getColumnIndexOrThrow("re_message_status");
    aK = i1;
    i1 = paramCursor.getColumnIndexOrThrow("re_participant_name");
    aL = i1;
    i1 = paramCursor.getColumnIndexOrThrow("re_participant_normalized_address");
    aM = i1;
    i1 = paramCursor.getColumnIndexOrThrow("re_entities_id");
    aN = i1;
    i1 = paramCursor.getColumnIndexOrThrow("re_entities_type");
    aO = i1;
    i1 = paramCursor.getColumnIndexOrThrow("re_entities_content");
    aP = i1;
    i1 = paramCursor.getColumnIndexOrThrow("re_entities_thumbnail");
    aQ = i1;
    i1 = paramCursor.getColumnIndexOrThrow("h_type");
    aR = i1;
    i1 = paramCursor.getColumnIndexOrThrow("h_number_type");
    aS = i1;
    i1 = paramCursor.getColumnIndexOrThrow("h_call_log_id");
    aT = i1;
    int i2 = paramCursor.getColumnIndexOrThrow("h_features");
    aU = i2;
  }
  
  private final Reaction[] a(long paramLong)
  {
    int i1 = aJ;
    Object localObject1 = getString(i1);
    if (localObject1 != null)
    {
      localObject1 = (CharSequence)localObject1;
      int i2 = 1;
      Object localObject2 = new char[i2];
      localObject2[0] = 124;
      int i3 = 6;
      localObject1 = c.n.m.a((CharSequence)localObject1, (char[])localObject2, 0, i3);
      if (localObject1 != null)
      {
        localObject1 = (Iterable)localObject1;
        localObject2 = new java/util/ArrayList;
        i3 = c.a.m.a((Iterable)localObject1, 10);
        ((ArrayList)localObject2).<init>(i3);
        localObject2 = (Collection)localObject2;
        localObject1 = ((Iterable)localObject1).iterator();
        for (;;)
        {
          boolean bool = ((Iterator)localObject1).hasNext();
          if (!bool) {
            break;
          }
          Object localObject3 = ((Iterator)localObject1).next();
          Object localObject4 = localObject3;
          localObject4 = (String)localObject3;
          localObject3 = new com/truecaller/messaging/data/types/Reaction;
          long l1 = -1;
          String str = "";
          long l2 = -1;
          int i4 = -1;
          ((Reaction)localObject3).<init>(l1, paramLong, str, (String)localObject4, l2, i4);
          ((Collection)localObject2).add(localObject3);
        }
        localObject2 = (Collection)localObject2;
        localObject1 = new Reaction[0];
        localObject1 = ((Collection)localObject2).toArray((Object[])localObject1);
        if (localObject1 != null) {
          return (Reaction[])localObject1;
        }
        localObject1 = new c/u;
        ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.Array<T>");
        throw ((Throwable)localObject1);
      }
    }
    return null;
  }
  
  private static List b(String paramString)
  {
    if (paramString == null) {
      return (List)y.a;
    }
    try
    {
      paramString = (CharSequence)paramString;
      int i1 = 1;
      localObject1 = new char[i1];
      int i2 = 124;
      localObject1[0] = i2;
      i2 = 6;
      paramString = c.n.m.a(paramString, (char[])localObject1, 0, i2);
      paramString = (Iterable)paramString;
      localObject1 = new java/util/ArrayList;
      i2 = 10;
      i2 = c.a.m.a(paramString, i2);
      ((ArrayList)localObject1).<init>(i2);
      localObject1 = (Collection)localObject1;
      paramString = paramString.iterator();
      for (;;)
      {
        boolean bool = paramString.hasNext();
        if (!bool) {
          break;
        }
        localObject2 = paramString.next();
        localObject2 = (String)localObject2;
        long l1 = Long.parseLong((String)localObject2);
        localObject2 = Long.valueOf(l1);
        ((Collection)localObject1).add(localObject2);
      }
      return (List)localObject1;
    }
    catch (NumberFormatException paramString)
    {
      Object localObject1 = new android/database/sqlite/SQLiteException;
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("Can not parse longs: ");
      paramString = paramString.getMessage();
      ((StringBuilder)localObject2).append(paramString);
      paramString = ((StringBuilder)localObject2).toString();
      ((SQLiteException)localObject1).<init>(paramString);
      throw ((Throwable)localObject1);
    }
  }
  
  private final ReplySnippet c()
  {
    int i1 = o;
    long l1 = getLong(i1);
    i1 = 0;
    Object localObject1 = null;
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    if (bool1) {
      return null;
    }
    int i2 = aN;
    Object localObject2 = b(getString(i2));
    int i3 = aO;
    List localList = a(getString(i3));
    k.a(localList, "readComposedStrings(getS…lyToEntitiesGroupedType))");
    int i4 = aP;
    Object localObject3 = a(getString(i4));
    k.a(localObject3, "readComposedStrings(getS…oEntitiesGroupedContent))");
    int i5 = aQ;
    Object localObject4 = a(getString(i5));
    Object localObject5 = "readComposedStrings(getS…eplyToEntitiesThumbnail))";
    k.a(localObject4, (String)localObject5);
    int i6 = ((List)localObject2).size();
    int i7 = localList.size();
    if (i6 == i7)
    {
      i6 = ((List)localObject2).size();
      i7 = ((List)localObject3).size();
      if (i6 == i7)
      {
        i6 = ((List)localObject2).size();
        i7 = ((List)localObject4).size();
        if (i6 == i7)
        {
          boolean bool2 = localList.isEmpty();
          if (bool2) {
            return null;
          }
          localObject5 = new java/util/LinkedHashSet;
          ((LinkedHashSet)localObject5).<init>();
          localObject5 = (Set)localObject5;
          Object localObject6 = localList;
          localObject6 = (Iterable)c.a.m.a((Collection)localList);
          Object localObject7 = new java/util/ArrayList;
          ((ArrayList)localObject7).<init>();
          localObject7 = (Collection)localObject7;
          localObject6 = ((Iterable)localObject6).iterator();
          for (;;)
          {
            boolean bool3 = ((Iterator)localObject6).hasNext();
            if (!bool3) {
              break;
            }
            Object localObject8 = localObject6;
            localObject8 = (ae)localObject6;
            int i8 = ((ae)localObject8).a();
            Object localObject9 = ((List)localObject2).get(i8);
            boolean bool4 = ((Set)localObject5).add(localObject9);
            if (!bool4)
            {
              i8 = 0;
              localObject8 = null;
            }
            else
            {
              localObject9 = (String)localList.get(i8);
              String str = (String)((List)localObject3).get(i8);
              localObject8 = (String)((List)localObject4).get(i8);
              localObject8 = Entity.a((String)localObject9, str, (String)localObject8);
            }
            if (localObject8 != null) {
              ((Collection)localObject7).add(localObject8);
            }
          }
          localObject3 = localObject7;
          localObject3 = (List)localObject7;
          localObject1 = new com/truecaller/messaging/data/types/ReplySnippet;
          i2 = aK;
          i3 = getInt(i2);
          i2 = aL;
          localObject4 = getString(i2);
          i2 = aM;
          localObject5 = getString(i2);
          localObject2 = localObject1;
          ((ReplySnippet)localObject1).<init>(l1, i3, (List)localObject3, (String)localObject4, (String)localObject5);
          return (ReplySnippet)localObject1;
        }
      }
    }
    localObject1 = new android/database/sqlite/SQLiteException;
    ((SQLiteException)localObject1).<init>("Inconsistent entities data");
    throw ((Throwable)localObject1);
  }
  
  public final long a()
  {
    int i1 = a;
    return getLong(i1);
  }
  
  public final Message b()
  {
    s locals = this;
    Object localObject1 = new com/truecaller/messaging/data/types/Message$a;
    ((Message.a)localObject1).<init>();
    int i1 = p;
    boolean bool1 = isNull(i1);
    boolean bool2 = true;
    boolean bool6 = false;
    Object localObject2 = null;
    long l1;
    if (bool1)
    {
      localObject3 = Participant.a;
      ((Message.a)localObject1).a((Participant)localObject3);
    }
    else
    {
      localObject3 = new com/truecaller/messaging/data/types/Participant$a;
      int i12 = q;
      i12 = getInt(i12);
      ((Participant.a)localObject3).<init>(i12);
      i12 = p;
      l1 = getLong(i12);
      localObject3 = ((Participant.a)localObject3).a(l1);
      i12 = s;
      localObject4 = getString(i12);
      localObject3 = ((Participant.a)localObject3).a((String)localObject4);
      i12 = t;
      localObject4 = getString(i12);
      localObject3 = ((Participant.a)localObject3).b((String)localObject4);
      i12 = u;
      localObject4 = getString(i12);
      localObject3 = ((Participant.a)localObject3).c((String)localObject4);
      i12 = v;
      localObject4 = getString(i12);
      localObject3 = ((Participant.a)localObject3).e((String)localObject4);
      i12 = w;
      l1 = getLong(i12);
      localObject3 = ((Participant.a)localObject3).b(l1);
      i12 = r;
      localObject4 = getString(i12);
      localObject3 = ((Participant.a)localObject3).d((String)localObject4);
      i12 = x;
      i12 = getInt(i12);
      localObject3 = ((Participant.a)localObject3).a(i12);
      i12 = y;
      i12 = getInt(i12);
      if (i12 != 0)
      {
        i12 = 1;
      }
      else
      {
        i12 = 0;
        localObject4 = null;
      }
      localObject3 = ((Participant.a)localObject3).a(i12);
      i13 = z;
      i13 = locals.getInt(i13);
      localObject3 = ((Participant.a)localObject3).b(i13);
      i13 = A;
      localObject4 = locals.getString(i13);
      localObject3 = ((Participant.a)localObject3).f((String)localObject4);
      i13 = B;
      localObject4 = locals.getString(i13);
      localObject3 = ((Participant.a)localObject3).g((String)localObject4);
      i13 = C;
      i13 = locals.getInt(i13);
      localObject3 = ((Participant.a)localObject3).c(i13);
      i13 = G;
      l1 = locals.getInt(i13);
      localObject3 = ((Participant.a)localObject3).c(l1);
      i13 = H;
      i13 = locals.getInt(i13);
      localObject3 = ((Participant.a)localObject3).d(i13);
      i13 = D;
      i13 = locals.getInt(i13);
      localObject3 = ((Participant.a)localObject3).e(i13);
      i13 = E;
      localObject4 = locals.getString(i13);
      localObject3 = ((Participant.a)localObject3).h((String)localObject4);
      i13 = F;
      l1 = locals.getLong(i13);
      localObject3 = ((Participant.a)localObject3).d(l1).a();
      localObject4 = "Participant.Builder(getI…me))\n            .build()";
      k.a(localObject3, (String)localObject4);
      ((Message.a)localObject1).a((Participant)localObject3);
    }
    int i2 = I;
    Object localObject3 = b(locals.getString(i2));
    int i13 = J;
    Object localObject4 = a(locals.getString(i13));
    k.a(localObject4, "readComposedStrings(getS…ing(entitiesGroupedType))");
    int i14 = L;
    Object localObject5 = locals.getString(i14);
    int i15 = 124;
    Object localObject6 = null;
    int i16 = 6;
    if (localObject5 != null)
    {
      localObject5 = (CharSequence)localObject5;
      localObject7 = new char[bool2];
      localObject7[0] = i15;
      localObject5 = c.n.m.a((CharSequence)localObject5, (char[])localObject7, 0, i16);
    }
    else
    {
      i14 = 0;
      localObject5 = null;
    }
    int i17 = M;
    Object localObject7 = locals.getString(i17);
    if (localObject7 != null)
    {
      localObject7 = (CharSequence)localObject7;
      localObject8 = new char[bool2];
      localObject8[0] = i15;
      localObject7 = c.n.m.a((CharSequence)localObject7, (char[])localObject8, 0, i16);
    }
    else
    {
      i17 = 0;
      localObject7 = null;
    }
    int i18 = N;
    Object localObject8 = locals.getString(i18);
    if (localObject8 != null)
    {
      localObject8 = (CharSequence)localObject8;
      localObject9 = new char[bool2];
      localObject9[0] = i15;
      localObject8 = c.n.m.a((CharSequence)localObject8, (char[])localObject9, 0, i16);
    }
    else
    {
      i18 = 0;
      localObject8 = null;
    }
    int i19 = O;
    Object localObject9 = locals.getString(i19);
    if (localObject9 != null)
    {
      localObject9 = (CharSequence)localObject9;
      localObject10 = new char[bool2];
      localObject10[0] = i15;
      localObject9 = c.n.m.a((CharSequence)localObject9, (char[])localObject10, 0, i16);
    }
    else
    {
      i19 = 0;
      localObject9 = null;
    }
    int i20 = P;
    Object localObject10 = locals.getString(i20);
    if (localObject10 != null)
    {
      localObject10 = (CharSequence)localObject10;
      localObject6 = new char[bool2];
      localObject6[0] = i15;
      localObject6 = c.n.m.a((CharSequence)localObject10, (char[])localObject6, 0, i16);
    }
    i15 = K;
    List localList = a(locals.getString(i15));
    k.a(localList, "readComposedStrings(getS…(entitiesGroupedContent))");
    i20 = Q;
    localObject10 = a(locals.getString(i20));
    Object localObject11 = "readComposedStrings(getS…ntitiesGroupedThumbnail))";
    k.a(localObject10, (String)localObject11);
    if ((localObject5 != null) && (localObject7 != null) && (localObject8 != null) && (localObject9 != null) && (localObject6 != null))
    {
      int i21 = ((List)localObject3).size();
      int i22 = ((List)localObject4).size();
      if (i21 == i22)
      {
        i21 = ((List)localObject3).size();
        i22 = localList.size();
        if (i21 == i22)
        {
          i21 = ((List)localObject3).size();
          i22 = ((List)localObject10).size();
          if (i21 == i22)
          {
            i21 = ((List)localObject3).size();
            i22 = ((List)localObject5).size();
            if (i21 == i22)
            {
              i21 = ((List)localObject3).size();
              i22 = ((List)localObject7).size();
              if (i21 == i22)
              {
                i21 = ((List)localObject3).size();
                i22 = ((List)localObject8).size();
                if (i21 == i22)
                {
                  i21 = ((List)localObject3).size();
                  i22 = ((List)localObject9).size();
                  if (i21 == i22)
                  {
                    i21 = ((List)localObject3).size();
                    i22 = ((List)localObject6).size();
                    if (i21 == i22)
                    {
                      localObject11 = new java/util/LinkedHashSet;
                      ((LinkedHashSet)localObject11).<init>();
                      localObject11 = (Set)localObject11;
                      Object localObject12 = localObject3;
                      localObject12 = (Collection)localObject3;
                      i22 = ((Collection)localObject12).size();
                      i16 = 0;
                      while (i16 < i22)
                      {
                        Number localNumber = (Number)((List)localObject3).get(i16);
                        long l2 = localNumber.longValue();
                        localObject13 = Long.valueOf(l2);
                        bool2 = ((Set)localObject11).add(localObject13);
                        if (bool2)
                        {
                          localObject13 = c.n.m.b((String)((List)localObject5).get(i16));
                          int i24;
                          if (localObject13 != null)
                          {
                            i3 = ((Integer)localObject13).intValue();
                            int i23 = i3;
                          }
                          else
                          {
                            i24 = -1;
                          }
                          localObject13 = c.n.m.b((String)((List)localObject7).get(i16));
                          int i26;
                          if (localObject13 != null)
                          {
                            i3 = ((Integer)localObject13).intValue();
                            int i25 = i3;
                          }
                          else
                          {
                            i26 = -1;
                          }
                          localObject13 = c.n.m.b((String)((List)localObject8).get(i16));
                          int i28;
                          if (localObject13 != null)
                          {
                            i3 = ((Integer)localObject13).intValue();
                            int i27 = i3;
                          }
                          else
                          {
                            i28 = -1;
                          }
                          localObject13 = c.n.m.b((String)((List)localObject9).get(i16));
                          int i30;
                          if (localObject13 != null)
                          {
                            i3 = ((Integer)localObject13).intValue();
                            int i29 = i3;
                          }
                          else
                          {
                            i30 = 0;
                          }
                          localObject13 = c.n.m.d((String)((List)localObject6).get(i16));
                          long l3;
                          if (localObject13 != null) {
                            l3 = ((Long)localObject13).longValue();
                          } else {
                            l3 = -1;
                          }
                          localObject13 = ((List)localObject4).get(i16);
                          Object localObject14 = localObject13;
                          localObject14 = (String)localObject13;
                          localObject13 = localList.get(i16);
                          Object localObject15 = localObject13;
                          localObject15 = (String)localObject13;
                          localObject13 = ((List)localObject10).get(i16);
                          Object localObject16 = localObject13;
                          localObject16 = (String)localObject13;
                          localObject13 = Entity.a(l2, (String)localObject14, i30, (String)localObject15, i24, i26, i28, l3, (String)localObject16);
                          ((Message.a)localObject1).a((Entity)localObject13);
                        }
                        i16 += 1;
                        i3 = 1;
                      }
                      localList.clear();
                      i2 = a;
                      long l4 = locals.getLong(i2);
                      localObject3 = ((Message.a)localObject1).a(l4);
                      int i3 = b;
                      long l5 = locals.getLong(i3);
                      localObject3 = ((Message.a)localObject3).b(l5);
                      i3 = c;
                      l5 = locals.getLong(i3);
                      localObject3 = ((Message.a)localObject3).c(l5);
                      i3 = d;
                      l5 = locals.getLong(i3);
                      localObject3 = ((Message.a)localObject3).d(l5);
                      i3 = e;
                      i3 = locals.getInt(i3);
                      localObject3 = ((Message.a)localObject3).a(i3);
                      i3 = f;
                      i3 = locals.getInt(i3);
                      if (i3 != 0)
                      {
                        i3 = 1;
                      }
                      else
                      {
                        i3 = 0;
                        localObject13 = null;
                      }
                      localObject3 = ((Message.a)localObject3).a(i3);
                      int i4 = g;
                      i4 = locals.getInt(i4);
                      if (i4 != 0)
                      {
                        i4 = 1;
                      }
                      else
                      {
                        i4 = 0;
                        localObject13 = null;
                      }
                      localObject3 = ((Message.a)localObject3).b(i4);
                      int i5 = h;
                      i5 = locals.getInt(i5);
                      if (i5 != 0)
                      {
                        i5 = 1;
                      }
                      else
                      {
                        i5 = 0;
                        localObject13 = null;
                      }
                      localObject3 = ((Message.a)localObject3).c(i5);
                      int i6 = k;
                      Object localObject13 = locals.getString(i6);
                      localObject3 = ((Message.a)localObject3).a((String)localObject13);
                      i6 = j;
                      i6 = locals.getInt(i6);
                      localObject3 = ((Message.a)localObject3).b(i6);
                      i6 = aE;
                      localObject13 = locals.getString(i6);
                      localObject3 = ((Message.a)localObject3).b((String)localObject13);
                      i6 = l;
                      localObject13 = locals.getString(i6);
                      localObject3 = ((Message.a)localObject3).c((String)localObject13);
                      i6 = m;
                      i6 = locals.getInt(i6);
                      localObject3 = ((Message.a)localObject3).d(i6);
                      i6 = n;
                      l5 = locals.getLong(i6);
                      localObject3 = ((Message.a)localObject3).e(l5);
                      i6 = aH;
                      i6 = locals.getInt(i6);
                      localObject3 = ((Message.a)localObject3).c(i6);
                      i6 = o;
                      l5 = locals.getLong(i6);
                      localObject3 = ((Message.a)localObject3).f(l5);
                      localObject13 = c();
                      ((Message.a)localObject3).a((ReplySnippet)localObject13);
                      i2 = i;
                      i2 = locals.getInt(i2);
                      int i7;
                      if (i2 == 0)
                      {
                        i2 = U;
                        localObject3 = locals.getString(i2);
                        k.a(localObject3, "getString(messageUri)");
                        localObject13 = localObject3;
                        localObject13 = (CharSequence)localObject3;
                        i6 = ((CharSequence)localObject13).length();
                        if (i6 == 0)
                        {
                          i6 = 1;
                        }
                        else
                        {
                          i6 = 0;
                          localObject13 = null;
                        }
                        if (i6 == 0)
                        {
                          localObject13 = new com/truecaller/messaging/transport/sms/SmsTransportInfo$a;
                          ((SmsTransportInfo.a)localObject13).<init>();
                          i13 = R;
                          l5 = locals.getLong(i13);
                          localObject13 = ((SmsTransportInfo.a)localObject13).a(l5).c(l4);
                          i13 = S;
                          i13 = locals.getInt(i13);
                          localObject13 = ((SmsTransportInfo.a)localObject13).a(i13);
                          i13 = T;
                          l1 = locals.getLong(i13);
                          localObject13 = ((SmsTransportInfo.a)localObject13).b(l1);
                          localObject3 = Uri.parse((String)localObject3);
                          localObject3 = ((SmsTransportInfo.a)localObject13).a((Uri)localObject3);
                          i6 = V;
                          i6 = locals.getInt(i6);
                          localObject3 = ((SmsTransportInfo.a)localObject3).b(i6);
                          i6 = W;
                          i6 = locals.getInt(i6);
                          localObject3 = ((SmsTransportInfo.a)localObject3).c(i6);
                          i6 = X;
                          localObject13 = locals.getString(i6);
                          localObject3 = ((SmsTransportInfo.a)localObject3).a((String)localObject13);
                          i6 = Y;
                          localObject13 = locals.getString(i6);
                          localObject3 = ((SmsTransportInfo.a)localObject3).b((String)localObject13);
                          i6 = Z;
                          i6 = locals.getInt(i6);
                          localObject3 = ((SmsTransportInfo.a)localObject3).d(i6);
                          i6 = aa;
                          i6 = locals.getInt(i6);
                          if (i6 != 0)
                          {
                            i6 = 1;
                          }
                          else
                          {
                            i6 = 0;
                            localObject13 = null;
                          }
                          localObject3 = ((SmsTransportInfo.a)localObject3).a(i6);
                          i7 = ab;
                          localObject13 = locals.getString(i7);
                          localObject3 = ((SmsTransportInfo.a)localObject3).c((String)localObject13).a();
                          localObject13 = "SmsTransportInfo.Builder…ss))\n            .build()";
                          k.a(localObject3, (String)localObject13);
                          localObject3 = (TransportInfo)localObject3;
                          ((Message.a)localObject1).a(0, (TransportInfo)localObject3);
                        }
                        else
                        {
                          localObject1 = new android/database/SQLException;
                          ((SQLException)localObject1).<init>("Empty message uri");
                          throw ((Throwable)localObject1);
                        }
                      }
                      else
                      {
                        i7 = 3;
                        if (i2 == i7)
                        {
                          localObject3 = new com/truecaller/messaging/transport/NullTransportInfo$a;
                          ((NullTransportInfo.a)localObject3).<init>();
                          localObject3 = (TransportInfo)((NullTransportInfo.a)localObject3).a(l4).a();
                          ((Message.a)localObject1).a(i7, (TransportInfo)localObject3);
                        }
                        else
                        {
                          i7 = 1;
                          int i11;
                          int i10;
                          if (i2 == i7)
                          {
                            i2 = U;
                            localObject3 = locals.getString(i2);
                            k.a(localObject3, "getString(messageUri)");
                            localObject13 = localObject3;
                            localObject13 = (CharSequence)localObject3;
                            i7 = ((CharSequence)localObject13).length();
                            if (i7 == 0)
                            {
                              i7 = 1;
                            }
                            else
                            {
                              i7 = 0;
                              localObject13 = null;
                            }
                            if (i7 == 0)
                            {
                              localObject13 = new com/truecaller/messaging/transport/mms/MmsTransportInfo$a;
                              ((MmsTransportInfo.a)localObject13).<init>();
                              i13 = R;
                              l5 = locals.getLong(i13);
                              localObject13 = ((MmsTransportInfo.a)localObject13).a(l5).d(l4);
                              i13 = S;
                              i13 = locals.getInt(i13);
                              localObject13 = ((MmsTransportInfo.a)localObject13).a(i13);
                              i13 = T;
                              l1 = locals.getLong(i13);
                              localObject13 = ((MmsTransportInfo.a)localObject13).b(l1);
                              localObject3 = Uri.parse((String)localObject3);
                              localObject3 = ((MmsTransportInfo.a)localObject13).a((Uri)localObject3);
                              i7 = ac;
                              localObject13 = locals.getString(i7);
                              localObject3 = ((MmsTransportInfo.a)localObject3).e((String)localObject13);
                              i7 = ad;
                              localObject13 = locals.getString(i7);
                              localObject3 = ((MmsTransportInfo.a)localObject3).d((String)localObject13);
                              i7 = ae;
                              localObject13 = locals.getString(i7);
                              i13 = af;
                              i13 = locals.getInt(i13);
                              localObject3 = ((MmsTransportInfo.a)localObject3).a((String)localObject13, i13);
                              i7 = ah;
                              l1 = locals.getLong(i7);
                              localObject3 = ((MmsTransportInfo.a)localObject3).e(l1);
                              i7 = ai;
                              i7 = locals.getInt(i7);
                              localObject3 = ((MmsTransportInfo.a)localObject3).d(i7);
                              i7 = aj;
                              i7 = locals.getInt(i7);
                              localObject3 = ((MmsTransportInfo.a)localObject3).i(i7);
                              i7 = ak;
                              i7 = locals.getInt(i7);
                              localObject3 = ((MmsTransportInfo.a)localObject3).e(i7);
                              i7 = al;
                              i7 = locals.getInt(i7);
                              localObject3 = ((MmsTransportInfo.a)localObject3).f(i7);
                              i7 = am;
                              i7 = locals.getInt(i7);
                              localObject3 = ((MmsTransportInfo.a)localObject3).g(i7);
                              i7 = an;
                              i7 = locals.getInt(i7);
                              localObject3 = ((MmsTransportInfo.a)localObject3).h(i7);
                              i7 = ao;
                              i7 = locals.getInt(i7);
                              localObject3 = ((MmsTransportInfo.a)localObject3).j(i7);
                              i7 = ap;
                              l1 = locals.getLong(i7);
                              localObject3 = ((MmsTransportInfo.a)localObject3).f(l1);
                              i7 = aq;
                              i7 = locals.getInt(i7);
                              localObject3 = ((MmsTransportInfo.a)localObject3).b(i7);
                              i7 = at;
                              localObject13 = locals.getString(i7);
                              localObject3 = ((MmsTransportInfo.a)localObject3).a((String)localObject13);
                              i7 = au;
                              i7 = locals.getInt(i7);
                              localObject3 = ((MmsTransportInfo.a)localObject3).c(i7);
                              i7 = av;
                              localObject13 = locals.getString(i7);
                              localObject3 = ((MmsTransportInfo.a)localObject3).b((String)localObject13);
                              i7 = aw;
                              localObject13 = locals.getString(i7);
                              localObject3 = ((MmsTransportInfo.a)localObject3).c((String)localObject13);
                              i7 = ax;
                              i7 = locals.getInt(i7);
                              localObject3 = ((MmsTransportInfo.a)localObject3).k(i7);
                              i7 = ay;
                              i7 = locals.getInt(i7);
                              localObject3 = ((MmsTransportInfo.a)localObject3).l(i7);
                              i7 = az;
                              i7 = locals.getInt(i7);
                              if (i7 != 0) {
                                bool6 = true;
                              }
                              localObject3 = ((MmsTransportInfo.a)localObject3).a(bool6);
                              localObject13 = "MmsTransportInfo.Builder…t(mmsReportAllowed) != 0)";
                              k.a(localObject3, (String)localObject13);
                              i7 = ae;
                              boolean bool3 = locals.isNull(i7);
                              if (!bool3)
                              {
                                i8 = ae;
                                localObject13 = locals.getString(i8);
                                i11 = af;
                                i11 = locals.getInt(i11);
                                ((MmsTransportInfo.a)localObject3).a((String)localObject13, i11);
                              }
                              int i8 = ar;
                              boolean bool4 = locals.isNull(i8);
                              if (!bool4)
                              {
                                i9 = ar;
                                localObject13 = locals.getString(i9);
                                i11 = as;
                                i11 = locals.getInt(i11);
                                ((MmsTransportInfo.a)localObject3).b((String)localObject13, i11);
                              }
                              int i9 = ag;
                              boolean bool5 = locals.isNull(i9);
                              if (!bool5)
                              {
                                i10 = ag;
                                localObject13 = Uri.parse(locals.getString(i10));
                                ((MmsTransportInfo.a)localObject3).b((Uri)localObject13);
                              }
                              localObject3 = ((MmsTransportInfo.a)localObject3).a();
                              localObject13 = "builder.build()";
                              k.a(localObject3, (String)localObject13);
                              localObject3 = (TransportInfo)localObject3;
                              i10 = 1;
                              ((Message.a)localObject1).a(i10, (TransportInfo)localObject3);
                            }
                            else
                            {
                              localObject1 = new android/database/SQLException;
                              ((SQLException)localObject1).<init>("Empty message uri");
                              throw ((Throwable)localObject1);
                            }
                          }
                          else
                          {
                            i10 = 2;
                            if (i2 == i10)
                            {
                              localObject3 = new com/truecaller/messaging/transport/im/ImTransportInfo$a;
                              ((ImTransportInfo.a)localObject3).<init>();
                              i11 = R;
                              localObject2 = locals.getString(i11);
                              if (localObject2 == null) {
                                localObject2 = "";
                              }
                              localObject3 = ((ImTransportInfo.a)localObject3).a((String)localObject2);
                              a = l4;
                              i11 = aA;
                              i11 = locals.getInt(i11);
                              b = i11;
                              i11 = aB;
                              i11 = locals.getInt(i11);
                              c = i11;
                              i11 = aC;
                              i11 = locals.getInt(i11);
                              d = i11;
                              i11 = aD;
                              i11 = locals.getInt(i11);
                              f = i11;
                              i11 = aF;
                              i11 = locals.getInt(i11);
                              h = i11;
                              i11 = aG;
                              i11 = locals.getInt(i11);
                              i = i11;
                              i11 = aI;
                              long l6 = locals.getLong(i11);
                              j = l6;
                              localObject2 = locals.a(l4);
                              localObject3 = (TransportInfo)((ImTransportInfo.a)localObject3).a((Reaction[])localObject2).a();
                              ((Message.a)localObject1).a(i10, (TransportInfo)localObject3);
                            }
                            else
                            {
                              i10 = 4;
                              if (i2 == i10)
                              {
                                localObject3 = new com/truecaller/backup/BackupTransportInfo;
                                ((BackupTransportInfo)localObject3).<init>(l4);
                                localObject3 = (TransportInfo)localObject3;
                                ((Message.a)localObject1).a(i10, (TransportInfo)localObject3);
                              }
                              else
                              {
                                i10 = 5;
                                if (i2 == i10)
                                {
                                  i2 = R;
                                  localObject3 = locals.getString(i2);
                                  localObject2 = "getString(rawId)";
                                  k.a(localObject3, (String)localObject2);
                                  l5 = Long.parseLong((String)localObject3);
                                  i2 = aT;
                                  i17 = locals.getInt(i2);
                                  i2 = aR;
                                  i18 = locals.getInt(i2);
                                  i2 = aU;
                                  i19 = locals.getInt(i2);
                                  i2 = aS;
                                  localObject10 = locals.getString(i2);
                                  localObject3 = new com/truecaller/messaging/transport/history/HistoryTransportInfo;
                                  localObject4 = localObject3;
                                  ((HistoryTransportInfo)localObject3).<init>(l4, l5, i17, i18, i19, (String)localObject10);
                                  localObject3 = (TransportInfo)localObject3;
                                  ((Message.a)localObject1).a(i10, (TransportInfo)localObject3);
                                }
                                else
                                {
                                  i10 = 6;
                                  if (i2 != i10) {
                                    break label3795;
                                  }
                                  i2 = R;
                                  localObject3 = locals.getString(i2);
                                  if (localObject3 == null) {
                                    localObject3 = "";
                                  }
                                  localObject2 = new com/truecaller/messaging/transport/status/StatusTransportInfo;
                                  ((StatusTransportInfo)localObject2).<init>(l4, (String)localObject3);
                                  localObject2 = (TransportInfo)localObject2;
                                  ((Message.a)localObject1).a(i10, (TransportInfo)localObject2);
                                }
                              }
                            }
                          }
                        }
                      }
                      localObject1 = ((Message.a)localObject1).b();
                      k.a(localObject1, "builder.build()");
                      return (Message)localObject1;
                      label3795:
                      localObject1 = new android/database/SQLException;
                      localObject3 = String.valueOf(i2);
                      localObject3 = "Unsupported transport type: ".concat((String)localObject3);
                      ((SQLException)localObject1).<init>((String)localObject3);
                      throw ((Throwable)localObject1);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    localObject1 = new android/database/sqlite/SQLiteException;
    ((SQLiteException)localObject1).<init>("Inconsistent entities data");
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */