package com.truecaller.messaging.data.a;

import android.database.Cursor;
import com.truecaller.messaging.conversation.a.b.n;
import com.truecaller.messaging.conversation.spamLinks.UrlReportDto;

public abstract interface p
  extends Cursor
{
  public abstract String a();
  
  public abstract n b();
  
  public abstract UrlReportDto c();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.a.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */