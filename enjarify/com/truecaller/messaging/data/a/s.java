package com.truecaller.messaging.data.a;

import android.database.Cursor;
import com.truecaller.messaging.data.types.TransportInfo;

public abstract interface s
  extends Cursor
{
  public abstract long a();
  
  public abstract long b();
  
  public abstract int c();
  
  public abstract int d();
  
  public abstract TransportInfo e();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.a.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */