package com.truecaller.messaging.data.a;

import android.database.Cursor;
import android.database.CursorWrapper;
import c.g.b.k;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.ImGroupPermissions;

public final class i
  extends CursorWrapper
  implements h
{
  private final int a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  private final int j;
  private final int k;
  
  public i(Cursor paramCursor)
  {
    super(paramCursor);
    int m = getColumnIndexOrThrow("im_group_id");
    a = m;
    m = getColumnIndexOrThrow("title");
    b = m;
    m = getColumnIndexOrThrow("avatar");
    c = m;
    m = getColumnIndexOrThrow("invited_date");
    d = m;
    m = getColumnIndexOrThrow("invited_by");
    e = m;
    m = getColumnIndexOrThrow("roles");
    f = m;
    m = getColumnIndexOrThrow("actions");
    g = m;
    m = getColumnIndexOrThrow("role_update_restriction_mask");
    h = m;
    m = getColumnIndexOrThrow("role_update_mask");
    i = m;
    m = getColumnIndexOrThrow("self_role_update_mask");
    j = m;
    m = getColumnIndexOrThrow("notification_settings");
    k = m;
  }
  
  public final ImGroupInfo a()
  {
    ImGroupInfo localImGroupInfo = new com/truecaller/messaging/data/types/ImGroupInfo;
    int m = a;
    String str1 = getString(m);
    k.a(str1, "getString(imGroupId)");
    m = b;
    String str2 = getString(m);
    m = c;
    String str3 = getString(m);
    m = d;
    long l = getLong(m);
    m = e;
    String str4 = getString(m);
    m = f;
    int n = getInt(m);
    ImGroupPermissions localImGroupPermissions = new com/truecaller/messaging/data/types/ImGroupPermissions;
    m = g;
    m = getInt(m);
    int i1 = h;
    i1 = getInt(i1);
    int i2 = i;
    i2 = getInt(i2);
    int i3 = j;
    i3 = getInt(i3);
    localImGroupPermissions.<init>(m, i1, i2, i3);
    m = k;
    i1 = getInt(m);
    localImGroupInfo.<init>(str1, str2, str3, l, str4, n, localImGroupPermissions, i1);
    return localImGroupInfo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.a.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */