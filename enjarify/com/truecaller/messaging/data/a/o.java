package com.truecaller.messaging.data.a;

import android.database.Cursor;
import android.database.CursorWrapper;
import c.g.b.k;
import com.truecaller.messaging.data.types.Participant.a;
import com.truecaller.messaging.data.types.Reaction;

public final class o
  extends CursorWrapper
  implements n
{
  private final int a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  private final int j;
  
  public o(Cursor paramCursor)
  {
    super(paramCursor);
    int k = getColumnIndexOrThrow("im_reaction_id");
    a = k;
    k = getColumnIndexOrThrow("im_reaction_message_id");
    b = k;
    k = getColumnIndexOrThrow("im_reaction_from_peer_id");
    c = k;
    k = getColumnIndexOrThrow("im_reaction_emoji");
    d = k;
    k = getColumnIndexOrThrow("im_reaction_date");
    e = k;
    k = getColumnIndexOrThrow("im_reaction_status");
    f = k;
    k = getColumnIndexOrThrow("im_participant_number");
    g = k;
    k = getColumnIndexOrThrow("im_participant_name");
    h = k;
    k = getColumnIndexOrThrow("im_participant_image_url");
    i = k;
    k = getColumnIndexOrThrow("im_participant_phonebook_id");
    j = k;
  }
  
  public final c.n a()
  {
    Reaction localReaction = new com/truecaller/messaging/data/types/Reaction;
    int k = a;
    long l1 = getLong(k);
    k = b;
    long l2 = getLong(k);
    k = c;
    String str1 = getString(k);
    k.a(str1, "getString(fromPeerIdIndex)");
    k = d;
    String str2 = getString(k);
    k = e;
    long l3 = getLong(k);
    k = f;
    int m = getInt(k);
    Object localObject1 = localReaction;
    localReaction.<init>(l1, l2, str1, str2, l3, m);
    localObject1 = c;
    int n = g;
    Object localObject2 = getString(n);
    Participant.a locala = new com/truecaller/messaging/data/types/Participant$a;
    int i1;
    if (localObject2 == null) {
      i1 = 3;
    } else {
      i1 = 0;
    }
    locala.<init>(i1);
    if (localObject2 == null) {
      localObject2 = localObject1;
    }
    localObject1 = locala.b((String)localObject2).d((String)localObject1);
    n = h;
    localObject2 = getString(n);
    localObject1 = ((Participant.a)localObject1).f((String)localObject2);
    n = i;
    localObject2 = getString(n);
    if (localObject2 == null) {
      localObject2 = "";
    }
    localObject1 = ((Participant.a)localObject1).g((String)localObject2);
    n = j;
    l1 = getLong(n);
    localObject1 = ((Participant.a)localObject1).c(l1).a();
    k.a(localObject1, "Participant.Builder(if (…ex))\n            .build()");
    localObject2 = new c/n;
    ((c.n)localObject2).<init>(localReaction, localObject1);
    return (c.n)localObject2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.a.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */