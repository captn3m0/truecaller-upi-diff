package com.truecaller.messaging.data.a;

import android.database.Cursor;
import android.database.CursorWrapper;

public final class e
  extends CursorWrapper
  implements c
{
  private final Cursor a;
  
  public e(Cursor paramCursor)
  {
    super(paramCursor);
    a = paramCursor;
  }
  
  public final int a(int paramInt)
  {
    IllegalArgumentException localIllegalArgumentException = null;
    String str;
    switch (paramInt)
    {
    default: 
      localIllegalArgumentException = new java/lang/IllegalArgumentException;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("Filter type ");
      localStringBuilder.append(paramInt);
      localStringBuilder.append(" is not supported in cursor");
      str = localStringBuilder.toString();
      localIllegalArgumentException.<init>(str);
      throw ((Throwable)localIllegalArgumentException);
    case 4: 
      paramInt = 0;
      str = null;
      break;
    case 3: 
      paramInt = 1;
      break;
    case 2: 
      paramInt = 2;
    }
    moveToPosition(paramInt);
    return a.getInt(0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */