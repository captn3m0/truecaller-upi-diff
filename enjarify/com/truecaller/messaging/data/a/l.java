package com.truecaller.messaging.data.a;

import android.database.Cursor;
import c.n.m;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.data.a;
import com.truecaller.messaging.newconversation.i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class l
  extends a
  implements k
{
  public l(Cursor paramCursor)
  {
    super(paramCursor);
  }
  
  public final i a()
  {
    l locall = this;
    Object localObject1 = a(com.truecaller.utils.extensions.k.d(this, "numbers_group"));
    c.g.b.k.a(localObject1, "readComposedStrings(stri…y.PHONE_NUMBERS_GROUPED))");
    List localList = a(com.truecaller.utils.extensions.k.d(this, "names_group"));
    c.g.b.k.a(localList, "readComposedStrings(stri…ionsQuery.NAMES_GROUPED))");
    Object localObject2 = f.a(com.truecaller.utils.extensions.k.d(this, "phonebook_ids_grouped"));
    int i = ((List)localObject1).size();
    int k = localList.size();
    String str1 = null;
    if (i == k)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject3 = null;
    }
    Object localObject4 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(i, (String[])localObject4);
    int j = localList.size();
    int m = ((List)localObject2).size();
    if (j == m)
    {
      j = 1;
    }
    else
    {
      j = 0;
      localObject3 = null;
    }
    localObject4 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(j, (String[])localObject4);
    localObject4 = com.truecaller.utils.extensions.k.d(locall, "conversation_id");
    localObject1 = (Iterable)localObject1;
    Object localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    localObject3 = (Collection)localObject3;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject5 = (String)((Iterator)localObject1).next();
      localObject6 = new com/truecaller/data/entity/Number;
      ((com.truecaller.data.entity.Number)localObject6).<init>((String)localObject5);
      ((Collection)localObject3).add(localObject6);
    }
    Object localObject5 = localObject3;
    localObject5 = (List)localObject3;
    Object localObject6 = com.truecaller.utils.extensions.k.d(locall, "im_id");
    long l1 = com.truecaller.utils.extensions.k.f(locall, "im_registration_timestamp");
    localObject2 = (Iterable)localObject2;
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    localObject2 = ((Iterable)localObject2).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject2).hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = m.d((String)((Iterator)localObject2).next());
      Object localObject7 = null;
      if (localObject3 != null)
      {
        Object localObject8 = localObject3;
        localObject8 = (Number)localObject3;
        long l2 = ((Number)localObject8).longValue();
        long l3 = 0L;
        boolean bool3 = l2 < l3;
        int n;
        if (bool3)
        {
          n = 1;
        }
        else
        {
          n = 0;
          localObject8 = null;
        }
        if (n == 0) {
          localObject7 = localObject3;
        }
      }
      if (localObject7 != null) {
        ((Collection)localObject1).add(localObject7);
      }
    }
    Object localObject9 = localObject1;
    localObject9 = (List)localObject1;
    str1 = com.truecaller.utils.extensions.k.d(locall, "image_uri");
    long l4 = com.truecaller.utils.extensions.k.f(locall, "date_sorting");
    int i1 = com.truecaller.utils.extensions.k.e(locall, "transport_type");
    int i2 = com.truecaller.utils.extensions.k.e(locall, "group_sorting");
    String str2 = com.truecaller.utils.extensions.k.d(locall, "im_group_id");
    String str3 = com.truecaller.utils.extensions.k.d(locall, "im_group_title");
    String str4 = com.truecaller.utils.extensions.k.d(locall, "im_group_avatar");
    localObject1 = new com/truecaller/messaging/newconversation/i;
    localObject3 = localObject1;
    ((i)localObject1).<init>((String)localObject4, localList, (List)localObject9, str1, (List)localObject5, (String)localObject6, l1, l4, i1, i2, str2, str3, str4);
    return (i)localObject1;
  }
  
  public final int b()
  {
    return com.truecaller.utils.extensions.k.e(this, "group_sorting");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.a.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */