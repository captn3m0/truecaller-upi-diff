package com.truecaller.messaging.data.a;

import android.database.Cursor;
import android.database.CursorWrapper;
import c.g.b.k;
import com.truecaller.messaging.conversation.a.b.n;
import com.truecaller.messaging.conversation.spamLinks.UrlReportDto;

public final class q
  extends CursorWrapper
  implements p
{
  private final int a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  
  public q(Cursor paramCursor)
  {
    super(paramCursor);
    int i = paramCursor.getColumnIndexOrThrow("url");
    a = i;
    i = paramCursor.getColumnIndexOrThrow("spam_count");
    b = i;
    i = paramCursor.getColumnIndexOrThrow("expires_at");
    c = i;
    i = paramCursor.getColumnIndexOrThrow("is_spam_reported");
    d = i;
    i = paramCursor.getColumnIndexOrThrow("sync_status");
    e = i;
    i = paramCursor.getColumnIndexOrThrow("spam_score");
    f = i;
    int j = paramCursor.getColumnIndexOrThrow("last_updated_at");
    g = j;
  }
  
  public final String a()
  {
    int i = a;
    String str = getString(i);
    k.a(str, "getString(urlIndex)");
    return str;
  }
  
  public final n b()
  {
    n localn = new com/truecaller/messaging/conversation/a/b/n;
    int i = b;
    int j = getInt(i);
    i = c;
    long l1 = getLong(i);
    i = f;
    int k = getInt(i);
    i = d;
    int m = getInt(i);
    i = e;
    int n = getInt(i);
    i = g;
    long l2 = getLong(i);
    localn.<init>(j, l1, k, m, n, l2);
    return localn;
  }
  
  public final UrlReportDto c()
  {
    int i = d;
    i = getInt(i);
    String str1;
    switch (i)
    {
    default: 
      return null;
    case 2: 
      str1 = "notspam";
      break;
    case 1: 
      str1 = "spam";
    }
    UrlReportDto localUrlReportDto = new com/truecaller/messaging/conversation/spamLinks/UrlReportDto;
    int j = a;
    String str2 = getString(j);
    k.a(str2, "getString(urlIndex)");
    localUrlReportDto.<init>(str2, str1);
    return localUrlReportDto;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.a.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */