package com.truecaller.messaging.data.a;

import android.database.Cursor;
import android.database.SQLException;
import c.g.b.k;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.Conversation.a;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.ImGroupPermissions;
import com.truecaller.messaging.data.types.Participant.a;
import com.truecaller.messaging.transport.history.HistoryTransportInfo;
import java.util.ArrayList;
import java.util.List;

public final class b
  extends com.truecaller.messaging.data.a
  implements a
{
  private final int A;
  private final int B;
  private final int C;
  private final int D;
  private final int E;
  private final int F;
  private final int G;
  private final int H;
  private final int I;
  private final int J;
  private final int K;
  private final int L;
  private final int M;
  private final int N;
  private final int O;
  private final int P;
  private final int Q;
  private final int R;
  private final int S;
  private final int T;
  private final int U;
  private final int V;
  private final int W;
  private final int X;
  private final int Y;
  private final int Z;
  private final int a;
  private final int aa;
  private final int ab;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  private final int j;
  private final int k;
  private final int l;
  private final int m;
  private final int n;
  private final int o;
  private final int p;
  private final int q;
  private final int r;
  private final int s;
  private final int t;
  private final int u;
  private final int v;
  private final int w;
  private final int x;
  private final int y;
  private final int z;
  
  public b(Cursor paramCursor)
  {
    super(paramCursor);
    int i1 = paramCursor.getColumnIndexOrThrow("_id");
    a = i1;
    i1 = paramCursor.getColumnIndexOrThrow("tc_group_id");
    b = i1;
    i1 = paramCursor.getColumnIndexOrThrow("type");
    c = i1;
    i1 = paramCursor.getColumnIndexOrThrow("latest_message_id");
    d = i1;
    i1 = paramCursor.getColumnIndexOrThrow("latest_message_status");
    e = i1;
    i1 = paramCursor.getColumnIndexOrThrow("latest_message_media_count");
    f = i1;
    i1 = paramCursor.getColumnIndexOrThrow("latest_message_media_type");
    g = i1;
    i1 = paramCursor.getColumnIndexOrThrow("latest_sim_token");
    h = i1;
    i1 = paramCursor.getColumnIndexOrThrow("date");
    i = i1;
    i1 = paramCursor.getColumnIndexOrThrow("snippet_text");
    j = i1;
    i1 = paramCursor.getColumnIndexOrThrow("unread_messages_count");
    k = i1;
    i1 = paramCursor.getColumnIndexOrThrow("actions_dismissed");
    l = i1;
    i1 = paramCursor.getColumnIndexOrThrow("outgoing_message_count");
    m = i1;
    i1 = paramCursor.getColumnIndexOrThrow("phonebook_count");
    n = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_id");
    o = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_type");
    p = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_im_id");
    q = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_raw_destinantion");
    r = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_normalized_destination");
    s = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_country_codes");
    t = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_tc_id");
    u = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_aggregated_contact_id");
    v = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_filter_action");
    w = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_is_top_spammer");
    x = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_top_spam_score");
    y = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_name");
    z = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_image_url");
    A = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_source");
    B = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_phonebook_id");
    C = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_spam_score");
    D = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_badges");
    E = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_company");
    F = i1;
    i1 = paramCursor.getColumnIndexOrThrow("participants_search_time");
    G = i1;
    i1 = paramCursor.getColumnIndexOrThrow("filter");
    H = i1;
    i1 = paramCursor.getColumnIndexOrThrow("split_criteria");
    I = i1;
    i1 = paramCursor.getColumnIndexOrThrow("preferred_transport");
    J = i1;
    i1 = paramCursor.getColumnIndexOrThrow("latest_message_delivery_status");
    K = i1;
    i1 = paramCursor.getColumnIndexOrThrow("latest_message_read_status");
    L = i1;
    i1 = paramCursor.getColumnIndexOrThrow("latest_message_raw_status");
    M = i1;
    i1 = paramCursor.getColumnIndexOrThrow("latest_message_transport");
    N = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_group_id");
    O = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_group_title");
    P = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_group_avatar");
    Q = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_group_invited_date");
    R = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_group_invited_by");
    S = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_group_roles");
    T = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_group_actions");
    U = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_group_role_update_restriction_mask");
    V = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_group_role_update_mask");
    W = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_group_self_role_update_mask");
    X = i1;
    i1 = paramCursor.getColumnIndexOrThrow("im_group_notification_settings");
    Y = i1;
    i1 = paramCursor.getColumnIndex("history_type");
    Z = i1;
    i1 = paramCursor.getColumnIndex("history_features");
    aa = i1;
    int i2 = paramCursor.getColumnIndex("history_number_type");
    ab = i2;
  }
  
  private static List a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11, String paramString12, String paramString13, String paramString14, String paramString15, String paramString16, String paramString17, String paramString18, String paramString19)
  {
    Object localObject1 = f.a(paramString1);
    Object localObject2 = f.a(paramString2);
    Object localObject3 = a(paramString3);
    k.a(localObject3, "readComposedStrings(imIds)");
    Object localObject4 = a(paramString4);
    k.a(localObject4, "readComposedStrings(rawDestinations)");
    Object localObject5 = a(paramString5);
    k.a(localObject5, "readComposedStrings(normalizedDestinations)");
    List localList1 = a(paramString6);
    k.a(localList1, "readComposedStrings(countryCodes)");
    List localList2 = a(paramString7);
    k.a(localList2, "readComposedStrings(tcIds)");
    List localList3 = f.a(paramString8);
    List localList4 = f.a(paramString9);
    List localList5 = f.a(paramString10);
    List localList6 = f.a(paramString11);
    Object localObject6 = a(paramString12);
    k.a(localObject6, "readComposedStrings(names)");
    Object localObject7 = a(paramString13);
    k.a(localObject7, "readComposedStrings(imageUrls)");
    List localList7 = f.a(paramString14);
    List localList8 = f.a(paramString15);
    Object localObject8 = f.a(paramString16);
    paramString1 = localList1;
    paramString2 = f.a(paramString17);
    localList1 = a(paramString18);
    paramString3 = (String)localObject8;
    k.a(localList1, "readComposedStrings(companies)");
    localObject8 = f.a(paramString19);
    paramString4 = (String)localObject8;
    int i1 = ((List)localObject1).size();
    Object localObject9 = localObject1;
    int i2 = ((List)localObject2).size();
    if (i1 == i2)
    {
      i2 = ((List)localObject3).size();
      if (i1 == i2)
      {
        i2 = ((List)localObject4).size();
        if (i1 == i2)
        {
          i2 = ((List)localObject5).size();
          if (i1 == i2)
          {
            i2 = ((List)localObject5).size();
            if (i1 == i2)
            {
              i2 = localList2.size();
              if (i1 == i2)
              {
                i2 = localList3.size();
                if (i1 == i2)
                {
                  i2 = localList4.size();
                  if (i1 == i2)
                  {
                    i2 = localList5.size();
                    if (i1 == i2)
                    {
                      i2 = localList6.size();
                      if (i1 == i2)
                      {
                        i2 = ((List)localObject6).size();
                        if (i1 == i2)
                        {
                          i2 = ((List)localObject7).size();
                          if (i1 == i2)
                          {
                            i2 = localList7.size();
                            if (i1 == i2)
                            {
                              i2 = localList8.size();
                              if (i1 == i2)
                              {
                                i2 = paramString3.size();
                                if (i1 == i2)
                                {
                                  i2 = paramString2.size();
                                  if (i1 == i2)
                                  {
                                    i2 = localList1.size();
                                    if (i1 == i2)
                                    {
                                      i2 = ((List)localObject8).size();
                                      if (i1 == i2)
                                      {
                                        localObject1 = new java/util/ArrayList;
                                        ((ArrayList)localObject1).<init>();
                                        paramString5 = (String)localObject1;
                                        i2 = 0;
                                        localObject1 = null;
                                        while (i2 < i1)
                                        {
                                          Object localObject10 = (String)((List)localObject2).get(i2);
                                          Object localObject11 = localObject2;
                                          int i3 = Integer.parseInt((String)localObject10);
                                          int i4 = i1;
                                          localObject8 = new com/truecaller/messaging/data/types/Participant$a;
                                          ((Participant.a)localObject8).<init>(i3);
                                          localObject2 = localObject9;
                                          localObject9 = (String)((List)localObject9).get(i2);
                                          paramString7 = (String)localObject6;
                                          paramString8 = (String)localObject7;
                                          long l1 = Long.parseLong((String)localObject9);
                                          localObject6 = ((Participant.a)localObject8).a(l1);
                                          localObject7 = (String)((List)localObject4).get(i2);
                                          localObject6 = ((Participant.a)localObject6).a((String)localObject7);
                                          localObject7 = (String)((List)localObject5).get(i2);
                                          localObject6 = ((Participant.a)localObject6).b((String)localObject7);
                                          localObject7 = paramString1;
                                          localObject9 = paramString1.get(i2);
                                          localObject10 = localObject2;
                                          localObject2 = localObject9;
                                          localObject2 = (String)localObject9;
                                          localObject2 = ((Participant.a)localObject6).c((String)localObject2);
                                          int i5 = Integer.parseInt((String)localList4.get(i2));
                                          localObject2 = ((Participant.a)localObject2).a(i5);
                                          localObject6 = (String)localList5.get(i2);
                                          i5 = Integer.parseInt((String)localObject6);
                                          if (i5 != 0)
                                          {
                                            i5 = 1;
                                          }
                                          else
                                          {
                                            i5 = 0;
                                            localObject6 = null;
                                          }
                                          localObject2 = ((Participant.a)localObject2).a(i5);
                                          int i6 = Integer.parseInt((String)localList6.get(i2));
                                          localObject2 = ((Participant.a)localObject2).b(i6);
                                          i6 = Integer.parseInt((String)localList7.get(i2));
                                          localObject2 = ((Participant.a)localObject2).c(i6);
                                          localObject6 = (String)localList8.get(i2);
                                          paramString1 = (String)localObject4;
                                          localObject9 = localObject5;
                                          long l2 = Long.parseLong((String)localObject6);
                                          localObject2 = ((Participant.a)localObject2).c(l2);
                                          localObject4 = paramString3;
                                          int i7 = Integer.parseInt((String)paramString3.get(i2));
                                          localObject2 = ((Participant.a)localObject2).d(i7);
                                          localObject5 = paramString2;
                                          localObject6 = (String)paramString2.get(i2);
                                          i6 = Integer.parseInt((String)localObject6);
                                          ((Participant.a)localObject2).e(i6);
                                          localObject2 = ((List)localObject3).get(i2);
                                          if (localObject2 != null)
                                          {
                                            localObject2 = (String)((List)localObject3).get(i2);
                                            ((Participant.a)localObject8).d((String)localObject2);
                                          }
                                          localObject2 = localList2.get(i2);
                                          if (localObject2 != null)
                                          {
                                            localObject2 = (String)localList2.get(i2);
                                            ((Participant.a)localObject8).e((String)localObject2);
                                            localObject2 = paramString7;
                                          }
                                          else
                                          {
                                            localObject2 = paramString7;
                                          }
                                          localObject6 = ((List)localObject2).get(i2);
                                          if (localObject6 != null)
                                          {
                                            localObject6 = (String)((List)localObject2).get(i2);
                                            ((Participant.a)localObject8).f((String)localObject6);
                                            localObject6 = paramString8;
                                          }
                                          else
                                          {
                                            localObject6 = paramString8;
                                          }
                                          Object localObject12 = ((List)localObject6).get(i2);
                                          if (localObject12 != null)
                                          {
                                            localObject12 = ((List)localObject6).get(i2);
                                            paramString7 = (String)localObject2;
                                            localObject2 = localObject12;
                                            localObject2 = (String)localObject12;
                                            ((Participant.a)localObject8).g((String)localObject2);
                                          }
                                          else
                                          {
                                            paramString7 = (String)localObject2;
                                          }
                                          localObject2 = localList1.get(i2);
                                          if (localObject2 != null)
                                          {
                                            localObject2 = (String)localList1.get(i2);
                                            ((Participant.a)localObject8).h((String)localObject2);
                                            localObject2 = paramString4;
                                          }
                                          else
                                          {
                                            localObject2 = paramString4;
                                          }
                                          localObject12 = ((List)localObject2).get(i2);
                                          if (localObject12 != null)
                                          {
                                            localObject12 = (String)((List)localObject2).get(i2);
                                            if (localObject12 != null)
                                            {
                                              long l3 = Long.parseLong((String)localObject12);
                                              localObject12 = Long.valueOf(l3);
                                            }
                                            else
                                            {
                                              localObject12 = null;
                                            }
                                            paramString4 = (String)localObject2;
                                            paramString2 = (String)localObject3;
                                            l4 = ((Long)localObject12).longValue();
                                            ((Participant.a)localObject8).d(l4);
                                          }
                                          else
                                          {
                                            paramString4 = (String)localObject2;
                                            paramString2 = (String)localObject3;
                                          }
                                          long l4 = Long.parseLong((String)localList3.get(i2));
                                          ((Participant.a)localObject8).b(l4);
                                          localObject2 = ((Participant.a)localObject8).a();
                                          localObject3 = paramString5;
                                          paramString5.add(localObject2);
                                          i2 += 1;
                                          i1 = i4;
                                          paramString3 = (String)localObject4;
                                          localObject2 = localObject11;
                                          localObject4 = paramString1;
                                          localObject3 = paramString2;
                                          paramString2 = (String)localObject5;
                                          paramString1 = (String)localObject7;
                                          localObject5 = localObject9;
                                          localObject9 = localObject10;
                                          localObject7 = localObject6;
                                          localObject6 = paramString7;
                                        }
                                        localObject3 = paramString5;
                                        localObject1 = paramString5;
                                        return (List)paramString5;
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    localObject1 = new android/database/SQLException;
    ((SQLException)localObject1).<init>("Inconsistent lengths in grouped fields");
    throw ((Throwable)localObject1);
  }
  
  public final long a()
  {
    int i1 = a;
    return getLong(i1);
  }
  
  public final Conversation b()
  {
    Object localObject1 = new com/truecaller/messaging/data/types/Conversation$a;
    ((Conversation.a)localObject1).<init>();
    int i1 = a;
    long l1 = getLong(i1);
    localObject1 = ((Conversation.a)localObject1).a(l1);
    i1 = b;
    l1 = getLong(i1);
    localObject1 = ((Conversation.a)localObject1).b(l1);
    i1 = c;
    i1 = getInt(i1);
    localObject1 = ((Conversation.a)localObject1).a(i1);
    i1 = d;
    l1 = getLong(i1);
    localObject1 = ((Conversation.a)localObject1).c(l1);
    i1 = e;
    i1 = getInt(i1);
    localObject1 = ((Conversation.a)localObject1).b(i1);
    i1 = f;
    i1 = getInt(i1);
    localObject1 = ((Conversation.a)localObject1).c(i1);
    i1 = g;
    Object localObject2 = getString(i1);
    localObject1 = ((Conversation.a)localObject1).a((String)localObject2);
    i1 = h;
    localObject2 = getString(i1);
    localObject1 = ((Conversation.a)localObject1).b((String)localObject2);
    localObject2 = new org/a/a/b;
    int i2 = i;
    long l2 = getLong(i2);
    ((org.a.a.b)localObject2).<init>(l2);
    localObject1 = ((Conversation.a)localObject1).a((org.a.a.b)localObject2);
    i1 = j;
    localObject2 = getString(i1);
    localObject1 = ((Conversation.a)localObject1).c((String)localObject2);
    i1 = k;
    i1 = getInt(i1);
    localObject1 = ((Conversation.a)localObject1).d(i1);
    i1 = l;
    i1 = getInt(i1);
    i2 = 1;
    if (i1 != i2) {
      i2 = 0;
    }
    localObject1 = ((Conversation.a)localObject1).a(i2);
    i1 = m;
    i1 = getInt(i1);
    localObject1 = ((Conversation.a)localObject1).f(i1);
    i1 = n;
    i1 = getInt(i1);
    localObject1 = ((Conversation.a)localObject1).e(i1);
    i1 = H;
    i1 = getInt(i1);
    localObject1 = ((Conversation.a)localObject1).g(i1);
    i1 = I;
    i1 = getInt(i1);
    localObject1 = ((Conversation.a)localObject1).h(i1);
    i1 = J;
    i1 = getInt(i1);
    localObject1 = ((Conversation.a)localObject1).i(i1);
    i1 = K;
    i1 = getInt(i1);
    localObject1 = ((Conversation.a)localObject1).k(i1);
    i1 = L;
    i1 = getInt(i1);
    localObject1 = ((Conversation.a)localObject1).l(i1);
    i1 = M;
    i1 = getInt(i1);
    localObject1 = ((Conversation.a)localObject1).j(i1);
    i1 = N;
    i1 = getInt(i1);
    localObject1 = ((Conversation.a)localObject1).m(i1);
    i1 = Z;
    int i3 = -1;
    int i4 = 0;
    if (i1 == i3)
    {
      i1 = 0;
      localObject2 = null;
    }
    else
    {
      localObject2 = new com/truecaller/messaging/transport/history/HistoryTransportInfo;
      i3 = d;
      long l3 = getLong(i3);
      i3 = Z;
      int i5 = getInt(i3);
      i3 = aa;
      int i6 = getInt(i3);
      i3 = ab;
      String str1 = getString(i3);
      ((HistoryTransportInfo)localObject2).<init>(l3, i5, i6, str1);
    }
    localObject1 = ((Conversation.a)localObject1).a((HistoryTransportInfo)localObject2);
    i1 = O;
    String str2 = getString(i1);
    if (str2 == null)
    {
      i1 = 0;
      localObject2 = null;
    }
    else
    {
      localObject2 = new com/truecaller/messaging/data/types/ImGroupInfo;
      i3 = T;
      int i7 = getInt(i3);
      i3 = P;
      String str3 = getString(i3);
      i3 = Q;
      String str4 = getString(i3);
      i3 = R;
      long l4 = getLong(i3);
      i3 = S;
      String str5 = getString(i3);
      ImGroupPermissions localImGroupPermissions = new com/truecaller/messaging/data/types/ImGroupPermissions;
      i3 = U;
      i3 = getInt(i3);
      i4 = V;
      i4 = getInt(i4);
      int i8 = W;
      i8 = getInt(i8);
      int i9 = X;
      i9 = getInt(i9);
      localImGroupPermissions.<init>(i3, i4, i8, i9);
      i3 = Y;
      i9 = getInt(i3);
      ((ImGroupInfo)localObject2).<init>(str2, str3, str4, l4, str5, i7, localImGroupPermissions, i9);
    }
    localObject1 = ((Conversation.a)localObject1).a((ImGroupInfo)localObject2);
    k.a(localObject1, "Conversation.Builder()\n …oupInfo(getImGroupInfo())");
    localObject2 = c();
    ((Conversation.a)localObject1).a((List)localObject2);
    localObject1 = ((Conversation.a)localObject1).a();
    k.a(localObject1, "builder.build()");
    return (Conversation)localObject1;
  }
  
  public final List c()
  {
    int i1 = o;
    String str1 = getString(i1);
    i1 = p;
    String str2 = getString(i1);
    i1 = q;
    String str3 = getString(i1);
    i1 = r;
    String str4 = getString(i1);
    i1 = s;
    String str5 = getString(i1);
    i1 = t;
    String str6 = getString(i1);
    i1 = u;
    String str7 = getString(i1);
    i1 = v;
    String str8 = getString(i1);
    i1 = w;
    String str9 = getString(i1);
    i1 = x;
    String str10 = getString(i1);
    i1 = y;
    String str11 = getString(i1);
    i1 = z;
    String str12 = getString(i1);
    i1 = A;
    String str13 = getString(i1);
    i1 = B;
    String str14 = getString(i1);
    i1 = C;
    String str15 = getString(i1);
    i1 = D;
    String str16 = getString(i1);
    i1 = E;
    String str17 = getString(i1);
    i1 = F;
    String str18 = getString(i1);
    i1 = G;
    String str19 = getString(i1);
    return a(str1, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, str12, str13, str14, str15, str16, str17, str18, str19);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */