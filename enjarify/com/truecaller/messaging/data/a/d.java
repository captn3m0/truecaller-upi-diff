package com.truecaller.messaging.data.a;

import android.database.Cursor;
import android.database.CursorWrapper;

public final class d
  extends CursorWrapper
  implements c
{
  public static final d.a a;
  private final Cursor b;
  
  static
  {
    d.a locala = new com/truecaller/messaging/data/a/d$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public d(Cursor paramCursor)
  {
    super(paramCursor);
    b = paramCursor;
  }
  
  public final int a(int paramInt)
  {
    boolean bool = isFirst();
    if (!bool) {
      moveToFirst();
    }
    Object localObject = b;
    String str;
    switch (paramInt)
    {
    default: 
      localObject = new java/lang/IllegalArgumentException;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>("Filter type ");
      localStringBuilder.append(paramInt);
      localStringBuilder.append(" is not supported in cursor");
      str = localStringBuilder.toString();
      ((IllegalArgumentException)localObject).<init>(str);
      throw ((Throwable)localObject);
    case 4: 
      str = "INBOX";
      break;
    case 3: 
      str = "SPAM";
      break;
    case 2: 
      str = "NON_SPAM";
    }
    paramInt = ((Cursor)localObject).getColumnIndex(str);
    int i = -1;
    if (paramInt != i) {
      return b.getInt(paramInt);
    }
    return 0;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */