package com.truecaller.messaging.data;

import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d
{
  private final e a;
  private final Provider b;
  private final Provider c;
  
  private j(e parame, Provider paramProvider1, Provider paramProvider2)
  {
    a = parame;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static j a(e parame, Provider paramProvider1, Provider paramProvider2)
  {
    j localj = new com/truecaller/messaging/data/j;
    localj.<init>(parame, paramProvider1, paramProvider2);
    return localj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */