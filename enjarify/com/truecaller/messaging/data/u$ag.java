package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import org.a.a.b;

final class u$ag
  extends u
{
  private final int b;
  private final b c;
  
  private u$ag(e parame, int paramInt, b paramb)
  {
    super(parame);
    b = paramInt;
    c = paramb;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".sendNextPendingMessage(");
    Object localObject = Integer.valueOf(b);
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */