package com.truecaller.messaging.data;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.androidactors.w;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.content.TruecallerContract.ab;
import com.truecaller.content.TruecallerContract.g;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.a.a;
import org.a.a.a.g;
import org.a.a.b;

public final class q
  implements o
{
  private final ContentResolver a;
  private final c b;
  private final e c;
  private final ab d;
  
  public q(ContentResolver paramContentResolver, c paramc, e parame, ab paramab)
  {
    a = paramContentResolver;
    b = paramc;
    c = parame;
    d = paramab;
  }
  
  private final Cursor b(int paramInt)
  {
    ContentResolver localContentResolver = a;
    Uri localUri = TruecallerContract.g.a(paramInt);
    String[] arrayOfString = { "COUNT()" };
    String str = c(paramInt);
    return localContentResolver.query(localUri, arrayOfString, str, null, null);
  }
  
  /* Error */
  private final long c()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 33	com/truecaller/messaging/data/q:a	Landroid/content/ContentResolver;
    //   4: astore_1
    //   5: iconst_3
    //   6: istore_2
    //   7: iload_2
    //   8: invokestatic 44	com/truecaller/content/TruecallerContract$g:a	(I)Landroid/net/Uri;
    //   11: astore_3
    //   12: iconst_1
    //   13: anewarray 48	java/lang/String
    //   16: dup
    //   17: iconst_0
    //   18: ldc 60
    //   20: aastore
    //   21: astore 4
    //   23: aload_0
    //   24: iload_2
    //   25: invokespecial 51	com/truecaller/messaging/data/q:c	(I)Ljava/lang/String;
    //   28: astore 5
    //   30: ldc 62
    //   32: astore 6
    //   34: aload_3
    //   35: astore 7
    //   37: aload 4
    //   39: astore_3
    //   40: aload 5
    //   42: astore 4
    //   44: aconst_null
    //   45: astore 5
    //   47: aload_1
    //   48: aload 7
    //   50: aload_3
    //   51: aload 4
    //   53: aconst_null
    //   54: aload 6
    //   56: invokevirtual 57	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   59: astore_1
    //   60: aload_1
    //   61: ifnull +92 -> 153
    //   64: aload_1
    //   65: checkcast 64	java/io/Closeable
    //   68: astore_1
    //   69: iconst_0
    //   70: istore_2
    //   71: aconst_null
    //   72: astore 7
    //   74: aload_1
    //   75: astore_3
    //   76: aload_1
    //   77: checkcast 66	android/database/Cursor
    //   80: astore_3
    //   81: aload_3
    //   82: invokeinterface 70 1 0
    //   87: istore 8
    //   89: iload 8
    //   91: ifle +33 -> 124
    //   94: aload_3
    //   95: invokeinterface 74 1 0
    //   100: pop
    //   101: iconst_0
    //   102: istore 8
    //   104: aconst_null
    //   105: astore 4
    //   107: aload_3
    //   108: iconst_0
    //   109: invokeinterface 78 2 0
    //   114: lstore 9
    //   116: aload_1
    //   117: aconst_null
    //   118: invokestatic 83	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   121: lload 9
    //   123: lreturn
    //   124: getstatic 88	c/x:a	Lc/x;
    //   127: astore_3
    //   128: aload_1
    //   129: aconst_null
    //   130: invokestatic 83	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   133: goto +20 -> 153
    //   136: astore_3
    //   137: goto +8 -> 145
    //   140: astore 7
    //   142: aload 7
    //   144: athrow
    //   145: aload_1
    //   146: aload 7
    //   148: invokestatic 83	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   151: aload_3
    //   152: athrow
    //   153: lconst_0
    //   154: lreturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	155	0	this	q
    //   4	142	1	localObject1	Object
    //   6	65	2	i	int
    //   11	117	3	localObject2	Object
    //   136	16	3	localObject3	Object
    //   21	85	4	localObject4	Object
    //   28	18	5	str1	String
    //   32	23	6	str2	String
    //   35	38	7	localObject5	Object
    //   140	7	7	localThrowable	Throwable
    //   87	16	8	j	int
    //   114	8	9	l	long
    // Exception table:
    //   from	to	target	type
    //   142	145	136	finally
    //   76	80	140	finally
    //   81	87	140	finally
    //   94	101	140	finally
    //   108	114	140	finally
    //   124	127	140	finally
  }
  
  private final String c(int paramInt)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("(");
    String str = d.a(paramInt);
    localStringBuilder.append(str);
    localStringBuilder.append(") AND (unread_messages_count > 0)");
    return localStringBuilder.toString();
  }
  
  public final w a()
  {
    Cursor localCursor = null;
    try
    {
      Object localObject1 = a;
      Object localObject3 = TruecallerContract.aa.a();
      Object localObject4 = "COUNT(*)";
      localObject4 = new String[] { localObject4 };
      localCursor = ((ContentResolver)localObject1).query((Uri)localObject3, (String[])localObject4, null, null, null);
      int i = 0;
      localObject1 = null;
      if (localCursor != null)
      {
        localCursor.moveToFirst();
        i = localCursor.getInt(0);
        localObject1 = Integer.valueOf(i);
        localObject1 = w.b(localObject1);
        localObject3 = "Promise.wrap(cursor.getInt(0))";
        k.a(localObject1, (String)localObject3);
        return (w)localObject1;
      }
      localObject1 = Integer.valueOf(0);
      localObject1 = w.b(localObject1);
      localObject3 = "Promise.wrap(0)";
      k.a(localObject1, (String)localObject3);
      return (w)localObject1;
    }
    finally
    {
      com.truecaller.util.q.a(localCursor);
    }
  }
  
  public final w a(int paramInt)
  {
    Object localObject1 = d;
    String str1 = ((ab)localObject1).a(paramInt);
    ContentResolver localContentResolver = a;
    Uri localUri = TruecallerContract.g.a(paramInt);
    String str2 = "date DESC";
    Object localObject2 = localContentResolver.query(localUri, null, str1, null, str2);
    if (localObject2 == null)
    {
      localObject2 = w.b(null);
      k.a(localObject2, "Promise.wrap(null)");
      return (w)localObject2;
    }
    localObject2 = b.a((Cursor)localObject2);
    localObject1 = (com.truecaller.androidactors.ab)q.f.a;
    localObject2 = w.a(localObject2, (com.truecaller.androidactors.ab)localObject1);
    k.a(localObject2, "Promise.wrap<Conversatio…(cursor), { it.close() })");
    return (w)localObject2;
  }
  
  public final w a(long paramLong)
  {
    Object localObject1 = null;
    try
    {
      ContentResolver localContentResolver = a;
      Uri localUri = TruecallerContract.g.a(paramLong);
      Object localObject2 = localContentResolver.query(localUri, null, null, null, null);
      if (localObject2 == null)
      {
        localObject2 = w.b(null);
        localObject4 = "Promise.wrap(null)";
        k.a(localObject2, (String)localObject4);
        com.truecaller.util.q.a(null);
        return (w)localObject2;
      }
      Object localObject4 = b;
      localObject2 = ((c)localObject4).a((Cursor)localObject2);
      if (localObject2 == null) {}
      try
      {
        k.a();
        boolean bool = ((a)localObject2).moveToNext();
        if (bool) {
          localObject1 = ((a)localObject2).b();
        }
        localObject4 = w.b(localObject1);
        localObject1 = "Promise.wrap(conversation)";
        k.a(localObject4, (String)localObject1);
        com.truecaller.util.q.a((Cursor)localObject2);
        return (w)localObject4;
      }
      finally
      {
        localObject1 = localObject2;
        localObject2 = localObject5;
      }
      com.truecaller.util.q.a((Cursor)localObject1);
    }
    finally {}
    throw ((Throwable)localObject3);
  }
  
  public final w a(long paramLong, int paramInt1, int paramInt2, Integer paramInteger)
  {
    ContentResolver localContentResolver = a;
    Uri localUri = TruecallerContract.ab.a(paramLong);
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("(status & 2) = 0 ");
    Object localObject2 = com.truecaller.content.c.a(c, paramInt1, paramInt2);
    ((StringBuilder)localObject1).append((String)localObject2);
    String str1 = ((StringBuilder)localObject1).toString();
    localObject1 = new java/lang/StringBuilder;
    localObject2 = "date DESC";
    ((StringBuilder)localObject1).<init>((String)localObject2);
    if (paramInteger != null)
    {
      paramInteger = (Number)paramInteger;
      int i = paramInteger.intValue();
      String str2 = " LIMIT ";
      localObject2 = String.valueOf(i);
      localObject2 = str2.concat((String)localObject2);
      if (localObject2 != null) {}
    }
    else
    {
      localObject2 = "";
    }
    ((StringBuilder)localObject1).append(localObject2);
    String str3 = ((StringBuilder)localObject1).toString();
    localObject1 = localContentResolver.query(localUri, null, str1, null, str3);
    if (localObject1 == null)
    {
      localObject1 = w.b(null);
      k.a(localObject1, "Promise.wrap(null)");
      return (w)localObject1;
    }
    localObject1 = b.b((Cursor)localObject1);
    localObject2 = (com.truecaller.androidactors.ab)q.d.a;
    localObject1 = w.a(localObject1, (com.truecaller.androidactors.ab)localObject2);
    k.a(localObject1, "Promise.wrap<MessageCurs…r(cursor)) { it.close() }");
    return (w)localObject1;
  }
  
  public final w a(Integer paramInteger)
  {
    Object localObject1 = "date DESC";
    Object localObject3;
    if (paramInteger != null)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append("  LIMIT ");
      ((StringBuilder)localObject2).append(paramInteger);
      localObject1 = ((StringBuilder)localObject2).toString();
      localObject3 = localObject1;
    }
    else
    {
      localObject3 = localObject1;
    }
    Object localObject2 = a;
    Uri localUri = TruecallerContract.g.a();
    paramInteger = ((ContentResolver)localObject2).query(localUri, null, null, null, (String)localObject3);
    if (paramInteger == null)
    {
      paramInteger = w.b(null);
      k.a(paramInteger, "Promise.wrap(null)");
      return paramInteger;
    }
    paramInteger = b.a(paramInteger);
    localObject1 = (com.truecaller.androidactors.ab)q.a.a;
    paramInteger = w.a(paramInteger, (com.truecaller.androidactors.ab)localObject1);
    k.a(paramInteger, "Promise.wrap<Conversatio…r(cursor)) { it.close() }");
    return paramInteger;
  }
  
  public final w a(String paramString)
  {
    Object localObject = "uriFilter";
    k.b(paramString, (String)localObject);
    ContentResolver localContentResolver = a;
    Uri localUri = TruecallerContract.ab.a(paramString);
    paramString = localContentResolver.query(localUri, null, null, null, null);
    if (paramString == null)
    {
      paramString = w.b(null);
      k.a(paramString, "Promise.wrap(null)");
      return paramString;
    }
    paramString = b.b(paramString);
    localObject = (com.truecaller.androidactors.ab)q.e.a;
    paramString = w.a(paramString, (com.truecaller.androidactors.ab)localObject);
    k.a(paramString, "Promise.wrap<MessageCurs…r(cursor)) { it.close() }");
    return paramString;
  }
  
  /* Error */
  public final w a(java.util.List paramList)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc -17
    //   3: invokestatic 21	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: new 90	java/lang/StringBuilder
    //   9: astore_2
    //   10: aload_2
    //   11: ldc -15
    //   13: invokespecial 95	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   16: aload_1
    //   17: astore_3
    //   18: aload_1
    //   19: checkcast 243	java/lang/Iterable
    //   22: astore_3
    //   23: ldc -11
    //   25: checkcast 247	java/lang/CharSequence
    //   28: astore 4
    //   30: bipush 62
    //   32: istore 5
    //   34: aload_3
    //   35: aload 4
    //   37: aconst_null
    //   38: aconst_null
    //   39: iconst_0
    //   40: aconst_null
    //   41: aconst_null
    //   42: iload 5
    //   44: invokestatic 253	c/a/m:a	(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lc/g/a/b;I)Ljava/lang/String;
    //   47: astore_1
    //   48: aload_2
    //   49: aload_1
    //   50: invokevirtual 103	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   53: pop
    //   54: aload_2
    //   55: ldc -1
    //   57: invokevirtual 103	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   60: pop
    //   61: aload_2
    //   62: invokevirtual 109	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   65: astore 6
    //   67: aload_0
    //   68: getfield 33	com/truecaller/messaging/data/q:a	Landroid/content/ContentResolver;
    //   71: astore 7
    //   73: invokestatic 114	com/truecaller/content/TruecallerContract$aa:a	()Landroid/net/Uri;
    //   76: astore_3
    //   77: iconst_1
    //   78: anewarray 48	java/lang/String
    //   81: dup
    //   82: iconst_0
    //   83: ldc_w 257
    //   86: aastore
    //   87: astore 8
    //   89: ldc 62
    //   91: astore 9
    //   93: aconst_null
    //   94: astore 4
    //   96: aload 7
    //   98: aload_3
    //   99: aconst_null
    //   100: aload 6
    //   102: aload 8
    //   104: aload 9
    //   106: invokevirtual 57	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   109: astore_1
    //   110: aload_1
    //   111: ifnull +92 -> 203
    //   114: aload_1
    //   115: checkcast 64	java/io/Closeable
    //   118: astore_1
    //   119: aconst_null
    //   120: astore_2
    //   121: aload_1
    //   122: astore 7
    //   124: aload_1
    //   125: checkcast 66	android/database/Cursor
    //   128: astore 7
    //   130: aload 7
    //   132: invokeinterface 74 1 0
    //   137: istore 10
    //   139: iload 10
    //   141: ifeq +33 -> 174
    //   144: getstatic 263	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   147: astore 7
    //   149: aload 7
    //   151: invokestatic 131	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   154: astore 7
    //   156: ldc_w 265
    //   159: astore_3
    //   160: aload 7
    //   162: aload_3
    //   163: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   166: aload_1
    //   167: aconst_null
    //   168: invokestatic 83	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   171: aload 7
    //   173: areturn
    //   174: getstatic 88	c/x:a	Lc/x;
    //   177: astore 7
    //   179: aload_1
    //   180: aconst_null
    //   181: invokestatic 83	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   184: goto +19 -> 203
    //   187: astore 7
    //   189: goto +6 -> 195
    //   192: astore_2
    //   193: aload_2
    //   194: athrow
    //   195: aload_1
    //   196: aload_2
    //   197: invokestatic 83	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   200: aload 7
    //   202: athrow
    //   203: getstatic 268	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   206: invokestatic 131	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   209: astore_1
    //   210: aload_1
    //   211: ldc_w 270
    //   214: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   217: aload_1
    //   218: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	219	0	this	q
    //   0	219	1	paramList	java.util.List
    //   9	112	2	localStringBuilder	StringBuilder
    //   192	5	2	localThrowable	Throwable
    //   17	146	3	localObject1	Object
    //   28	67	4	localCharSequence	CharSequence
    //   32	11	5	i	int
    //   65	36	6	str1	String
    //   71	107	7	localObject2	Object
    //   187	14	7	localObject3	Object
    //   87	16	8	arrayOfString	String[]
    //   91	14	9	str2	String
    //   137	3	10	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   193	195	187	finally
    //   124	128	192	finally
    //   130	137	192	finally
    //   144	147	192	finally
    //   149	154	192	finally
    //   162	166	192	finally
    //   174	177	192	finally
  }
  
  public final w a(b paramb)
  {
    k.b(paramb, "afterDate");
    Object localObject1 = null;
    try
    {
      Object localObject2 = a;
      Uri localUri = TruecallerContract.g.a();
      String str1 = "date>?";
      int i = 1;
      String[] arrayOfString = new String[i];
      String str2 = null;
      long l = a;
      paramb = String.valueOf(l);
      arrayOfString[0] = paramb;
      str2 = "date DESC LIMIT 1";
      paramb = ((ContentResolver)localObject2).query(localUri, null, str1, arrayOfString, str2);
      if (paramb == null)
      {
        paramb = w.b(null);
        localObject2 = "Promise.wrap(null)";
        k.a(paramb, (String)localObject2);
        com.truecaller.util.q.a(null);
        return paramb;
      }
      localObject2 = b;
      paramb = ((c)localObject2).a(paramb);
      if (paramb == null) {}
      try
      {
        k.a();
        boolean bool = paramb.moveToNext();
        if (bool) {
          localObject1 = paramb.b();
        }
        localObject1 = w.b(localObject1);
        localObject2 = "Promise.wrap(conversation)";
        k.a(localObject1, (String)localObject2);
        com.truecaller.util.q.a((Cursor)paramb);
        return (w)localObject1;
      }
      finally
      {
        localObject1 = paramb;
        paramb = (b)localObject3;
      }
      com.truecaller.util.q.a((Cursor)localObject1);
    }
    finally {}
    throw paramb;
  }
  
  /* Error */
  public final w a(com.truecaller.messaging.data.types.Participant[] paramArrayOfParticipant, int paramInt)
  {
    // Byte code:
    //   0: ldc_w 285
    //   3: astore_3
    //   4: aload_1
    //   5: aload_3
    //   6: invokestatic 21	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   9: aload_1
    //   10: arraylength
    //   11: istore 4
    //   13: iconst_1
    //   14: istore 5
    //   16: iconst_0
    //   17: istore 6
    //   19: iload 4
    //   21: ifne +9 -> 30
    //   24: iconst_1
    //   25: istore 4
    //   27: goto +8 -> 35
    //   30: iconst_0
    //   31: istore 4
    //   33: aconst_null
    //   34: astore_3
    //   35: iload 4
    //   37: iload 5
    //   39: ixor
    //   40: istore 4
    //   42: iconst_1
    //   43: anewarray 48	java/lang/String
    //   46: dup
    //   47: iconst_0
    //   48: ldc_w 287
    //   51: aastore
    //   52: astore 7
    //   54: iload 4
    //   56: aload 7
    //   58: invokestatic 293	com/truecaller/log/AssertionUtil:isTrue	(Z[Ljava/lang/String;)V
    //   61: new 295	com/truecaller/messaging/data/types/Draft$a
    //   64: astore_3
    //   65: aload_3
    //   66: invokespecial 296	com/truecaller/messaging/data/types/Draft$a:<init>	()V
    //   69: getstatic 301	com/truecaller/messaging/data/q$c:a	Lcom/truecaller/messaging/data/q$c;
    //   72: checkcast 303	java/util/Comparator
    //   75: astore 7
    //   77: aload_1
    //   78: arraylength
    //   79: istore 8
    //   81: aload_1
    //   82: iload 8
    //   84: invokestatic 309	java/util/Arrays:copyOf	([Ljava/lang/Object;I)[Ljava/lang/Object;
    //   87: checkcast 311	[Lcom/truecaller/messaging/data/types/Participant;
    //   90: astore 9
    //   92: aload 7
    //   94: ldc_w 313
    //   97: invokestatic 21	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   100: aload 9
    //   102: ldc_w 315
    //   105: invokestatic 21	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   108: new 317	java/util/TreeSet
    //   111: astore 10
    //   113: aload 10
    //   115: aload 7
    //   117: invokespecial 320	java/util/TreeSet:<init>	(Ljava/util/Comparator;)V
    //   120: aload 10
    //   122: checkcast 322	java/util/Collection
    //   125: astore 10
    //   127: aload 9
    //   129: aload 10
    //   131: invokestatic 327	c/a/f:b	([Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
    //   134: checkcast 317	java/util/TreeSet
    //   137: checkcast 243	java/lang/Iterable
    //   140: invokestatic 331	c/a/m:l	(Ljava/lang/Iterable;)Ljava/util/Set;
    //   143: astore 7
    //   145: new 317	java/util/TreeSet
    //   148: astore 9
    //   150: getstatic 336	com/truecaller/messaging/data/q$b:a	Lcom/truecaller/messaging/data/q$b;
    //   153: checkcast 303	java/util/Comparator
    //   156: astore 10
    //   158: aload 9
    //   160: aload 10
    //   162: invokespecial 320	java/util/TreeSet:<init>	(Ljava/util/Comparator;)V
    //   165: aconst_null
    //   166: astore 10
    //   168: new 338	java/util/ArrayList
    //   171: astore 11
    //   173: aload_1
    //   174: arraylength
    //   175: istore 12
    //   177: aload 11
    //   179: iload 12
    //   181: invokespecial 341	java/util/ArrayList:<init>	(I)V
    //   184: aload 11
    //   186: checkcast 322	java/util/Collection
    //   189: astore 11
    //   191: aload_1
    //   192: arraylength
    //   193: istore 12
    //   195: iconst_0
    //   196: istore 13
    //   198: aconst_null
    //   199: astore 14
    //   201: iload 13
    //   203: iload 12
    //   205: if_icmpge +35 -> 240
    //   208: aload_1
    //   209: iload 13
    //   211: aaload
    //   212: astore 15
    //   214: aload 15
    //   216: getfield 347	com/truecaller/messaging/data/types/Participant:f	Ljava/lang/String;
    //   219: astore 15
    //   221: aload 11
    //   223: aload 15
    //   225: invokeinterface 351 2 0
    //   230: pop
    //   231: iload 13
    //   233: iconst_1
    //   234: iadd
    //   235: istore 13
    //   237: goto -36 -> 201
    //   240: aload 11
    //   242: checkcast 353	java/util/List
    //   245: astore 11
    //   247: aload 11
    //   249: checkcast 322	java/util/Collection
    //   252: astore 11
    //   254: iconst_0
    //   255: anewarray 48	java/lang/String
    //   258: astore_1
    //   259: aload 11
    //   261: aload_1
    //   262: invokeinterface 357 2 0
    //   267: astore_1
    //   268: aload_1
    //   269: ifnull +1012 -> 1281
    //   272: aload_1
    //   273: checkcast 359	[Ljava/lang/String;
    //   276: astore_1
    //   277: aload_0
    //   278: getfield 35	com/truecaller/messaging/data/q:b	Lcom/truecaller/messaging/data/c;
    //   281: astore 11
    //   283: aload_0
    //   284: getfield 33	com/truecaller/messaging/data/q:a	Landroid/content/ContentResolver;
    //   287: astore 16
    //   289: aload_1
    //   290: invokestatic 364	com/truecaller/content/TruecallerContract$af:a	([Ljava/lang/String;)Landroid/net/Uri;
    //   293: astore 14
    //   295: iconst_0
    //   296: istore 17
    //   298: aconst_null
    //   299: astore 15
    //   301: iconst_0
    //   302: istore 18
    //   304: ldc_w 366
    //   307: astore 19
    //   309: aload 16
    //   311: aload 14
    //   313: aconst_null
    //   314: aconst_null
    //   315: aconst_null
    //   316: aload 19
    //   318: invokevirtual 57	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   321: astore_1
    //   322: aload 11
    //   324: aload_1
    //   325: invokeinterface 369 2 0
    //   330: astore_1
    //   331: aload_1
    //   332: ifnull +264 -> 596
    //   335: aload_1
    //   336: invokeinterface 372 1 0
    //   341: istore 20
    //   343: iload 20
    //   345: ifeq +251 -> 596
    //   348: aload_1
    //   349: invokeinterface 375 1 0
    //   354: astore 11
    //   356: ldc_w 377
    //   359: astore 16
    //   361: aload 11
    //   363: aload 16
    //   365: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   368: aload 7
    //   370: aload 11
    //   372: invokeinterface 382 2 0
    //   377: istore 12
    //   379: iload 12
    //   381: ifne +56 -> 437
    //   384: aload 11
    //   386: invokevirtual 385	com/truecaller/messaging/data/types/Participant:l	()Lcom/truecaller/messaging/data/types/Participant$a;
    //   389: astore 16
    //   391: aload 11
    //   393: getfield 388	com/truecaller/messaging/data/types/Participant:e	Ljava/lang/String;
    //   396: astore 14
    //   398: aload 16
    //   400: aload 14
    //   402: invokevirtual 393	com/truecaller/messaging/data/types/Participant$a:b	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Participant$a;
    //   405: astore 16
    //   407: aload 16
    //   409: invokevirtual 394	com/truecaller/messaging/data/types/Participant$a:a	()Lcom/truecaller/messaging/data/types/Participant;
    //   412: astore 16
    //   414: ldc_w 396
    //   417: astore 14
    //   419: aload 16
    //   421: aload 14
    //   423: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   426: aload 7
    //   428: aload 16
    //   430: invokeinterface 382 2 0
    //   435: istore 12
    //   437: iload 12
    //   439: ifne +88 -> 527
    //   442: aload_1
    //   443: invokeinterface 398 1 0
    //   448: astore 14
    //   450: aload 14
    //   452: astore 15
    //   454: aload 14
    //   456: checkcast 247	java/lang/CharSequence
    //   459: astore 15
    //   461: aload 15
    //   463: invokestatic 404	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   466: istore 17
    //   468: iload 17
    //   470: ifne +57 -> 527
    //   473: aload 11
    //   475: invokevirtual 385	com/truecaller/messaging/data/types/Participant:l	()Lcom/truecaller/messaging/data/types/Participant$a;
    //   478: astore 16
    //   480: aload 14
    //   482: ifnonnull +6 -> 488
    //   485: invokestatic 168	c/g/b/k:a	()V
    //   488: aload 16
    //   490: aload 14
    //   492: invokevirtual 393	com/truecaller/messaging/data/types/Participant$a:b	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Participant$a;
    //   495: astore 16
    //   497: aload 16
    //   499: invokevirtual 394	com/truecaller/messaging/data/types/Participant$a:a	()Lcom/truecaller/messaging/data/types/Participant;
    //   502: astore 16
    //   504: ldc_w 406
    //   507: astore 14
    //   509: aload 16
    //   511: aload 14
    //   513: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   516: aload 7
    //   518: aload 16
    //   520: invokeinterface 382 2 0
    //   525: istore 12
    //   527: iload 12
    //   529: ifeq +14 -> 543
    //   532: aload 9
    //   534: aload 11
    //   536: invokevirtual 407	java/util/TreeSet:add	(Ljava/lang/Object;)Z
    //   539: pop
    //   540: goto -205 -> 335
    //   543: aload 11
    //   545: getfield 409	com/truecaller/messaging/data/types/Participant:b	J
    //   548: lstore 21
    //   550: iconst_m1
    //   551: i2l
    //   552: lstore 23
    //   554: lload 21
    //   556: lload 23
    //   558: lcmp
    //   559: istore 18
    //   561: iload 18
    //   563: ifeq -228 -> 335
    //   566: aload 9
    //   568: aload 11
    //   570: invokevirtual 410	java/util/TreeSet:remove	(Ljava/lang/Object;)Z
    //   573: istore 12
    //   575: iload 12
    //   577: ifeq -242 -> 335
    //   580: aload 9
    //   582: aload 11
    //   584: invokevirtual 407	java/util/TreeSet:add	(Ljava/lang/Object;)Z
    //   587: pop
    //   588: goto -253 -> 335
    //   591: astore 25
    //   593: goto +712 -> 1305
    //   596: aload_1
    //   597: checkcast 66	android/database/Cursor
    //   600: astore_1
    //   601: aload_1
    //   602: invokestatic 140	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   605: aload 7
    //   607: invokeinterface 412 1 0
    //   612: istore 26
    //   614: iload 26
    //   616: ifeq +251 -> 867
    //   619: aload 9
    //   621: astore_1
    //   622: aload 9
    //   624: checkcast 243	java/lang/Iterable
    //   627: astore_1
    //   628: new 338	java/util/ArrayList
    //   631: astore 7
    //   633: aload_1
    //   634: bipush 10
    //   636: invokestatic 416	c/a/m:a	(Ljava/lang/Iterable;I)I
    //   639: istore 20
    //   641: aload 7
    //   643: iload 20
    //   645: invokespecial 341	java/util/ArrayList:<init>	(I)V
    //   648: aload 7
    //   650: checkcast 322	java/util/Collection
    //   653: astore 7
    //   655: aload_1
    //   656: invokeinterface 420 1 0
    //   661: astore_1
    //   662: aload_1
    //   663: invokeinterface 425 1 0
    //   668: istore 20
    //   670: iload 20
    //   672: ifeq +30 -> 702
    //   675: aload_1
    //   676: invokeinterface 429 1 0
    //   681: checkcast 343	com/truecaller/messaging/data/types/Participant
    //   684: getfield 347	com/truecaller/messaging/data/types/Participant:f	Ljava/lang/String;
    //   687: astore 11
    //   689: aload 7
    //   691: aload 11
    //   693: invokeinterface 351 2 0
    //   698: pop
    //   699: goto -37 -> 662
    //   702: aload 7
    //   704: checkcast 353	java/util/List
    //   707: checkcast 322	java/util/Collection
    //   710: astore 7
    //   712: iconst_0
    //   713: anewarray 48	java/lang/String
    //   716: astore_1
    //   717: aload 7
    //   719: aload_1
    //   720: invokeinterface 357 2 0
    //   725: astore_1
    //   726: aload_1
    //   727: ifnull +127 -> 854
    //   730: aload_1
    //   731: checkcast 359	[Ljava/lang/String;
    //   734: astore_1
    //   735: aload_0
    //   736: getfield 35	com/truecaller/messaging/data/q:b	Lcom/truecaller/messaging/data/c;
    //   739: astore 7
    //   741: aload_0
    //   742: getfield 33	com/truecaller/messaging/data/q:a	Landroid/content/ContentResolver;
    //   745: astore 11
    //   747: aload_1
    //   748: iload_2
    //   749: invokestatic 432	com/truecaller/content/TruecallerContract$g:a	([Ljava/lang/String;I)Landroid/net/Uri;
    //   752: astore 16
    //   754: iconst_0
    //   755: istore 13
    //   757: aconst_null
    //   758: astore 14
    //   760: iconst_0
    //   761: istore 17
    //   763: aconst_null
    //   764: astore 15
    //   766: iconst_0
    //   767: istore 18
    //   769: aload 11
    //   771: aload 16
    //   773: aconst_null
    //   774: aconst_null
    //   775: aconst_null
    //   776: aconst_null
    //   777: invokevirtual 57	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   780: astore_1
    //   781: aload 7
    //   783: aload_1
    //   784: invokeinterface 151 2 0
    //   789: astore_1
    //   790: aload_1
    //   791: ifnull +33 -> 824
    //   794: aload_1
    //   795: invokeinterface 433 1 0
    //   800: istore_2
    //   801: iload_2
    //   802: ifeq +22 -> 824
    //   805: aload_1
    //   806: invokeinterface 176 1 0
    //   811: astore 25
    //   813: goto +16 -> 829
    //   816: astore 25
    //   818: aload_1
    //   819: astore 10
    //   821: goto +22 -> 843
    //   824: iconst_0
    //   825: istore_2
    //   826: aconst_null
    //   827: astore 25
    //   829: aload_1
    //   830: checkcast 66	android/database/Cursor
    //   833: astore_1
    //   834: aload_1
    //   835: invokestatic 140	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   838: goto +49 -> 887
    //   841: astore 25
    //   843: aload 10
    //   845: checkcast 66	android/database/Cursor
    //   848: invokestatic 140	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   851: aload 25
    //   853: athrow
    //   854: new 435	c/u
    //   857: astore_1
    //   858: aload_1
    //   859: ldc_w 437
    //   862: invokespecial 438	c/u:<init>	(Ljava/lang/String;)V
    //   865: aload_1
    //   866: athrow
    //   867: aload 7
    //   869: checkcast 322	java/util/Collection
    //   872: astore 7
    //   874: aload 9
    //   876: aload 7
    //   878: invokevirtual 442	java/util/TreeSet:addAll	(Ljava/util/Collection;)Z
    //   881: pop
    //   882: iconst_0
    //   883: istore_2
    //   884: aconst_null
    //   885: astore 25
    //   887: aload 9
    //   889: checkcast 322	java/util/Collection
    //   892: astore 9
    //   894: iconst_0
    //   895: anewarray 343	com/truecaller/messaging/data/types/Participant
    //   898: astore_1
    //   899: aload 9
    //   901: aload_1
    //   902: invokeinterface 357 2 0
    //   907: astore_1
    //   908: aload_1
    //   909: ifnull +359 -> 1268
    //   912: aload_1
    //   913: checkcast 311	[Lcom/truecaller/messaging/data/types/Participant;
    //   916: astore_1
    //   917: aload_3
    //   918: aload_1
    //   919: invokevirtual 445	com/truecaller/messaging/data/types/Draft$a:a	([Lcom/truecaller/messaging/data/types/Participant;)Lcom/truecaller/messaging/data/types/Draft$a;
    //   922: pop
    //   923: aload 25
    //   925: ifnull +326 -> 1251
    //   928: aload_3
    //   929: aload 25
    //   931: invokevirtual 448	com/truecaller/messaging/data/types/Draft$a:a	(Lcom/truecaller/messaging/data/types/Conversation;)Lcom/truecaller/messaging/data/types/Draft$a;
    //   934: pop
    //   935: aload_0
    //   936: getfield 33	com/truecaller/messaging/data/q:a	Landroid/content/ContentResolver;
    //   939: astore 11
    //   941: aload 25
    //   943: getfield 451	com/truecaller/messaging/data/types/Conversation:a	J
    //   946: lstore 27
    //   948: lload 27
    //   950: invokestatic 181	com/truecaller/content/TruecallerContract$ab:a	(J)Landroid/net/Uri;
    //   953: astore 16
    //   955: iconst_0
    //   956: istore 13
    //   958: aconst_null
    //   959: astore 14
    //   961: ldc_w 453
    //   964: astore 15
    //   966: iconst_0
    //   967: istore 18
    //   969: aload 11
    //   971: aload 16
    //   973: aconst_null
    //   974: aload 15
    //   976: aconst_null
    //   977: aconst_null
    //   978: invokevirtual 57	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   981: astore 10
    //   983: aload 10
    //   985: ifnull +209 -> 1194
    //   988: aload 10
    //   990: invokeinterface 74 1 0
    //   995: istore 26
    //   997: iload 26
    //   999: ifne +6 -> 1005
    //   1002: goto +192 -> 1194
    //   1005: aload_0
    //   1006: getfield 35	com/truecaller/messaging/data/q:b	Lcom/truecaller/messaging/data/c;
    //   1009: astore_1
    //   1010: aload_1
    //   1011: aload 10
    //   1013: invokeinterface 209 2 0
    //   1018: astore_1
    //   1019: aload_1
    //   1020: ifnonnull +6 -> 1026
    //   1023: invokestatic 168	c/g/b/k:a	()V
    //   1026: aload_1
    //   1027: invokeinterface 458 1 0
    //   1032: astore_1
    //   1033: ldc_w 460
    //   1036: astore 25
    //   1038: aload_1
    //   1039: aload 25
    //   1041: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1044: aload_1
    //   1045: invokevirtual 465	com/truecaller/messaging/data/types/Message:a	()J
    //   1048: lstore 29
    //   1050: aload_3
    //   1051: lload 29
    //   1053: invokevirtual 468	com/truecaller/messaging/data/types/Draft$a:a	(J)Lcom/truecaller/messaging/data/types/Draft$a;
    //   1056: pop
    //   1057: aload_1
    //   1058: getfield 471	com/truecaller/messaging/data/types/Message:x	J
    //   1061: lstore 29
    //   1063: aload_3
    //   1064: lload 29
    //   1066: invokevirtual 473	com/truecaller/messaging/data/types/Draft$a:b	(J)Lcom/truecaller/messaging/data/types/Draft$a;
    //   1069: pop
    //   1070: aload_1
    //   1071: invokevirtual 476	com/truecaller/messaging/data/types/Message:g	()Z
    //   1074: istore_2
    //   1075: iload_2
    //   1076: ifeq +16 -> 1092
    //   1079: aload_1
    //   1080: getfield 480	com/truecaller/messaging/data/types/Message:v	Lcom/truecaller/messaging/data/types/ReplySnippet;
    //   1083: astore 25
    //   1085: aload_3
    //   1086: aload 25
    //   1088: invokevirtual 483	com/truecaller/messaging/data/types/Draft$a:a	(Lcom/truecaller/messaging/data/types/ReplySnippet;)Lcom/truecaller/messaging/data/types/Draft$a;
    //   1091: pop
    //   1092: aload_1
    //   1093: getfield 487	com/truecaller/messaging/data/types/Message:n	[Lcom/truecaller/messaging/data/types/Entity;
    //   1096: astore_1
    //   1097: ldc_w 489
    //   1100: astore 25
    //   1102: aload_1
    //   1103: aload 25
    //   1105: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1108: aload_1
    //   1109: arraylength
    //   1110: istore_2
    //   1111: iload 6
    //   1113: iload_2
    //   1114: if_icmpge +122 -> 1236
    //   1117: aload_1
    //   1118: iload 6
    //   1120: aaload
    //   1121: astore 7
    //   1123: ldc_w 491
    //   1126: astore 9
    //   1128: aload 7
    //   1130: aload 9
    //   1132: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1135: aload 7
    //   1137: invokevirtual 495	com/truecaller/messaging/data/types/Entity:a	()Z
    //   1140: istore 8
    //   1142: iload 8
    //   1144: ifeq +27 -> 1171
    //   1147: aload 7
    //   1149: checkcast 497	com/truecaller/messaging/data/types/TextEntity
    //   1152: astore 7
    //   1154: aload 7
    //   1156: getfield 499	com/truecaller/messaging/data/types/TextEntity:a	Ljava/lang/String;
    //   1159: astore 7
    //   1161: aload_3
    //   1162: aload 7
    //   1164: invokevirtual 502	com/truecaller/messaging/data/types/Draft$a:a	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Draft$a;
    //   1167: pop
    //   1168: goto +17 -> 1185
    //   1171: aload 7
    //   1173: checkcast 504	com/truecaller/messaging/data/types/BinaryEntity
    //   1176: astore 7
    //   1178: aload_3
    //   1179: aload 7
    //   1181: invokevirtual 507	com/truecaller/messaging/data/types/Draft$a:a	(Lcom/truecaller/messaging/data/types/BinaryEntity;)Lcom/truecaller/messaging/data/types/Draft$a;
    //   1184: pop
    //   1185: iload 6
    //   1187: iconst_1
    //   1188: iadd
    //   1189: istore 6
    //   1191: goto -80 -> 1111
    //   1194: aload_3
    //   1195: invokevirtual 510	com/truecaller/messaging/data/types/Draft$a:d	()Lcom/truecaller/messaging/data/types/Draft;
    //   1198: astore_1
    //   1199: aload_1
    //   1200: invokestatic 131	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   1203: astore_1
    //   1204: ldc_w 512
    //   1207: astore 25
    //   1209: aload_1
    //   1210: aload 25
    //   1212: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1215: aload 10
    //   1217: invokestatic 140	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   1220: aload_1
    //   1221: areturn
    //   1222: astore_1
    //   1223: goto +21 -> 1244
    //   1226: astore_1
    //   1227: aload_1
    //   1228: checkcast 514	java/lang/Throwable
    //   1231: astore_1
    //   1232: aload_1
    //   1233: invokestatic 518	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   1236: aload 10
    //   1238: invokestatic 140	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   1241: goto +10 -> 1251
    //   1244: aload 10
    //   1246: invokestatic 140	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   1249: aload_1
    //   1250: athrow
    //   1251: aload_3
    //   1252: invokevirtual 510	com/truecaller/messaging/data/types/Draft$a:d	()Lcom/truecaller/messaging/data/types/Draft;
    //   1255: invokestatic 131	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   1258: astore_1
    //   1259: aload_1
    //   1260: ldc_w 512
    //   1263: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1266: aload_1
    //   1267: areturn
    //   1268: new 435	c/u
    //   1271: astore_1
    //   1272: aload_1
    //   1273: ldc_w 437
    //   1276: invokespecial 438	c/u:<init>	(Ljava/lang/String;)V
    //   1279: aload_1
    //   1280: athrow
    //   1281: new 435	c/u
    //   1284: astore_1
    //   1285: ldc_w 437
    //   1288: astore 25
    //   1290: aload_1
    //   1291: aload 25
    //   1293: invokespecial 438	c/u:<init>	(Ljava/lang/String;)V
    //   1296: aload_1
    //   1297: athrow
    //   1298: astore 25
    //   1300: iconst_0
    //   1301: istore 26
    //   1303: aconst_null
    //   1304: astore_1
    //   1305: aload_1
    //   1306: checkcast 66	android/database/Cursor
    //   1309: invokestatic 140	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   1312: aload 25
    //   1314: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1315	0	this	q
    //   0	1315	1	paramArrayOfParticipant	com.truecaller.messaging.data.types.Participant[]
    //   0	1315	2	paramInt	int
    //   3	1249	3	localObject1	Object
    //   11	44	4	i	int
    //   14	26	5	j	int
    //   17	1173	6	k	int
    //   52	1128	7	localObject2	Object
    //   79	4	8	m	int
    //   1140	3	8	bool1	boolean
    //   90	1041	9	localObject3	Object
    //   111	1134	10	localObject4	Object
    //   171	799	11	localObject5	Object
    //   175	31	12	n	int
    //   377	199	12	bool2	boolean
    //   196	761	13	i1	int
    //   199	761	14	localObject6	Object
    //   212	763	15	localObject7	Object
    //   287	685	16	localObject8	Object
    //   296	466	17	bool3	boolean
    //   302	666	18	bool4	boolean
    //   307	10	19	str	String
    //   341	3	20	bool5	boolean
    //   639	5	20	i2	int
    //   668	3	20	bool6	boolean
    //   548	7	21	l1	long
    //   552	5	23	l2	long
    //   591	1	25	localObject9	Object
    //   811	1	25	localConversation	com.truecaller.messaging.data.types.Conversation
    //   816	1	25	localObject10	Object
    //   827	1	25	localObject11	Object
    //   841	11	25	localObject12	Object
    //   885	407	25	localObject13	Object
    //   1298	15	25	localObject14	Object
    //   612	690	26	bool7	boolean
    //   946	3	27	l3	long
    //   1048	17	29	l4	long
    // Exception table:
    //   from	to	target	type
    //   335	341	591	finally
    //   348	354	591	finally
    //   363	368	591	finally
    //   370	377	591	finally
    //   384	389	591	finally
    //   391	396	591	finally
    //   400	405	591	finally
    //   407	412	591	finally
    //   421	426	591	finally
    //   428	435	591	finally
    //   442	448	591	finally
    //   454	459	591	finally
    //   461	466	591	finally
    //   473	478	591	finally
    //   485	488	591	finally
    //   490	495	591	finally
    //   497	502	591	finally
    //   511	516	591	finally
    //   518	525	591	finally
    //   534	540	591	finally
    //   543	548	591	finally
    //   568	573	591	finally
    //   582	588	591	finally
    //   794	800	816	finally
    //   805	811	816	finally
    //   735	739	841	finally
    //   741	745	841	finally
    //   748	752	841	finally
    //   776	780	841	finally
    //   783	789	841	finally
    //   935	939	1222	finally
    //   941	946	1222	finally
    //   948	953	1222	finally
    //   977	981	1222	finally
    //   988	995	1222	finally
    //   1005	1009	1222	finally
    //   1011	1018	1222	finally
    //   1023	1026	1222	finally
    //   1026	1032	1222	finally
    //   1039	1044	1222	finally
    //   1044	1048	1222	finally
    //   1051	1057	1222	finally
    //   1057	1061	1222	finally
    //   1064	1070	1222	finally
    //   1070	1074	1222	finally
    //   1079	1083	1222	finally
    //   1086	1092	1222	finally
    //   1092	1096	1222	finally
    //   1103	1108	1222	finally
    //   1108	1110	1222	finally
    //   1118	1121	1222	finally
    //   1130	1135	1222	finally
    //   1135	1140	1222	finally
    //   1147	1152	1222	finally
    //   1154	1159	1222	finally
    //   1162	1168	1222	finally
    //   1171	1176	1222	finally
    //   1179	1185	1222	finally
    //   1194	1198	1222	finally
    //   1199	1203	1222	finally
    //   1210	1215	1222	finally
    //   1227	1231	1222	finally
    //   1232	1236	1222	finally
    //   935	939	1226	android/database/SQLException
    //   941	946	1226	android/database/SQLException
    //   948	953	1226	android/database/SQLException
    //   977	981	1226	android/database/SQLException
    //   988	995	1226	android/database/SQLException
    //   1005	1009	1226	android/database/SQLException
    //   1011	1018	1226	android/database/SQLException
    //   1023	1026	1226	android/database/SQLException
    //   1026	1032	1226	android/database/SQLException
    //   1039	1044	1226	android/database/SQLException
    //   1044	1048	1226	android/database/SQLException
    //   1051	1057	1226	android/database/SQLException
    //   1057	1061	1226	android/database/SQLException
    //   1064	1070	1226	android/database/SQLException
    //   1070	1074	1226	android/database/SQLException
    //   1079	1083	1226	android/database/SQLException
    //   1086	1092	1226	android/database/SQLException
    //   1092	1096	1226	android/database/SQLException
    //   1103	1108	1226	android/database/SQLException
    //   1108	1110	1226	android/database/SQLException
    //   1118	1121	1226	android/database/SQLException
    //   1130	1135	1226	android/database/SQLException
    //   1135	1140	1226	android/database/SQLException
    //   1147	1152	1226	android/database/SQLException
    //   1154	1159	1226	android/database/SQLException
    //   1162	1168	1226	android/database/SQLException
    //   1171	1176	1226	android/database/SQLException
    //   1179	1185	1226	android/database/SQLException
    //   1194	1198	1226	android/database/SQLException
    //   1199	1203	1226	android/database/SQLException
    //   1210	1215	1226	android/database/SQLException
    //   168	171	1298	finally
    //   173	175	1298	finally
    //   179	184	1298	finally
    //   184	189	1298	finally
    //   191	193	1298	finally
    //   209	212	1298	finally
    //   214	219	1298	finally
    //   223	231	1298	finally
    //   240	245	1298	finally
    //   247	252	1298	finally
    //   254	258	1298	finally
    //   261	267	1298	finally
    //   272	276	1298	finally
    //   277	281	1298	finally
    //   283	287	1298	finally
    //   289	293	1298	finally
    //   316	321	1298	finally
    //   324	330	1298	finally
    //   1281	1284	1298	finally
    //   1291	1296	1298	finally
    //   1296	1298	1298	finally
  }
  
  /* Error */
  public final w b()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 37	com/truecaller/messaging/data/q:c	Lcom/truecaller/featuretoggles/e;
    //   4: astore_1
    //   5: aload_1
    //   6: invokevirtual 526	com/truecaller/featuretoggles/e:w	()Lcom/truecaller/featuretoggles/b;
    //   9: astore_1
    //   10: aload_1
    //   11: invokeinterface 529 1 0
    //   16: istore_2
    //   17: iconst_4
    //   18: istore_3
    //   19: iconst_1
    //   20: istore 4
    //   22: iconst_2
    //   23: istore 5
    //   25: iconst_3
    //   26: istore 6
    //   28: iload_2
    //   29: ifeq +132 -> 161
    //   32: aload_0
    //   33: iload_3
    //   34: invokespecial 534	com/truecaller/messaging/data/q:b	(I)Landroid/database/Cursor;
    //   37: astore_1
    //   38: aload_0
    //   39: iload 6
    //   41: invokespecial 534	com/truecaller/messaging/data/q:b	(I)Landroid/database/Cursor;
    //   44: astore 7
    //   46: aload_0
    //   47: iload 5
    //   49: invokespecial 534	com/truecaller/messaging/data/q:b	(I)Landroid/database/Cursor;
    //   52: astore 8
    //   54: new 536	android/database/MergeCursor
    //   57: astore 9
    //   59: iload 6
    //   61: anewarray 66	android/database/Cursor
    //   64: astore 10
    //   66: aload 10
    //   68: iconst_0
    //   69: aload_1
    //   70: aastore
    //   71: aload 10
    //   73: iload 4
    //   75: aload 7
    //   77: aastore
    //   78: aload 10
    //   80: iload 5
    //   82: aload 8
    //   84: aastore
    //   85: aload 9
    //   87: aload 10
    //   89: invokespecial 539	android/database/MergeCursor:<init>	([Landroid/database/Cursor;)V
    //   92: new 541	c/n
    //   95: astore_1
    //   96: aload_0
    //   97: getfield 35	com/truecaller/messaging/data/q:b	Lcom/truecaller/messaging/data/c;
    //   100: astore 7
    //   102: aload 9
    //   104: checkcast 66	android/database/Cursor
    //   107: astore 9
    //   109: aload 7
    //   111: aload 9
    //   113: iload 4
    //   115: invokeinterface 544 3 0
    //   120: astore 7
    //   122: aload_0
    //   123: invokespecial 546	com/truecaller/messaging/data/q:c	()J
    //   126: lstore 11
    //   128: lload 11
    //   130: invokestatic 551	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   133: astore 13
    //   135: aload_1
    //   136: aload 7
    //   138: aload 13
    //   140: invokespecial 554	c/n:<init>	(Ljava/lang/Object;Ljava/lang/Object;)V
    //   143: aload_1
    //   144: invokestatic 131	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   147: astore_1
    //   148: ldc_w 556
    //   151: astore 7
    //   153: aload_1
    //   154: aload 7
    //   156: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   159: aload_1
    //   160: areturn
    //   161: iload 6
    //   163: anewarray 48	java/lang/String
    //   166: astore_1
    //   167: new 90	java/lang/StringBuilder
    //   170: astore 8
    //   172: ldc_w 558
    //   175: astore 9
    //   177: aload 8
    //   179: aload 9
    //   181: invokespecial 95	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   184: aload_0
    //   185: iload_3
    //   186: invokespecial 51	com/truecaller/messaging/data/q:c	(I)Ljava/lang/String;
    //   189: astore 7
    //   191: aload 8
    //   193: aload 7
    //   195: invokevirtual 103	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   198: pop
    //   199: bipush 32
    //   201: istore_3
    //   202: aload 8
    //   204: iload_3
    //   205: invokevirtual 562	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   208: pop
    //   209: ldc_w 564
    //   212: astore 9
    //   214: aload 8
    //   216: aload 9
    //   218: invokevirtual 103	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   221: pop
    //   222: aload 8
    //   224: invokevirtual 109	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   227: astore 8
    //   229: aload_1
    //   230: iconst_0
    //   231: aload 8
    //   233: aastore
    //   234: new 90	java/lang/StringBuilder
    //   237: astore 8
    //   239: ldc_w 558
    //   242: astore 9
    //   244: aload 8
    //   246: aload 9
    //   248: invokespecial 95	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   251: aload_0
    //   252: iload 6
    //   254: invokespecial 51	com/truecaller/messaging/data/q:c	(I)Ljava/lang/String;
    //   257: astore 10
    //   259: aload 8
    //   261: aload 10
    //   263: invokevirtual 103	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   266: pop
    //   267: aload 8
    //   269: iload_3
    //   270: invokevirtual 562	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   273: pop
    //   274: ldc_w 566
    //   277: astore 10
    //   279: aload 8
    //   281: aload 10
    //   283: invokevirtual 103	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   286: pop
    //   287: aload 8
    //   289: invokevirtual 109	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   292: astore 10
    //   294: aload_1
    //   295: iload 4
    //   297: aload 10
    //   299: aastore
    //   300: new 90	java/lang/StringBuilder
    //   303: astore 13
    //   305: ldc_w 558
    //   308: astore 10
    //   310: aload 13
    //   312: aload 10
    //   314: invokespecial 95	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   317: aload_0
    //   318: iload 5
    //   320: invokespecial 51	com/truecaller/messaging/data/q:c	(I)Ljava/lang/String;
    //   323: astore 10
    //   325: aload 13
    //   327: aload 10
    //   329: invokevirtual 103	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   332: pop
    //   333: aload 13
    //   335: iload_3
    //   336: invokevirtual 562	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   339: pop
    //   340: ldc_w 568
    //   343: astore 7
    //   345: aload 13
    //   347: aload 7
    //   349: invokevirtual 103	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   352: pop
    //   353: aload 13
    //   355: invokevirtual 109	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   358: astore 7
    //   360: aload_1
    //   361: iload 5
    //   363: aload 7
    //   365: aastore
    //   366: aload_1
    //   367: invokestatic 571	c/a/m:b	([Ljava/lang/Object;)Ljava/util/List;
    //   370: astore_1
    //   371: aload_0
    //   372: getfield 33	com/truecaller/messaging/data/q:a	Landroid/content/ContentResolver;
    //   375: astore 10
    //   377: invokestatic 220	com/truecaller/content/TruecallerContract$g:a	()Landroid/net/Uri;
    //   380: astore 8
    //   382: aload_1
    //   383: checkcast 322	java/util/Collection
    //   386: astore_1
    //   387: aload_1
    //   388: ifnull +122 -> 510
    //   391: iconst_0
    //   392: anewarray 48	java/lang/String
    //   395: astore 7
    //   397: aload_1
    //   398: aload 7
    //   400: invokeinterface 357 2 0
    //   405: astore_1
    //   406: aload_1
    //   407: ifnull +86 -> 493
    //   410: aload_1
    //   411: astore 9
    //   413: aload_1
    //   414: checkcast 359	[Ljava/lang/String;
    //   417: astore 9
    //   419: aload 10
    //   421: aload 8
    //   423: aload 9
    //   425: aconst_null
    //   426: aconst_null
    //   427: aconst_null
    //   428: invokevirtual 57	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   431: astore_1
    //   432: new 541	c/n
    //   435: astore 7
    //   437: aload_0
    //   438: getfield 35	com/truecaller/messaging/data/q:b	Lcom/truecaller/messaging/data/c;
    //   441: astore 13
    //   443: aload 13
    //   445: aload_1
    //   446: iconst_0
    //   447: invokeinterface 544 3 0
    //   452: astore_1
    //   453: aload_0
    //   454: invokespecial 546	com/truecaller/messaging/data/q:c	()J
    //   457: lstore 11
    //   459: lload 11
    //   461: invokestatic 551	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   464: astore 13
    //   466: aload 7
    //   468: aload_1
    //   469: aload 13
    //   471: invokespecial 554	c/n:<init>	(Ljava/lang/Object;Ljava/lang/Object;)V
    //   474: aload 7
    //   476: invokestatic 131	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   479: astore_1
    //   480: ldc_w 556
    //   483: astore 7
    //   485: aload_1
    //   486: aload 7
    //   488: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   491: aload_1
    //   492: areturn
    //   493: new 435	c/u
    //   496: astore_1
    //   497: ldc_w 437
    //   500: astore 7
    //   502: aload_1
    //   503: aload 7
    //   505: invokespecial 438	c/u:<init>	(Ljava/lang/String;)V
    //   508: aload_1
    //   509: athrow
    //   510: new 435	c/u
    //   513: astore_1
    //   514: ldc_w 573
    //   517: astore 7
    //   519: aload_1
    //   520: aload 7
    //   522: invokespecial 438	c/u:<init>	(Ljava/lang/String;)V
    //   525: aload_1
    //   526: athrow
    //   527: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	528	0	this	q
    //   4	522	1	localObject1	Object
    //   16	13	2	bool1	boolean
    //   18	318	3	c1	char
    //   20	276	4	bool2	boolean
    //   23	339	5	i	int
    //   26	227	6	j	int
    //   44	477	7	localObject2	Object
    //   52	370	8	localObject3	Object
    //   57	367	9	localObject4	Object
    //   64	356	10	localObject5	Object
    //   126	334	11	l	long
    //   133	337	13	localObject6	Object
    // Exception table:
    //   from	to	target	type
    //   0	4	527	finally
    //   5	9	527	finally
    //   10	16	527	finally
    //   33	37	527	finally
    //   39	44	527	finally
    //   47	52	527	finally
    //   54	57	527	finally
    //   59	64	527	finally
    //   69	71	527	finally
    //   75	78	527	finally
    //   82	85	527	finally
    //   87	92	527	finally
    //   92	95	527	finally
    //   96	100	527	finally
    //   102	107	527	finally
    //   113	120	527	finally
    //   122	126	527	finally
    //   128	133	527	finally
    //   138	143	527	finally
    //   143	147	527	finally
    //   154	159	527	finally
    //   161	166	527	finally
    //   167	170	527	finally
    //   179	184	527	finally
    //   185	189	527	finally
    //   193	199	527	finally
    //   204	209	527	finally
    //   216	222	527	finally
    //   222	227	527	finally
    //   231	234	527	finally
    //   234	237	527	finally
    //   246	251	527	finally
    //   252	257	527	finally
    //   261	267	527	finally
    //   269	274	527	finally
    //   281	287	527	finally
    //   287	292	527	finally
    //   297	300	527	finally
    //   300	303	527	finally
    //   312	317	527	finally
    //   318	323	527	finally
    //   327	333	527	finally
    //   335	340	527	finally
    //   347	353	527	finally
    //   353	358	527	finally
    //   363	366	527	finally
    //   366	370	527	finally
    //   371	375	527	finally
    //   377	380	527	finally
    //   382	386	527	finally
    //   391	395	527	finally
    //   398	405	527	finally
    //   413	417	527	finally
    //   427	431	527	finally
    //   432	435	527	finally
    //   437	441	527	finally
    //   446	452	527	finally
    //   453	457	527	finally
    //   459	464	527	finally
    //   469	474	527	finally
    //   474	479	527	finally
    //   486	491	527	finally
    //   493	496	527	finally
    //   503	508	527	finally
    //   508	510	527	finally
    //   510	513	527	finally
    //   520	525	527	finally
    //   525	527	527	finally
  }
  
  /* Error */
  public final w b(long paramLong)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aload_0
    //   3: getfield 33	com/truecaller/messaging/data/q:a	Landroid/content/ContentResolver;
    //   6: astore 4
    //   8: invokestatic 574	com/truecaller/content/TruecallerContract$ab:a	()Landroid/net/Uri;
    //   11: astore 5
    //   13: ldc_w 576
    //   16: astore 6
    //   18: iconst_1
    //   19: istore 7
    //   21: iload 7
    //   23: anewarray 48	java/lang/String
    //   26: astore 8
    //   28: lload_1
    //   29: invokestatic 283	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   32: astore 9
    //   34: aload 8
    //   36: iconst_0
    //   37: aload 9
    //   39: aastore
    //   40: aload 4
    //   42: aload 5
    //   44: aconst_null
    //   45: aload 6
    //   47: aload 8
    //   49: aconst_null
    //   50: invokevirtual 57	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   53: astore 9
    //   55: aload_0
    //   56: getfield 35	com/truecaller/messaging/data/q:b	Lcom/truecaller/messaging/data/c;
    //   59: astore 10
    //   61: aload 10
    //   63: aload 9
    //   65: invokeinterface 209 2 0
    //   70: astore 9
    //   72: aload 9
    //   74: ifnull +83 -> 157
    //   77: aload 9
    //   79: invokeinterface 577 1 0
    //   84: istore 11
    //   86: iload 11
    //   88: ifeq +69 -> 157
    //   91: aload 9
    //   93: invokeinterface 458 1 0
    //   98: astore 10
    //   100: aload 10
    //   102: invokestatic 131	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   105: astore 10
    //   107: ldc_w 579
    //   110: astore 4
    //   112: aload 10
    //   114: aload 4
    //   116: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   119: aload 9
    //   121: checkcast 66	android/database/Cursor
    //   124: invokestatic 140	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   127: aload 10
    //   129: areturn
    //   130: astore 10
    //   132: goto +13 -> 145
    //   135: astore 10
    //   137: goto +49 -> 186
    //   140: astore 10
    //   142: aconst_null
    //   143: astore 9
    //   145: aload 10
    //   147: checkcast 514	java/lang/Throwable
    //   150: astore 10
    //   152: aload 10
    //   154: invokestatic 518	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   157: aload 9
    //   159: checkcast 66	android/database/Cursor
    //   162: invokestatic 140	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   165: aconst_null
    //   166: invokestatic 131	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   169: astore 9
    //   171: aload 9
    //   173: ldc -110
    //   175: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   178: aload 9
    //   180: areturn
    //   181: astore 10
    //   183: aload 9
    //   185: astore_3
    //   186: aload_3
    //   187: checkcast 66	android/database/Cursor
    //   190: invokestatic 140	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   193: aload 10
    //   195: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	196	0	this	q
    //   0	196	1	paramLong	long
    //   1	186	3	localObject1	Object
    //   6	109	4	localObject2	Object
    //   11	32	5	localUri	Uri
    //   16	30	6	str	String
    //   19	3	7	i	int
    //   26	22	8	arrayOfString	String[]
    //   32	152	9	localObject3	Object
    //   59	69	10	localObject4	Object
    //   130	1	10	localSQLException1	android.database.SQLException
    //   135	1	10	localObject5	Object
    //   140	6	10	localSQLException2	android.database.SQLException
    //   150	3	10	localThrowable	Throwable
    //   181	13	10	localObject6	Object
    //   84	3	11	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   77	84	130	android/database/SQLException
    //   91	98	130	android/database/SQLException
    //   100	105	130	android/database/SQLException
    //   114	119	130	android/database/SQLException
    //   2	6	135	finally
    //   8	11	135	finally
    //   21	26	135	finally
    //   28	32	135	finally
    //   37	40	135	finally
    //   49	53	135	finally
    //   55	59	135	finally
    //   63	70	135	finally
    //   2	6	140	android/database/SQLException
    //   8	11	140	android/database/SQLException
    //   21	26	140	android/database/SQLException
    //   28	32	140	android/database/SQLException
    //   37	40	140	android/database/SQLException
    //   49	53	140	android/database/SQLException
    //   55	59	140	android/database/SQLException
    //   63	70	140	android/database/SQLException
    //   77	84	181	finally
    //   91	98	181	finally
    //   100	105	181	finally
    //   114	119	181	finally
    //   145	150	181	finally
    //   152	157	181	finally
  }
  
  /* Error */
  public final w b(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 581
    //   4: invokestatic 21	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: aload_0
    //   8: getfield 33	com/truecaller/messaging/data/q:a	Landroid/content/ContentResolver;
    //   11: astore_2
    //   12: invokestatic 584	com/truecaller/content/TruecallerContract$w:a	()Landroid/net/Uri;
    //   15: astore_3
    //   16: iconst_1
    //   17: anewarray 48	java/lang/String
    //   20: dup
    //   21: iconst_0
    //   22: ldc_w 586
    //   25: aastore
    //   26: astore 4
    //   28: ldc_w 588
    //   31: astore 5
    //   33: iconst_1
    //   34: anewarray 48	java/lang/String
    //   37: astore 6
    //   39: aconst_null
    //   40: astore 7
    //   42: aload 6
    //   44: iconst_0
    //   45: aload_1
    //   46: aastore
    //   47: aload_2
    //   48: aload_3
    //   49: aload 4
    //   51: aload 5
    //   53: aload 6
    //   55: aconst_null
    //   56: invokevirtual 57	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   59: astore_1
    //   60: aload_1
    //   61: ifnull +105 -> 166
    //   64: aload_1
    //   65: checkcast 64	java/io/Closeable
    //   68: astore_1
    //   69: aconst_null
    //   70: astore_2
    //   71: aload_1
    //   72: astore_3
    //   73: aload_1
    //   74: checkcast 66	android/database/Cursor
    //   77: astore_3
    //   78: aload_3
    //   79: invokeinterface 74 1 0
    //   84: istore 8
    //   86: iload 8
    //   88: ifeq +44 -> 132
    //   91: aload_3
    //   92: iconst_0
    //   93: invokeinterface 78 2 0
    //   98: lstore 9
    //   100: lload 9
    //   102: invokestatic 551	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   105: astore 7
    //   107: aload 7
    //   109: invokestatic 131	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   112: astore 7
    //   114: ldc_w 590
    //   117: astore_3
    //   118: aload 7
    //   120: aload_3
    //   121: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   124: aload_1
    //   125: aconst_null
    //   126: invokestatic 83	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   129: aload 7
    //   131: areturn
    //   132: getstatic 88	c/x:a	Lc/x;
    //   135: astore 7
    //   137: aload_1
    //   138: aconst_null
    //   139: invokestatic 83	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   142: goto +24 -> 166
    //   145: astore 7
    //   147: goto +11 -> 158
    //   150: astore 7
    //   152: aload 7
    //   154: astore_2
    //   155: aload 7
    //   157: athrow
    //   158: aload_1
    //   159: aload_2
    //   160: invokestatic 83	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   163: aload 7
    //   165: athrow
    //   166: iconst_m1
    //   167: i2l
    //   168: invokestatic 551	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   171: invokestatic 131	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   174: astore_1
    //   175: aload_1
    //   176: ldc_w 592
    //   179: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   182: aload_1
    //   183: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	184	0	this	q
    //   0	184	1	paramString	String
    //   11	149	2	localObject1	Object
    //   15	106	3	localObject2	Object
    //   26	24	4	arrayOfString1	String[]
    //   31	21	5	str	String
    //   37	17	6	arrayOfString2	String[]
    //   40	96	7	localObject3	Object
    //   145	1	7	localObject4	Object
    //   150	14	7	localObject5	Object
    //   84	3	8	bool	boolean
    //   98	3	9	l	long
    // Exception table:
    //   from	to	target	type
    //   155	158	145	finally
    //   73	77	150	finally
    //   78	84	150	finally
    //   92	98	150	finally
    //   100	105	150	finally
    //   107	112	150	finally
    //   120	124	150	finally
    //   132	135	150	finally
  }
  
  /* Error */
  public final w c(long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 33	com/truecaller/messaging/data/q:a	Landroid/content/ContentResolver;
    //   4: astore_3
    //   5: invokestatic 584	com/truecaller/content/TruecallerContract$w:a	()Landroid/net/Uri;
    //   8: astore 4
    //   10: iconst_1
    //   11: anewarray 48	java/lang/String
    //   14: dup
    //   15: iconst_0
    //   16: ldc_w 594
    //   19: aastore
    //   20: astore 5
    //   22: ldc_w 596
    //   25: astore 6
    //   27: iconst_1
    //   28: istore 7
    //   30: iload 7
    //   32: anewarray 48	java/lang/String
    //   35: astore 8
    //   37: lload_1
    //   38: invokestatic 283	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   41: astore 9
    //   43: aconst_null
    //   44: astore 10
    //   46: aload 8
    //   48: iconst_0
    //   49: aload 9
    //   51: aastore
    //   52: aload_3
    //   53: aload 4
    //   55: aload 5
    //   57: aload 6
    //   59: aload 8
    //   61: aconst_null
    //   62: invokevirtual 57	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   65: astore 9
    //   67: aconst_null
    //   68: astore_3
    //   69: aload 9
    //   71: ifnull +109 -> 180
    //   74: aload 9
    //   76: checkcast 64	java/io/Closeable
    //   79: astore 9
    //   81: aload 9
    //   83: astore 4
    //   85: aload 9
    //   87: checkcast 66	android/database/Cursor
    //   90: astore 4
    //   92: aload 4
    //   94: invokeinterface 74 1 0
    //   99: istore 11
    //   101: iload 11
    //   103: ifeq +41 -> 144
    //   106: aload 4
    //   108: iconst_0
    //   109: invokeinterface 599 2 0
    //   114: astore 10
    //   116: aload 10
    //   118: invokestatic 131	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   121: astore 10
    //   123: ldc_w 601
    //   126: astore 4
    //   128: aload 10
    //   130: aload 4
    //   132: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   135: aload 9
    //   137: aconst_null
    //   138: invokestatic 83	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   141: aload 10
    //   143: areturn
    //   144: getstatic 88	c/x:a	Lc/x;
    //   147: astore 10
    //   149: aload 9
    //   151: aconst_null
    //   152: invokestatic 83	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   155: goto +25 -> 180
    //   158: astore 10
    //   160: goto +11 -> 171
    //   163: astore 10
    //   165: aload 10
    //   167: astore_3
    //   168: aload 10
    //   170: athrow
    //   171: aload 9
    //   173: aload_3
    //   174: invokestatic 83	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   177: aload 10
    //   179: athrow
    //   180: aconst_null
    //   181: invokestatic 131	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   184: astore 9
    //   186: aload 9
    //   188: ldc -110
    //   190: invokestatic 135	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   193: aload 9
    //   195: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	196	0	this	q
    //   0	196	1	paramLong	long
    //   4	170	3	localObject1	Object
    //   8	123	4	localObject2	Object
    //   20	36	5	arrayOfString1	String[]
    //   25	33	6	str	String
    //   28	3	7	i	int
    //   35	25	8	arrayOfString2	String[]
    //   41	153	9	localObject3	Object
    //   44	104	10	localObject4	Object
    //   158	1	10	localObject5	Object
    //   163	15	10	localObject6	Object
    //   99	3	11	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   168	171	158	finally
    //   85	90	163	finally
    //   92	99	163	finally
    //   108	114	163	finally
    //   116	121	163	finally
    //   130	135	163	finally
    //   144	147	163	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */