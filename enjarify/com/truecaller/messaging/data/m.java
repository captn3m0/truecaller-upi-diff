package com.truecaller.messaging.data;

import dagger.a.d;
import javax.inject.Provider;

public final class m
  implements d
{
  private final e a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  private final Provider g;
  
  private m(e parame, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6)
  {
    a = parame;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
  }
  
  public static m a(e parame, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6)
  {
    m localm = new com/truecaller/messaging/data/m;
    localm.<init>(parame, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6);
    return localm;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */