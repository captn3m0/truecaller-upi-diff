package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class u$w
  extends u
{
  private final boolean b;
  
  private u$w(e parame, boolean paramBoolean)
  {
    super(parame);
    b = paramBoolean;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".performFullSync(");
    String str = a(Boolean.valueOf(b), 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */