package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.types.Participant;
import java.util.List;
import org.a.a.b;

public final class p
  implements o
{
  private final v a;
  
  public p(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return o.class.equals(paramClass);
  }
  
  public final w a()
  {
    v localv = a;
    p.a locala = new com/truecaller/messaging/data/p$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, (byte)0);
    return w.a(localv, locala);
  }
  
  public final w a(int paramInt)
  {
    v localv = a;
    p.k localk = new com/truecaller/messaging/data/p$k;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localk.<init>(locale, paramInt, (byte)0);
    return w.a(localv, localk);
  }
  
  public final w a(long paramLong)
  {
    v localv = a;
    p.c localc = new com/truecaller/messaging/data/p$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, paramLong, (byte)0);
    return w.a(localv, localc);
  }
  
  public final w a(long paramLong, int paramInt1, int paramInt2, Integer paramInteger)
  {
    v localv = a;
    p.f localf = new com/truecaller/messaging/data/p$f;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localf.<init>(locale, paramLong, paramInt1, paramInt2, paramInteger, (byte)0);
    return w.a(localv, localf);
  }
  
  public final w a(Integer paramInteger)
  {
    v localv = a;
    p.b localb = new com/truecaller/messaging/data/p$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramInteger, (byte)0);
    return w.a(localv, localb);
  }
  
  public final w a(String paramString)
  {
    v localv = a;
    p.g localg = new com/truecaller/messaging/data/p$g;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localg.<init>(locale, paramString, (byte)0);
    return w.a(localv, localg);
  }
  
  public final w a(List paramList)
  {
    v localv = a;
    p.m localm = new com/truecaller/messaging/data/p$m;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localm.<init>(locale, paramList, (byte)0);
    return w.a(localv, localm);
  }
  
  public final w a(b paramb)
  {
    v localv = a;
    p.d locald = new com/truecaller/messaging/data/p$d;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locald.<init>(locale, paramb, (byte)0);
    return w.a(localv, locald);
  }
  
  public final w a(Participant[] paramArrayOfParticipant, int paramInt)
  {
    v localv = a;
    p.e locale = new com/truecaller/messaging/data/p$e;
    e locale1 = new com/truecaller/androidactors/e;
    locale1.<init>();
    locale.<init>(locale1, paramArrayOfParticipant, paramInt, (byte)0);
    return w.a(localv, locale);
  }
  
  public final w b()
  {
    v localv = a;
    p.l locall = new com/truecaller/messaging/data/p$l;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locall.<init>(locale, (byte)0);
    return w.a(localv, locall);
  }
  
  public final w b(long paramLong)
  {
    v localv = a;
    p.i locali = new com/truecaller/messaging/data/p$i;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locali.<init>(locale, paramLong, (byte)0);
    return w.a(localv, locali);
  }
  
  public final w b(String paramString)
  {
    v localv = a;
    p.h localh = new com/truecaller/messaging/data/p$h;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localh.<init>(locale, paramString, (byte)0);
    return w.a(localv, localh);
  }
  
  public final w c(long paramLong)
  {
    v localv = a;
    p.j localj = new com/truecaller/messaging/data/p$j;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localj.<init>(locale, paramLong, (byte)0);
    return w.a(localv, localj);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */