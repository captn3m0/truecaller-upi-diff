package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.messaging.data.types.Participant;

final class p$e
  extends u
{
  private final Participant[] b;
  private final int c;
  
  private p$e(e parame, Participant[] paramArrayOfParticipant, int paramInt)
  {
    super(parame);
    b = paramArrayOfParticipant;
    c = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".fetchDraft(");
    String str = a(b, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(Integer.valueOf(c), 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.p.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */