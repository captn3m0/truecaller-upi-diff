package com.truecaller.messaging.data;

import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d
{
  private final e a;
  private final Provider b;
  
  private f(e parame, Provider paramProvider)
  {
    a = parame;
    b = paramProvider;
  }
  
  public static f a(e parame, Provider paramProvider)
  {
    f localf = new com/truecaller/messaging/data/f;
    localf.<init>(parame, paramProvider);
    return localf;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */