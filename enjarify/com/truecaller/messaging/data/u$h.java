package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import org.a.a.b;

final class u$h
  extends u
{
  private final b b;
  
  private u$h(e parame, b paramb)
  {
    super(parame);
    b = paramb;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".enqueueScheduledMessages(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */