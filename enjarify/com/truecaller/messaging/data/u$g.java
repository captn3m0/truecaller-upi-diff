package com.truecaller.messaging.data;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.messaging.data.types.Message;

final class u$g
  extends u
{
  private final Message b;
  
  private u$g(e parame, Message paramMessage)
  {
    super(parame);
    b = paramMessage;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".enqueueFailedMessage(");
    String str = a(b, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.u.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */