package com.truecaller.messaging.data;

import android.database.Cursor;
import android.database.CursorWrapper;
import com.truecaller.messaging.data.a.m;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Participant.a;

final class aa
  extends CursorWrapper
  implements m
{
  private final int a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  private final int j;
  private final int k;
  private final int l;
  private final int m;
  private final int n;
  private final int o;
  private final int p;
  private final int q;
  private final int r;
  private final int s;
  private final int t;
  
  aa(Cursor paramCursor)
  {
    super(paramCursor);
    int i1 = paramCursor.getColumnIndexOrThrow("_id");
    a = i1;
    i1 = paramCursor.getColumnIndexOrThrow("type");
    b = i1;
    i1 = paramCursor.getColumnIndexOrThrow("raw_destination");
    c = i1;
    i1 = paramCursor.getColumnIndexOrThrow("normalized_destination");
    d = i1;
    i1 = paramCursor.getColumnIndexOrThrow("country_code");
    e = i1;
    i1 = paramCursor.getColumnIndexOrThrow("tc_im_peer_id");
    f = i1;
    i1 = paramCursor.getColumnIndexOrThrow("tc_id");
    g = i1;
    i1 = paramCursor.getColumnIndexOrThrow("aggregated_contact_id");
    h = i1;
    i1 = paramCursor.getColumnIndexOrThrow("filter_action");
    i = i1;
    i1 = paramCursor.getColumnIndexOrThrow("is_top_spammer");
    j = i1;
    i1 = paramCursor.getColumnIndexOrThrow("top_spam_score");
    k = i1;
    i1 = paramCursor.getColumnIndexOrThrow("name");
    l = i1;
    i1 = paramCursor.getColumnIndexOrThrow("image_url");
    m = i1;
    i1 = paramCursor.getColumnIndexOrThrow("source");
    n = i1;
    i1 = paramCursor.getColumnIndexOrThrow("phonebook_id");
    o = i1;
    i1 = paramCursor.getColumnIndexOrThrow("spam_score");
    p = i1;
    i1 = paramCursor.getColumnIndex("national_destination");
    q = i1;
    i1 = paramCursor.getColumnIndex("badges");
    r = i1;
    i1 = paramCursor.getColumnIndex("company_name");
    s = i1;
    int i2 = paramCursor.getColumnIndex("search_time");
    t = i2;
  }
  
  public final Participant a()
  {
    Participant.a locala = new com/truecaller/messaging/data/types/Participant$a;
    int i1 = b;
    i1 = getInt(i1);
    locala.<init>(i1);
    i1 = a;
    long l1 = getLong(i1);
    b = l1;
    i1 = c;
    String str = getString(i1);
    d = str;
    i1 = d;
    str = getString(i1);
    e = str;
    i1 = e;
    str = getString(i1);
    f = str;
    i1 = f;
    str = getString(i1);
    c = str;
    i1 = g;
    str = getString(i1);
    g = str;
    i1 = h;
    l1 = getLong(i1);
    h = l1;
    i1 = i;
    i1 = getInt(i1);
    i = i1;
    i1 = j;
    i1 = getInt(i1);
    if (i1 != 0)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      str = null;
    }
    j = i1;
    i1 = k;
    i1 = getInt(i1);
    k = i1;
    i1 = l;
    str = getString(i1);
    l = str;
    i1 = m;
    str = getString(i1);
    m = str;
    i1 = n;
    i1 = getInt(i1);
    n = i1;
    i1 = o;
    l1 = getLong(i1);
    o = l1;
    i1 = p;
    i1 = getInt(i1);
    p = i1;
    i1 = r;
    i1 = getInt(i1);
    s = i1;
    i1 = s;
    str = getString(i1);
    q = str;
    i1 = t;
    l1 = getLong(i1);
    r = l1;
    return locala.a();
  }
  
  public final String b()
  {
    int i1 = q;
    int i2 = -1;
    if (i1 == i2) {
      return null;
    }
    return getString(i1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.data.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */