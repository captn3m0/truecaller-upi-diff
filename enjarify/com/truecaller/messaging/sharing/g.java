package com.truecaller.messaging.sharing;

import android.content.Intent;

abstract interface g
{
  public abstract Intent a();
  
  public abstract boolean a(String paramString);
  
  public abstract Intent b();
  
  public abstract void finish();
  
  public abstract void startActivity(Intent paramIntent);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.sharing.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */