package com.truecaller.messaging.sharing;

import android.content.Intent;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.messaging.conversation.ConversationActivity;
import com.truecaller.messaging.newconversation.NewConversationActivity;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.wizard.utils.i;

public class SharingActivity
  extends AppCompatActivity
  implements g
{
  e a;
  
  public final Intent a()
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(this, NewConversationActivity.class);
    return localIntent;
  }
  
  public final boolean a(String paramString)
  {
    return i.a(this, paramString, 1);
  }
  
  public final Intent b()
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(this, ConversationActivity.class);
    return localIntent;
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getTheme();
    int i = aresId;
    paramBundle.applyStyle(i, false);
    paramBundle = new com/truecaller/messaging/sharing/a$a;
    paramBundle.<init>((byte)0);
    Object localObject1 = (bp)dagger.a.g.a(((bk)getApplicationContext()).a());
    b = ((bp)localObject1);
    localObject1 = new com/truecaller/messaging/sharing/c;
    Object localObject2 = getIntent();
    ((c)localObject1).<init>((Intent)localObject2);
    localObject1 = (c)dagger.a.g.a(localObject1);
    a = ((c)localObject1);
    dagger.a.g.a(a, c.class);
    dagger.a.g.a(b, bp.class);
    localObject1 = new com/truecaller/messaging/sharing/a;
    localObject2 = a;
    paramBundle = b;
    ((a)localObject1).<init>((c)localObject2, paramBundle, (byte)0);
    ((b)localObject1).a(this);
    a.a(this);
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    a.y_();
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    i.a(paramArrayOfString, paramArrayOfInt);
    a.a(paramArrayOfString, paramArrayOfInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.sharing.SharingActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */