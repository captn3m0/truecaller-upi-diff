package com.truecaller.messaging.sharing;

import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import com.truecaller.common.h.o;
import com.truecaller.common.h.u;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.utils.l;
import org.c.a.a.a.a;

final class f
  extends e
{
  private final Intent a;
  private final l c;
  private final u d;
  
  f(Intent paramIntent, l paraml, u paramu)
  {
    a = paramIntent;
    c = paraml;
    d = paramu;
  }
  
  private void a()
  {
    Object localObject1 = b;
    if (localObject1 == null) {
      return;
    }
    localObject1 = a.getAction();
    Object localObject2 = "android.intent.action.SENDTO";
    boolean bool1 = ((String)localObject2).equals(localObject1);
    Object localObject3 = null;
    if (!bool1)
    {
      localObject2 = "android.intent.action.VIEW";
      bool1 = ((String)localObject2).equals(localObject1);
      if (!bool1)
      {
        localObject2 = "android.intent.action.SEND";
        bool1 = ((String)localObject2).equals(localObject1);
        if (!bool1)
        {
          localObject2 = "android.intent.action.SEND_MULTIPLE";
          bool2 = ((String)localObject2).equals(localObject1);
          if (!bool2)
          {
            bool2 = false;
            localObject1 = null;
            break label117;
          }
        }
        localObject1 = a;
        localObject2 = "com.truecaller.suggestions.extra.PHONE_NUMBER";
        localObject1 = (Uri)((Intent)localObject1).getParcelableExtra((String)localObject2);
        break label117;
      }
    }
    localObject1 = a.getData();
    label117:
    if (localObject1 != null)
    {
      localObject2 = d;
      localObject3 = Participant.a((Uri)localObject1, (u)localObject2, "-1");
    }
    boolean bool2 = a.b((Object[])localObject3);
    if (bool2)
    {
      localObject1 = ((g)b).b();
      ((Intent)localObject1).putExtra("participants", (Parcelable[])localObject3);
      localObject2 = "send_intent";
      localObject3 = a;
      ((Intent)localObject1).putExtra((String)localObject2, (Parcelable)localObject3);
    }
    else
    {
      localObject1 = ((g)b).a();
      localObject2 = "send_intent";
      localObject3 = a;
      ((Intent)localObject1).putExtra((String)localObject2, (Parcelable)localObject3);
    }
    o.a(a, (Intent)localObject1);
    ((g)b).startActivity((Intent)localObject1);
  }
  
  final void a(String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    int i = 0;
    localObject = null;
    for (;;)
    {
      int j = paramArrayOfString.length;
      if (i >= j) {
        break;
      }
      String str1 = "android.permission.READ_EXTERNAL_STORAGE";
      String str2 = paramArrayOfString[i];
      boolean bool = str1.equals(str2);
      if (bool)
      {
        int k = paramArrayOfInt[i];
        if (k == 0)
        {
          a();
          break;
        }
      }
      i += 1;
    }
    ((g)b).finish();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.sharing.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */