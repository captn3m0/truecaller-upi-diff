package com.truecaller.messaging;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;
import com.truecaller.messaging.data.types.BinaryEntity;

public final class ForwardContentItem$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    ForwardContentItem localForwardContentItem = new com/truecaller/messaging/ForwardContentItem;
    String str = paramParcel.readString();
    ClassLoader localClassLoader = ForwardContentItem.class.getClassLoader();
    paramParcel = (BinaryEntity)paramParcel.readParcelable(localClassLoader);
    localForwardContentItem.<init>(str, paramParcel);
    return localForwardContentItem;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new ForwardContentItem[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.ForwardContentItem.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */