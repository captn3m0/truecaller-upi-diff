package com.truecaller.messaging;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.google.common.collect.Lists;
import java.util.List;
import org.a.a.a.g;
import org.a.a.b;
import org.c.a.a.a.k;

final class i
  implements h
{
  private final SharedPreferences a;
  
  i(SharedPreferences paramSharedPreferences)
  {
    a = paramSharedPreferences;
  }
  
  private void f(String paramString)
  {
    SharedPreferences.Editor localEditor = a.edit();
    long l = a.getLong(paramString, 0L) + 1L;
    localEditor.putLong(paramString, l).apply();
  }
  
  public final String A()
  {
    return a.getString("messageSignature", null);
  }
  
  public final String B()
  {
    return a.getString("autoDownloadMedia", "wifi");
  }
  
  public final void C()
  {
    a.edit().putBoolean("additionalPermissionsDialogShown", true).apply();
  }
  
  public final boolean D()
  {
    return a.getBoolean("additionalPermissionsDialogShown", false);
  }
  
  public final b E()
  {
    b localb = new org/a/a/b;
    long l = a.getLong("lastImReadTime", 0L);
    localb.<init>(l);
    return localb;
  }
  
  public final b F()
  {
    b localb = new org/a/a/b;
    long l = a.getLong("lastImSendTime", 0L);
    localb.<init>(l);
    return localb;
  }
  
  public final String G()
  {
    return a.getString("imPeerId", null);
  }
  
  public final void H()
  {
    a.edit().putBoolean("hasUnconsumedEvents", true).apply();
  }
  
  public final int I()
  {
    return a.getInt("imForceUpgradeVersion", 0);
  }
  
  public final long J()
  {
    return a.getLong("getImUserMissTtl", 0L);
  }
  
  public final int K()
  {
    return a.getInt("getUrlSpamScoreThreshold", 0);
  }
  
  public final long L()
  {
    return a.getLong("imMaxMediaSize", 104857600L);
  }
  
  public final boolean M()
  {
    return a.getBoolean("imTracingEnabled", false);
  }
  
  public final boolean N()
  {
    return a.getBoolean("imPromo", true);
  }
  
  public final void O()
  {
    a.edit().putBoolean("imPromo", false).apply();
  }
  
  public final int P()
  {
    return a.getInt("smsPermissionForBlockQuestionCount", 0);
  }
  
  public final boolean Q()
  {
    return a.getBoolean("isImPresenceReported", false);
  }
  
  public final long R()
  {
    return a.getLong("imInitialSyncTimestamp", -1);
  }
  
  public final long S()
  {
    return a.getLong("spamTabVisitedTimestamp", 0L);
  }
  
  public final List T()
  {
    return Lists.newArrayList(a.getString("reactions_emoji", "👍,🤣,😮,😍,😠,😢,👎").split(","));
  }
  
  public final boolean U()
  {
    return a.getBoolean("appUpdatePromo", false);
  }
  
  public final long V()
  {
    return a.getLong("lastTimeAppUpdatePromo", 0L);
  }
  
  public final int W()
  {
    return a.getInt("appUpdatePromoPeriod", 30);
  }
  
  public final int X()
  {
    return a.getInt("appUpdateToVersion", -1);
  }
  
  public final int Y()
  {
    return a.getInt("imNewJoinersPeriodDays", 7);
  }
  
  public final int Z()
  {
    return a.getInt("imGroupBatchParticipantCount", 20);
  }
  
  public final long a()
  {
    return a.getLong("MsgLastSyncTime", 0L);
  }
  
  public final long a(int paramInt)
  {
    SharedPreferences localSharedPreferences = a;
    String str = String.valueOf(paramInt);
    str = "MsgLastTransportSyncTime_".concat(str);
    return localSharedPreferences.getLong(str, 0L);
  }
  
  public final void a(int paramInt, long paramLong)
  {
    SharedPreferences.Editor localEditor = a.edit();
    String str = String.valueOf(paramInt);
    str = "MsgLastTransportSyncTime_".concat(str);
    localEditor.putLong(str, paramLong).apply();
  }
  
  public final void a(int paramInt, boolean paramBoolean)
  {
    String str;
    if (paramInt == 0)
    {
      str = "requestSmsDeliveryReport";
    }
    else
    {
      int i = 1;
      if (i != paramInt) {
        return;
      }
      str = "requestSimTwoSmsDeliveryReport";
    }
    a.edit().putBoolean(str, paramBoolean).apply();
    return;
  }
  
  public final void a(long paramLong)
  {
    a.edit().putLong("MsgLastSyncTime", paramLong).apply();
  }
  
  public final void a(String paramString)
  {
    SharedPreferences.Editor localEditor = a.edit();
    paramString = k.n(paramString);
    localEditor.putString("messagingRingtone", paramString).apply();
  }
  
  public final void a(b paramb)
  {
    SharedPreferences.Editor localEditor = a.edit();
    long l = a;
    localEditor.putLong("LastMessagePromotionDate", l).apply();
  }
  
  public final void a(boolean paramBoolean)
  {
    a.edit().putBoolean("wasDefaultSmsApp", paramBoolean).apply();
  }
  
  public final boolean aa()
  {
    return a.getBoolean("historyMessagesInitialSyncCompleted", false);
  }
  
  public final int ab()
  {
    return a.getInt("imVoiceClipMaxDurationMins", 59);
  }
  
  public final boolean ac()
  {
    return a.getBoolean("hasDismissedReadReplyPromo", false);
  }
  
  public final void b(int paramInt, boolean paramBoolean)
  {
    String str;
    if (paramInt == 0)
    {
      str = "MmsAutoDownload";
    }
    else
    {
      int i = 1;
      if (i != paramInt) {
        return;
      }
      str = "SimTwoMmsAutoDownload";
    }
    a.edit().putBoolean(str, paramBoolean).apply();
    return;
  }
  
  public final void b(long paramLong)
  {
    a.edit().putLong("defaultSmsAppTimestamp", paramLong).apply();
  }
  
  public final void b(String paramString)
  {
    a.edit().putString("messageSignature", paramString).apply();
  }
  
  public final void b(b paramb)
  {
    SharedPreferences.Editor localEditor = a.edit();
    long l = a;
    localEditor.putLong("JoinImUsersNotificationDate", l).apply();
  }
  
  public final void b(boolean paramBoolean)
  {
    a.edit().putBoolean("hadSmsReadAccess", paramBoolean).apply();
  }
  
  public final boolean b()
  {
    return a.getBoolean("wasDefaultSmsApp", false);
  }
  
  public final boolean b(int paramInt)
  {
    int i = 1;
    String str;
    if (paramInt == 0)
    {
      str = "requestSmsDeliveryReport";
    }
    else
    {
      if (i != paramInt) {
        break label32;
      }
      str = "requestSimTwoSmsDeliveryReport";
    }
    return a.getBoolean(str, i);
    label32:
    return false;
  }
  
  public final long c()
  {
    return a.getLong("defaultSmsAppTimestamp", 0L);
  }
  
  public final void c(int paramInt, boolean paramBoolean)
  {
    String str;
    if (paramInt == 0)
    {
      str = "MmsAutoDownloadWhenRoaming";
    }
    else
    {
      int i = 1;
      if (i != paramInt) {
        return;
      }
      str = "SimTwoMmsAutoDownloadWhenRoaming";
    }
    a.edit().putBoolean(str, paramBoolean).apply();
    return;
  }
  
  public final void c(long paramLong)
  {
    a.edit().putLong("latestSpamProtectionOffNotificationShowtime", paramLong).apply();
  }
  
  public final void c(String paramString)
  {
    SharedPreferences.Editor localEditor = a.edit();
    String str1 = "autoDownloadMedia";
    String str2 = "wifi";
    if (paramString == null) {
      paramString = str2;
    }
    localEditor.putString(str1, paramString).apply();
  }
  
  public final void c(b paramb)
  {
    SharedPreferences.Editor localEditor = a.edit();
    long l = a;
    localEditor.putLong("lastImReadTime", l).apply();
  }
  
  public final void c(boolean paramBoolean)
  {
    a.edit().putBoolean("BlockedMessagesNotification", paramBoolean).apply();
  }
  
  public final boolean c(int paramInt)
  {
    int i = 1;
    String str;
    if (paramInt == 0)
    {
      str = "MmsAutoDownload";
    }
    else
    {
      if (i != paramInt) {
        return i;
      }
      str = "SimTwoMmsAutoDownload";
    }
    return a.getBoolean(str, i);
    return i;
  }
  
  public final void d(long paramLong)
  {
    a.edit().putLong("getImUserMissTtl", paramLong).apply();
  }
  
  public final void d(String paramString)
  {
    a.edit().putString("imPeerId", paramString).apply();
  }
  
  public final void d(b paramb)
  {
    SharedPreferences.Editor localEditor = a.edit();
    long l = a;
    localEditor.putLong("lastImSendTime", l).apply();
  }
  
  public final void d(boolean paramBoolean)
  {
    a.edit().putBoolean("messagingVibration", paramBoolean).apply();
  }
  
  public final boolean d()
  {
    return a.getBoolean("hadSmsReadAccess", false);
  }
  
  public final boolean d(int paramInt)
  {
    String str;
    if (paramInt == 0)
    {
      str = "MmsAutoDownloadWhenRoaming";
    }
    else
    {
      int i = 1;
      if (i != paramInt) {
        break label32;
      }
      str = "SimTwoMmsAutoDownloadWhenRoaming";
    }
    return a.getBoolean(str, false);
    label32:
    return false;
  }
  
  public final void e()
  {
    a.edit().putBoolean("hasShownUndoTip", true).apply();
  }
  
  public final void e(int paramInt)
  {
    a.edit().putInt("spamSearchStatus", paramInt).apply();
  }
  
  public final void e(long paramLong)
  {
    a.edit().putLong("imMaxMediaSize", paramLong).apply();
  }
  
  public final void e(String paramString)
  {
    a.edit().putString("reactions_emoji", paramString).apply();
  }
  
  public final void e(boolean paramBoolean)
  {
    a.edit().putBoolean("showDefaultSmsScreen", paramBoolean).apply();
  }
  
  public final void f(int paramInt)
  {
    a.edit().putInt("pendingSpamProtectionOffNotificationsCount", paramInt).apply();
  }
  
  public final void f(long paramLong)
  {
    a.edit().putLong("imInitialSyncTimestamp", paramLong).apply();
  }
  
  public final void f(boolean paramBoolean)
  {
    a.edit().putBoolean("messagingSendGroupSms", paramBoolean).apply();
  }
  
  public final boolean f()
  {
    return a.getBoolean("hasShownUndoTip", false);
  }
  
  public final b g()
  {
    b localb = new org/a/a/b;
    long l = a.getLong("LastMessagePromotionDate", 0L);
    localb.<init>(l);
    return localb;
  }
  
  public final void g(int paramInt)
  {
    a.edit().putInt("pendingIncomingMsgNotificationsCount", paramInt).apply();
  }
  
  public final void g(long paramLong)
  {
    a.edit().putLong("spamTabVisitedTimestamp", paramLong).apply();
  }
  
  public final void g(boolean paramBoolean)
  {
    a.edit().putBoolean("imTracingEnabled", paramBoolean).apply();
  }
  
  public final b h()
  {
    b localb = new org/a/a/b;
    long l = a.getLong("JoinImUsersNotificationDate", 0L);
    localb.<init>(l);
    return localb;
  }
  
  public final void h(int paramInt)
  {
    a.edit().putInt("imForceUpgradeVersion", paramInt).apply();
  }
  
  public final void h(long paramLong)
  {
    a.edit().putLong("lastTimeAppUpdatePromo", paramLong).apply();
  }
  
  public final void h(boolean paramBoolean)
  {
    a.edit().putBoolean("isImPresenceReported", paramBoolean).apply();
  }
  
  public final void i(int paramInt)
  {
    a.edit().putInt("getUrlSpamScoreThreshold", paramInt).apply();
  }
  
  public final void i(boolean paramBoolean)
  {
    a.edit().putBoolean("appUpdatePromo", paramBoolean).apply();
  }
  
  public final boolean i()
  {
    return a.getBoolean("BlockedMessagesNotification", false);
  }
  
  public final void j(int paramInt)
  {
    a.edit().putInt("smsPermissionForBlockQuestionCount", paramInt).apply();
  }
  
  public final void j(boolean paramBoolean)
  {
    a.edit().putBoolean("historyMessagesInitialSyncCompleted", paramBoolean).apply();
  }
  
  public final boolean j()
  {
    return a.getBoolean("qaEnableAvailability", false);
  }
  
  public final void k(int paramInt)
  {
    a.edit().putInt("appUpdatePromoPeriod", paramInt).apply();
  }
  
  public final void k(boolean paramBoolean)
  {
    a.edit().putBoolean("hasDismissedReadReplyPromo", paramBoolean).apply();
  }
  
  public final boolean k()
  {
    return a.getBoolean("featureAvailability", false);
  }
  
  public final void l(int paramInt)
  {
    a.edit().putInt("appUpdateToVersion", paramInt).apply();
  }
  
  public final boolean l()
  {
    return a.getBoolean("availability_enabled", false);
  }
  
  public final void m(int paramInt)
  {
    a.edit().putInt("imNewJoinersPeriodDays", paramInt).apply();
  }
  
  public final boolean m()
  {
    return a.getBoolean("flash_enabled", false);
  }
  
  public final void n(int paramInt)
  {
    a.edit().putInt("imGroupMaxParticipantCount", paramInt).apply();
  }
  
  public final boolean n()
  {
    String str = "addressFieldBlinkedCount";
    SharedPreferences localSharedPreferences = a;
    long l1 = localSharedPreferences.getLong(str, 0L);
    long l2 = 3;
    boolean bool = l1 < l2;
    return !bool;
  }
  
  public final void o()
  {
    f("addressFieldBlinkedCount");
  }
  
  public final void o(int paramInt)
  {
    a.edit().putInt("imGroupBatchParticipantCount", paramInt).apply();
  }
  
  public final void p()
  {
    f("counterFacebookInvite");
  }
  
  public final void p(int paramInt)
  {
    a.edit().putInt("imVoiceClipMaxDurationMins", paramInt).apply();
  }
  
  public final boolean q()
  {
    return a.contains("messagingRingtone");
  }
  
  public final String r()
  {
    Object localObject = a;
    String str1 = "messagingRingtone";
    String str2 = "";
    localObject = ((SharedPreferences)localObject).getString(str1, str2);
    boolean bool = k.b((CharSequence)localObject);
    if (bool) {
      return null;
    }
    return (String)localObject;
  }
  
  public final boolean s()
  {
    return a.getBoolean("messagingVibration", false);
  }
  
  public final int t()
  {
    return a.getInt("spamSearchStatus", 0);
  }
  
  public final boolean u()
  {
    return a.getBoolean("showDefaultSmsScreen", false);
  }
  
  public final int v()
  {
    return a.getInt("pendingSpamProtectionOffNotificationsCount", 0);
  }
  
  public final int w()
  {
    return a.getInt("pendingIncomingMsgNotificationsCount", 0);
  }
  
  public final long x()
  {
    return a.getLong("latestSpamProtectionOffNotificationShowtime", 0L);
  }
  
  public final boolean y()
  {
    return a.getBoolean("messagingSendGroupSms", true);
  }
  
  public final boolean z()
  {
    return a.contains("messagingSendGroupSms");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */