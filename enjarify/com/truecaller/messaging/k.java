package com.truecaller.messaging;

import com.truecaller.utils.d;

public final class k
  implements j
{
  private final d a;
  private final h b;
  
  public k(d paramd, h paramh)
  {
    a = paramd;
    b = paramh;
  }
  
  public final boolean a()
  {
    Object localObject = a;
    boolean bool = ((d)localObject).a();
    if (!bool)
    {
      localObject = b;
      int i = ((h)localObject).P();
      h localh = b;
      int j = (i + 1) % 5;
      localh.j(j);
      return i == 0;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */