package com.truecaller.messaging;

import android.content.Context;
import android.content.res.Resources;
import c.a.ag;
import c.n;
import c.t;
import com.truecaller.utils.o;
import java.util.Map;

public class g
  implements f
{
  protected final Map a;
  protected final o b;
  protected final Context c;
  private final int d;
  private final int e;
  
  public g(o paramo, Context paramContext)
  {
    b = paramo;
    c = paramContext;
    paramo = new n[3];
    Object localObject1 = Integer.valueOf(0);
    Object localObject2 = new com/truecaller/messaging/g$a$d;
    ((g.a.d)localObject2).<init>();
    localObject1 = t.a(localObject1, localObject2);
    paramo[0] = localObject1;
    int i = 1;
    localObject1 = Integer.valueOf(i);
    localObject2 = new com/truecaller/messaging/g$a$c;
    ((g.a.c)localObject2).<init>();
    localObject1 = t.a(localObject1, localObject2);
    paramo[i] = localObject1;
    i = 2;
    localObject1 = Integer.valueOf(i);
    localObject2 = new com/truecaller/messaging/g$a$b;
    ((g.a.b)localObject2).<init>();
    localObject1 = t.a(localObject1, localObject2);
    paramo[i] = localObject1;
    paramo = ag.a(paramo);
    a = paramo;
    int j = b.e(2130968855);
    d = j;
    j = b.e(2130968856);
    e = j;
  }
  
  public final int a()
  {
    return d;
  }
  
  public final int a(int paramInt)
  {
    Map localMap = a;
    Object localObject = Integer.valueOf(paramInt);
    localObject = (g.a)localMap.get(localObject);
    if (localObject != null) {
      return ((g.a)localObject).e();
    }
    localObject = g.a.a.c;
    return g.a.a.b;
  }
  
  public final int b()
  {
    return e;
  }
  
  public final int b(int paramInt)
  {
    Resources localResources = c.getResources();
    Map localMap = a;
    Object localObject = Integer.valueOf(paramInt);
    localObject = (g.a)localMap.get(localObject);
    if (localObject != null)
    {
      paramInt = ((g.a)localObject).d();
    }
    else
    {
      localObject = g.a.a.c;
      paramInt = g.a.a.a;
    }
    return localResources.getColor(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */