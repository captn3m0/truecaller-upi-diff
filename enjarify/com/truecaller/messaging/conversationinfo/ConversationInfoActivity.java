package com.truecaller.messaging.conversationinfo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.AppCompatActivity;
import com.truecaller.log.AssertionUtil;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;

public class ConversationInfoActivity
  extends AppCompatActivity
{
  public void onCreate(Bundle paramBundle)
  {
    int i = aresId;
    setTheme(i);
    super.onCreate(paramBundle);
    paramBundle = getIntent();
    long l1 = -1;
    long l2 = paramBundle.getLongExtra("conversation_id", l1);
    String str = "filter";
    int j = paramBundle.getIntExtra(str, 0);
    boolean bool = l2 < l1;
    if (!bool)
    {
      bool = true;
    }
    else
    {
      bool = false;
      str = null;
    }
    String[] arrayOfString = new String[0];
    AssertionUtil.isFalse(bool, arrayOfString);
    paramBundle = f.a(l2, j);
    getSupportFragmentManager().a().b(16908290, paramBundle).c();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversationinfo.ConversationInfoActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */