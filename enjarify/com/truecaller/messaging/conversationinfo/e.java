package com.truecaller.messaging.conversationinfo;

import android.net.Uri;
import android.view.View;
import android.widget.TextView;
import com.truecaller.search.local.model.c.a;
import com.truecaller.ui.view.AvailabilityView;
import com.truecaller.ui.view.ContactPhoto;

final class e
  extends g
  implements h.b
{
  private final ContactPhoto a;
  private final TextView c;
  private final TextView d;
  private final AvailabilityView e;
  private final TextView f;
  private boolean g;
  
  e(View paramView)
  {
    super(paramView);
    Object localObject = (ContactPhoto)paramView.findViewById(2131362554);
    a = ((ContactPhoto)localObject);
    localObject = (TextView)paramView.findViewById(2131363718);
    c = ((TextView)localObject);
    localObject = (TextView)paramView.findViewById(2131364281);
    d = ((TextView)localObject);
    localObject = (AvailabilityView)paramView.findViewById(2131362066);
    e = ((AvailabilityView)localObject);
    paramView = (TextView)paramView.findViewById(2131364563);
    f = paramView;
  }
  
  public final String a()
  {
    return null;
  }
  
  public final void a(int paramInt)
  {
    a.setContactBadgeDrawable(paramInt);
  }
  
  public final void a(Uri paramUri)
  {
    a.a(paramUri, null);
  }
  
  public final void a(c.a parama)
  {
    e.a(parama);
  }
  
  public final void b(String paramString)
  {
    a.setContentDescription(paramString);
    c.setText(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    a.setIsSpam(paramBoolean);
  }
  
  public final boolean b()
  {
    return g;
  }
  
  public final void c(String paramString)
  {
    d.setText(paramString);
  }
  
  public final void c(boolean paramBoolean)
  {
    TextView localTextView = f;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localTextView.setVisibility(paramBoolean);
  }
  
  public final void c_(String paramString) {}
  
  public final void c_(boolean paramBoolean)
  {
    g = paramBoolean;
  }
  
  public final void d(String paramString)
  {
    f.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversationinfo.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */