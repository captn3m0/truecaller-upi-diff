package com.truecaller.messaging.conversationinfo;

import android.view.View;
import android.widget.TextView;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.util.at;

final class b
  extends g
  implements h.a
{
  private final TextView a;
  private final TintedImageView c;
  private boolean d;
  private View e;
  
  b(View paramView)
  {
    super(paramView);
    Object localObject = (TextView)paramView.findViewById(2131364681);
    a = ((TextView)localObject);
    localObject = (TintedImageView)paramView.findViewById(2131363301);
    c = ((TintedImageView)localObject);
    paramView = paramView.findViewById(2131362867);
    e = paramView;
  }
  
  public final String a()
  {
    return null;
  }
  
  public final void a(int paramInt)
  {
    a.setText(paramInt);
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    c.setImageResource(paramInt1);
    TintedImageView localTintedImageView = c;
    paramInt2 = com.truecaller.utils.ui.b.a(localTintedImageView.getContext(), paramInt2);
    localTintedImageView.setTint(paramInt2);
  }
  
  public final void b(boolean paramBoolean)
  {
    at.a(e, paramBoolean);
  }
  
  public final boolean b()
  {
    return d;
  }
  
  public final void c_(String paramString) {}
  
  public final void c_(boolean paramBoolean)
  {
    d = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversationinfo.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */