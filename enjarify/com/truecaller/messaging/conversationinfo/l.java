package com.truecaller.messaging.conversationinfo;

import android.text.TextUtils;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.common.h.am;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.featuretoggles.e;
import com.truecaller.filters.p;
import com.truecaller.filters.s;
import com.truecaller.messaging.data.o;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.search.local.model.c;
import com.truecaller.util.aa;
import com.truecaller.util.al;
import com.truecaller.utils.n;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

final class l
  extends k
{
  private final long a;
  private final i c;
  private final al d;
  private final p e;
  private final n f;
  private final f g;
  private final f h;
  private final f i;
  private final c j;
  private final com.truecaller.analytics.b k;
  private final f l;
  private final f m;
  private final List n;
  private final com.truecaller.utils.l o;
  private final e p;
  private final int q;
  private Conversation r;
  private Participant[] s;
  private com.truecaller.androidactors.a t;
  private String u;
  
  l(i parami, long paramLong, c paramc, al paramal, p paramp, n paramn, f paramf1, f paramf2, f paramf3, com.truecaller.analytics.b paramb, f paramf4, f paramf5, com.truecaller.utils.l paraml, e parame, int paramInt)
  {
    a = paramLong;
    Object localObject = parami;
    c = parami;
    localObject = paramal;
    d = paramal;
    localObject = paramp;
    e = paramp;
    localObject = paramn;
    f = paramn;
    localObject = paramf1;
    g = paramf1;
    localObject = paramf2;
    h = paramf2;
    localObject = paramf3;
    i = paramf3;
    localObject = paramc;
    j = paramc;
    localObject = paramb;
    k = paramb;
    localObject = paramf4;
    l = paramf4;
    localObject = paramf5;
    m = paramf5;
    localObject = paraml;
    o = paraml;
    localObject = parame;
    p = parame;
    q = paramInt;
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    n = ((List)localObject);
    localObject = new Participant[0];
    s = ((Participant[])localObject);
  }
  
  private int e(int paramInt)
  {
    Participant[] arrayOfParticipant = s;
    int i1 = arrayOfParticipant.length;
    if (paramInt < i1) {
      return paramInt;
    }
    int i2 = arrayOfParticipant.length;
    return paramInt - i2;
  }
  
  private void g()
  {
    Object localObject = (o)i.a();
    long l1 = a;
    localObject = ((o)localObject).a(l1);
    i locali = c;
    -..Lambda.VHwN_lnLwMkrSeom62Zjn5zXvW4 localVHwN_lnLwMkrSeom62Zjn5zXvW4 = new com/truecaller/messaging/conversationinfo/-$$Lambda$VHwN_lnLwMkrSeom62Zjn5zXvW4;
    localVHwN_lnLwMkrSeom62Zjn5zXvW4.<init>(this);
    localObject = ((w)localObject).a(locali, localVHwN_lnLwMkrSeom62Zjn5zXvW4);
    t = ((com.truecaller.androidactors.a)localObject);
  }
  
  private List h()
  {
    ArrayList localArrayList1 = new java/util/ArrayList;
    localArrayList1.<init>();
    ArrayList localArrayList2 = new java/util/ArrayList;
    localArrayList2.<init>();
    ArrayList localArrayList3 = new java/util/ArrayList;
    localArrayList3.<init>();
    Object localObject = s;
    int i1 = localObject.length;
    int i2 = 0;
    while (i2 < i1)
    {
      String str1 = localObject[i2];
      String str2 = f;
      localArrayList1.add(str2);
      int i3 = c;
      if (i3 == 0) {
        str2 = "PHONE_NUMBER";
      } else {
        str2 = "OTHER";
      }
      localArrayList2.add(str2);
      str1 = str1.a();
      localArrayList3.add(str1);
      i2 += 1;
    }
    localObject = new List[3];
    localObject[0] = localArrayList1;
    localObject[1] = localArrayList2;
    localObject[2] = localArrayList3;
    return Arrays.asList((Object[])localObject);
  }
  
  private boolean i()
  {
    com.truecaller.utils.l locall = o;
    boolean bool = locall.b();
    if (bool)
    {
      locall = o;
      bool = locall.c();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  private void j()
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    localObject = (m)b;
    String str = "android.permission.READ_EXTERNAL_STORAGE";
    boolean bool = ((m)localObject).a(str);
    if (!bool)
    {
      localObject = (m)b;
      str = "android.permission.WRITE_EXTERNAL_STORAGE";
      bool = ((m)localObject).a(str);
      if (!bool)
      {
        ((m)b).e();
        return;
      }
    }
    ((m)b).d();
  }
  
  public final int a()
  {
    int i1 = s.length;
    int i2 = n.size();
    return i1 + i2;
  }
  
  public final int a(int paramInt)
  {
    Participant[] arrayOfParticipant = s;
    int i1 = arrayOfParticipant.length;
    if (paramInt < i1) {
      return 1;
    }
    return 2;
  }
  
  public final void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    if (paramInt == 0)
    {
      com.truecaller.utils.l locall = o;
      String[] arrayOfString = { "android.permission.WRITE_EXTERNAL_STORAGE" };
      paramInt = locall.a(paramArrayOfString, paramArrayOfInt, arrayOfString);
      if (paramInt != 0)
      {
        paramInt = 1;
        a(paramInt);
      }
    }
  }
  
  final void a(Conversation paramConversation)
  {
    Object localObject1 = t;
    int i1;
    if (localObject1 != null)
    {
      ((com.truecaller.androidactors.a)localObject1).a();
      i1 = 0;
      localObject1 = null;
      t = null;
    }
    r = paramConversation;
    n.clear();
    paramConversation = r;
    if (paramConversation != null)
    {
      paramConversation = l;
      s = paramConversation;
      paramConversation = n;
      localObject1 = a.a;
      paramConversation.add(localObject1);
      paramConversation = e;
      boolean bool1 = paramConversation.g();
      i1 = 1;
      if (bool1)
      {
        paramConversation = p.w();
        bool1 = paramConversation.a();
        if (!bool1)
        {
          bool1 = true;
          break label126;
        }
      }
      bool1 = false;
      paramConversation = null;
      label126:
      Object localObject2 = s;
      int i2 = localObject2.length;
      if (i2 == i1)
      {
        localObject2 = d;
        boolean bool2 = ((al)localObject2).a();
        if (bool2)
        {
          localObject2 = r;
          bool2 = ((Conversation)localObject2).c();
          if (bool2)
          {
            localObject2 = r;
            bool2 = ((Conversation)localObject2).a(bool1);
            if (!bool2)
            {
              localObject2 = n;
              a locala = a.d;
              ((List)localObject2).add(locala);
            }
          }
          localObject2 = r;
          bool1 = ((Conversation)localObject2).a(bool1);
          if (bool1)
          {
            paramConversation = n;
            localObject2 = a.c;
            paramConversation.add(localObject2);
          }
          else
          {
            paramConversation = n;
            localObject2 = a.b;
            paramConversation.add(localObject2);
          }
        }
      }
      paramConversation = b;
      if (paramConversation != null)
      {
        paramConversation = (m)b;
        localObject2 = s;
        int i3 = localObject2.length;
        if (i3 == i1) {
          i1 = 2131886427;
        } else {
          i1 = 2131886426;
        }
        paramConversation.a(i1);
        paramConversation = (m)b;
        paramConversation.f();
      }
    }
  }
  
  public final void a(String paramString, TruecallerContract.Filters.EntityType paramEntityType)
  {
    Object localObject1 = s;
    int i1 = localObject1.length;
    if (i1 != 0)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        boolean bool = am.b(paramString);
        m localm = null;
        if (!bool)
        {
          localObject1 = (aa)m.a();
          long l1 = s[0].i;
          localObject1 = ((aa)localObject1).a(l1);
          localObject2 = c;
          localObject3 = new com/truecaller/messaging/conversationinfo/-$$Lambda$l$jMGeQu1g6GhIxUUE5VNh0UrFqvk;
          ((-..Lambda.l.jMGeQu1g6GhIxUUE5VNh0UrFqvk)localObject3).<init>(this, paramString, paramEntityType);
          ((w)localObject1).a((i)localObject2, (ac)localObject3);
        }
        localObject1 = h();
        Object localObject2 = g.a();
        Object localObject3 = localObject2;
        localObject3 = (s)localObject2;
        localObject2 = ((List)localObject1).get(0);
        Object localObject4 = localObject2;
        localObject4 = (List)localObject2;
        int i3 = 1;
        List localList1 = (List)((List)localObject1).get(i3);
        int i4 = 2;
        List localList2;
        if (paramString == null)
        {
          localList2 = (List)((List)localObject1).get(i4);
        }
        else
        {
          i4 = ((List)((List)localObject1).get(i4)).size();
          localList2 = Collections.nCopies(i4, paramString);
        }
        String str = "conversationInfo";
        ((s)localObject3).a((List)localObject4, localList1, localList2, str, false, paramEntityType).c();
        paramEntityType = b;
        if (paramEntityType != null)
        {
          if (paramString == null) {
            paramString = s[0].a();
          }
          paramEntityType = s[0].b();
          localObject3 = f;
          int i5 = 2131755028;
          localList1 = (List)((List)localObject1).get(0);
          int i6 = localList1.size();
          localObject2 = new Object[i3];
          int i2 = ((List)((List)localObject1).get(0)).size();
          localObject1 = Integer.valueOf(i2);
          localObject2[0] = localObject1;
          localObject1 = ((n)localObject3).a(i5, i6, (Object[])localObject2);
          localm = (m)b;
          localm.a(paramString, paramEntityType, (String)localObject1);
        }
        return;
      }
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      boolean bool = i();
      if (!bool)
      {
        j();
        return;
      }
    }
    Object localObject1 = h.a();
    Object localObject2 = localObject1;
    localObject2 = (t)localObject1;
    long l1 = r.a;
    int i1 = r.p;
    localObject1 = r;
    int i2 = q;
    ((t)localObject2).a(l1, i1, i2, paramBoolean).c();
    Object localObject3 = b;
    if (localObject3 != null)
    {
      localObject3 = (m)b;
      ((m)localObject3).c();
    }
  }
  
  public final long b(int paramInt)
  {
    return 0L;
  }
  
  public final void b()
  {
    j.b();
  }
  
  public final void b(boolean paramBoolean)
  {
    Object localObject = b;
    if ((localObject != null) && (!paramBoolean))
    {
      m localm = (m)b;
      boolean bool = true;
      long l1 = a;
      localm.a(bool, l1);
      localm = (m)b;
      localm.b();
    }
  }
  
  public final void c()
  {
    j.c();
  }
  
  public final void c(int paramInt)
  {
    int i1 = e(paramInt);
    Object localObject1 = b;
    if (localObject1 != null)
    {
      paramInt = a(paramInt);
      Object localObject2;
      Object localObject3;
      Object localObject4;
      switch (paramInt)
      {
      default: 
        break;
      case 2: 
        localObject2 = l.1.a;
        localObject1 = n;
        localObject3 = (a)((List)localObject1).get(i1);
        i1 = ((a)localObject3).ordinal();
        paramInt = localObject2[i1];
        switch (paramInt)
        {
        default: 
          break;
        case 4: 
          u = "unblock";
          localObject2 = (m)b;
          ((m)localObject2).a();
          break;
        case 3: 
          u = "notspam";
          ((m)b).a();
          return;
        case 2: 
          localObject2 = (m)b;
          localObject3 = s;
          boolean bool2 = false;
          localObject1 = null;
          localObject3 = localObject3[0].a();
          localObject4 = s[0];
          boolean bool3 = ((Participant)localObject4).i();
          if (bool3)
          {
            localObject4 = d;
            bool3 = ((al)localObject4).a();
            if (bool3) {
              bool2 = true;
            }
          }
          ((m)localObject2).a((String)localObject3, bool2);
          return;
        case 1: 
          localObject2 = (o)i.a();
          localObject3 = Collections.singletonList(Long.valueOf(r.a));
          localObject2 = ((o)localObject2).a((List)localObject3);
          localObject3 = c;
          localObject1 = new com/truecaller/messaging/conversationinfo/-$$Lambda$l$5aU0h0TXTMMvIuqsqMNgwz09kbw;
          ((-..Lambda.l.5aU0h0TXTMMvIuqsqMNgwz09kbw)localObject1).<init>(this);
          ((w)localObject2).a((i)localObject3, (ac)localObject1);
          return;
        }
        break;
      case 1: 
        localObject2 = s[i1];
        boolean bool1 = ((Participant)localObject2).h();
        if (bool1)
        {
          localObject3 = (m)b;
          localObject1 = f;
          localObject4 = e;
          String str = m;
          localObject2 = h;
          ((m)localObject3).a((String)localObject1, (String)localObject4, str, (String)localObject2);
          return;
        }
        break;
      }
    }
  }
  
  public final boolean d(int paramInt)
  {
    Object localObject1 = s;
    int i1 = localObject1.length;
    if (paramInt < i1)
    {
      Object localObject2 = f;
      boolean bool = TextUtils.isEmpty((CharSequence)localObject2);
      if (!bool)
      {
        localObject1 = b;
        if (localObject1 != null)
        {
          localObject1 = (m)b;
          ((m)localObject1).b((String)localObject2);
          localObject2 = (m)b;
          ((m)localObject2).g();
        }
      }
    }
    return true;
  }
  
  public final void e()
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (m)b;
      ((m)localObject).b();
    }
  }
  
  public final void f()
  {
    Object localObject1 = s;
    int i1 = localObject1.length;
    if (i1 != 0)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        localObject1 = h();
        Object localObject2 = g.a();
        Object localObject3 = localObject2;
        localObject3 = (s)localObject2;
        localObject2 = ((List)localObject1).get(0);
        Object localObject4 = localObject2;
        localObject4 = (List)localObject2;
        localObject2 = ((List)localObject1).get(1);
        Object localObject5 = localObject2;
        localObject5 = (List)localObject2;
        localObject1 = ((List)localObject1).get(2);
        Object localObject6 = localObject1;
        localObject6 = (List)localObject1;
        String str = u;
        localObject1 = ((s)localObject3).a((List)localObject4, (List)localObject5, (List)localObject6, str, "conversationInfo", false);
        localObject2 = c;
        localObject3 = new com/truecaller/messaging/conversationinfo/-$$Lambda$l$PWWubgNIY6j2mjpAYS1dGA_i9kU;
        ((-..Lambda.l.PWWubgNIY6j2mjpAYS1dGA_i9kU)localObject3).<init>(this);
        ((w)localObject1).a((i)localObject2, (ac)localObject3);
        return;
      }
    }
  }
  
  public final void y_()
  {
    super.y_();
    com.truecaller.androidactors.a locala = t;
    if (locala != null)
    {
      locala.a();
      locala = null;
      t = null;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversationinfo.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */