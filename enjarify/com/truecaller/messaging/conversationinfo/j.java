package com.truecaller.messaging.conversationinfo;

import com.truecaller.analytics.b;
import com.truecaller.androidactors.f;
import com.truecaller.featuretoggles.e;
import com.truecaller.filters.p;
import com.truecaller.search.local.model.c;
import com.truecaller.util.al;
import com.truecaller.utils.l;
import com.truecaller.utils.n;
import dagger.a.d;
import dagger.a.g;
import javax.inject.Provider;

public final class j
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  private final Provider g;
  private final Provider h;
  private final Provider i;
  private final Provider j;
  private final Provider k;
  private final Provider l;
  private final Provider m;
  private final Provider n;
  private final Provider o;
  
  public static k a(com.truecaller.androidactors.k paramk, long paramLong, c paramc, al paramal, p paramp, n paramn, f paramf1, f paramf2, f paramf3, b paramb, f paramf4, f paramf5, l paraml, e parame, int paramInt)
  {
    return (k)g.a(i.a(paramk, paramLong, paramc, paramal, paramp, paramn, paramf1, paramf2, paramf3, paramb, paramf4, paramf5, paraml, parame, paramInt), "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversationinfo.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */