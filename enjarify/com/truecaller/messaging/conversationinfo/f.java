package com.truecaller.messaging.conversationinfo;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Toast;
import com.truecaller.bk;
import com.truecaller.bl;
import com.truecaller.bp;
import com.truecaller.log.AssertionUtil;
import com.truecaller.ui.components.d.a;
import com.truecaller.ui.components.d.b;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.ui.details.a;
import com.truecaller.ui.dialogs.o;
import com.truecaller.ui.q;
import com.truecaller.util.r;
import dagger.a.g;

public final class f
  extends Fragment
  implements m
{
  k a;
  private c b;
  
  static f a(long paramLong, int paramInt)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putLong("conversation_id", paramLong);
    localBundle.putInt("filter", paramInt);
    f localf = new com/truecaller/messaging/conversationinfo/f;
    localf.<init>();
    localf.setArguments(localBundle);
    return localf;
  }
  
  public final void a()
  {
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject = getContext();
    localBuilder.<init>((Context)localObject);
    localBuilder = localBuilder.setMessage(2131886194);
    localObject = new com/truecaller/messaging/conversationinfo/-$$Lambda$f$Txs__uP9Qlv2iWcn-dDRO7ecdjw;
    ((-..Lambda.f.Txs__uP9Qlv2iWcn-dDRO7ecdjw)localObject).<init>(this);
    localBuilder = localBuilder.setPositiveButton(2131887235, (DialogInterface.OnClickListener)localObject);
    localObject = new com/truecaller/messaging/conversationinfo/-$$Lambda$f$JnpjLf-Kc_DM0ew2gtOC6rCL1XI;
    ((-..Lambda.f.JnpjLf-Kc_DM0ew2gtOC6rCL1XI)localObject).<init>(this);
    localBuilder.setNegativeButton(2131887214, (DialogInterface.OnClickListener)localObject).show();
  }
  
  public final void a(int paramInt)
  {
    Object localObject = (AppCompatActivity)getActivity();
    if (localObject != null) {
      localObject = ((AppCompatActivity)localObject).getSupportActionBar();
    } else {
      localObject = null;
    }
    if (localObject != null) {
      ((ActionBar)localObject).setTitle(paramInt);
    }
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    o localo = new com/truecaller/ui/dialogs/o;
    Context localContext = getContext();
    localo.<init>(localContext, paramString1, paramString2, paramString3);
    paramString1 = new com/truecaller/messaging/conversationinfo/-$$Lambda$f$x-pySRCSTvMoS6gaTTOp4PQU48k;
    paramString1.<init>(this, localo);
    localo.setOnDismissListener(paramString1);
    localo.show();
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    Context localContext = getContext();
    DetailsFragment.SourceType localSourceType = DetailsFragment.SourceType.Conversation;
    DetailsFragment.a(localContext, paramString4, paramString3, paramString1, paramString2, null, localSourceType, true, 20);
  }
  
  public final void a(String paramString, boolean paramBoolean)
  {
    a locala = new com/truecaller/ui/details/a;
    Context localContext = getContext();
    locala.<init>(localContext, paramString, paramBoolean, true);
    paramString = new com/truecaller/messaging/conversationinfo/-$$Lambda$f$DPqZze0496_WHwDplKgcbihL__8;
    paramString.<init>(this);
    a = paramString;
    paramString = new com/truecaller/messaging/conversationinfo/-$$Lambda$f$E8SOAOTvD2h6dnyp4eSrL0FyCus;
    paramString.<init>(this);
    b = paramString;
    locala.show();
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = 1;
    boolean[] arrayOfBoolean = new boolean[i];
    int j = 0;
    CheckBox localCheckBox = null;
    arrayOfBoolean[0] = i;
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject1 = getContext();
    localBuilder.<init>((Context)localObject1);
    int k = 2131886492;
    localBuilder = localBuilder.setMessage(k);
    localObject1 = new com/truecaller/messaging/conversationinfo/-$$Lambda$f$iJykjMb50DpaEf0MSkizubs9tFE;
    ((-..Lambda.f.iJykjMb50DpaEf0MSkizubs9tFE)localObject1).<init>(this, arrayOfBoolean);
    localBuilder = localBuilder.setPositiveButton(2131887540, (DialogInterface.OnClickListener)localObject1);
    localObject1 = new com/truecaller/messaging/conversationinfo/-$$Lambda$f$h7C2KRDOxtDmI92ZV3nCKgila2k;
    ((-..Lambda.f.h7C2KRDOxtDmI92ZV3nCKgila2k)localObject1).<init>(this);
    int m = 2131887197;
    localBuilder = localBuilder.setNegativeButton(m, (DialogInterface.OnClickListener)localObject1);
    if (paramBoolean)
    {
      Object localObject2 = LayoutInflater.from(getContext());
      k = 2131558849;
      m = 0;
      localObject2 = ((LayoutInflater)localObject2).inflate(k, null, false);
      j = 2131362466;
      localCheckBox = (CheckBox)((View)localObject2).findViewById(j);
      localObject1 = new com/truecaller/messaging/conversationinfo/-$$Lambda$f$Hhrb3G6TBTVULfag77yr8445uic;
      ((-..Lambda.f.Hhrb3G6TBTVULfag77yr8445uic)localObject1).<init>(arrayOfBoolean);
      localCheckBox.setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject1);
      localBuilder.setView((View)localObject2);
    }
    localBuilder.show();
  }
  
  public final void a(boolean paramBoolean, long paramLong)
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      int i = -1;
      Object localObject = new android/content/Intent;
      ((Intent)localObject).<init>();
      String str = "RESULT_NUMBER_BLOCKED";
      Intent localIntent = ((Intent)localObject).putExtra(str, paramBoolean);
      localObject = "CONVERSATION_ID";
      localIntent = localIntent.putExtra((String)localObject, paramLong);
      localf.setResult(i, localIntent);
    }
  }
  
  public final boolean a(String paramString)
  {
    return com.truecaller.wizard.utils.i.a(getActivity(), paramString);
  }
  
  public final void b()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null) {
      localf.finish();
    }
  }
  
  public final void b(String paramString)
  {
    r.a(getActivity(), paramString, null);
  }
  
  public final void c()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      int i = 2131362603;
      localf.setResult(i);
      localf.finish();
    }
  }
  
  public final void d()
  {
    bl localbl = new com/truecaller/bl;
    Context localContext = getContext();
    localbl.<init>(localContext, 2131886806, 2131886805, 2131234158);
    localbl.show();
  }
  
  public final void e()
  {
    String[] arrayOfString = { "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE" };
    com.truecaller.wizard.utils.i.b(this, arrayOfString, 0);
  }
  
  public final void f()
  {
    b.notifyDataSetChanged();
  }
  
  public final void g()
  {
    Toast.makeText(getContext(), 2131887203, 0).show();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getArguments();
    long l1 = -1;
    long l2 = paramBundle.getLong("conversation_id", l1);
    paramBundle = getArguments();
    Object localObject1 = "filter";
    int i = paramBundle.getInt((String)localObject1, 0);
    boolean bool = l2 < l1;
    if (!bool)
    {
      bool = true;
    }
    else
    {
      bool = false;
      localObject1 = null;
    }
    Object localObject2 = new String[0];
    AssertionUtil.isFalse(bool, (String[])localObject2);
    localObject1 = new com/truecaller/messaging/conversationinfo/n$a;
    ((n.a)localObject1).<init>((byte)0);
    localObject2 = (bp)g.a(((bk)getContext().getApplicationContext()).a());
    b = ((bp)localObject2);
    localObject2 = new com/truecaller/messaging/conversationinfo/i;
    ((i)localObject2).<init>(l2, i);
    paramBundle = (i)g.a(localObject2);
    a = paramBundle;
    g.a(a, i.class);
    g.a(b, bp.class);
    paramBundle = new com/truecaller/messaging/conversationinfo/n;
    localObject2 = a;
    localObject1 = b;
    paramBundle.<init>((i)localObject2, (bp)localObject1, (byte)0);
    paramBundle.a(this);
    paramBundle = new com/truecaller/messaging/conversationinfo/c;
    localObject1 = a;
    paramBundle.<init>((k)localObject1);
    b = paramBundle;
    paramBundle = b;
    localObject1 = new com/truecaller/messaging/conversationinfo/-$$Lambda$f$u0YE3D5A88tZGqEy4A03elOgKUY;
    ((-..Lambda.f.u0YE3D5A88tZGqEy4A03elOgKUY)localObject1).<init>(this);
    a = ((d.a)localObject1);
    localObject1 = new com/truecaller/messaging/conversationinfo/-$$Lambda$f$PGqm7E3iqOCzWLHtJkpBqNPZDG0;
    ((-..Lambda.f.PGqm7E3iqOCzWLHtJkpBqNPZDG0)localObject1).<init>(this);
    b = ((d.b)localObject1);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    setHasOptionsMenu(true);
    return paramLayoutInflater.inflate(2131558679, paramViewGroup, false);
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    int i = paramMenuItem.getItemId();
    int j = 16908332;
    if (i != j) {
      return super.onOptionsItemSelected(paramMenuItem);
    }
    a.e();
    return true;
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    com.truecaller.wizard.utils.i.a(paramArrayOfString, paramArrayOfInt);
    a.a(paramInt, paramArrayOfString, paramArrayOfInt);
  }
  
  public final void onStart()
  {
    super.onStart();
    a.b();
  }
  
  public final void onStop()
  {
    a.c();
    super.onStop();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    paramBundle = (Toolbar)paramView.findViewById(2131364907);
    Object localObject = (AppCompatActivity)getActivity();
    ((AppCompatActivity)localObject).setSupportActionBar(paramBundle);
    paramBundle = ((AppCompatActivity)localObject).getSupportActionBar();
    int i = 1;
    paramBundle.setDisplayHomeAsUpEnabled(i);
    paramView = (RecyclerView)paramView.findViewById(2131362602);
    paramBundle = new android/support/v7/widget/LinearLayoutManager;
    Context localContext = getContext();
    paramBundle.<init>(localContext, i, false);
    localObject = new com/truecaller/ui/q;
    localContext = getContext();
    ((q)localObject).<init>(localContext, 2131559205, 0);
    paramView.addItemDecoration((RecyclerView.ItemDecoration)localObject);
    paramView.setLayoutManager(paramBundle);
    paramBundle = b;
    paramView.setAdapter(paramBundle);
    a.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversationinfo.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */