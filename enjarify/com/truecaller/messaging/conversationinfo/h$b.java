package com.truecaller.messaging.conversationinfo;

import android.net.Uri;
import com.truecaller.search.local.model.c.a;
import com.truecaller.ui.q.a;

public abstract interface h$b
  extends h, q.a
{
  public abstract void a(int paramInt);
  
  public abstract void a(Uri paramUri);
  
  public abstract void a(c.a parama);
  
  public abstract void b(String paramString);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c(String paramString);
  
  public abstract void c(boolean paramBoolean);
  
  public abstract void d(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversationinfo.h.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */