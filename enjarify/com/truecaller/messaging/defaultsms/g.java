package com.truecaller.messaging.defaultsms;

import com.truecaller.analytics.ae;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.common.h.am;
import com.truecaller.log.AssertionUtil;
import com.truecaller.tracking.events.aa;
import com.truecaller.tracking.events.aa.a;
import com.truecaller.util.al;
import com.truecaller.utils.l;
import org.apache.a.a;

class g
  extends e
{
  protected final com.truecaller.messaging.h a;
  private final al c;
  private final com.truecaller.utils.d d;
  private final f e;
  private final com.truecaller.analytics.b f;
  private final String g;
  private final l h;
  
  g(al paramal, com.truecaller.utils.d paramd, String paramString, f paramf, com.truecaller.analytics.b paramb, com.truecaller.messaging.h paramh, l paraml)
  {
    c = paramal;
    d = paramd;
    g = paramString;
    e = paramf;
    f = paramb;
    a = paramh;
    h = paraml;
  }
  
  private void a(String paramString)
  {
    Object localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>("PermissionChanged");
    paramString = ((e.a)localObject).a("Permission", "SMSApp").a("State", paramString);
    String str = g;
    paramString = paramString.a("Context", str);
    localObject = f;
    paramString = paramString.a();
    ((com.truecaller.analytics.b)localObject).a(paramString);
  }
  
  public void a() {}
  
  public final void a(int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 != null)
    {
      int i = 1;
      if (paramInt == i)
      {
        Object localObject2 = d;
        paramInt = ((com.truecaller.utils.d)localObject2).d();
        if (paramInt != 0)
        {
          localObject2 = am.n(d.k());
          try
          {
            Object localObject3 = e;
            localObject3 = ((f)localObject3).a();
            localObject3 = (ae)localObject3;
            Object localObject4 = g;
            aa.a locala1 = aa.b();
            Object localObject5 = "defaultMessagingApp";
            localObject5 = locala1.a((CharSequence)localObject5);
            localObject5 = ((aa.a)localObject5).b((CharSequence)localObject2);
            ((aa.a)localObject5).c((CharSequence)localObject4);
            localObject4 = locala1.a();
            ((ae)localObject3).a((org.apache.a.d.d)localObject4);
          }
          catch (a locala)
          {
            AssertionUtil.reportThrowableButNeverCrash(locala);
          }
          a((String)localObject2);
          localObject2 = a;
          org.a.a.b localb = new org/a/a/b;
          long l1 = 0L;
          localb.<init>(l1);
          ((com.truecaller.messaging.h)localObject2).a(localb);
          a.c(l1);
          localObject2 = a;
          localb = org.a.a.b.ay_();
          long l2 = a;
          ((com.truecaller.messaging.h)localObject2).b(l2);
          a.e(i);
          c.j();
          localObject2 = h;
          localObject1 = new String[] { "android.permission.SEND_SMS" };
          paramInt = ((l)localObject2).a((String[])localObject1);
          if (paramInt != 0)
          {
            c();
            return;
          }
          ((h)b).a("android.permission.SEND_SMS");
          return;
        }
        localObject2 = (h)b;
        ((h)localObject2).b();
      }
    }
  }
  
  public void a(h paramh)
  {
    super.a(paramh);
    Object localObject = d;
    boolean bool = ((com.truecaller.utils.d)localObject).d();
    if (bool)
    {
      localObject = h;
      String[] arrayOfString = { "android.permission.SEND_SMS" };
      bool = ((l)localObject).a(arrayOfString);
      if (bool)
      {
        paramh.a();
        return;
      }
      paramh.a("android.permission.SEND_SMS");
      return;
    }
    paramh.c();
    a("Asked");
  }
  
  final void a(String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    int i = 0;
    localObject = null;
    int j = 0;
    for (;;)
    {
      int k = paramArrayOfString.length;
      if (j >= k) {
        break;
      }
      String str1 = "android.permission.SEND_SMS";
      String str2 = paramArrayOfString[j];
      boolean bool = str1.equals(str2);
      if (bool)
      {
        int m = paramArrayOfInt[j];
        if (m != 0) {
          break;
        }
        m = 1;
        i = 1;
        break;
      }
      j += 1;
    }
    if (i != 0)
    {
      c();
      return;
    }
    ((h)b).b();
  }
  
  public void b() {}
  
  protected void c()
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (h)b;
      ((h)localObject).a();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.defaultsms.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */