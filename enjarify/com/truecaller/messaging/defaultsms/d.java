package com.truecaller.messaging.defaultsms;

import android.os.Build;
import android.os.Build.VERSION;
import com.truecaller.analytics.b;
import com.truecaller.messaging.h;
import com.truecaller.util.al;
import com.truecaller.utils.l;
import javax.inject.Provider;

public final class d
  implements dagger.a.d
{
  private final c a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  private final Provider g;
  
  public static e a(c paramc, al paramal, com.truecaller.utils.d paramd, com.truecaller.androidactors.f paramf, b paramb, h paramh, l paraml)
  {
    Object localObject1 = paramc;
    String str1 = Build.MANUFACTURER;
    boolean bool1 = paramh.D();
    if (!bool1)
    {
      localObject2 = "xiaomi";
      bool1 = str1.equalsIgnoreCase((String)localObject2);
      int j = 21;
      int k = 1;
      int m;
      if (bool1)
      {
        i = Build.VERSION.SDK_INT;
        m = 24;
        if (i < m)
        {
          i = Build.VERSION.SDK_INT;
          if (i >= j)
          {
            i = 1;
            break label87;
          }
        }
      }
      int i = 0;
      localObject2 = null;
      label87:
      label145:
      String str2;
      if (i == 0)
      {
        localObject2 = "oppo";
        boolean bool2 = str1.equalsIgnoreCase((String)localObject2);
        m = Build.VERSION.SDK_INT;
        if (m != j)
        {
          j = Build.VERSION.SDK_INT;
          m = 22;
          if (j != m)
          {
            j = 0;
            break label145;
          }
        }
        j = 1;
        if ((bool2) && (j != 0))
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          localObject2 = null;
        }
        if (!bool2)
        {
          k = 0;
          str2 = null;
        }
      }
      if (k != 0)
      {
        localObject3 = new com/truecaller/messaging/defaultsms/f;
        str2 = a;
        localObject1 = localObject3;
        localObject2 = paramal;
        ((f)localObject3).<init>(paramal, paramd, str2, paramf, paramb, paramh, str1, paraml);
        break label258;
      }
    }
    Object localObject2 = new com/truecaller/messaging/defaultsms/g;
    String str3 = a;
    ((g)localObject2).<init>(paramal, paramd, str3, paramf, paramb, paramh, paraml);
    Object localObject3 = localObject2;
    label258:
    return (e)dagger.a.g.a(localObject3, "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.defaultsms.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */