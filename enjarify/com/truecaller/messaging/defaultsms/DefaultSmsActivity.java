package com.truecaller.messaging.defaultsms;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.log.AssertionUtil;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.wizard.utils.i;
import dagger.a.g;

public class DefaultSmsActivity
  extends AppCompatActivity
  implements h
{
  private static String b = "SHOW_PREP_SCREEN";
  e a;
  
  public static Intent a(Context paramContext, String paramString)
  {
    return a(paramContext, paramString, false);
  }
  
  public static Intent a(Context paramContext, String paramString, boolean paramBoolean)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, DefaultSmsActivity.class);
    localIntent.putExtra("AppUserInteraction.Context", paramString);
    paramContext = b;
    localIntent.putExtra(paramContext, paramBoolean);
    return localIntent;
  }
  
  public final void a()
  {
    setResult(-1);
    finish();
  }
  
  public final void a(int paramInt)
  {
    int i = 1;
    if (paramInt == i) {
      paramInt = 2131886523;
    } else {
      paramInt = 2131886522;
    }
    AlertDialog.Builder localBuilder1 = new android/support/v7/app/AlertDialog$Builder;
    localBuilder1.<init>(this);
    AlertDialog.Builder localBuilder2 = localBuilder1.setMessage(paramInt).setCancelable(false);
    -..Lambda.DefaultSmsActivity.MEUTTCil4jZbgFNMaK2N34j3mOw localMEUTTCil4jZbgFNMaK2N34j3mOw = new com/truecaller/messaging/defaultsms/-$$Lambda$DefaultSmsActivity$MEUTTCil4jZbgFNMaK2N34j3mOw;
    localMEUTTCil4jZbgFNMaK2N34j3mOw.<init>(this);
    localBuilder2.setPositiveButton(2131886521, localMEUTTCil4jZbgFNMaK2N34j3mOw).create().show();
  }
  
  public final void a(String paramString)
  {
    i.a(this, paramString, 0);
  }
  
  public final void b()
  {
    setResult(0);
    finish();
  }
  
  public final boolean b(int paramInt)
  {
    int i = 1;
    if (paramInt == i) {
      return i.c(this);
    }
    return i.d(this);
  }
  
  public final void c()
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.provider.Telephony.ACTION_CHANGE_DEFAULT");
    localIntent.putExtra("package", "com.truecaller");
    startActivityForResult(localIntent, 1);
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    a.a(paramInt1);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    com.truecaller.utils.extensions.a.a(this);
    paramBundle = getTheme();
    int i = aresId;
    paramBundle.applyStyle(i, false);
    paramBundle = getIntent().getStringExtra("AppUserInteraction.Context");
    Object localObject1 = new String[0];
    AssertionUtil.isNotNull(paramBundle, (String[])localObject1);
    localObject1 = new com/truecaller/messaging/defaultsms/a$a;
    ((a.a)localObject1).<init>((byte)0);
    Object localObject2 = (bp)g.a(((bk)getApplicationContext()).a());
    b = ((bp)localObject2);
    localObject2 = new com/truecaller/messaging/defaultsms/c;
    ((c)localObject2).<init>(paramBundle);
    paramBundle = (c)g.a(localObject2);
    a = paramBundle;
    g.a(a, c.class);
    g.a(b, bp.class);
    paramBundle = new com/truecaller/messaging/defaultsms/a;
    localObject2 = a;
    localObject1 = b;
    paramBundle.<init>((c)localObject2, (bp)localObject1, (byte)0);
    paramBundle.a(this);
    a.a(this);
  }
  
  public void onDestroy()
  {
    a.y_();
    super.onDestroy();
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    a.a(paramArrayOfString, paramArrayOfInt);
  }
  
  public void onResume()
  {
    super.onResume();
    a.a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.defaultsms.DefaultSmsActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */