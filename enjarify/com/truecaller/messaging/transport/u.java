package com.truecaller.messaging.transport;

import dagger.a.d;
import javax.inject.Provider;

public final class u
  implements d
{
  private final o a;
  private final Provider b;
  
  private u(o paramo, Provider paramProvider)
  {
    a = paramo;
    b = paramProvider;
  }
  
  public static u a(o paramo, Provider paramProvider)
  {
    u localu = new com/truecaller/messaging/transport/u;
    localu.<init>(paramo, paramProvider);
    return localu;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */