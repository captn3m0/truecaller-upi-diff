package com.truecaller.messaging.transport;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.net.Uri;
import com.truecaller.log.AssertionUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ad
{
  static final ContentProviderResult[] a = new ContentProviderResult[0];
  public final String b;
  List c;
  
  public ad(String paramString)
  {
    b = paramString;
  }
  
  public final ad.a.a a(Uri paramUri)
  {
    Object localObject = b;
    String str = paramUri.getHost();
    boolean bool = ((String)localObject).equals(str);
    String[] arrayOfString = new String[0];
    AssertionUtil.isTrue(bool, arrayOfString);
    localObject = new com/truecaller/messaging/transport/ad$a$a;
    ((ad.a.a)localObject).<init>(1, paramUri, (byte)0);
    return (ad.a.a)localObject;
  }
  
  public final void a(int paramInt)
  {
    List localList1 = c;
    if (localList1 == null) {
      return;
    }
    int i = localList1.size();
    while (paramInt < i)
    {
      List localList2 = c;
      int j = i + -1;
      localList2.remove(j);
      i += -1;
    }
  }
  
  public final void a(ad.a parama)
  {
    Object localObject = c;
    if (localObject == null)
    {
      localObject = new java/util/ArrayList;
      ((ArrayList)localObject).<init>();
      c = ((List)localObject);
    }
    c.add(parama);
  }
  
  public boolean a()
  {
    List localList = c;
    if (localList != null)
    {
      boolean bool = localList.isEmpty();
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  final ContentProviderResult[] a(ContentResolver paramContentResolver)
  {
    Object localObject1 = c;
    if (localObject1 != null)
    {
      boolean bool1 = ((List)localObject1).isEmpty();
      if (!bool1)
      {
        localObject1 = new java/util/ArrayList;
        ((ArrayList)localObject1).<init>();
        Object localObject2 = c.iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject2).hasNext();
          if (!bool2) {
            break;
          }
          ContentProviderOperation localContentProviderOperation = ((ad.a)((Iterator)localObject2).next()).a();
          ((ArrayList)localObject1).add(localContentProviderOperation);
        }
        localObject2 = b;
        return paramContentResolver.applyBatch((String)localObject2, (ArrayList)localObject1);
      }
    }
    return a;
  }
  
  public final int b()
  {
    List localList = c;
    if (localList == null) {
      return 0;
    }
    return localList.size();
  }
  
  public final ad.a.a b(Uri paramUri)
  {
    Object localObject = b;
    String str = paramUri.getHost();
    boolean bool = ((String)localObject).equals(str);
    String[] arrayOfString = new String[0];
    AssertionUtil.isTrue(bool, arrayOfString);
    localObject = new com/truecaller/messaging/transport/ad$a$a;
    ((ad.a.a)localObject).<init>(2, paramUri, (byte)0);
    return (ad.a.a)localObject;
  }
  
  public final String c()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */