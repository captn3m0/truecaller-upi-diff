package com.truecaller.messaging.transport;

import android.content.Intent;
import com.google.common.collect.Iterables;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Draft.a;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.h;
import com.truecaller.util.al;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.a.a.a.g;
import org.a.a.b;

final class n
  implements m, Iterable
{
  private static final long a = TimeUnit.SECONDS.toMillis(3);
  private static final int[] t;
  private final h b;
  private final al c;
  private final com.truecaller.utils.d d;
  private final f e;
  private final f f;
  private final l g;
  private final l h;
  private final l i;
  private final l j;
  private final l k;
  private final l l;
  private final l m;
  private final l n;
  private final l o;
  private final bw p;
  private final com.truecaller.utils.l q;
  private final com.truecaller.messaging.a r;
  private final com.truecaller.messaging.c.a s;
  
  static
  {
    int[] arrayOfInt = new int[7];
    arrayOfInt[0] = 3;
    arrayOfInt[1] = 0;
    arrayOfInt[2] = 1;
    arrayOfInt[3] = 2;
    arrayOfInt[4] = 4;
    arrayOfInt[5] = 5;
    arrayOfInt[6] = 6;
    t = arrayOfInt;
  }
  
  n(h paramh, al paramal, com.truecaller.utils.d paramd, f paramf1, f paramf2, l paraml1, l paraml2, l paraml3, l paraml4, l paraml5, l paraml6, l paraml7, l paraml8, l paraml9, bw parambw, com.truecaller.utils.l paraml, com.truecaller.messaging.a parama, com.truecaller.messaging.c.a parama1)
  {
    b = paramh;
    c = paramal;
    d = paramd;
    g = paraml1;
    f = paramf2;
    h = paraml2;
    i = paraml3;
    j = paraml4;
    k = paraml5;
    l = paraml6;
    m = paraml7;
    n = paraml8;
    o = paraml9;
    e = paramf1;
    p = parambw;
    q = paraml;
    r = parama;
    s = parama1;
  }
  
  private boolean b(Participant[] paramArrayOfParticipant)
  {
    l locall = a(0);
    int i1 = paramArrayOfParticipant.length;
    int i2 = 0;
    while (i2 < i1)
    {
      Participant localParticipant = paramArrayOfParticipant[i2];
      boolean bool = locall.a(localParticipant);
      if (!bool) {
        return false;
      }
      i2 += 1;
    }
    return true;
  }
  
  public final int a(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    int i1 = j;
    int i3 = 1;
    int i4 = 2;
    if (i1 == i4) {
      i1 = 1;
    } else {
      i1 = 0;
    }
    boolean bool1 = paramMessage.d();
    int i2 = a(bool1, paramArrayOfParticipant, i1);
    l locall = a(i2);
    int i5 = paramArrayOfParticipant.length;
    int i6 = 0;
    while (i6 < i5)
    {
      Participant localParticipant = paramArrayOfParticipant[i6];
      boolean bool2 = locall.a(localParticipant);
      if (!bool2) {
        break label131;
      }
      i6 += 1;
    }
    int i7 = locall.f();
    i5 = j;
    if (i7 != i5)
    {
      boolean bool3 = locall.c(paramMessage);
      if (bool3) {}
    }
    else
    {
      label131:
      i3 = 0;
    }
    if (i3 != 0) {
      return i2;
    }
    return 3;
  }
  
  public final int a(boolean paramBoolean1, Participant[] paramArrayOfParticipant, boolean paramBoolean2)
  {
    if (!paramBoolean2)
    {
      paramBoolean2 = a(paramArrayOfParticipant);
      if (paramBoolean2) {
        return 2;
      }
    }
    paramBoolean2 = true;
    if (paramBoolean1) {
      return paramBoolean2;
    }
    paramBoolean1 = paramArrayOfParticipant.length;
    if (paramBoolean1 > paramBoolean2)
    {
      h localh = b;
      paramBoolean1 = localh.y();
      if (paramBoolean1)
      {
        paramBoolean1 = b(paramArrayOfParticipant);
        if (paramBoolean1) {}
      }
      else
      {
        return paramBoolean2;
      }
    }
    return 0;
  }
  
  public final com.truecaller.androidactors.a a(Message paramMessage, i parami, ac paramac)
  {
    boolean bool = paramMessage.b();
    String[] arrayOfString1 = null;
    String[] arrayOfString2 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(bool, arrayOfString2);
    TransportInfo localTransportInfo = m;
    long l1 = localTransportInfo.c();
    long l2 = -1;
    bool = l1 < l2;
    if (bool)
    {
      bool = true;
    }
    else
    {
      bool = false;
      localTransportInfo = null;
    }
    arrayOfString1 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(bool, arrayOfString1);
    paramMessage = paramMessage.m();
    f = 17;
    paramMessage = paramMessage.b();
    return ((t)e.a()).d(paramMessage).a(parami, paramac);
  }
  
  public final com.truecaller.androidactors.a a(Message paramMessage, Participant[] paramArrayOfParticipant, i parami, ac paramac)
  {
    boolean bool1 = paramMessage.b();
    int i1 = 0;
    Object localObject1 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(bool1, (String[])localObject1);
    long l1 = b;
    long l2 = -1;
    bool1 = true;
    boolean bool2 = l1 < l2;
    boolean bool3;
    if (bool2)
    {
      bool3 = true;
    }
    else
    {
      bool3 = false;
      localObject1 = null;
    }
    Object localObject2 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(bool3, (String[])localObject2);
    localObject1 = m;
    l1 = ((TransportInfo)localObject1).c();
    bool2 = l1 < l2;
    if (!bool2)
    {
      bool1 = false;
      localObject3 = null;
    }
    localObject1 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(bool1, (String[])localObject1);
    Object localObject3 = new com/truecaller/messaging/data/types/Draft$a;
    ((Draft.a)localObject3).<init>();
    paramArrayOfParticipant = ((Draft.a)localObject3).a(paramArrayOfParticipant);
    localObject3 = paramMessage.j();
    d = ((String)localObject3);
    localObject3 = n;
    int i2 = localObject3.length;
    while (i1 < i2)
    {
      localObject2 = localObject3[i1];
      boolean bool4 = ((Entity)localObject2).a();
      if (!bool4)
      {
        localObject2 = (BinaryEntity)localObject2;
        paramArrayOfParticipant.a((BinaryEntity)localObject2);
      }
      i1 += 1;
    }
    localObject3 = (t)e.a();
    paramArrayOfParticipant = paramArrayOfParticipant.d();
    paramArrayOfParticipant = ((t)localObject3).a(paramArrayOfParticipant);
    localObject3 = new com/truecaller/messaging/transport/-$$Lambda$n$gmP3R2w0gNT7GznwW9YjY0tW0KM;
    ((-..Lambda.n.gmP3R2w0gNT7GznwW9YjY0tW0KM)localObject3).<init>(this, paramMessage, paramac);
    return paramArrayOfParticipant.a(parami, (ac)localObject3);
  }
  
  public final w a(Message paramMessage)
  {
    Participant[] arrayOfParticipant = new Participant[1];
    Participant localParticipant = c;
    arrayOfParticipant[0] = localParticipant;
    return a(paramMessage, arrayOfParticipant, false, false);
  }
  
  public final w a(Message paramMessage, Participant[] paramArrayOfParticipant, boolean paramBoolean1, boolean paramBoolean2)
  {
    boolean bool = paramMessage.c();
    int i1 = 0;
    if (!bool) {
      return w.b(null);
    }
    bool = paramMessage.d();
    paramBoolean2 = a(bool, paramArrayOfParticipant, paramBoolean2);
    l locall = a(paramBoolean2);
    int i2 = 0;
    String[] arrayOfString = new String[0];
    AssertionUtil.isNotNull(locall, arrayOfString);
    bool = locall.b(paramMessage);
    if (!bool) {
      return w.b(null);
    }
    if (paramBoolean1)
    {
      paramBoolean1 = true;
      if (paramBoolean2 != paramBoolean1) {
        i2 = 1;
      }
    }
    Object localObject = (d)f.a();
    long l1;
    if (i2 != 0) {
      l1 = a;
    } else {
      l1 = 0L;
    }
    i1 = (int)l1;
    paramArrayOfParticipant = ((d)localObject).a(paramMessage, paramArrayOfParticipant, paramBoolean2, i1);
    localObject = s;
    paramMessage = o;
    ((com.truecaller.messaging.c.a)localObject).a(paramMessage);
    return paramArrayOfParticipant;
  }
  
  public final l a(int paramInt)
  {
    Object localObject = b(paramInt);
    if (localObject != null) {
      return (l)localObject;
    }
    localObject = new java/lang/IllegalArgumentException;
    ((IllegalArgumentException)localObject).<init>("Unsupported transport type");
    throw ((Throwable)localObject);
  }
  
  public final Iterable a()
  {
    return this;
  }
  
  public final void a(int paramInt1, Intent paramIntent, int paramInt2)
  {
    l locall = b(paramInt1);
    if (locall == null) {
      return;
    }
    ((d)f.a()).a(locall, paramIntent, paramInt2);
  }
  
  public final void a(boolean paramBoolean)
  {
    com.truecaller.utils.d locald = d;
    boolean bool1 = locald.d();
    Object localObject1 = b;
    boolean bool2 = ((h)localObject1).b();
    com.truecaller.utils.l locall = q;
    Object localObject2 = { "android.permission.READ_SMS" };
    boolean bool3 = locall.a((String[])localObject2);
    localObject2 = b;
    boolean bool4 = ((h)localObject2).d();
    boolean bool5 = true;
    if (bool2 != bool1)
    {
      localObject1 = b;
      ((h)localObject1).a(bool1);
      if (bool1)
      {
        localObject1 = (t)e.a();
        ((t)localObject1).b();
        bool2 = true;
        break label131;
      }
    }
    bool2 = false;
    localObject1 = null;
    label131:
    if (bool3 != bool4)
    {
      localObject1 = b;
      ((h)localObject1).b(bool3);
      if (bool3)
      {
        localObject1 = c;
        ((al)localObject1).j();
      }
      bool2 = true;
    }
    if (!bool3) {
      return;
    }
    if (bool2)
    {
      ((t)e.a()).a(bool5);
      return;
    }
    if (paramBoolean) {
      return;
    }
    ((t)e.a()).b(bool1);
  }
  
  public final boolean a(Message paramMessage, Entity paramEntity)
  {
    long l1 = h;
    long l2 = -1;
    boolean bool1 = l1 < l2;
    boolean bool2;
    if (bool1) {
      bool2 = true;
    } else {
      bool2 = false;
    }
    String[] arrayOfString = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(bool2, arrayOfString);
    int i1 = j;
    return a(i1).a(paramMessage, paramEntity);
  }
  
  public final boolean a(String paramString, boolean paramBoolean1, Participant[] paramArrayOfParticipant, boolean paramBoolean2, a parama)
  {
    paramBoolean1 = a(paramBoolean1, paramArrayOfParticipant, paramBoolean2);
    return a(paramBoolean1).a(paramString, parama);
  }
  
  public final boolean a(Participant[] paramArrayOfParticipant)
  {
    Object localObject = p;
    boolean bool1 = ((bw)localObject).a();
    if (bool1)
    {
      int i1 = paramArrayOfParticipant.length;
      int i2 = 1;
      if (i1 == i2)
      {
        localObject = j;
        paramArrayOfParticipant = paramArrayOfParticipant[0];
        boolean bool2 = ((l)localObject).a(paramArrayOfParticipant);
        if (bool2) {
          return i2;
        }
      }
    }
    return false;
  }
  
  public final l b(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      return null;
    case 6: 
      return o;
    case 5: 
      return n;
    case 4: 
      return k;
    case 3: 
      return g;
    case 2: 
      return j;
    case 1: 
      locald = d;
      str = r.b();
      paramInt = locald.a(str);
      if (paramInt != 0) {
        return i;
      }
      return m;
    }
    com.truecaller.utils.d locald = d;
    String str = r.b();
    paramInt = locald.a(str);
    if (paramInt != 0) {
      return h;
    }
    return l;
  }
  
  public final List b()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Iterator localIterator = iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject = (l)localIterator.next();
      boolean bool2 = ((l)localObject).c();
      if (bool2)
      {
        int i1 = ((l)localObject).f();
        localObject = Integer.valueOf(i1);
        localArrayList.add(localObject);
      }
    }
    return localArrayList;
  }
  
  public final List b(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    boolean bool = paramMessage.d();
    int i1 = a(bool, paramArrayOfParticipant, false);
    Object localObject = Integer.valueOf(i1);
    localArrayList.add(localObject);
    localObject = Arrays.asList(org.c.a.a.a.a.a(t));
    -..Lambda.n.KExfGeCK65xvT_ib5ITs_AGzYIU localKExfGeCK65xvT_ib5ITs_AGzYIU = new com/truecaller/messaging/transport/-$$Lambda$n$KExfGeCK65xvT_ib5ITs_AGzYIU;
    localKExfGeCK65xvT_ib5ITs_AGzYIU.<init>(this, i1, paramMessage, paramArrayOfParticipant);
    paramMessage = Iterables.filter((Iterable)localObject, localKExfGeCK65xvT_ib5ITs_AGzYIU);
    Iterables.addAll(localArrayList, paramMessage);
    return localArrayList;
  }
  
  public final boolean b(Message paramMessage)
  {
    int i1 = f;
    int i2 = 9;
    i1 &= i2;
    if (i1 != i2) {
      return false;
    }
    Object localObject = (t)e.a();
    long l1 = ay_a;
    localObject = ((t)localObject).a(paramMessage, l1);
    -..Lambda.n.ZbCl5pW11Hb04ZqUbbYxfPZHQSw localZbCl5pW11Hb04ZqUbbYxfPZHQSw = new com/truecaller/messaging/transport/-$$Lambda$n$ZbCl5pW11Hb04ZqUbbYxfPZHQSw;
    localZbCl5pW11Hb04ZqUbbYxfPZHQSw.<init>(this, paramMessage);
    ((w)localObject).a(localZbCl5pW11Hb04ZqUbbYxfPZHQSw);
    return true;
  }
  
  public final int c(Message paramMessage)
  {
    int i1 = j;
    l locall = b(i1);
    String[] arrayOfString = new String[0];
    AssertionUtil.AlwaysFatal.isNotNull(locall, arrayOfString);
    return locall.e(paramMessage);
  }
  
  public final boolean d(Message paramMessage)
  {
    int i1 = j;
    l locall = b(i1);
    String[] arrayOfString = new String[0];
    AssertionUtil.AlwaysFatal.isNotNull(locall, arrayOfString);
    return locall.f(paramMessage);
  }
  
  public final Iterator iterator()
  {
    n.a locala = new com/truecaller/messaging/transport/n$a;
    locala.<init>(this, (byte)0);
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */