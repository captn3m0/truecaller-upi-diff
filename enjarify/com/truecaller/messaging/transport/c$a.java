package com.truecaller.messaging.transport;

import android.database.Cursor;
import com.truecaller.messaging.data.types.Message;

public abstract interface c$a
  extends Cursor
{
  public abstract long a();
  
  public abstract long b();
  
  public abstract long c();
  
  public abstract boolean d();
  
  public abstract boolean e();
  
  public abstract boolean f();
  
  public abstract int g();
  
  public abstract int h();
  
  public abstract Message i();
  
  public abstract String j();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */