package com.truecaller.messaging.transport;

import dagger.a.d;
import javax.inject.Provider;

public final class s
  implements d
{
  private final o a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  
  private s(o paramo, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    a = paramo;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static s a(o paramo, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    s locals = new com/truecaller/messaging/transport/s;
    locals.<init>(paramo, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
    return locals;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */