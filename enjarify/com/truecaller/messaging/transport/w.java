package com.truecaller.messaging.transport;

import dagger.a.d;
import javax.inject.Provider;

public final class w
  implements d
{
  private final o a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private w(o paramo, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramo;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static w a(o paramo, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    w localw = new com/truecaller/messaging/transport/w;
    localw.<init>(paramo, paramProvider1, paramProvider2, paramProvider3);
    return localw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */