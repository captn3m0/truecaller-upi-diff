package com.truecaller.messaging.transport;

import dagger.a.d;
import javax.inject.Provider;

public final class y
  implements d
{
  private final o a;
  private final Provider b;
  
  private y(o paramo, Provider paramProvider)
  {
    a = paramo;
    b = paramProvider;
  }
  
  public static y a(o paramo, Provider paramProvider)
  {
    y localy = new com/truecaller/messaging/transport/y;
    localy.<init>(paramo, paramProvider);
    return localy;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */