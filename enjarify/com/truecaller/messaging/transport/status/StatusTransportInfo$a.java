package com.truecaller.messaging.transport.status;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class StatusTransportInfo$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    StatusTransportInfo localStatusTransportInfo = new com/truecaller/messaging/transport/status/StatusTransportInfo;
    long l = paramParcel.readLong();
    paramParcel = paramParcel.readString();
    localStatusTransportInfo.<init>(l, paramParcel);
    return localStatusTransportInfo;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new StatusTransportInfo[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.status.StatusTransportInfo.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */