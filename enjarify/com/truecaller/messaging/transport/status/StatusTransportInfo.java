package com.truecaller.messaging.transport.status;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;
import com.truecaller.messaging.data.types.TransportInfo;
import org.a.a.b;

public final class StatusTransportInfo
  implements TransportInfo
{
  public static final Parcelable.Creator CREATOR;
  public final String a;
  private final long b;
  
  static
  {
    StatusTransportInfo.a locala = new com/truecaller/messaging/transport/status/StatusTransportInfo$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public StatusTransportInfo(long paramLong, String paramString)
  {
    b = paramLong;
    a = paramString;
  }
  
  public final int a()
  {
    return 0;
  }
  
  public final String a(b paramb)
  {
    k.b(paramb, "date");
    return a;
  }
  
  public final int b()
  {
    return 0;
  }
  
  public final long c()
  {
    return b;
  }
  
  public final long d()
  {
    return 0L;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final long e()
  {
    return -1;
  }
  
  public final boolean f()
  {
    return false;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    long l = b;
    paramParcel.writeLong(l);
    String str = a;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.status.StatusTransportInfo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */