package com.truecaller.messaging.transport.status;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import c.a.an;
import c.u;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.content.TruecallerContract.al;
import com.truecaller.content.TruecallerContract.z;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.transport.ad;
import com.truecaller.messaging.transport.ad.b;
import com.truecaller.messaging.transport.i;
import com.truecaller.messaging.transport.l;
import com.truecaller.messaging.transport.l.a;
import com.truecaller.utils.q;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.a.a.a.g;

public final class a
  implements l
{
  private final ContentResolver a;
  private final ad.b b;
  
  public a(ContentResolver paramContentResolver, ad.b paramb)
  {
    a = paramContentResolver;
    b = paramb;
  }
  
  private final ContentProviderResult[] a(ArrayList paramArrayList)
  {
    try
    {
      ContentResolver localContentResolver = a;
      String str = TruecallerContract.a();
      return localContentResolver.applyBatch(str, paramArrayList);
    }
    catch (RemoteException|OperationApplicationException localRemoteException) {}
    return null;
  }
  
  public final long a(long paramLong)
  {
    return paramLong;
  }
  
  public final long a(com.truecaller.messaging.transport.f paramf, i parami, r paramr, org.a.a.b paramb1, org.a.a.b paramb2, int paramInt, List paramList, q paramq, boolean paramBoolean1, boolean paramBoolean2, Set paramSet)
  {
    c.g.b.k.b(paramf, "threadInfoCache");
    c.g.b.k.b(parami, "participantCache");
    c.g.b.k.b(paramr, "cursor");
    c.g.b.k.b(paramb1, "timeTo");
    c.g.b.k.b(paramb2, "timeFrom");
    c.g.b.k.b(paramList, "operations");
    c.g.b.k.b(paramq, "trace");
    c.g.b.k.b(paramSet, "messagesToClassify");
    return Long.MIN_VALUE;
  }
  
  public final l.a a(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramArrayOfParticipant, "recipients");
    paramMessage = new java/lang/UnsupportedOperationException;
    paramMessage.<init>();
    throw ((Throwable)paramMessage);
  }
  
  public final String a()
  {
    return "status";
  }
  
  public final String a(String paramString)
  {
    c.g.b.k.b(paramString, "simToken");
    return paramString;
  }
  
  public final void a(Intent paramIntent, int paramInt)
  {
    c.g.b.k.b(paramIntent, "intent");
    paramIntent = new java/lang/UnsupportedOperationException;
    paramIntent.<init>();
    throw ((Throwable)paramIntent);
  }
  
  public final void a(BinaryEntity paramBinaryEntity)
  {
    c.g.b.k.b(paramBinaryEntity, "entity");
    paramBinaryEntity = new java/lang/UnsupportedOperationException;
    paramBinaryEntity.<init>();
    throw ((Throwable)paramBinaryEntity);
  }
  
  public final void a(Message paramMessage, String paramString1, String paramString2)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramString2, "initiatedVia");
    paramMessage = new java/lang/UnsupportedOperationException;
    paramMessage.<init>();
    throw ((Throwable)paramMessage);
  }
  
  public final void a(org.a.a.b paramb)
  {
    c.g.b.k.b(paramb, "time");
  }
  
  public final boolean a(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = m;
    if (localObject1 != null)
    {
      localObject1 = (StatusTransportInfo)localObject1;
      Object localObject2 = ContentProviderOperation.newAssertQuery(TruecallerContract.al.a());
      int i = 1;
      String[] arrayOfString = new String[i];
      Object localObject3 = a;
      arrayOfString[0] = localObject3;
      localObject2 = ((ContentProviderOperation.Builder)localObject2).withSelection("raw_id = ?", arrayOfString).withExpectedCount(0).build();
      localArrayList.add(localObject2);
      localObject2 = localArrayList;
      localObject2 = (List)localArrayList;
      Object localObject4 = c;
      com.truecaller.messaging.data.b.a((List)localObject2, (Participant)localObject4);
      localObject4 = an.a(c);
      int j = com.truecaller.messaging.data.b.a((List)localObject2, (Set)localObject4);
      int k = localArrayList.size();
      localObject4 = ContentProviderOperation.newInsert(TruecallerContract.aa.a()).withValueBackReference("conversation_id", j);
      int m = -1;
      Object localObject5 = Integer.valueOf(m);
      localObject4 = ((ContentProviderOperation.Builder)localObject4).withValue("participant_id", localObject5);
      localObject3 = new android/content/ContentValues;
      ((ContentValues)localObject3).<init>();
      Object localObject6 = e;
      c.g.b.k.a(localObject6, "date");
      localObject6 = Long.valueOf(a);
      ((ContentValues)localObject3).put("date", (Long)localObject6);
      localObject6 = d;
      c.g.b.k.a(localObject6, "dateSent");
      long l = a;
      localObject6 = Long.valueOf(l);
      ((ContentValues)localObject3).put("date_sent", (Long)localObject6);
      localObject6 = Integer.valueOf(f);
      ((ContentValues)localObject3).put("status", (Integer)localObject6);
      localObject6 = Integer.valueOf(i);
      ((ContentValues)localObject3).put("seen", (Integer)localObject6);
      localObject6 = Boolean.valueOf(h);
      ((ContentValues)localObject3).put("read", (Boolean)localObject6);
      localObject6 = Integer.valueOf(j);
      ((ContentValues)localObject3).put("transport", (Integer)localObject6);
      int n = 2;
      Integer localInteger = Integer.valueOf(n);
      ((ContentValues)localObject3).put("category", localInteger);
      localObject5 = "classification";
      localObject6 = Integer.valueOf(n);
      ((ContentValues)localObject3).put((String)localObject5, (Integer)localObject6);
      localObject4 = ((ContentProviderOperation.Builder)localObject4).withValues((ContentValues)localObject3).build();
      localArrayList.add(localObject4);
      com.truecaller.messaging.data.b.a((List)localObject2, k, (StatusTransportInfo)localObject1);
      localObject1 = n;
      int i1 = localObject1.length;
      if (i1 == i)
      {
        bool = true;
      }
      else
      {
        bool = false;
        localObject1 = null;
      }
      localObject2 = new String[0];
      AssertionUtil.AlwaysFatal.isTrue(bool, (String[])localObject2);
      paramMessage = n;
      c.g.b.k.a(paramMessage, "message.entities");
      paramMessage = c.a.f.b(paramMessage);
      c.g.b.k.a(paramMessage, "message.entities.first()");
      paramMessage = (Entity)paramMessage;
      boolean bool = paramMessage.a();
      localObject2 = new String[0];
      AssertionUtil.AlwaysFatal.isTrue(bool, (String[])localObject2);
      localObject1 = ContentProviderOperation.newInsert(TruecallerContract.z.a());
      localObject2 = new android/content/ContentValues;
      ((ContentValues)localObject2).<init>();
      paramMessage.a((ContentValues)localObject2);
      paramMessage = ((ContentProviderOperation.Builder)localObject1).withValues((ContentValues)localObject2);
      localObject1 = "message_id";
      paramMessage = paramMessage.withValueBackReference((String)localObject1, k).build();
      localArrayList.add(paramMessage);
      paramMessage = a(localArrayList);
      int i2;
      if (paramMessage != null)
      {
        i2 = paramMessage.length;
      }
      else
      {
        i2 = 0;
        paramMessage = null;
      }
      if (i2 > 0) {
        return i;
      }
      return false;
    }
    paramMessage = new c/u;
    paramMessage.<init>("null cannot be cast to non-null type com.truecaller.messaging.transport.status.StatusTransportInfo");
    throw paramMessage;
  }
  
  public final boolean a(Message paramMessage, Entity paramEntity)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramEntity, "entity");
    paramMessage = new java/lang/UnsupportedOperationException;
    paramMessage.<init>();
    throw ((Throwable)paramMessage);
  }
  
  public final boolean a(Participant paramParticipant)
  {
    c.g.b.k.b(paramParticipant, "participant");
    return false;
  }
  
  public final boolean a(String paramString, com.truecaller.messaging.transport.a parama)
  {
    c.g.b.k.b(paramString, "text");
    c.g.b.k.b(parama, "result");
    return false;
  }
  
  public final void b(long paramLong)
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>();
    throw ((Throwable)localUnsupportedOperationException);
  }
  
  public final boolean b(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return false;
  }
  
  public final boolean b(ad paramad)
  {
    String str = "transaction";
    c.g.b.k.b(paramad, str);
    boolean bool1 = paramad.a();
    if (!bool1)
    {
      paramad = paramad.c();
      str = TruecallerContract.a();
      boolean bool2 = c.g.b.k.a(paramad, str);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean c()
  {
    return false;
  }
  
  public final boolean c(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return false;
  }
  
  public final com.truecaller.messaging.transport.k d(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    paramMessage = new java/lang/UnsupportedOperationException;
    paramMessage.<init>();
    throw ((Throwable)paramMessage);
  }
  
  public final org.a.a.b d()
  {
    org.a.a.b localb = org.a.a.b.ay_();
    c.g.b.k.a(localb, "DateTime.now()");
    return localb;
  }
  
  public final int e(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return 0;
  }
  
  public final boolean e()
  {
    return false;
  }
  
  public final int f()
  {
    return 6;
  }
  
  public final boolean f(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    paramMessage = new java/lang/UnsupportedOperationException;
    paramMessage.<init>();
    throw ((Throwable)paramMessage);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.status.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */