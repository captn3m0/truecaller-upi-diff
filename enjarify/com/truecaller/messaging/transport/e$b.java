package com.truecaller.messaging.transport;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.messaging.data.types.Message;

final class e$b
  extends u
{
  private final Message b;
  
  private e$b(e parame, Message paramMessage)
  {
    super(parame);
    b = paramMessage;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".resendMessage(");
    String str = a(b, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */