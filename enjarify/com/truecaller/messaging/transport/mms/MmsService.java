package com.truecaller.messaging.transport.mms;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.a;
import dagger.a.g;
import java.util.Collections;
import java.util.Map;
import okhttp3.u;

public class MmsService
  extends IntentService
{
  private static volatile j a;
  
  public MmsService()
  {
    super("MmsSender");
  }
  
  private static j a(Context paramContext)
  {
    Object localObject1 = a;
    if (localObject1 == null) {
      synchronized (MmsService.class)
      {
        localObject1 = a;
        if (localObject1 == null)
        {
          paramContext = paramContext.getApplicationContext();
          paramContext = (bk)paramContext;
          paramContext = paramContext.a();
          localObject1 = new com/truecaller/messaging/transport/mms/f$a;
          ((f.a)localObject1).<init>((byte)0);
          paramContext = g.a(paramContext);
          paramContext = (bp)paramContext;
          b = paramContext;
          paramContext = a;
          if (paramContext == null)
          {
            paramContext = new com/truecaller/messaging/transport/mms/o;
            paramContext.<init>();
            a = paramContext;
          }
          paramContext = b;
          Object localObject2 = bp.class;
          g.a(paramContext, (Class)localObject2);
          paramContext = new com/truecaller/messaging/transport/mms/f;
          localObject2 = a;
          localObject1 = b;
          paramContext.<init>((o)localObject2, (bp)localObject1, (byte)0);
          a = paramContext;
          localObject1 = paramContext;
        }
      }
    }
    return (j)localObject1;
  }
  
  public static boolean a(boolean paramBoolean, Context paramContext, Uri paramUri, u paramu, PendingIntent paramPendingIntent, a parama, SimInfo paramSimInfo)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, MmsService.class);
    Map localMap1 = parama.f();
    Map localMap2;
    if (localMap1 == null)
    {
      localMap1 = Collections.emptyMap();
      localMap2 = localMap1;
    }
    else
    {
      localMap2 = localMap1;
    }
    MmsRequest localMmsRequest = new com/truecaller/messaging/transport/mms/MmsRequest;
    String str = parama.e();
    boolean bool1 = parama.g();
    boolean bool2 = parama.c();
    localMmsRequest.<init>(paramBoolean, paramUri, paramu, paramPendingIntent, str, bool1, localMap2, paramSimInfo, bool2);
    localIntent.putExtra("request", localMmsRequest);
    paramContext.startService(localIntent);
    return true;
  }
  
  protected void onHandleIntent(Intent paramIntent)
  {
    String str = "request";
    paramIntent = (MmsRequest)paramIntent.getParcelableExtra(str);
    if (paramIntent == null) {
      return;
    }
    a(this).a().a(paramIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.MmsService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */