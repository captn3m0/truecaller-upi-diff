package com.truecaller.messaging.transport.mms;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.Telephony.Mms;
import android.provider.Telephony.Mms.Inbox;
import android.provider.Telephony.Threads;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.SparseArray;
import com.android.a.a.a.n;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e.a;
import com.truecaller.common.h.am;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.transport.ad;
import com.truecaller.messaging.transport.ad.b;
import com.truecaller.messaging.transport.i;
import com.truecaller.messaging.transport.k.b;
import com.truecaller.messaging.transport.l.a;
import com.truecaller.multisim.SimInfo;
import com.truecaller.tracking.events.u.a;
import com.truecaller.utils.extensions.m;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.a.a.a.g;

final class ao
  implements com.truecaller.messaging.transport.l
{
  static final int[] a;
  public static final Uri b;
  public static final String c;
  private static final long d;
  private static final String[] e;
  private static final String[] f;
  private static final String g;
  private final Context h;
  private final com.truecaller.messaging.h i;
  private final com.truecaller.androidactors.f j;
  private final com.truecaller.multisim.h k;
  private final ak l;
  private final com.truecaller.utils.d m;
  private final an n;
  private final h o;
  private final TelephonyManager p;
  private final Set q;
  private final com.truecaller.androidactors.f r;
  private final com.truecaller.androidactors.f s;
  private final ad.b t;
  private final com.truecaller.analytics.b u;
  private final com.truecaller.utils.l v;
  private final com.truecaller.messaging.a w;
  private ao.a x;
  private boolean y;
  
  static
  {
    Object localObject1 = TimeUnit.DAYS;
    long l1 = 7;
    long l2 = ((TimeUnit)localObject1).toSeconds(l1);
    d = l2;
    int i1 = 3;
    localObject1 = new int[i1];
    localObject1[0] = '';
    localObject1[1] = '';
    localObject1[2] = '';
    a = (int[])localObject1;
    String str1 = "pri";
    String str2 = "d_rpt";
    String str3 = "rr";
    String[] tmp64_61 = new String[5];
    String[] tmp65_64 = tmp64_61;
    String[] tmp65_64 = tmp64_61;
    tmp65_64[0] = "date";
    tmp65_64[1] = "m_cls";
    String[] tmp74_65 = tmp65_64;
    String[] tmp74_65 = tmp65_64;
    tmp74_65[2] = str1;
    tmp74_65[3] = str2;
    tmp74_65[4] = str3;
    e = tmp74_65;
    f = new String[] { "charset", "address" };
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("type IN (");
    Object localObject2 = a;
    if (localObject2 == null)
    {
      localObject2 = null;
    }
    else
    {
      int i2 = localObject2.length;
      localObject2 = org.c.a.a.a.k.a((int[])localObject2, i2);
    }
    ((StringBuilder)localObject1).append((String)localObject2);
    ((StringBuilder)localObject1).append(")");
    g = ((StringBuilder)localObject1).toString();
    localObject1 = Telephony.Mms.CONTENT_URI;
    b = (Uri)localObject1;
    c = ((Uri)localObject1).getAuthority();
  }
  
  ao(Context paramContext, com.truecaller.androidactors.f paramf1, com.truecaller.messaging.h paramh, com.truecaller.utils.d paramd, com.truecaller.multisim.h paramh1, ak paramak, TelephonyManager paramTelephonyManager, an paraman, com.truecaller.androidactors.f paramf2, h paramh2, com.truecaller.androidactors.f paramf3, ad.b paramb, com.truecaller.analytics.b paramb1, com.truecaller.utils.l paraml, com.truecaller.messaging.a parama)
  {
    Object localObject = new java/util/HashSet;
    ((HashSet)localObject).<init>();
    q = ((Set)localObject);
    x = null;
    y = false;
    localObject = paramContext;
    h = paramContext;
    localObject = paramh;
    i = paramh;
    localObject = paramf1;
    j = paramf1;
    localObject = paramd;
    m = paramd;
    localObject = paraman;
    n = paraman;
    localObject = paramh1;
    k = paramh1;
    localObject = paramak;
    l = paramak;
    localObject = paramTelephonyManager;
    p = paramTelephonyManager;
    localObject = paramf2;
    r = paramf2;
    localObject = paramf3;
    s = paramf3;
    localObject = paramh2;
    o = paramh2;
    localObject = paramb;
    t = paramb;
    localObject = paramb1;
    u = paramb1;
    localObject = paraml;
    v = paraml;
    localObject = parama;
    w = parama;
  }
  
  private long a(Message paramMessage, MmsTransportInfo paramMmsTransportInfo)
  {
    paramMmsTransportInfo = E;
    int i1 = 0;
    String str = null;
    Object localObject1 = new String[0];
    AssertionUtil.AlwaysFatal.isNotNull(paramMmsTransportInfo, (String[])localObject1);
    int i3 = paramMmsTransportInfo.size();
    int i4 = 1;
    if (i3 > 0)
    {
      i3 = 1;
    }
    else
    {
      i3 = 0;
      localObject1 = null;
    }
    Object localObject2 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(i3, (String[])localObject2);
    localObject1 = new java/util/HashSet;
    ((HashSet)localObject1).<init>();
    localObject2 = a;
    int i5 = localObject2.length;
    while (i1 < i5)
    {
      int i6 = localObject2[i1];
      Set localSet = (Set)paramMmsTransportInfo.get(i6);
      if (localSet != null) {
        ((Set)localObject1).addAll(localSet);
      }
      i1 += 1;
    }
    i1 = f & i4;
    if (i1 == 0)
    {
      int i7 = ((Set)localObject1).size();
      if (i7 == i4) {
        ((Set)localObject1).clear();
      }
      paramMessage = c.f;
      ((Set)localObject1).add(paramMessage);
    }
    else
    {
      int i8 = ((Set)localObject1).size();
      if (i8 > i4)
      {
        i8 = 137;
        paramMessage = (Set)paramMmsTransportInfo.get(i8);
        if (paramMessage != null) {
          ((Set)localObject1).addAll(paramMessage);
        }
      }
    }
    try
    {
      paramMessage = h;
      return Telephony.Threads.getOrCreateThreadId(paramMessage, (Set)localObject1);
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      paramMessage = new java/lang/StringBuilder;
      paramMessage.<init>("For some reasons we can not create thread for addresses: [");
      paramMmsTransportInfo = ((Set)localObject1).iterator();
      for (;;)
      {
        boolean bool1 = paramMmsTransportInfo.hasNext();
        if (!bool1) {
          break;
        }
        str = (String)paramMmsTransportInfo.next();
        paramMessage.append("{isEmpty:");
        boolean bool2 = am.a("insert-address-token", str);
        paramMessage.append(bool2);
        localObject1 = ", length:";
        paramMessage.append((String)localObject1);
        int i2;
        if (str == null) {
          i2 = -1;
        } else {
          i2 = str.length();
        }
        paramMessage.append(i2);
        str = "}, ";
        paramMessage.append(str);
      }
      paramMessage.append("]");
      AssertionUtil.reportWeirdnessButNeverCrash(paramMessage.toString());
    }
    return -1;
  }
  
  private com.android.a.a.a.f a(Uri paramUri, boolean paramBoolean)
  {
    InputStream localInputStream = null;
    try
    {
      Object localObject = h;
      localObject = ((Context)localObject).getContentResolver();
      localInputStream = ((ContentResolver)localObject).openInputStream(paramUri);
      if (localInputStream != null)
      {
        paramUri = new com/android/a/a/a/n;
        localObject = m.a(localInputStream);
        paramUri.<init>((byte[])localObject, paramBoolean);
        paramUri = paramUri.a();
        return paramUri;
      }
      IOException localIOException = new java/io/IOException;
      localObject = "Can't open stream with PDU content from ";
      paramUri = String.valueOf(paramUri);
      paramUri = ((String)localObject).concat(paramUri);
      localIOException.<init>(paramUri);
      throw localIOException;
    }
    finally
    {
      com.truecaller.utils.extensions.d.a(localInputStream);
    }
  }
  
  /* Error */
  private Entity a(long paramLong, PduEntity paramPduEntity)
  {
    // Byte code:
    //   0: new 322	android/content/ContentValues
    //   3: astore 4
    //   5: aload 4
    //   7: invokespecial 323	android/content/ContentValues:<init>	()V
    //   10: aload_0
    //   11: getfield 162	com/truecaller/messaging/transport/mms/ao:o	Lcom/truecaller/messaging/transport/mms/h;
    //   14: astore 5
    //   16: aload 5
    //   18: aload_3
    //   19: aload 4
    //   21: invokeinterface 328 3 0
    //   26: aload_0
    //   27: getfield 142	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   30: astore 5
    //   32: aload 5
    //   34: invokevirtual 283	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   37: astore 5
    //   39: new 91	java/lang/StringBuilder
    //   42: astore 6
    //   44: ldc_w 330
    //   47: astore 7
    //   49: aload 6
    //   51: aload 7
    //   53: invokespecial 97	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   56: aload 6
    //   58: lload_1
    //   59: invokevirtual 333	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   62: pop
    //   63: ldc_w 335
    //   66: astore 8
    //   68: aload 6
    //   70: aload 8
    //   72: invokevirtual 106	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   75: pop
    //   76: aload 6
    //   78: invokevirtual 112	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   81: astore 8
    //   83: aload 8
    //   85: invokestatic 339	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   88: astore 8
    //   90: iconst_1
    //   91: istore 9
    //   93: iload 9
    //   95: anewarray 81	java/lang/String
    //   98: astore 6
    //   100: new 91	java/lang/StringBuilder
    //   103: astore 7
    //   105: ldc_w 341
    //   108: astore 10
    //   110: aload 7
    //   112: aload 10
    //   114: invokespecial 97	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   117: aload 8
    //   119: invokevirtual 342	android/net/Uri:toString	()Ljava/lang/String;
    //   122: astore 10
    //   124: aload 7
    //   126: aload 10
    //   128: invokevirtual 106	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   131: pop
    //   132: aload 7
    //   134: invokevirtual 112	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   137: astore 7
    //   139: aconst_null
    //   140: astore 10
    //   142: aload 6
    //   144: iconst_0
    //   145: aload 7
    //   147: aastore
    //   148: aconst_null
    //   149: astore 6
    //   151: aload 5
    //   153: aload 8
    //   155: aload 4
    //   157: invokevirtual 346	android/content/ContentResolver:insert	(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    //   160: astore 11
    //   162: aload 11
    //   164: ifnonnull +72 -> 236
    //   167: ldc_w 348
    //   170: astore 8
    //   172: aload_3
    //   173: getfield 351	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   176: invokevirtual 354	android/net/Uri:getScheme	()Ljava/lang/String;
    //   179: astore 12
    //   181: aload 8
    //   183: aload 12
    //   185: invokevirtual 357	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   188: istore 13
    //   190: iload 13
    //   192: ifeq +42 -> 234
    //   195: new 359	java/io/File
    //   198: astore 8
    //   200: aload_3
    //   201: getfield 351	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   204: invokevirtual 362	android/net/Uri:getPath	()Ljava/lang/String;
    //   207: astore 12
    //   209: aload 8
    //   211: aload 12
    //   213: invokespecial 363	java/io/File:<init>	(Ljava/lang/String;)V
    //   216: aload 8
    //   218: invokevirtual 366	java/io/File:exists	()Z
    //   221: istore 9
    //   223: iload 9
    //   225: ifeq +9 -> 234
    //   228: aload 8
    //   230: invokevirtual 369	java/io/File:delete	()Z
    //   233: pop
    //   234: aconst_null
    //   235: areturn
    //   236: iload 9
    //   238: anewarray 81	java/lang/String
    //   241: astore 7
    //   243: new 91	java/lang/StringBuilder
    //   246: astore 14
    //   248: ldc_w 371
    //   251: astore 15
    //   253: aload 14
    //   255: aload 15
    //   257: invokespecial 97	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   260: aload 8
    //   262: invokevirtual 342	android/net/Uri:toString	()Ljava/lang/String;
    //   265: astore 8
    //   267: aload 14
    //   269: aload 8
    //   271: invokevirtual 106	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   274: pop
    //   275: ldc_w 373
    //   278: astore 8
    //   280: aload 14
    //   282: aload 8
    //   284: invokevirtual 106	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   287: pop
    //   288: aload_3
    //   289: getfield 375	com/truecaller/messaging/transport/mms/PduEntity:j	Ljava/lang/String;
    //   292: astore 8
    //   294: aload 14
    //   296: aload 8
    //   298: invokevirtual 106	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   301: pop
    //   302: ldc_w 377
    //   305: astore 8
    //   307: aload 14
    //   309: aload 8
    //   311: invokevirtual 106	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   314: pop
    //   315: aload 11
    //   317: invokevirtual 342	android/net/Uri:toString	()Ljava/lang/String;
    //   320: astore 8
    //   322: aload 14
    //   324: aload 8
    //   326: invokevirtual 106	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   329: pop
    //   330: aload 14
    //   332: invokevirtual 112	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   335: astore 8
    //   337: aload 7
    //   339: iconst_0
    //   340: aload 8
    //   342: aastore
    //   343: ldc_w 379
    //   346: astore 8
    //   348: aload 4
    //   350: aload 8
    //   352: invokevirtual 383	android/content/ContentValues:containsKey	(Ljava/lang/String;)Z
    //   355: istore 13
    //   357: iload 13
    //   359: ifeq +108 -> 467
    //   362: aload_3
    //   363: getfield 375	com/truecaller/messaging/transport/mms/PduEntity:j	Ljava/lang/String;
    //   366: astore 8
    //   368: ldc_w 379
    //   371: astore 12
    //   373: aload 4
    //   375: aload 12
    //   377: invokevirtual 386	android/content/ContentValues:getAsString	(Ljava/lang/String;)Ljava/lang/String;
    //   380: astore 12
    //   382: aload_3
    //   383: getfield 387	com/truecaller/messaging/transport/mms/PduEntity:d	J
    //   386: lstore 16
    //   388: aload 8
    //   390: aload 12
    //   392: lload 16
    //   394: invokestatic 392	com/truecaller/messaging/data/types/Entity:a	(Ljava/lang/String;Ljava/lang/String;J)Lcom/truecaller/messaging/data/types/Entity;
    //   397: astore 8
    //   399: ldc_w 348
    //   402: astore 12
    //   404: aload_3
    //   405: getfield 351	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   408: invokevirtual 354	android/net/Uri:getScheme	()Ljava/lang/String;
    //   411: astore 4
    //   413: aload 12
    //   415: aload 4
    //   417: invokevirtual 357	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   420: istore 9
    //   422: iload 9
    //   424: ifeq +40 -> 464
    //   427: new 359	java/io/File
    //   430: astore 12
    //   432: aload_3
    //   433: getfield 351	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   436: invokevirtual 362	android/net/Uri:getPath	()Ljava/lang/String;
    //   439: astore_3
    //   440: aload 12
    //   442: aload_3
    //   443: invokespecial 363	java/io/File:<init>	(Ljava/lang/String;)V
    //   446: aload 12
    //   448: invokevirtual 366	java/io/File:exists	()Z
    //   451: istore 18
    //   453: iload 18
    //   455: ifeq +9 -> 464
    //   458: aload 12
    //   460: invokevirtual 369	java/io/File:delete	()Z
    //   463: pop
    //   464: aload 8
    //   466: areturn
    //   467: aload_3
    //   468: getfield 351	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   471: astore 8
    //   473: aload 5
    //   475: aload 8
    //   477: invokevirtual 289	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   480: astore 8
    //   482: aload 8
    //   484: ifnonnull +167 -> 651
    //   487: iload 9
    //   489: anewarray 81	java/lang/String
    //   492: astore 12
    //   494: new 91	java/lang/StringBuilder
    //   497: astore 5
    //   499: ldc_w 394
    //   502: astore 7
    //   504: aload 5
    //   506: aload 7
    //   508: invokespecial 97	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   511: aload_3
    //   512: getfield 351	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   515: astore 7
    //   517: aload 5
    //   519: aload 7
    //   521: invokevirtual 397	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   524: pop
    //   525: ldc_w 399
    //   528: astore 7
    //   530: aload 5
    //   532: aload 7
    //   534: invokevirtual 106	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   537: pop
    //   538: ldc_w 401
    //   541: astore 7
    //   543: aload 4
    //   545: aload 7
    //   547: invokevirtual 404	android/content/ContentValues:get	(Ljava/lang/String;)Ljava/lang/Object;
    //   550: astore 4
    //   552: aload 5
    //   554: aload 4
    //   556: invokevirtual 397	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   559: pop
    //   560: aload 5
    //   562: invokevirtual 112	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   565: astore 4
    //   567: aload 12
    //   569: iconst_0
    //   570: aload 4
    //   572: aastore
    //   573: aload 8
    //   575: invokestatic 407	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   578: aconst_null
    //   579: invokestatic 407	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   582: ldc_w 348
    //   585: astore 8
    //   587: aload_3
    //   588: getfield 351	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   591: invokevirtual 354	android/net/Uri:getScheme	()Ljava/lang/String;
    //   594: astore 12
    //   596: aload 8
    //   598: aload 12
    //   600: invokevirtual 357	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   603: istore 13
    //   605: iload 13
    //   607: ifeq +42 -> 649
    //   610: new 359	java/io/File
    //   613: astore 8
    //   615: aload_3
    //   616: getfield 351	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   619: invokevirtual 362	android/net/Uri:getPath	()Ljava/lang/String;
    //   622: astore 12
    //   624: aload 8
    //   626: aload 12
    //   628: invokespecial 363	java/io/File:<init>	(Ljava/lang/String;)V
    //   631: aload 8
    //   633: invokevirtual 366	java/io/File:exists	()Z
    //   636: istore 9
    //   638: iload 9
    //   640: ifeq +9 -> 649
    //   643: aload 8
    //   645: invokevirtual 369	java/io/File:delete	()Z
    //   648: pop
    //   649: aconst_null
    //   650: areturn
    //   651: aload 5
    //   653: aload 11
    //   655: invokevirtual 411	android/content/ContentResolver:openOutputStream	(Landroid/net/Uri;)Ljava/io/OutputStream;
    //   658: astore 5
    //   660: aload 5
    //   662: ifnonnull +162 -> 824
    //   665: iload 9
    //   667: anewarray 81	java/lang/String
    //   670: astore 12
    //   672: new 91	java/lang/StringBuilder
    //   675: astore 7
    //   677: ldc_w 413
    //   680: astore 14
    //   682: aload 7
    //   684: aload 14
    //   686: invokespecial 97	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   689: aload 7
    //   691: aload 11
    //   693: invokevirtual 397	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   696: pop
    //   697: ldc_w 399
    //   700: astore 14
    //   702: aload 7
    //   704: aload 14
    //   706: invokevirtual 106	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   709: pop
    //   710: ldc_w 401
    //   713: astore 14
    //   715: aload 4
    //   717: aload 14
    //   719: invokevirtual 404	android/content/ContentValues:get	(Ljava/lang/String;)Ljava/lang/Object;
    //   722: astore 4
    //   724: aload 7
    //   726: aload 4
    //   728: invokevirtual 397	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   731: pop
    //   732: aload 7
    //   734: invokevirtual 112	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   737: astore 4
    //   739: aload 12
    //   741: iconst_0
    //   742: aload 4
    //   744: aastore
    //   745: aload 8
    //   747: invokestatic 407	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   750: aload 5
    //   752: invokestatic 407	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   755: ldc_w 348
    //   758: astore 8
    //   760: aload_3
    //   761: getfield 351	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   764: invokevirtual 354	android/net/Uri:getScheme	()Ljava/lang/String;
    //   767: astore 12
    //   769: aload 8
    //   771: aload 12
    //   773: invokevirtual 357	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   776: istore 13
    //   778: iload 13
    //   780: ifeq +42 -> 822
    //   783: new 359	java/io/File
    //   786: astore 8
    //   788: aload_3
    //   789: getfield 351	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   792: invokevirtual 362	android/net/Uri:getPath	()Ljava/lang/String;
    //   795: astore 12
    //   797: aload 8
    //   799: aload 12
    //   801: invokespecial 363	java/io/File:<init>	(Ljava/lang/String;)V
    //   804: aload 8
    //   806: invokevirtual 366	java/io/File:exists	()Z
    //   809: istore 9
    //   811: iload 9
    //   813: ifeq +9 -> 822
    //   816: aload 8
    //   818: invokevirtual 369	java/io/File:delete	()Z
    //   821: pop
    //   822: aconst_null
    //   823: areturn
    //   824: aload 8
    //   826: aload 5
    //   828: invokestatic 416	com/truecaller/utils/extensions/m:a	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   831: pop2
    //   832: aload 5
    //   834: invokevirtual 421	java/io/OutputStream:flush	()V
    //   837: aload 8
    //   839: invokestatic 407	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   842: aload 5
    //   844: invokestatic 407	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   847: aload_3
    //   848: getfield 375	com/truecaller/messaging/transport/mms/PduEntity:j	Ljava/lang/String;
    //   851: astore 14
    //   853: iconst_m1
    //   854: istore 19
    //   856: iconst_m1
    //   857: istore 20
    //   859: aload_3
    //   860: getfield 387	com/truecaller/messaging/transport/mms/PduEntity:d	J
    //   863: lstore 21
    //   865: aload 14
    //   867: aload 11
    //   869: iload 19
    //   871: iload 20
    //   873: iconst_0
    //   874: lload 21
    //   876: invokestatic 424	com/truecaller/messaging/data/types/Entity:a	(Ljava/lang/String;Landroid/net/Uri;IIZJ)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   879: astore 8
    //   881: ldc_w 348
    //   884: astore 12
    //   886: aload_3
    //   887: getfield 351	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   890: invokevirtual 354	android/net/Uri:getScheme	()Ljava/lang/String;
    //   893: astore 4
    //   895: aload 12
    //   897: aload 4
    //   899: invokevirtual 357	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   902: istore 9
    //   904: iload 9
    //   906: ifeq +40 -> 946
    //   909: new 359	java/io/File
    //   912: astore 12
    //   914: aload_3
    //   915: getfield 351	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   918: invokevirtual 362	android/net/Uri:getPath	()Ljava/lang/String;
    //   921: astore_3
    //   922: aload 12
    //   924: aload_3
    //   925: invokespecial 363	java/io/File:<init>	(Ljava/lang/String;)V
    //   928: aload 12
    //   930: invokevirtual 366	java/io/File:exists	()Z
    //   933: istore 18
    //   935: iload 18
    //   937: ifeq +9 -> 946
    //   940: aload 12
    //   942: invokevirtual 369	java/io/File:delete	()Z
    //   945: pop
    //   946: aload 8
    //   948: areturn
    //   949: astore 12
    //   951: goto +38 -> 989
    //   954: astore 12
    //   956: goto +123 -> 1079
    //   959: astore 12
    //   961: aconst_null
    //   962: astore 5
    //   964: goto +25 -> 989
    //   967: astore 12
    //   969: iconst_0
    //   970: istore 13
    //   972: aconst_null
    //   973: astore 8
    //   975: goto +104 -> 1079
    //   978: astore 12
    //   980: iconst_0
    //   981: istore 13
    //   983: aconst_null
    //   984: astore 8
    //   986: aconst_null
    //   987: astore 5
    //   989: aload 12
    //   991: invokestatic 428	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   994: aload 8
    //   996: invokestatic 407	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   999: aload 5
    //   1001: invokestatic 407	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   1004: ldc_w 348
    //   1007: astore 8
    //   1009: aload_3
    //   1010: getfield 351	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   1013: invokevirtual 354	android/net/Uri:getScheme	()Ljava/lang/String;
    //   1016: astore 12
    //   1018: aload 8
    //   1020: aload 12
    //   1022: invokevirtual 357	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1025: istore 13
    //   1027: iload 13
    //   1029: ifeq +42 -> 1071
    //   1032: new 359	java/io/File
    //   1035: astore 8
    //   1037: aload_3
    //   1038: getfield 351	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   1041: invokevirtual 362	android/net/Uri:getPath	()Ljava/lang/String;
    //   1044: astore 12
    //   1046: aload 8
    //   1048: aload 12
    //   1050: invokespecial 363	java/io/File:<init>	(Ljava/lang/String;)V
    //   1053: aload 8
    //   1055: invokevirtual 366	java/io/File:exists	()Z
    //   1058: istore 9
    //   1060: iload 9
    //   1062: ifeq +9 -> 1071
    //   1065: aload 8
    //   1067: invokevirtual 369	java/io/File:delete	()Z
    //   1070: pop
    //   1071: aconst_null
    //   1072: areturn
    //   1073: astore 12
    //   1075: aload 5
    //   1077: astore 6
    //   1079: aload 8
    //   1081: invokestatic 407	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   1084: aload 6
    //   1086: invokestatic 407	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   1089: aload 12
    //   1091: athrow
    //   1092: astore 8
    //   1094: aload 8
    //   1096: invokestatic 428	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   1099: ldc_w 348
    //   1102: astore 8
    //   1104: aload_3
    //   1105: getfield 351	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   1108: invokevirtual 354	android/net/Uri:getScheme	()Ljava/lang/String;
    //   1111: astore 12
    //   1113: aload 8
    //   1115: aload 12
    //   1117: invokevirtual 357	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1120: istore 13
    //   1122: iload 13
    //   1124: ifeq +42 -> 1166
    //   1127: new 359	java/io/File
    //   1130: astore 8
    //   1132: aload_3
    //   1133: getfield 351	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   1136: invokevirtual 362	android/net/Uri:getPath	()Ljava/lang/String;
    //   1139: astore 12
    //   1141: aload 8
    //   1143: aload 12
    //   1145: invokespecial 363	java/io/File:<init>	(Ljava/lang/String;)V
    //   1148: aload 8
    //   1150: invokevirtual 366	java/io/File:exists	()Z
    //   1153: istore 9
    //   1155: iload 9
    //   1157: ifeq +9 -> 1166
    //   1160: aload 8
    //   1162: invokevirtual 369	java/io/File:delete	()Z
    //   1165: pop
    //   1166: aconst_null
    //   1167: areturn
    //   1168: astore 8
    //   1170: ldc_w 348
    //   1173: astore 12
    //   1175: aload_3
    //   1176: getfield 351	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   1179: invokevirtual 354	android/net/Uri:getScheme	()Ljava/lang/String;
    //   1182: astore 4
    //   1184: aload 12
    //   1186: aload 4
    //   1188: invokevirtual 357	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1191: istore 9
    //   1193: iload 9
    //   1195: ifeq +40 -> 1235
    //   1198: new 359	java/io/File
    //   1201: astore 12
    //   1203: aload_3
    //   1204: getfield 351	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   1207: invokevirtual 362	android/net/Uri:getPath	()Ljava/lang/String;
    //   1210: astore_3
    //   1211: aload 12
    //   1213: aload_3
    //   1214: invokespecial 363	java/io/File:<init>	(Ljava/lang/String;)V
    //   1217: aload 12
    //   1219: invokevirtual 366	java/io/File:exists	()Z
    //   1222: istore 18
    //   1224: iload 18
    //   1226: ifeq +9 -> 1235
    //   1229: aload 12
    //   1231: invokevirtual 369	java/io/File:delete	()Z
    //   1234: pop
    //   1235: aload 8
    //   1237: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1238	0	this	ao
    //   0	1238	1	paramLong	long
    //   0	1238	3	paramPduEntity	PduEntity
    //   3	1184	4	localObject1	Object
    //   14	1062	5	localObject2	Object
    //   42	1043	6	localObject3	Object
    //   47	686	7	localObject4	Object
    //   66	1014	8	localObject5	Object
    //   1092	3	8	localRuntimeException	RuntimeException
    //   1102	59	8	localObject6	Object
    //   1168	68	8	localObject7	Object
    //   91	1103	9	bool1	boolean
    //   108	33	10	str1	String
    //   160	708	11	localUri	Uri
    //   179	762	12	localObject8	Object
    //   949	1	12	localIOException1	IOException
    //   954	1	12	localObject9	Object
    //   959	1	12	localIOException2	IOException
    //   967	1	12	localObject10	Object
    //   978	12	12	localIOException3	IOException
    //   1016	33	12	str2	String
    //   1073	17	12	localObject11	Object
    //   1111	119	12	localObject12	Object
    //   188	935	13	bool2	boolean
    //   246	620	14	localObject13	Object
    //   251	5	15	str3	String
    //   386	7	16	l1	long
    //   451	774	18	bool3	boolean
    //   854	16	19	i1	int
    //   857	15	20	i2	int
    //   863	12	21	l2	long
    // Exception table:
    //   from	to	target	type
    //   665	670	949	java/io/IOException
    //   672	675	949	java/io/IOException
    //   684	689	949	java/io/IOException
    //   691	697	949	java/io/IOException
    //   704	710	949	java/io/IOException
    //   717	722	949	java/io/IOException
    //   726	732	949	java/io/IOException
    //   732	737	949	java/io/IOException
    //   742	745	949	java/io/IOException
    //   826	832	949	java/io/IOException
    //   832	837	949	java/io/IOException
    //   487	492	954	finally
    //   494	497	954	finally
    //   506	511	954	finally
    //   511	515	954	finally
    //   519	525	954	finally
    //   532	538	954	finally
    //   545	550	954	finally
    //   554	560	954	finally
    //   560	565	954	finally
    //   570	573	954	finally
    //   653	658	954	finally
    //   487	492	959	java/io/IOException
    //   494	497	959	java/io/IOException
    //   506	511	959	java/io/IOException
    //   511	515	959	java/io/IOException
    //   519	525	959	java/io/IOException
    //   532	538	959	java/io/IOException
    //   545	550	959	java/io/IOException
    //   554	560	959	java/io/IOException
    //   560	565	959	java/io/IOException
    //   570	573	959	java/io/IOException
    //   653	658	959	java/io/IOException
    //   467	471	967	finally
    //   475	480	967	finally
    //   467	471	978	java/io/IOException
    //   475	480	978	java/io/IOException
    //   665	670	1073	finally
    //   672	675	1073	finally
    //   684	689	1073	finally
    //   691	697	1073	finally
    //   704	710	1073	finally
    //   717	722	1073	finally
    //   726	732	1073	finally
    //   732	737	1073	finally
    //   742	745	1073	finally
    //   826	832	1073	finally
    //   832	837	1073	finally
    //   989	994	1073	finally
    //   155	160	1092	java/lang/RuntimeException
    //   0	3	1168	finally
    //   5	10	1168	finally
    //   10	14	1168	finally
    //   19	26	1168	finally
    //   26	30	1168	finally
    //   32	37	1168	finally
    //   39	42	1168	finally
    //   51	56	1168	finally
    //   58	63	1168	finally
    //   70	76	1168	finally
    //   76	81	1168	finally
    //   83	88	1168	finally
    //   93	98	1168	finally
    //   100	103	1168	finally
    //   112	117	1168	finally
    //   117	122	1168	finally
    //   126	132	1168	finally
    //   132	137	1168	finally
    //   145	148	1168	finally
    //   155	160	1168	finally
    //   236	241	1168	finally
    //   243	246	1168	finally
    //   255	260	1168	finally
    //   260	265	1168	finally
    //   269	275	1168	finally
    //   282	288	1168	finally
    //   288	292	1168	finally
    //   296	302	1168	finally
    //   309	315	1168	finally
    //   315	320	1168	finally
    //   324	330	1168	finally
    //   330	335	1168	finally
    //   340	343	1168	finally
    //   350	355	1168	finally
    //   362	366	1168	finally
    //   375	380	1168	finally
    //   382	386	1168	finally
    //   392	397	1168	finally
    //   573	578	1168	finally
    //   578	582	1168	finally
    //   745	750	1168	finally
    //   750	755	1168	finally
    //   837	842	1168	finally
    //   842	847	1168	finally
    //   847	851	1168	finally
    //   859	863	1168	finally
    //   874	879	1168	finally
    //   994	999	1168	finally
    //   999	1004	1168	finally
    //   1079	1084	1168	finally
    //   1084	1089	1168	finally
    //   1089	1092	1168	finally
    //   1094	1099	1168	finally
  }
  
  private Message a(Intent paramIntent)
  {
    byte[] arrayOfByte = paramIntent.getByteArrayExtra("data");
    if (arrayOfByte == null)
    {
      AssertionUtil.reportWeirdnessButNeverCrash("Got new MMS intent without PDU");
      return null;
    }
    paramIntent = k.a(paramIntent);
    Object localObject = "-1";
    boolean bool1 = ((String)localObject).equals(paramIntent);
    if (bool1) {
      paramIntent = k.f();
    }
    Intent localIntent = paramIntent;
    paramIntent = k.c(localIntent);
    localObject = new com/android/a/a/a/n;
    boolean bool2 = paramIntent.c();
    ((n)localObject).<init>(arrayOfByte, bool2);
    com.android.a.a.a.f localf = ((n)localObject).a();
    if (localf == null)
    {
      new String[1][0] = "Invalid PDU data";
      return null;
    }
    localObject = o;
    boolean bool3 = c(localIntent);
    return ((h)localObject).a(localf, bool3, localIntent, -1);
  }
  
  private Message a(Message paramMessage, MmsTransportInfo paramMmsTransportInfo, boolean paramBoolean)
  {
    int i1 = g;
    int i2 = 0;
    Object localObject1 = null;
    Object localObject2;
    Object localObject3;
    boolean bool;
    switch (i1)
    {
    case 131: 
    default: 
      paramMessage = null;
      break;
    case 132: 
      paramMessage = c(paramMessage, paramMmsTransportInfo);
      if (paramMessage == null) {
        return null;
      }
      paramMmsTransportInfo = (MmsTransportInfo)m;
      localObject2 = e;
      if (localObject2 == null) {
        return null;
      }
      localObject2 = e;
      localObject2 = b((Uri)localObject2);
      if (localObject2 != null) {
        b((org.a.a.b)localObject2);
      }
      if (paramBoolean)
      {
        localObject3 = l;
        if (localObject3 != null)
        {
          localObject3 = l;
          localObject2 = l;
          localObject1 = k;
          localObject3 = ((ak)localObject3).a((String)localObject2, (com.truecaller.multisim.h)localObject1);
          bool = D;
          if (bool)
          {
            localObject3 = (ai)((com.truecaller.androidactors.f)localObject3).a();
            localObject2 = o.getBytes();
            paramMmsTransportInfo = l;
            i2 = 129;
            ((ai)localObject3).a((byte[])localObject2, paramMmsTransportInfo, i2);
          }
          else
          {
            localObject3 = (ai)((com.truecaller.androidactors.f)localObject3).a();
            localObject2 = o.getBytes();
            paramMmsTransportInfo = l;
            ((ai)localObject3).a((byte[])localObject2, paramMmsTransportInfo);
          }
        }
      }
      break;
    case 130: 
      paramMessage = b(paramMessage, paramMmsTransportInfo);
      if (paramMessage != null)
      {
        paramMmsTransportInfo = (MmsTransportInfo)m;
        localObject2 = e;
        b((org.a.a.b)localObject2);
        localObject2 = l;
        if (localObject2 == null)
        {
          paramMmsTransportInfo = "Received Notification pdu without location uri.Have no idea what to do with it.";
          AssertionUtil.reportWeirdnessButNeverCrash(paramMmsTransportInfo);
        }
        else
        {
          localObject2 = l;
          bool = c((String)localObject2);
          if (bool)
          {
            localObject2 = e;
            if (localObject2 != null)
            {
              paramBoolean = a(paramMessage, false);
              if (!paramBoolean)
              {
                localObject3 = new android/content/ContentValues;
                ((ContentValues)localObject3).<init>();
                int i3 = 194;
                Object localObject4 = Integer.valueOf(i3);
                ((ContentValues)localObject3).put("retr_st", (Integer)localObject4);
                localObject4 = Integer.valueOf(0);
                ((ContentValues)localObject3).put("seen", (Integer)localObject4);
                localObject2 = h.getContentResolver();
                localObject4 = e;
                ((ContentResolver)localObject2).update((Uri)localObject4, (ContentValues)localObject3, null, null);
                localObject3 = (com.truecaller.messaging.notifications.a)r.a();
                ((com.truecaller.messaging.notifications.a)localObject3).d();
              }
              paramBoolean = false;
              localObject3 = null;
            }
          }
          if (paramBoolean)
          {
            localObject3 = l;
            localObject2 = l;
            localObject1 = k;
            localObject3 = (ai)((ak)localObject3).a((String)localObject2, (com.truecaller.multisim.h)localObject1).a();
            localObject2 = o.getBytes();
            paramMmsTransportInfo = l;
            i2 = 131;
            ((ai)localObject3).a((byte[])localObject2, paramMmsTransportInfo, i2);
          }
        }
      }
      break;
    case 129: 
      paramMessage = e(paramMessage, paramMmsTransportInfo);
      if (paramMessage == null) {
        return null;
      }
      paramMmsTransportInfo = (MmsTransportInfo)m;
      localObject3 = e;
      if (localObject3 == null) {
        return null;
      }
      paramMmsTransportInfo = e;
      paramMmsTransportInfo = b(paramMmsTransportInfo);
      if (paramMmsTransportInfo != null) {
        b(paramMmsTransportInfo);
      }
      break;
    case 128: 
      localObject1 = d(paramMessage, paramMmsTransportInfo);
      if (localObject1 != null)
      {
        paramMessage = e;
        b(paramMessage);
      }
      paramMessage = (Message)localObject1;
    }
    return paramMessage;
  }
  
  private List a(Uri paramUri)
  {
    long l1 = ContentUris.parseId(paramUri);
    paramUri = null;
    long l2 = -1;
    boolean bool1 = l1 < l2;
    if (!bool1) {
      return null;
    }
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>("content://mms/");
    ((StringBuilder)localObject1).append(l1);
    ((StringBuilder)localObject1).append("/addr");
    Object localObject2 = ((StringBuilder)localObject1).toString();
    Uri localUri = Uri.parse((String)localObject2);
    try
    {
      localObject2 = h;
      localObject1 = ((Context)localObject2).getContentResolver();
      String[] arrayOfString = f;
      String str = g;
      paramUri = ((ContentResolver)localObject1).query(localUri, arrayOfString, str, null, null);
      if (paramUri != null) {
        for (;;)
        {
          boolean bool2 = paramUri.moveToNext();
          if (!bool2) {
            break;
          }
          bool2 = false;
          localObject2 = null;
          int i1 = paramUri.getInt(0);
          int i2 = 1;
          Object localObject4 = paramUri.getString(i2);
          if (localObject4 != null)
          {
            localObject4 = ((String)localObject4).getBytes();
            localObject4 = com.android.a.a.c.a((byte[])localObject4, i1);
          }
          localArrayList.add(localObject4);
        }
      }
      return localArrayList;
    }
    finally
    {
      com.truecaller.util.q.a(paramUri);
    }
  }
  
  private void a(Uri paramUri, SparseArray paramSparseArray)
  {
    long l1 = ContentUris.parseId(paramUri);
    long l2 = -1;
    boolean bool1 = l1 < l2;
    if (bool1)
    {
      int i1 = paramSparseArray.size();
      if (i1 != 0)
      {
        paramUri = new java/lang/StringBuilder;
        paramUri.<init>("content://mms/");
        paramUri.append(l1);
        paramUri.append("/addr");
        paramUri = Uri.parse(paramUri.toString());
        ContentResolver localContentResolver = h.getContentResolver();
        localContentResolver.delete(paramUri, null, null);
        ContentValues localContentValues = new android/content/ContentValues;
        localContentValues.<init>();
        int i2 = 0;
        for (;;)
        {
          int i3 = paramSparseArray.size();
          if (i2 >= i3) {
            break;
          }
          int i4 = paramSparseArray.keyAt(i2);
          Object localObject = Integer.valueOf(i4);
          localContentValues.put("type", (Integer)localObject);
          Iterator localIterator = ((Set)paramSparseArray.valueAt(i2)).iterator();
          for (;;)
          {
            boolean bool2 = localIterator.hasNext();
            if (!bool2) {
              break;
            }
            localObject = (String)localIterator.next();
            localContentValues.put("address", (String)localObject);
            localObject = "charset";
            int i5 = 106;
            Integer localInteger = Integer.valueOf(i5);
            localContentValues.put((String)localObject, localInteger);
            localContentResolver.insert(paramUri, localContentValues);
          }
          i2 += 1;
        }
        return;
      }
    }
  }
  
  private boolean a(Message paramMessage, boolean paramBoolean)
  {
    MmsTransportInfo localMmsTransportInfo = (MmsTransportInfo)m;
    ??? = null;
    Object localObject2 = new String[0];
    AssertionUtil.AlwaysFatal.isNotNull(localMmsTransportInfo, (String[])localObject2);
    long l1 = a;
    boolean bool1 = true;
    long l2 = -1;
    boolean bool2 = l1 < l2;
    if (bool2)
    {
      bool3 = true;
    }
    else
    {
      bool3 = false;
      localObject2 = null;
    }
    Object localObject3 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(bool3, (String[])localObject3);
    boolean bool3 = o.isEmpty();
    localObject3 = new String[0];
    AssertionUtil.AlwaysFatal.isFalse(bool3, (String[])localObject3);
    localObject2 = l;
    if (localObject2 == null)
    {
      AssertionUtil.reportWeirdnessButNeverCrash("Should never try to download MMS content without content uri");
      return false;
    }
    localObject2 = k;
    localObject3 = l;
    int i1 = ((com.truecaller.multisim.h)localObject2).g((String)localObject3);
    int i2 = 2;
    if (i1 == i2)
    {
      new String[1][0] = "Mobile data is disabled. Skip MMS data download";
      return false;
    }
    synchronized (q)
    {
      localObject2 = q;
      l2 = b;
      localObject3 = Long.valueOf(l2);
      boolean bool4 = ((Set)localObject2).contains(localObject3);
      if (bool4) {
        return bool1;
      }
      localObject2 = q;
      l2 = b;
      localObject3 = Long.valueOf(l2);
      ((Set)localObject2).add(localObject3);
      ??? = l;
      paramMessage = l;
      localObject2 = k;
      paramMessage = ((ak)???).a(paramMessage, (com.truecaller.multisim.h)localObject2).a();
      Object localObject4 = paramMessage;
      localObject4 = (ai)paramMessage;
      long l3 = b;
      byte[] arrayOfByte = o.getBytes();
      Uri localUri = l;
      boolean bool5 = paramBoolean ^ true;
      ((ai)localObject4).a(l3, arrayOfByte, localUri, bool5);
      return bool1;
    }
  }
  
  private boolean a(MmsTransportInfo paramMmsTransportInfo)
  {
    int i1 = g;
    int i2 = 130;
    if (i1 == i2)
    {
      Object localObject1 = l;
      if (localObject1 != null)
      {
        ContentResolver localContentResolver = h.getContentResolver();
        i1 = 0;
        localObject1 = null;
        try
        {
          Object localObject2 = org.a.a.b.ay_();
          int i3 = 7;
          Object localObject3 = ((org.a.a.b)localObject2).c(i3);
          Object localObject4 = ((org.a.a.b)localObject2).a(i3);
          Object localObject5 = b;
          Object localObject6 = "_id";
          localObject6 = new String[] { localObject6 };
          String str = "((m_type<>130) OR (exp>?)) AND (date>?) AND (date<?) AND (ct_l=?)";
          int i4 = 4;
          String[] arrayOfString = new String[i4];
          long l1 = a;
          long l2 = 1000L;
          l1 /= l2;
          localObject2 = String.valueOf(l1);
          arrayOfString[0] = localObject2;
          int i5 = 1;
          l1 = a;
          l1 /= l2;
          localObject3 = String.valueOf(l1);
          arrayOfString[i5] = localObject3;
          i5 = 2;
          long l3 = a;
          l3 /= l2;
          localObject4 = String.valueOf(l3);
          arrayOfString[i5] = localObject4;
          i5 = 3;
          paramMmsTransportInfo = l;
          paramMmsTransportInfo = paramMmsTransportInfo.toString();
          arrayOfString[i5] = paramMmsTransportInfo;
          boolean bool = false;
          paramMmsTransportInfo = null;
          localObject2 = localObject5;
          localObject4 = localObject6;
          localObject3 = str;
          localObject5 = arrayOfString;
          localObject6 = null;
          str = null;
          localObject1 = localContentResolver.query((Uri)localObject2, (String[])localObject4, (String)localObject3, arrayOfString, null, null);
          if (localObject1 != null)
          {
            bool = ((Cursor)localObject1).moveToFirst();
            return bool;
          }
          return false;
        }
        finally
        {
          if (localObject1 != null) {
            ((Cursor)localObject1).close();
          }
        }
      }
    }
    return false;
  }
  
  private boolean a(ap paramap)
  {
    boolean bool = d;
    if (!bool)
    {
      new String[1][0] = "SMS permissions is not granted";
      return false;
    }
    try
    {
      ad.b localb = t;
      paramap = localb.a(paramap);
      int i1 = paramap.length;
      return i1 != 0;
    }
    catch (OperationApplicationException paramap) {}catch (SecurityException paramap) {}catch (RemoteException paramap) {}
    AssertionUtil.reportThrowableButNeverCrash(paramap);
    return false;
  }
  
  private Message b(Message paramMessage, MmsTransportInfo paramMmsTransportInfo)
  {
    Object localObject1 = new android/content/ContentValues;
    ((ContentValues)localObject1).<init>();
    long l1 = a(paramMessage, paramMmsTransportInfo);
    int i1 = 1;
    long l2 = -1;
    boolean bool1 = l1 < l2;
    if (!bool1)
    {
      paramMmsTransportInfo = new String[i1];
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("For some reasons we can not create thread for address ");
      paramMessage = c.f;
      ((StringBuilder)localObject1).append(paramMessage);
      paramMessage = ((StringBuilder)localObject1).toString();
      paramMmsTransportInfo[0] = paramMessage;
      AssertionUtil.OnlyInDebug.fail(paramMmsTransportInfo);
      return null;
    }
    Object localObject2 = Integer.valueOf(0);
    ((ContentValues)localObject1).put("read", (Integer)localObject2);
    localObject2 = Integer.valueOf(0);
    ((ContentValues)localObject1).put("locked", (Integer)localObject2);
    localObject2 = Boolean.valueOf(g);
    ((ContentValues)localObject1).put("seen", (Boolean)localObject2);
    localObject2 = Long.valueOf(l1);
    ((ContentValues)localObject1).put("thread_id", (Long)localObject2);
    long l3 = e.a;
    long l4 = 1000L;
    localObject2 = Long.valueOf(l3 / l4);
    ((ContentValues)localObject1).put("date", (Long)localObject2);
    l3 = d.a / l4;
    localObject2 = Long.valueOf(l3);
    ((ContentValues)localObject1).put("date_sent", (Long)localObject2);
    localObject2 = v;
    ((ContentValues)localObject1).put("m_id", (String)localObject2);
    localObject2 = h;
    ((ContentValues)localObject1).put("sub", (String)localObject2);
    int i2 = i;
    localObject2 = Integer.valueOf(i2);
    ((ContentValues)localObject1).put("sub_cs", (Integer)localObject2);
    localObject2 = m;
    ((ContentValues)localObject1).put("ct_t", (String)localObject2);
    Object localObject3 = l;
    if (localObject3 != null)
    {
      localObject3 = "ct_l";
      localObject2 = l.toString();
      ((ContentValues)localObject1).put((String)localObject3, (String)localObject2);
    }
    localObject3 = p;
    l2 = a;
    long l5 = 0L;
    bool1 = l2 < l5;
    if (bool1)
    {
      String str = "exp";
      l2 /= l4;
      localObject3 = Long.valueOf(l2);
      ((ContentValues)localObject1).put(str, (Long)localObject3);
    }
    localObject2 = u;
    ((ContentValues)localObject1).put("m_cls", (String)localObject2);
    localObject2 = Integer.valueOf(g);
    ((ContentValues)localObject1).put("m_type", (Integer)localObject2);
    localObject2 = Integer.valueOf(f);
    ((ContentValues)localObject1).put("v", (Integer)localObject2);
    localObject2 = Integer.valueOf(x);
    ((ContentValues)localObject1).put("m_size", (Integer)localObject2);
    localObject2 = Integer.valueOf(q);
    ((ContentValues)localObject1).put("pri", (Integer)localObject2);
    localObject3 = "rr";
    i2 = A;
    localObject2 = Integer.valueOf(i2);
    ((ContentValues)localObject1).put((String)localObject3, (Integer)localObject2);
    boolean bool3 = C;
    if (bool3)
    {
      localObject3 = "rpt_a";
      localObject2 = Integer.valueOf(i1);
      ((ContentValues)localObject1).put((String)localObject3, (Integer)localObject2);
    }
    int i3 = s;
    if (i3 > 0)
    {
      localObject3 = "resp_st";
      i2 = s;
      localObject2 = Integer.valueOf(i2);
      ((ContentValues)localObject1).put((String)localObject3, (Integer)localObject2);
    }
    i3 = c;
    if (i3 > 0)
    {
      localObject3 = "st";
      i2 = c;
      localObject2 = Integer.valueOf(i2);
      ((ContentValues)localObject1).put((String)localObject3, (Integer)localObject2);
    }
    localObject3 = "tr_id";
    localObject2 = o;
    ((ContentValues)localObject1).put((String)localObject3, (String)localObject2);
    i3 = r;
    if (i3 > 0)
    {
      localObject3 = "retr_st";
      i2 = r;
      localObject2 = Integer.valueOf(i2);
      ((ContentValues)localObject1).put((String)localObject3, (Integer)localObject2);
    }
    localObject3 = j;
    boolean bool4 = TextUtils.isEmpty((CharSequence)localObject3);
    if (!bool4)
    {
      localObject2 = j;
      ((ContentValues)localObject1).put("retr_txt", (String)localObject2);
      localObject3 = "retr_txt_cs";
      i2 = k;
      localObject2 = Integer.valueOf(i2);
      ((ContentValues)localObject1).put((String)localObject3, (Integer)localObject2);
    }
    int i4 = B;
    if (i4 > 0)
    {
      localObject3 = "read_status";
      i2 = B;
      localObject2 = Integer.valueOf(i2);
      ((ContentValues)localObject1).put((String)localObject3, (Integer)localObject2);
    }
    i2 = n;
    localObject2 = Integer.valueOf(i2);
    ((ContentValues)localObject1).put("ct_cls", (Integer)localObject2);
    localObject3 = "resp_txt";
    localObject2 = t;
    ((ContentValues)localObject1).put((String)localObject3, (String)localObject2);
    l2 = z;
    bool1 = l2 < l5;
    if (bool1)
    {
      localObject3 = "d_tm";
      l3 = z;
      localObject2 = Long.valueOf(l3);
      ((ContentValues)localObject1).put((String)localObject3, (Long)localObject2);
    }
    localObject2 = Integer.valueOf(y);
    ((ContentValues)localObject1).put("d_rpt", (Integer)localObject2);
    boolean bool2 = paramMessage.d() ^ i1;
    localObject2 = Boolean.valueOf(bool2);
    ((ContentValues)localObject1).put("text_only", (Boolean)localObject2);
    localObject3 = k.c();
    if (localObject3 != null)
    {
      localObject2 = l;
      ((ContentValues)localObject1).put((String)localObject3, (String)localObject2);
    }
    try
    {
      localObject3 = h;
      localObject3 = ((Context)localObject3).getContentResolver();
      localObject2 = Telephony.Mms.Inbox.CONTENT_URI;
      localObject1 = ((ContentResolver)localObject3).insert((Uri)localObject2, (ContentValues)localObject1);
      if (localObject1 == null) {
        return null;
      }
      l2 = ContentUris.parseId((Uri)localObject1);
      paramMmsTransportInfo = paramMmsTransportInfo.g();
      d = l1;
      b = l2;
      e = ((Uri)localObject1);
      paramMmsTransportInfo = paramMmsTransportInfo.a();
      paramMessage = paramMessage.m();
      paramMessage = paramMessage.a(i1, paramMmsTransportInfo);
      paramMessage = paramMessage.b();
      SparseArray localSparseArray = E;
      String[] arrayOfString = new String[0];
      AssertionUtil.AlwaysFatal.isNotNull(localSparseArray, arrayOfString);
      paramMmsTransportInfo = E;
      a((Uri)localObject1, paramMmsTransportInfo);
      return paramMessage;
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localRuntimeException;
    }
    return null;
  }
  
  private org.a.a.b b(Uri paramUri)
  {
    Object localObject1 = null;
    try
    {
      Object localObject2 = h;
      ContentResolver localContentResolver = ((Context)localObject2).getContentResolver();
      localObject2 = "date";
      String[] arrayOfString = { localObject2 };
      paramUri = localContentResolver.query(paramUri, arrayOfString, null, null, null);
      if (paramUri != null) {
        try
        {
          boolean bool = paramUri.moveToFirst();
          if (bool)
          {
            localObject1 = new org/a/a/b;
            bool = false;
            localObject2 = null;
            long l1 = paramUri.getLong(0);
            long l2 = 1000L;
            l1 *= l2;
            ((org.a.a.b)localObject1).<init>(l1);
            if (paramUri != null) {
              paramUri.close();
            }
            return (org.a.a.b)localObject1;
          }
        }
        finally
        {
          localObject1 = paramUri;
          paramUri = (Uri)localObject3;
          break label126;
        }
      }
      if (paramUri != null) {
        paramUri.close();
      }
      return null;
    }
    finally
    {
      label126:
      if (localObject1 != null) {
        ((Cursor)localObject1).close();
      }
    }
  }
  
  private void b(String paramString)
  {
    Object localObject1 = new com/truecaller/analytics/e$a;
    ((e.a)localObject1).<init>("MessageDownload");
    String str = "mms";
    paramString = ((e.a)localObject1).a("Type", str).a("Status", paramString);
    localObject1 = "Sim";
    Object localObject2 = k;
    boolean bool = ((com.truecaller.multisim.h)localObject2).j();
    if (bool) {
      localObject2 = "Multi";
    } else {
      localObject2 = "Single";
    }
    paramString = paramString.a((String)localObject1, (String)localObject2).a();
    u.a(paramString);
  }
  
  private void b(org.a.a.b paramb)
  {
    com.truecaller.messaging.h localh = i;
    int i1 = 1;
    long l1 = localh.a(i1);
    paramb = paramb.f();
    boolean bool = paramb.d(l1);
    if (bool)
    {
      localh = i;
      l1 = a;
      localh.a(i1, l1);
    }
  }
  
  private static boolean b(MmsTransportInfo paramMmsTransportInfo)
  {
    int i1 = r;
    int i2 = 192;
    if (i1 >= i2)
    {
      int i3 = r;
      i1 = 255;
      if (i3 < i1) {
        return true;
      }
    }
    return false;
  }
  
  private Message c(Message paramMessage, MmsTransportInfo paramMmsTransportInfo)
  {
    Object localObject1 = h.getContentResolver();
    Object localObject2 = e;
    int i1 = 0;
    Object localObject3 = new String[0];
    AssertionUtil.AlwaysFatal.isNotNull(localObject2, (String[])localObject3);
    localObject2 = new android/content/ContentValues;
    ((ContentValues)localObject2).<init>();
    Object localObject4 = Integer.valueOf(0);
    ((ContentValues)localObject2).put("read", (Integer)localObject4);
    localObject4 = Integer.valueOf(0);
    ((ContentValues)localObject2).put("seen", (Integer)localObject4);
    localObject4 = Integer.valueOf(r);
    ((ContentValues)localObject2).put("retr_st", (Integer)localObject4);
    localObject4 = j;
    ((ContentValues)localObject2).put("retr_txt", (String)localObject4);
    localObject3 = paramMessage.m();
    boolean bool1 = b(paramMmsTransportInfo);
    Object localObject5;
    long l2;
    if (!bool1)
    {
      localObject5 = v;
      ((ContentValues)localObject2).put("m_id", (String)localObject5);
      localObject5 = Integer.valueOf(g);
      ((ContentValues)localObject2).put("m_type", (Integer)localObject5);
      localObject5 = o;
      ((ContentValues)localObject2).put("tr_id", (String)localObject5);
      localObject5 = h;
      ((ContentValues)localObject2).put("sub", (String)localObject5);
      localObject5 = Integer.valueOf(i);
      ((ContentValues)localObject2).put("sub_cs", (Integer)localObject5);
      localObject5 = Integer.valueOf(0);
      ((ContentValues)localObject2).put("exp", (Integer)localObject5);
      localObject5 = Integer.valueOf(x);
      ((ContentValues)localObject2).put("m_size", (Integer)localObject5);
      localObject5 = Integer.valueOf(q);
      ((ContentValues)localObject2).put("pri", (Integer)localObject5);
      ((ContentValues)localObject2).putNull("ct_l");
      localObject5 = m;
      ((ContentValues)localObject2).put("ct_t", (String)localObject5);
      localObject5 = u;
      ((ContentValues)localObject2).put("m_cls", (String)localObject5);
      localObject5 = Integer.valueOf(s);
      ((ContentValues)localObject2).put("resp_st", (Integer)localObject5);
      localObject5 = t;
      ((ContentValues)localObject2).put("resp_txt", (String)localObject5);
      int i3 = y;
      localObject5 = Integer.valueOf(i3);
      ((ContentValues)localObject2).put("d_rpt", (Integer)localObject5);
      long l1 = z;
      localObject5 = Long.valueOf(l1);
      ((ContentValues)localObject2).put("d_tm", (Long)localObject5);
      localObject4 = k.c();
      if (localObject4 != null)
      {
        localObject5 = l;
        ((ContentValues)localObject2).put((String)localObject4, (String)localObject5);
      }
      localObject4 = E;
      if (localObject4 != null)
      {
        localObject4 = E;
        i2 = ((SparseArray)localObject4).size();
        if (i2 > 0)
        {
          l2 = a(paramMessage, paramMmsTransportInfo);
          long l3 = -1;
          boolean bool3 = l2 < l3;
          if (bool3)
          {
            String str = "thread_id";
            Long localLong = Long.valueOf(l2);
            ((ContentValues)localObject2).put(str, localLong);
            paramMmsTransportInfo = paramMmsTransportInfo.g();
            d = l2;
            paramMmsTransportInfo = paramMmsTransportInfo.a();
          }
        }
      }
    }
    int i2 = 0;
    localObject4 = null;
    try
    {
      localObject5 = e;
      int i4 = ((ContentResolver)localObject1).update((Uri)localObject5, (ContentValues)localObject2, null, null);
      if (i4 == 0) {
        return null;
      }
      i4 = 1;
      ((Message.a)localObject3).a(i4, paramMmsTransportInfo);
      localObject1 = e;
      if (localObject1 != null)
      {
        localObject1 = E;
        if (localObject1 != null)
        {
          localObject1 = E;
          i4 = ((SparseArray)localObject1).size();
          if (i4 > 0)
          {
            localObject1 = e;
            localObject2 = E;
            a((Uri)localObject1, (SparseArray)localObject2);
          }
        }
      }
      boolean bool4 = paramMessage.c();
      if (!bool4) {
        return ((Message.a)localObject3).b();
      }
      ((Message.a)localObject3).a();
      paramMessage = n;
      int i5 = paramMessage.length;
      while (i1 < i5)
      {
        localObject2 = paramMessage[i1];
        boolean bool2 = localObject2 instanceof PduEntity;
        localObject5 = new String[] { "Only new received messages should be processed by this method" };
        AssertionUtil.AlwaysFatal.isTrue(bool2, (String[])localObject5);
        l2 = b;
        localObject2 = (PduEntity)localObject2;
        localObject2 = a(l2, (PduEntity)localObject2);
        if (localObject2 != null) {
          ((Message.a)localObject3).a((Entity)localObject2);
        }
        i1 += 1;
      }
      return ((Message.a)localObject3).b();
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localRuntimeException;
    }
    return null;
  }
  
  private boolean c(long paramLong)
  {
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    int i1 = 5;
    Object localObject = Integer.valueOf(i1);
    localContentValues.put("msg_box", (Integer)localObject);
    ContentResolver localContentResolver = h.getContentResolver();
    localObject = b;
    String str1 = "_id=?";
    boolean bool = true;
    String[] arrayOfString = new String[bool];
    String str2 = String.valueOf(paramLong);
    arrayOfString[0] = str2;
    int i2 = localContentResolver.update((Uri)localObject, localContentValues, str1, arrayOfString);
    if (i2 > 0) {
      return bool;
    }
    return false;
  }
  
  private boolean c(String paramString)
  {
    paramString = k.b(paramString);
    if (paramString == null) {
      return false;
    }
    Object localObject = i;
    int i1 = a;
    boolean bool1 = ((com.truecaller.messaging.h)localObject).c(i1);
    if (!bool1) {
      return false;
    }
    localObject = p;
    bool1 = ((TelephonyManager)localObject).isNetworkRoaming();
    if (bool1)
    {
      localObject = i;
      int i2 = a;
      boolean bool2 = ((com.truecaller.messaging.h)localObject).d(i2);
      if (!bool2) {
        return false;
      }
    }
    return true;
  }
  
  private Message d(Message paramMessage, MmsTransportInfo paramMmsTransportInfo)
  {
    ao localao = this;
    Object localObject1 = paramMessage;
    Object localObject2 = paramMmsTransportInfo;
    Object localObject3 = new android/content/ContentValues;
    ((ContentValues)localObject3).<init>();
    Object localObject4 = k;
    Object localObject5 = l;
    localObject4 = ((com.truecaller.multisim.h)localObject4).c((String)localObject5);
    localObject5 = o;
    Object localObject6 = n;
    localObject4 = ((h)localObject5).a((Entity[])localObject6, (com.truecaller.multisim.a)localObject4);
    boolean bool1 = false;
    localObject5 = null;
    if (localObject4 == null) {
      return null;
    }
    localObject6 = ((List)localObject4).iterator();
    int i1 = 0;
    Object localObject7 = null;
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject6).hasNext();
      if (!bool2) {
        break;
      }
      PduEntity localPduEntity = (PduEntity)((Iterator)localObject6).next();
      long l1 = i1;
      long l2 = d;
      l1 += l2;
      i1 = (int)l1;
    }
    long l3 = a(paramMessage, paramMmsTransportInfo);
    int i2 = 1;
    long l4 = -1;
    boolean bool3 = l3 < l4;
    if (!bool3)
    {
      localObject2 = new String[i2];
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("For some reasons we can not create thread for address ");
      localObject1 = c.f;
      ((StringBuilder)localObject3).append((String)localObject1);
      localObject1 = ((StringBuilder)localObject3).toString();
      localObject2[0] = localObject1;
      AssertionUtil.OnlyInDebug.fail((String[])localObject2);
      return null;
    }
    Object localObject8 = Integer.valueOf(g);
    ((ContentValues)localObject3).put("m_type", (Integer)localObject8);
    localObject8 = Long.valueOf(l3);
    ((ContentValues)localObject3).put("thread_id", (Long)localObject8);
    localObject8 = m;
    ((ContentValues)localObject3).put("ct_t", (String)localObject8);
    localObject8 = h;
    ((ContentValues)localObject3).put("sub", (String)localObject8);
    localObject8 = Integer.valueOf(i);
    ((ContentValues)localObject3).put("sub_cs", (Integer)localObject8);
    localObject8 = Integer.valueOf(q);
    ((ContentValues)localObject3).put("pri", (Integer)localObject8);
    localObject8 = Integer.valueOf(A);
    ((ContentValues)localObject3).put("rr", (Integer)localObject8);
    int i4 = y;
    localObject8 = Integer.valueOf(i4);
    ((ContentValues)localObject3).put("d_rpt", (Integer)localObject8);
    localObject8 = u;
    ((ContentValues)localObject3).put("m_cls", (String)localObject8);
    long l5 = p.a;
    long l6 = 1000L;
    localObject8 = Long.valueOf(l5 / l6);
    ((ContentValues)localObject3).put("exp", (Long)localObject8);
    localObject8 = Long.valueOf(e.a / l6);
    ((ContentValues)localObject3).put("date", (Long)localObject8);
    l5 = d.a / l6;
    localObject8 = Long.valueOf(l5);
    ((ContentValues)localObject3).put("date_sent", (Long)localObject8);
    localObject7 = Integer.valueOf(i1);
    ((ContentValues)localObject3).put("m_size", (Integer)localObject7);
    int i3 = 4;
    Object localObject9 = Integer.valueOf(i3);
    ((ContentValues)localObject3).put("msg_box", (Integer)localObject9);
    localObject7 = k.c();
    if (localObject7 != null)
    {
      localObject9 = l;
      ((ContentValues)localObject3).put((String)localObject7, (String)localObject9);
    }
    localObject7 = h.getContentResolver();
    try
    {
      localObject9 = e;
      if (localObject9 == null)
      {
        localObject9 = b;
        localObject3 = ((ContentResolver)localObject7).insert((Uri)localObject9, (ContentValues)localObject3);
      }
      else
      {
        localObject9 = e;
        int i5 = ((ContentResolver)localObject7).update((Uri)localObject9, (ContentValues)localObject3, null, null);
        if (i5 > 0)
        {
          localObject3 = e;
        }
        else
        {
          i5 = 0;
          localObject3 = null;
        }
      }
      long l7 = ContentUris.parseId((Uri)localObject3);
      boolean bool4 = l7 < l4;
      if (!bool4) {
        return null;
      }
      localObject2 = paramMmsTransportInfo.g();
      b = l7;
      d = l3;
      localObject2 = ((MmsTransportInfo.a)localObject2).c(l7);
      localObject2 = ((MmsTransportInfo.a)localObject2).a();
      localObject3 = paramMessage.m();
      ((Message.a)localObject3).a(i2, (TransportInfo)localObject2);
      localObject5 = E;
      localObject6 = new String[0];
      AssertionUtil.AlwaysFatal.isNotNull(localObject5, (String[])localObject6);
      localObject5 = e;
      localObject6 = E;
      localao.a((Uri)localObject5, (SparseArray)localObject6);
      bool1 = ((List)localObject4).isEmpty();
      if (bool1) {
        return (Message)localObject1;
      }
      ((Message.a)localObject3).a();
      localObject1 = ((List)localObject4).iterator();
      for (;;)
      {
        boolean bool5 = ((Iterator)localObject1).hasNext();
        if (!bool5) {
          break;
        }
        localObject4 = (PduEntity)((Iterator)localObject1).next();
        long l8 = b;
        localObject4 = localao.a(l8, (PduEntity)localObject4);
        if (localObject4 != null) {
          ((Message.a)localObject3).a((Entity)localObject4);
        }
      }
      return ((Message.a)localObject3).b();
    }
    catch (RuntimeException localRuntimeException) {}
    return null;
  }
  
  private void d(String paramString)
  {
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("DeliverMmsError");
    paramString = locala.a("Type", paramString).a();
    u.a(paramString);
  }
  
  private Message e(Message paramMessage, MmsTransportInfo paramMmsTransportInfo)
  {
    Object localObject1 = e;
    Object localObject2 = new String[0];
    AssertionUtil.AlwaysFatal.isNotNull(localObject1, (String[])localObject2);
    localObject1 = new android/content/ContentValues;
    ((ContentValues)localObject1).<init>();
    Object localObject3 = v;
    ((ContentValues)localObject1).put("m_id", (String)localObject3);
    localObject3 = o;
    ((ContentValues)localObject1).put("tr_id", (String)localObject3);
    localObject3 = Integer.valueOf(s);
    ((ContentValues)localObject1).put("resp_st", (Integer)localObject3);
    localObject3 = t;
    ((ContentValues)localObject1).put("resp_txt", (String)localObject3);
    int i1 = 2;
    Integer localInteger = Integer.valueOf(i1);
    ((ContentValues)localObject1).put("msg_box", localInteger);
    localObject2 = h.getContentResolver();
    localInteger = null;
    try
    {
      Uri localUri = e;
      int i2 = ((ContentResolver)localObject2).update(localUri, (ContentValues)localObject1, null, null);
      if (i2 <= 0) {
        return null;
      }
      paramMmsTransportInfo = paramMmsTransportInfo.g();
      v = i1;
      paramMmsTransportInfo = paramMmsTransportInfo.a();
      paramMessage = paramMessage.m();
      i2 = 1;
      paramMessage = paramMessage.a(i2, paramMmsTransportInfo);
      return paramMessage.b();
    }
    catch (RuntimeException localRuntimeException) {}
    return null;
  }
  
  private boolean g()
  {
    com.truecaller.utils.d locald = m;
    String str = w.b();
    return locald.a(str);
  }
  
  public final long a(long paramLong)
  {
    long l1 = 1000L;
    return paramLong / l1 * l1;
  }
  
  public final long a(com.truecaller.messaging.transport.f paramf, i parami, r paramr, org.a.a.b paramb1, org.a.a.b paramb2, int paramInt, List paramList, com.truecaller.utils.q paramq, boolean paramBoolean1, boolean paramBoolean2, Set paramSet)
  {
    com.truecaller.utils.l locall = v;
    String[] arrayOfString = { "android.permission.READ_SMS" };
    boolean bool = locall.a(arrayOfString);
    if (!bool)
    {
      new String[1][0] = "SMS permissions is not granted";
      return 0L;
    }
    return n.a(paramf, parami, paramr, paramb1, paramb2, paramInt, paramList, paramq, paramBoolean1, paramBoolean2, paramSet);
  }
  
  public final l.a a(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    boolean bool = g();
    if (!bool)
    {
      new String[1][0] = "We are not default SMS app and can not send message";
      paramMessage = new com/truecaller/messaging/transport/l$a;
      paramMessage.<init>(0);
      return paramMessage;
    }
    int i1 = j;
    int i2 = 3;
    int i3 = 128;
    int i4 = 129;
    long l1;
    String str1;
    if (i1 == i2)
    {
      localObject1 = new com/truecaller/messaging/transport/mms/MmsTransportInfo$a;
      ((MmsTransportInfo.a)localObject1).<init>();
      l1 = a;
      a = l1;
      int i5 = 106;
      localObject1 = ((MmsTransportInfo.a)localObject1).a("No title", i5);
      w = i3;
      l2 = d;
      localObject1 = ((MmsTransportInfo.a)localObject1).e(l2);
      o = "personal";
      y = i4;
      A = i4;
      r = i4;
      l = "application/vnd.wap.multipart.related";
      localObject2 = E;
      if (localObject2 == null)
      {
        localObject2 = new android/util/SparseArray;
        ((SparseArray)localObject2).<init>();
        E = ((SparseArray)localObject2);
      }
      localObject2 = E;
      i3 = 151;
      localObject2 = (Set)((SparseArray)localObject2).get(i3);
      if (localObject2 == null)
      {
        localObject2 = new java/util/HashSet;
        ((HashSet)localObject2).<init>();
        localObject3 = E;
        ((SparseArray)localObject3).put(i3, localObject2);
      }
      i3 = paramArrayOfParticipant.length;
      i4 = 0;
      localObject3 = null;
      while (i4 < i3)
      {
        str1 = f;
        ((Set)localObject2).add(str1);
        i4 += 1;
      }
      paramArrayOfParticipant = ((MmsTransportInfo.a)localObject1).a();
      paramMessage = a(paramMessage, paramArrayOfParticipant, false);
      if (paramMessage == null)
      {
        paramMessage = new com/truecaller/messaging/transport/l$a;
        paramMessage.<init>(0);
        return paramMessage;
      }
      paramArrayOfParticipant = new com/truecaller/messaging/transport/l$a;
      paramMessage = m;
      paramArrayOfParticipant.<init>(paramMessage);
      return paramArrayOfParticipant;
    }
    paramArrayOfParticipant = ((MmsTransportInfo)m).g();
    i1 = 4;
    v = i1;
    w = i3;
    long l2 = d;
    paramArrayOfParticipant = paramArrayOfParticipant.e(l2);
    o = "personal";
    y = i4;
    A = i4;
    r = i4;
    l = "application/vnd.wap.multipart.related";
    paramArrayOfParticipant = paramArrayOfParticipant.a();
    Object localObject2 = new android/content/ContentValues;
    ((ContentValues)localObject2).<init>();
    long l3 = e.a;
    long l4 = 1000L;
    Object localObject3 = Long.valueOf(l3 / l4);
    ((ContentValues)localObject2).put("date", (Long)localObject3);
    String str2 = "date_sent";
    l3 = d.a / l4;
    paramMessage = Long.valueOf(l3);
    ((ContentValues)localObject2).put(str2, paramMessage);
    Object localObject1 = Integer.valueOf(i1);
    ((ContentValues)localObject2).put("msg_box", (Integer)localObject1);
    paramMessage = h.getContentResolver();
    int i6;
    try
    {
      localObject1 = b;
      str2 = "_id=?";
      i4 = 1;
      localObject3 = new String[i4];
      l1 = b;
      str1 = String.valueOf(l1);
      localObject3[0] = str1;
      i6 = paramMessage.update((Uri)localObject1, (ContentValues)localObject2, str2, (String[])localObject3);
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localRuntimeException);
      i6 = 0;
      paramMessage = null;
    }
    if (i6 == 0)
    {
      paramMessage = new com/truecaller/messaging/transport/l$a;
      paramMessage.<init>(0);
      return paramMessage;
    }
    paramMessage = new com/truecaller/messaging/transport/l$a;
    paramMessage.<init>(paramArrayOfParticipant);
    return paramMessage;
  }
  
  public final String a()
  {
    return "mms";
  }
  
  public final String a(String paramString)
  {
    return paramString;
  }
  
  /* Error */
  public final void a(Intent paramIntent, int paramInt)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore_3
    //   2: aload_1
    //   3: astore 4
    //   5: iload_2
    //   6: istore 5
    //   8: aload_1
    //   9: invokevirtual 1011	android/content/Intent:getAction	()Ljava/lang/String;
    //   12: astore 6
    //   14: ldc_w 1013
    //   17: astore 7
    //   19: aload 7
    //   21: aload 6
    //   23: invokevirtual 357	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   26: istore 8
    //   28: iload 8
    //   30: ifeq +64 -> 94
    //   33: ldc_w 1015
    //   36: astore 7
    //   38: aload_1
    //   39: invokevirtual 1018	android/content/Intent:getType	()Ljava/lang/String;
    //   42: astore 9
    //   44: aload 7
    //   46: aload 9
    //   48: invokevirtual 357	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   51: istore 8
    //   53: iload 8
    //   55: ifeq +39 -> 94
    //   58: aload_0
    //   59: aload_1
    //   60: invokespecial 1021	com/truecaller/messaging/transport/mms/ao:a	(Landroid/content/Intent;)Lcom/truecaller/messaging/data/types/Message;
    //   63: astore 4
    //   65: aload 4
    //   67: ifnull +26 -> 93
    //   70: aload_0
    //   71: getfield 146	com/truecaller/messaging/transport/mms/ao:j	Lcom/truecaller/androidactors/f;
    //   74: invokeinterface 495 1 0
    //   79: checkcast 1023	com/truecaller/messaging/data/t
    //   82: astore 10
    //   84: aload 10
    //   86: aload 4
    //   88: invokeinterface 1026 2 0
    //   93: return
    //   94: ldc_w 1028
    //   97: astore 7
    //   99: aload 7
    //   101: aload 6
    //   103: invokevirtual 357	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   106: istore 8
    //   108: iload 8
    //   110: ifeq +149 -> 259
    //   113: ldc_w 1015
    //   116: astore 7
    //   118: aload_1
    //   119: invokevirtual 1018	android/content/Intent:getType	()Ljava/lang/String;
    //   122: astore 9
    //   124: aload 7
    //   126: aload 9
    //   128: invokevirtual 357	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   131: istore 8
    //   133: iload 8
    //   135: ifeq +124 -> 259
    //   138: new 847	com/truecaller/analytics/e$a
    //   141: astore 10
    //   143: aload 10
    //   145: ldc_w 1030
    //   148: invokespecial 850	com/truecaller/analytics/e$a:<init>	(Ljava/lang/String;)V
    //   151: aload 10
    //   153: ldc_w 852
    //   156: ldc_w 854
    //   159: invokevirtual 857	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   162: astore 10
    //   164: ldc_w 1032
    //   167: astore 6
    //   169: aload_3
    //   170: getfield 170	com/truecaller/messaging/transport/mms/ao:w	Lcom/truecaller/messaging/a;
    //   173: invokeinterface 947 1 0
    //   178: astore 7
    //   180: ldc_w 1034
    //   183: astore 9
    //   185: aload 7
    //   187: ifnonnull +7 -> 194
    //   190: aload 9
    //   192: astore 7
    //   194: aload 10
    //   196: aload 6
    //   198: aload 7
    //   200: invokevirtual 857	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   203: invokevirtual 870	com/truecaller/analytics/e$a:a	()Lcom/truecaller/analytics/e;
    //   206: astore 10
    //   208: aload_3
    //   209: getfield 166	com/truecaller/messaging/transport/mms/ao:u	Lcom/truecaller/analytics/b;
    //   212: astore 6
    //   214: aload 6
    //   216: aload 10
    //   218: invokeinterface 875 2 0
    //   223: aload_0
    //   224: aload_1
    //   225: invokespecial 1021	com/truecaller/messaging/transport/mms/ao:a	(Landroid/content/Intent;)Lcom/truecaller/messaging/data/types/Message;
    //   228: astore 4
    //   230: aload 4
    //   232: ifnull +26 -> 258
    //   235: aload_3
    //   236: getfield 158	com/truecaller/messaging/transport/mms/ao:r	Lcom/truecaller/androidactors/f;
    //   239: invokeinterface 495 1 0
    //   244: checkcast 540	com/truecaller/messaging/notifications/a
    //   247: astore 10
    //   249: aload 10
    //   251: aload 4
    //   253: invokeinterface 1035 2 0
    //   258: return
    //   259: ldc_w 1037
    //   262: astore 7
    //   264: aload 7
    //   266: aload 6
    //   268: invokevirtual 357	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   271: istore 8
    //   273: iconst_m1
    //   274: istore 11
    //   276: iconst_m1
    //   277: i2l
    //   278: lstore 12
    //   280: iconst_1
    //   281: istore 14
    //   283: iload 8
    //   285: ifeq +667 -> 952
    //   288: aload 4
    //   290: ldc_w 1039
    //   293: invokevirtual 1042	android/content/Intent:getStringExtra	(Ljava/lang/String;)Ljava/lang/String;
    //   296: astore 6
    //   298: aload 6
    //   300: invokestatic 778	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   303: istore 8
    //   305: iload 8
    //   307: ifne +29 -> 336
    //   310: aload_3
    //   311: getfield 142	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   314: invokevirtual 283	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   317: astore 7
    //   319: aload 6
    //   321: invokestatic 339	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   324: astore 6
    //   326: aload 7
    //   328: aload 6
    //   330: aconst_null
    //   331: aconst_null
    //   332: invokevirtual 589	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   335: pop
    //   336: ldc_w 1044
    //   339: astore 6
    //   341: aload 4
    //   343: aload 6
    //   345: lload 12
    //   347: invokevirtual 1048	android/content/Intent:getLongExtra	(Ljava/lang/String;J)J
    //   350: lstore 15
    //   352: lload 15
    //   354: lload 12
    //   356: lcmp
    //   357: istore 17
    //   359: iload 17
    //   361: ifne +13 -> 374
    //   364: iconst_1
    //   365: anewarray 81	java/lang/String
    //   368: iconst_0
    //   369: ldc_w 1050
    //   372: aastore
    //   373: return
    //   374: ldc_w 1052
    //   377: astore 18
    //   379: aload 4
    //   381: aload 18
    //   383: lload 12
    //   385: invokevirtual 1048	android/content/Intent:getLongExtra	(Ljava/lang/String;J)J
    //   388: lstore 19
    //   390: lload 19
    //   392: lload 12
    //   394: lcmp
    //   395: istore 21
    //   397: iload 21
    //   399: ifne +13 -> 412
    //   402: iconst_1
    //   403: anewarray 81	java/lang/String
    //   406: iconst_0
    //   407: ldc_w 1054
    //   410: aastore
    //   411: return
    //   412: aload 4
    //   414: ldc_w 1056
    //   417: invokevirtual 1042	android/content/Intent:getStringExtra	(Ljava/lang/String;)Ljava/lang/String;
    //   420: astore 22
    //   422: aload 22
    //   424: ifnonnull +28 -> 452
    //   427: ldc_w 447
    //   430: astore 22
    //   432: ldc_w 1058
    //   435: astore 23
    //   437: iconst_1
    //   438: anewarray 81	java/lang/String
    //   441: iconst_0
    //   442: aload 23
    //   444: aastore
    //   445: aload 22
    //   447: astore 24
    //   449: goto +7 -> 456
    //   452: aload 22
    //   454: astore 24
    //   456: aload 4
    //   458: ldc_w 1060
    //   461: invokevirtual 438	android/content/Intent:getByteArrayExtra	(Ljava/lang/String;)[B
    //   464: astore 4
    //   466: new 633	org/a/a/b
    //   469: astore 22
    //   471: aload 22
    //   473: lload 19
    //   475: invokespecial 845	org/a/a/b:<init>	(J)V
    //   478: aload_3
    //   479: getfield 152	com/truecaller/messaging/transport/mms/ao:k	Lcom/truecaller/multisim/h;
    //   482: aload 24
    //   484: invokeinterface 452 2 0
    //   489: astore 23
    //   491: aload 4
    //   493: ifnonnull +51 -> 544
    //   496: aload_3
    //   497: lload 15
    //   499: invokespecial 1062	com/truecaller/messaging/transport/mms/ao:c	(J)Z
    //   502: istore 25
    //   504: iload 25
    //   506: ifeq +30 -> 536
    //   509: aload_3
    //   510: getfield 146	com/truecaller/messaging/transport/mms/ao:j	Lcom/truecaller/androidactors/f;
    //   513: invokeinterface 495 1 0
    //   518: checkcast 1023	com/truecaller/messaging/data/t
    //   521: astore 4
    //   523: aload 4
    //   525: iload 14
    //   527: aload 22
    //   529: iload 14
    //   531: invokeinterface 1065 4 0
    //   536: aload_3
    //   537: ldc_w 1067
    //   540: invokespecial 1069	com/truecaller/messaging/transport/mms/ao:d	(Ljava/lang/String;)V
    //   543: return
    //   544: new 291	com/android/a/a/a/n
    //   547: astore 18
    //   549: aload 23
    //   551: invokeinterface 456 1 0
    //   556: istore 26
    //   558: aload 18
    //   560: aload 4
    //   562: iload 26
    //   564: invokespecial 299	com/android/a/a/a/n:<init>	([BZ)V
    //   567: aload 18
    //   569: invokevirtual 302	com/android/a/a/a/n:a	()Lcom/android/a/a/a/f;
    //   572: astore 27
    //   574: aload 27
    //   576: ifnonnull +20 -> 596
    //   579: iconst_1
    //   580: anewarray 81	java/lang/String
    //   583: iconst_0
    //   584: ldc_w 458
    //   587: aastore
    //   588: aload_3
    //   589: ldc_w 1071
    //   592: invokespecial 1069	com/truecaller/messaging/transport/mms/ao:d	(Ljava/lang/String;)V
    //   595: return
    //   596: aload 27
    //   598: invokevirtual 1075	com/android/a/a/a/f:a	()I
    //   601: istore 25
    //   603: sipush 129
    //   606: istore 26
    //   608: iload 25
    //   610: iload 26
    //   612: if_icmpeq +11 -> 623
    //   615: aload_3
    //   616: ldc_w 1077
    //   619: invokespecial 1069	com/truecaller/messaging/transport/mms/ao:d	(Ljava/lang/String;)V
    //   622: return
    //   623: aload_3
    //   624: getfield 162	com/truecaller/messaging/transport/mms/ao:o	Lcom/truecaller/messaging/transport/mms/h;
    //   627: astore 18
    //   629: aload_3
    //   630: aload 24
    //   632: invokespecial 460	com/truecaller/messaging/transport/mms/ao:c	(Ljava/lang/String;)Z
    //   635: istore 21
    //   637: lload 15
    //   639: lstore 28
    //   641: aload 18
    //   643: aload 27
    //   645: iload 21
    //   647: aload 24
    //   649: lload 15
    //   651: invokeinterface 463 6 0
    //   656: astore 4
    //   658: aload 4
    //   660: getfield 471	com/truecaller/messaging/data/types/Message:m	Lcom/truecaller/messaging/data/types/TransportInfo;
    //   663: checkcast 172	com/truecaller/messaging/transport/mms/MmsTransportInfo
    //   666: astore 23
    //   668: aload 23
    //   670: getfield 465	com/truecaller/messaging/transport/mms/MmsTransportInfo:g	I
    //   673: istore 30
    //   675: aload 23
    //   677: getfield 762	com/truecaller/messaging/transport/mms/MmsTransportInfo:s	I
    //   680: istore 21
    //   682: iconst_2
    //   683: iload 30
    //   685: iload 21
    //   687: invokestatic 1080	com/truecaller/messaging/transport/mms/MmsTransportInfo:a	(III)I
    //   690: bipush 8
    //   692: iand
    //   693: istore 17
    //   695: iload 17
    //   697: ifeq +92 -> 789
    //   700: new 847	com/truecaller/analytics/e$a
    //   703: astore 18
    //   705: aload 18
    //   707: ldc_w 1082
    //   710: invokespecial 850	com/truecaller/analytics/e$a:<init>	(Ljava/lang/String;)V
    //   713: aload 18
    //   715: ldc_w 1084
    //   718: iload 5
    //   720: invokevirtual 1087	com/truecaller/analytics/e$a:a	(Ljava/lang/String;I)Lcom/truecaller/analytics/e$a;
    //   723: astore 18
    //   725: aload 23
    //   727: getfield 762	com/truecaller/messaging/transport/mms/MmsTransportInfo:s	I
    //   730: istore 21
    //   732: aload 18
    //   734: ldc_w 1089
    //   737: iload 21
    //   739: invokevirtual 1087	com/truecaller/analytics/e$a:a	(Ljava/lang/String;I)Lcom/truecaller/analytics/e$a;
    //   742: astore 18
    //   744: ldc_w 1091
    //   747: astore 27
    //   749: aload 23
    //   751: getfield 772	com/truecaller/messaging/transport/mms/MmsTransportInfo:r	I
    //   754: istore 26
    //   756: aload 18
    //   758: aload 27
    //   760: iload 26
    //   762: invokevirtual 1087	com/truecaller/analytics/e$a:a	(Ljava/lang/String;I)Lcom/truecaller/analytics/e$a;
    //   765: astore 23
    //   767: aload_3
    //   768: getfield 166	com/truecaller/messaging/transport/mms/ao:u	Lcom/truecaller/analytics/b;
    //   771: astore 18
    //   773: aload 23
    //   775: invokevirtual 870	com/truecaller/analytics/e$a:a	()Lcom/truecaller/analytics/e;
    //   778: astore 23
    //   780: aload 18
    //   782: aload 23
    //   784: invokeinterface 875 2 0
    //   789: iload 5
    //   791: iload 11
    //   793: if_icmpeq +44 -> 837
    //   796: aload_3
    //   797: lload 15
    //   799: invokespecial 1062	com/truecaller/messaging/transport/mms/ao:c	(J)Z
    //   802: istore 25
    //   804: iload 25
    //   806: ifeq +30 -> 836
    //   809: aload_3
    //   810: getfield 146	com/truecaller/messaging/transport/mms/ao:j	Lcom/truecaller/androidactors/f;
    //   813: invokeinterface 495 1 0
    //   818: checkcast 1023	com/truecaller/messaging/data/t
    //   821: astore 4
    //   823: aload 4
    //   825: iload 14
    //   827: aload 22
    //   829: iload 14
    //   831: invokeinterface 1065 4 0
    //   836: return
    //   837: aload_3
    //   838: getfield 146	com/truecaller/messaging/transport/mms/ao:j	Lcom/truecaller/androidactors/f;
    //   841: invokeinterface 495 1 0
    //   846: checkcast 1023	com/truecaller/messaging/data/t
    //   849: aload 4
    //   851: invokeinterface 1026 2 0
    //   856: aload_3
    //   857: getfield 138	com/truecaller/messaging/transport/mms/ao:x	Lcom/truecaller/messaging/transport/mms/ao$a;
    //   860: astore 10
    //   862: aload 10
    //   864: ifnull +87 -> 951
    //   867: aload 10
    //   869: getfield 1094	com/truecaller/messaging/transport/mms/ao$a:a	J
    //   872: lstore 31
    //   874: lload 15
    //   876: lload 31
    //   878: lcmp
    //   879: istore 25
    //   881: iload 25
    //   883: ifne +63 -> 946
    //   886: aload 10
    //   888: getfield 1097	com/truecaller/messaging/transport/mms/ao$a:b	Ljava/util/concurrent/locks/Lock;
    //   891: astore 4
    //   893: aload 4
    //   895: invokeinterface 1102 1 0
    //   900: aload 10
    //   902: getfield 1105	com/truecaller/messaging/transport/mms/ao$a:c	Ljava/util/concurrent/locks/Condition;
    //   905: astore 4
    //   907: aload 4
    //   909: invokeinterface 1110 1 0
    //   914: aload 10
    //   916: getfield 1097	com/truecaller/messaging/transport/mms/ao$a:b	Ljava/util/concurrent/locks/Lock;
    //   919: astore 4
    //   921: aload 4
    //   923: invokeinterface 1113 1 0
    //   928: goto +18 -> 946
    //   931: astore 4
    //   933: aload 10
    //   935: getfield 1097	com/truecaller/messaging/transport/mms/ao$a:b	Ljava/util/concurrent/locks/Lock;
    //   938: invokeinterface 1113 1 0
    //   943: aload 4
    //   945: athrow
    //   946: aload_3
    //   947: aconst_null
    //   948: putfield 138	com/truecaller/messaging/transport/mms/ao:x	Lcom/truecaller/messaging/transport/mms/ao$a;
    //   951: return
    //   952: ldc_w 1115
    //   955: astore 7
    //   957: aload 7
    //   959: aload 6
    //   961: invokevirtual 357	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   964: istore 33
    //   966: iload 33
    //   968: ifeq +869 -> 1837
    //   971: aload_1
    //   972: invokevirtual 1119	android/content/Intent:getData	()Landroid/net/Uri;
    //   975: astore 6
    //   977: aload 6
    //   979: ifnonnull +20 -> 999
    //   982: iconst_1
    //   983: anewarray 81	java/lang/String
    //   986: iconst_0
    //   987: ldc_w 1121
    //   990: aastore
    //   991: aload_3
    //   992: ldc_w 1123
    //   995: invokespecial 1125	com/truecaller/messaging/transport/mms/ao:b	(Ljava/lang/String;)V
    //   998: return
    //   999: aload 4
    //   1001: ldc_w 1039
    //   1004: invokevirtual 1042	android/content/Intent:getStringExtra	(Ljava/lang/String;)Ljava/lang/String;
    //   1007: astore 7
    //   1009: ldc_w 1044
    //   1012: astore 18
    //   1014: aload 4
    //   1016: aload 18
    //   1018: lload 12
    //   1020: invokevirtual 1048	android/content/Intent:getLongExtra	(Ljava/lang/String;J)J
    //   1023: lstore 28
    //   1025: lload 28
    //   1027: lload 12
    //   1029: lcmp
    //   1030: istore 17
    //   1032: iload 17
    //   1034: ifne +46 -> 1080
    //   1037: iconst_1
    //   1038: anewarray 81	java/lang/String
    //   1041: iconst_0
    //   1042: ldc_w 1050
    //   1045: aastore
    //   1046: aload_3
    //   1047: getfield 142	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   1050: invokevirtual 283	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   1053: astore 4
    //   1055: aload 7
    //   1057: invokestatic 339	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   1060: astore 10
    //   1062: aload 4
    //   1064: aload 10
    //   1066: aconst_null
    //   1067: aconst_null
    //   1068: invokevirtual 589	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   1071: pop
    //   1072: aload_3
    //   1073: ldc_w 1123
    //   1076: invokespecial 1125	com/truecaller/messaging/transport/mms/ao:b	(Ljava/lang/String;)V
    //   1079: return
    //   1080: aload 4
    //   1082: ldc_w 1127
    //   1085: invokevirtual 438	android/content/Intent:getByteArrayExtra	(Ljava/lang/String;)[B
    //   1088: astore 22
    //   1090: aload 22
    //   1092: ifnonnull +46 -> 1138
    //   1095: iconst_1
    //   1096: anewarray 81	java/lang/String
    //   1099: iconst_0
    //   1100: ldc_w 1129
    //   1103: aastore
    //   1104: aload_3
    //   1105: getfield 142	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   1108: invokevirtual 283	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   1111: astore 4
    //   1113: aload 7
    //   1115: invokestatic 339	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   1118: astore 10
    //   1120: aload 4
    //   1122: aload 10
    //   1124: aconst_null
    //   1125: aconst_null
    //   1126: invokevirtual 589	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   1129: pop
    //   1130: aload_3
    //   1131: ldc_w 1123
    //   1134: invokespecial 1125	com/truecaller/messaging/transport/mms/ao:b	(Ljava/lang/String;)V
    //   1137: return
    //   1138: aload 4
    //   1140: ldc_w 1056
    //   1143: invokevirtual 1042	android/content/Intent:getStringExtra	(Ljava/lang/String;)Ljava/lang/String;
    //   1146: astore 23
    //   1148: aload 23
    //   1150: ifnonnull +21 -> 1171
    //   1153: ldc_w 447
    //   1156: astore 23
    //   1158: ldc_w 1058
    //   1161: astore 18
    //   1163: iconst_1
    //   1164: anewarray 81	java/lang/String
    //   1167: iconst_0
    //   1168: aload 18
    //   1170: aastore
    //   1171: aload 7
    //   1173: invokestatic 339	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   1176: astore 7
    //   1178: iconst_0
    //   1179: istore 30
    //   1181: aconst_null
    //   1182: astore 27
    //   1184: aload 4
    //   1186: ldc_w 1131
    //   1189: iconst_0
    //   1190: invokevirtual 1135	android/content/Intent:getBooleanExtra	(Ljava/lang/String;Z)Z
    //   1193: istore 25
    //   1195: aload_3
    //   1196: getfield 136	com/truecaller/messaging/transport/mms/ao:q	Ljava/util/Set;
    //   1199: astore 18
    //   1201: aload 18
    //   1203: monitorenter
    //   1204: aload_3
    //   1205: getfield 136	com/truecaller/messaging/transport/mms/ao:q	Ljava/util/Set;
    //   1208: astore 34
    //   1210: lload 28
    //   1212: invokestatic 624	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   1215: astore 24
    //   1217: aload 34
    //   1219: aload 24
    //   1221: invokeinterface 1138 2 0
    //   1226: pop
    //   1227: aload 18
    //   1229: monitorexit
    //   1230: aload_3
    //   1231: getfield 152	com/truecaller/messaging/transport/mms/ao:k	Lcom/truecaller/multisim/h;
    //   1234: aload 23
    //   1236: invokeinterface 452 2 0
    //   1241: astore 18
    //   1243: iload 5
    //   1245: iload 11
    //   1247: if_icmpne +338 -> 1585
    //   1250: aload 7
    //   1252: ifnonnull +6 -> 1258
    //   1255: goto +330 -> 1585
    //   1258: aload 18
    //   1260: invokeinterface 456 1 0
    //   1265: istore 5
    //   1267: aload_3
    //   1268: aload 7
    //   1270: iload 5
    //   1272: invokespecial 1141	com/truecaller/messaging/transport/mms/ao:a	(Landroid/net/Uri;Z)Lcom/android/a/a/a/f;
    //   1275: astore 34
    //   1277: aload_3
    //   1278: getfield 142	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   1281: invokevirtual 283	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   1284: astore 10
    //   1286: aload 10
    //   1288: aload 7
    //   1290: aconst_null
    //   1291: aconst_null
    //   1292: invokevirtual 589	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   1295: pop
    //   1296: aload 34
    //   1298: ifnonnull +41 -> 1339
    //   1301: iload 14
    //   1303: anewarray 81	java/lang/String
    //   1306: astore 4
    //   1308: aload 7
    //   1310: invokestatic 315	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   1313: astore 6
    //   1315: ldc_w 1143
    //   1318: aload 6
    //   1320: invokevirtual 319	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   1323: astore 10
    //   1325: aload 4
    //   1327: iconst_0
    //   1328: aload 10
    //   1330: aastore
    //   1331: aload_3
    //   1332: ldc_w 1123
    //   1335: invokespecial 1125	com/truecaller/messaging/transport/mms/ao:b	(Ljava/lang/String;)V
    //   1338: return
    //   1339: aload 34
    //   1341: instanceof 1145
    //   1344: istore 5
    //   1346: iload 5
    //   1348: ifeq +54 -> 1402
    //   1351: aload 34
    //   1353: astore 10
    //   1355: aload 34
    //   1357: checkcast 1145	com/android/a/a/a/s
    //   1360: astore 10
    //   1362: aload 10
    //   1364: getfield 1148	com/android/a/a/a/s:a	Lcom/android/a/a/a/m;
    //   1367: astore 7
    //   1369: sipush 152
    //   1372: istore 11
    //   1374: aload 7
    //   1376: iload 11
    //   1378: invokevirtual 1154	com/android/a/a/a/m:b	(I)[B
    //   1381: astore 7
    //   1383: aload 7
    //   1385: invokestatic 1159	org/c/a/a/a/a:a	([B)Z
    //   1388: istore 8
    //   1390: iload 8
    //   1392: ifeq +10 -> 1402
    //   1395: aload 10
    //   1397: aload 22
    //   1399: invokevirtual 1162	com/android/a/a/a/s:a	([B)V
    //   1402: aload_3
    //   1403: getfield 162	com/truecaller/messaging/transport/mms/ao:o	Lcom/truecaller/messaging/transport/mms/h;
    //   1406: astore 27
    //   1408: aconst_null
    //   1409: astore 24
    //   1411: aload 27
    //   1413: aload 34
    //   1415: checkcast 1073	com/android/a/a/a/f
    //   1418: iconst_0
    //   1419: aload 23
    //   1421: lload 28
    //   1423: invokeinterface 463 6 0
    //   1428: astore 10
    //   1430: aload 10
    //   1432: getfield 471	com/truecaller/messaging/data/types/Message:m	Lcom/truecaller/messaging/data/types/TransportInfo;
    //   1435: checkcast 172	com/truecaller/messaging/transport/mms/MmsTransportInfo
    //   1438: invokevirtual 816	com/truecaller/messaging/transport/mms/MmsTransportInfo:g	()Lcom/truecaller/messaging/transport/mms/MmsTransportInfo$a;
    //   1441: astore 7
    //   1443: aload 7
    //   1445: aload 6
    //   1447: putfield 1164	com/truecaller/messaging/transport/mms/MmsTransportInfo$a:k	Landroid/net/Uri;
    //   1450: aload 7
    //   1452: iload 25
    //   1454: putfield 1165	com/truecaller/messaging/transport/mms/MmsTransportInfo$a:D	Z
    //   1457: aload 7
    //   1459: invokevirtual 824	com/truecaller/messaging/transport/mms/MmsTransportInfo$a:a	()Lcom/truecaller/messaging/transport/mms/MmsTransportInfo;
    //   1462: astore 4
    //   1464: aload 10
    //   1466: invokevirtual 827	com/truecaller/messaging/data/types/Message:m	()Lcom/truecaller/messaging/data/types/Message$a;
    //   1469: astore 10
    //   1471: aload 10
    //   1473: iload 14
    //   1475: aload 4
    //   1477: invokevirtual 832	com/truecaller/messaging/data/types/Message$a:a	(ILcom/truecaller/messaging/data/types/TransportInfo;)Lcom/truecaller/messaging/data/types/Message$a;
    //   1480: invokevirtual 835	com/truecaller/messaging/data/types/Message$a:b	()Lcom/truecaller/messaging/data/types/Message;
    //   1483: astore 4
    //   1485: aload 4
    //   1487: invokevirtual 896	com/truecaller/messaging/data/types/Message:c	()Z
    //   1490: istore 5
    //   1492: iload 5
    //   1494: ifne +20 -> 1514
    //   1497: iconst_1
    //   1498: anewarray 81	java/lang/String
    //   1501: iconst_0
    //   1502: ldc_w 1167
    //   1505: aastore
    //   1506: aload_3
    //   1507: ldc_w 1123
    //   1510: invokespecial 1125	com/truecaller/messaging/transport/mms/ao:b	(Ljava/lang/String;)V
    //   1513: return
    //   1514: ldc_w 1169
    //   1517: astore 10
    //   1519: aload_3
    //   1520: aload 10
    //   1522: invokespecial 1125	com/truecaller/messaging/transport/mms/ao:b	(Ljava/lang/String;)V
    //   1525: goto +278 -> 1803
    //   1528: astore 4
    //   1530: goto +37 -> 1567
    //   1533: astore 4
    //   1535: aload 4
    //   1537: invokestatic 428	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   1540: ldc_w 1123
    //   1543: astore 4
    //   1545: aload_3
    //   1546: aload 4
    //   1548: invokespecial 1125	com/truecaller/messaging/transport/mms/ao:b	(Ljava/lang/String;)V
    //   1551: aload_3
    //   1552: getfield 142	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   1555: invokevirtual 283	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   1558: aload 7
    //   1560: aconst_null
    //   1561: aconst_null
    //   1562: invokevirtual 589	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   1565: pop
    //   1566: return
    //   1567: aload_3
    //   1568: getfield 142	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   1571: invokevirtual 283	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   1574: aload 7
    //   1576: aconst_null
    //   1577: aconst_null
    //   1578: invokevirtual 589	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   1581: pop
    //   1582: aload 4
    //   1584: athrow
    //   1585: lload 28
    //   1587: lstore 19
    //   1589: iload 25
    //   1591: ifeq +52 -> 1643
    //   1594: aload_3
    //   1595: getfield 154	com/truecaller/messaging/transport/mms/ao:l	Lcom/truecaller/messaging/transport/mms/ak;
    //   1598: astore 10
    //   1600: aload_3
    //   1601: getfield 152	com/truecaller/messaging/transport/mms/ao:k	Lcom/truecaller/multisim/h;
    //   1604: astore 9
    //   1606: aload 10
    //   1608: aload 23
    //   1610: aload 9
    //   1612: invokevirtual 488	com/truecaller/messaging/transport/mms/ak:a	(Ljava/lang/String;Lcom/truecaller/multisim/h;)Lcom/truecaller/androidactors/f;
    //   1615: invokeinterface 495 1 0
    //   1620: checkcast 497	com/truecaller/messaging/transport/mms/ai
    //   1623: astore 10
    //   1625: sipush 131
    //   1628: istore 11
    //   1630: aload 10
    //   1632: aload 22
    //   1634: aload 6
    //   1636: iload 11
    //   1638: invokeinterface 507 4 0
    //   1643: new 818	com/truecaller/messaging/transport/mms/MmsTransportInfo$a
    //   1646: astore 10
    //   1648: aload 10
    //   1650: invokespecial 976	com/truecaller/messaging/transport/mms/MmsTransportInfo$a:<init>	()V
    //   1653: aload 10
    //   1655: lload 19
    //   1657: putfield 820	com/truecaller/messaging/transport/mms/MmsTransportInfo$a:b	J
    //   1660: aload 10
    //   1662: lload 19
    //   1664: invokevirtual 938	com/truecaller/messaging/transport/mms/MmsTransportInfo$a:c	(J)Lcom/truecaller/messaging/transport/mms/MmsTransportInfo$a;
    //   1667: astore 10
    //   1669: aload 10
    //   1671: sipush 132
    //   1674: putfield 985	com/truecaller/messaging/transport/mms/MmsTransportInfo$a:w	I
    //   1677: sipush 194
    //   1680: istore 33
    //   1682: aload 10
    //   1684: iload 33
    //   1686: putfield 1171	com/truecaller/messaging/transport/mms/MmsTransportInfo$a:s	I
    //   1689: aload 10
    //   1691: invokevirtual 824	com/truecaller/messaging/transport/mms/MmsTransportInfo$a:a	()Lcom/truecaller/messaging/transport/mms/MmsTransportInfo;
    //   1694: astore 10
    //   1696: new 829	com/truecaller/messaging/data/types/Message$a
    //   1699: astore 6
    //   1701: aload 6
    //   1703: invokespecial 1172	com/truecaller/messaging/data/types/Message$a:<init>	()V
    //   1706: getstatic 1174	com/truecaller/messaging/data/types/Participant:a	Lcom/truecaller/messaging/data/types/Participant;
    //   1709: astore 9
    //   1711: aload 6
    //   1713: aload 9
    //   1715: putfield 1175	com/truecaller/messaging/data/types/Message$a:c	Lcom/truecaller/messaging/data/types/Participant;
    //   1718: iload 25
    //   1720: iload 14
    //   1722: ixor
    //   1723: istore 25
    //   1725: aload 6
    //   1727: iload 25
    //   1729: putfield 1176	com/truecaller/messaging/data/types/Message$a:g	Z
    //   1732: aload 6
    //   1734: iload 14
    //   1736: aload 10
    //   1738: invokevirtual 832	com/truecaller/messaging/data/types/Message$a:a	(ILcom/truecaller/messaging/data/types/TransportInfo;)Lcom/truecaller/messaging/data/types/Message$a;
    //   1741: aload 23
    //   1743: invokevirtual 1179	com/truecaller/messaging/data/types/Message$a:a	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Message$a;
    //   1746: invokevirtual 835	com/truecaller/messaging/data/types/Message$a:b	()Lcom/truecaller/messaging/data/types/Message;
    //   1749: astore 4
    //   1751: aload 7
    //   1753: ifnull +22 -> 1775
    //   1756: aload_3
    //   1757: getfield 142	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   1760: invokevirtual 283	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   1763: astore 10
    //   1765: aload 10
    //   1767: aload 7
    //   1769: aconst_null
    //   1770: aconst_null
    //   1771: invokevirtual 589	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   1774: pop
    //   1775: aload_3
    //   1776: getfield 158	com/truecaller/messaging/transport/mms/ao:r	Lcom/truecaller/androidactors/f;
    //   1779: invokeinterface 495 1 0
    //   1784: checkcast 540	com/truecaller/messaging/notifications/a
    //   1787: invokeinterface 542 1 0
    //   1792: ldc_w 1123
    //   1795: astore 10
    //   1797: aload_3
    //   1798: aload 10
    //   1800: invokespecial 1125	com/truecaller/messaging/transport/mms/ao:b	(Ljava/lang/String;)V
    //   1803: aload_3
    //   1804: getfield 146	com/truecaller/messaging/transport/mms/ao:j	Lcom/truecaller/androidactors/f;
    //   1807: invokeinterface 495 1 0
    //   1812: checkcast 1023	com/truecaller/messaging/data/t
    //   1815: astore 10
    //   1817: aload 10
    //   1819: aload 4
    //   1821: invokeinterface 1026 2 0
    //   1826: goto +11 -> 1837
    //   1829: astore 4
    //   1831: aload 18
    //   1833: monitorexit
    //   1834: aload 4
    //   1836: athrow
    //   1837: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1838	0	this	ao
    //   0	1838	1	paramIntent	Intent
    //   0	1838	2	paramInt	int
    //   1	1803	3	localao	ao
    //   3	919	4	localObject1	Object
    //   931	84	4	localObject2	Object
    //   1053	433	4	localObject3	Object
    //   1528	1	4	localObject4	Object
    //   1533	3	4	localIOException	IOException
    //   1543	277	4	localObject5	Object
    //   1829	6	4	localObject6	Object
    //   6	1242	5	i1	int
    //   1265	228	5	bool1	boolean
    //   12	1721	6	localObject7	Object
    //   17	1751	7	localObject8	Object
    //   26	1365	8	bool2	boolean
    //   42	1672	9	localObject9	Object
    //   82	1736	10	localObject10	Object
    //   274	1363	11	i2	int
    //   278	750	12	l1	long
    //   281	1454	14	i3	int
    //   350	525	15	l2	long
    //   357	3	17	bool3	boolean
    //   693	3	17	i4	int
    //   1030	3	17	bool4	boolean
    //   377	1455	18	localObject11	Object
    //   388	1275	19	l3	long
    //   395	251	21	bool5	boolean
    //   680	58	21	i5	int
    //   420	1213	22	localObject12	Object
    //   435	1307	23	localObject13	Object
    //   447	963	24	localObject14	Object
    //   502	3	25	bool6	boolean
    //   601	12	25	i6	int
    //   802	926	25	i7	int
    //   556	7	26	bool7	boolean
    //   606	155	26	i8	int
    //   572	840	27	localObject15	Object
    //   639	947	28	l4	long
    //   673	507	30	i9	int
    //   872	5	31	l5	long
    //   964	3	33	bool8	boolean
    //   1680	5	33	i10	int
    //   1208	206	34	localObject16	Object
    // Exception table:
    //   from	to	target	type
    //   900	905	931	finally
    //   907	914	931	finally
    //   1258	1265	1528	finally
    //   1270	1275	1528	finally
    //   1535	1540	1528	finally
    //   1546	1551	1528	finally
    //   1258	1265	1533	java/io/IOException
    //   1270	1275	1533	java/io/IOException
    //   1204	1208	1829	finally
    //   1210	1215	1829	finally
    //   1219	1227	1829	finally
    //   1227	1230	1829	finally
    //   1831	1834	1829	finally
  }
  
  public final void a(BinaryEntity paramBinaryEntity)
  {
    paramBinaryEntity = new java/lang/IllegalStateException;
    paramBinaryEntity.<init>("Mms transport can not be used to cancel attachments.");
    throw paramBinaryEntity;
  }
  
  public final void a(Message paramMessage, String paramString1, String paramString2)
  {
    paramMessage = new java/lang/IllegalStateException;
    paramMessage.<init>("MMS transport does not support sending reactions");
    throw paramMessage;
  }
  
  public final void a(org.a.a.b paramb)
  {
    com.truecaller.messaging.h localh = i;
    long l1 = fa;
    localh.a(1, l1);
  }
  
  public final boolean a(Message paramMessage)
  {
    Object localObject1 = (MmsTransportInfo)m;
    boolean bool1 = a((MmsTransportInfo)localObject1);
    if (bool1) {
      return false;
    }
    localObject1 = (MmsTransportInfo)m;
    int i1 = 1;
    paramMessage = a(paramMessage, (MmsTransportInfo)localObject1, i1);
    if (paramMessage == null) {
      return false;
    }
    localObject1 = m).e;
    if (localObject1 == null) {
      return false;
    }
    localObject1 = (MmsTransportInfo)m;
    int i2 = g;
    int i3 = 130;
    if (i2 == i3)
    {
      String str = c.f;
      Object localObject2 = "+";
      boolean bool2 = str.startsWith((String)localObject2);
      if (bool2) {
        str = str.substring(i1);
      }
      try
      {
        int i4 = f & i1;
        if (i4 == 0)
        {
          localObject2 = com.truecaller.tracking.events.u.b();
          Object localObject3 = "mms";
          localObject2 = ((u.a)localObject2).d((CharSequence)localObject3);
          localObject3 = e;
          localObject1 = ((MmsTransportInfo)localObject1).a((org.a.a.b)localObject3);
          localObject1 = ((u.a)localObject2).a((CharSequence)localObject1);
          localObject1 = ((u.a)localObject1).b(str);
          paramMessage = c;
          paramMessage = paramMessage.j();
          paramMessage = ((u.a)localObject1).c(paramMessage);
          paramMessage = paramMessage.a();
          localObject1 = s;
          localObject1 = ((com.truecaller.androidactors.f)localObject1).a();
          localObject1 = (ae)localObject1;
          ((ae)localObject1).a(paramMessage);
        }
      }
      catch (org.apache.a.a paramMessage)
      {
        localObject1 = new String[0];
        AssertionUtil.shouldNeverHappen(paramMessage, (String[])localObject1);
      }
    }
    return i1;
  }
  
  public final boolean a(Message paramMessage, Entity paramEntity)
  {
    return false;
  }
  
  public final boolean a(Participant paramParticipant)
  {
    int i1 = c;
    int i2 = 3;
    return i1 != i2;
  }
  
  public final boolean a(String paramString, com.truecaller.messaging.transport.a parama)
  {
    parama.a(0, 0, 0, 1);
    return false;
  }
  
  public final void b(long paramLong)
  {
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("MMS transport does not support retry");
    throw localIllegalStateException;
  }
  
  public final boolean b(Message paramMessage)
  {
    return g();
  }
  
  public final boolean b(ad paramad)
  {
    boolean bool1 = paramad.a();
    if (!bool1)
    {
      paramad = b;
      String str = c;
      boolean bool2 = paramad.equals(str);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean c()
  {
    com.truecaller.utils.l locall = v;
    String[] arrayOfString = { "android.permission.READ_SMS" };
    boolean bool = locall.a(arrayOfString);
    if (bool)
    {
      bool = g();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean c(Message paramMessage)
  {
    return true;
  }
  
  public final com.truecaller.messaging.transport.k d(Message paramMessage)
  {
    ao localao = this;
    Object localObject1 = paramMessage;
    Object localObject5 = l;
    int i1 = k.g((String)localObject5);
    Object localObject6 = "Unknown";
    int i2 = 1;
    int i3 = 2;
    if (i1 == i2) {
      localObject6 = "On";
    } else if (i1 == i3) {
      localObject6 = "Off";
    }
    localObject5 = new com/truecaller/analytics/e$a;
    ((e.a)localObject5).<init>("SendMmsMobileDataState");
    Object localObject7 = "State";
    localObject5 = ((e.a)localObject5).a((String)localObject7, (String)localObject6).a();
    localObject6 = u;
    ((com.truecaller.analytics.b)localObject6).a((com.truecaller.analytics.e)localObject5);
    localObject5 = m;
    i1 = ((com.truecaller.utils.d)localObject5).h();
    int i4 = 21;
    if (i1 < i4)
    {
      localObject5 = k;
      localObject6 = l;
      i1 = ((com.truecaller.multisim.h)localObject5).g((String)localObject6);
      if (i1 == i3)
      {
        new String[1][0] = "Mobile data is disabled. Skip MMS data download";
        return k.b.a;
      }
    }
    localObject5 = (MmsTransportInfo)m;
    i4 = 0;
    localObject6 = null;
    localObject7 = new String[0];
    AssertionUtil.AlwaysFatal.isNotNull(localObject5, (String[])localObject7);
    localObject7 = e;
    Object localObject8 = new String[0];
    AssertionUtil.AlwaysFatal.isNotNull(localObject7, (String[])localObject8);
    localObject7 = e;
    List localList = localao.a((Uri)localObject7);
    if (localList == null) {
      return k.b.a;
    }
    localObject7 = null;
    Object localObject13;
    try
    {
      localObject8 = h;
      localObject8 = ((Context)localObject8).getContentResolver();
      Object localObject9 = e;
      String[] arrayOfString = e;
      int i5 = 0;
      int i6 = 0;
      Object localObject10 = null;
      int i7 = 0;
      Uri localUri = null;
      Object localObject11 = localObject8;
      Object localObject12 = ((ContentResolver)localObject8).query((Uri)localObject9, arrayOfString, null, null, null);
      if (localObject12 != null) {
        try
        {
          boolean bool = ((Cursor)localObject12).moveToFirst();
          long l1;
          String str;
          if (bool)
          {
            localObject11 = new java/lang/StringBuilder;
            localObject9 = "content://mms/";
            ((StringBuilder)localObject11).<init>((String)localObject9);
            l1 = b;
            ((StringBuilder)localObject11).append(l1);
            localObject9 = "/part";
            ((StringBuilder)localObject11).append((String)localObject9);
            localObject11 = ((StringBuilder)localObject11).toString();
            localObject9 = Uri.parse((String)localObject11);
            arrayOfString = ag.a;
            i5 = 0;
            i6 = 0;
            localObject10 = null;
            i7 = 0;
            localUri = null;
            localObject11 = localObject8;
            localObject7 = ((ContentResolver)localObject8).query((Uri)localObject9, arrayOfString, null, null, null);
            if (localObject7 == null)
            {
              localObject1 = k.b.a;
              com.truecaller.util.q.a((Cursor)localObject12);
              com.truecaller.util.q.a((Cursor)localObject7);
              return (com.truecaller.messaging.transport.k)localObject1;
            }
            localObject8 = o;
            localObject11 = ((Cursor)localObject12).getString(i2);
            l1 = ((Cursor)localObject12).getLong(0);
            i5 = ((Cursor)localObject12).getInt(i3);
            i4 = 3;
            i6 = ((Cursor)localObject12).getInt(i4);
            i4 = 4;
            i7 = ((Cursor)localObject12).getInt(i4);
            localObject6 = new com/truecaller/messaging/transport/mms/ag;
            ((ag)localObject6).<init>((Cursor)localObject7);
            str = l;
            localObject13 = localObject12;
            localObject12 = localObject6;
          }
          long l2;
          long l3;
          long l4;
          localObject13 = localObject12;
        }
        finally
        {
          try
          {
            localObject6 = ((h)localObject8).a((String)localObject11, l1, i5, i6, i7, localList, (h.a)localObject6, str);
            localObject10 = localObject6;
            localObject10 = (com.android.a.a.a.u)localObject6;
            if (localObject10 == null)
            {
              localObject1 = k.b.a;
              com.truecaller.util.q.a((Cursor)localObject13);
              com.truecaller.util.q.a((Cursor)localObject7);
              return (com.truecaller.messaging.transport.k)localObject1;
            }
            com.truecaller.util.q.a((Cursor)localObject13);
            com.truecaller.util.q.a((Cursor)localObject7);
            localObject6 = l;
            str = l;
            localObject13 = k;
            localObject6 = ((ak)localObject6).a(str, (com.truecaller.multisim.h)localObject13).a();
            localObject8 = localObject6;
            localObject8 = (ai)localObject6;
            l2 = b;
            l3 = e.a;
            localUri = e;
            ((ai)localObject8).a(l2, l3, (com.android.a.a.a.u)localObject10, localUri);
            localObject1 = new com/truecaller/messaging/transport/mms/ao$a;
            l4 = b;
            ((ao.a)localObject1).<init>(l4);
            x = ((ao.a)localObject1);
            return x;
          }
          finally
          {
            k.b localb;
            break label757;
          }
          localObject2 = finally;
          localObject13 = localObject12;
          break label757;
        }
      }
      localb = k.b.a;
      com.truecaller.util.q.a((Cursor)localObject12);
      com.truecaller.util.q.a(null);
      return localb;
    }
    finally
    {
      i3 = 0;
      localObject13 = null;
    }
    label757:
    com.truecaller.util.q.a((Cursor)localObject13);
    com.truecaller.util.q.a((Cursor)localObject7);
    throw ((Throwable)localObject4);
  }
  
  /* Error */
  public final org.a.a.b d()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 144	com/truecaller/messaging/transport/mms/ao:i	Lcom/truecaller/messaging/h;
    //   4: astore_1
    //   5: iconst_1
    //   6: istore_2
    //   7: aload_1
    //   8: iload_2
    //   9: invokeinterface 879 2 0
    //   14: lstore_3
    //   15: aload_0
    //   16: getfield 140	com/truecaller/messaging/transport/mms/ao:y	Z
    //   19: istore 5
    //   21: iload 5
    //   23: ifne +315 -> 338
    //   26: aload_0
    //   27: getfield 168	com/truecaller/messaging/transport/mms/ao:v	Lcom/truecaller/utils/l;
    //   30: astore_1
    //   31: iconst_1
    //   32: anewarray 81	java/lang/String
    //   35: dup
    //   36: iconst_0
    //   37: ldc_w 1315
    //   40: aastore
    //   41: astore 6
    //   43: aload_1
    //   44: aload 6
    //   46: invokeinterface 958 2 0
    //   51: istore 5
    //   53: iload 5
    //   55: ifeq +283 -> 338
    //   58: aload_0
    //   59: getfield 168	com/truecaller/messaging/transport/mms/ao:v	Lcom/truecaller/utils/l;
    //   62: astore_1
    //   63: iconst_1
    //   64: anewarray 81	java/lang/String
    //   67: dup
    //   68: iconst_0
    //   69: ldc_w 953
    //   72: aastore
    //   73: astore 6
    //   75: aload_1
    //   76: aload 6
    //   78: invokeinterface 958 2 0
    //   83: istore 5
    //   85: iload 5
    //   87: ifeq +251 -> 338
    //   90: aload_0
    //   91: getfield 142	com/truecaller/messaging/transport/mms/ao:h	Landroid/content/Context;
    //   94: invokevirtual 283	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   97: astore_1
    //   98: aconst_null
    //   99: astore 7
    //   101: getstatic 121	com/truecaller/messaging/transport/mms/ao:b	Landroid/net/Uri;
    //   104: astore 8
    //   106: ldc 71
    //   108: astore 6
    //   110: iconst_1
    //   111: anewarray 81	java/lang/String
    //   114: dup
    //   115: iconst_0
    //   116: aload 6
    //   118: aastore
    //   119: astore 9
    //   121: ldc_w 1317
    //   124: astore 10
    //   126: aconst_null
    //   127: astore 11
    //   129: ldc_w 1319
    //   132: astore 12
    //   134: aload_1
    //   135: astore 6
    //   137: aload_1
    //   138: aload 8
    //   140: aload 9
    //   142: aload 10
    //   144: aconst_null
    //   145: aload 12
    //   147: invokevirtual 562	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   150: astore 6
    //   152: aload 6
    //   154: ifnull +143 -> 297
    //   157: aload 6
    //   159: invokeinterface 567 1 0
    //   164: istore 13
    //   166: iload 13
    //   168: ifeq +129 -> 297
    //   171: iconst_0
    //   172: istore 13
    //   174: aconst_null
    //   175: astore 8
    //   177: aload 6
    //   179: iconst_0
    //   180: invokeinterface 842 2 0
    //   185: lstore 14
    //   187: ldc2_w 652
    //   190: lstore 16
    //   192: lload 14
    //   194: lload 16
    //   196: lmul
    //   197: lstore 14
    //   199: new 322	android/content/ContentValues
    //   202: astore 10
    //   204: aload 10
    //   206: invokespecial 323	android/content/ContentValues:<init>	()V
    //   209: ldc_w 911
    //   212: astore 11
    //   214: iconst_5
    //   215: istore 18
    //   217: iload 18
    //   219: invokestatic 528	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   222: astore 12
    //   224: aload 10
    //   226: aload 11
    //   228: aload 12
    //   230: invokevirtual 532	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   233: getstatic 121	com/truecaller/messaging/transport/mms/ao:b	Landroid/net/Uri;
    //   236: astore 11
    //   238: ldc_w 1317
    //   241: astore 12
    //   243: aload_1
    //   244: aload 11
    //   246: aload 10
    //   248: aload 12
    //   250: aconst_null
    //   251: invokevirtual 538	android/content/ContentResolver:update	(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   254: pop
    //   255: lload 14
    //   257: lload_3
    //   258: lcmp
    //   259: istore 5
    //   261: iload 5
    //   263: ifge +6 -> 269
    //   266: lload 14
    //   268: lstore_3
    //   269: aload_0
    //   270: getfield 144	com/truecaller/messaging/transport/mms/ao:i	Lcom/truecaller/messaging/h;
    //   273: astore_1
    //   274: aload_1
    //   275: iload_2
    //   276: lload_3
    //   277: invokeinterface 887 4 0
    //   282: goto +15 -> 297
    //   285: astore_1
    //   286: goto +45 -> 331
    //   289: astore_1
    //   290: aload 6
    //   292: astore 7
    //   294: goto +20 -> 314
    //   297: aload 6
    //   299: invokestatic 586	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   302: goto +21 -> 323
    //   305: astore_1
    //   306: aload 7
    //   308: astore 6
    //   310: goto +21 -> 331
    //   313: astore_1
    //   314: aload_1
    //   315: invokestatic 428	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   318: aload 7
    //   320: invokestatic 586	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   323: aload_0
    //   324: iload_2
    //   325: putfield 140	com/truecaller/messaging/transport/mms/ao:y	Z
    //   328: goto +10 -> 338
    //   331: aload 6
    //   333: invokestatic 586	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   336: aload_1
    //   337: athrow
    //   338: new 633	org/a/a/b
    //   341: astore_1
    //   342: aload_1
    //   343: lload_3
    //   344: invokespecial 845	org/a/a/b:<init>	(J)V
    //   347: aload_1
    //   348: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	349	0	this	ao
    //   4	271	1	localObject1	Object
    //   285	1	1	localObject2	Object
    //   289	1	1	localRuntimeException1	RuntimeException
    //   305	1	1	localObject3	Object
    //   313	24	1	localRuntimeException2	RuntimeException
    //   341	7	1	localb	org.a.a.b
    //   6	319	2	i1	int
    //   14	330	3	l1	long
    //   19	243	5	bool1	boolean
    //   41	291	6	localObject4	Object
    //   99	220	7	localObject5	Object
    //   104	72	8	localUri	Uri
    //   119	22	9	arrayOfString	String[]
    //   124	123	10	localObject6	Object
    //   127	118	11	localObject7	Object
    //   132	117	12	localObject8	Object
    //   164	9	13	bool2	boolean
    //   185	82	14	l2	long
    //   190	5	16	l3	long
    //   215	3	18	i2	int
    // Exception table:
    //   from	to	target	type
    //   157	164	285	finally
    //   179	185	285	finally
    //   199	202	285	finally
    //   204	209	285	finally
    //   217	222	285	finally
    //   228	233	285	finally
    //   233	236	285	finally
    //   250	255	285	finally
    //   269	273	285	finally
    //   276	282	285	finally
    //   157	164	289	java/lang/RuntimeException
    //   179	185	289	java/lang/RuntimeException
    //   199	202	289	java/lang/RuntimeException
    //   204	209	289	java/lang/RuntimeException
    //   217	222	289	java/lang/RuntimeException
    //   228	233	289	java/lang/RuntimeException
    //   233	236	289	java/lang/RuntimeException
    //   250	255	289	java/lang/RuntimeException
    //   269	273	289	java/lang/RuntimeException
    //   276	282	289	java/lang/RuntimeException
    //   101	104	305	finally
    //   110	119	305	finally
    //   145	150	305	finally
    //   314	318	305	finally
    //   101	104	313	java/lang/RuntimeException
    //   110	119	313	java/lang/RuntimeException
    //   145	150	313	java/lang/RuntimeException
  }
  
  public final int e(Message paramMessage)
  {
    paramMessage = (MmsTransportInfo)m;
    synchronized (q)
    {
      Set localSet = q;
      long l1 = b;
      Long localLong = Long.valueOf(l1);
      boolean bool1 = localSet.contains(localLong);
      if (bool1)
      {
        int i2 = 2;
        return i2;
      }
      int i3 = g;
      int i1 = 130;
      if (i3 == i1)
      {
        ??? = p;
        long l2 = org.a.a.e.a();
        boolean bool3 = ((org.a.a.a.c)???).d(l2);
        if (!bool3)
        {
          boolean bool2 = b(paramMessage);
          if (bool2) {
            return 3;
          }
          return 1;
        }
      }
      return 0;
    }
  }
  
  public final boolean e()
  {
    return true;
  }
  
  public final int f()
  {
    return 1;
  }
  
  public final boolean f(Message paramMessage)
  {
    int i1 = j;
    String[] arrayOfString = null;
    int i2 = 1;
    if (i1 == i2) {
      i1 = 1;
    } else {
      i1 = 0;
    }
    arrayOfString = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(i1, arrayOfString);
    return a(paramMessage, i2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.ao
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */