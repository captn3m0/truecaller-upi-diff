package com.truecaller.messaging.transport.mms;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Handler;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.multisim.z;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

final class ad
  extends ac
{
  private final Method c;
  private final Method d;
  private final z e;
  
  ad(Context paramContext, Handler paramHandler, ConnectivityManager paramConnectivityManager, z paramz)
  {
    super(paramContext, paramHandler, paramConnectivityManager, paramz);
    paramContext = paramConnectivityManager.getClass();
    paramHandler = null;
    paramConnectivityManager = "startUsingNetworkFeatureGemini";
    int i = 3;
    try
    {
      Object localObject = new Class[i];
      Class localClass = Integer.TYPE;
      localObject[0] = localClass;
      localClass = String.class;
      int j = 1;
      localObject[j] = localClass;
      localClass = Integer.TYPE;
      int k = 2;
      localObject[k] = localClass;
      paramConnectivityManager = paramContext.getMethod(paramConnectivityManager, (Class[])localObject);
      localObject = "stopUsingNetworkFeatureGemini";
      Class[] arrayOfClass = new Class[i];
      localClass = Integer.TYPE;
      arrayOfClass[0] = localClass;
      localClass = String.class;
      arrayOfClass[j] = localClass;
      localClass = Integer.TYPE;
      arrayOfClass[k] = localClass;
      paramContext = paramContext.getMethod((String)localObject, arrayOfClass);
    }
    catch (NoSuchMethodException paramContext)
    {
      paramHandler = new String[0];
      AssertionUtil.OnlyInDebug.shouldNeverHappen(paramContext, paramHandler);
      paramConnectivityManager = null;
      paramContext = null;
    }
    c = paramConnectivityManager;
    d = paramContext;
    e = paramz;
  }
  
  protected final int a(String paramString, boolean paramBoolean)
  {
    Object localObject1 = e;
    int i = ((z)localObject1).h(paramString);
    int j = -1;
    if (i == j) {
      return super.a(paramString, paramBoolean);
    }
    paramString = c;
    paramBoolean = true;
    if (paramString != null) {
      try
      {
        ConnectivityManager localConnectivityManager = a;
        Object[] arrayOfObject = new Object[paramBoolean];
        int k = 0;
        Object localObject2 = Integer.valueOf(0);
        arrayOfObject[0] = localObject2;
        k = 1;
        localObject2 = "enableMMS";
        arrayOfObject[k] = localObject2;
        k = 2;
        localObject1 = Integer.valueOf(i);
        arrayOfObject[k] = localObject1;
        paramString = paramString.invoke(localConnectivityManager, arrayOfObject);
        paramString = (Integer)paramString;
        return paramString.intValue();
      }
      catch (IllegalAccessException paramString)
      {
        AssertionUtil.reportThrowableButNeverCrash(paramString);
      }
      catch (InvocationTargetException paramString)
      {
        AssertionUtil.reportThrowableButNeverCrash(paramString);
      }
    }
    return paramBoolean;
  }
  
  protected final int c(String paramString)
  {
    Object localObject1 = e;
    int i = ((z)localObject1).h(paramString);
    int j = -1;
    if (i == j) {
      return super.c(paramString);
    }
    paramString = d;
    j = 3;
    if (paramString != null) {
      try
      {
        ConnectivityManager localConnectivityManager = a;
        Object[] arrayOfObject = new Object[j];
        int k = 0;
        Object localObject2 = Integer.valueOf(0);
        arrayOfObject[0] = localObject2;
        k = 1;
        localObject2 = "enableMMS";
        arrayOfObject[k] = localObject2;
        k = 2;
        localObject1 = Integer.valueOf(i);
        arrayOfObject[k] = localObject1;
        paramString = paramString.invoke(localConnectivityManager, arrayOfObject);
        paramString = (Integer)paramString;
        return paramString.intValue();
      }
      catch (IllegalAccessException paramString)
      {
        AssertionUtil.reportThrowableButNeverCrash(paramString);
      }
      catch (InvocationTargetException paramString)
      {
        AssertionUtil.reportThrowableButNeverCrash(paramString);
      }
    }
    return j;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */