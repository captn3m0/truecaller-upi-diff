package com.truecaller.messaging.transport.mms;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.SparseArray;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.TransportInfo;
import org.a.a.a.g;
import org.a.a.b;
import org.c.a.a.a.k;

public final class MmsTransportInfo
  implements TransportInfo
{
  public static final Parcelable.Creator CREATOR;
  public final int A;
  public final int B;
  public final boolean C;
  boolean D;
  final SparseArray E;
  public final long a;
  public final long b;
  public final int c;
  public final long d;
  public final Uri e;
  public final int f;
  public final int g;
  public final String h;
  public final int i;
  public final String j;
  public final int k;
  public final Uri l;
  public final String m;
  public final int n;
  public final String o;
  public final b p;
  public final int q;
  public final int r;
  public final int s;
  public final String t;
  public final String u;
  public final String v;
  public final int w;
  public final int x;
  public final int y;
  public final long z;
  
  static
  {
    MmsTransportInfo.1 local1 = new com/truecaller/messaging/transport/mms/MmsTransportInfo$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  private MmsTransportInfo(Parcel paramParcel)
  {
    long l1 = paramParcel.readLong();
    a = l1;
    l1 = paramParcel.readLong();
    b = l1;
    int i1 = paramParcel.readInt();
    c = i1;
    l1 = paramParcel.readLong();
    d = l1;
    Object localObject = Uri.class.getClassLoader();
    localObject = (Uri)paramParcel.readParcelable((ClassLoader)localObject);
    e = ((Uri)localObject);
    i1 = paramParcel.readInt();
    f = i1;
    localObject = paramParcel.readString();
    h = ((String)localObject);
    i1 = paramParcel.readInt();
    i = i1;
    localObject = paramParcel.readString();
    j = ((String)localObject);
    i1 = paramParcel.readInt();
    k = i1;
    localObject = Uri.class.getClassLoader();
    localObject = (Uri)paramParcel.readParcelable((ClassLoader)localObject);
    l = ((Uri)localObject);
    localObject = paramParcel.readString();
    m = ((String)localObject);
    i1 = paramParcel.readInt();
    n = i1;
    localObject = paramParcel.readString();
    o = ((String)localObject);
    localObject = new org/a/a/b;
    long l2 = paramParcel.readLong();
    ((b)localObject).<init>(l2);
    p = ((b)localObject);
    i1 = paramParcel.readInt();
    q = i1;
    i1 = paramParcel.readInt();
    r = i1;
    i1 = paramParcel.readInt();
    s = i1;
    localObject = paramParcel.readString();
    t = ((String)localObject);
    localObject = paramParcel.readString();
    u = ((String)localObject);
    localObject = paramParcel.readString();
    v = ((String)localObject);
    i1 = paramParcel.readInt();
    w = i1;
    i1 = paramParcel.readInt();
    g = i1;
    i1 = paramParcel.readInt();
    x = i1;
    i1 = paramParcel.readInt();
    y = i1;
    l1 = paramParcel.readLong();
    z = l1;
    i1 = paramParcel.readInt();
    A = i1;
    i1 = paramParcel.readInt();
    B = i1;
    i1 = paramParcel.readInt();
    boolean bool = true;
    if (i1 != 0)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      localObject = null;
    }
    C = i1;
    int i2 = paramParcel.readInt();
    if (i2 == 0) {
      bool = false;
    }
    D = bool;
    E = null;
  }
  
  private MmsTransportInfo(MmsTransportInfo.a parama)
  {
    long l1 = a;
    a = l1;
    l1 = b;
    b = l1;
    int i1 = c;
    c = i1;
    l1 = d;
    d = l1;
    Object localObject = e;
    e = ((Uri)localObject);
    i1 = f;
    f = i1;
    localObject = g;
    h = ((String)localObject);
    i1 = h;
    i = i1;
    localObject = i;
    j = ((String)localObject);
    i1 = j;
    k = i1;
    localObject = k;
    l = ((Uri)localObject);
    localObject = k.n(p);
    o = ((String)localObject);
    localObject = q;
    if (localObject == null)
    {
      localObject = new org/a/a/b;
      long l2 = 0L;
      ((b)localObject).<init>(l2);
    }
    else
    {
      localObject = q;
    }
    p = ((b)localObject);
    i1 = r;
    q = i1;
    i1 = s;
    r = i1;
    i1 = t;
    s = i1;
    localObject = k.n(u);
    v = ((String)localObject);
    i1 = v;
    w = i1;
    i1 = w;
    g = i1;
    i1 = x;
    x = i1;
    i1 = y;
    y = i1;
    l1 = z;
    z = l1;
    localObject = k.n(l);
    m = ((String)localObject);
    i1 = m;
    n = i1;
    localObject = n;
    t = ((String)localObject);
    localObject = k.n(o);
    u = ((String)localObject);
    i1 = A;
    A = i1;
    i1 = B;
    B = i1;
    boolean bool = C;
    C = bool;
    bool = D;
    D = bool;
    parama = E;
    E = parama;
  }
  
  static int a(int paramInt)
  {
    int i1 = paramInt & 0x1;
    if (i1 != 0)
    {
      i1 = paramInt & 0x8;
      if (i1 != 0) {
        return 5;
      }
      i1 = paramInt & 0x4;
      int i2 = 4;
      if (i1 == 0)
      {
        paramInt &= 0x10;
        if (paramInt == 0) {
          return i2;
        }
      }
      return i2;
    }
    return 1;
  }
  
  static int a(int paramInt1, int paramInt2, int paramInt3)
  {
    int i1 = 9;
    switch (paramInt1)
    {
    case 3: 
    default: 
      break;
    case 4: 
      i1 = 5;
      break;
    case 2: 
      paramInt1 = 1;
      if (paramInt3 != 0)
      {
        paramInt2 = 128;
        if (paramInt3 != paramInt2) {}
      }
      else
      {
        i1 = 1;
      }
      break;
    case 1: 
      paramInt1 = 130;
      if (paramInt2 == paramInt1) {
        i1 = 4;
      } else {
        i1 = 0;
      }
      break;
    }
    return i1;
  }
  
  public final int a()
  {
    return 0;
  }
  
  public final String a(b paramb)
  {
    return Message.a(b, paramb);
  }
  
  public final int b()
  {
    return 0;
  }
  
  public final long c()
  {
    return a;
  }
  
  public final long d()
  {
    return b;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final long e()
  {
    return d;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        paramObject = (MmsTransportInfo)paramObject;
        long l1 = a;
        long l2 = a;
        boolean bool2 = l1 < l2;
        if (bool2) {
          return false;
        }
        l1 = b;
        l2 = b;
        bool2 = l1 < l2;
        if (bool2) {
          return false;
        }
        int i1 = c;
        int i2 = c;
        if (i1 != i2) {
          return false;
        }
        i1 = f;
        i2 = f;
        if (i1 != i2) {
          return false;
        }
        i1 = g;
        i2 = g;
        if (i1 != i2) {
          return false;
        }
        i1 = i;
        i2 = i;
        if (i1 != i2) {
          return false;
        }
        i1 = k;
        i2 = k;
        if (i1 != i2) {
          return false;
        }
        i1 = n;
        i2 = n;
        if (i1 != i2) {
          return false;
        }
        i1 = q;
        i2 = q;
        if (i1 != i2) {
          return false;
        }
        i1 = r;
        i2 = r;
        if (i1 != i2) {
          return false;
        }
        i1 = s;
        i2 = s;
        if (i1 != i2) {
          return false;
        }
        i1 = w;
        i2 = w;
        if (i1 != i2) {
          return false;
        }
        i1 = x;
        i2 = x;
        if (i1 != i2) {
          return false;
        }
        i1 = y;
        i2 = y;
        if (i1 != i2) {
          return false;
        }
        l1 = z;
        l2 = z;
        bool2 = l1 < l2;
        if (bool2) {
          return false;
        }
        i1 = A;
        i2 = A;
        if (i1 != i2) {
          return false;
        }
        i1 = B;
        i2 = B;
        if (i1 != i2) {
          return false;
        }
        boolean bool3 = C;
        boolean bool4 = C;
        if (bool3 != bool4) {
          return false;
        }
        bool3 = D;
        bool4 = D;
        if (bool3 != bool4) {
          return false;
        }
        localObject1 = e;
        if (localObject1 != null)
        {
          localObject2 = e;
          bool3 = ((Uri)localObject1).equals(localObject2);
          if (bool3) {
            break label494;
          }
        }
        else
        {
          localObject1 = e;
          if (localObject1 == null) {
            break label494;
          }
        }
        return false;
        label494:
        localObject1 = h;
        if (localObject1 != null)
        {
          localObject2 = h;
          bool3 = ((String)localObject1).equals(localObject2);
          if (bool3) {
            break label536;
          }
        }
        else
        {
          localObject1 = h;
          if (localObject1 == null) {
            break label536;
          }
        }
        return false;
        label536:
        localObject1 = j;
        if (localObject1 != null)
        {
          localObject2 = j;
          bool3 = ((String)localObject1).equals(localObject2);
          if (bool3) {
            break label578;
          }
        }
        else
        {
          localObject1 = j;
          if (localObject1 == null) {
            break label578;
          }
        }
        return false;
        label578:
        localObject1 = l;
        if (localObject1 != null)
        {
          localObject2 = l;
          bool3 = ((Uri)localObject1).equals(localObject2);
          if (bool3) {
            break label620;
          }
        }
        else
        {
          localObject1 = l;
          if (localObject1 == null) {
            break label620;
          }
        }
        return false;
        label620:
        localObject1 = m;
        localObject2 = m;
        bool3 = ((String)localObject1).equals(localObject2);
        if (!bool3) {
          return false;
        }
        localObject1 = o;
        localObject2 = o;
        bool3 = ((String)localObject1).equals(localObject2);
        if (!bool3) {
          return false;
        }
        localObject1 = p;
        localObject2 = p;
        bool3 = ((b)localObject1).equals(localObject2);
        if (!bool3) {
          return false;
        }
        localObject1 = t;
        localObject2 = t;
        bool3 = k.a((CharSequence)localObject1, (CharSequence)localObject2);
        if (!bool3) {
          return false;
        }
        localObject1 = u;
        localObject2 = u;
        bool3 = ((String)localObject1).equals(localObject2);
        if (!bool3) {
          return false;
        }
        localObject1 = v;
        paramObject = v;
        boolean bool5 = k.a((CharSequence)localObject1, (CharSequence)paramObject);
        if (!bool5) {
          return false;
        }
        return bool1;
      }
    }
    return false;
  }
  
  public final boolean f()
  {
    return true;
  }
  
  public final MmsTransportInfo.a g()
  {
    MmsTransportInfo.a locala = new com/truecaller/messaging/transport/mms/MmsTransportInfo$a;
    locala.<init>(this, (byte)0);
    return locala;
  }
  
  public final int h()
  {
    int i1 = w;
    int i2 = g;
    int i3 = s;
    return a(i1, i2, i3);
  }
  
  public final int hashCode()
  {
    long l1 = a;
    int i1 = 32;
    long l2 = l1 >>> i1;
    l1 ^= l2;
    int i2 = (int)l1 * 31;
    l2 = b;
    long l3 = l2 >>> i1;
    l2 ^= l3;
    int i3 = (int)l2;
    i2 = (i2 + i3) * 31;
    i3 = c;
    i2 = (i2 + i3) * 31;
    Object localObject = e;
    int i6 = 0;
    if (localObject != null)
    {
      i3 = ((Uri)localObject).hashCode();
    }
    else
    {
      i3 = 0;
      localObject = null;
    }
    i2 = (i2 + i3) * 31;
    i3 = f;
    i2 = (i2 + i3) * 31;
    i3 = g;
    i2 = (i2 + i3) * 31;
    localObject = h;
    if (localObject != null)
    {
      i3 = ((String)localObject).hashCode();
    }
    else
    {
      i3 = 0;
      localObject = null;
    }
    i2 = (i2 + i3) * 31;
    i3 = i;
    i2 = (i2 + i3) * 31;
    localObject = j;
    if (localObject != null)
    {
      i3 = ((String)localObject).hashCode();
    }
    else
    {
      i3 = 0;
      localObject = null;
    }
    i2 = (i2 + i3) * 31;
    i3 = k;
    i2 = (i2 + i3) * 31;
    localObject = l;
    if (localObject != null) {
      i6 = ((Uri)localObject).hashCode();
    }
    i2 = (i2 + i6) * 31;
    i3 = m.hashCode();
    i2 = (i2 + i3) * 31;
    i3 = n;
    i2 = (i2 + i3) * 31;
    i3 = o.hashCode();
    i2 = (i2 + i3) * 31;
    i3 = p.hashCode();
    i2 = (i2 + i3) * 31;
    i3 = q;
    i2 = (i2 + i3) * 31;
    i3 = r;
    i2 = (i2 + i3) * 31;
    i3 = s;
    i2 = (i2 + i3) * 31;
    i3 = t.hashCode();
    i2 = (i2 + i3) * 31;
    i3 = u.hashCode();
    i2 = (i2 + i3) * 31;
    i3 = v.hashCode();
    i2 = (i2 + i3) * 31;
    i3 = w;
    i2 = (i2 + i3) * 31;
    i3 = x;
    i2 = (i2 + i3) * 31;
    i3 = y;
    i2 = (i2 + i3) * 31;
    l2 = z;
    l3 = l2 >>> i1;
    i3 = (int)(l2 ^ l3);
    i2 = (i2 + i3) * 31;
    i3 = A;
    i2 = (i2 + i3) * 31;
    i3 = B;
    i2 = (i2 + i3) * 31;
    int i4 = C;
    i2 = (i2 + i4) * 31;
    int i5 = D;
    return i2 + i5;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("{ type : mms, messageId: ");
    long l1 = a;
    localStringBuilder.append(l1);
    localStringBuilder.append(", uri: \"");
    String str = String.valueOf(e);
    localStringBuilder.append(str);
    localStringBuilder.append("\" }");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    long l1 = a;
    paramParcel.writeLong(l1);
    l1 = b;
    paramParcel.writeLong(l1);
    paramInt = c;
    paramParcel.writeInt(paramInt);
    l1 = d;
    paramParcel.writeLong(l1);
    Object localObject = e;
    paramParcel.writeParcelable((Parcelable)localObject, 0);
    paramInt = f;
    paramParcel.writeInt(paramInt);
    localObject = h;
    paramParcel.writeString((String)localObject);
    paramInt = i;
    paramParcel.writeInt(paramInt);
    localObject = j;
    paramParcel.writeString((String)localObject);
    paramInt = k;
    paramParcel.writeInt(paramInt);
    localObject = l;
    paramParcel.writeParcelable((Parcelable)localObject, 0);
    localObject = m;
    paramParcel.writeString((String)localObject);
    paramInt = n;
    paramParcel.writeInt(paramInt);
    localObject = o;
    paramParcel.writeString((String)localObject);
    l1 = p.a;
    paramParcel.writeLong(l1);
    paramInt = q;
    paramParcel.writeInt(paramInt);
    paramInt = r;
    paramParcel.writeInt(paramInt);
    paramInt = s;
    paramParcel.writeInt(paramInt);
    localObject = t;
    paramParcel.writeString((String)localObject);
    localObject = u;
    paramParcel.writeString((String)localObject);
    localObject = v;
    paramParcel.writeString((String)localObject);
    paramInt = w;
    paramParcel.writeInt(paramInt);
    paramInt = g;
    paramParcel.writeInt(paramInt);
    paramInt = x;
    paramParcel.writeInt(paramInt);
    paramInt = y;
    paramParcel.writeInt(paramInt);
    l1 = z;
    paramParcel.writeLong(l1);
    paramInt = A;
    paramParcel.writeInt(paramInt);
    paramInt = B;
    paramParcel.writeInt(paramInt);
    paramInt = C;
    paramParcel.writeInt(paramInt);
    paramInt = D;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.MmsTransportInfo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */