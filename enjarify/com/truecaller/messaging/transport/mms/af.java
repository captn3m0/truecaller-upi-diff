package com.truecaller.messaging.transport.mms;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.ab;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;

final class af
  extends aa
{
  private final Method c;
  private final Method d;
  private final Method e;
  private final Method f;
  private final Method g;
  private final Class h;
  private final Method i;
  private boolean j;
  private final ab k;
  
  af(Context paramContext, Handler paramHandler, ConnectivityManager paramConnectivityManager, ab paramab)
  {
    super(paramContext, paramHandler, paramConnectivityManager, paramab);
    paramContext = null;
    j = false;
    k = paramab;
    paramHandler = paramConnectivityManager.getClass();
    int m = 2;
    paramab = null;
    int n = 1;
    Object localObject1 = "getTypeMobileFromSimSlot";
    Object localObject4;
    Object localObject5;
    try
    {
      localObject4 = new Class[m];
      localObject5 = Integer.TYPE;
      localObject4[0] = localObject5;
      localObject5 = Integer.TYPE;
      localObject4[n] = localObject5;
      localObject1 = paramHandler.getMethod((String)localObject1, (Class[])localObject4);
    }
    catch (NoSuchMethodException localNoSuchMethodException4)
    {
      localObject1 = null;
    }
    d = ((Method)localObject1);
    localObject1 = d;
    if (localObject1 != null)
    {
      localObject1 = "switchDataNetwork";
      try
      {
        localObject4 = new Class[n];
        localObject5 = Integer.TYPE;
        localObject4[0] = localObject5;
        localObject1 = paramHandler.getMethod((String)localObject1, (Class[])localObject4);
      }
      catch (NoSuchMethodException localNoSuchMethodException1)
      {
        localObject4 = new String[0];
        AssertionUtil.OnlyInDebug.shouldNeverHappen(localNoSuchMethodException1, (String[])localObject4);
        break label207;
      }
    }
    else
    {
      Object localObject2 = "switchDataNetworkForMMS";
      try
      {
        localObject4 = new Class[n];
        localObject5 = Integer.TYPE;
        localObject4[0] = localObject5;
        localObject2 = paramHandler.getMethod((String)localObject2, (Class[])localObject4);
      }
      catch (NoSuchMethodException localNoSuchMethodException2)
      {
        localObject4 = new String[0];
        AssertionUtil.OnlyInDebug.shouldNeverHappen(localNoSuchMethodException2, (String[])localObject4);
      }
    }
    label207:
    Object localObject3 = null;
    c = ((Method)localObject3);
    localObject3 = "startUsingNetworkFeature";
    try
    {
      localObject4 = new Class[m];
      localObject5 = Integer.TYPE;
      localObject4[0] = localObject5;
      localObject5 = String.class;
      localObject4[n] = localObject5;
      localObject3 = paramHandler.getMethod((String)localObject3, (Class[])localObject4);
      localObject4 = "stopUsingNetworkFeature";
      localObject5 = new Class[m];
      Class localClass = Integer.TYPE;
      localObject5[0] = localClass;
      localClass = String.class;
      localObject5[n] = localClass;
      localObject4 = paramHandler.getMethod((String)localObject4, (Class[])localObject5);
    }
    catch (NoSuchMethodException localNoSuchMethodException3)
    {
      localObject4 = new String[0];
      AssertionUtil.OnlyInDebug.shouldNeverHappen(localNoSuchMethodException3, (String[])localObject4);
      str = null;
      localObject4 = null;
    }
    e = str;
    f = ((Method)localObject4);
    String str = "requestRouteToHost";
    try
    {
      localObject4 = new Class[m];
      localObject5 = Integer.TYPE;
      localObject4[0] = localObject5;
      localObject5 = Integer.TYPE;
      localObject4[n] = localObject5;
      paramHandler = paramHandler.getMethod(str, (Class[])localObject4);
    }
    catch (NoSuchMethodException localNoSuchMethodException5)
    {
      paramHandler = null;
    }
    g = paramHandler;
    paramHandler = "android.os.SystemProperties";
    try
    {
      paramHandler = Class.forName(paramHandler);
      str = "getInt";
      try
      {
        paramConnectivityManager = new Class[m];
        localObject4 = String.class;
        paramConnectivityManager[0] = localObject4;
        localObject4 = Integer.TYPE;
        paramConnectivityManager[n] = localObject4;
        paramab = paramHandler.getMethod(str, paramConnectivityManager);
      }
      catch (NoSuchMethodException paramConnectivityManager) {}catch (ClassNotFoundException paramConnectivityManager) {}
      paramContext = new String[0];
    }
    catch (NoSuchMethodException paramConnectivityManager)
    {
      paramHandler = null;
      paramContext = new String[0];
      AssertionUtil.OnlyInDebug.shouldNeverHappen(paramConnectivityManager, paramContext);
    }
    catch (ClassNotFoundException paramConnectivityManager)
    {
      paramHandler = null;
    }
    AssertionUtil.OnlyInDebug.shouldNeverHappen(paramConnectivityManager, paramContext);
    h = paramHandler;
    i = paramab;
  }
  
  private int b()
  {
    Object localObject1 = h;
    int m = -1;
    if (localObject1 != null)
    {
      Method localMethod = i;
      if (localMethod != null)
      {
        int n = 2;
        try
        {
          Object[] arrayOfObject = new Object[n];
          int i1 = 0;
          Object localObject2 = "persist.sys.dataprefer.simid";
          arrayOfObject[0] = localObject2;
          i1 = 1;
          localObject2 = Integer.valueOf(m);
          arrayOfObject[i1] = localObject2;
          localObject1 = localMethod.invoke(localObject1, arrayOfObject);
          localObject1 = (Integer)localObject1;
          return ((Integer)localObject1).intValue();
        }
        catch (IllegalAccessException localIllegalAccessException)
        {
          AssertionUtil.reportThrowableButNeverCrash(localIllegalAccessException);
        }
        catch (InvocationTargetException localInvocationTargetException)
        {
          AssertionUtil.reportThrowableButNeverCrash(localInvocationTargetException);
        }
        return m;
      }
    }
    return m;
  }
  
  protected final int a(int paramInt)
  {
    int m = 2;
    if (paramInt == m)
    {
      paramInt = j;
      if (paramInt != 0) {
        return m;
      }
    }
    return 3;
  }
  
  protected final int a(String paramString, boolean paramBoolean)
  {
    new String[1][0] = "Start MMS connectivity";
    Object localObject1 = c;
    int m = 3;
    if (localObject1 != null)
    {
      localObject1 = e;
      if (localObject1 != null)
      {
        localObject1 = k;
        paramString = ((ab)localObject1).b(paramString);
        if (paramString == null) {
          return m;
        }
        try
        {
          boolean bool1 = b();
          boolean bool2 = true;
          Object localObject3;
          if (!paramBoolean)
          {
            paramBoolean = true;
            if (bool1 != paramBoolean)
            {
              paramBoolean = a;
              if (bool1 != paramBoolean)
              {
                localMethod = c;
                localObject1 = a;
                localObject2 = new Object[bool2];
                int i1 = a;
                localObject3 = Integer.valueOf(i1);
                localObject2[0] = localObject3;
                localMethod.invoke(localObject1, (Object[])localObject2);
                j = bool2;
              }
            }
          }
          Method localMethod = d;
          int n = 2;
          int i2;
          if (localMethod != null)
          {
            localMethod = d;
            localObject2 = a;
            localObject3 = new Object[n];
            Integer localInteger = Integer.valueOf(0);
            localObject3[0] = localInteger;
            i2 = a;
            paramString = Integer.valueOf(i2);
            localObject3[bool2] = paramString;
            paramString = localMethod.invoke(localObject2, (Object[])localObject3);
            paramString = (Integer)paramString;
            i2 = paramString.intValue();
          }
          else
          {
            i2 = 0;
            paramString = null;
          }
          localMethod = e;
          Object localObject2 = a;
          localObject1 = new Object[n];
          paramString = Integer.valueOf(i2);
          localObject1[0] = paramString;
          paramString = "enableMMS";
          localObject1[bool2] = paramString;
          paramString = localMethod.invoke(localObject2, (Object[])localObject1);
          paramString = (Integer)paramString;
          return paramString.intValue();
        }
        catch (IllegalAccessException paramString)
        {
          AssertionUtil.reportThrowableButNeverCrash(paramString);
        }
        catch (InvocationTargetException paramString)
        {
          AssertionUtil.reportThrowableButNeverCrash(paramString);
        }
        return m;
      }
    }
    return m;
  }
  
  protected final boolean a(String paramString, InetAddress paramInetAddress)
  {
    Object localObject = g;
    if (localObject == null) {
      return false;
    }
    localObject = k;
    paramString = ((ab)localObject).b(paramString);
    if (paramString == null) {
      return false;
    }
    int m = 1;
    try
    {
      Method localMethod = d;
      int n = 2;
      if (localMethod != null)
      {
        localMethod = d;
        localConnectivityManager = a;
        Object[] arrayOfObject1 = new Object[n];
        Integer localInteger = Integer.valueOf(n);
        arrayOfObject1[0] = localInteger;
        i1 = a;
        paramString = Integer.valueOf(i1);
        arrayOfObject1[m] = paramString;
        paramString = localMethod.invoke(localConnectivityManager, arrayOfObject1);
        paramString = (Integer)paramString;
        i1 = paramString.intValue();
      }
      else
      {
        i1 = 2;
      }
      localMethod = g;
      ConnectivityManager localConnectivityManager = a;
      Object[] arrayOfObject2 = new Object[n];
      paramString = Integer.valueOf(i1);
      arrayOfObject2[0] = paramString;
      int i1 = b(paramInetAddress);
      paramString = Integer.valueOf(i1);
      arrayOfObject2[m] = paramString;
      paramString = localMethod.invoke(localConnectivityManager, arrayOfObject2);
      paramString = (Boolean)paramString;
      return paramString.booleanValue();
    }
    catch (Exception paramString)
    {
      paramInetAddress = new String[m];
      paramString = String.valueOf(paramString);
      paramString = "ConnectivityManager.requestRouteToHost failed ".concat(paramString);
      paramInetAddress[0] = paramString;
    }
    return false;
  }
  
  protected final int c(String paramString)
  {
    new String[1][0] = "End MMS connectivity";
    Object localObject1 = c;
    int m = 3;
    if (localObject1 != null)
    {
      localObject1 = f;
      if (localObject1 != null)
      {
        localObject1 = k;
        paramString = ((ab)localObject1).b(paramString);
        if (paramString == null) {
          return m;
        }
        try
        {
          localObject1 = d;
          int n = 2;
          int i1 = 1;
          if (localObject1 != null)
          {
            localObject1 = d;
            localObject2 = a;
            localObject3 = new Object[n];
            Integer localInteger = Integer.valueOf(0);
            localObject3[0] = localInteger;
            int i2 = a;
            localInteger = Integer.valueOf(i2);
            localObject3[i1] = localInteger;
            localObject1 = ((Method)localObject1).invoke(localObject2, (Object[])localObject3);
            localObject1 = (Integer)localObject1;
            i3 = ((Integer)localObject1).intValue();
          }
          else
          {
            i3 = 0;
            localObject1 = null;
          }
          Object localObject2 = f;
          Object localObject3 = a;
          Object localObject4 = new Object[n];
          localObject1 = Integer.valueOf(i3);
          localObject4[0] = localObject1;
          localObject1 = "enableMMS";
          localObject4[i1] = localObject1;
          localObject1 = ((Method)localObject2).invoke(localObject3, (Object[])localObject4);
          localObject1 = (Integer)localObject1;
          m = ((Integer)localObject1).intValue();
          int i3 = b();
          n = -1;
          if (i3 != n)
          {
            int i4 = a;
            if (i3 != i4)
            {
              paramString = c;
              localObject4 = a;
              Object[] arrayOfObject = new Object[i1];
              localObject1 = Integer.valueOf(i3);
              arrayOfObject[0] = localObject1;
              paramString.invoke(localObject4, arrayOfObject);
            }
          }
          j = false;
        }
        catch (IllegalAccessException paramString)
        {
          AssertionUtil.reportThrowableButNeverCrash(paramString);
        }
        catch (InvocationTargetException paramString)
        {
          AssertionUtil.reportThrowableButNeverCrash(paramString);
        }
        return m;
      }
    }
    return m;
  }
  
  protected final NetworkInfo d(String paramString)
  {
    paramString = k.b(paramString);
    if (paramString == null) {
      return null;
    }
    Object localObject = d;
    int m = 2;
    if (localObject != null)
    {
      int n = 1;
      try
      {
        ConnectivityManager localConnectivityManager = a;
        Object[] arrayOfObject = new Object[m];
        Integer localInteger = Integer.valueOf(m);
        arrayOfObject[0] = localInteger;
        int i1 = a;
        paramString = Integer.valueOf(i1);
        arrayOfObject[n] = paramString;
        paramString = ((Method)localObject).invoke(localConnectivityManager, arrayOfObject);
        paramString = (Integer)paramString;
        m = paramString.intValue();
      }
      catch (Exception paramString)
      {
        localObject = new String[n];
        paramString = String.valueOf(paramString);
        paramString = "ConnectivityManager.getTypeMobileFromSimSlot failed ".concat(paramString);
        localObject[0] = paramString;
        return null;
      }
    }
    return a.getNetworkInfo(m);
  }
  
  protected final int e(String paramString)
  {
    Object localObject1 = d;
    int m = 2;
    if (localObject1 == null) {
      return m;
    }
    localObject1 = k;
    paramString = ((ab)localObject1).b(paramString);
    int n = -1;
    if (paramString == null) {
      return n;
    }
    int i1 = 1;
    try
    {
      Method localMethod = d;
      ConnectivityManager localConnectivityManager = a;
      Object[] arrayOfObject = new Object[m];
      localObject2 = Integer.valueOf(m);
      arrayOfObject[0] = localObject2;
      int i2 = a;
      paramString = Integer.valueOf(i2);
      arrayOfObject[i1] = paramString;
      paramString = localMethod.invoke(localConnectivityManager, arrayOfObject);
      paramString = (Integer)paramString;
      return paramString.intValue();
    }
    catch (Exception paramString)
    {
      Object localObject2 = new String[i1];
      paramString = String.valueOf(paramString);
      paramString = "ConnectivityManager.getTypeMobileFromSimSlot failed ".concat(paramString);
      localObject2[0] = paramString;
    }
    return n;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.af
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */