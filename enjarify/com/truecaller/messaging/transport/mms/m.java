package com.truecaller.messaging.transport.mms;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.net.Uri;
import android.text.TextUtils;
import com.android.a.a.c;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.transport.f;
import com.truecaller.messaging.transport.i;
import com.truecaller.multisim.h;
import com.truecaller.utils.n;
import org.c.a.a.a.k;

final class m
  extends CursorWrapper
  implements an.a
{
  private static volatile String[] C;
  private final int A;
  private final int B;
  private final n D;
  private final f E;
  private final i F;
  private final int a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  private final int j;
  private final int k;
  private final int l;
  private final int m;
  private final int n;
  private final int o;
  private final int p;
  private final int q;
  private final int r;
  private final int s;
  private final int t;
  private final int u;
  private final int v;
  private final int w;
  private final int x;
  private final int y;
  private final int z;
  
  m(n paramn, f paramf, i parami, Cursor paramCursor, h paramh)
  {
    super(paramCursor);
    D = paramn;
    int i1 = paramCursor.getColumnIndexOrThrow("_id");
    a = i1;
    i1 = paramCursor.getColumnIndexOrThrow("thread_id");
    b = i1;
    i1 = paramCursor.getColumnIndexOrThrow("st");
    c = i1;
    i1 = paramCursor.getColumnIndexOrThrow("seen");
    d = i1;
    i1 = paramCursor.getColumnIndexOrThrow("read");
    e = i1;
    i1 = paramCursor.getColumnIndexOrThrow("locked");
    f = i1;
    i1 = paramCursor.getColumnIndexOrThrow("date_sent");
    g = i1;
    i1 = paramCursor.getColumnIndexOrThrow("date");
    h = i1;
    i1 = paramCursor.getColumnIndexOrThrow("sub");
    i = i1;
    i1 = paramCursor.getColumnIndexOrThrow("sub_cs");
    j = i1;
    i1 = paramCursor.getColumnIndexOrThrow("tr_id");
    k = i1;
    i1 = paramCursor.getColumnIndexOrThrow("ct_l");
    l = i1;
    i1 = paramCursor.getColumnIndexOrThrow("ct_t");
    m = i1;
    i1 = paramCursor.getColumnIndexOrThrow("exp");
    n = i1;
    i1 = paramCursor.getColumnIndexOrThrow("pri");
    o = i1;
    i1 = paramCursor.getColumnIndexOrThrow("retr_st");
    p = i1;
    i1 = paramCursor.getColumnIndexOrThrow("resp_st");
    q = i1;
    i1 = paramCursor.getColumnIndexOrThrow("m_id");
    r = i1;
    i1 = paramCursor.getColumnIndexOrThrow("msg_box");
    s = i1;
    i1 = paramCursor.getColumnIndexOrThrow("m_type");
    t = i1;
    i1 = paramCursor.getColumnIndexOrThrow("m_cls");
    u = i1;
    i1 = paramCursor.getColumnIndexOrThrow("m_size");
    v = i1;
    i1 = paramCursor.getColumnIndexOrThrow("d_rpt");
    w = i1;
    i1 = paramCursor.getColumnIndexOrThrow("d_tm");
    x = i1;
    i1 = paramCursor.getColumnIndexOrThrow("rr");
    y = i1;
    i1 = paramCursor.getColumnIndexOrThrow("read_status");
    z = i1;
    i1 = paramCursor.getColumnIndexOrThrow("rpt_a");
    A = i1;
    paramn = paramh.c();
    if (paramn != null) {
      i1 = paramCursor.getColumnIndex(paramn);
    } else {
      i1 = -1;
    }
    B = i1;
    E = paramf;
    F = parami;
  }
  
  static String a(n paramn, MmsTransportInfo paramMmsTransportInfo)
  {
    String[] arrayOfString = C;
    if (arrayOfString == null)
    {
      int i1 = 2130903042;
      arrayOfString = paramn.a(i1);
      C = arrayOfString;
    }
    paramn = h;
    if (paramn == null)
    {
      paramn = null;
    }
    else
    {
      paramn = c.a(h, 4);
      i2 = i;
      paramn = c.a(paramn, i2);
    }
    int i3 = g;
    int i2 = 130;
    int i5 = 0;
    if (i3 == i2)
    {
      paramMmsTransportInfo = arrayOfString[0];
      return (String)k.d(paramn, paramMmsTransportInfo);
    }
    boolean bool2 = k.d(paramn);
    if (bool2) {
      return null;
    }
    int i4 = arrayOfString.length;
    while (i5 < i4)
    {
      String str = arrayOfString[i5];
      boolean bool1 = str.equalsIgnoreCase(paramn);
      if (bool1) {
        return null;
      }
      i5 += 1;
    }
    return paramn;
  }
  
  private int n()
  {
    int i1 = s;
    return getInt(i1);
  }
  
  public final long a()
  {
    int i1 = a;
    return getLong(i1);
  }
  
  public final long b()
  {
    int i1 = b;
    boolean bool = isNull(i1);
    if (bool) {
      return -1;
    }
    int i2 = b;
    return getLong(i2);
  }
  
  public final long c()
  {
    int i1 = h;
    return getLong(i1) * 1000L;
  }
  
  public final boolean d()
  {
    int i1 = d;
    i1 = getInt(i1);
    return i1 != 0;
  }
  
  public final boolean e()
  {
    int i1 = e;
    i1 = getInt(i1);
    return i1 != 0;
  }
  
  public final boolean f()
  {
    int i1 = f;
    i1 = getInt(i1);
    return i1 != 0;
  }
  
  public final int g()
  {
    int i1 = n();
    int i2 = t;
    i2 = getInt(i2);
    int i3 = q;
    i3 = getInt(i3);
    return MmsTransportInfo.a(i1, i2, i3);
  }
  
  public final int h()
  {
    int i1 = c;
    return getInt(i1);
  }
  
  public final Message i()
  {
    Object localObject1 = new com/truecaller/messaging/transport/mms/MmsTransportInfo$a;
    ((MmsTransportInfo.a)localObject1).<init>();
    long l1 = a();
    int i1 = j;
    i1 = getInt(i1);
    int i2 = i;
    String str = k.n(getString(i2));
    b = l1;
    Object localObject2 = ((MmsTransportInfo.a)localObject1).c(l1);
    int i3 = h();
    c = i3;
    long l2 = b();
    d = l2;
    localObject2 = ((MmsTransportInfo.a)localObject2).a(str, i1);
    i3 = k;
    Object localObject3 = getString(i3);
    p = ((String)localObject3);
    i3 = n;
    long l3 = getLong(i3);
    localObject2 = ((MmsTransportInfo.a)localObject2).e(l3);
    i3 = o;
    i3 = getInt(i3);
    r = i3;
    i3 = l();
    s = i3;
    i3 = k();
    t = i3;
    i3 = r;
    localObject3 = getString(i3);
    u = ((String)localObject3);
    i3 = n();
    v = i3;
    i3 = t;
    i3 = getInt(i3);
    w = i3;
    i3 = u;
    localObject3 = getString(i3);
    o = ((String)localObject3);
    i3 = v;
    i3 = getInt(i3);
    x = i3;
    i3 = m();
    y = i3;
    i3 = m;
    localObject3 = getString(i3);
    l = ((String)localObject3);
    i3 = x;
    l3 = getLong(i3);
    z = l3;
    i3 = y;
    i3 = getInt(i3);
    A = i3;
    i3 = z;
    i3 = getInt(i3);
    B = i3;
    i3 = A;
    i3 = getInt(i3);
    i1 = 1;
    i2 = 0;
    str = null;
    if (i3 != 0)
    {
      i3 = 1;
    }
    else
    {
      i3 = 0;
      localObject3 = null;
    }
    C = i3;
    int i4 = l;
    localObject2 = getString(i4);
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject2);
    if (!bool1)
    {
      localObject2 = Uri.parse((String)localObject2);
      k = ((Uri)localObject2);
    }
    localObject1 = ((MmsTransportInfo.a)localObject1).a();
    i4 = b;
    l1 = getLong(i4);
    Object localObject4 = "-1";
    int i5 = B;
    if (i5 >= 0)
    {
      boolean bool2 = isNull(i5);
      if (!bool2)
      {
        int i6 = B;
        localObject4 = getString(i6);
      }
    }
    Object localObject5 = new com/truecaller/messaging/data/types/Message$a;
    ((Message.a)localObject5).<init>();
    int i7 = g;
    long l4 = getLong(i7);
    long l5 = 1000L;
    l4 *= l5;
    localObject5 = ((Message.a)localObject5).d(l4);
    l4 = c();
    localObject5 = ((Message.a)localObject5).c(l4);
    i7 = ((MmsTransportInfo)localObject1).h();
    f = i7;
    boolean bool3 = d();
    g = bool3;
    bool3 = e();
    h = bool3;
    bool3 = f();
    i = bool3;
    Message.a locala = ((Message.a)localObject5).a((String)localObject4).a(i1, (TransportInfo)localObject1);
    localObject4 = e;
    localObject5 = new String[] { "Message URI can not be null" };
    AssertionUtil.AlwaysFatal.isNotNull(localObject4, (String[])localObject5);
    localObject4 = E;
    localObject5 = e;
    localObject2 = ((f)localObject4).a(l1, (Uri)localObject5);
    localObject3 = F;
    localObject2 = ((i)localObject3).a((String)localObject2);
    c = ((Participant)localObject2);
    localObject2 = D;
    localObject1 = a((n)localObject2, (MmsTransportInfo)localObject1);
    if (localObject1 != null)
    {
      localObject2 = "text/plain";
      l2 = -1;
      localObject1 = Entity.a((String)localObject2, 0, (String)localObject1, l2);
      locala.a((Entity)localObject1);
    }
    return locala.b();
  }
  
  public final String j()
  {
    return null;
  }
  
  public final int k()
  {
    int i1 = q;
    return getInt(i1);
  }
  
  public final int l()
  {
    int i1 = p;
    return getInt(i1);
  }
  
  public final int m()
  {
    int i1 = w;
    return getInt(i1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */