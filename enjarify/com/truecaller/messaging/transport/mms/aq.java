package com.truecaller.messaging.transport.mms;

import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import android.telephony.SmsManager;
import c.u;
import java.lang.reflect.Method;

final class aq
  implements k
{
  private final Method a;
  private final Method b;
  private final Context c;
  private final String d;
  private final SmsManager e;
  
  public aq(Context paramContext, String paramString, SmsManager paramSmsManager)
  {
    c = paramContext;
    d = paramString;
    e = paramSmsManager;
    paramContext = Class.forName("com.samsung.android.telephony.MultiSimManager");
    int i = 1;
    Class[] arrayOfClass = new Class[i];
    Class localClass = Integer.TYPE;
    arrayOfClass[0] = localClass;
    paramString = paramContext.getMethod("getDefaultSubId", arrayOfClass);
    c.g.b.k.a(paramString, "multiSimManager.getMetho…aultSubId\", Integer.TYPE)");
    a = paramString;
    arrayOfClass = new Class[2];
    localClass = Integer.TYPE;
    arrayOfClass[0] = localClass;
    localClass = Long.TYPE;
    arrayOfClass[i] = localClass;
    paramContext = paramContext.getMethod("setDefaultSubId", arrayOfClass);
    c.g.b.k.a(paramContext, "multiSimManager.getMetho…YPE, java.lang.Long.TYPE)");
    b = paramContext;
  }
  
  public final boolean a(Uri paramUri, String paramString, PendingIntent paramPendingIntent)
  {
    Object localObject1 = paramUri;
    c.g.b.k.b(paramUri, "contentUri");
    c.g.b.k.b(paramPendingIntent, "sentIntent");
    try
    {
      Object localObject2 = a;
      boolean bool = true;
      Object localObject3 = new Object[bool];
      int i = 2;
      Object localObject4 = Integer.valueOf(i);
      localObject3[0] = localObject4;
      localObject2 = ((Method)localObject2).invoke(null, (Object[])localObject3);
      if (localObject2 != null)
      {
        localObject2 = (Long)localObject2;
        long l = ((Long)localObject2).longValue();
        localObject2 = b;
        localObject3 = new Object[i];
        localObject4 = Integer.valueOf(i);
        localObject3[0] = localObject4;
        localObject4 = d;
        localObject4 = Long.valueOf((String)localObject4);
        localObject3[bool] = localObject4;
        ((Method)localObject2).invoke(null, (Object[])localObject3);
        localObject2 = e;
        localObject3 = c;
        localObject4 = paramString;
        ((SmsManager)localObject2).sendMultimediaMessage((Context)localObject3, paramUri, paramString, null, paramPendingIntent);
        localObject2 = b;
        localObject3 = new Object[i];
        localObject1 = Integer.valueOf(i);
        localObject3[0] = localObject1;
        localObject1 = Long.valueOf(l);
        localObject3[bool] = localObject1;
        ((Method)localObject2).invoke(null, (Object[])localObject3);
        return bool;
      }
      localObject2 = new c/u;
      localObject3 = "null cannot be cast to non-null type kotlin.Long";
      ((u)localObject2).<init>((String)localObject3);
      throw ((Throwable)localObject2);
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    return false;
  }
  
  public final boolean a(String paramString, Uri paramUri, PendingIntent paramPendingIntent)
  {
    Object localObject1 = paramString;
    c.g.b.k.b(paramString, "locationUrl");
    c.g.b.k.b(paramUri, "contentUri");
    c.g.b.k.b(paramPendingIntent, "downloadedIntent");
    try
    {
      Object localObject2 = a;
      boolean bool = true;
      Object localObject3 = new Object[bool];
      int i = 2;
      Object localObject4 = Integer.valueOf(i);
      localObject3[0] = localObject4;
      localObject2 = ((Method)localObject2).invoke(null, (Object[])localObject3);
      if (localObject2 != null)
      {
        localObject2 = (Long)localObject2;
        long l = ((Long)localObject2).longValue();
        localObject2 = b;
        localObject3 = new Object[i];
        localObject4 = Integer.valueOf(i);
        localObject3[0] = localObject4;
        localObject4 = d;
        localObject4 = Long.valueOf((String)localObject4);
        localObject3[bool] = localObject4;
        ((Method)localObject2).invoke(null, (Object[])localObject3);
        localObject2 = e;
        localObject3 = c;
        localObject4 = null;
        ((SmsManager)localObject2).downloadMultimediaMessage((Context)localObject3, paramString, paramUri, null, paramPendingIntent);
        localObject2 = b;
        localObject3 = new Object[i];
        localObject1 = Integer.valueOf(i);
        localObject3[0] = localObject1;
        localObject1 = Long.valueOf(l);
        localObject3[bool] = localObject1;
        ((Method)localObject2).invoke(null, (Object[])localObject3);
        return bool;
      }
      localObject2 = new c/u;
      localObject3 = "null cannot be cast to non-null type kotlin.Long";
      ((u)localObject2).<init>((String)localObject3);
      throw ((Throwable)localObject2);
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.aq
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */