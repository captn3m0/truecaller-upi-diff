package com.truecaller.messaging.transport.mms;

import android.net.Uri;
import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class aj$d
  extends u
{
  private final byte[] b;
  private final Uri c;
  private final int d;
  
  private aj$d(e parame, byte[] paramArrayOfByte, Uri paramUri, int paramInt)
  {
    super(parame);
    b = paramArrayOfByte;
    c = paramUri;
    d = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".sendNotifyResponseForMmsDownload(");
    Object localObject = b;
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(d), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.aj.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */