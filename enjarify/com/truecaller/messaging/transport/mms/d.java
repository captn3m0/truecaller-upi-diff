package com.truecaller.messaging.transport.mms;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.provider.Telephony.Carriers;
import android.telephony.TelephonyManager;
import com.truecaller.multisim.SimInfo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.c.a.a.a.k;

final class d
  implements c
{
  private static final String[] a = tmp18_5;
  private final Context b;
  private final com.truecaller.utils.d c;
  private final TelephonyManager d;
  private final b e;
  private final Map f;
  
  static
  {
    String[] tmp4_1 = new String[4];
    String[] tmp5_4 = tmp4_1;
    String[] tmp5_4 = tmp4_1;
    tmp5_4[0] = "type";
    tmp5_4[1] = "mmsc";
    tmp5_4[2] = "mmsproxy";
    String[] tmp18_5 = tmp5_4;
    tmp18_5[3] = "mmsport";
  }
  
  d(Context paramContext, TelephonyManager paramTelephonyManager, com.truecaller.utils.d paramd, b paramb)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    f = localHashMap;
    b = paramContext;
    d = paramTelephonyManager;
    c = paramd;
    e = paramb;
  }
  
  private Cursor a(Uri paramUri, boolean paramBoolean, String paramString1, String paramString2)
  {
    int i = 1;
    Object localObject1 = new String[i];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Loading APNs from system, checkCurrent=");
    ((StringBuilder)localObject2).append(paramBoolean);
    ((StringBuilder)localObject2).append(" apnName=");
    ((StringBuilder)localObject2).append(paramString2);
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    if (paramBoolean)
    {
      localObject2 = "current IS NOT NULL";
      ((StringBuilder)localObject1).append((String)localObject2);
    }
    paramString2 = k.j(paramString2);
    int j = paramString2.length();
    if (j > 0)
    {
      j = ((StringBuilder)localObject1).length();
      if (j > 0)
      {
        localObject2 = " AND ";
        ((StringBuilder)localObject1).append((String)localObject2);
      }
      ((StringBuilder)localObject1).append("apn=?");
      localObject2 = new String[i];
      localObject2[0] = paramString2;
    }
    else
    {
      j = 0;
      localObject2 = null;
    }
    Object localObject3 = c;
    int k = ((com.truecaller.utils.d)localObject3).h();
    int m = 22;
    if (k >= m)
    {
      localObject3 = "-1";
      boolean bool = ((String)localObject3).equals(paramString1);
      if (!bool)
      {
        j = ((StringBuilder)localObject1).length();
        if (j > 0)
        {
          localObject2 = " AND ";
          ((StringBuilder)localObject1).append((String)localObject2);
        }
        ((StringBuilder)localObject1).append("sub_id=?");
        localObject2 = new String[i];
        paramString1 = String.valueOf(paramString1);
        localObject2[0] = paramString1;
        localObject4 = localObject2;
        break label260;
      }
    }
    Object localObject4 = localObject2;
    try
    {
      label260:
      paramString1 = b;
      localObject3 = paramString1.getContentResolver();
      String[] arrayOfString1 = a;
      String str = ((StringBuilder)localObject1).toString();
      paramString1 = ((ContentResolver)localObject3).query(paramUri, arrayOfString1, str, (String[])localObject4, null);
      if (paramString1 != null)
      {
        int n = paramString1.getCount();
        if (n > 0) {
          return paramString1;
        }
      }
      if (paramString1 != null) {
        paramString1.close();
      }
      paramString1 = new String[i];
      localObject1 = new java/lang/StringBuilder;
      localObject2 = "Query ";
      ((StringBuilder)localObject1).<init>((String)localObject2);
      ((StringBuilder)localObject1).append(paramUri);
      paramUri = " with apn ";
      ((StringBuilder)localObject1).append(paramUri);
      ((StringBuilder)localObject1).append(paramString2);
      paramUri = " and ";
      ((StringBuilder)localObject1).append(paramUri);
      if (paramBoolean) {
        paramUri = "checking CURRENT";
      } else {
        paramUri = "not checking CURRENT";
      }
      ((StringBuilder)localObject1).append(paramUri);
      paramUri = " returned empty";
      ((StringBuilder)localObject1).append(paramUri);
      paramUri = ((StringBuilder)localObject1).toString();
      paramString1[0] = paramUri;
      return null;
    }
    catch (SecurityException paramUri)
    {
      arrayOfString2 = new String[i];
      paramString1 = String.valueOf(paramUri);
      paramString1 = "Platform restricts APN table access: ".concat(paramString1);
      arrayOfString2[0] = paramString1;
      throw paramUri;
    }
    catch (SQLiteException paramUri)
    {
      String[] arrayOfString2 = new String[i];
      paramUri = String.valueOf(paramUri);
      paramUri = "APN table query exception: ".concat(paramUri);
      arrayOfString2[0] = paramUri;
    }
    return null;
  }
  
  private void a(String paramString, List paramList1, List paramList2)
  {
    int i = 0;
    String str1 = null;
    int j = 0;
    String str2 = null;
    Object localObject1 = null;
    int k = 0;
    int m;
    for (;;)
    {
      m = 1;
      if (localObject1 == null) {
        try
        {
          try
          {
            int n = paramList1.size();
            if (k >= n) {
              break label118;
            }
            Object localObject2 = paramList1.get(k);
            localObject2 = (String)localObject2;
            Uri localUri1 = Telephony.Carriers.CONTENT_URI;
            localObject1 = a(localUri1, m, paramString, (String)localObject2);
            if (localObject1 == null)
            {
              Uri localUri2 = Telephony.Carriers.CONTENT_URI;
              localObject1 = a(localUri2, false, paramString, (String)localObject2);
            }
          }
          catch (SecurityException localSecurityException) {}
          k += 1;
        }
        finally
        {
          break label290;
        }
      }
    }
    label118:
    if (localObject1 == null)
    {
      paramList1 = Telephony.Carriers.CONTENT_URI;
      localObject1 = a(paramList1, m, paramString, null);
    }
    if (localObject1 == null)
    {
      paramList1 = Telephony.Carriers.CONTENT_URI;
      paramString = a(paramList1, false, paramString, null);
      localObject1 = paramString;
      break label175;
      if (localObject1 != null) {
        ((Cursor)localObject1).close();
      }
      return;
    }
    label175:
    if (localObject1 == null)
    {
      if (localObject1 != null) {
        ((Cursor)localObject1).close();
      }
      return;
    }
    boolean bool = ((Cursor)localObject1).moveToFirst();
    if (bool)
    {
      paramString = ((Cursor)localObject1).getString(0);
      paramList1 = ((Cursor)localObject1).getString(m);
      i = 2;
      str1 = ((Cursor)localObject1).getString(i);
      j = 3;
      str2 = ((Cursor)localObject1).getString(j);
      paramString = a.a(paramString, paramList1, str1, str2);
      if (paramString != null) {
        paramList2.add(paramString);
      }
    }
    if (localObject1 != null)
    {
      ((Cursor)localObject1).close();
      return;
    }
    return;
    label290:
    if (localObject1 != null) {
      ((Cursor)localObject1).close();
    }
    throw paramString;
  }
  
  public final List a(SimInfo paramSimInfo, List paramList)
  {
    String str1;
    if (paramSimInfo != null) {
      str1 = b;
    } else {
      str1 = "-1";
    }
    Object localObject1 = (List)f.get(str1);
    if (localObject1 != null) {
      return (List)localObject1;
    }
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    if (paramSimInfo != null) {
      paramSimInfo = e;
    } else {
      paramSimInfo = d.getSimOperator();
    }
    a(str1, paramList, (List)localObject1);
    int i = ((List)localObject1).size();
    if (i <= 0)
    {
      Object localObject2 = e;
      String str2 = "apns.json";
      ((b)localObject2).a(paramSimInfo, str2, paramList, (List)localObject1);
      int j = ((List)localObject1).size();
      if (j <= 0)
      {
        paramList = e;
        localObject2 = "apns.json";
        str2 = null;
        paramList.a(paramSimInfo, (String)localObject2, null, (List)localObject1);
      }
    }
    paramSimInfo = Collections.unmodifiableList((List)localObject1);
    f.put(str1, paramSimInfo);
    return paramSimInfo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */