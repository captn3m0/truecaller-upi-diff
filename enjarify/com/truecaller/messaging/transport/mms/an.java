package com.truecaller.messaging.transport.mms;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import com.truecaller.common.h.am;
import com.truecaller.content.TruecallerContract.z;
import com.truecaller.featuretoggles.e;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.transport.i;
import com.truecaller.messaging.transport.l.b;
import com.truecaller.multisim.h;
import com.truecaller.util.bd;
import com.truecaller.util.de;
import com.truecaller.utils.n;
import com.truecaller.utils.q;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.a.a.b;

final class an
  extends com.truecaller.messaging.transport.c
{
  private static final Uri d = Uri.parse("content://mms/part");
  private static final String[] e = tmp22_13;
  private final n f;
  private final bd g;
  private android.support.v4.f.f h;
  private android.support.v4.f.f i;
  private StringBuilder j;
  
  static
  {
    String[] tmp12_9 = new String[5];
    String[] tmp13_12 = tmp12_9;
    String[] tmp13_12 = tmp12_9;
    tmp13_12[0] = "_id";
    tmp13_12[1] = "mid";
    String[] tmp22_13 = tmp13_12;
    String[] tmp22_13 = tmp13_12;
    tmp22_13[2] = "ct";
    tmp22_13[3] = "chset";
    tmp22_13[4] = "text";
  }
  
  an(Context paramContext, h paramh, n paramn, bd parambd, e parame)
  {
    super(paramContext, paramh, parame);
    f = paramn;
    g = parambd;
  }
  
  /* Error */
  private static BitmapFactory.Options a(Context paramContext, Uri paramUri)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 55	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   4: astore_0
    //   5: aload_0
    //   6: aload_1
    //   7: invokevirtual 61	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   10: astore_0
    //   11: new 63	android/graphics/BitmapFactory$Options
    //   14: astore_1
    //   15: aload_1
    //   16: invokespecial 66	android/graphics/BitmapFactory$Options:<init>	()V
    //   19: iconst_1
    //   20: istore_2
    //   21: aload_1
    //   22: iload_2
    //   23: putfield 71	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   26: aload_0
    //   27: aconst_null
    //   28: aload_1
    //   29: invokestatic 77	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   32: pop
    //   33: aload_1
    //   34: getfield 81	android/graphics/BitmapFactory$Options:outMimeType	Ljava/lang/String;
    //   37: astore_3
    //   38: aload_3
    //   39: invokestatic 87	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   42: istore_2
    //   43: iload_2
    //   44: ifeq +9 -> 53
    //   47: aload_0
    //   48: invokestatic 93	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   51: aconst_null
    //   52: areturn
    //   53: aload_0
    //   54: invokestatic 93	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   57: aload_1
    //   58: areturn
    //   59: astore_1
    //   60: goto +26 -> 86
    //   63: astore_1
    //   64: goto +12 -> 76
    //   67: astore_1
    //   68: aconst_null
    //   69: astore_0
    //   70: goto +16 -> 86
    //   73: astore_1
    //   74: aconst_null
    //   75: astore_0
    //   76: aload_1
    //   77: invokestatic 99	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   80: aload_0
    //   81: invokestatic 93	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   84: aconst_null
    //   85: areturn
    //   86: aload_0
    //   87: invokestatic 93	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   90: aload_1
    //   91: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	92	0	paramContext	Context
    //   0	92	1	paramUri	Uri
    //   20	24	2	bool	boolean
    //   37	2	3	str	String
    // Exception table:
    //   from	to	target	type
    //   11	14	59	finally
    //   15	19	59	finally
    //   22	26	59	finally
    //   28	33	59	finally
    //   33	37	59	finally
    //   38	42	59	finally
    //   76	80	59	finally
    //   11	14	63	java/io/FileNotFoundException
    //   15	19	63	java/io/FileNotFoundException
    //   22	26	63	java/io/FileNotFoundException
    //   28	33	63	java/io/FileNotFoundException
    //   33	37	63	java/io/FileNotFoundException
    //   38	42	63	java/io/FileNotFoundException
    //   6	10	67	finally
    //   6	10	73	java/io/FileNotFoundException
  }
  
  private void a(String paramString, android.support.v4.f.f paramf1, android.support.v4.f.f paramf2, List paramList)
  {
    int k = paramf2.b();
    if (k == 0)
    {
      k = paramf1.b();
      if (k == 0) {
        return;
      }
    }
    Object localObject1 = a.getContentResolver();
    k = 0;
    try
    {
      Object localObject2 = d;
      Object localObject3 = e;
      String str1 = null;
      int m = 0;
      Object localObject4 = null;
      Object localObject5 = paramString;
      paramString = ((ContentResolver)localObject1).query((Uri)localObject2, (String[])localObject3, paramString, null, null);
      if (paramString != null) {
        try
        {
          for (;;)
          {
            boolean bool1 = paramString.moveToNext();
            if (!bool1) {
              break;
            }
            int n = 2;
            str1 = paramString.getString(n);
            localObject1 = "application/smil";
            boolean bool2 = ((String)localObject1).equals(str1);
            if (!bool2)
            {
              bool2 = false;
              localObject1 = null;
              localObject2 = paramString.getString(0);
              int i5 = 1;
              long l = paramString.getLong(i5);
              m = 3;
              boolean bool7 = paramString.isNull(m);
              int i1;
              if (!bool7) {
                i1 = paramString.getInt(m);
              }
              m = 4;
              localObject4 = paramString.getString(m);
              Object localObject6 = d;
              localObject6 = ((Uri)localObject6).buildUpon();
              localObject2 = ((Uri.Builder)localObject6).appendPath((String)localObject2);
              localObject2 = ((Uri.Builder)localObject2).build();
              localObject6 = paramf1.a(l, null);
              localObject6 = (Integer)localObject6;
              localObject3 = paramf2.a(l, null);
              localObject5 = localObject3;
              localObject5 = (Long)localObject3;
              boolean bool6 = Entity.a(str1);
              int i7;
              int i8;
              String str2;
              if (bool6)
              {
                localObject2 = am.n((String)localObject4);
                localObject2 = com.android.a.a.c.a((String)localObject2, i1);
                if (i1 == 0)
                {
                  localObject1 = new java/lang/String;
                  ((String)localObject1).<init>((byte[])localObject2);
                  localObject4 = localObject1;
                }
                else
                {
                  try
                  {
                    localObject1 = com.android.a.a.a.c.a(i1);
                    localObject3 = new java/lang/String;
                    ((String)localObject3).<init>((byte[])localObject2, (String)localObject1);
                    localObject4 = localObject3;
                  }
                  catch (UnsupportedEncodingException localUnsupportedEncodingException1)
                  {
                    try
                    {
                      localObject1 = new java/lang/String;
                      localObject3 = "iso-8859-1";
                      ((String)localObject1).<init>((byte[])localObject2, (String)localObject3);
                      localObject4 = localObject1;
                    }
                    catch (UnsupportedEncodingException localUnsupportedEncodingException2)
                    {
                      localObject1 = new java/lang/String;
                      ((String)localObject1).<init>((byte[])localObject2);
                      localObject4 = localObject1;
                    }
                  }
                }
                boolean bool3 = TextUtils.isEmpty((CharSequence)localObject4);
                if (!bool3)
                {
                  int i2 = -1;
                  i7 = -1;
                  i8 = -1;
                  str2 = "";
                  localObject2 = paramList;
                  localObject3 = localObject6;
                  bool7 = i2;
                  a(paramList, (Integer)localObject6, (Long)localObject5, str1, (String)localObject4, i2, i7, i8, str2);
                }
              }
              else
              {
                str2 = "";
                boolean bool4 = Entity.c(str1);
                int i6 = -1;
                int i4;
                if (bool4)
                {
                  localObject1 = a;
                  localObject1 = a((Context)localObject1, (Uri)localObject2);
                  if (localObject1 != null)
                  {
                    m = outWidth;
                    i3 = outHeight;
                  }
                  else
                  {
                    i3 = -1;
                    m = -1;
                  }
                  i7 = i3;
                  int i3 = m;
                  i8 = -1;
                }
                else
                {
                  boolean bool5 = Entity.d(str1);
                  if (bool5)
                  {
                    localObject1 = g;
                    localObject1 = ((bd)localObject1).a((Uri)localObject2);
                    if (localObject1 != null)
                    {
                      i6 = a;
                      m = b;
                      i8 = c;
                      bool5 = i6;
                      i7 = m;
                      break label589;
                    }
                  }
                  i4 = -1;
                  i7 = -1;
                  i8 = -1;
                }
                label589:
                localObject4 = ((Uri)localObject2).toString();
                localObject2 = paramList;
                localObject3 = localObject6;
                bool7 = i4;
                a(paramList, (Integer)localObject6, (Long)localObject5, str1, (String)localObject4, i4, i7, i8, str2);
              }
            }
          }
          if (paramString == null) {
            break label647;
          }
        }
        finally
        {
          break label651;
        }
      }
      paramString.close();
      return;
    }
    finally
    {
      label647:
      paramString = null;
      if (paramString == null) {
        break label661;
      }
      paramString.close();
    }
  }
  
  private static void a(List paramList, Integer paramInteger, Long paramLong, String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, String paramString3)
  {
    ContentProviderOperation.Builder localBuilder = ContentProviderOperation.newInsert(TruecallerContract.z.a());
    String str = "type";
    localBuilder.withValue(str, paramString1);
    localBuilder.withValue("content", paramString2);
    paramString2 = Integer.valueOf(paramInt1);
    localBuilder.withValue("width", paramString2);
    paramString2 = Integer.valueOf(paramInt2);
    localBuilder.withValue("height", paramString2);
    paramString2 = Integer.valueOf(paramInt3);
    localBuilder.withValue("duration", paramString2);
    paramString1 = "thumbnail";
    localBuilder.withValue(paramString1, paramString3);
    if (paramLong == null)
    {
      paramLong = new String[0];
      AssertionUtil.AlwaysFatal.isNotNull(paramInteger, paramLong);
      paramLong = "message_id";
      int k = paramInteger.intValue();
      localBuilder.withValueBackReference(paramLong, k);
    }
    else
    {
      paramInteger = "message_id";
      localBuilder.withValue(paramInteger, paramLong);
    }
    paramInteger = localBuilder.build();
    paramList.add(paramInteger);
  }
  
  public final long a(com.truecaller.messaging.transport.f paramf, i parami, r paramr, b paramb1, b paramb2, int paramInt, List paramList, q paramq, boolean paramBoolean1, boolean paramBoolean2, Set paramSet)
  {
    Object localObject = new java/lang/StringBuilder;
    String str = "ct != 'application/smil' AND mid IN (";
    ((StringBuilder)localObject).<init>(str);
    j = ((StringBuilder)localObject);
    localObject = new android/support/v4/f/f;
    ((android.support.v4.f.f)localObject).<init>();
    h = ((android.support.v4.f.f)localObject);
    localObject = new android/support/v4/f/f;
    ((android.support.v4.f.f)localObject).<init>();
    i = ((android.support.v4.f.f)localObject);
    long l = super.a(paramf, parami, paramr, paramb1, paramb2, paramInt, paramList, paramq, paramBoolean1, paramBoolean2, paramSet);
    boolean bool = l.b.b(l);
    if (!bool)
    {
      j.append(")");
      paramr = j.toString();
      paramb1 = h;
      paramb2 = i;
      a(paramr, paramb1, paramb2, paramList);
    }
    h = null;
    i = null;
    j = null;
    return l;
  }
  
  public final Set a(long paramLong, com.truecaller.messaging.transport.f paramf, i parami, Participant paramParticipant, boolean paramBoolean)
  {
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    localHashSet.add(paramParticipant);
    Iterator localIterator = paramf.a(paramLong).iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = (String)localIterator.next();
      localObject = parami.a((String)localObject);
      localHashSet.add(localObject);
    }
    return localHashSet;
  }
  
  public final boolean a(int paramInt)
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.an
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */