package com.truecaller.messaging.transport.mms;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.Telephony.Mms;
import android.util.SparseArray;
import java.util.HashSet;
import java.util.Set;
import org.a.a.b;

public final class MmsTransportInfo$a
{
  public int A;
  public int B;
  public boolean C;
  boolean D;
  SparseArray E;
  public long a;
  public long b = -1;
  public int c;
  public long d;
  public Uri e;
  public int f;
  String g;
  int h;
  String i;
  int j;
  public Uri k;
  public String l;
  public int m;
  public String n;
  public String o;
  public String p;
  b q;
  public int r;
  public int s;
  public int t;
  public String u;
  public int v;
  public int w;
  public int x;
  public int y;
  public long z;
  
  public MmsTransportInfo$a() {}
  
  private MmsTransportInfo$a(MmsTransportInfo paramMmsTransportInfo)
  {
    long l1 = a;
    a = l1;
    l1 = b;
    b = l1;
    int i1 = c;
    c = i1;
    l1 = d;
    d = l1;
    Object localObject = e;
    e = ((Uri)localObject);
    i1 = f;
    f = i1;
    localObject = h;
    g = ((String)localObject);
    i1 = i;
    h = i1;
    localObject = j;
    i = ((String)localObject);
    i1 = k;
    j = i1;
    localObject = l;
    k = ((Uri)localObject);
    localObject = m;
    l = ((String)localObject);
    i1 = n;
    m = i1;
    localObject = t;
    n = ((String)localObject);
    localObject = u;
    o = ((String)localObject);
    localObject = o;
    p = ((String)localObject);
    localObject = p;
    q = ((b)localObject);
    i1 = q;
    r = i1;
    i1 = r;
    s = i1;
    i1 = s;
    t = i1;
    localObject = v;
    u = ((String)localObject);
    i1 = w;
    v = i1;
    i1 = g;
    w = i1;
    i1 = x;
    x = i1;
    i1 = y;
    y = i1;
    l1 = z;
    z = l1;
    i1 = A;
    A = i1;
    i1 = B;
    B = i1;
    boolean bool = C;
    C = bool;
    bool = D;
    D = bool;
    paramMmsTransportInfo = E;
    E = paramMmsTransportInfo;
  }
  
  public final a a(int paramInt)
  {
    c = paramInt;
    return this;
  }
  
  final a a(int paramInt, String paramString)
  {
    Object localObject = E;
    if (localObject == null)
    {
      localObject = new android/util/SparseArray;
      ((SparseArray)localObject).<init>();
      E = ((SparseArray)localObject);
    }
    localObject = (Set)E.get(paramInt);
    if (localObject == null)
    {
      localObject = new java/util/HashSet;
      ((HashSet)localObject).<init>();
      SparseArray localSparseArray = E;
      localSparseArray.put(paramInt, localObject);
    }
    ((Set)localObject).add(paramString);
    return this;
  }
  
  public final a a(long paramLong)
  {
    b = paramLong;
    return this;
  }
  
  public final a a(Uri paramUri)
  {
    e = paramUri;
    return this;
  }
  
  public final a a(String paramString)
  {
    l = paramString;
    return this;
  }
  
  public final a a(String paramString, int paramInt)
  {
    g = paramString;
    h = paramInt;
    return this;
  }
  
  public final a a(boolean paramBoolean)
  {
    C = paramBoolean;
    return this;
  }
  
  public final MmsTransportInfo a()
  {
    MmsTransportInfo localMmsTransportInfo = new com/truecaller/messaging/transport/mms/MmsTransportInfo;
    localMmsTransportInfo.<init>(this, (byte)0);
    return localMmsTransportInfo;
  }
  
  public final a b(int paramInt)
  {
    f = paramInt;
    return this;
  }
  
  public final a b(long paramLong)
  {
    d = paramLong;
    return this;
  }
  
  public final a b(Uri paramUri)
  {
    k = paramUri;
    return this;
  }
  
  public final a b(String paramString)
  {
    n = paramString;
    return this;
  }
  
  public final a b(String paramString, int paramInt)
  {
    i = paramString;
    j = paramInt;
    return this;
  }
  
  public final a c(int paramInt)
  {
    m = paramInt;
    return this;
  }
  
  public final a c(long paramLong)
  {
    Uri localUri = ContentUris.withAppendedId(Telephony.Mms.CONTENT_URI, paramLong);
    e = localUri;
    return this;
  }
  
  public final a c(String paramString)
  {
    o = paramString;
    return this;
  }
  
  public final a d(int paramInt)
  {
    r = paramInt;
    return this;
  }
  
  public final a d(long paramLong)
  {
    a = paramLong;
    return this;
  }
  
  public final a d(String paramString)
  {
    p = paramString;
    return this;
  }
  
  public final a e(int paramInt)
  {
    s = paramInt;
    return this;
  }
  
  public final a e(long paramLong)
  {
    b localb = new org/a/a/b;
    paramLong *= 1000L;
    localb.<init>(paramLong);
    q = localb;
    return this;
  }
  
  public final a e(String paramString)
  {
    u = paramString;
    return this;
  }
  
  public final a f(int paramInt)
  {
    t = paramInt;
    return this;
  }
  
  public final a f(long paramLong)
  {
    z = paramLong;
    return this;
  }
  
  public final a g(int paramInt)
  {
    v = paramInt;
    return this;
  }
  
  public final a h(int paramInt)
  {
    w = paramInt;
    return this;
  }
  
  public final a i(int paramInt)
  {
    x = paramInt;
    return this;
  }
  
  public final a j(int paramInt)
  {
    y = paramInt;
    return this;
  }
  
  public final a k(int paramInt)
  {
    A = paramInt;
    return this;
  }
  
  public final a l(int paramInt)
  {
    B = paramInt;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.MmsTransportInfo.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */