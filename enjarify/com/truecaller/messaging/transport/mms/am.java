package com.truecaller.messaging.transport.mms;

import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import com.truecaller.log.AssertionUtil;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.a;

final class am
  extends e
{
  private final k d;
  
  am(Context paramContext, SimInfo paramSimInfo, a parama, k paramk)
  {
    super(paramContext, paramSimInfo, parama);
    d = paramk;
  }
  
  protected final boolean a(Uri paramUri1, Uri paramUri2, PendingIntent paramPendingIntent)
  {
    if (paramUri2 == null) {
      paramUri2 = null;
    }
    try
    {
      paramUri2 = paramUri2.toString();
      k localk = d;
      return localk.a(paramUri1, paramUri2, paramPendingIntent);
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localRuntimeException;
    }
    return false;
  }
  
  final boolean b(Uri paramUri1, Uri paramUri2, PendingIntent paramPendingIntent)
  {
    if (paramUri2 == null) {
      paramUri2 = null;
    }
    try
    {
      paramUri2 = paramUri2.toString();
      k localk = d;
      return localk.a(paramUri2, paramUri1, paramPendingIntent);
    }
    catch (RuntimeException localRuntimeException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localRuntimeException;
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.am
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */