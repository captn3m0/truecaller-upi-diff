package com.truecaller.messaging.transport.mms;

import android.telephony.TelephonyManager;
import android.text.TextUtils;

final class as
  implements ar
{
  private final TelephonyManager a;
  private volatile boolean b = false;
  private String c;
  private String d;
  
  as(TelephonyManager paramTelephonyManager)
  {
    a = paramTelephonyManager;
  }
  
  private void c()
  {
    boolean bool = b;
    if (bool) {
      return;
    }
    try
    {
      bool = b;
      if (bool) {
        return;
      }
      d();
      bool = true;
      b = bool;
      return;
    }
    finally {}
  }
  
  private void d()
  {
    String str = a.getMmsUserAgent();
    c = str;
    str = a.getMmsUAProfUrl();
    d = str;
    str = c;
    boolean bool = TextUtils.isEmpty(str);
    if (bool)
    {
      str = "Android MmsLib/1.0";
      c = str;
    }
    str = d;
    bool = TextUtils.isEmpty(str);
    if (bool)
    {
      str = "http://www.gstatic.com/android/sms/mms_ua_profile.xml";
      d = str;
    }
  }
  
  public final String a()
  {
    c();
    return c;
  }
  
  public final String b()
  {
    c();
    return d;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.as
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */