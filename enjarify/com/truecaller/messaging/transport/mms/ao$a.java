package com.truecaller.messaging.transport.mms;

import com.truecaller.messaging.transport.k.e;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

final class ao$a
  implements k.e
{
  final long a;
  final Lock b;
  final Condition c;
  
  ao$a(long paramLong)
  {
    Object localObject = new java/util/concurrent/locks/ReentrantLock;
    ((ReentrantLock)localObject).<init>();
    b = ((Lock)localObject);
    localObject = b.newCondition();
    c = ((Condition)localObject);
    a = paramLong;
  }
  
  /* Error */
  public final com.truecaller.messaging.transport.k a(java.util.concurrent.TimeUnit paramTimeUnit)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 21	com/truecaller/messaging/transport/mms/ao$a:b	Ljava/util/concurrent/locks/Lock;
    //   4: astore_2
    //   5: aload_2
    //   6: invokeinterface 34 1 0
    //   11: aload_0
    //   12: getfield 29	com/truecaller/messaging/transport/mms/ao$a:c	Ljava/util/concurrent/locks/Condition;
    //   15: astore_2
    //   16: iconst_2
    //   17: i2l
    //   18: lstore_3
    //   19: aload_2
    //   20: lload_3
    //   21: aload_1
    //   22: invokeinterface 42 4 0
    //   27: istore 5
    //   29: iload 5
    //   31: ifeq +31 -> 62
    //   34: getstatic 47	com/truecaller/messaging/transport/k$d:a	Lcom/truecaller/messaging/transport/k$d;
    //   37: astore_1
    //   38: aload_0
    //   39: getfield 21	com/truecaller/messaging/transport/mms/ao$a:b	Ljava/util/concurrent/locks/Lock;
    //   42: invokeinterface 50 1 0
    //   47: aload_1
    //   48: areturn
    //   49: astore_1
    //   50: aload_0
    //   51: getfield 21	com/truecaller/messaging/transport/mms/ao$a:b	Ljava/util/concurrent/locks/Lock;
    //   54: invokeinterface 50 1 0
    //   59: aload_1
    //   60: athrow
    //   61: pop
    //   62: aload_0
    //   63: getfield 21	com/truecaller/messaging/transport/mms/ao$a:b	Ljava/util/concurrent/locks/Lock;
    //   66: invokeinterface 50 1 0
    //   71: getstatic 55	com/truecaller/messaging/transport/k$b:a	Lcom/truecaller/messaging/transport/k$b;
    //   74: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	75	0	this	a
    //   0	75	1	paramTimeUnit	java.util.concurrent.TimeUnit
    //   4	16	2	localObject	Object
    //   18	3	3	l	long
    //   27	3	5	bool	boolean
    //   61	1	5	localInterruptedException	InterruptedException
    // Exception table:
    //   from	to	target	type
    //   11	15	49	finally
    //   21	27	49	finally
    //   34	37	49	finally
    //   11	15	61	java/lang/InterruptedException
    //   21	27	61	java/lang/InterruptedException
    //   34	37	61	java/lang/InterruptedException
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.ao.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */