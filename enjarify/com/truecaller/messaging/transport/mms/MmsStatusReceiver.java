package com.truecaller.messaging.transport.mms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.messaging.transport.m;

public class MmsStatusReceiver
  extends BroadcastReceiver
{
  public static Intent a(Context paramContext, Uri paramUri1, Uri paramUri2, long paramLong1, long paramLong2, String paramString)
  {
    Intent localIntent = new android/content/Intent;
    String str = "com.truecaller.messaging.transport.mms.MmsStatusReceiver.MMS_SENT";
    Class localClass = MmsStatusReceiver.class;
    localIntent.<init>(str, paramUri1, paramContext, localClass);
    if (paramUri2 != null)
    {
      paramContext = "pdu_uri";
      paramUri1 = paramUri2.toString();
      localIntent.putExtra(paramContext, paramUri1);
    }
    paramContext = "raw_message_id";
    localIntent.putExtra(paramContext, paramLong1);
    long l = -1;
    boolean bool = paramLong2 < l;
    if (bool)
    {
      paramContext = "message_date";
      localIntent.putExtra(paramContext, paramLong2);
    }
    localIntent.putExtra("sim_token", paramString);
    return localIntent;
  }
  
  public static Intent a(Context paramContext, Uri paramUri1, byte[] paramArrayOfByte, Uri paramUri2, long paramLong, boolean paramBoolean, String paramString)
  {
    Intent localIntent = new android/content/Intent;
    String str = "com.truecaller.messaging.transport.mms.MmsStatusReceiver.MMS_DOWNLOAD";
    Class localClass = MmsStatusReceiver.class;
    localIntent.<init>(str, paramUri1, paramContext, localClass);
    if (paramUri2 != null)
    {
      paramContext = "pdu_uri";
      paramUri1 = paramUri2.toString();
      localIntent.putExtra(paramContext, paramUri1);
    }
    localIntent.putExtra("transaction_id", paramArrayOfByte);
    localIntent.putExtra("raw_message_id", paramLong);
    localIntent.putExtra("is_auto_download", paramBoolean);
    localIntent.putExtra("sim_token", paramString);
    return localIntent;
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    int i = 1;
    String[] arrayOfString = new String[i];
    String str1 = String.valueOf(paramIntent);
    String str2 = "MMS status changed: ".concat(str1);
    str1 = null;
    arrayOfString[0] = str2;
    if (paramIntent == null) {
      return;
    }
    paramContext = ((bk)paramContext.getApplicationContext()).a().o();
    int j = getResultCode();
    paramContext.a(i, paramIntent, j);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.MmsStatusReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */