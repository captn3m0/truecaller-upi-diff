package com.truecaller.messaging.transport.mms;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.multisim.h;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Inet4Address;
import java.net.InetAddress;

class ac
  extends aa
{
  private static final Integer c = Integer.valueOf(2);
  private final Method d;
  private final Method e;
  private final Method f;
  private final Method g;
  
  ac(Context paramContext, Handler paramHandler, ConnectivityManager paramConnectivityManager, h paramh)
  {
    super(paramContext, paramHandler, paramConnectivityManager, paramh);
    paramContext = paramConnectivityManager.getClass();
    paramHandler = null;
    int i = 1;
    int j = 2;
    Class localClass1 = null;
    Object localObject1 = "startUsingNetworkFeature";
    Object localObject3;
    Object localObject4;
    try
    {
      localObject3 = new Class[j];
      localObject4 = Integer.TYPE;
      localObject3[0] = localObject4;
      localObject4 = String.class;
      localObject3[i] = localObject4;
      localObject1 = paramContext.getMethod((String)localObject1, (Class[])localObject3);
      localObject3 = "stopUsingNetworkFeature";
      localObject4 = new Class[j];
      Class localClass2 = Integer.TYPE;
      localObject4[0] = localClass2;
      localClass2 = String.class;
      localObject4[i] = localClass2;
      localObject3 = paramContext.getMethod((String)localObject3, (Class[])localObject4);
    }
    catch (NoSuchMethodException localNoSuchMethodException1)
    {
      localObject3 = new String[0];
      AssertionUtil.OnlyInDebug.shouldNeverHappen(localNoSuchMethodException1, (String[])localObject3);
      localObject2 = null;
      localObject3 = null;
    }
    d = ((Method)localObject2);
    e = ((Method)localObject3);
    Object localObject2 = "requestRouteToHostAddress";
    try
    {
      localObject3 = new Class[j];
      localObject4 = Integer.TYPE;
      localObject3[0] = localObject4;
      localObject4 = InetAddress.class;
      localObject3[i] = localObject4;
      localObject2 = paramContext.getMethod((String)localObject2, (Class[])localObject3);
    }
    catch (NoSuchMethodException localNoSuchMethodException2)
    {
      localObject2 = null;
    }
    f = ((Method)localObject2);
    localObject2 = "requestRouteToHost";
    try
    {
      paramh = new Class[j];
      localObject3 = Integer.TYPE;
      paramh[0] = localObject3;
      localClass1 = Integer.TYPE;
      paramh[i] = localClass1;
      paramHandler = paramContext.getMethod((String)localObject2, paramh);
    }
    catch (NoSuchMethodException localNoSuchMethodException3)
    {
      for (;;) {}
    }
    g = paramHandler;
  }
  
  protected final int a(int paramInt)
  {
    return 3;
  }
  
  protected int a(String paramString, boolean paramBoolean)
  {
    new String[1][0] = "Start MMS connectivity";
    paramString = d;
    if (paramString != null) {
      try
      {
        ConnectivityManager localConnectivityManager = a;
        int i = 2;
        Object[] arrayOfObject = new Object[i];
        int j = 0;
        Object localObject = Integer.valueOf(0);
        arrayOfObject[0] = localObject;
        j = 1;
        localObject = "enableMMS";
        arrayOfObject[j] = localObject;
        paramString = paramString.invoke(localConnectivityManager, arrayOfObject);
        paramString = (Integer)paramString;
        return paramString.intValue();
      }
      catch (IllegalAccessException paramString)
      {
        AssertionUtil.reportThrowableButNeverCrash(paramString);
      }
      catch (InvocationTargetException paramString)
      {
        AssertionUtil.reportThrowableButNeverCrash(paramString);
      }
    }
    return 3;
  }
  
  public final boolean a(String paramString, InetAddress paramInetAddress)
  {
    paramString = f;
    int i = 2;
    int j = 1;
    Object localObject1;
    Object localObject2;
    if (paramString != null) {
      try
      {
        localObject1 = a;
        localObject2 = new Object[i];
        Integer localInteger = c;
        localObject2[0] = localInteger;
        localObject2[j] = paramInetAddress;
        paramString = paramString.invoke(localObject1, (Object[])localObject2);
        paramString = (Boolean)paramString;
        return paramString.booleanValue();
      }
      catch (Exception paramString)
      {
        localObject1 = new String[j];
        localObject2 = "ConnectivityManager.requestRouteToHostAddress failed ";
        paramString = String.valueOf(paramString);
        paramString = ((String)localObject2).concat(paramString);
        localObject1[0] = paramString;
      }
    }
    boolean bool = paramInetAddress instanceof Inet4Address;
    if (bool)
    {
      paramString = g;
      if (paramString != null) {
        try
        {
          localObject1 = a;
          localObject3 = new Object[i];
          localObject2 = c;
          localObject3[0] = localObject2;
          int k = b((InetAddress)paramInetAddress);
          paramInetAddress = Integer.valueOf(k);
          localObject3[j] = paramInetAddress;
          paramString = paramString.invoke(localObject1, (Object[])localObject3);
          paramString = (Boolean)paramString;
          return paramString.booleanValue();
        }
        catch (Exception paramString)
        {
          paramInetAddress = new String[j];
          Object localObject3 = "ConnectivityManager.requestRouteToHost failed ";
          paramString = String.valueOf(paramString);
          paramString = ((String)localObject3).concat(paramString);
          paramInetAddress[0] = paramString;
        }
      }
    }
    return false;
  }
  
  protected int c(String paramString)
  {
    new String[1][0] = "End MMS connectivity";
    paramString = e;
    if (paramString != null) {
      try
      {
        ConnectivityManager localConnectivityManager = a;
        int i = 2;
        Object[] arrayOfObject = new Object[i];
        int j = 0;
        Object localObject = Integer.valueOf(0);
        arrayOfObject[0] = localObject;
        j = 1;
        localObject = "enableMMS";
        arrayOfObject[j] = localObject;
        paramString = paramString.invoke(localConnectivityManager, arrayOfObject);
        paramString = (Integer)paramString;
        return paramString.intValue();
      }
      catch (IllegalAccessException paramString)
      {
        AssertionUtil.reportThrowableButNeverCrash(paramString);
      }
      catch (InvocationTargetException paramString)
      {
        AssertionUtil.reportThrowableButNeverCrash(paramString);
      }
    }
    return 3;
  }
  
  protected final NetworkInfo d(String paramString)
  {
    return a.getNetworkInfo(2);
  }
  
  protected final int e(String paramString)
  {
    return 2;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.ac
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */