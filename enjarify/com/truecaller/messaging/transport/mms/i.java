package com.truecaller.messaging.transport.mms;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.util.SparseArray;
import com.android.a.a.a.e;
import com.android.a.a.a.f;
import com.android.a.a.a.g;
import com.android.a.a.a.j;
import com.android.a.a.a.m;
import com.android.a.a.a.o;
import com.android.a.a.c;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.ImageEntity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Participant.a;
import com.truecaller.messaging.data.types.TextEntity;
import com.truecaller.messaging.data.types.TransportInfo;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.a.a.b;
import org.c.a.a.a.k;

final class i
  implements h
{
  private final ContentResolver a;
  private final com.truecaller.common.h.u b;
  private final File c;
  
  i(ContentResolver paramContentResolver, com.truecaller.common.h.u paramu, File paramFile)
  {
    a = paramContentResolver;
    b = paramu;
    paramContentResolver = new java/io/File;
    paramContentResolver.<init>(paramFile, "pdu_parts");
    c = paramContentResolver;
  }
  
  private j a(h.a parama)
  {
    j localj = new com/android/a/a/a/j;
    localj.<init>();
    int i = 0;
    Object localObject1 = null;
    for (;;)
    {
      boolean bool1 = parama.moveToNext();
      if (!bool1) {
        break;
      }
      Object localObject2 = parama.a();
      boolean bool2 = Entity.a((String)localObject2);
      int j = 106;
      Object localObject3 = null;
      int k = 1;
      if (!bool2)
      {
        bool2 = Entity.b((String)localObject2);
        if (!bool2)
        {
          bool2 = Entity.c((String)localObject2);
          if (bool2)
          {
            localObject4 = parama.c();
            str1 = parama.d();
            m = i + 1;
            n = b((Uri)localObject4);
            if (n >= 0)
            {
              localObject3 = new com/android/a/a/a/o;
              ((o)localObject3).<init>();
              localObject2 = ((String)localObject2).getBytes();
              ((o)localObject3).e((byte[])localObject2);
              ((o)localObject3).a((Uri)localObject4, n);
              localObject2 = Locale.US;
              localObject4 = "image.%06d";
              arrayOfObject = new Object[k];
              localObject1 = Integer.valueOf(i);
              arrayOfObject[0] = localObject1;
              localObject1 = String.format((Locale)localObject2, (String)localObject4, arrayOfObject).getBytes();
              ((o)localObject3).b((byte[])localObject1);
              localObject1 = str1.getBytes();
              ((o)localObject3).c((byte[])localObject1);
            }
            i = m;
            break label664;
          }
          bool2 = Entity.f((String)localObject2);
          if (bool2)
          {
            localObject4 = parama.c();
            String str2 = parama.d();
            n = i + 1;
            int i1 = b((Uri)localObject4);
            if (i1 >= 0)
            {
              localObject3 = new com/android/a/a/a/o;
              ((o)localObject3).<init>();
              ((o)localObject3).a(j);
              localObject2 = ((String)localObject2).getBytes();
              ((o)localObject3).e((byte[])localObject2);
              localObject2 = Locale.US;
              str1 = "vcard.%06d";
              arrayOfObject = new Object[k];
              localObject1 = Integer.valueOf(i);
              arrayOfObject[0] = localObject1;
              localObject1 = String.format((Locale)localObject2, str1, arrayOfObject).getBytes();
              ((o)localObject3).b((byte[])localObject1);
              localObject1 = str2.getBytes();
              ((o)localObject3).c((byte[])localObject1);
              ((o)localObject3).a((Uri)localObject4, i1);
            }
            i = n;
            break label664;
          }
          bool2 = Entity.d((String)localObject2);
          if (!bool2) {
            break label664;
          }
          localObject4 = parama.c();
          str1 = parama.d();
          m = i + 1;
          int n = b((Uri)localObject4);
          if (n < 0) {
            break label661;
          }
          localObject3 = new com/android/a/a/a/o;
          ((o)localObject3).<init>();
          localObject2 = ((String)localObject2).getBytes();
          ((o)localObject3).e((byte[])localObject2);
          ((o)localObject3).a((Uri)localObject4, n);
          localObject2 = Locale.US;
          localObject4 = "video.%06d";
          arrayOfObject = new Object[k];
          localObject1 = Integer.valueOf(i);
          arrayOfObject[0] = localObject1;
          localObject1 = String.format((Locale)localObject2, (String)localObject4, arrayOfObject).getBytes();
          ((o)localObject3).b((byte[])localObject1);
          localObject1 = str1.getBytes();
          ((o)localObject3).c((byte[])localObject1);
          break label661;
        }
      }
      Object localObject4 = parama.b();
      localObject3 = parama.d();
      int m = i + 1;
      o localo = new com/android/a/a/a/o;
      localo.<init>();
      localo.a(j);
      localObject2 = ((String)localObject2).getBytes();
      localo.e((byte[])localObject2);
      localObject2 = Locale.US;
      String str1 = "text.%06d";
      Object[] arrayOfObject = new Object[k];
      localObject1 = Integer.valueOf(i);
      arrayOfObject[0] = localObject1;
      localObject1 = String.format((Locale)localObject2, str1, arrayOfObject).getBytes();
      localo.b((byte[])localObject1);
      localObject1 = ((String)localObject3).getBytes();
      localo.c((byte[])localObject1);
      localObject1 = ((String)localObject4).getBytes();
      localo.a((byte[])localObject1);
      localObject3 = localo;
      label661:
      i = m;
      label664:
      if (localObject3 != null) {
        localj.a((o)localObject3);
      }
    }
    return localj;
  }
  
  /* Error */
  private PduEntity a(ImageEntity paramImageEntity, int paramInt1, int paramInt2, int paramInt3, StringBuilder paramStringBuilder, int paramInt4)
  {
    // Byte code:
    //   0: aload_1
    //   1: astore 7
    //   3: new 122	android/graphics/BitmapFactory$Options
    //   6: astore 8
    //   8: aload 8
    //   10: invokespecial 123	android/graphics/BitmapFactory$Options:<init>	()V
    //   13: iconst_0
    //   14: istore 9
    //   16: fconst_0
    //   17: fstore 10
    //   19: aconst_null
    //   20: astore 11
    //   22: aload_0
    //   23: astore 12
    //   25: aload_0
    //   26: getfield 18	com/truecaller/messaging/transport/mms/i:a	Landroid/content/ContentResolver;
    //   29: astore 13
    //   31: aload_1
    //   32: getfield 128	com/truecaller/messaging/data/types/ImageEntity:b	Landroid/net/Uri;
    //   35: astore 14
    //   37: aload 13
    //   39: aload 14
    //   41: invokevirtual 134	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   44: astore 13
    //   46: aload 13
    //   48: ifnonnull +10 -> 58
    //   51: aload 13
    //   53: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   56: aconst_null
    //   57: areturn
    //   58: aload 13
    //   60: invokevirtual 145	java/io/InputStream:available	()I
    //   63: istore 15
    //   65: iconst_1
    //   66: istore 16
    //   68: ldc 48
    //   70: fstore 17
    //   72: aload 8
    //   74: iload 16
    //   76: putfield 149	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   79: aload 13
    //   81: aconst_null
    //   82: aload 8
    //   84: invokestatic 155	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   87: pop
    //   88: aload 13
    //   90: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   93: aload_1
    //   94: getfield 159	com/truecaller/messaging/data/types/ImageEntity:j	Ljava/lang/String;
    //   97: astore 13
    //   99: aload 8
    //   101: getfield 163	android/graphics/BitmapFactory$Options:outWidth	I
    //   104: istore 18
    //   106: aload 8
    //   108: getfield 166	android/graphics/BitmapFactory$Options:outHeight	I
    //   111: istore 19
    //   113: ldc -89
    //   115: istore 20
    //   117: ldc -87
    //   119: istore 21
    //   121: ldc -85
    //   123: istore 22
    //   125: iload_2
    //   126: istore 23
    //   128: iload 18
    //   130: iload_2
    //   131: if_icmpgt +54 -> 185
    //   134: iload_3
    //   135: istore 24
    //   137: iload 19
    //   139: iload_3
    //   140: if_icmple +6 -> 146
    //   143: goto +45 -> 188
    //   146: iload 6
    //   148: istore 25
    //   150: aload 8
    //   152: astore 26
    //   154: iconst_0
    //   155: istore 23
    //   157: aconst_null
    //   158: astore 27
    //   160: aload_0
    //   161: astore 8
    //   163: aload 13
    //   165: astore 28
    //   167: iload 15
    //   169: istore 24
    //   171: aload 5
    //   173: astore 13
    //   175: aload_1
    //   176: astore 14
    //   178: iload 4
    //   180: istore 29
    //   182: goto +391 -> 573
    //   185: iload_3
    //   186: istore 24
    //   188: iload 18
    //   190: iload 19
    //   192: if_icmple +37 -> 229
    //   195: iload 4
    //   197: istore 30
    //   199: aload 5
    //   201: astore 31
    //   203: iload 6
    //   205: istore 25
    //   207: aload 7
    //   209: astore 14
    //   211: aload 8
    //   213: astore 26
    //   215: aload 12
    //   217: astore 8
    //   219: iconst_1
    //   220: istore 29
    //   222: ldc 48
    //   224: fstore 32
    //   226: goto +60 -> 286
    //   229: iload 4
    //   231: istore 33
    //   233: aload 5
    //   235: astore 34
    //   237: iload 6
    //   239: istore 35
    //   241: aload 8
    //   243: astore 14
    //   245: aload 7
    //   247: astore 8
    //   249: aload 12
    //   251: astore 7
    //   253: aload 14
    //   255: astore 26
    //   257: iload 33
    //   259: istore 30
    //   261: aload 34
    //   263: astore 31
    //   265: iload 35
    //   267: istore 25
    //   269: aload 8
    //   271: astore 14
    //   273: aload 7
    //   275: astore 8
    //   277: iconst_0
    //   278: istore 29
    //   280: fconst_0
    //   281: fstore 32
    //   283: aconst_null
    //   284: astore 7
    //   286: iload 18
    //   288: iload 23
    //   290: if_icmpgt +1172 -> 1462
    //   293: iload 19
    //   295: iload 24
    //   297: if_icmple +6 -> 303
    //   300: goto +1162 -> 1462
    //   303: aload 13
    //   305: invokevirtual 175	java/lang/String:hashCode	()I
    //   308: istore 29
    //   310: iload 29
    //   312: iload 22
    //   314: if_icmpeq +78 -> 392
    //   317: iload 29
    //   319: iload 21
    //   321: if_icmpeq +43 -> 364
    //   324: iload 29
    //   326: iload 20
    //   328: if_icmpeq +6 -> 334
    //   331: goto +89 -> 420
    //   334: ldc -79
    //   336: astore 7
    //   338: aload 13
    //   340: aload 7
    //   342: invokevirtual 181	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   345: istore 29
    //   347: iload 29
    //   349: ifeq +71 -> 420
    //   352: iconst_0
    //   353: istore 29
    //   355: fconst_0
    //   356: fstore 32
    //   358: aconst_null
    //   359: astore 7
    //   361: goto +67 -> 428
    //   364: ldc -73
    //   366: astore 7
    //   368: aload 13
    //   370: aload 7
    //   372: invokevirtual 181	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   375: istore 29
    //   377: iload 29
    //   379: ifeq +41 -> 420
    //   382: iconst_1
    //   383: istore 29
    //   385: ldc 48
    //   387: fstore 32
    //   389: goto +39 -> 428
    //   392: ldc -71
    //   394: astore 7
    //   396: aload 13
    //   398: aload 7
    //   400: invokevirtual 181	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   403: istore 29
    //   405: iload 29
    //   407: ifeq +13 -> 420
    //   410: iconst_2
    //   411: istore 29
    //   413: ldc -70
    //   415: fstore 32
    //   417: goto +11 -> 428
    //   420: iconst_m1
    //   421: istore 29
    //   423: fconst_0
    //   424: fconst_0
    //   425: fdiv
    //   426: fstore 32
    //   428: iload 29
    //   430: tableswitch	default:+26->456, 0:+54->484, 1:+42->472, 2:+42->472
    //   456: getstatic 192	android/graphics/Bitmap$CompressFormat:JPEG	Landroid/graphics/Bitmap$CompressFormat;
    //   459: astore 7
    //   461: ldc -73
    //   463: astore 13
    //   465: aload 7
    //   467: astore 36
    //   469: goto +24 -> 493
    //   472: getstatic 192	android/graphics/Bitmap$CompressFormat:JPEG	Landroid/graphics/Bitmap$CompressFormat;
    //   475: astore 7
    //   477: aload 7
    //   479: astore 36
    //   481: goto +12 -> 493
    //   484: getstatic 195	android/graphics/Bitmap$CompressFormat:PNG	Landroid/graphics/Bitmap$CompressFormat;
    //   487: astore 7
    //   489: aload 7
    //   491: astore 36
    //   493: aload 8
    //   495: getfield 18	com/truecaller/messaging/transport/mms/i:a	Landroid/content/ContentResolver;
    //   498: astore 7
    //   500: aload 14
    //   502: getfield 128	com/truecaller/messaging/data/types/ImageEntity:b	Landroid/net/Uri;
    //   505: astore 37
    //   507: aload 26
    //   509: getfield 163	android/graphics/BitmapFactory$Options:outWidth	I
    //   512: istore 23
    //   514: aload 26
    //   516: getfield 166	android/graphics/BitmapFactory$Options:outHeight	I
    //   519: istore 20
    //   521: bipush 95
    //   523: istore 38
    //   525: aload 37
    //   527: astore 34
    //   529: iload 23
    //   531: istore 35
    //   533: aload 7
    //   535: aload 37
    //   537: iload 23
    //   539: iload 18
    //   541: iload 19
    //   543: aload 36
    //   545: iload 38
    //   547: invokestatic 201	com/truecaller/common/h/n:a	(Landroid/content/ContentResolver;Landroid/net/Uri;IIILandroid/graphics/Bitmap$CompressFormat;I)[B
    //   550: astore 7
    //   552: aload 7
    //   554: arraylength
    //   555: istore 24
    //   557: aload 7
    //   559: astore 27
    //   561: aload 13
    //   563: astore 28
    //   565: iload 30
    //   567: istore 29
    //   569: aload 31
    //   571: astore 13
    //   573: iload 24
    //   575: iload 29
    //   577: if_icmple +314 -> 891
    //   580: getstatic 192	android/graphics/Bitmap$CompressFormat:JPEG	Landroid/graphics/Bitmap$CompressFormat;
    //   583: astore 28
    //   585: ldc -73
    //   587: astore 27
    //   589: bipush 95
    //   591: istore 30
    //   593: iload 24
    //   595: istore 21
    //   597: iload 19
    //   599: istore 24
    //   601: iload 18
    //   603: istore 19
    //   605: bipush 95
    //   607: istore 18
    //   609: bipush 50
    //   611: istore 22
    //   613: iload 18
    //   615: iload 22
    //   617: if_icmple +112 -> 729
    //   620: aload 27
    //   622: astore_1
    //   623: iload 18
    //   625: i2d
    //   626: dstore 39
    //   628: ldc2_w 205
    //   631: dstore 41
    //   633: iload 29
    //   635: i2d
    //   636: dstore 43
    //   638: dload 43
    //   640: invokestatic 212	java/lang/Double:isNaN	(D)Z
    //   643: pop
    //   644: dload 43
    //   646: dload 41
    //   648: dmul
    //   649: dstore 43
    //   651: iload 21
    //   653: i2d
    //   654: dstore 45
    //   656: dload 45
    //   658: invokestatic 212	java/lang/Double:isNaN	(D)Z
    //   661: pop
    //   662: dload 43
    //   664: dload 45
    //   666: ddiv
    //   667: invokestatic 218	java/lang/Math:sqrt	(D)D
    //   670: dstore 45
    //   672: dload 39
    //   674: invokestatic 212	java/lang/Double:isNaN	(D)Z
    //   677: pop
    //   678: dload 45
    //   680: dload 39
    //   682: dmul
    //   683: dstore 45
    //   685: dload 45
    //   687: d2i
    //   688: istore 9
    //   690: ldc2_w 221
    //   693: dstore 43
    //   695: dload 39
    //   697: invokestatic 212	java/lang/Double:isNaN	(D)Z
    //   700: pop
    //   701: dload 39
    //   703: dload 43
    //   705: dmul
    //   706: dstore 39
    //   708: dload 39
    //   710: d2i
    //   711: istore 47
    //   713: iload 9
    //   715: iload 47
    //   717: invokestatic 226	java/lang/Math:min	(II)I
    //   720: istore 9
    //   722: iload 9
    //   724: istore 18
    //   726: goto +52 -> 778
    //   729: aload 27
    //   731: astore_1
    //   732: iload 19
    //   734: i2f
    //   735: fstore 10
    //   737: ldc -28
    //   739: fstore 48
    //   741: fload 10
    //   743: fload 48
    //   745: fmul
    //   746: fstore 10
    //   748: fload 10
    //   750: f2i
    //   751: istore 9
    //   753: iload 24
    //   755: i2f
    //   756: fload 48
    //   758: fmul
    //   759: fstore 17
    //   761: fload 17
    //   763: f2i
    //   764: istore 47
    //   766: iload 9
    //   768: istore 19
    //   770: iload 47
    //   772: istore 24
    //   774: bipush 95
    //   776: istore 18
    //   778: aload 8
    //   780: getfield 18	com/truecaller/messaging/transport/mms/i:a	Landroid/content/ContentResolver;
    //   783: astore 11
    //   785: aload 14
    //   787: getfield 128	com/truecaller/messaging/data/types/ImageEntity:b	Landroid/net/Uri;
    //   790: astore 12
    //   792: aload 26
    //   794: getfield 163	android/graphics/BitmapFactory$Options:outWidth	I
    //   797: istore 16
    //   799: aload 26
    //   801: getfield 166	android/graphics/BitmapFactory$Options:outHeight	I
    //   804: istore 21
    //   806: aload 12
    //   808: astore 34
    //   810: iload 16
    //   812: istore 35
    //   814: aload 28
    //   816: astore 36
    //   818: iload 18
    //   820: istore 38
    //   822: aload 11
    //   824: aload 12
    //   826: iload 16
    //   828: iload 19
    //   830: iload 24
    //   832: aload 28
    //   834: iload 18
    //   836: invokestatic 201	com/truecaller/common/h/n:a	(Landroid/content/ContentResolver;Landroid/net/Uri;IIILandroid/graphics/Bitmap$CompressFormat;I)[B
    //   839: astore 27
    //   841: aload 27
    //   843: arraylength
    //   844: istore 21
    //   846: iload 21
    //   848: iload 29
    //   850: if_icmpgt +13 -> 863
    //   853: aload_1
    //   854: astore 12
    //   856: iload 21
    //   858: istore 24
    //   860: goto +35 -> 895
    //   863: aload_1
    //   864: astore 27
    //   866: iconst_0
    //   867: istore 9
    //   869: fconst_0
    //   870: fstore 10
    //   872: aconst_null
    //   873: astore 11
    //   875: aload_0
    //   876: astore 12
    //   878: iconst_1
    //   879: istore 16
    //   881: ldc 48
    //   883: fstore 17
    //   885: goto -276 -> 609
    //   888: pop
    //   889: aconst_null
    //   890: areturn
    //   891: aload 28
    //   893: astore 12
    //   895: getstatic 83	java/util/Locale:US	Ljava/util/Locale;
    //   898: astore 7
    //   900: ldc 85
    //   902: astore 11
    //   904: iconst_1
    //   905: istore 16
    //   907: ldc 48
    //   909: fstore 17
    //   911: iload 16
    //   913: anewarray 4	java/lang/Object
    //   916: astore 49
    //   918: iload 25
    //   920: invokestatic 91	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   923: astore 50
    //   925: iconst_0
    //   926: istore 19
    //   928: aconst_null
    //   929: astore 51
    //   931: aload 49
    //   933: iconst_0
    //   934: aload 50
    //   936: aastore
    //   937: aload 7
    //   939: aload 11
    //   941: aload 49
    //   943: invokestatic 95	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   946: astore 49
    //   948: aload 12
    //   950: invokevirtual 175	java/lang/String:hashCode	()I
    //   953: istore 29
    //   955: ldc -85
    //   957: istore 9
    //   959: ldc -84
    //   961: fstore 10
    //   963: iload 29
    //   965: iload 9
    //   967: if_icmpeq +126 -> 1093
    //   970: ldc -27
    //   972: istore 9
    //   974: ldc -26
    //   976: fstore 10
    //   978: iload 29
    //   980: iload 9
    //   982: if_icmpeq +80 -> 1062
    //   985: ldc -87
    //   987: istore 16
    //   989: ldc -86
    //   991: fstore 17
    //   993: iload 29
    //   995: iload 16
    //   997: if_icmpeq +41 -> 1038
    //   1000: ldc -89
    //   1002: istore 20
    //   1004: iload 29
    //   1006: iload 20
    //   1008: if_icmpeq +6 -> 1014
    //   1011: goto +106 -> 1117
    //   1014: ldc -79
    //   1016: astore 7
    //   1018: aload 12
    //   1020: aload 7
    //   1022: invokevirtual 181	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1025: istore 29
    //   1027: iload 29
    //   1029: ifeq +88 -> 1117
    //   1032: iconst_2
    //   1033: istore 52
    //   1035: goto +85 -> 1120
    //   1038: ldc -73
    //   1040: astore 7
    //   1042: aload 12
    //   1044: aload 7
    //   1046: invokevirtual 181	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1049: istore 29
    //   1051: iload 29
    //   1053: ifeq +64 -> 1117
    //   1056: iconst_0
    //   1057: istore 52
    //   1059: goto +61 -> 1120
    //   1062: ldc -24
    //   1064: astore 7
    //   1066: aload 12
    //   1068: aload 7
    //   1070: invokevirtual 181	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1073: istore 29
    //   1075: iload 29
    //   1077: ifeq +40 -> 1117
    //   1080: iconst_3
    //   1081: istore 16
    //   1083: ldc -23
    //   1085: fstore 17
    //   1087: iconst_3
    //   1088: istore 52
    //   1090: goto +30 -> 1120
    //   1093: ldc -71
    //   1095: astore 7
    //   1097: aload 12
    //   1099: aload 7
    //   1101: invokevirtual 181	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   1104: istore 29
    //   1106: iload 29
    //   1108: ifeq +9 -> 1117
    //   1111: iconst_1
    //   1112: istore 52
    //   1114: goto +6 -> 1120
    //   1117: iconst_m1
    //   1118: istore 52
    //   1120: iload 52
    //   1122: tableswitch	default:+30->1152, 0:+128->1250, 1:+128->1250, 2:+84->1206, 3:+40->1162
    //   1152: iconst_1
    //   1153: anewarray 66	java/lang/String
    //   1156: iconst_0
    //   1157: ldc -21
    //   1159: aastore
    //   1160: aconst_null
    //   1161: areturn
    //   1162: new 237	java/lang/StringBuilder
    //   1165: astore 7
    //   1167: aload 7
    //   1169: invokespecial 238	java/lang/StringBuilder:<init>	()V
    //   1172: aload 7
    //   1174: aload 49
    //   1176: invokevirtual 242	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1179: pop
    //   1180: ldc -12
    //   1182: astore 11
    //   1184: aload 7
    //   1186: aload 11
    //   1188: invokevirtual 242	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1191: pop
    //   1192: aload 7
    //   1194: invokevirtual 247	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1197: astore 7
    //   1199: aload 7
    //   1201: astore 51
    //   1203: goto +88 -> 1291
    //   1206: new 237	java/lang/StringBuilder
    //   1209: astore 7
    //   1211: aload 7
    //   1213: invokespecial 238	java/lang/StringBuilder:<init>	()V
    //   1216: aload 7
    //   1218: aload 49
    //   1220: invokevirtual 242	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1223: pop
    //   1224: ldc -7
    //   1226: astore 11
    //   1228: aload 7
    //   1230: aload 11
    //   1232: invokevirtual 242	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1235: pop
    //   1236: aload 7
    //   1238: invokevirtual 247	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1241: astore 7
    //   1243: aload 7
    //   1245: astore 51
    //   1247: goto +44 -> 1291
    //   1250: new 237	java/lang/StringBuilder
    //   1253: astore 7
    //   1255: aload 7
    //   1257: invokespecial 238	java/lang/StringBuilder:<init>	()V
    //   1260: aload 7
    //   1262: aload 49
    //   1264: invokevirtual 242	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1267: pop
    //   1268: ldc -5
    //   1270: astore 11
    //   1272: aload 7
    //   1274: aload 11
    //   1276: invokevirtual 242	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1279: pop
    //   1280: aload 7
    //   1282: invokevirtual 247	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1285: astore 7
    //   1287: aload 7
    //   1289: astore 51
    //   1291: aload 27
    //   1293: ifnull +97 -> 1390
    //   1296: aload 8
    //   1298: getfield 18	com/truecaller/messaging/transport/mms/i:a	Landroid/content/ContentResolver;
    //   1301: astore 7
    //   1303: aload 14
    //   1305: getfield 128	com/truecaller/messaging/data/types/ImageEntity:b	Landroid/net/Uri;
    //   1308: astore 8
    //   1310: aload 7
    //   1312: aload 8
    //   1314: invokevirtual 255	android/content/ContentResolver:openOutputStream	(Landroid/net/Uri;)Ljava/io/OutputStream;
    //   1317: astore 11
    //   1319: aload 11
    //   1321: ifnonnull +10 -> 1331
    //   1324: aload 11
    //   1326: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   1329: aconst_null
    //   1330: areturn
    //   1331: aload 11
    //   1333: aload 27
    //   1335: invokevirtual 260	java/io/OutputStream:write	([B)V
    //   1338: aload 11
    //   1340: invokevirtual 263	java/io/OutputStream:flush	()V
    //   1343: aload 11
    //   1345: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   1348: goto +42 -> 1390
    //   1351: astore 7
    //   1353: aload 11
    //   1355: astore 53
    //   1357: goto +8 -> 1365
    //   1360: astore 7
    //   1362: aconst_null
    //   1363: astore 53
    //   1365: aload 53
    //   1367: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   1370: aload 7
    //   1372: athrow
    //   1373: pop
    //   1374: iconst_0
    //   1375: istore 9
    //   1377: fconst_0
    //   1378: fstore 10
    //   1380: aconst_null
    //   1381: astore 11
    //   1383: aload 11
    //   1385: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   1388: aconst_null
    //   1389: areturn
    //   1390: iconst_1
    //   1391: anewarray 4	java/lang/Object
    //   1394: astore 8
    //   1396: aload 8
    //   1398: iconst_0
    //   1399: aload 51
    //   1401: aastore
    //   1402: ldc_w 265
    //   1405: aload 8
    //   1407: invokestatic 268	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   1410: astore 7
    //   1412: aload 13
    //   1414: aload 7
    //   1416: invokevirtual 242	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1419: pop
    //   1420: new 270	com/truecaller/messaging/transport/mms/PduEntity
    //   1423: astore 7
    //   1425: aload 14
    //   1427: getfield 128	com/truecaller/messaging/data/types/ImageEntity:b	Landroid/net/Uri;
    //   1430: astore 13
    //   1432: aload 7
    //   1434: astore 11
    //   1436: iload 24
    //   1438: istore 15
    //   1440: aload 7
    //   1442: aload 12
    //   1444: aload 13
    //   1446: iload 24
    //   1448: iconst_m1
    //   1449: aload 49
    //   1451: aload 51
    //   1453: invokespecial 273	com/truecaller/messaging/transport/mms/PduEntity:<init>	(Ljava/lang/String;Landroid/net/Uri;IILjava/lang/String;Ljava/lang/String;)V
    //   1456: aload 7
    //   1458: areturn
    //   1459: pop
    //   1460: aconst_null
    //   1461: areturn
    //   1462: ldc -85
    //   1464: istore 9
    //   1466: ldc -84
    //   1468: fstore 10
    //   1470: ldc -87
    //   1472: istore 16
    //   1474: ldc -86
    //   1476: fstore 17
    //   1478: iconst_1
    //   1479: istore 21
    //   1481: iconst_0
    //   1482: istore 22
    //   1484: iload 29
    //   1486: ifeq +91 -> 1577
    //   1489: iload 18
    //   1491: i2f
    //   1492: fstore 32
    //   1494: iload 19
    //   1496: i2f
    //   1497: fstore 48
    //   1499: fload 32
    //   1501: fload 48
    //   1503: fdiv
    //   1504: fstore 32
    //   1506: iload 23
    //   1508: i2f
    //   1509: fload 32
    //   1511: fdiv
    //   1512: fstore 48
    //   1514: fload 48
    //   1516: f2i
    //   1517: istore 19
    //   1519: aload 8
    //   1521: astore 7
    //   1523: aload 14
    //   1525: astore 8
    //   1527: aload 26
    //   1529: astore 14
    //   1531: iload 23
    //   1533: istore 18
    //   1535: iload 30
    //   1537: istore 33
    //   1539: aload 31
    //   1541: astore 34
    //   1543: iload 25
    //   1545: istore 35
    //   1547: iconst_0
    //   1548: istore 9
    //   1550: fconst_0
    //   1551: fstore 10
    //   1553: aconst_null
    //   1554: astore 11
    //   1556: aload_0
    //   1557: astore 12
    //   1559: iconst_1
    //   1560: istore 16
    //   1562: ldc 48
    //   1564: fstore 17
    //   1566: ldc -87
    //   1568: istore 21
    //   1570: ldc -85
    //   1572: istore 22
    //   1574: goto -1321 -> 253
    //   1577: iload 19
    //   1579: i2f
    //   1580: fstore 32
    //   1582: iload 18
    //   1584: i2f
    //   1585: fstore 48
    //   1587: fload 32
    //   1589: fload 48
    //   1591: fdiv
    //   1592: fstore 32
    //   1594: iload 24
    //   1596: i2f
    //   1597: fload 32
    //   1599: fdiv
    //   1600: fstore 48
    //   1602: fload 48
    //   1604: f2i
    //   1605: istore 18
    //   1607: iload 24
    //   1609: istore 19
    //   1611: iconst_1
    //   1612: istore 29
    //   1614: ldc 48
    //   1616: fstore 32
    //   1618: iconst_0
    //   1619: istore 9
    //   1621: fconst_0
    //   1622: fstore 10
    //   1624: aconst_null
    //   1625: astore 11
    //   1627: aload_0
    //   1628: astore 12
    //   1630: iconst_1
    //   1631: istore 16
    //   1633: ldc 48
    //   1635: fstore 17
    //   1637: ldc -87
    //   1639: istore 21
    //   1641: ldc -85
    //   1643: istore 22
    //   1645: goto -1359 -> 286
    //   1648: astore 7
    //   1650: aload 13
    //   1652: astore 53
    //   1654: goto +8 -> 1662
    //   1657: astore 7
    //   1659: aconst_null
    //   1660: astore 53
    //   1662: aload 53
    //   1664: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   1667: aload 7
    //   1669: athrow
    //   1670: pop
    //   1671: aconst_null
    //   1672: astore 13
    //   1674: aload 13
    //   1676: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   1679: aconst_null
    //   1680: areturn
    //   1681: pop
    //   1682: goto -299 -> 1383
    //   1685: pop
    //   1686: goto -12 -> 1674
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1689	0	this	i
    //   0	1689	1	paramImageEntity	ImageEntity
    //   0	1689	2	paramInt1	int
    //   0	1689	3	paramInt2	int
    //   0	1689	4	paramInt3	int
    //   0	1689	5	paramStringBuilder	StringBuilder
    //   0	1689	6	paramInt4	int
    //   1	1310	7	localObject1	Object
    //   1351	1	7	localObject2	Object
    //   1360	11	7	localObject3	Object
    //   1410	112	7	localObject4	Object
    //   1648	1	7	localObject5	Object
    //   1657	11	7	localObject6	Object
    //   6	1520	8	localObject7	Object
    //   14	1606	9	i	int
    //   17	1606	10	f1	float
    //   20	1606	11	localObject8	Object
    //   23	1606	12	localObject9	Object
    //   29	1646	13	localObject10	Object
    //   35	1495	14	localObject11	Object
    //   63	1376	15	j	int
    //   66	1566	16	k	int
    //   70	1566	17	f2	float
    //   104	1502	18	m	int
    //   111	1499	19	n	int
    //   115	894	20	i1	int
    //   119	1521	21	i2	int
    //   123	1521	22	i3	int
    //   126	1406	23	i4	int
    //   135	1473	24	i5	int
    //   148	1396	25	i6	int
    //   152	1376	26	localObject12	Object
    //   158	1176	27	localObject13	Object
    //   165	727	28	localObject14	Object
    //   180	149	29	i7	int
    //   345	61	29	bool1	boolean
    //   411	598	29	i8	int
    //   1025	588	29	bool2	boolean
    //   197	1339	30	i9	int
    //   201	1339	31	localObject15	Object
    //   224	1393	32	f3	float
    //   231	1307	33	i10	int
    //   235	1307	34	localObject16	Object
    //   239	1307	35	i11	int
    //   467	350	36	localObject17	Object
    //   505	31	37	localUri	Uri
    //   523	298	38	i12	int
    //   626	83	39	d1	double
    //   631	16	41	d2	double
    //   636	68	43	d3	double
    //   654	32	45	d4	double
    //   711	60	47	i13	int
    //   739	864	48	f4	float
    //   916	534	49	localObject18	Object
    //   923	12	50	localInteger	Integer
    //   929	523	51	localObject19	Object
    //   1033	88	52	i14	int
    //   1355	308	53	localObject20	Object
    //   888	1	58	localIOException1	java.io.IOException
    //   1373	1	59	localIOException2	java.io.IOException
    //   1459	1	60	localIOException3	java.io.IOException
    //   1670	1	61	localIOException4	java.io.IOException
    //   1681	1	62	localIOException5	java.io.IOException
    //   1685	1	63	localIOException6	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   778	783	888	java/io/IOException
    //   778	783	888	java/lang/SecurityException
    //   785	790	888	java/io/IOException
    //   785	790	888	java/lang/SecurityException
    //   792	797	888	java/io/IOException
    //   792	797	888	java/lang/SecurityException
    //   799	804	888	java/io/IOException
    //   799	804	888	java/lang/SecurityException
    //   834	839	888	java/io/IOException
    //   834	839	888	java/lang/SecurityException
    //   841	844	888	java/io/IOException
    //   841	844	888	java/lang/SecurityException
    //   1333	1338	1351	finally
    //   1338	1343	1351	finally
    //   1296	1301	1360	finally
    //   1303	1308	1360	finally
    //   1312	1317	1360	finally
    //   1296	1301	1373	java/io/IOException
    //   1296	1301	1373	java/lang/SecurityException
    //   1303	1308	1373	java/io/IOException
    //   1303	1308	1373	java/lang/SecurityException
    //   1312	1317	1373	java/io/IOException
    //   1312	1317	1373	java/lang/SecurityException
    //   493	498	1459	java/io/IOException
    //   493	498	1459	java/lang/SecurityException
    //   500	505	1459	java/io/IOException
    //   500	505	1459	java/lang/SecurityException
    //   507	512	1459	java/io/IOException
    //   507	512	1459	java/lang/SecurityException
    //   514	519	1459	java/io/IOException
    //   514	519	1459	java/lang/SecurityException
    //   545	550	1459	java/io/IOException
    //   545	550	1459	java/lang/SecurityException
    //   552	555	1459	java/io/IOException
    //   552	555	1459	java/lang/SecurityException
    //   58	63	1648	finally
    //   74	79	1648	finally
    //   82	88	1648	finally
    //   25	29	1657	finally
    //   31	35	1657	finally
    //   39	44	1657	finally
    //   25	29	1670	java/io/IOException
    //   25	29	1670	java/lang/SecurityException
    //   31	35	1670	java/io/IOException
    //   31	35	1670	java/lang/SecurityException
    //   39	44	1670	java/io/IOException
    //   39	44	1670	java/lang/SecurityException
    //   1333	1338	1681	java/io/IOException
    //   1333	1338	1681	java/lang/SecurityException
    //   1338	1343	1681	java/io/IOException
    //   1338	1343	1681	java/lang/SecurityException
    //   58	63	1685	java/io/IOException
    //   58	63	1685	java/lang/SecurityException
    //   74	79	1685	java/io/IOException
    //   74	79	1685	java/lang/SecurityException
    //   82	88	1685	java/io/IOException
    //   82	88	1685	java/lang/SecurityException
  }
  
  /* Error */
  private File a(byte[] paramArrayOfByte)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 29	com/truecaller/messaging/transport/mms/i:c	Ljava/io/File;
    //   4: astore_2
    //   5: aload_2
    //   6: invokevirtual 280	java/io/File:exists	()Z
    //   9: istore_3
    //   10: iconst_0
    //   11: istore 4
    //   13: aconst_null
    //   14: astore 5
    //   16: iload_3
    //   17: ifne +19 -> 36
    //   20: aload_0
    //   21: getfield 29	com/truecaller/messaging/transport/mms/i:c	Ljava/io/File;
    //   24: astore_2
    //   25: aload_2
    //   26: invokevirtual 283	java/io/File:mkdirs	()Z
    //   29: istore_3
    //   30: iload_3
    //   31: ifne +5 -> 36
    //   34: aconst_null
    //   35: areturn
    //   36: new 22	java/io/File
    //   39: astore_2
    //   40: aload_0
    //   41: getfield 29	com/truecaller/messaging/transport/mms/i:c	Ljava/io/File;
    //   44: astore 6
    //   46: bipush 45
    //   48: istore 7
    //   50: iload 7
    //   52: invokestatic 289	org/c/a/a/a/i:b	(I)Ljava/lang/String;
    //   55: astore 8
    //   57: aload_2
    //   58: aload 6
    //   60: aload 8
    //   62: invokespecial 27	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   65: new 291	java/io/FileOutputStream
    //   68: astore 6
    //   70: aload 6
    //   72: aload_2
    //   73: invokespecial 294	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   76: aload 6
    //   78: aload_1
    //   79: invokevirtual 295	java/io/FileOutputStream:write	([B)V
    //   82: aload 6
    //   84: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   87: aload_2
    //   88: areturn
    //   89: astore_1
    //   90: goto +11 -> 101
    //   93: astore_1
    //   94: goto +39 -> 133
    //   97: astore_1
    //   98: aconst_null
    //   99: astore 6
    //   101: aload_1
    //   102: invokestatic 301	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   105: aload 6
    //   107: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   110: aload_2
    //   111: invokevirtual 280	java/io/File:exists	()Z
    //   114: istore 9
    //   116: iload 9
    //   118: ifeq +8 -> 126
    //   121: aload_2
    //   122: invokevirtual 304	java/io/File:delete	()Z
    //   125: pop
    //   126: aconst_null
    //   127: areturn
    //   128: astore_1
    //   129: aload 6
    //   131: astore 5
    //   133: aload 5
    //   135: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   138: aload_2
    //   139: invokevirtual 280	java/io/File:exists	()Z
    //   142: istore 4
    //   144: iload 4
    //   146: ifeq +8 -> 154
    //   149: aload_2
    //   150: invokevirtual 304	java/io/File:delete	()Z
    //   153: pop
    //   154: aload_1
    //   155: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	156	0	this	i
    //   0	156	1	paramArrayOfByte	byte[]
    //   4	146	2	localFile	File
    //   9	22	3	bool1	boolean
    //   11	134	4	bool2	boolean
    //   14	120	5	localObject1	Object
    //   44	86	6	localObject2	Object
    //   48	3	7	i	int
    //   55	6	8	str	String
    //   114	3	9	bool3	boolean
    // Exception table:
    //   from	to	target	type
    //   78	82	89	java/io/IOException
    //   65	68	93	finally
    //   72	76	93	finally
    //   65	68	97	java/io/IOException
    //   72	76	97	java/io/IOException
    //   78	82	128	finally
    //   101	105	128	finally
  }
  
  /* Error */
  private List a(j paramj)
  {
    // Byte code:
    //   0: new 306	java/util/ArrayList
    //   3: astore_2
    //   4: aload_2
    //   5: invokespecial 307	java/util/ArrayList:<init>	()V
    //   8: aload_0
    //   9: getfield 29	com/truecaller/messaging/transport/mms/i:c	Ljava/io/File;
    //   12: astore_3
    //   13: aload_3
    //   14: invokevirtual 280	java/io/File:exists	()Z
    //   17: istore 4
    //   19: iload 4
    //   21: ifne +21 -> 42
    //   24: aload_0
    //   25: getfield 29	com/truecaller/messaging/transport/mms/i:c	Ljava/io/File;
    //   28: astore_3
    //   29: aload_3
    //   30: invokevirtual 283	java/io/File:mkdirs	()Z
    //   33: istore 4
    //   35: iload 4
    //   37: ifne +5 -> 42
    //   40: aload_2
    //   41: areturn
    //   42: aload_1
    //   43: getfield 310	com/android/a/a/a/j:a	Ljava/util/Vector;
    //   46: astore_3
    //   47: aload_3
    //   48: invokevirtual 315	java/util/Vector:size	()I
    //   51: istore 4
    //   53: iconst_0
    //   54: istore 5
    //   56: iload 5
    //   58: iload 4
    //   60: if_icmpge +247 -> 307
    //   63: aload_1
    //   64: iload 5
    //   66: invokevirtual 318	com/android/a/a/a/j:a	(I)Lcom/android/a/a/a/o;
    //   69: astore 6
    //   71: new 22	java/io/File
    //   74: astore 7
    //   76: aload_0
    //   77: getfield 29	com/truecaller/messaging/transport/mms/i:c	Ljava/io/File;
    //   80: astore 8
    //   82: bipush 45
    //   84: istore 9
    //   86: iload 9
    //   88: invokestatic 289	org/c/a/a/a/i:b	(I)Ljava/lang/String;
    //   91: astore 10
    //   93: aload 7
    //   95: aload 8
    //   97: aload 10
    //   99: invokespecial 27	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   102: iconst_0
    //   103: istore 11
    //   105: aconst_null
    //   106: astore 8
    //   108: new 291	java/io/FileOutputStream
    //   111: astore 10
    //   113: aload 10
    //   115: aload 7
    //   117: invokespecial 294	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   120: aload 6
    //   122: getfield 321	com/android/a/a/a/o:f	[B
    //   125: astore 8
    //   127: aload 8
    //   129: ifnull +24 -> 153
    //   132: aload 10
    //   134: aload 8
    //   136: invokevirtual 295	java/io/FileOutputStream:write	([B)V
    //   139: aload 8
    //   141: arraylength
    //   142: istore 12
    //   144: iconst_0
    //   145: istore 11
    //   147: aconst_null
    //   148: astore 8
    //   150: goto +9 -> 159
    //   153: iconst_1
    //   154: istore 11
    //   156: iconst_0
    //   157: istore 12
    //   159: aload 10
    //   161: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   164: iload 11
    //   166: ifeq +21 -> 187
    //   169: aload 7
    //   171: invokevirtual 280	java/io/File:exists	()Z
    //   174: istore 11
    //   176: iload 11
    //   178: ifeq +9 -> 187
    //   181: aload 7
    //   183: invokevirtual 304	java/io/File:delete	()Z
    //   186: pop
    //   187: new 270	com/truecaller/messaging/transport/mms/PduEntity
    //   190: astore 8
    //   192: aload 7
    //   194: invokestatic 327	android/net/Uri:fromFile	(Ljava/io/File;)Landroid/net/Uri;
    //   197: astore 7
    //   199: aload 8
    //   201: aload 6
    //   203: aload 7
    //   205: iload 12
    //   207: invokespecial 330	com/truecaller/messaging/transport/mms/PduEntity:<init>	(Lcom/android/a/a/a/o;Landroid/net/Uri;I)V
    //   210: aload_2
    //   211: aload 8
    //   213: invokeinterface 335 2 0
    //   218: pop
    //   219: goto +54 -> 273
    //   222: astore_1
    //   223: aload 10
    //   225: astore 8
    //   227: goto +55 -> 282
    //   230: astore 6
    //   232: aload 10
    //   234: astore 8
    //   236: goto +9 -> 245
    //   239: astore_1
    //   240: goto +42 -> 282
    //   243: astore 6
    //   245: aload 6
    //   247: invokestatic 301	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   250: aload 8
    //   252: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   255: aload 7
    //   257: invokevirtual 280	java/io/File:exists	()Z
    //   260: istore 13
    //   262: iload 13
    //   264: ifeq +9 -> 273
    //   267: aload 7
    //   269: invokevirtual 304	java/io/File:delete	()Z
    //   272: pop
    //   273: iload 5
    //   275: iconst_1
    //   276: iadd
    //   277: istore 5
    //   279: goto -223 -> 56
    //   282: aload 8
    //   284: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   287: aload 7
    //   289: invokevirtual 280	java/io/File:exists	()Z
    //   292: istore 14
    //   294: iload 14
    //   296: ifeq +9 -> 305
    //   299: aload 7
    //   301: invokevirtual 304	java/io/File:delete	()Z
    //   304: pop
    //   305: aload_1
    //   306: athrow
    //   307: aload_2
    //   308: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	309	0	this	i
    //   0	309	1	paramj	j
    //   3	305	2	localArrayList	ArrayList
    //   12	36	3	localObject1	Object
    //   17	19	4	bool1	boolean
    //   51	10	4	i	int
    //   54	224	5	j	int
    //   69	133	6	localo	o
    //   230	1	6	localIOException1	java.io.IOException
    //   243	3	6	localIOException2	java.io.IOException
    //   74	226	7	localObject2	Object
    //   80	203	8	localObject3	Object
    //   84	3	9	k	int
    //   91	142	10	localObject4	Object
    //   103	74	11	bool2	boolean
    //   142	64	12	m	int
    //   260	3	13	bool3	boolean
    //   292	3	14	bool4	boolean
    // Exception table:
    //   from	to	target	type
    //   120	125	222	finally
    //   134	139	222	finally
    //   139	142	222	finally
    //   120	125	230	java/io/IOException
    //   134	139	230	java/io/IOException
    //   139	142	230	java/io/IOException
    //   108	111	239	finally
    //   115	120	239	finally
    //   245	250	239	finally
    //   108	111	243	java/io/IOException
    //   115	120	243	java/io/IOException
  }
  
  private void a(Uri paramUri)
  {
    Object localObject = "file";
    String str = paramUri.getScheme();
    boolean bool1 = ((String)localObject).equals(str);
    if (!bool1) {
      return;
    }
    localObject = new java/io/File;
    paramUri = paramUri.getPath();
    ((File)localObject).<init>(paramUri);
    boolean bool2 = ((File)localObject).exists();
    if (!bool2) {
      return;
    }
    paramUri = ((File)localObject).getParentFile().getAbsolutePath();
    str = c.getAbsolutePath();
    bool2 = paramUri.equals(str);
    if (!bool2) {
      return;
    }
    ((File)localObject).delete();
  }
  
  /* Error */
  private byte[] a(PduEntity paramPduEntity)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_0
    //   3: getfield 18	com/truecaller/messaging/transport/mms/i:a	Landroid/content/ContentResolver;
    //   6: astore_3
    //   7: aload_1
    //   8: getfield 354	com/truecaller/messaging/transport/mms/PduEntity:b	Landroid/net/Uri;
    //   11: astore_1
    //   12: aload_3
    //   13: aload_1
    //   14: invokevirtual 134	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   17: astore_1
    //   18: aload_1
    //   19: ifnonnull +9 -> 28
    //   22: aload_1
    //   23: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   26: aconst_null
    //   27: areturn
    //   28: aload_1
    //   29: invokestatic 359	com/truecaller/utils/extensions/m:a	(Ljava/io/InputStream;)[B
    //   32: astore_2
    //   33: aload_1
    //   34: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   37: aload_2
    //   38: areturn
    //   39: astore_3
    //   40: goto +10 -> 50
    //   43: astore_1
    //   44: goto +34 -> 78
    //   47: astore_3
    //   48: aconst_null
    //   49: astore_1
    //   50: aconst_null
    //   51: astore 4
    //   53: iconst_0
    //   54: anewarray 66	java/lang/String
    //   57: astore 4
    //   59: aload_3
    //   60: aload 4
    //   62: invokestatic 365	com/truecaller/log/AssertionUtil$OnlyInDebug:shouldNeverHappen	(Ljava/lang/Throwable;[Ljava/lang/String;)V
    //   65: aload_1
    //   66: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   69: aconst_null
    //   70: areturn
    //   71: astore 5
    //   73: aload_1
    //   74: astore_2
    //   75: aload 5
    //   77: astore_1
    //   78: aload_2
    //   79: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   82: aload_1
    //   83: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	84	0	this	i
    //   0	84	1	paramPduEntity	PduEntity
    //   1	78	2	localObject1	Object
    //   6	7	3	localContentResolver	ContentResolver
    //   39	1	3	localIOException1	java.io.IOException
    //   47	13	3	localIOException2	java.io.IOException
    //   51	10	4	arrayOfString	String[]
    //   71	5	5	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   28	32	39	java/io/IOException
    //   2	6	43	finally
    //   7	11	43	finally
    //   13	17	43	finally
    //   2	6	47	java/io/IOException
    //   7	11	47	java/io/IOException
    //   13	17	47	java/io/IOException
    //   28	32	71	finally
    //   53	57	71	finally
    //   60	65	71	finally
  }
  
  /* Error */
  private int b(Uri paramUri)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_0
    //   3: getfield 18	com/truecaller/messaging/transport/mms/i:a	Landroid/content/ContentResolver;
    //   6: astore_3
    //   7: aload_3
    //   8: aload_1
    //   9: invokevirtual 134	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   12: astore_2
    //   13: aload_2
    //   14: ifnull +24 -> 38
    //   17: aload_2
    //   18: invokevirtual 145	java/io/InputStream:available	()I
    //   21: istore 4
    //   23: aload_2
    //   24: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   27: iload 4
    //   29: ireturn
    //   30: astore_1
    //   31: aload_2
    //   32: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   35: aload_1
    //   36: athrow
    //   37: pop
    //   38: aload_2
    //   39: invokestatic 139	com/truecaller/util/q:a	(Ljava/io/Closeable;)V
    //   42: iconst_m1
    //   43: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	44	0	this	i
    //   0	44	1	paramUri	Uri
    //   1	38	2	localInputStream	java.io.InputStream
    //   6	2	3	localContentResolver	ContentResolver
    //   21	7	4	i	int
    //   37	1	5	localIOException	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   2	6	30	finally
    //   8	12	30	finally
    //   17	21	30	finally
    //   2	6	37	java/io/IOException
    //   2	6	37	java/lang/SecurityException
    //   8	12	37	java/io/IOException
    //   8	12	37	java/lang/SecurityException
    //   17	21	37	java/io/IOException
    //   17	21	37	java/lang/SecurityException
  }
  
  public final f a(String paramString1, long paramLong, int paramInt1, int paramInt2, int paramInt3, List paramList, h.a parama, String paramString2)
  {
    com.android.a.a.a.u localu = new com/android/a/a/a/u;
    localu.<init>();
    paramString2 = b.a(paramString2);
    Object localObject = new java/util/ArrayList;
    int i = paramList.size();
    ((ArrayList)localObject).<init>(i);
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      String str = (String)paramList.next();
      boolean bool2 = k.d(str);
      if (!bool2)
      {
        bool2 = k.a(paramString2, str);
        if (!bool2)
        {
          e locale = new com/android/a/a/a/e;
          locale.<init>(str);
          ((List)localObject).add(locale);
        }
      }
    }
    int j = ((List)localObject).size();
    paramList = new e[j];
    paramList = (e[])((List)localObject).toArray(paramList);
    paramString2 = a;
    localObject = new String[0];
    AssertionUtil.AlwaysFatal.isNotNull(paramList, (String[])localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    Collections.addAll((Collection)localObject, paramList);
    paramList = a;
    int k = 151;
    paramList.put(k, localObject);
    localu.a(paramLong);
    if (paramString1 != null)
    {
      paramString1 = paramString1.getBytes();
      localu.a(paramString1);
    }
    paramString1 = null;
    try
    {
      localu.b(paramInt1);
      localu.c(paramInt2);
      localu.d(paramInt3);
      j localj = a(parama);
      int m = localj.a();
      if (m == 0) {
        return null;
      }
      b = localj;
      long l = localj.a();
      localu.b(l);
      return localu;
    }
    catch (com.android.a.a.a locala) {}
    return null;
  }
  
  public final Message a(f paramf, boolean paramBoolean, String paramString, long paramLong)
  {
    i locali = this;
    Object localObject1 = paramf;
    Object localObject2 = paramString;
    long l1 = paramLong;
    Message.a locala = new com/truecaller/messaging/data/types/Message$a;
    locala.<init>();
    m localm = a;
    Object localObject3 = new com/truecaller/messaging/transport/mms/MmsTransportInfo$a;
    ((MmsTransportInfo.a)localObject3).<init>();
    Object localObject4 = a;
    int i = 150;
    Object localObject5 = ((m)localObject4).c(i);
    if (localObject5 != null)
    {
      localObject6 = c.a(((e)localObject5).a());
      i = a;
      ((MmsTransportInfo.a)localObject3).a((String)localObject6, i);
    }
    i = 154;
    localObject5 = ((m)localObject4).c(i);
    if (localObject5 != null)
    {
      localObject6 = c.a(((e)localObject5).a());
      i = a;
      ((MmsTransportInfo.a)localObject3).b((String)localObject6, i);
    }
    i = 131;
    localObject5 = ((m)localObject4).b(i);
    if (localObject5 != null)
    {
      localObject5 = c.a((byte[])localObject5);
      j = ((String)localObject5).length();
      if (j != 0)
      {
        localObject5 = Uri.parse((String)localObject5);
        k = ((Uri)localObject5);
      }
    }
    i = 132;
    Object localObject6 = ((m)localObject4).b(i);
    if (localObject6 != null)
    {
      localObject6 = c.a((byte[])localObject6);
      l = ((String)localObject6);
    }
    int j = 152;
    localObject6 = ((m)localObject4).b(j);
    if (localObject6 != null)
    {
      localObject6 = c.a((byte[])localObject6);
      p = ((String)localObject6);
    }
    j = 147;
    localObject6 = ((m)localObject4).c(j);
    if (localObject6 != null)
    {
      localObject6 = ((e)localObject6).b();
      n = ((String)localObject6);
    }
    j = 139;
    localObject6 = ((m)localObject4).b(j);
    if (localObject6 != null)
    {
      localObject6 = c.a((byte[])localObject6);
      u = ((String)localObject6);
    }
    j = 138;
    localObject6 = ((m)localObject4).b(j);
    if (localObject6 != null)
    {
      localObject6 = c.a((byte[])localObject6);
      o = ((String)localObject6);
    }
    j = ((m)localObject4).a(149);
    if (j != 0) {
      c = j;
    }
    j = ((m)localObject4).a(153);
    if (j != 0) {
      s = j;
    }
    j = ((m)localObject4).a(146);
    if (j != 0) {
      t = j;
    }
    j = ((m)localObject4).a(140);
    if (j != 0) {
      w = j;
    }
    j = ((m)localObject4).a(143);
    if (j != 0) {
      r = j;
    }
    j = ((m)localObject4).a(141);
    if (j != 0) {
      f = j;
    }
    j = ((m)localObject4).a(186);
    if (j != 0) {
      m = j;
    }
    j = ((m)localObject4).a(134);
    if (j != 0) {
      y = j;
    }
    j = ((m)localObject4).a(144);
    if (j != 0) {
      A = j;
    }
    j = ((m)localObject4).a(155);
    if (j != 0) {
      B = j;
    }
    j = ((m)localObject4).a(145);
    int m = 1;
    if (j != 0) {
      C = m;
    }
    long l2 = ((m)localObject4).e(135);
    long l3 = -1;
    boolean bool1 = l2 < l3;
    if (bool1) {
      z = l2;
    }
    l2 = ((m)localObject4).e(136);
    bool1 = l2 < l3;
    if (bool1) {
      ((MmsTransportInfo.a)localObject3).e(l2);
    }
    l2 = ((m)localObject4).e(142);
    bool1 = l2 < l3;
    if (bool1)
    {
      k = (int)l2;
      x = k;
    }
    int k = 137;
    Object localObject7 = ((m)localObject4).c(k);
    if (localObject7 != null) {
      localObject7 = ((e)localObject7).b();
    } else {
      localObject7 = null;
    }
    if (localObject7 == null) {
      localObject7 = "Unknown sender";
    }
    ((MmsTransportInfo.a)localObject3).a(k, (String)localObject7);
    localObject7 = ao.a;
    int n = localObject7.length;
    i = 0;
    localObject5 = null;
    while (i < n)
    {
      m = localObject7[i];
      localObject6 = ((m)localObject4).d(m);
      Object localObject9;
      if (localObject6 != null)
      {
        int i1 = localObject6.length;
        int i2 = 0;
        while (i2 < i1)
        {
          Object localObject8 = localObject6[i2];
          localObject9 = localObject4;
          localObject4 = ((e)localObject8).b();
          boolean bool3 = k.d((CharSequence)localObject4);
          if (!bool3) {
            ((MmsTransportInfo.a)localObject3).a(m, (String)localObject4);
          }
          i2 += 1;
          localObject4 = localObject9;
        }
        localObject9 = localObject4;
      }
      else
      {
        localObject9 = localObject4;
      }
      i += 1;
      localObject4 = localObject9;
      m = 1;
      l3 = -1;
    }
    long l4 = l3;
    boolean bool2 = l1 < l3;
    if (bool2)
    {
      b = l1;
      ((MmsTransportInfo.a)localObject3).c(l1);
    }
    Object localObject10 = ((MmsTransportInfo.a)localObject3).a();
    Object localObject11 = null;
    g = false;
    h = false;
    i = false;
    k = 3;
    long l5 = localm.e(133);
    l4 = -1;
    boolean bool4 = l5 < l4;
    if (bool4)
    {
      l4 = 1000L;
      l5 *= l4;
      locala.c(l5);
    }
    else
    {
      localObject11 = b.ay_().f();
      e = ((b)localObject11);
    }
    int i3 = ((MmsTransportInfo)localObject10).h();
    f = i3;
    locala.a(1, (TransportInfo)localObject10);
    locala.a((String)localObject2);
    i3 = paramf.a();
    int i5 = 128;
    Participant localParticipant;
    int i4;
    if (i3 != i5)
    {
      i5 = 130;
      if (i3 != i5)
      {
        i5 = 132;
        if (i3 != i5)
        {
          n = 0;
          localParticipant = null;
          break label1310;
        }
      }
      else
      {
        boolean bool5 = paramBoolean;
        g = paramBoolean;
      }
      localObject11 = E;
      i5 = 0;
      localm = null;
      localObject3 = new String[0];
      AssertionUtil.AlwaysFatal.isNotNull(localObject11, (String[])localObject3);
      localObject10 = E;
      i4 = 137;
      localObject10 = (Set)((SparseArray)localObject10).get(i4);
      localObject11 = new String[0];
      AssertionUtil.AlwaysFatal.isNotNull(localObject10, (String[])localObject11);
      localObject10 = (String)((Set)localObject10).iterator().next();
      localObject11 = b;
      localParticipant = Participant.a((String)localObject10, (com.truecaller.common.h.u)localObject11, (String)localObject2);
    }
    else
    {
      i5 = 0;
      localm = null;
      localObject11 = E;
      localObject3 = new String[0];
      AssertionUtil.AlwaysFatal.isNotNull(localObject11, (String[])localObject3);
      localObject10 = E;
      i4 = 151;
      localObject10 = (Set)((SparseArray)localObject10).get(i4);
      localObject11 = new String[0];
      AssertionUtil.AlwaysFatal.isNotNull(localObject10, (String[])localObject11);
      localObject10 = (String)((Set)localObject10).iterator().next();
      localObject11 = b;
      localParticipant = Participant.a((String)localObject10, (com.truecaller.common.h.u)localObject11, (String)localObject2);
    }
    label1310:
    if (localParticipant == null)
    {
      localObject2 = new com/truecaller/messaging/data/types/Participant$a;
      int i6 = 1;
      ((Participant.a)localObject2).<init>(i6);
      d = "Unknown sender";
      localObject10 = "Unknown sender";
      e = ((String)localObject10);
      localParticipant = ((Participant.a)localObject2).a();
    }
    c = localParticipant;
    boolean bool6 = localObject1 instanceof g;
    if (bool6)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        localObject1 = locali.a((j)localObject1);
        locala.a((Collection)localObject1);
      }
    }
    return locala.b();
  }
  
  public final List a(Entity[] paramArrayOfEntity, com.truecaller.multisim.a parama)
  {
    i locali = this;
    Object localObject1 = paramArrayOfEntity;
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
    localStringBuilder1.<init>();
    int i = parama.j();
    int j = 0;
    Object localObject2 = null;
    int k = 0;
    Object localObject3 = null;
    int n = 0;
    Object localObject4 = null;
    int i3 = 0;
    int i4 = 0;
    int i5;
    int i7;
    Object localObject6;
    Object localObject7;
    Object localObject9;
    Object localObject12;
    Object localObject13;
    int i12;
    Object localObject14;
    int i1;
    int i13;
    int i14;
    boolean bool3;
    for (;;)
    {
      i5 = localObject1.length;
      i6 = 1;
      if (k >= i5) {
        break;
      }
      Object localObject5 = localObject1[k];
      i7 = ((Entity)localObject5).b();
      label1059:
      boolean bool1;
      if (i7 != 0)
      {
        n += 1;
      }
      else
      {
        i7 = localObject5 instanceof PduEntity;
        if (i7 != 0)
        {
          localObject6 = localObject5;
          localObject6 = (PduEntity)localObject5;
          i9 = n;
          localObject7 = localObject6;
        }
        else
        {
          i7 = ((Entity)localObject5).b();
          localObject7 = new String[0];
          AssertionUtil.AlwaysFatal.isFalse(i7, (String[])localObject7);
          i7 = ((Entity)localObject5).a();
          Object localObject8;
          Object localObject10;
          int i11;
          Object localObject11;
          if (i7 != 0)
          {
            localObject6 = localObject5;
            localObject6 = (TextEntity)localObject5;
            localObject7 = a.getBytes();
            File localFile = locali.a((byte[])localObject7);
            if (localFile != null)
            {
              localObject8 = Locale.US;
              localObject1 = new Object[i6];
              localObject9 = Integer.valueOf(k);
              localObject1[0] = localObject9;
              localObject1 = String.format((Locale)localObject8, "text.%06d", (Object[])localObject1);
              localObject8 = new Object[i6];
              localStringBuilder2 = new java/lang/StringBuilder;
              localStringBuilder2.<init>();
              localStringBuilder2.append((String)localObject1);
              i9 = n;
              localStringBuilder2.append(".txt");
              localObject4 = localStringBuilder2.toString();
              localObject8[0] = localObject4;
              localObject4 = String.format("<par dur=\"5000ms\"><text src=\"%s\" region=\"Text\" /></par>", (Object[])localObject8);
              localStringBuilder1.append((String)localObject4);
              localObject4 = new com/truecaller/messaging/transport/mms/PduEntity;
              localObject6 = j;
              localObject10 = Uri.fromFile(localFile);
              int i10 = localObject7.length;
              i11 = 106;
              localObject2 = new java/lang/StringBuilder;
              ((StringBuilder)localObject2).<init>();
              ((StringBuilder)localObject2).append((String)localObject1);
              localObject8 = ".txt";
              ((StringBuilder)localObject2).append((String)localObject8);
              localObject11 = ((StringBuilder)localObject2).toString();
              localObject12 = localObject4;
              localObject13 = localObject6;
              i12 = i10;
              localObject14 = localObject1;
              ((PduEntity)localObject4).<init>((String)localObject6, (Uri)localObject10, i10, i11, (String)localObject1, (String)localObject11);
              localObject7 = localObject4;
              break label1059;
            }
            i9 = n;
          }
          else
          {
            i9 = n;
            i1 = ((Entity)localObject5).d();
            if (i1 != 0)
            {
              localObject4 = localObject5;
              localObject4 = (BinaryEntity)localObject5;
              localObject6 = b;
              i12 = locali.b((Uri)localObject6);
              if (i12 >= 0)
              {
                localObject6 = Locale.US;
                localObject1 = new Object[i6];
                localObject2 = Integer.valueOf(k);
                i13 = 0;
                localObject8 = null;
                localObject1[0] = localObject2;
                localObject6 = String.format((Locale)localObject6, "vcard.%06d", (Object[])localObject1);
                localObject7 = new java/lang/StringBuilder;
                ((StringBuilder)localObject7).<init>();
                ((StringBuilder)localObject7).append((String)localObject6);
                ((StringBuilder)localObject7).append(".vcf");
                localObject11 = ((StringBuilder)localObject7).toString();
                localObject1 = new Object[i6];
                localObject1[0] = localObject11;
                localObject7 = String.format("<par dur=\"5000ms\"><text src=\"%s\" region=\"Text\" /></par>", (Object[])localObject1);
                localStringBuilder1.append((String)localObject7);
                localObject7 = new com/truecaller/messaging/transport/mms/PduEntity;
                localObject1 = j;
                localObject4 = b;
                i11 = 106;
                localObject12 = localObject7;
                localObject13 = localObject1;
                localObject10 = localObject4;
                localObject14 = localObject6;
                ((PduEntity)localObject7).<init>((String)localObject1, (Uri)localObject4, i12, i11, (String)localObject6, (String)localObject11);
                break label1059;
              }
            }
            else
            {
              i1 = ((Entity)localObject5).c();
              if (i1 != 0)
              {
                localObject4 = localObject5;
                localObject4 = (BinaryEntity)localObject5;
                localObject6 = b;
                i12 = locali.b((Uri)localObject6);
                if (i12 >= 0)
                {
                  localObject6 = Locale.US;
                  localObject1 = new Object[i6];
                  localObject2 = Integer.valueOf(k);
                  localObject8 = null;
                  localObject1[0] = localObject2;
                  localObject6 = String.format((Locale)localObject6, "video.%06d", (Object[])localObject1);
                  localObject7 = j;
                  j = -1;
                  i14 = ((String)localObject7).hashCode();
                  i13 = -1664118616;
                  if (i14 != i13)
                  {
                    i13 = 1331848029;
                    if (i14 == i13)
                    {
                      localObject1 = "video/mp4";
                      bool3 = ((String)localObject7).equals(localObject1);
                      if (bool3) {
                        j = 1;
                      }
                    }
                  }
                  else
                  {
                    localObject1 = "video/3gpp";
                    bool3 = ((String)localObject7).equals(localObject1);
                    if (bool3)
                    {
                      j = 0;
                      localObject2 = null;
                    }
                  }
                  switch (j)
                  {
                  default: 
                    localObject4 = "Unknown video type. Skip.";
                    new String[1][0] = localObject4;
                    break;
                  case 1: 
                    localObject7 = new java/lang/StringBuilder;
                    ((StringBuilder)localObject7).<init>();
                    ((StringBuilder)localObject7).append((String)localObject6);
                    localObject1 = ".mp4";
                    ((StringBuilder)localObject7).append((String)localObject1);
                    localObject7 = ((StringBuilder)localObject7).toString();
                    localObject11 = localObject7;
                    break;
                  case 0: 
                    localObject7 = new java/lang/StringBuilder;
                    ((StringBuilder)localObject7).<init>();
                    ((StringBuilder)localObject7).append((String)localObject6);
                    localObject1 = ".3gp";
                    ((StringBuilder)localObject7).append((String)localObject1);
                    localObject7 = ((StringBuilder)localObject7).toString();
                    localObject11 = localObject7;
                  }
                  localObject7 = Locale.US;
                  j = 2;
                  localObject2 = new Object[j];
                  localObject2[0] = localObject11;
                  i13 = 5000;
                  localObject8 = Integer.valueOf(i13);
                  localObject2[i6] = localObject8;
                  localObject7 = String.format((Locale)localObject7, "<par dur=\"%2$dms\"><video src=\"%1$s\" dur=\"%2$dms\" /></par>", (Object[])localObject2);
                  localStringBuilder1.append((String)localObject7);
                  localObject7 = new com/truecaller/messaging/transport/mms/PduEntity;
                  localObject1 = j;
                  localObject4 = b;
                  i11 = -1;
                  localObject12 = localObject7;
                  localObject13 = localObject1;
                  localObject10 = localObject4;
                  localObject14 = localObject6;
                  ((PduEntity)localObject7).<init>((String)localObject1, (Uri)localObject4, i12, i11, (String)localObject6, (String)localObject11);
                  break label1059;
                }
              }
            }
          }
          bool3 = false;
          localObject7 = null;
        }
        if (localObject7 != null)
        {
          i1 = ((Entity)localObject5).a() | i4;
          localArrayList.add(localObject7);
          long l1 = i3;
          long l2 = d;
          l1 += l2;
          i5 = (int)l1;
          if (i5 > i)
          {
            localObject15 = localArrayList.iterator();
            for (;;)
            {
              bool1 = ((Iterator)localObject15).hasNext();
              if (!bool1) {
                break;
              }
              localObject3 = nextb;
              locali.a((Uri)localObject3);
            }
            return null;
          }
          i4 = i1;
          i3 = i5;
          i1 = i9;
        }
        else
        {
          i1 = i9;
        }
      }
      int m;
      bool1 += true;
      localObject1 = paramArrayOfEntity;
      j = 0;
      localObject2 = null;
    }
    int i9 = i1;
    boolean bool2;
    if (i1 > 0)
    {
      i14 = (i - i3) / i1;
      i = 2048;
      if (i14 < i)
      {
        localObject15 = localArrayList.iterator();
        for (;;)
        {
          bool2 = ((Iterator)localObject15).hasNext();
          if (!bool2) {
            break;
          }
          localObject3 = nextb;
          locali.a((Uri)localObject3);
        }
        return null;
      }
      j = 0;
      localObject2 = null;
      i13 = parama.h();
      int i15 = parama.i();
      i7 = 0;
      localObject6 = null;
      for (;;)
      {
        localObject7 = paramArrayOfEntity;
        i = paramArrayOfEntity.length;
        if (i7 >= i) {
          break;
        }
        localObject15 = paramArrayOfEntity[i7];
        bool2 = ((Entity)localObject15).b();
        int i16;
        if (bool2)
        {
          localObject3 = localObject15;
          localObject3 = (ImageEntity)localObject15;
          localObject15 = this;
          i1 = i13;
          i3 = i15;
          i5 = i14;
          i16 = i7;
          localObject6 = localStringBuilder1;
          bool3 = i7;
          localObject15 = a((ImageEntity)localObject3, i13, i15, i14, localStringBuilder1, i7);
          if (localObject15 != null) {
            localArrayList.add(localObject15);
          }
        }
        else
        {
          i16 = i7;
        }
        int i8 = i16 + 1;
      }
    }
    j = 0;
    localObject2 = null;
    int i6 = 0;
    StringBuilder localStringBuilder2 = null;
    if (i6 != 0)
    {
      if (i4 != 0)
      {
        localObject15 = "<smil><head><layout><root-layout/><region id=\"Image\" fit=\"meet\" top=\"0\" left=\"0\" height=\"80%%\" width=\"100%%\"/><region id=\"Text\" top=\"80%%\" left=\"0\" height=\"20%%\" width=\"100%%\"/></layout></head><body>";
        bool2 = false;
        localObject3 = null;
        localStringBuilder1.insert(0, (String)localObject15);
      }
      else
      {
        bool2 = false;
        localObject3 = null;
        localObject15 = "<smil><head><layout><root-layout/><region id=\"Image\" fit=\"meet\" top=\"0\" left=\"0\" height=\"100%%\" width=\"100%%\"/></layout></head><body>";
        localStringBuilder1.insert(0, (String)localObject15);
      }
    }
    else
    {
      localObject15 = "<smil><head><layout><root-layout/><region id=\"Text\" top=\"0\" left=\"0\" height=\"100%%\" width=\"100%%\"/></layout></head><body>";
      localStringBuilder1.append((String)localObject15);
    }
    localStringBuilder1.append("</body></smil>");
    Object localObject15 = localStringBuilder1.toString().getBytes();
    localObject3 = locali.a((byte[])localObject15);
    if (localObject3 == null)
    {
      int i2 = 0;
      localObject4 = null;
    }
    else
    {
      localObject4 = new com/truecaller/messaging/transport/mms/PduEntity;
      localObject12 = "application/smil";
      localObject13 = Uri.fromFile((File)localObject3);
      i = localObject15.length;
      i12 = 106;
      String str = "smil";
      localObject14 = "smil.xml";
      localObject9 = localObject4;
      ((PduEntity)localObject4).<init>((String)localObject12, (Uri)localObject13, i, i12, str, (String)localObject14);
    }
    if (localObject4 != null)
    {
      i = 0;
      localObject15 = null;
      localArrayList.add(0, localObject4);
    }
    return localArrayList;
  }
  
  public final void a(PduEntity paramPduEntity, ContentValues paramContentValues)
  {
    int i = a;
    Object localObject1 = Integer.valueOf(i);
    paramContentValues.put("chset", (Integer)localObject1);
    localObject1 = j;
    paramContentValues.put("ct", (String)localObject1);
    Object localObject2 = "text/plain";
    localObject1 = j;
    boolean bool2 = ((String)localObject2).equals(localObject1);
    if (bool2)
    {
      localObject2 = a(paramPduEntity);
      if (localObject2 != null)
      {
        localObject1 = new com/android/a/a/a/e;
        int k = a;
        ((e)localObject1).<init>(k, (byte[])localObject2);
        localObject2 = ((e)localObject1).b();
      }
      else
      {
        localObject2 = "";
      }
      localObject1 = "BEGIN:VCARD";
      boolean bool1 = ((String)localObject2).startsWith((String)localObject1);
      if (bool1)
      {
        localObject2 = "ct";
        localObject1 = j;
        paramContentValues.put((String)localObject2, (String)localObject1);
      }
      else
      {
        localObject1 = "text";
        paramContentValues.put((String)localObject1, (String)localObject2);
      }
    }
    localObject2 = "application/smil";
    localObject1 = j;
    bool2 = ((String)localObject2).equals(localObject1);
    if (bool2)
    {
      int j = -1;
      localObject1 = Integer.valueOf(j);
      paramContentValues.put("chset", (Integer)localObject1);
      localObject2 = a(paramPduEntity);
      if (localObject2 != null)
      {
        localObject1 = "text";
        e locale = new com/android/a/a/a/e;
        int m = a;
        locale.<init>(m, (byte[])localObject2);
        localObject2 = locale.b();
        paramContentValues.put((String)localObject1, (String)localObject2);
      }
      else
      {
        localObject2 = "text";
        localObject1 = "";
        paramContentValues.put((String)localObject2, (String)localObject1);
      }
    }
    localObject2 = j;
    bool2 = Entity.f((String)localObject2);
    if (bool2)
    {
      localObject2 = "ct";
      localObject1 = "text/x-vcard";
      paramContentValues.put((String)localObject2, (String)localObject1);
    }
    localObject2 = k;
    if (localObject2 != null)
    {
      localObject2 = "fn";
      localObject1 = k;
      paramContentValues.put((String)localObject2, (String)localObject1);
    }
    localObject2 = l;
    if (localObject2 != null)
    {
      localObject2 = "name";
      localObject1 = l;
      paramContentValues.put((String)localObject2, (String)localObject1);
    }
    localObject2 = m;
    if (localObject2 != null)
    {
      localObject2 = "cd";
      localObject1 = m;
      paramContentValues.put((String)localObject2, (String)localObject1);
    }
    localObject2 = n;
    if (localObject2 != null)
    {
      localObject2 = "cid";
      localObject1 = n;
      paramContentValues.put((String)localObject2, (String)localObject1);
    }
    localObject2 = o;
    if (localObject2 != null)
    {
      localObject2 = "cl";
      paramPduEntity = o;
      paramContentValues.put((String)localObject2, paramPduEntity);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */