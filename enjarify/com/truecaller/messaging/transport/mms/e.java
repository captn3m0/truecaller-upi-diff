package com.truecaller.messaging.transport.mms;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.android.a.a.a.f;
import com.android.a.a.a.i;
import com.android.a.a.a.u;
import com.truecaller.log.AssertionUtil;
import com.truecaller.multisim.SimInfo;
import com.truecaller.util.TempContentProvider;

abstract class e
  implements ai
{
  final Context a;
  final SimInfo b;
  final com.truecaller.multisim.a c;
  
  e(Context paramContext, SimInfo paramSimInfo, com.truecaller.multisim.a parama)
  {
    a = paramContext;
    b = paramSimInfo;
    c = parama;
  }
  
  /* Error */
  private Uri a(f paramf)
  {
    // Byte code:
    //   0: new 24	com/android/a/a/a/k
    //   3: astore_2
    //   4: aload_0
    //   5: getfield 18	com/truecaller/messaging/transport/mms/e:a	Landroid/content/Context;
    //   8: astore_3
    //   9: aload_2
    //   10: aload_3
    //   11: aload_1
    //   12: invokespecial 27	com/android/a/a/a/k:<init>	(Landroid/content/Context;Lcom/android/a/a/a/f;)V
    //   15: aload_2
    //   16: invokevirtual 30	com/android/a/a/a/k:a	()[B
    //   19: astore_1
    //   20: aconst_null
    //   21: astore_2
    //   22: aload_1
    //   23: ifnull +176 -> 199
    //   26: aload_1
    //   27: arraylength
    //   28: istore 4
    //   30: iload 4
    //   32: ifne +6 -> 38
    //   35: goto +164 -> 199
    //   38: aload_0
    //   39: getfield 18	com/truecaller/messaging/transport/mms/e:a	Landroid/content/Context;
    //   42: aconst_null
    //   43: aconst_null
    //   44: invokestatic 35	com/truecaller/util/TempContentProvider:a	(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Integer;)Landroid/net/Uri;
    //   47: astore_3
    //   48: aload_3
    //   49: ifnonnull +5 -> 54
    //   52: aconst_null
    //   53: areturn
    //   54: aload_0
    //   55: getfield 18	com/truecaller/messaging/transport/mms/e:a	Landroid/content/Context;
    //   58: astore 5
    //   60: aload 5
    //   62: invokevirtual 41	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   65: astore 5
    //   67: ldc 43
    //   69: astore 6
    //   71: aload 5
    //   73: aload_3
    //   74: aload 6
    //   76: invokevirtual 49	android/content/ContentResolver:openOutputStream	(Landroid/net/Uri;Ljava/lang/String;)Ljava/io/OutputStream;
    //   79: astore 5
    //   81: aload 5
    //   83: ifnonnull +24 -> 107
    //   86: aload 5
    //   88: invokestatic 54	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   91: aload_0
    //   92: getfield 18	com/truecaller/messaging/transport/mms/e:a	Landroid/content/Context;
    //   95: invokevirtual 41	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   98: aload_3
    //   99: aconst_null
    //   100: aconst_null
    //   101: invokevirtual 58	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   104: pop
    //   105: aconst_null
    //   106: areturn
    //   107: aload 5
    //   109: aload_1
    //   110: invokevirtual 64	java/io/OutputStream:write	([B)V
    //   113: aload 5
    //   115: invokestatic 54	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   118: aload_3
    //   119: areturn
    //   120: astore_1
    //   121: goto +7 -> 128
    //   124: astore_1
    //   125: aconst_null
    //   126: astore 5
    //   128: aload 5
    //   130: invokestatic 54	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   133: aload_0
    //   134: getfield 18	com/truecaller/messaging/transport/mms/e:a	Landroid/content/Context;
    //   137: invokevirtual 41	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   140: aload_3
    //   141: aconst_null
    //   142: aconst_null
    //   143: invokevirtual 58	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   146: pop
    //   147: aload_1
    //   148: athrow
    //   149: pop
    //   150: aconst_null
    //   151: astore 5
    //   153: aload 5
    //   155: invokestatic 54	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   158: aload_0
    //   159: getfield 18	com/truecaller/messaging/transport/mms/e:a	Landroid/content/Context;
    //   162: invokevirtual 41	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   165: aload_3
    //   166: aconst_null
    //   167: aconst_null
    //   168: invokevirtual 58	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   171: pop
    //   172: aconst_null
    //   173: areturn
    //   174: pop
    //   175: aconst_null
    //   176: astore 5
    //   178: aload 5
    //   180: invokestatic 54	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   183: aload_0
    //   184: getfield 18	com/truecaller/messaging/transport/mms/e:a	Landroid/content/Context;
    //   187: invokevirtual 41	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   190: aload_3
    //   191: aconst_null
    //   192: aconst_null
    //   193: invokevirtual 58	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   196: pop
    //   197: aconst_null
    //   198: areturn
    //   199: aconst_null
    //   200: areturn
    //   201: pop
    //   202: goto -49 -> 153
    //   205: pop
    //   206: goto -28 -> 178
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	209	0	this	e
    //   0	209	1	paramf	f
    //   3	19	2	localk	com.android.a.a.a.k
    //   8	183	3	localObject1	Object
    //   28	3	4	i	int
    //   58	121	5	localObject2	Object
    //   69	6	6	str	String
    //   149	1	7	localIOException1	java.io.IOException
    //   174	1	8	localFileNotFoundException1	java.io.FileNotFoundException
    //   201	1	9	localIOException2	java.io.IOException
    //   205	1	10	localFileNotFoundException2	java.io.FileNotFoundException
    // Exception table:
    //   from	to	target	type
    //   109	113	120	finally
    //   54	58	124	finally
    //   60	65	124	finally
    //   74	79	124	finally
    //   54	58	149	java/io/IOException
    //   60	65	149	java/io/IOException
    //   74	79	149	java/io/IOException
    //   54	58	174	java/io/FileNotFoundException
    //   60	65	174	java/io/FileNotFoundException
    //   74	79	174	java/io/FileNotFoundException
    //   109	113	201	java/io/IOException
    //   109	113	205	java/io/FileNotFoundException
  }
  
  private void a(long paramLong1, long paramLong2, f paramf, Uri paramUri1, Uri paramUri2)
  {
    paramf = a(paramf);
    Context localContext1 = a;
    Object localObject1 = b;
    if (localObject1 != null) {
      localObject1 = b;
    } else {
      localObject1 = "-1";
    }
    Object localObject2 = localObject1;
    localObject1 = paramUri2;
    Object localObject3 = MmsStatusReceiver.a(localContext1, paramUri2, paramf, paramLong1, paramLong2, (String)localObject2);
    if (paramf == null)
    {
      new String[1][0] = "Can't send MMS, error occurred during pdu creation";
      a.sendBroadcast((Intent)localObject3);
      return;
    }
    Context localContext2 = a;
    int i = 134217728;
    localObject3 = PendingIntent.getBroadcast(localContext2, 0, (Intent)localObject3, i);
    boolean bool = a(paramf, paramUri1, (PendingIntent)localObject3);
    if (!bool)
    {
      int j = 5;
      try
      {
        ((PendingIntent)localObject3).send(j);
        return;
      }
      catch (PendingIntent.CanceledException localCanceledException) {}
    }
  }
  
  public final void a(long paramLong1, long paramLong2, u paramu, Uri paramUri)
  {
    a(paramLong1, paramLong2, paramu, null, paramUri);
  }
  
  public final void a(long paramLong, byte[] paramArrayOfByte, Uri paramUri, boolean paramBoolean)
  {
    Uri localUri = TempContentProvider.a(a, null, null);
    Context localContext1 = a;
    Object localObject1 = b;
    if (localObject1 != null) {
      localObject1 = b;
    } else {
      localObject1 = "-1";
    }
    Object localObject2 = MmsStatusReceiver.a(localContext1, paramUri, paramArrayOfByte, localUri, paramLong, paramBoolean, (String)localObject1);
    if (localUri == null)
    {
      new String[1][0] = "Can't download MMS, error occurred during pdu creation";
      a.sendBroadcast((Intent)localObject2);
      return;
    }
    Context localContext2 = a;
    paramArrayOfByte = null;
    paramBoolean = 134217728;
    localObject2 = PendingIntent.getBroadcast(localContext2, 0, (Intent)localObject2, paramBoolean);
    boolean bool = b(localUri, paramUri, (PendingIntent)localObject2);
    if (!bool)
    {
      int i = 5;
      try
      {
        ((PendingIntent)localObject2).send(i);
        return;
      }
      catch (PendingIntent.CanceledException localCanceledException) {}
    }
  }
  
  public final void a(byte[] paramArrayOfByte, Uri paramUri)
  {
    try
    {
      com.android.a.a.a.a locala = new com/android/a/a/a/a;
      locala.<init>(paramArrayOfByte);
      paramArrayOfByte = null;
      Object localObject = c;
      boolean bool = ((com.truecaller.multisim.a)localObject).d();
      Uri localUri;
      if (bool) {
        localUri = paramUri;
      } else {
        localUri = null;
      }
      long l1 = -1;
      long l2 = -1;
      localObject = this;
      a(l1, l2, locala, localUri, paramUri);
      return;
    }
    catch (com.android.a.a.a locala1)
    {
      AssertionUtil.reportThrowableButNeverCrash(locala1;
    }
  }
  
  public final void a(byte[] paramArrayOfByte, Uri paramUri, int paramInt)
  {
    try
    {
      i locali = new com/android/a/a/a/i;
      locali.<init>(paramArrayOfByte, paramInt);
      paramArrayOfByte = null;
      com.truecaller.multisim.a locala = c;
      paramInt = locala.d();
      Uri localUri;
      if (paramInt != 0) {
        localUri = paramUri;
      } else {
        localUri = null;
      }
      long l1 = -1;
      long l2 = -1;
      a(l1, l2, locali, localUri, paramUri);
      return;
    }
    catch (com.android.a.a.a locala1)
    {
      AssertionUtil.reportThrowableButNeverCrash(locala1;
    }
  }
  
  abstract boolean a(Uri paramUri1, Uri paramUri2, PendingIntent paramPendingIntent);
  
  abstract boolean b(Uri paramUri1, Uri paramUri2, PendingIntent paramPendingIntent);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */