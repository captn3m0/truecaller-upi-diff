package com.truecaller.messaging.transport.mms;

import okhttp3.u;
import org.c.a.a.a.k;

final class a
  implements c.a
{
  private final u a;
  private final String b;
  private final int c;
  
  private a(u paramu, String paramString, int paramInt)
  {
    a = paramu;
    b = paramString;
    c = paramInt;
  }
  
  static a a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    paramString1 = k.j(paramString1);
    String str1 = "mms";
    boolean bool1 = k.b(paramString1);
    int j = 1;
    if (!bool1)
    {
      paramString1 = k.a(paramString1, ',');
      int i = paramString1.length;
      int k = 0;
      while (k < i)
      {
        Object localObject = paramString1[k];
        boolean bool2 = ((String)localObject).equals(str1);
        if (bool2) {
          break label100;
        }
        String str2 = "*";
        boolean bool3 = ((String)localObject).equals(str2);
        if (bool3) {
          break label100;
        }
        k += 1;
      }
      j = 0;
    }
    label100:
    int m = 0;
    paramString1 = null;
    if (j == 0) {
      return null;
    }
    paramString2 = k.j(paramString2);
    int n = paramString2.length();
    if (n == 0) {
      return null;
    }
    paramString2 = u.e(a(paramString2));
    if (paramString2 == null) {
      return null;
    }
    m = 80;
    paramString3 = k.j(paramString3);
    n = paramString3.length();
    if (n > 0)
    {
      paramString3 = a(paramString3);
      paramString4 = k.j(paramString4);
      n = paramString4.length();
      if (n <= 0) {}
    }
    try
    {
      m = Integer.parseInt(paramString4);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      for (;;) {}
    }
    paramString4 = new com/truecaller/messaging/transport/mms/a;
    paramString4.<init>(paramString2, paramString3, m);
    return paramString4;
  }
  
  private static String a(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    char c1 = '.';
    String[] arrayOfString = k.a(paramString, c1);
    int i = arrayOfString.length;
    int j = 4;
    if (i != j) {
      return paramString;
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(16);
    int k = 0;
    while (k < j) {
      try
      {
        String str = arrayOfString[k];
        int m = str.length();
        int n = 3;
        if (m > n) {
          return paramString;
        }
        str = arrayOfString[k];
        m = Integer.parseInt(str);
        localStringBuilder.append(m);
        if (k < n) {
          localStringBuilder.append(c1);
        }
        k += 1;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        return paramString;
      }
    }
    return localStringBuilder.toString();
  }
  
  public final u a()
  {
    return a;
  }
  
  public final String b()
  {
    return b;
  }
  
  public final int c()
  {
    return c;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */