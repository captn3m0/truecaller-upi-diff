package com.truecaller.messaging.transport.mms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.SystemClock;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.multisim.h;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.c.a.a.a.k;

abstract class aa
  implements z
{
  private static final long c = TimeUnit.MINUTES.toMillis(3);
  private static final long d = TimeUnit.SECONDS.toMillis(15);
  private static final long e = TimeUnit.SECONDS.toMillis(5);
  private static final long f = TimeUnit.SECONDS.toMillis(30);
  protected final ConnectivityManager a;
  protected final h b;
  private final Lock g;
  private final Condition h;
  private final Context i;
  private int j;
  private int k;
  private String l;
  private final IntentFilter m;
  private boolean n;
  private final Runnable o;
  private final Handler p;
  private final BroadcastReceiver q;
  
  aa(Context paramContext, Handler paramHandler, ConnectivityManager paramConnectivityManager, h paramh)
  {
    Object localObject = new java/util/concurrent/locks/ReentrantLock;
    ((ReentrantLock)localObject).<init>();
    g = ((Lock)localObject);
    localObject = g.newCondition();
    h = ((Condition)localObject);
    j = 0;
    k = 0;
    l = null;
    n = false;
    localObject = new com/truecaller/messaging/transport/mms/-$$Lambda$aa$8PqMm4nENg8h10OlF2E3XoTaMHY;
    ((-..Lambda.aa.8PqMm4nENg8h10OlF2E3XoTaMHY)localObject).<init>(this);
    o = ((Runnable)localObject);
    localObject = new com/truecaller/messaging/transport/mms/aa$1;
    ((aa.1)localObject).<init>(this);
    q = ((BroadcastReceiver)localObject);
    i = paramContext;
    a = paramConnectivityManager;
    b = paramh;
    p = paramHandler;
    paramContext = new android/content/IntentFilter;
    paramContext.<init>();
    m = paramContext;
    m.addAction("android.net.conn.CONNECTIVITY_CHANGE");
  }
  
  private int a(boolean paramBoolean)
  {
    String str = l;
    String[] arrayOfString = new String[0];
    AssertionUtil.AlwaysFatal.isNotNull(str, arrayOfString);
    str = l;
    paramBoolean = a(str, paramBoolean);
    if (!paramBoolean)
    {
      b();
      return 0;
    }
    boolean bool = true;
    if (paramBoolean != bool)
    {
      c();
      return a(paramBoolean);
    }
    return bool;
  }
  
  protected static int b(InetAddress paramInetAddress)
  {
    paramInetAddress = paramInetAddress.getAddress();
    int i1 = (paramInetAddress[3] & 0xFF) << 24;
    int i2 = (paramInetAddress[2] & 0xFF) << 16;
    i1 |= i2;
    i2 = (paramInetAddress[1] & 0xFF) << 8;
    i1 |= i2;
    return paramInetAddress[0] & 0xFF | i1;
  }
  
  private void b()
  {
    boolean bool = n;
    if (!bool)
    {
      Handler localHandler = p;
      Runnable localRunnable = o;
      long l1 = f;
      localHandler.postDelayed(localRunnable, l1);
      bool = true;
      n = bool;
    }
  }
  
  private void c()
  {
    boolean bool = n;
    if (bool)
    {
      Handler localHandler = p;
      Runnable localRunnable = o;
      localHandler.removeCallbacks(localRunnable);
      bool = false;
      localHandler = null;
      n = false;
    }
  }
  
  protected abstract int a(int paramInt);
  
  protected abstract int a(String paramString, boolean paramBoolean);
  
  public final List a()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    NetworkInfo[] arrayOfNetworkInfo = a.getAllNetworkInfo();
    if (arrayOfNetworkInfo != null)
    {
      int i1 = arrayOfNetworkInfo.length;
      if (i1 != 0)
      {
        i1 = arrayOfNetworkInfo.length;
        int i2 = 0;
        while (i2 < i1)
        {
          Object localObject = arrayOfNetworkInfo[i2];
          int i3 = 2;
          int i4 = ((NetworkInfo)localObject).getType();
          if (i3 == i4)
          {
            localObject = ((NetworkInfo)localObject).getExtraInfo();
            localArrayList.add(localObject);
          }
          i2 += 1;
        }
        return localArrayList;
      }
    }
    return localArrayList;
  }
  
  public final boolean a(String paramString)
  {
    localObject1 = g;
    ((Lock)localObject1).lock();
    i1 = 1;
    for (;;)
    {
      try
      {
        int i2 = j + i1;
        j = i2;
        if (i2 == i1)
        {
          localObject2 = i;
          localBroadcastReceiver = q;
          IntentFilter localIntentFilter = m;
          ((Context)localObject2).registerReceiver(localBroadcastReceiver, localIntentFilter);
        }
        int i3 = k + i1;
        k = i3;
        bool3 = false;
        BroadcastReceiver localBroadcastReceiver = null;
        if (i3 == i1)
        {
          l = paramString;
        }
        else
        {
          localObject2 = l;
          bool1 = k.a(paramString, (CharSequence)localObject2);
          if (!bool1)
          {
            i6 = j - i1;
            j = i6;
            if (i6 == 0)
            {
              paramString = i;
              localObject1 = q;
              paramString.unregisterReceiver((BroadcastReceiver)localObject1);
            }
            g.unlock();
            return false;
          }
        }
        l1 = c;
        l2 = SystemClock.elapsedRealtime();
        boolean bool1 = false;
        localObject2 = null;
        localObject3 = b;
        int i7 = ((h)localObject3).g(paramString);
        int i8 = 2;
        if (i7 == i8)
        {
          i6 = j - i1;
          j = i6;
          if (i6 == 0)
          {
            paramString = i;
            localObject1 = q;
            paramString.unregisterReceiver((BroadcastReceiver)localObject1);
          }
          g.unlock();
          return false;
        }
        int i4 = a(bool1);
        switch (i4)
        {
        default: 
          i6 = j - i1;
          j = i6;
          if (i6 == 0)
          {
            paramString = i;
            localObject1 = q;
            paramString.unregisterReceiver((BroadcastReceiver)localObject1);
          }
          break;
        case 3: 
          i6 = j - i1;
          j = i6;
          if (i6 == 0)
          {
            paramString = i;
            localObject1 = q;
            paramString.unregisterReceiver((BroadcastReceiver)localObject1);
          }
          g.unlock();
          return false;
        case 2: 
          l3 = e;
          break;
        case 1: 
          l3 = d;
        }
      }
      finally
      {
        Object localObject2;
        boolean bool3;
        int i6;
        long l1;
        long l2;
        Object localObject3;
        long l3;
        boolean bool2;
        int i5 = j - i1;
        j = i5;
        if (i5 == 0)
        {
          localObject1 = i;
          localObject2 = q;
          ((Context)localObject1).unregisterReceiver((BroadcastReceiver)localObject2);
        }
        g.unlock();
      }
      try
      {
        localObject2 = h;
        l1 = Math.min(l1, l3);
        localObject3 = TimeUnit.MILLISECONDS;
        ((Condition)localObject2).await(l1, (TimeUnit)localObject3);
      }
      catch (InterruptedException localInterruptedException)
      {
        continue;
      }
      l1 = c;
      l3 = SystemClock.elapsedRealtime() - l2;
      l1 -= l3;
      l3 = 0L;
      bool2 = l1 < l3;
      if (!bool2)
      {
        i6 = a(i1);
        if (i6 == 0) {
          bool3 = true;
        }
        i6 = j - i1;
        j = i6;
        if (i6 == 0)
        {
          paramString = i;
          localObject1 = q;
          paramString.unregisterReceiver((BroadcastReceiver)localObject1);
        }
        g.unlock();
        return bool3;
      }
      bool2 = true;
    }
    i6 = j - i1;
    j = i6;
    if (i6 == 0)
    {
      paramString = i;
      localObject2 = q;
      paramString.unregisterReceiver((BroadcastReceiver)localObject2);
    }
    g.unlock();
    return i1;
    g.unlock();
    return false;
  }
  
  protected abstract boolean a(String paramString, InetAddress paramInetAddress);
  
  public final boolean a(InetAddress paramInetAddress)
  {
    Object localObject = g;
    ((Lock)localObject).lock();
    try
    {
      localObject = l;
      if (localObject != null)
      {
        int i1 = k;
        if (i1 != 0)
        {
          localObject = l;
          boolean bool = a((String)localObject, paramInetAddress);
          return bool;
        }
      }
      return false;
    }
    finally
    {
      g.unlock();
    }
  }
  
  public final void b(String paramString)
  {
    Object localObject = g;
    ((Lock)localObject).lock();
    try
    {
      localObject = l;
      boolean bool = k.a((CharSequence)localObject, paramString);
      if (!bool) {
        return;
      }
      int i1 = k + -1;
      k = i1;
      if (i1 != 0) {
        return;
      }
      i1 = 0;
      localObject = null;
      l = null;
      c();
      c(paramString);
      return;
    }
    finally
    {
      g.unlock();
    }
  }
  
  protected abstract int c(String paramString);
  
  protected abstract NetworkInfo d(String paramString);
  
  protected abstract int e(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.aa
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */