package com.truecaller.messaging.transport.mms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.util.concurrent.locks.Lock;

final class aa$1
  extends BroadcastReceiver
{
  aa$1(aa paramaa) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    paramContext = "android.net.conn.CONNECTIVITY_CHANGE";
    String str = paramIntent.getAction();
    boolean bool = paramContext.equals(str);
    if (!bool) {
      return;
    }
    int i = aa.a(paramIntent);
    paramIntent = aa.a(a);
    paramIntent.lock();
    try
    {
      paramIntent = a;
      aa.a(paramIntent, i);
      return;
    }
    finally
    {
      aa.a(a).unlock();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.aa.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */