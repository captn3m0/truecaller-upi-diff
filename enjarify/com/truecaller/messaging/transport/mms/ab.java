package com.truecaller.messaging.transport.mms;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Handler;
import c.g.b.k;
import com.truecaller.multisim.h;
import com.truecaller.multisim.x;
import com.truecaller.multisim.y;

public final class ab
{
  public static final ab.a a;
  
  static
  {
    ab.a locala = new com/truecaller/messaging/transport/mms/ab$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public static final z a(h paramh, Context paramContext, Handler paramHandler, ConnectivityManager paramConnectivityManager)
  {
    k.b(paramh, "multiSimManager");
    k.b(paramContext, "context");
    k.b(paramHandler, "handler");
    Object localObject = "connectivityManager";
    k.b(paramConnectivityManager, (String)localObject);
    boolean bool = paramh instanceof com.truecaller.multisim.ab;
    if (bool)
    {
      localObject = new com/truecaller/messaging/transport/mms/af;
      paramh = (com.truecaller.multisim.ab)paramh;
      ((af)localObject).<init>(paramContext, paramHandler, paramConnectivityManager, paramh);
      return (z)localObject;
    }
    bool = paramh instanceof x;
    if (bool)
    {
      localObject = new com/truecaller/messaging/transport/mms/ad;
      paramh = (com.truecaller.multisim.z)paramh;
      ((ad)localObject).<init>(paramContext, paramHandler, paramConnectivityManager, paramh);
      return (z)localObject;
    }
    bool = paramh instanceof y;
    if (bool)
    {
      localObject = new com/truecaller/messaging/transport/mms/ae;
      ((ae)localObject).<init>(paramContext, paramHandler, paramConnectivityManager, (h)paramh);
      return (z)localObject;
    }
    localObject = new com/truecaller/messaging/transport/mms/ac;
    ((ac)localObject).<init>(paramContext, paramHandler, paramConnectivityManager, paramh);
    return (z)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */