package com.truecaller.messaging.transport.mms;

import android.net.Uri;
import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class aj$a
  extends u
{
  private final long b;
  private final byte[] c;
  private final Uri d;
  private final boolean e;
  
  private aj$a(e parame, long paramLong, byte[] paramArrayOfByte, Uri paramUri, boolean paramBoolean)
  {
    super(parame);
    b = paramLong;
    c = paramArrayOfByte;
    d = paramUri;
    e = paramBoolean;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".downloadMms(");
    Object localObject = Long.valueOf(b);
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(d, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Boolean.valueOf(e), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.aj.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */