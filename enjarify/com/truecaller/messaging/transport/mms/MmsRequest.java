package com.truecaller.messaging.transport.mms;

import android.app.PendingIntent;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.multisim.SimInfo;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import okhttp3.ab;
import okhttp3.ab.a;
import okhttp3.ac;
import okhttp3.u;
import okhttp3.w;

class MmsRequest
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private static final w g = w.b("application/vnd.wap.mms-message; charset=utf-8");
  private static final w h = w.b("application/vnd.wap.mms-message");
  final boolean a;
  final Uri b;
  public final u c;
  final PendingIntent d;
  final SimInfo e;
  final boolean f;
  private final String i;
  private final boolean j;
  private final Map k;
  
  static
  {
    MmsRequest.1 local1 = new com/truecaller/messaging/transport/mms/MmsRequest$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  private MmsRequest(Parcel paramParcel)
  {
    int m = paramParcel.readInt();
    boolean bool = true;
    if (m != 0)
    {
      m = 1;
    }
    else
    {
      m = 0;
      localObject = null;
    }
    a = m;
    Object localObject = Uri.class.getClassLoader();
    localObject = (Uri)paramParcel.readParcelable((ClassLoader)localObject);
    b = ((Uri)localObject);
    localObject = paramParcel.readString();
    if (localObject == null)
    {
      m = 0;
      localObject = null;
      c = null;
    }
    else
    {
      localObject = u.e((String)localObject);
      c = ((u)localObject);
    }
    localObject = PendingIntent.class.getClassLoader();
    localObject = (PendingIntent)paramParcel.readParcelable((ClassLoader)localObject);
    d = ((PendingIntent)localObject);
    localObject = paramParcel.readString();
    i = ((String)localObject);
    m = paramParcel.readInt();
    if (m != 0)
    {
      m = 1;
    }
    else
    {
      m = 0;
      localObject = null;
    }
    j = m;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    k = ((Map)localObject);
    localObject = k;
    ClassLoader localClassLoader = ClassLoader.getSystemClassLoader();
    paramParcel.readMap((Map)localObject, localClassLoader);
    localObject = SimInfo.class.getClassLoader();
    localObject = (SimInfo)paramParcel.readParcelable((ClassLoader)localObject);
    e = ((SimInfo)localObject);
    int n = paramParcel.readInt();
    if (n == 0) {
      bool = false;
    }
    f = bool;
  }
  
  MmsRequest(boolean paramBoolean1, Uri paramUri, u paramu, PendingIntent paramPendingIntent, String paramString, boolean paramBoolean2, Map paramMap, SimInfo paramSimInfo, boolean paramBoolean3)
  {
    a = paramBoolean1;
    b = paramUri;
    c = paramu;
    d = paramPendingIntent;
    i = paramString;
    j = paramBoolean2;
    k = paramMap;
    e = paramSimInfo;
    f = paramBoolean3;
  }
  
  final ab a(ar paramar, u paramu, byte[] paramArrayOfByte)
  {
    ab.a locala = new okhttp3/ab$a;
    locala.<init>();
    Object localObject = c;
    if (localObject != null) {
      paramu = (u)localObject;
    }
    locala.a(paramu);
    locala.b("Accept", "*/*, application/vnd.wap.mms-message, application/vnd.wap.sic");
    paramu = "User-Agent";
    localObject = paramar.a();
    locala.b(paramu, (String)localObject);
    paramar = paramar.b();
    boolean bool1 = TextUtils.isEmpty(paramar);
    if (!bool1)
    {
      paramu = i;
      locala.b(paramu, paramar);
    }
    paramar = k.entrySet().iterator();
    for (;;)
    {
      bool1 = paramar.hasNext();
      if (!bool1) {
        break;
      }
      paramu = (Map.Entry)paramar.next();
      localObject = (String)paramu.getKey();
      paramu = (String)paramu.getValue();
      locala.a((String)localObject, paramu);
    }
    boolean bool2 = j;
    if (bool2) {
      paramar = g;
    } else {
      paramar = h;
    }
    bool1 = a;
    if (bool1)
    {
      paramar = "GET";
      bool1 = false;
      paramu = null;
      locala.a(paramar, null);
    }
    else
    {
      bool1 = false;
      paramu = new String[0];
      AssertionUtil.AlwaysFatal.isNotNull(paramArrayOfByte, paramu);
      paramar = ac.a(paramar, paramArrayOfByte);
      paramu = "POST";
      locala.a(paramu, paramar);
    }
    return locala.a();
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int m = a;
    paramParcel.writeInt(m);
    Object localObject = b;
    paramParcel.writeParcelable((Parcelable)localObject, 0);
    localObject = c;
    if (localObject == null)
    {
      n = 0;
      localObject = null;
      paramParcel.writeString(null);
    }
    else
    {
      localObject = ((u)localObject).toString();
      paramParcel.writeString((String)localObject);
    }
    localObject = d;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    localObject = i;
    paramParcel.writeString((String)localObject);
    int n = j;
    paramParcel.writeInt(n);
    localObject = k;
    paramParcel.writeMap((Map)localObject);
    localObject = e;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    paramInt = f;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.MmsRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */