package com.truecaller.messaging.transport.mms;

import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.a;
import okhttp3.u;

final class al
  extends e
{
  al(Context paramContext, SimInfo paramSimInfo, a parama)
  {
    super(paramContext, paramSimInfo, parama);
  }
  
  final boolean a(Uri paramUri1, Uri paramUri2, PendingIntent paramPendingIntent)
  {
    if (paramUri2 == null) {
      paramUri2 = null;
    } else {
      paramUri2 = u.e(paramUri2.toString());
    }
    Context localContext = a;
    a locala = c;
    SimInfo localSimInfo = b;
    return MmsService.a(false, localContext, paramUri1, paramUri2, paramPendingIntent, locala, localSimInfo);
  }
  
  final boolean b(Uri paramUri1, Uri paramUri2, PendingIntent paramPendingIntent)
  {
    if (paramUri2 == null) {
      paramUri2 = null;
    } else {
      paramUri2 = u.e(paramUri2.toString());
    }
    Context localContext = a;
    a locala = c;
    SimInfo localSimInfo = b;
    return MmsService.a(true, localContext, paramUri1, paramUri2, paramPendingIntent, locala, localSimInfo);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.al
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */