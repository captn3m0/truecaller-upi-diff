package com.truecaller.messaging.transport.mms;

import dagger.a.d;
import javax.inject.Provider;

public final class w
  implements d
{
  private final n a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private w(n paramn, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramn;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static w a(n paramn, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    w localw = new com/truecaller/messaging/transport/mms/w;
    localw.<init>(paramn, paramProvider1, paramProvider2, paramProvider3);
    return localw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */