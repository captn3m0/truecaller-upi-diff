package com.truecaller.messaging.transport.mms;

import dagger.a.d;
import javax.inject.Provider;

public final class u
  implements d
{
  private final n a;
  private final Provider b;
  private final Provider c;
  
  private u(n paramn, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramn;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static u a(n paramn, Provider paramProvider1, Provider paramProvider2)
  {
    u localu = new com/truecaller/messaging/transport/mms/u;
    localu.<init>(paramn, paramProvider1, paramProvider2);
    return localu;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */