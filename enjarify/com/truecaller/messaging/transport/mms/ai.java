package com.truecaller.messaging.transport.mms;

import android.net.Uri;
import com.android.a.a.a.u;

public abstract interface ai
{
  public abstract void a(long paramLong1, long paramLong2, u paramu, Uri paramUri);
  
  public abstract void a(long paramLong, byte[] paramArrayOfByte, Uri paramUri, boolean paramBoolean);
  
  public abstract void a(byte[] paramArrayOfByte, Uri paramUri);
  
  public abstract void a(byte[] paramArrayOfByte, Uri paramUri, int paramInt);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.ai
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */