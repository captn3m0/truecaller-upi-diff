package com.truecaller.messaging.transport.mms;

import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d
{
  private final o a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private q(o paramo, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramo;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static q a(o paramo, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    q localq = new com/truecaller/messaging/transport/mms/q;
    localq.<init>(paramo, paramProvider1, paramProvider2, paramProvider3);
    return localq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */