package com.truecaller.messaging.transport.mms;

import android.content.res.AssetManager;
import com.google.gson.stream.JsonReader;
import com.truecaller.log.AssertionUtil;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

final class b
{
  private final AssetManager a;
  
  b(AssetManager paramAssetManager)
  {
    a = paramAssetManager;
  }
  
  private static c.a a(JsonReader paramJsonReader, List paramList, int paramInt1, int paramInt2)
  {
    paramJsonReader.beginObject();
    String str1 = null;
    String str2 = null;
    String str3 = null;
    String str4 = null;
    String str5 = null;
    int i = 0;
    int j = 0;
    for (;;)
    {
      boolean bool1 = paramJsonReader.hasNext();
      if (!bool1) {
        break;
      }
      String str6 = paramJsonReader.nextName();
      if (str6 == null)
      {
        paramJsonReader.skipValue();
      }
      else
      {
        int i3 = str6.hashCode();
        String str7;
        boolean bool4;
        switch (i3)
        {
        default: 
          break;
        case 1183882708: 
          str7 = "mmsport";
          bool1 = str6.equals(str7);
          if (bool1) {
            int k = 6;
          }
          break;
        case 3575610: 
          str7 = "type";
          boolean bool2 = str6.equals(str7);
          if (bool2) {
            int m = 3;
          }
          break;
        case 3355632: 
          str7 = "mmsc";
          boolean bool3 = str6.equals(str7);
          if (bool3) {
            int n = 4;
          }
          break;
        case 108258: 
          str7 = "mnc";
          bool4 = str6.equals(str7);
          if (bool4) {
            bool4 = true;
          }
          break;
        case 107917: 
          str7 = "mcc";
          bool4 = str6.equals(str7);
          if (bool4)
          {
            bool4 = false;
            str6 = null;
          }
          break;
        case 96799: 
          str7 = "apn";
          bool4 = str6.equals(str7);
          if (bool4) {
            int i1 = 2;
          }
          break;
        case -1954254981: 
          str7 = "mmsproxy";
          boolean bool5 = str6.equals(str7);
          if (bool5) {
            i2 = 5;
          }
          break;
        }
        int i2 = -1;
        switch (i2)
        {
        default: 
          paramJsonReader.skipValue();
          break;
        case 6: 
          str5 = paramJsonReader.nextString();
          break;
        case 5: 
          str4 = paramJsonReader.nextString();
          break;
        case 4: 
          str3 = paramJsonReader.nextString();
          break;
        case 3: 
          str2 = paramJsonReader.nextString();
          break;
        case 2: 
          str1 = paramJsonReader.nextString();
          break;
        case 1: 
          j = paramJsonReader.nextInt();
          break;
        case 0: 
          i = paramJsonReader.nextInt();
        }
      }
    }
    paramJsonReader.endObject();
    if ((paramInt1 == i) && (paramInt2 == j)) {
      if (paramList != null)
      {
        boolean bool6 = paramList.contains(str1);
        if (!bool6) {}
      }
      else
      {
        return a.a(str2, str3, str4, str5);
      }
    }
    return null;
  }
  
  final void a(String paramString1, String paramString2, List paramList1, List paramList2)
  {
    int i = 1;
    int j = 3;
    int k;
    int m;
    Object localObject2;
    String str2;
    try
    {
      localObject1 = paramString1.substring(0, j);
      k = Integer.parseInt((String)localObject1);
      String str1 = paramString1.substring(j);
      m = Integer.parseInt(str1);
    }
    catch (Exception localException)
    {
      Object localObject1 = new String[i];
      localObject2 = new java/lang/StringBuilder;
      str2 = "Invalid mcc/mnc from system ";
      ((StringBuilder)localObject2).<init>(str2);
      ((StringBuilder)localObject2).append(paramString1);
      ((StringBuilder)localObject2).append(": ");
      ((StringBuilder)localObject2).append(localException);
      paramString1 = ((StringBuilder)localObject2).toString();
      localObject1[0] = paramString1;
      m = 0;
      paramString1 = null;
      k = 0;
      localObject1 = null;
    }
    if ((k == 0) && (m == 0))
    {
      new String[1][0] = "Can not get valid mcc/mnc from system";
      return;
    }
    j = 0;
    InputStream localInputStream = null;
    try
    {
      localObject2 = a;
      int n = 2;
      localInputStream = ((AssetManager)localObject2).open(paramString2, n);
      paramString2 = new com/google/gson/stream/JsonReader;
      localObject2 = new java/io/InputStreamReader;
      ((InputStreamReader)localObject2).<init>(localInputStream);
      paramString2.<init>((Reader)localObject2);
      paramString2.beginObject();
      for (;;)
      {
        boolean bool = paramString2.hasNext();
        if (!bool) {
          break;
        }
        localObject2 = paramString2.nextName();
        if (localObject2 == null)
        {
          paramString2.skipValue();
        }
        else
        {
          n = -1;
          int i1 = ((String)localObject2).hashCode();
          int i2 = 96799;
          String str3;
          if (i1 != i2)
          {
            i2 = 351608024;
            if (i1 == i2)
            {
              str3 = "version";
              bool = ((String)localObject2).equals(str3);
              if (bool)
              {
                n = 0;
                str2 = null;
              }
            }
          }
          else
          {
            str3 = "apn";
            bool = ((String)localObject2).equals(str3);
            if (bool) {
              n = 1;
            }
          }
          switch (n)
          {
          default: 
            paramString2.skipValue();
            break;
          case 1: 
            paramString2.beginArray();
            for (;;)
            {
              bool = paramString2.hasNext();
              if (!bool) {
                break;
              }
              localObject2 = a(paramString2, paramList1, k, m);
              if (localObject2 != null) {
                paramList2.add(localObject2);
              }
            }
            paramString2.endArray();
            break;
          case 0: 
            paramString2.skipValue();
          }
        }
      }
      return;
    }
    catch (IOException paramString1)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramString1);
      if (localInputStream != null) {
        try
        {
          localInputStream.close();
          return;
        }
        catch (IOException localIOException) {}
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */