package com.truecaller.messaging.transport.mms;

import android.net.ConnectivityManager.NetworkCallback;
import android.net.Network;

final class ae$1
  extends ConnectivityManager.NetworkCallback
{
  ae$1(ae paramae) {}
  
  public final void onAvailable(Network arg1)
  {
    synchronized (a)
    {
      ae localae = a;
      ae.a(localae, 0);
      return;
    }
  }
  
  public final void onLost(Network arg1)
  {
    synchronized (a)
    {
      ae localae = a;
      int i = -1;
      ae.a(localae, i);
      return;
    }
  }
  
  public final void onUnavailable()
  {
    synchronized (a)
    {
      ae localae2 = a;
      int i = 1;
      ae.a(localae2, i);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.ae.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */