package com.truecaller.messaging.transport.mms;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.net.Uri;
import com.truecaller.common.h.am;

final class ag
  extends CursorWrapper
  implements h.a
{
  static final String[] a = tmp14_5;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  
  static
  {
    String[] tmp4_1 = new String[5];
    String[] tmp5_4 = tmp4_1;
    String[] tmp5_4 = tmp4_1;
    tmp5_4[0] = "_id";
    tmp5_4[1] = "ct";
    String[] tmp14_5 = tmp5_4;
    String[] tmp14_5 = tmp5_4;
    tmp14_5[2] = "text";
    tmp14_5[3] = "chset";
    tmp14_5[4] = "cl";
  }
  
  ag(Cursor paramCursor)
  {
    super(paramCursor);
    int i = paramCursor.getColumnIndexOrThrow("_id");
    b = i;
    i = paramCursor.getColumnIndexOrThrow("ct");
    c = i;
    i = paramCursor.getColumnIndexOrThrow("text");
    d = i;
    i = paramCursor.getColumnIndexOrThrow("chset");
    e = i;
    int j = paramCursor.getColumnIndexOrThrow("cl");
    f = j;
  }
  
  public final String a()
  {
    int i = c;
    return am.n(getString(i));
  }
  
  public final String b()
  {
    int i = d;
    return am.n(getString(i));
  }
  
  public final Uri c()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("content://mms/part/");
    int i = b;
    long l = getLong(i);
    localStringBuilder.append(l);
    return Uri.parse(localStringBuilder.toString());
  }
  
  public final String d()
  {
    int i = f;
    return getString(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */