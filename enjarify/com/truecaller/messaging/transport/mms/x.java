package com.truecaller.messaging.transport.mms;

import dagger.a.d;
import javax.inject.Provider;

public final class x
  implements d
{
  private final n a;
  private final Provider b;
  private final Provider c;
  
  private x(n paramn, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramn;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static x a(n paramn, Provider paramProvider1, Provider paramProvider2)
  {
    x localx = new com/truecaller/messaging/transport/mms/x;
    localx.<init>(paramn, paramProvider1, paramProvider2);
    return localx;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */