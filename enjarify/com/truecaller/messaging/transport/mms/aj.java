package com.truecaller.messaging.transport.mms;

import android.net.Uri;
import com.android.a.a.a.u;
import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;

public final class aj
  implements ai
{
  private final v a;
  
  public aj(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return ai.class.equals(paramClass);
  }
  
  public final void a(long paramLong1, long paramLong2, u paramu, Uri paramUri)
  {
    v localv = a;
    aj.c localc = new com/truecaller/messaging/transport/mms/aj$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, paramLong1, paramLong2, paramu, paramUri, (byte)0);
    localv.a(localc);
  }
  
  public final void a(long paramLong, byte[] paramArrayOfByte, Uri paramUri, boolean paramBoolean)
  {
    v localv = a;
    aj.a locala = new com/truecaller/messaging/transport/mms/aj$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramLong, paramArrayOfByte, paramUri, paramBoolean, (byte)0);
    localv.a(locala);
  }
  
  public final void a(byte[] paramArrayOfByte, Uri paramUri)
  {
    v localv = a;
    aj.b localb = new com/truecaller/messaging/transport/mms/aj$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramArrayOfByte, paramUri, (byte)0);
    localv.a(localb);
  }
  
  public final void a(byte[] paramArrayOfByte, Uri paramUri, int paramInt)
  {
    v localv = a;
    aj.d locald = new com/truecaller/messaging/transport/mms/aj$d;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locald.<init>(locale, paramArrayOfByte, paramUri, paramInt, (byte)0);
    localv.a(locald);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.aj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */