package com.truecaller.messaging.transport.mms;

import android.net.Uri;
import com.truecaller.androidactors.e;

final class aj$c
  extends com.truecaller.androidactors.u
{
  private final long b;
  private final long c;
  private final com.android.a.a.a.u d;
  private final Uri e;
  
  private aj$c(e parame, long paramLong1, long paramLong2, com.android.a.a.a.u paramu, Uri paramUri)
  {
    super(parame);
    b = paramLong1;
    c = paramLong2;
    d = paramu;
    e = paramUri;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".sendMms(");
    Object localObject = Long.valueOf(b);
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Long.valueOf(c), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(d, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(e, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.aj.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */