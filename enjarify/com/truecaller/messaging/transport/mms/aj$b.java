package com.truecaller.messaging.transport.mms;

import android.net.Uri;
import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class aj$b
  extends u
{
  private final byte[] b;
  private final Uri c;
  
  private aj$b(e parame, byte[] paramArrayOfByte, Uri paramUri)
  {
    super(parame);
    b = paramArrayOfByte;
    c = paramUri;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".sendAcknowledgeForMmsDownload(");
    Object localObject = b;
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.aj.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */