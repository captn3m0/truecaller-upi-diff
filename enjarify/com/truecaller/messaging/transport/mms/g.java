package com.truecaller.messaging.transport.mms;

import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import android.telephony.SmsManager;

final class g
  implements k
{
  private final Context a;
  private final SmsManager b;
  
  public g(Context paramContext, SmsManager paramSmsManager)
  {
    a = paramContext;
    b = paramSmsManager;
  }
  
  public final boolean a(Uri paramUri, String paramString, PendingIntent paramPendingIntent)
  {
    c.g.b.k.b(paramUri, "contentUri");
    c.g.b.k.b(paramPendingIntent, "sentIntent");
    SmsManager localSmsManager = b;
    Context localContext = a;
    localSmsManager.sendMultimediaMessage(localContext, paramUri, paramString, null, paramPendingIntent);
    return true;
  }
  
  public final boolean a(String paramString, Uri paramUri, PendingIntent paramPendingIntent)
  {
    c.g.b.k.b(paramString, "locationUrl");
    c.g.b.k.b(paramUri, "contentUri");
    c.g.b.k.b(paramPendingIntent, "downloadedIntent");
    SmsManager localSmsManager = b;
    Context localContext = a;
    localSmsManager.downloadMultimediaMessage(localContext, paramString, paramUri, null, paramPendingIntent);
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */