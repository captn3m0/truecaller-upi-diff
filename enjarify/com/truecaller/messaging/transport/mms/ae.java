package com.truecaller.messaging.transport.mms;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.ConnectivityManager.NetworkCallback;
import android.net.NetworkRequest;
import android.net.NetworkRequest.Builder;
import android.os.Handler;
import com.truecaller.multisim.h;

final class ae
  extends ac
{
  private volatile int c = -1;
  private ConnectivityManager.NetworkCallback d;
  
  ae(Context paramContext, Handler paramHandler, ConnectivityManager paramConnectivityManager, h paramh)
  {
    super(paramContext, paramHandler, paramConnectivityManager, paramh);
    paramContext = new com/truecaller/messaging/transport/mms/ae$1;
    paramContext.<init>(this);
    d = paramContext;
  }
  
  protected final int a(String paramString, boolean paramBoolean)
  {
    int i = 1;
    ConnectivityManager.NetworkCallback localNetworkCallback = null;
    if (paramBoolean) {
      try
      {
        j = c;
        paramBoolean = true;
        switch (j)
        {
        default: 
          break;
        case 1: 
          return paramBoolean;
        case 0: 
          return 0;
        case -1: 
          return i;
        }
        return paramBoolean;
      }
      finally {}
    }
    Object localObject = new android/net/NetworkRequest$Builder;
    ((NetworkRequest.Builder)localObject).<init>();
    ((NetworkRequest.Builder)localObject).addTransportType(0);
    ((NetworkRequest.Builder)localObject).addCapability(0);
    ((NetworkRequest.Builder)localObject).setNetworkSpecifier(paramString);
    int j = -1;
    try
    {
      c = j;
      paramString = a;
      localObject = ((NetworkRequest.Builder)localObject).build();
      localNetworkCallback = d;
      paramString.requestNetwork((NetworkRequest)localObject, localNetworkCallback);
      return i;
    }
    finally {}
  }
  
  protected final int c(String paramString)
  {
    try
    {
      paramString = a;
      ConnectivityManager.NetworkCallback localNetworkCallback = d;
      paramString.unregisterNetworkCallback(localNetworkCallback);
      return 4;
    }
    finally {}
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.ae
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */