package com.truecaller.messaging.transport.mms;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.SparseArray;
import com.android.a.a.a.o;
import com.android.a.a.c;
import com.truecaller.messaging.data.types.BinaryEntity;

class PduEntity
  extends BinaryEntity
{
  public static final Parcelable.Creator CREATOR;
  final int a;
  final String k;
  final String l;
  final String m;
  final String n;
  final String o;
  
  static
  {
    PduEntity.1 local1 = new com/truecaller/messaging/transport/mms/PduEntity$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  private PduEntity(Parcel paramParcel)
  {
    super(paramParcel);
    int i = paramParcel.readInt();
    a = i;
    String str = paramParcel.readString();
    k = str;
    str = paramParcel.readString();
    l = str;
    str = paramParcel.readString();
    m = str;
    str = paramParcel.readString();
    n = str;
    paramParcel = paramParcel.readString();
    o = paramParcel;
  }
  
  PduEntity(o paramo, Uri paramUri, int paramInt)
  {
    super((String)localObject, paramUri, l1);
    int i = paramo.b();
    a = i;
    paramUri = paramo.g();
    paramInt = 0;
    String str2 = null;
    if (paramUri != null)
    {
      localObject = new java/lang/String;
      ((String)localObject).<init>(paramUri);
    }
    else
    {
      j = 0;
      localObject = null;
    }
    k = ((String)localObject);
    paramUri = paramo.f();
    if (paramUri != null)
    {
      localObject = new java/lang/String;
      ((String)localObject).<init>(paramUri);
    }
    else
    {
      j = 0;
      localObject = null;
    }
    l = ((String)localObject);
    paramUri = d;
    int j = 197;
    paramUri = (byte[])paramUri.get(j);
    if (paramUri != null)
    {
      paramUri = c.a(paramUri);
    }
    else
    {
      i = 0;
      paramUri = null;
    }
    m = paramUri;
    paramUri = paramo.a();
    if (paramUri != null)
    {
      paramUri = c.a(paramUri);
    }
    else
    {
      i = 0;
      paramUri = null;
    }
    n = paramUri;
    paramo = paramo.c();
    if (paramo != null) {
      str2 = c.a(paramo);
    }
    o = str2;
  }
  
  PduEntity(String paramString1, Uri paramUri, int paramInt1, int paramInt2, String paramString2, String paramString3)
  {
    super(paramString1, paramUri, l1);
    a = paramInt2;
    k = null;
    l = null;
    m = null;
    n = paramString2;
    o = paramString3;
  }
  
  public final boolean a()
  {
    return false;
  }
  
  public final boolean b()
  {
    return false;
  }
  
  public final boolean c()
  {
    return false;
  }
  
  public final boolean d()
  {
    return false;
  }
  
  public final boolean e()
  {
    return false;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    paramInt = a;
    paramParcel.writeInt(paramInt);
    String str = k;
    paramParcel.writeString(str);
    str = l;
    paramParcel.writeString(str);
    str = m;
    paramParcel.writeString(str);
    str = n;
    paramParcel.writeString(str);
    str = o;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.PduEntity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */