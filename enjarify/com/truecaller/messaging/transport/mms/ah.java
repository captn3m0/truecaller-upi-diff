package com.truecaller.messaging.transport.mms;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.ParcelFileDescriptor.AutoCloseInputStream;
import android.text.TextUtils;
import com.truecaller.utils.extensions.d;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import okhttp3.u;
import okhttp3.y;

final class ah
  implements MmsService.a
{
  private final Context a;
  private final c b;
  private final z c;
  private final ar d;
  private final y e;
  
  ah(Context paramContext, y paramy, ar paramar, z paramz, c paramc)
  {
    a = paramContext;
    b = paramc;
    d = paramar;
    c = paramz;
    e = paramy;
  }
  
  private void a(PendingIntent paramPendingIntent, int paramInt1, int paramInt2, byte[] paramArrayOfByte)
  {
    if (paramPendingIntent == null) {
      return;
    }
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    if (paramArrayOfByte != null)
    {
      String str = "android.telephony.extra.MMS_DATA";
      localIntent.putExtra(str, paramArrayOfByte);
    }
    int i = -1;
    if (paramInt2 != i)
    {
      paramArrayOfByte = "android.telephony.extra.MMS_HTTP_STATUS";
      localIntent.putExtra(paramArrayOfByte, paramInt2);
    }
    try
    {
      Context localContext = a;
      paramPendingIntent.send(localContext, paramInt1, localIntent);
      return;
    }
    catch (PendingIntent.CanceledException localCanceledException) {}
  }
  
  private static boolean a(z paramz, c.a parama, u paramu)
  {
    parama = parama.b();
    boolean bool1 = TextUtils.isEmpty(parama);
    if (bool1) {
      parama = b;
    }
    int i = 1;
    bool1 = false;
    try
    {
      arrayOfInetAddress = InetAddress.getAllByName(parama);
      j = arrayOfInetAddress.length;
      k = 0;
      bool2 = false;
      arrayOfString = null;
    }
    catch (UnknownHostException localUnknownHostException2)
    {
      try
      {
        InetAddress[] arrayOfInetAddress;
        int j;
        int k;
        Object localObject1 = arrayOfInetAddress[k];
        boolean bool3 = paramz.a((InetAddress)localObject1);
        Object localObject2;
        if (bool3)
        {
          try
          {
            arrayOfString = new String[i];
            localObject2 = "Requested route to ";
            localObject1 = String.valueOf(localObject1);
            localObject1 = ((String)localObject2).concat((String)localObject1);
            arrayOfString[0] = localObject1;
            bool2 = true;
          }
          catch (UnknownHostException localUnknownHostException1)
          {
            bool2 = true;
            break label170;
          }
        }
        else
        {
          localObject2 = new String[i];
          String str = "Could not requested route to ";
          localObject1 = String.valueOf(localObject1);
          localObject1 = str.concat((String)localObject1);
          localObject2[0] = localObject1;
        }
        k += 1;
      }
      catch (UnknownHostException localUnknownHostException3)
      {
        boolean bool2;
        String[] arrayOfString;
        for (;;) {}
      }
      localUnknownHostException2;
      bool2 = false;
      arrayOfString = null;
    }
    if (k < j)
    {
      label170:
      paramz = new String[i];
      paramu = "Unknown host ";
      parama = String.valueOf(parama);
      parama = paramu.concat(parama);
      paramz[0] = parama;
    }
    return bool2;
  }
  
  private byte[] a(Uri paramUri)
  {
    Object localObject1 = a.getContentResolver();
    Object localObject2 = null;
    Object localObject3 = "r";
    try
    {
      paramUri = ((ContentResolver)localObject1).openFileDescriptor(paramUri, (String)localObject3);
      localObject1 = new java/io/ByteArrayOutputStream;
      ((ByteArrayOutputStream)localObject1).<init>();
      int i = 1024;
      localObject3 = new byte[i];
      ParcelFileDescriptor.AutoCloseInputStream localAutoCloseInputStream = new android/os/ParcelFileDescriptor$AutoCloseInputStream;
      localAutoCloseInputStream.<init>(paramUri);
      try
      {
        for (;;)
        {
          int j = localAutoCloseInputStream.read((byte[])localObject3);
          if (j <= 0) {
            break;
          }
          localObject2 = null;
          ((ByteArrayOutputStream)localObject1).write((byte[])localObject3, 0, j);
        }
        ((ByteArrayOutputStream)localObject1).close();
        paramUri = ((ByteArrayOutputStream)localObject1).toByteArray();
        d.a(localAutoCloseInputStream);
        return paramUri;
      }
      finally
      {
        localObject2 = localAutoCloseInputStream;
      }
      d.a((Closeable)localObject2);
    }
    finally {}
    throw paramUri;
  }
  
  /* Error */
  public final boolean a(MmsRequest paramMmsRequest)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore_2
    //   2: aload_1
    //   3: astore_3
    //   4: aload_1
    //   5: getfield 146	com/truecaller/messaging/transport/mms/MmsRequest:a	Z
    //   8: istore 4
    //   10: iconst_5
    //   11: istore 5
    //   13: iconst_1
    //   14: istore 6
    //   16: iconst_m1
    //   17: istore 7
    //   19: iconst_0
    //   20: istore 8
    //   22: aconst_null
    //   23: astore 9
    //   25: iload 4
    //   27: ifne +63 -> 90
    //   30: aload_1
    //   31: getfield 150	com/truecaller/messaging/transport/mms/MmsRequest:b	Landroid/net/Uri;
    //   34: astore 10
    //   36: aload_0
    //   37: aload 10
    //   39: invokespecial 153	com/truecaller/messaging/transport/mms/ah:a	(Landroid/net/Uri;)[B
    //   42: astore 10
    //   44: aload 10
    //   46: ifnonnull +50 -> 96
    //   49: aload_1
    //   50: getfield 156	com/truecaller/messaging/transport/mms/MmsRequest:d	Landroid/app/PendingIntent;
    //   53: astore 10
    //   55: aload_0
    //   56: aload 10
    //   58: iload 6
    //   60: iload 7
    //   62: aconst_null
    //   63: invokespecial 159	com/truecaller/messaging/transport/mms/ah:a	(Landroid/app/PendingIntent;II[B)V
    //   66: iconst_0
    //   67: ireturn
    //   68: invokestatic 165	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   71: aload_3
    //   72: getfield 156	com/truecaller/messaging/transport/mms/MmsRequest:d	Landroid/app/PendingIntent;
    //   75: astore 10
    //   77: aload_2
    //   78: aload 10
    //   80: iload 5
    //   82: iload 7
    //   84: aconst_null
    //   85: invokespecial 159	com/truecaller/messaging/transport/mms/ah:a	(Landroid/app/PendingIntent;II[B)V
    //   88: iconst_0
    //   89: ireturn
    //   90: iconst_0
    //   91: istore 4
    //   93: aconst_null
    //   94: astore 10
    //   96: aload_3
    //   97: getfield 168	com/truecaller/messaging/transport/mms/MmsRequest:e	Lcom/truecaller/multisim/SimInfo;
    //   100: astore 11
    //   102: aload 11
    //   104: ifnull +15 -> 119
    //   107: aload_3
    //   108: getfield 168	com/truecaller/messaging/transport/mms/MmsRequest:e	Lcom/truecaller/multisim/SimInfo;
    //   111: getfield 171	com/truecaller/multisim/SimInfo:b	Ljava/lang/String;
    //   114: astore 11
    //   116: goto +7 -> 123
    //   119: ldc -83
    //   121: astore 11
    //   123: aload_2
    //   124: getfield 28	com/truecaller/messaging/transport/mms/ah:c	Lcom/truecaller/messaging/transport/mms/z;
    //   127: astore 12
    //   129: aload 12
    //   131: aload 11
    //   133: invokeinterface 176 2 0
    //   138: istore 13
    //   140: iload 13
    //   142: ifne +37 -> 179
    //   145: aload_3
    //   146: getfield 156	com/truecaller/messaging/transport/mms/MmsRequest:d	Landroid/app/PendingIntent;
    //   149: astore 10
    //   151: bipush 8
    //   153: istore 14
    //   155: aload_2
    //   156: aload 10
    //   158: iload 14
    //   160: iload 7
    //   162: aconst_null
    //   163: invokespecial 159	com/truecaller/messaging/transport/mms/ah:a	(Landroid/app/PendingIntent;II[B)V
    //   166: aload_2
    //   167: getfield 28	com/truecaller/messaging/transport/mms/ah:c	Lcom/truecaller/messaging/transport/mms/z;
    //   170: aload 11
    //   172: invokeinterface 180 2 0
    //   177: iconst_0
    //   178: ireturn
    //   179: aload_2
    //   180: getfield 24	com/truecaller/messaging/transport/mms/ah:b	Lcom/truecaller/messaging/transport/mms/c;
    //   183: astore 12
    //   185: aload_3
    //   186: getfield 168	com/truecaller/messaging/transport/mms/MmsRequest:e	Lcom/truecaller/multisim/SimInfo;
    //   189: astore 15
    //   191: aload_2
    //   192: getfield 28	com/truecaller/messaging/transport/mms/ah:c	Lcom/truecaller/messaging/transport/mms/z;
    //   195: astore 16
    //   197: aload 16
    //   199: invokeinterface 183 1 0
    //   204: astore 16
    //   206: aload 12
    //   208: aload 15
    //   210: aload 16
    //   212: invokeinterface 188 3 0
    //   217: astore 12
    //   219: aload 12
    //   221: invokeinterface 193 1 0
    //   226: istore 17
    //   228: iload 17
    //   230: ifeq +36 -> 266
    //   233: aload_3
    //   234: getfield 156	com/truecaller/messaging/transport/mms/MmsRequest:d	Landroid/app/PendingIntent;
    //   237: astore 10
    //   239: iconst_2
    //   240: istore 14
    //   242: aload_2
    //   243: aload 10
    //   245: iload 14
    //   247: iload 7
    //   249: aconst_null
    //   250: invokespecial 159	com/truecaller/messaging/transport/mms/ah:a	(Landroid/app/PendingIntent;II[B)V
    //   253: aload_2
    //   254: getfield 28	com/truecaller/messaging/transport/mms/ah:c	Lcom/truecaller/messaging/transport/mms/z;
    //   257: aload 11
    //   259: invokeinterface 180 2 0
    //   264: iconst_0
    //   265: ireturn
    //   266: aload 12
    //   268: invokeinterface 198 1 0
    //   273: astore 12
    //   275: iconst_m1
    //   276: istore 17
    //   278: aload 12
    //   280: invokeinterface 203 1 0
    //   285: istore 18
    //   287: iload 18
    //   289: ifeq +487 -> 776
    //   292: aload 12
    //   294: invokeinterface 207 1 0
    //   299: astore 16
    //   301: aload 16
    //   303: checkcast 54	com/truecaller/messaging/transport/mms/c$a
    //   306: astore 16
    //   308: iload 6
    //   310: anewarray 82	java/lang/String
    //   313: astore 19
    //   315: new 209	java/lang/StringBuilder
    //   318: astore 20
    //   320: ldc -45
    //   322: astore 21
    //   324: aload 20
    //   326: aload 21
    //   328: invokespecial 213	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   331: aload 16
    //   333: invokeinterface 216 1 0
    //   338: astore 21
    //   340: aload 20
    //   342: aload 21
    //   344: invokevirtual 220	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   347: pop
    //   348: ldc -34
    //   350: astore 21
    //   352: aload 20
    //   354: aload 21
    //   356: invokevirtual 225	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   359: pop
    //   360: aload 16
    //   362: invokeinterface 57 1 0
    //   367: astore 21
    //   369: aload 20
    //   371: aload 21
    //   373: invokevirtual 225	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   376: pop
    //   377: ldc -29
    //   379: astore 21
    //   381: aload 20
    //   383: aload 21
    //   385: invokevirtual 225	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   388: pop
    //   389: aload 16
    //   391: invokeinterface 230 1 0
    //   396: istore 22
    //   398: aload 20
    //   400: iload 22
    //   402: invokevirtual 233	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   405: pop
    //   406: ldc -21
    //   408: astore 21
    //   410: aload 20
    //   412: aload 21
    //   414: invokevirtual 225	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   417: pop
    //   418: aload 20
    //   420: invokevirtual 238	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   423: astore 20
    //   425: aload 19
    //   427: iconst_0
    //   428: aload 20
    //   430: aastore
    //   431: aload_3
    //   432: getfield 241	com/truecaller/messaging/transport/mms/MmsRequest:c	Lokhttp3/u;
    //   435: astore 19
    //   437: aload 19
    //   439: ifnonnull +15 -> 454
    //   442: aload 16
    //   444: invokeinterface 216 1 0
    //   449: astore 19
    //   451: goto +9 -> 460
    //   454: aload_3
    //   455: getfield 241	com/truecaller/messaging/transport/mms/MmsRequest:c	Lokhttp3/u;
    //   458: astore 19
    //   460: aload_2
    //   461: getfield 28	com/truecaller/messaging/transport/mms/ah:c	Lcom/truecaller/messaging/transport/mms/z;
    //   464: astore 20
    //   466: aload 20
    //   468: aload 16
    //   470: aload 19
    //   472: invokestatic 244	com/truecaller/messaging/transport/mms/ah:a	(Lcom/truecaller/messaging/transport/mms/z;Lcom/truecaller/messaging/transport/mms/c$a;Lokhttp3/u;)Z
    //   475: istore 23
    //   477: iload 23
    //   479: ifeq -201 -> 278
    //   482: aload_2
    //   483: getfield 26	com/truecaller/messaging/transport/mms/ah:d	Lcom/truecaller/messaging/transport/mms/ar;
    //   486: astore 19
    //   488: aload 16
    //   490: invokeinterface 216 1 0
    //   495: astore 20
    //   497: aload_3
    //   498: aload 19
    //   500: aload 20
    //   502: aload 10
    //   504: invokevirtual 247	com/truecaller/messaging/transport/mms/MmsRequest:a	(Lcom/truecaller/messaging/transport/mms/ar;Lokhttp3/u;[B)Lokhttp3/ab;
    //   507: astore 19
    //   509: aload 16
    //   511: invokeinterface 57 1 0
    //   516: astore 20
    //   518: aload_2
    //   519: getfield 30	com/truecaller/messaging/transport/mms/ah:e	Lokhttp3/y;
    //   522: astore 21
    //   524: aload 20
    //   526: invokestatic 63	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   529: istore 24
    //   531: iload 24
    //   533: ifne +72 -> 605
    //   536: new 249	java/net/Proxy
    //   539: astore 21
    //   541: getstatic 255	java/net/Proxy$Type:HTTP	Ljava/net/Proxy$Type;
    //   544: astore 25
    //   546: new 257	java/net/InetSocketAddress
    //   549: astore 26
    //   551: aload 16
    //   553: invokeinterface 230 1 0
    //   558: istore 18
    //   560: aload 26
    //   562: aload 20
    //   564: iload 18
    //   566: invokespecial 260	java/net/InetSocketAddress:<init>	(Ljava/lang/String;I)V
    //   569: aload 21
    //   571: aload 25
    //   573: aload 26
    //   575: invokespecial 263	java/net/Proxy:<init>	(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V
    //   578: aload_2
    //   579: getfield 30	com/truecaller/messaging/transport/mms/ah:e	Lokhttp3/y;
    //   582: astore 26
    //   584: aload 26
    //   586: invokevirtual 268	okhttp3/y:c	()Lokhttp3/y$a;
    //   589: astore 26
    //   591: aload 26
    //   593: aload 21
    //   595: putfield 273	okhttp3/y$a:b	Ljava/net/Proxy;
    //   598: aload 26
    //   600: invokevirtual 276	okhttp3/y$a:d	()Lokhttp3/y;
    //   603: astore 21
    //   605: aload 21
    //   607: aload 19
    //   609: iconst_0
    //   610: invokestatic 281	okhttp3/aa:a	(Lokhttp3/y;Lokhttp3/ab;Z)Lokhttp3/aa;
    //   613: astore 26
    //   615: aload 26
    //   617: invokestatic 287	com/google/firebase/perf/network/FirebasePerfOkHttpClient:execute	(Lokhttp3/e;)Lokhttp3/ad;
    //   620: astore 26
    //   622: aload 26
    //   624: invokevirtual 291	okhttp3/ad:c	()Z
    //   627: istore 18
    //   629: iload 18
    //   631: ifne +70 -> 701
    //   634: iload 6
    //   636: anewarray 82	java/lang/String
    //   639: astore 16
    //   641: new 209	java/lang/StringBuilder
    //   644: astore 19
    //   646: ldc_w 293
    //   649: astore 20
    //   651: aload 19
    //   653: aload 20
    //   655: invokespecial 213	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   658: aload 26
    //   660: getfield 295	okhttp3/ad:d	Ljava/lang/String;
    //   663: astore 20
    //   665: aload 19
    //   667: aload 20
    //   669: invokevirtual 225	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   672: pop
    //   673: aload 19
    //   675: invokevirtual 238	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   678: astore 19
    //   680: aload 16
    //   682: iconst_0
    //   683: aload 19
    //   685: aastore
    //   686: aload 26
    //   688: getfield 298	okhttp3/ad:c	I
    //   691: istore 17
    //   693: aload 26
    //   695: ifnull +75 -> 770
    //   698: goto +67 -> 765
    //   701: aload 26
    //   703: getfield 302	okhttp3/ad:g	Lokhttp3/ae;
    //   706: astore 16
    //   708: aload 16
    //   710: invokevirtual 306	okhttp3/ae:e	()[B
    //   713: astore 10
    //   715: aload 26
    //   717: ifnull +65 -> 782
    //   720: aload 26
    //   722: invokevirtual 307	okhttp3/ad:close	()V
    //   725: goto +57 -> 782
    //   728: astore 10
    //   730: goto +11 -> 741
    //   733: astore 10
    //   735: iconst_0
    //   736: istore 5
    //   738: aconst_null
    //   739: astore 26
    //   741: aload 26
    //   743: ifnull +8 -> 751
    //   746: aload 26
    //   748: invokevirtual 307	okhttp3/ad:close	()V
    //   751: aload 10
    //   753: athrow
    //   754: iconst_0
    //   755: istore 5
    //   757: aconst_null
    //   758: astore 26
    //   760: aload 26
    //   762: ifnull +8 -> 770
    //   765: aload 26
    //   767: invokevirtual 307	okhttp3/ad:close	()V
    //   770: iconst_5
    //   771: istore 5
    //   773: goto -495 -> 278
    //   776: iconst_0
    //   777: istore 4
    //   779: aconst_null
    //   780: astore 10
    //   782: aload 10
    //   784: ifnull +586 -> 1370
    //   787: aload 10
    //   789: arraylength
    //   790: istore 5
    //   792: iload 5
    //   794: ifne +6 -> 800
    //   797: goto +573 -> 1370
    //   800: aload_3
    //   801: getfield 146	com/truecaller/messaging/transport/mms/MmsRequest:a	Z
    //   804: istore 5
    //   806: iload 5
    //   808: ifeq +159 -> 967
    //   811: aload_3
    //   812: getfield 150	com/truecaller/messaging/transport/mms/MmsRequest:b	Landroid/net/Uri;
    //   815: astore 26
    //   817: aload_2
    //   818: getfield 22	com/truecaller/messaging/transport/mms/ah:a	Landroid/content/Context;
    //   821: astore 12
    //   823: aload 12
    //   825: invokevirtual 104	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   828: astore 12
    //   830: ldc_w 309
    //   833: astore 15
    //   835: aload 12
    //   837: aload 26
    //   839: aload 15
    //   841: invokevirtual 112	android/content/ContentResolver:openFileDescriptor	(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    //   844: astore 26
    //   846: new 311	android/os/ParcelFileDescriptor$AutoCloseOutputStream
    //   849: astore 12
    //   851: aload 12
    //   853: aload 26
    //   855: invokespecial 312	android/os/ParcelFileDescriptor$AutoCloseOutputStream:<init>	(Landroid/os/ParcelFileDescriptor;)V
    //   858: aload 12
    //   860: aload 10
    //   862: invokevirtual 315	android/os/ParcelFileDescriptor$AutoCloseOutputStream:write	([B)V
    //   865: aload 12
    //   867: invokevirtual 318	android/os/ParcelFileDescriptor$AutoCloseOutputStream:flush	()V
    //   870: aload 12
    //   872: invokestatic 141	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   875: aload_3
    //   876: getfield 156	com/truecaller/messaging/transport/mms/MmsRequest:d	Landroid/app/PendingIntent;
    //   879: astore 10
    //   881: aload_2
    //   882: aload 10
    //   884: iload 7
    //   886: iload 7
    //   888: aconst_null
    //   889: invokespecial 159	com/truecaller/messaging/transport/mms/ah:a	(Landroid/app/PendingIntent;II[B)V
    //   892: aload_2
    //   893: getfield 28	com/truecaller/messaging/transport/mms/ah:c	Lcom/truecaller/messaging/transport/mms/z;
    //   896: aload 11
    //   898: invokeinterface 180 2 0
    //   903: iload 6
    //   905: ireturn
    //   906: astore 10
    //   908: goto +11 -> 919
    //   911: astore 10
    //   913: iconst_0
    //   914: istore 13
    //   916: aconst_null
    //   917: astore 12
    //   919: aload 12
    //   921: invokestatic 141	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   924: aload 10
    //   926: athrow
    //   927: astore 10
    //   929: aload 10
    //   931: invokestatic 165	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   934: aload_3
    //   935: getfield 156	com/truecaller/messaging/transport/mms/MmsRequest:d	Landroid/app/PendingIntent;
    //   938: astore 10
    //   940: iconst_5
    //   941: istore 14
    //   943: aload_2
    //   944: aload 10
    //   946: iload 14
    //   948: iload 7
    //   950: aconst_null
    //   951: invokespecial 159	com/truecaller/messaging/transport/mms/ah:a	(Landroid/app/PendingIntent;II[B)V
    //   954: aload_2
    //   955: getfield 28	com/truecaller/messaging/transport/mms/ah:c	Lcom/truecaller/messaging/transport/mms/z;
    //   958: aload 11
    //   960: invokeinterface 180 2 0
    //   965: iconst_0
    //   966: ireturn
    //   967: new 320	com/android/a/a/a/n
    //   970: astore 26
    //   972: aload_3
    //   973: getfield 323	com/truecaller/messaging/transport/mms/MmsRequest:f	Z
    //   976: istore 13
    //   978: aload 26
    //   980: aload 10
    //   982: iload 13
    //   984: invokespecial 326	com/android/a/a/a/n:<init>	([BZ)V
    //   987: aload 26
    //   989: invokevirtual 329	com/android/a/a/a/n:a	()Lcom/android/a/a/a/f;
    //   992: astore 26
    //   994: aload 26
    //   996: ifnull +269 -> 1265
    //   999: aload 26
    //   1001: instanceof 331
    //   1004: istore 13
    //   1006: iload 13
    //   1008: ifne +6 -> 1014
    //   1011: goto +254 -> 1265
    //   1014: aload 10
    //   1016: arraylength
    //   1017: istore 13
    //   1019: ldc_w 332
    //   1022: istore 17
    //   1024: iload 13
    //   1026: iload 17
    //   1028: if_icmplt +86 -> 1114
    //   1031: iload 6
    //   1033: anewarray 82	java/lang/String
    //   1036: astore 26
    //   1038: new 209	java/lang/StringBuilder
    //   1041: astore 27
    //   1043: ldc_w 335
    //   1046: astore 12
    //   1048: aload 27
    //   1050: aload 12
    //   1052: invokespecial 213	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1055: aload 10
    //   1057: arraylength
    //   1058: istore 4
    //   1060: aload 27
    //   1062: iload 4
    //   1064: invokevirtual 233	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1067: pop
    //   1068: aload 27
    //   1070: invokevirtual 238	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1073: astore 10
    //   1075: aload 26
    //   1077: iconst_0
    //   1078: aload 10
    //   1080: aastore
    //   1081: aload_3
    //   1082: getfield 156	com/truecaller/messaging/transport/mms/MmsRequest:d	Landroid/app/PendingIntent;
    //   1085: astore 10
    //   1087: iconst_5
    //   1088: istore 14
    //   1090: aload_2
    //   1091: aload 10
    //   1093: iload 14
    //   1095: iload 7
    //   1097: aconst_null
    //   1098: invokespecial 159	com/truecaller/messaging/transport/mms/ah:a	(Landroid/app/PendingIntent;II[B)V
    //   1101: aload_2
    //   1102: getfield 28	com/truecaller/messaging/transport/mms/ah:c	Lcom/truecaller/messaging/transport/mms/z;
    //   1105: aload 11
    //   1107: invokeinterface 180 2 0
    //   1112: iconst_0
    //   1113: ireturn
    //   1114: aload 26
    //   1116: checkcast 331	com/android/a/a/a/t
    //   1119: astore 26
    //   1121: aload 26
    //   1123: getfield 338	com/android/a/a/a/t:a	Lcom/android/a/a/a/m;
    //   1126: astore 26
    //   1128: sipush 146
    //   1131: istore 8
    //   1133: aload 26
    //   1135: iload 8
    //   1137: invokevirtual 344	com/android/a/a/a/m:a	(I)I
    //   1140: istore 5
    //   1142: sipush 227
    //   1145: istore 8
    //   1147: iload 5
    //   1149: iload 8
    //   1151: if_icmpeq +48 -> 1199
    //   1154: sipush 132
    //   1157: istore 8
    //   1159: iload 5
    //   1161: iload 8
    //   1163: if_icmpne +6 -> 1169
    //   1166: goto +33 -> 1199
    //   1169: aload_3
    //   1170: getfield 156	com/truecaller/messaging/transport/mms/MmsRequest:d	Landroid/app/PendingIntent;
    //   1173: astore_3
    //   1174: aload_2
    //   1175: aload_3
    //   1176: iload 7
    //   1178: iload 7
    //   1180: aload 10
    //   1182: invokespecial 159	com/truecaller/messaging/transport/mms/ah:a	(Landroid/app/PendingIntent;II[B)V
    //   1185: aload_2
    //   1186: getfield 28	com/truecaller/messaging/transport/mms/ah:c	Lcom/truecaller/messaging/transport/mms/z;
    //   1189: aload 11
    //   1191: invokeinterface 180 2 0
    //   1196: iload 6
    //   1198: ireturn
    //   1199: iload 6
    //   1201: anewarray 82	java/lang/String
    //   1204: astore 27
    //   1206: ldc_w 348
    //   1209: astore 9
    //   1211: iload 5
    //   1213: invokestatic 351	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   1216: astore 26
    //   1218: aload 9
    //   1220: aload 26
    //   1222: invokevirtual 92	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   1225: astore 26
    //   1227: aload 27
    //   1229: iconst_0
    //   1230: aload 26
    //   1232: aastore
    //   1233: aload_3
    //   1234: getfield 156	com/truecaller/messaging/transport/mms/MmsRequest:d	Landroid/app/PendingIntent;
    //   1237: astore_3
    //   1238: iconst_5
    //   1239: istore 5
    //   1241: aload_2
    //   1242: aload_3
    //   1243: iload 5
    //   1245: iload 7
    //   1247: aload 10
    //   1249: invokespecial 159	com/truecaller/messaging/transport/mms/ah:a	(Landroid/app/PendingIntent;II[B)V
    //   1252: aload_2
    //   1253: getfield 28	com/truecaller/messaging/transport/mms/ah:c	Lcom/truecaller/messaging/transport/mms/z;
    //   1256: aload 11
    //   1258: invokeinterface 180 2 0
    //   1263: iconst_0
    //   1264: ireturn
    //   1265: iload 6
    //   1267: anewarray 82	java/lang/String
    //   1270: astore 10
    //   1272: new 209	java/lang/StringBuilder
    //   1275: astore 27
    //   1277: ldc_w 353
    //   1280: astore 12
    //   1282: aload 27
    //   1284: aload 12
    //   1286: invokespecial 213	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1289: aload 26
    //   1291: ifnonnull +11 -> 1302
    //   1294: ldc_w 355
    //   1297: astore 26
    //   1299: goto +17 -> 1316
    //   1302: aload 26
    //   1304: invokevirtual 359	com/android/a/a/a/f:a	()I
    //   1307: istore 5
    //   1309: iload 5
    //   1311: invokestatic 364	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   1314: astore 26
    //   1316: aload 27
    //   1318: aload 26
    //   1320: invokevirtual 220	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1323: pop
    //   1324: aload 27
    //   1326: invokevirtual 238	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1329: astore 26
    //   1331: aload 10
    //   1333: iconst_0
    //   1334: aload 26
    //   1336: aastore
    //   1337: aload_3
    //   1338: getfield 156	com/truecaller/messaging/transport/mms/MmsRequest:d	Landroid/app/PendingIntent;
    //   1341: astore 10
    //   1343: iconst_5
    //   1344: istore 14
    //   1346: aload_2
    //   1347: aload 10
    //   1349: iload 14
    //   1351: iload 7
    //   1353: aconst_null
    //   1354: invokespecial 159	com/truecaller/messaging/transport/mms/ah:a	(Landroid/app/PendingIntent;II[B)V
    //   1357: aload_2
    //   1358: getfield 28	com/truecaller/messaging/transport/mms/ah:c	Lcom/truecaller/messaging/transport/mms/z;
    //   1361: aload 11
    //   1363: invokeinterface 180 2 0
    //   1368: iconst_0
    //   1369: ireturn
    //   1370: aload_3
    //   1371: getfield 156	com/truecaller/messaging/transport/mms/MmsRequest:d	Landroid/app/PendingIntent;
    //   1374: astore 10
    //   1376: iconst_5
    //   1377: istore 14
    //   1379: aload_2
    //   1380: aload 10
    //   1382: iload 14
    //   1384: iload 17
    //   1386: aconst_null
    //   1387: invokespecial 159	com/truecaller/messaging/transport/mms/ah:a	(Landroid/app/PendingIntent;II[B)V
    //   1390: aload_2
    //   1391: getfield 28	com/truecaller/messaging/transport/mms/ah:c	Lcom/truecaller/messaging/transport/mms/z;
    //   1394: aload 11
    //   1396: invokeinterface 180 2 0
    //   1401: iconst_0
    //   1402: ireturn
    //   1403: astore 10
    //   1405: aload_2
    //   1406: getfield 28	com/truecaller/messaging/transport/mms/ah:c	Lcom/truecaller/messaging/transport/mms/z;
    //   1409: aload 11
    //   1411: invokeinterface 180 2 0
    //   1416: aload 10
    //   1418: athrow
    //   1419: pop
    //   1420: goto -666 -> 754
    //   1423: pop
    //   1424: goto -664 -> 760
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1427	0	this	ah
    //   0	1427	1	paramMmsRequest	MmsRequest
    //   1	1405	2	localah	ah
    //   3	1368	3	localObject1	Object
    //   8	770	4	bool1	boolean
    //   1058	5	4	i	int
    //   11	782	5	j	int
    //   804	3	5	bool2	boolean
    //   1140	170	5	k	int
    //   14	1252	6	m	int
    //   17	1335	7	n	int
    //   20	1144	8	i1	int
    //   23	1196	9	str	String
    //   34	680	10	localObject2	Object
    //   728	1	10	localObject3	Object
    //   733	19	10	localObject4	Object
    //   780	103	10	localObject5	Object
    //   906	1	10	localObject6	Object
    //   911	14	10	localObject7	Object
    //   927	3	10	localIOException1	java.io.IOException
    //   938	443	10	localObject8	Object
    //   1403	14	10	localObject9	Object
    //   100	1310	11	localObject10	Object
    //   127	1158	12	localObject11	Object
    //   138	869	13	bool3	boolean
    //   1017	12	13	i2	int
    //   153	1230	14	i3	int
    //   189	651	15	localObject12	Object
    //   195	514	16	localObject13	Object
    //   226	3	17	bool4	boolean
    //   276	1109	17	i4	int
    //   285	3	18	bool5	boolean
    //   558	7	18	i5	int
    //   627	3	18	bool6	boolean
    //   313	371	19	localObject14	Object
    //   318	350	20	localObject15	Object
    //   322	284	21	localObject16	Object
    //   396	5	22	i6	int
    //   475	3	23	bool7	boolean
    //   529	3	24	bool8	boolean
    //   544	28	25	localType	java.net.Proxy.Type
    //   549	786	26	localObject17	Object
    //   1041	284	27	localObject18	Object
    //   68	1	43	localIOException2	java.io.IOException
    //   1419	1	44	localIOException3	java.io.IOException
    //   1423	1	45	localIOException4	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   30	34	68	java/io/IOException
    //   37	42	68	java/io/IOException
    //   49	53	68	java/io/IOException
    //   62	66	68	java/io/IOException
    //   622	627	728	finally
    //   634	639	728	finally
    //   641	644	728	finally
    //   653	658	728	finally
    //   658	663	728	finally
    //   667	673	728	finally
    //   673	678	728	finally
    //   683	686	728	finally
    //   686	691	728	finally
    //   701	706	728	finally
    //   708	713	728	finally
    //   609	613	733	finally
    //   615	620	733	finally
    //   860	865	906	finally
    //   865	870	906	finally
    //   839	844	911	finally
    //   846	849	911	finally
    //   853	858	911	finally
    //   811	815	927	java/io/IOException
    //   817	821	927	java/io/IOException
    //   823	828	927	java/io/IOException
    //   870	875	927	java/io/IOException
    //   919	924	927	java/io/IOException
    //   924	927	927	java/io/IOException
    //   123	127	1403	finally
    //   131	138	1403	finally
    //   145	149	1403	finally
    //   162	166	1403	finally
    //   179	183	1403	finally
    //   185	189	1403	finally
    //   191	195	1403	finally
    //   197	204	1403	finally
    //   210	217	1403	finally
    //   219	226	1403	finally
    //   233	237	1403	finally
    //   249	253	1403	finally
    //   266	273	1403	finally
    //   278	285	1403	finally
    //   292	299	1403	finally
    //   301	306	1403	finally
    //   308	313	1403	finally
    //   315	318	1403	finally
    //   326	331	1403	finally
    //   331	338	1403	finally
    //   342	348	1403	finally
    //   354	360	1403	finally
    //   360	367	1403	finally
    //   371	377	1403	finally
    //   383	389	1403	finally
    //   389	396	1403	finally
    //   400	406	1403	finally
    //   412	418	1403	finally
    //   418	423	1403	finally
    //   428	431	1403	finally
    //   431	435	1403	finally
    //   442	449	1403	finally
    //   454	458	1403	finally
    //   460	464	1403	finally
    //   470	475	1403	finally
    //   482	486	1403	finally
    //   488	495	1403	finally
    //   502	507	1403	finally
    //   509	516	1403	finally
    //   518	522	1403	finally
    //   524	529	1403	finally
    //   536	539	1403	finally
    //   541	544	1403	finally
    //   546	549	1403	finally
    //   551	558	1403	finally
    //   564	569	1403	finally
    //   573	578	1403	finally
    //   578	582	1403	finally
    //   584	589	1403	finally
    //   593	598	1403	finally
    //   598	603	1403	finally
    //   720	725	1403	finally
    //   746	751	1403	finally
    //   751	754	1403	finally
    //   765	770	1403	finally
    //   787	790	1403	finally
    //   800	804	1403	finally
    //   811	815	1403	finally
    //   817	821	1403	finally
    //   823	828	1403	finally
    //   870	875	1403	finally
    //   875	879	1403	finally
    //   888	892	1403	finally
    //   919	924	1403	finally
    //   924	927	1403	finally
    //   929	934	1403	finally
    //   934	938	1403	finally
    //   950	954	1403	finally
    //   967	970	1403	finally
    //   972	976	1403	finally
    //   982	987	1403	finally
    //   987	992	1403	finally
    //   1014	1017	1403	finally
    //   1031	1036	1403	finally
    //   1038	1041	1403	finally
    //   1050	1055	1403	finally
    //   1055	1058	1403	finally
    //   1062	1068	1403	finally
    //   1068	1073	1403	finally
    //   1078	1081	1403	finally
    //   1081	1085	1403	finally
    //   1097	1101	1403	finally
    //   1114	1119	1403	finally
    //   1121	1126	1403	finally
    //   1135	1140	1403	finally
    //   1169	1173	1403	finally
    //   1180	1185	1403	finally
    //   1199	1204	1403	finally
    //   1211	1216	1403	finally
    //   1220	1225	1403	finally
    //   1230	1233	1403	finally
    //   1233	1237	1403	finally
    //   1247	1252	1403	finally
    //   1265	1270	1403	finally
    //   1272	1275	1403	finally
    //   1284	1289	1403	finally
    //   1302	1307	1403	finally
    //   1309	1314	1403	finally
    //   1318	1324	1403	finally
    //   1324	1329	1403	finally
    //   1334	1337	1403	finally
    //   1337	1341	1403	finally
    //   1353	1357	1403	finally
    //   1370	1374	1403	finally
    //   1386	1390	1403	finally
    //   609	613	1419	java/io/IOException
    //   615	620	1419	java/io/IOException
    //   622	627	1423	java/io/IOException
    //   634	639	1423	java/io/IOException
    //   641	644	1423	java/io/IOException
    //   653	658	1423	java/io/IOException
    //   658	663	1423	java/io/IOException
    //   667	673	1423	java/io/IOException
    //   673	678	1423	java/io/IOException
    //   683	686	1423	java/io/IOException
    //   686	691	1423	java/io/IOException
    //   701	706	1423	java/io/IOException
    //   708	713	1423	java/io/IOException
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */