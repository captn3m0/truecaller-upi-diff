package com.truecaller.messaging.transport.mms;

import android.app.PendingIntent;
import android.net.Uri;

public abstract interface k
{
  public abstract boolean a(Uri paramUri, String paramString, PendingIntent paramPendingIntent);
  
  public abstract boolean a(String paramString, Uri paramUri, PendingIntent paramPendingIntent);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */