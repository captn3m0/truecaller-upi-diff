package com.truecaller.messaging.transport.mms;

import android.database.Cursor;
import android.net.Uri;

public abstract interface h$a
  extends Cursor
{
  public abstract String a();
  
  public abstract String b();
  
  public abstract Uri c();
  
  public abstract String d();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.h.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */