package com.truecaller.messaging.transport.mms;

import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d
{
  private final o a;
  private final Provider b;
  
  private p(o paramo, Provider paramProvider)
  {
    a = paramo;
    b = paramProvider;
  }
  
  public static p a(o paramo, Provider paramProvider)
  {
    p localp = new com/truecaller/messaging/transport/mms/p;
    localp.<init>(paramo, paramProvider);
    return localp;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */