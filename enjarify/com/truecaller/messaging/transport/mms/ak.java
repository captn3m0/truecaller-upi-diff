package com.truecaller.messaging.transport.mms;

import android.content.Context;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.a;
import com.truecaller.multisim.ac;
import com.truecaller.multisim.h;
import com.truecaller.multisim.o;

public final class ak
{
  private final Context a;
  private final i b;
  
  public ak(Context paramContext, i parami)
  {
    a = paramContext;
    b = parami;
  }
  
  public final f a(String paramString, h paramh)
  {
    c.g.b.k.b(paramString, "simToken");
    Object localObject1 = "multiSimManager";
    c.g.b.k.b(paramh, (String)localObject1);
    boolean bool1 = paramh instanceof com.truecaller.multisim.l;
    if (!bool1)
    {
      boolean bool2 = paramh instanceof o;
      if (!bool2)
      {
        bool2 = paramh instanceof ac;
        if (!bool2)
        {
          localObject1 = paramh.b(paramString);
          paramString = paramh.c(paramString);
          c.g.b.k.a(paramString, "multiSimManager.getCarrierConfiguration(simToken)");
          paramh = new com/truecaller/messaging/transport/mms/al;
          localObject2 = a;
          paramh.<init>((Context)localObject2, (SimInfo)localObject1, paramString);
          paramString = b.a(ai.class, paramh);
          c.g.b.k.a(paramString, "mThread.bind(MmsSender::class.java, sender)");
          return paramString;
        }
      }
    }
    Object localObject2 = ((h)paramh).b(paramString);
    a locala = ((h)paramh).c(paramString);
    c.g.b.k.a(locala, "multiSimManager.getCarrierConfiguration(simToken)");
    Object localObject3 = l.a;
    localObject3 = a;
    c.g.b.k.b(localObject3, "context");
    c.g.b.k.b(paramh, "multiSimManager");
    String str = "simToken";
    c.g.b.k.b(paramString, str);
    if (!bool1)
    {
      bool1 = paramh instanceof o;
      if (!bool1)
      {
        bool1 = paramh instanceof ac;
        if (bool1)
        {
          localObject1 = new com/truecaller/messaging/transport/mms/aq;
          paramh = ((h)paramh).f(paramString);
          str = "multiSimManager.getSmsManager(simToken)";
          c.g.b.k.a(paramh, str);
          ((aq)localObject1).<init>((Context)localObject3, paramString, paramh);
          localObject1 = (k)localObject1;
          break label330;
        }
        paramString = new java/lang/IllegalArgumentException;
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        paramh = paramh.getClass().getCanonicalName();
        ((StringBuilder)localObject1).append(paramh);
        ((StringBuilder)localObject1).append(" is not supported");
        paramh = ((StringBuilder)localObject1).toString();
        paramString.<init>(paramh);
        throw ((Throwable)paramString);
      }
    }
    localObject1 = new com/truecaller/messaging/transport/mms/g;
    paramString = ((h)paramh).f(paramString);
    paramh = "multiSimManager.getSmsManager(simToken)";
    c.g.b.k.a(paramString, paramh);
    ((g)localObject1).<init>((Context)localObject3, paramString);
    localObject1 = (k)localObject1;
    label330:
    paramString = new com/truecaller/messaging/transport/mms/am;
    paramh = a;
    paramString.<init>(paramh, (SimInfo)localObject2, locala, (k)localObject1);
    paramString = b.a(ai.class, paramString);
    c.g.b.k.a(paramString, "mThread.bind(MmsSender::class.java, sender)");
    return paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.ak
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */