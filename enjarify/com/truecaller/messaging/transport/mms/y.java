package com.truecaller.messaging.transport.mms;

import dagger.a.d;
import javax.inject.Provider;

public final class y
  implements d
{
  private final n a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  
  private y(n paramn, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    a = paramn;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
  }
  
  public static y a(n paramn, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5)
  {
    y localy = new com/truecaller/messaging/transport/mms/y;
    localy.<init>(paramn, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5);
    return localy;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */