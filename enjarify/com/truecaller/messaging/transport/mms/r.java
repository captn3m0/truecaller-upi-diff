package com.truecaller.messaging.transport.mms;

import dagger.a.d;
import javax.inject.Provider;

public final class r
  implements d
{
  private final o a;
  private final Provider b;
  private final Provider c;
  
  private r(o paramo, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramo;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static r a(o paramo, Provider paramProvider1, Provider paramProvider2)
  {
    r localr = new com/truecaller/messaging/transport/mms/r;
    localr.<init>(paramo, paramProvider1, paramProvider2);
    return localr;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.mms.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */