package com.truecaller.messaging.transport;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.truecaller.messaging.data.types.TransportInfo;
import org.a.a.b;

public class NullTransportInfo
  implements TransportInfo
{
  public static final Parcelable.Creator CREATOR;
  public static final NullTransportInfo b;
  public final long a;
  
  static
  {
    Object localObject = new com/truecaller/messaging/transport/NullTransportInfo;
    ((NullTransportInfo)localObject).<init>();
    b = (NullTransportInfo)localObject;
    localObject = new com/truecaller/messaging/transport/NullTransportInfo$1;
    ((NullTransportInfo.1)localObject).<init>();
    CREATOR = (Parcelable.Creator)localObject;
  }
  
  private NullTransportInfo()
  {
    a = -1;
  }
  
  private NullTransportInfo(Parcel paramParcel)
  {
    long l = paramParcel.readLong();
    a = l;
  }
  
  private NullTransportInfo(NullTransportInfo.a parama)
  {
    long l = a;
    a = l;
  }
  
  public final int a()
  {
    return 0;
  }
  
  public final String a(b paramb)
  {
    return "";
  }
  
  public final int b()
  {
    return 0;
  }
  
  public final long c()
  {
    return a;
  }
  
  public final long d()
  {
    return a;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public final long e()
  {
    return -1;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    if (paramObject != null)
    {
      Class localClass1 = getClass();
      Class localClass2 = paramObject.getClass();
      if (localClass1 == localClass2)
      {
        paramObject = (NullTransportInfo)paramObject;
        long l1 = a;
        long l2 = a;
        boolean bool2 = l1 < l2;
        if (!bool2) {
          return bool1;
        }
        return false;
      }
    }
    return false;
  }
  
  public final boolean f()
  {
    return false;
  }
  
  public int hashCode()
  {
    long l1 = a;
    long l2 = l1 >>> 32;
    return (int)(l1 ^ l2);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("{ type : null, messageId: ");
    long l = a;
    localStringBuilder.append(l);
    localStringBuilder.append(" }");
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    long l = a;
    paramParcel.writeLong(l);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.NullTransportInfo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */