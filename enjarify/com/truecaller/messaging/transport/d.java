package com.truecaller.messaging.transport;

import android.content.Intent;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;

public abstract interface d
{
  public abstract w a(Message paramMessage, Participant[] paramArrayOfParticipant, int paramInt1, int paramInt2);
  
  public abstract void a(Message paramMessage);
  
  public abstract void a(l paraml, Intent paramIntent, int paramInt);
  
  public abstract void b(Message paramMessage);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */