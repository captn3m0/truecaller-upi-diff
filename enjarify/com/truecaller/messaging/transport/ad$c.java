package com.truecaller.messaging.transport;

import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import java.util.List;

final class ad$c
  implements ad.b
{
  private final ContentResolver a;
  private boolean b = true;
  
  ad$c(ContentResolver paramContentResolver)
  {
    a = paramContentResolver;
  }
  
  public final ContentProviderResult[] a(ad paramad)
  {
    boolean bool1 = b;
    int i = 0;
    if (bool1) {
      try
      {
        localObject1 = a;
        localObject1 = paramad.a((ContentResolver)localObject1);
        if (localObject1 != null) {
          return (ContentProviderResult[])localObject1;
        }
        localObject1 = "Batch returned null result";
        new String[1][0] = localObject1;
        b = false;
      }
      catch (RemoteException localRemoteException)
      {
        AssertionUtil.reportThrowableButNeverCrash(localRemoteException);
        return ad.d();
      }
      catch (SecurityException|NullPointerException|OperationApplicationException localSecurityException)
      {
        b = false;
      }
    }
    Object localObject1 = a;
    Object localObject2 = c;
    if (localObject2 != null)
    {
      localObject2 = c;
      boolean bool2 = ((List)localObject2).isEmpty();
      if (!bool2)
      {
        int j = c.size();
        localObject2 = new ContentProviderResult[j];
        List localList = c;
        int k = localList.size();
        while (i < k)
        {
          Object localObject3 = (ad.a)c.get(i);
          int m = a;
          Object localObject4;
          Object localObject5;
          int n;
          switch (m)
          {
          default: 
            AssertionUtil.AlwaysFatal.fail(new String[] { "Unsupported operation" });
            return ad.a;
          case 2: 
            localObject4 = b;
            localObject5 = d;
            localObject3 = e;
            n = ((ContentResolver)localObject1).delete((Uri)localObject4, (String)localObject5, (String[])localObject3);
            localObject4 = new android/content/ContentProviderResult;
            ((ContentProviderResult)localObject4).<init>(n);
            localObject2[i] = localObject4;
            break;
          case 1: 
            localObject4 = b;
            localObject5 = c;
            String str = d;
            localObject3 = e;
            n = ((ContentResolver)localObject1).update((Uri)localObject4, (ContentValues)localObject5, str, (String[])localObject3);
            localObject4 = new android/content/ContentProviderResult;
            ((ContentProviderResult)localObject4).<init>(n);
            localObject2[i] = localObject4;
            break;
          case 0: 
            localObject4 = b;
            localObject3 = c;
            localObject3 = ((ContentResolver)localObject1).insert((Uri)localObject4, (ContentValues)localObject3);
            localObject4 = new android/content/ContentProviderResult;
            ((ContentProviderResult)localObject4).<init>((Uri)localObject3);
            localObject2[i] = localObject4;
          }
          i += 1;
        }
        return (ContentProviderResult[])localObject2;
      }
    }
    return ad.a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.ad.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */