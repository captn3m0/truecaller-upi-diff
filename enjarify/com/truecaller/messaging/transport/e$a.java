package com.truecaller.messaging.transport;

import android.content.Intent;
import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class e$a
  extends u
{
  private final l b;
  private final Intent c;
  private final int d;
  
  private e$a(e parame, l paraml, Intent paramIntent, int paramInt)
  {
    super(parame);
    b = paraml;
    c = paramIntent;
    d = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".deliverIntentToTransport(");
    Object localObject = b;
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(d), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */