package com.truecaller.messaging.transport;

import dagger.a.d;
import javax.inject.Provider;

public final class t
  implements d
{
  private final o a;
  private final Provider b;
  private final Provider c;
  
  private t(o paramo, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramo;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static t a(o paramo, Provider paramProvider1, Provider paramProvider2)
  {
    t localt = new com/truecaller/messaging/transport/t;
    localt.<init>(paramo, paramProvider1, paramProvider2);
    return localt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */