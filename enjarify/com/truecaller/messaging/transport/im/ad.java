package com.truecaller.messaging.transport.im;

import dagger.a.d;
import javax.inject.Provider;

public final class ad
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private ad(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static ad a(Provider paramProvider1, Provider paramProvider2)
  {
    ad localad = new com/truecaller/messaging/transport/im/ad;
    localad.<init>(paramProvider1, paramProvider2);
    return localad;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */