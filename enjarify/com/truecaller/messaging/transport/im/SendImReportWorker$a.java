package com.truecaller.messaging.transport.im;

import androidx.work.a;
import androidx.work.c.a;
import androidx.work.e.a;
import androidx.work.j;
import androidx.work.k.a;
import com.truecaller.api.services.messenger.v1.models.input.InputReportType;
import java.util.concurrent.TimeUnit;

public final class SendImReportWorker$a
{
  public static androidx.work.k a(InputReportType paramInputReportType, String paramString1, String paramString2, String paramString3)
  {
    c.g.b.k.b(paramInputReportType, "reportType");
    c.g.b.k.b(paramString1, "rawMessageId");
    c.g.b.k.b(paramString2, "imPeerId");
    k.a locala = new androidx/work/k$a;
    locala.<init>(SendImReportWorker.class);
    Object localObject = a.a;
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    locala = (k.a)locala.a((a)localObject, 30, localTimeUnit);
    localObject = new androidx/work/e$a;
    ((e.a)localObject).<init>();
    int i = paramInputReportType.getNumber();
    paramInputReportType = ((e.a)localObject).a("report_type", i).a("raw_message_id", paramString1).a("im_peer_id", paramString2).a("im_group_id", paramString3).a();
    paramInputReportType = (k.a)((k.a)locala.a(paramInputReportType)).a("send_im_report");
    paramString1 = new androidx/work/c$a;
    paramString1.<init>();
    paramString2 = j.b;
    paramString1 = paramString1.a(paramString2).a();
    paramInputReportType = ((k.a)paramInputReportType.a(paramString1)).c();
    c.g.b.k.a(paramInputReportType, "OneTimeWorkRequest.Build…\n                .build()");
    return (androidx.work.k)paramInputReportType;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.SendImReportWorker.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */