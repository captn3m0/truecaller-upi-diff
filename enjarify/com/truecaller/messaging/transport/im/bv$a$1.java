package com.truecaller.messaging.transport.im;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.api.services.messenger.v1.f.d;
import java.lang.ref.WeakReference;
import java.util.List;
import kotlinx.coroutines.ag;

final class bv$a$1
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  bv$a$1(bv.a parama, f.d paramd, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    1 local1 = new com/truecaller/messaging/transport/im/bv$a$1;
    bv.a locala = b;
    f.d locald = c;
    local1.<init>(locala, locald, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return local1;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = (ce)b.c.get();
        if (paramObject != null)
        {
          localObject = c;
          String str = "response";
          c.g.b.k.a(localObject, str);
          localObject = ((f.d)localObject).a();
          ((ce)paramObject).a((List)localObject);
        }
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (1)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((1)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bv.a.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */