package com.truecaller.messaging.transport.im;

import c.a.an;
import com.truecaller.messaging.conversation.ConversationActivity;
import com.truecaller.messaging.imgroupinfo.ImGroupInfoActivity;
import com.truecaller.messaging.imgroupinvitation.ImGroupInvitationActivity;
import com.truecaller.ui.TruecallerInit;
import java.util.Set;

public final class ax
{
  private static final Set a;
  
  static
  {
    Class[] arrayOfClass = new Class[4];
    arrayOfClass[0] = TruecallerInit.class;
    arrayOfClass[1] = ConversationActivity.class;
    arrayOfClass[2] = ImGroupInfoActivity.class;
    arrayOfClass[3] = ImGroupInvitationActivity.class;
    a = an.a(arrayOfClass);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ax
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */