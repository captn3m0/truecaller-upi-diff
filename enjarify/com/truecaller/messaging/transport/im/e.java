package com.truecaller.messaging.transport.im;

import android.os.Message;
import io.grpc.c.f;

final class e
  implements f
{
  private final ch a;
  
  public e(ch paramch)
  {
    a = paramch;
  }
  
  private final void a(int paramInt, Object paramObject)
  {
    ch localch = a;
    Message localMessage = localch.obtainMessage(paramInt, paramObject);
    localch.sendMessage(localMessage);
  }
  
  public final void a()
  {
    a(3, null);
  }
  
  public final void a(Throwable paramThrowable)
  {
    a(2, paramThrowable);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */