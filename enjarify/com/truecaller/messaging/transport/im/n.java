package com.truecaller.messaging.transport.im;

import android.content.Intent;
import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.api.services.messenger.v1.events.Event;
import com.truecaller.api.services.messenger.v1.events.Event.PayloadCase;
import com.truecaller.api.services.messenger.v1.events.Event.h;
import com.truecaller.api.services.messenger.v1.events.Event.l;
import com.truecaller.messaging.h;
import dagger.a;

public final class n
  implements m
{
  private final bu a;
  private final a b;
  private final h c;
  private final b d;
  
  public n(bu parambu, a parama, h paramh, b paramb)
  {
    a = parambu;
    b = parama;
    c = paramh;
    d = paramb;
  }
  
  private final void b(Event paramEvent, boolean paramBoolean)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("event");
    paramEvent = paramEvent.toByteArray();
    localIntent.putExtra("event", paramEvent);
    localIntent.putExtra("from_push", paramBoolean);
    ((com.truecaller.messaging.transport.m)b.get()).a(2, localIntent, 0);
  }
  
  public final ProcessResult a(Event paramEvent, boolean paramBoolean)
  {
    k.b(paramEvent, "event");
    Object localObject1 = paramEvent.a();
    Object localObject2 = Event.PayloadCase.INCOMPATIBLE_EVENT;
    if (localObject1 == localObject2)
    {
      localObject1 = paramEvent.l();
      k.a(localObject1, "event.original");
      int i = ((Event.l)localObject1).a();
      localObject2 = a;
      boolean bool1 = ((bu)localObject2).b(i);
      if (bool1)
      {
        paramEvent = paramEvent.l();
        k.a(paramEvent, "event.original");
        paramEvent = Event.a(paramEvent.b());
        localObject1 = "Event.parseFrom(event.original.event)";
        k.a(paramEvent, (String)localObject1);
        b(paramEvent, paramBoolean);
      }
      else
      {
        Object localObject3 = paramEvent.l();
        k.a(localObject3, "event.original");
        localObject3 = Event.a(((Event.l)localObject3).b());
        k.a(localObject3, "Event.parseFrom(event.original.event)");
        localObject2 = paramEvent.l();
        k.a(localObject2, "event.original");
        int j = ((Event.l)localObject2).a();
        Intent localIntent = new android/content/Intent;
        localIntent.<init>("unsupported_event");
        localObject3 = ((Event)localObject3).toByteArray();
        localIntent.putExtra("unsupported_event", (byte[])localObject3);
        localIntent.putExtra("api_version", j);
        localObject3 = (com.truecaller.messaging.transport.m)b.get();
        j = 2;
        ((com.truecaller.messaging.transport.m)localObject3).a(j, localIntent, 0);
        paramEvent = paramEvent.k();
        localObject3 = "event.incompatibleEvent";
        k.a(paramEvent, (String)localObject3);
        boolean bool2 = paramEvent.a();
        if (!bool2)
        {
          c.h(i);
          paramEvent = d;
          localObject3 = new com/truecaller/analytics/e$a;
          ((e.a)localObject3).<init>("ImForceUpgradeEvent");
          int k = a.a();
          localObject3 = ((e.a)localObject3).a("ClientVersion", k).a("ApiVersion", i).a();
          k.a(localObject3, "AnalyticsEvent.Builder(I…\n                .build()");
          paramEvent.a((e)localObject3);
          return ProcessResult.FORCE_UPGRADE_ENCOUNTERED;
        }
        a.a(i);
        paramEvent = c;
        paramBoolean = true;
        paramEvent.i(paramBoolean);
      }
    }
    else
    {
      b(paramEvent, paramBoolean);
    }
    return ProcessResult.SUCCESS;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */