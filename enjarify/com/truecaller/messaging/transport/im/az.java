package com.truecaller.messaging.transport.im;

import android.os.HandlerThread;
import android.os.Looper;
import c.g.b.k;
import com.truecaller.messaging.conversation.bw;

public final class az
  implements ay
{
  io.grpc.c.f a;
  ch b;
  boolean c;
  final cb d;
  final m e;
  final bu f;
  final bw g;
  final com.truecaller.androidactors.f h;
  private final Runnable i;
  private HandlerThread j;
  private final com.truecaller.utils.a k;
  private final a l;
  
  public az(com.truecaller.utils.a parama, a parama1, cb paramcb, m paramm, bu parambu, bw parambw, com.truecaller.androidactors.f paramf)
  {
    k = parama;
    l = parama1;
    d = paramcb;
    e = paramm;
    f = parambu;
    g = parambw;
    h = paramf;
    parama = new com/truecaller/messaging/transport/im/az$b;
    parama.<init>(this);
    parama = (Runnable)parama;
    i = parama;
  }
  
  private final void d()
  {
    new String[1][0] = "Quitting thread";
    HandlerThread localHandlerThread = j;
    if (localHandlerThread == null)
    {
      String str = "thread";
      k.a(str);
    }
    localHandlerThread.quitSafely();
  }
  
  public final void a()
  {
    Object localObject1 = new android/os/HandlerThread;
    Object localObject2 = "im_subscription";
    ((HandlerThread)localObject1).<init>((String)localObject2);
    j = ((HandlerThread)localObject1);
    localObject1 = j;
    if (localObject1 == null)
    {
      localObject2 = "thread";
      k.a((String)localObject2);
    }
    ((HandlerThread)localObject1).start();
    localObject1 = new com/truecaller/messaging/transport/im/ch;
    localObject2 = j;
    if (localObject2 == null)
    {
      str = "thread";
      k.a(str);
    }
    localObject2 = ((HandlerThread)localObject2).getLooper();
    String str = "thread.looper";
    k.a(localObject2, str);
    ((ch)localObject1).<init>(this, (Looper)localObject2);
    b = ((ch)localObject1);
    localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "handler";
      k.a((String)localObject2);
    }
    localObject2 = i;
    ((ch)localObject1).post((Runnable)localObject2);
  }
  
  final void a(boolean paramBoolean)
  {
    a locala = null;
    a = null;
    boolean bool = c;
    if (!bool)
    {
      locala = l;
      com.truecaller.utils.a locala1 = k;
      long l1 = locala1.b();
      long l2 = locala.a(l1, paramBoolean);
      paramBoolean = true;
      Object localObject1 = new String[paramBoolean];
      Object localObject2 = null;
      Object localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>("Retrying subscription in ");
      ((StringBuilder)localObject3).append(l2);
      String str = " ms";
      ((StringBuilder)localObject3).append(str);
      localObject3 = ((StringBuilder)localObject3).toString();
      localObject1[0] = localObject3;
      localObject1 = b;
      if (localObject1 == null)
      {
        localObject2 = "handler";
        k.a((String)localObject2);
      }
      localObject2 = i;
      ((ch)localObject1).postDelayed((Runnable)localObject2, l2);
      return;
    }
    d();
  }
  
  public final void b()
  {
    ch localch = b;
    if (localch == null)
    {
      localObject = "handler";
      k.a((String)localObject);
    }
    Object localObject = new com/truecaller/messaging/transport/im/az$a;
    ((az.a)localObject).<init>(this);
    localObject = (Runnable)localObject;
    localch.post((Runnable)localObject);
  }
  
  final void c()
  {
    new String[1][0] = "Closing subscription";
    boolean bool = true;
    c = bool;
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "handler";
      k.a((String)localObject2);
    }
    Object localObject2 = i;
    ((ch)localObject1).removeCallbacks((Runnable)localObject2);
    localObject1 = a;
    if (localObject1 != null)
    {
      if (localObject1 != null) {
        ((io.grpc.c.f)localObject1).a();
      }
      return;
    }
    d();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.az
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */