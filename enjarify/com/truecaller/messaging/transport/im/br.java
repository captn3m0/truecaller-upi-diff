package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import c.a.ag;
import c.g.a.b;
import c.g.b.k;
import c.m.l;
import c.u;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.h.b;
import com.truecaller.api.services.messenger.v1.h.b.a;
import com.truecaller.api.services.messenger.v1.h.d;
import com.truecaller.api.services.messenger.v1.k.a;
import com.truecaller.api.services.messenger.v1.models.j;
import com.truecaller.content.TruecallerContract.y;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.h;
import com.truecaller.network.d.j.a;
import com.truecaller.util.al;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public final class br
  implements bp
{
  private final int a;
  private final int b;
  private final com.truecaller.utils.a c;
  private final cb d;
  private final ContentResolver e;
  private final bw f;
  private final al g;
  private final h h;
  private final com.truecaller.utils.i i;
  private final dagger.a j;
  
  public br(int paramInt1, int paramInt2, com.truecaller.utils.a parama, cb paramcb, ContentResolver paramContentResolver, bw parambw, al paramal, h paramh, com.truecaller.utils.i parami, dagger.a parama1)
  {
    a = paramInt1;
    b = paramInt2;
    c = parama;
    d = paramcb;
    e = paramContentResolver;
    f = parambw;
    g = paramal;
    h = paramh;
    i = parami;
    j = parama1;
  }
  
  private final void a(String paramString1, String paramString2, boolean paramBoolean)
  {
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    localContentValues.put("normalized_number", paramString1);
    localContentValues.put("im_peer_id", paramString2);
    long l = c.a();
    Object localObject = Long.valueOf(l);
    localContentValues.put("date", (Long)localObject);
    ContentResolver localContentResolver = e;
    localObject = TruecallerContract.y.a();
    localContentResolver.insert((Uri)localObject, localContentValues);
    boolean bool = e(paramString2);
    if (!bool)
    {
      paramString2 = new String[1];
      paramString2[0] = paramString1;
      paramString1 = (Collection)c.a.m.d(paramString2);
      b(paramString1, paramBoolean);
      return;
    }
    if (paramBoolean)
    {
      paramString1 = (Collection)c.a.m.a(paramString1);
      c(paramString1);
    }
  }
  
  /* Error */
  private final Collection b(Collection paramCollection)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore_2
    //   2: aload_1
    //   3: astore_3
    //   4: aload_0
    //   5: getfield 66	com/truecaller/messaging/transport/im/br:h	Lcom/truecaller/messaging/h;
    //   8: invokeinterface 138 1 0
    //   13: lstore 4
    //   15: lload 4
    //   17: invokestatic 97	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   20: astore 6
    //   22: aload 6
    //   24: astore 7
    //   26: aload 6
    //   28: checkcast 140	java/lang/Number
    //   31: astore 7
    //   33: aload 7
    //   35: invokevirtual 143	java/lang/Number:longValue	()J
    //   38: lstore 8
    //   40: lconst_0
    //   41: lstore 10
    //   43: lload 8
    //   45: lload 10
    //   47: lcmp
    //   48: istore 12
    //   50: iload 12
    //   52: ifle +9 -> 61
    //   55: iconst_1
    //   56: istore 13
    //   58: goto +9 -> 67
    //   61: iconst_0
    //   62: istore 13
    //   64: aconst_null
    //   65: astore 7
    //   67: aconst_null
    //   68: astore 14
    //   70: iload 13
    //   72: ifeq +6 -> 78
    //   75: goto +6 -> 81
    //   78: aconst_null
    //   79: astore 6
    //   81: aload 6
    //   83: ifnull +13 -> 96
    //   86: aload 6
    //   88: invokevirtual 144	java/lang/Long:longValue	()J
    //   91: lstore 4
    //   93: goto +8 -> 101
    //   96: invokestatic 147	com/truecaller/messaging/transport/im/bt:a	()J
    //   99: lstore 4
    //   101: aload_2
    //   102: getfield 56	com/truecaller/messaging/transport/im/br:c	Lcom/truecaller/utils/a;
    //   105: astore 15
    //   107: aload 15
    //   109: invokeinterface 91 1 0
    //   114: lstore 10
    //   116: lload 10
    //   118: lload 4
    //   120: lsub
    //   121: lstore 4
    //   123: aload_2
    //   124: getfield 60	com/truecaller/messaging/transport/im/br:e	Landroid/content/ContentResolver;
    //   127: astore 16
    //   129: invokestatic 105	com/truecaller/content/TruecallerContract$y:a	()Landroid/net/Uri;
    //   132: astore 17
    //   134: iconst_1
    //   135: anewarray 117	java/lang/String
    //   138: dup
    //   139: iconst_0
    //   140: ldc 78
    //   142: aastore
    //   143: astore 18
    //   145: new 149	java/lang/StringBuilder
    //   148: astore 19
    //   150: aload 19
    //   152: ldc -105
    //   154: invokespecial 154	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   157: aload_3
    //   158: astore 20
    //   160: aload_3
    //   161: checkcast 156	java/lang/Iterable
    //   164: astore 20
    //   166: getstatic 161	com/truecaller/messaging/transport/im/br$b:a	Lcom/truecaller/messaging/transport/im/br$b;
    //   169: astore 21
    //   171: aload 21
    //   173: astore 22
    //   175: aload 21
    //   177: checkcast 163	c/g/a/b
    //   180: astore 22
    //   182: bipush 31
    //   184: istore 23
    //   186: aload 20
    //   188: astore 21
    //   190: aload 20
    //   192: aconst_null
    //   193: aconst_null
    //   194: aconst_null
    //   195: iconst_0
    //   196: aconst_null
    //   197: aload 22
    //   199: iload 23
    //   201: invokestatic 167	c/a/m:a	(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lc/g/a/b;I)Ljava/lang/String;
    //   204: astore 21
    //   206: aload 19
    //   208: aload 21
    //   210: invokevirtual 171	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   213: pop
    //   214: ldc -83
    //   216: astore 21
    //   218: aload 19
    //   220: aload 21
    //   222: invokevirtual 171	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   225: pop
    //   226: aload 19
    //   228: invokevirtual 177	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   231: astore 19
    //   233: lload 4
    //   235: invokestatic 180	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   238: astore 6
    //   240: aload_3
    //   241: aload 6
    //   243: invokestatic 183	c/a/m:a	(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;
    //   246: checkcast 124	java/util/Collection
    //   249: astore_3
    //   250: lload 10
    //   252: invokestatic 180	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   255: astore 6
    //   257: aload_3
    //   258: aload 6
    //   260: invokestatic 183	c/a/m:a	(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;
    //   263: checkcast 124	java/util/Collection
    //   266: astore_3
    //   267: iconst_0
    //   268: anewarray 117	java/lang/String
    //   271: astore 6
    //   273: aload_3
    //   274: aload 6
    //   276: invokeinterface 187 2 0
    //   281: astore_3
    //   282: aload_3
    //   283: ifnull +162 -> 445
    //   286: aload_3
    //   287: checkcast 189	[Ljava/lang/String;
    //   290: astore_3
    //   291: aconst_null
    //   292: astore 21
    //   294: aload 20
    //   296: astore 6
    //   298: aload_3
    //   299: astore 20
    //   301: aload 16
    //   303: aload 17
    //   305: aload 18
    //   307: aload 19
    //   309: aload_3
    //   310: aconst_null
    //   311: invokevirtual 193	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   314: astore_3
    //   315: aload_3
    //   316: ifnull +101 -> 417
    //   319: aload_3
    //   320: astore 7
    //   322: aload_3
    //   323: checkcast 195	java/io/Closeable
    //   326: astore 7
    //   328: new 197	java/util/ArrayList
    //   331: astore 15
    //   333: aload 15
    //   335: invokespecial 198	java/util/ArrayList:<init>	()V
    //   338: aload 15
    //   340: checkcast 124	java/util/Collection
    //   343: astore 15
    //   345: aload_3
    //   346: invokeinterface 204 1 0
    //   351: istore 24
    //   353: iload 24
    //   355: ifeq +25 -> 380
    //   358: aload_3
    //   359: iconst_0
    //   360: invokeinterface 208 2 0
    //   365: astore 25
    //   367: aload 15
    //   369: aload 25
    //   371: invokeinterface 212 2 0
    //   376: pop
    //   377: goto -32 -> 345
    //   380: aload 15
    //   382: astore_3
    //   383: aload 15
    //   385: checkcast 214	java/util/List
    //   388: astore_3
    //   389: aload 7
    //   391: aconst_null
    //   392: invokestatic 219	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   395: goto +24 -> 419
    //   398: astore_3
    //   399: goto +9 -> 408
    //   402: astore_3
    //   403: aload_3
    //   404: astore 14
    //   406: aload_3
    //   407: athrow
    //   408: aload 7
    //   410: aload 14
    //   412: invokestatic 219	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   415: aload_3
    //   416: athrow
    //   417: aconst_null
    //   418: astore_3
    //   419: aload_3
    //   420: ifnonnull +10 -> 430
    //   423: getstatic 224	c/a/y:a	Lc/a/y;
    //   426: checkcast 214	java/util/List
    //   429: astore_3
    //   430: aload_3
    //   431: checkcast 156	java/lang/Iterable
    //   434: astore_3
    //   435: aload 6
    //   437: aload_3
    //   438: invokestatic 227	c/a/m:c	(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/util/List;
    //   441: checkcast 124	java/util/Collection
    //   444: areturn
    //   445: new 229	c/u
    //   448: astore_3
    //   449: aload_3
    //   450: ldc -25
    //   452: invokespecial 232	c/u:<init>	(Ljava/lang/String;)V
    //   455: aload_3
    //   456: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	457	0	this	br
    //   0	457	1	paramCollection	Collection
    //   1	123	2	localbr	br
    //   3	386	3	localObject1	Object
    //   398	1	3	localObject2	Object
    //   402	14	3	localObject3	Object
    //   418	38	3	localObject4	Object
    //   13	221	4	l1	long
    //   20	416	6	localObject5	Object
    //   24	385	7	localObject6	Object
    //   38	6	8	l2	long
    //   41	210	10	l3	long
    //   48	3	12	bool1	boolean
    //   56	15	13	k	int
    //   68	343	14	localObject7	Object
    //   105	279	15	localObject8	Object
    //   127	175	16	localContentResolver	ContentResolver
    //   132	172	17	localUri	Uri
    //   143	163	18	arrayOfString	String[]
    //   148	160	19	localObject9	Object
    //   158	142	20	localObject10	Object
    //   169	124	21	localObject11	Object
    //   173	25	22	localObject12	Object
    //   184	16	23	m	int
    //   351	3	24	bool2	boolean
    //   365	5	25	str	String
    // Exception table:
    //   from	to	target	type
    //   406	408	398	finally
    //   328	331	402	finally
    //   333	338	402	finally
    //   338	343	402	finally
    //   345	351	402	finally
    //   359	365	402	finally
    //   369	377	402	finally
    //   383	388	402	finally
  }
  
  private final boolean b(Collection paramCollection, boolean paramBoolean)
  {
    Object localObject1 = f;
    boolean bool1 = ((bw)localObject1).a();
    if (bool1)
    {
      localObject1 = i;
      bool1 = ((com.truecaller.utils.i)localObject1).a();
      if (bool1)
      {
        localObject1 = d(paramCollection);
        Object localObject2 = paramCollection;
        localObject2 = (Iterable)paramCollection;
        Object localObject3 = new java/util/ArrayList;
        int k = c.a.m.a((Iterable)localObject2, 10);
        ((ArrayList)localObject3).<init>(k);
        localObject3 = (Collection)localObject3;
        localObject2 = ((Iterable)localObject2).iterator();
        boolean bool3;
        Object localObject4;
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject2).hasNext();
          bool3 = true;
          if (!bool2) {
            break;
          }
          localObject4 = (String)((Iterator)localObject2).next();
          ContentValues localContentValues = new android/content/ContentValues;
          localContentValues.<init>();
          localContentValues.put("normalized_number", (String)localObject4);
          String str = "date";
          long l = c.a();
          Object localObject5 = Long.valueOf(l);
          localContentValues.put(str, (Long)localObject5);
          if (localObject1 != null)
          {
            localObject4 = (j)((Map)localObject1).get(localObject4);
            str = "im_peer_id";
            localObject5 = null;
            Object localObject6;
            int n;
            if (localObject4 != null)
            {
              localObject6 = ((j)localObject4).a();
            }
            else
            {
              n = 0;
              localObject6 = null;
            }
            localContentValues.put(str, (String)localObject6);
            str = "registration_timestamp";
            if (localObject4 != null)
            {
              n = ((j)localObject4).b();
              localObject6 = Integer.valueOf(n);
            }
            else
            {
              localObject6 = Integer.valueOf(0);
            }
            localContentValues.put(str, (Integer)localObject6);
            if (localObject4 != null) {
              localObject5 = ((j)localObject4).a();
            }
            localObject5 = (CharSequence)localObject5;
            Integer localInteger;
            if (localObject5 != null)
            {
              int m = ((CharSequence)localObject5).length();
              if (m != 0)
              {
                bool3 = false;
                localInteger = null;
              }
            }
            if (bool3)
            {
              localObject4 = "join_im_notification";
              localInteger = Integer.valueOf(0);
              localContentValues.put((String)localObject4, localInteger);
            }
          }
          ((Collection)localObject3).add(localContentValues);
        }
        localObject3 = (Collection)localObject3;
        localObject2 = new ContentValues[0];
        localObject2 = ((Collection)localObject3).toArray((Object[])localObject2);
        if (localObject2 != null)
        {
          localObject2 = (ContentValues[])localObject2;
          localObject3 = e;
          localObject4 = TruecallerContract.y.a();
          ((ContentResolver)localObject3).bulkInsert((Uri)localObject4, (ContentValues[])localObject2);
          if (paramBoolean) {
            c(paramCollection);
          }
          if (localObject1 != null) {
            return bool3;
          }
          return false;
        }
        paramCollection = new c/u;
        paramCollection.<init>("null cannot be cast to non-null type kotlin.Array<T>");
        throw paramCollection;
      }
    }
    return false;
  }
  
  private final void c(Collection paramCollection)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("com.truecaller.messaging.transport.im.ACTION_IM_USED_ADDED");
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(paramCollection);
    localIntent.putStringArrayListExtra("phone_numbers", localArrayList);
    g.a(localIntent);
  }
  
  private final Map d(Collection paramCollection)
  {
    Object localObject1 = (k.a)j.a.a(d);
    if (localObject1 == null) {
      return null;
    }
    paramCollection = (Iterable)paramCollection;
    int k = ag.a(c.a.m.a(paramCollection, 10));
    int n = 16;
    k = c.k.i.c(k, n);
    Object localObject2 = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject2).<init>(k);
    localObject2 = (Map)localObject2;
    paramCollection = paramCollection.iterator();
    Object localObject4;
    for (;;)
    {
      bool1 = paramCollection.hasNext();
      if (!bool1) {
        break label245;
      }
      localObject3 = paramCollection.next();
      localObject4 = localObject3;
      localObject4 = (String)localObject3;
      Object localObject5 = localObject4;
      localObject5 = (CharSequence)localObject4;
      String str = "receiver$0";
      k.b(localObject5, str);
      int i1 = ((CharSequence)localObject5).length();
      int i2 = 1;
      int i3 = 0;
      if (i1 > 0)
      {
        char c1 = ((CharSequence)localObject5).charAt(0);
        i1 = 43;
        boolean bool3 = c.n.a.a(c1, i1, false);
        if (bool3) {
          i3 = 1;
        }
      }
      if (i3 == 0)
      {
        localObject4 = null;
      }
      else
      {
        if (localObject4 == null) {
          break;
        }
        localObject4 = ((String)localObject4).substring(i2);
        localObject5 = "(this as java.lang.String).substring(startIndex)";
        k.a(localObject4, (String)localObject5);
        localObject4 = c.n.m.d((String)localObject4);
      }
      ((Map)localObject2).put(localObject4, localObject3);
    }
    paramCollection = new c/u;
    paramCollection.<init>("null cannot be cast to non-null type java.lang.String");
    throw paramCollection;
    label245:
    paramCollection = c.a.m.e((Iterable)((Map)localObject2).keySet());
    boolean bool1 = paramCollection.isEmpty();
    if (bool1) {
      return ag.a();
    }
    Object localObject3 = h.b.a();
    paramCollection = (Iterable)paramCollection;
    paramCollection = (h.b)((h.b.a)localObject3).a(paramCollection).build();
    try
    {
      paramCollection = ((k.a)localObject1).a(paramCollection);
      localObject1 = "response";
      k.a(paramCollection, (String)localObject1);
      paramCollection = paramCollection.a();
      localObject1 = "response.usersMap";
      k.a(paramCollection, (String)localObject1);
      localObject1 = new java/util/LinkedHashMap;
      int m = paramCollection.size();
      m = ag.a(m);
      ((LinkedHashMap)localObject1).<init>(m);
      localObject1 = (Map)localObject1;
      paramCollection = paramCollection.entrySet();
      paramCollection = (Iterable)paramCollection;
      paramCollection = paramCollection.iterator();
      for (;;)
      {
        boolean bool2 = paramCollection.hasNext();
        if (!bool2) {
          break;
        }
        localObject3 = paramCollection.next();
        localObject4 = localObject3;
        localObject4 = (Map.Entry)localObject3;
        localObject4 = ((Map.Entry)localObject4).getKey();
        localObject4 = ((Map)localObject2).get(localObject4);
        localObject4 = (String)localObject4;
        localObject3 = (Map.Entry)localObject3;
        localObject3 = ((Map.Entry)localObject3).getValue();
        ((Map)localObject1).put(localObject4, localObject3);
      }
      return (Map)localObject1;
    }
    catch (RuntimeException localRuntimeException)
    {
      com.truecaller.multisim.b.c.a();
    }
    return null;
  }
  
  /* Error */
  private final boolean d(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 60	com/truecaller/messaging/transport/im/br:e	Landroid/content/ContentResolver;
    //   4: astore_2
    //   5: invokestatic 105	com/truecaller/content/TruecallerContract$y:a	()Landroid/net/Uri;
    //   8: astore_3
    //   9: iconst_1
    //   10: anewarray 117	java/lang/String
    //   13: dup
    //   14: iconst_0
    //   15: ldc_w 425
    //   18: aastore
    //   19: astore 4
    //   21: ldc_w 427
    //   24: astore 5
    //   26: iconst_1
    //   27: istore 6
    //   29: iload 6
    //   31: anewarray 117	java/lang/String
    //   34: astore 7
    //   36: aload 7
    //   38: iconst_0
    //   39: aload_1
    //   40: aastore
    //   41: aload_2
    //   42: aload_3
    //   43: aload 4
    //   45: aload 5
    //   47: aload 7
    //   49: aconst_null
    //   50: invokevirtual 193	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   53: astore_1
    //   54: aload_1
    //   55: ifnull +58 -> 113
    //   58: aload_1
    //   59: checkcast 195	java/io/Closeable
    //   62: astore_1
    //   63: aconst_null
    //   64: astore_2
    //   65: aload_1
    //   66: astore_3
    //   67: aload_1
    //   68: checkcast 200	android/database/Cursor
    //   71: astore_3
    //   72: aload_3
    //   73: invokeinterface 430 1 0
    //   78: istore 8
    //   80: iload 8
    //   82: ifle +6 -> 88
    //   85: goto +6 -> 91
    //   88: iconst_0
    //   89: istore 6
    //   91: aload_1
    //   92: aconst_null
    //   93: invokestatic 219	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   96: iload 6
    //   98: ireturn
    //   99: astore_3
    //   100: goto +6 -> 106
    //   103: astore_2
    //   104: aload_2
    //   105: athrow
    //   106: aload_1
    //   107: aload_2
    //   108: invokestatic 219	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   111: aload_3
    //   112: athrow
    //   113: iconst_0
    //   114: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	115	0	this	br
    //   0	115	1	paramString	String
    //   4	61	2	localContentResolver	ContentResolver
    //   103	5	2	localThrowable	Throwable
    //   8	65	3	localObject1	Object
    //   99	13	3	localObject2	Object
    //   19	25	4	arrayOfString1	String[]
    //   24	22	5	str	String
    //   27	70	6	bool	boolean
    //   34	14	7	arrayOfString2	String[]
    //   78	3	8	k	int
    // Exception table:
    //   from	to	target	type
    //   104	106	99	finally
    //   67	71	103	finally
    //   72	78	103	finally
  }
  
  /* Error */
  private final boolean e(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull +5 -> 6
    //   4: iconst_0
    //   5: ireturn
    //   6: aload_0
    //   7: getfield 60	com/truecaller/messaging/transport/im/br:e	Landroid/content/ContentResolver;
    //   10: astore_2
    //   11: invokestatic 105	com/truecaller/content/TruecallerContract$y:a	()Landroid/net/Uri;
    //   14: astore_3
    //   15: iconst_1
    //   16: anewarray 117	java/lang/String
    //   19: dup
    //   20: iconst_0
    //   21: ldc_w 274
    //   24: aastore
    //   25: astore 4
    //   27: ldc_w 432
    //   30: astore 5
    //   32: iconst_1
    //   33: istore 6
    //   35: iload 6
    //   37: anewarray 117	java/lang/String
    //   40: astore 7
    //   42: aload 7
    //   44: iconst_0
    //   45: aload_1
    //   46: aastore
    //   47: aload_2
    //   48: aload_3
    //   49: aload 4
    //   51: aload 5
    //   53: aload 7
    //   55: aconst_null
    //   56: invokevirtual 193	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   59: astore_1
    //   60: aconst_null
    //   61: astore_2
    //   62: aload_1
    //   63: ifnull +113 -> 176
    //   66: aload_1
    //   67: astore_3
    //   68: aload_1
    //   69: checkcast 195	java/io/Closeable
    //   72: astore_3
    //   73: new 197	java/util/ArrayList
    //   76: astore 4
    //   78: aload 4
    //   80: invokespecial 198	java/util/ArrayList:<init>	()V
    //   83: aload 4
    //   85: checkcast 124	java/util/Collection
    //   88: astore 4
    //   90: aload_1
    //   91: invokeinterface 204 1 0
    //   96: istore 8
    //   98: iload 8
    //   100: ifeq +32 -> 132
    //   103: aload_1
    //   104: iconst_0
    //   105: invokeinterface 435 2 0
    //   110: istore 8
    //   112: iload 8
    //   114: invokestatic 282	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   117: astore 5
    //   119: aload 4
    //   121: aload 5
    //   123: invokeinterface 212 2 0
    //   128: pop
    //   129: goto -39 -> 90
    //   132: aload 4
    //   134: checkcast 214	java/util/List
    //   137: astore 4
    //   139: aload_3
    //   140: aconst_null
    //   141: invokestatic 219	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   144: aload 4
    //   146: invokestatic 438	c/a/m:e	(Ljava/util/List;)Ljava/lang/Object;
    //   149: astore_1
    //   150: aload_1
    //   151: astore_2
    //   152: aload_1
    //   153: checkcast 279	java/lang/Integer
    //   156: astore_2
    //   157: goto +19 -> 176
    //   160: astore_1
    //   161: goto +8 -> 169
    //   164: astore_1
    //   165: aload_1
    //   166: astore_2
    //   167: aload_1
    //   168: athrow
    //   169: aload_3
    //   170: aload_2
    //   171: invokestatic 219	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   174: aload_1
    //   175: athrow
    //   176: aload_2
    //   177: ifnonnull +6 -> 183
    //   180: goto +14 -> 194
    //   183: aload_2
    //   184: invokevirtual 441	java/lang/Integer:intValue	()I
    //   187: istore 9
    //   189: iload 9
    //   191: ifeq +6 -> 197
    //   194: iload 6
    //   196: ireturn
    //   197: iconst_0
    //   198: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	199	0	this	br
    //   0	199	1	paramString	String
    //   10	174	2	localObject1	Object
    //   14	156	3	localObject2	Object
    //   25	120	4	localObject3	Object
    //   30	92	5	localObject4	Object
    //   33	162	6	bool1	boolean
    //   40	14	7	arrayOfString	String[]
    //   96	3	8	bool2	boolean
    //   110	3	8	k	int
    //   187	3	9	m	int
    // Exception table:
    //   from	to	target	type
    //   167	169	160	finally
    //   73	76	164	finally
    //   78	83	164	finally
    //   83	88	164	finally
    //   90	96	164	finally
    //   104	110	164	finally
    //   112	117	164	finally
    //   121	129	164	finally
    //   132	137	164	finally
  }
  
  /* Error */
  public final w a()
  {
    // Byte code:
    //   0: iconst_2
    //   1: istore_1
    //   2: iload_1
    //   3: anewarray 117	java/lang/String
    //   6: astore_2
    //   7: iconst_0
    //   8: istore_3
    //   9: aload_2
    //   10: iconst_0
    //   11: ldc_w 446
    //   14: aastore
    //   15: getstatic 451	com/google/c/a/k$d:b	Lcom/google/c/a/k$d;
    //   18: invokevirtual 454	com/google/c/a/k$d:name	()Ljava/lang/String;
    //   21: astore 4
    //   23: iconst_1
    //   24: istore 5
    //   26: aload_2
    //   27: iload 5
    //   29: aload 4
    //   31: aastore
    //   32: ldc_w 456
    //   35: astore 4
    //   37: aload_0
    //   38: getfield 60	com/truecaller/messaging/transport/im/br:e	Landroid/content/ContentResolver;
    //   41: astore 6
    //   43: invokestatic 459	com/truecaller/content/TruecallerContract$k:a	()Landroid/net/Uri;
    //   46: astore 7
    //   48: iconst_1
    //   49: anewarray 117	java/lang/String
    //   52: dup
    //   53: iconst_0
    //   54: ldc_w 461
    //   57: aastore
    //   58: astore 8
    //   60: new 149	java/lang/StringBuilder
    //   63: astore 9
    //   65: aload 9
    //   67: invokespecial 462	java/lang/StringBuilder:<init>	()V
    //   70: aload 9
    //   72: ldc_w 443
    //   75: invokevirtual 171	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   78: pop
    //   79: aload 9
    //   81: ldc_w 464
    //   84: invokevirtual 171	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   87: pop
    //   88: aload 9
    //   90: aload 4
    //   92: invokevirtual 171	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   95: pop
    //   96: aload 9
    //   98: invokevirtual 177	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   101: astore 9
    //   103: ldc_w 466
    //   106: astore 10
    //   108: aload 6
    //   110: aload 7
    //   112: aload 8
    //   114: aload 9
    //   116: aload_2
    //   117: aload 10
    //   119: invokevirtual 193	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   122: astore 11
    //   124: aload 11
    //   126: ifnull +84 -> 210
    //   129: aload 11
    //   131: checkcast 195	java/io/Closeable
    //   134: astore 11
    //   136: iconst_0
    //   137: istore_1
    //   138: aconst_null
    //   139: astore 4
    //   141: aload 11
    //   143: astore 6
    //   145: aload 11
    //   147: checkcast 200	android/database/Cursor
    //   150: astore 6
    //   152: aload 6
    //   154: invokeinterface 430 1 0
    //   159: istore 12
    //   161: iload 12
    //   163: ifle +5 -> 168
    //   166: iconst_1
    //   167: istore_3
    //   168: iload_3
    //   169: invokestatic 471	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   172: astore 6
    //   174: aload 6
    //   176: invokestatic 476	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   179: astore 6
    //   181: aload 11
    //   183: aconst_null
    //   184: invokestatic 219	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   187: aload 6
    //   189: areturn
    //   190: astore 6
    //   192: goto +8 -> 200
    //   195: astore 4
    //   197: aload 4
    //   199: athrow
    //   200: aload 11
    //   202: aload 4
    //   204: invokestatic 219	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   207: aload 6
    //   209: athrow
    //   210: getstatic 480	java/lang/Boolean:FALSE	Ljava/lang/Boolean;
    //   213: invokestatic 476	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   216: astore 11
    //   218: aload 11
    //   220: ldc_w 482
    //   223: invokestatic 356	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   226: aload 11
    //   228: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	229	0	this	br
    //   1	137	1	k	int
    //   6	111	2	arrayOfString1	String[]
    //   8	161	3	bool	boolean
    //   21	119	4	str1	String
    //   195	8	4	localThrowable	Throwable
    //   24	4	5	m	int
    //   41	147	6	localObject1	Object
    //   190	18	6	localObject2	Object
    //   46	65	7	localUri	Uri
    //   58	55	8	arrayOfString2	String[]
    //   63	52	9	localObject3	Object
    //   106	12	10	str2	String
    //   122	105	11	localObject4	Object
    //   159	3	12	n	int
    // Exception table:
    //   from	to	target	type
    //   197	200	190	finally
    //   145	150	195	finally
    //   152	159	195	finally
    //   168	172	195	finally
    //   174	179	195	finally
  }
  
  /* Error */
  public final w a(long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore_3
    //   2: ldc_w 443
    //   5: astore 4
    //   7: aload_0
    //   8: getfield 66	com/truecaller/messaging/transport/im/br:h	Lcom/truecaller/messaging/h;
    //   11: invokeinterface 485 1 0
    //   16: astore 5
    //   18: aload 5
    //   20: ifnull +47 -> 67
    //   23: new 149	java/lang/StringBuilder
    //   26: astore 6
    //   28: ldc_w 487
    //   31: astore 7
    //   33: aload 6
    //   35: aload 7
    //   37: invokespecial 154	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   40: aload 5
    //   42: invokestatic 493	android/database/DatabaseUtils:sqlEscapeString	(Ljava/lang/String;)Ljava/lang/String;
    //   45: astore 5
    //   47: aload 6
    //   49: aload 5
    //   51: invokevirtual 171	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   54: pop
    //   55: aload 6
    //   57: invokevirtual 177	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   60: astore 5
    //   62: aload 5
    //   64: ifnonnull +8 -> 72
    //   67: ldc_w 495
    //   70: astore 5
    //   72: new 149	java/lang/StringBuilder
    //   75: astore 6
    //   77: ldc_w 497
    //   80: astore 7
    //   82: aload 6
    //   84: aload 7
    //   86: invokespecial 154	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   89: aload 6
    //   91: aload 5
    //   93: invokevirtual 171	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   96: pop
    //   97: bipush 41
    //   99: istore 8
    //   101: aload 6
    //   103: iload 8
    //   105: invokevirtual 501	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   108: pop
    //   109: aload 6
    //   111: invokevirtual 177	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   114: astore 6
    //   116: iconst_3
    //   117: istore 9
    //   119: iload 9
    //   121: anewarray 117	java/lang/String
    //   124: astore 10
    //   126: aload 10
    //   128: iconst_0
    //   129: ldc_w 446
    //   132: aastore
    //   133: getstatic 451	com/google/c/a/k$d:b	Lcom/google/c/a/k$d;
    //   136: invokevirtual 454	com/google/c/a/k$d:name	()Ljava/lang/String;
    //   139: astore 11
    //   141: iconst_1
    //   142: istore 12
    //   144: aload 10
    //   146: iload 12
    //   148: aload 11
    //   150: aastore
    //   151: lload_1
    //   152: invokestatic 180	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   155: astore 11
    //   157: iconst_2
    //   158: istore 13
    //   160: aload 10
    //   162: iload 13
    //   164: aload 11
    //   166: aastore
    //   167: aload 10
    //   169: invokestatic 505	c/a/m:b	([Ljava/lang/Object;)Ljava/util/List;
    //   172: astore 10
    //   174: aload_3
    //   175: getfield 60	com/truecaller/messaging/transport/im/br:e	Landroid/content/ContentResolver;
    //   178: astore 14
    //   180: invokestatic 459	com/truecaller/content/TruecallerContract$k:a	()Landroid/net/Uri;
    //   183: astore 15
    //   185: iconst_1
    //   186: anewarray 117	java/lang/String
    //   189: dup
    //   190: iconst_0
    //   191: ldc_w 507
    //   194: aastore
    //   195: astore 16
    //   197: new 149	java/lang/StringBuilder
    //   200: astore 11
    //   202: aload 11
    //   204: invokespecial 462	java/lang/StringBuilder:<init>	()V
    //   207: aload 11
    //   209: aload 4
    //   211: invokevirtual 171	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   214: pop
    //   215: ldc_w 464
    //   218: astore 4
    //   220: aload 11
    //   222: aload 4
    //   224: invokevirtual 171	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   227: pop
    //   228: aload 11
    //   230: aload 6
    //   232: invokevirtual 171	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   235: pop
    //   236: aload 11
    //   238: invokevirtual 177	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   241: astore 17
    //   243: aload 10
    //   245: checkcast 124	java/util/Collection
    //   248: astore 10
    //   250: aload 10
    //   252: ifnull +868 -> 1120
    //   255: iconst_0
    //   256: anewarray 117	java/lang/String
    //   259: astore 4
    //   261: aload 10
    //   263: aload 4
    //   265: invokeinterface 187 2 0
    //   270: astore 4
    //   272: aload 4
    //   274: ifnull +831 -> 1105
    //   277: aload 4
    //   279: astore 18
    //   281: aload 4
    //   283: checkcast 189	[Ljava/lang/String;
    //   286: astore 18
    //   288: aconst_null
    //   289: astore 19
    //   291: aload 14
    //   293: aload 15
    //   295: aload 16
    //   297: aload 17
    //   299: aload 18
    //   301: aconst_null
    //   302: invokevirtual 193	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   305: astore 4
    //   307: aconst_null
    //   308: astore 6
    //   310: aload 4
    //   312: ifnull +776 -> 1088
    //   315: aload 4
    //   317: astore 10
    //   319: aload 4
    //   321: checkcast 195	java/io/Closeable
    //   324: astore 10
    //   326: new 197	java/util/ArrayList
    //   329: astore 11
    //   331: aload 11
    //   333: invokespecial 198	java/util/ArrayList:<init>	()V
    //   336: aload 11
    //   338: checkcast 124	java/util/Collection
    //   341: astore 11
    //   343: aload 4
    //   345: invokeinterface 204 1 0
    //   350: istore 20
    //   352: iload 20
    //   354: ifeq +26 -> 380
    //   357: aload 4
    //   359: iconst_0
    //   360: invokeinterface 208 2 0
    //   365: astore 14
    //   367: aload 11
    //   369: aload 14
    //   371: invokeinterface 212 2 0
    //   376: pop
    //   377: goto -34 -> 343
    //   380: aload 11
    //   382: checkcast 214	java/util/List
    //   385: astore 11
    //   387: aload 10
    //   389: aconst_null
    //   390: invokestatic 219	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   393: aload 11
    //   395: checkcast 156	java/lang/Iterable
    //   398: astore 11
    //   400: aload 11
    //   402: invokestatic 511	c/a/m:l	(Ljava/lang/Iterable;)Ljava/util/Set;
    //   405: astore 4
    //   407: aload 4
    //   409: ifnonnull +6 -> 415
    //   412: goto +676 -> 1088
    //   415: aload_3
    //   416: getfield 60	com/truecaller/messaging/transport/im/br:e	Landroid/content/ContentResolver;
    //   419: astore 14
    //   421: invokestatic 514	com/truecaller/content/TruecallerContract$ae:a	()Landroid/net/Uri;
    //   424: astore 15
    //   426: iconst_1
    //   427: anewarray 117	java/lang/String
    //   430: dup
    //   431: iconst_0
    //   432: ldc_w 516
    //   435: aastore
    //   436: astore 16
    //   438: ldc_w 518
    //   441: astore 17
    //   443: aconst_null
    //   444: astore 18
    //   446: aconst_null
    //   447: astore 19
    //   449: aload 14
    //   451: aload 15
    //   453: aload 16
    //   455: aload 17
    //   457: aconst_null
    //   458: aconst_null
    //   459: invokevirtual 193	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   462: astore 10
    //   464: aload 10
    //   466: ifnull +581 -> 1047
    //   469: aload 10
    //   471: astore 11
    //   473: aload 10
    //   475: checkcast 195	java/io/Closeable
    //   478: astore 11
    //   480: new 197	java/util/ArrayList
    //   483: astore 14
    //   485: aload 14
    //   487: invokespecial 198	java/util/ArrayList:<init>	()V
    //   490: aload 14
    //   492: checkcast 124	java/util/Collection
    //   495: astore 14
    //   497: aload 10
    //   499: invokeinterface 204 1 0
    //   504: istore 21
    //   506: iload 21
    //   508: ifeq +26 -> 534
    //   511: aload 10
    //   513: iconst_0
    //   514: invokeinterface 208 2 0
    //   519: astore 15
    //   521: aload 14
    //   523: aload 15
    //   525: invokeinterface 212 2 0
    //   530: pop
    //   531: goto -34 -> 497
    //   534: aload 14
    //   536: checkcast 214	java/util/List
    //   539: astore 14
    //   541: aload 11
    //   543: aconst_null
    //   544: invokestatic 219	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   547: aload 14
    //   549: checkcast 124	java/util/Collection
    //   552: astore 14
    //   554: aload 4
    //   556: aload 14
    //   558: invokeinterface 524 2 0
    //   563: pop
    //   564: aload_3
    //   565: getfield 60	com/truecaller/messaging/transport/im/br:e	Landroid/content/ContentResolver;
    //   568: astore 15
    //   570: invokestatic 528	com/truecaller/content/TruecallerContract$a:b	()Landroid/net/Uri;
    //   573: astore 16
    //   575: ldc_w 534
    //   578: astore 14
    //   580: iconst_5
    //   581: anewarray 117	java/lang/String
    //   584: dup
    //   585: dup2
    //   586: iconst_0
    //   587: ldc_w 530
    //   590: aastore
    //   591: iconst_1
    //   592: ldc_w 532
    //   595: aastore
    //   596: dup2
    //   597: iconst_2
    //   598: aload 14
    //   600: aastore
    //   601: iconst_3
    //   602: ldc_w 536
    //   605: aastore
    //   606: iconst_4
    //   607: ldc_w 538
    //   610: aastore
    //   611: astore 17
    //   613: new 149	java/lang/StringBuilder
    //   616: astore 10
    //   618: aload 10
    //   620: ldc_w 540
    //   623: invokespecial 154	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   626: aload 4
    //   628: astore 18
    //   630: aload 4
    //   632: checkcast 156	java/lang/Iterable
    //   635: astore 18
    //   637: aconst_null
    //   638: astore 19
    //   640: getstatic 545	com/truecaller/messaging/transport/im/br$c:a	Lcom/truecaller/messaging/transport/im/br$c;
    //   643: astore 11
    //   645: aload 11
    //   647: astore 22
    //   649: aload 11
    //   651: checkcast 163	c/g/a/b
    //   654: astore 22
    //   656: bipush 31
    //   658: istore 23
    //   660: aload 18
    //   662: aconst_null
    //   663: aconst_null
    //   664: aconst_null
    //   665: iconst_0
    //   666: aconst_null
    //   667: aload 22
    //   669: iload 23
    //   671: invokestatic 167	c/a/m:a	(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lc/g/a/b;I)Ljava/lang/String;
    //   674: astore 11
    //   676: aload 10
    //   678: aload 11
    //   680: invokevirtual 171	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   683: pop
    //   684: aload 10
    //   686: iload 8
    //   688: invokevirtual 501	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   691: pop
    //   692: aload 10
    //   694: invokevirtual 177	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   697: astore 18
    //   699: aload 4
    //   701: checkcast 124	java/util/Collection
    //   704: astore 4
    //   706: aload 4
    //   708: ifnull +299 -> 1007
    //   711: iconst_0
    //   712: anewarray 117	java/lang/String
    //   715: astore 5
    //   717: aload 4
    //   719: aload 5
    //   721: invokeinterface 187 2 0
    //   726: astore 4
    //   728: aload 4
    //   730: ifnull +262 -> 992
    //   733: aload 4
    //   735: astore 19
    //   737: aload 4
    //   739: checkcast 189	[Ljava/lang/String;
    //   742: astore 19
    //   744: aload 15
    //   746: aload 16
    //   748: aload 17
    //   750: aload 18
    //   752: aload 19
    //   754: aconst_null
    //   755: invokevirtual 193	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   758: astore 4
    //   760: aload 4
    //   762: ifnull +212 -> 974
    //   765: aload 4
    //   767: astore 5
    //   769: aload 4
    //   771: checkcast 195	java/io/Closeable
    //   774: astore 5
    //   776: new 197	java/util/ArrayList
    //   779: astore 10
    //   781: aload 10
    //   783: invokespecial 198	java/util/ArrayList:<init>	()V
    //   786: aload 10
    //   788: checkcast 124	java/util/Collection
    //   791: astore 10
    //   793: aload 4
    //   795: invokeinterface 204 1 0
    //   800: istore 24
    //   802: iload 24
    //   804: ifeq +116 -> 920
    //   807: new 547	com/truecaller/messaging/transport/im/bx
    //   810: astore 11
    //   812: aload 4
    //   814: iconst_0
    //   815: invokeinterface 435 2 0
    //   820: istore 21
    //   822: aload 4
    //   824: iload 12
    //   826: invokeinterface 208 2 0
    //   831: astore 16
    //   833: aload 4
    //   835: iload 13
    //   837: invokeinterface 208 2 0
    //   842: astore 17
    //   844: ldc_w 549
    //   847: astore 14
    //   849: aload 17
    //   851: aload 14
    //   853: invokestatic 356	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   856: aload 4
    //   858: iload 9
    //   860: invokeinterface 208 2 0
    //   865: astore 18
    //   867: iconst_4
    //   868: istore 20
    //   870: aload 4
    //   872: iload 20
    //   874: invokeinterface 554 2 0
    //   879: lstore 25
    //   881: lload 25
    //   883: invokestatic 97	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   886: astore 19
    //   888: aload 11
    //   890: astore 14
    //   892: aload 11
    //   894: iload 21
    //   896: aload 16
    //   898: aload 17
    //   900: aload 18
    //   902: aload 19
    //   904: invokespecial 557	com/truecaller/messaging/transport/im/bx:<init>	(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    //   907: aload 10
    //   909: aload 11
    //   911: invokeinterface 212 2 0
    //   916: pop
    //   917: goto -124 -> 793
    //   920: aload 10
    //   922: checkcast 214	java/util/List
    //   925: astore 10
    //   927: aload 5
    //   929: aconst_null
    //   930: invokestatic 219	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   933: aload 10
    //   935: checkcast 156	java/lang/Iterable
    //   938: astore 10
    //   940: aload 10
    //   942: invokestatic 559	c/a/m:g	(Ljava/lang/Iterable;)Ljava/util/List;
    //   945: astore 6
    //   947: goto +27 -> 974
    //   950: astore 4
    //   952: goto +12 -> 964
    //   955: astore 4
    //   957: aload 4
    //   959: astore 6
    //   961: aload 4
    //   963: athrow
    //   964: aload 5
    //   966: aload 6
    //   968: invokestatic 219	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   971: aload 4
    //   973: athrow
    //   974: aload 6
    //   976: invokestatic 476	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   979: astore 4
    //   981: aload 4
    //   983: ldc_w 561
    //   986: invokestatic 356	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   989: aload 4
    //   991: areturn
    //   992: new 229	c/u
    //   995: astore 4
    //   997: aload 4
    //   999: ldc -25
    //   1001: invokespecial 232	c/u:<init>	(Ljava/lang/String;)V
    //   1004: aload 4
    //   1006: athrow
    //   1007: new 229	c/u
    //   1010: astore 4
    //   1012: aload 4
    //   1014: ldc_w 563
    //   1017: invokespecial 232	c/u:<init>	(Ljava/lang/String;)V
    //   1020: aload 4
    //   1022: athrow
    //   1023: astore 4
    //   1025: goto +12 -> 1037
    //   1028: astore 4
    //   1030: aload 4
    //   1032: astore 6
    //   1034: aload 4
    //   1036: athrow
    //   1037: aload 11
    //   1039: aload 6
    //   1041: invokestatic 219	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   1044: aload 4
    //   1046: athrow
    //   1047: aconst_null
    //   1048: invokestatic 476	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   1051: astore 4
    //   1053: aload 4
    //   1055: ldc_w 565
    //   1058: invokestatic 356	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1061: aload 4
    //   1063: areturn
    //   1064: astore 4
    //   1066: goto +12 -> 1078
    //   1069: astore 4
    //   1071: aload 4
    //   1073: astore 6
    //   1075: aload 4
    //   1077: athrow
    //   1078: aload 10
    //   1080: aload 6
    //   1082: invokestatic 219	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   1085: aload 4
    //   1087: athrow
    //   1088: aconst_null
    //   1089: invokestatic 476	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   1092: astore 4
    //   1094: aload 4
    //   1096: ldc_w 565
    //   1099: invokestatic 356	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1102: aload 4
    //   1104: areturn
    //   1105: new 229	c/u
    //   1108: astore 4
    //   1110: aload 4
    //   1112: ldc -25
    //   1114: invokespecial 232	c/u:<init>	(Ljava/lang/String;)V
    //   1117: aload 4
    //   1119: athrow
    //   1120: new 229	c/u
    //   1123: astore 4
    //   1125: aload 4
    //   1127: ldc_w 563
    //   1130: invokespecial 232	c/u:<init>	(Ljava/lang/String;)V
    //   1133: aload 4
    //   1135: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1136	0	this	br
    //   0	1136	1	paramLong	long
    //   1	564	3	localbr	br
    //   5	866	4	localObject1	Object
    //   950	1	4	localObject2	Object
    //   955	17	4	localObject3	Object
    //   979	42	4	localObject4	Object
    //   1023	1	4	localObject5	Object
    //   1028	17	4	localObject6	Object
    //   1051	11	4	localw	w
    //   1064	1	4	localObject7	Object
    //   1069	17	4	localObject8	Object
    //   1092	42	4	localObject9	Object
    //   16	949	5	localObject10	Object
    //   26	1055	6	localObject11	Object
    //   31	54	7	str	String
    //   99	588	8	c1	char
    //   117	742	9	k	int
    //   124	955	10	localObject12	Object
    //   139	899	11	localObject13	Object
    //   142	683	12	m	int
    //   158	678	13	n	int
    //   178	713	14	localObject14	Object
    //   183	562	15	localObject15	Object
    //   195	702	16	localObject16	Object
    //   241	658	17	localObject17	Object
    //   279	622	18	localObject18	Object
    //   289	614	19	localObject19	Object
    //   350	3	20	bool1	boolean
    //   868	5	20	i1	int
    //   504	3	21	bool2	boolean
    //   820	75	21	i2	int
    //   647	21	22	localObject20	Object
    //   658	12	23	i3	int
    //   800	3	24	bool3	boolean
    //   879	3	25	l	long
    // Exception table:
    //   from	to	target	type
    //   961	964	950	finally
    //   776	779	955	finally
    //   781	786	955	finally
    //   786	791	955	finally
    //   793	800	955	finally
    //   807	810	955	finally
    //   814	820	955	finally
    //   824	831	955	finally
    //   835	842	955	finally
    //   851	856	955	finally
    //   858	865	955	finally
    //   872	879	955	finally
    //   881	886	955	finally
    //   902	907	955	finally
    //   909	917	955	finally
    //   920	925	955	finally
    //   1034	1037	1023	finally
    //   480	483	1028	finally
    //   485	490	1028	finally
    //   490	495	1028	finally
    //   497	504	1028	finally
    //   513	519	1028	finally
    //   523	531	1028	finally
    //   534	539	1028	finally
    //   1075	1078	1064	finally
    //   326	329	1069	finally
    //   331	336	1069	finally
    //   336	341	1069	finally
    //   343	350	1069	finally
    //   359	365	1069	finally
    //   369	377	1069	finally
    //   380	385	1069	finally
  }
  
  public final w a(String paramString)
  {
    k.b(paramString, "normalizedNumber");
    a(paramString, null, true);
    paramString = w.b(Boolean.valueOf(d(paramString)));
    k.a(paramString, "Promise.wrap(hasImId(normalizedNumber))");
    return paramString;
  }
  
  public final w a(Collection paramCollection, boolean paramBoolean)
  {
    k.b(paramCollection, "normalizedNumbers");
    Object localObject = f;
    boolean bool = ((bw)localObject).a();
    if (bool)
    {
      localObject = i;
      bool = ((com.truecaller.utils.i)localObject).a();
      if (bool)
      {
        paramCollection = c.a.m.n((Iterable)paramCollection);
        int k = b;
        paramCollection = l.a(paramCollection, k);
        localObject = new com/truecaller/messaging/transport/im/br$a;
        ((br.a)localObject).<init>(this, paramBoolean);
        localObject = (b)localObject;
        paramCollection = l.a(l.c(paramCollection, (b)localObject));
        paramBoolean = a;
        paramCollection = l.a(paramCollection, paramBoolean).a();
        paramBoolean = true;
        k = 1;
        for (;;)
        {
          int m = paramCollection.hasNext();
          if (m == 0) {
            break;
          }
          Collection localCollection = (Collection)paramCollection.next();
          m = b(localCollection, paramBoolean);
          k &= m;
        }
        paramCollection = w.b(Boolean.valueOf(k));
        k.a(paramCollection, "Promise.wrap(it)");
        k.a(paramCollection, "normalizedNumbers.asSequ….let { Promise.wrap(it) }");
        return paramCollection;
      }
    }
    paramCollection = w.b(Boolean.FALSE);
    k.a(paramCollection, "Promise.wrap(false)");
    return paramCollection;
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "normalizedNumber");
    k.b(paramString2, "imPeerId");
    a(paramString1, paramString2, false);
  }
  
  public final void a(Collection paramCollection)
  {
    k.b(paramCollection, "normalizedNumbers");
    Object localObject = f;
    boolean bool1 = ((bw)localObject).a();
    if (bool1)
    {
      localObject = i;
      bool1 = ((com.truecaller.utils.i)localObject).a();
      if (bool1)
      {
        paramCollection = c.a.m.n((Iterable)paramCollection);
        int k = b;
        paramCollection = l.a(paramCollection, k);
        localObject = new com/truecaller/messaging/transport/im/br$d;
        ((br.d)localObject).<init>(this);
        localObject = (b)localObject;
        paramCollection = (Collection)l.d(l.a(l.c(paramCollection, (b)localObject)));
        boolean bool2 = paramCollection.isEmpty();
        if (bool2) {
          return;
        }
        paramCollection = (List)paramCollection;
        localObject = (com.truecaller.presence.c)((f)j.get()).a();
        paramCollection = (Collection)paramCollection;
        ((com.truecaller.presence.c)localObject).a(paramCollection);
        return;
      }
    }
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "numbers");
    paramList = (Iterable)paramList;
    Object localObject1 = new java/util/ArrayList;
    int k = c.a.m.a(paramList, 10);
    ((ArrayList)localObject1).<init>(k);
    localObject1 = (Collection)localObject1;
    paramList = paramList.iterator();
    int m;
    Object localObject3;
    for (;;)
    {
      boolean bool = paramList.hasNext();
      m = 1;
      if (!bool) {
        break;
      }
      localObject2 = (String)paramList.next();
      localObject3 = new android/content/ContentValues;
      ((ContentValues)localObject3).<init>();
      String str = "normalized_number";
      ((ContentValues)localObject3).put(str, (String)localObject2);
      localObject2 = "join_im_notification";
      Integer localInteger = Integer.valueOf(m);
      ((ContentValues)localObject3).put((String)localObject2, localInteger);
      ((Collection)localObject1).add(localObject3);
    }
    localObject1 = (Collection)localObject1;
    paramList = null;
    Object localObject2 = new ContentValues[0];
    localObject1 = ((Collection)localObject1).toArray((Object[])localObject2);
    if (localObject1 != null)
    {
      localObject1 = (ContentValues[])localObject1;
      localObject2 = e;
      localObject3 = TruecallerContract.y.a();
      int n = ((ContentResolver)localObject2).bulkInsert((Uri)localObject3, (ContentValues[])localObject1);
      localObject2 = new String[m];
      localObject1 = String.valueOf(n);
      localObject1 = "mark Im User As Notified successfully: ".concat((String)localObject1);
      localObject2[0] = localObject1;
      com.truecaller.multisim.b.c.c((String[])localObject2);
      return;
    }
    paramList = new c/u;
    paramList.<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw paramList;
  }
  
  public final w b(String paramString)
  {
    k.b(paramString, "normalizedNumber");
    Collection localCollection = (Collection)c.a.m.a(paramString);
    a(localCollection);
    paramString = w.b(Boolean.valueOf(d(paramString)));
    k.a(paramString, "Promise.wrap(hasImId(normalizedNumber))");
    return paramString;
  }
  
  /* Error */
  public final w c(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 606
    //   4: invokestatic 32	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: aload_0
    //   8: getfield 60	com/truecaller/messaging/transport/im/br:e	Landroid/content/ContentResolver;
    //   11: astore_2
    //   12: invokestatic 105	com/truecaller/content/TruecallerContract$y:a	()Landroid/net/Uri;
    //   15: astore_3
    //   16: iconst_1
    //   17: anewarray 117	java/lang/String
    //   20: dup
    //   21: iconst_0
    //   22: ldc 78
    //   24: aastore
    //   25: astore 4
    //   27: ldc_w 642
    //   30: astore 5
    //   32: iconst_1
    //   33: anewarray 117	java/lang/String
    //   36: astore 6
    //   38: aconst_null
    //   39: astore 7
    //   41: aload 6
    //   43: iconst_0
    //   44: aload_1
    //   45: aastore
    //   46: aload_2
    //   47: aload_3
    //   48: aload 4
    //   50: aload 5
    //   52: aload 6
    //   54: aconst_null
    //   55: invokevirtual 193	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   58: astore_1
    //   59: aconst_null
    //   60: astore_2
    //   61: aload_1
    //   62: ifnull +72 -> 134
    //   65: aload_1
    //   66: checkcast 195	java/io/Closeable
    //   69: astore_1
    //   70: aload_1
    //   71: astore_3
    //   72: aload_1
    //   73: checkcast 200	android/database/Cursor
    //   76: astore_3
    //   77: aload_3
    //   78: invokeinterface 645 1 0
    //   83: istore 8
    //   85: iload 8
    //   87: ifeq +15 -> 102
    //   90: aload_3
    //   91: iconst_0
    //   92: invokeinterface 208 2 0
    //   97: astore 7
    //   99: goto +6 -> 105
    //   102: aconst_null
    //   103: astore 7
    //   105: aload_1
    //   106: aconst_null
    //   107: invokestatic 219	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   110: goto +27 -> 137
    //   113: astore 7
    //   115: goto +11 -> 126
    //   118: astore 7
    //   120: aload 7
    //   122: astore_2
    //   123: aload 7
    //   125: athrow
    //   126: aload_1
    //   127: aload_2
    //   128: invokestatic 219	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   131: aload 7
    //   133: athrow
    //   134: aconst_null
    //   135: astore 7
    //   137: aload 7
    //   139: invokestatic 476	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   142: astore_1
    //   143: aload_1
    //   144: ldc_w 647
    //   147: invokestatic 356	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   150: aload_1
    //   151: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	152	0	this	br
    //   0	152	1	paramString	String
    //   11	117	2	localObject1	Object
    //   15	76	3	localObject2	Object
    //   25	24	4	arrayOfString1	String[]
    //   30	21	5	str1	String
    //   36	17	6	arrayOfString2	String[]
    //   39	65	7	str2	String
    //   113	1	7	localObject3	Object
    //   118	14	7	localObject4	Object
    //   135	3	7	localObject5	Object
    //   83	3	8	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   123	126	113	finally
    //   72	76	118	finally
    //   77	83	118	finally
    //   91	97	118	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.br
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */