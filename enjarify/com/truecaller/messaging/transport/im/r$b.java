package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class r$b
  extends u
{
  private final String b;
  private final long c;
  private final String d;
  private final String e;
  private final String f;
  private final String g;
  
  private r$b(e parame, String paramString1, long paramLong, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    super(parame);
    b = paramString1;
    c = paramLong;
    d = paramString2;
    e = paramString3;
    f = paramString4;
    g = paramString5;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".sendReaction(");
    String str = b;
    int i = 2;
    str = a(str, i);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(Long.valueOf(c), i);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(d, i);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(e, i);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(f, i);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(g, i);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.r.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */