package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import com.truecaller.common.background.b;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.h;
import com.truecaller.util.af;
import com.truecaller.util.al;

public final class k
  implements j
{
  private final af a;
  private final ContentResolver b;
  private final h c;
  private final bp d;
  private final bw e;
  private final al f;
  private final b g;
  
  public k(af paramaf, ContentResolver paramContentResolver, h paramh, bp parambp, bw parambw, al paramal, b paramb)
  {
    a = paramaf;
    b = paramContentResolver;
    c = paramh;
    d = parambp;
    e = parambw;
    f = paramal;
    g = paramb;
  }
  
  public final boolean a()
  {
    Object localObject = e;
    boolean bool = ((bw)localObject).a();
    if (bool)
    {
      localObject = f;
      bool = ((al)localObject).c();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  /* Error */
  public final void b()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 47	com/truecaller/messaging/transport/im/k:b	Landroid/content/ContentResolver;
    //   4: astore_1
    //   5: invokestatic 72	com/truecaller/content/TruecallerContract$k:a	()Landroid/net/Uri;
    //   8: astore_2
    //   9: iconst_1
    //   10: anewarray 76	java/lang/String
    //   13: dup
    //   14: iconst_0
    //   15: ldc 74
    //   17: aastore
    //   18: astore_3
    //   19: ldc 78
    //   21: astore 4
    //   23: iconst_2
    //   24: istore 5
    //   26: iload 5
    //   28: anewarray 76	java/lang/String
    //   31: astore 6
    //   33: iconst_0
    //   34: istore 7
    //   36: aload 6
    //   38: iconst_0
    //   39: ldc 81
    //   41: aastore
    //   42: getstatic 86	com/google/c/a/k$d:b	Lcom/google/c/a/k$d;
    //   45: invokevirtual 90	com/google/c/a/k$d:name	()Ljava/lang/String;
    //   48: astore 8
    //   50: iconst_1
    //   51: istore 9
    //   53: aload 6
    //   55: iload 9
    //   57: aload 8
    //   59: aastore
    //   60: aconst_null
    //   61: astore 8
    //   63: aload_1
    //   64: aload_2
    //   65: aload_3
    //   66: aload 4
    //   68: aload 6
    //   70: aconst_null
    //   71: invokevirtual 96	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   74: astore_1
    //   75: aload_1
    //   76: ifnull +229 -> 305
    //   79: aload_1
    //   80: astore_2
    //   81: aload_1
    //   82: checkcast 98	java/io/Closeable
    //   85: astore_2
    //   86: iconst_0
    //   87: istore 10
    //   89: aconst_null
    //   90: astore_3
    //   91: new 100	java/util/ArrayList
    //   94: astore 4
    //   96: aload 4
    //   98: invokespecial 101	java/util/ArrayList:<init>	()V
    //   101: aload 4
    //   103: checkcast 103	java/util/Collection
    //   106: astore 4
    //   108: aload_1
    //   109: invokeinterface 108 1 0
    //   114: istore 5
    //   116: iload 5
    //   118: ifeq +25 -> 143
    //   121: aload_1
    //   122: iconst_0
    //   123: invokeinterface 112 2 0
    //   128: astore 6
    //   130: aload 4
    //   132: aload 6
    //   134: invokeinterface 116 2 0
    //   139: pop
    //   140: goto -32 -> 108
    //   143: aload 4
    //   145: checkcast 118	java/util/List
    //   148: astore 4
    //   150: aload_2
    //   151: aconst_null
    //   152: invokestatic 123	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   155: aload 4
    //   157: invokeinterface 126 1 0
    //   162: istore 11
    //   164: iload 11
    //   166: ifeq +4 -> 170
    //   169: return
    //   170: aload_0
    //   171: getfield 49	com/truecaller/messaging/transport/im/k:c	Lcom/truecaller/messaging/h;
    //   174: astore_1
    //   175: aload_1
    //   176: invokeinterface 132 1 0
    //   181: lstore 12
    //   183: lconst_0
    //   184: lstore 14
    //   186: lload 12
    //   188: lload 14
    //   190: lcmp
    //   191: istore 10
    //   193: iload 10
    //   195: ifle +24 -> 219
    //   198: aload_0
    //   199: getfield 51	com/truecaller/messaging/transport/im/k:d	Lcom/truecaller/messaging/transport/im/bp;
    //   202: astore_1
    //   203: aload 4
    //   205: checkcast 103	java/util/Collection
    //   208: astore 4
    //   210: aload_1
    //   211: aload 4
    //   213: invokeinterface 137 2 0
    //   218: return
    //   219: aload_0
    //   220: getfield 51	com/truecaller/messaging/transport/im/k:d	Lcom/truecaller/messaging/transport/im/bp;
    //   223: astore_1
    //   224: aload 4
    //   226: checkcast 103	java/util/Collection
    //   229: astore 4
    //   231: aload_1
    //   232: aload 4
    //   234: iconst_0
    //   235: invokeinterface 140 3 0
    //   240: invokevirtual 145	com/truecaller/androidactors/w:d	()Ljava/lang/Object;
    //   243: checkcast 147	java/lang/Boolean
    //   246: astore_1
    //   247: aload_1
    //   248: ifnull +9 -> 257
    //   251: aload_1
    //   252: invokevirtual 150	java/lang/Boolean:booleanValue	()Z
    //   255: istore 7
    //   257: iload 7
    //   259: ifeq +29 -> 288
    //   262: aload_0
    //   263: getfield 49	com/truecaller/messaging/transport/im/k:c	Lcom/truecaller/messaging/h;
    //   266: astore_1
    //   267: aload_0
    //   268: getfield 45	com/truecaller/messaging/transport/im/k:a	Lcom/truecaller/util/af;
    //   271: astore_2
    //   272: aload_2
    //   273: invokeinterface 154 1 0
    //   278: lstore 16
    //   280: aload_1
    //   281: lload 16
    //   283: invokeinterface 157 3 0
    //   288: return
    //   289: astore_1
    //   290: goto +8 -> 298
    //   293: astore_1
    //   294: aload_1
    //   295: astore_3
    //   296: aload_1
    //   297: athrow
    //   298: aload_2
    //   299: aload_3
    //   300: invokestatic 123	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   303: aload_1
    //   304: athrow
    //   305: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	306	0	this	k
    //   4	277	1	localObject1	Object
    //   289	1	1	localObject2	Object
    //   293	11	1	localObject3	Object
    //   8	291	2	localObject4	Object
    //   18	282	3	localObject5	Object
    //   21	212	4	localObject6	Object
    //   24	3	5	i	int
    //   114	3	5	bool1	boolean
    //   31	102	6	localObject7	Object
    //   34	224	7	bool2	boolean
    //   48	14	8	str	String
    //   51	5	9	j	int
    //   87	107	10	bool3	boolean
    //   162	3	11	bool4	boolean
    //   181	6	12	l1	long
    //   184	5	14	l2	long
    //   278	4	16	l3	long
    // Exception table:
    //   from	to	target	type
    //   296	298	289	finally
    //   91	94	293	finally
    //   96	101	293	finally
    //   101	106	293	finally
    //   108	114	293	finally
    //   122	128	293	finally
    //   132	140	293	finally
    //   143	148	293	finally
  }
  
  public final void c()
  {
    g.b(10031);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */