package com.truecaller.messaging.transport.im;

import c.a.f;
import c.g.b.k;
import c.u;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Reaction;
import java.util.Collection;
import java.util.List;

public final class ImTransportInfo$a
{
  public long a;
  public int b;
  public int c;
  public int d;
  public int e;
  public int f;
  int g = -1;
  public int h;
  public int i;
  public long j;
  List k;
  Participant l;
  private String m = "";
  
  public ImTransportInfo$a() {}
  
  public ImTransportInfo$a(ImTransportInfo paramImTransportInfo)
  {
    this();
    long l1 = ImTransportInfo.a(paramImTransportInfo);
    a = l1;
    String str = b;
    int n = c;
    b = n;
    n = ImTransportInfo.b(paramImTransportInfo);
    c = n;
    n = ImTransportInfo.c(paramImTransportInfo);
    d = n;
    n = f;
    e = n;
    n = g;
    f = n;
    n = l;
    n = h;
    h = n;
    n = i;
    i = n;
    l1 = j;
    j = l1;
    paramImTransportInfo = k;
    if (paramImTransportInfo != null) {
      paramImTransportInfo = f.g(paramImTransportInfo);
    } else {
      paramImTransportInfo = null;
    }
    k = paramImTransportInfo;
  }
  
  public final a a(String paramString)
  {
    k.b(paramString, "rawId");
    m = paramString;
    return this;
  }
  
  public final a a(Reaction[] paramArrayOfReaction)
  {
    if (paramArrayOfReaction != null) {
      paramArrayOfReaction = f.g(paramArrayOfReaction);
    } else {
      paramArrayOfReaction = null;
    }
    k = paramArrayOfReaction;
    return this;
  }
  
  public final ImTransportInfo a()
  {
    a locala = this;
    long l1 = a;
    String str = m;
    int n = b;
    int i1 = c;
    int i2 = d;
    int i3 = e;
    int i4 = f;
    int i5 = h;
    int i6 = i;
    long l2 = j;
    Object localObject1 = k;
    if (localObject1 != null)
    {
      localObject1 = (Collection)localObject1;
      if (localObject1 != null)
      {
        localObject2 = new Reaction[0];
        localObject1 = ((Collection)localObject1).toArray((Object[])localObject2);
        if (localObject1 != null)
        {
          localObject1 = (Reaction[])localObject1;
        }
        else
        {
          localObject1 = new c/u;
          ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.Array<T>");
          throw ((Throwable)localObject1);
        }
      }
      else
      {
        localObject1 = new c/u;
        ((u)localObject1).<init>("null cannot be cast to non-null type java.util.Collection<T>");
        throw ((Throwable)localObject1);
      }
    }
    else
    {
      localObject1 = null;
    }
    Object localObject2 = localObject1;
    int i7 = g;
    Participant localParticipant = l;
    ImTransportInfo localImTransportInfo = new com/truecaller/messaging/transport/im/ImTransportInfo;
    localObject1 = localImTransportInfo;
    localImTransportInfo.<init>(l1, str, n, i1, i2, i3, i4, i5, i6, l2, (Reaction[])localObject2, i7, localParticipant, (byte)0);
    return localImTransportInfo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ImTransportInfo.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */