package com.truecaller.messaging.transport.im;

import dagger.a.d;
import javax.inject.Provider;

public final class o
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private o(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
  }
  
  public static o a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    o localo = new com/truecaller/messaging/transport/im/o;
    localo.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */