package com.truecaller.messaging.transport.im;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.WorkerParameters;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.b;
import com.truecaller.bp;
import com.truecaller.common.background.TrackedWorker;

public final class FetchImContactsWorker
  extends TrackedWorker
{
  public static final FetchImContactsWorker.a d;
  public j b;
  public b c;
  
  static
  {
    FetchImContactsWorker.a locala = new com/truecaller/messaging/transport/im/FetchImContactsWorker$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  public FetchImContactsWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final b b()
  {
    b localb = c;
    if (localb == null)
    {
      String str = "analytics";
      k.a(str);
    }
    return localb;
  }
  
  public final boolean c()
  {
    j localj = b;
    if (localj == null)
    {
      String str = "imContactFetcher";
      k.a(str);
    }
    return localj.a();
  }
  
  public final ListenableWorker.a d()
  {
    Object localObject = b;
    if (localObject == null)
    {
      String str = "imContactFetcher";
      k.a(str);
    }
    ((j)localObject).b();
    localObject = ListenableWorker.a.a();
    k.a(localObject, "Result.success()");
    return (ListenableWorker.a)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.FetchImContactsWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */