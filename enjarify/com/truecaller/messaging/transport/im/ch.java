package com.truecaller.messaging.transport.im;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import c.g.b.k;
import c.u;
import com.truecaller.api.services.messenger.v1.events.Event;
import com.truecaller.api.services.messenger.v1.events.Event.a;
import com.truecaller.api.services.messenger.v1.events.Event.a.a;
import io.grpc.ba.a;
import io.grpc.c.f;
import java.lang.ref.WeakReference;

final class ch
  extends Handler
{
  private final WeakReference a;
  
  public ch(az paramaz, Looper paramLooper)
  {
    super(paramLooper);
    paramLooper = new java/lang/ref/WeakReference;
    paramLooper.<init>(paramaz);
    a = paramLooper;
  }
  
  public final void handleMessage(Message paramMessage)
  {
    k.b(paramMessage, "msg");
    Object localObject1 = (az)a.get();
    if (localObject1 == null) {
      return;
    }
    int i = what;
    boolean bool2 = false;
    int[] arrayOfInt = null;
    Object localObject2;
    switch (i)
    {
    default: 
      break;
    case 3: 
      paramMessage = "Subscription completed";
      new String[1][0] = paramMessage;
      break;
    case 2: 
      paramMessage = io.grpc.ba.a((Throwable)obj);
      if (paramMessage != null) {
        paramMessage = paramMessage.a();
      } else {
        paramMessage = null;
      }
      localObject2 = ba.a.n;
      if (paramMessage != localObject2)
      {
        localObject2 = ba.a.o;
        if (paramMessage != localObject2) {
          break;
        }
      }
      else
      {
        bool2 = true;
      }
      ((az)localObject1).a(bool2);
      break;
    }
    paramMessage = obj;
    if (paramMessage != null)
    {
      paramMessage = (Event)paramMessage;
      k.b(paramMessage, "event");
      localObject2 = f;
      boolean bool1 = ((bu)localObject2).b();
      if (!bool1)
      {
        bool1 = c;
        if (!bool1)
        {
          localObject2 = e.a(paramMessage, false);
          arrayOfInt = ba.a;
          int j = ((ProcessResult)localObject2).ordinal();
          j = arrayOfInt[j];
          switch (j)
          {
          default: 
            break;
          case 2: 
            ((az)localObject1).c();
            break;
          case 1: 
            localObject2 = Event.a.a();
            long l = paramMessage.b();
            paramMessage = (Event.a)((Event.a.a)localObject2).a(l).build();
            if (paramMessage != null)
            {
              localObject1 = a;
              if (localObject1 != null) {
                ((f)localObject1).a(paramMessage);
              }
            }
            return;
          }
          return;
        }
      }
      return;
    }
    paramMessage = new c/u;
    paramMessage.<init>("null cannot be cast to non-null type com.truecaller.api.services.messenger.v1.events.Event");
    throw paramMessage;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ch
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */