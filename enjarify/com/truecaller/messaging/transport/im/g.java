package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.analytics.e.a;
import com.truecaller.api.services.messenger.v1.models.MessageContent;
import com.truecaller.api.services.messenger.v1.models.MessageContent.AttachmentCase;
import com.truecaller.api.services.messenger.v1.models.MessageContent.a;
import com.truecaller.api.services.messenger.v1.models.MessageContent.d;
import com.truecaller.api.services.messenger.v1.models.MessageContent.h;
import com.truecaller.api.services.messenger.v1.models.MessageContent.j;
import com.truecaller.messaging.data.providers.c;
import com.truecaller.messaging.data.types.AudioEntity;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.VideoEntity;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import okhttp3.ab;
import okhttp3.e;
import okhttp3.o;
import okhttp3.y;

public final class g
  implements f
{
  private final ContentResolver a;
  private final com.truecaller.util.g b;
  private final y c;
  private final com.truecaller.analytics.b d;
  private final cb e;
  private final cl f;
  private final c g;
  
  public g(ContentResolver paramContentResolver, com.truecaller.util.g paramg, y paramy, com.truecaller.analytics.b paramb, cb paramcb, cl paramcl, c paramc)
  {
    a = paramContentResolver;
    b = paramg;
    c = paramy;
    d = paramb;
    e = paramcb;
    f = paramcl;
    g = paramc;
  }
  
  private final void a(Entity paramEntity, long paramLong, String paramString)
  {
    boolean bool = paramEntity instanceof BinaryEntity;
    if (!bool)
    {
      bool = false;
      localObject1 = null;
    }
    else
    {
      localObject1 = paramEntity;
    }
    Object localObject1 = (BinaryEntity)localObject1;
    long l;
    if (localObject1 != null) {
      l = d;
    } else {
      l = -1;
    }
    float f1 = (float)l / 1024.0F;
    float f2 = (float)TimeUnit.MILLISECONDS.toSeconds(paramLong);
    f1 /= f2;
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("ImAttachmentUpload");
    paramEntity = j;
    paramEntity = locala.a("Type", paramEntity).a("Status", paramString).a("SizeAbsolute", l);
    localObject1 = u.b(l);
    paramEntity = paramEntity.a("SizeBatch", (String)localObject1);
    paramString = Double.valueOf(f1);
    paramEntity = paramEntity.a(paramString);
    Object localObject2 = u.a(paramLong);
    paramEntity = paramEntity.a("TimeBatch", (String)localObject2).a();
    localObject2 = d;
    k.a(paramEntity, "event");
    ((com.truecaller.analytics.b)localObject2).a(paramEntity);
  }
  
  private static void a(y paramy, Object paramObject)
  {
    Object localObject1 = paramy.b().b();
    Object localObject2 = "dispatcher().queuedCalls()";
    k.a(localObject1, (String)localObject2);
    localObject1 = ((Iterable)localObject1).iterator();
    boolean bool1;
    for (;;)
    {
      bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (e)((Iterator)localObject1).next();
      Object localObject3 = ((e)localObject2).a().d();
      boolean bool2 = k.a(localObject3, paramObject);
      if (bool2) {
        ((e)localObject2).c();
      }
    }
    paramy = paramy.b().c();
    localObject1 = "dispatcher().runningCalls()";
    k.a(paramy, (String)localObject1);
    paramy = ((Iterable)paramy).iterator();
    for (;;)
    {
      boolean bool3 = paramy.hasNext();
      if (!bool3) {
        break;
      }
      localObject1 = (e)paramy.next();
      localObject2 = ((e)localObject1).a().d();
      bool1 = k.a(localObject2, paramObject);
      if (bool1) {
        ((e)localObject1).c();
      }
    }
  }
  
  /* Error */
  private final byte[] a(Uri paramUri)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 47	com/truecaller/messaging/transport/im/g:b	Lcom/truecaller/util/g;
    //   4: astore_2
    //   5: aload_2
    //   6: aload_1
    //   7: invokeinterface 182 2 0
    //   12: astore_1
    //   13: aload_1
    //   14: ifnonnull +5 -> 19
    //   17: aconst_null
    //   18: areturn
    //   19: aload_0
    //   20: getfield 47	com/truecaller/messaging/transport/im/g:b	Lcom/truecaller/util/g;
    //   23: astore_2
    //   24: aload_2
    //   25: aload_1
    //   26: invokeinterface 185 2 0
    //   31: astore_2
    //   32: getstatic 191	android/net/Uri:EMPTY	Landroid/net/Uri;
    //   35: astore_3
    //   36: aload_1
    //   37: aload_3
    //   38: invokestatic 171	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/Object;)Z
    //   41: iconst_1
    //   42: ixor
    //   43: istore 4
    //   45: iload 4
    //   47: ifeq +25 -> 72
    //   50: aload_0
    //   51: getfield 57	com/truecaller/messaging/transport/im/g:g	Lcom/truecaller/messaging/data/providers/c;
    //   54: astore_3
    //   55: aload_3
    //   56: aload_1
    //   57: invokeinterface 196 2 0
    //   62: astore_1
    //   63: aload_1
    //   64: ifnull +8 -> 72
    //   67: aload_1
    //   68: invokevirtual 201	java/io/File:delete	()Z
    //   71: pop
    //   72: aload_2
    //   73: areturn
    //   74: astore_2
    //   75: getstatic 191	android/net/Uri:EMPTY	Landroid/net/Uri;
    //   78: astore_3
    //   79: aload_1
    //   80: aload_3
    //   81: invokestatic 171	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/Object;)Z
    //   84: iconst_1
    //   85: ixor
    //   86: istore 4
    //   88: iload 4
    //   90: ifeq +25 -> 115
    //   93: aload_0
    //   94: getfield 57	com/truecaller/messaging/transport/im/g:g	Lcom/truecaller/messaging/data/providers/c;
    //   97: astore_3
    //   98: aload_3
    //   99: aload_1
    //   100: invokeinterface 196 2 0
    //   105: astore_1
    //   106: aload_1
    //   107: ifnull +8 -> 115
    //   110: aload_1
    //   111: invokevirtual 201	java/io/File:delete	()Z
    //   114: pop
    //   115: aload_2
    //   116: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	117	0	this	g
    //   0	117	1	paramUri	Uri
    //   4	69	2	localObject1	Object
    //   74	42	2	localObject2	Object
    //   35	64	3	localObject3	Object
    //   43	46	4	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   19	23	74	finally
    //   25	31	74	finally
  }
  
  public final Uri a(long paramLong, byte[] paramArrayOfByte, int paramInt1, int paramInt2, String paramString)
  {
    k.b(paramArrayOfByte, "thumbnail");
    k.b(paramString, "type");
    c localc = g;
    Object localObject1 = localc.a(paramLong);
    if (localObject1 != null)
    {
      Object localObject2 = b;
      double d1 = paramInt1;
      double d2 = 0.08D;
      Double.isNaN(d1);
      d1 *= d2;
      int i = (int)d1;
      localObject2 = ((com.truecaller.util.g)localObject2).a(paramArrayOfByte, paramInt1, paramInt2, i);
      if (localObject2 != null)
      {
        paramArrayOfByte = new com/truecaller/messaging/transport/im/g$a;
        paramArrayOfByte.<init>((File)localObject1);
        paramArrayOfByte = (c.g.a.b)paramArrayOfByte;
        com.truecaller.utils.extensions.b.a((Bitmap)localObject2, paramArrayOfByte);
      }
      localObject2 = g;
      paramArrayOfByte = null;
      localObject1 = ((c)localObject2).a((File)localObject1, paramString, false);
      if (localObject1 != null) {
        return (Uri)localObject1;
      }
      localObject1 = new java/io/IOException;
      ((IOException)localObject1).<init>("Invalid attachment's file URI");
      throw ((Throwable)localObject1);
    }
    localObject1 = new java/io/IOException;
    ((IOException)localObject1).<init>("Can't create file for attachment");
    throw ((Throwable)localObject1);
  }
  
  public final Entity a(MessageContent paramMessageContent, int paramInt)
  {
    Object localObject1 = paramMessageContent;
    k.b(paramMessageContent, "content");
    Object localObject2 = paramMessageContent.a();
    Object localObject3 = null;
    if (localObject2 != null)
    {
      Object localObject4 = h.a;
      int i = ((MessageContent.AttachmentCase)localObject2).ordinal();
      i = localObject4[i];
      String str1;
      Object localObject5;
      long l2;
      int j;
      Object localObject6;
      int k;
      switch (i)
      {
      default: 
        break;
      case 5: 
        return null;
      case 4: 
        localObject2 = paramMessageContent.d();
        k.a(localObject2, "content.vcard");
        localObject3 = ((MessageContent.h)localObject2).a();
        long l1 = ((MessageContent.h)localObject2).b();
        i = paramInt;
        localObject2 = Entity.a("text/vcard", paramInt, (String)localObject3, l1);
        k.a(localObject2, "Entity.create(Entity.CON…atus, uri, size.toLong())");
        return (Entity)localObject2;
      case 3: 
        i = paramInt;
        localObject1 = paramMessageContent.f();
        k.a(localObject1, "content.audio");
        localObject3 = new com/truecaller/messaging/data/types/AudioEntity;
        str1 = ((MessageContent.a)localObject1).a();
        k.a(str1, "mimeType");
        localObject5 = Uri.parse(((MessageContent.a)localObject1).b());
        k.a(localObject5, "Uri.parse(uri)");
        l2 = ((MessageContent.a)localObject1).c();
        j = ((MessageContent.a)localObject1).d();
        ((AudioEntity)localObject3).<init>(-1, str1, paramInt, (Uri)localObject5, l2, j);
        return (Entity)localObject3;
      case 2: 
        i = paramInt;
        localObject1 = paramMessageContent.e();
        k.a(localObject1, "content.video");
        localObject6 = ((MessageContent.j)localObject1).g().toByteArray();
        k.a(localObject6, "thumbnail.toByteArray()");
        k = ((MessageContent.j)localObject1).d();
        int m = ((MessageContent.j)localObject1).e();
        String str2 = ((MessageContent.j)localObject1).a();
        k.a(str2, "mimeType");
        localObject4 = this;
        localObject3 = a(-1, (byte[])localObject6, k, m, str2);
        localObject4 = new com/truecaller/messaging/data/types/VideoEntity;
        str1 = ((MessageContent.j)localObject1).a();
        k.a(str1, "mimeType");
        localObject5 = ((MessageContent.j)localObject1).b();
        k.a(localObject5, "uri");
        l2 = ((MessageContent.j)localObject1).c();
        j = ((MessageContent.j)localObject1).d();
        int n = ((MessageContent.j)localObject1).e();
        int i1 = ((MessageContent.j)localObject1).f();
        localObject1 = ((Uri)localObject3).toString();
        k.a(localObject1, "thumbnailUrl.toString()");
        ((VideoEntity)localObject4).<init>(-1, str1, paramInt, (String)localObject5, l2, j, n, i1, (String)localObject1);
        return (Entity)localObject4;
      case 1: 
        i = paramInt;
        localObject1 = paramMessageContent.c();
        k.a(localObject1, "content.image");
        localObject3 = new com/truecaller/messaging/transport/im/ImageEntityWithThumbnail;
        localObject6 = ((MessageContent.d)localObject1).a();
        k.a(localObject6, "mimeType");
        str1 = ((MessageContent.d)localObject1).b();
        k.a(str1, "uri");
        long l3 = ((MessageContent.d)localObject1).c();
        int i2 = ((MessageContent.d)localObject1).d();
        int i3 = ((MessageContent.d)localObject1).e();
        byte[] arrayOfByte = ((MessageContent.d)localObject1).f().toByteArray();
        k.a(arrayOfByte, "thumbnail.toByteArray()");
        k = paramInt;
        ((ImageEntityWithThumbnail)localObject3).<init>((String)localObject6, paramInt, str1, l3, i2, i3, arrayOfByte);
        return (Entity)localObject3;
      }
    }
    localObject2 = new String[1];
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Found unsupported attachment ");
    localObject1 = paramMessageContent.a();
    localStringBuilder.append(localObject1);
    localObject1 = localStringBuilder.toString();
    localObject2[0] = localObject1;
    return null;
  }
  
  /* Error */
  public final void a(com.truecaller.api.services.messenger.v1.models.input.InputMessageContent.c paramc, BinaryEntity paramBinaryEntity, com.truecaller.messaging.data.types.Message paramMessage)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 382
    //   4: invokestatic 27	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: aload_2
    //   8: ldc_w 384
    //   11: invokestatic 27	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   14: ldc_w 386
    //   17: astore 4
    //   19: aload_3
    //   20: aload 4
    //   22: invokestatic 27	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   25: aload_2
    //   26: astore_3
    //   27: aload_2
    //   28: checkcast 85	com/truecaller/messaging/data/types/Entity
    //   31: astore_3
    //   32: aload_3
    //   33: invokevirtual 388	com/truecaller/messaging/data/types/Entity:b	()Z
    //   36: istore 5
    //   38: iconst_0
    //   39: istore 6
    //   41: aconst_null
    //   42: astore 7
    //   44: iconst_1
    //   45: istore 8
    //   47: iload 5
    //   49: ifne +48 -> 97
    //   52: aload_3
    //   53: invokevirtual 390	com/truecaller/messaging/data/types/Entity:c	()Z
    //   56: istore 5
    //   58: iload 5
    //   60: ifne +37 -> 97
    //   63: aload_3
    //   64: invokevirtual 392	com/truecaller/messaging/data/types/Entity:d	()Z
    //   67: istore 5
    //   69: iload 5
    //   71: ifne +26 -> 97
    //   74: aload_3
    //   75: invokevirtual 394	com/truecaller/messaging/data/types/Entity:e	()Z
    //   78: istore 5
    //   80: iload 5
    //   82: ifeq +6 -> 88
    //   85: goto +12 -> 97
    //   88: iconst_0
    //   89: istore 5
    //   91: aconst_null
    //   92: astore 4
    //   94: goto +6 -> 100
    //   97: iconst_1
    //   98: istore 5
    //   100: iload 5
    //   102: ifne +4 -> 106
    //   105: return
    //   106: aload_0
    //   107: getfield 53	com/truecaller/messaging/transport/im/g:e	Lcom/truecaller/messaging/transport/im/cb;
    //   110: invokestatic 399	com/truecaller/network/d/j$a:a	(Lcom/truecaller/network/d/j;)Lio/grpc/c/a;
    //   113: checkcast 401	com/truecaller/api/services/messenger/v1/k$a
    //   116: astore 4
    //   118: aload 4
    //   120: ifnonnull +4 -> 124
    //   123: return
    //   124: invokestatic 406	com/truecaller/api/services/messenger/v1/MediaHandles$Request:a	()Lcom/truecaller/api/services/messenger/v1/MediaHandles$Request$a;
    //   127: astore 9
    //   129: aload_2
    //   130: getfield 62	com/truecaller/messaging/data/types/BinaryEntity:d	J
    //   133: lstore 10
    //   135: aload 9
    //   137: lload 10
    //   139: invokevirtual 411	com/truecaller/api/services/messenger/v1/MediaHandles$Request$a:a	(J)Lcom/truecaller/api/services/messenger/v1/MediaHandles$Request$a;
    //   142: astore 9
    //   144: aload_2
    //   145: getfield 412	com/truecaller/messaging/data/types/BinaryEntity:j	Ljava/lang/String;
    //   148: astore 12
    //   150: aload 9
    //   152: aload 12
    //   154: invokevirtual 415	com/truecaller/api/services/messenger/v1/MediaHandles$Request$a:a	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/MediaHandles$Request$a;
    //   157: invokevirtual 419	com/truecaller/api/services/messenger/v1/MediaHandles$Request$a:build	()Lcom/google/f/q;
    //   160: checkcast 403	com/truecaller/api/services/messenger/v1/MediaHandles$Request
    //   163: astore 9
    //   165: aload 4
    //   167: aload 9
    //   169: invokevirtual 422	com/truecaller/api/services/messenger/v1/k$a:a	(Lcom/truecaller/api/services/messenger/v1/MediaHandles$Request;)Lcom/truecaller/api/services/messenger/v1/MediaHandles$c;
    //   172: astore 4
    //   174: aload 4
    //   176: ldc_w 424
    //   179: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   182: aload 4
    //   184: invokevirtual 429	com/truecaller/api/services/messenger/v1/MediaHandles$c:c	()Ljava/util/Map;
    //   187: astore 9
    //   189: aload 9
    //   191: ldc_w 431
    //   194: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   197: aload 4
    //   199: invokevirtual 432	com/truecaller/api/services/messenger/v1/MediaHandles$c:a	()Ljava/lang/String;
    //   202: astore 12
    //   204: aload 12
    //   206: ldc_w 434
    //   209: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   212: new 436	okhttp3/x$a
    //   215: astore 13
    //   217: aload 13
    //   219: invokespecial 437	okhttp3/x$a:<init>	()V
    //   222: getstatic 442	okhttp3/x:e	Lokhttp3/w;
    //   225: astore 14
    //   227: aload 13
    //   229: aload 14
    //   231: invokevirtual 445	okhttp3/x$a:a	(Lokhttp3/w;)Lokhttp3/x$a;
    //   234: astore 13
    //   236: aload 9
    //   238: invokeinterface 451 1 0
    //   243: invokeinterface 454 1 0
    //   248: astore 9
    //   250: aload 9
    //   252: invokeinterface 155 1 0
    //   257: istore 15
    //   259: iload 15
    //   261: ifeq +52 -> 313
    //   264: aload 9
    //   266: invokeinterface 159 1 0
    //   271: checkcast 456	java/util/Map$Entry
    //   274: astore 14
    //   276: aload 14
    //   278: invokeinterface 459 1 0
    //   283: checkcast 370	java/lang/String
    //   286: astore 16
    //   288: aload 14
    //   290: invokeinterface 462 1 0
    //   295: checkcast 370	java/lang/String
    //   298: astore 14
    //   300: aload 13
    //   302: aload 16
    //   304: aload 14
    //   306: invokevirtual 465	okhttp3/x$a:a	(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/x$a;
    //   309: pop
    //   310: goto -60 -> 250
    //   313: aload_2
    //   314: getfield 469	com/truecaller/messaging/data/types/BinaryEntity:b	Landroid/net/Uri;
    //   317: astore 14
    //   319: aload 14
    //   321: ldc_w 471
    //   324: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   327: aload 14
    //   329: invokevirtual 474	android/net/Uri:getPathSegments	()Ljava/util/List;
    //   332: astore 14
    //   334: aload 14
    //   336: ldc_w 476
    //   339: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   342: aload 14
    //   344: invokestatic 481	c/a/m:f	(Ljava/util/List;)Ljava/lang/Object;
    //   347: checkcast 370	java/lang/String
    //   350: astore 14
    //   352: new 483	com/truecaller/messaging/transport/im/c
    //   355: astore 16
    //   357: aload_0
    //   358: getfield 45	com/truecaller/messaging/transport/im/g:a	Landroid/content/ContentResolver;
    //   361: astore 17
    //   363: aload 16
    //   365: aload 17
    //   367: aload_2
    //   368: invokespecial 486	com/truecaller/messaging/transport/im/c:<init>	(Landroid/content/ContentResolver;Lcom/truecaller/messaging/data/types/BinaryEntity;)V
    //   371: aload 16
    //   373: checkcast 488	okhttp3/ac
    //   376: astore 16
    //   378: aload 13
    //   380: ldc_w 467
    //   383: aload 14
    //   385: aload 16
    //   387: invokevirtual 491	okhttp3/x$a:a	(Ljava/lang/String;Ljava/lang/String;Lokhttp3/ac;)Lokhttp3/x$a;
    //   390: invokevirtual 494	okhttp3/x$a:a	()Lokhttp3/x;
    //   393: astore 9
    //   395: aload 9
    //   397: ldc_w 496
    //   400: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   403: aload 9
    //   405: checkcast 488	okhttp3/ac
    //   408: astore 9
    //   410: new 498	okhttp3/ab$a
    //   413: astore 13
    //   415: aload 13
    //   417: invokespecial 499	okhttp3/ab$a:<init>	()V
    //   420: aload 13
    //   422: aload 12
    //   424: invokevirtual 502	okhttp3/ab$a:a	(Ljava/lang/String;)Lokhttp3/ab$a;
    //   427: astore 12
    //   429: aload_2
    //   430: getfield 505	com/truecaller/messaging/data/types/BinaryEntity:h	J
    //   433: lstore 18
    //   435: lload 18
    //   437: invokestatic 510	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   440: astore 13
    //   442: aload 12
    //   444: aload 13
    //   446: invokevirtual 513	okhttp3/ab$a:a	(Ljava/lang/Object;)Lokhttp3/ab$a;
    //   449: astore 12
    //   451: aload 12
    //   453: aload 9
    //   455: invokevirtual 516	okhttp3/ab$a:a	(Lokhttp3/ac;)Lokhttp3/ab$a;
    //   458: invokevirtual 517	okhttp3/ab$a:a	()Lokhttp3/ab;
    //   461: astore 9
    //   463: invokestatic 523	java/lang/System:currentTimeMillis	()J
    //   466: lstore 10
    //   468: aload_0
    //   469: getfield 49	com/truecaller/messaging/transport/im/g:c	Lokhttp3/y;
    //   472: aload 9
    //   474: invokevirtual 526	okhttp3/y:a	(Lokhttp3/ab;)Lokhttp3/e;
    //   477: astore 9
    //   479: ldc_w 528
    //   482: astore 14
    //   484: aload 9
    //   486: aload 14
    //   488: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   491: aload 9
    //   493: invokestatic 534	com/google/firebase/perf/network/FirebasePerfOkHttpClient:execute	(Lokhttp3/e;)Lokhttp3/ad;
    //   496: astore 14
    //   498: aload 14
    //   500: checkcast 536	java/io/Closeable
    //   503: astore 14
    //   505: aconst_null
    //   506: astore 16
    //   508: aload 14
    //   510: astore 17
    //   512: aload 14
    //   514: checkcast 538	okhttp3/ad
    //   517: astore 17
    //   519: ldc_w 540
    //   522: astore 20
    //   524: aload 17
    //   526: aload 20
    //   528: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   531: aload 17
    //   533: invokevirtual 541	okhttp3/ad:c	()Z
    //   536: istore 21
    //   538: iload 21
    //   540: ifne +138 -> 678
    //   543: aload_2
    //   544: checkcast 85	com/truecaller/messaging/data/types/Entity
    //   547: astore_2
    //   548: invokestatic 523	java/lang/System:currentTimeMillis	()J
    //   551: lload 10
    //   553: lsub
    //   554: lstore 22
    //   556: ldc_w 543
    //   559: astore_1
    //   560: aload_0
    //   561: aload_2
    //   562: lload 22
    //   564: aload_1
    //   565: invokespecial 546	com/truecaller/messaging/transport/im/g:a	(Lcom/truecaller/messaging/data/types/Entity;JLjava/lang/String;)V
    //   568: new 548	com/truecaller/messaging/transport/im/cj
    //   571: astore_1
    //   572: ldc_w 540
    //   575: astore_2
    //   576: aload 17
    //   578: aload_2
    //   579: invokestatic 27	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   582: aload 17
    //   584: invokevirtual 549	okhttp3/ad:b	()I
    //   587: istore 24
    //   589: sipush 400
    //   592: istore 25
    //   594: iload 24
    //   596: iload 25
    //   598: if_icmpne +39 -> 637
    //   601: aload 17
    //   603: invokevirtual 553	okhttp3/ad:d	()Lokhttp3/ae;
    //   606: astore_2
    //   607: aload_2
    //   608: invokestatic 558	com/truecaller/messaging/transport/im/cl:a	(Lokhttp3/ae;)Ljava/lang/String;
    //   611: astore_2
    //   612: ldc_w 560
    //   615: astore_3
    //   616: aload_2
    //   617: aload_3
    //   618: invokestatic 171	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/Object;)Z
    //   621: istore 24
    //   623: iload 24
    //   625: ifne +6 -> 631
    //   628: goto +9 -> 637
    //   631: iconst_3
    //   632: istore 24
    //   634: goto +31 -> 665
    //   637: aload 17
    //   639: invokevirtual 549	okhttp3/ad:b	()I
    //   642: istore 24
    //   644: sipush 403
    //   647: istore 25
    //   649: iload 24
    //   651: iload 25
    //   653: if_icmpne +9 -> 662
    //   656: iconst_2
    //   657: istore 24
    //   659: goto +6 -> 665
    //   662: iconst_1
    //   663: istore 24
    //   665: aload_1
    //   666: iload 24
    //   668: invokespecial 566	com/truecaller/messaging/transport/im/cj:<init>	(I)V
    //   671: aload_1
    //   672: checkcast 241	java/lang/Throwable
    //   675: astore_1
    //   676: aload_1
    //   677: athrow
    //   678: getstatic 571	c/x:a	Lc/x;
    //   681: astore 17
    //   683: aload 14
    //   685: aconst_null
    //   686: invokestatic 576	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   689: invokestatic 523	java/lang/System:currentTimeMillis	()J
    //   692: lload 10
    //   694: lsub
    //   695: lstore 26
    //   697: ldc_w 578
    //   700: astore 9
    //   702: aload_0
    //   703: aload_3
    //   704: lload 26
    //   706: aload 9
    //   708: invokespecial 546	com/truecaller/messaging/transport/im/g:a	(Lcom/truecaller/messaging/data/types/Entity;JLjava/lang/String;)V
    //   711: aload_2
    //   712: invokevirtual 579	com/truecaller/messaging/data/types/BinaryEntity:b	()Z
    //   715: istore 25
    //   717: iload 25
    //   719: ifeq +180 -> 899
    //   722: aload_2
    //   723: astore_3
    //   724: aload_2
    //   725: checkcast 581	com/truecaller/messaging/data/types/ImageEntity
    //   728: astore_3
    //   729: aload 4
    //   731: invokevirtual 582	com/truecaller/api/services/messenger/v1/MediaHandles$c:b	()Ljava/lang/String;
    //   734: astore 4
    //   736: aload 4
    //   738: ldc_w 584
    //   741: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   744: aload_0
    //   745: getfield 47	com/truecaller/messaging/transport/im/g:b	Lcom/truecaller/util/g;
    //   748: astore 7
    //   750: aload_2
    //   751: getfield 469	com/truecaller/messaging/data/types/BinaryEntity:b	Landroid/net/Uri;
    //   754: astore_2
    //   755: ldc_w 471
    //   758: astore 28
    //   760: aload_2
    //   761: aload 28
    //   763: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   766: aload 7
    //   768: aload_2
    //   769: invokeinterface 185 2 0
    //   774: astore_2
    //   775: invokestatic 589	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d:a	()Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a;
    //   778: aload 4
    //   780: invokevirtual 594	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a:b	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a;
    //   783: astore 4
    //   785: aload_3
    //   786: getfield 595	com/truecaller/messaging/data/types/ImageEntity:j	Ljava/lang/String;
    //   789: astore 7
    //   791: aload 4
    //   793: aload 7
    //   795: invokevirtual 597	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a:a	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a;
    //   798: astore 4
    //   800: aload_3
    //   801: getfield 600	com/truecaller/messaging/data/types/ImageEntity:a	I
    //   804: istore 6
    //   806: aload 4
    //   808: iload 6
    //   810: invokevirtual 603	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a:b	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a;
    //   813: astore 4
    //   815: aload_3
    //   816: getfield 606	com/truecaller/messaging/data/types/ImageEntity:k	I
    //   819: istore 6
    //   821: aload 4
    //   823: iload 6
    //   825: invokevirtual 608	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a:c	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a;
    //   828: astore 4
    //   830: aload_3
    //   831: getfield 609	com/truecaller/messaging/data/types/ImageEntity:d	J
    //   834: lstore 29
    //   836: lload 29
    //   838: l2i
    //   839: istore 25
    //   841: aload 4
    //   843: iload 25
    //   845: invokevirtual 611	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a:a	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a;
    //   848: astore_3
    //   849: aload_2
    //   850: ifnull +25 -> 875
    //   853: ldc_w 382
    //   856: astore 4
    //   858: aload_3
    //   859: aload 4
    //   861: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   864: aload_2
    //   865: invokestatic 615	com/google/f/f:copyFrom	([B)Lcom/google/f/f;
    //   868: astore_2
    //   869: aload_3
    //   870: aload_2
    //   871: invokevirtual 618	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a:a	(Lcom/google/f/f;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a;
    //   874: pop
    //   875: aload_3
    //   876: invokevirtual 619	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d$a:build	()Lcom/google/f/q;
    //   879: astore_2
    //   880: aload_2
    //   881: ldc_w 621
    //   884: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   887: aload_2
    //   888: checkcast 586	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d
    //   891: astore_2
    //   892: aload_1
    //   893: aload_2
    //   894: invokevirtual 626	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c:a	(Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$d;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c;
    //   897: pop
    //   898: return
    //   899: aload_2
    //   900: invokevirtual 627	com/truecaller/messaging/data/types/BinaryEntity:c	()Z
    //   903: istore 25
    //   905: iload 25
    //   907: ifeq +182 -> 1089
    //   910: aload_2
    //   911: astore_3
    //   912: aload_2
    //   913: checkcast 334	com/truecaller/messaging/data/types/VideoEntity
    //   916: astore_3
    //   917: aload 4
    //   919: invokevirtual 582	com/truecaller/api/services/messenger/v1/MediaHandles$c:b	()Ljava/lang/String;
    //   922: astore 4
    //   924: aload 4
    //   926: ldc_w 584
    //   929: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   932: aload_2
    //   933: getfield 469	com/truecaller/messaging/data/types/BinaryEntity:b	Landroid/net/Uri;
    //   936: astore_2
    //   937: aload_2
    //   938: ldc_w 471
    //   941: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   944: aload_0
    //   945: aload_2
    //   946: invokespecial 629	com/truecaller/messaging/transport/im/g:a	(Landroid/net/Uri;)[B
    //   949: astore_2
    //   950: invokestatic 634	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j:a	()Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a;
    //   953: aload 4
    //   955: invokevirtual 639	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a:b	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a;
    //   958: astore 4
    //   960: aload_3
    //   961: getfield 640	com/truecaller/messaging/data/types/VideoEntity:j	Ljava/lang/String;
    //   964: astore 7
    //   966: aload 4
    //   968: aload 7
    //   970: invokevirtual 642	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a:a	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a;
    //   973: astore 4
    //   975: aload_3
    //   976: getfield 643	com/truecaller/messaging/data/types/VideoEntity:d	J
    //   979: lstore 29
    //   981: lload 29
    //   983: l2i
    //   984: istore 8
    //   986: aload 4
    //   988: iload 8
    //   990: invokevirtual 646	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a:a	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a;
    //   993: astore 4
    //   995: aload_3
    //   996: getfield 647	com/truecaller/messaging/data/types/VideoEntity:a	I
    //   999: istore 6
    //   1001: aload 4
    //   1003: iload 6
    //   1005: invokevirtual 649	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a:b	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a;
    //   1008: astore 4
    //   1010: aload_3
    //   1011: getfield 650	com/truecaller/messaging/data/types/VideoEntity:k	I
    //   1014: istore 6
    //   1016: aload 4
    //   1018: iload 6
    //   1020: invokevirtual 652	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a:c	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a;
    //   1023: astore 4
    //   1025: aload_3
    //   1026: getfield 655	com/truecaller/messaging/data/types/VideoEntity:l	I
    //   1029: istore 25
    //   1031: aload 4
    //   1033: iload 25
    //   1035: invokevirtual 657	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a:d	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a;
    //   1038: astore_3
    //   1039: aload_2
    //   1040: ifnull +25 -> 1065
    //   1043: ldc_w 382
    //   1046: astore 4
    //   1048: aload_3
    //   1049: aload 4
    //   1051: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1054: aload_2
    //   1055: invokestatic 615	com/google/f/f:copyFrom	([B)Lcom/google/f/f;
    //   1058: astore_2
    //   1059: aload_3
    //   1060: aload_2
    //   1061: invokevirtual 660	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a:a	(Lcom/google/f/f;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a;
    //   1064: pop
    //   1065: aload_3
    //   1066: invokevirtual 661	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j$a:build	()Lcom/google/f/q;
    //   1069: astore_2
    //   1070: aload_2
    //   1071: ldc_w 621
    //   1074: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1077: aload_2
    //   1078: checkcast 631	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j
    //   1081: astore_2
    //   1082: aload_1
    //   1083: aload_2
    //   1084: invokevirtual 664	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c:a	(Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$j;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c;
    //   1087: pop
    //   1088: return
    //   1089: aload_2
    //   1090: invokevirtual 665	com/truecaller/messaging/data/types/BinaryEntity:e	()Z
    //   1093: istore 25
    //   1095: iload 25
    //   1097: ifeq +91 -> 1188
    //   1100: aload_2
    //   1101: checkcast 287	com/truecaller/messaging/data/types/AudioEntity
    //   1104: astore_2
    //   1105: aload 4
    //   1107: invokevirtual 582	com/truecaller/api/services/messenger/v1/MediaHandles$c:b	()Ljava/lang/String;
    //   1110: astore_3
    //   1111: aload_3
    //   1112: ldc_w 584
    //   1115: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1118: invokestatic 670	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a:a	()Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a;
    //   1121: aload_3
    //   1122: invokevirtual 675	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a:b	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a;
    //   1125: astore_3
    //   1126: aload_2
    //   1127: getfield 676	com/truecaller/messaging/data/types/AudioEntity:j	Ljava/lang/String;
    //   1130: astore 4
    //   1132: aload_3
    //   1133: aload 4
    //   1135: invokevirtual 678	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a:a	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a;
    //   1138: astore_3
    //   1139: aload_2
    //   1140: getfield 679	com/truecaller/messaging/data/types/AudioEntity:d	J
    //   1143: l2i
    //   1144: istore 6
    //   1146: aload_3
    //   1147: iload 6
    //   1149: invokevirtual 682	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a:a	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a;
    //   1152: astore_3
    //   1153: aload_2
    //   1154: getfield 683	com/truecaller/messaging/data/types/AudioEntity:a	I
    //   1157: istore 24
    //   1159: aload_3
    //   1160: iload 24
    //   1162: invokevirtual 685	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a:b	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a;
    //   1165: invokevirtual 686	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a$a:build	()Lcom/google/f/q;
    //   1168: astore_2
    //   1169: aload_2
    //   1170: ldc_w 621
    //   1173: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1176: aload_2
    //   1177: checkcast 667	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a
    //   1180: astore_2
    //   1181: aload_1
    //   1182: aload_2
    //   1183: invokevirtual 689	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c:a	(Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$a;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c;
    //   1186: pop
    //   1187: return
    //   1188: aload_2
    //   1189: invokevirtual 690	com/truecaller/messaging/data/types/BinaryEntity:d	()Z
    //   1192: istore 25
    //   1194: iload 25
    //   1196: ifeq +60 -> 1256
    //   1199: aload 4
    //   1201: invokevirtual 582	com/truecaller/api/services/messenger/v1/MediaHandles$c:b	()Ljava/lang/String;
    //   1204: astore_3
    //   1205: aload_3
    //   1206: ldc_w 584
    //   1209: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1212: invokestatic 695	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h:a	()Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h$a;
    //   1215: aload_3
    //   1216: invokevirtual 700	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h$a:a	(Ljava/lang/String;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h$a;
    //   1219: astore_3
    //   1220: aload_2
    //   1221: getfield 62	com/truecaller/messaging/data/types/BinaryEntity:d	J
    //   1224: l2i
    //   1225: istore 24
    //   1227: aload_3
    //   1228: iload 24
    //   1230: invokevirtual 703	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h$a:a	(I)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h$a;
    //   1233: invokevirtual 704	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h$a:build	()Lcom/google/f/q;
    //   1236: astore_2
    //   1237: aload_2
    //   1238: ldc_w 706
    //   1241: invokestatic 126	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   1244: aload_2
    //   1245: checkcast 692	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h
    //   1248: astore_2
    //   1249: aload_1
    //   1250: aload_2
    //   1251: invokevirtual 709	com/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c:a	(Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$h;)Lcom/truecaller/api/services/messenger/v1/models/input/InputMessageContent$c;
    //   1254: pop
    //   1255: return
    //   1256: iload 8
    //   1258: anewarray 370	java/lang/String
    //   1261: astore_1
    //   1262: new 372	java/lang/StringBuilder
    //   1265: astore_3
    //   1266: aload_3
    //   1267: ldc_w 711
    //   1270: invokespecial 375	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1273: aload_2
    //   1274: getfield 412	com/truecaller/messaging/data/types/BinaryEntity:j	Ljava/lang/String;
    //   1277: astore_2
    //   1278: aload_3
    //   1279: aload_2
    //   1280: invokevirtual 714	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1283: pop
    //   1284: aload_3
    //   1285: invokevirtual 380	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1288: astore_2
    //   1289: aload_1
    //   1290: iconst_0
    //   1291: aload_2
    //   1292: aastore
    //   1293: aload_1
    //   1294: invokestatic 720	com/truecaller/log/AssertionUtil$OnlyInDebug:fail	([Ljava/lang/String;)V
    //   1297: return
    //   1298: astore_1
    //   1299: goto +9 -> 1308
    //   1302: astore_1
    //   1303: aload_1
    //   1304: astore 16
    //   1306: aload_1
    //   1307: athrow
    //   1308: aload 14
    //   1310: aload 16
    //   1312: invokestatic 576	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   1315: aload_1
    //   1316: athrow
    //   1317: aload 9
    //   1319: invokeinterface 721 1 0
    //   1324: istore 31
    //   1326: iload 31
    //   1328: ifeq +16 -> 1344
    //   1331: new 723	java/util/concurrent/CancellationException
    //   1334: astore_1
    //   1335: aload_1
    //   1336: invokespecial 724	java/util/concurrent/CancellationException:<init>	()V
    //   1339: aload_1
    //   1340: checkcast 241	java/lang/Throwable
    //   1343: athrow
    //   1344: new 548	com/truecaller/messaging/transport/im/cj
    //   1347: astore_1
    //   1348: aload_1
    //   1349: iload 8
    //   1351: invokespecial 566	com/truecaller/messaging/transport/im/cj:<init>	(I)V
    //   1354: aload_1
    //   1355: checkcast 241	java/lang/Throwable
    //   1358: athrow
    //   1359: pop
    //   1360: goto -43 -> 1317
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1363	0	this	g
    //   0	1363	1	paramc	com.truecaller.api.services.messenger.v1.models.input.InputMessageContent.c
    //   0	1363	2	paramBinaryEntity	BinaryEntity
    //   0	1363	3	paramMessage	com.truecaller.messaging.data.types.Message
    //   17	1183	4	localObject1	Object
    //   36	65	5	bool1	boolean
    //   39	1109	6	i	int
    //   42	927	7	localObject2	Object
    //   45	1305	8	j	int
    //   127	1191	9	localObject3	Object
    //   133	560	10	l1	long
    //   148	304	12	localObject4	Object
    //   215	230	13	localObject5	Object
    //   225	1084	14	localObject6	Object
    //   257	3	15	bool2	boolean
    //   286	1025	16	localObject7	Object
    //   361	321	17	localObject8	Object
    //   433	3	18	l2	long
    //   522	5	20	str1	String
    //   536	3	21	bool3	boolean
    //   554	9	22	l3	long
    //   587	12	24	k	int
    //   621	3	24	bool4	boolean
    //   632	597	24	m	int
    //   592	62	25	n	int
    //   715	3	25	bool5	boolean
    //   839	5	25	i1	int
    //   903	3	25	bool6	boolean
    //   1029	5	25	i2	int
    //   1093	102	25	bool7	boolean
    //   695	10	26	l4	long
    //   758	4	28	str2	String
    //   834	148	29	l5	long
    //   1324	3	31	bool8	boolean
    //   1359	1	34	localIOException	IOException
    // Exception table:
    //   from	to	target	type
    //   1306	1308	1298	finally
    //   512	517	1302	finally
    //   526	531	1302	finally
    //   531	536	1302	finally
    //   543	547	1302	finally
    //   548	551	1302	finally
    //   564	568	1302	finally
    //   568	571	1302	finally
    //   578	582	1302	finally
    //   582	587	1302	finally
    //   601	606	1302	finally
    //   607	611	1302	finally
    //   617	621	1302	finally
    //   637	642	1302	finally
    //   666	671	1302	finally
    //   671	675	1302	finally
    //   676	678	1302	finally
    //   678	681	1302	finally
    //   491	496	1359	java/io/IOException
    //   498	503	1359	java/io/IOException
    //   685	689	1359	java/io/IOException
    //   1310	1315	1359	java/io/IOException
    //   1315	1317	1359	java/io/IOException
  }
  
  public final void a(BinaryEntity paramBinaryEntity)
  {
    k.b(paramBinaryEntity, "entity");
    Object localObject1 = new String[1];
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>("Cancel uploading entity: ");
    long l = h;
    ((StringBuilder)localObject2).append(l);
    localObject2 = ((StringBuilder)localObject2).toString();
    localObject1[0] = localObject2;
    localObject1 = c;
    paramBinaryEntity = Long.valueOf(h);
    a((y)localObject1, paramBinaryEntity);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */