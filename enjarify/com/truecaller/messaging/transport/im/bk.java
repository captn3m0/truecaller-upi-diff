package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.events.Event;

public final class bk
  implements bj
{
  private final v a;
  
  public bk(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return bj.class.equals(paramClass);
  }
  
  public final w a()
  {
    v localv = a;
    bk.a locala = new com/truecaller/messaging/transport/im/bk$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, (byte)0);
    return w.a(localv, locala);
  }
  
  public final void a(Event paramEvent, int paramInt)
  {
    v localv = a;
    bk.b localb = new com/truecaller/messaging/transport/im/bk$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramEvent, paramInt, (byte)0);
    localv.a(localb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bk
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */