package com.truecaller.messaging.transport.im;

import android.content.ContentValues;
import android.os.Parcel;
import c.g.b.k;
import com.truecaller.messaging.data.types.BinaryEntity;

public final class ImageEntityWithThumbnail
  extends BinaryEntity
{
  public static final ImageEntityWithThumbnail.a CREATOR;
  final int a;
  final int k;
  final byte[] l;
  
  static
  {
    ImageEntityWithThumbnail.a locala = new com/truecaller/messaging/transport/im/ImageEntityWithThumbnail$a;
    locala.<init>((byte)0);
    CREATOR = locala;
  }
  
  public ImageEntityWithThumbnail(Parcel paramParcel)
  {
    super(paramParcel);
    int i = paramParcel.readInt();
    a = i;
    i = paramParcel.readInt();
    k = i;
    paramParcel = paramParcel.createByteArray();
    k.a(paramParcel, "source.createByteArray()");
    l = paramParcel;
  }
  
  public ImageEntityWithThumbnail(String paramString1, int paramInt1, String paramString2, long paramLong, int paramInt2, int paramInt3, byte[] paramArrayOfByte)
  {
    super(-1, paramString1, paramInt1, paramString2, paramLong);
    a = paramInt2;
    k = paramInt3;
    l = paramArrayOfByte;
  }
  
  public final void a(ContentValues paramContentValues)
  {
    k.b(paramContentValues, "contentValues");
    Object localObject = j;
    paramContentValues.put("type", (String)localObject);
    localObject = Integer.valueOf(i);
    paramContentValues.put("status", (Integer)localObject);
    localObject = Integer.valueOf(a);
    paramContentValues.put("width", (Integer)localObject);
    localObject = Integer.valueOf(k);
    paramContentValues.put("height", (Integer)localObject);
    localObject = Long.valueOf(d);
    paramContentValues.put("size", (Long)localObject);
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    super.writeToParcel(paramParcel, paramInt);
    paramInt = a;
    paramParcel.writeInt(paramInt);
    paramInt = k;
    paramParcel.writeInt(paramInt);
    byte[] arrayOfByte = l;
    paramParcel.writeByteArray(arrayOfByte);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ImageEntityWithThumbnail
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */