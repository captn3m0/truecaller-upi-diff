package com.truecaller.messaging.transport.im;

public enum ProcessResult
{
  static
  {
    ProcessResult[] arrayOfProcessResult = new ProcessResult[2];
    ProcessResult localProcessResult = new com/truecaller/messaging/transport/im/ProcessResult;
    localProcessResult.<init>("SUCCESS", 0);
    SUCCESS = localProcessResult;
    arrayOfProcessResult[0] = localProcessResult;
    localProcessResult = new com/truecaller/messaging/transport/im/ProcessResult;
    int i = 1;
    localProcessResult.<init>("FORCE_UPGRADE_ENCOUNTERED", i);
    FORCE_UPGRADE_ENCOUNTERED = localProcessResult;
    arrayOfProcessResult[i] = localProcessResult;
    $VALUES = arrayOfProcessResult;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ProcessResult
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */