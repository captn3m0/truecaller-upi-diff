package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.utils.extensions.m;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import okhttp3.ac;
import okhttp3.w;

final class bo$a
  extends ac
{
  private final ContentResolver a;
  private final String b;
  private final Uri c;
  
  public bo$a(ContentResolver paramContentResolver, String paramString, Uri paramUri)
  {
    a = paramContentResolver;
    b = paramString;
    c = paramUri;
  }
  
  public final w a()
  {
    return w.b(b);
  }
  
  public final void a(d.d paramd)
  {
    if (paramd != null)
    {
      InputStream localInputStream = null;
      try
      {
        Object localObject = a;
        Uri localUri = c;
        localInputStream = ((ContentResolver)localObject).openInputStream(localUri);
        localObject = "input";
        k.a(localInputStream, (String)localObject);
        paramd = paramd.c();
        localObject = "sink.outputStream()";
        k.a(paramd, (String)localObject);
        m.a(localInputStream, paramd);
        return;
      }
      finally
      {
        com.truecaller.utils.extensions.d.a((Closeable)localInputStream);
      }
    }
    paramd = new java/io/IOException;
    paramd.<init>();
    throw ((Throwable)paramd);
  }
  
  public final long b()
  {
    long l = -1;
    try
    {
      Object localObject = a;
      Uri localUri = c;
      localObject = ((ContentResolver)localObject).openInputStream(localUri);
      if (localObject != null)
      {
        int i = ((InputStream)localObject).available();
        l = i;
      }
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
    return l;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bo.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */