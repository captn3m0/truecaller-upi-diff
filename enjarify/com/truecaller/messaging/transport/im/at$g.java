package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.messaging.data.types.Reaction;

final class at$g
  extends u
{
  private final Reaction[] b;
  
  private at$g(e parame, Reaction[] paramArrayOfReaction)
  {
    super(parame);
    b = paramArrayOfReaction;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".saveReactions(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.at.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */