package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.w;
import java.util.Collection;
import java.util.List;

public abstract interface bp
{
  public abstract w a();
  
  public abstract w a(long paramLong);
  
  public abstract w a(String paramString);
  
  public abstract w a(Collection paramCollection, boolean paramBoolean);
  
  public abstract void a(String paramString1, String paramString2);
  
  public abstract void a(Collection paramCollection);
  
  public abstract void a(List paramList);
  
  public abstract w b(String paramString);
  
  public abstract w c(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bp
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */