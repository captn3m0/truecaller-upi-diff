package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.api.services.messenger.v1.models.input.InputReportType;

final class r$c
  extends u
{
  private final InputReportType b;
  private final String c;
  private final String d;
  private final String e;
  
  private r$c(e parame, InputReportType paramInputReportType, String paramString1, String paramString2, String paramString3)
  {
    super(parame);
    b = paramInputReportType;
    c = paramString1;
    d = paramString2;
    e = paramString3;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".sendReport(");
    Object localObject = b;
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(d, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(e, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.r.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */