package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import c.g.b.k;
import com.truecaller.analytics.b;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.k.a;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer.a;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer.b;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer.b.a;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer.d;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer.d.a;
import com.truecaller.api.services.messenger.v1.models.input.InputReactionContent;
import com.truecaller.api.services.messenger.v1.models.input.InputReactionContent.a;
import com.truecaller.api.services.messenger.v1.models.input.InputReactionContent.b;
import com.truecaller.api.services.messenger.v1.models.input.InputReactionContent.b.a;
import com.truecaller.api.services.messenger.v1.models.input.InputReportType;
import com.truecaller.api.services.messenger.v1.r.b;
import com.truecaller.api.services.messenger.v1.r.b.a;
import com.truecaller.api.services.messenger.v1.r.d;
import com.truecaller.api.services.messenger.v1.t.b;
import com.truecaller.api.services.messenger.v1.t.b.a;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Reaction;
import com.truecaller.messaging.transport.m;
import com.truecaller.network.d.j.a;
import dagger.a;
import io.grpc.ba;
import io.grpc.bc;
import java.io.File;
import java.util.concurrent.TimeUnit;
import okhttp3.y;

public final class s
  implements q
{
  private final Context a;
  private final a b;
  private final cb c;
  private final com.truecaller.messaging.data.providers.c d;
  private final y e;
  private final b f;
  private final com.truecaller.messaging.data.c g;
  private final ContentResolver h;
  
  public s(Context paramContext, a parama, cb paramcb, com.truecaller.messaging.data.providers.c paramc, y paramy, b paramb, com.truecaller.messaging.data.c paramc1, ContentResolver paramContentResolver)
  {
    a = paramContext;
    b = parama;
    c = paramcb;
    d = paramc;
    e = paramy;
    f = paramb;
    g = paramc1;
    h = paramContentResolver;
  }
  
  private static BinaryEntity a(d paramd, int paramInt)
  {
    long l1 = b;
    Uri localUri = d;
    long l2 = e;
    paramd = Entity.a(l1, "application/octet-stream", paramInt, localUri, l2);
    k.a(paramd, "Entity.create(item.entit…ntUri, -1, -1, item.size)");
    return paramd;
  }
  
  /* Error */
  private final BinaryEntity a(File paramFile, long paramLong, Uri paramUri)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore 5
    //   3: aconst_null
    //   4: astore 6
    //   6: aload_0
    //   7: getfield 61	com/truecaller/messaging/transport/im/s:g	Lcom/truecaller/messaging/data/c;
    //   10: astore 7
    //   12: aload_0
    //   13: getfield 63	com/truecaller/messaging/transport/im/s:h	Landroid/content/ContentResolver;
    //   16: astore 8
    //   18: invokestatic 89	com/truecaller/content/TruecallerContract$z:a	()Landroid/net/Uri;
    //   21: astore 9
    //   23: iconst_0
    //   24: istore 10
    //   26: aconst_null
    //   27: astore 11
    //   29: ldc 91
    //   31: astore 12
    //   33: lload_2
    //   34: invokestatic 97	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   37: astore 13
    //   39: aload 12
    //   41: aload 13
    //   43: invokevirtual 101	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   46: astore 12
    //   48: aconst_null
    //   49: astore 13
    //   51: aconst_null
    //   52: astore 14
    //   54: aload 8
    //   56: aload 9
    //   58: aconst_null
    //   59: aload 12
    //   61: aconst_null
    //   62: aconst_null
    //   63: invokevirtual 107	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   66: astore 8
    //   68: aload 7
    //   70: aload 8
    //   72: invokeinterface 112 2 0
    //   77: astore 8
    //   79: aload 8
    //   81: ifnull +278 -> 359
    //   84: aload 8
    //   86: astore 9
    //   88: aload 8
    //   90: checkcast 114	java/io/Closeable
    //   93: astore 9
    //   95: aload 9
    //   97: astore 7
    //   99: aload 9
    //   101: checkcast 116	com/truecaller/messaging/data/a/g
    //   104: astore 7
    //   106: aload 7
    //   108: invokeinterface 120 1 0
    //   113: istore 10
    //   115: iconst_1
    //   116: istore 15
    //   118: iload 10
    //   120: iload 15
    //   122: if_icmpne +174 -> 296
    //   125: aload 7
    //   127: invokeinterface 124 1 0
    //   132: astore 7
    //   134: aload 7
    //   136: instanceof 126
    //   139: istore 10
    //   141: iload 10
    //   143: ifne +6 -> 149
    //   146: aconst_null
    //   147: astore 7
    //   149: aload 7
    //   151: checkcast 126	com/truecaller/messaging/data/types/VideoEntity
    //   154: astore 7
    //   156: aload 7
    //   158: ifnull +132 -> 290
    //   161: aload 7
    //   163: getfield 129	com/truecaller/messaging/data/types/VideoEntity:m	Landroid/net/Uri;
    //   166: astore 11
    //   168: getstatic 134	android/net/Uri:EMPTY	Landroid/net/Uri;
    //   171: astore 13
    //   173: aload 11
    //   175: aload 13
    //   177: invokestatic 137	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/Object;)Z
    //   180: iload 15
    //   182: ixor
    //   183: istore 10
    //   185: iload 10
    //   187: ifeq +39 -> 226
    //   190: aload 5
    //   192: getfield 55	com/truecaller/messaging/transport/im/s:d	Lcom/truecaller/messaging/data/providers/c;
    //   195: astore 11
    //   197: aload 7
    //   199: getfield 129	com/truecaller/messaging/data/types/VideoEntity:m	Landroid/net/Uri;
    //   202: astore 12
    //   204: aload 11
    //   206: aload 12
    //   208: invokeinterface 142 2 0
    //   213: astore 11
    //   215: aload 11
    //   217: ifnull +9 -> 226
    //   220: aload 11
    //   222: invokevirtual 147	java/io/File:delete	()Z
    //   225: pop
    //   226: aload 7
    //   228: getfield 151	com/truecaller/messaging/data/types/VideoEntity:j	Ljava/lang/String;
    //   231: astore 14
    //   233: aload 7
    //   235: getfield 154	com/truecaller/messaging/data/types/VideoEntity:a	I
    //   238: istore 16
    //   240: aload 7
    //   242: getfield 157	com/truecaller/messaging/data/types/VideoEntity:k	I
    //   245: istore 17
    //   247: aload 7
    //   249: getfield 160	com/truecaller/messaging/data/types/VideoEntity:l	I
    //   252: istore 18
    //   254: aload_1
    //   255: invokestatic 165	com/truecaller/utils/extensions/l:a	(Ljava/io/File;)J
    //   258: lstore 19
    //   260: getstatic 134	android/net/Uri:EMPTY	Landroid/net/Uri;
    //   263: astore 21
    //   265: lload_2
    //   266: aload 14
    //   268: iconst_0
    //   269: aload 4
    //   271: iload 16
    //   273: iload 17
    //   275: iload 18
    //   277: iconst_0
    //   278: lload 19
    //   280: aload 21
    //   282: invokestatic 168	com/truecaller/messaging/data/types/Entity:a	(JLjava/lang/String;ILandroid/net/Uri;IIIZJLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   285: astore 7
    //   287: goto +17 -> 304
    //   290: aconst_null
    //   291: astore 7
    //   293: goto +11 -> 304
    //   296: iload 10
    //   298: ifne +15 -> 313
    //   301: aconst_null
    //   302: astore 7
    //   304: aload 9
    //   306: aconst_null
    //   307: invokestatic 173	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   310: goto +52 -> 362
    //   313: new 175	c/l
    //   316: astore 7
    //   318: aload 7
    //   320: invokespecial 176	c/l:<init>	()V
    //   323: aload 7
    //   325: athrow
    //   326: astore 7
    //   328: goto +12 -> 340
    //   331: astore 7
    //   333: aload 7
    //   335: astore 6
    //   337: aload 7
    //   339: athrow
    //   340: aload 9
    //   342: aload 6
    //   344: invokestatic 173	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   347: aload 7
    //   349: athrow
    //   350: astore 7
    //   352: aload 8
    //   354: astore 6
    //   356: goto +19 -> 375
    //   359: aconst_null
    //   360: astore 7
    //   362: aload 8
    //   364: checkcast 178	android/database/Cursor
    //   367: invokestatic 183	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   370: aload 7
    //   372: areturn
    //   373: astore 7
    //   375: aload 6
    //   377: checkcast 178	android/database/Cursor
    //   380: invokestatic 183	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   383: aload 7
    //   385: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	386	0	this	s
    //   0	386	1	paramFile	File
    //   0	386	2	paramLong	long
    //   0	386	4	paramUri	Uri
    //   1	190	5	locals	s
    //   4	372	6	localObject1	Object
    //   10	314	7	localObject2	Object
    //   326	1	7	localObject3	Object
    //   331	17	7	localObject4	Object
    //   350	1	7	localObject5	Object
    //   360	11	7	localBinaryEntity	BinaryEntity
    //   373	11	7	localObject6	Object
    //   16	347	8	localObject7	Object
    //   21	320	9	localObject8	Object
    //   24	273	10	bool1	boolean
    //   27	194	11	localObject9	Object
    //   31	176	12	localObject10	Object
    //   37	139	13	localObject11	Object
    //   52	215	14	str	String
    //   116	67	15	bool2	boolean
    //   238	34	16	i	int
    //   245	29	17	j	int
    //   252	24	18	k	int
    //   258	21	19	l	long
    //   263	18	21	localUri	Uri
    // Exception table:
    //   from	to	target	type
    //   337	340	326	finally
    //   99	104	331	finally
    //   106	113	331	finally
    //   125	132	331	finally
    //   149	154	331	finally
    //   161	166	331	finally
    //   168	171	331	finally
    //   175	180	331	finally
    //   190	195	331	finally
    //   197	202	331	finally
    //   206	213	331	finally
    //   220	226	331	finally
    //   226	231	331	finally
    //   233	238	331	finally
    //   240	245	331	finally
    //   247	252	331	finally
    //   254	258	331	finally
    //   260	263	331	finally
    //   280	285	331	finally
    //   313	316	331	finally
    //   318	323	331	finally
    //   323	326	331	finally
    //   88	93	350	finally
    //   306	310	350	finally
    //   342	347	350	finally
    //   347	350	350	finally
    //   6	10	373	finally
    //   12	16	373	finally
    //   18	21	373	finally
    //   33	37	373	finally
    //   41	46	373	finally
    //   62	66	373	finally
    //   70	77	373	finally
  }
  
  private static BinaryEntity a(String paramString, File paramFile, long paramLong, Uri paramUri)
  {
    long l = com.truecaller.utils.extensions.l.a(paramFile);
    paramString = Entity.a(paramLong, paramString, 0, paramUri, -1, -1, false, l);
    k.a(paramString, "Entity.create(id, type, …false, file.safeLength())");
    return paramString;
  }
  
  /* Error */
  private final Entity a(d paramd)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore_2
    //   2: aload_1
    //   3: astore_3
    //   4: iconst_2
    //   5: istore 4
    //   7: ldc -65
    //   9: astore 5
    //   11: aload_0
    //   12: getfield 49	com/truecaller/messaging/transport/im/s:a	Landroid/content/Context;
    //   15: astore 6
    //   17: aload 6
    //   19: invokevirtual 197	android/content/Context:getCacheDir	()Ljava/io/File;
    //   22: astore 6
    //   24: aconst_null
    //   25: astore 7
    //   27: aload 5
    //   29: aconst_null
    //   30: aload 6
    //   32: invokestatic 201	java/io/File:createTempFile	(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    //   35: astore 5
    //   37: ldc 70
    //   39: astore 6
    //   41: aload 6
    //   43: invokestatic 206	okhttp3/w:b	(Ljava/lang/String;)Lokhttp3/w;
    //   46: astore 6
    //   48: new 208	java/io/FileOutputStream
    //   51: astore 8
    //   53: aload 8
    //   55: aload 5
    //   57: invokespecial 211	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   60: aload 8
    //   62: checkcast 114	java/io/Closeable
    //   65: astore 8
    //   67: aload 8
    //   69: astore 9
    //   71: aload 8
    //   73: checkcast 208	java/io/FileOutputStream
    //   76: astore 9
    //   78: new 213	okhttp3/ab$a
    //   81: astore 10
    //   83: aload 10
    //   85: invokespecial 214	okhttp3/ab$a:<init>	()V
    //   88: aload_1
    //   89: getfield 217	com/truecaller/messaging/transport/im/d:c	Lokhttp3/u;
    //   92: astore 11
    //   94: aload 10
    //   96: aload 11
    //   98: invokevirtual 220	okhttp3/ab$a:a	(Lokhttp3/u;)Lokhttp3/ab$a;
    //   101: astore 10
    //   103: aload 10
    //   105: invokevirtual 223	okhttp3/ab$a:a	()Lokhttp3/ab;
    //   108: astore 10
    //   110: aload_0
    //   111: getfield 57	com/truecaller/messaging/transport/im/s:e	Lokhttp3/y;
    //   114: astore 11
    //   116: aload 11
    //   118: aload 10
    //   120: invokevirtual 228	okhttp3/y:a	(Lokhttp3/ab;)Lokhttp3/e;
    //   123: astore 10
    //   125: aload 10
    //   127: invokestatic 234	com/google/firebase/perf/network/FirebasePerfOkHttpClient:execute	(Lokhttp3/e;)Lokhttp3/ad;
    //   130: astore 10
    //   132: aload 10
    //   134: checkcast 114	java/io/Closeable
    //   137: astore 10
    //   139: aload 10
    //   141: astore 11
    //   143: aload 10
    //   145: checkcast 236	okhttp3/ad
    //   148: astore 11
    //   150: aload 11
    //   152: invokevirtual 239	okhttp3/ad:d	()Lokhttp3/ae;
    //   155: astore 12
    //   157: ldc -15
    //   159: astore 13
    //   161: aload 11
    //   163: aload 13
    //   165: invokestatic 84	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   168: aload 11
    //   170: invokevirtual 243	okhttp3/ad:c	()Z
    //   173: istore 14
    //   175: iload 14
    //   177: ifeq +704 -> 881
    //   180: aload 12
    //   182: ifnonnull +6 -> 188
    //   185: goto +696 -> 881
    //   188: ldc -11
    //   190: astore 13
    //   192: aload 11
    //   194: aload 13
    //   196: invokevirtual 247	okhttp3/ad:a	(Ljava/lang/String;)Ljava/lang/String;
    //   199: astore 11
    //   201: aload 11
    //   203: ifnonnull +7 -> 210
    //   206: ldc -7
    //   208: astore 11
    //   210: aload 11
    //   212: invokestatic 206	okhttp3/w:b	(Ljava/lang/String;)Lokhttp3/w;
    //   215: astore 11
    //   217: aload 11
    //   219: ifnonnull +6 -> 225
    //   222: goto +7 -> 229
    //   225: aload 11
    //   227: astore 6
    //   229: aload 12
    //   231: invokevirtual 254	okhttp3/ae:d	()Ljava/io/InputStream;
    //   234: astore 11
    //   236: ldc_w 256
    //   239: astore 12
    //   241: aload 11
    //   243: aload 12
    //   245: invokestatic 84	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   248: aload 9
    //   250: checkcast 258	java/io/OutputStream
    //   253: astore 9
    //   255: aload 11
    //   257: aload 9
    //   259: invokestatic 263	com/truecaller/utils/extensions/m:a	(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    //   262: pop2
    //   263: aload 10
    //   265: aconst_null
    //   266: invokestatic 173	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   269: aload 8
    //   271: aconst_null
    //   272: invokestatic 173	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   275: aload 6
    //   277: invokestatic 266	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   280: astore 6
    //   282: aload_2
    //   283: getfield 55	com/truecaller/messaging/transport/im/s:d	Lcom/truecaller/messaging/data/providers/c;
    //   286: astore 7
    //   288: aload_3
    //   289: getfield 68	com/truecaller/messaging/transport/im/d:b	J
    //   292: lstore 15
    //   294: aload 7
    //   296: lload 15
    //   298: aload 6
    //   300: invokeinterface 269 4 0
    //   305: astore 7
    //   307: aload 7
    //   309: ifnonnull +17 -> 326
    //   312: aload_3
    //   313: iload 4
    //   315: invokestatic 272	com/truecaller/messaging/transport/im/s:a	(Lcom/truecaller/messaging/transport/im/d;I)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   318: astore 5
    //   320: aload 5
    //   322: checkcast 77	com/truecaller/messaging/data/types/Entity
    //   325: areturn
    //   326: ldc_w 274
    //   329: astore 8
    //   331: aload 5
    //   333: aload 8
    //   335: invokestatic 84	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   338: aload 5
    //   340: aload 7
    //   342: invokestatic 279	c/f/e:a	(Ljava/io/File;Ljava/io/File;)Ljava/io/File;
    //   345: pop
    //   346: aload 5
    //   348: invokevirtual 147	java/io/File:delete	()Z
    //   351: pop
    //   352: aload_2
    //   353: getfield 55	com/truecaller/messaging/transport/im/s:d	Lcom/truecaller/messaging/data/providers/c;
    //   356: astore 8
    //   358: aload 7
    //   360: invokevirtual 283	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   363: astore 9
    //   365: aload 9
    //   367: invokestatic 288	c/a/m:a	(Ljava/lang/Object;)Ljava/util/List;
    //   370: astore 9
    //   372: aload 8
    //   374: aload 9
    //   376: invokeinterface 291 2 0
    //   381: aload_3
    //   382: getfield 73	com/truecaller/messaging/transport/im/d:d	Landroid/net/Uri;
    //   385: astore 8
    //   387: getstatic 134	android/net/Uri:EMPTY	Landroid/net/Uri;
    //   390: astore 9
    //   392: aload 8
    //   394: aload 9
    //   396: invokestatic 137	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/Object;)Z
    //   399: istore 17
    //   401: iconst_1
    //   402: istore 18
    //   404: iload 17
    //   406: iload 18
    //   408: ixor
    //   409: istore 17
    //   411: iload 17
    //   413: ifeq +37 -> 450
    //   416: aload_2
    //   417: getfield 55	com/truecaller/messaging/transport/im/s:d	Lcom/truecaller/messaging/data/providers/c;
    //   420: astore 8
    //   422: aload_3
    //   423: getfield 73	com/truecaller/messaging/transport/im/d:d	Landroid/net/Uri;
    //   426: astore 10
    //   428: aload 8
    //   430: aload 10
    //   432: invokeinterface 142 2 0
    //   437: astore 8
    //   439: aload 8
    //   441: ifnull +9 -> 450
    //   444: aload 8
    //   446: invokevirtual 147	java/io/File:delete	()Z
    //   449: pop
    //   450: aload_2
    //   451: getfield 55	com/truecaller/messaging/transport/im/s:d	Lcom/truecaller/messaging/data/providers/c;
    //   454: astore 8
    //   456: aload 8
    //   458: aload 7
    //   460: aload 6
    //   462: iload 18
    //   464: invokeinterface 294 4 0
    //   469: astore 19
    //   471: aload 19
    //   473: ifnonnull +17 -> 490
    //   476: aload_3
    //   477: iload 4
    //   479: invokestatic 272	com/truecaller/messaging/transport/im/s:a	(Lcom/truecaller/messaging/transport/im/d;I)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   482: astore 5
    //   484: aload 5
    //   486: checkcast 77	com/truecaller/messaging/data/types/Entity
    //   489: areturn
    //   490: aload 6
    //   492: astore 8
    //   494: aload 6
    //   496: checkcast 296	java/lang/CharSequence
    //   499: astore 8
    //   501: aload 8
    //   503: invokeinterface 300 1 0
    //   508: istore 17
    //   510: iload 17
    //   512: ifne +9 -> 521
    //   515: iconst_1
    //   516: istore 17
    //   518: goto +9 -> 527
    //   521: iconst_0
    //   522: istore 17
    //   524: aconst_null
    //   525: astore 8
    //   527: iload 17
    //   529: ifeq +32 -> 561
    //   532: ldc 70
    //   534: astore 5
    //   536: aload_3
    //   537: getfield 68	com/truecaller/messaging/transport/im/d:b	J
    //   540: lstore 15
    //   542: aload 5
    //   544: aload 7
    //   546: lload 15
    //   548: aload 19
    //   550: invokestatic 303	com/truecaller/messaging/transport/im/s:a	(Ljava/lang/String;Ljava/io/File;JLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   553: astore 5
    //   555: aload 5
    //   557: checkcast 77	com/truecaller/messaging/data/types/Entity
    //   560: areturn
    //   561: aload 6
    //   563: invokestatic 306	com/truecaller/messaging/data/types/Entity:c	(Ljava/lang/String;)Z
    //   566: istore 17
    //   568: iload 17
    //   570: ifeq +112 -> 682
    //   573: aload_3
    //   574: getfield 68	com/truecaller/messaging/transport/im/d:b	J
    //   577: lstore 20
    //   579: new 308	android/graphics/BitmapFactory$Options
    //   582: astore 5
    //   584: aload 5
    //   586: invokespecial 309	android/graphics/BitmapFactory$Options:<init>	()V
    //   589: aload 5
    //   591: iload 18
    //   593: putfield 313	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   596: aload 7
    //   598: invokevirtual 283	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   601: astore 6
    //   603: aload 6
    //   605: aload 5
    //   607: invokestatic 319	android/graphics/BitmapFactory:decodeFile	(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   610: pop
    //   611: aload 5
    //   613: getfield 322	android/graphics/BitmapFactory$Options:outMimeType	Ljava/lang/String;
    //   616: astore 12
    //   618: iconst_0
    //   619: istore 14
    //   621: aconst_null
    //   622: astore 13
    //   624: aload 5
    //   626: getfield 325	android/graphics/BitmapFactory$Options:outWidth	I
    //   629: istore 22
    //   631: aload 5
    //   633: getfield 328	android/graphics/BitmapFactory$Options:outHeight	I
    //   636: istore 23
    //   638: aload 7
    //   640: invokestatic 165	com/truecaller/utils/extensions/l:a	(Ljava/io/File;)J
    //   643: lstore 24
    //   645: lload 20
    //   647: aload 12
    //   649: iconst_0
    //   650: aload 19
    //   652: iload 22
    //   654: iload 23
    //   656: iconst_0
    //   657: lload 24
    //   659: invokestatic 186	com/truecaller/messaging/data/types/Entity:a	(JLjava/lang/String;ILandroid/net/Uri;IIZJ)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   662: astore 5
    //   664: ldc_w 330
    //   667: astore 6
    //   669: aload 5
    //   671: aload 6
    //   673: invokestatic 84	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   676: aload 5
    //   678: checkcast 77	com/truecaller/messaging/data/types/Entity
    //   681: areturn
    //   682: aload 6
    //   684: invokestatic 332	com/truecaller/messaging/data/types/Entity:d	(Ljava/lang/String;)Z
    //   687: istore 17
    //   689: iload 17
    //   691: ifeq +57 -> 748
    //   694: aload_3
    //   695: getfield 68	com/truecaller/messaging/transport/im/d:b	J
    //   698: lstore 15
    //   700: aload_2
    //   701: aload 7
    //   703: lload 15
    //   705: aload 19
    //   707: invokespecial 335	com/truecaller/messaging/transport/im/s:a	(Ljava/io/File;JLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   710: astore 7
    //   712: aload 7
    //   714: ifnull +9 -> 723
    //   717: aload 7
    //   719: checkcast 77	com/truecaller/messaging/data/types/Entity
    //   722: areturn
    //   723: aload_3
    //   724: getfield 68	com/truecaller/messaging/transport/im/d:b	J
    //   727: lstore 26
    //   729: aload 6
    //   731: aload 5
    //   733: lload 26
    //   735: aload 19
    //   737: invokestatic 303	com/truecaller/messaging/transport/im/s:a	(Ljava/lang/String;Ljava/io/File;JLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   740: astore 5
    //   742: aload 5
    //   744: checkcast 77	com/truecaller/messaging/data/types/Entity
    //   747: areturn
    //   748: aload 6
    //   750: invokestatic 337	com/truecaller/messaging/data/types/Entity:e	(Ljava/lang/String;)Z
    //   753: istore 17
    //   755: iload 17
    //   757: ifeq +57 -> 814
    //   760: aload_3
    //   761: getfield 68	com/truecaller/messaging/transport/im/d:b	J
    //   764: lstore 15
    //   766: aload_2
    //   767: aload 7
    //   769: lload 15
    //   771: aload 19
    //   773: invokespecial 339	com/truecaller/messaging/transport/im/s:b	(Ljava/io/File;JLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   776: astore 7
    //   778: aload 7
    //   780: ifnull +9 -> 789
    //   783: aload 7
    //   785: checkcast 77	com/truecaller/messaging/data/types/Entity
    //   788: areturn
    //   789: aload_3
    //   790: getfield 68	com/truecaller/messaging/transport/im/d:b	J
    //   793: lstore 26
    //   795: aload 6
    //   797: aload 5
    //   799: lload 26
    //   801: aload 19
    //   803: invokestatic 303	com/truecaller/messaging/transport/im/s:a	(Ljava/lang/String;Ljava/io/File;JLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   806: astore 5
    //   808: aload 5
    //   810: checkcast 77	com/truecaller/messaging/data/types/Entity
    //   813: areturn
    //   814: aload 6
    //   816: invokestatic 341	com/truecaller/messaging/data/types/Entity:f	(Ljava/lang/String;)Z
    //   819: istore 28
    //   821: iload 28
    //   823: ifeq +33 -> 856
    //   826: ldc_w 343
    //   829: astore 5
    //   831: aload_3
    //   832: getfield 68	com/truecaller/messaging/transport/im/d:b	J
    //   835: lstore 15
    //   837: aload 5
    //   839: aload 7
    //   841: lload 15
    //   843: aload 19
    //   845: invokestatic 303	com/truecaller/messaging/transport/im/s:a	(Ljava/lang/String;Ljava/io/File;JLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   848: astore 5
    //   850: aload 5
    //   852: checkcast 77	com/truecaller/messaging/data/types/Entity
    //   855: areturn
    //   856: aload_3
    //   857: getfield 68	com/truecaller/messaging/transport/im/d:b	J
    //   860: lstore 15
    //   862: aload 6
    //   864: aload 7
    //   866: lload 15
    //   868: aload 19
    //   870: invokestatic 303	com/truecaller/messaging/transport/im/s:a	(Ljava/lang/String;Ljava/io/File;JLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   873: astore 5
    //   875: aload 5
    //   877: checkcast 77	com/truecaller/messaging/data/types/Entity
    //   880: areturn
    //   881: aload 11
    //   883: invokevirtual 345	okhttp3/ad:b	()I
    //   886: istore 28
    //   888: sipush 404
    //   891: istore 29
    //   893: iload 28
    //   895: iload 29
    //   897: if_icmpeq +21 -> 918
    //   900: aload_3
    //   901: iload 4
    //   903: invokestatic 272	com/truecaller/messaging/transport/im/s:a	(Lcom/truecaller/messaging/transport/im/d;I)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   906: astore 5
    //   908: aload 5
    //   910: checkcast 77	com/truecaller/messaging/data/types/Entity
    //   913: astore 5
    //   915: goto +21 -> 936
    //   918: iconst_3
    //   919: istore 28
    //   921: aload_3
    //   922: iload 28
    //   924: invokestatic 272	com/truecaller/messaging/transport/im/s:a	(Lcom/truecaller/messaging/transport/im/d;I)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   927: astore 5
    //   929: aload 5
    //   931: checkcast 77	com/truecaller/messaging/data/types/Entity
    //   934: astore 5
    //   936: aload 10
    //   938: aconst_null
    //   939: invokestatic 173	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   942: aload 8
    //   944: aconst_null
    //   945: invokestatic 173	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   948: aload 5
    //   950: areturn
    //   951: astore 5
    //   953: aload 5
    //   955: astore 6
    //   957: aload 5
    //   959: athrow
    //   960: astore 5
    //   962: aload 10
    //   964: aload 6
    //   966: invokestatic 173	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   969: aload 5
    //   971: athrow
    //   972: astore 5
    //   974: goto +12 -> 986
    //   977: astore 5
    //   979: aload 5
    //   981: astore 7
    //   983: aload 5
    //   985: athrow
    //   986: aload 8
    //   988: aload 7
    //   990: invokestatic 173	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   993: aload 5
    //   995: athrow
    //   996: checkcast 349	java/lang/Throwable
    //   999: invokestatic 355	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   1002: aload_3
    //   1003: iload 4
    //   1005: invokestatic 272	com/truecaller/messaging/transport/im/s:a	(Lcom/truecaller/messaging/transport/im/d;I)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   1008: checkcast 77	com/truecaller/messaging/data/types/Entity
    //   1011: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1012	0	this	s
    //   0	1012	1	paramd	d
    //   1	766	2	locals	s
    //   3	1000	3	locald	d
    //   5	999	4	i	int
    //   9	940	5	localObject1	Object
    //   951	7	5	localObject2	Object
    //   960	10	5	localObject3	Object
    //   972	1	5	localObject4	Object
    //   977	17	5	localObject5	Object
    //   15	950	6	localObject6	Object
    //   25	964	7	localObject7	Object
    //   51	936	8	localObject8	Object
    //   69	326	9	localObject9	Object
    //   81	882	10	localObject10	Object
    //   92	790	11	localObject11	Object
    //   155	493	12	localObject12	Object
    //   159	464	13	str	String
    //   173	447	14	bool1	boolean
    //   292	575	15	l1	long
    //   399	13	17	bool2	boolean
    //   508	20	17	j	int
    //   566	190	17	bool3	boolean
    //   402	190	18	bool4	boolean
    //   469	400	19	localUri	Uri
    //   577	69	20	l2	long
    //   629	24	22	k	int
    //   636	19	23	m	int
    //   643	15	24	l3	long
    //   727	73	26	l4	long
    //   819	3	28	bool5	boolean
    //   886	37	28	n	int
    //   891	7	29	i1	int
    //   996	1	33	localIOException	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   143	148	951	finally
    //   150	155	951	finally
    //   163	168	951	finally
    //   168	173	951	finally
    //   194	199	951	finally
    //   210	215	951	finally
    //   229	234	951	finally
    //   243	248	951	finally
    //   248	253	951	finally
    //   257	263	951	finally
    //   881	886	951	finally
    //   901	906	951	finally
    //   908	913	951	finally
    //   922	927	951	finally
    //   929	934	951	finally
    //   957	960	960	finally
    //   983	986	972	finally
    //   71	76	977	finally
    //   78	81	977	finally
    //   83	88	977	finally
    //   88	92	977	finally
    //   96	101	977	finally
    //   103	108	977	finally
    //   110	114	977	finally
    //   118	123	977	finally
    //   125	130	977	finally
    //   132	137	977	finally
    //   265	269	977	finally
    //   938	942	977	finally
    //   964	969	977	finally
    //   969	972	977	finally
    //   11	15	996	java/io/IOException
    //   17	22	996	java/io/IOException
    //   30	35	996	java/io/IOException
    //   41	46	996	java/io/IOException
    //   48	51	996	java/io/IOException
    //   55	60	996	java/io/IOException
    //   60	65	996	java/io/IOException
    //   271	275	996	java/io/IOException
    //   275	280	996	java/io/IOException
    //   282	286	996	java/io/IOException
    //   288	292	996	java/io/IOException
    //   298	305	996	java/io/IOException
    //   313	318	996	java/io/IOException
    //   320	325	996	java/io/IOException
    //   333	338	996	java/io/IOException
    //   340	346	996	java/io/IOException
    //   346	352	996	java/io/IOException
    //   352	356	996	java/io/IOException
    //   358	363	996	java/io/IOException
    //   365	370	996	java/io/IOException
    //   374	381	996	java/io/IOException
    //   381	385	996	java/io/IOException
    //   387	390	996	java/io/IOException
    //   394	399	996	java/io/IOException
    //   416	420	996	java/io/IOException
    //   422	426	996	java/io/IOException
    //   430	437	996	java/io/IOException
    //   444	450	996	java/io/IOException
    //   450	454	996	java/io/IOException
    //   462	469	996	java/io/IOException
    //   477	482	996	java/io/IOException
    //   484	489	996	java/io/IOException
    //   494	499	996	java/io/IOException
    //   501	508	996	java/io/IOException
    //   536	540	996	java/io/IOException
    //   548	553	996	java/io/IOException
    //   555	560	996	java/io/IOException
    //   561	566	996	java/io/IOException
    //   573	577	996	java/io/IOException
    //   579	582	996	java/io/IOException
    //   584	589	996	java/io/IOException
    //   591	596	996	java/io/IOException
    //   596	601	996	java/io/IOException
    //   605	611	996	java/io/IOException
    //   611	616	996	java/io/IOException
    //   624	629	996	java/io/IOException
    //   631	636	996	java/io/IOException
    //   638	643	996	java/io/IOException
    //   657	662	996	java/io/IOException
    //   671	676	996	java/io/IOException
    //   676	681	996	java/io/IOException
    //   682	687	996	java/io/IOException
    //   694	698	996	java/io/IOException
    //   705	710	996	java/io/IOException
    //   717	722	996	java/io/IOException
    //   723	727	996	java/io/IOException
    //   735	740	996	java/io/IOException
    //   742	747	996	java/io/IOException
    //   748	753	996	java/io/IOException
    //   760	764	996	java/io/IOException
    //   771	776	996	java/io/IOException
    //   783	788	996	java/io/IOException
    //   789	793	996	java/io/IOException
    //   801	806	996	java/io/IOException
    //   808	813	996	java/io/IOException
    //   814	819	996	java/io/IOException
    //   831	835	996	java/io/IOException
    //   843	848	996	java/io/IOException
    //   850	855	996	java/io/IOException
    //   856	860	996	java/io/IOException
    //   868	873	996	java/io/IOException
    //   875	880	996	java/io/IOException
    //   944	948	996	java/io/IOException
    //   988	993	996	java/io/IOException
    //   993	996	996	java/io/IOException
  }
  
  private final SendResult a(InputReportType paramInputReportType, String paramString, InputPeer paramInputPeer)
  {
    k.a locala = (k.a)j.a.a(c);
    if (locala == null) {
      return SendResult.FAILURE_PERMANENT;
    }
    try
    {
      try
      {
        t.b.a locala1 = t.b.a();
        paramString = locala1.a(paramString);
        paramString = paramString.a(paramInputPeer);
        paramInputReportType = paramString.a(paramInputReportType);
        paramInputReportType = paramInputReportType.build();
        paramInputReportType = (t.b)paramInputReportType;
        locala.a(paramInputReportType);
        paramInputReportType = SendResult.SUCCESS;
      }
      catch (RuntimeException localRuntimeException)
      {
        paramInputReportType = SendResult.FAILURE_PERMANENT;
      }
      return paramInputReportType;
    }
    catch (bc localbc)
    {
      paramInputReportType = localbc.a();
      paramString = "e.status";
      k.a(paramInputReportType, paramString);
      boolean bool = com.truecaller.messaging.i.l.a(paramInputReportType);
      if (bool) {
        return SendResult.FAILURE_TRANSIENT;
      }
    }
    return SendResult.FAILURE_PERMANENT;
  }
  
  private final void a(String paramString, Reaction paramReaction)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("update_reaction");
    paramReaction = (Parcelable)paramReaction;
    paramString = localIntent.putExtra("reaction", paramReaction).putExtra("rawId", paramString);
    ((m)b.get()).a(2, paramString, 0);
  }
  
  /* Error */
  private final BinaryEntity b(File paramFile, long paramLong, Uri paramUri)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 5
    //   3: aload_0
    //   4: getfield 61	com/truecaller/messaging/transport/im/s:g	Lcom/truecaller/messaging/data/c;
    //   7: astore 6
    //   9: aload_0
    //   10: getfield 63	com/truecaller/messaging/transport/im/s:h	Landroid/content/ContentResolver;
    //   13: astore 7
    //   15: invokestatic 89	com/truecaller/content/TruecallerContract$z:a	()Landroid/net/Uri;
    //   18: astore 8
    //   20: iconst_0
    //   21: istore 9
    //   23: ldc 91
    //   25: astore 10
    //   27: lload_2
    //   28: invokestatic 97	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   31: astore 11
    //   33: aload 10
    //   35: aload 11
    //   37: invokevirtual 101	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   40: astore 10
    //   42: aconst_null
    //   43: astore 11
    //   45: aload 7
    //   47: aload 8
    //   49: aconst_null
    //   50: aload 10
    //   52: aconst_null
    //   53: aconst_null
    //   54: invokevirtual 107	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   57: astore 7
    //   59: aload 6
    //   61: aload 7
    //   63: invokeinterface 112 2 0
    //   68: astore 7
    //   70: aload 7
    //   72: ifnull +205 -> 277
    //   75: aload 7
    //   77: astore 8
    //   79: aload 7
    //   81: checkcast 114	java/io/Closeable
    //   84: astore 8
    //   86: aload 8
    //   88: astore 6
    //   90: aload 8
    //   92: checkcast 116	com/truecaller/messaging/data/a/g
    //   95: astore 6
    //   97: aload 6
    //   99: invokeinterface 120 1 0
    //   104: istore 9
    //   106: iconst_1
    //   107: istore 12
    //   109: iload 9
    //   111: iload 12
    //   113: if_icmpne +101 -> 214
    //   116: aload 6
    //   118: invokeinterface 124 1 0
    //   123: astore 6
    //   125: aload 6
    //   127: instanceof 446
    //   130: istore 9
    //   132: iload 9
    //   134: ifne +6 -> 140
    //   137: aconst_null
    //   138: astore 6
    //   140: aload 6
    //   142: checkcast 446	com/truecaller/messaging/data/types/AudioEntity
    //   145: astore 6
    //   147: aload 6
    //   149: ifnull +59 -> 208
    //   152: aload 6
    //   154: getfield 447	com/truecaller/messaging/data/types/AudioEntity:j	Ljava/lang/String;
    //   157: astore 11
    //   159: iconst_m1
    //   160: istore 13
    //   162: iconst_m1
    //   163: istore 14
    //   165: aload 6
    //   167: getfield 448	com/truecaller/messaging/data/types/AudioEntity:a	I
    //   170: istore 15
    //   172: aload_1
    //   173: invokestatic 165	com/truecaller/utils/extensions/l:a	(Ljava/io/File;)J
    //   176: lstore 16
    //   178: getstatic 134	android/net/Uri:EMPTY	Landroid/net/Uri;
    //   181: astore 18
    //   183: lload_2
    //   184: aload 11
    //   186: iconst_0
    //   187: aload 4
    //   189: iload 13
    //   191: iload 14
    //   193: iload 15
    //   195: iconst_0
    //   196: lload 16
    //   198: aload 18
    //   200: invokestatic 168	com/truecaller/messaging/data/types/Entity:a	(JLjava/lang/String;ILandroid/net/Uri;IIIZJLandroid/net/Uri;)Lcom/truecaller/messaging/data/types/BinaryEntity;
    //   203: astore 6
    //   205: goto +17 -> 222
    //   208: aconst_null
    //   209: astore 6
    //   211: goto +11 -> 222
    //   214: iload 9
    //   216: ifne +15 -> 231
    //   219: aconst_null
    //   220: astore 6
    //   222: aload 8
    //   224: aconst_null
    //   225: invokestatic 173	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   228: goto +52 -> 280
    //   231: new 175	c/l
    //   234: astore 6
    //   236: aload 6
    //   238: invokespecial 176	c/l:<init>	()V
    //   241: aload 6
    //   243: athrow
    //   244: astore 6
    //   246: goto +12 -> 258
    //   249: astore 6
    //   251: aload 6
    //   253: astore 5
    //   255: aload 6
    //   257: athrow
    //   258: aload 8
    //   260: aload 5
    //   262: invokestatic 173	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   265: aload 6
    //   267: athrow
    //   268: astore 6
    //   270: aload 7
    //   272: astore 5
    //   274: goto +19 -> 293
    //   277: aconst_null
    //   278: astore 6
    //   280: aload 7
    //   282: checkcast 178	android/database/Cursor
    //   285: invokestatic 183	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   288: aload 6
    //   290: areturn
    //   291: astore 6
    //   293: aload 5
    //   295: checkcast 178	android/database/Cursor
    //   298: invokestatic 183	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   301: aload 6
    //   303: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	304	0	this	s
    //   0	304	1	paramFile	File
    //   0	304	2	paramLong	long
    //   0	304	4	paramUri	Uri
    //   1	293	5	localObject1	Object
    //   7	235	6	localObject2	Object
    //   244	1	6	localObject3	Object
    //   249	17	6	localObject4	Object
    //   268	1	6	localObject5	Object
    //   278	11	6	localBinaryEntity	BinaryEntity
    //   291	11	6	localObject6	Object
    //   13	268	7	localObject7	Object
    //   18	241	8	localObject8	Object
    //   21	194	9	bool1	boolean
    //   25	26	10	str1	String
    //   31	154	11	str2	String
    //   107	7	12	bool2	boolean
    //   160	30	13	i	int
    //   163	29	14	j	int
    //   170	24	15	k	int
    //   176	21	16	l	long
    //   181	18	18	localUri	Uri
    // Exception table:
    //   from	to	target	type
    //   255	258	244	finally
    //   90	95	249	finally
    //   97	104	249	finally
    //   116	123	249	finally
    //   140	145	249	finally
    //   152	157	249	finally
    //   165	170	249	finally
    //   172	176	249	finally
    //   178	181	249	finally
    //   198	203	249	finally
    //   231	234	249	finally
    //   236	241	249	finally
    //   241	244	249	finally
    //   79	84	268	finally
    //   224	228	268	finally
    //   260	265	268	finally
    //   265	268	268	finally
    //   3	7	291	finally
    //   9	13	291	finally
    //   15	18	291	finally
    //   27	31	291	finally
    //   35	40	291	finally
    //   53	57	291	finally
    //   61	68	291	finally
  }
  
  public final w a(InputReportType paramInputReportType, String paramString1, String paramString2, String paramString3)
  {
    k.b(paramInputReportType, "type");
    k.b(paramString1, "rawId");
    k.b(paramString2, "imPeerId");
    Object localObject1 = InputPeer.a();
    Object localObject2 = paramString3;
    localObject2 = (CharSequence)paramString3;
    int i = 1;
    if (localObject2 != null)
    {
      j = ((CharSequence)localObject2).length();
      if (j != 0)
      {
        j = 0;
        localObject2 = null;
        break label74;
      }
    }
    int j = 1;
    label74:
    if (j == i)
    {
      paramString3 = InputPeer.d.a();
      paramString2 = paramString3.a(paramString2);
      ((InputPeer.a)localObject1).a(paramString2);
    }
    else
    {
      paramString2 = InputPeer.b.a().a(paramString3);
      ((InputPeer.a)localObject1).a(paramString2);
    }
    paramString2 = ((InputPeer.a)localObject1).build();
    k.a(paramString2, "InputPeer.newBuilder().a…      }\n        }.build()");
    paramString2 = (InputPeer)paramString2;
    paramString2 = a(paramInputReportType, paramString1, paramString2);
    paramString3 = SendResult.SUCCESS;
    int k;
    if (paramString2 == paramString3) {
      k = 4;
    } else {
      k = 3;
    }
    localObject1 = new com/truecaller/messaging/transport/im/ImTransportInfo$a;
    ((ImTransportInfo.a)localObject1).<init>();
    paramString1 = ((ImTransportInfo.a)localObject1).a(paramString1);
    localObject1 = t.a;
    j = paramInputReportType.ordinal();
    int m = localObject1[j];
    switch (m)
    {
    default: 
      paramString1 = new java/lang/IllegalArgumentException;
      paramInputReportType = String.valueOf(paramInputReportType);
      paramInputReportType = "Unknown report type ".concat(paramInputReportType);
      paramString1.<init>(paramInputReportType);
      throw ((Throwable)paramString1);
    case 2: 
      f = k;
      paramInputReportType = new android/content/Intent;
      paramString3 = "update_read_sync_status";
      paramInputReportType.<init>(paramString3);
      break;
    case 1: 
      e = k;
      paramInputReportType = new android/content/Intent;
      paramString3 = "update_read_sync_status";
      paramInputReportType.<init>(paramString3);
    }
    paramString1 = (Parcelable)paramString1.a();
    paramInputReportType.putExtra("transport_info", paramString1);
    ((m)b.get()).a(2, paramInputReportType, 0);
    paramInputReportType = w.b(paramString2);
    k.a(paramInputReportType, "Promise.wrap(result)");
    return paramInputReportType;
  }
  
  public final w a(String paramString1, long paramLong, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    s locals = this;
    String str = paramString1;
    Object localObject1 = paramString4;
    Object localObject3 = paramString5;
    k.b(paramString1, "rawId");
    k.b(paramString2, "fromPeerId");
    Object localObject4 = (k.a)j.a.a(c);
    if (localObject4 == null)
    {
      localObject1 = w.b(SendResult.FAILURE_PERMANENT);
      k.a(localObject1, "Promise.wrap(FAILURE_PERMANENT)");
      return (w)localObject1;
    }
    label123:
    Object localObject2;
    try
    {
      Object localObject5 = InputReactionContent.a();
      localObject5 = ((InputReactionContent.a)localObject5).a(paramString1);
      Object localObject6 = paramString5;
      localObject6 = (CharSequence)paramString5;
      if (localObject6 != null)
      {
        i = ((CharSequence)localObject6).length();
        if (i != 0)
        {
          i = 0;
          localObject6 = null;
          break label123;
        }
      }
      int i = 1;
      if (i == 0)
      {
        localObject6 = InputReactionContent.b.a();
        localObject6 = ((InputReactionContent.b.a)localObject6).a((String)localObject3);
        ((InputReactionContent.a)localObject5).a((InputReactionContent.b.a)localObject6);
      }
      Object localObject7;
      if (localObject1 != null)
      {
        localObject6 = InputPeer.a();
        localObject7 = InputPeer.b.a();
        localObject1 = ((InputPeer.b.a)localObject7).a((String)localObject1);
        localObject1 = ((InputPeer.a)localObject6).a((InputPeer.b.a)localObject1);
      }
      else
      {
        localObject1 = InputPeer.a();
        localObject6 = InputPeer.d.a();
        localObject7 = paramString3;
        localObject6 = ((InputPeer.d.a)localObject6).a(paramString3);
        localObject1 = ((InputPeer.a)localObject1).a((InputPeer.d.a)localObject6);
      }
      localObject6 = r.b.a();
      localObject1 = ((r.b.a)localObject6).a((InputPeer.a)localObject1);
      localObject1 = ((r.b.a)localObject1).a((InputReactionContent.a)localObject5);
      localObject1 = ((r.b.a)localObject1).build();
      localObject1 = (r.b)localObject1;
      localObject1 = ((k.a)localObject4).a((r.b)localObject1);
      localObject4 = TimeUnit.SECONDS;
      localObject5 = "response";
      k.a(localObject1, (String)localObject5);
      int j = ((r.d)localObject1).a();
      l1 = j;
      l2 = ((TimeUnit)localObject4).toMillis(l1);
      localObject1 = new com/truecaller/messaging/data/types/Reaction;
      l1 = 0L;
      k = 0;
      m = 1;
      localObject4 = localObject1;
      ((Reaction)localObject1).<init>(l1, paramLong, paramString2, paramString5, l2, 0, m);
      locals.a(str, (Reaction)localObject1);
      localObject1 = SendResult.SUCCESS;
      localObject1 = w.b(localObject1);
      localObject4 = "Promise.wrap(SUCCESS)";
      k.a(localObject1, (String)localObject4);
    }
    catch (RuntimeException localRuntimeException)
    {
      localObject1 = new com/truecaller/messaging/data/types/Reaction;
      l1 = 0L;
      l2 = System.currentTimeMillis();
      k = 2;
      m = 1;
      localObject4 = localObject1;
      ((Reaction)localObject1).<init>(l1, paramLong, paramString2, paramString5, l2, k, m);
      locals.a(str, (Reaction)localObject1);
      localObject1 = w.b(SendResult.FAILURE_PERMANENT);
      str = "Promise.wrap(FAILURE_PERMANENT)";
      k.a(localObject1, str);
    }
    catch (bc localbc)
    {
      Reaction localReaction = new com/truecaller/messaging/data/types/Reaction;
      long l1 = 0L;
      long l2 = System.currentTimeMillis();
      int k = 2;
      int n = 1;
      localObject4 = localReaction;
      localObject3 = localReaction;
      int m = n;
      localReaction.<init>(l1, paramLong, paramString2, paramString5, l2, k, n);
      locals.a(str, localReaction);
      localObject2 = localbc.a();
      str = "e.status";
      k.a(localObject2, str);
      boolean bool = com.truecaller.messaging.i.l.a((ba)localObject2);
      if (bool)
      {
        localObject2 = w.b(SendResult.FAILURE_TRANSIENT);
        str = "Promise.wrap(FAILURE_TRANSIENT)";
      }
      else
      {
        localObject2 = w.b(SendResult.FAILURE_PERMANENT);
        str = "Promise.wrap(FAILURE_PERMANENT)";
      }
      k.a(localObject2, str);
    }
    return (w)localObject2;
  }
  
  /* Error */
  public final void a()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 63	com/truecaller/messaging/transport/im/s:h	Landroid/content/ContentResolver;
    //   4: astore_1
    //   5: invokestatic 610	com/truecaller/content/TruecallerContract$q:a	()Landroid/net/Uri;
    //   8: astore_2
    //   9: ldc_w 620
    //   12: astore_3
    //   13: iconst_5
    //   14: anewarray 93	java/lang/String
    //   17: dup
    //   18: dup2
    //   19: iconst_0
    //   20: ldc_w 612
    //   23: aastore
    //   24: iconst_1
    //   25: ldc_w 614
    //   28: aastore
    //   29: dup2
    //   30: iconst_2
    //   31: ldc_w 616
    //   34: aastore
    //   35: iconst_3
    //   36: ldc_w 618
    //   39: aastore
    //   40: iconst_4
    //   41: aload_3
    //   42: aastore
    //   43: astore 4
    //   45: ldc_w 622
    //   48: astore 5
    //   50: ldc_w 624
    //   53: astore 6
    //   55: iconst_0
    //   56: istore 7
    //   58: aconst_null
    //   59: astore 8
    //   61: aload_1
    //   62: aload_2
    //   63: aload 4
    //   65: aload 5
    //   67: aconst_null
    //   68: aload 6
    //   70: invokevirtual 107	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   73: astore_1
    //   74: aload_1
    //   75: ifnull +762 -> 837
    //   78: aload_1
    //   79: checkcast 114	java/io/Closeable
    //   82: astore_1
    //   83: aconst_null
    //   84: astore_2
    //   85: aload_1
    //   86: astore 4
    //   88: aload_1
    //   89: checkcast 178	android/database/Cursor
    //   92: astore 4
    //   94: new 626	com/truecaller/messaging/transport/im/s$a
    //   97: astore 5
    //   99: aload 5
    //   101: aload 4
    //   103: invokespecial 628	com/truecaller/messaging/transport/im/s$a:<init>	(Landroid/database/Cursor;)V
    //   106: aload 5
    //   108: checkcast 630	c/g/a/a
    //   111: astore 5
    //   113: ldc_w 632
    //   116: astore 8
    //   118: aload 5
    //   120: aload 8
    //   122: invokestatic 29	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   125: new 634	c/m/h
    //   128: astore 8
    //   130: new 636	c/m/o$d
    //   133: astore 6
    //   135: aload 6
    //   137: aload 5
    //   139: invokespecial 639	c/m/o$d:<init>	(Lc/g/a/a;)V
    //   142: aload 6
    //   144: checkcast 641	c/g/a/b
    //   147: astore 6
    //   149: aload 8
    //   151: aload 5
    //   153: aload 6
    //   155: invokespecial 644	c/m/h:<init>	(Lc/g/a/a;Lc/g/a/b;)V
    //   158: aload 8
    //   160: checkcast 646	c/m/i
    //   163: astore 8
    //   165: aload 8
    //   167: invokestatic 651	c/m/l:b	(Lc/m/i;)Lc/m/i;
    //   170: astore 5
    //   172: new 653	com/truecaller/messaging/transport/im/s$b
    //   175: astore 8
    //   177: aload 8
    //   179: aload 4
    //   181: invokespecial 654	com/truecaller/messaging/transport/im/s$b:<init>	(Landroid/database/Cursor;)V
    //   184: aload 8
    //   186: checkcast 641	c/g/a/b
    //   189: astore 8
    //   191: aload 5
    //   193: aload 8
    //   195: invokestatic 657	c/m/l:d	(Lc/m/i;Lc/g/a/b;)Lc/m/i;
    //   198: astore 4
    //   200: aload 4
    //   202: invokestatic 660	c/m/l:d	(Lc/m/i;)Ljava/util/List;
    //   205: astore 4
    //   207: aload_1
    //   208: aconst_null
    //   209: invokestatic 173	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   212: aload 4
    //   214: ifnonnull +6 -> 220
    //   217: goto +620 -> 837
    //   220: aload 4
    //   222: checkcast 662	java/lang/Iterable
    //   225: astore 4
    //   227: aload 4
    //   229: invokeinterface 666 1 0
    //   234: astore_1
    //   235: aload_1
    //   236: invokeinterface 671 1 0
    //   241: istore 9
    //   243: iload 9
    //   245: ifeq +575 -> 820
    //   248: aload_1
    //   249: invokeinterface 674 1 0
    //   254: checkcast 65	com/truecaller/messaging/transport/im/d
    //   257: astore 4
    //   259: invokestatic 605	java/lang/System:currentTimeMillis	()J
    //   262: lstore 10
    //   264: aload_0
    //   265: aload 4
    //   267: invokespecial 677	com/truecaller/messaging/transport/im/s:a	(Lcom/truecaller/messaging/transport/im/d;)Lcom/truecaller/messaging/data/types/Entity;
    //   270: astore 6
    //   272: invokestatic 605	java/lang/System:currentTimeMillis	()J
    //   275: lload 10
    //   277: lsub
    //   278: lstore 12
    //   280: aload 6
    //   282: instanceof 679
    //   285: istore 14
    //   287: iload 14
    //   289: ifne +12 -> 301
    //   292: iconst_0
    //   293: istore 14
    //   295: aconst_null
    //   296: astore 5
    //   298: goto +7 -> 305
    //   301: aload 6
    //   303: astore 5
    //   305: aload 5
    //   307: checkcast 679	com/truecaller/messaging/data/types/BinaryEntity
    //   310: astore 5
    //   312: aload 5
    //   314: ifnull +13 -> 327
    //   317: aload 5
    //   319: getfield 681	com/truecaller/messaging/data/types/BinaryEntity:d	J
    //   322: lstore 10
    //   324: goto +7 -> 331
    //   327: iconst_m1
    //   328: i2l
    //   329: lstore 10
    //   331: lload 10
    //   333: l2f
    //   334: ldc_w 683
    //   337: fdiv
    //   338: fstore 15
    //   340: getstatic 686	java/util/concurrent/TimeUnit:MILLISECONDS	Ljava/util/concurrent/TimeUnit;
    //   343: lload 12
    //   345: invokevirtual 689	java/util/concurrent/TimeUnit:toSeconds	(J)J
    //   348: lstore 16
    //   350: lload 16
    //   352: l2f
    //   353: fstore 18
    //   355: fload 15
    //   357: fload 18
    //   359: fdiv
    //   360: fstore 15
    //   362: new 691	com/truecaller/analytics/e$a
    //   365: astore 19
    //   367: aload 19
    //   369: ldc_w 693
    //   372: invokespecial 694	com/truecaller/analytics/e$a:<init>	(Ljava/lang/String;)V
    //   375: aload 6
    //   377: checkcast 77	com/truecaller/messaging/data/types/Entity
    //   380: getfield 697	com/truecaller/messaging/data/types/Entity:j	Ljava/lang/String;
    //   383: astore 20
    //   385: aload 19
    //   387: ldc_w 696
    //   390: aload 20
    //   392: invokevirtual 700	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   395: astore 19
    //   397: ldc_w 702
    //   400: astore 21
    //   402: aload 6
    //   404: checkcast 77	com/truecaller/messaging/data/types/Entity
    //   407: getfield 705	com/truecaller/messaging/data/types/Entity:i	I
    //   410: istore 22
    //   412: iload 22
    //   414: tableswitch	default:+34->448, 0:+74->488, 1:+66->480, 2:+58->472, 3:+50->464, 4:+42->456
    //   448: ldc_w 707
    //   451: astore 20
    //   453: goto +40 -> 493
    //   456: ldc_w 709
    //   459: astore 20
    //   461: goto +32 -> 493
    //   464: ldc_w 711
    //   467: astore 20
    //   469: goto +24 -> 493
    //   472: ldc_w 713
    //   475: astore 20
    //   477: goto +16 -> 493
    //   480: ldc_w 715
    //   483: astore 20
    //   485: goto +8 -> 493
    //   488: ldc_w 717
    //   491: astore 20
    //   493: aload 19
    //   495: aload 21
    //   497: aload 20
    //   499: invokevirtual 700	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   502: ldc_w 719
    //   505: lload 10
    //   507: invokevirtual 722	com/truecaller/analytics/e$a:a	(Ljava/lang/String;J)Lcom/truecaller/analytics/e$a;
    //   510: astore 19
    //   512: ldc_w 724
    //   515: astore 21
    //   517: lload 10
    //   519: invokestatic 728	com/truecaller/messaging/transport/im/u:b	(J)Ljava/lang/String;
    //   522: astore 5
    //   524: aload 19
    //   526: aload 21
    //   528: aload 5
    //   530: invokevirtual 700	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   533: astore 5
    //   535: fload 15
    //   537: f2d
    //   538: dstore 23
    //   540: dload 23
    //   542: invokestatic 733	java/lang/Double:valueOf	(D)Ljava/lang/Double;
    //   545: astore 8
    //   547: aload 5
    //   549: aload 8
    //   551: invokevirtual 736	com/truecaller/analytics/e$a:a	(Ljava/lang/Double;)Lcom/truecaller/analytics/e$a;
    //   554: astore 5
    //   556: lload 12
    //   558: invokestatic 740	com/truecaller/messaging/transport/im/u:a	(J)Ljava/lang/String;
    //   561: astore_3
    //   562: aload 5
    //   564: ldc_w 738
    //   567: aload_3
    //   568: invokevirtual 700	com/truecaller/analytics/e$a:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/truecaller/analytics/e$a;
    //   571: invokevirtual 743	com/truecaller/analytics/e$a:a	()Lcom/truecaller/analytics/e;
    //   574: astore 5
    //   576: aload_0
    //   577: getfield 59	com/truecaller/messaging/transport/im/s:f	Lcom/truecaller/analytics/b;
    //   580: astore 8
    //   582: aload 5
    //   584: ldc_w 745
    //   587: invokestatic 84	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   590: aload 8
    //   592: aload 5
    //   594: invokeinterface 750 2 0
    //   599: aload 6
    //   601: checkcast 77	com/truecaller/messaging/data/types/Entity
    //   604: getfield 705	com/truecaller/messaging/data/types/Entity:i	I
    //   607: istore 14
    //   609: iconst_2
    //   610: istore 7
    //   612: aconst_null
    //   613: astore_3
    //   614: iload 14
    //   616: ifeq +89 -> 705
    //   619: new 415	android/content/Intent
    //   622: astore 4
    //   624: aload 4
    //   626: ldc_w 752
    //   629: invokespecial 420	android/content/Intent:<init>	(Ljava/lang/String;)V
    //   632: aload 6
    //   634: checkcast 77	com/truecaller/messaging/data/types/Entity
    //   637: getfield 754	com/truecaller/messaging/data/types/Entity:h	J
    //   640: lstore 25
    //   642: aload 4
    //   644: ldc_w 614
    //   647: lload 25
    //   649: invokevirtual 757	android/content/Intent:putExtra	(Ljava/lang/String;J)Landroid/content/Intent;
    //   652: astore 4
    //   654: aload 6
    //   656: checkcast 77	com/truecaller/messaging/data/types/Entity
    //   659: getfield 705	com/truecaller/messaging/data/types/Entity:i	I
    //   662: istore 27
    //   664: aload 4
    //   666: ldc_w 759
    //   669: iload 27
    //   671: invokevirtual 762	android/content/Intent:putExtra	(Ljava/lang/String;I)Landroid/content/Intent;
    //   674: astore 4
    //   676: aload_0
    //   677: getfield 51	com/truecaller/messaging/transport/im/s:b	Ldagger/a;
    //   680: invokeinterface 439 1 0
    //   685: checkcast 441	com/truecaller/messaging/transport/m
    //   688: astore 5
    //   690: aload 5
    //   692: iload 7
    //   694: aload 4
    //   696: iconst_0
    //   697: invokeinterface 444 4 0
    //   702: goto -467 -> 235
    //   705: new 415	android/content/Intent
    //   708: astore 5
    //   710: aload 5
    //   712: ldc_w 764
    //   715: invokespecial 420	android/content/Intent:<init>	(Ljava/lang/String;)V
    //   718: aload 6
    //   720: checkcast 424	android/os/Parcelable
    //   723: astore 6
    //   725: aload 5
    //   727: ldc_w 766
    //   730: aload 6
    //   732: invokevirtual 428	android/content/Intent:putExtra	(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    //   735: astore 5
    //   737: aload_0
    //   738: getfield 51	com/truecaller/messaging/transport/im/s:b	Ldagger/a;
    //   741: invokeinterface 439 1 0
    //   746: checkcast 441	com/truecaller/messaging/transport/m
    //   749: iload 7
    //   751: aload 5
    //   753: iconst_0
    //   754: invokeinterface 444 4 0
    //   759: aload_0
    //   760: getfield 63	com/truecaller/messaging/transport/im/s:h	Landroid/content/ContentResolver;
    //   763: astore 5
    //   765: invokestatic 769	com/truecaller/content/TruecallerContract$p:a	()Landroid/net/Uri;
    //   768: astore 8
    //   770: ldc_w 771
    //   773: astore 6
    //   775: iconst_1
    //   776: istore 28
    //   778: iload 28
    //   780: anewarray 93	java/lang/String
    //   783: astore 29
    //   785: aload 4
    //   787: getfield 773	com/truecaller/messaging/transport/im/d:a	J
    //   790: lstore 30
    //   792: lload 30
    //   794: invokestatic 97	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   797: astore 4
    //   799: aload 29
    //   801: iconst_0
    //   802: aload 4
    //   804: aastore
    //   805: aload 5
    //   807: aload 8
    //   809: aload 6
    //   811: aload 29
    //   813: invokevirtual 776	android/content/ContentResolver:delete	(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    //   816: pop
    //   817: goto -582 -> 235
    //   820: return
    //   821: astore 4
    //   823: goto +6 -> 829
    //   826: astore_2
    //   827: aload_2
    //   828: athrow
    //   829: aload_1
    //   830: aload_2
    //   831: invokestatic 173	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   834: aload 4
    //   836: athrow
    //   837: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	838	0	this	s
    //   4	826	1	localObject1	Object
    //   8	77	2	localUri	Uri
    //   826	5	2	localThrowable	Throwable
    //   12	602	3	str1	String
    //   43	760	4	localObject2	Object
    //   821	14	4	localObject3	Object
    //   48	758	5	localObject4	Object
    //   53	757	6	localObject5	Object
    //   56	694	7	i	int
    //   59	749	8	localObject6	Object
    //   241	3	9	bool1	boolean
    //   262	256	10	l1	long
    //   278	279	12	l2	long
    //   285	9	14	bool2	boolean
    //   607	8	14	j	int
    //   338	198	15	f1	float
    //   348	3	16	l3	long
    //   353	5	18	f2	float
    //   365	160	19	locala	com.truecaller.analytics.e.a
    //   383	115	20	str2	String
    //   400	127	21	str3	String
    //   410	3	22	k	int
    //   538	3	23	d1	double
    //   640	8	25	l4	long
    //   662	8	27	m	int
    //   776	3	28	n	int
    //   783	29	29	arrayOfString	String[]
    //   790	3	30	l5	long
    // Exception table:
    //   from	to	target	type
    //   827	829	821	finally
    //   88	92	826	finally
    //   94	97	826	finally
    //   101	106	826	finally
    //   106	111	826	finally
    //   120	125	826	finally
    //   125	128	826	finally
    //   130	133	826	finally
    //   137	142	826	finally
    //   142	147	826	finally
    //   153	158	826	finally
    //   158	163	826	finally
    //   165	170	826	finally
    //   172	175	826	finally
    //   179	184	826	finally
    //   184	189	826	finally
    //   193	198	826	finally
    //   200	205	826	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */