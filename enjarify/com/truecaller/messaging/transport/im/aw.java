package com.truecaller.messaging.transport.im;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import c.g.b.k;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public final class aw
  implements Application.ActivityLifecycleCallbacks
{
  private final Map a;
  
  public aw()
  {
    Object localObject = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject).<init>();
    localObject = (Map)localObject;
    a = ((Map)localObject);
  }
  
  public final void onActivityCreated(Activity paramActivity, Bundle paramBundle) {}
  
  public final void onActivityDestroyed(Activity paramActivity) {}
  
  public final void onActivityPaused(Activity paramActivity) {}
  
  public final void onActivityResumed(Activity paramActivity) {}
  
  public final void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
  
  public final void onActivityStarted(Activity paramActivity)
  {
    k.b(paramActivity, "activity");
    Object localObject1 = ax.a();
    Object localObject2 = paramActivity.getClass();
    boolean bool = ((Set)localObject1).contains(localObject2);
    if (!bool) {
      return;
    }
    localObject1 = new com/truecaller/messaging/transport/im/ci;
    ((ci)localObject1).<init>();
    a.put(paramActivity, localObject1);
    localObject2 = new android/content/Intent;
    Object localObject3 = paramActivity;
    localObject3 = (Context)paramActivity;
    ((Intent)localObject2).<init>((Context)localObject3, ImSubscriptionService.class);
    localObject1 = (ServiceConnection)localObject1;
    paramActivity.bindService((Intent)localObject2, (ServiceConnection)localObject1, 1);
  }
  
  public final void onActivityStopped(Activity paramActivity)
  {
    k.b(paramActivity, "activity");
    ServiceConnection localServiceConnection = (ServiceConnection)a.remove(paramActivity);
    if (localServiceConnection != null)
    {
      paramActivity.unbindService(localServiceConnection);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.aw
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */