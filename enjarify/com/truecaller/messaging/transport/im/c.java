package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.utils.extensions.m;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import okhttp3.ac;
import okhttp3.w;

final class c
  extends ac
{
  private final ContentResolver a;
  private final BinaryEntity b;
  
  public c(ContentResolver paramContentResolver, BinaryEntity paramBinaryEntity)
  {
    a = paramContentResolver;
    b = paramBinaryEntity;
  }
  
  public final w a()
  {
    return w.b(b.j);
  }
  
  public final void a(d.d paramd)
  {
    if (paramd != null)
    {
      InputStream localInputStream = null;
      try
      {
        Object localObject1 = a;
        Object localObject2 = b;
        localObject2 = b;
        localInputStream = ((ContentResolver)localObject1).openInputStream((Uri)localObject2);
        localObject1 = "input";
        k.a(localInputStream, (String)localObject1);
        paramd = paramd.c();
        localObject1 = "sink.outputStream()";
        k.a(paramd, (String)localObject1);
        m.a(localInputStream, paramd);
        return;
      }
      finally
      {
        com.truecaller.utils.extensions.d.a((Closeable)localInputStream);
      }
    }
    paramd = new java/io/IOException;
    paramd.<init>();
    throw ((Throwable)paramd);
  }
  
  public final long b()
  {
    long l = -1;
    try
    {
      Object localObject1 = a;
      Object localObject2 = b;
      localObject2 = b;
      localObject1 = ((ContentResolver)localObject1).openInputStream((Uri)localObject2);
      if (localObject1 != null)
      {
        int i = ((InputStream)localObject1).available();
        l = i;
      }
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
    return l;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */