package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class at$e
  extends u
{
  private final long[] b;
  
  private at$e(e parame, long[] paramArrayOfLong)
  {
    super(parame);
    b = paramArrayOfLong;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".markReactionsSeenByMessageIds(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.at.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */