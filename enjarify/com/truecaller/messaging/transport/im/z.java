package com.truecaller.messaging.transport.im;

import dagger.a.d;
import javax.inject.Provider;

public final class z
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private z(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static z a(Provider paramProvider1, Provider paramProvider2)
  {
    z localz = new com/truecaller/messaging/transport/im/z;
    localz.<init>(paramProvider1, paramProvider2);
    return localz;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */