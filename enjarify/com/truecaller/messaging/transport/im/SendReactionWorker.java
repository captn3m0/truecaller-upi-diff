package com.truecaller.messaging.transport.im;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.e;
import c.g.b.k;
import c.l;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.w;
import com.truecaller.bp;

public final class SendReactionWorker
  extends Worker
{
  public static final SendReactionWorker.a c;
  public q b;
  
  static
  {
    SendReactionWorker.a locala = new com/truecaller/messaging/transport/im/SendReactionWorker$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public SendReactionWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject1 = getInputData();
    Object localObject2 = "raw_id";
    String str1 = ((e)localObject1).b((String)localObject2);
    if (str1 == null)
    {
      localObject1 = ListenableWorker.a.a();
      k.a(localObject1, "Result.success()");
      return (ListenableWorker.a)localObject1;
    }
    long l = getInputData().a("message_id", -1);
    localObject1 = getInputData();
    localObject2 = "from_peer_id";
    String str2 = ((e)localObject1).b((String)localObject2);
    if (str2 == null)
    {
      localObject1 = ListenableWorker.a.a();
      k.a(localObject1, "Result.success()");
      return (ListenableWorker.a)localObject1;
    }
    String str3 = getInputData().b("to_peer_id");
    String str4 = getInputData().b("to_group_id");
    localObject1 = getInputData();
    localObject2 = "emoji";
    String str5 = ((e)localObject1).b((String)localObject2);
    q localq = b;
    if (localq == null)
    {
      localObject1 = "imManager";
      k.a((String)localObject1);
    }
    localObject1 = (SendResult)localq.a(str1, l, str2, str3, str4, str5).d();
    int i;
    if (localObject1 != null)
    {
      localObject2 = cg.a;
      i = ((SendResult)localObject1).ordinal();
      i = localObject2[i];
    }
    switch (i)
    {
    default: 
      localObject1 = new c/l;
      ((l)localObject1).<init>();
      throw ((Throwable)localObject1);
    case 3: 
      localObject1 = ListenableWorker.a.a();
      k.a(localObject1, "Result.success()");
      return (ListenableWorker.a)localObject1;
    case 2: 
      i = getRunAttemptCount();
      int j = 3;
      if (i < j) {
        localObject1 = ListenableWorker.a.b();
      } else {
        localObject1 = ListenableWorker.a.a();
      }
      k.a(localObject1, "if (runAttemptCount < MA…y() else Result.success()");
      return (ListenableWorker.a)localObject1;
    }
    localObject1 = ListenableWorker.a.a();
    k.a(localObject1, "Result.success()");
    return (ListenableWorker.a)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.SendReactionWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */