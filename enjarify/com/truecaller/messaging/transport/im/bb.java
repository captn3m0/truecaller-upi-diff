package com.truecaller.messaging.transport.im;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.Context;
import androidx.work.g;
import androidx.work.p;
import c.a.an;
import c.g.b.k;
import com.truecaller.api.services.messenger.v1.models.input.InputReportType;
import com.truecaller.content.TruecallerContract.w;
import com.truecaller.featuretoggles.e;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.transport.c;
import com.truecaller.messaging.transport.f;
import com.truecaller.messaging.transport.i;
import com.truecaller.multisim.h;
import java.util.List;
import java.util.Set;

public final class bb
  extends c
{
  private final p d;
  
  public bb(Context paramContext, h paramh, e parame, p paramp)
  {
    super(paramContext, paramh, parame);
    d = paramp;
  }
  
  private final void a(List paramList, r paramr, v paramv, InputReportType paramInputReportType)
  {
    int i = paramv.l();
    String str = null;
    int j = 1;
    if (i == j) {
      i = 1;
    } else {
      i = 0;
    }
    Object localObject1 = ContentProviderOperation.newUpdate(TruecallerContract.w.a());
    Object localObject2 = "message_id=?";
    String[] arrayOfString = new String[j];
    long l = paramr.a();
    paramr = String.valueOf(l);
    arrayOfString[0] = paramr;
    paramr = ((ContentProviderOperation.Builder)localObject1).withSelection((String)localObject2, arrayOfString);
    int k;
    if (i != 0) {
      k = 5;
    } else {
      k = 2;
    }
    localObject2 = bc.a;
    int m = paramInputReportType.ordinal();
    int n = localObject2[m];
    Object localObject3;
    switch (n)
    {
    default: 
      localObject3 = new String[j];
      localObject2 = String.valueOf(paramInputReportType);
      localObject1 = "Unknown report type ".concat((String)localObject2);
      localObject3[0] = localObject1;
      AssertionUtil.AlwaysFatal.fail((String[])localObject3);
      break;
    case 2: 
      str = "read_sync_status";
      localObject3 = Integer.valueOf(k);
      paramr.withValue(str, localObject3);
      break;
    case 1: 
      str = "delivery_sync_status";
      localObject3 = Integer.valueOf(k);
      paramr.withValue(str, localObject3);
    }
    paramr = paramr.build();
    str = "update.build()";
    k.a(paramr, str);
    paramList.add(paramr);
    if (i != 0) {
      return;
    }
    paramList = SendImReportWorker.c;
    paramList = paramv.m();
    paramr = paramv.k();
    paramv = paramv.p();
    paramList = SendImReportWorker.a.a(paramInputReportType, paramList, paramr, paramv);
    paramr = d;
    paramInputReportType = g.c;
    paramr.a("SendImReportV2", paramInputReportType, paramList);
  }
  
  private static boolean a(r paramr, v paramv)
  {
    paramv = (CharSequence)paramv.m();
    int i = paramv.length();
    boolean bool = true;
    if (i > 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      paramv = null;
    }
    if (i != 0)
    {
      int j = paramr.i() & 0x20;
      if (j != 0) {
        return bool;
      }
    }
    return false;
  }
  
  private static boolean a(v paramv)
  {
    int i = paramv.n();
    int j = 1;
    if (i == j) {
      return j;
    }
    return false;
  }
  
  private static boolean b(v paramv)
  {
    int i = paramv.o();
    int j = 1;
    if (i == j) {
      return j;
    }
    return false;
  }
  
  public final Set a(long paramLong, f paramf, i parami, Participant paramParticipant, boolean paramBoolean)
  {
    k.b(paramf, "threadInfoCache");
    k.b(parami, "participantCache");
    k.b(paramParticipant, "participant");
    Participant[] arrayOfParticipant = new Participant[1];
    arrayOfParticipant[0] = paramParticipant;
    return an.b(arrayOfParticipant);
  }
  
  public final boolean a(int paramInt)
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bb
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */