package com.truecaller.messaging.transport.im;

import c.g.b.k;

public final class bx
{
  final String a;
  final String b;
  final String c;
  final Long d;
  private final int e;
  
  public bx(int paramInt, String paramString1, String paramString2, String paramString3, Long paramLong)
  {
    e = paramInt;
    a = paramString1;
    b = paramString2;
    c = paramString3;
    d = paramLong;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof bx;
      if (bool2)
      {
        paramObject = (bx)paramObject;
        int i = e;
        int j = e;
        Object localObject;
        if (i == j)
        {
          i = 1;
        }
        else
        {
          i = 0;
          localObject = null;
        }
        if (i != 0)
        {
          localObject = a;
          String str = a;
          boolean bool3 = k.a(localObject, str);
          if (bool3)
          {
            localObject = b;
            str = b;
            bool3 = k.a(localObject, str);
            if (bool3)
            {
              localObject = c;
              str = c;
              bool3 = k.a(localObject, str);
              if (bool3)
              {
                localObject = d;
                paramObject = d;
                boolean bool4 = k.a(localObject, paramObject);
                if (bool4) {
                  return bool1;
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = e * 31;
    Object localObject = a;
    int j = 0;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    i = (i + k) * 31;
    localObject = b;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    i = (i + k) * 31;
    localObject = c;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    i = (i + k) * 31;
    localObject = d;
    if (localObject != null) {
      j = localObject.hashCode();
    }
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("JoinedImUser(contactId=");
    int i = e;
    localStringBuilder.append(i);
    localStringBuilder.append(", name=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", normalizedNumber=");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", imageUri=");
    localObject = c;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", phonebookId=");
    localObject = d;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bx
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */