package com.truecaller.messaging.transport.im;

import java.util.concurrent.TimeUnit;

public final class u
{
  public static final String a(long paramLong)
  {
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    long l1 = 1L;
    long l2 = localTimeUnit.toMillis(l1);
    paramLong /= l2;
    int i = (int)paramLong;
    if (i == 0) {
      return "0-1";
    }
    int j = 1;
    if (i == j) {
      return "1-2";
    }
    j = 2;
    if (i == j) {
      return "2-3";
    }
    j = 3;
    if (i == j) {
      return "3-4";
    }
    j = 4;
    if (i == j) {
      return "4-5";
    }
    j = 5;
    if (i == j) {
      return "5-6";
    }
    j = 6;
    if (i == j) {
      return "6-7";
    }
    j = 7;
    if (i == j) {
      return "7-8";
    }
    j = 8;
    if (i == j) {
      return "8-9";
    }
    j = 10;
    int k = 9;
    if ((k <= i) && (j >= i)) {
      return "9-10";
    }
    return ">10";
  }
  
  public static final String b(long paramLong)
  {
    long l1 = 1000L;
    paramLong /= l1;
    long l2 = 0L;
    boolean bool1 = l2 < paramLong;
    if (!bool1)
    {
      l2 = 99;
      bool1 = l2 < paramLong;
      if (!bool1) {
        return "0-100";
      }
    }
    l2 = 199L;
    long l3 = 100;
    boolean bool2 = l3 < paramLong;
    if (!bool2)
    {
      bool1 = l2 < paramLong;
      if (!bool1) {
        return "100-200";
      }
    }
    l2 = 299L;
    l3 = 200L;
    bool2 = l3 < paramLong;
    if (!bool2)
    {
      bool1 = l2 < paramLong;
      if (!bool1) {
        return "200-300";
      }
    }
    l2 = 399L;
    l3 = 300L;
    bool2 = l3 < paramLong;
    if (!bool2)
    {
      bool1 = l2 < paramLong;
      if (!bool1) {
        return "300-500";
      }
    }
    l2 = 499L;
    l3 = 400L;
    bool2 = l3 < paramLong;
    if (!bool2)
    {
      bool1 = l2 < paramLong;
      if (!bool1) {
        return "400-500";
      }
    }
    l2 = 599L;
    l3 = 500L;
    bool2 = l3 < paramLong;
    if (!bool2)
    {
      bool1 = l2 < paramLong;
      if (!bool1) {
        return "500-600";
      }
    }
    l2 = 699L;
    l3 = 600L;
    bool2 = l3 < paramLong;
    if (!bool2)
    {
      bool1 = l2 < paramLong;
      if (!bool1) {
        return "600-700";
      }
    }
    l2 = 799L;
    l3 = 700L;
    bool2 = l3 < paramLong;
    if (!bool2)
    {
      bool1 = l2 < paramLong;
      if (!bool1) {
        return "700-800";
      }
    }
    l2 = 899L;
    l3 = 800L;
    bool2 = l3 < paramLong;
    if (!bool2)
    {
      bool1 = l2 < paramLong;
      if (!bool1) {
        return "800-900";
      }
    }
    l2 = 900L;
    bool1 = l2 < paramLong;
    if (!bool1)
    {
      boolean bool3 = l1 < paramLong;
      if (!bool3) {
        return "900-1000";
      }
    }
    return ">1mb";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */