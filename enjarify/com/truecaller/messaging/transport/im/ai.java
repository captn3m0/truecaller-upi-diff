package com.truecaller.messaging.transport.im;

import dagger.a.d;
import javax.inject.Provider;

public final class ai
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private ai(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
  }
  
  public static ai a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    ai localai = new com/truecaller/messaging/transport/im/ai;
    localai.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return localai;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ai
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */