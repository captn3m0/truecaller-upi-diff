package com.truecaller.messaging.transport.im.a;

import android.content.ContentValues;
import com.truecaller.messaging.data.types.ImGroupPermissions;

public final class e
{
  private static final ImGroupPermissions a;
  
  static
  {
    ImGroupPermissions localImGroupPermissions = new com/truecaller/messaging/data/types/ImGroupPermissions;
    localImGroupPermissions.<init>(0, -1 >>> 1, 0, 0);
    a = localImGroupPermissions;
  }
  
  private static final ContentValues b(ImGroupPermissions paramImGroupPermissions)
  {
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    Integer localInteger = Integer.valueOf(a);
    localContentValues.put("actions", localInteger);
    localInteger = Integer.valueOf(b);
    localContentValues.put("role_update_restriction_mask", localInteger);
    localInteger = Integer.valueOf(c);
    localContentValues.put("role_update_mask", localInteger);
    paramImGroupPermissions = Integer.valueOf(d);
    localContentValues.put("self_role_update_mask", paramImGroupPermissions);
    return localContentValues;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */