package com.truecaller.messaging.transport.im.a;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.List;

final class b$d
  extends u
{
  private final List b;
  private final String c;
  private final String d;
  
  private b$d(e parame, List paramList, String paramString1, String paramString2)
  {
    super(parame);
    b = paramList;
    c = paramString1;
    d = paramString2;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".createGroup(");
    Object localObject = b;
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(d, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */