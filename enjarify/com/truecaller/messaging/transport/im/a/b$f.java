package com.truecaller.messaging.transport.im.a;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class b$f
  extends u
{
  private final String b;
  private final String c;
  private final String d;
  
  private b$f(e parame, String paramString1, String paramString2, String paramString3)
  {
    super(parame);
    b = paramString1;
    c = paramString2;
    d = paramString3;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".editGroup(");
    String str = b;
    int i = 2;
    str = a(str, i);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(c, i);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(d, i);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */