package com.truecaller.messaging.transport.im.a;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.messaging.data.types.Participant;

final class b$s
  extends u
{
  private final String b;
  private final Participant c;
  
  private b$s(e parame, String paramString, Participant paramParticipant)
  {
    super(parame);
    b = paramString;
    c = paramParticipant;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".removeParticipant(");
    String str = b;
    int i = 2;
    str = a(str, i);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(c, i);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.b.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */