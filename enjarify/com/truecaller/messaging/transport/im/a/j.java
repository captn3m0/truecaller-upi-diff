package com.truecaller.messaging.transport.im.a;

import c.g.b.k;
import com.truecaller.api.services.messenger.v1.models.GroupAction;
import com.truecaller.messaging.data.types.ImGroupPermissions;
import com.truecaller.utils.n;

public final class j
  implements i
{
  private final n a;
  
  public j(n paramn)
  {
    a = paramn;
  }
  
  private static long b(String paramString)
  {
    paramString = (CharSequence)paramString;
    long l1 = 5381L;
    int i = 0;
    for (;;)
    {
      int j = paramString.length();
      if (i >= j) {
        break;
      }
      j = paramString.charAt(i);
      int k = 5;
      long l2 = (l1 << k) + l1;
      l1 = j + l2;
      i += 1;
    }
    return l1;
  }
  
  public final String a(int paramInt)
  {
    int i = 0x40000000 & paramInt;
    Object[] arrayOfObject = null;
    Integer localInteger;
    if (i != 0)
    {
      paramInt = 2131886614;
      localInteger = Integer.valueOf(paramInt);
    }
    else
    {
      i = 0x20000000 & paramInt;
      if (i != 0)
      {
        paramInt = 2131886612;
        localInteger = Integer.valueOf(paramInt);
      }
      else
      {
        i = paramInt & 0x8;
        if (i != 0)
        {
          paramInt = 2131886615;
          localInteger = Integer.valueOf(paramInt);
        }
        else
        {
          paramInt &= 0x2;
          if (paramInt != 0)
          {
            paramInt = 2131886613;
            localInteger = Integer.valueOf(paramInt);
          }
          else
          {
            paramInt = 0;
            localInteger = null;
          }
        }
      }
    }
    if (localInteger != null)
    {
      paramInt = ((Number)localInteger).intValue();
      n localn = a;
      arrayOfObject = new Object[0];
      return localn.a(paramInt, arrayOfObject);
    }
    return null;
  }
  
  public final String a(String paramString)
  {
    k.b(paramString, "imPeerId");
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("User");
    long l = Math.abs(b(paramString) % 1000000L);
    localStringBuilder.append(l);
    return localStringBuilder.toString();
  }
  
  public final boolean a(ImGroupPermissions paramImGroupPermissions, int paramInt1, int paramInt2)
  {
    String str = "permissions";
    k.b(paramImGroupPermissions, str);
    paramInt2 ^= paramInt1;
    int i = b;
    paramInt1 &= i;
    if (paramInt1 == 0)
    {
      int j = c & paramInt2;
      if (j == paramInt2) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean a(ImGroupPermissions paramImGroupPermissions, GroupAction paramGroupAction)
  {
    k.b(paramImGroupPermissions, "permissions");
    String str = "action";
    k.b(paramGroupAction, str);
    int i = a;
    int j = paramGroupAction.getNumber();
    i &= j;
    return i != 0;
  }
  
  public final boolean a(ImGroupPermissions paramImGroupPermissions, GroupAction paramGroupAction, f paramf)
  {
    k.b(paramImGroupPermissions, "permissions");
    k.b(paramGroupAction, "action");
    String str = "participant";
    k.b(paramf, str);
    boolean bool = a(paramImGroupPermissions, paramGroupAction);
    if (bool)
    {
      int i = b;
      int j = b & i;
      if (j == 0) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */