package com.truecaller.messaging.transport.im.a;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.api.services.messenger.v1.events.Event.x;

final class b$r
  extends u
{
  private final Event.x b;
  
  private b$r(e parame, Event.x paramx)
  {
    super(parame);
    b = paramx;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".onRolesUpdated(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.b.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */