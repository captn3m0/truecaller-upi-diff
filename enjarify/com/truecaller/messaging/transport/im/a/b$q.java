package com.truecaller.messaging.transport.im.a;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.api.services.messenger.v1.events.Event.p;

final class b$q
  extends u
{
  private final Event.p b;
  
  private b$q(e parame, Event.p paramp)
  {
    super(parame);
    b = paramp;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".onParticipantsRemoved(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.b.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */