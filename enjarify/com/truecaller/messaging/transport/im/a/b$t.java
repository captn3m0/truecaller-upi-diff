package com.truecaller.messaging.transport.im.a;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class b$t
  extends u
{
  private final String b;
  private final int c;
  
  private b$t(e parame, String paramString, int paramInt)
  {
    super(parame);
    b = paramString;
    c = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".setGroupNotificationSettings(");
    String str = b;
    int i = 2;
    str = a(str, i);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(Integer.valueOf(c), i);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.b.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */