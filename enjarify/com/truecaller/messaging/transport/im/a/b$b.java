package com.truecaller.messaging.transport.im.a;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.List;

final class b$b
  extends u
{
  private final String b;
  private final List c;
  
  private b$b(e parame, String paramString, List paramList)
  {
    super(parame);
    b = paramString;
    c = paramList;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".addParticipants(");
    String str = b;
    int i = 2;
    str = a(str, i);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(c, i);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */