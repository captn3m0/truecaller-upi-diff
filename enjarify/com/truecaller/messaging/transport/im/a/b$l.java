package com.truecaller.messaging.transport.im.a;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class b$l
  extends u
{
  private final String b;
  private final boolean c;
  
  private b$l(e parame, String paramString, boolean paramBoolean)
  {
    super(parame);
    b = paramString;
    c = paramBoolean;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".leaveGroup(");
    String str = b;
    int i = 2;
    str = a(str, i);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(Boolean.valueOf(c), i);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.b.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */