package com.truecaller.messaging.transport.im.a;

import c.g.b.k;

public final class f
{
  public final String a;
  public final int b;
  public final String c;
  public final String d;
  public final String e;
  public final String f;
  public final long g;
  public final String h;
  
  public f(String paramString1, int paramInt, String paramString2, String paramString3, String paramString4, String paramString5, long paramLong, String paramString6)
  {
    a = paramString1;
    b = paramInt;
    c = paramString2;
    d = paramString3;
    e = paramString4;
    f = paramString5;
    g = paramLong;
    h = paramString6;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof f;
      if (bool2)
      {
        paramObject = (f)paramObject;
        String str1 = a;
        String str2 = a;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          int i = b;
          int j = b;
          if (i == j)
          {
            i = 1;
          }
          else
          {
            i = 0;
            str1 = null;
          }
          if (i != 0)
          {
            str1 = c;
            str2 = c;
            boolean bool3 = k.a(str1, str2);
            if (bool3)
            {
              str1 = d;
              str2 = d;
              bool3 = k.a(str1, str2);
              if (bool3)
              {
                str1 = e;
                str2 = e;
                bool3 = k.a(str1, str2);
                if (bool3)
                {
                  str1 = f;
                  str2 = f;
                  bool3 = k.a(str1, str2);
                  if (bool3)
                  {
                    long l1 = g;
                    long l2 = g;
                    bool3 = l1 < l2;
                    if (!bool3)
                    {
                      bool3 = true;
                    }
                    else
                    {
                      bool3 = false;
                      str1 = null;
                    }
                    if (bool3)
                    {
                      str1 = h;
                      paramObject = h;
                      boolean bool4 = k.a(str1, paramObject);
                      if (bool4) {
                        return bool1;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str1 = a;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    int k = b;
    int j = (j + k) * 31;
    String str2 = c;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = d;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = e;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = f;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    long l1 = g;
    int m = 32;
    long l2 = l1 >>> m;
    l1 ^= l2;
    int n = (int)l1;
    j = (j + n) * 31;
    str2 = h;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ImGroupParticipant(imPeerId=");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append(", roles=");
    int i = b;
    localStringBuilder.append(i);
    localStringBuilder.append(", normalizedNumber=");
    str = c;
    localStringBuilder.append(str);
    localStringBuilder.append(", rawNumber=");
    str = d;
    localStringBuilder.append(str);
    localStringBuilder.append(", name=");
    str = e;
    localStringBuilder.append(str);
    localStringBuilder.append(", imageUrl=");
    str = f;
    localStringBuilder.append(str);
    localStringBuilder.append(", phonebookId=");
    long l = g;
    localStringBuilder.append(l);
    localStringBuilder.append(", tcContactId=");
    str = h;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */