package com.truecaller.messaging.transport.im.a;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.events.Event.d;
import com.truecaller.api.services.messenger.v1.events.Event.f;
import com.truecaller.api.services.messenger.v1.events.Event.n;
import com.truecaller.api.services.messenger.v1.events.Event.p;
import com.truecaller.api.services.messenger.v1.events.Event.x;
import com.truecaller.messaging.data.types.Participant;
import java.util.List;

public final class b
  implements a
{
  private final v a;
  
  public b(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return a.class.equals(paramClass);
  }
  
  public final w a()
  {
    v localv = a;
    b.h localh = new com/truecaller/messaging/transport/im/a/b$h;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localh.<init>(locale, (byte)0);
    return w.a(localv, localh);
  }
  
  public final w a(String paramString)
  {
    v localv = a;
    b.i locali = new com/truecaller/messaging/transport/im/a/b$i;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locali.<init>(locale, paramString, (byte)0);
    return w.a(localv, locali);
  }
  
  public final w a(String paramString, int paramInt)
  {
    v localv = a;
    b.t localt = new com/truecaller/messaging/transport/im/a/b$t;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localt.<init>(locale, paramString, paramInt, (byte)0);
    return w.a(localv, localt);
  }
  
  public final w a(String paramString, Participant paramParticipant)
  {
    v localv = a;
    b.s locals = new com/truecaller/messaging/transport/im/a/b$s;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locals.<init>(locale, paramString, paramParticipant, (byte)0);
    return w.a(localv, locals);
  }
  
  public final w a(String paramString1, String paramString2, int paramInt)
  {
    v localv = a;
    b.u localu = new com/truecaller/messaging/transport/im/a/b$u;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localu.<init>(locale, paramString1, paramString2, paramInt, (byte)0);
    return w.a(localv, localu);
  }
  
  public final w a(String paramString1, String paramString2, String paramString3)
  {
    v localv = a;
    b.f localf = new com/truecaller/messaging/transport/im/a/b$f;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localf.<init>(locale, paramString1, paramString2, paramString3, (byte)0);
    return w.a(localv, localf);
  }
  
  public final w a(String paramString, List paramList)
  {
    v localv = a;
    b.b localb = new com/truecaller/messaging/transport/im/a/b$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramString, paramList, (byte)0);
    return w.a(localv, localb);
  }
  
  public final w a(String paramString, boolean paramBoolean)
  {
    v localv = a;
    b.l locall = new com/truecaller/messaging/transport/im/a/b$l;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locall.<init>(locale, paramString, paramBoolean, (byte)0);
    return w.a(localv, locall);
  }
  
  public final w a(List paramList, String paramString1, String paramString2)
  {
    v localv = a;
    b.d locald = new com/truecaller/messaging/transport/im/a/b$d;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locald.<init>(locale, paramList, paramString1, paramString2, (byte)0);
    return w.a(localv, locald);
  }
  
  public final void a(Event.d paramd)
  {
    v localv = a;
    b.n localn = new com/truecaller/messaging/transport/im/a/b$n;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localn.<init>(locale, paramd, (byte)0);
    localv.a(localn);
  }
  
  public final void a(Event.f paramf)
  {
    v localv = a;
    b.o localo = new com/truecaller/messaging/transport/im/a/b$o;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localo.<init>(locale, paramf, (byte)0);
    localv.a(localo);
  }
  
  public final void a(Event.n paramn)
  {
    v localv = a;
    b.p localp = new com/truecaller/messaging/transport/im/a/b$p;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localp.<init>(locale, paramn, (byte)0);
    localv.a(localp);
  }
  
  public final void a(Event.p paramp)
  {
    v localv = a;
    b.q localq = new com/truecaller/messaging/transport/im/a/b$q;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localq.<init>(locale, paramp, (byte)0);
    localv.a(localq);
  }
  
  public final void a(Event.x paramx)
  {
    v localv = a;
    b.r localr = new com/truecaller/messaging/transport/im/a/b$r;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localr.<init>(locale, paramx, (byte)0);
    localv.a(localr);
  }
  
  public final w b(String paramString)
  {
    v localv = a;
    b.k localk = new com/truecaller/messaging/transport/im/a/b$k;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localk.<init>(locale, paramString, (byte)0);
    return w.a(localv, localk);
  }
  
  public final w b(String paramString, boolean paramBoolean)
  {
    v localv = a;
    b.e locale = new com/truecaller/messaging/transport/im/a/b$e;
    e locale1 = new com/truecaller/androidactors/e;
    locale1.<init>();
    locale.<init>(locale1, paramString, paramBoolean, (byte)0);
    return w.a(localv, locale);
  }
  
  public final void b()
  {
    v localv = a;
    b.c localc = new com/truecaller/messaging/transport/im/a/b$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, (byte)0);
    localv.a(localc);
  }
  
  public final w c(String paramString)
  {
    v localv = a;
    b.j localj = new com/truecaller/messaging/transport/im/a/b$j;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localj.<init>(locale, paramString, (byte)0);
    return w.a(localv, localj);
  }
  
  public final w d(String paramString)
  {
    v localv = a;
    b.a locala = new com/truecaller/messaging/transport/im/a/b$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramString, (byte)0);
    return w.a(localv, locala);
  }
  
  public final w e(String paramString)
  {
    v localv = a;
    b.g localg = new com/truecaller/messaging/transport/im/a/b$g;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localg.<init>(locale, paramString, (byte)0);
    return w.a(localv, localg);
  }
  
  public final void f(String paramString)
  {
    v localv = a;
    b.m localm = new com/truecaller/messaging/transport/im/a/b$m;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localm.<init>(locale, paramString, (byte)0);
    localv.a(localm);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */