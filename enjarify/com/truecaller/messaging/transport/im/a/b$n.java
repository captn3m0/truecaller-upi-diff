package com.truecaller.messaging.transport.im.a;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.api.services.messenger.v1.events.Event.d;

final class b$n
  extends u
{
  private final Event.d b;
  
  private b$n(e parame, Event.d paramd)
  {
    super(parame);
    b = paramd;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".onGroupCreated(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.b.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */