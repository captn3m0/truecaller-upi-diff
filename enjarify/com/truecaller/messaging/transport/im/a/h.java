package com.truecaller.messaging.transport.im.a;

import android.database.Cursor;
import android.database.CursorWrapper;
import c.g.b.k;

public final class h
  extends CursorWrapper
  implements g
{
  private final int a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  
  public h(Cursor paramCursor)
  {
    super(paramCursor);
    int i = getColumnIndexOrThrow("im_peer_id");
    a = i;
    i = getColumnIndexOrThrow("normalized_number");
    b = i;
    i = getColumnIndexOrThrow("raw_number");
    c = i;
    i = getColumnIndexOrThrow("name");
    d = i;
    i = getColumnIndexOrThrow("image_url");
    e = i;
    i = getColumnIndexOrThrow("roles");
    f = i;
    i = getColumnIndexOrThrow("phonebook_id");
    g = i;
    i = getColumnIndexOrThrow("tc_contact_id");
    h = i;
  }
  
  public final f a()
  {
    f localf = new com/truecaller/messaging/transport/im/a/f;
    int i = a;
    String str1 = getString(i);
    k.a(str1, "getString(imPeerId)");
    i = f;
    int j = getInt(i);
    i = b;
    String str2 = getString(i);
    i = c;
    String str3 = getString(i);
    i = d;
    String str4 = getString(i);
    i = e;
    String str5 = getString(i);
    i = g;
    long l = getLong(i);
    i = h;
    String str6 = getString(i);
    localf.<init>(str1, j, str2, str3, str4, str5, l, str6);
    return localf;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */