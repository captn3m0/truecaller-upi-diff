package com.truecaller.messaging.transport.im.a;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class b$e
  extends u
{
  private final String b;
  private final boolean c;
  
  private b$e(e parame, String paramString, boolean paramBoolean)
  {
    super(parame);
    b = paramString;
    c = paramBoolean;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".deleteHistory(");
    String str = b;
    int i = 2;
    str = a(str, i);
    localStringBuilder.append(str);
    localStringBuilder.append(",");
    str = a(Boolean.valueOf(c), i);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */