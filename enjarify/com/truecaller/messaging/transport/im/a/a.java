package com.truecaller.messaging.transport.im.a;

import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.events.Event.d;
import com.truecaller.api.services.messenger.v1.events.Event.f;
import com.truecaller.api.services.messenger.v1.events.Event.n;
import com.truecaller.api.services.messenger.v1.events.Event.p;
import com.truecaller.api.services.messenger.v1.events.Event.x;
import com.truecaller.messaging.data.types.Participant;
import java.util.List;

public abstract interface a
{
  public abstract w a();
  
  public abstract w a(String paramString);
  
  public abstract w a(String paramString, int paramInt);
  
  public abstract w a(String paramString, Participant paramParticipant);
  
  public abstract w a(String paramString1, String paramString2, int paramInt);
  
  public abstract w a(String paramString1, String paramString2, String paramString3);
  
  public abstract w a(String paramString, List paramList);
  
  public abstract w a(String paramString, boolean paramBoolean);
  
  public abstract w a(List paramList, String paramString1, String paramString2);
  
  public abstract void a(Event.d paramd);
  
  public abstract void a(Event.f paramf);
  
  public abstract void a(Event.n paramn);
  
  public abstract void a(Event.p paramp);
  
  public abstract void a(Event.x paramx);
  
  public abstract w b(String paramString);
  
  public abstract w b(String paramString, boolean paramBoolean);
  
  public abstract void b();
  
  public abstract w c(String paramString);
  
  public abstract w d(String paramString);
  
  public abstract w e(String paramString);
  
  public abstract void f(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */