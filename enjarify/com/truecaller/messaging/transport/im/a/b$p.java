package com.truecaller.messaging.transport.im.a;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.api.services.messenger.v1.events.Event.n;

final class b$p
  extends u
{
  private final Event.n b;
  
  private b$p(e parame, Event.n paramn)
  {
    super(parame);
    b = paramn;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".onParticipantsAdded(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.b.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */