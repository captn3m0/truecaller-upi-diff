package com.truecaller.messaging.transport.im.a;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import c.a.ag;
import c.n;
import c.u;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.a.b;
import com.truecaller.api.services.messenger.v1.a.b.a;
import com.truecaller.api.services.messenger.v1.a.d;
import com.truecaller.api.services.messenger.v1.d.b;
import com.truecaller.api.services.messenger.v1.d.b.a;
import com.truecaller.api.services.messenger.v1.d.d;
import com.truecaller.api.services.messenger.v1.events.Event.d;
import com.truecaller.api.services.messenger.v1.events.Event.f;
import com.truecaller.api.services.messenger.v1.events.Event.n;
import com.truecaller.api.services.messenger.v1.events.Event.p;
import com.truecaller.api.services.messenger.v1.events.Event.x;
import com.truecaller.api.services.messenger.v1.k.a;
import com.truecaller.api.services.messenger.v1.models.Peer;
import com.truecaller.api.services.messenger.v1.models.Peer.TypeCase;
import com.truecaller.api.services.messenger.v1.models.Peer.b;
import com.truecaller.api.services.messenger.v1.models.Peer.d;
import com.truecaller.api.services.messenger.v1.models.input.InputNotificationScope;
import com.truecaller.api.services.messenger.v1.models.input.InputNotificationScope.a;
import com.truecaller.api.services.messenger.v1.models.input.InputNotificationSettings;
import com.truecaller.api.services.messenger.v1.models.input.InputNotificationSettings.a;
import com.truecaller.api.services.messenger.v1.models.input.InputNotificationSettings.b;
import com.truecaller.api.services.messenger.v1.models.input.InputNotificationSettings.d;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer.a;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer.d;
import com.truecaller.api.services.messenger.v1.models.input.InputPeer.d.a;
import com.truecaller.api.services.messenger.v1.models.input.a.a;
import com.truecaller.api.services.messenger.v1.n.b;
import com.truecaller.api.services.messenger.v1.n.b.a;
import com.truecaller.api.services.messenger.v1.n.d;
import com.truecaller.api.services.messenger.v1.v.b;
import com.truecaller.api.services.messenger.v1.v.b.a;
import com.truecaller.api.services.messenger.v1.v.d;
import com.truecaller.api.services.messenger.v1.x.b;
import com.truecaller.api.services.messenger.v1.x.b.a;
import com.truecaller.api.services.messenger.v1.z.b;
import com.truecaller.api.services.messenger.v1.z.b.a;
import com.truecaller.api.services.messenger.v1.z.d;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.r;
import com.truecaller.content.TruecallerContract.s;
import com.truecaller.content.TruecallerContract.t;
import com.truecaller.data.entity.Contact;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.ImGroupPermissions;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Participant.a;
import com.truecaller.messaging.transport.im.cb;
import com.truecaller.network.d.j.a;
import com.truecaller.tracking.events.q;
import com.truecaller.tracking.events.q.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.a.d.d;

public final class c
  implements a
{
  private final cb a;
  private final ContentResolver b;
  private final com.truecaller.messaging.data.c c;
  private final com.truecaller.messaging.h d;
  private final com.truecaller.utils.a e;
  private final m f;
  private final com.truecaller.data.access.m g;
  private final com.truecaller.messaging.notifications.a h;
  private final com.truecaller.featuretoggles.e i;
  private final com.truecaller.androidactors.f j;
  private final com.truecaller.androidactors.f k;
  private final com.truecaller.androidactors.f l;
  private final com.truecaller.analytics.b m;
  
  public c(cb paramcb, ContentResolver paramContentResolver, com.truecaller.messaging.data.c paramc, com.truecaller.messaging.h paramh, com.truecaller.utils.a parama, m paramm, com.truecaller.data.access.m paramm1, com.truecaller.messaging.notifications.a parama1, com.truecaller.featuretoggles.e parame, com.truecaller.androidactors.f paramf1, com.truecaller.androidactors.f paramf2, com.truecaller.androidactors.f paramf3, com.truecaller.analytics.b paramb)
  {
    a = paramcb;
    b = paramContentResolver;
    c = paramc;
    d = paramh;
    e = parama;
    f = paramm;
    g = paramm1;
    h = parama1;
    i = parame;
    j = paramf1;
    k = paramf2;
    l = paramf3;
    m = paramb;
  }
  
  private final Participant a(Map paramMap, ImGroupInfo paramImGroupInfo)
  {
    Object localObject1 = new com/truecaller/messaging/data/types/Participant$a;
    ((Participant.a)localObject1).<init>(4);
    Object localObject2 = a;
    localObject1 = ((Participant.a)localObject1).b((String)localObject2).a();
    c.g.b.k.a(localObject1, "Builder(ParticipantTable…pId)\n            .build()");
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    Object localObject3 = ContentProviderOperation.newInsert(TruecallerContract.r.a());
    ContentValues localContentValues = e.a(paramImGroupInfo);
    localObject3 = ((ContentProviderOperation.Builder)localObject3).withValues(localContentValues).build();
    ((ArrayList)localObject2).add(localObject3);
    localObject3 = ContentProviderOperation.newDelete(TruecallerContract.s.a());
    String[] arrayOfString = new String[1];
    String str = a;
    arrayOfString[0] = str;
    localObject3 = ((ContentProviderOperation.Builder)localObject3).withSelection("im_group_id = ?", arrayOfString).build();
    ((ArrayList)localObject2).add(localObject3);
    paramImGroupInfo = a;
    a((ArrayList)localObject2, paramMap, paramImGroupInfo);
    a((ArrayList)localObject2);
    return (Participant)localObject1;
  }
  
  /* Error */
  private final List a(List paramList)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore_2
    //   2: aload_1
    //   3: invokeinterface 172 1 0
    //   8: istore_3
    //   9: iload_3
    //   10: ifeq +7 -> 17
    //   13: invokestatic 178	java/util/Collections:emptyList	()Ljava/util/List;
    //   16: areturn
    //   17: aload_1
    //   18: astore 4
    //   20: aload_1
    //   21: checkcast 180	java/lang/Iterable
    //   24: astore 4
    //   26: new 114	java/util/ArrayList
    //   29: astore 5
    //   31: aload 4
    //   33: bipush 10
    //   35: invokestatic 186	c/a/m:a	(Ljava/lang/Iterable;I)I
    //   38: istore 6
    //   40: aload 5
    //   42: iload 6
    //   44: invokespecial 187	java/util/ArrayList:<init>	(I)V
    //   47: aload 5
    //   49: checkcast 189	java/util/Collection
    //   52: astore 5
    //   54: aload 4
    //   56: invokeinterface 193 1 0
    //   61: astore 4
    //   63: aload 4
    //   65: invokeinterface 198 1 0
    //   70: istore 6
    //   72: iload 6
    //   74: ifeq +31 -> 105
    //   77: aload 4
    //   79: invokeinterface 202 1 0
    //   84: checkcast 204	com/truecaller/messaging/data/types/Participant
    //   87: getfield 206	com/truecaller/messaging/data/types/Participant:f	Ljava/lang/String;
    //   90: astore 7
    //   92: aload 5
    //   94: aload 7
    //   96: invokeinterface 207 2 0
    //   101: pop
    //   102: goto -39 -> 63
    //   105: aload 5
    //   107: checkcast 168	java/util/List
    //   110: astore 5
    //   112: aload_2
    //   113: getfield 87	com/truecaller/messaging/transport/im/a/c:k	Lcom/truecaller/androidactors/f;
    //   116: invokeinterface 211 1 0
    //   121: checkcast 213	com/truecaller/messaging/transport/im/bp
    //   124: astore 4
    //   126: aload 5
    //   128: astore 7
    //   130: aload 5
    //   132: checkcast 189	java/util/Collection
    //   135: astore 7
    //   137: iconst_1
    //   138: istore 8
    //   140: aload 4
    //   142: aload 7
    //   144: iload 8
    //   146: invokeinterface 216 3 0
    //   151: invokevirtual 220	com/truecaller/androidactors/w:d	()Ljava/lang/Object;
    //   154: checkcast 222	java/lang/Boolean
    //   157: astore 4
    //   159: getstatic 226	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   162: astore 9
    //   164: aload 4
    //   166: aload 9
    //   168: invokestatic 229	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/Object;)Z
    //   171: istore_3
    //   172: aconst_null
    //   173: astore 9
    //   175: iload_3
    //   176: ifeq +363 -> 539
    //   179: aload_2
    //   180: getfield 69	com/truecaller/messaging/transport/im/a/c:b	Landroid/content/ContentResolver;
    //   183: astore 10
    //   185: invokestatic 232	com/truecaller/content/TruecallerContract$y:a	()Landroid/net/Uri;
    //   188: astore 11
    //   190: iconst_2
    //   191: anewarray 156	java/lang/String
    //   194: dup
    //   195: iconst_0
    //   196: ldc -22
    //   198: aastore
    //   199: dup
    //   200: iconst_1
    //   201: ldc -20
    //   203: aastore
    //   204: astore 12
    //   206: new 238	java/lang/StringBuilder
    //   209: astore 4
    //   211: aload 4
    //   213: ldc -16
    //   215: invokespecial 243	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   218: aload 5
    //   220: astore 13
    //   222: aload 5
    //   224: checkcast 180	java/lang/Iterable
    //   227: astore 13
    //   229: getstatic 248	com/truecaller/messaging/transport/im/a/c$b:a	Lcom/truecaller/messaging/transport/im/a/c$b;
    //   232: astore 5
    //   234: aload 5
    //   236: astore 14
    //   238: aload 5
    //   240: checkcast 250	c/g/a/b
    //   243: astore 14
    //   245: bipush 31
    //   247: istore 15
    //   249: aload 13
    //   251: aconst_null
    //   252: aconst_null
    //   253: aconst_null
    //   254: iconst_0
    //   255: aconst_null
    //   256: aload 14
    //   258: iload 15
    //   260: invokestatic 254	c/a/m:a	(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lc/g/a/b;I)Ljava/lang/String;
    //   263: astore 5
    //   265: aload 4
    //   267: aload 5
    //   269: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   272: pop
    //   273: aload 4
    //   275: ldc_w 260
    //   278: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   281: pop
    //   282: aload 4
    //   284: invokevirtual 264	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   287: astore 16
    //   289: iconst_0
    //   290: istore_3
    //   291: aconst_null
    //   292: astore 4
    //   294: iconst_0
    //   295: anewarray 156	java/lang/String
    //   298: astore 5
    //   300: aload 7
    //   302: aload 5
    //   304: invokeinterface 268 2 0
    //   309: astore 5
    //   311: aload 5
    //   313: ifnull +210 -> 523
    //   316: aload 5
    //   318: checkcast 270	[Ljava/lang/String;
    //   321: astore 5
    //   323: aload 5
    //   325: arraylength
    //   326: istore 6
    //   328: aload 5
    //   330: iload 6
    //   332: invokestatic 276	java/util/Arrays:copyOf	([Ljava/lang/Object;I)[Ljava/lang/Object;
    //   335: astore 5
    //   337: aload 5
    //   339: astore 13
    //   341: aload 5
    //   343: checkcast 270	[Ljava/lang/String;
    //   346: astore 13
    //   348: aload 10
    //   350: aload 11
    //   352: aload 12
    //   354: aload 16
    //   356: aload 13
    //   358: aconst_null
    //   359: invokevirtual 282	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   362: astore 5
    //   364: aload 5
    //   366: ifnull +155 -> 521
    //   369: aload 5
    //   371: astore 7
    //   373: aload 5
    //   375: checkcast 284	java/io/Closeable
    //   378: astore 7
    //   380: new 114	java/util/ArrayList
    //   383: astore 10
    //   385: aload 10
    //   387: invokespecial 115	java/util/ArrayList:<init>	()V
    //   390: aload 10
    //   392: checkcast 189	java/util/Collection
    //   395: astore 10
    //   397: aload 5
    //   399: invokeinterface 289 1 0
    //   404: istore 17
    //   406: iload 17
    //   408: ifeq +73 -> 481
    //   411: aload 5
    //   413: iconst_0
    //   414: invokeinterface 293 2 0
    //   419: astore 11
    //   421: aload 5
    //   423: iload 8
    //   425: invokeinterface 293 2 0
    //   430: astore 12
    //   432: new 93	com/truecaller/messaging/data/types/Participant$a
    //   435: astore 16
    //   437: aload 16
    //   439: iconst_0
    //   440: invokespecial 97	com/truecaller/messaging/data/types/Participant$a:<init>	(I)V
    //   443: aload 16
    //   445: aload 11
    //   447: invokevirtual 105	com/truecaller/messaging/data/types/Participant$a:b	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Participant$a;
    //   450: astore 11
    //   452: aload 11
    //   454: aload 12
    //   456: invokevirtual 295	com/truecaller/messaging/data/types/Participant$a:d	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Participant$a;
    //   459: astore 11
    //   461: aload 11
    //   463: invokevirtual 108	com/truecaller/messaging/data/types/Participant$a:a	()Lcom/truecaller/messaging/data/types/Participant;
    //   466: astore 11
    //   468: aload 10
    //   470: aload 11
    //   472: invokeinterface 207 2 0
    //   477: pop
    //   478: goto -81 -> 397
    //   481: aload 10
    //   483: checkcast 168	java/util/List
    //   486: astore 10
    //   488: aload 7
    //   490: aconst_null
    //   491: invokestatic 300	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   494: aload 10
    //   496: areturn
    //   497: astore 4
    //   499: goto +12 -> 511
    //   502: astore 4
    //   504: aload 4
    //   506: astore 9
    //   508: aload 4
    //   510: athrow
    //   511: aload 7
    //   513: aload 9
    //   515: invokestatic 300	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   518: aload 4
    //   520: athrow
    //   521: aconst_null
    //   522: areturn
    //   523: new 302	c/u
    //   526: astore 4
    //   528: aload 4
    //   530: ldc_w 304
    //   533: invokespecial 305	c/u:<init>	(Ljava/lang/String;)V
    //   536: aload 4
    //   538: athrow
    //   539: aconst_null
    //   540: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	541	0	this	c
    //   0	541	1	paramList	List
    //   1	179	2	localc	c
    //   8	283	3	bool1	boolean
    //   18	275	4	localObject1	Object
    //   497	1	4	localObject2	Object
    //   502	17	4	localObject3	Object
    //   526	11	4	localu	u
    //   29	393	5	localObject4	Object
    //   38	5	6	n	int
    //   70	3	6	bool2	boolean
    //   326	5	6	i1	int
    //   90	422	7	localObject5	Object
    //   138	286	8	i2	int
    //   162	352	9	localObject6	Object
    //   183	312	10	localObject7	Object
    //   188	283	11	localObject8	Object
    //   204	251	12	localObject9	Object
    //   220	137	13	localObject10	Object
    //   236	21	14	localObject11	Object
    //   247	12	15	i3	int
    //   287	157	16	localObject12	Object
    //   404	3	17	bool3	boolean
    // Exception table:
    //   from	to	target	type
    //   508	511	497	finally
    //   380	383	502	finally
    //   385	390	502	finally
    //   390	395	502	finally
    //   397	404	502	finally
    //   413	419	502	finally
    //   423	430	502	finally
    //   432	435	502	finally
    //   439	443	502	finally
    //   445	450	502	finally
    //   454	459	502	finally
    //   461	466	502	finally
    //   470	478	502	finally
    //   481	486	502	finally
  }
  
  private final void a(ArrayList paramArrayList, String paramString1, String paramString2, int paramInt, ImGroupPermissions paramImGroupPermissions)
  {
    Object localObject1 = ContentProviderOperation.newUpdate(TruecallerContract.s.a());
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = ((ContentProviderOperation.Builder)localObject1).withValue("roles", localObject2);
    String str = "im_group_id=? AND im_peer_id=?";
    int n = 2;
    localObject2 = new String[n];
    localObject2[0] = paramString1;
    int i1 = 1;
    localObject2[i1] = paramString2;
    localObject1 = ((ContentProviderOperation.Builder)localObject1).withSelection(str, (String[])localObject2).build();
    paramArrayList.add(localObject1);
    localObject1 = d.G();
    boolean bool = c.g.b.k.a(paramString2, localObject1);
    if (bool)
    {
      paramString2 = ContentProviderOperation.newUpdate(TruecallerContract.r.a());
      localObject1 = "roles";
      Object localObject3 = Integer.valueOf(paramInt);
      paramString2 = paramString2.withValue((String)localObject1, localObject3);
      localObject3 = e.a(paramImGroupPermissions);
      paramString2 = paramString2.withValues((ContentValues)localObject3);
      localObject3 = "im_group_id = ?";
      paramImGroupPermissions = new String[i1];
      paramImGroupPermissions[0] = paramString1;
      paramString1 = paramString2.withSelection((String)localObject3, paramImGroupPermissions).build();
      paramArrayList.add(paramString1);
    }
  }
  
  private final void a(ArrayList paramArrayList, List paramList, String paramString)
  {
    Object localObject1 = d.G();
    paramList = (Iterable)paramList;
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (Collection)localObject2;
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = nextd;
      if (localObject3 != null) {
        ((Collection)localObject2).add(localObject3);
      }
    }
    localObject2 = (List)localObject2;
    paramList = ContentProviderOperation.newDelete(TruecallerContract.s.a());
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>("im_group_id=? AND im_peer_id IN (");
    Object localObject4 = localObject2;
    localObject4 = (Iterable)localObject2;
    Object localObject5 = c.a.a;
    Object localObject6 = localObject5;
    localObject6 = (c.g.a.b)localObject5;
    int n = 31;
    localObject5 = localObject4;
    localObject5 = c.a.m.a((Iterable)localObject4, null, null, null, 0, null, (c.g.a.b)localObject6, n);
    ((StringBuilder)localObject3).append((String)localObject5);
    ((StringBuilder)localObject3).append(')');
    localObject3 = ((StringBuilder)localObject3).toString();
    int i1 = 1;
    String[] arrayOfString1 = new String[i1];
    arrayOfString1[0] = paramString;
    localObject2 = (Collection)localObject2;
    String[] arrayOfString2 = new String[0];
    localObject2 = ((Collection)localObject2).toArray(arrayOfString2);
    if (localObject2 != null)
    {
      localObject2 = (String[])c.a.f.a(arrayOfString1, (Object[])localObject2);
      paramList = paramList.withSelection((String)localObject3, (String[])localObject2).build();
      paramArrayList.add(paramList);
      boolean bool2 = c.a.m.a((Iterable)localObject4, localObject1);
      if (bool2)
      {
        paramList = ContentProviderOperation.newUpdate(TruecallerContract.r.a());
        localObject2 = Integer.valueOf(0);
        paramList = paramList.withValue("roles", localObject2);
        localObject1 = e.a(e.a());
        paramList = paramList.withValues((ContentValues)localObject1);
        localObject1 = "im_group_id = ?";
        localObject2 = new String[i1];
        localObject2[0] = paramString;
        paramList = paramList.withSelection((String)localObject1, (String[])localObject2).build();
        paramArrayList.add(paramList);
      }
      return;
    }
    paramArrayList = new c/u;
    paramArrayList.<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw paramArrayList;
  }
  
  private static void a(ArrayList paramArrayList, Map paramMap, String paramString)
  {
    paramMap = paramMap.entrySet().iterator();
    for (;;)
    {
      boolean bool = paramMap.hasNext();
      if (!bool) {
        break;
      }
      Object localObject1 = (Map.Entry)paramMap.next();
      Object localObject2 = (Participant)((Map.Entry)localObject1).getKey();
      int n = ((Number)((Map.Entry)localObject1).getValue()).intValue();
      Object localObject3 = ContentProviderOperation.newInsert(TruecallerContract.s.a()).withValue("im_group_id", paramString);
      String str = "im_peer_id";
      localObject2 = d;
      localObject2 = ((ContentProviderOperation.Builder)localObject3).withValue(str, localObject2);
      localObject3 = "roles";
      localObject1 = Integer.valueOf(n);
      localObject1 = ((ContentProviderOperation.Builder)localObject2).withValue((String)localObject3, localObject1).build();
      paramArrayList.add(localObject1);
    }
  }
  
  private final void a(Map paramMap)
  {
    paramMap = paramMap.entrySet().iterator();
    for (;;)
    {
      boolean bool = paramMap.hasNext();
      if (!bool) {
        break;
      }
      Object localObject1 = (Map.Entry)paramMap.next();
      String str1 = (String)((Map.Entry)localObject1).getKey();
      localObject1 = (com.truecaller.api.services.messenger.v1.models.k)((Map.Entry)localObject1).getValue();
      Object localObject2 = g;
      String str2 = ((com.truecaller.api.services.messenger.v1.models.k)localObject1).c();
      localObject2 = ((com.truecaller.data.access.m)localObject2).a(str2);
      if (localObject2 == null)
      {
        localObject2 = new com/truecaller/data/entity/Contact;
        ((Contact)localObject2).<init>();
        str2 = ((com.truecaller.api.services.messenger.v1.models.k)localObject1).c();
        ((Contact)localObject2).setTcId(str2);
        int n = 1;
        ((Contact)localObject2).setSource(n);
        long l1 = 0L;
        ((Contact)localObject2).a(l1);
        str2 = "private";
        ((Contact)localObject2).b(str2);
      }
      str2 = ((com.truecaller.api.services.messenger.v1.models.k)localObject1).a();
      ((Contact)localObject2).l(str2);
      localObject1 = ((com.truecaller.api.services.messenger.v1.models.k)localObject1).b();
      ((Contact)localObject2).j((String)localObject1);
      ((Contact)localObject2).c(str1);
      localObject1 = g;
      ((com.truecaller.data.access.m)localObject1).a((Contact)localObject2);
    }
  }
  
  private final boolean a(String paramString, List paramList, boolean paramBoolean)
  {
    boolean bool1 = paramList.isEmpty();
    boolean bool2 = true;
    if (bool1) {
      return bool2;
    }
    Object localObject1 = (k.a)j.a.a(a);
    if (localObject1 == null) {
      return false;
    }
    try
    {
      Object localObject2 = a.b.a();
      Object localObject3 = paramList;
      localObject3 = (Iterable)paramList;
      Object localObject4 = new java/util/ArrayList;
      int n = 10;
      int i1 = c.a.m.a((Iterable)localObject3, n);
      ((ArrayList)localObject4).<init>(i1);
      localObject4 = (Collection)localObject4;
      localObject3 = ((Iterable)localObject3).iterator();
      boolean bool4;
      Object localObject5;
      for (;;)
      {
        bool4 = ((Iterator)localObject3).hasNext();
        if (!bool4) {
          break;
        }
        localObject5 = ((Iterator)localObject3).next();
        localObject5 = (Participant)localObject5;
        localObject5 = e.a((Participant)localObject5);
        ((Collection)localObject4).add(localObject5);
      }
      localObject4 = (List)localObject4;
      localObject4 = (Iterable)localObject4;
      localObject2 = ((a.b.a)localObject2).a((Iterable)localObject4);
      localObject3 = e.b(paramString);
      localObject2 = ((a.b.a)localObject2).a((InputPeer)localObject3);
      localObject3 = c.j.c.c;
      localObject3 = c.j.c.d();
      long l1 = ((c.j.c)localObject3).c();
      localObject2 = ((a.b.a)localObject2).a(l1);
      localObject2 = ((a.b.a)localObject2).build();
      localObject2 = (a.b)localObject2;
      localObject1 = ((k.a)localObject1).a((a.b)localObject2);
      localObject2 = "stub.addParticipants(it)";
      c.g.b.k.a(localObject1, (String)localObject2);
      localObject2 = "AddParticipants.Request.…tub.addParticipants(it) }";
      c.g.b.k.a(localObject1, (String)localObject2);
      localObject2 = ((a.d)localObject1).b();
      c.g.b.k.a(localObject2, "response.invalidPeersList");
      localObject2 = (Iterable)localObject2;
      localObject3 = new java/util/ArrayList;
      int i3 = c.a.m.a((Iterable)localObject2, n);
      ((ArrayList)localObject3).<init>(i3);
      localObject3 = (Collection)localObject3;
      localObject2 = ((Iterable)localObject2).iterator();
      for (;;)
      {
        boolean bool6 = ((Iterator)localObject2).hasNext();
        if (!bool6) {
          break;
        }
        localObject4 = (Peer)((Iterator)localObject2).next();
        c.g.b.k.a(localObject4, "it");
        localObject4 = ((Peer)localObject4).b();
        localObject5 = "it.user";
        c.g.b.k.a(localObject4, (String)localObject5);
        localObject4 = ((Peer.d)localObject4).a();
        ((Collection)localObject3).add(localObject4);
      }
      localObject2 = c.a.m.i((Iterable)localObject3);
      paramList = (Iterable)paramList;
      localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>();
      localObject3 = (Collection)localObject3;
      localObject4 = paramList.iterator();
      Object localObject6;
      for (;;)
      {
        bool4 = ((Iterator)localObject4).hasNext();
        if (!bool4) {
          break;
        }
        localObject5 = ((Iterator)localObject4).next();
        localObject6 = localObject5;
        localObject6 = d;
        boolean bool7 = ((Set)localObject2).contains(localObject6) ^ bool2;
        if (bool7) {
          ((Collection)localObject3).add(localObject5);
        }
      }
      localObject3 = (Iterable)localObject3;
      localObject4 = new java/util/LinkedHashMap;
      n = ag.a(c.a.m.a((Iterable)localObject3, n));
      int i2 = 16;
      n = c.k.i.c(n, i2);
      ((LinkedHashMap)localObject4).<init>(n);
      localObject3 = ((Iterable)localObject3).iterator();
      for (;;)
      {
        boolean bool3 = ((Iterator)localObject3).hasNext();
        if (!bool3) {
          break;
        }
        localObject7 = ((Iterator)localObject3).next();
        localObject5 = localObject4;
        localObject5 = (Map)localObject4;
        int i4 = ((a.d)localObject1).c();
        localObject6 = Integer.valueOf(i4);
        ((Map)localObject5).put(localObject7, localObject6);
      }
      localObject4 = (Map)localObject4;
      localObject3 = new java/util/ArrayList;
      ((ArrayList)localObject3).<init>();
      a((ArrayList)localObject3, (Map)localObject4, paramString);
      a((ArrayList)localObject3);
      localObject3 = f;
      localObject1 = ((a.d)localObject1).a();
      c.g.b.k.a(localObject1, "response.messageId");
      localObject4 = new java/util/ArrayList;
      ((ArrayList)localObject4).<init>();
      localObject4 = (Collection)localObject4;
      Object localObject7 = paramList.iterator();
      for (;;)
      {
        boolean bool5 = ((Iterator)localObject7).hasNext();
        if (!bool5) {
          break;
        }
        localObject5 = nextd;
        if (localObject5 != null) {
          ((Collection)localObject4).add(localObject5);
        }
      }
      localObject4 = (List)localObject4;
      ((m)localObject3).a(paramString, (String)localObject1, (List)localObject4);
      if (paramBoolean)
      {
        Object localObject8 = new java/util/ArrayList;
        ((ArrayList)localObject8).<init>();
        localObject8 = (Collection)localObject8;
        paramList = paramList.iterator();
        for (;;)
        {
          bool1 = paramList.hasNext();
          if (!bool1) {
            break;
          }
          localObject1 = paramList.next();
          localObject3 = localObject1;
          localObject3 = d;
          boolean bool8 = ((Set)localObject2).contains(localObject3);
          if (bool8) {
            ((Collection)localObject8).add(localObject1);
          }
        }
        localObject8 = (List)localObject8;
        paramList = a((List)localObject8);
        if (paramList != null) {
          a(paramString, paramList, false);
        }
      }
      return bool2;
    }
    catch (RuntimeException localRuntimeException) {}
    return false;
  }
  
  private final ContentProviderResult[] a(ArrayList paramArrayList)
  {
    try
    {
      Object localObject = b;
      String str = TruecallerContract.a();
      paramArrayList = ((ContentResolver)localObject).applyBatch(str, paramArrayList);
      localObject = "contentResolver.applyBat…tAuthority(), operations)";
      c.g.b.k.a(paramArrayList, (String)localObject);
      return paramArrayList;
    }
    catch (OperationApplicationException localOperationApplicationException)
    {
      paramArrayList = (Throwable)localOperationApplicationException;
      AssertionUtil.reportThrowableButNeverCrash(paramArrayList);
    }
    catch (RemoteException localRemoteException)
    {
      paramArrayList = (Throwable)localRemoteException;
      AssertionUtil.reportThrowableButNeverCrash(paramArrayList);
    }
    return new ContentProviderResult[0];
  }
  
  private final n.d b(String paramString, Participant paramParticipant)
  {
    Object localObject1 = (k.a)j.a.a(a);
    if (localObject1 == null) {
      return null;
    }
    try
    {
      Object localObject2 = n.b.a();
      Object localObject3 = e.a(paramParticipant);
      localObject2 = ((n.b.a)localObject2).b((InputPeer)localObject3);
      localObject3 = e.b(paramString);
      localObject2 = ((n.b.a)localObject2).a((InputPeer)localObject3);
      localObject3 = c.j.c.c;
      localObject3 = c.j.c.d();
      long l1 = ((c.j.c)localObject3).c();
      localObject2 = ((n.b.a)localObject2).a(l1);
      localObject2 = ((n.b.a)localObject2).build();
      localObject2 = (n.b)localObject2;
      localObject1 = ((k.a)localObject1).a((n.b)localObject2);
      localObject2 = "stub.removeParticipants(it)";
      c.g.b.k.a(localObject1, (String)localObject2);
      localObject2 = "RemoveParticipants.Reque….removeParticipants(it) }";
      c.g.b.k.a(localObject1, (String)localObject2);
      localObject2 = new java/util/ArrayList;
      ((ArrayList)localObject2).<init>();
      paramParticipant = c.a.m.a(paramParticipant);
      a((ArrayList)localObject2, paramParticipant, paramString);
      a((ArrayList)localObject2);
      return (n.d)localObject1;
    }
    catch (RuntimeException localRuntimeException) {}
    return null;
  }
  
  private final z.d b(String paramString1, String paramString2, int paramInt)
  {
    Object localObject1 = (k.a)j.a.a(a);
    if (localObject1 == null) {
      return null;
    }
    try
    {
      Object localObject2 = z.b.a();
      Object localObject3 = e.b(paramString1);
      localObject2 = ((z.b.a)localObject2).a((InputPeer)localObject3);
      localObject3 = InputPeer.a();
      Object localObject4 = InputPeer.d.a();
      localObject4 = ((InputPeer.d.a)localObject4).a(paramString2);
      localObject3 = ((InputPeer.a)localObject3).a((InputPeer.d.a)localObject4);
      localObject2 = ((z.b.a)localObject2).a((InputPeer.a)localObject3);
      localObject2 = ((z.b.a)localObject2).a(paramInt);
      localObject3 = c.j.c.c;
      localObject3 = c.j.c.d();
      long l1 = ((c.j.c)localObject3).c();
      localObject2 = ((z.b.a)localObject2).a(l1);
      localObject2 = ((z.b.a)localObject2).build();
      localObject2 = (z.b)localObject2;
      localObject1 = ((k.a)localObject1).a((z.b)localObject2);
      localObject2 = "stub.updateRoles(it)";
      c.g.b.k.a(localObject1, (String)localObject2);
      localObject2 = "UpdateRoles.Request.newB… { stub.updateRoles(it) }";
      c.g.b.k.a(localObject1, (String)localObject2);
      localObject2 = new java/util/ArrayList;
      ((ArrayList)localObject2).<init>();
      localObject3 = ((z.d)localObject1).b();
      localObject4 = "response.permissions";
      c.g.b.k.a(localObject3, (String)localObject4);
      ImGroupPermissions localImGroupPermissions = e.a((com.truecaller.api.services.messenger.v1.models.a)localObject3);
      localObject3 = this;
      localObject4 = localObject2;
      a((ArrayList)localObject2, paramString1, paramString2, paramInt, localImGroupPermissions);
      a((ArrayList)localObject2);
      return (z.d)localObject1;
    }
    catch (RuntimeException localRuntimeException) {}
    return null;
  }
  
  private final boolean b(String paramString1, String paramString2, String paramString3)
  {
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    localContentValues.put("title", paramString2);
    localContentValues.put("avatar", paramString3);
    paramString2 = b;
    paramString3 = TruecallerContract.r.a();
    String str = "im_group_id=?";
    boolean bool = true;
    String[] arrayOfString = new String[bool];
    arrayOfString[0] = paramString1;
    int n = paramString2.update(paramString3, localContentValues, str, arrayOfString);
    if (n > 0) {
      return bool;
    }
    return false;
  }
  
  private final void c(String paramString1, String paramString2, String paramString3)
  {
    q.a locala = q.b();
    paramString1 = (CharSequence)paramString1;
    paramString1 = locala.a(paramString1);
    paramString2 = (CharSequence)paramString2;
    paramString1 = paramString1.b(paramString2);
    paramString3 = (CharSequence)paramString3;
    paramString1 = paramString1.c(paramString3);
    paramString2 = (CharSequence)"Receive";
    paramString1 = paramString1.d(paramString2);
    paramString2 = (ae)l.a();
    paramString1 = (d)paramString1.a();
    paramString2.a(paramString1);
    paramString1 = new com/truecaller/analytics/e$a;
    paramString1.<init>("IMGroupInvite");
    paramString1 = paramString1.a("action", "Receive");
    paramString2 = m;
    paramString1 = paramString1.a();
    c.g.b.k.a(paramString1, "it.build()");
    paramString2.b(paramString1);
  }
  
  private final void c(String paramString, boolean paramBoolean)
  {
    paramString = g(paramString);
    if (paramString != null)
    {
      long l1 = ((Number)paramString).longValue();
      paramString = j.a();
      ((com.truecaller.messaging.data.t)paramString).a(l1, 1, 0, paramBoolean).d();
      return;
    }
  }
  
  /* Error */
  private final Long g(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 69	com/truecaller/messaging/transport/im/a/c:b	Landroid/content/ContentResolver;
    //   4: astore_2
    //   5: invokestatic 720	com/truecaller/content/TruecallerContract$i:a	()Landroid/net/Uri;
    //   8: astore_3
    //   9: iconst_1
    //   10: anewarray 156	java/lang/String
    //   13: dup
    //   14: iconst_0
    //   15: ldc_w 722
    //   18: aastore
    //   19: astore 4
    //   21: ldc_w 724
    //   24: astore 5
    //   26: iconst_1
    //   27: istore 6
    //   29: iload 6
    //   31: anewarray 156	java/lang/String
    //   34: astore 7
    //   36: aload 7
    //   38: iconst_0
    //   39: aload_1
    //   40: aastore
    //   41: aload_2
    //   42: aload_3
    //   43: aload 4
    //   45: aload 5
    //   47: aload 7
    //   49: aconst_null
    //   50: invokevirtual 282	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   53: astore_1
    //   54: aconst_null
    //   55: astore_2
    //   56: aload_1
    //   57: ifnull +69 -> 126
    //   60: aload_1
    //   61: checkcast 284	java/io/Closeable
    //   64: astore_1
    //   65: aload_1
    //   66: astore_3
    //   67: aload_1
    //   68: checkcast 286	android/database/Cursor
    //   71: astore_3
    //   72: aload_3
    //   73: invokeinterface 727 1 0
    //   78: istore 8
    //   80: iload 8
    //   82: ifeq +21 -> 103
    //   85: aload_3
    //   86: iconst_0
    //   87: invokeinterface 731 2 0
    //   92: lstore 9
    //   94: lload 9
    //   96: invokestatic 736	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   99: astore_3
    //   100: goto +5 -> 105
    //   103: aconst_null
    //   104: astore_3
    //   105: aload_1
    //   106: aconst_null
    //   107: invokestatic 300	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   110: aload_3
    //   111: areturn
    //   112: astore_3
    //   113: goto +6 -> 119
    //   116: astore_2
    //   117: aload_2
    //   118: athrow
    //   119: aload_1
    //   120: aload_2
    //   121: invokestatic 300	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   124: aload_3
    //   125: athrow
    //   126: aconst_null
    //   127: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	128	0	this	c
    //   0	128	1	paramString	String
    //   4	52	2	localContentResolver	ContentResolver
    //   116	5	2	localThrowable	Throwable
    //   8	103	3	localObject1	Object
    //   112	13	3	localObject2	Object
    //   19	25	4	arrayOfString1	String[]
    //   24	22	5	str	String
    //   27	3	6	n	int
    //   34	14	7	arrayOfString2	String[]
    //   78	3	8	bool	boolean
    //   92	3	9	l1	long
    // Exception table:
    //   from	to	target	type
    //   117	119	112	finally
    //   67	71	116	finally
    //   72	78	116	finally
    //   86	92	116	finally
    //   94	99	116	finally
  }
  
  private final void h(String paramString)
  {
    paramString = (ImGroupInfo)e(paramString).d();
    if (paramString != null)
    {
      h.a(paramString);
      return;
    }
  }
  
  private final boolean i(String paramString)
  {
    Object localObject = i.i();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject).a();
    if (!bool1) {
      return false;
    }
    localObject = b;
    Uri localUri = TruecallerContract.r.a();
    c.g.b.k.a(localUri, "ImGroupInfoTable.getContentUri()");
    String str1 = "roles";
    String str2 = "im_group_id = ?";
    boolean bool2 = true;
    String[] arrayOfString = new String[bool2];
    arrayOfString[0] = paramString;
    paramString = com.truecaller.utils.extensions.h.a((ContentResolver)localObject, localUri, str1, str2, arrayOfString);
    if (paramString != null)
    {
      int n = paramString.intValue();
      if (n != 0) {
        return false;
      }
    }
    return bool2;
  }
  
  /* Error */
  public final w a()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 69	com/truecaller/messaging/transport/im/a/c:b	Landroid/content/ContentResolver;
    //   4: astore_1
    //   5: invokestatic 120	com/truecaller/content/TruecallerContract$r:a	()Landroid/net/Uri;
    //   8: astore_2
    //   9: iconst_1
    //   10: anewarray 156	java/lang/String
    //   13: dup
    //   14: iconst_0
    //   15: ldc_w 645
    //   18: aastore
    //   19: astore_3
    //   20: ldc_w 763
    //   23: astore 4
    //   25: aconst_null
    //   26: astore 5
    //   28: iconst_0
    //   29: istore 6
    //   31: aconst_null
    //   32: astore 7
    //   34: aload_1
    //   35: aload_2
    //   36: aload_3
    //   37: aconst_null
    //   38: aconst_null
    //   39: aload 4
    //   41: invokevirtual 282	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   44: astore_1
    //   45: aconst_null
    //   46: astore_2
    //   47: aload_1
    //   48: ifnull +101 -> 149
    //   51: aload_1
    //   52: astore_3
    //   53: aload_1
    //   54: checkcast 284	java/io/Closeable
    //   57: astore_3
    //   58: new 114	java/util/ArrayList
    //   61: astore 5
    //   63: aload 5
    //   65: invokespecial 115	java/util/ArrayList:<init>	()V
    //   68: aload 5
    //   70: checkcast 189	java/util/Collection
    //   73: astore 5
    //   75: aload_1
    //   76: invokeinterface 289 1 0
    //   81: istore 6
    //   83: iload 6
    //   85: ifeq +31 -> 116
    //   88: iconst_0
    //   89: istore 6
    //   91: aconst_null
    //   92: astore 7
    //   94: aload_1
    //   95: iconst_0
    //   96: invokeinterface 293 2 0
    //   101: astore 7
    //   103: aload 5
    //   105: aload 7
    //   107: invokeinterface 207 2 0
    //   112: pop
    //   113: goto -38 -> 75
    //   116: aload 5
    //   118: astore_1
    //   119: aload 5
    //   121: checkcast 168	java/util/List
    //   124: astore_1
    //   125: aload_3
    //   126: aconst_null
    //   127: invokestatic 300	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   130: goto +21 -> 151
    //   133: astore_1
    //   134: goto +8 -> 142
    //   137: astore_1
    //   138: aload_1
    //   139: astore_2
    //   140: aload_1
    //   141: athrow
    //   142: aload_3
    //   143: aload_2
    //   144: invokestatic 300	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   147: aload_1
    //   148: athrow
    //   149: aconst_null
    //   150: astore_1
    //   151: aload_1
    //   152: ifnull +6 -> 158
    //   155: goto +7 -> 162
    //   158: invokestatic 178	java/util/Collections:emptyList	()Ljava/util/List;
    //   161: astore_1
    //   162: aload_1
    //   163: checkcast 189	java/util/Collection
    //   166: invokestatic 766	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   169: astore_1
    //   170: aload_1
    //   171: ldc_w 768
    //   174: invokestatic 112	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   177: aload_1
    //   178: ldc_w 770
    //   181: invokestatic 112	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   184: aload_1
    //   185: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	186	0	this	c
    //   4	121	1	localObject1	Object
    //   133	1	1	localObject2	Object
    //   137	11	1	localObject3	Object
    //   150	35	1	localObject4	Object
    //   8	136	2	localObject5	Object
    //   19	124	3	localObject6	Object
    //   23	17	4	str1	String
    //   26	94	5	localObject7	Object
    //   29	61	6	bool	boolean
    //   32	74	7	str2	String
    // Exception table:
    //   from	to	target	type
    //   140	142	133	finally
    //   58	61	137	finally
    //   63	68	137	finally
    //   68	73	137	finally
    //   75	81	137	finally
    //   95	101	137	finally
    //   105	113	137	finally
    //   119	124	137	finally
  }
  
  public final w a(String paramString)
  {
    c.g.b.k.b(paramString, "groupId");
    ContentResolver localContentResolver = b;
    String str = d.G();
    Uri localUri = TruecallerContract.t.a(paramString, str);
    paramString = localContentResolver.query(localUri, null, null, null, "is_self DESC, roles DESC, name IS NULL ASC, name COLLATE NOCASE ASC");
    paramString = w.b(c.i(paramString));
    c.g.b.k.a(paramString, "Promise.wrap(it)");
    c.g.b.k.a(paramString, "contentResolver.query(\n ….let { Promise.wrap(it) }");
    return paramString;
  }
  
  public final w a(String paramString, int paramInt)
  {
    c.g.b.k.b(paramString, "groupId");
    Object localObject1 = (k.a)j.a.a(a);
    if (localObject1 == null)
    {
      paramString = w.b(Boolean.FALSE);
      c.g.b.k.a(paramString, "Promise.wrap(false)");
      return paramString;
    }
    Object localObject2 = InputNotificationSettings.a();
    Object localObject3;
    Object localObject4;
    switch (paramInt)
    {
    default: 
      paramString = new java/lang/IllegalArgumentException;
      localObject3 = String.valueOf(paramInt);
      localObject3 = "Unknown notification settings: ".concat((String)localObject3);
      paramString.<init>((String)localObject3);
      throw ((Throwable)paramString);
    case 1: 
      localObject4 = InputNotificationSettings.d.a();
      ((InputNotificationSettings.a)localObject2).a((InputNotificationSettings.d)localObject4);
      break;
    case 0: 
      localObject4 = InputNotificationSettings.b.a();
      ((InputNotificationSettings.a)localObject2).a((InputNotificationSettings.b)localObject4);
    }
    try
    {
      localObject4 = x.b.a();
      Object localObject5 = InputNotificationScope.a();
      localObject5 = ((InputNotificationScope.a)localObject5).a(paramString);
      localObject5 = ((InputNotificationScope.a)localObject5).build();
      localObject5 = (InputNotificationScope)localObject5;
      localObject4 = ((x.b.a)localObject4).a((InputNotificationScope)localObject5);
      localObject2 = ((x.b.a)localObject4).a((InputNotificationSettings.a)localObject2);
      localObject2 = ((x.b.a)localObject2).build();
      localObject2 = (x.b)localObject2;
      ((k.a)localObject1).a((x.b)localObject2);
      localObject1 = b;
      localObject2 = TruecallerContract.r.a();
      localObject4 = new android/content/ContentValues;
      ((ContentValues)localObject4).<init>();
      localObject3 = Integer.valueOf(paramInt);
      ((ContentValues)localObject4).put("notification_settings", (Integer)localObject3);
      localObject5 = new String[1];
      localObject5[0] = paramString;
      ((ContentResolver)localObject1).update((Uri)localObject2, (ContentValues)localObject4, "im_group_id = ?", (String[])localObject5);
      paramString = w.b(Boolean.TRUE);
      c.g.b.k.a(paramString, "Promise.wrap(true)");
      return paramString;
    }
    catch (RuntimeException localRuntimeException)
    {
      paramString = w.b(Boolean.FALSE);
      c.g.b.k.a(paramString, "Promise.wrap(false)");
    }
    return paramString;
  }
  
  public final w a(String paramString, Participant paramParticipant)
  {
    c.g.b.k.b(paramString, "groupId");
    c.g.b.k.b(paramParticipant, "participant");
    Object localObject = b(paramString, paramParticipant);
    if (localObject == null)
    {
      paramString = w.b(Boolean.FALSE);
      c.g.b.k.a(paramString, "Promise.wrap(false)");
      return paramString;
    }
    paramParticipant = d;
    if (paramParticipant != null)
    {
      m localm = f;
      localObject = ((n.d)localObject).a();
      c.g.b.k.a(localObject, "response.messageId");
      String str = "imPeerId";
      c.g.b.k.a(paramParticipant, str);
      localm.c(paramString, (String)localObject, paramParticipant);
    }
    paramString = w.b(Boolean.TRUE);
    c.g.b.k.a(paramString, "Promise.wrap(true)");
    return paramString;
  }
  
  public final w a(String paramString1, String paramString2, int paramInt)
  {
    c.g.b.k.b(paramString1, "groupId");
    c.g.b.k.b(paramString2, "imPeerId");
    Object localObject = b(paramString1, paramString2, paramInt);
    if (localObject == null)
    {
      paramString1 = w.b(Boolean.FALSE);
      c.g.b.k.a(paramString1, "Promise.wrap(false)");
      return paramString1;
    }
    m localm = f;
    localObject = ((z.d)localObject).a();
    c.g.b.k.a(localObject, "response.messageId");
    localm.a(paramString1, (String)localObject, paramInt, paramString2);
    paramString1 = w.b(Boolean.TRUE);
    c.g.b.k.a(paramString1, "Promise.wrap(true)");
    return paramString1;
  }
  
  public final w a(String paramString1, String paramString2, String paramString3)
  {
    c.g.b.k.b(paramString1, "groupId");
    c.g.b.k.b(paramString2, "title");
    Object localObject1 = (k.a)j.a.a(a);
    if (localObject1 == null)
    {
      paramString1 = w.b(Boolean.FALSE);
      c.g.b.k.a(paramString1, "Promise.wrap(false)");
      return paramString1;
    }
    Object localObject2 = v.b.a().a(paramString1);
    Object localObject3 = com.truecaller.api.services.messenger.v1.models.input.a.a().a(paramString2);
    if (paramString3 != null) {
      ((a.a)localObject3).b(paramString3);
    }
    localObject2 = ((v.b.a)localObject2).a((a.a)localObject3);
    localObject3 = c.j.c.c;
    localObject3 = c.j.c.d();
    long l1 = ((c.j.c)localObject3).c();
    localObject2 = (v.b)((v.b.a)localObject2).a(l1).build();
    try
    {
      localObject1 = ((k.a)localObject1).a((v.b)localObject2);
      localObject2 = (ImGroupInfo)e(paramString1).d();
      c.g.b.k.a(localObject1, "response");
      localObject3 = ((v.d)localObject1).b();
      c.g.b.k.a(localObject3, "response.groupInfo");
      localObject3 = ((com.truecaller.api.services.messenger.v1.models.c)localObject3).a();
      c.g.b.k.a(localObject3, "response.groupInfo.title");
      Object localObject4 = ((v.d)localObject1).b();
      Object localObject5 = "response.groupInfo";
      c.g.b.k.a(localObject4, (String)localObject5);
      localObject4 = ((com.truecaller.api.services.messenger.v1.models.c)localObject4).b();
      boolean bool1 = b(paramString1, (String)localObject3, (String)localObject4);
      if (!bool1)
      {
        paramString1 = w.b(Boolean.FALSE);
        c.g.b.k.a(paramString1, "Promise.wrap(false)");
        return paramString1;
      }
      bool1 = false;
      localObject3 = null;
      if (localObject2 != null)
      {
        localObject4 = b;
      }
      else
      {
        bool2 = false;
        localObject4 = null;
      }
      boolean bool2 = c.g.b.k.a(localObject4, paramString2) ^ true;
      if (bool2)
      {
        localObject4 = f;
        localObject5 = new java/lang/StringBuilder;
        ((StringBuilder)localObject5).<init>();
        String str = ((v.d)localObject1).a();
        ((StringBuilder)localObject5).append(str);
        str = "-title";
        ((StringBuilder)localObject5).append(str);
        localObject5 = ((StringBuilder)localObject5).toString();
        ((m)localObject4).e(paramString1, (String)localObject5, paramString2);
      }
      if (localObject2 != null) {
        localObject3 = c;
      }
      boolean bool3 = c.g.b.k.a(localObject3, paramString3) ^ true;
      if (bool3)
      {
        paramString2 = f;
        paramString3 = new java/lang/StringBuilder;
        paramString3.<init>();
        localObject1 = ((v.d)localObject1).a();
        paramString3.append((String)localObject1);
        localObject1 = "-avatar";
        paramString3.append((String)localObject1);
        paramString3 = paramString3.toString();
        paramString2.c(paramString1, paramString3);
      }
      paramString1 = w.b(Boolean.TRUE);
      c.g.b.k.a(paramString1, "Promise.wrap(true)");
      return paramString1;
    }
    catch (RuntimeException localRuntimeException)
    {
      paramString1 = w.b(Boolean.FALSE);
      c.g.b.k.a(paramString1, "Promise.wrap(false)");
    }
    return paramString1;
  }
  
  public final w a(String paramString, List paramList)
  {
    c.g.b.k.b(paramString, "groupId");
    c.g.b.k.b(paramList, "participants");
    paramString = w.b(Boolean.valueOf(a(paramString, paramList, true)));
    c.g.b.k.a(paramString, "Promise.wrap(addParticip…pId, participants, true))");
    return paramString;
  }
  
  public final w a(String paramString, boolean paramBoolean)
  {
    c.g.b.k.b(paramString, "groupId");
    Object localObject = d.G();
    if (localObject == null)
    {
      paramString = w.b(Boolean.FALSE);
      c.g.b.k.a(paramString, "Promise.wrap(false)");
      return paramString;
    }
    localObject = e.a((String)localObject);
    localObject = b(paramString, (Participant)localObject);
    if (localObject == null)
    {
      paramString = w.b(Boolean.FALSE);
      c.g.b.k.a(paramString, "Promise.wrap(false)");
      return paramString;
    }
    m localm;
    if (paramBoolean)
    {
      paramBoolean = false;
      localm = null;
      c(paramString, false);
    }
    else
    {
      localm = f;
      localObject = ((n.d)localObject).a();
      String str = "response.messageId";
      c.g.b.k.a(localObject, str);
      localm.a(paramString, (String)localObject);
    }
    paramString = w.b(Boolean.TRUE);
    c.g.b.k.a(paramString, "Promise.wrap(true)");
    return paramString;
  }
  
  public final w a(List paramList, String paramString1, String paramString2)
  {
    c localc = this;
    Object localObject1 = paramList;
    Object localObject2 = paramString1;
    Object localObject3 = paramString2;
    c.g.b.k.b(paramList, "participants");
    c.g.b.k.b(paramString1, "title");
    Object localObject4 = (k.a)j.a.a(a);
    Object localObject5 = null;
    if (localObject4 == null)
    {
      localObject1 = w.b(null);
      c.g.b.k.a(localObject1, "Promise.wrap(null)");
      return (w)localObject1;
    }
    Object localObject6 = d;
    String str1 = ((com.truecaller.messaging.h)localObject6).G();
    if (str1 == null)
    {
      localObject1 = w.b(null);
      c.g.b.k.a(localObject1, "Promise.wrap(null)");
      return (w)localObject1;
    }
    localObject6 = com.truecaller.api.services.messenger.v1.models.input.a.a().a(paramString1);
    if (paramString2 != null) {
      ((a.a)localObject6).b(paramString2);
    }
    localObject3 = d.b.a();
    Object localObject7 = c.j.c.c;
    localObject7 = c.j.c.d();
    long l1 = ((c.j.c)localObject7).c();
    localObject3 = ((d.b.a)localObject3).a(l1).a((a.a)localObject6);
    localObject1 = (Iterable)localObject1;
    localObject6 = new java/util/ArrayList;
    int n = 10;
    int i2 = c.a.m.a((Iterable)localObject1, n);
    ((ArrayList)localObject6).<init>(i2);
    localObject6 = (Collection)localObject6;
    Object localObject8 = ((Iterable)localObject1).iterator();
    boolean bool4;
    Object localObject9;
    for (;;)
    {
      bool4 = ((Iterator)localObject8).hasNext();
      if (!bool4) {
        break;
      }
      localObject9 = e.a((Participant)((Iterator)localObject8).next());
      ((Collection)localObject6).add(localObject9);
    }
    localObject6 = (Iterable)localObject6;
    localObject3 = ((d.b.a)localObject3).a((Iterable)localObject6).build();
    localObject6 = "Request.newBuilder()\n   …) })\n            .build()";
    c.g.b.k.a(localObject3, (String)localObject6);
    localObject3 = (d.b)localObject3;
    try
    {
      localObject3 = ((k.a)localObject4).a((d.b)localObject3);
      localObject4 = "stub.createGroup(request)";
      c.g.b.k.a(localObject3, (String)localObject4);
      localObject4 = ((d.d)localObject3).c();
      c.g.b.k.a(localObject4, "response.invalidPeersList");
      localObject4 = (Iterable)localObject4;
      localObject5 = new java/util/ArrayList;
      int i5 = c.a.m.a((Iterable)localObject4, n);
      ((ArrayList)localObject5).<init>(i5);
      localObject5 = (Collection)localObject5;
      localObject4 = ((Iterable)localObject4).iterator();
      boolean bool5;
      for (;;)
      {
        bool5 = ((Iterator)localObject4).hasNext();
        if (!bool5) {
          break;
        }
        localObject6 = (Peer)((Iterator)localObject4).next();
        c.g.b.k.a(localObject6, "it");
        localObject6 = ((Peer)localObject6).b();
        localObject8 = "it.user";
        c.g.b.k.a(localObject6, (String)localObject8);
        localObject6 = ((Peer.d)localObject6).a();
        ((Collection)localObject5).add(localObject6);
      }
      localObject4 = c.a.m.i((Iterable)localObject5);
      localObject5 = new java/util/ArrayList;
      ((ArrayList)localObject5).<init>();
      localObject5 = (Collection)localObject5;
      localObject6 = ((Iterable)localObject1).iterator();
      for (;;)
      {
        boolean bool3 = ((Iterator)localObject6).hasNext();
        if (!bool3) {
          break;
        }
        localObject8 = ((Iterator)localObject6).next();
        localObject9 = localObject8;
        localObject9 = d;
        bool4 = ((Set)localObject4).contains(localObject9) ^ true;
        if (bool4) {
          ((Collection)localObject5).add(localObject8);
        }
      }
      localObject5 = (Iterable)localObject5;
      localObject6 = new java/util/LinkedHashMap;
      n = ag.a(c.a.m.a((Iterable)localObject5, n));
      int i3 = 16;
      n = c.k.i.c(n, i3);
      ((LinkedHashMap)localObject6).<init>(n);
      localObject5 = ((Iterable)localObject5).iterator();
      for (;;)
      {
        boolean bool1 = ((Iterator)localObject5).hasNext();
        if (!bool1) {
          break;
        }
        localObject7 = ((Iterator)localObject5).next();
        localObject8 = localObject6;
        localObject8 = (Map)localObject6;
        int i4 = ((d.d)localObject3).e();
        localObject9 = Integer.valueOf(i4);
        ((Map)localObject8).put(localObject7, localObject9);
      }
      localObject6 = (Map)localObject6;
      localObject5 = e.a(str1);
      int i1 = ((d.d)localObject3).d();
      localObject7 = Integer.valueOf(i1);
      localObject5 = c.t.a(localObject5, localObject7);
      localObject5 = ag.a((Map)localObject6, (n)localObject5);
      localObject6 = f;
      localObject7 = ((d.d)localObject3).a();
      c.g.b.k.a(localObject7, "response.groupId");
      localObject8 = ((d.d)localObject3).b();
      c.g.b.k.a(localObject8, "response.messageId");
      ((m)localObject6).b((String)localObject7, (String)localObject8, (String)localObject2);
      localObject2 = new com/truecaller/messaging/data/types/ImGroupInfo;
      localObject8 = ((d.d)localObject3).a();
      c.g.b.k.a(localObject8, "response.groupId");
      localObject6 = ((d.d)localObject3).g();
      c.g.b.k.a(localObject6, "response.groupInfo");
      localObject9 = ((com.truecaller.api.services.messenger.v1.models.c)localObject6).a();
      localObject6 = ((d.d)localObject3).g();
      c.g.b.k.a(localObject6, "response.groupInfo");
      String str2 = ((com.truecaller.api.services.messenger.v1.models.c)localObject6).b();
      long l2 = e.a();
      int i6 = ((d.d)localObject3).d();
      localObject6 = ((d.d)localObject3).f();
      c.g.b.k.a(localObject6, "response.permissions");
      ImGroupPermissions localImGroupPermissions = e.a((com.truecaller.api.services.messenger.v1.models.a)localObject6);
      localObject7 = localObject2;
      ((ImGroupInfo)localObject2).<init>((String)localObject8, (String)localObject9, str2, l2, str1, i6, localImGroupPermissions, 0);
      localObject2 = localc.a((Map)localObject5, (ImGroupInfo)localObject2);
      localObject5 = new java/util/ArrayList;
      ((ArrayList)localObject5).<init>();
      localObject5 = (Collection)localObject5;
      localObject1 = ((Iterable)localObject1).iterator();
      for (;;)
      {
        bool5 = ((Iterator)localObject1).hasNext();
        if (!bool5) {
          break;
        }
        localObject6 = ((Iterator)localObject1).next();
        localObject7 = localObject6;
        localObject7 = d;
        boolean bool2 = ((Set)localObject4).contains(localObject7);
        if (bool2) {
          ((Collection)localObject5).add(localObject6);
        }
      }
      localObject5 = (List)localObject5;
      localObject1 = localc.a((List)localObject5);
      if (localObject1 != null)
      {
        localObject3 = ((d.d)localObject3).a();
        c.g.b.k.a(localObject3, "response.groupId");
        localObject4 = null;
        localc.a((String)localObject3, (List)localObject1, false);
      }
      localObject1 = w.b(localObject2);
      c.g.b.k.a(localObject1, "Promise.wrap(groupParticipant)");
      return (w)localObject1;
    }
    catch (RuntimeException localRuntimeException)
    {
      localObject1 = w.b(null);
      c.g.b.k.a(localObject1, "Promise.wrap(null)");
    }
    return (w)localObject1;
  }
  
  public final void a(Event.d paramd)
  {
    c localc = this;
    Object localObject1 = paramd;
    c.g.b.k.b(paramd, "event");
    Object localObject2 = paramd.f();
    c.g.b.k.a(localObject2, "event.userInfoMap");
    a((Map)localObject2);
    localObject2 = paramd.h();
    Object localObject3 = "event.participantsList";
    c.g.b.k.a(localObject2, (String)localObject3);
    localObject2 = (Iterable)localObject2;
    int n = ag.a(c.a.m.a((Iterable)localObject2, 10));
    int i2 = 16;
    n = c.k.i.c(n, i2);
    Object localObject4 = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject4).<init>(n);
    localObject4 = (Map)localObject4;
    localObject2 = ((Iterable)localObject2).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject2).hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = (com.truecaller.api.services.messenger.v1.models.f)((Iterator)localObject2).next();
      c.g.b.k.a(localObject3, "it");
      localObject5 = ((com.truecaller.api.services.messenger.v1.models.f)localObject3).a();
      localObject6 = "it.peer";
      c.g.b.k.a(localObject5, (String)localObject6);
      localObject5 = com.truecaller.messaging.i.i.a((Peer)localObject5);
      int i1 = ((com.truecaller.api.services.messenger.v1.models.f)localObject3).b();
      localObject3 = Integer.valueOf(i1);
      localObject3 = c.t.a(localObject5, localObject3);
      localObject5 = a;
      localObject3 = b;
      ((Map)localObject4).put(localObject5, localObject3);
    }
    localObject2 = paramd.a();
    c.g.b.k.a(localObject2, "event.sender");
    localObject2 = com.truecaller.messaging.i.i.a((Peer)localObject2);
    localObject3 = Integer.valueOf(paramd.g());
    localObject2 = c.t.a(localObject2, localObject3);
    localObject2 = ag.a((Map)localObject4, (n)localObject2);
    localObject3 = paramd.b();
    c.g.b.k.a(localObject3, "event.groupId");
    boolean bool2 = localc.i((String)localObject3);
    localObject4 = d.G();
    Object localObject5 = paramd.h();
    Object localObject6 = "event.participantsList";
    c.g.b.k.a(localObject5, (String)localObject6);
    localObject5 = ((Iterable)localObject5).iterator();
    boolean bool4;
    do
    {
      bool3 = ((Iterator)localObject5).hasNext();
      if (!bool3) {
        break;
      }
      localObject6 = ((Iterator)localObject5).next();
      localObject7 = localObject6;
      localObject7 = (com.truecaller.api.services.messenger.v1.models.f)localObject6;
      c.g.b.k.a(localObject7, "it");
      localObject7 = ((com.truecaller.api.services.messenger.v1.models.f)localObject7).a();
      c.g.b.k.a(localObject7, "it.peer");
      localObject7 = ((Peer)localObject7).b();
      localObject8 = "it.peer.user";
      c.g.b.k.a(localObject7, (String)localObject8);
      localObject7 = ((Peer.d)localObject7).a();
      bool4 = c.g.b.k.a(localObject7, localObject4);
    } while (!bool4);
    break label438;
    boolean bool3 = false;
    localObject6 = null;
    label438:
    localObject6 = (com.truecaller.api.services.messenger.v1.models.f)localObject6;
    localObject5 = new com/truecaller/messaging/data/types/ImGroupInfo;
    Object localObject8 = paramd.b();
    c.g.b.k.a(localObject8, "event.groupId");
    Object localObject7 = paramd.e();
    c.g.b.k.a(localObject7, "event.groupInfo");
    String str1 = ((com.truecaller.api.services.messenger.v1.models.c)localObject7).a();
    localObject7 = paramd.e();
    c.g.b.k.a(localObject7, "event.groupInfo");
    String str2 = ((com.truecaller.api.services.messenger.v1.models.c)localObject7).b();
    localObject7 = TimeUnit.SECONDS;
    int i4 = paramd.d();
    long l1 = i4;
    l1 = ((TimeUnit)localObject7).toMillis(l1);
    localObject7 = paramd.a();
    c.g.b.k.a(localObject7, "event.sender");
    localObject7 = ((Peer)localObject7).b();
    c.g.b.k.a(localObject7, "event.sender.user");
    String str3 = ((Peer.d)localObject7).a();
    int i3;
    int i5;
    if (localObject6 != null)
    {
      i3 = ((com.truecaller.api.services.messenger.v1.models.f)localObject6).b();
      i5 = i3;
    }
    else
    {
      i3 = 0;
      localObject6 = null;
      i5 = 0;
    }
    localObject6 = paramd.i();
    c.g.b.k.a(localObject6, "event.permissions");
    ImGroupPermissions localImGroupPermissions = e.a((com.truecaller.api.services.messenger.v1.models.a)localObject6);
    localObject7 = localObject5;
    ((ImGroupInfo)localObject5).<init>((String)localObject8, str1, str2, l1, str3, i5, localImGroupPermissions, 0);
    localc.a((Map)localObject2, (ImGroupInfo)localObject5);
    localObject2 = f;
    localObject5 = paramd.b();
    c.g.b.k.a(localObject5, "event.groupId");
    localObject6 = paramd.c();
    c.g.b.k.a(localObject6, "event.messageId");
    localObject7 = paramd.a();
    c.g.b.k.a(localObject7, "event.sender");
    localObject7 = ((Peer)localObject7).b();
    c.g.b.k.a(localObject7, "event.sender.user");
    localObject7 = ((Peer.d)localObject7).a();
    c.g.b.k.a(localObject7, "event.sender.user.id");
    localObject8 = paramd.e();
    c.g.b.k.a(localObject8, "event.groupInfo");
    localObject8 = ((com.truecaller.api.services.messenger.v1.models.c)localObject8).a();
    str1 = "event.groupInfo.title";
    c.g.b.k.a(localObject8, str1);
    ((m)localObject2).a((String)localObject5, (String)localObject6, (String)localObject7, (String)localObject8);
    if (bool2)
    {
      localObject2 = paramd.b();
      c.g.b.k.a(localObject2, "event.groupId");
      localc.h((String)localObject2);
      localObject2 = paramd.b();
      c.g.b.k.a(localObject2, "event.groupId");
      localObject1 = paramd.a();
      c.g.b.k.a(localObject1, "event.sender");
      localObject1 = ((Peer)localObject1).b();
      c.g.b.k.a(localObject1, "event.sender.user");
      localObject1 = ((Peer.d)localObject1).a();
      localObject3 = "event.sender.user.id";
      c.g.b.k.a(localObject1, (String)localObject3);
      if (localObject4 == null) {
        localObject4 = "";
      }
      localc.c((String)localObject2, (String)localObject1, (String)localObject4);
    }
  }
  
  public final void a(Event.f paramf)
  {
    c.g.b.k.b(paramf, "event");
    Object localObject1 = paramf.b();
    c.g.b.k.a(localObject1, "event.groupId");
    localObject1 = (ImGroupInfo)e((String)localObject1).d();
    String str1 = paramf.b();
    c.g.b.k.a(str1, "event.groupId");
    Object localObject2 = paramf.d();
    c.g.b.k.a(localObject2, "event.groupInfo");
    localObject2 = ((com.truecaller.api.services.messenger.v1.models.c)localObject2).a();
    c.g.b.k.a(localObject2, "event.groupInfo.title");
    Object localObject3 = paramf.d();
    Object localObject4 = "event.groupInfo";
    c.g.b.k.a(localObject3, (String)localObject4);
    localObject3 = ((com.truecaller.api.services.messenger.v1.models.c)localObject3).b();
    b(str1, (String)localObject2, (String)localObject3);
    str1 = null;
    if (localObject1 != null)
    {
      localObject2 = b;
    }
    else
    {
      bool1 = false;
      localObject2 = null;
    }
    localObject3 = paramf.d();
    localObject4 = "event.groupInfo";
    c.g.b.k.a(localObject3, (String)localObject4);
    localObject3 = ((com.truecaller.api.services.messenger.v1.models.c)localObject3).a();
    boolean bool1 = c.g.b.k.a(localObject2, localObject3) ^ true;
    if (bool1)
    {
      localObject2 = f;
      localObject3 = paramf.b();
      c.g.b.k.a(localObject3, "event.groupId");
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      Object localObject5 = paramf.c();
      ((StringBuilder)localObject4).append((String)localObject5);
      ((StringBuilder)localObject4).append("-title");
      localObject4 = ((StringBuilder)localObject4).toString();
      localObject5 = paramf.a();
      c.g.b.k.a(localObject5, "event.sender");
      localObject5 = ((Peer)localObject5).b();
      c.g.b.k.a(localObject5, "event.sender.user");
      localObject5 = ((Peer.d)localObject5).a();
      c.g.b.k.a(localObject5, "event.sender.user.id");
      Object localObject6 = paramf.d();
      c.g.b.k.a(localObject6, "event.groupInfo");
      localObject6 = ((com.truecaller.api.services.messenger.v1.models.c)localObject6).a();
      String str2 = "event.groupInfo.title";
      c.g.b.k.a(localObject6, str2);
      ((m)localObject2).b((String)localObject3, (String)localObject4, (String)localObject5, (String)localObject6);
    }
    if (localObject1 != null) {
      str1 = c;
    }
    localObject1 = paramf.d();
    localObject2 = "event.groupInfo";
    c.g.b.k.a(localObject1, (String)localObject2);
    localObject1 = ((com.truecaller.api.services.messenger.v1.models.c)localObject1).b();
    boolean bool2 = c.g.b.k.a(str1, localObject1) ^ true;
    if (bool2)
    {
      localObject1 = f;
      str1 = paramf.b();
      c.g.b.k.a(str1, "event.groupId");
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject3 = paramf.c();
      ((StringBuilder)localObject2).append((String)localObject3);
      ((StringBuilder)localObject2).append("-avatar");
      localObject2 = ((StringBuilder)localObject2).toString();
      paramf = paramf.a();
      c.g.b.k.a(paramf, "event.sender");
      paramf = paramf.b();
      c.g.b.k.a(paramf, "event.sender.user");
      paramf = paramf.a();
      localObject3 = "event.sender.user.id";
      c.g.b.k.a(paramf, (String)localObject3);
      ((m)localObject1).d(str1, (String)localObject2, paramf);
    }
  }
  
  public final void a(Event.n paramn)
  {
    c localc = this;
    Object localObject1 = paramn;
    c.g.b.k.b(paramn, "event");
    Object localObject2 = paramn.f();
    c.g.b.k.a(localObject2, "event.userInfoMap");
    a((Map)localObject2);
    localObject2 = d.G();
    Object localObject3 = paramn.g();
    Object localObject4 = "event.participantsList";
    c.g.b.k.a(localObject3, (String)localObject4);
    localObject3 = ((Iterable)localObject3).iterator();
    Object localObject5;
    boolean bool2;
    do
    {
      bool1 = ((Iterator)localObject3).hasNext();
      if (!bool1) {
        break;
      }
      localObject4 = ((Iterator)localObject3).next();
      localObject5 = localObject4;
      localObject5 = (com.truecaller.api.services.messenger.v1.models.f)localObject4;
      c.g.b.k.a(localObject5, "it");
      localObject5 = ((com.truecaller.api.services.messenger.v1.models.f)localObject5).a();
      c.g.b.k.a(localObject5, "it.peer");
      localObject5 = ((Peer)localObject5).b();
      localObject6 = "it.peer.user";
      c.g.b.k.a(localObject5, (String)localObject6);
      localObject5 = ((Peer.d)localObject5).a();
      bool2 = c.g.b.k.a(localObject5, localObject2);
    } while (!bool2);
    break label178;
    boolean bool1 = false;
    localObject4 = null;
    label178:
    localObject4 = (com.truecaller.api.services.messenger.v1.models.f)localObject4;
    int i2 = 16;
    int i1 = 10;
    if (localObject4 != null)
    {
      localObject6 = paramn.h();
      localObject7 = "event.existingParticipantsList";
      c.g.b.k.a(localObject6, (String)localObject7);
      localObject6 = (Iterable)localObject6;
      int i3 = c.k.i.c(ag.a(c.a.m.a((Iterable)localObject6, i1)), i2);
      localObject8 = new java/util/LinkedHashMap;
      ((LinkedHashMap)localObject8).<init>(i3);
      localObject8 = (Map)localObject8;
      localObject6 = ((Iterable)localObject6).iterator();
      for (;;)
      {
        boolean bool4 = ((Iterator)localObject6).hasNext();
        if (!bool4) {
          break;
        }
        localObject7 = (com.truecaller.api.services.messenger.v1.models.f)((Iterator)localObject6).next();
        c.g.b.k.a(localObject7, "it");
        localObject9 = ((com.truecaller.api.services.messenger.v1.models.f)localObject7).a();
        localObject10 = "it.peer";
        c.g.b.k.a(localObject9, (String)localObject10);
        localObject9 = com.truecaller.messaging.i.i.a((Peer)localObject9);
        i4 = ((com.truecaller.api.services.messenger.v1.models.f)localObject7).b();
        localObject7 = Integer.valueOf(i4);
        localObject7 = c.t.a(localObject9, localObject7);
        localObject9 = a;
        localObject7 = b;
        ((Map)localObject8).put(localObject9, localObject7);
      }
      localObject6 = paramn.b();
      c.g.b.k.a(localObject6, "event.context");
      localObject6 = ((Peer)localObject6).c();
      c.g.b.k.a(localObject6, "event.context.group");
      localObject6 = ((Peer.b)localObject6).a();
      c.g.b.k.a(localObject6, "event.context.group.id");
      boolean bool5 = localc.i((String)localObject6);
      localObject7 = new com/truecaller/messaging/data/types/ImGroupInfo;
      localObject9 = paramn.b();
      c.g.b.k.a(localObject9, "event.context");
      localObject9 = ((Peer)localObject9).c();
      c.g.b.k.a(localObject9, "event.context.group");
      localObject10 = ((Peer.b)localObject9).a();
      c.g.b.k.a(localObject10, "event.context.group.id");
      localObject9 = paramn.e();
      c.g.b.k.a(localObject9, "event.groupInfo");
      str1 = ((com.truecaller.api.services.messenger.v1.models.c)localObject9).a();
      localObject9 = paramn.e();
      c.g.b.k.a(localObject9, "event.groupInfo");
      String str2 = ((com.truecaller.api.services.messenger.v1.models.c)localObject9).b();
      localObject9 = TimeUnit.SECONDS;
      int i6 = paramn.d();
      long l1 = i6;
      l1 = ((TimeUnit)localObject9).toMillis(l1);
      localObject9 = paramn.a();
      c.g.b.k.a(localObject9, "event.sender");
      localObject9 = ((Peer)localObject9).b();
      c.g.b.k.a(localObject9, "event.sender.user");
      String str3 = ((Peer.d)localObject9).a();
      int i7 = ((com.truecaller.api.services.messenger.v1.models.f)localObject4).b();
      localObject4 = paramn.i();
      c.g.b.k.a(localObject4, "event.permissions");
      ImGroupPermissions localImGroupPermissions = e.a((com.truecaller.api.services.messenger.v1.models.a)localObject4);
      localObject9 = localObject7;
      ((ImGroupInfo)localObject7).<init>((String)localObject10, str1, str2, l1, str3, i7, localImGroupPermissions, 0);
      localc.a((Map)localObject8, (ImGroupInfo)localObject7);
      if (bool5)
      {
        localObject4 = paramn.b();
        c.g.b.k.a(localObject4, "event.context");
        localObject4 = ((Peer)localObject4).c();
        c.g.b.k.a(localObject4, "event.context.group");
        localObject4 = ((Peer.b)localObject4).a();
        c.g.b.k.a(localObject4, "event.context.group.id");
        localc.h((String)localObject4);
        localObject4 = paramn.b();
        c.g.b.k.a(localObject4, "event.context");
        localObject4 = ((Peer)localObject4).c();
        c.g.b.k.a(localObject4, "event.context.group");
        localObject4 = ((Peer.b)localObject4).a();
        c.g.b.k.a(localObject4, "event.context.group.id");
        localObject6 = paramn.a();
        c.g.b.k.a(localObject6, "event.sender");
        localObject6 = ((Peer)localObject6).b();
        c.g.b.k.a(localObject6, "event.sender.user");
        localObject6 = ((Peer.d)localObject6).a();
        localObject7 = "event.sender.user.id";
        c.g.b.k.a(localObject6, (String)localObject7);
        if (localObject2 == null) {
          localObject2 = "";
        }
        localc.c((String)localObject4, (String)localObject6, (String)localObject2);
      }
    }
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject4 = ContentProviderOperation.newAssertQuery(TruecallerContract.r.a());
    int i4 = 1;
    Object localObject8 = new String[i4];
    Object localObject9 = null;
    Object localObject10 = paramn.b();
    c.g.b.k.a(localObject10, "event.context");
    localObject10 = ((Peer)localObject10).c();
    String str1 = "event.context.group";
    c.g.b.k.a(localObject10, str1);
    localObject10 = ((Peer.b)localObject10).a();
    localObject8[0] = localObject10;
    localObject4 = ((ContentProviderOperation.Builder)localObject4).withSelection("im_group_id=?", (String[])localObject8).withExpectedCount(i4).build();
    ((ArrayList)localObject2).add(localObject4);
    localObject4 = paramn.g();
    c.g.b.k.a(localObject4, "event.participantsList");
    localObject4 = (Iterable)localObject4;
    int i5 = ag.a(c.a.m.a((Iterable)localObject4, i1));
    i2 = c.k.i.c(i5, i2);
    Object localObject6 = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject6).<init>(i2);
    localObject6 = (Map)localObject6;
    localObject3 = ((Iterable)localObject4).iterator();
    for (;;)
    {
      bool1 = ((Iterator)localObject3).hasNext();
      if (!bool1) {
        break;
      }
      localObject4 = (com.truecaller.api.services.messenger.v1.models.f)((Iterator)localObject3).next();
      c.g.b.k.a(localObject4, "it");
      localObject7 = ((com.truecaller.api.services.messenger.v1.models.f)localObject4).a();
      localObject8 = "it.peer";
      c.g.b.k.a(localObject7, (String)localObject8);
      localObject7 = com.truecaller.messaging.i.i.a((Peer)localObject7);
      int n = ((com.truecaller.api.services.messenger.v1.models.f)localObject4).b();
      localObject4 = Integer.valueOf(n);
      localObject4 = c.t.a(localObject7, localObject4);
      localObject7 = a;
      localObject4 = b;
      ((Map)localObject6).put(localObject7, localObject4);
    }
    localObject3 = paramn.b();
    c.g.b.k.a(localObject3, "event.context");
    localObject3 = ((Peer)localObject3).c();
    c.g.b.k.a(localObject3, "event.context.group");
    localObject3 = ((Peer.b)localObject3).a();
    c.g.b.k.a(localObject3, "event.context.group.id");
    a((ArrayList)localObject2, (Map)localObject6, (String)localObject3);
    localc.a((ArrayList)localObject2);
    localObject2 = f;
    localObject3 = paramn.b();
    c.g.b.k.a(localObject3, "event.context");
    localObject3 = ((Peer)localObject3).c();
    c.g.b.k.a(localObject3, "event.context.group");
    localObject3 = ((Peer.b)localObject3).a();
    c.g.b.k.a(localObject3, "event.context.group.id");
    localObject4 = paramn.c();
    c.g.b.k.a(localObject4, "event.messageId");
    localObject6 = paramn.a();
    c.g.b.k.a(localObject6, "event.sender");
    localObject6 = ((Peer)localObject6).b();
    c.g.b.k.a(localObject6, "event.sender.user");
    localObject6 = ((Peer.d)localObject6).a();
    c.g.b.k.a(localObject6, "event.sender.user.id");
    localObject1 = paramn.g();
    c.g.b.k.a(localObject1, "event.participantsList");
    localObject1 = (Iterable)localObject1;
    Object localObject7 = new java/util/ArrayList;
    i1 = c.a.m.a((Iterable)localObject1, i1);
    ((ArrayList)localObject7).<init>(i1);
    localObject7 = (Collection)localObject7;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool3 = ((Iterator)localObject1).hasNext();
      if (!bool3) {
        break;
      }
      localObject5 = (com.truecaller.api.services.messenger.v1.models.f)((Iterator)localObject1).next();
      c.g.b.k.a(localObject5, "it");
      localObject5 = ((com.truecaller.api.services.messenger.v1.models.f)localObject5).a();
      c.g.b.k.a(localObject5, "it.peer");
      localObject5 = ((Peer)localObject5).b();
      localObject8 = "it.peer.user";
      c.g.b.k.a(localObject5, (String)localObject8);
      localObject5 = ((Peer.d)localObject5).a();
      ((Collection)localObject7).add(localObject5);
    }
    localObject7 = (List)localObject7;
    ((m)localObject2).a((String)localObject3, (String)localObject4, (String)localObject6, (List)localObject7);
  }
  
  public final void a(Event.p paramp)
  {
    c.g.b.k.b(paramp, "event");
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Object localObject2 = paramp.d();
    c.g.b.k.a(localObject2, "event.participantsList");
    localObject2 = (Iterable)localObject2;
    Object localObject3 = new java/util/ArrayList;
    int n = 10;
    int i1 = c.a.m.a((Iterable)localObject2, n);
    ((ArrayList)localObject3).<init>(i1);
    localObject3 = (Collection)localObject3;
    localObject2 = ((Iterable)localObject2).iterator();
    boolean bool2;
    for (;;)
    {
      bool2 = ((Iterator)localObject2).hasNext();
      if (!bool2) {
        break;
      }
      localObject4 = (Peer)((Iterator)localObject2).next();
      localObject5 = "it";
      c.g.b.k.a(localObject4, (String)localObject5);
      localObject4 = com.truecaller.messaging.i.i.a((Peer)localObject4);
      ((Collection)localObject3).add(localObject4);
    }
    localObject3 = (List)localObject3;
    localObject2 = paramp.b();
    c.g.b.k.a(localObject2, "event.context");
    localObject2 = ((Peer)localObject2).c();
    c.g.b.k.a(localObject2, "event.context.group");
    localObject2 = ((Peer.b)localObject2).a();
    c.g.b.k.a(localObject2, "event.context.group.id");
    a((ArrayList)localObject1, (List)localObject3, (String)localObject2);
    a((ArrayList)localObject1);
    localObject1 = f;
    localObject2 = paramp.b();
    c.g.b.k.a(localObject2, "event.context");
    localObject2 = ((Peer)localObject2).c();
    c.g.b.k.a(localObject2, "event.context.group");
    localObject2 = ((Peer.b)localObject2).a();
    c.g.b.k.a(localObject2, "event.context.group.id");
    localObject3 = paramp.c();
    c.g.b.k.a(localObject3, "event.messageId");
    Object localObject4 = paramp.a();
    c.g.b.k.a(localObject4, "it");
    Object localObject5 = ((Peer)localObject4).a();
    Object localObject6 = Peer.TypeCase.USER;
    int i2;
    if (localObject5 == localObject6)
    {
      i2 = 1;
    }
    else
    {
      i2 = 0;
      localObject5 = null;
    }
    localObject6 = null;
    if (i2 == 0)
    {
      bool2 = false;
      localObject4 = null;
    }
    if (localObject4 != null)
    {
      localObject4 = ((Peer)localObject4).b();
      if (localObject4 != null) {
        localObject6 = ((Peer.d)localObject4).a();
      }
    }
    paramp = paramp.d();
    c.g.b.k.a(paramp, "event.participantsList");
    paramp = (Iterable)paramp;
    localObject4 = new java/util/ArrayList;
    n = c.a.m.a(paramp, n);
    ((ArrayList)localObject4).<init>(n);
    localObject4 = (Collection)localObject4;
    paramp = paramp.iterator();
    for (;;)
    {
      boolean bool1 = paramp.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject7 = (Peer)paramp.next();
      c.g.b.k.a(localObject7, "it");
      localObject7 = ((Peer)localObject7).b();
      localObject5 = "it.user";
      c.g.b.k.a(localObject7, (String)localObject5);
      localObject7 = ((Peer.d)localObject7).a();
      ((Collection)localObject4).add(localObject7);
    }
    localObject4 = (List)localObject4;
    ((m)localObject1).b((String)localObject2, (String)localObject3, (String)localObject6, (List)localObject4);
  }
  
  public final void a(Event.x paramx)
  {
    c.g.b.k.b(paramx, "event");
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Object localObject2 = paramx.b();
    c.g.b.k.a(localObject2, "event.context");
    localObject2 = ((Peer)localObject2).c();
    c.g.b.k.a(localObject2, "event.context.group");
    String str1 = ((Peer.b)localObject2).a();
    c.g.b.k.a(str1, "event.context.group.id");
    localObject2 = paramx.d();
    c.g.b.k.a(localObject2, "event.participant");
    localObject2 = ((Peer)localObject2).b();
    c.g.b.k.a(localObject2, "event.participant.user");
    String str2 = ((Peer.d)localObject2).a();
    c.g.b.k.a(str2, "event.participant.user.id");
    int n = paramx.e();
    localObject2 = paramx.f();
    c.g.b.k.a(localObject2, "event.permissions");
    ImGroupPermissions localImGroupPermissions = e.a((com.truecaller.api.services.messenger.v1.models.a)localObject2);
    localObject2 = this;
    Object localObject3 = localObject1;
    a((ArrayList)localObject1, str1, str2, n, localImGroupPermissions);
    a((ArrayList)localObject1);
    m localm = f;
    localObject1 = paramx.b();
    c.g.b.k.a(localObject1, "event.context");
    localObject1 = ((Peer)localObject1).c();
    c.g.b.k.a(localObject1, "event.context.group");
    String str3 = ((Peer.b)localObject1).a();
    c.g.b.k.a(str3, "event.context.group.id");
    String str4 = paramx.c();
    c.g.b.k.a(str4, "event.messageId");
    int i1 = paramx.e();
    localObject1 = paramx.a();
    c.g.b.k.a(localObject1, "it");
    localObject2 = ((Peer)localObject1).a();
    localObject3 = Peer.TypeCase.USER;
    int i2;
    if (localObject2 == localObject3)
    {
      i2 = 1;
    }
    else
    {
      i2 = 0;
      localObject2 = null;
    }
    localObject3 = null;
    if (i2 == 0) {
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      localObject1 = ((Peer)localObject1).b();
      if (localObject1 != null)
      {
        localObject1 = ((Peer.d)localObject1).a();
        localObject4 = localObject1;
        break label282;
      }
    }
    Object localObject4 = null;
    label282:
    paramx = paramx.d();
    c.g.b.k.a(paramx, "event.participant");
    paramx = paramx.b();
    c.g.b.k.a(paramx, "event.participant.user");
    String str5 = paramx.a();
    c.g.b.k.a(str5, "event.participant.user.id");
    localm.a(str3, str4, i1, (String)localObject4, str5);
  }
  
  /* Error */
  public final w b(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 772
    //   4: invokestatic 37	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: aload_0
    //   8: getfield 69	com/truecaller/messaging/transport/im/a/c:b	Landroid/content/ContentResolver;
    //   11: astore_2
    //   12: iconst_0
    //   13: istore_3
    //   14: aconst_null
    //   15: astore 4
    //   17: aload_1
    //   18: aconst_null
    //   19: invokestatic 777	com/truecaller/content/TruecallerContract$t:a	(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    //   22: astore 5
    //   24: iconst_0
    //   25: istore 6
    //   27: aconst_null
    //   28: astore 7
    //   30: aload_2
    //   31: aload 5
    //   33: aconst_null
    //   34: aconst_null
    //   35: aconst_null
    //   36: aconst_null
    //   37: invokevirtual 282	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   40: astore_1
    //   41: aload_0
    //   42: getfield 71	com/truecaller/messaging/transport/im/a/c:c	Lcom/truecaller/messaging/data/c;
    //   45: astore_2
    //   46: aload_2
    //   47: aload_1
    //   48: invokeinterface 784 2 0
    //   53: astore_1
    //   54: aload_1
    //   55: ifnull +286 -> 341
    //   58: aload_1
    //   59: checkcast 286	android/database/Cursor
    //   62: astore_1
    //   63: aload_1
    //   64: astore_2
    //   65: aload_1
    //   66: checkcast 284	java/io/Closeable
    //   69: astore_2
    //   70: new 114	java/util/ArrayList
    //   73: astore 5
    //   75: aload 5
    //   77: invokespecial 115	java/util/ArrayList:<init>	()V
    //   80: aload 5
    //   82: checkcast 189	java/util/Collection
    //   85: astore 5
    //   87: aload_1
    //   88: invokeinterface 289 1 0
    //   93: istore 6
    //   95: iload 6
    //   97: ifeq +34 -> 131
    //   100: aload_1
    //   101: astore 7
    //   103: aload_1
    //   104: checkcast 1197	com/truecaller/messaging/transport/im/a/g
    //   107: astore 7
    //   109: aload 7
    //   111: invokeinterface 1200 1 0
    //   116: astore 7
    //   118: aload 5
    //   120: aload 7
    //   122: invokeinterface 207 2 0
    //   127: pop
    //   128: goto -41 -> 87
    //   131: aload 5
    //   133: checkcast 168	java/util/List
    //   136: astore 5
    //   138: aload_2
    //   139: aconst_null
    //   140: invokestatic 300	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   143: aload 5
    //   145: checkcast 180	java/lang/Iterable
    //   148: astore 5
    //   150: new 114	java/util/ArrayList
    //   153: astore_1
    //   154: aload 5
    //   156: bipush 10
    //   158: invokestatic 186	c/a/m:a	(Ljava/lang/Iterable;I)I
    //   161: istore_3
    //   162: aload_1
    //   163: iload_3
    //   164: invokespecial 187	java/util/ArrayList:<init>	(I)V
    //   167: aload_1
    //   168: checkcast 189	java/util/Collection
    //   171: astore_1
    //   172: aload 5
    //   174: invokeinterface 193 1 0
    //   179: astore 4
    //   181: aload 4
    //   183: invokeinterface 198 1 0
    //   188: istore 8
    //   190: iload 8
    //   192: ifeq +119 -> 311
    //   195: aload 4
    //   197: invokeinterface 202 1 0
    //   202: checkcast 1202	com/truecaller/messaging/transport/im/a/f
    //   205: astore_2
    //   206: new 93	com/truecaller/messaging/data/types/Participant$a
    //   209: astore 5
    //   211: iconst_3
    //   212: istore 6
    //   214: aload 5
    //   216: iload 6
    //   218: invokespecial 97	com/truecaller/messaging/data/types/Participant$a:<init>	(I)V
    //   221: aload_2
    //   222: getfield 1204	com/truecaller/messaging/transport/im/a/f:a	Ljava/lang/String;
    //   225: astore 7
    //   227: aload 5
    //   229: aload 7
    //   231: invokevirtual 105	com/truecaller/messaging/data/types/Participant$a:b	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Participant$a;
    //   234: astore 5
    //   236: aload_2
    //   237: getfield 1206	com/truecaller/messaging/transport/im/a/f:e	Ljava/lang/String;
    //   240: astore 7
    //   242: aload 5
    //   244: aload 7
    //   246: invokevirtual 1208	com/truecaller/messaging/data/types/Participant$a:f	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Participant$a;
    //   249: astore 5
    //   251: aload_2
    //   252: getfield 1211	com/truecaller/messaging/transport/im/a/f:g	J
    //   255: lstore 9
    //   257: aload 5
    //   259: lload 9
    //   261: invokevirtual 1214	com/truecaller/messaging/data/types/Participant$a:c	(J)Lcom/truecaller/messaging/data/types/Participant$a;
    //   264: astore 5
    //   266: aload_2
    //   267: getfield 1215	com/truecaller/messaging/transport/im/a/f:f	Ljava/lang/String;
    //   270: astore_2
    //   271: aload_2
    //   272: ifnonnull +7 -> 279
    //   275: ldc_w 1110
    //   278: astore_2
    //   279: aload 5
    //   281: aload_2
    //   282: invokevirtual 1217	com/truecaller/messaging/data/types/Participant$a:g	(Ljava/lang/String;)Lcom/truecaller/messaging/data/types/Participant$a;
    //   285: invokevirtual 108	com/truecaller/messaging/data/types/Participant$a:a	()Lcom/truecaller/messaging/data/types/Participant;
    //   288: astore_2
    //   289: ldc_w 1219
    //   292: astore 5
    //   294: aload_2
    //   295: aload 5
    //   297: invokestatic 112	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   300: aload_1
    //   301: aload_2
    //   302: invokeinterface 207 2 0
    //   307: pop
    //   308: goto -127 -> 181
    //   311: aload_1
    //   312: astore 4
    //   314: aload_1
    //   315: checkcast 168	java/util/List
    //   318: astore 4
    //   320: goto +21 -> 341
    //   323: astore_1
    //   324: goto +9 -> 333
    //   327: astore_1
    //   328: aload_1
    //   329: astore 4
    //   331: aload_1
    //   332: athrow
    //   333: aload_2
    //   334: aload 4
    //   336: invokestatic 300	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   339: aload_1
    //   340: athrow
    //   341: aload 4
    //   343: invokestatic 766	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   346: astore_1
    //   347: aload_1
    //   348: ldc_w 786
    //   351: invokestatic 112	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   354: aload_1
    //   355: ldc_w 1221
    //   358: invokestatic 112	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   361: aload_1
    //   362: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	363	0	this	c
    //   0	363	1	paramString	String
    //   11	323	2	localObject1	Object
    //   13	151	3	n	int
    //   15	327	4	localObject2	Object
    //   22	274	5	localObject3	Object
    //   25	71	6	bool1	boolean
    //   212	5	6	i1	int
    //   28	217	7	localObject4	Object
    //   188	3	8	bool2	boolean
    //   255	5	9	l1	long
    // Exception table:
    //   from	to	target	type
    //   331	333	323	finally
    //   70	73	327	finally
    //   75	80	327	finally
    //   80	85	327	finally
    //   87	93	327	finally
    //   103	107	327	finally
    //   109	116	327	finally
    //   120	128	327	finally
    //   131	136	327	finally
  }
  
  public final w b(String paramString, boolean paramBoolean)
  {
    c.g.b.k.b(paramString, "groupId");
    c(paramString, paramBoolean);
    f.a(paramString);
    paramString = w.b(Boolean.TRUE);
    c.g.b.k.a(paramString, "Promise.wrap(true)");
    return paramString;
  }
  
  /* Error */
  public final void b()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 69	com/truecaller/messaging/transport/im/a/c:b	Landroid/content/ContentResolver;
    //   4: astore_1
    //   5: invokestatic 120	com/truecaller/content/TruecallerContract$r:a	()Landroid/net/Uri;
    //   8: astore_2
    //   9: ldc_w 1225
    //   12: astore_3
    //   13: aconst_null
    //   14: astore 4
    //   16: iconst_0
    //   17: istore 5
    //   19: aconst_null
    //   20: astore 6
    //   22: aconst_null
    //   23: astore 7
    //   25: aload_1
    //   26: aload_2
    //   27: aconst_null
    //   28: aload_3
    //   29: aconst_null
    //   30: aconst_null
    //   31: invokevirtual 282	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   34: astore_1
    //   35: aload_1
    //   36: ifnull +204 -> 240
    //   39: aload_0
    //   40: getfield 71	com/truecaller/messaging/transport/im/a/c:c	Lcom/truecaller/messaging/data/c;
    //   43: astore_2
    //   44: aload_2
    //   45: aload_1
    //   46: invokeinterface 1228 2 0
    //   51: astore_1
    //   52: aload_1
    //   53: ifnull +187 -> 240
    //   56: aload_1
    //   57: checkcast 286	android/database/Cursor
    //   60: astore_1
    //   61: aload_1
    //   62: astore_2
    //   63: aload_1
    //   64: checkcast 284	java/io/Closeable
    //   67: astore_2
    //   68: aconst_null
    //   69: astore 4
    //   71: new 114	java/util/ArrayList
    //   74: astore_3
    //   75: aload_3
    //   76: invokespecial 115	java/util/ArrayList:<init>	()V
    //   79: aload_3
    //   80: checkcast 189	java/util/Collection
    //   83: astore_3
    //   84: aload_1
    //   85: invokeinterface 289 1 0
    //   90: istore 5
    //   92: iload 5
    //   94: ifeq +122 -> 216
    //   97: aload_1
    //   98: astore 6
    //   100: aload_1
    //   101: checkcast 1230	com/truecaller/messaging/data/a/h
    //   104: astore 6
    //   106: aload 6
    //   108: invokeinterface 1233 1 0
    //   113: astore 6
    //   115: aload 6
    //   117: getfield 1234	com/truecaller/messaging/data/types/ImGroupInfo:e	Ljava/lang/String;
    //   120: astore 7
    //   122: aload 7
    //   124: ifnull +75 -> 199
    //   127: aload_0
    //   128: getfield 77	com/truecaller/messaging/transport/im/a/c:f	Lcom/truecaller/messaging/transport/im/a/m;
    //   131: astore 7
    //   133: aload 6
    //   135: getfield 102	com/truecaller/messaging/data/types/ImGroupInfo:a	Ljava/lang/String;
    //   138: astore 8
    //   140: new 238	java/lang/StringBuilder
    //   143: astore 9
    //   145: ldc_w 1236
    //   148: astore 10
    //   150: aload 9
    //   152: aload 10
    //   154: invokespecial 243	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   157: aload 6
    //   159: getfield 102	com/truecaller/messaging/data/types/ImGroupInfo:a	Ljava/lang/String;
    //   162: astore 10
    //   164: aload 9
    //   166: aload 10
    //   168: invokevirtual 258	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   171: pop
    //   172: aload 9
    //   174: invokevirtual 264	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   177: astore 9
    //   179: aload 6
    //   181: getfield 1234	com/truecaller/messaging/data/types/ImGroupInfo:e	Ljava/lang/String;
    //   184: astore 6
    //   186: aload 7
    //   188: aload 8
    //   190: aload 9
    //   192: aload 6
    //   194: invokeinterface 1238 4 0
    //   199: getstatic 1243	c/x:a	Lc/x;
    //   202: astore 6
    //   204: aload_3
    //   205: aload 6
    //   207: invokeinterface 207 2 0
    //   212: pop
    //   213: goto -129 -> 84
    //   216: aload_2
    //   217: aconst_null
    //   218: invokestatic 300	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   221: return
    //   222: astore_1
    //   223: goto +9 -> 232
    //   226: astore_1
    //   227: aload_1
    //   228: astore 4
    //   230: aload_1
    //   231: athrow
    //   232: aload_2
    //   233: aload 4
    //   235: invokestatic 300	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   238: aload_1
    //   239: athrow
    //   240: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	241	0	this	c
    //   4	97	1	localObject1	Object
    //   222	1	1	localObject2	Object
    //   226	13	1	localObject3	Object
    //   8	225	2	localObject4	Object
    //   12	193	3	localObject5	Object
    //   14	220	4	localObject6	Object
    //   17	76	5	bool	boolean
    //   20	186	6	localObject7	Object
    //   23	164	7	localObject8	Object
    //   138	51	8	str1	String
    //   143	48	9	localObject9	Object
    //   148	19	10	str2	String
    // Exception table:
    //   from	to	target	type
    //   230	232	222	finally
    //   71	74	226	finally
    //   75	79	226	finally
    //   79	83	226	finally
    //   84	90	226	finally
    //   100	104	226	finally
    //   106	113	226	finally
    //   115	120	226	finally
    //   127	131	226	finally
    //   133	138	226	finally
    //   140	143	226	finally
    //   152	157	226	finally
    //   157	162	226	finally
    //   166	172	226	finally
    //   172	177	226	finally
    //   179	184	226	finally
    //   192	199	226	finally
    //   199	202	226	finally
    //   205	213	226	finally
  }
  
  public final w c(String paramString)
  {
    c.g.b.k.b(paramString, "groupId");
    ContentResolver localContentResolver = b;
    Uri localUri = TruecallerContract.s.a();
    c.g.b.k.a(localUri, "ImGroupParticipantsTable.getContentUri()");
    String str1 = "COUNT()";
    String str2 = "im_group_id = ?";
    int n = 1;
    String[] arrayOfString = new String[n];
    arrayOfString[0] = paramString;
    paramString = com.truecaller.utils.extensions.h.a(localContentResolver, localUri, str1, str2, arrayOfString);
    if (paramString == null) {
      paramString = Integer.valueOf(0);
    }
    paramString = w.b(paramString);
    c.g.b.k.a(paramString, "Promise.wrap(it ?: 0)");
    c.g.b.k.a(paramString, "contentResolver.queryInt…{ Promise.wrap(it ?: 0) }");
    return paramString;
  }
  
  public final w d(String paramString)
  {
    c.g.b.k.b(paramString, "groupId");
    Object localObject = d.G();
    if (localObject == null)
    {
      paramString = w.b(Boolean.FALSE);
      c.g.b.k.a(paramString, "Promise.wrap(false)");
      return paramString;
    }
    int n = 8;
    localObject = b(paramString, (String)localObject, n);
    if (localObject == null)
    {
      paramString = w.b(Boolean.FALSE);
      c.g.b.k.a(paramString, "Promise.wrap(false)");
      return paramString;
    }
    m localm = f;
    localObject = ((z.d)localObject).a();
    c.g.b.k.a(localObject, "response.messageId");
    localm.b(paramString, (String)localObject);
    paramString = w.b(Boolean.TRUE);
    c.g.b.k.a(paramString, "Promise.wrap(true)");
    return paramString;
  }
  
  /* Error */
  public final w e(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 772
    //   4: invokestatic 37	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   7: aload_0
    //   8: getfield 69	com/truecaller/messaging/transport/im/a/c:b	Landroid/content/ContentResolver;
    //   11: astore_2
    //   12: invokestatic 120	com/truecaller/content/TruecallerContract$r:a	()Landroid/net/Uri;
    //   15: astore_3
    //   16: ldc -103
    //   18: astore 4
    //   20: iconst_1
    //   21: anewarray 156	java/lang/String
    //   24: astore 5
    //   26: aload 5
    //   28: iconst_0
    //   29: aload_1
    //   30: aastore
    //   31: aload_2
    //   32: aload_3
    //   33: aconst_null
    //   34: aload 4
    //   36: aload 5
    //   38: aconst_null
    //   39: invokevirtual 282	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   42: astore_1
    //   43: aconst_null
    //   44: astore 6
    //   46: aload_1
    //   47: ifnull +85 -> 132
    //   50: aload_1
    //   51: checkcast 284	java/io/Closeable
    //   54: astore_1
    //   55: aload_1
    //   56: astore_2
    //   57: aload_1
    //   58: checkcast 286	android/database/Cursor
    //   61: astore_2
    //   62: aload_0
    //   63: getfield 71	com/truecaller/messaging/transport/im/a/c:c	Lcom/truecaller/messaging/data/c;
    //   66: astore_3
    //   67: aload_3
    //   68: aload_2
    //   69: invokeinterface 1228 2 0
    //   74: astore_2
    //   75: aload_2
    //   76: ifnull +26 -> 102
    //   79: aload_2
    //   80: invokeinterface 1255 1 0
    //   85: istore 7
    //   87: iload 7
    //   89: ifeq +13 -> 102
    //   92: aload_2
    //   93: invokeinterface 1233 1 0
    //   98: astore_2
    //   99: goto +5 -> 104
    //   102: aconst_null
    //   103: astore_2
    //   104: aload_1
    //   105: aconst_null
    //   106: invokestatic 300	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   109: aload_2
    //   110: astore 6
    //   112: goto +20 -> 132
    //   115: astore_2
    //   116: goto +8 -> 124
    //   119: astore 6
    //   121: aload 6
    //   123: athrow
    //   124: aload_1
    //   125: aload 6
    //   127: invokestatic 300	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   130: aload_2
    //   131: athrow
    //   132: aload 6
    //   134: invokestatic 766	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   137: astore_1
    //   138: aload_1
    //   139: ldc_w 786
    //   142: invokestatic 112	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   145: aload_1
    //   146: ldc_w 788
    //   149: invokestatic 112	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   152: aload_1
    //   153: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	154	0	this	c
    //   0	154	1	paramString	String
    //   11	99	2	localObject1	Object
    //   115	16	2	localObject2	Object
    //   15	53	3	localObject3	Object
    //   18	17	4	str	String
    //   24	13	5	arrayOfString	String[]
    //   44	67	6	localObject4	Object
    //   119	14	6	localThrowable	Throwable
    //   85	3	7	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   121	124	115	finally
    //   57	61	119	finally
    //   62	66	119	finally
    //   68	74	119	finally
    //   79	85	119	finally
    //   92	98	119	finally
  }
  
  public final void f(String paramString)
  {
    String str = "groupId";
    c.g.b.k.b(paramString, str);
    paramString = g(paramString);
    if (paramString != null)
    {
      long l1 = ((Number)paramString).longValue();
      ((com.truecaller.messaging.data.t)j.a()).a(l1, 1, 0);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */