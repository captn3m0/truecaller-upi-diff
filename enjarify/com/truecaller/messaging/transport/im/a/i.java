package com.truecaller.messaging.transport.im.a;

import com.truecaller.api.services.messenger.v1.models.GroupAction;
import com.truecaller.messaging.data.types.ImGroupPermissions;

public abstract interface i
{
  public abstract String a(int paramInt);
  
  public abstract String a(String paramString);
  
  public abstract boolean a(ImGroupPermissions paramImGroupPermissions, int paramInt1, int paramInt2);
  
  public abstract boolean a(ImGroupPermissions paramImGroupPermissions, GroupAction paramGroupAction);
  
  public abstract boolean a(ImGroupPermissions paramImGroupPermissions, GroupAction paramGroupAction, f paramf);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */