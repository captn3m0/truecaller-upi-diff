package com.truecaller.messaging.transport.im.a;

import android.content.ContentResolver;
import c.g.b.k;
import com.truecaller.androidactors.f;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant.a;
import com.truecaller.messaging.data.types.TextEntity;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.status.StatusTransportInfo;
import com.truecaller.utils.a;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public final class n
  implements m
{
  private final f a;
  private final ContentResolver b;
  private final a c;
  private final e d;
  private final com.truecaller.utils.n e;
  private final i f;
  private final h g;
  
  public n(f paramf, ContentResolver paramContentResolver, a parama, e parame, com.truecaller.utils.n paramn, i parami, h paramh)
  {
    a = paramf;
    b = paramContentResolver;
    c = parama;
    d = parame;
    e = paramn;
    f = parami;
    g = paramh;
  }
  
  private final void a(String paramString1, String paramString2, int paramInt1, int paramInt2, Object... paramVarArgs)
  {
    com.truecaller.utils.n localn = e;
    int i = paramVarArgs.length;
    paramVarArgs = Arrays.copyOf(paramVarArgs, i);
    String str = localn.a(paramInt1, paramInt2, paramVarArgs);
    k.a(str, "resourceProvider.getQuan…s, quantity, *formatArgs)");
    a(this, paramString1, paramString2, str);
  }
  
  private final void a(String paramString1, String paramString2, int paramInt, Object... paramVarArgs)
  {
    com.truecaller.utils.n localn = e;
    int i = paramVarArgs.length;
    paramVarArgs = Arrays.copyOf(paramVarArgs, i);
    String str = localn.a(paramInt, paramVarArgs);
    k.a(str, "resourceProvider.getString(res, *formatArgs)");
    a(this, paramString1, paramString2, str);
  }
  
  private final void a(String paramString1, String paramString2, String paramString3, boolean paramBoolean1, boolean paramBoolean2)
  {
    Object localObject1 = d.i();
    boolean bool = ((b)localObject1).a();
    if (!bool) {
      return;
    }
    if (paramBoolean2)
    {
      paramBoolean2 = c(paramString1) & 0x2;
      if (paramBoolean2) {
        return;
      }
    }
    Object localObject2 = new com/truecaller/messaging/data/types/Participant$a;
    ((Participant.a)localObject2).<init>(4);
    paramString1 = ((Participant.a)localObject2).b(paramString1).a();
    k.a(paramString1, "Participant.Builder(Part…pId)\n            .build()");
    localObject2 = new com/truecaller/messaging/data/types/Message$a;
    ((Message.a)localObject2).<init>();
    paramString1 = ((Message.a)localObject2).a(paramString1);
    localObject1 = new com/truecaller/messaging/transport/status/StatusTransportInfo;
    ((StatusTransportInfo)localObject1).<init>(-1, paramString2);
    localObject1 = (TransportInfo)localObject1;
    paramString1 = paramString1.a(6, (TransportInfo)localObject1);
    paramString2 = TextEntity.a("text/plain", paramString3);
    paramString1 = paramString1.a(paramString2).b(paramBoolean1).b();
    k.a(paramString1, "Message.Builder()\n      …ead)\n            .build()");
    ((t)a.a()).a(paramString1);
  }
  
  /* Error */
  private final String b(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 47	com/truecaller/messaging/transport/im/a/n:b	Landroid/content/ContentResolver;
    //   4: astore_2
    //   5: aload_1
    //   6: invokestatic 162	com/truecaller/content/TruecallerContract$t:a	(Ljava/lang/String;)Landroid/net/Uri;
    //   9: astore_3
    //   10: iconst_1
    //   11: anewarray 166	java/lang/String
    //   14: dup
    //   15: iconst_0
    //   16: ldc -92
    //   18: aastore
    //   19: astore 4
    //   21: iconst_0
    //   22: istore 5
    //   24: aload_2
    //   25: aload_3
    //   26: aload 4
    //   28: aconst_null
    //   29: aconst_null
    //   30: aconst_null
    //   31: invokevirtual 172	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   34: astore_2
    //   35: aconst_null
    //   36: astore_3
    //   37: aload_2
    //   38: ifnull +77 -> 115
    //   41: aload_2
    //   42: checkcast 174	java/io/Closeable
    //   45: astore_2
    //   46: aload_2
    //   47: astore 4
    //   49: aload_2
    //   50: checkcast 176	android/database/Cursor
    //   53: astore 4
    //   55: aload 4
    //   57: invokeinterface 179 1 0
    //   62: istore 5
    //   64: iload 5
    //   66: ifeq +19 -> 85
    //   69: iconst_0
    //   70: istore 5
    //   72: aload 4
    //   74: iconst_0
    //   75: invokeinterface 183 2 0
    //   80: astore 4
    //   82: goto +6 -> 88
    //   85: aconst_null
    //   86: astore 4
    //   88: aload_2
    //   89: aconst_null
    //   90: invokestatic 188	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   93: aload 4
    //   95: astore_3
    //   96: goto +19 -> 115
    //   99: astore_1
    //   100: goto +8 -> 108
    //   103: astore_1
    //   104: aload_1
    //   105: astore_3
    //   106: aload_1
    //   107: athrow
    //   108: aload_2
    //   109: aload_3
    //   110: invokestatic 188	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   113: aload_1
    //   114: athrow
    //   115: aload_3
    //   116: ifnonnull +16 -> 132
    //   119: aload_0
    //   120: getfield 55	com/truecaller/messaging/transport/im/a/n:f	Lcom/truecaller/messaging/transport/im/a/i;
    //   123: astore_2
    //   124: aload_2
    //   125: aload_1
    //   126: invokeinterface 193 2 0
    //   131: astore_3
    //   132: aload_3
    //   133: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	134	0	this	n
    //   0	134	1	paramString	String
    //   4	121	2	localObject1	Object
    //   9	124	3	localObject2	Object
    //   19	75	4	localObject3	Object
    //   22	49	5	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   106	108	99	finally
    //   49	53	103	finally
    //   55	62	103	finally
    //   74	80	103	finally
  }
  
  private final void b(String paramString1, String paramString2, int paramInt, Object... paramVarArgs)
  {
    com.truecaller.utils.n localn = e;
    int i = paramVarArgs.length;
    paramVarArgs = Arrays.copyOf(paramVarArgs, i);
    String str = localn.a(paramInt, paramVarArgs);
    k.a(str, "resourceProvider.getString(res, *formatArgs)");
    a(paramString1, paramString2, str, false, false);
  }
  
  /* Error */
  private final int c(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 47	com/truecaller/messaging/transport/im/a/n:b	Landroid/content/ContentResolver;
    //   4: astore_2
    //   5: invokestatic 198	com/truecaller/content/TruecallerContract$r:a	()Landroid/net/Uri;
    //   8: astore_3
    //   9: iconst_1
    //   10: anewarray 166	java/lang/String
    //   13: dup
    //   14: iconst_0
    //   15: ldc -56
    //   17: aastore
    //   18: astore 4
    //   20: ldc -54
    //   22: astore 5
    //   24: iconst_1
    //   25: istore 6
    //   27: iload 6
    //   29: anewarray 166	java/lang/String
    //   32: astore 7
    //   34: aload 7
    //   36: iconst_0
    //   37: aload_1
    //   38: aastore
    //   39: aload_2
    //   40: aload_3
    //   41: aload 4
    //   43: aload 5
    //   45: aload 7
    //   47: aconst_null
    //   48: invokevirtual 172	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   51: astore_1
    //   52: aload_1
    //   53: ifnull +81 -> 134
    //   56: aload_1
    //   57: checkcast 174	java/io/Closeable
    //   60: astore_1
    //   61: aconst_null
    //   62: astore_2
    //   63: aload_1
    //   64: astore_3
    //   65: aload_1
    //   66: checkcast 176	android/database/Cursor
    //   69: astore_3
    //   70: aload_3
    //   71: invokeinterface 179 1 0
    //   76: istore 8
    //   78: iload 8
    //   80: ifeq +21 -> 101
    //   83: aload_3
    //   84: iconst_0
    //   85: invokeinterface 206 2 0
    //   90: istore 9
    //   92: iload 9
    //   94: invokestatic 212	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   97: astore_3
    //   98: goto +8 -> 106
    //   101: iconst_0
    //   102: istore 9
    //   104: aconst_null
    //   105: astore_3
    //   106: aload_1
    //   107: aconst_null
    //   108: invokestatic 188	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   111: aload_3
    //   112: ifnull +22 -> 134
    //   115: aload_3
    //   116: invokevirtual 216	java/lang/Integer:intValue	()I
    //   119: ireturn
    //   120: astore_3
    //   121: goto +6 -> 127
    //   124: astore_2
    //   125: aload_2
    //   126: athrow
    //   127: aload_1
    //   128: aload_2
    //   129: invokestatic 188	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   132: aload_3
    //   133: athrow
    //   134: iconst_0
    //   135: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	136	0	this	n
    //   0	136	1	paramString	String
    //   4	59	2	localContentResolver	ContentResolver
    //   124	5	2	localThrowable	Throwable
    //   8	108	3	localObject1	Object
    //   120	13	3	localObject2	Object
    //   18	24	4	arrayOfString1	String[]
    //   22	22	5	str	String
    //   25	3	6	i	int
    //   32	14	7	arrayOfString2	String[]
    //   76	3	8	bool	boolean
    //   90	13	9	j	int
    // Exception table:
    //   from	to	target	type
    //   125	127	120	finally
    //   65	69	124	finally
    //   70	76	124	finally
    //   84	90	124	finally
    //   92	97	124	finally
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "groupId");
    String str = UUID.randomUUID().toString();
    k.a(str, "UUID.randomUUID().toString()");
    Object[] arrayOfObject = new Object[0];
    a(paramString, str, 2131887177, arrayOfObject);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    Object[] arrayOfObject = new Object[0];
    a(paramString1, paramString2, 2131887189, arrayOfObject);
  }
  
  public final void a(String paramString1, String paramString2, int paramInt, String paramString3)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString3, "imPeerId");
    i locali = f;
    String str = locali.a(paramInt);
    if (str == null) {
      return;
    }
    Object[] arrayOfObject = new Object[2];
    paramString3 = b(paramString3);
    arrayOfObject[0] = paramString3;
    arrayOfObject[1] = str;
    a(paramString1, paramString2, 2131887187, arrayOfObject);
  }
  
  public final void a(String paramString1, String paramString2, int paramInt, String paramString3, String paramString4)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString4, "imPeerId");
    String str = f.a(paramInt);
    if (str == null) {
      return;
    }
    int i = 2;
    int j = 1;
    if (paramString3 == null)
    {
      paramString3 = new Object[i];
      paramString4 = b(paramString4);
      paramString3[0] = paramString4;
      paramString3[j] = str;
      a(paramString1, paramString2, 2131887185, paramString3);
      return;
    }
    paramInt &= 0x8;
    if (paramInt != 0)
    {
      paramInt = k.a(paramString3, paramString4);
      if (paramInt != 0)
      {
        paramString3 = new Object[j];
        paramString4 = b(paramString4);
        paramString3[0] = paramString4;
        a(paramString1, paramString2, 2131887181, paramString3);
        return;
      }
    }
    Object[] arrayOfObject = new Object[3];
    paramString3 = b(paramString3);
    arrayOfObject[0] = paramString3;
    paramString3 = b(paramString4);
    arrayOfObject[j] = paramString3;
    arrayOfObject[i] = str;
    a(paramString1, paramString2, 2131887186, arrayOfObject);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString3, "senderPeerId");
    Object[] arrayOfObject = new Object[1];
    paramString3 = b(paramString3);
    arrayOfObject[0] = paramString3;
    b(paramString1, paramString2, 2131887180, arrayOfObject);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString3, "senderPeerId");
    k.b(paramString4, "title");
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = paramString4;
    paramString3 = b(paramString3);
    arrayOfObject[1] = paramString3;
    b(paramString1, paramString2, 2131887173, arrayOfObject);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, List paramList)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString3, "senderPeerId");
    k.b(paramList, "imPeerIds");
    Object localObject = paramList;
    localObject = (Iterable)paramList;
    String str = g.G();
    boolean bool = c.a.m.a((Iterable)localObject, str);
    str = null;
    int j = 1;
    if (bool)
    {
      localObject = new Object[j];
      paramString3 = b(paramString3);
      localObject[0] = paramString3;
      b(paramString1, paramString2, 2131887180, (Object[])localObject);
      return;
    }
    int i = paramList.size();
    int k = 2;
    if (i == j)
    {
      Object[] arrayOfObject1 = new Object[k];
      paramList = (String)c.a.m.d(paramList);
      paramList = b(paramList);
      arrayOfObject1[0] = paramList;
      paramString3 = b(paramString3);
      arrayOfObject1[j] = paramString3;
      a(paramString1, paramString2, 2131887178, arrayOfObject1);
      return;
    }
    i = paramList.size();
    if (i > j)
    {
      int m = paramList.size() + -1;
      int n = 2131755034;
      i = 3;
      Object[] arrayOfObject2 = new Object[i];
      paramList = (String)c.a.m.d(paramList);
      paramList = b(paramList);
      arrayOfObject2[0] = paramList;
      paramList = Integer.valueOf(m);
      arrayOfObject2[j] = paramList;
      paramString3 = b(paramString3);
      arrayOfObject2[k] = paramString3;
      a(paramString1, paramString2, n, m, arrayOfObject2);
    }
  }
  
  public final void a(String paramString1, String paramString2, List paramList)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    String str = "imPeerIds";
    k.b(paramList, str);
    int i = paramList.size();
    int j = 1;
    if (i == j)
    {
      Object[] arrayOfObject1 = new Object[j];
      paramList = (String)c.a.m.d(paramList);
      paramList = b(paramList);
      arrayOfObject1[0] = paramList;
      a(paramString1, paramString2, 2131887179, arrayOfObject1);
      return;
    }
    i = paramList.size();
    if (i > j)
    {
      int k = paramList.size() + -1;
      int m = 2131755035;
      i = 2;
      Object[] arrayOfObject2 = new Object[i];
      paramList = (String)c.a.m.d(paramList);
      paramList = b(paramList);
      arrayOfObject2[0] = paramList;
      paramList = Integer.valueOf(k);
      arrayOfObject2[j] = paramList;
      a(paramString1, paramString2, m, k, arrayOfObject2);
    }
  }
  
  public final void b(String paramString1, String paramString2)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    Object[] arrayOfObject = new Object[0];
    a(paramString1, paramString2, 2131887188, arrayOfObject);
  }
  
  public final void b(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString3, "title");
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramString3;
    a(paramString1, paramString2, 2131887174, arrayOfObject);
  }
  
  public final void b(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString3, "senderPeerId");
    k.b(paramString4, "title");
    Object[] arrayOfObject = new Object[2];
    paramString3 = b(paramString3);
    arrayOfObject[0] = paramString3;
    arrayOfObject[1] = paramString4;
    a(paramString1, paramString2, 2131887175, arrayOfObject);
  }
  
  public final void b(String paramString1, String paramString2, String paramString3, List paramList)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    String str1 = "imPeerIds";
    k.b(paramList, str1);
    paramList = ((Iterable)paramList).iterator();
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      str1 = (String)paramList.next();
      Object localObject = g.G();
      boolean bool2 = k.a(str1, localObject);
      int j = 1;
      char c1 = '-';
      Object[] arrayOfObject1;
      if (bool2)
      {
        int i;
        if (paramString3 == null)
        {
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          ((StringBuilder)localObject).append(paramString2);
          ((StringBuilder)localObject).append(c1);
          ((StringBuilder)localObject).append(str1);
          str1 = ((StringBuilder)localObject).toString();
          i = 2131887189;
          arrayOfObject1 = new Object[0];
          a(paramString1, str1, i, arrayOfObject1);
        }
        else
        {
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          ((StringBuilder)localObject).append(paramString2);
          ((StringBuilder)localObject).append(c1);
          ((StringBuilder)localObject).append(str1);
          str1 = ((StringBuilder)localObject).toString();
          i = 2131887190;
          arrayOfObject1 = new Object[j];
          String str2 = b(paramString3);
          arrayOfObject1[0] = str2;
          a(paramString1, str1, i, arrayOfObject1);
        }
      }
      else
      {
        int k;
        if (paramString3 != null)
        {
          boolean bool3 = k.a(paramString3, str1);
          if (!bool3)
          {
            localObject = new java/lang/StringBuilder;
            ((StringBuilder)localObject).<init>();
            ((StringBuilder)localObject).append(paramString2);
            ((StringBuilder)localObject).append(c1);
            ((StringBuilder)localObject).append(str1);
            localObject = ((StringBuilder)localObject).toString();
            k = 2131887183;
            int n = 2;
            Object[] arrayOfObject2 = new Object[n];
            str1 = b(str1);
            arrayOfObject2[0] = str1;
            str1 = b(paramString3);
            arrayOfObject2[j] = str1;
            a(paramString1, (String)localObject, k, arrayOfObject2);
            continue;
          }
        }
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        ((StringBuilder)localObject).append(paramString2);
        ((StringBuilder)localObject).append(k);
        ((StringBuilder)localObject).append(str1);
        localObject = ((StringBuilder)localObject).toString();
        int m = 2131887182;
        arrayOfObject1 = new Object[j];
        str1 = b(str1);
        arrayOfObject1[0] = str1;
        a(paramString1, (String)localObject, m, arrayOfObject1);
      }
    }
  }
  
  public final void c(String paramString1, String paramString2)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    Object[] arrayOfObject = new Object[0];
    a(paramString1, paramString2, 2131887172, arrayOfObject);
  }
  
  public final void c(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString3, "imPeerId");
    Object[] arrayOfObject = new Object[1];
    paramString3 = b(paramString3);
    arrayOfObject[0] = paramString3;
    a(paramString1, paramString2, 2131887184, arrayOfObject);
  }
  
  public final void d(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString3, "senderPeerId");
    Object[] arrayOfObject = new Object[1];
    paramString3 = b(paramString3);
    arrayOfObject[0] = paramString3;
    a(paramString1, paramString2, 2131887171, arrayOfObject);
  }
  
  public final void e(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "groupId");
    k.b(paramString2, "rawId");
    k.b(paramString3, "title");
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramString3;
    a(paramString1, paramString2, 2131887176, arrayOfObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */