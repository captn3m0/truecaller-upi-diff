package com.truecaller.messaging.transport.im.a;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.api.services.messenger.v1.events.Event.f;

final class b$o
  extends u
{
  private final Event.f b;
  
  private b$o(e parame, Event.f paramf)
  {
    super(parame);
    b = paramf;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".onGroupInfoUpdated(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.a.b.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */