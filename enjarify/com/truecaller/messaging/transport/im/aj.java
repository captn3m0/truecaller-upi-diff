package com.truecaller.messaging.transport.im;

import dagger.a.d;
import javax.inject.Provider;

public final class aj
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private aj(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static aj a(Provider paramProvider1, Provider paramProvider2)
  {
    aj localaj = new com/truecaller/messaging/transport/im/aj;
    localaj.<init>(paramProvider1, paramProvider2);
    return localaj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.aj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */