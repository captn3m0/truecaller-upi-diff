package com.truecaller.messaging.transport.im;

public enum SendResult
{
  static
  {
    SendResult[] arrayOfSendResult = new SendResult[3];
    SendResult localSendResult = new com/truecaller/messaging/transport/im/SendResult;
    localSendResult.<init>("SUCCESS", 0);
    SUCCESS = localSendResult;
    arrayOfSendResult[0] = localSendResult;
    localSendResult = new com/truecaller/messaging/transport/im/SendResult;
    int i = 1;
    localSendResult.<init>("FAILURE_TRANSIENT", i);
    FAILURE_TRANSIENT = localSendResult;
    arrayOfSendResult[i] = localSendResult;
    localSendResult = new com/truecaller/messaging/transport/im/SendResult;
    i = 2;
    localSendResult.<init>("FAILURE_PERMANENT", i);
    FAILURE_PERMANENT = localSendResult;
    arrayOfSendResult[i] = localSendResult;
    $VALUES = arrayOfSendResult;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.SendResult
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */