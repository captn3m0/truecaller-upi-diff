package com.truecaller.messaging.transport.im;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.log.AssertionUtil;

public final class ImSubscriptionService
  extends Service
{
  public ay a;
  private final Binder b;
  private final Handler c;
  private final Runnable d;
  
  public ImSubscriptionService()
  {
    Object localObject = new com/truecaller/messaging/transport/im/ImSubscriptionService$a;
    ((ImSubscriptionService.a)localObject).<init>();
    localObject = (Binder)localObject;
    b = ((Binder)localObject);
    localObject = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    ((Handler)localObject).<init>(localLooper);
    c = ((Handler)localObject);
    localObject = new com/truecaller/messaging/transport/im/ImSubscriptionService$b;
    ((ImSubscriptionService.b)localObject).<init>(this);
    localObject = (Runnable)localObject;
    d = ((Runnable)localObject);
    localObject = TrueApp.y();
    k.a(localObject, "TrueApp.getApp()");
    ((TrueApp)localObject).a().a(this);
  }
  
  private final void a()
  {
    Object localObject1 = c;
    Object localObject2 = d;
    ((Handler)localObject1).removeCallbacks((Runnable)localObject2);
    try
    {
      localObject1 = new android/content/Intent;
      localObject2 = this;
      localObject2 = (Context)this;
      Class localClass = ImSubscriptionService.class;
      ((Intent)localObject1).<init>((Context)localObject2, localClass);
      startService((Intent)localObject1);
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      AssertionUtil.reportThrowableButNeverCrash((Throwable)localIllegalStateException);
    }
  }
  
  public final IBinder onBind(Intent paramIntent)
  {
    a();
    return (IBinder)b;
  }
  
  public final void onCreate()
  {
    super.onCreate();
    ay localay = a;
    if (localay == null)
    {
      String str = "subscriptionManager";
      k.a(str);
    }
    localay.a();
  }
  
  public final void onDestroy()
  {
    ay localay = a;
    if (localay == null)
    {
      String str = "subscriptionManager";
      k.a(str);
    }
    localay.b();
    super.onDestroy();
  }
  
  public final void onRebind(Intent paramIntent)
  {
    a();
  }
  
  public final boolean onUnbind(Intent paramIntent)
  {
    paramIntent = c;
    Runnable localRunnable = d;
    paramIntent.postDelayed(localRunnable, 10000L);
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ImSubscriptionService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */