package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.List;

final class bq$f
  extends u
{
  private final List b;
  
  private bq$f(e parame, List paramList)
  {
    super(parame);
    b = paramList;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".markJoinedImUsersAsNotified(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bq.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */