package com.truecaller.messaging.transport.im;

import c.d.f;
import c.g.a.m;
import c.g.b.k;
import com.truecaller.featuretoggles.b;
import com.truecaller.messaging.h;
import java.lang.ref.WeakReference;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;

public final class bv
  implements bu
{
  final com.truecaller.common.g.a a;
  final dagger.a b;
  final f c;
  private final h d;
  private final com.truecaller.featuretoggles.e e;
  private final f f;
  
  public bv(h paramh, com.truecaller.common.g.a parama, com.truecaller.featuretoggles.e parame, dagger.a parama1, f paramf1, f paramf2)
  {
    d = paramh;
    a = parama;
    e = parame;
    b = parama1;
    f = paramf1;
    c = paramf2;
  }
  
  public final int a()
  {
    b localb = e.i();
    boolean bool = localb.a();
    if (bool) {
      return 5;
    }
    return 4;
  }
  
  public final void a(int paramInt)
  {
    d.l(paramInt);
  }
  
  public final void a(WeakReference paramWeakReference)
  {
    Object localObject1 = "listenerRef";
    k.b(paramWeakReference, (String)localObject1);
    int i = c();
    int j = a();
    if (i < j) {
      return;
    }
    localObject1 = (ag)bg.a;
    f localf = f;
    Object localObject2 = new com/truecaller/messaging/transport/im/bv$a;
    ((bv.a)localObject2).<init>(this, paramWeakReference, null);
    localObject2 = (m)localObject2;
    kotlinx.coroutines.e.b((ag)localObject1, localf, (m)localObject2, 2);
  }
  
  public final boolean b()
  {
    h localh = d;
    int i = localh.I();
    boolean bool = b(i);
    return !bool;
  }
  
  public final boolean b(int paramInt)
  {
    int i = a();
    return i >= paramInt;
  }
  
  public final int c()
  {
    return d.X();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bv
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */