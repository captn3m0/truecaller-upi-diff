package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.models.input.InputReportType;

public final class r
  implements q
{
  private final v a;
  
  public r(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return q.class.equals(paramClass);
  }
  
  public final w a(InputReportType paramInputReportType, String paramString1, String paramString2, String paramString3)
  {
    v localv = a;
    r.c localc = new com/truecaller/messaging/transport/im/r$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, paramInputReportType, paramString1, paramString2, paramString3, (byte)0);
    return w.a(localv, localc);
  }
  
  public final w a(String paramString1, long paramLong, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    v localv = a;
    r.b localb = new com/truecaller/messaging/transport/im/r$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramString1, paramLong, paramString2, paramString3, paramString4, paramString5, (byte)0);
    return w.a(localv, localb);
  }
  
  public final void a()
  {
    v localv = a;
    r.a locala = new com/truecaller/messaging/transport/im/r$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, (byte)0);
    localv.a(locala);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */