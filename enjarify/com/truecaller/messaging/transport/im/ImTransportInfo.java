package com.truecaller.messaging.transport.im;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Reaction;
import com.truecaller.messaging.data.types.TransportInfo;
import org.a.a.b;

public final class ImTransportInfo
  implements TransportInfo
{
  public static final Parcelable.Creator CREATOR;
  public final long a;
  public final String b;
  public final int c;
  public final int d;
  public final int e;
  public final int f;
  public final int g;
  public final int h;
  public final int i;
  public final long j;
  public final Reaction[] k;
  final int l;
  final Participant m;
  
  static
  {
    ImTransportInfo.b localb = new com/truecaller/messaging/transport/im/ImTransportInfo$b;
    localb.<init>();
    CREATOR = localb;
  }
  
  private ImTransportInfo(long paramLong1, String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, long paramLong2, Reaction[] paramArrayOfReaction, int paramInt8, Participant paramParticipant)
  {
    a = paramLong1;
    b = paramString;
    c = paramInt1;
    d = paramInt2;
    e = paramInt3;
    f = paramInt4;
    g = paramInt5;
    h = paramInt6;
    i = paramInt7;
    j = paramLong2;
    k = paramArrayOfReaction;
    l = paramInt8;
    m = paramParticipant;
  }
  
  public final int a()
  {
    return d;
  }
  
  public final String a(b paramb)
  {
    k.b(paramb, "date");
    return b;
  }
  
  public final int b()
  {
    return e;
  }
  
  public final long c()
  {
    return a;
  }
  
  public final long d()
  {
    return 0L;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final long e()
  {
    return -1;
  }
  
  public final boolean f()
  {
    return false;
  }
  
  public final ImTransportInfo.a g()
  {
    ImTransportInfo.a locala = new com/truecaller/messaging/transport/im/ImTransportInfo$a;
    locala.<init>(this);
    return locala;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    long l1 = a;
    paramParcel.writeLong(l1);
    Object localObject = b;
    paramParcel.writeString((String)localObject);
    int n = c;
    paramParcel.writeInt(n);
    n = d;
    paramParcel.writeInt(n);
    n = e;
    paramParcel.writeInt(n);
    n = f;
    paramParcel.writeInt(n);
    n = g;
    paramParcel.writeInt(n);
    n = h;
    paramParcel.writeInt(n);
    n = i;
    paramParcel.writeInt(n);
    l1 = j;
    paramParcel.writeLong(l1);
    localObject = k;
    int i1 = 0;
    if (localObject != null)
    {
      paramParcel.writeInt(1);
      int i2 = localObject.length;
      paramParcel.writeInt(i2);
      while (i2 > i1)
      {
        Parcelable localParcelable = localObject[i1];
        paramParcel.writeParcelable(localParcelable, paramInt);
        i1 += 1;
      }
    }
    paramParcel.writeInt(0);
    n = l;
    paramParcel.writeInt(n);
    localObject = m;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ImTransportInfo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */