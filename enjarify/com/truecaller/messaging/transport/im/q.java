package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.models.input.InputReportType;

public abstract interface q
{
  public abstract w a(InputReportType paramInputReportType, String paramString1, String paramString2, String paramString3);
  
  public abstract w a(String paramString1, long paramLong, String paramString2, String paramString3, String paramString4, String paramString5);
  
  public abstract void a();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */