package com.truecaller.messaging.transport.im;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import c.g.b.k;
import com.google.f.x;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.events.Event;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.x;
import com.truecaller.log.AssertionUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class bl
  implements bj
{
  private final ContentResolver a;
  private final bu b;
  private final m c;
  
  public bl(ContentResolver paramContentResolver, bu parambu, m paramm)
  {
    a = paramContentResolver;
    b = parambu;
    c = paramm;
  }
  
  /* Error */
  private final List a(int paramInt)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 29	com/truecaller/messaging/transport/im/bl:a	Landroid/content/ContentResolver;
    //   4: astore_2
    //   5: invokestatic 38	com/truecaller/content/TruecallerContract$x:a	()Landroid/net/Uri;
    //   8: astore_3
    //   9: iconst_2
    //   10: anewarray 44	java/lang/String
    //   13: dup
    //   14: iconst_0
    //   15: ldc 40
    //   17: aastore
    //   18: dup
    //   19: iconst_1
    //   20: ldc 42
    //   22: aastore
    //   23: astore 4
    //   25: ldc 46
    //   27: astore 5
    //   29: iconst_1
    //   30: istore 6
    //   32: iload 6
    //   34: anewarray 44	java/lang/String
    //   37: astore 7
    //   39: iload_1
    //   40: invokestatic 51	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   43: astore 8
    //   45: aload 7
    //   47: iconst_0
    //   48: aload 8
    //   50: aastore
    //   51: ldc 53
    //   53: astore 8
    //   55: aload_2
    //   56: aload_3
    //   57: aload 4
    //   59: aload 5
    //   61: aload 7
    //   63: aload 8
    //   65: invokevirtual 59	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   68: astore_2
    //   69: aload_2
    //   70: ifnull +158 -> 228
    //   73: aload_2
    //   74: astore_3
    //   75: aload_2
    //   76: checkcast 61	java/io/Closeable
    //   79: astore_3
    //   80: aconst_null
    //   81: astore 4
    //   83: new 63	java/util/ArrayList
    //   86: astore 5
    //   88: aload 5
    //   90: invokespecial 64	java/util/ArrayList:<init>	()V
    //   93: aload 5
    //   95: checkcast 66	java/util/Collection
    //   98: astore 5
    //   100: aload_2
    //   101: invokeinterface 72 1 0
    //   106: istore 9
    //   108: iload 9
    //   110: ifeq +61 -> 171
    //   113: new 74	com/truecaller/messaging/transport/im/bl$a
    //   116: astore 7
    //   118: aload_2
    //   119: iconst_0
    //   120: invokeinterface 78 2 0
    //   125: lstore 10
    //   127: aload_2
    //   128: iload 6
    //   130: invokeinterface 82 2 0
    //   135: astore 8
    //   137: ldc 84
    //   139: astore 12
    //   141: aload 8
    //   143: aload 12
    //   145: invokestatic 86	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   148: aload 7
    //   150: lload 10
    //   152: aload 8
    //   154: iload_1
    //   155: invokespecial 89	com/truecaller/messaging/transport/im/bl$a:<init>	(J[BI)V
    //   158: aload 5
    //   160: aload 7
    //   162: invokeinterface 93 2 0
    //   167: pop
    //   168: goto -68 -> 100
    //   171: aload 5
    //   173: checkcast 95	java/util/List
    //   176: astore 5
    //   178: aload_3
    //   179: aconst_null
    //   180: invokestatic 100	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   183: aload 5
    //   185: checkcast 102	java/lang/Iterable
    //   188: astore 5
    //   190: aload 5
    //   192: invokestatic 108	c/a/m:g	(Ljava/lang/Iterable;)Ljava/util/List;
    //   195: astore 13
    //   197: aload 13
    //   199: ifnonnull +37 -> 236
    //   202: goto +26 -> 228
    //   205: astore 13
    //   207: goto +12 -> 219
    //   210: astore 13
    //   212: aload 13
    //   214: astore 4
    //   216: aload 13
    //   218: athrow
    //   219: aload_3
    //   220: aload 4
    //   222: invokestatic 100	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   225: aload 13
    //   227: athrow
    //   228: getstatic 113	c/a/y:a	Lc/a/y;
    //   231: checkcast 95	java/util/List
    //   234: astore 13
    //   236: aload 13
    //   238: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	239	0	this	bl
    //   0	239	1	paramInt	int
    //   4	124	2	localObject1	Object
    //   8	212	3	localObject2	Object
    //   23	198	4	localObject3	Object
    //   27	164	5	localObject4	Object
    //   30	99	6	i	int
    //   37	124	7	localObject5	Object
    //   43	110	8	localObject6	Object
    //   106	3	9	bool	boolean
    //   125	26	10	l	long
    //   139	5	12	str	String
    //   195	3	13	localList1	List
    //   205	1	13	localObject7	Object
    //   210	16	13	localObject8	Object
    //   234	3	13	localList2	List
    // Exception table:
    //   from	to	target	type
    //   216	219	205	finally
    //   83	86	210	finally
    //   88	93	210	finally
    //   93	98	210	finally
    //   100	106	210	finally
    //   113	116	210	finally
    //   119	125	210	finally
    //   128	135	210	finally
    //   143	148	210	finally
    //   154	158	210	finally
    //   160	168	210	finally
    //   171	176	210	finally
  }
  
  private final boolean a(List paramList)
  {
    boolean bool1 = paramList.isEmpty();
    if (bool1) {
      return false;
    }
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramList = (Iterable)paramList;
    Object localObject1 = new java/util/ArrayList;
    int i = c.a.m.a(paramList, 10);
    ((ArrayList)localObject1).<init>(i);
    localObject1 = (Collection)localObject1;
    paramList = paramList.iterator();
    boolean bool3;
    for (;;)
    {
      boolean bool2 = paramList.hasNext();
      bool3 = true;
      if (!bool2) {
        break;
      }
      long l = ((Number)paramList.next()).longValue();
      Object localObject2 = ContentProviderOperation.newDelete(TruecallerContract.x.a());
      String str1 = "_id=?";
      String[] arrayOfString = new String[bool3];
      String str2 = String.valueOf(l);
      arrayOfString[0] = str2;
      localObject2 = ((ContentProviderOperation.Builder)localObject2).withSelection(str1, arrayOfString).build();
      ((Collection)localObject1).add(localObject2);
    }
    localObject1 = (Collection)localObject1;
    localArrayList.addAll((Collection)localObject1);
    paramList = a(localArrayList);
    if (paramList != null)
    {
      int j = paramList.length;
      if (j == 0)
      {
        j = 1;
      }
      else
      {
        j = 0;
        paramList = null;
      }
      if (j == 0) {
        return bool3;
      }
    }
    return false;
  }
  
  private final ContentProviderResult[] a(ArrayList paramArrayList)
  {
    try
    {
      ContentResolver localContentResolver = a;
      String str = TruecallerContract.a();
      return localContentResolver.applyBatch(str, paramArrayList);
    }
    catch (RemoteException|OperationApplicationException localRemoteException) {}
    return null;
  }
  
  public final w a()
  {
    int i = b.a();
    Object localObject1 = a(i);
    int k = 1;
    boolean bool2 = false;
    String[] arrayOfString = null;
    for (;;)
    {
      boolean bool3 = ((List)localObject1).isEmpty();
      if (bool3) {
        break;
      }
      Object localObject2 = new java/util/ArrayList;
      ((ArrayList)localObject2).<init>();
      localObject2 = (List)localObject2;
      localObject1 = ((Iterable)localObject1).iterator();
      for (;;)
      {
        boolean bool4 = ((Iterator)localObject1).hasNext();
        if (!bool4) {
          break;
        }
        localObject3 = (bl.a)((Iterator)localObject1).next();
        try
        {
          localObject4 = b;
          localObject4 = Event.a((byte[])localObject4);
          Object localObject5 = c;
          String str = "event";
          k.a(localObject4, str);
          localObject4 = ((m)localObject5).a((Event)localObject4, false);
          localObject5 = ProcessResult.SUCCESS;
          if (localObject4 == localObject5)
          {
            long l = a;
            localObject3 = Long.valueOf(l);
            ((List)localObject2).add(localObject3);
          }
        }
        catch (x localx)
        {
          localObject3 = (Throwable)localx;
          AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject3);
        }
      }
      boolean bool1 = a((List)localObject2);
      if (!bool1) {
        break;
      }
      int j = b.a();
      localObject1 = a(j);
      arrayOfString = new String[k];
      Object localObject3 = new java/lang/StringBuilder;
      Object localObject4 = "Unsupported events are processed successfully, processed count ";
      ((StringBuilder)localObject3).<init>((String)localObject4);
      int m = ((List)localObject2).size();
      ((StringBuilder)localObject3).append(m);
      localObject2 = ((StringBuilder)localObject3).toString();
      arrayOfString[0] = localObject2;
      bool2 = true;
    }
    localObject1 = w.b(Boolean.valueOf(bool2));
    k.a(localObject1, "Promise.wrap(isSuccess)");
    return (w)localObject1;
  }
  
  public final void a(Event paramEvent, int paramInt)
  {
    k.b(paramEvent, "event");
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    paramEvent = paramEvent.toByteArray();
    localContentValues.put("event", paramEvent);
    Object localObject = Integer.valueOf(paramInt);
    localContentValues.put("api_version", (Integer)localObject);
    paramEvent = a;
    localObject = TruecallerContract.x.a();
    paramEvent.insert((Uri)localObject, localContentValues);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */