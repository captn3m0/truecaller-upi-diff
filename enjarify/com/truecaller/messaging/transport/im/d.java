package com.truecaller.messaging.transport.im;

import android.net.Uri;
import c.g.b.k;
import okhttp3.u;

final class d
{
  final long a;
  final long b;
  final u c;
  final Uri d;
  final long e;
  
  public d(long paramLong1, long paramLong2, u paramu, Uri paramUri, long paramLong3)
  {
    a = paramLong1;
    b = paramLong2;
    c = paramu;
    d = paramUri;
    e = paramLong3;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof d;
      if (bool2)
      {
        paramObject = (d)paramObject;
        long l1 = a;
        long l2 = a;
        bool2 = l1 < l2;
        Object localObject1;
        if (!bool2)
        {
          bool2 = true;
        }
        else
        {
          bool2 = false;
          localObject1 = null;
        }
        if (bool2)
        {
          l1 = b;
          l2 = b;
          bool2 = l1 < l2;
          if (!bool2)
          {
            bool2 = true;
          }
          else
          {
            bool2 = false;
            localObject1 = null;
          }
          if (bool2)
          {
            localObject1 = c;
            Object localObject2 = c;
            bool2 = k.a(localObject1, localObject2);
            if (bool2)
            {
              localObject1 = d;
              localObject2 = d;
              bool2 = k.a(localObject1, localObject2);
              if (bool2)
              {
                l1 = e;
                l2 = e;
                boolean bool3 = l1 < l2;
                if (!bool3)
                {
                  bool3 = true;
                }
                else
                {
                  bool3 = false;
                  paramObject = null;
                }
                if (bool3) {
                  return bool1;
                }
              }
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    long l1 = a;
    int i = 32;
    long l2 = l1 >>> i;
    l1 ^= l2;
    int j = (int)l1 * 31;
    l2 = b;
    long l3 = l2 >>> i;
    l2 ^= l3;
    int k = (int)l2;
    j = (j + k) * 31;
    Object localObject = c;
    int m = 0;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = d;
    if (localObject != null) {
      m = localObject.hashCode();
    }
    j = (j + m) * 31;
    l2 = e;
    l3 = l2 >>> i;
    k = (int)(l2 ^ l3);
    return j + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("DownloadQueueItem(id=");
    long l = a;
    localStringBuilder.append(l);
    localStringBuilder.append(", entityId=");
    l = b;
    localStringBuilder.append(l);
    localStringBuilder.append(", source=");
    Object localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", currentUri=");
    localObject = d;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", size=");
    l = e;
    localStringBuilder.append(l);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */