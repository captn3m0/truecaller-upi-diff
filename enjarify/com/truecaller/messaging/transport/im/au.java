package com.truecaller.messaging.transport.im;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import c.a.m;
import c.g.b.k;
import com.truecaller.androidactors.w;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.u;
import com.truecaller.messaging.data.c;
import com.truecaller.messaging.data.types.Reaction;
import com.truecaller.messaging.h;
import com.truecaller.messaging.notifications.g;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public final class au
  implements as
{
  private long a;
  private final ContentResolver b;
  private final c c;
  private final h d;
  private final g e;
  
  public au(ContentResolver paramContentResolver, c paramc, h paramh, g paramg)
  {
    b = paramContentResolver;
    c = paramc;
    d = paramh;
    e = paramg;
    a = -1;
  }
  
  private final ContentProviderResult[] a(ArrayList paramArrayList)
  {
    try
    {
      ContentResolver localContentResolver = b;
      String str = TruecallerContract.a();
      return localContentResolver.applyBatch(str, paramArrayList);
    }
    catch (RemoteException|OperationApplicationException localRemoteException) {}
    return null;
  }
  
  public final w a(Reaction[] paramArrayOfReaction)
  {
    k.b(paramArrayOfReaction, "reactions");
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject1 = new java/util/ArrayList;
    int i = paramArrayOfReaction.length;
    ((ArrayList)localObject1).<init>(i);
    localObject1 = (Collection)localObject1;
    i = paramArrayOfReaction.length;
    boolean bool2 = false;
    int k = 0;
    ContentProviderOperation.Builder localBuilder = null;
    int m;
    Object localObject2;
    Object localObject3;
    for (;;)
    {
      m = 1;
      if (k >= i) {
        break;
      }
      localObject2 = paramArrayOfReaction[k];
      localObject3 = ContentProviderOperation.newDelete(TruecallerContract.u.a());
      String str1 = "message_id=? AND from_peer_id=?";
      int n = 2;
      String[] arrayOfString = new String[n];
      long l1 = b;
      String str2 = String.valueOf(l1);
      arrayOfString[0] = str2;
      localObject2 = c;
      arrayOfString[m] = localObject2;
      ContentProviderOperation localContentProviderOperation = ((ContentProviderOperation.Builder)localObject3).withSelection(str1, arrayOfString).build();
      ((Collection)localObject1).add(localContentProviderOperation);
      k += 1;
    }
    localObject1 = (Collection)localObject1;
    localArrayList.addAll((Collection)localObject1);
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    i = paramArrayOfReaction.length;
    k = 0;
    localBuilder = null;
    while (k < i)
    {
      localObject2 = paramArrayOfReaction[k];
      localObject3 = (CharSequence)d;
      if (localObject3 != null)
      {
        i1 = ((CharSequence)localObject3).length();
        if (i1 != 0)
        {
          i1 = 0;
          localObject3 = null;
          break label244;
        }
      }
      int i1 = 1;
      label244:
      i1 ^= m;
      if (i1 != 0) {
        ((Collection)localObject1).add(localObject2);
      }
      k += 1;
    }
    localObject1 = (Iterable)localObject1;
    paramArrayOfReaction = new java/util/ArrayList;
    i = m.a((Iterable)localObject1, 10);
    paramArrayOfReaction.<init>(i);
    paramArrayOfReaction = (Collection)paramArrayOfReaction;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      Object localObject4 = (Reaction)((Iterator)localObject1).next();
      localBuilder = ContentProviderOperation.newInsert(TruecallerContract.u.a());
      localObject3 = Long.valueOf(b);
      localBuilder.withValue("message_id", localObject3);
      localObject3 = c;
      localBuilder.withValue("from_peer_id", localObject3);
      localObject3 = d;
      localBuilder.withValue("emoji", localObject3);
      long l2 = e;
      localObject3 = Long.valueOf(l2);
      localBuilder.withValue("send_date", localObject3);
      localObject2 = "status";
      int j = f;
      localObject4 = Integer.valueOf(j);
      localObject4 = localBuilder.withValue((String)localObject2, localObject4).build();
      paramArrayOfReaction.add(localObject4);
    }
    paramArrayOfReaction = (Collection)paramArrayOfReaction;
    localArrayList.addAll(paramArrayOfReaction);
    paramArrayOfReaction = a(localArrayList);
    if (paramArrayOfReaction != null)
    {
      int i2 = paramArrayOfReaction.length;
      if (i2 == 0)
      {
        i2 = 1;
      }
      else
      {
        i2 = 0;
        paramArrayOfReaction = null;
      }
      if (i2 == 0) {
        bool2 = true;
      }
    }
    paramArrayOfReaction = w.b(Boolean.valueOf(bool2));
    k.a(paramArrayOfReaction, "Promise.wrap(isSuccess)");
    return paramArrayOfReaction;
  }
  
  /* Error */
  public final void a()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 35	com/truecaller/messaging/transport/im/au:b	Landroid/content/ContentResolver;
    //   4: astore_1
    //   5: invokestatic 199	com/truecaller/content/TruecallerContract$ai:a	()Landroid/net/Uri;
    //   8: astore_2
    //   9: ldc -55
    //   11: astore_3
    //   12: iconst_3
    //   13: anewarray 85	java/lang/String
    //   16: astore 4
    //   18: aload 4
    //   20: iconst_0
    //   21: ldc -52
    //   23: aastore
    //   24: aload 4
    //   26: iconst_1
    //   27: ldc -52
    //   29: aastore
    //   30: aload_0
    //   31: getfield 43	com/truecaller/messaging/transport/im/au:a	J
    //   34: lstore 5
    //   36: lload 5
    //   38: invokestatic 93	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   41: astore 7
    //   43: iconst_2
    //   44: istore 8
    //   46: aload 4
    //   48: iload 8
    //   50: aload 7
    //   52: aastore
    //   53: ldc -50
    //   55: astore 9
    //   57: aconst_null
    //   58: astore 7
    //   60: aload_1
    //   61: aload_2
    //   62: aconst_null
    //   63: aload_3
    //   64: aload 4
    //   66: aload 9
    //   68: invokevirtual 210	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   71: astore_1
    //   72: aload_0
    //   73: getfield 37	com/truecaller/messaging/transport/im/au:c	Lcom/truecaller/messaging/data/c;
    //   76: aload_1
    //   77: invokeinterface 216 2 0
    //   82: astore_1
    //   83: aconst_null
    //   84: astore_2
    //   85: aload_1
    //   86: ifnull +114 -> 200
    //   89: aload_1
    //   90: checkcast 218	android/database/Cursor
    //   93: astore_1
    //   94: aload_1
    //   95: astore 7
    //   97: aload_1
    //   98: checkcast 220	java/io/Closeable
    //   101: astore 7
    //   103: new 62	java/util/ArrayList
    //   106: astore_3
    //   107: aload_3
    //   108: invokespecial 63	java/util/ArrayList:<init>	()V
    //   111: aload_3
    //   112: checkcast 68	java/util/Collection
    //   115: astore_3
    //   116: aload_1
    //   117: invokeinterface 223 1 0
    //   122: istore 10
    //   124: iload 10
    //   126: ifeq +33 -> 159
    //   129: aload_1
    //   130: astore 4
    //   132: aload_1
    //   133: checkcast 225	com/truecaller/messaging/data/a/n
    //   136: astore 4
    //   138: aload 4
    //   140: invokeinterface 228 1 0
    //   145: astore 4
    //   147: aload_3
    //   148: aload 4
    //   150: invokeinterface 110 2 0
    //   155: pop
    //   156: goto -40 -> 116
    //   159: aload_3
    //   160: checkcast 112	java/util/List
    //   163: astore_3
    //   164: aload 7
    //   166: aconst_null
    //   167: invokestatic 233	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   170: aload_3
    //   171: checkcast 126	java/lang/Iterable
    //   174: astore_3
    //   175: aload_3
    //   176: invokestatic 238	c/a/ag:a	(Ljava/lang/Iterable;)Ljava/util/Map;
    //   179: astore_2
    //   180: goto +20 -> 200
    //   183: astore_1
    //   184: goto +8 -> 192
    //   187: astore_1
    //   188: aload_1
    //   189: astore_2
    //   190: aload_1
    //   191: athrow
    //   192: aload 7
    //   194: aload_2
    //   195: invokestatic 233	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   198: aload_1
    //   199: athrow
    //   200: aload_0
    //   201: getfield 41	com/truecaller/messaging/transport/im/au:e	Lcom/truecaller/messaging/notifications/g;
    //   204: aload_2
    //   205: invokeinterface 243 2 0
    //   210: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	211	0	this	au
    //   4	129	1	localObject1	Object
    //   183	1	1	localObject2	Object
    //   187	12	1	localObject3	Object
    //   8	197	2	localObject4	Object
    //   11	165	3	localObject5	Object
    //   16	133	4	localObject6	Object
    //   34	3	5	l	long
    //   41	152	7	localObject7	Object
    //   44	5	8	i	int
    //   55	12	9	str	String
    //   122	3	10	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   190	192	183	finally
    //   103	106	187	finally
    //   107	111	187	finally
    //   111	115	187	finally
    //   116	122	187	finally
    //   132	136	187	finally
    //   138	145	187	finally
    //   148	156	187	finally
    //   159	163	187	finally
  }
  
  /* Error */
  public final void a(long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 35	com/truecaller/messaging/transport/im/au:b	Landroid/content/ContentResolver;
    //   4: astore_3
    //   5: invokestatic 248	com/truecaller/content/TruecallerContract$aa:a	()Landroid/net/Uri;
    //   8: astore 4
    //   10: iconst_1
    //   11: anewarray 85	java/lang/String
    //   14: dup
    //   15: iconst_0
    //   16: ldc -6
    //   18: aastore
    //   19: astore 5
    //   21: ldc -11
    //   23: invokestatic 255	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   26: astore 6
    //   28: ldc -4
    //   30: aload 6
    //   32: invokevirtual 259	java/lang/String:concat	(Ljava/lang/String;)Ljava/lang/String;
    //   35: astore 7
    //   37: iconst_1
    //   38: istore 8
    //   40: iload 8
    //   42: anewarray 85	java/lang/String
    //   45: astore 9
    //   47: lload_1
    //   48: invokestatic 93	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   51: astore 10
    //   53: iconst_0
    //   54: istore 11
    //   56: aload 9
    //   58: iconst_0
    //   59: aload 10
    //   61: aastore
    //   62: aload_3
    //   63: aload 4
    //   65: aload 5
    //   67: aload 7
    //   69: aload 9
    //   71: aconst_null
    //   72: invokevirtual 210	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   75: astore 10
    //   77: aload 10
    //   79: ifnull +157 -> 236
    //   82: aload 10
    //   84: astore_3
    //   85: aload 10
    //   87: checkcast 220	java/io/Closeable
    //   90: astore_3
    //   91: aconst_null
    //   92: astore 4
    //   94: new 62	java/util/ArrayList
    //   97: astore 5
    //   99: aload 5
    //   101: invokespecial 63	java/util/ArrayList:<init>	()V
    //   104: aload 5
    //   106: checkcast 68	java/util/Collection
    //   109: astore 5
    //   111: aload 10
    //   113: invokeinterface 223 1 0
    //   118: istore 12
    //   120: iload 12
    //   122: ifeq +33 -> 155
    //   125: aload 10
    //   127: iconst_0
    //   128: invokeinterface 263 2 0
    //   133: lstore 13
    //   135: lload 13
    //   137: invokestatic 156	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   140: astore 7
    //   142: aload 5
    //   144: aload 7
    //   146: invokeinterface 110 2 0
    //   151: pop
    //   152: goto -41 -> 111
    //   155: aload 5
    //   157: checkcast 112	java/util/List
    //   160: astore 5
    //   162: aload_3
    //   163: aconst_null
    //   164: invokestatic 233	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   167: aload 5
    //   169: checkcast 68	java/util/Collection
    //   172: astore 5
    //   174: aload 5
    //   176: invokestatic 266	c/a/m:c	(Ljava/util/Collection;)[J
    //   179: astore 10
    //   181: aload 10
    //   183: arraylength
    //   184: istore 15
    //   186: iload 15
    //   188: ifne +6 -> 194
    //   191: iconst_1
    //   192: istore 11
    //   194: iload 11
    //   196: iload 8
    //   198: ixor
    //   199: istore 11
    //   201: iload 11
    //   203: ifeq +9 -> 212
    //   206: aload_0
    //   207: aload 10
    //   209: invokevirtual 269	com/truecaller/messaging/transport/im/au:a	([J)V
    //   212: return
    //   213: astore 10
    //   215: goto +12 -> 227
    //   218: astore 10
    //   220: aload 10
    //   222: astore 4
    //   224: aload 10
    //   226: athrow
    //   227: aload_3
    //   228: aload 4
    //   230: invokestatic 233	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   233: aload 10
    //   235: athrow
    //   236: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	237	0	this	au
    //   0	237	1	paramLong	long
    //   4	224	3	localObject1	Object
    //   8	221	4	localObject2	Object
    //   19	156	5	localObject3	Object
    //   26	5	6	str	String
    //   35	110	7	localObject4	Object
    //   38	161	8	i	int
    //   45	25	9	arrayOfString	String[]
    //   51	157	10	localObject5	Object
    //   213	1	10	localObject6	Object
    //   218	16	10	localObject7	Object
    //   54	148	11	j	int
    //   118	3	12	bool	boolean
    //   133	3	13	l	long
    //   184	3	15	k	int
    // Exception table:
    //   from	to	target	type
    //   224	227	213	finally
    //   94	97	218	finally
    //   99	104	218	finally
    //   104	109	218	finally
    //   111	118	218	finally
    //   127	133	218	finally
    //   135	140	218	finally
    //   144	152	218	finally
    //   155	160	218	finally
  }
  
  public final void a(long[] paramArrayOfLong)
  {
    k.b(paramArrayOfLong, "messageIds");
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    int i = paramArrayOfLong.length;
    int j = 0;
    int k = 0;
    for (;;)
    {
      int m = 1;
      if (k >= i) {
        break;
      }
      long l = paramArrayOfLong[k];
      ContentProviderOperation.Builder localBuilder = ContentProviderOperation.newUpdate(TruecallerContract.u.a());
      String str1 = "message_id=?";
      Object localObject = new String[m];
      String str2 = String.valueOf(l);
      localObject[0] = str2;
      localObject = localBuilder.withSelection(str1, (String[])localObject);
      str2 = "status";
      Integer localInteger = Integer.valueOf(0);
      localObject = ((ContentProviderOperation.Builder)localObject).withValue(str2, localInteger).build();
      localArrayList.add(localObject);
      k += 1;
    }
    paramArrayOfLong = a(localArrayList);
    if (paramArrayOfLong != null)
    {
      int n = paramArrayOfLong.length;
      if (n == 0)
      {
        n = 1;
      }
      else
      {
        n = 0;
        paramArrayOfLong = null;
      }
      if (n == 0) {}
    }
    else
    {
      j = 1;
    }
    if (j == 0) {
      a();
    }
  }
  
  public final void b(long paramLong)
  {
    a = paramLong;
  }
  
  public final void c(long paramLong)
  {
    long l = a;
    boolean bool = l < paramLong;
    if (!bool)
    {
      paramLong = -1;
      a = paramLong;
    }
  }
  
  /* Error */
  public final w d(long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 35	com/truecaller/messaging/transport/im/au:b	Landroid/content/ContentResolver;
    //   4: astore_3
    //   5: invokestatic 74	com/truecaller/content/TruecallerContract$u:a	()Landroid/net/Uri;
    //   8: astore 4
    //   10: iconst_1
    //   11: anewarray 85	java/lang/String
    //   14: dup
    //   15: iconst_0
    //   16: ldc -92
    //   18: aastore
    //   19: astore 5
    //   21: ldc_w 281
    //   24: astore 6
    //   26: iconst_2
    //   27: istore 7
    //   29: iload 7
    //   31: anewarray 85	java/lang/String
    //   34: astore 8
    //   36: aload_0
    //   37: getfield 39	com/truecaller/messaging/transport/im/au:d	Lcom/truecaller/messaging/h;
    //   40: invokeinterface 286 1 0
    //   45: astore 9
    //   47: aload 8
    //   49: iconst_0
    //   50: aload 9
    //   52: aastore
    //   53: lload_1
    //   54: invokestatic 93	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   57: astore 10
    //   59: aload 8
    //   61: iconst_1
    //   62: aload 10
    //   64: aastore
    //   65: aconst_null
    //   66: astore 9
    //   68: aload_3
    //   69: aload 4
    //   71: aload 5
    //   73: aload 6
    //   75: aload 8
    //   77: aconst_null
    //   78: invokevirtual 210	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   81: astore 10
    //   83: aconst_null
    //   84: astore 11
    //   86: aload 10
    //   88: ifnull +75 -> 163
    //   91: aload 10
    //   93: checkcast 220	java/io/Closeable
    //   96: astore 10
    //   98: aload 10
    //   100: astore_3
    //   101: aload 10
    //   103: checkcast 218	android/database/Cursor
    //   106: astore_3
    //   107: aload_3
    //   108: invokeinterface 289 1 0
    //   113: istore 12
    //   115: iload 12
    //   117: ifeq +14 -> 131
    //   120: aload_3
    //   121: iconst_0
    //   122: invokeinterface 293 2 0
    //   127: astore_3
    //   128: goto +5 -> 133
    //   131: aconst_null
    //   132: astore_3
    //   133: aload 10
    //   135: aconst_null
    //   136: invokestatic 233	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   139: aload_3
    //   140: astore 11
    //   142: goto +21 -> 163
    //   145: astore_3
    //   146: goto +8 -> 154
    //   149: astore 11
    //   151: aload 11
    //   153: athrow
    //   154: aload 10
    //   156: aload 11
    //   158: invokestatic 233	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   161: aload_3
    //   162: athrow
    //   163: aload 11
    //   165: invokestatic 192	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   168: astore 10
    //   170: aload 10
    //   172: ldc_w 295
    //   175: invokestatic 196	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   178: aload 10
    //   180: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	181	0	this	au
    //   0	181	1	paramLong	long
    //   4	136	3	localObject1	Object
    //   145	17	3	localObject2	Object
    //   8	62	4	localUri	android.net.Uri
    //   19	53	5	arrayOfString1	String[]
    //   24	50	6	str1	String
    //   27	3	7	i	int
    //   34	42	8	arrayOfString2	String[]
    //   45	22	9	str2	String
    //   57	122	10	localObject3	Object
    //   84	57	11	localObject4	Object
    //   149	15	11	localThrowable	Throwable
    //   113	3	12	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   151	154	145	finally
    //   101	106	149	finally
    //   107	113	149	finally
    //   121	127	149	finally
  }
  
  /* Error */
  public final w e(long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 35	com/truecaller/messaging/transport/im/au:b	Landroid/content/ContentResolver;
    //   4: astore_3
    //   5: invokestatic 199	com/truecaller/content/TruecallerContract$ai:a	()Landroid/net/Uri;
    //   8: astore 4
    //   10: ldc_w 297
    //   13: astore 5
    //   15: iconst_1
    //   16: anewarray 85	java/lang/String
    //   19: astore 6
    //   21: lload_1
    //   22: invokestatic 93	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   25: astore 7
    //   27: aload 6
    //   29: iconst_0
    //   30: aload 7
    //   32: aastore
    //   33: iconst_0
    //   34: istore 8
    //   36: aconst_null
    //   37: astore 9
    //   39: aload_3
    //   40: aload 4
    //   42: aconst_null
    //   43: aload 5
    //   45: aload 6
    //   47: aconst_null
    //   48: invokevirtual 210	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   51: astore 7
    //   53: aload_0
    //   54: getfield 37	com/truecaller/messaging/transport/im/au:c	Lcom/truecaller/messaging/data/c;
    //   57: aload 7
    //   59: invokeinterface 216 2 0
    //   64: astore 7
    //   66: aconst_null
    //   67: astore 10
    //   69: aload 7
    //   71: ifnull +135 -> 206
    //   74: aload 7
    //   76: checkcast 218	android/database/Cursor
    //   79: astore 7
    //   81: aload 7
    //   83: astore_3
    //   84: aload 7
    //   86: checkcast 220	java/io/Closeable
    //   89: astore_3
    //   90: new 62	java/util/ArrayList
    //   93: astore 4
    //   95: aload 4
    //   97: invokespecial 63	java/util/ArrayList:<init>	()V
    //   100: aload 4
    //   102: checkcast 68	java/util/Collection
    //   105: astore 4
    //   107: aload 7
    //   109: invokeinterface 223 1 0
    //   114: istore 8
    //   116: iload 8
    //   118: ifeq +36 -> 154
    //   121: aload 7
    //   123: astore 9
    //   125: aload 7
    //   127: checkcast 225	com/truecaller/messaging/data/a/n
    //   130: astore 9
    //   132: aload 9
    //   134: invokeinterface 228 1 0
    //   139: astore 9
    //   141: aload 4
    //   143: aload 9
    //   145: invokeinterface 110 2 0
    //   150: pop
    //   151: goto -44 -> 107
    //   154: aload 4
    //   156: checkcast 112	java/util/List
    //   159: astore 4
    //   161: aload_3
    //   162: aconst_null
    //   163: invokestatic 233	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   166: aload 4
    //   168: checkcast 126	java/lang/Iterable
    //   171: astore 4
    //   173: aload 4
    //   175: invokestatic 238	c/a/ag:a	(Ljava/lang/Iterable;)Ljava/util/Map;
    //   178: astore 10
    //   180: goto +26 -> 206
    //   183: astore 7
    //   185: goto +12 -> 197
    //   188: astore 7
    //   190: aload 7
    //   192: astore 10
    //   194: aload 7
    //   196: athrow
    //   197: aload_3
    //   198: aload 10
    //   200: invokestatic 233	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   203: aload 7
    //   205: athrow
    //   206: aload 10
    //   208: invokestatic 192	com/truecaller/androidactors/w:b	(Ljava/lang/Object;)Lcom/truecaller/androidactors/w;
    //   211: astore 7
    //   213: aload 7
    //   215: ldc_w 299
    //   218: invokestatic 196	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   221: aload 7
    //   223: ldc_w 301
    //   226: invokestatic 196	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   229: aload 7
    //   231: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	232	0	this	au
    //   0	232	1	paramLong	long
    //   4	194	3	localObject1	Object
    //   8	166	4	localObject2	Object
    //   13	31	5	str	String
    //   19	27	6	arrayOfString	String[]
    //   25	101	7	localObject3	Object
    //   183	1	7	localObject4	Object
    //   188	16	7	localObject5	Object
    //   211	19	7	localw	w
    //   34	83	8	bool	boolean
    //   37	107	9	localObject6	Object
    //   67	140	10	localObject7	Object
    // Exception table:
    //   from	to	target	type
    //   194	197	183	finally
    //   90	93	188	finally
    //   95	100	188	finally
    //   100	105	188	finally
    //   107	114	188	finally
    //   125	130	188	finally
    //   132	139	188	finally
    //   143	151	188	finally
    //   154	159	188	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.au
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */