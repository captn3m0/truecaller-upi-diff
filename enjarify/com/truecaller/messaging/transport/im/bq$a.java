package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.Collection;

final class bq$a
  extends u
{
  private final Collection b;
  private final boolean c;
  
  private bq$a(e parame, Collection paramCollection, boolean paramBoolean)
  {
    super(parame);
    b = paramCollection;
    c = paramBoolean;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".forceUpdateImUsers(");
    Object localObject = b;
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Boolean.valueOf(c), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bq.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */