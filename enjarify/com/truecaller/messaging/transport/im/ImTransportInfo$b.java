package com.truecaller.messaging.transport.im;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Reaction;

public final class ImTransportInfo$b
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    Object localObject1 = paramParcel;
    k.b(paramParcel, "in");
    ImTransportInfo localImTransportInfo = new com/truecaller/messaging/transport/im/ImTransportInfo;
    long l1 = paramParcel.readLong();
    String str = paramParcel.readString();
    int i = paramParcel.readInt();
    int j = paramParcel.readInt();
    int k = paramParcel.readInt();
    int m = paramParcel.readInt();
    int n = paramParcel.readInt();
    int i1 = paramParcel.readInt();
    int i2 = paramParcel.readInt();
    long l2 = paramParcel.readLong();
    int i3 = paramParcel.readInt();
    if (i3 != 0)
    {
      i3 = paramParcel.readInt();
      arrayOfReaction = new Reaction[i3];
      i4 = 0;
      l3 = l2;
      int i5 = 0;
      while (i3 > i5)
      {
        Object localObject2 = ImTransportInfo.class.getClassLoader();
        localObject2 = (Reaction)((Parcel)localObject1).readParcelable((ClassLoader)localObject2);
        arrayOfReaction[i5] = localObject2;
        i5 += 1;
      }
    }
    long l3 = l2;
    i3 = 0;
    Object localObject3 = null;
    Reaction[] arrayOfReaction = null;
    int i4 = paramParcel.readInt();
    localObject3 = ImTransportInfo.class.getClassLoader();
    localObject1 = (Participant)((Parcel)localObject1).readParcelable((ClassLoader)localObject3);
    localObject3 = localImTransportInfo;
    l2 = l3;
    localImTransportInfo.<init>(l1, str, i, j, k, m, n, i1, i2, l3, arrayOfReaction, i4, (Participant)localObject1);
    return localImTransportInfo;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new ImTransportInfo[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ImTransportInfo.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */