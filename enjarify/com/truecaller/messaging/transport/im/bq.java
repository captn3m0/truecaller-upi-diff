package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import java.util.Collection;
import java.util.List;

public final class bq
  implements bp
{
  private final v a;
  
  public bq(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return bp.class.equals(paramClass);
  }
  
  public final w a()
  {
    v localv = a;
    bq.d locald = new com/truecaller/messaging/transport/im/bq$d;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locald.<init>(locale, (byte)0);
    return w.a(localv, locald);
  }
  
  public final w a(long paramLong)
  {
    v localv = a;
    bq.b localb = new com/truecaller/messaging/transport/im/bq$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramLong, (byte)0);
    return w.a(localv, localb);
  }
  
  public final w a(String paramString)
  {
    v localv = a;
    bq.e locale = new com/truecaller/messaging/transport/im/bq$e;
    e locale1 = new com/truecaller/androidactors/e;
    locale1.<init>();
    locale.<init>(locale1, paramString, (byte)0);
    return w.a(localv, locale);
  }
  
  public final w a(Collection paramCollection, boolean paramBoolean)
  {
    v localv = a;
    bq.a locala = new com/truecaller/messaging/transport/im/bq$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramCollection, paramBoolean, (byte)0);
    return w.a(localv, locala);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    v localv = a;
    bq.g localg = new com/truecaller/messaging/transport/im/bq$g;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localg.<init>(locale, paramString1, paramString2, (byte)0);
    localv.a(localg);
  }
  
  public final void a(Collection paramCollection)
  {
    v localv = a;
    bq.h localh = new com/truecaller/messaging/transport/im/bq$h;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localh.<init>(locale, paramCollection, (byte)0);
    localv.a(localh);
  }
  
  public final void a(List paramList)
  {
    v localv = a;
    bq.f localf = new com/truecaller/messaging/transport/im/bq$f;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localf.<init>(locale, paramList, (byte)0);
    localv.a(localf);
  }
  
  public final w b(String paramString)
  {
    v localv = a;
    bq.i locali = new com/truecaller/messaging/transport/im/bq$i;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locali.<init>(locale, paramString, (byte)0);
    return w.a(localv, locali);
  }
  
  public final w c(String paramString)
  {
    v localv = a;
    bq.c localc = new com/truecaller/messaging/transport/im/bq$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, paramString, (byte)0);
    return w.a(localv, localc);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bq
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */