package com.truecaller.messaging.transport.im;

import android.util.Base64;
import c.g.b.k;
import com.google.firebase.messaging.RemoteMessage;
import com.truecaller.api.services.messenger.v1.events.Event;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.h;
import com.truecaller.wizard.b.c;
import java.util.Map;

public final class aq
  implements ap
{
  private final h a;
  private final bw b;
  private final m c;
  private final bu d;
  
  public aq(h paramh, bw parambw, m paramm, bu parambu)
  {
    a = paramh;
    b = parambw;
    c = paramm;
    d = parambu;
  }
  
  public final void a(RemoteMessage paramRemoteMessage)
  {
    k.b(paramRemoteMessage, "remoteMessage");
    Object localObject = b;
    boolean bool = ((bw)localObject).a();
    if (bool)
    {
      bool = c.e();
      if (bool)
      {
        localObject = d;
        bool = ((bu)localObject).b();
        if (!bool)
        {
          a.H();
          paramRemoteMessage = Event.a(Base64.decode((String)paramRemoteMessage.a().get("payload"), 0));
          localObject = c;
          k.a(paramRemoteMessage, "event");
          ((m)localObject).a(paramRemoteMessage, true);
          return;
        }
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.aq
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */