package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class bq$i
  extends u
{
  private final String b;
  
  private bq$i(e parame, String paramString)
  {
    super(parame);
    b = paramString;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".verifyUserImStatus(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bq.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */