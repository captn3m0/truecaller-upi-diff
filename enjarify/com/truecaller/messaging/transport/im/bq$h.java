package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.Collection;

final class bq$h
  extends u
{
  private final Collection b;
  
  private bq$h(e parame, Collection paramCollection)
  {
    super(parame);
    b = paramCollection;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".updateImUsers(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bq.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */