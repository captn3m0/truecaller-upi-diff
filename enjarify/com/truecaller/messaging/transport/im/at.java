package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.types.Reaction;

public final class at
  implements as
{
  private final v a;
  
  public at(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return as.class.equals(paramClass);
  }
  
  public final w a(Reaction[] paramArrayOfReaction)
  {
    v localv = a;
    at.g localg = new com/truecaller/messaging/transport/im/at$g;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localg.<init>(locale, paramArrayOfReaction, (byte)0);
    return w.a(localv, localg);
  }
  
  public final void a()
  {
    v localv = a;
    at.f localf = new com/truecaller/messaging/transport/im/at$f;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localf.<init>(locale, (byte)0);
    localv.a(localf);
  }
  
  public final void a(long paramLong)
  {
    v localv = a;
    at.d locald = new com/truecaller/messaging/transport/im/at$d;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locald.<init>(locale, paramLong, (byte)0);
    localv.a(locald);
  }
  
  public final void a(long[] paramArrayOfLong)
  {
    v localv = a;
    at.e locale = new com/truecaller/messaging/transport/im/at$e;
    e locale1 = new com/truecaller/androidactors/e;
    locale1.<init>();
    locale.<init>(locale1, paramArrayOfLong, (byte)0);
    localv.a(locale);
  }
  
  public final void b(long paramLong)
  {
    v localv = a;
    at.c localc = new com/truecaller/messaging/transport/im/at$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, paramLong, (byte)0);
    localv.a(localc);
  }
  
  public final void c(long paramLong)
  {
    v localv = a;
    at.h localh = new com/truecaller/messaging/transport/im/at$h;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localh.<init>(locale, paramLong, (byte)0);
    localv.a(localh);
  }
  
  public final w d(long paramLong)
  {
    v localv = a;
    at.a locala = new com/truecaller/messaging/transport/im/at$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramLong, (byte)0);
    return w.a(localv, locala);
  }
  
  public final w e(long paramLong)
  {
    v localv = a;
    at.b localb = new com/truecaller/messaging/transport/im/at$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramLong, (byte)0);
    return w.a(localv, localb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.at
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */