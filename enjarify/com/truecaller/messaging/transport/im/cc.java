package com.truecaller.messaging.transport.im;

import c.a.m;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.k.a;
import com.truecaller.api.services.messenger.v1.l.b;
import com.truecaller.api.services.messenger.v1.l.d;
import com.truecaller.common.account.r;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.messaging.h;
import com.truecaller.util.bt;
import io.grpc.ao;
import io.grpc.ao.b;
import io.grpc.ao.e;
import io.grpc.ba;
import io.grpc.ba.a;
import io.grpc.bc;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

public final class cc
  extends com.truecaller.network.d.k
  implements cb
{
  private final com.truecaller.network.a.d a;
  private final h b;
  private final com.truecaller.analytics.b c;
  private final com.truecaller.utils.i d;
  private final dagger.a e;
  private final bu f;
  private final bt g;
  
  public cc(r paramr, com.truecaller.common.account.k paramk, com.truecaller.utils.d paramd, com.truecaller.network.d.e parame, boolean paramBoolean, String paramString, com.truecaller.network.d.b paramb, com.truecaller.common.edge.a parama, com.truecaller.common.network.c paramc, com.truecaller.d.b paramb1, com.truecaller.common.h.i parami, com.truecaller.network.a.d paramd1, h paramh, com.truecaller.analytics.b paramb2, com.truecaller.utils.i parami1, dagger.a parama1, bu parambu, bt parambt)
  {
    super(localKnownEndpoints, paramr, paramk, paramd, localInteger, paramb, parama, paramc, paramBoolean, parame, paramString, paramb1, parami);
    a = paramd1;
    b = paramh;
    c = paramb2;
    d = parami1;
    e = parama1;
    f = parambu;
    g = parambt;
  }
  
  private final io.grpc.c.a a(io.grpc.c.a parama)
  {
    ao localao = new io/grpc/ao;
    localao.<init>();
    Object localObject1 = ao.b;
    Object localObject2 = ao.e.a("Version", (ao.b)localObject1);
    localObject1 = String.valueOf(f.a());
    localao.a((ao.e)localObject2, localObject1);
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (List)localObject2;
    localObject1 = g;
    boolean bool = ((bt)localObject1).c();
    if (bool)
    {
      localObject1 = "versioning";
      ((List)localObject2).add(localObject1);
    }
    localObject1 = g;
    bool = ((bt)localObject1).d();
    if (bool)
    {
      localObject1 = "commands";
      ((List)localObject2).add(localObject1);
    }
    localObject1 = g;
    bool = ((bt)localObject1).e();
    if (bool)
    {
      localObject1 = "no-user-info";
      ((List)localObject2).add(localObject1);
    }
    localObject1 = b;
    bool = ((h)localObject1).M();
    if (bool)
    {
      localObject1 = "tracing";
      ((List)localObject2).add(localObject1);
    }
    localObject1 = localObject2;
    localObject1 = (Collection)localObject2;
    bool = ((Collection)localObject1).isEmpty() ^ true;
    if (bool)
    {
      Object localObject3 = localObject2;
      localObject3 = (Iterable)localObject2;
      CharSequence localCharSequence = (CharSequence)",";
      int i = 62;
      localObject2 = m.a((Iterable)localObject3, localCharSequence, null, null, 0, null, null, i);
      localObject3 = ao.b;
      localObject1 = ao.e.a("Debug", (ao.b)localObject3);
      localao.a((ao.e)localObject1, localObject2);
    }
    parama = io.grpc.c.e.a(parama, localao);
    c.g.b.k.a(parama, "MetadataUtils.attachHeaders(this, metadata)");
    return parama;
  }
  
  private final String b()
  {
    com.truecaller.utils.i locali = d;
    boolean bool = locali.a();
    if (bool) {
      return d.b();
    }
    return "no-connection";
  }
  
  private final boolean c()
  {
    try
    {
      Object localObject1 = b;
      localObject1 = ((h)localObject1).G();
      localObject1 = (CharSequence)localObject1;
      boolean bool1 = false;
      Object localObject3 = null;
      boolean bool2 = true;
      if (localObject1 != null)
      {
        i = ((CharSequence)localObject1).length();
        if (i != 0)
        {
          i = 0;
          localObject1 = null;
          break label57;
        }
      }
      int i = 1;
      label57:
      if (i != 0)
      {
        bool3 = d();
        if (!bool3) {
          return false;
        }
      }
      localObject1 = b;
      boolean bool3 = ((h)localObject1).Q();
      if (!bool3)
      {
        localObject1 = b;
        localObject3 = e;
        localObject3 = ((dagger.a)localObject3).get();
        localObject3 = (com.truecaller.androidactors.f)localObject3;
        localObject3 = ((com.truecaller.androidactors.f)localObject3).a();
        localObject3 = (com.truecaller.presence.c)localObject3;
        localObject3 = ((com.truecaller.presence.c)localObject3).a();
        localObject3 = ((w)localObject3).d();
        localObject3 = (Boolean)localObject3;
        Boolean localBoolean = Boolean.TRUE;
        bool1 = c.g.b.k.a(localObject3, localBoolean);
        ((h)localObject1).h(bool1);
      }
      return bool2;
    }
    finally {}
  }
  
  private final boolean d()
  {
    Object localObject1 = (com.truecaller.common.network.e)com.truecaller.common.network.e.a.a;
    localObject1 = (k.a)super.b((com.truecaller.common.network.e)localObject1);
    if (localObject1 == null) {
      return false;
    }
    Object localObject2 = c;
    Object localObject3 = new com/truecaller/analytics/e$a;
    ((com.truecaller.analytics.e.a)localObject3).<init>("IMRequest");
    localObject3 = ((com.truecaller.analytics.e.a)localObject3).a("Type", "GetPeerImId");
    Object localObject4 = b();
    localObject3 = ((com.truecaller.analytics.e.a)localObject3).a("ConnectionType", (String)localObject4);
    localObject4 = "Attempt";
    localObject3 = ((com.truecaller.analytics.e.a)localObject3).a("Status", (String)localObject4).a();
    Object localObject5 = "AnalyticsEvent.Builder(A…\n                .build()";
    c.g.b.k.a(localObject3, (String)localObject5);
    ((com.truecaller.analytics.b)localObject2).a((com.truecaller.analytics.e)localObject3);
    boolean bool1 = true;
    try
    {
      localObject3 = l.b.a();
      localObject1 = ((k.a)localObject1).a((l.b)localObject3);
      localObject3 = b;
      if (localObject1 != null) {
        localObject1 = ((l.d)localObject1).a();
      } else {
        localObject1 = null;
      }
      ((h)localObject3).d((String)localObject1);
      localObject1 = b;
      ((h)localObject1).h(false);
      return bool1;
    }
    catch (RuntimeException localRuntimeException)
    {
      localObject3 = localRuntimeException;
      localObject3 = (Throwable)localRuntimeException;
      boolean bool2 = localObject3 instanceof bc;
      if (bool2)
      {
        localObject5 = c;
        localObject4 = new com/truecaller/analytics/e$a;
        ((com.truecaller.analytics.e.a)localObject4).<init>("IMRequest");
        localObject4 = ((com.truecaller.analytics.e.a)localObject4).a("Type", "GetPeerImId");
        String str2 = b();
        localObject4 = ((com.truecaller.analytics.e.a)localObject4).a("ConnectionType", str2).a("Status", "Failure");
        String str3 = "ErrorCode";
        localObject3 = ((bc)localObject3).a();
        str2 = "error.status";
        c.g.b.k.a(localObject3, str2);
        localObject3 = ((ba)localObject3).a().name();
        localObject3 = ((com.truecaller.analytics.e.a)localObject4).a(str3, (String)localObject3).a();
        localObject4 = "AnalyticsEvent.Builder(A…status.code.name).build()";
        c.g.b.k.a(localObject3, (String)localObject4);
        ((com.truecaller.analytics.b)localObject5).a((com.truecaller.analytics.e)localObject3);
      }
      localObject2 = new String[bool1];
      String str1 = String.valueOf(localRuntimeException);
      str1 = "Failed to register to IM: ".concat(str1);
      localObject2[0] = str1;
    }
    return false;
  }
  
  public final Collection a()
  {
    com.truecaller.network.d.f localf = new com/truecaller/network/d/f;
    com.truecaller.network.a.d locald = a;
    h localh = b;
    localf.<init>(locald, localh);
    return (Collection)m.a(localf);
  }
  
  public final void a(io.grpc.okhttp.e parame)
  {
    c.g.b.k.b(parame, "builder");
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    parame.b(localTimeUnit);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.cc
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */