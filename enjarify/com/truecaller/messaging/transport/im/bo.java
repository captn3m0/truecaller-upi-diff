package com.truecaller.messaging.transport.im;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.api.services.messenger.v1.MediaHandles.Request;
import com.truecaller.api.services.messenger.v1.MediaHandles.Request.a;
import com.truecaller.api.services.messenger.v1.MediaHandles.c;
import com.truecaller.api.services.messenger.v1.k.a;
import com.truecaller.log.AssertionUtil;
import com.truecaller.network.d.j.a;
import com.truecaller.utils.extensions.r;
import java.io.IOException;
import java.util.Map;
import okhttp3.y;

public final class bo
  implements bn
{
  private final cb a;
  private final ContentResolver b;
  private final y c;
  private final Context d;
  
  public bo(cb paramcb, ContentResolver paramContentResolver, y paramy, Context paramContext)
  {
    a = paramcb;
    b = paramContentResolver;
    c = paramy;
    d = paramContext;
  }
  
  /* Error */
  private final boolean a(Map paramMap, String paramString1, String paramString2, Uri paramUri)
  {
    // Byte code:
    //   0: aload 4
    //   2: invokevirtual 45	android/net/Uri:getPathSegments	()Ljava/util/List;
    //   5: astore 5
    //   7: aload 5
    //   9: ldc 47
    //   11: invokestatic 49	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   14: aload 5
    //   16: invokestatic 55	c/a/m:f	(Ljava/util/List;)Ljava/lang/Object;
    //   19: checkcast 57	java/lang/String
    //   22: astore 5
    //   24: new 59	okhttp3/x$a
    //   27: astore 6
    //   29: aload 6
    //   31: invokespecial 60	okhttp3/x$a:<init>	()V
    //   34: getstatic 66	okhttp3/x:e	Lokhttp3/w;
    //   37: astore 7
    //   39: aload 6
    //   41: aload 7
    //   43: invokevirtual 69	okhttp3/x$a:a	(Lokhttp3/w;)Lokhttp3/x$a;
    //   46: astore 6
    //   48: aload_1
    //   49: invokeinterface 75 1 0
    //   54: invokeinterface 81 1 0
    //   59: astore_1
    //   60: aload_1
    //   61: invokeinterface 87 1 0
    //   66: istore 8
    //   68: iload 8
    //   70: ifeq +51 -> 121
    //   73: aload_1
    //   74: invokeinterface 91 1 0
    //   79: checkcast 93	java/util/Map$Entry
    //   82: astore 7
    //   84: aload 7
    //   86: invokeinterface 96 1 0
    //   91: checkcast 57	java/lang/String
    //   94: astore 9
    //   96: aload 7
    //   98: invokeinterface 99 1 0
    //   103: checkcast 57	java/lang/String
    //   106: astore 7
    //   108: aload 6
    //   110: aload 9
    //   112: aload 7
    //   114: invokevirtual 102	okhttp3/x$a:a	(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/x$a;
    //   117: pop
    //   118: goto -58 -> 60
    //   121: new 106	com/truecaller/messaging/transport/im/bo$a
    //   124: astore 7
    //   126: aload_0
    //   127: getfield 35	com/truecaller/messaging/transport/im/bo:b	Landroid/content/ContentResolver;
    //   130: astore 9
    //   132: aload 7
    //   134: aload 9
    //   136: aload_3
    //   137: aload 4
    //   139: invokespecial 109	com/truecaller/messaging/transport/im/bo$a:<init>	(Landroid/content/ContentResolver;Ljava/lang/String;Landroid/net/Uri;)V
    //   142: aload 7
    //   144: checkcast 111	okhttp3/ac
    //   147: astore 7
    //   149: aload 6
    //   151: ldc 104
    //   153: aload 5
    //   155: aload 7
    //   157: invokevirtual 114	okhttp3/x$a:a	(Ljava/lang/String;Ljava/lang/String;Lokhttp3/ac;)Lokhttp3/x$a;
    //   160: invokevirtual 117	okhttp3/x$a:a	()Lokhttp3/x;
    //   163: astore_1
    //   164: aload_1
    //   165: ldc 119
    //   167: invokestatic 49	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   170: aload_1
    //   171: checkcast 111	okhttp3/ac
    //   174: astore_1
    //   175: new 121	okhttp3/ab$a
    //   178: astore_3
    //   179: aload_3
    //   180: invokespecial 122	okhttp3/ab$a:<init>	()V
    //   183: aload_3
    //   184: aload_2
    //   185: invokevirtual 125	okhttp3/ab$a:a	(Ljava/lang/String;)Lokhttp3/ab$a;
    //   188: aload 5
    //   190: invokevirtual 128	okhttp3/ab$a:a	(Ljava/lang/Object;)Lokhttp3/ab$a;
    //   193: aload_1
    //   194: invokevirtual 131	okhttp3/ab$a:a	(Lokhttp3/ac;)Lokhttp3/ab$a;
    //   197: invokevirtual 134	okhttp3/ab$a:a	()Lokhttp3/ab;
    //   200: astore_1
    //   201: aload_0
    //   202: getfield 37	com/truecaller/messaging/transport/im/bo:c	Lokhttp3/y;
    //   205: aload_1
    //   206: invokevirtual 139	okhttp3/y:a	(Lokhttp3/ab;)Lokhttp3/e;
    //   209: astore_1
    //   210: aload_1
    //   211: ldc -115
    //   213: invokestatic 49	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   216: aconst_null
    //   217: astore_2
    //   218: aload_1
    //   219: invokestatic 147	com/google/firebase/perf/network/FirebasePerfOkHttpClient:execute	(Lokhttp3/e;)Lokhttp3/ad;
    //   222: astore_1
    //   223: aload_1
    //   224: checkcast 149	java/io/Closeable
    //   227: astore_1
    //   228: aconst_null
    //   229: astore_3
    //   230: aload_1
    //   231: astore 4
    //   233: aload_1
    //   234: checkcast 151	okhttp3/ad
    //   237: astore 4
    //   239: iconst_1
    //   240: istore 10
    //   242: aload 4
    //   244: ifnull +20 -> 264
    //   247: aload 4
    //   249: invokevirtual 154	okhttp3/ad:c	()Z
    //   252: istore 11
    //   254: iload 11
    //   256: iload 10
    //   258: if_icmpne +6 -> 264
    //   261: goto +9 -> 270
    //   264: iconst_0
    //   265: istore 10
    //   267: aconst_null
    //   268: astore 5
    //   270: aload_1
    //   271: aconst_null
    //   272: invokestatic 159	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   275: iload 10
    //   277: ireturn
    //   278: astore 4
    //   280: goto +6 -> 286
    //   283: astore_3
    //   284: aload_3
    //   285: athrow
    //   286: aload_1
    //   287: aload_3
    //   288: invokestatic 159	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   291: aload 4
    //   293: athrow
    //   294: checkcast 161	java/lang/Throwable
    //   297: invokestatic 166	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   300: iconst_0
    //   301: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	302	0	this	bo
    //   0	302	1	paramMap	Map
    //   0	302	2	paramString1	String
    //   0	302	3	paramString2	String
    //   0	302	4	paramUri	Uri
    //   5	264	5	localObject1	Object
    //   27	123	6	locala	okhttp3.x.a
    //   37	119	7	localObject2	Object
    //   66	3	8	bool1	boolean
    //   94	41	9	localObject3	Object
    //   240	36	10	bool2	boolean
    //   252	7	11	bool3	boolean
    //   294	1	12	localIOException	IOException
    // Exception table:
    //   from	to	target	type
    //   284	286	278	finally
    //   233	237	283	finally
    //   247	252	283	finally
    //   218	222	294	java/io/IOException
    //   223	227	294	java/io/IOException
    //   271	275	294	java/io/IOException
    //   287	291	294	java/io/IOException
    //   291	294	294	java/io/IOException
  }
  
  public final ck a(Uri paramUri)
  {
    int i = 2131886744;
    int j = 2;
    if (paramUri == null)
    {
      paramUri = new com/truecaller/messaging/transport/im/ck;
      localInteger = Integer.valueOf(i);
      paramUri.<init>(false, null, localInteger, j);
      return paramUri;
    }
    Object localObject1 = (k.a)j.a.a(a);
    if (localObject1 == null)
    {
      paramUri = new com/truecaller/messaging/transport/im/ck;
      localInteger = Integer.valueOf(i);
      paramUri.<init>(false, null, localInteger, j);
      return paramUri;
    }
    Object localObject2 = TrueApp.x();
    String str = "getAppContext()";
    k.a(localObject2, str);
    localObject2 = r.a(paramUri, (Context)localObject2);
    if (localObject2 != null)
    {
      long l = ((Long)localObject2).longValue();
      Object localObject3 = d;
      localObject3 = r.c(paramUri, (Context)localObject3);
      if (localObject3 == null)
      {
        paramUri = new com/truecaller/messaging/transport/im/ck;
        localInteger = Integer.valueOf(2131886747);
        paramUri.<init>(false, null, localInteger, j);
        return paramUri;
      }
      try
      {
        Object localObject4 = MediaHandles.Request.a();
        localObject2 = ((MediaHandles.Request.a)localObject4).a(l);
        localObject2 = ((MediaHandles.Request.a)localObject2).a((String)localObject3);
        localObject2 = ((MediaHandles.Request.a)localObject2).build();
        localObject2 = (MediaHandles.Request)localObject2;
        localObject1 = ((k.a)localObject1).a((MediaHandles.Request)localObject2);
        localObject2 = "stub.getMediaHandles(request)";
        k.a(localObject1, (String)localObject2);
        localObject2 = ((MediaHandles.c)localObject1).c();
        str = "result.formFieldsMap";
        k.a(localObject2, str);
        str = ((MediaHandles.c)localObject1).a();
        localObject4 = "result.uploadUrl";
        k.a(str, (String)localObject4);
        boolean bool1 = a((Map)localObject2, str, (String)localObject3, paramUri);
        if (bool1)
        {
          paramUri = new com/truecaller/messaging/transport/im/ck;
          boolean bool2 = true;
          localObject1 = ((MediaHandles.c)localObject1).b();
          int k = 4;
          paramUri.<init>(bool2, (String)localObject1, null, k);
        }
        else
        {
          paramUri = new com/truecaller/messaging/transport/im/ck;
          localObject1 = Integer.valueOf(i);
          paramUri.<init>(false, null, (Integer)localObject1, j);
        }
      }
      catch (RuntimeException localRuntimeException)
      {
        paramUri = new com/truecaller/messaging/transport/im/ck;
        localInteger = Integer.valueOf(i);
        paramUri.<init>(false, null, localInteger, j);
      }
      catch (IOException localIOException)
      {
        AssertionUtil.reportThrowableButNeverCrash((Throwable)localIOException);
        paramUri = new com/truecaller/messaging/transport/im/ck;
        localInteger = Integer.valueOf(i);
        paramUri.<init>(false, null, localInteger, j);
      }
      return paramUri;
    }
    paramUri = new com/truecaller/messaging/transport/im/ck;
    Integer localInteger = Integer.valueOf(2131886745);
    paramUri.<init>(false, null, localInteger, j);
    return paramUri;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */