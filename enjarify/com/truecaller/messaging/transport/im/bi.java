package com.truecaller.messaging.transport.im;

import dagger.a.d;
import javax.inject.Provider;

public final class bi
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  private final Provider g;
  private final Provider h;
  private final Provider i;
  private final Provider j;
  private final Provider k;
  private final Provider l;
  private final Provider m;
  private final Provider n;
  private final Provider o;
  private final Provider p;
  private final Provider q;
  private final Provider r;
  private final Provider s;
  private final Provider t;
  private final Provider u;
  private final Provider v;
  private final Provider w;
  private final Provider x;
  
  private bi(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7, Provider paramProvider8, Provider paramProvider9, Provider paramProvider10, Provider paramProvider11, Provider paramProvider12, Provider paramProvider13, Provider paramProvider14, Provider paramProvider15, Provider paramProvider16, Provider paramProvider17, Provider paramProvider18, Provider paramProvider19, Provider paramProvider20, Provider paramProvider21, Provider paramProvider22, Provider paramProvider23, Provider paramProvider24)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
    e = paramProvider5;
    f = paramProvider6;
    g = paramProvider7;
    h = paramProvider8;
    i = paramProvider9;
    j = paramProvider10;
    k = paramProvider11;
    l = paramProvider12;
    m = paramProvider13;
    n = paramProvider14;
    o = paramProvider15;
    p = paramProvider16;
    q = paramProvider17;
    r = paramProvider18;
    s = paramProvider19;
    t = paramProvider20;
    u = paramProvider21;
    v = paramProvider22;
    w = paramProvider23;
    x = paramProvider24;
  }
  
  public static bi a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7, Provider paramProvider8, Provider paramProvider9, Provider paramProvider10, Provider paramProvider11, Provider paramProvider12, Provider paramProvider13, Provider paramProvider14, Provider paramProvider15, Provider paramProvider16, Provider paramProvider17, Provider paramProvider18, Provider paramProvider19, Provider paramProvider20, Provider paramProvider21, Provider paramProvider22, Provider paramProvider23, Provider paramProvider24)
  {
    bi localbi = new com/truecaller/messaging/transport/im/bi;
    localbi.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13, paramProvider14, paramProvider15, paramProvider16, paramProvider17, paramProvider18, paramProvider19, paramProvider20, paramProvider21, paramProvider22, paramProvider23, paramProvider24);
    return localbi;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bi
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */