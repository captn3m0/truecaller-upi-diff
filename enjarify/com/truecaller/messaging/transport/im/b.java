package com.truecaller.messaging.transport.im;

public final class b
  implements a
{
  private long a;
  private long b;
  private final long c = 1000L;
  private final long d = 10000L;
  private final long e = 5000L;
  private final long f = 30000L;
  
  private b() {}
  
  public b(char paramChar)
  {
    this((byte)0);
  }
  
  public final long a(long paramLong, boolean paramBoolean)
  {
    if (paramBoolean) {
      l1 = f;
    } else {
      l1 = d;
    }
    long l2 = b;
    l2 = paramLong - l2;
    double d1 = 1.0E-323D;
    long l3 = 2 * l1;
    boolean bool = l2 < l3;
    if (bool)
    {
      l2 = 0L;
      d2 = 0.0D;
      a = l2;
    }
    if (paramBoolean) {
      l2 = e;
    } else {
      l2 = c;
    }
    double d2 = l2;
    double d3 = a;
    d1 = Math.pow(2.0D, d3);
    Double.isNaN(d2);
    d2 *= d1;
    double d4 = l1;
    long l1 = Math.min(d2, d4);
    l2 = a + 1L;
    a = l2;
    b = paramLong;
    return l1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */