package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.api.services.messenger.v1.events.Event;

final class bk$b
  extends u
{
  private final Event b;
  private final int c;
  
  private bk$b(e parame, Event paramEvent, int paramInt)
  {
    super(parame);
    b = paramEvent;
    c = paramInt;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".saveUnsupportedEvent(");
    Object localObject = b;
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(c), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bk.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */