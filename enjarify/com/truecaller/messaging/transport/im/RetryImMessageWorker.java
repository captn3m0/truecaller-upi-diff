package com.truecaller.messaging.transport.im;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.e;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.f;
import com.truecaller.bp;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.data.t;
import org.a.a.b;

public final class RetryImMessageWorker
  extends Worker
{
  public static final RetryImMessageWorker.a d;
  public bw b;
  public f c;
  
  static
  {
    RetryImMessageWorker.a locala = new com/truecaller/messaging/transport/im/RetryImMessageWorker$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  public RetryImMessageWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      str1 = "imStatusProvider";
      k.a(str1);
    }
    boolean bool1 = ((bw)localObject1).a();
    if (!bool1)
    {
      localObject1 = ListenableWorker.a.a();
      k.a(localObject1, "Result.success()");
      return (ListenableWorker.a)localObject1;
    }
    localObject1 = getInputData();
    String str1 = "to_date";
    long l1 = 0L;
    long l2 = ((e)localObject1).a(str1, l1);
    boolean bool2 = l2 < l1;
    if (!bool2)
    {
      localObject1 = ListenableWorker.a.c();
      k.a(localObject1, "Result.failure()");
      return (ListenableWorker.a)localObject1;
    }
    Object localObject2 = c;
    if (localObject2 == null)
    {
      String str2 = "messagesStorage";
      k.a(str2);
    }
    localObject2 = (t)((f)localObject2).a();
    b localb = new org/a/a/b;
    localb.<init>(l2);
    ((t)localObject2).a(2, localb);
    localObject1 = ListenableWorker.a.a();
    k.a(localObject1, "Result.success()");
    return (ListenableWorker.a)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.RetryImMessageWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */