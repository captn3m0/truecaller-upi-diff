package com.truecaller.messaging.transport.im;

import dagger.a.d;
import javax.inject.Provider;

public final class bm
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private bm(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static bm a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    bm localbm = new com/truecaller/messaging/transport/im/bm;
    localbm.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localbm;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bm
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */