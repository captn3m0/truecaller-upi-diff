package com.truecaller.messaging.transport.im;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Parcelable;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.g;
import c.g.b.k;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.common.h.an;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Participant.a;
import com.truecaller.messaging.h;
import com.truecaller.notificationchannels.e;
import com.truecaller.notifications.a;
import com.truecaller.notifications.l;
import com.truecaller.notifications.l.a;
import com.truecaller.util.af;
import com.truecaller.util.al;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.a.a.a.g;

public final class bz
  implements by
{
  final al a;
  final Context b;
  private final bw c;
  private final f d;
  private final h e;
  private final af f;
  private final a g;
  private final l h;
  private final e i;
  private final com.truecaller.common.h.u j;
  private final an k;
  
  public bz(bw parambw, f paramf, al paramal, h paramh, af paramaf, Context paramContext, a parama, l paraml, e parame, com.truecaller.common.h.u paramu, an paraman)
  {
    c = parambw;
    d = paramf;
    a = paramal;
    e = paramh;
    f = paramaf;
    b = paramContext;
    g = parama;
    h = paraml;
    i = parame;
    j = paramu;
    k = paraman;
  }
  
  private final Participant a(bx parambx)
  {
    Object localObject1 = b;
    Object localObject2 = j;
    String str = ((com.truecaller.common.h.u)localObject2).a();
    localObject1 = Participant.a((String)localObject1, (com.truecaller.common.h.u)localObject2, str).l();
    localObject2 = d;
    if (localObject2 != null)
    {
      localObject2 = (Number)localObject2;
      long l = ((Number)localObject2).longValue();
      ((Participant.a)localObject1).c(l);
    }
    localObject2 = c;
    if (localObject2 != null) {
      ((Participant.a)localObject1).g((String)localObject2);
    }
    parambx = a;
    if (parambx != null) {
      ((Participant.a)localObject1).f(parambx);
    }
    return ((Participant.a)localObject1).a();
  }
  
  public final boolean a()
  {
    org.a.a.b localb1 = new org/a/a/b;
    localb1.<init>();
    localb1 = localb1.az_();
    k.a(localb1, "DateTime().withTimeAtStartOfDay()");
    Object localObject1 = f;
    org.a.a.b localb2 = ((af)localObject1).b();
    org.a.a.b localb3 = localb1.b(22);
    Object localObject2 = "startOfDay.plusHours(22)";
    k.a(localb3, (String)localObject2);
    boolean bool1 = ((af)localObject1).a(localb2, localb3);
    boolean bool2 = true;
    localb3 = null;
    if (bool1)
    {
      localObject1 = f;
      localObject2 = ((af)localObject1).b();
      int m = 18;
      localb1 = localb1.b(m);
      String str = "startOfDay.plusHours(18)";
      k.a(localb1, str);
      bool3 = ((af)localObject1).b((org.a.a.b)localObject2, localb1);
      if (bool3)
      {
        bool3 = true;
        break label133;
      }
    }
    boolean bool3 = false;
    localb1 = null;
    label133:
    localObject1 = e.h();
    localObject2 = "settings.lastJoinUserNotificationDate";
    k.a(localObject1, (String)localObject2);
    long l1 = a;
    long l2 = 0L;
    boolean bool4 = l1 < l2;
    if (bool4)
    {
      an localan = k;
      long l3 = a;
      long l4 = 7;
      TimeUnit localTimeUnit = TimeUnit.DAYS;
      bool1 = localan.a(l3, l4, localTimeUnit);
      if (!bool1)
      {
        bool1 = false;
        localObject1 = null;
        break label228;
      }
    }
    bool1 = true;
    label228:
    localObject2 = c;
    boolean bool5 = ((bw)localObject2).a();
    if (bool5)
    {
      localObject2 = a;
      bool5 = ((al)localObject2).c();
      if (bool5)
      {
        localObject2 = e;
        l1 = ((h)localObject2).R();
        bool4 = l1 < l2;
        if ((bool4) && (bool3) && (bool1)) {
          return bool2;
        }
      }
    }
    return false;
  }
  
  public final void b()
  {
    Object localObject1 = e;
    long l1 = ((h)localObject1).R();
    Object localObject2 = f;
    long l2 = ((af)localObject2).c();
    boolean bool1 = l1 < l2;
    Object localObject3;
    if (bool1)
    {
      l1 = 0L;
      localObject2 = e;
      localObject3 = f;
      long l3 = ((af)localObject3).c();
      ((h)localObject2).f(l3);
    }
    localObject2 = (bp)d.a();
    localObject1 = (Collection)((bp)localObject2).a(l1).d();
    if (localObject1 != null)
    {
      boolean bool2 = ((Collection)localObject1).isEmpty();
      int n = 1;
      bool2 ^= n;
      if (bool2 == n)
      {
        Object localObject4 = e;
        localObject3 = f.b();
        ((h)localObject4).b((org.a.a.b)localObject3);
        localObject4 = b.getResources();
        if (localObject4 != null)
        {
          localObject3 = localObject1;
          localObject3 = (Iterable)localObject1;
          Object localObject5 = ((Iterable)localObject3).iterator();
          label273:
          int i5;
          label322:
          do
          {
            bool3 = ((Iterator)localObject5).hasNext();
            i1 = 0;
            localObject6 = null;
            if (!bool3) {
              break;
            }
            localObject7 = ((Iterator)localObject5).next();
            localObject8 = localObject7;
            localObject8 = (bx)localObject7;
            localObject9 = (CharSequence)c;
            if (localObject9 != null)
            {
              i4 = ((CharSequence)localObject9).length();
              if (i4 != 0)
              {
                i4 = 0;
                localObject9 = null;
                break label273;
              }
            }
            i4 = 1;
            if (i4 == 0)
            {
              localObject8 = (CharSequence)a;
              if (localObject8 != null)
              {
                i5 = ((CharSequence)localObject8).length();
                if (i5 != 0)
                {
                  i5 = 0;
                  localObject8 = null;
                  break label322;
                }
              }
              i5 = 1;
              if (i5 == 0)
              {
                i5 = 1;
                continue;
              }
            }
            i5 = 0;
            localObject8 = null;
          } while (i5 == 0);
          break label353;
          boolean bool3 = false;
          Object localObject7 = null;
          label353:
          localObject7 = (bx)localObject7;
          if (localObject7 == null)
          {
            localObject5 = ((Iterable)localObject3).iterator();
            label448:
            do
            {
              bool3 = ((Iterator)localObject5).hasNext();
              if (!bool3) {
                break;
              }
              localObject7 = ((Iterator)localObject5).next();
              localObject8 = localObject7;
              localObject8 = (CharSequence)a;
              if (localObject8 != null)
              {
                i5 = ((CharSequence)localObject8).length();
                if (i5 != 0)
                {
                  i5 = 0;
                  localObject8 = null;
                  break label448;
                }
              }
              i5 = 1;
              i5 ^= n;
            } while (i5 == 0);
            break label469;
            bool3 = false;
            localObject7 = null;
            label469:
            localObject7 = (bx)localObject7;
          }
          if (localObject7 == null)
          {
            localObject5 = c.a.m.b((Iterable)localObject3);
            localObject7 = localObject5;
            localObject7 = (bx)localObject5;
          }
          localObject5 = a;
          if (localObject5 != null)
          {
            localObject5 = (CharSequence)localObject5;
            localObject6 = new String[] { " " };
            i6 = 6;
            localObject5 = c.n.m.c((CharSequence)localObject5, (String[])localObject6, false, i6);
            if (localObject5 != null)
            {
              localObject5 = (String)((List)localObject5).get(0);
              if (localObject5 != null) {
                break label575;
              }
            }
          }
          localObject5 = a;
          label575:
          if (localObject5 == null) {
            localObject5 = b;
          }
          int i1 = ((Collection)localObject1).size();
          int i6 = 2;
          if (i1 == n)
          {
            localObject6 = localObject5;
          }
          else
          {
            i2 = 2131888306;
            localObject9 = new Object[i6];
            localObject9[0] = localObject5;
            int i7 = ((Collection)localObject1).size() - n;
            localObject10 = Integer.valueOf(i7);
            localObject9[n] = localObject10;
            localObject6 = ((Resources)localObject4).getString(i2, (Object[])localObject9);
            localObject9 = "resources.getString(R.st…, joinedImUsers.size - 1)";
            k.a(localObject6, (String)localObject9);
          }
          Object localObject10 = new Object[n];
          localObject10[0] = localObject5;
          localObject5 = ((Resources)localObject4).getString(2131888307, (Object[])localObject10);
          int i4 = 2131888305;
          Object localObject8 = new Object[i6];
          localObject10 = "👋";
          localObject8[0] = localObject10;
          localObject8[n] = localObject6;
          localObject4 = ((Resources)localObject4).getString(i4, (Object[])localObject8);
          int i2 = ((Collection)localObject1).size();
          i6 = 268435456;
          if (i2 == n)
          {
            localObject2 = b;
            localObject3 = (bx)c.a.m.b((Iterable)localObject3);
            localObject3 = a((bx)localObject3);
            k.b(localObject2, "context");
            localObject6 = new android/content/Intent;
            ((Intent)localObject6).<init>((Context)localObject2, JoinedImUsersBroadcastReceiver.class);
            ((Intent)localObject6).setAction("com.truecaller.open_conversation");
            localObject9 = "participant";
            localObject3 = (Parcelable)localObject3;
            ((Intent)localObject6).putExtra((String)localObject9, (Parcelable)localObject3);
            localObject2 = PendingIntent.getBroadcast((Context)localObject2, 0, (Intent)localObject6, i6);
            localObject3 = "PendingIntent.getBroadca…tent.FLAG_CANCEL_CURRENT)";
            k.a(localObject2, (String)localObject3);
          }
          else
          {
            localObject2 = b;
            localObject6 = new java/util/ArrayList;
            ((ArrayList)localObject6).<init>();
            localObject6 = (Collection)localObject6;
            localObject3 = ((Iterable)localObject3).iterator();
            for (;;)
            {
              boolean bool4 = ((Iterator)localObject3).hasNext();
              if (!bool4) {
                break;
              }
              localObject9 = (bx)((Iterator)localObject3).next();
              localObject9 = a((bx)localObject9);
              if (localObject9 != null) {
                ((Collection)localObject6).add(localObject9);
              }
            }
            localObject6 = (Collection)localObject6;
            localObject3 = new Participant[0];
            localObject3 = ((Collection)localObject6).toArray((Object[])localObject3);
            if (localObject3 == null) {
              break label1454;
            }
            localObject3 = (Participant[])localObject3;
            k.b(localObject2, "context");
            k.b(localObject3, "participantList");
            localObject6 = new android/content/Intent;
            ((Intent)localObject6).<init>((Context)localObject2, JoinedImUsersBroadcastReceiver.class);
            ((Intent)localObject6).setAction("com.truecaller.open_new_conversation");
            localObject9 = "participant_list";
            localObject3 = (Parcelable[])localObject3;
            ((Intent)localObject6).putExtra((String)localObject9, (Parcelable[])localObject3);
            localObject2 = PendingIntent.getBroadcast((Context)localObject2, 0, (Intent)localObject6, i6);
            localObject3 = "PendingIntent.getBroadca…tent.FLAG_CANCEL_CURRENT)";
            k.a(localObject2, (String)localObject3);
          }
          localObject3 = g.a((PendingIntent)localObject2, "notificationJoinedImUsers", "Opened");
          Object localObject6 = i.a();
          localObject8 = new android/support/v4/app/z$d;
          Object localObject9 = b;
          ((z.d)localObject8).<init>((Context)localObject9, (String)localObject6);
          localObject5 = (CharSequence)localObject5;
          localObject5 = ((z.d)localObject8).a((CharSequence)localObject5);
          localObject4 = (CharSequence)localObject4;
          localObject5 = ((z.d)localObject5).b((CharSequence)localObject4);
          localObject6 = new android/support/v4/app/z$c;
          ((z.c)localObject6).<init>();
          localObject4 = (z.g)((z.c)localObject6).b((CharSequence)localObject4);
          localObject4 = ((z.d)localObject5).a((z.g)localObject4);
          localObject5 = b;
          int i3 = 2131100594;
          int m = android.support.v4.content.b.c((Context)localObject5, i3);
          localObject4 = ((z.d)localObject4).f(m).a(2131234787).c(-1).e().a((PendingIntent)localObject2);
          localObject2 = b;
          m = 2131888304;
          localObject2 = (CharSequence)((Context)localObject2).getString(m);
          localObject4 = ((z.d)localObject4).a(0, (CharSequence)localObject2, (PendingIntent)localObject3);
          localObject2 = g;
          int i8 = 2131363574;
          localObject5 = h;
          localObject6 = new com/truecaller/messaging/transport/im/bz$a;
          ((bz.a)localObject6).<init>(this, (bx)localObject7);
          localObject6 = (l.a)localObject6;
          localObject4 = ((l)localObject5).a((z.d)localObject4, (l.a)localObject6);
          k.a(localObject4, "notificationIconHelper.c…lder) { getAvatar(user) }");
          localObject5 = "notificationJoinedImUsers";
          ((a)localObject2).a(i8, (Notification)localObject4, (String)localObject5);
        }
        else
        {
          localObject4 = (bp)d.a();
          localObject1 = (Iterable)localObject1;
          localObject2 = new java/util/ArrayList;
          ((ArrayList)localObject2).<init>();
          localObject2 = (Collection)localObject2;
          localObject1 = ((Iterable)localObject1).iterator();
          for (;;)
          {
            boolean bool5 = ((Iterator)localObject1).hasNext();
            if (!bool5) {
              break;
            }
            localObject3 = nextb;
            if (localObject3 != null) {
              ((Collection)localObject2).add(localObject3);
            }
          }
          localObject2 = (List)localObject2;
          ((bp)localObject4).a((List)localObject2);
          break label1467;
        }
        label1454:
        localObject1 = new c/u;
        ((c.u)localObject1).<init>("null cannot be cast to non-null type kotlin.Array<T>");
        throw ((Throwable)localObject1);
      }
      label1467:
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bz
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */