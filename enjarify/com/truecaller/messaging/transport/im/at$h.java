package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class at$h
  extends u
{
  private final long b;
  
  private at$h(e parame, long paramLong)
  {
    super(parame);
    b = paramLong;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".unlockConversation(");
    String str = a(Long.valueOf(b), 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.at.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */