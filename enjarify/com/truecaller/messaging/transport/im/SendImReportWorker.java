package com.truecaller.messaging.transport.im;

import android.content.Context;
import androidx.work.ListenableWorker.a;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.e;
import c.a.an;
import c.g.b.k;
import c.l;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.models.input.InputReportType;
import com.truecaller.bp;
import java.util.Set;

public final class SendImReportWorker
  extends Worker
{
  public static final SendImReportWorker.a c;
  public q b;
  
  static
  {
    SendImReportWorker.a locala = new com/truecaller/messaging/transport/im/SendImReportWorker$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public SendImReportWorker(Context paramContext, WorkerParameters paramWorkerParameters)
  {
    super(paramContext, paramWorkerParameters);
    paramContext = TrueApp.y();
    k.a(paramContext, "TrueApp.getApp()");
    paramContext.a().a(this);
  }
  
  public final ListenableWorker.a a()
  {
    Object localObject1 = getInputData();
    boolean bool = false;
    Object localObject2 = null;
    int i = ((e)localObject1).a("report_type", 0);
    localObject1 = InputReportType.forNumber(i);
    Object localObject3 = getInputData().b("raw_message_id");
    String str = getInputData().b("im_peer_id");
    Object localObject4 = getInputData();
    Object localObject5 = "im_group_id";
    localObject4 = ((e)localObject4).b((String)localObject5);
    if (localObject1 != null)
    {
      int j = 2;
      localObject5 = new InputReportType[j];
      InputReportType localInputReportType = InputReportType.RECEIVED;
      localObject5[0] = localInputReportType;
      localInputReportType = InputReportType.READ;
      localObject5[1] = localInputReportType;
      localObject2 = an.a((Object[])localObject5);
      bool = ((Set)localObject2).contains(localObject1);
      if ((bool) && (localObject3 != null) && (str != null))
      {
        localObject2 = b;
        if (localObject2 == null)
        {
          localObject5 = "imManager";
          k.a((String)localObject5);
        }
        localObject1 = (SendResult)((q)localObject2).a((InputReportType)localObject1, (String)localObject3, str, (String)localObject4).d();
        if (localObject1 != null)
        {
          localObject3 = cf.a;
          i = ((SendResult)localObject1).ordinal();
          i = localObject3[i];
        }
        switch (i)
        {
        default: 
          localObject1 = new c/l;
          ((l)localObject1).<init>();
          throw ((Throwable)localObject1);
        case 3: 
          localObject1 = ListenableWorker.a.a();
          k.a(localObject1, "Result.success()");
          return (ListenableWorker.a)localObject1;
        case 2: 
          i = getRunAttemptCount();
          int k = 3;
          if (i < k) {
            localObject1 = ListenableWorker.a.b();
          } else {
            localObject1 = ListenableWorker.a.a();
          }
          k.a(localObject1, "if (runAttemptCount < MA…y() else Result.success()");
          return (ListenableWorker.a)localObject1;
        }
        localObject1 = ListenableWorker.a.a();
        k.a(localObject1, "Result.success()");
        return (ListenableWorker.a)localObject1;
      }
    }
    localObject1 = ListenableWorker.a.a();
    k.a(localObject1, "Result.success()");
    return (ListenableWorker.a)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.SendImReportWorker
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */