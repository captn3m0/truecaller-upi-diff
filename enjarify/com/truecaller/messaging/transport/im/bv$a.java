package com.truecaller.messaging.transport.im;

import c.d.c;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.api.services.messenger.v1.f.b;
import com.truecaller.api.services.messenger.v1.f.b.a;
import com.truecaller.api.services.messenger.v1.f.d;
import com.truecaller.api.services.messenger.v1.k.a;
import com.truecaller.network.d.j.a;
import io.grpc.bc;
import java.lang.ref.WeakReference;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

final class bv$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  bv$a(bv parambv, WeakReference paramWeakReference, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/messaging/transport/im/bv$a;
    bv localbv = b;
    WeakReference localWeakReference = c;
    locala.<init>(localbv, localWeakReference, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    Object localObject2;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = (k.a)j.a.a((cb)b.b.get());
        if (paramObject == null) {
          return x.a;
        }
        localObject1 = b.a;
        localObject2 = "profileCountryIso";
        localObject1 = ((com.truecaller.common.g.a)localObject1).a((String)localObject2);
        if (localObject1 == null) {
          localObject1 = "";
        }
        localObject2 = "coreSettings.getString(C…ROFILE_COUNTRY_ISO) ?: \"\"";
        c.g.b.k.a(localObject1, (String)localObject2);
      }
    }
    try
    {
      localObject2 = f.b.a();
      Object localObject3 = b;
      int j = ((bv)localObject3).a();
      localObject2 = ((f.b.a)localObject2).a(j);
      localObject3 = b;
      j = ((bv)localObject3).c();
      localObject2 = ((f.b.a)localObject2).b(j);
      localObject1 = ((f.b.a)localObject2).a((String)localObject1);
      localObject1 = ((f.b.a)localObject1).build();
      localObject1 = (f.b)localObject1;
      paramObject = ((k.a)paramObject).a((f.b)localObject1);
      localObject1 = bg.a;
      localObject1 = (ag)localObject1;
      localObject2 = b;
      localObject2 = c;
      localObject3 = new com/truecaller/messaging/transport/im/bv$a$1;
      ((bv.a.1)localObject3).<init>(this, (f.d)paramObject, null);
      localObject3 = (m)localObject3;
      int k = 2;
      e.b((ag)localObject1, (f)localObject2, (m)localObject3, k);
    }
    catch (bc|RuntimeException localbc)
    {
      for (;;) {}
    }
    return x.a;
    throw a;
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bv.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */