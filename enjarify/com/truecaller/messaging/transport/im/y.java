package com.truecaller.messaging.transport.im;

import android.content.Context;
import com.truecaller.util.h;
import dagger.a.d;
import javax.inject.Provider;

public final class y
  implements d
{
  private final Provider a;
  
  private y(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static y a(Provider paramProvider)
  {
    y localy = new com/truecaller/messaging/transport/im/y;
    localy.<init>(paramProvider);
    return localy;
  }
  
  public static com.truecaller.util.g a(Context paramContext)
  {
    h localh = new com/truecaller/util/h;
    localh.<init>(paramContext);
    return (com.truecaller.util.g)dagger.a.g.a(localh, "Cannot return null from a non-@Nullable @Provides method");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.y
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */