package com.truecaller.messaging.transport.im;

import android.database.Cursor;
import android.database.CursorWrapper;
import c.g.b.k;
import com.truecaller.messaging.transport.c.a;

public final class v
  extends CursorWrapper
  implements c.a
{
  private final int a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  
  public v(Cursor paramCursor)
  {
    super(paramCursor);
    int i = paramCursor.getColumnIndexOrThrow("date");
    a = i;
    i = paramCursor.getColumnIndexOrThrow("im_peer_id");
    b = i;
    i = paramCursor.getColumnIndexOrThrow("filter_action");
    c = i;
    i = paramCursor.getColumnIndexOrThrow("raw_id");
    d = i;
    i = paramCursor.getColumnIndexOrThrow("delivery_sync_status");
    e = i;
    i = paramCursor.getColumnIndexOrThrow("read_sync_status");
    f = i;
    int j = paramCursor.getColumnIndexOrThrow("im_group_id");
    g = j;
  }
  
  public final long a()
  {
    return 0L;
  }
  
  public final long b()
  {
    return 0L;
  }
  
  public final long c()
  {
    int i = a;
    return getLong(i);
  }
  
  public final boolean d()
  {
    return false;
  }
  
  public final boolean e()
  {
    return false;
  }
  
  public final boolean f()
  {
    return false;
  }
  
  public final int g()
  {
    return 0;
  }
  
  public final int h()
  {
    return 0;
  }
  
  public final String j()
  {
    return null;
  }
  
  public final String k()
  {
    int i = b;
    String str = getString(i);
    k.a(str, "getString(indexImPeerId)");
    return str;
  }
  
  public final int l()
  {
    int i = c;
    return getInt(i);
  }
  
  public final String m()
  {
    int i = d;
    String str = getString(i);
    k.a(str, "getString(indexRawId)");
    return str;
  }
  
  public final int n()
  {
    int i = e;
    return getInt(i);
  }
  
  public final int o()
  {
    int i = f;
    return getInt(i);
  }
  
  public final String p()
  {
    int i = g;
    return getString(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */