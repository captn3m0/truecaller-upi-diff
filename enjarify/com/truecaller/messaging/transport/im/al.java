package com.truecaller.messaging.transport.im;

import dagger.a.d;
import javax.inject.Provider;

public final class al
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private al(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static al a(Provider paramProvider1, Provider paramProvider2)
  {
    al localal = new com/truecaller/messaging/transport/im/al;
    localal.<init>(paramProvider1, paramProvider2);
    return localal;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.al
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */