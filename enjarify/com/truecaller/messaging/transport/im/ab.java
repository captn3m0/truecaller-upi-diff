package com.truecaller.messaging.transport.im;

import dagger.a.d;
import javax.inject.Provider;

public final class ab
  implements d
{
  private final Provider a;
  
  private ab(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static ab a(Provider paramProvider)
  {
    ab localab = new com/truecaller/messaging/transport/im/ab;
    localab.<init>(paramProvider);
    return localab;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */