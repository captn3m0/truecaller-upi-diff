package com.truecaller.messaging.transport.im;

public final class p
{
  final long a;
  final long b;
  final long c;
  
  public p(long paramLong1, long paramLong2, long paramLong3)
  {
    a = paramLong1;
    b = paramLong2;
    c = paramLong3;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof p;
      if (bool2)
      {
        paramObject = (p)paramObject;
        long l1 = a;
        long l2 = a;
        bool2 = l1 < l2;
        if (!bool2) {
          bool2 = true;
        } else {
          bool2 = false;
        }
        if (bool2)
        {
          l1 = b;
          l2 = b;
          bool2 = l1 < l2;
          if (!bool2) {
            bool2 = true;
          } else {
            bool2 = false;
          }
          if (bool2)
          {
            l1 = c;
            l2 = c;
            boolean bool3 = l1 < l2;
            if (!bool3)
            {
              bool3 = true;
            }
            else
            {
              bool3 = false;
              paramObject = null;
            }
            if (bool3) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    long l1 = a;
    int i = 32;
    long l2 = l1 >>> i;
    int j = (int)(l1 ^ l2) * 31;
    l2 = b;
    long l3 = l2 >>> i;
    int k = (int)(l2 ^ l3);
    j = (j + k) * 31;
    l2 = c;
    l3 = l2 >>> i;
    k = (int)(l2 ^ l3);
    return j + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ImGroupMessage(messageId=");
    long l = a;
    localStringBuilder.append(l);
    localStringBuilder.append(", conversationId=");
    l = b;
    localStringBuilder.append(l);
    localStringBuilder.append(", date=");
    l = c;
    localStringBuilder.append(l);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */