package com.truecaller.messaging.transport.im;

import android.content.Context;
import android.os.Bundle;
import c.g.b.k;
import c.l.g;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.featuretoggles.b;
import java.util.concurrent.TimeUnit;

public final class JoinedImUsersNotificationTask
  extends PersistentBackgroundTask
{
  public by a;
  public com.truecaller.featuretoggles.e b;
  
  public JoinedImUsersNotificationTask()
  {
    TrueApp localTrueApp = TrueApp.y();
    k.a(localTrueApp, "TrueApp.getApp()");
    localTrueApp.a().a(this);
  }
  
  public final int a()
  {
    return 10033;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    paramBundle = "serviceContext";
    k.b(paramContext, paramBundle);
    paramContext = a;
    if (paramContext == null)
    {
      paramBundle = "joinedImUsersManager";
      k.a(paramBundle);
    }
    paramContext.b();
    return PersistentBackgroundTask.RunResult.Success;
  }
  
  public final boolean a(Context paramContext)
  {
    Object localObject1 = "serviceContext";
    k.b(paramContext, (String)localObject1);
    paramContext = b;
    if (paramContext == null)
    {
      localObject1 = "featureRegistry";
      k.a((String)localObject1);
    }
    localObject1 = p;
    Object localObject2 = com.truecaller.featuretoggles.e.a;
    int i = 44;
    localObject2 = localObject2[i];
    paramContext = ((com.truecaller.featuretoggles.e.a)localObject1).a(paramContext, (g)localObject2);
    boolean bool = paramContext.a();
    if (bool)
    {
      paramContext = a;
      if (paramContext == null)
      {
        localObject1 = "joinedImUsersManager";
        k.a((String)localObject1);
      }
      bool = paramContext.a();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public final com.truecaller.common.background.e b()
  {
    Object localObject = new com/truecaller/common/background/e$a;
    int i = 1;
    ((com.truecaller.common.background.e.a)localObject).<init>(i);
    TimeUnit localTimeUnit = TimeUnit.HOURS;
    localObject = ((com.truecaller.common.background.e.a)localObject).a(2, localTimeUnit);
    localTimeUnit = TimeUnit.MINUTES;
    localObject = ((com.truecaller.common.background.e.a)localObject).b(30, localTimeUnit).a(i).b();
    k.a(localObject, "TaskConfiguration.Builde…YPE_ANY)\n        .build()");
    return (com.truecaller.common.background.e)localObject;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.JoinedImUsersNotificationTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */