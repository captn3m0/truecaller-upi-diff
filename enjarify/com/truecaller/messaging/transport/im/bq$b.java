package com.truecaller.messaging.transport.im;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class bq$b
  extends u
{
  private final long b;
  
  private bq$b(e parame, long paramLong)
  {
    super(parame);
    b = paramLong;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".getJoinedImUsersForNotification(");
    String str = a(Long.valueOf(b), 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.bq.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */