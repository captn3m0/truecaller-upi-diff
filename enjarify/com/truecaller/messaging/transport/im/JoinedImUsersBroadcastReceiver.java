package com.truecaller.messaging.transport.im;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.app.ac;
import com.truecaller.messaging.conversation.ConversationActivity;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.newconversation.NewConversationActivity;

public final class JoinedImUsersBroadcastReceiver
  extends BroadcastReceiver
{
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if ((paramContext != null) && (paramIntent != null))
    {
      Object localObject1 = paramIntent.getAction();
      if (localObject1 != null)
      {
        int i = ((String)localObject1).hashCode();
        int j = -603694704;
        int k = 268435456;
        String str;
        boolean bool;
        if (i != j)
        {
          j = 1009479503;
          if (i != j) {
            break label238;
          }
          str = "com.truecaller.open_new_conversation";
          bool = ((String)localObject1).equals(str);
          if (!bool) {
            break label238;
          }
          paramIntent.getParcelableArrayExtra("participant_list");
          paramIntent = new android/content/Intent;
          localObject1 = NewConversationActivity.class;
          paramIntent.<init>(paramContext, (Class)localObject1);
          paramIntent.setFlags(k);
          paramContext.startActivity(paramIntent);
        }
        else
        {
          str = "com.truecaller.open_conversation";
          bool = ((String)localObject1).equals(str);
          if (!bool) {
            break label238;
          }
          localObject1 = "participant";
          paramIntent = paramIntent.getParcelableExtra((String)localObject1);
          bool = paramIntent instanceof Participant;
          if (!bool) {
            paramIntent = null;
          }
          paramIntent = (Participant)paramIntent;
          if (paramIntent != null)
          {
            localObject1 = new android/content/Intent;
            ((Intent)localObject1).<init>(paramContext, ConversationActivity.class);
            ((Intent)localObject1).setFlags(k);
            str = "participants";
            j = 1;
            Object localObject2 = new Participant[j];
            k = 0;
            localObject2[0] = paramIntent;
            localObject2 = (Parcelable[])localObject2;
            ((Intent)localObject1).putExtra(str, (Parcelable[])localObject2);
            paramContext.startActivity((Intent)localObject1);
          }
        }
        ac.a(paramContext).a(2131363574);
        paramIntent = new android/content/Intent;
        paramIntent.<init>("android.intent.action.CLOSE_SYSTEM_DIALOGS");
        paramContext.sendBroadcast(paramIntent);
        return;
      }
      label238:
      paramContext = new java/lang/RuntimeException;
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("Unknown action ");
      paramIntent = paramIntent.getAction();
      ((StringBuilder)localObject1).append(paramIntent);
      ((StringBuilder)localObject1).append(" in onReceive");
      paramIntent = ((StringBuilder)localObject1).toString();
      paramContext.<init>(paramIntent);
      throw ((Throwable)paramContext);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.im.JoinedImUsersBroadcastReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */