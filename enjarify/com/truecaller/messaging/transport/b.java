package com.truecaller.messaging.transport;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.SystemClock;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.c.a;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.transport.a.c;

final class b
  implements d
{
  private final Context a;
  private final f b;
  private final f c;
  private final f d;
  private final a e;
  
  b(Context paramContext, f paramf1, f paramf2, f paramf3, a parama)
  {
    a = paramContext;
    b = paramf1;
    c = paramf2;
    d = paramf3;
    e = parama;
  }
  
  public final w a(Message paramMessage, Participant[] paramArrayOfParticipant, int paramInt1, int paramInt2)
  {
    try
    {
      Object localObject1 = b;
      localObject1 = ((f)localObject1).a();
      localObject1 = (t)localObject1;
      paramMessage = ((t)localObject1).a(paramMessage, paramArrayOfParticipant, paramInt1);
      paramMessage = paramMessage.d();
      paramMessage = (Message)paramMessage;
      if (paramMessage == null) {
        return w.b(null);
      }
      boolean bool1 = paramMessage.b();
      paramInt1 = 0;
      Object localObject2 = null;
      localObject1 = new String[0];
      AssertionUtil.AlwaysFatal.isTrue(bool1, (String[])localObject1);
      int i = f & 0x10;
      boolean bool3 = true;
      if (i != 0)
      {
        i = 1;
      }
      else
      {
        i = 0;
        paramArrayOfParticipant = null;
      }
      String[] arrayOfString1 = new String[0];
      AssertionUtil.AlwaysFatal.isTrue(i, arrayOfString1);
      int j = k;
      int m = 3;
      if (j != m)
      {
        j = 1;
      }
      else
      {
        j = 0;
        paramArrayOfParticipant = null;
      }
      String[] arrayOfString2 = new String[0];
      AssertionUtil.AlwaysFatal.isTrue(j, arrayOfString2);
      int k = j;
      if (k == m)
      {
        k = 1;
      }
      else
      {
        k = 0;
        paramArrayOfParticipant = null;
      }
      arrayOfString1 = new String[0];
      AssertionUtil.AlwaysFatal.isTrue(k, arrayOfString1);
      paramArrayOfParticipant = m;
      long l1 = paramArrayOfParticipant.c();
      long l2 = -1;
      boolean bool2 = l1 < l2;
      if (!bool2)
      {
        bool3 = false;
        localObject1 = null;
      }
      paramArrayOfParticipant = new String[0];
      AssertionUtil.AlwaysFatal.isTrue(bool3, paramArrayOfParticipant);
      if (paramInt2 == 0)
      {
        paramArrayOfParticipant = b;
        paramArrayOfParticipant = paramArrayOfParticipant.a();
        paramArrayOfParticipant = (t)paramArrayOfParticipant;
        paramArrayOfParticipant = paramArrayOfParticipant.a(null);
        paramArrayOfParticipant = paramArrayOfParticipant.d();
        localObject2 = Boolean.FALSE;
        if (paramArrayOfParticipant == localObject2) {
          return w.b(null);
        }
        return w.b(paramMessage);
      }
      paramArrayOfParticipant = a;
      localObject1 = e;
      paramArrayOfParticipant = ScheduledMessageReceiver.a(paramArrayOfParticipant, (org.a.a.b)localObject1);
      localObject1 = a;
      ((Context)localObject1).sendBroadcast(paramArrayOfParticipant);
      paramArrayOfParticipant = a;
      localObject1 = a;
      localObject1 = ScheduledMessageReceiver.a((Context)localObject1, null);
      int n = 268435456;
      paramArrayOfParticipant = PendingIntent.getBroadcast(paramArrayOfParticipant, 0, (Intent)localObject1, n);
      long l3 = SystemClock.elapsedRealtime();
      long l4 = paramInt2;
      l3 += l4;
      localObject2 = a;
      String str = "alarm";
      localObject2 = ((Context)localObject2).getSystemService(str);
      localObject2 = (AlarmManager)localObject2;
      paramInt2 = Build.VERSION.SDK_INT;
      int i1 = 23;
      int i2 = 2;
      if (paramInt2 >= i1) {
        ((AlarmManager)localObject2).setExactAndAllowWhileIdle(i2, l3, paramArrayOfParticipant);
      } else {
        android.support.v4.app.b.a((AlarmManager)localObject2, i2, l3, paramArrayOfParticipant);
      }
      return w.b(paramMessage);
    }
    catch (InterruptedException localInterruptedException) {}
    return w.b(null);
  }
  
  public final void a(Message paramMessage)
  {
    int i = f;
    int j = 9;
    i &= j;
    if (i == j) {
      i = 1;
    } else {
      i = 0;
    }
    String[] arrayOfString = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(i, arrayOfString);
    ((t)b.a()).c(paramMessage).c();
  }
  
  public final void a(l paraml, Intent paramIntent, int paramInt)
  {
    paraml.a(paramIntent, paramInt);
  }
  
  public final void b(Message paramMessage)
  {
    int i = j;
    int j = 2;
    if (i == j)
    {
      localObject = (c)d.a();
      ((c)localObject).a(paramMessage);
    }
    else
    {
      localObject = (c)c.a();
      ((c)localObject).a(paramMessage);
    }
    Object localObject = e;
    paramMessage = o;
    ((a)localObject).c(paramMessage);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */