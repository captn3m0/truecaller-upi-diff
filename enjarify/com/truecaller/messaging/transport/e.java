package com.truecaller.messaging.transport;

import android.content.Intent;
import com.truecaller.androidactors.v;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;

public final class e
  implements d
{
  private final v a;
  
  public e(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return d.class.equals(paramClass);
  }
  
  public final w a(Message paramMessage, Participant[] paramArrayOfParticipant, int paramInt1, int paramInt2)
  {
    v localv = a;
    e.c localc = new com/truecaller/messaging/transport/e$c;
    com.truecaller.androidactors.e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, paramMessage, paramArrayOfParticipant, paramInt1, paramInt2, (byte)0);
    return w.a(localv, localc);
  }
  
  public final void a(Message paramMessage)
  {
    v localv = a;
    e.b localb = new com/truecaller/messaging/transport/e$b;
    com.truecaller.androidactors.e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramMessage, (byte)0);
    localv.a(localb);
  }
  
  public final void a(l paraml, Intent paramIntent, int paramInt)
  {
    v localv = a;
    e.a locala = new com/truecaller/messaging/transport/e$a;
    com.truecaller.androidactors.e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paraml, paramIntent, paramInt, (byte)0);
    localv.a(locala);
  }
  
  public final void b(Message paramMessage)
  {
    v localv = a;
    e.d locald = new com/truecaller/messaging/transport/e$d;
    com.truecaller.androidactors.e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locald.<init>(locale, paramMessage, (byte)0);
    localv.a(locald);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */