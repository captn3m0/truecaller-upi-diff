package com.truecaller.messaging.transport.history;

import android.database.Cursor;
import android.database.CursorWrapper;
import c.g.b.k;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.transport.c.a;
import com.truecaller.messaging.transport.i;

public final class a
  extends CursorWrapper
  implements c.a
{
  private final int a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  private final int j;
  private final int k;
  private final int l;
  private final i m;
  
  public a(Cursor paramCursor, i parami)
  {
    super(paramCursor);
    m = parami;
    int n = paramCursor.getColumnIndexOrThrow("_id");
    a = n;
    n = paramCursor.getColumnIndexOrThrow("timestamp");
    b = n;
    n = paramCursor.getColumnIndexOrThrow("new");
    c = n;
    n = paramCursor.getColumnIndexOrThrow("is_read");
    d = n;
    n = paramCursor.getColumnIndexOrThrow("type");
    e = n;
    n = paramCursor.getColumnIndexOrThrow("normalized_number");
    f = n;
    n = paramCursor.getColumnIndexOrThrow("call_log_id");
    g = n;
    n = paramCursor.getColumnIndexOrThrow("feature");
    h = n;
    n = paramCursor.getColumnIndexOrThrow("number_type");
    i = n;
    n = paramCursor.getColumnIndexOrThrow("subscription_id");
    j = n;
    n = paramCursor.getColumnIndexOrThrow("tc_flag");
    k = n;
    int i1 = paramCursor.getColumnIndexOrThrow("subscription_component_name");
    l = i1;
  }
  
  public final long a()
  {
    int n = a;
    return getLong(n);
  }
  
  public final long b()
  {
    return 0L;
  }
  
  public final long c()
  {
    int n = b;
    return getLong(n);
  }
  
  public final boolean d()
  {
    int n = c;
    n = getInt(n);
    if (n != 0)
    {
      n = g();
      int i1 = 8;
      if (n == i1) {
        return false;
      }
    }
    return true;
  }
  
  public final boolean e()
  {
    int n = d;
    n = getInt(n);
    int i1 = 1;
    if (n != i1)
    {
      n = g();
      int i2 = 8;
      if (n == i2) {
        return false;
      }
    }
    return i1;
  }
  
  public final boolean f()
  {
    return false;
  }
  
  public final int g()
  {
    int n = e;
    n = getInt(n);
    switch (n)
    {
    default: 
      return 0;
    case 3: 
      return 8;
    }
    return 1;
  }
  
  public final int h()
  {
    return 0;
  }
  
  public final Message i()
  {
    int n = f;
    Object localObject1 = getString(n);
    Object localObject2 = localObject1;
    localObject2 = (CharSequence)localObject1;
    int i1 = 0;
    int i2 = 1;
    if (localObject2 != null)
    {
      i3 = ((CharSequence)localObject2).length();
      if (i3 != 0)
      {
        i3 = 0;
        localObject2 = null;
        break label55;
      }
    }
    int i3 = 1;
    label55:
    if (i3 != 0)
    {
      localObject1 = Participant.a;
      localObject2 = "Participant.EMPTY";
      k.a(localObject1, (String)localObject2);
    }
    else
    {
      localObject2 = m;
      localObject1 = ((i)localObject2).a((String)localObject1);
    }
    localObject2 = new com/truecaller/messaging/data/types/Message$a;
    ((Message.a)localObject2).<init>();
    long l1 = c();
    localObject2 = ((Message.a)localObject2).c(l1);
    localObject1 = ((Message.a)localObject2).a((Participant)localObject1);
    i3 = g();
    localObject1 = ((Message.a)localObject1).a(i3);
    i3 = 5;
    Object localObject3 = new com/truecaller/messaging/transport/history/HistoryTransportInfo;
    long l2 = -1;
    long l3 = a();
    int i5 = g;
    int i6 = getInt(i5);
    i5 = k;
    i5 = getInt(i5);
    int i7 = 3;
    if (i5 == i7)
    {
      i7 = 1;
    }
    else
    {
      i2 = l;
      String str1 = getString(i2);
      if (str1 != null)
      {
        i5 = str1.hashCode();
        i7 = -1547699361;
        boolean bool1;
        if (i5 != i7)
        {
          i7 = -847387129;
          if (i5 == i7)
          {
            localObject4 = "com.truecaller.voip.manager.VOIP";
            bool1 = str1.equals(localObject4);
            if (bool1)
            {
              i1 = 4;
              i7 = 4;
              break label301;
            }
          }
        }
        else
        {
          localObject4 = "com.whatsapp";
          bool1 = str1.equals(localObject4);
          if (bool1)
          {
            i1 = 2;
            i7 = 2;
            break label301;
          }
        }
      }
      i7 = 0;
    }
    label301:
    i1 = h;
    int i8 = getInt(i1);
    i1 = i;
    String str2 = getString(i1);
    Object localObject4 = localObject3;
    ((HistoryTransportInfo)localObject3).<init>(l2, l3, i6, i7, i8, str2);
    localObject3 = (TransportInfo)localObject3;
    localObject1 = ((Message.a)localObject1).a(i3, (TransportInfo)localObject3);
    localObject2 = Entity.a("history", "");
    localObject1 = ((Message.a)localObject1).a((Entity)localObject2);
    boolean bool2 = e();
    localObject1 = ((Message.a)localObject1).b(bool2);
    bool2 = d();
    localObject1 = ((Message.a)localObject1).a(bool2);
    int i4 = j;
    localObject2 = getString(i4);
    localObject1 = ((Message.a)localObject1).a((String)localObject2).b();
    k.a(localObject1, "Message.Builder()\n      …Id))\n            .build()");
    return (Message)localObject1;
  }
  
  public final String j()
  {
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.history.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */