package com.truecaller.messaging.transport.history;

import android.content.Context;
import c.a.an;
import c.g.b.k;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.transport.c;
import com.truecaller.messaging.transport.f;
import com.truecaller.messaging.transport.i;
import com.truecaller.multisim.h;
import java.util.Set;

public final class b
  extends c
{
  public b(Context paramContext, h paramh, e parame)
  {
    super(paramContext, paramh, parame);
  }
  
  public final Set a(long paramLong, f paramf, i parami, Participant paramParticipant, boolean paramBoolean)
  {
    k.b(paramf, "threadInfoCache");
    k.b(parami, "participantCache");
    k.b(paramParticipant, "participant");
    Participant[] arrayOfParticipant = new Participant[1];
    arrayOfParticipant[0] = paramParticipant;
    return an.b(arrayOfParticipant);
  }
  
  public final boolean a(int paramInt)
  {
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.history.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */