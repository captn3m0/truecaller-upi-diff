package com.truecaller.messaging.transport.history;

import android.content.Intent;
import android.net.Uri;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.a;
import com.truecaller.messaging.transport.ad;
import com.truecaller.messaging.transport.ad.a;
import com.truecaller.messaging.transport.ad.a.a;
import com.truecaller.messaging.transport.ad.b;
import com.truecaller.messaging.transport.i;
import com.truecaller.messaging.transport.l;
import com.truecaller.messaging.transport.l.a;
import com.truecaller.utils.q;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.a.a.a.g;

public final class e
  implements l
{
  private final com.truecaller.androidactors.f a;
  private final b b;
  private final h c;
  private final ad.b d;
  
  public e(com.truecaller.androidactors.f paramf, b paramb, h paramh, ad.b paramb1)
  {
    a = paramf;
    b = paramb;
    c = paramh;
    d = paramb1;
  }
  
  private static void a(HistoryTransportInfo paramHistoryTransportInfo, f paramf)
  {
    long l = a;
    Object localObject = TruecallerContract.aa.a(l);
    localObject = paramf.a((Uri)localObject);
    int i = 1;
    Integer localInteger1 = Integer.valueOf(i);
    localObject = ((ad.a.a)localObject).a("read", localInteger1);
    localInteger1 = Integer.valueOf(i);
    localObject = ((ad.a.a)localObject).a("seen", localInteger1);
    String str = "sync_status";
    Integer localInteger2 = Integer.valueOf(i);
    localObject = ((ad.a.a)localObject).a(str, localInteger2).a();
    paramf.a((ad.a)localObject);
    int j = c;
    if (j != 0)
    {
      l = c;
      paramf.a(l);
      return;
    }
    l = b;
    paramf.b(l);
  }
  
  public final long a(long paramLong)
  {
    return paramLong;
  }
  
  public final long a(com.truecaller.messaging.transport.f paramf, i parami, r paramr, org.a.a.b paramb1, org.a.a.b paramb2, int paramInt, List paramList, q paramq, boolean paramBoolean1, boolean paramBoolean2, Set paramSet)
  {
    c.g.b.k.b(paramf, "threadInfoCache");
    c.g.b.k.b(parami, "participantCache");
    c.g.b.k.b(paramr, "cursor");
    c.g.b.k.b(paramb1, "timeTo");
    c.g.b.k.b(paramb2, "timeFrom");
    c.g.b.k.b(paramList, "operations");
    c.g.b.k.b(paramq, "trace");
    c.g.b.k.b(paramSet, "messagesToClassify");
    return b.a(paramf, parami, paramr, paramb1, paramb2, paramInt, paramList, paramq, paramBoolean1, paramBoolean2, paramSet);
  }
  
  public final l.a a(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramArrayOfParticipant, "recipients");
    paramMessage = new java/lang/IllegalStateException;
    paramMessage.<init>("History transport cannot enqueue any message");
    throw ((Throwable)paramMessage);
  }
  
  public final String a()
  {
    return "history";
  }
  
  public final String a(String paramString)
  {
    c.g.b.k.b(paramString, "simToken");
    return paramString;
  }
  
  public final void a(Intent paramIntent, int paramInt)
  {
    c.g.b.k.b(paramIntent, "intent");
    paramIntent = new java/lang/IllegalStateException;
    paramIntent.<init>("History transport does not expect any intent");
    throw ((Throwable)paramIntent);
  }
  
  public final void a(BinaryEntity paramBinaryEntity)
  {
    c.g.b.k.b(paramBinaryEntity, "entity");
    paramBinaryEntity = new java/lang/IllegalStateException;
    paramBinaryEntity.<init>("History transport doesn't support attachments");
    throw ((Throwable)paramBinaryEntity);
  }
  
  public final void a(Message paramMessage, String paramString1, String paramString2)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramString2, "initiatedVia");
    paramMessage = new java/lang/IllegalStateException;
    paramMessage.<init>("History transport does not support reactions");
    throw ((Throwable)paramMessage);
  }
  
  public final void a(org.a.a.b paramb)
  {
    c.g.b.k.b(paramb, "time");
    h localh = c;
    long l = a;
    localh.a(5, l);
  }
  
  public final boolean a(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    paramMessage = new java/lang/IllegalStateException;
    paramMessage.<init>("History transport should only sync up with already existing events");
    throw ((Throwable)paramMessage);
  }
  
  public final boolean a(Message paramMessage, Entity paramEntity)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramEntity, "entity");
    paramMessage = new java/lang/IllegalStateException;
    paramMessage.<init>("History transport doesn't support entity download");
    throw ((Throwable)paramMessage);
  }
  
  public final boolean a(Participant paramParticipant)
  {
    c.g.b.k.b(paramParticipant, "participant");
    return true;
  }
  
  public final boolean a(String paramString, a parama)
  {
    c.g.b.k.b(paramString, "text");
    c.g.b.k.b(parama, "result");
    parama.a(0, 0, 0, 5);
    return false;
  }
  
  public final void b(long paramLong)
  {
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("History transport does not support retry");
    throw ((Throwable)localIllegalStateException);
  }
  
  public final boolean b(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return false;
  }
  
  public final boolean b(ad paramad)
  {
    c.g.b.k.b(paramad, "transaction");
    paramad = (f)paramad;
    Collection localCollection = (Collection)e;
    boolean bool1 = localCollection.isEmpty();
    boolean bool2 = true;
    bool1 ^= bool2;
    if (!bool1)
    {
      localCollection = (Collection)g;
      bool1 = localCollection.isEmpty() ^ bool2;
      if (!bool1)
      {
        localCollection = (Collection)d;
        bool1 = localCollection.isEmpty() ^ bool2;
        if (!bool1)
        {
          localCollection = (Collection)f;
          bool1 = localCollection.isEmpty() ^ bool2;
          if (!bool1)
          {
            boolean bool3 = paramad.a();
            if (bool3) {
              return false;
            }
          }
        }
      }
    }
    return bool2;
  }
  
  public final boolean c()
  {
    return false;
  }
  
  public final boolean c(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return false;
  }
  
  public final com.truecaller.messaging.transport.k d(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    paramMessage = new java/lang/IllegalStateException;
    paramMessage.<init>("History transport cannot send messages");
    throw ((Throwable)paramMessage);
  }
  
  public final org.a.a.b d()
  {
    org.a.a.b localb = new org/a/a/b;
    long l = c.a(5);
    localb.<init>(l);
    return localb;
  }
  
  public final int e(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return 0;
  }
  
  public final boolean e()
  {
    return false;
  }
  
  public final int f()
  {
    return 5;
  }
  
  public final boolean f(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    paramMessage = new java/lang/IllegalStateException;
    paramMessage.<init>("History transport doesn't support content download");
    throw ((Throwable)paramMessage);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.history.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */