package com.truecaller.messaging.transport.history;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;
import com.truecaller.messaging.data.types.TransportInfo;
import org.a.a.b;

public final class HistoryTransportInfo
  implements TransportInfo
{
  public static final Parcelable.Creator CREATOR;
  final long a;
  public final long b;
  public final int c;
  public final int d;
  public final int e;
  public final String f;
  
  static
  {
    HistoryTransportInfo.a locala = new com/truecaller/messaging/transport/history/HistoryTransportInfo$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public HistoryTransportInfo(long paramLong1, long paramLong2, int paramInt1, int paramInt2, int paramInt3, String paramString)
  {
    a = paramLong1;
    b = paramLong2;
    c = paramInt1;
    d = paramInt2;
    e = paramInt3;
    f = paramString;
  }
  
  public final int a()
  {
    return 0;
  }
  
  public final String a(b paramb)
  {
    k.b(paramb, "date");
    return String.valueOf(b);
  }
  
  public final int b()
  {
    return 0;
  }
  
  public final long c()
  {
    return a;
  }
  
  public final long d()
  {
    return b;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final long e()
  {
    return -1;
  }
  
  public final boolean f()
  {
    return true;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    long l = a;
    paramParcel.writeLong(l);
    l = b;
    paramParcel.writeLong(l);
    paramInt = c;
    paramParcel.writeInt(paramInt);
    paramInt = d;
    paramParcel.writeInt(paramInt);
    paramInt = e;
    paramParcel.writeInt(paramInt);
    String str = f;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.history.HistoryTransportInfo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */