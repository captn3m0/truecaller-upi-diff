package com.truecaller.messaging.transport.history;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class HistoryTransportInfo$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    HistoryTransportInfo localHistoryTransportInfo = new com/truecaller/messaging/transport/history/HistoryTransportInfo;
    long l1 = paramParcel.readLong();
    long l2 = paramParcel.readLong();
    int i = paramParcel.readInt();
    int j = paramParcel.readInt();
    int k = paramParcel.readInt();
    String str = paramParcel.readString();
    localHistoryTransportInfo.<init>(l1, l2, i, j, k, str);
    return localHistoryTransportInfo;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new HistoryTransportInfo[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.history.HistoryTransportInfo.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */