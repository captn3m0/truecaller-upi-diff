package com.truecaller.messaging.transport;

import dagger.a.d;
import javax.inject.Provider;

public final class v
  implements d
{
  private final o a;
  private final Provider b;
  
  private v(o paramo, Provider paramProvider)
  {
    a = paramo;
    b = paramProvider;
  }
  
  public static v a(o paramo, Provider paramProvider)
  {
    v localv = new com/truecaller/messaging/transport/v;
    localv.<init>(paramo, paramProvider);
    return localv;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */