package com.truecaller.messaging.transport.a;

import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.transport.k.a;
import com.truecaller.messaging.transport.k.b;
import com.truecaller.messaging.transport.k.c;
import com.truecaller.messaging.transport.k.d;
import com.truecaller.messaging.transport.l;
import org.a.a.a.g;
import org.a.a.b;

public abstract class a
  implements c
{
  private final f a;
  private final com.truecaller.messaging.c.a b;
  
  public a(f paramf, com.truecaller.messaging.c.a parama)
  {
    a = paramf;
    b = parama;
  }
  
  public final void a(com.truecaller.messaging.transport.k paramk, Message paramMessage, l paraml)
  {
    c.g.b.k.b(paramk, "result");
    Object localObject1 = "message";
    c.g.b.k.b(paramMessage, (String)localObject1);
    boolean bool1 = paramk instanceof k.d;
    Object localObject2;
    if (bool1)
    {
      if (paraml != null)
      {
        localObject2 = (t)a.a();
        int i = paraml.f();
        b localb = e;
        ((t)localObject2).a(i, localb, false);
      }
    }
    else
    {
      boolean bool2 = paramk instanceof k.b;
      if (!bool2)
      {
        bool2 = paramk instanceof k.a;
        if (!bool2)
        {
          boolean bool3 = paramk instanceof k.c;
          if (bool3)
          {
            paraml = (t)a.a();
            paramk = (k.c)paramk;
            long l = a.a;
            boolean bool4 = b;
            paraml.a(paramMessage, l, bool4).c();
            return;
          }
          paramMessage = new java/lang/IllegalStateException;
          paramk = String.valueOf(paramk);
          paramk = "Unexpected result ".concat(paramk);
          paramMessage.<init>(paramk);
          throw ((Throwable)paramMessage);
        }
      }
      localObject2 = ((t)a.a()).e(paramMessage);
      ((w)localObject2).c();
    }
    if (bool1)
    {
      paramk = "Success";
    }
    else
    {
      bool1 = paramk instanceof k.a;
      if (bool1)
      {
        paramk = "Cancel";
      }
      else
      {
        bool1 = paramk instanceof k.b;
        if (!bool1) {
          break label287;
        }
        paramk = "Failure";
      }
    }
    localObject1 = b;
    int j;
    if (paraml != null) {
      j = paraml.f();
    } else {
      j = 3;
    }
    ((com.truecaller.messaging.c.a)localObject1).a(paramk, paramMessage, j);
    return;
    label287:
    paramMessage = new java/lang/IllegalStateException;
    paramk = String.valueOf(paramk);
    paramk = "Unexpected result ".concat(paramk);
    paramMessage.<init>(paramk);
    throw ((Throwable)paramMessage);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */