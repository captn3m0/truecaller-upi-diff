package com.truecaller.messaging.transport.a;

import com.truecaller.androidactors.f;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.transport.l;
import com.truecaller.messaging.transport.m;

public final class b
  extends a
{
  private final dagger.a a;
  
  public b(dagger.a parama, f paramf, com.truecaller.messaging.c.a parama1)
  {
    super(paramf, parama1);
    a = parama;
  }
  
  public final void a(Message paramMessage)
  {
    Object localObject = "message";
    c.g.b.k.b(paramMessage, (String)localObject);
    int i = j;
    boolean bool = true;
    int k = 2;
    if (i == k)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject = null;
    }
    String[] arrayOfString = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(i, arrayOfString);
    int j = f & 0x4;
    if (j == 0)
    {
      bool = false;
      localk = null;
    }
    localObject = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(bool, (String[])localObject);
    localObject = ((m)a.get()).a(k);
    c.g.b.k.a(localObject, "transportManager.get().g…nsport(Transport.TYPE_IM)");
    com.truecaller.messaging.transport.k localk = ((l)localObject).d(paramMessage);
    c.g.b.k.a(localk, "transport.sendMessage(message)");
    a(localk, paramMessage, (l)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */