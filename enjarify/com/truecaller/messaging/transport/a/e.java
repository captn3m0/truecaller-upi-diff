package com.truecaller.messaging.transport.a;

import com.truecaller.androidactors.f;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.transport.k.b;
import com.truecaller.messaging.transport.k.e;
import com.truecaller.messaging.transport.l;
import com.truecaller.messaging.transport.m;
import java.util.concurrent.TimeUnit;

public final class e
  extends a
{
  private final dagger.a a;
  
  public e(dagger.a parama, f paramf, com.truecaller.messaging.c.a parama1)
  {
    super(paramf, parama1);
    a = parama;
  }
  
  public final void a(Message paramMessage)
  {
    Object localObject1 = "message";
    c.g.b.k.b(paramMessage, (String)localObject1);
    int i = f & 0x4;
    int j = 0;
    Object localObject2 = null;
    if (i != 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject1 = null;
    }
    localObject2 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(i, (String[])localObject2);
    localObject1 = (m)a.get();
    j = j;
    localObject1 = ((m)localObject1).b(j);
    if (localObject1 != null)
    {
      localObject2 = ((l)localObject1).d(paramMessage);
      if (localObject2 != null) {}
    }
    else
    {
      localObject2 = (com.truecaller.messaging.transport.k)k.b.a;
    }
    Object localObject3 = "transport?.sendMessage(m…age) ?: SendResult.Failed";
    c.g.b.k.a(localObject2, (String)localObject3);
    boolean bool = localObject2 instanceof k.e;
    if (bool)
    {
      localObject2 = (k.e)localObject2;
      localObject3 = TimeUnit.MINUTES;
      localObject2 = ((k.e)localObject2).a((TimeUnit)localObject3);
    }
    a((com.truecaller.messaging.transport.k)localObject2, paramMessage, (l)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */