package com.truecaller.messaging.transport;

import dagger.a.d;
import javax.inject.Provider;

public final class x
  implements d
{
  private final o a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private x(o paramo, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramo;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static x a(o paramo, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    x localx = new com/truecaller/messaging/transport/x;
    localx.<init>(paramo, paramProvider1, paramProvider2, paramProvider3);
    return localx;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */