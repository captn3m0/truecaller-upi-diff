package com.truecaller.messaging.transport;

import c.g.b.k;
import com.truecaller.common.h.u;
import com.truecaller.messaging.data.types.Participant;
import java.util.LinkedHashMap;
import java.util.Map;

public final class j
  implements i
{
  private final Map a;
  private final u b;
  
  public j(u paramu)
  {
    b = paramu;
    paramu = new java/util/LinkedHashMap;
    paramu.<init>();
    paramu = (Map)paramu;
    a = paramu;
  }
  
  public final Participant a(String paramString)
  {
    k.b(paramString, "address");
    Object localObject1 = (Participant)a.get(paramString);
    if (localObject1 == null)
    {
      localObject1 = b;
      Object localObject2 = ((u)localObject1).a();
      localObject1 = Participant.a(paramString, (u)localObject1, (String)localObject2);
      localObject2 = a;
      String str = "this";
      k.a(localObject1, str);
      ((Map)localObject2).put(paramString, localObject1);
      paramString = "Participant.buildFromAdd…cipants[address] = this }";
      k.a(localObject1, paramString);
    }
    return (Participant)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */