package com.truecaller.messaging.transport;

import dagger.a.d;
import javax.inject.Provider;

public final class z
  implements d
{
  private final o a;
  private final Provider b;
  
  private z(o paramo, Provider paramProvider)
  {
    a = paramo;
    b = paramProvider;
  }
  
  public static z a(o paramo, Provider paramProvider)
  {
    z localz = new com/truecaller/messaging/transport/z;
    localz.<init>(paramo, paramProvider);
    return localz;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */