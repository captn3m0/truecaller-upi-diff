package com.truecaller.messaging.transport;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentValues;
import android.net.Uri;
import com.truecaller.log.AssertionUtil.AlwaysFatal;

public final class ad$a
{
  final int a;
  final Uri b;
  final ContentValues c;
  final String d;
  final String[] e;
  
  private ad$a(ad.a.a parama)
  {
    int i = a;
    a = i;
    Object localObject = b;
    b = ((Uri)localObject);
    localObject = c;
    c = ((ContentValues)localObject);
    localObject = d;
    d = ((String)localObject);
    parama = e;
    e = parama;
  }
  
  final ContentProviderOperation a()
  {
    int i = a;
    ContentProviderOperation.Builder localBuilder;
    switch (i)
    {
    default: 
      AssertionUtil.AlwaysFatal.fail(new String[] { "Unsupported operation" });
      return null;
    case 2: 
      localBuilder = ContentProviderOperation.newDelete(b);
      break;
    case 1: 
      localBuilder = ContentProviderOperation.newUpdate(b);
      break;
    case 0: 
      localBuilder = ContentProviderOperation.newInsert(b);
    }
    Object localObject = c;
    int j = ((ContentValues)localObject).size();
    if (j != 0)
    {
      localObject = c;
      localBuilder.withValues((ContentValues)localObject);
    }
    localObject = d;
    if (localObject != null)
    {
      String[] arrayOfString = e;
      localBuilder.withSelection((String)localObject, arrayOfString);
    }
    return localBuilder.build();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.ad.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */