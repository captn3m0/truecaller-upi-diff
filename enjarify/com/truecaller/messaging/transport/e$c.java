package com.truecaller.messaging.transport;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;

final class e$c
  extends u
{
  private final Message b;
  private final Participant[] c;
  private final int d;
  private final int e;
  
  private e$c(e parame, Message paramMessage, Participant[] paramArrayOfParticipant, int paramInt1, int paramInt2)
  {
    super(parame);
    b = paramMessage;
    c = paramArrayOfParticipant;
    d = paramInt1;
    e = paramInt2;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".scheduleMessage(");
    Object localObject = b;
    int i = 1;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = Integer.valueOf(d);
    i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Integer.valueOf(e), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.e.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */