package com.truecaller.messaging.transport.sms;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.provider.Telephony.Sms;
import com.truecaller.common.h.ab;
import com.truecaller.common.h.am;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.transport.c;
import com.truecaller.messaging.transport.c.a;
import com.truecaller.messaging.transport.f;
import com.truecaller.messaging.transport.i;
import com.truecaller.multisim.h;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.c.a.a.a.a;

final class g
  extends c
{
  private static final String[] d = tmp82_66;
  private static final String[] e = { "date_sent" };
  private static volatile int f = -1;
  private static volatile String[] g = null;
  
  static
  {
    String[] tmp5_2 = new String[16];
    String[] tmp6_5 = tmp5_2;
    String[] tmp6_5 = tmp5_2;
    tmp6_5[0] = "_id";
    tmp6_5[1] = "address";
    String[] tmp15_6 = tmp6_5;
    String[] tmp15_6 = tmp6_5;
    tmp15_6[2] = "body";
    tmp15_6[3] = "date";
    String[] tmp24_15 = tmp15_6;
    String[] tmp24_15 = tmp15_6;
    tmp24_15[4] = "error_code";
    tmp24_15[5] = "locked";
    String[] tmp33_24 = tmp24_15;
    String[] tmp33_24 = tmp24_15;
    tmp33_24[6] = "person";
    tmp33_24[7] = "protocol";
    String[] tmp44_33 = tmp33_24;
    String[] tmp44_33 = tmp33_24;
    tmp44_33[8] = "read";
    tmp44_33[9] = "reply_path_present";
    String[] tmp55_44 = tmp44_33;
    String[] tmp55_44 = tmp44_33;
    tmp55_44[10] = "seen";
    tmp55_44[11] = "service_center";
    String[] tmp66_55 = tmp55_44;
    String[] tmp66_55 = tmp55_44;
    tmp66_55[12] = "status";
    tmp66_55[13] = "subject";
    tmp66_55[14] = "thread_id";
    String[] tmp82_66 = tmp66_55;
    tmp82_66[15] = "type";
  }
  
  g(Context paramContext, h paramh, e parame)
  {
    super(paramContext, paramh, parame);
  }
  
  private String[] b(ContentResolver paramContentResolver)
  {
    Object localObject = g;
    if (localObject == null) {
      try
      {
        localObject = g;
        if (localObject == null)
        {
          localObject = d;
          boolean bool = a(paramContentResolver);
          if (bool)
          {
            paramContentResolver = "date_sent";
            paramContentResolver = a.b((Object[])localObject, paramContentResolver);
            localObject = paramContentResolver;
            localObject = (String[])paramContentResolver;
          }
          paramContentResolver = b;
          paramContentResolver = paramContentResolver.b();
          if (paramContentResolver != null)
          {
            paramContentResolver = a.b((Object[])localObject, paramContentResolver);
            localObject = paramContentResolver;
            localObject = (String[])paramContentResolver;
          }
          g = (String[])localObject;
        }
      }
      finally {}
    }
    return (String[])localObject;
  }
  
  public final c.a a(ContentResolver paramContentResolver, f paramf, i parami, org.a.a.b paramb1, org.a.a.b paramb2, boolean paramBoolean1, boolean paramBoolean2)
  {
    int i = 2;
    String[] arrayOfString1 = new String[i];
    Object localObject = paramb1;
    localObject = String.valueOf(a);
    arrayOfString1[0] = localObject;
    localObject = paramb2;
    long l = a;
    localObject = String.valueOf(l);
    int j = 1;
    arrayOfString1[j] = localObject;
    Uri localUri = Telephony.Sms.CONTENT_URI;
    String[] arrayOfString2 = b(paramContentResolver);
    String str1 = "date>=? AND date<=? AND type != 3";
    String str2 = "date DESC, _id DESC";
    Cursor localCursor = paramContentResolver.query(localUri, arrayOfString2, str1, arrayOfString1, str2);
    if (localCursor == null) {
      return null;
    }
    localObject = new com/truecaller/messaging/transport/sms/b;
    h localh = b;
    ((b)localObject).<init>(paramf, parami, localh, localCursor, paramBoolean1);
    return (c.a)localObject;
  }
  
  public final Set a(long paramLong, f paramf, i parami, Participant paramParticipant, boolean paramBoolean)
  {
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    localHashSet.add(paramParticipant);
    Iterator localIterator = paramf.a(paramLong).iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      Object localObject = (String)localIterator.next();
      if (paramBoolean) {
        localObject = ab.c((String)localObject);
      }
      localObject = parami.a((String)localObject);
      localHashSet.add(localObject);
    }
    return localHashSet;
  }
  
  public final boolean a(int paramInt)
  {
    int i = 1;
    paramInt &= i;
    if (paramInt == 0) {
      return i;
    }
    return false;
  }
  
  final boolean a(ContentResolver paramContentResolver)
  {
    int i = f;
    boolean bool = true;
    int j = -1;
    if (i == j) {}
    for (;;)
    {
      Uri localUri;
      String[] arrayOfString;
      String str;
      int k;
      try
      {
        i = f;
        if (i != j) {}
      }
      finally {}
      try
      {
        try
        {
          localUri = Telephony.Sms.CONTENT_URI;
          arrayOfString = e;
          str = "date_sent ASC LIMIT 1";
          paramContentResolver = paramContentResolver.query(localUri, arrayOfString, null, null, str);
          if (paramContentResolver != null) {
            paramContentResolver.close();
          }
          k = 1;
        }
        finally {}
      }
      catch (SQLException localSQLException) {}
    }
    k = 0;
    paramContentResolver = null;
    f = k;
    i = k;
    if (i != 0) {
      return bool;
    }
    return false;
  }
  
  public final boolean a(r paramr, c.a parama)
  {
    int i = paramr.i();
    int m = parama.g();
    int n = i & 0x20;
    if (n != 0)
    {
      n = m & 0x4;
      if (n != 0) {
        return false;
      }
    }
    n = 1;
    if (i == m)
    {
      boolean bool1 = paramr.h();
      boolean bool4 = parama.f();
      if (bool1 == bool4)
      {
        int j = paramr.n();
        if (j != n)
        {
          boolean bool2 = paramr.g();
          bool4 = parama.e();
          if (bool2 != bool4) {}
        }
        else
        {
          int k = paramr.n();
          if (k != n)
          {
            boolean bool3 = paramr.f();
            bool4 = parama.d();
            if (bool3 != bool4) {}
          }
          else
          {
            long l1 = paramr.c();
            int i1 = parama.h();
            long l2 = i1;
            boolean bool5 = l1 < l2;
            if (!bool5) {
              return false;
            }
          }
        }
      }
    }
    return n;
  }
  
  public final boolean a(f paramf, i parami, List paramList, r paramr, c.a parama, boolean paramBoolean)
  {
    paramf = parama.i();
    parami = (SmsTransportInfo)m;
    long l = paramr.a();
    paramf = paramf.m();
    parami = parami.g();
    a = l;
    parami = parami.a();
    paramf = paramf.a(0, parami);
    a = l;
    paramf = paramf.b();
    int i = paramr.n();
    int j = 1;
    if (i == j)
    {
      paramf = paramf.m();
      boolean bool = paramr.f();
      g = bool;
      bool = paramr.g();
      h = bool;
      paramf = paramf.b();
    }
    com.truecaller.messaging.data.b.a(paramList, paramf, -1);
    return j;
  }
  
  public final boolean b(r paramr, c.a parama)
  {
    int i = paramr.i();
    int j = 1;
    i &= j;
    if (i == 0)
    {
      paramr = paramr.m();
      parama = parama.j();
      boolean bool = am.a(paramr, parama);
      if (!bool) {
        return j;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */