package com.truecaller.messaging.transport.sms;

import android.content.ContentUris;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.net.Uri;
import android.provider.Telephony.Sms;
import android.text.TextUtils;
import com.truecaller.common.h.ab;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.transport.c.a;
import com.truecaller.messaging.transport.f;
import com.truecaller.messaging.transport.i;
import com.truecaller.multisim.h;
import java.util.List;
import org.c.a.a.a.k;

final class b
  extends CursorWrapper
  implements c.a
{
  private final int a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  private final int j;
  private final int k;
  private final int l;
  private final int m;
  private final int n;
  private final int o;
  private final int p;
  private final int q;
  private final f r;
  private final i s;
  private final boolean t;
  
  b(f paramf, i parami, h paramh, Cursor paramCursor, boolean paramBoolean)
  {
    super(paramCursor);
    int i1 = paramCursor.getColumnIndexOrThrow("_id");
    a = i1;
    i1 = paramCursor.getColumnIndexOrThrow("thread_id");
    b = i1;
    i1 = paramCursor.getColumnIndexOrThrow("status");
    c = i1;
    i1 = paramCursor.getColumnIndexOrThrow("protocol");
    d = i1;
    i1 = paramCursor.getColumnIndexOrThrow("type");
    e = i1;
    i1 = paramCursor.getColumnIndexOrThrow("service_center");
    f = i1;
    i1 = paramCursor.getColumnIndexOrThrow("error_code");
    g = i1;
    i1 = paramCursor.getColumnIndexOrThrow("reply_path_present");
    h = i1;
    i1 = paramCursor.getColumnIndexOrThrow("subject");
    i = i1;
    i1 = paramCursor.getColumnIndexOrThrow("seen");
    j = i1;
    i1 = paramCursor.getColumnIndexOrThrow("read");
    k = i1;
    i1 = paramCursor.getColumnIndexOrThrow("locked");
    l = i1;
    i1 = paramCursor.getColumnIndexOrThrow("date_sent");
    m = i1;
    i1 = paramCursor.getColumnIndexOrThrow("date");
    n = i1;
    i1 = paramCursor.getColumnIndexOrThrow("body");
    o = i1;
    String str = "address";
    i1 = paramCursor.getColumnIndexOrThrow(str);
    p = i1;
    r = paramf;
    s = parami;
    paramf = paramh.b();
    int i2;
    if (paramf != null) {
      i2 = paramCursor.getColumnIndex(paramf);
    } else {
      i2 = -1;
    }
    q = i2;
    t = paramBoolean;
  }
  
  private String a(String paramString)
  {
    boolean bool = t;
    if (bool) {
      paramString = ab.c(paramString);
    }
    return paramString;
  }
  
  public final long a()
  {
    int i1 = a;
    return getLong(i1);
  }
  
  public final long b()
  {
    int i1 = b;
    boolean bool = isNull(i1);
    if (bool) {
      return -1;
    }
    int i2 = b;
    return getLong(i2);
  }
  
  public final long c()
  {
    int i1 = n;
    return getLong(i1);
  }
  
  public final boolean d()
  {
    int i1 = j;
    i1 = getInt(i1);
    return i1 != 0;
  }
  
  public final boolean e()
  {
    int i1 = k;
    i1 = getInt(i1);
    return i1 != 0;
  }
  
  public final boolean f()
  {
    int i1 = l;
    i1 = getInt(i1);
    return i1 != 0;
  }
  
  public final int g()
  {
    int i1 = e;
    return SmsTransportInfo.a(getInt(i1));
  }
  
  public final int h()
  {
    int i1 = c;
    return getInt(i1);
  }
  
  public final Message i()
  {
    int i1 = p;
    Object localObject1 = k.n(getString(i1));
    Object localObject2 = a((String)localObject1);
    long l1 = a();
    Object localObject3 = new com/truecaller/messaging/transport/sms/SmsTransportInfo$a;
    ((SmsTransportInfo.a)localObject3).<init>();
    b = l1;
    int i2 = h();
    c = i2;
    long l2 = b();
    d = l2;
    i2 = d;
    i2 = getInt(i2);
    f = i2;
    i2 = e;
    i2 = getInt(i2);
    g = i2;
    i2 = f;
    Object localObject4 = getString(i2);
    h = ((String)localObject4);
    i2 = g;
    i2 = getInt(i2);
    i = i2;
    i2 = h;
    i2 = getInt(i2);
    int i3 = 1;
    if (i2 != 0)
    {
      i2 = 1;
    }
    else
    {
      i2 = 0;
      localObject4 = null;
    }
    j = i2;
    localObject4 = Telephony.Sms.CONTENT_URI;
    Object localObject5 = ContentUris.withAppendedId((Uri)localObject4, l1);
    e = ((Uri)localObject5);
    int i4 = i;
    localObject5 = getString(i4);
    k = ((String)localObject5);
    l = ((String)localObject2);
    localObject5 = ((SmsTransportInfo.a)localObject3).a();
    Object localObject6 = "-1";
    int i5 = q;
    if (i5 >= 0)
    {
      boolean bool2 = isNull(i5);
      if (!bool2)
      {
        int i6 = q;
        localObject6 = getString(i6);
      }
    }
    localObject3 = new com/truecaller/messaging/data/types/Message$a;
    ((Message.a)localObject3).<init>();
    i2 = m;
    long l3 = getLong(i2);
    localObject4 = ((Message.a)localObject3).d(l3);
    l3 = c();
    localObject4 = ((Message.a)localObject4).c(l3);
    int i7 = SmsTransportInfo.a(h);
    f = i7;
    boolean bool3 = d();
    g = bool3;
    bool3 = e();
    h = bool3;
    bool3 = f();
    i = bool3;
    localObject5 = ((Message.a)localObject4).a(0, (TransportInfo)localObject5).a((String)localObject6);
    i2 = o;
    localObject4 = k.n(getString(i2));
    l3 = -1;
    localObject6 = Entity.a("text/plain", 0, (String)localObject4, l3);
    localObject5 = ((Message.a)localObject5).a((Entity)localObject6);
    p = ((String)localObject1);
    localObject1 = s.a((String)localObject2);
    int i8 = c;
    if (i8 == i3)
    {
      i8 = b;
      boolean bool4 = isNull(i8);
      if (!bool4)
      {
        localObject2 = r;
        i4 = b;
        l1 = getLong(i4);
        localObject2 = ((f)localObject2).a(l1);
        i4 = ((List)localObject2).size();
        if (i4 == i3)
        {
          localObject2 = (String)((List)localObject2).get(0);
          localObject2 = a((String)localObject2);
          localObject5 = e;
          boolean bool1 = TextUtils.equals((CharSequence)localObject2, (CharSequence)localObject5);
          if (!bool1) {
            localObject1 = s.a((String)localObject2);
          }
        }
      }
    }
    c = ((Participant)localObject1);
    return ((Message.a)localObject3).b();
  }
  
  public final String j()
  {
    int i1 = p;
    String str = k.n(getString(i1));
    return a(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */