package com.truecaller.messaging.transport.sms;

import com.truecaller.messaging.transport.ad;
import java.util.LinkedHashSet;
import java.util.Set;

final class i
  extends ad
{
  final Set d;
  final boolean e;
  
  public i(boolean paramBoolean)
  {
    super(str);
    e = paramBoolean;
    Object localObject = new java/util/LinkedHashSet;
    ((LinkedHashSet)localObject).<init>();
    localObject = (Set)localObject;
    d = ((Set)localObject);
  }
  
  public final boolean a()
  {
    boolean bool = super.a();
    if (bool)
    {
      Set localSet = d;
      bool = localSet.isEmpty();
      if (bool) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */