package com.truecaller.messaging.transport.sms;

import android.net.Uri;

public final class SmsTransportInfo$a
{
  public long a;
  public long b;
  public int c = -1;
  public long d;
  public Uri e;
  public int f;
  public int g;
  public String h;
  public int i;
  public boolean j;
  public String k;
  public String l;
  
  public SmsTransportInfo$a() {}
  
  private SmsTransportInfo$a(SmsTransportInfo paramSmsTransportInfo)
  {
    long l1 = a;
    a = l1;
    l1 = b;
    b = l1;
    int m = c;
    c = m;
    l1 = d;
    d = l1;
    Object localObject = e;
    e = ((Uri)localObject);
    m = g;
    f = m;
    m = h;
    g = m;
    localObject = i;
    h = ((String)localObject);
    m = j;
    i = m;
    boolean bool = k;
    j = bool;
    localObject = f;
    k = ((String)localObject);
    paramSmsTransportInfo = l;
    l = paramSmsTransportInfo;
  }
  
  public final a a(int paramInt)
  {
    c = paramInt;
    return this;
  }
  
  public final a a(long paramLong)
  {
    b = paramLong;
    return this;
  }
  
  public final a a(Uri paramUri)
  {
    e = paramUri;
    return this;
  }
  
  public final a a(String paramString)
  {
    h = paramString;
    return this;
  }
  
  public final a a(boolean paramBoolean)
  {
    j = paramBoolean;
    return this;
  }
  
  public final SmsTransportInfo a()
  {
    SmsTransportInfo localSmsTransportInfo = new com/truecaller/messaging/transport/sms/SmsTransportInfo;
    localSmsTransportInfo.<init>(this, (byte)0);
    return localSmsTransportInfo;
  }
  
  public final a b(int paramInt)
  {
    f = paramInt;
    return this;
  }
  
  public final a b(long paramLong)
  {
    d = paramLong;
    return this;
  }
  
  public final a b(String paramString)
  {
    k = paramString;
    return this;
  }
  
  public final a c(int paramInt)
  {
    g = paramInt;
    return this;
  }
  
  public final a c(long paramLong)
  {
    a = paramLong;
    return this;
  }
  
  public final a c(String paramString)
  {
    l = paramString;
    return this;
  }
  
  public final a d(int paramInt)
  {
    i = paramInt;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.SmsTransportInfo.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */