package com.truecaller.messaging.transport.sms;

import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.RemoteException;
import android.provider.Telephony.Sms;
import android.provider.Telephony.Sms.Inbox;
import android.provider.Telephony.Sms.Intents;
import android.provider.Telephony.Sms.Outbox;
import android.provider.Telephony.Threads;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.telephony.SmsMessage.MessageClass;
import android.text.TextUtils;
import c.a.m;
import c.x;
import com.truecaller.TrueApp;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.w;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.transport.ad;
import com.truecaller.messaging.transport.ad.a;
import com.truecaller.messaging.transport.ad.a.a;
import com.truecaller.messaging.transport.ad.b;
import com.truecaller.messaging.transport.k.b;
import com.truecaller.messaging.transport.l.a;
import com.truecaller.multisim.SimInfo;
import com.truecaller.payments.j;
import com.truecaller.tracking.events.u.a;
import com.truecaller.utils.d;
import com.truecaller.utils.q;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

final class h
  implements com.truecaller.messaging.transport.l
{
  public static final Uri a;
  public static final String b;
  private static final SmsMessage[] c = new SmsMessage[0];
  private final Context d;
  private final com.truecaller.androidactors.f e;
  private final d f;
  private final HandlerThread g;
  private final com.truecaller.androidactors.f h;
  private final g i;
  private final com.truecaller.messaging.h j;
  private final ContentObserver k;
  private final com.truecaller.androidactors.f l;
  private final com.truecaller.multisim.h m;
  private final com.truecaller.common.h.u n;
  private final ad.b o;
  private final com.truecaller.analytics.b p;
  private final com.truecaller.utils.l q;
  private final j r;
  private final com.truecaller.smsparser.a s;
  private final com.truecaller.featuretoggles.e t;
  private final com.truecaller.messaging.a u;
  private h.a v = null;
  private boolean w = false;
  
  static
  {
    Uri localUri = Telephony.Sms.CONTENT_URI;
    a = localUri;
    b = localUri.getAuthority();
  }
  
  h(Context paramContext, com.truecaller.androidactors.f paramf1, HandlerThread paramHandlerThread, d paramd, com.truecaller.androidactors.f paramf2, g paramg, com.truecaller.messaging.h paramh, com.truecaller.androidactors.f paramf3, com.truecaller.multisim.h paramh1, com.truecaller.common.h.u paramu, ad.b paramb, com.truecaller.analytics.b paramb1, com.truecaller.utils.l paraml, j paramj, com.truecaller.smsparser.a parama, com.truecaller.featuretoggles.e parame, com.truecaller.messaging.a parama1)
  {
    Object localObject1 = paramContext;
    d = paramContext;
    Object localObject2 = paramHandlerThread;
    g = paramHandlerThread;
    Object localObject3 = paramf1;
    e = paramf1;
    localObject3 = paramd;
    f = paramd;
    h = paramf2;
    localObject3 = new com/truecaller/messaging/transport/sms/h$b;
    localObject2 = paramHandlerThread.getLooper();
    localObject1 = paramContext.getContentResolver();
    ((h.b)localObject3).<init>((Looper)localObject2, (ContentResolver)localObject1, paramf2);
    k = ((ContentObserver)localObject3);
    i = paramg;
    j = paramh;
    l = paramf3;
    m = paramh1;
    n = paramu;
    o = paramb;
    p = paramb1;
    q = paraml;
    r = paramj;
    s = parama;
    t = parame;
    u = parama1;
  }
  
  private Uri a(Context paramContext, Message paramMessage)
  {
    Object localObject1 = c;
    String str1 = null;
    Object localObject2 = new String[0];
    AssertionUtil.isNotNull(localObject1, (String[])localObject2);
    localObject1 = c.f;
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1);
    localObject2 = new String[0];
    AssertionUtil.isFalse(bool1, (String[])localObject2);
    bool1 = g();
    localObject2 = null;
    if (!bool1)
    {
      new String[1][0] = "Sms permissions is not granted";
      return null;
    }
    localObject1 = new android/content/ContentValues;
    ((ContentValues)localObject1).<init>();
    long l1 = -1;
    long l2;
    try
    {
      localObject3 = c;
      localObject3 = f;
      l2 = Telephony.Threads.getOrCreateThreadId(paramContext, (String)localObject3);
    }
    catch (IllegalArgumentException|SecurityException localIllegalArgumentException)
    {
      l2 = l1;
    }
    int i2 = l2 < l1;
    if (i2 == 0)
    {
      paramContext = new java/lang/StringBuilder;
      paramContext.<init>("For some reasons we can not create thread for address. Is empty: ");
      str1 = c.f;
      bool1 = org.c.a.a.a.k.a("insert-address-token", str1);
      paramContext.append(bool1);
      paramContext.append(" type: ");
      int i1 = c.c;
      paramContext.append(i1);
      paramContext.append(" address length: ");
      localObject1 = c.f;
      int i3;
      if (localObject1 == null)
      {
        i3 = -1;
      }
      else
      {
        paramMessage = c.f;
        i3 = paramMessage.length();
      }
      paramContext.append(i3);
      AssertionUtil.reportWeirdnessButNeverCrash(paramContext.toString());
      return null;
    }
    Object localObject4 = paramMessage.j();
    boolean bool3 = TextUtils.isEmpty((CharSequence)localObject4);
    i2 = 1;
    if (bool3)
    {
      paramContext = new String[i2];
      paramMessage = String.valueOf(paramMessage);
      paramMessage = "Message does not have a content: ".concat(paramMessage);
      paramContext[0] = paramMessage;
      AssertionUtil.OnlyInDebug.fail(paramContext);
      return null;
    }
    SmsTransportInfo localSmsTransportInfo = (SmsTransportInfo)m;
    String str2 = "thread_id";
    Object localObject3 = Long.valueOf(l2);
    ((ContentValues)localObject1).put(str2, (Long)localObject3);
    Object localObject5 = c.f;
    ((ContentValues)localObject1).put("address", (String)localObject5);
    long l3 = e.a;
    localObject5 = Long.valueOf(l3);
    ((ContentValues)localObject1).put("date", (Long)localObject5);
    localObject3 = i;
    localObject5 = paramContext.getContentResolver();
    boolean bool4 = ((g)localObject3).a((ContentResolver)localObject5);
    if (bool4)
    {
      localObject3 = "date_sent";
      l3 = d.a;
      localObject5 = Long.valueOf(l3);
      ((ContentValues)localObject1).put((String)localObject3, (Long)localObject5);
    }
    localObject5 = Integer.valueOf(i);
    ((ContentValues)localObject1).put("locked", (Integer)localObject5);
    localObject5 = Integer.valueOf(g);
    ((ContentValues)localObject1).put("seen", (Integer)localObject5);
    localObject5 = Integer.valueOf(h);
    ((ContentValues)localObject1).put("read", (Integer)localObject5);
    localObject5 = Integer.valueOf(j);
    ((ContentValues)localObject1).put("error_code", (Integer)localObject5);
    localObject5 = Boolean.valueOf(k);
    ((ContentValues)localObject1).put("reply_path_present", (Boolean)localObject5);
    localObject5 = Integer.valueOf(g);
    ((ContentValues)localObject1).put("protocol", (Integer)localObject5);
    localObject5 = i;
    ((ContentValues)localObject1).put("service_center", (String)localObject5);
    localObject5 = f;
    ((ContentValues)localObject1).put("subject", (String)localObject5);
    int i5 = c;
    localObject5 = Integer.valueOf(i5);
    ((ContentValues)localObject1).put("status", (Integer)localObject5);
    localObject3 = "body";
    ((ContentValues)localObject1).put((String)localObject3, (String)localObject4);
    localObject4 = m.b();
    if (localObject4 != null)
    {
      localObject3 = l;
      ((ContentValues)localObject1).put((String)localObject4, (String)localObject3);
    }
    paramContext = paramContext.getContentResolver();
    try
    {
      localObject4 = e;
      if (localObject4 != null)
      {
        localObject4 = "type";
        int i4 = f;
        i4 = SmsTransportInfo.b(i4);
        localObject3 = Integer.valueOf(i4);
        ((ContentValues)localObject1).put((String)localObject4, (Integer)localObject3);
        localObject4 = e;
        int i6 = paramContext.update((Uri)localObject4, (ContentValues)localObject1, null, null);
        if (i6 != 0) {
          localObject2 = e;
        }
      }
      else
      {
        int i7 = f & i2;
        if (i7 == 0) {
          localObject4 = Telephony.Sms.Inbox.CONTENT_URI;
        } else {
          localObject4 = Telephony.Sms.Outbox.CONTENT_URI;
        }
        localObject2 = paramContext.insert((Uri)localObject4, (ContentValues)localObject1);
      }
    }
    catch (RuntimeException paramContext)
    {
      AssertionUtil.reportThrowableButNeverCrash(paramContext);
    }
    if (localObject2 != null)
    {
      paramContext = e;
      paramMessage = j;
      l1 = paramMessage.a(0);
      boolean bool2 = paramContext.d(l1);
      if (bool2)
      {
        paramMessage = j;
        l1 = a;
        paramMessage.a(0, l1);
      }
    }
    return (Uri)localObject2;
  }
  
  private Message a(SmsMessage[] paramArrayOfSmsMessage, String paramString, int paramInt)
  {
    Object localObject1 = paramArrayOfSmsMessage[0];
    SmsTransportInfo.a locala = new com/truecaller/messaging/transport/sms/SmsTransportInfo$a;
    locala.<init>();
    Object localObject2 = ((SmsMessage)localObject1).getServiceCenterAddress();
    h = ((String)localObject2);
    i = paramInt;
    paramInt = ((SmsMessage)localObject1).getProtocolIdentifier();
    f = paramInt;
    paramInt = ((SmsMessage)localObject1).isReplyPathPresent();
    j = paramInt;
    paramInt = ((SmsMessage)localObject1).getStatus();
    c = paramInt;
    Object localObject3 = ((SmsMessage)localObject1).getPseudoSubject();
    boolean bool = TextUtils.isEmpty((CharSequence)localObject3);
    if (!bool) {
      k = ((String)localObject3);
    }
    localObject3 = new com/truecaller/messaging/data/types/Message$a;
    ((Message.a)localObject3).<init>();
    localObject2 = (String)org.c.a.a.a.k.e(((SmsMessage)localObject1).getDisplayOriginatingAddress(), "Unknown sender");
    com.truecaller.common.h.u localu = n;
    localObject2 = Participant.a((String)localObject2, localu, paramString);
    c = ((Participant)localObject2);
    long l1 = System.currentTimeMillis();
    localObject2 = ((Message.a)localObject3).c(l1);
    long l2 = ((SmsMessage)localObject1).getTimestampMillis();
    localObject1 = ((Message.a)localObject2).d(l2);
    paramArrayOfSmsMessage = a(paramArrayOfSmsMessage);
    paramArrayOfSmsMessage = Entity.a("text/plain", 0, paramArrayOfSmsMessage, -1);
    paramArrayOfSmsMessage = ((Message.a)localObject1).a(paramArrayOfSmsMessage).a(paramString);
    paramString = locala.a();
    paramArrayOfSmsMessage.a(0, paramString);
    return ((Message.a)localObject3).b();
  }
  
  /* Error */
  private static String a(Context paramContext, long paramLong, String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 101	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   4: astore 4
    //   6: aconst_null
    //   7: astore_0
    //   8: lload_1
    //   9: invokestatic 464	com/truecaller/content/TruecallerContract$ab:a	(J)Landroid/net/Uri;
    //   12: astore 5
    //   14: ldc_w 466
    //   17: astore 6
    //   19: ldc_w 468
    //   22: astore 7
    //   24: iconst_2
    //   25: anewarray 137	java/lang/String
    //   28: dup
    //   29: iconst_0
    //   30: aload 6
    //   32: aastore
    //   33: dup
    //   34: iconst_1
    //   35: aload 7
    //   37: aastore
    //   38: astore 8
    //   40: ldc_w 470
    //   43: astore 9
    //   45: iconst_1
    //   46: istore 10
    //   48: iload 10
    //   50: anewarray 137	java/lang/String
    //   53: astore 11
    //   55: aload_3
    //   56: invokestatic 221	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   59: astore 7
    //   61: aconst_null
    //   62: astore_3
    //   63: aload 11
    //   65: iconst_0
    //   66: aload 7
    //   68: aastore
    //   69: ldc_w 472
    //   72: astore 12
    //   74: aload 4
    //   76: aload 5
    //   78: aload 8
    //   80: aload 9
    //   82: aload 11
    //   84: aload 12
    //   86: invokevirtual 476	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   89: astore 7
    //   91: aload 7
    //   93: ifnull +73 -> 166
    //   96: aload 7
    //   98: invokeinterface 481 1 0
    //   103: istore 13
    //   105: iload 13
    //   107: ifeq +59 -> 166
    //   110: aload 7
    //   112: iconst_0
    //   113: invokeinterface 485 2 0
    //   118: astore_3
    //   119: aload_3
    //   120: invokestatic 487	org/c/a/a/a/k:n	(Ljava/lang/String;)Ljava/lang/String;
    //   123: astore_3
    //   124: aload 7
    //   126: iload 10
    //   128: invokeinterface 490 2 0
    //   133: istore 10
    //   135: iload 10
    //   137: ifeq +6 -> 143
    //   140: goto +7 -> 147
    //   143: ldc_w 492
    //   146: astore_3
    //   147: aload 7
    //   149: ifnull +10 -> 159
    //   152: aload 7
    //   154: invokeinterface 495 1 0
    //   159: aload_3
    //   160: areturn
    //   161: astore 6
    //   163: goto +21 -> 184
    //   166: aload 7
    //   168: ifnull +33 -> 201
    //   171: goto +23 -> 194
    //   174: astore 6
    //   176: goto +32 -> 208
    //   179: astore 6
    //   181: aconst_null
    //   182: astore 7
    //   184: aload 6
    //   186: invokestatic 358	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   189: aload 7
    //   191: ifnull +10 -> 201
    //   194: aload 7
    //   196: invokeinterface 495 1 0
    //   201: aconst_null
    //   202: areturn
    //   203: astore 6
    //   205: aload 7
    //   207: astore_0
    //   208: aload_0
    //   209: ifnull +9 -> 218
    //   212: aload_0
    //   213: invokeinterface 495 1 0
    //   218: aload 6
    //   220: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	221	0	paramContext	Context
    //   0	221	1	paramLong	long
    //   0	221	3	paramString	String
    //   4	71	4	localContentResolver	ContentResolver
    //   12	65	5	localUri	Uri
    //   17	14	6	str1	String
    //   161	1	6	localRuntimeException1	RuntimeException
    //   174	1	6	localObject1	Object
    //   179	6	6	localRuntimeException2	RuntimeException
    //   203	16	6	localObject2	Object
    //   22	184	7	localObject3	Object
    //   38	41	8	arrayOfString1	String[]
    //   43	38	9	str2	String
    //   46	90	10	i1	int
    //   53	30	11	arrayOfString2	String[]
    //   72	13	12	str3	String
    //   103	3	13	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   96	103	161	java/lang/RuntimeException
    //   112	118	161	java/lang/RuntimeException
    //   119	123	161	java/lang/RuntimeException
    //   126	133	161	java/lang/RuntimeException
    //   8	12	174	finally
    //   24	38	174	finally
    //   48	53	174	finally
    //   55	59	174	finally
    //   66	69	174	finally
    //   84	89	174	finally
    //   8	12	179	java/lang/RuntimeException
    //   24	38	179	java/lang/RuntimeException
    //   48	53	179	java/lang/RuntimeException
    //   55	59	179	java/lang/RuntimeException
    //   66	69	179	java/lang/RuntimeException
    //   84	89	179	java/lang/RuntimeException
    //   96	103	203	finally
    //   112	118	203	finally
    //   119	123	203	finally
    //   126	133	203	finally
    //   184	189	203	finally
  }
  
  private static String a(SmsMessage[] paramArrayOfSmsMessage)
  {
    int i1 = paramArrayOfSmsMessage.length;
    int i2 = 0;
    int i3 = 1;
    if (i1 == i3) {
      return b(paramArrayOfSmsMessage[0].getDisplayMessageBody());
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    i3 = paramArrayOfSmsMessage.length;
    while (i2 < i3)
    {
      Object localObject = paramArrayOfSmsMessage[i2];
      try
      {
        localObject = ((SmsMessage)localObject).getDisplayMessageBody();
        localStringBuilder.append((String)localObject);
      }
      catch (NullPointerException localNullPointerException)
      {
        for (;;) {}
      }
      i2 += 1;
    }
    return b(localStringBuilder.toString());
  }
  
  private boolean a(i parami)
  {
    boolean bool1 = e;
    if (!bool1) {
      return false;
    }
    Object localObject1 = (Iterable)d;
    int i1 = 499;
    localObject1 = ((Iterable)m.e((Iterable)localObject1, i1)).iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break label336;
      }
      Object localObject2 = (List)((Iterator)localObject1).next();
      Object localObject3 = Telephony.Sms.CONTENT_URI;
      localObject3 = parami.b((Uri)localObject3);
      Object localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>("_id IN (");
      localObject2 = (Iterable)localObject2;
      CharSequence localCharSequence = (CharSequence)",";
      Object localObject5 = i.a.a;
      Object localObject6 = localObject5;
      localObject6 = (c.g.a.b)localObject5;
      int i2 = 30;
      Object localObject7 = localObject2;
      localObject5 = m.a((Iterable)localObject2, localCharSequence, null, null, 0, null, (c.g.a.b)localObject6, i2);
      ((StringBuilder)localObject4).append((String)localObject5);
      char c1 = ')';
      ((StringBuilder)localObject4).append(c1);
      localObject4 = ((StringBuilder)localObject4).toString();
      localObject5 = new java/util/ArrayList;
      int i3 = m.a((Iterable)localObject2, 10);
      ((ArrayList)localObject5).<init>(i3);
      localObject5 = (Collection)localObject5;
      localObject2 = ((Iterable)localObject2).iterator();
      for (;;)
      {
        boolean bool3 = ((Iterator)localObject2).hasNext();
        if (!bool3) {
          break;
        }
        long l1 = ((Number)((Iterator)localObject2).next()).longValue();
        localObject7 = String.valueOf(l1);
        ((Collection)localObject5).add(localObject7);
      }
      localObject5 = (Collection)localObject5;
      localObject2 = new String[0];
      localObject2 = ((Collection)localObject5).toArray((Object[])localObject2);
      if (localObject2 == null) {
        break;
      }
      localObject2 = (String[])localObject2;
      ((ad.a.a)localObject3).a((String)localObject4, (String[])localObject2);
      localObject2 = ((ad.a.a)localObject3).a();
      parami.a((ad.a)localObject2);
    }
    parami = new c/u;
    parami.<init>("null cannot be cast to non-null type kotlin.Array<T>");
    throw parami;
    label336:
    localObject1 = x.a;
    localObject1 = d;
    ((Set)localObject1).clear();
    try
    {
      localObject1 = o;
      parami = ((ad.b)localObject1).a(parami);
      int i4 = parami.length;
      return i4 != 0;
    }
    catch (OperationApplicationException parami) {}catch (SecurityException parami) {}catch (RemoteException parami) {}
    AssertionUtil.reportThrowableButNeverCrash(parami);
    return false;
  }
  
  private static SmsMessage[] a(Intent paramIntent)
  {
    Object localObject1 = paramIntent.getExtras();
    if (localObject1 == null) {
      return c;
    }
    Object localObject2 = ((Bundle)localObject1).get("pdus");
    if (localObject2 == null)
    {
      paramIntent = new java/lang/StringBuilder;
      paramIntent.<init>("Intent from Telephony.Sms.Intents.SMS_RECEIVED_ACTION does not have pdus extra, but has: [");
      localObject1 = org.c.a.a.a.k.a(((Bundle)localObject1).keySet(), ',');
      paramIntent.append((String)localObject1);
      paramIntent.append("]");
      AssertionUtil.reportWeirdnessButNeverCrash(paramIntent.toString());
      return c;
    }
    paramIntent = Telephony.Sms.Intents.getMessagesFromIntent(paramIntent);
    if (paramIntent != null)
    {
      int i1 = paramIntent.length;
      if (i1 > 0) {
        return paramIntent;
      }
    }
    return c;
  }
  
  private static String b(String paramString)
  {
    if (paramString == null) {
      return "";
    }
    return paramString.replace('\f', '\n');
  }
  
  private boolean c(String paramString)
  {
    SimInfo localSimInfo = m.b(paramString);
    paramString = m.c(paramString);
    if (localSimInfo == null) {
      return false;
    }
    boolean bool = paramString.b();
    if (bool)
    {
      paramString = j;
      int i1 = a;
      bool = paramString.b(i1);
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  private boolean g()
  {
    d locald = f;
    String str = u.b();
    return locald.a(str);
  }
  
  private boolean h()
  {
    d locald = f;
    String str = u.b();
    return locald.b(str);
  }
  
  public final long a(long paramLong)
  {
    return paramLong;
  }
  
  public final long a(com.truecaller.messaging.transport.f paramf, com.truecaller.messaging.transport.i parami, r paramr, org.a.a.b paramb1, org.a.a.b paramb2, int paramInt, List paramList, q paramq, boolean paramBoolean1, boolean paramBoolean2, Set paramSet)
  {
    com.truecaller.utils.l locall = q;
    String[] arrayOfString = { "android.permission.READ_SMS" };
    boolean bool = locall.a(arrayOfString);
    if (!bool)
    {
      new String[1][0] = "SMS permissions is not granted";
      return 0L;
    }
    return i.a(paramf, parami, paramr, paramb1, paramb2, paramInt, paramList, paramq, paramBoolean1, paramBoolean2, paramSet);
  }
  
  public final l.a a(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    h localh = this;
    Object localObject1 = paramArrayOfParticipant;
    int i1 = paramArrayOfParticipant.length;
    int i2 = 1;
    if (i1 > 0)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      localObject2 = null;
    }
    Object localObject3 = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(i1, (String[])localObject3);
    boolean bool1 = g();
    if (!bool1)
    {
      new String[1][0] = "We are not default SMS app and can not send message";
      localObject1 = new com/truecaller/messaging/transport/l$a;
      ((l.a)localObject1).<init>(0);
      return (l.a)localObject1;
    }
    Object localObject2 = n;
    localObject3 = paramMessage;
    String str1 = l;
    localObject2 = ((com.truecaller.common.h.u)localObject2).a(str1);
    int i3 = localObject1.length;
    Object localObject4 = null;
    int i4 = 0;
    while (i4 < i3)
    {
      SmsTransportInfo.a locala = localObject1[i4];
      int i5 = localObject1.length;
      Object localObject5;
      if (i5 > i2)
      {
        localObject5 = f;
        boolean bool2 = org.c.a.a.a.k.a((CharSequence)localObject2, (CharSequence)localObject5);
        if (bool2) {}
      }
      else
      {
        localObject4 = paramMessage.m();
        c = locala;
        localObject4 = ((Message.a)localObject4).b();
        int i7 = j;
        int i6 = 3;
        if (i7 == i6)
        {
          locala = new com/truecaller/messaging/transport/sms/SmsTransportInfo$a;
          locala.<init>();
          long l1 = a;
          a = l1;
        }
        else
        {
          locala = ((SmsTransportInfo)m).g();
        }
        i6 = 6;
        g = i6;
        Object localObject6 = d;
        long l2 = b;
        String str2 = l;
        localObject6 = a((Context)localObject6, l2, str2);
        boolean bool3 = org.c.a.a.a.k.e((CharSequence)localObject6);
        if (bool3) {
          h = ((String)localObject6);
        }
        localObject6 = l;
        boolean bool4 = localh.c((String)localObject6);
        int i8;
        if (bool4)
        {
          i8 = 32;
          c = i8;
        }
        else
        {
          i8 = -1;
          c = i8;
        }
        localObject4 = ((Message)localObject4).m();
        localObject6 = locala.a();
        localObject4 = ((Message.a)localObject4).a(0, (TransportInfo)localObject6).b();
        localObject6 = d;
        localObject4 = localh.a((Context)localObject6, (Message)localObject4);
        if (localObject4 == null)
        {
          localObject4 = null;
        }
        else
        {
          long l3 = ContentUris.parseId((Uri)localObject4);
          long l4 = 0L;
          boolean bool5 = l3 < l4;
          if (!bool5)
          {
            localObject4 = null;
          }
          else
          {
            ContentValues localContentValues = new android/content/ContentValues;
            localContentValues.<init>();
            str2 = "type";
            localObject5 = Integer.valueOf(i6);
            localContentValues.put(str2, (Integer)localObject5);
            localObject5 = d.getContentResolver();
            i6 = ((ContentResolver)localObject5).update((Uri)localObject4, localContentValues, null, null);
            if (i6 == 0)
            {
              localObject4 = null;
            }
            else
            {
              e = ((Uri)localObject4);
              b = l3;
              localObject4 = locala.a();
            }
          }
        }
        if (localObject4 == null)
        {
          localObject1 = new com/truecaller/messaging/transport/l$a;
          ((l.a)localObject1).<init>(0);
          return (l.a)localObject1;
        }
      }
      i4 += 1;
    }
    localObject2 = new String[0];
    AssertionUtil.AlwaysFatal.isNotNull(localObject4, (String[])localObject2);
    int i9 = localObject1.length;
    if (i9 > i2)
    {
      localObject1 = new com/truecaller/messaging/transport/l$a;
      ((l.a)localObject1).<init>(2);
      return (l.a)localObject1;
    }
    localObject1 = new com/truecaller/messaging/transport/l$a;
    ((l.a)localObject1).<init>((TransportInfo)localObject4);
    return (l.a)localObject1;
  }
  
  public final String a()
  {
    return "sms";
  }
  
  public final String a(String paramString)
  {
    return paramString;
  }
  
  public final void a(Intent paramIntent, int paramInt)
  {
    Object localObject1 = paramIntent.getAction();
    Object localObject2 = "com.truecaller.messaging.SmsStatusReceived.SMS_SENT";
    boolean bool1 = ((String)localObject2).equals(localObject1);
    Object localObject3 = null;
    boolean bool3 = true;
    Object localObject4;
    long l2;
    Object localObject7;
    label298:
    int i6;
    if (bool1)
    {
      localObject1 = "message_part";
      int i1 = -1;
      int i3 = paramIntent.getIntExtra((String)localObject1, i1);
      if (i3 == i1)
      {
        AssertionUtil.OnlyInDebug.fail(new String[] { "Invalid message part" });
        return;
      }
      localObject4 = "errorCode";
      int i5 = paramIntent.getIntExtra((String)localObject4, 0);
      paramIntent = paramIntent.getData();
      Object localObject5 = g;
      String[] arrayOfString = new String[0];
      AssertionUtil.onSameThread((Thread)localObject5, arrayOfString);
      localObject5 = v;
      if (localObject5 != null)
      {
        long l1 = i3;
        boolean bool4 = ((h.a)localObject5).a(paramIntent, paramInt, l1);
        if (!bool4) {}
      }
      else
      {
        localObject1 = new android/content/ContentValues;
        ((ContentValues)localObject1).<init>();
        if (paramInt != i1)
        {
          i1 = 4;
          if (paramInt != i1)
          {
            switch (paramInt)
            {
            default: 
              break;
            case 1: 
              localObject6 = "error_code";
              localObject2 = Integer.valueOf(i5);
              ((ContentValues)localObject1).put((String)localObject6, (Integer)localObject2);
            }
          }
          else
          {
            i1 = 5;
            localObject2 = Integer.valueOf(i1);
            ((ContentValues)localObject1).put("type", (Integer)localObject2);
            localObject6 = "seen";
            localObject2 = Integer.valueOf(0);
            ((ContentValues)localObject1).put((String)localObject6, (Integer)localObject2);
            break label298;
          }
        }
        else
        {
          i1 = 2;
          localObject2 = Integer.valueOf(i1);
          ((ContentValues)localObject1).put("type", (Integer)localObject2);
          localObject6 = "date_sent";
          l2 = System.currentTimeMillis();
          localObject2 = Long.valueOf(l2);
          ((ContentValues)localObject1).put((String)localObject6, (Long)localObject2);
        }
        bool3 = false;
        localObject7 = null;
        try
        {
          localObject6 = d;
          localObject6 = ((Context)localObject6).getContentResolver();
          i6 = ((ContentResolver)localObject6).update(paramIntent, (ContentValues)localObject1, null, null);
          if (i6 > 0)
          {
            paramIntent = h;
            paramIntent = paramIntent.a();
            paramIntent = (t)paramIntent;
            localObject6 = Integer.valueOf(0);
            localObject6 = Collections.singleton(localObject6);
            paramIntent.a(bool3, (Set)localObject6);
          }
        }
        catch (RuntimeException paramIntent)
        {
          AssertionUtil.reportThrowableButNeverCrash(paramIntent);
        }
        v = null;
      }
      return;
    }
    Object localObject6 = "com.truecaller.messaging.SmsStatusReceived.SMS_STATUS";
    paramInt = ((String)localObject6).equals(localObject1);
    if (paramInt != 0)
    {
      localObject6 = "date";
      long l3 = -1;
      l2 = paramIntent.getLongExtra((String)localObject6, l3);
      paramInt = l2 < l3;
      if (paramInt == 0)
      {
        AssertionUtil.OnlyInDebug.fail(new String[] { "Invalid message date" });
        return;
      }
      localObject6 = paramIntent.getData();
      localObject1 = new org/a/a/b;
      ((org.a.a.b)localObject1).<init>(l2);
      localObject2 = g;
      localObject7 = new String[0];
      AssertionUtil.onSameThread((Thread)localObject2, (String[])localObject7);
      boolean bool2 = g();
      if (bool2)
      {
        localObject2 = "pdu";
        paramIntent = paramIntent.getByteArrayExtra((String)localObject2);
        if (paramIntent == null)
        {
          AssertionUtil.reportWeirdnessButNeverCrash("PDU is null in delivery report");
          return;
        }
        paramIntent = SmsMessage.createFromPdu(paramIntent);
        if (paramIntent == null)
        {
          AssertionUtil.reportWeirdnessButNeverCrash("Can not decode message");
          return;
        }
        localObject2 = new android/content/ContentValues;
        ((ContentValues)localObject2).<init>();
        l2 = System.currentTimeMillis();
        localObject4 = Long.valueOf(l2);
        ((ContentValues)localObject2).put("date_sent", (Long)localObject4);
        localObject7 = "status";
        i6 = paramIntent.getStatus();
        paramIntent = Integer.valueOf(i6);
        ((ContentValues)localObject2).put((String)localObject7, paramIntent);
        try
        {
          paramIntent = d;
          paramIntent = paramIntent.getContentResolver();
          paramIntent.update((Uri)localObject6, (ContentValues)localObject2, null, null);
          paramIntent = (t)h.a();
          paramIntent.a(0, (org.a.a.b)localObject1, false);
        }
        catch (RuntimeException localRuntimeException)
        {
          AssertionUtil.reportThrowableButNeverCrash(localRuntimeException);
          return;
        }
      }
      return;
    }
    localObject6 = "android.provider.Telephony.SMS_RECEIVED";
    paramInt = org.c.a.a.a.k.a((CharSequence)localObject1, (CharSequence)localObject6);
    int i4;
    if (paramInt != 0)
    {
      localObject6 = g;
      localObject1 = new String[0];
      AssertionUtil.onSameThread((Thread)localObject6, (String[])localObject1);
      localObject6 = new com/truecaller/analytics/e$a;
      ((e.a)localObject6).<init>("MessageReceived");
      localObject6 = ((e.a)localObject6).a("Type", "sms");
      localObject1 = "AppId";
      localObject2 = u.b();
      localObject3 = "NotSupported";
      if (localObject2 == null) {
        localObject2 = localObject3;
      }
      localObject6 = ((e.a)localObject6).a((String)localObject1, (String)localObject2).a();
      localObject1 = p;
      ((com.truecaller.analytics.b)localObject1).a((com.truecaller.analytics.e)localObject6);
      paramInt = h();
      if (paramInt == 0)
      {
        localObject6 = new String[bool3];
        localObject2 = String.valueOf(paramIntent);
        localObject1 = "Received intent: ".concat((String)localObject2);
        localObject6[0] = localObject1;
        localObject6 = a(paramIntent);
        i4 = localObject6.length;
        if (i4 != 0)
        {
          localObject1 = "errorCode";
          i4 = paramIntent.getIntExtra((String)localObject1, 0);
          localObject2 = m;
          paramIntent = ((com.truecaller.multisim.h)localObject2).a(paramIntent);
          paramIntent = a((SmsMessage[])localObject6, paramIntent, i4);
          ((com.truecaller.messaging.notifications.a)l.a()).a(paramIntent);
          localObject6 = t.w();
          paramInt = ((com.truecaller.featuretoggles.b)localObject6).a();
          if (paramInt == 0)
          {
            localObject6 = r;
            ((j)localObject6).a(paramIntent);
          }
          localObject6 = q;
          localObject1 = new String[] { "android.permission.READ_SMS" };
          paramInt = ((com.truecaller.utils.l)localObject6).a((String[])localObject1);
          if (paramInt != 0)
          {
            localObject6 = (t)h.a();
            paramIntent = d;
            ((t)localObject6).a(0, paramIntent, false);
          }
        }
      }
      return;
    }
    localObject6 = "android.provider.Telephony.SMS_DELIVER";
    paramInt = ((String)localObject6).equals(localObject1);
    if (paramInt != 0)
    {
      localObject6 = g;
      localObject1 = new String[0];
      AssertionUtil.onSameThread((Thread)localObject6, (String[])localObject1);
      localObject6 = new String[bool3];
      localObject2 = String.valueOf(paramIntent);
      localObject1 = "Intent received: ".concat((String)localObject2);
      localObject6[0] = localObject1;
      localObject6 = a(paramIntent);
      i4 = localObject6.length;
      if (i4 != 0)
      {
        i4 = paramIntent.getIntExtra("errorCode", 0);
        paramIntent = m.a(paramIntent);
        paramIntent = a((SmsMessage[])localObject6, paramIntent, i4);
        localObject1 = new String[bool3];
        localObject3 = String.valueOf(paramIntent);
        localObject2 = "New sms: ".concat((String)localObject3);
        localObject1[0] = localObject2;
        localObject1 = t.w();
        boolean bool5 = ((com.truecaller.featuretoggles.b)localObject1).a();
        if (!bool5)
        {
          localObject1 = r;
          ((j)localObject1).a(paramIntent);
        }
        localObject6 = localObject6[0].getMessageClass();
        localObject1 = SmsMessage.MessageClass.CLASS_0;
        if (localObject6 == localObject1)
        {
          ((com.truecaller.messaging.notifications.a)l.a()).c(paramIntent);
          return;
        }
        localObject6 = (t)h.a();
        ((t)localObject6).a(paramIntent);
      }
      return;
    }
    localObject6 = "android.provider.action.DEFAULT_SMS_PACKAGE_CHANGED";
    paramInt = ((String)localObject6).equals(localObject1);
    if (paramInt != 0)
    {
      paramInt = Build.VERSION.SDK_INT;
      int i2 = 24;
      if (paramInt >= i2)
      {
        u.a();
        localObject6 = g;
        localObject1 = new String[0];
        AssertionUtil.onSameThread((Thread)localObject6, (String[])localObject1);
        localObject6 = u.b();
        localObject1 = "NotSupported";
        if (localObject6 == null) {
          localObject6 = localObject1;
        }
        localObject1 = "android.provider.extra.IS_DEFAULT_SMS_APP";
        boolean bool6 = paramIntent.getBooleanExtra((String)localObject1, false);
        if (!bool6)
        {
          localObject1 = d.getApplicationContext()).c;
          i2 = 10012;
          ((com.truecaller.common.background.b)localObject1).b(i2);
        }
        localObject1 = (t)h.a();
        localObject2 = org.a.a.b.ay_();
        localObject3 = b.i();
        long l4 = a;
        long l5 = ((org.a.a.i)localObject3).b(l4, 3);
        localObject2 = ((org.a.a.b)localObject2).a_(l5);
        localObject1 = ((t)localObject1).b((org.a.a.b)localObject2);
        localObject2 = new com/truecaller/messaging/transport/sms/-$$Lambda$h$DdmLLhy8FkDXD_fwy12AnP2a4Ks;
        ((-..Lambda.h.DdmLLhy8FkDXD_fwy12AnP2a4Ks)localObject2).<init>(this, bool6, (String)localObject6);
        ((w)localObject1).a((ac)localObject2);
        return;
      }
    }
    paramIntent = new String[bool3];
    localObject1 = String.valueOf(localObject1);
    localObject6 = "Unknown intent action: ".concat((String)localObject1);
    paramIntent[0] = localObject6;
    AssertionUtil.OnlyInDebug.fail(paramIntent);
  }
  
  public final void a(BinaryEntity paramBinaryEntity)
  {
    paramBinaryEntity = new java/lang/IllegalStateException;
    paramBinaryEntity.<init>("Sms transport can not be used to cancel attachments.");
    throw paramBinaryEntity;
  }
  
  public final void a(Message paramMessage, String paramString1, String paramString2)
  {
    paramMessage = new java/lang/IllegalStateException;
    paramMessage.<init>("SMS transport does not support sending reactions");
    throw paramMessage;
  }
  
  public final void a(org.a.a.b paramb)
  {
    com.truecaller.messaging.h localh = j;
    long l1 = a;
    localh.a(0, l1);
  }
  
  public final boolean a(Message paramMessage)
  {
    Object localObject1 = d;
    localObject1 = a((Context)localObject1, paramMessage);
    long l1 = -1;
    long l2;
    if (localObject1 != null) {
      try
      {
        l2 = ContentUris.parseId((Uri)localObject1);
      }
      catch (NumberFormatException localNumberFormatException)
      {
        l2 = l1;
      }
    } else {
      l2 = l1;
    }
    localObject1 = null;
    boolean bool1 = l2 < l1;
    if (!bool1) {
      return false;
    }
    boolean bool2 = g;
    int i1 = 1;
    if (!bool2)
    {
      Object localObject2 = e;
      localObject2 = Message.a(l2, (org.a.a.b)localObject2);
      String str1 = c.f;
      Object localObject3 = "+";
      boolean bool3 = str1.startsWith((String)localObject3);
      if (bool3) {
        str1 = str1.substring(i1);
      }
      try
      {
        int i2 = f & i1;
        if (i2 == 0)
        {
          localObject3 = com.truecaller.tracking.events.u.b();
          String str2 = "sms";
          localObject3 = ((u.a)localObject3).d(str2);
          localObject2 = ((u.a)localObject3).a((CharSequence)localObject2);
          localObject2 = ((u.a)localObject2).b(str1);
          paramMessage = c;
          paramMessage = paramMessage.j();
          paramMessage = ((u.a)localObject2).c(paramMessage);
          paramMessage = paramMessage.a();
          localObject2 = e;
          localObject2 = ((com.truecaller.androidactors.f)localObject2).a();
          localObject2 = (ae)localObject2;
          ((ae)localObject2).a(paramMessage);
        }
      }
      catch (org.apache.a.a paramMessage)
      {
        localObject1 = new String[0];
        AssertionUtil.shouldNeverHappen(paramMessage, (String[])localObject1);
      }
    }
    return i1;
  }
  
  public final boolean a(Message paramMessage, Entity paramEntity)
  {
    return false;
  }
  
  public final boolean a(Participant paramParticipant)
  {
    int i1 = c;
    int i2 = 1;
    if (i1 != 0)
    {
      int i3 = c;
      if (i3 != i2) {
        return false;
      }
    }
    return i2;
  }
  
  public final boolean a(String paramString, com.truecaller.messaging.transport.a parama)
  {
    boolean bool = paramString.isEmpty();
    if (bool)
    {
      parama.a(0, 0, 0, 0);
      return false;
    }
    paramString = SmsMessage.calculateLength(org.c.a.a.a.k.n(paramString), false);
    bool = true;
    int i1 = paramString[bool];
    int i2 = paramString[2];
    int i3 = paramString[0];
    parama.a(i1, i2, i3, 0);
    return bool;
  }
  
  public final void b(long paramLong)
  {
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("SMS transport does not support retry");
    throw localIllegalStateException;
  }
  
  public final boolean b(Message paramMessage)
  {
    boolean bool1 = g();
    if (bool1)
    {
      com.truecaller.utils.l locall = q;
      String[] arrayOfString = { "android.permission.SEND_SMS" };
      bool1 = locall.a(arrayOfString);
      if (bool1)
      {
        bool1 = c(paramMessage);
        if (bool1)
        {
          paramMessage = c;
          boolean bool2 = a(paramMessage);
          if (bool2) {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  public final boolean b(ad paramad)
  {
    boolean bool1 = paramad.a();
    if (!bool1)
    {
      paramad = b;
      String str = b;
      boolean bool2 = paramad.equals(str);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean c()
  {
    com.truecaller.utils.l locall = q;
    String[] arrayOfString = { "android.permission.READ_SMS" };
    boolean bool = locall.a(arrayOfString);
    if (bool)
    {
      bool = g();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean c(Message paramMessage)
  {
    boolean bool1 = paramMessage.c();
    if (bool1)
    {
      boolean bool2 = paramMessage.d();
      if (!bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final com.truecaller.messaging.transport.k d(Message paramMessage)
  {
    h localh = this;
    Object localObject1 = paramMessage;
    String[] arrayOfString = null;
    Object localObject2 = new String[0];
    AssertionUtil.notOnMainThread((String[])localObject2);
    int i1 = j;
    if (i1 == 0)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      localObject2 = null;
    }
    Object localObject3 = new String[0];
    AssertionUtil.isTrue(i1, (String[])localObject3);
    localObject2 = (SmsTransportInfo)m;
    localObject3 = e;
    Object localObject4 = { "Save message to system database before actual sending" };
    AssertionUtil.isNotNull(localObject3, (String[])localObject4);
    localObject3 = SmsManager.getDefault();
    localObject4 = paramMessage.j();
    localObject3 = ((SmsManager)localObject3).divideMessage((String)localObject4);
    ArrayList localArrayList1 = new java/util/ArrayList;
    localArrayList1.<init>();
    ArrayList localArrayList2 = new java/util/ArrayList;
    localArrayList2.<init>();
    int i2 = ((ArrayList)localObject3).size();
    int i3 = 0;
    Object localObject5 = null;
    int i5;
    Object localObject6;
    Object localObject7;
    Object localObject8;
    for (;;)
    {
      i5 = 268435456;
      if (i3 >= i2) {
        break;
      }
      localObject6 = new android/content/Intent;
      localObject7 = d;
      localObject8 = SmsReceiver.class;
      ((Intent)localObject6).<init>((Context)localObject7, (Class)localObject8);
      ((Intent)localObject6).setAction("com.truecaller.messaging.SmsStatusReceived.SMS_SENT");
      localObject7 = e;
      ((Intent)localObject6).setData((Uri)localObject7);
      localObject7 = "message_part";
      ((Intent)localObject6).putExtra((String)localObject7, i3);
      ((Intent)localObject6).setFlags(i5);
      localObject9 = PendingIntent.getBroadcast(d, i3, (Intent)localObject6, 0);
      localArrayList1.add(localObject9);
      i3 += 1;
    }
    localObject4 = l;
    boolean bool1 = localh.c((String)localObject4);
    if (bool1)
    {
      localObject4 = new android/content/Intent;
      localObject5 = d;
      ((Intent)localObject4).<init>((Context)localObject5, SmsReceiver.class);
      ((Intent)localObject4).setAction("com.truecaller.messaging.SmsStatusReceived.SMS_STATUS");
      localObject5 = e;
      ((Intent)localObject4).setData((Uri)localObject5);
      localObject6 = e;
      long l1 = a;
      ((Intent)localObject4).putExtra("date", l1);
      ((Intent)localObject4).setFlags(i5);
      localObject5 = d;
      localObject4 = PendingIntent.getBroadcast((Context)localObject5, 0, (Intent)localObject4, 0);
      localArrayList2.add(localObject4);
    }
    localObject4 = c.f;
    boolean bool2 = TextUtils.isEmpty((CharSequence)localObject4);
    Object localObject9 = { "Destination can not be empty" };
    AssertionUtil.isFalse(bool2, (String[])localObject9);
    try
    {
      localObject5 = m;
      localObject9 = l;
      localObject5 = ((com.truecaller.multisim.h)localObject5).c((String)localObject9);
      bool2 = ((com.truecaller.multisim.a)localObject5).a();
      if (bool2)
      {
        int i4 = ((ArrayList)localObject3).size();
        i5 = 0;
        localObject9 = null;
        while (i5 < i4)
        {
          boolean bool3 = false;
          localObject6 = null;
          boolean bool4 = localArrayList2.isEmpty();
          if (!bool4)
          {
            int i6 = i4 + -1;
            if (i5 == i6)
            {
              localObject6 = localArrayList2.get(0);
              localObject6 = (PendingIntent)localObject6;
              localObject10 = localObject6;
              break label525;
            }
          }
          Object localObject10 = null;
          label525:
          localObject8 = m;
          String str = i;
          localObject6 = ((ArrayList)localObject3).get(i5);
          Object localObject11 = localObject6;
          localObject11 = (String)localObject6;
          localObject6 = localArrayList1.get(i5);
          Object localObject12 = localObject6;
          localObject12 = (PendingIntent)localObject6;
          localObject6 = l;
          bool3 = ((com.truecaller.multisim.h)localObject8).a((String)localObject4, str, (String)localObject11, (PendingIntent)localObject12, (PendingIntent)localObject10, (String)localObject6);
          if (!bool3) {
            return k.b.a;
          }
          i5 += 1;
        }
      }
      localObject5 = m;
      localObject6 = i;
      localObject8 = l;
      localObject9 = localObject4;
      localObject7 = localObject3;
      boolean bool5 = ((com.truecaller.multisim.h)localObject5).a((String)localObject4, (String)localObject6, (ArrayList)localObject3, localArrayList1, localArrayList2, (String)localObject8);
      if (!bool5) {
        return k.b.a;
      }
      localObject1 = new com/truecaller/messaging/transport/sms/h$a;
      localObject2 = e;
      int i7 = ((ArrayList)localObject3).size();
      ((h.a)localObject1).<init>((Uri)localObject2, i7, (byte)0);
      v = ((h.a)localObject1);
      return v;
    }
    catch (RuntimeException localRuntimeException)
    {
      arrayOfString = new String[0];
      AssertionUtil.OnlyInDebug.shouldNeverHappen(localRuntimeException, arrayOfString);
      AssertionUtil.reportThrowableButNeverCrash(localRuntimeException);
    }
    return k.b.a;
  }
  
  /* Error */
  public final org.a.a.b d()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 110	com/truecaller/messaging/transport/sms/h:j	Lcom/truecaller/messaging/h;
    //   4: astore_1
    //   5: aload_1
    //   6: iconst_0
    //   7: invokeinterface 363 2 0
    //   12: lstore_2
    //   13: aload_0
    //   14: getfield 77	com/truecaller/messaging/transport/sms/h:w	Z
    //   17: istore 4
    //   19: iload 4
    //   21: ifne +301 -> 322
    //   24: aload_0
    //   25: getfield 122	com/truecaller/messaging/transport/sms/h:q	Lcom/truecaller/utils/l;
    //   28: astore_1
    //   29: iconst_1
    //   30: anewarray 137	java/lang/String
    //   33: dup
    //   34: iconst_0
    //   35: ldc_w 1109
    //   38: aastore
    //   39: astore 5
    //   41: aload_1
    //   42: aload 5
    //   44: invokeinterface 771 2 0
    //   49: istore 4
    //   51: iload 4
    //   53: ifeq +269 -> 322
    //   56: aload_0
    //   57: getfield 122	com/truecaller/messaging/transport/sms/h:q	Lcom/truecaller/utils/l;
    //   60: astore_1
    //   61: iconst_1
    //   62: anewarray 137	java/lang/String
    //   65: dup
    //   66: iconst_0
    //   67: ldc_w 766
    //   70: aastore
    //   71: astore 5
    //   73: aload_1
    //   74: aload 5
    //   76: invokeinterface 771 2 0
    //   81: istore 4
    //   83: iload 4
    //   85: ifeq +237 -> 322
    //   88: aload_0
    //   89: getfield 79	com/truecaller/messaging/transport/sms/h:d	Landroid/content/Context;
    //   92: invokevirtual 101	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   95: astore_1
    //   96: aconst_null
    //   97: astore 6
    //   99: getstatic 61	com/truecaller/messaging/transport/sms/h:a	Landroid/net/Uri;
    //   102: astore 7
    //   104: ldc -2
    //   106: astore 5
    //   108: iconst_1
    //   109: anewarray 137	java/lang/String
    //   112: dup
    //   113: iconst_0
    //   114: aload 5
    //   116: aastore
    //   117: astore 8
    //   119: ldc_w 1198
    //   122: astore 9
    //   124: aconst_null
    //   125: astore 10
    //   127: ldc_w 472
    //   130: astore 11
    //   132: aload_1
    //   133: astore 5
    //   135: aload_1
    //   136: aload 7
    //   138: aload 8
    //   140: aload 9
    //   142: aconst_null
    //   143: aload 11
    //   145: invokevirtual 476	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   148: astore 5
    //   150: aload 5
    //   152: ifnull +125 -> 277
    //   155: aload 5
    //   157: invokeinterface 1201 1 0
    //   162: istore 12
    //   164: iload 12
    //   166: ifeq +111 -> 277
    //   169: aload 5
    //   171: iconst_0
    //   172: invokeinterface 1204 2 0
    //   177: lstore 13
    //   179: new 164	android/content/ContentValues
    //   182: astore 9
    //   184: aload 9
    //   186: invokespecial 165	android/content/ContentValues:<init>	()V
    //   189: ldc_w 333
    //   192: astore 10
    //   194: iconst_5
    //   195: istore 15
    //   197: iload 15
    //   199: invokestatic 280	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   202: astore 11
    //   204: aload 9
    //   206: aload 10
    //   208: aload 11
    //   210: invokevirtual 283	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   213: getstatic 61	com/truecaller/messaging/transport/sms/h:a	Landroid/net/Uri;
    //   216: astore 10
    //   218: ldc_w 1198
    //   221: astore 11
    //   223: aload_1
    //   224: aload 10
    //   226: aload 9
    //   228: aload 11
    //   230: aconst_null
    //   231: invokevirtual 344	android/content/ContentResolver:update	(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    //   234: pop
    //   235: lload 13
    //   237: lload_2
    //   238: lcmp
    //   239: istore 4
    //   241: iload 4
    //   243: ifge +6 -> 249
    //   246: lload 13
    //   248: lstore_2
    //   249: aload_0
    //   250: getfield 110	com/truecaller/messaging/transport/sms/h:j	Lcom/truecaller/messaging/h;
    //   253: astore_1
    //   254: aload_1
    //   255: iconst_0
    //   256: lload_2
    //   257: invokeinterface 371 4 0
    //   262: goto +15 -> 277
    //   265: astore_1
    //   266: goto +49 -> 315
    //   269: astore_1
    //   270: aload 5
    //   272: astore 6
    //   274: goto +20 -> 294
    //   277: aload 5
    //   279: invokestatic 1209	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   282: goto +21 -> 303
    //   285: astore_1
    //   286: aload 6
    //   288: astore 5
    //   290: goto +25 -> 315
    //   293: astore_1
    //   294: aload_1
    //   295: invokestatic 358	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   298: aload 6
    //   300: invokestatic 1209	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   303: iconst_1
    //   304: istore 4
    //   306: aload_0
    //   307: iload 4
    //   309: putfield 77	com/truecaller/messaging/transport/sms/h:w	Z
    //   312: goto +10 -> 322
    //   315: aload 5
    //   317: invokestatic 1209	com/truecaller/util/q:a	(Landroid/database/Cursor;)V
    //   320: aload_1
    //   321: athrow
    //   322: new 365	org/a/a/b
    //   325: astore_1
    //   326: aload_1
    //   327: lload_2
    //   328: invokespecial 888	org/a/a/b:<init>	(J)V
    //   331: aload_1
    //   332: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	333	0	this	h
    //   4	251	1	localObject1	Object
    //   265	1	1	localObject2	Object
    //   269	1	1	localRuntimeException1	RuntimeException
    //   285	1	1	localObject3	Object
    //   293	28	1	localRuntimeException2	RuntimeException
    //   325	7	1	localb	org.a.a.b
    //   12	316	2	l1	long
    //   17	291	4	bool1	boolean
    //   39	277	5	localObject4	Object
    //   97	202	6	localObject5	Object
    //   102	35	7	localUri	Uri
    //   117	22	8	arrayOfString	String[]
    //   122	105	9	localObject6	Object
    //   125	100	10	localObject7	Object
    //   130	99	11	localObject8	Object
    //   162	3	12	bool2	boolean
    //   177	70	13	l2	long
    //   195	3	15	i1	int
    // Exception table:
    //   from	to	target	type
    //   155	162	265	finally
    //   171	177	265	finally
    //   179	182	265	finally
    //   184	189	265	finally
    //   197	202	265	finally
    //   208	213	265	finally
    //   213	216	265	finally
    //   230	235	265	finally
    //   249	253	265	finally
    //   256	262	265	finally
    //   155	162	269	java/lang/RuntimeException
    //   171	177	269	java/lang/RuntimeException
    //   179	182	269	java/lang/RuntimeException
    //   184	189	269	java/lang/RuntimeException
    //   197	202	269	java/lang/RuntimeException
    //   208	213	269	java/lang/RuntimeException
    //   213	216	269	java/lang/RuntimeException
    //   230	235	269	java/lang/RuntimeException
    //   249	253	269	java/lang/RuntimeException
    //   256	262	269	java/lang/RuntimeException
    //   99	102	285	finally
    //   108	117	285	finally
    //   143	148	285	finally
    //   294	298	285	finally
    //   99	102	293	java/lang/RuntimeException
    //   108	117	293	java/lang/RuntimeException
    //   143	148	293	java/lang/RuntimeException
  }
  
  public final int e(Message paramMessage)
  {
    return 0;
  }
  
  public final boolean e()
  {
    return true;
  }
  
  public final int f()
  {
    return 0;
  }
  
  public final boolean f(Message paramMessage)
  {
    return false;
  }
  
  public final String toString()
  {
    return "SMS transport";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */