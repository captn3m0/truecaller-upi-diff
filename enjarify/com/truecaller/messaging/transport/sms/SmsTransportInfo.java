package com.truecaller.messaging.transport.sms;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.TransportInfo;
import org.a.a.b;

public class SmsTransportInfo
  implements TransportInfo
{
  public static final Parcelable.Creator CREATOR;
  public final long a;
  public final long b;
  public final int c;
  public final long d;
  public final Uri e;
  public final String f;
  public final int g;
  public final int h;
  public final String i;
  public final int j;
  public final boolean k;
  public final String l;
  
  static
  {
    SmsTransportInfo.1 local1 = new com/truecaller/messaging/transport/sms/SmsTransportInfo$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  private SmsTransportInfo(Parcel paramParcel)
  {
    long l1 = paramParcel.readLong();
    a = l1;
    l1 = paramParcel.readLong();
    b = l1;
    int m = paramParcel.readInt();
    c = m;
    l1 = paramParcel.readLong();
    d = l1;
    Object localObject = paramParcel.readString();
    boolean bool = TextUtils.isEmpty((CharSequence)localObject);
    if (bool)
    {
      m = 0;
      localObject = null;
      e = null;
    }
    else
    {
      localObject = Uri.parse((String)localObject);
      e = ((Uri)localObject);
    }
    m = paramParcel.readInt();
    g = m;
    m = paramParcel.readInt();
    h = m;
    localObject = paramParcel.readString();
    i = ((String)localObject);
    localObject = paramParcel.readString();
    f = ((String)localObject);
    m = paramParcel.readInt();
    j = m;
    m = paramParcel.readInt();
    if (m != 0)
    {
      m = 1;
    }
    else
    {
      m = 0;
      localObject = null;
    }
    k = m;
    paramParcel = paramParcel.readString();
    l = paramParcel;
  }
  
  private SmsTransportInfo(SmsTransportInfo.a parama)
  {
    long l1 = a;
    a = l1;
    l1 = b;
    b = l1;
    int m = c;
    c = m;
    l1 = d;
    d = l1;
    Object localObject = e;
    e = ((Uri)localObject);
    m = f;
    g = m;
    m = g;
    h = m;
    localObject = h;
    i = ((String)localObject);
    localObject = k;
    f = ((String)localObject);
    m = i;
    j = m;
    boolean bool = j;
    k = bool;
    parama = l;
    l = parama;
  }
  
  static int a(int paramInt)
  {
    switch (paramInt)
    {
    case 3: 
    default: 
      break;
    case 5: 
      paramInt = 9;
      break;
    case 4: 
    case 6: 
      paramInt = 5;
      break;
    case 2: 
      paramInt = 1;
      break;
    }
    paramInt = 0;
    return paramInt;
  }
  
  static int b(int paramInt)
  {
    int m = paramInt & 0x1;
    if (m != 0)
    {
      m = paramInt & 0x8;
      if (m != 0) {
        return 5;
      }
      m = paramInt & 0x4;
      if (m != 0) {
        return 6;
      }
      paramInt &= 0x10;
      if (paramInt != 0) {
        return 3;
      }
      return 2;
    }
    return 1;
  }
  
  public final int a()
  {
    int m = c;
    if (m != 0)
    {
      int n = 32;
      if (m != n)
      {
        n = 64;
        if (m != n) {
          return 0;
        }
        return 1;
      }
      return 2;
    }
    return 3;
  }
  
  public final String a(b paramb)
  {
    return Message.a(b, paramb);
  }
  
  public final int b()
  {
    return 0;
  }
  
  public final long c()
  {
    return a;
  }
  
  public final long d()
  {
    return b;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public final long e()
  {
    return d;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {
      return bool1;
    }
    if (paramObject != null)
    {
      Object localObject1 = getClass();
      Object localObject2 = paramObject.getClass();
      if (localObject1 == localObject2)
      {
        paramObject = (SmsTransportInfo)paramObject;
        long l1 = a;
        long l2 = a;
        boolean bool2 = l1 < l2;
        if (bool2) {
          return false;
        }
        l1 = b;
        l2 = b;
        bool2 = l1 < l2;
        if (bool2) {
          return false;
        }
        int m = c;
        int n = c;
        if (m != n) {
          return false;
        }
        m = g;
        n = g;
        if (m != n) {
          return false;
        }
        m = h;
        n = h;
        if (m != n) {
          return false;
        }
        m = j;
        n = j;
        if (m != n) {
          return false;
        }
        boolean bool3 = k;
        boolean bool4 = k;
        if (bool3 != bool4) {
          return false;
        }
        localObject1 = e;
        if (localObject1 != null)
        {
          localObject2 = e;
          bool3 = ((Uri)localObject1).equals(localObject2);
          if (bool3) {
            break label237;
          }
        }
        else
        {
          localObject1 = e;
          if (localObject1 == null) {
            break label237;
          }
        }
        return false;
        label237:
        localObject1 = f;
        if (localObject1 != null)
        {
          localObject2 = f;
          bool3 = ((String)localObject1).equals(localObject2);
          if (bool3) {
            break label279;
          }
        }
        else
        {
          localObject1 = f;
          if (localObject1 == null) {
            break label279;
          }
        }
        return false;
        label279:
        localObject1 = i;
        if (localObject1 != null)
        {
          paramObject = i;
          return ((String)localObject1).equals(paramObject);
        }
        paramObject = i;
        if (paramObject == null) {
          return bool1;
        }
        return false;
      }
    }
    return false;
  }
  
  public final boolean f()
  {
    return true;
  }
  
  public final SmsTransportInfo.a g()
  {
    SmsTransportInfo.a locala = new com/truecaller/messaging/transport/sms/SmsTransportInfo$a;
    locala.<init>(this, (byte)0);
    return locala;
  }
  
  public int hashCode()
  {
    long l1 = a;
    int m = 32;
    long l2 = l1 >>> m;
    l1 ^= l2;
    int n = (int)l1 * 31;
    l2 = b;
    long l3 = l2 >>> m;
    l2 ^= l3;
    int i1 = (int)l2;
    n = (n + i1) * 31;
    i1 = c;
    n = (n + i1) * 31;
    Object localObject = e;
    m = 0;
    if (localObject != null)
    {
      i1 = ((Uri)localObject).hashCode();
    }
    else
    {
      i1 = 0;
      localObject = null;
    }
    n = (n + i1) * 31;
    localObject = f;
    if (localObject != null)
    {
      i1 = ((String)localObject).hashCode();
    }
    else
    {
      i1 = 0;
      localObject = null;
    }
    n = (n + i1) * 31;
    i1 = g;
    n = (n + i1) * 31;
    i1 = h;
    n = (n + i1) * 31;
    localObject = i;
    if (localObject != null) {
      m = ((String)localObject).hashCode();
    }
    n = (n + m) * 31;
    i1 = j;
    n = (n + i1) * 31;
    int i2 = k;
    return n + i2;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("{ type : sms, messageId: ");
    long l1 = a;
    localStringBuilder.append(l1);
    localStringBuilder.append(", uri: \"");
    String str = String.valueOf(e);
    localStringBuilder.append(str);
    localStringBuilder.append("\" }");
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    long l1 = a;
    paramParcel.writeLong(l1);
    l1 = b;
    paramParcel.writeLong(l1);
    paramInt = c;
    paramParcel.writeInt(paramInt);
    l1 = d;
    paramParcel.writeLong(l1);
    Object localObject = e;
    if (localObject == null)
    {
      paramInt = 0;
      localObject = null;
      paramParcel.writeString(null);
    }
    else
    {
      localObject = ((Uri)localObject).toString();
      paramParcel.writeString((String)localObject);
    }
    paramInt = g;
    paramParcel.writeInt(paramInt);
    paramInt = h;
    paramParcel.writeInt(paramInt);
    localObject = i;
    paramParcel.writeString((String)localObject);
    localObject = f;
    paramParcel.writeString((String)localObject);
    paramInt = j;
    paramParcel.writeInt(paramInt);
    paramInt = k;
    paramParcel.writeInt(paramInt);
    localObject = l;
    paramParcel.writeString((String)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.SmsTransportInfo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */