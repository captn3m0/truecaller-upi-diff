package com.truecaller.messaging.transport.sms;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.u;
import com.truecaller.messaging.data.o;
import com.truecaller.messaging.data.types.Draft;
import com.truecaller.messaging.data.types.Draft.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.transport.m;
import com.truecaller.multisim.h;
import org.c.a.a.a.k;

public class NoConfirmationSmsSendService
  extends Service
{
  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public void onStart(Intent paramIntent, int paramInt)
  {
    super.onStart(paramIntent, paramInt);
    if (paramIntent == null)
    {
      new String[1][0] = "Intent missed";
      return;
    }
    Object localObject1 = paramIntent.getAction();
    Object localObject2 = "android.intent.action.RESPOND_VIA_MESSAGE";
    boolean bool1 = ((String)localObject2).equals(localObject1);
    if (!bool1) {
      return;
    }
    localObject1 = paramIntent.getData();
    int i = 1;
    if (localObject1 == null)
    {
      localObject3 = new String[i];
      paramIntent = String.valueOf(paramIntent);
      paramIntent = "Empty data uri: ".concat(paramIntent);
      localObject3[0] = paramIntent;
      return;
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Object localObject4 = "android.intent.extra.SUBJECT";
    boolean bool2 = paramIntent.hasExtra((String)localObject4);
    if (bool2)
    {
      localObject4 = paramIntent.getStringExtra("android.intent.extra.SUBJECT");
      localStringBuilder.append((String)localObject4);
      localObject4 = "android.intent.extra.TEXT";
      bool2 = paramIntent.hasExtra((String)localObject4);
      if (bool2)
      {
        char c = '\n';
        localStringBuilder.append(c);
      }
    }
    localObject4 = k.n(paramIntent.getStringExtra("android.intent.extra.TEXT"));
    localStringBuilder.append((String)localObject4);
    int j = localStringBuilder.length();
    if (j == 0)
    {
      new String[1][0] = "Empty message text";
      return;
    }
    localObject4 = ((bk)getApplication()).a();
    Object localObject5 = ((bp)localObject4).U();
    String str1 = ((h)localObject5).b(paramIntent);
    String str2 = "-1";
    boolean bool3 = str2.equals(str1);
    if (bool3) {
      str1 = ((h)localObject5).f();
    }
    localObject5 = ((bp)localObject4).V();
    localObject1 = Participant.a((Uri)localObject1, (u)localObject5, str1);
    int k = localObject1.length;
    if (k == 0)
    {
      localObject3 = new String[i];
      paramIntent = String.valueOf(paramIntent);
      paramIntent = "Empty participants list: ".concat(paramIntent);
      localObject3[0] = paramIntent;
      return;
    }
    paramIntent = new com/truecaller/messaging/data/types/Draft$a;
    paramIntent.<init>();
    i = localObject1.length;
    k = 0;
    localObject5 = null;
    while (k < i)
    {
      str2 = localObject1[k];
      paramIntent.a(str2);
      k += 1;
    }
    localObject2 = localStringBuilder.toString();
    d = ((String)localObject2);
    paramIntent = paramIntent.d().a(str1);
    paramIntent = ((bp)localObject4).o().a(paramIntent);
    localObject2 = new com/truecaller/messaging/transport/sms/-$$Lambda$NoConfirmationSmsSendService$GxpicrJbVLeLNoNbtEpM25h8gfs;
    ((-..Lambda.NoConfirmationSmsSendService.GxpicrJbVLeLNoNbtEpM25h8gfs)localObject2).<init>(this, paramInt);
    paramIntent.a((ac)localObject2);
    paramIntent = ((o)((bp)localObject4).q().a()).a((Participant[])localObject1, 0);
    Object localObject3 = new com/truecaller/messaging/transport/sms/-$$Lambda$NoConfirmationSmsSendService$_xqxM5QXky-jGCJRqJtfPAQ6rk8;
    ((-..Lambda.NoConfirmationSmsSendService._xqxM5QXky-jGCJRqJtfPAQ6rk8)localObject3).<init>(this, (bp)localObject4);
    paramIntent.a((ac)localObject3);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.NoConfirmationSmsSendService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */