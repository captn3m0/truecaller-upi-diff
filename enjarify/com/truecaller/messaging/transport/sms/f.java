package com.truecaller.messaging.transport.sms;

import dagger.a.d;
import javax.inject.Provider;

public final class f
  implements d
{
  private final c a;
  private final Provider b;
  private final Provider c;
  
  private f(c paramc, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramc;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static f a(c paramc, Provider paramProvider1, Provider paramProvider2)
  {
    f localf = new com/truecaller/messaging/transport/sms/f;
    localf.<init>(paramc, paramProvider1, paramProvider2);
    return localf;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */