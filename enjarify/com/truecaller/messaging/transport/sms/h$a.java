package com.truecaller.messaging.transport.sms;

import android.net.Uri;
import com.truecaller.messaging.transport.k.e;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

final class h$a
  implements k.e
{
  private final Lock a;
  private final Condition b;
  private final Uri c;
  private long d;
  
  private h$a(Uri paramUri, int paramInt)
  {
    Object localObject = new java/util/concurrent/locks/ReentrantLock;
    ((ReentrantLock)localObject).<init>();
    a = ((Lock)localObject);
    localObject = a.newCondition();
    b = ((Condition)localObject);
    c = paramUri;
    double d1 = paramInt;
    long l = Math.pow(2.0D, d1) - 1L;
    d = l;
  }
  
  /* Error */
  public final com.truecaller.messaging.transport.k a(java.util.concurrent.TimeUnit paramTimeUnit)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 23	com/truecaller/messaging/transport/sms/h$a:a	Ljava/util/concurrent/locks/Lock;
    //   4: astore_2
    //   5: aload_2
    //   6: invokeinterface 53 1 0
    //   11: aload_0
    //   12: getfield 31	com/truecaller/messaging/transport/sms/h$a:b	Ljava/util/concurrent/locks/Condition;
    //   15: astore_2
    //   16: iconst_2
    //   17: i2l
    //   18: lstore_3
    //   19: aload_2
    //   20: lload_3
    //   21: aload_1
    //   22: invokeinterface 61 4 0
    //   27: istore 5
    //   29: iload 5
    //   31: ifeq +27 -> 58
    //   34: getstatic 66	com/truecaller/messaging/transport/k$d:a	Lcom/truecaller/messaging/transport/k$d;
    //   37: astore_1
    //   38: aload_0
    //   39: getfield 23	com/truecaller/messaging/transport/sms/h$a:a	Ljava/util/concurrent/locks/Lock;
    //   42: invokeinterface 69 1 0
    //   47: aload_1
    //   48: areturn
    //   49: astore_1
    //   50: goto +21 -> 71
    //   53: astore_1
    //   54: aload_1
    //   55: invokestatic 75	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   58: aload_0
    //   59: getfield 23	com/truecaller/messaging/transport/sms/h$a:a	Ljava/util/concurrent/locks/Lock;
    //   62: invokeinterface 69 1 0
    //   67: getstatic 80	com/truecaller/messaging/transport/k$b:a	Lcom/truecaller/messaging/transport/k$b;
    //   70: areturn
    //   71: aload_0
    //   72: getfield 23	com/truecaller/messaging/transport/sms/h$a:a	Ljava/util/concurrent/locks/Lock;
    //   75: invokeinterface 69 1 0
    //   80: aload_1
    //   81: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	82	0	this	a
    //   0	82	1	paramTimeUnit	java.util.concurrent.TimeUnit
    //   4	16	2	localObject	Object
    //   18	3	3	l	long
    //   27	3	5	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   11	15	49	finally
    //   21	27	49	finally
    //   34	37	49	finally
    //   54	58	49	finally
    //   11	15	53	java/lang/InterruptedException
    //   21	27	53	java/lang/InterruptedException
    //   34	37	53	java/lang/InterruptedException
  }
  
  final boolean a(Uri paramUri, int paramInt, long paramLong)
  {
    Uri localUri = c;
    boolean bool1 = localUri.equals(paramUri);
    boolean bool2 = true;
    if (!bool1) {
      return bool2;
    }
    paramUri = a;
    paramUri.lock();
    int i = -1;
    if (paramInt == i) {}
    try
    {
      long l1 = d;
      double d1 = 2.0D;
      double d2 = paramLong;
      d2 = Math.pow(d1, d2);
      paramInt = (int)d2;
      i ^= paramInt;
      long l2 = i & l1;
      d = l2;
      l2 = d;
      paramLong = 0L;
      boolean bool3 = l2 < paramLong;
      if (bool3) {
        return false;
      }
      paramUri = b;
      paramUri.signalAll();
      return bool2;
    }
    finally
    {
      a.unlock();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.h.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */