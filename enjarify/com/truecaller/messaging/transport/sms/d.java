package com.truecaller.messaging.transport.sms;

import javax.inject.Provider;

public final class d
  implements dagger.a.d
{
  private final c a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private d(c paramc, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramc;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static d a(c paramc, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    d locald = new com/truecaller/messaging/transport/sms/d;
    locald.<init>(paramc, paramProvider1, paramProvider2, paramProvider3);
    return locald;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */