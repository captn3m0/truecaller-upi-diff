package com.truecaller.messaging.transport.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.messaging.transport.m;

public class SmsReceiver
  extends BroadcastReceiver
{
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent == null) {
      return;
    }
    paramContext = ((bk)paramContext.getApplicationContext()).a().o();
    int i = getResultCode();
    paramContext.a(0, paramIntent, i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.SmsReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */