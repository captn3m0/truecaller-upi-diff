package com.truecaller.messaging.transport.sms;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.Looper;
import com.truecaller.androidactors.f;
import com.truecaller.messaging.data.t;
import java.util.Collections;
import java.util.Set;

final class h$b
  extends ContentObserver
{
  private final ContentResolver a;
  private final f b;
  
  h$b(Looper paramLooper, ContentResolver paramContentResolver, f paramf)
  {
    super(localHandler);
    a = paramContentResolver;
    b = paramf;
  }
  
  public final void onChange(boolean paramBoolean)
  {
    t localt = (t)b.a();
    Set localSet = Collections.singleton(Integer.valueOf(0));
    localt.b(true, localSet);
    a.unregisterContentObserver(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.h.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */