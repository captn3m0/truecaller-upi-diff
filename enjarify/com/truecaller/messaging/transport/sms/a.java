package com.truecaller.messaging.transport.sms;

import android.content.Intent;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.transport.ad;
import com.truecaller.messaging.transport.ad.a.a;
import com.truecaller.messaging.transport.ad.b;
import com.truecaller.messaging.transport.f;
import com.truecaller.messaging.transport.i;
import com.truecaller.messaging.transport.l;
import com.truecaller.messaging.transport.l.a;
import com.truecaller.utils.q;
import java.util.List;
import java.util.Set;
import org.a.a.b;

public final class a
  implements l
{
  private final l a;
  private final ad.b b;
  
  public a(l paraml, ad.b paramb)
  {
    a = paraml;
    b = paramb;
  }
  
  public final long a(long paramLong)
  {
    return a.a(paramLong);
  }
  
  public final long a(f paramf, i parami, r paramr, b paramb1, b paramb2, int paramInt, List paramList, q paramq, boolean paramBoolean1, boolean paramBoolean2, Set paramSet)
  {
    c.g.b.k.b(paramf, "threadInfoCache");
    c.g.b.k.b(parami, "participantCache");
    c.g.b.k.b(paramr, "cursor");
    c.g.b.k.b(paramb1, "timeTo");
    c.g.b.k.b(paramb2, "timeFrom");
    c.g.b.k.b(paramList, "operations");
    c.g.b.k.b(paramq, "trace");
    c.g.b.k.b(paramSet, "messagesToClassify");
    return a.a(paramf, parami, paramr, paramb1, paramb2, paramInt, paramList, paramq, paramBoolean1, paramBoolean2, paramSet);
  }
  
  public final l.a a(Message paramMessage, Participant[] paramArrayOfParticipant)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramArrayOfParticipant, "recipients");
    paramMessage = a.a(paramMessage, paramArrayOfParticipant);
    c.g.b.k.a(paramMessage, "transport.enqueueMessage(message, recipients)");
    return paramMessage;
  }
  
  public final String a()
  {
    String str = a.a();
    c.g.b.k.a(str, "transport.name");
    return str;
  }
  
  public final String a(String paramString)
  {
    c.g.b.k.b(paramString, "simToken");
    paramString = a.a(paramString);
    c.g.b.k.a(paramString, "transport.prepareSimTokenToStore(simToken)");
    return paramString;
  }
  
  public final void a(Intent paramIntent, int paramInt)
  {
    c.g.b.k.b(paramIntent, "intent");
    a.a(paramIntent, paramInt);
  }
  
  public final void a(BinaryEntity paramBinaryEntity)
  {
    c.g.b.k.b(paramBinaryEntity, "entity");
    a.a(paramBinaryEntity);
  }
  
  public final void a(Message paramMessage, String paramString1, String paramString2)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramString2, "initiatedVia");
    paramMessage = new java/lang/IllegalStateException;
    paramMessage.<init>("No SMS Permission transport does not support sending reactions");
    throw ((Throwable)paramMessage);
  }
  
  public final void a(b paramb)
  {
    c.g.b.k.b(paramb, "time");
    a.a(paramb);
  }
  
  public final boolean a(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return a.a(paramMessage);
  }
  
  public final boolean a(Message paramMessage, Entity paramEntity)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramEntity, "entity");
    return a.a(paramMessage, paramEntity);
  }
  
  public final boolean a(Message paramMessage, ad paramad)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramad, "transaction");
    return false;
  }
  
  public final boolean a(Participant paramParticipant)
  {
    c.g.b.k.b(paramParticipant, "participant");
    return a.a(paramParticipant);
  }
  
  public final boolean a(TransportInfo paramTransportInfo, long paramLong1, long paramLong2, ad paramad)
  {
    c.g.b.k.b(paramTransportInfo, "info");
    c.g.b.k.b(paramad, "transaction");
    paramTransportInfo = TruecallerContract.aa.a(paramTransportInfo.c());
    paramTransportInfo = paramad.a(paramTransportInfo);
    int i = 1;
    Integer localInteger = Integer.valueOf(i);
    paramTransportInfo.a("read", localInteger);
    localInteger = Integer.valueOf(i);
    paramTransportInfo.a("seen", localInteger);
    localInteger = Integer.valueOf(i);
    paramTransportInfo.a("sync_status", localInteger);
    paramTransportInfo = paramTransportInfo.a();
    paramad.a(paramTransportInfo);
    return i;
  }
  
  public final boolean a(TransportInfo paramTransportInfo, ad paramad)
  {
    c.g.b.k.b(paramTransportInfo, "info");
    c.g.b.k.b(paramad, "transaction");
    paramTransportInfo = TruecallerContract.aa.a(paramTransportInfo.c());
    paramTransportInfo = paramad.a(paramTransportInfo);
    int i = 1;
    Integer localInteger = Integer.valueOf(i);
    paramTransportInfo.a("seen", localInteger);
    localInteger = Integer.valueOf(i);
    paramTransportInfo.a("sync_status", localInteger);
    paramTransportInfo = paramTransportInfo.a();
    paramad.a(paramTransportInfo);
    return i;
  }
  
  public final boolean a(TransportInfo paramTransportInfo, ad paramad, boolean paramBoolean)
  {
    c.g.b.k.b(paramTransportInfo, "info");
    c.g.b.k.b(paramad, "transaction");
    return false;
  }
  
  public final boolean a(ad paramad)
  {
    c.g.b.k.b(paramad, "transaction");
    boolean bool1 = false;
    try
    {
      Object localObject = b;
      paramad = ((ad.b)localObject).a(paramad);
      localObject = "transactionExecutor.execute(transaction)";
      c.g.b.k.a(paramad, (String)localObject);
      int i = paramad.length;
      boolean bool2 = true;
      if (i == 0) {
        bool1 = true;
      }
      bool1 ^= bool2;
    }
    catch (OperationApplicationException localOperationApplicationException)
    {
      paramad = (Throwable)localOperationApplicationException;
      AssertionUtil.reportThrowableButNeverCrash(paramad);
    }
    catch (SecurityException localSecurityException)
    {
      paramad = (Throwable)localSecurityException;
      AssertionUtil.reportThrowableButNeverCrash(paramad);
    }
    catch (RemoteException localRemoteException)
    {
      paramad = (Throwable)localRemoteException;
      AssertionUtil.reportThrowableButNeverCrash(paramad);
    }
    return bool1;
  }
  
  public final boolean a(String paramString, com.truecaller.messaging.transport.a parama)
  {
    c.g.b.k.b(paramString, "text");
    c.g.b.k.b(parama, "result");
    return a.a(paramString, parama);
  }
  
  public final ad b()
  {
    ad localad = new com/truecaller/messaging/transport/ad;
    String str = TruecallerContract.a();
    localad.<init>(str);
    return localad;
  }
  
  public final void b(long paramLong)
  {
    a.b(paramLong);
  }
  
  public final boolean b(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return a.b(paramMessage);
  }
  
  public final boolean b(ad paramad)
  {
    String str = "transaction";
    c.g.b.k.b(paramad, str);
    boolean bool1 = paramad.a();
    if (!bool1)
    {
      paramad = paramad.c();
      str = TruecallerContract.a();
      boolean bool2 = c.g.b.k.a(paramad, str);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean c()
  {
    return a.c();
  }
  
  public final boolean c(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return a.c(paramMessage);
  }
  
  public final com.truecaller.messaging.transport.k d(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    paramMessage = a.d(paramMessage);
    c.g.b.k.a(paramMessage, "transport.sendMessage(message)");
    return paramMessage;
  }
  
  public final b d()
  {
    b localb = a.d();
    c.g.b.k.a(localb, "transport.lastSyncTime");
    return localb;
  }
  
  public final int e(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return a.e(paramMessage);
  }
  
  public final boolean e()
  {
    return a.e();
  }
  
  public final int f()
  {
    return a.f();
  }
  
  public final boolean f(Message paramMessage)
  {
    c.g.b.k.b(paramMessage, "message");
    return a.f(paramMessage);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.sms.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */