package com.truecaller.messaging.transport;

import java.util.Iterator;

final class n$a
  implements Iterator
{
  private int b = 0;
  
  private n$a(n paramn) {}
  
  public final boolean hasNext()
  {
    int i = b;
    int[] arrayOfInt = n.c();
    int j = arrayOfInt.length;
    return i < j;
  }
  
  public final void remove()
  {
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("Not allowed for this iterator");
    throw localIllegalStateException;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.n.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */