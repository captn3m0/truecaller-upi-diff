package com.truecaller.messaging.transport;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.messaging.data.t;
import org.a.a.a.g;
import org.a.a.b;

public class ScheduledMessageReceiver
  extends BroadcastReceiver
{
  static Intent a(Context paramContext, b paramb)
  {
    Intent localIntent = new android/content/Intent;
    Class localClass = ScheduledMessageReceiver.class;
    localIntent.<init>(paramContext, localClass);
    localIntent.setAction("com.truecaller.messaging.transport.SendMessageService.SEND");
    paramContext = "date";
    long l;
    if (paramb != null) {
      l = a;
    } else {
      l = 0L;
    }
    localIntent.putExtra(paramContext, l);
    localIntent.setFlags(268435456);
    return localIntent;
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent == null) {
      return;
    }
    String str1 = "com.truecaller.messaging.transport.SendMessageService.SEND";
    String str2 = paramIntent.getAction();
    boolean bool1 = str1.equals(str2);
    if (!bool1) {
      return;
    }
    str1 = "date";
    long l1 = 0L;
    long l2 = paramIntent.getLongExtra(str1, l1);
    boolean bool2 = l2 < l1;
    if (!bool2)
    {
      bool2 = false;
      paramIntent = null;
    }
    else
    {
      paramIntent = new org/a/a/b;
      paramIntent.<init>(l2);
    }
    ((t)((bk)paramContext.getApplicationContext()).a().p().a()).a(paramIntent).c();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.ScheduledMessageReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */