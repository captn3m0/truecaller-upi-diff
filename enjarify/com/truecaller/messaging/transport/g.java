package com.truecaller.messaging.transport;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.provider.Telephony.Threads;
import com.android.a.a.c;
import java.util.Collections;
import java.util.List;

final class g
  implements f
{
  private static final Uri a = Telephony.Threads.CONTENT_URI.buildUpon().appendQueryParameter("simple", "true").build();
  private static final String[] b = { "_id", "recipient_ids" };
  private static final Uri c = Uri.parse("content://mms-sms/canonical-addresses");
  private final Context d;
  private android.support.v4.f.f e = null;
  
  g(Context paramContext)
  {
    d = paramContext;
  }
  
  /* Error */
  private android.support.v4.f.f a()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aconst_null
    //   3: astore_2
    //   4: aload_0
    //   5: getfield 66	com/truecaller/messaging/transport/g:d	Landroid/content/Context;
    //   8: astore_3
    //   9: aload_3
    //   10: invokevirtual 72	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   13: astore 4
    //   15: getstatic 58	com/truecaller/messaging/transport/g:c	Landroid/net/Uri;
    //   18: astore 5
    //   20: iconst_0
    //   21: istore 6
    //   23: aconst_null
    //   24: astore 7
    //   26: aload 4
    //   28: aload 5
    //   30: aconst_null
    //   31: aconst_null
    //   32: aconst_null
    //   33: aconst_null
    //   34: invokevirtual 78	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   37: astore_2
    //   38: aload_2
    //   39: ifnull +107 -> 146
    //   42: ldc 80
    //   44: astore_3
    //   45: aload_2
    //   46: aload_3
    //   47: invokeinterface 86 2 0
    //   52: istore 8
    //   54: ldc 44
    //   56: astore 4
    //   58: aload_2
    //   59: aload 4
    //   61: invokeinterface 86 2 0
    //   66: istore 9
    //   68: new 88	android/support/v4/f/f
    //   71: astore 5
    //   73: aload_2
    //   74: invokeinterface 92 1 0
    //   79: istore 6
    //   81: aload 5
    //   83: iload 6
    //   85: invokespecial 95	android/support/v4/f/f:<init>	(I)V
    //   88: aload_2
    //   89: invokeinterface 99 1 0
    //   94: istore 6
    //   96: iload 6
    //   98: ifeq +35 -> 133
    //   101: aload_2
    //   102: iload 9
    //   104: invokeinterface 103 2 0
    //   109: lstore 10
    //   111: aload_2
    //   112: iload 8
    //   114: invokeinterface 107 2 0
    //   119: astore 7
    //   121: aload 5
    //   123: lload 10
    //   125: aload 7
    //   127: invokevirtual 110	android/support/v4/f/f:c	(JLjava/lang/Object;)V
    //   130: goto -42 -> 88
    //   133: aload_2
    //   134: ifnull +9 -> 143
    //   137: aload_2
    //   138: invokeinterface 113 1 0
    //   143: aload 5
    //   145: areturn
    //   146: aload_2
    //   147: ifnull +33 -> 180
    //   150: goto +24 -> 174
    //   153: astore_1
    //   154: goto +37 -> 191
    //   157: astore_3
    //   158: iconst_0
    //   159: anewarray 48	java/lang/String
    //   162: astore 4
    //   164: aload_3
    //   165: aload 4
    //   167: invokestatic 119	com/truecaller/log/AssertionUtil:shouldNeverHappen	(Ljava/lang/Throwable;[Ljava/lang/String;)V
    //   170: aload_2
    //   171: ifnull +9 -> 180
    //   174: aload_2
    //   175: invokeinterface 113 1 0
    //   180: new 88	android/support/v4/f/f
    //   183: astore_2
    //   184: aload_2
    //   185: iconst_0
    //   186: invokespecial 95	android/support/v4/f/f:<init>	(I)V
    //   189: aload_2
    //   190: areturn
    //   191: aload_2
    //   192: ifnull +9 -> 201
    //   195: aload_2
    //   196: invokeinterface 113 1 0
    //   201: aload_1
    //   202: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	203	0	this	g
    //   1	1	1	localObject1	Object
    //   153	49	1	localObject2	Object
    //   3	193	2	localObject3	Object
    //   8	39	3	localObject4	Object
    //   157	8	3	localRuntimeException	RuntimeException
    //   13	153	4	localObject5	Object
    //   18	126	5	localObject6	Object
    //   21	63	6	i	int
    //   94	3	6	bool	boolean
    //   24	102	7	str	String
    //   52	61	8	j	int
    //   66	37	9	k	int
    //   109	15	10	l	long
    // Exception table:
    //   from	to	target	type
    //   4	8	153	finally
    //   9	13	153	finally
    //   15	18	153	finally
    //   33	37	153	finally
    //   46	52	153	finally
    //   59	66	153	finally
    //   68	71	153	finally
    //   73	79	153	finally
    //   83	88	153	finally
    //   88	94	153	finally
    //   102	109	153	finally
    //   112	119	153	finally
    //   125	130	153	finally
    //   158	162	153	finally
    //   165	170	153	finally
    //   4	8	157	java/lang/RuntimeException
    //   9	13	157	java/lang/RuntimeException
    //   15	18	157	java/lang/RuntimeException
    //   33	37	157	java/lang/RuntimeException
    //   46	52	157	java/lang/RuntimeException
    //   59	66	157	java/lang/RuntimeException
    //   68	71	157	java/lang/RuntimeException
    //   73	79	157	java/lang/RuntimeException
    //   83	88	157	java/lang/RuntimeException
    //   88	94	157	java/lang/RuntimeException
    //   102	109	157	java/lang/RuntimeException
    //   112	119	157	java/lang/RuntimeException
    //   125	130	157	java/lang/RuntimeException
  }
  
  /* Error */
  private android.support.v4.f.f b()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aconst_null
    //   3: astore_2
    //   4: aload_0
    //   5: getfield 64	com/truecaller/messaging/transport/g:e	Landroid/support/v4/f/f;
    //   8: astore_3
    //   9: aload_3
    //   10: ifnull +8 -> 18
    //   13: aload_0
    //   14: getfield 64	com/truecaller/messaging/transport/g:e	Landroid/support/v4/f/f;
    //   17: areturn
    //   18: aload_0
    //   19: invokespecial 124	com/truecaller/messaging/transport/g:a	()Landroid/support/v4/f/f;
    //   22: astore_3
    //   23: aload_3
    //   24: invokevirtual 126	android/support/v4/f/f:b	()I
    //   27: istore 4
    //   29: iload 4
    //   31: ifne +22 -> 53
    //   34: new 88	android/support/v4/f/f
    //   37: astore_3
    //   38: aload_3
    //   39: iconst_0
    //   40: invokespecial 95	android/support/v4/f/f:<init>	(I)V
    //   43: aload_0
    //   44: aload_3
    //   45: putfield 64	com/truecaller/messaging/transport/g:e	Landroid/support/v4/f/f;
    //   48: aload_0
    //   49: getfield 64	com/truecaller/messaging/transport/g:e	Landroid/support/v4/f/f;
    //   52: areturn
    //   53: aload_0
    //   54: getfield 66	com/truecaller/messaging/transport/g:d	Landroid/content/Context;
    //   57: astore 5
    //   59: aload 5
    //   61: invokevirtual 72	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   64: astore 6
    //   66: getstatic 42	com/truecaller/messaging/transport/g:a	Landroid/net/Uri;
    //   69: astore 7
    //   71: getstatic 50	com/truecaller/messaging/transport/g:b	[Ljava/lang/String;
    //   74: astore 8
    //   76: iconst_0
    //   77: istore 9
    //   79: aconst_null
    //   80: astore 10
    //   82: iconst_0
    //   83: istore 11
    //   85: aload 6
    //   87: aload 7
    //   89: aload 8
    //   91: aconst_null
    //   92: aconst_null
    //   93: aconst_null
    //   94: invokevirtual 78	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   97: astore 5
    //   99: aload 5
    //   101: ifnull +226 -> 327
    //   104: new 88	android/support/v4/f/f
    //   107: astore 6
    //   109: aload 5
    //   111: invokeinterface 92 1 0
    //   116: istore 12
    //   118: aload 6
    //   120: iload 12
    //   122: invokespecial 95	android/support/v4/f/f:<init>	(I)V
    //   125: aload_0
    //   126: aload 6
    //   128: putfield 64	com/truecaller/messaging/transport/g:e	Landroid/support/v4/f/f;
    //   131: aload 5
    //   133: invokeinterface 99 1 0
    //   138: istore 13
    //   140: iload 13
    //   142: ifeq +155 -> 297
    //   145: iconst_1
    //   146: istore 13
    //   148: aload 5
    //   150: iload 13
    //   152: invokeinterface 107 2 0
    //   157: astore 6
    //   159: bipush 32
    //   161: istore 12
    //   163: aload 6
    //   165: iload 12
    //   167: invokestatic 133	org/c/a/a/a/k:a	(Ljava/lang/String;C)[Ljava/lang/String;
    //   170: astore 6
    //   172: new 135	java/util/ArrayList
    //   175: astore 7
    //   177: aload 6
    //   179: arraylength
    //   180: istore 14
    //   182: aload 7
    //   184: iload 14
    //   186: invokespecial 136	java/util/ArrayList:<init>	(I)V
    //   189: aload 6
    //   191: arraylength
    //   192: istore 14
    //   194: iconst_0
    //   195: istore 9
    //   197: iload 9
    //   199: iload 14
    //   201: if_icmpge -70 -> 131
    //   204: aload 6
    //   206: iload 9
    //   208: aaload
    //   209: astore 10
    //   211: aload 10
    //   213: invokestatic 142	java/lang/Long:parseLong	(Ljava/lang/String;)J
    //   216: lstore 15
    //   218: aload_3
    //   219: lload 15
    //   221: aconst_null
    //   222: invokevirtual 145	android/support/v4/f/f:a	(JLjava/lang/Object;)Ljava/lang/Object;
    //   225: astore 10
    //   227: aload 10
    //   229: checkcast 48	java/lang/String
    //   232: astore 10
    //   234: aload 10
    //   236: invokestatic 151	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   239: istore 11
    //   241: iload 11
    //   243: ifne +13 -> 256
    //   246: aload 7
    //   248: aload 10
    //   250: invokeinterface 157 2 0
    //   255: pop
    //   256: aload_0
    //   257: getfield 64	com/truecaller/messaging/transport/g:e	Landroid/support/v4/f/f;
    //   260: astore 10
    //   262: aload 5
    //   264: iconst_0
    //   265: invokeinterface 103 2 0
    //   270: lstore 17
    //   272: aload 7
    //   274: invokestatic 163	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
    //   277: astore 19
    //   279: aload 10
    //   281: lload 17
    //   283: aload 19
    //   285: invokevirtual 165	android/support/v4/f/f:b	(JLjava/lang/Object;)V
    //   288: iload 9
    //   290: iconst_1
    //   291: iadd
    //   292: istore 9
    //   294: goto -97 -> 197
    //   297: aload_0
    //   298: getfield 64	com/truecaller/messaging/transport/g:e	Landroid/support/v4/f/f;
    //   301: astore_1
    //   302: aload 5
    //   304: ifnull +10 -> 314
    //   307: aload 5
    //   309: invokeinterface 113 1 0
    //   314: aload_1
    //   315: areturn
    //   316: astore_2
    //   317: goto +74 -> 391
    //   320: astore_3
    //   321: aload 5
    //   323: astore_1
    //   324: goto +26 -> 350
    //   327: aload 5
    //   329: ifnull +43 -> 372
    //   332: aload 5
    //   334: invokeinterface 113 1 0
    //   339: goto +33 -> 372
    //   342: astore_2
    //   343: aload_1
    //   344: astore 5
    //   346: goto +45 -> 391
    //   349: astore_3
    //   350: iconst_0
    //   351: anewarray 48	java/lang/String
    //   354: astore 5
    //   356: aload_3
    //   357: aload 5
    //   359: invokestatic 119	com/truecaller/log/AssertionUtil:shouldNeverHappen	(Ljava/lang/Throwable;[Ljava/lang/String;)V
    //   362: aload_1
    //   363: ifnull +9 -> 372
    //   366: aload_1
    //   367: invokeinterface 113 1 0
    //   372: new 88	android/support/v4/f/f
    //   375: astore_1
    //   376: aload_1
    //   377: iconst_0
    //   378: invokespecial 95	android/support/v4/f/f:<init>	(I)V
    //   381: aload_0
    //   382: aload_1
    //   383: putfield 64	com/truecaller/messaging/transport/g:e	Landroid/support/v4/f/f;
    //   386: aload_0
    //   387: getfield 64	com/truecaller/messaging/transport/g:e	Landroid/support/v4/f/f;
    //   390: areturn
    //   391: aload 5
    //   393: ifnull +10 -> 403
    //   396: aload 5
    //   398: invokeinterface 113 1 0
    //   403: aload_2
    //   404: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	405	0	this	g
    //   1	382	1	localObject1	Object
    //   3	1	2	localObject2	Object
    //   316	1	2	localObject3	Object
    //   342	62	2	localObject4	Object
    //   8	211	3	localf	android.support.v4.f.f
    //   320	1	3	localRuntimeException1	RuntimeException
    //   349	8	3	localRuntimeException2	RuntimeException
    //   27	3	4	i	int
    //   57	340	5	localObject5	Object
    //   64	141	6	localObject6	Object
    //   69	204	7	localObject7	Object
    //   74	16	8	arrayOfString	String[]
    //   77	216	9	j	int
    //   80	200	10	localObject8	Object
    //   83	159	11	bool	boolean
    //   116	50	12	k	int
    //   138	13	13	m	int
    //   180	22	14	n	int
    //   216	4	15	l1	long
    //   270	12	17	l2	long
    //   277	7	19	localList	List
    // Exception table:
    //   from	to	target	type
    //   104	107	316	finally
    //   109	116	316	finally
    //   120	125	316	finally
    //   126	131	316	finally
    //   131	138	316	finally
    //   150	157	316	finally
    //   165	170	316	finally
    //   172	175	316	finally
    //   177	180	316	finally
    //   184	189	316	finally
    //   189	192	316	finally
    //   206	209	316	finally
    //   211	216	316	finally
    //   221	225	316	finally
    //   227	232	316	finally
    //   234	239	316	finally
    //   248	256	316	finally
    //   256	260	316	finally
    //   264	270	316	finally
    //   272	277	316	finally
    //   283	288	316	finally
    //   297	301	316	finally
    //   104	107	320	java/lang/RuntimeException
    //   109	116	320	java/lang/RuntimeException
    //   120	125	320	java/lang/RuntimeException
    //   126	131	320	java/lang/RuntimeException
    //   131	138	320	java/lang/RuntimeException
    //   150	157	320	java/lang/RuntimeException
    //   165	170	320	java/lang/RuntimeException
    //   172	175	320	java/lang/RuntimeException
    //   177	180	320	java/lang/RuntimeException
    //   184	189	320	java/lang/RuntimeException
    //   189	192	320	java/lang/RuntimeException
    //   206	209	320	java/lang/RuntimeException
    //   211	216	320	java/lang/RuntimeException
    //   221	225	320	java/lang/RuntimeException
    //   227	232	320	java/lang/RuntimeException
    //   234	239	320	java/lang/RuntimeException
    //   248	256	320	java/lang/RuntimeException
    //   256	260	320	java/lang/RuntimeException
    //   264	270	320	java/lang/RuntimeException
    //   272	277	320	java/lang/RuntimeException
    //   283	288	320	java/lang/RuntimeException
    //   297	301	320	java/lang/RuntimeException
    //   4	8	342	finally
    //   13	17	342	finally
    //   18	22	342	finally
    //   23	27	342	finally
    //   34	37	342	finally
    //   39	43	342	finally
    //   44	48	342	finally
    //   48	52	342	finally
    //   53	57	342	finally
    //   59	64	342	finally
    //   66	69	342	finally
    //   71	74	342	finally
    //   93	97	342	finally
    //   350	354	342	finally
    //   357	362	342	finally
    //   4	8	349	java/lang/RuntimeException
    //   13	17	349	java/lang/RuntimeException
    //   18	22	349	java/lang/RuntimeException
    //   23	27	349	java/lang/RuntimeException
    //   34	37	349	java/lang/RuntimeException
    //   39	43	349	java/lang/RuntimeException
    //   44	48	349	java/lang/RuntimeException
    //   48	52	349	java/lang/RuntimeException
    //   53	57	349	java/lang/RuntimeException
    //   59	64	349	java/lang/RuntimeException
    //   66	69	349	java/lang/RuntimeException
    //   71	74	349	java/lang/RuntimeException
    //   93	97	349	java/lang/RuntimeException
  }
  
  public final String a(long paramLong, Uri paramUri)
  {
    Object localObject1 = a(paramLong);
    boolean bool1 = ((List)localObject1).isEmpty();
    if (bool1) {
      return "";
    }
    int i = ((List)localObject1).size();
    int j = 1;
    if (i == j) {
      return (String)((List)localObject1).get(0);
    }
    localObject1 = null;
    try
    {
      Object localObject2 = d;
      ContentResolver localContentResolver = ((Context)localObject2).getContentResolver();
      localObject2 = "addr";
      Uri localUri = Uri.withAppendedPath(paramUri, (String)localObject2);
      localObject2 = "address";
      paramUri = "charset";
      String[] arrayOfString = { localObject2, paramUri };
      String str = "type=137";
      localObject1 = localContentResolver.query(localUri, arrayOfString, str, null, null);
      if (localObject1 != null)
      {
        boolean bool2 = ((Cursor)localObject1).moveToFirst();
        if (bool2)
        {
          localObject2 = ((Cursor)localObject1).getString(0);
          int k = ((Cursor)localObject1).getInt(j);
          localObject2 = c.a((String)localObject2, k);
          k = ((Cursor)localObject1).getInt(j);
          localObject2 = c.a((byte[])localObject2, k);
          return (String)localObject2;
        }
      }
      return "";
    }
    finally
    {
      if (localObject1 != null) {
        ((Cursor)localObject1).close();
      }
    }
  }
  
  public final List a(long paramLong)
  {
    android.support.v4.f.f localf = b();
    List localList = (List)localf.a(paramLong, null);
    if (localList == null) {
      localList = Collections.emptyList();
    }
    return localList;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */