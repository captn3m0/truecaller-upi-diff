package com.truecaller.messaging.transport;

import dagger.a.d;
import javax.inject.Provider;

public final class r
  implements d
{
  private final o a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private r(o paramo, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramo;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static r a(o paramo, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    r localr = new com/truecaller/messaging/transport/r;
    localr.<init>(paramo, paramProvider1, paramProvider2, paramProvider3);
    return localr;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */