package com.truecaller.messaging.transport;

import android.content.ContentValues;
import android.net.Uri;

public final class ad$a$a
{
  final int a;
  final Uri b;
  final ContentValues c;
  String d;
  String[] e;
  
  private ad$a$a(int paramInt, Uri paramUri)
  {
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    c = localContentValues;
    a = paramInt;
    b = paramUri;
  }
  
  public final a a(String paramString, Integer paramInteger)
  {
    c.put(paramString, paramInteger);
    return this;
  }
  
  public final a a(String paramString, String[] paramArrayOfString)
  {
    d = paramString;
    e = paramArrayOfString;
    return this;
  }
  
  public final ad.a a()
  {
    ad.a locala = new com/truecaller/messaging/transport/ad$a;
    locala.<init>(this, (byte)0);
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.ad.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */