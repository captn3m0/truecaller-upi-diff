package com.truecaller.messaging.transport;

import android.content.Intent;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.utils.q;
import java.util.List;
import java.util.Set;
import org.a.a.b;

public abstract interface l
{
  public abstract long a(long paramLong);
  
  public abstract long a(f paramf, i parami, r paramr, b paramb1, b paramb2, int paramInt, List paramList, q paramq, boolean paramBoolean1, boolean paramBoolean2, Set paramSet);
  
  public abstract l.a a(Message paramMessage, Participant[] paramArrayOfParticipant);
  
  public abstract String a();
  
  public abstract String a(String paramString);
  
  public abstract void a(Intent paramIntent, int paramInt);
  
  public abstract void a(BinaryEntity paramBinaryEntity);
  
  public abstract void a(Message paramMessage, String paramString1, String paramString2);
  
  public abstract void a(b paramb);
  
  public abstract boolean a(Message paramMessage);
  
  public abstract boolean a(Message paramMessage, Entity paramEntity);
  
  public abstract boolean a(Message paramMessage, ad paramad);
  
  public abstract boolean a(Participant paramParticipant);
  
  public abstract boolean a(TransportInfo paramTransportInfo, long paramLong1, long paramLong2, ad paramad);
  
  public abstract boolean a(TransportInfo paramTransportInfo, ad paramad);
  
  public abstract boolean a(TransportInfo paramTransportInfo, ad paramad, boolean paramBoolean);
  
  public abstract boolean a(ad paramad);
  
  public abstract boolean a(String paramString, a parama);
  
  public abstract ad b();
  
  public abstract void b(long paramLong);
  
  public abstract boolean b(Message paramMessage);
  
  public abstract boolean b(ad paramad);
  
  public abstract boolean c();
  
  public abstract boolean c(Message paramMessage);
  
  public abstract k d(Message paramMessage);
  
  public abstract b d();
  
  public abstract int e(Message paramMessage);
  
  public abstract boolean e();
  
  public abstract int f();
  
  public abstract boolean f(Message paramMessage);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */