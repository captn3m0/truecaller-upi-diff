package com.truecaller.messaging.transport;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import com.truecaller.content.TruecallerContract.aa;
import com.truecaller.content.TruecallerContract.z;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.a.r;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.multisim.h;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.a.a.a.g;

public abstract class c
{
  protected final Context a;
  protected final h b;
  protected final e c;
  
  protected c(Context paramContext, h paramh, e parame)
  {
    paramContext = paramContext.getApplicationContext();
    a = paramContext;
    b = paramh;
    c = parame;
  }
  
  private void a(f paramf, i parami, List paramList, Message paramMessage, boolean paramBoolean, Set paramSet)
  {
    Object localObject1 = paramMessage;
    ContentProviderOperation.Builder localBuilder1 = ContentProviderOperation.newInsert(TruecallerContract.aa.a());
    Object localObject2 = m;
    int i = paramList.size();
    long l1 = ((TransportInfo)localObject2).e();
    Object localObject3 = c;
    localObject2 = this;
    Object localObject4 = paramf;
    Object localObject5 = parami;
    localObject2 = a(l1, paramf, parami, (Participant)localObject3, paramBoolean);
    a((Set)localObject2);
    Object localObject6 = ((Set)localObject2).iterator();
    int j = -1;
    int k = -1;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject6).hasNext();
      if (!bool1) {
        break;
      }
      localObject5 = (Participant)((Iterator)localObject6).next();
      localObject3 = c;
      boolean bool2 = ((Participant)localObject3).equals(localObject5);
      if (bool2) {
        k = com.truecaller.messaging.data.b.a(paramList, (Participant)localObject5);
      } else {
        com.truecaller.messaging.data.b.a(paramList, (Participant)localObject5);
      }
    }
    boolean bool3 = ((Set)localObject2).isEmpty();
    if (bool3)
    {
      i1 = paramList.size();
      paramList.subList(i, i1).clear();
      return;
    }
    if (k == j)
    {
      localObject6 = c;
      k = com.truecaller.messaging.data.b.a(paramList, (Participant)localObject6);
    }
    int i1 = com.truecaller.messaging.data.b.a(paramList, (Set)localObject2);
    localBuilder1.withValueBackReference("participant_id", k);
    localBuilder1.withValueBackReference("conversation_id", i1);
    localObject6 = Long.valueOf(d.a);
    localBuilder1.withValue("date_sent", localObject6);
    l1 = e.a;
    localObject6 = Long.valueOf(l1);
    localBuilder1.withValue("date", localObject6);
    localObject6 = Integer.valueOf(f);
    localBuilder1.withValue("status", localObject6);
    localObject6 = Boolean.valueOf(g);
    localBuilder1.withValue("seen", localObject6);
    localObject6 = Boolean.valueOf(h);
    localBuilder1.withValue("read", localObject6);
    localObject6 = Boolean.valueOf(i);
    localBuilder1.withValue("locked", localObject6);
    localObject6 = Integer.valueOf(j);
    localBuilder1.withValue("transport", localObject6);
    localObject6 = l;
    localBuilder1.withValue("sim_token", localObject6);
    localObject6 = o;
    localBuilder1.withValue("analytics_id", localObject6);
    localObject6 = p;
    localBuilder1.withValue("raw_address", localObject6);
    localObject2 = "category";
    int n = 2;
    Integer localInteger = Integer.valueOf(n);
    localBuilder1.withValue((String)localObject2, localInteger);
    i1 = f;
    boolean bool4 = a(i1);
    j = 0;
    localInteger = null;
    if (bool4)
    {
      localObject6 = Integer.valueOf(0);
      localBuilder1.withValue("classification", localObject6);
      long l2 = -1;
      localObject2 = Long.valueOf(l2);
      localObject6 = paramSet;
      paramSet.add(localObject2);
    }
    else
    {
      localObject2 = "classification";
      localObject6 = Integer.valueOf(n);
      localBuilder1.withValue((String)localObject2, localObject6);
    }
    localObject2 = "sync_status";
    n = r;
    localObject6 = Integer.valueOf(n);
    localBuilder1.withValue((String)localObject2, localObject6);
    int i2 = paramList.size();
    localObject6 = localBuilder1.build();
    paramList.add(localObject6);
    localObject6 = m;
    localObject4 = paramf;
    a(paramf, paramList, (TransportInfo)localObject6, i2);
    localObject6 = new android/content/ContentValues;
    ((ContentValues)localObject6).<init>();
    localObject4 = n;
    int m = localObject4.length;
    while (j < m)
    {
      localObject3 = localObject4[j];
      ContentProviderOperation.Builder localBuilder2 = ContentProviderOperation.newInsert(TruecallerContract.z.a());
      localObject1 = "message_id";
      localBuilder2.withValueBackReference((String)localObject1, i2);
      ((ContentValues)localObject6).clear();
      ((Entity)localObject3).a((ContentValues)localObject6);
      localBuilder2.withValues((ContentValues)localObject6);
      localObject3 = localBuilder2.build();
      paramList.add(localObject3);
      j += 1;
    }
  }
  
  private static void a(Set paramSet)
  {
    paramSet = paramSet.iterator();
    for (;;)
    {
      boolean bool = paramSet.hasNext();
      if (!bool) {
        break;
      }
      Participant localParticipant = (Participant)paramSet.next();
      bool = localParticipant.c();
      if (bool) {
        paramSet.remove();
      }
    }
  }
  
  /* Error */
  public long a(f paramf, i parami, r paramr, org.a.a.b paramb1, org.a.a.b paramb2, int paramInt, List paramList, com.truecaller.utils.q paramq, boolean paramBoolean1, boolean paramBoolean2, Set paramSet)
  {
    // Byte code:
    //   0: aload_0
    //   1: astore 12
    //   3: aload_3
    //   4: astore 13
    //   6: aload 7
    //   8: astore 14
    //   10: aload 8
    //   12: astore 15
    //   14: iconst_0
    //   15: istore 16
    //   17: aconst_null
    //   18: astore 17
    //   20: aload_0
    //   21: getfield 22	com/truecaller/messaging/transport/c:a	Landroid/content/Context;
    //   24: astore 18
    //   26: aload 18
    //   28: invokevirtual 247	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   31: astore 19
    //   33: aload_0
    //   34: astore 18
    //   36: aload_1
    //   37: astore 20
    //   39: aload_2
    //   40: astore 21
    //   42: aload 4
    //   44: astore 22
    //   46: iload 9
    //   48: istore 23
    //   50: aload_0
    //   51: aload 19
    //   53: aload_1
    //   54: aload_2
    //   55: aload 4
    //   57: aload 5
    //   59: iload 9
    //   61: iload 10
    //   63: invokevirtual 250	com/truecaller/messaging/transport/c:a	(Landroid/content/ContentResolver;Lcom/truecaller/messaging/transport/f;Lcom/truecaller/messaging/transport/i;Lorg/a/a/b;Lorg/a/a/b;ZZ)Lcom/truecaller/messaging/transport/c$a;
    //   66: astore 24
    //   68: aload 24
    //   70: ifnull +1732 -> 1802
    //   73: aload_3
    //   74: invokeinterface 255 1 0
    //   79: istore 25
    //   81: aload 24
    //   83: invokeinterface 258 1 0
    //   88: istore 26
    //   90: iload 6
    //   92: istore 27
    //   94: iload 25
    //   96: istore 28
    //   98: iconst_0
    //   99: istore 23
    //   101: aconst_null
    //   102: astore 29
    //   104: iconst_0
    //   105: istore 30
    //   107: iconst_0
    //   108: istore 31
    //   110: iload 28
    //   112: ifeq +1238 -> 1350
    //   115: iload 26
    //   117: ifeq +1233 -> 1350
    //   120: aload_3
    //   121: invokeinterface 259 1 0
    //   126: lstore 32
    //   128: aload 24
    //   130: invokeinterface 261 1 0
    //   135: lstore 34
    //   137: lload 32
    //   139: lload 34
    //   141: lcmp
    //   142: istore 25
    //   144: iload 25
    //   146: ifle +60 -> 206
    //   149: aload_3
    //   150: invokeinterface 259 1 0
    //   155: lstore 32
    //   157: iload 23
    //   159: istore 36
    //   161: aload_3
    //   162: invokeinterface 263 1 0
    //   167: lstore 37
    //   169: aload 14
    //   171: lload 37
    //   173: invokestatic 266	com/truecaller/messaging/data/b:a	(Ljava/util/List;J)V
    //   176: iload 31
    //   178: iconst_1
    //   179: iadd
    //   180: istore 31
    //   182: iload 27
    //   184: iconst_m1
    //   185: iadd
    //   186: istore 27
    //   188: aload_3
    //   189: invokeinterface 255 1 0
    //   194: istore 25
    //   196: lload 32
    //   198: lstore 34
    //   200: iconst_1
    //   201: istore 28
    //   203: goto +1019 -> 1222
    //   206: iload 23
    //   208: istore 36
    //   210: aload_3
    //   211: invokeinterface 259 1 0
    //   216: lstore 32
    //   218: aload 24
    //   220: invokeinterface 261 1 0
    //   225: lstore 39
    //   227: lload 32
    //   229: lload 39
    //   231: lcmp
    //   232: istore 25
    //   234: iload 25
    //   236: ifge +98 -> 334
    //   239: aload 24
    //   241: invokeinterface 261 1 0
    //   246: lstore 34
    //   248: aload 24
    //   250: invokeinterface 270 1 0
    //   255: astore 22
    //   257: aload_0
    //   258: astore 18
    //   260: aload_1
    //   261: astore 19
    //   263: aload_2
    //   264: astore 20
    //   266: aload 7
    //   268: astore 21
    //   270: iconst_1
    //   271: istore 23
    //   273: iload 36
    //   275: istore 16
    //   277: iload 28
    //   279: istore 36
    //   281: iconst_1
    //   282: istore 28
    //   284: aload 11
    //   286: astore 29
    //   288: aload_0
    //   289: aload_1
    //   290: aload_2
    //   291: aload 7
    //   293: aload 22
    //   295: iload 9
    //   297: aload 11
    //   299: invokespecial 273	com/truecaller/messaging/transport/c:a	(Lcom/truecaller/messaging/transport/f;Lcom/truecaller/messaging/transport/i;Ljava/util/List;Lcom/truecaller/messaging/data/types/Message;ZLjava/util/Set;)V
    //   302: iload 30
    //   304: iconst_1
    //   305: iadd
    //   306: istore 30
    //   308: iload 27
    //   310: iconst_m1
    //   311: iadd
    //   312: istore 27
    //   314: aload 24
    //   316: invokeinterface 258 1 0
    //   321: istore 26
    //   323: iload 16
    //   325: istore 23
    //   327: iload 36
    //   329: istore 25
    //   331: goto +891 -> 1222
    //   334: iload 23
    //   336: istore 16
    //   338: iload 28
    //   340: istore 36
    //   342: iconst_1
    //   343: istore 28
    //   345: aload_3
    //   346: invokeinterface 275 1 0
    //   351: lstore 32
    //   353: aload 24
    //   355: invokeinterface 276 1 0
    //   360: lstore 39
    //   362: lload 32
    //   364: lload 39
    //   366: lcmp
    //   367: istore 25
    //   369: iload 25
    //   371: ifle +53 -> 424
    //   374: aload_3
    //   375: invokeinterface 259 1 0
    //   380: lstore 32
    //   382: aload_3
    //   383: invokeinterface 263 1 0
    //   388: lstore 39
    //   390: aload 14
    //   392: lload 39
    //   394: invokestatic 266	com/truecaller/messaging/data/b:a	(Ljava/util/List;J)V
    //   397: iload 31
    //   399: iconst_1
    //   400: iadd
    //   401: istore 31
    //   403: iload 27
    //   405: iconst_m1
    //   406: iadd
    //   407: istore 27
    //   409: aload_3
    //   410: invokeinterface 255 1 0
    //   415: istore 25
    //   417: lload 32
    //   419: lstore 34
    //   421: goto +801 -> 1222
    //   424: aload_3
    //   425: invokeinterface 275 1 0
    //   430: lstore 41
    //   432: aload 24
    //   434: invokeinterface 276 1 0
    //   439: lstore 32
    //   441: lload 41
    //   443: lload 32
    //   445: lcmp
    //   446: istore 43
    //   448: iload 43
    //   450: ifge +80 -> 530
    //   453: aload 24
    //   455: invokeinterface 261 1 0
    //   460: lstore 34
    //   462: aload 24
    //   464: invokeinterface 270 1 0
    //   469: astore 22
    //   471: aload_0
    //   472: astore 18
    //   474: aload_1
    //   475: astore 19
    //   477: aload_2
    //   478: astore 20
    //   480: aload 7
    //   482: astore 21
    //   484: aload 11
    //   486: astore 29
    //   488: aload_0
    //   489: aload_1
    //   490: aload_2
    //   491: aload 7
    //   493: aload 22
    //   495: iload 9
    //   497: aload 11
    //   499: invokespecial 273	com/truecaller/messaging/transport/c:a	(Lcom/truecaller/messaging/transport/f;Lcom/truecaller/messaging/transport/i;Ljava/util/List;Lcom/truecaller/messaging/data/types/Message;ZLjava/util/Set;)V
    //   502: iload 30
    //   504: iconst_1
    //   505: iadd
    //   506: istore 30
    //   508: iload 27
    //   510: iconst_m1
    //   511: iadd
    //   512: istore 27
    //   514: aload 24
    //   516: invokeinterface 258 1 0
    //   521: istore 26
    //   523: iload 36
    //   525: istore 25
    //   527: goto +695 -> 1222
    //   530: aload 12
    //   532: aload 13
    //   534: aload 24
    //   536: invokevirtual 279	com/truecaller/messaging/transport/c:b	(Lcom/truecaller/messaging/data/a/r;Lcom/truecaller/messaging/transport/c$a;)Z
    //   539: istore 25
    //   541: iload 25
    //   543: ifeq +148 -> 691
    //   546: aload_3
    //   547: invokeinterface 281 1 0
    //   552: istore 25
    //   554: aload_3
    //   555: invokeinterface 263 1 0
    //   560: lstore 44
    //   562: aload 14
    //   564: lload 44
    //   566: invokestatic 266	com/truecaller/messaging/data/b:a	(Ljava/util/List;J)V
    //   569: iload 31
    //   571: iconst_1
    //   572: iadd
    //   573: istore 31
    //   575: iload 27
    //   577: iconst_m1
    //   578: iadd
    //   579: istore 27
    //   581: aload_3
    //   582: invokeinterface 255 1 0
    //   587: istore 46
    //   589: aload 24
    //   591: invokeinterface 261 1 0
    //   596: lstore 47
    //   598: aload 24
    //   600: invokeinterface 270 1 0
    //   605: astore 19
    //   607: aload 19
    //   609: invokevirtual 284	com/truecaller/messaging/data/types/Message:m	()Lcom/truecaller/messaging/data/types/Message$a;
    //   612: astore 19
    //   614: aload 19
    //   616: iload 25
    //   618: putfield 289	com/truecaller/messaging/data/types/Message$a:s	I
    //   621: aload 19
    //   623: invokevirtual 291	com/truecaller/messaging/data/types/Message$a:b	()Lcom/truecaller/messaging/data/types/Message;
    //   626: astore 22
    //   628: aload_0
    //   629: astore 18
    //   631: aload_1
    //   632: astore 19
    //   634: aload_2
    //   635: astore 20
    //   637: aload 7
    //   639: astore 21
    //   641: aload 11
    //   643: astore 29
    //   645: aload_0
    //   646: aload_1
    //   647: aload_2
    //   648: aload 7
    //   650: aload 22
    //   652: iload 9
    //   654: aload 11
    //   656: invokespecial 273	com/truecaller/messaging/transport/c:a	(Lcom/truecaller/messaging/transport/f;Lcom/truecaller/messaging/transport/i;Ljava/util/List;Lcom/truecaller/messaging/data/types/Message;ZLjava/util/Set;)V
    //   659: iload 30
    //   661: iconst_1
    //   662: iadd
    //   663: istore 30
    //   665: iload 27
    //   667: iconst_m1
    //   668: iadd
    //   669: istore 27
    //   671: aload 24
    //   673: invokeinterface 258 1 0
    //   678: istore 26
    //   680: iload 46
    //   682: istore 25
    //   684: lload 47
    //   686: lstore 34
    //   688: goto +534 -> 1222
    //   691: aload 24
    //   693: invokeinterface 261 1 0
    //   698: lstore 34
    //   700: aload 12
    //   702: aload 13
    //   704: aload 24
    //   706: invokevirtual 293	com/truecaller/messaging/transport/c:a	(Lcom/truecaller/messaging/data/a/r;Lcom/truecaller/messaging/transport/c$a;)Z
    //   709: istore 25
    //   711: iload 25
    //   713: ifeq +186 -> 899
    //   716: aload_3
    //   717: invokeinterface 296 1 0
    //   722: istore 25
    //   724: iload 25
    //   726: i2l
    //   727: lstore 37
    //   729: aload 24
    //   731: invokeinterface 297 1 0
    //   736: istore 25
    //   738: iload 25
    //   740: i2l
    //   741: lstore 49
    //   743: aload_0
    //   744: astore 18
    //   746: aload_1
    //   747: astore 19
    //   749: aload_2
    //   750: astore 20
    //   752: lload 49
    //   754: lstore 51
    //   756: aload 7
    //   758: astore 21
    //   760: aload_3
    //   761: astore 22
    //   763: lload 37
    //   765: lstore 53
    //   767: iload 9
    //   769: istore 23
    //   771: aload_0
    //   772: aload_1
    //   773: aload_2
    //   774: aload 7
    //   776: aload_3
    //   777: aload 24
    //   779: iload 9
    //   781: invokevirtual 300	com/truecaller/messaging/transport/c:a	(Lcom/truecaller/messaging/transport/f;Lcom/truecaller/messaging/transport/i;Ljava/util/List;Lcom/truecaller/messaging/data/a/r;Lcom/truecaller/messaging/transport/c$a;Z)Z
    //   784: istore 25
    //   786: iload 25
    //   788: ifeq +18 -> 806
    //   791: iload 16
    //   793: iconst_1
    //   794: iadd
    //   795: istore 23
    //   797: iload 27
    //   799: iconst_m1
    //   800: iadd
    //   801: istore 27
    //   803: goto +7 -> 810
    //   806: iload 16
    //   808: istore 23
    //   810: aload_3
    //   811: invokeinterface 296 1 0
    //   816: istore 25
    //   818: iload 25
    //   820: i2l
    //   821: lstore 41
    //   823: lload 53
    //   825: lload 41
    //   827: lcmp
    //   828: istore 55
    //   830: iload 55
    //   832: ifne +35 -> 867
    //   835: aload 24
    //   837: invokeinterface 297 1 0
    //   842: istore 25
    //   844: iload 25
    //   846: i2l
    //   847: lstore 41
    //   849: lload 51
    //   851: lload 41
    //   853: lcmp
    //   854: istore 55
    //   856: iload 55
    //   858: ifne +9 -> 867
    //   861: iconst_1
    //   862: istore 25
    //   864: goto +9 -> 873
    //   867: iconst_0
    //   868: istore 25
    //   870: aconst_null
    //   871: astore 18
    //   873: ldc_w 302
    //   876: astore 19
    //   878: iconst_1
    //   879: anewarray 304	java/lang/String
    //   882: dup
    //   883: iconst_0
    //   884: aload 19
    //   886: aastore
    //   887: astore 19
    //   889: iload 25
    //   891: aload 19
    //   893: invokestatic 310	com/truecaller/log/AssertionUtil$AlwaysFatal:isTrue	(Z[Ljava/lang/String;)V
    //   896: goto +3 -> 899
    //   899: aload_3
    //   900: invokeinterface 281 1 0
    //   905: istore 25
    //   907: iload 25
    //   909: iload 28
    //   911: if_icmpne +128 -> 1039
    //   914: aload_3
    //   915: invokeinterface 312 1 0
    //   920: istore 25
    //   922: aload 24
    //   924: invokeinterface 314 1 0
    //   929: istore 26
    //   931: iload 25
    //   933: iload 26
    //   935: if_icmpne +104 -> 1039
    //   938: aload_3
    //   939: invokeinterface 316 1 0
    //   944: istore 25
    //   946: aload 24
    //   948: invokeinterface 318 1 0
    //   953: istore 26
    //   955: iload 25
    //   957: iload 26
    //   959: if_icmpne +80 -> 1039
    //   962: aload_3
    //   963: invokeinterface 263 1 0
    //   968: lstore 41
    //   970: lload 41
    //   972: invokestatic 321	com/truecaller/content/TruecallerContract$aa:a	(J)Landroid/net/Uri;
    //   975: astore 18
    //   977: aload 18
    //   979: invokestatic 324	android/content/ContentProviderOperation:newUpdate	(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;
    //   982: astore 18
    //   984: ldc -51
    //   986: astore 19
    //   988: iconst_0
    //   989: istore 55
    //   991: aconst_null
    //   992: astore 20
    //   994: iconst_0
    //   995: invokestatic 150	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   998: astore 21
    //   1000: aload 18
    //   1002: aload 19
    //   1004: aload 21
    //   1006: invokevirtual 135	android/content/ContentProviderOperation$Builder:withValue	(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;
    //   1009: pop
    //   1010: aload 18
    //   1012: invokevirtual 212	android/content/ContentProviderOperation$Builder:build	()Landroid/content/ContentProviderOperation;
    //   1015: astore 18
    //   1017: aload 14
    //   1019: aload 18
    //   1021: invokeinterface 213 2 0
    //   1026: pop
    //   1027: iload 23
    //   1029: iconst_1
    //   1030: iadd
    //   1031: istore 23
    //   1033: iload 27
    //   1035: iconst_m1
    //   1036: iadd
    //   1037: istore 27
    //   1039: aload 12
    //   1041: getfield 26	com/truecaller/messaging/transport/c:c	Lcom/truecaller/featuretoggles/e;
    //   1044: astore 18
    //   1046: aload 18
    //   1048: invokevirtual 330	com/truecaller/featuretoggles/e:w	()Lcom/truecaller/featuretoggles/b;
    //   1051: astore 18
    //   1053: aload 18
    //   1055: invokeinterface 334 1 0
    //   1060: istore 25
    //   1062: iload 25
    //   1064: ifeq +141 -> 1205
    //   1067: aload_3
    //   1068: invokeinterface 336 1 0
    //   1073: istore 25
    //   1075: iload 25
    //   1077: ifne +125 -> 1202
    //   1080: aload_3
    //   1081: invokeinterface 338 1 0
    //   1086: istore 25
    //   1088: aload 12
    //   1090: iload 25
    //   1092: invokevirtual 198	com/truecaller/messaging/transport/c:a	(I)Z
    //   1095: istore 25
    //   1097: iload 25
    //   1099: ifeq +31 -> 1130
    //   1102: aload_3
    //   1103: invokeinterface 263 1 0
    //   1108: lstore 41
    //   1110: lload 41
    //   1112: invokestatic 131	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   1115: astore 18
    //   1117: aload 11
    //   1119: aload 18
    //   1121: invokeinterface 203 2 0
    //   1126: pop
    //   1127: goto +66 -> 1193
    //   1130: aload_3
    //   1131: invokeinterface 263 1 0
    //   1136: lstore 41
    //   1138: lload 41
    //   1140: invokestatic 321	com/truecaller/content/TruecallerContract$aa:a	(J)Landroid/net/Uri;
    //   1143: astore 18
    //   1145: aload 18
    //   1147: invokestatic 324	android/content/ContentProviderOperation:newUpdate	(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;
    //   1150: astore 18
    //   1152: ldc -56
    //   1154: astore 19
    //   1156: iconst_2
    //   1157: istore 55
    //   1159: iload 55
    //   1161: invokestatic 150	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   1164: astore 20
    //   1166: aload 18
    //   1168: aload 19
    //   1170: aload 20
    //   1172: invokevirtual 135	android/content/ContentProviderOperation$Builder:withValue	(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;
    //   1175: pop
    //   1176: aload 18
    //   1178: invokevirtual 212	android/content/ContentProviderOperation$Builder:build	()Landroid/content/ContentProviderOperation;
    //   1181: astore 18
    //   1183: aload 14
    //   1185: aload 18
    //   1187: invokeinterface 213 2 0
    //   1192: pop
    //   1193: iload 27
    //   1195: iconst_m1
    //   1196: iadd
    //   1197: istore 27
    //   1199: goto +6 -> 1205
    //   1202: goto +3 -> 1205
    //   1205: aload_3
    //   1206: invokeinterface 255 1 0
    //   1211: istore 25
    //   1213: aload 24
    //   1215: invokeinterface 258 1 0
    //   1220: istore 26
    //   1222: iload 27
    //   1224: ifgt +119 -> 1343
    //   1227: iload 28
    //   1229: anewarray 304	java/lang/String
    //   1232: astore 13
    //   1234: new 340	java/lang/StringBuilder
    //   1237: astore 18
    //   1239: ldc_w 342
    //   1242: astore 19
    //   1244: aload 18
    //   1246: aload 19
    //   1248: invokespecial 345	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1251: aload 18
    //   1253: iload 30
    //   1255: invokevirtual 349	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1258: pop
    //   1259: ldc_w 351
    //   1262: astore 19
    //   1264: aload 18
    //   1266: aload 19
    //   1268: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1271: pop
    //   1272: aload 18
    //   1274: iload 23
    //   1276: invokevirtual 349	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1279: pop
    //   1280: ldc_w 356
    //   1283: astore 19
    //   1285: aload 18
    //   1287: aload 19
    //   1289: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1292: pop
    //   1293: aload 18
    //   1295: iload 31
    //   1297: invokevirtual 349	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1300: pop
    //   1301: ldc_w 358
    //   1304: astore 19
    //   1306: aload 18
    //   1308: aload 19
    //   1310: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1313: pop
    //   1314: aload 18
    //   1316: invokevirtual 362	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1319: astore 18
    //   1321: iconst_0
    //   1322: istore 26
    //   1324: aconst_null
    //   1325: astore 19
    //   1327: aload 13
    //   1329: iconst_0
    //   1330: aload 18
    //   1332: aastore
    //   1333: lload 34
    //   1335: invokestatic 367	com/truecaller/messaging/transport/l$b:a	(J)J
    //   1338: lstore 56
    //   1340: goto +433 -> 1773
    //   1343: iload 25
    //   1345: istore 28
    //   1347: goto -1237 -> 110
    //   1350: iload 23
    //   1352: istore 16
    //   1354: iload 28
    //   1356: istore 36
    //   1358: iconst_1
    //   1359: istore 28
    //   1361: iload 26
    //   1363: ifeq +210 -> 1573
    //   1366: aload 24
    //   1368: invokeinterface 270 1 0
    //   1373: astore 29
    //   1375: aload_0
    //   1376: astore 18
    //   1378: aload_1
    //   1379: astore 19
    //   1381: aload_2
    //   1382: astore 20
    //   1384: aload 7
    //   1386: astore 21
    //   1388: aload 29
    //   1390: astore 22
    //   1392: aload 11
    //   1394: astore 29
    //   1396: aload_0
    //   1397: aload_1
    //   1398: aload_2
    //   1399: aload 7
    //   1401: aload 22
    //   1403: iload 9
    //   1405: aload 11
    //   1407: invokespecial 273	com/truecaller/messaging/transport/c:a	(Lcom/truecaller/messaging/transport/f;Lcom/truecaller/messaging/transport/i;Ljava/util/List;Lcom/truecaller/messaging/data/types/Message;ZLjava/util/Set;)V
    //   1410: iload 30
    //   1412: iconst_1
    //   1413: iadd
    //   1414: istore 30
    //   1416: iload 27
    //   1418: iconst_m1
    //   1419: iadd
    //   1420: istore 27
    //   1422: iload 27
    //   1424: ifgt +137 -> 1561
    //   1427: iload 28
    //   1429: anewarray 304	java/lang/String
    //   1432: astore 13
    //   1434: new 340	java/lang/StringBuilder
    //   1437: astore 18
    //   1439: ldc_w 342
    //   1442: astore 19
    //   1444: aload 18
    //   1446: aload 19
    //   1448: invokespecial 345	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1451: aload 18
    //   1453: iload 30
    //   1455: invokevirtual 349	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1458: pop
    //   1459: ldc_w 351
    //   1462: astore 19
    //   1464: aload 18
    //   1466: aload 19
    //   1468: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1471: pop
    //   1472: aload 18
    //   1474: iload 16
    //   1476: invokevirtual 349	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1479: pop
    //   1480: ldc_w 356
    //   1483: astore 19
    //   1485: aload 18
    //   1487: aload 19
    //   1489: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1492: pop
    //   1493: aload 18
    //   1495: iload 31
    //   1497: invokevirtual 349	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1500: pop
    //   1501: ldc_w 358
    //   1504: astore 19
    //   1506: aload 18
    //   1508: aload 19
    //   1510: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1513: pop
    //   1514: aload 18
    //   1516: invokevirtual 362	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1519: astore 18
    //   1521: iconst_0
    //   1522: istore 26
    //   1524: aconst_null
    //   1525: astore 19
    //   1527: aload 13
    //   1529: iconst_0
    //   1530: aload 18
    //   1532: aastore
    //   1533: aload 22
    //   1535: astore 13
    //   1537: aload 22
    //   1539: getfield 139	com/truecaller/messaging/data/types/Message:e	Lorg/a/a/b;
    //   1542: astore 13
    //   1544: aload 13
    //   1546: getfield 125	org/a/a/a/g:a	J
    //   1549: lstore 56
    //   1551: lload 56
    //   1553: invokestatic 367	com/truecaller/messaging/transport/l$b:a	(J)J
    //   1556: lstore 56
    //   1558: goto +215 -> 1773
    //   1561: aload 24
    //   1563: invokeinterface 258 1 0
    //   1568: istore 26
    //   1570: goto -209 -> 1361
    //   1573: iload 36
    //   1575: istore 25
    //   1577: iload 25
    //   1579: ifeq +35 -> 1614
    //   1582: aload_3
    //   1583: invokeinterface 263 1 0
    //   1588: lstore 41
    //   1590: aload 14
    //   1592: lload 41
    //   1594: invokestatic 266	com/truecaller/messaging/data/b:a	(Ljava/util/List;J)V
    //   1597: iload 31
    //   1599: iconst_1
    //   1600: iadd
    //   1601: istore 31
    //   1603: aload_3
    //   1604: invokeinterface 255 1 0
    //   1609: istore 25
    //   1611: goto -34 -> 1577
    //   1614: ldc_w 369
    //   1617: astore 13
    //   1619: aload 15
    //   1621: aload 13
    //   1623: iload 30
    //   1625: invokeinterface 374 3 0
    //   1630: ldc_w 376
    //   1633: astore 13
    //   1635: aload 15
    //   1637: aload 13
    //   1639: iload 16
    //   1641: invokeinterface 374 3 0
    //   1646: ldc_w 378
    //   1649: astore 13
    //   1651: aload 15
    //   1653: aload 13
    //   1655: iload 31
    //   1657: invokeinterface 374 3 0
    //   1662: iload 28
    //   1664: anewarray 304	java/lang/String
    //   1667: astore 13
    //   1669: new 340	java/lang/StringBuilder
    //   1672: astore 18
    //   1674: ldc_w 380
    //   1677: astore 19
    //   1679: aload 18
    //   1681: aload 19
    //   1683: invokespecial 345	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1686: aload 18
    //   1688: iload 30
    //   1690: invokevirtual 349	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1693: pop
    //   1694: ldc_w 351
    //   1697: astore 19
    //   1699: aload 18
    //   1701: aload 19
    //   1703: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1706: pop
    //   1707: aload 18
    //   1709: iload 16
    //   1711: invokevirtual 349	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1714: pop
    //   1715: ldc_w 356
    //   1718: astore 19
    //   1720: aload 18
    //   1722: aload 19
    //   1724: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1727: pop
    //   1728: aload 18
    //   1730: iload 31
    //   1732: invokevirtual 349	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1735: pop
    //   1736: ldc_w 358
    //   1739: astore 19
    //   1741: aload 18
    //   1743: aload 19
    //   1745: invokevirtual 354	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1748: pop
    //   1749: aload 18
    //   1751: invokevirtual 362	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1754: astore 18
    //   1756: iconst_0
    //   1757: istore 26
    //   1759: aconst_null
    //   1760: astore 19
    //   1762: aload 13
    //   1764: iconst_0
    //   1765: aload 18
    //   1767: aastore
    //   1768: ldc2_w 381
    //   1771: lstore 56
    //   1773: aload 24
    //   1775: ifnull +10 -> 1785
    //   1778: aload 24
    //   1780: invokeinterface 385 1 0
    //   1785: lload 56
    //   1787: lreturn
    //   1788: astore 13
    //   1790: goto +57 -> 1847
    //   1793: astore 13
    //   1795: aload 24
    //   1797: astore 17
    //   1799: goto +29 -> 1828
    //   1802: aload 24
    //   1804: ifnull +41 -> 1845
    //   1807: aload 24
    //   1809: invokeinterface 385 1 0
    //   1814: goto +31 -> 1845
    //   1817: astore 13
    //   1819: aload 17
    //   1821: astore 24
    //   1823: goto +24 -> 1847
    //   1826: astore 13
    //   1828: aload 13
    //   1830: invokestatic 391	com/truecaller/log/AssertionUtil:reportThrowableButNeverCrash	(Ljava/lang/Throwable;)V
    //   1833: aload 17
    //   1835: ifnull +10 -> 1845
    //   1838: aload 17
    //   1840: invokeinterface 385 1 0
    //   1845: lconst_0
    //   1846: lreturn
    //   1847: aload 24
    //   1849: ifnull +10 -> 1859
    //   1852: aload 24
    //   1854: invokeinterface 385 1 0
    //   1859: aload 13
    //   1861: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1862	0	this	c
    //   0	1862	1	paramf	f
    //   0	1862	2	parami	i
    //   0	1862	3	paramr	r
    //   0	1862	4	paramb1	org.a.a.b
    //   0	1862	5	paramb2	org.a.a.b
    //   0	1862	6	paramInt	int
    //   0	1862	7	paramList	List
    //   0	1862	8	paramq	com.truecaller.utils.q
    //   0	1862	9	paramBoolean1	boolean
    //   0	1862	10	paramBoolean2	boolean
    //   0	1862	11	paramSet	Set
    //   1	1088	12	localc	c
    //   4	1759	13	localObject1	Object
    //   1788	1	13	localObject2	Object
    //   1793	1	13	localRuntimeException1	RuntimeException
    //   1817	1	13	localObject3	Object
    //   1826	34	13	localRuntimeException2	RuntimeException
    //   8	1583	14	localList	List
    //   12	1640	15	localq	com.truecaller.utils.q
    //   15	1695	16	i	int
    //   18	1821	17	localObject4	Object
    //   24	1742	18	localObject5	Object
    //   31	1730	19	localObject6	Object
    //   37	1346	20	localObject7	Object
    //   40	1347	21	localObject8	Object
    //   44	1494	22	localObject9	Object
    //   48	722	23	j	int
    //   795	556	23	k	int
    //   66	1787	24	localObject10	Object
    //   79	463	25	m	int
    //   552	131	25	n	int
    //   709	3	25	bool1	boolean
    //   722	17	25	i1	int
    //   784	3	25	bool2	boolean
    //   816	74	25	i2	int
    //   905	7	25	i3	int
    //   920	143	25	bool3	boolean
    //   1073	18	25	i4	int
    //   1095	515	25	i5	int
    //   88	1670	26	bool4	boolean
    //   92	1331	27	i6	int
    //   96	1259	28	i7	int
    //   1359	304	28	i8	int
    //   102	1293	29	localObject11	Object
    //   105	1584	30	i9	int
    //   108	1623	31	i10	int
    //   126	318	32	l1	long
    //   135	1199	34	l2	long
    //   159	1415	36	i11	int
    //   167	597	37	l3	long
    //   225	168	39	l4	long
    //   430	1163	41	l5	long
    //   446	3	43	bool5	boolean
    //   560	5	44	l6	long
    //   587	94	46	i12	int
    //   596	89	47	l7	long
    //   741	12	49	l8	long
    //   754	96	51	l9	long
    //   765	59	53	l10	long
    //   828	162	55	bool6	boolean
    //   1157	3	55	i13	int
    //   1338	448	56	l11	long
    // Exception table:
    //   from	to	target	type
    //   73	79	1788	finally
    //   81	88	1788	finally
    //   120	126	1788	finally
    //   128	135	1788	finally
    //   149	155	1788	finally
    //   161	167	1788	finally
    //   171	176	1788	finally
    //   188	194	1788	finally
    //   210	216	1788	finally
    //   218	225	1788	finally
    //   239	246	1788	finally
    //   248	255	1788	finally
    //   297	302	1788	finally
    //   314	321	1788	finally
    //   345	351	1788	finally
    //   353	360	1788	finally
    //   374	380	1788	finally
    //   382	388	1788	finally
    //   392	397	1788	finally
    //   409	415	1788	finally
    //   424	430	1788	finally
    //   432	439	1788	finally
    //   453	460	1788	finally
    //   462	469	1788	finally
    //   497	502	1788	finally
    //   514	521	1788	finally
    //   534	539	1788	finally
    //   546	552	1788	finally
    //   554	560	1788	finally
    //   564	569	1788	finally
    //   581	587	1788	finally
    //   589	596	1788	finally
    //   598	605	1788	finally
    //   607	612	1788	finally
    //   616	621	1788	finally
    //   621	626	1788	finally
    //   654	659	1788	finally
    //   671	678	1788	finally
    //   691	698	1788	finally
    //   704	709	1788	finally
    //   716	722	1788	finally
    //   729	736	1788	finally
    //   779	784	1788	finally
    //   810	816	1788	finally
    //   835	842	1788	finally
    //   878	887	1788	finally
    //   891	896	1788	finally
    //   899	905	1788	finally
    //   914	920	1788	finally
    //   922	929	1788	finally
    //   938	944	1788	finally
    //   946	953	1788	finally
    //   962	968	1788	finally
    //   970	975	1788	finally
    //   977	982	1788	finally
    //   994	998	1788	finally
    //   1004	1010	1788	finally
    //   1010	1015	1788	finally
    //   1019	1027	1788	finally
    //   1039	1044	1788	finally
    //   1046	1051	1788	finally
    //   1053	1060	1788	finally
    //   1067	1073	1788	finally
    //   1080	1086	1788	finally
    //   1090	1095	1788	finally
    //   1102	1108	1788	finally
    //   1110	1115	1788	finally
    //   1119	1127	1788	finally
    //   1130	1136	1788	finally
    //   1138	1143	1788	finally
    //   1145	1150	1788	finally
    //   1159	1164	1788	finally
    //   1170	1176	1788	finally
    //   1176	1181	1788	finally
    //   1185	1193	1788	finally
    //   1205	1211	1788	finally
    //   1213	1220	1788	finally
    //   1227	1232	1788	finally
    //   1234	1237	1788	finally
    //   1246	1251	1788	finally
    //   1253	1259	1788	finally
    //   1266	1272	1788	finally
    //   1274	1280	1788	finally
    //   1287	1293	1788	finally
    //   1295	1301	1788	finally
    //   1308	1314	1788	finally
    //   1314	1319	1788	finally
    //   1330	1333	1788	finally
    //   1333	1338	1788	finally
    //   1366	1373	1788	finally
    //   1405	1410	1788	finally
    //   1427	1432	1788	finally
    //   1434	1437	1788	finally
    //   1446	1451	1788	finally
    //   1453	1459	1788	finally
    //   1466	1472	1788	finally
    //   1474	1480	1788	finally
    //   1487	1493	1788	finally
    //   1495	1501	1788	finally
    //   1508	1514	1788	finally
    //   1514	1519	1788	finally
    //   1530	1533	1788	finally
    //   1537	1542	1788	finally
    //   1544	1549	1788	finally
    //   1551	1556	1788	finally
    //   1561	1568	1788	finally
    //   1582	1588	1788	finally
    //   1592	1597	1788	finally
    //   1603	1609	1788	finally
    //   1623	1630	1788	finally
    //   1639	1646	1788	finally
    //   1655	1662	1788	finally
    //   1662	1667	1788	finally
    //   1669	1672	1788	finally
    //   1681	1686	1788	finally
    //   1688	1694	1788	finally
    //   1701	1707	1788	finally
    //   1709	1715	1788	finally
    //   1722	1728	1788	finally
    //   1730	1736	1788	finally
    //   1743	1749	1788	finally
    //   1749	1754	1788	finally
    //   1765	1768	1788	finally
    //   73	79	1793	java/lang/RuntimeException
    //   81	88	1793	java/lang/RuntimeException
    //   120	126	1793	java/lang/RuntimeException
    //   128	135	1793	java/lang/RuntimeException
    //   149	155	1793	java/lang/RuntimeException
    //   161	167	1793	java/lang/RuntimeException
    //   171	176	1793	java/lang/RuntimeException
    //   188	194	1793	java/lang/RuntimeException
    //   210	216	1793	java/lang/RuntimeException
    //   218	225	1793	java/lang/RuntimeException
    //   239	246	1793	java/lang/RuntimeException
    //   248	255	1793	java/lang/RuntimeException
    //   297	302	1793	java/lang/RuntimeException
    //   314	321	1793	java/lang/RuntimeException
    //   345	351	1793	java/lang/RuntimeException
    //   353	360	1793	java/lang/RuntimeException
    //   374	380	1793	java/lang/RuntimeException
    //   382	388	1793	java/lang/RuntimeException
    //   392	397	1793	java/lang/RuntimeException
    //   409	415	1793	java/lang/RuntimeException
    //   424	430	1793	java/lang/RuntimeException
    //   432	439	1793	java/lang/RuntimeException
    //   453	460	1793	java/lang/RuntimeException
    //   462	469	1793	java/lang/RuntimeException
    //   497	502	1793	java/lang/RuntimeException
    //   514	521	1793	java/lang/RuntimeException
    //   534	539	1793	java/lang/RuntimeException
    //   546	552	1793	java/lang/RuntimeException
    //   554	560	1793	java/lang/RuntimeException
    //   564	569	1793	java/lang/RuntimeException
    //   581	587	1793	java/lang/RuntimeException
    //   589	596	1793	java/lang/RuntimeException
    //   598	605	1793	java/lang/RuntimeException
    //   607	612	1793	java/lang/RuntimeException
    //   616	621	1793	java/lang/RuntimeException
    //   621	626	1793	java/lang/RuntimeException
    //   654	659	1793	java/lang/RuntimeException
    //   671	678	1793	java/lang/RuntimeException
    //   691	698	1793	java/lang/RuntimeException
    //   704	709	1793	java/lang/RuntimeException
    //   716	722	1793	java/lang/RuntimeException
    //   729	736	1793	java/lang/RuntimeException
    //   779	784	1793	java/lang/RuntimeException
    //   810	816	1793	java/lang/RuntimeException
    //   835	842	1793	java/lang/RuntimeException
    //   878	887	1793	java/lang/RuntimeException
    //   891	896	1793	java/lang/RuntimeException
    //   899	905	1793	java/lang/RuntimeException
    //   914	920	1793	java/lang/RuntimeException
    //   922	929	1793	java/lang/RuntimeException
    //   938	944	1793	java/lang/RuntimeException
    //   946	953	1793	java/lang/RuntimeException
    //   962	968	1793	java/lang/RuntimeException
    //   970	975	1793	java/lang/RuntimeException
    //   977	982	1793	java/lang/RuntimeException
    //   994	998	1793	java/lang/RuntimeException
    //   1004	1010	1793	java/lang/RuntimeException
    //   1010	1015	1793	java/lang/RuntimeException
    //   1019	1027	1793	java/lang/RuntimeException
    //   1039	1044	1793	java/lang/RuntimeException
    //   1046	1051	1793	java/lang/RuntimeException
    //   1053	1060	1793	java/lang/RuntimeException
    //   1067	1073	1793	java/lang/RuntimeException
    //   1080	1086	1793	java/lang/RuntimeException
    //   1090	1095	1793	java/lang/RuntimeException
    //   1102	1108	1793	java/lang/RuntimeException
    //   1110	1115	1793	java/lang/RuntimeException
    //   1119	1127	1793	java/lang/RuntimeException
    //   1130	1136	1793	java/lang/RuntimeException
    //   1138	1143	1793	java/lang/RuntimeException
    //   1145	1150	1793	java/lang/RuntimeException
    //   1159	1164	1793	java/lang/RuntimeException
    //   1170	1176	1793	java/lang/RuntimeException
    //   1176	1181	1793	java/lang/RuntimeException
    //   1185	1193	1793	java/lang/RuntimeException
    //   1205	1211	1793	java/lang/RuntimeException
    //   1213	1220	1793	java/lang/RuntimeException
    //   1227	1232	1793	java/lang/RuntimeException
    //   1234	1237	1793	java/lang/RuntimeException
    //   1246	1251	1793	java/lang/RuntimeException
    //   1253	1259	1793	java/lang/RuntimeException
    //   1266	1272	1793	java/lang/RuntimeException
    //   1274	1280	1793	java/lang/RuntimeException
    //   1287	1293	1793	java/lang/RuntimeException
    //   1295	1301	1793	java/lang/RuntimeException
    //   1308	1314	1793	java/lang/RuntimeException
    //   1314	1319	1793	java/lang/RuntimeException
    //   1330	1333	1793	java/lang/RuntimeException
    //   1333	1338	1793	java/lang/RuntimeException
    //   1366	1373	1793	java/lang/RuntimeException
    //   1405	1410	1793	java/lang/RuntimeException
    //   1427	1432	1793	java/lang/RuntimeException
    //   1434	1437	1793	java/lang/RuntimeException
    //   1446	1451	1793	java/lang/RuntimeException
    //   1453	1459	1793	java/lang/RuntimeException
    //   1466	1472	1793	java/lang/RuntimeException
    //   1474	1480	1793	java/lang/RuntimeException
    //   1487	1493	1793	java/lang/RuntimeException
    //   1495	1501	1793	java/lang/RuntimeException
    //   1508	1514	1793	java/lang/RuntimeException
    //   1514	1519	1793	java/lang/RuntimeException
    //   1530	1533	1793	java/lang/RuntimeException
    //   1537	1542	1793	java/lang/RuntimeException
    //   1544	1549	1793	java/lang/RuntimeException
    //   1551	1556	1793	java/lang/RuntimeException
    //   1561	1568	1793	java/lang/RuntimeException
    //   1582	1588	1793	java/lang/RuntimeException
    //   1592	1597	1793	java/lang/RuntimeException
    //   1603	1609	1793	java/lang/RuntimeException
    //   1623	1630	1793	java/lang/RuntimeException
    //   1639	1646	1793	java/lang/RuntimeException
    //   1655	1662	1793	java/lang/RuntimeException
    //   1662	1667	1793	java/lang/RuntimeException
    //   1669	1672	1793	java/lang/RuntimeException
    //   1681	1686	1793	java/lang/RuntimeException
    //   1688	1694	1793	java/lang/RuntimeException
    //   1701	1707	1793	java/lang/RuntimeException
    //   1709	1715	1793	java/lang/RuntimeException
    //   1722	1728	1793	java/lang/RuntimeException
    //   1730	1736	1793	java/lang/RuntimeException
    //   1743	1749	1793	java/lang/RuntimeException
    //   1749	1754	1793	java/lang/RuntimeException
    //   1765	1768	1793	java/lang/RuntimeException
    //   20	24	1817	finally
    //   26	31	1817	finally
    //   61	66	1817	finally
    //   1828	1833	1817	finally
    //   20	24	1826	java/lang/RuntimeException
    //   26	31	1826	java/lang/RuntimeException
    //   61	66	1826	java/lang/RuntimeException
  }
  
  protected abstract c.a a(ContentResolver paramContentResolver, f paramf, i parami, org.a.a.b paramb1, org.a.a.b paramb2, boolean paramBoolean1, boolean paramBoolean2);
  
  protected abstract Set a(long paramLong, f paramf, i parami, Participant paramParticipant, boolean paramBoolean);
  
  protected abstract void a(f paramf, List paramList, TransportInfo paramTransportInfo, int paramInt);
  
  protected abstract boolean a(int paramInt);
  
  protected abstract boolean a(r paramr, c.a parama);
  
  protected abstract boolean a(f paramf, i parami, List paramList, r paramr, c.a parama, boolean paramBoolean);
  
  protected abstract boolean b(r paramr, c.a parama);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.transport.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */