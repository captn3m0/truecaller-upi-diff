package com.truecaller.messaging.conversationlist;

import dagger.a.d;
import javax.inject.Provider;

public final class j
  implements d
{
  private final i a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  private final Provider e;
  private final Provider f;
  private final Provider g;
  private final Provider h;
  private final Provider i;
  private final Provider j;
  private final Provider k;
  private final Provider l;
  private final Provider m;
  private final Provider n;
  
  private j(i parami, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7, Provider paramProvider8, Provider paramProvider9, Provider paramProvider10, Provider paramProvider11, Provider paramProvider12, Provider paramProvider13)
  {
    a = parami;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
    e = paramProvider4;
    f = paramProvider5;
    g = paramProvider6;
    h = paramProvider7;
    i = paramProvider8;
    j = paramProvider9;
    k = paramProvider10;
    l = paramProvider11;
    m = paramProvider12;
    n = paramProvider13;
  }
  
  public static j a(i parami, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4, Provider paramProvider5, Provider paramProvider6, Provider paramProvider7, Provider paramProvider8, Provider paramProvider9, Provider paramProvider10, Provider paramProvider11, Provider paramProvider12, Provider paramProvider13)
  {
    j localj = new com/truecaller/messaging/conversationlist/j;
    localj.<init>(parami, paramProvider1, paramProvider2, paramProvider3, paramProvider4, paramProvider5, paramProvider6, paramProvider7, paramProvider8, paramProvider9, paramProvider10, paramProvider11, paramProvider12, paramProvider13);
    return localj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversationlist.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */