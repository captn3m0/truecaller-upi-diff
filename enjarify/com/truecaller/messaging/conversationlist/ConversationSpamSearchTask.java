package com.truecaller.messaging.conversationlist;

import android.content.Context;
import android.os.Bundle;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.background.PersistentBackgroundTask;
import com.truecaller.common.background.PersistentBackgroundTask.RunResult;
import com.truecaller.common.background.e;
import com.truecaller.common.background.e.a;
import dagger.a.g;
import java.util.concurrent.TimeUnit;

public class ConversationSpamSearchTask
  extends PersistentBackgroundTask
{
  a a;
  
  public ConversationSpamSearchTask()
  {
    Object localObject1 = new com/truecaller/messaging/conversationlist/d$a;
    ((d.a)localObject1).<init>((byte)0);
    Object localObject2 = (bp)g.a(TrueApp.y().a());
    a = ((bp)localObject2);
    g.a(a, bp.class);
    localObject2 = new com/truecaller/messaging/conversationlist/d;
    localObject1 = a;
    ((d)localObject2).<init>((bp)localObject1, (byte)0);
    ((q)localObject2).a(this);
  }
  
  public final int a()
  {
    return 10004;
  }
  
  public final PersistentBackgroundTask.RunResult a(Context paramContext, Bundle paramBundle)
  {
    paramContext = a;
    boolean bool = paramContext.b();
    if (bool) {
      return PersistentBackgroundTask.RunResult.Success;
    }
    return PersistentBackgroundTask.RunResult.FailedSkip;
  }
  
  public final boolean a(Context paramContext)
  {
    return a.a();
  }
  
  public final e b()
  {
    e.a locala = new com/truecaller/common/background/e$a;
    locala.<init>(0);
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    return locala.a(localTimeUnit).b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversationlist.ConversationSpamSearchTask
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */