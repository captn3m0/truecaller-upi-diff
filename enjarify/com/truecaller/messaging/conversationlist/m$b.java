package com.truecaller.messaging.conversationlist;

import c.g.a.b;
import c.g.b.j;
import c.g.b.w;
import c.l.c;

final class m$b
  extends j
  implements b
{
  m$b(m paramm)
  {
    super(1, paramm);
  }
  
  public final c a()
  {
    return w.a(m.class);
  }
  
  public final String b()
  {
    return "onConversationsMarkedRead";
  }
  
  public final String c()
  {
    return "onConversationsMarkedRead(Landroid/util/SparseBooleanArray;)V";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversationlist.m.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */