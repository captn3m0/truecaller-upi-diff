package com.truecaller.messaging.conversationlist;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.f;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.f;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import c.u;
import com.truecaller.analytics.az;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.ui.b.a;
import com.truecaller.messaging.defaultsms.DefaultSmsActivity;
import com.truecaller.messaging.newconversation.NewConversationActivity;
import com.truecaller.ui.SettingsFragment;
import com.truecaller.ui.SettingsFragment.SettingsViewType;
import com.truecaller.ui.ab;
import com.truecaller.ui.ae;
import com.truecaller.ui.components.FloatingActionButton.a;
import com.truecaller.ui.components.FloatingActionButton.c;
import com.truecaller.utils.extensions.t;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class h
  extends Fragment
  implements ViewPager.f, az, f, o, p, ab, ae, FloatingActionButton.c, com.truecaller.ui.p
{
  public l a;
  public com.truecaller.tcpermissions.l b;
  private final List d;
  private TabLayout e;
  private ViewPager f;
  private h.a g;
  private boolean h;
  private final boolean i;
  private HashMap j;
  
  public h()
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    localObject = (List)localObject;
    d = ((List)localObject);
    i = true;
  }
  
  public final boolean W_()
  {
    l locall = a;
    if (locall == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    return locall.g();
  }
  
  public final List a()
  {
    return d;
  }
  
  public final void a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject2 = "viewPager";
      c.g.b.k.a((String)localObject2);
    }
    Object localObject2 = g;
    if (localObject2 == null)
    {
      localObject2 = "adapter";
      c.g.b.k.a((String)localObject2);
    }
    switch (paramInt)
    {
    default: 
      localObject1 = new java/lang/IllegalStateException;
      str = String.valueOf(paramInt);
      str = "Unknown filter ".concat(str);
      ((IllegalStateException)localObject1).<init>(str);
      throw ((Throwable)localObject1);
    case 4: 
      paramInt = 0;
      str = null;
      break;
    case 3: 
      paramInt = 2;
      break;
    case 2: 
      paramInt = 1;
      ((ViewPager)localObject1).setCurrentItem(paramInt);
      return;
    }
    localObject1 = new java/lang/IllegalStateException;
    String str = String.valueOf(paramInt);
    str = "Invalid filter ".concat(str);
    ((IllegalStateException)localObject1).<init>(str);
    throw ((Throwable)localObject1);
  }
  
  public final void a(int paramInt, String paramString)
  {
    c.g.b.k.b(paramString, "tabTitle");
    Object localObject1 = e;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject2 = "tabsLayout";
      c.g.b.k.a((String)localObject2);
    }
    Object localObject3 = ((TabLayout)localObject1).a(paramInt);
    if (localObject3 != null)
    {
      localObject1 = LayoutInflater.from(getContext());
      int k = 0;
      Object localObject4 = null;
      localObject1 = ((LayoutInflater)localObject1).inflate(2131559211, null);
      int m = 2131364888;
      localObject2 = ((View)localObject1).findViewById(m);
      if (localObject2 != null)
      {
        localObject2 = (TextView)localObject2;
        k = 2131365399;
        localObject4 = ((View)localObject1).findViewById(k);
        if (localObject4 != null)
        {
          localObject4 = (TextView)localObject4;
          paramString = (CharSequence)paramString;
          ((TextView)localObject2).setText(paramString);
          ((TextView)localObject4).setVisibility(8);
          c.g.b.k.a(localObject1, "view");
          ((TabLayout.f)localObject3).a((View)localObject1);
          return;
        }
        localObject3 = new c/u;
        ((u)localObject3).<init>("null cannot be cast to non-null type android.widget.TextView");
        throw ((Throwable)localObject3);
      }
      localObject3 = new c/u;
      ((u)localObject3).<init>("null cannot be cast to non-null type android.widget.TextView");
      throw ((Throwable)localObject3);
    }
  }
  
  public final void a(e parame)
  {
    c.g.b.k.b(parame, "holder");
    c.g.b.k.b(parame, "holder");
    a().add(parame);
  }
  
  public final void a(String paramString)
  {
    l locall = a;
    if (locall == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locall.a(paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject1 = a;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      c.g.b.k.a((String)localObject2);
    }
    ((l)localObject1).c();
    localObject1 = ((Iterable)a()).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (e)((Iterator)localObject1).next();
      ((e)localObject2).c();
    }
    if (paramBoolean) {
      f.a.a(this);
    }
  }
  
  public final void b()
  {
    l locall = a;
    if (locall == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locall.o();
  }
  
  public final void b(int paramInt)
  {
    Object localObject1 = e;
    if (localObject1 == null)
    {
      String str = "tabsLayout";
      c.g.b.k.a(str);
    }
    Object localObject2 = ((TabLayout)localObject1).a(paramInt);
    if (localObject2 != null)
    {
      localObject2 = ((TabLayout.f)localObject2).a();
      if (localObject2 != null)
      {
        localObject1 = "tabsLayout.getTabAt(posi…on)?.customView ?: return";
        c.g.b.k.a(localObject2, (String)localObject1);
        int k = 2131365399;
        localObject2 = ((View)localObject2).findViewById(k);
        if (localObject2 != null)
        {
          ((TextView)localObject2).setVisibility(8);
          return;
        }
        localObject2 = new c/u;
        ((u)localObject2).<init>("null cannot be cast to non-null type android.widget.TextView");
        throw ((Throwable)localObject2);
      }
    }
  }
  
  public final void b(int paramInt, String paramString)
  {
    c.g.b.k.b(paramString, "unreadCount");
    Object localObject1 = e;
    if (localObject1 == null)
    {
      String str = "tabsLayout";
      c.g.b.k.a(str);
    }
    Object localObject2 = ((TabLayout)localObject1).a(paramInt);
    if (localObject2 != null)
    {
      localObject2 = ((TabLayout.f)localObject2).a();
      if (localObject2 != null)
      {
        localObject1 = "tabsLayout.getTabAt(posi…on)?.customView ?: return";
        c.g.b.k.a(localObject2, (String)localObject1);
        int k = 2131365399;
        localObject2 = ((View)localObject2).findViewById(k);
        if (localObject2 != null)
        {
          localObject2 = (TextView)localObject2;
          ((TextView)localObject2).setVisibility(0);
          paramString = (CharSequence)paramString;
          ((TextView)localObject2).setText(paramString);
          return;
        }
        localObject2 = new c/u;
        ((u)localObject2).<init>("null cannot be cast to non-null type android.widget.TextView");
        throw ((Throwable)localObject2);
      }
    }
  }
  
  public final void b(e parame)
  {
    c.g.b.k.b(parame, "holder");
    c.g.b.k.b(parame, "holder");
    a().remove(parame);
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "text");
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
      localBuilder.<init>((Context)localObject1);
      localObject1 = paramString;
      localObject1 = (CharSequence)paramString;
      localObject1 = localBuilder.setMessage((CharSequence)localObject1).setCancelable(false);
      Object localObject2 = new com/truecaller/messaging/conversationlist/h$c;
      ((h.c)localObject2).<init>(this, paramString);
      localObject2 = (DialogInterface.OnClickListener)localObject2;
      ((AlertDialog.Builder)localObject1).setPositiveButton(2131886506, (DialogInterface.OnClickListener)localObject2).setNegativeButton(2131887636, null).create().show();
      return;
    }
  }
  
  public final void b(boolean paramBoolean)
  {
    h = paramBoolean;
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      localf.invalidateOptionsMenu();
      return;
    }
  }
  
  public final void c()
  {
    l locall = a;
    if (locall == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locall.p();
  }
  
  public final void c(boolean paramBoolean)
  {
    Object localObject1 = e;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject2 = "tabsLayout";
      c.g.b.k.a((String)localObject2);
    }
    int k = 2;
    localObject1 = ((TabLayout)localObject1).a(k);
    if (localObject1 != null)
    {
      localObject1 = ((TabLayout.f)localObject1).a();
      if (localObject1 != null)
      {
        c.g.b.k.a(localObject1, "tabsLayout.getTabAt(posi…on)?.customView ?: return");
        k = 2131365400;
        localObject2 = ((View)localObject1).findViewById(k);
        if (localObject2 != null)
        {
          t.a((View)localObject2, paramBoolean);
          if (paramBoolean)
          {
            paramBoolean = 2131365399;
            localObject3 = ((View)localObject1).findViewById(paramBoolean);
            if (localObject3 != null)
            {
              localObject3 = (View)localObject3;
              localObject1 = null;
              t.a((View)localObject3, false);
            }
            else
            {
              localObject3 = new c/u;
              ((u)localObject3).<init>("null cannot be cast to non-null type android.widget.TextView");
              throw ((Throwable)localObject3);
            }
          }
          return;
        }
        Object localObject3 = new c/u;
        ((u)localObject3).<init>("null cannot be cast to non-null type android.view.View");
        throw ((Throwable)localObject3);
      }
    }
  }
  
  public final void d()
  {
    Iterator localIterator = ((Iterable)d).iterator();
    int k;
    do
    {
      bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = localIterator.next();
      Object localObject2 = localObject1;
      localObject2 = (e)localObject1;
      k = ((e)localObject2).a();
      int m = 4;
      if (k == m)
      {
        k = 1;
      }
      else
      {
        k = 0;
        localObject2 = null;
      }
    } while (k == 0);
    break label83;
    boolean bool = false;
    Object localObject1 = null;
    label83:
    localObject1 = (e)localObject1;
    if (localObject1 != null)
    {
      ((e)localObject1).e();
      return;
    }
  }
  
  public final l e()
  {
    l locall = a;
    if (locall == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    return locall;
  }
  
  public final int f()
  {
    l locall = a;
    if (locall == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    return locall.q();
  }
  
  public final boolean g()
  {
    return i;
  }
  
  public final void h()
  {
    Object localObject = getContext();
    if (localObject != null)
    {
      localObject = DefaultSmsActivity.a((Context)localObject, "inbox", false);
      startActivityForResult((Intent)localObject, 10);
      return;
    }
  }
  
  public final int i()
  {
    return 2131234299;
  }
  
  public final com.truecaller.ui.components.i[] j()
  {
    return null;
  }
  
  public final FloatingActionButton.a k()
  {
    h.b localb = new com/truecaller/messaging/conversationlist/h$b;
    localb.<init>(this);
    return (FloatingActionButton.a)localb;
  }
  
  public final boolean l()
  {
    Object localObject = a;
    if (localObject == null)
    {
      localObject = "presenter";
      c.g.b.k.a((String)localObject);
    }
    return true;
  }
  
  public final void m()
  {
    l locall = a;
    if (locall == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locall.e();
  }
  
  public final void n()
  {
    Object localObject1 = a;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      c.g.b.k.a((String)localObject2);
    }
    ((l)localObject1).f();
    localObject1 = ((Iterable)a()).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (e)((Iterator)localObject1).next();
      ((e)localObject2).d();
    }
  }
  
  public final void o()
  {
    Object localObject1 = this;
    localObject1 = (Fragment)this;
    Object localObject2 = b;
    if (localObject2 == null)
    {
      String str = "tcPermissionUtil";
      c.g.b.k.a(str);
    }
    localObject2 = ((com.truecaller.tcpermissions.l)localObject2).d();
    com.truecaller.wizard.utils.i.a((Fragment)localObject1, (String[])localObject2, 11);
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Object localObject1 = a;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      c.g.b.k.a((String)localObject2);
    }
    ((l)localObject1).a(paramInt1, paramInt2, paramIntent);
    localObject1 = ((Iterable)a()).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (e)((Iterator)localObject1).next();
      ((e)localObject2).a(paramInt1, paramInt2);
    }
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getContext();
    if (paramBundle != null)
    {
      c.a locala = c.a();
      c.g.b.k.a(paramBundle, "it");
      Object localObject = paramBundle.getApplicationContext();
      if (localObject != null)
      {
        localObject = ((bk)localObject).a();
        locala = locala.a((bp)localObject);
        localObject = new com/truecaller/messaging/conversationlist/i;
        ((i)localObject).<init>(paramBundle);
        paramBundle = locala.a((i)localObject).a();
        paramBundle.a(this);
      }
      else
      {
        paramBundle = new c/u;
        paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
        throw paramBundle;
      }
    }
    setHasOptionsMenu(true);
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
    if (paramMenuInflater != null)
    {
      int k = 2131623945;
      paramMenuInflater.inflate(k, paramMenu);
    }
    if (paramMenu != null)
    {
      int m = 2131361903;
      paramMenu = paramMenu.findItem(m);
      if (paramMenu != null)
      {
        boolean bool = h;
        paramMenu.setEnabled(bool);
        return;
      }
    }
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131559063, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    l locall = a;
    if (locall == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locall.y_();
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    int k;
    Object localObject;
    if (paramMenuItem != null)
    {
      k = paramMenuItem.getItemId();
      localObject = Integer.valueOf(k);
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int m;
    if (localObject != null)
    {
      m = ((Integer)localObject).intValue();
      int n = 2131361903;
      if (m == n)
      {
        paramMenuItem = a;
        if (paramMenuItem == null)
        {
          localObject = "presenter";
          c.g.b.k.a((String)localObject);
        }
        return paramMenuItem.l();
      }
    }
    if (localObject != null)
    {
      k = ((Integer)localObject).intValue();
      m = 2131361909;
      if (k == m)
      {
        paramMenuItem = a;
        if (paramMenuItem == null)
        {
          localObject = "presenter";
          c.g.b.k.a((String)localObject);
        }
        return paramMenuItem.m();
      }
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  public final void onPageScrollStateChanged(int paramInt) {}
  
  public final void onPageScrolled(int paramInt1, float paramFloat, int paramInt2) {}
  
  public final void onPageSelected(int paramInt)
  {
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    ((l)localObject).a(paramInt);
    l locall = a;
    if (locall == null)
    {
      localObject = "presenter";
      c.g.b.k.a((String)localObject);
    }
    locall.k();
    f.a.a(this);
  }
  
  public final void onPause()
  {
    super.onPause();
    l locall = a;
    if (locall == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locall.b();
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    c.g.b.k.b(paramArrayOfString, "permissions");
    c.g.b.k.b(paramArrayOfInt, "grantResults");
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    com.truecaller.wizard.utils.i.a(paramArrayOfString, paramArrayOfInt);
    l locall = a;
    if (locall == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locall.a(paramInt, paramArrayOfString, paramArrayOfInt);
  }
  
  public final void onResume()
  {
    super.onResume();
    l locall = a;
    if (locall == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locall.a();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject1 = "view";
    c.g.b.k.b(paramView, (String)localObject1);
    super.onViewCreated(paramView, paramBundle);
    paramBundle = paramView;
    paramBundle = (ViewPager)paramView;
    f = paramBundle;
    paramBundle = f;
    if (paramBundle == null)
    {
      localObject1 = "viewPager";
      c.g.b.k.a((String)localObject1);
    }
    int k = 2;
    paramBundle.setOffscreenPageLimit(k);
    paramBundle = new com/truecaller/messaging/conversationlist/h$a;
    localObject1 = getChildFragmentManager();
    c.g.b.k.a(localObject1, "childFragmentManager");
    Object localObject2 = getContext();
    l locall = a;
    if (locall == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    boolean bool = locall.r();
    paramBundle.<init>((j)localObject1, (Context)localObject2, bool);
    g = paramBundle;
    paramBundle = f;
    if (paramBundle == null)
    {
      localObject1 = "viewPager";
      c.g.b.k.a((String)localObject1);
    }
    localObject1 = g;
    if (localObject1 == null)
    {
      localObject2 = "adapter";
      c.g.b.k.a((String)localObject2);
    }
    localObject1 = (android.support.v4.view.o)localObject1;
    paramBundle.setAdapter((android.support.v4.view.o)localObject1);
    int m = 2131364647;
    paramView = paramView.findViewById(m);
    paramBundle = "view.findViewById(R.id.tabs_layout)";
    c.g.b.k.a(paramView, paramBundle);
    paramView = (TabLayout)paramView;
    e = paramView;
    paramView = e;
    if (paramView == null)
    {
      paramBundle = "tabsLayout";
      c.g.b.k.a(paramBundle);
    }
    paramBundle = f;
    if (paramBundle == null)
    {
      localObject1 = "viewPager";
      c.g.b.k.a((String)localObject1);
    }
    paramView.setupWithViewPager(paramBundle);
    paramView = f;
    if (paramView == null)
    {
      paramBundle = "viewPager";
      c.g.b.k.a(paramBundle);
    }
    paramBundle = this;
    paramBundle = (ViewPager.f)this;
    paramView.a(paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      c.g.b.k.a(paramBundle);
    }
    paramView.a(this);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      c.g.b.k.a(paramBundle);
    }
    paramView.n();
  }
  
  public final void p()
  {
    Context localContext = getContext();
    if (localContext != null)
    {
      SettingsFragment.SettingsViewType localSettingsViewType = SettingsFragment.SettingsViewType.SETTINGS_MESSAGING;
      SettingsFragment.c(localContext, localSettingsViewType);
      return;
    }
  }
  
  public final void q()
  {
    boolean bool = isAdded();
    if (bool)
    {
      int k = 2131886652;
      com.truecaller.ui.dialogs.k localk = com.truecaller.ui.dialogs.k.c(k);
      c.g.b.k.a(localk, "ProgressDialogFragment.newInstance(stringId)");
      j localj = getChildFragmentManager();
      String str = "progress_dialog_tag";
      localk.show(localj, str);
    }
  }
  
  public final void r()
  {
    boolean bool1 = isAdded();
    if (bool1)
    {
      Object localObject = getChildFragmentManager();
      String str = "progress_dialog_tag";
      localObject = ((j)localObject).a(str);
      boolean bool2 = localObject instanceof android.support.v4.app.e;
      if (bool2)
      {
        localObject = (android.support.v4.app.e)localObject;
        ((android.support.v4.app.e)localObject).dismissAllowingStateLoss();
      }
    }
  }
  
  public final void s()
  {
    Intent localIntent = new android/content/Intent;
    Context localContext = (Context)getActivity();
    localIntent.<init>(localContext, NewConversationActivity.class);
    startActivity(localIntent);
  }
  
  public final void t()
  {
    l locall = a;
    if (locall == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locall.h();
  }
  
  public final void u()
  {
    l locall = a;
    if (locall == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locall.i();
  }
  
  public final void v()
  {
    Object localObject = (Activity)getActivity();
    boolean bool = localObject instanceof b.a;
    if (bool)
    {
      localObject = (b.a)localObject;
      ((b.a)localObject).ag_();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversationlist.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */