package com.truecaller.messaging.conversationlist;

import com.truecaller.bp;

public final class c$a
{
  private i a;
  private bp b;
  
  public final a a(bp parambp)
  {
    parambp = (bp)dagger.a.g.a(parambp);
    b = parambp;
    return this;
  }
  
  public final a a(i parami)
  {
    parami = (i)dagger.a.g.a(parami);
    a = parami;
    return this;
  }
  
  public final g a()
  {
    dagger.a.g.a(a, i.class);
    dagger.a.g.a(b, bp.class);
    c localc = new com/truecaller/messaging/conversationlist/c;
    i locali = a;
    bp localbp = b;
    localc.<init>(locali, localbp, (byte)0);
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversationlist.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */