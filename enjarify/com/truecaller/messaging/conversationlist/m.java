package com.truecaller.messaging.conversationlist;

import android.content.Intent;
import c.g.b.k;
import com.truecaller.analytics.bc;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.a;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.common.h.an;
import com.truecaller.messaging.data.ak;
import com.truecaller.messaging.data.al;
import com.truecaller.messaging.data.al.a;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.h;
import com.truecaller.search.local.model.c;
import com.truecaller.utils.d;
import java.util.Iterator;
import java.util.List;

public final class m
  extends l
  implements al.a
{
  private a a;
  private a c;
  private int d;
  private int e;
  private int f;
  private int g;
  private boolean h;
  private String[] i;
  private final i j;
  private final f k;
  private final f l;
  private final com.truecaller.tcpermissions.l m;
  private final d n;
  private final com.truecaller.i.e o;
  private final com.truecaller.analytics.b p;
  private final com.truecaller.utils.o q;
  private final al r;
  private final h s;
  private final an t;
  private final c u;
  private final boolean v;
  
  public m(i parami, f paramf1, f paramf2, com.truecaller.tcpermissions.l paraml, d paramd, com.truecaller.i.e parame, com.truecaller.analytics.b paramb, com.truecaller.utils.o paramo, al paramal, h paramh, an paraman, c paramc, boolean paramBoolean)
  {
    j = parami;
    k = paramf1;
    l = paramf2;
    m = paraml;
    n = paramd;
    o = parame;
    p = paramb;
    q = paramo;
    r = paramal;
    s = paramh;
    t = paraman;
    u = paramc;
    v = paramBoolean;
    d = 4;
    parami = new String[3];
    paramf1 = q;
    paraml = new Object[0];
    paramf1 = paramf1.a(2131887254, paraml);
    parami[0] = paramf1;
    paramf1 = q;
    paraml = new Object[0];
    paramf1 = paramf1.a(2131887256, paraml);
    parami[1] = paramf1;
    paramf1 = q;
    paramf2 = new Object[0];
    paramf1 = paramf1.a(2131887257, paramf2);
    parami[2] = paramf1;
    i = parami;
  }
  
  private final void a(int paramInt1, int paramInt2)
  {
    o localo = (o)b;
    if (localo == null) {
      return;
    }
    if (paramInt1 != 0)
    {
      int i1 = 99;
      Object localObject;
      if (paramInt1 < i1)
      {
        localObject = String.valueOf(paramInt1);
      }
      else
      {
        localObject = q;
        i1 = 2131888357;
        Object[] arrayOfObject = new Object[0];
        localObject = ((com.truecaller.utils.o)localObject).a(i1, arrayOfObject);
        String str = "resourceProvider.getStri…tring.message_max_unread)";
        k.a(localObject, str);
      }
      localo.b(paramInt2, (String)localObject);
      return;
    }
    localo.b(paramInt2);
  }
  
  private final void a(long paramLong)
  {
    o localo = (o)b;
    if (localo != null)
    {
      int i1 = g;
      if (i1 > 0)
      {
        h localh = s;
        long l1 = localh.S();
        boolean bool1 = l1 < paramLong;
        if (bool1)
        {
          bool2 = true;
          break label58;
        }
      }
      boolean bool2 = false;
      label58:
      localo.c(bool2);
      return;
    }
  }
  
  private final void a(String paramString1, String paramString2)
  {
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("PermissionChanged");
    paramString1 = locala.a("Permission", paramString1).a("State", paramString2).a("Context", "inbox").a();
    paramString2 = p;
    k.a(paramString1, "event");
    paramString2.a(paramString1);
  }
  
  public final void a()
  {
    al localal = r;
    Object localObject = this;
    localObject = (al.a)this;
    localal.a((al.a)localObject);
  }
  
  public final void a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      h localh = s;
      an localan = t;
      long l1 = localan.a();
      localh.g(l1);
      l1 = 0L;
      a(l1);
      paramInt = 3;
      break;
    case 1: 
      paramInt = 2;
      break;
    case 0: 
      paramInt = 4;
    }
    d = paramInt;
  }
  
  public final void a(int paramInt1, int paramInt2, Intent paramIntent)
  {
    int i1 = -1;
    if (paramInt2 == i1) {
      switch (paramInt1)
      {
      default: 
        break;
      case 1: 
      case 2: 
        if (paramIntent != null)
        {
          Object localObject1 = "RESULT_NUMBER_BLOCKED";
          paramInt2 = 0;
          paramInt1 = paramIntent.getBooleanExtra((String)localObject1, false);
          Object localObject2 = "CONVERSATION_ID";
          long l1 = -1;
          long l2 = paramIntent.getLongExtra((String)localObject2, l1);
          boolean bool = l2 < l1;
          if ((bool) && (paramInt1 == 0))
          {
            localObject1 = c;
            if (localObject1 != null) {
              ((a)localObject1).a();
            }
            localObject1 = ((com.truecaller.messaging.data.o)l.a()).a(l2);
            localObject2 = j;
            paramIntent = new com/truecaller/messaging/conversationlist/m$a;
            paramIntent.<init>(this);
            paramIntent = (ac)paramIntent;
            localObject1 = ((w)localObject1).a((i)localObject2, paramIntent);
            c = ((a)localObject1);
            return;
          }
          localObject1 = (o)b;
          if (localObject1 != null)
          {
            ((o)localObject1).a(3);
            return;
          }
        }
        break;
      }
    }
  }
  
  public final void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    String str1 = "grantResults";
    k.b(paramArrayOfInt, str1);
    int i1 = 11;
    if (paramInt == i1)
    {
      paramInt = paramArrayOfString.length;
      i1 = 0;
      str1 = null;
      int i2 = 0;
      String str2 = null;
      while (i1 < paramInt)
      {
        String str3 = paramArrayOfString[i1];
        int i3 = i2 + 1;
        String str4 = "android.permission.READ_SMS";
        boolean bool = k.a(str3, str4);
        if (bool)
        {
          i2 = paramArrayOfInt[i2];
          if (i2 == 0)
          {
            str2 = "SMSRead";
            str3 = "Enabled";
            a(str2, str3);
          }
        }
        i1 += 1;
        i2 = i3;
      }
    }
  }
  
  public final void a(ak paramak)
  {
    Object localObject = "unreadThreadsCounter";
    k.b(paramak, (String)localObject);
    int i1 = a;
    e = i1;
    i1 = b;
    f = i1;
    i1 = c;
    g = i1;
    i1 = e;
    an localan = null;
    a(i1, 0);
    i1 = f;
    a(i1, 1);
    k();
    i1 = d;
    int i2 = 3;
    if (i1 == i2)
    {
      localObject = s;
      localan = t;
      long l1 = localan.a();
      ((h)localObject).g(l1);
    }
    long l2 = d;
    a(l2);
  }
  
  public final void a(String paramString)
  {
    com.truecaller.analytics.b localb = p;
    Object localObject = new com/truecaller/analytics/bc;
    ((bc)localObject).<init>("inbox", paramString);
    localObject = (com.truecaller.analytics.e)localObject;
    localb.a((com.truecaller.analytics.e)localObject);
  }
  
  public final void b()
  {
    al localal = r;
    Object localObject = this;
    localObject = (al.a)this;
    localal.b((al.a)localObject);
  }
  
  public final void c()
  {
    boolean bool = v;
    if (bool)
    {
      c localc = u;
      localc.c();
    }
  }
  
  public final void e()
  {
    o localo = (o)b;
    if (localo != null)
    {
      localo.a(4);
      return;
    }
  }
  
  public final void f()
  {
    Object localObject = o;
    String str = "notDefaultSmsBadgeShown";
    boolean bool1 = true;
    ((com.truecaller.i.e)localObject).b(str, bool1);
    boolean bool2 = v;
    if (bool2)
    {
      localObject = u;
      ((c)localObject).b();
    }
  }
  
  public final boolean g()
  {
    int i1 = d;
    int i2 = 3;
    if (i1 != i2)
    {
      i2 = 2;
      if (i1 != i2) {
        return false;
      }
    }
    o localo = (o)b;
    if (localo != null)
    {
      i2 = 4;
      localo.a(i2);
    }
    return true;
  }
  
  public final void h()
  {
    o localo = (o)b;
    if (localo != null)
    {
      Object localObject = n;
      boolean bool = ((d)localObject).d();
      if (bool)
      {
        localObject = m;
        bool = ((com.truecaller.tcpermissions.l)localObject).g();
        if (bool)
        {
          i();
          return;
        }
      }
      localo.h();
      return;
    }
  }
  
  public final void i()
  {
    o localo = (o)b;
    if (localo != null) {
      localo.o();
    }
    a("SMSRead", "Asked");
  }
  
  public final void j()
  {
    o localo = (o)b;
    if (localo != null)
    {
      localo.s();
      return;
    }
  }
  
  public final void k()
  {
    o localo = (o)b;
    if (localo == null) {
      return;
    }
    int i1 = d;
    boolean bool = true;
    switch (i1)
    {
    default: 
      break;
    case 4: 
      i1 = e;
      if (i1 <= 0) {
        bool = false;
      }
      localo.b(bool);
      return;
    case 3: 
      i1 = g;
      if (i1 <= 0) {
        bool = false;
      }
      localo.b(bool);
      break;
    case 2: 
      i1 = f;
      if (i1 <= 0) {
        bool = false;
      }
      localo.b(bool);
      return;
    }
  }
  
  public final boolean l()
  {
    Object localObject1 = (o)b;
    if (localObject1 != null) {
      ((o)localObject1).q();
    }
    localObject1 = a;
    if (localObject1 != null) {
      ((a)localObject1).a();
    }
    localObject1 = (t)k.a();
    int i1 = d;
    localObject1 = ((t)localObject1).a(i1);
    i locali = j;
    Object localObject2 = new com/truecaller/messaging/conversationlist/m$b;
    Object localObject3 = this;
    localObject3 = (m)this;
    ((m.b)localObject2).<init>((m)localObject3);
    localObject2 = (c.g.a.b)localObject2;
    localObject3 = new com/truecaller/messaging/conversationlist/n;
    ((n)localObject3).<init>((c.g.a.b)localObject2);
    localObject3 = (ac)localObject3;
    localObject1 = ((w)localObject1).a(locali, (ac)localObject3);
    a = ((a)localObject1);
    return true;
  }
  
  public final boolean m()
  {
    o localo = (o)b;
    if (localo != null) {
      localo.p();
    }
    return true;
  }
  
  public final void n()
  {
    int i1 = 3;
    Object localObject1 = new Integer[i1];
    Number localNumber = null;
    Object localObject2 = Integer.valueOf(0);
    localObject1[0] = localObject2;
    int i2 = 1;
    localObject2 = Integer.valueOf(i2);
    localObject1[i2] = localObject2;
    i2 = 2;
    localObject2 = Integer.valueOf(i2);
    localObject1[i2] = localObject2;
    localObject1 = c.a.m.b((Object[])localObject1).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject1).hasNext();
      if (!bool) {
        break;
      }
      localNumber = (Number)((Iterator)localObject1).next();
      int i3 = localNumber.intValue();
      localObject2 = (o)b;
      if (localObject2 != null)
      {
        String str1 = i[i3];
        String str2 = "tabTitle[position]";
        k.a(str1, str2);
        ((o)localObject2).a(i3, str1);
      }
    }
  }
  
  public final void o()
  {
    boolean bool = true;
    h = bool;
    o localo = (o)b;
    if (localo != null)
    {
      localo.v();
      return;
    }
  }
  
  public final void p()
  {
    h = false;
    o localo = (o)b;
    if (localo != null)
    {
      localo.v();
      return;
    }
  }
  
  public final int q()
  {
    boolean bool = h;
    if (bool) {
      return 4;
    }
    return 0;
  }
  
  public final boolean r()
  {
    return v;
  }
  
  public final void y_()
  {
    super.y_();
    a locala1 = a;
    if (locala1 != null) {
      locala1.a();
    }
    locala1 = null;
    a = null;
    a locala2 = c;
    if (locala2 != null) {
      locala2.a();
    }
    c = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversationlist.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */