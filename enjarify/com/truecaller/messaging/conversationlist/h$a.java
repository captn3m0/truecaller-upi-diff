package com.truecaller.messaging.conversationlist;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.n;

public final class h$a
  extends n
{
  private final Integer[] a;
  private final Context b;
  private final boolean c;
  
  public h$a(android.support.v4.app.j paramj, Context paramContext, boolean paramBoolean)
  {
    super(paramj);
    b = paramContext;
    c = paramBoolean;
    paramj = new Integer[3];
    paramContext = Integer.valueOf(2131887254);
    paramj[0] = paramContext;
    paramContext = Integer.valueOf(2131887256);
    paramj[1] = paramContext;
    paramContext = Integer.valueOf(2131887257);
    paramj[2] = paramContext;
    a = paramj;
  }
  
  public final Fragment a(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      localObject = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject).<init>("Invalid position");
      throw ((Throwable)localObject);
    case 2: 
      paramInt = 3;
      break;
    case 1: 
      paramInt = 2;
      break;
    case 0: 
      paramInt = 4;
    }
    boolean bool = c;
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putInt("type_filter", paramInt);
    localBundle.putBoolean("is_tc_x_enabled", bool);
    Object localObject = new com/truecaller/messaging/b/j;
    ((com.truecaller.messaging.b.j)localObject).<init>();
    ((com.truecaller.messaging.b.j)localObject).setArguments(localBundle);
    return (Fragment)localObject;
  }
  
  public final int getCount()
  {
    return a.length;
  }
  
  public final CharSequence getPageTitle(int paramInt)
  {
    Context localContext = b;
    String str;
    if (localContext != null)
    {
      Integer[] arrayOfInteger = a;
      paramInt = arrayOfInteger[paramInt].intValue();
      str = localContext.getString(paramInt);
    }
    else
    {
      paramInt = 0;
      str = null;
    }
    return (CharSequence)str;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversationlist.h.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */