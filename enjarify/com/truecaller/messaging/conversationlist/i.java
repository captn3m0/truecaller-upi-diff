package com.truecaller.messaging.conversationlist;

import android.content.Context;
import com.truecaller.androidactors.f;
import com.truecaller.common.h.an;
import com.truecaller.messaging.data.al;
import com.truecaller.messaging.h;
import com.truecaller.search.local.model.c;
import com.truecaller.utils.d;
import com.truecaller.utils.o;

public final class i
{
  final Context a;
  
  public i(Context paramContext)
  {
    a = paramContext;
  }
  
  public static l a(com.truecaller.androidactors.k paramk, f paramf1, f paramf2, com.truecaller.tcpermissions.l paraml, d paramd, com.truecaller.i.e parame, com.truecaller.analytics.b paramb, o paramo, al paramal, h paramh, an paraman, c paramc, com.truecaller.featuretoggles.e parame1)
  {
    c.g.b.k.b(paramk, "actorsThreads");
    c.g.b.k.b(paramf1, "messageStorage");
    c.g.b.k.b(paramf2, "fetchMessageStorage");
    c.g.b.k.b(paraml, "permissionUtil");
    c.g.b.k.b(paramd, "deviceInfoUtil");
    c.g.b.k.b(parame, "generalSettings");
    c.g.b.k.b(paramb, "analytics");
    c.g.b.k.b(paramo, "resourceProvider");
    c.g.b.k.b(paramal, "unreadThreadsCounter");
    c.g.b.k.b(paramh, "settings");
    c.g.b.k.b(paraman, "timestampUtil");
    c.g.b.k.b(paramc, "availabilityManager");
    c.g.b.k.b(parame1, "featuresRegistry");
    m localm = new com/truecaller/messaging/conversationlist/m;
    com.truecaller.androidactors.i locali = paramk.a();
    c.g.b.k.a(locali, "actorsThreads.ui()");
    boolean bool = parame1.e().a();
    localm.<init>(locali, paramf1, paramf2, paraml, paramd, parame, paramb, paramo, paramal, paramh, paraman, paramc, bool);
    return (l)localm;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversationlist.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */