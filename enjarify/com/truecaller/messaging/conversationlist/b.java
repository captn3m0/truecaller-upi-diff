package com.truecaller.messaging.conversationlist;

import android.content.Intent;
import com.truecaller.analytics.ax;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.o;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.h;
import com.truecaller.network.search.d;
import com.truecaller.network.search.d.b;
import com.truecaller.network.search.l;
import com.truecaller.util.al;
import com.truecaller.utils.i;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

final class b
  implements a
{
  private final f a;
  private final al b;
  private final l c;
  private final i d;
  private final h e;
  private final ax f;
  
  b(f paramf, al paramal, l paraml, i parami, h paramh, ax paramax)
  {
    a = paramf;
    b = paramal;
    c = paraml;
    d = parami;
    e = paramh;
    f = paramax;
  }
  
  private boolean a(Set paramSet1, Set paramSet2)
  {
    Object localObject1 = d;
    boolean bool1 = ((i)localObject1).a();
    if (!bool1) {
      return false;
    }
    localObject1 = new java/util/ArrayList;
    int i = paramSet1.size();
    ((ArrayList)localObject1).<init>(i);
    Object localObject2 = paramSet1.iterator();
    Object localObject3;
    Object localObject4;
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject2).hasNext();
      if (!bool2) {
        break;
      }
      localObject3 = (String)((Iterator)localObject2).next();
      localObject4 = new com/truecaller/network/search/d$b;
      ((d.b)localObject4).<init>((String)localObject3, null, null);
      ((List)localObject1).add(localObject4);
    }
    try
    {
      localObject2 = c;
      localObject3 = UUID.randomUUID();
      localObject4 = "autoSpamInbox";
      localObject2 = ((l)localObject2).b((UUID)localObject3, (String)localObject4);
      int j = 20;
      e = j;
      localObject1 = ((d)localObject2).a((Collection)localObject1);
      i = 1;
      c = i;
      d = i;
      localObject1 = ((d)localObject1).b();
      if (localObject1 == null) {
        return false;
      }
      paramSet2.addAll(paramSet1);
      paramSet1.clear();
      return i;
    }
    catch (IOException localIOException) {}
    return false;
  }
  
  private boolean c()
  {
    Object localObject1 = d;
    boolean bool1 = ((i)localObject1).a();
    Object localObject2 = null;
    if (!bool1) {
      return false;
    }
    bool1 = false;
    localObject1 = null;
    try
    {
      localObject4 = a;
      localObject4 = ((f)localObject4).a();
      localObject4 = (o)localObject4;
      int i = 300;
      localObject5 = Integer.valueOf(i);
      localObject4 = ((o)localObject4).a((Integer)localObject5);
      localObject4 = ((w)localObject4).d();
      localObject4 = (com.truecaller.messaging.data.a.a)localObject4;
      localObject1 = localObject4;
    }
    catch (InterruptedException localInterruptedException)
    {
      Object localObject4;
      Object localObject5;
      for (;;) {}
    }
    if (localObject1 == null) {
      return false;
    }
    localObject4 = new java/util/HashSet;
    ((HashSet)localObject4).<init>();
    localObject5 = new java/util/HashSet;
    ((HashSet)localObject5).<init>();
    try
    {
      boolean bool2 = ((com.truecaller.messaging.data.a.a)localObject1).moveToNext();
      int j = 1;
      if (bool2)
      {
        Object localObject6 = ((com.truecaller.messaging.data.a.a)localObject1).b();
        localObject6 = l;
        int k = localObject6.length;
        int m = 0;
        while (m < k)
        {
          String str1 = localObject6[m];
          boolean bool3 = k;
          if (!bool3)
          {
            int n = c;
            if (n != 0)
            {
              n = c;
              if (n != j) {}
            }
            else
            {
              int i1 = o & 0x2;
              if (i1 == 0)
              {
                i1 = o & 0xD;
                if (i1 == 0)
                {
                  String str2 = f;
                  boolean bool4 = ((Set)localObject4).contains(str2);
                  if (!bool4)
                  {
                    str1 = f;
                    ((Set)localObject5).add(str1);
                    int i3 = ((Set)localObject5).size();
                    int i2 = 25;
                    if (i3 >= i2)
                    {
                      boolean bool5 = a((Set)localObject5, (Set)localObject4);
                      if (!bool5) {
                        return false;
                      }
                    }
                  }
                }
              }
            }
          }
          m += 1;
        }
      }
      ((com.truecaller.messaging.data.a.a)localObject1).close();
      bool1 = ((Set)localObject5).isEmpty();
      if (!bool1)
      {
        bool1 = a((Set)localObject5, (Set)localObject4);
        if (!bool1) {
          return false;
        }
      }
      return j;
    }
    finally
    {
      ((com.truecaller.messaging.data.a.a)localObject1).close();
    }
  }
  
  public final boolean a()
  {
    Object localObject = b;
    boolean bool1 = ((al)localObject).a();
    if (bool1)
    {
      localObject = e;
      int i = ((h)localObject).t();
      if (i == 0)
      {
        localObject = e;
        long l1 = ((h)localObject).a();
        long l2 = 0L;
        boolean bool2 = l1 < l2;
        if (bool2) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final boolean b()
  {
    boolean bool = c();
    Object localObject = e;
    int i;
    if (bool) {
      i = 2;
    } else {
      i = 1;
    }
    ((h)localObject).e(i);
    localObject = b;
    Intent localIntent = new android/content/Intent;
    String str = "com.truecaller.messaging.spam.SEARCH_COMPLETED";
    localIntent.<init>(str);
    ((al)localObject).a(localIntent);
    if (bool)
    {
      localObject = f;
      ((ax)localObject).a();
    }
    return bool;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversationlist.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */