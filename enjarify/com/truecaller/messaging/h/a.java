package com.truecaller.messaging.h;

import android.content.ContentValues;
import android.os.Handler;
import android.os.Looper;
import com.truecaller.androidactors.f;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.t;

public final class a
  implements com.truecaller.content.d.a
{
  final f a;
  private final Handler b;
  private final Runnable c;
  private final e d;
  
  public a(e parame, f paramf)
  {
    d = parame;
    a = paramf;
    parame = new android/os/Handler;
    paramf = Looper.getMainLooper();
    parame.<init>(paramf);
    b = parame;
    parame = new com/truecaller/messaging/h/a$a;
    parame.<init>(this);
    parame = (Runnable)parame;
    c = parame;
  }
  
  public final void a(ContentValues paramContentValues)
  {
    Object localObject = d.e();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool1)
    {
      long l1 = 0L;
      if (paramContentValues != null)
      {
        String str = "timestamp";
        paramContentValues = paramContentValues.getAsLong(str);
        if (paramContentValues != null)
        {
          l2 = paramContentValues.longValue();
          break label54;
        }
      }
      long l2 = l1;
      label54:
      boolean bool2 = l2 < l1;
      if (bool2)
      {
        paramContentValues = (t)a.a();
        org.a.a.b localb = new org/a/a/b;
        localb.<init>(l2);
        paramContentValues.a(5, localb, false);
        return;
      }
      paramContentValues = b;
      localObject = c;
      paramContentValues.removeCallbacks((Runnable)localObject);
      paramContentValues = b;
      localObject = c;
      long l3 = 300L;
      paramContentValues.postDelayed((Runnable)localObject, l3);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.h.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */