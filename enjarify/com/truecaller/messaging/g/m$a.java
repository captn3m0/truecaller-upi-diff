package com.truecaller.messaging.g;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.List;

final class m$a
  extends u
{
  private final List b;
  private final String c;
  private final boolean d;
  private final boolean e;
  private final String f;
  
  private m$a(e parame, List paramList, String paramString1, boolean paramBoolean1, boolean paramBoolean2, String paramString2)
  {
    super(parame);
    b = paramList;
    c = paramString1;
    d = paramBoolean1;
    e = paramBoolean2;
    f = paramString2;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".scheduleDrafts(");
    Object localObject = b;
    int i = 2;
    localObject = a(localObject, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(c, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Boolean.valueOf(d), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(Boolean.valueOf(e), i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(",");
    localObject = a(f, i);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.g.m.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */