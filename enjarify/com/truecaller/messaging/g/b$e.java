package com.truecaller.messaging.g;

import c.g.b.k;
import java.util.List;

public final class b$e
  extends b
{
  final List a;
  final String b;
  final boolean c;
  
  public b$e(List paramList, String paramString, boolean paramBoolean)
  {
    super((byte)0);
    a = paramList;
    b = paramString;
    c = paramBoolean;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof e;
      if (bool2)
      {
        paramObject = (e)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool2 = k.a(localObject1, localObject2);
        if (bool2)
        {
          localObject1 = b;
          localObject2 = b;
          bool2 = k.a(localObject1, localObject2);
          if (bool2)
          {
            bool2 = c;
            boolean bool3 = c;
            if (bool2 == bool3)
            {
              bool3 = true;
            }
            else
            {
              bool3 = false;
              paramObject = null;
            }
            if (bool3) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    List localList = a;
    int i = 0;
    if (localList != null)
    {
      k = localList.hashCode();
    }
    else
    {
      k = 0;
      localList = null;
    }
    k *= 31;
    String str = b;
    if (str != null) {
      i = str.hashCode();
    }
    int k = (k + i) * 31;
    int j = c;
    if (j != 0) {
      j = 1;
    }
    return k + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Ready(draftsList=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", simToken=");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", asIM=");
    boolean bool = c;
    localStringBuilder.append(bool);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.g.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */