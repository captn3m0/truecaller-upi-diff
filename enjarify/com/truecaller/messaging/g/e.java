package com.truecaller.messaging.g;

import c.g.b.k;
import c.m.i;
import c.n;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Draft;
import com.truecaller.messaging.data.types.Draft.a;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.util.bg;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class e
  implements d
{
  final bg a;
  final com.truecaller.featuretoggles.e b;
  private final com.truecaller.utils.d c;
  private final com.truecaller.tcpermissions.l d;
  private final com.truecaller.messaging.transport.m e;
  private final f f;
  
  public e(com.truecaller.utils.d paramd, com.truecaller.tcpermissions.l paraml, bg parambg, com.truecaller.messaging.transport.m paramm, f paramf, com.truecaller.featuretoggles.e parame)
  {
    c = paramd;
    d = paraml;
    a = parambg;
    e = paramm;
    f = paramf;
    b = parame;
  }
  
  private static b a(List paramList)
  {
    paramList = c.a.m.n((Iterable)paramList);
    Object localObject1 = (c.g.a.b)e.d.a;
    paramList = c.m.l.c(paramList, (c.g.a.b)localObject1).a();
    int j;
    do
    {
      bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = paramList.next();
      Object localObject2 = localObject1;
      localObject2 = (Collection)localObject1;
      int i = ((Collection)localObject2).size();
      j = 1;
      if (i <= j) {
        j = 0;
      }
    } while (j == 0);
    break label87;
    boolean bool = false;
    localObject1 = null;
    label87:
    localObject1 = (Collection)localObject1;
    if (localObject1 != null) {
      return (b)b.g.a;
    }
    return null;
  }
  
  private static Collection a(Collection paramCollection, Draft paramDraft)
  {
    paramCollection = (Iterable)paramCollection;
    Object localObject1 = new java/util/ArrayList;
    int i = c.a.m.a(paramCollection, 10);
    ((ArrayList)localObject1).<init>(i);
    localObject1 = (Collection)localObject1;
    paramCollection = paramCollection.iterator();
    i = 0;
    Draft localDraft = null;
    for (;;)
    {
      boolean bool = paramCollection.hasNext();
      if (!bool) {
        break;
      }
      Object localObject2 = paramCollection.next();
      int j = i + 1;
      if (i < 0) {
        c.a.m.a();
      }
      localObject2 = (BinaryEntity)localObject2;
      Draft.a locala = paramDraft.c();
      long l = -1;
      locala.a(l);
      locala.b();
      localObject2 = (Collection)c.a.m.a(localObject2);
      locala.a((Collection)localObject2);
      if (i != 0) {
        locala.a();
      }
      localDraft = locala.d();
      ((Collection)localObject1).add(localDraft);
      i = j;
    }
    return (Collection)localObject1;
  }
  
  private static List b(List paramList)
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (List)localObject1;
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      Object localObject2 = (n)paramList.next();
      Object localObject3 = (Draft)a;
      localObject2 = (Collection)b;
      int i = ((Collection)localObject2).size();
      int j = 1;
      if (i > j)
      {
        localObject2 = a((Collection)localObject2, (Draft)localObject3);
        ((List)localObject1).addAll((Collection)localObject2);
      }
      else
      {
        localObject2 = ((Draft)localObject3).c().b().a((Collection)localObject2);
        localObject3 = ((Draft.a)localObject2).d();
        String str = "build()";
        k.a(localObject3, str);
        ((List)localObject1).add(localObject3);
        localObject3 = "draft.buildUpon()\n      …())\n                    }";
        k.a(localObject2, (String)localObject3);
      }
    }
    return (List)localObject1;
  }
  
  private final b c(List paramList)
  {
    paramList = c.a.m.n((Iterable)paramList);
    Object localObject1 = (c.g.a.b)e.a.a;
    paramList = c.m.l.a(paramList, (c.g.a.b)localObject1).a();
    int i;
    do
    {
      bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = paramList.next();
      Object localObject2 = localObject1;
      localObject2 = (n)localObject1;
      Object localObject3 = (Draft)a;
      localObject2 = (Message)b;
      Object localObject4 = e;
      localObject3 = d;
      localObject2 = ((com.truecaller.messaging.transport.m)localObject4).b((Message)localObject2, (Participant[])localObject3);
      k.a(localObject2, "transportManager.getSupp…sage, draft.participants)");
      i = 0;
      localObject3 = null;
      localObject4 = Integer.valueOf(0);
      boolean bool2 = ((List)localObject2).contains(localObject4);
      int j = 1;
      if (bool2)
      {
        localObject4 = Integer.valueOf(j);
        boolean bool3 = ((List)localObject2).contains(localObject4);
        if (bool3) {
          i = 1;
        }
      }
    } while (i == 0);
    break label175;
    boolean bool1 = false;
    localObject1 = null;
    label175:
    localObject1 = (n)localObject1;
    if (localObject1 != null) {
      return (b)b.b.a;
    }
    return null;
  }
  
  public final w a(b.e parame, boolean paramBoolean, String paramString)
  {
    k.b(parame, "preparationResult");
    k.b(paramString, "analyticsContext");
    Object localObject1 = f.a();
    Object localObject2 = localObject1;
    localObject2 = (l)localObject1;
    List localList = a;
    String str = b;
    boolean bool = c;
    return ((l)localObject2).a(localList, str, bool, paramBoolean, paramString);
  }
  
  public final b a(List paramList, String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    k.b(paramList, "draftContent");
    k.b(paramString, "simToken");
    k.b(paramList, "draftsAndMedia");
    k.b(paramString, "simToken");
    Object localObject1 = paramList;
    boolean bool1 = ((Collection)paramList).isEmpty();
    boolean bool2 = true;
    bool1 ^= bool2;
    boolean bool3 = false;
    Object localObject2 = new String[0];
    AssertionUtil.OnlyInDebug.isTrue(bool1, (String[])localObject2);
    bool1 = false;
    localObject1 = null;
    if (!paramBoolean1)
    {
      localObject2 = c;
      bool3 = ((com.truecaller.utils.d)localObject2).d();
      if (!bool3)
      {
        localObject2 = (b)b.d.a;
      }
      else
      {
        localObject2 = d;
        bool3 = ((com.truecaller.tcpermissions.l)localObject2).g();
        if (!bool3)
        {
          localObject2 = (b)b.c.a;
        }
        else
        {
          bool3 = false;
          localObject2 = null;
        }
      }
      if (localObject2 != null) {
        return (b)localObject2;
      }
    }
    if (!paramBoolean1)
    {
      localObject2 = paramList;
      localObject2 = c.a.m.n((Iterable)paramList).a();
      boolean bool6;
      do
      {
        bool5 = ((Iterator)localObject2).hasNext();
        if (!bool5) {
          break;
        }
        localObject3 = ((Iterator)localObject2).next();
        Object localObject4 = localObject3;
        localObject4 = (Draft)a;
        bool6 = ((Draft)localObject4).b();
      } while (!bool6);
      break label232;
      boolean bool5 = false;
      localObject3 = null;
      label232:
      localObject3 = (n)localObject3;
      if (localObject3 != null)
      {
        localObject2 = (b)b.f.a;
        break label261;
      }
    }
    bool3 = false;
    localObject2 = null;
    label261:
    if (localObject2 != null) {
      return (b)localObject2;
    }
    int j;
    if (!paramBoolean1) {
      bool3 = true;
    } else {
      j = 2;
    }
    Object localObject3 = a;
    long l = ((bg)localObject3).a(j);
    Object localObject5 = paramList;
    localObject5 = (Iterable)paramList;
    Object localObject6 = new java/util/ArrayList;
    ((ArrayList)localObject6).<init>();
    localObject6 = (Collection)localObject6;
    localObject5 = ((Iterable)localObject5).iterator();
    for (;;)
    {
      boolean bool7 = ((Iterator)localObject5).hasNext();
      if (!bool7) {
        break;
      }
      Iterable localIterable = (Iterable)nextb;
      c.a.m.a((Collection)localObject6, localIterable);
    }
    localObject5 = c.a.m.n((Iterable)localObject6);
    localObject6 = (c.g.a.b)e.b.a;
    localObject5 = c.m.l.a((i)localObject5, (c.g.a.b)localObject6);
    localObject6 = new com/truecaller/messaging/g/e$c;
    ((e.c)localObject6).<init>(this, l);
    localObject6 = (c.g.a.b)localObject6;
    localObject5 = c.m.l.d(c.m.l.a((i)localObject5, (c.g.a.b)localObject6));
    localObject6 = localObject5;
    localObject6 = (Collection)localObject5;
    boolean bool8 = ((Collection)localObject6).isEmpty();
    bool2 ^= bool8;
    if (bool2)
    {
      localObject1 = new com/truecaller/messaging/g/b$a;
      ((b.a)localObject1).<init>(l, (List)localObject5, j);
      localObject1 = (b)localObject1;
    }
    if (localObject1 != null) {
      return (b)localObject1;
    }
    if ((!paramBoolean1) && (!paramBoolean2))
    {
      localObject7 = a(paramList);
      if (localObject7 != null) {
        return (b)localObject7;
      }
    }
    paramList = b(paramList);
    Object localObject7 = paramList;
    localObject7 = (Iterable)paramList;
    localObject1 = new java/util/ArrayList;
    int i = c.a.m.a((Iterable)localObject7, 10);
    ((ArrayList)localObject1).<init>(i);
    localObject1 = (Collection)localObject1;
    Iterator localIterator = ((Iterable)localObject7).iterator();
    for (;;)
    {
      boolean bool4 = localIterator.hasNext();
      if (!bool4) {
        break;
      }
      localObject2 = ((Draft)localIterator.next()).a(paramString);
      ((Collection)localObject1).add(localObject2);
    }
    localObject1 = (Iterable)localObject1;
    localObject7 = c.a.m.d((Iterable)localObject7, (Iterable)localObject1);
    if ((!paramBoolean1) && (!paramBoolean3))
    {
      localObject7 = c((List)localObject7);
      if (localObject7 != null) {
        return (b)localObject7;
      }
    }
    localObject7 = new com/truecaller/messaging/g/b$e;
    ((b.e)localObject7).<init>(paramList, paramString, paramBoolean1);
    return (b)localObject7;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.g.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */