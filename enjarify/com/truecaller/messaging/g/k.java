package com.truecaller.messaging.g;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog.Builder;
import com.truecaller.messaging.defaultsms.DefaultSmsActivity;
import com.truecaller.tcpermissions.l;
import com.truecaller.wizard.utils.i;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public final class k
{
  public static final k a;
  
  static
  {
    k localk = new com/truecaller/messaging/g/k;
    localk.<init>();
    a = localk;
  }
  
  public static void a(Fragment paramFragment, long paramLong1, long paramLong2, int paramInt1, int paramInt2)
  {
    String str = "fragment";
    c.g.b.k.b(paramFragment, str);
    paramFragment = paramFragment.getContext();
    if (paramFragment == null) {
      return;
    }
    int i = 1;
    int j;
    if (paramInt1 != i) {
      j = 2131886490;
    } else {
      j = 2131886410;
    }
    long l = 60;
    boolean bool1 = paramLong2 < l;
    int k;
    if (bool1) {
      k = 2131888393;
    } else {
      k = 2131888391;
    }
    boolean bool2 = paramLong2 < l;
    if (!bool2)
    {
      localTimeUnit = TimeUnit.SECONDS;
      paramLong2 = localTimeUnit.toMinutes(paramLong2);
    }
    TimeUnit localTimeUnit = null;
    if (paramInt1 != i)
    {
      localObject1 = new Object[i];
      localObject2 = Long.valueOf(paramLong2);
      localObject1[0] = localObject2;
      localObject1 = paramFragment.getString(k, (Object[])localObject1);
      localObject2 = "context.getString(maxVid…e, maxVideoDurationValue)";
      c.g.b.k.a(localObject1, (String)localObject2);
    }
    else
    {
      float f1 = (float)paramLong1;
      float f2 = 1024.0F;
      f1 = f1 / f2 / f2;
      f2 = f1 % 1.0F;
      Object[] arrayOfObject = null;
      boolean bool3 = f2 < 0.0F;
      if (bool3) {
        localObject2 = "%.2f";
      } else {
        localObject2 = "%.0f";
      }
      arrayOfObject = new Object[i];
      localObject1 = Float.valueOf(f1);
      arrayOfObject[0] = localObject1;
      localObject1 = Arrays.copyOf(arrayOfObject, i);
      localObject1 = String.format((String)localObject2, (Object[])localObject1);
      localObject2 = "java.lang.String.format(this, *args)";
      c.g.b.k.a(localObject1, (String)localObject2);
    }
    Object localObject2 = new Object[2];
    localObject2[0] = localObject1;
    Object localObject1 = paramFragment.getString(paramInt2);
    localObject2[i] = localObject1;
    localObject1 = paramFragment.getString(j, (Object[])localObject2);
    localObject2 = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject2).<init>(paramFragment);
    localObject1 = (CharSequence)localObject1;
    ((AlertDialog.Builder)localObject2).setMessage((CharSequence)localObject1).setPositiveButton(2131887217, null).show();
  }
  
  public static void a(Fragment paramFragment, l paraml)
  {
    c.g.b.k.b(paramFragment, "fragment");
    c.g.b.k.b(paraml, "tcPermissionsUtil");
    paraml = paraml.d();
    i.a(paramFragment, paraml, 200);
  }
  
  public static void a(Fragment paramFragment, String paramString)
  {
    c.g.b.k.b(paramFragment, "fragment");
    c.g.b.k.b(paramString, "analyticsContext");
    Context localContext = paramFragment.getContext();
    if (localContext == null) {
      return;
    }
    paramString = DefaultSmsActivity.a(localContext, paramString, true);
    paramFragment.startActivityForResult(paramString, 200);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.g.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */