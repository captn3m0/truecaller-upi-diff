package com.truecaller.messaging.g;

import c.g.b.k;
import com.truecaller.analytics.ap;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.c.a;
import com.truecaller.messaging.data.types.Draft;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class n
  implements l
{
  private final com.truecaller.messaging.transport.m a;
  private final a b;
  private final ap c;
  
  public n(com.truecaller.messaging.transport.m paramm, a parama, ap paramap)
  {
    a = paramm;
    b = parama;
    c = paramap;
  }
  
  public final w a(List paramList, String paramString1, boolean paramBoolean1, boolean paramBoolean2, String paramString2)
  {
    n localn = this;
    Object localObject1 = paramString1;
    String str = paramString2;
    Object localObject2 = paramList;
    k.b(paramList, "draftsList");
    k.b(paramString1, "simToken");
    Object localObject3 = "analyticsContext";
    k.b(paramString2, (String)localObject3);
    boolean bool1 = paramList.isEmpty();
    if (bool1)
    {
      localObject1 = w.b(null);
      k.a(localObject1, "Promise.wrap(null)");
      return (w)localObject1;
    }
    localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    Object localObject4 = localObject3;
    localObject4 = (List)localObject3;
    Iterator localIterator = paramList.iterator();
    Object localObject5;
    for (;;)
    {
      bool1 = localIterator.hasNext();
      if (!bool1) {
        break label755;
      }
      localObject3 = localIterator.next();
      localObject5 = localObject3;
      localObject5 = (Draft)localObject3;
      Object localObject6 = ((Draft)localObject5).a((String)localObject1);
      k.a(localObject6, "draft.buildMessage(simToken)");
      localObject3 = a;
      localObject2 = e;
      Object localObject7 = "draft.media";
      k.a(localObject2, (String)localObject7);
      int k = localObject2.length;
      int n = 1;
      if (k == 0)
      {
        k = 1;
      }
      else
      {
        k = 0;
        localObject2 = null;
      }
      k ^= n;
      localObject7 = d;
      boolean bool5 = paramBoolean1 ^ true;
      int i1 = ((com.truecaller.messaging.transport.m)localObject3).a(k, (Participant[])localObject7, bool5);
      int i = paramString2.hashCode();
      int m = -1930796239;
      boolean bool2;
      if (i != m)
      {
        m = 3625376;
        if (i != m)
        {
          m = 79840983;
          if (i != m)
          {
            m = 108401386;
            if (i == m)
            {
              localObject3 = "reply";
              bool2 = str.equals(localObject3);
              if (bool2)
              {
                localObject3 = "Reply";
                localObject8 = localObject3;
                break label406;
              }
            }
          }
          else
          {
            localObject3 = "incallui";
            bool2 = str.equals(localObject3);
            if (bool2)
            {
              localObject3 = "InCallUI";
              localObject8 = localObject3;
              break label406;
            }
          }
        }
        else
        {
          localObject3 = "voip";
          bool2 = str.equals(localObject3);
          if (bool2)
          {
            localObject3 = "Voip";
            localObject8 = localObject3;
            break label406;
          }
        }
      }
      else
      {
        localObject3 = "forwardMessages";
        bool2 = str.equals(localObject3);
        if (bool2)
        {
          localObject3 = "Forward";
          localObject8 = localObject3;
          break label406;
        }
      }
      localObject3 = "UserInput";
      Object localObject8 = localObject3;
      label406:
      localObject3 = b;
      localObject2 = g;
      Object localObject9 = d;
      k.a(localObject9, "draft.participants");
      localObject7 = paramString2;
      bool5 = i1;
      ((a)localObject3).a((String)localObject2, paramString2, i1, (Participant[])localObject9, (String)localObject8);
      localObject3 = a;
      localObject2 = d;
      boolean bool4 = paramBoolean1 ^ true;
      bool5 = paramBoolean2;
      localObject3 = (Message)((com.truecaller.messaging.transport.m)localObject3).a((Message)localObject6, (Participant[])localObject2, paramBoolean2, bool4).d();
      localObject2 = a.a(i1);
      k.a(localObject2, "transportManager.getTransport(transportType)");
      localObject2 = ((com.truecaller.messaging.transport.l)localObject2).a();
      k.a(localObject2, "transportManager.getTransport(transportType).name");
      localObject7 = c;
      localObject9 = g;
      k.a(localObject9, "draft.analyticsId");
      localObject8 = d;
      localObject6 = "draft.participants";
      k.a(localObject8, (String)localObject6);
      ((ap)localObject7).a(str, (String)localObject9, (String)localObject2, (Participant[])localObject8);
      if (localObject3 == null) {
        break;
      }
      localObject2 = new c/n;
      ((c.n)localObject2).<init>(localObject5, localObject3);
      ((List)localObject4).add(localObject2);
    }
    localObject4 = (Iterable)localObject4;
    localObject1 = new java/util/ArrayList;
    int j = c.a.m.a((Iterable)localObject4, 10);
    ((ArrayList)localObject1).<init>(j);
    localObject1 = (Collection)localObject1;
    localObject3 = ((Iterable)localObject4).iterator();
    for (;;)
    {
      boolean bool3 = ((Iterator)localObject3).hasNext();
      if (!bool3) {
        break;
      }
      localObject2 = (Message)nextb;
      ((Collection)localObject1).add(localObject2);
    }
    localObject1 = (List)localObject1;
    localObject3 = new com/truecaller/messaging/g/c$a;
    ((c.a)localObject3).<init>((List)localObject1, (Draft)localObject5);
    localObject1 = w.b(localObject3);
    k.a(localObject1, "Promise.wrap(DraftSchedu…ap { it.second }, draft))");
    return (w)localObject1;
    label755:
    localObject1 = new com/truecaller/messaging/g/c$b;
    ((c.b)localObject1).<init>((List)localObject4);
    localObject1 = w.b(localObject1);
    k.a(localObject1, "Promise.wrap(DraftSchedu…heduledDraftMessageList))");
    return (w)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.g.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */