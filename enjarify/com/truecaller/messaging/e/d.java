package com.truecaller.messaging.e;

import c.g.b.k;
import com.truecaller.adapter_delegates.h;

public final class d
  extends n
  implements l.a.b
{
  private final int[] b;
  private final l.c.b.a c;
  
  public d(o paramo, l.c.b.a parama)
  {
    super(paramo);
    c = parama;
    paramo = new int[1];
    paramo[0] = 4;
    b = paramo;
  }
  
  public final boolean a(h paramh)
  {
    String str = "event";
    k.b(paramh, str);
    paramh = a;
    int i = paramh.hashCode();
    int j = -2061389113;
    boolean bool;
    if (i != j)
    {
      j = 754140810;
      if (i == j)
      {
        str = "ItemEvent.ACTION_LATER";
        bool = paramh.equals(str);
        if (bool) {
          return c.u();
        }
      }
    }
    else
    {
      str = "ItemEvent.ACTION_CREATE_SHORTCUT";
      bool = paramh.equals(str);
      if (bool) {
        return c.t();
      }
    }
    return false;
  }
  
  public final boolean a(l.b paramb)
  {
    return paramb instanceof l.b.b;
  }
  
  public final int[] a()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.e.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */