package com.truecaller.messaging.e;

import c.g.b.k;
import com.truecaller.adapter_delegates.h;

public final class p
  extends n
  implements l.a.e
{
  private final int[] b;
  private final l.c.e.a c;
  
  public p(o paramo, l.c.e.a parama)
  {
    super(paramo);
    c = parama;
    paramo = new int[1];
    paramo[0] = 4;
    b = paramo;
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    String str1 = a;
    String str2 = "ItemEvent.ACTION_ENABLE";
    boolean bool1 = k.a(str1, str2);
    if (bool1) {
      return c.r();
    }
    paramh = a;
    str1 = "ItemEvent.ACTION_GOT_IT";
    boolean bool2 = k.a(paramh, str1);
    if (bool2) {
      return c.s();
    }
    return false;
  }
  
  public final boolean a(l.b paramb)
  {
    return paramb instanceof l.b.d;
  }
  
  public final int[] a()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.e.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */