package com.truecaller.messaging.e;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.Button;
import com.truecaller.adapter_delegates.i;
import java.util.List;

public final class x
  extends RecyclerView.ViewHolder
  implements l.c.g
{
  public static final x.b a;
  private final RecyclerView b;
  private final Button c;
  private final Button d;
  
  static
  {
    x.b localb = new com/truecaller/messaging/e/x$b;
    localb.<init>((byte)0);
    a = localb;
  }
  
  public x(View paramView, com.truecaller.adapter_delegates.k paramk)
  {
    super(paramView);
    Object localObject = paramView.findViewById(2131362461);
    c.g.b.k.a(localObject, "view.findViewById(R.id.change_log_view)");
    localObject = (RecyclerView)localObject;
    b = ((RecyclerView)localObject);
    localObject = paramView.findViewById(2131362258);
    c.g.b.k.a(localObject, "view.findViewById(R.id.btn_update_app)");
    localObject = (Button)localObject;
    c = ((Button)localObject);
    paramView = paramView.findViewById(2131362223);
    c.g.b.k.a(paramView, "view.findViewById(R.id.btn_dismiss_update)");
    paramView = (Button)paramView;
    d = paramView;
    paramView = (View)c;
    localObject = this;
    localObject = (RecyclerView.ViewHolder)this;
    int i = 8;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, "ItemEvent.ACTION_UPDATE_APP", i);
    i.a((View)d, paramk, (RecyclerView.ViewHolder)localObject, "ItemEvent.ACTION_DISMISS_UPDATE_APP", i);
  }
  
  public final void a(List paramList)
  {
    c.g.b.k.b(paramList, "infoList");
    Object localObject1 = itemView;
    c.g.b.k.a(localObject1, "itemView");
    localObject1 = ((View)localObject1).getContext();
    RecyclerView localRecyclerView = b;
    Object localObject2 = new android/support/v7/widget/LinearLayoutManager;
    ((LinearLayoutManager)localObject2).<init>((Context)localObject1, 1, false);
    localObject2 = (RecyclerView.LayoutManager)localObject2;
    localRecyclerView.setLayoutManager((RecyclerView.LayoutManager)localObject2);
    localRecyclerView = b;
    localObject2 = new com/truecaller/messaging/e/x$a;
    c.g.b.k.a(localObject1, "context");
    ((x.a)localObject2).<init>((Context)localObject1, paramList);
    localObject2 = (RecyclerView.Adapter)localObject2;
    localRecyclerView.setAdapter((RecyclerView.Adapter)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.e.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */