package com.truecaller.messaging.e;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;

public final class i
  extends RecyclerView.ViewHolder
  implements l.c.c
{
  public static final i.a a;
  private final TextView b;
  private final View c;
  
  static
  {
    i.a locala = new com/truecaller/messaging/e/i$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public i(View paramView, com.truecaller.adapter_delegates.k paramk)
  {
    super(paramView);
    Object localObject = paramView.findViewById(2131364681);
    c.g.b.k.a(localObject, "view.findViewById(R.id.text)");
    localObject = (TextView)localObject;
    b = ((TextView)localObject);
    paramView = paramView.findViewById(2131364188);
    c.g.b.k.a(paramView, "view.findViewById(R.id.root)");
    c = paramView;
    paramView = c;
    localObject = this;
    localObject = (RecyclerView.ViewHolder)this;
    com.truecaller.adapter_delegates.i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, "ItemEvent.ACTION_MISSED_CALLS", 8);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "text");
    TextView localTextView = b;
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.e.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */