package com.truecaller.messaging.e;

import dagger.a.d;
import javax.inject.Provider;

public final class w
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private w(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static w a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    w localw = new com/truecaller/messaging/e/w;
    localw.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.e.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */