package com.truecaller.messaging.e;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.Button;
import com.truecaller.adapter_delegates.i;

public final class r
  extends RecyclerView.ViewHolder
  implements l.c.e
{
  public static final r.a a;
  private final Button b;
  private final Button c;
  
  static
  {
    r.a locala = new com/truecaller/messaging/e/r$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public r(View paramView, com.truecaller.adapter_delegates.k paramk)
  {
    super(paramView);
    Object localObject = paramView.findViewById(2131362226);
    c.g.b.k.a(localObject, "view.findViewById(R.id.btn_enable)");
    localObject = (Button)localObject;
    b = ((Button)localObject);
    paramView = paramView.findViewById(2131362230);
    c.g.b.k.a(paramView, "view.findViewById(R.id.btn_got_it)");
    paramView = (Button)paramView;
    c = paramView;
    paramView = (View)b;
    localObject = this;
    localObject = (RecyclerView.ViewHolder)this;
    int i = 8;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, "ItemEvent.ACTION_ENABLE", i);
    i.a((View)c, paramk, (RecyclerView.ViewHolder)localObject, "ItemEvent.ACTION_GOT_IT", i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.e.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */