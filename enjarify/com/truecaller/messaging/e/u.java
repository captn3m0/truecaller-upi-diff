package com.truecaller.messaging.e;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.truecaller.adapter_delegates.i;

public final class u
  extends RecyclerView.ViewHolder
  implements l.c.f
{
  public static final u.a a;
  private final TextView b;
  private final Button c;
  private final Button d;
  
  static
  {
    u.a locala = new com/truecaller/messaging/e/u$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public u(View paramView, com.truecaller.adapter_delegates.k paramk)
  {
    super(paramView);
    Object localObject = paramView.findViewById(2131364884);
    c.g.b.k.a(localObject, "view.findViewById(R.id.title)");
    localObject = (TextView)localObject;
    b = ((TextView)localObject);
    localObject = paramView.findViewById(2131362255);
    c.g.b.k.a(localObject, "view.findViewById(R.id.btn_start_im)");
    localObject = (Button)localObject;
    c = ((Button)localObject);
    paramView = paramView.findViewById(2131362222);
    c.g.b.k.a(paramView, "view.findViewById(R.id.btn_dismiss_im)");
    paramView = (Button)paramView;
    d = paramView;
    paramView = (View)c;
    localObject = this;
    localObject = (RecyclerView.ViewHolder)this;
    int i = 8;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, "ItemEvent.ACTION_START_IM", i);
    i.a((View)d, paramk, (RecyclerView.ViewHolder)localObject, "ItemEvent.ACTION_DISMISS_IM", i);
  }
  
  public final void a()
  {
    Object localObject1 = itemView;
    c.g.b.k.a(localObject1, "itemView");
    localObject1 = ((View)localObject1).getContext();
    c.g.b.k.a(localObject1, "itemView.context");
    localObject1 = ((Context)localObject1).getResources();
    c.g.b.k.a(localObject1, "itemView.context.resources");
    String str = ((Resources)localObject1).getString(2131887170);
    Object localObject2 = new android/text/SpannableStringBuilder;
    localObject1 = (CharSequence)((Resources)localObject1).getString(2131887169);
    ((SpannableStringBuilder)localObject2).<init>((CharSequence)localObject1);
    localObject1 = new android/text/style/StyleSpan;
    ((StyleSpan)localObject1).<init>(1);
    int i = str.length();
    ((SpannableStringBuilder)localObject2).setSpan(localObject1, 0, i, 33);
    localObject1 = b;
    localObject2 = (CharSequence)localObject2;
    ((TextView)localObject1).setText((CharSequence)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.e.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */