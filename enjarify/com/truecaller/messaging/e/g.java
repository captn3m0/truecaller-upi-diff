package com.truecaller.messaging.e;

import c.g.b.k;
import com.truecaller.adapter_delegates.h;

public final class g
  extends n
  implements l.a.c
{
  private final int[] b;
  private final o c;
  private final com.truecaller.utils.n d;
  private final l.c.c.a e;
  
  public g(o paramo, com.truecaller.utils.n paramn, l.c.c.a parama)
  {
    super(paramo);
    c = paramo;
    d = paramn;
    e = parama;
    paramo = new int[1];
    paramo[0] = 4;
    b = paramo;
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    paramh = a;
    String str = "ItemEvent.ACTION_MISSED_CALLS";
    boolean bool = k.a(paramh, str);
    if (bool) {
      return e.y();
    }
    return false;
  }
  
  public final boolean a(l.b paramb)
  {
    return paramb instanceof l.b.c;
  }
  
  public final int[] a()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.e.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */