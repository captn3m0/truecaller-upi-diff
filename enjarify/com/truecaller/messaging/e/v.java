package com.truecaller.messaging.e;

import c.g.b.k;
import com.truecaller.adapter_delegates.h;
import com.truecaller.messaging.transport.im.bu;

public final class v
  extends n
  implements l.a.g
{
  private final int[] b;
  private final l.c.g.a c;
  private final bu d;
  
  public v(o paramo, l.c.g.a parama, bu parambu)
  {
    super(paramo);
    c = parama;
    d = parambu;
    paramo = new int[1];
    paramo[0] = 4;
    b = paramo;
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    String str1 = a;
    String str2 = "ItemEvent.ACTION_UPDATE_APP";
    boolean bool1 = k.a(str1, str2);
    if (bool1) {
      return c.w();
    }
    paramh = a;
    str1 = "ItemEvent.ACTION_DISMISS_UPDATE_APP";
    boolean bool2 = k.a(paramh, str1);
    if (bool2) {
      return c.x();
    }
    return false;
  }
  
  public final boolean a(l.b paramb)
  {
    return paramb instanceof l.b.f;
  }
  
  public final int[] a()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.e.v
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */