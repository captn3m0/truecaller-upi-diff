package com.truecaller.messaging.e;

import dagger.a.d;
import javax.inject.Provider;

public final class t
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private t(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static t a(Provider paramProvider1, Provider paramProvider2)
  {
    t localt = new com/truecaller/messaging/e/t;
    localt.<init>(paramProvider1, paramProvider2);
    return localt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.e.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */