package com.truecaller.messaging.e;

import c.g.b.k;
import java.util.Collection;

public final class l$b$a
  implements l.b
{
  final Collection a;
  
  public l$b$a(Collection paramCollection)
  {
    a = paramCollection;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof a;
      if (bool1)
      {
        paramObject = (a)paramObject;
        Collection localCollection = a;
        paramObject = a;
        boolean bool2 = k.a(localCollection, paramObject);
        if (bool2) {}
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  public final int hashCode()
  {
    Collection localCollection = a;
    if (localCollection != null) {
      return localCollection.hashCode();
    }
    return 0;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ImGroupInvites(groupTitles=");
    Collection localCollection = a;
    localStringBuilder.append(localCollection);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.e.l.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */