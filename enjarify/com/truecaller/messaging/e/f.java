package com.truecaller.messaging.e;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.adapter_delegates.i;
import com.truecaller.utils.ui.b;

public final class f
  extends RecyclerView.ViewHolder
  implements l.c.b
{
  public static final f.a a;
  private final Button b;
  private final Button c;
  private final TextView d;
  private final TextView e;
  private final ImageView f;
  
  static
  {
    f.a locala = new com/truecaller/messaging/e/f$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public f(View paramView, com.truecaller.adapter_delegates.k paramk)
  {
    super(paramView);
    Object localObject = paramView.findViewById(2131362240);
    c.g.b.k.a(localObject, "view.findViewById(R.id.btn_positive)");
    localObject = (Button)localObject;
    b = ((Button)localObject);
    localObject = paramView.findViewById(2131362234);
    c.g.b.k.a(localObject, "view.findViewById(R.id.btn_negative)");
    localObject = (Button)localObject;
    c = ((Button)localObject);
    localObject = paramView.findViewById(2131364884);
    c.g.b.k.a(localObject, "view.findViewById(R.id.title)");
    localObject = (TextView)localObject;
    d = ((TextView)localObject);
    localObject = paramView.findViewById(2131363752);
    c.g.b.k.a(localObject, "view.findViewById(R.id.message)");
    localObject = (TextView)localObject;
    e = ((TextView)localObject);
    paramView = paramView.findViewById(2131363301);
    c.g.b.k.a(paramView, "view.findViewById(R.id.icon)");
    paramView = (ImageView)paramView;
    f = paramView;
    paramView = (View)b;
    localObject = this;
    localObject = (RecyclerView.ViewHolder)this;
    int i = 8;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, "ItemEvent.ACTION_CREATE_SHORTCUT", i);
    i.a((View)c, paramk, (RecyclerView.ViewHolder)localObject, "ItemEvent.ACTION_LATER", i);
  }
  
  public final void a()
  {
    b.setText(2131886457);
  }
  
  public final void b()
  {
    c.setText(2131886566);
  }
  
  public final void c()
  {
    e.setText(2131886458);
  }
  
  public final void d()
  {
    d.setText(2131886460);
  }
  
  public final void e()
  {
    TextView localTextView = d;
    View localView = itemView;
    c.g.b.k.a(localView, "itemView");
    int i = b.a(localView.getContext(), 2130969591);
    localTextView.setTextColor(i);
  }
  
  public final void f()
  {
    f.setImageResource(2131234347);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.e.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */