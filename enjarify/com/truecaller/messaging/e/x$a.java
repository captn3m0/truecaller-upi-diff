package com.truecaller.messaging.e;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import java.util.List;

public final class x$a
  extends RecyclerView.Adapter
{
  private final Context a;
  private final List b;
  
  public x$a(Context paramContext, List paramList)
  {
    a = paramContext;
    b = paramList;
  }
  
  public final int getItemCount()
  {
    return b.size();
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.e.x.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */