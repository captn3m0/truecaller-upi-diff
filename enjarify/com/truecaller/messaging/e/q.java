package com.truecaller.messaging.e;

import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private q(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static q a(Provider paramProvider1, Provider paramProvider2)
  {
    q localq = new com/truecaller/messaging/e/q;
    localq.<init>(paramProvider1, paramProvider2);
    return localq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.e.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */