package com.truecaller.messaging.e;

import dagger.a.d;
import javax.inject.Provider;

public final class b
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private b(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static b a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    b localb = new com/truecaller/messaging/e/b;
    localb.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */