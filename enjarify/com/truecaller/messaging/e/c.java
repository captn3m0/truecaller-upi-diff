package com.truecaller.messaging.e;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.truecaller.adapter_delegates.i;

public final class c
  extends RecyclerView.ViewHolder
  implements l.c.a
{
  public static final c.a a;
  private final TextView b;
  private final TextView c;
  private final Button d;
  
  static
  {
    c.a locala = new com/truecaller/messaging/e/c$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public c(View paramView, com.truecaller.adapter_delegates.k paramk)
  {
    super(paramView);
    Object localObject = paramView.findViewById(2131364884);
    c.g.b.k.a(localObject, "view.findViewById(R.id.title)");
    localObject = (TextView)localObject;
    b = ((TextView)localObject);
    localObject = paramView.findViewById(2131362810);
    c.g.b.k.a(localObject, "view.findViewById(R.id.description)");
    localObject = (TextView)localObject;
    c = ((TextView)localObject);
    paramView = paramView.findViewById(2131365402);
    c.g.b.k.a(paramView, "view.findViewById(R.id.update_button)");
    paramView = (Button)paramView;
    d = paramView;
    paramView = (View)d;
    localObject = this;
    localObject = (RecyclerView.ViewHolder)this;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, "ItemEvent.ACTION_UPDATE", 8);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "title");
    TextView localTextView = b;
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "description");
    TextView localTextView = c;
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.e.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */