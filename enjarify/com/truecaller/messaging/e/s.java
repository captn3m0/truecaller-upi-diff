package com.truecaller.messaging.e;

import c.g.b.k;
import com.truecaller.adapter_delegates.h;

public final class s
  extends n
  implements l.a.f
{
  private final int[] b;
  private final l.c.f.a c;
  
  public s(o paramo, l.c.f.a parama)
  {
    super(paramo);
    c = parama;
    paramo = new int[1];
    paramo[0] = 4;
    b = paramo;
  }
  
  public final boolean a(h paramh)
  {
    k.b(paramh, "event");
    String str1 = a;
    String str2 = "ItemEvent.ACTION_START_IM";
    boolean bool1 = k.a(str1, str2);
    if (bool1) {
      return c.p();
    }
    paramh = a;
    str1 = "ItemEvent.ACTION_DISMISS_IM";
    boolean bool2 = k.a(paramh, str1);
    if (bool2) {
      return c.q();
    }
    return false;
  }
  
  public final boolean a(l.b paramb)
  {
    return paramb instanceof l.b.e;
  }
  
  public final int[] a()
  {
    return b;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.e.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */