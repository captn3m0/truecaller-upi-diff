package com.truecaller.messaging;

import android.os.Build.VERSION;
import com.truecaller.utils.d;

public final class b
  implements a
{
  private String a;
  private final d b;
  
  public b(d paramd)
  {
    b = paramd;
    paramd = b.k();
    a = paramd;
  }
  
  public final void a()
  {
    String str = b.k();
    a = str;
  }
  
  public final String b()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 24;
    if (i >= j) {
      return a;
    }
    return b.k();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */