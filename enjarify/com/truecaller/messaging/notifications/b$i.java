package com.truecaller.messaging.notifications;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.messaging.data.types.Message;

final class b$i
  extends u
{
  private final Message b;
  
  private b$i(e parame, Message paramMessage)
  {
    super(parame);
    b = paramMessage;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".notifyFailed(");
    String str = a(b, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.b.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */