package com.truecaller.messaging.notifications;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.v4.app.z.d;
import android.support.v4.app.z.f;
import android.support.v4.app.z.g;
import android.support.v4.content.b;
import c.a.aa;
import c.a.m;
import c.g.b.k;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Reaction;
import com.truecaller.notificationchannels.e;
import com.truecaller.notificationchannels.j;
import com.truecaller.notifications.a;
import com.truecaller.notifications.l;
import com.truecaller.notifications.l.a;
import com.truecaller.util.al;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class h
  implements g
{
  private Set a;
  private final Context b;
  private final l c;
  private final a d;
  private final e e;
  private final j f;
  private final al g;
  
  public h(Context paramContext, l paraml, a parama, e parame, j paramj, al paramal)
  {
    b = paramContext;
    c = paraml;
    d = parama;
    e = parame;
    f = paramj;
    g = paramal;
    paramContext = (Set)aa.a;
    a = paramContext;
  }
  
  private final z.d a(Reaction paramReaction, Participant paramParticipant, long[] paramArrayOfLong, boolean paramBoolean)
  {
    if (paramBoolean) {
      localObject1 = f.d();
    } else {
      localObject1 = e.a();
    }
    Object localObject2 = new android/support/v4/app/z$d;
    Context localContext = b;
    ((z.d)localObject2).<init>(localContext, (String)localObject1);
    Object localObject1 = ((z.d)localObject2).a(2131234327);
    int i = b.c(b, 2131099685);
    localObject1 = ((z.d)localObject1).f(i);
    localObject2 = (CharSequence)m;
    localObject1 = ((z.d)localObject1).a((CharSequence)localObject2);
    localObject2 = b;
    Object[] arrayOfObject = new Object[1];
    String str = d;
    arrayOfObject[0] = str;
    localObject2 = (CharSequence)((Context)localObject2).getString(2131888603, arrayOfObject);
    Object localObject3 = ((z.d)localObject1).b((CharSequence)localObject2).c(-1).e(paramBoolean);
    localObject1 = b;
    long l1 = b;
    int j = (int)b;
    paramParticipant = f.a((Context)localObject1, paramParticipant, l1, j);
    paramParticipant = ((z.d)localObject3).a(paramParticipant);
    localObject3 = b;
    i = (int)b;
    paramArrayOfLong = f.a((Context)localObject3, paramArrayOfLong, i);
    paramParticipant = paramParticipant.b(paramArrayOfLong).e();
    long l2 = e;
    paramReaction = paramParticipant.a(l2);
    k.a(paramReaction, "NotificationCompat.Build…  .setWhen(reaction.date)");
    return paramReaction;
  }
  
  private final void b(Map paramMap)
  {
    Object localObject1 = (Iterable)paramMap.keySet();
    Object localObject2 = new com/truecaller/messaging/notifications/h$c;
    ((h.c)localObject2).<init>();
    localObject2 = (Comparator)localObject2;
    localObject1 = m.a((Iterable)localObject1, (Comparator)localObject2);
    localObject2 = new android/support/v4/app/z$f;
    ((z.f)localObject2).<init>();
    Object localObject3 = localObject1;
    localObject3 = (Iterable)localObject1;
    Object localObject4 = ((Iterable)localObject3).iterator();
    boolean bool2;
    int j;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject4).hasNext();
      bool2 = false;
      j = 1;
      if (!bool1) {
        break;
      }
      localObject5 = (Reaction)((Iterator)localObject4).next();
      localObject6 = (Participant)paramMap.get(localObject5);
      if (localObject6 != null)
      {
        localObject7 = b;
        int k = 2131888601;
        int m = 2;
        Object[] arrayOfObject = new Object[m];
        localObject6 = m;
        arrayOfObject[0] = localObject6;
        localObject5 = d;
        arrayOfObject[j] = localObject5;
        localObject5 = (CharSequence)((Context)localObject7).getString(k, arrayOfObject);
        ((z.f)localObject2).c((CharSequence)localObject5);
      }
    }
    localObject4 = b;
    int i = 2131888602;
    Object localObject6 = new Object[j];
    int n = paramMap.size();
    Object localObject7 = Integer.valueOf(n);
    localObject6[0] = localObject7;
    localObject4 = (CharSequence)((Context)localObject4).getString(i, (Object[])localObject6);
    ((z.f)localObject2).b((CharSequence)localObject4);
    localObject1 = (Reaction)m.d((List)localObject1);
    localObject4 = (Participant)paramMap.get(localObject1);
    if (localObject4 == null) {
      return;
    }
    Object localObject5 = new java/util/ArrayList;
    int i1 = m.a((Iterable)localObject3, 10);
    ((ArrayList)localObject5).<init>(i1);
    localObject5 = (Collection)localObject5;
    localObject3 = ((Iterable)localObject3).iterator();
    for (;;)
    {
      bool3 = ((Iterator)localObject3).hasNext();
      if (!bool3) {
        break;
      }
      long l1 = nextb;
      localObject6 = Long.valueOf(l1);
      ((Collection)localObject5).add(localObject6);
    }
    localObject3 = m.c((Collection)localObject5);
    localObject5 = (Iterable)paramMap.keySet();
    boolean bool3 = localObject5 instanceof Collection;
    if (bool3)
    {
      localObject6 = localObject5;
      localObject6 = (Collection)localObject5;
      bool3 = ((Collection)localObject6).isEmpty();
      if (bool3) {}
    }
    else
    {
      localObject5 = ((Iterable)localObject5).iterator();
      do
      {
        bool3 = ((Iterator)localObject5).hasNext();
        if (!bool3) {
          break;
        }
        localObject6 = (Reaction)((Iterator)localObject5).next();
        localObject7 = a;
        long l2 = a;
        localObject6 = Long.valueOf(l2);
        bool3 = ((Set)localObject7).contains(localObject6) ^ j;
      } while (!bool3);
      bool2 = true;
    }
    localObject1 = a((Reaction)localObject1, (Participant)localObject4, (long[])localObject3, bool2);
    localObject2 = (z.g)localObject2;
    localObject1 = ((z.d)localObject1).a((z.g)localObject2);
    localObject2 = "buildNotification(lastRe…ons).setStyle(inboxStyle)";
    k.a(localObject1, (String)localObject2);
    paramMap = m.i((Iterable)paramMap.values());
    int i2 = paramMap.size();
    if (i2 > j)
    {
      paramMap = (CharSequence)com.truecaller.messaging.i.g.a((Collection)paramMap);
      ((z.d)localObject1).a(paramMap);
    }
    paramMap = d;
    localObject3 = c;
    localObject5 = new com/truecaller/messaging/notifications/h$d;
    ((h.d)localObject5).<init>(this, (Participant)localObject4);
    localObject5 = (l.a)localObject5;
    localObject1 = ((l)localObject3).a((z.d)localObject1, (l.a)localObject5);
    k.a(localObject1, "notificationIconHelper.c…Avatar(lastParticipant) }");
    paramMap.a(2131363320, (Notification)localObject1, "notificationIncomingReaction");
  }
  
  public final void a(Map paramMap)
  {
    int i = 1;
    int j = 0;
    Object localObject1 = null;
    Object localObject2;
    if (paramMap != null)
    {
      bool2 = paramMap.isEmpty();
      if (!bool2)
      {
        bool2 = false;
        localObject2 = null;
        break label39;
      }
    }
    boolean bool2 = true;
    label39:
    int m = 2131363320;
    if (bool2)
    {
      d.a(m);
      paramMap = (Set)aa.a;
      a = paramMap;
      return;
    }
    int k = Build.VERSION.SDK_INT;
    int n = 24;
    int i1 = 10;
    if (k >= n)
    {
      localObject2 = (Iterable)paramMap.keySet();
      Object localObject3 = new com/truecaller/messaging/notifications/h$b;
      ((h.b)localObject3).<init>();
      localObject3 = (Comparator)localObject3;
      localObject2 = m.a((Iterable)localObject2, (Comparator)localObject3);
      localObject3 = localObject2;
      localObject3 = (Iterable)localObject2;
      Object localObject4 = ((Iterable)localObject3).iterator();
      Object localObject5;
      Object localObject6;
      Object localObject7;
      for (;;)
      {
        boolean bool3 = ((Iterator)localObject4).hasNext();
        if (!bool3) {
          break;
        }
        localObject5 = (Reaction)((Iterator)localObject4).next();
        localObject6 = (Participant)paramMap.get(localObject5);
        if (localObject6 != null)
        {
          localObject7 = a;
          long l1 = a;
          Object localObject8 = Long.valueOf(l1);
          boolean bool4 = ((Set)localObject7).contains(localObject8) ^ i;
          localObject8 = new long[i];
          long l2 = b;
          localObject8[0] = l2;
          localObject7 = a((Reaction)localObject5, (Participant)localObject6, (long[])localObject8, bool4).c("com.truecaller.messaging.notifications.REACTIONS");
          k.a(localObject7, "buildNotification(reacti…roup(GROUP_KEY_REACTIONS)");
          localObject8 = d;
          l2 = b;
          localObject5 = String.valueOf(l2);
          l locall = c;
          Object localObject9 = new com/truecaller/messaging/notifications/h$a;
          ((h.a)localObject9).<init>((Participant)localObject6, this, paramMap);
          localObject9 = (l.a)localObject9;
          localObject6 = locall.a((z.d)localObject7, (l.a)localObject9);
          k.a(localObject6, "notificationIconHelper.c… getAvatar(participant) }");
          localObject7 = "notificationIncomingReaction";
          ((a)localObject8).a((String)localObject5, m, (Notification)localObject6, (String)localObject7);
        }
      }
      localObject10 = (Reaction)m.d((List)localObject2);
      localObject4 = (Participant)paramMap.get(localObject10);
      if (localObject4 != null)
      {
        localObject5 = new java/util/ArrayList;
        int i4 = m.a((Iterable)localObject3, i1);
        ((ArrayList)localObject5).<init>(i4);
        localObject5 = (Collection)localObject5;
        localObject3 = ((Iterable)localObject3).iterator();
        for (;;)
        {
          boolean bool5 = ((Iterator)localObject3).hasNext();
          if (!bool5) {
            break;
          }
          l3 = nextb;
          localObject6 = Long.valueOf(l3);
          ((Collection)localObject5).add(localObject6);
        }
        localObject3 = m.c((Collection)localObject5);
        localObject5 = new android/support/v4/app/z$d;
        localObject6 = b;
        localObject7 = e.a();
        ((z.d)localObject5).<init>((Context)localObject6, (String)localObject7);
        localObject5 = ((z.d)localObject5).a(2131234327);
        localObject6 = b;
        int i3 = 2131099685;
        int i5 = b.c((Context)localObject6, i3);
        localObject5 = ((z.d)localObject5).f(i5);
        localObject6 = "com.truecaller.messaging.notifications.REACTIONS";
        localObject5 = ((z.d)localObject5).c((String)localObject6).f();
        k = ((List)localObject2).size();
        localObject2 = ((z.d)localObject5).b(k);
        int i2 = -1;
        localObject2 = ((z.d)localObject2).c(i2);
        localObject5 = b;
        long l3 = b;
        localObject10 = f.a((Context)localObject5, (Participant)localObject4, l3, 0);
        localObject10 = ((z.d)localObject2).a((PendingIntent)localObject10);
        localObject1 = f.a(b, (long[])localObject3, 0);
        localObject10 = ((z.d)localObject10).b((PendingIntent)localObject1).e().h();
        localObject1 = d;
        k.a(localObject10, "summaryNotification");
        localObject2 = "notificationIncomingReaction";
        ((a)localObject1).a(m, (Notification)localObject10, (String)localObject2);
      }
    }
    else
    {
      b(paramMap);
    }
    paramMap = (Iterable)paramMap.keySet();
    Object localObject10 = new java/util/ArrayList;
    j = m.a(paramMap, i1);
    ((ArrayList)localObject10).<init>(j);
    localObject10 = (Collection)localObject10;
    paramMap = paramMap.iterator();
    for (;;)
    {
      boolean bool1 = paramMap.hasNext();
      if (!bool1) {
        break;
      }
      long l4 = nexta;
      localObject1 = Long.valueOf(l4);
      ((Collection)localObject10).add(localObject1);
    }
    paramMap = m.i((Iterable)localObject10);
    a = paramMap;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */