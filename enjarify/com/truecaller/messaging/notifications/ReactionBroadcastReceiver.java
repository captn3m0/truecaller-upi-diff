package com.truecaller.messaging.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.androidactors.f;
import com.truecaller.bp;
import com.truecaller.messaging.conversation.ConversationActivity;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.transport.im.as;

public final class ReactionBroadcastReceiver
  extends BroadcastReceiver
{
  public f a;
  
  public ReactionBroadcastReceiver()
  {
    TrueApp localTrueApp = TrueApp.y();
    k.a(localTrueApp, "TrueApp.getApp()");
    localTrueApp.a().a(this);
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    if ((paramContext != null) && (paramIntent != null))
    {
      Object localObject1 = paramIntent.getAction();
      if (localObject1 != null)
      {
        int i = ((String)localObject1).hashCode();
        int j = -603694704;
        if (i != j)
        {
          int k = 545703614;
          if (i == k)
          {
            paramContext = "com.truecaller.mark_as_seen";
            boolean bool2 = ((String)localObject1).equals(paramContext);
            if (bool2)
            {
              paramContext = paramIntent.getLongArrayExtra("message_ids");
              k.a(paramContext, "intent.getLongArrayExtra(EXTRA_MESSAGE_IDS)");
              paramIntent = a;
              if (paramIntent == null)
              {
                localObject1 = "imReactionManagerRef";
                k.a((String)localObject1);
              }
              ((as)paramIntent.a()).a(paramContext);
            }
          }
        }
        else
        {
          String str1 = "com.truecaller.open_conversation";
          boolean bool3 = ((String)localObject1).equals(str1);
          if (bool3)
          {
            localObject1 = paramIntent.getParcelableExtra("participant");
            boolean bool1 = localObject1 instanceof Participant;
            if (!bool1)
            {
              bool3 = false;
              localObject1 = null;
            }
            localObject1 = (Participant)localObject1;
            str1 = "message_id";
            long l1 = -1;
            long l2 = paramIntent.getLongExtra(str1, l1);
            if (localObject1 != null)
            {
              paramIntent = new android/content/Intent;
              paramIntent.<init>(paramContext, ConversationActivity.class);
              int m = 268435456;
              paramIntent.setFlags(m);
              String str2 = "participants";
              int n = 1;
              Object localObject2 = new Participant[n];
              localObject2[0] = localObject1;
              localObject2 = (Parcelable[])localObject2;
              paramIntent.putExtra(str2, (Parcelable[])localObject2);
              localObject1 = "message_id";
              paramIntent.putExtra((String)localObject1, l2);
              paramContext.startActivity(paramIntent);
            }
            return;
          }
        }
      }
      paramContext = new java/lang/RuntimeException;
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("Unknown action ");
      paramIntent = paramIntent.getAction();
      ((StringBuilder)localObject1).append(paramIntent);
      ((StringBuilder)localObject1).append(" in onReceive");
      paramIntent = ((StringBuilder)localObject1).toString();
      paramContext.<init>(paramIntent);
      throw ((Throwable)paramContext);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.ReactionBroadcastReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */