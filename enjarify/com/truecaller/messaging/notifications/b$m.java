package com.truecaller.messaging.notifications;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import java.util.Map;

final class b$m
  extends u
{
  private final Map b;
  
  private b$m(e parame, Map paramMap)
  {
    super(parame);
    b = paramMap;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".notifyUnseenMessagesWithPermission(");
    String str = a(b, 1);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.b.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */