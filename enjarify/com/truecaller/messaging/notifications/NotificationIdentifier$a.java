package com.truecaller.messaging.notifications;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class NotificationIdentifier$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    NotificationIdentifier localNotificationIdentifier = new com/truecaller/messaging/notifications/NotificationIdentifier;
    int i = paramParcel.readInt();
    String str = paramParcel.readString();
    int j = paramParcel.readInt();
    localNotificationIdentifier.<init>(i, str, j);
    return localNotificationIdentifier;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new NotificationIdentifier[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.NotificationIdentifier.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */