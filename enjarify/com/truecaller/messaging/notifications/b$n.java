package com.truecaller.messaging.notifications;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;

final class b$n
  extends u
{
  private final long b;
  
  private b$n(e parame, long paramLong)
  {
    super(parame);
    b = paramLong;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".unlockConversation(");
    String str = a(Long.valueOf(b), 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.b.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */