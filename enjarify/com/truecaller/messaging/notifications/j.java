package com.truecaller.messaging.notifications;

import android.app.Notification;
import android.content.Context;
import android.support.v4.app.z.d;
import com.truecaller.notifications.l;
import com.truecaller.notifications.l.a;
import java.lang.reflect.Field;

public final class j
  implements l
{
  private final Context a;
  
  public j(Context paramContext)
  {
    a = paramContext;
  }
  
  private static Object a()
  {
    Object localObject = Class.forName("android.app.MiuiNotification").newInstance();
    Field localField = localObject.getClass().getDeclaredField("customizedIcon");
    localField.setAccessible(true);
    Boolean localBoolean = Boolean.TRUE;
    localField.set(localObject, localBoolean);
    return localObject;
  }
  
  private static void a(Notification paramNotification)
  {
    try
    {
      Object localObject = a();
      a(paramNotification, localObject);
      return;
    }
    catch (Exception localException) {}
  }
  
  private static void a(Notification paramNotification, Object paramObject)
  {
    Field localField = paramNotification.getClass().getField("extraNotification");
    localField.setAccessible(true);
    localField.set(paramNotification, paramObject);
  }
  
  public final Notification a(z.d paramd, l.a parama)
  {
    -..Lambda.j.05bFIID5ZvXkmyqZ_bN-KOANijg local05bFIID5ZvXkmyqZ_bN-KOANijg = new com/truecaller/messaging/notifications/-$$Lambda$j$05bFIID5ZvXkmyqZ_bN-KOANijg;
    local05bFIID5ZvXkmyqZ_bN-KOANijg.<init>(this);
    return a(paramd, parama, local05bFIID5ZvXkmyqZ_bN-KOANijg);
  }
  
  public final Notification a(z.d paramd, l.a parama1, l.a parama2)
  {
    parama1 = parama2.create();
    paramd.a(parama1);
    paramd = paramd.h();
    a(paramd);
    return paramd;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */