package com.truecaller.messaging.notifications;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.support.v4.app.z.d;
import android.support.v4.content.b;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.notifications.l;
import com.truecaller.notifications.l.a;

public final class e
{
  private final Context a;
  private final l b;
  
  public e(Context paramContext, l paraml)
  {
    a = paramContext;
    b = paraml;
  }
  
  private static boolean a(Participant paramParticipant)
  {
    boolean bool = paramParticipant.g();
    if (bool)
    {
      int i = q;
      if (i > 0) {
        return true;
      }
    }
    return false;
  }
  
  final Notification a(Participant paramParticipant, String paramString)
  {
    boolean bool1 = paramParticipant.g();
    int j = 1;
    if (!bool1)
    {
      int i = c;
      if (i == j) {
        return null;
      }
    }
    z.d locald = new android/support/v4/app/z$d;
    Object localObject1 = a;
    locald.<init>((Context)localObject1, paramString);
    int k = 2131234327;
    paramString = locald.a(k);
    localObject1 = a;
    int m = 2131099685;
    int n = b.c((Context)localObject1, m);
    C = n;
    boolean bool3 = a(paramParticipant);
    if (bool3)
    {
      localObject1 = a;
      m = 2131887528;
    }
    else
    {
      localObject1 = a;
      m = 2131887526;
    }
    localObject1 = ((Context)localObject1).getString(m);
    Object localObject2 = new Object[j];
    m = 0;
    String str = paramParticipant.b();
    localObject2[0] = str;
    localObject2 = String.format((String)localObject1, (Object[])localObject2);
    paramString = paramString.a((CharSequence)localObject2);
    boolean bool2 = a(paramParticipant);
    int i1;
    if (bool2)
    {
      localObject2 = a;
      i1 = 2131887527;
    }
    else
    {
      localObject2 = a;
      i1 = 2131887525;
    }
    localObject2 = ((Context)localObject2).getString(i1);
    paramString = paramString.b((CharSequence)localObject2);
    localObject2 = NotificationBroadcastReceiver.a(a, "notificationIncomingMessagePromo");
    f = ((PendingIntent)localObject2);
    paramString = b;
    localObject2 = new com/truecaller/messaging/notifications/-$$Lambda$e$3ddttDFwYAAU-9Kk0Iy_MdYHjqs;
    ((-..Lambda.e.3ddttDFwYAAU-9Kk0Iy_MdYHjqs)localObject2).<init>(this, paramParticipant);
    return paramString.a(locald, (l.a)localObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */