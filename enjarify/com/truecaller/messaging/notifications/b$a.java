package com.truecaller.messaging.notifications;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.u;
import com.truecaller.messaging.data.types.ImGroupInfo;

final class b$a
  extends u
{
  private final ImGroupInfo b;
  
  private b$a(e parame, ImGroupInfo paramImGroupInfo)
  {
    super(parame);
    b = paramImGroupInfo;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(".cancelImGroupInvitation(");
    String str = a(b, 2);
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */