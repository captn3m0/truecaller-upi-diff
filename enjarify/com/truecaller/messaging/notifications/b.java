package com.truecaller.messaging.notifications;

import com.truecaller.androidactors.e;
import com.truecaller.androidactors.v;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.Message;
import java.util.Map;

public final class b
  implements a
{
  private final v a;
  
  public b(v paramv)
  {
    a = paramv;
  }
  
  public static boolean a(Class paramClass)
  {
    return a.class.equals(paramClass);
  }
  
  public final void a()
  {
    v localv = a;
    b.f localf = new com/truecaller/messaging/notifications/b$f;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localf.<init>(locale, (byte)0);
    localv.a(localf);
  }
  
  public final void a(int paramInt)
  {
    v localv = a;
    b.p localp = new com/truecaller/messaging/notifications/b$p;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localp.<init>(locale, paramInt, (byte)0);
    localv.a(localp);
  }
  
  public final void a(long paramLong)
  {
    v localv = a;
    b.e locale = new com/truecaller/messaging/notifications/b$e;
    e locale1 = new com/truecaller/androidactors/e;
    locale1.<init>();
    locale.<init>(locale1, paramLong, (byte)0);
    localv.a(locale);
  }
  
  public final void a(ImGroupInfo paramImGroupInfo)
  {
    v localv = a;
    b.j localj = new com/truecaller/messaging/notifications/b$j;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localj.<init>(locale, paramImGroupInfo, (byte)0);
    localv.a(localj);
  }
  
  public final void a(Message paramMessage)
  {
    v localv = a;
    b.l locall = new com/truecaller/messaging/notifications/b$l;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locall.<init>(locale, paramMessage, (byte)0);
    localv.a(locall);
  }
  
  public final void a(Map paramMap)
  {
    v localv = a;
    b.m localm = new com/truecaller/messaging/notifications/b$m;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localm.<init>(locale, paramMap, (byte)0);
    localv.a(localm);
  }
  
  public final void b()
  {
    v localv = a;
    b.o localo = new com/truecaller/messaging/notifications/b$o;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localo.<init>(locale, (byte)0);
    localv.a(localo);
  }
  
  public final void b(long paramLong)
  {
    v localv = a;
    b.n localn = new com/truecaller/messaging/notifications/b$n;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localn.<init>(locale, paramLong, (byte)0);
    localv.a(localn);
  }
  
  public final void b(ImGroupInfo paramImGroupInfo)
  {
    v localv = a;
    b.a locala = new com/truecaller/messaging/notifications/b$a;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locala.<init>(locale, paramImGroupInfo, (byte)0);
    localv.a(locala);
  }
  
  public final void b(Message paramMessage)
  {
    v localv = a;
    b.i locali = new com/truecaller/messaging/notifications/b$i;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locali.<init>(locale, paramMessage, (byte)0);
    localv.a(locali);
  }
  
  public final void c()
  {
    v localv = a;
    b.c localc = new com/truecaller/messaging/notifications/b$c;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localc.<init>(locale, (byte)0);
    localv.a(localc);
  }
  
  public final void c(long paramLong)
  {
    v localv = a;
    b.b localb = new com/truecaller/messaging/notifications/b$b;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localb.<init>(locale, paramLong, (byte)0);
    localv.a(localb);
  }
  
  public final void c(Message paramMessage)
  {
    v localv = a;
    b.g localg = new com/truecaller/messaging/notifications/b$g;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localg.<init>(locale, paramMessage, (byte)0);
    localv.a(localg);
  }
  
  public final void d()
  {
    v localv = a;
    b.h localh = new com/truecaller/messaging/notifications/b$h;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localh.<init>(locale, (byte)0);
    localv.a(localh);
  }
  
  public final void e()
  {
    v localv = a;
    b.k localk = new com/truecaller/messaging/notifications/b$k;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    localk.<init>(locale, (byte)0);
    localv.a(localk);
  }
  
  public final void f()
  {
    v localv = a;
    b.d locald = new com/truecaller/messaging/notifications/b$d;
    e locale = new com/truecaller/androidactors/e;
    locale.<init>();
    locald.<init>(locale, (byte)0);
    localv.a(locald);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */