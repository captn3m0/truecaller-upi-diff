package com.truecaller.messaging.notifications;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.v4.app.ae.a;
import android.support.v4.app.z.a.a;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.e;
import android.support.v4.app.z.f;
import android.support.v4.app.z.g;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.Pair;
import android.util.Patterns;
import android.widget.RemoteViews;
import android.widget.Toast;
import c.a.m;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.TrueApp;
import com.truecaller.bp;
import com.truecaller.common.h.ah;
import com.truecaller.common.h.am;
import com.truecaller.common.h.aq.d;
import com.truecaller.data.entity.Contact;
import com.truecaller.filters.p;
import com.truecaller.filters.sync.TopSpammer;
import com.truecaller.filters.v;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.UnmutedException.g;
import com.truecaller.messaging.MessagesActivity;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Participant.a;
import com.truecaller.messaging.data.types.TextEntity;
import com.truecaller.messaging.h;
import com.truecaller.notifications.l.a;
import com.truecaller.notifications.x;
import com.truecaller.notifications.y;
import com.truecaller.notifications.y.b;
import com.truecaller.tracking.events.al.a;
import com.truecaller.tracking.events.ay;
import com.truecaller.tracking.events.ay.a;
import com.truecaller.tracking.events.bc;
import com.truecaller.tracking.events.bc.a;
import com.truecaller.tracking.events.z;
import com.truecaller.truepay.SenderInfo;
import com.truecaller.util.at;
import com.truecaller.util.bv;
import com.truecaller.utils.i;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.a.a.a.g;
import org.a.a.aa;
import org.a.a.r;

public final class c
  implements a
{
  private final com.truecaller.search.b A;
  private long B = -100;
  private boolean C;
  private boolean D;
  private long E = -1;
  private final Context a;
  private final com.truecaller.notifications.l b;
  private final com.truecaller.notifications.a c;
  private final com.truecaller.util.al d;
  private final com.truecaller.utils.d e;
  private final bv f;
  private final com.truecaller.network.search.l g;
  private final p h;
  private final com.truecaller.utils.n i;
  private final h j;
  private final com.truecaller.notifications.ae k;
  private final com.truecaller.androidactors.f l;
  private final e m;
  private final com.truecaller.notificationchannels.j n;
  private final com.truecaller.notificationchannels.e o;
  private final com.truecaller.abtest.c p;
  private Set q;
  private final com.truecaller.common.g.a r;
  private final y s;
  private final v t;
  private final i u;
  private final com.truecaller.messaging.i.d v;
  private final com.truecaller.featuretoggles.e w;
  private final com.truecaller.smsparser.a x;
  private final com.truecaller.smsparser.b.d y;
  private final com.truecaller.truepay.d z;
  
  c(Context paramContext, com.truecaller.notifications.a parama, com.truecaller.util.al paramal, com.truecaller.utils.d paramd, bv parambv, com.truecaller.network.search.l paraml, p paramp, com.truecaller.utils.n paramn, h paramh, com.truecaller.notifications.ae paramae, com.truecaller.androidactors.f paramf, com.truecaller.notifications.l paraml1, e parame, com.truecaller.notificationchannels.e parame1, com.truecaller.notificationchannels.j paramj, com.truecaller.common.g.a parama1, y paramy, com.truecaller.abtest.c paramc, v paramv, i parami, com.truecaller.messaging.i.d paramd1, com.truecaller.featuretoggles.e parame2, com.truecaller.smsparser.a parama2, com.truecaller.truepay.d paramd2, com.truecaller.search.b paramb)
  {
    Object localObject = paramContext;
    a = paramContext;
    localObject = parama;
    c = parama;
    localObject = paramal;
    d = paramal;
    localObject = paramd;
    e = paramd;
    localObject = parambv;
    f = parambv;
    localObject = paraml;
    g = paraml;
    localObject = paramp;
    h = paramp;
    localObject = paramn;
    i = paramn;
    localObject = paramh;
    j = paramh;
    localObject = paramae;
    k = paramae;
    localObject = paramf;
    l = paramf;
    localObject = paraml1;
    b = paraml1;
    localObject = parame;
    m = parame;
    localObject = parame1;
    o = parame1;
    localObject = paramj;
    n = paramj;
    localObject = parama1;
    r = parama1;
    localObject = paramy;
    s = paramy;
    localObject = paramc;
    p = paramc;
    localObject = paramv;
    t = paramv;
    localObject = parami;
    u = parami;
    localObject = paramd1;
    v = paramd1;
    localObject = parame2;
    w = parame2;
    localObject = parama2;
    x = parama2;
    localObject = new com/truecaller/smsparser/k;
    bp localbp = ((TrueApp)a.getApplicationContext()).a();
    ((com.truecaller.smsparser.k)localObject).<init>(localbp);
    y = ((com.truecaller.smsparser.b.d)localObject);
    localObject = paramd2;
    z = paramd2;
    localObject = paramb;
    A = paramb;
  }
  
  private Bitmap a(Uri paramUri, int paramInt)
  {
    if (paramUri != null) {}
    try
    {
      try
      {
        localObject = a;
        localObject = w.a((Context)localObject);
        paramUri = ((w)localObject).a(paramUri);
        localObject = aq.d.b();
        paramUri = paramUri.a((ai)localObject);
        paramUri = paramUri.d();
      }
      catch (IllegalArgumentException paramUri)
      {
        Object localObject = new com/truecaller/log/UnmutedException$g;
        paramUri = am.n(paramUri.getMessage());
        ((UnmutedException.g)localObject).<init>(paramUri);
        AssertionUtil.reportThrowableButNeverCrash((Throwable)localObject);
      }
    }
    catch (IOException|SecurityException localIOException)
    {
      for (;;) {}
    }
    paramUri = null;
    if ((paramUri == null) && (paramInt != 0)) {
      paramUri = at.a(android.support.v4.content.b.a(a, paramInt));
    }
    return paramUri;
  }
  
  private Bitmap a(Uri paramUri, boolean paramBoolean)
  {
    if (paramBoolean) {
      paramBoolean = 2131233857;
    } else {
      paramBoolean = 2131233849;
    }
    return a(paramUri, paramBoolean);
  }
  
  private Uri a(Conversation paramConversation, Map paramMap)
  {
    ImGroupInfo localImGroupInfo = x;
    if (localImGroupInfo != null)
    {
      paramConversation = x.c;
      if (paramConversation != null)
      {
        boolean bool = paramConversation.isEmpty();
        if (!bool) {
          return Uri.parse(paramConversation);
        }
      }
      return null;
    }
    int i1 = c;
    int i2 = 1;
    if (i1 == i2) {
      return null;
    }
    paramConversation = a(l[0], paramMap);
    paramMap = d;
    long l1 = p;
    paramConversation = n;
    return paramMap.a(l1, paramConversation, i2);
  }
  
  private android.support.v4.app.z.a a(String paramString, NotificationIdentifier paramNotificationIdentifier, List paramList)
  {
    android.support.v4.app.z.a locala = new android/support/v4/app/z$a;
    String str = a.getString(2131886388);
    paramString = NotificationBroadcastReceiver.a(a, paramNotificationIdentifier, paramString, false, paramList);
    locala.<init>(2131234474, str, paramString);
    return locala;
  }
  
  private android.support.v4.app.z.a a(List paramList, Uri paramUri)
  {
    paramList = NotificationBroadcastReceiver.a(a, paramList, paramUri);
    paramUri = new android/support/v4/app/z$a;
    String str = a.getString(2131888548);
    paramUri.<init>(2131234474, str, paramList);
    return paramUri;
  }
  
  private android.support.v4.app.z.a a(List paramList, NotificationIdentifier paramNotificationIdentifier)
  {
    android.support.v4.app.z.a locala = new android/support/v4/app/z$a;
    int i1 = paramList.size();
    int i2 = 1;
    if (i1 == i2)
    {
      localObject = a;
      i2 = 2131886651;
    }
    else
    {
      localObject = a;
      i2 = 2131886650;
    }
    Object localObject = ((Context)localObject).getString(i2);
    paramList = NotificationBroadcastReceiver.c(a, paramList, paramNotificationIdentifier);
    locala.<init>(2131234063, (CharSequence)localObject, paramList);
    return locala;
  }
  
  private android.support.v4.app.z.a a(List paramList, NotificationIdentifier paramNotificationIdentifier, String paramString)
  {
    int i1 = Build.VERSION.SDK_INT;
    int i2 = 24;
    if (i1 >= i2)
    {
      localObject = a;
      paramList = NotificationBroadcastReceiver.a((Context)localObject, paramList, paramNotificationIdentifier, paramString);
    }
    else
    {
      paramNotificationIdentifier = a;
      paramList = NotificationBroadcastReceiver.b(paramNotificationIdentifier, paramList);
    }
    paramNotificationIdentifier = a.getString(2131888676);
    paramString = new android/support/v4/app/ae$a;
    paramString.<init>("KEY_REPLY_TEXT");
    a = paramNotificationIdentifier;
    paramString = paramString.a();
    Object localObject = new android/support/v4/app/z$a$a;
    ((z.a.a)localObject).<init>(2131234528, paramNotificationIdentifier, paramList);
    return ((z.a.a)localObject).a(paramString).a();
  }
  
  private static Spannable a(String paramString)
  {
    SpannableStringBuilder localSpannableStringBuilder = new android/text/SpannableStringBuilder;
    localSpannableStringBuilder.<init>(paramString);
    StyleSpan localStyleSpan = new android/text/style/StyleSpan;
    localStyleSpan.<init>(1);
    int i1 = paramString.length();
    localSpannableStringBuilder.setSpan(localStyleSpan, 0, i1, 33);
    return localSpannableStringBuilder;
  }
  
  private RemoteViews a(int paramInt, String paramString1, NotificationIdentifier paramNotificationIdentifier, long paramLong, String paramString2)
  {
    RemoteViews localRemoteViews = new android/widget/RemoteViews;
    String str = a.getPackageName();
    localRemoteViews.<init>(str, paramInt);
    Object localObject = x.a(a, paramString1, paramLong, paramNotificationIdentifier);
    localObject = PendingIntent.getService(a, 0, (Intent)localObject, 134217728);
    paramString1 = paramString1.replaceAll("", " ").trim();
    localRemoteViews.setTextViewText(2131363847, paramString1);
    paramString1 = i;
    paramNotificationIdentifier = new Object[1];
    paramNotificationIdentifier[0] = paramString2;
    paramString1 = paramString1.a(2131888473, paramNotificationIdentifier);
    localRemoteViews.setTextViewText(2131364297, paramString1);
    localRemoteViews.setOnClickPendingIntent(2131362606, (PendingIntent)localObject);
    return localRemoteViews;
  }
  
  private static Participant a(Message paramMessage, Map paramMap)
  {
    return a(c, paramMap);
  }
  
  private Participant a(Participant paramParticipant, List paramList)
  {
    Object localObject1 = e;
    boolean bool1 = ((com.truecaller.utils.d)localObject1).d();
    if (bool1) {
      localObject1 = "notification";
    } else {
      localObject1 = "notificationNotDefault";
    }
    Object localObject2 = u;
    boolean bool2 = ((i)localObject2).a();
    if (!bool2)
    {
      a(paramParticipant, "noConnection", (String)localObject1, paramList);
      return paramParticipant;
    }
    localObject2 = d;
    bool2 = ((com.truecaller.util.al)localObject2).a();
    if (!bool2)
    {
      a(paramParticipant, "noAccount", (String)localObject1, paramList);
      return paramParticipant;
    }
    bool2 = paramParticipant.h();
    if (!bool2)
    {
      a(paramParticipant, "notNumber", (String)localObject1, paramList);
      return paramParticipant;
    }
    localObject2 = A;
    long l1 = t;
    int i3 = o;
    bool2 = ((com.truecaller.search.b)localObject2).a(l1, i3);
    if (bool2)
    {
      a(paramParticipant, "validCacheResult", (String)localObject1, paramList);
      return paramParticipant;
    }
    try
    {
      localObject2 = g;
      Object localObject3 = UUID.randomUUID();
      localObject1 = ((com.truecaller.network.search.l)localObject2).a((UUID)localObject3, (String)localObject1);
      localObject1 = ((com.truecaller.network.search.j)localObject1).a();
      localObject2 = f;
      i = ((String)localObject2);
      int i2 = 20;
      h = i2;
      a = paramList;
      paramList = ((com.truecaller.network.search.j)localObject1).f();
      if (paramList != null)
      {
        paramList = paramList.a();
        if (paramList != null)
        {
          int i1 = c;
          i2 = 0;
          localObject2 = null;
          int i4 = 1;
          if (i1 == i4)
          {
            i4 = 0;
            localObject3 = null;
          }
          localObject1 = paramParticipant.l();
          if (i4 != 0) {
            localObject3 = paramList.t();
          } else {
            localObject3 = paramParticipant.a();
          }
          l = ((String)localObject3);
          i4 = o;
          int i5 = paramList.getSource();
          i4 &= i5;
          n = i4;
          localObject3 = paramList.v();
          m = ((String)localObject3);
          int i6 = paramList.I();
          i6 = Math.max(0, i6);
          p = i6;
          return ((Participant.a)localObject1).a();
        }
      }
    }
    catch (IOException localIOException)
    {
      boolean bool3 = k;
      if (bool3)
      {
        paramList = t;
        localObject1 = f;
        paramList = paramList.a((String)localObject1);
        if (paramList != null)
        {
          localObject1 = paramParticipant.l();
          localObject2 = paramList.getLabel();
          if (localObject2 != null) {
            localObject2 = paramList.getLabel();
          } else {
            localObject2 = m;
          }
          l = ((String)localObject2);
          localObject2 = paramList.getReports();
          int i7;
          if (localObject2 != null)
          {
            paramParticipant = paramList.getReports();
            i7 = paramParticipant.intValue();
          }
          else
          {
            i7 = q;
          }
          p = i7;
          return ((Participant.a)localObject1).a();
        }
      }
    }
    return paramParticipant;
  }
  
  private static Participant a(Participant paramParticipant, Map paramMap)
  {
    String str = f;
    paramMap = (Participant)paramMap.get(str);
    if (paramMap != null) {
      return paramMap;
    }
    return paramParticipant;
  }
  
  private CharSequence a(Message paramMessage, Conversation paramConversation, Map paramMap, boolean paramBoolean)
  {
    SpannableStringBuilder localSpannableStringBuilder = new android/text/SpannableStringBuilder;
    localSpannableStringBuilder.<init>();
    int i1 = c;
    int i2 = 1;
    if ((i1 == i2) && (paramBoolean))
    {
      paramConversation = a(b(paramConversation, paramMap));
      paramConversation = localSpannableStringBuilder.append(paramConversation);
      String str = " ";
      paramConversation = paramConversation.append(str);
      paramMap = a(paramMessage, paramMap).a();
      paramConversation = paramConversation.append(paramMap);
      paramMap = ": ";
      paramConversation.append(paramMap);
    }
    else
    {
      int i3 = c;
      if ((i3 == i2) || (paramBoolean))
      {
        paramConversation = a(a(paramMessage, paramMap).a());
        paramConversation = localSpannableStringBuilder.append(paramConversation);
        paramMap = ": ";
        paramConversation.append(paramMap);
      }
    }
    paramConversation = new com/truecaller/messaging/i/c;
    paramMap = ah.a(v.a(paramMessage));
    paramConversation.<init>(paramMap);
    paramMap = a.getResources();
    paramConversation = paramConversation.a(paramMap);
    paramMessage = v.b(paramMessage);
    g = paramMessage;
    paramMessage = paramConversation.a();
    localSpannableStringBuilder.append(paramMessage);
    return localSpannableStringBuilder;
  }
  
  private Map a(List paramList)
  {
    HashMap localHashMap1 = new java/util/HashMap;
    localHashMap1.<init>();
    HashMap localHashMap2 = new java/util/HashMap;
    localHashMap2.<init>();
    paramList = paramList.iterator();
    boolean bool;
    Object localObject1;
    Object localObject2;
    Object localObject3;
    for (;;)
    {
      bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = (Message)paramList.next();
      localObject2 = c.f;
      localObject3 = c;
      localHashMap1.put(localObject2, localObject3);
      localObject2 = c.f;
      localObject2 = (List)localHashMap2.get(localObject2);
      if (localObject2 == null)
      {
        localObject2 = new java/util/ArrayList;
        ((ArrayList)localObject2).<init>();
        localObject3 = c.f;
        localHashMap2.put(localObject3, localObject2);
      }
      localObject1 = ((Message)localObject1).l();
      ((List)localObject2).add(localObject1);
    }
    paramList = localHashMap1.keySet().iterator();
    for (;;)
    {
      bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = (String)paramList.next();
      localObject2 = (Participant)localHashMap1.get(localObject1);
      localObject3 = (List)localHashMap2.get(localObject1);
      localObject2 = a((Participant)localObject2, (List)localObject3);
      localHashMap1.put(localObject1, localObject2);
    }
    return localHashMap1;
  }
  
  private void a(z.d paramd)
  {
    paramd = paramd.a(2131234327);
    int i1 = android.support.v4.content.b.c(a, 2131099685);
    C = i1;
  }
  
  private void a(Participant paramParticipant, String paramString1, String paramString2, List paramList)
  {
    com.truecaller.tracking.events.z.a locala = z.b();
    Object localObject1 = UUID.randomUUID().toString();
    locala.a((CharSequence)localObject1).d(paramString2).c("20");
    paramString2 = null;
    locala.b(null);
    boolean bool1 = false;
    localObject1 = null;
    locala.a(false);
    locala.b(false);
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject2 = com.truecaller.tracking.events.al.b();
    boolean bool2 = TextUtils.isEmpty(m);
    int i5 = 1;
    bool2 ^= i5;
    localObject2 = ((al.a)localObject2).b(bool2);
    bool2 = paramParticipant.d();
    localObject2 = ((al.a)localObject2).a(bool2);
    int i1 = q;
    Object localObject3 = Integer.valueOf(Math.max(0, i1));
    localObject2 = ((al.a)localObject2).a((Integer)localObject3);
    i1 = q;
    int i6 = 10;
    if (i1 >= i6)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      localObject3 = null;
    }
    localObject3 = Boolean.valueOf(i1);
    localObject2 = ((al.a)localObject2).d((Boolean)localObject3);
    int i2 = j;
    if (i2 == i5)
    {
      i2 = 1;
    }
    else
    {
      i2 = 0;
      localObject3 = null;
    }
    localObject3 = Boolean.valueOf(i2);
    localObject2 = ((al.a)localObject2).a((Boolean)localObject3);
    int i3 = j;
    int i7 = 2;
    if (i3 == i7)
    {
      i3 = 1;
    }
    else
    {
      i3 = 0;
      localObject3 = null;
    }
    localObject3 = Boolean.valueOf(i3);
    localObject2 = ((al.a)localObject2).c((Boolean)localObject3);
    localObject3 = Boolean.valueOf(k);
    localObject2 = ((al.a)localObject2).b((Boolean)localObject3);
    int i4 = o & 0x40;
    if (i4 != 0)
    {
      i4 = 1;
    }
    else
    {
      i4 = 0;
      localObject3 = null;
    }
    localObject3 = Boolean.valueOf(i4);
    localObject2 = ((al.a)localObject2).e((Boolean)localObject3).a();
    localObject3 = bc.b().a(null).b(null).c(null).a();
    int i8 = o & i5;
    if (i8 != 0)
    {
      paramString2 = f;
      String str = "+";
      bool1 = org.c.a.a.a.k.a(paramString2, str, false);
      if (bool1) {
        paramString2 = paramString2.substring(i5);
      }
    }
    localObject1 = ay.b();
    paramParticipant = f;
    paramParticipant = ((ay.a)localObject1).a(paramParticipant).a((bc)localObject3).a((com.truecaller.tracking.events.al)localObject2).b(paramString1).c(paramString2).a();
    localArrayList.add(paramParticipant);
    locala.a(localArrayList);
    locala.b(paramList);
    try
    {
      paramParticipant = l;
      paramParticipant = paramParticipant.a();
      paramParticipant = (com.truecaller.analytics.ae)paramParticipant;
      paramString1 = locala.a();
      paramParticipant.a(paramString1);
      return;
    }
    catch (org.apache.a.a locala1)
    {
      AssertionUtil.reportThrowableButNeverCrash(locala1;
    }
  }
  
  private void a(Collection paramCollection)
  {
    try
    {
      Object localObject1 = a;
      Object localObject2 = "notifications.state";
      boolean bool = false;
      Object localObject3 = null;
      localObject1 = ((Context)localObject1).openFileOutput((String)localObject2, 0);
      try
      {
        localObject2 = new java/io/DataOutputStream;
        ((DataOutputStream)localObject2).<init>((OutputStream)localObject1);
        paramCollection = paramCollection.iterator();
        for (;;)
        {
          bool = paramCollection.hasNext();
          if (!bool) {
            break;
          }
          localObject3 = paramCollection.next();
          localObject3 = (Long)localObject3;
          long l1 = ((Long)localObject3).longValue();
          ((DataOutputStream)localObject2).writeLong(l1);
        }
        return;
      }
      finally
      {
        com.truecaller.utils.extensions.d.a((Closeable)localObject1);
      }
      return;
    }
    catch (IOException localIOException)
    {
      AssertionUtil.reportThrowableButNeverCrash(localIOException;
    }
  }
  
  private void a(List paramList, String paramString1, String paramString2)
  {
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    paramList = paramList.iterator();
    Object localObject2;
    for (;;)
    {
      boolean bool1 = paramList.hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (Message)paramList.next();
      Object localObject3 = c.f;
      Object localObject4 = c;
      ((Map)localObject1).put(localObject3, localObject4);
      localObject3 = c.f;
      localObject3 = (List)localHashMap.get(localObject3);
      if (localObject3 == null)
      {
        localObject3 = new java/util/ArrayList;
        ((ArrayList)localObject3).<init>();
        localObject4 = c.f;
        localHashMap.put(localObject4, localObject3);
      }
      localObject2 = ((Message)localObject2).l();
      ((List)localObject3).add(localObject2);
    }
    paramList = ((Map)localObject1).entrySet().iterator();
    for (;;)
    {
      boolean bool2 = paramList.hasNext();
      if (!bool2) {
        break;
      }
      localObject1 = (Map.Entry)paramList.next();
      localObject2 = (Participant)((Map.Entry)localObject1).getValue();
      localObject1 = ((Map.Entry)localObject1).getKey();
      localObject1 = (List)localHashMap.get(localObject1);
      a((Participant)localObject2, paramString1, paramString2, (List)localObject1);
    }
  }
  
  private void a(Map paramMap1, Map paramMap2)
  {
    c localc = this;
    Map localMap = paramMap2;
    int i1 = Math.min(paramMap1.size(), 8);
    int i2 = paramMap1.size();
    int i3 = 0;
    Object localObject1 = null;
    int i4 = 1;
    if (i2 != i4) {
      i2 = 1;
    } else {
      i2 = 0;
    }
    Object localObject2 = paramMap1.entrySet().iterator();
    int i6 = 0;
    Object localObject3 = null;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject2).hasNext();
      if (!bool1) {
        break;
      }
      Object localObject4 = (Map.Entry)((Iterator)localObject2).next();
      Conversation localConversation = (Conversation)((Map.Entry)localObject4).getKey();
      localObject4 = (List)((Map.Entry)localObject4).getValue();
      boolean bool2 = ((List)localObject4).isEmpty();
      String[] arrayOfString = { "Messages cannot be empty" };
      AssertionUtil.isFalse(bool2, arrayOfString);
      Object localObject5 = (Message)((List)localObject4).get(0);
      long l1 = b;
      Object localObject6 = String.valueOf(l1);
      com.truecaller.featuretoggles.b localb = w.w();
      boolean bool3 = localb.a();
      if (!bool3)
      {
        bool3 = localConversation.c();
        if (bool3)
        {
          bool3 = true;
          break label222;
        }
      }
      bool3 = false;
      localb = null;
      label222:
      Object localObject7 = a;
      Object localObject8 = new Object[i4];
      Object localObject9 = l;
      localObject1 = -..Lambda.c.qHL7POPSRzeX6ovKs-OstRwBrbI.INSTANCE;
      Object localObject10 = localObject2;
      c.g.b.k.b(localObject9, "receiver$0");
      localObject2 = "selector";
      c.g.b.k.b(localObject1, (String)localObject2);
      int i7 = localObject9.length;
      if (i7 == 0)
      {
        i7 = 1;
      }
      else
      {
        i7 = 0;
        localObject2 = null;
      }
      int i8;
      int i9;
      Object localObject11;
      Object localObject12;
      if (i7 != 0)
      {
        i8 = i1;
        i9 = i6;
        localObject11 = localObject6;
        localObject12 = null;
      }
      else
      {
        i8 = i1;
        i7 = 0;
        localObject13 = localObject9[0];
        localObject2 = (Comparable)((c.g.a.b)localObject1).invoke(localObject13);
        localObject12 = localObject13;
        i1 = c.a.f.e((Object[])localObject9);
        if (i1 > 0)
        {
          i9 = i6;
          localObject11 = localObject6;
          localObject3 = localObject2;
          i7 = 1;
          for (;;)
          {
            localObject6 = localObject9[i7];
            Object localObject14 = ((c.g.a.b)localObject1).invoke(localObject6);
            Object localObject15 = localObject1;
            localObject1 = localObject14;
            localObject1 = (Comparable)localObject14;
            int i10 = ((Comparable)localObject3).compareTo(localObject1);
            if (i10 < 0)
            {
              localObject3 = localObject1;
              localObject12 = localObject6;
            }
            if (i7 == i1) {
              break;
            }
            i7 += 1;
            localObject1 = localObject15;
          }
        }
        i9 = i6;
        localObject11 = localObject6;
      }
      Object localObject13 = localObject12;
      localObject13 = (Participant)localObject12;
      if (localObject13 == null)
      {
        i3 = 0;
        localObject1 = null;
      }
      else
      {
        i3 = q;
      }
      localObject13 = Integer.valueOf(i3);
      localObject1 = null;
      localObject8[0] = localObject13;
      i1 = 2131886146;
      localObject13 = ((Context)localObject7).getString(i1, (Object[])localObject8);
      i3 = ((List)localObject4).size();
      int i5 = 1;
      if (i3 > i5)
      {
        localObject1 = new android/support/v4/app/z$f;
        ((z.f)localObject1).<init>();
        if (bool3) {
          ((z.f)localObject1).c((CharSequence)localObject13);
        }
        i5 = ((List)localObject4).size();
        if (bool3) {
          i7 = 2;
        } else {
          i7 = 1;
        }
        i6 = 5;
        i7 = 5 - i7;
        i5 = Math.min(i5, i7);
        i7 = 0;
        localObject2 = null;
        while (i7 < i5)
        {
          localObject6 = (Message)((List)localObject4).get(i7);
          localObject6 = localc.a((Message)localObject6, localConversation, localMap, i2);
          ((z.f)localObject1).c((CharSequence)localObject6);
          i7 += 1;
        }
        i5 = ((List)localObject4).size();
        if (i5 == i6)
        {
          i5 = 4;
          localObject9 = (Message)((List)localObject4).get(i5);
          localObject9 = localc.a((Message)localObject9, localConversation, localMap, i2);
          ((z.f)localObject1).c((CharSequence)localObject9);
          localObject8 = null;
        }
        else
        {
          i5 = ((List)localObject4).size();
          if (i5 > i6)
          {
            i5 = ((List)localObject4).size() - i6;
            i7 = 1;
            i5 += i7;
            localObject3 = i;
            i11 = 2131755022;
            localObject7 = new Object[i7];
            localObject2 = Integer.valueOf(i5);
            localObject8 = null;
            localObject7[0] = localObject2;
            localObject9 = ((com.truecaller.utils.n)localObject3).a(i11, i5, (Object[])localObject7);
            ((z.f)localObject1).c((CharSequence)localObject9);
          }
          else
          {
            localObject8 = null;
          }
        }
      }
      else
      {
        localObject8 = null;
        i3 = 0;
        localObject1 = null;
      }
      localObject9 = (Message)((List)localObject4).get(0);
      localObject2 = o.a();
      localObject3 = new android/support/v4/app/z$d;
      localObject6 = a;
      ((z.d)localObject3).<init>((Context)localObject6, (String)localObject2);
      localc.a((z.d)localObject3);
      localObject6 = localc.a((Message)localObject9, localConversation, localMap, false);
      if (bool3) {
        ((z.d)localObject3).b((CharSequence)localObject13);
      } else {
        ((z.d)localObject3).b((CharSequence)localObject6);
      }
      localObject7 = new java/lang/StringBuilder;
      ((StringBuilder)localObject7).<init>();
      if (bool3)
      {
        ((StringBuilder)localObject7).append((String)localObject13);
        localObject13 = "\n";
        ((StringBuilder)localObject7).append((String)localObject13);
      }
      ((StringBuilder)localObject7).append((CharSequence)localObject6);
      localObject13 = new android/support/v4/app/z$c;
      ((z.c)localObject13).<init>();
      localObject6 = ((StringBuilder)localObject7).toString();
      localObject13 = ((z.c)localObject13).b((CharSequence)localObject6);
      ((z.d)localObject3).a((z.g)localObject13);
      localObject13 = new java/lang/StringBuilder;
      ((StringBuilder)localObject13).<init>();
      localObject6 = localc.b(localConversation, localMap);
      ((StringBuilder)localObject13).append((String)localObject6);
      int i11 = ((List)localObject4).size();
      int i13 = 1;
      if (i11 > i13)
      {
        ((StringBuilder)localObject13).append(' ');
        ((StringBuilder)localObject13).append('(');
        i11 = ((List)localObject4).size();
        ((StringBuilder)localObject13).append(i11);
        i11 = 41;
        ((StringBuilder)localObject13).append(i11);
      }
      localObject13 = ((StringBuilder)localObject13).toString();
      ((z.d)localObject3).a((CharSequence)localObject13);
      long l2 = e.a;
      ((z.d)localObject3).a(l2);
      localObject13 = "notificationGroupNewMessages";
      u = ((String)localObject13);
      if (localObject1 != null) {
        ((z.d)localObject3).a((z.g)localObject1);
      }
      localObject13 = new com/truecaller/messaging/notifications/NotificationIdentifier;
      i3 = 2131363802;
      int i14 = (int)l1;
      localObject5 = localObject11;
      ((NotificationIdentifier)localObject13).<init>(i3, (String)localObject11, i14);
      localObject1 = NotificationBroadcastReceiver.a(a, (List)localObject4, (NotificationIdentifier)localObject13);
      ((z.d)localObject3).b((PendingIntent)localObject1);
      localObject1 = NotificationBroadcastReceiver.b(a, (List)localObject4, (NotificationIdentifier)localObject13);
      f = ((PendingIntent)localObject1);
      localObject1 = l;
      bool2 = false;
      localObject5 = null;
      localObject1 = localObject1[0];
      localObject9 = Collections.singletonList(localObject9);
      localObject9 = localc.b((List)localObject9);
      if (localObject9 != null)
      {
        i14 = 1;
      }
      else
      {
        i14 = 0;
        arrayOfString = null;
      }
      localObject6 = z;
      localObject7 = com.truecaller.common.h.ab.c(e);
      localObject6 = ((com.truecaller.truepay.d)localObject6).a((String)localObject7);
      localObject7 = w.n();
      boolean bool4 = ((com.truecaller.featuretoggles.b)localObject7).a();
      if ((bool4) && (localObject6 != null))
      {
        localObject7 = ((SenderInfo)localObject6).getCategory();
        localObject8 = "bank";
        bool4 = ((String)localObject7).equals(localObject8);
        if (bool4)
        {
          bool4 = true;
          break label1368;
        }
      }
      bool4 = false;
      localObject7 = null;
      label1368:
      localObject8 = localc.a((List)localObject4, (NotificationIdentifier)localObject13);
      ((z.d)localObject3).a((android.support.v4.app.z.a)localObject8);
      localObject8 = localc.b((List)localObject4, (NotificationIdentifier)localObject13);
      if (bool4)
      {
        localObject1 = ((SenderInfo)localObject6).getSymbol();
        localObject1 = localc.a((String)localObject1, (NotificationIdentifier)localObject13, (List)localObject4);
        ((z.d)localObject3).a((android.support.v4.app.z.a)localObject1);
      }
      else if (bool3)
      {
        ((z.d)localObject3).a((android.support.v4.app.z.a)localObject8);
      }
      else
      {
        i3 = c;
        int i12 = 1;
        if (i3 == i12)
        {
          ((z.d)localObject3).a((android.support.v4.app.z.a)localObject8);
        }
        else if (i14 != 0)
        {
          localObject1 = localc.a((List)localObject4, (Uri)localObject9);
          ((z.d)localObject3).a((android.support.v4.app.z.a)localObject1);
        }
        else
        {
          localObject1 = localc.a((List)localObject4, (NotificationIdentifier)localObject13, (String)localObject2);
          ((z.d)localObject3).a((android.support.v4.app.z.a)localObject1);
        }
      }
      localObject1 = localc.a(localConversation, localMap);
      localObject9 = "notificationIncomingMessage";
      localObject2 = b;
      localObject4 = new com/truecaller/messaging/notifications/-$$Lambda$c$uuy8UBqvqWb_vIBDcJQNeYN0GL4;
      ((-..Lambda.c.uuy8UBqvqWb_vIBDcJQNeYN0GL4)localObject4).<init>(localc, (Uri)localObject1, bool3);
      localObject1 = ((com.truecaller.notifications.l)localObject2).a((z.d)localObject3, (l.a)localObject4);
      localObject2 = c;
      localObject3 = b;
      i1 = a;
      ((com.truecaller.notifications.a)localObject2).a((String)localObject3, i1, (Notification)localObject1, (String)localObject9);
      i1 = 1;
      i6 = i9 + 1;
      i3 = i8;
      if (i6 >= i8) {
        break;
      }
      i1 = i8;
      localObject2 = localObject10;
      i3 = 0;
      localObject1 = null;
      i5 = 1;
    }
  }
  
  private boolean a(Message paramMessage, boolean paramBoolean1, boolean paramBoolean2)
  {
    Object localObject1 = r;
    Object localObject2 = "featureSmartNotifications";
    boolean bool1 = ((com.truecaller.common.g.a)localObject1).b((String)localObject2);
    if (bool1)
    {
      localObject2 = x;
      String str1 = com.truecaller.common.h.ab.c(c.e);
      String str2 = paramMessage.j();
      com.truecaller.smsparser.j localj = new com/truecaller/smsparser/j;
      localObject1 = n;
      com.truecaller.truepay.d locald = z;
      localj.<init>(paramMessage, (com.truecaller.notificationchannels.j)localObject1, locald);
      paramMessage = e;
      long l1 = a;
      boolean bool2 = ((com.truecaller.smsparser.a)localObject2).a(str1, str2, localj, paramBoolean1, paramBoolean2, l1);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  private boolean a(List paramList, boolean paramBoolean)
  {
    c localc = this;
    Object localObject1 = new java/util/ArrayList;
    Object localObject2 = paramList;
    ((ArrayList)localObject1).<init>(paramList);
    localObject2 = r;
    Object localObject3 = "featureOTPNotificationEnabled";
    boolean bool1 = ((com.truecaller.common.g.a)localObject2).b((String)localObject3);
    if (bool1)
    {
      bool1 = ((List)localObject1).isEmpty();
      if (!bool1)
      {
        localObject2 = -..Lambda.c.eYWY_gW3CluitrPtrGN0Ld2bYRo.INSTANCE;
        Collections.sort((List)localObject1, (Comparator)localObject2);
        int i1 = ((List)localObject1).size();
        int i2 = 1;
        i1 -= i2;
        localObject1 = ((List)localObject1).get(i1);
        Object localObject4 = localObject1;
        localObject4 = (Message)localObject1;
        boolean bool2 = d((Message)localObject4);
        if (bool2)
        {
          new String[1][0] = "mayBeHandleOtpMessage:: Message from phonebook contact we don't have to parse";
          return false;
        }
        localObject1 = n;
        i1 = localObject1.length;
        int i4 = 0;
        localObject3 = null;
        int i5 = 0;
        Object localObject5 = null;
        while (i4 < i1)
        {
          localObject6 = localObject1[i4];
          boolean bool3 = ((Entity)localObject6).a();
          if (bool3) {
            i5 = 1;
          }
          i4 += 1;
        }
        if (i5 == 0)
        {
          new String[1][0] = "mayBeHandleOtpMessage:: Has no text entity";
          return false;
        }
        localObject1 = Collections.singletonList(localObject4);
        localObject1 = localc.a((List)localObject1);
        localObject2 = c.f;
        localObject1 = ((Map)localObject1).get(localObject2);
        Object localObject7 = localObject1;
        localObject7 = (Participant)localObject1;
        localObject1 = ((Message)localObject4).j();
        localObject2 = s;
        localObject1 = ((y)localObject2).a((String)localObject1);
        if (localObject1 == null)
        {
          new String[1][0] = "mayBeHandleOtpMessage:: No otp found";
          return false;
        }
        Object localObject8 = c;
        NotificationIdentifier localNotificationIdentifier = new com/truecaller/messaging/notifications/NotificationIdentifier;
        int i3 = 2131363804;
        localNotificationIdentifier.<init>(i3);
        localObject1 = a;
        long l1 = a;
        localObject2 = x.b((Context)localObject1, l1, localNotificationIdentifier);
        i4 = 134217728;
        PendingIntent localPendingIntent = PendingIntent.getService((Context)localObject1, 0, (Intent)localObject2, i4);
        Object localObject9;
        if (paramBoolean)
        {
          localObject1 = a;
          localObject2 = Collections.singletonList(localObject4);
          localObject1 = NotificationBroadcastReceiver.b((Context)localObject1, (List)localObject2, localNotificationIdentifier);
          localObject2 = a;
          long l2 = a;
          localObject5 = x.a((Context)localObject2, l2, localNotificationIdentifier);
          localObject2 = PendingIntent.getService((Context)localObject2, 0, (Intent)localObject5, i4);
          localObject9 = localObject1;
          localObject10 = localObject2;
        }
        else
        {
          localObject1 = NotificationBroadcastReceiver.a(a, localNotificationIdentifier, false);
          localObject2 = NotificationBroadcastReceiver.a(a, localNotificationIdentifier, false);
          localObject9 = localObject1;
          localObject10 = localObject2;
        }
        Object localObject6 = n.d();
        Object localObject11 = new android/support/v4/app/z$d;
        localObject1 = a;
        ((z.d)localObject11).<init>((Context)localObject1, (String)localObject6);
        long l3 = a;
        String str = ((Participant)localObject7).a();
        localObject1 = this;
        localObject3 = localObject8;
        localObject5 = localNotificationIdentifier;
        Object localObject12 = localObject11;
        long l4 = l3;
        Object localObject13 = localObject10;
        Object localObject10 = str;
        localObject10 = a(2131559087, (String)localObject8, localNotificationIdentifier, l3, str);
        l4 = a;
        str = ((Participant)localObject7).a();
        localObject8 = localObject10;
        localObject10 = str;
        localObject1 = a(2131559088, (String)localObject3, localNotificationIdentifier, l4, str);
        localObject2 = new android/support/v4/app/z$d;
        localObject3 = a;
        ((z.d)localObject2).<init>((Context)localObject3, (String)localObject6);
        localObject3 = i;
        localObject6 = new Object[i2];
        localObject11 = ((Participant)localObject7).a();
        localObject6[0] = localObject11;
        localObject3 = ((com.truecaller.utils.n)localObject3).a(2131888473, (Object[])localObject6);
        localObject2 = ((z.d)localObject2).a((CharSequence)localObject3);
        localObject3 = i;
        localObject6 = new Object[0];
        localObject3 = ((com.truecaller.utils.n)localObject3).a(2131888472, (Object[])localObject6);
        localObject2 = ((z.d)localObject2).b((CharSequence)localObject3).h();
        localObject3 = localObject12;
        localc.a((z.d)localObject12);
        localObject5 = new android/support/v4/app/z$e;
        ((z.e)localObject5).<init>();
        localObject5 = ((z.d)localObject12).a((z.g)localObject5);
        G = ((RemoteViews)localObject8);
        F = ((RemoteViews)localObject1);
        H = ((RemoteViews)localObject1);
        f = ((PendingIntent)localObject9);
        D = 0;
        E = ((Notification)localObject2);
        l = 2;
        localObject2 = a.getString(2131886651);
        localObject6 = localObject13;
        localObject1 = ((z.d)localObject5).a(2131234063, (CharSequence)localObject2, (PendingIntent)localObject13);
        localObject5 = a.getString(2131887927);
        ((z.d)localObject1).a(2131234000, (CharSequence)localObject5, localPendingIntent).d(16);
        localc.b((z.d)localObject12);
        localObject1 = ((z.d)localObject12).h();
        localObject2 = c;
        i4 = a;
        ((com.truecaller.notifications.a)localObject2).a(i4, (Notification)localObject1, "notificationOtpMessage");
        localObject1 = y;
        l1 = a;
        ((com.truecaller.smsparser.b.d)localObject1).a(l1);
        return i2;
      }
    }
    new String[1][0] = "mayBeHandleOtpMessage:: Either feature flag is off or messages empty, returning";
    return false;
  }
  
  private Uri b(List paramList)
  {
    String str1 = a.getString(2131888543);
    paramList = paramList.iterator();
    boolean bool1 = false;
    Uri localUri = null;
    Object localObject1 = "";
    int i1 = 0;
    float f1 = 0.0F;
    Matcher localMatcher = null;
    for (;;)
    {
      boolean bool2 = paramList.hasNext();
      if (!bool2) {
        break;
      }
      localObject1 = (Message)paramList.next();
      String str2 = ((Message)localObject1).j();
      localObject1 = n;
      int i2 = localObject1.length;
      int i3 = i1;
      float f2 = f1;
      i1 = 0;
      f1 = 0.0F;
      localMatcher = null;
      while (i1 < i2)
      {
        Object localObject2 = localObject1[i1];
        boolean bool3 = ((Entity)localObject2).a();
        if (bool3)
        {
          localObject2 = (TextEntity)localObject2;
          Object localObject3 = a;
          bool3 = am.c((CharSequence)localObject3);
          if (bool3)
          {
            localObject2 = a;
            localObject3 = Locale.getDefault();
            localObject2 = ((String)localObject2).toLowerCase((Locale)localObject3);
            boolean bool4 = ((String)localObject2).contains(str1);
            if (bool4)
            {
              i3 = 1;
              f2 = Float.MIN_VALUE;
            }
          }
        }
        i1 += 1;
      }
      localObject1 = str2;
      i1 = i3;
      f1 = f2;
    }
    paramList = null;
    if (i1 != 0)
    {
      localMatcher = Patterns.WEB_URL.matcher((CharSequence)localObject1);
      boolean bool5;
      do
      {
        bool1 = localMatcher.find();
        if (!bool1) {
          break;
        }
        localUri = Uri.parse(localMatcher.group());
        localObject1 = localMatcher.group();
        bool5 = ((String)localObject1).contains(str1);
      } while (!bool5);
      paramList = localUri;
    }
    return paramList;
  }
  
  private android.support.v4.app.z.a b(List paramList, NotificationIdentifier paramNotificationIdentifier)
  {
    Participant localParticipant = get0c;
    int i1 = c;
    if (i1 == 0) {
      localObject1 = "PHONE_NUMBER";
    } else {
      localObject1 = "OTHER";
    }
    Object localObject2 = localObject1;
    Object localObject1 = new android/support/v4/app/z$a;
    String str1 = a.getString(2131886772);
    Context localContext = a;
    String str2 = f;
    String str3 = m;
    paramList = NotificationBroadcastReceiver.a(localContext, paramList, str2, (String)localObject2, str3, paramNotificationIdentifier);
    ((android.support.v4.app.z.a)localObject1).<init>(2131233887, str1, paramList);
    return (android.support.v4.app.z.a)localObject1;
  }
  
  private z.g b(Map paramMap1, Map paramMap2)
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Object localObject2 = paramMap1.entrySet().iterator();
    boolean bool = ((Iterator)localObject2).hasNext();
    Object localObject3;
    Object localObject5;
    if (bool)
    {
      localObject3 = (Map.Entry)((Iterator)localObject2).next();
      localIterator = ((List)((Map.Entry)localObject3).getValue()).iterator();
      for (;;)
      {
        i2 = localIterator.hasNext();
        if (i2 == 0) {
          break;
        }
        localObject4 = (Message)localIterator.next();
        localObject5 = ((Map.Entry)localObject3).getKey();
        localObject4 = Pair.create(localObject4, localObject5);
        ((List)localObject1).add(localObject4);
      }
    }
    localObject2 = -..Lambda.c.v84qnbk_rL0LBijXJw1V2-y66pQ.INSTANCE;
    Collections.sort((List)localObject1, (Comparator)localObject2);
    localObject2 = new android/support/v4/app/z$f;
    ((z.f)localObject2).<init>();
    int i1 = Math.min(((List)localObject1).size(), 5);
    Iterator localIterator = null;
    int i2 = 0;
    Object localObject4 = null;
    int i4;
    for (;;)
    {
      i4 = 1;
      if (i2 >= i1) {
        break;
      }
      Message localMessage = (Message)getfirst;
      Conversation localConversation = (Conversation)getsecond;
      int i5 = paramMap1.size();
      if (i5 == i4)
      {
        i4 = 0;
        localObject5 = null;
      }
      localObject5 = a(localMessage, localConversation, paramMap2, i4);
      ((z.f)localObject2).c((CharSequence)localObject5);
      int i3;
      i2 += 1;
    }
    int i6 = ((List)localObject1).size();
    if (i6 > i4)
    {
      paramMap1 = i;
      int i7 = 2131886767;
      localObject3 = new Object[i4];
      int i8 = ((List)localObject1).size();
      localObject1 = Integer.valueOf(i8);
      localObject3[0] = localObject1;
      paramMap1 = paramMap1.a(i7, (Object[])localObject3);
      ((z.f)localObject2).b(paramMap1);
    }
    return (z.g)localObject2;
  }
  
  private String b(Conversation paramConversation, Map paramMap)
  {
    Object localObject = x;
    if (localObject != null) {
      return am.n(x.b);
    }
    localObject = l;
    -..Lambda.c.glVnJ6DS4ziZUaEaR1mHPR3zBlo localglVnJ6DS4ziZUaEaR1mHPR3zBlo = new com/truecaller/messaging/notifications/-$$Lambda$c$glVnJ6DS4ziZUaEaR1mHPR3zBlo;
    localglVnJ6DS4ziZUaEaR1mHPR3zBlo.<init>(this, paramMap);
    return c.a.f.a((Object[])localObject, ", ", "", "", -1, "", localglVnJ6DS4ziZUaEaR1mHPR3zBlo);
  }
  
  private void b(z.d paramd)
  {
    Object localObject = d;
    int i1 = ((com.truecaller.util.al)localObject).g();
    h localh = j;
    boolean bool = localh.s();
    if ((bool) && (i1 != 0))
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      localObject = null;
    }
    if (i1 != 0) {
      i1 = 6;
    } else {
      i1 = 4;
    }
    paramd.c(i1);
    localObject = f.a();
    paramd.a((Uri)localObject);
  }
  
  private void b(Map paramMap)
  {
    c localc = this;
    Object localObject1 = paramMap;
    Object localObject2 = c(paramMap);
    Object localObject3 = -..Lambda.c.63PzRZJp3i8IUw1QKmo2Xj6v_TM.INSTANCE;
    Collections.sort((List)localObject2, (Comparator)localObject3);
    localObject3 = a((List)localObject2);
    Object localObject4 = q;
    if (localObject4 == null) {
      localObject4 = g();
    }
    Object localObject5 = ((List)localObject2).iterator();
    boolean bool1;
    boolean bool3;
    do
    {
      do
      {
        bool1 = ((Iterator)localObject5).hasNext();
        bool2 = false;
        localObject6 = null;
        i3 = 1;
        if (!bool1) {
          break;
        }
        localObject7 = (Message)((Iterator)localObject5).next();
        long l1 = a;
        localObject8 = Long.valueOf(l1);
        bool3 = ((Set)localObject4).contains(localObject8);
      } while (bool3);
      localObject8 = w.w();
      bool3 = ((com.truecaller.featuretoggles.b)localObject8).a();
      if (bool3) {
        break;
      }
      localObject7 = a((Message)localObject7, (Map)localObject3);
      bool1 = ((Participant)localObject7).g();
    } while (bool1);
    boolean bool4 = true;
    break label177;
    bool4 = false;
    localObject5 = null;
    label177:
    Object localObject7 = (Message)((List)localObject2).get(0);
    Object localObject8 = (Conversation)paramMap.keySet().iterator().next();
    Participant localParticipant = a((Message)localObject7, (Map)localObject3);
    com.truecaller.featuretoggles.b localb = w.w();
    int i6 = localb.a();
    if (i6 == 0)
    {
      i6 = localParticipant.g();
      if (i6 != 0)
      {
        i6 = 1;
        f1 = Float.MIN_VALUE;
        break label274;
      }
    }
    i6 = 0;
    float f1 = 0.0F;
    localb = null;
    label274:
    String str1;
    if (i6 != 0) {
      str1 = n.a();
    } else {
      str1 = n.d();
    }
    z.d locald = new android/support/v4/app/z$d;
    Object localObject9 = a;
    locald.<init>((Context)localObject9, str1);
    localc.a(locald);
    localObject9 = new java/lang/StringBuilder;
    ((StringBuilder)localObject9).<init>();
    Object localObject10 = localc.b((Conversation)localObject8, (Map)localObject3);
    ((StringBuilder)localObject9).append((String)localObject10);
    int i10;
    if (i6 != 0)
    {
      ((StringBuilder)localObject9).append(' ');
      i8 = 40;
      ((StringBuilder)localObject9).append(i8);
      localObject10 = a;
      localObject6 = new Object[i3];
      localObject11 = Integer.valueOf(q);
      i10 = 0;
      localObject6[0] = localObject11;
      i3 = 2131886146;
      localObject6 = ((Context)localObject10).getString(i3, (Object[])localObject6);
      ((StringBuilder)localObject9).append((String)localObject6);
      localObject6 = ")";
      ((StringBuilder)localObject9).append((String)localObject6);
    }
    long l2 = e.a;
    locald.a(l2);
    Object localObject6 = localc.a((Conversation)localObject8, (Map)localObject3);
    int i3 = ((List)localObject2).size();
    int i8 = 1;
    Object localObject12;
    if (i3 > i8)
    {
      i3 = paramMap.size();
      if (i3 > i8)
      {
        localObject12 = paramMap.keySet();
        String str2 = ", ";
        String str3 = "";
        String str4 = "";
        int i11 = -1;
        String str5 = "";
        localObject11 = new com/truecaller/messaging/notifications/-$$Lambda$c$QP2mlYWLu6w_cfuttbUkvW7midY;
        ((-..Lambda.c.QP2mlYWLu6w_cfuttbUkvW7midY)localObject11).<init>(localc, (Map)localObject3);
        localObject11 = m.a((Iterable)localObject12, str2, str3, str4, i11, str5, (c.g.a.b)localObject11);
        localObject11 = locald.a((CharSequence)localObject11);
        localObject8 = a;
        localObject9 = new Object[1];
        localObject10 = Integer.valueOf(((List)localObject2).size());
        i10 = 0;
        localObject9[0] = localObject10;
        i9 = 2131886767;
        localObject8 = ((Context)localObject8).getString(i9, (Object[])localObject9);
        localObject11 = ((z.d)localObject11).b((CharSequence)localObject8);
        localObject8 = at.a(android.support.v4.content.b.a(a, 2131233849));
        ((z.d)localObject11).a((Bitmap)localObject8);
        i12 = 0;
        localObject9 = null;
        break label718;
      }
    }
    Object localObject11 = ((StringBuilder)localObject9).toString();
    localObject11 = locald.a((CharSequence)localObject11);
    int i12 = 0;
    localObject9 = null;
    localObject8 = localc.a((Message)localObject7, (Conversation)localObject8, (Map)localObject3, false);
    ((z.d)localObject11).b((CharSequence)localObject8);
    label718:
    localObject11 = localc.b((Map)localObject1, (Map)localObject3);
    locald.a((z.g)localObject11);
    localObject11 = new com/truecaller/messaging/notifications/NotificationIdentifier;
    localObject8 = String.valueOf(b);
    long l3 = b;
    int i9 = (int)l3;
    i12 = 2131363802;
    ((NotificationIdentifier)localObject11).<init>(i12, (String)localObject8, i9);
    localObject7 = Collections.singletonList(localObject7);
    localObject7 = localc.b((List)localObject7);
    if (localObject7 != null)
    {
      bool3 = true;
    }
    else
    {
      bool3 = false;
      localObject8 = null;
    }
    localObject10 = z;
    localObject9 = com.truecaller.common.h.ab.c(e);
    localObject9 = ((com.truecaller.truepay.d)localObject10).a((String)localObject9);
    localObject10 = w.n();
    boolean bool6 = ((com.truecaller.featuretoggles.b)localObject10).a();
    if ((bool6) && (localObject9 != null))
    {
      localObject10 = ((SenderInfo)localObject9).getCategory();
      localObject12 = localObject6;
      localObject6 = "bank";
      bool2 = ((String)localObject10).equals(localObject6);
      if (bool2)
      {
        bool2 = true;
        break label911;
      }
    }
    else
    {
      localObject12 = localObject6;
    }
    boolean bool2 = false;
    localObject6 = null;
    label911:
    localObject10 = localc.a((List)localObject2, (NotificationIdentifier)localObject11);
    locald.a((android.support.v4.app.z.a)localObject10);
    localObject10 = localc.b((List)localObject2, (NotificationIdentifier)localObject11);
    int i13;
    if (i6 != 0)
    {
      i13 = i6;
      i7 = ((Map)localObject3).size();
      i14 = 1;
      if (i7 == i14)
      {
        if (bool2)
        {
          localObject7 = ((SenderInfo)localObject9).getSymbol();
          localObject7 = localc.a((String)localObject7, (NotificationIdentifier)localObject11, (List)localObject2);
          locald.a((android.support.v4.app.z.a)localObject7);
          break label1137;
        }
        locald.a((android.support.v4.app.z.a)localObject10);
        break label1137;
      }
    }
    else
    {
      i13 = i7;
      i14 = 1;
    }
    int i7 = paramMap.size();
    if (i7 == i14) {
      if (bool2)
      {
        localObject7 = ((SenderInfo)localObject9).getSymbol();
        localObject7 = localc.a((String)localObject7, (NotificationIdentifier)localObject11, (List)localObject2);
        locald.a((android.support.v4.app.z.a)localObject7);
      }
      else
      {
        i2 = c;
        if (i2 == i14)
        {
          locald.a((android.support.v4.app.z.a)localObject10);
        }
        else if (bool3)
        {
          localObject1 = localc.a((List)localObject2, (Uri)localObject7);
          locald.a((android.support.v4.app.z.a)localObject1);
        }
        else
        {
          localObject1 = localc.a((List)localObject2, (NotificationIdentifier)localObject11, str1);
          locald.a((android.support.v4.app.z.a)localObject1);
        }
      }
    }
    label1137:
    int i14 = 0;
    localObject1 = null;
    if (bool4)
    {
      localc.b(locald);
      localObject1 = "notificationIncomingMessage";
    }
    localObject7 = NotificationBroadcastReceiver.a(a, (List)localObject2, (NotificationIdentifier)localObject11);
    locald.b((PendingIntent)localObject7);
    localObject7 = NotificationBroadcastReceiver.b(a, (List)localObject2, (NotificationIdentifier)localObject11);
    f = ((PendingIntent)localObject7);
    int i1 = Build.VERSION.SDK_INT;
    int i2 = 21;
    if ((i1 >= i2) && (bool4))
    {
      bool4 = C;
      if (!bool4)
      {
        i4 = 2;
        l = i4;
      }
    }
    int i4 = Build.VERSION.SDK_INT;
    i1 = 24;
    if (i4 >= i1)
    {
      localObject2 = ((List)localObject2).iterator();
      boolean bool5;
      do
      {
        bool5 = ((Iterator)localObject2).hasNext();
        if (!bool5) {
          break;
        }
        long l4 = nexta;
        localObject5 = Long.valueOf(l4);
        bool5 = ((Set)localObject4).contains(localObject5);
      } while (bool5);
      i10 = 1;
      break label1326;
      i10 = 0;
      label1326:
      if (i10 != 0)
      {
        localObject2 = paramMap.entrySet();
        int i15 = ((Set)localObject2).size();
        int i16 = 8;
        if (i15 > i16)
        {
          localObject2 = c;
          i16 = 2131363802;
          ((com.truecaller.notifications.a)localObject2).a(i16);
          localObject2 = paramMap;
        }
        else
        {
          localObject2 = paramMap;
        }
        localc.a((Map)localObject2, (Map)localObject3);
        u = "notificationGroupNewMessages";
        i15 = paramMap.size();
        k = i15;
        i15 = 1;
        v = i15;
        localObject2 = b;
        localObject3 = new com/truecaller/messaging/notifications/-$$Lambda$c$icf079ZWEysZuRMYGCZeOinH_-k;
        localObject4 = localObject12;
        i2 = i13;
        ((-..Lambda.c.icf079ZWEysZuRMYGCZeOinH_-k)localObject3).<init>(localc, (Uri)localObject12, i13);
        localObject2 = ((com.truecaller.notifications.l)localObject2).a(locald, (l.a)localObject3);
        localObject3 = c;
        int i5 = 2131363802;
        ((com.truecaller.notifications.a)localObject3).a(i5, (Notification)localObject2, (String)localObject1);
      }
      return;
    }
    localObject4 = localObject12;
    i2 = i13;
    localObject2 = b;
    localObject3 = new com/truecaller/messaging/notifications/-$$Lambda$c$nyo57IeR80zUM2yjMOTrkeujZ2M;
    ((-..Lambda.c.nyo57IeR80zUM2yjMOTrkeujZ2M)localObject3).<init>(localc, (Uri)localObject12, i13);
    localObject2 = ((com.truecaller.notifications.l)localObject2).a(locald, (l.a)localObject3);
    c.a(2131363802, (Notification)localObject2, (String)localObject1);
  }
  
  private static List c(Map paramMap)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    paramMap = paramMap.values().iterator();
    for (;;)
    {
      boolean bool = paramMap.hasNext();
      if (!bool) {
        break;
      }
      List localList = (List)paramMap.next();
      localArrayList.addAll(localList);
    }
    return localArrayList;
  }
  
  private boolean d(Message paramMessage)
  {
    Context localContext = a;
    paramMessage = c.f;
    return com.truecaller.search.f.a(localContext, paramMessage);
  }
  
  /* Error */
  private Set g()
  {
    // Byte code:
    //   0: new 1354	java/util/HashSet
    //   3: astore_1
    //   4: aload_1
    //   5: invokespecial 1355	java/util/HashSet:<init>	()V
    //   8: aload_0
    //   9: getfield 76	com/truecaller/messaging/notifications/c:a	Landroid/content/Context;
    //   12: astore_2
    //   13: ldc_w 788
    //   16: astore_3
    //   17: aload_2
    //   18: aload_3
    //   19: invokevirtual 1359	android/content/Context:openFileInput	(Ljava/lang/String;)Ljava/io/FileInputStream;
    //   22: astore_2
    //   23: new 1361	java/io/DataInputStream
    //   26: astore_3
    //   27: aload_3
    //   28: aload_2
    //   29: invokespecial 1364	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
    //   32: aload_3
    //   33: invokevirtual 1367	java/io/DataInputStream:readLong	()J
    //   36: lstore 4
    //   38: lload 4
    //   40: invokestatic 1299	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   43: astore 6
    //   45: aload_1
    //   46: aload 6
    //   48: invokeinterface 1368 2 0
    //   53: pop
    //   54: goto -22 -> 32
    //   57: astore_3
    //   58: aload_2
    //   59: invokestatic 815	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   62: aload_3
    //   63: athrow
    //   64: aload_2
    //   65: invokestatic 815	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   68: aload_1
    //   69: invokestatic 1372	java/util/Collections:unmodifiableSet	(Ljava/util/Set;)Ljava/util/Set;
    //   72: areturn
    //   73: pop
    //   74: goto -10 -> 64
    //   77: pop
    //   78: goto -10 -> 68
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	81	0	this	c
    //   3	66	1	localHashSet	HashSet
    //   12	53	2	localObject1	Object
    //   16	17	3	localObject2	Object
    //   57	6	3	localObject3	Object
    //   36	3	4	l1	long
    //   43	4	6	localLong	Long
    //   73	1	7	localEOFException	java.io.EOFException
    //   77	1	8	localIOException	IOException
    // Exception table:
    //   from	to	target	type
    //   23	26	57	finally
    //   28	32	57	finally
    //   32	36	57	finally
    //   38	43	57	finally
    //   46	54	57	finally
    //   23	26	73	java/io/EOFException
    //   28	32	73	java/io/EOFException
    //   32	36	73	java/io/EOFException
    //   38	43	73	java/io/EOFException
    //   46	54	73	java/io/EOFException
    //   8	12	77	java/io/IOException
    //   18	22	77	java/io/IOException
    //   58	62	77	java/io/IOException
    //   62	64	77	java/io/IOException
    //   64	68	77	java/io/IOException
  }
  
  public final void a()
  {
    C = true;
  }
  
  public final void a(int paramInt)
  {
    com.truecaller.notifications.ae localae = k;
    Context localContext = a;
    localae.a(localContext, MessagesActivity.class, paramInt);
  }
  
  public final void a(long paramLong)
  {
    B = paramLong;
  }
  
  public final void a(ImGroupInfo paramImGroupInfo)
  {
    Object localObject1 = o.a();
    Object localObject2 = new android/support/v4/app/z$d;
    Object localObject3 = a;
    ((z.d)localObject2).<init>((Context)localObject3, (String)localObject1);
    localObject1 = b;
    localObject1 = ((z.d)localObject2).a((CharSequence)localObject1);
    localObject2 = a.getString(2131886606);
    localObject1 = ((z.d)localObject1).b((CharSequence)localObject2);
    localObject2 = new com/truecaller/messaging/notifications/NotificationIdentifier;
    localObject3 = a;
    Object localObject4 = a;
    int i1 = ((String)localObject4).hashCode();
    int i2 = 2131363316;
    ((NotificationIdentifier)localObject2).<init>(i2, (String)localObject3, i1);
    localObject3 = NotificationBroadcastReceiver.a(a, (NotificationIdentifier)localObject2, paramImGroupInfo);
    f = ((PendingIntent)localObject3);
    a((z.d)localObject1);
    localObject3 = c;
    boolean bool = am.b((CharSequence)localObject3);
    if (bool)
    {
      bool = false;
      localObject3 = null;
    }
    else
    {
      localObject3 = Uri.parse(c);
    }
    localObject4 = b;
    -..Lambda.c.CLu4ddU0EYy_HjyAEGZ4PxO378w localCLu4ddU0EYy_HjyAEGZ4PxO378w = new com/truecaller/messaging/notifications/-$$Lambda$c$CLu4ddU0EYy_HjyAEGZ4PxO378w;
    localCLu4ddU0EYy_HjyAEGZ4PxO378w.<init>(this, (Uri)localObject3);
    localObject1 = ((com.truecaller.notifications.l)localObject4).a((z.d)localObject1, localCLu4ddU0EYy_HjyAEGZ4PxO378w);
    localObject3 = c;
    paramImGroupInfo = a;
    int i3 = a;
    ((com.truecaller.notifications.a)localObject3).a(paramImGroupInfo, i3, (Notification)localObject1, "notificationImGroupInvitation");
  }
  
  public final void a(Message paramMessage)
  {
    Object localObject1 = d;
    boolean bool1 = ((com.truecaller.util.al)localObject1).a();
    if (!bool1) {
      return;
    }
    localObject1 = r;
    int i2 = 1;
    bool1 = ((com.truecaller.common.g.a)localObject1).a("smart_notifications", i2);
    int i3 = 0;
    if (bool1)
    {
      localObject2 = Collections.singletonList(paramMessage);
      boolean bool3 = a((List)localObject2, false);
      if (bool3) {
        return;
      }
    }
    Object localObject2 = q;
    if (localObject2 == null) {
      localObject2 = g();
    }
    if (bool1)
    {
      localObject1 = r;
      String str = "smart_notifications_clicked";
      bool1 = ((com.truecaller.common.g.a)localObject1).a(str, false);
      a(paramMessage, false, bool1);
    }
    long l1 = a;
    localObject1 = Long.valueOf(l1);
    bool1 = ((Set)localObject2).contains(localObject1);
    if (bool1) {
      return;
    }
    long l2 = TimeUnit.DAYS.toMillis(2);
    localObject1 = new org/a/a/b;
    ((org.a.a.b)localObject1).<init>();
    localObject1 = ((org.a.a.b)localObject1).az_();
    Object localObject3 = r.a();
    org.a.a.a locala = b;
    long l3 = a;
    long l4 = locala.b((aa)localObject3, l3);
    localObject1 = ((org.a.a.b)localObject1).a_(l4);
    l4 = org.a.a.e.a();
    bool1 = ((org.a.a.a.c)localObject1).d(l4);
    if (bool1)
    {
      bool1 = d(paramMessage);
      if (!bool1)
      {
        localObject1 = "featurePromoIncomingMsgCount";
        int i1 = com.truecaller.common.b.e.a((String)localObject1, 0);
        localObject3 = j;
        int i4 = ((h)localObject3).w();
        if (i4 < i1)
        {
          i1 = 1;
        }
        else
        {
          i1 = 0;
          localObject1 = null;
        }
        if (i1 != 0)
        {
          localObject1 = j.g().b(l2);
          l2 = org.a.a.e.a();
          boolean bool2 = ((org.a.a.a.c)localObject1).d(l2);
          if (bool2)
          {
            localObject1 = Collections.singletonList(paramMessage);
            localObject1 = a((List)localObject1);
            localObject2 = m;
            paramMessage = a(paramMessage, (Map)localObject1);
            localObject1 = o.a();
            paramMessage = ((e)localObject2).a(paramMessage, (String)localObject1);
            if (paramMessage != null)
            {
              localObject1 = c;
              localObject2 = "notificationIncomingMessagePromo";
              ((com.truecaller.notifications.a)localObject1).a(2131363759, paramMessage, (String)localObject2);
              i3 = 1;
            }
            if (i3 != 0)
            {
              int i5 = j.w();
              localObject1 = j;
              int i6;
              i5 += i2;
              ((h)localObject1).g(i6);
              paramMessage = j;
              localObject1 = org.a.a.b.ay_();
              paramMessage.a((org.a.a.b)localObject1);
            }
          }
        }
      }
    }
  }
  
  public final void a(Map paramMap)
  {
    c localc = this;
    Object localObject1 = c(paramMap);
    boolean bool1 = true;
    boolean bool2 = a((List)localObject1, bool1);
    if (bool2) {
      return;
    }
    localObject1 = new java/util/HashSet;
    ((HashSet)localObject1).<init>();
    Object localObject2 = new java/util/HashMap;
    ((HashMap)localObject2).<init>();
    Object localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    com.truecaller.common.g.a locala = r;
    boolean bool3 = locala.a("smart_notifications", bool1);
    Object localObject4 = paramMap.entrySet().iterator();
    boolean bool4 = ((Iterator)localObject4).hasNext();
    Object localObject5;
    Object localObject7;
    if (bool4)
    {
      localObject5 = (Map.Entry)((Iterator)localObject4).next();
      Conversation localConversation = (Conversation)((Map.Entry)localObject5).getKey();
      localObject5 = ((List)((Map.Entry)localObject5).getValue()).iterator();
      for (;;)
      {
        boolean bool5 = ((Iterator)localObject5).hasNext();
        if (!bool5) {
          break;
        }
        Object localObject6 = (Message)((Iterator)localObject5).next();
        long l1 = B;
        long l2 = b;
        boolean bool6 = l1 < l2;
        if (!bool6)
        {
          l1 = a;
          localObject7 = Long.valueOf(l1);
          ((Set)localObject1).add(localObject7);
          localArrayList.add(localObject6);
        }
        else
        {
          if (bool3)
          {
            localObject7 = r;
            localObject8 = "smart_notifications_clicked";
            bool7 = ((com.truecaller.common.g.a)localObject7).a((String)localObject8, false);
            bool7 = localc.a((Message)localObject6, bool1, bool7);
            if (bool7)
            {
              localObject7 = y;
              long l3 = a;
              ((com.truecaller.smsparser.b.d)localObject7).a(l3);
              continue;
            }
          }
          localObject7 = c;
          Object localObject8 = h;
          boolean bool8 = ((p)localObject8).g();
          if (bool8)
          {
            localObject8 = w.w();
            bool8 = ((com.truecaller.featuretoggles.b)localObject8).a();
            if (!bool8)
            {
              bool8 = true;
              break label370;
            }
          }
          bool8 = false;
          localObject8 = null;
          label370:
          boolean bool7 = ((Participant)localObject7).a(bool8);
          if (bool7)
          {
            ((List)localObject3).add(localObject6);
          }
          else
          {
            localObject7 = (List)((Map)localObject2).get(localConversation);
            if (localObject7 == null)
            {
              localObject7 = new java/util/ArrayList;
              ((ArrayList)localObject7).<init>();
              ((Map)localObject2).put(localConversation, localObject7);
            }
            ((List)localObject7).add(localObject6);
          }
          long l4 = a;
          localObject6 = Long.valueOf(l4);
          ((Set)localObject1).add(localObject6);
        }
      }
    }
    bool3 = ((Map)localObject2).isEmpty();
    if (bool3)
    {
      localObject2 = c;
      i1 = 2131363802;
      ((com.truecaller.notifications.a)localObject2).a(i1);
    }
    else
    {
      localc.b((Map)localObject2);
    }
    boolean bool9 = ((List)localObject3).isEmpty();
    int i1 = 2131363798;
    if (bool9)
    {
      localObject9 = c;
      ((com.truecaller.notifications.a)localObject9).a(i1);
    }
    else
    {
      localObject2 = "inSpammerList";
      localObject4 = "spamNotification";
      localc.a((List)localObject3, (String)localObject2, (String)localObject4);
      bool9 = D;
      if (!bool9)
      {
        localObject2 = j;
        bool9 = ((h)localObject2).i();
        if (bool9)
        {
          localObject2 = n.g();
          localObject4 = new android/support/v4/app/z$d;
          localObject5 = a;
          ((z.d)localObject4).<init>((Context)localObject5, (String)localObject2);
          int i5 = 2131234318;
          localObject2 = ((z.d)localObject4).a(i5);
          int i2 = android.support.v4.content.b.c(a, 2131099685);
          C = i2;
          localObject5 = a.getResources();
          int i6 = 2131755021;
          int i3 = ((List)localObject3).size();
          localObject9 = new Object[bool1];
          int i4 = ((List)localObject3).size();
          localObject7 = Integer.valueOf(i4);
          localObject9[0] = localObject7;
          localObject9 = ((Resources)localObject5).getQuantityString(i6, i3, (Object[])localObject9);
          localObject9 = ((z.d)localObject2).a((CharSequence)localObject9);
          localObject2 = a.getString(2131886687);
          localObject9 = ((z.d)localObject9).b((CharSequence)localObject2);
          localObject2 = a.getResources();
          i2 = 2131234328;
          localObject2 = BitmapFactory.decodeResource((Resources)localObject2, i2);
          localObject9 = ((z.d)localObject9).a((Bitmap)localObject2);
          localObject2 = a;
          localObject5 = new com/truecaller/messaging/notifications/NotificationIdentifier;
          ((NotificationIdentifier)localObject5).<init>(i1);
          localObject2 = NotificationBroadcastReceiver.a((Context)localObject2, (List)localObject3, (NotificationIdentifier)localObject5);
          localObject9 = ((z.d)localObject9).b((PendingIntent)localObject2);
          localObject2 = NotificationBroadcastReceiver.a(a, (List)localObject3);
          f = ((PendingIntent)localObject2);
          localObject9 = c;
          localObject2 = ((z.d)localObject4).h();
          localObject3 = "notificationBlockedMessage";
          ((com.truecaller.notifications.a)localObject9).a(i1, (Notification)localObject2, (String)localObject3);
        }
      }
    }
    Object localObject9 = Collections.unmodifiableSet((Set)localObject1);
    q = ((Set)localObject9);
    localc.a((Collection)localObject1);
    bool2 = localArrayList.isEmpty();
    if (!bool2)
    {
      localObject1 = "inConversationView";
      localObject9 = "conversation";
      localc.a(localArrayList, (String)localObject1, (String)localObject9);
    }
  }
  
  public final void b()
  {
    C = false;
  }
  
  public final void b(long paramLong)
  {
    long l1 = B;
    boolean bool = l1 < paramLong;
    if (!bool)
    {
      paramLong = -100;
      B = paramLong;
    }
  }
  
  public final void b(ImGroupInfo paramImGroupInfo)
  {
    com.truecaller.notifications.a locala = c;
    paramImGroupInfo = a;
    locala.a(paramImGroupInfo, 2131363316);
  }
  
  public final void b(Message paramMessage)
  {
    long l1 = B;
    long l2 = b;
    boolean bool = l1 < l2;
    if (!bool) {
      return;
    }
    l1 = b;
    E = l1;
    Object localObject1 = new android/support/v4/app/z$d;
    Object localObject2 = a;
    Object localObject3 = o.a();
    ((z.d)localObject1).<init>((Context)localObject2, (String)localObject3);
    a((z.d)localObject1);
    localObject2 = c;
    localObject3 = a.getResources();
    Object localObject4 = ((Resources)localObject3).getString(2131886692);
    Object[] arrayOfObject = new Object[1];
    localObject2 = ((Participant)localObject2).a();
    arrayOfObject[0] = localObject2;
    localObject2 = ((Resources)localObject3).getString(2131886689, arrayOfObject);
    ((z.d)localObject1).a((CharSequence)localObject4);
    ((z.d)localObject1).b((CharSequence)localObject2);
    localObject2 = BitmapFactory.decodeResource(a.getResources(), 2131234329);
    ((z.d)localObject1).a((Bitmap)localObject2);
    localObject2 = ((Resources)localObject3).getString(2131886690);
    localObject4 = NotificationBroadcastReceiver.a(a, paramMessage);
    ((z.d)localObject1).a(2131234000, (CharSequence)localObject2, (PendingIntent)localObject4);
    localObject2 = ((Resources)localObject3).getString(2131886691);
    localObject3 = NotificationBroadcastReceiver.b(a, paramMessage);
    ((z.d)localObject1).a(2131234528, (CharSequence)localObject2, (PendingIntent)localObject3);
    localObject2 = a;
    localObject3 = Collections.singletonList(paramMessage);
    NotificationIdentifier localNotificationIdentifier = new com/truecaller/messaging/notifications/NotificationIdentifier;
    int i1 = 2131363030;
    localNotificationIdentifier.<init>(i1);
    localObject2 = NotificationBroadcastReceiver.b((Context)localObject2, (List)localObject3, localNotificationIdentifier);
    f = ((PendingIntent)localObject2);
    long l3 = e.a;
    ((z.d)localObject1).a(l3);
    ((z.d)localObject1).d(16);
    b((z.d)localObject1);
    ((z.d)localObject1).b((PendingIntent)localObject4);
    paramMessage = c;
    localObject1 = ((z.d)localObject1).h();
    paramMessage.a(i1, (Notification)localObject1, "notificationFailedMessage");
  }
  
  public final void c()
  {
    c.a(2131363759);
  }
  
  public final void c(long paramLong)
  {
    long l1 = E;
    boolean bool = paramLong < l1;
    if (!bool)
    {
      com.truecaller.notifications.a locala = c;
      int i1 = 2131363030;
      locala.a(i1);
      paramLong = -1;
      E = paramLong;
    }
  }
  
  public final void c(Message paramMessage)
  {
    Context localContext = a;
    paramMessage = ClassZeroActivity.a(localContext, paramMessage);
    localContext.startActivity(paramMessage);
  }
  
  public final void d()
  {
    Toast.makeText(a, 2131886688, 0).show();
  }
  
  public final void e()
  {
    f();
    p.a();
    Object localObject1 = o.a();
    Object localObject2 = new android/support/v4/app/z$d;
    Object localObject3 = a;
    ((z.d)localObject2).<init>((Context)localObject3, (String)localObject1);
    localObject1 = a.getString(2131886694);
    localObject1 = ((z.d)localObject2).a((CharSequence)localObject1);
    localObject2 = a.getString(2131886693);
    localObject1 = ((z.d)localObject1).b((CharSequence)localObject2);
    localObject2 = NotificationBroadcastReceiver.a(a, "notificationSMSPromoSpamOff");
    f = ((PendingIntent)localObject2);
    a((z.d)localObject1);
    localObject2 = b;
    localObject3 = new com/truecaller/messaging/notifications/-$$Lambda$c$aFsKICY9ztiDpK5ZgyNNnMdemVo;
    ((-..Lambda.c.aFsKICY9ztiDpK5ZgyNNnMdemVo)localObject3).<init>(this);
    -..Lambda.c.onXxWD0OSK_vvJxZHrVLE-EQ_tI localonXxWD0OSK_vvJxZHrVLE-EQ_tI = new com/truecaller/messaging/notifications/-$$Lambda$c$onXxWD0OSK_vvJxZHrVLE-EQ_tI;
    localonXxWD0OSK_vvJxZHrVLE-EQ_tI.<init>(this);
    localObject1 = ((com.truecaller.notifications.l)localObject2).a((z.d)localObject1, (l.a)localObject3, localonXxWD0OSK_vvJxZHrVLE-EQ_tI);
    c.a(2131364562, (Notification)localObject1, "notificationSmsPromoSpamOff");
  }
  
  public final void f()
  {
    c.a(2131364562);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */