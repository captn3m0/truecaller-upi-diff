package com.truecaller.messaging.notifications;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.RemoteInput;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipData.Item;
import android.content.ClipDescription;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.TextUtils;
import c.g.b.k;
import com.truecaller.TrueApp;
import com.truecaller.analytics.ap;
import com.truecaller.analytics.bc;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.common.h.am;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.filters.s;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.log.AssertionUtil.OnlyInDebug;
import com.truecaller.messaging.conversation.ConversationActivity;
import com.truecaller.messaging.data.o;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.f.a.a;
import com.truecaller.messaging.f.c.a;
import com.truecaller.messaging.imgroupinvitation.ImGroupInvitationActivity;
import com.truecaller.messaging.imgroupinvitation.ImGroupInvitationActivity.a;
import com.truecaller.messaging.smspermission.SmsPermissionActivity;
import com.truecaller.messaging.transport.m;
import com.truecaller.smsparser.SmartPromoActivity;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.WizardActivity;
import com.truecaller.util.al;
import dagger.a.g;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class NotificationBroadcastReceiver
  extends BroadcastReceiver
{
  private static final String b = "com.truecaller.messaging.notifications.MARK_READ";
  private static final String c = "com.truecaller.messaging.notifications.MARK_MESSAGE_READ";
  public com.truecaller.messaging.f.c a;
  
  public static PendingIntent a(Context paramContext, Message paramMessage)
  {
    paramMessage = Collections.singletonList(paramMessage);
    return a(paramContext, "com.truecaller.messaging.notifications.ERROR_DISMISSED", paramMessage);
  }
  
  public static PendingIntent a(Context paramContext, NotificationIdentifier paramNotificationIdentifier)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, NotificationBroadcastReceiver.class);
    localIntent.setAction("com.truecaller.messaging.notifications.SMART_NOTIFICATION_PROMO");
    localIntent.putExtra("EXTRA_NOTIFICATION_ID", paramNotificationIdentifier);
    localIntent.putExtra("EXTRA_SMART_NOTIFICATION", true);
    int i = c;
    return PendingIntent.getBroadcast(paramContext, i, localIntent, 134217728);
  }
  
  public static PendingIntent a(Context paramContext, NotificationIdentifier paramNotificationIdentifier, ImGroupInfo paramImGroupInfo)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, NotificationBroadcastReceiver.class);
    localIntent.setAction("com.truecaller.messaging.notifications.IM_GROUP_INVITATION");
    localIntent.putExtra("EXTRA_NOTIFICATION_ID", paramNotificationIdentifier);
    localIntent.putExtra("EXTRA_IM_GROUP_INFO", paramImGroupInfo);
    int i = c;
    return PendingIntent.getBroadcast(paramContext, i, localIntent, 134217728);
  }
  
  public static PendingIntent a(Context paramContext, NotificationIdentifier paramNotificationIdentifier, String paramString, boolean paramBoolean, List paramList)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, NotificationBroadcastReceiver.class);
    localIntent.putExtra("EXTRA_BANK_SYMBOL", paramString);
    localIntent.setAction("com.truecaller.messaging.notifications.CHECK_BALANCE");
    localIntent.putExtra("EXTRA_NOTIFICATION_ID", paramNotificationIdentifier);
    localIntent.putExtra("EXTRA_SMART_NOTIFICATION", paramBoolean);
    a(localIntent, paramList);
    int i = c;
    return PendingIntent.getBroadcast(paramContext, i, localIntent, 134217728);
  }
  
  public static PendingIntent a(Context paramContext, NotificationIdentifier paramNotificationIdentifier, boolean paramBoolean)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, NotificationBroadcastReceiver.class);
    localIntent.setAction("com.truecaller.messaging.notifications.MESSAGES");
    localIntent.putExtra("EXTRA_SMART_NOTIFICATION", paramBoolean);
    localIntent.putExtra("EXTRA_NOTIFICATION_ID", paramNotificationIdentifier);
    int i = c;
    return PendingIntent.getBroadcast(paramContext, i, localIntent, 134217728);
  }
  
  public static PendingIntent a(Context paramContext, String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, NotificationBroadcastReceiver.class);
    localIntent.setAction("com.truecaller.messaging.notifications.PROMOTION");
    localIntent.putExtra("EXTRA_ANALYTICS_CONTEXT", paramString);
    return PendingIntent.getBroadcast(paramContext, 0, localIntent, 268435456);
  }
  
  public static PendingIntent a(Context paramContext, String paramString, NotificationIdentifier paramNotificationIdentifier)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, NotificationBroadcastReceiver.class);
    localIntent.setAction("com.truecaller.messaging.notifications.RECHARGE");
    boolean bool = true;
    localIntent.putExtra("EXTRA_AUTHORIZED_ONLY", bool);
    localIntent.putExtra("EXTRA_DEEPLINK_SUFFIX", paramString);
    localIntent.putExtra("EXTRA_NOTIFICATION_ID", paramNotificationIdentifier);
    localIntent.putExtra("EXTRA_SMART_NOTIFICATION", bool);
    int i = c;
    return PendingIntent.getBroadcast(paramContext, i, localIntent, 134217728);
  }
  
  private static PendingIntent a(Context paramContext, String paramString, List paramList)
  {
    NotificationIdentifier localNotificationIdentifier = new com/truecaller/messaging/notifications/NotificationIdentifier;
    localNotificationIdentifier.<init>(0);
    return a(paramContext, paramString, paramList, false, localNotificationIdentifier, "");
  }
  
  private static PendingIntent a(Context paramContext, String paramString, List paramList, boolean paramBoolean, NotificationIdentifier paramNotificationIdentifier)
  {
    return a(paramContext, paramString, paramList, paramBoolean, paramNotificationIdentifier, "");
  }
  
  private static PendingIntent a(Context paramContext, String paramString1, List paramList, boolean paramBoolean, NotificationIdentifier paramNotificationIdentifier, String paramString2)
  {
    paramString1 = b(paramContext, paramString1, paramList, paramBoolean, paramNotificationIdentifier, paramString2);
    int i = c;
    return PendingIntent.getBroadcast(paramContext, i, paramString1, 134217728);
  }
  
  public static PendingIntent a(Context paramContext, List paramList)
  {
    return a(paramContext, "com.truecaller.messaging.notifications.VIEW_BLOCKED", paramList);
  }
  
  public static PendingIntent a(Context paramContext, List paramList, Uri paramUri)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, NotificationBroadcastReceiver.class);
    localIntent.setAction("com.truecaller.messaging.notifications.PAY");
    a(localIntent, paramList);
    localIntent.putExtra("EXTRA_AUTHORIZED_ONLY", true);
    localIntent.setData(paramUri);
    return PendingIntent.getBroadcast(paramContext, 0, localIntent, 268435456);
  }
  
  public static PendingIntent a(Context paramContext, List paramList, NotificationIdentifier paramNotificationIdentifier)
  {
    return a(paramContext, "com.truecaller.messaging.notifications.DISMISSED", paramList, false, paramNotificationIdentifier);
  }
  
  public static PendingIntent a(Context paramContext, List paramList, NotificationIdentifier paramNotificationIdentifier, String paramString)
  {
    return a(paramContext, "com.truecaller.messaging.notifications.QUICK_REPLY", paramList, true, paramNotificationIdentifier, paramString);
  }
  
  public static PendingIntent a(Context paramContext, List paramList, String paramString1, String paramString2, String paramString3, NotificationIdentifier paramNotificationIdentifier)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, NotificationBroadcastReceiver.class);
    localIntent.setAction("com.truecaller.messaging.notifications.BLOCK");
    a(localIntent, paramList);
    localIntent.putExtra("EXTRA_NORMALIZED_ADDRESS", paramString1);
    localIntent.putExtra("EXTRA_TRACKING_TYPE", paramString2);
    localIntent.putExtra("EXTRA_LABEL", paramString3);
    localIntent.putExtra("EXTRA_AUTHORIZED_ONLY", true);
    localIntent.putExtra("EXTRA_NOTIFICATION_ID", paramNotificationIdentifier);
    return PendingIntent.getBroadcast(paramContext, 0, localIntent, 134217728);
  }
  
  private static void a(long paramLong, f paramf)
  {
    long l = -1;
    boolean bool = paramLong < l;
    if (!bool)
    {
      AssertionUtil.reportWeirdnessButNeverCrash("No message id was sent.");
      return;
    }
    paramf = (t)paramf.a();
    long[] arrayOfLong = new long[1];
    arrayOfLong[0] = paramLong;
    paramf.c(arrayOfLong);
  }
  
  private static void a(Context paramContext)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.CLOSE_SYSTEM_DIALOGS");
    paramContext.sendBroadcast(localIntent);
  }
  
  private static void a(Intent paramIntent, List paramList)
  {
    int i = paramList.size();
    int j = 0;
    boolean bool;
    Message localMessage;
    if (i > 0)
    {
      bool = true;
    }
    else
    {
      bool = false;
      localMessage = null;
    }
    String[] arrayOfString = new String[0];
    AssertionUtil.AlwaysFatal.isTrue(bool, arrayOfString);
    long[] arrayOfLong = new long[i];
    paramList = paramList.iterator();
    for (;;)
    {
      bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      localMessage = (Message)paramList.next();
      int k = j + 1;
      long l = a;
      arrayOfLong[j] = l;
      j = k;
    }
    paramIntent.putExtra("EXTRA_MESSAGE_IDS", arrayOfLong);
  }
  
  private static void a(com.truecaller.analytics.b paramb, String paramString, boolean paramBoolean)
  {
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("SmartNotificationAction");
    locala.a("EventType", paramString);
    locala.a("IsDefaultApp", paramBoolean);
    paramString = locala.a();
    paramb.a(paramString);
  }
  
  public static PendingIntent b(Context paramContext, Message paramMessage)
  {
    paramMessage = Collections.singletonList(paramMessage);
    return a(paramContext, "com.truecaller.messaging.notifications.RESEND", paramMessage);
  }
  
  private static PendingIntent b(Context paramContext, String paramString, List paramList, boolean paramBoolean, NotificationIdentifier paramNotificationIdentifier)
  {
    paramString = b(paramContext, paramString, paramList, paramBoolean, paramNotificationIdentifier, "");
    paramString.putExtra("EXTRA_SMART_NOTIFICATION", true);
    int i = c;
    return PendingIntent.getBroadcast(paramContext, i, paramString, 134217728);
  }
  
  public static PendingIntent b(Context paramContext, List paramList)
  {
    return a(paramContext, "com.truecaller.messaging.notifications.REPLY", paramList);
  }
  
  public static PendingIntent b(Context paramContext, List paramList, NotificationIdentifier paramNotificationIdentifier)
  {
    return a(paramContext, "com.truecaller.messaging.notifications.VIEW", paramList, false, paramNotificationIdentifier);
  }
  
  private static Intent b(Context paramContext, String paramString1, List paramList, boolean paramBoolean, NotificationIdentifier paramNotificationIdentifier, String paramString2)
  {
    Intent localIntent = new android/content/Intent;
    Object localObject = NotificationBroadcastReceiver.class;
    localIntent.<init>(paramContext, (Class)localObject);
    localIntent.setAction(paramString1);
    a(localIntent, paramList);
    paramContext = new java/util/LinkedHashSet;
    paramContext.<init>();
    paramString1 = paramList.iterator();
    for (;;)
    {
      boolean bool = paramString1.hasNext();
      if (!bool) {
        break;
      }
      long l1 = nextb;
      localObject = Long.valueOf(l1);
      paramContext.add(localObject);
    }
    paramString1 = "EXTRA_CONVERSATION_IDS";
    int i = paramContext.size();
    localObject = new Long[i];
    paramContext = org.c.a.a.a.a.a((Long[])paramContext.toArray((Object[])localObject));
    localIntent.putExtra(paramString1, paramContext);
    int j = paramList.size();
    int k = 1;
    if (j == k)
    {
      paramContext = "EXTRA_MESSAGE_ID";
      k = 0;
      paramString1 = (Message)paramList.get(0);
      long l2 = a;
      localIntent.putExtra(paramContext, l2);
    }
    localIntent.putExtra("EXTRA_AUTHORIZED_ONLY", paramBoolean);
    localIntent.putExtra("EXTRA_NOTIFICATION_ID", paramNotificationIdentifier);
    localIntent.putExtra("EXTRA_NOTIFICATION_CHANNEL_ID", paramString2);
    return localIntent;
  }
  
  public static PendingIntent c(Context paramContext, List paramList, NotificationIdentifier paramNotificationIdentifier)
  {
    int i = paramList.size();
    int j = 1;
    String str;
    if (i == j) {
      str = c;
    } else {
      str = b;
    }
    return a(paramContext, str, paramList, j, paramNotificationIdentifier);
  }
  
  public static PendingIntent d(Context paramContext, List paramList, NotificationIdentifier paramNotificationIdentifier)
  {
    int i = paramList.size();
    int j = 1;
    String str;
    if (i == j) {
      str = c;
    } else {
      str = b;
    }
    return b(paramContext, str, paramList, j, paramNotificationIdentifier);
  }
  
  public static PendingIntent e(Context paramContext, List paramList, NotificationIdentifier paramNotificationIdentifier)
  {
    return b(paramContext, "com.truecaller.messaging.notifications.VIEW", paramList, false, paramNotificationIdentifier);
  }
  
  public static PendingIntent f(Context paramContext, List paramList, NotificationIdentifier paramNotificationIdentifier)
  {
    return b(paramContext, "com.truecaller.messaging.notifications.DISMISSED", paramList, false, paramNotificationIdentifier);
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    NotificationBroadcastReceiver localNotificationBroadcastReceiver = this;
    Object localObject1 = paramContext;
    Object localObject2 = paramIntent;
    if (paramIntent != null)
    {
      Object localObject3 = paramIntent.getAction();
      if (localObject3 != null)
      {
        localObject3 = ((bk)paramContext.getApplicationContext()).a();
        Object localObject4 = ((bp)localObject3).t();
        Object localObject5 = ((bp)localObject3).c();
        Object localObject6 = ((bp)localObject3).bx();
        Object localObject7 = ((bp)localObject3).aF().o();
        boolean bool1 = ((com.truecaller.featuretoggles.b)localObject7).a();
        Object localObject8 = ((bp)localObject3).cf();
        String str2 = "EXTRA_AUTHORIZED_ONLY";
        boolean bool2 = false;
        Object localObject9 = null;
        boolean bool15 = paramIntent.getBooleanExtra(str2, false);
        if (bool15)
        {
          bool15 = ((al)localObject4).a();
          if (!bool15)
          {
            bool16 = ((al)localObject4).b();
            if (bool16)
            {
              bool16 = true;
              break label161;
            }
          }
        }
        boolean bool16 = false;
        localObject4 = null;
        label161:
        str2 = "EXTRA_SMART_NOTIFICATION";
        bool15 = ((Intent)localObject2).getBooleanExtra(str2, false);
        int i10 = ((com.truecaller.utils.d)localObject6).d();
        if (bool16)
        {
          com.truecaller.wizard.b.c.a((Context)localObject1, WizardActivity.class);
          return;
        }
        f localf = ((bp)localObject3).p();
        localObject4 = ((bp)localObject3).M();
        String str3 = paramIntent.getAction();
        localObject6 = ((Intent)localObject2).getParcelableExtra("EXTRA_NOTIFICATION_ID");
        Object localObject10 = localObject6;
        localObject10 = (NotificationIdentifier)localObject6;
        localObject6 = ((bp)localObject3).aO();
        if ((bool15) && (localObject10 != null))
        {
          i11 = a;
          ((com.truecaller.smsparser.b.a)localObject8).b(i11);
        }
        int i12 = str3.hashCode();
        int i11 = -647729222;
        int i15 = -1;
        if (i12 != i11)
        {
          i11 = -152188085;
          boolean bool17;
          if (i12 != i11)
          {
            i11 = 806490961;
            if (i12 == i11)
            {
              localObject8 = "com.truecaller.messaging.notifications.DISMISSED";
              bool17 = str3.equals(localObject8);
              if (bool17)
              {
                bool17 = false;
                localObject8 = null;
                break label404;
              }
            }
          }
          else
          {
            localObject8 = "com.truecaller.messaging.notifications.PROMOTION";
            bool17 = str3.equals(localObject8);
            if (bool17)
            {
              int i13 = 2;
              break label404;
            }
          }
        }
        else
        {
          localObject8 = "com.truecaller.messaging.notifications.ERROR_DISMISSED";
          boolean bool18 = str3.equals(localObject8);
          if (bool18)
          {
            bool18 = true;
            break label404;
          }
        }
        int i14 = -1;
        label404:
        i11 = 268435456;
        boolean bool19;
        boolean bool22;
        switch (i14)
        {
        default: 
          localObject8 = ((bp)localObject3).o();
          int i16 = str3.hashCode();
          i17 = 8;
          switch (i16)
          {
          }
          break;
        case 2: 
          ((a)((f)localObject4).a()).c();
          ((a)((f)localObject4).a()).f();
          localObject3 = "EXTRA_ANALYTICS_CONTEXT";
          localObject2 = ((Intent)localObject2).getStringExtra((String)localObject3);
          if (localObject2 != null)
          {
            bool19 = true;
          }
          else
          {
            bool19 = false;
            localObject3 = null;
          }
          localObject4 = new String[0];
          AssertionUtil.OnlyInDebug.isTrue(bool19, (String[])localObject4);
          localObject2 = SmsPermissionActivity.a((Context)localObject1, (String)localObject2).addFlags(i11);
          ((Context)localObject1).startActivity((Intent)localObject2);
          return;
        case 1: 
          localObject1 = "EXTRA_CONVERSATION_IDS";
          localObject2 = ((Intent)localObject2).getLongArrayExtra((String)localObject1);
          bool22 = org.c.a.a.a.a.b((long[])localObject2);
          if (bool22)
          {
            localObject1 = (a)((f)localObject4).a();
            bool19 = false;
            localObject3 = null;
            long l1 = localObject2[0];
            ((a)localObject1).c(l1);
          }
          return;
        case 0: 
          localObject1 = "EXTRA_MESSAGE_IDS";
          localObject2 = ((Intent)localObject2).getLongArrayExtra((String)localObject1);
          bool22 = org.c.a.a.a.a.a((long[])localObject2);
          if (!bool22)
          {
            localObject1 = (t)localf.a();
            ((t)localObject1).b((long[])localObject2);
          }
          return;
          localObject9 = "com.truecaller.messaging.notifications.PAY";
          bool2 = str3.equals(localObject9);
          if (bool2)
          {
            int k = 8;
            break label1134;
            localObject9 = "com.truecaller.messaging.notifications.MARK_MESSAGE_READ";
            boolean bool3 = str3.equals(localObject9);
            if (bool3)
            {
              int m = 4;
              break label1134;
              localObject9 = "com.truecaller.messaging.notifications.VIEW_BLOCKED";
              boolean bool4 = str3.equals(localObject9);
              if (bool4)
              {
                bool4 = true;
                break label1134;
                localObject9 = "com.truecaller.messaging.notifications.IM_GROUP_INVITATION";
                bool4 = str3.equals(localObject9);
                if (bool4)
                {
                  int n = 13;
                  break label1134;
                  localObject9 = "com.truecaller.messaging.notifications.VIEW";
                  boolean bool5 = str3.equals(localObject9);
                  if (bool5)
                  {
                    bool5 = false;
                    localObject9 = null;
                    break label1134;
                    localObject9 = "com.truecaller.messaging.notifications.RESEND";
                    bool5 = str3.equals(localObject9);
                    if (bool5)
                    {
                      int i1 = 6;
                      break label1134;
                      localObject9 = "com.truecaller.messaging.notifications.QUICK_REPLY";
                      boolean bool6 = str3.equals(localObject9);
                      if (bool6)
                      {
                        int i2 = 5;
                        break label1134;
                        localObject9 = "com.truecaller.messaging.notifications.MARK_READ";
                        boolean bool7 = str3.equals(localObject9);
                        if (bool7)
                        {
                          int i3 = 3;
                          break label1134;
                          localObject9 = "com.truecaller.messaging.notifications.REPLY";
                          boolean bool8 = str3.equals(localObject9);
                          if (bool8)
                          {
                            int i4 = 2;
                            break label1134;
                            localObject9 = "com.truecaller.messaging.notifications.BLOCK";
                            boolean bool9 = str3.equals(localObject9);
                            if (bool9)
                            {
                              int i5 = 7;
                              break label1134;
                              localObject9 = "com.truecaller.messaging.notifications.SMART_NOTIFICATION_PROMO";
                              boolean bool10 = str3.equals(localObject9);
                              if (bool10)
                              {
                                int i6 = 11;
                                break label1134;
                                localObject9 = "com.truecaller.messaging.notifications.MESSAGES";
                                boolean bool11 = str3.equals(localObject9);
                                if (bool11)
                                {
                                  int i7 = 10;
                                  break label1134;
                                  localObject9 = "com.truecaller.messaging.notifications.CHECK_BALANCE";
                                  boolean bool12 = str3.equals(localObject9);
                                  if (bool12)
                                  {
                                    int i8 = 12;
                                    break label1134;
                                    localObject9 = "com.truecaller.messaging.notifications.RECHARGE";
                                    boolean bool13 = str3.equals(localObject9);
                                    if (bool13) {
                                      i9 = 9;
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          break;
        }
        int i9 = -1;
        label1134:
        int i21 = i10;
        Object localObject11;
        boolean bool23;
        int i22;
        int i18;
        int i28;
        boolean bool20;
        long l2;
        label2579:
        long l5;
        int i26;
        switch (i9)
        {
        default: 
          localObject4 = localObject6;
          localObject11 = localObject5;
          bool23 = bool15;
          i22 = 2;
          break;
        case 13: 
          localObject3 = "EXTRA_IM_GROUP_INFO";
          localObject2 = (ImGroupInfo)((Intent)localObject2).getParcelableExtra((String)localObject3);
          if (localObject2 != null)
          {
            localObject3 = ImGroupInvitationActivity.a;
            localObject2 = ImGroupInvitationActivity.a.a((Context)localObject1, (ImGroupInfo)localObject2);
            i18 = 268435456;
            ((Intent)localObject2).addFlags(i18);
            ((Context)localObject1).startActivity((Intent)localObject2);
            localObject4 = localObject6;
            localObject11 = localObject5;
            bool23 = bool15;
            i22 = 2;
          }
          else
          {
            localObject4 = localObject6;
            localObject11 = localObject5;
            bool23 = bool15;
            i22 = 2;
          }
          break;
        case 12: 
          a(paramContext);
          localObject3 = ((Intent)localObject2).getLongArrayExtra("EXTRA_MESSAGE_IDS");
          bool16 = org.c.a.a.a.a.a((long[])localObject3);
          if (!bool16)
          {
            localObject4 = (t)localf.a();
            ((t)localObject4).b((long[])localObject3);
          }
          localObject3 = "EXTRA_BANK_SYMBOL";
          localObject2 = ((Intent)localObject2).getStringExtra((String)localObject3);
          if (!bool15)
          {
            localObject3 = new com/truecaller/analytics/e$a;
            ((e.a)localObject3).<init>("CheckBalance");
            ((e.a)localObject3).a("BankSymbol", (String)localObject2);
            localObject4 = "Context";
            localObject7 = "notification";
            ((e.a)localObject3).a((String)localObject4, (String)localObject7);
            localObject3 = ((e.a)localObject3).a();
            ((com.truecaller.analytics.b)localObject5).b((com.truecaller.analytics.e)localObject3);
          }
          localObject3 = new android/content/Intent;
          ((Intent)localObject3).<init>("android.intent.action.VIEW");
          localObject2 = String.valueOf(localObject2);
          localObject2 = Uri.parse("truecaller://balance_check?bank_symbol=".concat((String)localObject2));
          ((Intent)localObject3).setData((Uri)localObject2);
          i28 = 268468224;
          ((Intent)localObject3).addFlags(i28);
          ((Context)localObject1).startActivity((Intent)localObject3);
          localObject4 = localObject6;
          localObject11 = localObject5;
          bool23 = bool15;
          i22 = 2;
          break;
        case 11: 
          a(paramContext);
          localObject2 = ((bp)localObject3).I();
          bool16 = true;
          ((com.truecaller.common.g.a)localObject2).b("smart_notifications_clicked", bool16);
          localObject2 = new android/content/Intent;
          localObject3 = SmartPromoActivity.class;
          ((Intent)localObject2).<init>((Context)localObject1, (Class)localObject3);
          i18 = 276856832;
          ((Intent)localObject2).setFlags(i18);
          ((Context)localObject1).startActivity((Intent)localObject2);
          localObject4 = localObject6;
          localObject11 = localObject5;
          bool23 = bool15;
          i22 = 2;
          break;
        case 10: 
          a(paramContext);
          localObject3 = "notificationIncomingMessage";
          localObject2 = TruecallerInit.a((Context)localObject1, "messages", (String)localObject3);
          ((Context)localObject1).startActivity((Intent)localObject2);
          localObject4 = localObject6;
          localObject11 = localObject5;
          bool23 = bool15;
          i22 = 2;
          break;
        case 9: 
          a(paramContext);
          localObject3 = "EXTRA_DEEPLINK_SUFFIX";
          localObject2 = ((Intent)localObject2).getStringExtra((String)localObject3);
          bool20 = am.c((CharSequence)localObject2);
          if (bool20)
          {
            if (bool1)
            {
              localObject3 = new android/content/Intent;
              localObject4 = "android.intent.action.VIEW";
              ((Intent)localObject3).<init>((String)localObject4);
              localObject2 = Uri.parse((String)localObject2);
              ((Intent)localObject3).setData((Uri)localObject2);
              i28 = 268435456;
              ((Intent)localObject3).setFlags(i28);
              ((Context)localObject1).startActivity((Intent)localObject3);
            }
            localObject4 = localObject6;
            localObject11 = localObject5;
            bool23 = bool15;
            i22 = 2;
          }
          else
          {
            localObject4 = localObject6;
            localObject11 = localObject5;
            bool23 = bool15;
            i22 = 2;
          }
          break;
        case 8: 
          a(paramContext);
          localObject3 = paramIntent.getData();
          localObject4 = TrueApp.y();
          bool16 = ((TrueApp)localObject4).isTcPayEnabled();
          if (bool16)
          {
            localObject4 = ((Uri)localObject3).getQueryParameter("recipient");
            localObject7 = ((Uri)localObject3).getQueryParameter("amount");
            localObject8 = ((Uri)localObject3).getQueryParameter("comment");
            localObject9 = am.a((String)localObject4, "+ ", "");
            i11 = 0;
            localObject1 = paramContext;
            localObject3 = localObject7;
            localObject4 = localObject9;
            localObject11 = localObject6;
            localObject6 = localObject9;
            localObject7 = localObject8;
            i14 = 0;
            localObject8 = null;
            TransactionActivity.startForSend(paramContext, (String)localObject3, (String)localObject9, (String)localObject9, (String)localObject7, null);
          }
          else
          {
            localObject11 = localObject6;
          }
          localObject1 = "EXTRA_MESSAGE_IDS";
          localObject2 = ((Intent)localObject2).getLongArrayExtra((String)localObject1);
          bool22 = org.c.a.a.a.a.a((long[])localObject2);
          if (!bool22)
          {
            localObject1 = (t)localf.a();
            ((t)localObject1).b((long[])localObject2);
            bool23 = bool15;
            localObject4 = localObject11;
            i22 = 2;
            localObject11 = localObject5;
          }
          else
          {
            bool23 = bool15;
            localObject4 = localObject11;
            i22 = 2;
            localObject11 = localObject5;
          }
          break;
        case 7: 
          localObject11 = localObject6;
          localObject1 = ((Intent)localObject2).getStringExtra("EXTRA_NORMALIZED_ADDRESS");
          bool16 = TextUtils.isEmpty((CharSequence)localObject1);
          if (!bool16)
          {
            localObject4 = ((Intent)localObject2).getStringExtra("EXTRA_TRACKING_TYPE");
            localObject6 = ((Intent)localObject2).getStringExtra("EXTRA_LABEL");
            localObject3 = ((bp)localObject3).S().a();
            Object localObject12 = localObject3;
            localObject12 = (s)localObject3;
            List localList1 = Collections.singletonList(localObject1);
            List localList2 = Collections.singletonList(localObject4);
            List localList3 = Collections.singletonList(localObject6);
            String str4 = "notificationIncomingMessage";
            boolean bool31 = true;
            TruecallerContract.Filters.EntityType localEntityType = TruecallerContract.Filters.EntityType.UNKNOWN;
            ((s)localObject12).a(localList1, localList2, localList3, str4, bool31, localEntityType).c();
            localObject1 = "EXTRA_MESSAGE_IDS";
            localObject2 = ((Intent)localObject2).getLongArrayExtra((String)localObject1);
            bool22 = org.c.a.a.a.a.a((long[])localObject2);
            if (bool22) {
              return;
            }
            localObject1 = (t)localf.a();
            ((t)localObject1).b((long[])localObject2);
            bool23 = bool15;
            localObject4 = localObject11;
            i22 = 2;
            localObject11 = localObject5;
          }
          else
          {
            bool23 = bool15;
            localObject4 = localObject6;
            i22 = 2;
            localObject11 = localObject5;
          }
          break;
        case 6: 
          localObject11 = localObject6;
          localObject1 = ((Intent)localObject2).getLongArrayExtra("EXTRA_MESSAGE_IDS");
          localObject3 = "EXTRA_CONVERSATION_IDS";
          localObject2 = ((Intent)localObject2).getLongArrayExtra((String)localObject3);
          bool20 = org.c.a.a.a.a.b((long[])localObject2);
          if (bool20)
          {
            localObject3 = (a)((f)localObject4).a();
            bool16 = false;
            localObject4 = null;
            l2 = localObject2[0];
            ((a)localObject3).c(l2);
          }
          else
          {
            bool16 = false;
            localObject4 = null;
          }
          boolean bool28 = org.c.a.a.a.a.a((long[])localObject1);
          if (!bool28)
          {
            localObject2 = (t)localf.a();
            l2 = localObject1[0];
            localObject2 = ((t)localObject2).a(l2);
            localObject1 = new com/truecaller/messaging/notifications/-$$Lambda$NotificationBroadcastReceiver$-s0wYKk8jbRqLR21xqhMbFxB98g;
            ((-..Lambda.NotificationBroadcastReceiver.-s0wYKk8jbRqLR21xqhMbFxB98g)localObject1).<init>((m)localObject8);
            ((w)localObject2).a((com.truecaller.androidactors.ac)localObject1);
            bool23 = bool15;
            localObject4 = localObject11;
            i22 = 2;
            localObject11 = localObject5;
          }
          else
          {
            bool23 = bool15;
            localObject4 = localObject11;
            i22 = 2;
            localObject11 = localObject5;
          }
          break;
        case 5: 
          localObject4 = localObject6;
          localObject1 = ((Intent)localObject2).getLongArrayExtra("EXTRA_CONVERSATION_IDS");
          boolean bool24 = org.c.a.a.a.a.a((long[])localObject1);
          if (!bool24)
          {
            int i23 = Build.VERSION.SDK_INT;
            int i = 20;
            i14 = 0;
            localObject8 = null;
            int j;
            if (i23 >= i)
            {
              localObject6 = RemoteInput.getResultsFromIntent(paramIntent);
            }
            else
            {
              i23 = Build.VERSION.SDK_INT;
              i = 16;
              if (i23 >= i)
              {
                localObject6 = paramIntent.getClipData();
                if (localObject6 == null)
                {
                  i23 = 0;
                  localObject6 = null;
                }
                else
                {
                  localObject7 = ((ClipData)localObject6).getDescription();
                  localObject9 = "text/vnd.android.intent";
                  boolean bool14 = ((ClipDescription)localObject7).hasMimeType((String)localObject9);
                  if (!bool14)
                  {
                    i23 = 0;
                    localObject6 = null;
                  }
                  else
                  {
                    localObject7 = ((ClipDescription)localObject7).getLabel();
                    localObject9 = "android.remoteinput.results";
                    j = localObject7.equals(localObject9);
                    if (j == 0)
                    {
                      i23 = 0;
                      localObject6 = null;
                    }
                    else
                    {
                      j = 0;
                      localObject7 = null;
                      localObject6 = ((ClipData)localObject6).getItemAt(0).getIntent();
                    }
                  }
                }
                if (localObject6 != null)
                {
                  localObject6 = ((Intent)localObject6).getExtras();
                  localObject7 = "android.remoteinput.resultsData";
                  localObject6 = (Bundle)((Bundle)localObject6).getParcelable((String)localObject7);
                  break label2579;
                }
              }
              i23 = 0;
              localObject6 = null;
            }
            if (localObject6 != null)
            {
              localObject7 = "KEY_REPLY_TEXT";
              localObject8 = ((Bundle)localObject6).getCharSequence((String)localObject7);
            }
            boolean bool25 = am.e((CharSequence)localObject8);
            if (bool25)
            {
              int i24 = a;
              l2 = i24;
              localObject9 = new com/truecaller/messaging/f/a$a;
              i11 = 0;
              ((a.a)localObject9).<init>((byte)0);
              localObject3 = (bp)g.a(localObject3);
              b = ((bp)localObject3);
              localObject3 = new com/truecaller/messaging/f/e;
              localObject11 = localObject5;
              bool23 = bool15;
              long l3 = localObject1[0];
              ((com.truecaller.messaging.f.e)localObject3).<init>(l3, l2);
              localObject3 = (com.truecaller.messaging.f.e)g.a(localObject3);
              a = ((com.truecaller.messaging.f.e)localObject3);
              g.a(a, com.truecaller.messaging.f.e.class);
              g.a(b, bp.class);
              localObject3 = new com/truecaller/messaging/f/a;
              localObject6 = a;
              localObject7 = b;
              localObject5 = null;
              ((com.truecaller.messaging.f.a)localObject3).<init>((com.truecaller.messaging.f.e)localObject6, (bp)localObject7, (byte)0);
              ((com.truecaller.messaging.f.b)localObject3).a(localNotificationBroadcastReceiver);
              localObject3 = a;
              localObject6 = ((CharSequence)localObject8).toString();
              k.b(localObject6, "text");
              localObject7 = localObject6;
              localObject7 = (CharSequence)localObject6;
              j = am.d((CharSequence)localObject7);
              if (j == 0)
              {
                c = ((String)localObject6);
                localObject6 = (o)g.a();
                long l4 = e;
                localObject6 = ((o)localObject6).a(l4);
                localObject7 = h;
                localObject8 = new com/truecaller/messaging/f/c$a;
                localObject5 = localObject3;
                localObject5 = (com.truecaller.messaging.f.c)localObject3;
                ((c.a)localObject8).<init>((com.truecaller.messaging.f.c)localObject5);
                localObject8 = (c.g.a.b)localObject8;
                localObject5 = new com/truecaller/messaging/f/d;
                ((com.truecaller.messaging.f.d)localObject5).<init>((c.g.a.b)localObject8);
                localObject5 = (com.truecaller.androidactors.ac)localObject5;
                localObject6 = ((w)localObject6).a((i)localObject7, (com.truecaller.androidactors.ac)localObject5);
                a = ((com.truecaller.androidactors.a)localObject6);
                ((a)i.a()).a();
                localObject3 = l;
                localObject6 = new com/truecaller/analytics/bc;
                localObject7 = "quickReply";
                ((bc)localObject6).<init>((String)localObject7);
                localObject6 = (com.truecaller.analytics.e)localObject6;
                ((ap)localObject3).a((com.truecaller.analytics.e)localObject6);
              }
              localObject3 = ((Intent)localObject2).getLongArrayExtra("EXTRA_MESSAGE_IDS");
              boolean bool26 = org.c.a.a.a.a.b((long[])localObject3);
              if (bool26)
              {
                int i25 = localObject3.length;
                j = 1;
                if (i25 == j)
                {
                  localObject1 = "EXTRA_MESSAGE_ID";
                  l2 = -1;
                  l5 = ((Intent)localObject2).getLongExtra((String)localObject1, l2);
                  a(l5, localf);
                  i26 = 2;
                  break;
                }
              }
              i29 = Build.VERSION.SDK_INT;
              i26 = 24;
              if (i29 < i26)
              {
                localObject2 = ((t)localf.a()).a((long[])localObject1);
                ((w)localObject2).c();
                i26 = 2;
              }
              else
              {
                localObject2 = (t)localf.a();
                ((t)localObject2).c((long[])localObject3);
                i26 = 2;
              }
            }
            else
            {
              localObject11 = localObject5;
              bool23 = bool15;
              i26 = 2;
            }
          }
          else
          {
            localObject11 = localObject5;
            bool23 = bool15;
            i26 = 2;
          }
          break;
        case 4: 
          localObject4 = localObject6;
          localObject11 = localObject5;
          bool23 = bool15;
          localObject1 = "EXTRA_MESSAGE_ID";
          l2 = -1;
          l5 = ((Intent)localObject2).getLongExtra((String)localObject1, l2);
          a(l5, localf);
          i26 = 2;
          break;
        case 3: 
          localObject4 = localObject6;
          localObject11 = localObject5;
          bool23 = bool15;
          localObject1 = ((Intent)localObject2).getLongArrayExtra("EXTRA_CONVERSATION_IDS");
          bool20 = org.c.a.a.a.a.b((long[])localObject1);
          if (bool20)
          {
            int i19 = Build.VERSION.SDK_INT;
            i26 = 24;
            if (i19 < i26)
            {
              localObject2 = ((t)localf.a()).a((long[])localObject1);
              ((w)localObject2).c();
              i26 = 2;
            }
            else
            {
              localObject2 = ((Intent)localObject2).getLongArrayExtra("EXTRA_MESSAGE_IDS");
              localObject1 = (t)localf.a();
              ((t)localObject1).c((long[])localObject2);
              i26 = 2;
            }
          }
          else
          {
            i26 = 2;
          }
          break;
        case 0: 
        case 1: 
        case 2: 
          localObject4 = localObject6;
          localObject11 = localObject5;
          bool23 = bool15;
          a(paramContext);
          localObject3 = ((Intent)localObject2).getLongArrayExtra("EXTRA_MESSAGE_IDS");
          boolean bool27 = org.c.a.a.a.a.a((long[])localObject3);
          if (bool27) {
            return;
          }
          localObject6 = (t)localf.a();
          ((t)localObject6).b((long[])localObject3);
          localObject3 = "com.truecaller.messaging.notifications.VIEW_BLOCKED";
          boolean bool21 = ((String)localObject3).equals(str3);
          int i27;
          if (bool21)
          {
            localObject3 = "notificationBlockedMessage";
            localObject2 = TruecallerInit.a((Context)localObject1, "messages", (String)localObject3);
            ((Context)localObject1).startActivity((Intent)localObject2);
            i27 = 2;
          }
          else
          {
            localObject3 = "EXTRA_CONVERSATION_IDS";
            localObject2 = ((Intent)localObject2).getLongArrayExtra((String)localObject3);
            bool21 = org.c.a.a.a.a.a((long[])localObject2);
            if (bool21)
            {
              localObject3 = "notificationIncomingMessage";
              localObject2 = TruecallerInit.a((Context)localObject1, "messages", (String)localObject3);
              ((Context)localObject1).startActivity((Intent)localObject2);
              i27 = 2;
            }
            else
            {
              bool21 = false;
              l2 = localObject2[0];
              str3.equals("com.truecaller.messaging.notifications.REPLY");
              localObject2 = TruecallerInit.a((Context)localObject1, "messages", "notificationIncomingMessage");
              localObject3 = new android/content/Intent;
              ((Intent)localObject3).<init>((Context)localObject1, ConversationActivity.class);
              ((Intent)localObject3).putExtra("conversation_id", l2);
              i27 = 2;
              localObject7 = new Intent[i27];
              localObject8 = null;
              localObject7[0] = localObject2;
              i14 = 1;
              localObject7[i14] = localObject3;
              i29 = 1073741824;
              localObject2 = PendingIntent.getActivities((Context)localObject1, i14, (Intent[])localObject7, i29);
              try
              {
                ((PendingIntent)localObject2).send();
              }
              catch (PendingIntent.CanceledException localCanceledException)
              {
                localObject7 = localCanceledException;
                AssertionUtil.reportThrowableButNeverCrash(localCanceledException);
                ((Context)localObject1).startActivity((Intent)localObject3);
              }
            }
          }
          break;
        }
        int i29 = str3.hashCode();
        String str1;
        boolean bool29;
        switch (i29)
        {
        default: 
          break;
        case 2122492240: 
          str1 = "com.truecaller.messaging.notifications.PAY";
          bool29 = str3.equals(str1);
          if (bool29) {
            i17 = 3;
          }
          break;
        case 1737289192: 
          str1 = "com.truecaller.messaging.notifications.MARK_MESSAGE_READ";
          bool29 = str3.equals(str1);
          if (bool29) {
            i17 = 1;
          }
          break;
        case 1468630012: 
          str1 = "com.truecaller.messaging.notifications.IM_GROUP_INVITATION";
          bool29 = str3.equals(str1);
          if (bool29) {
            i17 = 9;
          }
          break;
        case 1372935901: 
          str1 = "com.truecaller.messaging.notifications.VIEW";
          bool29 = str3.equals(str1);
          if (bool29) {
            i17 = 5;
          }
          break;
        case -262798704: 
          str1 = "com.truecaller.messaging.notifications.MARK_READ";
          bool29 = str3.equals(str1);
          if (bool29) {
            i17 = 0;
          }
          break;
        case -407032011: 
          str1 = "com.truecaller.messaging.notifications.BLOCK";
          bool29 = str3.equals(str1);
          if (bool29) {
            i17 = 2;
          }
          break;
        case -844828887: 
          str1 = "com.truecaller.messaging.notifications.SMART_NOTIFICATION_PROMO";
          bool29 = str3.equals(str1);
          if (bool29) {
            i17 = 7;
          }
          break;
        case -995411580: 
          str1 = "com.truecaller.messaging.notifications.MESSAGES";
          bool29 = str3.equals(str1);
          if (bool29) {
            i17 = 6;
          }
          break;
        case -1086701267: 
          str1 = "com.truecaller.messaging.notifications.CHECK_BALANCE";
          bool29 = str3.equals(str1);
          if (!bool29) {
            break;
          }
          break;
        case -1339509025: 
          str1 = "com.truecaller.messaging.notifications.RECHARGE";
          bool29 = str3.equals(str1);
          if (bool29) {
            i17 = 4;
          }
          break;
        }
        int i17 = -1;
        int i20;
        switch (i17)
        {
        default: 
          break;
        case 0: 
        case 1: 
        case 2: 
        case 3: 
        case 4: 
        case 5: 
        case 6: 
        case 7: 
        case 8: 
        case 9: 
          if (localObject10 != null)
          {
            str1 = b;
            i20 = a;
            ((android.support.v4.app.ac)localObject4).a(str1, i20);
          }
          break;
        }
        if (bool23)
        {
          int i30 = str3.hashCode();
          boolean bool30;
          switch (i30)
          {
          default: 
            break;
          case 1737289192: 
            str1 = "com.truecaller.messaging.notifications.MARK_MESSAGE_READ";
            bool30 = str3.equals(str1);
            if (bool30) {
              i15 = 1;
            }
            break;
          case 1372935901: 
            str1 = "com.truecaller.messaging.notifications.VIEW";
            bool30 = str3.equals(str1);
            if (bool30) {
              i15 = 0;
            }
            break;
          case -995411580: 
            str1 = "com.truecaller.messaging.notifications.MESSAGES";
            bool30 = str3.equals(str1);
            if (bool30) {
              i15 = 3;
            }
            break;
          case -1086701267: 
            str1 = "com.truecaller.messaging.notifications.CHECK_BALANCE";
            bool30 = str3.equals(str1);
            if (bool30) {
              i15 = 4;
            }
            break;
          case -1339509025: 
            str1 = "com.truecaller.messaging.notifications.RECHARGE";
            bool30 = str3.equals(str1);
            if (bool30) {
              i15 = 2;
            }
            break;
          }
          switch (i15)
          {
          default: 
            break;
          case 4: 
            str1 = "CheckBalance";
            i20 = i21;
            a((com.truecaller.analytics.b)localObject11, str1, i21);
            break;
          case 3: 
            a((com.truecaller.analytics.b)localObject11, "NonDefault", false);
            return;
          case 2: 
            i20 = i21;
            a((com.truecaller.analytics.b)localObject11, "PayBill", i21);
            return;
          case 1: 
            a((com.truecaller.analytics.b)localObject11, "MarkAsRead", true);
            return;
          case 0: 
            a((com.truecaller.analytics.b)localObject11, "ShowSms", true);
            return;
          }
        }
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.NotificationBroadcastReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */