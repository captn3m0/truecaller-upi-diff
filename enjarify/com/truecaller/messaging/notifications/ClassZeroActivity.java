package com.truecaller.messaging.notifications;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import com.truecaller.androidactors.f;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Message.a;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import java.util.ArrayList;
import java.util.List;

public class ClassZeroActivity
  extends AppCompatActivity
{
  private long a = 0L;
  private AlertDialog b = null;
  private ArrayList c = null;
  private final Handler d;
  private final DialogInterface.OnClickListener e;
  private final DialogInterface.OnClickListener f;
  
  public ClassZeroActivity()
  {
    Object localObject = new com/truecaller/messaging/notifications/ClassZeroActivity$a;
    ((ClassZeroActivity.a)localObject).<init>(this);
    d = ((Handler)localObject);
    localObject = new com/truecaller/messaging/notifications/-$$Lambda$ClassZeroActivity$gCI7Zw6dWURnImAZvVtyXa5UWoU;
    ((-..Lambda.ClassZeroActivity.gCI7Zw6dWURnImAZvVtyXa5UWoU)localObject).<init>(this);
    e = ((DialogInterface.OnClickListener)localObject);
    localObject = new com/truecaller/messaging/notifications/-$$Lambda$ClassZeroActivity$9Je06oCpHrRfN8uss0oUf8gALTE;
    ((-..Lambda.ClassZeroActivity.9Je06oCpHrRfN8uss0oUf8gALTE)localObject).<init>(this);
    f = ((DialogInterface.OnClickListener)localObject);
  }
  
  static Intent a(Context paramContext, Message paramMessage)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, ClassZeroActivity.class);
    localIntent.putExtra("extra_message_values", paramMessage);
    localIntent.setFlags(402653184);
    return localIntent;
  }
  
  private void a()
  {
    Object localObject = b;
    if (localObject != null)
    {
      ((AlertDialog)localObject).dismiss();
      bool = false;
      localObject = null;
      b = null;
    }
    localObject = c;
    boolean bool = ((ArrayList)localObject).isEmpty();
    if (!bool)
    {
      localObject = c;
      ((ArrayList)localObject).remove(0);
    }
    localObject = c;
    bool = ((ArrayList)localObject).isEmpty();
    if (bool)
    {
      finish();
      return;
    }
    localObject = (Message)c.get(0);
    a((Message)localObject);
    long l = SystemClock.uptimeMillis() + 300000L;
    a = l;
    b();
  }
  
  private void a(Intent paramIntent)
  {
    Object localObject = "extra_message_values";
    paramIntent = (Message)paramIntent.getParcelableExtra((String)localObject);
    if (paramIntent != null)
    {
      localObject = c;
      boolean bool = ((ArrayList)localObject).contains(paramIntent);
      if (!bool)
      {
        localObject = c;
        ((ArrayList)localObject).add(paramIntent);
      }
    }
  }
  
  private void a(Message paramMessage)
  {
    Object localObject = b;
    if (localObject != null)
    {
      ((AlertDialog)localObject).dismiss();
      localObject = null;
      b = null;
    }
    localObject = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject).<init>(this);
    String str = paramMessage.j();
    localObject = ((AlertDialog.Builder)localObject).setMessage(str);
    DialogInterface.OnClickListener localOnClickListener = f;
    localObject = ((AlertDialog.Builder)localObject).setPositiveButton(2131887225, localOnClickListener);
    localOnClickListener = e;
    localObject = ((AlertDialog.Builder)localObject).setNegativeButton(2131887197, localOnClickListener);
    paramMessage = c.a();
    paramMessage = ((AlertDialog.Builder)localObject).setTitle(paramMessage).setCancelable(false).show();
    b = paramMessage;
  }
  
  private void a(boolean paramBoolean)
  {
    Object localObject = c;
    boolean bool = ((ArrayList)localObject).isEmpty();
    if (!bool)
    {
      localObject = (Message)c.get(0);
      bp localbp = ((bk)getApplicationContext()).a();
      localObject = ((Message)localObject).m();
      g = paramBoolean;
      Message localMessage = ((Message.a)localObject).b();
      localObject = (t)localbp.p().a();
      ((t)localObject).a(localMessage);
    }
  }
  
  private void b()
  {
    Handler localHandler = d;
    int i = 1;
    localHandler.removeMessages(i);
    long l1 = SystemClock.uptimeMillis();
    long l2 = a;
    boolean bool = l2 < l1;
    if (!bool)
    {
      d.sendEmptyMessage(i);
      return;
    }
    d.sendEmptyMessageAtTime(i, l2);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Object localObject = getTheme();
    ThemeManager.Theme localTheme = ThemeManager.a();
    int i = resId;
    ((Resources.Theme)localObject).applyStyle(i, false);
    int j = 1;
    requestWindowFeature(j);
    if (paramBundle != null)
    {
      localObject = "messages";
      bool = paramBundle.containsKey((String)localObject);
      if (bool)
      {
        localObject = paramBundle.getParcelableArrayList("messages");
        c = ((ArrayList)localObject);
      }
    }
    localObject = c;
    if (localObject == null)
    {
      localObject = new java/util/ArrayList;
      ((ArrayList)localObject).<init>();
      c = ((ArrayList)localObject);
    }
    localObject = getIntent();
    a((Intent)localObject);
    localObject = c;
    boolean bool = ((ArrayList)localObject).isEmpty();
    if (!bool)
    {
      localObject = (Message)c.get(0);
      a((Message)localObject);
      long l1 = SystemClock.uptimeMillis();
      long l2 = 300000L;
      l1 += l2;
      a = l1;
      if (paramBundle != null)
      {
        long l3 = a;
        l1 = paramBundle.getLong("timer_fire", l3);
        l2 = a;
        l1 = Math.min(l1, l2);
        a = l1;
      }
    }
    else
    {
      finish();
    }
  }
  
  public void onNewIntent(Intent paramIntent)
  {
    a(paramIntent);
  }
  
  protected void onRestart()
  {
    super.onRestart();
    Object localObject = c;
    boolean bool = ((ArrayList)localObject).isEmpty();
    if (!bool)
    {
      localObject = (Message)c.get(0);
      a((Message)localObject);
      return;
    }
    finish();
  }
  
  public void onResume()
  {
    super.onResume();
    ArrayList localArrayList = c;
    boolean bool = localArrayList.isEmpty();
    if (bool) {
      finish();
    }
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    Object localObject = c;
    int i = ((ArrayList)localObject).size();
    int j = 10;
    if (i <= j)
    {
      localObject = "timer_fire";
      long l = a;
      paramBundle.putLong((String)localObject, l);
    }
    localObject = c;
    int k = Math.max(((ArrayList)localObject).size() - j, 0);
    ((ArrayList)localObject).subList(0, k).clear();
    ArrayList localArrayList = c;
    paramBundle.putParcelableArrayList("messages", localArrayList);
  }
  
  public void onStart()
  {
    super.onStart();
    b();
  }
  
  public void onStop()
  {
    super.onStop();
    d.removeMessages(1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.ClassZeroActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */