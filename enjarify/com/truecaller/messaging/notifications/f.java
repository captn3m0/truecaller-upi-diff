package com.truecaller.messaging.notifications;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import c.g.b.k;
import com.truecaller.messaging.data.types.Participant;

public final class f
{
  public static final PendingIntent a(Context paramContext, Participant paramParticipant, long paramLong, int paramInt)
  {
    k.b(paramContext, "context");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, ReactionBroadcastReceiver.class);
    localIntent.setAction("com.truecaller.open_conversation");
    paramParticipant = (Parcelable)paramParticipant;
    localIntent.putExtra("participant", paramParticipant);
    localIntent.putExtra("message_id", paramLong);
    paramContext = PendingIntent.getBroadcast(paramContext, paramInt, localIntent, 134217728);
    k.a(paramContext, "PendingIntent.getBroadca…tent.FLAG_UPDATE_CURRENT)");
    return paramContext;
  }
  
  public static final PendingIntent a(Context paramContext, long[] paramArrayOfLong, int paramInt)
  {
    k.b(paramContext, "context");
    k.b(paramArrayOfLong, "messageIds");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, ReactionBroadcastReceiver.class);
    localIntent.setAction("com.truecaller.mark_as_seen");
    localIntent.putExtra("message_ids", paramArrayOfLong);
    paramContext = PendingIntent.getBroadcast(paramContext, paramInt, localIntent, 134217728);
    k.a(paramContext, "PendingIntent.getBroadca…tent.FLAG_UPDATE_CURRENT)");
    return paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */