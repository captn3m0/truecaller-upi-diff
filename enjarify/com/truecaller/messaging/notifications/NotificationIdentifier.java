package com.truecaller.messaging.notifications;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class NotificationIdentifier
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final int a;
  public final String b;
  final int c;
  
  static
  {
    NotificationIdentifier.a locala = new com/truecaller/messaging/notifications/NotificationIdentifier$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public NotificationIdentifier(int paramInt)
  {
    this(paramInt, 0, 6);
  }
  
  public NotificationIdentifier(int paramInt1, String paramString, int paramInt2)
  {
    a = paramInt1;
    b = paramString;
    c = paramInt2;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof NotificationIdentifier;
      if (bool2)
      {
        paramObject = (NotificationIdentifier)paramObject;
        int i = a;
        int k = a;
        String str1;
        if (i == k)
        {
          i = 1;
        }
        else
        {
          i = 0;
          str1 = null;
        }
        if (i != 0)
        {
          str1 = b;
          String str2 = b;
          boolean bool3 = k.a(str1, str2);
          if (bool3)
          {
            int j = c;
            int m = c;
            if (j == m)
            {
              m = 1;
            }
            else
            {
              m = 0;
              paramObject = null;
            }
            if (m != 0) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = a * 31;
    String str = b;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    i = (i + j) * 31;
    int j = c;
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("NotificationIdentifier(notificationId=");
    int i = a;
    localStringBuilder.append(i);
    localStringBuilder.append(", notificationTag=");
    String str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(", requestId=");
    i = c;
    localStringBuilder.append(i);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    paramInt = a;
    paramParcel.writeInt(paramInt);
    String str = b;
    paramParcel.writeString(str);
    paramInt = c;
    paramParcel.writeInt(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.NotificationIdentifier
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */