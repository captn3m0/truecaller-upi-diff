package com.truecaller.messaging.notifications;

import android.os.Handler;
import android.os.Message;
import java.lang.ref.WeakReference;

final class ClassZeroActivity$a
  extends Handler
{
  private final WeakReference a;
  
  ClassZeroActivity$a(ClassZeroActivity paramClassZeroActivity)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramClassZeroActivity);
    a = localWeakReference;
  }
  
  public final void handleMessage(Message paramMessage)
  {
    ClassZeroActivity localClassZeroActivity = (ClassZeroActivity)a.get();
    int i = what;
    int j = 1;
    if ((i == j) && (localClassZeroActivity != null))
    {
      ClassZeroActivity.a(localClassZeroActivity);
      ClassZeroActivity.b(localClassZeroActivity);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.notifications.ClassZeroActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */