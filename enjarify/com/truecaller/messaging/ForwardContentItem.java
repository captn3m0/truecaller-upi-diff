package com.truecaller.messaging;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;
import com.truecaller.messaging.data.types.BinaryEntity;

public final class ForwardContentItem
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final String a;
  public final BinaryEntity b;
  
  static
  {
    ForwardContentItem.a locala = new com/truecaller/messaging/ForwardContentItem$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public ForwardContentItem(String paramString, BinaryEntity paramBinaryEntity)
  {
    a = paramString;
    b = paramBinaryEntity;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof ForwardContentItem;
      if (bool1)
      {
        paramObject = (ForwardContentItem)paramObject;
        Object localObject = a;
        String str = a;
        bool1 = k.a(localObject, str);
        if (bool1)
        {
          localObject = b;
          paramObject = b;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final int hashCode()
  {
    String str = a;
    int i = 0;
    int j;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    BinaryEntity localBinaryEntity = b;
    if (localBinaryEntity != null) {
      i = localBinaryEntity.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ForwardContentItem(text=");
    Object localObject = a;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", mediaContent=");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    Object localObject = a;
    paramParcel.writeString((String)localObject);
    localObject = b;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.ForwardContentItem
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */