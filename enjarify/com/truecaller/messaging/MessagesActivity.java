package com.truecaller.messaging;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.truecaller.common.h.o;
import com.truecaller.ui.TruecallerInit;

public class MessagesActivity
  extends Activity
{
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = "android.intent.action.CREATE_SHORTCUT";
    Object localObject = getIntent().getAction();
    boolean bool = paramBundle.equals(localObject);
    if (bool)
    {
      int i = -1;
      localObject = new android/content/Intent;
      ((Intent)localObject).<init>(this, MessagesActivity.class);
      ((Intent)localObject).setAction("android.intent.action.MAIN");
      String str = "android.intent.category.DEFAULT";
      ((Intent)localObject).addCategory(str);
      ((Intent)localObject).addFlags(335544320);
      int j = 2131887268;
      int k = 2131689476;
      localObject = o.a(this, j, k, (Intent)localObject);
      setResult(i, (Intent)localObject);
    }
    else
    {
      localObject = "homescreenShortcut";
      paramBundle = TruecallerInit.a(this, "messages", (String)localObject);
      startActivity(paramBundle);
    }
    finish();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.MessagesActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */