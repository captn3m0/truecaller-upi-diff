package com.truecaller.messaging;

import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d
{
  private final l a;
  private final Provider b;
  
  private q(l paraml, Provider paramProvider)
  {
    a = paraml;
    b = paramProvider;
  }
  
  public static q a(l paraml, Provider paramProvider)
  {
    q localq = new com/truecaller/messaging/q;
    localq.<init>(paraml, paramProvider);
    return localq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */