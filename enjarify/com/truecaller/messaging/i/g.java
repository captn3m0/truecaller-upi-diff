package com.truecaller.messaging.i;

import com.truecaller.messaging.data.types.Participant;
import com.truecaller.utils.n;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

public final class g
{
  public static String a(n paramn, Participant paramParticipant, boolean paramBoolean)
  {
    int i = q;
    paramBoolean = paramParticipant.a(paramBoolean);
    int j = 1;
    Object[] arrayOfObject;
    Integer localInteger;
    if (paramBoolean)
    {
      if (i > 0)
      {
        arrayOfObject = new Object[j];
        localInteger = Integer.valueOf(i);
        arrayOfObject[0] = localInteger;
        return paramn.a(2131886144, arrayOfObject);
      }
      arrayOfObject = new Object[0];
      return paramn.a(2131886143, arrayOfObject);
    }
    boolean bool = paramParticipant.g();
    if (bool)
    {
      arrayOfObject = new Object[j];
      localInteger = Integer.valueOf(i);
      arrayOfObject[0] = localInteger;
      return paramn.a(2131886146, arrayOfObject);
    }
    return null;
  }
  
  public static String a(Collection paramCollection)
  {
    return a(paramCollection, ", ");
  }
  
  private static String a(Collection paramCollection, CharSequence paramCharSequence)
  {
    if (paramCollection == null) {
      return null;
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    paramCollection = paramCollection.iterator();
    for (;;)
    {
      boolean bool = paramCollection.hasNext();
      if (!bool) {
        break;
      }
      String str = ((Participant)paramCollection.next()).a();
      localStringBuilder.append(str);
      localStringBuilder.append(paramCharSequence);
    }
    int i = localStringBuilder.length();
    if (i > 0)
    {
      i = localStringBuilder.length();
      int j = paramCharSequence.length();
      i -= j;
      localStringBuilder.setLength(i);
    }
    return localStringBuilder.toString();
  }
  
  public static String a(Participant[] paramArrayOfParticipant)
  {
    return a(Arrays.asList(paramArrayOfParticipant), ", ");
  }
  
  public static boolean b(Participant[] paramArrayOfParticipant)
  {
    int i = paramArrayOfParticipant.length;
    int j = 1;
    if (i <= j)
    {
      boolean bool = c(paramArrayOfParticipant);
      if (!bool) {
        return false;
      }
    }
    return j;
  }
  
  public static boolean c(Participant[] paramArrayOfParticipant)
  {
    int i = paramArrayOfParticipant.length;
    int j = 1;
    if (i == j)
    {
      paramArrayOfParticipant = paramArrayOfParticipant[0];
      int k = c;
      i = 4;
      if (k == i) {
        return j;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.i.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */