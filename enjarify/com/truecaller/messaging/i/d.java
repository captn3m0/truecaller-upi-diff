package com.truecaller.messaging.i;

import android.graphics.drawable.Drawable;
import c.g.a.b;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.util.c.a;

public abstract interface d
{
  public abstract int a(int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  public abstract int a(Message paramMessage, b paramb);
  
  public abstract Drawable a(int paramInt1, int paramInt2, int paramInt3);
  
  public abstract Drawable a(int paramInt, String paramString);
  
  public abstract String a(Message paramMessage);
  
  public abstract String a(a parama);
  
  public abstract String a(String paramString1, int paramInt, String paramString2);
  
  public abstract boolean a(String paramString);
  
  public abstract String b(Message paramMessage);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.i.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */