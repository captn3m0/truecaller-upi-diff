package com.truecaller.messaging.i;

import c.g.b.k;
import com.google.f.t;
import com.truecaller.api.services.messenger.v1.models.Peer;
import com.truecaller.api.services.messenger.v1.models.Peer.TypeCase;
import com.truecaller.api.services.messenger.v1.models.Peer.b;
import com.truecaller.api.services.messenger.v1.models.Peer.d;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Participant.a;

public final class i
{
  private static final Participant a(Peer.b paramb)
  {
    Participant.a locala = new com/truecaller/messaging/data/types/Participant$a;
    locala.<init>(4);
    paramb = paramb.a();
    paramb = locala.b(paramb).a();
    k.a(paramb, "Participant.Builder(Part…alizedAddress(id).build()");
    return paramb;
  }
  
  public static final Participant a(Peer.d paramd)
  {
    Object localObject1 = "receiver$0";
    k.b(paramd, (String)localObject1);
    boolean bool = paramd.b();
    int i;
    Object localObject2;
    if (bool)
    {
      localObject1 = new com/truecaller/messaging/data/types/Participant$a;
      i = 0;
      ((Participant.a)localObject1).<init>(0);
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>("+");
      t localt = paramd.c();
      String str = "phoneNumber";
      k.a(localt, str);
      long l = localt.getValue();
      ((StringBuilder)localObject2).append(l);
      localObject2 = ((StringBuilder)localObject2).toString();
      localObject1 = ((Participant.a)localObject1).b((String)localObject2);
    }
    else
    {
      localObject1 = new com/truecaller/messaging/data/types/Participant$a;
      i = 3;
      ((Participant.a)localObject1).<init>(i);
      localObject2 = paramd.a();
      localObject1 = ((Participant.a)localObject1).b((String)localObject2);
    }
    k.a(localObject1, "if (hasPhoneNumber()) {\n…rmalizedAddress(id)\n    }");
    paramd = paramd.a();
    paramd = ((Participant.a)localObject1).d(paramd).a();
    k.a(paramd, "builder.setImPeerId(id).build()");
    return paramd;
  }
  
  public static final Participant a(Peer paramPeer)
  {
    k.b(paramPeer, "receiver$0");
    Peer.TypeCase localTypeCase = paramPeer.a();
    if (localTypeCase != null)
    {
      int[] arrayOfInt = j.a;
      int i = localTypeCase.ordinal();
      i = arrayOfInt[i];
      int j = 1;
      if (i == j) {}
    }
    else
    {
      paramPeer = paramPeer.b();
      k.a(paramPeer, "user");
      return a(paramPeer);
    }
    paramPeer = paramPeer.c();
    k.a(paramPeer, "group");
    return a(paramPeer);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.i.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */