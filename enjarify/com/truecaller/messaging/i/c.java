package com.truecaller.messaging.i;

import android.content.res.Resources;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.a;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import c.g.b.k;
import c.n.m;

public final class c
{
  public String a;
  public Drawable b;
  public Drawable c;
  public Drawable d;
  public Drawable e;
  public Integer f;
  public String g;
  public Paint.FontMetricsInt h;
  public Integer i;
  private SpannableString j;
  private Resources k;
  
  public c()
  {
    this(localCharSequence);
  }
  
  public c(CharSequence paramCharSequence)
  {
    SpannableString localSpannableString = new android/text/SpannableString;
    localSpannableString.<init>(paramCharSequence);
    j = localSpannableString;
  }
  
  private final void a(Drawable paramDrawable, SpannableStringBuilder paramSpannableStringBuilder, Integer paramInteger)
  {
    Object localObject = h;
    int m;
    int i2;
    if (localObject != null)
    {
      m = ascent;
      int n = descent;
      int i1 = ascent;
      n -= i1;
      i2 = descent;
      paramDrawable.setBounds(0, m, n, i2);
    }
    else
    {
      i2 = paramDrawable.getIntrinsicWidth();
      m = paramDrawable.getIntrinsicHeight();
      paramDrawable.setBounds(0, 0, i2, m);
    }
    if (paramInteger != null)
    {
      paramInteger = (Number)paramInteger;
      i3 = paramInteger.intValue();
      a.a(paramDrawable, i3);
    }
    int i3 = paramSpannableStringBuilder.length();
    localObject = (CharSequence)"  ";
    paramSpannableStringBuilder.append((CharSequence)localObject);
    localObject = new android/text/style/ImageSpan;
    ((ImageSpan)localObject).<init>(paramDrawable, 0);
    int i4 = i3 + 2;
    paramSpannableStringBuilder.setSpan(localObject, i3, i4, 33);
    paramDrawable = (CharSequence)" ";
    paramSpannableStringBuilder.append(paramDrawable);
  }
  
  public final SpannableStringBuilder a()
  {
    SpannableStringBuilder localSpannableStringBuilder = new android/text/SpannableStringBuilder;
    localSpannableStringBuilder.<init>();
    Object localObject1 = g;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject1 = (CharSequence)localObject1;
      localObject1 = localSpannableStringBuilder.append((CharSequence)localObject1);
      localObject2 = (CharSequence)" ";
      ((SpannableStringBuilder)localObject1).append((CharSequence)localObject2);
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      localObject2 = i;
      a((Drawable)localObject1, localSpannableStringBuilder, (Integer)localObject2);
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      localObject2 = i;
      a((Drawable)localObject1, localSpannableStringBuilder, (Integer)localObject2);
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      localObject2 = null;
      a((Drawable)localObject1, localSpannableStringBuilder, null);
    }
    localObject1 = f;
    if (localObject1 != null)
    {
      ((Number)localObject1).intValue();
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("(");
      localObject2 = f;
      ((StringBuilder)localObject1).append(localObject2);
      localObject2 = ") ";
      ((StringBuilder)localObject1).append((String)localObject2);
      localObject1 = (CharSequence)((StringBuilder)localObject1).toString();
      localSpannableStringBuilder.append((CharSequence)localObject1);
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject2 = i;
      a((Drawable)localObject1, localSpannableStringBuilder, (Integer)localObject2);
    }
    localObject1 = a;
    if (localObject1 != null)
    {
      localObject2 = new android/text/SpannableString;
      localObject1 = (CharSequence)localObject1;
      ((SpannableString)localObject2).<init>((CharSequence)localObject1);
      localObject2 = (CharSequence)localObject2;
      localSpannableStringBuilder.append((CharSequence)localObject2);
    }
    localObject1 = (CharSequence)j;
    localObject1 = localSpannableStringBuilder.append((CharSequence)localObject1);
    k.a(localObject1, "builder.append(text)");
    m.b((CharSequence)localObject1);
    return localSpannableStringBuilder;
  }
  
  public final c a(Resources paramResources)
  {
    k.b(paramResources, "res");
    k = paramResources;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.i.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */