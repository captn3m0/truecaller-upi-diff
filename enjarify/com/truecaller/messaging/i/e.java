package com.truecaller.messaging.i;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import c.g.a.b;
import c.g.b.k;
import c.g.b.z;
import c.u;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.utils.n;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public final class e
  implements d
{
  private final n a;
  
  public e(n paramn)
  {
    a = paramn;
  }
  
  private static boolean b(String paramString)
  {
    String str = "text/html";
    boolean bool1 = k.a(str, paramString);
    if (!bool1)
    {
      str = "text/plain";
      boolean bool2 = k.a(str, paramString);
      if (!bool2) {
        return false;
      }
    }
    return true;
  }
  
  private static List c(Message paramMessage)
  {
    paramMessage = n;
    k.a(paramMessage, "entities");
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    int i = paramMessage.length;
    int j = 0;
    while (j < i)
    {
      String str = j;
      Object localObject2 = str;
      localObject2 = (CharSequence)str;
      if (localObject2 != null)
      {
        k = ((CharSequence)localObject2).length();
        if (k != 0)
        {
          k = 0;
          localObject2 = null;
          break label90;
        }
      }
      int k = 1;
      label90:
      if (k != 0) {
        str = null;
      }
      if (str != null) {
        ((Collection)localObject1).add(str);
      }
      j += 1;
    }
    return (List)localObject1;
  }
  
  public final int a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = paramInt1 & 0x1;
    if (i == 0) {
      return 0;
    }
    i = paramInt1 & 0x74;
    int j = 1;
    if (i != 0) {
      i = 1;
    } else {
      i = 0;
    }
    int k = 3;
    if (paramInt3 == k)
    {
      if (paramInt4 == 0) {
        return 2131234199;
      }
      return 2131234435;
    }
    if (paramInt2 == k)
    {
      if (paramInt4 == 0) {
        return 2131234197;
      }
      return 2131234041;
    }
    if (i != 0)
    {
      if (paramInt4 == 0) {
        return 2131234200;
      }
      return 2131234536;
    }
    if (paramInt1 == j)
    {
      if (paramInt4 == 0) {
        return 2131234201;
      }
      return 2131234537;
    }
    return 0;
  }
  
  public final int a(Message paramMessage, b paramb)
  {
    k.b(paramb, "negativePredicate");
    int i = 0;
    if (paramMessage != null)
    {
      boolean bool1 = paramMessage.c();
      if (bool1)
      {
        Entity[] arrayOfEntity = n;
        String str1 = "message.entities";
        k.a(arrayOfEntity, str1);
        int j = arrayOfEntity.length;
        int k = 0;
        while (i < j)
        {
          Object localObject = arrayOfEntity[i];
          String str2 = "it";
          k.a(localObject, str2);
          localObject = (Boolean)paramb.invoke(localObject);
          boolean bool2 = ((Boolean)localObject).booleanValue();
          if (bool2) {
            k += 1;
          }
          i += 1;
        }
        return n.length - k;
      }
    }
    return 0;
  }
  
  public final Drawable a(int paramInt1, int paramInt2, int paramInt3)
  {
    int i = 0;
    paramInt1 = a(paramInt1, paramInt2, paramInt3, 0);
    Integer localInteger = Integer.valueOf(paramInt1);
    Object localObject = localInteger;
    localObject = (Number)localInteger;
    paramInt2 = ((Number)localObject).intValue();
    if (paramInt2 > 0) {
      i = 1;
    }
    paramInt2 = 0;
    localObject = null;
    if (i == 0)
    {
      paramInt1 = 0;
      localInteger = null;
    }
    if (localInteger != null)
    {
      paramInt1 = ((Number)localInteger).intValue();
      return a.c(paramInt1);
    }
    return null;
  }
  
  public final Drawable a(int paramInt, String paramString)
  {
    int i = 1;
    if ((paramInt == i) && (paramString != null))
    {
      paramInt = Entity.c(paramString);
      if (paramInt != 0) {
        return a.c(2131234198);
      }
      paramInt = Entity.d(paramString);
      if (paramInt != 0) {
        return a.c(2131234202);
      }
      paramInt = Entity.f(paramString);
      if (paramInt != 0) {
        return a.c(2131234196);
      }
      paramInt = Entity.e(paramString);
      if (paramInt != 0) {
        return a.c(2131234203);
      }
      return null;
    }
    return null;
  }
  
  public final String a(Message paramMessage)
  {
    if (paramMessage == null) {
      return "";
    }
    Object localObject1 = (Iterable)c(paramMessage);
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (Collection)localObject2;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      Object localObject3 = ((Iterator)localObject1).next();
      Object localObject4 = localObject3;
      localObject4 = (String)localObject3;
      boolean bool2 = b((String)localObject4);
      if (!bool2) {
        ((Collection)localObject2).add(localObject3);
      }
    }
    localObject2 = (List)localObject2;
    paramMessage = paramMessage.j();
    int i = ((List)localObject2).size();
    localObject2 = (String)c.a.m.e((List)localObject2);
    return a(paramMessage, i, (String)localObject2);
  }
  
  public final String a(com.truecaller.util.c.a parama)
  {
    k.b(parama, "place");
    double d1 = c;
    double d2 = d;
    Object localObject1 = z.a;
    localObject1 = Locale.US;
    k.a(localObject1, "Locale.US");
    String str1 = "%s%.7f,%.7f";
    int i = 3;
    Object[] arrayOfObject = new Object[i];
    String str2 = "https://maps.google.com/maps?q=";
    arrayOfObject[0] = str2;
    Object localObject2 = Double.valueOf(d1);
    boolean bool1 = true;
    arrayOfObject[bool1] = localObject2;
    localObject2 = Double.valueOf(d2);
    int j = 2;
    arrayOfObject[j] = localObject2;
    localObject2 = Arrays.copyOf(arrayOfObject, i);
    localObject2 = String.format((Locale)localObject1, str1, (Object[])localObject2);
    k.a(localObject2, "java.lang.String.format(locale, format, *args)");
    localObject2 = Uri.parse((String)localObject2);
    k.a(localObject2, "Uri.parse(uriStr)");
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localObject2 = ((Uri)localObject2).toString();
    localStringBuilder.append((String)localObject2);
    localObject2 = a;
    parama = b;
    if (localObject2 != null)
    {
      k = ((CharSequence)localObject2).length();
      if (k != 0)
      {
        k = 0;
        break label199;
      }
    }
    int k = 1;
    label199:
    label233:
    String str3;
    if (k == 0)
    {
      if (parama != null)
      {
        k = parama.length();
        if (k != 0)
        {
          k = 0;
          break label233;
        }
      }
      k = 1;
      if (k == 0)
      {
        if (parama == null) {
          k.a();
        }
        if (localObject2 == null) {
          k.a();
        }
        boolean bool2 = c.n.m.a(parama, (CharSequence)localObject2, bool1);
        if (!bool2)
        {
          str3 = " ";
          localStringBuilder.append(str3);
          localStringBuilder.append((CharSequence)localObject2);
          localObject2 = ", ";
          localStringBuilder.append((String)localObject2);
          localStringBuilder.append(parama);
          break label418;
        }
      }
    }
    if (parama != null)
    {
      m = parama.length();
      if (m != 0)
      {
        m = 0;
        break label340;
      }
    }
    int m = 1;
    label340:
    if (m == 0)
    {
      localObject2 = " ";
      localStringBuilder.append((String)localObject2);
      localStringBuilder.append(parama);
    }
    else
    {
      if (localObject2 != null)
      {
        int n = ((CharSequence)localObject2).length();
        if (n != 0)
        {
          bool1 = false;
          str3 = null;
        }
      }
      if (!bool1)
      {
        parama = " ";
        localStringBuilder.append(parama);
        localStringBuilder.append((CharSequence)localObject2);
      }
    }
    label418:
    parama = localStringBuilder.toString();
    k.a(parama, "it.toString()");
    k.a(parama, "StringBuilder().let {\n  …  it.toString()\n        }");
    return parama;
  }
  
  public final String a(String paramString1, int paramInt, String paramString2)
  {
    if (paramString1 != null) {
      if (paramString1 != null)
      {
        paramString1 = c.n.m.b((CharSequence)paramString1).toString();
        if (paramString1 != null)
        {
          char c = '\n';
          i = 32;
          paramString1 = c.n.m.a(paramString1, c, i);
          if (paramString1 != null) {
            break label63;
          }
        }
      }
      else
      {
        paramString1 = new c/u;
        paramString1.<init>("null cannot be cast to non-null type kotlin.CharSequence");
        throw paramString1;
      }
    }
    paramString1 = "";
    label63:
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(paramString1);
    int j = 2131755025;
    int i = 1;
    Object localObject1;
    Object localObject2;
    if (paramInt > i)
    {
      paramString2 = a;
      localObject1 = new Object[i];
      Integer localInteger = Integer.valueOf(paramInt);
      localObject1[0] = localInteger;
      paramString1 = paramString2.a(j, paramInt, (Object[])localObject1);
      localObject2 = localStringBuilder;
      localObject2 = (CharSequence)localStringBuilder;
      paramInt = ((CharSequence)localObject2).length();
      if (paramInt <= 0) {
        i = 0;
      }
      if (i != 0)
      {
        localObject2 = ", ";
        localStringBuilder.append((String)localObject2);
      }
      localStringBuilder.append(paramString1);
    }
    else if ((paramInt == i) && (paramString2 != null))
    {
      localObject2 = localStringBuilder;
      localObject2 = (CharSequence)localStringBuilder;
      paramInt = ((CharSequence)localObject2).length();
      if (paramInt == 0)
      {
        paramInt = 1;
      }
      else
      {
        paramInt = 0;
        localObject2 = null;
      }
      if (paramInt != 0)
      {
        paramInt = Entity.g(paramString2);
        if (paramInt != 0)
        {
          paramString1 = a;
          paramInt = 2131886684;
          paramString2 = new Object[0];
          paramString1 = paramString1.a(paramInt, paramString2);
        }
        else
        {
          paramInt = Entity.c(paramString2);
          if (paramInt != 0)
          {
            paramString1 = a;
            paramInt = 2131886685;
            paramString2 = new Object[0];
            paramString1 = paramString1.a(paramInt, paramString2);
          }
          else
          {
            paramInt = Entity.d(paramString2);
            if (paramInt != 0)
            {
              paramString1 = a;
              paramInt = 2131886699;
              paramString2 = new Object[0];
              paramString1 = paramString1.a(paramInt, paramString2);
            }
            else
            {
              paramInt = Entity.f(paramString2);
              if (paramInt != 0)
              {
                paramString1 = a;
                paramInt = 2131886658;
                paramString2 = new Object[0];
                paramString1 = paramString1.a(paramInt, paramString2);
              }
              else
              {
                paramInt = Entity.e(paramString2);
                if (paramInt != 0)
                {
                  paramString1 = a;
                  paramInt = 2131886700;
                  paramString2 = new Object[0];
                  paramString1 = paramString1.a(paramInt, paramString2);
                }
                else
                {
                  localObject2 = a;
                  paramString2 = new Object[i];
                  localObject1 = Integer.valueOf(i);
                  paramString2[0] = localObject1;
                  paramString1 = ((n)localObject2).a(j, i, paramString2);
                }
              }
            }
          }
        }
        localStringBuilder.append(paramString1);
      }
    }
    paramString1 = localStringBuilder.toString();
    k.a(paramString1, "fullText.toString()");
    return paramString1;
  }
  
  public final boolean a(String paramString)
  {
    k.b(paramString, "text");
    return com.truecaller.android.truemoji.c.a.a((CharSequence)paramString);
  }
  
  public final String b(Message paramMessage)
  {
    k.b(paramMessage, "message");
    paramMessage = (Iterable)c(paramMessage);
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (Collection)localObject1;
    paramMessage = paramMessage.iterator();
    for (;;)
    {
      i = paramMessage.hasNext();
      if (i == 0) {
        break;
      }
      Object localObject2 = paramMessage.next();
      localObject3 = localObject2;
      localObject3 = (String)localObject2;
      bool1 = b((String)localObject3);
      if (!bool1) {
        ((Collection)localObject1).add(localObject2);
      }
    }
    localObject1 = (List)localObject1;
    int j = ((List)localObject1).size();
    int i = 1;
    boolean bool1 = false;
    Object localObject3 = null;
    if (j != i) {
      return null;
    }
    paramMessage = (String)c.a.m.d((List)localObject1);
    boolean bool3 = Entity.c(paramMessage);
    if (bool3) {
      return "🌄";
    }
    bool3 = Entity.d(paramMessage);
    if (bool3) {
      return "🎥";
    }
    bool3 = Entity.f(paramMessage);
    if (bool3) {
      return "👤";
    }
    boolean bool2 = Entity.e(paramMessage);
    if (bool2) {
      return "🎙";
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.i.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */