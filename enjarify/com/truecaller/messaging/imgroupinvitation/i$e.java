package com.truecaller.messaging.imgroupinvitation;

import c.g.a.b;
import c.g.b.j;
import c.g.b.w;
import c.l.c;

final class i$e
  extends j
  implements b
{
  i$e(i parami)
  {
    super(1, parami);
  }
  
  public final c a()
  {
    return w.a(i.class);
  }
  
  public final String b()
  {
    return "onAcceptInviteResult";
  }
  
  public final String c()
  {
    return "onAcceptInviteResult(Ljava/lang/Boolean;)V";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.imgroupinvitation.i.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */