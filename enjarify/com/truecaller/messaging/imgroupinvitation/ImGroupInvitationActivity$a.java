package com.truecaller.messaging.imgroupinvitation;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import c.g.b.k;
import com.truecaller.messaging.data.types.ImGroupInfo;

public final class ImGroupInvitationActivity$a
{
  public static Intent a(Context paramContext, ImGroupInfo paramImGroupInfo)
  {
    k.b(paramContext, "context");
    k.b(paramImGroupInfo, "groupInfo");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, ImGroupInvitationActivity.class);
    paramImGroupInfo = (Parcelable)paramImGroupInfo;
    paramContext = localIntent.putExtra("group_info", paramImGroupInfo);
    k.a(paramContext, "Intent(context, ImGroupI…RA_GROUP_INFO, groupInfo)");
    return paramContext;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.imgroupinvitation.ImGroupInvitationActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */