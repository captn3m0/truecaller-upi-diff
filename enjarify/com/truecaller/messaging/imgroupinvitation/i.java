package com.truecaller.messaging.imgroupinvitation;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.Participant.a;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.im.a.a;
import com.truecaller.tracking.events.q;
import com.truecaller.tracking.events.q.a;
import com.truecaller.util.aa;
import com.truecaller.utils.n;
import org.apache.a.d.d;

public final class i
  extends h.a
{
  private final i.a a;
  private final com.truecaller.androidactors.i c;
  private final ImGroupInfo d;
  private final f e;
  private final n f;
  private final f g;
  private final com.truecaller.messaging.transport.im.a.i h;
  private final f i;
  private final f j;
  private final com.truecaller.analytics.b k;
  private final h l;
  private final ContentResolver m;
  private final Uri n;
  
  public i(com.truecaller.androidactors.i parami, ImGroupInfo paramImGroupInfo, f paramf1, n paramn, f paramf2, com.truecaller.messaging.transport.im.a.i parami1, f paramf3, f paramf4, com.truecaller.analytics.b paramb, h paramh, ContentResolver paramContentResolver, Uri paramUri)
  {
    c = parami;
    d = paramImGroupInfo;
    e = paramf1;
    f = paramn;
    g = paramf2;
    h = parami1;
    i = paramf3;
    j = paramf4;
    k = paramb;
    l = paramh;
    m = paramContentResolver;
    n = paramUri;
    parami = new com/truecaller/messaging/imgroupinvitation/i$a;
    paramImGroupInfo = new android/os/Handler;
    paramf1 = Looper.getMainLooper();
    paramImGroupInfo.<init>(paramf1);
    parami.<init>(this, paramImGroupInfo);
    a = parami;
  }
  
  private final void a(ImGroupInfo paramImGroupInfo)
  {
    if (paramImGroupInfo == null) {
      return;
    }
    Object localObject1 = (h.b)b;
    if (localObject1 == null) {
      return;
    }
    int i1 = f & 0x2;
    if (i1 == 0)
    {
      b(paramImGroupInfo);
      return;
    }
    Object localObject2 = b;
    if (localObject2 == null) {
      localObject2 = "";
    }
    ((h.b)localObject1).a((String)localObject2);
    localObject2 = c;
    if (localObject2 != null)
    {
      localObject2 = Uri.parse((String)localObject2);
    }
    else
    {
      i1 = 0;
      localObject2 = null;
    }
    ((h.b)localObject1).a((Uri)localObject2);
    localObject2 = f;
    int i2 = 1;
    Object localObject3 = new Object[i2];
    String str = b;
    if (str == null) {
      str = "";
    }
    int i3 = 2131886607;
    localObject3[0] = str;
    localObject2 = ((n)localObject2).a(i3, (Object[])localObject3);
    localObject3 = "resourceProvider.getStri…roupInfo.title.orEmpty())";
    c.g.b.k.a(localObject2, (String)localObject3);
    ((h.b)localObject1).b((String)localObject2);
    paramImGroupInfo = e;
    if (paramImGroupInfo != null)
    {
      paramImGroupInfo = ((aa)g.a()).b(paramImGroupInfo);
      localObject1 = c;
      localObject2 = new com/truecaller/messaging/imgroupinvitation/i$d;
      localObject3 = this;
      localObject3 = (i)this;
      ((i.d)localObject2).<init>((i)localObject3);
      localObject2 = (c.g.a.b)localObject2;
      localObject3 = new com/truecaller/messaging/imgroupinvitation/k;
      ((k)localObject3).<init>((c.g.a.b)localObject2);
      localObject3 = (ac)localObject3;
      paramImGroupInfo.a((com.truecaller.androidactors.i)localObject1, (ac)localObject3);
      return;
    }
  }
  
  private final void a(String paramString, Boolean paramBoolean)
  {
    Object localObject1 = Boolean.TRUE;
    boolean bool1 = c.g.b.k.a(paramBoolean, localObject1);
    if (bool1)
    {
      localObject1 = q.b();
      localObject2 = (CharSequence)d.a;
      localObject1 = ((q.a)localObject1).a((CharSequence)localObject2);
      localObject2 = d.e;
      if (localObject2 == null) {
        localObject2 = "";
      }
      localObject2 = (CharSequence)localObject2;
      localObject1 = ((q.a)localObject1).b((CharSequence)localObject2);
      localObject2 = l.G();
      if (localObject2 == null) {
        localObject2 = "";
      }
      localObject2 = (CharSequence)localObject2;
      localObject1 = ((q.a)localObject1).c((CharSequence)localObject2);
      localObject2 = paramString;
      localObject2 = (CharSequence)paramString;
      localObject1 = ((q.a)localObject1).d((CharSequence)localObject2);
      localObject2 = (ae)j.a();
      localObject1 = (d)((q.a)localObject1).a();
      ((ae)localObject2).a((d)localObject1);
    }
    localObject1 = new com/truecaller/analytics/e$a;
    ((e.a)localObject1).<init>("IMGroupInvite");
    paramString = ((e.a)localObject1).a("action", paramString);
    localObject1 = "status";
    Object localObject2 = Boolean.TRUE;
    boolean bool2 = c.g.b.k.a(paramBoolean, localObject2);
    if (bool2) {
      paramBoolean = "Success";
    } else {
      paramBoolean = "Failure";
    }
    paramString = paramString.a((String)localObject1, paramBoolean);
    paramBoolean = k;
    paramString = paramString.a();
    c.g.b.k.a(paramString, "it.build()");
    paramBoolean.b(paramString);
  }
  
  private final void b(ImGroupInfo paramImGroupInfo)
  {
    Object localObject = new com/truecaller/messaging/data/types/Participant$a;
    int i1 = 4;
    ((Participant.a)localObject).<init>(i1);
    paramImGroupInfo = a;
    paramImGroupInfo = ((Participant.a)localObject).b(paramImGroupInfo).a();
    c.g.b.k.a(paramImGroupInfo, "Participant.Builder(Part…pId)\n            .build()");
    localObject = (h.b)b;
    if (localObject != null)
    {
      ((h.b)localObject).b();
      ((h.b)localObject).a(paramImGroupInfo);
      return;
    }
  }
  
  private final void g()
  {
    Object localObject1 = (a)e.a();
    Object localObject2 = d.a;
    localObject1 = ((a)localObject1).e((String)localObject2);
    localObject2 = c;
    Object localObject3 = new com/truecaller/messaging/imgroupinvitation/i$b;
    Object localObject4 = this;
    localObject4 = (i)this;
    ((i.b)localObject3).<init>((i)localObject4);
    localObject3 = (c.g.a.b)localObject3;
    localObject4 = new com/truecaller/messaging/imgroupinvitation/j;
    ((j)localObject4).<init>((c.g.a.b)localObject3);
    localObject4 = (ac)localObject4;
    ((w)localObject1).a((com.truecaller.androidactors.i)localObject2, (ac)localObject4);
  }
  
  public final void a()
  {
    ContentResolver localContentResolver = m;
    Uri localUri = n;
    ContentObserver localContentObserver = (ContentObserver)a;
    localContentResolver.registerContentObserver(localUri, true, localContentObserver);
    g();
  }
  
  public final void b()
  {
    ContentResolver localContentResolver = m;
    ContentObserver localContentObserver = (ContentObserver)a;
    localContentResolver.unregisterContentObserver(localContentObserver);
  }
  
  public final boolean c()
  {
    h.b localb = (h.b)b;
    if (localb != null) {
      localb.b();
    }
    return true;
  }
  
  public final void e()
  {
    Object localObject1 = (h.b)b;
    if (localObject1 == null) {
      return;
    }
    ((h.b)localObject1).a(false);
    ((h.b)localObject1).b(true);
    localObject1 = (a)e.a();
    Object localObject2 = d.a;
    localObject1 = ((a)localObject1).d((String)localObject2);
    localObject2 = c;
    Object localObject3 = new com/truecaller/messaging/imgroupinvitation/i$e;
    Object localObject4 = this;
    localObject4 = (i)this;
    ((i.e)localObject3).<init>((i)localObject4);
    localObject3 = (c.g.a.b)localObject3;
    localObject4 = new com/truecaller/messaging/imgroupinvitation/j;
    ((j)localObject4).<init>((c.g.a.b)localObject3);
    localObject4 = (ac)localObject4;
    ((w)localObject1).a((com.truecaller.androidactors.i)localObject2, (ac)localObject4);
  }
  
  public final void f()
  {
    Object localObject1 = (a)e.a();
    Object localObject2 = d.a;
    localObject1 = ((a)localObject1).a((String)localObject2, true);
    localObject2 = c;
    Object localObject3 = new com/truecaller/messaging/imgroupinvitation/i$c;
    Object localObject4 = this;
    localObject4 = (i)this;
    ((i.c)localObject3).<init>((i)localObject4);
    localObject3 = (c.g.a.b)localObject3;
    localObject4 = new com/truecaller/messaging/imgroupinvitation/j;
    ((j)localObject4).<init>((c.g.a.b)localObject3);
    localObject4 = (ac)localObject4;
    ((w)localObject1).a((com.truecaller.androidactors.i)localObject2, (ac)localObject4);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.imgroupinvitation.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */