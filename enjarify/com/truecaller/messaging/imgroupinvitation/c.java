package com.truecaller.messaging.imgroupinvitation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import c.g.b.k;
import c.u;
import com.truecaller.R.id;
import com.truecaller.bk;
import com.truecaller.messaging.conversation.ConversationActivity;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.ui.view.ContactPhoto;
import java.util.HashMap;

public final class c
  extends Fragment
  implements h.b
{
  public static final c.a b;
  public h.a a;
  private HashMap c;
  
  static
  {
    c.a locala = new com/truecaller/messaging/imgroupinvitation/c$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final h.a a()
  {
    h.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void a(Uri paramUri)
  {
    int i = R.id.contact_photo;
    ((ContactPhoto)a(i)).a(paramUri, null);
  }
  
  public final void a(Participant paramParticipant)
  {
    k.b(paramParticipant, "participant");
    Intent localIntent = new android/content/Intent;
    Context localContext = getContext();
    localIntent.<init>(localContext, ConversationActivity.class);
    Object localObject = new Participant[1];
    localObject[0] = paramParticipant;
    localObject = (Parcelable[])localObject;
    localIntent.putExtra("participants", (Parcelable[])localObject);
    startActivity(localIntent);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "title");
    int i = R.id.toolbar;
    Toolbar localToolbar = (Toolbar)a(i);
    k.a(localToolbar, "toolbar");
    paramString = (CharSequence)paramString;
    localToolbar.setTitle(paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.join_button;
    Button localButton = (Button)a(i);
    k.a(localButton, "join_button");
    int j = 0;
    int k;
    if (paramBoolean)
    {
      k = 0;
      str = null;
    }
    else
    {
      k = 4;
    }
    localButton.setVisibility(k);
    i = R.id.decline_button;
    localButton = (Button)a(i);
    String str = "decline_button";
    k.a(localButton, str);
    if (!paramBoolean) {
      j = 4;
    }
    localButton.setVisibility(j);
  }
  
  public final void b()
  {
    f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "title");
    int i = R.id.title_text;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "title_text");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    int i = R.id.progress_bar;
    ProgressBar localProgressBar = (ProgressBar)a(i);
    String str = "progress_bar";
    k.a(localProgressBar, str);
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localProgressBar.setVisibility(paramBoolean);
  }
  
  public final void c()
  {
    Toast.makeText(getContext(), 2131886538, 1).show();
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "description");
    int i = R.id.description_text;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "description_text");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getActivity();
    if (paramBundle != null)
    {
      paramBundle = (Activity)paramBundle;
      Object localObject1 = getArguments();
      if (localObject1 != null)
      {
        Object localObject2 = "group_info";
        localObject1 = (ImGroupInfo)((Bundle)localObject1).getParcelable((String)localObject2);
        if (localObject1 != null)
        {
          localObject2 = a.a();
          paramBundle = paramBundle.getApplicationContext();
          if (paramBundle != null)
          {
            paramBundle = ((bk)paramBundle).a();
            paramBundle = ((a.a)localObject2).a(paramBundle);
            localObject2 = new com/truecaller/messaging/imgroupinvitation/d;
            ((d)localObject2).<init>((ImGroupInfo)localObject1);
            paramBundle.a((d)localObject2).a().a(this);
            return;
          }
          paramBundle = new c/u;
          paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
          throw paramBundle;
        }
      }
      return;
    }
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558710, paramViewGroup, false);
  }
  
  public final void onDestroyView()
  {
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((h.a)localObject).y_();
    super.onDestroyView();
    localObject = c;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onPause()
  {
    h.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.b();
    super.onPause();
  }
  
  public final void onResume()
  {
    super.onResume();
    h.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.a();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    paramView = getActivity();
    boolean bool = paramView instanceof AppCompatActivity;
    if (!bool)
    {
      j = 0;
      paramView = null;
    }
    paramView = (AppCompatActivity)paramView;
    if (paramView == null) {
      return;
    }
    int i = R.id.toolbar;
    paramBundle = (Toolbar)a(i);
    paramView.setSupportActionBar(paramBundle);
    paramView = paramView.getSupportActionBar();
    if (paramView != null)
    {
      paramView.setDisplayHomeAsUpEnabled(true);
      i = 0;
      paramBundle = null;
      paramView.setDisplayShowTitleEnabled(false);
    }
    int j = R.id.toolbar;
    paramView = (Toolbar)a(j);
    paramBundle = new com/truecaller/messaging/imgroupinvitation/c$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setNavigationOnClickListener(paramBundle);
    j = R.id.join_button;
    paramView = (Button)a(j);
    paramBundle = new com/truecaller/messaging/imgroupinvitation/c$c;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    j = R.id.decline_button;
    paramView = (Button)a(j);
    paramBundle = new com/truecaller/messaging/imgroupinvitation/c$d;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    j = R.id.contact_photo;
    ((ContactPhoto)a(j)).a();
    j = R.id.contact_photo;
    paramView = (ContactPhoto)a(j);
    i = 2131230873;
    paramView.setDrawableRes(i);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.imgroupinvitation.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */