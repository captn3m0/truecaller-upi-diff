package com.truecaller.messaging.imgroupinvitation;

import android.net.Uri;
import com.truecaller.messaging.data.types.Participant;

public abstract interface h$b
{
  public abstract void a(Uri paramUri);
  
  public abstract void a(Participant paramParticipant);
  
  public abstract void a(String paramString);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b();
  
  public abstract void b(String paramString);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c();
  
  public abstract void c(String paramString);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.imgroupinvitation.h.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */