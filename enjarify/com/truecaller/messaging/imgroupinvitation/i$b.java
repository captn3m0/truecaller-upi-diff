package com.truecaller.messaging.imgroupinvitation;

import c.g.a.b;
import c.g.b.j;
import c.g.b.w;
import c.l.c;

final class i$b
  extends j
  implements b
{
  i$b(i parami)
  {
    super(1, parami);
  }
  
  public final c a()
  {
    return w.a(i.class);
  }
  
  public final String b()
  {
    return "onGroupInfo";
  }
  
  public final String c()
  {
    return "onGroupInfo(Lcom/truecaller/messaging/data/types/ImGroupInfo;)V";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.imgroupinvitation.i.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */