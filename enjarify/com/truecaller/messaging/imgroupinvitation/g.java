package com.truecaller.messaging.imgroupinvitation;

import javax.inject.Provider;

public final class g
  implements dagger.a.d
{
  private final d a;
  private final Provider b;
  
  private g(d paramd, Provider paramProvider)
  {
    a = paramd;
    b = paramProvider;
  }
  
  public static g a(d paramd, Provider paramProvider)
  {
    g localg = new com/truecaller/messaging/imgroupinvitation/g;
    localg.<init>(paramd, paramProvider);
    return localg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.imgroupinvitation.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */