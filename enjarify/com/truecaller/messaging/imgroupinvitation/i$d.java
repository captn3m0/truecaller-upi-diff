package com.truecaller.messaging.imgroupinvitation;

import c.g.a.b;
import c.g.b.j;
import c.g.b.w;
import c.l.c;

final class i$d
  extends j
  implements b
{
  i$d(i parami)
  {
    super(1, parami);
  }
  
  public final c a()
  {
    return w.a(i.class);
  }
  
  public final String b()
  {
    return "onInvitedByContactResult";
  }
  
  public final String c()
  {
    return "onInvitedByContactResult(Lcom/truecaller/data/entity/Contact;)V";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.imgroupinvitation.i.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */