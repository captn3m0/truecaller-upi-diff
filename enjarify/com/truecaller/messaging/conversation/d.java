package com.truecaller.messaging.conversation;

import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Reaction;

public final class d
{
  public Reaction[] A;
  public boolean B;
  public boolean C;
  public boolean D;
  public int E;
  public final cj a;
  public final AttachmentType b;
  public final Message c;
  public final Entity d;
  public final String e;
  public final int f;
  public final int g;
  public final String h;
  public final boolean i;
  public final boolean j;
  public final boolean k;
  public final boolean l;
  public final boolean m;
  public final boolean n;
  public final int o;
  public final int p;
  public final int q;
  public final int r;
  public final String s;
  public final int t;
  public final String u;
  public final boolean v;
  public final String w;
  public final String x;
  public final boolean y;
  public final boolean z;
  
  private d(d.a parama)
  {
    Object localObject = a;
    a = ((cj)localObject);
    localObject = b;
    b = ((AttachmentType)localObject);
    localObject = c;
    c = ((Message)localObject);
    localObject = d;
    d = ((Entity)localObject);
    localObject = e;
    e = ((String)localObject);
    int i1 = f;
    f = i1;
    localObject = k;
    h = ((String)localObject);
    boolean bool1 = l;
    i = bool1;
    bool1 = m;
    j = bool1;
    int i2 = n;
    o = i2;
    i2 = g;
    g = i2;
    boolean bool2 = o;
    k = bool2;
    bool2 = p;
    l = bool2;
    bool2 = q;
    m = bool2;
    bool2 = r;
    n = bool2;
    int i3 = s;
    p = i3;
    i3 = t;
    q = i3;
    localObject = u;
    u = ((String)localObject);
    i3 = h;
    r = i3;
    localObject = i;
    s = ((String)localObject);
    i3 = j;
    t = i3;
    localObject = v;
    w = ((String)localObject);
    localObject = w;
    x = ((String)localObject);
    boolean bool3 = x;
    v = bool3;
    bool3 = y;
    y = bool3;
    bool3 = z;
    z = bool3;
    localObject = A;
    A = ((Reaction[])localObject);
    bool3 = B;
    B = bool3;
    bool3 = C;
    C = bool3;
    bool3 = D;
    D = bool3;
    int i4 = E;
    E = i4;
  }
  
  public final d.a a()
  {
    d.a locala = new com/truecaller/messaging/conversation/d$a;
    locala.<init>();
    Object localObject = a;
    a = ((cj)localObject);
    localObject = b;
    b = ((AttachmentType)localObject);
    localObject = c;
    c = ((Message)localObject);
    localObject = d;
    locala = locala.a((Entity)localObject);
    localObject = e;
    e = ((String)localObject);
    int i1 = f;
    f = i1;
    i1 = g;
    g = i1;
    localObject = h;
    k = ((String)localObject);
    boolean bool1 = i;
    l = bool1;
    bool1 = j;
    m = bool1;
    int i2 = o;
    n = i2;
    boolean bool2 = k;
    o = bool2;
    int i3 = p;
    s = i3;
    localObject = w;
    v = ((String)localObject);
    localObject = x;
    w = ((String)localObject);
    boolean bool3 = l;
    boolean bool4 = n;
    locala = locala.a(bool3, bool4);
    bool3 = y;
    y = bool3;
    bool3 = z;
    z = bool3;
    localObject = A;
    A = ((Reaction[])localObject);
    bool3 = B;
    B = bool3;
    bool3 = C;
    C = bool3;
    bool3 = D;
    D = bool3;
    int i4 = E;
    E = i4;
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */