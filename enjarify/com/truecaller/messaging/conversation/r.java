package com.truecaller.messaging.conversation;

import android.net.Uri;
import com.truecaller.flashsdk.models.FlashContact;
import com.truecaller.messaging.d.b;
import com.truecaller.messaging.data.types.ReplySnippet;
import java.util.ArrayList;
import java.util.List;

public abstract interface r
  extends b, com.truecaller.messaging.g.j
{
  public static final r.a o = r.a.a;
  
  public abstract void B();
  
  public abstract void C();
  
  public abstract void E();
  
  public abstract void G();
  
  public abstract void J();
  
  public abstract void K();
  
  public abstract void L();
  
  public abstract void M();
  
  public abstract boolean N();
  
  public abstract void O();
  
  public abstract void P();
  
  public abstract void W();
  
  public abstract void a(int paramInt1, int paramInt2);
  
  public abstract void a(com.truecaller.android.truemoji.j paramj);
  
  public abstract void a(FlashContact paramFlashContact);
  
  public abstract void a(ReplySnippet paramReplySnippet);
  
  public abstract void a(String paramString1, String paramString2);
  
  public abstract void a(List paramList);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void a(boolean paramBoolean, int paramInt1, int paramInt2);
  
  public abstract void a(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3);
  
  public abstract void b();
  
  public abstract void b(Uri paramUri);
  
  public abstract void b(ArrayList paramArrayList);
  
  public abstract void c();
  
  public abstract void c(String paramString);
  
  public abstract void d();
  
  public abstract void d(int paramInt);
  
  public abstract void e(int paramInt);
  
  public abstract void e(boolean paramBoolean);
  
  public abstract CharSequence f();
  
  public abstract void g();
  
  public abstract void j(boolean paramBoolean);
  
  public abstract void k(boolean paramBoolean);
  
  public abstract boolean k(String paramString);
  
  public abstract void l();
  
  public abstract void l(boolean paramBoolean);
  
  public abstract void m(int paramInt);
  
  public abstract void m(boolean paramBoolean);
  
  public abstract void o(boolean paramBoolean);
  
  public abstract void p(boolean paramBoolean);
  
  public abstract void q(int paramInt);
  
  public abstract void q(boolean paramBoolean);
  
  public abstract boolean q();
  
  public abstract void r();
  
  public abstract void r(int paramInt);
  
  public abstract void r(boolean paramBoolean);
  
  public abstract void x();
  
  public abstract void y();
  
  public abstract void z();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */