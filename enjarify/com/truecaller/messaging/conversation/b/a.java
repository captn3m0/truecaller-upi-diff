package com.truecaller.messaging.conversation.b;

import android.graphics.PorterDuff.Mode;
import android.view.View;
import android.widget.ImageView;
import com.truecaller.R.id;
import com.truecaller.utils.ui.b;
import java.util.HashMap;

public final class a
  extends c
  implements kotlinx.a.a.a
{
  public a.a a;
  private HashMap c;
  
  public a(View paramView)
  {
    super(paramView);
    int i = R.id.iconLink;
    paramView = (ImageView)a(i);
    int j = b.a(b.getContext(), 2130969220);
    PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
    paramView.setColorFilter(j, localMode);
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      c = ((HashMap)localObject1);
    }
    localObject1 = c;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = c;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */