package com.truecaller.messaging.conversation.b;

import android.util.SparseArray;
import android.view.View;
import com.truecaller.log.AssertionUtil;

public final class b
{
  private SparseArray a;
  
  public b()
  {
    SparseArray localSparseArray = new android/util/SparseArray;
    localSparseArray.<init>();
    a = localSparseArray;
  }
  
  public static void a(c paramc)
  {
    Object localObject = b;
    int i = 2131364652;
    localObject = (d)((View)localObject).getTag(i);
    if (localObject != null) {
      ((d)localObject).b(paramc);
    }
  }
  
  public final c a(int paramInt)
  {
    d locald = (d)a.get(paramInt);
    Object localObject = new String[0];
    AssertionUtil.isNotNull(locald, (String[])localObject);
    localObject = locald.c();
    b.setTag(2131364652, locald);
    return (c)localObject;
  }
  
  public final void a(int paramInt, d paramd)
  {
    a.put(paramInt, paramd);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.b.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */