package com.truecaller.messaging.conversation.b;

import android.support.v4.f.l.b;

public abstract class d
  extends l.b
{
  private int a = 0;
  
  public d()
  {
    super(12);
  }
  
  protected abstract void a(c paramc);
  
  protected abstract c b();
  
  public final boolean b(c paramc)
  {
    int i = a + -1;
    a = i;
    boolean bool = super.a(paramc);
    if (bool) {
      a(paramc);
    }
    return bool;
  }
  
  public final c c()
  {
    int i = a + 1;
    a = i;
    c localc = (c)super.a();
    if (localc == null) {
      localc = b();
    }
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */