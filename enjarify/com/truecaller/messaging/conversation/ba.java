package com.truecaller.messaging.conversation;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import c.g.b.k;

public abstract class ba
  extends RecyclerView.OnScrollListener
{
  private int a;
  private boolean b;
  private final int c;
  
  public ba(int paramInt)
  {
    c = paramInt;
  }
  
  public abstract int a();
  
  public abstract void b();
  
  public abstract void c();
  
  public void onScrolled(RecyclerView paramRecyclerView, int paramInt1, int paramInt2)
  {
    String str = "recyclerView";
    k.b(paramRecyclerView, str);
    if (paramInt2 == 0) {
      return;
    }
    int i = a();
    str = null;
    switch (i)
    {
    default: 
      if (paramInt2 >= 0) {
        return;
      }
      i = a + paramInt2;
      a = i;
      i = a;
      paramInt2 = -c;
      if (i >= paramInt2) {
        return;
      }
      a = 0;
      bool = b;
      if (!bool) {
        c();
      }
      break;
    case 0: 
      bool = b;
      if (bool) {
        b();
      }
      b = false;
      return;
    case -1: 
      return;
    }
    boolean bool = true;
    b = bool;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.ba
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */