package com.truecaller.messaging.conversation;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnItemTouchListener;
import android.view.MotionEvent;

final class k$4
  implements RecyclerView.OnItemTouchListener
{
  k$4(k paramk) {}
  
  public final boolean onInterceptTouchEvent(RecyclerView paramRecyclerView, MotionEvent paramMotionEvent)
  {
    int i = paramMotionEvent.getAction();
    int j = 1;
    if (i != j) {
      return false;
    }
    float f1 = paramMotionEvent.getX();
    float f2 = paramMotionEvent.getY();
    paramRecyclerView = paramRecyclerView.findChildViewUnder(f1, f2);
    if (paramRecyclerView == null)
    {
      a.a.v();
      return j;
    }
    return false;
  }
  
  public final void onRequestDisallowInterceptTouchEvent(boolean paramBoolean) {}
  
  public final void onTouchEvent(RecyclerView paramRecyclerView, MotionEvent paramMotionEvent) {}
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.k.4
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */