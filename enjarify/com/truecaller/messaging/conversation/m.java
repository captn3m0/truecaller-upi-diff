package com.truecaller.messaging.conversation;

import android.net.Uri;
import android.view.Menu;
import com.truecaller.bm;
import com.truecaller.messaging.data.types.Draft;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.util.c.a;
import java.util.ArrayList;
import java.util.List;

public abstract interface m
  extends bm, bn.a, e.b
{
  public abstract boolean A();
  
  public abstract void B();
  
  public abstract boolean C();
  
  public abstract void D();
  
  public abstract void E();
  
  public abstract void F();
  
  public abstract void G();
  
  public abstract int H();
  
  public abstract void I();
  
  public abstract void J();
  
  public abstract void a(int paramInt);
  
  public abstract void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt);
  
  public abstract void a(Uri paramUri);
  
  public abstract void a(Menu paramMenu);
  
  public abstract void a(Draft paramDraft);
  
  public abstract void a(Message paramMessage);
  
  public abstract void a(CharSequence paramCharSequence);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString1, Message paramMessage, String paramString2);
  
  public abstract void a(String paramString, ArrayList paramArrayList);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void a(boolean paramBoolean, Uri paramUri);
  
  public abstract void a(boolean paramBoolean, a parama);
  
  public abstract void a(boolean paramBoolean, List paramList);
  
  public abstract void b(Uri paramUri);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void g();
  
  public abstract void h();
  
  public abstract void i();
  
  public abstract void j();
  
  public abstract boolean k();
  
  public abstract void l();
  
  public abstract void m();
  
  public abstract void n();
  
  public abstract void o();
  
  public abstract void p();
  
  public abstract void q();
  
  public abstract void r();
  
  public abstract void s();
  
  public abstract void t();
  
  public abstract Draft u();
  
  public abstract void v();
  
  public abstract boolean w();
  
  public abstract boolean x();
  
  public abstract void y();
  
  public abstract void z();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */