package com.truecaller.messaging.conversation;

import android.os.SystemClock;
import android.widget.Chronometer;
import com.truecaller.R.id;
import com.truecaller.messaging.conversation.voice_notes.RecordToastView;
import com.truecaller.messaging.conversation.voice_notes.RecordView.a;

final class k$5
  implements RecordView.a
{
  k$5(k paramk) {}
  
  public final void a()
  {
    a.a.C();
    a.b.p();
    RecordToastView localRecordToastView = k.a(a);
    int i = R.id.chronometerCounter;
    Chronometer localChronometer = (Chronometer)localRecordToastView.a(i);
    c.g.b.k.a(localChronometer, "chronometerCounter");
    long l = SystemClock.elapsedRealtime();
    localChronometer.setBase(l);
    i = R.id.chronometerCounter;
    ((Chronometer)localRecordToastView.a(i)).start();
    localRecordToastView.b();
  }
  
  public final void a(String paramString)
  {
    a.b.a(paramString);
  }
  
  public final void b()
  {
    a.b.q();
  }
  
  public final void c()
  {
    a.b.r();
  }
  
  public final void d()
  {
    a.b.s();
  }
  
  public final void e()
  {
    a.b.t();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.k.5
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */