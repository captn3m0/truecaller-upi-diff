package com.truecaller.messaging.conversation;

import dagger.a.d;
import javax.inject.Provider;

public final class ci
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private ci(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static ci a(Provider paramProvider1, Provider paramProvider2)
  {
    ci localci = new com/truecaller/messaging/conversation/ci;
    localci.<init>(paramProvider1, paramProvider2);
    return localci;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.ci
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */