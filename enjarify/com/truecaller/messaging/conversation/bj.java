package com.truecaller.messaging.conversation;

import c.a.f;
import c.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.i.a;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class bj
  implements bi
{
  private boolean a;
  private boolean b;
  private final Comparator c;
  private final Map d;
  private Participant[] e;
  private ImGroupInfo f;
  
  public bj()
  {
    Object localObject = (Comparator)bj.a.a;
    c = ((Comparator)localObject);
    localObject = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject).<init>();
    localObject = (Map)localObject;
    d = ((Map)localObject);
    localObject = new Participant[0];
    e = ((Participant[])localObject);
  }
  
  public final int a()
  {
    Participant[] arrayOfParticipant = e;
    if (arrayOfParticipant != null) {
      return arrayOfParticipant.length;
    }
    return 0;
  }
  
  public final void a(ImGroupInfo paramImGroupInfo)
  {
    f = paramImGroupInfo;
  }
  
  public final void a(Message paramMessage)
  {
    k.b(paramMessage, "message");
    Map localMap = d;
    Long localLong = Long.valueOf(paramMessage.a());
    localMap.put(localLong, paramMessage);
  }
  
  public final void a(boolean paramBoolean)
  {
    a = paramBoolean;
  }
  
  public final void a(Participant[] paramArrayOfParticipant)
  {
    e = paramArrayOfParticipant;
  }
  
  public final boolean a(long paramLong)
  {
    Map localMap = d;
    Long localLong = Long.valueOf(paramLong);
    return localMap.containsKey(localLong);
  }
  
  public final void b(long paramLong)
  {
    Map localMap = d;
    Long localLong = Long.valueOf(paramLong);
    localMap.remove(localLong);
  }
  
  public final void b(boolean paramBoolean)
  {
    b = paramBoolean;
  }
  
  public final Participant[] b()
  {
    return e;
  }
  
  public final boolean c()
  {
    return a;
  }
  
  public final boolean d()
  {
    return b;
  }
  
  public final int e()
  {
    return d.size();
  }
  
  public final boolean f()
  {
    Map localMap = d;
    boolean bool = localMap.isEmpty();
    return !bool;
  }
  
  public final boolean g()
  {
    Object localObject = e;
    if (localObject != null)
    {
      localObject = (Participant)f.c((Object[])localObject);
      if (localObject != null)
      {
        int i = c;
        int j = 4;
        if (i == j)
        {
          localObject = f;
          if (localObject != null)
          {
            boolean bool = a.a((ImGroupInfo)localObject);
            if (!bool) {}
          }
          else
          {
            return false;
          }
        }
      }
    }
    return true;
  }
  
  public final void h()
  {
    d.clear();
  }
  
  public final Message i()
  {
    return (Message)((Map.Entry)d.entrySet().iterator().next()).getValue();
  }
  
  public final Collection j()
  {
    return (Collection)d.keySet();
  }
  
  public final Message[] k()
  {
    Object localObject1 = (Iterable)d.values();
    Object localObject2 = c;
    localObject1 = (Collection)m.a((Iterable)localObject1, (Comparator)localObject2);
    if (localObject1 != null)
    {
      localObject2 = new Message[0];
      localObject1 = ((Collection)localObject1).toArray((Object[])localObject2);
      if (localObject1 != null) {
        return (Message[])localObject1;
      }
      localObject1 = new c/u;
      ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.Array<T>");
      throw ((Throwable)localObject1);
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type java.util.Collection<T>");
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.bj
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */