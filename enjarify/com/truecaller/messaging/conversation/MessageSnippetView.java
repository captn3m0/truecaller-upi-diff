package com.truecaller.messaging.conversation;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.Uri.Builder;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import c.g.b.k;
import com.bumptech.glide.load.d.a.e;
import com.bumptech.glide.load.d.a.g;
import com.bumptech.glide.load.h;
import com.truecaller.R.id;
import com.truecaller.R.styleable;
import com.truecaller.android.truemoji.EmojiTextView;
import com.truecaller.bg;
import com.truecaller.bi;
import com.truecaller.bj;
import com.truecaller.common.h.ah;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.ImageEntity;
import com.truecaller.messaging.data.types.ReplySnippet;
import com.truecaller.messaging.data.types.VideoEntity;
import com.truecaller.profile.c;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.utils.extensions.t;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class MessageSnippetView
  extends RelativeLayout
{
  private boolean a;
  private final com.truecaller.common.g.a b;
  private final int c;
  private HashMap d;
  
  public MessageSnippetView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private MessageSnippetView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    Object localObject1 = paramContext.getApplicationContext();
    if (localObject1 != null)
    {
      localObject1 = ((com.truecaller.common.b.a)localObject1).u().c();
      b = ((com.truecaller.common.g.a)localObject1);
      int i = paramContext.getResources().getDimensionPixelSize(2131166204);
      c = i;
      Object localObject2 = this;
      localObject2 = (ViewGroup)this;
      RelativeLayout.inflate(paramContext, 2131559210, (ViewGroup)localObject2);
      setBackgroundResource(2131230868);
      i = c;
      setPaddingRelative(i, i, i, i);
      localObject1 = R.styleable.MessageSnippetView;
      paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject1, 0, 0);
      i = paramAttributeSet.getInt(0, 0);
      int j = 1;
      if (i == j) {
        paramChar = '\001';
      }
      a = paramChar;
      paramChar = a;
      int k;
      if (paramChar != 0)
      {
        paramChar = R.id.snippet_sender_text;
        localEmojiTextView = (EmojiTextView)a(paramChar);
        i = com.truecaller.utils.ui.b.a(paramContext, 2130969233);
        localEmojiTextView.setTextColor(i);
        paramChar = R.id.snippet_content_text;
        localEmojiTextView = (EmojiTextView)a(paramChar);
        i = 2130969227;
        k = com.truecaller.utils.ui.b.a(paramContext, i);
        localEmojiTextView.setTextColor(k);
        paramContext = this;
        paramContext = (View)this;
        paramChar = 2130969232;
        com.truecaller.utils.ui.b.a(paramContext, paramChar);
      }
      else
      {
        paramChar = R.id.snippet_sender_text;
        localEmojiTextView = (EmojiTextView)a(paramChar);
        i = com.truecaller.utils.ui.b.a(paramContext, 2130968790);
        localEmojiTextView.setTextColor(i);
        paramChar = R.id.snippet_content_text;
        localEmojiTextView = (EmojiTextView)a(paramChar);
        i = 2130969222;
        k = com.truecaller.utils.ui.b.a(paramContext, i);
        localEmojiTextView.setTextColor(k);
        paramContext = this;
        paramContext = (View)this;
        paramChar = 2130969231;
        com.truecaller.utils.ui.b.a(paramContext, paramChar);
      }
      paramAttributeSet.recycle();
      return;
    }
    paramContext = new c/u;
    paramContext.<init>("null cannot be cast to non-null type com.truecaller.common.app.ApplicationBase");
    throw paramContext;
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      d = ((HashMap)localObject1);
    }
    localObject1 = d;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = d;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  private final String a(ReplySnippet paramReplySnippet)
  {
    int i = b;
    int j = 1;
    if (i == j) {
      return c.b(b);
    }
    String str = d;
    if (str == null) {
      str = e;
    }
    return str;
  }
  
  private final void a(ImageEntity paramImageEntity)
  {
    paramImageEntity = b;
    k.a(paramImageEntity, "entity.content");
    setThumbnailPreview(paramImageEntity);
    int i = R.id.snippet_content_text;
    paramImageEntity = (EmojiTextView)a(i);
    k.a(paramImageEntity, "snippet_content_text");
    CharSequence localCharSequence = (CharSequence)getContext().getString(2131886681);
    paramImageEntity.setText(localCharSequence);
  }
  
  private final void a(VideoEntity paramVideoEntity)
  {
    paramVideoEntity = b.buildUpon().appendQueryParameter("com.truecaller.util.VideoRequestHandler.IS_VIDEO", "true").build();
    k.a(paramVideoEntity, "preview");
    setThumbnailPreview(paramVideoEntity);
    int i = R.id.snippet_content_text;
    paramVideoEntity = (EmojiTextView)a(i);
    k.a(paramVideoEntity, "snippet_content_text");
    CharSequence localCharSequence = (CharSequence)getContext().getString(2131886683);
    paramVideoEntity.setText(localCharSequence);
  }
  
  private final void b()
  {
    int i = R.id.attachment_image_view;
    Object localObject1 = (ImageView)a(i);
    k.a(localObject1, "attachment_image_view");
    t.a((View)localObject1, true);
    i = R.id.attachment_image_view;
    localObject1 = (ImageView)a(i);
    k.a(localObject1, "attachment_image_view");
    ((ImageView)localObject1).setBackground(null);
    i = R.id.attachment_image_view;
    localObject1 = (ImageView)a(i);
    int k = 2131234587;
    ((ImageView)localObject1).setImageResource(k);
    i = R.id.snippet_content_text;
    localObject1 = (EmojiTextView)a(i);
    k.a(localObject1, "snippet_content_text");
    Object localObject2 = getContext();
    int m = 2131886682;
    localObject2 = (CharSequence)((Context)localObject2).getString(m);
    ((EmojiTextView)localObject1).setText((CharSequence)localObject2);
    boolean bool = a;
    int j;
    if (bool) {
      j = 2130969237;
    } else {
      j = 2130969235;
    }
    localObject2 = getContext();
    m = R.id.attachment_image_view;
    ImageView localImageView = (ImageView)a(m);
    com.truecaller.utils.ui.b.a((Context)localObject2, localImageView, j);
  }
  
  private final void c()
  {
    int i = R.id.attachment_image_view;
    Object localObject1 = (ImageView)a(i);
    k.a(localObject1, "attachment_image_view");
    t.a((View)localObject1, false);
    i = R.id.snippet_content_text;
    localObject1 = (EmojiTextView)a(i);
    k.a(localObject1, "snippet_content_text");
    Object localObject2 = getContext();
    int j = 2131886700;
    localObject2 = (CharSequence)((Context)localObject2).getString(j);
    ((EmojiTextView)localObject1).setText((CharSequence)localObject2);
    boolean bool = a;
    int k = 2131234203;
    if (bool)
    {
      localObject1 = getContext();
      j = 2130969237;
      localObject1 = com.truecaller.utils.ui.b.a((Context)localObject1, k, j);
    }
    else
    {
      localObject1 = android.support.v4.content.b.a(getContext(), k);
    }
    k = R.id.snippet_content_text;
    ((EmojiTextView)a(k)).setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable)localObject1, null, null, null);
  }
  
  private final void setThumbnailPreview(Uri paramUri)
  {
    int i = R.id.attachment_image_view;
    Object localObject1 = (ImageView)a(i);
    k.a(localObject1, "attachment_image_view");
    localObject1 = (View)localObject1;
    boolean bool = true;
    t.a((View)localObject1, bool);
    paramUri = bg.a(getContext()).b(paramUri);
    localObject1 = getContext();
    k.a(localObject1, "context");
    i = ((Context)localObject1).getResources().getDimensionPixelSize(2131166136);
    paramUri = paramUri.g(i);
    localObject1 = new com/bumptech/glide/load/h;
    e[] arrayOfe = new e[2];
    Object localObject2 = new com/bumptech/glide/load/d/a/g;
    ((g)localObject2).<init>();
    localObject2 = (e)localObject2;
    arrayOfe[0] = localObject2;
    localObject2 = new com/bumptech/glide/load/d/a/u;
    Context localContext = getContext();
    k.a(localContext, "context");
    int j = localContext.getResources().getDimensionPixelSize(2131166135);
    ((com.bumptech.glide.load.d.a.u)localObject2).<init>(j);
    localObject2 = (e)localObject2;
    arrayOfe[bool] = localObject2;
    Collection localCollection = (Collection)c.a.m.b(arrayOfe);
    ((h)localObject1).<init>(localCollection);
    localObject1 = (com.bumptech.glide.load.m)localObject1;
    paramUri = paramUri.b((com.bumptech.glide.load.m)localObject1);
    i = R.id.attachment_image_view;
    localObject1 = (ImageView)a(i);
    paramUri.a((ImageView)localObject1);
  }
  
  public final void a()
  {
    int i = R.id.snippet_sender_text;
    Object localObject1 = (EmojiTextView)a(i);
    k.a(localObject1, "snippet_sender_text");
    t.a((View)localObject1, false);
    i = R.id.snippet_content_text;
    localObject1 = (EmojiTextView)a(i);
    k.a(localObject1, "snippet_content_text");
    Object localObject2 = getContext();
    int j = 2131886659;
    localObject2 = (CharSequence)((Context)localObject2).getString(j);
    ((EmojiTextView)localObject1).setText((CharSequence)localObject2);
    i = R.id.snippet_content_text;
    localObject1 = (EmojiTextView)a(i);
    k.a(localObject1, "snippet_content_text");
    int k = 1060320051;
    float f = 0.7F;
    ((EmojiTextView)localObject1).setAlpha(f);
    i = R.id.snippet_content_text;
    localObject1 = (EmojiTextView)a(i);
    localObject2 = "snippet_content_text";
    k.a(localObject1, (String)localObject2);
    localObject1 = ((EmojiTextView)localObject1).getLayoutParams();
    if (localObject1 != null)
    {
      localObject1 = (RelativeLayout.LayoutParams)localObject1;
      ((RelativeLayout.LayoutParams)localObject1).addRule(15, -1);
      k = R.id.snippet_content_text;
      localObject2 = (EmojiTextView)a(k);
      k.a(localObject2, "snippet_content_text");
      localObject1 = (ViewGroup.LayoutParams)localObject1;
      ((EmojiTextView)localObject2).setLayoutParams((ViewGroup.LayoutParams)localObject1);
      i = R.id.attachment_image_view;
      localObject1 = (ImageView)a(i);
      k.a(localObject1, "attachment_image_view");
      t.a((View)localObject1, false);
      setEnabled(false);
      return;
    }
    localObject1 = new c/u;
    ((c.u)localObject1).<init>("null cannot be cast to non-null type android.widget.RelativeLayout.LayoutParams");
    throw ((Throwable)localObject1);
  }
  
  public final void a(ReplySnippet paramReplySnippet, boolean paramBoolean)
  {
    k.b(paramReplySnippet, "message");
    int i = R.id.snippet_sender_text;
    Object localObject1 = (EmojiTextView)a(i);
    String str = "snippet_sender_text";
    k.a(localObject1, str);
    localObject1 = (View)localObject1;
    boolean bool1 = true;
    t.a((View)localObject1, bool1);
    i = R.id.snippet_sender_text;
    localObject1 = (EmojiTextView)a(i);
    k.a(localObject1, "snippet_sender_text");
    CharSequence localCharSequence = (CharSequence)a(paramReplySnippet);
    ((EmojiTextView)localObject1).setText(localCharSequence);
    i = R.id.attachment_image_view;
    localObject1 = (ImageView)a(i);
    k.a(localObject1, "attachment_image_view");
    localObject1 = (View)localObject1;
    localCharSequence = null;
    t.a((View)localObject1, false);
    i = R.id.snippet_content_text;
    ((EmojiTextView)a(i)).setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
    localObject1 = c.iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      Object localObject2 = (Entity)((Iterator)localObject1).next();
      boolean bool3 = ((Entity)localObject2).b();
      if (bool3)
      {
        if (localObject2 != null)
        {
          localObject2 = (ImageEntity)localObject2;
          a((ImageEntity)localObject2);
        }
        else
        {
          paramReplySnippet = new c/u;
          paramReplySnippet.<init>("null cannot be cast to non-null type com.truecaller.messaging.data.types.ImageEntity");
          throw paramReplySnippet;
        }
      }
      else
      {
        bool3 = ((Entity)localObject2).c();
        if (bool3)
        {
          if (localObject2 != null)
          {
            localObject2 = (VideoEntity)localObject2;
            a((VideoEntity)localObject2);
          }
          else
          {
            paramReplySnippet = new c/u;
            paramReplySnippet.<init>("null cannot be cast to non-null type com.truecaller.messaging.data.types.VideoEntity");
            throw paramReplySnippet;
          }
        }
        else
        {
          bool3 = ((Entity)localObject2).d();
          if (bool3)
          {
            b();
          }
          else
          {
            bool2 = ((Entity)localObject2).e();
            if (bool2) {
              c();
            }
          }
        }
      }
    }
    setEnabled(paramBoolean);
    paramBoolean = R.id.snippet_content_text;
    Object localObject3 = (EmojiTextView)a(paramBoolean);
    localObject1 = "snippet_content_text";
    k.a(localObject3, (String)localObject1);
    i = 1065353216;
    float f = 1.0F;
    ((EmojiTextView)localObject3).setAlpha(f);
    paramReplySnippet = paramReplySnippet.a();
    localObject3 = paramReplySnippet;
    localObject3 = (CharSequence)paramReplySnippet;
    paramBoolean = ((CharSequence)localObject3).length();
    if (!paramBoolean)
    {
      bool1 = false;
      str = null;
    }
    if (bool1)
    {
      paramBoolean = R.id.snippet_content_text;
      localObject3 = (EmojiTextView)a(paramBoolean);
      localObject1 = "snippet_content_text";
      k.a(localObject3, (String)localObject1);
      paramReplySnippet = (CharSequence)ah.a(paramReplySnippet);
      ((EmojiTextView)localObject3).setText(paramReplySnippet);
    }
  }
  
  public final void setDismissActionListener(View.OnClickListener paramOnClickListener)
  {
    k.b(paramOnClickListener, "clickListener");
    int i = R.id.dismiss_button;
    ((TintedImageView)a(i)).setOnClickListener(paramOnClickListener);
  }
  
  public final void setDismissActionVisible(boolean paramBoolean)
  {
    int i = R.id.dismiss_button;
    TintedImageView localTintedImageView = (TintedImageView)a(i);
    k.a(localTintedImageView, "dismiss_button");
    t.a((View)localTintedImageView, paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.MessageSnippetView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */