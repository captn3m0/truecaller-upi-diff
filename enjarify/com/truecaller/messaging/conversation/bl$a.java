package com.truecaller.messaging.conversation;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.support.v4.widget.m;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.load.d.a.g;
import com.bumptech.glide.load.d.a.u;
import com.bumptech.glide.load.h;
import com.google.common.collect.Lists;
import com.truecaller.bi;
import com.truecaller.bj;
import com.truecaller.ui.components.CyclicProgressBar;
import com.truecaller.ui.components.d.c;
import java.util.Collection;

final class bl$a
  extends d.c
  implements bm
{
  private final ImageView a;
  private final ImageView c;
  private final ImageView d;
  private final TextView e;
  private final CyclicProgressBar f;
  
  public bl$a(View paramView, bn parambn)
  {
    super(paramView);
    Object localObject = (ImageView)paramView.findViewById(2131363373);
    a = ((ImageView)localObject);
    localObject = (ImageView)paramView.findViewById(2131362556);
    c = ((ImageView)localObject);
    localObject = (TextView)paramView.findViewById(2131364826);
    e = ((TextView)localObject);
    localObject = (CyclicProgressBar)paramView.findViewById(2131363686);
    f = ((CyclicProgressBar)localObject);
    paramView = (ImageView)paramView.findViewById(2131362803);
    d = paramView;
    paramView = d;
    localObject = new com/truecaller/messaging/conversation/-$$Lambda$bl$a$naUcKWCgbEMSV1_EHQrztzQNe0w;
    ((-..Lambda.bl.a.naUcKWCgbEMSV1_EHQrztzQNe0w)localObject).<init>(this, parambn);
    paramView.setOnClickListener((View.OnClickListener)localObject);
  }
  
  public final void a()
  {
    a.setImageResource(0);
  }
  
  public final void a(int paramInt)
  {
    m.a(e, paramInt, 0);
  }
  
  public final void a(Uri paramUri, int paramInt1, int paramInt2)
  {
    a.setImageDrawable(null);
    Object localObject1 = itemView.getContext();
    paramUri = ((bj)com.bumptech.glide.e.b((Context)localObject1)).b(paramUri).e(paramInt2).f(paramInt2);
    paramInt2 = ((Context)localObject1).getResources().getDimensionPixelSize(2131165654);
    paramUri = paramUri.g(paramInt2);
    h localh = new com/bumptech/glide/load/h;
    localObject1 = new com.bumptech.glide.load.d.a.e[2];
    Object localObject2 = new com/bumptech/glide/load/d/a/g;
    ((g)localObject2).<init>();
    localObject1[0] = localObject2;
    localObject2 = new com/bumptech/glide/load/d/a/u;
    ((u)localObject2).<init>(paramInt1);
    localObject1[1] = localObject2;
    Object localObject3 = Lists.newArrayList((Object[])localObject1);
    localh.<init>((Collection)localObject3);
    paramUri = paramUri.b(localh);
    localObject3 = a;
    paramUri.a((ImageView)localObject3);
  }
  
  public final void a(String paramString)
  {
    e.setText(paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    ImageView localImageView = c;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localImageView.setVisibility(paramBoolean);
  }
  
  public final void b(boolean paramBoolean)
  {
    CyclicProgressBar localCyclicProgressBar = f;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localCyclicProgressBar.setVisibility(paramBoolean);
  }
  
  public final void c(boolean paramBoolean)
  {
    ImageView localImageView = d;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localImageView.setVisibility(paramBoolean);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.bl.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */