package com.truecaller.messaging.conversation;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.e;
import android.support.v7.app.AlertDialog.Builder;

public final class cl
  extends e
{
  m a;
  
  private void a(DialogInterface paramDialogInterface, int paramInt)
  {
    switch (paramInt)
    {
    default: 
      break;
    case -1: 
      paramDialogInterface = a;
      paramDialogInterface.h();
      break;
    case -2: 
      paramDialogInterface = a;
      paramDialogInterface.i();
    }
    dismiss();
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    ((f)paramContext).a().a(this);
  }
  
  public final Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new android/support/v7/app/AlertDialog$Builder;
    Object localObject = getContext();
    paramBundle.<init>((Context)localObject);
    paramBundle = paramBundle.setTitle(2131886515).setMessage(2131886514);
    localObject = new com/truecaller/messaging/conversation/-$$Lambda$cl$BnzLduPtlnOfduO9mkyHUHMLrok;
    ((-..Lambda.cl.BnzLduPtlnOfduO9mkyHUHMLrok)localObject).<init>(this);
    paramBundle = paramBundle.setPositiveButton(2131886513, (DialogInterface.OnClickListener)localObject);
    localObject = new com/truecaller/messaging/conversation/-$$Lambda$cl$BnzLduPtlnOfduO9mkyHUHMLrok;
    ((-..Lambda.cl.BnzLduPtlnOfduO9mkyHUHMLrok)localObject).<init>(this);
    return paramBundle.setNegativeButton(2131886512, (DialogInterface.OnClickListener)localObject).create();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.cl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */