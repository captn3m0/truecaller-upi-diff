package com.truecaller.messaging.conversation;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.data.entity.Contact;
import com.truecaller.messaging.conversation.a.b.g.a;
import com.truecaller.messaging.conversation.a.b.o;
import com.truecaller.messaging.data.types.Message;

abstract class bb
  extends com.truecaller.bb
  implements a, g.a, m.a
{
  abstract void a(int paramInt1, int paramInt2);
  
  abstract void a(int paramInt1, int paramInt2, Intent paramIntent);
  
  abstract void a(int paramInt, Message... paramVarArgs);
  
  abstract void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt);
  
  abstract void a(Bundle paramBundle);
  
  public abstract void a(Menu paramMenu);
  
  abstract void a(Contact paramContact, byte[] paramArrayOfByte);
  
  abstract void a(CallType paramCallType);
  
  public abstract void a(o paramo);
  
  public abstract void a(Message paramMessage, int paramInt);
  
  abstract void a(String paramString, TruecallerContract.Filters.EntityType paramEntityType);
  
  abstract void a(boolean paramBoolean);
  
  abstract void a(boolean paramBoolean1, boolean paramBoolean2);
  
  abstract void a(boolean paramBoolean1, boolean paramBoolean2, long... paramVarArgs);
  
  abstract void a(Message... paramVarArgs);
  
  abstract void b(Bundle paramBundle);
  
  abstract void b(boolean paramBoolean1, boolean paramBoolean2);
  
  abstract boolean b(int paramInt);
  
  public abstract boolean c(int paramInt);
  
  public abstract void d(int paramInt);
  
  abstract void e(int paramInt);
  
  abstract void h();
  
  abstract void i();
  
  abstract void j();
  
  abstract void k();
  
  abstract void l();
  
  abstract void m();
  
  abstract void n();
  
  abstract void o();
  
  abstract void p();
  
  abstract void q();
  
  abstract void r();
  
  abstract void s();
  
  public abstract void t();
  
  public abstract void u();
  
  abstract void v();
  
  abstract void w();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.bb
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */