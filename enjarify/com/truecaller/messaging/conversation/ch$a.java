package com.truecaller.messaging.conversation;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SimpleAdapter;

final class ch$a
  implements AdapterView.OnItemClickListener
{
  ch$a(View paramView, SimpleAdapter paramSimpleAdapter, cf paramcf) {}
  
  public final void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    if (paramInt != 0) {
      paramAdapterView = CallType.VOIP;
    } else {
      paramAdapterView = CallType.PHONE;
    }
    c.onCallClicked(paramAdapterView);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.ch.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */