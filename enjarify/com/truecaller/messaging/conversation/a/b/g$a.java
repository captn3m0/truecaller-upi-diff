package com.truecaller.messaging.conversation.a.b;

import com.truecaller.messaging.conversation.b.a.b;
import com.truecaller.messaging.conversation.voice_notes.e;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.ReplySnippet;

public abstract interface g$a
  extends a.b
{
  public abstract void A();
  
  public abstract void B();
  
  public abstract void C();
  
  public abstract void a(int paramInt, Message paramMessage, boolean paramBoolean);
  
  public abstract void a(Entity paramEntity);
  
  public abstract void a(Entity paramEntity, e parame);
  
  public abstract void a(Message paramMessage);
  
  public abstract void a(Message paramMessage, Entity paramEntity);
  
  public abstract void a(Message paramMessage, int[] paramArrayOfInt1, int[] paramArrayOfInt2);
  
  public abstract void a(ReplySnippet paramReplySnippet);
  
  public abstract void a(String paramString);
  
  public abstract void b(Message paramMessage);
  
  public abstract void b(Message paramMessage, Entity paramEntity);
  
  public abstract void b(String paramString);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c(Message paramMessage);
  
  public abstract void c(String paramString);
  
  public abstract void d(Message paramMessage);
  
  public abstract void e(Message paramMessage);
  
  public abstract void f(int paramInt);
  
  public abstract void f(Message paramMessage);
  
  public abstract void x();
  
  public abstract void y();
  
  public abstract void z();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.a.b.g.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */