package com.truecaller.messaging.conversation.a.b;

import com.truecaller.messaging.conversation.b.a.a;
import com.truecaller.messaging.conversation.b.a.b;

public final class p
  implements a.a
{
  private final k a;
  private final a.b b;
  
  public p(k paramk, a.b paramb)
  {
    a = paramk;
    b = paramb;
  }
  
  public final void a(o paramo, boolean paramBoolean)
  {
    c.g.b.k.b(paramo, "spamResult");
    String str1 = a.c;
    a.a(str1, paramBoolean);
    b.a(paramo, paramBoolean);
    b.D();
    k localk = a;
    paramo = b;
    String str2;
    if (paramBoolean) {
      str2 = "spam";
    } else {
      str2 = "notSpam";
    }
    localk.a(paramo, str2, str1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.a.b.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */