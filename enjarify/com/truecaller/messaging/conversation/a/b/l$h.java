package com.truecaller.messaging.conversation.a.b;

import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.ak;
import com.truecaller.messaging.conversation.spamLinks.UrlReportDto;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.ag;

final class l$h
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag d;
  
  l$h(l paraml, List paramList, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    h localh = new com/truecaller/messaging/conversation/a/b/l$h;
    l locall = b;
    List localList = c;
    localh.<init>(locall, localList, paramc);
    paramObject = (ag)paramObject;
    d = ((ag)paramObject);
    return localh;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = new java/util/ArrayList;
        ((ArrayList)paramObject).<init>();
        localObject1 = ((Iterable)c).iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          localObject2 = (UrlReportDto)((Iterator)localObject1).next();
          ContentProviderOperation.Builder localBuilder = ContentProviderOperation.newUpdate(TruecallerContract.ak.a());
          Object localObject3 = Integer.valueOf(2);
          localBuilder = localBuilder.withValue("sync_status", localObject3);
          String str = "url = ?";
          int j = 1;
          localObject3 = new String[j];
          localObject2 = ((UrlReportDto)localObject2).getUrl();
          localObject3[0] = localObject2;
          localObject2 = localBuilder.withSelection(str, (String[])localObject3).build();
          ((ArrayList)paramObject).add(localObject2);
        }
        localObject1 = b.d;
        Object localObject2 = TruecallerContract.a();
        ((ContentResolver)localObject1).applyBatch((String)localObject2, (ArrayList)paramObject);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (h)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((h)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.a.b.l.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */