package com.truecaller.messaging.conversation.a.b;

import android.net.Uri;
import android.net.Uri.Builder;
import c.a.ae;
import c.k.i;
import c.u;
import com.truecaller.common.h.ah;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.conversation.AttachmentType;
import com.truecaller.messaging.conversation.b.a.a;
import com.truecaller.messaging.conversation.b.a.b;
import com.truecaller.messaging.conversation.bf;
import com.truecaller.messaging.conversation.bh;
import com.truecaller.messaging.conversation.ca;
import com.truecaller.messaging.conversation.cj;
import com.truecaller.messaging.conversation.d.a;
import com.truecaller.messaging.data.types.AudioEntity;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.ImageEntity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Reaction;
import com.truecaller.messaging.data.types.VideoEntity;
import com.truecaller.messaging.transport.im.ImTransportInfo;
import com.truecaller.messaging.transport.im.bu;
import com.truecaller.messaging.transport.m;
import com.truecaller.util.af;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class h
  extends a
  implements a.a
{
  final g.a g;
  private final com.truecaller.util.g h;
  private final af i;
  private final com.truecaller.messaging.i.d j;
  private final bu k;
  private final k l;
  private final e m;
  
  public h(bh parambh, bf parambf, com.avito.konveyor.c.a parama, m paramm, g.a parama1, com.truecaller.util.g paramg, af paramaf, com.truecaller.messaging.i.d paramd, cj paramcj, bu parambu, k paramk, e parame)
  {
    super(parambh, parambf, paramm, paramcj, parama, parama1, parame);
    localObject2 = new com/truecaller/messaging/conversation/a/b/p;
    localObject1 = parama1;
    localObject1 = (a.b)parama1;
    ((p)localObject2).<init>(paramk, (a.b)localObject1);
    n = ((p)localObject2);
    g = parama1;
    h = paramg;
    i = paramaf;
    j = paramd;
    k = parambu;
    l = paramk;
    m = parame;
  }
  
  private final void a(Message paramMessage, ca paramca)
  {
    Object localObject = null;
    paramca.i(false);
    boolean bool = paramMessage.g();
    if (bool)
    {
      paramMessage = v;
      if (paramMessage != null)
      {
        c.g.b.k.a(paramMessage, "snippet");
        localObject = new com/truecaller/messaging/conversation/a/b/h$a;
        ((h.a)localObject).<init>(paramMessage, this, paramca);
        localObject = (c.g.a.b)localObject;
        paramca.a(paramMessage, (c.g.a.b)localObject);
        return;
      }
      paramca.g();
      return;
    }
    paramca.i(false);
  }
  
  private final boolean c(Message paramMessage)
  {
    boolean bool1 = f(paramMessage);
    if (!bool1)
    {
      paramMessage = a.b();
      if (paramMessage != null)
      {
        bool1 = com.truecaller.messaging.i.h.a(paramMessage);
        boolean bool2 = true;
        if (bool1 == bool2) {
          return bool2;
        }
      }
    }
    return false;
  }
  
  private final int d(Message paramMessage)
  {
    boolean bool = f(paramMessage);
    if (!bool) {
      return b.c();
    }
    bf localbf = b;
    int i1 = e(paramMessage);
    return localbf.d(i1);
  }
  
  private static int e(Message paramMessage)
  {
    int i1 = k;
    int i2 = 3;
    if (i1 != i2) {
      return k;
    }
    return j;
  }
  
  private static boolean f(Message paramMessage)
  {
    int i1 = f;
    int i2 = 1;
    i1 &= i2;
    if (i1 != 0) {
      return i2;
    }
    return false;
  }
  
  public final void a(o paramo, boolean paramBoolean)
  {
    c.g.b.k.b(paramo, "spamResult");
    n.a(paramo, paramBoolean);
  }
  
  public final void a(ca paramca, Message paramMessage, int paramInt)
  {
    h localh = this;
    Object localObject1 = paramca;
    Message localMessage = paramMessage;
    int i1 = paramInt;
    c.g.b.k.b(paramca, "view");
    c.g.b.k.b(paramMessage, "item");
    super.a(paramca, paramMessage, paramInt);
    Object localObject2 = new com/truecaller/messaging/conversation/d$a;
    ((d.a)localObject2).<init>();
    Object localObject3 = d;
    localObject2 = ((d.a)localObject2).a((cj)localObject3);
    int i12 = b.b(paramMessage);
    localObject2 = ((d.a)localObject2).a(i12);
    localObject3 = i;
    Object localObject4 = e;
    c.g.b.k.a(localObject4, "item.date");
    long l1 = a;
    localObject3 = ((af)localObject3).h(l1);
    localObject2 = ((d.a)localObject2).b((String)localObject3);
    String str = paramMessage.j();
    c.g.b.k.a(str, "item.buildMessageText()");
    localObject3 = str;
    localObject3 = (CharSequence)str;
    Object localObject5 = new c/n/k;
    ((c.n.k)localObject5).<init>("\\s+");
    Object localObject6 = ((c.n.k)localObject5).a((CharSequence)localObject3, "");
    localObject4 = j;
    localObject5 = (c.g.a.b)h.c.a;
    int i20 = ((com.truecaller.messaging.i.d)localObject4).a(paramMessage, (c.g.a.b)localObject5);
    int i22 = 0;
    Object localObject7 = null;
    if (i20 == 0)
    {
      i20 = 1;
    }
    else
    {
      i20 = 0;
      localObject4 = null;
    }
    localObject5 = j;
    boolean bool14 = ((com.truecaller.messaging.i.d)localObject5).a((String)localObject6);
    if ((bool14) && (i20 != 0))
    {
      bool14 = paramMessage.g();
      if (!bool14)
      {
        i25 = 1;
        break label267;
      }
    }
    int i25 = 0;
    label267:
    localObject5 = m.F();
    bool14 = ((com.truecaller.featuretoggles.b)localObject5).a();
    if (bool14)
    {
      localObject5 = m;
      bool14 = localObject5 instanceof ImTransportInfo;
      if (bool14)
      {
        localObject5 = m).k;
        break label332;
      }
    }
    bool14 = false;
    localObject5 = null;
    label332:
    if (localObject5 != null)
    {
      i26 = localObject5.length;
      if (i26 == 0)
      {
        i26 = 1;
      }
      else
      {
        i26 = 0;
        localObject8 = null;
      }
      if (i26 == 0)
      {
        i26 = 0;
        localObject8 = null;
        break label379;
      }
    }
    int i26 = 1;
    label379:
    int i28 = 2;
    if (i26 != 0)
    {
      i26 = j;
      if (i26 == i28)
      {
        bool15 = f(paramMessage);
        if (!bool15)
        {
          localObject8 = m.F();
          bool15 = ((com.truecaller.featuretoggles.b)localObject8).a();
          if (bool15)
          {
            bool15 = true;
            break label448;
          }
        }
      }
    }
    boolean bool15 = false;
    Object localObject8 = null;
    label448:
    if (bool15)
    {
      if (i1 == 0)
      {
        localObject8 = e.a(i1);
        boolean bool16 = localObject8 instanceof Message;
        if (!bool16)
        {
          bool15 = false;
          localObject8 = null;
        }
        localObject8 = (Message)localObject8;
        if (localObject8 != null)
        {
          bool15 = true;
        }
        else
        {
          bool15 = false;
          localObject8 = null;
        }
      }
      else
      {
        localObject8 = e;
        int i29 = i1 + -1;
        localObject8 = ((com.avito.konveyor.c.a)localObject8).a(i29);
        bool17 = localObject8 instanceof Message;
        if (!bool17)
        {
          bool15 = false;
          localObject8 = null;
        }
        localObject8 = (Message)localObject8;
        if (localObject8 == null)
        {
          bool15 = true;
        }
        else
        {
          bool15 = false;
          localObject8 = null;
        }
      }
      if ((bool15) && (i25 == 0))
      {
        localObject8 = a;
        bool15 = ((bh)localObject8).g();
        if (bool15)
        {
          bool15 = true;
          break label626;
        }
      }
    }
    bool15 = false;
    localObject8 = null;
    label626:
    boolean bool17 = f(paramMessage);
    int i33;
    int i30;
    if (bool17)
    {
      localObject9 = b;
      i33 = e(paramMessage);
      i30 = ((bf)localObject9).e(i33);
      i33 = i30;
    }
    else
    {
      localObject9 = b;
      i30 = ((bf)localObject9).d();
      i33 = i30;
    }
    boolean bool18 = f(paramMessage);
    if (bool18)
    {
      localObject9 = b;
      int i34 = e(paramMessage);
      i31 = ((bf)localObject9).e(i34);
    }
    else
    {
      localObject9 = b;
      i31 = ((bf)localObject9).d();
    }
    localObject2 = ((d.a)localObject2).c(i31);
    int i31 = localh.d(localMessage);
    localObject2 = ((d.a)localObject2).f(i31);
    boolean bool19 = f(paramMessage);
    localObject2 = ((d.a)localObject2).b(bool19);
    Object localObject9 = b;
    int i32 = ((bf)localObject9).a(localMessage);
    localObject2 = ((d.a)localObject2).b(i32).a((Reaction[])localObject5).i(bool15);
    bool14 = b(paramMessage);
    localObject2 = ((d.a)localObject2).j(bool14);
    int i23 = ((CharSequence)localObject3).length();
    if (i23 > 0)
    {
      i23 = 1;
    }
    else
    {
      i23 = 0;
      localObject5 = null;
    }
    if ((i23 != 0) && (i25 == 0))
    {
      i23 = 1;
    }
    else
    {
      i23 = 0;
      localObject5 = null;
    }
    localObject2 = ((d.a)localObject2).h(i23);
    Object localObject10 = ((d.a)localObject2).h(i1);
    com.truecaller.messaging.conversation.d locald = ((d.a)localObject10).a();
    paramca.c();
    i1 = localh.d(localMessage);
    boolean bool21 = f(paramMessage);
    if (bool21)
    {
      localObject2 = b;
      i24 = e(paramMessage);
      int i35 = ((bf)localObject2).c(i24);
      ((ca)localObject1).c(i35);
    }
    ((ca)localObject1).d(i1);
    localObject10 = "baseParams";
    c.g.b.k.a(locald, (String)localObject10);
    ((ca)localObject1).a(i33, locald);
    ((ca)localObject1).e(false);
    paramca.e();
    ((ca)localObject1).i(false);
    i1 = ((CharSequence)localObject3).length();
    float f1;
    if (i1 == 0)
    {
      i1 = 1;
      f1 = Float.MIN_VALUE;
    }
    else
    {
      i1 = 0;
      f1 = 0.0F;
      localObject10 = null;
    }
    int i37;
    if ((i1 == 0) && (i25 == 0))
    {
      localObject10 = ah.a(str);
      c.g.b.k.a(localObject10, "RtlUtils.stringWithPhoneNumbersLTR(text)");
      int i36 = a.f();
      i12 = 1;
      f2 = Float.MIN_VALUE;
      i36 ^= i12;
      ((ca)localObject1).a((String)localObject10, i36);
      localObject10 = b;
      i1 = ((bf)localObject10).j();
      localObject2 = b;
      i37 = ((bf)localObject2).a(locald);
      ((ca)localObject1).a(i1, i37);
      ((ca)localObject1).c(i12);
      boolean bool1 = localh.c(localMessage);
      if ((bool1) && (i20 != 0))
      {
        bool1 = true;
        f1 = Float.MIN_VALUE;
      }
      else
      {
        bool1 = false;
        f1 = 0.0F;
        localObject10 = null;
      }
      ((ca)localObject1).f(bool1);
      if (bool1)
      {
        localObject10 = c;
        c.g.b.k.a(localObject10, "item.participant");
        localObject10 = ((Participant)localObject10).a();
        localObject2 = "item.participant.displayName";
        c.g.b.k.a(localObject10, (String)localObject2);
        ((ca)localObject1).b((String)localObject10);
      }
      bool1 = D;
      if (bool1)
      {
        bool1 = C;
        if (bool1)
        {
          int i2 = E;
          ((ca)localObject1).b(i2);
        }
        else
        {
          localObject10 = A;
          ((ca)localObject1).a(localMessage, (Reaction[])localObject10);
        }
      }
    }
    else
    {
      ((ca)localObject1).c(false);
    }
    if (i20 != 0)
    {
      localh.a(localMessage, (ca)localObject1);
      boolean bool2 = paramMessage.g();
      if ((bool2) && (i20 != 0))
      {
        localObject10 = b;
        int i3 = ((bf)localObject10).j();
        ((ca)localObject1).e(i3);
      }
    }
    boolean bool3 = localh.c(localMessage);
    localObject9 = n;
    int i27 = localObject9.length;
    int i24 = 0;
    localObject5 = null;
    float f3;
    label2740:
    label2762:
    int i4;
    int i43;
    while (i24 < i27)
    {
      localObject2 = localObject9[i24];
      localObject3 = "entity";
      c.g.b.k.a(localObject2, (String)localObject3);
      boolean bool10 = ((Entity)localObject2).a();
      int i45;
      Object localObject11;
      int i47;
      if (!bool10)
      {
        bool10 = ((Entity)localObject2).b();
        if (bool10)
        {
          localObject3 = AttachmentType.IMAGE;
        }
        else
        {
          bool10 = ((Entity)localObject2).e();
          if (bool10)
          {
            localObject3 = AttachmentType.AUDIO;
          }
          else
          {
            bool10 = ((Entity)localObject2).c();
            if (bool10)
            {
              localObject3 = AttachmentType.VIDEO;
            }
            else
            {
              bool10 = ((Entity)localObject2).d();
              if (bool10) {
                localObject3 = AttachmentType.VCARD;
              } else {
                localObject3 = AttachmentType.UNKNOWN;
              }
            }
          }
        }
        localObject4 = locald.a().a((AttachmentType)localObject3).a(localMessage);
        if (bool3)
        {
          localObject7 = c;
          paramInt = i24;
          c.g.b.k.a(localObject7, "item.participant");
          localObject5 = ((Participant)localObject7).a();
        }
        else
        {
          paramInt = i24;
          i24 = 0;
          localObject5 = null;
        }
        localObject4 = ((d.a)localObject4).a((String)localObject5);
        localObject5 = b.a((Entity)localObject2);
        localObject4 = ((d.a)localObject4).e((String)localObject5);
        localObject5 = b;
        localObject7 = localObject2;
        localObject7 = (BinaryEntity)localObject2;
        i45 = i27;
        localObject11 = localObject9;
        long l2 = d;
        localObject5 = ((bf)localObject5).a(l2);
        localObject4 = ((d.a)localObject4).f((String)localObject5);
        localObject10 = ((d.a)localObject4).a(bool3);
        boolean bool12 = paramMessage.g();
        int i21;
        if (bool12)
        {
          i21 = localh.d(localMessage);
        }
        else
        {
          i21 = 0;
          localObject4 = null;
        }
        bool10 = supported;
        if (bool10)
        {
          bool10 = ((BinaryEntity)localObject7).b();
          i27 = 3;
          long l3;
          int i38;
          if (bool10)
          {
            localObject10 = ((d.a)localObject10).a((Entity)localObject2).c(i33);
            int i13 = b.f();
            localObject10 = ((d.a)localObject10).g(i13);
            localObject3 = b;
            i24 = i;
            l3 = d;
            localObject3 = ((bf)localObject3).a(i24, l3);
            localObject10 = ((d.a)localObject10).c((String)localObject3);
            i13 = i;
            if (i13 != 0)
            {
              i13 = 1;
              f2 = Float.MIN_VALUE;
            }
            else
            {
              i13 = 0;
              f2 = 0.0F;
              localObject3 = null;
            }
            localObject10 = ((d.a)localObject10).e(i13);
            i37 = i;
            if (i37 == i27)
            {
              i37 = 1;
              f3 = Float.MIN_VALUE;
            }
            else
            {
              i37 = 0;
              f3 = 0.0F;
              localObject2 = null;
            }
            localObject9 = ((d.a)localObject10).d(i37).f(i21).a();
            c.g.b.k.a(localObject9, "params\n                 …                 .build()");
            localObject10 = d;
            if (localObject10 != null)
            {
              localObject10 = (ImageEntity)localObject10;
              i38 = a;
              int i14 = k;
              localObject4 = h;
              i21 = ((com.truecaller.util.g)localObject4).a();
              i24 = -1;
              int i16;
              if ((i38 != i24) && (i14 != i24))
              {
                f3 = i38;
                f2 = i14;
                f3 /= f2;
                f2 = 1.0F;
                int i15 = f3 < f2;
                if (i15 > 0)
                {
                  i24 = (int)(i21 / f3);
                  localObject2 = b;
                  c.g.b.k.a(localObject2, "entity.content");
                  i27 = b.c();
                  int i46 = b.e();
                  localObject10 = paramca;
                  i15 = i21;
                  i21 = i24;
                  i47 = paramInt;
                  i24 = i27;
                  i27 = i46;
                  localObject1 = localObject11;
                  i22 = 2;
                  paramca.a((Uri)localObject2, i15, i21, i24, i46, (com.truecaller.messaging.conversation.d)localObject9);
                  i16 = 2;
                  f2 = 2.8E-45F;
                  i21 = 0;
                  localObject4 = null;
                  localObject5 = paramca;
                }
                else
                {
                  i47 = paramInt;
                  localObject1 = localObject11;
                  i22 = 2;
                  i16 = (int)(i21 * f3);
                  localObject2 = b;
                  c.g.b.k.a(localObject2, "entity.content");
                  i24 = b.c();
                  i27 = b.e();
                  localObject10 = paramca;
                  paramca.a((Uri)localObject2, i16, i21, i24, i27, (com.truecaller.messaging.conversation.d)localObject9);
                  i16 = 2;
                  f2 = 2.8E-45F;
                  i21 = 0;
                  localObject4 = null;
                  localObject5 = paramca;
                }
              }
              else
              {
                i47 = paramInt;
                localObject1 = localObject11;
                i22 = 2;
                localObject2 = b;
                c.g.b.k.a(localObject2, "entity.content");
                i24 = b.c();
                i27 = b.e();
                localObject10 = paramca;
                i16 = i21;
                i21 = i24;
                i24 = i27;
                localObject8 = localObject9;
                paramca.a((Uri)localObject2, i16, i21, i27, (com.truecaller.messaging.conversation.d)localObject9);
                i16 = 2;
                f2 = 2.8E-45F;
                i21 = 0;
                localObject4 = null;
                localObject5 = paramca;
              }
            }
            else
            {
              localObject10 = new c/u;
              ((u)localObject10).<init>("null cannot be cast to non-null type com.truecaller.messaging.data.types.ImageEntity");
              throw ((Throwable)localObject10);
            }
          }
          else
          {
            i47 = paramInt;
            localObject1 = localObject11;
            i32 = 2;
            boolean bool11 = ((BinaryEntity)localObject7).c();
            if (bool11)
            {
              localObject10 = ((d.a)localObject10).a((Entity)localObject2).c(i33);
              int i17 = b.f();
              localObject10 = ((d.a)localObject10).g(i17);
              localObject3 = b;
              i24 = i;
              l3 = d;
              localObject3 = ((bf)localObject3).a(i24, l3);
              localObject10 = ((d.a)localObject10).c((String)localObject3);
              i17 = b.h();
              localObject10 = ((d.a)localObject10).d(i17);
              localObject3 = i;
              localObject5 = localObject2;
              localObject5 = (VideoEntity)localObject2;
              i24 = l;
              localObject3 = ((af)localObject3).b(i24);
              localObject10 = ((d.a)localObject10).d((String)localObject3);
              localObject3 = b;
              i17 = ((bf)localObject3).i();
              localObject10 = ((d.a)localObject10).e(i17);
              i17 = i;
              if (i17 != 0)
              {
                i17 = 1;
                f2 = Float.MIN_VALUE;
              }
              else
              {
                i17 = 0;
                f2 = 0.0F;
                localObject3 = null;
              }
              localObject10 = ((d.a)localObject10).e(i17);
              i38 = i;
              if (i38 == i27)
              {
                i38 = 1;
                f3 = Float.MIN_VALUE;
              }
              else
              {
                i38 = 0;
                f3 = 0.0F;
                localObject2 = null;
              }
              localObject10 = ((d.a)localObject10).d(i38);
              int i39 = f & 0x34;
              if (i39 != 0)
              {
                i39 = 1;
                f3 = Float.MIN_VALUE;
              }
              else
              {
                i39 = 0;
                f3 = 0.0F;
                localObject2 = null;
              }
              localObject10 = ((d.a)localObject10).f(i39);
              boolean bool22 = f(paramMessage);
              if (bool22)
              {
                bool22 = paramMessage.e();
                if (bool22)
                {
                  i40 = k;
                  i32 = 2;
                  if (i40 != i32) {
                    break label2740;
                  }
                }
                else
                {
                  i32 = 2;
                }
                i40 = k;
                if (i40 == i32)
                {
                  i40 = 1;
                  f3 = Float.MIN_VALUE;
                  break label2762;
                }
              }
              else
              {
                i32 = 2;
              }
              int i40 = 0;
              f3 = 0.0F;
              localObject2 = null;
              localObject1 = ((d.a)localObject10).g(i40).f(i21).a();
              c.g.b.k.a(localObject1, "params\n                 …                 .build()");
              localObject10 = d;
              if (localObject10 != null)
              {
                localObject10 = (VideoEntity)localObject10;
                int i41 = a;
                i18 = k;
                i21 = i;
                if (i21 == 0)
                {
                  localObject10 = b.buildUpon();
                  localObject5 = "true";
                  localObject10 = ((Uri.Builder)localObject10).appendQueryParameter("com.truecaller.util.VideoRequestHandler.IS_VIDEO", (String)localObject5).build();
                  localObject4 = localObject10;
                }
                else
                {
                  localObject10 = m;
                  localObject4 = localObject10;
                }
                localObject10 = h;
                i24 = ((com.truecaller.util.g)localObject10).a();
                i4 = -1;
                f1 = 0.0F / 0.0F;
                if ((i41 != i4) && (i18 != i4))
                {
                  f1 = i41;
                  f3 = i18;
                  f1 /= f3;
                  f3 = 1.0F;
                  boolean bool23 = f1 < f3;
                  if (bool23)
                  {
                    f3 = i24 / f1;
                    i27 = (int)f3;
                    c.g.b.k.a(localObject4, "previewUri");
                    i22 = b.c();
                    int i48 = b.e();
                    localObject10 = paramca;
                    localObject2 = localObject4;
                    i18 = i24;
                    i21 = i27;
                    i24 = i22;
                    i27 = i48;
                    i22 = 2;
                    localObject9 = localObject1;
                    paramca.b((Uri)localObject4, i18, i21, i24, i48, (com.truecaller.messaging.conversation.d)localObject1);
                    i18 = 2;
                    f2 = 2.8E-45F;
                    i21 = 0;
                    localObject4 = null;
                    localObject5 = paramca;
                  }
                  else
                  {
                    i22 = 2;
                    f3 = i24 * f1;
                    i18 = (int)f3;
                    c.g.b.k.a(localObject4, "previewUri");
                    i27 = b.c();
                    i32 = b.e();
                    localObject10 = paramca;
                    localObject2 = localObject4;
                    i21 = i24;
                    i24 = i27;
                    i27 = i32;
                    localObject9 = localObject1;
                    paramca.b((Uri)localObject4, i18, i21, i24, i32, (com.truecaller.messaging.conversation.d)localObject1);
                    i18 = 2;
                    f2 = 2.8E-45F;
                    i21 = 0;
                    localObject4 = null;
                    localObject5 = paramca;
                  }
                }
                else
                {
                  i22 = 2;
                  c.g.b.k.a(localObject4, "previewUri");
                  i27 = b.c();
                  i32 = b.e();
                  localObject10 = paramca;
                  localObject2 = localObject4;
                  i18 = i24;
                  i21 = i27;
                  i24 = i32;
                  localObject8 = localObject1;
                  paramca.b((Uri)localObject4, i18, i27, i32, (com.truecaller.messaging.conversation.d)localObject1);
                  i18 = 2;
                  f2 = 2.8E-45F;
                  i21 = 0;
                  localObject4 = null;
                  localObject5 = paramca;
                }
              }
              else
              {
                localObject10 = new c/u;
                ((u)localObject10).<init>("null cannot be cast to non-null type com.truecaller.messaging.data.types.VideoEntity");
                throw ((Throwable)localObject10);
              }
            }
            else
            {
              i18 = 2;
              f2 = 2.8E-45F;
              bool13 = ((BinaryEntity)localObject7).e();
              if (bool13)
              {
                localObject10 = ((d.a)localObject10).a((Entity)localObject2).c(i33);
                localObject4 = b;
                i24 = i;
                l3 = d;
                localObject4 = ((bf)localObject4).a(i24, l3);
                localObject10 = ((d.a)localObject10).c((String)localObject4);
                bool13 = false;
                localObject4 = null;
                localObject10 = ((d.a)localObject10).d(0);
                localObject5 = i;
                localObject9 = localObject2;
                localObject9 = (AudioEntity)localObject2;
                i32 = a;
                localObject5 = ((af)localObject5).a(i32);
                localObject10 = ((d.a)localObject10).d((String)localObject5).e(i33);
                int i42 = i;
                if (i42 == i27)
                {
                  i42 = 1;
                  f3 = Float.MIN_VALUE;
                }
                else
                {
                  i42 = 0;
                  f3 = 0.0F;
                  localObject2 = null;
                }
                localObject10 = ((d.a)localObject10).d(i42).a();
                localObject2 = "params.entity(entity)\n  …                 .build()";
                c.g.b.k.a(localObject10, (String)localObject2);
                localObject5 = paramca;
                paramca.a((com.truecaller.messaging.conversation.d)localObject10);
              }
              else
              {
                bool13 = false;
                localObject4 = null;
                localObject5 = paramca;
                localObject10 = ((d.a)localObject10).a((Entity)localObject2).a();
                c.g.b.k.a(localObject10, "params.entity(entity).build()");
                localObject2 = b;
                i27 = e(paramMessage);
                i43 = ((bf)localObject2).c(i27);
                paramca.a((com.truecaller.messaging.conversation.d)localObject10, i33, i43);
              }
            }
          }
        }
        else
        {
          i47 = paramInt;
          localObject5 = localObject1;
          i18 = 2;
          f2 = 2.8E-45F;
          bool13 = false;
          localObject4 = null;
          localObject10 = ((d.a)localObject10).a();
          c.g.b.k.a(localObject10, "params.build()");
          localObject2 = b;
          i27 = e(paramMessage);
          i43 = ((bf)localObject2).c(i27);
          ((ca)localObject1).a((com.truecaller.messaging.conversation.d)localObject10, i33, i43);
        }
        localh.a(localMessage, (ca)localObject5);
        i4 = 0;
        f1 = 0.0F;
        localObject10 = null;
      }
      else
      {
        i47 = i24;
        i45 = i27;
        localObject11 = localObject9;
        localObject5 = localObject1;
        i18 = 2;
        f2 = 2.8E-45F;
        bool13 = false;
        localObject4 = null;
      }
      i43 = i47 + 1;
      localObject1 = localObject5;
      localObject9 = localObject11;
      i27 = i45;
      i22 = 0;
      localObject7 = null;
      i24 = i43;
    }
    localObject5 = localObject1;
    int i18 = 2;
    float f2 = 2.8E-45F;
    boolean bool13 = false;
    localObject4 = null;
    Object localObject12;
    if (i25 != 0)
    {
      localObject6 = (CharSequence)localObject6;
      i4 = com.truecaller.android.truemoji.c.a.b((CharSequence)localObject6);
      i43 = 4;
      f3 = 5.6E-45F;
      if (i4 <= i43)
      {
        i4 = 1076677837;
        f1 = 2.7F;
      }
      else
      {
        i4 = 1073741824;
        f1 = 2.0F;
      }
      boolean bool24 = localh.c(localMessage);
      localObject8 = locald.a().a(localMessage);
      if (bool24)
      {
        localObject9 = c;
        localObject1 = "item.participant";
        c.g.b.k.a(localObject9, (String)localObject1);
        localObject12 = ((Participant)localObject9).a();
      }
      else
      {
        i33 = 0;
        localObject12 = null;
      }
      localObject2 = ((d.a)localObject8).a((String)localObject12).a(bool24);
      i27 = b.g();
      localObject2 = ((d.a)localObject2).c(i27).a();
      localObject8 = "params.build()";
      c.g.b.k.a(localObject2, (String)localObject8);
      ((ca)localObject5).a((com.truecaller.messaging.conversation.d)localObject2, str, f1);
    }
    boolean bool4 = f(paramMessage);
    label3969:
    int i44;
    if (bool4)
    {
      bool4 = paramMessage.e();
      if (bool4)
      {
        i5 = k;
        if (i5 != i18)
        {
          i5 = 1;
          f1 = Float.MIN_VALUE;
          break label3969;
        }
      }
      int i5 = 0;
      f1 = 0.0F;
      localObject10 = null;
      ((ca)localObject5).d(i5);
      int i6 = f & 0x8;
      if (i6 != 0)
      {
        i6 = 1;
        f1 = Float.MIN_VALUE;
      }
      else
      {
        i6 = 0;
        f1 = 0.0F;
        localObject10 = null;
      }
      ((ca)localObject5).e(i6);
      if (i6 != 0)
      {
        int i7 = e(paramMessage);
        i44 = 2131886686;
        f3 = 1.9407958E38F;
        if (i7 == i18)
        {
          localObject12 = m;
          boolean bool5 = localObject12 instanceof ImTransportInfo;
          if (!bool5)
          {
            i33 = 0;
            localObject12 = null;
          }
          localObject12 = (ImTransportInfo)localObject12;
          if (localObject12 != null)
          {
            i22 = h;
          }
          else
          {
            i22 = 0;
            localObject7 = null;
          }
          switch (i22)
          {
          default: 
            break;
          case 7: 
            i44 = 2131886698;
            f3 = 1.9407982E38F;
            break;
          case 6: 
            i44 = 2131886695;
            f3 = 1.9407976E38F;
            break;
          case 5: 
            i44 = 2131886656;
            f3 = 1.9407897E38F;
            break;
          case 4: 
            i44 = 2131886696;
            f3 = 1.9407978E38F;
            break;
          case 3: 
            i44 = 2131886697;
            f3 = 1.940798E38F;
          }
        }
        ((ca)localObject5).a(i44);
        localObject10 = b;
        i44 = e(paramMessage);
        int i8 = ((bf)localObject10).e(i44);
        ((ca)localObject5).c(i8);
      }
    }
    else
    {
      boolean bool6 = paramMessage.f();
      if (bool6)
      {
        f1 = Float.MIN_VALUE;
        ((ca)localObject5).e(true);
        localObject12 = m;
        bool6 = localObject12 instanceof ImTransportInfo;
        if (!bool6)
        {
          i33 = 0;
          localObject12 = null;
        }
        localObject12 = (ImTransportInfo)localObject12;
        if (localObject12 != null)
        {
          i22 = i;
        }
        else
        {
          i22 = 0;
          localObject7 = null;
        }
        localObject10 = k;
        int i9 = ((bu)localObject10).a();
        if (i22 > i9)
        {
          i9 = 1;
          f1 = Float.MIN_VALUE;
        }
        else
        {
          i9 = 0;
          f1 = 0.0F;
          localObject10 = null;
        }
        if (i9 != 0)
        {
          i44 = 2131886481;
          f3 = 1.9407542E38F;
          ((ca)localObject5).a(i44);
        }
        else
        {
          i44 = 2131886479;
          f3 = 1.9407538E38F;
          ((ca)localObject5).a(i44);
        }
        localObject2 = new com/truecaller/messaging/conversation/a/b/h$b;
        ((h.b)localObject2).<init>(localh, i9);
        localObject2 = (c.g.a.b)localObject2;
        ((ca)localObject5).c((c.g.a.b)localObject2);
      }
    }
    boolean bool7 = f(paramMessage);
    if (!bool7)
    {
      localObject10 = m.c();
      bool7 = ((com.truecaller.featuretoggles.b)localObject10).a();
      if (bool7)
      {
        localObject10 = m.b();
        bool7 = ((com.truecaller.featuretoggles.b)localObject10).a();
        if (bool7)
        {
          localObject10 = c;
          int i10 = c;
          i19 = 1;
          f2 = Float.MIN_VALUE;
          if (i10 == i19) {
            break label4568;
          }
          localObject10 = a;
          bool8 = ((bh)localObject10).f();
          ((ca)localObject5).a(bool8, locald);
          break label4568;
        }
      }
    }
    int i19 = 1;
    f2 = Float.MIN_VALUE;
    label4568:
    boolean bool8 = paramMessage.e();
    if (!bool8)
    {
      int i11 = f & 0x7E;
      if (i11 <= 0)
      {
        localObject10 = n;
        localObject2 = "item.entities";
        c.g.b.k.a(localObject10, (String)localObject2);
        i44 = localObject10.length;
        i27 = 0;
        localObject8 = null;
        while (i27 < i44)
        {
          localObject9 = localObject10[i27];
          i32 = i;
          if (i32 != 0)
          {
            i32 = 1;
          }
          else
          {
            i32 = 0;
            localObject9 = null;
          }
          if (i32 != 0)
          {
            i11 = 1;
            f1 = Float.MIN_VALUE;
            break label4695;
          }
          i27 += 1;
        }
        i11 = 0;
        f1 = 0.0F;
        localObject10 = null;
        label4695:
        if (i11 == 0)
        {
          localObject10 = n;
          localObject2 = "item.entities";
          c.g.b.k.a(localObject10, (String)localObject2);
          i44 = localObject10.length;
          i27 = 0;
          localObject8 = null;
          while (i27 < i44)
          {
            localObject9 = localObject10[i27];
            localObject1 = "it";
            c.g.b.k.a(localObject9, (String)localObject1);
            boolean bool20 = ((Entity)localObject9).e();
            if (bool20)
            {
              i11 = 1;
              f1 = Float.MIN_VALUE;
              break label4796;
            }
            i27 += 1;
          }
          i11 = 0;
          f1 = 0.0F;
          localObject10 = null;
          label4796:
          if (i11 == 0)
          {
            boolean bool9 = paramMessage.d();
            if (bool9) {
              break label4824;
            }
          }
        }
      }
    }
    i19 = 0;
    f2 = 0.0F;
    localObject3 = null;
    label4824:
    ((ca)localObject5).h(i19);
  }
  
  public final void a(CharSequence paramCharSequence, ca paramca, com.truecaller.messaging.conversation.d paramd)
  {
    h localh = this;
    Object localObject1 = paramCharSequence;
    c.g.b.k.b(paramCharSequence, "text");
    c.g.b.k.b(paramca, "view");
    c.g.b.k.b(paramd, "params");
    List localList = l.a(paramCharSequence);
    Object localObject2 = localList;
    localObject2 = (Collection)localList;
    boolean bool1 = ((Collection)localObject2).isEmpty();
    int i2 = 1;
    bool1 ^= i2;
    if (bool1)
    {
      bool1 = false;
      localObject2 = localList.get(0);
      Object localObject3 = localObject2;
      localObject3 = (o)localObject2;
      localObject2 = a;
      Object localObject4 = l;
      n localn = b;
      int i3 = ((k)localObject4).a(localn);
      Object localObject5 = this;
      localObject5 = (a.a)this;
      boolean bool2 = true;
      localObject1 = paramCharSequence.toString();
      localObject2 = c;
      boolean bool3 = c.g.b.k.a(localObject1, localObject2) ^ true;
      localObject2 = paramca;
      localObject4 = paramd;
      paramca.a(paramd, (o)localObject3, i3, (a.a)localObject5, bool2, bool3);
      int i4 = localList.size();
      localObject1 = ((Iterable)i.b(i2, i4)).iterator();
      for (;;)
      {
        bool1 = ((Iterator)localObject1).hasNext();
        if (!bool1) {
          break;
        }
        localObject2 = localObject1;
        int i1 = ((ae)localObject1).a();
        localObject2 = localList.get(i1);
        localObject3 = localObject2;
        localObject3 = (o)localObject2;
        localObject2 = l;
        localObject4 = b;
        i3 = ((k)localObject2).a((n)localObject4);
        bool2 = false;
        bool3 = true;
        localObject2 = paramca;
        localObject4 = paramd;
        paramca.a(paramd, (o)localObject3, i3, (a.a)localObject5, false, bool3);
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.a.b.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */