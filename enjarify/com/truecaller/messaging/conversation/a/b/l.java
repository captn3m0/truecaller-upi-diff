package com.truecaller.messaging.conversation.a.b;

import android.content.ContentResolver;
import android.text.SpannableString;
import android.text.style.URLSpan;
import c.g.a.m;
import com.truecaller.analytics.ae;
import com.truecaller.analytics.e.a;
import com.truecaller.messaging.data.c;
import com.truecaller.messaging.h;
import com.truecaller.tracking.events.r.a;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;
import org.apache.a.d.d;

public final class l
  implements k
{
  k.a a;
  Map b;
  boolean c;
  final ContentResolver d;
  final c e;
  final c.d.f f;
  final com.truecaller.network.spamUrls.a g;
  private bn h;
  private final c.d.f i;
  private final h j;
  private final com.truecaller.messaging.conversation.spamLinks.a k;
  private final com.truecaller.analytics.b l;
  private final com.truecaller.androidactors.f m;
  
  public l(ContentResolver paramContentResolver, c paramc, c.d.f paramf1, c.d.f paramf2, com.truecaller.network.spamUrls.a parama, h paramh, com.truecaller.messaging.conversation.spamLinks.a parama1, com.truecaller.analytics.b paramb, com.truecaller.androidactors.f paramf)
  {
    d = paramContentResolver;
    e = paramc;
    i = paramf1;
    f = paramf2;
    g = parama;
    j = paramh;
    k = parama1;
    l = paramb;
    m = paramf;
    paramContentResolver = new java/util/LinkedHashMap;
    paramContentResolver.<init>();
    paramContentResolver = (Map)paramContentResolver;
    b = paramContentResolver;
  }
  
  static e.r a(e.b paramb)
  {
    e.r localr = null;
    try
    {
      localr = paramb.c();
    }
    catch (IOException|RuntimeException localIOException)
    {
      for (;;) {}
    }
    return localr;
  }
  
  private final String b(n paramn)
  {
    h localh = j;
    int n = localh.K();
    int i1 = c;
    if (i1 > n) {
      return "spam";
    }
    return "notSpam";
  }
  
  public final int a(n paramn)
  {
    c.g.b.k.b(paramn, "spamReport");
    h localh = j;
    int n = localh.K();
    int i1 = c;
    if (i1 > n) {
      return a;
    }
    int i2 = d;
    n = 1;
    if (i2 == n) {
      return n;
    }
    return 0;
  }
  
  public final List a(CharSequence paramCharSequence)
  {
    c.g.b.k.b(paramCharSequence, "text");
    SpannableString localSpannableString = SpannableString.valueOf(paramCharSequence);
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    int n = paramCharSequence.length();
    Class localClass = URLSpan.class;
    int i1 = 0;
    URLSpan[] arrayOfURLSpan = (URLSpan[])localSpannableString.getSpans(0, n, localClass);
    if (arrayOfURLSpan == null) {
      return (List)localArrayList;
    }
    int i2 = arrayOfURLSpan.length;
    while (i1 < i2)
    {
      Object localObject1 = arrayOfURLSpan[i1];
      int i3 = localSpannableString.getSpanStart(localObject1);
      int i4 = localSpannableString.getSpanEnd(localObject1);
      String str = paramCharSequence.subSequence(i3, i4).toString();
      f localf = new com/truecaller/messaging/conversation/a/b/f;
      c.g.b.k.a(localObject1, "urlSpan");
      localObject1 = ((URLSpan)localObject1).getURL();
      Object localObject2 = "urlSpan.url";
      c.g.b.k.a(localObject1, (String)localObject2);
      localf.<init>(i3, i4, str, (String)localObject1);
      localObject1 = k;
      boolean bool = ((com.truecaller.messaging.conversation.spamLinks.a)localObject1).a(localf);
      if (bool)
      {
        bool = c;
        Object localObject3;
        if (bool)
        {
          localObject1 = b;
          localObject3 = ((Map)localObject1).get(str);
          if (localObject3 == null)
          {
            localObject3 = new com/truecaller/messaging/conversation/a/b/n;
            ((n)localObject3).<init>();
            ((Map)localObject1).put(str, localObject3);
          }
          localObject3 = (n)localObject3;
          long l1 = f;
          long l2 = b;
          l1 += l2;
          l2 = System.currentTimeMillis();
          bool = l1 < l2;
          if (!bool)
          {
            l1 = System.currentTimeMillis();
            f = l1;
            localObject1 = (ag)bg.a;
            c.d.f localf1 = i;
            localObject2 = new com/truecaller/messaging/conversation/a/b/l$g;
            ((l.g)localObject2).<init>(this, str, (n)localObject3, null);
            localObject2 = (m)localObject2;
            int i5 = 2;
            kotlinx.coroutines.e.b((ag)localObject1, localf1, (m)localObject2, i5);
          }
        }
        else
        {
          localObject3 = new com/truecaller/messaging/conversation/a/b/n;
          ((n)localObject3).<init>();
        }
        localObject1 = new com/truecaller/messaging/conversation/a/b/o;
        ((o)localObject1).<init>(localf, (n)localObject3);
        localArrayList.add(localObject1);
      }
      i1 += 1;
    }
    return (List)localArrayList;
  }
  
  public final void a()
  {
    boolean bool = c;
    if (!bool)
    {
      ag localag = (ag)bg.a;
      c.d.f localf = i;
      Object localObject = new com/truecaller/messaging/conversation/a/b/l$c;
      ((l.c)localObject).<init>(this, null);
      localObject = (m)localObject;
      int n = 2;
      kotlinx.coroutines.e.b(localag, localf, (m)localObject, n);
    }
  }
  
  public final void a(k.a parama)
  {
    a = parama;
  }
  
  public final void a(n paramn, String paramString1, String paramString2)
  {
    c.g.b.k.b(paramn, "spamReport");
    c.g.b.k.b(paramString1, "action");
    c.g.b.k.b(paramString2, "url");
    paramn = b(paramn);
    Object localObject1 = new com/truecaller/analytics/e$a;
    ((e.a)localObject1).<init>("SpamLink");
    localObject1 = ((e.a)localObject1).a("Action", paramString1).a("Type", paramn).a();
    Object localObject2 = l;
    c.g.b.k.a(localObject1, "event");
    ((com.truecaller.analytics.b)localObject2).b((com.truecaller.analytics.e)localObject1);
    localObject1 = (ae)m.a();
    localObject2 = com.truecaller.tracking.events.r.b();
    paramString1 = (CharSequence)paramString1;
    paramString1 = ((r.a)localObject2).a(paramString1);
    paramn = (CharSequence)paramn;
    paramn = paramString1.b(paramn);
    paramString2 = (CharSequence)paramString2;
    paramn = (d)paramn.c(paramString2).a();
    ((ae)localObject1).a(paramn);
  }
  
  public final void a(o paramo)
  {
    c.g.b.k.b(paramo, "spamResult");
    String str = a.c;
    Object localObject1 = h;
    if (localObject1 != null)
    {
      boolean bool = ((bn)localObject1).c(null);
      int i1 = 1;
      if (bool == i1)
      {
        localObject1 = (n)b.get(str);
        if (localObject1 != null)
        {
          int i3 = d;
          if (i3 == i1)
          {
            i2 = a + -1;
            a = i2;
          }
          int i2 = 0;
          localag = null;
          d = 0;
        }
        localObject1 = a;
        if (localObject1 != null) {
          ((k.a)localObject1).onLinksLoaded();
        }
        localObject1 = (n)b.get(str);
        if (localObject1 == null) {
          return;
        }
        ag localag = (ag)bg.a;
        c.d.f localf = f;
        Object localObject2 = new com/truecaller/messaging/conversation/a/b/l$f;
        ((l.f)localObject2).<init>(this, str, (n)localObject1, null);
        localObject2 = (m)localObject2;
        int n = 2;
        kotlinx.coroutines.e.b(localag, localf, (m)localObject2, n);
      }
    }
    paramo = b;
    a(paramo, "undo", str);
  }
  
  public final void a(String paramString, boolean paramBoolean)
  {
    c.g.b.k.b(paramString, "url");
    n localn = (n)b.get(paramString);
    if (localn == null) {
      return;
    }
    int n = 1;
    int i1 = 2;
    int i2;
    if (paramBoolean) {
      i2 = 1;
    } else {
      i2 = 2;
    }
    d = i2;
    if (paramBoolean)
    {
      paramBoolean = a + n;
      a = paramBoolean;
    }
    ag localag = (ag)bg.a;
    c.d.f localf = f;
    Object localObject = new com/truecaller/messaging/conversation/a/b/l$e;
    ((l.e)localObject).<init>(this, paramString, localn, null);
    localObject = (m)localObject;
    paramString = kotlinx.coroutines.e.b(localag, localf, (m)localObject, i1);
    h = paramString;
  }
  
  /* Error */
  public final boolean b()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 60	com/truecaller/messaging/conversation/a/b/l:d	Landroid/content/ContentResolver;
    //   4: astore_1
    //   5: invokestatic 154	com/truecaller/content/TruecallerContract$ak:a	()Landroid/net/Uri;
    //   8: astore_2
    //   9: ldc_w 378
    //   12: astore_3
    //   13: iconst_2
    //   14: anewarray 382	java/lang/String
    //   17: dup
    //   18: iconst_0
    //   19: ldc_w 380
    //   22: aastore
    //   23: dup
    //   24: iconst_1
    //   25: ldc_w 380
    //   28: aastore
    //   29: astore 4
    //   31: iconst_0
    //   32: istore 5
    //   34: aload_1
    //   35: aload_2
    //   36: aconst_null
    //   37: aload_3
    //   38: aload 4
    //   40: aconst_null
    //   41: invokevirtual 386	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   44: astore_1
    //   45: new 182	java/util/ArrayList
    //   48: astore_2
    //   49: aload_2
    //   50: invokespecial 183	java/util/ArrayList:<init>	()V
    //   53: aload_0
    //   54: getfield 62	com/truecaller/messaging/conversation/a/b/l:e	Lcom/truecaller/messaging/data/c;
    //   57: aload_1
    //   58: invokeinterface 391 2 0
    //   63: astore_1
    //   64: aconst_null
    //   65: astore 6
    //   67: aload_1
    //   68: ifnull +81 -> 149
    //   71: aload_1
    //   72: checkcast 393	java/io/Closeable
    //   75: astore_1
    //   76: aload_1
    //   77: astore_3
    //   78: aload_1
    //   79: checkcast 395	com/truecaller/messaging/data/a/p
    //   82: astore_3
    //   83: aload_3
    //   84: invokeinterface 399 1 0
    //   89: istore 7
    //   91: iload 7
    //   93: ifeq +26 -> 119
    //   96: aload_3
    //   97: invokeinterface 402 1 0
    //   102: astore 4
    //   104: aload 4
    //   106: ifnull -23 -> 83
    //   109: aload_2
    //   110: aload 4
    //   112: invokevirtual 277	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   115: pop
    //   116: goto -33 -> 83
    //   119: getstatic 407	c/x:a	Lc/x;
    //   122: astore_3
    //   123: aload_1
    //   124: aconst_null
    //   125: invokestatic 412	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   128: goto +21 -> 149
    //   131: astore_2
    //   132: goto +9 -> 141
    //   135: astore_2
    //   136: aload_2
    //   137: astore 6
    //   139: aload_2
    //   140: athrow
    //   141: aload_1
    //   142: aload 6
    //   144: invokestatic 412	c/f/b:a	(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    //   147: aload_2
    //   148: athrow
    //   149: aload_2
    //   150: astore_1
    //   151: aload_2
    //   152: checkcast 414	java/util/Collection
    //   155: astore_1
    //   156: aload_1
    //   157: invokeinterface 417 1 0
    //   162: istore 8
    //   164: iconst_1
    //   165: istore 9
    //   167: iload 8
    //   169: iload 9
    //   171: ixor
    //   172: istore 8
    //   174: iload 8
    //   176: ifeq +187 -> 363
    //   179: aload_2
    //   180: invokevirtual 420	java/util/ArrayList:size	()I
    //   183: istore 8
    //   185: bipush 10
    //   187: iload 8
    //   189: invokestatic 427	java/lang/Math:min	(II)I
    //   192: istore 8
    //   194: iconst_0
    //   195: istore 7
    //   197: aconst_null
    //   198: astore 4
    //   200: iload 8
    //   202: istore 5
    //   204: iconst_0
    //   205: istore 8
    //   207: aconst_null
    //   208: astore_1
    //   209: iload 8
    //   211: iload 5
    //   213: if_icmpge +150 -> 363
    //   216: aload_2
    //   217: invokevirtual 420	java/util/ArrayList:size	()I
    //   220: istore 10
    //   222: iload 5
    //   224: iload 10
    //   226: if_icmpgt +137 -> 363
    //   229: aload_2
    //   230: iload 8
    //   232: iload 5
    //   234: invokevirtual 431	java/util/ArrayList:subList	(II)Ljava/util/List;
    //   237: astore_1
    //   238: aload_1
    //   239: ldc_w 433
    //   242: invokestatic 219	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   245: aload_0
    //   246: getfield 68	com/truecaller/messaging/conversation/a/b/l:g	Lcom/truecaller/network/spamUrls/a;
    //   249: aload_1
    //   250: invokeinterface 438 2 0
    //   255: invokestatic 441	com/truecaller/messaging/conversation/a/b/l:a	(Le/b;)Le/r;
    //   258: astore 11
    //   260: aload 11
    //   262: ifnonnull +5 -> 267
    //   265: iconst_0
    //   266: ireturn
    //   267: aload 11
    //   269: invokevirtual 445	e/r:d	()Z
    //   272: istore 10
    //   274: iload 10
    //   276: ifeq +85 -> 361
    //   279: getstatic 253	kotlinx/coroutines/bg:a	Lkotlinx/coroutines/bg;
    //   282: checkcast 255	kotlinx/coroutines/ag
    //   285: astore 11
    //   287: aload_0
    //   288: getfield 66	com/truecaller/messaging/conversation/a/b/l:f	Lc/d/f;
    //   291: astore 12
    //   293: new 447	com/truecaller/messaging/conversation/a/b/l$h
    //   296: astore 13
    //   298: aload 13
    //   300: aload_0
    //   301: aload_1
    //   302: aconst_null
    //   303: invokespecial 450	com/truecaller/messaging/conversation/a/b/l$h:<init>	(Lcom/truecaller/messaging/conversation/a/b/l;Ljava/util/List;Lc/d/c;)V
    //   306: aload 13
    //   308: checkcast 262	c/g/a/m
    //   311: astore 13
    //   313: aload 11
    //   315: aload 12
    //   317: aload 13
    //   319: iconst_2
    //   320: invokestatic 268	kotlinx/coroutines/e:b	(Lkotlinx/coroutines/ag;Lc/d/f;Lc/g/a/m;I)Lkotlinx/coroutines/bn;
    //   323: pop
    //   324: iload 5
    //   326: bipush 10
    //   328: iadd
    //   329: istore 8
    //   331: aload_2
    //   332: invokevirtual 420	java/util/ArrayList:size	()I
    //   335: istore 10
    //   337: iload 8
    //   339: iload 10
    //   341: invokestatic 427	java/lang/Math:min	(II)I
    //   344: istore 8
    //   346: iload 5
    //   348: istore 14
    //   350: iload 8
    //   352: istore 5
    //   354: iload 14
    //   356: istore 8
    //   358: goto -149 -> 209
    //   361: iconst_0
    //   362: ireturn
    //   363: iload 9
    //   365: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	366	0	this	l
    //   4	298	1	localObject1	Object
    //   8	102	2	localObject2	Object
    //   131	1	2	localObject3	Object
    //   135	197	2	localObject4	Object
    //   12	111	3	localObject5	Object
    //   29	170	4	localObject6	Object
    //   32	321	5	n	int
    //   65	78	6	localObject7	Object
    //   89	107	7	bool1	boolean
    //   162	13	8	bool2	boolean
    //   183	174	8	i1	int
    //   165	199	9	bool3	boolean
    //   220	7	10	i2	int
    //   272	3	10	bool4	boolean
    //   335	5	10	i3	int
    //   258	56	11	localObject8	Object
    //   291	25	12	localf	c.d.f
    //   296	22	13	localObject9	Object
    //   348	7	14	i4	int
    // Exception table:
    //   from	to	target	type
    //   139	141	131	finally
    //   78	82	135	finally
    //   83	89	135	finally
    //   96	102	135	finally
    //   110	116	135	finally
    //   119	122	135	finally
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.a.b.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */