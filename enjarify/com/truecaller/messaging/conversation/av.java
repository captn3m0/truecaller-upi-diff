package com.truecaller.messaging.conversation;

import dagger.a.d;
import javax.inject.Provider;

public final class av
  implements d
{
  private final s a;
  private final Provider b;
  
  private av(s params, Provider paramProvider)
  {
    a = params;
    b = paramProvider;
  }
  
  public static av a(s params, Provider paramProvider)
  {
    av localav = new com/truecaller/messaging/conversation/av;
    localav.<init>(params, paramProvider);
    return localav;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.av
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */