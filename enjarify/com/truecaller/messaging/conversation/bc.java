package com.truecaller.messaging.conversation;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.SystemClock;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.Pair;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.URLUtil;
import com.truecaller.analytics.ap;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.i;
import com.truecaller.common.h.ab;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.data.entity.Contact;
import com.truecaller.filters.p;
import com.truecaller.filters.s;
import com.truecaller.log.AssertionUtil;
import com.truecaller.log.AssertionUtil.AlwaysFatal;
import com.truecaller.messaging.conversation.emoji.PokeableEmoji;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.Draft;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.ReplySnippet;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.i.g;
import com.truecaller.messaging.transport.im.as;
import com.truecaller.messaging.transport.im.bp;
import com.truecaller.messaging.transport.im.bu;
import com.truecaller.messaging.transport.mms.MmsTransportInfo;
import com.truecaller.network.search.j.b;
import com.truecaller.search.local.model.c.a;
import com.truecaller.truepay.SenderInfo;
import com.truecaller.util.aa;
import com.truecaller.util.al;
import com.truecaller.util.ba;
import com.truecaller.util.bg;
import com.truecaller.utils.n;
import com.truecaller.voip.ai;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;
import org.a.a.x;

public final class bc
  extends bb
  implements h.a
{
  private static final ConversationAction[] a;
  private com.truecaller.androidactors.a A;
  private com.truecaller.androidactors.a B;
  private final com.truecaller.androidactors.f C;
  private com.truecaller.androidactors.f D;
  private final com.truecaller.messaging.data.providers.c E;
  private final com.truecaller.util.d.a F;
  private final com.truecaller.messaging.c.a G;
  private final com.truecaller.featuretoggles.e H;
  private com.truecaller.androidactors.a I;
  private SenderInfo J;
  private final m K;
  private final com.truecaller.androidactors.f L;
  private final com.truecaller.androidactors.f M;
  private final com.truecaller.tcpermissions.l N;
  private final com.truecaller.utils.l O;
  private final bu P;
  private final bw Q;
  private final com.truecaller.messaging.d.a R;
  private final com.truecaller.messaging.conversation.a.b.k S;
  private final com.truecaller.androidactors.f T;
  private final com.truecaller.androidactors.f U;
  private final bg V;
  private boolean W = false;
  private boolean X;
  private boolean Y;
  private long[] Z = null;
  private final ai aA;
  private final com.truecaller.search.b aB;
  private Integer aC;
  private com.truecaller.messaging.conversation.voice_notes.f aD;
  private com.truecaller.androidactors.f aE;
  private long aF;
  private Message[] aa;
  private final ap ab;
  private Parcelable ac;
  private String ad;
  private String ae = null;
  private Uri af = null;
  private Uri ag = null;
  private Participant ah = null;
  private Integer ai = null;
  private boolean aj = false;
  private int ak = 0;
  private ArrayList al;
  private final h am;
  private final bi an;
  private int ao;
  private final j.b ap;
  private com.truecaller.androidactors.a aq;
  private Entity[] ar;
  private Pair as;
  private final BroadcastReceiver at;
  private final Long au;
  private boolean av;
  private Message aw;
  private int ax;
  private com.truecaller.messaging.j ay;
  private com.truecaller.messaging.conversation.emoji.a az;
  private Conversation c;
  private Participant[] d;
  private Long e;
  private Long f;
  private int g;
  private final i h;
  private final com.truecaller.androidactors.f i;
  private final com.truecaller.androidactors.f j;
  private final com.truecaller.messaging.transport.m k;
  private final com.truecaller.search.local.model.c l;
  private final com.truecaller.messaging.h m;
  private final com.truecaller.utils.o n;
  private final al o;
  private final com.truecaller.utils.d p;
  private final com.truecaller.network.search.l q;
  private final p r;
  private final com.truecaller.androidactors.f s;
  private com.truecaller.androidactors.a t;
  private com.truecaller.androidactors.a u;
  private final com.truecaller.androidactors.f v;
  private final com.truecaller.android.truemoji.j w;
  private Draft x = null;
  private com.truecaller.androidactors.a y;
  private com.truecaller.androidactors.a z;
  
  static
  {
    ConversationAction[] arrayOfConversationAction = new ConversationAction[1];
    ConversationAction localConversationAction = ConversationAction.TOP_SAVE;
    arrayOfConversationAction[0] = localConversationAction;
    a = arrayOfConversationAction;
  }
  
  bc(i parami, Conversation paramConversation, Participant[] paramArrayOfParticipant, Long paramLong1, Long paramLong2, int paramInt1, int paramInt2, com.truecaller.androidactors.f paramf1, com.truecaller.androidactors.f paramf2, com.truecaller.utils.o paramo, al paramal, com.truecaller.utils.d paramd, com.truecaller.network.search.l paraml, com.truecaller.androidactors.f paramf3, com.truecaller.messaging.transport.m paramm, com.truecaller.search.local.model.c paramc, com.truecaller.messaging.h paramh, com.truecaller.androidactors.f paramf4, p paramp, com.truecaller.androidactors.f paramf5, com.truecaller.android.truemoji.j paramj, com.truecaller.androidactors.f paramf6, com.truecaller.messaging.data.providers.c paramc1, com.truecaller.util.d.a parama, h paramh1, bi parambi, ap paramap, com.truecaller.messaging.c.a parama1, com.truecaller.featuretoggles.e parame, m paramm1, com.truecaller.androidactors.f paramf7, com.truecaller.androidactors.f paramf8, com.truecaller.tcpermissions.l paraml1, com.truecaller.utils.l paraml2, bu parambu, bw parambw, com.truecaller.messaging.d.a parama2, com.truecaller.messaging.conversation.a.b.k paramk, com.truecaller.androidactors.f paramf9, com.truecaller.androidactors.f paramf10, bg parambg, com.truecaller.messaging.j paramj1, com.truecaller.messaging.conversation.emoji.a parama3, ai paramai, com.truecaller.search.b paramb, com.truecaller.messaging.conversation.voice_notes.f paramf, com.truecaller.androidactors.f paramf11)
  {
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    al = ((ArrayList)localObject2);
    int i1 = -1;
    ao = i1;
    localObject2 = new com/truecaller/messaging/conversation/bc$1;
    ((bc.1)localObject2).<init>(this);
    ap = ((j.b)localObject2);
    aq = null;
    localObject2 = new com/truecaller/messaging/conversation/bc$2;
    ((bc.2)localObject2).<init>(this);
    at = ((BroadcastReceiver)localObject2);
    long l1 = SystemClock.elapsedRealtime();
    localObject2 = Long.valueOf(l1);
    au = ((Long)localObject2);
    av = false;
    aC = null;
    long l2 = -1;
    aF = l2;
    F = parama;
    c = null;
    if (paramArrayOfParticipant == null)
    {
      if (paramConversation != null)
      {
        arrayOfParticipant = l;
        d = arrayOfParticipant;
      }
    }
    else {
      d = paramArrayOfParticipant;
    }
    arrayOfParticipant = d;
    localbi.a(arrayOfParticipant);
    if (localObject1 != null)
    {
      long l3 = a;
      localObject1 = Long.valueOf(l3);
      e = ((Long)localObject1);
      localObject1 = paramLong2;
    }
    else
    {
      localObject1 = paramLong1;
      e = paramLong1;
      localObject1 = paramLong2;
    }
    f = ((Long)localObject1);
    g = paramInt1;
    localObject1 = parami;
    h = parami;
    localObject1 = paramf1;
    i = paramf1;
    localObject1 = paramf2;
    j = paramf2;
    localObject1 = paramo;
    n = paramo;
    localObject1 = paramal;
    o = paramal;
    localObject1 = paramd;
    p = paramd;
    localObject1 = paraml;
    q = paraml;
    localObject1 = paramf3;
    v = paramf3;
    localObject1 = paramm;
    k = paramm;
    localObject1 = paramc;
    l = paramc;
    localObject1 = paramf4;
    C = paramf4;
    localObject1 = paramh;
    m = paramh;
    localObject1 = paramp;
    r = paramp;
    localObject1 = paramf5;
    s = paramf5;
    localObject1 = paramm1;
    K = paramm1;
    localObject1 = paramf8;
    M = paramf8;
    localObject1 = paraml1;
    N = paraml1;
    localObject1 = paraml2;
    O = paraml2;
    localObject1 = paramj;
    w = paramj;
    localObject1 = paramap;
    ab = paramap;
    localObject1 = parama1;
    G = parama1;
    localObject1 = paramf6;
    D = paramf6;
    localObject1 = paramc1;
    E = paramc1;
    localObject1 = paramh1;
    am = paramh1;
    an = localbi;
    localObject1 = parame;
    H = parame;
    localObject1 = paramf7;
    L = paramf7;
    localObject1 = parambu;
    P = parambu;
    localObject1 = parambw;
    Q = parambw;
    localObject1 = parama2;
    R = parama2;
    localObject1 = paramk;
    S = paramk;
    localObject1 = paramf9;
    T = paramf9;
    localObject1 = paramf10;
    U = paramf10;
    localObject1 = parambg;
    V = parambg;
    localObject1 = paramj1;
    ay = paramj1;
    localObject1 = parama3;
    az = parama3;
    localObject1 = paramai;
    aA = paramai;
    localObject1 = paramb;
    aB = paramb;
    localObject1 = paramf;
    aD = paramf;
    localObject1 = paramf11;
    aE = paramf11;
    localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = Integer.valueOf(paramInt2);
      aC = ((Integer)localObject1);
    }
  }
  
  private void F()
  {
    Object localObject1 = d;
    if (localObject1 != null)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        localObject1 = d;
        boolean bool1 = g.c((Participant[])localObject1);
        boolean bool2 = false;
        if (bool1)
        {
          localObject1 = c;
          boolean bool3 = false;
          localObject2 = null;
          if (localObject1 == null)
          {
            bool1 = false;
            localObject1 = null;
          }
          else
          {
            localObject1 = x;
          }
          if (localObject1 != null)
          {
            be localbe = (be)b;
            String str = org.c.a.a.a.k.n(b);
            localbe.a(str);
            localbe = (be)b;
            str = c;
            if (str != null) {
              localObject2 = Uri.parse(c);
            }
            localbe.a((Uri)localObject2);
            bool3 = com.truecaller.messaging.i.a.a((ImGroupInfo)localObject1);
            if (bool3)
            {
              ((be)b).Q();
              localObject2 = (be)b;
              i2 = 2131886444;
              ((be)localObject2).s(i2);
            }
            localObject2 = (be)b;
            int i1 = h;
            int i2 = 1;
            if (i1 == i2) {
              bool2 = true;
            }
            ((be)localObject2).d(bool2);
          }
          return;
        }
        localObject1 = (be)b;
        Object localObject2 = org.c.a.a.a.k.n(ae);
        ((be)localObject1).a((String)localObject2);
        localObject1 = (be)b;
        localObject2 = af;
        ((be)localObject1).a((Uri)localObject2);
        ((be)b).d(false);
        return;
      }
    }
  }
  
  private void G()
  {
    Object localObject1 = t;
    if (localObject1 != null) {
      ((com.truecaller.androidactors.a)localObject1).a();
    }
    localObject1 = x;
    if (localObject1 != null)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        localObject1 = j.a();
        Object localObject2 = localObject1;
        localObject2 = (com.truecaller.messaging.data.o)localObject1;
        long l1 = x.b.a;
        int i1 = g;
        int i2 = x.b.q;
        Integer localInteger = aC;
        localObject1 = ((com.truecaller.messaging.data.o)localObject2).a(l1, i1, i2, localInteger);
        localObject2 = h;
        -..Lambda.pAD1cMJ27lS58JrSKWoYnqEj3y4 localpAD1cMJ27lS58JrSKWoYnqEj3y4 = new com/truecaller/messaging/conversation/-$$Lambda$pAD1cMJ27lS58JrSKWoYnqEj3y4;
        localpAD1cMJ27lS58JrSKWoYnqEj3y4.<init>(this);
        localObject1 = ((com.truecaller.androidactors.w)localObject1).a((i)localObject2, localpAD1cMJ27lS58JrSKWoYnqEj3y4);
        t = ((com.truecaller.androidactors.a)localObject1);
        return;
      }
    }
    a(null);
  }
  
  private void H()
  {
    Object localObject1 = y;
    if (localObject1 != null) {
      ((com.truecaller.androidactors.a)localObject1).a();
    }
    localObject1 = d;
    Object localObject2;
    Object localObject3;
    if (localObject1 != null)
    {
      localObject1 = (com.truecaller.messaging.data.o)j.a();
      localObject2 = d;
      int i1 = g;
      localObject1 = ((com.truecaller.messaging.data.o)localObject1).a((Participant[])localObject2, i1);
      localObject2 = h;
      localObject3 = new com/truecaller/messaging/conversation/-$$Lambda$_wdI936e2iFQrb_GzFKr-FN98c4;
      ((-..Lambda._wdI936e2iFQrb_GzFKr-FN98c4)localObject3).<init>(this);
      localObject1 = ((com.truecaller.androidactors.w)localObject1).a((i)localObject2, (ac)localObject3);
      y = ((com.truecaller.androidactors.a)localObject1);
      return;
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      localObject1 = (com.truecaller.messaging.data.o)j.a();
      long l1 = e.longValue();
      localObject1 = ((com.truecaller.messaging.data.o)localObject1).a(l1);
      localObject2 = h;
      localObject3 = new com/truecaller/messaging/conversation/-$$Lambda$0jVKZIRo0yNq91gBqVjOATNEVwA;
      ((-..Lambda.0jVKZIRo0yNq91gBqVjOATNEVwA)localObject3).<init>(this);
      localObject1 = ((com.truecaller.androidactors.w)localObject1).a((i)localObject2, (ac)localObject3);
      y = ((com.truecaller.androidactors.a)localObject1);
      return;
    }
    AssertionUtil.AlwaysFatal.fail(new String[] { "At least one of conversation ID or participants list has to be not null" });
  }
  
  private void I()
  {
    am.d();
  }
  
  private boolean J()
  {
    Object localObject = an.i();
    Entity[] arrayOfEntity = n;
    int i1 = arrayOfEntity.length;
    int i2 = 0;
    int i3;
    for (;;)
    {
      i3 = 1;
      if (i2 >= i1) {
        break;
      }
      Entity localEntity = arrayOfEntity[i2];
      boolean bool = localEntity.b();
      if (!bool)
      {
        bool = localEntity.d();
        if (!bool) {}
      }
      else
      {
        int i4 = i;
        if (i4 == 0)
        {
          i5 = 1;
          break label92;
        }
      }
      i2 += 1;
    }
    int i5 = 0;
    arrayOfEntity = null;
    label92:
    int i6 = j;
    i1 = 2;
    if ((i6 != i1) && (i5 != 0))
    {
      localObject = an;
      i6 = ((bi)localObject).e();
      if (i6 == i3) {
        return i3;
      }
    }
    return false;
  }
  
  private boolean K()
  {
    Object localObject = an;
    int i1 = ((bi)localObject).e();
    int i3 = 1;
    if (i1 == i3)
    {
      localObject = an.i();
      boolean bool = ((Message)localObject).h();
      if (bool)
      {
        localObject = an.i();
        int i2 = j;
        int i4 = 5;
        if (i2 != i4) {
          return i3;
        }
      }
    }
    return false;
  }
  
  private boolean L()
  {
    Message[] arrayOfMessage = an.k();
    int i1 = arrayOfMessage.length;
    int i2 = 0;
    while (i2 < i1)
    {
      Message localMessage = arrayOfMessage[i2];
      boolean bool = localMessage.d();
      if (bool)
      {
        bool = localMessage.h();
        if (!bool) {}
      }
      else
      {
        bool = localMessage.f();
        if (!bool)
        {
          bool = localMessage.i();
          if (!bool)
          {
            int i4 = j;
            int i3 = 5;
            if (i4 != i3)
            {
              i2 += 1;
              continue;
            }
          }
        }
      }
      return false;
    }
    return true;
  }
  
  private boolean M()
  {
    Message localMessage = an.i();
    int i1 = f;
    int i2 = 65;
    return i1 == i2;
  }
  
  private void N()
  {
    Object localObject1 = d;
    boolean bool1 = org.c.a.a.a.a.a((Object[])localObject1);
    if (!bool1)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        localObject1 = (be)b;
        Object localObject2 = d;
        boolean bool2 = false;
        localObject2 = localObject2[0].a();
        Object localObject3 = o;
        boolean bool3 = ((al)localObject3).a();
        if (bool3)
        {
          localObject3 = d[0];
          bool3 = ((Participant)localObject3).i();
          if (bool3) {
            bool2 = true;
          }
        }
        ((be)localObject1).a((String)localObject2, bool2);
      }
    }
  }
  
  private void O()
  {
    List localList = am.b();
    ListIterator localListIterator = localList.listIterator();
    for (;;)
    {
      boolean bool1 = localListIterator.hasNext();
      if (!bool1) {
        break;
      }
      com.avito.konveyor.b.a locala = (com.avito.konveyor.b.a)localListIterator.next();
      long l1 = locala.a();
      long l2 = 4284967296L;
      boolean bool2 = l1 < l2;
      if (!bool2) {
        localListIterator.remove();
      }
    }
    am.a(localList);
  }
  
  private void P()
  {
    Object localObject1 = c;
    if (localObject1 == null) {
      localObject1 = d;
    } else {
      localObject1 = l;
    }
    Object localObject2 = b;
    if ((localObject2 != null) && (localObject1 != null))
    {
      int i1 = localObject1.length;
      int i2 = 1;
      if (i1 == i2)
      {
        ((be)b).k();
        localObject2 = (be)b;
        i2 = 0;
        localObject1 = 0f;
        ((be)localObject2).e((String)localObject1);
      }
    }
  }
  
  private void Q()
  {
    Object localObject = d;
    if (localObject != null)
    {
      int i1 = localObject.length;
      int i2 = 1;
      if (i1 == i2)
      {
        localObject = b;
        if (localObject != null)
        {
          ((be)b).k();
          localObject = aA;
          String str = d[0].f;
          ((ai)localObject).a(str, "conversation");
          return;
        }
      }
    }
  }
  
  private boolean R()
  {
    boolean bool = Y;
    if (!bool)
    {
      m localm = K;
      bool = localm.w();
      if (!bool)
      {
        localm = K;
        bool = localm.x();
        if (!bool) {
          return false;
        }
      }
    }
    return true;
  }
  
  private void S()
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    boolean bool1 = R();
    if (bool1)
    {
      boolean bool2 = X;
      if (!bool2)
      {
        ((be)b).s();
        X = true;
        return;
      }
    }
    if (!bool1)
    {
      bool1 = X;
      if (bool1)
      {
        ((be)b).t();
        bool1 = false;
        localObject = null;
        X = false;
      }
    }
  }
  
  private boolean T()
  {
    Object localObject = p;
    boolean bool = ((com.truecaller.utils.d)localObject).a();
    if (!bool)
    {
      localObject = b;
      if (localObject != null)
      {
        ((be)b).g(7);
        return true;
      }
    }
    return false;
  }
  
  private boolean U()
  {
    com.truecaller.utils.l locall = O;
    boolean bool = locall.b();
    if (bool)
    {
      locall = O;
      bool = locall.c();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  private void V()
  {
    Object localObject = d;
    if (localObject != null)
    {
      int i1 = localObject.length;
      int i2 = 1;
      if (i1 == i2)
      {
        localObject = Q;
        boolean bool = ((bw)localObject).a();
        if (bool)
        {
          localObject = d;
          i2 = 0;
          localObject = localObject[0];
          int i3 = c;
          if (i3 != 0) {
            return;
          }
          bp localbp = (bp)T.a();
          localObject = Collections.singletonList(f);
          localbp.a((Collection)localObject, false).c();
          return;
        }
      }
    }
  }
  
  private void W()
  {
    Object localObject = ay;
    boolean bool = ((com.truecaller.messaging.j)localObject).a();
    if (bool)
    {
      localObject = b;
      if (localObject != null) {
        ((be)b).g(5);
      }
    }
    else
    {
      N();
    }
  }
  
  private String X()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("truecaller://utility");
    Object localObject = J;
    if (localObject != null)
    {
      localStringBuilder.append("/");
      localObject = J.getCategory();
      localStringBuilder.append((String)localObject);
    }
    return localStringBuilder.toString();
  }
  
  private boolean Y()
  {
    Participant[] arrayOfParticipant = d;
    if (arrayOfParticipant != null)
    {
      boolean bool = g.c(arrayOfParticipant);
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  private boolean Z()
  {
    Object localObject = c;
    if (localObject != null)
    {
      localObject = x;
      if (localObject != null)
      {
        localObject = c.x;
        int i1 = f;
        if (i1 == 0) {
          return true;
        }
      }
    }
    return false;
  }
  
  private SpannableStringBuilder a(SpannableStringBuilder paramSpannableStringBuilder, int paramInt1, int paramInt2)
  {
    com.truecaller.utils.o localo = n;
    Object[] arrayOfObject = new Object[0];
    String str = localo.a(paramInt2, arrayOfObject);
    a(paramSpannableStringBuilder, paramInt1, str, true);
    return paramSpannableStringBuilder;
  }
  
  private SpannableStringBuilder a(SpannableStringBuilder paramSpannableStringBuilder, int paramInt, String paramString)
  {
    a(paramSpannableStringBuilder, paramInt, paramString, true);
    return paramSpannableStringBuilder;
  }
  
  private SpannableStringBuilder a(SpannableStringBuilder paramSpannableStringBuilder, int paramInt, String paramString, boolean paramBoolean)
  {
    StyleSpan localStyleSpan = new android/text/style/StyleSpan;
    localStyleSpan.<init>(1);
    int i1 = paramSpannableStringBuilder.length();
    com.truecaller.utils.o localo = n;
    Object[] arrayOfObject = new Object[0];
    String str = localo.a(paramInt, arrayOfObject);
    paramSpannableStringBuilder.append(str);
    paramInt = paramSpannableStringBuilder.length();
    paramSpannableStringBuilder.setSpan(localStyleSpan, i1, paramInt, 0);
    str = " ";
    paramSpannableStringBuilder.append(str);
    paramSpannableStringBuilder.append(paramString);
    if (paramBoolean)
    {
      str = "\n";
      paramSpannableStringBuilder.append(str);
    }
    return paramSpannableStringBuilder;
  }
  
  private void a(Participant paramParticipant, ConversationAction... paramVarArgs)
  {
    Object localObject1 = al;
    ((ArrayList)localObject1).clear();
    if (paramParticipant != null)
    {
      localObject1 = o;
      boolean bool1 = ((al)localObject1).a();
      if (bool1)
      {
        localObject1 = r;
        bool1 = ((p)localObject1).g();
        int i1 = 1;
        if (bool1)
        {
          localObject1 = H.w();
          bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
          if (!bool1)
          {
            bool1 = true;
            break label84;
          }
        }
        bool1 = false;
        localObject1 = null;
        label84:
        boolean bool2 = paramParticipant.g();
        if (bool2)
        {
          bool2 = paramParticipant.a(bool1);
          if (!bool2)
          {
            bool2 = true;
            break label120;
          }
        }
        bool2 = false;
        Object localObject2 = null;
        label120:
        Object localObject3;
        if (bool2)
        {
          localObject2 = H.w();
          bool2 = ((com.truecaller.featuretoggles.b)localObject2).a();
          if (!bool2)
          {
            localObject2 = al;
            localObject3 = ConversationAction.TOP_NOT_SPAM;
            ((ArrayList)localObject2).add(localObject3);
          }
        }
        localObject2 = H.w();
        bool2 = ((com.truecaller.featuretoggles.b)localObject2).a();
        Object localObject5;
        if (bool2)
        {
          localObject2 = ConversationAction.TOP_BLOCK;
          localObject3 = n;
          Object localObject4 = new Object[i1];
          String str = ae;
          localObject4[0] = str;
          localObject3 = ((com.truecaller.utils.o)localObject3).a(2131886405, (Object[])localObject4);
          dynamicTitle = ((String)localObject3);
          localObject2 = ConversationAction.TOP_UNBLOCK;
          localObject3 = n;
          int i2 = 2131886406;
          localObject5 = new Object[i1];
          localObject4 = ae;
          localObject5[0] = localObject4;
          localObject5 = ((com.truecaller.utils.o)localObject3).a(i2, (Object[])localObject5);
          dynamicTitle = ((String)localObject5);
        }
        else
        {
          TOP_BLOCKdynamicTitle = null;
          localObject5 = ConversationAction.TOP_UNBLOCK;
          dynamicTitle = null;
        }
        boolean bool3 = paramParticipant.a(bool1);
        if (bool3) {
          paramParticipant = ConversationAction.TOP_UNBLOCK;
        } else {
          paramParticipant = ConversationAction.TOP_BLOCK;
        }
        localObject1 = al;
        ((ArrayList)localObject1).add(paramParticipant);
      }
    }
    Collections.addAll(al, paramVarArgs);
  }
  
  private void a(SenderInfo paramSenderInfo)
  {
    if (paramSenderInfo != null)
    {
      Object localObject = paramSenderInfo.getCategory();
      if (localObject != null)
      {
        localObject = paramSenderInfo.getCategory();
        int i1 = -1;
        int i2 = ((String)localObject).hashCode();
        String str;
        boolean bool;
        ConversationAction localConversationAction;
        switch (i2)
        {
        default: 
          break;
        case 1789494714: 
          str = "datacard";
          bool = ((String)localObject).equals(str);
          if (bool) {
            i1 = 2;
          }
          break;
        case 958132849: 
          str = "electricity";
          bool = ((String)localObject).equals(str);
          if (bool) {
            i1 = 4;
          }
          break;
        case 757836652: 
          str = "postpaid";
          bool = ((String)localObject).equals(str);
          if (bool) {
            i1 = 7;
          }
          break;
        case 3016252: 
          str = "bank";
          bool = ((String)localObject).equals(str);
          if (bool)
          {
            i1 = 0;
            localConversationAction = null;
          }
          break;
        case 102105: 
          str = "gas";
          bool = ((String)localObject).equals(str);
          if (bool) {
            i1 = 5;
          }
          break;
        case 99800: 
          str = "dth";
          bool = ((String)localObject).equals(str);
          if (bool) {
            i1 = 3;
          }
          break;
        case 97920: 
          str = "bus";
          bool = ((String)localObject).equals(str);
          if (bool) {
            i1 = 9;
          }
          break;
        case -318370833: 
          str = "prepaid";
          bool = ((String)localObject).equals(str);
          if (bool) {
            i1 = 8;
          }
          break;
        case -1616620449: 
          str = "landline";
          bool = ((String)localObject).equals(str);
          if (bool) {
            i1 = 6;
          }
          break;
        case -1618906185: 
          str = "broadband";
          bool = ((String)localObject).equals(str);
          if (bool) {
            i1 = 1;
          }
          break;
        }
        switch (i1)
        {
        default: 
          break;
        case 9: 
          localObject = H.o();
          bool = ((com.truecaller.featuretoggles.b)localObject).a();
          if (bool)
          {
            localObject = al;
            localConversationAction = ConversationAction.TOP_BOOK_TICKET;
            ((ArrayList)localObject).add(localConversationAction);
          }
          break;
        case 1: 
        case 2: 
        case 3: 
        case 4: 
        case 5: 
        case 6: 
        case 7: 
        case 8: 
          localObject = H.o();
          bool = ((com.truecaller.featuretoggles.b)localObject).a();
          if (bool)
          {
            localObject = al;
            localConversationAction = ConversationAction.TOP_RECHARGE;
            ((ArrayList)localObject).add(localConversationAction);
          }
          break;
        case 0: 
          localObject = al;
          localConversationAction = ConversationAction.TOP_CONNECT_BANK;
          ((ArrayList)localObject).add(localConversationAction);
        }
        J = paramSenderInfo;
        paramSenderInfo = b;
        if (paramSenderInfo != null)
        {
          paramSenderInfo = (be)b;
          paramSenderInfo.R();
        }
      }
    }
  }
  
  private void a(Boolean paramBoolean)
  {
    if (paramBoolean != null)
    {
      boolean bool = paramBoolean.booleanValue();
      if (bool)
      {
        H();
        return;
      }
    }
  }
  
  private void a(String paramString1, String paramString2)
  {
    Object localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>("ViewAction");
    String str1 = "conversation";
    localObject = ((e.a)localObject).a("Context", str1);
    String str2 = "Action";
    paramString1 = ((e.a)localObject).a(str2, paramString1);
    if (paramString2 != null)
    {
      localObject = "SubAction";
      paramString1.a((String)localObject, paramString2);
    }
    paramString2 = ab;
    paramString1 = paramString1.a();
    paramString2.a(paramString1);
  }
  
  private void a(boolean paramBoolean, long... paramVarArgs)
  {
    if (paramBoolean)
    {
      boolean bool = U();
      if (!bool)
      {
        j(9);
        return;
      }
    }
    com.truecaller.androidactors.w localw = ((t)i.a()).a(paramBoolean, paramVarArgs);
    i locali = h;
    -..Lambda.bc.7ZfAIXt8PFIiZRb87pUIN3Y4qhM local7ZfAIXt8PFIiZRb87pUIN3Y4qhM = new com/truecaller/messaging/conversation/-$$Lambda$bc$7ZfAIXt8PFIiZRb87pUIN3Y4qhM;
    local7ZfAIXt8PFIiZRb87pUIN3Y4qhM.<init>(this, paramVarArgs);
    localw.a(locali, local7ZfAIXt8PFIiZRb87pUIN3Y4qhM);
  }
  
  private void a(Entity[] paramArrayOfEntity)
  {
    paramArrayOfEntity = ((ba)D.a()).a(paramArrayOfEntity);
    i locali = h;
    -..Lambda.bc.zlHIznwr8WEorGKaDhaFNq2nEjc localzlHIznwr8WEorGKaDhaFNq2nEjc = new com/truecaller/messaging/conversation/-$$Lambda$bc$zlHIznwr8WEorGKaDhaFNq2nEjc;
    localzlHIznwr8WEorGKaDhaFNq2nEjc.<init>(this);
    paramArrayOfEntity.a(locali, localzlHIznwr8WEorGKaDhaFNq2nEjc);
  }
  
  private void a(Participant[] paramArrayOfParticipant)
  {
    Object localObject1 = b;
    Object localObject2 = new String[0];
    AssertionUtil.isNotNull(localObject1, (String[])localObject2);
    localObject1 = g.a(paramArrayOfParticipant);
    ae = ((String)localObject1);
    localObject1 = H.w();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
    int i5 = 2;
    int i6 = 1;
    if (bool1)
    {
      i1 = g;
      i7 = 4;
      if (i1 != i7)
      {
        if (i1 == i5)
        {
          i1 = paramArrayOfParticipant.length;
          i7 = 0;
          localObject3 = null;
          while (i7 < i1)
          {
            localParticipant = paramArrayOfParticipant[i7];
            int i9 = c;
            if (i9 == i6)
            {
              i1 = 1;
              break label132;
            }
            i7 += 1;
          }
          i1 = 0;
          localObject1 = null;
          label132:
          if (i1 == 0) {}
        }
      }
      else
      {
        i1 = 0;
        localObject1 = null;
        break label151;
      }
    }
    int i1 = 1;
    label151:
    int i7 = paramArrayOfParticipant.length;
    int i10 = 0;
    Participant localParticipant = null;
    Object localObject4;
    while (i10 < i7)
    {
      localObject4 = paramArrayOfParticipant[i10];
      boolean bool7 = ((Participant)localObject4).g();
      if (bool7)
      {
        i7 = 1;
        break label207;
      }
      int i11;
      i10 += 1;
    }
    i7 = 0;
    Object localObject3 = null;
    label207:
    aj = i7;
    if (i1 != 0)
    {
      localObject3 = (be)b;
      bool6 = aj;
      ((be)localObject3).c(bool6);
    }
    i7 = paramArrayOfParticipant.length;
    boolean bool6 = false;
    localParticipant = null;
    long l1;
    Object localObject5;
    if (i7 == i6)
    {
      boolean bool5 = g.b(paramArrayOfParticipant);
      if (!bool5)
      {
        localObject3 = paramArrayOfParticipant[0];
        localObject4 = o;
        l1 = p;
        String str1 = n;
        localObject4 = ((al)localObject4).a(l1, str1, i6);
        af = ((Uri)localObject4);
        ((be)b).b(false);
        localObject4 = n;
        localObject5 = r;
        boolean bool9 = ((p)localObject5).g();
        if (bool9)
        {
          localObject5 = H.w();
          bool9 = ((com.truecaller.featuretoggles.b)localObject5).a();
          if (!bool9)
          {
            bool9 = true;
            break label390;
          }
        }
        bool9 = false;
        localObject5 = null;
        label390:
        localObject4 = g.a((n)localObject4, (Participant)localObject3, bool9);
        if ((localObject4 != null) && (i1 != 0))
        {
          ((be)b).a(null);
          localObject1 = (be)b;
          ((be)localObject1).b((String)localObject4);
        }
        else
        {
          localObject1 = (be)b;
          localObject4 = l.a((Participant)localObject3);
          ((be)localObject1).a((c.a)localObject4);
        }
        boolean bool2 = ((Participant)localObject3).g();
        if (bool2)
        {
          int i2 = q;
          localObject1 = Integer.valueOf(i2);
          ai = ((Integer)localObject1);
        }
        else
        {
          ai = null;
        }
        boolean bool3 = ((Participant)localObject3).d();
        int i12;
        if (bool3)
        {
          localObject1 = new ConversationAction[0];
          a((Participant)localObject3, (ConversationAction[])localObject1);
        }
        else
        {
          localObject1 = K;
          bool3 = ((m)localObject1).C();
          if (bool3)
          {
            localObject1 = H.n();
            bool3 = ((com.truecaller.featuretoggles.b)localObject1).a();
            if (bool3)
            {
              localObject1 = o;
              bool3 = ((al)localObject1).a();
              if (bool3)
              {
                localObject1 = I;
                if (localObject1 != null) {
                  ((com.truecaller.androidactors.a)localObject1).a();
                }
                localObject1 = ab.c(f);
                localObject1 = ((aa)C.a()).d((String)localObject1);
                localObject4 = h;
                localObject5 = new com/truecaller/messaging/conversation/-$$Lambda$bc$1j2X-3mcJKGD2JRZ2coUrwTsdcU;
                ((-..Lambda.bc.1j2X-3mcJKGD2JRZ2coUrwTsdcU)localObject5).<init>(this);
                localObject1 = ((com.truecaller.androidactors.w)localObject1).a((i)localObject4, (ac)localObject5);
                I = ((com.truecaller.androidactors.a)localObject1);
              }
            }
            ((be)b).Q();
            localObject1 = (be)b;
            i12 = 2131886471;
            ((be)localObject1).s(i12);
            localObject1 = new ConversationAction[0];
            a((Participant)localObject3, (ConversationAction[])localObject1);
          }
          else
          {
            localObject1 = a;
            a((Participant)localObject3, (ConversationAction[])localObject1);
          }
        }
        ah = ((Participant)localObject3);
        bool3 = ((Participant)localObject3).d();
        if (!bool3)
        {
          int i3 = j;
          if (i3 != i6)
          {
            i3 = j;
            if (i3 != i5)
            {
              localObject1 = am.b();
              localObject2 = am;
              i5 = ((h)localObject2).e();
              i12 = -1;
              if (i5 != i12)
              {
                localObject4 = new com/truecaller/messaging/conversation/a/a/b;
                localObject5 = al;
                ((com.truecaller.messaging.conversation.a.a.b)localObject4).<init>((List)localObject5);
                ((List)localObject1).set(i5, localObject4);
              }
              else
              {
                localObject2 = new com/truecaller/messaging/conversation/a/a/b;
                localObject4 = al;
                ((com.truecaller.messaging.conversation.a.a.b)localObject2).<init>((List)localObject4);
                ((List)localObject1).add(localObject2);
              }
              localObject2 = am;
              ((h)localObject2).a((List)localObject1);
              break label881;
            }
          }
        }
        O();
        label881:
        localObject1 = (be)b;
        i5 = com.truecaller.util.w.a(r, false);
        ((be)localObject1).b(i5);
        break label971;
      }
    }
    O();
    af = null;
    ((be)b).b(i6);
    localObject1 = new ConversationAction[0];
    a(null, (ConversationAction[])localObject1);
    boolean bool4 = g.c(paramArrayOfParticipant);
    if (!bool4)
    {
      localObject1 = (be)b;
      ((be)localObject1).a(null);
    }
    label971:
    F();
    localObject1 = (be)b;
    ((be)localObject1).e();
    bool4 = W;
    if (!bool4)
    {
      localObject1 = o;
      bool4 = ((al)localObject1).a();
      if (bool4)
      {
        int i4 = paramArrayOfParticipant.length;
        i5 = 0;
        localObject2 = null;
        while (i5 < i4)
        {
          localObject3 = paramArrayOfParticipant[i5];
          boolean bool8 = ((Participant)localObject3).h();
          if (bool8)
          {
            localObject4 = aB;
            l1 = t;
            int i13 = o;
            bool8 = ((com.truecaller.search.b)localObject4).a(l1, i13);
            if (bool8)
            {
              localObject4 = q;
              localObject5 = UUID.randomUUID();
              String str2 = "conversation";
              localObject4 = ((com.truecaller.network.search.l)localObject4).a((UUID)localObject5, str2).a();
              localObject3 = f;
              i = ((String)localObject3);
              int i8 = 20;
              h = i8;
              b = false;
              localObject3 = ap;
              ((com.truecaller.network.search.j)localObject4).a(null, i6, false, (j.b)localObject3);
            }
          }
          i5 += 1;
        }
      }
    }
    W = i6;
  }
  
  private static boolean a(be parambe)
  {
    String str = "android.permission.READ_EXTERNAL_STORAGE";
    boolean bool1 = parambe.k(str);
    if (!bool1)
    {
      str = "android.permission.WRITE_EXTERNAL_STORAGE";
      boolean bool2 = parambe.k(str);
      if (!bool2) {
        return false;
      }
    }
    return true;
  }
  
  private static List b(Participant[] paramArrayOfParticipant)
  {
    ArrayList localArrayList1 = new java/util/ArrayList;
    localArrayList1.<init>();
    ArrayList localArrayList2 = new java/util/ArrayList;
    localArrayList2.<init>();
    ArrayList localArrayList3 = new java/util/ArrayList;
    localArrayList3.<init>();
    int i1 = paramArrayOfParticipant.length;
    int i2 = 0;
    while (i2 < i1)
    {
      Object localObject = paramArrayOfParticipant[i2];
      String str = f;
      localArrayList1.add(str);
      int i3 = c;
      if (i3 == 0) {
        str = "PHONE_NUMBER";
      } else {
        str = "OTHER";
      }
      localArrayList2.add(str);
      localObject = ((Participant)localObject).a();
      localArrayList3.add(localObject);
      i2 += 1;
    }
    paramArrayOfParticipant = new List[3];
    paramArrayOfParticipant[0] = localArrayList1;
    paramArrayOfParticipant[1] = localArrayList2;
    paramArrayOfParticipant[2] = localArrayList3;
    return Arrays.asList(paramArrayOfParticipant);
  }
  
  private void b(Message paramMessage, int paramInt)
  {
    int i1 = 1;
    if ((paramInt == 0) || (paramInt == i1))
    {
      boolean bool1 = T();
      if (bool1)
      {
        aw = paramMessage;
        ax = paramInt;
        return;
      }
    }
    int i2 = f;
    int i3 = 9;
    i2 &= i3;
    if (i2 != i3)
    {
      i2 = f;
      i3 = 65;
      i2 &= i3;
      if (i2 != i3) {
        return;
      }
    }
    i2 = j;
    if (i2 == paramInt) {
      return;
    }
    boolean bool2 = paramMessage.d();
    Object localObject1;
    if (bool2)
    {
      long l1 = V.a(paramInt);
      localObject1 = n;
      i3 = localObject1.length;
      int i4 = 0;
      while (i4 < i3)
      {
        BinaryEntity localBinaryEntity = localObject1[i4];
        boolean bool3 = localBinaryEntity instanceof BinaryEntity;
        if (bool3)
        {
          localBinaryEntity = (BinaryEntity)localBinaryEntity;
          long l2 = d;
          boolean bool4 = l2 < l1;
          if (bool4) {
            break label201;
          }
        }
        i4 += 1;
      }
      i1 = 0;
      localObject2 = null;
      label201:
      if (i1 != 0)
      {
        paramMessage = b;
        if (paramMessage != null)
        {
          paramMessage = b;
          Object localObject3 = paramMessage;
          localObject3 = (be)paramMessage;
          paramMessage = V;
          long l3 = paramMessage.a(l1);
          ((be)localObject3).a(l1, l3, paramInt);
        }
        return;
      }
    }
    Object localObject2 = d;
    if (localObject2 != null)
    {
      i1 = localObject2.length;
      if (i1 > 0)
      {
        localObject2 = (t)i.a();
        localObject1 = R.e;
        localObject2 = ((t)localObject2).a(paramMessage, paramInt, (String)localObject1);
        localObject1 = new com/truecaller/messaging/conversation/-$$Lambda$bc$qVEq3mf0O5-GXMJ7DcJ84jUiNKA;
        ((-..Lambda.bc.qVEq3mf0O5-GXMJ7DcJ84jUiNKA)localObject1).<init>(this, paramMessage, paramInt);
        ((com.truecaller.androidactors.w)localObject2).a((ac)localObject1);
      }
    }
  }
  
  private void b(Boolean paramBoolean)
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    ((be)b).aa();
    localObject = Boolean.TRUE;
    if (paramBoolean != localObject)
    {
      paramBoolean = (be)b;
      int i1 = 2131886538;
      paramBoolean.m(i1);
    }
  }
  
  private void b(Message... paramVarArgs)
  {
    Object localObject1 = b;
    if ((localObject1 != null) && (paramVarArgs != null))
    {
      int i1 = paramVarArgs.length;
      if (i1 != 0)
      {
        localObject1 = new java/util/ArrayList;
        int i2 = paramVarArgs.length;
        ((ArrayList)localObject1).<init>(i2);
        Object localObject2 = new java/util/ArrayList;
        ((ArrayList)localObject2).<init>();
        int i3 = paramVarArgs.length;
        int i4 = 0;
        while (i4 < i3)
        {
          Object localObject3 = paramVarArgs[i4];
          LinkedList localLinkedList = new java/util/LinkedList;
          localLinkedList.<init>();
          Entity[] arrayOfEntity = n;
          int i5 = arrayOfEntity.length;
          int i6 = 0;
          while (i6 < i5)
          {
            Object localObject4 = arrayOfEntity[i6];
            boolean bool2 = localObject4 instanceof BinaryEntity;
            if (bool2)
            {
              localObject4 = (BinaryEntity)localObject4;
              localLinkedList.add(localObject4);
              localObject4 = b;
              ((List)localObject2).add(localObject4);
            }
            i6 += 1;
          }
          localObject3 = ((Message)localObject3).j();
          com.truecaller.messaging.c.a((Collection)localObject1, (String)localObject3, localLinkedList);
          i4 += 1;
        }
        -..Lambda.bc.V_7ajX8AXsehQ2st2vwEJ1PgzMw localV_7ajX8AXsehQ2st2vwEJ1PgzMw = null;
        aa = null;
        boolean bool1 = ((List)localObject2).isEmpty();
        if (!bool1)
        {
          bool1 = U();
          if (!bool1)
          {
            aa = paramVarArgs;
            j(12);
            return;
          }
          paramVarArgs = ((ba)D.a()).a((List)localObject2);
          localObject2 = h;
          localV_7ajX8AXsehQ2st2vwEJ1PgzMw = new com/truecaller/messaging/conversation/-$$Lambda$bc$V_7ajX8AXsehQ2st2vwEJ1PgzMw;
          localV_7ajX8AXsehQ2st2vwEJ1PgzMw.<init>(this, (ArrayList)localObject1);
          paramVarArgs.a((i)localObject2, localV_7ajX8AXsehQ2st2vwEJ1PgzMw);
          return;
        }
        ((be)b).a((ArrayList)localObject1);
        return;
      }
    }
  }
  
  private void c(Message paramMessage, int paramInt)
  {
    boolean bool = paramMessage.g();
    String str1;
    if (bool) {
      str1 = "Reply";
    } else {
      str1 = "UserInput";
    }
    com.truecaller.messaging.c.a locala = G;
    String str2 = o;
    Participant[] arrayOfParticipant = d;
    locala.a(str2, "conversation", paramInt, arrayOfParticipant, str1);
  }
  
  private void c(boolean paramBoolean)
  {
    Object localObject1 = K.u();
    if (localObject1 != null)
    {
      Object localObject2 = b;
      if (localObject2 != null)
      {
        localObject2 = y;
        if (localObject2 != null) {
          ((com.truecaller.androidactors.a)localObject2).a();
        }
        localObject2 = (t)i.a();
        localObject1 = ((t)localObject2).a((Draft)localObject1);
        if (paramBoolean)
        {
          localObject3 = h;
          localObject2 = new com/truecaller/messaging/conversation/-$$Lambda$_wdI936e2iFQrb_GzFKr-FN98c4;
          ((-..Lambda._wdI936e2iFQrb_GzFKr-FN98c4)localObject2).<init>(this);
          localObject3 = ((com.truecaller.androidactors.w)localObject1).a((i)localObject3, (ac)localObject2);
          y = ((com.truecaller.androidactors.a)localObject3);
          return;
        }
        Object localObject3 = h;
        localObject2 = K;
        localObject2.getClass();
        -..Lambda.0B4aoKbEyXlauna5ZZ-N0e6vxGk local0B4aoKbEyXlauna5ZZ-N0e6vxGk = new com/truecaller/messaging/conversation/-$$Lambda$0B4aoKbEyXlauna5ZZ-N0e6vxGk;
        local0B4aoKbEyXlauna5ZZ-N0e6vxGk.<init>((m)localObject2);
        ((com.truecaller.androidactors.w)localObject1).a((i)localObject3, local0B4aoKbEyXlauna5ZZ-N0e6vxGk);
        return;
      }
    }
  }
  
  private static boolean c(SparseBooleanArray paramSparseBooleanArray)
  {
    int i1 = paramSparseBooleanArray.indexOfKey(0);
    int i2 = 1;
    if (i1 >= 0)
    {
      bool1 = paramSparseBooleanArray.get(0);
      if (!bool1)
      {
        bool1 = true;
        break label29;
      }
    }
    boolean bool1 = false;
    label29:
    int i3 = paramSparseBooleanArray.indexOfKey(i2);
    if (i3 >= 0)
    {
      bool2 = paramSparseBooleanArray.get(i2);
      if (!bool2)
      {
        bool2 = true;
        break label62;
      }
    }
    boolean bool2 = false;
    paramSparseBooleanArray = null;
    label62:
    if ((!bool1) && (!bool2)) {
      return false;
    }
    return i2;
  }
  
  private boolean c(Message[] paramArrayOfMessage)
  {
    int i1 = paramArrayOfMessage.length;
    int i2 = 0;
    while (i2 < i1)
    {
      Entity[] arrayOfEntity = n;
      int i3 = arrayOfEntity.length;
      int i4 = 0;
      while (i4 < i3)
      {
        Object localObject = arrayOfEntity[i4];
        boolean bool1 = localObject instanceof BinaryEntity;
        if (bool1)
        {
          com.truecaller.messaging.data.providers.c localc = E;
          localObject = b;
          boolean bool2 = localc.c((Uri)localObject);
          if (bool2) {
            return true;
          }
        }
        i4 += 1;
      }
      i2 += 1;
    }
    return false;
  }
  
  private void d(String paramString)
  {
    Object localObject = new com/truecaller/analytics/e$a;
    ((e.a)localObject).<init>("CheckBalance");
    ((e.a)localObject).a("BankSymbol", paramString);
    ((e.a)localObject).a("Context", "conversation");
    paramString = ab;
    localObject = ((e.a)localObject).a();
    paramString.a((com.truecaller.analytics.e)localObject);
  }
  
  private void d(boolean paramBoolean)
  {
    Object localObject = b;
    if ((localObject != null) && (paramBoolean))
    {
      be localbe = (be)b;
      int i1 = 2131886442;
      localbe.m(i1);
    }
  }
  
  private void e(String paramString)
  {
    ap localap = ab;
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("VoiceClipPlayback");
    paramString = locala.a("Action", paramString).a();
    localap.a(paramString);
  }
  
  private void e(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      boolean bool = U();
      if (!bool)
      {
        j(10);
        return;
      }
    }
    K.v();
    Object localObject1 = c;
    if (localObject1 != null)
    {
      localObject1 = B;
      if (localObject1 != null) {
        ((com.truecaller.androidactors.a)localObject1).a();
      }
      localObject1 = i.a();
      Object localObject2 = localObject1;
      localObject2 = (t)localObject1;
      long l1 = c.a;
      int i1 = g;
      int i2 = c.q;
      Object localObject3 = ((t)localObject2).a(l1, i1, i2, paramBoolean);
      localObject1 = h;
      localObject2 = new com/truecaller/messaging/conversation/-$$Lambda$HeGEB5QnDicWkurSeDQDwvJvVNk;
      ((-..Lambda.HeGEB5QnDicWkurSeDQDwvJvVNk)localObject2).<init>(this);
      localObject3 = ((com.truecaller.androidactors.w)localObject3).a((i)localObject1, (ac)localObject2);
      B = ((com.truecaller.androidactors.a)localObject3);
    }
  }
  
  private void f(boolean paramBoolean)
  {
    String str1 = "deleteMedia";
    String str2;
    if (paramBoolean) {
      str2 = "clearStorage";
    } else {
      str2 = "keepStorage";
    }
    a(str1, str2);
  }
  
  private void g(int paramInt)
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (be)b;
      ((be)localObject).c(paramInt);
    }
  }
  
  private void g(Message paramMessage)
  {
    Object localObject = z;
    if (localObject != null) {
      ((com.truecaller.androidactors.a)localObject).a();
    }
    K.v();
    localObject = y;
    if (localObject != null) {
      ((com.truecaller.androidactors.a)localObject).a();
    }
    localObject = d;
    if (localObject != null)
    {
      com.truecaller.messaging.transport.m localm = k;
      i locali = h;
      -..Lambda.Eip93yCB3GgRH33rZhHMkbXOk-o localEip93yCB3GgRH33rZhHMkbXOk-o = new com/truecaller/messaging/conversation/-$$Lambda$Eip93yCB3GgRH33rZhHMkbXOk-o;
      localEip93yCB3GgRH33rZhHMkbXOk-o.<init>(this);
      paramMessage = localm.a(paramMessage, (Participant[])localObject, locali, localEip93yCB3GgRH33rZhHMkbXOk-o);
      y = paramMessage;
    }
  }
  
  private void h(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 != null)
    {
      localObject1 = x;
      if (localObject1 != null)
      {
        localObject1 = (com.truecaller.messaging.transport.im.a.a)aE.a();
        Object localObject2 = c.x.a;
        com.truecaller.androidactors.w localw = ((com.truecaller.messaging.transport.im.a.a)localObject1).a((String)localObject2, paramInt);
        localObject1 = h;
        localObject2 = new com/truecaller/messaging/conversation/-$$Lambda$bc$rutFUPEq_9sMoyg-I4tF6PmEH-w;
        ((-..Lambda.bc.rutFUPEq_9sMoyg-I4tF6PmEH-w)localObject2).<init>(this);
        localw.a((i)localObject1, (ac)localObject2);
        return;
      }
    }
  }
  
  private void h(Message paramMessage)
  {
    Object localObject = b;
    if (localObject != null)
    {
      K.a(paramMessage);
      localObject = (be)b;
      ReplySnippet localReplySnippet = new com/truecaller/messaging/data/types/ReplySnippet;
      localReplySnippet.<init>(paramMessage);
      ((be)localObject).a(localReplySnippet);
    }
  }
  
  private boolean i(int paramInt)
  {
    ConversationAction localConversationAction = ConversationAction.findById(paramInt);
    if (localConversationAction != null)
    {
      ArrayList localArrayList = al;
      paramInt = localArrayList.contains(localConversationAction);
      if (paramInt == 0) {
        return false;
      }
    }
    return true;
  }
  
  private void j(int paramInt)
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    localObject = (be)b;
    boolean bool = a((be)localObject);
    if (bool)
    {
      ((be)b).E();
      return;
    }
    ((be)b).q(paramInt);
  }
  
  public final void A()
  {
    e("Finish");
  }
  
  public final void B()
  {
    aD).a = null;
  }
  
  public final void C()
  {
    aD.d();
    aD.a();
  }
  
  public final void D()
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (be)b;
      ((be)localObject).e();
    }
  }
  
  final void E()
  {
    new String[1][0] = "spam links loaded callback";
    D();
  }
  
  public final void a()
  {
    t localt = (t)i.a();
    long l1 = c.a;
    localt.d(l1);
    H();
  }
  
  public final void a(int paramInt)
  {
    Object localObject1 = ConversationAction.findById(paramInt);
    Object localObject2 = b;
    if ((localObject2 != null) && (localObject1 != null))
    {
      localObject2 = bc.3.a;
      paramInt = ((ConversationAction)localObject1).ordinal();
      paramInt = localObject2[paramInt];
      int i1 = 1;
      switch (paramInt)
      {
      default: 
        break;
      case 7: 
        localObject1 = M;
        String str = "bookTicket";
        com.truecaller.analytics.bb.a((com.truecaller.androidactors.f)localObject1, "conversation", str);
        localObject1 = (be)b;
        localObject2 = "truecaller://utility/booking/redbus";
        ((be)localObject1).n((String)localObject2);
        break;
      case 6: 
        com.truecaller.analytics.bb.a(M, "conversation", "recharge");
        localObject1 = (be)b;
        localObject2 = X();
        ((be)localObject1).n((String)localObject2);
        return;
      case 5: 
        localObject1 = d;
        if (localObject1 != null)
        {
          int i2 = localObject1.length;
          if (i2 == i1)
          {
            localObject2 = J;
            if (localObject2 != null)
            {
              localObject1 = ((SenderInfo)localObject2).getSymbol();
            }
            else
            {
              i1 = 0;
              localObject2 = null;
              localObject1 = ab.c(0f);
            }
            d((String)localObject1);
            ((be)b).m((String)localObject1);
            return;
          }
        }
        break;
      case 4: 
        ad = "notspam";
        ((be)b).o();
        return;
      case 3: 
        ad = "unblock";
        ((be)b).o();
        return;
      case 2: 
        W();
        return;
      case 1: 
        localObject1 = ah;
        if (localObject1 != null)
        {
          c(i1);
          localObject1 = (be)b;
          localObject2 = ah;
          ((be)localObject1).a((Participant)localObject2);
        }
        a("save", null);
        return;
      }
      return;
    }
  }
  
  final void a(int paramInt1, int paramInt2)
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    boolean bool = true;
    int i1;
    if (paramInt1 < 0) {
      i1 = 1;
    } else {
      i1 = 0;
    }
    if ((i1 == 0) && (paramInt2 < paramInt1))
    {
      bool = false;
      localObject = null;
    }
    Y = bool;
    S();
  }
  
  final void a(int paramInt1, int paramInt2, Intent paramIntent)
  {
    int i1 = 5;
    int i2 = -1;
    Object localObject1;
    if (paramInt1 != i1)
    {
      i1 = 7;
      if (paramInt1 != i1)
      {
        i1 = 0;
        Object localObject2;
        switch (paramInt1)
        {
        default: 
          switch (paramInt1)
          {
          default: 
            break;
          case 14: 
            if (paramInt2 != i2) {
              break;
            }
            localObject1 = Z;
            if (localObject1 == null) {
              break;
            }
            a(false, (long[])localObject1);
            return;
          case 13: 
            if (paramInt2 != i2) {
              break;
            }
            e(false);
          }
          break;
        case 2: 
          paramInt1 = 2131362603;
          if (paramInt2 == paramInt1)
          {
            localObject1 = b;
            if (localObject1 != null)
            {
              ((be)b).h();
              return;
            }
          }
          if ((paramInt2 != i2) || (paramIntent == null)) {
            break;
          }
          localObject1 = b;
          if (localObject1 == null) {
            break;
          }
          localObject1 = (be)b;
          localObject2 = Boolean.valueOf(paramIntent.getBooleanExtra("RESULT_NUMBER_BLOCKED", false));
          paramIntent = Long.valueOf(paramIntent.getLongExtra("CONVERSATION_ID", -1));
          ((be)localObject1).a((Boolean)localObject2, paramIntent);
          ((be)b).h();
          return;
        case 1: 
          if (paramInt2 == i2)
          {
            localObject1 = paramIntent.getData();
            localObject1 = ((aa)C.a()).a((Uri)localObject1);
            localObject2 = h;
            paramIntent = new com/truecaller/messaging/conversation/-$$Lambda$bc$GrOtlwiZq7LS97B5aJAzOk2O1ug;
            paramIntent.<init>(this);
            ((com.truecaller.androidactors.w)localObject1).a((i)localObject2, paramIntent);
            return;
          }
          localObject1 = ((aa)C.a()).a();
          localObject2 = h;
          paramIntent = new com/truecaller/messaging/conversation/-$$Lambda$bc$-SwjyOr7I2rvzKUzS8QbYcnSEys;
          paramIntent.<init>(this);
          ((com.truecaller.androidactors.w)localObject1).a((i)localObject2, paramIntent);
          return;
        }
      }
      else if (paramInt2 == i2)
      {
        localObject1 = b;
        if (localObject1 != null)
        {
          localObject1 = aw;
          if (localObject1 != null)
          {
            paramInt2 = ax;
            int i3 = 3;
            if (paramInt2 != i3)
            {
              b((Message)localObject1, paramInt2);
              aw = null;
              ax = i3;
            }
          }
        }
      }
    }
    else if (paramInt2 == i2)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        N();
        return;
      }
    }
  }
  
  public final void a(int paramInt, Message paramMessage, boolean paramBoolean)
  {
    Object localObject = H.F();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    if (!bool) {
      return;
    }
    int i1 = j;
    int i2 = 2;
    if (i1 == i2)
    {
      localObject = (as)U.a();
      long l1 = a;
      localObject = ((as)localObject).d(l1);
      i locali = h;
      -..Lambda.bc.sQwFXx0L0gp5FrlJkXn_D9f2WaY localsQwFXx0L0gp5FrlJkXn_D9f2WaY = new com/truecaller/messaging/conversation/-$$Lambda$bc$sQwFXx0L0gp5FrlJkXn_D9f2WaY;
      localsQwFXx0L0gp5FrlJkXn_D9f2WaY.<init>(this, paramInt, paramMessage, paramBoolean);
      ((com.truecaller.androidactors.w)localObject).a(locali, localsQwFXx0L0gp5FrlJkXn_D9f2WaY);
    }
  }
  
  final void a(int paramInt, Message... paramVarArgs)
  {
    t localt = (t)i.a();
    localt.a(paramVarArgs, paramInt);
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (be)b;
      int i1 = paramVarArgs.length;
      ((be)localObject).k(i1);
    }
  }
  
  final void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    Object localObject1 = b;
    if (localObject1 == null) {
      return;
    }
    int i1 = 3;
    String[] arrayOfString = null;
    Object localObject2;
    if (paramInt != i1)
    {
      i1 = 12;
      if (paramInt != i1)
      {
        i1 = 1;
        switch (paramInt)
        {
        default: 
          break;
        case 10: 
          localObject2 = O;
          arrayOfString = new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" };
          paramInt = ((com.truecaller.utils.l)localObject2).a(paramArrayOfString, paramArrayOfInt, arrayOfString);
          if (paramInt == 0) {
            break;
          }
          e(i1);
          return;
        case 9: 
          localObject2 = O;
          arrayOfString = new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" };
          paramInt = ((com.truecaller.utils.l)localObject2).a(paramArrayOfString, paramArrayOfInt, arrayOfString);
          if (paramInt == 0) {
            break;
          }
          localObject2 = Z;
          if (localObject2 == null) {
            break;
          }
          a(i1, (long[])localObject2);
          return;
        case 8: 
          localObject2 = as;
          if (localObject2 == null) {
            break;
          }
          localObject2 = O;
          localObject1 = new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" };
          paramInt = ((com.truecaller.utils.l)localObject2).a(paramArrayOfString, paramArrayOfInt, (String[])localObject1);
          if (paramInt == 0) {
            break;
          }
          localObject2 = k;
          paramArrayOfString = (Message)as.first;
          paramArrayOfInt = (Entity)as.second;
          ((com.truecaller.messaging.transport.m)localObject2).a(paramArrayOfString, paramArrayOfInt);
          as = null;
          return;
        }
      }
      else
      {
        localObject2 = O;
        localObject1 = new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" };
        paramInt = ((com.truecaller.utils.l)localObject2).a(paramArrayOfString, paramArrayOfInt, (String[])localObject1);
        if (paramInt != 0)
        {
          localObject2 = aa;
          if (localObject2 != null) {
            b((Message[])localObject2);
          }
        }
      }
    }
    else
    {
      localObject2 = ar;
      if (localObject2 != null)
      {
        localObject2 = O;
        localObject1 = new String[] { "android.permission.READ_EXTERNAL_STORAGE" };
        paramInt = ((com.truecaller.utils.l)localObject2).a(paramArrayOfString, paramArrayOfInt, (String[])localObject1);
        if (paramInt != 0)
        {
          localObject2 = ar;
          a((Entity[])localObject2);
          ar = null;
          return;
        }
      }
    }
  }
  
  final void a(Bundle paramBundle)
  {
    if (paramBundle == null)
    {
      com.truecaller.analytics.bb.a(M, "conversation", "viewed");
      return;
    }
    Parcelable localParcelable = paramBundle.getParcelable("scroll");
    ac = localParcelable;
    int i1 = paramBundle.getInt("send_type");
    ak = i1;
    paramBundle = (Integer)paramBundle.getSerializable("limit");
    aC = paramBundle;
  }
  
  final void a(SparseBooleanArray paramSparseBooleanArray)
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    boolean bool = c(paramSparseBooleanArray);
    if (bool)
    {
      paramSparseBooleanArray = (be)b;
      localObject = n;
      Object[] arrayOfObject = new Object[0];
      localObject = ((com.truecaller.utils.o)localObject).a(2131886507, arrayOfObject);
      paramSparseBooleanArray.a((String)localObject, 13);
      return;
    }
    ((be)b).h();
  }
  
  public final void a(Menu paramMenu)
  {
    int i1 = 0;
    for (;;)
    {
      int i2 = paramMenu.size();
      if (i1 >= i2) {
        break;
      }
      MenuItem localMenuItem = paramMenu.getItem(i1);
      int i3 = localMenuItem.getItemId();
      int i9 = 1;
      Object localObject1;
      Object localObject2;
      label208:
      Object localObject3;
      int i13;
      String str;
      boolean bool4;
      int i10;
      switch (i3)
      {
      default: 
        i3 = localMenuItem.getItemId();
        boolean bool1 = i(i3);
        localMenuItem.setVisible(bool1);
        break;
      case 2131361942: 
        localObject1 = c;
        if (localObject1 != null)
        {
          localObject1 = x;
          if (localObject1 != null)
          {
            localObject1 = c.x;
            int i4 = h;
            if (i4 == i9)
            {
              boolean bool2 = Z();
              if (!bool2) {
                break label208;
              }
            }
          }
        }
        i9 = 0;
        localObject2 = null;
        localMenuItem.setVisible(i9);
        break;
      case 2131361940: 
        int i5 = localMenuItem.getItemId();
        boolean bool3 = i(i5);
        if (bool3)
        {
          localObject3 = H.w();
          boolean bool7 = ((com.truecaller.featuretoggles.b)localObject3).a();
          if (bool7)
          {
            localObject3 = n;
            i13 = 2131886406;
            localObject2 = new Object[i9];
            str = ae;
            localObject2[0] = str;
            localObject2 = ((com.truecaller.utils.o)localObject3).a(i13, (Object[])localObject2);
            localMenuItem.setTitle((CharSequence)localObject2);
          }
        }
        localMenuItem.setVisible(bool3);
        break;
      case 2131361915: 
        localObject1 = c;
        if (localObject1 != null)
        {
          localObject1 = x;
          if (localObject1 != null)
          {
            localObject1 = c.x;
            int i6 = h;
            if (i6 != i9)
            {
              bool4 = Z();
              if (!bool4) {
                break label398;
              }
            }
          }
        }
        i10 = 0;
        localObject2 = null;
        localMenuItem.setVisible(i10);
        break;
      case 2131361899: 
        bool4 = Y();
        if (bool4)
        {
          bool4 = Z();
          if (!bool4) {}
        }
        else
        {
          i10 = 0;
          localObject2 = null;
        }
        localMenuItem.setVisible(i10);
        break;
      case 2131361859: 
        bool4 = Y();
        localMenuItem.setVisible(bool4);
        break;
      case 2131361857: 
        bool4 = Y();
        if (bool4)
        {
          bool4 = Z();
          if (!bool4)
          {
            i10 = 0;
            localObject2 = null;
          }
        }
        localMenuItem.setVisible(i10);
        break;
      case 2131361844: 
        localObject1 = d;
        if (localObject1 != null)
        {
          int i11 = localObject1.length;
          if (i11 == i10)
          {
            localObject1 = localObject1[0];
            int i7 = c;
            if (i7 == 0)
            {
              localObject1 = H.e();
              boolean bool5 = ((com.truecaller.featuretoggles.b)localObject1).a();
              if (!bool5)
              {
                localObject1 = localMenuItem.getIcon().mutate();
                localObject3 = n;
                i13 = 2130968839;
                int i12 = ((com.truecaller.utils.o)localObject3).e(i13);
                PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
                ((Drawable)localObject1).setColorFilter(i12, localMode);
                localMenuItem.setVisible(i10);
                break;
              }
            }
          }
        }
        localMenuItem.setVisible(false);
        break;
      case 2131361835: 
        label398:
        int i8 = localMenuItem.getItemId();
        boolean bool6 = i(i8);
        if (bool6)
        {
          localObject3 = H.w();
          boolean bool8 = ((com.truecaller.featuretoggles.b)localObject3).a();
          if (bool8)
          {
            localObject3 = n;
            i13 = 2131886405;
            localObject2 = new Object[i10];
            str = ae;
            localObject2[0] = str;
            localObject2 = ((com.truecaller.utils.o)localObject3).a(i13, (Object[])localObject2);
            localMenuItem.setTitle((CharSequence)localObject2);
          }
        }
        localMenuItem.setVisible(bool6);
      }
      i1 += 1;
    }
  }
  
  public final void a(Contact paramContact, byte[] paramArrayOfByte)
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (be)b;
      ((be)localObject).a(paramContact, paramArrayOfByte);
    }
  }
  
  final void a(CallType paramCallType)
  {
    CallType localCallType = CallType.PHONE;
    if (paramCallType == localCallType)
    {
      P();
      return;
    }
    Q();
  }
  
  public final void a(com.truecaller.messaging.conversation.a.b.o paramo)
  {
    S.a(paramo);
  }
  
  public final void a(com.truecaller.messaging.conversation.a.b.o paramo, boolean paramBoolean)
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (be)b;
      ((be)localObject).a(paramo, paramBoolean);
    }
  }
  
  public final void a(PokeableEmoji paramPokeableEmoji)
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (be)b;
      int i1 = paramPokeableEmoji.getLargeRes();
      ((be)localObject).n(i1);
    }
  }
  
  final void a(com.truecaller.messaging.data.a.j paramj)
  {
    boolean bool1 = false;
    Object localObject1 = null;
    t = null;
    Object localObject2 = b;
    if (localObject2 == null)
    {
      if (paramj != null) {
        paramj.close();
      }
      return;
    }
    I();
    am.a(paramj);
    am.a(this);
    localObject2 = u;
    if (localObject2 != null)
    {
      ((com.truecaller.androidactors.a)localObject2).a();
      u = null;
    }
    localObject2 = c;
    if (localObject2 != null)
    {
      localObject2 = x;
      if (localObject2 != null)
      {
        localObject2 = b;
        if (localObject2 != null)
        {
          localObject2 = c.x;
          boolean bool2 = com.truecaller.messaging.i.a.a((ImGroupInfo)localObject2);
          if (bool2)
          {
            localObject2 = (be)b;
            ((be)localObject2).a(null);
          }
          else
          {
            localObject2 = (com.truecaller.messaging.transport.im.a.a)aE.a();
            localObject3 = c.x.a;
            localObject2 = ((com.truecaller.messaging.transport.im.a.a)localObject2).c((String)localObject3);
            localObject3 = h;
            localObject4 = new com/truecaller/messaging/conversation/-$$Lambda$bc$wdCBMfgp4LOXazvgt7fYw45nWt8;
            ((-..Lambda.bc.wdCBMfgp4LOXazvgt7fYw45nWt8)localObject4).<init>(this);
            localObject2 = ((com.truecaller.androidactors.w)localObject2).a((i)localObject3, (ac)localObject4);
            u = ((com.truecaller.androidactors.a)localObject2);
          }
        }
      }
    }
    int i1 = ao;
    int i2 = -1;
    int i3;
    if (i1 != i2)
    {
      localObject4 = am;
      i3 = ((h)localObject4).a();
      if (i1 == i3) {}
    }
    else
    {
      ((be)b).D();
      localObject2 = am;
      i1 = ((h)localObject2).a();
      ao = i1;
    }
    ((be)b).e();
    localObject2 = f;
    long l1;
    if (localObject2 != null)
    {
      localObject4 = am;
      l1 = ((Long)localObject2).longValue();
      i1 = ((h)localObject4).a(l1);
      if (i1 != i2)
      {
        localObject3 = (be)b;
        ((be)localObject3).l(i1);
      }
      f = null;
    }
    i1 = 1;
    i2 = 0;
    Object localObject3 = null;
    if (paramj != null)
    {
      i3 = paramj.getCount();
      if (i3 != 0) {}
    }
    else
    {
      localObject4 = d;
      if (localObject4 != null)
      {
        int i5 = localObject4.length;
        if (i5 == i1)
        {
          bool3 = g.b((Participant[])localObject4);
          if (!bool3)
          {
            bool3 = true;
            break label448;
          }
        }
      }
    }
    boolean bool3 = false;
    Object localObject4 = null;
    label448:
    Object localObject5;
    Object localObject7;
    if (bool3)
    {
      localObject4 = d[0];
      localObject5 = K;
      boolean bool4 = ((m)localObject5).A();
      int i6;
      String str;
      int i7;
      if (bool4)
      {
        localObject5 = n;
        i6 = 2131886430;
        localObject6 = new Object[i1];
        str = ((Participant)localObject4).a();
        localObject6[0] = str;
        localObject5 = ((com.truecaller.utils.o)localObject5).a(i6, (Object[])localObject6);
        localObject7 = n;
        i7 = 2130968846;
        localObject7 = ((com.truecaller.utils.o)localObject7).f(i7);
      }
      else
      {
        localObject5 = n;
        i6 = 2131886431;
        localObject6 = new Object[i1];
        str = ((Participant)localObject4).a();
        localObject6[0] = str;
        localObject5 = ((com.truecaller.utils.o)localObject5).a(i6, (Object[])localObject6);
        localObject7 = n;
        i7 = 2130968848;
        localObject7 = ((com.truecaller.utils.o)localObject7).f(i7);
      }
      Object localObject6 = ag;
      if (localObject6 == null)
      {
        localObject6 = o;
        long l2 = p;
        localObject4 = n;
        localObject3 = ((al)localObject6).a(l2, (String)localObject4, false);
        ag = ((Uri)localObject3);
      }
      localObject3 = (be)b;
      localObject4 = ag;
      ((be)localObject3).a((Uri)localObject4, (String)localObject5, (Drawable)localObject7);
    }
    else
    {
      localObject3 = (be)b;
      ((be)localObject3).X();
    }
    localObject3 = ac;
    if (localObject3 != null)
    {
      localObject3 = (be)b;
      localObject4 = ac;
      ((be)localObject3).a((Parcelable)localObject4);
      ac = null;
    }
    bool1 = av;
    if (!bool1)
    {
      long l3 = SystemClock.elapsedRealtime();
      l1 = au.longValue();
      l3 -= l1;
      double d1 = l3;
      localObject1 = ab;
      localObject5 = new com/truecaller/analytics/e$a;
      localObject7 = "ConversationLoaded";
      ((e.a)localObject5).<init>((String)localObject7);
      localObject3 = Double.valueOf(d1);
      a = ((Double)localObject3);
      localObject3 = ((e.a)localObject5).a();
      ((ap)localObject1).a((com.truecaller.analytics.e)localObject3);
      av = i1;
    }
    localObject1 = b;
    if ((localObject1 != null) && (paramj != null))
    {
      bool1 = paramj.moveToFirst();
      if (bool1)
      {
        localObject1 = az;
        paramj = paramj.b();
        paramj = ((com.truecaller.messaging.conversation.emoji.a)localObject1).a(paramj);
        if (paramj != null)
        {
          localObject1 = (be)b;
          i1 = paramj.getLargeRes();
          ((be)localObject1).o(i1);
          localObject1 = ab;
          localObject2 = new com/truecaller/analytics/e$a;
          ((e.a)localObject2).<init>("ImEmojiPoke");
          localObject4 = "Received";
          localObject2 = ((e.a)localObject2).a("Action", (String)localObject4);
          localObject3 = "Emoji";
          paramj = paramj.getAnalyticsName();
          paramj = ((e.a)localObject2).a((String)localObject3, paramj).a();
          ((ap)localObject1).a(paramj);
        }
      }
    }
    paramj = x;
    if (paramj != null)
    {
      paramj = b;
      if (paramj != null)
      {
        paramj = (t)i.a();
        long l4 = x.b.a;
        i2 = g;
        localObject4 = x.b;
        int i4 = q;
        paramj.a(l4, i2, i4);
        paramj = (as)U.a();
        localObject1 = x.b;
        l4 = a;
        paramj.a(l4);
      }
    }
  }
  
  final void a(Conversation paramConversation)
  {
    Object localObject1 = null;
    y = null;
    if (paramConversation == null)
    {
      paramConversation = b;
      if (paramConversation != null)
      {
        paramConversation = (be)b;
        paramConversation.h();
      }
      return;
    }
    localObject1 = l;
    d = ((Participant[])localObject1);
    localObject1 = an;
    Object localObject2 = d;
    ((bi)localObject1).a((Participant[])localObject2);
    localObject1 = an;
    paramConversation = x;
    ((bi)localObject1).a(paramConversation);
    paramConversation = (com.truecaller.messaging.data.o)j.a();
    localObject1 = d;
    int i1 = g;
    paramConversation = paramConversation.a((Participant[])localObject1, i1);
    localObject1 = h;
    localObject2 = new com/truecaller/messaging/conversation/-$$Lambda$_wdI936e2iFQrb_GzFKr-FN98c4;
    ((-..Lambda._wdI936e2iFQrb_GzFKr-FN98c4)localObject2).<init>(this);
    paramConversation = paramConversation.a((i)localObject1, (ac)localObject2);
    y = paramConversation;
    V();
  }
  
  final void a(Draft paramDraft)
  {
    b(paramDraft);
  }
  
  public final void a(Entity paramEntity)
  {
    boolean bool = paramEntity.a();
    if (!bool)
    {
      bool = U();
      if (!bool)
      {
        j(8);
        return;
      }
      Object localObject1 = E;
      Object localObject2 = paramEntity;
      localObject2 = (BinaryEntity)paramEntity;
      localObject1 = ((com.truecaller.messaging.data.providers.c)localObject1).a((BinaryEntity)localObject2);
      if (localObject1 != null)
      {
        localObject2 = ((ba)D.a()).a((Uri)localObject1);
        i locali = h;
        -..Lambda.bc.bQhyGzw16RhTpl7f6t2i4BxfIc0 localbQhyGzw16RhTpl7f6t2i4BxfIc0 = new com/truecaller/messaging/conversation/-$$Lambda$bc$bQhyGzw16RhTpl7f6t2i4BxfIc0;
        localbQhyGzw16RhTpl7f6t2i4BxfIc0.<init>(this, (Uri)localObject1, paramEntity);
        ((com.truecaller.androidactors.w)localObject2).a(locali, localbQhyGzw16RhTpl7f6t2i4BxfIc0);
        return;
      }
      paramEntity = b;
      if (paramEntity != null)
      {
        paramEntity = (be)b;
        int i1 = 2131886441;
        paramEntity.m(i1);
      }
    }
  }
  
  public final void a(Entity paramEntity, com.truecaller.messaging.conversation.voice_notes.e parame)
  {
    Object localObject = an;
    boolean bool1 = ((bi)localObject).d();
    if (bool1) {
      return;
    }
    if (paramEntity == null) {
      return;
    }
    bool1 = U();
    if (!bool1)
    {
      j(8);
      return;
    }
    long l1 = aF;
    long l2 = -1;
    boolean bool2 = l1 < l2;
    if (bool2)
    {
      l2 = h;
      bool2 = l1 < l2;
      if (bool2)
      {
        aD.d();
        localObject = aD;
        ((com.truecaller.messaging.conversation.voice_notes.f)localObject).a();
      }
    }
    l1 = h;
    aF = l1;
    localObject = aD;
    bool1 = ((com.truecaller.messaging.conversation.voice_notes.f)localObject).b();
    if (bool1)
    {
      aD.e();
      return;
    }
    localObject = aD;
    a = parame;
    paramEntity = b;
    ((com.truecaller.messaging.conversation.voice_notes.f)localObject).a(paramEntity);
    aD.c();
    e("Start");
  }
  
  public final void a(Message paramMessage)
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (be)b;
      ((be)localObject).u();
    }
    C();
    b(paramMessage);
  }
  
  public final void a(Message paramMessage, int paramInt)
  {
    Object localObject1 = b;
    if (localObject1 != null)
    {
      int i1 = paramMessage.b();
      if (i1 != 0)
      {
        i1 = 1;
        Object localObject2;
        switch (paramInt)
        {
        case 2131886434: 
        default: 
          break;
        case 2131886438: 
          k.b(paramMessage);
          localObject2 = k;
          boolean bool = paramMessage.d();
          Participant[] arrayOfParticipant = d;
          paramInt = ((com.truecaller.messaging.transport.m)localObject2).a(bool, arrayOfParticipant, i1);
          c(paramMessage, paramInt);
          return;
        case 2131886437: 
          b(paramMessage, 0);
          return;
        case 2131886436: 
          b(paramMessage, i1);
          return;
        case 2131886435: 
          paramInt = 2;
          b(paramMessage, paramInt);
          break;
        case 2131886433: 
          g(paramMessage);
          return;
        case 2131886432: 
          localObject2 = (t)i.a();
          long l1 = a;
          paramMessage = ((t)localObject2).c(l1);
          localObject2 = new com/truecaller/messaging/conversation/-$$Lambda$tyBNz-1In6d_NuP3DvKLVQRlBUw;
          ((-..Lambda.tyBNz-1In6d_NuP3DvKLVQRlBUw)localObject2).<init>(this);
          paramMessage.a((ac)localObject2);
          return;
        }
      }
    }
  }
  
  public final void a(Message paramMessage, Entity paramEntity)
  {
    Object localObject = b;
    if ((localObject != null) && (paramEntity != null) && (paramMessage != null))
    {
      boolean bool = U();
      if (!bool)
      {
        paramMessage = Pair.create(paramMessage, paramEntity);
        as = paramMessage;
        j(8);
        return;
      }
      k.a(paramMessage, paramEntity);
      return;
    }
  }
  
  public final void a(Message paramMessage, int[] paramArrayOfInt1, int[] paramArrayOfInt2)
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    ((be)b).a(paramMessage, paramArrayOfInt1, paramArrayOfInt2);
  }
  
  public final void a(ReplySnippet paramReplySnippet)
  {
    Object localObject = b;
    if ((localObject != null) && (paramReplySnippet != null))
    {
      localObject = am;
      long l1 = a;
      int i1 = ((h)localObject).a(l1);
      int i2 = -1;
      if (i1 != i2)
      {
        localObject = (be)b;
        ((be)localObject).l(i1);
      }
      return;
    }
  }
  
  public final void a(String paramString)
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (be)b;
      paramString = URLUtil.guessUrl(paramString);
      ((be)localObject).h(paramString);
      paramString = "link";
      localObject = null;
      a(paramString, null);
    }
  }
  
  final void a(String paramString, TruecallerContract.Filters.EntityType paramEntityType)
  {
    Object localObject1 = d;
    if (localObject1 != null)
    {
      int i1 = localObject1.length;
      if (i1 != 0)
      {
        boolean bool1 = org.c.a.a.a.k.b(paramString);
        be localbe = null;
        if (!bool1)
        {
          localObject1 = (aa)C.a();
          long l1 = d[0].i;
          localObject1 = ((aa)localObject1).a(l1);
          localObject2 = h;
          localObject3 = new com/truecaller/messaging/conversation/-$$Lambda$bc$nM1-N0yzrb1_AHlYSW1EQgm6nls;
          ((-..Lambda.bc.nM1-N0yzrb1_AHlYSW1EQgm6nls)localObject3).<init>(this, paramString, paramEntityType);
          ((com.truecaller.androidactors.w)localObject1).a((i)localObject2, (ac)localObject3);
        }
        localObject1 = b(d);
        Object localObject2 = s.a();
        Object localObject3 = localObject2;
        localObject3 = (s)localObject2;
        localObject2 = ((List)localObject1).get(0);
        Object localObject4 = localObject2;
        localObject4 = (List)localObject2;
        int i3 = 1;
        List localList1 = (List)((List)localObject1).get(i3);
        boolean bool2 = TextUtils.isEmpty(paramString);
        int i5 = 2;
        List localList2;
        if (bool2)
        {
          localList2 = (List)((List)localObject1).get(i5);
        }
        else
        {
          int i4 = ((List)((List)localObject1).get(i5)).size();
          localList2 = Collections.nCopies(i4, paramString);
        }
        String str = "conversation";
        boolean bool3 = true;
        paramEntityType = ((s)localObject3).a((List)localObject4, localList1, localList2, str, bool3, paramEntityType);
        localObject3 = h;
        localObject4 = new com/truecaller/messaging/conversation/-$$Lambda$bc$-SwjyOr7I2rvzKUzS8QbYcnSEys;
        ((-..Lambda.bc.-SwjyOr7I2rvzKUzS8QbYcnSEys)localObject4).<init>(this);
        paramEntityType.a((i)localObject3, (ac)localObject4);
        paramEntityType = b;
        if (paramEntityType != null)
        {
          boolean bool4 = TextUtils.isEmpty(paramString);
          if (bool4) {
            paramString = d[0].a();
          }
          paramEntityType = d[0].b();
          localObject3 = n;
          int i6 = 2131755028;
          localList1 = (List)((List)localObject1).get(0);
          int i7 = localList1.size();
          localObject2 = new Object[i3];
          int i2 = ((List)((List)localObject1).get(0)).size();
          localObject1 = Integer.valueOf(i2);
          localObject2[0] = localObject1;
          localObject1 = ((com.truecaller.utils.o)localObject3).a(i6, i7, (Object[])localObject2);
          localbe = (be)b;
          localbe.a(paramString, paramEntityType, (String)localObject1);
        }
        return;
      }
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject = b;
    if ((localObject != null) && (!paramBoolean))
    {
      be localbe = (be)b;
      localObject = Boolean.TRUE;
      Long localLong = e;
      localbe.a((Boolean)localObject, localLong);
      localbe = (be)b;
      localbe.h();
    }
  }
  
  final void a(boolean paramBoolean1, boolean paramBoolean2)
  {
    boolean bool;
    if ((paramBoolean1) && (paramBoolean2)) {
      bool = true;
    } else {
      bool = false;
    }
    e(bool);
    if (paramBoolean1) {
      f(paramBoolean2);
    }
  }
  
  final void a(boolean paramBoolean1, boolean paramBoolean2, long... paramVarArgs)
  {
    if (paramBoolean1) {
      f(paramBoolean2);
    }
    Z = paramVarArgs;
    if ((paramBoolean1) && (paramBoolean2)) {
      paramBoolean1 = true;
    } else {
      paramBoolean1 = false;
    }
    a(paramBoolean1, paramVarArgs);
  }
  
  final void a(Message... paramVarArgs)
  {
    Object localObject = (t)i.a();
    int i1 = 1;
    ((t)localObject).a(paramVarArgs, i1);
    localObject = b;
    if (localObject != null)
    {
      localObject = (be)b;
      int i2 = paramVarArgs.length;
      ((be)localObject).j(i2);
    }
  }
  
  public final void b()
  {
    Object localObject = d;
    if (localObject != null)
    {
      localObject = e;
      if (localObject != null)
      {
        localObject = am;
        int i1 = ((h)localObject).c();
        int i2 = 1;
        if (i1 > i2)
        {
          G();
          return;
        }
      }
    }
    H();
  }
  
  final void b(Bundle paramBundle)
  {
    Object localObject1 = b;
    if (localObject1 != null)
    {
      localObject1 = "scroll";
      localObject2 = ((be)b).w();
      paramBundle.putParcelable((String)localObject1, (Parcelable)localObject2);
    }
    int i1 = ak;
    paramBundle.putInt("send_type", i1);
    Object localObject2 = aC;
    paramBundle.putSerializable("limit", (Serializable)localObject2);
  }
  
  final void b(SparseBooleanArray paramSparseBooleanArray)
  {
    Object localObject = b;
    if (localObject != null)
    {
      boolean bool = c(paramSparseBooleanArray);
      if (bool)
      {
        paramSparseBooleanArray = (be)b;
        localObject = n;
        Object[] arrayOfObject = new Object[0];
        localObject = ((com.truecaller.utils.o)localObject).a(2131886507, arrayOfObject);
        int i1 = 6;
        paramSparseBooleanArray.a((String)localObject, i1);
      }
    }
  }
  
  final void b(Draft paramDraft)
  {
    be localbe = null;
    y = null;
    Object localObject1 = b;
    if (localObject1 == null) {
      return;
    }
    int i1;
    if (paramDraft != null)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        localObject1 = b.x;
        if (localObject1 != null)
        {
          localObject1 = b.x;
          i1 = f & 0x2;
          if (i1 != 0)
          {
            localbe = (be)b;
            paramDraft = b.x;
            localbe.b(paramDraft);
            ((be)b).h();
            return;
          }
        }
      }
    }
    localObject1 = K;
    ((m)localObject1).a(paramDraft);
    Object localObject2;
    long l3;
    if (paramDraft != null)
    {
      localObject1 = d;
      d = ((Participant[])localObject1);
      localObject1 = an;
      localObject2 = d;
      ((bi)localObject1).a((Participant[])localObject2);
      localObject1 = d;
      a((Participant[])localObject1);
      localObject1 = b;
      if (localObject1 != null)
      {
        localObject1 = c;
        if (localObject1 != null)
        {
          long l1 = a;
          Conversation localConversation = b;
          long l2 = a;
          boolean bool1 = l1 < l2;
          if (!bool1) {}
        }
        else
        {
          localObject1 = x;
          if (localObject1 != null)
          {
            localObject1 = b;
            if (localObject1 != null)
            {
              localObject1 = (com.truecaller.messaging.notifications.a)v.a();
              l3 = x.b.a;
              ((com.truecaller.messaging.notifications.a)localObject1).b(l3);
              localObject1 = (as)U.a();
              localObject2 = x.b;
              l3 = a;
              ((as)localObject1).c(l3);
            }
          }
          localObject1 = (com.truecaller.messaging.notifications.a)v.a();
          l3 = b.a;
          ((com.truecaller.messaging.notifications.a)localObject1).a(l3);
          localObject1 = (as)U.a();
          l3 = b.a;
          ((as)localObject1).b(l3);
          localObject1 = (com.truecaller.messaging.notifications.a)v.a();
          localObject2 = b;
          l3 = a;
          ((com.truecaller.messaging.notifications.a)localObject1).c(l3);
        }
      }
    }
    else
    {
      localObject1 = x;
      if (localObject1 != null)
      {
        localObject1 = b;
        if (localObject1 != null)
        {
          localObject1 = (com.truecaller.messaging.notifications.a)v.a();
          l3 = x.b.a;
          ((com.truecaller.messaging.notifications.a)localObject1).b(l3);
          localObject1 = (as)U.a();
          localObject2 = x.b;
          l3 = a;
          ((as)localObject1).c(l3);
        }
      }
    }
    x = paramDraft;
    boolean bool2;
    if (paramDraft == null)
    {
      bool2 = false;
      paramDraft = null;
    }
    else
    {
      paramDraft = b;
    }
    c = paramDraft;
    paramDraft = an;
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject1 = x;
    }
    else
    {
      i1 = 0;
      localObject1 = null;
    }
    paramDraft.a((ImGroupInfo)localObject1);
    F();
    paramDraft = c;
    if (paramDraft != null)
    {
      long l4 = a;
      paramDraft = Long.valueOf(l4);
      e = paramDraft;
      paramDraft = c;
      bool2 = m;
      if (!bool2)
      {
        paramDraft = c;
        int i2 = n;
        if (i2 <= 0) {}
      }
      else
      {
        O();
      }
      G();
    }
    else
    {
      a(null);
    }
    paramDraft = b;
    if (paramDraft != null)
    {
      paramDraft = (be)b;
      paramDraft.j();
    }
  }
  
  public final void b(Message paramMessage)
  {
    bc localbc = this;
    Object localObject1 = paramMessage;
    Object localObject2 = an;
    long l1 = a;
    boolean bool1 = ((bi)localObject2).a(l1);
    if (bool1)
    {
      localObject2 = an;
      l1 = a;
      ((bi)localObject2).b(l1);
    }
    else
    {
      localObject2 = an;
      ((bi)localObject2).a(paramMessage);
    }
    localObject1 = b;
    if (localObject1 == null) {
      return;
    }
    ((be)b).e();
    localObject1 = an;
    boolean bool2 = ((bi)localObject1).f();
    if (bool2)
    {
      localObject1 = c;
      bool1 = false;
      localObject2 = null;
      int i4 = 1;
      if (localObject1 != null)
      {
        i1 = q;
        if (i1 == i4)
        {
          i1 = 1;
          break label152;
        }
      }
      int i1 = 0;
      localObject1 = null;
      label152:
      Object localObject3 = H.w();
      boolean bool5 = ((com.truecaller.featuretoggles.b)localObject3).a();
      int i7 = 3;
      if (bool5)
      {
        int i5 = g;
        if ((i5 != i7) && (i1 != 0))
        {
          bool9 = true;
          break label205;
        }
      }
      boolean bool9 = false;
      label205:
      localObject3 = H.w();
      boolean bool6 = ((com.truecaller.featuretoggles.b)localObject3).a();
      int i6;
      if (bool6)
      {
        i6 = g;
        if ((i6 == i7) && (i1 != 0))
        {
          bool10 = true;
          break label255;
        }
      }
      boolean bool10 = false;
      label255:
      localObject1 = an;
      boolean bool3 = ((bi)localObject1).c();
      if (bool3)
      {
        localObject1 = (be)b;
        i6 = an.e();
        localObject3 = String.valueOf(i6);
        ((be)localObject1).f((String)localObject3);
      }
      localObject1 = an;
      int i2 = ((bi)localObject1).e();
      if (i2 != i4)
      {
        localObject1 = b;
        localObject4 = localObject1;
        localObject4 = (be)localObject1;
        bool11 = L();
        ((be)localObject4).a(false, false, bool11, false, bool9, bool10, false, false, false, false);
        return;
      }
      localObject1 = an.i();
      boolean bool7 = ((Message)localObject1).f();
      String str = ((Message)localObject1).j();
      boolean bool8 = TextUtils.isEmpty(str) ^ i4;
      Object localObject4 = an;
      boolean bool12 = ((bi)localObject4).g();
      boolean bool13 = J();
      boolean bool14 = K();
      boolean bool15 = L();
      boolean bool16 = M();
      if ((bool16) && (bool12)) {
        bool16 = true;
      } else {
        bool16 = false;
      }
      com.truecaller.featuretoggles.e locale = H;
      boolean bool11 = ((Message)localObject1).a(locale);
      boolean bool17;
      if ((bool11) && (bool12)) {
        bool17 = true;
      } else {
        bool17 = false;
      }
      bool12 = an.c();
      boolean bool18 = bool12 ^ true;
      localObject4 = b;
      Object localObject5 = localObject4;
      localObject5 = (be)localObject4;
      boolean bool19;
      if ((bool8) && (!bool7)) {
        bool19 = true;
      } else {
        bool19 = false;
      }
      int i8 = j;
      int i9 = 5;
      boolean bool20;
      if (i8 != i9) {
        bool20 = true;
      } else {
        bool20 = false;
      }
      boolean bool21;
      if ((bool13) && (!bool7)) {
        bool21 = true;
      } else {
        bool21 = false;
      }
      boolean bool22;
      if ((bool9) && (!bool7)) {
        bool22 = true;
      } else {
        bool22 = false;
      }
      boolean bool23;
      if ((bool10) && (!bool7)) {
        bool23 = true;
      } else {
        bool23 = false;
      }
      ((be)localObject5).a(bool19, bool20, bool15, bool21, bool22, bool23, bool14, bool16, bool17, bool18);
      if (bool16)
      {
        boolean bool4 = ((Message)localObject1).d();
        int i3;
        if (bool4) {
          i3 = 2131886436;
        } else {
          i3 = 2131886437;
        }
        localObject2 = (be)b;
        ((be)localObject2).h(i3);
      }
      return;
    }
    ((be)b).v();
  }
  
  public final void b(Message paramMessage, Entity paramEntity)
  {
    Object localObject = b;
    if ((localObject != null) && (paramEntity != null) && (paramMessage != null))
    {
      boolean bool = paramEntity.c();
      if (bool)
      {
        int i1 = j;
        int i2 = 2;
        if (i1 == i2)
        {
          localObject = k;
          int i3 = j;
          paramMessage = ((com.truecaller.messaging.transport.m)localObject).a(i3);
          paramEntity = (BinaryEntity)paramEntity;
          paramMessage.a(paramEntity);
          return;
        }
        d(paramMessage);
        return;
      }
    }
  }
  
  public final void b(String paramString)
  {
    Object localObject = b;
    if (localObject != null)
    {
      ((be)b).i(paramString);
      paramString = "email";
      localObject = null;
      a(paramString, null);
    }
  }
  
  public final void b(boolean paramBoolean)
  {
    Object localObject = b;
    if (localObject == null) {
      return;
    }
    if (paramBoolean)
    {
      ((be)b).U();
      return;
    }
    ((be)b).V();
  }
  
  final void b(boolean paramBoolean1, boolean paramBoolean2)
  {
    Object localObject1 = c;
    if (localObject1 != null)
    {
      localObject1 = x;
      if (localObject1 != null)
      {
        localObject1 = (com.truecaller.messaging.transport.im.a.a)aE.a();
        Object localObject2 = c.x.a;
        boolean bool;
        if ((paramBoolean1) && (paramBoolean2))
        {
          bool = true;
        }
        else
        {
          bool = false;
          localzOvuiuAyDk3qgoicIt51HUS1ecI = null;
        }
        localObject1 = ((com.truecaller.messaging.transport.im.a.a)localObject1).b((String)localObject2, bool);
        localObject2 = h;
        -..Lambda.bc.zOvuiuAyDk3qgoicIt51HUS1ecI localzOvuiuAyDk3qgoicIt51HUS1ecI = new com/truecaller/messaging/conversation/-$$Lambda$bc$zOvuiuAyDk3qgoicIt51HUS1ecI;
        localzOvuiuAyDk3qgoicIt51HUS1ecI.<init>(this);
        ((com.truecaller.androidactors.w)localObject1).a((i)localObject2, localzOvuiuAyDk3qgoicIt51HUS1ecI);
        if (paramBoolean1) {
          f(paramBoolean2);
        }
        return;
      }
    }
  }
  
  final boolean b(int paramInt)
  {
    Object localObject1 = b;
    int i1 = 0;
    Object localObject2 = null;
    if (localObject1 != null)
    {
      localObject1 = an;
      int i3 = ((bi)localObject1).f();
      if (i3 != 0)
      {
        i3 = 1;
        Object localObject3;
        Object localObject4;
        label682:
        int i2;
        switch (paramInt)
        {
        default: 
          return false;
        case 2131361936: 
          localObject3 = an.k();
          localObject2 = b;
          if (localObject2 != null)
          {
            localObject2 = (be)b;
            ((be)localObject2).a((Message[])localObject3);
          }
          break;
        case 2131361935: 
          localObject3 = (be)b;
          localObject2 = an.i();
          ((be)localObject3).a((Message)localObject2);
          break;
        case 2131361927: 
          localObject3 = an.i();
          i1 = ((Message)localObject3).d();
          b((Message)localObject3, i1);
          break;
        case 2131361926: 
          localObject3 = an.i();
          h((Message)localObject3);
          break;
        case 2131361916: 
          localObject3 = an.k();
          localObject2 = b;
          if (localObject2 != null)
          {
            localObject2 = (be)b;
            ((be)localObject2).b((Message[])localObject3);
          }
          break;
        case 2131361914: 
          an.a(i3);
          a("multiSelect", null);
          return i3;
        case 2131361872: 
          localObject3 = an.i();
          localObject4 = (be)b;
          int i4 = f & i3;
          Object localObject5;
          if (i4 != 0)
          {
            i4 = 1;
          }
          else
          {
            i4 = 0;
            localObject5 = null;
          }
          int i6 = j;
          Object localObject6;
          if (i6 == 0)
          {
            i6 = 1;
          }
          else
          {
            i6 = 0;
            localObject6 = null;
          }
          int i9 = j;
          String str;
          if (i9 == i3)
          {
            i9 = 1;
          }
          else
          {
            i9 = 0;
            str = null;
          }
          SpannableStringBuilder localSpannableStringBuilder = new android/text/SpannableStringBuilder;
          localSpannableStringBuilder.<init>();
          org.a.a.d.b localb = org.a.a.d.a.b("MM");
          int i10 = 2131886673;
          if (i6 != 0)
          {
            i6 = 2131886675;
            a(localSpannableStringBuilder, i10, i6);
          }
          else if (i9 != 0)
          {
            i6 = 2131886674;
            a(localSpannableStringBuilder, i10, i6);
          }
          i6 = 2131886668;
          i10 = 2131886669;
          int i11;
          if (i4 != 0)
          {
            localObject5 = c.b();
            i11 = 2131886672;
            a(localSpannableStringBuilder, i11, (String)localObject5);
            localObject5 = e;
            localObject5 = localb.a((x)localObject5);
            a(localSpannableStringBuilder, i10, (String)localObject5);
            localObject5 = m;
            i4 = ((TransportInfo)localObject5).a();
            i10 = 3;
            if (i4 == i10)
            {
              localObject5 = d;
              localObject5 = localb.a((x)localObject5);
              a(localSpannableStringBuilder, i6, (String)localObject5);
            }
          }
          else
          {
            localObject5 = c;
            i4 = c;
            if (i4 == i3)
            {
              localObject5 = p;
              boolean bool2 = org.c.a.a.a.k.c((CharSequence)localObject5);
              if (bool2)
              {
                localObject5 = p;
                break label682;
              }
            }
            localObject5 = c.b();
            i11 = 2131886662;
            a(localSpannableStringBuilder, i11, (String)localObject5);
            localObject5 = d;
            localObject5 = localb.a((x)localObject5);
            a(localSpannableStringBuilder, i10, (String)localObject5);
            localObject5 = e;
            localObject5 = localb.a((x)localObject5);
            a(localSpannableStringBuilder, i6, (String)localObject5);
          }
          if (i9 != 0)
          {
            localObject5 = (MmsTransportInfo)m;
            localObject6 = h;
            boolean bool3 = org.c.a.a.a.k.b((CharSequence)localObject6);
            if (!bool3)
            {
              i7 = 2131886671;
              str = h;
              a(localSpannableStringBuilder, i7, str);
            }
            int i7 = 2131886664;
            i9 = q;
            int i12 = 128;
            if (i9 != i12)
            {
              i12 = 130;
              if (i9 != i12) {
                i9 = 2131886667;
              } else {
                i9 = 2131886665;
              }
            }
            else
            {
              i9 = 2131886666;
            }
            a(localSpannableStringBuilder, i7, i9);
            localObject6 = u;
            boolean bool4 = TextUtils.isEmpty((CharSequence)localObject6);
            if (!bool4)
            {
              int i8 = 2131886661;
              str = u;
              a(localSpannableStringBuilder, i8, str);
            }
            localObject6 = k;
            i9 = j;
            localObject6 = ((com.truecaller.messaging.transport.m)localObject6).a(i9);
            if (localObject6 != null)
            {
              paramInt = ((com.truecaller.messaging.transport.l)localObject6).e((Message)localObject3);
              if (paramInt != i3)
              {
                paramInt = 2131886670;
                localObject6 = new java/lang/StringBuilder;
                ((StringBuilder)localObject6).<init>();
                long l1 = x + 1023L;
                long l2 = 1024L;
                l1 /= l2;
                ((StringBuilder)localObject6).append(l1);
                localObject5 = ((StringBuilder)localObject6).toString();
                a(localSpannableStringBuilder, paramInt, (String)localObject5, false);
                localObject3 = n;
                int i5 = 2131886663;
                localObject2 = new Object[0];
                localObject3 = ((com.truecaller.utils.o)localObject3).a(i5, (Object[])localObject2);
                localSpannableStringBuilder.append((CharSequence)localObject3);
              }
            }
          }
          ((be)localObject4).a(localSpannableStringBuilder);
          break;
        case 2131361868: 
          localObject3 = an.k();
          b((Message[])localObject3);
          break;
        case 2131361864: 
          localObject3 = an.i();
          g((Message)localObject3);
          break;
        case 2131361863: 
          localObject3 = an.i();
          localObject2 = O;
          localObject4 = new String[] { "android.permission.READ_EXTERNAL_STORAGE" };
          boolean bool1 = ((com.truecaller.utils.l)localObject2).a((String[])localObject4);
          if (bool1)
          {
            localObject3 = n;
            a((Entity[])localObject3);
          }
          else
          {
            localObject2 = (be)b;
            localObject4 = "android.permission.READ_EXTERNAL_STORAGE";
            bool1 = ((be)localObject2).k((String)localObject4);
            if (bool1)
            {
              localObject3 = (be)b;
              ((be)localObject3).E();
            }
            else
            {
              localObject3 = n;
              ar = ((Entity[])localObject3);
              localObject3 = (be)b;
              ((be)localObject3).F();
            }
          }
          break;
        case 2131361857: 
          localObject3 = b;
          if (localObject3 != null)
          {
            localObject3 = an.j();
            i2 = an.e();
            localObject2 = new Long[i2];
            localObject3 = org.c.a.a.a.a.a((Long[])((Collection)localObject3).toArray((Object[])localObject2));
            localObject2 = (be)b;
            localObject4 = an.k();
            boolean bool5 = c((Message[])localObject4);
            ((be)localObject2).a(bool5, (long[])localObject3);
          }
          break;
        case 2131361853: 
          localObject3 = an.i();
          localObject2 = o;
          localObject4 = c.a();
          localObject3 = ((Message)localObject3).j();
          ((al)localObject2).a((String)localObject4, (String)localObject3);
          localObject3 = (be)b;
          i2 = 2131887203;
          ((be)localObject3).m(i2);
          localObject3 = "copy";
          localObject2 = "actionbar";
          a((String)localObject3, (String)localObject2);
        }
        ((be)b).v();
        return i3;
      }
    }
    return false;
  }
  
  public final void c()
  {
    Participant[] arrayOfParticipant = d;
    if (arrayOfParticipant != null)
    {
      boolean bool = g.c(arrayOfParticipant);
      if (bool) {
        H();
      }
    }
  }
  
  public final void c(Message paramMessage)
  {
    Message[] arrayOfMessage = new Message[1];
    arrayOfMessage[0] = paramMessage;
    b(arrayOfMessage);
  }
  
  public final void c(String paramString)
  {
    Object localObject = b;
    if (localObject != null)
    {
      ((be)b).j(paramString);
      paramString = "phoneNumber";
      localObject = null;
      a(paramString, null);
    }
  }
  
  public final boolean c(int paramInt)
  {
    Object localObject1 = b;
    Object localObject2 = null;
    if (localObject1 == null) {
      return false;
    }
    C();
    int i1 = 16908332;
    int i2 = 1;
    Object localObject3;
    if (paramInt == i1)
    {
      localObject3 = (be)b;
      ((be)localObject3).h();
    }
    else
    {
      i1 = 2131361844;
      if (paramInt == i1)
      {
        localObject3 = d;
        if (localObject3 != null)
        {
          localObject3 = localObject3[0];
          localObject1 = aA;
          localObject2 = new com/truecaller/messaging/conversation/-$$Lambda$bc$RxOpeAYIlMM8HYDuuMg7JIZIkZU;
          ((-..Lambda.bc.RxOpeAYIlMM8HYDuuMg7JIZIkZU)localObject2).<init>(this, (Participant)localObject3);
          ((ai)localObject1).a((Participant)localObject3, (com.truecaller.voip.e)localObject2);
        }
      }
      else
      {
        i1 = 2131361857;
        long l1;
        if (paramInt == i1)
        {
          localObject3 = c;
          if (localObject3 != null)
          {
            localObject3 = A;
            if (localObject3 != null) {
              ((com.truecaller.androidactors.a)localObject3).a();
            }
            localObject3 = (com.truecaller.messaging.data.o)j.a();
            l1 = c.a;
            localObject1 = Collections.singletonList(Long.valueOf(l1));
            localObject3 = ((com.truecaller.messaging.data.o)localObject3).a((List)localObject1);
            localObject1 = h;
            localObject2 = new com/truecaller/messaging/conversation/-$$Lambda$bc$bNFiFIyb8odpEzRnZrFhHoE0avE;
            ((-..Lambda.bc.bNFiFIyb8odpEzRnZrFhHoE0avE)localObject2).<init>(this);
            localObject3 = ((com.truecaller.androidactors.w)localObject3).a((i)localObject1, (ac)localObject2);
            A = ((com.truecaller.androidactors.a)localObject3);
          }
        }
        else
        {
          i1 = 2131361937;
          if (paramInt == i1)
          {
            localObject3 = K;
            ((m)localObject3).z();
          }
          else
          {
            i1 = 2131361859;
            if (paramInt == i1)
            {
              localObject3 = c;
              if (localObject3 != null)
              {
                localObject3 = A;
                if (localObject3 != null) {
                  ((com.truecaller.androidactors.a)localObject3).a();
                }
                localObject3 = (com.truecaller.messaging.data.o)j.a();
                l1 = c.a;
                localObject1 = Collections.singletonList(Long.valueOf(l1));
                localObject3 = ((com.truecaller.messaging.data.o)localObject3).a((List)localObject1);
                localObject1 = h;
                localObject2 = new com/truecaller/messaging/conversation/-$$Lambda$bc$YLlD4X1Ls3RLBU69cLKtuvco08U;
                ((-..Lambda.bc.YLlD4X1Ls3RLBU69cLKtuvco08U)localObject2).<init>(this);
                localObject3 = ((com.truecaller.androidactors.w)localObject3).a((i)localObject1, (ac)localObject2);
                A = ((com.truecaller.androidactors.a)localObject3);
              }
            }
            else
            {
              i1 = 2131361899;
              if (paramInt == i1)
              {
                localObject3 = c;
                if (localObject3 != null)
                {
                  localObject3 = x;
                  if (localObject3 != null)
                  {
                    localObject3 = (be)b;
                    localObject1 = org.c.a.a.a.k.n(c.x.b);
                    ((be)localObject3).o((String)localObject1);
                  }
                }
              }
              else
              {
                i1 = 2131361915;
                if (paramInt == i1)
                {
                  h(i2);
                }
                else
                {
                  i1 = 2131361942;
                  if (paramInt == i1) {
                    h(0);
                  } else {
                    a(paramInt);
                  }
                }
              }
            }
          }
        }
      }
    }
    return i2;
  }
  
  public final void d(int paramInt)
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (be)b;
      ((be)localObject).g(paramInt);
    }
  }
  
  public final void d(Message paramMessage)
  {
    Object localObject = z;
    if (localObject != null) {
      ((com.truecaller.androidactors.a)localObject).a();
    }
    boolean bool = paramMessage.e();
    if (bool)
    {
      localObject = k;
      i locali = h;
      -..Lambda.bc._-cGB1N8ZwGuz58U7TeM5a3v1KM local_-cGB1N8ZwGuz58U7TeM5a3v1KM = new com/truecaller/messaging/conversation/-$$Lambda$bc$_-cGB1N8ZwGuz58U7TeM5a3v1KM;
      local_-cGB1N8ZwGuz58U7TeM5a3v1KM.<init>(this);
      paramMessage = ((com.truecaller.messaging.transport.m)localObject).a(paramMessage, locali, local_-cGB1N8ZwGuz58U7TeM5a3v1KM);
      z = paramMessage;
    }
  }
  
  public final void e()
  {
    b();
  }
  
  final void e(int paramInt)
  {
    Message localMessage = (Message)am.a(paramInt);
    h(localMessage);
  }
  
  public final void e(Message paramMessage)
  {
    com.truecaller.messaging.transport.m localm = k;
    boolean bool = localm.d(paramMessage);
    if (bool)
    {
      paramMessage = b;
      if (paramMessage != null) {
        ((be)b).e();
      }
    }
    else
    {
      paramMessage = (com.truecaller.messaging.notifications.a)v.a();
      paramMessage.d();
    }
  }
  
  public final void f()
  {
    P();
  }
  
  public final void f(int paramInt)
  {
    Integer localInteger1 = aC;
    if (localInteger1 != null)
    {
      double d1 = paramInt;
      double d2 = localInteger1.intValue();
      double d3 = 0.8D;
      Double.isNaN(d2);
      d2 *= d3;
      paramInt = d1 < d2;
      if (paramInt >= 0)
      {
        paramInt = aC.intValue() * 2;
        Integer localInteger2 = Integer.valueOf(paramInt);
        aC = localInteger2;
        G();
      }
    }
  }
  
  public final void f(Message paramMessage)
  {
    Object localObject = H.F();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    if (!bool) {
      return;
    }
    localObject = (as)U.a();
    long l1 = a;
    paramMessage = ((as)localObject).e(l1);
    localObject = h;
    -..Lambda.bc.DE0n9iNfVurvCpxsZ3L87vhiRW8 localDE0n9iNfVurvCpxsZ3L87vhiRW8 = new com/truecaller/messaging/conversation/-$$Lambda$bc$DE0n9iNfVurvCpxsZ3L87vhiRW8;
    localDE0n9iNfVurvCpxsZ3L87vhiRW8.<init>(this);
    paramMessage.a((i)localObject, localDE0n9iNfVurvCpxsZ3L87vhiRW8);
  }
  
  public final void g()
  {
    Q();
  }
  
  public final void h()
  {
    Object localObject1 = o;
    boolean bool = ((al)localObject1).a();
    if (!bool)
    {
      localObject1 = o;
      bool = ((al)localObject1).b();
      if (bool)
      {
        localObject1 = b;
        if (localObject1 != null)
        {
          ((be)b).i();
          localObject1 = (be)b;
          ((be)localObject1).h();
        }
        return;
      }
    }
    localObject1 = N;
    bool = ((com.truecaller.tcpermissions.l)localObject1).f();
    if (!bool)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        localObject1 = (be)b;
        localObject2 = "messages";
        ((be)localObject1).g((String)localObject2);
        localObject1 = (be)b;
        ((be)localObject1).h();
      }
      return;
    }
    localObject1 = o;
    Object localObject2 = at;
    String[] arrayOfString = { "com.truecaller.messaging.transport.im.ACTION_IM_USED_ADDED" };
    ((al)localObject1).a((BroadcastReceiver)localObject2, arrayOfString);
    localObject1 = x;
    if (localObject1 == null) {
      return;
    }
    localObject1 = b;
    if (localObject1 == null) {
      return;
    }
    localObject1 = (com.truecaller.messaging.notifications.a)v.a();
    long l1 = x.b.a;
    ((com.truecaller.messaging.notifications.a)localObject1).a(l1);
    localObject1 = (as)U.a();
    l1 = x.b.a;
    ((as)localObject1).b(l1);
    localObject1 = (com.truecaller.messaging.notifications.a)v.a();
    localObject2 = x.b;
    l1 = a;
    ((com.truecaller.messaging.notifications.a)localObject1).c(l1);
    localObject1 = d;
    if (localObject1 != null)
    {
      bool = g.c((Participant[])localObject1);
      if (bool)
      {
        H();
        break label308;
      }
    }
    b();
    label308:
    K.J();
  }
  
  final void i()
  {
    I();
    Object localObject1 = o;
    Object localObject2 = at;
    ((al)localObject1).a((BroadcastReceiver)localObject2);
    localObject1 = x;
    if (localObject1 != null)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        localObject1 = (com.truecaller.messaging.notifications.a)v.a();
        long l1 = x.b.a;
        ((com.truecaller.messaging.notifications.a)localObject1).b(l1);
        localObject1 = (as)U.a();
        localObject2 = x.b;
        l1 = a;
        ((as)localObject1).c(l1);
      }
    }
    localObject1 = aD;
    boolean bool = ((com.truecaller.messaging.conversation.voice_notes.f)localObject1).b();
    if (bool)
    {
      localObject1 = "Interrupt";
      e((String)localObject1);
    }
    aD.a();
  }
  
  final void j()
  {
    l.b();
  }
  
  final void k()
  {
    c(false);
    l.c();
  }
  
  final void l()
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = d;
      be localbe;
      if (localObject != null)
      {
        boolean bool1 = g.c((Participant[])localObject);
        if (bool1)
        {
          localObject = c;
          if (localObject == null)
          {
            bool1 = false;
            localObject = null;
          }
          else
          {
            localObject = x;
          }
          if (localObject != null)
          {
            boolean bool2 = com.truecaller.messaging.i.a.a((ImGroupInfo)localObject);
            if (!bool2)
            {
              localbe = (be)b;
              localbe.a((ImGroupInfo)localObject);
            }
          }
          return;
        }
      }
      localObject = c;
      if (localObject != null)
      {
        localObject = (be)b;
        long l1 = c.a;
        int i2 = g;
        ((be)localObject).a(l1, i2);
        return;
      }
      localObject = d;
      if (localObject != null)
      {
        int i1 = localObject.length;
        if (i1 > 0)
        {
          i1 = 0;
          localObject = localObject[0];
          localbe = (be)b;
          String str1 = f;
          String str2 = e;
          String str3 = m;
          localObject = h;
          localbe.a(str1, str2, str3, (String)localObject);
        }
      }
    }
  }
  
  final void m()
  {
    F.a(1);
    int i1 = 0;
    Object localObject1 = null;
    a("createSMSShortcut", null);
    Object localObject2 = b;
    if (localObject2 != null)
    {
      localObject2 = p;
      int i2 = ((com.truecaller.utils.d)localObject2).h();
      i1 = 26;
      if (i2 < i1)
      {
        localObject2 = (be)b;
        localObject1 = n;
        int i3 = 2131886459;
        Object[] arrayOfObject = new Object[0];
        localObject1 = ((com.truecaller.utils.o)localObject1).a(i3, arrayOfObject);
        ((be)localObject2).l((String)localObject1);
      }
    }
  }
  
  final void n()
  {
    a("smsShortcutDismissed", null);
  }
  
  final void o()
  {
    Object localObject1 = d;
    if (localObject1 != null)
    {
      int i1 = localObject1.length;
      if (i1 != 0)
      {
        localObject1 = b((Participant[])localObject1);
        Object localObject2 = s.a();
        Object localObject3 = localObject2;
        localObject3 = (s)localObject2;
        localObject2 = ((List)localObject1).get(0);
        Object localObject4 = localObject2;
        localObject4 = (List)localObject2;
        localObject2 = ((List)localObject1).get(1);
        Object localObject5 = localObject2;
        localObject5 = (List)localObject2;
        localObject1 = ((List)localObject1).get(2);
        Object localObject6 = localObject1;
        localObject6 = (List)localObject1;
        String str = ad;
        localObject1 = ((s)localObject3).a((List)localObject4, (List)localObject5, (List)localObject6, str, "conversation", true);
        localObject2 = h;
        localObject3 = new com/truecaller/messaging/conversation/-$$Lambda$bc$TI02W66LVJFYmGm0W3UPidaGqBA;
        ((-..Lambda.bc.TI02W66LVJFYmGm0W3UPidaGqBA)localObject3).<init>(this);
        ((com.truecaller.androidactors.w)localObject1).a((i)localObject2, (ac)localObject3);
        return;
      }
    }
  }
  
  final void p()
  {
    Object localObject = b;
    if (localObject != null)
    {
      an.a(false);
      an.h();
      ((be)b).e();
      localObject = (be)b;
      ((be)localObject).D();
    }
  }
  
  final void q()
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (be)b;
      int i1 = 2131886583;
      ((be)localObject).m(i1);
    }
  }
  
  public final void r()
  {
    S();
  }
  
  final void s()
  {
    K.y();
  }
  
  public final void t()
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (be)b;
      ((be)localObject).T();
    }
  }
  
  public final void u()
  {
    Object localObject = b;
    if (localObject != null)
    {
      ((be)b).l(0);
      localObject = (be)b;
      ((be)localObject).n(false);
    }
  }
  
  final void v()
  {
    bi localbi = an;
    int i1 = localbi.e();
    int i2 = 1;
    if (i1 == i2)
    {
      localbi = an;
      boolean bool = localbi.c();
      if (!bool) {
        x();
      }
    }
  }
  
  final void w()
  {
    Object localObject1 = b;
    if (localObject1 != null)
    {
      localObject1 = c;
      if (localObject1 != null)
      {
        localObject1 = x;
        if (localObject1 != null)
        {
          ((be)b).Z();
          localObject1 = (com.truecaller.messaging.transport.im.a.a)aE.a();
          Object localObject2 = c.x.a;
          localObject1 = ((com.truecaller.messaging.transport.im.a.a)localObject1).a((String)localObject2, false);
          localObject2 = h;
          -..Lambda.bc.4Aiy5j6vRflF_4-a42gQPUrq6tQ local4Aiy5j6vRflF_4-a42gQPUrq6tQ = new com/truecaller/messaging/conversation/-$$Lambda$bc$4Aiy5j6vRflF_4-a42gQPUrq6tQ;
          local4Aiy5j6vRflF_4-a42gQPUrq6tQ.<init>(this);
          ((com.truecaller.androidactors.w)localObject1).a((i)localObject2, local4Aiy5j6vRflF_4-a42gQPUrq6tQ);
          return;
        }
      }
    }
  }
  
  public final void x()
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (be)b;
      ((be)localObject).v();
    }
  }
  
  public final void y()
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = (be)b;
      ((be)localObject).T();
    }
  }
  
  public final void y_()
  {
    I();
    w.c();
    am.a(null);
    Object localObject = t;
    if (localObject != null)
    {
      ((com.truecaller.androidactors.a)localObject).a();
      t = null;
    }
    localObject = y;
    if (localObject != null)
    {
      ((com.truecaller.androidactors.a)localObject).a();
      y = null;
    }
    localObject = I;
    if (localObject != null)
    {
      ((com.truecaller.androidactors.a)localObject).a();
      I = null;
    }
    localObject = aq;
    if (localObject != null)
    {
      ((com.truecaller.androidactors.a)localObject).a();
      aq = null;
    }
    localObject = A;
    if (localObject != null)
    {
      ((com.truecaller.androidactors.a)localObject).a();
      A = null;
    }
    localObject = B;
    if (localObject != null)
    {
      ((com.truecaller.androidactors.a)localObject).a();
      B = null;
    }
    localObject = H.c();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool)
    {
      localObject = H.b();
      bool = ((com.truecaller.featuretoggles.b)localObject).a();
      if (bool)
      {
        localObject = S;
        ((com.truecaller.messaging.conversation.a.b.k)localObject).a(null);
      }
    }
    super.y_();
  }
  
  public final void z()
  {
    Object localObject = H.F();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    if (!bool) {
      return;
    }
    localObject = b;
    if (localObject != null)
    {
      localObject = (be)b;
      ((be)localObject).D();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.bc
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */