package com.truecaller.messaging.conversation;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.Layout;
import android.text.SpannableString;
import android.text.method.MovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import c.a.ae;
import c.g.b.k;
import c.k.i;
import com.bumptech.glide.load.d.a.e;
import com.bumptech.glide.load.h;
import com.truecaller.R.id;
import com.truecaller.android.truemoji.Emoji;
import com.truecaller.android.truemoji.EmojiTextView;
import com.truecaller.bg;
import com.truecaller.bi;
import com.truecaller.bj;
import com.truecaller.common.h.ah;
import com.truecaller.common.h.am;
import com.truecaller.common.h.ap;
import com.truecaller.messaging.conversation.a.b.n;
import com.truecaller.messaging.conversation.a.b.o;
import com.truecaller.messaging.conversation.b.a.a;
import com.truecaller.messaging.conversation.b.a.c;
import com.truecaller.messaging.conversation.b.a.d;
import com.truecaller.messaging.conversation.b.c;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Reaction;
import com.truecaller.messaging.data.types.ReplySnippet;
import com.truecaller.ui.components.CyclicProgressBar;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.ui.view.TintedTextView;
import com.truecaller.util.at;
import com.truecaller.utils.extensions.t;
import com.truecaller.utils.ui.LinkClickMovementMethod;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class cb
  extends com.avito.konveyor.a.b
  implements ca
{
  boolean a;
  private final EmojiTextView b;
  private final ViewGroup c;
  private final View d;
  private final EmojiTextView e;
  private final View f;
  private final View g;
  private final TextView h;
  private final ViewGroup i;
  private final FrameLayout j;
  private final TextView k;
  private final LinearLayout l;
  private String m;
  private boolean n;
  private final int o;
  private final com.truecaller.messaging.conversation.a.b.g p;
  
  public cb(View paramView, com.truecaller.messaging.conversation.a.b.g paramg)
  {
    super(paramView);
    p = paramg;
    paramg = (EmojiTextView)paramView.findViewById(2131362596);
    b = paramg;
    paramg = (ViewGroup)paramView.findViewById(2131362592);
    c = paramg;
    paramg = paramView.findViewById(2131362593);
    d = paramg;
    paramg = (EmojiTextView)paramView.findViewById(2131364298);
    e = paramg;
    paramg = paramView.findViewById(2131362334);
    f = paramg;
    paramg = paramView.findViewById(2131363150);
    g = paramg;
    paramg = (TextView)paramView.findViewById(2131363756);
    h = paramg;
    paramg = (ViewGroup)paramView.findViewById(2131365507);
    i = paramg;
    paramg = (FrameLayout)paramView.findViewById(2131361948);
    j = paramg;
    paramg = (TextView)paramView.findViewById(2131364880);
    k = paramg;
    paramg = (LinearLayout)paramView.findViewById(2131364068);
    l = paramg;
    a = true;
    int i1 = android.support.v4.graphics.a.b(com.truecaller.utils.ui.b.a(paramView.getContext(), 2130968843), 179);
    o = i1;
  }
  
  private final void a(View paramView)
  {
    ViewGroup localViewGroup = i;
    if (localViewGroup != null)
    {
      localViewGroup.addView(paramView);
      return;
    }
  }
  
  private final void a(View paramView, Uri paramUri, int paramInt1, int paramInt2, int paramInt3, int paramInt4, d paramd, int paramInt5)
  {
    Object localObject1 = paramd;
    int i1 = 2131363323;
    Object localObject2 = paramView;
    ImageView localImageView = (ImageView)paramView.findViewById(i1);
    localObject2 = paramView.getResources();
    int i2 = ((Resources)localObject2).getColor(2131100191);
    k.a(localImageView, "imageView");
    boolean bool1 = false;
    Object localObject3 = null;
    localImageView.setVisibility(0);
    Object localObject4 = localImageView.getLayoutParams();
    if (localObject4 != null)
    {
      localObject4 = (RelativeLayout.LayoutParams)localObject4;
      int i4 = paramInt5;
      ((RelativeLayout.LayoutParams)localObject4).setMargins(paramInt5, paramInt5, paramInt5, paramInt5);
      localObject4 = (ViewGroup.LayoutParams)localObject4;
      localImageView.setLayoutParams((ViewGroup.LayoutParams)localObject4);
      localObject4 = this;
      Object localObject5 = k;
      int i6 = g;
      ((TextView)localObject5).setTextColor(i6);
      Context localContext = localImageView.getContext();
      boolean bool2 = v;
      if (bool2)
      {
        localObject5 = new com/truecaller/common/h/ap;
        ((ap)localObject5).<init>(i2);
        localObject5 = (com.truecaller.common.h.b)localObject5;
      }
      else
      {
        localObject2 = new com/truecaller/common/h/m;
        ((com.truecaller.common.h.m)localObject2).<init>();
        localObject5 = localObject2;
        localObject5 = (com.truecaller.common.h.b)localObject2;
      }
      i2 = 2;
      localObject2 = new e[i2];
      Object localObject6 = new com/bumptech/glide/load/d/a/g;
      ((com.bumptech.glide.load.d.a.g)localObject6).<init>();
      localObject6 = (e)localObject6;
      localObject2[0] = localObject6;
      localObject5 = (e)localObject5;
      localObject2[1] = localObject5;
      localObject2 = c.a.m.c((Object[])localObject2);
      bool1 = j;
      if (bool1)
      {
        localObject3 = e;
        if (localObject3 != null)
        {
          localObject3 = new com/truecaller/common/h/p;
          k.a(localContext, "context");
          localObject5 = e;
          i6 = paramInt3;
          ((com.truecaller.common.h.p)localObject3).<init>(localContext, paramInt3, paramInt4, (String)localObject5);
          ((List)localObject2).add(localObject3);
        }
      }
      localObject3 = new com/bumptech/glide/load/d/a/u;
      int i5 = at.a(localContext);
      ((com.bumptech.glide.load.d.a.u)localObject3).<init>(i5);
      ((List)localObject2).add(localObject3);
      localObject3 = bg.a(localContext);
      localObject5 = paramUri;
      localObject3 = ((bj)localObject3).b(paramUri);
      localObject5 = new com/truecaller/messaging/conversation/cd;
      k.a(localContext, "context");
      int i7 = p;
      int i8 = g;
      ((cd)localObject5).<init>(localContext, i7, i8, paramInt1, paramInt2);
      localObject5 = (Drawable)localObject5;
      localObject1 = ((bi)localObject3).b((Drawable)localObject5);
      int i3 = paramInt1;
      i5 = paramInt2;
      localObject1 = ((bi)localObject1).b(paramInt1, paramInt2);
      localObject3 = new com/bumptech/glide/load/h;
      localObject2 = (Collection)localObject2;
      ((h)localObject3).<init>((Collection)localObject2);
      localObject3 = (com.bumptech.glide.load.m)localObject3;
      ((bi)localObject1).b((com.bumptech.glide.load.m)localObject3).a(localImageView);
      return;
    }
    localObject4 = this;
    localObject1 = new c/u;
    ((c.u)localObject1).<init>("null cannot be cast to non-null type android.widget.RelativeLayout.LayoutParams");
    throw ((Throwable)localObject1);
  }
  
  private final void a(View paramView, c.g.a.b paramb)
  {
    Object localObject = new com/truecaller/messaging/conversation/cb$s;
    ((cb.s)localObject).<init>(this, paramb);
    localObject = (View.OnClickListener)localObject;
    paramView.setOnClickListener((View.OnClickListener)localObject);
  }
  
  private static void a(View paramView, d paramd)
  {
    int i1 = p;
    int i2 = R.id.content_bubble;
    Object localObject = (RelativeLayout)paramView.findViewById(i2);
    if (localObject != null)
    {
      localObject = ((RelativeLayout)localObject).getBackground();
      if (localObject != null)
      {
        PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
        ((Drawable)localObject).setColorFilter(i1, localMode);
      }
    }
    i2 = R.id.content_bubble_shadow;
    paramView = (FrameLayout)paramView.findViewById(i2);
    if (paramView != null)
    {
      if (i1 == 0)
      {
        i1 = 4;
      }
      else
      {
        i1 = 0;
        paramd = null;
      }
      paramView.setVisibility(i1);
      return;
    }
  }
  
  private final void a(View paramView, d paramd, int paramInt1, int paramInt2)
  {
    int i1 = p;
    int i2 = R.id.content_bubble;
    Object localObject1 = (RelativeLayout)paramView.findViewById(i2);
    Object localObject2 = "view.content_bubble";
    k.a(localObject1, (String)localObject2);
    localObject1 = ((RelativeLayout)localObject1).getBackground();
    if (localObject1 != null)
    {
      localObject2 = PorterDuff.Mode.SRC_IN;
      ((Drawable)localObject1).setColorFilter(i1, (PorterDuff.Mode)localObject2);
    }
    i1 = 2131364884;
    localObject1 = paramView.findViewById(i1);
    if (localObject1 != null)
    {
      localObject1 = (TextView)localObject1;
      localObject2 = b;
      int i4 = title;
      ((TextView)localObject1).setText(i4);
      boolean bool1 = i;
      if (bool1)
      {
        localObject3 = paramView.findViewById(i1);
        if (localObject3 != null)
        {
          localObject3 = (TextView)localObject3;
          ((TextView)localObject3).setTextColor(paramInt2);
        }
        else
        {
          paramView = new c/u;
          paramView.<init>("null cannot be cast to non-null type android.widget.TextView");
          throw paramView;
        }
      }
      paramInt2 = 2131364298;
      Object localObject4 = (TextView)paramView.findViewById(paramInt2);
      i1 = 0;
      Object localObject3 = null;
      int i3 = 8;
      if (localObject4 != null)
      {
        localObject2 = (CharSequence)e;
        ((TextView)localObject4).setText((CharSequence)localObject2);
        boolean bool2 = j;
        if (bool2)
        {
          bool2 = false;
          localObject2 = null;
        }
        else
        {
          i5 = 8;
        }
        ((TextView)localObject4).setVisibility(i5);
      }
      localObject4 = (TintedImageView)paramView.findViewById(2131363301);
      int i5 = b.icon;
      ((TintedImageView)localObject4).setImageResource(i5);
      i5 = g;
      ((TintedImageView)localObject4).setTint(i5);
      localObject4 = new com/truecaller/messaging/conversation/cb$m;
      ((cb.m)localObject4).<init>(this, paramd);
      localObject4 = (View.OnClickListener)localObject4;
      paramView.setOnClickListener((View.OnClickListener)localObject4);
      localObject4 = new com/truecaller/messaging/conversation/cb$n;
      ((cb.n)localObject4).<init>(this);
      localObject4 = (c.g.a.b)localObject4;
      b(paramView, (c.g.a.b)localObject4);
      localObject4 = (TextView)paramView.findViewById(2131364880);
      ((TextView)localObject4).setTextColor(paramInt1);
      localObject2 = "timestampView";
      k.a(localObject4, (String)localObject2);
      a((TextView)localObject4, paramd);
      paramInt2 = 2131364528;
      localObject4 = (TextView)paramView.findViewById(paramInt2);
      if (localObject4 != null)
      {
        localObject2 = (CharSequence)x;
        boolean bool3 = am.b((CharSequence)localObject2);
        int i6;
        if (bool3)
        {
          i6 = 8;
        }
        else
        {
          i6 = 0;
          localObject2 = null;
        }
        ((TextView)localObject4).setVisibility(i6);
        localObject2 = (CharSequence)x;
        ((TextView)localObject4).setText((CharSequence)localObject2);
        ((TextView)localObject4).setTextColor(paramInt1);
      }
      paramInt2 = 2131365385;
      localObject4 = (TextView)paramView.findViewById(paramInt2);
      if (localObject4 != null)
      {
        localObject2 = (CharSequence)w;
        boolean bool4 = am.b((CharSequence)localObject2);
        if (bool4) {
          i1 = 8;
        }
        ((TextView)localObject4).setVisibility(i1);
        paramd = (CharSequence)w;
        ((TextView)localObject4).setText(paramd);
        ((TextView)localObject4).setTextColor(paramInt1);
      }
      a(paramView);
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type android.widget.TextView");
    throw paramView;
  }
  
  private static void a(View paramView, boolean paramBoolean)
  {
    if (paramView != null)
    {
      if (paramBoolean) {
        paramBoolean = false;
      } else {
        paramBoolean = true;
      }
      paramView.setVisibility(paramBoolean);
      return;
    }
  }
  
  private final void a(LinearLayout paramLinearLayout, int paramInt)
  {
    paramLinearLayout.removeAllViews();
    Object localObject1 = paramLinearLayout.getContext();
    k.a(localObject1, "reactionsView.context");
    localObject1 = LayoutInflater.from((Context)localObject1);
    Object localObject2 = paramLinearLayout;
    localObject2 = (ViewGroup)paramLinearLayout;
    localObject1 = ((LayoutInflater)localObject1).inflate(2131558875, (ViewGroup)localObject2, false);
    paramLinearLayout.addView((View)localObject1);
    localObject1 = new com/truecaller/messaging/conversation/cb$j;
    ((cb.j)localObject1).<init>(this, paramInt);
    localObject1 = (View.OnClickListener)localObject1;
    paramLinearLayout.setOnClickListener((View.OnClickListener)localObject1);
  }
  
  private final void a(LinearLayout paramLinearLayout, d paramd)
  {
    boolean bool1 = D;
    int i2 = 0;
    if (bool1)
    {
      bool1 = B;
      if (!bool1)
      {
        bool1 = C;
        if (bool1) {
          break label83;
        }
        localObject = A;
        if (localObject != null)
        {
          i1 = localObject.length;
          if (i1 == 0)
          {
            i1 = 1;
          }
          else
          {
            i1 = 0;
            localObject = null;
          }
          if (i1 == 0)
          {
            i1 = 0;
            localObject = null;
            break label76;
          }
        }
        int i1 = 1;
        label76:
        if (i1 == 0) {
          break label83;
        }
      }
    }
    i2 = 1;
    label83:
    if (i2 != 0)
    {
      paramLinearLayout.removeAllViews();
      return;
    }
    boolean bool2 = C;
    if (bool2)
    {
      int i3 = E;
      a(paramLinearLayout, i3);
      return;
    }
    Object localObject = c;
    paramd = A;
    a((Message)localObject, paramd, paramLinearLayout);
  }
  
  private static void a(TextView paramTextView, d paramd)
  {
    CharSequence localCharSequence = (CharSequence)h;
    paramTextView.setText(localCharSequence);
    int i1 = f;
    int i2 = o;
    android.support.v4.widget.m.a(paramTextView, i1, i2);
  }
  
  private final void a(Message paramMessage, Reaction[] paramArrayOfReaction, LinearLayout paramLinearLayout)
  {
    paramLinearLayout.removeAllViews();
    Object localObject1 = new com/truecaller/messaging/conversation/cb$k;
    ((cb.k)localObject1).<init>(this, paramMessage);
    localObject1 = (View.OnClickListener)localObject1;
    paramLinearLayout.setOnClickListener((View.OnClickListener)localObject1);
    paramMessage = paramLinearLayout.getContext();
    k.a(paramMessage, "reactionsView.context");
    localObject1 = paramMessage.getResources();
    int i1 = 2131166122;
    int i2 = ((Resources)localObject1).getDimensionPixelSize(i1) * -1;
    if (paramArrayOfReaction != null)
    {
      i1 = paramArrayOfReaction.length;
      int i3 = 0;
      int i4 = 0;
      Object localObject2 = null;
      while (i3 < i1)
      {
        Object localObject3 = paramArrayOfReaction[i3];
        int i5 = i4 + 1;
        int i6 = 2;
        if (i4 < i6)
        {
          localObject2 = d;
          if (localObject2 != null)
          {
            localObject3 = com.truecaller.android.truemoji.f.b;
            localObject3 = com.truecaller.android.truemoji.f.c();
            localObject2 = (CharSequence)localObject2;
            localObject2 = ((com.truecaller.android.truemoji.f)localObject3).b((CharSequence)localObject2);
            if (localObject2 != null)
            {
              i4 = ((Emoji)localObject2).a();
            }
            else
            {
              i4 = 0;
              localObject2 = null;
            }
            localObject3 = LayoutInflater.from(paramMessage);
            i6 = 2131558879;
            Object localObject4 = paramLinearLayout;
            localObject4 = (ViewGroup)paramLinearLayout;
            localObject3 = ((LayoutInflater)localObject3).inflate(i6, (ViewGroup)localObject4, false);
            if (localObject3 != null)
            {
              localObject3 = (ImageView)localObject3;
              ((ImageView)localObject3).setImageResource(i4);
              i4 = paramLinearLayout.getChildCount();
              if (i4 > 0)
              {
                localObject2 = ((ImageView)localObject3).getLayoutParams();
                if (localObject2 != null)
                {
                  localObject2 = (LinearLayout.LayoutParams)localObject2;
                  ((LinearLayout.LayoutParams)localObject2).setMarginStart(i2);
                }
                else
                {
                  paramMessage = new c/u;
                  paramMessage.<init>("null cannot be cast to non-null type android.widget.LinearLayout.LayoutParams");
                  throw paramMessage;
                }
              }
              localObject3 = (View)localObject3;
              paramLinearLayout.addView((View)localObject3);
            }
            else
            {
              paramMessage = new c/u;
              paramMessage.<init>("null cannot be cast to non-null type android.widget.ImageView");
              throw paramMessage;
            }
          }
          i3 += 1;
          i4 = i5;
        }
        else
        {
          Object localObject5 = LayoutInflater.from(paramMessage);
          i3 = 2131558880;
          localObject2 = paramLinearLayout;
          localObject2 = (ViewGroup)paramLinearLayout;
          localObject5 = ((LayoutInflater)localObject5).inflate(i3, (ViewGroup)localObject2, false);
          if (localObject5 != null)
          {
            localObject5 = (TextView)localObject5;
            i3 = 2131888599;
            i4 = 1;
            localObject2 = new Object[i4];
            int i7 = paramArrayOfReaction.length - i6;
            paramArrayOfReaction = Integer.valueOf(i7);
            localObject2[0] = paramArrayOfReaction;
            paramMessage = (CharSequence)paramMessage.getString(i3, (Object[])localObject2);
            ((TextView)localObject5).setText(paramMessage);
            paramMessage = ((TextView)localObject5).getLayoutParams();
            if (paramMessage != null)
            {
              ((LinearLayout.LayoutParams)paramMessage).setMarginStart(i2);
              localObject5 = (View)localObject5;
              paramLinearLayout.addView((View)localObject5);
              return;
            }
            paramMessage = new c/u;
            paramMessage.<init>("null cannot be cast to non-null type android.widget.LinearLayout.LayoutParams");
            throw paramMessage;
          }
          paramMessage = new c/u;
          paramMessage.<init>("null cannot be cast to non-null type android.widget.TextView");
          throw paramMessage;
        }
      }
      return;
    }
  }
  
  private static void a(boolean paramBoolean, View paramView)
  {
    int i1 = R.id.voiceClipSeekBar;
    a((ProgressBar)paramView.findViewById(i1), paramBoolean);
    i1 = R.id.duration_indicator;
    a((TintedTextView)paramView.findViewById(i1), paramBoolean);
    i1 = R.id.voiceClipTitle;
    TextView localTextView = (TextView)paramView.findViewById(i1);
    boolean bool = paramBoolean ^ true;
    a(localTextView, bool);
    i1 = R.id.status_text;
    paramView = (TintedTextView)paramView.findViewById(i1);
    paramBoolean ^= true;
    a(paramView, paramBoolean);
  }
  
  private final int b(d paramd)
  {
    int i1 = p;
    if (i1 == 0) {
      return 0;
    }
    paramd = itemView;
    k.a(paramd, "itemView");
    paramd = paramd.getContext();
    k.a(paramd, "itemView.context");
    return paramd.getResources().getDimensionPixelSize(2131166204);
  }
  
  private final void b(View paramView, c.g.a.b paramb)
  {
    Object localObject = new com/truecaller/messaging/conversation/cb$u;
    ((cb.u)localObject).<init>(this, paramb);
    localObject = (View.OnLongClickListener)localObject;
    paramView.setOnLongClickListener((View.OnLongClickListener)localObject);
  }
  
  private static void b(View paramView, d paramd)
  {
    paramView = (TextView)paramView.findViewById(2131362922);
    Object localObject = paramView;
    a((View)paramView, true);
    int i1 = t;
    paramView.setTextColor(i1);
    k.a(paramView, "duration");
    localObject = (CharSequence)s;
    paramView.setText((CharSequence)localObject);
    int i2 = r;
    android.support.v4.widget.m.a(paramView, i2, 0);
  }
  
  private final void c(View paramView, d paramd)
  {
    int i1 = 2131361840;
    Object localObject1 = (ImageButton)paramView.findViewById(i1);
    boolean bool2 = m;
    int i5 = 0;
    if (bool2)
    {
      int i2 = 2131234121;
      ((ImageButton)localObject1).setImageResource(i2);
      paramView.setOnClickListener(null);
    }
    else
    {
      boolean bool3 = n;
      if (bool3)
      {
        bool3 = l;
        int i3;
        if (bool3) {
          i3 = 2131234463;
        } else {
          i3 = 2131234061;
        }
        ((ImageButton)localObject1).setImageResource(i3);
        localObject2 = new com/truecaller/messaging/conversation/cb$w;
        ((cb.w)localObject2).<init>(this, paramd);
        localObject2 = (View.OnClickListener)localObject2;
        ((ImageButton)localObject1).setOnClickListener((View.OnClickListener)localObject2);
        localObject2 = new com/truecaller/messaging/conversation/cb$x;
        ((cb.x)localObject2).<init>(this, paramd);
        localObject2 = (View.OnClickListener)localObject2;
        paramView.setOnClickListener((View.OnClickListener)localObject2);
      }
      else
      {
        boolean bool4 = y;
        int i4;
        if (bool4)
        {
          bool4 = z;
          if (bool4)
          {
            i4 = 2131233992;
          }
          else
          {
            i4 = 0;
            localObject2 = null;
          }
          ((ImageButton)localObject1).setImageResource(i4);
          localObject2 = new com/truecaller/messaging/conversation/cb$y;
          ((cb.y)localObject2).<init>(this, paramd);
          localObject2 = (View.OnClickListener)localObject2;
          ((ImageButton)localObject1).setOnClickListener((View.OnClickListener)localObject2);
          paramView.setOnClickListener(null);
        }
        else
        {
          i4 = 2131234386;
          ((ImageButton)localObject1).setImageResource(i4);
          localObject2 = new com/truecaller/messaging/conversation/cb$z;
          ((cb.z)localObject2).<init>(this, paramd);
          localObject2 = (View.OnClickListener)localObject2;
          ((ImageButton)localObject1).setOnClickListener((View.OnClickListener)localObject2);
          localObject2 = new com/truecaller/messaging/conversation/cb$aa;
          ((cb.aa)localObject2).<init>(this, paramd);
          localObject2 = (View.OnClickListener)localObject2;
          paramView.setOnClickListener((View.OnClickListener)localObject2);
        }
      }
    }
    Object localObject2 = (TextView)paramView.findViewById(2131364597);
    k.a(localObject2, "imageStatusText");
    CharSequence localCharSequence = (CharSequence)u;
    ((TextView)localObject2).setText(localCharSequence);
    boolean bool5 = k;
    int i6 = 4;
    if (!bool5)
    {
      localObject2 = "actionButton";
      k.a(localObject1, (String)localObject2);
      ((ImageButton)localObject1).setVisibility(0);
    }
    else
    {
      localObject2 = "actionButton";
      k.a(localObject1, (String)localObject2);
      ((ImageButton)localObject1).setVisibility(i6);
      ((ImageButton)localObject1).setOnClickListener(null);
    }
    paramView = (CyclicProgressBar)paramView.findViewById(2131363686);
    localObject1 = "progress";
    k.a(paramView, (String)localObject1);
    boolean bool1 = k;
    if (!bool1)
    {
      boolean bool6 = y;
      if (!bool6) {
        i5 = 4;
      }
    }
    paramView.setVisibility(i5);
  }
  
  private final void d(View paramView, d paramd)
  {
    int i1 = 2131363301;
    Object localObject1 = (TintedImageView)paramView.findViewById(i1);
    boolean bool2 = m;
    Object localObject2;
    if (bool2)
    {
      ((TintedImageView)localObject1).setImageResource(2131234121);
      bool2 = false;
      localObject2 = null;
      paramView.setOnClickListener(null);
    }
    else
    {
      bool2 = n;
      int i3;
      if (bool2)
      {
        bool2 = l;
        if (bool2) {
          i3 = 2131234463;
        } else {
          i3 = 2131234061;
        }
        ((TintedImageView)localObject1).setImageResource(i3);
        localObject2 = new com/truecaller/messaging/conversation/cb$o;
        ((cb.o)localObject2).<init>(this, paramd);
        localObject2 = (View.OnClickListener)localObject2;
        paramView.setOnClickListener((View.OnClickListener)localObject2);
      }
      else
      {
        i3 = b.icon;
        ((TintedImageView)localObject1).setImageResource(i3);
        localObject2 = new com/truecaller/messaging/conversation/cb$p;
        ((cb.p)localObject2).<init>(this, paramd);
        localObject2 = (View.OnClickListener)localObject2;
        paramView.setOnClickListener((View.OnClickListener)localObject2);
      }
    }
    localObject1 = (View)localObject1;
    boolean bool3 = k ^ true;
    a((View)localObject1, bool3);
    i1 = 2131362055;
    paramView = (CyclicProgressBar)paramView.findViewById(i1);
    if (paramView != null)
    {
      boolean bool1 = k;
      int i2;
      if (bool1)
      {
        bool1 = false;
        localObject1 = null;
      }
      else
      {
        i2 = 8;
      }
      paramView.setVisibility(i2);
      int i4 = g;
      paramView.setStrokeColor(i4);
    }
  }
  
  public final String a()
  {
    return m;
  }
  
  public final void a(int paramInt)
  {
    TextView localTextView = h;
    if (localTextView != null)
    {
      localTextView.setText(paramInt);
      return;
    }
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    Object localObject1 = b;
    if (localObject1 == null) {
      return;
    }
    RelativeLayout.LayoutParams localLayoutParams = new android/widget/RelativeLayout$LayoutParams;
    int i1 = -2;
    localLayoutParams.<init>(i1, i1);
    Object localObject2 = k;
    k.a(localObject2, "timestampSimIndicator");
    Object localObject3 = localLayoutParams;
    localObject3 = (ViewGroup.LayoutParams)localLayoutParams;
    ((TextView)localObject2).setLayoutParams((ViewGroup.LayoutParams)localObject3);
    i1 = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);
    int i2 = 0;
    int i3 = View.MeasureSpec.makeMeasureSpec(0, 0);
    ((EmojiTextView)localObject1).measure(i1, i3);
    localObject2 = ((EmojiTextView)localObject1).getLayout();
    Object localObject4 = ((EmojiTextView)localObject1).getLayout();
    String str1 = "contentText.layout";
    k.a(localObject4, str1);
    i3 = ((Layout)localObject4).getLineCount();
    int i4 = 1;
    i3 -= i4;
    float f1 = ((Layout)localObject2).getLineWidth(i3);
    float f2 = paramInt1 - f1;
    localObject2 = android.support.v4.e.a.a();
    localObject4 = ((EmojiTextView)localObject1).getText();
    boolean bool = ((android.support.v4.e.a)localObject2).a((CharSequence)localObject4);
    if (bool)
    {
      bool = com.truecaller.common.e.f.b();
      if (!bool) {}
    }
    else
    {
      localObject2 = android.support.v4.e.a.a();
      localObject4 = ((EmojiTextView)localObject1).getText();
      bool = ((android.support.v4.e.a)localObject2).a((CharSequence)localObject4);
      if (bool) {
        break label224;
      }
      bool = com.truecaller.common.e.f.b();
      if (!bool) {
        break label224;
      }
    }
    bool = true;
    f1 = Float.MIN_VALUE;
    break label233;
    label224:
    bool = false;
    f1 = 0.0F;
    localObject2 = null;
    label233:
    localObject4 = ((EmojiTextView)localObject1).getLayout();
    String str2 = "contentText.layout";
    k.a(localObject4, str2);
    i3 = ((Layout)localObject4).getLineCount();
    if (i3 > i4)
    {
      i3 = 1;
    }
    else
    {
      i3 = 0;
      localObject4 = null;
    }
    float f3 = paramInt2;
    paramInt1 = f2 < f3;
    if ((paramInt1 >= 0) && ((!bool) || (i3 == 0)))
    {
      i4 = 0;
      str1 = null;
    }
    paramInt1 = 21;
    f2 = 2.9E-44F;
    int i5;
    if (i4 != 0)
    {
      paramInt2 = 3;
      f3 = 4.2E-45F;
      i5 = ((EmojiTextView)localObject1).getId();
      localLayoutParams.addRule(paramInt2, i5);
      localLayoutParams.addRule(paramInt1);
    }
    else
    {
      paramInt2 = 8;
      f3 = 1.1E-44F;
      if (i3 != 0)
      {
        i5 = ((EmojiTextView)localObject1).getId();
        localLayoutParams.addRule(paramInt2, i5);
        localLayoutParams.addRule(paramInt1);
      }
      else
      {
        paramInt1 = ((EmojiTextView)localObject1).getId();
        localLayoutParams.addRule(paramInt2, paramInt1);
        paramInt1 = 17;
        f2 = 2.4E-44F;
        paramInt2 = ((EmojiTextView)localObject1).getId();
        localLayoutParams.addRule(paramInt1, paramInt2);
      }
    }
    TextView localTextView = k;
    k.a(localTextView, "timestampSimIndicator");
    localTextView.setLayoutParams((ViewGroup.LayoutParams)localObject3);
    localTextView = k;
    Object localObject5 = "timestampSimIndicator";
    k.a(localTextView, (String)localObject5);
    paramInt2 = com.truecaller.common.e.f.b();
    if (paramInt2 != 0)
    {
      localObject5 = k;
      localObject1 = "timestampSimIndicator";
      k.a(localObject5, (String)localObject1);
      localObject5 = ((TextView)localObject5).getResources();
      i5 = 2131165931;
      i2 = ((Resources)localObject5).getDimensionPixelSize(i5);
    }
    localTextView.setMinWidth(i2);
  }
  
  public final void a(int paramInt, d paramd)
  {
    k.b(paramd, "params");
    k.setTextColor(paramInt);
    TextView localTextView = k;
    k.a(localTextView, "timestampSimIndicator");
    a(localTextView, paramd);
  }
  
  public final void a(Uri paramUri, int paramInt1, int paramInt2, int paramInt3, int paramInt4, d paramd)
  {
    d locald = paramd;
    Object localObject1 = paramUri;
    k.b(paramUri, "image");
    k.b(paramd, "params");
    Object localObject2 = a;
    boolean bool1 = i;
    View localView = ab;
    localObject2 = (TextView)localView.findViewById(2131364880);
    int i1 = q;
    ((TextView)localObject2).setTextColor(i1);
    k.a(localObject2, "simIndicator");
    a((TextView)localObject2, paramd);
    a(localView, paramd);
    localObject2 = new android/widget/LinearLayout$LayoutParams;
    i1 = -2;
    ((LinearLayout.LayoutParams)localObject2).<init>(i1, i1);
    localObject2 = (ViewGroup.LayoutParams)localObject2;
    localView.setLayoutParams((ViewGroup.LayoutParams)localObject2);
    a(localView);
    localObject2 = new com/truecaller/messaging/conversation/cb$g;
    ((cb.g)localObject2).<init>(this, paramd);
    localObject2 = (View.OnClickListener)localObject2;
    localView.setOnClickListener((View.OnClickListener)localObject2);
    localObject2 = new com/truecaller/messaging/conversation/cb$h;
    ((cb.h)localObject2).<init>(this);
    localObject2 = (c.g.a.b)localObject2;
    b(localView, (c.g.a.b)localObject2);
    int i4 = b(paramd);
    localObject2 = this;
    Object localObject3 = localView;
    a(localView, paramUri, paramInt1, paramInt2, paramInt3, paramInt4, paramd, i4);
    int i5 = 2131363686;
    localObject2 = localView.findViewById(i5);
    localObject3 = "frame.findViewById<View>(R.id.loading_progress)";
    k.a(localObject2, (String)localObject3);
    boolean bool2 = k;
    int i6 = 8;
    int i2;
    if (bool2)
    {
      bool2 = false;
      localObject3 = null;
    }
    else
    {
      i2 = 8;
    }
    ((View)localObject2).setVisibility(i2);
    i5 = 2131363369;
    localObject2 = localView.findViewById(i5);
    localObject3 = "frame.findViewById<View>(R.id.image_expired)";
    k.a(localObject2, (String)localObject3);
    boolean bool3 = m;
    if (bool3)
    {
      i6 = 0;
      localObject1 = null;
    }
    ((View)localObject2).setVisibility(i6);
    i5 = 2131361840;
    localObject2 = (ImageButton)localView.findViewById(i5);
    bool3 = l;
    int i3;
    if (bool3) {
      i3 = 2131234463;
    } else {
      i3 = 2131234061;
    }
    ((ImageButton)localObject2).setImageResource(i3);
    localObject3 = (TextView)localView.findViewById(2131364597);
    k.a(localObject3, "imageStatusText");
    localObject1 = (CharSequence)u;
    ((TextView)localObject3).setText((CharSequence)localObject1);
    boolean bool4 = n;
    if (bool4)
    {
      k.a(localObject2, "button");
      ((ImageButton)localObject2).setVisibility(0);
      localObject3 = new com/truecaller/messaging/conversation/cb$v;
      ((cb.v)localObject3).<init>(this, locald);
      localObject3 = (View.OnClickListener)localObject3;
      ((ImageButton)localObject2).setOnClickListener((View.OnClickListener)localObject3);
    }
    else
    {
      k.a(localObject2, "button");
      ((ImageButton)localObject2).setVisibility(4);
      bool4 = false;
      localObject3 = null;
      ((ImageButton)localObject2).setOnClickListener(null);
    }
    localObject2 = (LinearLayout)localView.findViewById(2131364068);
    k.a(localObject2, "reactionsView");
    a((LinearLayout)localObject2, locald);
  }
  
  public final void a(Uri paramUri, int paramInt1, int paramInt2, int paramInt3, d paramd)
  {
    k.b(paramUri, "image");
    k.b(paramd, "params");
    a(paramUri, paramInt1, paramInt1, paramInt2, paramInt3, paramd);
  }
  
  public final void a(c.g.a.b paramb)
  {
    k.b(paramb, "listener");
    View localView = f;
    if (localView != null)
    {
      a(localView, paramb);
      return;
    }
  }
  
  public final void a(bv parambv)
  {
    k.b(parambv, "params");
    Object localObject1 = c;
    if (localObject1 != null)
    {
      localObject1 = ((ViewGroup)localObject1).getBackground();
      if (localObject1 != null)
      {
        i1 = a;
        localObject2 = PorterDuff.Mode.SRC_IN;
        ((Drawable)localObject1).setColorFilter(i1, (PorterDuff.Mode)localObject2);
      }
    }
    localObject1 = itemView;
    k.a(localObject1, "itemView");
    int i1 = R.id.history_status_icon;
    localObject1 = (AppCompatImageView)((View)localObject1).findViewById(i1);
    Object localObject3 = localObject1;
    localObject3 = (View)localObject1;
    Object localObject2 = c;
    boolean bool1 = true;
    boolean bool2;
    if (localObject2 != null)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      localObject2 = null;
    }
    a((View)localObject3, bool2);
    localObject3 = c;
    ((AppCompatImageView)localObject1).setImageDrawable((Drawable)localObject3);
    localObject1 = itemView;
    k.a(localObject1, "itemView");
    i1 = R.id.history_type_icon;
    localObject1 = (AppCompatImageView)((View)localObject1).findViewById(i1);
    localObject3 = localObject1;
    localObject3 = (View)localObject1;
    localObject2 = d;
    if (localObject2 != null)
    {
      bool2 = true;
    }
    else
    {
      bool2 = false;
      localObject2 = null;
    }
    a((View)localObject3, bool2);
    localObject3 = d;
    ((AppCompatImageView)localObject1).setImageDrawable((Drawable)localObject3);
    i1 = b;
    ((AppCompatImageView)localObject1).setColorFilter(i1);
    localObject1 = itemView;
    k.a(localObject1, "itemView");
    i1 = R.id.history_type;
    localObject1 = (AppCompatTextView)((View)localObject1).findViewById(i1);
    localObject3 = (CharSequence)e;
    ((AppCompatTextView)localObject1).setText((CharSequence)localObject3);
    i1 = b;
    ((AppCompatTextView)localObject1).setTextColor(i1);
    localObject1 = itemView;
    k.a(localObject1, "itemView");
    i1 = R.id.history_number_type;
    localObject1 = (AppCompatTextView)((View)localObject1).findViewById(i1);
    localObject3 = localObject1;
    localObject3 = (View)localObject1;
    localObject2 = f;
    if (localObject2 == null) {
      bool1 = false;
    }
    a((View)localObject3, bool1);
    localObject3 = (CharSequence)f;
    ((AppCompatTextView)localObject1).setText((CharSequence)localObject3);
    i1 = b;
    ((AppCompatTextView)localObject1).setTextColor(i1);
    localObject1 = (TextView)localObject1;
    i1 = b;
    com.truecaller.utils.extensions.p.a((TextView)localObject1, i1);
    localObject1 = itemView;
    k.a(localObject1, "itemView");
    i1 = R.id.history_timestamp;
    localObject1 = (AppCompatTextView)((View)localObject1).findViewById(i1);
    localObject3 = (CharSequence)g;
    ((AppCompatTextView)localObject1).setText((CharSequence)localObject3);
    i1 = b;
    ((AppCompatTextView)localObject1).setTextColor(i1);
    localObject1 = (TextView)localObject1;
    int i2 = b;
    com.truecaller.utils.extensions.p.a((TextView)localObject1, i2);
  }
  
  public final void a(d paramd)
  {
    k.b(paramd, "params");
    Object localObject1 = a;
    boolean bool1 = i;
    localObject1 = db;
    int i1 = R.id.content_bubble;
    Object localObject2 = (RelativeLayout)((View)localObject1).findViewById(i1);
    Object localObject3 = "view.content_bubble";
    k.a(localObject2, (String)localObject3);
    localObject2 = ((RelativeLayout)localObject2).getBackground();
    if (localObject2 != null)
    {
      i3 = p;
      localObject4 = PorterDuff.Mode.SRC_IN;
      ((Drawable)localObject2).setColorFilter(i3, (PorterDuff.Mode)localObject4);
    }
    boolean bool2 = i;
    int i3 = 0;
    localObject3 = null;
    if (!bool2)
    {
      i2 = 2131364298;
      localObject2 = (EmojiTextView)((View)localObject1).findViewById(i2);
      k.a(localObject2, "senderNameView");
      localObject4 = (CharSequence)e;
      ((EmojiTextView)localObject2).setText((CharSequence)localObject4);
      boolean bool3 = j;
      if (bool3)
      {
        bool3 = false;
        localObject4 = null;
      }
      else
      {
        i4 = 8;
      }
      ((EmojiTextView)localObject2).setVisibility(i4);
      i2 = R.id.status_text;
      localObject2 = (TintedTextView)((View)localObject1).findViewById(i2);
      k.a(localObject2, "view.status_text");
      localObject4 = (CharSequence)u;
      ((TintedTextView)localObject2).setText((CharSequence)localObject4);
    }
    b((View)localObject1, paramd);
    int i2 = 2131363301;
    localObject2 = (TintedImageView)((View)localObject1).findViewById(i2);
    int i4 = R.id.voiceClipSeekBar;
    Object localObject4 = (ProgressBar)((View)localObject1).findViewById(i4);
    k.a(localObject4, "view.voiceClipSeekBar");
    ((ProgressBar)localObject4).setProgress(0);
    boolean bool4 = m;
    if (bool4)
    {
      a(false, (View)localObject1);
      int i5 = 2131234121;
      ((TintedImageView)localObject2).setImageResource(i5);
      ((View)localObject1).setOnClickListener(null);
      ((TintedImageView)localObject2).setOnClickListener(null);
    }
    else
    {
      boolean bool5 = n;
      int i6;
      if (bool5)
      {
        a(false, (View)localObject1);
        bool5 = l;
        if (bool5) {
          i6 = 2131234463;
        } else {
          i6 = 2131234062;
        }
        ((TintedImageView)localObject2).setImageResource(i6);
        localObject4 = new com/truecaller/messaging/conversation/cb$a;
        ((cb.a)localObject4).<init>(this, paramd);
        localObject4 = (View.OnClickListener)localObject4;
        ((View)localObject1).setOnClickListener((View.OnClickListener)localObject4);
        localObject4 = new com/truecaller/messaging/conversation/cb$b;
        ((cb.b)localObject4).<init>(this, paramd);
        localObject4 = (View.OnClickListener)localObject4;
        ((TintedImageView)localObject2).setOnClickListener((View.OnClickListener)localObject4);
      }
      else
      {
        a(k ^ true, (View)localObject1);
        i6 = 2131234387;
        ((TintedImageView)localObject2).setImageResource(i6);
        ((View)localObject1).setOnClickListener(null);
        localObject4 = new com/truecaller/messaging/conversation/cb$c;
        ((cb.c)localObject4).<init>(this, paramd, (View)localObject1, (TintedImageView)localObject2);
        localObject4 = (View.OnClickListener)localObject4;
        ((TintedImageView)localObject2).setOnClickListener((View.OnClickListener)localObject4);
      }
    }
    localObject4 = "iconView";
    k.a(localObject2, (String)localObject4);
    boolean bool6 = k;
    if (bool6) {
      i3 = 4;
    }
    ((TintedImageView)localObject2).setVisibility(i3);
    i2 = 2131362055;
    localObject2 = (CyclicProgressBar)((View)localObject1).findViewById(i2);
    if (localObject2 != null)
    {
      localObject3 = localObject2;
      localObject3 = (View)localObject2;
      bool6 = k;
      a((View)localObject3, bool6);
      i3 = g;
      ((CyclicProgressBar)localObject2).setStrokeColor(i3);
    }
    localObject2 = new com/truecaller/messaging/conversation/cb$d;
    ((cb.d)localObject2).<init>(this);
    localObject2 = (c.g.a.b)localObject2;
    b((View)localObject1, (c.g.a.b)localObject2);
    localObject2 = (TextView)((View)localObject1).findViewById(2131364880);
    i3 = g;
    ((TextView)localObject2).setTextColor(i3);
    k.a(localObject2, "timestampView");
    a((TextView)localObject2, paramd);
    a((View)localObject1);
    localObject1 = (LinearLayout)((View)localObject1).findViewById(2131364068);
    k.a(localObject1, "reactionsView");
    a((LinearLayout)localObject1, paramd);
  }
  
  public final void a(d paramd, int paramInt1, int paramInt2)
  {
    k.b(paramd, "params");
    Object localObject = a;
    boolean bool = i;
    localObject = bb;
    a((View)localObject, paramd, paramInt1, paramInt2);
    d((View)localObject, paramd);
    LinearLayout localLinearLayout = (LinearLayout)((View)localObject).findViewById(2131364068);
    k.a(localLinearLayout, "reactionsView");
    a(localLinearLayout, paramd);
  }
  
  public final void a(d paramd, o paramo, int paramInt, a.a parama, boolean paramBoolean1, boolean paramBoolean2)
  {
    k.b(paramd, "params");
    k.b(paramo, "spamResult");
    k.b(parama, "actionListener");
    ViewGroup localViewGroup = c;
    if (localViewGroup != null)
    {
      paramd = a.a();
      k.a(paramd, "params.viewProvider.acquireSpamUrlViewHolder()");
      k.b(paramo, "spamResult");
      k.b(parama, "actionListener");
      a = parama;
      boolean bool = R.id.divider;
      parama = paramd.a(bool);
      Object localObject1 = "divider";
      k.a(parama, (String)localObject1);
      t.a(parama, paramBoolean1);
      Object localObject2;
      if (paramBoolean2)
      {
        bool = R.id.textSpamUrl;
        parama = (TextView)paramd.a(bool);
        k.a(parama, "textSpamUrl");
        localObject2 = (CharSequence)a.c;
        parama.setText((CharSequence)localObject2);
        bool = R.id.iconLink;
        parama = (ImageView)paramd.a(bool);
        k.a(parama, "iconLink");
        t.a((View)parama);
        bool = R.id.textSpamUrl;
        parama = (TextView)paramd.a(bool);
        localObject2 = "textSpamUrl";
        k.a(parama, (String)localObject2);
        parama = (View)parama;
        t.a(parama);
      }
      else
      {
        bool = R.id.iconLink;
        parama = (ImageView)paramd.a(bool);
        k.a(parama, "iconLink");
        t.b((View)parama);
        bool = R.id.textSpamUrl;
        parama = (TextView)paramd.a(bool);
        localObject2 = "textSpamUrl";
        k.a(parama, (String)localObject2);
        parama = (View)parama;
        t.b(parama);
      }
      parama = b.getContext();
      paramBoolean1 = true;
      Object localObject3;
      int i2;
      if (paramInt > 0)
      {
        paramBoolean2 = R.id.spamCount;
        localObject3 = (TextView)paramd.a(paramBoolean2);
        k.a(localObject3, "spamCount");
        t.a((View)localObject3);
        paramBoolean2 = R.id.spamCount;
        localObject3 = (TextView)paramd.a(paramBoolean2);
        k.a(localObject3, "spamCount");
        k.a(parama, "context");
        localObject1 = parama.getResources();
        Object[] arrayOfObject = new Object[paramBoolean1];
        Integer localInteger = Integer.valueOf(paramInt);
        arrayOfObject[0] = localInteger;
        localObject1 = (CharSequence)((Resources)localObject1).getQuantityString(2131755065, paramInt, arrayOfObject);
        ((TextView)localObject3).setText((CharSequence)localObject1);
        paramBoolean2 = R.id.spamIndicator;
        localObject3 = paramd.a(paramBoolean2);
        k.a(localObject3, "spamIndicator");
        localObject1 = new android/graphics/drawable/ColorDrawable;
        i2 = 2130969224;
        bool = com.truecaller.utils.ui.b.a(parama, i2);
        ((ColorDrawable)localObject1).<init>(bool);
        localObject1 = (Drawable)localObject1;
        ((View)localObject3).setBackground((Drawable)localObject1);
      }
      else
      {
        paramBoolean2 = R.id.spamCount;
        localObject3 = (TextView)paramd.a(paramBoolean2);
        k.a(localObject3, "spamCount");
        t.b((View)localObject3);
        paramBoolean2 = R.id.spamIndicator;
        localObject3 = paramd.a(paramBoolean2);
        k.a(localObject3, "spamIndicator");
        localObject1 = new android/graphics/drawable/ColorDrawable;
        i2 = 2130969243;
        bool = com.truecaller.utils.ui.b.a(parama, i2);
        ((ColorDrawable)localObject1).<init>(bool);
        localObject1 = (Drawable)localObject1;
        ((View)localObject3).setBackground((Drawable)localObject1);
      }
      parama = b;
      bool = d;
      if (bool)
      {
        bool = R.id.reportSpam;
        parama = (Button)paramd.a(bool);
        k.a(parama, "reportSpam");
        t.b((View)parama);
        bool = R.id.reportNotSpam;
        parama = (Button)paramd.a(bool);
        localObject3 = "reportNotSpam";
        k.a(parama, (String)localObject3);
        parama = (View)parama;
        t.b(parama);
      }
      else
      {
        bool = R.id.reportSpam;
        parama = (Button)paramd.a(bool);
        localObject3 = "reportSpam";
        k.a(parama, (String)localObject3);
        parama = (View)parama;
        t.a(parama);
        int i3;
        if (paramInt > 0)
        {
          bool = R.id.reportSpam;
          parama = (Button)paramd.a(bool);
          k.a(parama, "reportSpam");
          localObject3 = b.getContext();
          i3 = 2131888683;
          localObject3 = (CharSequence)((Context)localObject3).getString(i3);
          parama.setText((CharSequence)localObject3);
          bool = R.id.reportNotSpam;
          parama = (Button)paramd.a(bool);
          k.a(parama, "reportNotSpam");
          t.a((View)parama);
          bool = R.id.reportNotSpam;
          parama = (Button)paramd.a(bool);
          localObject3 = new com/truecaller/messaging/conversation/b/a$d;
          ((a.d)localObject3).<init>(paramd, paramo);
          localObject3 = (View.OnClickListener)localObject3;
          parama.setOnClickListener((View.OnClickListener)localObject3);
        }
        else
        {
          bool = R.id.reportSpam;
          parama = (Button)paramd.a(bool);
          k.a(parama, "reportSpam");
          localObject3 = b.getContext();
          i3 = 2131888681;
          localObject3 = (CharSequence)((Context)localObject3).getString(i3);
          parama.setText((CharSequence)localObject3);
          bool = R.id.reportNotSpam;
          parama = (Button)paramd.a(bool);
          localObject3 = "reportNotSpam";
          k.a(parama, (String)localObject3);
          parama = (View)parama;
          t.b(parama);
        }
        bool = R.id.reportSpam;
        parama = (Button)paramd.a(bool);
        localObject3 = new com/truecaller/messaging/conversation/b/a$c;
        ((a.c)localObject3).<init>(paramd, paramo);
        localObject3 = (View.OnClickListener)localObject3;
        parama.setOnClickListener((View.OnClickListener)localObject3);
      }
      parama = b;
      bool = d;
      paramBoolean2 = true;
      if (bool == paramBoolean2)
      {
        i1 = R.id.textReportedNotSpam;
        parama = (TextView)paramd.a(i1);
        localObject3 = "textReportedNotSpam";
        k.a(parama, (String)localObject3);
        parama = (View)parama;
        t.a(parama);
      }
      else
      {
        i1 = R.id.textReportedNotSpam;
        parama = (TextView)paramd.a(i1);
        localObject3 = "textReportedNotSpam";
        k.a(parama, (String)localObject3);
        parama = (View)parama;
        t.b(parama);
      }
      int i1 = 2131364560;
      parama = (ViewGroup)localViewGroup.findViewById(i1);
      if (parama != null)
      {
        paramd = b;
        parama.addView(paramd);
        parama = (View)parama;
        a(parama, paramBoolean1);
      }
      paramd = b;
      if (paramd != null)
      {
        paramd = (TextView)paramd;
        parama = new android/text/SpannableString;
        localObject2 = paramd.getText();
        parama.<init>((CharSequence)localObject2);
        if (paramInt > 0)
        {
          localObject4 = paramd.getContext();
          paramBoolean1 = 2130969241;
          paramInt = com.truecaller.utils.ui.b.a((Context)localObject4, paramBoolean1);
        }
        else
        {
          localObject4 = paramd.getContext();
          paramBoolean1 = 2130969240;
          paramInt = com.truecaller.utils.ui.b.a((Context)localObject4, paramBoolean1);
        }
        localObject2 = new android/text/style/ForegroundColorSpan;
        ((ForegroundColorSpan)localObject2).<init>(paramInt);
        Object localObject4 = a;
        paramInt = a;
        paramo = a;
        int i4 = b;
        paramBoolean2 = true;
        parama.setSpan(localObject2, paramInt, i4, paramBoolean2);
        parama = (CharSequence)parama;
        paramd.setText(parama);
      }
      return;
    }
  }
  
  public final void a(d paramd, String paramString, float paramFloat)
  {
    k.b(paramd, "params");
    k.b(paramString, "text");
    Object localObject1 = a;
    boolean bool1 = i;
    localObject1 = cb;
    bool1 = i;
    if (!bool1)
    {
      int i1 = 2131364298;
      localObject2 = (EmojiTextView)((View)localObject1).findViewById(i1);
      k.a(localObject2, "senderNameView");
      CharSequence localCharSequence = (CharSequence)e;
      ((EmojiTextView)localObject2).setText(localCharSequence);
      boolean bool2 = j;
      int i2;
      if (bool2)
      {
        bool2 = false;
        localCharSequence = null;
      }
      else
      {
        i2 = 8;
      }
      ((EmojiTextView)localObject2).setVisibility(i2);
    }
    Object localObject2 = (EmojiTextView)((View)localObject1).findViewById(2131362596);
    ((EmojiTextView)localObject2).setEmojiScale(paramFloat);
    k.a(localObject2, "emojiTextView");
    paramString = (CharSequence)ah.a(paramString);
    ((EmojiTextView)localObject2).setText(paramString);
    localObject2 = (View)localObject2;
    paramString = new com/truecaller/messaging/conversation/cb$e;
    paramString.<init>(this);
    paramString = (c.g.a.b)paramString;
    a((View)localObject2, paramString);
    paramString = new com/truecaller/messaging/conversation/cb$f;
    paramString.<init>(this);
    paramString = (c.g.a.b)paramString;
    b((View)localObject2, paramString);
    paramString = (TextView)((View)localObject1).findViewById(2131364880);
    int i3 = g;
    paramString.setTextColor(i3);
    k.a(paramString, "timestampView");
    a(paramString, paramd);
    paramString = (LinearLayout)((View)localObject1).findViewById(2131364068);
    k.a(paramString, "reactionsView");
    a(paramString, paramd);
    a((View)localObject1);
  }
  
  public final void a(Message paramMessage, Reaction[] paramArrayOfReaction)
  {
    k.b(paramMessage, "message");
    LinearLayout localLinearLayout = l;
    k.a(localLinearLayout, "reactions");
    a(paramMessage, paramArrayOfReaction, localLinearLayout);
  }
  
  public final void a(ReplySnippet paramReplySnippet, c.g.a.b paramb)
  {
    k.b(paramReplySnippet, "message");
    k.b(paramb, "listener");
    i(true);
    View localView = itemView;
    k.a(localView, "itemView");
    int i1 = R.id.viewReplySnippet;
    MessageSnippetView.a((MessageSnippetView)localView.findViewById(i1), paramReplySnippet);
    paramReplySnippet = itemView;
    k.a(paramReplySnippet, "itemView");
    int i2 = R.id.viewReplySnippet;
    paramReplySnippet = (MessageSnippetView)paramReplySnippet.findViewById(i2);
    k.a(paramReplySnippet, "itemView.viewReplySnippet");
    paramReplySnippet = (View)paramReplySnippet;
    a(paramReplySnippet, paramb);
  }
  
  public final void a(String paramString)
  {
    m = paramString;
  }
  
  public final void a(String paramString, boolean paramBoolean)
  {
    k.b(paramString, "contents");
    Object localObject1 = b;
    if (localObject1 != null)
    {
      Object localObject2 = paramString;
      localObject2 = (CharSequence)paramString;
      ((EmojiTextView)localObject1).setText((CharSequence)localObject2);
      if (paramBoolean)
      {
        localObject2 = localObject1;
        localObject2 = (TextView)localObject1;
        int i1 = 7;
        android.support.v4.e.a.b.a((TextView)localObject2, i1);
        localObject2 = new com/truecaller/utils/ui/LinkClickMovementMethod;
        Context localContext = ((EmojiTextView)localObject1).getContext();
        k.a(localContext, "context");
        Object localObject3 = new com/truecaller/messaging/conversation/cb$q;
        ((cb.q)localObject3).<init>(this, paramString, paramBoolean);
        localObject3 = (c.g.a.m)localObject3;
        ((LinkClickMovementMethod)localObject2).<init>(localContext, (c.g.a.m)localObject3);
        localObject2 = (MovementMethod)localObject2;
        ((EmojiTextView)localObject1).setMovementMethod((MovementMethod)localObject2);
        localObject1 = (View)localObject1;
        localObject2 = new com/truecaller/messaging/conversation/cb$r;
        ((cb.r)localObject2).<init>(this, paramString, paramBoolean);
        localObject2 = (c.g.a.b)localObject2;
        b((View)localObject1, (c.g.a.b)localObject2);
      }
      return;
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    n = paramBoolean;
  }
  
  public final void a(boolean paramBoolean, d paramd)
  {
    k.b(paramd, "params");
    Object localObject1 = b;
    if (localObject1 != null)
    {
      if (paramBoolean)
      {
        localObject2 = localObject1;
        localObject2 = (TextView)localObject1;
        int i1 = 1;
        android.support.v4.e.a.b.a((TextView)localObject2, i1);
      }
      Object localObject2 = p;
      localObject1 = ((EmojiTextView)localObject1).getText();
      k.a(localObject1, "text");
      Object localObject3 = this;
      localObject3 = (ca)this;
      ((com.truecaller.messaging.conversation.a.b.g)localObject2).a((CharSequence)localObject1, (ca)localObject3, paramd);
      return;
    }
  }
  
  public final void b(int paramInt)
  {
    LinearLayout localLinearLayout = l;
    k.a(localLinearLayout, "reactions");
    a(localLinearLayout, paramInt);
  }
  
  public final void b(Uri paramUri, int paramInt1, int paramInt2, int paramInt3, int paramInt4, d paramd)
  {
    k.b(paramUri, "videoFrame");
    k.b(paramd, "params");
    Object localObject = a;
    boolean bool = i;
    View localView = ab;
    localObject = (TextView)localView.findViewById(2131364880);
    int i1 = q;
    ((TextView)localObject).setTextColor(i1);
    k.a(localObject, "simIndicator");
    a((TextView)localObject, paramd);
    b(localView, paramd);
    a(localView, paramd);
    localObject = new android/widget/LinearLayout$LayoutParams;
    i1 = -2;
    ((LinearLayout.LayoutParams)localObject).<init>(i1, i1);
    localObject = (ViewGroup.LayoutParams)localObject;
    localView.setLayoutParams((ViewGroup.LayoutParams)localObject);
    a(localView);
    localObject = new com/truecaller/messaging/conversation/cb$l;
    ((cb.l)localObject).<init>(this);
    localObject = (c.g.a.b)localObject;
    b(localView, (c.g.a.b)localObject);
    int i2 = b(paramd);
    localObject = this;
    a(localView, paramUri, paramInt1, paramInt2, paramInt3, paramInt4, paramd, i2);
    c(localView, paramd);
    localObject = (LinearLayout)localView.findViewById(2131364068);
    k.a(localObject, "reactionsView");
    a((LinearLayout)localObject, paramd);
  }
  
  public final void b(Uri paramUri, int paramInt1, int paramInt2, int paramInt3, d paramd)
  {
    k.b(paramUri, "videoFrame");
    k.b(paramd, "params");
    b(paramUri, paramInt1, paramInt1, paramInt2, paramInt3, paramd);
  }
  
  public final void b(c.g.a.b paramb)
  {
    k.b(paramb, "listener");
    View localView = g;
    if (localView != null)
    {
      a(localView, paramb);
      return;
    }
  }
  
  public final void b(d paramd, int paramInt1, int paramInt2)
  {
    k.b(paramd, "params");
    Object localObject = a;
    boolean bool = i;
    localObject = bb;
    a((View)localObject, paramd, paramInt1, paramInt2);
    d((View)localObject, paramd);
    paramd = new com/truecaller/messaging/conversation/cb$i;
    paramd.<init>(this);
    paramd = (c.g.a.b)paramd;
    a((View)localObject, paramd);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "name");
    EmojiTextView localEmojiTextView = e;
    if (localEmojiTextView != null)
    {
      paramString = (CharSequence)paramString;
      localEmojiTextView.setText(paramString);
      return;
    }
  }
  
  public final void b(boolean paramBoolean)
  {
    a = paramBoolean;
  }
  
  public final boolean b()
  {
    return n;
  }
  
  public final void c()
  {
    EmojiTextView localEmojiTextView = b;
    if (localEmojiTextView != null)
    {
      localEmojiTextView.setEmojiScale(1.5F);
      return;
    }
  }
  
  public final void c(int paramInt)
  {
    EmojiTextView localEmojiTextView = b;
    if (localEmojiTextView == null) {
      return;
    }
    localEmojiTextView.setLinkTextColor(paramInt);
    b.setTextColor(paramInt);
  }
  
  public final void c(c.g.a.b paramb)
  {
    k.b(paramb, "listener");
    Object localObject = h;
    if (localObject != null)
    {
      localObject = (View)localObject;
      a((View)localObject, paramb);
      return;
    }
  }
  
  public final void c(boolean paramBoolean)
  {
    a((View)c, paramBoolean);
  }
  
  public final void d()
  {
    FrameLayout localFrameLayout = j;
    k.a(localFrameLayout, "activeOverlay");
    localFrameLayout.setVisibility(8);
  }
  
  public final void d(int paramInt)
  {
    Object localObject = c;
    if (localObject != null)
    {
      localObject = ((ViewGroup)localObject).getBackground();
      if (localObject != null)
      {
        PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
        ((Drawable)localObject).setColorFilter(paramInt, localMode);
      }
    }
    localObject = d;
    if (localObject != null)
    {
      if (paramInt == 0) {
        paramInt = 4;
      } else {
        paramInt = 0;
      }
      ((View)localObject).setVisibility(paramInt);
      return;
    }
  }
  
  public final void d(c.g.a.b paramb)
  {
    k.b(paramb, "listener");
    Object localObject1 = c;
    if (localObject1 != null)
    {
      localObject1 = (View)localObject1;
      Object localObject2 = new com/truecaller/messaging/conversation/cb$t;
      ((cb.t)localObject2).<init>(paramb);
      localObject2 = (c.g.a.b)localObject2;
      b((View)localObject1, (c.g.a.b)localObject2);
      return;
    }
  }
  
  public final void d(boolean paramBoolean)
  {
    a(f, paramBoolean);
  }
  
  public final void e()
  {
    l.removeAllViews();
  }
  
  public final void e(int paramInt)
  {
    Object localObject1 = c;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = itemView;
    k.a(localObject2, "itemView");
    localObject2 = ((View)localObject2).getContext();
    Object localObject3 = itemView;
    String str = "itemView";
    k.a(localObject3, str);
    int i1 = R.id.viewReplySnippet;
    localObject3 = (MessageSnippetView)((View)localObject3).findViewById(i1);
    i1 = -1 << -1;
    int i2 = View.MeasureSpec.makeMeasureSpec(paramInt, i1);
    int i3 = View.MeasureSpec.makeMeasureSpec(0, 0);
    ((MessageSnippetView)localObject3).measure(i2, i3);
    k.a(localObject2, "context");
    int i4 = ((Context)localObject2).getResources().getDimensionPixelSize(2131165626);
    Object localObject4 = ((Context)localObject2).getResources();
    i3 = 2131166182;
    i2 = ((Resources)localObject4).getDimensionPixelSize(i3);
    i4 += i2;
    int i5 = ((Context)localObject2).getResources().getDimensionPixelSize(i3);
    i4 += i5;
    localObject2 = itemView;
    k.a(localObject2, "itemView");
    i2 = R.id.viewReplySnippet;
    localObject2 = (MessageSnippetView)((View)localObject2).findViewById(i2);
    localObject4 = "itemView.viewReplySnippet";
    k.a(localObject2, (String)localObject4);
    i5 = ((MessageSnippetView)localObject2).getMeasuredWidth() + i4;
    paramInt = View.MeasureSpec.makeMeasureSpec(paramInt, i1);
    i4 = View.MeasureSpec.makeMeasureSpec(0, 0);
    ((ViewGroup)localObject1).measure(paramInt, i4);
    paramInt = ((ViewGroup)localObject1).getMeasuredWidth();
    localObject1 = itemView;
    k.a(localObject1, "itemView");
    i4 = R.id.viewReplySnippet;
    localObject1 = (MessageSnippetView)((View)localObject1).findViewById(i4);
    localObject3 = "itemView.viewReplySnippet";
    k.a(localObject1, (String)localObject3);
    localObject1 = ((MessageSnippetView)localObject1).getLayoutParams();
    if (localObject1 != null)
    {
      localObject1 = (RelativeLayout.LayoutParams)localObject1;
      localObject3 = k;
      str = "timestampSimIndicator";
      k.a(localObject3, str);
      localObject3 = ((TextView)localObject3).getLayoutParams();
      if (localObject3 != null)
      {
        localObject3 = (RelativeLayout.LayoutParams)localObject3;
        i1 = 19;
        if (i5 < paramInt)
        {
          localObject5 = k;
          localObject2 = "timestampSimIndicator";
          k.a(localObject5, (String)localObject2);
          paramInt = ((TextView)localObject5).getId();
          ((RelativeLayout.LayoutParams)localObject1).addRule(i1, paramInt);
          ((RelativeLayout.LayoutParams)localObject3).removeRule(i1);
        }
        else
        {
          ((RelativeLayout.LayoutParams)localObject1).removeRule(i1);
          localObject5 = itemView;
          k.a(localObject5, "itemView");
          i5 = R.id.viewReplySnippet;
          localObject5 = (MessageSnippetView)((View)localObject5).findViewById(i5);
          localObject2 = "itemView.viewReplySnippet";
          k.a(localObject5, (String)localObject2);
          paramInt = ((MessageSnippetView)localObject5).getId();
          ((RelativeLayout.LayoutParams)localObject3).addRule(i1, paramInt);
        }
        localObject5 = k;
        k.a(localObject5, "timestampSimIndicator");
        localObject3 = (ViewGroup.LayoutParams)localObject3;
        ((TextView)localObject5).setLayoutParams((ViewGroup.LayoutParams)localObject3);
        localObject5 = itemView;
        k.a(localObject5, "itemView");
        i5 = R.id.viewReplySnippet;
        localObject5 = (MessageSnippetView)((View)localObject5).findViewById(i5);
        k.a(localObject5, "itemView.viewReplySnippet");
        localObject1 = (ViewGroup.LayoutParams)localObject1;
        ((MessageSnippetView)localObject5).setLayoutParams((ViewGroup.LayoutParams)localObject1);
        return;
      }
      localObject5 = new c/u;
      ((c.u)localObject5).<init>("null cannot be cast to non-null type android.widget.RelativeLayout.LayoutParams");
      throw ((Throwable)localObject5);
    }
    Object localObject5 = new c/u;
    ((c.u)localObject5).<init>("null cannot be cast to non-null type android.widget.RelativeLayout.LayoutParams");
    throw ((Throwable)localObject5);
  }
  
  public final void e(c.g.a.b paramb)
  {
    k.b(paramb, "listener");
    Object localObject = j;
    k.a(localObject, "activeOverlay");
    localObject = (View)localObject;
    a((View)localObject, paramb);
  }
  
  public final void e(boolean paramBoolean)
  {
    a((View)h, paramBoolean);
  }
  
  public final Iterable f()
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Object localObject2 = i;
    int i1;
    if (localObject2 != null)
    {
      i1 = ((ViewGroup)localObject2).getChildCount();
      Object localObject3 = (Iterable)i.b(0, i1);
      Object localObject4 = localObject1;
      localObject4 = (Collection)localObject1;
      localObject3 = ((Iterable)localObject3).iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject3).hasNext();
        if (!bool) {
          break label143;
        }
        Object localObject5 = localObject3;
        int i2 = ((ae)localObject3).a();
        localObject5 = ((ViewGroup)localObject2).getChildAt(i2);
        String str = "container.getChildAt(it)";
        k.a(localObject5, str);
        localObject5 = ((View)localObject5).getTag();
        if (localObject5 == null) {
          break;
        }
        localObject5 = (c)localObject5;
        ((Collection)localObject4).add(localObject5);
      }
      localObject1 = new c/u;
      ((c.u)localObject1).<init>("null cannot be cast to non-null type com.truecaller.messaging.conversation.viewcache.ViewHolder");
      throw ((Throwable)localObject1);
      label143:
      ((ViewGroup)localObject2).removeAllViews();
    }
    localObject2 = c;
    if (localObject2 != null)
    {
      i1 = 2131364560;
      localObject2 = (ViewGroup)((ViewGroup)localObject2).findViewById(i1);
      if (localObject2 != null)
      {
        ((ViewGroup)localObject2).removeAllViews();
        localObject2 = (View)localObject2;
        a((View)localObject2, false);
      }
    }
    return (Iterable)localObject1;
  }
  
  public final void f(boolean paramBoolean)
  {
    a((View)e, paramBoolean);
  }
  
  public final void g()
  {
    i(true);
    View localView = itemView;
    k.a(localView, "itemView");
    int i1 = R.id.viewReplySnippet;
    ((MessageSnippetView)localView.findViewById(i1)).a();
  }
  
  public final void g(boolean paramBoolean)
  {
    FrameLayout localFrameLayout = j;
    k.a(localFrameLayout, "activeOverlay");
    int i1 = 0;
    localFrameLayout.setVisibility(0);
    localFrameLayout = j;
    if (!paramBoolean) {
      i1 = o;
    }
    localFrameLayout.setBackgroundColor(i1);
  }
  
  public final void h(boolean paramBoolean)
  {
    View localView = g;
    if (localView != null)
    {
      a(localView, paramBoolean);
      return;
    }
  }
  
  public final void i(boolean paramBoolean)
  {
    View localView1 = itemView;
    k.a(localView1, "itemView");
    int i1 = R.id.viewReplySnippet;
    a((MessageSnippetView)localView1.findViewById(i1), paramBoolean);
    View localView2 = itemView;
    k.a(localView2, "itemView");
    int i2 = R.id.viewReplySnippet;
    ((MessageSnippetView)localView2.findViewById(i2)).setDismissActionVisible(false);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.cb
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */