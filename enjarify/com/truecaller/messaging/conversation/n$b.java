package com.truecaller.messaging.conversation;

import c.g.a.b;
import c.g.b.k;
import c.g.b.l;
import java.util.Map;

final class n$b
  extends l
  implements b
{
  n$b(Map paramMap)
  {
    super(1);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "key");
    Integer localInteger = (Integer)a.get(paramString);
    int i;
    if (localInteger != null)
    {
      i = localInteger.intValue();
    }
    else
    {
      i = 0;
      localInteger = null;
    }
    Map localMap = a;
    localInteger = Integer.valueOf(i + 1);
    localMap.put(paramString, localInteger);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.n.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */