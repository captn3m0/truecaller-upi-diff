package com.truecaller.messaging.conversation;

import android.text.Editable;
import android.text.TextWatcher;

final class k$1
  implements TextWatcher
{
  k$1(k paramk) {}
  
  public final void afterTextChanged(Editable paramEditable)
  {
    a.b.a(paramEditable);
  }
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.k.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */