package com.truecaller.messaging.conversation;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import c.a.m;
import c.a.y;
import c.g.b.k;
import com.avito.konveyor.b.a;
import com.truecaller.content.TruecallerContract.r;
import com.truecaller.messaging.data.a.j;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class i
  implements h
{
  h.a a;
  boolean b;
  private final i.a c;
  private List d;
  private final i.b e;
  private j f;
  private final ContentResolver g;
  
  public i(ContentResolver paramContentResolver)
  {
    g = paramContentResolver;
    paramContentResolver = new com/truecaller/messaging/conversation/i$a;
    Handler localHandler = new android/os/Handler;
    localHandler.<init>();
    paramContentResolver.<init>(this, localHandler);
    c = paramContentResolver;
    paramContentResolver = (List)y.a;
    d = paramContentResolver;
    paramContentResolver = new com/truecaller/messaging/conversation/i$b;
    localHandler = new android/os/Handler;
    Looper localLooper = Looper.getMainLooper();
    localHandler.<init>(localLooper);
    paramContentResolver.<init>(this, localHandler);
    e = paramContentResolver;
  }
  
  public final int a()
  {
    j localj = f;
    if (localj != null)
    {
      int i = localj.getCount();
      int j = d.size();
      return i + j;
    }
    return 0;
  }
  
  public final int a(long paramLong)
  {
    j localj = f;
    int i = -1;
    if (localj == null) {
      return i;
    }
    int j = 0;
    int k = localj.getCount();
    while (j < k)
    {
      localj.moveToPosition(j);
      long l = localj.a();
      boolean bool = paramLong < l;
      if (!bool)
      {
        int m = d.size();
        return j + m;
      }
      j += 1;
    }
    return i;
  }
  
  public final void a(h.a parama)
  {
    Object localObject = "messagesObserver";
    k.b(parama, (String)localObject);
    a = parama;
    boolean bool1 = b;
    if (bool1) {
      return;
    }
    parama = f;
    if (parama != null)
    {
      localObject = (ContentObserver)c;
      parama.registerContentObserver((ContentObserver)localObject);
    }
    parama = g;
    localObject = TruecallerContract.r.a();
    ContentObserver localContentObserver = (ContentObserver)e;
    boolean bool2 = true;
    parama.registerContentObserver((Uri)localObject, bool2, localContentObserver);
    b = bool2;
  }
  
  public final void a(j paramj)
  {
    j localj = f;
    if (localj != null)
    {
      boolean bool = localj.isClosed();
      if (!bool) {
        localj.close();
      }
    }
    f = paramj;
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "items");
    d = paramList;
  }
  
  public final List b()
  {
    return m.d((Collection)d);
  }
  
  public final int c()
  {
    j localj = f;
    if (localj != null) {
      return localj.getCount();
    }
    return 0;
  }
  
  public final void d()
  {
    Object localObject = null;
    a = null;
    boolean bool = b;
    if (!bool) {
      return;
    }
    localObject = f;
    if (localObject != null)
    {
      localContentObserver = (ContentObserver)c;
      ((j)localObject).unregisterContentObserver(localContentObserver);
    }
    localObject = g;
    ContentObserver localContentObserver = (ContentObserver)e;
    ((ContentResolver)localObject).unregisterContentObserver(localContentObserver);
    b = false;
  }
  
  public final int e()
  {
    Iterator localIterator = d.iterator();
    int i = 0;
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      a locala = (a)localIterator.next();
      long l1 = locala.a();
      long l2 = 4284967296L;
      boolean bool2 = l1 < l2;
      if (!bool2)
      {
        bool1 = true;
      }
      else
      {
        bool1 = false;
        locala = null;
      }
      if (bool1) {
        return i;
      }
      i += 1;
    }
    return -1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */