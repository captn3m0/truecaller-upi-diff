package com.truecaller.messaging.conversation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import com.truecaller.bp;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.a.a;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import java.util.ArrayList;

public class ConversationActivity
  extends AppCompatActivity
  implements f
{
  private k a;
  private g b;
  
  public final g a()
  {
    return b;
  }
  
  public void onBackPressed()
  {
    Object localObject = a;
    if (localObject != null)
    {
      localObject = b;
      boolean bool = ((m)localObject).k();
      if (bool) {}
    }
    else
    {
      super.onBackPressed();
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    int i = aresId;
    setTheme(i);
    Object localObject1 = getIntent();
    Object localObject2 = "filter";
    int j = 0;
    i = ((Intent)localObject1).getIntExtra((String)localObject2, 0);
    Object localObject3 = "conversation_id";
    boolean bool2 = ((Intent)localObject1).hasExtra((String)localObject3);
    long l1 = -1;
    Object localObject4 = null;
    int k;
    Object localObject6;
    if (bool2)
    {
      long l2 = ((Intent)localObject1).getLongExtra("conversation_id", l1);
      localObject3 = Long.valueOf(l2);
      k = i;
      localObject5 = localObject3;
      bool2 = false;
      localObject3 = null;
      localObject6 = null;
    }
    else
    {
      localObject3 = "participants";
      bool2 = ((Intent)localObject1).hasExtra((String)localObject3);
      int m;
      if (bool2)
      {
        localObject3 = ((Intent)localObject1).getParcelableArrayExtra("participants");
        if (localObject3 != null)
        {
          m = localObject3.length;
          if (m > 0)
          {
            m = localObject3.length;
            localObject5 = new Participant[m];
            k = localObject3.length;
            System.arraycopy(localObject3, 0, localObject5, 0, k);
            break label169;
          }
        }
        m = 0;
        localObject5 = null;
        label169:
        k = i;
        bool2 = false;
        localObject3 = null;
        localObject6 = localObject5;
        m = 0;
        localObject5 = null;
      }
      else
      {
        localObject3 = (Conversation)((Intent)localObject1).getParcelableExtra("conversation");
        if (i == 0) {
          i = p;
        }
        localObject5 = new String[0];
        AssertionUtil.isNotNull(localObject3, (String[])localObject5);
        k = i;
        m = 0;
        localObject5 = null;
        localObject6 = null;
      }
    }
    localObject2 = "message_id";
    boolean bool1 = ((Intent)localObject1).hasExtra((String)localObject2);
    if (bool1)
    {
      l1 = ((Intent)localObject1).getLongExtra("message_id", l1);
      localObject2 = Long.valueOf(l1);
      localObject4 = localObject2;
    }
    bk.a locala = new com/truecaller/messaging/conversation/bk$a;
    locala.<init>((byte)0);
    localObject2 = (bp)dagger.a.g.a(((com.truecaller.bk)getApplication()).a());
    c = ((bp)localObject2);
    s locals = new com/truecaller/messaging/conversation/s;
    localObject2 = locals;
    Object localObject7 = localObject6;
    Object localObject8 = localObject5;
    Object localObject5 = this;
    locals.<init>((Conversation)localObject3, (Participant[])localObject6, (Long)localObject8, (Long)localObject4, this, k);
    localObject2 = (s)dagger.a.g.a(locals);
    a = ((s)localObject2);
    localObject2 = new com/truecaller/messaging/a/a;
    ((a)localObject2).<init>(this);
    localObject2 = (a)dagger.a.g.a(localObject2);
    b = ((a)localObject2);
    dagger.a.g.a(a, s.class);
    dagger.a.g.a(b, a.class);
    dagger.a.g.a(c, bp.class);
    localObject2 = new com/truecaller/messaging/conversation/bk;
    localObject3 = a;
    localObject7 = b;
    localObject8 = c;
    ((bk)localObject2).<init>((s)localObject3, (a)localObject7, (bp)localObject8, (byte)0);
    b = ((g)localObject2);
    super.onCreate(paramBundle);
    if (paramBundle != null) {
      return;
    }
    localObject2 = new android/os/Bundle;
    ((Bundle)localObject2).<init>();
    localObject3 = "send_intent";
    bool2 = ((Intent)localObject1).hasExtra((String)localObject3);
    if (bool2)
    {
      localObject3 = ((Intent)localObject1).getParcelableExtra("send_intent");
      localObject1 = localObject3;
      localObject1 = (Intent)localObject3;
    }
    localObject3 = com.truecaller.common.h.o.b((Intent)localObject1);
    if (localObject3 != null)
    {
      localObject7 = "initial_content";
      ((Bundle)localObject2).putString((String)localObject7, (String)localObject3);
    }
    localObject3 = ((Intent)localObject1).getType();
    if (localObject3 != null)
    {
      localObject3 = ((Intent)localObject1).getType();
      bool2 = Entity.a((String)localObject3);
      if (bool2) {}
    }
    else
    {
      j = 1;
    }
    if (j != 0)
    {
      localObject3 = com.truecaller.common.h.o.a((Intent)localObject1);
      if (localObject3 != null)
      {
        boolean bool3 = ((ArrayList)localObject3).isEmpty();
        if (!bool3)
        {
          localObject7 = "initial_attachments";
          ((Bundle)localObject2).putParcelableArrayList((String)localObject7, (ArrayList)localObject3);
        }
      }
    }
    localObject3 = new com/truecaller/messaging/conversation/k;
    ((k)localObject3).<init>();
    a = ((k)localObject3);
    a.setArguments((Bundle)localObject2);
    localObject2 = getSupportFragmentManager().a();
    localObject7 = a;
    ((android.support.v4.app.o)localObject2).b(16908290, (Fragment)localObject7).c();
  }
  
  public void startActivity(Intent paramIntent)
  {
    Context localContext = getApplicationContext();
    boolean bool = com.truecaller.common.h.o.b(localContext, paramIntent);
    if (bool)
    {
      com.truecaller.common.h.o.a(getApplicationContext(), paramIntent, "android.intent.action.VIEW");
      super.startActivity(paramIntent);
      return;
    }
    Toast.makeText(getApplicationContext(), 2131887195, 1).show();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.ConversationActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */