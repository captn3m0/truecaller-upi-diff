package com.truecaller.messaging.conversation;

import dagger.a.d;
import javax.inject.Provider;

public final class ag
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private ag(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static ag a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    ag localag = new com/truecaller/messaging/conversation/ag;
    localag.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localag;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.ag
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */