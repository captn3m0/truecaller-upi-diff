package com.truecaller.messaging.conversation;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import c.g.b.k;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.g.a;
import com.truecaller.messaging.g.a.a;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.ae;
import com.truecaller.util.af;
import com.truecaller.utils.o;
import java.util.Map;
import org.a.a.d.a;
import org.a.a.x;

public final class bg
  extends com.truecaller.messaging.g
  implements bf
{
  private final org.a.a.d.b d;
  private final org.a.a.d.b e;
  private final int f;
  private final int g;
  private final int h;
  private final int i;
  private final int j;
  private final int k;
  private final int l;
  private final af m;
  private final com.truecaller.utils.d n;
  private final ae o;
  private final boolean p;
  private final com.truecaller.messaging.i.d q;
  
  public bg(o paramo, af paramaf, com.truecaller.utils.d paramd, ae paramae, boolean paramBoolean, com.truecaller.messaging.i.d paramd1, Context paramContext)
  {
    super(paramo, paramContext);
    m = paramaf;
    n = paramd;
    o = paramae;
    p = paramBoolean;
    q = paramd1;
    paramaf = a.a("EEEE, dd MMM YYYY");
    d = paramaf;
    paramaf = a.a("EEEE, dd MMM");
    e = paramaf;
    int i1 = paramo.e(2130969236);
    f = i1;
    i1 = paramo.e(2130969533);
    g = i1;
    i1 = -1;
    h = i1;
    int i2 = paramo.e(2130969221);
    i = i2;
    int i3 = paramo.e(2130969235);
    j = i3;
    k = 2131234659;
    l = i1;
  }
  
  private final int a(Map paramMap, int paramInt, c.g.a.b paramb)
  {
    k.b(paramMap, "receiver$0");
    o localo = b;
    Integer localInteger = Integer.valueOf(paramInt);
    paramMap = (g.a)paramMap.get(localInteger);
    if (paramMap != null)
    {
      paramMap = (Integer)paramb.invoke(paramMap);
      if (paramMap != null)
      {
        i1 = paramMap.intValue();
        break label79;
      }
    }
    paramMap = g.a.a.c;
    paramMap = (Number)paramb.invoke(paramMap);
    int i1 = paramMap.intValue();
    label79:
    return localo.e(i1);
  }
  
  public final int a(d paramd)
  {
    Object localObject1 = "params";
    k.b(paramd, (String)localObject1);
    int i1 = o;
    int i2 = 2131166182;
    int i3 = 0;
    Object localObject2 = null;
    if (i1 == 0)
    {
      i1 = 0;
      localObject1 = null;
    }
    else
    {
      localObject1 = c;
      int i4 = o;
      localObject1 = android.support.v4.content.b.a((Context)localObject1, i4);
      boolean bool1 = localObject1 instanceof BitmapDrawable;
      if (!bool1)
      {
        i1 = 0;
        localObject1 = null;
      }
      localObject1 = (BitmapDrawable)localObject1;
      if (localObject1 != null)
      {
        localObject1 = ((BitmapDrawable)localObject1).getBitmap();
        k.a(localObject1, "it.bitmap");
        i1 = ((Bitmap)localObject1).getWidth();
        localObject3 = c.getResources();
        i5 = ((Resources)localObject3).getDimensionPixelSize(i2);
        i1 += i5;
      }
      else
      {
        i1 = 0;
        localObject1 = null;
      }
    }
    int i5 = f;
    if (i5 == 0)
    {
      i2 = 0;
    }
    else
    {
      localObject3 = c;
      int i6 = f;
      localObject3 = android.support.v4.content.b.a((Context)localObject3, i6);
      boolean bool2 = localObject3 instanceof BitmapDrawable;
      if (bool2) {
        localObject2 = localObject3;
      }
      localObject2 = (BitmapDrawable)localObject2;
      if (localObject2 != null)
      {
        localObject2 = ((BitmapDrawable)localObject2).getBitmap();
        k.a(localObject2, "it.bitmap");
        i3 = ((Bitmap)localObject2).getWidth();
        localObject3 = c.getResources();
        i2 = ((Resources)localObject3).getDimensionPixelSize(i2) + i3;
      }
      else
      {
        i2 = 0;
      }
    }
    paramd = h;
    k.a(paramd, "params.timestamp");
    localObject2 = new android/graphics/Paint;
    ((Paint)localObject2).<init>();
    Object localObject4 = c.getResources();
    k.a(localObject4, "context.resources");
    localObject4 = ((Resources)localObject4).getDisplayMetrics();
    float f1 = TypedValue.applyDimension(2, 10.0F, (DisplayMetrics)localObject4);
    ((Paint)localObject2).setTextSize(f1);
    Object localObject3 = Paint.Style.FILL;
    ((Paint)localObject2).setStyle((Paint.Style)localObject3);
    localObject3 = new android/graphics/Rect;
    ((Rect)localObject3).<init>();
    int i7 = paramd.length();
    ((Paint)localObject2).getTextBounds(paramd, 0, i7, (Rect)localObject3);
    int i8 = ((Rect)localObject3).width() + i1 + i2;
    i1 = c.getResources().getDimensionPixelSize(2131166277);
    return i8 + i1;
  }
  
  public final int a(Message paramMessage)
  {
    k.b(paramMessage, "message");
    com.truecaller.messaging.i.d locald = q;
    int i1 = f;
    TransportInfo localTransportInfo = m;
    k.a(localTransportInfo, "message.transportInfo");
    int i2 = localTransportInfo.a();
    paramMessage = m;
    k.a(paramMessage, "message.transportInfo");
    int i3 = paramMessage.b();
    return locald.a(i1, i2, i3, 1);
  }
  
  public final String a(int paramInt, long paramLong)
  {
    Object[] arrayOfObject1 = null;
    switch (paramInt)
    {
    default: 
      return null;
    case 4: 
    case 5: 
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      Object localObject2 = String.valueOf(paramLong / 1000L);
      ((StringBuilder)localObject1).append((String)localObject2);
      ((StringBuilder)localObject1).append(" ");
      localObject2 = b;
      arrayOfObject1 = new Object[0];
      localObject2 = ((o)localObject2).a(2131886663, arrayOfObject1);
      ((StringBuilder)localObject1).append((String)localObject2);
      return ((StringBuilder)localObject1).toString();
    case 3: 
      localObject1 = b;
      arrayOfObject2 = new Object[0];
      return ((o)localObject1).a(2131886679, arrayOfObject2);
    case 2: 
      localObject1 = b;
      arrayOfObject2 = new Object[0];
      return ((o)localObject1).a(2131886680, arrayOfObject2);
    }
    Object localObject1 = b;
    Object[] arrayOfObject2 = new Object[0];
    return ((o)localObject1).a(2131886678, arrayOfObject2);
  }
  
  public final String a(long paramLong)
  {
    long l1 = 0L;
    boolean bool = paramLong < l1;
    if (bool) {
      return "";
    }
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Object localObject = String.valueOf((paramLong + 1023L) / 1024L);
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(" ");
    localObject = b;
    Object[] arrayOfObject = new Object[0];
    localObject = ((o)localObject).a(2131886663, arrayOfObject);
    localStringBuilder.append((String)localObject);
    return localStringBuilder.toString();
  }
  
  public final String a(Entity paramEntity)
  {
    String str = "entity";
    k.b(paramEntity, str);
    boolean bool = paramEntity.d();
    if (bool)
    {
      paramEntity = b;
      int i1 = 2131886118;
      Object[] arrayOfObject = new Object[0];
      paramEntity = paramEntity.a(i1, arrayOfObject);
    }
    for (str = "resourceProvider.getStri…ring.AttachmentTypeVCard)";; str = "entity.type")
    {
      k.a(paramEntity, str);
      return paramEntity;
      paramEntity = j;
    }
  }
  
  public final String a(org.a.a.b paramb)
  {
    k.b(paramb, "messageDate");
    Object localObject1 = new org/a/a/b;
    ((org.a.a.b)localObject1).<init>();
    Object localObject2 = m;
    long l1 = a;
    boolean bool = ((af)localObject2).a(l1);
    if (bool)
    {
      paramb = b;
      localObject2 = new Object[0];
      paramb = paramb.a(2131886446, (Object[])localObject2);
      k.a(paramb, "resourceProvider.getStri….ConversationHeaderToday)");
      return paramb;
    }
    localObject2 = m;
    long l2 = a;
    bool = ((af)localObject2).b(l2);
    if (bool)
    {
      paramb = b;
      localObject2 = new Object[0];
      paramb = paramb.a(2131886447, (Object[])localObject2);
      k.a(paramb, "resourceProvider.getStri…versationHeaderYesterday)");
      return paramb;
    }
    int i1 = paramb.g();
    int i2 = ((org.a.a.b)localObject1).g();
    if (i1 != i2)
    {
      localObject1 = d;
      paramb = (x)paramb;
      paramb = ((org.a.a.d.b)localObject1).a(paramb);
      k.a(paramb, "DATE_WITH_YEAR.print(messageDate)");
      return paramb;
    }
    localObject1 = e;
    long l3 = a;
    paramb = ((org.a.a.d.b)localObject1).a(l3);
    k.a(paramb, "DATE_WITHOUT_YEAR.print(messageDate.millis)");
    return paramb;
  }
  
  public final bq[] a(boolean paramBoolean, int paramInt1, int paramInt2)
  {
    bq localbq1 = new com/truecaller/messaging/conversation/bq;
    int i1 = 2131234463;
    int i2 = 2131886438;
    localbq1.<init>(i2, i1);
    bq localbq2 = new com/truecaller/messaging/conversation/bq;
    int i3 = 2131886432;
    localbq2.<init>(i3, 2131234038);
    bq localbq3 = new com/truecaller/messaging/conversation/bq;
    int i4 = 2131886433;
    localbq3.<init>(i4, 2131234070);
    bq localbq4 = new com/truecaller/messaging/conversation/bq;
    localbq4.<init>(2131886434, i1);
    Object localObject = new com/truecaller/messaging/conversation/bq;
    int i5 = 2131886437;
    ((bq)localObject).<init>(i5, 2131234565);
    bq localbq5 = new com/truecaller/messaging/conversation/bq;
    int i6 = 2131886436;
    localbq5.<init>(i6, 2131234281);
    bq localbq6 = new com/truecaller/messaging/conversation/bq;
    localbq6.<init>(2131886435, 2131234522);
    int i7 = 1;
    int i8 = 3;
    int i9 = 2;
    if (paramInt2 != i8)
    {
      switch (paramInt2)
      {
      default: 
        localObject = localbq4;
        break;
      case 2: 
        localObject = localbq6;
        break;
      case 1: 
        localObject = localbq5;
      }
      if (paramBoolean)
      {
        arrayOfbq = new bq[4];
        arrayOfbq[0] = localbq1;
        arrayOfbq[i7] = localObject;
        arrayOfbq[i9] = localbq2;
        arrayOfbq[i8] = localbq3;
        return arrayOfbq;
      }
      arrayOfbq = new bq[i8];
      arrayOfbq[0] = localObject;
      arrayOfbq[i7] = localbq2;
      arrayOfbq[i9] = localbq3;
      return arrayOfbq;
    }
    if ((!paramBoolean) && (paramInt1 == i9))
    {
      arrayOfbq = new bq[i9];
      arrayOfbq[0] = localbq2;
      arrayOfbq[i7] = localbq3;
      return arrayOfbq;
    }
    bq[] arrayOfbq = new bq[i8];
    arrayOfbq[0] = localbq1;
    arrayOfbq[i7] = localbq2;
    arrayOfbq[i9] = localbq3;
    return arrayOfbq;
  }
  
  public final int b(Message paramMessage)
  {
    Object localObject = "message";
    k.b(paramMessage, (String)localObject);
    boolean bool = p;
    if (bool)
    {
      localObject = paramMessage.k();
      bool = ((TransportInfo)localObject).f();
      if (bool)
      {
        localObject = o;
        paramMessage = l;
        paramMessage = ((ae)localObject).a(paramMessage);
        if (paramMessage != null)
        {
          int i1 = a;
          if (i1 == 0)
          {
            i2 = 2131234549;
            break label99;
          }
          i2 = a;
          i1 = 1;
          if (i2 == i1)
          {
            i2 = 2131234550;
            break label99;
          }
        }
      }
    }
    int i2 = 0;
    paramMessage = null;
    label99:
    return i2;
  }
  
  public final String b(org.a.a.b paramb)
  {
    k.b(paramb, "expiry");
    Object localObject = b.a(2130903043);
    k.a(localObject, "resourceProvider.getStri…array.MmsExpirationMonth)");
    o localo = b;
    Object[] arrayOfObject = new Object[2];
    int i1 = paramb.h();
    int i2 = 1;
    i1 -= i2;
    localObject = localObject[i1];
    arrayOfObject[0] = localObject;
    paramb = Integer.valueOf(paramb.i());
    arrayOfObject[i2] = paramb;
    paramb = localo.a(2131886713, arrayOfObject);
    k.a(paramb, "resourceProvider.getStri… - 1], expiry.dayOfMonth)");
    return paramb;
  }
  
  public final int c()
  {
    return i;
  }
  
  public final int c(int paramInt)
  {
    Map localMap = a;
    c.g.a.b localb = (c.g.a.b)bg.c.a;
    return a(localMap, paramInt, localb);
  }
  
  public final int d()
  {
    return j;
  }
  
  public final int d(int paramInt)
  {
    Map localMap = a;
    c.g.a.b localb = (c.g.a.b)bg.a.a;
    return a(localMap, paramInt, localb);
  }
  
  public final int e()
  {
    return g;
  }
  
  public final int e(int paramInt)
  {
    Map localMap = a;
    c.g.a.b localb = (c.g.a.b)bg.b.a;
    return a(localMap, paramInt, localb);
  }
  
  public final int f()
  {
    return h;
  }
  
  public final int g()
  {
    return f;
  }
  
  public final int h()
  {
    return k;
  }
  
  public final int i()
  {
    return l;
  }
  
  public final int j()
  {
    int i1 = c.getResources().getDimensionPixelSize(2131165626);
    int i2 = c.getResources().getDimensionPixelSize(2131166182);
    i1 += i2;
    Resources localResources = Resources.getSystem();
    k.a(localResources, "Resources.getSystem()");
    i2 = getDisplayMetricswidthPixels;
    int i3 = c.getResources().getDimensionPixelSize(2131165924);
    i2 -= i3;
    i3 = c.getResources().getDimensionPixelSize(2131166204) * 2;
    return i2 - i3 - i1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.bg
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */