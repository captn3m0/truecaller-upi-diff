package com.truecaller.messaging.conversation;

import c.g.b.k;
import c.u;
import com.truecaller.messaging.conversation.b.a;
import com.truecaller.messaging.conversation.b.b;
import com.truecaller.messaging.conversation.b.c;

public final class ck
  implements cj
{
  private final b a;
  
  public ck(b paramb)
  {
    a = paramb;
  }
  
  public final a a()
  {
    Object localObject = a;
    int i = 3;
    localObject = ((b)localObject).a(i);
    if (localObject != null) {
      return (a)localObject;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type com.truecaller.messaging.conversation.viewcache.SpamUrlViewHolder");
    throw ((Throwable)localObject);
  }
  
  public final c a(boolean paramBoolean)
  {
    b localb = a;
    if (paramBoolean) {
      paramBoolean = true;
    } else {
      paramBoolean = true;
    }
    c localc = localb.a(paramBoolean);
    k.a(localc, "viewCacher.acquireFor(\n …TYPE_IMAGE_INCOMING\n    )");
    return localc;
  }
  
  public final void a(c paramc)
  {
    b.a(paramc);
  }
  
  public final c b(boolean paramBoolean)
  {
    b localb = a;
    if (paramBoolean) {
      paramBoolean = true;
    } else {
      paramBoolean = true;
    }
    c localc = localb.a(paramBoolean);
    k.a(localc, "viewCacher.acquireFor(\n …ATTACHMENT_INCOMING\n    )");
    return localc;
  }
  
  public final c c(boolean paramBoolean)
  {
    b localb = a;
    if (paramBoolean) {
      paramBoolean = true;
    } else {
      paramBoolean = true;
    }
    c localc = localb.a(paramBoolean);
    k.a(localc, "viewCacher.acquireFor(\n …TYPE_EMOJI_INCOMING\n    )");
    return localc;
  }
  
  public final c d(boolean paramBoolean)
  {
    b localb = a;
    if (paramBoolean) {
      paramBoolean = true;
    } else {
      paramBoolean = true;
    }
    c localc = localb.a(paramBoolean);
    k.a(localc, "viewCacher.acquireFor(\n …VOICE_CLIP_INCOMING\n    )");
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.ck
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */