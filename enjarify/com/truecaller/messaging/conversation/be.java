package com.truecaller.messaging.conversation;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Parcelable;
import android.text.Spannable;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.messaging.conversation.a.b.o;
import com.truecaller.messaging.d.b;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.ReplySnippet;
import com.truecaller.messaging.g.j;
import com.truecaller.search.local.model.c.a;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract interface be
  extends b, j
{
  public abstract void D();
  
  public abstract void E();
  
  public abstract void F();
  
  public abstract void Q();
  
  public abstract void R();
  
  public abstract void S();
  
  public abstract void T();
  
  public abstract void U();
  
  public abstract void V();
  
  public abstract void X();
  
  public abstract void Z();
  
  public abstract void a(int paramInt, Message paramMessage, List paramList, String paramString, boolean paramBoolean);
  
  public abstract void a(long paramLong, int paramInt);
  
  public abstract void a(Uri paramUri);
  
  public abstract void a(Uri paramUri, String paramString, Drawable paramDrawable);
  
  public abstract void a(Parcelable paramParcelable);
  
  public abstract void a(Spannable paramSpannable);
  
  public abstract void a(Contact paramContact, byte[] paramArrayOfByte);
  
  public abstract void a(Number paramNumber);
  
  public abstract void a(o paramo, boolean paramBoolean);
  
  public abstract void a(ImGroupInfo paramImGroupInfo);
  
  public abstract void a(Message paramMessage);
  
  public abstract void a(Message paramMessage, int[] paramArrayOfInt1, int[] paramArrayOfInt2);
  
  public abstract void a(Participant paramParticipant);
  
  public abstract void a(ReplySnippet paramReplySnippet);
  
  public abstract void a(c.a parama);
  
  public abstract void a(Boolean paramBoolean, Long paramLong);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString, int paramInt);
  
  public abstract void a(String paramString1, String paramString2, String paramString3);
  
  public abstract void a(String paramString1, String paramString2, String paramString3, String paramString4);
  
  public abstract void a(String paramString, boolean paramBoolean);
  
  public abstract void a(ArrayList paramArrayList);
  
  public abstract void a(Map paramMap);
  
  public abstract void a(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, boolean paramBoolean8, boolean paramBoolean9, boolean paramBoolean10);
  
  public abstract void a(boolean paramBoolean, long... paramVarArgs);
  
  public abstract void a(Message... paramVarArgs);
  
  public abstract boolean a(Uri paramUri, String paramString);
  
  public abstract void aa();
  
  public abstract void b(int paramInt);
  
  public abstract void b(ImGroupInfo paramImGroupInfo);
  
  public abstract void b(String paramString);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void b(Message[] paramArrayOfMessage);
  
  public abstract void c(int paramInt);
  
  public abstract void c(boolean paramBoolean);
  
  public abstract void d(boolean paramBoolean);
  
  public abstract void e();
  
  public abstract void e(String paramString);
  
  public abstract void f(String paramString);
  
  public abstract void g(int paramInt);
  
  public abstract void g(String paramString);
  
  public abstract void g(boolean paramBoolean);
  
  public abstract void h();
  
  public abstract void h(int paramInt);
  
  public abstract void h(String paramString);
  
  public abstract void h(boolean paramBoolean);
  
  public abstract void i();
  
  public abstract void i(int paramInt);
  
  public abstract void i(String paramString);
  
  public abstract void j();
  
  public abstract void j(int paramInt);
  
  public abstract void j(String paramString);
  
  public abstract void k();
  
  public abstract void k(int paramInt);
  
  public abstract boolean k(String paramString);
  
  public abstract void l(int paramInt);
  
  public abstract void l(String paramString);
  
  public abstract void m(int paramInt);
  
  public abstract void m(String paramString);
  
  public abstract void n(int paramInt);
  
  public abstract void n(String paramString);
  
  public abstract void n(boolean paramBoolean);
  
  public abstract void o();
  
  public abstract void o(int paramInt);
  
  public abstract void o(String paramString);
  
  public abstract void q(int paramInt);
  
  public abstract void s();
  
  public abstract void s(int paramInt);
  
  public abstract void t();
  
  public abstract void u();
  
  public abstract void v();
  
  public abstract Parcelable w();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.be
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */