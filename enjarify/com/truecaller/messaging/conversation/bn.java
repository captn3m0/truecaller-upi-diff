package com.truecaller.messaging.conversation;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.truecaller.b;
import com.truecaller.bb;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.util.x;
import java.util.Collection;
import java.util.List;

public abstract class bn
  extends bb
  implements b, e.a
{
  abstract void a(int paramInt1, int paramInt2, Intent paramIntent);
  
  abstract void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt);
  
  abstract void a(Uri paramUri, String paramString, Runnable paramRunnable);
  
  abstract void a(Bundle paramBundle);
  
  abstract void a(bn.a parama);
  
  abstract void a(x paramx);
  
  abstract void a(Collection paramCollection, Long paramLong, boolean paramBoolean);
  
  abstract void a(List paramList);
  
  abstract void a(boolean paramBoolean);
  
  abstract void a(boolean paramBoolean, BinaryEntity... paramVarArgs);
  
  abstract void b(Bundle paramBundle);
  
  abstract void c(int paramInt);
  
  abstract void d(int paramInt);
  
  abstract void e();
  
  abstract void f();
  
  abstract Collection g();
  
  abstract boolean h();
  
  abstract boolean i();
  
  abstract void j();
  
  abstract String[] k();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.bn
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */