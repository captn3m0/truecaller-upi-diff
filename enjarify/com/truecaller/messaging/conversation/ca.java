package com.truecaller.messaging.conversation;

import android.net.Uri;
import c.g.a.b;
import com.truecaller.messaging.conversation.a.b.o;
import com.truecaller.messaging.conversation.b.a.a;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Reaction;
import com.truecaller.messaging.data.types.ReplySnippet;

public abstract interface ca
  extends com.avito.konveyor.b.d, l.a
{
  public abstract void a(int paramInt);
  
  public abstract void a(int paramInt1, int paramInt2);
  
  public abstract void a(int paramInt, d paramd);
  
  public abstract void a(Uri paramUri, int paramInt1, int paramInt2, int paramInt3, int paramInt4, d paramd);
  
  public abstract void a(Uri paramUri, int paramInt1, int paramInt2, int paramInt3, d paramd);
  
  public abstract void a(b paramb);
  
  public abstract void a(bv parambv);
  
  public abstract void a(d paramd);
  
  public abstract void a(d paramd, int paramInt1, int paramInt2);
  
  public abstract void a(d paramd, o paramo, int paramInt, a.a parama, boolean paramBoolean1, boolean paramBoolean2);
  
  public abstract void a(d paramd, String paramString, float paramFloat);
  
  public abstract void a(Message paramMessage, Reaction[] paramArrayOfReaction);
  
  public abstract void a(ReplySnippet paramReplySnippet, b paramb);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString, boolean paramBoolean);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void a(boolean paramBoolean, d paramd);
  
  public abstract void b(int paramInt);
  
  public abstract void b(Uri paramUri, int paramInt1, int paramInt2, int paramInt3, int paramInt4, d paramd);
  
  public abstract void b(Uri paramUri, int paramInt1, int paramInt2, int paramInt3, d paramd);
  
  public abstract void b(b paramb);
  
  public abstract void b(d paramd, int paramInt1, int paramInt2);
  
  public abstract void b(String paramString);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c();
  
  public abstract void c(int paramInt);
  
  public abstract void c(b paramb);
  
  public abstract void c(boolean paramBoolean);
  
  public abstract void d();
  
  public abstract void d(int paramInt);
  
  public abstract void d(b paramb);
  
  public abstract void d(boolean paramBoolean);
  
  public abstract void e();
  
  public abstract void e(int paramInt);
  
  public abstract void e(b paramb);
  
  public abstract void e(boolean paramBoolean);
  
  public abstract Iterable f();
  
  public abstract void f(boolean paramBoolean);
  
  public abstract void g();
  
  public abstract void g(boolean paramBoolean);
  
  public abstract void h(boolean paramBoolean);
  
  public abstract void i(boolean paramBoolean);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.ca
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */