package com.truecaller.messaging.conversation;

import android.content.Context;
import android.support.v7.widget.ListPopupWindow;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import c.a.m;
import c.g.b.k;
import com.truecaller.calling.ah;
import com.truecaller.calling.dialer.ax;
import com.truecaller.data.entity.Number;
import com.truecaller.utils.n;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class ch
  implements cg
{
  private ListPopupWindow a;
  private final n b;
  private final ax c;
  
  public ch(n paramn, ax paramax)
  {
    b = paramn;
    c = paramax;
  }
  
  public final void a()
  {
    ListPopupWindow localListPopupWindow = a;
    if (localListPopupWindow != null)
    {
      localListPopupWindow.dismiss();
      return;
    }
  }
  
  public final void a(Context paramContext, View paramView, Number paramNumber, cf paramcf)
  {
    k.b(paramContext, "context");
    k.b(paramView, "anchor");
    k.b(paramNumber, "number");
    k.b(paramcf, "listener");
    int i = 2;
    Object localObject1 = new HashMap[i];
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    Object localObject2 = localHashMap;
    localObject2 = (Map)localHashMap;
    int j = 2131234374;
    Object localObject3 = Integer.valueOf(j);
    ((Map)localObject2).put("ICON", localObject3);
    Object localObject4 = "TITLE";
    localObject3 = paramNumber.n();
    if (localObject3 == null) {
      localObject3 = "";
    }
    ((Map)localObject2).put(localObject4, localObject3);
    localObject3 = b;
    Object localObject5 = c;
    localObject3 = ah.a(paramNumber, (n)localObject3, (ax)localObject5);
    ((Map)localObject2).put("SUBTITLE", localObject3);
    localObject1[0] = localHashMap;
    int k = 1;
    localObject2 = new java/util/HashMap;
    ((HashMap)localObject2).<init>();
    localObject4 = localObject2;
    localObject4 = (Map)localObject2;
    localObject5 = Integer.valueOf(2131234671);
    ((Map)localObject4).put("ICON", localObject5);
    int m = 2131889034;
    localObject5 = paramContext.getString(m);
    Object localObject6 = "context.getString(R.string.voip_text)";
    k.a(localObject5, (String)localObject6);
    ((Map)localObject4).put("TITLE", localObject5);
    localObject3 = "SUBTITLE";
    paramNumber = paramNumber.n();
    if (paramNumber == null) {
      paramNumber = "";
    }
    ((Map)localObject4).put(localObject3, paramNumber);
    localObject1[k] = localObject2;
    List localList = m.b((Object[])localObject1);
    paramNumber = new android/widget/SimpleAdapter;
    String[] tmp277_274 = new String[3];
    String[] tmp278_277 = tmp277_274;
    String[] tmp278_277 = tmp277_274;
    tmp278_277[0] = "ICON";
    tmp278_277[1] = "TITLE";
    tmp278_277[2] = "SUBTITLE";
    String[] arrayOfString = tmp278_277;
    int[] arrayOfInt = new int[3];
    int[] tmp300_298 = arrayOfInt;
    tmp300_298[0] = 2131363371;
    int[] tmp305_300 = tmp300_298;
    tmp305_300[1] = 2131364825;
    tmp305_300[2] = 2131364822;
    localObject5 = paramNumber;
    localObject6 = paramContext;
    paramNumber.<init>(paramContext, localList, 2131559171, arrayOfString, arrayOfInt);
    localObject1 = new android/support/v7/widget/ListPopupWindow;
    ((ListPopupWindow)localObject1).<init>(paramContext);
    ((ListPopupWindow)localObject1).setAnchorView(paramView);
    ((ListPopupWindow)localObject1).setHeight(-2);
    paramContext = paramNumber;
    paramContext = (ListAdapter)paramNumber;
    ((ListPopupWindow)localObject1).setAdapter(paramContext);
    paramContext = new com/truecaller/messaging/conversation/ch$a;
    paramContext.<init>(paramView, paramNumber, paramcf);
    paramContext = (AdapterView.OnItemClickListener)paramContext;
    ((ListPopupWindow)localObject1).setOnItemClickListener(paramContext);
    ((ListPopupWindow)localObject1).show();
    a = ((ListPopupWindow)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.ch
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */