package com.truecaller.messaging.conversation;

import android.content.ContentResolver;
import android.content.Context;
import android.media.AudioManager;
import com.avito.konveyor.a.a;
import com.truecaller.analytics.ap;
import com.truecaller.analytics.aq;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.conversation.a.b.j;
import com.truecaller.messaging.conversation.a.b.q;
import com.truecaller.messaging.conversation.a.b.r;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.transport.im.bu;
import com.truecaller.messaging.transport.m;
import com.truecaller.multisim.ae;
import com.truecaller.util.af;
import com.truecaller.util.al;
import com.truecaller.util.bd;
import com.truecaller.util.ch;
import com.truecaller.util.ci;
import com.truecaller.utils.l;
import com.truecaller.utils.n;
import com.truecaller.utils.o;

class s
{
  final Conversation a;
  final Participant[] b;
  final Long c;
  final Long d;
  final int e;
  final Context f;
  
  s(Conversation paramConversation, Participant[] paramArrayOfParticipant, Long paramLong1, Long paramLong2, Context paramContext, int paramInt)
  {
    boolean bool;
    if ((paramConversation == null) && (paramArrayOfParticipant == null) && (paramLong1 == null)) {
      bool = true;
    } else {
      bool = false;
    }
    String[] arrayOfString = { "At least one should be not null" };
    AssertionUtil.isFalse(bool, arrayOfString);
    a = paramConversation;
    b = paramArrayOfParticipant;
    c = paramLong1;
    d = paramLong2;
    f = paramContext;
    e = paramInt;
  }
  
  static com.avito.konveyor.a.a a(com.avito.konveyor.a parama, h paramh)
  {
    com.avito.konveyor.a.d locald = new com/avito/konveyor/a/d;
    locald.<init>(parama, parama);
    locald.a(paramh);
    return locald;
  }
  
  static com.avito.konveyor.a a(com.truecaller.messaging.conversation.a.a.a parama, com.truecaller.messaging.conversation.a.b.e parame, com.truecaller.messaging.conversation.a.b.d paramd, com.truecaller.messaging.conversation.a.b.i parami, com.truecaller.messaging.conversation.a.b.b paramb, q paramq)
  {
    a.a locala = new com/avito/konveyor/a$a;
    locala.<init>();
    parama = locala.a(paramb).a(parama).a(paramq).a(paramd).a(parami).a(parame);
    parame = new com/avito/konveyor/a;
    paramd = a;
    parama = b;
    parame.<init>(paramd, parama, (byte)0);
    return parame;
  }
  
  static ap a(com.truecaller.androidactors.f paramf, com.truecaller.analytics.b paramb)
  {
    aq localaq = new com/truecaller/analytics/aq;
    localaq.<init>(paramf, paramb);
    return localaq;
  }
  
  static com.truecaller.androidactors.i a(com.truecaller.androidactors.k paramk)
  {
    return paramk.a();
  }
  
  static com.truecaller.messaging.conversation.a.a.a a(com.truecaller.messaging.conversation.a.a.c paramc)
  {
    com.truecaller.messaging.conversation.a.a.a locala = new com/truecaller/messaging/conversation/a/a/a;
    locala.<init>(paramc);
    return locala;
  }
  
  static com.truecaller.messaging.conversation.a.a.c a(bb parambb)
  {
    com.truecaller.messaging.conversation.a.a.c localc = new com/truecaller/messaging/conversation/a/a/c;
    localc.<init>(parambb);
    return localc;
  }
  
  static com.truecaller.messaging.conversation.a.b.b a(com.truecaller.messaging.conversation.a.b.c paramc)
  {
    com.truecaller.messaging.conversation.a.b.b localb = new com/truecaller/messaging/conversation/a/b/b;
    localb.<init>(paramc);
    return localb;
  }
  
  static com.truecaller.messaging.conversation.a.b.c a(bi parambi, bf parambf, h paramh, m paramm, bb parambb, af paramaf, cj paramcj, com.truecaller.featuretoggles.e parame, bs parambs)
  {
    com.truecaller.messaging.conversation.a.b.c localc = new com/truecaller/messaging/conversation/a/b/c;
    localc.<init>(parambi, parambf, paramh, paramm, parambb, paramcj, paramaf, parame, parambs);
    return localc;
  }
  
  static com.truecaller.messaging.conversation.a.b.d a(com.truecaller.messaging.conversation.a.b.g paramg)
  {
    com.truecaller.messaging.conversation.a.b.d locald = new com/truecaller/messaging/conversation/a/b/d;
    locald.<init>(paramg);
    return locald;
  }
  
  static com.truecaller.messaging.conversation.a.b.e a(j paramj)
  {
    com.truecaller.messaging.conversation.a.b.e locale = new com/truecaller/messaging/conversation/a/b/e;
    locale.<init>(paramj);
    return locale;
  }
  
  static com.truecaller.messaging.conversation.a.b.g a(bi parambi, bf parambf, h paramh, m paramm, bb parambb, com.truecaller.util.g paramg, af paramaf, com.truecaller.messaging.i.d paramd, cj paramcj, bu parambu, com.truecaller.messaging.conversation.a.b.k paramk, com.truecaller.featuretoggles.e parame)
  {
    com.truecaller.messaging.conversation.a.b.h localh = new com/truecaller/messaging/conversation/a/b/h;
    localh.<init>(parambi, parambf, paramh, paramm, parambb, paramg, paramaf, paramd, paramcj, parambu, paramk, parame);
    return localh;
  }
  
  static j a(bi parambi, bf parambf, h paramh, m paramm, bb parambb, af paramaf, cj paramcj, com.truecaller.featuretoggles.e parame)
  {
    j localj = new com/truecaller/messaging/conversation/a/b/j;
    localj.<init>(parambi, parambf, paramh, paramm, parambb, paramcj, paramaf, parame);
    return localj;
  }
  
  static q a(r paramr)
  {
    q localq = new com/truecaller/messaging/conversation/a/b/q;
    localq.<init>(paramr);
    return localq;
  }
  
  static r a(bi parambi, bf parambf, m paramm, cj paramcj, h paramh, bb parambb, com.truecaller.featuretoggles.e parame)
  {
    r localr = new com/truecaller/messaging/conversation/a/b/r;
    localr.<init>(parambi, parambf, paramm, paramcj, paramh, parambb, parame);
    return localr;
  }
  
  static bf a(o paramo, Context paramContext, com.truecaller.utils.d paramd, com.truecaller.multisim.h paramh, ae paramae, af paramaf, com.truecaller.messaging.i.d paramd1)
  {
    bg localbg = new com/truecaller/messaging/conversation/bg;
    boolean bool = paramh.j();
    localbg.<init>(paramo, paramaf, paramd, paramae, bool, paramd1, paramContext);
    return localbg;
  }
  
  static bi a()
  {
    bj localbj = new com/truecaller/messaging/conversation/bj;
    localbj.<init>();
    return localbj;
  }
  
  static bn a(com.truecaller.androidactors.k paramk, com.truecaller.androidactors.f paramf1, com.truecaller.util.bg parambg, al paramal, ContentResolver paramContentResolver, o paramo, com.truecaller.androidactors.f paramf2, com.truecaller.analytics.b paramb, ch paramch, l paraml, af paramaf, bd parambd)
  {
    bo localbo = new com/truecaller/messaging/conversation/bo;
    com.truecaller.androidactors.i locali = paramk.a();
    localbo.<init>(locali, paramf1, parambg, paramal, paramContentResolver, paramo, paramf2, paramb, paramch, paraml, paramaf, parambd);
    return localbo;
  }
  
  static bs a(o paramo, com.truecaller.multisim.h paramh, ae paramae)
  {
    bt localbt = new com/truecaller/messaging/conversation/bt;
    boolean bool = paramh.j();
    localbt.<init>(paramo, bool, paramae);
    return localbt;
  }
  
  static cj a(com.truecaller.messaging.conversation.b.b paramb)
  {
    ck localck = new com/truecaller/messaging/conversation/ck;
    localck.<init>(paramb);
    return localck;
  }
  
  static com.truecaller.messaging.conversation.voice_notes.f a(Context paramContext)
  {
    com.truecaller.messaging.conversation.voice_notes.d locald = new com/truecaller/messaging/conversation/voice_notes/d;
    locald.<init>(paramContext);
    return locald;
  }
  
  static com.truecaller.messaging.d.a a(n paramn, com.truecaller.multisim.h paramh, com.truecaller.analytics.b paramb)
  {
    com.truecaller.messaging.d.a locala = new com/truecaller/messaging/d/a;
    locala.<init>(paramh, paramn, paramb, "conversation");
    return locala;
  }
  
  static ch a(ContentResolver paramContentResolver)
  {
    ci localci = new com/truecaller/util/ci;
    localci.<init>(paramContentResolver);
    return localci;
  }
  
  static AudioManager b(Context paramContext)
  {
    return com.truecaller.utils.extensions.i.i(paramContext);
  }
  
  static com.truecaller.messaging.conversation.a.b.i b(com.truecaller.messaging.conversation.a.b.g paramg)
  {
    com.truecaller.messaging.conversation.a.b.i locali = new com/truecaller/messaging/conversation/a/b/i;
    locali.<init>(paramg);
    return locali;
  }
  
  static com.truecaller.messaging.conversation.b.b b()
  {
    com.truecaller.messaging.conversation.b.b localb = new com/truecaller/messaging/conversation/b/b;
    localb.<init>();
    return localb;
  }
  
  static int c()
  {
    return 100;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */