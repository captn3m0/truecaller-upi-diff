package com.truecaller.messaging.conversation;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.State;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import c.g.a.a;
import c.g.b.k;
import c.g.b.r;
import c.g.b.s;
import c.g.b.w;
import c.l.b;
import com.truecaller.R.id;

public final class l
  extends RecyclerView.ItemDecoration
{
  private final View b;
  private final AppCompatTextView c;
  private final int d;
  private final Paint e;
  private final int f;
  private final int g;
  
  static
  {
    c.l.g[] arrayOfg = new c.l.g[2];
    Object localObject = new c/g/b/s;
    b localb = w.a(l.class);
    ((s)localObject).<init>(localb, "widthSpec", "<v#0>");
    localObject = (c.l.g)w.a((r)localObject);
    arrayOfg[0] = localObject;
    localObject = new c/g/b/s;
    localb = w.a(l.class);
    ((s)localObject).<init>(localb, "heightSpec", "<v#1>");
    localObject = (c.l.g)w.a((r)localObject);
    arrayOfg[1] = localObject;
    a = arrayOfg;
  }
  
  public l(Context paramContext, int paramInt1, int paramInt2)
  {
    f = paramInt1;
    g = paramInt2;
    Object localObject1 = LayoutInflater.from(paramContext);
    paramInt2 = f;
    localObject1 = ((LayoutInflater)localObject1).inflate(paramInt2, null);
    Object localObject2 = new android/support/v7/widget/RecyclerView$LayoutParams;
    int i = d;
    ((RecyclerView.LayoutParams)localObject2).<init>(-1, i);
    localObject2 = (ViewGroup.LayoutParams)localObject2;
    ((View)localObject1).setLayoutParams((ViewGroup.LayoutParams)localObject2);
    paramInt2 = com.truecaller.common.e.f.b();
    ((View)localObject1).setLayoutDirection(paramInt2);
    b = ((View)localObject1);
    localObject1 = b;
    k.a(localObject1, "headerView");
    paramInt2 = R.id.headerText;
    localObject1 = (AppCompatTextView)((View)localObject1).findViewById(paramInt2);
    c = ((AppCompatTextView)localObject1);
    int j = paramContext.getResources().getDimensionPixelSize(2131165864);
    d = j;
    paramContext = new android/graphics/Paint;
    paramContext.<init>();
    paramContext.setColor(0);
    localObject1 = Paint.Style.FILL;
    paramContext.setStyle((Paint.Style)localObject1);
    e = paramContext;
  }
  
  public final void getItemOffsets(Rect paramRect, View paramView, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    k.b(paramRect, "outRect");
    k.b(paramView, "view");
    k.b(paramRecyclerView, "parent");
    Object localObject = "state";
    k.b(paramState, (String)localObject);
    super.getItemOffsets(paramRect, paramView, paramRecyclerView, paramState);
    paramState = paramRecyclerView.getChildViewHolder(paramView);
    boolean bool1 = paramState instanceof l.a;
    int j;
    if (!bool1)
    {
      j = 0;
      paramState = null;
    }
    paramState = (l.a)paramState;
    if (paramState != null)
    {
      paramState = paramState.a();
      if (paramState != null)
      {
        paramState = b;
        int i = paramRecyclerView.getWidth();
        int k = 1073741824;
        i = View.MeasureSpec.makeMeasureSpec(i, k);
        int m = d;
        k = View.MeasureSpec.makeMeasureSpec(m, k);
        paramState.measure(i, k);
        paramRect.setEmpty();
        j = top;
        localObject = b;
        String str = "headerView";
        k.a(localObject, str);
        i = ((View)localObject).getMeasuredHeight();
        j += i;
        top = j;
        paramView = paramRecyclerView.getChildViewHolder(paramView);
        boolean bool2 = paramView instanceof l.a;
        boolean bool3;
        if (!bool2)
        {
          bool3 = false;
          paramView = null;
        }
        paramView = (l.a)paramView;
        if (paramView != null)
        {
          bool3 = paramView.b();
          if (bool3)
          {
            int i1 = top;
            int n = g;
            i1 += n;
            top = i1;
          }
          return;
        }
        return;
      }
    }
  }
  
  public final void onDraw(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    l locall = this;
    Canvas localCanvas = paramCanvas;
    RecyclerView localRecyclerView = paramRecyclerView;
    k.b(paramCanvas, "c");
    k.b(paramRecyclerView, "parent");
    Object localObject1 = paramState;
    k.b(paramState, "state");
    super.onDraw(paramCanvas, paramRecyclerView, paramState);
    Object localObject2 = new com/truecaller/messaging/conversation/l$c;
    ((l.c)localObject2).<init>(paramRecyclerView);
    c.f localf1 = c.g.a((a)localObject2);
    localObject2 = new com/truecaller/messaging/conversation/l$b;
    ((l.b)localObject2).<init>(this);
    localObject2 = (a)localObject2;
    c.f localf2 = c.g.a((a)localObject2);
    int i = paramRecyclerView.getChildCount();
    int j = 0;
    while (j < i)
    {
      localObject2 = localRecyclerView.getChildAt(j);
      localObject1 = localRecyclerView.getChildViewHolder((View)localObject2);
      boolean bool = localObject1 instanceof l.a;
      int m;
      float f1;
      if (!bool)
      {
        m = 0;
        f1 = 0.0F;
        localObject1 = null;
      }
      localObject1 = (l.a)localObject1;
      if (localObject1 != null)
      {
        localObject1 = ((l.a)localObject1).a();
        if (localObject1 != null)
        {
          Object localObject3 = c;
          k.a(localObject3, "headerTextView");
          localObject1 = (CharSequence)localObject1;
          ((AppCompatTextView)localObject3).setText((CharSequence)localObject1);
          paramCanvas.save();
          View localView = b;
          localView.invalidate();
          m = ((Number)localf1.b()).intValue();
          int k = ((Number)localf2.b()).intValue();
          localView.measure(m, k);
          localObject1 = b;
          k.a(localObject1, "headerView");
          m = ((View)localObject1).getMeasuredWidth();
          localObject3 = b;
          String str = "headerView";
          k.a(localObject3, str);
          k = ((View)localObject3).getMeasuredHeight();
          localView.layout(0, 0, m, k);
          localObject1 = null;
          k.a(localObject2, "child");
          k = ((View)localObject2).getTop();
          int n = localView.getHeight();
          float f2 = k - n;
          localCanvas.translate(0.0F, f2);
          m = ((View)localObject2).getLeft();
          f1 = m;
          k = 0;
          f2 = 0.0F;
          localObject3 = null;
          float f3 = ((View)localObject2).getRight();
          int i1 = localView.getHeight();
          float f4 = i1;
          Paint localPaint = e;
          localObject2 = paramCanvas;
          paramCanvas.drawRect(f1, 0.0F, f3, f4, localPaint);
          localView.draw(localCanvas);
          paramCanvas.restore();
        }
      }
      j += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */