package com.truecaller.messaging.conversation;

import android.view.View;
import android.widget.ProgressBar;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.messaging.conversation.a.b.g;
import com.truecaller.messaging.conversation.voice_notes.e;
import com.truecaller.ui.view.TintedImageView;

public final class cb$c$1
  implements e
{
  cb$c$1(cb.c paramc) {}
  
  public final void a(int paramInt)
  {
    Object localObject = a.c;
    int i = R.id.voiceClipSeekBar;
    localObject = (ProgressBar)((View)localObject).findViewById(i);
    k.a(localObject, "view.voiceClipSeekBar");
    ((ProgressBar)localObject).setMax(paramInt);
  }
  
  public final void b(int paramInt)
  {
    Object localObject = a.c;
    int i = R.id.voiceClipSeekBar;
    localObject = (ProgressBar)((View)localObject).findViewById(i);
    k.a(localObject, "view.voiceClipSeekBar");
    ((ProgressBar)localObject).setProgress(paramInt);
  }
  
  public final void c(int paramInt)
  {
    TintedImageView localTintedImageView;
    int i;
    if (paramInt == 0)
    {
      localTintedImageView = a.d;
      i = 2131234363;
      localTintedImageView.setImageResource(i);
    }
    else
    {
      localTintedImageView = a.d;
      i = 2131234387;
      localTintedImageView.setImageResource(i);
    }
    int j = 3;
    if (paramInt == j)
    {
      g localg = cb.a(a.a);
      localg.a();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.cb.c.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */