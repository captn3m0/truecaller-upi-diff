package com.truecaller.messaging.conversation;

import dagger.a.d;
import javax.inject.Provider;

public final class ah
  implements d
{
  private final Provider a;
  
  private ah(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static ah a(Provider paramProvider)
  {
    ah localah = new com/truecaller/messaging/conversation/ah;
    localah.<init>(paramProvider);
    return localah;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.ah
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */