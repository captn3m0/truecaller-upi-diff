package com.truecaller.messaging.conversation;

public enum CallType
{
  static
  {
    CallType[] arrayOfCallType = new CallType[2];
    CallType localCallType = new com/truecaller/messaging/conversation/CallType;
    localCallType.<init>("PHONE", 0);
    PHONE = localCallType;
    arrayOfCallType[0] = localCallType;
    localCallType = new com/truecaller/messaging/conversation/CallType;
    int i = 1;
    localCallType.<init>("VOIP", i);
    VOIP = localCallType;
    arrayOfCallType[i] = localCallType;
    $VALUES = arrayOfCallType;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.CallType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */