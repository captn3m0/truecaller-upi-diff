package com.truecaller.messaging.conversation;

import dagger.a.d;
import javax.inject.Provider;

public final class x
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private x(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static x a(Provider paramProvider1, Provider paramProvider2)
  {
    x localx = new com/truecaller/messaging/conversation/x;
    localx.<init>(paramProvider1, paramProvider2);
    return localx;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.x
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */