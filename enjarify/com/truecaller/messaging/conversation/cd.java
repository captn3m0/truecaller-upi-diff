package com.truecaller.messaging.conversation;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;

public final class cd
  extends LayerDrawable
{
  private final int a;
  private final int b;
  
  public cd(Context paramContext, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super(arrayOfDrawable);
    a = paramInt3;
    b = paramInt4;
  }
  
  public final int getIntrinsicHeight()
  {
    return b;
  }
  
  public final int getIntrinsicWidth()
  {
    return a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.cd
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */