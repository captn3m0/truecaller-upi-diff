package com.truecaller.messaging.conversation;

import android.content.res.Resources;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import com.truecaller.utils.ui.b;

public final class e
  implements ViewTreeObserver.OnGlobalLayoutListener
{
  private static final boolean g;
  final View a;
  final View b;
  final View c;
  final View d;
  final View e;
  PopupWindow f;
  private final View h;
  private final View i;
  
  static
  {
    int j = Build.VERSION.SDK_INT;
    int k = 21;
    if (j >= k) {
      j = 1;
    } else {
      j = 0;
    }
    g = j;
  }
  
  e(ViewGroup paramViewGroup, View paramView, e.b paramb, e.a parama)
  {
    paramViewGroup = LayoutInflater.from(paramViewGroup.getContext()).inflate(2131559149, paramViewGroup, false);
    h = paramViewGroup;
    paramViewGroup = h;
    PorterDuff.Mode localMode = PorterDuff.Mode.MULTIPLY;
    int j = b.a(paramViewGroup.getContext(), 2130969548);
    b.a(paramViewGroup, j, localMode);
    i = paramView;
    paramViewGroup = h.findViewById(2131362336);
    a = paramViewGroup;
    paramViewGroup = a;
    paramView = new com/truecaller/messaging/conversation/-$$Lambda$e$u_SIilZ3mL315zjTP4dhPCMEGaQ;
    paramView.<init>(paramb);
    paramViewGroup.setOnClickListener(paramView);
    paramViewGroup = h.findViewById(2131362329);
    b = paramViewGroup;
    paramViewGroup = b;
    paramView = new com/truecaller/messaging/conversation/-$$Lambda$e$4djuYC9v94EXUyp4ir4CR6W5PhY;
    paramView.<init>(paramb);
    paramViewGroup.setOnClickListener(paramView);
    paramViewGroup = h.findViewById(2131362309);
    c = paramViewGroup;
    paramViewGroup = c;
    paramView = new com/truecaller/messaging/conversation/-$$Lambda$e$44sxCLo2DHXvbinYNxnxAXtv6QM;
    paramView.<init>(parama);
    paramViewGroup.setOnClickListener(paramView);
    paramViewGroup = h.findViewById(2131362342);
    d = paramViewGroup;
    paramViewGroup = d;
    paramView = new com/truecaller/messaging/conversation/-$$Lambda$e$F1LHE3_isAkjw_9D1yM3arCtlYY;
    paramView.<init>(parama);
    paramViewGroup.setOnClickListener(paramView);
    paramViewGroup = h.findViewById(2131362328);
    e = paramViewGroup;
    paramViewGroup = e;
    paramView = new com/truecaller/messaging/conversation/-$$Lambda$e$Bgta4Yu5EecLz-iQcaBiajqIBmA;
    paramView.<init>(paramb);
    paramViewGroup.setOnClickListener(paramView);
    paramViewGroup = h.findViewById(2131362332);
    paramView = new com/truecaller/messaging/conversation/-$$Lambda$e$fypQ8Ns2Q9VqrMyw5oaHbkXxefk;
    paramView.<init>(paramb);
    paramViewGroup.setOnClickListener(paramView);
    paramViewGroup = h.findViewById(2131362312);
    paramView = new com/truecaller/messaging/conversation/-$$Lambda$e$GXbjMO_bM2O8UDKnajoo44DkPrA;
    paramView.<init>(paramb);
    paramViewGroup.setOnClickListener(paramView);
  }
  
  private int b()
  {
    int j = h.getMeasuredHeight();
    int k = i.getMeasuredHeight();
    return -(j + k);
  }
  
  public final void a()
  {
    Object localObject1 = f;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject1 = new android/widget/PopupWindow;
      localObject2 = h;
      j = -1;
      k = -2;
      boolean bool1 = true;
      ((PopupWindow)localObject1).<init>((View)localObject2, j, k, bool1);
      f = ((PopupWindow)localObject1);
      localObject1 = f;
      localObject2 = new android/graphics/drawable/ColorDrawable;
      ((ColorDrawable)localObject2).<init>();
      ((PopupWindow)localObject1).setBackgroundDrawable((Drawable)localObject2);
      localObject1 = f;
      localObject2 = new com/truecaller/messaging/conversation/-$$Lambda$e$30Gc0OhwfkZn49fK2IZ1jmSK9jM;
      ((-..Lambda.e.30Gc0OhwfkZn49fK2IZ1jmSK9jM)localObject2).<init>(this);
      ((PopupWindow)localObject1).setOnDismissListener((PopupWindow.OnDismissListener)localObject2);
    }
    f.setSoftInputMode(16);
    localObject1 = f;
    int n = 2;
    ((PopupWindow)localObject1).setInputMethodMode(n);
    i.getViewTreeObserver().addOnGlobalLayoutListener(this);
    localObject1 = h;
    Object localObject3 = Resources.getSystem().getDisplayMetrics();
    int j = View.MeasureSpec.makeMeasureSpec(widthPixels, 1073741824);
    int k = 0;
    int m = View.MeasureSpec.makeMeasureSpec(0, 0);
    ((View)localObject1).measure(j, m);
    boolean bool2 = g;
    if (bool2)
    {
      localObject1 = f;
      if (localObject1 != null)
      {
        j = 2131951897;
        ((PopupWindow)localObject1).setAnimationStyle(j);
      }
    }
    localObject1 = f;
    localObject3 = i;
    m = b();
    int i1 = 81;
    ((PopupWindow)localObject1).showAsDropDown((View)localObject3, 0, m, i1);
    bool2 = g;
    if (bool2)
    {
      localObject1 = new int[n];
      i.getLocationInWindow((int[])localObject1);
      localObject2 = h;
      localObject3 = new com/truecaller/messaging/conversation/-$$Lambda$e$sBtACrywoEk8A0vpglOAs1KIA6k;
      ((-..Lambda.e.sBtACrywoEk8A0vpglOAs1KIA6k)localObject3).<init>(this, (int[])localObject1);
      ((View)localObject2).post((Runnable)localObject3);
    }
  }
  
  public final void onGlobalLayout()
  {
    PopupWindow localPopupWindow = f;
    if (localPopupWindow != null)
    {
      View localView = i;
      int j = b();
      int k = -1;
      int m = -1;
      localPopupWindow.update(localView, 0, j, k, m);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */