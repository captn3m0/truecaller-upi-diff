package com.truecaller.messaging.conversation;

import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import java.util.Collection;

public abstract interface bi
  extends bh
{
  public abstract void a(ImGroupInfo paramImGroupInfo);
  
  public abstract void a(Message paramMessage);
  
  public abstract void a(Participant[] paramArrayOfParticipant);
  
  public abstract void b(long paramLong);
  
  public abstract void h();
  
  public abstract Message i();
  
  public abstract Collection j();
  
  public abstract Message[] k();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.bi
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */