package com.truecaller.messaging.conversation.emoji;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import c.g.b.k;
import java.util.Arrays;

public final class d
{
  static final AnimatorSet a(Object paramObject, float paramFloat)
  {
    Animator[] arrayOfAnimator = new Animator[2];
    int i = 1;
    float[] arrayOfFloat = new float[i];
    arrayOfFloat[0] = paramFloat;
    Object localObject = ObjectAnimator.ofFloat(paramObject, "scaleX", arrayOfFloat);
    k.a(localObject, "ofFloat(target, \"scaleX\", value)");
    localObject = (Animator)localObject;
    arrayOfAnimator[0] = localObject;
    arrayOfFloat = new float[i];
    arrayOfFloat[0] = paramFloat;
    paramObject = ObjectAnimator.ofFloat(paramObject, "scaleY", arrayOfFloat);
    k.a(paramObject, "ofFloat(target, \"scaleY\", value)");
    paramObject = (Animator)paramObject;
    arrayOfAnimator[i] = paramObject;
    return a(arrayOfAnimator);
  }
  
  static final AnimatorSet a(Animator... paramVarArgs)
  {
    AnimatorSet localAnimatorSet = new android/animation/AnimatorSet;
    localAnimatorSet.<init>();
    int i = paramVarArgs.length;
    paramVarArgs = (Animator[])Arrays.copyOf(paramVarArgs, i);
    localAnimatorSet.playTogether(paramVarArgs);
    return localAnimatorSet;
  }
  
  static final ValueAnimator a()
  {
    Object localObject = new int[1];
    localObject[0] = 0;
    localObject = ValueAnimator.ofInt((int[])localObject);
    k.a(localObject, "this");
    ((ValueAnimator)localObject).setDuration(300L);
    return (ValueAnimator)localObject;
  }
  
  static final AnimatorSet b(Animator... paramVarArgs)
  {
    AnimatorSet localAnimatorSet = new android/animation/AnimatorSet;
    localAnimatorSet.<init>();
    paramVarArgs = (Animator[])Arrays.copyOf(paramVarArgs, 3);
    localAnimatorSet.playSequentially(paramVarArgs);
    return localAnimatorSet;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.emoji.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */