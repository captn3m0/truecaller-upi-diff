package com.truecaller.messaging.conversation.emoji;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import c.g.a.a;
import c.g.b.k;
import c.l;

public final class EmojiPokeView
  extends FrameLayout
{
  private final ImageView a;
  
  public EmojiPokeView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private EmojiPokeView(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet, 0);
    paramAttributeSet = this;
    paramAttributeSet = (ViewGroup)this;
    paramByte = 2131559194;
    FrameLayout.inflate(paramContext, paramByte, paramAttributeSet);
    paramContext = findViewById(2131363373);
    paramAttributeSet = "findViewById(R.id.image_view)";
    k.a(paramContext, paramAttributeSet);
    paramContext = (ImageView)paramContext;
    a = paramContext;
    int i = 4;
    setVisibility(i);
    paramContext = getBackground();
    if (paramContext != null)
    {
      paramContext = paramContext.mutate();
    }
    else
    {
      i = 0;
      paramContext = null;
    }
    setBackground(paramContext);
    boolean bool = isInEditMode();
    if (bool)
    {
      paramContext = a;
      int j = 2131231162;
      paramContext.setImageResource(j);
    }
  }
  
  private final Animator a(boolean paramBoolean)
  {
    boolean bool = true;
    if (paramBoolean == bool)
    {
      localObject = a;
      float[] arrayOfFloat = new float[bool];
      arrayOfFloat[0] = 0.0F;
      return (Animator)ObjectAnimator.ofFloat(localObject, "translationY", arrayOfFloat);
    }
    if (!paramBoolean) {
      return (Animator)d.a(a, 1.0F);
    }
    Object localObject = new c/l;
    ((l)localObject).<init>();
    throw ((Throwable)localObject);
  }
  
  private final Animator b(boolean paramBoolean)
  {
    boolean bool = true;
    if (paramBoolean == bool)
    {
      localObject = a;
      float[] arrayOfFloat = new float[bool];
      float f = getHeight() / 2.0F;
      arrayOfFloat[0] = f;
      return (Animator)ObjectAnimator.ofFloat(localObject, "translationY", arrayOfFloat);
    }
    if (!paramBoolean) {
      return (Animator)d.a(a, 0.0F);
    }
    Object localObject = new c/l;
    ((l)localObject).<init>();
    throw ((Throwable)localObject);
  }
  
  public final void a(int paramInt, boolean paramBoolean, a parama)
  {
    Object localObject1 = a;
    ((ImageView)localObject1).setImageResource(paramInt);
    paramInt = 1065353216;
    float f1 = 1.0F;
    int[] arrayOfInt = null;
    if (paramBoolean)
    {
      i = 1065353216;
      f2 = 1.0F;
    }
    else
    {
      i = 0;
      f2 = 0.0F;
      localImageView = null;
    }
    ((ImageView)localObject1).setScaleX(f2);
    if (paramBoolean)
    {
      i = 1065353216;
      f2 = 1.0F;
    }
    else
    {
      i = 0;
      f2 = 0.0F;
      localImageView = null;
    }
    ((ImageView)localObject1).setScaleY(f2);
    ((ImageView)localObject1).setAlpha(0.0F);
    int i = 1073741824;
    float f2 = 2.0F;
    int j;
    float f3;
    if (paramBoolean)
    {
      j = getHeight();
      f3 = j / f2;
    }
    else
    {
      j = 0;
      f3 = 0.0F;
    }
    ((ImageView)localObject1).setTranslationY(f3);
    localObject1 = getBackground();
    k.a(localObject1, "background");
    ((Drawable)localObject1).setAlpha(0);
    setVisibility(0);
    int k = 3;
    Animator[] arrayOfAnimator1 = new Animator[k];
    Animator[] arrayOfAnimator2 = new Animator[k];
    Object localObject2 = a(paramBoolean);
    Object localObject3 = new android/view/animation/OvershootInterpolator;
    ((OvershootInterpolator)localObject3).<init>(f2);
    localObject3 = (TimeInterpolator)localObject3;
    ((Animator)localObject2).setInterpolator((TimeInterpolator)localObject3);
    k.a(localObject2, "enterAnimation(isSending…ershootInterpolator(2f) }");
    arrayOfAnimator2[0] = localObject2;
    ImageView localImageView = a;
    int m = 1;
    float[] arrayOfFloat = new float[m];
    arrayOfFloat[0] = f1;
    Object localObject4 = ObjectAnimator.ofFloat(localImageView, "alpha", arrayOfFloat);
    k.a(localObject4, "ofFloat(imageView, \"alpha\", 1f)");
    localObject4 = (Animator)localObject4;
    arrayOfAnimator2[m] = localObject4;
    localObject4 = getBackground();
    localObject2 = new int[m];
    localObject2[0] = 76;
    localObject4 = ObjectAnimator.ofInt(localObject4, "alpha", (int[])localObject2);
    k.a(localObject4, "ofInt(background, \"alpha\", BACKGROUND_ALPHA_MAX)");
    localObject4 = (Animator)localObject4;
    i = 2;
    arrayOfAnimator2[i] = localObject4;
    localObject4 = d.a(arrayOfAnimator2);
    long l = 300L;
    ((AnimatorSet)localObject4).setDuration(l);
    localObject4 = (Animator)localObject4;
    arrayOfAnimator1[0] = localObject4;
    localObject4 = d.a();
    k.a(localObject4, "animateDelay(ANIMATION_DURATION)");
    localObject4 = (Animator)localObject4;
    arrayOfAnimator1[m] = localObject4;
    localObject4 = new Animator[k];
    Object localObject5 = b(paramBoolean);
    k.a(localObject5, "exitAnimation(isSending)");
    localObject4[0] = localObject5;
    localObject5 = a;
    arrayOfFloat = new float[m];
    arrayOfFloat[0] = 0.0F;
    localObject5 = ObjectAnimator.ofFloat(localObject5, "alpha", arrayOfFloat);
    k.a(localObject5, "ofFloat(imageView, \"alpha\", 0f)");
    localObject5 = (Animator)localObject5;
    localObject4[m] = localObject5;
    localObject5 = getBackground();
    arrayOfInt = new int[m];
    arrayOfInt[0] = 0;
    localObject5 = ObjectAnimator.ofInt(localObject5, "alpha", arrayOfInt);
    k.a(localObject5, "ofInt(background, \"alpha\", BACKGROUND_ALPHA_MIN)");
    localObject5 = (Animator)localObject5;
    localObject4[i] = localObject5;
    localObject4 = d.a((Animator[])localObject4);
    ((AnimatorSet)localObject4).setDuration(l);
    localObject4 = (Animator)localObject4;
    arrayOfAnimator1[i] = localObject4;
    localObject4 = d.b(arrayOfAnimator1);
    localObject5 = new com/truecaller/messaging/conversation/emoji/EmojiPokeView$a;
    ((EmojiPokeView.a)localObject5).<init>(this, parama);
    localObject5 = (Animator.AnimatorListener)localObject5;
    ((AnimatorSet)localObject4).addListener((Animator.AnimatorListener)localObject5);
    ((AnimatorSet)localObject4).start();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.emoji.EmojiPokeView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */