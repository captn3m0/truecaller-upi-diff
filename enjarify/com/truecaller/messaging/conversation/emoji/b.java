package com.truecaller.messaging.conversation.emoji;

import c.g.b.k;
import com.truecaller.abtest.c;
import com.truecaller.featuretoggles.e;
import com.truecaller.messaging.data.types.Message;

public final class b
  implements a
{
  private long a;
  private final e b;
  private final c c;
  
  public b(e parame, c paramc)
  {
    b = parame;
    c = paramc;
    a = -1;
  }
  
  public final PokeableEmoji a()
  {
    Object localObject = c;
    String str = "defaultPokeEmoji_17105";
    localObject = ((c)localObject).a(str);
    if (localObject != null)
    {
      int i = ((String)localObject).hashCode();
      int j = -311820956;
      boolean bool;
      if (i != j)
      {
        j = 1566945496;
        if (i == j)
        {
          str = "thumbsUp";
          bool = ((String)localObject).equals(str);
          if (bool) {
            return PokeableEmoji.THUMBS_UP;
          }
        }
      }
      else
      {
        str = "foldedHands";
        bool = ((String)localObject).equals(str);
        if (bool) {
          return PokeableEmoji.FOLDED_HANDS;
        }
      }
    }
    return PokeableEmoji.WAVING_HAND;
  }
  
  public final PokeableEmoji a(Message paramMessage)
  {
    k.b(paramMessage, "message");
    Object localObject = b.C();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool1)
    {
      long l1 = paramMessage.a();
      long l2 = a;
      bool1 = l1 < l2;
      if (bool1)
      {
        bool1 = h;
        if (!bool1)
        {
          int i = j;
          int j = 2;
          if (i == j)
          {
            i = f & 0x1;
            if (i == 0)
            {
              l1 = paramMessage.a();
              a = l1;
              paramMessage = paramMessage.j();
              k.a(paramMessage, "message.buildMessageText()");
              localObject = PokeableEmoji.values();
              j = localObject.length;
              int k = 0;
              while (k < j)
              {
                PokeableEmoji localPokeableEmoji = localObject[k];
                String str = localPokeableEmoji.getEmoji();
                boolean bool2 = k.a(str, paramMessage);
                if (bool2) {
                  return localPokeableEmoji;
                }
                k += 1;
              }
              return null;
            }
          }
        }
      }
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.emoji.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */