package com.truecaller.messaging.conversation.emoji;

public enum PokeableEmoji
{
  private final String analyticsName;
  private final String emoji;
  private final int largeRes;
  private final int smallRes;
  
  static
  {
    PokeableEmoji[] arrayOfPokeableEmoji = new PokeableEmoji[3];
    PokeableEmoji localPokeableEmoji1 = new com/truecaller/messaging/conversation/emoji/PokeableEmoji;
    PokeableEmoji localPokeableEmoji2 = localPokeableEmoji1;
    localPokeableEmoji1.<init>("WAVING_HAND", 0, "👋", 2131231884, 2131231162, "WavingHand");
    WAVING_HAND = localPokeableEmoji1;
    arrayOfPokeableEmoji[0] = localPokeableEmoji1;
    localPokeableEmoji2 = new com/truecaller/messaging/conversation/emoji/PokeableEmoji;
    localPokeableEmoji2.<init>("THUMBS_UP", 1, "👍", 2131231896, 2131231161, "ThumbsUp");
    THUMBS_UP = localPokeableEmoji2;
    arrayOfPokeableEmoji[1] = localPokeableEmoji2;
    localPokeableEmoji2 = new com/truecaller/messaging/conversation/emoji/PokeableEmoji;
    localPokeableEmoji2.<init>("FOLDED_HANDS", 2, "🙏", 2131232843, 2131231160, "FoldedHands");
    FOLDED_HANDS = localPokeableEmoji2;
    arrayOfPokeableEmoji[2] = localPokeableEmoji2;
    $VALUES = arrayOfPokeableEmoji;
  }
  
  private PokeableEmoji(String paramString2, int paramInt2, int paramInt3, String paramString3)
  {
    emoji = paramString2;
    smallRes = paramInt2;
    largeRes = paramInt3;
    analyticsName = paramString3;
  }
  
  public final String getAnalyticsName()
  {
    return analyticsName;
  }
  
  public final String getEmoji()
  {
    return emoji;
  }
  
  public final int getLargeRes()
  {
    return largeRes;
  }
  
  public final int getSmallRes()
  {
    return smallRes;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.emoji.PokeableEmoji
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */