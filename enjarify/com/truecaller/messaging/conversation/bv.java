package com.truecaller.messaging.conversation;

import android.graphics.drawable.Drawable;

public final class bv
{
  final int a;
  final int b;
  final Drawable c;
  final Drawable d;
  final String e;
  final String f;
  final String g;
  
  public bv(int paramInt1, int paramInt2, Drawable paramDrawable1, Drawable paramDrawable2, String paramString1, String paramString2, String paramString3)
  {
    a = paramInt1;
    b = paramInt2;
    c = paramDrawable1;
    d = paramDrawable2;
    e = paramString1;
    f = paramString2;
    g = paramString3;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.bv
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */