package com.truecaller.messaging.conversation;

import android.animation.TimeInterpolator;
import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.ViewPropertyAnimator;
import android.view.animation.LinearInterpolator;

public final class EmojiPokeButton
  extends AppCompatImageView
{
  private final TimeInterpolator a;
  
  public EmojiPokeButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private EmojiPokeButton(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    paramContext = new android/view/animation/LinearInterpolator;
    paramContext.<init>();
    paramContext = (TimeInterpolator)paramContext;
    a = paramContext;
  }
  
  public final void setVisible(boolean paramBoolean)
  {
    long l = 150L;
    if (paramBoolean)
    {
      setVisibility(0);
      localViewPropertyAnimator = animate();
      f = 1.0F;
      localViewPropertyAnimator = localViewPropertyAnimator.scaleX(f).scaleY(f).setDuration(l).alpha(f);
      localObject = a;
      localViewPropertyAnimator.setInterpolator((TimeInterpolator)localObject).start();
      return;
    }
    ViewPropertyAnimator localViewPropertyAnimator = animate();
    float f = 0.5F;
    localViewPropertyAnimator = localViewPropertyAnimator.scaleX(f).scaleY(f).setDuration(l).alpha(0.0F);
    Object localObject = a;
    localViewPropertyAnimator = localViewPropertyAnimator.setInterpolator((TimeInterpolator)localObject);
    localObject = new com/truecaller/messaging/conversation/EmojiPokeButton$a;
    ((EmojiPokeButton.a)localObject).<init>(this);
    localObject = (Runnable)localObject;
    localViewPropertyAnimator.withEndAction((Runnable)localObject).start();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.EmojiPokeButton
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */