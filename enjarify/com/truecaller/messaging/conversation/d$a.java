package com.truecaller.messaging.conversation;

import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Reaction;

public final class d$a
{
  Reaction[] A;
  boolean B;
  boolean C;
  boolean D;
  int E;
  cj a;
  AttachmentType b;
  Message c;
  Entity d;
  String e;
  int f;
  int g;
  int h;
  String i;
  int j;
  String k;
  boolean l;
  boolean m;
  int n;
  boolean o;
  boolean p;
  boolean q;
  boolean r;
  int s;
  int t;
  String u;
  String v;
  String w;
  boolean x;
  boolean y;
  boolean z;
  
  public final a a(int paramInt)
  {
    f = paramInt;
    return this;
  }
  
  public final a a(AttachmentType paramAttachmentType)
  {
    b = paramAttachmentType;
    return this;
  }
  
  public final a a(cj paramcj)
  {
    a = paramcj;
    return this;
  }
  
  public final a a(Entity paramEntity)
  {
    d = paramEntity;
    boolean bool = false;
    if (paramEntity != null)
    {
      int i1 = i;
      int i2 = 1;
      if (i1 == i2) {
        i1 = 1;
      } else {
        i1 = 0;
      }
      o = i1;
      i1 = i;
      int i3 = 2;
      if (i1 != i3)
      {
        i1 = i;
        int i4 = 3;
        if (i1 != i4)
        {
          i1 = 0;
          break label75;
        }
      }
      i1 = 1;
      label75:
      p = i1;
      i1 = i;
      if (i1 != i3)
      {
        i1 = i;
        i3 = 4;
        if (i1 != i3)
        {
          int i5 = i;
          i1 = 5;
          if (i5 != i1) {
            break label121;
          }
        }
      }
      bool = true;
      label121:
      r = bool;
    }
    else
    {
      p = false;
      o = false;
    }
    return this;
  }
  
  public final a a(Message paramMessage)
  {
    c = paramMessage;
    return this;
  }
  
  public final a a(String paramString)
  {
    e = paramString;
    return this;
  }
  
  public final a a(boolean paramBoolean)
  {
    m = paramBoolean;
    return this;
  }
  
  public final a a(boolean paramBoolean1, boolean paramBoolean2)
  {
    p = paramBoolean1;
    r = paramBoolean2;
    return this;
  }
  
  public final a a(Reaction[] paramArrayOfReaction)
  {
    A = paramArrayOfReaction;
    return this;
  }
  
  public final d a()
  {
    d locald = new com/truecaller/messaging/conversation/d;
    locald.<init>(this, (byte)0);
    return locald;
  }
  
  public final a b(int paramInt)
  {
    n = paramInt;
    return this;
  }
  
  public final a b(String paramString)
  {
    k = paramString;
    return this;
  }
  
  public final a b(boolean paramBoolean)
  {
    l = paramBoolean;
    return this;
  }
  
  public final a c(int paramInt)
  {
    g = paramInt;
    return this;
  }
  
  public final a c(String paramString)
  {
    u = paramString;
    return this;
  }
  
  public final a c(boolean paramBoolean)
  {
    o = paramBoolean;
    return this;
  }
  
  public final a d(int paramInt)
  {
    h = paramInt;
    return this;
  }
  
  public final a d(String paramString)
  {
    i = paramString;
    return this;
  }
  
  public final a d(boolean paramBoolean)
  {
    q = paramBoolean;
    return this;
  }
  
  public final a e(int paramInt)
  {
    j = paramInt;
    return this;
  }
  
  public final a e(String paramString)
  {
    v = paramString;
    return this;
  }
  
  public final a e(boolean paramBoolean)
  {
    x = paramBoolean;
    return this;
  }
  
  public final a f(int paramInt)
  {
    s = paramInt;
    return this;
  }
  
  public final a f(String paramString)
  {
    w = paramString;
    return this;
  }
  
  public final a f(boolean paramBoolean)
  {
    y = paramBoolean;
    return this;
  }
  
  public final a g(int paramInt)
  {
    t = paramInt;
    return this;
  }
  
  public final a g(boolean paramBoolean)
  {
    z = paramBoolean;
    return this;
  }
  
  public final a h(int paramInt)
  {
    E = paramInt;
    return this;
  }
  
  public final a h(boolean paramBoolean)
  {
    B = paramBoolean;
    return this;
  }
  
  public final a i(boolean paramBoolean)
  {
    C = paramBoolean;
    return this;
  }
  
  public final a j(boolean paramBoolean)
  {
    D = paramBoolean;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.d.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */