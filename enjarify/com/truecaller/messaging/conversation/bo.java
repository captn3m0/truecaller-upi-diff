package com.truecaller.messaging.conversation;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import c.n;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.truecaller.analytics.b;
import com.truecaller.analytics.e;
import com.truecaller.analytics.e.a;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.f;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.messaging.d;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.util.aa;
import com.truecaller.util.af;
import com.truecaller.util.al;
import com.truecaller.util.az;
import com.truecaller.util.az.a;
import com.truecaller.util.az.c;
import com.truecaller.util.ba;
import com.truecaller.util.bd;
import com.truecaller.util.bg;
import com.truecaller.util.ch;
import com.truecaller.util.de;
import com.truecaller.util.x;
import com.truecaller.utils.l;
import com.truecaller.utils.o;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.c.a.a.a.a;
import org.c.a.a.a.k;

final class bo
  extends bn
{
  private final i a;
  private final f c;
  private final bg d;
  private final al e;
  private final ContentResolver f;
  private final o g;
  private final f h;
  private final b i;
  private final List j;
  private final List k;
  private final ch l;
  private final l m;
  private af n;
  private Uri o;
  private boolean p;
  private int q;
  private final HashMap r;
  private bd s;
  private final int t;
  private bn.a u;
  private Runnable v;
  
  bo(i parami, f paramf1, bg parambg, al paramal, ContentResolver paramContentResolver, o paramo, f paramf2, b paramb, ch paramch, l paraml, af paramaf, bd parambd)
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    j = ((List)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    k = ((List)localObject);
    q = 3;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    r = ((HashMap)localObject);
    a = parami;
    c = paramf1;
    d = parambg;
    e = paramal;
    f = paramContentResolver;
    g = paramo;
    h = paramf2;
    i = paramb;
    l = paramch;
    int i1 = g.b(2131165653);
    t = i1;
    m = paraml;
    n = paramaf;
    s = parambd;
  }
  
  private int a(Uri paramUri)
  {
    bd localbd = s;
    paramUri = localbd.a(paramUri);
    if (paramUri != null) {
      return c;
    }
    return -1;
  }
  
  private void a(Collection paramCollection)
  {
    Object localObject1 = b;
    if (localObject1 == null) {
      return;
    }
    paramCollection = paramCollection.iterator();
    for (;;)
    {
      boolean bool1 = paramCollection.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (d)paramCollection.next();
      Object localObject2 = b;
      boolean bool2 = k.c((CharSequence)localObject2);
      Uri localUri;
      if (bool2)
      {
        localObject2 = b;
      }
      else
      {
        localObject2 = d;
        localUri = a;
        localObject2 = ((bg)localObject2).a(localUri);
      }
      if (localObject2 != null)
      {
        boolean bool3 = Entity.d((String)localObject2);
        int i1;
        if (bool3)
        {
          localUri = a;
          i1 = a(localUri);
        }
        else
        {
          i1 = -1;
        }
        List localList = k;
        bo.a locala = new com/truecaller/messaging/conversation/bo$a;
        localObject1 = a.toString();
        localObject1 = (BinaryEntity)Entity.a((String)localObject2, (String)localObject1, i1);
        bool2 = false;
        localObject2 = null;
        locala.<init>((BinaryEntity)localObject1, false, (byte)0);
        localList.add(locala);
      }
    }
    o();
    ((bp)b).A();
    ((bp)b).i(true);
  }
  
  private void a(boolean paramBoolean, Collection paramCollection)
  {
    if (paramBoolean) {
      a(paramCollection);
    }
  }
  
  private void b(boolean paramBoolean)
  {
    Object localObject1 = b;
    if (localObject1 == null) {
      return;
    }
    if (paramBoolean)
    {
      localObject1 = n();
    }
    else
    {
      localObject1 = new android/content/Intent;
      localObject2 = "android.media.action.IMAGE_CAPTURE";
      ((Intent)localObject1).<init>((String)localObject2);
    }
    Object localObject2 = (bp)b;
    ((bp)localObject2).K();
    p = paramBoolean;
    boolean bool = m();
    if (bool)
    {
      localObject2 = e;
      bool = ((al)localObject2).b((Intent)localObject1);
      if (bool)
      {
        int i2;
        if (paramBoolean)
        {
          localObject2 = (bp)b;
          i2 = 101;
          ((bp)localObject2).startActivityForResult((Intent)localObject1, i2);
        }
        else
        {
          localObject2 = e;
          i2 = 1000;
          localObject3 = Integer.valueOf(i2);
          localObject2 = ((al)localObject2).a((Intent)localObject1, (Integer)localObject3);
          o = ((Uri)localObject2);
          localObject2 = o;
          if (localObject2 != null)
          {
            localObject3 = "output";
            ((Intent)localObject1).putExtra((String)localObject3, (Parcelable)localObject2);
            localObject2 = (bp)b;
            i2 = 100;
            ((bp)localObject2).startActivityForResult((Intent)localObject1, i2);
          }
        }
      }
      else
      {
        localObject1 = (bp)b;
        int i1 = 2131887195;
        ((bp)localObject1).m(i1);
      }
    }
    localObject1 = i;
    localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("ViewAction");
    localObject2 = ((e.a)localObject2).a("Action", "attachment");
    String str = "conversation";
    localObject2 = ((e.a)localObject2).a("Context", str);
    Object localObject3 = "SubAction";
    if (paramBoolean) {
      localObject4 = "video";
    } else {
      localObject4 = "photo";
    }
    Object localObject4 = ((e.a)localObject2).a((String)localObject3, (String)localObject4).a();
    ((b)localObject1).a((e)localObject4);
  }
  
  private boolean l()
  {
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    Object localObject2 = j.iterator();
    Object localObject3;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject2).hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = (bo.a)((Iterator)localObject2).next();
      Object localObject4 = a;
      boolean bool2 = ((BinaryEntity)localObject4).d();
      if (bool2)
      {
        localObject3 = a.b;
        localObject4 = r;
        bool2 = ((HashMap)localObject4).containsKey(localObject3);
        if (!bool2) {
          ((ArrayList)localObject1).add(localObject3);
        }
      }
    }
    boolean bool3 = ((ArrayList)localObject1).isEmpty();
    if (!bool3)
    {
      localObject1 = ((aa)h.a()).a((List)localObject1);
      localObject2 = a;
      localObject3 = new com/truecaller/messaging/conversation/-$$Lambda$jlscwXK0-DTpEPaRJlq_OcU_BKA;
      ((-..Lambda.jlscwXK0-DTpEPaRJlq_OcU_BKA)localObject3).<init>(this);
      ((w)localObject1).a((i)localObject2, (ac)localObject3);
      return true;
    }
    return false;
  }
  
  private boolean m()
  {
    Object localObject = m;
    String[] arrayOfString = { "android.permission.CAMERA" };
    boolean bool = ((l)localObject).a(arrayOfString);
    arrayOfString = null;
    if (!bool)
    {
      localObject = (bp)b;
      str = "android.permission.CAMERA";
      bool = ((bp)localObject).k(str);
      if (bool)
      {
        localObject = (bp)b;
        ((bp)localObject).H();
      }
      else
      {
        localObject = (bp)b;
        ((bp)localObject).I();
      }
      return false;
    }
    localObject = m;
    bool = ((l)localObject).b();
    if (bool)
    {
      localObject = m;
      bool = ((l)localObject).c();
      if (bool) {
        return true;
      }
    }
    localObject = (bp)b;
    String str = "android.permission.READ_EXTERNAL_STORAGE";
    bool = ((bp)localObject).k(str);
    if (!bool)
    {
      localObject = (bp)b;
      str = "android.permission.WRITE_EXTERNAL_STORAGE";
      bool = ((bp)localObject).k(str);
      if (!bool)
      {
        localObject = (bp)b;
        int i1 = 11;
        ((bp)localObject).q(i1);
        break label218;
      }
    }
    localObject = (bp)b;
    ((bp)localObject).E();
    label218:
    return false;
  }
  
  private Intent n()
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.media.action.VIDEO_CAPTURE");
    bg localbg = d;
    int i1 = q;
    long l1 = localbg.a(i1);
    int i2 = q;
    int i3 = 2;
    TimeUnit localTimeUnit;
    Object localObject;
    if (i2 != i3)
    {
      localTimeUnit = null;
      localIntent.putExtra("android.intent.extra.videoQuality", 0);
      localObject = "sms";
      i3 = 1;
      localIntent.putExtra((String)localObject, i3);
      long l2 = 0L;
      boolean bool = l1 < l2;
      if (bool)
      {
        localObject = "android.intent.extra.sizeLimit";
        localIntent.putExtra((String)localObject, l1);
      }
    }
    else
    {
      localObject = TimeUnit.SECONDS;
      l1 = d.a(l1);
      l1 = ((TimeUnit)localObject).toMinutes(l1);
      localObject = "android.intent.extra.durationLimit";
      localTimeUnit = TimeUnit.MINUTES;
      l1 = localTimeUnit.toSeconds(l1);
      i1 = (int)l1;
      localIntent.putExtra((String)localObject, i1);
    }
    return localIntent;
  }
  
  private void o()
  {
    bn.a locala = u;
    if (locala != null) {
      locala.K();
    }
  }
  
  public final int a()
  {
    int i1 = j.size();
    int i2 = k.size();
    return i1 + i2;
  }
  
  public final int a(int paramInt)
  {
    return 0;
  }
  
  final void a(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Uri localUri = null;
    int i1 = -1;
    boolean bool = true;
    Object localObject1;
    Object localObject2;
    switch (paramInt1)
    {
    default: 
      break;
    case 101: 
      if (paramIntent != null) {
        localUri = paramIntent.getData();
      }
      if (localUri != null)
      {
        if (paramInt2 == i1)
        {
          localObject1 = new com/truecaller/messaging/d;
          ((d)localObject1).<init>(localUri, (byte)0);
          localObject1 = Collections.singletonList(localObject1);
          a(bool, (Collection)localObject1);
          localObject1 = (ba)c.a();
          localObject2 = d;
          int i2 = q;
          long l1 = ((bg)localObject2).a(i2);
          localObject1 = ((ba)localObject1).a(localUri, bool, l1);
          localObject2 = a;
          paramIntent = new com/truecaller/messaging/conversation/-$$Lambda$3ej_sBp360r7qdGiwyfZ0UPxYmg;
          paramIntent.<init>(this);
          ((w)localObject1).a((i)localObject2, paramIntent);
          return;
        }
        localObject1 = l;
        ((ch)localObject1).a(localUri);
      }
      break;
    case 100: 
      localObject1 = o;
      if (localObject1 != null)
      {
        if (paramInt2 == i1)
        {
          localObject2 = new com/truecaller/messaging/d;
          ((d)localObject2).<init>((Uri)localObject1, (byte)0);
          localObject2 = Collections.singletonList(localObject2);
          a(bool, (Collection)localObject2);
          localObject1 = ((ba)c.a()).a((Uri)localObject1, bool);
          localObject2 = a;
          paramIntent = new com/truecaller/messaging/conversation/-$$Lambda$3ej_sBp360r7qdGiwyfZ0UPxYmg;
          paramIntent.<init>(this);
          ((w)localObject1).a((i)localObject2, paramIntent);
        }
        else
        {
          localObject2 = l;
          ((ch)localObject2).a((Uri)localObject1);
        }
        o = null;
        return;
      }
      break;
    }
  }
  
  final void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    int i1 = 4;
    l locall;
    String[] arrayOfString;
    if (paramInt != i1)
    {
      i1 = 11;
      if (paramInt == i1)
      {
        locall = m;
        String str = "android.permission.WRITE_EXTERNAL_STORAGE";
        arrayOfString = new String[] { "android.permission.READ_EXTERNAL_STORAGE", str };
        paramInt = locall.a(paramArrayOfString, paramArrayOfInt, arrayOfString);
        if (paramInt != 0)
        {
          paramInt = p;
          b(paramInt);
        }
      }
    }
    else
    {
      locall = m;
      arrayOfString = new String[] { "android.permission.CAMERA" };
      paramInt = locall.a(paramArrayOfString, paramArrayOfInt, arrayOfString);
      if (paramInt != 0)
      {
        paramInt = p;
        b(paramInt);
        return;
      }
    }
  }
  
  public final void a(Uri paramUri, String paramString, Runnable paramRunnable)
  {
    v = paramRunnable;
    paramRunnable = new com/truecaller/messaging/d;
    paramRunnable.<init>(paramUri, paramString);
    paramUri = Collections.singletonList(paramRunnable);
    paramString = d;
    int i1 = q;
    paramString = Long.valueOf(paramString.a(i1));
    a(paramUri, paramString, false);
  }
  
  final void a(Bundle paramBundle)
  {
    if (paramBundle != null)
    {
      Object localObject = (Uri)paramBundle.getParcelable("camera_output_uri");
      o = ((Uri)localObject);
      localObject = "transport_type";
      int i1 = paramBundle.getInt((String)localObject);
      q = i1;
    }
  }
  
  final void a(n paramn)
  {
    paramn = Collections.singletonList(paramn);
    b(paramn);
  }
  
  final void a(bn.a parama)
  {
    u = parama;
  }
  
  public final void a(x paramx)
  {
    Object localObject1 = a;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = a.toString();
    long l1 = -1;
    localObject1 = Entity.a("text/x-vcard", 0, (String)localObject2, l1);
    boolean bool = paramx.a();
    if (bool)
    {
      localObject2 = r;
      Uri localUri = a;
      ((HashMap)localObject2).put(localUri, paramx);
    }
    paramx = new BinaryEntity[1];
    localObject1 = (BinaryEntity)localObject1;
    paramx[0] = localObject1;
    a(false, paramx);
  }
  
  final void a(Collection paramCollection, Long paramLong, boolean paramBoolean)
  {
    boolean bool = paramCollection.isEmpty();
    if (bool) {
      return;
    }
    a(paramBoolean, paramCollection);
    long l1;
    if (paramLong != null)
    {
      l1 = paramLong.longValue();
    }
    else
    {
      paramLong = d;
      paramBoolean = q;
      l1 = paramLong.a(paramBoolean);
    }
    paramCollection = ((ba)c.a()).a(paramCollection, l1);
    paramLong = a;
    -..Lambda.O8Vqn1gjmqFAHtX8BCsQ3k3u6DA localO8Vqn1gjmqFAHtX8BCsQ3k3u6DA = new com/truecaller/messaging/conversation/-$$Lambda$O8Vqn1gjmqFAHtX8BCsQ3k3u6DA;
    localO8Vqn1gjmqFAHtX8BCsQ3k3u6DA.<init>(this);
    paramCollection.a(paramLong, localO8Vqn1gjmqFAHtX8BCsQ3k3u6DA);
  }
  
  final void a(List paramList)
  {
    Iterator localIterator1 = j.iterator();
    for (;;)
    {
      boolean bool1 = localIterator1.hasNext();
      if (!bool1) {
        break;
      }
      bo.a locala = (bo.a)localIterator1.next();
      Iterator localIterator2 = paramList.iterator();
      boolean bool2;
      do
      {
        bool2 = localIterator2.hasNext();
        if (!bool2) {
          break;
        }
        BinaryEntity localBinaryEntity1 = (BinaryEntity)localIterator2.next();
        BinaryEntity localBinaryEntity2 = a;
        bool2 = localBinaryEntity2.equals(localBinaryEntity1);
      } while (!bool2);
      localIterator1.remove();
    }
    o();
    paramList = b;
    if (paramList != null)
    {
      ((bp)b).A();
      paramList = j;
      boolean bool3 = paramList.isEmpty();
      if (bool3)
      {
        paramList = (bp)b;
        localIterator1 = null;
        paramList.i(false);
      }
    }
  }
  
  final void a(Map paramMap)
  {
    boolean bool;
    if (paramMap != null)
    {
      bool = paramMap.isEmpty();
      if (!bool)
      {
        HashMap localHashMap = r;
        localHashMap.putAll(paramMap);
      }
    }
    o();
    paramMap = b;
    if (paramMap != null)
    {
      ((bp)b).A();
      paramMap = (bp)b;
      bool = true;
      paramMap.i(bool);
    }
  }
  
  final void a(boolean paramBoolean)
  {
    boolean bool;
    Object localObject2;
    if (paramBoolean)
    {
      localObject1 = j.iterator();
      for (;;)
      {
        bool = ((Iterator)localObject1).hasNext();
        if (!bool) {
          break;
        }
        localObject2 = (bo.a)((Iterator)localObject1).next();
        ch localch = l;
        localObject2 = a;
        localch.a((BinaryEntity)localObject2);
      }
    }
    j.clear();
    r.clear();
    k.clear();
    o();
    Object localObject1 = b;
    if (localObject1 != null)
    {
      ((bp)b).A();
      localObject1 = (bp)b;
      bool = false;
      localObject2 = null;
      ((bp)localObject1).i(false);
    }
  }
  
  public final void a(boolean paramBoolean, BinaryEntity... paramVarArgs)
  {
    int i1 = paramVarArgs.length;
    if (i1 > 0)
    {
      i1 = paramVarArgs.length;
      int i2 = 0;
      while (i2 < i1)
      {
        BinaryEntity localBinaryEntity = paramVarArgs[i2];
        List localList = j;
        bo.a locala = new com/truecaller/messaging/conversation/bo$a;
        locala.<init>(localBinaryEntity, paramBoolean, (byte)0);
        localList.add(locala);
        i2 += 1;
      }
      paramBoolean = l();
      if (!paramBoolean)
      {
        Object localObject = b;
        if (localObject != null)
        {
          o();
          ((bp)b).A();
          localObject = (bp)b;
          boolean bool = true;
          ((bp)localObject).i(bool);
        }
      }
    }
  }
  
  public final long b(int paramInt)
  {
    return 0L;
  }
  
  public final void b()
  {
    Uri localUri = o;
    if (localUri != null)
    {
      ch localch = l;
      localch.a(localUri);
      localUri = null;
      o = null;
    }
    b(false);
  }
  
  final void b(Bundle paramBundle)
  {
    Uri localUri = o;
    paramBundle.putParcelable("camera_output_uri", localUri);
    int i1 = q;
    paramBundle.putInt("transport_type", i1);
  }
  
  final void b(List paramList)
  {
    Object localObject1 = v;
    boolean bool1 = false;
    BinaryEntity localBinaryEntity1 = null;
    if (localObject1 != null)
    {
      ((Runnable)localObject1).run();
      v = null;
    }
    localObject1 = b;
    if (localObject1 == null) {
      return;
    }
    int i1 = 2131886439;
    if (paramList == null)
    {
      ((bp)b).p(i1);
      return;
    }
    Object localObject2 = new java/util/ArrayList;
    int i2 = paramList.size();
    ((ArrayList)localObject2).<init>(i2);
    long l1 = 0L;
    paramList = paramList.iterator();
    long l2 = l1;
    i2 = 0;
    bo.a locala = null;
    int i3 = 0;
    int i4 = 0;
    Object localObject3 = null;
    for (;;)
    {
      boolean bool2 = paramList.hasNext();
      if (!bool2) {
        break;
      }
      Object localObject4 = (n)paramList.next();
      BinaryEntity localBinaryEntity2;
      boolean bool3;
      if (localObject4 != null)
      {
        localBinaryEntity2 = (BinaryEntity)a;
      }
      else
      {
        bool3 = false;
        localBinaryEntity2 = null;
      }
      if (localObject4 != null)
      {
        localObject4 = (az)b;
      }
      else
      {
        bool2 = false;
        localObject4 = null;
      }
      if ((localBinaryEntity2 != null) && (localObject4 == null))
      {
        ((List)localObject2).add(localBinaryEntity2);
      }
      else
      {
        bool3 = localObject4 instanceof az.a;
        if (bool3)
        {
          i2 += 1;
          localObject4 = (az.a)localObject4;
          l2 = a;
        }
        else
        {
          bool2 = localObject4 instanceof az.c;
          if (bool2) {
            i3 += 1;
          } else {
            i4 += 1;
          }
        }
      }
    }
    if (i2 > 0)
    {
      paramList = b;
      localObject3 = paramList;
      localObject3 = (bp)paramList;
      paramList = d;
      long l3 = paramList.a(l2);
      int i5 = q;
      ((bp)localObject3).a(l2, l3, i5);
    }
    else if (i3 > 0)
    {
      paramList = (bp)b;
      i1 = 2131886441;
      paramList.p(i1);
    }
    else if (i4 > 0)
    {
      paramList = (bp)b;
      paramList.p(i1);
    }
    paramList = k;
    int i6 = paramList.isEmpty();
    if (i6 == 0)
    {
      localObject1 = k;
      ((List)localObject1).clear();
    }
    i1 = 1;
    i6 ^= i1;
    bool1 = ((List)localObject2).isEmpty();
    if (!bool1)
    {
      paramList = ((List)localObject2).iterator();
      for (;;)
      {
        bool1 = paramList.hasNext();
        if (!bool1) {
          break;
        }
        localBinaryEntity1 = (BinaryEntity)paramList.next();
        localObject2 = j;
        locala = new com/truecaller/messaging/conversation/bo$a;
        locala.<init>(localBinaryEntity1, false, (byte)0);
        ((List)localObject2).add(locala);
      }
      o();
      ((bp)b).A();
      ((bp)b).i(i1);
      return;
    }
    if (i6 != 0)
    {
      o();
      ((bp)b).A();
      paramList = j;
      boolean bool4 = paramList.isEmpty();
      if (bool4)
      {
        paramList = (bp)b;
        paramList.i(false);
      }
    }
  }
  
  public final void c()
  {
    b(true);
  }
  
  final void c(int paramInt)
  {
    Object localObject1 = j.get(paramInt)).a;
    boolean bool = c;
    if (bool)
    {
      ch localch = l;
      localch.a((BinaryEntity)localObject1);
    }
    localObject1 = j;
    ((List)localObject1).remove(paramInt);
    o();
    Object localObject2 = b;
    if (localObject2 != null)
    {
      ((bp)b).A();
      localObject2 = j;
      paramInt = ((List)localObject2).isEmpty();
      if (paramInt != 0)
      {
        localObject2 = k;
        paramInt = ((List)localObject2).isEmpty();
        if (paramInt != 0)
        {
          localObject2 = (bp)b;
          localObject1 = null;
          ((bp)localObject2).i(false);
        }
      }
    }
  }
  
  final void d(int paramInt)
  {
    q = paramInt;
  }
  
  final void e()
  {
    Iterator localIterator = j.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      bo.a locala = (bo.a)localIterator.next();
      boolean bool2 = true;
      b = bool2;
    }
  }
  
  final void f()
  {
    List localList = j;
    -..Lambda.bo.tUtCC68kxpjEBfkS1RjXcEFsECE localtUtCC68kxpjEBfkS1RjXcEFsECE = -..Lambda.bo.tUtCC68kxpjEBfkS1RjXcEFsECE.INSTANCE;
    Iterables.removeIf(localList, localtUtCC68kxpjEBfkS1RjXcEFsECE);
  }
  
  final Collection g()
  {
    List localList = j;
    -..Lambda.bo.NNKNTHuIwwBOFPM5CCVqbvcJER4 localNNKNTHuIwwBOFPM5CCVqbvcJER4 = -..Lambda.bo.NNKNTHuIwwBOFPM5CCVqbvcJER4.INSTANCE;
    return Collections2.transform(localList, localNNKNTHuIwwBOFPM5CCVqbvcJER4);
  }
  
  final boolean h()
  {
    List localList = j;
    int i1 = localList.size();
    return i1 > 0;
  }
  
  final boolean i()
  {
    List localList = k;
    int i1 = localList.size();
    return i1 > 0;
  }
  
  final void j()
  {
    u = null;
  }
  
  public final String[] k()
  {
    String[] arrayOfString1 = Entity.g;
    String[] arrayOfString2 = Entity.f;
    return (String[])a.a(arrayOfString1, arrayOfString2);
  }
  
  public final void y_()
  {
    super.y_();
    Runnable localRunnable = v;
    if (localRunnable != null)
    {
      localRunnable.run();
      localRunnable = null;
      v = null;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.bo
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */