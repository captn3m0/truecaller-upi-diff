package com.truecaller.messaging.conversation;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView.Recycler;
import android.support.v7.widget.RecyclerView.State;

final class k$6
  extends LinearLayoutManager
{
  k$6(k paramk, Context paramContext)
  {
    super(paramContext);
  }
  
  public final int scrollVerticallyBy(int paramInt, RecyclerView.Recycler paramRecycler, RecyclerView.State paramState)
  {
    int i = super.scrollVerticallyBy(paramInt, paramRecycler, paramState);
    a.a.a(paramInt, i);
    return i;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.k.6
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */