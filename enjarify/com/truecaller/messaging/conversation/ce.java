package com.truecaller.messaging.conversation;

import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;

public final class ce
{
  static final Drawable a(Drawable paramDrawable, int paramInt)
  {
    PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
    paramDrawable.setColorFilter(paramInt, localMode);
    return paramDrawable;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.ce
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */