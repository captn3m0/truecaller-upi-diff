package com.truecaller.messaging.conversation;

import dagger.a.d;
import javax.inject.Provider;

public final class al
  implements d
{
  private final s a;
  private final Provider b;
  private final Provider c;
  
  private al(s params, Provider paramProvider1, Provider paramProvider2)
  {
    a = params;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static al a(s params, Provider paramProvider1, Provider paramProvider2)
  {
    al localal = new com/truecaller/messaging/conversation/al;
    localal.<init>(params, paramProvider1, paramProvider2);
    return localal;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.al
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */