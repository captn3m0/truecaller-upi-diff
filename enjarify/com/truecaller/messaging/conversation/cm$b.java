package com.truecaller.messaging.conversation;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import c.g.b.k;

final class cm$b
  implements View.OnTouchListener
{
  cm$b(cm paramcm, RecyclerView.ViewHolder paramViewHolder) {}
  
  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    paramView = a;
    String str = "event";
    k.a(paramMotionEvent, str);
    int i = paramMotionEvent.getAction();
    int j = 1;
    int k = 3;
    if (i != k)
    {
      int m = paramMotionEvent.getAction();
      if (m != j) {
        j = 0;
      }
    }
    cm.a(paramView, j);
    paramView = a;
    boolean bool = cm.a(paramView);
    if (bool)
    {
      paramView = cm.c(a);
      float f1 = Math.abs(paramView.getTranslationX());
      paramMotionEvent = a;
      int n = cm.b(paramMotionEvent);
      float f2 = n;
      bool = f1 < f2;
      if (!bool)
      {
        paramView = b;
        int i1 = paramView.getAdapterPosition();
        n = -1;
        f2 = 0.0F / 0.0F;
        if (i1 != n)
        {
          paramView = cm.d(a);
          paramMotionEvent = b;
          n = paramMotionEvent.getAdapterPosition();
          paramView.onSwipeToReply(n);
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.cm.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */