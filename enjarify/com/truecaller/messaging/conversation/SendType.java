package com.truecaller.messaging.conversation;

public enum SendType
{
  static
  {
    SendType[] arrayOfSendType = new SendType[3];
    SendType localSendType = new com/truecaller/messaging/conversation/SendType;
    localSendType.<init>("DEFAULT", 0);
    DEFAULT = localSendType;
    arrayOfSendType[0] = localSendType;
    localSendType = new com/truecaller/messaging/conversation/SendType;
    int i = 1;
    localSendType.<init>("SMS", i);
    SMS = localSendType;
    arrayOfSendType[i] = localSendType;
    localSendType = new com/truecaller/messaging/conversation/SendType;
    i = 2;
    localSendType.<init>("IM", i);
    IM = localSendType;
    arrayOfSendType[i] = localSendType;
    $VALUES = arrayOfSendType;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.SendType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */