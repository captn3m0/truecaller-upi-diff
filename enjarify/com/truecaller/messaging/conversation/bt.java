package com.truecaller.messaging.conversation;

import android.graphics.drawable.Drawable;
import c.g.b.k;
import com.google.c.a.k.d;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.ae;
import com.truecaller.utils.o;

public final class bt
  implements bs
{
  private final o a;
  private final boolean b;
  private final ae c;
  
  public bt(o paramo, boolean paramBoolean, ae paramae)
  {
    a = paramo;
    b = paramBoolean;
    c = paramae;
  }
  
  public final Drawable a()
  {
    Drawable localDrawable = a.a(2131234634, 2130969239);
    k.a(localDrawable, "resourceProvider.getTint…message_textColorHistory)");
    return localDrawable;
  }
  
  public final Drawable a(Message paramMessage)
  {
    Object localObject = "message";
    k.b(paramMessage, (String)localObject);
    boolean bool = b;
    if (bool)
    {
      localObject = paramMessage.k();
      bool = ((TransportInfo)localObject).f();
      if (bool)
      {
        localObject = c;
        paramMessage = l;
        paramMessage = ((ae)localObject).a(paramMessage);
        if (paramMessage != null)
        {
          paramMessage = Integer.valueOf(a);
          return a(paramMessage, true);
        }
      }
    }
    return null;
  }
  
  public final Drawable a(Integer paramInteger, boolean paramBoolean)
  {
    if (!paramBoolean) {
      return null;
    }
    if (paramInteger != null)
    {
      paramBoolean = paramInteger.intValue();
      if (!paramBoolean) {
        return a.c(2131234554).mutate();
      }
    }
    if (paramInteger != null)
    {
      boolean bool = paramInteger.intValue();
      paramBoolean = true;
      if (bool == paramBoolean) {
        return a.c(2131234555).mutate();
      }
    }
    return null;
  }
  
  public final String a(int paramInt)
  {
    int i = 4;
    Object[] arrayOfObject1 = null;
    if (paramInt != i)
    {
      switch (paramInt)
      {
      default: 
        localObject = a;
        arrayOfObject1 = new Object[0];
        localObject = ((o)localObject).a(2131886455, arrayOfObject1);
        k.a(localObject, "resourceProvider.getStri…nHistoryItemOutgoingCall)");
        return (String)localObject;
      case 2: 
        localObject = a;
        arrayOfObject1 = new Object[0];
        localObject = ((o)localObject).a(2131886456, arrayOfObject1);
        k.a(localObject, "resourceProvider.getStri…ationHistoryItemWhatsApp)");
        return (String)localObject;
      }
      localObject = a;
      arrayOfObject1 = new Object[0];
      localObject = ((o)localObject).a(2131886449, arrayOfObject1);
      k.a(localObject, "resourceProvider.getStri…ersationHistoryItemFlash)");
      return (String)localObject;
    }
    Object localObject = a;
    Object[] arrayOfObject2 = new Object[1];
    Object[] arrayOfObject3 = new Object[0];
    String str = ((o)localObject).a(2131889034, arrayOfObject3);
    arrayOfObject2[0] = str;
    localObject = ((o)localObject).a(2131886454, arrayOfObject2);
    k.a(localObject, "resourceProvider.getStri…ring(R.string.voip_text))");
    return (String)localObject;
  }
  
  public final String a(k.d paramd)
  {
    k.b(paramd, "phoneNumberType");
    Object localObject = bu.a;
    int i = paramd.ordinal();
    i = localObject[i];
    localObject = null;
    switch (i)
    {
    default: 
      paramd = a;
      localObject = new Object[0];
      paramd = paramd.a(2131887220, (Object[])localObject);
      k.a(paramd, "resourceProvider.getString(R.string.StrOther)");
      return paramd;
    case 2: 
      paramd = a;
      localObject = new Object[0];
      paramd = paramd.a(2131886343, (Object[])localObject);
      k.a(paramd, "resourceProvider.getStri…lerIDLandlineNumberTitle)");
      return paramd;
    }
    paramd = a;
    localObject = new Object[0];
    paramd = paramd.a(2131886341, (Object[])localObject);
    k.a(paramd, "resourceProvider.getStri…erIDCellphoneNumberTitle)");
    return paramd;
  }
  
  public final Drawable b()
  {
    Drawable localDrawable = a.a(2131234352, 2130969533);
    k.a(localDrawable, "resourceProvider.getTint…R.attr.theme_actionColor)");
    return localDrawable;
  }
  
  public final String b(int paramInt)
  {
    int i = 4;
    Object[] arrayOfObject1 = null;
    if (paramInt != i)
    {
      switch (paramInt)
      {
      default: 
        localObject = a;
        arrayOfObject1 = new Object[0];
        localObject = ((o)localObject).a(2131886451, arrayOfObject1);
        k.a(localObject, "resourceProvider.getStri…nHistoryItemIncomingCall)");
        return (String)localObject;
      case 2: 
        localObject = a;
        arrayOfObject1 = new Object[0];
        localObject = ((o)localObject).a(2131886456, arrayOfObject1);
        k.a(localObject, "resourceProvider.getStri…ationHistoryItemWhatsApp)");
        return (String)localObject;
      }
      localObject = a;
      arrayOfObject1 = new Object[0];
      localObject = ((o)localObject).a(2131886449, arrayOfObject1);
      k.a(localObject, "resourceProvider.getStri…ersationHistoryItemFlash)");
      return (String)localObject;
    }
    Object localObject = a;
    Object[] arrayOfObject2 = new Object[1];
    Object[] arrayOfObject3 = new Object[0];
    String str = ((o)localObject).a(2131889034, arrayOfObject3);
    arrayOfObject2[0] = str;
    localObject = ((o)localObject).a(2131886450, arrayOfObject2);
    k.a(localObject, "resourceProvider.getStri…ring(R.string.voip_text))");
    return (String)localObject;
  }
  
  public final Drawable c()
  {
    Drawable localDrawable = a.a(2131234205, 2130969533);
    k.a(localDrawable, "resourceProvider.getTint…R.attr.theme_actionColor)");
    return localDrawable;
  }
  
  public final String c(int paramInt)
  {
    int i = 4;
    Object[] arrayOfObject1 = null;
    if (paramInt != i)
    {
      switch (paramInt)
      {
      default: 
        localObject = a;
        arrayOfObject1 = new Object[0];
        localObject = ((o)localObject).a(2131886453, arrayOfObject1);
        k.a(localObject, "resourceProvider.getStri…ionHistoryItemMissedCall)");
        return (String)localObject;
      case 2: 
        localObject = a;
        arrayOfObject1 = new Object[0];
        localObject = ((o)localObject).a(2131886456, arrayOfObject1);
        k.a(localObject, "resourceProvider.getStri…ationHistoryItemWhatsApp)");
        return (String)localObject;
      }
      localObject = a;
      arrayOfObject1 = new Object[0];
      localObject = ((o)localObject).a(2131886449, arrayOfObject1);
      k.a(localObject, "resourceProvider.getStri…ersationHistoryItemFlash)");
      return (String)localObject;
    }
    Object localObject = a;
    Object[] arrayOfObject2 = new Object[1];
    Object[] arrayOfObject3 = new Object[0];
    String str = ((o)localObject).a(2131889034, arrayOfObject3);
    arrayOfObject2[0] = str;
    localObject = ((o)localObject).a(2131886452, arrayOfObject2);
    k.a(localObject, "resourceProvider.getStri…ring(R.string.voip_text))");
    return (String)localObject;
  }
  
  public final Drawable d()
  {
    Drawable localDrawable = a.a(2131234276, 2130969223);
    k.a(localDrawable, "resourceProvider.getTint…essage_missedOrSpamColor)");
    return localDrawable;
  }
  
  public final Drawable e()
  {
    Drawable localDrawable = a.a(2131233844, 2130969239);
    k.a(localDrawable, "resourceProvider.getTint…message_textColorHistory)");
    return localDrawable;
  }
  
  public final int f()
  {
    return a.e(2130969217);
  }
  
  public final int g()
  {
    return a.e(2130969239);
  }
  
  public final int h()
  {
    return a.d(2131099921);
  }
  
  public final Drawable i()
  {
    Drawable localDrawable = a.a(2131233844, 2130969592);
    k.a(localDrawable, "resourceProvider.getTint…theme_textColorSecondary)");
    return localDrawable;
  }
  
  public final Drawable j()
  {
    Drawable localDrawable = a.a(2131233929, 2130969592);
    k.a(localDrawable, "resourceProvider.getTint…theme_textColorSecondary)");
    return localDrawable;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.bt
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */