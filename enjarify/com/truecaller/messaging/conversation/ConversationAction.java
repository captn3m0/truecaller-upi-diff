package com.truecaller.messaging.conversation;

public enum ConversationAction
{
  public String dynamicTitle;
  public final int icon;
  public final int menuId;
  public final int textViewId;
  public final int tint;
  public final int title;
  
  static
  {
    ConversationAction localConversationAction = new com/truecaller/messaging/conversation/ConversationAction;
    Object localObject1 = localConversationAction;
    localConversationAction.<init>("TOP_SAVE", 0, 2131234478, 2131886486, 2130969592, 2131361930, 2131361931);
    TOP_SAVE = localConversationAction;
    localObject1 = new com/truecaller/messaging/conversation/ConversationAction;
    int i = 2130969592;
    ((ConversationAction)localObject1).<init>("TOP_BLOCK", 1, 2131233879, 2131886414, i, 2131361835, 2131361837);
    TOP_BLOCK = (ConversationAction)localObject1;
    localObject1 = new com/truecaller/messaging/conversation/ConversationAction;
    Object localObject2 = localObject1;
    ((ConversationAction)localObject1).<init>("TOP_UNBLOCK", 2, 2131233879, 2131886424, 2130969585, 2131361940, 2131361941);
    TOP_UNBLOCK = (ConversationAction)localObject1;
    localObject1 = new com/truecaller/messaging/conversation/ConversationAction;
    ((ConversationAction)localObject1).<init>("TOP_NOT_SPAM", 3, 2131234313, 2131886419, i, 2131361916, 2131361917);
    TOP_NOT_SPAM = (ConversationAction)localObject1;
    localObject1 = new com/truecaller/messaging/conversation/ConversationAction;
    int j = 2130969592;
    localObject2 = localObject1;
    ((ConversationAction)localObject1).<init>("TOP_CONNECT_BANK", 4, 2131233790, 2131886416, j, 2131361849, 2131361850);
    TOP_CONNECT_BANK = (ConversationAction)localObject1;
    localObject1 = new com/truecaller/messaging/conversation/ConversationAction;
    ((ConversationAction)localObject1).<init>("TOP_RECHARGE", 5, 2131234584, 2131886420, i, 2131361920, 2131361921);
    TOP_RECHARGE = (ConversationAction)localObject1;
    localObject1 = new com/truecaller/messaging/conversation/ConversationAction;
    localObject2 = localObject1;
    ((ConversationAction)localObject1).<init>("TOP_BOOK_TICKET", 6, 2131234440, 2131886415, j, 2131361838, 2131361839);
    TOP_BOOK_TICKET = (ConversationAction)localObject1;
    localObject1 = new ConversationAction[7];
    localObject2 = TOP_SAVE;
    localObject1[0] = localObject2;
    localObject2 = TOP_BLOCK;
    localObject1[1] = localObject2;
    localObject2 = TOP_UNBLOCK;
    localObject1[2] = localObject2;
    localObject2 = TOP_NOT_SPAM;
    localObject1[3] = localObject2;
    localObject2 = TOP_CONNECT_BANK;
    localObject1[4] = localObject2;
    localObject2 = TOP_RECHARGE;
    localObject1[5] = localObject2;
    localObject2 = TOP_BOOK_TICKET;
    localObject1[6] = localObject2;
    $VALUES = (ConversationAction[])localObject1;
  }
  
  private ConversationAction(int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6)
  {
    menuId = paramInt5;
    icon = paramInt2;
    title = paramInt3;
    tint = paramInt4;
    textViewId = paramInt6;
  }
  
  public static ConversationAction findById(int paramInt)
  {
    ConversationAction[] arrayOfConversationAction = values();
    int i = arrayOfConversationAction.length;
    int j = 0;
    while (j < i)
    {
      ConversationAction localConversationAction = arrayOfConversationAction[j];
      int k = menuId;
      if (k == paramInt) {
        return localConversationAction;
      }
      j += 1;
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.ConversationAction
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */