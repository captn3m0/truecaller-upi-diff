package com.truecaller.messaging.conversation;

import android.graphics.drawable.Drawable;
import c.g.b.k;

public final class bv$a
{
  public int a;
  public int b;
  public Drawable c;
  public Drawable d;
  public String e = "";
  public String f;
  public String g = "";
  
  public final a a(int paramInt)
  {
    a locala = this;
    locala = (a)this;
    b = paramInt;
    return locala;
  }
  
  public final a a(Drawable paramDrawable)
  {
    a locala = this;
    locala = (a)this;
    c = paramDrawable;
    return locala;
  }
  
  public final a a(String paramString)
  {
    k.b(paramString, "type");
    a locala = this;
    locala = (a)this;
    e = paramString;
    return locala;
  }
  
  public final a b(Drawable paramDrawable)
  {
    a locala = this;
    locala = (a)this;
    d = paramDrawable;
    return locala;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.bv.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */