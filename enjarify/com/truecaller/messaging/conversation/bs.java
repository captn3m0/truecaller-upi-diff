package com.truecaller.messaging.conversation;

import android.graphics.drawable.Drawable;
import com.google.c.a.k.d;
import com.truecaller.messaging.data.types.Message;

public abstract interface bs
{
  public abstract Drawable a();
  
  public abstract Drawable a(Message paramMessage);
  
  public abstract Drawable a(Integer paramInteger, boolean paramBoolean);
  
  public abstract String a(int paramInt);
  
  public abstract String a(k.d paramd);
  
  public abstract Drawable b();
  
  public abstract String b(int paramInt);
  
  public abstract Drawable c();
  
  public abstract String c(int paramInt);
  
  public abstract Drawable d();
  
  public abstract Drawable e();
  
  public abstract int f();
  
  public abstract int g();
  
  public abstract int h();
  
  public abstract Drawable i();
  
  public abstract Drawable j();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.bs
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */