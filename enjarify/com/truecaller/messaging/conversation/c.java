package com.truecaller.messaging.conversation;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import c.g.a.a;
import c.g.b.k;
import c.u;

public final class c
  extends com.avito.konveyor.a.b
  implements b
{
  private final ViewGroup a;
  private final View b;
  
  public c(View paramView)
  {
    super(paramView);
    int i = 2131361947;
    Object localObject = paramView.findViewById(i);
    if (localObject != null)
    {
      localObject = (ViewGroup)localObject;
      a = ((ViewGroup)localObject);
      paramView = paramView.findViewById(2131362311);
      b = paramView;
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type android.view.ViewGroup");
    throw paramView;
  }
  
  public final void a()
  {
    Object localObject = a;
    int i = ((ViewGroup)localObject).getChildCount() + -1;
    while (i >= 0)
    {
      View localView = a.getChildAt(i);
      String str = "child";
      k.a(localView, str);
      int j = localView.getVisibility();
      if (j == 0)
      {
        localObject = localView.findViewWithTag("divider");
        k.a(localObject, "child.findViewWithTag<View>(DIVIDER_TAG)");
        ((View)localObject).setVisibility(8);
        return;
      }
      i += -1;
    }
  }
  
  public final void a(int paramInt)
  {
    View localView = itemView.findViewById(paramInt);
    k.a(localView, "itemView.findViewById<View>(itemId)");
    localView.setVisibility(0);
  }
  
  public final void a(int paramInt, String paramString)
  {
    k.b(paramString, "title");
    Object localObject = a.findViewById(paramInt);
    k.a(localObject, "actionsContainer.findViewById<TextView>(viewId)");
    localObject = (TextView)localObject;
    paramString = (CharSequence)paramString;
    ((TextView)localObject).setText(paramString);
  }
  
  public final void a(a parama)
  {
    k.b(parama, "listener");
    View localView = b;
    Object localObject = new com/truecaller/messaging/conversation/c$b;
    ((c.b)localObject).<init>(parama);
    localObject = (View.OnClickListener)localObject;
    localView.setOnClickListener((View.OnClickListener)localObject);
  }
  
  public final void a(c.g.a.b paramb)
  {
    k.b(paramb, "listener");
    ViewGroup localViewGroup = a;
    int i = localViewGroup.getChildCount();
    int j = 0;
    while (j < i)
    {
      View localView = a.getChildAt(j);
      Object localObject = new com/truecaller/messaging/conversation/c$a;
      ((c.a)localObject).<init>(paramb);
      localObject = (View.OnClickListener)localObject;
      localView.setOnClickListener((View.OnClickListener)localObject);
      j += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */