package com.truecaller.messaging.conversation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import com.truecaller.messaging.conversation.b.c;

abstract class bz$a
  extends bz
{
  bz$a(int paramInt, Context paramContext)
  {
    super(paramInt, paramContext);
  }
  
  public final c b()
  {
    Object localObject = c;
    int i = b;
    localObject = ((LayoutInflater)localObject).inflate(i, null);
    c localc = new com/truecaller/messaging/conversation/b/c;
    localc.<init>((View)localObject);
    ((View)localObject).setTag(localc);
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.bz.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */