package com.truecaller.messaging.conversation;

import android.support.v7.widget.LinearLayoutManager;

final class k$3
  extends ba
{
  k$3(k paramk, int paramInt, LinearLayoutManager paramLinearLayoutManager)
  {
    super(paramInt);
  }
  
  public final int a()
  {
    return a.findFirstVisibleItemPosition();
  }
  
  public final void b()
  {
    b.n(false);
  }
  
  public final void c()
  {
    b.n(true);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.k.3
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */