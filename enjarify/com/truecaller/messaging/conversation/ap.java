package com.truecaller.messaging.conversation;

import dagger.a.d;
import javax.inject.Provider;

public final class ap
  implements d
{
  private final Provider a;
  
  private ap(Provider paramProvider)
  {
    a = paramProvider;
  }
  
  public static ap a(Provider paramProvider)
  {
    ap localap = new com/truecaller/messaging/conversation/ap;
    localap.<init>(paramProvider);
    return localap;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.ap
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */