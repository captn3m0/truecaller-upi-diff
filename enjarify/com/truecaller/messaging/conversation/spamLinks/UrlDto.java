package com.truecaller.messaging.conversation.spamLinks;

import c.g.b.k;

public final class UrlDto
{
  private String kind;
  private final String url;
  
  public UrlDto(String paramString1, String paramString2)
  {
    url = paramString1;
    kind = paramString2;
  }
  
  public final String getKind()
  {
    return kind;
  }
  
  public final String getUrl()
  {
    return url;
  }
  
  public final void setKind(String paramString)
  {
    k.b(paramString, "<set-?>");
    kind = paramString;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.spamLinks.UrlDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */