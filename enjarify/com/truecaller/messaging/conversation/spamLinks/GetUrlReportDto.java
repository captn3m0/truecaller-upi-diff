package com.truecaller.messaging.conversation.spamLinks;

public final class GetUrlReportDto
{
  private final String url;
  
  public GetUrlReportDto(String paramString)
  {
    url = paramString;
  }
  
  public final String getUrl()
  {
    return url;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.spamLinks.GetUrlReportDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */