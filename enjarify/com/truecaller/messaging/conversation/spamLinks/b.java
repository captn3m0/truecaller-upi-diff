package com.truecaller.messaging.conversation.spamLinks;

import android.net.Uri;
import c.n.m;
import c.u;
import com.truecaller.messaging.conversation.a.b.f;
import com.truecaller.network.spamUrls.d;
import java.util.Iterator;
import java.util.Set;

public final class b
  implements a
{
  private final d a;
  
  public b(d paramd)
  {
    a = paramd;
  }
  
  private final boolean a(String paramString)
  {
    Object localObject1 = a.b();
    boolean bool1 = ((Set)localObject1).contains(paramString);
    boolean bool2 = true;
    if (bool1) {
      return bool2;
    }
    localObject1 = ((Iterable)a.c()).iterator();
    do
    {
      bool3 = ((Iterator)localObject1).hasNext();
      if (!bool3) {
        break;
      }
      localObject2 = (String)((Iterator)localObject1).next();
      c.n.k localk = new c/n/k;
      localk.<init>((String)localObject2);
      localObject2 = paramString;
      localObject2 = (CharSequence)paramString;
      bool3 = localk.a((CharSequence)localObject2);
    } while (!bool3);
    return bool2;
    paramString = m.d(paramString, ".", "");
    localObject1 = paramString;
    localObject1 = (CharSequence)paramString;
    int i = ((CharSequence)localObject1).length();
    boolean bool3 = false;
    Object localObject2 = null;
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject1 = null;
    }
    if (i == 0)
    {
      boolean bool4 = a(paramString);
      if (bool4) {
        return bool2;
      }
    }
    return false;
  }
  
  public final boolean a(f paramf)
  {
    c.g.b.k.b(paramf, "matchResult");
    paramf = Uri.parse(d);
    c.g.b.k.a(paramf, "uri");
    String str1 = paramf.getHost();
    if (str1 != null) {
      if (str1 != null)
      {
        str1 = str1.toLowerCase();
        String str2 = "(this as java.lang.String).toLowerCase()";
        c.g.b.k.a(str1, str2);
        if (str1 != null)
        {
          paramf = paramf.getScheme();
          boolean bool1 = true;
          if (paramf != null)
          {
            String str3 = "http";
            boolean bool2 = c.g.b.k.a(paramf, str3);
            if (!bool2)
            {
              str3 = "https";
              bool3 = c.g.b.k.a(paramf, str3);
              if (bool3) {}
            }
          }
          else
          {
            bool3 = false;
            paramf = null;
            break label116;
          }
          boolean bool3 = true;
          label116:
          if (bool3)
          {
            bool3 = a(str1);
            if (!bool3) {
              return bool1;
            }
          }
          return false;
        }
      }
      else
      {
        paramf = new c/u;
        paramf.<init>("null cannot be cast to non-null type java.lang.String");
        throw paramf;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.spamLinks.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */