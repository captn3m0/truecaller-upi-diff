package com.truecaller.messaging.conversation.spamLinks;

import java.util.List;

public final class WhitelistDto
{
  private final List urls;
  
  public WhitelistDto(List paramList)
  {
    urls = paramList;
  }
  
  public final List getUrls()
  {
    return urls;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.spamLinks.WhitelistDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */