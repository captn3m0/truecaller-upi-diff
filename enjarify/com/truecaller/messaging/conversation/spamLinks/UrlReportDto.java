package com.truecaller.messaging.conversation.spamLinks;

public final class UrlReportDto
{
  private final String report;
  private final String url;
  
  public UrlReportDto(String paramString1, String paramString2)
  {
    url = paramString1;
    report = paramString2;
  }
  
  public final String getReport()
  {
    return report;
  }
  
  public final String getUrl()
  {
    return url;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.spamLinks.UrlReportDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */