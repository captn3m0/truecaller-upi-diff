package com.truecaller.messaging.conversation.spamLinks;

public final class SpamReportDto
{
  private int spamCount;
  private int spamScore;
  private long ttl;
  
  public SpamReportDto()
  {
    this(0, 0L, 0, 7, null);
  }
  
  public SpamReportDto(int paramInt1, long paramLong, int paramInt2)
  {
    spamCount = paramInt1;
    ttl = paramLong;
    spamScore = paramInt2;
  }
  
  public final int getSpamCount()
  {
    return spamCount;
  }
  
  public final int getSpamScore()
  {
    return spamScore;
  }
  
  public final long getTtl()
  {
    return ttl;
  }
  
  public final void setSpamCount(int paramInt)
  {
    spamCount = paramInt;
  }
  
  public final void setSpamScore(int paramInt)
  {
    spamScore = paramInt;
  }
  
  public final void setTtl(long paramLong)
  {
    ttl = paramLong;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.spamLinks.SpamReportDto
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */