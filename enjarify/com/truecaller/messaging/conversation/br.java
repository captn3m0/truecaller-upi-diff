package com.truecaller.messaging.conversation;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

final class br
  extends ArrayAdapter
{
  private int[] a;
  
  br(Context paramContext, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super(paramContext, 2131558854, 2131363718, paramArrayOfString);
    a = paramArrayOfInt;
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    paramView = super.getView(paramInt, paramView, paramViewGroup);
    paramViewGroup = (ImageView)paramView.findViewById(2131363301);
    paramInt = a[paramInt];
    paramViewGroup.setImageResource(paramInt);
    return paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.br
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */