package com.truecaller.messaging.conversation;

import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.truecaller.analytics.ap;
import com.truecaller.analytics.e.a;
import com.truecaller.android.truemoji.j;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.bb;
import com.truecaller.flashsdk.models.FlashContact;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.conversation.emoji.PokeableEmoji;
import com.truecaller.messaging.conversation.voice_notes.RecordView.RecordState;
import com.truecaller.messaging.data.t;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.Draft;
import com.truecaller.messaging.data.types.Draft.a;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.ReplySnippet;
import com.truecaller.messaging.g.b.a;
import com.truecaller.messaging.g.b.b;
import com.truecaller.messaging.g.b.c;
import com.truecaller.messaging.g.b.d;
import com.truecaller.messaging.g.b.e;
import com.truecaller.messaging.g.b.f;
import com.truecaller.messaging.g.b.g;
import com.truecaller.messaging.i.g;
import com.truecaller.util.aa;
import com.truecaller.util.al;
import com.truecaller.util.ba;
import com.truecaller.util.bg;
import com.truecaller.voip.ai;
import com.truecaller.voip.incall.VoipService;
import com.truecaller.voip.incoming.IncomingVoipService;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class n
  extends bb
  implements m
{
  private final com.truecaller.androidactors.f A;
  private final com.truecaller.multisim.h B;
  private final bg C;
  private final j D;
  private final com.truecaller.featuretoggles.e E;
  private final com.truecaller.payments.a F;
  private final com.truecaller.flashsdk.core.b G;
  private final com.truecaller.androidactors.f H;
  private final com.truecaller.messaging.g.d I;
  private final com.truecaller.messaging.conversation.emoji.a J;
  private final com.truecaller.analytics.a.f K;
  private final com.truecaller.messaging.c.a L;
  private final ai M;
  private final com.truecaller.androidactors.f N;
  private final bi O;
  private final al P;
  private final AudioManager Q;
  final dagger.a a;
  private final c.n.k c;
  private Draft d;
  private SendType e;
  private final com.truecaller.messaging.transport.a f;
  private boolean g;
  private boolean h;
  private boolean i;
  private boolean j;
  private PokeableEmoji k;
  private boolean l;
  private boolean m;
  private Boolean n;
  private RecordView.RecordState o;
  private final com.truecaller.androidactors.k p;
  private final com.truecaller.messaging.transport.m q;
  private final com.truecaller.messaging.d.a r;
  private final bn s;
  private final com.truecaller.messaging.h t;
  private final bf u;
  private final bw v;
  private final ap w;
  private final com.truecaller.utils.l x;
  private final com.truecaller.utils.d y;
  private final com.truecaller.messaging.i.d z;
  
  public n(com.truecaller.androidactors.k paramk, dagger.a parama, com.truecaller.messaging.transport.m paramm, com.truecaller.messaging.d.a parama1, bn parambn, com.truecaller.messaging.h paramh, bf parambf, bw parambw, ap paramap, com.truecaller.utils.l paraml, com.truecaller.utils.d paramd, com.truecaller.messaging.i.d paramd1, com.truecaller.androidactors.f paramf1, com.truecaller.multisim.h paramh1, bg parambg, j paramj, com.truecaller.featuretoggles.e parame, com.truecaller.payments.a parama2, com.truecaller.flashsdk.core.b paramb, com.truecaller.androidactors.f paramf2, com.truecaller.messaging.g.d paramd2, com.truecaller.messaging.conversation.emoji.a parama3, com.truecaller.analytics.a.f paramf, com.truecaller.messaging.c.a parama4, ai paramai, com.truecaller.androidactors.f paramf3, bi parambi, al paramal, AudioManager paramAudioManager)
  {
    p = paramk;
    a = parama;
    q = paramm;
    r = parama1;
    s = parambn;
    t = paramh;
    u = parambf;
    v = parambw;
    w = paramap;
    x = paraml;
    y = paramd;
    z = paramd1;
    A = paramf1;
    B = paramh1;
    localObject = parambg;
    C = parambg;
    D = paramj;
    localObject = parame;
    E = parame;
    F = parama2;
    localObject = paramb;
    G = paramb;
    H = paramf2;
    localObject = paramd2;
    I = paramd2;
    J = parama3;
    localObject = paramf;
    K = paramf;
    L = parama4;
    localObject = paramai;
    M = paramai;
    N = paramf3;
    localObject = parambi;
    O = parambi;
    P = paramal;
    localObject = paramAudioManager;
    Q = paramAudioManager;
    localObject = new c/n/k;
    ((c.n.k)localObject).<init>(".*[a-zA-Z]+.*");
    c = ((c.n.k)localObject);
    localObject = SendType.DEFAULT;
    e = ((SendType)localObject);
    localObject = new com/truecaller/messaging/transport/a;
    ((com.truecaller.messaging.transport.a)localObject).<init>();
    f = ((com.truecaller.messaging.transport.a)localObject);
  }
  
  private final boolean L()
  {
    Draft localDraft = d;
    if (localDraft != null) {
      return localDraft.b();
    }
    return false;
  }
  
  private final boolean M()
  {
    Draft localDraft = d;
    if (localDraft == null) {
      return false;
    }
    boolean bool1 = R();
    boolean bool2 = true;
    if (!bool1)
    {
      localbn = s;
      bool1 = localbn.h();
      if (!bool1)
      {
        localbn = s;
        bool1 = localbn.i();
        if (!bool1)
        {
          bool1 = true;
          break label64;
        }
      }
    }
    bool1 = false;
    bn localbn = null;
    label64:
    com.truecaller.featuretoggles.b localb = E.C();
    boolean bool3 = localb.a();
    if ((bool3) && (bool1))
    {
      boolean bool4 = localDraft.b();
      if (!bool4)
      {
        bool4 = A();
        if (bool4) {
          return bool2;
        }
      }
    }
    return false;
  }
  
  private final void N()
  {
    r localr = (r)b;
    if (localr == null) {
      return;
    }
    boolean bool = A();
    int i1;
    if (bool)
    {
      i1 = 2131886463;
    }
    else
    {
      Object localObject = d;
      if (localObject != null)
      {
        localObject = d;
        if (localObject != null)
        {
          i1 = localObject.length;
          break label57;
        }
      }
      i1 = 0;
      localObject = null;
      label57:
      int i2 = 1;
      if (i1 > i2) {
        i1 = 2131886462;
      } else {
        i1 = 2131886461;
      }
    }
    localr.d(i1);
  }
  
  private final void O()
  {
    boolean bool = A();
    bf localbf;
    int i1;
    if (bool)
    {
      localbf = u;
      i1 = localbf.b();
    }
    else
    {
      localbf = u;
      i1 = localbf.a();
    }
    r localr = (r)b;
    if (localr != null)
    {
      localr.e(i1);
      return;
    }
  }
  
  private final void P()
  {
    r localr = (r)b;
    if (localr != null) {
      localr.c(null);
    }
    s.a(false);
    localr = (r)b;
    if (localr != null)
    {
      localr.C();
      return;
    }
  }
  
  private final boolean Q()
  {
    com.truecaller.messaging.transport.a locala = f;
    int i1 = locala.c();
    int i2 = 1;
    if (i1 == i2) {
      return i2;
    }
    return false;
  }
  
  private final boolean R()
  {
    Object localObject = (r)b;
    if (localObject != null)
    {
      localObject = ((r)localObject).f();
    }
    else
    {
      i1 = 0;
      localObject = null;
    }
    boolean bool = true;
    if (localObject != null)
    {
      i1 = ((CharSequence)localObject).length();
      if (i1 != 0)
      {
        i1 = 0;
        localObject = null;
        break label55;
      }
    }
    int i1 = 1;
    label55:
    if (i1 == 0) {
      return bool;
    }
    return false;
  }
  
  private final void S()
  {
    boolean bool = T();
    int i1;
    if (bool)
    {
      i1 = 2131234669;
    }
    else
    {
      bf localbf1 = u;
      localObject = f;
      int i2 = ((com.truecaller.messaging.transport.a)localObject).c();
      i1 = localbf1.a(i2);
    }
    Object localObject = (r)b;
    if (localObject != null)
    {
      bf localbf2 = u;
      int i3 = f.c();
      int i4 = localbf2.b(i3);
      ((r)localObject).a(i1, i4);
      return;
    }
  }
  
  private boolean T()
  {
    boolean bool1 = R();
    boolean bool2 = true;
    if (!bool1)
    {
      localbn = s;
      bool1 = localbn.h();
      if (!bool1)
      {
        localbn = s;
        bool1 = localbn.i();
        if (!bool1)
        {
          bool1 = true;
          break label48;
        }
      }
    }
    bool1 = false;
    bn localbn = null;
    label48:
    Object localObject = P;
    boolean bool3 = ((al)localObject).h();
    if (!bool3)
    {
      localObject = Q;
      int i1 = ((AudioManager)localObject).getMode();
      if (i1 == 0)
      {
        bool4 = U();
        if (!bool4)
        {
          bool4 = false;
          localObject = null;
          break label111;
        }
      }
    }
    boolean bool4 = true;
    label111:
    com.truecaller.featuretoggles.b localb = E.k();
    boolean bool5 = localb.a();
    if ((bool5) && (bool1))
    {
      bool1 = A();
      if ((bool1) && (!bool4)) {
        return bool2;
      }
    }
    return false;
  }
  
  private static boolean U()
  {
    boolean bool = VoipService.p();
    if (!bool)
    {
      bool = IncomingVoipService.h();
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  private final void V()
  {
    Object localObject1 = E.e();
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
    boolean bool2 = false;
    Object localObject2 = null;
    if (!bool1)
    {
      localObject1 = (r)b;
      if (localObject1 != null)
      {
        ((r)localObject1).o(false);
        return;
      }
      return;
    }
    localObject1 = O;
    bool1 = ((bi)localObject1).d();
    if (bool1)
    {
      localObject1 = (r)b;
      if (localObject1 != null)
      {
        ((r)localObject1).o(false);
        return;
      }
      return;
    }
    bool1 = m;
    if (bool1)
    {
      localObject1 = (r)b;
      if (localObject1 != null)
      {
        ((r)localObject1).o(false);
        return;
      }
      return;
    }
    bool1 = j;
    if (!bool1)
    {
      bool1 = L();
      if (!bool1)
      {
        bool1 = R();
        if (!bool1)
        {
          localObject1 = s;
          bool1 = ((bn)localObject1).h();
          if (!bool1)
          {
            localObject1 = s;
            bool1 = ((bn)localObject1).i();
            if (!bool1)
            {
              localObject1 = (r)b;
              if (localObject1 != null)
              {
                boolean bool3 = true;
                ((r)localObject1).o(bool3);
              }
              localObject1 = (r)b;
              if (localObject1 != null) {
                ((r)localObject1).p(false);
              }
              localObject1 = n;
              if (localObject1 == null)
              {
                bool1 = m;
                if (!bool1)
                {
                  localObject1 = d;
                  if (localObject1 == null) {
                    break label336;
                  }
                  localObject1 = d;
                  if (localObject1 == null) {
                    break label336;
                  }
                  localObject1 = (Participant)c.a.f.c((Object[])localObject1);
                  if (localObject1 == null) {
                    break label336;
                  }
                  localObject2 = M;
                  Object localObject3 = new com/truecaller/messaging/conversation/n$h;
                  ((n.h)localObject3).<init>(this);
                  localObject3 = (com.truecaller.voip.e)localObject3;
                  ((ai)localObject2).a((Participant)localObject1, (com.truecaller.voip.e)localObject3);
                  break label336;
                }
              }
              localObject1 = n;
              if (localObject1 != null)
              {
                bool1 = ((Boolean)localObject1).booleanValue();
                localObject2 = (r)b;
                if (localObject2 != null) {
                  ((r)localObject2).p(bool1);
                }
              }
              label336:
              localObject1 = (r)b;
              if (localObject1 != null)
              {
                bool2 = X();
                ((r)localObject1).q(bool2);
              }
              localObject1 = (r)b;
              if (localObject1 != null)
              {
                bool2 = W();
                ((r)localObject1).r(bool2);
                return;
              }
              return;
            }
          }
        }
        localObject1 = (r)b;
        if (localObject1 != null)
        {
          ((r)localObject1).o(false);
          return;
        }
        return;
      }
    }
    localObject1 = (r)b;
    if (localObject1 != null)
    {
      ((r)localObject1).o(false);
      return;
    }
  }
  
  private final boolean W()
  {
    Object localObject = E.n();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    if (bool)
    {
      localObject = Y();
      if (localObject != null) {
        return true;
      }
    }
    return false;
  }
  
  private final boolean X()
  {
    Object localObject1 = t;
    boolean bool1 = ((com.truecaller.messaging.h)localObject1).m();
    if (bool1)
    {
      localObject1 = d;
      if (localObject1 != null)
      {
        localObject1 = d;
      }
      else
      {
        bool1 = false;
        localObject1 = null;
      }
      if (localObject1 == null) {
        localObject1 = new Participant[0];
      }
      int i1 = localObject1.length;
      int i2 = 0;
      boolean bool2;
      for (;;)
      {
        bool2 = true;
        if (i2 >= i1) {
          break;
        }
        Object localObject2 = f;
        c.g.b.k.a(localObject2, "participant.normalizedAddress");
        Object localObject3 = "+";
        String str = "";
        localObject2 = c.n.m.d(c.n.m.a((String)localObject2, (String)localObject3, str));
        if (localObject2 != null)
        {
          long l1 = ((Long)localObject2).longValue();
          localObject2 = String.valueOf(l1);
          if (localObject2 != null)
          {
            localObject3 = G;
            bool3 = hc;
            localObject2 = Boolean.valueOf(bool3);
            break label162;
          }
        }
        boolean bool3 = false;
        localObject2 = null;
        label162:
        bool3 = com.truecaller.utils.extensions.c.a((Boolean)localObject2);
        if (bool3)
        {
          bool1 = true;
          break label192;
        }
        i2 += 1;
      }
      bool1 = false;
      localObject1 = null;
      label192:
      if (bool1) {
        return bool2;
      }
    }
    return false;
  }
  
  private final String Y()
  {
    Object localObject = d;
    com.truecaller.payments.a locala = null;
    if (localObject != null)
    {
      localObject = d;
      if (localObject != null)
      {
        localObject = (Participant)c.a.f.c((Object[])localObject);
        if (localObject != null)
        {
          int i1 = c;
          if (i1 != 0) {
            return null;
          }
          locala = F;
          localObject = f;
          c.g.b.k.a(localObject, "participant.normalizedAddress");
          return locala.a((String)localObject);
        }
      }
    }
    return null;
  }
  
  private final boolean Z()
  {
    com.truecaller.utils.l locall = x;
    String[] tmp9_6 = new String[3];
    String[] tmp10_9 = tmp9_6;
    String[] tmp10_9 = tmp9_6;
    tmp10_9[0] = "android.permission.RECORD_AUDIO";
    tmp10_9[1] = "android.permission.READ_EXTERNAL_STORAGE";
    tmp10_9[2] = "android.permission.WRITE_EXTERNAL_STORAGE";
    String[] arrayOfString = tmp10_9;
    return locall.a(arrayOfString);
  }
  
  private final void a(int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    r localr = (r)b;
    if (localr == null) {
      return;
    }
    SendType localSendType = e;
    SendType[] arrayOfSendType = SendType.values();
    Object localObject1 = arrayOfSendType[paramInt];
    e = ((SendType)localObject1);
    paramInt = 2;
    if (paramBoolean1)
    {
      localObject2 = d;
      if (localObject2 != null)
      {
        localObject2 = b;
        if (localObject2 != null)
        {
          long l1 = a;
          localObject2 = e;
          Object localObject3 = SendType.SMS;
          if (localObject2 == localObject3)
          {
            paramBoolean1 = false;
            localObject2 = null;
          }
          else
          {
            paramBoolean1 = true;
          }
          localObject3 = (t)H.a();
          ((t)localObject3).a(l1, paramBoolean1);
        }
      }
    }
    Object localObject2 = localr.f();
    b((CharSequence)localObject2);
    N();
    localObject2 = e;
    if ((localSendType != localObject2) && (paramBoolean2))
    {
      localObject4 = SendType.SMS;
      if (localObject2 == localObject4) {
        paramBoolean1 = 2131886484;
      } else {
        paramBoolean1 = 2131886483;
      }
      localr.m(paramBoolean1);
    }
    paramBoolean1 = A();
    if (!paramBoolean1) {
      paramInt = 1;
    }
    s.d(paramInt);
    localObject1 = w;
    localObject2 = e;
    Object localObject4 = o.a;
    paramBoolean1 = ((SendType)localObject2).ordinal();
    paramBoolean1 = localObject4[paramBoolean1];
    switch (paramBoolean1)
    {
    default: 
      localObject1 = new c/l;
      ((c.l)localObject1).<init>();
      throw ((Throwable)localObject1);
    case 3: 
      localObject2 = "SMS";
      break;
    case 2: 
      localObject2 = "IM";
      break;
    case 1: 
      localObject2 = "Unknown";
    }
    ((ap)localObject1).a((String)localObject2, "conversation");
  }
  
  private final void a(String paramString1, String paramString2)
  {
    ap localap = w;
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("ViewAction");
    paramString1 = locala.a("Context", "conversation").a("Action", paramString1).a("SubAction", paramString2).a();
    c.g.b.k.a(paramString1, "AnalyticsEvent.Builder(V…\n                .build()");
    localap.a(paramString1);
  }
  
  private static boolean a(String paramString, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    paramArrayOfInt = (Iterable)c.a.f.a(paramArrayOfInt);
    paramArrayOfString = (Iterable)c.a.f.a(paramArrayOfString, paramArrayOfInt);
    paramArrayOfInt = paramArrayOfString;
    paramArrayOfInt = (Collection)paramArrayOfString;
    boolean bool1 = paramArrayOfInt.isEmpty();
    if (!bool1)
    {
      paramArrayOfString = paramArrayOfString.iterator();
      boolean bool3;
      int i1;
      do
      {
        bool1 = paramArrayOfString.hasNext();
        if (!bool1) {
          break;
        }
        paramArrayOfInt = (c.n)paramArrayOfString.next();
        String str = (String)a;
        boolean bool2 = c.g.b.k.a(str, paramString);
        bool3 = true;
        if (bool2)
        {
          paramArrayOfInt = (Number)b;
          i1 = paramArrayOfInt.intValue();
          if (i1 == 0)
          {
            i1 = 1;
            continue;
          }
        }
        i1 = 0;
        paramArrayOfInt = null;
      } while (i1 == 0);
      return bool3;
    }
    return false;
  }
  
  private final boolean b(int paramInt)
  {
    r localr = (r)b;
    boolean bool1 = false;
    String str = null;
    if (localr == null) {
      return false;
    }
    com.truecaller.utils.l locall = x;
    boolean bool2 = locall.b();
    if (bool2)
    {
      locall = x;
      bool2 = locall.c();
      if (bool2) {
        return false;
      }
    }
    str = "android.permission.READ_EXTERNAL_STORAGE";
    bool1 = localr.k(str);
    if (!bool1)
    {
      str = "android.permission.WRITE_EXTERNAL_STORAGE";
      bool1 = localr.k(str);
      if (!bool1)
      {
        localr.q(paramInt);
        break label116;
      }
    }
    localr.E();
    label116:
    return true;
  }
  
  private final void c(Uri paramUri)
  {
    paramUri = ((aa)A.a()).b(paramUri);
    i locali = p.a();
    Object localObject1 = new com/truecaller/messaging/conversation/n$a;
    Object localObject2 = this;
    localObject2 = (n)this;
    ((n.a)localObject1).<init>((n)localObject2);
    localObject1 = (c.g.a.b)localObject1;
    localObject2 = new com/truecaller/messaging/conversation/q;
    ((q)localObject2).<init>((c.g.a.b)localObject1);
    localObject2 = (ac)localObject2;
    paramUri.a(locali, (ac)localObject2);
  }
  
  private final void c(String paramString)
  {
    ap localap = w;
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("XAction");
    paramString = locala.a("Context", "Conversation").a("Action", paramString).a();
    c.g.b.k.a(paramString, "AnalyticsEvent.Builder(X…\n                .build()");
    localap.a(paramString);
  }
  
  private final void c(boolean paramBoolean)
  {
    Object localObject1 = d;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = (r)b;
    if (localObject2 == null) {
      return;
    }
    Object localObject3 = e;
    Object localObject4 = SendType.SMS;
    boolean bool1 = true;
    if (localObject3 != localObject4)
    {
      localObject3 = f;
      int i1 = ((com.truecaller.messaging.transport.a)localObject3).c();
      int i2 = 2;
      if (i1 == i2)
      {
        i1 = 0;
        localObject3 = null;
        i3 = 0;
        break label85;
      }
    }
    int i3 = 1;
    label85:
    boolean bool2 = R();
    if (bool2)
    {
      localObject1 = ((Draft)localObject1).c();
      localObject3 = ((r)localObject2).f().toString();
      localObject1 = ((Draft.a)localObject1).a((String)localObject3).d();
      localObject3 = "draft.buildUpon().setTex…ext().toString()).build()";
      c.g.b.k.a(localObject1, (String)localObject3);
    }
    localObject3 = I;
    localObject4 = s.g();
    localObject4 = com.truecaller.messaging.g.a.a((Draft)localObject1, (Collection)localObject4);
    Object localObject5 = r.a();
    String str = "multiSimHelper.selectedSimToken";
    c.g.b.k.a(localObject5, str);
    boolean bool3 = i3 ^ 0x1;
    boolean bool4 = h;
    Object localObject6 = t;
    boolean bool5 = ((com.truecaller.messaging.h)localObject6).z();
    Object localObject7 = ((com.truecaller.messaging.g.d)localObject3).a((List)localObject4, (String)localObject5, bool3, bool4, bool5);
    bool2 = localObject7 instanceof b.d;
    if (bool2)
    {
      ((r)localObject2).m();
    }
    else
    {
      bool2 = localObject7 instanceof b.c;
      if (bool2)
      {
        ((r)localObject2).n();
      }
      else
      {
        bool2 = localObject7 instanceof b.a;
        if (bool2)
        {
          localObject3 = localObject7;
          localObject3 = (b.a)localObject7;
          localObject4 = s;
          localObject5 = b;
          ((bn)localObject4).a((List)localObject5);
          localObject4 = C;
          int i5 = c;
          long l1 = ((bg)localObject4).a(i5);
          long l2 = a;
          localObject6 = C;
          long l3 = ((bg)localObject6).a(l1);
          int i4 = c;
          localObject3 = localObject2;
          l1 = l2;
          l2 = l3;
          ((r)localObject2).a(l1, l3, i4);
        }
        else
        {
          bool2 = localObject7 instanceof b.g;
          if (bool2)
          {
            ((r)localObject2).P();
          }
          else
          {
            bool2 = localObject7 instanceof b.b;
            if (bool2)
            {
              ((r)localObject2).L();
            }
            else
            {
              bool2 = localObject7 instanceof b.f;
              if (bool2) {
                ((r)localObject2).C();
              }
            }
          }
        }
      }
    }
    bool2 = localObject7 instanceof b.e;
    if (!bool2) {
      return;
    }
    if ((i3 != 0) && (paramBoolean))
    {
      ((r)localObject2).l();
      t.e();
      return;
    }
    P();
    Object localObject8 = (CharSequence)"";
    b((CharSequence)localObject8);
    paramBoolean = ((Draft)localObject1).b();
    if (paramBoolean) {
      localObject8 = "reply";
    } else {
      localObject8 = "conversation";
    }
    localObject1 = I;
    localObject7 = (b.e)localObject7;
    localObject8 = ((com.truecaller.messaging.g.d)localObject1).a((b.e)localObject7, bool1, (String)localObject8);
    localObject1 = p.a();
    localObject2 = new com/truecaller/messaging/conversation/n$g;
    localObject3 = this;
    localObject3 = (n)this;
    ((n.g)localObject2).<init>((n)localObject3);
    localObject2 = (c.g.a.b)localObject2;
    localObject3 = new com/truecaller/messaging/conversation/q;
    ((q)localObject3).<init>((c.g.a.b)localObject2);
    localObject3 = (ac)localObject3;
    ((w)localObject8).a((i)localObject1, (ac)localObject3);
  }
  
  private final void d(String paramString)
  {
    ap localap = w;
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("VoiceClipSend");
    paramString = locala.a("Action", paramString).a();
    c.g.b.k.a(paramString, "AnalyticsEvent.Builder(A…\n                .build()");
    localap.a(paramString);
  }
  
  public final boolean A()
  {
    Object localObject = v;
    boolean bool = ((bw)localObject).a();
    if (bool)
    {
      localObject = f;
      int i1 = ((com.truecaller.messaging.transport.a)localObject).c();
      int i2 = 2;
      if (i1 == i2)
      {
        localObject = e;
        SendType localSendType = SendType.SMS;
        if (localObject != localSendType) {
          return true;
        }
      }
    }
    return false;
  }
  
  public final void B()
  {
    Object localObject = d;
    if (localObject != null) {
      localObject = ((Draft)localObject).c().c().d();
    } else {
      localObject = null;
    }
    d = ((Draft)localObject);
    localObject = (r)b;
    if (localObject != null)
    {
      boolean bool = M();
      ((r)localObject).j(bool);
      return;
    }
  }
  
  public final boolean C()
  {
    return j;
  }
  
  public final void D()
  {
    ((m.a)a.get()).f();
    c("Call");
  }
  
  public final void E()
  {
    ((m.a)a.get()).g();
    c("Voip");
  }
  
  public final void F()
  {
    f();
    c("Flash");
  }
  
  public final void G()
  {
    a();
    c("Pay");
  }
  
  public final int H()
  {
    return t.ab();
  }
  
  public final void I()
  {
    Object localObject = O;
    boolean bool = ((bi)localObject).d();
    if (bool)
    {
      O.b(false);
      V();
      localObject = (r)b;
      if (localObject != null) {
        ((r)localObject).a(false);
      }
      localObject = (r)b;
      if (localObject != null)
      {
        ((r)localObject).d();
        return;
      }
    }
  }
  
  public final void J()
  {
    boolean bool = T();
    r localr = (r)b;
    if (localr != null)
    {
      if (bool)
      {
        bool = Z();
        if (bool)
        {
          bool = true;
          break label37;
        }
      }
      bool = false;
      label37:
      localr.l(bool);
    }
    S();
  }
  
  public final void K()
  {
    Object localObject = (r)b;
    if (localObject != null)
    {
      localObject = ((r)localObject).f();
      b((CharSequence)localObject);
      V();
      return;
    }
  }
  
  public final void a()
  {
    boolean bool = W();
    if (!bool) {
      return;
    }
    Object localObject1 = (r)b;
    if (localObject1 != null) {
      ((r)localObject1).K();
    }
    localObject1 = Y();
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = d;
    if (localObject2 != null)
    {
      localObject2 = d;
      if (localObject2 != null)
      {
        localObject2 = (Participant)c.a.f.c((Object[])localObject2);
        if (localObject2 != null)
        {
          localObject2 = m;
          break label78;
        }
      }
    }
    localObject2 = null;
    label78:
    r localr = (r)b;
    if (localr != null) {
      localr.a((String)localObject1, (String)localObject2);
    }
    a("attachment", "payment");
  }
  
  public final void a(int paramInt)
  {
    Object localObject1 = (r)b;
    if (localObject1 != null) {
      ((r)localObject1).M();
    }
    boolean bool = true;
    a(paramInt, bool, bool);
    S();
    Object localObject2 = SendType.values()[paramInt];
    localObject1 = SendType.SMS;
    if (localObject2 == localObject1)
    {
      paramInt = L();
      if (paramInt != 0)
      {
        localObject2 = (r)b;
        if (localObject2 != null)
        {
          ((r)localObject2).C();
          return;
        }
      }
    }
  }
  
  public final void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    c.g.b.k.b(paramArrayOfString, "permissions");
    String str = "grantResults";
    c.g.b.k.b(paramArrayOfInt, str);
    int i1 = 200;
    Object localObject;
    if (paramInt != i1)
    {
      switch (paramInt)
      {
      default: 
        break;
      case 206: 
        paramInt = Z();
        if (paramInt == 0) {
          break;
        }
        localObject = (r)b;
        if (localObject == null) {
          break;
        }
        boolean bool = T();
        ((r)localObject).l(bool);
        return;
      case 205: 
        localObject = "android.permission.READ_EXTERNAL_STORAGE";
        paramInt = a((String)localObject, paramArrayOfString, paramArrayOfInt);
        if (paramInt == 0) {
          break;
        }
        localObject = (r)b;
        if (localObject != null) {
          ((r)localObject).z();
        }
        return;
      case 204: 
        localObject = "android.permission.READ_EXTERNAL_STORAGE";
        paramInt = a((String)localObject, paramArrayOfString, paramArrayOfInt);
        if (paramInt == 0) {
          break;
        }
        localObject = (r)b;
        if (localObject != null) {
          ((r)localObject).x();
        }
        return;
      }
    }
    else
    {
      localObject = "android.permission.READ_SMS";
      paramInt = a((String)localObject, paramArrayOfString, paramArrayOfInt);
      if (paramInt != 0)
      {
        paramInt = t.f() ^ true;
        c(paramInt);
        return;
      }
    }
  }
  
  public final void a(Uri paramUri)
  {
    c.g.b.k.b(paramUri, "uri");
    c(paramUri);
  }
  
  public final void a(Menu paramMenu)
  {
    c.g.b.k.b(paramMenu, "menu");
    int i1 = 2131361937;
    paramMenu = paramMenu.findItem(i1);
    SendType localSendType1 = e;
    SendType localSendType2 = SendType.SMS;
    if (localSendType1 == localSendType2) {
      i1 = 2131886422;
    } else {
      i1 = 2131886423;
    }
    paramMenu.setTitle(i1);
    boolean bool = i;
    if (bool)
    {
      bool = j;
      if (!bool)
      {
        bool = true;
        break label81;
      }
    }
    bool = false;
    localSendType1 = null;
    label81:
    paramMenu.setVisible(bool);
  }
  
  public final void a(Draft paramDraft)
  {
    r localr = (r)b;
    if (localr == null) {
      return;
    }
    boolean bool1 = true;
    localr.e(bool1);
    if (paramDraft == null) {
      return;
    }
    s.f();
    Object localObject1 = s;
    Object localObject2 = e;
    int i2 = localObject2.length;
    localObject2 = (BinaryEntity[])Arrays.copyOf((Object[])localObject2, i2);
    ((bn)localObject1).a(bool1, (BinaryEntity[])localObject2);
    localObject1 = d;
    localObject2 = null;
    if (localObject1 != null)
    {
      localObject1 = b;
    }
    else
    {
      bool3 = false;
      localObject1 = null;
    }
    if (localObject1 == null)
    {
      localObject1 = b;
      if (localObject1 != null)
      {
        localObject1 = r;
        str = b.h;
        ((com.truecaller.messaging.d.a)localObject1).a(str);
      }
    }
    boolean bool3 = R();
    if (!bool3)
    {
      bool3 = paramDraft.a();
      if (bool3)
      {
        localObject1 = t.A();
        localr.c((String)localObject1);
      }
      else
      {
        localObject1 = c;
        localr.c((String)localObject1);
      }
    }
    localObject1 = i;
    if (localObject1 != null)
    {
      str = "it";
      c.g.b.k.a(localObject1, str);
      localr.a((ReplySnippet)localObject1);
    }
    else
    {
      localObject1 = (r)b;
      if (localObject1 != null)
      {
        boolean bool2 = paramDraft.b();
        if (bool2) {
          ((r)localObject1).B();
        } else {
          ((r)localObject1).C();
        }
      }
    }
    d = paramDraft;
    localObject1 = d;
    String str = "draft.participants";
    c.g.b.k.a(localObject1, str);
    int i3 = localObject1.length;
    boolean bool4 = false;
    int i4 = 0;
    while (i4 < i3)
    {
      Object localObject3 = localObject1[i4];
      int i5 = c;
      int i6 = 4;
      if (i5 != i6)
      {
        localObject3 = f;
        c.g.b.k.a(localObject3, "it.normalizedAddress");
        localObject3 = (CharSequence)localObject3;
        c.n.k localk = c;
        bool5 = localk.a((CharSequence)localObject3);
        if (bool5)
        {
          bool5 = true;
          break label395;
        }
      }
      boolean bool5 = false;
      localObject3 = null;
      label395:
      if (bool5)
      {
        bool4 = true;
        break;
      }
      i4 += 1;
    }
    j = bool4;
    localObject1 = d;
    bool3 = g.b((Participant[])localObject1);
    m = bool3;
    paramDraft = b;
    int i7;
    if (paramDraft != null)
    {
      i7 = r;
      localObject2 = Integer.valueOf(i7);
    }
    paramDraft = e;
    localObject1 = SendType.DEFAULT;
    if (paramDraft == localObject1)
    {
      if (localObject2 != null)
      {
        i7 = ((Integer)localObject2).intValue();
        if (i7 == 0)
        {
          paramDraft = SendType.SMS;
          break label509;
        }
      }
      paramDraft = SendType.DEFAULT;
      label509:
      e = paramDraft;
    }
    paramDraft = localr.f();
    b(paramDraft);
    N();
    V();
    boolean bool6 = A();
    int i1;
    if (bool6) {
      i1 = 2;
    }
    s.d(i1);
  }
  
  public final void a(Message paramMessage)
  {
    Object localObject = "message";
    c.g.b.k.b(paramMessage, (String)localObject);
    int i1 = j;
    int i2 = 2;
    if (i1 == i2)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      localObject = null;
    }
    String[] arrayOfString = new String[0];
    AssertionUtil.isTrue(i1, arrayOfString);
    localObject = (r)b;
    if (localObject != null) {
      ((r)localObject).j(false);
    }
    boolean bool = A();
    if (!bool) {
      a(i2, false, false);
    }
    localObject = d;
    if (localObject != null)
    {
      localObject = ((Draft)localObject).c();
      long l1 = paramMessage.a();
      localObject = ((Draft.a)localObject).b(l1);
      ReplySnippet localReplySnippet = new com/truecaller/messaging/data/types/ReplySnippet;
      localReplySnippet.<init>(paramMessage);
      paramMessage = ((Draft.a)localObject).a(localReplySnippet).d();
    }
    else
    {
      paramMessage = null;
    }
    d = paramMessage;
  }
  
  public final void a(CharSequence paramCharSequence)
  {
    c.g.b.k.b(paramCharSequence, "text");
    bi localbi = O;
    boolean bool = localbi.d();
    if (bool) {
      return;
    }
    b(paramCharSequence);
    V();
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "voiceNotePath");
    d("Finish");
    Object localObject1 = O;
    Object localObject2 = null;
    ((bi)localObject1).b(false);
    V();
    localObject1 = (r)b;
    if (localObject1 != null) {
      ((r)localObject1).a(false);
    }
    localObject1 = d;
    if (localObject1 == null) {
      return;
    }
    localObject2 = new java/io/File;
    ((File)localObject2).<init>(paramString);
    paramString = Uri.fromFile((File)localObject2);
    paramString = ((ba)N.a()).b(paramString, true);
    localObject2 = new com/truecaller/messaging/conversation/n$e;
    ((n.e)localObject2).<init>(this, (Draft)localObject1);
    localObject2 = (ac)localObject2;
    paramString.a((ac)localObject2);
    P();
  }
  
  public final void a(String paramString1, Message paramMessage, String paramString2)
  {
    c.g.b.k.b(paramMessage, "message");
    c.g.b.k.b(paramString2, "initiatedVia");
    ((t)H.a()).a(paramMessage, paramString1, paramString2);
  }
  
  public final void a(String paramString, ArrayList paramArrayList)
  {
    Object localObject = (r)b;
    if (localObject == null) {
      return;
    }
    ((r)localObject).c(paramString);
    if (paramArrayList != null)
    {
      paramArrayList = (Iterable)paramArrayList;
      paramString = c.a.m.e(paramArrayList);
      if (paramString != null)
      {
        boolean bool1 = paramString.isEmpty();
        if (bool1) {
          paramString = null;
        }
        if (paramString != null)
        {
          paramString = (Iterable)paramString;
          paramArrayList = new java/util/ArrayList;
          int i1 = c.a.m.a(paramString, 10);
          paramArrayList.<init>(i1);
          paramArrayList = (Collection)paramArrayList;
          paramString = paramString.iterator();
          for (;;)
          {
            boolean bool2 = paramString.hasNext();
            if (!bool2) {
              break;
            }
            localObject = (Uri)paramString.next();
            com.truecaller.messaging.d locald = new com/truecaller/messaging/d;
            locald.<init>((Uri)localObject);
            paramArrayList.add(locald);
          }
          paramArrayList = (List)paramArrayList;
          paramString = s;
          paramArrayList = (Collection)paramArrayList;
          localObject = Long.valueOf(C.a(2));
          paramString.a(paramArrayList, (Long)localObject, false);
          return;
        }
      }
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    boolean bool1 = T();
    boolean bool2 = true;
    Object localObject1;
    if (bool1)
    {
      localObject1 = (r)b;
      if (localObject1 != null)
      {
        localObject2 = x;
        String[] arrayOfString = { "android.permission.RECORD_AUDIO" };
        bool1 = ((com.truecaller.utils.l)localObject2).a(arrayOfString);
        if (!bool1)
        {
          localObject2 = "android.permission.RECORD_AUDIO";
          bool1 = ((r)localObject1).k((String)localObject2);
          if (bool1) {
            ((r)localObject1).J();
          } else {
            ((r)localObject1).G();
          }
          paramBoolean = true;
          break label213;
        }
        localObject2 = x;
        bool1 = ((com.truecaller.utils.l)localObject2).b();
        if (bool1)
        {
          localObject2 = x;
          bool1 = ((com.truecaller.utils.l)localObject2).c();
          if (!bool1) {}
        }
      }
      else
      {
        paramBoolean = false;
        localObject1 = null;
        break label213;
      }
      Object localObject2 = "android.permission.READ_EXTERNAL_STORAGE";
      bool1 = ((r)localObject1).k((String)localObject2);
      if (!bool1)
      {
        localObject2 = "android.permission.WRITE_EXTERNAL_STORAGE";
        bool1 = ((r)localObject1).k((String)localObject2);
        if (!bool1)
        {
          int i1 = 206;
          ((r)localObject1).q(i1);
          break label211;
        }
      }
      ((r)localObject1).E();
      label211:
      paramBoolean = true;
      label213:
      if (!paramBoolean)
      {
        localObject1 = (r)b;
        if (localObject1 != null) {
          ((r)localObject1).l(bool2);
        }
      }
    }
    else
    {
      if (paramBoolean)
      {
        localObject1 = (r)b;
        if (localObject1 != null) {
          ((r)localObject1).M();
        }
        return;
      }
      localObject1 = s;
      paramBoolean = ((bn)localObject1).i();
      if (paramBoolean) {
        return;
      }
      paramBoolean = R();
      if (!paramBoolean)
      {
        localObject1 = s;
        paramBoolean = ((bn)localObject1).h();
        if (!paramBoolean) {}
      }
      else
      {
        localObject1 = t;
        paramBoolean = ((com.truecaller.messaging.h)localObject1).f() ^ bool2;
        c(paramBoolean);
      }
    }
  }
  
  public final void a(boolean paramBoolean, Uri paramUri)
  {
    if ((paramBoolean) && (paramUri != null))
    {
      Object localObject = B;
      paramBoolean = ((com.truecaller.multisim.h)localObject).l();
      if (!paramBoolean)
      {
        paramBoolean = A();
        if (!paramBoolean)
        {
          c(paramUri);
          return;
        }
      }
      localObject = (r)b;
      if (localObject != null) {
        ((r)localObject).b(paramUri);
      }
      return;
    }
  }
  
  public final void a(boolean paramBoolean, com.truecaller.util.c.a parama)
  {
    if ((paramBoolean) && (parama != null))
    {
      r localr = (r)b;
      if (localr == null) {
        return;
      }
      Object localObject = new CharSequence[2];
      CharSequence localCharSequence = c.n.m.b(localr.f());
      localObject[0] = localCharSequence;
      parama = (CharSequence)z.a(parama);
      localObject[1] = parama;
      parama = c.a.m.n((Iterable)c.a.m.b((Object[])localObject));
      localObject = (c.g.a.b)n.f.a;
      parama = c.m.l.a(parama, (c.g.a.b)localObject);
      localObject = (CharSequence)"\n\n";
      parama = c.m.l.a(parama, (CharSequence)localObject);
      localr.c(parama);
      return;
    }
  }
  
  public final void a(boolean paramBoolean, List paramList)
  {
    String str = "uriList";
    c.g.b.k.b(paramList, str);
    if (paramBoolean)
    {
      paramBoolean = paramList.isEmpty();
      if (!paramBoolean)
      {
        paramList = (Iterable)paramList;
        Object localObject = c.a.m.e(paramList);
        boolean bool1 = ((List)localObject).isEmpty();
        str = null;
        if (bool1)
        {
          paramBoolean = false;
          localObject = null;
        }
        if (localObject != null)
        {
          localObject = (Iterable)localObject;
          paramList = new java/util/ArrayList;
          int i1 = c.a.m.a((Iterable)localObject, 10);
          paramList.<init>(i1);
          paramList = (Collection)paramList;
          localObject = ((Iterable)localObject).iterator();
          for (;;)
          {
            boolean bool2 = ((Iterator)localObject).hasNext();
            if (!bool2) {
              break;
            }
            Uri localUri = (Uri)((Iterator)localObject).next();
            com.truecaller.messaging.d locald = new com/truecaller/messaging/d;
            locald.<init>(localUri);
            paramList.add(locald);
          }
          paramList = (List)paramList;
          localObject = s;
          paramList = (Collection)paramList;
          ((bn)localObject).a(paramList, null, true);
          return;
        }
        return;
      }
    }
  }
  
  public final void b()
  {
    r localr = (r)b;
    if (localr != null)
    {
      localr.K();
      boolean bool = b(204);
      if (!bool) {
        localr.x();
      }
    }
    a("attachment", "gallery");
  }
  
  public final void b(Uri paramUri)
  {
    c.g.b.k.b(paramUri, "uri");
    paramUri = ((aa)A.a()).c(paramUri);
    i locali = p.a();
    Object localObject1 = new com/truecaller/messaging/conversation/n$c;
    Object localObject2 = this;
    localObject2 = (n)this;
    ((n.c)localObject1).<init>((n)localObject2);
    localObject1 = (c.g.a.b)localObject1;
    localObject2 = new com/truecaller/messaging/conversation/q;
    ((q)localObject2).<init>((c.g.a.b)localObject1);
    localObject2 = (ac)localObject2;
    paramUri.a(locali, (ac)localObject2);
  }
  
  final void b(CharSequence paramCharSequence)
  {
    Object localObject1 = (r)b;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = d;
    if (localObject2 == null) {
      return;
    }
    bn localbn = s;
    boolean bool1 = localbn.h();
    int i1 = 1;
    if (!bool1)
    {
      localbn = s;
      bool1 = localbn.i();
      if (!bool1)
      {
        bool1 = false;
        localbn = null;
        break label77;
      }
    }
    bool1 = true;
    label77:
    Object localObject3 = q;
    Object localObject4 = paramCharSequence.toString();
    Participant[] arrayOfParticipant = d;
    paramCharSequence = e;
    SendType localSendType = SendType.SMS;
    boolean bool2;
    if (paramCharSequence == localSendType) {
      bool2 = true;
    } else {
      bool2 = false;
    }
    com.truecaller.messaging.transport.a locala = f;
    boolean bool3 = ((com.truecaller.messaging.transport.m)localObject3).a((String)localObject4, bool1, arrayOfParticipant, bool2, locala);
    if (bool3)
    {
      paramCharSequence = f;
      int i2 = paramCharSequence.a();
      int i3 = 50;
      if (i2 > i3)
      {
        paramCharSequence = f;
        i2 = paramCharSequence.b();
        if (i2 <= i1)
        {
          bool4 = false;
          paramCharSequence = null;
          break label204;
        }
      }
      bool4 = true;
      label204:
      localObject3 = f;
      int i4 = ((com.truecaller.messaging.transport.a)localObject3).a();
      localObject4 = f;
      int i5 = ((com.truecaller.messaging.transport.a)localObject4).b();
      ((r)localObject1).a(bool4, i4, i5);
    }
    else
    {
      ((r)localObject1).a(false, 0, 0);
    }
    boolean bool4 = T();
    if (bool4)
    {
      bool5 = Z();
      if (bool5)
      {
        bool5 = true;
        break label288;
      }
    }
    boolean bool5 = false;
    localObject3 = null;
    label288:
    ((r)localObject1).l(bool5);
    localObject3 = q;
    localObject4 = d;
    bool5 = ((com.truecaller.messaging.transport.m)localObject3).a((Participant[])localObject4);
    if (bool5)
    {
      localObject2 = d;
      bool6 = g.c((Participant[])localObject2);
      if (!bool6)
      {
        bool6 = true;
        break label351;
      }
    }
    boolean bool6 = false;
    localObject2 = null;
    label351:
    i = bool6;
    bool6 = i;
    if ((bool6) && (!bool4))
    {
      bool4 = true;
    }
    else
    {
      bool4 = false;
      paramCharSequence = null;
    }
    ((r)localObject1).k(bool4);
    O();
    S();
    bool4 = A();
    if (bool4)
    {
      ((r)localObject1).m(i1);
      paramCharSequence = r;
      paramCharSequence.d();
      bool4 = M();
      ((r)localObject1).j(bool4);
      if (bool4)
      {
        bool4 = l;
        if (!bool4)
        {
          paramCharSequence = K;
          localObject1 = "ab_test_poke_emoji_17105_seen";
          paramCharSequence.a((String)localObject1);
        }
        l = i1;
      }
    }
    else
    {
      bool4 = bool1 ^ true;
      ((r)localObject1).m(bool4);
      paramCharSequence = r;
      paramCharSequence.c();
      ((r)localObject1).j(false);
    }
  }
  
  final void b(String paramString)
  {
    Object localObject1 = (r)b;
    int i1 = 0;
    int i2 = 1;
    if (localObject1 != null)
    {
      localObject1 = ((r)localObject1).f();
      if (localObject1 != null)
      {
        int i3 = ((CharSequence)localObject1).length();
        if (i3 > 0)
        {
          i3 = 1;
        }
        else
        {
          i3 = 0;
          localObject1 = null;
        }
        if (i3 == i2)
        {
          localObject1 = "Yes";
          break label70;
        }
      }
    }
    localObject1 = "No";
    label70:
    Object localObject2 = e;
    Object localObject3 = SendType.SMS;
    if (localObject2 != localObject3)
    {
      localObject2 = f;
      i4 = ((com.truecaller.messaging.transport.a)localObject2).c();
      i5 = 2;
      if (i4 == i5)
      {
        i4 = 0;
        localObject2 = null;
        break label126;
      }
    }
    int i4 = 1;
    label126:
    if (i4 != 0) {
      localObject2 = "SMS";
    } else {
      localObject2 = "IM";
    }
    int i5 = 0;
    localObject3 = null;
    Object localObject4 = s.g();
    Object localObject5 = "draftEntityPresenter.mediaEntities";
    c.g.b.k.a(localObject4, (String)localObject5);
    boolean bool1 = ((Collection)localObject4).isEmpty() ^ i2;
    if (bool1)
    {
      localObject3 = new java/util/LinkedHashMap;
      ((LinkedHashMap)localObject3).<init>();
      localObject3 = (Map)localObject3;
      localObject4 = new com/truecaller/messaging/conversation/n$b;
      ((n.b)localObject4).<init>((Map)localObject3);
      localObject5 = s.g().iterator();
      for (;;)
      {
        boolean bool3 = ((Iterator)localObject5).hasNext();
        if (!bool3) {
          break;
        }
        Object localObject6 = (BinaryEntity)((Iterator)localObject5).next();
        String str1 = "entity";
        c.g.b.k.a(localObject6, str1);
        boolean bool4 = ((BinaryEntity)localObject6).b();
        if (bool4)
        {
          localObject6 = "Image";
          ((n.b)localObject4).a((String)localObject6);
        }
        else
        {
          bool4 = ((BinaryEntity)localObject6).c();
          if (bool4)
          {
            localObject6 = "Video";
            ((n.b)localObject4).a((String)localObject6);
          }
          else
          {
            bool3 = ((BinaryEntity)localObject6).d();
            if (bool3)
            {
              localObject6 = "VCard";
              ((n.b)localObject4).a((String)localObject6);
            }
          }
        }
      }
      int i6 = ((Map)localObject3).size();
      if (i6 == i2) {
        str2 = (String)c.a.m.b((Iterable)((Map)localObject3).keySet(), 0);
      } else {
        str2 = "Mixed";
      }
      localObject3 = ((Map)localObject3).entrySet().iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject3).hasNext();
        if (!bool2) {
          break;
        }
        localObject4 = (Number)((Map.Entry)((Iterator)localObject3).next()).getValue();
        int i7 = ((Number)localObject4).intValue();
        i1 += i7;
      }
    }
    i2 = 0;
    String str2 = null;
    localObject3 = new com/truecaller/analytics/e$a;
    ((e.a)localObject3).<init>("DraftInfo");
    localObject4 = "Event";
    paramString = ((e.a)localObject3).a((String)localObject4, paramString);
    localObject3 = "HasText";
    paramString = paramString.a((String)localObject3, (String)localObject1);
    localObject1 = "Transport";
    paramString = paramString.a((String)localObject1, (String)localObject2);
    if (str2 != null)
    {
      localObject1 = paramString.a("MediaType", str2);
      str2 = "MediaCount";
      ((e.a)localObject1).a(str2, i1);
    }
    localObject1 = w;
    paramString = paramString.a();
    c.g.b.k.a(paramString, "eventBuilder.build()");
    ((ap)localObject1).a(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      Object localObject = y;
      paramBoolean = ((com.truecaller.utils.d)localObject).d();
      if (paramBoolean)
      {
        c(false);
        paramBoolean = true;
        g = paramBoolean;
        localObject = (r)b;
        if (localObject != null)
        {
          ((r)localObject).O();
          return;
        }
        return;
      }
    }
  }
  
  public final void c()
  {
    r localr = (r)b;
    if (localr != null) {
      localr.K();
    }
    localr = (r)b;
    if (localr != null) {
      localr.y();
    }
    a("attachment", "location");
  }
  
  public final void e()
  {
    r localr = (r)b;
    if (localr != null) {
      localr.K();
    }
    boolean bool = b(205);
    if (!bool)
    {
      localr = (r)b;
      if (localr != null) {
        localr.z();
      }
    }
    a("attachment", "contact");
  }
  
  public final void f()
  {
    Object localObject1 = (r)b;
    if (localObject1 != null) {
      ((r)localObject1).K();
    }
    localObject1 = d;
    boolean bool1 = false;
    Object localObject2 = null;
    if (localObject1 != null)
    {
      localObject1 = d;
    }
    else
    {
      i1 = 0;
      localObject1 = null;
    }
    Object localObject3 = null;
    if (localObject1 == null) {
      localObject1 = new Participant[0];
    }
    Object localObject4 = new java/util/ArrayList;
    ((ArrayList)localObject4).<init>();
    localObject4 = (Collection)localObject4;
    int i2 = localObject1.length;
    int i3 = 0;
    while (i3 < i2)
    {
      Object localObject5 = localObject1[i3];
      String str1 = f;
      c.g.b.k.a(str1, "it.normalizedAddress");
      String str2 = "";
      str1 = c.n.m.a(str1, "+", str2);
      Object localObject6 = G.h(str1);
      boolean bool2 = c;
      if (bool2)
      {
        localObject6 = new com/truecaller/flashsdk/models/FlashContact;
        c.g.b.k.a(localObject5, "it");
        localObject5 = ((Participant)localObject5).a();
        str2 = "it.displayName";
        c.g.b.k.a(localObject5, str2);
        ((FlashContact)localObject6).<init>(str1, (String)localObject5, null);
      }
      else
      {
        bool2 = false;
        localObject6 = null;
      }
      if (localObject6 != null) {
        ((Collection)localObject4).add(localObject6);
      }
      i3 += 1;
    }
    localObject4 = (List)localObject4;
    int i1 = ((List)localObject4).size();
    switch (i1)
    {
    default: 
      localObject1 = (r)b;
      if (localObject1 != null)
      {
        localObject4 = (ArrayList)localObject4;
        ((r)localObject1).b((ArrayList)localObject4);
      }
      break;
    case 1: 
      localObject1 = (FlashContact)((List)localObject4).get(0);
      localObject2 = G;
      localObject3 = a;
      bool1 = ((com.truecaller.flashsdk.core.b)localObject2).a((String)localObject3);
      if (bool1)
      {
        localObject2 = new android/os/Bundle;
        ((Bundle)localObject2).<init>();
        ((Bundle)localObject2).putString("flash_context", "conversation");
        localObject3 = com.truecaller.flashsdk.core.c.a();
        localObject4 = "ANDROID_FLASH_TAPPED";
        ((com.truecaller.flashsdk.core.b)localObject3).a((String)localObject4, (Bundle)localObject2);
      }
      localObject2 = (r)b;
      if (localObject2 != null) {
        ((r)localObject2).a((FlashContact)localObject1);
      }
      break;
    }
    a("attachment", "flash");
  }
  
  public final void g()
  {
    c(false);
  }
  
  public final void h()
  {
    com.truecaller.messaging.h localh = t;
    boolean bool1 = true;
    localh.f(bool1);
    boolean bool2 = t.f() ^ bool1;
    c(bool2);
  }
  
  public final void i()
  {
    t.f(false);
    boolean bool = t.f() ^ true;
    c(bool);
  }
  
  public final void j()
  {
    boolean bool1 = A();
    com.truecaller.messaging.e locale;
    int i1;
    int i2;
    Object localObject;
    int i4;
    int i5;
    int i7;
    if (bool1)
    {
      localr = (r)b;
      if (localr != null)
      {
        locale = new com/truecaller/messaging/e;
        i1 = SendType.SMS.ordinal();
        i2 = u.a();
        localObject = u;
        int i3 = Q();
        i4 = ((bf)localObject).a(i3);
        localObject = u;
        i5 = ((bf)localObject).b(0);
        boolean bool2 = Q();
        int i6;
        if (bool2)
        {
          i6 = 2131888777;
          i7 = 2131888777;
        }
        else
        {
          i6 = 2131888778;
          i7 = 2131888778;
        }
        localObject = locale;
        locale.<init>(i1, i2, i4, i5, i7);
        localObject = c.a.m.a(locale);
        localr.a((List)localObject);
      }
      return;
    }
    r localr = (r)b;
    if (localr != null)
    {
      locale = new com/truecaller/messaging/e;
      i1 = SendType.IM.ordinal();
      i2 = u.b();
      localObject = u;
      i4 = 2;
      i5 = ((bf)localObject).a(i4);
      i7 = u.b(i4);
      localObject = locale;
      i4 = i5;
      i5 = i7;
      i7 = 2131888776;
      locale.<init>(i1, i2, i4, i5, i7);
      localObject = c.a.m.a(locale);
      localr.a((List)localObject);
      return;
    }
  }
  
  public final boolean k()
  {
    r localr = (r)b;
    if (localr == null) {
      return false;
    }
    boolean bool = localr.N();
    if (bool)
    {
      localr.M();
      return true;
    }
    return localr.q();
  }
  
  public final void l()
  {
    boolean bool1 = A();
    boolean bool2;
    boolean bool3;
    label51:
    boolean bool4;
    if (bool1)
    {
      localObject1 = E.e();
      bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
      bool2 = false;
      bool3 = true;
      if (!bool1)
      {
        bool1 = W();
        if (bool1)
        {
          bool1 = true;
          break label51;
        }
      }
      bool1 = false;
      localObject1 = null;
      Object localObject2 = E.e();
      bool4 = ((com.truecaller.featuretoggles.b)localObject2).a();
      if (!bool4)
      {
        bool4 = X();
        if (bool4) {
          bool2 = true;
        }
      }
      localObject2 = (r)b;
      if (localObject2 != null) {
        ((r)localObject2).a(bool1, bool3, bool2);
      }
      return;
    }
    Object localObject1 = s;
    bool1 = ((bn)localObject1).h();
    if (!bool1)
    {
      localObject1 = s;
      bool1 = ((bn)localObject1).i();
      if (!bool1)
      {
        localObject1 = (r)b;
        if (localObject1 != null)
        {
          bool2 = W();
          bool3 = B.l();
          bool4 = X();
          ((r)localObject1).a(bool2, bool3, bool4);
          return;
        }
        return;
      }
    }
    localObject1 = (r)b;
    if (localObject1 != null)
    {
      int i1 = 2131886409;
      ((r)localObject1).m(i1);
    }
  }
  
  public final void m()
  {
    Object localObject1 = d;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = ((Draft)localObject1).c();
    Object localObject3 = k;
    if (localObject3 == null)
    {
      localObject4 = "defaultPokeableEmoji";
      c.g.b.k.a((String)localObject4);
    }
    localObject3 = ((PokeableEmoji)localObject3).getEmoji();
    localObject2 = ((Draft.a)localObject2).a((String)localObject3).b().d().a("-1");
    c.g.b.k.a(localObject2, "draft.buildUpon()\n      …anager.SIM_TOKEN_UNKNOWN)");
    Object localObject4 = L;
    String str1 = o;
    String str2 = "conversation";
    int i1 = 2;
    Participant[] arrayOfParticipant = d;
    c.g.b.k.a(arrayOfParticipant, "draft.participants");
    String str3 = "UserInput";
    ((com.truecaller.messaging.c.a)localObject4).a(str1, str2, i1, arrayOfParticipant, str3);
    localObject3 = q;
    localObject1 = d;
    localObject4 = null;
    localObject1 = ((com.truecaller.messaging.transport.m)localObject3).a((Message)localObject2, (Participant[])localObject1, false, false);
    localObject2 = new com/truecaller/messaging/conversation/n$d;
    ((n.d)localObject2).<init>(this);
    localObject2 = (ac)localObject2;
    ((w)localObject1).a((ac)localObject2);
    localObject1 = (m.a)a.get();
    localObject2 = k;
    if (localObject2 == null)
    {
      localObject3 = "defaultPokeableEmoji";
      c.g.b.k.a((String)localObject3);
    }
    ((m.a)localObject1).a((PokeableEmoji)localObject2);
    localObject1 = (r)b;
    if (localObject1 != null) {
      ((r)localObject1).j(false);
    }
    K.a("ab_test_poke_emoji_17105_converted");
    localObject1 = w;
    localObject2 = new com/truecaller/analytics/e$a;
    ((e.a)localObject2).<init>("ImEmojiPoke");
    localObject2 = ((e.a)localObject2).a("Action", "Sent");
    localObject3 = "Emoji";
    localObject4 = k;
    if (localObject4 == null)
    {
      str1 = "defaultPokeableEmoji";
      c.g.b.k.a(str1);
    }
    localObject4 = ((PokeableEmoji)localObject4).getAnalyticsName();
    localObject2 = ((e.a)localObject2).a((String)localObject3, (String)localObject4).a();
    c.g.b.k.a(localObject2, "AnalyticsEvent.Builder(I…\n                .build()");
    ((ap)localObject1).a((com.truecaller.analytics.e)localObject2);
  }
  
  public final void n()
  {
    a("smiley", "button");
  }
  
  public final void o()
  {
    Object localObject = (r)b;
    if (localObject != null)
    {
      localObject = ((r)localObject).f();
      b((CharSequence)localObject);
      return;
    }
  }
  
  public final void p()
  {
    d("Initiate");
    Object localObject = O;
    boolean bool = true;
    ((bi)localObject).b(bool);
    V();
    localObject = (r)b;
    if (localObject != null) {
      ((r)localObject).a(bool);
    }
    localObject = o;
    RecordView.RecordState localRecordState = RecordView.RecordState.RECORD;
    if (localObject == localRecordState) {
      return;
    }
    localObject = RecordView.RecordState.RECORD;
    o = ((RecordView.RecordState)localObject);
    localObject = (r)b;
    if (localObject != null)
    {
      ((r)localObject).b();
      return;
    }
  }
  
  public final void q()
  {
    d("Cancel");
    O.b(false);
    V();
    r localr = (r)b;
    if (localr != null)
    {
      localr.a(false);
      return;
    }
  }
  
  public final void r()
  {
    r localr = (r)b;
    if (localr != null)
    {
      localr.a(false);
      return;
    }
  }
  
  public final void s()
  {
    Object localObject = o;
    RecordView.RecordState localRecordState = RecordView.RecordState.DELETE;
    if (localObject == localRecordState) {
      return;
    }
    localObject = RecordView.RecordState.DELETE;
    o = ((RecordView.RecordState)localObject);
    localObject = (r)b;
    if (localObject != null)
    {
      ((r)localObject).c();
      return;
    }
  }
  
  public final void t()
  {
    Object localObject = o;
    RecordView.RecordState localRecordState = RecordView.RecordState.RECORD;
    if (localObject == localRecordState) {
      return;
    }
    localObject = RecordView.RecordState.RECORD;
    o = ((RecordView.RecordState)localObject);
    localObject = (r)b;
    if (localObject != null)
    {
      ((r)localObject).b();
      return;
    }
  }
  
  public final Draft u()
  {
    Object localObject1 = d;
    Object localObject2 = null;
    if (localObject1 == null) {
      return null;
    }
    b("Saved");
    Object localObject3 = (r)b;
    if (localObject3 != null)
    {
      localObject3 = ((r)localObject3).f();
      if (localObject3 != null)
      {
        localObject3 = localObject3.toString();
        if (localObject3 != null)
        {
          String str = t.A();
          boolean bool = c.g.b.k.a(localObject3, str) ^ true;
          if (bool) {
            localObject2 = localObject3;
          }
          if (localObject2 != null) {
            break label88;
          }
        }
      }
    }
    localObject2 = "";
    label88:
    localObject1 = ((Draft)localObject1).c().a((String)localObject2).b();
    localObject2 = s.g();
    localObject1 = ((Draft.a)localObject1).a((Collection)localObject2).d();
    c.g.b.k.a(localObject1, "draft.buildUpon()\n      …ies)\n            .build()");
    s.e();
    return (Draft)localObject1;
  }
  
  public final void v()
  {
    Object localObject = (r)b;
    if (localObject != null)
    {
      bool = false;
      ((r)localObject).c(null);
    }
    localObject = s;
    boolean bool = true;
    ((bn)localObject).a(bool);
    localObject = (r)b;
    if (localObject != null)
    {
      ((r)localObject).C();
      return;
    }
  }
  
  public final boolean w()
  {
    return s.h();
  }
  
  public final boolean x()
  {
    return s.i();
  }
  
  public final void y()
  {
    boolean bool1 = true;
    h = bool1;
    boolean bool2 = t.f();
    bool1 ^= bool2;
    c(bool1);
  }
  
  public final void y_()
  {
    r localr = (r)b;
    if (localr != null) {
      localr.r();
    }
    s.j();
    super.y_();
  }
  
  public final void z()
  {
    SendType localSendType1 = e;
    SendType localSendType2 = SendType.SMS;
    int i1 = 1;
    if (localSendType1 == localSendType2)
    {
      a(2, i1, i1);
      return;
    }
    a(i1, i1, i1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */