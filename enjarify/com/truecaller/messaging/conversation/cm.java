package com.truecaller.messaging.conversation;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Xfermode;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.support.v7.widget.helper.ItemTouchHelper.SimpleCallback;
import android.view.View;
import android.view.View.OnTouchListener;
import c.g.b.k;
import com.truecaller.util.at;

public final class cm
  extends ItemTouchHelper.SimpleCallback
{
  private final Paint a;
  private final Drawable b;
  private final Drawable c;
  private final int d;
  private final int e;
  private final int f;
  private boolean g;
  private View h;
  private final cm.a i;
  
  public cm(Context paramContext, cm.a parama)
  {
    super(0, 8);
    i = parama;
    parama = new android/graphics/Paint;
    parama.<init>();
    Object localObject = new android/graphics/PorterDuffXfermode;
    PorterDuff.Mode localMode = PorterDuff.Mode.CLEAR;
    ((PorterDuffXfermode)localObject).<init>(localMode);
    localObject = (Xfermode)localObject;
    parama.setXfermode((Xfermode)localObject);
    a = parama;
    parama = android.support.v4.content.b.a(paramContext, 2131233805);
    b = parama;
    parama = android.support.v4.content.b.a(paramContext, 2131234639);
    c = parama;
    int j = at.a(paramContext, 70.0F);
    d = j;
    j = com.truecaller.utils.ui.b.a(paramContext, 2130969219);
    e = j;
    int k = com.truecaller.utils.ui.b.a(paramContext, 2130969218);
    f = k;
  }
  
  public final int convertToAbsoluteDirection(int paramInt1, int paramInt2)
  {
    boolean bool = g;
    if (bool)
    {
      g = false;
      return 0;
    }
    return super.convertToAbsoluteDirection(paramInt1, paramInt2);
  }
  
  public final int getMovementFlags(RecyclerView paramRecyclerView, RecyclerView.ViewHolder paramViewHolder)
  {
    k.b(paramRecyclerView, "recyclerView");
    Object localObject = "viewHolder";
    k.b(paramViewHolder, (String)localObject);
    boolean bool = paramViewHolder instanceof cb;
    if (bool)
    {
      localObject = paramViewHolder;
      localObject = (cb)paramViewHolder;
      bool = a;
      if (bool)
      {
        localObject = itemView;
        k.a(localObject, "viewHolder.itemView");
        h = ((View)localObject);
        return super.getMovementFlags(paramRecyclerView, (RecyclerView.ViewHolder)paramViewHolder);
      }
    }
    return 0;
  }
  
  public final void onChildDraw(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.ViewHolder paramViewHolder, float paramFloat1, float paramFloat2, int paramInt, boolean paramBoolean)
  {
    k.b(paramCanvas, "c");
    k.b(paramRecyclerView, "recyclerView");
    Object localObject1 = "viewHolder";
    k.b(paramViewHolder, (String)localObject1);
    int j = 1;
    float f1 = Float.MIN_VALUE;
    if (paramInt == j)
    {
      localObject2 = new com/truecaller/messaging/conversation/cm$b;
      ((cm.b)localObject2).<init>(this, paramViewHolder);
      localObject2 = (View.OnTouchListener)localObject2;
      paramRecyclerView.setOnTouchListener((View.OnTouchListener)localObject2);
    }
    Object localObject2 = null;
    boolean bool1 = paramFloat1 < 0.0F;
    if ((bool1) || (paramBoolean))
    {
      j = 0;
      f1 = 0.0F;
      localObject1 = null;
    }
    if (j != 0)
    {
      localObject1 = h;
      if (localObject1 == null)
      {
        localObject2 = "mView";
        k.a((String)localObject2);
      }
      j = ((View)localObject1).getRight();
      f1 = j;
      float f2 = f1 + paramFloat1;
      localObject1 = h;
      if (localObject1 == null)
      {
        localObject2 = "mView";
        k.a((String)localObject2);
      }
      j = ((View)localObject1).getTop();
      float f3 = j;
      localObject1 = h;
      if (localObject1 == null)
      {
        localObject2 = "mView";
        k.a((String)localObject2);
      }
      j = ((View)localObject1).getRight();
      float f4 = j;
      localObject1 = h;
      if (localObject1 == null)
      {
        localObject2 = "mView";
        k.a((String)localObject2);
      }
      j = ((View)localObject1).getBottom();
      float f5 = j;
      if (paramCanvas != null)
      {
        Paint localPaint = a;
        localObject2 = paramCanvas;
        paramCanvas.drawRect(f2, f3, f4, f5, localPaint);
      }
      super.onChildDraw(paramCanvas, paramRecyclerView, paramViewHolder, paramFloat1, paramFloat2, paramInt, paramBoolean);
      return;
    }
    super.onChildDraw(paramCanvas, paramRecyclerView, paramViewHolder, paramFloat1, paramFloat2, paramInt, paramBoolean);
    paramRecyclerView = c;
    if (paramRecyclerView != null)
    {
      k = f;
      localObject3 = PorterDuff.Mode.SRC_IN;
      paramRecyclerView.setColorFilter(k, (PorterDuff.Mode)localObject3);
    }
    paramRecyclerView = b;
    if (paramRecyclerView != null)
    {
      k = e;
      localObject3 = PorterDuff.Mode.SRC_IN;
      paramRecyclerView.setColorFilter(k, (PorterDuff.Mode)localObject3);
    }
    paramRecyclerView = h;
    if (paramRecyclerView == null)
    {
      paramViewHolder = "mView";
      k.a(paramViewHolder);
    }
    float f6 = paramRecyclerView.getTranslationX();
    int k = 1073741824;
    float f7 = 2.0F;
    f6 /= f7;
    boolean bool2 = (int)f6;
    paramViewHolder = h;
    if (paramViewHolder == null)
    {
      localObject3 = "mView";
      k.a((String)localObject3);
    }
    k = paramViewHolder.getTop();
    Object localObject3 = h;
    if (localObject3 == null)
    {
      String str = "mView";
      k.a(str);
    }
    int n = ((View)localObject3).getMeasuredHeight() / 2;
    k += n;
    localObject3 = c;
    boolean bool4;
    Drawable localDrawable1;
    Drawable localDrawable2;
    if (localObject3 != null)
    {
      boolean bool3 = ((Drawable)localObject3).getIntrinsicWidth() / 2;
      bool4 = bool2 - bool3;
      localDrawable1 = c;
      paramInt = localDrawable1.getIntrinsicHeight() / 2;
      paramInt = k - paramInt;
      localDrawable2 = c;
      paramBoolean = localDrawable2.getIntrinsicWidth() / 2 + bool2;
      localObject1 = c;
      j = ((Drawable)localObject1).getIntrinsicHeight() / 2 + k;
      ((Drawable)localObject3).setBounds(bool4, paramInt, paramBoolean, j);
    }
    localObject3 = c;
    if (localObject3 != null) {
      ((Drawable)localObject3).draw(paramCanvas);
    }
    localObject3 = b;
    if (localObject3 != null)
    {
      bool4 = ((Drawable)localObject3).getIntrinsicWidth() / 2;
      int i1 = bool2 - bool4;
      localDrawable1 = b;
      paramInt = localDrawable1.getIntrinsicHeight() / 2;
      paramInt = k - paramInt;
      paramBoolean = b.getIntrinsicWidth() / 2;
      int m;
      bool2 += paramBoolean;
      localDrawable2 = b;
      paramBoolean = localDrawable2.getIntrinsicHeight() / 2;
      k += paramBoolean;
      ((Drawable)localObject3).setBounds(i1, paramInt, m, k);
    }
    paramRecyclerView = b;
    if (paramRecyclerView != null)
    {
      paramRecyclerView.draw(paramCanvas);
      return;
    }
  }
  
  public final boolean onMove(RecyclerView paramRecyclerView, RecyclerView.ViewHolder paramViewHolder1, RecyclerView.ViewHolder paramViewHolder2)
  {
    k.b(paramRecyclerView, "recyclerView");
    k.b(paramViewHolder1, "fromViewHolder");
    k.b(paramViewHolder2, "toViewHolder");
    return false;
  }
  
  public final void onSwiped(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    k.b(paramViewHolder, "viewHolder");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.cm
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */