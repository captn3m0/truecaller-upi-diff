package com.truecaller.messaging.conversation;

import dagger.a.d;
import javax.inject.Provider;

public final class by
  implements d
{
  private final Provider a;
  private final Provider b;
  
  private by(Provider paramProvider1, Provider paramProvider2)
  {
    a = paramProvider1;
    b = paramProvider2;
  }
  
  public static by a(Provider paramProvider1, Provider paramProvider2)
  {
    by localby = new com/truecaller/messaging/conversation/by;
    localby.<init>(paramProvider1, paramProvider2);
    return localby;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.by
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */