package com.truecaller.messaging.conversation.voice_notes;

import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.support.v4.content.b.f;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.log.AssertionUtil;
import com.truecaller.util.at;
import com.truecaller.utils.extensions.i;
import com.truecaller.utils.extensions.l;
import com.truecaller.utils.extensions.t;
import com.truecaller.utils.ui.b;
import java.io.File;
import java.util.HashMap;
import java.util.Objects;

public final class RecordView
  extends RelativeLayout
{
  RecordView.a a;
  long b;
  float c;
  float d;
  float e;
  boolean f;
  long g;
  float h = 8.0F;
  MediaRecorder i;
  public boolean j;
  final String k;
  final int l;
  a m;
  private long n;
  private HashMap o;
  
  public RecordView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private RecordView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    paramAttributeSet = new java/lang/StringBuilder;
    paramAttributeSet.<init>();
    File localFile = paramContext.getCacheDir();
    paramAttributeSet.append(localFile);
    paramAttributeSet.append("/voice-note");
    long l1 = System.currentTimeMillis();
    paramAttributeSet.append(l1);
    paramAttributeSet.append(".aac");
    paramAttributeSet = paramAttributeSet.toString();
    k = paramAttributeSet;
    paramAttributeSet = getResources();
    k.a(paramAttributeSet, "resources");
    int i1 = getDisplayMetricswidthPixels / 2;
    l = i1;
    paramAttributeSet = this;
    paramAttributeSet = (ViewGroup)this;
    RelativeLayout.inflate(paramContext, 2131559217, paramAttributeSet);
    i1 = R.id.tvSlideToCancel;
    paramAttributeSet = (TextView)a(i1);
    paramContext = at.b(paramContext, 2131234876, 2130969391);
    paramAttributeSet.setCompoundDrawablesRelativeWithIntrinsicBounds(paramContext, null, null, null);
    t.a(this, false);
  }
  
  private final void b()
  {
    try
    {
      localObject1 = i;
      if (localObject1 != null) {
        ((MediaRecorder)localObject1).stop();
      }
      localObject1 = null;
      j = false;
      localObject1 = m;
      if (localObject1 == null)
      {
        localObject2 = "audioFocusHandler";
        k.a((String)localObject2);
      }
      Object localObject2 = getContext();
      String str = "context";
      k.a(localObject2, str);
      localObject2 = i.i((Context)localObject2);
      ((a)localObject1).a((AudioManager)localObject2);
    }
    catch (RuntimeException localRuntimeException)
    {
      localObject1 = (Exception)localRuntimeException;
      a((Exception)localObject1);
    }
    Object localObject1 = i;
    if (localObject1 != null)
    {
      ((MediaRecorder)localObject1).release();
      return;
    }
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = o;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      o = ((HashMap)localObject1);
    }
    localObject1 = o;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = o;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a()
  {
    b();
    File localFile = new java/io/File;
    String str = k;
    localFile.<init>(str);
    l.b(localFile);
  }
  
  final void a(RecordFloatingActionButton paramRecordFloatingActionButton, TextView paramTextView, float paramFloat1, float paramFloat2)
  {
    Object localObject1 = new float[2];
    float f1 = paramRecordFloatingActionButton.getX();
    localObject1[0] = f1;
    localObject1[1] = paramFloat1;
    localObject1 = ValueAnimator.ofFloat((float[])localObject1);
    k.a(localObject1, "positionAnimator");
    Object localObject2 = new android/view/animation/AccelerateDecelerateInterpolator;
    ((AccelerateDecelerateInterpolator)localObject2).<init>();
    localObject2 = (TimeInterpolator)localObject2;
    ((ValueAnimator)localObject1).setInterpolator((TimeInterpolator)localObject2);
    localObject2 = new com/truecaller/messaging/conversation/voice_notes/RecordView$b;
    ((RecordView.b)localObject2).<init>(paramRecordFloatingActionButton);
    localObject2 = (ValueAnimator.AnimatorUpdateListener)localObject2;
    ((ValueAnimator)localObject1).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject2);
    f1 = 1.0F;
    paramRecordFloatingActionButton.a(f1);
    long l1 = 0L;
    ((ValueAnimator)localObject1).setDuration(l1);
    ((ValueAnimator)localObject1).start();
    localObject1 = null;
    boolean bool = paramFloat2 < 0.0F;
    if (bool)
    {
      paramFloat1 -= paramFloat2;
      paramTextView = paramTextView.animate().x(paramFloat1).setDuration(l1);
      paramTextView.start();
    }
    paramTextView = RecordView.RecordState.RECORD;
    a(paramRecordFloatingActionButton, paramTextView);
  }
  
  final void a(RecordFloatingActionButton paramRecordFloatingActionButton, RecordView.RecordState paramRecordState)
  {
    Object localObject1 = getContext();
    int i1 = paramRecordState.getColor();
    localObject1 = b.b((Context)localObject1, i1);
    paramRecordFloatingActionButton.setBackgroundTintList((ColorStateList)localObject1);
    localObject1 = getContext();
    k.a(localObject1, "context");
    localObject1 = ((Context)localObject1).getResources();
    int i2 = paramRecordState.getIcon();
    Object localObject2 = getContext();
    k.a(localObject2, "context");
    localObject2 = ((Context)localObject2).getTheme();
    paramRecordState = android.support.v4.graphics.drawable.a.e((Drawable)Objects.requireNonNull(f.a((Resources)localObject1, i2, (Resources.Theme)localObject2))).mutate();
    int i3 = getResources().getColor(2131100499);
    android.support.v4.graphics.drawable.a.a(paramRecordState, i3);
    paramRecordFloatingActionButton.setImageDrawable(paramRecordState);
  }
  
  final void a(Exception paramException)
  {
    AssertionUtil.reportThrowableButNeverCrash((Throwable)paramException);
    j = false;
    paramException = a;
    if (paramException != null) {
      paramException.c();
    }
    paramException = new java/io/File;
    String str = k;
    paramException.<init>(str);
    l.b(paramException);
  }
  
  public final long getMaxDurationMs()
  {
    return b;
  }
  
  public final RecordView.a getRecordListener()
  {
    return a;
  }
  
  public final void setMaxDurationMs(long paramLong)
  {
    b = paramLong;
  }
  
  public final void setRecordListener(RecordView.a parama)
  {
    a = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.voice_notes.RecordView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */