package com.truecaller.messaging.conversation.voice_notes;

import android.media.AudioFocusRequest;
import android.media.AudioManager;
import c.g.b.k;

final class c
  implements a
{
  private final AudioFocusRequest a;
  
  public c(AudioFocusRequest paramAudioFocusRequest)
  {
    a = paramAudioFocusRequest;
  }
  
  public final void a(AudioManager paramAudioManager)
  {
    k.b(paramAudioManager, "audioManager");
    AudioFocusRequest localAudioFocusRequest = a;
    paramAudioManager.abandonAudioFocusRequest(localAudioFocusRequest);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.voice_notes.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */