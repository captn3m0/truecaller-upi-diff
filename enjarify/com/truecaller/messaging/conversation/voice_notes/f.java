package com.truecaller.messaging.conversation.voice_notes;

import android.net.Uri;

public abstract interface f
{
  public abstract void a();
  
  public abstract void a(Uri paramUri);
  
  public abstract boolean b();
  
  public abstract void c();
  
  public abstract void d();
  
  public abstract void e();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.voice_notes.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */