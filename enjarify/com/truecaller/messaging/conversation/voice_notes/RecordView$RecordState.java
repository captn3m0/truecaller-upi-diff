package com.truecaller.messaging.conversation.voice_notes;

public enum RecordView$RecordState
{
  private final int color;
  private final int icon;
  
  static
  {
    RecordState[] arrayOfRecordState = new RecordState[2];
    RecordState localRecordState = new com/truecaller/messaging/conversation/voice_notes/RecordView$RecordState;
    localRecordState.<init>("RECORD", 0, 2131234669, 2130969226);
    RECORD = localRecordState;
    arrayOfRecordState[0] = localRecordState;
    localRecordState = new com/truecaller/messaging/conversation/voice_notes/RecordView$RecordState;
    int i = 1;
    localRecordState.<init>("DELETE", i, 2131234668, 2130969230);
    DELETE = localRecordState;
    arrayOfRecordState[i] = localRecordState;
    $VALUES = arrayOfRecordState;
  }
  
  private RecordView$RecordState(int paramInt2, int paramInt3)
  {
    icon = paramInt2;
    color = paramInt3;
  }
  
  public final int getColor()
  {
    return color;
  }
  
  public final int getIcon()
  {
    return icon;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.voice_notes.RecordView.RecordState
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */