package com.truecaller.messaging.conversation.voice_notes;

import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import c.g.b.k;

final class b
  implements a
{
  private final AudioManager.OnAudioFocusChangeListener a;
  
  public b(AudioManager.OnAudioFocusChangeListener paramOnAudioFocusChangeListener)
  {
    a = paramOnAudioFocusChangeListener;
  }
  
  public final void a(AudioManager paramAudioManager)
  {
    k.b(paramAudioManager, "audioManager");
    AudioManager.OnAudioFocusChangeListener localOnAudioFocusChangeListener = a;
    paramAudioManager.abandonAudioFocus(localOnAudioFocusChangeListener);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.voice_notes.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */