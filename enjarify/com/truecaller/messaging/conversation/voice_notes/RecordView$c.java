package com.truecaller.messaging.conversation.voice_notes;

final class RecordView$c
  implements Runnable
{
  RecordView$c(RecordView paramRecordView) {}
  
  public final void run()
  {
    Object localObject = a;
    long l1 = System.currentTimeMillis();
    RecordView localRecordView = a;
    long l2 = RecordView.b(localRecordView);
    l1 -= l2;
    RecordView.a((RecordView)localObject, l1);
    localObject = a;
    long l3 = RecordView.a((RecordView)localObject);
    boolean bool = RecordView.a(l3);
    if (bool)
    {
      localObject = a;
      bool = RecordView.c((RecordView)localObject);
      if (!bool)
      {
        RecordView.d(a);
        localObject = a.getRecordListener();
        if (localObject != null) {
          ((RecordView.a)localObject).b();
        }
        return;
      }
    }
    localObject = a;
    bool = RecordView.c((RecordView)localObject);
    if (!bool)
    {
      RecordView.e(a);
      localObject = a.getRecordListener();
      if (localObject != null)
      {
        String str = RecordView.f(a);
        ((RecordView.a)localObject).a(str);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.voice_notes.RecordView.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */