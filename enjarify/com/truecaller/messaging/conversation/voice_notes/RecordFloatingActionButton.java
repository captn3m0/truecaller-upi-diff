package com.truecaller.messaging.conversation.voice_notes;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.media.MediaRecorder;
import android.media.MediaRecorder.OnInfoListener;
import android.os.Handler;
import android.support.constraint.Guideline;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.R.id;
import com.truecaller.common.e.f;
import com.truecaller.utils.extensions.i;
import com.truecaller.utils.extensions.t;
import java.io.IOException;

public final class RecordFloatingActionButton
  extends FloatingActionButton
  implements View.OnTouchListener
{
  public RecordView c;
  private boolean d;
  
  public RecordFloatingActionButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private RecordFloatingActionButton(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    paramContext = this;
    paramContext = (View.OnTouchListener)this;
    setOnTouchListener(paramContext);
  }
  
  private final void setClip(View paramView)
  {
    for (;;)
    {
      Object localObject = paramView.getParent();
      if (localObject == null) {
        return;
      }
      boolean bool = paramView instanceof ViewGroup;
      if (bool)
      {
        localObject = paramView;
        localObject = (ViewGroup)paramView;
        ((ViewGroup)localObject).setClipChildren(false);
        ((ViewGroup)localObject).setClipToPadding(false);
      }
      localObject = ((View)paramView).getParent();
      bool = localObject instanceof View;
      if (!bool) {
        return;
      }
      paramView = ((View)paramView).getParent();
      if (paramView == null) {
        break;
      }
      paramView = (View)paramView;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type android.view.View");
    throw paramView;
  }
  
  public final AnimatorSet a(float paramFloat)
  {
    AnimatorSet localAnimatorSet = new android/animation/AnimatorSet;
    localAnimatorSet.<init>();
    localAnimatorSet.setDuration(150L);
    Object localObject = new android/view/animation/AccelerateDecelerateInterpolator;
    ((AccelerateDecelerateInterpolator)localObject).<init>();
    localObject = (TimeInterpolator)localObject;
    localAnimatorSet.setInterpolator((TimeInterpolator)localObject);
    localObject = new Animator[2];
    int i = 1;
    float[] arrayOfFloat = new float[i];
    arrayOfFloat[0] = paramFloat;
    Animator localAnimator1 = (Animator)ObjectAnimator.ofFloat(this, "scaleY", arrayOfFloat);
    localObject[0] = localAnimator1;
    arrayOfFloat = new float[i];
    arrayOfFloat[0] = paramFloat;
    Animator localAnimator2 = (Animator)ObjectAnimator.ofFloat(this, "scaleX", arrayOfFloat);
    localObject[i] = localAnimator2;
    localAnimatorSet.playTogether((Animator[])localObject);
    localAnimatorSet.start();
    return localAnimatorSet;
  }
  
  public final RecordView getRecordView()
  {
    RecordView localRecordView = c;
    if (localRecordView == null)
    {
      String str = "recordView";
      k.a(str);
    }
    return localRecordView;
  }
  
  public final boolean getRecordingEnabled()
  {
    return d;
  }
  
  public final void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    Object localObject = this;
    localObject = (View)this;
    setClip((View)localObject);
  }
  
  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    boolean bool1 = d;
    if (bool1)
    {
      int i;
      Object localObject1;
      float f1;
      if (paramMotionEvent != null)
      {
        i = paramMotionEvent.getAction();
        localObject1 = Integer.valueOf(i);
      }
      else
      {
        i = 0;
        f1 = 0.0F;
        localObject1 = null;
      }
      int k = 2;
      float f2 = 2.8E-45F;
      int i5 = 0;
      float f3 = 0.0F;
      String str1 = null;
      int i6 = 1;
      int i7;
      String str2;
      Object localObject2;
      float f4;
      if (localObject1 != null)
      {
        i7 = ((Integer)localObject1).intValue();
        if (i7 == 0)
        {
          paramMotionEvent = c;
          if (paramMotionEvent == null)
          {
            localObject1 = "recordView";
            k.a((String)localObject1);
          }
          if (paramView != null)
          {
            paramView = (RecordFloatingActionButton)paramView;
            k.b(paramView, "recordButton");
            localObject1 = paramMotionEvent.getContext();
            str2 = "context";
            k.a(localObject1, str2);
            localObject1 = g.a(i.i((Context)localObject1));
            m = ((a)localObject1);
            localObject1 = new android/media/MediaRecorder;
            ((MediaRecorder)localObject1).<init>();
            ((MediaRecorder)localObject1).reset();
            ((MediaRecorder)localObject1).setAudioSource(i6);
            ((MediaRecorder)localObject1).setOutputFormat(k);
            f2 = 4.2E-45F;
            ((MediaRecorder)localObject1).setAudioEncoder(3);
            long l1 = b;
            k = (int)l1;
            ((MediaRecorder)localObject1).setMaxDuration(k);
            localObject2 = k;
            ((MediaRecorder)localObject1).setOutputFile((String)localObject2);
            localObject2 = new com/truecaller/messaging/conversation/voice_notes/RecordView$d;
            ((RecordView.d)localObject2).<init>(paramMotionEvent, paramView);
            localObject2 = (MediaRecorder.OnInfoListener)localObject2;
            ((MediaRecorder)localObject1).setOnInfoListener((MediaRecorder.OnInfoListener)localObject2);
            try
            {
              ((MediaRecorder)localObject1).prepare();
              ((MediaRecorder)localObject1).start();
              j = i6;
            }
            catch (IOException localIOException)
            {
              localObject2 = (Exception)localIOException;
              paramMotionEvent.a((Exception)localObject2);
              localObject2 = i;
              if (localObject2 != null) {
                ((MediaRecorder)localObject2).release();
              }
            }
            catch (IllegalStateException localIllegalStateException)
            {
              localObject2 = (Exception)localIllegalStateException;
              paramMotionEvent.a((Exception)localObject2);
              localObject2 = i;
              if (localObject2 != null) {
                ((MediaRecorder)localObject2).release();
              }
            }
            i = ((MediaRecorder)localObject1);
            i = 1073741824;
            f1 = 2.0F;
            paramView.a(f1);
            f4 = paramView.getX();
            c = f4;
            t.a(paramMotionEvent);
            long l2 = System.currentTimeMillis();
            g = l2;
            f = false;
            paramView = a;
            if (paramView == null) {
              break label2101;
            }
            paramView.a();
            break label2101;
          }
          paramView = new c/u;
          paramView.<init>("null cannot be cast to non-null type com.truecaller.messaging.conversation.voice_notes.RecordFloatingActionButton");
          throw paramView;
        }
      }
      label670:
      label863:
      label872:
      int i9;
      if (localObject1 != null)
      {
        i7 = ((Integer)localObject1).intValue();
        if (i7 == k)
        {
          localObject1 = c;
          if (localObject1 == null)
          {
            localObject2 = "recordView";
            k.a((String)localObject2);
          }
          if (paramView != null)
          {
            paramView = (RecordFloatingActionButton)paramView;
            k.b(paramView, "recordButton");
            localObject2 = "motionEvent";
            k.b(paramMotionEvent, (String)localObject2);
            boolean bool3 = f;
            if (bool3) {
              break label2101;
            }
            bool3 = f.b();
            i7 = 0;
            str2 = null;
            Object localObject3;
            int i8;
            String str3;
            float f6;
            if (bool3)
            {
              int m = R.id.tvSlideToCancel;
              localObject2 = (TextView)((RecordView)localObject1).a(m);
              localObject3 = "tvSlideToCancel";
              k.a(localObject2, (String)localObject3);
              f2 = ((TextView)localObject2).getX();
              boolean bool4 = f2 < 0.0F;
              if (bool4)
              {
                int n = R.id.tvSlideToCancel;
                localObject2 = (TextView)((RecordView)localObject1).a(n);
                k.a(localObject2, "tvSlideToCancel");
                f2 = ((TextView)localObject2).getX();
                i8 = R.id.guidelineRecord;
                localObject3 = (Guideline)((RecordView)localObject1).a(i8);
                str3 = "guidelineRecord";
                k.a(localObject3, str3);
                f5 = ((Guideline)localObject3).getX();
                f6 = h;
                f5 -= f6;
                bool5 = f2 < f5;
                if (!bool5) {}
              }
              else
              {
                f2 = paramView.getX();
                i8 = l;
                f5 = i8;
                bool5 = f2 < f5;
                if (bool5) {
                  break label670;
                }
              }
              boolean bool5 = true;
              f2 = Float.MIN_VALUE;
              break label872;
              bool5 = false;
              f2 = 0.0F;
              localObject2 = null;
            }
            else
            {
              int i1 = R.id.tvSlideToCancel;
              localObject2 = (TextView)((RecordView)localObject1).a(i1);
              localObject3 = "tvSlideToCancel";
              k.a(localObject2, (String)localObject3);
              f2 = ((TextView)localObject2).getX();
              boolean bool6 = f2 < 0.0F;
              if (bool6)
              {
                int i2 = R.id.tvSlideToCancel;
                localObject2 = (TextView)((RecordView)localObject1).a(i2);
                k.a(localObject2, "tvSlideToCancel");
                f2 = ((TextView)localObject2).getX();
                i8 = R.id.guidelineRecord;
                localObject3 = (Guideline)((RecordView)localObject1).a(i8);
                str3 = "guidelineRecord";
                k.a(localObject3, str3);
                f5 = ((Guideline)localObject3).getX();
                f6 = h;
                f5 += f6;
                bool7 = f2 < f5;
                if (!bool7) {}
              }
              else
              {
                f2 = paramView.getX();
                i8 = l;
                f5 = i8;
                bool7 = f2 < f5;
                if (bool7) {
                  break label863;
                }
              }
              bool7 = true;
              f2 = Float.MIN_VALUE;
              break label872;
              bool7 = false;
              f2 = 0.0F;
              localObject2 = null;
            }
            if (bool7)
            {
              t.a((View)localObject1, false);
              i9 = R.id.tvSlideToCancel;
              paramMotionEvent = (TextView)((RecordView)localObject1).a(i9);
              localObject2 = "tvSlideToCancel";
              k.a(paramMotionEvent, (String)localObject2);
              f2 = c;
              f3 = d;
              ((RecordView)localObject1).a(paramView, paramMotionEvent, f2, f3);
              f = i6;
              paramView = a;
              if (paramView != null) {
                paramView.b();
              }
              ((RecordView)localObject1).a();
              break label2101;
            }
            boolean bool7 = f.b();
            if (bool7)
            {
              f2 = paramMotionEvent.getRawX();
              f5 = c;
              bool7 = f2 < f5;
              if (bool7)
              {
                bool7 = true;
                f2 = Float.MIN_VALUE;
              }
              else
              {
                bool7 = false;
                f2 = 0.0F;
                localObject2 = null;
              }
            }
            else
            {
              f2 = paramMotionEvent.getRawX();
              f5 = c;
              bool7 = f2 < f5;
              if (bool7)
              {
                bool7 = true;
                f2 = Float.MIN_VALUE;
              }
              else
              {
                bool7 = false;
                f2 = 0.0F;
                localObject2 = null;
              }
            }
            if (!bool7) {
              break label2101;
            }
            localObject2 = paramView.animate();
            float f5 = paramMotionEvent.getRawX();
            localObject2 = ((ViewPropertyAnimator)localObject2).x(f5);
            long l3 = 0L;
            localObject2 = ((ViewPropertyAnimator)localObject2).setDuration(l3);
            ((ViewPropertyAnimator)localObject2).start();
            f2 = e;
            bool7 = f2 < 0.0F;
            if (!bool7)
            {
              f2 = c;
              e = f2;
            }
            bool7 = f.b();
            float f7;
            int i10;
            Object localObject4;
            String str4;
            boolean bool8;
            if (bool7)
            {
              f2 = paramMotionEvent.getRawX();
              f7 = e;
              bool7 = f2 < f7;
              if (bool7)
              {
                f2 = paramMotionEvent.getRawX();
                i10 = R.id.guidelineThreshold;
                localObject4 = (Guideline)((RecordView)localObject1).a(i10);
                str4 = "guidelineThreshold";
                k.a(localObject4, str4);
                f7 = ((Guideline)localObject4).getX();
                bool7 = f2 < f7;
                if (!bool7)
                {
                  int i3 = R.id.guidelineThreshold;
                  localObject2 = (Guideline)((RecordView)localObject1).a(i3);
                  localObject4 = "guidelineThreshold";
                  k.a(localObject2, (String)localObject4);
                  f2 = ((Guideline)localObject2).getX();
                  bool8 = f2 < 0.0F;
                  if (bool8)
                  {
                    bool8 = true;
                    f2 = Float.MIN_VALUE;
                    break label1398;
                  }
                }
              }
              bool8 = false;
              f2 = 0.0F;
              localObject2 = null;
            }
            else
            {
              f2 = paramMotionEvent.getRawX();
              f7 = e;
              bool8 = f2 < f7;
              if (bool8)
              {
                f2 = paramMotionEvent.getRawX();
                i10 = R.id.guidelineThreshold;
                localObject4 = (Guideline)((RecordView)localObject1).a(i10);
                str4 = "guidelineThreshold";
                k.a(localObject4, str4);
                f7 = ((Guideline)localObject4).getX();
                bool8 = f2 < f7;
                if (!bool8)
                {
                  bool8 = true;
                  f2 = Float.MIN_VALUE;
                  break label1398;
                }
              }
              bool8 = false;
              f2 = 0.0F;
              localObject2 = null;
            }
            label1398:
            if (bool8)
            {
              localObject2 = RecordView.RecordState.DELETE;
              ((RecordView)localObject1).a(paramView, (RecordView.RecordState)localObject2);
              paramView = a;
              if (paramView != null) {
                paramView.d();
              }
            }
            else
            {
              bool8 = f.b();
              if (bool8)
              {
                f2 = paramMotionEvent.getRawX();
                f7 = e;
                bool8 = f2 < f7;
                if (bool8)
                {
                  f2 = paramMotionEvent.getRawX();
                  i10 = R.id.guidelineThreshold;
                  localObject4 = (Guideline)((RecordView)localObject1).a(i10);
                  str4 = "guidelineThreshold";
                  k.a(localObject4, str4);
                  f7 = ((Guideline)localObject4).getX();
                  bool8 = f2 < f7;
                  if (bool8)
                  {
                    i5 = 1;
                    f3 = Float.MIN_VALUE;
                  }
                }
              }
              else
              {
                f2 = paramMotionEvent.getRawX();
                f7 = e;
                bool8 = f2 < f7;
                if (bool8)
                {
                  f2 = paramMotionEvent.getRawX();
                  i10 = R.id.guidelineThreshold;
                  localObject4 = (Guideline)((RecordView)localObject1).a(i10);
                  str4 = "guidelineThreshold";
                  k.a(localObject4, str4);
                  f7 = ((Guideline)localObject4).getX();
                  bool8 = f2 < f7;
                  if (bool8)
                  {
                    i5 = 1;
                    f3 = Float.MIN_VALUE;
                  }
                }
              }
              if (i5 != 0)
              {
                localObject2 = RecordView.RecordState.RECORD;
                ((RecordView)localObject1).a(paramView, (RecordView.RecordState)localObject2);
                paramView = a;
                if (paramView != null) {
                  paramView.e();
                }
              }
            }
            f4 = paramMotionEvent.getRawX();
            e = f4;
            f4 = d;
            boolean bool9 = f4 < 0.0F;
            if (!bool9)
            {
              int i11 = R.id.tvSlideToCancel;
              paramView = (TextView)((RecordView)localObject1).a(i11);
              localObject2 = "tvSlideToCancel";
              k.a(paramView, (String)localObject2);
              f4 = paramView.getX();
              boolean bool10 = f4 < 0.0F;
              if (bool10)
              {
                bool10 = f.b();
                int i4;
                if (bool10)
                {
                  f4 = c;
                  i4 = R.id.tvSlideToCancel;
                  localObject2 = (TextView)((RecordView)localObject1).a(i4);
                  str1 = "tvSlideToCancel";
                  k.a(localObject2, str1);
                  i4 = ((TextView)localObject2).getLeft();
                  f2 = i4;
                  f4 -= f2;
                }
                else
                {
                  f4 = c;
                  i4 = R.id.tvSlideToCancel;
                  localObject2 = (TextView)((RecordView)localObject1).a(i4);
                  str1 = "tvSlideToCancel";
                  k.a(localObject2, str1);
                  f2 = ((TextView)localObject2).getX();
                  f4 -= f2;
                }
                d = f4;
              }
            }
            int i12 = R.id.tvSlideToCancel;
            paramView = ((TextView)((RecordView)localObject1).a(i12)).animate();
            float f8 = paramMotionEvent.getRawX();
            f1 = d;
            f8 -= f1;
            paramView = paramView.x(f8).setDuration(l3);
            paramView.start();
            break label2101;
          }
          paramView = new c/u;
          paramView.<init>("null cannot be cast to non-null type com.truecaller.messaging.conversation.voice_notes.RecordFloatingActionButton");
          throw paramView;
        }
      }
      if (localObject1 != null)
      {
        i9 = ((Integer)localObject1).intValue();
        if (i9 == i6)
        {
          paramMotionEvent = c;
          if (paramMotionEvent == null)
          {
            localObject1 = "recordView";
            k.a((String)localObject1);
          }
          if (paramView != null)
          {
            paramView = (RecordFloatingActionButton)paramView;
            localObject1 = "recordButton";
            k.b(paramView, (String)localObject1);
            boolean bool2 = j;
            if (bool2)
            {
              t.a(paramMotionEvent, false);
              int j = R.id.tvSlideToCancel;
              localObject1 = (TextView)paramMotionEvent.a(j);
              localObject2 = "tvSlideToCancel";
              k.a(localObject1, (String)localObject2);
              f2 = c;
              f3 = d;
              paramMotionEvent.a(paramView, (TextView)localObject1, f2, f3);
              paramView = new android/os/Handler;
              paramView.<init>();
              localObject1 = new com/truecaller/messaging/conversation/voice_notes/RecordView$c;
              ((RecordView.c)localObject1).<init>(paramMotionEvent);
              localObject1 = (Runnable)localObject1;
              long l4 = 300L;
              paramView.postDelayed((Runnable)localObject1, l4);
            }
          }
          else
          {
            paramView = new c/u;
            paramView.<init>("null cannot be cast to non-null type com.truecaller.messaging.conversation.voice_notes.RecordFloatingActionButton");
            throw paramView;
          }
        }
      }
    }
    label2101:
    return d;
  }
  
  public final void onWindowFocusChanged(boolean paramBoolean)
  {
    super.onWindowFocusChanged(paramBoolean);
    if (!paramBoolean)
    {
      RecordView localRecordView = c;
      if (localRecordView == null)
      {
        localObject = "recordView";
        k.a((String)localObject);
      }
      Object localObject = "recordBtn";
      k.b(this, (String)localObject);
      boolean bool = j;
      if (bool)
      {
        localObject = a;
        if (localObject != null) {
          ((RecordView.a)localObject).b();
        }
        localObject = null;
        t.a(localRecordView, false);
        int i = 1065353216;
        float f = 1.0F;
        a(f);
        localRecordView.a();
      }
    }
  }
  
  public final void setRecordView(RecordView paramRecordView)
  {
    k.b(paramRecordView, "<set-?>");
    c = paramRecordView;
  }
  
  public final void setRecordingEnabled(boolean paramBoolean)
  {
    d = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.voice_notes.RecordFloatingActionButton
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */