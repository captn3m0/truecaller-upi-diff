package com.truecaller.messaging.conversation.voice_notes;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import com.truecaller.utils.extensions.i;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class d
  implements f
{
  public e a;
  private final Context b;
  private MediaPlayer c;
  private Uri d;
  private ScheduledExecutorService e;
  private Runnable f;
  private a g;
  
  public d(Context paramContext)
  {
    paramContext = paramContext.getApplicationContext();
    b = paramContext;
  }
  
  private void f()
  {
    Object localObject = e;
    if (localObject != null)
    {
      ((ScheduledExecutorService)localObject).shutdownNow();
      e = null;
      f = null;
      localObject = a;
      if (localObject != null) {
        ((e)localObject).b(0);
      }
    }
  }
  
  private void g()
  {
    MediaPlayer localMediaPlayer = c;
    if (localMediaPlayer != null)
    {
      boolean bool = localMediaPlayer.isPlaying();
      if (bool)
      {
        localMediaPlayer = c;
        int i = localMediaPlayer.getCurrentPosition();
        e locale = a;
        if (locale != null) {
          locale.b(i);
        }
      }
    }
  }
  
  public final void a()
  {
    MediaPlayer localMediaPlayer = c;
    if (localMediaPlayer != null)
    {
      String str = "release() and mMediaPlayer = null";
      new String[1][0] = str;
      localMediaPlayer.release();
      localMediaPlayer = null;
      c = null;
    }
  }
  
  public final void a(Uri paramUri)
  {
    d = paramUri;
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new android/media/MediaPlayer;
      ((MediaPlayer)localObject1).<init>();
      c = ((MediaPlayer)localObject1);
      localObject1 = c;
      localp934R87VjQBMtc6LdtBOC3Lbsl8 = new com/truecaller/messaging/conversation/voice_notes/-$$Lambda$d$p934R87VjQBMtc6LdtBOC3Lbsl8;
      localp934R87VjQBMtc6LdtBOC3Lbsl8.<init>(this);
      ((MediaPlayer)localObject1).setOnCompletionListener(localp934R87VjQBMtc6LdtBOC3Lbsl8);
      localObject1 = "mMediaPlayer = new MediaPlayer()";
      new String[1][0] = localObject1;
    }
    int i = 1;
    -..Lambda.d.p934R87VjQBMtc6LdtBOC3Lbsl8 localp934R87VjQBMtc6LdtBOC3Lbsl8 = null;
    Object localObject2 = "load() {1. setDataSource}";
    Object localObject3;
    try
    {
      new String[1][0] = localObject2;
      localObject2 = c;
      localObject3 = b;
      ((MediaPlayer)localObject2).setDataSource((Context)localObject3, paramUri);
    }
    catch (Exception paramUri)
    {
      localObject2 = new String[i];
      paramUri = paramUri.toString();
      localObject2[0] = paramUri;
    }
    paramUri = "load() {2. prepare}";
    try
    {
      new String[1][0] = paramUri;
      paramUri = c;
      paramUri.prepare();
    }
    catch (Exception paramUri)
    {
      localObject2 = new String[i];
      paramUri = paramUri.toString();
      localObject2[0] = paramUri;
    }
    paramUri = c;
    int j = paramUri.getDuration();
    localObject2 = a;
    if (localObject2 != null)
    {
      ((e)localObject2).a(j);
      a.b(0);
      localObject2 = new String[i];
      localObject3 = Locale.getDefault();
      String str = "firing setPlaybackDuration(%d sec)";
      localObject1 = new Object[i];
      TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
      long l1 = j;
      long l2 = localTimeUnit.toSeconds(l1);
      paramUri = Long.valueOf(l2);
      localObject1[0] = paramUri;
      paramUri = String.format((Locale)localObject3, str, (Object[])localObject1);
      localObject2[0] = paramUri;
      paramUri = "firing setPlaybackPosition(0)";
      new String[1][0] = paramUri;
    }
    new String[1][0] = "initializeProgressCallback()";
  }
  
  public final boolean b()
  {
    MediaPlayer localMediaPlayer = c;
    if (localMediaPlayer != null) {
      return localMediaPlayer.isPlaying();
    }
    return false;
  }
  
  public final void c()
  {
    Object localObject = c;
    if (localObject != null)
    {
      boolean bool = ((MediaPlayer)localObject).isPlaying();
      if (!bool)
      {
        localObject = g.a(i.i(b));
        g = ((a)localObject);
        c.start();
        localObject = a;
        if (localObject != null)
        {
          localScheduledExecutorService = null;
          ((e)localObject).c(0);
        }
        localObject = e;
        if (localObject == null)
        {
          localObject = Executors.newSingleThreadScheduledExecutor();
          e = ((ScheduledExecutorService)localObject);
        }
        localObject = f;
        if (localObject == null)
        {
          localObject = new com/truecaller/messaging/conversation/voice_notes/-$$Lambda$d$bq4YqeBf_rtLM8e7DlOzdQe6HSI;
          ((-..Lambda.d.bq4YqeBf_rtLM8e7DlOzdQe6HSI)localObject).<init>(this);
          f = ((Runnable)localObject);
        }
        ScheduledExecutorService localScheduledExecutorService = e;
        Runnable localRunnable = f;
        long l1 = 0L;
        long l2 = 50;
        TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
        localScheduledExecutorService.scheduleAtFixedRate(localRunnable, l1, l2, localTimeUnit);
      }
    }
  }
  
  public final void d()
  {
    Object localObject = c;
    if (localObject != null)
    {
      String str = "playbackReset()";
      new String[1][0] = str;
      ((MediaPlayer)localObject).reset();
      localObject = d;
      a((Uri)localObject);
      localObject = a;
      if (localObject != null)
      {
        int i = 2;
        ((e)localObject).c(i);
      }
      f();
    }
  }
  
  public final void e()
  {
    Object localObject = c;
    if (localObject != null)
    {
      boolean bool = ((MediaPlayer)localObject).isPlaying();
      if (bool)
      {
        localObject = g;
        AudioManager localAudioManager = i.i(b);
        ((a)localObject).a(localAudioManager);
        c.pause();
        localObject = a;
        if (localObject != null)
        {
          int i = 1;
          ((e)localObject).c(i);
        }
        localObject = "playbackPause()";
        new String[1][0] = localObject;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.voice_notes.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */