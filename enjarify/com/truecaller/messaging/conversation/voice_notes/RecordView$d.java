package com.truecaller.messaging.conversation.voice_notes;

import android.media.MediaRecorder;
import android.media.MediaRecorder.OnInfoListener;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.utils.extensions.t;

final class RecordView$d
  implements MediaRecorder.OnInfoListener
{
  RecordView$d(RecordView paramRecordView, RecordFloatingActionButton paramRecordFloatingActionButton) {}
  
  public final void onInfo(MediaRecorder paramMediaRecorder, int paramInt1, int paramInt2)
  {
    int i = 800;
    if (paramInt1 == i)
    {
      RecordView.e(a);
      paramMediaRecorder = a.getRecordListener();
      if (paramMediaRecorder != null)
      {
        localObject = RecordView.f(a);
        paramMediaRecorder.a((String)localObject);
      }
      paramMediaRecorder = a;
      paramInt1 = 0;
      t.a(paramMediaRecorder, false);
      paramMediaRecorder = a;
      Object localObject = b;
      paramInt2 = R.id.tvSlideToCancel;
      TextView localTextView = (TextView)paramMediaRecorder.a(paramInt2);
      k.a(localTextView, "tvSlideToCancel");
      RecordView localRecordView1 = a;
      float f1 = RecordView.g(localRecordView1);
      RecordView localRecordView2 = a;
      float f2 = RecordView.h(localRecordView2);
      RecordView.a(paramMediaRecorder, (RecordFloatingActionButton)localObject, localTextView, f1, f2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.voice_notes.RecordView.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */