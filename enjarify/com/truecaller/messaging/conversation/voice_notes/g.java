package com.truecaller.messaging.conversation.voice_notes;

import android.media.AudioAttributes;
import android.media.AudioAttributes.Builder;
import android.media.AudioFocusRequest;
import android.media.AudioFocusRequest.Builder;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.os.Build.VERSION;
import c.g.b.k;

public final class g
{
  public static final a a(AudioManager paramAudioManager)
  {
    Object localObject = "receiver$0";
    k.b(paramAudioManager, (String)localObject);
    int i = Build.VERSION.SDK_INT;
    int j = 4;
    int k = 26;
    if (i >= k)
    {
      localObject = new android/media/AudioAttributes$Builder;
      ((AudioAttributes.Builder)localObject).<init>();
      localObject = ((AudioAttributes.Builder)localObject).setContentType(1).setUsage(2).build();
      AudioFocusRequest.Builder localBuilder = new android/media/AudioFocusRequest$Builder;
      localBuilder.<init>(j);
      AudioManager.OnAudioFocusChangeListener localOnAudioFocusChangeListener = (AudioManager.OnAudioFocusChangeListener)g.c.a;
      localObject = localBuilder.setOnAudioFocusChangeListener(localOnAudioFocusChangeListener).setAudioAttributes((AudioAttributes)localObject).build();
      paramAudioManager.requestAudioFocus((AudioFocusRequest)localObject);
      paramAudioManager = new com/truecaller/messaging/conversation/voice_notes/c;
      k.a(localObject, "audioFocusRequest");
      paramAudioManager.<init>((AudioFocusRequest)localObject);
      return (a)paramAudioManager;
    }
    localObject = (AudioManager.OnAudioFocusChangeListener)g.a.a;
    paramAudioManager.requestAudioFocus((AudioManager.OnAudioFocusChangeListener)localObject, 0, j);
    paramAudioManager = new com/truecaller/messaging/conversation/voice_notes/b;
    localObject = (AudioManager.OnAudioFocusChangeListener)g.b.a;
    paramAudioManager.<init>((AudioManager.OnAudioFocusChangeListener)localObject);
    return (a)paramAudioManager;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.voice_notes.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */