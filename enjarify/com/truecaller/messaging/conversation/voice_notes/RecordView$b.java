package com.truecaller.messaging.conversation.voice_notes;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import c.g.b.k;
import c.u;

final class RecordView$b
  implements ValueAnimator.AnimatorUpdateListener
{
  RecordView$b(RecordFloatingActionButton paramRecordFloatingActionButton) {}
  
  public final void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    String str = "animation";
    k.a(paramValueAnimator, str);
    paramValueAnimator = paramValueAnimator.getAnimatedValue();
    if (paramValueAnimator != null)
    {
      float f = ((Float)paramValueAnimator).floatValue();
      a.setX(f);
      return;
    }
    paramValueAnimator = new c/u;
    paramValueAnimator.<init>("null cannot be cast to non-null type kotlin.Float");
    throw paramValueAnimator;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.voice_notes.RecordView.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */