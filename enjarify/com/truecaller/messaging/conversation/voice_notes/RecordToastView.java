package com.truecaller.messaging.conversation.voice_notes;

import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.utils.ui.b;
import java.util.HashMap;

public final class RecordToastView
  extends RelativeLayout
{
  private final TimeInterpolator a;
  private AlphaAnimation b;
  private final int c;
  private final int d;
  private HashMap e;
  
  public RecordToastView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private RecordToastView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    paramAttributeSet = new android/view/animation/LinearInterpolator;
    paramAttributeSet.<init>();
    paramAttributeSet = (TimeInterpolator)paramAttributeSet;
    a = paramAttributeSet;
    int i = paramContext.getResources().getDimensionPixelSize(2131166182);
    c = i;
    i = paramContext.getResources().getDimensionPixelSize(2131165704);
    d = i;
    paramAttributeSet = this;
    paramAttributeSet = (ViewGroup)this;
    RelativeLayout.inflate(paramContext, 2131559218, paramAttributeSet);
    setBackgroundResource(2131234877);
    paramContext = getBackground();
    i = b.a(getContext(), 2130968853);
    PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
    paramContext.setColorFilter(i, localMode);
    int j = d;
    i = c;
    setPaddingRelative(j, i, j, i);
    setAlpha(0.7F);
  }
  
  public final View a(int paramInt)
  {
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      e = ((HashMap)localObject1);
    }
    localObject1 = e;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = e;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void a()
  {
    Object localObject = this;
    localObject = b;
    if (localObject != null)
    {
      localObject = b;
      String str;
      if (localObject == null)
      {
        str = "recordAlphaAnimation";
        k.a(str);
      }
      ((AlphaAnimation)localObject).cancel();
      localObject = b;
      if (localObject == null)
      {
        str = "recordAlphaAnimation";
        k.a(str);
      }
      ((AlphaAnimation)localObject).reset();
    }
    int i = R.id.recordDot;
    ((ImageView)a(i)).clearAnimation();
  }
  
  public final void b()
  {
    Object localObject1 = new android/view/animation/AlphaAnimation;
    int i = 0;
    Object localObject2 = null;
    float f = 1.0F;
    ((AlphaAnimation)localObject1).<init>(0.0F, f);
    b = ((AlphaAnimation)localObject1);
    localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "recordAlphaAnimation";
      k.a((String)localObject2);
    }
    long l = 500L;
    ((AlphaAnimation)localObject1).setDuration(l);
    localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "recordAlphaAnimation";
      k.a((String)localObject2);
    }
    i = 2;
    ((AlphaAnimation)localObject1).setRepeatMode(i);
    localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "recordAlphaAnimation";
      k.a((String)localObject2);
    }
    i = -1;
    ((AlphaAnimation)localObject1).setRepeatCount(i);
    int j = R.id.recordDot;
    localObject1 = (ImageView)a(j);
    localObject2 = b;
    if (localObject2 == null)
    {
      String str = "recordAlphaAnimation";
      k.a(str);
    }
    localObject2 = (Animation)localObject2;
    ((ImageView)localObject1).startAnimation((Animation)localObject2);
  }
  
  public final void setVisible(boolean paramBoolean)
  {
    long l = 150L;
    if (paramBoolean)
    {
      setVisibility(0);
      localViewPropertyAnimator = animate().translationY(0.0F).setDuration(l);
      localObject = a;
      localViewPropertyAnimator.setInterpolator((TimeInterpolator)localObject).start();
      return;
    }
    ViewPropertyAnimator localViewPropertyAnimator = animate();
    float f = getHeight();
    localViewPropertyAnimator = localViewPropertyAnimator.translationY(f).setDuration(l);
    Object localObject = a;
    localViewPropertyAnimator = localViewPropertyAnimator.setInterpolator((TimeInterpolator)localObject);
    localObject = new com/truecaller/messaging/conversation/voice_notes/RecordToastView$a;
    ((RecordToastView.a)localObject).<init>(this);
    localObject = (Runnable)localObject;
    localViewPropertyAnimator.withEndAction((Runnable)localObject).start();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.voice_notes.RecordToastView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */