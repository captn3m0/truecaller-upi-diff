package com.truecaller.messaging.conversation;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipData.Item;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.helper.Flow;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.OnItemTouchListener;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.support.v7.widget.RecyclerView.RecyclerListener;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Chronometer;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker.IntentBuilder;
import com.truecaller.R.id;
import com.truecaller.TrueApp;
import com.truecaller.android.truemoji.EmojiRootLayout;
import com.truecaller.android.truemoji.h;
import com.truecaller.android.truemoji.h.a;
import com.truecaller.android.truemoji.h.b;
import com.truecaller.android.truemoji.h.c;
import com.truecaller.android.truemoji.h.d;
import com.truecaller.android.truemoji.h.e;
import com.truecaller.android.truemoji.h.f;
import com.truecaller.android.truemoji.p;
import com.truecaller.bi;
import com.truecaller.bj;
import com.truecaller.calling.initiate_call.b.a;
import com.truecaller.calling.initiate_call.b.a.a;
import com.truecaller.common.h.ai;
import com.truecaller.data.entity.Contact;
import com.truecaller.data.entity.Number;
import com.truecaller.flashsdk.models.FlashContact;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.conversation.emoji.EmojiPokeView;
import com.truecaller.messaging.conversation.voice_notes.RecordFloatingActionButton;
import com.truecaller.messaging.conversation.voice_notes.RecordToastView;
import com.truecaller.messaging.conversation.voice_notes.RecordView;
import com.truecaller.messaging.conversation.voice_notes.RecordView.a;
import com.truecaller.messaging.conversationinfo.ConversationInfoActivity;
import com.truecaller.messaging.data.types.BinaryEntity;
import com.truecaller.messaging.data.types.Entity;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.Message;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.ReplySnippet;
import com.truecaller.messaging.defaultsms.DefaultSmsActivity;
import com.truecaller.messaging.imgroupinfo.ImGroupInfoActivity.a;
import com.truecaller.messaging.imgroupinvitation.ImGroupInvitationActivity.a;
import com.truecaller.messaging.newconversation.NewConversationActivity;
import com.truecaller.messaging.views.MediaEditText;
import com.truecaller.messaging.views.MediaEditText.a;
import com.truecaller.search.local.model.c.a;
import com.truecaller.truepay.app.ui.transaction.views.activities.TransactionActivity;
import com.truecaller.ui.RequiredPermissionsActivity;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.ui.components.FloatingActionButton.a;
import com.truecaller.ui.components.a.c;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.ui.dialogs.f.a;
import com.truecaller.ui.view.AvailabilityView;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.ui.view.TintedTextView;
import com.truecaller.util.al;
import com.truecaller.util.at;
import com.truecaller.util.br.a;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public final class k
  extends Fragment
  implements ActionMode.Callback, be, bp, r, MediaEditText.a, FloatingActionButton.a
{
  private RecyclerView A;
  private View B;
  private RecyclerView C;
  private View D;
  private EmojiPokeButton E;
  private ViewGroup F;
  private ImageView G;
  private TextView H;
  private View I;
  private Button J;
  private android.support.design.widget.FloatingActionButton K;
  private ViewStub L;
  private View M;
  private TextView N;
  private View O;
  private ImageView P;
  private EmojiPokeView Q;
  private MessageSnippetView R;
  private ConstraintLayout S;
  private TintedImageView T;
  private TintedImageView U;
  private TintedImageView V;
  private Flow W;
  private Flow X;
  private Flow Y;
  private TintedTextView Z;
  bb a;
  private TintedTextView aa;
  private TintedTextView ab;
  private View ac;
  private RecordToastView ad;
  private RecordView ae;
  private e af;
  private com.truecaller.ui.components.FloatingActionButton ag;
  private ActionMode ah;
  private AlertDialog ai = null;
  private bl aj;
  private AlertDialog ak;
  private PopupWindow al;
  private Dialog am;
  private final TextWatcher an;
  private final PhoneStateListener ao;
  m b;
  com.truecaller.messaging.d.a c;
  bn d;
  com.avito.konveyor.a.a e;
  com.truecaller.messaging.conversation.b.b f;
  com.avito.konveyor.a g;
  cj h;
  com.truecaller.util.c.b i;
  com.truecaller.tcpermissions.l j;
  com.truecaller.calling.initiate_call.b k;
  com.truecaller.flashsdk.core.b l;
  cg m;
  com.truecaller.featuretoggles.e n;
  private com.avito.konveyor.a.e p;
  private ContactPhoto q;
  private TextView r;
  private AvailabilityView s;
  private MediaEditText t;
  private RecordFloatingActionButton u;
  private TextView v;
  private h w;
  private EmojiRootLayout x;
  private TintedImageView y;
  private ImageView z;
  
  public k()
  {
    Object localObject = new com/truecaller/messaging/conversation/k$1;
    ((k.1)localObject).<init>(this);
    an = ((TextWatcher)localObject);
    localObject = new com/truecaller/messaging/conversation/k$7;
    ((k.7)localObject).<init>(this);
    ao = ((PhoneStateListener)localObject);
  }
  
  private ObjectAnimator a(float paramFloat1, float paramFloat2)
  {
    View localView = B;
    float[] arrayOfFloat = new float[2];
    arrayOfFloat[0] = paramFloat1;
    arrayOfFloat[1] = paramFloat2;
    ObjectAnimator localObjectAnimator = ObjectAnimator.ofFloat(localView, "alpha", arrayOfFloat);
    localObjectAnimator.setDuration(200L);
    return localObjectAnimator;
  }
  
  private String ab()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = ((Activity)localObject).getIntent();
      String str = "launch_source";
      localObject = ((Intent)localObject).getStringExtra(str);
      if (localObject != null) {
        return (String)localObject;
      }
    }
    return "conversation";
  }
  
  private void s(boolean paramBoolean)
  {
    Object localObject = n.e();
    boolean bool = ((com.truecaller.featuretoggles.b)localObject).a();
    int i2 = 0;
    int i1;
    if ((bool) && (!paramBoolean))
    {
      bool = false;
      localObject = null;
    }
    else
    {
      i1 = 8;
    }
    ConstraintLayout localConstraintLayout = S;
    localConstraintLayout.setVisibility(i1);
    localObject = R;
    if (!paramBoolean) {
      i2 = 8;
    }
    ((MessageSnippetView)localObject).setVisibility(i2);
  }
  
  public final void A()
  {
    aj.notifyDataSetChanged();
    a.r();
  }
  
  public final void B()
  {
    s(true);
    R.a();
    MessageSnippetView localMessageSnippetView = R;
    -..Lambda.k.ArkTqh2a-9bQ6Nct_jiQ5wAv77E localArkTqh2a-9bQ6Nct_jiQ5wAv77E = new com/truecaller/messaging/conversation/-$$Lambda$k$ArkTqh2a-9bQ6Nct_jiQ5wAv77E;
    localArkTqh2a-9bQ6Nct_jiQ5wAv77E.<init>(this);
    localMessageSnippetView.setDismissActionListener(localArkTqh2a-9bQ6Nct_jiQ5wAv77E);
  }
  
  public final void C()
  {
    s(false);
    b.B();
  }
  
  public final void D()
  {
    PopupWindow localPopupWindow = al;
    if (localPopupWindow != null)
    {
      boolean bool = localPopupWindow.isShowing();
      if (bool)
      {
        localPopupWindow = al;
        localPopupWindow.dismiss();
      }
    }
  }
  
  public final void E()
  {
    com.truecaller.bl localbl = new com/truecaller/bl;
    Context localContext = getContext();
    localbl.<init>(localContext, 2131886806, 2131886805, 2131234158);
    localbl.show();
  }
  
  public final void F()
  {
    com.truecaller.wizard.utils.i.a(this, "android.permission.READ_EXTERNAL_STORAGE", 3, true);
  }
  
  public final void G()
  {
    String[] tmp4_1 = new String[3];
    String[] tmp5_4 = tmp4_1;
    String[] tmp5_4 = tmp4_1;
    tmp5_4[0] = "android.permission.RECORD_AUDIO";
    tmp5_4[1] = "android.permission.READ_EXTERNAL_STORAGE";
    tmp5_4[2] = "android.permission.WRITE_EXTERNAL_STORAGE";
    String[] arrayOfString = tmp5_4;
    com.truecaller.wizard.utils.i.b(this, arrayOfString, 206);
  }
  
  public final void H()
  {
    com.truecaller.bl localbl = new com/truecaller/bl;
    Context localContext = getContext();
    localbl.<init>(localContext, 2131886796, 2131886795, 2131233942);
    localbl.show();
  }
  
  public final void I()
  {
    com.truecaller.wizard.utils.i.a(this, "android.permission.CAMERA", 4, true);
  }
  
  public final void J()
  {
    com.truecaller.bl localbl = new com/truecaller/bl;
    Context localContext = requireContext();
    localbl.<init>(localContext, 2131886803, 2131886802, 2131234539);
    localbl.show();
  }
  
  public final void K()
  {
    Object localObject = af;
    if (localObject != null)
    {
      PopupWindow localPopupWindow = f;
      if (localPopupWindow != null)
      {
        localPopupWindow = f;
        boolean bool = localPopupWindow.isShowing();
        if (bool)
        {
          localObject = f;
          ((PopupWindow)localObject).dismiss();
        }
      }
    }
  }
  
  public final void L()
  {
    android.support.v4.app.o localo = getFragmentManager().a();
    cl localcl = new com/truecaller/messaging/conversation/cl;
    localcl.<init>();
    localo.a(localcl, null).f();
  }
  
  public final void M()
  {
    ag.c();
  }
  
  public final boolean N()
  {
    return ag.a;
  }
  
  public final void O()
  {
    Object localObject1 = getContext();
    if (localObject1 == null) {
      return;
    }
    View localView = View.inflate((Context)localObject1, 2131558830, null);
    Object localObject2 = (Button)localView.findViewById(2131362240);
    Button localButton = (Button)localView.findViewById(2131362234);
    Object localObject3 = (TextView)localView.findViewById(2131363752);
    TextView localTextView = (TextView)localView.findViewById(2131364884);
    ImageView localImageView = (ImageView)localView.findViewById(2131363301);
    localTextView.setText(2131886460);
    int i1 = com.truecaller.utils.ui.b.a(localTextView.getContext(), 2130969591);
    localTextView.setTextColor(i1);
    ((TextView)localObject3).setText(2131886458);
    localImageView.setImageResource(2131234347);
    ((Button)localObject2).setText(2131886457);
    localObject3 = new com/truecaller/messaging/conversation/-$$Lambda$k$gbz8v1H7kB77yPdXizuABzQD-r0;
    ((-..Lambda.k.gbz8v1H7kB77yPdXizuABzQD-r0)localObject3).<init>(this);
    ((Button)localObject2).setOnClickListener((View.OnClickListener)localObject3);
    localButton.setText(2131886566);
    localObject2 = new com/truecaller/messaging/conversation/-$$Lambda$k$BWDBLwdT4xHRDcOkbQueqBsp7_U;
    ((-..Lambda.k.BWDBLwdT4xHRDcOkbQueqBsp7_U)localObject2).<init>(this);
    localButton.setOnClickListener((View.OnClickListener)localObject2);
    localObject2 = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject2).<init>((Context)localObject1);
    localObject1 = new com/truecaller/messaging/conversation/-$$Lambda$k$X1iGBlHQYFButxOx6viRb-IZ_p4;
    ((-..Lambda.k.X1iGBlHQYFButxOx6viRb-IZ_p4)localObject1).<init>(this);
    localObject1 = ((AlertDialog.Builder)localObject2).setOnCancelListener((DialogInterface.OnCancelListener)localObject1).setView(localView).create();
    ak = ((AlertDialog)localObject1);
    ak.show();
  }
  
  public final void P()
  {
    Object localObject = getContext();
    if (localObject == null) {
      return;
    }
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    localBuilder.<init>((Context)localObject);
    localObject = localBuilder.setMessage(2131886511);
    -..Lambda.k.3St4WlJsp3Bbf6tEXqgR2hQFS-c local3St4WlJsp3Bbf6tEXqgR2hQFS-c = new com/truecaller/messaging/conversation/-$$Lambda$k$3St4WlJsp3Bbf6tEXqgR2hQFS-c;
    local3St4WlJsp3Bbf6tEXqgR2hQFS-c.<init>(this);
    ((AlertDialog.Builder)localObject).setPositiveButton(2131887217, local3St4WlJsp3Bbf6tEXqgR2hQFS-c).setNegativeButton(2131887197, null).create().show();
  }
  
  public final void Q()
  {
    ViewGroup localViewGroup = F;
    int i1 = 4;
    localViewGroup.setVisibility(i1);
    G.setVisibility(i1);
    C.setVisibility(8);
  }
  
  public final void R()
  {
    p.notifyDataSetChanged();
  }
  
  public final void S()
  {
    I.setVisibility(0);
    RelativeLayout.LayoutParams localLayoutParams = (RelativeLayout.LayoutParams)A.getLayoutParams();
    int i1 = I.getId();
    localLayoutParams.addRule(3, i1);
  }
  
  public final void T()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      localf = getActivity();
      com.truecaller.common.h.o.b(localf);
    }
  }
  
  public final void U()
  {
    Object localObject = getContext();
    if (localObject == null) {
      return;
    }
    localObject = new android/support/v7/app/AlertDialog$Builder;
    Context localContext = getContext();
    ((AlertDialog.Builder)localObject).<init>(localContext);
    localObject = ((AlertDialog.Builder)localObject).setMessage(2131886429).setCancelable(true);
    -..Lambda.k.vEo3dyDytCjAlHF8ZUdetTbTfYY localvEo3dyDytCjAlHF8ZUdetTbTfYY = new com/truecaller/messaging/conversation/-$$Lambda$k$vEo3dyDytCjAlHF8ZUdetTbTfYY;
    localvEo3dyDytCjAlHF8ZUdetTbTfYY.<init>(this);
    ((AlertDialog.Builder)localObject).setPositiveButton(2131888360, localvEo3dyDytCjAlHF8ZUdetTbTfYY).show();
  }
  
  public final void V()
  {
    Object localObject = getContext();
    if (localObject == null) {
      return;
    }
    localObject = new android/support/v7/app/AlertDialog$Builder;
    Context localContext = getContext();
    ((AlertDialog.Builder)localObject).<init>(localContext);
    ((AlertDialog.Builder)localObject).setMessage(2131886428).setCancelable(true).setPositiveButton(2131887217, null).show();
  }
  
  public final void W()
  {
    A.scrollToPosition(0);
    K.b(null, true);
  }
  
  public final void X()
  {
    View localView = M;
    if (localView != null)
    {
      int i1 = localView.getVisibility();
      int i2 = 8;
      if (i1 != i2)
      {
        localView = M;
        localView.setVisibility(i2);
      }
    }
  }
  
  public final String[] Y()
  {
    return d.k();
  }
  
  public final void Z()
  {
    Object localObject = com.truecaller.ui.dialogs.f.a;
    localObject = f.a.a(getContext());
    am = ((Dialog)localObject);
  }
  
  public final void a() {}
  
  public final void a(int paramInt)
  {
    b.a(paramInt);
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    Object localObject = getContext();
    if (localObject == null) {
      return;
    }
    Resources localResources = ((Context)localObject).getResources();
    localObject = ((Context)localObject).getTheme();
    Drawable localDrawable = android.support.v4.graphics.drawable.a.e((Drawable)Objects.requireNonNull(android.support.v4.content.b.f.a(localResources, paramInt1, (Resources.Theme)localObject))).mutate();
    android.support.v4.graphics.drawable.a.a(localDrawable, paramInt2);
    u.setImageDrawable(localDrawable);
  }
  
  public final void a(int paramInt, Message paramMessage, List paramList, String paramString, boolean paramBoolean)
  {
    Object localObject1 = A;
    Object localObject2 = ((RecyclerView)localObject1).findViewHolderForLayoutPosition(paramInt);
    if (localObject2 != null)
    {
      localObject2 = itemView;
      if (paramBoolean)
      {
        bool1 = 2131888604;
        localObject1 = getString(bool1);
      }
      else
      {
        bool1 = false;
        localObject1 = null;
      }
      Object localObject3 = new com/truecaller/ui/view/i;
      Context localContext = getContext();
      ((com.truecaller.ui.view.i)localObject3).<init>(localContext, paramList, paramString, (String)localObject1);
      paramList = new com/truecaller/messaging/conversation/-$$Lambda$k$6lhZP_PA_tbXaKOXhUUTujlTJ6o;
      paramList.<init>(this, paramMessage, paramBoolean);
      ((com.truecaller.ui.view.i)localObject3).setOnReactionPickListener(paramList);
      paramList = null;
      ((com.truecaller.ui.view.i)localObject3).measure(0, 0);
      boolean bool3 = f;
      boolean bool5 = true;
      bool3 &= bool5;
      if (bool3)
      {
        bool3 = true;
      }
      else
      {
        bool3 = false;
        paramMessage = null;
      }
      paramBoolean = 2131166204;
      int i1;
      if (bool3)
      {
        paramMessage = A;
        bool3 = paramMessage.getWidth();
        bool1 = ((com.truecaller.ui.view.i)localObject3).getMeasuredWidth();
        boolean bool4;
        bool3 -= bool1;
        localObject1 = getResources();
        bool1 = ((Resources)localObject1).getDimensionPixelSize(paramBoolean);
        bool4 -= bool1;
      }
      else
      {
        paramMessage = getResources();
        i1 = paramMessage.getDimensionPixelSize(paramBoolean);
      }
      boolean bool1 = ((View)localObject2).getTop();
      boolean bool6 = ((com.truecaller.ui.view.i)localObject3).getMeasuredHeight();
      bool1 -= bool6;
      if (bool1)
      {
        bool1 = true;
      }
      else
      {
        bool1 = false;
        localObject1 = null;
      }
      if (bool1)
      {
        paramBoolean = ((View)localObject2).getHeight();
        bool1 = ((com.truecaller.ui.view.i)localObject3).getMeasuredHeight();
        paramBoolean += bool1;
        localObject1 = getContext();
        i2 = 1092616192;
        float f1 = 10.0F;
        boolean bool2 = at.a((Context)localObject1, f1);
        paramBoolean = (paramBoolean - bool2) * -1;
      }
      else
      {
        localObject1 = getResources();
        paramBoolean = ((Resources)localObject1).getDimensionPixelSize(paramBoolean) * -1;
      }
      localObject1 = new android/widget/PopupWindow;
      int i2 = -2;
      ((PopupWindow)localObject1).<init>((View)localObject3, i2, i2, false);
      al = ((PopupWindow)localObject1);
      localObject1 = al;
      localObject3 = new android/graphics/drawable/ColorDrawable;
      ((ColorDrawable)localObject3).<init>(0);
      ((PopupWindow)localObject1).setBackgroundDrawable((Drawable)localObject3);
      al.setOutsideTouchable(bool5);
      al.showAsDropDown((View)localObject2, i1, paramBoolean);
      return;
    }
  }
  
  public final void a(long paramLong, int paramInt)
  {
    Intent localIntent = new android/content/Intent;
    android.support.v4.app.f localf = getActivity();
    localIntent.<init>(localf, ConversationInfoActivity.class);
    localIntent.putExtra("conversation_id", paramLong);
    localIntent.putExtra("filter", paramInt);
    startActivityForResult(localIntent, 2);
  }
  
  public final void a(long paramLong1, long paramLong2, int paramInt)
  {
    com.truecaller.messaging.g.k.a(this, paramLong1, paramLong2, paramInt, 2131886476);
  }
  
  public final void a(Uri paramUri)
  {
    q.a(paramUri, null);
  }
  
  public final void a(Uri paramUri, String paramString, Drawable paramDrawable)
  {
    Object localObject = M;
    if (localObject == null)
    {
      localObject = L.inflate();
      M = ((View)localObject);
      localObject = (TextView)M.findViewById(2131362973);
      N = ((TextView)localObject);
      localObject = M.findViewById(2131362971);
      O = ((View)localObject);
      localObject = M;
      i1 = 2131362965;
      localObject = (ImageView)((View)localObject).findViewById(i1);
      P = ((ImageView)localObject);
    }
    localObject = M;
    int i1 = 0;
    ((View)localObject).setVisibility(0);
    localObject = N;
    paramString = Html.fromHtml(paramString);
    ((TextView)localObject).setText(paramString);
    paramString = O;
    paramString.setBackground(paramDrawable);
    if (paramUri != null)
    {
      P.setVisibility(0);
      paramUri = ((bj)com.bumptech.glide.e.b(getContext())).b(paramUri).l().k();
      paramString = P;
      paramUri.a(paramString);
      return;
    }
    P.setVisibility(8);
  }
  
  public final void a(Uri paramUri, String paramString, Runnable paramRunnable)
  {
    d.a(paramUri, paramString, paramRunnable);
  }
  
  public final void a(Parcelable paramParcelable)
  {
    A.getLayoutManager().onRestoreInstanceState(paramParcelable);
  }
  
  public final void a(Spannable paramSpannable)
  {
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Context localContext = getContext();
    localBuilder.<init>(localContext);
    localBuilder.setTitle(2131886660).setMessage(paramSpannable).setCancelable(true).show();
  }
  
  public final void a(com.truecaller.android.truemoji.j paramj)
  {
    Object localObject = x;
    h.a locala = new com/truecaller/android/truemoji/h$a;
    locala.<init>((EmojiRootLayout)localObject);
    localObject = new com/truecaller/messaging/conversation/-$$Lambda$k$a34LlaAmz58HBeAjZKoYVxWd91I;
    ((-..Lambda.k.a34LlaAmz58HBeAjZKoYVxWd91I)localObject).<init>(this);
    i = ((com.truecaller.android.truemoji.a.a)localObject);
    localObject = new com/truecaller/messaging/conversation/-$$Lambda$k$FQgbWRtvcYX74r-t5fJxGeGDihQ;
    ((-..Lambda.k.FQgbWRtvcYX74r-t5fJxGeGDihQ)localObject).<init>(this);
    e = ((h.d)localObject);
    localObject = new com/truecaller/messaging/conversation/-$$Lambda$k$50itvEUbRNdYhfBol4cANV1Z4i0;
    ((-..Lambda.k.50itvEUbRNdYhfBol4cANV1Z4i0)localObject).<init>(this);
    j = ((h.c)localObject);
    k = paramj;
    MediaEditText localMediaEditText = t;
    paramj = com.truecaller.android.truemoji.f.b;
    com.truecaller.android.truemoji.f.c().b();
    com.truecaller.android.truemoji.c.b.a(localMediaEditText, "EditText can't be null");
    paramj = new com/truecaller/android/truemoji/h;
    EmojiRootLayout localEmojiRootLayout = a;
    com.truecaller.android.truemoji.j localj = k;
    p localp = l;
    int i1 = b;
    int i2 = c;
    int i3 = d;
    paramj.<init>(localEmojiRootLayout, localMediaEditText, localj, localp, i1, i2, i3, (byte)0);
    localObject = f;
    j = ((h.e)localObject);
    localObject = i;
    m = ((com.truecaller.android.truemoji.a.a)localObject);
    localObject = g;
    k = ((h.f)localObject);
    localObject = e;
    i = ((h.d)localObject);
    localObject = j;
    n = ((h.c)localObject);
    localObject = h;
    l = ((h.b)localObject);
    w = paramj;
  }
  
  public final void a(Contact paramContact, byte[] paramArrayOfByte)
  {
    paramContact = com.truecaller.util.t.a(paramContact, paramArrayOfByte);
    com.truecaller.common.h.o.a(this, paramContact, 1);
  }
  
  public final void a(Number paramNumber)
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = m;
      if (localObject != null)
      {
        android.support.v4.app.f localf = getActivity();
        View localView = ac;
        -..Lambda.k.Ido2xCdrnuUw7xgfmxUDTX-jgtk localIdo2xCdrnuUw7xgfmxUDTX-jgtk = new com/truecaller/messaging/conversation/-$$Lambda$k$Ido2xCdrnuUw7xgfmxUDTX-jgtk;
        localIdo2xCdrnuUw7xgfmxUDTX-jgtk.<init>(this);
        ((cg)localObject).a(localf, localView, paramNumber, localIdo2xCdrnuUw7xgfmxUDTX-jgtk);
        return;
      }
    }
  }
  
  public final void a(FlashContact paramFlashContact)
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    com.truecaller.flashsdk.core.b localb = l;
    long l1 = Long.parseLong(a);
    String str = b;
    localb.b(localContext, l1, str, "conversation");
  }
  
  public final void a(com.truecaller.messaging.conversation.a.b.o paramo, boolean paramBoolean)
  {
    View localView = getView();
    if (localView != null)
    {
      if (paramBoolean) {
        paramBoolean = 2131888852;
      } else {
        paramBoolean = 2131888851;
      }
      localView = getView();
      Snackbar localSnackbar = Snackbar.a(localView, paramBoolean, 0);
      int i1 = 2131888853;
      -..Lambda.k.OU1UJSOLIrK8yzff2UpKCWG0o1M localOU1UJSOLIrK8yzff2UpKCWG0o1M = new com/truecaller/messaging/conversation/-$$Lambda$k$OU1UJSOLIrK8yzff2UpKCWG0o1M;
      localOU1UJSOLIrK8yzff2UpKCWG0o1M.<init>(this, paramo);
      localSnackbar.a(i1, localOU1UJSOLIrK8yzff2UpKCWG0o1M);
      localSnackbar.c();
    }
  }
  
  public final void a(ImGroupInfo paramImGroupInfo)
  {
    paramImGroupInfo = ImGroupInfoActivity.a.a(getContext(), paramImGroupInfo);
    startActivity(paramImGroupInfo);
  }
  
  public final void a(Message paramMessage)
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    Entity[] arrayOfEntity = n;
    int i1 = arrayOfEntity.length;
    int i2 = 0;
    while (i2 < i1)
    {
      Object localObject1 = arrayOfEntity[i2];
      boolean bool = localObject1 instanceof BinaryEntity;
      if (bool)
      {
        String str1 = paramMessage.j();
        Object localObject2 = localObject1;
        localObject2 = (BinaryEntity)localObject1;
        Uri localUri = b;
        String str2 = j;
        localObject1 = ai.a(null, null, str1, localUri, str2, null);
        com.truecaller.common.h.o.a(localContext, (Intent)localObject1);
      }
      i2 += 1;
    }
  }
  
  public final void a(Message paramMessage, int[] paramArrayOfInt1, int[] paramArrayOfInt2)
  {
    Object localObject1 = ai;
    if (localObject1 != null) {
      ((AlertDialog)localObject1).dismiss();
    }
    int i1 = paramArrayOfInt1.length;
    localObject1 = new String[i1];
    int i2 = 0;
    AlertDialog.Builder localBuilder = null;
    for (;;)
    {
      int i3 = paramArrayOfInt1.length;
      if (i2 >= i3) {
        break;
      }
      i3 = paramArrayOfInt1[i2];
      localObject2 = getString(i3);
      localObject1[i2] = localObject2;
      i2 += 1;
    }
    localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject2 = getContext();
    localBuilder.<init>((Context)localObject2);
    localBuilder = localBuilder.setTitle(2131886491);
    localObject2 = new com/truecaller/messaging/conversation/br;
    Context localContext = getContext();
    ((br)localObject2).<init>(localContext, (String[])localObject1, paramArrayOfInt2);
    paramArrayOfInt2 = new com/truecaller/messaging/conversation/-$$Lambda$k$E2fDSyhxceh4-OCfO1qs-r98VuE;
    paramArrayOfInt2.<init>(this, paramMessage, paramArrayOfInt1);
    paramMessage = localBuilder.setAdapter((ListAdapter)localObject2, paramArrayOfInt2).create();
    ai = paramMessage;
    ai.show();
  }
  
  public final void a(Participant paramParticipant)
  {
    try
    {
      Object localObject1 = new com/truecaller/data/entity/Contact;
      ((Contact)localObject1).<init>();
      Object localObject2 = new com/truecaller/data/entity/Number;
      String str = f;
      ((Number)localObject2).<init>(str);
      ((Contact)localObject1).a((Number)localObject2);
      localObject2 = m;
      if (localObject2 != null)
      {
        paramParticipant = m;
        ((Contact)localObject1).l(paramParticipant);
      }
      paramParticipant = a;
      paramParticipant.getClass();
      localObject2 = new com/truecaller/messaging/conversation/-$$Lambda$26bwNf7HIHLsMmM-2J4INgu4WPk;
      ((-..Lambda.26bwNf7HIHLsMmM-2J4INgu4WPk)localObject2).<init>(paramParticipant);
      paramParticipant = com.truecaller.util.br.a((Contact)localObject1, (br.a)localObject2);
      localObject1 = getFragmentManager();
      localObject2 = com.truecaller.util.br.a;
      paramParticipant.show((android.support.v4.app.j)localObject1, (String)localObject2);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      com.truecaller.log.d.a(localActivityNotFoundException, "Cannot find an activity to insert contact");
    }
  }
  
  public final void a(ReplySnippet paramReplySnippet)
  {
    boolean bool = true;
    s(bool);
    t.requestFocus();
    com.truecaller.utils.extensions.t.a(t, bool, 0L);
    R.a(paramReplySnippet, false);
    paramReplySnippet = R;
    -..Lambda.k.f0GLy3yu7WZvgv3WKA9gVuhg63I localf0GLy3yu7WZvgv3WKA9gVuhg63I = new com/truecaller/messaging/conversation/-$$Lambda$k$f0GLy3yu7WZvgv3WKA9gVuhg63I;
    localf0GLy3yu7WZvgv3WKA9gVuhg63I.<init>(this);
    paramReplySnippet.setDismissActionListener(localf0GLy3yu7WZvgv3WKA9gVuhg63I);
  }
  
  public final void a(c.a parama)
  {
    AvailabilityView localAvailabilityView = s;
    int i1 = com.truecaller.utils.ui.b.a(getContext(), 2130968840);
    localAvailabilityView.setTextColor(i1);
    s.a(parama);
  }
  
  public final void a(Boolean paramBoolean, Long paramLong)
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      int i1 = -1;
      Object localObject = new android/content/Intent;
      ((Intent)localObject).<init>();
      String str = "RESULT_NUMBER_BLOCKED";
      paramBoolean = ((Intent)localObject).putExtra(str, paramBoolean);
      localObject = "CONVERSATION_ID";
      paramBoolean = paramBoolean.putExtra((String)localObject, paramLong);
      localf.setResult(i1, paramBoolean);
    }
  }
  
  public final void a(String paramString)
  {
    r.setText(paramString);
  }
  
  public final void a(String paramString, int paramInt)
  {
    Object localObject = new android/support/v7/app/AlertDialog$Builder;
    android.support.v4.app.f localf = getActivity();
    ((AlertDialog.Builder)localObject).<init>(localf);
    paramString = ((AlertDialog.Builder)localObject).setMessage(paramString).setCancelable(false);
    localObject = new com/truecaller/messaging/conversation/-$$Lambda$k$rZNlF5Nvrrpkkr53NUbfSPLWXCs;
    ((-..Lambda.k.rZNlF5Nvrrpkkr53NUbfSPLWXCs)localObject).<init>(this, paramInt);
    paramString.setPositiveButton(2131886506, (DialogInterface.OnClickListener)localObject).setNegativeButton(2131887636, null).create().show();
  }
  
  public final void a(String paramString1, String paramString2)
  {
    TransactionActivity.startForSend(getContext(), paramString1, paramString2);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    com.truecaller.ui.dialogs.o localo = new com/truecaller/ui/dialogs/o;
    Context localContext = getContext();
    localo.<init>(localContext, paramString1, paramString2, paramString3);
    paramString1 = new com/truecaller/messaging/conversation/-$$Lambda$k$6zPfkia1JyjneK_dSlP-L3xJ2jM;
    paramString1.<init>(this, localo);
    localo.setOnDismissListener(paramString1);
    localo.show();
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    Context localContext = getContext();
    DetailsFragment.SourceType localSourceType = DetailsFragment.SourceType.Conversation;
    DetailsFragment.a(localContext, paramString4, paramString3, paramString1, paramString2, null, localSourceType, true, 20);
  }
  
  public final void a(String paramString, boolean paramBoolean)
  {
    com.truecaller.ui.details.a locala = new com/truecaller/ui/details/a;
    Context localContext = getContext();
    locala.<init>(localContext, paramString, paramBoolean, true);
    paramString = new com/truecaller/messaging/conversation/-$$Lambda$k$v-IJHCIgtPLUgU3ZsQFNVaZ78bg;
    paramString.<init>(this);
    a = paramString;
    paramString = new com/truecaller/messaging/conversation/-$$Lambda$k$iIVoaTkTCyQrUcoi3LlhkRgyMqc;
    paramString.<init>(this);
    b = paramString;
    locala.show();
  }
  
  public final void a(ArrayList paramArrayList)
  {
    Intent localIntent = new android/content/Intent;
    Context localContext = getContext();
    localIntent.<init>(localContext, NewConversationActivity.class);
    localIntent.setAction("android.intent.action.SEND");
    localIntent.putParcelableArrayListExtra("forward_content", paramArrayList);
    startActivity(localIntent);
  }
  
  public final void a(List paramList)
  {
    int i1 = paramList.size();
    com.truecaller.ui.components.i[] arrayOfi = new com.truecaller.ui.components.i[i1];
    int i2 = 0;
    for (;;)
    {
      int i3 = paramList.size();
      if (i2 >= i3) {
        break;
      }
      com.truecaller.ui.components.i locali = com.truecaller.messaging.i.k.a((com.truecaller.messaging.e)paramList.get(i2));
      arrayOfi[i2] = locali;
      i2 += 1;
    }
    ag.setMenuItems(arrayOfi);
    ag.b();
  }
  
  public final void a(Map paramMap)
  {
    Object localObject = getContext();
    if (localObject != null)
    {
      localObject = new com/truecaller/ui/dialogs/i;
      Context localContext = getContext();
      al localal = TrueApp.y().a().t();
      ((com.truecaller.ui.dialogs.i)localObject).<init>(localContext, localal, paramMap);
      ((com.truecaller.ui.dialogs.i)localObject).show();
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    if (!paramBoolean)
    {
      RecordToastView localRecordToastView = ad;
      int i1 = R.id.chronometerCounter;
      Chronometer localChronometer = (Chronometer)localRecordToastView.a(i1);
      localChronometer.stop();
      localRecordToastView.a();
    }
    ad.setVisible(paramBoolean);
  }
  
  public final void a(boolean paramBoolean, int paramInt1, int paramInt2)
  {
    Object localObject1 = v;
    if (paramBoolean)
    {
      paramBoolean = false;
      localTextView = null;
    }
    else
    {
      paramBoolean = true;
    }
    ((TextView)localObject1).setVisibility(paramBoolean);
    TextView localTextView = v;
    localObject1 = getResources();
    Object[] arrayOfObject = new Object[2];
    Object localObject2 = Integer.valueOf(paramInt1);
    arrayOfObject[0] = localObject2;
    Integer localInteger = Integer.valueOf(paramInt2);
    arrayOfObject[1] = localInteger;
    localObject2 = ((Resources)localObject1).getString(2131886485, arrayOfObject);
    localTextView.setText((CharSequence)localObject2);
  }
  
  public final void a(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    Object localObject = af;
    if (localObject == null)
    {
      localObject = (ViewGroup)getView();
      if (localObject == null) {
        return;
      }
      e locale1 = new com/truecaller/messaging/conversation/e;
      View localView = D;
      m localm = b;
      bn localbn = d;
      locale1.<init>((ViewGroup)localObject, localView, localm, localbn);
      af = locale1;
    }
    at.a(af.a, paramBoolean1);
    e locale2 = af;
    at.a(b, paramBoolean2);
    at.a(c, paramBoolean2);
    at.a(d, paramBoolean2);
    at.a(af.e, paramBoolean3);
    af.a();
  }
  
  public final void a(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, boolean paramBoolean8, boolean paramBoolean9, boolean paramBoolean10)
  {
    Object localObject = ah;
    if (localObject != null)
    {
      localObject = ((ActionMode)localObject).getMenu();
      int i1 = 2131361853;
      MenuItem localMenuItem1 = ((Menu)localObject).findItem(i1);
      localMenuItem1.setVisible(paramBoolean1);
      ((Menu)localObject).findItem(2131361872).setVisible(paramBoolean2);
      ((Menu)localObject).findItem(2131361868).setVisible(paramBoolean3);
      ((Menu)localObject).findItem(2131361863).setVisible(paramBoolean4);
      ((Menu)localObject).findItem(2131361936).setVisible(paramBoolean5);
      ((Menu)localObject).findItem(2131361916).setVisible(paramBoolean6);
      ((Menu)localObject).findItem(2131361935).setVisible(paramBoolean7);
      ((Menu)localObject).findItem(2131361927).setVisible(paramBoolean8);
      ((Menu)localObject).findItem(2131361864).setVisible(paramBoolean8);
      ((Menu)localObject).findItem(2131361926).setVisible(paramBoolean9);
      paramBoolean1 = 2131361914;
      MenuItem localMenuItem2 = ((Menu)localObject).findItem(paramBoolean1);
      localMenuItem2.setVisible(paramBoolean10);
    }
  }
  
  public final void a(boolean paramBoolean, long... paramVarArgs)
  {
    int i1 = 1;
    boolean[] arrayOfBoolean = new boolean[i1];
    -..Lambda.k.CkDhhpasZfpxnILyLhgKUr2Cj5o localCkDhhpasZfpxnILyLhgKUr2Cj5o = null;
    arrayOfBoolean[0] = i1;
    int i2 = paramVarArgs.length;
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject1 = getContext();
    localBuilder.<init>((Context)localObject1);
    localObject1 = getResources();
    Object localObject2 = new Object[i1];
    Integer localInteger = Integer.valueOf(i2);
    localObject2[0] = localInteger;
    int i3 = 2131755010;
    localObject2 = ((Resources)localObject1).getQuantityString(i3, i2, (Object[])localObject2);
    localObject2 = localBuilder.setMessage((CharSequence)localObject2);
    -..Lambda.k.pXqm4568Ad9bjA-CpjfDwn2QINk localpXqm4568Ad9bjA-CpjfDwn2QINk = new com/truecaller/messaging/conversation/-$$Lambda$k$pXqm4568Ad9bjA-CpjfDwn2QINk;
    localpXqm4568Ad9bjA-CpjfDwn2QINk.<init>(this, paramBoolean, arrayOfBoolean, paramVarArgs);
    int i4 = 2131887540;
    paramVarArgs = ((AlertDialog.Builder)localObject2).setPositiveButton(i4, localpXqm4568Ad9bjA-CpjfDwn2QINk);
    localObject2 = new com/truecaller/messaging/conversation/-$$Lambda$k$xDj3EoTpb1wuGgRBStH7M9PL4FQ;
    ((-..Lambda.k.xDj3EoTpb1wuGgRBStH7M9PL4FQ)localObject2).<init>(this);
    i2 = 2131887197;
    paramVarArgs = paramVarArgs.setNegativeButton(i2, (DialogInterface.OnClickListener)localObject2);
    if (paramBoolean)
    {
      Object localObject3 = LayoutInflater.from(getContext());
      i2 = 0;
      localpXqm4568Ad9bjA-CpjfDwn2QINk = null;
      localObject3 = ((LayoutInflater)localObject3).inflate(2131558849, null, false);
      i1 = 2131362466;
      localObject2 = (CheckBox)((View)localObject3).findViewById(i1);
      localCkDhhpasZfpxnILyLhgKUr2Cj5o = new com/truecaller/messaging/conversation/-$$Lambda$k$CkDhhpasZfpxnILyLhgKUr2Cj5o;
      localCkDhhpasZfpxnILyLhgKUr2Cj5o.<init>(arrayOfBoolean);
      ((CheckBox)localObject2).setOnCheckedChangeListener(localCkDhhpasZfpxnILyLhgKUr2Cj5o);
      paramVarArgs.setView((View)localObject3);
    }
    paramVarArgs.show();
  }
  
  public final void a(Message... paramVarArgs)
  {
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject = getContext();
    localBuilder.<init>((Context)localObject);
    localBuilder = localBuilder.setTitle(2131886472).setMessage(2131886473);
    localObject = new com/truecaller/messaging/conversation/-$$Lambda$k$lPMfqI3MniofPpJAONTW7hBDSYg;
    ((-..Lambda.k.lPMfqI3MniofPpJAONTW7hBDSYg)localObject).<init>(this, paramVarArgs);
    localBuilder.setPositiveButton(2131887235, (DialogInterface.OnClickListener)localObject).setNegativeButton(2131887197, null).show();
  }
  
  public final boolean a(Uri paramUri, String paramString)
  {
    Intent localIntent = new android/content/Intent;
    String str = "android.intent.action.VIEW";
    localIntent.<init>(str);
    localIntent.setDataAndType(paramUri, paramString);
    int i1 = 1;
    localIntent.setFlags(i1);
    try
    {
      startActivity(localIntent);
      return i1;
    }
    catch (ActivityNotFoundException localActivityNotFoundException) {}
    return false;
  }
  
  public final void aa()
  {
    Dialog localDialog = am;
    if (localDialog != null)
    {
      localDialog.dismiss();
      localDialog = null;
      am = null;
    }
  }
  
  public final void b()
  {
    RecordToastView localRecordToastView = ad;
    localRecordToastView.setVisibility(0);
    int i1 = R.id.tvRecordTip;
    ((TextView)localRecordToastView.a(i1)).setText(2131886469);
    i1 = R.id.chronometerCounter;
    Object localObject = (Chronometer)localRecordToastView.a(i1);
    c.g.b.k.a(localObject, "chronometerCounter");
    ((Chronometer)localObject).setVisibility(0);
    i1 = R.id.recordDot;
    localObject = (ImageView)localRecordToastView.a(i1);
    c.g.b.k.a(localObject, "recordDot");
    ((ImageView)localObject).setVisibility(0);
    localRecordToastView.b();
  }
  
  public final void b(int paramInt)
  {
    q.setContactBadgeDrawable(paramInt);
  }
  
  public final void b(Uri paramUri)
  {
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject1 = getContext();
    localBuilder.<init>((Context)localObject1);
    localBuilder = localBuilder.setTitle(2131886707);
    localObject1 = new CharSequence[2];
    Object localObject2 = getString(2131886705);
    localObject1[0] = localObject2;
    localObject2 = getString(2131886706);
    localObject1[1] = localObject2;
    localObject2 = new com/truecaller/messaging/conversation/-$$Lambda$k$S93BX9AIRpW_Dw3Cr1Dn0rVGH_s;
    ((-..Lambda.k.S93BX9AIRpW_Dw3Cr1Dn0rVGH_s)localObject2).<init>(this, paramUri);
    localBuilder.setItems((CharSequence[])localObject1, (DialogInterface.OnClickListener)localObject2).create().show();
  }
  
  public final void b(ImGroupInfo paramImGroupInfo)
  {
    Context localContext = getContext();
    if (localContext != null)
    {
      paramImGroupInfo = ImGroupInvitationActivity.a.a(localContext, paramImGroupInfo);
      localContext.startActivity(paramImGroupInfo);
    }
  }
  
  public final void b(String paramString)
  {
    s.setVisibility(0);
    AvailabilityView localAvailabilityView = s;
    int i1 = com.truecaller.utils.ui.b.a(getContext(), 2130968841);
    localAvailabilityView.setTextColor(i1);
    s.setText(paramString);
  }
  
  public final void b(ArrayList paramArrayList)
  {
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    l.a(localContext, paramArrayList, "conversation");
  }
  
  public final void b(boolean paramBoolean)
  {
    q.setIsGroup(paramBoolean);
  }
  
  public final void b(Message... paramVarArgs)
  {
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject = getContext();
    localBuilder.<init>((Context)localObject);
    localObject = LayoutInflater.from(getContext()).inflate(2131558589, null, false);
    localBuilder = localBuilder.setCustomTitle((View)localObject);
    localObject = new com/truecaller/messaging/conversation/-$$Lambda$k$TuFHGoD79yNPRP4QpPDHyJTrVYE;
    ((-..Lambda.k.TuFHGoD79yNPRP4QpPDHyJTrVYE)localObject).<init>(this, paramVarArgs);
    localBuilder.setSingleChoiceItems(2130903041, -1, (DialogInterface.OnClickListener)localObject).setNegativeButton(2131887197, null).show();
  }
  
  public final void c()
  {
    RecordToastView localRecordToastView = ad;
    localRecordToastView.a();
    localRecordToastView.setVisibility(4);
  }
  
  public final void c(int paramInt)
  {
    s.setVisibility(0);
    AvailabilityView localAvailabilityView = s;
    int i1 = com.truecaller.utils.ui.b.a(getContext(), 2130968840);
    localAvailabilityView.setTextColor(i1);
    localAvailabilityView = s;
    Resources localResources = getResources();
    Object[] arrayOfObject = new Object[1];
    Integer localInteger = Integer.valueOf(paramInt);
    arrayOfObject[0] = localInteger;
    String str = localResources.getQuantityString(2131755026, paramInt, arrayOfObject);
    localAvailabilityView.setText(str);
  }
  
  public final void c(String paramString)
  {
    t.setText(paramString);
  }
  
  public final void c(boolean paramBoolean)
  {
    q.setIsSpam(paramBoolean);
  }
  
  public final void d()
  {
    RecordView localRecordView = ae;
    RecordFloatingActionButton localRecordFloatingActionButton = u;
    String str = "recordBtn";
    c.g.b.k.b(localRecordFloatingActionButton, str);
    boolean bool = j;
    if (bool)
    {
      str = null;
      com.truecaller.utils.extensions.t.a(localRecordView, false);
      int i1 = 1065353216;
      float f1 = 1.0F;
      localRecordFloatingActionButton.a(f1);
      localRecordView.a();
    }
  }
  
  public final void d(int paramInt)
  {
    t.setHint(paramInt);
  }
  
  public final void d(String paramString)
  {
    Toast.makeText(getContext(), paramString, 0).show();
  }
  
  public final void d(boolean paramBoolean)
  {
    TextView localTextView = r;
    Object localObject;
    if (paramBoolean)
    {
      localObject = getContext();
      int i1 = 2131234692;
      int i2 = 2130968839;
      localObject = com.truecaller.utils.ui.b.a((Context)localObject, i1, i2);
    }
    else
    {
      paramBoolean = false;
      localObject = null;
    }
    android.support.v4.widget.m.a(localTextView, null, (Drawable)localObject);
  }
  
  public final void e()
  {
    p.notifyDataSetChanged();
  }
  
  public final void e(int paramInt)
  {
    RecordFloatingActionButton localRecordFloatingActionButton = u;
    ColorStateList localColorStateList = ColorStateList.valueOf(paramInt);
    localRecordFloatingActionButton.setBackgroundTintList(localColorStateList);
  }
  
  public final void e(String paramString)
  {
    Object localObject = getActivity();
    if (localObject == null) {
      return;
    }
    localObject = new com/truecaller/calling/initiate_call/b$a$a;
    ((b.a.a)localObject).<init>(paramString, "conversation");
    paramString = k;
    localObject = ((b.a.a)localObject).a();
    paramString.a((b.a)localObject);
  }
  
  public final void e(boolean paramBoolean)
  {
    t.setEnabled(paramBoolean);
  }
  
  public final CharSequence f()
  {
    return t.getText();
  }
  
  public final void f(int paramInt)
  {
    z.setImageResource(paramInt);
  }
  
  public final void f(String paramString)
  {
    ActionMode localActionMode = ah;
    if (localActionMode != null) {
      localActionMode.setTitle(paramString);
    }
  }
  
  public final void f(boolean paramBoolean)
  {
    ImageView localImageView = z;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localImageView.setVisibility(paramBoolean);
  }
  
  public final void g()
  {
    t.requestFocus();
    MediaEditText localMediaEditText = t;
    int i1 = localMediaEditText.getText().length();
    localMediaEditText.setSelection(i1);
  }
  
  public final void g(int paramInt)
  {
    Object localObject = getContext();
    String str = ab();
    localObject = DefaultSmsActivity.a((Context)localObject, str, false);
    startActivityForResult((Intent)localObject, paramInt);
  }
  
  public final void g(String paramString)
  {
    RequiredPermissionsActivity.a(getContext(), paramString);
  }
  
  public final void g(boolean paramBoolean)
  {
    int i1 = 1;
    boolean[] arrayOfBoolean = new boolean[i1];
    int i2 = 0;
    CheckBox localCheckBox = null;
    arrayOfBoolean[0] = i1;
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject1 = getContext();
    localBuilder.<init>((Context)localObject1);
    localBuilder = localBuilder.setMessage(2131886492);
    localObject1 = new com/truecaller/messaging/conversation/-$$Lambda$k$ulknWL4I5r4DfKFJkWNt8WrRALM;
    ((-..Lambda.k.ulknWL4I5r4DfKFJkWNt8WrRALM)localObject1).<init>(this, paramBoolean, arrayOfBoolean);
    localBuilder = localBuilder.setPositiveButton(2131887540, (DialogInterface.OnClickListener)localObject1);
    localObject1 = null;
    int i3 = 2131887197;
    localBuilder = localBuilder.setNegativeButton(i3, null);
    if (paramBoolean)
    {
      Object localObject2 = LayoutInflater.from(getContext());
      i3 = 2131558849;
      localObject2 = ((LayoutInflater)localObject2).inflate(i3, null, false);
      i2 = 2131362466;
      localCheckBox = (CheckBox)((View)localObject2).findViewById(i2);
      localObject1 = new com/truecaller/messaging/conversation/-$$Lambda$k$xBpEVKaf7gVdGuKNhgIqiDGY-P0;
      ((-..Lambda.k.xBpEVKaf7gVdGuKNhgIqiDGY-P0)localObject1).<init>(arrayOfBoolean);
      localCheckBox.setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject1);
      localBuilder.setView((View)localObject2);
    }
    localBuilder.show();
  }
  
  public final void h()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null) {
      localf.finish();
    }
  }
  
  public final void h(int paramInt)
  {
    Object localObject = ah;
    if (localObject != null)
    {
      localObject = ((ActionMode)localObject).getMenu();
      int i1 = 2131361927;
      localObject = ((Menu)localObject).findItem(i1);
      ((MenuItem)localObject).setTitle(paramInt);
    }
  }
  
  public final void h(String paramString)
  {
    com.truecaller.common.h.o.d(requireContext(), paramString);
  }
  
  public final void h(boolean paramBoolean)
  {
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Context localContext = getContext();
    localBuilder.<init>(localContext);
    localContext = null;
    int i1;
    CheckBox localCheckBox;
    if (paramBoolean)
    {
      Object localObject = LayoutInflater.from(getContext());
      local3TFMkX-hseMgJTouAB6EPEnHW6I = null;
      localObject = ((LayoutInflater)localObject).inflate(2131558849, null, false);
      i1 = 2131362466;
      localCheckBox = (CheckBox)((View)localObject).findViewById(i1);
      localBuilder.setView((View)localObject);
    }
    else
    {
      i1 = 0;
      localCheckBox = null;
    }
    localBuilder = localBuilder.setMessage(2131886413);
    -..Lambda.k.3TFMkX-hseMgJTouAB6EPEnHW6I local3TFMkX-hseMgJTouAB6EPEnHW6I = new com/truecaller/messaging/conversation/-$$Lambda$k$3TFMkX-hseMgJTouAB6EPEnHW6I;
    local3TFMkX-hseMgJTouAB6EPEnHW6I.<init>(this, paramBoolean, localCheckBox);
    localBuilder.setPositiveButton(2131887223, local3TFMkX-hseMgJTouAB6EPEnHW6I).setNegativeButton(2131887197, null).show();
  }
  
  public final void i()
  {
    TruecallerInit.b(getActivity(), "conversation");
  }
  
  public final void i(int paramInt)
  {
    Resources localResources = getResources();
    Object[] arrayOfObject = new Object[1];
    Integer localInteger = Integer.valueOf(paramInt);
    arrayOfObject[0] = localInteger;
    String str = localResources.getQuantityString(2131755009, paramInt, arrayOfObject);
    Toast.makeText(getContext(), str, 0).show();
  }
  
  public final void i(String paramString)
  {
    com.truecaller.common.h.o.e(requireContext(), paramString);
  }
  
  public final void i(boolean paramBoolean)
  {
    RecyclerView localRecyclerView = C;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localRecyclerView.setVisibility(paramBoolean);
  }
  
  public final void j()
  {
    getActivity().invalidateOptionsMenu();
  }
  
  public final void j(int paramInt)
  {
    Resources localResources = getResources();
    Object[] arrayOfObject = new Object[1];
    Integer localInteger = Integer.valueOf(paramInt);
    arrayOfObject[0] = localInteger;
    String str = localResources.getQuantityString(2131755012, paramInt, arrayOfObject);
    Toast.makeText(getContext(), str, 0).show();
  }
  
  public final void j(String paramString)
  {
    com.truecaller.common.h.o.a(requireContext(), paramString);
  }
  
  public final void j(boolean paramBoolean)
  {
    E.setVisible(paramBoolean);
  }
  
  public final void k()
  {
    cg localcg = m;
    if (localcg == null) {
      return;
    }
    localcg.a();
  }
  
  public final void k(int paramInt)
  {
    Resources localResources = getResources();
    Object[] arrayOfObject = new Object[1];
    Integer localInteger = Integer.valueOf(paramInt);
    arrayOfObject[0] = localInteger;
    String str = localResources.getQuantityString(2131755011, paramInt, arrayOfObject);
    Toast.makeText(getContext(), str, 0).show();
  }
  
  public final void k(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      ag.setVisibility(0);
      ag.setFabActionListener(this);
      RecordFloatingActionButton localRecordFloatingActionButton = u;
      -..Lambda.k.q_tbSvy6s6OCrprjlCEo891khGE localq_tbSvy6s6OCrprjlCEo891khGE = new com/truecaller/messaging/conversation/-$$Lambda$k$q_tbSvy6s6OCrprjlCEo891khGE;
      localq_tbSvy6s6OCrprjlCEo891khGE.<init>(this);
      localRecordFloatingActionButton.setOnLongClickListener(localq_tbSvy6s6OCrprjlCEo891khGE);
      return;
    }
    ag.setVisibility(8);
    ag.setFabActionListener(null);
    u.setOnLongClickListener(null);
  }
  
  public final boolean k(String paramString)
  {
    return com.truecaller.wizard.utils.i.a(getActivity(), paramString);
  }
  
  public final void l()
  {
    Object localObject1 = LayoutInflater.from(getContext()).inflate(2131558600, null);
    Object localObject2 = ((View)localObject1).findViewById(2131363757).getBackground();
    int i1 = com.truecaller.utils.ui.b.a(getContext(), 2130969228);
    PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
    ((Drawable)localObject2).setColorFilter(i1, localMode);
    localObject2 = new android/support/v7/app/AlertDialog$Builder;
    Context localContext = getContext();
    ((AlertDialog.Builder)localObject2).<init>(localContext);
    localObject1 = ((AlertDialog.Builder)localObject2).setTitle(2131886489).setView((View)localObject1).setPositiveButton(2131886785, null);
    -..Lambda.k.FsS0fz-BUg3mc9VDTG2MeTNcrRc localFsS0fz-BUg3mc9VDTG2MeTNcrRc = new com/truecaller/messaging/conversation/-$$Lambda$k$FsS0fz-BUg3mc9VDTG2MeTNcrRc;
    localFsS0fz-BUg3mc9VDTG2MeTNcrRc.<init>(this);
    ((AlertDialog.Builder)localObject1).setOnDismissListener(localFsS0fz-BUg3mc9VDTG2MeTNcrRc).show();
  }
  
  public final void l(int paramInt)
  {
    A.smoothScrollToPosition(paramInt);
  }
  
  public final void l(String paramString)
  {
    Toast.makeText(getContext(), paramString, 1).show();
  }
  
  public final void l(boolean paramBoolean)
  {
    u.setRecordingEnabled(paramBoolean);
  }
  
  public final void m()
  {
    Object localObject = com.truecaller.messaging.g.k.a;
    localObject = ab();
    com.truecaller.messaging.g.k.a(this, (String)localObject);
  }
  
  public final void m(int paramInt)
  {
    Toast.makeText(getContext(), paramInt, 0).show();
  }
  
  public final void m(String paramString)
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = new android/content/Intent;
      ((Intent)localObject).<init>("android.intent.action.VIEW");
      String str = "truecaller://balance_check?bank_symbol=";
      paramString = String.valueOf(paramString);
      paramString = Uri.parse(str.concat(paramString));
      ((Intent)localObject).setData(paramString);
      int i1 = 268468224;
      ((Intent)localObject).addFlags(i1);
      startActivity((Intent)localObject);
    }
  }
  
  public final void m(boolean paramBoolean)
  {
    View localView = D;
    int i1;
    float f1;
    if (paramBoolean)
    {
      i1 = 1065353216;
      f1 = 1.0F;
    }
    else
    {
      i1 = 1050253722;
      f1 = 0.3F;
    }
    localView.setAlpha(f1);
    localView = D;
    Object localObject;
    if (paramBoolean)
    {
      localObject = getContext();
      i1 = 2130969389;
      f1 = 1.7547459E38F;
      localObject = com.truecaller.utils.ui.b.c((Context)localObject, i1);
    }
    else
    {
      paramBoolean = false;
      localObject = null;
    }
    localView.setBackground((Drawable)localObject);
  }
  
  public final void n()
  {
    Object localObject = com.truecaller.messaging.g.k.a;
    localObject = j;
    com.truecaller.messaging.g.k.a(this, (com.truecaller.tcpermissions.l)localObject);
  }
  
  public final void n(int paramInt)
  {
    EmojiPokeView localEmojiPokeView = Q;
    -..Lambda.k.VDD8Vczgdlx0XvjO1TArTLLpFkE localVDD8Vczgdlx0XvjO1TArTLLpFkE = new com/truecaller/messaging/conversation/-$$Lambda$k$VDD8Vczgdlx0XvjO1TArTLLpFkE;
    localVDD8Vczgdlx0XvjO1TArTLLpFkE.<init>(this);
    localEmojiPokeView.a(paramInt, true, localVDD8Vczgdlx0XvjO1TArTLLpFkE);
  }
  
  public final void n(String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.VIEW");
    paramString = Uri.parse(paramString);
    localIntent.setData(paramString);
    startActivity(localIntent);
  }
  
  public final void n(boolean paramBoolean)
  {
    boolean bool = true;
    if (paramBoolean)
    {
      K.a(null, bool);
      return;
    }
    K.b(null, bool);
  }
  
  public final void o()
  {
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Object localObject = getContext();
    localBuilder.<init>((Context)localObject);
    localBuilder = localBuilder.setMessage(2131886194);
    localObject = new com/truecaller/messaging/conversation/-$$Lambda$k$qL5wX9n4vvtLWfMg_i26vN5XtWI;
    ((-..Lambda.k.qL5wX9n4vvtLWfMg_i26vN5XtWI)localObject).<init>(this);
    localBuilder = localBuilder.setPositiveButton(2131887235, (DialogInterface.OnClickListener)localObject);
    localObject = new com/truecaller/messaging/conversation/-$$Lambda$k$TbSZLvRdqy5sxs8mmaFie0apa-U;
    ((-..Lambda.k.TbSZLvRdqy5sxs8mmaFie0apa-U)localObject).<init>(this);
    localBuilder.setNegativeButton(2131887214, (DialogInterface.OnClickListener)localObject).show();
  }
  
  public final void o(int paramInt)
  {
    Q.a(paramInt, false, null);
  }
  
  public final void o(String paramString)
  {
    Object localObject1 = new android/support/v7/app/AlertDialog$Builder;
    Object localObject2 = getContext();
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    localObject2 = new Object[1];
    localObject2[0] = paramString;
    paramString = getString(2131886610, (Object[])localObject2);
    paramString = ((AlertDialog.Builder)localObject1).setTitle(paramString).setMessage(2131886609);
    localObject1 = new com/truecaller/messaging/conversation/-$$Lambda$k$xBxLtINogHIz615dnKNfrBlo214;
    ((-..Lambda.k.xBxLtINogHIz615dnKNfrBlo214)localObject1).<init>(this);
    paramString.setPositiveButton(2131886608, (DialogInterface.OnClickListener)localObject1).setNegativeButton(2131886387, null).show();
  }
  
  public final void o(boolean paramBoolean)
  {
    ConstraintLayout localConstraintLayout = S;
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localConstraintLayout.setVisibility(paramBoolean);
  }
  
  public final boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
  {
    paramActionMode = a;
    int i1 = paramMenuItem.getItemId();
    return paramActionMode.b(i1);
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    a.a(paramInt1, paramInt2, paramIntent);
    d.a(paramInt1, paramInt2, paramIntent);
    int i1 = 0;
    Object localObject1 = null;
    int i2 = -1;
    if (paramInt2 == i2) {
      paramInt2 = 1;
    } else {
      paramInt2 = 0;
    }
    i2 = 0;
    Object localObject2 = null;
    m localm;
    switch (paramInt1)
    {
    default: 
      break;
    case 203: 
      localm = b;
      localObject2 = new java/util/ArrayList;
      ((ArrayList)localObject2).<init>();
      if (paramIntent != null)
      {
        Object localObject3 = paramIntent.getClipData();
        if (localObject3 != null)
        {
          localObject3 = paramIntent.getClipData();
          int i3 = ((ClipData)localObject3).getItemCount();
          if (i3 > 0) {
            for (;;)
            {
              localObject3 = paramIntent.getClipData();
              i3 = ((ClipData)localObject3).getItemCount();
              if (i1 >= i3) {
                break;
              }
              localObject3 = paramIntent.getClipData().getItemAt(i1);
              if (localObject3 != null)
              {
                localObject3 = ((ClipData.Item)localObject3).getUri();
                ((List)localObject2).add(localObject3);
              }
              i1 += 1;
            }
          }
        }
        localObject1 = paramIntent.getData();
        if (localObject1 != null)
        {
          paramIntent = paramIntent.getData();
          ((List)localObject2).add(paramIntent);
        }
      }
      localm.a(paramInt2, (List)localObject2);
      break;
    case 202: 
      localm = b;
      if (paramIntent != null)
      {
        localObject1 = i;
        localObject2 = ((com.truecaller.util.c.b)localObject1).a(paramIntent);
      }
      localm.a(paramInt2, (com.truecaller.util.c.a)localObject2);
      return;
    case 201: 
      localm = b;
      if (paramIntent != null) {
        localObject2 = paramIntent.getData();
      }
      localm.a(paramInt2, (Uri)localObject2);
      return;
    case 200: 
      b.b(paramInt2);
      return;
    }
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    ((f)paramContext).a().a(this);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getArguments();
    Object localObject1 = new String[0];
    AssertionUtil.isNotNull(paramBundle, (String[])localObject1);
    paramBundle = new com/avito/konveyor/a/e;
    localObject1 = e;
    Object localObject2 = g;
    paramBundle.<init>((com.avito.konveyor.a.a)localObject1, (com.avito.konveyor.b.e)localObject2);
    p = paramBundle;
    paramBundle = p;
    int i1 = 1;
    paramBundle.setHasStableIds(i1);
    paramBundle = f;
    localObject2 = new com/truecaller/messaging/conversation/k$8;
    Context localContext = getContext();
    ((k.8)localObject2).<init>(this, localContext);
    paramBundle.a(i1, (com.truecaller.messaging.conversation.b.d)localObject2);
    paramBundle = f;
    localObject1 = new com/truecaller/messaging/conversation/k$9;
    localObject2 = getContext();
    ((k.9)localObject1).<init>(this, (Context)localObject2);
    paramBundle.a(2, (com.truecaller.messaging.conversation.b.d)localObject1);
    paramBundle = f;
    localObject1 = new com/truecaller/messaging/conversation/k$10;
    localObject2 = getContext();
    ((k.10)localObject1).<init>(this, (Context)localObject2);
    paramBundle.a(4, (com.truecaller.messaging.conversation.b.d)localObject1);
    paramBundle = f;
    localObject1 = new com/truecaller/messaging/conversation/k$11;
    localObject2 = getContext();
    ((k.11)localObject1).<init>(this, (Context)localObject2);
    paramBundle.a(5, (com.truecaller.messaging.conversation.b.d)localObject1);
    paramBundle = f;
    localObject1 = new com/truecaller/messaging/conversation/k$12;
    localObject2 = getContext();
    ((k.12)localObject1).<init>(this, (Context)localObject2);
    paramBundle.a(98, (com.truecaller.messaging.conversation.b.d)localObject1);
    paramBundle = f;
    localObject1 = new com/truecaller/messaging/conversation/k$13;
    localObject2 = getContext();
    ((k.13)localObject1).<init>(this, (Context)localObject2);
    paramBundle.a(99, (com.truecaller.messaging.conversation.b.d)localObject1);
    paramBundle = f;
    localObject1 = new com/truecaller/messaging/conversation/k$14;
    localObject2 = getContext();
    ((k.14)localObject1).<init>(this, (Context)localObject2);
    paramBundle.a(6, (com.truecaller.messaging.conversation.b.d)localObject1);
    paramBundle = f;
    localObject1 = new com/truecaller/messaging/conversation/k$2;
    localObject2 = getContext();
    ((k.2)localObject1).<init>(this, (Context)localObject2);
    paramBundle.a(7, (com.truecaller.messaging.conversation.b.d)localObject1);
    paramBundle = f;
    localObject1 = new com/truecaller/messaging/conversation/bz$b;
    localObject2 = getContext();
    ((bz.b)localObject1).<init>((Context)localObject2);
    paramBundle.a(3, (com.truecaller.messaging.conversation.b.d)localObject1);
    paramBundle = new com/truecaller/messaging/conversation/bl;
    localObject1 = d;
    paramBundle.<init>((bn)localObject1);
    aj = paramBundle;
  }
  
  public final boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    paramActionMode.getMenuInflater().inflate(2131623943, paramMenu);
    ah = paramActionMode;
    return true;
  }
  
  public final void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
    paramMenuInflater.inflate(2131623946, paramMenu);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    setHasOptionsMenu(true);
    return paramLayoutInflater.inflate(2131558678, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    TelephonyManager localTelephonyManager = com.truecaller.utils.extensions.i.b((Context)Objects.requireNonNull(getContext()));
    PhoneStateListener localPhoneStateListener = ao;
    localTelephonyManager.listen(localPhoneStateListener, 0);
    a.y_();
    b.y_();
    d.y_();
    c.d = null;
    super.onDestroy();
  }
  
  public final void onDestroyActionMode(ActionMode paramActionMode)
  {
    a.p();
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    bb localbb = a;
    int i1 = paramMenuItem.getItemId();
    return localbb.c(i1);
  }
  
  public final void onPause()
  {
    super.onPause();
    a.i();
  }
  
  public final boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    return false;
  }
  
  public final void onPrepareOptionsMenu(Menu paramMenu)
  {
    super.onPrepareOptionsMenu(paramMenu);
    a.a(paramMenu);
    b.a(paramMenu);
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    com.truecaller.wizard.utils.i.a(paramArrayOfString, paramArrayOfInt);
    d.a(paramInt, paramArrayOfString, paramArrayOfInt);
    b.a(paramInt, paramArrayOfString, paramArrayOfInt);
    a.a(paramInt, paramArrayOfString, paramArrayOfInt);
  }
  
  public final void onResume()
  {
    super.onResume();
    a.h();
  }
  
  public final void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    a.b(paramBundle);
    d.b(paramBundle);
    String str = c.e;
    paramBundle.putString("sim_token", str);
  }
  
  public final void onStart()
  {
    super.onStart();
    a.j();
    t.setMediaCallback(this);
  }
  
  public final void onStop()
  {
    super.onStop();
    a.k();
    t.setMediaCallback(null);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    Object localObject1 = c;
    if (paramBundle != null)
    {
      localObject2 = paramBundle.getString("sim_token");
      ((com.truecaller.messaging.d.a)localObject1).b((String)localObject2);
    }
    else
    {
      ((com.truecaller.messaging.d.a)localObject1).b();
    }
    a.a(paramBundle);
    d.a(paramBundle);
    paramBundle = getArguments();
    boolean bool = false;
    localObject1 = null;
    Object localObject2 = new String[0];
    AssertionUtil.isNotNull(paramBundle, (String[])localObject2);
    int i2 = 2131364907;
    localObject2 = (Toolbar)paramView.findViewById(i2);
    Object localObject3 = (ContactPhoto)paramView.findViewById(2131362554);
    q = ((ContactPhoto)localObject3);
    localObject3 = (TextView)paramView.findViewById(2131364892);
    r = ((TextView)localObject3);
    localObject3 = (AvailabilityView)paramView.findViewById(2131362066);
    s = ((AvailabilityView)localObject3);
    localObject3 = (MediaEditText)paramView.findViewById(2131363761);
    t = ((MediaEditText)localObject3);
    localObject3 = (TextView)paramView.findViewById(2131364809);
    v = ((TextView)localObject3);
    localObject3 = t;
    Object localObject4 = an;
    ((MediaEditText)localObject3).addTextChangedListener((TextWatcher)localObject4);
    localObject3 = (ImageView)paramView.findViewById(2131364519);
    z = ((ImageView)localObject3);
    localObject3 = z;
    localObject4 = new com/truecaller/messaging/conversation/-$$Lambda$k$aPcsBeZlGA2fYLKJEK3k-sI_10k;
    ((-..Lambda.k.aPcsBeZlGA2fYLKJEK3k-sI_10k)localObject4).<init>(this);
    ((ImageView)localObject3).setOnClickListener((View.OnClickListener)localObject4);
    localObject3 = paramView;
    localObject3 = (EmojiRootLayout)paramView;
    x = ((EmojiRootLayout)localObject3);
    localObject3 = (TintedImageView)paramView.findViewById(2131362955);
    y = ((TintedImageView)localObject3);
    localObject3 = paramView.findViewById(2131363474);
    B = ((View)localObject3);
    localObject3 = (ViewGroup)paramView.findViewById(2131362140);
    F = ((ViewGroup)localObject3);
    localObject3 = (ImageView)paramView.findViewById(2131363473);
    G = ((ImageView)localObject3);
    localObject3 = (RecyclerView)paramView.findViewById(2131362985);
    C = ((RecyclerView)localObject3);
    localObject3 = (TextView)paramView.findViewById(2131364700);
    H = ((TextView)localObject3);
    localObject3 = paramView.findViewById(2131363247);
    I = ((View)localObject3);
    localObject3 = (Button)paramView.findViewById(2131363246);
    J = ((Button)localObject3);
    localObject3 = J;
    localObject4 = new com/truecaller/messaging/conversation/-$$Lambda$k$jUBdFjrXQKXCkn059fCH7qXZXlk;
    ((-..Lambda.k.jUBdFjrXQKXCkn059fCH7qXZXlk)localObject4).<init>(this);
    ((Button)localObject3).setOnClickListener((View.OnClickListener)localObject4);
    localObject3 = (android.support.design.widget.FloatingActionButton)paramView.findViewById(2131363867);
    K = ((android.support.design.widget.FloatingActionButton)localObject3);
    localObject3 = K;
    localObject4 = new com/truecaller/messaging/conversation/-$$Lambda$k$1-YTMgKfzs-ikqTIfkUaXKUMz4s;
    ((-..Lambda.k.1-YTMgKfzs-ikqTIfkUaXKUMz4s)localObject4).<init>(this);
    ((android.support.design.widget.FloatingActionButton)localObject3).setOnClickListener((View.OnClickListener)localObject4);
    localObject3 = (ViewStub)paramView.findViewById(2131362974);
    L = ((ViewStub)localObject3);
    localObject3 = (EmojiPokeView)paramView.findViewById(2131362954);
    Q = ((EmojiPokeView)localObject3);
    localObject3 = (RecordFloatingActionButton)paramView.findViewById(2131364292);
    u = ((RecordFloatingActionButton)localObject3);
    localObject3 = u;
    localObject4 = new com/truecaller/messaging/conversation/-$$Lambda$k$Nbzvd9ATq2H8M9Wj-N97HEvwjhU;
    ((-..Lambda.k.Nbzvd9ATq2H8M9Wj-N97HEvwjhU)localObject4).<init>(this);
    ((RecordFloatingActionButton)localObject3).setOnClickListener((View.OnClickListener)localObject4);
    int i3 = Build.VERSION.SDK_INT;
    int i5 = 21;
    if (i3 < i5)
    {
      localObject3 = u;
      i6 = 0;
      localObject4 = null;
      ((RecordFloatingActionButton)localObject3).setCompatElevation(0.0F);
    }
    localObject3 = (EmojiPokeButton)paramView.findViewById(2131362953);
    E = ((EmojiPokeButton)localObject3);
    localObject3 = E;
    localObject4 = new com/truecaller/messaging/conversation/-$$Lambda$k$unfHQKPQjjYKpCTJ_XtU57W579M;
    ((-..Lambda.k.unfHQKPQjjYKpCTJ_XtU57W579M)localObject4).<init>(this);
    ((EmojiPokeButton)localObject3).setOnClickListener((View.OnClickListener)localObject4);
    c.d = this;
    d.a(this);
    b.a(this);
    localObject3 = C;
    localObject4 = aj;
    ((RecyclerView)localObject3).setAdapter((RecyclerView.Adapter)localObject4);
    localObject3 = paramView.findViewById(2131362053);
    D = ((View)localObject3);
    localObject3 = D;
    localObject4 = new com/truecaller/messaging/conversation/-$$Lambda$k$1GYlS9fAoxjX-yHDIEWbS8tNhzg;
    ((-..Lambda.k.1GYlS9fAoxjX-yHDIEWbS8tNhzg)localObject4).<init>(this);
    ((View)localObject3).setOnClickListener((View.OnClickListener)localObject4);
    localObject3 = (com.truecaller.ui.components.FloatingActionButton)paramView.findViewById(2131363031);
    ag = ((com.truecaller.ui.components.FloatingActionButton)localObject3);
    ag.getButtonView().getLayoutParams().width = 0;
    localObject3 = ag.getButtonView().getLayoutParams();
    int i6 = ag.getContext().getResources().getDimensionPixelSize(2131165753);
    height = i6;
    localObject3 = (RecyclerView)paramView.findViewById(2131364093);
    A = ((RecyclerView)localObject3);
    localObject3 = (MessageSnippetView)paramView.findViewById(2131365478);
    R = ((MessageSnippetView)localObject3);
    localObject3 = (ConstraintLayout)paramView.findViewById(2131363465);
    S = ((ConstraintLayout)localObject3);
    localObject3 = S.getBackground();
    localObject4 = requireContext();
    int i7 = 2130969060;
    i6 = com.truecaller.utils.ui.b.a((Context)localObject4, i7);
    Object localObject5 = PorterDuff.Mode.SRC_IN;
    ((Drawable)localObject3).setColorFilter(i6, (PorterDuff.Mode)localObject5);
    localObject3 = paramView.findViewById(2131363122);
    localObject4 = new com/truecaller/messaging/conversation/-$$Lambda$k$wFnp7pO6ehw41MbrIXMZNfoeVuo;
    ((-..Lambda.k.wFnp7pO6ehw41MbrIXMZNfoeVuo)localObject4).<init>(this);
    ((View)localObject3).setOnClickListener((View.OnClickListener)localObject4);
    localObject3 = (TintedImageView)paramView.findViewById(2131363464);
    T = ((TintedImageView)localObject3);
    localObject3 = (Flow)paramView.findViewById(2131363125);
    W = ((Flow)localObject3);
    localObject3 = W;
    localObject4 = new com/truecaller/messaging/conversation/-$$Lambda$k$h0z4fVQgKgc2WHOudBf5L-bHMNo;
    ((-..Lambda.k.h0z4fVQgKgc2WHOudBf5L-bHMNo)localObject4).<init>(this);
    ((Flow)localObject3).setOnClickListener((View.OnClickListener)localObject4);
    localObject3 = (TintedImageView)paramView.findViewById(2131363468);
    U = ((TintedImageView)localObject3);
    localObject3 = (Flow)paramView.findViewById(2131363123);
    X = ((Flow)localObject3);
    localObject3 = X;
    localObject4 = new com/truecaller/messaging/conversation/-$$Lambda$k$zoMx0bk74Xy386Lb3PBUoJ5HaTI;
    ((-..Lambda.k.zoMx0bk74Xy386Lb3PBUoJ5HaTI)localObject4).<init>(this);
    ((Flow)localObject3).setOnClickListener((View.OnClickListener)localObject4);
    localObject3 = (TintedImageView)paramView.findViewById(2131363470);
    V = ((TintedImageView)localObject3);
    localObject3 = (Flow)paramView.findViewById(2131363124);
    Y = ((Flow)localObject3);
    localObject3 = Y;
    localObject4 = new com/truecaller/messaging/conversation/-$$Lambda$k$eT_na2ylamOCkqW-nTAQcE3MWGg;
    ((-..Lambda.k.eT_na2ylamOCkqW-nTAQcE3MWGg)localObject4).<init>(this);
    ((Flow)localObject3).setOnClickListener((View.OnClickListener)localObject4);
    localObject3 = (TintedTextView)paramView.findViewById(2131363471);
    Z = ((TintedTextView)localObject3);
    localObject3 = (TintedTextView)paramView.findViewById(2131363472);
    aa = ((TintedTextView)localObject3);
    localObject3 = (TintedTextView)paramView.findViewById(2131363469);
    ab = ((TintedTextView)localObject3);
    localObject3 = paramView.findViewById(2131363937);
    ac = ((View)localObject3);
    localObject3 = (RecordToastView)paramView.findViewById(2131364085);
    ad = ((RecordToastView)localObject3);
    a.a(this);
    localObject3 = b;
    localObject4 = paramBundle.getString("initial_content");
    paramBundle = paramBundle.getParcelableArrayList("initial_attachments");
    ((m)localObject3).a((String)localObject4, paramBundle);
    paramBundle = new com/truecaller/messaging/conversation/k$6;
    localObject3 = getContext();
    paramBundle.<init>(this, (Context)localObject3);
    i3 = 1;
    paramBundle.setReverseLayout(i3);
    A.setLayoutManager(paramBundle);
    localObject4 = A;
    localObject5 = p;
    ((RecyclerView)localObject4).setAdapter((RecyclerView.Adapter)localObject5);
    localObject4 = A;
    localObject5 = new com/truecaller/messaging/conversation/-$$Lambda$k$yJJs88eJMFQrde6ngMZGy5mpVHc;
    ((-..Lambda.k.yJJs88eJMFQrde6ngMZGy5mpVHc)localObject5).<init>(this);
    ((RecyclerView)localObject4).setRecyclerListener((RecyclerView.RecyclerListener)localObject5);
    localObject4 = A;
    localObject5 = new com/truecaller/messaging/conversation/k$3;
    Object localObject6 = paramView.getContext();
    float f1 = 100.0F;
    int i8 = at.a((Context)localObject6, f1);
    ((k.3)localObject5).<init>(this, i8, paramBundle);
    ((RecyclerView)localObject4).addOnScrollListener((RecyclerView.OnScrollListener)localObject5);
    paramBundle = new android/support/v7/widget/helper/ItemTouchHelper;
    localObject4 = new com/truecaller/messaging/conversation/cm;
    localObject5 = paramView.getContext();
    localObject6 = new com/truecaller/messaging/conversation/-$$Lambda$k$yzENtEBO0jfJAFMEqb3ZsxSLK_k;
    ((-..Lambda.k.yzENtEBO0jfJAFMEqb3ZsxSLK_k)localObject6).<init>(this);
    ((cm)localObject4).<init>((Context)localObject5, (cm.a)localObject6);
    paramBundle.<init>((ItemTouchHelper.Callback)localObject4);
    localObject4 = A;
    paramBundle.attachToRecyclerView((RecyclerView)localObject4);
    paramBundle = A;
    localObject4 = new com/truecaller/messaging/conversation/k$4;
    ((k.4)localObject4).<init>(this);
    paramBundle.addOnItemTouchListener((RecyclerView.OnItemTouchListener)localObject4);
    at.a((Toolbar)localObject2);
    ((AppCompatActivity)getActivity()).setSupportActionBar((Toolbar)localObject2);
    paramBundle = ((AppCompatActivity)getActivity()).getSupportActionBar();
    if (paramBundle != null)
    {
      paramBundle.setDisplayHomeAsUpEnabled(i3);
      paramBundle.setDisplayShowTitleEnabled(false);
    }
    int i9 = 2131363879;
    paramBundle = paramView.findViewById(i9);
    localObject1 = new com/truecaller/messaging/conversation/-$$Lambda$k$rOkKcygDztX9rtOEHvhyHFmSC2g;
    ((-..Lambda.k.rOkKcygDztX9rtOEHvhyHFmSC2g)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    paramBundle = A;
    paramBundle.setItemAnimator(null);
    paramBundle = A;
    localObject1 = n.e();
    bool = ((com.truecaller.featuretoggles.b)localObject1).a();
    i2 = 2131166182;
    int i1;
    int i4;
    if (bool)
    {
      i1 = 2131559207;
      localObject3 = requireContext().getResources();
      i4 = ((Resources)localObject3).getDimensionPixelSize(i2);
    }
    else
    {
      i1 = 2131559206;
      localObject3 = requireContext().getResources();
      i6 = 2131166204;
      i4 = ((Resources)localObject3).getDimensionPixelSize(i6);
    }
    localObject4 = new com/truecaller/messaging/conversation/l;
    localObject5 = requireContext();
    ((l)localObject4).<init>((Context)localObject5, i1, i4);
    paramBundle.addItemDecoration((RecyclerView.ItemDecoration)localObject4);
    paramBundle = A;
    localObject1 = c.a((int)getResources().getDimension(i2));
    paramBundle.addItemDecoration((RecyclerView.ItemDecoration)localObject1);
    paramBundle = y;
    localObject1 = new com/truecaller/messaging/conversation/-$$Lambda$k$nmxm3GOqeISb-_6KJsIDGCEBOY4;
    ((-..Lambda.k.nmxm3GOqeISb-_6KJsIDGCEBOY4)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
    paramBundle = (RecordView)paramView.findViewById(2131364083);
    ae = paramBundle;
    paramBundle = u;
    localObject1 = ae;
    paramBundle.setRecordView((RecordView)localObject1);
    paramBundle = ae;
    localObject1 = TimeUnit.MILLISECONDS;
    long l1 = b.H();
    localObject4 = TimeUnit.MINUTES;
    long l2 = ((TimeUnit)localObject1).convert(l1, (TimeUnit)localObject4);
    paramBundle.setMaxDurationMs(l2);
    paramBundle = ae;
    localObject1 = new com/truecaller/messaging/conversation/k$5;
    ((k.5)localObject1).<init>(this);
    paramBundle.setRecordListener((RecordView.a)localObject1);
    paramView = com.truecaller.utils.extensions.i.b(paramView.getContext());
    paramBundle = ao;
    paramView.listen(paramBundle, 32);
  }
  
  public final void p() {}
  
  public final void p(int paramInt)
  {
    AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
    Context localContext = getContext();
    localBuilder.<init>(localContext);
    localBuilder.setMessage(paramInt).setPositiveButton(2131887217, null).show();
  }
  
  public final void p(boolean paramBoolean)
  {
    W.setEnabled(paramBoolean);
    TintedImageView localTintedImageView = T;
    float f1;
    if (paramBoolean)
    {
      paramBoolean = 1065353216;
      f1 = 1.0F;
    }
    else
    {
      paramBoolean = 1053609165;
      f1 = 0.4F;
    }
    localTintedImageView.setAlpha(f1);
    TintedTextView localTintedTextView = aa;
    float f2 = T.getAlpha();
    localTintedTextView.setAlpha(f2);
  }
  
  public final void q(int paramInt)
  {
    String[] arrayOfString = { "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE" };
    com.truecaller.wizard.utils.i.b(this, arrayOfString, paramInt);
  }
  
  public final void q(boolean paramBoolean)
  {
    X.setEnabled(paramBoolean);
    TintedImageView localTintedImageView = U;
    float f1;
    if (paramBoolean)
    {
      paramBoolean = 1065353216;
      f1 = 1.0F;
    }
    else
    {
      paramBoolean = 1053609165;
      f1 = 0.4F;
    }
    localTintedImageView.setAlpha(f1);
    TintedTextView localTintedTextView = ab;
    float f2 = U.getAlpha();
    localTintedTextView.setAlpha(f2);
  }
  
  public final boolean q()
  {
    PopupWindow localPopupWindow = w.e;
    boolean bool = localPopupWindow.isShowing();
    if (bool)
    {
      w.a();
      return true;
    }
    return false;
  }
  
  public final void r()
  {
    h localh = w;
    if (localh != null) {
      localh.a();
    }
  }
  
  public final void r(int paramInt)
  {
    E.setImageResource(paramInt);
  }
  
  public final void r(boolean paramBoolean)
  {
    Object localObject1 = Y;
    int i1 = 8;
    int i2 = 0;
    Flow localFlow = null;
    if (paramBoolean) {
      i3 = 0;
    } else {
      i3 = 8;
    }
    ((Flow)localObject1).setVisibility(i3);
    localObject1 = V;
    if (paramBoolean)
    {
      i1 = 0;
      localObject2 = null;
    }
    ((TintedImageView)localObject1).setVisibility(i1);
    localObject1 = Z;
    i1 = V.getVisibility();
    ((TintedTextView)localObject1).setVisibility(i1);
    localObject1 = new android/support/constraint/d;
    ((android.support.constraint.d)localObject1).<init>();
    Object localObject2 = S;
    ((android.support.constraint.d)localObject1).a((ConstraintLayout)localObject2);
    localObject2 = X;
    i1 = ((Flow)localObject2).getId();
    int i3 = 7;
    ((android.support.constraint.d)localObject1).b(i1, i3);
    i1 = 6;
    if (paramBoolean)
    {
      paramBoolean = X.getId();
      i2 = W.getId();
      ((android.support.constraint.d)localObject1).a(paramBoolean, i1, i2, i3);
      localObject3 = X;
      paramBoolean = ((Flow)localObject3).getId();
      localFlow = Y;
      i2 = localFlow.getId();
      ((android.support.constraint.d)localObject1).a(paramBoolean, i3, i2, i1);
    }
    else
    {
      paramBoolean = X.getId();
      ((android.support.constraint.d)localObject1).b(paramBoolean, i1);
      localObject3 = X;
      paramBoolean = ((Flow)localObject3).getId();
      ((android.support.constraint.d)localObject1).a(paramBoolean, i3, 0, i3);
    }
    Object localObject3 = S;
    ((android.support.constraint.d)localObject1).b((ConstraintLayout)localObject3);
  }
  
  public final void s()
  {
    a(0.0F, 1.0F).start();
  }
  
  public final void s(int paramInt)
  {
    H.setText(paramInt);
    H.setVisibility(0);
  }
  
  public final void t()
  {
    a(1.0F, 0.0F).start();
  }
  
  public final void u()
  {
    ((AppCompatActivity)getActivity()).startSupportActionMode(this);
  }
  
  public final void v()
  {
    ActionMode localActionMode = ah;
    if (localActionMode != null) {
      localActionMode.finish();
    }
  }
  
  public final Parcelable w()
  {
    return A.getLayoutManager().onSaveInstanceState();
  }
  
  public final void x()
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.GET_CONTENT");
    localIntent.setType("*/*");
    Object localObject = { "image/*", "video/*" };
    String str = "android.intent.extra.MIME_TYPES";
    localIntent.putExtra(str, (String[])localObject);
    localObject = b;
    boolean bool1 = ((m)localObject).A();
    if (bool1)
    {
      localObject = "android.intent.extra.ALLOW_MULTIPLE";
      boolean bool2 = true;
      localIntent.putExtra((String)localObject, bool2);
    }
    startActivityForResult(localIntent, 203);
  }
  
  public final void y()
  {
    Object localObject = new com/google/android/gms/location/places/ui/PlacePicker$IntentBuilder;
    ((PlacePicker.IntentBuilder)localObject).<init>();
    try
    {
      android.support.v4.app.f localf = getActivity();
      localObject = ((PlacePicker.IntentBuilder)localObject).a(localf);
      int i1 = 202;
      startActivityForResult((Intent)localObject, i1);
      return;
    }
    catch (GooglePlayServicesRepairableException|GooglePlayServicesNotAvailableException localGooglePlayServicesRepairableException)
    {
      a.q();
    }
  }
  
  public final void z()
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.PICK");
    localIntent.setType("vnd.android.cursor.dir/phone_v2");
    startActivityForResult(localIntent, 201);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */