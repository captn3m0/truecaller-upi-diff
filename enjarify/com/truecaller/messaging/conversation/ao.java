package com.truecaller.messaging.conversation;

import dagger.a.d;
import javax.inject.Provider;

public final class ao
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private ao(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static ao a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    ao localao = new com/truecaller/messaging/conversation/ao;
    localao.<init>(paramProvider1, paramProvider2, paramProvider3);
    return localao;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.ao
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */