package com.truecaller.messaging.conversation;

import dagger.a.d;
import javax.inject.Provider;

public final class ax
  implements d
{
  private final s a;
  private final Provider b;
  
  private ax(s params, Provider paramProvider)
  {
    a = params;
    b = paramProvider;
  }
  
  public static ax a(s params, Provider paramProvider)
  {
    ax localax = new com/truecaller/messaging/conversation/ax;
    localax.<init>(params, paramProvider);
    return localax;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.ax
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */