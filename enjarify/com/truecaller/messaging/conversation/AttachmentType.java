package com.truecaller.messaging.conversation;

public enum AttachmentType
{
  public final int icon;
  public final boolean supported;
  public final int title;
  
  static
  {
    AttachmentType localAttachmentType = new com/truecaller/messaging/conversation/AttachmentType;
    Object localObject1 = localAttachmentType;
    localAttachmentType.<init>("IMAGE", 0, true, 0, 0);
    IMAGE = localAttachmentType;
    localObject1 = new com/truecaller/messaging/conversation/AttachmentType;
    boolean bool1 = true;
    ((AttachmentType)localObject1).<init>("AUDIO", 1, bool1, 2131234279, 2131886407);
    AUDIO = (AttachmentType)localObject1;
    localObject1 = new com/truecaller/messaging/conversation/AttachmentType;
    boolean bool2 = true;
    Object localObject2 = localObject1;
    ((AttachmentType)localObject1).<init>("VIDEO", 2, bool2, 2131234284, 2131886412);
    VIDEO = (AttachmentType)localObject1;
    localObject1 = new com/truecaller/messaging/conversation/AttachmentType;
    ((AttachmentType)localObject1).<init>("VCARD", 3, bool1, 2131234283, 2131886408);
    VCARD = (AttachmentType)localObject1;
    localObject1 = new com/truecaller/messaging/conversation/AttachmentType;
    localObject2 = localObject1;
    ((AttachmentType)localObject1).<init>("PENDING_MMS", 4, bool2, 2131234282, 2131886512);
    PENDING_MMS = (AttachmentType)localObject1;
    localObject1 = new com/truecaller/messaging/conversation/AttachmentType;
    ((AttachmentType)localObject1).<init>("UNKNOWN", 5, false, 2131234282, 2131886411);
    UNKNOWN = (AttachmentType)localObject1;
    localObject1 = new AttachmentType[6];
    localObject2 = IMAGE;
    localObject1[0] = localObject2;
    localObject2 = AUDIO;
    localObject1[1] = localObject2;
    localObject2 = VIDEO;
    localObject1[2] = localObject2;
    localObject2 = VCARD;
    localObject1[3] = localObject2;
    localObject2 = PENDING_MMS;
    localObject1[4] = localObject2;
    localObject2 = UNKNOWN;
    localObject1[5] = localObject2;
    $VALUES = (AttachmentType[])localObject1;
  }
  
  private AttachmentType(boolean paramBoolean, int paramInt2, int paramInt3)
  {
    supported = paramBoolean;
    icon = paramInt2;
    title = paramInt3;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.conversation.AttachmentType
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */