package com.truecaller.messaging;

import dagger.a.d;
import javax.inject.Provider;

public final class n
  implements d
{
  private final l a;
  private final Provider b;
  
  private n(l paraml, Provider paramProvider)
  {
    a = paraml;
    b = paramProvider;
  }
  
  public static n a(l paraml, Provider paramProvider)
  {
    n localn = new com/truecaller/messaging/n;
    localn.<init>(paraml, paramProvider);
    return localn;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */