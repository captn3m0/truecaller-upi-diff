package com.truecaller.messaging;

import android.net.Uri;
import c.g.b.k;

public final class d
{
  public static final d.a c;
  public final Uri a;
  public final String b;
  
  static
  {
    d.a locala = new com/truecaller/messaging/d$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public d(Uri paramUri, byte paramByte)
  {
    this(paramUri);
  }
  
  public d(Uri paramUri, String paramString)
  {
    a = paramUri;
    b = paramString;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof d;
      if (bool1)
      {
        paramObject = (d)paramObject;
        Object localObject = a;
        Uri localUri = a;
        bool1 = k.a(localObject, localUri);
        if (bool1)
        {
          localObject = b;
          paramObject = b;
          boolean bool2 = k.a(localObject, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final int hashCode()
  {
    Uri localUri = a;
    int i = 0;
    int j;
    if (localUri != null)
    {
      j = localUri.hashCode();
    }
    else
    {
      j = 0;
      localUri = null;
    }
    j *= 31;
    String str = b;
    if (str != null) {
      i = str.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("MediaAttachRequest(uri=");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", mimeType=");
    localObject = b;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */