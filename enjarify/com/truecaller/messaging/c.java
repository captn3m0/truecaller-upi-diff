package com.truecaller.messaging;

import c.a.m;
import c.g.b.k;
import com.truecaller.messaging.data.types.BinaryEntity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class c
{
  public static final List a(List paramList)
  {
    k.b(paramList, "receiver$0");
    paramList = (Iterable)paramList;
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    localObject = (Collection)localObject;
    paramList = paramList.iterator();
    for (;;)
    {
      boolean bool = paramList.hasNext();
      if (!bool) {
        break;
      }
      BinaryEntity localBinaryEntity = nextb;
      if (localBinaryEntity != null) {
        ((Collection)localObject).add(localBinaryEntity);
      }
    }
    return (List)localObject;
  }
  
  public static final void a(Collection paramCollection, String paramString, List paramList)
  {
    k.b(paramCollection, "receiver$0");
    k.b(paramString, "text");
    Object localObject1 = "entities";
    k.b(paramList, (String)localObject1);
    boolean bool1 = paramList.isEmpty();
    int i = 0;
    ForwardContentItem localForwardContentItem = null;
    if (!bool1)
    {
      localObject1 = paramList;
    }
    else
    {
      bool1 = false;
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      localObject1 = (Iterable)localObject1;
      i = 0;
      localForwardContentItem = null;
      localObject1 = ((Iterable)localObject1).iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        Object localObject2 = ((Iterator)localObject1).next();
        int j = i + 1;
        if (i < 0) {
          m.a();
        }
        localObject2 = (BinaryEntity)localObject2;
        int k = paramList.size() + -1;
        if (i != k)
        {
          localForwardContentItem = new com/truecaller/messaging/ForwardContentItem;
          String str = "";
          localForwardContentItem.<init>(str, (BinaryEntity)localObject2);
        }
        else
        {
          localForwardContentItem = new com/truecaller/messaging/ForwardContentItem;
          localForwardContentItem.<init>(paramString, (BinaryEntity)localObject2);
        }
        paramCollection.add(localForwardContentItem);
        i = j;
      }
      return;
    }
    paramList = new com/truecaller/messaging/ForwardContentItem;
    paramList.<init>(paramString, null);
    paramCollection.add(paramList);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */