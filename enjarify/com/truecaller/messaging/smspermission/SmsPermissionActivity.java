package com.truecaller.messaging.smspermission;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import com.truecaller.bk;
import com.truecaller.bp;
import com.truecaller.log.AssertionUtil;
import com.truecaller.messaging.defaultsms.DefaultSmsActivity;
import com.truecaller.tcpermissions.l;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;
import com.truecaller.ui.TruecallerInit;
import com.truecaller.wizard.utils.i;

public class SmsPermissionActivity
  extends AppCompatActivity
  implements g
{
  e a;
  l b;
  
  public static Intent a(Context paramContext, String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, SmsPermissionActivity.class);
    localIntent.putExtra("AppUserInteraction.Context", paramString);
    localIntent.putExtra("success_intent", null);
    return localIntent;
  }
  
  public final void a()
  {
    String[] arrayOfString = b.d();
    int i = 0;
    int j = 0;
    String str1 = null;
    int m;
    for (;;)
    {
      int k = 3;
      if (j >= k) {
        break;
      }
      String str2 = arrayOfString[j];
      m = i.a(this, str2);
      if (m != 0) {
        return;
      }
      j += 1;
    }
    while (i < m)
    {
      str1 = arrayOfString[i];
      boolean bool = i.a(this, str1);
      if (bool)
      {
        i.a(this);
        return;
      }
      i += 1;
    }
    i = 1;
    android.support.v4.app.a.a(this, arrayOfString, i);
  }
  
  public final void a(String paramString)
  {
    TruecallerInit.b(this, "messages", paramString);
  }
  
  public final Intent b()
  {
    return (Intent)getIntent().getParcelableExtra("success_intent");
  }
  
  public final void b(String paramString)
  {
    paramString = DefaultSmsActivity.a(this, paramString);
    startActivity(paramString);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    int i = aresId;
    setTheme(i);
    super.onCreate(paramBundle);
    setContentView(2131559226);
    paramBundle = getIntent().getStringExtra("AppUserInteraction.Context");
    Object localObject1 = { "Setting context should not be null. Use SmsPermissionActivity.createIntent()." };
    AssertionUtil.isNotNull(paramBundle, (String[])localObject1);
    localObject1 = new com/truecaller/messaging/smspermission/a$a;
    ((a.a)localObject1).<init>((byte)0);
    Object localObject2 = (bp)dagger.a.g.a(((bk)getApplicationContext()).a());
    b = ((bp)localObject2);
    localObject2 = new com/truecaller/messaging/smspermission/c;
    ((c)localObject2).<init>(paramBundle);
    paramBundle = (c)dagger.a.g.a(localObject2);
    a = paramBundle;
    dagger.a.g.a(a, c.class);
    dagger.a.g.a(b, bp.class);
    paramBundle = new com/truecaller/messaging/smspermission/a;
    localObject2 = a;
    localObject1 = b;
    paramBundle.<init>((c)localObject2, (bp)localObject1, (byte)0);
    paramBundle.a(this);
    a.a(this);
    paramBundle = findViewById(2131364541);
    localObject1 = new com/truecaller/messaging/smspermission/-$$Lambda$SmsPermissionActivity$O268MbwlMsK4ygnG82ewg05fveE;
    ((-..Lambda.SmsPermissionActivity.O268MbwlMsK4ygnG82ewg05fveE)localObject1).<init>(this);
    paramBundle.setOnClickListener((View.OnClickListener)localObject1);
  }
  
  public void onDestroy()
  {
    a.y_();
    super.onDestroy();
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    i.a(paramArrayOfString, paramArrayOfInt);
  }
  
  public void onResume()
  {
    super.onResume();
    a.a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.smspermission.SmsPermissionActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */