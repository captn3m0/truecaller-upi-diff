package com.truecaller.messaging.smspermission;

import android.content.Intent;

abstract interface g
{
  public abstract void a();
  
  public abstract void a(String paramString);
  
  public abstract Intent b();
  
  public abstract void b(String paramString);
  
  public abstract void finish();
  
  public abstract void startActivity(Intent paramIntent);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.smspermission.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */