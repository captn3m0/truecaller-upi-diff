package com.truecaller.messaging.b;

import android.content.Context;
import com.truecaller.ads.CustomTemplate;
import com.truecaller.ads.k.b;
import com.truecaller.ads.k.c;
import com.truecaller.ads.k.d;
import com.truecaller.ads.provider.e;
import com.truecaller.ads.provider.l;
import com.truecaller.androidactors.i;
import com.truecaller.messaging.conversation.bs;
import com.truecaller.messaging.conversation.bt;
import com.truecaller.multisim.ae;
import com.truecaller.multisim.h;
import com.truecaller.utils.o;

public final class k
{
  public static final k.a d;
  final Context a;
  final int b;
  boolean c;
  
  static
  {
    k.a locala = new com/truecaller/messaging/b/k$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  public k(Context paramContext, int paramInt, boolean paramBoolean)
  {
    a = paramContext;
    b = paramInt;
    c = paramBoolean;
  }
  
  public static e a(com.truecaller.ads.provider.f paramf, c.d.f paramf1)
  {
    c.g.b.k.b(paramf, "provider");
    c.g.b.k.b(paramf1, "corutineContext");
    l locall = new com/truecaller/ads/provider/l;
    Object localObject = com.truecaller.ads.k.n;
    localObject = k.d.a().a("/43067329/A*Inbox_1*Native*GPS", "/43067329/A*Inbox_1*Unified*GPS").b("INBOX").c("inbox").b().c();
    CustomTemplate[] arrayOfCustomTemplate = new CustomTemplate[1];
    CustomTemplate localCustomTemplate = CustomTemplate.NATIVE_CONTENT_DUAL_TRACKER;
    arrayOfCustomTemplate[0] = localCustomTemplate;
    localObject = ((k.b)localObject).a(arrayOfCustomTemplate).e();
    locall.<init>(paramf, (com.truecaller.ads.k)localObject, paramf1);
    return (e)locall;
  }
  
  public static i a(com.truecaller.androidactors.k paramk)
  {
    c.g.b.k.b(paramk, "actorsThreads");
    paramk = paramk.a();
    c.g.b.k.a(paramk, "actorsThreads.ui()");
    return paramk;
  }
  
  public static bs a(o paramo, h paramh, ae paramae)
  {
    c.g.b.k.b(paramo, "resourceProvider");
    c.g.b.k.b(paramh, "multiSimManager");
    c.g.b.k.b(paramae, "simInfoCache");
    bt localbt = new com/truecaller/messaging/conversation/bt;
    boolean bool = paramh.j();
    localbt.<init>(paramo, bool, paramae);
    return (bs)localbt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.b.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */