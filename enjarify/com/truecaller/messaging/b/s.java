package com.truecaller.messaging.b;

import android.content.BroadcastReceiver;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.Looper;
import android.util.SparseBooleanArray;
import c.g.b.k;
import c.u;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.i;
import com.truecaller.androidactors.w;
import com.truecaller.bc;
import com.truecaller.bd;
import com.truecaller.common.h.am;
import com.truecaller.content.TruecallerContract.Filters.EntityType;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.data.o;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.e.l.b;
import com.truecaller.messaging.transport.im.bp;
import com.truecaller.util.aa;
import com.truecaller.utils.d;
import com.truecaller.utils.l;
import com.truecaller.utils.n;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public final class s
  extends bc
  implements r.a, com.truecaller.network.search.e.a
{
  private final com.truecaller.androidactors.f A;
  private final com.truecaller.ads.provider.e B;
  private final com.truecaller.androidactors.f C;
  private final n D;
  private final com.truecaller.androidactors.f E;
  private final com.truecaller.analytics.b F;
  private final com.truecaller.androidactors.f G;
  private final com.truecaller.androidactors.f H;
  private final com.truecaller.util.d.a I;
  private final l J;
  private final com.truecaller.androidactors.f K;
  private final bw L;
  private final com.truecaller.i.a M;
  private final v N;
  private final com.truecaller.messaging.data.al O;
  private final boolean P;
  private final com.truecaller.androidactors.f Q;
  private com.truecaller.messaging.data.a.a d;
  private boolean e;
  private boolean f;
  private com.truecaller.androidactors.a g;
  private com.truecaller.androidactors.a h;
  private com.truecaller.androidactors.a i;
  private com.truecaller.androidactors.a j;
  private final Map k;
  private boolean l;
  private boolean m;
  private Conversation[] n;
  private l.b o;
  private final s.i p;
  private final s.b q;
  private final s.a r;
  private final int s;
  private final c.d.f t;
  private final com.truecaller.util.al u;
  private final d v;
  private final com.truecaller.multisim.h w;
  private final com.truecaller.messaging.h x;
  private final com.truecaller.network.search.e y;
  private final i z;
  
  public s(int paramInt, c.d.f paramf, com.truecaller.util.al paramal, d paramd, com.truecaller.multisim.h paramh, com.truecaller.messaging.h paramh1, com.truecaller.network.search.e parame, i parami, com.truecaller.androidactors.f paramf1, com.truecaller.ads.provider.e parame1, com.truecaller.androidactors.f paramf2, n paramn, com.truecaller.androidactors.f paramf3, com.truecaller.analytics.b paramb, com.truecaller.androidactors.f paramf4, com.truecaller.androidactors.f paramf5, com.truecaller.util.d.a parama, l paraml, com.truecaller.androidactors.f paramf6, bw parambw, com.truecaller.i.a parama1, v paramv, com.truecaller.messaging.data.al paramal1, boolean paramBoolean, com.truecaller.androidactors.f paramf7)
  {
    super(paramf);
    s = paramInt;
    t = paramf;
    u = paramal;
    v = paramd;
    w = paramh;
    x = paramh1;
    y = parame;
    z = parami;
    A = paramf1;
    B = parame1;
    C = paramf2;
    D = paramn;
    E = paramf3;
    F = paramb;
    G = paramf4;
    localObject1 = paramf5;
    H = paramf5;
    localObject1 = parama;
    I = parama;
    localObject1 = paraml;
    localObject2 = paramf6;
    J = paraml;
    K = paramf6;
    localObject1 = parambw;
    localObject2 = parama1;
    L = parambw;
    M = parama1;
    localObject1 = paramv;
    localObject2 = paramal1;
    N = paramv;
    O = paramal1;
    P = paramBoolean;
    localObject1 = paramf7;
    Q = paramf7;
    localObject1 = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject1).<init>();
    localObject1 = (Map)localObject1;
    k = ((Map)localObject1);
    localObject1 = new Conversation[0];
    n = ((Conversation[])localObject1);
    localObject1 = new com/truecaller/messaging/b/s$i;
    ((s.i)localObject1).<init>(this);
    p = ((s.i)localObject1);
    localObject1 = new com/truecaller/messaging/b/s$b;
    localObject2 = new android/os/Handler;
    localObject3 = Looper.getMainLooper();
    ((Handler)localObject2).<init>((Looper)localObject3);
    ((s.b)localObject1).<init>(this, (Handler)localObject2);
    q = ((s.b)localObject1);
    localObject1 = new com/truecaller/messaging/b/s$a;
    ((s.a)localObject1).<init>(this);
    r = ((s.a)localObject1);
  }
  
  private final void A()
  {
    boolean bool = f;
    if (!bool)
    {
      com.truecaller.messaging.data.a.a locala = d;
      if (locala != null)
      {
        ContentObserver localContentObserver = (ContentObserver)q;
        locala.registerContentObserver(localContentObserver);
      }
      bool = true;
      f = bool;
    }
  }
  
  private final void B()
  {
    boolean bool = f;
    if (bool)
    {
      com.truecaller.messaging.data.a.a locala = d;
      if (locala != null)
      {
        ContentObserver localContentObserver = (ContentObserver)q;
        locala.unregisterContentObserver(localContentObserver);
      }
      bool = false;
      locala = null;
      f = false;
    }
  }
  
  private final void C()
  {
    Object localObject1 = L;
    boolean bool1 = ((bw)localObject1).a();
    if (!bool1) {
      return;
    }
    localObject1 = (r.c)b;
    if (localObject1 == null) {
      return;
    }
    com.truecaller.messaging.data.a.a locala = d;
    if (locala == null) {
      return;
    }
    localObject1 = (Iterable)((r.c)localObject1).m();
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (Collection)localObject2;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      int i1 = ((Number)((Iterator)localObject1).next()).intValue();
      locala.moveToPosition(i1);
      Object localObject3 = locala.c();
      int i2 = ((List)localObject3).size();
      int i3 = 1;
      if (i2 == i3)
      {
        Participant localParticipant = (Participant)c.a.m.d((List)localObject3);
        i2 = c;
        if (i2 == 0)
        {
          localObject3 = df;
          break label184;
        }
      }
      i1 = 0;
      localObject3 = null;
      label184:
      if (localObject3 != null) {
        ((Collection)localObject2).add(localObject3);
      }
    }
    localObject2 = (List)localObject2;
    bool1 = ((List)localObject2).isEmpty();
    if (bool1) {
      return;
    }
    localObject1 = (bp)K.a();
    localObject2 = (Collection)localObject2;
    ((bp)localObject1).a((Collection)localObject2);
  }
  
  private final void D()
  {
    r.c localc = (r.c)b;
    if (localc != null)
    {
      Set localSet = B.b();
      localc.a(localSet);
      return;
    }
  }
  
  private final void E()
  {
    x.O();
    I();
  }
  
  private final void F()
  {
    x.i(false);
    com.truecaller.messaging.h localh = x;
    long l1 = System.currentTimeMillis();
    localh.h(l1);
    I();
  }
  
  private final boolean G()
  {
    l locall = J;
    boolean bool = locall.b();
    if (bool)
    {
      locall = J;
      bool = locall.c();
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  private final void H()
  {
    r.c localc = (r.c)b;
    if (localc != null)
    {
      String str = "android.permission.READ_EXTERNAL_STORAGE";
      boolean bool = localc.d(str);
      if (!bool)
      {
        str = "android.permission.WRITE_EXTERNAL_STORAGE";
        bool = localc.d(str);
        if (!bool)
        {
          localc.o();
          return;
        }
      }
      localc.n();
      return;
    }
  }
  
  private final void I()
  {
    Object localObject = new com/truecaller/messaging/b/s$h;
    ((s.h)localObject).<init>(this, null);
    localObject = (c.g.a.m)localObject;
    kotlinx.coroutines.e.b(this, null, (c.g.a.m)localObject, 3);
  }
  
  private final void a(String paramString)
  {
    com.truecaller.analytics.b localb = F;
    com.truecaller.analytics.e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("ViewAction");
    paramString = locala.a("Action", paramString).a("Context", "inbox").a();
    k.a(paramString, "AnalyticsEvent.Builder(A…\n                .build()");
    localb.a(paramString);
  }
  
  private static boolean a(SparseBooleanArray paramSparseBooleanArray)
  {
    int i1 = paramSparseBooleanArray.indexOfKey(0);
    int i2 = 1;
    if (i1 >= 0)
    {
      bool1 = paramSparseBooleanArray.get(0);
      if (!bool1)
      {
        bool1 = true;
        break label29;
      }
    }
    boolean bool1 = false;
    label29:
    int i3 = paramSparseBooleanArray.indexOfKey(i2);
    if (i3 >= 0)
    {
      bool2 = paramSparseBooleanArray.get(i2);
      if (!bool2)
      {
        bool2 = true;
        break label62;
      }
    }
    boolean bool2 = false;
    paramSparseBooleanArray = null;
    label62:
    if ((!bool1) && (!bool2)) {
      return false;
    }
    return i2;
  }
  
  private final void b(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      boolean bool = G();
      if (!bool)
      {
        H();
        return;
      }
    }
    Object localObject1 = n;
    int i1 = localObject1.length;
    int i2 = 1;
    if (i1 == 0)
    {
      i1 = 1;
    }
    else
    {
      i1 = 0;
      localObject1 = null;
    }
    i1 ^= i2;
    if (i1 != 0)
    {
      localObject1 = (r.c)b;
      if (localObject1 != null)
      {
        i2 = 2131886493;
        ((r.c)localObject1).b(i2);
      }
      localObject1 = h;
      if (localObject1 != null) {
        ((com.truecaller.androidactors.a)localObject1).a();
      }
      localObject1 = (com.truecaller.messaging.data.t)C.a();
      Object localObject2 = n;
      Object localObject3 = ((com.truecaller.messaging.data.t)localObject1).a((Conversation[])localObject2, paramBoolean);
      localObject1 = z;
      localObject2 = new com/truecaller/messaging/b/s$c;
      Object localObject4 = this;
      localObject4 = (s)this;
      ((s.c)localObject2).<init>((s)localObject4);
      localObject2 = (c.g.a.b)localObject2;
      localObject4 = new com/truecaller/messaging/b/t;
      ((t)localObject4).<init>((c.g.a.b)localObject2);
      localObject4 = (ac)localObject4;
      localObject3 = ((w)localObject3).a((i)localObject1, (ac)localObject4);
      h = ((com.truecaller.androidactors.a)localObject3);
    }
  }
  
  private static boolean b(Conversation paramConversation, int paramInt)
  {
    paramConversation = x;
    if (paramConversation == null) {
      return false;
    }
    boolean bool = com.truecaller.messaging.i.a.a(paramConversation);
    if (!bool)
    {
      bool = com.truecaller.messaging.i.a.b(paramConversation);
      if (!bool)
      {
        int i1 = h;
        if (i1 != paramInt) {
          return true;
        }
      }
    }
    return false;
  }
  
  private final void c(int paramInt)
  {
    Object localObject1 = (Iterable)k.values();
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (Collection)localObject2;
    localObject1 = ((Iterable)localObject1).iterator();
    Object localObject3;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = nextx;
      if (localObject3 != null)
      {
        localObject3 = a;
      }
      else
      {
        bool1 = false;
        localObject3 = null;
      }
      if (localObject3 != null) {
        ((Collection)localObject2).add(localObject3);
      }
    }
    localObject2 = (Iterable)localObject2;
    localObject1 = ((Iterable)localObject2).iterator();
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject1).hasNext();
      if (!bool2) {
        break;
      }
      localObject2 = (String)((Iterator)localObject1).next();
      localObject3 = (com.truecaller.messaging.transport.im.a.a)Q.a();
      localObject2 = ((com.truecaller.messaging.transport.im.a.a)localObject3).a((String)localObject2, paramInt);
      ((w)localObject2).c();
    }
    r.c localc = (r.c)b;
    if (localc != null)
    {
      localc.b();
      return;
    }
  }
  
  private final void z()
  {
    Object localObject1 = g;
    if (localObject1 != null) {
      ((com.truecaller.androidactors.a)localObject1).a();
    }
    localObject1 = (o)A.a();
    int i1 = s;
    localObject1 = ((o)localObject1).a(i1);
    i locali = z;
    Object localObject2 = new com/truecaller/messaging/b/s$d;
    ((s.d)localObject2).<init>(this);
    localObject2 = (ac)localObject2;
    localObject1 = ((w)localObject1).a(locali, (ac)localObject2);
    g = ((com.truecaller.androidactors.a)localObject1);
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    int i1 = 10;
    if (paramInt1 == i1)
    {
      paramInt1 = -1;
      if (paramInt2 == paramInt1)
      {
        Conversation[] arrayOfConversation = n;
        paramInt1 = arrayOfConversation.length;
        paramInt2 = 1;
        i1 = 0;
        if (paramInt1 == 0)
        {
          paramInt1 = 1;
        }
        else
        {
          paramInt1 = 0;
          arrayOfConversation = null;
        }
        paramInt1 ^= paramInt2;
        if (paramInt1 != 0)
        {
          b(false);
          A();
        }
      }
    }
  }
  
  public final void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    Object localObject = "grantResults";
    k.b(paramArrayOfInt, (String)localObject);
    if (paramInt == 0)
    {
      l locall = J;
      localObject = new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" };
      paramInt = locall.a(paramArrayOfString, paramArrayOfInt, (String[])localObject);
      if (paramInt != 0)
      {
        paramInt = 1;
        b(paramInt);
      }
    }
  }
  
  public final void a(long paramLong, int paramInt)
  {
    r.b localb = (r.b)c;
    if (localb != null)
    {
      localb.a(paramLong, paramInt);
      return;
    }
  }
  
  public final void a(long paramLong, String paramString1, String paramString2, String paramString3, String paramString4)
  {
    k.b(paramString1, "normalizedNumber");
    Object localObject1 = c;
    Object localObject2 = localObject1;
    localObject2 = (r.b)localObject1;
    if (localObject2 != null)
    {
      ((r.b)localObject2).a(paramLong, paramString1, paramString2, paramString3, paramString4);
      return;
    }
  }
  
  public final void a(Conversation paramConversation, int paramInt)
  {
    k.b(paramConversation, "conversation");
    r.b localb = (r.b)c;
    if (localb != null)
    {
      localb.a(paramConversation, paramInt);
      return;
    }
  }
  
  public final void a(ImGroupInfo paramImGroupInfo)
  {
    k.b(paramImGroupInfo, "imGroupInfo");
    r.b localb = (r.b)c;
    if (localb != null)
    {
      localb.a(paramImGroupInfo);
      return;
    }
  }
  
  public final void a(String paramString, TruecallerContract.Filters.EntityType paramEntityType)
  {
    k.b(paramEntityType, "entityType");
    Object localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>();
    localObject1 = (List)localObject1;
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    Object localObject3 = localObject2;
    localObject3 = (List)localObject2;
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    localObject2 = (List)localObject2;
    Object localObject4 = k.values().iterator();
    boolean bool1 = false;
    Object localObject5 = null;
    int i1 = 0;
    Object localObject6 = null;
    int i2;
    for (;;)
    {
      bool1 = ((Iterator)localObject4).hasNext();
      i2 = 1;
      if (!bool1) {
        break;
      }
      localObject5 = nextl;
      int i3 = localObject5.length;
      i4 = 0;
      while (i4 < i3)
      {
        localObject7 = localObject5[i4];
        Object localObject8 = f;
        Object localObject9 = "participant.normalizedAddress";
        k.a(localObject8, (String)localObject9);
        ((List)localObject1).add(localObject8);
        int i5 = c;
        if (i5 == 0) {
          localObject8 = "PHONE_NUMBER";
        } else {
          localObject8 = "OTHER";
        }
        ((List)localObject3).add(localObject8);
        k.a(localObject7, "participant");
        localObject8 = ((Participant)localObject7).a();
        localObject9 = "participant.displayName";
        k.a(localObject8, (String)localObject9);
        ((List)localObject2).add(localObject8);
        if (localObject6 == null)
        {
          if (paramString != null)
          {
            localObject6 = paramString;
            localObject6 = (CharSequence)paramString;
            i1 = ((CharSequence)localObject6).length();
            if (i1 > 0)
            {
              i1 = 1;
            }
            else
            {
              i1 = 0;
              localObject6 = null;
            }
            if (i1 != 0)
            {
              localObject6 = (aa)G.a();
              long l1 = i;
              localObject6 = ((aa)localObject6).a(l1);
              localObject8 = z;
              localObject9 = new com/truecaller/messaging/b/s$e;
              ((s.e)localObject9).<init>(this, paramString, paramEntityType);
              localObject9 = (ac)localObject9;
              ((w)localObject6).a((i)localObject8, (ac)localObject9);
            }
          }
          localObject6 = localObject7;
        }
        i4 += 1;
      }
    }
    localObject4 = (com.truecaller.filters.s)E.a();
    if (paramString != null)
    {
      int i6 = ((List)localObject2).size();
      localObject2 = Collections.nCopies(i6, paramString);
    }
    localObject5 = localObject2;
    String str = "inbox";
    int i4 = 0;
    localObject2 = localObject4;
    localObject4 = localObject1;
    Object localObject7 = paramEntityType;
    paramEntityType = ((com.truecaller.filters.s)localObject2).a((List)localObject1, (List)localObject3, (List)localObject5, str, false, paramEntityType);
    paramEntityType.c();
    if (localObject6 != null)
    {
      paramEntityType = paramString;
      paramEntityType = (CharSequence)paramString;
      boolean bool2 = am.b(paramEntityType);
      if (bool2) {
        paramString = ((Participant)localObject6).a();
      }
      paramEntityType = ((Participant)localObject6).b();
      k.a(paramEntityType, "firstParticipant.presentableAddress");
      localObject2 = D;
      int i7 = 2131755028;
      int i8 = ((List)localObject1).size();
      localObject5 = new Object[i2];
      int i9 = ((List)localObject1).size();
      localObject1 = Integer.valueOf(i9);
      localObject5[0] = localObject1;
      localObject1 = ((n)localObject2).a(i7, i8, (Object[])localObject5);
      k.a(localObject1, "resourceProvider.getQuan…ses.size, addresses.size)");
      localObject2 = (r.c)b;
      if (localObject2 != null) {
        ((r.c)localObject2).a(paramString, paramEntityType, (String)localObject1);
      }
    }
    paramString = (r.c)b;
    if (paramString != null)
    {
      paramString.b();
      return;
    }
  }
  
  public final void a(Collection paramCollection)
  {
    k.b(paramCollection, "normalizedNumbers");
    z();
  }
  
  public final void a(Set paramSet)
  {
    String str = "normalizedNumbers";
    k.b(paramSet, str);
    paramSet = (r.c)b;
    if (paramSet != null)
    {
      paramSet.h();
      return;
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    m = paramBoolean;
    com.truecaller.ads.provider.e locale1 = B;
    boolean bool = paramBoolean ^ true;
    locale1.a(bool);
    if (paramBoolean)
    {
      com.truecaller.ads.provider.e locale2 = B;
      locale2.f();
    }
  }
  
  public final void a(boolean paramBoolean1, boolean paramBoolean2)
  {
    b(paramBoolean2);
    if (paramBoolean1)
    {
      if (paramBoolean2) {
        localObject1 = "clearStorage";
      } else {
        localObject1 = "keepStorage";
      }
      com.truecaller.analytics.b localb = F;
      Object localObject2 = new com/truecaller/analytics/e$a;
      ((com.truecaller.analytics.e.a)localObject2).<init>("ViewAction");
      String str1 = "deleteMedia";
      Object localObject1 = ((com.truecaller.analytics.e.a)localObject2).a("Action", str1).a("SubAction", (String)localObject1);
      String str2 = "inbox";
      localObject1 = ((com.truecaller.analytics.e.a)localObject1).a("Context", str2).a();
      localObject2 = "AnalyticsEvent.Builder(A…\n                .build()";
      k.a(localObject1, (String)localObject2);
      localb.a((com.truecaller.analytics.e)localObject1);
    }
  }
  
  public final boolean a()
  {
    return l;
  }
  
  public final boolean a(int paramInt)
  {
    int i1 = 1;
    int i2 = 0;
    Object localObject1 = null;
    Object localObject2;
    Object localObject3;
    Object localObject4;
    Object localObject5;
    int i3;
    switch (paramInt)
    {
    default: 
      break;
    case 2131361942: 
      c(0);
      break;
    case 2131361934: 
      localObject2 = d;
      if (localObject2 != null)
      {
        i2 = ((com.truecaller.messaging.data.a.a)localObject2).moveToFirst();
        if (i2 != 0) {
          do
          {
            localObject1 = k;
            long l1 = ((com.truecaller.messaging.data.a.a)localObject2).a();
            localObject3 = Long.valueOf(l1);
            localObject4 = ((com.truecaller.messaging.data.a.a)localObject2).b();
            ((Map)localObject1).put(localObject3, localObject4);
            i2 = ((com.truecaller.messaging.data.a.a)localObject2).moveToNext();
          } while (i2 != 0);
        }
        localObject2 = (r.c)b;
        if (localObject2 != null) {
          ((r.c)localObject2).h();
        }
        localObject2 = (r.c)b;
        if (localObject2 != null) {
          ((r.c)localObject2).j();
        }
      }
      break;
    case 2131361915: 
      c(i1);
      break;
    case 2131361906: 
      localObject2 = c.a.m.c((Collection)k.keySet());
      paramInt = localObject2.length;
      if (paramInt == 0)
      {
        paramInt = 1;
      }
      else
      {
        paramInt = 0;
        localObject2 = null;
      }
      paramInt ^= i1;
      if (paramInt != 0)
      {
        localObject2 = (r.c)b;
        if (localObject2 != null)
        {
          int i4 = 2131886652;
          ((r.c)localObject2).b(i4);
        }
        localObject2 = i;
        if (localObject2 != null) {
          ((com.truecaller.androidactors.a)localObject2).a();
        }
        localObject2 = (com.truecaller.messaging.data.t)C.a();
        localObject3 = k.values();
        if (localObject3 != null)
        {
          localObject1 = new Conversation[0];
          localObject1 = ((Collection)localObject3).toArray((Object[])localObject1);
          if (localObject1 != null)
          {
            localObject1 = (Conversation[])localObject1;
            localObject2 = ((com.truecaller.messaging.data.t)localObject2).a((Conversation[])localObject1);
            localObject1 = z;
            localObject3 = new com/truecaller/messaging/b/s$g;
            localObject4 = this;
            localObject4 = (s)this;
            ((s.g)localObject3).<init>((s)localObject4);
            localObject3 = (c.g.a.b)localObject3;
            localObject4 = new com/truecaller/messaging/b/t;
            ((t)localObject4).<init>((c.g.a.b)localObject3);
            localObject4 = (ac)localObject4;
            localObject2 = ((w)localObject2).a((i)localObject1, (ac)localObject4);
            i = ((com.truecaller.androidactors.a)localObject2);
          }
          else
          {
            localObject2 = new c/u;
            ((u)localObject2).<init>("null cannot be cast to non-null type kotlin.Array<T>");
            throw ((Throwable)localObject2);
          }
        }
        else
        {
          localObject2 = new c/u;
          ((u)localObject2).<init>("null cannot be cast to non-null type java.util.Collection<T>");
          throw ((Throwable)localObject2);
        }
      }
      break;
    case 2131361857: 
      localObject2 = k;
      paramInt = ((Map)localObject2).isEmpty();
      if (paramInt == 0)
      {
        localObject2 = k.values();
        if (localObject2 != null)
        {
          localObject3 = new Conversation[0];
          localObject2 = ((Collection)localObject2).toArray((Object[])localObject3);
          if (localObject2 != null)
          {
            localObject2 = (Conversation[])localObject2;
            n = ((Conversation[])localObject2);
            localObject2 = j;
            if (localObject2 != null) {
              ((com.truecaller.androidactors.a)localObject2).a();
            }
            localObject2 = (o)A.a();
            localObject3 = n;
            localObject4 = new java/util/ArrayList;
            int i5 = localObject3.length;
            ((ArrayList)localObject4).<init>(i5);
            localObject4 = (Collection)localObject4;
            i5 = localObject3.length;
            while (i2 < i5)
            {
              long l2 = a;
              localObject5 = Long.valueOf(l2);
              ((Collection)localObject4).add(localObject5);
              i2 += 1;
            }
            localObject4 = (List)localObject4;
            localObject2 = ((o)localObject2).a((List)localObject4);
            localObject1 = z;
            localObject3 = new com/truecaller/messaging/b/s$f;
            ((s.f)localObject3).<init>(this);
            localObject3 = (ac)localObject3;
            localObject2 = ((w)localObject2).a((i)localObject1, (ac)localObject3);
            j = ((com.truecaller.androidactors.a)localObject2);
          }
          else
          {
            localObject2 = new c/u;
            ((u)localObject2).<init>("null cannot be cast to non-null type kotlin.Array<T>");
            throw ((Throwable)localObject2);
          }
        }
        else
        {
          localObject2 = new c/u;
          ((u)localObject2).<init>("null cannot be cast to non-null type java.util.Collection<T>");
          throw ((Throwable)localObject2);
        }
      }
      break;
    case 2131361835: 
      localObject2 = (r.c)b;
      if (localObject2 != null)
      {
        localObject3 = k;
        boolean bool1 = ((Map)localObject3).isEmpty();
        if (!bool1)
        {
          localObject3 = ((Iterable)k.values()).iterator();
          int i7 = 0;
          localObject4 = null;
          int i6;
          for (;;)
          {
            boolean bool2 = ((Iterator)localObject3).hasNext();
            if (!bool2) {
              break;
            }
            Participant[] arrayOfParticipant = nextl;
            i6 = arrayOfParticipant.length;
            i7 += i6;
          }
          int i8;
          if (i7 == i1)
          {
            localObject3 = (Conversation)k.values().iterator().next();
            localObject4 = u;
            i8 = ((com.truecaller.util.al)localObject4).a();
            if (i8 != 0)
            {
              localObject4 = l[0];
              i8 = ((Participant)localObject4).i();
              if (i8 != 0) {
                i3 = 1;
              }
            }
            localObject3 = ((Conversation)localObject3).b();
            localObject4 = "conversation.title";
            k.a(localObject3, (String)localObject4);
            ((r.c)localObject2).a((String)localObject3, i3);
          }
          else
          {
            localObject3 = D;
            i6 = 2131755008;
            localObject5 = new Object[i1];
            Integer localInteger = Integer.valueOf(i8);
            localObject5[0] = localInteger;
            localObject1 = ((n)localObject3).a(i6, i8, (Object[])localObject5);
            localObject3 = "resourceProvider.getQuan…dressCount, addressCount)";
            k.a(localObject1, (String)localObject3);
            ((r.c)localObject2).b((String)localObject1);
          }
        }
      }
      break;
    }
    return i1;
  }
  
  public final com.truecaller.messaging.data.a.a b()
  {
    return d;
  }
  
  public final void b(ImGroupInfo paramImGroupInfo)
  {
    k.b(paramImGroupInfo, "groupInfo");
    r.b localb = (r.b)c;
    if (localb != null)
    {
      localb.b(paramImGroupInfo);
      return;
    }
  }
  
  public final boolean b(int paramInt)
  {
    int i1 = 4;
    int i4 = 1;
    Object localObject1;
    Object localObject2;
    boolean bool2;
    int i3;
    Object localObject3;
    Object localObject4;
    boolean bool3;
    switch (paramInt)
    {
    default: 
      break;
    case 2131361942: 
      localObject1 = (Iterable)k.values();
      boolean bool1 = localObject1 instanceof Collection;
      if (bool1)
      {
        localObject2 = localObject1;
        localObject2 = (Collection)localObject1;
        bool1 = ((Collection)localObject2).isEmpty();
        if (bool1) {
          break;
        }
      }
      else
      {
        localObject1 = ((Iterable)localObject1).iterator();
        do
        {
          bool1 = ((Iterator)localObject1).hasNext();
          if (!bool1) {
            break;
          }
          localObject2 = (Conversation)((Iterator)localObject1).next();
          bool1 = b((Conversation)localObject2, 0);
        } while (bool1);
        return false;
        return i4;
      }
      break;
    case 2131361934: 
      localObject1 = d;
      if (localObject1 != null)
      {
        paramInt = ((com.truecaller.messaging.data.a.a)localObject1).getCount();
        localObject2 = k;
        int i2 = ((Map)localObject2).size();
        if (paramInt == i2) {
          return false;
        }
      }
      return i4;
    case 2131361915: 
      localObject1 = (Iterable)k.values();
      bool2 = localObject1 instanceof Collection;
      if (bool2)
      {
        localObject2 = localObject1;
        localObject2 = (Collection)localObject1;
        bool2 = ((Collection)localObject2).isEmpty();
        if (bool2) {
          break;
        }
      }
      else
      {
        localObject1 = ((Iterable)localObject1).iterator();
        do
        {
          bool2 = ((Iterator)localObject1).hasNext();
          if (!bool2) {
            break;
          }
          localObject2 = (Conversation)((Iterator)localObject1).next();
          bool2 = b((Conversation)localObject2, i4);
        } while (bool2);
        return false;
        return i4;
      }
      break;
    case 2131361906: 
      localObject1 = (Iterable)k.values();
      bool2 = localObject1 instanceof Collection;
      if (bool2)
      {
        localObject2 = localObject1;
        localObject2 = (Collection)localObject1;
        bool2 = ((Collection)localObject2).isEmpty();
        if (bool2) {
          return false;
        }
      }
      localObject1 = ((Iterable)localObject1).iterator();
      do
      {
        bool2 = ((Iterator)localObject1).hasNext();
        if (!bool2) {
          break;
        }
        localObject2 = (Conversation)((Iterator)localObject1).next();
        i3 = k;
        if (i3 > 0)
        {
          i3 = 1;
        }
        else
        {
          i3 = 0;
          localObject2 = null;
        }
      } while (i3 == 0);
      break;
      return false;
    case 2131361857: 
      localObject1 = (Iterable)k.values();
      int i5 = localObject1 instanceof Collection;
      if (i5 != 0)
      {
        localObject3 = localObject1;
        localObject3 = (Collection)localObject1;
        i5 = ((Collection)localObject3).isEmpty();
        if (i5 != 0) {
          break;
        }
      }
      else
      {
        localObject1 = ((Iterable)localObject1).iterator();
        do
        {
          i5 = ((Iterator)localObject1).hasNext();
          if (i5 == 0) {
            break;
          }
          localObject3 = (Conversation)((Iterator)localObject1).next();
          localObject4 = l;
          String str = "participants";
          k.a(localObject4, str);
          localObject4 = (Participant)c.a.f.c((Object[])localObject4);
          if (localObject4 != null)
          {
            int i7 = c;
            if (i7 == i3)
            {
              localObject3 = x;
              if (localObject3 != null)
              {
                i5 = com.truecaller.messaging.i.a.a((ImGroupInfo)localObject3);
                if (i5 == i4) {}
              }
              else
              {
                bool3 = false;
                localObject3 = null;
                continue;
              }
            }
          }
          bool3 = true;
        } while (bool3);
        return false;
        return i4;
      }
      break;
    case 2131361835: 
      localObject1 = (Iterable)k.values();
      bool3 = localObject1 instanceof Collection;
      if (bool3)
      {
        localObject3 = localObject1;
        localObject3 = (Collection)localObject1;
        bool3 = ((Collection)localObject3).isEmpty();
        if (bool3) {
          break;
        }
      }
      else
      {
        localObject1 = ((Iterable)localObject1).iterator();
        int i6;
        do
        {
          bool3 = ((Iterator)localObject1).hasNext();
          if (!bool3) {
            break;
          }
          localObject3 = nextl;
          localObject4 = "participants";
          k.a(localObject3, (String)localObject4);
          localObject3 = (Participant)c.a.f.c((Object[])localObject3);
          if (localObject3 != null)
          {
            i6 = c;
            if (i6 == i3)
            {
              i6 = 0;
              localObject3 = null;
              continue;
            }
          }
          i6 = 1;
        } while (i6 != 0);
        return false;
        return i4;
      }
      break;
    }
    return i4;
  }
  
  public final boolean b(Conversation paramConversation)
  {
    k.b(paramConversation, "conversation");
    Map localMap = k;
    paramConversation = Long.valueOf(a);
    return localMap.containsKey(paramConversation);
  }
  
  public final int c()
  {
    return s;
  }
  
  public final boolean e()
  {
    r.c localc = (r.c)b;
    boolean bool = true;
    if (localc != null)
    {
      localc.g();
      localc.b(bool);
    }
    return bool;
  }
  
  public final void f()
  {
    r.c localc = (r.c)b;
    if (localc == null) {
      return;
    }
    k.clear();
    localc.b(false);
    localc.h();
  }
  
  public final String g()
  {
    n localn = D;
    int i1 = 2;
    Object[] arrayOfObject = new Object[i1];
    int i2 = k.size();
    Object localObject = Integer.valueOf(i2);
    int i3 = 0;
    arrayOfObject[0] = localObject;
    localObject = d;
    if (localObject != null) {
      i3 = ((com.truecaller.messaging.data.a.a)localObject).getCount();
    }
    localObject = Integer.valueOf(i3);
    arrayOfObject[1] = localObject;
    return localn.a(2131886292, arrayOfObject);
  }
  
  public final void h()
  {
    Object localObject1 = y;
    Object localObject2 = this;
    localObject2 = (com.truecaller.network.search.e.a)this;
    ((com.truecaller.network.search.e)localObject1).a((com.truecaller.network.search.e.a)localObject2);
    localObject1 = u;
    localObject2 = (BroadcastReceiver)p;
    String[] arrayOfString = { "com.truecaller.messaging.spam.SEARCH_COMPLETED" };
    ((com.truecaller.util.al)localObject1).a((BroadcastReceiver)localObject2, arrayOfString);
    boolean bool = w.j();
    l = bool;
    localObject1 = (r.c)b;
    if (localObject1 != null) {
      ((r.c)localObject1).h();
    }
    z();
    I();
  }
  
  public final void i()
  {
    Object localObject1 = g;
    if (localObject1 != null) {
      ((com.truecaller.androidactors.a)localObject1).a();
    }
    g = null;
    B();
    localObject1 = y;
    Object localObject2 = this;
    localObject2 = (com.truecaller.network.search.e.a)this;
    ((com.truecaller.network.search.e)localObject1).b((com.truecaller.network.search.e.a)localObject2);
    localObject1 = u;
    localObject2 = (BroadcastReceiver)p;
    ((com.truecaller.util.al)localObject1).a((BroadcastReceiver)localObject2);
  }
  
  public final void j()
  {
    r.c localc = (r.c)b;
    if (localc != null)
    {
      localc.b();
      return;
    }
  }
  
  public final void k()
  {
    boolean bool1 = m;
    if (bool1)
    {
      localObject = B;
      boolean bool2 = true;
      ((com.truecaller.ads.provider.e)localObject).a(bool2);
    }
    Object localObject = TimeUnit.SECONDS;
    com.truecaller.i.a locala = M;
    String str = "adFeatureRetentionTime";
    long l1 = 0L;
    long l2 = locala.a(str, l1);
    long l3 = ((TimeUnit)localObject).toMillis(l2);
    boolean bool3 = l3 < l1;
    if (!bool3)
    {
      B.d();
      return;
    }
    B.a(l3);
  }
  
  public final void l()
  {
    boolean bool = m;
    if (bool)
    {
      B.a(false);
      com.truecaller.ads.provider.e locale = B;
      locale.f();
    }
    D();
  }
  
  public final void m()
  {
    C();
  }
  
  public final void n()
  {
    I();
  }
  
  public final l.b o()
  {
    return o;
  }
  
  public final boolean p()
  {
    E();
    r.c localc = (r.c)b;
    if (localc != null) {
      localc.i();
    }
    a("imPromoClicked");
    return true;
  }
  
  public final boolean q()
  {
    E();
    a("imPromoDismissed");
    return true;
  }
  
  public final boolean r()
  {
    r.c localc = (r.c)b;
    if (localc != null) {
      localc.l();
    }
    return true;
  }
  
  public final boolean s()
  {
    com.truecaller.messaging.h localh = x;
    boolean bool = true;
    localh.k(bool);
    I();
    return bool;
  }
  
  public final boolean t()
  {
    Object localObject1 = I;
    int i1 = 1;
    ((com.truecaller.util.d.a)localObject1).a(i1);
    localObject1 = x;
    Object localObject2 = null;
    ((com.truecaller.messaging.h)localObject1).e(false);
    I();
    a("createSMSShortcut");
    localObject1 = v;
    int i2 = ((d)localObject1).h();
    int i3 = 26;
    if (i2 < i3)
    {
      localObject1 = (r.c)b;
      if (localObject1 != null)
      {
        Object localObject3 = D;
        int i4 = 2131886459;
        localObject2 = new Object[0];
        localObject2 = ((n)localObject3).a(i4, (Object[])localObject2);
        localObject3 = "resourceProvider.getStri…tMessagesShortcutCreated)";
        k.a(localObject2, (String)localObject3);
        ((r.c)localObject1).a((String)localObject2);
      }
    }
    return i1;
  }
  
  public final boolean u()
  {
    x.e(false);
    I();
    a("smsShortcutDismissed");
    return true;
  }
  
  public final boolean v()
  {
    r.c localc = (r.c)b;
    if (localc != null) {
      localc.p();
    }
    return true;
  }
  
  public final boolean w()
  {
    F();
    r.c localc = (r.c)b;
    if (localc != null) {
      localc.p();
    }
    a("imSoftUpgradeClicked");
    return true;
  }
  
  public final boolean x()
  {
    F();
    a("imSoftUpgradeDismissed");
    return true;
  }
  
  public final boolean y()
  {
    r.c localc = (r.c)b;
    if (localc != null) {
      localc.q();
    }
    return true;
  }
  
  public final void y_()
  {
    super.y_();
    com.truecaller.messaging.data.a.a locala = d;
    if (locala != null) {
      locala.close();
    }
    locala = null;
    d = null;
    com.truecaller.androidactors.a locala1 = h;
    if (locala1 != null) {
      locala1.a();
    }
    h = null;
    locala1 = i;
    if (locala1 != null) {
      locala1.a();
    }
    i = null;
    locala1 = j;
    if (locala1 != null) {
      locala1.a();
    }
    j = null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.b.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */