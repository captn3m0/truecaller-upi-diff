package com.truecaller.messaging.b;

import c.a.f;
import c.g.b.k;
import c.n.m;
import com.truecaller.analytics.e.a;
import com.truecaller.calling.initiate_call.b.a.a;
import com.truecaller.filters.p;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.i.d;
import com.truecaller.messaging.transport.history.HistoryTransportInfo;
import com.truecaller.multisim.ae;
import com.truecaller.util.af;
import com.truecaller.util.al;
import com.truecaller.utils.o;
import com.truecaller.voip.ai;
import java.util.ArrayList;
import java.util.List;

public final class c
  extends com.truecaller.adapter_delegates.c
  implements b.b
{
  private final b.a b;
  private final h c;
  private final al d;
  private final af e;
  private final d f;
  private final o g;
  private final com.truecaller.messaging.h h;
  private final p i;
  private final com.truecaller.network.search.e j;
  private final ae k;
  private final com.truecaller.flashsdk.core.b l;
  private final a m;
  private final com.truecaller.featuretoggles.e n;
  private final com.truecaller.search.local.model.c o;
  private final com.truecaller.search.b p;
  private final y q;
  private final ai r;
  private final com.truecaller.calling.initiate_call.b s;
  private final com.truecaller.analytics.b t;
  private final boolean u;
  
  public c(b.a parama, h paramh, al paramal, af paramaf, d paramd, o paramo, com.truecaller.messaging.h paramh1, p paramp, com.truecaller.network.search.e parame, ae paramae, com.truecaller.flashsdk.core.b paramb, a parama1, com.truecaller.featuretoggles.e parame1, com.truecaller.search.local.model.c paramc, com.truecaller.search.b paramb1, y paramy, ai paramai, com.truecaller.calling.initiate_call.b paramb2, com.truecaller.analytics.b paramb3, boolean paramBoolean)
  {
    b = parama;
    c = paramh;
    d = paramal;
    e = paramaf;
    f = paramd;
    g = paramo;
    h = paramh1;
    i = paramp;
    j = parame;
    k = paramae;
    l = paramb;
    m = parama1;
    n = parame1;
    o = paramc;
    p = paramb1;
    q = paramy;
    r = paramai;
    s = paramb2;
    t = paramb3;
    u = paramBoolean;
  }
  
  private final com.truecaller.messaging.data.a.a a(int paramInt)
  {
    com.truecaller.messaging.data.a.a locala = b.b();
    if (locala != null)
    {
      locala.moveToPosition(paramInt);
      return locala;
    }
    return null;
  }
  
  private final Object a(Conversation paramConversation)
  {
    return b.a(paramConversation);
  }
  
  private final void a(b.c paramc, Conversation paramConversation)
  {
    Object localObject1 = h;
    boolean bool1 = ((com.truecaller.messaging.h)localObject1).m();
    if (bool1)
    {
      localObject1 = i;
      bool1 = ((p)localObject1).g();
      boolean bool2 = true;
      if (bool1)
      {
        localObject1 = n.w();
        bool1 = ((com.truecaller.featuretoggles.b)localObject1).a();
        if (!bool1)
        {
          bool1 = true;
          break label71;
        }
      }
      bool1 = false;
      localObject1 = null;
      label71:
      bool1 = paramConversation.a(bool1);
      if (!bool1)
      {
        localObject1 = l;
        Object localObject2 = "conversation.participants";
        k.a(localObject1, (String)localObject2);
        bool1 = com.truecaller.messaging.i.h.b((Participant[])localObject1);
        if (!bool1)
        {
          localObject1 = new java/util/ArrayList;
          ((ArrayList)localObject1).<init>();
          localObject1 = (List)localObject1;
          localObject2 = new java/util/ArrayList;
          ((ArrayList)localObject2).<init>();
          localObject2 = (List)localObject2;
          paramConversation = l;
          String str1 = "conversation.participants";
          k.a(paramConversation, str1);
          int i1 = paramConversation.length;
          int i2 = 0;
          while (i2 < i1)
          {
            Object localObject3 = paramConversation[i2];
            Object localObject4 = f;
            k.a(localObject4, "it.normalizedAddress");
            String str2 = "+";
            Object localObject5 = "";
            localObject4 = m.d(m.a((String)localObject4, str2, (String)localObject5));
            if (localObject4 != null)
            {
              long l1 = ((Long)localObject4).longValue();
              localObject5 = l;
              String str3 = String.valueOf(l1);
              localObject5 = ((com.truecaller.flashsdk.core.b)localObject5).h(str3);
              boolean bool3 = c;
              if (bool3)
              {
                localObject4 = Long.valueOf(l1);
                ((List)localObject1).add(localObject4);
                k.a(localObject3, "it");
                localObject3 = ((Participant)localObject3).a();
                localObject4 = "it.displayName";
                k.a(localObject3, (String)localObject4);
                ((List)localObject2).add(localObject3);
              }
            }
            i2 += 1;
          }
          paramc.a((List)localObject1, (List)localObject2);
          int i3 = ((List)localObject1).size();
          if (i3 <= 0) {
            bool2 = false;
          }
          paramc.g(bool2);
          return;
        }
      }
    }
    paramc.a(null, null);
  }
  
  private final void a(String paramString)
  {
    com.truecaller.analytics.b localb = t;
    e.a locala = new com/truecaller/analytics/e$a;
    locala.<init>("XAction");
    paramString = locala.a("Context", "Home").a("Action", paramString).a();
    k.a(paramString, "AnalyticsEvent.Builder(X…\n                .build()");
    localb.b(paramString);
  }
  
  public final boolean a(com.truecaller.adapter_delegates.h paramh)
  {
    k.b(paramh, "event");
    int i1 = b;
    Object localObject1 = a(i1);
    boolean bool2 = false;
    Object localObject2 = null;
    if (localObject1 != null)
    {
      localObject1 = ((com.truecaller.messaging.data.a.a)localObject1).b();
      if (localObject1 != null)
      {
        Object localObject3 = b;
        int i2 = ((b.a)localObject3).c();
        paramh = a;
        int i3 = paramh.hashCode();
        int i4 = -1743572928;
        boolean bool3 = true;
        label307:
        boolean bool5;
        String str1;
        if (i3 != i4)
        {
          i4 = -1614871260;
          if (i3 != i4)
          {
            i2 = -1314591573;
            if (i3 != i2)
            {
              i2 = -9897989;
              if (i3 == i2)
              {
                localObject3 = "ItemEvent.ACTION_HISTORY_CLICK";
                boolean bool4 = paramh.equals(localObject3);
                if (bool4)
                {
                  paramh = w;
                  if (paramh != null)
                  {
                    int i5 = d;
                    if (i5 != 0)
                    {
                      i2 = 2;
                      if (i5 != i2)
                      {
                        i2 = 4;
                        if (i5 == i2)
                        {
                          paramh = r;
                          localObject1 = l[0].f;
                          k.a(localObject1, "conversation.participants[0].normalizedAddress");
                          localObject2 = "inbox";
                          paramh.a((String)localObject1, (String)localObject2);
                          paramh = "Voip";
                          a(paramh);
                          break label307;
                        }
                        paramh = new java/lang/IllegalStateException;
                        paramh.<init>("Action should be hidden for other types");
                        throw ((Throwable)paramh);
                      }
                    }
                    paramh = s;
                    localObject3 = new com/truecaller/calling/initiate_call/b$a$a;
                    localObject1 = l[0].f;
                    k.a(localObject1, "conversation.participants[0].normalizedAddress");
                    localObject2 = "inbox";
                    ((b.a.a)localObject3).<init>((String)localObject1, (String)localObject2);
                    localObject1 = ((b.a.a)localObject3).a();
                    paramh.a((com.truecaller.calling.initiate_call.b.a)localObject1);
                    paramh = "Call";
                    a(paramh);
                  }
                  return bool3;
                }
              }
            }
            else
            {
              localObject3 = "ItemEvent.LONG_CLICKED";
              bool5 = paramh.equals(localObject3);
              if (bool5)
              {
                bool5 = a;
                if (!bool5)
                {
                  paramh = c;
                  paramh.e();
                  a((Conversation)localObject1);
                }
                return bool3;
              }
            }
          }
          else
          {
            str1 = "ItemEvent.ACTION_AVATAR_CLICK";
            bool5 = paramh.equals(str1);
            if (bool5)
            {
              bool5 = a;
              if (!bool5)
              {
                paramh = l;
                k.a(paramh, "conversation.participants");
                localObject2 = l;
                str1 = "conversation.participants";
                k.a(localObject2, str1);
                bool2 = com.truecaller.messaging.i.h.b((Participant[])localObject2);
                if (bool2)
                {
                  paramh = x;
                  if (paramh != null)
                  {
                    boolean bool1 = com.truecaller.messaging.i.a.a(paramh);
                    if (!bool1)
                    {
                      bool1 = com.truecaller.messaging.i.a.b(paramh);
                      if (!bool1)
                      {
                        localObject1 = m;
                        ((a)localObject1).b(paramh);
                      }
                    }
                  }
                }
                else
                {
                  localObject2 = l;
                  str1 = "conversation.participants";
                  k.a(localObject2, str1);
                  bool2 = com.truecaller.messaging.i.h.a((Participant[])localObject2);
                  if (!bool2)
                  {
                    localObject2 = (Participant)f.b(paramh);
                    bool2 = ((Participant)localObject2).h();
                    if (bool2)
                    {
                      paramh = (Participant)f.b(paramh);
                      a locala = m;
                      long l1 = a;
                      String str2 = f;
                      localObject1 = "normalizedAddress";
                      k.a(str2, (String)localObject1);
                      String str3 = e;
                      String str4 = m;
                      String str5 = h;
                      locala.a(l1, str2, str3, str4, str5);
                      break label616;
                    }
                  }
                  paramh = m;
                  long l2 = a;
                  paramh.a(l2, i2);
                }
                label616:
                return bool3;
              }
            }
          }
        }
        else
        {
          str1 = "ItemEvent.CLICKED";
          bool5 = paramh.equals(str1);
          if (bool5)
          {
            bool5 = a;
            if (bool5)
            {
              a((Conversation)localObject1);
            }
            else
            {
              paramh = x;
              if (paramh != null)
              {
                bool5 = com.truecaller.messaging.i.a.b(paramh);
                if (bool5 == bool3)
                {
                  paramh = x;
                  if (paramh != null)
                  {
                    localObject1 = m;
                    localObject2 = "it";
                    k.a(paramh, (String)localObject2);
                    ((a)localObject1).a(paramh);
                  }
                  return bool3;
                }
              }
              m.a((Conversation)localObject1, i2);
              return bool3;
            }
          }
        }
        return false;
      }
    }
    return false;
  }
  
  public final int getItemCount()
  {
    com.truecaller.messaging.data.a.a locala = b.b();
    if (locala != null) {
      return locala.getCount();
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    com.truecaller.messaging.data.a.a locala = a(paramInt);
    if (locala != null) {
      return locala.a();
    }
    return -1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */