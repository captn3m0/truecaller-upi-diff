package com.truecaller.messaging.b;

import android.graphics.drawable.Drawable;
import c.g.b.k;
import com.truecaller.backup.BackupTransportInfo;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.TransportInfo;
import com.truecaller.messaging.i.d;
import com.truecaller.messaging.transport.NullTransportInfo.a;
import com.truecaller.messaging.transport.im.ImTransportInfo.a;
import com.truecaller.messaging.transport.mms.MmsTransportInfo.a;
import com.truecaller.messaging.transport.sms.SmsTransportInfo.a;

public final class f
{
  public static final int a(Conversation paramConversation)
  {
    String str = "receiver$0";
    k.b(paramConversation, str);
    int i = e & 0x2;
    if (i != 0) {
      return 1;
    }
    return 0;
  }
  
  public static final Drawable a(Conversation paramConversation, d paramd)
  {
    k.b(paramConversation, "receiver$0");
    Object localObject = "messageUtil";
    k.b(paramd, (String)localObject);
    int i = v;
    int j = 0;
    int k;
    switch (i)
    {
    default: 
      i = 0;
      localObject = null;
      break;
    case 6: 
      i = 0;
      localObject = null;
      break;
    case 5: 
      i = 0;
      localObject = null;
      break;
    case 4: 
      localObject = new com/truecaller/backup/BackupTransportInfo;
      long l = d;
      ((BackupTransportInfo)localObject).<init>(l);
      localObject = (TransportInfo)localObject;
      break;
    case 3: 
      localObject = new com/truecaller/messaging/transport/NullTransportInfo$a;
      ((NullTransportInfo.a)localObject).<init>();
      localObject = (TransportInfo)((NullTransportInfo.a)localObject).a();
      break;
    case 2: 
      localObject = new com/truecaller/messaging/transport/im/ImTransportInfo$a;
      ((ImTransportInfo.a)localObject).<init>();
      k = s;
      c = k;
      k = t;
      d = k;
      localObject = (TransportInfo)((ImTransportInfo.a)localObject).a();
      break;
    case 1: 
      localObject = new com/truecaller/messaging/transport/mms/MmsTransportInfo$a;
      ((MmsTransportInfo.a)localObject).<init>();
      k = u;
      localObject = (TransportInfo)((MmsTransportInfo.a)localObject).a(k).a();
      break;
    case 0: 
      localObject = new com/truecaller/messaging/transport/sms/SmsTransportInfo$a;
      ((SmsTransportInfo.a)localObject).<init>();
      k = u;
      localObject = (TransportInfo)((SmsTransportInfo.a)localObject).a(k).a();
    }
    if (localObject != null)
    {
      int m = e;
      j = ((TransportInfo)localObject).a();
      i = ((TransportInfo)localObject).b();
      return paramd.a(m, j, i);
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.b.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */