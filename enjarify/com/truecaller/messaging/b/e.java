package com.truecaller.messaging.b;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.widget.m;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils.TruncateAt;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.truecaller.adapter_delegates.i;
import com.truecaller.android.truemoji.EmojiTextView;
import com.truecaller.common.h.ah;
import com.truecaller.flashsdk.ui.CompoundFlashButton;
import com.truecaller.messaging.i.c;
import com.truecaller.search.local.model.c.a;
import com.truecaller.ui.view.AvailabilityBadgeView;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.ui.view.TintedImageView;
import com.truecaller.utils.extensions.p;
import com.truecaller.utils.extensions.t;
import com.truecaller.utils.ui.b;
import java.util.Collection;
import java.util.List;

public final class e
  extends RecyclerView.ViewHolder
  implements b.c
{
  private final ContactPhoto b;
  private final TintedImageView c;
  private final AvailabilityBadgeView d;
  private final TextView e;
  private final EmojiTextView f;
  private final TextView g;
  private final View h;
  private final TextView i;
  private final CompoundFlashButton j;
  private final View k;
  private final ForegroundColorSpan l;
  private final ForegroundColorSpan m;
  private final int n;
  private final int o;
  private final int p;
  private final Drawable q;
  private final Drawable r;
  private final Drawable s;
  private final View t;
  private final com.truecaller.adapter_delegates.k u;
  
  public e(View paramView, com.truecaller.adapter_delegates.k paramk)
  {
    super(paramView);
    t = paramView;
    u = paramk;
    paramView = t.findViewById(2131362554);
    c.g.b.k.a(paramView, "view.findViewById(R.id.contact_photo)");
    paramView = (ContactPhoto)paramView;
    b = paramView;
    paramView = (TintedImageView)t.findViewById(2131362555);
    c = paramView;
    paramView = (AvailabilityBadgeView)t.findViewById(2131362065);
    d = paramView;
    paramView = t.findViewById(2131363880);
    c.g.b.k.a(paramView, "view.findViewById(R.id.participants_text)");
    paramView = (TextView)paramView;
    e = paramView;
    paramView = t.findViewById(2131364550);
    c.g.b.k.a(paramView, "view.findViewById(R.id.snippet_text)");
    paramView = (EmojiTextView)paramView;
    f = paramView;
    paramView = t.findViewById(2131364872);
    c.g.b.k.a(paramView, "view.findViewById(R.id.time_text)");
    paramView = (TextView)paramView;
    g = paramView;
    paramView = t.findViewById(2131364018);
    c.g.b.k.a(paramView, "view.findViewById(R.id.progress_bar)");
    h = paramView;
    paramView = (TextView)t.findViewById(2131364563);
    i = paramView;
    paramView = (CompoundFlashButton)t.findViewById(2131363109);
    j = paramView;
    paramView = t.findViewById(2131361867);
    k = paramView;
    paramView = new android/text/style/ForegroundColorSpan;
    int i1 = b.a(t.getContext(), 2130969215);
    paramView.<init>(i1);
    l = paramView;
    paramView = new android/text/style/ForegroundColorSpan;
    i1 = b.a(t.getContext(), 2130969214);
    paramView.<init>(i1);
    m = paramView;
    int i2 = b.a(t.getContext(), 16842806);
    n = i2;
    i2 = b.a(t.getContext(), 16842808);
    o = i2;
    i2 = b.a(t.getContext(), 2130968654);
    p = i2;
    paramView = t.getContext();
    i1 = 2130969528;
    paramView = b.a(paramView, 2131234626, i1);
    q = paramView;
    paramView = b.a(t.getContext(), 2131234692, 2130969592);
    r = paramView;
    paramView = b.a(t.getContext(), 2131235004, i1);
    s = paramView;
    paramView = t;
    paramk = u;
    Object localObject = this;
    localObject = (RecyclerView.ViewHolder)this;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, null, 12);
    paramView = (View)b;
    paramk = u;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject, "ItemEvent.ACTION_AVATAR_CLICK", 8);
    paramView = t;
    paramk = u;
    i.a(paramView, paramk, (RecyclerView.ViewHolder)localObject);
  }
  
  public final void a()
  {
    f.setCompoundDrawables(null, null, null, null);
  }
  
  public final void a(int paramInt)
  {
    b.setContactBadgeDrawable(paramInt);
  }
  
  public final void a(int paramInt, String paramString1, String paramString2, Drawable paramDrawable1, Drawable paramDrawable2, Drawable paramDrawable3, Drawable paramDrawable4, Integer paramInteger, TextUtils.TruncateAt paramTruncateAt)
  {
    c.g.b.k.b(paramString1, "snippet");
    c.g.b.k.b(paramTruncateAt, "ellipsizeMode");
    Object localObject1 = new android/text/SpannableStringBuilder;
    ((SpannableStringBuilder)localObject1).<init>();
    Object localObject2 = f.getContext();
    if (paramInt != 0)
    {
      int i1 = ((SpannableStringBuilder)localObject1).length();
      int i2 = 1;
      if (paramInt == i2)
      {
        if (paramInt != i2) {
          localObject3 = l;
        } else {
          localObject3 = m;
        }
        localObject2 = (CharSequence)((Context)localObject2).getString(2131886676);
        ((SpannableStringBuilder)localObject1).append((CharSequence)localObject2);
        int i3 = ((SpannableStringBuilder)localObject1).length();
        i2 = 33;
        ((SpannableStringBuilder)localObject1).setSpan(localObject3, i1, i3, i2);
        localObject3 = (CharSequence)" ";
        ((SpannableStringBuilder)localObject1).append((CharSequence)localObject3);
      }
      else
      {
        paramString1 = new java/lang/IllegalStateException;
        paramString2 = new java/lang/StringBuilder;
        paramString2.<init>("Status ");
        paramString2.append(paramInt);
        paramString2.append(" is unknown for message status indicator");
        localObject3 = paramString2.toString();
        paramString1.<init>((String)localObject3);
        throw ((Throwable)paramString1);
      }
    }
    Object localObject3 = (CharSequence)ah.a(paramString1);
    ((SpannableStringBuilder)localObject1).append((CharSequence)localObject3);
    localObject3 = f;
    paramString1 = new com/truecaller/messaging/i/c;
    localObject1 = (CharSequence)localObject1;
    paramString1.<init>((CharSequence)localObject1);
    localObject1 = f.getPaint();
    c.g.b.k.a(localObject1, "snippetText.paint");
    localObject1 = ((TextPaint)localObject1).getFontMetricsInt();
    c.g.b.k.a(localObject1, "snippetText.paint.fontMetricsInt");
    localObject2 = "fontMetricsInt";
    c.g.b.k.b(localObject1, (String)localObject2);
    h = ((Paint.FontMetricsInt)localObject1);
    c = paramDrawable1;
    a = paramString2;
    d = paramDrawable2;
    e = paramDrawable3;
    f = paramInteger;
    b = paramDrawable4;
    paramString2 = f.getResources();
    paramDrawable1 = "snippetText.resources";
    c.g.b.k.a(paramString2, paramDrawable1);
    paramString1.a(paramString2);
    paramString2 = f.getTextColors();
    int i4;
    if (paramString2 != null)
    {
      i4 = paramString2.getDefaultColor();
      paramString2 = Integer.valueOf(i4);
    }
    else
    {
      i4 = 0;
      paramString2 = null;
    }
    i = paramString2;
    paramString1 = (CharSequence)paramString1.a();
    ((EmojiTextView)localObject3).setText(paramString1);
    f.setEllipsize(paramTruncateAt);
  }
  
  public final void a(Drawable paramDrawable)
  {
    Object localObject1 = (ImageView)t.findViewById(2131361840);
    boolean bool1 = true;
    if (localObject1 != null)
    {
      Object localObject2 = localObject1;
      localObject2 = (View)localObject1;
      boolean bool2;
      if (paramDrawable != null) {
        bool2 = true;
      } else {
        bool2 = false;
      }
      t.a((View)localObject2, bool2);
      ((ImageView)localObject1).setImageDrawable(paramDrawable);
    }
    localObject1 = t;
    int i1 = 2131361841;
    localObject1 = ((View)localObject1).findViewById(i1);
    if (localObject1 != null)
    {
      if (paramDrawable == null)
      {
        bool1 = false;
        localObject3 = null;
      }
      t.a((View)localObject1, bool1);
      paramDrawable = u;
      Object localObject3 = this;
      localObject3 = (RecyclerView.ViewHolder)this;
      i.a((View)localObject1, paramDrawable, (RecyclerView.ViewHolder)localObject3, "ItemEvent.ACTION_HISTORY_CLICK", 8);
      return;
    }
  }
  
  public final void a(Uri paramUri)
  {
    b.a(paramUri, null);
  }
  
  public final void a(c.a parama)
  {
    AvailabilityBadgeView localAvailabilityBadgeView = d;
    if (localAvailabilityBadgeView != null)
    {
      localAvailabilityBadgeView.b();
      a = parama;
      localAvailabilityBadgeView.a();
      return;
    }
  }
  
  public final void a(Integer paramInteger)
  {
    Object localObject1 = t;
    int i1 = 2131362084;
    localObject1 = (TextView)((View)localObject1).findViewById(i1);
    if (localObject1 != null)
    {
      Object localObject2 = localObject1;
      localObject2 = (View)localObject1;
      boolean bool;
      if (paramInteger != null) {
        bool = true;
      } else {
        bool = false;
      }
      t.a((View)localObject2, bool);
      if (paramInteger != null)
      {
        paramInteger = (CharSequence)String.valueOf(((Number)paramInteger).intValue());
        ((TextView)localObject1).setText(paramInteger);
        return;
      }
      return;
    }
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "title");
    TextView localTextView = e;
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(List paramList1, List paramList2)
  {
    if (paramList1 != null)
    {
      Object localObject = paramList1;
      localObject = (Collection)paramList1;
      boolean bool = ((Collection)localObject).isEmpty() ^ true;
      if ((bool) && (paramList2 != null))
      {
        localObject = k;
        String str = null;
        if (localObject != null) {
          ((View)localObject).setVisibility(0);
        }
        localObject = j;
        if (localObject != null) {
          ((CompoundFlashButton)localObject).setVisibility(0);
        }
        localObject = j;
        if (localObject != null)
        {
          str = "conversation";
          ((CompoundFlashButton)localObject).a(paramList1, paramList2, str);
        }
        return;
      }
    }
    paramList1 = k;
    int i1 = 8;
    if (paramList1 != null) {
      paramList1.setVisibility(i1);
    }
    paramList1 = j;
    if (paramList1 != null) {
      paramList1.setVisibility(i1);
    }
    paramList1 = j;
    if (paramList1 != null)
    {
      paramList1.b();
      return;
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    b.setIsGroup(paramBoolean);
  }
  
  public final void a(boolean paramBoolean1, boolean paramBoolean2)
  {
    if (paramBoolean2) {
      paramBoolean1 = p;
    } else if (paramBoolean1) {
      paramBoolean1 = n;
    } else {
      paramBoolean1 = o;
    }
    if (paramBoolean2) {
      paramBoolean2 = p;
    } else {
      paramBoolean2 = o;
    }
    f.setTextColor(paramBoolean1);
    g.setTextColor(paramBoolean2);
  }
  
  public final void b()
  {
    TextView localTextView = e;
    Drawable localDrawable = q;
    p.a(localTextView, null, null, localDrawable, null, 11);
  }
  
  public final void b(int paramInt)
  {
    Object localObject = c;
    if (localObject != null) {
      ((TintedImageView)localObject).setImageResource(paramInt);
    }
    localObject = c;
    if (localObject != null)
    {
      localObject = (View)localObject;
      if (paramInt != 0) {
        paramInt = 1;
      } else {
        paramInt = 0;
      }
      t.a((View)localObject, paramInt);
      return;
    }
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "time");
    TextView localTextView = g;
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void b(boolean paramBoolean)
  {
    b.setIsSpam(paramBoolean);
  }
  
  public final void c()
  {
    TextView localTextView = e;
    Drawable localDrawable = r;
    p.a(localTextView, null, null, localDrawable, null, 11);
  }
  
  public final void c(int paramInt)
  {
    Drawable localDrawable = b.a(t.getContext(), paramInt, 2130969537);
    m.a((TextView)f, localDrawable, null);
  }
  
  public final void c(String paramString)
  {
    c.g.b.k.b(paramString, "message");
    TextView localTextView = i;
    if (localTextView != null)
    {
      paramString = (CharSequence)paramString;
      localTextView.setText(paramString);
      return;
    }
  }
  
  public final void c(boolean paramBoolean)
  {
    Object localObject1 = e.getTypeface();
    if (localObject1 != null)
    {
      boolean bool = ((Typeface)localObject1).isBold();
      if (bool == paramBoolean) {}
    }
    else
    {
      localTextView1 = e;
      localObject1 = Typeface.create((Typeface)localObject1, paramBoolean);
      localTextView1.setTypeface((Typeface)localObject1);
    }
    int i1;
    if (paramBoolean) {
      i1 = n;
    } else {
      i1 = o;
    }
    f.setTextColor(i1);
    TextView localTextView1 = g;
    localTextView1.setTextColor(i1);
    TextView localTextView2 = e;
    localObject1 = s;
    Object localObject2;
    if (paramBoolean)
    {
      localObject2 = localObject1;
    }
    else
    {
      paramBoolean = false;
      localObject2 = null;
    }
    p.a(localTextView2, (Drawable)localObject2, null, null, null, 14);
  }
  
  public final void d()
  {
    p.a(e, null, null, null, null, 11);
  }
  
  public final void d(boolean paramBoolean)
  {
    t.a(h, paramBoolean);
  }
  
  public final void e(boolean paramBoolean)
  {
    TextView localTextView = i;
    if (localTextView != null)
    {
      t.a((View)localTextView, paramBoolean);
      return;
    }
  }
  
  public final void f(boolean paramBoolean)
  {
    View localView = itemView;
    c.g.b.k.a(localView, "itemView");
    localView.setActivated(paramBoolean);
  }
  
  public final void g(boolean paramBoolean)
  {
    Object localObject = j;
    if (localObject != null)
    {
      ((CompoundFlashButton)localObject).setEnabled(paramBoolean);
      ((CompoundFlashButton)localObject).setClickable(paramBoolean);
      ((CompoundFlashButton)localObject).setFocusable(paramBoolean);
    }
    localObject = k;
    if (localObject != null)
    {
      ((View)localObject).setEnabled(paramBoolean);
      ((View)localObject).setClickable(paramBoolean);
      ((View)localObject).setFocusable(paramBoolean);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.b.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */