package com.truecaller.messaging.b;

import c.g.a.m;
import c.l.g;
import c.o.b;
import c.x;
import com.truecaller.androidactors.f;
import com.truecaller.featuretoggles.b;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.messaging.conversation.bw;
import com.truecaller.messaging.e.l.b;
import com.truecaller.messaging.e.l.b.a;
import com.truecaller.messaging.e.l.b.b;
import com.truecaller.messaging.e.l.b.c;
import com.truecaller.messaging.e.l.b.d;
import com.truecaller.messaging.e.l.b.e;
import com.truecaller.messaging.e.l.b.f;
import com.truecaller.messaging.h;
import com.truecaller.messaging.transport.im.bp;
import com.truecaller.utils.l;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.ag;

final class w$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  w$a(w paramw, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/messaging/b/w$a;
    w localw = b;
    locala.<init>(localw, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = b;
        localObject1 = a;
        bool1 = ((com.truecaller.util.al)localObject1).a();
        if (bool1)
        {
          localObject1 = i;
          bool1 = ((bw)localObject1).a();
          int j = 1;
          Object localObject3;
          boolean bool2;
          if (bool1)
          {
            localObject1 = h;
            Object localObject2 = d;
            localObject3 = e.a;
            int k = 12;
            localObject3 = localObject3[k];
            localObject1 = ((e.a)localObject2).a((e)localObject1, (g)localObject3);
            bool1 = ((b)localObject1).a();
            if (bool1)
            {
              localObject1 = h.i();
              bool1 = ((b)localObject1).a();
              if (!bool1)
              {
                localObject1 = (Collection)((com.truecaller.messaging.transport.im.a.a)g.a()).a().d();
                if (localObject1 != null)
                {
                  bool2 = ((Collection)localObject1).isEmpty() ^ j;
                  if (bool2)
                  {
                    localObject2 = new com/truecaller/messaging/e/l$b$a;
                    ((l.b.a)localObject2).<init>((Collection)localObject1);
                    localObject1 = localObject2;
                    localObject1 = (l.b)localObject2;
                    break label212;
                  }
                }
                bool1 = false;
                localObject1 = null;
                break label212;
              }
            }
          }
          bool1 = false;
          localObject1 = null;
          label212:
          Object localObject4;
          if (localObject1 == null)
          {
            localObject1 = j.a();
            bool2 = k;
            if (bool2)
            {
              bool2 = ((List)localObject1).isEmpty();
              if (!bool2) {}
            }
            else
            {
              j = 0;
              localObject4 = null;
            }
            if (j == 0)
            {
              bool1 = false;
              localObject1 = null;
            }
            if (localObject1 != null)
            {
              localObject4 = new com/truecaller/messaging/e/l$b$c;
              ((l.b.c)localObject4).<init>((List)localObject1);
            }
            else
            {
              j = 0;
              localObject4 = null;
            }
            localObject4 = (l.b)localObject4;
            localObject1 = localObject4;
          }
          if (localObject1 == null)
          {
            localObject1 = b;
            bool1 = ((h)localObject1).U();
            if (bool1)
            {
              long l1 = b.V();
              localObject1 = TimeUnit.DAYS;
              localObject3 = b;
              int m = ((h)localObject3).W();
              long l2 = m;
              l2 = ((TimeUnit)localObject1).toMillis(l2);
              l1 += l2;
              localObject1 = e;
              l2 = ((com.truecaller.utils.a)localObject1).a();
              bool1 = l1 < l2;
              if (!bool1)
              {
                localObject1 = (l.b)l.b.f.a;
                break label412;
              }
            }
            bool1 = false;
            localObject1 = null;
          }
          label412:
          if (localObject1 == null)
          {
            localObject1 = i;
            bool1 = ((bw)localObject1).a();
            if (bool1)
            {
              localObject1 = b;
              bool1 = ((h)localObject1).N();
              if (bool1)
              {
                localObject1 = (Boolean)((bp)f.a()).a().d();
                localObject4 = Boolean.TRUE;
                bool1 = c.g.b.k.a(localObject1, localObject4);
                if (bool1)
                {
                  localObject1 = (l.b)l.b.e.a;
                  break label502;
                }
              }
            }
            bool1 = false;
            localObject1 = null;
          }
          label502:
          if (localObject1 == null)
          {
            localObject1 = c;
            localObject4 = new String[] { "android.permission.READ_SMS" };
            bool1 = ((l)localObject1).a((String[])localObject4);
            if (!bool1)
            {
              localObject1 = b;
              bool1 = ((h)localObject1).ac();
              if (!bool1)
              {
                localObject1 = (l.b)l.b.d.a;
                break label565;
              }
            }
            bool1 = false;
            localObject1 = null;
          }
          label565:
          if (localObject1 != null) {
            break label623;
          }
          bool1 = k;
          if (!bool1)
          {
            localObject1 = d;
            bool1 = ((com.truecaller.common.h.c)localObject1).a();
            if (!bool1)
            {
              paramObject = b;
              boolean bool3 = ((h)paramObject).u();
              if (bool3) {
                return (l.b)l.b.b.a;
              }
            }
          }
        }
        bool1 = false;
        localObject1 = null;
        label623:
        return localObject1;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.b.w.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */