package com.truecaller.messaging.b;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils.TruncateAt;
import com.truecaller.calling.p;
import com.truecaller.search.local.model.c.a;
import java.util.List;

public abstract interface b$c
  extends p
{
  public static final b.c.a a = b.c.a.a;
  
  public abstract void a();
  
  public abstract void a(int paramInt, String paramString1, String paramString2, Drawable paramDrawable1, Drawable paramDrawable2, Drawable paramDrawable3, Drawable paramDrawable4, Integer paramInteger, TextUtils.TruncateAt paramTruncateAt);
  
  public abstract void a(Drawable paramDrawable);
  
  public abstract void a(Uri paramUri);
  
  public abstract void a(c.a parama);
  
  public abstract void a(Integer paramInteger);
  
  public abstract void a(String paramString);
  
  public abstract void a(List paramList1, List paramList2);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void a(boolean paramBoolean1, boolean paramBoolean2);
  
  public abstract void b();
  
  public abstract void b(int paramInt);
  
  public abstract void b(String paramString);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c();
  
  public abstract void c(int paramInt);
  
  public abstract void c(String paramString);
  
  public abstract void c(boolean paramBoolean);
  
  public abstract void d();
  
  public abstract void d(boolean paramBoolean);
  
  public abstract void e(boolean paramBoolean);
  
  public abstract void f(boolean paramBoolean);
  
  public abstract void g(boolean paramBoolean);
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.b.b.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */