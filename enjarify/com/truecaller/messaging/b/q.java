package com.truecaller.messaging.b;

import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d
{
  private final k a;
  private final Provider b;
  
  private q(k paramk, Provider paramProvider)
  {
    a = paramk;
    b = paramProvider;
  }
  
  public static q a(k paramk, Provider paramProvider)
  {
    q localq = new com/truecaller/messaging/b/q;
    localq.<init>(paramk, paramProvider);
    return localq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.b.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */