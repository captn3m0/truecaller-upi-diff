package com.truecaller.messaging.b;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import c.a.ae;
import c.u;
import com.truecaller.adapter_delegates.l;
import com.truecaller.adapter_delegates.n;
import com.truecaller.adapter_delegates.s;
import com.truecaller.adapter_delegates.t;
import com.truecaller.ads.b.w;
import com.truecaller.bk;
import com.truecaller.bl;
import com.truecaller.bp;
import com.truecaller.messaging.conversation.ConversationActivity;
import com.truecaller.messaging.conversationinfo.ConversationInfoActivity;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.e.l.a.a;
import com.truecaller.messaging.e.l.a.b;
import com.truecaller.messaging.e.l.a.c;
import com.truecaller.messaging.e.l.a.d;
import com.truecaller.messaging.e.l.a.e;
import com.truecaller.messaging.e.l.a.f;
import com.truecaller.messaging.e.l.a.g;
import com.truecaller.messaging.imgroupinfo.ImGroupInfoActivity.a;
import com.truecaller.messaging.imgroupinvitation.ImGroupInvitationActivity.a;
import com.truecaller.messaging.newconversation.NewConversationActivity;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.ui.details.a.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class j
  extends Fragment
  implements r.b, r.c
{
  public static final j.a k;
  public r.a a;
  public b.b b;
  public w c;
  public l.a.f d;
  public l.a.e e;
  public l.a.b f;
  public l.a.g g;
  public l.a.a h;
  public l.a.c i;
  public l.a.d j;
  private TextView l;
  private RecyclerView m;
  private com.truecaller.messaging.conversationlist.p n;
  private com.truecaller.messaging.conversationlist.f o;
  private com.truecaller.adapter_delegates.f p;
  private com.truecaller.adapter_delegates.p q;
  private com.truecaller.adapter_delegates.m r;
  private com.truecaller.adapter_delegates.m s;
  private ActionMode t;
  private boolean u;
  private int v;
  private boolean w;
  private final j.b x;
  private HashMap y;
  
  static
  {
    j.a locala = new com/truecaller/messaging/b/j$a;
    locala.<init>((byte)0);
    k = locala;
  }
  
  public j()
  {
    j.b localb = new com/truecaller/messaging/b/j$b;
    localb.<init>(this);
    x = localb;
  }
  
  public final int a()
  {
    return v;
  }
  
  public final void a(int paramInt)
  {
    TextView localTextView = l;
    if (localTextView == null)
    {
      String str = "emptyText";
      c.g.b.k.a(str);
    }
    localTextView.setText(paramInt);
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    r.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.a(paramInt1, paramInt2);
  }
  
  public final void a(int paramInt, boolean paramBoolean)
  {
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      int i1 = 1;
      boolean[] arrayOfBoolean = new boolean[i1];
      arrayOfBoolean[0] = i1;
      Object localObject2 = new android/support/v7/app/AlertDialog$Builder;
      ((AlertDialog.Builder)localObject2).<init>((Context)localObject1);
      localObject1 = getResources();
      int i2 = 2131755014;
      Object localObject3 = new Object[i1];
      Integer localInteger = Integer.valueOf(paramInt);
      localObject3[0] = localInteger;
      localObject1 = (CharSequence)((Resources)localObject1).getQuantityString(i2, paramInt, (Object[])localObject3);
      localObject1 = ((AlertDialog.Builder)localObject2).setMessage((CharSequence)localObject1).setCancelable(false);
      localObject2 = new com/truecaller/messaging/b/j$p;
      ((j.p)localObject2).<init>(arrayOfBoolean, this, paramInt, paramBoolean);
      localObject2 = (DialogInterface.OnClickListener)localObject2;
      Object localObject4 = ((AlertDialog.Builder)localObject1).setPositiveButton(2131887540, (DialogInterface.OnClickListener)localObject2);
      int i3 = 2131887197;
      i1 = 0;
      localObject3 = null;
      localObject4 = ((AlertDialog.Builder)localObject4).setNegativeButton(i3, null);
      if (paramBoolean)
      {
        View localView = LayoutInflater.from(getContext()).inflate(2131558849, null, false);
        i3 = 2131362466;
        localObject1 = localView.findViewById(i3);
        if (localObject1 != null)
        {
          localObject1 = (CheckBox)localObject1;
          localObject3 = new com/truecaller/messaging/b/j$q;
          ((j.q)localObject3).<init>(arrayOfBoolean);
          localObject3 = (CompoundButton.OnCheckedChangeListener)localObject3;
          ((CheckBox)localObject1).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject3);
          ((AlertDialog.Builder)localObject4).setView(localView);
        }
        else
        {
          localObject4 = new c/u;
          ((u)localObject4).<init>("null cannot be cast to non-null type android.widget.CheckBox");
          throw ((Throwable)localObject4);
        }
      }
      ((AlertDialog.Builder)localObject4).create().show();
      return;
    }
  }
  
  public final void a(long paramLong, int paramInt)
  {
    Intent localIntent = new android/content/Intent;
    Context localContext = (Context)getActivity();
    localIntent.<init>(localContext, ConversationInfoActivity.class);
    localIntent.putExtra("conversation_id", paramLong);
    localIntent.putExtra("filter", paramInt);
    startActivity(localIntent);
  }
  
  public final void a(long paramLong, String paramString1, String paramString2, String paramString3, String paramString4)
  {
    c.g.b.k.b(paramString1, "normalizedNumber");
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      Object localObject = localf;
      localObject = (Activity)localf;
      DetailsFragment.SourceType localSourceType = DetailsFragment.SourceType.Conversation;
      Long localLong = Long.valueOf(paramLong);
      DetailsFragment.a((Activity)localObject, paramString4, paramString3, paramString1, paramString2, localSourceType, localLong);
      return;
    }
  }
  
  public final void a(Conversation paramConversation, int paramInt)
  {
    c.g.b.k.b(paramConversation, "conversation");
    Intent localIntent = new android/content/Intent;
    Object localObject = getContext();
    Class localClass = ConversationActivity.class;
    localIntent.<init>((Context)localObject, localClass);
    localObject = "conversation";
    paramConversation = (Parcelable)paramConversation;
    localIntent.putExtra((String)localObject, paramConversation);
    localIntent.putExtra("filter", paramInt);
    paramConversation = getActivity();
    if (paramConversation != null)
    {
      paramConversation.startActivityForResult(localIntent, 1);
      return;
    }
  }
  
  public final void a(ImGroupInfo paramImGroupInfo)
  {
    c.g.b.k.b(paramImGroupInfo, "imGroupInfo");
    android.support.v4.app.f localf = getActivity();
    if (localf == null) {
      return;
    }
    c.g.b.k.a(localf, "activity ?: return");
    paramImGroupInfo = ImGroupInvitationActivity.a.a((Context)localf, paramImGroupInfo);
    localf.startActivity(paramImGroupInfo);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "message");
    Context localContext = getContext();
    paramString = (CharSequence)paramString;
    Toast.makeText(localContext, paramString, 1).show();
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    c.g.b.k.b(paramString2, "address");
    c.g.b.k.b(paramString3, "message");
    Context localContext = getContext();
    if (localContext != null)
    {
      com.truecaller.ui.dialogs.o localo = new com/truecaller/ui/dialogs/o;
      localo.<init>(localContext, paramString1, paramString2, paramString3);
      localo.show();
      return;
    }
  }
  
  public final void a(String paramString, boolean paramBoolean)
  {
    c.g.b.k.b(paramString, "name");
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      Object localObject2 = new com/truecaller/ui/details/a;
      ((com.truecaller.ui.details.a)localObject2).<init>((Context)localObject1, paramString, paramBoolean, true);
      localObject1 = new com/truecaller/messaging/b/j$n;
      ((j.n)localObject1).<init>(this, paramString, paramBoolean);
      localObject1 = (a.a)localObject1;
      localObject1 = ((com.truecaller.ui.details.a)localObject2).a((a.a)localObject1);
      localObject2 = new com/truecaller/messaging/b/j$o;
      ((j.o)localObject2).<init>(this, paramString, paramBoolean);
      localObject2 = (a.a)localObject2;
      ((com.truecaller.ui.details.a)localObject1).b((a.a)localObject2).show();
      return;
    }
  }
  
  public final void a(Set paramSet)
  {
    Object localObject1 = "adsPositions";
    c.g.b.k.b(paramSet, (String)localObject1);
    paramSet = ((Iterable)paramSet).iterator();
    for (;;)
    {
      boolean bool = paramSet.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = (Number)paramSet.next();
      int i1 = ((Number)localObject1).intValue();
      Object localObject2 = r;
      if (localObject2 == null)
      {
        localObject3 = "adsDelegate";
        c.g.b.k.a((String)localObject3);
      }
      i1 = ((com.truecaller.adapter_delegates.m)localObject2).a_(i1);
      localObject2 = p;
      if (localObject2 == null)
      {
        localObject3 = "messagingListAdapter";
        c.g.b.k.a((String)localObject3);
      }
      Object localObject3 = p;
      if (localObject3 == null)
      {
        String str = "messagingListAdapter";
        c.g.b.k.a(str);
      }
      int i2 = ((com.truecaller.adapter_delegates.f)localObject3).getItemCount() - i1;
      ((com.truecaller.adapter_delegates.f)localObject2).notifyItemRangeChanged(i1, i2);
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    TextView localTextView = l;
    if (localTextView == null)
    {
      String str = "emptyText";
      c.g.b.k.a(str);
    }
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localTextView.setVisibility(paramBoolean);
  }
  
  public final void b()
  {
    ActionMode localActionMode = t;
    if (localActionMode != null)
    {
      localActionMode.finish();
      return;
    }
  }
  
  public final void b(int paramInt)
  {
    boolean bool = isAdded();
    if (bool)
    {
      com.truecaller.ui.dialogs.k localk = com.truecaller.ui.dialogs.k.c(paramInt);
      c.g.b.k.a(localk, "ProgressDialogFragment.newInstance(text)");
      android.support.v4.app.j localj = getChildFragmentManager();
      String str = "messaging_list_progress_dialog_tag";
      localk.show(localj, str);
    }
  }
  
  public final void b(ImGroupInfo paramImGroupInfo)
  {
    c.g.b.k.b(paramImGroupInfo, "groupInfo");
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    c.g.b.k.a(localContext, "context ?: return");
    paramImGroupInfo = ImGroupInfoActivity.a.a(localContext, paramImGroupInfo);
    startActivity(paramImGroupInfo);
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "message");
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
      localBuilder.<init>((Context)localObject1);
      localObject1 = paramString;
      localObject1 = (CharSequence)paramString;
      localObject1 = localBuilder.setMessage((CharSequence)localObject1).setCancelable(false);
      Object localObject2 = new com/truecaller/messaging/b/j$m;
      ((j.m)localObject2).<init>(this, paramString);
      localObject2 = (DialogInterface.OnClickListener)localObject2;
      ((AlertDialog.Builder)localObject1).setPositiveButton(2131886772, (DialogInterface.OnClickListener)localObject2).setNegativeButton(2131887197, null).create().show();
      return;
    }
  }
  
  public final void b(boolean paramBoolean)
  {
    b.b localb = b;
    if (localb == null)
    {
      String str = "conversationItemPresenter";
      c.g.b.k.a(str);
    }
    localb.b(paramBoolean);
  }
  
  public final void c()
  {
    r.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.k();
  }
  
  public final void c(String paramString)
  {
    c.g.b.k.b(paramString, "message");
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      AlertDialog.Builder localBuilder = new android/support/v7/app/AlertDialog$Builder;
      localBuilder.<init>((Context)localObject1);
      localObject1 = paramString;
      localObject1 = (CharSequence)paramString;
      localObject1 = localBuilder.setMessage((CharSequence)localObject1).setCancelable(false);
      Object localObject2 = new com/truecaller/messaging/b/j$r;
      ((j.r)localObject2).<init>(this, paramString);
      localObject2 = (DialogInterface.OnClickListener)localObject2;
      ((AlertDialog.Builder)localObject1).setPositiveButton(2131886506, (DialogInterface.OnClickListener)localObject2).setNegativeButton(2131887636, null).create().show();
      return;
    }
  }
  
  public final void c(boolean paramBoolean)
  {
    Resources localResources;
    if (paramBoolean)
    {
      paramBoolean = false;
      localResources = null;
    }
    else
    {
      localResources = getResources();
      int i1 = 2131166204;
      paramBoolean = localResources.getDimensionPixelSize(i1);
    }
    RecyclerView localRecyclerView = m;
    if (localRecyclerView == null)
    {
      localObject1 = "recyclerView";
      c.g.b.k.a((String)localObject1);
    }
    Object localObject1 = m;
    if (localObject1 == null)
    {
      localObject2 = "recyclerView";
      c.g.b.k.a((String)localObject2);
    }
    int i2 = ((RecyclerView)localObject1).getPaddingStart();
    Object localObject2 = m;
    if (localObject2 == null)
    {
      localObject3 = "recyclerView";
      c.g.b.k.a((String)localObject3);
    }
    int i3 = ((RecyclerView)localObject2).getPaddingEnd();
    Object localObject3 = m;
    if (localObject3 == null)
    {
      String str = "recyclerView";
      c.g.b.k.a(str);
    }
    int i4 = ((RecyclerView)localObject3).getPaddingBottom();
    localRecyclerView.setPadding(i2, paramBoolean, i3, i4);
  }
  
  public final void d()
  {
    r.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.l();
  }
  
  public final boolean d(String paramString)
  {
    c.g.b.k.b(paramString, "permissionName");
    android.support.v4.app.f localf = getActivity();
    if (localf != null) {
      return com.truecaller.wizard.utils.i.a((Activity)localf, paramString);
    }
    return false;
  }
  
  public final void e()
  {
    r.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.n();
  }
  
  public final r.a f()
  {
    r.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    return locala;
  }
  
  public final void g()
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      localObject = (AppCompatActivity)localObject;
      ActionMode.Callback localCallback = (ActionMode.Callback)x;
      ((AppCompatActivity)localObject).startSupportActionMode(localCallback);
      return;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type android.support.v7.app.AppCompatActivity");
    throw ((Throwable)localObject);
  }
  
  public final void h()
  {
    com.truecaller.adapter_delegates.f localf = p;
    if (localf == null)
    {
      String str = "messagingListAdapter";
      c.g.b.k.a(str);
    }
    localf.notifyDataSetChanged();
  }
  
  public final void i()
  {
    Intent localIntent = new android/content/Intent;
    Context localContext = (Context)getActivity();
    localIntent.<init>(localContext, NewConversationActivity.class);
    startActivity(localIntent);
  }
  
  public final void j()
  {
    ActionMode localActionMode = t;
    if (localActionMode != null)
    {
      localActionMode.invalidate();
      return;
    }
  }
  
  public final void k()
  {
    boolean bool1 = isAdded();
    if (bool1)
    {
      Object localObject = getChildFragmentManager();
      String str = "messaging_list_progress_dialog_tag";
      localObject = ((android.support.v4.app.j)localObject).a(str);
      boolean bool2 = localObject instanceof android.support.v4.app.e;
      if (bool2)
      {
        localObject = (android.support.v4.app.e)localObject;
        ((android.support.v4.app.e)localObject).dismissAllowingStateLoss();
      }
    }
  }
  
  public final void l()
  {
    com.truecaller.messaging.conversationlist.p localp = n;
    if (localp != null)
    {
      localp.u();
      return;
    }
  }
  
  public final List m()
  {
    Object localObject1 = m;
    if (localObject1 == null)
    {
      localObject2 = "recyclerView";
      c.g.b.k.a((String)localObject2);
    }
    int i1 = ((RecyclerView)localObject1).getChildCount();
    int i2 = 0;
    Object localObject2 = null;
    localObject1 = (Iterable)c.k.i.b(0, i1);
    Object localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    localObject3 = (Collection)localObject3;
    localObject1 = ((Iterable)localObject1).iterator();
    Object localObject4;
    Object localObject5;
    Object localObject6;
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject4 = localObject1;
      localObject4 = (ae)localObject1;
      i3 = ((ae)localObject4).a();
      localObject5 = m;
      if (localObject5 == null)
      {
        localObject6 = "recyclerView";
        c.g.b.k.a((String)localObject6);
      }
      localObject4 = ((RecyclerView)localObject5).getChildAt(i3);
      if (localObject4 != null) {
        ((Collection)localObject3).add(localObject4);
      }
    }
    localObject3 = (Iterable)localObject3;
    localObject1 = new java/util/ArrayList;
    int i3 = 10;
    int i4 = c.a.m.a((Iterable)localObject3, i3);
    ((ArrayList)localObject1).<init>(i4);
    localObject1 = (Collection)localObject1;
    localObject3 = ((Iterable)localObject3).iterator();
    Object localObject7;
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject3).hasNext();
      if (!bool2) {
        break;
      }
      localObject5 = (View)((Iterator)localObject3).next();
      localObject6 = m;
      if (localObject6 == null)
      {
        localObject7 = "recyclerView";
        c.g.b.k.a((String)localObject7);
      }
      int i5 = ((RecyclerView)localObject6).getChildAdapterPosition((View)localObject5);
      localObject5 = Integer.valueOf(i5);
      ((Collection)localObject1).add(localObject5);
    }
    localObject1 = (Iterable)localObject1;
    localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>();
    localObject3 = (Collection)localObject3;
    localObject1 = ((Iterable)localObject1).iterator();
    for (;;)
    {
      boolean bool3 = ((Iterator)localObject1).hasNext();
      if (!bool3) {
        break;
      }
      localObject5 = ((Iterator)localObject1).next();
      localObject6 = localObject5;
      localObject6 = (Number)localObject5;
      int i6 = ((Number)localObject6).intValue();
      int i7 = -1;
      if (i6 != i7)
      {
        localObject7 = q;
        if (localObject7 == null)
        {
          localObject8 = "conversationDelegate";
          c.g.b.k.a((String)localObject8);
        }
        Object localObject8 = p;
        if (localObject8 == null)
        {
          String str = "messagingListAdapter";
          c.g.b.k.a(str);
        }
        i6 = ((com.truecaller.adapter_delegates.f)localObject8).getItemViewType(i6);
        bool4 = ((com.truecaller.adapter_delegates.p)localObject7).b(i6);
        if (bool4)
        {
          bool4 = true;
          break label416;
        }
      }
      boolean bool4 = false;
      localObject6 = null;
      label416:
      if (bool4) {
        ((Collection)localObject3).add(localObject5);
      }
    }
    localObject3 = (Iterable)localObject3;
    localObject1 = new java/util/ArrayList;
    i2 = c.a.m.a((Iterable)localObject3, i3);
    ((ArrayList)localObject1).<init>(i2);
    localObject1 = (Collection)localObject1;
    localObject2 = ((Iterable)localObject3).iterator();
    for (;;)
    {
      boolean bool5 = ((Iterator)localObject2).hasNext();
      if (!bool5) {
        break;
      }
      localObject3 = (Number)((Iterator)localObject2).next();
      int i8 = ((Number)localObject3).intValue();
      localObject4 = p;
      if (localObject4 == null)
      {
        localObject5 = "messagingListAdapter";
        c.g.b.k.a((String)localObject5);
      }
      i8 = ((com.truecaller.adapter_delegates.f)localObject4).a(i8);
      localObject3 = Integer.valueOf(i8);
      ((Collection)localObject1).add(localObject3);
    }
    return (List)localObject1;
  }
  
  public final void n()
  {
    bl localbl = new com/truecaller/bl;
    Context localContext = getContext();
    if (localContext == null) {
      c.g.b.k.a();
    }
    localbl.<init>(localContext, 2131886806, 2131886805, 2131234158);
    localbl.show();
  }
  
  public final void o()
  {
    Object localObject = this;
    localObject = (Fragment)this;
    String[] arrayOfString = { "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE" };
    com.truecaller.wizard.utils.i.a((Fragment)localObject, arrayOfString, 0);
  }
  
  public final void onAttach(Context paramContext)
  {
    super.onAttach(paramContext);
    paramContext = getParentFragment();
    boolean bool = paramContext instanceof com.truecaller.messaging.conversationlist.p;
    if (!bool) {
      paramContext = null;
    }
    paramContext = (com.truecaller.messaging.conversationlist.p)paramContext;
    n = paramContext;
    paramContext = getParentFragment();
    bool = paramContext instanceof com.truecaller.messaging.conversationlist.f;
    if (!bool) {
      paramContext = null;
    }
    paramContext = (com.truecaller.messaging.conversationlist.f)paramContext;
    o = paramContext;
    paramContext = o;
    if (paramContext != null)
    {
      Object localObject = this;
      localObject = (com.truecaller.messaging.conversationlist.e)this;
      paramContext.a((com.truecaller.messaging.conversationlist.e)localObject);
      return;
    }
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getArguments();
    int i1;
    if (paramBundle != null)
    {
      localObject1 = "type_filter";
      i1 = paramBundle.getInt((String)localObject1);
    }
    else
    {
      i1 = -1;
    }
    v = i1;
    paramBundle = getArguments();
    Object localObject1 = null;
    boolean bool1;
    if (paramBundle != null)
    {
      localObject2 = "is_tc_x_enabled";
      bool1 = paramBundle.getBoolean((String)localObject2);
    }
    else
    {
      bool1 = false;
      paramBundle = null;
    }
    w = bool1;
    paramBundle = getContext();
    if (paramBundle == null) {
      return;
    }
    c.g.b.k.a(paramBundle, "context ?: return");
    Object localObject2 = g.a();
    Object localObject3 = paramBundle.getApplicationContext();
    if (localObject3 != null)
    {
      localObject3 = ((bk)localObject3).a();
      localObject2 = ((g.a)localObject2).a((bp)localObject3);
      localObject3 = new com/truecaller/messaging/b/k;
      int i3 = v;
      boolean bool3 = w;
      ((k)localObject3).<init>(paramBundle, i3, bool3);
      paramBundle = ((g.a)localObject2).a((k)localObject3).a();
      paramBundle.a(this);
      bool1 = w;
      int i2;
      if (bool1) {
        i2 = 2131558856;
      } else {
        i2 = 2131558852;
      }
      localObject2 = new com/truecaller/adapter_delegates/p;
      localObject3 = b;
      if (localObject3 == null)
      {
        localObject4 = "conversationItemPresenter";
        c.g.b.k.a((String)localObject4);
      }
      localObject3 = (com.truecaller.adapter_delegates.b)localObject3;
      Object localObject4 = new com/truecaller/messaging/b/j$c;
      ((j.c)localObject4).<init>(this);
      localObject4 = (c.g.a.b)localObject4;
      Object localObject5 = (c.g.a.b)j.d.a;
      ((com.truecaller.adapter_delegates.p)localObject2).<init>((com.truecaller.adapter_delegates.b)localObject3, i2, (c.g.a.b)localObject4, (c.g.a.b)localObject5);
      q = ((com.truecaller.adapter_delegates.p)localObject2);
      paramBundle = c;
      if (paramBundle == null)
      {
        localObject2 = "multiAdsPresenter";
        c.g.b.k.a((String)localObject2);
      }
      paramBundle = com.truecaller.ads.b.j.a(paramBundle);
      r = paramBundle;
      paramBundle = new com/truecaller/adapter_delegates/m;
      int i5 = 7;
      localObject2 = new l[i5];
      localObject3 = new com/truecaller/adapter_delegates/l;
      localObject4 = h;
      if (localObject4 == null)
      {
        localObject5 = "imGroupInvitesPresenter";
        c.g.b.k.a((String)localObject5);
      }
      localObject4 = (n)localObject4;
      int i4 = 2131365491;
      Object localObject6 = new com/truecaller/messaging/b/j$e;
      ((j.e)localObject6).<init>(this);
      localObject6 = (c.g.a.b)localObject6;
      ((l)localObject3).<init>((n)localObject4, i4, (c.g.a.b)localObject6);
      localObject2[0] = localObject3;
      localObject3 = new com/truecaller/adapter_delegates/l;
      localObject4 = d;
      if (localObject4 == null)
      {
        localObject5 = "startImPresenter";
        c.g.b.k.a((String)localObject5);
      }
      localObject4 = (n)localObject4;
      i4 = 2131365505;
      localObject6 = new com/truecaller/messaging/b/j$f;
      ((j.f)localObject6).<init>(this);
      localObject6 = (c.g.a.b)localObject6;
      ((l)localObject3).<init>((n)localObject4, i4, (c.g.a.b)localObject6);
      i3 = 1;
      localObject2[i3] = localObject3;
      localObject3 = new com/truecaller/adapter_delegates/l;
      localObject5 = e;
      if (localObject5 == null)
      {
        localObject6 = "readAndReplyPresenter";
        c.g.b.k.a((String)localObject6);
      }
      localObject5 = (n)localObject5;
      int i6 = 2131365504;
      Object localObject7 = new com/truecaller/messaging/b/j$g;
      ((j.g)localObject7).<init>(this);
      localObject7 = (c.g.a.b)localObject7;
      ((l)localObject3).<init>((n)localObject5, i6, (c.g.a.b)localObject7);
      i4 = 2;
      localObject2[i4] = localObject3;
      int i7 = 3;
      localObject6 = new com/truecaller/adapter_delegates/l;
      localObject7 = f;
      if (localObject7 == null)
      {
        localObject8 = "messageShortcutPresenter";
        c.g.b.k.a((String)localObject8);
      }
      localObject7 = (n)localObject7;
      int i8 = 2131365495;
      Object localObject9 = new com/truecaller/messaging/b/j$h;
      ((j.h)localObject9).<init>(this);
      localObject9 = (c.g.a.b)localObject9;
      ((l)localObject6).<init>((n)localObject7, i8, (c.g.a.b)localObject9);
      localObject2[i7] = localObject6;
      localObject3 = new com/truecaller/adapter_delegates/l;
      localObject6 = g;
      if (localObject6 == null)
      {
        localObject7 = "updateAppPresenter";
        c.g.b.k.a((String)localObject7);
      }
      localObject6 = (n)localObject6;
      int i9 = 2131365506;
      Object localObject8 = new com/truecaller/messaging/b/j$i;
      ((j.i)localObject8).<init>(this);
      localObject8 = (c.g.a.b)localObject8;
      ((l)localObject3).<init>((n)localObject6, i9, (c.g.a.b)localObject8);
      i6 = 4;
      localObject2[i6] = localObject3;
      i7 = 5;
      localObject7 = new com/truecaller/adapter_delegates/l;
      localObject8 = i;
      if (localObject8 == null)
      {
        localObject9 = "missedCallsPresenter";
        c.g.b.k.a((String)localObject9);
      }
      localObject8 = (n)localObject8;
      int i10 = 2131365496;
      Object localObject10 = new com/truecaller/messaging/b/j$j;
      ((j.j)localObject10).<init>(this);
      localObject10 = (c.g.a.b)localObject10;
      ((l)localObject7).<init>((n)localObject8, i10, (c.g.a.b)localObject10);
      localObject2[i7] = localObject7;
      i7 = 6;
      localObject7 = new com/truecaller/adapter_delegates/l;
      localObject8 = j;
      if (localObject8 == null)
      {
        localObject9 = "nonePromoPresenter";
        c.g.b.k.a((String)localObject9);
      }
      localObject8 = (n)localObject8;
      i10 = 2131365503;
      localObject10 = (c.g.a.b)j.k.a;
      ((l)localObject7).<init>((n)localObject8, i10, (c.g.a.b)localObject10);
      localObject2[i7] = localObject7;
      paramBundle.<init>((l[])localObject2);
      s = paramBundle;
      paramBundle = new com/truecaller/adapter_delegates/f;
      localObject2 = q;
      if (localObject2 == null)
      {
        localObject3 = "conversationDelegate";
        c.g.b.k.a((String)localObject3);
      }
      localObject3 = r;
      if (localObject3 == null)
      {
        localObject7 = "adsDelegate";
        c.g.b.k.a((String)localObject7);
      }
      localObject3 = (com.truecaller.adapter_delegates.a)localObject3;
      localObject7 = new com/truecaller/adapter_delegates/o;
      i8 = 12;
      ((com.truecaller.adapter_delegates.o)localObject7).<init>(i4, i8, i6);
      localObject7 = (s)localObject7;
      localObject2 = ((com.truecaller.adapter_delegates.p)localObject2).a((com.truecaller.adapter_delegates.a)localObject3, (s)localObject7);
      localObject3 = s;
      if (localObject3 == null)
      {
        localObject5 = "promoDelegate";
        c.g.b.k.a((String)localObject5);
      }
      localObject3 = (com.truecaller.adapter_delegates.a)localObject3;
      localObject5 = new com/truecaller/adapter_delegates/g;
      ((com.truecaller.adapter_delegates.g)localObject5).<init>((byte)0);
      localObject5 = (s)localObject5;
      localObject1 = (com.truecaller.adapter_delegates.a)((t)localObject2).a((com.truecaller.adapter_delegates.a)localObject3, (s)localObject5);
      paramBundle.<init>((com.truecaller.adapter_delegates.a)localObject1);
      paramBundle.setHasStableIds(i3);
      p = paramBundle;
      setHasOptionsMenu(i3);
      paramBundle = a;
      if (paramBundle == null)
      {
        localObject1 = "presenter";
        c.g.b.k.a((String)localObject1);
      }
      paramBundle.b(this);
      boolean bool2 = u;
      setUserVisibleHint(bool2);
      return;
    }
    paramBundle = new c/u;
    paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
    throw paramBundle;
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    c.g.b.k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558680, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    r.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.x_();
    super.onDestroy();
  }
  
  public final void onDestroyView()
  {
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    ((r.a)localObject).y_();
    super.onDestroyView();
    localObject = y;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onDetach()
  {
    super.onDetach();
    n = null;
    com.truecaller.messaging.conversationlist.f localf = o;
    if (localf != null)
    {
      Object localObject = this;
      localObject = (com.truecaller.messaging.conversationlist.e)this;
      localf.b((com.truecaller.messaging.conversationlist.e)localObject);
    }
    o = null;
  }
  
  public final void onPause()
  {
    super.onPause();
    r.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.i();
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    c.g.b.k.b(paramArrayOfString, "permissions");
    c.g.b.k.b(paramArrayOfInt, "grantResults");
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    com.truecaller.wizard.utils.i.a(paramArrayOfString, paramArrayOfInt);
    r.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.a(paramInt, paramArrayOfString, paramArrayOfInt);
  }
  
  public final void onResume()
  {
    super.onResume();
    r.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      c.g.b.k.a(str);
    }
    locala.h();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject = "view";
    c.g.b.k.b(paramView, (String)localObject);
    super.onViewCreated(paramView, paramBundle);
    int i1 = 2131364093;
    paramBundle = paramView.findViewById(i1);
    if (paramBundle != null)
    {
      paramBundle = (RecyclerView)paramBundle;
      m = paramBundle;
      i1 = 2131362975;
      paramView = paramView.findViewById(i1);
      paramBundle = "view.findViewById(R.id.empty_text)";
      c.g.b.k.a(paramView, paramBundle);
      paramView = (TextView)paramView;
      l = paramView;
      paramView = m;
      if (paramView == null)
      {
        paramBundle = "recyclerView";
        c.g.b.k.a(paramBundle);
      }
      i1 = 0;
      paramBundle = null;
      paramView.setItemAnimator(null);
      paramView = m;
      if (paramView == null)
      {
        paramBundle = "recyclerView";
        c.g.b.k.a(paramBundle);
      }
      paramBundle = new android/support/v7/widget/LinearLayoutManager;
      localObject = getContext();
      paramBundle.<init>((Context)localObject);
      paramBundle = (RecyclerView.LayoutManager)paramBundle;
      paramView.setLayoutManager(paramBundle);
      paramView = m;
      if (paramView == null)
      {
        paramBundle = "recyclerView";
        c.g.b.k.a(paramBundle);
      }
      paramBundle = p;
      if (paramBundle == null)
      {
        localObject = "messagingListAdapter";
        c.g.b.k.a((String)localObject);
      }
      paramBundle = (RecyclerView.Adapter)paramBundle;
      paramView.setAdapter(paramBundle);
      paramView = m;
      if (paramView == null)
      {
        paramBundle = "recyclerView";
        c.g.b.k.a(paramBundle);
      }
      paramBundle = new com/truecaller/messaging/b/j$l;
      paramBundle.<init>(this);
      paramBundle = (RecyclerView.OnScrollListener)paramBundle;
      paramView.addOnScrollListener(paramBundle);
      paramView = a;
      if (paramView == null)
      {
        paramBundle = "presenter";
        c.g.b.k.a(paramBundle);
      }
      paramView.a(this);
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type android.support.v7.widget.RecyclerView");
    throw paramView;
  }
  
  public final void p()
  {
    Context localContext = getContext();
    if (localContext != null)
    {
      com.truecaller.common.h.o.b(localContext);
      return;
    }
  }
  
  public final void q()
  {
    com.truecaller.messaging.conversationlist.f localf = o;
    if (localf != null)
    {
      localf.a(2);
      return;
    }
  }
  
  public final void r()
  {
    com.truecaller.messaging.conversationlist.f localf = o;
    if (localf != null)
    {
      localf.d();
      return;
    }
  }
  
  public final void setUserVisibleHint(boolean paramBoolean)
  {
    super.setUserVisibleHint(paramBoolean);
    Object localObject = this;
    localObject = a;
    if (localObject != null)
    {
      localObject = a;
      if (localObject == null)
      {
        String str = "presenter";
        c.g.b.k.a(str);
      }
      ((r.a)localObject).a(paramBoolean);
    }
    u = paramBoolean;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.b.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */