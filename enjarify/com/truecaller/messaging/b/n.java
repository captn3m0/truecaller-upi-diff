package com.truecaller.messaging.b;

import dagger.a.d;
import javax.inject.Provider;

public final class n
  implements d
{
  private final k a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private n(k paramk, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramk;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static n a(k paramk, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    n localn = new com/truecaller/messaging/b/n;
    localn.<init>(paramk, paramProvider1, paramProvider2, paramProvider3);
    return localn;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.b.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */