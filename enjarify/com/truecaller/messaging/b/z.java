package com.truecaller.messaging.b;

import android.graphics.drawable.Drawable;
import android.text.TextUtils.TruncateAt;
import c.g.b.k;
import com.google.c.a.k.d;
import com.truecaller.common.h.ab;
import com.truecaller.messaging.conversation.bs;
import com.truecaller.messaging.data.types.Conversation;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.i.a;
import com.truecaller.messaging.i.d;
import com.truecaller.messaging.i.h;
import com.truecaller.messaging.transport.history.HistoryTransportInfo;
import com.truecaller.multisim.SimInfo;
import com.truecaller.multisim.ae;
import com.truecaller.util.af;
import com.truecaller.utils.o;
import org.a.a.a.g;

public final class z
  implements y
{
  private final o a;
  private final bs b;
  private final d c;
  private final ae d;
  private final af e;
  
  public z(o paramo, bs parambs, d paramd, ae paramae, af paramaf)
  {
    a = paramo;
    b = parambs;
    c = paramd;
    d = paramae;
    e = paramaf;
  }
  
  private final Drawable a(String paramString, boolean paramBoolean)
  {
    bs localbs = b;
    ae localae = d;
    paramString = localae.a(paramString);
    int i;
    if (paramString != null)
    {
      i = a;
      paramString = Integer.valueOf(i);
    }
    else
    {
      i = 0;
      paramString = null;
    }
    return localbs.a(paramString, paramBoolean);
  }
  
  private static String b(String paramString, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      if (paramString != null)
      {
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        localStringBuilder.append(paramString);
        localStringBuilder.append(" · ");
        return localStringBuilder.toString();
      }
      paramString = null;
    }
    return paramString;
  }
  
  public final void a(b.c paramc, Conversation paramConversation, boolean paramBoolean1, boolean paramBoolean2, String paramString)
  {
    z localz = this;
    b.c localc = paramc;
    Object localObject1 = paramConversation;
    boolean bool1 = paramBoolean2;
    Object localObject2 = paramString;
    k.b(paramc, "itemView");
    k.b(paramConversation, "conversation");
    Object localObject3 = e;
    Object localObject4 = i;
    k.a(localObject4, "conversation.date");
    long l = a;
    localObject3 = ((af)localObject3).g(l);
    Object localObject5 = new java/lang/StringBuilder;
    ((StringBuilder)localObject5).<init>();
    ((StringBuilder)localObject5).append(" · ");
    ((StringBuilder)localObject5).append((CharSequence)localObject3);
    localObject3 = ((StringBuilder)localObject5).toString();
    paramc.b((String)localObject3);
    int j = k;
    localObject3 = Integer.valueOf(j);
    localObject4 = localObject3;
    localObject4 = (Number)localObject3;
    int n = ((Number)localObject4).intValue();
    int i2 = 1;
    Object localObject6 = null;
    if (n > 0)
    {
      n = 1;
    }
    else
    {
      n = 0;
      localObject4 = null;
    }
    if (n == 0)
    {
      j = 0;
      localObject3 = null;
    }
    localc.a((Integer)localObject3);
    j = k;
    if (j > 0)
    {
      j = 1;
    }
    else
    {
      j = 0;
      localObject3 = null;
    }
    n = e;
    int i3 = 8;
    Object localObject7;
    if (n != i3)
    {
      if (paramBoolean1)
      {
        bool4 = paramConversation.c();
        if (bool4)
        {
          localObject4 = l;
          localObject7 = "conversation.participants";
          k.a(localObject4, (String)localObject7);
          bool4 = h.a((Participant[])localObject4);
          if (!bool4) {
            break label283;
          }
        }
      }
      bool4 = false;
      localObject4 = null;
      break label286;
    }
    label283:
    boolean bool4 = true;
    label286:
    localc.a(j, bool4);
    int k = v;
    int i1 = 5;
    if (k == i1)
    {
      k = 1;
    }
    else
    {
      k = 0;
      localObject3 = null;
    }
    if (k != 0)
    {
      localObject3 = localObject1;
    }
    else
    {
      k = 0;
      localObject3 = null;
    }
    Object localObject8;
    if (localObject3 != null)
    {
      localObject3 = w;
      localObject8 = localObject3;
    }
    else
    {
      localObject8 = null;
    }
    int i4 = 2;
    int i5 = 4;
    Object localObject9;
    label761:
    int i6;
    Object localObject10;
    TextUtils.TruncateAt localTruncateAt1;
    boolean bool5;
    if (localObject8 != null)
    {
      k = e;
      if (k != i2)
      {
        if (k != i3)
        {
          localObject3 = b.c();
          localObject4 = localObject3;
        }
        else
        {
          localObject3 = b.d();
          localObject4 = localObject3;
        }
      }
      else
      {
        k = d;
        if (k == i2)
        {
          localObject3 = b.a();
          localObject4 = localObject3;
        }
        else
        {
          localObject3 = b.b();
          localObject4 = localObject3;
        }
      }
      if (paramBoolean1)
      {
        boolean bool2 = paramConversation.c();
        if (bool2)
        {
          localObject3 = l;
          localObject9 = "conversation.participants";
          k.a(localObject3, (String)localObject9);
          bool2 = h.a((Participant[])localObject3);
          if (!bool2)
          {
            localObject3 = "";
            break label761;
          }
        }
      }
      int m = d;
      if (m == i2)
      {
        localObject3 = a;
        i3 = 2131886449;
        localObject7 = new Object[0];
        localObject3 = ((o)localObject3).a(i3, (Object[])localObject7);
        localObject9 = "resourceProvider.getStri…ersationHistoryItemFlash)";
        k.a(localObject3, (String)localObject9);
      }
      else
      {
        m = d;
        if (m == i5)
        {
          localObject3 = a;
          i3 = 2131886448;
          localObject7 = new Object[0];
          localObject3 = ((o)localObject3).a(i3, (Object[])localObject7);
          localObject9 = "resourceProvider.getStri…ersationHistoryItemAudio)";
          k.a(localObject3, (String)localObject9);
        }
        else
        {
          m = d;
          if (m == i4)
          {
            localObject3 = a;
            i3 = 2131886456;
            localObject7 = new Object[0];
            localObject3 = ((o)localObject3).a(i3, (Object[])localObject7);
            localObject9 = "resourceProvider.getStri…ationHistoryItemWhatsApp)";
            k.a(localObject3, (String)localObject9);
          }
          else
          {
            localObject3 = b;
            localObject9 = f;
            localObject7 = k.d.l;
            localObject9 = ab.a((String)localObject9, (k.d)localObject7);
            localObject7 = "PhoneNumberUtils.safeNum… PhoneNumberType.UNKNOWN)";
            k.a(localObject9, (String)localObject7);
            localObject3 = ((bs)localObject3).a((k.d)localObject9);
          }
        }
      }
      i3 = f.a(paramConversation);
      localObject7 = localObject3;
      localObject7 = (CharSequence)localObject3;
      i6 = ((CharSequence)localObject7).length();
      if (i6 <= 0)
      {
        i2 = 0;
        localObject5 = null;
      }
      localObject5 = b((String)localObject2, i2);
      localObject1 = h;
      k.a(localObject1, "conversation.latestSimToken");
      localObject10 = localz.a((String)localObject1, bool1);
      localTruncateAt1 = TextUtils.TruncateAt.END;
      localObject1 = paramc;
      bool1 = i3;
      localObject2 = localObject3;
      localObject3 = localObject5;
      bool5 = false;
      localObject5 = null;
      localObject6 = null;
      localObject9 = localObject10;
      i6 = 0;
      localObject7 = null;
      localObject10 = localTruncateAt1;
      paramc.a(i3, (String)localObject2, (String)localObject3, (Drawable)localObject4, null, null, (Drawable)localObject9, null, localTruncateAt1);
    }
    else
    {
      localObject3 = x;
      if (localObject3 != null)
      {
        boolean bool3 = a.b((ImGroupInfo)localObject3);
        if (bool3 == bool5)
        {
          localObject1 = a;
          i = 2131888363;
          localObject2 = new Object[0];
          localObject1 = ((o)localObject1).a(i, (Object[])localObject2);
          k.a(localObject1, "resourceProvider.getStri…ging_im_group_invitation)");
          TextUtils.TruncateAt localTruncateAt2 = TextUtils.TruncateAt.END;
          b.c.b.a(localc, (String)localObject1, localTruncateAt2);
          break label1272;
        }
      }
      localObject3 = c;
      localObject4 = j;
      i6 = f;
      localObject10 = g;
      localObject3 = ((d)localObject3).a((String)localObject4, i6, (String)localObject10);
      localObject4 = c;
      i6 = f;
      localObject10 = g;
      localObject4 = ((d)localObject4).a(i6, (String)localObject10);
      i6 = e;
      i3 &= i6;
      if (i3 != 0)
      {
        localObject9 = a;
        i6 = 2131234119;
        i7 = 2130969215;
        localObject9 = ((o)localObject9).a(i6, i7);
      }
      else
      {
        i3 = 0;
        localObject9 = null;
      }
      i6 = f.a(paramConversation);
      localObject10 = localObject3;
      localObject10 = (CharSequence)localObject3;
      int i7 = ((CharSequence)localObject10).length();
      if (i7 <= 0)
      {
        bool5 = false;
        localObject5 = null;
      }
      localObject5 = b((String)localObject2, bool5);
      localObject2 = c;
      localObject6 = f.a((Conversation)localObject1, (d)localObject2);
      localObject1 = h;
      k.a(localObject1, "conversation.latestSimToken");
      localObject10 = localz.a((String)localObject1, i);
      localTruncateAt1 = TextUtils.TruncateAt.END;
      localObject1 = paramc;
      int i = i6;
      localObject2 = localObject3;
      localObject3 = localObject5;
      localObject5 = localObject6;
      localObject6 = localObject9;
      localObject9 = localObject10;
      i6 = 0;
      localObject7 = null;
      localObject10 = localTruncateAt1;
      paramc.a(i, (String)localObject2, (String)localObject3, (Drawable)localObject4, (Drawable)localObject5, (Drawable)localObject6, (Drawable)localObject9, null, localTruncateAt1);
    }
    label1272:
    if (localObject8 != null)
    {
      int i8 = d;
      if ((i8 != 0) && (i8 != i4))
      {
        if (i8 == i5)
        {
          localObject1 = b.i();
          localc.a((Drawable)localObject1);
        }
      }
      else
      {
        localObject1 = b.j();
        localc.a((Drawable)localObject1);
        return;
      }
    }
    localc.a(null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.b.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */