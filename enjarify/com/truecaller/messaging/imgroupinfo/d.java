package com.truecaller.messaging.imgroupinfo;

import android.net.Uri;
import com.truecaller.androidactors.i;
import com.truecaller.content.TruecallerContract.r;
import com.truecaller.messaging.data.types.ImGroupInfo;

public final class d
{
  final ImGroupInfo a;
  
  public d(ImGroupInfo paramImGroupInfo)
  {
    a = paramImGroupInfo;
  }
  
  public static Uri a()
  {
    Uri localUri = TruecallerContract.r.a();
    c.g.b.k.a(localUri, "ImGroupInfoTable.getContentUri()");
    return localUri;
  }
  
  public static i a(com.truecaller.androidactors.k paramk)
  {
    c.g.b.k.b(paramk, "actorsThreads");
    paramk = paramk.a();
    c.g.b.k.a(paramk, "actorsThreads.ui()");
    return paramk;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.imgroupinfo.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */