package com.truecaller.messaging.imgroupinfo;

import c.g.b.k;
import com.truecaller.adapter_delegates.c;
import com.truecaller.common.g.a;
import com.truecaller.messaging.transport.im.a.f;
import com.truecaller.messaging.transport.im.a.g;
import com.truecaller.messaging.transport.im.a.i;
import com.truecaller.util.al;

public final class m
  extends c
  implements l.d
{
  private final l.b b;
  private final al c;
  private final l.c d;
  private final i e;
  private final com.truecaller.messaging.h f;
  private final a g;
  
  public m(l.b paramb, al paramal, l.c paramc, i parami, com.truecaller.messaging.h paramh, a parama)
  {
    b = paramb;
    c = paramal;
    d = paramc;
    e = parami;
    f = paramh;
    g = parama;
  }
  
  private final f a(int paramInt)
  {
    g localg = b.i();
    if (localg != null)
    {
      localg.moveToPosition(paramInt);
      if (localg != null) {
        return localg.a();
      }
    }
    return null;
  }
  
  public final boolean a(com.truecaller.adapter_delegates.h paramh)
  {
    k.b(paramh, "event");
    int i = b;
    f localf = a(i);
    if (localf == null) {
      return false;
    }
    paramh = a;
    int j = paramh.hashCode();
    int k = -2047777667;
    String str;
    boolean bool;
    if (j != k)
    {
      k = -981297897;
      if (j != k)
      {
        k = 1076450088;
        if (j != k)
        {
          k = 1662714625;
          if (j != k) {
            break label208;
          }
          str = "ItemEvent.ImGroupParticipantItemMvp.DISMISS_ADMIN";
          bool = paramh.equals(str);
          if (!bool) {
            break label208;
          }
          paramh = d;
          paramh.c(localf);
        }
        else
        {
          str = "ItemEvent.ImGroupParticipantItemMvp.VIEW_PROFILE";
          bool = paramh.equals(str);
          if (!bool) {
            break label208;
          }
          paramh = d;
          paramh.a(localf);
        }
      }
      else
      {
        str = "ItemEvent.ImGroupParticipantItemMvp.MAKE_ADMIN";
        bool = paramh.equals(str);
        if (!bool) {
          break label208;
        }
        paramh = d;
        paramh.b(localf);
      }
    }
    else
    {
      str = "ItemEvent.ImGroupParticipantItemMvp.REMOVE";
      bool = paramh.equals(str);
      if (!bool) {
        break label208;
      }
      paramh = d;
      paramh.d(localf);
    }
    return true;
    label208:
    return false;
  }
  
  public final int getItemCount()
  {
    g localg = b.i();
    if (localg != null) {
      return localg.getCount();
    }
    return 0;
  }
  
  public final long getItemId(int paramInt)
  {
    Object localObject = a(paramInt);
    if (localObject != null)
    {
      localObject = a;
    }
    else
    {
      paramInt = 0;
      localObject = null;
    }
    if (localObject != null)
    {
      paramInt = localObject.hashCode();
    }
    else
    {
      paramInt = 0;
      localObject = null;
    }
    return paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.imgroupinfo.m
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */