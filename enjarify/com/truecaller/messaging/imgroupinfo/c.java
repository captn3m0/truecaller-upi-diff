package com.truecaller.messaging.imgroupinfo;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.c;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.Toolbar.OnMenuItemClickListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import c.g.b.k;
import c.u;
import com.truecaller.R.id;
import com.truecaller.adapter_delegates.p;
import com.truecaller.bk;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.newconversation.NewConversationActivity;
import com.truecaller.messaging.newconversation.j;
import com.truecaller.messaging.newconversation.j.a;
import com.truecaller.ui.details.DetailsFragment;
import com.truecaller.ui.details.DetailsFragment.SourceType;
import com.truecaller.ui.dialogs.f.a;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.utils.extensions.t;
import java.util.HashMap;
import java.util.List;

public final class c
  extends Fragment
  implements h.b
{
  public static final c.a c;
  public h.a a;
  public l.d b;
  private com.truecaller.adapter_delegates.f d;
  private Dialog e;
  private HashMap f;
  
  static
  {
    c.a locala = new com/truecaller/messaging/imgroupinfo/c$a;
    locala.<init>((byte)0);
    c = locala;
  }
  
  public final h.a a()
  {
    h.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return locala;
  }
  
  public final void a(int paramInt)
  {
    int i = R.id.participant_count;
    TextView localTextView = (TextView)b(i);
    k.a(localTextView, "participant_count");
    Resources localResources = getResources();
    Object[] arrayOfObject = new Object[1];
    Integer localInteger = Integer.valueOf(paramInt);
    arrayOfObject[0] = localInteger;
    CharSequence localCharSequence = (CharSequence)localResources.getQuantityString(2131755026, paramInt, arrayOfObject);
    localTextView.setText(localCharSequence);
  }
  
  public final void a(Uri paramUri)
  {
    int i = R.id.contact_photo;
    ((ContactPhoto)b(i)).a(paramUri, null);
  }
  
  public final void a(ImGroupInfo paramImGroupInfo)
  {
    k.b(paramImGroupInfo, "imGroupInfo");
    Object localObject = NewConversationActivity.a;
    localObject = requireContext();
    k.a(localObject, "requireContext()");
    k.b(localObject, "context");
    k.b(paramImGroupInfo, "imGroupInfo");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>((Context)localObject, NewConversationActivity.class);
    paramImGroupInfo = (Parcelable)paramImGroupInfo;
    paramImGroupInfo = localIntent.putExtra("im_group_info", paramImGroupInfo);
    k.a(paramImGroupInfo, "Intent(context, NewConve…_GROUP_INFO, imGroupInfo)");
    startActivityForResult(paramImGroupInfo, 1);
  }
  
  public final void a(String paramString)
  {
    int i = R.id.name_text;
    Object localObject = (TextView)b(i);
    k.a(localObject, "name_text");
    paramString = (CharSequence)paramString;
    ((TextView)localObject).setText(paramString);
    i = R.id.toolbar;
    localObject = (Toolbar)b(i);
    k.a(localObject, "toolbar");
    ((Toolbar)localObject).setTitle(paramString);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    Context localContext = requireContext();
    DetailsFragment.SourceType localSourceType = DetailsFragment.SourceType.Conversation;
    DetailsFragment.a(localContext, paramString4, paramString3, paramString1, paramString2, null, localSourceType, true, 20);
  }
  
  public final void a(boolean paramBoolean)
  {
    int i = R.id.toolbar;
    Object localObject = (Toolbar)b(i);
    k.a(localObject, "toolbar");
    localObject = ((Toolbar)localObject).getMenu().findItem(2131361864);
    k.a(localObject, "toolbar.menu.findItem(R.id.action_edit)");
    ((MenuItem)localObject).setVisible(paramBoolean);
  }
  
  public final View b(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = getView();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void b()
  {
    com.truecaller.adapter_delegates.f localf = d;
    if (localf == null)
    {
      String str = "adapter";
      k.a(str);
    }
    localf.notifyDataSetChanged();
  }
  
  public final void b(ImGroupInfo paramImGroupInfo)
  {
    k.b(paramImGroupInfo, "imGroupInfo");
    Object localObject = EditImGroupInfoActivity.a;
    localObject = requireContext();
    k.a(localObject, "requireContext()");
    k.b(localObject, "context");
    k.b(paramImGroupInfo, "imGroupInfo");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>((Context)localObject, EditImGroupInfoActivity.class);
    paramImGroupInfo = (Parcelable)paramImGroupInfo;
    paramImGroupInfo = localIntent.putExtra("im_group_info", paramImGroupInfo);
    k.a(paramImGroupInfo, "Intent(context, EditImGr…_GROUP_INFO, imGroupInfo)");
    startActivityForResult(paramImGroupInfo, 2);
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "groupTitle");
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    k.a(localContext, "context ?: return");
    Object localObject = new android/support/v7/app/AlertDialog$Builder;
    ((AlertDialog.Builder)localObject).<init>(localContext);
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramString;
    paramString = (CharSequence)getString(2131886610, arrayOfObject);
    paramString = ((AlertDialog.Builder)localObject).setTitle(paramString).setMessage(2131886609);
    localObject = new com/truecaller/messaging/imgroupinfo/c$j;
    ((c.j)localObject).<init>(this);
    localObject = (DialogInterface.OnClickListener)localObject;
    paramString.setPositiveButton(2131886608, (DialogInterface.OnClickListener)localObject).setNegativeButton(2131886387, null).show();
  }
  
  public final void b(boolean paramBoolean)
  {
    int i = R.id.add_participants_view;
    LinearLayout localLinearLayout = (LinearLayout)b(i);
    k.a(localLinearLayout, "add_participants_view");
    t.a((View)localLinearLayout, paramBoolean);
  }
  
  public final void c()
  {
    android.support.v4.app.f localf = getActivity();
    if (localf != null)
    {
      localf.finish();
      return;
    }
  }
  
  public final void c(boolean paramBoolean)
  {
    int i = R.id.mute_notifications_switch;
    SwitchCompat localSwitchCompat = (SwitchCompat)b(i);
    k.a(localSwitchCompat, "mute_notifications_switch");
    localSwitchCompat.setChecked(paramBoolean);
  }
  
  public final void d()
  {
    Toast.makeText(getContext(), 2131886538, 1).show();
  }
  
  public final void e()
  {
    Object localObject = getContext();
    if (localObject == null) {
      return;
    }
    k.a(localObject, "context ?: return");
    localObject = (Dialog)f.a.a((Context)localObject);
    e = ((Dialog)localObject);
  }
  
  public final void f()
  {
    Dialog localDialog = e;
    if (localDialog != null) {
      localDialog.dismiss();
    }
    e = null;
  }
  
  public final void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if (paramIntent == null) {
      return;
    }
    switch (paramInt1)
    {
    default: 
      return;
    case 2: 
      return;
    }
    h.a locala = a;
    if (locala == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    Object localObject = j.e;
    localObject = j.a.a(paramIntent);
    locala.a((List)localObject);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getActivity();
    if (paramBundle != null)
    {
      paramBundle = (Activity)paramBundle;
      Object localObject1 = getArguments();
      if (localObject1 != null)
      {
        Object localObject2 = "group_info";
        localObject1 = (ImGroupInfo)((Bundle)localObject1).getParcelable((String)localObject2);
        if (localObject1 != null)
        {
          localObject2 = a.a();
          paramBundle = paramBundle.getApplicationContext();
          if (paramBundle != null)
          {
            paramBundle = ((bk)paramBundle).a();
            paramBundle = ((a.a)localObject2).a(paramBundle);
            localObject2 = new com/truecaller/messaging/imgroupinfo/d;
            ((d)localObject2).<init>((ImGroupInfo)localObject1);
            paramBundle.a((d)localObject2).a().a(this);
            paramBundle = new com/truecaller/adapter_delegates/p;
            localObject1 = b;
            if (localObject1 == null)
            {
              localObject2 = "participantItemPresenter";
              k.a((String)localObject2);
            }
            localObject1 = (com.truecaller.adapter_delegates.b)localObject1;
            Object localObject3 = new com/truecaller/messaging/imgroupinfo/c$b;
            ((c.b)localObject3).<init>(this);
            localObject3 = (c.g.a.b)localObject3;
            c.g.a.b localb = (c.g.a.b)c.c.a;
            paramBundle.<init>((com.truecaller.adapter_delegates.b)localObject1, 2131558870, (c.g.a.b)localObject3, localb);
            localObject1 = new com/truecaller/adapter_delegates/f;
            paramBundle = (com.truecaller.adapter_delegates.a)paramBundle;
            ((com.truecaller.adapter_delegates.f)localObject1).<init>(paramBundle);
            d = ((com.truecaller.adapter_delegates.f)localObject1);
            return;
          }
          paramBundle = new c/u;
          paramBundle.<init>("null cannot be cast to non-null type com.truecaller.GraphHolder");
          throw paramBundle;
        }
      }
      return;
    }
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    return paramLayoutInflater.inflate(2131558709, paramViewGroup, false);
  }
  
  public final void onDestroyView()
  {
    Object localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((h.a)localObject).y_();
    super.onDestroyView();
    localObject = f;
    if (localObject != null) {
      ((HashMap)localObject).clear();
    }
  }
  
  public final void onStart()
  {
    super.onStart();
    h.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.a();
  }
  
  public final void onStop()
  {
    super.onStop();
    h.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.b();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    Object localObject1 = "view";
    k.b(paramView, (String)localObject1);
    super.onViewCreated(paramView, paramBundle);
    paramView = getActivity();
    boolean bool = paramView instanceof AppCompatActivity;
    if (!bool)
    {
      j = 0;
      paramView = null;
    }
    paramView = (AppCompatActivity)paramView;
    if (paramView == null) {
      return;
    }
    int i = R.id.toolbar;
    paramBundle = (Toolbar)b(i);
    localObject1 = new com/truecaller/messaging/imgroupinfo/c$d;
    ((c.d)localObject1).<init>(this);
    localObject1 = (View.OnClickListener)localObject1;
    paramBundle.setNavigationOnClickListener((View.OnClickListener)localObject1);
    i = R.id.toolbar;
    ((Toolbar)b(i)).inflateMenu(2131623956);
    i = R.id.toolbar;
    paramBundle = (Toolbar)b(i);
    localObject1 = new com/truecaller/messaging/imgroupinfo/c$e;
    ((c.e)localObject1).<init>(this);
    localObject1 = (Toolbar.OnMenuItemClickListener)localObject1;
    paramBundle.setOnMenuItemClickListener((Toolbar.OnMenuItemClickListener)localObject1);
    paramBundle = requireContext();
    i = com.truecaller.utils.ui.b.a(paramBundle, 2130969274);
    int k = R.id.toolbar;
    localObject1 = (Toolbar)b(k);
    Object localObject2 = "toolbar";
    k.a(localObject1, (String)localObject2);
    localObject1 = ((Toolbar)localObject1).getMenu();
    int m = 2131361864;
    localObject1 = ((Menu)localObject1).findItem(m);
    if (localObject1 != null)
    {
      localObject2 = ((MenuItem)localObject1).getIcon();
      if (localObject2 != null)
      {
        localObject2 = android.support.v4.graphics.drawable.a.e((Drawable)localObject2).mutate();
        android.support.v4.graphics.drawable.a.a((Drawable)localObject2, i);
        ((MenuItem)localObject1).setIcon((Drawable)localObject2);
      }
    }
    k = R.id.toolbar;
    localObject1 = (Toolbar)b(k);
    localObject2 = "toolbar";
    k.a(localObject1, (String)localObject2);
    localObject1 = ((Toolbar)localObject1).getNavigationIcon();
    if (localObject1 != null)
    {
      localObject1 = android.support.v4.graphics.drawable.a.e((Drawable)localObject1).mutate();
      android.support.v4.graphics.drawable.a.a((Drawable)localObject1, i);
      m = R.id.toolbar;
      localObject2 = (Toolbar)b(m);
      String str = "toolbar";
      k.a(localObject2, str);
      ((Toolbar)localObject2).setNavigationIcon((Drawable)localObject1);
    }
    k = R.id.toolbar;
    localObject1 = (Toolbar)b(k);
    ((Toolbar)localObject1).setTitleTextColor(i);
    paramView = paramView.getSupportActionBar();
    if (paramView != null)
    {
      paramView.setDisplayHomeAsUpEnabled(true);
      i = 0;
      paramBundle = null;
      paramView.setDisplayShowTitleEnabled(false);
    }
    int j = R.id.app_bar_layout;
    paramView = (AppBarLayout)b(j);
    paramBundle = new com/truecaller/messaging/imgroupinfo/c$f;
    paramBundle.<init>(this);
    paramBundle = (AppBarLayout.c)paramBundle;
    paramView.a(paramBundle);
    j = R.id.leave_group_view;
    paramView = (TextView)b(j);
    paramBundle = new com/truecaller/messaging/imgroupinfo/c$g;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    j = R.id.add_participants_view;
    paramView = (LinearLayout)b(j);
    paramBundle = new com/truecaller/messaging/imgroupinfo/c$h;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    j = R.id.mute_notifications_switch;
    paramView = (SwitchCompat)b(j);
    paramBundle = new com/truecaller/messaging/imgroupinfo/c$i;
    paramBundle.<init>(this);
    paramBundle = (CompoundButton.OnCheckedChangeListener)paramBundle;
    paramView.setOnCheckedChangeListener(paramBundle);
    j = R.id.recycler_view;
    paramView = (RecyclerView)b(j);
    k.a(paramView, "recycler_view");
    paramBundle = d;
    if (paramBundle == null)
    {
      localObject1 = "adapter";
      k.a((String)localObject1);
    }
    paramBundle = (RecyclerView.Adapter)paramBundle;
    paramView.setAdapter(paramBundle);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.imgroupinfo.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */