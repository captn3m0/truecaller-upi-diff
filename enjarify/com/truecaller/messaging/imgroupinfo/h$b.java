package com.truecaller.messaging.imgroupinfo;

import android.net.Uri;
import com.truecaller.messaging.data.types.ImGroupInfo;

public abstract interface h$b
{
  public abstract void a(int paramInt);
  
  public abstract void a(Uri paramUri);
  
  public abstract void a(ImGroupInfo paramImGroupInfo);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString1, String paramString2, String paramString3, String paramString4);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void b();
  
  public abstract void b(ImGroupInfo paramImGroupInfo);
  
  public abstract void b(String paramString);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c();
  
  public abstract void c(boolean paramBoolean);
  
  public abstract void d();
  
  public abstract void e();
  
  public abstract void f();
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.imgroupinfo.h.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */