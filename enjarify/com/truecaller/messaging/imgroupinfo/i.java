package com.truecaller.messaging.imgroupinfo;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import c.g.a.b;
import c.g.b.k;
import c.l;
import com.truecaller.androidactors.ac;
import com.truecaller.androidactors.w;
import com.truecaller.api.services.messenger.v1.models.GroupAction;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.data.types.ImGroupPermissions;
import com.truecaller.messaging.data.types.Participant;
import com.truecaller.messaging.data.types.Participant.a;
import com.truecaller.messaging.transport.im.a.a;
import com.truecaller.messaging.transport.im.a.g;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class i
  extends h.a
{
  private g a;
  private final i.a c;
  private boolean d;
  private final i.b e;
  private ImGroupInfo f;
  private final com.truecaller.androidactors.i g;
  private final com.truecaller.androidactors.f h;
  private final ContentResolver i;
  private final Uri j;
  private final com.truecaller.messaging.transport.im.a.i k;
  
  public i(ImGroupInfo paramImGroupInfo, com.truecaller.androidactors.i parami, com.truecaller.androidactors.f paramf, ContentResolver paramContentResolver, Uri paramUri, com.truecaller.messaging.transport.im.a.i parami1)
  {
    f = paramImGroupInfo;
    g = parami;
    h = paramf;
    i = paramContentResolver;
    j = paramUri;
    k = parami1;
    paramImGroupInfo = new com/truecaller/messaging/imgroupinfo/i$a;
    parami = new android/os/Handler;
    paramf = Looper.getMainLooper();
    parami.<init>(paramf);
    paramImGroupInfo.<init>(this, parami);
    c = paramImGroupInfo;
    paramImGroupInfo = new com/truecaller/messaging/imgroupinfo/i$b;
    parami = new android/os/Handler;
    paramf = Looper.getMainLooper();
    parami.<init>(paramf);
    paramImGroupInfo.<init>(this, parami);
    e = paramImGroupInfo;
  }
  
  private final void a(ImGroupInfo paramImGroupInfo)
  {
    if (paramImGroupInfo == null) {
      return;
    }
    f = paramImGroupInfo;
    k();
  }
  
  private final void k()
  {
    h.b localb = (h.b)b;
    if (localb != null)
    {
      Object localObject = f.b;
      if (localObject == null) {
        localObject = "";
      }
      localb.a((String)localObject);
      localObject = f.c;
      if (localObject != null)
      {
        localObject = Uri.parse((String)localObject);
      }
      else
      {
        bool = false;
        localObject = null;
      }
      localb.a((Uri)localObject);
      localObject = k;
      ImGroupPermissions localImGroupPermissions = f.g;
      GroupAction localGroupAction = GroupAction.UPDATE_INFO;
      boolean bool = ((com.truecaller.messaging.transport.im.a.i)localObject).a(localImGroupPermissions, localGroupAction);
      localb.a(bool);
      localObject = k;
      localImGroupPermissions = f.g;
      localGroupAction = GroupAction.INVITE;
      bool = ((com.truecaller.messaging.transport.im.a.i)localObject).a(localImGroupPermissions, localGroupAction);
      localb.b(bool);
      localObject = f;
      int m = h;
      int n = 1;
      if (m != n)
      {
        n = 0;
        localImGroupPermissions = null;
      }
      localb.c(n);
      localb.b();
      return;
    }
  }
  
  private final void l()
  {
    Object localObject1 = (a)h.a();
    Object localObject2 = f.a;
    localObject1 = ((a)localObject1).a((String)localObject2);
    localObject2 = g;
    Object localObject3 = new com/truecaller/messaging/imgroupinfo/i$d;
    Object localObject4 = this;
    localObject4 = (i)this;
    ((i.d)localObject3).<init>((i)localObject4);
    localObject3 = (b)localObject3;
    localObject4 = new com/truecaller/messaging/imgroupinfo/j;
    ((j)localObject4).<init>((b)localObject3);
    localObject4 = (ac)localObject4;
    ((w)localObject1).a((com.truecaller.androidactors.i)localObject2, (ac)localObject4);
  }
  
  private final void m()
  {
    Object localObject1 = (a)h.a();
    Object localObject2 = f.a;
    localObject1 = ((a)localObject1).e((String)localObject2);
    localObject2 = g;
    Object localObject3 = new com/truecaller/messaging/imgroupinfo/i$c;
    Object localObject4 = this;
    localObject4 = (i)this;
    ((i.c)localObject3).<init>((i)localObject4);
    localObject3 = (b)localObject3;
    localObject4 = new com/truecaller/messaging/imgroupinfo/j;
    ((j)localObject4).<init>((b)localObject3);
    localObject4 = (ac)localObject4;
    ((w)localObject1).a((com.truecaller.androidactors.i)localObject2, (ac)localObject4);
  }
  
  private final void n()
  {
    boolean bool = d;
    if (!bool) {
      return;
    }
    g localg = a;
    if (localg != null)
    {
      ContentObserver localContentObserver = (ContentObserver)c;
      localg.unregisterContentObserver(localContentObserver);
    }
    d = false;
  }
  
  public final void a()
  {
    l();
    m();
    ContentResolver localContentResolver = i;
    Uri localUri = j;
    ContentObserver localContentObserver = (ContentObserver)e;
    localContentResolver.registerContentObserver(localUri, true, localContentObserver);
  }
  
  public final void a(com.truecaller.messaging.transport.im.a.f paramf)
  {
    k.b(paramf, "participant");
    h.b localb = (h.b)b;
    if (localb != null)
    {
      String str1 = c;
      String str2 = d;
      String str3 = e;
      paramf = h;
      localb.a(str1, str2, str3, paramf);
      return;
    }
  }
  
  public final void a(List paramList)
  {
    if (paramList != null)
    {
      paramList = (Iterable)paramList;
      Object localObject1 = new java/util/ArrayList;
      ((ArrayList)localObject1).<init>();
      localObject1 = (Collection)localObject1;
      paramList = paramList.iterator();
      int m;
      Object localObject2;
      for (;;)
      {
        boolean bool1 = paramList.hasNext();
        m = 1;
        if (!bool1) {
          break;
        }
        localObject2 = paramList.next();
        Object localObject3 = localObject2;
        localObject3 = (CharSequence)d;
        if (localObject3 != null)
        {
          int n = ((CharSequence)localObject3).length();
          if (n != 0) {
            m = 0;
          }
        }
        if (m == 0) {
          ((Collection)localObject1).add(localObject2);
        }
      }
      localObject1 = (List)localObject1;
      paramList = (List)localObject1;
      paramList = (Collection)localObject1;
      boolean bool2 = paramList.isEmpty() ^ m;
      if (!bool2) {
        localObject1 = null;
      }
      if (localObject1 != null)
      {
        paramList = (a)h.a();
        localObject2 = f.a;
        paramList = paramList.a((String)localObject2, (List)localObject1);
        localObject1 = g;
        localObject2 = new com/truecaller/messaging/imgroupinfo/i$e;
        ((i.e)localObject2).<init>(this);
        localObject2 = (ac)localObject2;
        paramList.a((com.truecaller.androidactors.i)localObject1, (ac)localObject2);
        return;
      }
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    boolean bool = true;
    com.truecaller.androidactors.i locali;
    if (paramBoolean != bool)
    {
      if (!paramBoolean)
      {
        bool = false;
        locali = null;
      }
    }
    else
    {
      localObject1 = f;
      paramBoolean = h;
      if (bool == paramBoolean) {
        return;
      }
      localObject1 = (a)h.a();
      Object localObject2 = f.a;
      localObject1 = ((a)localObject1).a((String)localObject2, bool);
      locali = g;
      localObject2 = new com/truecaller/messaging/imgroupinfo/i$i;
      ((i.i)localObject2).<init>(this);
      localObject2 = (ac)localObject2;
      ((w)localObject1).a(locali, (ac)localObject2);
      return;
    }
    Object localObject1 = new c/l;
    ((l)localObject1).<init>();
    throw ((Throwable)localObject1);
  }
  
  public final void b()
  {
    n();
    ContentResolver localContentResolver = i;
    ContentObserver localContentObserver = (ContentObserver)e;
    localContentResolver.unregisterContentObserver(localContentObserver);
  }
  
  public final void b(com.truecaller.messaging.transport.im.a.f paramf)
  {
    k.b(paramf, "participant");
    Object localObject1 = (a)h.a();
    Object localObject2 = f.a;
    paramf = a;
    paramf = ((a)localObject1).a((String)localObject2, paramf, 536870912);
    localObject1 = g;
    localObject2 = new com/truecaller/messaging/imgroupinfo/i$h;
    Object localObject3 = this;
    localObject3 = (i)this;
    ((i.h)localObject2).<init>((i)localObject3);
    localObject2 = (b)localObject2;
    localObject3 = new com/truecaller/messaging/imgroupinfo/j;
    ((j)localObject3).<init>((b)localObject2);
    localObject3 = (ac)localObject3;
    paramf.a((com.truecaller.androidactors.i)localObject1, (ac)localObject3);
  }
  
  public final void c(com.truecaller.messaging.transport.im.a.f paramf)
  {
    k.b(paramf, "participant");
    Object localObject1 = (a)h.a();
    Object localObject2 = f.a;
    paramf = a;
    paramf = ((a)localObject1).a((String)localObject2, paramf, 8);
    localObject1 = g;
    localObject2 = new com/truecaller/messaging/imgroupinfo/i$f;
    Object localObject3 = this;
    localObject3 = (i)this;
    ((i.f)localObject2).<init>((i)localObject3);
    localObject2 = (b)localObject2;
    localObject3 = new com/truecaller/messaging/imgroupinfo/j;
    ((j)localObject3).<init>((b)localObject2);
    localObject3 = (ac)localObject3;
    paramf.a((com.truecaller.androidactors.i)localObject1, (ac)localObject3);
  }
  
  public final boolean c()
  {
    h.b localb = (h.b)b;
    if (localb != null) {
      localb.c();
    }
    return true;
  }
  
  public final void d(com.truecaller.messaging.transport.im.a.f paramf)
  {
    k.b(paramf, "participant");
    Object localObject1 = (a)h.a();
    Object localObject2 = f.a;
    paramf = a;
    Object localObject3 = new com/truecaller/messaging/data/types/Participant$a;
    ((Participant.a)localObject3).<init>(3);
    paramf = ((Participant.a)localObject3).b(paramf).d(paramf).a();
    k.a(paramf, "Participant.Builder(Part…Id(imId)\n        .build()");
    paramf = ((a)localObject1).a((String)localObject2, paramf);
    localObject1 = g;
    localObject2 = new com/truecaller/messaging/imgroupinfo/i$j;
    localObject3 = this;
    localObject3 = (i)this;
    ((i.j)localObject2).<init>((i)localObject3);
    localObject2 = (b)localObject2;
    localObject3 = new com/truecaller/messaging/imgroupinfo/j;
    ((j)localObject3).<init>((b)localObject2);
    localObject3 = (ac)localObject3;
    paramf.a((com.truecaller.androidactors.i)localObject1, (ac)localObject3);
  }
  
  public final void e()
  {
    h.b localb = (h.b)b;
    if (localb != null)
    {
      ImGroupInfo localImGroupInfo = f;
      localb.a(localImGroupInfo);
      return;
    }
  }
  
  public final void f()
  {
    h.b localb = (h.b)b;
    if (localb != null)
    {
      String str = f.b;
      if (str == null) {
        str = "";
      }
      localb.b(str);
      return;
    }
  }
  
  public final void g()
  {
    Object localObject1 = (h.b)b;
    if (localObject1 != null) {
      ((h.b)localObject1).e();
    }
    localObject1 = (a)h.a();
    Object localObject2 = f.a;
    localObject1 = ((a)localObject1).a((String)localObject2, false);
    localObject2 = g;
    Object localObject3 = new com/truecaller/messaging/imgroupinfo/i$g;
    Object localObject4 = this;
    localObject4 = (i)this;
    ((i.g)localObject3).<init>((i)localObject4);
    localObject3 = (b)localObject3;
    localObject4 = new com/truecaller/messaging/imgroupinfo/j;
    ((j)localObject4).<init>((b)localObject3);
    localObject4 = (ac)localObject4;
    ((w)localObject1).a((com.truecaller.androidactors.i)localObject2, (ac)localObject4);
  }
  
  public final boolean h()
  {
    h.b localb = (h.b)b;
    if (localb != null)
    {
      ImGroupInfo localImGroupInfo = f;
      localb.b(localImGroupInfo);
    }
    return true;
  }
  
  public final g i()
  {
    return a;
  }
  
  public final ImGroupInfo j()
  {
    return f;
  }
  
  public final void y_()
  {
    g localg = a;
    if (localg != null) {
      localg.close();
    }
    a = null;
    super.y_();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.imgroupinfo.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */