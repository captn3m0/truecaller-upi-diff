package com.truecaller.messaging.imgroupinfo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.AppCompatActivity;
import c.g.b.k;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;

public final class ImGroupInfoActivity
  extends AppCompatActivity
{
  public static final ImGroupInfoActivity.a a;
  
  static
  {
    ImGroupInfoActivity.a locala = new com/truecaller/messaging/imgroupinfo/ImGroupInfoActivity$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    Object localObject1 = ThemeManager.a();
    int i = resId;
    setTheme(i);
    super.onCreate(paramBundle);
    if (paramBundle != null) {
      return;
    }
    paramBundle = getIntent();
    localObject1 = "group_info";
    paramBundle = (ImGroupInfo)paramBundle.getParcelableExtra((String)localObject1);
    if (paramBundle == null) {
      return;
    }
    localObject1 = getSupportFragmentManager().a();
    Object localObject2 = c.c;
    k.b(paramBundle, "groupInfo");
    localObject2 = new com/truecaller/messaging/imgroupinfo/c;
    ((c)localObject2).<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    paramBundle = (Parcelable)paramBundle;
    localBundle.putParcelable("group_info", paramBundle);
    ((c)localObject2).setArguments(localBundle);
    localObject2 = (Fragment)localObject2;
    ((o)localObject1).b(16908290, (Fragment)localObject2).c();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.imgroupinfo.ImGroupInfoActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */