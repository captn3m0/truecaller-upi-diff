package com.truecaller.messaging.imgroupinfo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.AppCompatActivity;
import c.g.b.k;
import com.truecaller.messaging.data.types.ImGroupInfo;
import com.truecaller.messaging.newconversation.a.c;
import com.truecaller.ui.ThemeManager;
import com.truecaller.ui.ThemeManager.Theme;

public final class EditImGroupInfoActivity
  extends AppCompatActivity
{
  public static final EditImGroupInfoActivity.a a;
  
  static
  {
    EditImGroupInfoActivity.a locala = new com/truecaller/messaging/imgroupinfo/EditImGroupInfoActivity$a;
    locala.<init>((byte)0);
    a = locala;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    ThemeManager.Theme localTheme = ThemeManager.a();
    int i = resId;
    setTheme(i);
    super.onCreate(paramBundle);
    if (paramBundle == null)
    {
      paramBundle = getSupportFragmentManager().a();
      i = 16908290;
      Object localObject1 = c.c;
      localObject1 = getIntent().getParcelableExtra("im_group_info");
      k.a(localObject1, "intent.getParcelableExtra(EXTRA_IM_GROUP_INFO)");
      localObject1 = (ImGroupInfo)localObject1;
      k.b(localObject1, "imGroupInfo");
      Object localObject2 = new com/truecaller/messaging/newconversation/a/c;
      ((c)localObject2).<init>();
      Bundle localBundle = new android/os/Bundle;
      localBundle.<init>();
      localObject1 = (Parcelable)localObject1;
      localBundle.putParcelable("im_group_info", (Parcelable)localObject1);
      localObject1 = "im_group_mode";
      String str = "im_group_mode_edit";
      localBundle.putString((String)localObject1, str);
      ((c)localObject2).setArguments(localBundle);
      localObject2 = (Fragment)localObject2;
      paramBundle = paramBundle.b(i, (Fragment)localObject2);
      paramBundle.c();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.imgroupinfo.EditImGroupInfoActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */