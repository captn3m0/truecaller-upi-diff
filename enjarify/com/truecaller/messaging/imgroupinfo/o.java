package com.truecaller.messaging.imgroupinfo;

import android.net.Uri;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.utils.extensions.t;

public final class o
  extends RecyclerView.ViewHolder
  implements l.e
{
  private final ContactPhoto a;
  private final TextView b;
  private final TextView c;
  private final View d;
  private final com.truecaller.adapter_delegates.k e;
  
  public o(View paramView, com.truecaller.adapter_delegates.k paramk)
  {
    super(paramView);
    e = paramk;
    paramk = paramView.findViewById(2131362554);
    c.g.b.k.a(paramk, "view.findViewById(R.id.contact_photo)");
    paramk = (ContactPhoto)paramk;
    a = paramk;
    paramk = paramView.findViewById(2131363791);
    c.g.b.k.a(paramk, "view.findViewById(R.id.name_text)");
    paramk = (TextView)paramk;
    b = paramk;
    paramk = paramView.findViewById(2131364187);
    c.g.b.k.a(paramk, "view.findViewById(R.id.roles_text)");
    paramk = (TextView)paramk;
    c = paramk;
    paramView = paramView.findViewById(2131363736);
    c.g.b.k.a(paramView, "view.findViewById(R.id.menu_button)");
    d = paramView;
  }
  
  public final void a(Uri paramUri)
  {
    a.setImageURI(paramUri);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "name");
    TextView localTextView = b;
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void a(boolean paramBoolean)
  {
    t.a((View)c, paramBoolean);
  }
  
  public final void a(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
  {
    View localView = d;
    boolean bool;
    if ((!paramBoolean1) && (!paramBoolean2) && (!paramBoolean3) && (!paramBoolean4)) {
      bool = false;
    } else {
      bool = true;
    }
    t.a(localView, bool);
    localView = d;
    Object localObject = new com/truecaller/messaging/imgroupinfo/o$a;
    ((o.a)localObject).<init>(this, paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4);
    localObject = (View.OnClickListener)localObject;
    localView.setOnClickListener((View.OnClickListener)localObject);
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "roles");
    TextView localTextView = c;
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.imgroupinfo.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */