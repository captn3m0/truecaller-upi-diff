package com.truecaller.messaging.imgroupinfo;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.c;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.R.id;
import com.truecaller.ui.view.ContactPhoto;
import com.truecaller.utils.ui.b;

final class c$f
  implements AppBarLayout.c
{
  c$f(c paramc) {}
  
  public final void a(AppBarLayout paramAppBarLayout, int paramInt)
  {
    k.b(paramAppBarLayout, "appBarLayout");
    int i = paramAppBarLayout.getTotalScrollRange();
    paramInt = Math.abs(paramInt);
    float f1 = i - paramInt;
    float f2 = paramAppBarLayout.getTotalScrollRange();
    f1 /= f2;
    paramAppBarLayout = a;
    i = R.id.contact_photo;
    paramAppBarLayout = (ContactPhoto)paramAppBarLayout.b(i);
    k.a(paramAppBarLayout, "contact_photo");
    paramAppBarLayout.setAlpha(f1);
    paramAppBarLayout = a;
    i = R.id.name_text;
    paramAppBarLayout = (TextView)paramAppBarLayout.b(i);
    String str = "name_text";
    k.a(paramAppBarLayout, str);
    paramAppBarLayout.setAlpha(f1);
    f2 = 0.0F;
    paramAppBarLayout = null;
    boolean bool = f1 < 0.0F;
    int j;
    if (!bool)
    {
      paramAppBarLayout = a.requireContext();
      paramInt = 2130969274;
      f1 = 1.7547225E38F;
      j = b.a(paramAppBarLayout, paramInt);
    }
    else
    {
      j = 0;
      f2 = 0.0F;
      paramAppBarLayout = null;
    }
    c localc = a;
    i = R.id.toolbar;
    ((Toolbar)localc.b(i)).setTitleTextColor(j);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.imgroupinfo.c.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */