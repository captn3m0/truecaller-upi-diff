package com.truecaller.messaging.imgroupinfo;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import c.g.b.k;

final class o$a
  implements View.OnClickListener
{
  o$a(o paramo, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4) {}
  
  public final void onClick(View paramView)
  {
    paramView = new android/support/v7/widget/PopupMenu;
    Object localObject1 = o.a(a).getContext();
    Object localObject2 = o.a(a);
    paramView.<init>((Context)localObject1, (View)localObject2);
    paramView.inflate(2131623957);
    localObject1 = new com/truecaller/messaging/imgroupinfo/o$a$1;
    ((o.a.1)localObject1).<init>(this);
    localObject1 = (PopupMenu.OnMenuItemClickListener)localObject1;
    paramView.setOnMenuItemClickListener((PopupMenu.OnMenuItemClickListener)localObject1);
    localObject1 = paramView.getMenu();
    localObject2 = ((Menu)localObject1).findItem(2131361923);
    k.a(localObject2, "findItem(R.id.action_remove)");
    boolean bool1 = b;
    ((MenuItem)localObject2).setVisible(bool1);
    localObject2 = ((Menu)localObject1).findItem(2131361902);
    k.a(localObject2, "findItem(R.id.action_make_admin)");
    bool1 = c;
    ((MenuItem)localObject2).setVisible(bool1);
    localObject2 = ((Menu)localObject1).findItem(2131361861);
    k.a(localObject2, "findItem(R.id.action_dismiss_admin)");
    bool1 = d;
    ((MenuItem)localObject2).setVisible(bool1);
    localObject1 = ((Menu)localObject1).findItem(2131361944);
    k.a(localObject1, "findItem(R.id.action_view_profile)");
    boolean bool2 = e;
    ((MenuItem)localObject1).setVisible(bool2);
    paramView.show();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.imgroupinfo.o.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */