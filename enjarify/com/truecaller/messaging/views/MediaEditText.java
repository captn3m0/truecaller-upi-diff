package com.truecaller.messaging.views;

import android.content.Context;
import android.support.b.a.a.a;
import android.support.b.a.a.b;
import android.support.b.a.a.b.a;
import android.util.AttributeSet;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import c.g.b.k;

public final class MediaEditText
  extends com.truecaller.android.truemoji.c
{
  private MediaEditText.a a;
  private android.support.b.a.a.c b;
  private final Runnable c;
  private final b.a d;
  
  public MediaEditText(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    paramContext = new com/truecaller/messaging/views/MediaEditText$c;
    paramContext.<init>(this);
    paramContext = (Runnable)paramContext;
    c = paramContext;
    paramContext = new com/truecaller/messaging/views/MediaEditText$b;
    paramContext.<init>(this);
    paramContext = (b.a)paramContext;
    d = paramContext;
  }
  
  public final MediaEditText.a getMediaCallback()
  {
    return a;
  }
  
  public final InputConnection onCreateInputConnection(EditorInfo paramEditorInfo)
  {
    k.b(paramEditorInfo, "outAttrs");
    InputConnection localInputConnection = super.onCreateInputConnection(paramEditorInfo);
    if (localInputConnection == null) {
      return null;
    }
    Object localObject = a;
    if (localObject == null) {
      return localInputConnection;
    }
    localObject = ((MediaEditText.a)localObject).Y();
    a.a(paramEditorInfo, (String[])localObject);
    localObject = d;
    return b.a(localInputConnection, paramEditorInfo, (b.a)localObject);
  }
  
  public final void setMediaCallback(MediaEditText.a parama)
  {
    a = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.views.MediaEditText
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */