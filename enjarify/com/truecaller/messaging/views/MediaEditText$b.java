package com.truecaller.messaging.views;

import android.content.ClipDescription;
import android.os.Build.VERSION;
import android.support.b.a.a.b.a;
import android.support.b.a.a.c;
import c.g.b.k;

final class MediaEditText$b
  implements b.a
{
  MediaEditText$b(MediaEditText paramMediaEditText) {}
  
  public final boolean a(c paramc, int paramInt)
  {
    Object localObject1 = MediaEditText.a(a);
    if (localObject1 != null) {
      return false;
    }
    if (paramc == null) {
      return false;
    }
    localObject1 = a.getMediaCallback();
    if (localObject1 == null) {
      return false;
    }
    int i = 1;
    paramInt &= i;
    if (paramInt != 0)
    {
      paramInt = 1;
    }
    else
    {
      paramInt = 0;
      localObject2 = null;
    }
    int j = Build.VERSION.SDK_INT;
    int k = 25;
    if ((j >= k) && (paramInt != 0)) {
      try
      {
        paramc.c();
      }
      catch (Exception localException)
      {
        return false;
      }
    }
    Object localObject2 = paramc.b();
    String str1 = "it";
    k.a(localObject2, str1);
    j = ((ClipDescription)localObject2).getMimeTypeCount();
    if (j == i)
    {
      j = 1;
    }
    else
    {
      j = 0;
      str1 = null;
    }
    k = 0;
    String str2 = null;
    if (j == 0)
    {
      paramInt = 0;
      localObject2 = null;
    }
    if (localObject2 != null) {
      str2 = ((ClipDescription)localObject2).getMimeType(0);
    }
    MediaEditText.a(a, paramc);
    paramc = paramc.a();
    k.a(paramc, "inputContentInfo.contentUri");
    localObject2 = MediaEditText.b(a);
    ((MediaEditText.a)localObject1).a(paramc, str2, (Runnable)localObject2);
    return i;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.views.MediaEditText.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */