package com.truecaller.messaging.views;

import android.os.Build.VERSION;
import android.support.b.a.a.c;

final class MediaEditText$c
  implements Runnable
{
  MediaEditText$c(MediaEditText paramMediaEditText) {}
  
  public final void run()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 25;
    if (i >= j)
    {
      c localc = MediaEditText.a(a);
      if (localc != null) {
        localc.d();
      }
    }
    MediaEditText.a(a, null);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.messaging.views.MediaEditText.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */