package com.truecaller.flashsdk.core;

public enum Theme
{
  static
  {
    Theme[] arrayOfTheme = new Theme[6];
    Theme localTheme = new com/truecaller/flashsdk/core/Theme;
    localTheme.<init>("DARK", 0);
    DARK = localTheme;
    arrayOfTheme[0] = localTheme;
    localTheme = new com/truecaller/flashsdk/core/Theme;
    int i = 1;
    localTheme.<init>("LIGHT", i);
    LIGHT = localTheme;
    arrayOfTheme[i] = localTheme;
    localTheme = new com/truecaller/flashsdk/core/Theme;
    i = 2;
    localTheme.<init>("COFFEE", i);
    COFFEE = localTheme;
    arrayOfTheme[i] = localTheme;
    localTheme = new com/truecaller/flashsdk/core/Theme;
    i = 3;
    localTheme.<init>("RAMADAN", i);
    RAMADAN = localTheme;
    arrayOfTheme[i] = localTheme;
    localTheme = new com/truecaller/flashsdk/core/Theme;
    i = 4;
    localTheme.<init>("PITCH_BLACK", i);
    PITCH_BLACK = localTheme;
    arrayOfTheme[i] = localTheme;
    localTheme = new com/truecaller/flashsdk/core/Theme;
    i = 5;
    localTheme.<init>("LIGHT_GRAY", i);
    LIGHT_GRAY = localTheme;
    arrayOfTheme[i] = localTheme;
    $VALUES = arrayOfTheme;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.Theme
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */