package com.truecaller.flashsdk.core;

import android.graphics.Bitmap;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.o.b;
import c.x;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.models.Contact;
import com.truecaller.flashsdk.models.Payload;
import com.truecaller.flashsdk.models.QueuedFlash;
import com.truecaller.flashsdk.models.Sender;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ax;
import kotlinx.coroutines.g;

final class KidFlashService$d
  extends c.d.b.a.k
  implements c.g.a.m
{
  Object a;
  Object b;
  Object c;
  int d;
  private ag h;
  
  KidFlashService$d(KidFlashService paramKidFlashService, QueuedFlash paramQueuedFlash, Bitmap paramBitmap, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    d locald = new com/truecaller/flashsdk/core/KidFlashService$d;
    KidFlashService localKidFlashService = e;
    QueuedFlash localQueuedFlash = f;
    Bitmap localBitmap = g;
    locald.<init>(localKidFlashService, localQueuedFlash, localBitmap, paramc);
    paramObject = (ag)paramObject;
    h = ((ag)paramObject);
    return locald;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = d;
    int j = 1;
    Bitmap localBitmap = null;
    String str;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      localObject1 = (Sender)b;
      str = (String)a;
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label428;
      }
      f.a(false);
      paramObject = f.f();
      c.g.b.k.a(paramObject, "flash.payload");
      str = ((Payload)paramObject).b();
      paramObject = f.a();
      c.g.b.k.a(paramObject, "sender");
      long l = ((Sender)paramObject).a().longValue();
      Object localObject2 = String.valueOf(l);
      localObject3 = KidFlashService.d(e);
      localObject4 = (f)ax.b();
      localObject3 = ((f)localObject3).plus((f)localObject4);
      localObject4 = new com/truecaller/flashsdk/core/KidFlashService$d$a;
      ((KidFlashService.d.a)localObject4).<init>(this, (String)localObject2, null);
      localObject4 = (c.g.a.m)localObject4;
      a = str;
      b = paramObject;
      c = localObject2;
      d = j;
      localObject2 = g.a((f)localObject3, (c.g.a.m)localObject4, this);
      if (localObject2 == localObject1) {
        return localObject1;
      }
      localObject1 = paramObject;
      paramObject = localObject2;
    }
    paramObject = (Contact)paramObject;
    if (paramObject != null)
    {
      paramObject = ((Contact)paramObject).getName();
      if (paramObject != null) {}
    }
    else
    {
      c.g.b.k.a(localObject1, "sender");
      paramObject = ((Sender)localObject1).b();
    }
    localObject1 = e;
    int k = R.string.resume_flash;
    Object localObject5 = new Object[j];
    c.g.b.k.a(paramObject, "name");
    Object localObject3 = paramObject;
    localObject3 = (CharSequence)paramObject;
    Object localObject4 = (CharSequence)" ";
    boolean bool3 = c.n.m.a((CharSequence)localObject3, (CharSequence)localObject4, false);
    if (bool3)
    {
      paramObject = new String[] { " " };
      int m = 6;
      paramObject = (String)c.n.m.c((CharSequence)localObject3, (String[])paramObject, false, m).get(0);
    }
    localObject5[0] = paramObject;
    paramObject = ((KidFlashService)localObject1).getString(k, (Object[])localObject5);
    localObject1 = e;
    localObject5 = f;
    localBitmap = g;
    c.g.b.k.a(str, "title");
    c.g.b.k.a(paramObject, "content");
    KidFlashService.a((KidFlashService)localObject1, (QueuedFlash)localObject5, localBitmap, str, (String)paramObject);
    return x.a;
    label428:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (d)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((d)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.KidFlashService.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */