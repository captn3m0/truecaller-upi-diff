package com.truecaller.flashsdk.core;

import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.IBinder;
import android.os.Parcelable;
import android.support.v4.content.d;
import android.widget.Toast;
import c.g.b.k;
import c.u;
import com.truecaller.flashsdk.core.a.a.a;
import com.truecaller.flashsdk.core.a.b.s;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.ImageFlash;

public final class FlashMediaService
  extends Service
  implements f.b
{
  public static final FlashMediaService.a b;
  public f.a a;
  
  static
  {
    FlashMediaService.a locala = new com/truecaller/flashsdk/core/FlashMediaService$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  private final void a(Intent paramIntent)
  {
    d.a((Context)this).a(paramIntent);
  }
  
  public final void a()
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("action_image_flash");
    localIntent.putExtra("extra_state", "state_flash_failed");
    a(localIntent);
  }
  
  public final void a(ImageFlash paramImageFlash)
  {
    k.b(paramImageFlash, "flash");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("action_image_flash");
    localIntent.putExtra("extra_state", "state_uploading_failed");
    paramImageFlash = (Parcelable)paramImageFlash;
    localIntent.putExtra("extra_image_flash", paramImageFlash);
    a(localIntent);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "message");
    Object localObject = this;
    localObject = (Context)this;
    paramString = (CharSequence)paramString;
    Toast.makeText((Context)localObject, paramString, 0).show();
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "downloadUrl");
    k.b(paramString2, "fileName");
    k.b(paramString3, "notificationTitle");
    Object localObject = getSystemService("download");
    if (localObject != null)
    {
      localObject = (DownloadManager)localObject;
      paramString1 = Uri.parse(paramString1);
      DownloadManager.Request localRequest = new android/app/DownloadManager$Request;
      localRequest.<init>(paramString1);
      paramString1 = localRequest.setAllowedNetworkTypes(3);
      int i = 1;
      paramString1 = paramString1.setAllowedOverRoaming(i);
      paramString3 = (CharSequence)paramString3;
      paramString1 = paramString1.setTitle(paramString3);
      paramString3 = Environment.DIRECTORY_DOWNLOADS;
      paramString1.setDestinationInExternalPublicDir(paramString3, paramString2).setMimeType("image/jpeg").setVisibleInDownloadsUi(i).setNotificationVisibility(i).allowScanningByMediaScanner();
      ((DownloadManager)localObject).enqueue(localRequest);
      return;
    }
    paramString1 = new c/u;
    paramString1.<init>("null cannot be cast to non-null type android.app.DownloadManager");
    throw paramString1;
  }
  
  public final void b()
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("action_image_flash");
    localIntent.putExtra("extra_state", "state_flash_sent");
    a(localIntent);
  }
  
  public final void b(ImageFlash paramImageFlash)
  {
    k.b(paramImageFlash, "flash");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("action_image_flash");
    localIntent.putExtra("extra_state", "state_uploading_failed");
    paramImageFlash = (Parcelable)paramImageFlash;
    localIntent.putExtra("extra_image_flash", paramImageFlash);
    a(localIntent);
  }
  
  public final void c()
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("action_image_flash");
    localIntent.putExtra("extra_state", "state_uploaded");
    a(localIntent);
  }
  
  public final IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public final void onCreate()
  {
    super.onCreate();
    Object localObject1 = c.b;
    localObject1 = c.b();
    Object localObject2 = new com/truecaller/flashsdk/core/a/b/s;
    ((s)localObject2).<init>();
    ((a)localObject1).a((s)localObject2).a(this);
    localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    ((f.a)localObject1).a(this);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    f.a locala = a;
    if (locala == null)
    {
      String str = "presenter";
      k.a(str);
    }
    locala.y_();
  }
  
  public final int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    if (paramIntent == null) {
      return 2;
    }
    f.a locala = a;
    if (locala == null)
    {
      str = "presenter";
      k.a(str);
    }
    String str = paramIntent.getAction();
    Flash localFlash = (Flash)paramIntent.getParcelableExtra("extra_flash");
    paramIntent = (ImageFlash)paramIntent.getParcelableExtra("extra_image_flash");
    return locala.a(str, localFlash, paramIntent);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.FlashMediaService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */