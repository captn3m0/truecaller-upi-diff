package com.truecaller.flashsdk.core;

import android.content.res.Resources;
import android.graphics.BitmapFactory;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.u;
import c.x;
import com.truecaller.flashsdk.assist.al;
import com.truecaller.flashsdk.assist.d;
import com.truecaller.flashsdk.assist.g;
import com.truecaller.flashsdk.models.Contact;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.Sender;
import kotlinx.coroutines.ag;

final class KidFlashService$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  KidFlashService$b(KidFlashService paramKidFlashService, Flash paramFlash, int paramInt, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/flashsdk/core/KidFlashService$b;
    KidFlashService localKidFlashService = b;
    Flash localFlash = c;
    int i = d;
    localb.<init>(localKidFlashService, localFlash, i, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1)
      {
        paramObject = null;
        localObject = b.b();
        bool1 = ((g)localObject).f();
        if (bool1)
        {
          paramObject = b.a();
          localObject = c.a();
          String str = "flash.sender";
          c.g.b.k.a(localObject, str);
          long l = ((Sender)localObject).a().longValue();
          localObject = String.valueOf(l);
          paramObject = ((d)paramObject).b((String)localObject);
        }
        if (paramObject != null)
        {
          paramObject = ((Contact)paramObject).getImageUrl();
          if (paramObject != null) {}
        }
        else
        {
          paramObject = c.a();
          localObject = "flash.sender";
          c.g.b.k.a(paramObject, (String)localObject);
          paramObject = ((Sender)paramObject).c();
        }
        if (paramObject != null)
        {
          localObject = paramObject;
          localObject = (CharSequence)paramObject;
          int j = ((CharSequence)localObject).length();
          boolean bool2 = true;
          if (j == 0)
          {
            j = 1;
          }
          else
          {
            j = 0;
            localObject = null;
          }
          if (j != 0)
          {
            paramObject = b.getResources();
            j = d;
            return BitmapFactory.decodeResource((Resources)paramObject, j);
          }
          return b.c().a((String)paramObject, bool2);
        }
        paramObject = new c/u;
        ((u)paramObject).<init>("null cannot be cast to non-null type kotlin.String");
        throw ((Throwable)paramObject);
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.KidFlashService.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */