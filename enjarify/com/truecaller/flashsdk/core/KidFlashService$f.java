package com.truecaller.flashsdk.core;

import android.os.SystemClock;
import c.a.ag;
import c.g.b.k;
import com.truecaller.flashsdk.models.QueuedFlash;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimerTask;

public final class KidFlashService$f
  extends TimerTask
{
  KidFlashService$f(KidFlashService paramKidFlashService) {}
  
  public final void run()
  {
    Object localObject1 = KidFlashService.a(a);
    Object localObject2 = "receiver$0";
    k.b(localObject1, (String)localObject2);
    int i = ((Map)localObject1).size();
    switch (i)
    {
    default: 
      k.b(localObject1, "receiver$0");
      localObject2 = new java/util/LinkedHashMap;
      ((LinkedHashMap)localObject2).<init>((Map)localObject1);
      localObject1 = localObject2;
      localObject1 = (Map)localObject2;
      break;
    case 1: 
      localObject1 = ag.a((Map)localObject1);
      break;
    case 0: 
      localObject1 = ag.a();
    }
    localObject1 = ((Map)localObject1).entrySet().iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject1).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      long l1 = SystemClock.elapsedRealtime();
      Object localObject3 = (QueuedFlash)((Map.Entry)localObject2).getValue();
      long l2 = ((QueuedFlash)localObject3).g();
      l1 -= l2;
      l2 = SystemClock.elapsedRealtime();
      QueuedFlash localQueuedFlash = (QueuedFlash)((Map.Entry)localObject2).getValue();
      long l3 = localQueuedFlash.g();
      l2 -= l3;
      l3 = 60000L;
      boolean bool2 = l2 < l3;
      if (!bool2)
      {
        KidFlashService localKidFlashService = a;
        String str = (String)((Map.Entry)localObject2).getKey();
        localObject2 = (QueuedFlash)((Map.Entry)localObject2).getValue();
        KidFlashService.a(localKidFlashService, str, (QueuedFlash)localObject2);
      }
      else
      {
        localObject3 = a;
        localObject2 = (QueuedFlash)((Map.Entry)localObject2).getValue();
        l3 -= l1;
        KidFlashService.a((KidFlashService)localObject3, (QueuedFlash)localObject2, l3);
      }
    }
    localObject1 = KidFlashService.a(a);
    boolean bool3 = ((Map)localObject1).isEmpty();
    if (bool3)
    {
      KidFlashService.b(a);
      localObject1 = a;
      KidFlashService.c((KidFlashService)localObject1);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.KidFlashService.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */