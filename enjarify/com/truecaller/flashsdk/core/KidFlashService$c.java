package com.truecaller.flashsdk.core;

import android.graphics.Bitmap;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.QueuedFlash;
import java.util.Map;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;

final class KidFlashService$c
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  KidFlashService$c(KidFlashService paramKidFlashService, QueuedFlash paramQueuedFlash, Flash paramFlash, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    c localc = new com/truecaller/flashsdk/core/KidFlashService$c;
    KidFlashService localKidFlashService = b;
    QueuedFlash localQueuedFlash = c;
    Flash localFlash = d;
    localc.<init>(localKidFlashService, localQueuedFlash, localFlash, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localc;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = a;
    int j = 1;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label238;
      }
      paramObject = b;
      localObject2 = (Flash)c;
      paramObject = KidFlashService.a((KidFlashService)paramObject, (Flash)localObject2);
      a = j;
      paramObject = ((ao)paramObject).a(this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Bitmap)paramObject;
    localObject1 = "final";
    Object localObject2 = d.d();
    boolean bool2 = c.g.b.k.a(localObject1, localObject2);
    if (bool2)
    {
      localObject1 = b;
      localObject2 = c;
      KidFlashService.a((KidFlashService)localObject1, (QueuedFlash)localObject2, (Bitmap)paramObject);
    }
    else
    {
      localObject1 = KidFlashService.a(b);
      int k = ((Map)localObject1).size();
      if (k == j)
      {
        localObject1 = b;
        localObject2 = c;
        KidFlashService.b((KidFlashService)localObject1, (QueuedFlash)localObject2, (Bitmap)paramObject);
      }
      else
      {
        localObject1 = b;
        localObject2 = c;
        KidFlashService.c((KidFlashService)localObject1, (QueuedFlash)localObject2, (Bitmap)paramObject);
      }
    }
    return x.a;
    label238:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (c)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((c)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.KidFlashService.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */