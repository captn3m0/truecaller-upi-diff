package com.truecaller.flashsdk.core;

import android.net.Uri;
import android.os.Bundle;
import c.g.a.m;
import c.o.b;
import com.truecaller.flashsdk.assist.q;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.FormField;
import com.truecaller.flashsdk.models.ImageFlash;
import com.truecaller.flashsdk.models.MediaUrl;
import e.r;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ax;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;
import okhttp3.ac;
import okhttp3.x.b;

public final class l
  implements k
{
  final b a;
  j b;
  final n c;
  private final c.d.f d;
  private final c.d.f e;
  private final com.truecaller.flashsdk.assist.g f;
  private final q g;
  
  public l(c.d.f paramf, n paramn, com.truecaller.flashsdk.assist.g paramg, q paramq)
  {
    e = paramf;
    c = paramn;
    f = paramg;
    g = paramq;
    paramf = c.a();
    a = paramf;
    paramf = e;
    d = paramf;
  }
  
  static r a(e.b paramb)
  {
    try
    {
      paramb = paramb.c();
    }
    catch (IOException localIOException)
    {
      paramb = null;
    }
    return paramb;
  }
  
  private final void a(Flash paramFlash, String paramString)
  {
    String str1 = paramFlash.e();
    if (str1 == null) {
      str1 = " ";
    }
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    String str2 = "flash_context";
    int i = str1.length();
    int j = 2;
    if (i > j) {
      str1 = "reply";
    } else {
      str1 = "send";
    }
    localBundle.putString(str2, str1);
    str1 = "sentFailed";
    localBundle.putString(str1, paramString);
    paramString = "flash_thread_id";
    paramFlash = paramFlash.c();
    if (paramFlash == null) {
      paramFlash = UUID.randomUUID().toString();
    }
    localBundle.putString(paramString, paramFlash);
    a.a("ANDROID_FLASH_SENT_FAILED", localBundle);
  }
  
  public final Object a(ImageFlash paramImageFlash)
  {
    try
    {
      Object localObject1 = paramImageFlash.o();
      Object localObject2 = g;
      Object localObject3 = paramImageFlash.n();
      Object localObject4 = "flash.imageUri";
      c.g.b.k.a(localObject3, (String)localObject4);
      localObject2 = ((q)localObject2).a((Uri)localObject3);
      localObject3 = "file";
      localObject4 = null;
      localObject2 = x.b.a((String)localObject3, null, (ac)localObject2);
      localObject3 = ((MediaUrl)localObject1).getFormField();
      localObject4 = "receiver$0";
      c.g.b.k.b(localObject3, (String)localObject4);
      localObject4 = new java/util/LinkedHashMap;
      ((LinkedHashMap)localObject4).<init>();
      localObject4 = (Map)localObject4;
      String str = "X-Amz-Algorithm";
      Object localObject5 = ((FormField)localObject3).getAlgorithm();
      localObject5 = com.truecaller.flashsdk.models.f.a((String)localObject5);
      ((Map)localObject4).put(str, localObject5);
      str = "Policy";
      localObject5 = ((FormField)localObject3).getPolicy();
      localObject5 = com.truecaller.flashsdk.models.f.a((String)localObject5);
      ((Map)localObject4).put(str, localObject5);
      str = "X-Amz-Signature";
      localObject5 = ((FormField)localObject3).getSignature();
      localObject5 = com.truecaller.flashsdk.models.f.a((String)localObject5);
      ((Map)localObject4).put(str, localObject5);
      str = "key";
      localObject5 = ((FormField)localObject3).getKey();
      localObject5 = com.truecaller.flashsdk.models.f.a((String)localObject5);
      ((Map)localObject4).put(str, localObject5);
      str = "acl";
      localObject5 = ((FormField)localObject3).getAccess();
      localObject5 = com.truecaller.flashsdk.models.f.a((String)localObject5);
      ((Map)localObject4).put(str, localObject5);
      str = "X-Amz-Date";
      localObject5 = ((FormField)localObject3).getDate();
      localObject5 = com.truecaller.flashsdk.models.f.a((String)localObject5);
      ((Map)localObject4).put(str, localObject5);
      str = "bucket";
      localObject5 = ((FormField)localObject3).getBucket();
      localObject5 = com.truecaller.flashsdk.models.f.a((String)localObject5);
      ((Map)localObject4).put(str, localObject5);
      str = "X-Amz-Credential";
      localObject3 = ((FormField)localObject3).getCredential();
      localObject3 = com.truecaller.flashsdk.models.f.a((String)localObject3);
      ((Map)localObject4).put(str, localObject3);
      localObject3 = c;
      localObject1 = ((MediaUrl)localObject1).getUploadUrl();
      str = "filePart";
      c.g.b.k.a(localObject2, str);
      localObject1 = ((n)localObject3).a((String)localObject1, (Map)localObject4, (x.b)localObject2);
      localObject1 = ((e.b)localObject1).c();
      localObject2 = paramImageFlash;
      localObject2 = (Flash)paramImageFlash;
      int i = ((r)localObject1).b();
      localObject3 = String.valueOf(i);
      a((Flash)localObject2, (String)localObject3);
      localObject2 = "result";
      c.g.b.k.a(localObject1, (String)localObject2);
      boolean bool = ((r)localObject1).d();
      return Boolean.valueOf(bool);
    }
    catch (IOException localIOException)
    {
      paramImageFlash = (Flash)paramImageFlash;
      a(paramImageFlash, "14");
    }
    return Boolean.FALSE;
  }
  
  public final Object a(ImageFlash paramImageFlash, c.d.c paramc)
  {
    boolean bool1 = paramc instanceof l.a;
    int k;
    if (bool1)
    {
      localObject1 = paramc;
      localObject1 = (l.a)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        k = b - j;
        b = k;
        break label77;
      }
    }
    Object localObject1 = new com/truecaller/flashsdk/core/l$a;
    ((l.a)localObject1).<init>(this, (c.d.c)paramc);
    label77:
    paramc = a;
    Object localObject2 = c.d.a.a.a;
    int j = b;
    boolean bool2;
    switch (j)
    {
    default: 
      paramImageFlash = new java/lang/IllegalStateException;
      paramImageFlash.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramImageFlash;
    case 1: 
      paramImageFlash = (ImageFlash)e;
      localObject1 = (l)d;
      bool2 = paramc instanceof o.b;
      if (!bool2) {
        break;
      }
    }
    try
    {
      paramc = (o.b)paramc;
      paramc = a;
      throw paramc;
    }
    catch (IOException localIOException2)
    {
      boolean bool3;
      for (;;) {}
    }
    bool3 = paramc instanceof o.b;
    if (!bool3)
    {
      try
      {
        paramc = d;
        Object localObject3 = ax.b();
        localObject3 = (c.d.f)localObject3;
        paramc = paramc.plus((c.d.f)localObject3);
        localObject3 = new com/truecaller/flashsdk/core/l$b;
        ((l.b)localObject3).<init>(this, null);
        localObject3 = (m)localObject3;
        d = this;
        e = paramImageFlash;
        int m = 1;
        b = m;
        paramc = kotlinx.coroutines.g.a(paramc, (m)localObject3, (c.d.c)localObject1);
        if (paramc == localObject2) {
          return localObject2;
        }
        localObject1 = this;
        paramc = (r)paramc;
        localObject2 = "result";
        c.g.b.k.a(paramc, (String)localObject2);
        bool2 = paramc.d();
        if (bool2) {
          return paramc.e();
        }
        localObject2 = paramImageFlash;
        localObject2 = (Flash)paramImageFlash;
        k = paramc.b();
        paramc = String.valueOf(k);
        ((l)localObject1).a((Flash)localObject2, paramc);
      }
      catch (IOException localIOException1)
      {
        localObject1 = this;
      }
      paramImageFlash = (Flash)paramImageFlash;
      paramc = "14";
      ((l)localObject1).a(paramImageFlash, paramc);
      return null;
    }
    throw a;
  }
  
  final void a(Flash paramFlash, int paramInt)
  {
    j localj = b;
    if (localj != null) {
      localj.a(paramFlash);
    }
    String str = String.valueOf(paramInt);
    a(paramFlash, str);
  }
  
  public final void a(Flash paramFlash, String paramString, boolean paramBoolean, j paramj)
  {
    c.g.b.k.b(paramFlash, "flash");
    Object localObject = "fromScreen";
    c.g.b.k.b(paramString, (String)localObject);
    b = paramj;
    paramString = f;
    boolean bool = paramString.a();
    if (!bool)
    {
      a(paramFlash, 10);
      return;
    }
    paramString = (ag)bg.a;
    paramj = d;
    localObject = new com/truecaller/flashsdk/core/l$e;
    ((l.e)localObject).<init>(this, paramFlash, paramBoolean, null);
    localObject = (m)localObject;
    e.b(paramString, paramj, (m)localObject, 2);
  }
  
  final void a(ImageFlash paramImageFlash, int paramInt, a parama)
  {
    if (parama != null) {
      parama.a(paramImageFlash);
    }
    paramImageFlash = (Flash)paramImageFlash;
    String str = String.valueOf(paramInt);
    a(paramImageFlash, str);
  }
  
  public final void a(ImageFlash paramImageFlash, String paramString, a parama)
  {
    c.g.b.k.b(paramImageFlash, "flash");
    Object localObject1 = "fromScreen";
    c.g.b.k.b(paramString, (String)localObject1);
    paramString = f;
    boolean bool = paramString.a();
    if (!bool)
    {
      a(paramImageFlash, 10, parama);
      return;
    }
    paramString = (ag)bg.a;
    localObject1 = d;
    Object localObject2 = new com/truecaller/flashsdk/core/l$f;
    ((l.f)localObject2).<init>(this, paramImageFlash, parama, true, null);
    localObject2 = (m)localObject2;
    e.b(paramString, (c.d.f)localObject1, (m)localObject2, 2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */