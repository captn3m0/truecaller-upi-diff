package com.truecaller.flashsdk.core;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import c.g.b.k;
import com.truecaller.flashsdk.models.ImageFlash;

public final class FlashMediaService$a
{
  public static Intent a(Context paramContext, ImageFlash paramImageFlash)
  {
    k.b(paramContext, "context");
    k.b(paramImageFlash, "imageFlash");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, FlashMediaService.class);
    paramImageFlash = (Parcelable)paramImageFlash;
    localIntent.putExtra("extra_image_flash", paramImageFlash);
    return localIntent;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.FlashMediaService.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */