package com.truecaller.flashsdk.core;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import c.n.m;
import c.u;
import com.google.android.gms.tasks.RuntimeExecutionException;
import com.google.gson.f;
import com.truecaller.flashsdk.R.style;
import com.truecaller.flashsdk.assist.aa;
import com.truecaller.flashsdk.core.a.a.a;
import com.truecaller.flashsdk.db.g;
import com.truecaller.flashsdk.db.h;
import com.truecaller.flashsdk.db.l;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.Payload;
import com.truecaller.flashsdk.ui.contactselector.FlashContactSelectorActivity;
import com.truecaller.flashsdk.ui.contactselector.c;
import com.truecaller.flashsdk.ui.onboarding.FlashOnBoardingActivity;
import com.truecaller.flashsdk.ui.send.SendActivity;
import com.truecaller.flashsdk.ui.send.SendActivity.a;
import com.truecaller.flashsdk.ui.whatsnew.FlashWithFriendsActivity.a;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public final class d
  implements b
{
  public h a;
  public aa b;
  public k c;
  public f d;
  public a e;
  private i f;
  private s g;
  private Theme h;
  
  public d()
  {
    Theme localTheme = Theme.LIGHT;
    h = localTheme;
  }
  
  private static String i(String paramString)
  {
    int i = paramString.length();
    int j = 7;
    if (i > j)
    {
      i = paramString.length() - j;
      j = paramString.length();
      if (paramString != null)
      {
        paramString = paramString.substring(i, j);
        c.g.b.k.a(paramString, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return paramString;
      }
      paramString = new c/u;
      paramString.<init>("null cannot be cast to non-null type java.lang.String");
      throw paramString;
    }
    return null;
  }
  
  public final Intent a(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean, String paramString5)
  {
    c.g.b.k.b(paramContext, "context");
    return FlashWithFriendsActivity.a.a(paramContext, paramString1, paramString2, paramString3, paramString4, paramBoolean, paramString5);
  }
  
  public final void a(long paramLong)
  {
    String str1 = i(String.valueOf(paramLong));
    if (str1 == null) {
      return;
    }
    aa localaa = b;
    if (localaa == null)
    {
      String str2 = "preferenceUtil";
      c.g.b.k.a(str2);
    }
    Long localLong = Long.valueOf(paramLong);
    localaa.a(str1, localLong);
  }
  
  public final void a(long paramLong, String paramString)
  {
    Object localObject1 = new com/truecaller/flashsdk/models/Payload;
    String str1 = "call_me_back";
    ((Payload)localObject1).<init>(str1, paramString, null, null);
    paramString = new com/truecaller/flashsdk/models/Flash;
    paramString.<init>();
    paramString.a(paramLong);
    paramString.a((Payload)localObject1);
    paramString.b("");
    paramString.i();
    paramString.j();
    localObject1 = c;
    if (localObject1 == null)
    {
      str1 = "flashRequestHandler";
      c.g.b.k.a(str1);
    }
    ((k)localObject1).a(paramString, "call_me_back", false, null);
    localObject1 = new android/os/Bundle;
    ((Bundle)localObject1).<init>();
    Object localObject2 = paramString.f();
    c.g.b.k.a(localObject2, "flash.payload");
    localObject2 = ((Payload)localObject2).a();
    ((Bundle)localObject1).putString("type", (String)localObject2);
    localObject2 = paramString.h();
    ((Bundle)localObject1).putString("flash_message_id", (String)localObject2);
    String str2 = String.valueOf(paramLong);
    ((Bundle)localObject1).putString("flash_receiver_id", str2);
    ((Bundle)localObject1).putString("flash_context", "callMeBack");
    ((Bundle)localObject1).putString("flash_reply_id", null);
    String str3 = paramString.c();
    ((Bundle)localObject1).putString("flash_thread_id", str3);
    ((Bundle)localObject1).putString("FlashFromHistory", "false");
    ((Bundle)localObject1).putString("history_length", "0");
    a("ANDROID_FLASH_SENT", (Bundle)localObject1);
  }
  
  public final void a(long paramLong, List paramList, String paramString)
  {
    c.g.b.k.b(paramList, "responses");
    c.g.b.k.b(paramString, "message");
    Flash localFlash = new com/truecaller/flashsdk/models/Flash;
    localFlash.<init>();
    localFlash.a(paramLong);
    localFlash.b("");
    localFlash.i();
    localFlash.j();
    Object localObject = new com/truecaller/flashsdk/models/Payload;
    String str = "custom_flash";
    ((Payload)localObject).<init>(str, paramString, paramList, null);
    localFlash.a((Payload)localObject);
    localObject = c;
    if (localObject == null)
    {
      str = "flashRequestHandler";
      c.g.b.k.a(str);
    }
    ((k)localObject).a(localFlash, "paying", false, null);
  }
  
  public final void a(Context paramContext, long paramLong, String paramString1, String paramString2)
  {
    c.g.b.k.b(paramContext, "context");
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "preferenceUtil";
      c.g.b.k.a((String)localObject2);
    }
    Object localObject2 = "first_time_user";
    boolean bool = ((aa)localObject1).c((String)localObject2);
    if (bool)
    {
      localObject1 = FlashOnBoardingActivity.b;
      c.g.b.k.b(paramContext, "context");
      localObject1 = new android/content/Intent;
      ((Intent)localObject1).<init>(paramContext, FlashOnBoardingActivity.class);
      ((Intent)localObject1).putExtra("to_phone", paramLong);
      ((Intent)localObject1).putExtra("to_name", paramString1);
      ((Intent)localObject1).putExtra("screen_context", paramString2);
      ((Intent)localObject1).addFlags(268435456);
      paramContext.startActivity((Intent)localObject1);
      return;
    }
    localObject1 = SendActivity.q;
    c.g.b.k.b(paramContext, "context");
    localObject2 = paramContext;
    Intent localIntent = SendActivity.a.a(paramContext, paramLong, paramString1, paramString2, 0, true, 16);
    paramContext.startActivity(localIntent);
  }
  
  public final void a(Context paramContext, long paramLong1, String paramString1, String paramString2, long paramLong2)
  {
    c.g.b.k.b(paramContext, "context");
    SendActivity.a.a(paramContext, paramLong1, paramString1, paramString2, paramLong2);
  }
  
  public final void a(Context paramContext, long paramLong, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, boolean paramBoolean, String paramString6)
  {
    c.g.b.k.b(paramContext, "context");
    c.g.b.k.b(paramString5, "message");
    Object localObject = SendActivity.q;
    localObject = SendActivity.a.a(paramContext, paramLong, paramString1, paramString2, paramString3, paramString4, paramString5, paramBoolean, paramString6);
    aa localaa = b;
    if (localaa == null)
    {
      str = "preferenceUtil";
      c.g.b.k.a(str);
    }
    String str = "first_time_user";
    boolean bool = localaa.c(str);
    if (bool)
    {
      localObject = FlashOnBoardingActivity.b;
      c.g.b.k.b(paramContext, "context");
      c.g.b.k.b(paramString5, "description");
      localObject = new android/content/Intent;
      ((Intent)localObject).<init>(paramContext, FlashOnBoardingActivity.class);
      ((Intent)localObject).putExtra("to_phone", paramLong);
      ((Intent)localObject).putExtra("to_name", paramString1);
      ((Intent)localObject).putExtra("image", paramString3);
      ((Intent)localObject).putExtra("video", paramString4);
      ((Intent)localObject).putExtra("description", paramString5);
      ((Intent)localObject).putExtra("mode", paramBoolean);
      ((Intent)localObject).putExtra("background", paramString6);
      ((Intent)localObject).putExtra("screen_context", paramString2);
      ((Intent)localObject).addFlags(268435456);
      paramContext.startActivity((Intent)localObject);
      return;
    }
    paramContext.startActivity((Intent)localObject);
  }
  
  public final void a(Context paramContext, ArrayList paramArrayList, String paramString)
  {
    c.g.b.k.b(paramContext, "context");
    c.g.b.k.b(paramArrayList, "contacts");
    c.g.b.k.b(paramString, "screenContext");
    Object localObject = c.b;
    paramArrayList = (List)paramArrayList;
    c.g.b.k.b(paramContext, "context");
    c.g.b.k.b(paramArrayList, "contactList");
    c.g.b.k.b(paramString, "screenContext");
    localObject = new android/content/Intent;
    ((Intent)localObject).<init>(paramContext, FlashContactSelectorActivity.class);
    ArrayList localArrayList = new java/util/ArrayList;
    paramArrayList = (Collection)paramArrayList;
    localArrayList.<init>(paramArrayList);
    ((Intent)localObject).putParcelableArrayListExtra("contact_list", localArrayList);
    ((Intent)localObject).putExtra("screen_context", paramString);
    paramContext.startActivity((Intent)localObject);
  }
  
  public final void a(Theme paramTheme)
  {
    c.g.b.k.b(paramTheme, "theme");
    h = paramTheme;
  }
  
  public final void a(i parami)
  {
    c.g.b.k.b(parami, "flashPoint");
    f = parami;
  }
  
  public final void a(s params)
  {
    c.g.b.k.b(params, "logger");
    g = params;
  }
  
  public final void a(l paraml)
  {
    c.g.b.k.b(paraml, "listener");
    h localh = a;
    if (localh == null)
    {
      String str = "flashPendingManager";
      c.g.b.k.a(str);
    }
    localh.a(paraml);
  }
  
  public final void a(l paraml, String... paramVarArgs)
  {
    c.g.b.k.b(paraml, "listener");
    c.g.b.k.b(paramVarArgs, "numbersWithoutPlus");
    h localh = a;
    if (localh == null)
    {
      String str = "flashPendingManager";
      c.g.b.k.a(str);
    }
    int i = paramVarArgs.length;
    paramVarArgs = (String[])Arrays.copyOf(paramVarArgs, i);
    localh.a(paraml, paramVarArgs);
  }
  
  public final void a(Flash paramFlash)
  {
    c.g.b.k.b(paramFlash, "flash");
    k localk = c;
    if (localk == null)
    {
      String str = "flashRequestHandler";
      c.g.b.k.a(str);
    }
    localk.a(paramFlash, "responding", true, null);
  }
  
  public final void a(String paramString, long paramLong, Flash paramFlash)
  {
    c.g.b.k.b(paramString, "phone");
    c.g.b.k.b(paramFlash, "flash");
    h localh = a;
    if (localh == null)
    {
      String str = "flashPendingManager";
      c.g.b.k.a(str);
    }
    localh.a(paramString, paramLong, paramFlash);
  }
  
  public final void a(String paramString, Bundle paramBundle)
  {
    c.g.b.k.b(paramString, "key");
    c.g.b.k.b(paramBundle, "values");
    s locals = g;
    if (locals != null)
    {
      locals.a(paramString, paramBundle);
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "phoneWithoutPlus");
    c.g.b.k.b(paramString2, "name");
    i locali = f;
    if (locali != null)
    {
      locali.a(4, paramString1, paramString2);
      return;
    }
  }
  
  public final void a(List paramList)
  {
    c.g.b.k.b(paramList, "statusList");
    h localh = a;
    if (localh == null)
    {
      String str = "flashPendingManager";
      c.g.b.k.a(str);
    }
    localh.a(paramList);
  }
  
  public final boolean a()
  {
    i locali = f;
    if (locali != null) {
      return locali.b(null);
    }
    return false;
  }
  
  public final boolean a(String paramString)
  {
    String str = "phoneWithoutPlus";
    c.g.b.k.b(paramString, str);
    paramString = g(paramString);
    long l1 = b;
    long l2 = System.currentTimeMillis() - l1;
    l1 = 60000L;
    boolean bool = l2 < l1;
    return !bool;
  }
  
  public final void b()
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "preferenceUtil";
      c.g.b.k.a((String)localObject2);
    }
    Object localObject2 = "flash_settings_version";
    int i = -1;
    int j = ((aa)localObject1).a((String)localObject2, i);
    int k = 1;
    if (j <= 0)
    {
      j = 1;
    }
    else
    {
      j = 0;
      localObject1 = null;
    }
    if (j == 0) {
      return;
    }
    localObject1 = f;
    if (localObject1 != null)
    {
      localObject1 = ((i)localObject1).A();
    }
    else
    {
      j = 0;
      localObject1 = null;
    }
    d((String)localObject1);
    localObject1 = b;
    if (localObject1 == null)
    {
      String str = "preferenceUtil";
      c.g.b.k.a(str);
    }
    localObject2 = Integer.valueOf(k);
    ((aa)localObject1).a("flash_settings_version", localObject2);
  }
  
  public final void b(long paramLong)
  {
    String str1 = i(String.valueOf(paramLong));
    if (str1 == null) {
      return;
    }
    aa localaa = b;
    if (localaa == null)
    {
      String str2 = "preferenceUtil";
      c.g.b.k.a(str2);
    }
    localaa.a(str1);
  }
  
  public final void b(long paramLong, String paramString)
  {
    Flash localFlash = new com/truecaller/flashsdk/models/Flash;
    localFlash.<init>();
    localFlash.a(paramLong);
    localFlash.b("");
    localFlash.a("final");
    localFlash.i();
    localFlash.j();
    Object localObject = new com/truecaller/flashsdk/models/Payload;
    String str = "call_me_back";
    ((Payload)localObject).<init>(str, paramString, null, null);
    localFlash.a((Payload)localObject);
    localObject = c;
    if (localObject == null)
    {
      str = "flashRequestHandler";
      c.g.b.k.a(str);
    }
    ((k)localObject).a(localFlash, "call_me_back_req", false, null);
  }
  
  public final void b(Context paramContext, long paramLong, String paramString1, String paramString2)
  {
    c.g.b.k.b(paramContext, "context");
    Object localObject = String.valueOf(paramLong);
    localObject = g((String)localObject);
    long l1 = b;
    long l2 = System.currentTimeMillis() - l1;
    l1 = 60000L;
    boolean bool = l2 < l1;
    if (!bool) {
      bool = true;
    } else {
      bool = false;
    }
    if (bool)
    {
      a(paramContext, paramLong, paramString1, paramString2);
      return;
    }
    long l3 = l1 - l2;
    l2 = paramLong;
    a(paramContext, paramLong, paramString1, paramString2, l3);
  }
  
  public final void b(String paramString)
  {
    c.g.b.k.b(paramString, "phone");
    i locali = f;
    if (locali != null)
    {
      locali.a(5, paramString, null);
      return;
    }
  }
  
  public final boolean c()
  {
    aa localaa = b;
    if (localaa == null)
    {
      String str = "preferenceUtil";
      c.g.b.k.a(str);
    }
    return localaa.a();
  }
  
  public final boolean c(String paramString)
  {
    c.g.b.k.b(paramString, "phone");
    i locali = f;
    if (locali != null) {
      return locali.a(paramString);
    }
    return false;
  }
  
  public final Uri d()
  {
    Object localObject1 = f;
    if (localObject1 != null)
    {
      localObject1 = b;
      if (localObject1 == null)
      {
        localObject2 = "preferenceUtil";
        c.g.b.k.a((String)localObject2);
      }
      boolean bool1 = ((aa)localObject1).a();
      localObject2 = null;
      if (bool1)
      {
        localObject1 = b;
        if (localObject1 == null)
        {
          localObject3 = "preferenceUtil";
          c.g.b.k.a((String)localObject3);
        }
        localObject1 = ((aa)localObject1).b();
      }
      else
      {
        localObject1 = f;
        if (localObject1 != null)
        {
          localObject1 = ((i)localObject1).A();
        }
        else
        {
          bool1 = false;
          localObject1 = null;
        }
      }
      Object localObject3 = localObject1;
      localObject3 = (CharSequence)localObject1;
      boolean bool2 = TextUtils.isEmpty((CharSequence)localObject3);
      if (bool2)
      {
        localObject1 = f;
        if (localObject1 != null) {
          localObject2 = ((i)localObject1).A();
        }
        localObject1 = localObject2;
      }
      d((String)localObject1);
      localObject1 = Uri.parse((String)localObject1);
      c.g.b.k.a(localObject1, "Uri.parse(flashRingtone)");
      return (Uri)localObject1;
    }
    localObject1 = new com/google/android/gms/tasks/RuntimeExecutionException;
    Object localObject2 = new java/lang/Throwable;
    ((Throwable)localObject2).<init>("FlashPoint not set");
    ((RuntimeExecutionException)localObject1).<init>((Throwable)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public final void d(String paramString)
  {
    aa localaa = b;
    if (localaa == null)
    {
      String str = "preferenceUtil";
      c.g.b.k.a(str);
    }
    localaa.d(paramString);
  }
  
  public final long e(String paramString)
  {
    Object localObject = "phoneNumber";
    c.g.b.k.b(paramString, (String)localObject);
    paramString = i(paramString);
    if (paramString == null) {
      return -1;
    }
    localObject = b;
    if (localObject == null)
    {
      String str = "preferenceUtil";
      c.g.b.k.a(str);
    }
    return ((aa)localObject).b(paramString);
  }
  
  public final i e()
  {
    return f;
  }
  
  public final int f(String paramString)
  {
    c.g.b.k.b(paramString, "numberWithPlus");
    h localh = a;
    if (localh == null)
    {
      String str = "flashPendingManager";
      c.g.b.k.a(str);
    }
    paramString = m.a(paramString, "+", "");
    return bb;
  }
  
  public final Theme f()
  {
    return h;
  }
  
  public final int g()
  {
    Theme localTheme = h;
    int[] arrayOfInt = e.a;
    int i = localTheme.ordinal();
    i = arrayOfInt[i];
    switch (i)
    {
    default: 
      return R.style.DefaultV2;
    case 6: 
      return R.style.GeneGrayV2;
    case 5: 
      return R.style.BlackPantherV2;
    case 4: 
      return R.style.RamadanV2;
    case 3: 
      return R.style.FilterCoffeeV2;
    case 2: 
      return R.style.DefaultV2;
    }
    return R.style.DarkKnightV2;
  }
  
  public final g g(String paramString)
  {
    c.g.b.k.b(paramString, "phone");
    h localh = a;
    if (localh == null)
    {
      String str = "flashPendingManager";
      c.g.b.k.a(str);
    }
    return localh.a(paramString);
  }
  
  public final com.truecaller.flashsdk.models.e h(String paramString)
  {
    c.g.b.k.b(paramString, "phoneWithoutPlus");
    h localh = a;
    if (localh == null)
    {
      String str = "flashPendingManager";
      c.g.b.k.a(str);
    }
    return localh.b(paramString);
  }
  
  public final boolean h()
  {
    aa localaa1 = b;
    if (localaa1 == null)
    {
      String str1 = "preferenceUtil";
      c.g.b.k.a(str1);
    }
    long l1 = localaa1.d();
    aa localaa2 = b;
    if (localaa2 == null)
    {
      String str2 = "preferenceUtil";
      c.g.b.k.a(str2);
    }
    long l2 = localaa2.f();
    long l3 = 1L;
    boolean bool1 = l1 < l3;
    if (!bool1)
    {
      boolean bool2 = l2 < l3;
      if (!bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final void i()
  {
    aa localaa = b;
    if (localaa == null)
    {
      String str = "preferenceUtil";
      c.g.b.k.a(str);
    }
    localaa.e();
  }
  
  public final void j()
  {
    aa localaa = b;
    if (localaa == null)
    {
      String str = "preferenceUtil";
      c.g.b.k.a(str);
    }
    localaa.i();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */