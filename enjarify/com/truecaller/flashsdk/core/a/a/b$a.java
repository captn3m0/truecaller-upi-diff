package com.truecaller.flashsdk.core.a.a;

import com.truecaller.flashsdk.core.a.b.v;
import dagger.a.g;

public final class b$a
{
  private v a;
  private com.truecaller.flashsdk.core.a.b.a b;
  private com.truecaller.flashsdk.core.a.b.t c;
  private com.truecaller.utils.t d;
  private com.truecaller.common.a e;
  
  public final a a()
  {
    Object localObject = a;
    if (localObject == null)
    {
      localObject = new com/truecaller/flashsdk/core/a/b/v;
      ((v)localObject).<init>();
      a = ((v)localObject);
    }
    localObject = b;
    if (localObject == null)
    {
      localObject = new com/truecaller/flashsdk/core/a/b/a;
      ((com.truecaller.flashsdk.core.a.b.a)localObject).<init>();
      b = ((com.truecaller.flashsdk.core.a.b.a)localObject);
    }
    localObject = c;
    if (localObject == null)
    {
      localObject = new com/truecaller/flashsdk/core/a/b/t;
      ((com.truecaller.flashsdk.core.a.b.t)localObject).<init>();
      c = ((com.truecaller.flashsdk.core.a.b.t)localObject);
    }
    g.a(d, com.truecaller.utils.t.class);
    g.a(e, com.truecaller.common.a.class);
    localObject = new com/truecaller/flashsdk/core/a/a/b;
    v localv = a;
    com.truecaller.flashsdk.core.a.b.a locala = b;
    com.truecaller.flashsdk.core.a.b.t localt = c;
    com.truecaller.utils.t localt1 = d;
    com.truecaller.common.a locala1 = e;
    ((b)localObject).<init>(localv, locala, localt, localt1, locala1, (byte)0);
    return (a)localObject;
  }
  
  public final a a(com.truecaller.common.a parama)
  {
    parama = (com.truecaller.common.a)g.a(parama);
    e = parama;
    return this;
  }
  
  public final a a(com.truecaller.flashsdk.core.a.b.a parama)
  {
    parama = (com.truecaller.flashsdk.core.a.b.a)g.a(parama);
    b = parama;
    return this;
  }
  
  public final a a(com.truecaller.utils.t paramt)
  {
    paramt = (com.truecaller.utils.t)g.a(paramt);
    d = paramt;
    return this;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.a.a.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */