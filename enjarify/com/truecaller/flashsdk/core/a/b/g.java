package com.truecaller.flashsdk.core.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class g
  implements d
{
  private final a a;
  private final Provider b;
  
  private g(a parama, Provider paramProvider)
  {
    a = parama;
    b = paramProvider;
  }
  
  public static g a(a parama, Provider paramProvider)
  {
    g localg = new com/truecaller/flashsdk/core/a/b/g;
    localg.<init>(parama, paramProvider);
    return localg;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.a.b.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */