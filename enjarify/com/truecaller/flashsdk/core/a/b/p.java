package com.truecaller.flashsdk.core.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class p
  implements d
{
  private final a a;
  private final Provider b;
  
  private p(a parama, Provider paramProvider)
  {
    a = parama;
    b = paramProvider;
  }
  
  public static p a(a parama, Provider paramProvider)
  {
    p localp = new com/truecaller/flashsdk/core/a/b/p;
    localp.<init>(parama, paramProvider);
    return localp;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.a.b.p
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */