package com.truecaller.flashsdk.core.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class w
  implements d
{
  private final v a;
  private final Provider b;
  private final Provider c;
  
  private w(v paramv, Provider paramProvider1, Provider paramProvider2)
  {
    a = paramv;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static w a(v paramv, Provider paramProvider1, Provider paramProvider2)
  {
    w localw = new com/truecaller/flashsdk/core/a/b/w;
    localw.<init>(paramv, paramProvider1, paramProvider2);
    return localw;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.a.b.w
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */