package com.truecaller.flashsdk.core.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class u
  implements d
{
  private final t a;
  private final Provider b;
  
  private u(t paramt, Provider paramProvider)
  {
    a = paramt;
    b = paramProvider;
  }
  
  public static u a(t paramt, Provider paramProvider)
  {
    u localu = new com/truecaller/flashsdk/core/a/b/u;
    localu.<init>(paramt, paramProvider);
    return localu;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.a.b.u
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */