package com.truecaller.flashsdk.core.a.b;

import dagger.a.d;
import javax.inject.Provider;

public final class q
  implements d
{
  private final a a;
  private final Provider b;
  private final Provider c;
  
  private q(a parama, Provider paramProvider1, Provider paramProvider2)
  {
    a = parama;
    b = paramProvider1;
    c = paramProvider2;
  }
  
  public static q a(a parama, Provider paramProvider1, Provider paramProvider2)
  {
    q localq = new com/truecaller/flashsdk/core/a/b/q;
    localq.<init>(parama, paramProvider1, paramProvider2);
    return localq;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.a.b.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */