package com.truecaller.flashsdk.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import c.g.b.k;

public final class KidFlashService$a
  extends BroadcastReceiver
{
  KidFlashService$a(KidFlashService paramKidFlashService) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    String str = "context";
    k.b(paramContext, str);
    k.b(paramIntent, "intent");
    paramContext = paramIntent.getAction();
    if (paramContext == null) {
      return;
    }
    int i = paramContext.hashCode();
    boolean bool;
    switch (i)
    {
    default: 
      break;
    case 781901877: 
      paramIntent = "type_stop_ringer";
      bool = paramContext.equals(paramIntent);
      if (bool)
      {
        KidFlashService.c(a);
        return;
      }
      break;
    case 24379781: 
      str = "type_stop_progress";
      bool = paramContext.equals(str);
      if (bool)
      {
        paramContext = a;
        paramIntent = paramIntent.getExtras();
        k.a(paramIntent, "intent.extras");
        KidFlashService.a(paramContext, paramIntent);
        return;
      }
      break;
    case -214270734: 
      str = "type_flash_minimized";
      bool = paramContext.equals(str);
      if (bool)
      {
        paramContext = a;
        paramIntent = paramIntent.getExtras();
        k.a(paramIntent, "intent.extras");
        KidFlashService.c(paramContext, paramIntent);
        return;
      }
      break;
    case -1510496486: 
      str = "type_flash_active";
      bool = paramContext.equals(str);
      if (bool)
      {
        paramContext = a;
        paramIntent = paramIntent.getExtras();
        str = "intent.extras";
        k.a(paramIntent, str);
        KidFlashService.d(paramContext, paramIntent);
      }
      break;
    case -1619416219: 
      str = "type_flash_replied";
      bool = paramContext.equals(str);
      if (bool)
      {
        paramContext = a;
        paramIntent = paramIntent.getExtras();
        k.a(paramIntent, "intent.extras");
        KidFlashService.b(paramContext, paramIntent);
        return;
      }
      break;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.KidFlashService.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */