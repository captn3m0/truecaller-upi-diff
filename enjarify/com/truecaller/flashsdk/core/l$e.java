package com.truecaller.flashsdk.core;

import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.FlashRequest;
import e.r;
import java.io.IOException;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;

final class l$e
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  long c;
  int d;
  private ag h;
  
  l$e(l paraml, Flash paramFlash, boolean paramBoolean, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    e locale = new com/truecaller/flashsdk/core/l$e;
    l locall = e;
    Flash localFlash = f;
    boolean bool = g;
    locale.<init>(locall, localFlash, bool, paramc);
    paramObject = (ag)paramObject;
    h = ((ag)paramObject);
    return locale;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = d;
    int m = 14;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1) {
        break;
      }
    }
    try
    {
      paramObject = (o.b)paramObject;
      paramObject = a;
      throw ((Throwable)paramObject);
    }
    catch (IOException localIOException)
    {
      int j;
      Object localObject2;
      long l1;
      int k;
      Object localObject3;
      Object localObject4;
      boolean bool2;
      Object localObject5;
      Object localObject6;
      long l2;
      Flash localFlash;
      int n;
      paramObject = e;
      localObject1 = f;
      ((l)paramObject).a((Flash)localObject1, m);
    }
    j = paramObject instanceof o.b;
    if (j != 0)
    {
      paramObject = (o.b)paramObject;
      paramObject = a;
      throw ((Throwable)paramObject);
      j = paramObject instanceof o.b;
      if (j != 0) {
        break label533;
      }
      paramObject = e;
      localObject2 = f;
      l1 = ((Flash)localObject2).b();
      localObject2 = String.valueOf(l1);
      paramObject = l.a((l)paramObject, (String)localObject2);
      j = 1;
      d = j;
      paramObject = ((ao)paramObject).a(this);
      if (paramObject == localObject1) {
        return localObject1;
      }
    }
    paramObject = (String)paramObject;
    if (paramObject == null)
    {
      paramObject = e;
      localObject1 = f;
      k = 11;
      ((l)paramObject).a((Flash)localObject1, k);
      return x.a;
    }
    localObject2 = f;
    localObject2 = ((Flash)localObject2).c((String)paramObject);
    localObject3 = "flash.toFlashRequest(pushToken)";
    c.g.b.k.a(localObject2, (String)localObject3);
    localObject3 = e;
    localObject4 = f;
    l.a((l)localObject3, (Flash)localObject4);
    bool2 = g;
    if (bool2)
    {
      localObject3 = e;
      localObject4 = f;
      l.b((l)localObject3, (Flash)localObject4);
    }
    localObject3 = f;
    localObject3 = ((Flash)localObject3).d();
    localObject4 = "final";
    bool2 = c.g.b.k.a(localObject3, localObject4);
    if (bool2) {
      l1 = 0L;
    } else {
      l1 = System.currentTimeMillis();
    }
    localObject5 = e;
    localObject5 = a;
    localObject6 = f;
    l2 = ((Flash)localObject6).b();
    localObject6 = String.valueOf(l2);
    localFlash = f;
    ((b)localObject5).a((String)localObject6, l1, localFlash);
    localObject5 = e;
    localObject5 = l.a((l)localObject5, (FlashRequest)localObject2);
    a = paramObject;
    b = localObject2;
    c = l1;
    n = 2;
    d = n;
    paramObject = ((ao)localObject5).a(this);
    if (paramObject == localObject1) {
      return localObject1;
    }
    paramObject = (r)paramObject;
    if (paramObject == null)
    {
      paramObject = e;
      localObject1 = f;
      ((l)paramObject).a((Flash)localObject1, m);
      return x.a;
    }
    localObject1 = e;
    localObject2 = f;
    bool2 = ((r)paramObject).d();
    if (bool2)
    {
      paramObject = b;
      if (paramObject != null) {
        ((j)paramObject).b((Flash)localObject2);
      }
    }
    else
    {
      n = ((r)paramObject).b();
      ((l)localObject1).a((Flash)localObject2, n);
    }
    return x.a;
    label533:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (e)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((e)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.l.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */