package com.truecaller.flashsdk.core;

import c.g.b.k;
import com.truecaller.common.network.util.KnownEndpoints;
import com.truecaller.common.network.util.a;
import com.truecaller.common.network.util.h;
import com.truecaller.flashsdk.models.FlashRequest;
import e.b;
import java.util.Map;
import okhttp3.x.b;
import okhttp3.y;

public final class o
  implements n
{
  private final y a;
  
  public o(y paramy)
  {
    a = paramy;
  }
  
  public final b a()
  {
    return ((o.b)h.a(KnownEndpoints.FLASH, o.b.class)).a();
  }
  
  public final b a(FlashRequest paramFlashRequest)
  {
    k.b(paramFlashRequest, "flashRequest");
    return ((o.c)h.a(KnownEndpoints.FLASH, o.c.class)).a(paramFlashRequest);
  }
  
  public final b a(String paramString)
  {
    k.b(paramString, "phoneNumber");
    return ((o.a)h.a(KnownEndpoints.PUSHID, o.a.class)).a(paramString);
  }
  
  public final b a(String paramString, Map paramMap, x.b paramb)
  {
    k.b(paramString, "url");
    k.b(paramMap, "partMap");
    k.b(paramb, "file");
    a locala = new com/truecaller/common/network/util/a;
    locala.<init>();
    Object localObject = KnownEndpoints.FLASH;
    locala = locala.a((KnownEndpoints)localObject).a(o.b.class);
    localObject = a;
    return ((o.b)locala.a((y)localObject).b(o.b.class)).a(paramString, paramMap, paramb);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */