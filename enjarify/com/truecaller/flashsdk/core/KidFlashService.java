package com.truecaller.flashsdk.core;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.AudioAttributes;
import android.media.AudioAttributes.Builder;
import android.media.AudioFocusRequest;
import android.media.AudioFocusRequest.Builder;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcelable;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings.System;
import android.support.v4.app.z.d;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import c.d.f;
import c.g.a.m;
import c.g.b.k;
import c.u;
import com.truecaller.flashsdk.R.color;
import com.truecaller.flashsdk.R.drawable;
import com.truecaller.flashsdk.assist.aa;
import com.truecaller.flashsdk.assist.al;
import com.truecaller.flashsdk.assist.g;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.Payload;
import com.truecaller.flashsdk.models.QueuedFlash;
import com.truecaller.flashsdk.models.Sender;
import com.truecaller.flashsdk.ui.incoming.FlashActivity;
import com.truecaller.flashsdk.ui.incoming.FlashActivity.a;
import com.truecaller.log.UnmutedException.d;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.bn;

public final class KidFlashService
  extends Service
  implements AudioManager.OnAudioFocusChangeListener
{
  public f a;
  public AudioManager b;
  public com.truecaller.flashsdk.assist.d c;
  public g d;
  public al e;
  public com.truecaller.flashsdk.c.a f;
  public com.truecaller.notificationchannels.e g;
  public aa h;
  public b i;
  private MediaPlayer j;
  private Vibrator k;
  private Map l;
  private Timer m;
  private boolean n;
  private boolean o;
  private bn p;
  private int q;
  private final IntentFilter r;
  private Intent s;
  private final Intent t;
  private final Intent u;
  private AudioFocusRequest v;
  private boolean w;
  private final KidFlashService.a x;
  
  public KidFlashService()
  {
    Object localObject = new java/util/LinkedHashMap;
    ((LinkedHashMap)localObject).<init>();
    localObject = (Map)localObject;
    l = ((Map)localObject);
    localObject = new android/content/IntentFilter;
    ((IntentFilter)localObject).<init>();
    r = ((IntentFilter)localObject);
    localObject = new android/content/Intent;
    ((Intent)localObject).<init>("type_publish_progress");
    s = ((Intent)localObject);
    localObject = new android/content/Intent;
    ((Intent)localObject).<init>("type_flash_timer_expired");
    t = ((Intent)localObject);
    localObject = new android/content/Intent;
    ((Intent)localObject).<init>("type_flash_received");
    u = ((Intent)localObject);
    w = true;
    localObject = new com/truecaller/flashsdk/core/KidFlashService$a;
    ((KidFlashService.a)localObject).<init>(this);
    x = ((KidFlashService.a)localObject);
  }
  
  private final z.d a(Context paramContext)
  {
    z.d locald = new android/support/v4/app/z$d;
    Object localObject = g;
    if (localObject == null)
    {
      String str = "coreNotificationChannelProvider";
      k.a(str);
    }
    localObject = ((com.truecaller.notificationchannels.e)localObject).h();
    locald.<init>(paramContext, (String)localObject);
    return locald;
  }
  
  private final void a(QueuedFlash paramQueuedFlash)
  {
    paramQueuedFlash = paramQueuedFlash.a();
    k.a(paramQueuedFlash, "flash.sender");
    paramQueuedFlash = paramQueuedFlash.a();
    long l1 = paramQueuedFlash.longValue();
    long l2 = 1000000000L;
    l1 %= l2;
    int i1 = (int)l1;
    Object localObject = getSystemService("notification");
    if (localObject != null)
    {
      ((NotificationManager)localObject).cancel(i1);
      return;
    }
    paramQueuedFlash = new c/u;
    paramQueuedFlash.<init>("null cannot be cast to non-null type android.app.NotificationManager");
    throw paramQueuedFlash;
  }
  
  private final void a(QueuedFlash paramQueuedFlash, Bitmap paramBitmap, String paramString1, String paramString2)
  {
    Object localObject1 = paramQueuedFlash.a();
    String str1 = "flash.sender";
    k.a(localObject1, str1);
    long l1 = ((Sender)localObject1).a().longValue();
    long l2 = 1000000000L;
    l1 %= l2;
    int i1 = (int)l1;
    localObject1 = FlashActivity.r;
    localObject1 = this;
    localObject1 = (Context)this;
    boolean bool = paramQueuedFlash.q();
    Object localObject2 = FlashActivity.a.a((Context)localObject1, paramQueuedFlash, bool);
    int i2 = 134217728;
    localObject2 = PendingIntent.getActivity((Context)localObject1, i1, (Intent)localObject2, i2);
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    String str2 = "flash";
    paramQueuedFlash = (Parcelable)paramQueuedFlash;
    localIntent.putExtra(str2, paramQueuedFlash);
    localIntent.setAction("com.truecaller.flashsdk.receiver.ACTION_DISMISS");
    paramQueuedFlash = PendingIntent.getBroadcast((Context)localObject1, i1, localIntent, i2);
    z.d locald = a((Context)localObject1);
    int i3 = R.drawable.ic_stat_flash;
    locald = locald.a(i3);
    i3 = R.color.truecolor;
    int i4 = android.support.v4.content.b.c((Context)localObject1, i3);
    localObject1 = locald.f(i4);
    paramString1 = (CharSequence)paramString1;
    paramString1 = ((z.d)localObject1).a(paramString1);
    paramString2 = (CharSequence)paramString2;
    paramString1 = paramString1.b(paramString2).e();
    int i5 = 1;
    i4 = -65536;
    paramString1 = paramString1.a(i4, i5, i5);
    paramQueuedFlash = paramString1.b(paramQueuedFlash).a((PendingIntent)localObject2).a(paramBitmap);
    paramBitmap = getSystemService("notification");
    if (paramBitmap != null)
    {
      paramBitmap = (NotificationManager)paramBitmap;
      paramQueuedFlash = paramQueuedFlash.h();
      paramBitmap.notify(i1, paramQueuedFlash);
      return;
    }
    paramQueuedFlash = new c/u;
    paramQueuedFlash.<init>("null cannot be cast to non-null type android.app.NotificationManager");
    throw paramQueuedFlash;
  }
  
  private final void a(QueuedFlash paramQueuedFlash, String paramString1, String paramString2)
  {
    boolean bool1 = paramQueuedFlash.m();
    if (bool1)
    {
      Bundle localBundle = new android/os/Bundle;
      localBundle.<init>();
      Object localObject1 = paramQueuedFlash.a();
      if (localObject1 != null)
      {
        localObject1 = ((Sender)localObject1).a();
        if (localObject1 != null)
        {
          long l1 = ((Long)localObject1).longValue();
          localObject1 = String.valueOf(l1);
          if (localObject1 != null) {
            break label68;
          }
        }
      }
      localObject1 = "";
      label68:
      Object localObject2 = d;
      if (localObject2 == null)
      {
        localObject3 = "deviceUtils";
        k.a((String)localObject3);
      }
      boolean bool2 = ((g)localObject2).f();
      if (bool2)
      {
        localObject2 = c;
        if (localObject2 == null)
        {
          localObject3 = "contactUtils";
          k.a((String)localObject3);
        }
        bool3 = ((com.truecaller.flashsdk.assist.d)localObject2).a((String)localObject1);
      }
      else
      {
        bool3 = false;
        localObject1 = null;
      }
      Object localObject3 = paramQueuedFlash.f();
      String str = "flash.payload";
      k.a(localObject3, str);
      localObject3 = ((Payload)localObject3).a();
      localBundle.putString("type", (String)localObject3);
      localObject3 = paramQueuedFlash.h();
      localBundle.putString("flash_message_id", (String)localObject3);
      localObject2 = "flash_sender_id";
      localObject3 = paramQueuedFlash.a();
      if (localObject3 != null)
      {
        localObject3 = ((Sender)localObject3).a();
        if (localObject3 != null)
        {
          long l2 = ((Long)localObject3).longValue();
          localObject3 = String.valueOf(l2);
          break label242;
        }
      }
      localObject3 = null;
      label242:
      localBundle.putString((String)localObject2, (String)localObject3);
      paramQueuedFlash = paramQueuedFlash.c();
      localBundle.putString("flash_thread_id", paramQueuedFlash);
      localBundle.putBoolean("flash_from_phonebook", bool3);
      localObject1 = paramString2;
      localObject1 = (CharSequence)paramString2;
      localObject2 = (CharSequence)"ANDROID_FLASH_MISSED";
      boolean bool3 = TextUtils.equals((CharSequence)localObject1, (CharSequence)localObject2);
      localBundle.putBoolean("flash_missed", bool3);
      localBundle.putString("flash_action_name", paramString2);
      paramQueuedFlash = i;
      if (paramQueuedFlash == null)
      {
        paramString2 = "flashManager";
        k.a(paramString2);
      }
      paramQueuedFlash.a(paramString1, localBundle);
    }
  }
  
  private final void a(String paramString)
  {
    if (paramString == null) {
      return;
    }
    l.remove(paramString);
  }
  
  private final void a(boolean paramBoolean)
  {
    try
    {
      MediaPlayer localMediaPlayer = j;
      if (localMediaPlayer == null) {
        return;
      }
      boolean bool = n;
      if (bool)
      {
        bool = localMediaPlayer.isPlaying();
        if (bool)
        {
          float f2;
          if (paramBoolean)
          {
            paramBoolean = 1045220557;
            float f1 = 0.2F;
            int i1 = q;
            f2 = i1 * f1;
          }
          else
          {
            paramBoolean = q;
            f2 = paramBoolean;
          }
          localMediaPlayer.setVolume(f2, f2);
          return;
        }
      }
    }
    catch (IllegalStateException localIllegalStateException) {}
  }
  
  private final void b(QueuedFlash paramQueuedFlash)
  {
    paramQueuedFlash = paramQueuedFlash.h();
    a(paramQueuedFlash);
    paramQueuedFlash = l;
    boolean bool = paramQueuedFlash.isEmpty();
    if (bool)
    {
      h();
      f();
      stopSelf();
    }
  }
  
  private final void b(boolean paramBoolean)
  {
    int i1 = 3;
    Object localObject = new long[i1];
    Object tmp7_6 = localObject;
    tmp7_6[0] = 0L;
    Object tmp11_7 = tmp7_6;
    tmp11_7[1] = 100;
    tmp11_7[2] = 1000L;
    g localg = d;
    if (localg == null)
    {
      String str = "deviceUtils";
      k.a(str);
    }
    boolean bool1 = localg.c();
    boolean bool2 = true;
    int i2 = -1;
    Vibrator localVibrator;
    if (bool1 == bool2)
    {
      if (paramBoolean)
      {
        localVibrator = k;
        if (localVibrator != null)
        {
          localObject = VibrationEffect.createWaveform((long[])localObject, 0);
          localVibrator.vibrate((VibrationEffect)localObject);
        }
        return;
      }
      localVibrator = k;
      if (localVibrator != null)
      {
        localObject = VibrationEffect.createWaveform((long[])localObject, i2);
        localVibrator.vibrate((VibrationEffect)localObject);
        return;
      }
      return;
    }
    if (!bool1)
    {
      if (paramBoolean)
      {
        localVibrator = k;
        if (localVibrator != null) {
          localVibrator.vibrate((long[])localObject, 0);
        }
        return;
      }
      localVibrator = k;
      if (localVibrator != null)
      {
        localVibrator.vibrate((long[])localObject, i2);
        return;
      }
    }
  }
  
  private final void c(QueuedFlash paramQueuedFlash)
  {
    KidFlashService localKidFlashService = this;
    localKidFlashService = (KidFlashService)this;
    ag localag = (ag)bg.a;
    f localf = d();
    Object localObject = new com/truecaller/flashsdk/core/KidFlashService$e;
    ((KidFlashService.e)localObject).<init>(this, paramQueuedFlash, localKidFlashService, null);
    localObject = (m)localObject;
    kotlinx.coroutines.e.b(localag, localf, (m)localObject, 2);
  }
  
  private final f d()
  {
    f localf = a;
    if (localf == null)
    {
      String str = "uiContext";
      k.a(str);
    }
    return localf;
  }
  
  private final void d(QueuedFlash paramQueuedFlash)
  {
    Object localObject1 = i;
    if (localObject1 == null)
    {
      localObject2 = "flashManager";
      k.a((String)localObject2);
    }
    Object localObject2 = paramQueuedFlash.a();
    k.a(localObject2, "flash.sender");
    localObject2 = String.valueOf(((Sender)localObject2).a().longValue());
    Object localObject3 = paramQueuedFlash;
    localObject3 = (Flash)paramQueuedFlash;
    ((b)localObject1).a((String)localObject2, 0L, (Flash)localObject3);
    localObject1 = FlashActivity.r;
    localObject1 = this;
    paramQueuedFlash = FlashActivity.a.a((Context)this, paramQueuedFlash, true);
    startActivity(paramQueuedFlash);
  }
  
  private final void e()
  {
    Object localObject1 = getSystemService("vibrator");
    if (localObject1 != null)
    {
      localObject1 = (Vibrator)localObject1;
      k = ((Vibrator)localObject1);
      localObject1 = b;
      Object localObject3;
      if (localObject1 == null)
      {
        localObject3 = "audioManager";
        k.a((String)localObject3);
      }
      int i1 = ((AudioManager)localObject1).getRingerMode();
      boolean bool1;
      switch (i1)
      {
      default: 
        break;
      case 2: 
        localObject1 = getApplicationContext();
        k.a(localObject1, "this.applicationContext");
        localObject1 = ((Context)localObject1).getContentResolver();
        localObject3 = "vibrate_when_ringing";
        Object localObject4 = null;
        i1 = Settings.System.getInt((ContentResolver)localObject1, (String)localObject3, 0);
        int i2 = 1;
        if (i1 == i2)
        {
          bool1 = w;
          b(bool1);
        }
        try
        {
          bool1 = w;
          int i3 = 2;
          if (bool1)
          {
            localObject1 = c.a();
            localObject1 = ((b)localObject1).d();
          }
          else
          {
            localObject1 = RingtoneManager.getDefaultUri(i3);
          }
          Object localObject5 = d;
          if (localObject5 == null)
          {
            localObject6 = "deviceUtils";
            k.a((String)localObject6);
          }
          boolean bool2 = ((g)localObject5).c();
          int i5 = 3;
          int i6 = 10;
          Object localObject7;
          int i4;
          if (bool2)
          {
            localObject5 = new android/media/AudioAttributes$Builder;
            ((AudioAttributes.Builder)localObject5).<init>();
            localObject5 = ((AudioAttributes.Builder)localObject5).setUsage(i6);
            localObject5 = ((AudioAttributes.Builder)localObject5).setContentType(0);
            localObject5 = ((AudioAttributes.Builder)localObject5).build();
            localObject7 = new android/media/AudioFocusRequest$Builder;
            ((AudioFocusRequest.Builder)localObject7).<init>(i5);
            localObject5 = ((AudioFocusRequest.Builder)localObject7).setAudioAttributes((AudioAttributes)localObject5);
            localObject5 = ((AudioFocusRequest.Builder)localObject5).setAcceptsDelayedFocusGain(false);
            localObject5 = ((AudioFocusRequest.Builder)localObject5).setWillPauseWhenDucked(false);
            localObject6 = this;
            localObject6 = (AudioManager.OnAudioFocusChangeListener)this;
            localObject7 = new android/os/Handler;
            ((Handler)localObject7).<init>();
            localObject5 = ((AudioFocusRequest.Builder)localObject5).setOnAudioFocusChangeListener((AudioManager.OnAudioFocusChangeListener)localObject6, (Handler)localObject7);
            localObject5 = ((AudioFocusRequest.Builder)localObject5).build();
            v = ((AudioFocusRequest)localObject5);
            localObject5 = b;
            if (localObject5 == null)
            {
              localObject6 = "audioManager";
              k.a((String)localObject6);
            }
            localObject6 = v;
            i4 = ((AudioManager)localObject5).requestAudioFocus((AudioFocusRequest)localObject6);
          }
          else
          {
            localObject5 = b;
            if (localObject5 == null)
            {
              localObject7 = "audioManager";
              k.a((String)localObject7);
            }
            localObject7 = this;
            localObject7 = (AudioManager.OnAudioFocusChangeListener)this;
            i4 = ((AudioManager)localObject5).requestAudioFocus((AudioManager.OnAudioFocusChangeListener)localObject7, i5, i2);
          }
          Object localObject6 = new android/media/MediaPlayer;
          ((MediaPlayer)localObject6).<init>();
          j = ((MediaPlayer)localObject6);
          localObject6 = b;
          if (localObject6 == null)
          {
            localObject7 = "audioManager";
            k.a((String)localObject7);
          }
          i5 = ((AudioManager)localObject6).getStreamVolume(i3);
          q = i5;
          i5 = q;
          if ((i5 != 0) && (i4 == i2))
          {
            localObject5 = j;
            if (localObject5 != null)
            {
              localObject6 = this;
              localObject6 = (Context)this;
              ((MediaPlayer)localObject5).setDataSource((Context)localObject6, (Uri)localObject1);
            }
            localObject1 = d;
            if (localObject1 == null)
            {
              localObject5 = "deviceUtils";
              k.a((String)localObject5);
            }
            bool1 = ((g)localObject1).c();
            if (bool1)
            {
              localObject1 = new android/media/AudioAttributes$Builder;
              ((AudioAttributes.Builder)localObject1).<init>();
              localObject1 = ((AudioAttributes.Builder)localObject1).setUsage(i6);
              localObject1 = ((AudioAttributes.Builder)localObject1).setContentType(0);
              localObject1 = ((AudioAttributes.Builder)localObject1).build();
              localObject4 = j;
              if (localObject4 != null) {
                ((MediaPlayer)localObject4).setAudioAttributes((AudioAttributes)localObject1);
              }
            }
            else
            {
              localObject1 = j;
              if (localObject1 != null) {
                ((MediaPlayer)localObject1).setAudioStreamType(i3);
              }
            }
            localObject1 = j;
            if (localObject1 != null) {
              ((MediaPlayer)localObject1).setLooping(i2);
            }
            localObject1 = j;
            if (localObject1 != null) {
              ((MediaPlayer)localObject1).prepare();
            }
            localObject1 = j;
            if (localObject1 != null) {
              ((MediaPlayer)localObject1).start();
            }
            n = i2;
            return;
          }
        }
        catch (Exception localException)
        {
          localObject3 = new com/truecaller/log/UnmutedException$d;
          localObject4 = new java/lang/StringBuilder;
          String str = "Error while Ringing Flash: ";
          ((StringBuilder)localObject4).<init>(str);
          localObject2 = localException.getMessage();
          ((StringBuilder)localObject4).append((String)localObject2);
          localObject2 = ((StringBuilder)localObject4).toString();
          ((UnmutedException.d)localObject3).<init>((String)localObject2);
          localObject3 = (Throwable)localObject3;
          com.truecaller.log.d.a((Throwable)localObject3);
        }
      case 1: 
        bool1 = w;
        b(bool1);
        return;
      }
      return;
    }
    Object localObject2 = new c/u;
    ((u)localObject2).<init>("null cannot be cast to non-null type android.os.Vibrator");
    throw ((Throwable)localObject2);
  }
  
  private final void e(QueuedFlash paramQueuedFlash)
  {
    Object localObject = i;
    if (localObject == null)
    {
      String str = "flashManager";
      k.a(str);
    }
    localObject = ((b)localObject).e();
    if (localObject != null)
    {
      paramQueuedFlash = (Flash)paramQueuedFlash;
      ((i)localObject).a(paramQueuedFlash);
      return;
    }
  }
  
  private final void f()
  {
    try
    {
      boolean bool = n;
      if (bool)
      {
        localObject1 = j;
        if (localObject1 != null)
        {
          localObject1 = j;
          if (localObject1 != null) {
            ((MediaPlayer)localObject1).stop();
          }
          localObject1 = j;
          if (localObject1 != null) {
            ((MediaPlayer)localObject1).release();
          }
        }
      }
      Object localObject1 = d;
      if (localObject1 == null)
      {
        localObject2 = "deviceUtils";
        k.a((String)localObject2);
      }
      bool = ((g)localObject1).c();
      if (bool)
      {
        localObject1 = v;
        if (localObject1 != null)
        {
          localObject1 = b;
          if (localObject1 == null)
          {
            localObject2 = "audioManager";
            k.a((String)localObject2);
          }
          localObject2 = v;
          ((AudioManager)localObject1).abandonAudioFocusRequest((AudioFocusRequest)localObject2);
          break label142;
        }
      }
      localObject1 = b;
      if (localObject1 == null)
      {
        localObject2 = "audioManager";
        k.a((String)localObject2);
      }
      Object localObject2 = this;
      localObject2 = (AudioManager.OnAudioFocusChangeListener)this;
      ((AudioManager)localObject1).abandonAudioFocus((AudioManager.OnAudioFocusChangeListener)localObject2);
      label142:
      localObject1 = k;
      if (localObject1 != null) {
        ((Vibrator)localObject1).cancel();
      }
    }
    catch (IllegalStateException localIllegalStateException)
    {
      for (;;) {}
    }
    n = false;
  }
  
  private final void g()
  {
    h();
    Object localObject1 = new java/util/Timer;
    ((Timer)localObject1).<init>();
    m = ((Timer)localObject1);
    localObject1 = new com/truecaller/flashsdk/core/KidFlashService$f;
    ((KidFlashService.f)localObject1).<init>(this);
    Object localObject2 = localObject1;
    localObject2 = (TimerTask)localObject1;
    Timer localTimer = m;
    if (localTimer != null)
    {
      long l1 = p.a;
      localTimer.schedule((TimerTask)localObject2, 0L, l1);
      return;
    }
  }
  
  private final void h()
  {
    Timer localTimer = m;
    if (localTimer != null) {
      localTimer.cancel();
    }
    m = null;
  }
  
  public final com.truecaller.flashsdk.assist.d a()
  {
    com.truecaller.flashsdk.assist.d locald = c;
    if (locald == null)
    {
      String str = "contactUtils";
      k.a(str);
    }
    return locald;
  }
  
  public final g b()
  {
    g localg = d;
    if (localg == null)
    {
      String str = "deviceUtils";
      k.a(str);
    }
    return localg;
  }
  
  public final al c()
  {
    al localal = e;
    if (localal == null)
    {
      String str = "resourceProvider";
      k.a(str);
    }
    return localal;
  }
  
  public final void onAudioFocusChange(int paramInt)
  {
    try
    {
      boolean bool = n;
      if (bool)
      {
        int i1 = -3;
        int i2 = 1;
        if (paramInt == i1)
        {
          a(i2);
          return;
        }
        if (paramInt == i2)
        {
          paramInt = 0;
          localThrowable = null;
          a(false);
          return;
        }
        i1 = -1;
        if (paramInt != i1)
        {
          i1 = -2;
          if (paramInt != i1) {}
        }
        else
        {
          f();
        }
        return;
      }
    }
    catch (Exception localException)
    {
      Throwable localThrowable = (Throwable)localException;
      com.truecaller.log.d.a(localThrowable);
    }
  }
  
  public final IBinder onBind(Intent paramIntent)
  {
    k.b(paramIntent, "intent");
    return null;
  }
  
  public final void onCreate()
  {
    super.onCreate();
    Object localObject1 = c.b;
    localObject1 = c.b();
    Object localObject2 = new com/truecaller/flashsdk/core/q;
    ((q)localObject2).<init>(this);
    ((com.truecaller.flashsdk.core.a.a.a)localObject1).a((q)localObject2).a(this);
    localObject1 = c.a();
    i = ((b)localObject1);
    r.addAction("type_stop_progress");
    r.addAction("type_flash_replied");
    r.addAction("type_stop_ringer");
    r.addAction("type_flash_minimized");
    r.addAction("type_flash_active");
    localObject1 = this;
    localObject1 = android.support.v4.content.d.a((Context)this);
    localObject2 = (BroadcastReceiver)x;
    IntentFilter localIntentFilter = r;
    ((android.support.v4.content.d)localObject1).a((BroadcastReceiver)localObject2, localIntentFilter);
  }
  
  public final void onDestroy()
  {
    Object localObject = p;
    if (localObject != null) {
      ((bn)localObject).n();
    }
    localObject = this;
    localObject = android.support.v4.content.d.a((Context)this);
    BroadcastReceiver localBroadcastReceiver = (BroadcastReceiver)x;
    ((android.support.v4.content.d)localObject).a(localBroadcastReceiver);
    l.clear();
    h();
    f();
    super.onDestroy();
  }
  
  public final int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    k.b(paramIntent, "intent");
    Object localObject1 = "extra_flash";
    paramInt1 = paramIntent.hasExtra((String)localObject1);
    paramInt2 = 2;
    if (paramInt1 == 0) {
      return paramInt2;
    }
    paramIntent = paramIntent.getExtras().getParcelable("extra_flash");
    k.a(paramIntent, "flashBundle.getParcelable(EXTRA_FLASH)");
    paramIntent = (Flash)paramIntent;
    localObject1 = new com/truecaller/flashsdk/models/QueuedFlash;
    ((QueuedFlash)localObject1).<init>();
    ((QueuedFlash)localObject1).a(paramIntent);
    Object localObject2 = i;
    Object localObject4;
    if (localObject2 == null)
    {
      localObject4 = "flashManager";
      k.a((String)localObject4);
    }
    boolean bool = ((b)localObject2).a();
    if (!bool)
    {
      localObject2 = i;
      if (localObject2 == null)
      {
        localObject4 = "flashManager";
        k.a((String)localObject4);
      }
      bool = ((b)localObject2).a();
      if (!bool)
      {
        localObject2 = paramIntent.f();
        k.a(localObject2, "flash.payload");
        localObject2 = ((Payload)localObject2).a();
        k.a(localObject2, "flash.payload.type");
        localObject4 = "call_me_back";
        bool = k.a(localObject2, localObject4);
        if (!bool) {}
      }
    }
    else
    {
      localObject2 = h;
      if (localObject2 == null)
      {
        localObject4 = "preferenceUtil";
        k.a((String)localObject4);
      }
      ((aa)localObject2).g();
      localObject2 = getSystemService("phone");
      if (localObject2 == null) {
        break label470;
      }
      localObject2 = (TelephonyManager)localObject2;
      int i1 = ((TelephonyManager)localObject2).getCallState();
      if (i1 == 0)
      {
        i1 = 1;
      }
      else
      {
        i1 = 0;
        localObject2 = null;
      }
      w = i1;
      try
      {
        localObject2 = l;
        localObject4 = ((QueuedFlash)localObject1).h();
        localObject5 = "flash.instanceId";
        k.a(localObject4, (String)localObject5);
        ((Map)localObject2).put(localObject4, localObject1);
      }
      catch (IllegalStateException localIllegalStateException)
      {
        localObject4 = new com/truecaller/log/UnmutedException$d;
        localObject5 = new java/lang/StringBuilder;
        str = "Error while adding Flash to the queue ";
        ((StringBuilder)localObject5).<init>(str);
        localObject3 = localIllegalStateException.getMessage();
        ((StringBuilder)localObject5).append((String)localObject3);
        localObject3 = ((StringBuilder)localObject5).toString();
        ((UnmutedException.d)localObject4).<init>((String)localObject3);
        localObject4 = (Throwable)localObject4;
        com.truecaller.log.d.a((Throwable)localObject4);
      }
      Object localObject3 = u;
      Object localObject5 = paramIntent;
      localObject5 = (Parcelable)paramIntent;
      ((Intent)localObject3).putExtra("extra_flash", (Parcelable)localObject5);
      localObject3 = this;
      localObject3 = android.support.v4.content.d.a((Context)this);
      localObject4 = u;
      ((android.support.v4.content.d)localObject3).a((Intent)localObject4);
      localObject3 = (ag)bg.a;
      localObject4 = d();
      localObject5 = new com/truecaller/flashsdk/core/KidFlashService$c;
      String str = null;
      ((KidFlashService.c)localObject5).<init>(this, (QueuedFlash)localObject1, paramIntent, null);
      localObject5 = (m)localObject5;
      paramIntent = kotlinx.coroutines.e.b((ag)localObject3, (f)localObject4, (m)localObject5, paramInt2);
      p = paramIntent;
    }
    return paramInt2;
    label470:
    paramIntent = new c/u;
    paramIntent.<init>("null cannot be cast to non-null type android.telephony.TelephonyManager");
    throw paramIntent;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.KidFlashService
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */