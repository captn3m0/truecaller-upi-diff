package com.truecaller.flashsdk.core;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.app.z.b;
import android.support.v4.app.z.c;
import android.support.v4.app.z.d;
import android.support.v4.app.z.g;
import android.support.v4.content.b;
import c.d.a.a;
import c.d.c;
import c.d.f;
import c.g.b.z;
import c.o.b;
import c.u;
import c.x;
import com.truecaller.flashsdk.R.color;
import com.truecaller.flashsdk.R.drawable;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.assist.d;
import com.truecaller.flashsdk.assist.g;
import com.truecaller.flashsdk.models.Contact;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.Payload;
import com.truecaller.flashsdk.models.QueuedFlash;
import com.truecaller.flashsdk.models.Sender;
import com.truecaller.flashsdk.ui.send.SendActivity;
import com.truecaller.flashsdk.ui.send.SendActivity.a;
import java.util.Arrays;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;
import kotlinx.coroutines.ax;
import kotlinx.coroutines.e;

final class KidFlashService$e
  extends c.d.b.a.k
  implements c.g.a.m
{
  Object a;
  Object b;
  Object c;
  Object d;
  Object e;
  Object f;
  Object g;
  Object h;
  Object i;
  Object j;
  Object k;
  Object l;
  Object m;
  Object n;
  int o;
  int p;
  private ag u;
  
  KidFlashService$e(KidFlashService paramKidFlashService1, QueuedFlash paramQueuedFlash, KidFlashService paramKidFlashService2, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    e locale = new com/truecaller/flashsdk/core/KidFlashService$e;
    KidFlashService localKidFlashService1 = q;
    QueuedFlash localQueuedFlash = r;
    KidFlashService localKidFlashService2 = s;
    locale.<init>(localKidFlashService1, localQueuedFlash, localKidFlashService2, paramc);
    paramObject = (ag)paramObject;
    u = ((ag)paramObject);
    return locale;
  }
  
  public final Object a(Object paramObject)
  {
    e locale = this;
    Object localObject1 = paramObject;
    Object localObject2 = a.a;
    int i1 = p;
    boolean bool2;
    Object localObject3;
    Object localObject4;
    Object localObject5;
    Object localObject8;
    Object localObject9;
    Object localObject10;
    Object localObject12;
    int i4;
    Object localObject13;
    int i9;
    Object localObject19;
    Object localObject20;
    Object localObject21;
    switch (i1)
    {
    default: 
      localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)localObject1);
    case 2: 
      localObject2 = (z.d)k;
      i1 = o;
      bool2 = paramObject instanceof o.b;
      if (!bool2) {
        break label2008;
      }
      throw a;
    case 1: 
      localObject3 = (z.d)k;
      localObject4 = (Uri)j;
      localObject5 = (PendingIntent)i;
      localObject6 = (Intent)h;
      localObject7 = (PendingIntent)g;
      localObject8 = (Intent)f;
      localObject9 = (String)e;
      localObject10 = (String)d;
      localObject11 = (String)c;
      localObject12 = (Sender)b;
      i4 = o;
      localObject13 = (Contact)a;
      bool2 = paramObject instanceof o.b;
      if (!bool2)
      {
        localObject14 = localObject9;
        localObject9 = localObject6;
        localObject6 = localObject7;
        Object localObject15 = localObject12;
        localObject12 = localObject4;
        localObject4 = localObject15;
        Object localObject16 = localObject10;
        localObject10 = localObject5;
        localObject5 = localObject11;
        localObject11 = localObject16;
      }
      else
      {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label2077;
      }
      localObject1 = u;
      localObject3 = q.b();
      bool1 = ((g)localObject3).f();
      if (bool1)
      {
        localObject3 = q.a();
        localObject4 = r.a();
        localObject5 = "flash.sender";
        c.g.b.k.a(localObject4, (String)localObject5);
        l1 = ((Sender)localObject4).a().longValue();
        localObject4 = String.valueOf(l1);
        localObject3 = ((d)localObject3).b((String)localObject4);
        localObject13 = localObject3;
      }
      else
      {
        localObject13 = null;
      }
      localObject3 = r.a();
      c.g.b.k.a(localObject3, "flash.sender");
      localObject3 = ((Sender)localObject3).a();
      long l1 = ((Long)localObject3).longValue();
      long l2 = 1000000000L;
      l1 %= l2;
      i2 = (int)l1;
      localObject4 = r.a();
      if (localObject13 != null)
      {
        localObject5 = ((Contact)localObject13).getName();
        if (localObject5 != null) {}
      }
      else
      {
        c.g.b.k.a(localObject4, "sender");
        localObject5 = ((Sender)localObject4).b();
      }
      c.g.b.k.a(localObject5, "name");
      localObject6 = localObject5;
      localObject6 = (CharSequence)localObject5;
      localObject7 = (CharSequence)" ";
      i4 = 0;
      localObject17 = null;
      boolean bool3 = c.n.m.a((CharSequence)localObject6, (CharSequence)localObject7, false);
      int i6 = 6;
      if (bool3)
      {
        localObject7 = new String[] { " " };
        localObject6 = (String)c.n.m.c((CharSequence)localObject6, (String[])localObject7, false, i6).get(0);
        localObject11 = localObject6;
      }
      else
      {
        localObject11 = localObject5;
      }
      localObject6 = z.a;
      int i5 = 2;
      localObject8 = new Object[i5];
      localObject8[0] = localObject11;
      localObject9 = r.f();
      c.g.b.k.a(localObject9, "flash.payload");
      localObject9 = ((Payload)localObject9).b();
      int i7 = 1;
      localObject8[i7] = localObject9;
      localObject8 = Arrays.copyOf((Object[])localObject8, i5);
      localObject9 = String.format("%s: %s", (Object[])localObject8);
      c.g.b.k.a(localObject9, "java.lang.String.format(format, *args)");
      localObject6 = SendActivity.q;
      localObject6 = (Context)s;
      localObject7 = r.a();
      c.g.b.k.a(localObject7, "flash.sender");
      localObject7 = ((Sender)localObject7).a();
      c.g.b.k.a(localObject7, "flash.sender.phone");
      long l3 = ((Long)localObject7).longValue();
      localObject10 = r.a();
      c.g.b.k.a(localObject10, "flash.sender");
      localObject10 = ((Sender)localObject10).b();
      localObject18 = null;
      int i8 = 224;
      localObject14 = localObject9;
      localObject9 = localObject10;
      localObject10 = "notification";
      localObject12 = localObject11;
      i9 = i2;
      localObject19 = localObject2;
      localObject20 = localObject11;
      i6 = 0;
      i4 = i8;
      localObject8 = SendActivity.a.a((Context)localObject6, l3, (String)localObject9, (String)localObject10, i2, false, i8);
      localObject6 = (Context)s;
      i5 = 1073741824;
      localObject6 = PendingIntent.getActivity((Context)localObject6, i2, (Intent)localObject8, i5);
      localObject9 = new android/content/Intent;
      ((Intent)localObject9).<init>("com.truecaller.flashsdk.receiver.ACTION_CALL_PHONE");
      localObject11 = (Parcelable)r;
      ((Intent)localObject9).putExtra("flash", (Parcelable)localObject11);
      ((Intent)localObject9).putExtra("notification_id", i2);
      localObject10 = PendingIntent.getBroadcast((Context)s, i2, (Intent)localObject9, i5);
      localObject12 = RingtoneManager.getDefaultUri(2);
      localObject11 = q;
      localObject17 = (Context)s;
      localObject11 = KidFlashService.a((KidFlashService)localObject11, (Context)localObject17);
      i4 = R.drawable.ic_state_missed;
      localObject11 = ((z.d)localObject11).a(i4);
      localObject17 = (Context)s;
      i5 = R.color.truecolor;
      i5 = b.c((Context)localObject17, i5);
      localObject7 = ((z.d)localObject11).f(i5);
      localObject11 = q;
      i4 = R.string.missed_flash;
      localObject11 = (CharSequence)((KidFlashService)localObject11).getString(i4);
      localObject7 = ((z.d)localObject7).a((CharSequence)localObject11).a();
      long l4 = System.currentTimeMillis();
      localObject2 = ((z.d)localObject7).a(l4);
      localObject3 = localObject14;
      localObject3 = (CharSequence)localObject14;
      localObject2 = ((z.d)localObject2).b((CharSequence)localObject3).a((Uri)localObject12);
      i5 = R.drawable.ic_reply_call;
      localObject17 = q;
      localObject21 = localObject3;
      i2 = R.string.call_back;
      localObject3 = (CharSequence)((KidFlashService)localObject17).getString(i2);
      localObject2 = ((z.d)localObject2).a(i5, (CharSequence)localObject3, (PendingIntent)localObject10);
      i2 = R.drawable.ic_stat_flash;
      localObject7 = q;
      i4 = R.string.reply;
      localObject7 = (CharSequence)((KidFlashService)localObject7).getString(i4);
      localObject3 = ((z.d)localObject2).a(i2, (CharSequence)localObject7, (PendingIntent)localObject6).e();
      localObject2 = r.f();
      c.g.b.k.a(localObject2, "flash.payload");
      localObject2 = ((Payload)localObject2).a();
      localObject7 = "location";
      boolean bool4 = c.g.b.k.a(localObject2, localObject7);
      if (!bool4) {
        break label1819;
      }
      localObject2 = r.f();
      localObject7 = "flash.payload";
      c.g.b.k.a(localObject2, (String)localObject7);
      localObject2 = (CharSequence)((Payload)localObject2).d();
      if (localObject2 != null)
      {
        i10 = ((CharSequence)localObject2).length();
        if (i10 != 0)
        {
          i10 = 0;
          localObject2 = null;
          break label1190;
        }
      }
      int i10 = 1;
      label1190:
      if (i10 != 0) {
        break label1819;
      }
      localObject2 = r.f();
      c.g.b.k.a(localObject2, "flash.payload");
      localObject2 = ((Payload)localObject2).d();
      c.g.b.k.a(localObject2, "flash.payload.attachment");
      localObject2 = (CharSequence)localObject2;
      localObject7 = new String[] { "," };
      localObject18 = localObject6;
      i4 = 0;
      localObject17 = null;
      localObject2 = c.n.m.c((CharSequence)localObject2, (String[])localObject7, false, 6);
      int i11 = ((List)localObject2).size();
      i5 = 1;
      if (i11 <= i5) {
        break label1812;
      }
      localObject6 = r.f();
      localObject17 = new android/content/Intent;
      localObject7 = "android.intent.action.VIEW";
      Object localObject22 = localObject3;
      localObject3 = q;
      Object localObject23 = localObject12;
      i6 = R.string.geo_loc;
      Object localObject24 = localObject9;
      Object localObject25 = localObject10;
      i7 = 2;
      localObject9 = new Object[i7];
      c.g.b.k.a(localObject6, "payload");
      localObject10 = ((Payload)localObject6).d();
      localObject9[0] = localObject10;
      localObject18 = ((Payload)localObject6).d();
      i8 = 1;
      localObject9[i8] = localObject18;
      localObject3 = Uri.parse(((KidFlashService)localObject3).getString(i6, (Object[])localObject9));
      ((Intent)localObject17).<init>((String)localObject7, (Uri)localObject3);
      localObject3 = q;
      i5 = R.string.map_activity;
      localObject3 = ((KidFlashService)localObject3).getString(i5);
      ((Intent)localObject17).setPackage((String)localObject3);
      localObject3 = q.getPackageManager();
      localObject3 = ((Intent)localObject17).resolveActivity((PackageManager)localObject3);
      if (localObject3 == null)
      {
        localObject17 = new android/content/Intent;
        localObject3 = "android.intent.action.VIEW";
        localObject7 = q;
        int i12 = R.string.map_browser;
        i6 = 1;
        localObject10 = new Object[i6];
        localObject12 = ((Payload)localObject6).d();
        localObject18 = null;
        localObject10[0] = localObject12;
        localObject7 = Uri.parse(((KidFlashService)localObject7).getString(i12, (Object[])localObject10));
        ((Intent)localObject17).<init>((String)localObject3, (Uri)localObject7);
      }
      localObject3 = (Context)s;
      i5 = 1073741824;
      localObject7 = PendingIntent.getActivity((Context)localObject3, i9, (Intent)localObject17, i5);
      localObject3 = KidFlashService.d(q);
      localObject9 = (f)ax.b();
      localObject3 = ((f)localObject3).plus((f)localObject9);
      localObject9 = new com/truecaller/flashsdk/core/KidFlashService$e$a;
      ((KidFlashService.e.a)localObject9).<init>(locale, (List)localObject2, null);
      localObject9 = (c.g.a.m)localObject9;
      i7 = 2;
      localObject1 = e.a((ag)localObject1, (f)localObject3, (c.g.a.m)localObject9, i7);
      a = localObject13;
      o = i9;
      b = localObject4;
      c = localObject5;
      localObject3 = localObject20;
      d = localObject20;
      e = localObject14;
      f = localObject8;
      g = localObject7;
      localObject9 = localObject24;
      h = localObject24;
      localObject10 = localObject25;
      i = localObject25;
      localObject12 = localObject23;
      j = localObject23;
      localObject3 = localObject22;
      k = localObject22;
      l = localObject2;
      m = localObject6;
      n = localObject17;
      i10 = 1;
      p = i10;
      localObject1 = ((ao)localObject1).a(locale);
      localObject2 = localObject19;
      if (localObject1 == localObject19) {
        return localObject19;
      }
      localObject6 = localObject7;
      i4 = i9;
      localObject11 = localObject20;
    }
    localObject1 = (Bitmap)localObject1;
    Object localObject7 = new android/support/v4/app/z$b;
    ((z.b)localObject7).<init>();
    localObject1 = (z.g)((z.b)localObject7).a((Bitmap)localObject1);
    ((z.d)localObject3).a((z.g)localObject1).a((PendingIntent)localObject6);
    localObject1 = localObject3;
    int i2 = i4;
    break label1878;
    label1812:
    localObject2 = localObject19;
    break label1867;
    label1819:
    Object localObject18 = localObject6;
    localObject2 = localObject19;
    localObject1 = new android/support/v4/app/z$c;
    ((z.c)localObject1).<init>();
    Object localObject6 = localObject21;
    localObject1 = (z.g)((z.c)localObject1).b((CharSequence)localObject21);
    localObject1 = ((z.d)localObject3).a((z.g)localObject1);
    localObject6 = localObject18;
    ((z.d)localObject1).a((PendingIntent)localObject18);
    label1867:
    localObject1 = localObject3;
    i2 = i9;
    Object localObject11 = localObject20;
    label1878:
    localObject7 = q;
    Object localObject17 = (Flash)r;
    localObject7 = KidFlashService.a((KidFlashService)localObject7, (Flash)localObject17);
    a = localObject13;
    o = i2;
    b = localObject4;
    c = localObject5;
    d = localObject11;
    e = localObject14;
    f = localObject8;
    g = localObject6;
    h = localObject9;
    i = localObject10;
    j = localObject12;
    k = localObject1;
    int i3 = 2;
    p = i3;
    Object localObject14 = ((ao)localObject7).a(locale);
    if (localObject14 == localObject2) {
      return localObject2;
    }
    localObject2 = localObject1;
    localObject1 = localObject14;
    label2008:
    localObject1 = (Bitmap)localObject1;
    ((z.d)localObject2).a((Bitmap)localObject1);
    localObject1 = q;
    localObject14 = "notification";
    localObject1 = ((KidFlashService)localObject1).getSystemService((String)localObject14);
    if (localObject1 != null)
    {
      localObject1 = (NotificationManager)localObject1;
      localObject2 = ((z.d)localObject2).h();
      ((NotificationManager)localObject1).notify(i2, (Notification)localObject2);
      return x.a;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type android.app.NotificationManager");
    throw ((Throwable)localObject1);
    label2077:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (e)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((e)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.KidFlashService.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */