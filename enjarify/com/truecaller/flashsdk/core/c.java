package com.truecaller.flashsdk.core;

import android.app.Application;
import android.content.Context;
import c.f;
import c.g.b.k;
import c.g.b.t;
import c.g.b.w;
import com.truecaller.flashsdk.core.a.a.b.a;
import com.truecaller.utils.t.a;

public final class c
{
  public static final c b;
  private static final f c = c.g.a((c.g.a.a)c.a.a);
  
  static
  {
    Object localObject1 = new c.l.g[1];
    Object localObject2 = new c/g/b/u;
    c.l.b localb = w.a(c.class);
    ((c.g.b.u)localObject2).<init>(localb, "instance", "getInstance()Lcom/truecaller/flashsdk/core/FlashManager;");
    localObject2 = (c.l.g)w.a((t)localObject2);
    localObject1[0] = localObject2;
    a = (c.l.g[])localObject1;
    localObject1 = new com/truecaller/flashsdk/core/c;
    ((c)localObject1).<init>();
    b = (c)localObject1;
  }
  
  public static final b a()
  {
    return (b)c.b();
  }
  
  public static void a(Application paramApplication)
  {
    k.b(paramApplication, "application");
    Object localObject1 = a();
    if (localObject1 != null)
    {
      localObject1 = (d)localObject1;
      k.b(paramApplication, "application");
      Object localObject2 = paramApplication;
      localObject2 = ((com.truecaller.common.b.a)paramApplication).u();
      k.a(localObject2, "(application as ApplicationBase).commonGraph");
      b.a locala = com.truecaller.flashsdk.core.a.a.b.s();
      t.a locala1 = com.truecaller.utils.c.a();
      paramApplication = (Context)paramApplication;
      paramApplication = locala1.a(paramApplication).a();
      paramApplication = locala.a(paramApplication).a((com.truecaller.common.a)localObject2);
      localObject2 = new com/truecaller/flashsdk/core/a/b/a;
      ((com.truecaller.flashsdk.core.a.b.a)localObject2).<init>();
      paramApplication = paramApplication.a((com.truecaller.flashsdk.core.a.b.a)localObject2).a();
      localObject2 = "DaggerAppComponent.build…e())\n            .build()";
      k.a(paramApplication, (String)localObject2);
      e = paramApplication;
      paramApplication = e;
      if (paramApplication == null)
      {
        localObject2 = "component";
        k.a((String)localObject2);
      }
      paramApplication.a((d)localObject1);
      return;
    }
    paramApplication = new c/u;
    paramApplication.<init>("null cannot be cast to non-null type com.truecaller.flashsdk.core.FlashManagerImpl");
    throw paramApplication;
  }
  
  public static com.truecaller.flashsdk.core.a.a.a b()
  {
    Object localObject = a();
    if (localObject != null)
    {
      localObject = e;
      if (localObject == null)
      {
        String str = "component";
        k.a(str);
      }
      return (com.truecaller.flashsdk.core.a.a.a)localObject;
    }
    localObject = new c/u;
    ((c.u)localObject).<init>("null cannot be cast to non-null type com.truecaller.flashsdk.core.FlashManagerImpl");
    throw ((Throwable)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */