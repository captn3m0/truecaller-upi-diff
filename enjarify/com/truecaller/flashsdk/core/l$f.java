package com.truecaller.flashsdk.core;

import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.FlashRequest;
import com.truecaller.flashsdk.models.ImageFlash;
import e.r;
import java.io.IOException;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ao;

final class l$f
  extends c.d.b.a.k
  implements m
{
  Object a;
  Object b;
  long c;
  int d;
  private ag i;
  
  l$f(l paraml, ImageFlash paramImageFlash, a parama, boolean paramBoolean, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    f localf = new com/truecaller/flashsdk/core/l$f;
    l locall = e;
    ImageFlash localImageFlash = f;
    a locala = g;
    boolean bool = h;
    localf.<init>(locall, localImageFlash, locala, bool, paramc);
    paramObject = (ag)paramObject;
    i = ((ag)paramObject);
    return localf;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = c.d.a.a.a;
    int j = d;
    int m = 14;
    switch (j)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 2: 
      boolean bool1 = paramObject instanceof o.b;
      if (!bool1) {
        break;
      }
    }
    try
    {
      paramObject = (o.b)paramObject;
      paramObject = a;
      throw ((Throwable)paramObject);
    }
    catch (IOException localIOException)
    {
      int k;
      long l1;
      int n;
      Object localObject3;
      Object localObject4;
      boolean bool2;
      Object localObject5;
      Object localObject6;
      long l2;
      Object localObject7;
      int i1;
      boolean bool3;
      paramObject = e;
      localObject1 = f;
      Object localObject2 = g;
      ((l)paramObject).a((ImageFlash)localObject1, m, (a)localObject2);
    }
    k = paramObject instanceof o.b;
    if (k != 0)
    {
      paramObject = (o.b)paramObject;
      paramObject = a;
      throw ((Throwable)paramObject);
      k = paramObject instanceof o.b;
      if (k != 0) {
        break label585;
      }
      paramObject = e;
      localObject2 = f;
      l1 = ((ImageFlash)localObject2).b();
      localObject2 = String.valueOf(l1);
      paramObject = l.a((l)paramObject, (String)localObject2);
      k = 1;
      d = k;
      paramObject = ((ao)paramObject).a(this);
      if (paramObject == localObject1) {
        return localObject1;
      }
    }
    paramObject = (String)paramObject;
    if (paramObject == null)
    {
      paramObject = e;
      localObject1 = f;
      localObject2 = g;
      n = 11;
      ((l)paramObject).a((ImageFlash)localObject1, n, (a)localObject2);
      return x.a;
    }
    localObject2 = f;
    localObject2 = ((ImageFlash)localObject2).c((String)paramObject);
    localObject3 = "flash.toFlashRequest(pushToken)";
    c.g.b.k.a(localObject2, (String)localObject3);
    localObject3 = e;
    localObject4 = f;
    localObject4 = (Flash)localObject4;
    l.a((l)localObject3, (Flash)localObject4);
    bool2 = h;
    if (bool2)
    {
      localObject3 = e;
      localObject4 = f;
      localObject4 = (Flash)localObject4;
      l.b((l)localObject3, (Flash)localObject4);
    }
    localObject3 = f;
    localObject3 = ((ImageFlash)localObject3).d();
    localObject4 = "final";
    bool2 = c.g.b.k.a(localObject3, localObject4);
    if (bool2) {
      l1 = 0L;
    } else {
      l1 = System.currentTimeMillis();
    }
    localObject5 = e;
    localObject5 = a;
    localObject6 = f;
    l2 = ((ImageFlash)localObject6).b();
    localObject6 = String.valueOf(l2);
    localObject7 = f;
    localObject7 = (Flash)localObject7;
    ((b)localObject5).a((String)localObject6, l1, (Flash)localObject7);
    localObject5 = e;
    localObject5 = l.a((l)localObject5, (FlashRequest)localObject2);
    a = paramObject;
    b = localObject2;
    c = l1;
    i1 = 2;
    d = i1;
    paramObject = ((ao)localObject5).a(this);
    if (paramObject == localObject1) {
      return localObject1;
    }
    paramObject = (r)paramObject;
    if (paramObject == null)
    {
      paramObject = e;
      localObject1 = f;
      localObject2 = g;
      ((l)paramObject).a((ImageFlash)localObject1, m, (a)localObject2);
      return x.a;
    }
    localObject1 = e;
    localObject2 = f;
    localObject3 = g;
    bool3 = ((r)paramObject).d();
    if (bool3)
    {
      if (localObject3 != null) {
        ((a)localObject3).b((ImageFlash)localObject2);
      }
    }
    else
    {
      i1 = ((r)paramObject).b();
      ((l)localObject1).a((ImageFlash)localObject2, i1, (a)localObject3);
    }
    return x.a;
    label585:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (f)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((f)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.l.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */