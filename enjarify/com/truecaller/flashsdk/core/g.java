package com.truecaller.flashsdk.core;

import c.d.f;
import c.x;
import com.truecaller.ba;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.assist.al;
import com.truecaller.flashsdk.assist.j;
import com.truecaller.flashsdk.assist.q;
import com.truecaller.flashsdk.assist.u;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.FlashExtras;
import com.truecaller.flashsdk.models.FlashImageEntity;
import com.truecaller.flashsdk.models.ImageFlash;
import com.truecaller.flashsdk.models.MediaUrl;
import com.truecaller.flashsdk.models.Payload;
import com.truecaller.flashsdk.models.Sender;
import java.util.List;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class g
  extends ba
  implements a, f.a
{
  private final f c;
  private final f d;
  private final k e;
  private final al f;
  private final j g;
  private final q h;
  private final com.truecaller.flashsdk.c.a i;
  private final u j;
  
  public g(f paramf1, f paramf2, k paramk, al paramal, j paramj, q paramq, com.truecaller.flashsdk.c.a parama, u paramu)
  {
    super(paramf2);
    c = paramf1;
    d = paramf2;
    e = paramk;
    f = paramal;
    g = paramj;
    h = paramq;
    i = parama;
    j = paramu;
  }
  
  private final void c(ImageFlash paramImageFlash)
  {
    f.b localb = (f.b)b;
    if (localb != null) {
      localb.b(paramImageFlash);
    }
    i.d(paramImageFlash);
  }
  
  private final void d(ImageFlash paramImageFlash)
  {
    i.b(paramImageFlash);
    Object localObject1 = paramImageFlash.o();
    FlashExtras localFlashExtras = new com/truecaller/flashsdk/models/FlashExtras;
    FlashImageEntity localFlashImageEntity = new com/truecaller/flashsdk/models/FlashImageEntity;
    Object localObject2 = ((MediaUrl)localObject1).getDownloadUrl();
    localFlashImageEntity.<init>((String)localObject2, "image/jpg");
    localObject2 = localFlashExtras;
    localFlashExtras.<init>(null, localFlashImageEntity, null, 5, null);
    localObject2 = paramImageFlash.f();
    c.g.b.k.a(localObject2, "flash.payload");
    localObject1 = ((MediaUrl)localObject1).getDownloadUrl();
    ((Payload)localObject2).a((String)localObject1);
    localObject1 = paramImageFlash.f();
    c.g.b.k.a(localObject1, "flash.payload");
    localObject2 = j.a(localFlashExtras);
    ((Payload)localObject1).b((String)localObject2);
    localObject1 = e;
    localObject2 = paramImageFlash.r();
    c.g.b.k.a(localObject2, "flash.screenContext");
    Object localObject3 = this;
    localObject3 = (a)this;
    ((k)localObject1).a(paramImageFlash, (String)localObject2, (a)localObject3);
  }
  
  public final int a(String paramString, Flash paramFlash, ImageFlash paramImageFlash)
  {
    String str = "action_image_download";
    boolean bool = c.g.b.k.a(str, paramString);
    int k = 2;
    int m = 0;
    Object localObject1 = null;
    Object localObject2;
    if (bool)
    {
      if (paramFlash != null)
      {
        paramString = j;
        paramImageFlash = paramFlash.f();
        c.g.b.k.a(paramImageFlash, "flash.payload");
        paramImageFlash = paramImageFlash.e();
        c.g.b.k.a(paramImageFlash, "flash.payload.extra");
        localObject2 = FlashExtras.class;
        paramString = (FlashExtras)paramString.a(paramImageFlash, (Class)localObject2);
        if (paramString != null)
        {
          paramString = paramString.getMedia();
          if (paramString != null)
          {
            paramString = paramString.getImageUrl();
            break label100;
          }
        }
        bool = false;
        paramString = null;
        label100:
        paramImageFlash = null;
        Object localObject3;
        label167:
        int i1;
        if (paramString == null)
        {
          localObject2 = paramFlash.f();
          localObject3 = "flash.payload";
          c.g.b.k.a(localObject2, (String)localObject3);
          localObject2 = (CharSequence)((Payload)localObject2).d();
          if (localObject2 != null)
          {
            n = ((CharSequence)localObject2).length();
            if (n != 0)
            {
              n = 0;
              localObject2 = null;
              break label167;
            }
          }
          int n = 1;
          if (n == 0)
          {
            paramString = paramFlash.f();
            c.g.b.k.a(paramString, "flash.payload");
            paramString = paramString.d();
            c.g.b.k.a(paramString, "flash.payload.attachment");
            paramString = (CharSequence)paramString;
            localObject2 = new String[] { "," };
            i1 = 6;
            paramString = (String)c.n.m.c(paramString, (String[])localObject2, false, i1).get(0);
          }
        }
        if (paramString != null)
        {
          localObject2 = (f.b)b;
          if (localObject2 != null)
          {
            localObject3 = f;
            int i2 = R.string.flash_downloading;
            Object[] arrayOfObject1 = new Object[0];
            localObject3 = ((al)localObject3).a(i2, arrayOfObject1);
            ((f.b)localObject2).a((String)localObject3);
          }
          localObject2 = g;
          paramFlash = paramFlash.a();
          c.g.b.k.a(paramFlash, "flash.sender");
          long l = paramFlash.a().longValue();
          paramFlash = String.valueOf(l);
          localObject3 = ".jpg";
          paramFlash = ((j)localObject2).a(paramFlash, (String)localObject3);
          localObject2 = (f.b)b;
          if (localObject2 != null)
          {
            localObject1 = f;
            i1 = R.string.true_flash;
            Object[] arrayOfObject2 = new Object[0];
            localObject1 = ((al)localObject1).a(i1, arrayOfObject2);
            ((f.b)localObject2).a(paramString, paramFlash, (String)localObject1);
            localObject1 = x.a;
          }
          if (localObject1 != null) {}
        }
        else
        {
          paramString = this;
          paramString = (g)this;
          paramFlash = (f.b)b;
          if (paramFlash != null)
          {
            paramString = f;
            m = R.string.flash_download_failed;
            paramImageFlash = new Object[0];
            paramString = paramString.a(m, paramImageFlash);
            paramFlash.a(paramString);
            paramString = x.a;
          }
        }
      }
    }
    else if (paramImageFlash != null)
    {
      paramString = paramImageFlash.f();
      if (paramString != null)
      {
        paramString = (ag)bg.a;
        paramFlash = d;
        localObject2 = c;
        paramFlash = paramFlash.plus((f)localObject2);
        localObject2 = new com/truecaller/flashsdk/core/g$d;
        ((g.d)localObject2).<init>(paramImageFlash, null, this);
        localObject2 = (c.g.a.m)localObject2;
        e.a(paramString, paramFlash, (c.g.a.m)localObject2, k);
      }
      else
      {
        paramString = new java/lang/IllegalStateException;
        paramString.<init>("Flash must've a payload set");
        throw ((Throwable)paramString);
      }
    }
    return k;
  }
  
  public final void a(ImageFlash paramImageFlash)
  {
    c.g.b.k.b(paramImageFlash, "flash");
    f.b localb = (f.b)b;
    if (localb != null) {
      localb.a();
    }
    i.a(paramImageFlash);
  }
  
  public final void b(ImageFlash paramImageFlash)
  {
    c.g.b.k.b(paramImageFlash, "flash");
    Object localObject = (f.b)b;
    if (localObject != null) {
      ((f.b)localObject).b();
    }
    localObject = i;
    paramImageFlash = (Flash)paramImageFlash;
    ((com.truecaller.flashsdk.c.a)localObject).c(paramImageFlash);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.core.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */