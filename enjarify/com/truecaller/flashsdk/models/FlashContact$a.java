package com.truecaller.flashsdk.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class FlashContact$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    FlashContact localFlashContact = new com/truecaller/flashsdk/models/FlashContact;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    paramParcel = paramParcel.readString();
    localFlashContact.<init>(str1, str2, paramParcel);
    return localFlashContact;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new FlashContact[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.FlashContact.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */