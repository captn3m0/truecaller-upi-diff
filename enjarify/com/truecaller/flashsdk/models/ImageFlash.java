package com.truecaller.flashsdk.models;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class ImageFlash
  extends Flash
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private Uri i;
  private MediaUrl j;
  private boolean k;
  private boolean l;
  private String m;
  
  static
  {
    ImageFlash.1 local1 = new com/truecaller/flashsdk/models/ImageFlash$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public ImageFlash() {}
  
  private ImageFlash(Parcel paramParcel)
  {
    super(paramParcel);
    Object localObject = Uri.class.getClassLoader();
    localObject = (Uri)paramParcel.readParcelable((ClassLoader)localObject);
    i = ((Uri)localObject);
    localObject = MediaUrl.class.getClassLoader();
    localObject = (MediaUrl)paramParcel.readParcelable((ClassLoader)localObject);
    j = ((MediaUrl)localObject);
    int n = paramParcel.readByte();
    boolean bool = true;
    if (n != 0)
    {
      n = 1;
    }
    else
    {
      n = 0;
      localObject = null;
    }
    k = n;
    n = paramParcel.readByte();
    if (n == 0) {
      bool = false;
    }
    l = bool;
    paramParcel = paramParcel.readString();
    m = paramParcel;
  }
  
  public final void a(Uri paramUri)
  {
    i = paramUri;
  }
  
  public final void a(Flash paramFlash)
  {
    Object localObject = a;
    a = ((Sender)localObject);
    long l1 = b;
    b = l1;
    localObject = c;
    c = ((String)localObject);
    localObject = d;
    d = ((String)localObject);
    localObject = e;
    e = ((String)localObject);
    localObject = f;
    f = ((Payload)localObject);
    l1 = g;
    g = l1;
    paramFlash = h;
    h = paramFlash;
  }
  
  public final void a(MediaUrl paramMediaUrl)
  {
    j = paramMediaUrl;
  }
  
  public final void d(String paramString)
  {
    m = paramString;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public final Uri n()
  {
    return i;
  }
  
  public final MediaUrl o()
  {
    return j;
  }
  
  public final boolean p()
  {
    return k;
  }
  
  public final void q()
  {
    k = true;
  }
  
  public final String r()
  {
    return m;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    Object localObject = i;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    localObject = j;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    paramInt = (byte)k;
    paramParcel.writeByte(paramInt);
    paramInt = (byte)l;
    paramParcel.writeByte(paramInt);
    String str = m;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.ImageFlash
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */