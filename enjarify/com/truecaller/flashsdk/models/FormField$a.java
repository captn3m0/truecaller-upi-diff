package com.truecaller.flashsdk.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class FormField$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    FormField localFormField = new com/truecaller/flashsdk/models/FormField;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    String str4 = paramParcel.readString();
    String str5 = paramParcel.readString();
    String str6 = paramParcel.readString();
    String str7 = paramParcel.readString();
    String str8 = paramParcel.readString();
    localFormField.<init>(str1, str2, str3, str4, str5, str6, str7, str8);
    return localFormField;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new FormField[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.FormField.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */