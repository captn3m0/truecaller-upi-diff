package com.truecaller.flashsdk.models;

import c.g.b.k;

public final class FlashImageEntity
{
  private final String imageUrl;
  private final String mediaType;
  
  public FlashImageEntity()
  {
    this(null, null, 3, null);
  }
  
  public FlashImageEntity(String paramString1, String paramString2)
  {
    imageUrl = paramString1;
    mediaType = paramString2;
  }
  
  public final String component1()
  {
    return imageUrl;
  }
  
  public final String component2()
  {
    return mediaType;
  }
  
  public final FlashImageEntity copy(String paramString1, String paramString2)
  {
    k.b(paramString1, "imageUrl");
    k.b(paramString2, "mediaType");
    FlashImageEntity localFlashImageEntity = new com/truecaller/flashsdk/models/FlashImageEntity;
    localFlashImageEntity.<init>(paramString1, paramString2);
    return localFlashImageEntity;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof FlashImageEntity;
      if (bool1)
      {
        paramObject = (FlashImageEntity)paramObject;
        String str1 = imageUrl;
        String str2 = imageUrl;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = mediaType;
          paramObject = mediaType;
          boolean bool2 = k.a(str1, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final String getImageUrl()
  {
    return imageUrl;
  }
  
  public final String getMediaType()
  {
    return mediaType;
  }
  
  public final int hashCode()
  {
    String str1 = imageUrl;
    int i = 0;
    int j;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = mediaType;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FlashImageEntity(imageUrl=");
    String str = imageUrl;
    localStringBuilder.append(str);
    localStringBuilder.append(", mediaType=");
    str = mediaType;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.FlashImageEntity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */