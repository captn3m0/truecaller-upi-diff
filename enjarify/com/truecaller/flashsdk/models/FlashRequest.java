package com.truecaller.flashsdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class FlashRequest
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final Data data;
  private final String messageId;
  private final String version;
  
  static
  {
    FlashRequest.a locala = new com/truecaller/flashsdk/models/FlashRequest$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public FlashRequest(String paramString1, String paramString2, Data paramData)
  {
    messageId = paramString1;
    version = paramString2;
    data = paramData;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final Data getData()
  {
    return data;
  }
  
  public final String getMessageId()
  {
    return messageId;
  }
  
  public final String getVersion()
  {
    return version;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = messageId;
    paramParcel.writeString(str);
    str = version;
    paramParcel.writeString(str);
    data.writeToParcel(paramParcel, 0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.FlashRequest
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */