package com.truecaller.flashsdk.models;

import android.content.ContentValues;
import c.g.b.k;
import java.io.Serializable;

public final class e
  implements Serializable
{
  public String a;
  public int b;
  public boolean c;
  
  public e(String paramString, int paramInt, boolean paramBoolean)
  {
    a = paramString;
    b = paramInt;
    c = paramBoolean;
  }
  
  public final ContentValues a()
  {
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    Object localObject = a;
    localContentValues.put("phone", (String)localObject);
    localObject = Integer.valueOf(c);
    localContentValues.put("flash_enabled", (Integer)localObject);
    localObject = Integer.valueOf(b);
    localContentValues.put("version", (Integer)localObject);
    return localContentValues;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof e;
      if (bool2)
      {
        paramObject = (e)paramObject;
        String str1 = a;
        String str2 = a;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          int i = b;
          int j = b;
          if (i == j)
          {
            i = 1;
          }
          else
          {
            i = 0;
            str1 = null;
          }
          if (i != 0)
          {
            boolean bool3 = c;
            boolean bool4 = c;
            if (bool3 == bool4)
            {
              bool4 = true;
            }
            else
            {
              bool4 = false;
              paramObject = null;
            }
            if (bool4) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str = a;
    if (str != null)
    {
      i = str.hashCode();
    }
    else
    {
      i = 0;
      str = null;
    }
    i *= 31;
    int j = b;
    int i = (i + j) * 31;
    int k = c;
    if (k != 0) {
      k = 1;
    }
    return i + k;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FlashState{Number =");
    Object localObject = a;
    if (localObject == null)
    {
      localObject = "null";
    }
    else
    {
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>("<non-null number>Version =");
      int i = b;
      ((StringBuilder)localObject).append(i);
      String str = "Enabled =";
      ((StringBuilder)localObject).append(str);
      boolean bool = c;
      ((StringBuilder)localObject).append(bool);
      char c1 = '}';
      ((StringBuilder)localObject).append(c1);
      localObject = ((StringBuilder)localObject).toString();
    }
    localStringBuilder.append((String)localObject);
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */