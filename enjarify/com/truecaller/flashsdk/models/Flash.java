package com.truecaller.flashsdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.SystemClock;
import android.text.TextUtils;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.f;
import com.truecaller.flashsdk.assist.c;
import java.util.Map;
import java.util.Random;

public class Flash
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  protected Sender a;
  protected long b;
  protected String c;
  protected String d;
  protected String e;
  protected Payload f;
  protected long g;
  protected String h;
  
  static
  {
    Flash.1 local1 = new com/truecaller/flashsdk/models/Flash$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public Flash() {}
  
  protected Flash(Parcel paramParcel)
  {
    Object localObject = Sender.class.getClassLoader();
    localObject = (Sender)paramParcel.readParcelable((ClassLoader)localObject);
    a = ((Sender)localObject);
    long l = paramParcel.readLong();
    b = l;
    localObject = paramParcel.readString();
    c = ((String)localObject);
    localObject = paramParcel.readString();
    d = ((String)localObject);
    localObject = paramParcel.readString();
    e = ((String)localObject);
    localObject = Payload.class.getClassLoader();
    localObject = (Payload)paramParcel.readParcelable((ClassLoader)localObject);
    f = ((Payload)localObject);
    l = paramParcel.readLong();
    g = l;
    paramParcel = paramParcel.readString();
    h = paramParcel;
  }
  
  public static Flash a(RemoteMessage paramRemoteMessage, f paramf)
  {
    Map localMap = paramRemoteMessage.a();
    Object localObject1 = "sender";
    boolean bool1 = localMap.containsKey(localObject1);
    if (bool1)
    {
      localObject1 = "payload";
      bool1 = localMap.containsKey(localObject1);
      if (bool1)
      {
        localObject1 = "timestamp";
        bool1 = localMap.containsKey(localObject1);
        if (bool1)
        {
          localObject1 = new com/truecaller/flashsdk/models/Flash;
          ((Flash)localObject1).<init>();
          Object localObject2 = (String)localMap.get("sender");
          localObject2 = (Sender)paramf.a((String)localObject2, Sender.class);
          a = ((Sender)localObject2);
          localObject2 = (String)localMap.get("payload");
          Class localClass = Payload.class;
          paramf = (Payload)paramf.a((String)localObject2, localClass);
          f = paramf;
          long l = Long.parseLong((String)localMap.get("timestamp"));
          g = l;
          paramf = "instanceId";
          boolean bool2 = localMap.containsKey(paramf);
          if (bool2)
          {
            paramRemoteMessage = (String)localMap.get("instanceId");
            h = paramRemoteMessage;
          }
          else
          {
            paramf = paramRemoteMessage.b();
            if (paramf != null)
            {
              paramRemoteMessage = paramRemoteMessage.b();
              h = paramRemoteMessage;
            }
          }
          paramRemoteMessage = "history";
          boolean bool3 = localMap.containsKey(paramRemoteMessage);
          if (bool3)
          {
            paramRemoteMessage = (String)localMap.get("history");
            e = paramRemoteMessage;
          }
          else
          {
            paramRemoteMessage = c.a("💬");
            e = paramRemoteMessage;
          }
          paramRemoteMessage = "state";
          bool3 = localMap.containsKey(paramRemoteMessage);
          if (bool3)
          {
            paramRemoteMessage = (String)localMap.get("state");
            d = paramRemoteMessage;
          }
          paramRemoteMessage = "thread_id";
          bool3 = localMap.containsKey(paramRemoteMessage);
          if (bool3)
          {
            paramRemoteMessage = (String)localMap.get("thread_id");
            c = paramRemoteMessage;
          }
          paramRemoteMessage = "threadId";
          bool3 = localMap.containsKey(paramRemoteMessage);
          if (bool3)
          {
            paramRemoteMessage = (String)localMap.get("threadId");
            c = paramRemoteMessage;
          }
          return (Flash)localObject1;
        }
      }
    }
    return null;
  }
  
  private static String d(String paramString)
  {
    Object[] arrayOfObject = new Object[2];
    Object localObject = Long.toHexString(SystemClock.elapsedRealtime());
    arrayOfObject[0] = localObject;
    localObject = new java/util/Random;
    long l = paramString.hashCode();
    ((Random)localObject).<init>(l);
    paramString = Integer.toHexString(((Random)localObject).nextInt());
    arrayOfObject[1] = paramString;
    return String.format("%s-%s", arrayOfObject);
  }
  
  public final Sender a()
  {
    return a;
  }
  
  public final void a(long paramLong)
  {
    b = paramLong;
  }
  
  public final void a(Payload paramPayload)
  {
    f = paramPayload;
  }
  
  public final void a(String paramString)
  {
    d = paramString;
  }
  
  public final long b()
  {
    return b;
  }
  
  public final void b(long paramLong)
  {
    g = paramLong;
  }
  
  public final void b(String paramString)
  {
    e = paramString;
  }
  
  public final FlashRequest c(String paramString)
  {
    FlashRequest localFlashRequest = new com/truecaller/flashsdk/models/FlashRequest;
    String str1 = h;
    Data localData = new com/truecaller/flashsdk/models/Data;
    long l = b;
    String str2 = c;
    String str3 = d;
    String str4 = e;
    Payload localPayload = f;
    localData.<init>(l, str2, str3, str4, paramString, localPayload);
    localFlashRequest.<init>(str1, "6", localData);
    return localFlashRequest;
  }
  
  public final String c()
  {
    return c;
  }
  
  public final String d()
  {
    return d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public final String e()
  {
    return e;
  }
  
  public final Payload f()
  {
    return f;
  }
  
  public final long g()
  {
    return g;
  }
  
  public final String h()
  {
    return h;
  }
  
  public final void i()
  {
    String str = d(Long.toString(b));
    c = str;
  }
  
  public final void j()
  {
    Object[] arrayOfObject = new Object[2];
    Object localObject = Long.valueOf(b);
    arrayOfObject[0] = localObject;
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str1 = c;
    ((StringBuilder)localObject).append(str1);
    long l = System.currentTimeMillis();
    ((StringBuilder)localObject).append(l);
    localObject = d(String.valueOf(((StringBuilder)localObject).toString()));
    arrayOfObject[1] = localObject;
    String str2 = String.format("-%s-%s", arrayOfObject);
    h = str2;
  }
  
  public final boolean k()
  {
    Object localObject = a;
    if (localObject == null) {
      return false;
    }
    long l = a;
    localObject = Long.valueOf(l);
    if (localObject == null) {
      return false;
    }
    localObject = f;
    if (localObject == null) {
      return false;
    }
    localObject = a;
    boolean bool = TextUtils.isEmpty((CharSequence)localObject);
    if (bool) {
      return false;
    }
    localObject = f.a;
    bool = TextUtils.isEmpty((CharSequence)localObject);
    return !bool;
  }
  
  public final boolean l()
  {
    long l1 = b;
    long l2 = 0L;
    boolean bool1 = l1 < l2;
    if (!bool1) {
      return false;
    }
    Object localObject = f;
    if (localObject == null) {
      return false;
    }
    localObject = a;
    boolean bool2 = TextUtils.isEmpty((CharSequence)localObject);
    if (bool2) {
      return false;
    }
    localObject = f.a;
    bool2 = TextUtils.isEmpty((CharSequence)localObject);
    return !bool2;
  }
  
  public final boolean m()
  {
    String str = h;
    boolean bool = TextUtils.isEmpty(str);
    if (!bool)
    {
      str = c;
      bool = TextUtils.isEmpty(str);
      if (!bool) {
        return true;
      }
    }
    return false;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    Object localObject = a;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    long l = b;
    paramParcel.writeLong(l);
    localObject = c;
    paramParcel.writeString((String)localObject);
    localObject = d;
    paramParcel.writeString((String)localObject);
    localObject = e;
    paramParcel.writeString((String)localObject);
    localObject = f;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    l = g;
    paramParcel.writeLong(l);
    String str = h;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.Flash
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */