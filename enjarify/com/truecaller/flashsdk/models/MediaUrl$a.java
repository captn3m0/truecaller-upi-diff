package com.truecaller.flashsdk.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class MediaUrl$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    MediaUrl localMediaUrl = new com/truecaller/flashsdk/models/MediaUrl;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    paramParcel = (FormField)FormField.CREATOR.createFromParcel(paramParcel);
    localMediaUrl.<init>(str1, str2, paramParcel);
    return localMediaUrl;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new MediaUrl[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.MediaUrl.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */