package com.truecaller.flashsdk.models;

import c.g.b.k;

public final class Contact
{
  private final String imageUrl;
  private final String name;
  
  public Contact(String paramString1, String paramString2)
  {
    name = paramString1;
    imageUrl = paramString2;
  }
  
  public final String component1()
  {
    return name;
  }
  
  public final String component2()
  {
    return imageUrl;
  }
  
  public final Contact copy(String paramString1, String paramString2)
  {
    k.b(paramString1, "name");
    Contact localContact = new com/truecaller/flashsdk/models/Contact;
    localContact.<init>(paramString1, paramString2);
    return localContact;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof Contact;
      if (bool1)
      {
        paramObject = (Contact)paramObject;
        String str1 = name;
        String str2 = name;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = imageUrl;
          paramObject = imageUrl;
          boolean bool2 = k.a(str1, paramObject);
          if (bool2) {
            break label68;
          }
        }
      }
      return false;
    }
    label68:
    return true;
  }
  
  public final String getImageUrl()
  {
    return imageUrl;
  }
  
  public final String getName()
  {
    return name;
  }
  
  public final int hashCode()
  {
    String str1 = name;
    int i = 0;
    int j;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = imageUrl;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("Contact(name=");
    String str = name;
    localStringBuilder.append(str);
    localStringBuilder.append(", imageUrl=");
    str = imageUrl;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.Contact
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */