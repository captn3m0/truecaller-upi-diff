package com.truecaller.flashsdk.models;

import c.g.b.k;
import java.util.List;

public final class FlashExtras
{
  private final FlashLocationExtras location;
  private final FlashImageEntity media;
  private final List replyActions;
  
  public FlashExtras()
  {
    this(null, null, null, 7, null);
  }
  
  public FlashExtras(List paramList, FlashImageEntity paramFlashImageEntity, FlashLocationExtras paramFlashLocationExtras)
  {
    replyActions = paramList;
    media = paramFlashImageEntity;
    location = paramFlashLocationExtras;
  }
  
  public final List component1()
  {
    return replyActions;
  }
  
  public final FlashImageEntity component2()
  {
    return media;
  }
  
  public final FlashLocationExtras component3()
  {
    return location;
  }
  
  public final FlashExtras copy(List paramList, FlashImageEntity paramFlashImageEntity, FlashLocationExtras paramFlashLocationExtras)
  {
    FlashExtras localFlashExtras = new com/truecaller/flashsdk/models/FlashExtras;
    localFlashExtras.<init>(paramList, paramFlashImageEntity, paramFlashLocationExtras);
    return localFlashExtras;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof FlashExtras;
      if (bool1)
      {
        paramObject = (FlashExtras)paramObject;
        Object localObject1 = replyActions;
        Object localObject2 = replyActions;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = media;
          localObject2 = media;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = location;
            paramObject = location;
            boolean bool2 = k.a(localObject1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final FlashLocationExtras getLocation()
  {
    return location;
  }
  
  public final FlashImageEntity getMedia()
  {
    return media;
  }
  
  public final List getReplyActions()
  {
    return replyActions;
  }
  
  public final int hashCode()
  {
    List localList = replyActions;
    int i = 0;
    if (localList != null)
    {
      j = localList.hashCode();
    }
    else
    {
      j = 0;
      localList = null;
    }
    j *= 31;
    Object localObject = media;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = location;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FlashExtras(replyActions=");
    Object localObject = replyActions;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", media=");
    localObject = media;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", location=");
    localObject = location;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.FlashExtras
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */