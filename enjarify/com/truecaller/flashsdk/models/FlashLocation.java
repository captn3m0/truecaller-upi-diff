package com.truecaller.flashsdk.models;

import c.g.b.k;

public final class FlashLocation
{
  private final String area;
  private final String city;
  private final String street;
  
  public FlashLocation()
  {
    this(null, null, null, 7, null);
  }
  
  public FlashLocation(String paramString1, String paramString2, String paramString3)
  {
    street = paramString1;
    area = paramString2;
    city = paramString3;
  }
  
  public final String component1()
  {
    return street;
  }
  
  public final String component2()
  {
    return area;
  }
  
  public final String component3()
  {
    return city;
  }
  
  public final FlashLocation copy(String paramString1, String paramString2, String paramString3)
  {
    FlashLocation localFlashLocation = new com/truecaller/flashsdk/models/FlashLocation;
    localFlashLocation.<init>(paramString1, paramString2, paramString3);
    return localFlashLocation;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof FlashLocation;
      if (bool1)
      {
        paramObject = (FlashLocation)paramObject;
        String str1 = street;
        String str2 = street;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = area;
          str2 = area;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = city;
            paramObject = city;
            boolean bool2 = k.a(str1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String getArea()
  {
    return area;
  }
  
  public final String getCity()
  {
    return city;
  }
  
  public final String getStreet()
  {
    return street;
  }
  
  public final int hashCode()
  {
    String str1 = street;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = area;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = city;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FlashLocation(street=");
    String str = street;
    localStringBuilder.append(str);
    localStringBuilder.append(", area=");
    str = area;
    localStringBuilder.append(str);
    localStringBuilder.append(", city=");
    str = city;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.FlashLocation
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */