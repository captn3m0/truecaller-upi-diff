package com.truecaller.flashsdk.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class Data$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    Data localData = new com/truecaller/flashsdk/models/Data;
    long l = paramParcel.readLong();
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    String str3 = paramParcel.readString();
    String str4 = paramParcel.readString();
    Object localObject1 = Data.class.getClassLoader();
    paramParcel = paramParcel.readParcelable((ClassLoader)localObject1);
    Object localObject2 = paramParcel;
    localObject2 = (Payload)paramParcel;
    localObject1 = localData;
    localData.<init>(l, str1, str2, str3, str4, (Payload)localObject2);
    return localData;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new Data[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.Data.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */