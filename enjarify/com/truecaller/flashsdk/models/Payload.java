package com.truecaller.flashsdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.List;

public class Payload
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final String a;
  private final String b;
  private final List c;
  private String d;
  private String e;
  
  static
  {
    Payload.1 local1 = new com/truecaller/flashsdk/models/Payload$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  protected Payload(Parcel paramParcel)
  {
    Object localObject = paramParcel.readString();
    a = ((String)localObject);
    localObject = paramParcel.readString();
    b = ((String)localObject);
    localObject = paramParcel.createStringArrayList();
    c = ((List)localObject);
    localObject = paramParcel.readString();
    d = ((String)localObject);
    paramParcel = paramParcel.readString();
    e = paramParcel;
  }
  
  public Payload(String paramString1, String paramString2, List paramList, String paramString3)
  {
    this(paramString1, paramString2, paramList, paramString3, (byte)0);
  }
  
  private Payload(String paramString1, String paramString2, List paramList, String paramString3, byte paramByte)
  {
    a = paramString1;
    b = paramString2;
    c = paramList;
    d = paramString3;
    e = null;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final void a(String paramString)
  {
    d = paramString;
  }
  
  public final String b()
  {
    return b;
  }
  
  public final void b(String paramString)
  {
    e = paramString;
  }
  
  public final List c()
  {
    return c;
  }
  
  public final String d()
  {
    return d;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public final String e()
  {
    return e;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    Object localObject = a;
    paramParcel.writeString((String)localObject);
    localObject = b;
    paramParcel.writeString((String)localObject);
    localObject = c;
    paramParcel.writeStringList((List)localObject);
    localObject = d;
    paramParcel.writeString((String)localObject);
    localObject = e;
    paramParcel.writeString((String)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.Payload
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */