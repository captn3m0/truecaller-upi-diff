package com.truecaller.flashsdk.models;

import c.g.b.k;

public final class FlashLocationExtras
{
  private final String address;
  private final Double lat;
  private final String location_text;
  private final Double jdField_long;
  
  public FlashLocationExtras(String paramString1, Double paramDouble1, Double paramDouble2, String paramString2)
  {
    address = paramString1;
    lat = paramDouble1;
    jdField_long = paramDouble2;
    location_text = paramString2;
  }
  
  public final String component1()
  {
    return address;
  }
  
  public final Double component2()
  {
    return lat;
  }
  
  public final Double component3()
  {
    return jdField_long;
  }
  
  public final String component4()
  {
    return location_text;
  }
  
  public final FlashLocationExtras copy(String paramString1, Double paramDouble1, Double paramDouble2, String paramString2)
  {
    FlashLocationExtras localFlashLocationExtras = new com/truecaller/flashsdk/models/FlashLocationExtras;
    localFlashLocationExtras.<init>(paramString1, paramDouble1, paramDouble2, paramString2);
    return localFlashLocationExtras;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof FlashLocationExtras;
      if (bool1)
      {
        paramObject = (FlashLocationExtras)paramObject;
        Object localObject1 = address;
        Object localObject2 = address;
        bool1 = k.a(localObject1, localObject2);
        if (bool1)
        {
          localObject1 = lat;
          localObject2 = lat;
          bool1 = k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = jdField_long;
            localObject2 = jdField_long;
            bool1 = k.a(localObject1, localObject2);
            if (bool1)
            {
              localObject1 = location_text;
              paramObject = location_text;
              boolean bool2 = k.a(localObject1, paramObject);
              if (bool2) {
                break label112;
              }
            }
          }
        }
      }
      return false;
    }
    label112:
    return true;
  }
  
  public final String getAddress()
  {
    return address;
  }
  
  public final Double getLat()
  {
    return lat;
  }
  
  public final String getLocation_text()
  {
    return location_text;
  }
  
  public final Double getLong()
  {
    return jdField_long;
  }
  
  public final int hashCode()
  {
    String str = address;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = lat;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = jdField_long;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    j = (j + k) * 31;
    localObject = location_text;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FlashLocationExtras(address=");
    Object localObject = address;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", lat=");
    localObject = lat;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", long=");
    localObject = jdField_long;
    localStringBuilder.append(localObject);
    localStringBuilder.append(", location_text=");
    localObject = location_text;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.FlashLocationExtras
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */