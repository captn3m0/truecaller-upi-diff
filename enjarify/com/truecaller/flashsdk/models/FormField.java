package com.truecaller.flashsdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class FormField
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String access;
  private final String algorithm;
  private final String bucket;
  private final String credential;
  private final String date;
  private final String key;
  private final String policy;
  private final String signature;
  
  static
  {
    FormField.a locala = new com/truecaller/flashsdk/models/FormField$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public FormField(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8)
  {
    algorithm = paramString1;
    policy = paramString2;
    signature = paramString3;
    key = paramString4;
    access = paramString5;
    date = paramString6;
    bucket = paramString7;
    credential = paramString8;
  }
  
  public final String component1()
  {
    return algorithm;
  }
  
  public final String component2()
  {
    return policy;
  }
  
  public final String component3()
  {
    return signature;
  }
  
  public final String component4()
  {
    return key;
  }
  
  public final String component5()
  {
    return access;
  }
  
  public final String component6()
  {
    return date;
  }
  
  public final String component7()
  {
    return bucket;
  }
  
  public final String component8()
  {
    return credential;
  }
  
  public final FormField copy(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8)
  {
    k.b(paramString1, "algorithm");
    k.b(paramString2, "policy");
    k.b(paramString3, "signature");
    k.b(paramString4, "key");
    k.b(paramString5, "access");
    k.b(paramString6, "date");
    k.b(paramString7, "bucket");
    k.b(paramString8, "credential");
    FormField localFormField = new com/truecaller/flashsdk/models/FormField;
    localFormField.<init>(paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7, paramString8);
    return localFormField;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof FormField;
      if (bool1)
      {
        paramObject = (FormField)paramObject;
        String str1 = algorithm;
        String str2 = algorithm;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = policy;
          str2 = policy;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = signature;
            str2 = signature;
            bool1 = k.a(str1, str2);
            if (bool1)
            {
              str1 = key;
              str2 = key;
              bool1 = k.a(str1, str2);
              if (bool1)
              {
                str1 = access;
                str2 = access;
                bool1 = k.a(str1, str2);
                if (bool1)
                {
                  str1 = date;
                  str2 = date;
                  bool1 = k.a(str1, str2);
                  if (bool1)
                  {
                    str1 = bucket;
                    str2 = bucket;
                    bool1 = k.a(str1, str2);
                    if (bool1)
                    {
                      str1 = credential;
                      paramObject = credential;
                      boolean bool2 = k.a(str1, paramObject);
                      if (bool2) {
                        break label200;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }
    label200:
    return true;
  }
  
  public final String getAccess()
  {
    return access;
  }
  
  public final String getAlgorithm()
  {
    return algorithm;
  }
  
  public final String getBucket()
  {
    return bucket;
  }
  
  public final String getCredential()
  {
    return credential;
  }
  
  public final String getDate()
  {
    return date;
  }
  
  public final String getKey()
  {
    return key;
  }
  
  public final String getPolicy()
  {
    return policy;
  }
  
  public final String getSignature()
  {
    return signature;
  }
  
  public final int hashCode()
  {
    String str1 = algorithm;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = policy;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = signature;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = key;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = access;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = date;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = bucket;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    j = (j + k) * 31;
    str2 = credential;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FormField(algorithm=");
    String str = algorithm;
    localStringBuilder.append(str);
    localStringBuilder.append(", policy=");
    str = policy;
    localStringBuilder.append(str);
    localStringBuilder.append(", signature=");
    str = signature;
    localStringBuilder.append(str);
    localStringBuilder.append(", key=");
    str = key;
    localStringBuilder.append(str);
    localStringBuilder.append(", access=");
    str = access;
    localStringBuilder.append(str);
    localStringBuilder.append(", date=");
    str = date;
    localStringBuilder.append(str);
    localStringBuilder.append(", bucket=");
    str = bucket;
    localStringBuilder.append(str);
    localStringBuilder.append(", credential=");
    str = credential;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = algorithm;
    paramParcel.writeString(str);
    str = policy;
    paramParcel.writeString(str);
    str = signature;
    paramParcel.writeString(str);
    str = key;
    paramParcel.writeString(str);
    str = access;
    paramParcel.writeString(str);
    str = date;
    paramParcel.writeString(str);
    str = bucket;
    paramParcel.writeString(str);
    str = credential;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.FormField
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */