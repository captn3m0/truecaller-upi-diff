package com.truecaller.flashsdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class FlashContact
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final String a;
  public final String b;
  public final String c;
  
  static
  {
    FlashContact.a locala = new com/truecaller/flashsdk/models/FlashContact$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public FlashContact(String paramString1, String paramString2, String paramString3)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof FlashContact;
      if (bool1)
      {
        paramObject = (FlashContact)paramObject;
        String str1 = a;
        String str2 = a;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = b;
          str2 = b;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = c;
            paramObject = c;
            boolean bool2 = k.a(str1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final int hashCode()
  {
    String str1 = a;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = b;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = c;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("FlashContact(phone=");
    String str = a;
    localStringBuilder.append(str);
    localStringBuilder.append(", firstName=");
    str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(", lastName=");
    str = c;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = a;
    paramParcel.writeString(str);
    str = b;
    paramParcel.writeString(str);
    str = c;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.FlashContact
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */