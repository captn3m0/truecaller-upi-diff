package com.truecaller.flashsdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class Data
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String history;
  private final Payload payload;
  private final String pushToken;
  private final String state;
  private final String threadId;
  private final long to;
  
  static
  {
    Data.a locala = new com/truecaller/flashsdk/models/Data$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public Data(long paramLong, String paramString1, String paramString2, String paramString3, String paramString4, Payload paramPayload)
  {
    to = paramLong;
    threadId = paramString1;
    state = paramString2;
    history = paramString3;
    pushToken = paramString4;
    payload = paramPayload;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final String getHistory()
  {
    return history;
  }
  
  public final Payload getPayload()
  {
    return payload;
  }
  
  public final String getPushToken()
  {
    return pushToken;
  }
  
  public final String getState()
  {
    return state;
  }
  
  public final String getThreadId()
  {
    return threadId;
  }
  
  public final long getTo()
  {
    return to;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    long l = to;
    paramParcel.writeLong(l);
    Object localObject = threadId;
    paramParcel.writeString((String)localObject);
    localObject = state;
    paramParcel.writeString((String)localObject);
    localObject = history;
    paramParcel.writeString((String)localObject);
    localObject = pushToken;
    paramParcel.writeString((String)localObject);
    localObject = payload;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.Data
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */