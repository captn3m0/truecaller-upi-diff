package com.truecaller.flashsdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;

public class Sender
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  public final long a;
  private final String b;
  private final String c;
  
  static
  {
    Sender.1 local1 = new com/truecaller/flashsdk/models/Sender$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  private Sender(Parcel paramParcel)
  {
    long l = paramParcel.readLong();
    a = l;
    String str = paramParcel.readString();
    b = str;
    paramParcel = paramParcel.readString();
    c = paramParcel;
  }
  
  public final Long a()
  {
    return Long.valueOf(a);
  }
  
  public final String b()
  {
    String str = b;
    if (str != null)
    {
      str = str.trim();
      boolean bool = TextUtils.isEmpty(str);
      if (!bool) {
        return b;
      }
    }
    return Long.toString(a);
  }
  
  public final String c()
  {
    return c;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    long l = a;
    paramParcel.writeLong(l);
    String str = b;
    paramParcel.writeString(str);
    str = c;
    paramParcel.writeString(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.Sender
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */