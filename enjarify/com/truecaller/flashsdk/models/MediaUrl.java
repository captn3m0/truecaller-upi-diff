package com.truecaller.flashsdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class MediaUrl
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private final String downloadUrl;
  private final FormField formField;
  private final String uploadUrl;
  
  static
  {
    MediaUrl.a locala = new com/truecaller/flashsdk/models/MediaUrl$a;
    locala.<init>();
    CREATOR = locala;
  }
  
  public MediaUrl(String paramString1, String paramString2, FormField paramFormField)
  {
    uploadUrl = paramString1;
    downloadUrl = paramString2;
    formField = paramFormField;
  }
  
  public final String component1()
  {
    return uploadUrl;
  }
  
  public final String component2()
  {
    return downloadUrl;
  }
  
  public final FormField component3()
  {
    return formField;
  }
  
  public final MediaUrl copy(String paramString1, String paramString2, FormField paramFormField)
  {
    k.b(paramString1, "uploadUrl");
    k.b(paramString2, "downloadUrl");
    k.b(paramFormField, "formField");
    MediaUrl localMediaUrl = new com/truecaller/flashsdk/models/MediaUrl;
    localMediaUrl.<init>(paramString1, paramString2, paramFormField);
    return localMediaUrl;
  }
  
  public final int describeContents()
  {
    return 0;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof MediaUrl;
      if (bool1)
      {
        paramObject = (MediaUrl)paramObject;
        Object localObject = uploadUrl;
        String str = uploadUrl;
        bool1 = k.a(localObject, str);
        if (bool1)
        {
          localObject = downloadUrl;
          str = downloadUrl;
          bool1 = k.a(localObject, str);
          if (bool1)
          {
            localObject = formField;
            paramObject = formField;
            boolean bool2 = k.a(localObject, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String getDownloadUrl()
  {
    return downloadUrl;
  }
  
  public final FormField getFormField()
  {
    return formField;
  }
  
  public final String getUploadUrl()
  {
    return uploadUrl;
  }
  
  public final int hashCode()
  {
    String str = uploadUrl;
    int i = 0;
    if (str != null)
    {
      j = str.hashCode();
    }
    else
    {
      j = 0;
      str = null;
    }
    j *= 31;
    Object localObject = downloadUrl;
    int k;
    if (localObject != null)
    {
      k = localObject.hashCode();
    }
    else
    {
      k = 0;
      localObject = null;
    }
    int j = (j + k) * 31;
    localObject = formField;
    if (localObject != null) {
      i = localObject.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("MediaUrl(uploadUrl=");
    Object localObject = uploadUrl;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", downloadUrl=");
    localObject = downloadUrl;
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(", formField=");
    localObject = formField;
    localStringBuilder.append(localObject);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt)
  {
    k.b(paramParcel, "parcel");
    String str = uploadUrl;
    paramParcel.writeString(str);
    str = downloadUrl;
    paramParcel.writeString(str);
    formField.writeToParcel(paramParcel, 0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.MediaUrl
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */