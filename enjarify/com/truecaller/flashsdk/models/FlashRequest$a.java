package com.truecaller.flashsdk.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import c.g.b.k;

public final class FlashRequest$a
  implements Parcelable.Creator
{
  public final Object createFromParcel(Parcel paramParcel)
  {
    k.b(paramParcel, "in");
    FlashRequest localFlashRequest = new com/truecaller/flashsdk/models/FlashRequest;
    String str1 = paramParcel.readString();
    String str2 = paramParcel.readString();
    paramParcel = (Data)Data.CREATOR.createFromParcel(paramParcel);
    localFlashRequest.<init>(str1, str2, paramParcel);
    return localFlashRequest;
  }
  
  public final Object[] newArray(int paramInt)
  {
    return new FlashRequest[paramInt];
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.FlashRequest.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */