package com.truecaller.flashsdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class QueuedFlash
  extends Flash
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR;
  private boolean i = false;
  private boolean j;
  private boolean k;
  private Payload l;
  
  static
  {
    QueuedFlash.1 local1 = new com/truecaller/flashsdk/models/QueuedFlash$1;
    local1.<init>();
    CREATOR = local1;
  }
  
  public QueuedFlash()
  {
    boolean bool = true;
    j = bool;
    k = bool;
  }
  
  private QueuedFlash(Parcel paramParcel)
  {
    boolean bool = true;
    j = bool;
    k = bool;
    Object localObject = Sender.class.getClassLoader();
    localObject = (Sender)paramParcel.readParcelable((ClassLoader)localObject);
    a = ((Sender)localObject);
    long l1 = paramParcel.readLong();
    b = l1;
    localObject = paramParcel.readString();
    c = ((String)localObject);
    localObject = paramParcel.readString();
    d = ((String)localObject);
    localObject = paramParcel.readString();
    e = ((String)localObject);
    localObject = Payload.class.getClassLoader();
    localObject = (Payload)paramParcel.readParcelable((ClassLoader)localObject);
    f = ((Payload)localObject);
    l1 = paramParcel.readLong();
    g = l1;
    localObject = paramParcel.readString();
    h = ((String)localObject);
    localObject = Payload.class.getClassLoader();
    paramParcel = (Payload)paramParcel.readParcelable((ClassLoader)localObject);
    l = paramParcel;
  }
  
  public final void a(Flash paramFlash)
  {
    Object localObject = a;
    a = ((Sender)localObject);
    long l1 = b;
    b = l1;
    localObject = c;
    c = ((String)localObject);
    localObject = d;
    d = ((String)localObject);
    localObject = e;
    e = ((String)localObject);
    localObject = f;
    f = ((Payload)localObject);
    l1 = g;
    g = l1;
    paramFlash = h;
    h = paramFlash;
  }
  
  public final void a(boolean paramBoolean)
  {
    j = paramBoolean;
  }
  
  public final void b(Payload paramPayload)
  {
    l = paramPayload;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public final boolean n()
  {
    return i;
  }
  
  public final void o()
  {
    i = true;
  }
  
  public final boolean p()
  {
    return j;
  }
  
  public final boolean q()
  {
    return k;
  }
  
  public final void r()
  {
    k = false;
  }
  
  public final Payload s()
  {
    return l;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("firstFlash: ");
    boolean bool = i;
    localStringBuilder.append(bool);
    localStringBuilder.append(", updateProgress: ");
    bool = j;
    localStringBuilder.append(bool);
    return localStringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    Object localObject = a;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    long l1 = b;
    paramParcel.writeLong(l1);
    localObject = c;
    paramParcel.writeString((String)localObject);
    localObject = d;
    paramParcel.writeString((String)localObject);
    localObject = e;
    paramParcel.writeString((String)localObject);
    localObject = f;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
    l1 = g;
    paramParcel.writeLong(l1);
    localObject = h;
    paramParcel.writeString((String)localObject);
    localObject = l;
    paramParcel.writeParcelable((Parcelable)localObject, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.QueuedFlash
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */