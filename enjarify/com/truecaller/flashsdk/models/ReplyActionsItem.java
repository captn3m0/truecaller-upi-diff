package com.truecaller.flashsdk.models;

import c.g.b.k;

public final class ReplyActionsItem
{
  private final String action;
  private final String name;
  private final String type;
  
  public ReplyActionsItem()
  {
    this(null, null, null, 7, null);
  }
  
  public ReplyActionsItem(String paramString1, String paramString2, String paramString3)
  {
    name = paramString1;
    action = paramString2;
    type = paramString3;
  }
  
  public final String component1()
  {
    return name;
  }
  
  public final String component2()
  {
    return action;
  }
  
  public final String component3()
  {
    return type;
  }
  
  public final ReplyActionsItem copy(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "name");
    k.b(paramString2, "action");
    k.b(paramString3, "type");
    ReplyActionsItem localReplyActionsItem = new com/truecaller/flashsdk/models/ReplyActionsItem;
    localReplyActionsItem.<init>(paramString1, paramString2, paramString3);
    return localReplyActionsItem;
  }
  
  public final boolean equals(Object paramObject)
  {
    if (this != paramObject)
    {
      boolean bool1 = paramObject instanceof ReplyActionsItem;
      if (bool1)
      {
        paramObject = (ReplyActionsItem)paramObject;
        String str1 = name;
        String str2 = name;
        bool1 = k.a(str1, str2);
        if (bool1)
        {
          str1 = action;
          str2 = action;
          bool1 = k.a(str1, str2);
          if (bool1)
          {
            str1 = type;
            paramObject = type;
            boolean bool2 = k.a(str1, paramObject);
            if (bool2) {
              break label90;
            }
          }
        }
      }
      return false;
    }
    label90:
    return true;
  }
  
  public final String getAction()
  {
    return action;
  }
  
  public final String getName()
  {
    return name;
  }
  
  public final String getType()
  {
    return type;
  }
  
  public final int hashCode()
  {
    String str1 = name;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    String str2 = action;
    int k;
    if (str2 != null)
    {
      k = str2.hashCode();
    }
    else
    {
      k = 0;
      str2 = null;
    }
    int j = (j + k) * 31;
    str2 = type;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("ReplyActionsItem(name=");
    String str = name;
    localStringBuilder.append(str);
    localStringBuilder.append(", action=");
    str = action;
    localStringBuilder.append(str);
    localStringBuilder.append(", type=");
    str = type;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.ReplyActionsItem
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */