package com.truecaller.flashsdk.models;

import android.text.TextUtils;
import com.truecaller.flashsdk.R.layout;

public class a
  implements d
{
  private String a;
  private String b;
  private String c;
  
  public a(String paramString1, String paramString2, String paramString3)
  {
    a = paramString1;
    b = paramString2;
    c = paramString3;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final String b()
  {
    return b;
  }
  
  public final String c()
  {
    return c;
  }
  
  public final int d()
  {
    return R.layout.flashsdk_item_favourite_contact;
  }
  
  public boolean equals(Object paramObject)
  {
    Object localObject1 = paramObject.getClass();
    Object localObject2 = a.class;
    if (localObject1 != localObject2) {
      return false;
    }
    paramObject = (a)paramObject;
    localObject1 = a;
    localObject2 = a;
    boolean bool1 = TextUtils.equals((CharSequence)localObject1, (CharSequence)localObject2);
    if (bool1)
    {
      localObject1 = b;
      localObject2 = b;
      bool1 = TextUtils.equals((CharSequence)localObject1, (CharSequence)localObject2);
      if (bool1)
      {
        localObject1 = c;
        paramObject = c;
        boolean bool2 = TextUtils.equals((CharSequence)localObject1, (CharSequence)paramObject);
        if (bool2) {
          return true;
        }
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.models.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */