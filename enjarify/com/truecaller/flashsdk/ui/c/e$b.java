package com.truecaller.flashsdk.ui.c;

import android.os.CountDownTimer;
import android.widget.ProgressBar;

public final class e$b
  extends CountDownTimer
{
  public e$b(e parame, long paramLong)
  {
    super(paramLong, l);
    parame = e.a(parame);
    int i = (int)paramLong;
    parame.setProgress(i);
  }
  
  public final void onFinish()
  {
    e.b(a);
  }
  
  public final void onTick(long paramLong)
  {
    ProgressBar localProgressBar = e.a(a);
    int i = (int)paramLong;
    localProgressBar.setProgress(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.c.e.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */