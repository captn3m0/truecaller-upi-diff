package com.truecaller.flashsdk.ui.c;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public final class e$a
{
  public static Fragment a(String paramString1, long paramLong1, String paramString2, boolean paramBoolean, long paramLong2)
  {
    e locale = new com/truecaller/flashsdk/ui/c/e;
    locale.<init>();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    locale.setArguments(localBundle);
    String str = f.c();
    localBundle.putString(str, paramString1);
    paramString1 = f.e();
    localBundle.putString(paramString1, paramString2);
    paramString1 = f.f();
    localBundle.putLong(paramString1, paramLong2);
    paramString1 = f.d();
    localBundle.putBoolean(paramString1, paramBoolean);
    localBundle.putLong("time_left", paramLong1);
    return (Fragment)locale;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.c.e.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */