package com.truecaller.flashsdk.ui.c;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.o;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import c.g.b.k;
import c.n.m;
import c.u;
import com.truecaller.flashsdk.R.array;
import com.truecaller.flashsdk.R.attr;
import com.truecaller.flashsdk.R.dimen;
import com.truecaller.flashsdk.R.drawable;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.assist.al;
import com.truecaller.flashsdk.core.c;
import java.util.HashMap;

public final class e
  extends Fragment
  implements View.OnClickListener
{
  public static final e.a d;
  public com.truecaller.flashsdk.assist.g a;
  public al b;
  public com.truecaller.flashsdk.assist.a c;
  private ViewPager e;
  private ProgressBar f;
  private TextView g;
  private ImageView h;
  private TextView i;
  private View j;
  private TextView k;
  private TextView l;
  private e.b m;
  private com.truecaller.flashsdk.ui.whatsnew.b n;
  private HashMap o;
  
  static
  {
    e.a locala = new com/truecaller/flashsdk/ui/c/e$a;
    locala.<init>((byte)0);
    d = locala;
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "message");
    Object localObject = m;
    if (localObject == null)
    {
      str = "countDownTimer";
      k.a(str);
    }
    ((e.b)localObject).cancel();
    localObject = e;
    if (localObject == null)
    {
      str = "tipsPager";
      k.a(str);
    }
    int i1 = 8;
    ((ViewPager)localObject).setVisibility(i1);
    localObject = f;
    if (localObject == null)
    {
      str = "progressBar";
      k.a(str);
    }
    i1 = 0;
    String str = null;
    ((ProgressBar)localObject).setProgress(0);
    localObject = i;
    if (localObject == null)
    {
      str = "statusText";
      k.a(str);
    }
    paramString = (CharSequence)paramString;
    ((TextView)localObject).setText(paramString);
  }
  
  public final void onClick(View paramView)
  {
    k.b(paramView, "view");
    android.support.v4.app.f localf = getActivity();
    if (localf == null) {
      return;
    }
    k.a(localf, "activity ?: return");
    Object localObject1 = getArguments();
    if (localObject1 == null) {
      return;
    }
    k.a(localObject1, "arguments ?: return");
    Object localObject2 = f.f();
    long l1 = ((Bundle)localObject1).getLong((String)localObject2);
    localObject2 = f.e();
    String str1 = ((Bundle)localObject1).getString((String)localObject2);
    int i1 = paramView.getId();
    int i3 = R.id.btnCall;
    int i2;
    Object localObject3;
    Object localObject4;
    if (i1 == i3)
    {
      paramView = a;
      if (paramView == null)
      {
        localObject1 = "deviceUtils";
        k.a((String)localObject1);
      }
      i2 = paramView.d();
      i3 = 0;
      localObject1 = null;
      int i4 = 1;
      if (i2 != 0)
      {
        paramView = new android/content/Intent;
        localObject3 = "android.intent.action.CALL";
        paramView.<init>((String)localObject3);
        int i5 = R.string.tel_num;
        localObject2 = new Object[i4];
        localObject4 = String.valueOf(l1);
        localObject2[0] = localObject4;
        localObject1 = Uri.parse(getString(i5, (Object[])localObject2));
        paramView.setData((Uri)localObject1);
        localf.startActivity(paramView);
      }
      else
      {
        paramView = new android/content/Intent;
        localObject3 = "android.intent.action.VIEW";
        int i6 = R.string.tel_num;
        localObject2 = new Object[i4];
        String str2 = String.valueOf(l1);
        localObject2[0] = str2;
        localObject1 = Uri.parse(getString(i6, (Object[])localObject2));
        paramView.<init>((String)localObject3, (Uri)localObject1);
        localf.startActivity(paramView);
      }
      localf.finish();
      return;
    }
    i3 = R.id.btnSendFlash;
    if (i2 == i3)
    {
      localf.finish();
      localObject3 = c.a();
      localObject4 = localf;
      localObject4 = (Context)localf;
      String str3 = f.g();
      ((com.truecaller.flashsdk.core.b)localObject3).a((Context)localObject4, l1, str1, str3);
    }
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i1 = R.layout.waiting_reply_fragment;
    return paramLayoutInflater.inflate(i1, paramViewGroup, false);
  }
  
  public final void onDestroyView()
  {
    super.onDestroyView();
    Object localObject1 = "ANDROID_FLASH_CLOSE_WAITING";
    Object localObject2 = new android/os/Bundle;
    ((Bundle)localObject2).<init>();
    Object localObject3 = "flash_waiting_timer";
    Object localObject4 = f;
    if (localObject4 == null)
    {
      String str = "progressBar";
      k.a(str);
    }
    int i1 = ((ProgressBar)localObject4).getProgress() / 1000;
    localObject4 = String.valueOf(i1);
    ((Bundle)localObject2).putString((String)localObject3, (String)localObject4);
    localObject3 = c.a();
    ((com.truecaller.flashsdk.core.b)localObject3).a((String)localObject1, (Bundle)localObject2);
    localObject1 = m;
    if (localObject1 == null)
    {
      localObject2 = "countDownTimer";
      k.a((String)localObject2);
    }
    ((e.b)localObject1).cancel();
    localObject1 = n;
    if (localObject1 != null)
    {
      localObject1 = a;
      if (localObject1 != null) {
        ((PopupWindow)localObject1).dismiss();
      }
    }
    localObject1 = o;
    if (localObject1 != null) {
      ((HashMap)localObject1).clear();
    }
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    int i1 = R.id.historyText;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.historyText)");
    paramBundle = (TextView)paramBundle;
    g = paramBundle;
    i1 = R.id.tipsPager;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.tipsPager)");
    paramBundle = (ViewPager)paramBundle;
    e = paramBundle;
    i1 = R.id.progressBarWaiting;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.progressBarWaiting)");
    paramBundle = (ProgressBar)paramBundle;
    f = paramBundle;
    i1 = R.id.centralIcon;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.centralIcon)");
    paramBundle = (ImageView)paramBundle;
    h = paramBundle;
    i1 = R.id.statusText;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.statusText)");
    paramBundle = (TextView)paramBundle;
    i = paramBundle;
    i1 = R.id.replyButtonContainer;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.replyButtonContainer)");
    j = paramBundle;
    i1 = R.id.btnSendFlash;
    paramBundle = paramView.findViewById(i1);
    k.a(paramBundle, "view.findViewById(R.id.btnSendFlash)");
    paramBundle = (TextView)paramBundle;
    k = paramBundle;
    i1 = R.id.btnCall;
    paramView = paramView.findViewById(i1);
    k.a(paramView, "view.findViewById(R.id.btnCall)");
    paramView = (TextView)paramView;
    l = paramView;
    paramView = c.b;
    paramView = c.b();
    paramBundle = new com/truecaller/flashsdk/ui/c/b;
    Object localObject1 = getContext();
    if (localObject1 != null)
    {
      localObject1 = (Activity)localObject1;
      paramBundle.<init>((Activity)localObject1);
      paramView.a(paramBundle).a(this);
      paramView = e;
      if (paramView == null)
      {
        paramBundle = "tipsPager";
        k.a(paramBundle);
      }
      paramBundle = getResources();
      int i2 = R.dimen.control_space;
      i1 = paramBundle.getDimensionPixelSize(i2);
      paramView.setPageMargin(i1);
      paramView = e;
      if (paramView == null)
      {
        paramBundle = "tipsPager";
        k.a(paramBundle);
      }
      i1 = 3;
      paramView.setOffscreenPageLimit(i1);
      paramView = getActivity();
      if (paramView == null) {
        return;
      }
      k.a(paramView, "activity ?: return");
      paramBundle = paramView.getResources();
      if (paramBundle != null)
      {
        i2 = R.array.flash_tips;
        paramBundle = paramBundle.getStringArray(i2);
        if (paramBundle != null)
        {
          localObject1 = e;
          if (localObject1 == null)
          {
            localObject2 = "tipsPager";
            k.a((String)localObject2);
          }
          Object localObject2 = new com/truecaller/flashsdk/ui/c/g;
          ((g)localObject2).<init>(paramBundle);
          localObject2 = (o)localObject2;
          ((ViewPager)localObject1).setAdapter((o)localObject2);
          paramBundle = f;
          if (paramBundle == null)
          {
            localObject1 = "progressBar";
            k.a((String)localObject1);
          }
          paramBundle = paramBundle.getProgressDrawable();
          localObject1 = c;
          if (localObject1 == null)
          {
            localObject2 = "colorProvider";
            k.a((String)localObject2);
          }
          int i4 = R.attr.theme_incoming_secondary_text;
          i2 = ((com.truecaller.flashsdk.assist.a)localObject1).b(i4);
          localObject2 = PorterDuff.Mode.SRC_IN;
          paramBundle.setColorFilter(i2, (PorterDuff.Mode)localObject2);
          paramBundle = f;
          if (paramBundle == null)
          {
            localObject1 = "progressBar";
            k.a((String)localObject1);
          }
          paramBundle = paramBundle.getBackground();
          localObject1 = c;
          if (localObject1 == null)
          {
            localObject2 = "colorProvider";
            k.a((String)localObject2);
          }
          i4 = R.attr.theme_incoming_secondary_text;
          i2 = ((com.truecaller.flashsdk.assist.a)localObject1).b(i4);
          localObject2 = PorterDuff.Mode.SRC_IN;
          paramBundle.setColorFilter(i2, (PorterDuff.Mode)localObject2);
          paramBundle = h;
          if (paramBundle == null)
          {
            localObject1 = "centralIcon";
            k.a((String)localObject1);
          }
          localObject1 = c;
          if (localObject1 == null)
          {
            localObject2 = "colorProvider";
            k.a((String)localObject2);
          }
          i4 = R.attr.theme_incoming_secondary_text;
          i2 = ((com.truecaller.flashsdk.assist.a)localObject1).b(i4);
          localObject2 = PorterDuff.Mode.SRC_IN;
          paramBundle.setColorFilter(i2, (PorterDuff.Mode)localObject2);
          paramBundle = f;
          if (paramBundle == null)
          {
            localObject1 = "progressBar";
            k.a((String)localObject1);
          }
          long l1 = f.b();
          i4 = (int)l1;
          paramBundle.setMax(i4);
          paramBundle = getArguments();
          if (paramBundle == null) {
            return;
          }
          k.a(paramBundle, "arguments ?: return");
          localObject1 = f.c();
          localObject1 = paramBundle.getString((String)localObject1);
          localObject2 = "time_left";
          long l2 = paramBundle.getLong((String)localObject2);
          Object localObject3 = g;
          String str1;
          if (localObject3 == null)
          {
            str1 = "historyText";
            k.a(str1);
          }
          localObject1 = (CharSequence)localObject1;
          ((TextView)localObject3).setText((CharSequence)localObject1);
          localObject1 = new com/truecaller/flashsdk/ui/c/e$b;
          ((e.b)localObject1).<init>(this, l2);
          m = ((e.b)localObject1);
          localObject1 = m;
          if (localObject1 == null)
          {
            localObject2 = "countDownTimer";
            k.a((String)localObject2);
          }
          ((e.b)localObject1).start();
          localObject1 = f.d();
          boolean bool = paramBundle.getBoolean((String)localObject1);
          if (bool)
          {
            localObject1 = paramView.getResources();
            i4 = R.dimen.control_triplespace;
            int i3 = ((Resources)localObject1).getDimensionPixelSize(i4);
            localObject2 = f.e();
            paramBundle = paramBundle.getString((String)localObject2);
            k.a(paramBundle, "arguments.getString(KEY_FLASH_SENT_TO)");
            paramBundle = m.a(paramBundle, " ");
            localObject2 = new com/truecaller/flashsdk/ui/whatsnew/b;
            paramView = (Context)paramView;
            int i5 = R.string.post_flash_popup_1;
            int i6 = 1;
            localObject3 = new Object[i6];
            str1 = null;
            localObject3[0] = paramBundle;
            paramBundle = getString(i5, (Object[])localObject3);
            String str2 = "getString(R.string.post_flash_popup_1, name)";
            k.a(paramBundle, str2);
            i5 = R.drawable.flash_ic_tooltip_center_bottom;
            ((com.truecaller.flashsdk.ui.whatsnew.b)localObject2).<init>(paramView, paramBundle, i5);
            n = ((com.truecaller.flashsdk.ui.whatsnew.b)localObject2);
            paramView = h;
            if (paramView == null)
            {
              paramBundle = "centralIcon";
              k.a(paramBundle);
            }
            paramBundle = new com/truecaller/flashsdk/ui/c/e$c;
            paramBundle.<init>(this, i3);
            paramBundle = (Runnable)paramBundle;
            paramView.post(paramBundle);
          }
          paramView = l;
          if (paramView == null)
          {
            paramBundle = "buttonCall";
            k.a(paramBundle);
          }
          paramBundle = this;
          paramBundle = (View.OnClickListener)this;
          paramView.setOnClickListener(paramBundle);
          paramView = k;
          if (paramView == null)
          {
            localObject1 = "buttonSendFlash";
            k.a((String)localObject1);
          }
          paramView.setOnClickListener(paramBundle);
          return;
        }
      }
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type android.app.Activity");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.c.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */