package com.truecaller.flashsdk.ui.c;

import android.support.v4.view.o;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.flashsdk.R.layout;

public final class g
  extends o
{
  private final String[] a;
  
  public g(String[] paramArrayOfString)
  {
    a = paramArrayOfString;
  }
  
  public final void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    k.b(paramViewGroup, "container");
    k.b(paramObject, "obj");
    paramObject = (View)paramObject;
    paramViewGroup.removeView((View)paramObject);
  }
  
  public final int getCount()
  {
    return a.length;
  }
  
  public final float getPageWidth(int paramInt)
  {
    return h.a();
  }
  
  public final Object instantiateItem(ViewGroup paramViewGroup, int paramInt)
  {
    k.b(paramViewGroup, "container");
    Object localObject1 = LayoutInflater.from(paramViewGroup.getContext());
    int i = R.layout.tips_item_text;
    localObject1 = ((LayoutInflater)localObject1).inflate(i, paramViewGroup, false);
    if (localObject1 != null)
    {
      Object localObject2 = localObject1;
      localObject2 = (TextView)localObject1;
      CharSequence localCharSequence = (CharSequence)a[paramInt];
      ((TextView)localObject2).setText(localCharSequence);
      paramViewGroup.addView((View)localObject1);
      return localObject1;
    }
    paramViewGroup = new c/u;
    paramViewGroup.<init>("null cannot be cast to non-null type android.widget.TextView");
    throw paramViewGroup;
  }
  
  public final boolean isViewFromObject(View paramView, Object paramObject)
  {
    k.b(paramView, "view");
    k.b(paramObject, "obj");
    return k.a(paramView, paramObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.c.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */