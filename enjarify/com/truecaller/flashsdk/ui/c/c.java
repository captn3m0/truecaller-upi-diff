package com.truecaller.flashsdk.ui.c;

import dagger.a.d;
import javax.inject.Provider;

public final class c
  implements d
{
  private final b a;
  private final Provider b;
  
  private c(b paramb, Provider paramProvider)
  {
    a = paramb;
    b = paramProvider;
  }
  
  public static c a(b paramb, Provider paramProvider)
  {
    c localc = new com/truecaller/flashsdk/ui/c/c;
    localc.<init>(paramb, paramProvider);
    return localc;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.c.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */