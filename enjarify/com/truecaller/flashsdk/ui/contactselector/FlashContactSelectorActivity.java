package com.truecaller.flashsdk.ui.contactselector;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import c.g.b.k;
import com.truecaller.flashsdk.R.layout;

public final class FlashContactSelectorActivity
  extends AppCompatActivity
  implements c.c
{
  public final void a()
  {
    finish();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = R.layout.activity_frame;
    setContentView(i);
    paramBundle = getIntent();
    Object localObject = "intent";
    k.a(paramBundle, (String)localObject);
    paramBundle = paramBundle.getExtras();
    if (paramBundle == null) {
      return;
    }
    localObject = new com/truecaller/flashsdk/ui/contactselector/c;
    ((c)localObject).<init>();
    ((c)localObject).setArguments(paramBundle);
    paramBundle = getSupportFragmentManager();
    String str = ((c)localObject).getTag();
    ((c)localObject).show(paramBundle, str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.contactselector.FlashContactSelectorActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */