package com.truecaller.flashsdk.ui.contactselector;

import android.view.View;
import android.view.View.OnClickListener;
import c.g.b.k;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.ui.FlashButton;

final class c$b$a
  implements View.OnClickListener
{
  c$b$a(c.b paramb) {}
  
  public final void onClick(View paramView)
  {
    int i = R.id.flash_button;
    Object localObject = (FlashButton)paramView.findViewById(i);
    ((FlashButton)localObject).onClick(paramView);
    paramView = a.a.a;
    if (paramView == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    paramView.a();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.contactselector.c.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */