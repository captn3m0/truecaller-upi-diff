package com.truecaller.flashsdk.ui.contactselector;

import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import java.util.List;

public final class c$b
  extends RecyclerView.Adapter
{
  private final LayoutInflater b;
  private final List c;
  private final String d;
  
  public c$b(c paramc, LayoutInflater paramLayoutInflater, List paramList, String paramString)
  {
    b = paramLayoutInflater;
    c = paramList;
    d = paramString;
  }
  
  public final int getItemCount()
  {
    return c.size();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.contactselector.c.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */