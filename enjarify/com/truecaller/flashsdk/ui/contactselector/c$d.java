package com.truecaller.flashsdk.ui.contactselector;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.ui.FlashButton;

public final class c$d
  extends RecyclerView.ViewHolder
{
  final FlashButton a;
  final TextView b;
  
  public c$d(View paramView)
  {
    super(paramView);
    int i = R.id.flash_button;
    Object localObject = paramView.findViewById(i);
    k.a(localObject, "view.findViewById(R.id.flash_button)");
    localObject = (FlashButton)localObject;
    a = ((FlashButton)localObject);
    i = R.id.text1;
    paramView = paramView.findViewById(i);
    k.a(paramView, "view.findViewById(R.id.text1)");
    paramView = (TextView)paramView;
    b = paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.contactselector.c.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */