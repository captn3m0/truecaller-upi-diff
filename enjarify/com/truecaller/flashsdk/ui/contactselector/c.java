package com.truecaller.flashsdk.ui.contactselector;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import c.g.b.k;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;
import java.util.HashMap;
import java.util.List;

public final class c
  extends android.support.design.widget.b
  implements f
{
  public static final c.a b;
  public g a;
  private RecyclerView c;
  private c.c d;
  private HashMap e;
  
  static
  {
    c.a locala = new com/truecaller/flashsdk/ui/contactselector/c$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public final List a()
  {
    Object localObject = getArguments();
    if (localObject != null)
    {
      String str = "contact_list";
      localObject = ((Bundle)localObject).getParcelableArrayList(str);
      if (localObject != null) {
        return (List)localObject;
      }
    }
    return null;
  }
  
  public final void a(List paramList, String paramString)
  {
    k.b(paramList, "contacts");
    k.b(paramString, "screenContext");
    RecyclerView localRecyclerView = c;
    if (localRecyclerView == null)
    {
      localObject = "recyclerView";
      k.a((String)localObject);
    }
    Object localObject = new com/truecaller/flashsdk/ui/contactselector/c$b;
    LayoutInflater localLayoutInflater = LayoutInflater.from(getContext());
    k.a(localLayoutInflater, "LayoutInflater.from(context)");
    ((c.b)localObject).<init>(this, localLayoutInflater, paramList, paramString);
    localObject = (RecyclerView.Adapter)localObject;
    localRecyclerView.setAdapter((RecyclerView.Adapter)localObject);
  }
  
  public final String b()
  {
    Object localObject = getArguments();
    if (localObject != null)
    {
      String str = "screen_context";
      localObject = ((Bundle)localObject).getString(str);
      if (localObject != null) {}
    }
    else
    {
      localObject = "";
    }
    return (String)localObject;
  }
  
  public final void c()
  {
    dismissAllowingStateLoss();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      com.truecaller.flashsdk.core.b localb = com.truecaller.flashsdk.core.c.a();
      int i = localb.g();
      ((android.support.v4.app.f)localObject).setTheme(i);
    }
    super.onCreate(paramBundle);
    paramBundle = a.a();
    localObject = com.truecaller.flashsdk.core.c.b;
    localObject = com.truecaller.flashsdk.core.c.b();
    paramBundle = paramBundle.a((com.truecaller.flashsdk.core.a.a.a)localObject);
    localObject = new com/truecaller/flashsdk/ui/contactselector/d;
    ((d)localObject).<init>();
    paramBundle.a((d)localObject).a().a(this);
    paramBundle = getActivity();
    if (paramBundle != null)
    {
      boolean bool = paramBundle instanceof c.c;
      if (bool)
      {
        paramBundle = (c.c)paramBundle;
        d = paramBundle;
        return;
      }
      dismiss();
      return;
    }
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i = R.layout.layout_flash_contact_selector;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    Object localObject = d;
    if (localObject != null) {
      ((c.c)localObject).a();
    }
    localObject = a;
    if (localObject == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((g)localObject).y_();
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    String str = "view";
    k.b(paramView, str);
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.recyclerView;
    paramView = paramView.findViewById(i);
    k.a(paramView, "view.findViewById(R.id.recyclerView)");
    paramView = (RecyclerView)paramView;
    c = paramView;
    paramView = new android/support/v7/widget/LinearLayoutManager;
    paramBundle = getContext();
    paramView.<init>(paramBundle);
    paramBundle = c;
    if (paramBundle == null)
    {
      str = "recyclerView";
      k.a(str);
    }
    paramView = (RecyclerView.LayoutManager)paramView;
    paramBundle.setLayoutManager(paramView);
    paramView = a;
    if (paramView == null)
    {
      paramBundle = "presenter";
      k.a(paramBundle);
    }
    paramView.a(this);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.contactselector.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */