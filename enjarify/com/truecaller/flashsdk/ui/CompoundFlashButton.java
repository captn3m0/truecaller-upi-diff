package com.truecaller.flashsdk.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import com.truecaller.utils.extensions.t;
import java.util.ArrayList;
import java.util.List;

public class CompoundFlashButton
  extends FlashButton
{
  private final List h;
  private final List i;
  private String j;
  private boolean k;
  private Runnable l;
  
  public CompoundFlashButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private CompoundFlashButton(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet, (byte)0);
    paramContext = new com/truecaller/flashsdk/ui/CompoundFlashButton$1;
    paramContext.<init>(this);
    l = paramContext;
    setOnClickListener(this);
    paramContext = new java/util/ArrayList;
    paramContext.<init>();
    h = paramContext;
    paramContext = new java/util/ArrayList;
    paramContext.<init>();
    i = paramContext;
    k = true;
  }
  
  public final void a(List paramList, String paramString1, String paramString2)
  {
    b();
    int m = paramList.size();
    if (m == 0)
    {
      setVisibility(8);
      return;
    }
    m = paramList.size();
    int n = 1;
    if (m == n)
    {
      Long localLong = (Long)paramList.get(0);
      long l1 = localLong.longValue();
      super.a(l1, paramString1, paramString2);
      k = n;
      paramString1 = h;
      paramList = paramList.get(0);
      paramString1.add(paramList);
    }
    else
    {
      paramString2 = h;
      paramString2.addAll(paramList);
      b = paramString1;
      k = false;
    }
    setVisibility(0);
  }
  
  public final void a(List paramList1, List paramList2, String paramString)
  {
    b();
    int m = paramList1.size();
    int n = 8;
    if (m == 0)
    {
      setVisibility(n);
      return;
    }
    m = paramList1.size();
    int i1 = 1;
    List localList;
    if (m == i1)
    {
      long l1 = ((Long)paramList1.get(0)).longValue();
      String str = (String)paramList2.get(0);
      super.a(l1, str, paramString);
      k = i1;
      localList = h;
      paramList1 = paramList1.get(0);
      localList.add(paramList1);
      paramList1 = i;
      paramList2 = paramList2.get(0);
      paramList1.add(paramList2);
      j = paramString;
    }
    else
    {
      int i2 = paramList1.size();
      m = paramList2.size();
      if (i2 != m)
      {
        setVisibility(n);
        return;
      }
      i2 = 0;
      paramString = null;
      for (;;)
      {
        m = paramList1.size();
        if (i2 >= m) {
          break;
        }
        localList = h;
        Object localObject = paramList1.get(i2);
        localList.add(localObject);
        localList = i;
        localObject = paramList2.get(i2);
        localList.add(localObject);
        i2 += 1;
      }
      k = false;
    }
    setVisibility(0);
  }
  
  public final boolean a()
  {
    boolean bool = k;
    if (bool)
    {
      bool = super.a();
      if (!bool) {
        return false;
      }
    }
    return true;
  }
  
  public final void b()
  {
    super.b();
    h.clear();
    i.clear();
  }
  
  public void onClick(View paramView)
  {
    t.a(paramView, false, 2);
    paramView = l;
    postDelayed(paramView, 50);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.CompoundFlashButton
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */