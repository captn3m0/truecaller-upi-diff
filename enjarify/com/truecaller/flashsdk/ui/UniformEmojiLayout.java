package com.truecaller.flashsdk.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.flashsdk.R.layout;
import com.truecaller.flashsdk.a.d;

public final class UniformEmojiLayout
  extends ViewGroup
  implements View.OnClickListener
{
  private int a;
  private final ViewGroup.LayoutParams b;
  private int c;
  private int d;
  private d[] e;
  private UniformEmojiLayout.a f;
  
  public UniformEmojiLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private UniformEmojiLayout(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    paramContext = new android/view/ViewGroup$LayoutParams;
    paramContext.<init>(-2, -1);
    b = paramContext;
  }
  
  public final UniformEmojiLayout.a getOnItemClickListener()
  {
    return f;
  }
  
  public final void onClick(View paramView)
  {
    k.b(paramView, "v");
    UniformEmojiLayout.a locala = f;
    if (locala != null)
    {
      int i = paramView.getId();
      locala.a(i);
      return;
    }
  }
  
  protected final void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    removeAllViewsInLayout();
    Object localObject1 = e;
    paramInt1 = 0;
    if (localObject1 != null)
    {
      paramBoolean = localObject1.length;
    }
    else
    {
      paramBoolean = false;
      localObject1 = null;
    }
    paramInt3 = getMeasuredWidth();
    int i = getPaddingLeft();
    paramInt3 -= i;
    i = getPaddingRight();
    i = paramInt3 - i;
    paramInt3 = 0;
    int j;
    for (;;)
    {
      j = 1;
      if (paramInt3 >= paramBoolean) {
        break label295;
      }
      Object localObject2 = getContext();
      int m = R.layout.recent_emoji_item;
      int n = 0;
      Object localObject3 = null;
      localObject2 = View.inflate((Context)localObject2, m, null);
      if (localObject2 == null) {
        break;
      }
      localObject2 = (TextView)localObject2;
      Object localObject4 = localObject2;
      localObject4 = (View)localObject2;
      Object localObject5 = b;
      addViewInLayout((View)localObject4, paramInt3, (ViewGroup.LayoutParams)localObject5, j);
      ((TextView)localObject2).setId(paramInt3);
      localObject5 = e;
      if (localObject5 != null)
      {
        localObject5 = localObject5[paramInt3];
        if (localObject5 != null) {
          localObject3 = ((d)localObject5).a();
        }
      }
      localObject3 = (CharSequence)localObject3;
      ((TextView)localObject2).setText((CharSequence)localObject3);
      localObject3 = this;
      localObject3 = (View.OnClickListener)this;
      ((TextView)localObject2).setOnClickListener((View.OnClickListener)localObject3);
      n = c;
      int i1 = d;
      measureChild((View)localObject4, n, i1);
      n = ((TextView)localObject2).getMeasuredWidth();
      i -= n;
      if (i < 0)
      {
        removeViewInLayout((View)localObject4);
        paramBoolean = ((TextView)localObject2).getMeasuredWidth();
        i += paramBoolean;
        break label295;
      }
      paramInt3 += 1;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type android.widget.TextView");
    throw ((Throwable)localObject1);
    label295:
    paramBoolean = getChildCount() + j;
    i /= paramBoolean;
    a = i;
    paramBoolean = getPaddingLeft();
    paramInt3 = a;
    paramBoolean += paramInt3;
    paramInt3 = getChildCount();
    while (paramInt1 < paramInt3)
    {
      View localView = getChildAt(paramInt1);
      String str = "textView";
      k.a(localView, str);
      int k = localView.getMeasuredWidth() + paramBoolean;
      localView.layout(paramBoolean, paramInt2, k, paramInt4);
      paramBoolean = a + k;
      paramInt1 += 1;
    }
  }
  
  protected final void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    c = paramInt1;
    d = paramInt2;
  }
  
  public final void setEmojis(d[] paramArrayOfd)
  {
    k.b(paramArrayOfd, "emojiList");
    e = paramArrayOfd;
    requestLayout();
  }
  
  public final void setOnItemClickListener(UniformEmojiLayout.a parama)
  {
    f = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.UniformEmojiLayout
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */