package com.truecaller.flashsdk.ui.onboarding;

import android.os.Bundle;
import c.g.b.z;
import c.n.m;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.assist.aa;
import com.truecaller.flashsdk.assist.al;
import com.truecaller.flashsdk.core.i;
import com.truecaller.flashsdk.models.FlashContact;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class b
  implements a
{
  private FlashContact a;
  private boolean b;
  private boolean c;
  private int d;
  private long e;
  private final c f;
  private final aa g;
  private final al h;
  
  public b(c paramc, aa paramaa, al paramal)
  {
    f = paramc;
    g = paramaa;
    h = paramal;
  }
  
  public final void a()
  {
    boolean bool = b;
    if (!bool)
    {
      bool = c;
      if (!bool)
      {
        c localc = f;
        localc.b();
      }
    }
  }
  
  public final void a(int paramInt)
  {
    int i = R.id.btnSend;
    long l1;
    if (paramInt == i)
    {
      l1 = e;
      long l2 = 0L;
      paramInt = l1 < l2;
      if (paramInt == 0)
      {
        f.h();
        return;
      }
      f.e();
      return;
    }
    i = R.id.tryFlashYourself;
    if (paramInt == i)
    {
      Object localObject1 = a;
      if (localObject1 == null) {
        return;
      }
      Object localObject2 = (CharSequence)a;
      Object localObject3 = new c/n/k;
      ((c.n.k)localObject3).<init>("^[+]");
      Object localObject4 = "";
      c.g.b.k.b(localObject2, "input");
      String str1 = "replacement";
      c.g.b.k.b(localObject4, str1);
      localObject2 = a.matcher((CharSequence)localObject2).replaceFirst((String)localObject4);
      localObject3 = "nativePattern.matcher(in…replaceFirst(replacement)";
      c.g.b.k.a(localObject2, (String)localObject3);
      localObject2 = m.d((String)localObject2);
      if (localObject2 != null)
      {
        l1 = ((Long)localObject2).longValue();
        localObject4 = (CharSequence)c;
        str1 = null;
        int j = 1;
        if (localObject4 != null)
        {
          bool = m.a((CharSequence)localObject4);
          if (!bool)
          {
            bool = false;
            localObject4 = null;
            break label205;
          }
        }
        boolean bool = true;
        label205:
        if (bool)
        {
          localObject1 = b;
        }
        else
        {
          localObject4 = z.a;
          int k = 2;
          Object[] arrayOfObject = new Object[k];
          String str2 = b;
          arrayOfObject[0] = str2;
          localObject1 = c;
          arrayOfObject[j] = localObject1;
          localObject1 = Arrays.copyOf(arrayOfObject, k);
          localObject1 = String.format("%s %s", (Object[])localObject1);
          localObject4 = "java.lang.String.format(format, *args)";
          c.g.b.k.a(localObject1, (String)localObject4);
        }
        localObject4 = f;
        ((c)localObject4).a((String)localObject1, l1);
      }
      else {}
    }
  }
  
  public final void a(Bundle paramBundle)
  {
    Object localObject1 = g;
    Object localObject2 = "first_time_user";
    Object localObject3 = Boolean.FALSE;
    ((aa)localObject1).a((String)localObject2, localObject3);
    long l1 = 0L;
    if (paramBundle != null)
    {
      localObject3 = "to_phone";
      l2 = paramBundle.getLong((String)localObject3);
    }
    else
    {
      l2 = l1;
    }
    e = l2;
    long l2 = e;
    boolean bool = l2 < l1;
    int i;
    if (!bool)
    {
      paramBundle = h;
      i = R.string.got_it;
      localObject2 = new Object[0];
      paramBundle = paramBundle.a(i, (Object[])localObject2);
      i = 0;
      localObject1 = null;
    }
    else
    {
      localObject1 = com.truecaller.flashsdk.core.c.a().e();
      if (localObject1 != null)
      {
        localObject1 = ((i)localObject1).C();
      }
      else
      {
        i = 0;
        localObject1 = null;
      }
      a = ((FlashContact)localObject1);
      localObject1 = a;
      int j = 1;
      if (localObject1 != null)
      {
        i = 1;
      }
      else
      {
        i = 0;
        localObject1 = null;
      }
      localObject3 = h;
      int k = R.string.intro_send_a_flash_to;
      localObject2 = new Object[j];
      String str;
      if (paramBundle != null)
      {
        str = "to_name";
        paramBundle = paramBundle.getString(str);
        if (paramBundle != null) {}
      }
      else
      {
        paramBundle = new java/lang/StringBuilder;
        str = "+";
        paramBundle.<init>(str);
        long l3 = e;
        paramBundle.append(l3);
        paramBundle = paramBundle.toString();
      }
      localObject2[0] = paramBundle;
      paramBundle = ((al)localObject3).a(k, (Object[])localObject2);
    }
    f.a(i, paramBundle);
  }
  
  public final void b()
  {
    boolean bool = b;
    if (!bool)
    {
      bool = c;
      if (!bool)
      {
        c localc = f;
        localc.c();
      }
    }
  }
  
  public final void c()
  {
    b = true;
    f.g();
    f.d();
  }
  
  public final void d()
  {
    boolean bool = b;
    if (!bool)
    {
      int i = d;
      int j = 1;
      i += j;
      d = i;
      i = d;
      int k = 2;
      if (i < k)
      {
        f.b();
        return;
      }
      c = j;
      c localc = f;
      localc.f();
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.onboarding.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */