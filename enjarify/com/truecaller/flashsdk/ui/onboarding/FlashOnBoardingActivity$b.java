package com.truecaller.flashsdk.ui.onboarding;

import android.animation.AnimatorSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

final class FlashOnBoardingActivity$b
  implements View.OnTouchListener
{
  FlashOnBoardingActivity$b(FlashOnBoardingActivity paramFlashOnBoardingActivity) {}
  
  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    FlashOnBoardingActivity.a(a).end();
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.onboarding.FlashOnBoardingActivity.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */