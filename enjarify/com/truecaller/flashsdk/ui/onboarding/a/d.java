package com.truecaller.flashsdk.ui.onboarding.a;

import javax.inject.Provider;

public final class d
  implements dagger.a.d
{
  private final b a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private d(b paramb, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramb;
    b = paramProvider1;
    c = paramProvider2;
    d = paramProvider3;
  }
  
  public static d a(b paramb, Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    d locald = new com/truecaller/flashsdk/ui/onboarding/a/d;
    locald.<init>(paramb, paramProvider1, paramProvider2, paramProvider3);
    return locald;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.onboarding.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */