package com.truecaller.flashsdk.ui.onboarding;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Property;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import c.u;
import com.airbnb.lottie.LottieAnimationView;
import com.truecaller.flashsdk.R.dimen;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;
import com.truecaller.flashsdk.R.style;
import com.truecaller.flashsdk.ui.customviews.ArrowView;
import com.truecaller.flashsdk.ui.customviews.BouncingView;
import com.truecaller.flashsdk.ui.customviews.BouncingView.b;
import com.truecaller.flashsdk.ui.onboarding.a.a.a;
import com.truecaller.flashsdk.ui.onboarding.a.b;
import com.truecaller.flashsdk.ui.onboarding.a.e;
import com.truecaller.flashsdk.ui.send.SendActivity;

public final class FlashOnBoardingActivity
  extends AppCompatActivity
  implements View.OnClickListener, BouncingView.b, c
{
  public static final FlashOnBoardingActivity.a b;
  public a a;
  private BouncingView c;
  private ImageView d;
  private CardView e;
  private View f;
  private View g;
  private final AnimatorSet h;
  private Button i;
  private TextView j;
  private LottieAnimationView k;
  private ArrowView l;
  
  static
  {
    FlashOnBoardingActivity.a locala = new com/truecaller/flashsdk/ui/onboarding/FlashOnBoardingActivity$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public FlashOnBoardingActivity()
  {
    AnimatorSet localAnimatorSet = new android/animation/AnimatorSet;
    localAnimatorSet.<init>();
    h = localAnimatorSet;
  }
  
  public final void K_()
  {
    a locala = a;
    if (locala == null)
    {
      String str = "onBoardingPresenter";
      c.g.b.k.a(str);
    }
    locala.c();
  }
  
  public final void a(String paramString, long paramLong)
  {
    c.g.b.k.b(paramString, "yourName");
    Object localObject = getIntent().clone();
    if (localObject != null)
    {
      localObject = (Intent)localObject;
      ((Intent)localObject).putExtra("to_phone", paramLong);
      ((Intent)localObject).putExtra("to_name", paramString);
      paramString = this;
      paramString = (Context)this;
      ((Intent)localObject).setClass(paramString, SendActivity.class);
      startActivity((Intent)localObject);
      finish();
      return;
    }
    paramString = new c/u;
    paramString.<init>("null cannot be cast to non-null type android.content.Intent");
    throw paramString;
  }
  
  public final void a(boolean paramBoolean, String paramString)
  {
    c.g.b.k.b(paramString, "footerText");
    int m = R.id.bouncingView;
    Object localObject1 = findViewById(m);
    c.g.b.k.a(localObject1, "findViewById(R.id.bouncingView)");
    localObject1 = (BouncingView)localObject1;
    c = ((BouncingView)localObject1);
    m = R.id.overLayViewContainer;
    localObject1 = findViewById(m);
    c.g.b.k.a(localObject1, "findViewById(R.id.overLayViewContainer)");
    g = ((View)localObject1);
    m = R.id.imageContentBottom;
    localObject1 = findViewById(m);
    c.g.b.k.a(localObject1, "findViewById(R.id.imageContentBottom)");
    localObject1 = (ImageView)localObject1;
    d = ((ImageView)localObject1);
    m = R.id.imageContainerBottom;
    localObject1 = findViewById(m);
    c.g.b.k.a(localObject1, "findViewById(R.id.imageContainerBottom)");
    localObject1 = (CardView)localObject1;
    e = ((CardView)localObject1);
    m = R.id.flashIntroContainer;
    localObject1 = findViewById(m);
    c.g.b.k.a(localObject1, "findViewById(R.id.flashIntroContainer)");
    f = ((View)localObject1);
    m = R.id.btnSend;
    localObject1 = findViewById(m);
    c.g.b.k.a(localObject1, "findViewById(R.id.btnSend)");
    localObject1 = (Button)localObject1;
    i = ((Button)localObject1);
    m = R.id.tryFlashYourself;
    localObject1 = findViewById(m);
    c.g.b.k.a(localObject1, "findViewById(R.id.tryFlashYourself)");
    localObject1 = (TextView)localObject1;
    j = ((TextView)localObject1);
    m = R.id.imageOverlayBackground;
    localObject1 = findViewById(m);
    c.g.b.k.a(localObject1, "findViewById(R.id.imageOverlayBackground)");
    localObject1 = (LottieAnimationView)localObject1;
    k = ((LottieAnimationView)localObject1);
    m = R.id.arrowView;
    localObject1 = findViewById(m);
    String str = "findViewById(R.id.arrowView)";
    c.g.b.k.a(localObject1, str);
    localObject1 = (ArrowView)localObject1;
    l = ((ArrowView)localObject1);
    localObject1 = i;
    if (localObject1 == null)
    {
      str = "sendButton";
      c.g.b.k.a(str);
    }
    paramString = (CharSequence)paramString;
    ((Button)localObject1).setText(paramString);
    paramString = c;
    if (paramString == null)
    {
      localObject1 = "bouncingView";
      c.g.b.k.a((String)localObject1);
    }
    localObject1 = this;
    localObject1 = (BouncingView.b)this;
    boolean bool = true;
    paramString.a((BouncingView.b)localObject1, bool);
    paramString = c;
    if (paramString == null)
    {
      localObject1 = "bouncingView";
      c.g.b.k.a((String)localObject1);
    }
    m = R.id.overLayViewContainer;
    paramString.setDragViewResId(m);
    paramString = g;
    if (paramString == null)
    {
      localObject1 = "layerView";
      c.g.b.k.a((String)localObject1);
    }
    localObject1 = new com/truecaller/flashsdk/ui/onboarding/FlashOnBoardingActivity$b;
    ((FlashOnBoardingActivity.b)localObject1).<init>(this);
    localObject1 = (View.OnTouchListener)localObject1;
    paramString.setOnTouchListener((View.OnTouchListener)localObject1);
    paramString = g;
    if (paramString == null)
    {
      localObject1 = "layerView";
      c.g.b.k.a((String)localObject1);
    }
    localObject1 = View.TRANSLATION_Y;
    int n = 2;
    Object localObject2 = new float[n];
    localObject2[0] = 0.0F;
    Object localObject3 = getResources();
    int i1 = R.dimen.bouncing_view_jump;
    float f1 = -((Resources)localObject3).getDimension(i1);
    localObject2[bool] = f1;
    paramString = ObjectAnimator.ofFloat(paramString, (Property)localObject1, (float[])localObject2);
    c.g.b.k.a(paramString, "animator");
    long l1 = com.truecaller.flashsdk.ui.incoming.k.d();
    paramString.setDuration(l1);
    m = com.truecaller.flashsdk.ui.incoming.k.b();
    l1 = m;
    paramString.setStartDelay(l1);
    localObject1 = new android/view/animation/DecelerateInterpolator;
    ((DecelerateInterpolator)localObject1).<init>();
    localObject1 = (TimeInterpolator)localObject1;
    paramString.setInterpolator((TimeInterpolator)localObject1);
    localObject1 = g;
    if (localObject1 == null)
    {
      localObject2 = "layerView";
      c.g.b.k.a((String)localObject2);
    }
    localObject2 = View.TRANSLATION_Y;
    localObject3 = new float[n];
    Resources localResources = getResources();
    int i2 = R.dimen.bouncing_view_jump;
    float f2 = -localResources.getDimension(i2);
    localObject3[0] = f2;
    localObject3[bool] = 0.0F;
    localObject1 = ObjectAnimator.ofFloat(localObject1, (Property)localObject2, (float[])localObject3);
    c.g.b.k.a(localObject1, "animator1");
    long l2 = com.truecaller.flashsdk.ui.incoming.k.d() * 2;
    ((ObjectAnimator)localObject1).setDuration(l2);
    int i3 = com.truecaller.flashsdk.ui.incoming.k.e();
    l2 = i3;
    ((ObjectAnimator)localObject1).setStartDelay(l2);
    localObject2 = new android/view/animation/BounceInterpolator;
    ((BounceInterpolator)localObject2).<init>();
    localObject2 = (TimeInterpolator)localObject2;
    ((ObjectAnimator)localObject1).setInterpolator((TimeInterpolator)localObject2);
    localObject2 = h;
    Animator[] arrayOfAnimator = new Animator[n];
    paramString = (Animator)paramString;
    arrayOfAnimator[0] = paramString;
    localObject1 = (Animator)localObject1;
    arrayOfAnimator[bool] = localObject1;
    ((AnimatorSet)localObject2).playSequentially(arrayOfAnimator);
    paramString = h;
    localObject1 = new com/truecaller/flashsdk/ui/onboarding/FlashOnBoardingActivity$c;
    ((FlashOnBoardingActivity.c)localObject1).<init>(this);
    localObject1 = (Animator.AnimatorListener)localObject1;
    paramString.addListener((Animator.AnimatorListener)localObject1);
    paramString = i;
    if (paramString == null)
    {
      localObject1 = "sendButton";
      c.g.b.k.a((String)localObject1);
    }
    localObject1 = this;
    localObject1 = (View.OnClickListener)this;
    paramString.setOnClickListener((View.OnClickListener)localObject1);
    paramString = j;
    if (paramString == null)
    {
      str = "tryFlashWithYourself";
      c.g.b.k.a(str);
    }
    paramString.setOnClickListener((View.OnClickListener)localObject1);
    if (!paramBoolean)
    {
      localObject4 = j;
      if (localObject4 == null)
      {
        paramString = "tryFlashWithYourself";
        c.g.b.k.a(paramString);
      }
      int i4 = 8;
      ((TextView)localObject4).setVisibility(i4);
    }
    Object localObject4 = k;
    if (localObject4 == null)
    {
      paramString = "logoLottieView";
      c.g.b.k.a(paramString);
    }
    ((LottieAnimationView)localObject4).setProgress(1.0F);
  }
  
  public final void b()
  {
    h.start();
  }
  
  public final void c()
  {
    h.end();
  }
  
  public final void d()
  {
    Object localObject = d;
    if (localObject == null)
    {
      String str = "bottomImage";
      c.g.b.k.a(str);
    }
    localObject = ((ImageView)localObject).getDrawable();
    if (localObject != null)
    {
      ((AnimationDrawable)localObject).start();
      return;
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type android.graphics.drawable.AnimationDrawable");
    throw ((Throwable)localObject);
  }
  
  public final void e()
  {
    Object localObject1 = getIntent().clone();
    if (localObject1 != null)
    {
      localObject1 = (Intent)localObject1;
      Object localObject2 = this;
      localObject2 = (Context)this;
      ((Intent)localObject1).setClass((Context)localObject2, SendActivity.class);
      startActivity((Intent)localObject1);
      finish();
      return;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type android.content.Intent");
    throw ((Throwable)localObject1);
  }
  
  public final void f()
  {
    Object localObject1 = k;
    if (localObject1 == null)
    {
      localObject2 = "logoLottieView";
      c.g.b.k.a((String)localObject2);
    }
    ((LottieAnimationView)localObject1).a();
    localObject1 = k;
    if (localObject1 == null)
    {
      localObject2 = "logoLottieView";
      c.g.b.k.a((String)localObject2);
    }
    Object localObject2 = new com/truecaller/flashsdk/ui/onboarding/FlashOnBoardingActivity$d;
    ((FlashOnBoardingActivity.d)localObject2).<init>(this);
    localObject2 = (Animator.AnimatorListener)localObject2;
    ((LottieAnimationView)localObject1).a((Animator.AnimatorListener)localObject2);
    localObject1 = l;
    if (localObject1 == null)
    {
      localObject2 = "arrowView";
      c.g.b.k.a((String)localObject2);
    }
    localObject2 = null;
    ((ArrowView)localObject1).setVisibility(0);
    localObject1 = l;
    if (localObject1 == null)
    {
      localObject2 = "arrowView";
      c.g.b.k.a((String)localObject2);
    }
    ((ArrowView)localObject1).b();
  }
  
  public final void g()
  {
    Object localObject = k;
    String str;
    if (localObject == null)
    {
      str = "logoLottieView";
      c.g.b.k.a(str);
    }
    ((LottieAnimationView)localObject).d();
    localObject = l;
    if (localObject == null)
    {
      str = "arrowView";
      c.g.b.k.a(str);
    }
    ((ArrowView)localObject).a();
  }
  
  public final void h()
  {
    finish();
  }
  
  public final void onClick(View paramView)
  {
    c.g.b.k.b(paramView, "v");
    a locala = a;
    if (locala == null)
    {
      String str = "onBoardingPresenter";
      c.g.b.k.a(str);
    }
    int m = paramView.getId();
    locala.a(m);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    int m = R.style.DefaultV2;
    setTheme(m);
    super.onCreate(paramBundle);
    int n = R.layout.layout_onbaording_v2;
    setContentView(n);
    paramBundle = com.truecaller.flashsdk.ui.onboarding.a.a.a();
    Object localObject1 = com.truecaller.flashsdk.core.c.b;
    localObject1 = com.truecaller.flashsdk.core.c.b();
    paramBundle = paramBundle.a((com.truecaller.flashsdk.core.a.a.a)localObject1);
    localObject1 = new com/truecaller/flashsdk/ui/onboarding/a/b;
    Object localObject2 = this;
    localObject2 = (c)this;
    ((b)localObject1).<init>((c)localObject2);
    paramBundle.a((b)localObject1).a().a(this);
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject1 = "onBoardingPresenter";
      c.g.b.k.a((String)localObject1);
    }
    localObject1 = getIntent();
    c.g.b.k.a(localObject1, "intent");
    localObject1 = ((Intent)localObject1).getExtras();
    paramBundle.a((Bundle)localObject1);
  }
  
  public final void onStart()
  {
    super.onStart();
    a locala = a;
    if (locala == null)
    {
      String str = "onBoardingPresenter";
      c.g.b.k.a(str);
    }
    locala.a();
  }
  
  public final void onStop()
  {
    super.onStop();
    a locala = a;
    if (locala == null)
    {
      String str = "onBoardingPresenter";
      c.g.b.k.a(str);
    }
    locala.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.onboarding.FlashOnBoardingActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */