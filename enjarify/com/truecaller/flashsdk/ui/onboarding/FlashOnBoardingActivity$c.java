package com.truecaller.flashsdk.ui.onboarding;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import c.g.b.k;

public final class FlashOnBoardingActivity$c
  extends AnimatorListenerAdapter
{
  FlashOnBoardingActivity$c(FlashOnBoardingActivity paramFlashOnBoardingActivity) {}
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    paramAnimator = a.a;
    if (paramAnimator == null)
    {
      String str = "onBoardingPresenter";
      k.a(str);
    }
    paramAnimator.d();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.onboarding.FlashOnBoardingActivity.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */