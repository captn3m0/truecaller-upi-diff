package com.truecaller.flashsdk.ui.onboarding;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.airbnb.lottie.LottieAnimationView;

public final class FlashOnBoardingActivity$d
  extends AnimatorListenerAdapter
{
  FlashOnBoardingActivity$d(FlashOnBoardingActivity paramFlashOnBoardingActivity) {}
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    paramAnimator = FlashOnBoardingActivity.b(a);
    Object localObject = new com/truecaller/flashsdk/ui/onboarding/FlashOnBoardingActivity$d$a;
    ((FlashOnBoardingActivity.d.a)localObject).<init>(this);
    localObject = (Runnable)localObject;
    paramAnimator.postDelayed((Runnable)localObject, 4000L);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.onboarding.FlashOnBoardingActivity.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */