package com.truecaller.flashsdk.ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.truecaller.flashsdk.R.color;
import com.truecaller.flashsdk.R.layout;
import com.truecaller.flashsdk.core.Theme;
import com.truecaller.flashsdk.core.c;
import java.util.List;
import java.util.Locale;

final class CompoundFlashButton$a
  extends BaseAdapter
{
  Dialog a;
  final LayoutInflater b;
  
  CompoundFlashButton$a(CompoundFlashButton paramCompoundFlashButton)
  {
    paramCompoundFlashButton = LayoutInflater.from(a);
    b = paramCompoundFlashButton;
  }
  
  public final int getCount()
  {
    return CompoundFlashButton.d(c).size();
  }
  
  public final Object getItem(int paramInt)
  {
    return CompoundFlashButton.d(c).get(paramInt);
  }
  
  public final long getItemId(int paramInt)
  {
    return ((Long)CompoundFlashButton.d(c).get(paramInt)).longValue();
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    Object localObject1 = b;
    int i = R.layout.flashsdk_item_select_number;
    if (paramView == null)
    {
      paramView = ((LayoutInflater)localObject1).inflate(i, paramViewGroup, false);
      paramViewGroup = new com/truecaller/flashsdk/ui/CompoundFlashButton$b;
      localObject1 = c;
      paramViewGroup.<init>((CompoundFlashButton)localObject1, paramView);
      paramView.setTag(paramViewGroup);
    }
    else
    {
      paramViewGroup = (CompoundFlashButton.b)paramView.getTag();
    }
    localObject1 = a;
    i = c.c;
    Object localObject2 = c.f;
    ((FlashButton)localObject1).a(i, (PorterDuff.Mode)localObject2);
    localObject1 = a;
    i = c.d;
    localObject2 = c.g;
    ((FlashButton)localObject1).b(i, (PorterDuff.Mode)localObject2);
    localObject1 = a;
    i = c.e;
    ((FlashButton)localObject1).setButtonColor(i);
    localObject1 = CompoundFlashButton.2.a;
    Object localObject3 = c.a().f();
    i = ((Theme)localObject3).ordinal();
    int j = localObject1[i];
    int k;
    switch (j)
    {
    default: 
      localObject1 = b;
      localObject3 = c.a;
      k = R.color.light_primary_text;
      i = android.support.v4.content.b.c((Context)localObject3, k);
      ((TextView)localObject1).setTextColor(i);
      localObject1 = c.a;
      i = R.color.light_bg;
      j = android.support.v4.content.b.c((Context)localObject1, i);
      paramView.setBackgroundColor(j);
      break;
    case 6: 
      localObject1 = b;
      localObject3 = c.a;
      k = R.color.light_gray_primary_text;
      i = android.support.v4.content.b.c((Context)localObject3, k);
      ((TextView)localObject1).setTextColor(i);
      localObject1 = c.a;
      i = R.color.light_gray_bg;
      j = android.support.v4.content.b.c((Context)localObject1, i);
      paramView.setBackgroundColor(j);
      break;
    case 5: 
      localObject1 = b;
      localObject3 = c.a;
      k = R.color.pitch_black_primary_text;
      i = android.support.v4.content.b.c((Context)localObject3, k);
      ((TextView)localObject1).setTextColor(i);
      localObject1 = c.a;
      i = R.color.pitch_black_bg;
      j = android.support.v4.content.b.c((Context)localObject1, i);
      paramView.setBackgroundColor(j);
      break;
    case 4: 
      localObject1 = b;
      localObject3 = c.a;
      k = R.color.ramadan_primary_text;
      i = android.support.v4.content.b.c((Context)localObject3, k);
      ((TextView)localObject1).setTextColor(i);
      localObject1 = c.a;
      i = R.color.ramadan_bg;
      j = android.support.v4.content.b.c((Context)localObject1, i);
      paramView.setBackgroundColor(j);
      break;
    case 3: 
      localObject1 = b;
      localObject3 = c.a;
      k = R.color.coffee_primary_text;
      i = android.support.v4.content.b.c((Context)localObject3, k);
      ((TextView)localObject1).setTextColor(i);
      localObject1 = c.a;
      i = R.color.coffee_bg;
      j = android.support.v4.content.b.c((Context)localObject1, i);
      paramView.setBackgroundColor(j);
      break;
    case 2: 
      localObject1 = b;
      localObject3 = c.a;
      k = R.color.light_primary_text;
      i = android.support.v4.content.b.c((Context)localObject3, k);
      ((TextView)localObject1).setTextColor(i);
      localObject1 = c.a;
      i = R.color.light_bg;
      j = android.support.v4.content.b.c((Context)localObject1, i);
      paramView.setBackgroundColor(j);
      break;
    case 1: 
      localObject1 = b;
      localObject3 = c.a;
      k = R.color.dark_primary_text;
      i = android.support.v4.content.b.c((Context)localObject3, k);
      ((TextView)localObject1).setTextColor(i);
      localObject1 = c.a;
      i = R.color.dark_bg;
      j = android.support.v4.content.b.c((Context)localObject1, i);
      paramView.setBackgroundColor(j);
    }
    localObject1 = CompoundFlashButton.e(c);
    j = ((List)localObject1).size();
    if (j > 0)
    {
      localObject1 = b;
      localObject3 = (CharSequence)CompoundFlashButton.e(c).get(paramInt);
      ((TextView)localObject1).setText((CharSequence)localObject3);
    }
    else
    {
      localObject1 = b;
      localObject3 = Locale.getDefault();
      localObject2 = "+%d";
      int m = 1;
      Object[] arrayOfObject = new Object[m];
      Object localObject4 = CompoundFlashButton.d(c).get(paramInt);
      arrayOfObject[0] = localObject4;
      localObject3 = String.format((Locale)localObject3, (String)localObject2, arrayOfObject);
      ((TextView)localObject1).setText((CharSequence)localObject3);
    }
    localObject1 = CompoundFlashButton.e(c);
    j = ((List)localObject1).size();
    long l;
    if (j > paramInt)
    {
      localObject1 = a;
      localObject3 = (Long)CompoundFlashButton.d(c).get(paramInt);
      l = ((Long)localObject3).longValue();
      localObject5 = (String)CompoundFlashButton.e(c).get(paramInt);
      localObject2 = CompoundFlashButton.f(c);
      ((FlashButton)localObject1).a(l, (String)localObject5, (String)localObject2);
    }
    else
    {
      localObject1 = a;
      localObject3 = CompoundFlashButton.d(c);
      l = ((Long)((List)localObject3).get(paramInt)).longValue();
      localObject5 = c.b;
      localObject2 = CompoundFlashButton.f(c);
      ((FlashButton)localObject1).a(l, (String)localObject5, (String)localObject2);
    }
    Object localObject5 = a;
    paramViewGroup = new com/truecaller/flashsdk/ui/-$$Lambda$CompoundFlashButton$a$VCr1px40bgDq1mi-EF4yE2zkd0M;
    paramViewGroup.<init>(this);
    ((FlashButton)localObject5).setPostOnClickListener(paramViewGroup);
    localObject5 = -..Lambda.CompoundFlashButton.a.XwHDia4AXYN5hLPPSuOiIOxaSjc.INSTANCE;
    paramView.setOnClickListener((View.OnClickListener)localObject5);
    return paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.CompoundFlashButton.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */