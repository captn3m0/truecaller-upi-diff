package com.truecaller.flashsdk.ui;

import android.view.View;
import android.widget.TextView;
import com.truecaller.flashsdk.R.id;

final class CompoundFlashButton$b
{
  final FlashButton a;
  final TextView b;
  
  public CompoundFlashButton$b(CompoundFlashButton paramCompoundFlashButton, View paramView)
  {
    int i = R.id.flash_button;
    paramCompoundFlashButton = (FlashButton)paramView.findViewById(i);
    a = paramCompoundFlashButton;
    i = R.id.text1;
    paramCompoundFlashButton = (TextView)paramView.findViewById(i);
    b = paramCompoundFlashButton;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.CompoundFlashButton.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */