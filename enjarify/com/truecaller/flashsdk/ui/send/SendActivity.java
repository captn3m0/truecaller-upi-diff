package com.truecaller.flashsdk.ui.send;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.d.b.ab;
import com.d.b.ag;
import com.d.b.w;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.truecaller.flashsdk.R.drawable;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.assist.p;
import com.truecaller.flashsdk.core.FlashMediaService.a;
import com.truecaller.flashsdk.models.ImageFlash;
import com.truecaller.flashsdk.ui.base.BaseFlashActivity;
import com.truecaller.flashsdk.ui.c.e.a;
import com.truecaller.flashsdk.ui.customviews.FlashAddBackgroundButton;
import com.truecaller.flashsdk.ui.customviews.FlashContactHeaderView;
import com.truecaller.flashsdk.ui.customviews.FlashContactHeaderView.a;
import com.truecaller.flashsdk.ui.customviews.FlashSendFooterView;
import com.truecaller.flashsdk.ui.customviews.FlashSendFooterView.a;
import com.truecaller.flashsdk.ui.customviews.b.a;
import java.util.HashMap;

public final class SendActivity
  extends BaseFlashActivity
  implements ActionMode.Callback, OnMapReadyCallback, OnCompleteListener, FlashContactHeaderView.a, FlashSendFooterView.a, c
{
  public static final SendActivity.a q;
  private FlashAddBackgroundButton A;
  private final SendActivity.g B;
  private final SendActivity.o C;
  private final ag D;
  private HashMap E;
  private View r;
  private EditText s;
  private EditText t;
  private EditText u;
  private View v;
  private ImageView w;
  private View x;
  private EditText y;
  private ActionMode z;
  
  static
  {
    SendActivity.a locala = new com/truecaller/flashsdk/ui/send/SendActivity$a;
    locala.<init>((byte)0);
    q = locala;
  }
  
  public SendActivity()
  {
    Object localObject = new com/truecaller/flashsdk/ui/send/SendActivity$g;
    ((SendActivity.g)localObject).<init>(this);
    B = ((SendActivity.g)localObject);
    localObject = new com/truecaller/flashsdk/ui/send/SendActivity$o;
    ((SendActivity.o)localObject).<init>(this);
    C = ((SendActivity.o)localObject);
    localObject = new com/truecaller/flashsdk/ui/send/SendActivity$n;
    ((SendActivity.n)localObject).<init>(this);
    localObject = (ag)localObject;
    D = ((ag)localObject);
  }
  
  public final void A()
  {
    super.A();
    Object localObject = s;
    if (localObject == null)
    {
      str1 = "flashText";
      k.a(str1);
    }
    String str1 = null;
    ((EditText)localObject).setText(null);
    localObject = t;
    String str2;
    if (localObject == null)
    {
      str2 = "imageText";
      k.a(str2);
    }
    ((EditText)localObject).setText(null);
    localObject = v;
    if (localObject == null)
    {
      str1 = "bodyContainer";
      k.a(str1);
    }
    str1 = null;
    ((View)localObject).setVisibility(0);
    localObject = s;
    if (localObject == null)
    {
      str2 = "flashText";
      k.a(str2);
    }
    ((EditText)localObject).setVisibility(0);
  }
  
  public final void B()
  {
    int i = R.id.flashMapView;
    Object localObject1 = (MapView)findViewById(i);
    m = ((MapView)localObject1);
    i = R.id.mapContentTextSendV2;
    localObject1 = (EditText)findViewById(i);
    y = ((EditText)localObject1);
    localObject1 = m;
    if (localObject1 != null)
    {
      ((MapView)localObject1).a();
      Object localObject2 = this;
      localObject2 = (OnMapReadyCallback)this;
      ((MapView)localObject1).a((OnMapReadyCallback)localObject2);
      ((MapView)localObject1).b();
      return;
    }
  }
  
  public final void C()
  {
    Object localObject1 = s;
    if (localObject1 == null)
    {
      localObject2 = "flashText";
      k.a((String)localObject2);
    }
    boolean bool = false;
    ((EditText)localObject1).setVisibility(0);
    localObject1 = g();
    int i = 8;
    ((View)localObject1).setVisibility(i);
    ((FlashSendFooterView)e()).a(false);
    localObject1 = (FlashSendFooterView)e();
    Object localObject2 = s;
    if (localObject2 == null)
    {
      String str = "flashText";
      k.a(str);
    }
    bool = TextUtils.isEmpty((CharSequence)((EditText)localObject2).getText()) ^ true;
    ((FlashSendFooterView)localObject1).c(bool);
  }
  
  public final void D()
  {
    Object localObject1 = s;
    if (localObject1 == null)
    {
      str1 = "flashText";
      k.a(str1);
    }
    String str1 = null;
    ((EditText)localObject1).setVisibility(0);
    localObject1 = g();
    int i = 8;
    ((View)localObject1).setVisibility(i);
    ((FlashSendFooterView)e()).a(false);
    ((FlashSendFooterView)e()).h();
    localObject1 = (FlashSendFooterView)e();
    Object localObject2 = s;
    if (localObject2 == null)
    {
      String str2 = "flashText";
      k.a(str2);
    }
    localObject2 = (CharSequence)((EditText)localObject2).getText();
    boolean bool = TextUtils.isEmpty((CharSequence)localObject2) ^ true;
    ((FlashSendFooterView)localObject1).c(bool);
    int j = R.id.flashMapContainerV2;
    localObject1 = (FrameLayout)e(j);
    if (localObject1 != null) {
      ((FrameLayout)localObject1).setVisibility(i);
    }
    localObject1 = y;
    if (localObject1 != null) {
      ((EditText)localObject1).setVisibility(i);
    }
    j = R.id.body_container;
    localObject1 = (ScrollView)e(j);
    localObject2 = "body_container";
    k.a(localObject1, (String)localObject2);
    ((ScrollView)localObject1).setVisibility(0);
    j().setVisibility(i);
    localObject1 = A;
    if (localObject1 != null) {
      ((FlashAddBackgroundButton)localObject1).setVisibility(0);
    }
    localObject1 = x;
    String str3;
    if (localObject1 == null)
    {
      str3 = "emojiContainer";
      k.a(str3);
    }
    ((View)localObject1).setVisibility(0);
    localObject1 = w;
    if (localObject1 == null)
    {
      str3 = "closeReplyContact";
      k.a(str3);
    }
    ((ImageView)localObject1).setVisibility(0);
    localObject1 = m;
    if (localObject1 != null) {
      ((MapView)localObject1).c();
    }
    localObject1 = m;
    if (localObject1 != null)
    {
      ((MapView)localObject1).d();
      return;
    }
  }
  
  public final void E()
  {
    FlashSendFooterView localFlashSendFooterView = (FlashSendFooterView)e();
    int i = R.string.tip_use_location;
    localFlashSendFooterView.c(i);
  }
  
  public final void F()
  {
    EditText localEditText = s;
    if (localEditText == null)
    {
      String str = "flashText";
      k.a(str);
    }
    localEditText.requestFocus();
  }
  
  public final void G()
  {
    EditText localEditText = s;
    if (localEditText == null)
    {
      localObject = "flashText";
      k.a((String)localObject);
    }
    Object localObject = new com/truecaller/flashsdk/ui/send/SendActivity$j;
    ((SendActivity.j)localObject).<init>(this);
    localObject = (Runnable)localObject;
    localEditText.postDelayed((Runnable)localObject, 200L);
  }
  
  public final void H()
  {
    EditText localEditText = y;
    if (localEditText != null)
    {
      Object localObject = new com/truecaller/flashsdk/ui/send/SendActivity$i;
      ((SendActivity.i)localObject).<init>(this);
      localObject = (Runnable)localObject;
      localEditText.postDelayed((Runnable)localObject, 200L);
      return;
    }
  }
  
  public final void I()
  {
    ((FlashSendFooterView)e()).e();
    FlashAddBackgroundButton localFlashAddBackgroundButton = A;
    if (localFlashAddBackgroundButton != null)
    {
      localFlashAddBackgroundButton.setVisibility(8);
      return;
    }
  }
  
  public final void J()
  {
    ((FlashSendFooterView)e()).f();
    FlashAddBackgroundButton localFlashAddBackgroundButton = A;
    if (localFlashAddBackgroundButton != null)
    {
      localFlashAddBackgroundButton.setVisibility(0);
      return;
    }
  }
  
  public final void K()
  {
    ((FlashSendFooterView)e()).e();
  }
  
  public final void L()
  {
    ((FlashSendFooterView)e()).j();
  }
  
  public final void M()
  {
    ((FlashSendFooterView)e()).f();
  }
  
  public final void N()
  {
    ActionMode localActionMode = z;
    if (localActionMode != null)
    {
      localActionMode.finish();
      return;
    }
  }
  
  public final void N_()
  {
    Object localObject = g();
    int i = ((View)localObject).getVisibility();
    String str;
    if (i == 0)
    {
      localObject = t;
      if (localObject == null)
      {
        str = "imageText";
        k.a(str);
      }
      localObject = ((EditText)localObject).getText().toString();
    }
    else
    {
      localObject = i();
      i = ((View)localObject).getVisibility();
      if (i == 0)
      {
        localObject = u;
        if (localObject == null)
        {
          str = "videoText";
          k.a(str);
        }
        localObject = ((EditText)localObject).getText().toString();
      }
      else
      {
        localObject = y;
        if (localObject != null)
        {
          i = ((EditText)localObject).getVisibility();
          if (i == 0)
          {
            localObject = y;
            if (localObject != null)
            {
              localObject = ((EditText)localObject).getText();
            }
            else
            {
              i = 0;
              localObject = null;
            }
            localObject = String.valueOf(localObject);
            break label154;
          }
        }
        localObject = s;
        if (localObject == null)
        {
          str = "flashText";
          k.a(str);
        }
        localObject = ((EditText)localObject).getText().toString();
      }
    }
    label154:
    ((i)c()).d_((String)localObject);
  }
  
  public final void O()
  {
    com.truecaller.flashsdk.a.a locala = k;
    if (locala != null)
    {
      boolean bool1 = locala.isShowing();
      Object localObject1 = k;
      if (localObject1 != null)
      {
        localObject1 = ((com.truecaller.flashsdk.a.a)localObject1).c();
        if (localObject1 != null)
        {
          boolean bool2 = ((Boolean)localObject1).booleanValue();
          i locali = (i)c();
          Object localObject2 = s;
          if (localObject2 == null)
          {
            String str = "flashText";
            k.a(str);
          }
          localObject2 = ((EditText)localObject2).getText().toString();
          locali.a((String)localObject2, bool1, bool2);
          return;
        }
      }
      return;
    }
  }
  
  public final void P()
  {
    EditText localEditText = s;
    if (localEditText == null)
    {
      localObject = "flashText";
      k.a((String)localObject);
    }
    Object localObject = new com/truecaller/flashsdk/ui/send/SendActivity$h;
    ((SendActivity.h)localObject).<init>(this);
    localObject = (Runnable)localObject;
    localEditText.postDelayed((Runnable)localObject, 200L);
  }
  
  public final void Q()
  {
    com.truecaller.flashsdk.ui.whatsnew.b localb = new com/truecaller/flashsdk/ui/whatsnew/b;
    Object localObject = this;
    localObject = (Context)this;
    int i = R.string.tip_send_edit_text;
    String str1 = getString(i);
    String str2 = "getString(R.string.tip_send_edit_text)";
    k.a(str1, str2);
    localb.<init>((Context)localObject, str1);
    localObject = s;
    if (localObject == null)
    {
      str1 = "flashText";
      k.a(str1);
    }
    localObject = (View)localObject;
    localb.a((View)localObject, 0);
  }
  
  public final void R()
  {
    com.truecaller.flashsdk.a.a locala = k;
    if (locala != null) {
      locala.d();
    }
    com.truecaller.utils.extensions.t.a((View)f(), false, 2);
  }
  
  public final void S()
  {
    int i = R.id.addPhoto;
    Object localObject1 = (FlashAddBackgroundButton)findViewById(i);
    A = ((FlashAddBackgroundButton)localObject1);
    i = R.id.closeButtonContact;
    localObject1 = findViewById(i);
    Object localObject2 = "findViewById(R.id.closeButtonContact)";
    k.a(localObject1, (String)localObject2);
    localObject1 = (ImageView)localObject1;
    w = ((ImageView)localObject1);
    i = R.id.imageBackgroundV2;
    localObject1 = (ImageView)findViewById(i);
    this.j = ((ImageView)localObject1);
    localObject1 = w;
    if (localObject1 == null)
    {
      localObject2 = "closeReplyContact";
      k.a((String)localObject2);
    }
    int j = -1;
    Object localObject3 = PorterDuff.Mode.SRC_IN;
    ((ImageView)localObject1).setColorFilter(j, (PorterDuff.Mode)localObject3);
    localObject1 = s;
    if (localObject1 == null)
    {
      localObject2 = "flashText";
      k.a((String)localObject2);
    }
    localObject2 = new com/truecaller/flashsdk/ui/send/SendActivity$c;
    localObject3 = (i)c();
    ((SendActivity.c)localObject2).<init>((i)localObject3);
    localObject2 = (c.g.a.b)localObject2;
    com.truecaller.flashsdk.assist.t.a((EditText)localObject1, (c.g.a.b)localObject2);
    com.truecaller.flashsdk.assist.t.a((EditText)localObject1);
    j = 0;
    localObject2 = null;
    ((EditText)localObject1).setVisibility(0);
    localObject3 = this;
    localObject3 = (ActionMode.Callback)this;
    ((EditText)localObject1).setCustomSelectionActionModeCallback((ActionMode.Callback)localObject3);
    localObject1 = A;
    if (localObject1 != null) {
      ((FlashAddBackgroundButton)localObject1).setVisibility(0);
    }
    localObject1 = w;
    if (localObject1 == null)
    {
      localObject3 = "closeReplyContact";
      k.a((String)localObject3);
    }
    ((ImageView)localObject1).setVisibility(0);
    localObject1 = j();
    j = 8;
    ((Toolbar)localObject1).setVisibility(j);
    localObject1 = A;
    if (localObject1 != null)
    {
      localObject2 = new com/truecaller/flashsdk/ui/send/SendActivity$d;
      ((SendActivity.d)localObject2).<init>(this);
      localObject2 = (View.OnClickListener)localObject2;
      ((FlashAddBackgroundButton)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    }
    localObject1 = w;
    if (localObject1 == null)
    {
      localObject2 = "closeReplyContact";
      k.a((String)localObject2);
    }
    localObject2 = new com/truecaller/flashsdk/ui/send/SendActivity$e;
    ((SendActivity.e)localObject2).<init>(this);
    localObject2 = (View.OnClickListener)localObject2;
    ((ImageView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
  }
  
  public final void T()
  {
    int i = R.id.flashImageContainerV2;
    Object localObject1 = (FrameLayout)e(i);
    k.a(localObject1, "flashImageContainerV2");
    Object localObject2 = null;
    ((FrameLayout)localObject1).setVisibility(0);
    localObject1 = w;
    String str;
    if (localObject1 == null)
    {
      str = "closeReplyContact";
      k.a(str);
    }
    ((ImageView)localObject1).setVisibility(0);
    localObject1 = j();
    int j = 8;
    ((Toolbar)localObject1).setVisibility(j);
    localObject1 = A;
    if (localObject1 != null) {
      ((FlashAddBackgroundButton)localObject1).setVisibility(0);
    }
    localObject1 = s;
    if (localObject1 == null)
    {
      str = "flashText";
      k.a(str);
    }
    ((EditText)localObject1).setVisibility(0);
    localObject1 = x;
    if (localObject1 == null)
    {
      str = "emojiContainer";
      k.a(str);
    }
    ((View)localObject1).setVisibility(0);
    localObject1 = A;
    if (localObject1 != null)
    {
      localObject2 = b;
      j = R.drawable.ic_flash_outline_remove_photo_alternate;
      ((ImageView)localObject2).setImageResource(j);
      localObject2 = a;
      localObject1 = ((FlashAddBackgroundButton)localObject1).getResources();
      j = R.string.flash_remove_photo;
      localObject1 = (CharSequence)((Resources)localObject1).getString(j);
      ((TextView)localObject2).setText((CharSequence)localObject1);
    }
    ((FlashSendFooterView)e()).c(true);
  }
  
  public final void U()
  {
    int i = R.id.flashImageContainerV2;
    Object localObject1 = (FrameLayout)e(i);
    Object localObject2 = "flashImageContainerV2";
    k.a(localObject1, (String)localObject2);
    int j = 8;
    ((FrameLayout)localObject1).setVisibility(j);
    localObject1 = w;
    if (localObject1 == null)
    {
      localObject2 = "closeReplyContact";
      k.a((String)localObject2);
    }
    j = 0;
    localObject2 = null;
    ((ImageView)localObject1).setVisibility(0);
    localObject1 = s;
    if (localObject1 == null)
    {
      localObject3 = "flashText";
      k.a((String)localObject3);
    }
    ((EditText)localObject1).setVisibility(0);
    localObject1 = x;
    if (localObject1 == null)
    {
      localObject3 = "emojiContainer";
      k.a((String)localObject3);
    }
    ((View)localObject1).setVisibility(0);
    ((FlashSendFooterView)e()).h();
    localObject1 = (FlashSendFooterView)e();
    Object localObject3 = s;
    if (localObject3 == null)
    {
      String str = "flashText";
      k.a(str);
    }
    localObject3 = (CharSequence)((EditText)localObject3).getText();
    boolean bool = TextUtils.isEmpty((CharSequence)localObject3) ^ true;
    ((FlashSendFooterView)localObject1).c(bool);
    localObject1 = A;
    if (localObject1 != null) {
      ((FlashAddBackgroundButton)localObject1).setVisibility(0);
    }
    localObject1 = A;
    if (localObject1 != null)
    {
      localObject2 = b;
      int k = R.drawable.ic_flash_outline_add_photo_alternate;
      ((ImageView)localObject2).setImageResource(k);
      localObject2 = a;
      localObject1 = ((FlashAddBackgroundButton)localObject1).getResources();
      k = R.string.flash_add_photo;
      localObject1 = (CharSequence)((Resources)localObject1).getString(k);
      ((TextView)localObject2).setText((CharSequence)localObject1);
    }
    localObject1 = this.j;
    if (localObject1 != null)
    {
      ((ImageView)localObject1).setBackground(null);
      return;
    }
  }
  
  public final void V()
  {
    int i = R.id.flashImageContainerV2;
    Object localObject = (FrameLayout)e(i);
    String str1 = "flashImageContainerV2";
    k.a(localObject, str1);
    int j = 8;
    ((FrameLayout)localObject).setVisibility(j);
    localObject = j();
    String str2 = null;
    ((Toolbar)localObject).setVisibility(0);
    localObject = w;
    if (localObject == null)
    {
      str2 = "closeReplyContact";
      k.a(str2);
    }
    ((ImageView)localObject).setVisibility(j);
    localObject = A;
    if (localObject != null)
    {
      ((FlashAddBackgroundButton)localObject).setVisibility(j);
      return;
    }
  }
  
  public final void W()
  {
    int i = R.id.flashMapContainerV2;
    Object localObject = (FrameLayout)e(i);
    int j = 8;
    if (localObject != null) {
      ((FrameLayout)localObject).setVisibility(j);
    }
    localObject = s;
    if (localObject == null)
    {
      str = "flashText";
      k.a(str);
    }
    ((EditText)localObject).setVisibility(j);
    localObject = j();
    String str = null;
    ((Toolbar)localObject).setVisibility(0);
    localObject = w;
    if (localObject == null)
    {
      str = "closeReplyContact";
      k.a(str);
    }
    ((ImageView)localObject).setVisibility(j);
    localObject = A;
    if (localObject != null)
    {
      ((FlashAddBackgroundButton)localObject).setVisibility(j);
      return;
    }
  }
  
  public final void X()
  {
    Object localObject = this;
    localObject = android.support.v4.content.d.a((Context)this);
    BroadcastReceiver localBroadcastReceiver = (BroadcastReceiver)C;
    IntentFilter localIntentFilter = new android/content/IntentFilter;
    localIntentFilter.<init>("action_image_flash");
    ((android.support.v4.content.d)localObject).a(localBroadcastReceiver, localIntentFilter);
  }
  
  public final void Y()
  {
    Object localObject = this;
    localObject = android.support.v4.content.d.a((Context)this);
    BroadcastReceiver localBroadcastReceiver = (BroadcastReceiver)B;
    IntentFilter localIntentFilter = new android/content/IntentFilter;
    localIntentFilter.<init>("type_flash_received");
    ((android.support.v4.content.d)localObject).a(localBroadcastReceiver, localIntentFilter);
  }
  
  public final void a(float paramFloat)
  {
    EditText localEditText = s;
    if (localEditText == null)
    {
      String str = "flashText";
      k.a(str);
    }
    localEditText.setTextSize(paramFloat);
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    FlashContactHeaderView localFlashContactHeaderView = f();
    Drawable localDrawable = com.truecaller.utils.ui.b.c((Context)this, paramInt1);
    localFlashContactHeaderView.setBackground(localDrawable);
    f().setHeaderTextColor(paramInt2);
  }
  
  public final void a(GoogleMap paramGoogleMap)
  {
    p = paramGoogleMap;
    ((i)c()).i();
  }
  
  public final void a(ImageFlash paramImageFlash)
  {
    k.b(paramImageFlash, "imageFlash");
    paramImageFlash = FlashMediaService.a.a((Context)this, paramImageFlash);
    startService(paramImageFlash);
  }
  
  public final void a(String paramString, int paramInt1, int paramInt2, int paramInt3)
  {
    k.b(paramString, "text");
    Object localObject = s;
    if (localObject == null)
    {
      String str1 = "flashText";
      k.a(str1);
    }
    localObject = ((EditText)localObject).getText();
    paramString = (CharSequence)paramString;
    ((Editable)localObject).replace(paramInt1, paramInt2, paramString);
    paramString = s;
    if (paramString == null)
    {
      String str2 = "flashText";
      k.a(str2);
    }
    paramString.setSelection(paramInt3);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "location");
    k.b(paramString2, "lat");
    k.b(paramString3, "long");
    SendActivity.b localb = new com/truecaller/flashsdk/ui/send/SendActivity$b;
    Object localObject1 = this;
    localObject1 = (Context)this;
    localb.<init>((Context)localObject1, paramString1);
    paramString1 = p;
    if (paramString1 != null)
    {
      int i = R.id.flashMapContainerV2;
      localObject1 = (FrameLayout)e(i);
      if (localObject1 != null) {
        ((FrameLayout)localObject1).setVisibility(0);
      }
      localObject1 = y;
      if (localObject1 != null) {
        ((EditText)localObject1).setVisibility(0);
      }
      i = R.id.body_container;
      localObject1 = (ScrollView)e(i);
      Object localObject2 = "body_container";
      k.a(localObject1, (String)localObject2);
      int j = 8;
      ((ScrollView)localObject1).setVisibility(j);
      j().setVisibility(j);
      localObject1 = A;
      if (localObject1 != null) {
        ((FlashAddBackgroundButton)localObject1).setVisibility(j);
      }
      localObject1 = s;
      String str;
      if (localObject1 == null)
      {
        str = "flashText";
        k.a(str);
      }
      ((EditText)localObject1).setVisibility(j);
      localObject1 = x;
      if (localObject1 == null)
      {
        str = "emojiContainer";
        k.a(str);
      }
      ((View)localObject1).setVisibility(j);
      localObject1 = w;
      if (localObject1 == null)
      {
        localObject2 = "closeReplyContact";
        k.a((String)localObject2);
      }
      ((ImageView)localObject1).setVisibility(0);
      ((FlashSendFooterView)e()).k();
      localObject1 = y;
      if (localObject1 != null)
      {
        localObject2 = this;
        localObject2 = (ActionMode.Callback)this;
        ((EditText)localObject1).setCustomSelectionActionModeCallback((ActionMode.Callback)localObject2);
      }
      localObject1 = (FlashSendFooterView)e();
      j = 1;
      ((FlashSendFooterView)localObject1).c(j);
      localObject1 = localb;
      localObject1 = (GoogleMap.InfoWindowAdapter)localb;
      paramString1.a((GoogleMap.InfoWindowAdapter)localObject1);
      double d1 = Double.parseDouble(paramString2);
      double d2 = Double.parseDouble(paramString3);
      p.a(paramString1, d1, d2);
      paramString1 = w;
      if (paramString1 == null)
      {
        localObject1 = "closeReplyContact";
        k.a((String)localObject1);
      }
      localObject1 = new com/truecaller/flashsdk/ui/send/SendActivity$k;
      ((SendActivity.k)localObject1).<init>(this, localb, paramString2, paramString3);
      localObject1 = (View.OnClickListener)localObject1;
      paramString1.setOnClickListener((View.OnClickListener)localObject1);
      paramString1 = y;
      if (paramString1 != null)
      {
        paramString1.setVisibility(0);
        paramString2 = new com/truecaller/flashsdk/ui/send/SendActivity$l;
        paramString3 = (i)c();
        paramString2.<init>(paramString3);
        paramString2 = (c.g.a.b)paramString2;
        com.truecaller.flashsdk.assist.t.a(paramString1, paramString2);
        com.truecaller.flashsdk.assist.t.a(paramString1);
        return;
      }
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    k.b(paramString1, "placeName");
    k.b(paramString2, "locationMessage");
    k.b(paramString3, "lat");
    k.b(paramString4, "long");
    a(paramString1, paramString3, paramString4);
  }
  
  public final void a(String paramString1, String paramString2, boolean paramBoolean, long paramLong1, long paramLong2)
  {
    SendActivity localSendActivity = this;
    Object localObject1 = paramString1;
    k.b(paramString1, "history");
    k.b(paramString2, "name");
    Object localObject2 = s;
    if (localObject2 == null)
    {
      localObject3 = "flashText";
      k.a((String)localObject3);
    }
    com.truecaller.utils.extensions.t.a((View)localObject2, false, 2);
    ((FlashSendFooterView)e()).d();
    localObject2 = (FlashSendFooterView)e();
    int i = 8;
    ((FlashSendFooterView)localObject2).setVisibility(i);
    localObject2 = v;
    if (localObject2 == null)
    {
      String str = "bodyContainer";
      k.a(str);
    }
    ((View)localObject2).setVisibility(i);
    localObject2 = getSupportFragmentManager().a();
    int j = R.id.waiting_container;
    Object localObject3 = com.truecaller.flashsdk.ui.c.e.d;
    localObject1 = paramString1;
    localObject1 = e.a.a(paramString1, paramLong1, paramString2, paramBoolean, paramLong2);
    localObject2 = ((o)localObject2).b(j, (Fragment)localObject1);
    int k = 4097;
    ((o)localObject2).a(k).d();
    localObject2 = k;
    if (localObject2 != null) {
      ((com.truecaller.flashsdk.a.a)localObject2).d();
    }
    q();
    int m = R.id.waiting_container;
    localObject2 = findViewById(m);
    k.a(localObject2, "view");
    ((View)localObject2).setVisibility(0);
  }
  
  public final void a(String paramString, boolean paramBoolean)
  {
    k.b(paramString, "message");
    EditText localEditText = s;
    if (localEditText == null)
    {
      String str = "flashText";
      k.a(str);
    }
    localEditText.setEnabled(paramBoolean);
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    localEditText.setText((CharSequence)localObject);
    int i = paramString.length();
    localEditText.setSelection(i);
  }
  
  public final void b(Uri paramUri)
  {
    k.b(paramUri, "uri");
    paramUri = d().a(paramUri);
    ag localag = D;
    paramUri.a(localag);
  }
  
  public final void b(String paramString1, String paramString2)
  {
    k.b(paramString1, "imageUrl");
    String str = "message";
    k.b(paramString2, str);
    super.b(paramString1, paramString2);
    paramString1 = s;
    if (paramString1 == null)
    {
      str = "flashText";
      k.a(str);
    }
    int i = 8;
    paramString1.setVisibility(i);
    paramString1 = t;
    if (paramString1 == null)
    {
      str = "imageText";
      k.a(str);
    }
    paramString2 = (CharSequence)paramString2;
    paramString1.setText(paramString2);
    paramString1 = t;
    if (paramString1 == null)
    {
      paramString2 = "imageText";
      k.a(paramString2);
    }
    paramString2 = t;
    if (paramString2 == null)
    {
      str = "imageText";
      k.a(str);
    }
    int j = paramString2.getText().length();
    paramString1.setSelection(j);
    ((FlashSendFooterView)e()).c(true);
  }
  
  public final void c(boolean paramBoolean)
  {
    EditText localEditText = s;
    if (localEditText == null)
    {
      String str = "flashText";
      k.a(str);
    }
    localEditText.setCursorVisible(paramBoolean);
  }
  
  public final void d(String paramString1, String paramString2)
  {
    k.b(paramString1, "videoUrl");
    String str = "message";
    k.b(paramString2, str);
    super.d(paramString1, paramString2);
    paramString1 = s;
    if (paramString1 == null)
    {
      str = "flashText";
      k.a(str);
    }
    int i = 8;
    paramString1.setVisibility(i);
    paramString1 = u;
    if (paramString1 == null)
    {
      str = "videoText";
      k.a(str);
    }
    paramString2 = (CharSequence)paramString2;
    paramString1.setText(paramString2);
    paramString1 = u;
    if (paramString1 == null)
    {
      paramString2 = "videoText";
      k.a(paramString2);
    }
    paramString2 = t;
    if (paramString2 == null)
    {
      str = "imageText";
      k.a(str);
    }
    int j = paramString2.getText().length();
    paramString1.setSelection(j);
    ((FlashSendFooterView)e()).c(true);
  }
  
  public final void d(boolean paramBoolean)
  {
    ((FlashSendFooterView)e()).c(paramBoolean);
  }
  
  public final View e(int paramInt)
  {
    Object localObject1 = E;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      E = ((HashMap)localObject1);
    }
    localObject1 = E;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = E;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void e(String paramString)
  {
    k.b(paramString, "hint");
    EditText localEditText = s;
    if (localEditText == null)
    {
      String str = "flashText";
      k.a(str);
    }
    paramString = (CharSequence)paramString;
    localEditText.setHint(paramString);
  }
  
  public final void e(String paramString1, String paramString2)
  {
    k.b(paramString1, "placeName");
    k.b(paramString2, "locationImageUrl");
    Object localObject = t;
    if (localObject == null)
    {
      String str = "imageText";
      k.a(str);
    }
    paramString1 = (CharSequence)paramString1;
    ((EditText)localObject).setText(paramString1);
    paramString1 = s;
    if (paramString1 == null)
    {
      localObject = "flashText";
      k.a((String)localObject);
    }
    paramString1.setVisibility(8);
    g().setVisibility(0);
    paramString1 = d().a(paramString2);
    int i = R.drawable.ic_map_placeholder;
    paramString1 = paramString1.a(i);
    paramString2 = h();
    localObject = null;
    paramString1.a(paramString2, null);
    paramString1 = t;
    if (paramString1 == null)
    {
      paramString2 = "imageText";
      k.a(paramString2);
    }
    paramString2 = t;
    if (paramString2 == null)
    {
      localObject = "imageText";
      k.a((String)localObject);
    }
    paramString2 = paramString2.getText();
    i = paramString2.length();
    paramString1.setSelection(i);
    paramString1 = (FlashSendFooterView)e();
    i = 1;
    paramString1.a(i);
    paramString1 = t;
    if (paramString1 == null)
    {
      paramString2 = "imageText";
      k.a(paramString2);
    }
    paramString1.requestFocus();
  }
  
  public final void f(int paramInt)
  {
    i locali = (i)c();
    Object localObject1 = s;
    if (localObject1 == null)
    {
      localObject2 = "flashText";
      k.a((String)localObject2);
    }
    localObject1 = ((EditText)localObject1).getText().toString();
    Object localObject2 = s;
    if (localObject2 == null)
    {
      localObject3 = "flashText";
      k.a((String)localObject3);
    }
    int i = ((EditText)localObject2).getSelectionStart();
    Object localObject3 = s;
    if (localObject3 == null)
    {
      String str = "flashText";
      k.a(str);
    }
    int j = ((EditText)localObject3).getSelectionEnd();
    locali.a((String)localObject1, paramInt, i, j);
  }
  
  public final void f(String paramString)
  {
    k.b(paramString, "message");
    FlashSendFooterView localFlashSendFooterView = (FlashSendFooterView)e();
    Object localObject = new com/truecaller/flashsdk/ui/send/SendActivity$m;
    ((SendActivity.m)localObject).<init>(this, paramString);
    localObject = (Runnable)localObject;
    localFlashSendFooterView.postDelayed((Runnable)localObject, 200L);
  }
  
  public final void f(String paramString1, String paramString2)
  {
    k.b(paramString1, "url");
    k.b(paramString2, "description");
    paramString1 = d().a(paramString1);
    Object localObject = D;
    paramString1.a((ag)localObject);
    paramString1 = s;
    if (paramString1 == null)
    {
      localObject = "flashText";
      k.a((String)localObject);
    }
    paramString2 = (CharSequence)paramString2;
    paramString1.setText(paramString2);
    int i = paramString1.getText().length();
    paramString1.setSelection(i);
  }
  
  public final void g(int paramInt)
  {
    Object localObject = getSystemService("notification");
    if (localObject != null)
    {
      ((NotificationManager)localObject).cancel(paramInt);
      return;
    }
    u localu = new c/u;
    localu.<init>("null cannot be cast to non-null type android.app.NotificationManager");
    throw localu;
  }
  
  public final void h(int paramInt)
  {
    EditText localEditText = s;
    if (localEditText == null)
    {
      String str = "flashText";
      k.a(str);
    }
    localEditText.setHintTextColor(paramInt);
  }
  
  public final void k()
  {
    super.k();
    int i = R.id.containerSend;
    Object localObject1 = findViewById(i);
    k.a(localObject1, "findViewById(R.id.containerSend)");
    r = ((View)localObject1);
    i = R.id.editTextSendFlash;
    localObject1 = findViewById(i);
    k.a(localObject1, "findViewById(R.id.editTextSendFlash)");
    localObject1 = (EditText)localObject1;
    s = ((EditText)localObject1);
    i = R.id.imageText;
    localObject1 = findViewById(i);
    k.a(localObject1, "findViewById(R.id.imageText)");
    localObject1 = (EditText)localObject1;
    t = ((EditText)localObject1);
    i = R.id.videoText;
    localObject1 = findViewById(i);
    k.a(localObject1, "findViewById(R.id.videoText)");
    localObject1 = (EditText)localObject1;
    u = ((EditText)localObject1);
    i = R.id.body_container;
    localObject1 = findViewById(i);
    k.a(localObject1, "findViewById(R.id.body_container)");
    v = ((View)localObject1);
    i = R.id.emojiContainer;
    localObject1 = findViewById(i);
    Object localObject2 = "findViewById(R.id.emojiContainer)";
    k.a(localObject1, (String)localObject2);
    x = ((View)localObject1);
    localObject1 = s;
    if (localObject1 == null)
    {
      localObject2 = "flashText";
      k.a((String)localObject2);
    }
    localObject2 = new com/truecaller/flashsdk/ui/send/SendActivity$f;
    i locali = (i)c();
    ((SendActivity.f)localObject2).<init>(locali);
    localObject2 = (c.g.a.b)localObject2;
    com.truecaller.flashsdk.assist.t.a((EditText)localObject1, (c.g.a.b)localObject2);
    localObject1 = s;
    if (localObject1 == null)
    {
      localObject2 = "flashText";
      k.a((String)localObject2);
    }
    com.truecaller.flashsdk.assist.t.a((EditText)localObject1);
    localObject1 = (FlashSendFooterView)e();
    localObject2 = this;
    localObject2 = (b.a)this;
    ((FlashSendFooterView)localObject1).setActionListener((b.a)localObject2);
    localObject1 = f();
    localObject2 = this;
    localObject2 = (FlashContactHeaderView.a)this;
    ((FlashContactHeaderView)localObject1).setContactClickListener$flash_release((FlashContactHeaderView.a)localObject2);
    localObject1 = s;
    if (localObject1 == null)
    {
      localObject2 = "flashText";
      k.a((String)localObject2);
    }
    ((EditText)localObject1).clearFocus();
    localObject1 = s;
    if (localObject1 == null)
    {
      localObject2 = "flashText";
      k.a((String)localObject2);
    }
    localObject2 = this;
    localObject2 = (ActionMode.Callback)this;
    ((EditText)localObject1).setCustomSelectionActionModeCallback((ActionMode.Callback)localObject2);
  }
  
  public final View l()
  {
    View localView = r;
    if (localView == null)
    {
      String str = "rootView";
      k.a(str);
    }
    return localView;
  }
  
  public final boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
  {
    return false;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = R.layout.activity_send_flash;
    setContentView(i);
    paramBundle = a.a();
    Object localObject = com.truecaller.flashsdk.core.c.b;
    localObject = com.truecaller.flashsdk.core.c.b();
    paramBundle = paramBundle.a((com.truecaller.flashsdk.core.a.a.a)localObject);
    localObject = new com/truecaller/flashsdk/ui/send/e;
    ((e)localObject).<init>(this);
    paramBundle.a((e)localObject).a().a(this);
    paramBundle = (i)c();
    localObject = this;
    localObject = (com.truecaller.flashsdk.ui.base.d)this;
    paramBundle.a((com.truecaller.flashsdk.ui.base.d)localObject);
  }
  
  public final boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    z = paramActionMode;
    return true;
  }
  
  public final void onDestroy()
  {
    Object localObject1 = this;
    localObject1 = (Context)this;
    Object localObject2 = android.support.v4.content.d.a((Context)localObject1);
    BroadcastReceiver localBroadcastReceiver = (BroadcastReceiver)B;
    ((android.support.v4.content.d)localObject2).a(localBroadcastReceiver);
    localObject1 = android.support.v4.content.d.a((Context)localObject1);
    localObject2 = (BroadcastReceiver)C;
    ((android.support.v4.content.d)localObject1).a((BroadcastReceiver)localObject2);
    super.onDestroy();
  }
  
  public final void onDestroyActionMode(ActionMode paramActionMode)
  {
    z = null;
  }
  
  public final void onEmoticonClicked(com.truecaller.flashsdk.a.d paramd)
  {
    k.b(paramd, "emoticon");
    i locali = (i)c();
    Object localObject1 = s;
    if (localObject1 == null)
    {
      localObject2 = "flashText";
      k.a((String)localObject2);
    }
    localObject1 = ((EditText)localObject1).getText().toString();
    Object localObject2 = s;
    if (localObject2 == null)
    {
      localObject3 = "flashText";
      k.a((String)localObject3);
    }
    int i = ((EditText)localObject2).getSelectionStart();
    Object localObject3 = s;
    if (localObject3 == null)
    {
      String str = "flashText";
      k.a(str);
    }
    int j = ((EditText)localObject3).getSelectionEnd();
    locali.a((String)localObject1, paramd, i, j);
  }
  
  public final void onPause()
  {
    super.onPause();
    ((i)c()).n();
  }
  
  public final boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    return false;
  }
  
  public final void onResume()
  {
    ((i)c()).m();
    super.onResume();
  }
  
  public final void onStart()
  {
    super.onStart();
    ((i)c()).h();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.send.SendActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */