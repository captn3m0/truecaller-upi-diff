package com.truecaller.flashsdk.ui.send;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import c.u;
import com.truecaller.flashsdk.R.attr;
import com.truecaller.flashsdk.R.color;
import com.truecaller.flashsdk.R.drawable;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.assist.aa;
import com.truecaller.flashsdk.assist.ae;
import com.truecaller.flashsdk.assist.al;
import com.truecaller.flashsdk.assist.g;
import com.truecaller.flashsdk.assist.q;
import com.truecaller.flashsdk.core.b;
import com.truecaller.flashsdk.models.Contact;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.FlashExtras;
import com.truecaller.flashsdk.models.FlashImageEntity;
import com.truecaller.flashsdk.models.FlashLocationExtras;
import com.truecaller.flashsdk.models.ImageFlash;
import com.truecaller.flashsdk.models.Payload;
import com.truecaller.flashsdk.models.Sender;
import com.truecaller.utils.l;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

public final class j
  extends com.truecaller.flashsdk.ui.base.c
  implements com.truecaller.flashsdk.core.j, i
{
  private boolean A;
  private long B;
  private boolean C;
  private boolean D;
  private String E;
  private String F;
  private boolean G;
  private boolean H;
  private Uri I;
  private ImageFlash J;
  private final aa K;
  private final com.truecaller.flashsdk.assist.d L;
  private final com.google.gson.f M;
  private final com.truecaller.flashsdk.core.k N;
  private String p;
  private String q;
  private String r;
  private String s;
  private String t;
  private String u;
  private Flash v;
  private boolean w;
  private boolean x;
  private boolean y;
  private String z;
  
  public j(c.d.f paramf, com.google.firebase.messaging.a parama, ae paramae, aa paramaa, al paramal, g paramg, com.truecaller.flashsdk.assist.d paramd, com.truecaller.flashsdk.assist.a parama1, com.truecaller.flashsdk.d.a parama2, com.truecaller.flashsdk.assist.y paramy, com.google.gson.f paramf1, com.truecaller.flashsdk.core.k paramk, q paramq, l paraml, com.truecaller.common.g.a parama3)
  {
    super(paramf, paramae, parama, paramal, paramg, parama1, parama2, paramy, paramf1, paramq, paraml, parama3);
    K = paramaa;
    L = paramd;
    M = paramf1;
    N = paramk;
    boolean bool = true;
    x = bool;
    H = bool;
  }
  
  private void a(c paramc)
  {
    j localj = this;
    c localc = paramc;
    c.g.b.k.b(paramc, "presenterView");
    Object localObject1 = paramc;
    localObject1 = (com.truecaller.flashsdk.ui.base.d)paramc;
    super.b((com.truecaller.flashsdk.ui.base.d)localObject1);
    localObject1 = paramc.y().getExtras();
    c.g.b.k.a(localObject1, "bundle");
    Object localObject2 = "to_phone";
    boolean bool1 = ((Bundle)localObject1).containsKey((String)localObject2);
    long l1 = 0L;
    long l2;
    if (bool1)
    {
      localObject2 = "to_phone";
      l2 = ((Bundle)localObject1).getLong((String)localObject2);
    }
    else
    {
      l2 = l1;
    }
    localObject2 = "to_name";
    bool1 = ((Bundle)localObject1).containsKey((String)localObject2);
    if (bool1)
    {
      localObject2 = ((Bundle)localObject1).getString("to_name");
      p = ((String)localObject2);
    }
    String str1 = d.a();
    localObject2 = ((Bundle)localObject1).getString("screen_context", str1);
    q = ((String)localObject2);
    localObject2 = "image";
    bool1 = ((Bundle)localObject1).containsKey((String)localObject2);
    int j = 0;
    str1 = null;
    if (bool1)
    {
      localObject2 = ((Bundle)localObject1).getString("image");
      r = ((String)localObject2);
      H = false;
    }
    localObject2 = "background";
    bool1 = ((Bundle)localObject1).containsKey((String)localObject2);
    if (bool1)
    {
      localObject2 = ((Bundle)localObject1).getString("background");
      s = ((String)localObject2);
      H = false;
    }
    localObject2 = "video";
    bool1 = ((Bundle)localObject1).containsKey((String)localObject2);
    if (bool1)
    {
      localObject2 = ((Bundle)localObject1).getString("video");
      t = ((String)localObject2);
      H = false;
    }
    localObject2 = "description";
    bool1 = ((Bundle)localObject1).containsKey((String)localObject2);
    if (bool1)
    {
      localObject2 = ((Bundle)localObject1).getString("description");
      u = ((String)localObject2);
      H = false;
    }
    localObject2 = "mode";
    bool1 = ((Bundle)localObject1).containsKey((String)localObject2);
    if (bool1)
    {
      localObject2 = "mode";
      bool1 = ((Bundle)localObject1).getBoolean((String)localObject2);
      e = bool1;
    }
    localObject2 = "time_left";
    bool1 = ((Bundle)localObject1).containsKey((String)localObject2);
    if (bool1)
    {
      localObject2 = "time_left";
      long l3 = ((Bundle)localObject1).getLong((String)localObject2);
      B = l3;
    }
    localObject2 = "prefilled_text";
    bool1 = ((Bundle)localObject1).containsKey((String)localObject2);
    if (bool1)
    {
      localObject2 = ((Bundle)localObject1).getString("prefilled_text");
      F = ((String)localObject2);
    }
    localObject2 = "preset_flash_type";
    bool1 = ((Bundle)localObject1).containsKey((String)localObject2);
    if (bool1)
    {
      localObject2 = ((Bundle)localObject1).getString("preset_flash_type");
      E = ((String)localObject2);
    }
    localObject2 = "show_waiting";
    bool1 = ((Bundle)localObject1).containsKey((String)localObject2);
    if (bool1)
    {
      localObject2 = "show_waiting";
      bool1 = ((Bundle)localObject1).getBoolean((String)localObject2);
      x = bool1;
    }
    localObject2 = new com/truecaller/flashsdk/models/Flash;
    ((Flash)localObject2).<init>();
    v = ((Flash)localObject2);
    localObject2 = v;
    if (localObject2 != null) {
      ((Flash)localObject2).a(l2);
    }
    localObject2 = (c)a;
    if (localObject2 != null)
    {
      String str2 = "notification_id";
      int k = -1;
      m = ((Bundle)localObject1).getInt(str2, k);
      if (m != k) {
        ((c)localObject2).g(m);
      }
    }
    long l4 = B;
    int i2 = 1;
    boolean bool2 = l4 < l1;
    if (bool2)
    {
      A = i2;
      m = R.string.flash_sent_to;
      localj.d(m);
      localObject1 = "";
      localObject2 = String.valueOf(z);
      boolean bool5 = G;
      long l5 = B;
      Flash localFlash = v;
      if (localFlash != null)
      {
        long l6 = localFlash.b();
        localc = paramc;
        paramc.a((String)localObject1, (String)localObject2, bool5, l5, l6);
      }
      return;
    }
    int m = R.string.send_flash_to_v2;
    localj.d(m);
    localObject1 = v;
    if (localObject1 != null)
    {
      l4 = ((Flash)localObject1).b();
      Object localObject3 = h;
      localc.a((ae)localObject3, l4);
      localj.a(l4);
      localObject1 = o;
      localObject2 = "featureShareImageInFlash";
      boolean bool3 = ((com.truecaller.common.g.a)localObject1).b((String)localObject2);
      int i6;
      if (bool3)
      {
        int n = 2;
        localObject1 = new com.truecaller.flashsdk.ui.customviews.a[n];
        localObject2 = new com/truecaller/flashsdk/ui/customviews/a;
        int i4 = R.drawable.flash_ic_location__selected_24dp;
        int i5 = R.string.sfc_location;
        localObject3 = k;
        i6 = R.attr.theme_flash_attach_button_tint;
        int i7 = ((com.truecaller.flashsdk.assist.a)localObject3).b(i6);
        ((com.truecaller.flashsdk.ui.customviews.a)localObject2).<init>(0, i4, i5, i7, (byte)0);
        localObject1[0] = localObject2;
        localObject2 = new com/truecaller/flashsdk/ui/customviews/a;
        int i8 = 1;
        int i9 = R.drawable.flash_ic_photo_camera_24dp;
        int i10 = R.string.flash_attach_camera;
        localObject3 = k;
        i6 = R.attr.theme_flash_attach_button_tint;
        int i11 = ((com.truecaller.flashsdk.assist.a)localObject3).b(i6);
        ((com.truecaller.flashsdk.ui.customviews.a)localObject2).<init>(i8, i9, i10, i11, (byte)0);
        localObject1[i2] = localObject2;
        c.a.m.c((Object[])localObject1);
        paramc.S();
      }
      boolean bool4 = e;
      if (bool4)
      {
        localObject1 = r;
        bool1 = false;
        localObject2 = null;
        if (localObject1 != null)
        {
          localObject3 = localObject1;
          localObject3 = (CharSequence)localObject1;
          int i3 = ((CharSequence)localObject3).length();
          if (i3 == 0)
          {
            i3 = 1;
          }
          else
          {
            i3 = 0;
            localObject3 = null;
          }
          i3 ^= i2;
          if (i3 == 0)
          {
            bool4 = false;
            localObject1 = null;
          }
          if (localObject1 != null)
          {
            localObject2 = o;
            localObject3 = "featureShareImageInFlash";
            bool1 = ((com.truecaller.common.g.a)localObject2).b((String)localObject3);
            if (bool1)
            {
              localObject2 = u;
              if (localObject2 == null) {
                localObject2 = "";
              }
              localc.f((String)localObject1, (String)localObject2);
              paramc.T();
              localObject1 = k;
              int i = R.color.white;
              int i1 = ((com.truecaller.flashsdk.assist.a)localObject1).a(i);
              localc.h(i1);
              i1 = R.attr.theme_bg_contact_transparent_header;
              localObject2 = k;
              i3 = R.color.white;
              i = ((com.truecaller.flashsdk.assist.a)localObject2).a(i3);
              localc.a(i1, i);
              paramc.P();
              return;
            }
            localObject2 = u;
            if (localObject2 == null) {
              localObject2 = "";
            }
            localc.b((String)localObject1, (String)localObject2);
            return;
          }
        }
        localObject1 = localj;
        localObject1 = (j)localj;
        localObject3 = t;
        if (localObject3 != null)
        {
          Object localObject4 = localObject3;
          localObject4 = (CharSequence)localObject3;
          i6 = ((CharSequence)localObject4).length();
          if (i6 == 0) {
            j = 1;
          }
          i6 = j ^ 0x1;
          if (i6 != 0) {
            localObject2 = localObject3;
          }
          if (localObject2 != null)
          {
            localObject1 = u;
            if (localObject1 == null) {
              localObject1 = "";
            }
            localc.d((String)localObject2, (String)localObject1);
            return;
          }
        }
      }
      return;
    }
  }
  
  private final void c(String paramString)
  {
    paramString = (CharSequence)paramString;
    int i = paramString.length();
    int j = 1;
    int k = i - j;
    i = 0;
    c localc = null;
    int m = 0;
    int n;
    for (;;)
    {
      n = 32;
      if (i > k) {
        break;
      }
      if (m == 0) {
        i1 = i;
      } else {
        i1 = k;
      }
      int i1 = paramString.charAt(i1);
      if (i1 <= n) {
        i1 = 1;
      } else {
        i1 = 0;
      }
      if (m == 0)
      {
        if (i1 == 0) {
          m = 1;
        } else {
          i += 1;
        }
      }
      else
      {
        if (i1 == 0) {
          break;
        }
        k += -1;
      }
    }
    k += j;
    paramString = paramString.subSequence(i, k).toString();
    int i2 = paramString.length();
    float f;
    if (i2 != 0)
    {
      i = 8;
      if (i2 < i)
      {
        i2 = 1111490560;
        f = 48.0F;
        break label219;
      }
    }
    i = 16;
    if (i2 < i)
    {
      i2 = 1107296256;
      f = 32.0F;
    }
    else if (i2 < n)
    {
      i2 = 1103101952;
      f = 24.0F;
    }
    else
    {
      i2 = 1098907648;
      f = 16.0F;
    }
    label219:
    localc = (c)a;
    if (localc != null)
    {
      localc.a(f);
      return;
    }
  }
  
  private final Payload d(String paramString)
  {
    Payload localPayload = new com/truecaller/flashsdk/models/Payload;
    Object localObject1 = b;
    String str = null;
    localPayload.<init>("location", paramString, null, (String)localObject1);
    Object localObject2 = o;
    localObject1 = "featureShareImageInFlash";
    boolean bool1 = ((com.truecaller.common.g.a)localObject2).b((String)localObject1);
    if (bool1)
    {
      localObject2 = b;
      if (localObject2 == null) {
        return null;
      }
      localObject2 = (CharSequence)localObject2;
      Object localObject3 = new c/n/k;
      ((c.n.k)localObject3).<init>(",");
      localObject1 = null;
      localObject2 = ((c.n.k)localObject3).a((CharSequence)localObject2, 0);
      boolean bool2 = ((List)localObject2).isEmpty();
      int j = 1;
      int i;
      CharSequence localCharSequence;
      int k;
      if (!bool2)
      {
        i = ((List)localObject2).size();
        localObject3 = ((List)localObject2).listIterator(i);
        do
        {
          boolean bool3 = ((ListIterator)localObject3).hasPrevious();
          if (!bool3) {
            break;
          }
          localCharSequence = (CharSequence)((ListIterator)localObject3).previous();
          k = localCharSequence.length();
          if (k == 0)
          {
            k = 1;
          }
          else
          {
            k = 0;
            localCharSequence = null;
          }
        } while (k != 0);
        localObject2 = (Iterable)localObject2;
        i = ((ListIterator)localObject3).nextIndex() + j;
        localObject2 = c.a.m.d((Iterable)localObject2, i);
      }
      else
      {
        localObject2 = (List)c.a.y.a;
      }
      localObject2 = (Collection)localObject2;
      if (localObject2 != null)
      {
        localObject3 = new String[0];
        localObject2 = ((Collection)localObject2).toArray((Object[])localObject3);
        if (localObject2 != null)
        {
          localObject2 = (String[])localObject2;
          i = localObject2.length;
          k = 2;
          if (i < k) {
            return null;
          }
          str = c;
          double d1 = Double.parseDouble(localObject2[0]);
          localObject1 = Double.valueOf(d1);
          double d2 = Double.parseDouble(localObject2[j]);
          localObject2 = Double.valueOf(d2);
          FlashLocationExtras localFlashLocationExtras = new com/truecaller/flashsdk/models/FlashLocationExtras;
          localFlashLocationExtras.<init>(str, (Double)localObject1, (Double)localObject2, paramString);
          paramString = new com/truecaller/flashsdk/models/FlashExtras;
          j = 0;
          k = 0;
          localCharSequence = null;
          int m = 3;
          localObject3 = paramString;
          paramString.<init>(null, null, localFlashLocationExtras, m, null);
          localObject2 = M;
          paramString = ((com.google.gson.f)localObject2).b(paramString);
          localPayload.b(paramString);
        }
        else
        {
          paramString = new c/u;
          paramString.<init>("null cannot be cast to non-null type kotlin.Array<T>");
          throw paramString;
        }
      }
      else
      {
        paramString = new c/u;
        paramString.<init>("null cannot be cast to non-null type java.util.Collection<T>");
        throw paramString;
      }
    }
    return localPayload;
  }
  
  private final void d(int paramInt)
  {
    Object localObject1 = v;
    String str1 = null;
    if (localObject1 != null)
    {
      long l = ((Flash)localObject1).b();
      localObject1 = Long.valueOf(l);
    }
    else
    {
      i = 0;
      localObject1 = null;
    }
    localObject1 = String.valueOf(localObject1);
    Object localObject2 = j;
    boolean bool1 = ((g)localObject2).f();
    if (bool1)
    {
      localObject2 = L.b((String)localObject1);
    }
    else
    {
      bool1 = false;
      localObject2 = null;
    }
    c localc = (c)a;
    if (localc == null) {
      return;
    }
    if (localObject2 != null)
    {
      localObject3 = (CharSequence)((Contact)localObject2).getName();
      bool2 = TextUtils.isEmpty((CharSequence)localObject3);
      if (!bool2)
      {
        localObject3 = ((Contact)localObject2).getName();
        bool2 = c.g.b.k.a(localObject3, localObject1) ^ true;
        if (bool2)
        {
          localObject1 = ((Contact)localObject2).getName();
          break label167;
        }
      }
    }
    Object localObject3 = p;
    if (localObject3 != null) {
      localObject1 = localObject3;
    }
    label167:
    z = ((String)localObject1);
    localObject1 = this.i;
    boolean bool2 = false;
    localObject3 = null;
    Object[] arrayOfObject = new Object[0];
    String str2 = ((al)localObject1).a(paramInt, arrayOfObject);
    localObject1 = String.valueOf(z);
    localc.a(str2, (String)localObject1);
    if (localObject2 != null) {
      str1 = ((Contact)localObject2).getImageUrl();
    }
    if (str1 != null)
    {
      str2 = ((Contact)localObject2).getImageUrl();
      localc.a(str2);
    }
    else
    {
      paramInt = R.drawable.ic_empty_avatar;
      localc.c(paramInt);
    }
    str2 = F;
    if (str2 == null) {
      return;
    }
    int i = Math.max(str2.length(), 80) + -1;
    localc.a(str2, 0, 0, i);
  }
  
  private void q()
  {
    boolean bool1 = false;
    Object localObject1 = null;
    w = false;
    Object localObject2 = v;
    if (localObject2 == null) {
      return;
    }
    Object localObject3 = ((Flash)localObject2).f();
    c.g.b.k.a(localObject3, "flash.payload");
    localObject3 = (CharSequence)((Payload)localObject3).a();
    Object localObject4 = (CharSequence)"emoji";
    boolean bool2 = TextUtils.equals((CharSequence)localObject3, (CharSequence)localObject4);
    if (bool2)
    {
      localObject3 = ((Flash)localObject2).f();
      localObject4 = "flash.payload";
      c.g.b.k.a(localObject3, (String)localObject4);
      localObject3 = ((Payload)localObject3).b();
    }
    else
    {
      localObject3 = ((Flash)localObject2).f();
      localObject4 = "flash.payload";
      c.g.b.k.a(localObject3, (String)localObject4);
      localObject3 = com.truecaller.flashsdk.assist.c.a(((Payload)localObject3).a());
    }
    ((Flash)localObject2).b((String)localObject3);
    bool2 = ((Flash)localObject2).l();
    boolean bool3 = true;
    Object localObject5;
    if (bool2)
    {
      long l1 = ((Flash)localObject2).b();
      long l2 = 0L;
      bool2 = l1 < l2;
      if (bool2)
      {
        localObject3 = ((Flash)localObject2).f();
        c.g.b.k.a(localObject3, "flash.payload");
        localObject3 = (CharSequence)((Payload)localObject3).a();
        localObject5 = (CharSequence)"location";
        bool2 = TextUtils.equals((CharSequence)localObject3, (CharSequence)localObject5);
        if (bool2)
        {
          localObject3 = ((Flash)localObject2).f();
          localObject5 = "flash.payload";
          c.g.b.k.a(localObject3, (String)localObject5);
          localObject3 = (CharSequence)((Payload)localObject3).d();
          bool2 = TextUtils.isEmpty((CharSequence)localObject3);
          if (bool2)
          {
            bool2 = false;
            localObject3 = null;
            break label275;
          }
        }
        bool2 = true;
        break label275;
      }
    }
    bool2 = false;
    localObject3 = null;
    label275:
    if (bool2)
    {
      localObject3 = (c)a;
      if (localObject3 != null)
      {
        boolean bool4 = e;
        if (bool4)
        {
          localObject5 = (CharSequence)r;
          if (localObject5 != null)
          {
            bool4 = c.n.m.a((CharSequence)localObject5);
            if (!bool4)
            {
              bool4 = false;
              localObject5 = null;
              break label349;
            }
          }
          bool4 = true;
          label349:
          if (bool4)
          {
            localObject5 = o;
            Object localObject6 = "featureShareImageInFlash";
            bool4 = ((com.truecaller.common.g.a)localObject5).b((String)localObject6);
            if (bool4)
            {
              localObject5 = I;
              if (localObject5 == null) {
                break label542;
              }
              localObject6 = J;
              if (localObject6 == null)
              {
                localObject6 = new com/truecaller/flashsdk/models/ImageFlash;
                ((ImageFlash)localObject6).<init>();
                ((ImageFlash)localObject6).a((Flash)localObject2);
                ((ImageFlash)localObject6).a((Uri)localObject5);
                localObject5 = q;
                ((ImageFlash)localObject6).d((String)localObject5);
              }
              ((c)localObject3).a((ImageFlash)localObject6);
              ((c)localObject3).I();
              localObject5 = this.i;
              int i = R.string.flash_uploading_media;
              Object[] arrayOfObject = new Object[0];
              localObject5 = ((al)localObject5).a(i, arrayOfObject);
              ((c)localObject3).a((String)localObject5, false);
              w = bool3;
              break label542;
            }
          }
        }
        w = bool3;
        localObject1 = N;
        localObject3 = "sending";
        localObject5 = this;
        localObject5 = (com.truecaller.flashsdk.core.j)this;
        ((com.truecaller.flashsdk.core.k)localObject1).a((Flash)localObject2, (String)localObject3, bool3, (com.truecaller.flashsdk.core.j)localObject5);
      }
    }
    label542:
    localObject1 = I;
    if (localObject1 != null)
    {
      localObject1 = ((Flash)localObject2).f();
      c.g.b.k.a(localObject1, "flashCopy.payload");
      localObject1 = (CharSequence)((Payload)localObject1).a();
      localObject2 = (CharSequence)"location";
      bool1 = TextUtils.equals((CharSequence)localObject1, (CharSequence)localObject2);
      if (!bool1) {}
    }
    else
    {
      r();
    }
  }
  
  private final void r()
  {
    Object localObject1 = v;
    if (localObject1 == null) {
      return;
    }
    c localc = (c)a;
    if (localc == null) {
      return;
    }
    Object localObject2 = ((Flash)localObject1).f();
    c.g.b.k.a(localObject2, "flashCopy.payload");
    localObject2 = (CharSequence)((Payload)localObject2).a();
    Object localObject3 = (CharSequence)"emoji";
    boolean bool1 = TextUtils.equals((CharSequence)localObject2, (CharSequence)localObject3);
    if (bool1)
    {
      localObject2 = h;
      long l1 = ((Flash)localObject1).b();
      ((ae)localObject2).b(l1);
    }
    localObject2 = v;
    int j = 0;
    localObject3 = null;
    boolean bool4;
    if (localObject2 != null)
    {
      localObject4 = (CharSequence)((Flash)localObject2).h();
      boolean bool2 = TextUtils.isEmpty((CharSequence)localObject4);
      if (!bool2)
      {
        localObject4 = (CharSequence)((Flash)localObject2).c();
        bool2 = TextUtils.isEmpty((CharSequence)localObject4);
        if (!bool2)
        {
          localObject4 = (CharSequence)q;
          bool2 = TextUtils.isEmpty((CharSequence)localObject4);
          if (!bool2)
          {
            localObject4 = new android/os/Bundle;
            ((Bundle)localObject4).<init>();
            long l2 = ((Flash)localObject2).b();
            String str1 = Long.toString(l2);
            Object localObject5 = this.j;
            boolean bool3 = ((g)localObject5).f();
            if (bool3)
            {
              localObject5 = L;
              str2 = "phoneNumber";
              c.g.b.k.a(str1, str2);
              bool3 = ((com.truecaller.flashsdk.assist.d)localObject5).a(str1);
            }
            else
            {
              bool3 = false;
              localObject5 = null;
            }
            Object localObject6 = ((Flash)localObject2).f();
            String str3 = "flashCopy.payload";
            c.g.b.k.a(localObject6, str3);
            localObject6 = ((Payload)localObject6).a();
            ((Bundle)localObject4).putString("type", (String)localObject6);
            localObject6 = ((Flash)localObject2).h();
            ((Bundle)localObject4).putString("flash_message_id", (String)localObject6);
            ((Bundle)localObject4).putString("flash_receiver_id", str1);
            String str2 = q;
            ((Bundle)localObject4).putString("flash_context", str2);
            ((Bundle)localObject4).putBoolean("flash_from_phonebook", bool3);
            bool3 = false;
            localObject5 = null;
            ((Bundle)localObject4).putString("flash_reply_id", null);
            localObject2 = ((Flash)localObject2).c();
            ((Bundle)localObject4).putString("flash_thread_id", (String)localObject2);
            bool4 = g;
            str1 = String.valueOf(bool4);
            ((Bundle)localObject4).putString("FlashFromHistory", str1);
            localObject2 = (CharSequence)u;
            bool1 = TextUtils.isEmpty((CharSequence)localObject2);
            if (!bool1)
            {
              localObject2 = "CampaignDescription";
              str1 = u;
              ((Bundle)localObject4).putString((String)localObject2, str1);
            }
            ((Bundle)localObject4).putString("history_length", "0");
            localObject2 = com.truecaller.flashsdk.core.c.a();
            str1 = "ANDROID_FLASH_SENT";
            ((b)localObject2).a(str1, (Bundle)localObject4);
            g = false;
          }
        }
      }
    }
    localObject2 = h;
    Object localObject4 = v;
    if (localObject4 != null)
    {
      long l3 = ((Flash)localObject4).b();
      ((ae)localObject2).b(l3);
      localObject2 = this.i;
      int k = R.string.flash_sent_to;
      localObject3 = new Object[0];
      localObject2 = ((al)localObject2).a(k, (Object[])localObject3);
      localObject3 = String.valueOf(z);
      localc.a((String)localObject2, (String)localObject3);
      bool1 = x;
      if (bool1)
      {
        bool1 = true;
        A = bool1;
        localObject2 = ((Flash)localObject1).e();
        c.g.b.k.a(localObject2, "flashCopy.history");
        localObject3 = b((String)localObject2);
        localObject4 = String.valueOf(z);
        bool4 = G;
        long l4 = 60000L;
        long l5 = ((Flash)localObject1).b();
        localObject2 = localc;
        localc.a((String)localObject3, (String)localObject4, bool4, l4, l5);
      }
      else
      {
        localc.x();
      }
      K.c();
      localObject1 = o;
      localObject2 = "featureShareImageInFlash";
      boolean bool5 = ((com.truecaller.common.g.a)localObject1).b((String)localObject2);
      if (bool5)
      {
        localc.V();
        int m = R.attr.theme_bg_contact_header;
        localObject2 = this.k;
        j = R.attr.theme_incoming_text;
        int i = ((com.truecaller.flashsdk.assist.a)localObject2).b(j);
        localc.a(m, i);
        boolean bool6 = y;
        if (bool6)
        {
          k();
          localc.W();
        }
      }
      return;
    }
  }
  
  private final boolean s()
  {
    Object localObject = (CharSequence)r;
    boolean bool1 = true;
    if (localObject != null)
    {
      bool2 = c.n.m.a((CharSequence)localObject);
      if (!bool2)
      {
        bool2 = false;
        localObject = null;
        break label35;
      }
    }
    boolean bool2 = true;
    label35:
    if (bool2)
    {
      localObject = I;
      if (localObject == null)
      {
        localObject = b;
        if (localObject == null) {
          return false;
        }
      }
    }
    return bool1;
  }
  
  private final void t()
  {
    c localc = (c)a;
    if (localc == null) {
      return;
    }
    Object localObject = j;
    boolean bool = ((g)localObject).a();
    if (bool)
    {
      u();
      q();
      return;
    }
    localObject = this.i;
    int i = R.string.no_internet;
    Object[] arrayOfObject = new Object[0];
    localObject = ((al)localObject).a(i, arrayOfObject);
    localc.d((String)localObject);
  }
  
  private final void u()
  {
    Flash localFlash = v;
    if (localFlash == null) {
      return;
    }
    localFlash.i();
    localFlash.j();
  }
  
  public final void a()
  {
    super.a();
    p = null;
    q = null;
    r = null;
    t = null;
    s = null;
    u = null;
    v = null;
    e = false;
    z = null;
    B = 0L;
    D = false;
    w = false;
    y = false;
    A = false;
    H = true;
  }
  
  public final void a(Uri paramUri)
  {
    c.g.b.k.b(paramUri, "uri");
    boolean bool = true;
    e = bool;
    I = paramUri;
    c localc = (c)a;
    if (localc != null)
    {
      localc.T();
      localc.b(paramUri);
      int i = R.attr.theme_bg_contact_transparent_header;
      com.truecaller.flashsdk.assist.a locala = this.k;
      int j = R.color.white;
      int k = locala.a(j);
      localc.a(i, k);
      localc.P();
      paramUri = this.i;
      k = R.string.flash_hint_image_caption;
      Object[] arrayOfObject = new Object[0];
      paramUri = paramUri.a(k, arrayOfObject);
      localc.e(paramUri);
      paramUri = this.k;
      k = R.color.white;
      i = paramUri.a(k);
      localc.h(i);
      return;
    }
  }
  
  public final void a(com.truecaller.flashsdk.a.d paramd)
  {
    c.g.b.k.b(paramd, "emoticon");
    Object localObject = l;
    int i = 2;
    ((com.truecaller.flashsdk.d.a)localObject).a(i);
    localObject = new com/truecaller/flashsdk/models/Payload;
    String str = "emoji";
    paramd = paramd.a();
    ((Payload)localObject).<init>(str, paramd, null, null);
    paramd = v;
    if (paramd != null) {
      paramd.a((Payload)localObject);
    }
    t();
  }
  
  public final void a(Flash paramFlash)
  {
    Object localObject = "flash";
    c.g.b.k.b(paramFlash, (String)localObject);
    paramFlash = (c)a;
    if (paramFlash == null) {
      return;
    }
    localObject = this.i;
    int i = R.string.no_internet;
    Object[] arrayOfObject = new Object[0];
    localObject = ((al)localObject).a(i, arrayOfObject);
    paramFlash.d((String)localObject);
  }
  
  public final void a(CharSequence paramCharSequence)
  {
    c localc = (c)a;
    if (localc != null)
    {
      boolean bool1 = false;
      if (paramCharSequence != null)
      {
        bool2 = c.n.m.a(paramCharSequence);
        if (!bool2)
        {
          bool2 = false;
          break label41;
        }
      }
      boolean bool2 = true;
      label41:
      if (bool2)
      {
        bool2 = s();
        if (!bool2) {}
      }
      else
      {
        bool1 = true;
      }
      localc.d(bool1);
    }
    if (paramCharSequence != null)
    {
      paramCharSequence = paramCharSequence.toString();
      if (paramCharSequence != null)
      {
        c(paramCharSequence);
        return;
      }
    }
  }
  
  public final void a(String paramString, ImageFlash paramImageFlash)
  {
    c localc = (c)a;
    if (localc == null) {
      return;
    }
    if (paramString == null) {
      return;
    }
    int i = paramString.hashCode();
    int j = -1350001074;
    Object localObject;
    boolean bool2;
    int k;
    if (i != j)
    {
      j = -1223111947;
      if (i != j)
      {
        j = -849991191;
        boolean bool1 = true;
        if (i != j)
        {
          j = 1034431578;
          if (i == j)
          {
            localObject = "state_flash_failed";
            bool2 = paramString.equals(localObject);
            if (bool2)
            {
              if (paramImageFlash == null) {
                return;
              }
              J = paramImageFlash;
              paramString = J;
              if (paramString != null)
              {
                paramString = ((Flash)paramString).f();
                c.g.b.k.a(paramString, "(imageFlashDraft as Flash).payload");
                paramString = paramString.b();
                paramImageFlash = "(imageFlashDraft as Flash).payload.message";
                c.g.b.k.a(paramString, paramImageFlash);
                localc.a(paramString, bool1);
                localc.M();
                paramString = this.i;
                k = R.string.flash_sending_failed;
                localObject = new Object[0];
                paramString = paramString.a(k, (Object[])localObject);
                localc.d(paramString);
              }
              else
              {
                paramString = new c/u;
                paramString.<init>("null cannot be cast to non-null type com.truecaller.flashsdk.models.Flash");
                throw paramString;
              }
            }
          }
        }
        else
        {
          localObject = "state_uploading_failed";
          bool2 = paramString.equals(localObject);
          if (bool2)
          {
            if (paramImageFlash == null) {
              return;
            }
            J = paramImageFlash;
            localc.J();
            paramString = J;
            if (paramString != null)
            {
              paramString = ((Flash)paramString).f();
              c.g.b.k.a(paramString, "(imageFlashDraft as Flash).payload");
              paramString = paramString.b();
              c.g.b.k.a(paramString, "(imageFlashDraft as Flash).payload.message");
              localc.a(paramString, bool1);
              paramString = this.i;
              k = R.string.flash_sending_failed;
              localObject = new Object[0];
              paramString = paramString.a(k, (Object[])localObject);
              localc.d(paramString);
              return;
            }
            paramString = new c/u;
            paramString.<init>("null cannot be cast to non-null type com.truecaller.flashsdk.models.Flash");
            throw paramString;
          }
        }
      }
      else
      {
        paramImageFlash = "state_flash_sent";
        bool2 = paramString.equals(paramImageFlash);
        if (bool2)
        {
          paramString = this.i;
          k = R.string.flash_sent;
          localObject = new Object[0];
          paramString = paramString.a(k, (Object[])localObject);
          localc.a(paramString, false);
          localc.L();
          r();
        }
      }
    }
    else
    {
      paramImageFlash = "state_uploaded";
      bool2 = paramString.equals(paramImageFlash);
      if (bool2)
      {
        paramString = this.i;
        k = R.string.flash_sending_flash;
        localObject = new Object[0];
        paramString = paramString.a(k, (Object[])localObject);
        localc.a(paramString, false);
        localc.K();
        return;
      }
    }
  }
  
  public final boolean a(int paramInt)
  {
    boolean bool1 = true;
    int i = 16908332;
    Object localObject;
    if (paramInt == i)
    {
      localObject = (c)a;
      if (localObject != null) {
        ((c)localObject).x();
      }
      return bool1;
    }
    i = R.id.about;
    if (paramInt == i)
    {
      D = bool1;
      localObject = l;
      i = 8;
      ((com.truecaller.flashsdk.d.a)localObject).a(i);
      localObject = (c)a;
      if (localObject != null) {
        ((c)localObject).r();
      }
      localObject = "ANDROID_FLASH_TUTORIAL_CLICKED";
      Bundle localBundle = new android/os/Bundle;
      localBundle.<init>();
      String str1 = "flash_context";
      boolean bool2 = A;
      String str2;
      if (bool2) {
        str2 = "waiting";
      } else {
        str2 = "send";
      }
      localBundle.putString(str1, str2);
      com.truecaller.flashsdk.core.c.a().a((String)localObject, localBundle);
      return bool1;
    }
    return false;
  }
  
  public final boolean a(Intent paramIntent)
  {
    c.g.b.k.b(paramIntent, "viewIntent");
    Object localObject1 = "to_phone";
    boolean bool = paramIntent.hasExtra((String)localObject1);
    if (!bool)
    {
      localObject1 = (c)a;
      if (localObject1 != null)
      {
        Object localObject2 = this.i;
        int i = R.string.required_to_send;
        Object[] arrayOfObject = new Object[0];
        localObject2 = ((al)localObject2).a(i, arrayOfObject);
        ((c)localObject1).d((String)localObject2);
      }
    }
    return bool;
  }
  
  public final void b()
  {
    b localb = com.truecaller.flashsdk.core.c.a();
    Object localObject = v;
    if (localObject != null)
    {
      long l = ((Flash)localObject).b();
      localObject = String.valueOf(l);
      if (localObject != null)
      {
        localb.b((String)localObject);
        return;
      }
    }
  }
  
  public final void b(Bundle paramBundle)
  {
    c.g.b.k.b(paramBundle, "flashBundle");
    paramBundle = paramBundle.getParcelable("extra_flash");
    c.g.b.k.a(paramBundle, "flashBundle.getParcelable(EXTRA_FLASH)");
    paramBundle = (Flash)paramBundle;
    Object localObject1 = paramBundle.f();
    c.g.b.k.a(localObject1, "flashReplied.payload");
    localObject1 = ((Payload)localObject1).a();
    boolean bool1 = c.g.b.k.a(localObject1, "call");
    int i = 0;
    Long localLong = null;
    if (bool1)
    {
      localObject1 = paramBundle.a();
      c.g.b.k.a(localObject1, "flashReplied.sender");
      localObject1 = ((Sender)localObject1).a();
      Object localObject2 = v;
      if (localObject2 != null)
      {
        long l1 = ((Flash)localObject2).b();
        localObject2 = Long.valueOf(l1);
      }
      else
      {
        localObject2 = null;
      }
      bool1 = c.g.b.k.a(localObject1, localObject2);
      if (bool1)
      {
        paramBundle = (c)a;
        if (paramBundle != null)
        {
          localObject1 = this.i;
          i = R.string.calling_you_back;
          localObject2 = new Object[0];
          localObject1 = ((al)localObject1).a(i, (Object[])localObject2);
          paramBundle.f((String)localObject1);
        }
        return;
      }
    }
    paramBundle = paramBundle.a();
    c.g.b.k.a(paramBundle, "flashReplied.sender");
    paramBundle = paramBundle.a();
    localObject1 = v;
    if (localObject1 != null)
    {
      long l2 = ((Flash)localObject1).b();
      localLong = Long.valueOf(l2);
    }
    boolean bool2 = c.g.b.k.a(paramBundle, localLong);
    if (bool2)
    {
      paramBundle = (c)a;
      if (paramBundle != null) {
        paramBundle.x();
      }
      return;
    }
    C = true;
  }
  
  public final void b(Flash paramFlash)
  {
    c.g.b.k.b(paramFlash, "flash");
  }
  
  public final void b(boolean paramBoolean)
  {
    c localc = (c)a;
    if (localc == null) {
      return;
    }
    if (paramBoolean)
    {
      localc.q();
      localc.G();
      return;
    }
    localc.x();
  }
  
  public final void c()
  {
    boolean bool = H;
    if (bool)
    {
      H = false;
      Object localObject = this.l;
      aa localaa = a;
      long l = localaa.d();
      localObject = a;
      String str = "send_tooltips";
      int j = 15;
      int i = ((aa)localObject).a(str, j);
      i = com.truecaller.flashsdk.d.a.a(l, i);
      c(i);
    }
  }
  
  public final void d_(String paramString)
  {
    c.g.b.k.b(paramString, "messageText");
    Object localObject1 = paramString;
    localObject1 = (CharSequence)paramString;
    boolean bool1 = c.n.m.a((CharSequence)localObject1);
    boolean bool3 = true;
    bool1 ^= bool3;
    int k = 4;
    Object[] arrayOfObject = null;
    FlashImageEntity localFlashImageEntity = null;
    label118:
    Object localObject2;
    Object localObject3;
    int m;
    if (bool1)
    {
      bool1 = y;
      if (bool1)
      {
        localObject1 = l;
        ((com.truecaller.flashsdk.d.a)localObject1).a(k);
        paramString = d(paramString);
        if (paramString == null) {
          return;
        }
        localObject1 = paramString;
      }
      else
      {
        bool1 = e;
        String str;
        if (bool1)
        {
          localObject1 = (CharSequence)r;
          if (localObject1 != null)
          {
            bool1 = c.n.m.a((CharSequence)localObject1);
            if (!bool1)
            {
              bool1 = false;
              localObject1 = null;
              break label118;
            }
          }
          bool1 = true;
          if (bool1)
          {
            localObject1 = o;
            str = "featureShareImageInFlash";
            bool1 = ((com.truecaller.common.g.a)localObject1).b(str);
            if (bool1)
            {
              l.a(k);
              localObject1 = new com/truecaller/flashsdk/models/Payload;
              localObject2 = "image";
              ((Payload)localObject1).<init>((String)localObject2, paramString, null, null);
              break label477;
            }
          }
        }
        bool1 = e;
        if (bool1)
        {
          localObject1 = (CharSequence)r;
          bool1 = TextUtils.isEmpty((CharSequence)localObject1);
          if (!bool1)
          {
            c.g.b.k.b(paramString, "messageText");
            localObject1 = (CharSequence)s;
            if (localObject1 != null)
            {
              int i = ((CharSequence)localObject1).length();
              if (i != 0)
              {
                bool3 = false;
                localObject2 = null;
              }
            }
            if (!bool3)
            {
              localObject1 = new java/lang/StringBuilder;
              ((StringBuilder)localObject1).<init>();
              localObject2 = r;
              ((StringBuilder)localObject1).append((String)localObject2);
              char c = ',';
              ((StringBuilder)localObject1).append(c);
              localObject2 = s;
              ((StringBuilder)localObject1).append((String)localObject2);
              localObject1 = ((StringBuilder)localObject1).toString();
            }
            else
            {
              localObject1 = r;
            }
            localObject2 = new com/truecaller/flashsdk/models/Payload;
            localObject3 = "image";
            ((Payload)localObject2).<init>((String)localObject3, paramString, null, (String)localObject1);
            paramString = r;
            if (paramString != null)
            {
              localObject1 = new com/truecaller/flashsdk/models/FlashExtras;
              arrayOfObject = null;
              localFlashImageEntity = new com/truecaller/flashsdk/models/FlashImageEntity;
              localFlashImageEntity.<init>(paramString, "image/jpg");
              m = 0;
              str = null;
              int n = 5;
              localObject3 = localObject1;
              ((FlashExtras)localObject1).<init>(null, localFlashImageEntity, null, n, null);
              paramString = M.b(localObject1);
              ((Payload)localObject2).b(paramString);
            }
            localObject1 = localObject2;
            break label477;
          }
        }
        boolean bool2 = e;
        if (bool2)
        {
          localObject1 = (CharSequence)t;
          bool2 = TextUtils.isEmpty((CharSequence)localObject1);
          if (!bool2)
          {
            localObject1 = new com/truecaller/flashsdk/models/Payload;
            localObject2 = "video";
            localObject3 = t;
            ((Payload)localObject1).<init>((String)localObject2, paramString, null, (String)localObject3);
            break label477;
          }
        }
        localObject1 = new com/truecaller/flashsdk/models/Payload;
        localObject2 = E;
        if (localObject2 == null) {
          localObject2 = "text";
        }
        ((Payload)localObject1).<init>((String)localObject2, paramString, null, null);
      }
      label477:
      paramString = v;
      if (paramString != null) {
        paramString.a((Payload)localObject1);
      }
      t();
      return;
    }
    boolean bool4 = y;
    int j;
    if (bool4)
    {
      l.a(k);
      paramString = v;
      if (paramString != null)
      {
        localObject1 = this.i;
        j = R.string.flash_shared_via;
        localObject3 = new Object[0];
        localObject1 = ((al)localObject1).a(j, (Object[])localObject3);
        localObject1 = d((String)localObject1);
        if (localObject1 == null) {
          return;
        }
        paramString.a((Payload)localObject1);
      }
      t();
      return;
    }
    bool4 = e;
    if (bool4)
    {
      paramString = (CharSequence)r;
      if (paramString != null)
      {
        bool4 = c.n.m.a(paramString);
        if (!bool4)
        {
          j = 0;
          localObject2 = null;
        }
      }
      if (j != 0)
      {
        paramString = o;
        localObject1 = "featureShareImageInFlash";
        bool4 = paramString.b((String)localObject1);
        if (bool4)
        {
          l.a(k);
          paramString = v;
          if (paramString != null)
          {
            localObject1 = new com/truecaller/flashsdk/models/Payload;
            localObject2 = "image";
            localObject3 = this.i;
            m = R.string.flash_shared_via;
            arrayOfObject = new Object[0];
            localObject3 = ((al)localObject3).a(m, arrayOfObject);
            ((Payload)localObject1).<init>((String)localObject2, (String)localObject3, null, null);
            paramString.a((Payload)localObject1);
          }
          t();
          return;
        }
      }
    }
    paramString = (c)a;
    if (paramString != null)
    {
      localObject1 = this.i;
      j = R.string.enter_valid_message;
      localObject3 = new Object[0];
      localObject1 = ((al)localObject1).a(j, (Object[])localObject3);
      paramString.d((String)localObject1);
      return;
    }
  }
  
  public final void h()
  {
    c localc = (c)a;
    if (localc == null) {
      return;
    }
    com.truecaller.common.g.a locala = o;
    String str = "featureShareImageInFlash";
    boolean bool = locala.b(str);
    if (bool) {
      localc.X();
    }
    localc.Y();
  }
  
  public final void j()
  {
    super.j();
    e = false;
    y = true;
  }
  
  public final void k()
  {
    super.k();
    Object localObject = o;
    String str = "featureShareImageInFlash";
    boolean bool = ((com.truecaller.common.g.a)localObject).b(str);
    if (bool)
    {
      localObject = ".";
      c((String)localObject);
    }
    bool = false;
    y = false;
    localObject = (c)a;
    if (localObject != null)
    {
      ((c)localObject).F();
      return;
    }
  }
  
  public final void l()
  {
    int i = 1;
    G = i;
    com.truecaller.flashsdk.d.a locala = l;
    locala.a(i);
    c localc = (c)a;
    if (localc != null)
    {
      localc.Q();
      return;
    }
  }
  
  public final void m()
  {
    boolean bool = A;
    if (!bool)
    {
      bool = f;
      if (!bool)
      {
        bool = e;
        if (bool)
        {
          localObject = o;
          String str = "featureShareImageInFlash";
          bool = ((com.truecaller.common.g.a)localObject).b(str);
          if (bool)
          {
            localObject = (c)a;
            if (localObject != null) {
              ((c)localObject).P();
            }
            return;
          }
        }
        Object localObject = (c)a;
        if (localObject != null)
        {
          ((c)localObject).G();
          return;
        }
      }
    }
  }
  
  public final void n()
  {
    boolean bool = C;
    if (bool)
    {
      localc = (c)a;
      if (localc != null) {
        localc.x();
      }
      return;
    }
    c localc = (c)a;
    if (localc != null)
    {
      localc.R();
      return;
    }
  }
  
  public final void o()
  {
    boolean bool = s();
    if (!bool)
    {
      localObject1 = (c)a;
      if (localObject1 != null) {
        ((c)localObject1).x();
      }
      return;
    }
    Object localObject1 = (c)a;
    if (localObject1 != null)
    {
      ((c)localObject1).G();
      int i = R.attr.theme_bg_contact_header;
      Object localObject2 = this.k;
      int j = R.attr.theme_incoming_text;
      int k = ((com.truecaller.flashsdk.assist.a)localObject2).b(j);
      ((c)localObject1).a(i, k);
      I = null;
      r = null;
      bool = false;
      localObject1 = null;
      e = false;
      c localc = (c)a;
      if (localc != null)
      {
        localObject2 = this.k;
        j = R.attr.theme_text_hint;
        k = ((com.truecaller.flashsdk.assist.a)localObject2).b(j);
        localc.h(k);
        localc.U();
        localObject2 = this.i;
        j = R.string.type_a_flash;
        localObject1 = new Object[0];
        localObject1 = ((al)localObject2).a(j, (Object[])localObject1);
        localc.e((String)localObject1);
      }
      bool = y;
      if (bool) {
        k();
      }
      return;
    }
  }
  
  public final void p()
  {
    boolean bool = e;
    if (bool)
    {
      o();
      return;
    }
    b(1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.send.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */