package com.truecaller.flashsdk.ui.send;

import android.view.View;
import android.widget.EditText;
import com.truecaller.utils.extensions.t;

final class SendActivity$i
  implements Runnable
{
  SendActivity$i(SendActivity paramSendActivity) {}
  
  public final void run()
  {
    EditText localEditText = SendActivity.b(a);
    if (localEditText != null)
    {
      localEditText.setCursorVisible(true);
      t.a((View)localEditText, false, 3);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.send.SendActivity.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */