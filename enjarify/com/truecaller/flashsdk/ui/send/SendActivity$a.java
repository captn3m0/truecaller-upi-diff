package com.truecaller.flashsdk.ui.send;

import android.content.Context;
import android.content.Intent;
import c.g.b.k;

public final class SendActivity$a
{
  private static Intent a(Context paramContext, long paramLong, String paramString1, String paramString2, int paramInt, boolean paramBoolean)
  {
    k.b(paramContext, "context");
    Intent localIntent = new android/content/Intent;
    Class localClass = SendActivity.class;
    localIntent.<init>(paramContext, localClass);
    localIntent.putExtra("to_phone", paramLong);
    localIntent.putExtra("to_name", paramString1);
    paramContext = "screen_context";
    localIntent.putExtra(paramContext, paramString2);
    localIntent.addFlags(268435456);
    localIntent.addFlags(536870912);
    int i = -1;
    if (paramInt != i)
    {
      paramContext = "notification_id";
      localIntent.putExtra(paramContext, paramInt);
    }
    localIntent.putExtra("prefilled_text", null);
    localIntent.putExtra("preset_flash_type", null);
    localIntent.putExtra("show_waiting", paramBoolean);
    return localIntent;
  }
  
  public static Intent a(Context paramContext, long paramLong, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, boolean paramBoolean, String paramString6)
  {
    k.b(paramContext, "context");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, SendActivity.class);
    localIntent.putExtra("to_phone", paramLong);
    localIntent.putExtra("to_name", paramString1);
    localIntent.putExtra("image", paramString3);
    localIntent.putExtra("video", paramString4);
    localIntent.putExtra("description", paramString5);
    localIntent.putExtra("background", paramString6);
    localIntent.putExtra("mode", paramBoolean);
    localIntent.putExtra("screen_context", paramString2);
    localIntent.addFlags(268435456);
    localIntent.addFlags(536870912);
    return localIntent;
  }
  
  public static void a(Context paramContext, long paramLong1, String paramString1, String paramString2, long paramLong2)
  {
    k.b(paramContext, "context");
    Intent localIntent = b(paramContext, paramLong1, paramString1, paramString2, paramLong2);
    paramContext.startActivity(localIntent);
  }
  
  private static Intent b(Context paramContext, long paramLong1, String paramString1, String paramString2, long paramLong2)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, SendActivity.class);
    localIntent.putExtra("to_phone", paramLong1);
    localIntent.putExtra("to_name", paramString1);
    localIntent.putExtra("screen_context", paramString2);
    localIntent.putExtra("time_left", paramLong2);
    localIntent.addFlags(268435456);
    localIntent.addFlags(536870912);
    return localIntent;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.send.SendActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */