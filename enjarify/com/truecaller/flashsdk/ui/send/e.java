package com.truecaller.flashsdk.ui.send;

import android.content.Context;
import com.truecaller.flashsdk.assist.aa;
import com.truecaller.flashsdk.assist.ae;
import com.truecaller.flashsdk.assist.al;
import com.truecaller.flashsdk.assist.b;
import com.truecaller.flashsdk.assist.d;
import com.truecaller.flashsdk.assist.g;
import com.truecaller.flashsdk.assist.q;
import com.truecaller.flashsdk.assist.y;
import com.truecaller.utils.l;

public final class e
{
  final SendActivity a;
  
  public e(SendActivity paramSendActivity)
  {
    a = paramSendActivity;
  }
  
  public static com.truecaller.flashsdk.assist.a a(SendActivity paramSendActivity)
  {
    c.g.b.k.b(paramSendActivity, "activity");
    b localb = new com/truecaller/flashsdk/assist/b;
    paramSendActivity = (Context)paramSendActivity;
    localb.<init>(paramSendActivity);
    return (com.truecaller.flashsdk.assist.a)localb;
  }
  
  public static i a(c.d.f paramf, com.google.firebase.messaging.a parama, ae paramae, aa paramaa, al paramal, g paramg, d paramd, com.truecaller.flashsdk.assist.a parama1, com.truecaller.flashsdk.d.a parama2, y paramy, com.google.gson.f paramf1, com.truecaller.flashsdk.core.k paramk, q paramq, l paraml, com.truecaller.common.g.a parama3)
  {
    c.g.b.k.b(paramf, "uiContext");
    c.g.b.k.b(parama, "messaging");
    c.g.b.k.b(paramae, "recentEmojiManager");
    c.g.b.k.b(paramaa, "preferenceUtil");
    c.g.b.k.b(paramal, "resourceProvider");
    c.g.b.k.b(paramg, "deviceUtils");
    c.g.b.k.b(paramd, "contactUtils");
    c.g.b.k.b(parama1, "colorProvider");
    c.g.b.k.b(parama2, "toolTipsManager");
    c.g.b.k.b(paramy, "locationFormatter");
    c.g.b.k.b(paramf1, "gson");
    c.g.b.k.b(paramk, "flashRequestHandler");
    c.g.b.k.b(paramq, "flashMediaHelper");
    c.g.b.k.b(paraml, "permissionUtil");
    c.g.b.k.b(parama3, "coreSettings");
    j localj = new com/truecaller/flashsdk/ui/send/j;
    localj.<init>(paramf, parama, paramae, paramaa, paramal, paramg, paramd, parama1, parama2, paramy, paramf1, paramk, paramq, paraml, parama3);
    return (i)localj;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.send.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */