package com.truecaller.flashsdk.ui.send;

import android.net.Uri;
import com.truecaller.flashsdk.models.ImageFlash;
import com.truecaller.flashsdk.ui.base.d;

public abstract interface c
  extends d
{
  public abstract void P();
  
  public abstract void Q();
  
  public abstract void R();
  
  public abstract void S();
  
  public abstract void T();
  
  public abstract void U();
  
  public abstract void V();
  
  public abstract void W();
  
  public abstract void X();
  
  public abstract void Y();
  
  public abstract void a(float paramFloat);
  
  public abstract void a(ImageFlash paramImageFlash);
  
  public abstract void a(String paramString1, String paramString2, boolean paramBoolean, long paramLong1, long paramLong2);
  
  public abstract void b(Uri paramUri);
  
  public abstract void f(String paramString);
  
  public abstract void f(String paramString1, String paramString2);
  
  public abstract void g(int paramInt);
  
  public abstract void h(int paramInt);
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.send.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */