package com.truecaller.flashsdk.ui.send;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import c.g.b.k;
import com.truecaller.flashsdk.models.ImageFlash;

public final class SendActivity$o
  extends BroadcastReceiver
{
  SendActivity$o(SendActivity paramSendActivity) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    String str = "context";
    k.b(paramContext, str);
    k.b(paramIntent, "intent");
    paramContext = paramIntent.getExtras();
    if (paramContext != null)
    {
      paramIntent = (i)a.c();
      str = paramContext.getString("extra_state");
      paramContext = (ImageFlash)paramContext.getParcelable("extra_image_flash");
      paramIntent.a(str, paramContext);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.send.SendActivity.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */