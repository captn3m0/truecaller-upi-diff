package com.truecaller.flashsdk.ui.send;

import android.os.Bundle;
import com.truecaller.flashsdk.models.ImageFlash;
import com.truecaller.flashsdk.ui.base.b;

public abstract interface i
  extends b
{
  public abstract void a(String paramString, ImageFlash paramImageFlash);
  
  public abstract void b(Bundle paramBundle);
  
  public abstract void d_(String paramString);
  
  public abstract void m();
  
  public abstract void n();
  
  public abstract void o();
  
  public abstract void p();
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.send.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */