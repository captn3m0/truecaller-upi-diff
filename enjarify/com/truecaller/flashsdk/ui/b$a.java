package com.truecaller.flashsdk.ui;

import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.PopupWindow.OnDismissListener;

public final class b$a
  implements PopupWindow.OnDismissListener
{
  public b$a(b paramb) {}
  
  public final void onDismiss()
  {
    ViewTreeObserver localViewTreeObserver = b.a(a).getViewTreeObserver();
    ViewTreeObserver.OnGlobalLayoutListener localOnGlobalLayoutListener = (ViewTreeObserver.OnGlobalLayoutListener)a;
    localViewTreeObserver.removeOnGlobalLayoutListener(localOnGlobalLayoutListener);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */