package com.truecaller.flashsdk.ui;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import android.database.ContentObserver;
import android.graphics.PorterDuff.Mode;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;
import com.truecaller.flashsdk.R.styleable;
import com.truecaller.flashsdk.core.b;
import com.truecaller.flashsdk.core.c;
import com.truecaller.flashsdk.db.g;
import com.truecaller.flashsdk.db.j;

public class FlashButton
  extends FrameLayout
  implements View.OnClickListener
{
  protected final Context a;
  protected String b;
  protected int c;
  protected int d;
  protected int e;
  protected PorterDuff.Mode f;
  protected PorterDuff.Mode g;
  private final Handler h;
  private final ImageView i;
  private long j;
  private long k;
  private String l;
  private FlashButton.a m;
  private View.OnClickListener n;
  private final Runnable o;
  
  public FlashButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  public FlashButton(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet, 0);
    Object localObject = new com/truecaller/flashsdk/ui/FlashButton$1;
    ((FlashButton.1)localObject).<init>(this);
    o = ((Runnable)localObject);
    localObject = getContext();
    int i1 = getLayout();
    inflate((Context)localObject, i1, this);
    setOnClickListener(this);
    a = paramContext;
    localObject = new android/os/Handler;
    ((Handler)localObject).<init>();
    h = ((Handler)localObject);
    int i2 = R.id.flash_button_image;
    localObject = (ImageView)findViewById(i2);
    i = ((ImageView)localObject);
    localObject = R.styleable.flash_button;
    paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject, 0, 0);
    int i3 = R.styleable.flash_button_normalColor;
    i3 = paramContext.getColor(i3, -12303292);
    paramByte = R.styleable.flash_button_tintModeAccent;
    PorterDuff.Mode localMode = a(paramContext, paramByte);
    a(i3, localMode);
    i3 = R.styleable.flash_button_disabledColor;
    i3 = paramContext.getColor(i3, -65536);
    paramByte = R.styleable.flash_button_tintModeProgress;
    localMode = a(paramContext, paramByte);
    b(i3, localMode);
    i3 = R.styleable.flash_button_buttonColor;
    paramByte = -16776961;
    i3 = paramContext.getColor(i3, paramByte);
    setButtonColor(i3);
    paramContext.recycle();
    boolean bool = isInEditMode();
    if (!bool)
    {
      int i4 = 8;
      setVisibility(i4);
    }
  }
  
  private static PorterDuff.Mode a(TypedArray paramTypedArray, int paramInt)
  {
    int i1 = paramTypedArray.getInt(paramInt, 0);
    paramInt = 1;
    if (i1 != paramInt) {
      return PorterDuff.Mode.SRC_IN;
    }
    return PorterDuff.Mode.MULTIPLY;
  }
  
  private void c()
  {
    Object localObject1 = c.a();
    long l1 = j;
    Object localObject2 = Long.toString(l1);
    long l2 = gb;
    k = l2;
    l2 = System.currentTimeMillis();
    long l3 = k;
    l2 -= l3;
    l3 = 60000L - l2;
    localObject1 = i;
    int i1 = c;
    PorterDuff.Mode localMode = f;
    ((ImageView)localObject1).setColorFilter(i1, localMode);
    l2 = 0L;
    boolean bool = l3 < l2;
    if (bool)
    {
      localObject1 = i;
      i1 = d;
      localMode = g;
      ((ImageView)localObject1).setColorFilter(i1, localMode);
      localObject1 = h;
      localObject2 = o;
      ((Handler)localObject1).removeCallbacks((Runnable)localObject2);
      localObject1 = h;
      localObject2 = o;
      ((Handler)localObject1).postDelayed((Runnable)localObject2, l3);
      int i2 = (int)l3;
      a(i2);
      return;
    }
    a(0);
  }
  
  private void d()
  {
    Object localObject1 = o;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = h;
      ((Handler)localObject2).removeCallbacks((Runnable)localObject1);
    }
    localObject1 = m;
    if (localObject1 != null)
    {
      localObject1 = a.getContentResolver();
      localObject2 = m;
      ((ContentResolver)localObject1).unregisterContentObserver((ContentObserver)localObject2);
    }
  }
  
  private Activity getActivity()
  {
    boolean bool1;
    for (Object localObject = getContext();; localObject = ((ContextWrapper)localObject).getBaseContext())
    {
      bool1 = localObject instanceof Activity;
      if (bool1) {
        break;
      }
      boolean bool2 = localObject instanceof ContextWrapper;
      if (!bool2) {
        break;
      }
    }
    if (bool1) {
      return (Activity)localObject;
    }
    localObject = new java/lang/RuntimeException;
    ((RuntimeException)localObject).<init>("Unable to get Activity.");
    throw ((Throwable)localObject);
  }
  
  protected void a(int paramInt) {}
  
  public final void a(int paramInt, PorterDuff.Mode paramMode)
  {
    c = paramInt;
    f = paramMode;
    ImageView localImageView = i;
    int i1 = c;
    localImageView.setColorFilter(i1, paramMode);
  }
  
  public final void a(long paramLong, String paramString1, String paramString2)
  {
    j = paramLong;
    b = paramString1;
    l = paramString2;
    Object localObject1 = c.a();
    Object localObject2 = Long.toString(j);
    paramLong = gb;
    k = paramLong;
    localObject1 = j.b;
    long l1 = j;
    localObject2 = Long.toString(l1);
    localObject1 = Uri.withAppendedPath((Uri)localObject1, (String)localObject2);
    localObject2 = m;
    if (localObject2 != null)
    {
      localObject2 = a.getContentResolver();
      paramString1 = m;
      ((ContentResolver)localObject2).unregisterContentObserver(paramString1);
    }
    localObject2 = new com/truecaller/flashsdk/ui/FlashButton$a;
    paramString1 = h;
    ((FlashButton.a)localObject2).<init>((Uri)localObject1, paramString1, this);
    m = ((FlashButton.a)localObject2);
    localObject2 = a.getContentResolver();
    paramString2 = m;
    ((ContentResolver)localObject2).registerContentObserver((Uri)localObject1, true, paramString2);
    setVisibility(0);
    c();
  }
  
  public boolean a()
  {
    long l1 = j;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (bool)
    {
      l1 = System.currentTimeMillis();
      l2 = k;
      l1 -= l2;
      l2 = 60000L;
      bool = l1 < l2;
      if (bool) {
        return true;
      }
    }
    return false;
  }
  
  public void b()
  {
    j = 0L;
    setVisibility(8);
    d();
  }
  
  public final void b(int paramInt, PorterDuff.Mode paramMode)
  {
    d = paramInt;
    g = paramMode;
  }
  
  protected int getLayout()
  {
    return R.layout.com_flashsdk_flash_button;
  }
  
  public void onClick(View paramView)
  {
    boolean bool = a();
    Object localObject1;
    String str2;
    if (bool)
    {
      b localb = c.a();
      localObject1 = getActivity();
      long l1 = j;
      String str1 = b;
      str2 = l;
      localb.a((Context)localObject1, l1, str1, str2);
      localObject2 = new android/os/Bundle;
      ((Bundle)localObject2).<init>();
      localObject1 = l;
      ((Bundle)localObject2).putString("flash_context", (String)localObject1);
      localb = c.a();
      localObject1 = "ANDROID_FLASH_TAPPED";
      localb.a((String)localObject1, (Bundle)localObject2);
    }
    else
    {
      long l2 = System.currentTimeMillis();
      localObject1 = c.a();
      Activity localActivity = getActivity();
      long l3 = j;
      str2 = b;
      String str3 = l;
      long l4 = k;
      l2 -= l4;
      long l5 = 60000L - l2;
      ((b)localObject1).a(localActivity, l3, str2, str3, l5);
    }
    Object localObject2 = n;
    if (localObject2 != null) {
      ((View.OnClickListener)localObject2).onClick(paramView);
    }
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    d();
  }
  
  public void setAccentColor(int paramInt)
  {
    PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
    a(paramInt, localMode);
  }
  
  public void setButtonColor(int paramInt)
  {
    e = paramInt;
  }
  
  protected void setPostOnClickListener(View.OnClickListener paramOnClickListener)
  {
    n = paramOnClickListener;
  }
  
  public void setProgressColor(int paramInt)
  {
    PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
    b(paramInt, localMode);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.FlashButton
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */