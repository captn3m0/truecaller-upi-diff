package com.truecaller.flashsdk.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.PopupWindow;
import c.g.b.k;
import com.truecaller.flashsdk.R.layout;

public final class b
  implements ViewTreeObserver.OnGlobalLayoutListener
{
  public final View a;
  public PopupWindow b;
  public final View c;
  
  public b(Context paramContext, View paramView)
  {
    c = paramView;
    paramContext = LayoutInflater.from(paramContext);
    int i = R.layout.flash_tooltip_layout;
    paramContext = paramContext.inflate(i, null, false);
    k.a(paramContext, "LayoutInflater.from(cont…ltip_layout, null, false)");
    a = paramContext;
  }
  
  public final void onGlobalLayout()
  {
    PopupWindow localPopupWindow = b;
    if (localPopupWindow != null)
    {
      View localView = c;
      localPopupWindow.update(localView, 0, 0, -1, -1);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */