package com.truecaller.flashsdk.ui.b;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.f;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import c.g.b.k;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;
import com.truecaller.flashsdk.ui.whatsnew.FlashWithFriendsActivity.a;
import java.util.HashMap;

public final class a
  extends android.support.design.widget.b
  implements b
{
  public c a;
  private ImageView b;
  private ImageView c;
  private HashMap d;
  
  public final c a()
  {
    c localc = a;
    if (localc == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localc;
  }
  
  public final void a(Uri paramUri, String paramString)
  {
    k.b(paramUri, "imageUri");
    k.b(paramString, "shareText");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.SEND");
    localIntent.setType("image/jpeg");
    paramUri = (Parcelable)paramUri;
    localIntent.putExtra("android.intent.extra.STREAM", paramUri);
    localIntent.putExtra("android.intent.extra.TEXT", paramString);
    paramUri = (CharSequence)"Share image";
    paramUri = Intent.createChooser(localIntent, paramUri);
    startActivity(paramUri);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    k.b(paramString1, "imageUrl");
    k.b(paramString2, "imageDescription");
    Context localContext = getContext();
    if (localContext == null) {
      return;
    }
    k.a(localContext, "context ?: return");
    boolean bool = true;
    paramString1 = FlashWithFriendsActivity.a.a(localContext, paramString1, null, null, paramString2, bool, paramString3);
    paramString1.addFlags(268435456);
    paramString1.addFlags(536870912);
    int i = 67108864;
    paramString1.addFlags(i);
    localContext.startActivity(paramString1);
    paramString1 = getActivity();
    if (paramString1 != null)
    {
      paramString1.finish();
      return;
    }
  }
  
  public final Bundle b()
  {
    return getArguments();
  }
  
  public final void c()
  {
    dismiss();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    Object localObject = getActivity();
    if (localObject != null)
    {
      com.truecaller.flashsdk.core.b localb = com.truecaller.flashsdk.core.c.a();
      int i = localb.g();
      ((f)localObject).setTheme(i);
    }
    super.onCreate(paramBundle);
    paramBundle = com.truecaller.flashsdk.ui.b.a.a.a();
    localObject = com.truecaller.flashsdk.core.c.b;
    localObject = com.truecaller.flashsdk.core.c.b();
    paramBundle = paramBundle.a((com.truecaller.flashsdk.core.a.a.a)localObject);
    localObject = new com/truecaller/flashsdk/ui/b/a/c;
    ((com.truecaller.flashsdk.ui.b.a.c)localObject).<init>();
    paramBundle.a((com.truecaller.flashsdk.ui.b.a.c)localObject).a().a(this);
    paramBundle = a;
    if (paramBundle == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    localObject = this;
    localObject = (b)this;
    paramBundle.a((b)localObject);
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    k.b(paramLayoutInflater, "inflater");
    int i = R.layout.layout_flash_bottom_share;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public final void onViewCreated(View paramView, Bundle paramBundle)
  {
    k.b(paramView, "view");
    super.onViewCreated(paramView, paramBundle);
    int i = R.id.imageShare;
    paramBundle = paramView.findViewById(i);
    String str = "view.findViewById(R.id.imageShare)";
    k.a(paramBundle, str);
    paramBundle = (ImageView)paramBundle;
    b = paramBundle;
    i = R.id.imageShareViaFlash;
    paramView = paramView.findViewById(i);
    paramBundle = "view.findViewById(R.id.imageShareViaFlash)";
    k.a(paramView, paramBundle);
    paramView = (ImageView)paramView;
    c = paramView;
    paramView = b;
    if (paramView == null)
    {
      paramBundle = "shareImage";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/flashsdk/ui/b/a$a;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
    paramView = c;
    if (paramView == null)
    {
      paramBundle = "shareViaFlash";
      k.a(paramBundle);
    }
    paramBundle = new com/truecaller/flashsdk/ui/b/a$b;
    paramBundle.<init>(this);
    paramBundle = (View.OnClickListener)paramBundle;
    paramView.setOnClickListener(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */