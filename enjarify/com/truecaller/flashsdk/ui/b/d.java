package com.truecaller.flashsdk.ui.b;

import android.net.Uri;
import android.os.Bundle;
import c.a.m;
import c.a.y;
import c.u;
import com.truecaller.flashsdk.models.Payload;
import com.truecaller.flashsdk.models.QueuedFlash;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

public final class d
  implements c
{
  private b a;
  private Uri b;
  private String c = "";
  private String d = "";
  private QueuedFlash e;
  
  private final void a(Bundle paramBundle)
  {
    Object localObject = paramBundle.getString("share_image");
    if (localObject == null) {
      return;
    }
    localObject = Uri.parse((String)localObject);
    c.g.b.k.a(localObject, "Uri.parse(bundle.getString(SHARE_IMAGE) ?: return)");
    b = ((Uri)localObject);
    localObject = paramBundle.getString("share_text");
    c.g.b.k.a(localObject, "bundle.getString(SHARE_TEXT)");
    c = ((String)localObject);
    paramBundle = (QueuedFlash)paramBundle.getParcelable("flash");
    e = paramBundle;
  }
  
  public final void a()
  {
    b localb = a;
    if (localb == null)
    {
      localObject = "presenterView";
      c.g.b.k.a((String)localObject);
    }
    Object localObject = b;
    if (localObject == null)
    {
      str = "imageUri";
      c.g.b.k.a(str);
    }
    String str = c;
    localb.a((Uri)localObject, str);
    localb = a;
    if (localb == null)
    {
      localObject = "presenterView";
      c.g.b.k.a((String)localObject);
    }
    localb.c();
  }
  
  public final void a(b paramb)
  {
    String str = "shareImageFragmentView";
    c.g.b.k.b(paramb, str);
    a = paramb;
    paramb = a;
    if (paramb == null)
    {
      str = "presenterView";
      c.g.b.k.a(str);
    }
    paramb = paramb.b();
    if (paramb == null) {
      return;
    }
    a(paramb);
  }
  
  public final void b()
  {
    Object localObject1 = e;
    if (localObject1 == null) {
      return;
    }
    localObject1 = ((QueuedFlash)localObject1).f();
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = ((Payload)localObject1).d();
    c.g.b.k.a(localObject2, "payload.attachment");
    localObject2 = (CharSequence)localObject2;
    Object localObject3 = new c/n/k;
    ((c.n.k)localObject3).<init>(",");
    localObject2 = ((c.n.k)localObject3).a((CharSequence)localObject2, 0);
    boolean bool1 = ((List)localObject2).isEmpty();
    int j = 1;
    Object localObject4;
    if (!bool1)
    {
      int i = ((List)localObject2).size();
      localObject3 = ((List)localObject2).listIterator(i);
      int k;
      do
      {
        boolean bool2 = ((ListIterator)localObject3).hasPrevious();
        if (!bool2) {
          break;
        }
        localObject4 = (CharSequence)((ListIterator)localObject3).previous();
        k = ((CharSequence)localObject4).length();
        if (k == 0)
        {
          k = 1;
        }
        else
        {
          k = 0;
          localObject4 = null;
        }
      } while (k != 0);
      localObject2 = (Iterable)localObject2;
      i = ((ListIterator)localObject3).nextIndex() + j;
      localObject2 = m.d((Iterable)localObject2, i);
    }
    else
    {
      localObject2 = (List)y.a;
    }
    localObject2 = (Collection)localObject2;
    if (localObject2 != null)
    {
      localObject3 = new String[0];
      localObject2 = ((Collection)localObject2).toArray((Object[])localObject3);
      if (localObject2 != null)
      {
        localObject2 = (String[])localObject2;
        localObject3 = "";
        localObject4 = ((Payload)localObject1).b();
        int m = localObject2.length;
        int n = 2;
        if (m == n)
        {
          localObject1 = localObject2[0];
          d = ((String)localObject1);
          localObject3 = localObject2[j];
        }
        else
        {
          localObject1 = ((Payload)localObject1).d();
          localObject2 = "payload.attachment";
          c.g.b.k.a(localObject1, (String)localObject2);
          d = ((String)localObject1);
        }
        localObject1 = a;
        if (localObject1 == null)
        {
          localObject2 = "presenterView";
          c.g.b.k.a((String)localObject2);
        }
        localObject2 = d;
        c.g.b.k.a(localObject4, "imageDescription");
        ((b)localObject1).a((String)localObject2, (String)localObject4, (String)localObject3);
        return;
      }
      localObject1 = new c/u;
      ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.Array<T>");
      throw ((Throwable)localObject1);
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type java.util.Collection<T>");
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.b.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */