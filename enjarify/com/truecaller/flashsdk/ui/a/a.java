package com.truecaller.flashsdk.ui.a;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.d.b.ab;
import com.d.b.ai;
import com.d.b.w;
import com.truecaller.common.h.aq.d;
import com.truecaller.flashsdk.R.drawable;
import com.truecaller.flashsdk.R.layout;
import com.truecaller.flashsdk.assist.x;
import com.truecaller.flashsdk.models.c;
import com.truecaller.flashsdk.models.d;
import com.truecaller.flashsdk.ui.FlashButton;
import java.util.List;

public final class a
  extends RecyclerView.Adapter
{
  private final LayoutInflater a;
  private final Context b;
  private final w c;
  private final List d;
  private final x e;
  private final b f;
  
  public a(Context paramContext, w paramw, List paramList, x paramx, b paramb)
  {
    b = paramContext;
    c = paramw;
    d = paramList;
    e = paramx;
    f = paramb;
    paramContext = LayoutInflater.from(b);
    k.a(paramContext, "LayoutInflater.from(context)");
    a = paramContext;
  }
  
  public final int getItemCount()
  {
    return d.size();
  }
  
  public final long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public final int getItemViewType(int paramInt)
  {
    return ((d)d.get(paramInt)).d();
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    k.b(paramViewHolder, "holder");
    Object localObject1 = d;
    Object localObject2 = (d)((List)localObject1).get(paramInt);
    int i = ((d)localObject2).d();
    int j = R.layout.flash_popup_content_header;
    if (i == j)
    {
      localObject1 = f;
      paramViewHolder = (e)paramViewHolder;
      if (localObject2 != null)
      {
        localObject2 = (c)localObject2;
        ((b)localObject1).a(paramViewHolder, (c)localObject2);
        return;
      }
      paramViewHolder = new c/u;
      paramViewHolder.<init>("null cannot be cast to non-null type com.truecaller.flashsdk.models.FlashPopupHeaderItem");
      throw paramViewHolder;
    }
    paramViewHolder = (a.a)paramViewHolder;
    if (localObject2 != null)
    {
      localObject2 = (com.truecaller.flashsdk.models.a)localObject2;
      k.b(localObject2, "contact");
      localObject1 = ((com.truecaller.flashsdk.models.a)localObject2).a();
      Object localObject3 = ((com.truecaller.flashsdk.models.a)localObject2).c();
      localObject2 = ((com.truecaller.flashsdk.models.a)localObject2).b();
      Object localObject4 = b;
      k.a(localObject2, "phone");
      long l = Long.parseLong((String)localObject2);
      ((FlashButton)localObject4).a(l, (String)localObject1, "flashShare");
      localObject2 = b;
      int k = 0;
      localObject4 = null;
      ((FlashButton)localObject2).setVisibility(0);
      localObject2 = d.b;
      int m = R.drawable.ic_flash_empty_avatar_round;
      localObject2 = android.support.v4.content.b.a((Context)localObject2, m);
      Object localObject5 = localObject3;
      localObject5 = (CharSequence)localObject3;
      if (localObject5 != null)
      {
        m = ((CharSequence)localObject5).length();
        if (m != 0) {}
      }
      else
      {
        k = 1;
      }
      if (k == 0)
      {
        localObject3 = d.c.a((String)localObject3);
        localObject4 = (ai)aq.d.b();
        localObject2 = ((ab)localObject3).a((ai)localObject4).a((Drawable)localObject2).b((Drawable)localObject2);
        localObject3 = c;
        k = 0;
        localObject4 = null;
        ((ab)localObject2).a((ImageView)localObject3, null);
      }
      else
      {
        localObject3 = c;
        ((ImageView)localObject3).setImageDrawable((Drawable)localObject2);
      }
      localObject2 = a;
      localObject1 = (CharSequence)localObject1;
      ((TextView)localObject2).setText((CharSequence)localObject1);
      localObject2 = itemView;
      paramViewHolder = (View.OnClickListener)paramViewHolder;
      ((View)localObject2).setOnClickListener(paramViewHolder);
      return;
    }
    paramViewHolder = new c/u;
    paramViewHolder.<init>("null cannot be cast to non-null type com.truecaller.flashsdk.models.FlashFavouriteContact");
    throw paramViewHolder;
  }
  
  public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    Object localObject1 = "parent";
    k.b(paramViewGroup, (String)localObject1);
    int i = R.layout.flash_popup_content_header;
    if (paramInt == i)
    {
      localObject2 = new com/truecaller/flashsdk/ui/a/f;
      localObject1 = a;
      j = R.layout.flash_popup_content_header;
      paramViewGroup = ((LayoutInflater)localObject1).inflate(j, paramViewGroup, false);
      k.a(paramViewGroup, "inflater.inflate(R.layou…nt_header, parent, false)");
      localObject1 = c;
      ((f)localObject2).<init>(paramViewGroup, (w)localObject1);
      return (RecyclerView.ViewHolder)localObject2;
    }
    Object localObject2 = new com/truecaller/flashsdk/ui/a/a$a;
    localObject1 = a;
    int j = R.layout.flashsdk_item_favourite_contact;
    paramViewGroup = ((LayoutInflater)localObject1).inflate(j, paramViewGroup, false);
    k.a(paramViewGroup, "inflater.inflate(R.layou…e_contact, parent, false)");
    ((a.a)localObject2).<init>(this, paramViewGroup);
    return (RecyclerView.ViewHolder)localObject2;
  }
  
  public final void onViewAttachedToWindow(RecyclerView.ViewHolder paramViewHolder)
  {
    String str = "holder";
    k.b(paramViewHolder, str);
    boolean bool = paramViewHolder instanceof e;
    if (!bool) {
      paramViewHolder = null;
    }
    paramViewHolder = (e)paramViewHolder;
    if (paramViewHolder != null)
    {
      f.a(paramViewHolder);
      return;
    }
  }
  
  public final void onViewDetachedFromWindow(RecyclerView.ViewHolder paramViewHolder)
  {
    String str = "holder";
    k.b(paramViewHolder, str);
    boolean bool = paramViewHolder instanceof e;
    if (!bool) {
      paramViewHolder = null;
    }
    paramViewHolder = (e)paramViewHolder;
    if (paramViewHolder != null)
    {
      f.b(paramViewHolder);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */