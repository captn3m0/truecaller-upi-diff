package com.truecaller.flashsdk.ui.a;

import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import c.g.b.k;
import com.d.b.ab;
import com.d.b.w;
import com.google.android.c.a.c;
import com.google.android.c.a.c.a;
import com.google.android.c.a.c.b;
import com.google.android.c.a.c.c;
import com.truecaller.flashsdk.R.attr;
import com.truecaller.flashsdk.R.drawable;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.string;
import java.util.HashMap;
import kotlinx.a.a.a;

public final class f
  extends RecyclerView.ViewHolder
  implements c.a, e, a
{
  private c a;
  private String b;
  private final Drawable c;
  private final View d;
  private final w e;
  private HashMap f;
  
  public f(View paramView, w paramw)
  {
    super(paramView);
    d = paramView;
    e = paramw;
    paramView = d.getContext();
    int i = R.drawable.flash_friend_popup;
    int j = R.attr.theme_incoming_text;
    PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
    paramView = com.truecaller.utils.ui.b.a(paramView, i, j, localMode);
    c = paramView;
    int k = R.id.image;
    paramView = (ImageView)a(k);
    paramw = c;
    paramView.setImageDrawable(paramw);
  }
  
  private View a(int paramInt)
  {
    Object localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      f = ((HashMap)localObject1);
    }
    localObject1 = f;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = a();
      if (localObject1 == null) {
        return null;
      }
      localObject1 = ((View)localObject1).findViewById(paramInt);
      localObject2 = f;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final View a()
  {
    return d;
  }
  
  public final void a(c.c paramc, com.google.android.c.a.b paramb)
  {
    k.b(paramc, "provider");
    k.b(paramb, "youTubeInitializationResult");
    paramc = d.getContext();
    int i = R.string.error_youtube_player;
    paramb = (CharSequence)paramc.getString(i);
    Toast.makeText(paramc, paramb, 0).show();
  }
  
  public final void a(c.c paramc, c paramc1, boolean paramBoolean)
  {
    String str = "provider";
    k.b(paramc, str);
    k.b(paramc1, "youTubePlayer");
    a = paramc1;
    paramc = a;
    if (paramc != null)
    {
      if (!paramBoolean)
      {
        paramc1 = c.b.b;
        paramc.a(paramc1);
        paramc1 = b;
        if (paramc1 == null) {
          return;
        }
        paramc.a(paramc1);
      }
      return;
    }
  }
  
  public final void a(e.a parama)
  {
    k.b(parama, "youtubeInitHelper");
    Object localObject1 = b;
    if (localObject1 != null)
    {
      int i = R.id.youtubeContainer;
      boolean bool = parama.a(i);
      if (!bool) {
        localObject1 = null;
      }
      if (localObject1 != null)
      {
        localObject1 = new com/google/android/c/a/d;
        ((com.google.android.c.a.d)localObject1).<init>();
        int j = R.id.youtubeContainer;
        parama.a(j, (com.google.android.c.a.d)localObject1);
        Object localObject2 = this;
        localObject2 = (c.a)this;
        ((com.google.android.c.a.d)localObject1).a("AIzaSyCd6tpLEKJi-5w6SDpTpzj6UTZpS47j7fw", (c.a)localObject2);
        return;
      }
    }
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "imageUrl");
    int i = R.id.image;
    Object localObject = (ImageView)a(i);
    k.a(localObject, "image");
    ((ImageView)localObject).setVisibility(0);
    paramString = e.a(paramString);
    localObject = c;
    paramString = paramString.a((Drawable)localObject);
    i = R.id.image;
    localObject = (ImageView)a(i);
    paramString.a((ImageView)localObject, null);
  }
  
  public final void a(String paramString, e.a parama)
  {
    k.b(paramString, "videoUrl");
    k.b(parama, "youtubeInitHelper");
    b = paramString;
    int i = R.id.image;
    paramString = (ImageView)a(i);
    k.a(paramString, "image");
    paramString.setVisibility(8);
    i = R.id.youtubeContainer;
    paramString = (FrameLayout)a(i);
    k.a(paramString, "youtubeContainer");
    paramString.setVisibility(0);
  }
  
  public final void b()
  {
    try
    {
      c localc = a;
      if (localc != null)
      {
        boolean bool = localc.b();
        if (!bool) {
          localc = null;
        }
        if (localc != null) {
          localc.a();
        }
      }
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      com.truecaller.log.d.a((Throwable)localIllegalStateException);
    }
  }
  
  public final void b(String paramString)
  {
    k.b(paramString, "text");
    int i = R.id.headerText;
    TextView localTextView = (TextView)a(i);
    k.a(localTextView, "headerText");
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */