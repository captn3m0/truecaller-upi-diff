package com.truecaller.flashsdk.ui.a;

import android.os.Bundle;
import c.g.b.k;
import c.n.m;

public final class c
  implements b
{
  private e.a a;
  
  public final void a(e.a parama)
  {
    a = parama;
  }
  
  public final void a(e parame)
  {
    k.b(parame, "headerItemView");
    e.a locala = a;
    if (locala != null)
    {
      parame.a(locala);
      return;
    }
  }
  
  public final void a(e parame, com.truecaller.flashsdk.models.c paramc)
  {
    k.b(parame, "headerItemView");
    Object localObject1 = "flashPopupHeaderItem";
    k.b(paramc, (String)localObject1);
    paramc = a;
    if (paramc == null) {
      return;
    }
    localObject1 = paramc.getString("image");
    Object localObject2;
    boolean bool1;
    boolean bool2;
    if (localObject1 != null)
    {
      localObject2 = localObject1;
      localObject2 = (CharSequence)localObject1;
      bool1 = m.a((CharSequence)localObject2) ^ true;
      if (!bool1)
      {
        bool2 = false;
        localObject1 = null;
      }
      if (localObject1 != null) {
        parame.a((String)localObject1);
      }
    }
    localObject1 = paramc.getString("video");
    if (localObject1 != null)
    {
      localObject2 = localObject1;
      localObject2 = (CharSequence)localObject1;
      bool1 = m.a((CharSequence)localObject2) ^ true;
      if (!bool1)
      {
        bool2 = false;
        localObject1 = null;
      }
      if (localObject1 != null)
      {
        localObject2 = a;
        if (localObject2 != null) {
          parame.a((String)localObject1, (e.a)localObject2);
        }
      }
    }
    localObject1 = "promo";
    paramc = paramc.getString((String)localObject1);
    if (paramc != null)
    {
      localObject1 = paramc;
      localObject1 = (CharSequence)paramc;
      bool2 = m.a((CharSequence)localObject1) ^ true;
      if (!bool2) {
        paramc = null;
      }
      if (paramc != null)
      {
        parame.b(paramc);
        return;
      }
    }
  }
  
  public final void b(e parame)
  {
    k.b(parame, "headerItemView");
    parame.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */