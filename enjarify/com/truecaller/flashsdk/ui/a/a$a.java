package com.truecaller.flashsdk.ui.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import c.g.b.k;
import com.truecaller.flashsdk.R.attr;
import com.truecaller.flashsdk.R.drawable;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.assist.x;
import com.truecaller.flashsdk.ui.FlashButton;
import com.truecaller.utils.ui.b;
import java.util.List;

public final class a$a
  extends RecyclerView.ViewHolder
  implements View.OnClickListener
{
  final TextView a;
  final FlashButton b;
  final ImageView c;
  
  public a$a(a parama, View paramView)
  {
    super(paramView);
    int i = R.id.textName;
    parama = paramView.findViewById(i);
    k.a(parama, "view.findViewById(R.id.textName)");
    parama = (TextView)parama;
    a = parama;
    i = R.id.flash_button;
    parama = paramView.findViewById(i);
    k.a(parama, "view.findViewById(R.id.flash_button)");
    parama = (FlashButton)parama;
    b = parama;
    i = R.id.imageAvatar;
    parama = paramView.findViewById(i);
    k.a(parama, "view.findViewById(R.id.imageAvatar)");
    parama = (ImageView)parama;
    c = parama;
    parama = b;
    paramView = paramView.getContext();
    int j = R.drawable.bg_solid_white_rad_4dp;
    int k = R.attr.colorPrimary;
    paramView = b.a(paramView, j, k);
    parama.setBackground(paramView);
  }
  
  public final void onClick(View paramView)
  {
    int i = getAdapterPosition();
    paramView = Integer.valueOf(i);
    Object localObject = paramView;
    localObject = (Number)paramView;
    int j = ((Number)localObject).intValue();
    int k = 1;
    int m = -1;
    if (j != m)
    {
      j = 1;
    }
    else
    {
      j = 0;
      localObject = null;
    }
    m = 0;
    Context localContext = null;
    if (j == 0)
    {
      i = 0;
      paramView = null;
    }
    if (paramView != null)
    {
      i = ((Number)paramView).intValue();
      localObject = a.b(d);
      paramView = ((List)localObject).get(i);
      boolean bool = paramView instanceof com.truecaller.flashsdk.models.a;
      if (!bool)
      {
        i = 0;
        paramView = null;
      }
      paramView = (com.truecaller.flashsdk.models.a)paramView;
      if (paramView != null)
      {
        localObject = b;
        bool = ((FlashButton)localObject).a();
        if (bool)
        {
          a.c(d).a(paramView);
          return;
        }
        localObject = a.a(d);
        localContext = a.a(d);
        int n = R.string.please_wait_before_sending;
        Object[] arrayOfObject = new Object[k];
        paramView = paramView.a();
        arrayOfObject[0] = paramView;
        paramView = (CharSequence)localContext.getString(n, arrayOfObject);
        Toast.makeText((Context)localObject, paramView, 0).show();
        return;
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.a.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */