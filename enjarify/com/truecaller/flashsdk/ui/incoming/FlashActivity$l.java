package com.truecaller.flashsdk.ui.incoming;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

final class FlashActivity$l
  implements View.OnTouchListener
{
  FlashActivity$l(FlashActivity paramFlashActivity) {}
  
  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    paramView.performClick();
    ((h)a.c()).p();
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.incoming.FlashActivity.l
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */