package com.truecaller.flashsdk.ui.incoming;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.ImageFlash;
import com.truecaller.flashsdk.models.QueuedFlash;
import com.truecaller.flashsdk.ui.base.d;
import com.truecaller.flashsdk.ui.customviews.a.a;
import com.truecaller.flashsdk.ui.customviews.a.h;
import java.util.List;

public abstract interface j
  extends d
{
  public abstract void P();
  
  public abstract void Q();
  
  public abstract void R();
  
  public abstract void S();
  
  public abstract void T();
  
  public abstract void U();
  
  public abstract void V();
  
  public abstract void W();
  
  public abstract void X();
  
  public abstract void Y();
  
  public abstract void Z();
  
  public abstract void a(Intent paramIntent);
  
  public abstract void a(Drawable paramDrawable);
  
  public abstract void a(Uri paramUri, String paramString);
  
  public abstract void a(Flash paramFlash);
  
  public abstract void a(ImageFlash paramImageFlash);
  
  public abstract void a(QueuedFlash paramQueuedFlash);
  
  public abstract void a(a parama);
  
  public abstract void a(h paramh);
  
  public abstract void a(String paramString1, long paramLong, String paramString2);
  
  public abstract void a(List paramList);
  
  public abstract void aa();
  
  public abstract void ab();
  
  public abstract void ac();
  
  public abstract void ad();
  
  public abstract void ae();
  
  public abstract void af();
  
  public abstract void ag();
  
  public abstract void ah();
  
  public abstract void ai();
  
  public abstract void aj();
  
  public abstract void ak();
  
  public abstract void al();
  
  public abstract void am();
  
  public abstract void an();
  
  public abstract void b(int paramInt1, int paramInt2);
  
  public abstract void b(Drawable paramDrawable);
  
  public abstract void b(Uri paramUri);
  
  public abstract void b(Flash paramFlash);
  
  public abstract void b(String paramString1, String paramString2, String paramString3);
  
  public abstract void b(List paramList);
  
  public abstract void c(int paramInt1, int paramInt2);
  
  public abstract void c(Flash paramFlash);
  
  public abstract void c(String paramString1, String paramString2, String paramString3);
  
  public abstract void c(List paramList);
  
  public abstract void d(Flash paramFlash);
  
  public abstract void e(Flash paramFlash);
  
  public abstract void e(boolean paramBoolean);
  
  public abstract void f(String paramString);
  
  public abstract void f(String paramString1, String paramString2);
  
  public abstract void f(boolean paramBoolean);
  
  public abstract void g(int paramInt);
  
  public abstract void g(String paramString);
  
  public abstract void g(boolean paramBoolean);
  
  public abstract boolean g(String paramString1, String paramString2);
  
  public abstract void h(int paramInt);
  
  public abstract void h(String paramString);
  
  public abstract void h(String paramString1, String paramString2);
  
  public abstract void h(boolean paramBoolean);
  
  public abstract void i(int paramInt);
  
  public abstract void i(String paramString);
  
  public abstract void i(boolean paramBoolean);
  
  public abstract void j(int paramInt);
  
  public abstract void j(String paramString);
  
  public abstract void k(int paramInt);
  
  public abstract void k(String paramString);
  
  public abstract void l(int paramInt);
  
  public abstract void l(String paramString);
  
  public abstract void m(int paramInt);
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.incoming.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */