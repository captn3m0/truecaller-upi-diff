package com.truecaller.flashsdk.ui.incoming;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import com.truecaller.flashsdk.models.ImageFlash;
import com.truecaller.flashsdk.ui.base.b;

public abstract interface h
  extends b
{
  public abstract void a(int paramInt, String paramString);
  
  public abstract void a(Bitmap paramBitmap);
  
  public abstract void a(Bundle paramBundle, String paramString);
  
  public abstract void a(CharSequence paramCharSequence, boolean paramBoolean);
  
  public abstract void a(String paramString, ImageFlash paramImageFlash);
  
  public abstract boolean a(KeyEvent paramKeyEvent);
  
  public abstract void b(CharSequence paramCharSequence, boolean paramBoolean);
  
  public abstract void c(boolean paramBoolean);
  
  public abstract boolean m();
  
  public abstract void n();
  
  public abstract void o();
  
  public abstract void p();
  
  public abstract void q();
  
  public abstract void r();
  
  public abstract void s();
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.incoming.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */