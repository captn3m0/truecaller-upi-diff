package com.truecaller.flashsdk.ui.incoming;

public final class k
{
  private static final int a = 2;
  private static final int b = 700;
  private static final float c = 0.2F;
  private static final int d = 400;
  private static final int e = 50;
  private static final long f = 400L;
  private static final int g = 180;
  private static final int h = 360;
  
  public static final int a()
  {
    return a;
  }
  
  public static final int b()
  {
    return b;
  }
  
  public static final float c()
  {
    return c;
  }
  
  public static final int d()
  {
    return d;
  }
  
  public static final int e()
  {
    return e;
  }
  
  public static final long f()
  {
    return f;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.incoming.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */