package com.truecaller.flashsdk.ui.incoming;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.PopupWindow;
import c.g.b.k;
import c.u;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.ui.whatsnew.b;

final class FlashActivity$n
  implements Runnable
{
  FlashActivity$n(FlashActivity paramFlashActivity, String paramString) {}
  
  public final void run()
  {
    Object localObject1 = new com/truecaller/flashsdk/ui/whatsnew/b;
    Object localObject2 = a;
    Object localObject3 = localObject2;
    localObject3 = (Context)localObject2;
    int i = R.string.tip_first_received_flash;
    int j = 1;
    Object localObject4 = new Object[j];
    String str1 = b;
    localObject4[0] = str1;
    localObject2 = ((FlashActivity)localObject2).getString(i, (Object[])localObject4);
    String str2 = "getString(R.string.tip_first_received_flash, name)";
    k.a(localObject2, str2);
    ((b)localObject1).<init>((Context)localObject3, (String)localObject2);
    localObject2 = (View)FlashActivity.b(a);
    k.b(localObject2, "view");
    localObject3 = ((View)localObject2).getContext();
    if (localObject3 != null)
    {
      localObject3 = (Activity)localObject3;
      boolean bool = ((Activity)localObject3).isFinishing();
      if (!bool)
      {
        localObject3 = ((View)localObject2).getApplicationWindowToken();
        if (localObject3 != null)
        {
          localObject3 = a.getContentView();
          ((View)localObject3).measure(0, 0);
          i = ((View)localObject2).getMeasuredWidth() / 2;
          localObject4 = "contentView";
          k.a(localObject3, (String)localObject4);
          int k = ((View)localObject3).getMeasuredWidth() / 2;
          i -= k;
          localObject1 = a;
          ((PopupWindow)localObject1).showAsDropDown((View)localObject2, i, 0);
        }
      }
      localObject1 = new com/truecaller/flashsdk/ui/whatsnew/b;
      localObject2 = a;
      localObject3 = localObject2;
      localObject3 = (Context)localObject2;
      i = R.string.tip_first_flash_reply;
      localObject2 = ((FlashActivity)localObject2).getString(i);
      k.a(localObject2, "getString(R.string.tip_first_flash_reply)");
      ((b)localObject1).<init>((Context)localObject3, (String)localObject2);
      localObject2 = (View)FlashActivity.c(a);
      ((b)localObject1).a((View)localObject2, 0);
      return;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type android.app.Activity");
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.incoming.FlashActivity.n
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */