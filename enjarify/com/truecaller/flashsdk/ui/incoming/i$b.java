package com.truecaller.flashsdk.ui.incoming;

import android.graphics.Bitmap;
import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.g.a.m;
import c.o.b;
import c.x;
import kotlinx.coroutines.ag;

final class i$b
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag e;
  
  i$b(i parami, Bitmap paramBitmap, j paramj, c paramc)
  {
    super(2, paramc);
  }
  
  public final c a(Object paramObject, c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    b localb = new com/truecaller/flashsdk/ui/incoming/i$b;
    i locali = b;
    Bitmap localBitmap = c;
    j localj = d;
    localb.<init>(locali, localBitmap, localj, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return localb;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject = a.a;
    int i = a;
    if (i == 0)
    {
      boolean bool = paramObject instanceof o.b;
      if (!bool)
      {
        paramObject = i.c(b);
        localObject = c;
        paramObject = ((com.truecaller.flashsdk.assist.j)paramObject).a((Bitmap)localObject);
        if (paramObject == null) {
          return x.a;
        }
        localObject = d;
        String str = i.d(b);
        ((j)localObject).a((Uri)paramObject, str);
        return x.a;
      }
      throw a;
    }
    paramObject = new java/lang/IllegalStateException;
    ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
    throw ((Throwable)paramObject);
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c)paramObject2;
    paramObject1 = (b)a(paramObject1, (c)paramObject2);
    paramObject2 = x.a;
    return ((b)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.incoming.i.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */