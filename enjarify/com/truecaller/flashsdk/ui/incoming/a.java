package com.truecaller.flashsdk.ui.incoming;

import android.app.WallpaperManager;
import com.d.b.w;
import javax.inject.Provider;

public final class a
  implements b
{
  private final com.truecaller.flashsdk.core.a.a.a a;
  private Provider b;
  private Provider c;
  private Provider d;
  private Provider e;
  private Provider f;
  private Provider g;
  private Provider h;
  private Provider i;
  private Provider j;
  private Provider k;
  private Provider l;
  private Provider m;
  private Provider n;
  private Provider o;
  private Provider p;
  private Provider q;
  private Provider r;
  private Provider s;
  
  private a(c paramc, com.truecaller.flashsdk.core.a.a.a parama)
  {
    a = parama;
    Object localObject3 = new com/truecaller/flashsdk/ui/incoming/a$o;
    ((a.o)localObject3).<init>(parama);
    b = ((Provider)localObject3);
    localObject3 = new com/truecaller/flashsdk/ui/incoming/a$g;
    ((a.g)localObject3).<init>(parama);
    c = ((Provider)localObject3);
    localObject3 = new com/truecaller/flashsdk/ui/incoming/a$e;
    ((a.e)localObject3).<init>(parama);
    d = ((Provider)localObject3);
    localObject3 = new com/truecaller/flashsdk/ui/incoming/a$m;
    ((a.m)localObject3).<init>(parama);
    e = ((Provider)localObject3);
    localObject3 = new com/truecaller/flashsdk/ui/incoming/a$n;
    ((a.n)localObject3).<init>(parama);
    f = ((Provider)localObject3);
    localObject3 = new com/truecaller/flashsdk/ui/incoming/a$d;
    ((a.d)localObject3).<init>(parama);
    g = ((Provider)localObject3);
    localObject3 = new com/truecaller/flashsdk/ui/incoming/a$b;
    ((a.b)localObject3).<init>(parama);
    h = ((Provider)localObject3);
    localObject3 = dagger.a.c.a(e.a(paramc));
    i = ((Provider)localObject3);
    localObject3 = i;
    localObject3 = dagger.a.c.a(d.a(paramc, (Provider)localObject3));
    j = ((Provider)localObject3);
    localObject3 = new com/truecaller/flashsdk/ui/incoming/a$i;
    ((a.i)localObject3).<init>(parama);
    k = ((Provider)localObject3);
    localObject3 = new com/truecaller/flashsdk/ui/incoming/a$k;
    ((a.k)localObject3).<init>(parama);
    l = ((Provider)localObject3);
    localObject3 = new com/truecaller/flashsdk/ui/incoming/a$j;
    ((a.j)localObject3).<init>(parama);
    m = ((Provider)localObject3);
    localObject3 = new com/truecaller/flashsdk/ui/incoming/a$f;
    ((a.f)localObject3).<init>(parama);
    n = ((Provider)localObject3);
    localObject3 = new com/truecaller/flashsdk/ui/incoming/a$h;
    ((a.h)localObject3).<init>(parama);
    o = ((Provider)localObject3);
    localObject3 = new com/truecaller/flashsdk/ui/incoming/a$l;
    ((a.l)localObject3).<init>(parama);
    p = ((Provider)localObject3);
    localObject3 = new com/truecaller/flashsdk/ui/incoming/a$c;
    ((a.c)localObject3).<init>(parama);
    q = ((Provider)localObject3);
    localObject3 = b;
    Provider localProvider1 = c;
    Provider localProvider2 = d;
    Provider localProvider3 = e;
    Provider localProvider4 = f;
    Provider localProvider5 = g;
    Provider localProvider6 = h;
    Provider localProvider7 = j;
    Provider localProvider8 = k;
    Provider localProvider9 = l;
    Provider localProvider10 = m;
    Provider localProvider11 = n;
    Provider localProvider12 = o;
    localObject2 = p;
    localObject1 = q;
    Object localObject4 = localObject2;
    localObject2 = paramc;
    Object localObject5 = localObject1;
    localObject1 = localObject4;
    localObject4 = localObject5;
    localObject2 = dagger.a.c.a(f.a(paramc, (Provider)localObject3, localProvider1, localProvider2, localProvider3, localProvider4, localProvider5, localProvider6, localProvider7, localProvider8, localProvider9, localProvider10, localProvider11, localProvider12, (Provider)localObject1, (Provider)localObject5));
    r = ((Provider)localObject2);
    localObject2 = i;
    localObject3 = paramc;
    localObject2 = dagger.a.c.a(g.a(paramc, (Provider)localObject2));
    s = ((Provider)localObject2);
  }
  
  public static a.a a()
  {
    a.a locala = new com/truecaller/flashsdk/ui/incoming/a$a;
    locala.<init>((byte)0);
    return locala;
  }
  
  public final void a(FlashActivity paramFlashActivity)
  {
    Object localObject = (com.truecaller.flashsdk.ui.base.b)r.get();
    a = ((com.truecaller.flashsdk.ui.base.b)localObject);
    localObject = (w)dagger.a.g.a(a.a(), "Cannot return null from a non-@Nullable component method");
    b = ((w)localObject);
    localObject = (com.truecaller.featuretoggles.e)dagger.a.g.a(a.n(), "Cannot return null from a non-@Nullable component method");
    c = ((com.truecaller.featuretoggles.e)localObject);
    localObject = (com.truecaller.common.g.a)dagger.a.g.a(a.q(), "Cannot return null from a non-@Nullable component method");
    d = ((com.truecaller.common.g.a)localObject);
    localObject = (WallpaperManager)s.get();
    q = ((WallpaperManager)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.incoming.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */