package com.truecaller.flashsdk.ui.incoming;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.app.Activity;
import android.app.WallpaperManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.os.PowerManager;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintLayout.a;
import android.support.constraint.Guideline;
import android.support.transition.n;
import android.support.transition.n.c;
import android.support.transition.s;
import android.support.v4.app.o;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.Toolbar.OnMenuItemClickListener;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Property;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import c.a.ae;
import c.u;
import com.d.b.ab;
import com.d.b.ag;
import com.d.b.ai;
import com.d.b.w;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.truecaller.common.h.aq.d;
import com.truecaller.common.h.l;
import com.truecaller.flashsdk.R.dimen;
import com.truecaller.flashsdk.R.drawable;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;
import com.truecaller.flashsdk.R.menu;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.core.FlashMediaService;
import com.truecaller.flashsdk.core.FlashMediaService.a;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.ImageFlash;
import com.truecaller.flashsdk.models.QueuedFlash;
import com.truecaller.flashsdk.ui.base.BaseFlashActivity;
import com.truecaller.flashsdk.ui.c.e;
import com.truecaller.flashsdk.ui.c.e.a;
import com.truecaller.flashsdk.ui.customviews.ArrowView;
import com.truecaller.flashsdk.ui.customviews.BouncingView;
import com.truecaller.flashsdk.ui.customviews.BouncingView.b;
import com.truecaller.flashsdk.ui.customviews.FlashAttachButton;
import com.truecaller.flashsdk.ui.customviews.FlashAttachButton.a;
import com.truecaller.flashsdk.ui.customviews.FlashAttachButton.b;
import com.truecaller.flashsdk.ui.customviews.FlashAttachButton.f;
import com.truecaller.flashsdk.ui.customviews.FlashContactHeaderView;
import com.truecaller.flashsdk.ui.customviews.FlashContactHeaderView.a;
import com.truecaller.flashsdk.ui.customviews.FlashReceiveFooterView;
import com.truecaller.flashsdk.ui.customviews.FlashReceiveFooterView.a;
import com.truecaller.flashsdk.ui.customviews.a.f.a;
import com.truecaller.flashsdk.ui.customviews.a.g;
import com.truecaller.flashsdk.ui.customviews.a.i;
import com.truecaller.flashsdk.ui.customviews.b.a;
import com.truecaller.log.UnmutedException.d;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public final class FlashActivity
  extends BaseFlashActivity
  implements ActionMode.Callback, View.OnClickListener, OnMapReadyCallback, BouncingView.b, FlashAttachButton.a, FlashContactHeaderView.a, FlashReceiveFooterView.a, j
{
  public static final FlashActivity.a r;
  private final FlashActivity.d A;
  private com.truecaller.flashsdk.ui.customviews.a.f B;
  private AppCompatTextView C;
  private TextView D;
  private TextView E;
  private View F;
  private TextView G;
  private Button H;
  private Button I;
  private Button J;
  private TextView K;
  private BouncingView L;
  private ArrowView M;
  private View N;
  private ProgressBar O;
  private TextView P;
  private TextView Q;
  private ImageView R;
  private TextView S;
  private ImageView T;
  private View U;
  private FrameLayout V;
  private ConstraintLayout W;
  private ImageView X;
  private View Y;
  private MapView Z;
  private GoogleMap aa;
  private FrameLayout ab;
  private FlashAttachButton ac;
  private EditText ad;
  private ActionMode ae;
  private final FlashActivity.r af;
  private final ag ag;
  private HashMap ah;
  public WallpaperManager q;
  private final Intent s;
  private final Intent t;
  private final Intent u;
  private final Intent v;
  private final Intent w;
  private final AnimatorSet x;
  private final Runnable y;
  private final IntentFilter z;
  
  static
  {
    FlashActivity.a locala = new com/truecaller/flashsdk/ui/incoming/FlashActivity$a;
    locala.<init>((byte)0);
    r = locala;
  }
  
  public FlashActivity()
  {
    Object localObject = new android/content/Intent;
    ((Intent)localObject).<init>("type_flash_replied");
    s = ((Intent)localObject);
    localObject = new android/content/Intent;
    ((Intent)localObject).<init>("type_stop_progress");
    t = ((Intent)localObject);
    localObject = new android/content/Intent;
    ((Intent)localObject).<init>("type_flash_minimized");
    u = ((Intent)localObject);
    localObject = new android/content/Intent;
    ((Intent)localObject).<init>("type_flash_active");
    v = ((Intent)localObject);
    localObject = new android/content/Intent;
    ((Intent)localObject).<init>("type_stop_ringer");
    w = ((Intent)localObject);
    localObject = new android/animation/AnimatorSet;
    ((AnimatorSet)localObject).<init>();
    x = ((AnimatorSet)localObject);
    localObject = new com/truecaller/flashsdk/ui/incoming/FlashActivity$c;
    ((FlashActivity.c)localObject).<init>(this);
    localObject = (Runnable)localObject;
    y = ((Runnable)localObject);
    localObject = new android/content/IntentFilter;
    ((IntentFilter)localObject).<init>("type_publish_progress");
    ((IntentFilter)localObject).addAction("type_flash_timer_expired");
    ((IntentFilter)localObject).addAction("type_flash_received");
    z = ((IntentFilter)localObject);
    localObject = new com/truecaller/flashsdk/ui/incoming/FlashActivity$d;
    ((FlashActivity.d)localObject).<init>(this);
    A = ((FlashActivity.d)localObject);
    localObject = new com/truecaller/flashsdk/ui/incoming/FlashActivity$r;
    ((FlashActivity.r)localObject).<init>(this);
    af = ((FlashActivity.r)localObject);
    localObject = new com/truecaller/flashsdk/ui/incoming/FlashActivity$q;
    ((FlashActivity.q)localObject).<init>(this);
    localObject = (ag)localObject;
    ag = ((ag)localObject);
  }
  
  private final void ao()
  {
    Object localObject = X;
    if (localObject == null)
    {
      str1 = "closeReplyContact";
      c.g.b.k.a(str1);
    }
    int i = 0;
    String str1 = null;
    ((ImageView)localObject).setVisibility(0);
    localObject = Z;
    if (localObject != null) {
      ((MapView)localObject).setVisibility(0);
    }
    localObject = ab;
    if (localObject != null) {
      ((FrameLayout)localObject).setVisibility(0);
    }
    localObject = j();
    i = 8;
    ((Toolbar)localObject).setVisibility(i);
    localObject = E;
    String str2;
    if (localObject == null)
    {
      str2 = "imageTextV2";
      c.g.b.k.a(str2);
    }
    ((TextView)localObject).setVisibility(i);
    localObject = K;
    if (localObject == null)
    {
      str2 = "replyWithText";
      c.g.b.k.a(str2);
    }
    ((TextView)localObject).setVisibility(i);
  }
  
  private final boolean b(Intent paramIntent)
  {
    return android.support.v4.content.d.a((Context)this).a(paramIntent);
  }
  
  public final void A()
  {
    super.A();
    Object localObject1 = W;
    if (localObject1 == null)
    {
      localObject2 = "flashUIContainer";
      c.g.b.k.a((String)localObject2);
    }
    Object localObject2 = (View)B;
    ((ConstraintLayout)localObject1).removeView((View)localObject2);
    localObject1 = f().getMenu();
    int i = R.id.header_action_group;
    ((Menu)localObject1).removeGroup(i);
    localObject1 = (FlashReceiveFooterView)e();
    i = 0;
    localObject2 = null;
    ((FlashReceiveFooterView)localObject1).setVisibility(0);
    localObject1 = C;
    String str;
    if (localObject1 == null)
    {
      str = "flashText";
      c.g.b.k.a(str);
    }
    ((AppCompatTextView)localObject1).setVisibility(0);
    localObject1 = Y;
    if (localObject1 == null)
    {
      str = "emojiDivider";
      c.g.b.k.a(str);
    }
    ((View)localObject1).setVisibility(0);
    localObject1 = K;
    if (localObject1 == null)
    {
      localObject2 = "replyWithText";
      c.g.b.k.a((String)localObject2);
    }
    ((TextView)localObject1).setVisibility(8);
    ((FlashReceiveFooterView)e()).k();
    boolean bool = true;
    g(bool);
    h(bool);
  }
  
  public final void B()
  {
    int i = R.id.flashMapContainerV2;
    Object localObject1 = (FrameLayout)findViewById(i);
    ab = ((FrameLayout)localObject1);
    i = R.id.flashMapView;
    localObject1 = (MapView)findViewById(i);
    Z = ((MapView)localObject1);
    localObject1 = Z;
    Object localObject2;
    if (localObject1 != null)
    {
      ((MapView)localObject1).a();
      localObject2 = this;
      localObject2 = (OnMapReadyCallback)this;
      ((MapView)localObject1).a((OnMapReadyCallback)localObject2);
      ((MapView)localObject1).b();
    }
    i = R.id.flashMapContainerV2;
    localObject1 = (FrameLayout)e(i);
    if (localObject1 != null)
    {
      localObject2 = new com/truecaller/flashsdk/ui/incoming/FlashActivity$e;
      ((FlashActivity.e)localObject2).<init>(this);
      localObject2 = (View.OnClickListener)localObject2;
      ((FrameLayout)localObject1).setOnClickListener((View.OnClickListener)localObject2);
      return;
    }
  }
  
  public final void C()
  {
    ((FlashReceiveFooterView)e()).k();
  }
  
  public final void D()
  {
    Object localObject1 = ac;
    if (localObject1 == null)
    {
      localObject2 = "attachView";
      c.g.b.k.a((String)localObject2);
    }
    boolean bool = false;
    Object localObject2 = null;
    ((FlashAttachButton)localObject1).setVisibility(0);
    ((FlashReceiveFooterView)e()).m();
    localObject1 = V;
    String str1;
    if (localObject1 == null)
    {
      str1 = "imageContainerV2";
      c.g.b.k.a(str1);
    }
    int i = 8;
    ((FrameLayout)localObject1).setVisibility(i);
    localObject1 = Y;
    if (localObject1 == null)
    {
      str2 = "emojiDivider";
      c.g.b.k.a(str2);
    }
    ((View)localObject1).setVisibility(0);
    localObject1 = C;
    if (localObject1 == null)
    {
      str2 = "flashText";
      c.g.b.k.a(str2);
    }
    ((AppCompatTextView)localObject1).setVisibility(0);
    j().setVisibility(0);
    localObject1 = X;
    if (localObject1 == null)
    {
      str2 = "closeReplyContact";
      c.g.b.k.a(str2);
    }
    ((ImageView)localObject1).setVisibility(i);
    int j = R.id.buttonContainer;
    localObject1 = (LinearLayout)e(j);
    String str2 = "buttonContainer";
    c.g.b.k.a(localObject1, str2);
    ((LinearLayout)localObject1).setVisibility(0);
    g().setVisibility(i);
    localObject1 = E;
    if (localObject1 == null)
    {
      localObject2 = "imageTextV2";
      c.g.b.k.a((String)localObject2);
    }
    ((TextView)localObject1).setVisibility(i);
    ((FlashReceiveFooterView)e()).h();
    j = R.id.flashMapContainerV2;
    localObject1 = (FrameLayout)e(j);
    if (localObject1 != null) {
      ((FrameLayout)localObject1).setVisibility(i);
    }
    localObject1 = m;
    if (localObject1 != null) {
      ((MapView)localObject1).c();
    }
    localObject1 = m;
    if (localObject1 != null) {
      ((MapView)localObject1).d();
    }
    localObject1 = (FlashReceiveFooterView)e();
    localObject2 = ad;
    if (localObject2 == null)
    {
      str1 = "editMessageText";
      c.g.b.k.a(str1);
    }
    bool = TextUtils.isEmpty((CharSequence)((EditText)localObject2).getText()) ^ true;
    ((FlashReceiveFooterView)localObject1).c(bool);
  }
  
  public final void E()
  {
    FlashReceiveFooterView localFlashReceiveFooterView = (FlashReceiveFooterView)e();
    int i = R.string.tip_reply_with_location;
    localFlashReceiveFooterView.c(i);
  }
  
  public final void F()
  {
    ((FlashReceiveFooterView)e()).o();
  }
  
  public final void G()
  {
    ((FlashReceiveFooterView)e()).n();
  }
  
  public final void H() {}
  
  public final void I()
  {
    ((FlashReceiveFooterView)e()).e();
  }
  
  public final void J()
  {
    ((FlashReceiveFooterView)e()).i();
  }
  
  public final void J_()
  {
    c();
  }
  
  public final void K()
  {
    ((FlashReceiveFooterView)e()).e();
  }
  
  public final void K_()
  {
    ((h)c()).q();
  }
  
  public final void L()
  {
    ((FlashReceiveFooterView)e()).j();
  }
  
  public final void L_()
  {
    ((h)c()).f();
  }
  
  public final void M()
  {
    ((FlashReceiveFooterView)e()).i();
  }
  
  public final void N()
  {
    ActionMode localActionMode = ae;
    if (localActionMode != null)
    {
      localActionMode.finish();
      return;
    }
  }
  
  public final void O()
  {
    h localh = (h)c();
    String str = ((FlashReceiveFooterView)e()).getMessageText();
    com.truecaller.flashsdk.a.a locala = k;
    if (locala != null)
    {
      boolean bool1 = locala.isShowing();
      Object localObject = k;
      if (localObject != null)
      {
        localObject = ((com.truecaller.flashsdk.a.a)localObject).c();
        if (localObject != null)
        {
          boolean bool2 = ((Boolean)localObject).booleanValue();
          localh.a(str, bool1, bool2);
          return;
        }
      }
      return;
    }
  }
  
  public final void P()
  {
    int i = R.id.receiveImageTextV2;
    Object localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.receiveImageTextV2)");
    localObject1 = (TextView)localObject1;
    E = ((TextView)localObject1);
    i = R.id.receiveImageTextV2Container;
    localObject1 = findViewById(i);
    F = ((View)localObject1);
    i = R.id.flashImageContainerV2;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.flashImageContainerV2)");
    localObject1 = (FrameLayout)localObject1;
    V = ((FrameLayout)localObject1);
    i = R.id.closeButtonContact;
    localObject1 = findViewById(i);
    Object localObject2 = "findViewById(R.id.closeButtonContact)";
    c.g.b.k.a(localObject1, (String)localObject2);
    localObject1 = (ImageView)localObject1;
    X = ((ImageView)localObject1);
    localObject1 = X;
    if (localObject1 == null)
    {
      localObject2 = "closeReplyContact";
      c.g.b.k.a((String)localObject2);
    }
    int j = -1;
    PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
    ((ImageView)localObject1).setColorFilter(j, localMode);
    i = R.id.imageContentV2;
    localObject1 = (ImageView)e(i);
    localObject2 = new com/truecaller/flashsdk/ui/incoming/FlashActivity$f;
    ((FlashActivity.f)localObject2).<init>(this);
    localObject2 = (View.OnClickListener)localObject2;
    ((ImageView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = X;
    if (localObject1 == null)
    {
      localObject2 = "closeReplyContact";
      c.g.b.k.a((String)localObject2);
    }
    localObject2 = new com/truecaller/flashsdk/ui/incoming/FlashActivity$g;
    ((FlashActivity.g)localObject2).<init>(this);
    localObject2 = (View.OnClickListener)localObject2;
    ((ImageView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = E;
    if (localObject1 == null)
    {
      localObject2 = "imageTextV2";
      c.g.b.k.a((String)localObject2);
    }
    localObject2 = this;
    localObject2 = (ActionMode.Callback)this;
    ((TextView)localObject1).setCustomSelectionActionModeCallback((ActionMode.Callback)localObject2);
  }
  
  public final void Q()
  {
    Object localObject1 = J;
    if (localObject1 == null)
    {
      localObject2 = "btnNo";
      c.g.b.k.a((String)localObject2);
    }
    Object localObject2 = this;
    localObject2 = (Context)this;
    int i = R.drawable.bg_solid_white_rad_24dp;
    Object localObject3 = android.support.v4.content.b.a((Context)localObject2, i);
    ((Button)localObject1).setBackground((Drawable)localObject3);
    localObject1 = H;
    if (localObject1 == null)
    {
      localObject3 = "btnYes";
      c.g.b.k.a((String)localObject3);
    }
    i = R.drawable.bg_solid_white_rad_24dp;
    localObject3 = android.support.v4.content.b.a((Context)localObject2, i);
    ((Button)localObject1).setBackground((Drawable)localObject3);
    localObject1 = I;
    if (localObject1 == null)
    {
      localObject3 = "btnOk";
      c.g.b.k.a((String)localObject3);
    }
    i = R.drawable.bg_solid_white_rad_24dp;
    localObject2 = android.support.v4.content.b.a((Context)localObject2, i);
    ((Button)localObject1).setBackground((Drawable)localObject2);
    localObject1 = X;
    if (localObject1 == null)
    {
      localObject2 = "closeReplyContact";
      c.g.b.k.a((String)localObject2);
    }
    int j = 0;
    localObject2 = null;
    ((ImageView)localObject1).setVisibility(0);
    localObject1 = Z;
    if (localObject1 != null) {
      ((MapView)localObject1).setVisibility(0);
    }
    localObject1 = ab;
    if (localObject1 != null) {
      ((FrameLayout)localObject1).setVisibility(0);
    }
    int k = R.id.buttonContainer;
    localObject1 = (LinearLayout)e(k);
    localObject3 = "buttonContainer";
    c.g.b.k.a(localObject1, (String)localObject3);
    ((LinearLayout)localObject1).setVisibility(0);
    ((FlashReceiveFooterView)e()).p();
    localObject1 = j();
    j = 8;
    ((Toolbar)localObject1).setVisibility(j);
    localObject1 = K;
    if (localObject1 == null)
    {
      localObject3 = "replyWithText";
      c.g.b.k.a((String)localObject3);
    }
    ((TextView)localObject1).setVisibility(j);
  }
  
  public final void R()
  {
    Object localObject = ab;
    int i = 8;
    if (localObject != null) {
      ((FrameLayout)localObject).setVisibility(i);
    }
    localObject = V;
    String str;
    if (localObject == null)
    {
      str = "imageContainerV2";
      c.g.b.k.a(str);
    }
    ((FrameLayout)localObject).setVisibility(i);
    localObject = E;
    if (localObject == null)
    {
      str = "imageTextV2";
      c.g.b.k.a(str);
    }
    ((TextView)localObject).setVisibility(i);
  }
  
  public final void S()
  {
    Object localObject1 = J;
    if (localObject1 == null)
    {
      localObject2 = "btnNo";
      c.g.b.k.a((String)localObject2);
    }
    Object localObject2 = this;
    localObject2 = (Context)this;
    int i = R.drawable.bg_solid_white_rad_24dp;
    Object localObject3 = android.support.v4.content.b.a((Context)localObject2, i);
    ((Button)localObject1).setBackground((Drawable)localObject3);
    localObject1 = H;
    if (localObject1 == null)
    {
      localObject3 = "btnYes";
      c.g.b.k.a((String)localObject3);
    }
    i = R.drawable.bg_solid_white_rad_24dp;
    localObject3 = android.support.v4.content.b.a((Context)localObject2, i);
    ((Button)localObject1).setBackground((Drawable)localObject3);
    localObject1 = I;
    if (localObject1 == null)
    {
      localObject3 = "btnOk";
      c.g.b.k.a((String)localObject3);
    }
    i = R.drawable.bg_solid_white_rad_24dp;
    localObject2 = android.support.v4.content.b.a((Context)localObject2, i);
    ((Button)localObject1).setBackground((Drawable)localObject2);
    localObject1 = X;
    if (localObject1 == null)
    {
      localObject2 = "closeReplyContact";
      c.g.b.k.a((String)localObject2);
    }
    int j = 0;
    localObject2 = null;
    ((ImageView)localObject1).setVisibility(0);
    localObject1 = F;
    if (localObject1 != null) {
      ((View)localObject1).setVisibility(0);
    }
    localObject1 = V;
    if (localObject1 == null)
    {
      localObject3 = "imageContainerV2";
      c.g.b.k.a((String)localObject3);
    }
    ((FrameLayout)localObject1).setVisibility(0);
    int k = R.id.buttonContainer;
    localObject1 = (LinearLayout)e(k);
    localObject3 = "buttonContainer";
    c.g.b.k.a(localObject1, (String)localObject3);
    ((LinearLayout)localObject1).setVisibility(0);
    ((FlashReceiveFooterView)e()).p();
    localObject1 = j();
    j = 8;
    ((Toolbar)localObject1).setVisibility(j);
    localObject1 = K;
    if (localObject1 == null)
    {
      localObject3 = "replyWithText";
      c.g.b.k.a((String)localObject3);
    }
    ((TextView)localObject1).setVisibility(j);
    localObject1 = ab;
    if (localObject1 != null)
    {
      ((FrameLayout)localObject1).setVisibility(j);
      return;
    }
  }
  
  public final void T()
  {
    int i = R.id.flashAttachButton;
    Object localObject1 = findViewById(i);
    Object localObject2 = "findViewById(R.id.flashAttachButton)";
    c.g.b.k.a(localObject1, (String)localObject2);
    localObject1 = (FlashAttachButton)localObject1;
    ac = ((FlashAttachButton)localObject1);
    ((FlashReceiveFooterView)e()).p();
    localObject1 = ac;
    if (localObject1 == null)
    {
      localObject2 = "attachView";
      c.g.b.k.a((String)localObject2);
    }
    localObject2 = LayoutInflater.from(((FlashAttachButton)localObject1).getContext());
    int j = R.layout.flash_attach_viewv2;
    Object localObject3 = localObject1;
    localObject3 = (ViewGroup)localObject1;
    ((LayoutInflater)localObject2).inflate(j, (ViewGroup)localObject3);
    localObject2 = null;
    ((FlashAttachButton)localObject1).setClipChildren(false);
    j = R.id.fab_icon;
    Object localObject4 = (ImageView)((FlashAttachButton)localObject1).findViewById(j);
    d = ((ImageView)localObject4);
    j = R.id.fab_menu;
    localObject4 = (LinearLayout)((FlashAttachButton)localObject1).findViewById(j);
    e = ((LinearLayout)localObject4);
    j = R.id.fab_backdrop;
    localObject4 = ((FlashAttachButton)localObject1).findViewById(j);
    f = ((View)localObject4);
    localObject4 = f;
    if (localObject4 != null)
    {
      localObject3 = new com/truecaller/flashsdk/ui/customviews/FlashAttachButton$f;
      ((FlashAttachButton.f)localObject3).<init>((FlashAttachButton)localObject1);
      localObject3 = (View.OnClickListener)localObject3;
      ((View)localObject4).setOnClickListener((View.OnClickListener)localObject3);
    }
    int k = com.truecaller.common.h.k.d();
    int n;
    if (k != 0)
    {
      localObject4 = d;
      if (localObject4 != null)
      {
        localObject3 = ((FlashAttachButton)localObject1).getContext();
        float f1 = 6.0F;
        n = l.a((Context)localObject3, f1);
        f2 = n;
        ((ImageView)localObject4).setElevation(f2);
      }
    }
    else
    {
      localObject4 = new com/truecaller/flashsdk/ui/customviews/FlashAttachButton$b;
      localObject3 = ((FlashAttachButton)localObject1).getContext();
      Object localObject5 = "context";
      c.g.b.k.a(localObject3, (String)localObject5);
      ((FlashAttachButton.b)localObject4).<init>((Context)localObject3);
      c = ((FlashAttachButton.b)localObject4);
      k = 0;
      localObject4 = null;
      for (;;)
      {
        n = ((FlashAttachButton)localObject1).getChildCount();
        if (k >= n) {
          break;
        }
        localObject3 = ((FlashAttachButton)localObject1).getChildAt(k);
        localObject5 = d;
        boolean bool = c.g.b.k.a(localObject3, localObject5);
        if (bool) {
          break;
        }
        k += 1;
      }
      localObject3 = (View)c;
      localObject5 = new android/widget/FrameLayout$LayoutParams;
      ((FrameLayout.LayoutParams)localObject5).<init>(0, 0);
      localObject5 = (ViewGroup.LayoutParams)localObject5;
      ((FlashAttachButton)localObject1).addView((View)localObject3, m, (ViewGroup.LayoutParams)localObject5);
    }
    localObject4 = ((FlashAttachButton)localObject1).getResources();
    localObject3 = "resources";
    c.g.b.k.a(localObject4, (String)localObject3);
    localObject4 = ((Resources)localObject4).getConfiguration();
    int m = orientation;
    int i1 = 2;
    float f2 = 2.8E-45F;
    if (m == i1)
    {
      m = 1;
    }
    else
    {
      m = 0;
      localObject4 = null;
    }
    g = m;
    localObject4 = d;
    if (localObject4 != null)
    {
      localObject3 = localObject1;
      localObject3 = (View.OnClickListener)localObject1;
      ((ImageView)localObject4).setOnClickListener((View.OnClickListener)localObject3);
    }
    ((FlashAttachButton)localObject1).setVisibility(0);
    localObject2 = this;
    localObject2 = (FlashAttachButton.a)this;
    ((FlashAttachButton)localObject1).setFabActionListener((FlashAttachButton.a)localObject2);
    ((h)c()).s();
  }
  
  public final void U()
  {
    Object localObject = ac;
    if (localObject == null)
    {
      String str1 = "attachView";
      c.g.b.k.a(str1);
    }
    int i = 8;
    ((FlashAttachButton)localObject).setVisibility(i);
    ((FlashReceiveFooterView)e()).setVisibility(i);
    localObject = V;
    if (localObject == null)
    {
      String str2 = "imageContainerV2";
      c.g.b.k.a(str2);
    }
    ((FrameLayout)localObject).setForeground(null);
    f().setVisibility(i);
    int j = R.id.buttonContainer;
    localObject = (LinearLayout)e(j);
    c.g.b.k.a(localObject, "buttonContainer");
    ((LinearLayout)localObject).setVisibility(i);
  }
  
  public final void V()
  {
    f().setVisibility(0);
    Object localObject1 = V;
    if (localObject1 == null)
    {
      localObject2 = "imageContainerV2";
      c.g.b.k.a((String)localObject2);
    }
    Object localObject2 = this;
    localObject2 = (Context)this;
    int i = R.drawable.flash_gradient_image_bg;
    localObject2 = android.support.v4.content.b.a((Context)localObject2, i);
    ((FrameLayout)localObject1).setForeground((Drawable)localObject2);
    ((FlashReceiveFooterView)e()).setVisibility(0);
    int j = R.id.buttonContainer;
    localObject1 = (LinearLayout)e(j);
    c.g.b.k.a(localObject1, "buttonContainer");
    ((LinearLayout)localObject1).setVisibility(0);
  }
  
  public final void W()
  {
    Object localObject1 = T;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject2 = "overlayBackgroundImage";
      c.g.b.k.a((String)localObject2);
    }
    float f = k.c();
    ((ImageView)localObject1).setAlpha(f);
    try
    {
      localObject1 = T;
      if (localObject1 == null)
      {
        localObject2 = "overlayBackgroundImage";
        c.g.b.k.a((String)localObject2);
      }
      localObject2 = q;
      if (localObject2 == null)
      {
        String str = "wallpaperManager";
        c.g.b.k.a(str);
      }
      localObject2 = ((WallpaperManager)localObject2).getDrawable();
      ((ImageView)localObject1).setImageDrawable((Drawable)localObject2);
      return;
    }
    catch (Exception localException)
    {
      localObject1 = new com/truecaller/log/UnmutedException$d;
      ((UnmutedException.d)localObject1).<init>("exception setting flash wallpaper");
      com.truecaller.log.d.a((Throwable)localObject1);
    }
  }
  
  public final void X()
  {
    Object localObject = ac;
    if (localObject == null)
    {
      String str = "attachView";
      c.g.b.k.a(str);
    }
    localObject = d;
    if (localObject != null)
    {
      ((ImageView)localObject).setBackground(null);
      return;
    }
  }
  
  public final void Y()
  {
    ArrowView localArrowView = M;
    if (localArrowView == null)
    {
      String str = "arrowView";
      c.g.b.k.a(str);
    }
    localArrowView.b();
  }
  
  public final void Z()
  {
    Handler localHandler = e;
    Runnable localRunnable = y;
    localHandler.removeCallbacks(localRunnable);
  }
  
  public final void a(int paramInt)
  {
    ((h)c()).b(paramInt);
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    FlashContactHeaderView localFlashContactHeaderView = f();
    Drawable localDrawable = com.truecaller.utils.ui.b.c((Context)this, paramInt1);
    localFlashContactHeaderView.setBackground(localDrawable);
    f().setHeaderTextColor(paramInt2);
  }
  
  public final void a(Intent paramIntent)
  {
    c.g.b.k.b(paramIntent, "intent");
    startActivity(paramIntent);
  }
  
  public final void a(Drawable paramDrawable)
  {
    c.g.b.k.b(paramDrawable, "drawable");
    Button localButton = J;
    String str;
    if (localButton == null)
    {
      str = "btnNo";
      c.g.b.k.a(str);
    }
    localButton.setBackground(paramDrawable);
    localButton = H;
    if (localButton == null)
    {
      str = "btnYes";
      c.g.b.k.a(str);
    }
    localButton.setBackground(paramDrawable);
    localButton = I;
    if (localButton == null)
    {
      str = "btnOk";
      c.g.b.k.a(str);
    }
    localButton.setBackground(paramDrawable);
  }
  
  public final void a(Uri paramUri, String paramString)
  {
    c.g.b.k.b(paramUri, "contentUriFromBitmap");
    c.g.b.k.b(paramString, "imageDescription");
    com.truecaller.flashsdk.ui.b.a locala = new com/truecaller/flashsdk/ui/b/a;
    locala.<init>();
    Object localObject = getIntent();
    c.g.b.k.a(localObject, "intent");
    localObject = ((Intent)localObject).getExtras();
    paramUri = paramUri.toString();
    ((Bundle)localObject).putString("share_image", paramUri);
    ((Bundle)localObject).putString("share_text", paramString);
    locala.setArguments((Bundle)localObject);
    paramUri = getSupportFragmentManager();
    paramString = locala.getTag();
    locala.show(paramUri, paramString);
  }
  
  public final void a(GoogleMap paramGoogleMap)
  {
    aa = paramGoogleMap;
    ((h)c()).i();
    paramGoogleMap = aa;
    if (paramGoogleMap != null)
    {
      Object localObject = new com/truecaller/flashsdk/ui/incoming/FlashActivity$i;
      ((FlashActivity.i)localObject).<init>(this);
      localObject = (GoogleMap.OnMapClickListener)localObject;
      paramGoogleMap.a((GoogleMap.OnMapClickListener)localObject);
      return;
    }
  }
  
  public final void a(Flash paramFlash)
  {
    c.g.b.k.b(paramFlash, "flash");
    Intent localIntent = t;
    paramFlash = (Parcelable)paramFlash;
    localIntent.putExtra("extra_flash", paramFlash);
    paramFlash = t;
    b(paramFlash);
  }
  
  public final void a(ImageFlash paramImageFlash)
  {
    c.g.b.k.b(paramImageFlash, "imageFlash");
    paramImageFlash = FlashMediaService.a.a((Context)this, paramImageFlash);
    startService(paramImageFlash);
  }
  
  public final void a(QueuedFlash paramQueuedFlash)
  {
    c.g.b.k.b(paramQueuedFlash, "flash");
    Intent localIntent = v;
    paramQueuedFlash = (Parcelable)paramQueuedFlash;
    localIntent.putExtra("extra_flash", paramQueuedFlash);
    paramQueuedFlash = v;
    b(paramQueuedFlash);
  }
  
  public final void a(com.truecaller.flashsdk.ui.customviews.a.a parama)
  {
    c.g.b.k.b(parama, "displayableEmojiAttributes");
    Object localObject1 = new com/truecaller/flashsdk/ui/customviews/a/b;
    Object localObject2 = this;
    localObject2 = (Context)this;
    ((com.truecaller.flashsdk.ui.customviews.a.b)localObject1).<init>((Context)localObject2);
    localObject1 = (com.truecaller.flashsdk.ui.customviews.a.f)localObject1;
    B = ((com.truecaller.flashsdk.ui.customviews.a.f)localObject1);
    localObject1 = B;
    if (localObject1 != null)
    {
      parama = (f.a)parama;
      ((com.truecaller.flashsdk.ui.customviews.a.f)localObject1).setEmojiAttributes$flash_release(parama);
    }
    parama = W;
    if (parama == null)
    {
      localObject1 = "flashUIContainer";
      c.g.b.k.a((String)localObject1);
    }
    localObject1 = (View)B;
    localObject2 = new android/support/constraint/ConstraintLayout$a;
    int i = -1;
    ((ConstraintLayout.a)localObject2).<init>(i, i);
    localObject2 = (ViewGroup.LayoutParams)localObject2;
    parama.addView((View)localObject1, (ViewGroup.LayoutParams)localObject2);
  }
  
  public final void a(com.truecaller.flashsdk.ui.customviews.a.h paramh)
  {
    c.g.b.k.b(paramh, "displayableEmojiAttributes");
    Object localObject1 = new com/truecaller/flashsdk/ui/customviews/a/i;
    Object localObject2 = this;
    localObject2 = (Context)this;
    ((i)localObject1).<init>((Context)localObject2);
    localObject1 = (com.truecaller.flashsdk.ui.customviews.a.f)localObject1;
    B = ((com.truecaller.flashsdk.ui.customviews.a.f)localObject1);
    localObject1 = B;
    if (localObject1 != null)
    {
      paramh = (f.a)paramh;
      ((com.truecaller.flashsdk.ui.customviews.a.f)localObject1).setEmojiAttributes$flash_release(paramh);
    }
    paramh = W;
    if (paramh == null)
    {
      localObject1 = "flashUIContainer";
      c.g.b.k.a((String)localObject1);
    }
    localObject1 = (View)B;
    localObject2 = new android/support/constraint/ConstraintLayout$a;
    int i = -1;
    ((ConstraintLayout.a)localObject2).<init>(i, i);
    localObject2 = (ViewGroup.LayoutParams)localObject2;
    paramh.addView((View)localObject1, (ViewGroup.LayoutParams)localObject2);
  }
  
  public final void a(CharSequence paramCharSequence)
  {
    ((h)c()).a(paramCharSequence);
  }
  
  public final void a(CharSequence paramCharSequence, boolean paramBoolean)
  {
    c.g.b.k.b(paramCharSequence, "text");
    ((h)c()).a(paramCharSequence, paramBoolean);
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "contactImageUrl");
    super.a(paramString);
    paramString = d().a(paramString);
    Object localObject = (ai)aq.d.b();
    paramString = paramString.a((ai)localObject);
    int i = R.drawable.ic_empty_avatar;
    paramString = paramString.a(i);
    localObject = R;
    if (localObject == null)
    {
      String str = "overlayImage";
      c.g.b.k.a(str);
    }
    paramString.a((ImageView)localObject, null);
  }
  
  public final void a(String paramString, int paramInt1, int paramInt2, int paramInt3)
  {
    c.g.b.k.b(paramString, "text");
    FlashReceiveFooterView localFlashReceiveFooterView = (FlashReceiveFooterView)e();
    c.g.b.k.b(paramString, "emoji");
    Object localObject = q;
    if (localObject == null)
    {
      String str1 = "editMessageText";
      c.g.b.k.a(str1);
    }
    localObject = ((EditText)localObject).getText();
    paramString = (CharSequence)paramString;
    ((Editable)localObject).replace(paramInt1, paramInt2, paramString);
    paramString = q;
    if (paramString == null)
    {
      String str2 = "editMessageText";
      c.g.b.k.a(str2);
    }
    paramString.setSelection(paramInt3);
  }
  
  public final void a(String paramString1, long paramLong, String paramString2)
  {
    c.g.b.k.b(paramString1, "history");
    c.g.b.k.b(paramString2, "name");
    Object localObject1 = W;
    if (localObject1 == null)
    {
      localObject2 = "flashUIContainer";
      c.g.b.k.a((String)localObject2);
    }
    Object localObject2 = (View)B;
    ((ConstraintLayout)localObject1).removeView((View)localObject2);
    com.truecaller.utils.extensions.t.a((View)e(), false, 2);
    ((FlashReceiveFooterView)e()).d();
    localObject1 = f().getMenu();
    int i = R.id.header_action_group;
    ((Menu)localObject1).setGroupEnabled(i, false);
    localObject1 = f().getMenu();
    i = R.id.header_action_group;
    ((Menu)localObject1).setGroupVisible(i, false);
    localObject1 = k;
    if (localObject1 != null) {
      ((com.truecaller.flashsdk.a.a)localObject1).dismiss();
    }
    localObject1 = (FlashReceiveFooterView)e();
    i = 8;
    ((FlashReceiveFooterView)localObject1).setVisibility(i);
    g().setVisibility(i);
    i().setVisibility(i);
    localObject1 = C;
    if (localObject1 == null)
    {
      localObject3 = "flashText";
      c.g.b.k.a((String)localObject3);
    }
    ((AppCompatTextView)localObject1).setVisibility(i);
    g(false);
    h(false);
    localObject1 = getSupportFragmentManager().a();
    i = R.id.waiting_container;
    Object localObject3 = e.d;
    long l = com.truecaller.flashsdk.ui.c.f.b();
    paramString1 = e.a.a(paramString1, l, paramString2, false, paramLong);
    ((o)localObject1).b(i, paramString1).a(4097).d();
    int j = R.id.waiting_container;
    paramString1 = findViewById(j);
    c.g.b.k.a(paramString1, "view");
    paramString1.setVisibility(0);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "firstLine");
    c.g.b.k.b(paramString2, "boldText");
    super.a(paramString1, paramString2);
    Object localObject = P;
    if (localObject == null)
    {
      String str = "overlayName";
      c.g.b.k.a(str);
    }
    paramString2 = (CharSequence)paramString2;
    ((TextView)localObject).setText(paramString2);
    paramString2 = Q;
    if (paramString2 == null)
    {
      localObject = "overlayCaller";
      c.g.b.k.a((String)localObject);
    }
    paramString1 = (CharSequence)paramString1;
    paramString2.setText(paramString1);
  }
  
  public final void a(String paramString1, String paramString2, String paramString3)
  {
    c.g.b.k.b(paramString1, "location");
    c.g.b.k.b(paramString2, "lat");
    c.g.b.k.b(paramString3, "long");
    Object localObject1 = new com/truecaller/flashsdk/ui/incoming/FlashActivity$b;
    Object localObject2 = this;
    localObject2 = (Context)this;
    ((FlashActivity.b)localObject1).<init>((Context)localObject2, paramString1);
    paramString1 = aa;
    if (paramString1 != null)
    {
      Object localObject3 = J;
      if (localObject3 == null)
      {
        localObject4 = "btnNo";
        c.g.b.k.a((String)localObject4);
      }
      int i = R.drawable.bg_solid_white_rad_24dp;
      Object localObject4 = android.support.v4.content.b.a((Context)localObject2, i);
      ((Button)localObject3).setBackground((Drawable)localObject4);
      localObject3 = H;
      if (localObject3 == null)
      {
        localObject4 = "btnYes";
        c.g.b.k.a((String)localObject4);
      }
      i = R.drawable.bg_solid_white_rad_24dp;
      localObject4 = android.support.v4.content.b.a((Context)localObject2, i);
      ((Button)localObject3).setBackground((Drawable)localObject4);
      localObject3 = I;
      if (localObject3 == null)
      {
        localObject4 = "btnOk";
        c.g.b.k.a((String)localObject4);
      }
      i = R.drawable.bg_solid_white_rad_24dp;
      localObject4 = android.support.v4.content.b.a((Context)localObject2, i);
      ((Button)localObject3).setBackground((Drawable)localObject4);
      localObject3 = (FlashReceiveFooterView)e();
      i = R.drawable.flash_round_button_default_v2;
      localObject2 = android.support.v4.content.b.a((Context)localObject2, i);
      ((FlashReceiveFooterView)localObject3).setBackground((Drawable)localObject2);
      ao();
      localObject2 = Y;
      if (localObject2 == null)
      {
        localObject3 = "emojiDivider";
        c.g.b.k.a((String)localObject3);
      }
      int j = 8;
      ((View)localObject2).setVisibility(j);
      int k = R.id.buttonContainer;
      localObject2 = (LinearLayout)e(k);
      localObject4 = "buttonContainer";
      c.g.b.k.a(localObject2, (String)localObject4);
      ((LinearLayout)localObject2).setVisibility(j);
      localObject2 = ac;
      if (localObject2 == null)
      {
        localObject4 = "attachView";
        c.g.b.k.a((String)localObject4);
      }
      ((FlashAttachButton)localObject2).setVisibility(j);
      localObject1 = (GoogleMap.InfoWindowAdapter)localObject1;
      paramString1.a((GoogleMap.InfoWindowAdapter)localObject1);
      double d1 = Double.parseDouble(paramString2);
      double d2 = Double.parseDouble(paramString3);
      com.truecaller.flashsdk.assist.p.a(paramString1, d1, d2);
      ((FlashReceiveFooterView)e()).l();
      ((FlashReceiveFooterView)e()).q();
      ((FlashReceiveFooterView)e()).o();
      ((FlashReceiveFooterView)e()).n();
      ((FlashReceiveFooterView)e()).c(true);
      return;
    }
  }
  
  public final void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    c.g.b.k.b(paramString1, "placeName");
    c.g.b.k.b(paramString2, "locationMessage");
    c.g.b.k.b(paramString3, "lat");
    c.g.b.k.b(paramString4, "long");
    FlashActivity.b localb = new com/truecaller/flashsdk/ui/incoming/FlashActivity$b;
    Object localObject = this;
    localObject = (Context)this;
    localb.<init>((Context)localObject, paramString1);
    paramString1 = aa;
    if (paramString1 != null)
    {
      ao();
      int i = R.id.buttonContainer;
      localObject = (LinearLayout)e(i);
      c.g.b.k.a(localObject, "buttonContainer");
      ((LinearLayout)localObject).setVisibility(0);
      ((FlashReceiveFooterView)e()).p();
      localObject = localb;
      localObject = (GoogleMap.InfoWindowAdapter)localb;
      paramString1.a((GoogleMap.InfoWindowAdapter)localObject);
      localObject = new com/truecaller/flashsdk/ui/incoming/FlashActivity$p;
      ((FlashActivity.p)localObject).<init>(this, localb, paramString3, paramString4, paramString2);
      localObject = (GoogleMap.OnInfoWindowClickListener)localObject;
      paramString1.a((GoogleMap.OnInfoWindowClickListener)localObject);
      double d1 = Double.parseDouble(paramString3);
      double d2 = Double.parseDouble(paramString4);
      com.truecaller.flashsdk.assist.p.a(paramString1, d1, d2);
      paramString1 = F;
      if (paramString1 != null) {
        paramString1.setVisibility(0);
      }
      paramString1 = E;
      if (paramString1 == null)
      {
        paramString3 = "imageTextV2";
        c.g.b.k.a(paramString3);
      }
      paramString1.setVisibility(0);
      paramString1 = E;
      if (paramString1 == null)
      {
        paramString3 = "imageTextV2";
        c.g.b.k.a(paramString3);
      }
      paramString2 = (CharSequence)paramString2;
      paramString1.setText(paramString2);
      return;
    }
  }
  
  public final void a(String paramString, boolean paramBoolean)
  {
    c.g.b.k.b(paramString, "message");
    Object localObject1 = (FlashReceiveFooterView)e();
    String str = "message";
    c.g.b.k.b(paramString, str);
    localObject1 = q;
    if (localObject1 == null)
    {
      str = "editMessageText";
      c.g.b.k.a(str);
    }
    ((EditText)localObject1).setEnabled(paramBoolean);
    Object localObject2 = paramString;
    localObject2 = (CharSequence)paramString;
    ((EditText)localObject1).setText((CharSequence)localObject2);
    int i = paramString.length();
    ((EditText)localObject1).setSelection(i);
  }
  
  public final void a(List paramList)
  {
    Object localObject1 = "responses";
    c.g.b.k.b(paramList, (String)localObject1);
    int i = paramList.size();
    long l1 = i;
    long l2 = 3;
    boolean bool = l1 < l2;
    if (bool) {
      return;
    }
    localObject1 = H;
    if (localObject1 == null)
    {
      localObject2 = "btnYes";
      c.g.b.k.a((String)localObject2);
    }
    int j = 0;
    Object localObject2 = (CharSequence)paramList.get(0);
    ((Button)localObject1).setText((CharSequence)localObject2);
    localObject1 = I;
    if (localObject1 == null)
    {
      localObject2 = "btnOk";
      c.g.b.k.a((String)localObject2);
    }
    j = 1;
    localObject2 = (CharSequence)paramList.get(j);
    ((Button)localObject1).setText((CharSequence)localObject2);
    localObject1 = J;
    if (localObject1 == null)
    {
      localObject2 = "btnNo";
      c.g.b.k.a((String)localObject2);
    }
    paramList = (CharSequence)paramList.get(2);
    ((Button)localObject1).setText(paramList);
  }
  
  public final void aa()
  {
    AnimatorSet localAnimatorSet = x;
    boolean bool = localAnimatorSet.isRunning();
    if (bool)
    {
      localAnimatorSet = x;
      localAnimatorSet.end();
    }
  }
  
  public final void ab()
  {
    ArrowView localArrowView = M;
    if (localArrowView == null)
    {
      String str = "arrowView";
      c.g.b.k.a(str);
    }
    localArrowView.a();
  }
  
  public final void ac()
  {
    getWindow().clearFlags(128);
  }
  
  public final void ad()
  {
    Object localObject1 = B;
    if (localObject1 != null)
    {
      Object localObject2 = ((com.truecaller.flashsdk.ui.customviews.a.f)localObject1).getContext();
      if (localObject2 != null)
      {
        localObject2 = ((Activity)localObject2).getWindow();
        c.g.b.k.a(localObject2, "(context as Activity).window");
        localObject2 = ((Window)localObject2).getDecorView();
        c.g.b.k.a(localObject2, "(context as Activity).window.decorView");
        localObject2 = ((View)localObject2).getRootView();
        c.g.b.k.a(localObject2, "(context as Activity).window.decorView.rootView");
        int i = ((View)localObject2).getHeight() * 4;
        Object localObject3 = localObject1;
        localObject3 = (ViewGroup)localObject1;
        Object localObject4 = new android/support/transition/s;
        ((s)localObject4).<init>();
        Object localObject5 = new android/support/transition/d;
        ((android.support.transition.d)localObject5).<init>();
        long l = i;
        localObject2 = ((android.support.transition.d)localObject5).a(l);
        localObject5 = new android/support/transition/b;
        ((android.support.transition.b)localObject5).<init>();
        ((android.support.transition.b)localObject5).a();
        ((android.support.transition.b)localObject5).b();
        localObject2 = ((s)localObject4).a((n)localObject2);
        localObject4 = new com/truecaller/flashsdk/ui/customviews/a/g;
        ((g)localObject4).<init>();
        localObject4 = (n)localObject4;
        localObject2 = (n)((s)localObject2).a((n)localObject4);
        android.support.transition.p.a((ViewGroup)localObject3, (n)localObject2);
        localObject2 = l;
        if (localObject2 == null) {
          return;
        }
        localObject3 = new c/k/h;
        int j = ((com.truecaller.flashsdk.ui.customviews.a.f)localObject1).getChildCount();
        int m = 1;
        ((c.k.h)localObject3).<init>(m, j);
        localObject3 = ((Iterable)localObject3).iterator();
        for (;;)
        {
          boolean bool1 = ((Iterator)localObject3).hasNext();
          if (!bool1) {
            break label514;
          }
          localObject4 = localObject3;
          localObject4 = (ae)localObject3;
          int k = ((ae)localObject4).a();
          m = k + -1;
          localObject5 = ((com.truecaller.flashsdk.ui.customviews.a.f)localObject1).getChildAt(m);
          boolean bool3 = localObject5 instanceof TextView;
          float f2;
          if (bool3)
          {
            localObject5 = (TextView)localObject5;
            Object localObject6 = ((TextView)localObject5).getLayoutParams();
            if (localObject6 != null)
            {
              localObject6 = (ConstraintLayout.a)localObject6;
              Random localRandom = k;
              float f1 = localRandom.nextFloat();
              z = f1;
              ((com.truecaller.flashsdk.ui.customviews.a.f)localObject1).b((ConstraintLayout.a)localObject6);
              f2 = ((TextView)localObject5).getWidth();
              f1 = 2.0F;
              f2 /= f1;
              ((TextView)localObject5).setPivotX(f2);
              int n = ((TextView)localObject5).getHeight();
              f2 = n / f1;
              ((TextView)localObject5).setPivotY(f2);
              localObject6 = k;
              f2 = ((Random)localObject6).nextFloat();
              f1 = 20.0F;
              f2 *= f1;
              k %= 2;
              if (k == 0) {
                f2 = -f2;
              }
              ((TextView)localObject5).setRotation(f2);
            }
            else
            {
              localObject1 = new c/u;
              ((u)localObject1).<init>("null cannot be cast to non-null type android.support.constraint.ConstraintLayout.LayoutParams");
              throw ((Throwable)localObject1);
            }
          }
          else
          {
            boolean bool2 = localObject5 instanceof Guideline;
            if (bool2)
            {
              localObject5 = (Guideline)localObject5;
              localObject4 = ((Guideline)localObject5).getLayoutParams();
              if (localObject4 == null) {
                break;
              }
              localObject4 = (ConstraintLayout.a)localObject4;
              f2 = d;
              c = f2;
              localObject4 = (ViewGroup.LayoutParams)localObject4;
              ((Guideline)localObject5).setLayoutParams((ViewGroup.LayoutParams)localObject4);
            }
          }
        }
        localObject1 = new c/u;
        ((u)localObject1).<init>("null cannot be cast to non-null type android.support.constraint.ConstraintLayout.LayoutParams");
        throw ((Throwable)localObject1);
        label514:
        ((com.truecaller.flashsdk.ui.customviews.a.f)localObject1).requestLayout();
        return;
      }
      localObject1 = new c/u;
      ((u)localObject1).<init>("null cannot be cast to non-null type android.app.Activity");
      throw ((Throwable)localObject1);
    }
  }
  
  public final void ae()
  {
    ProgressBar localProgressBar = O;
    if (localProgressBar == null)
    {
      String str1 = "progressbar";
      c.g.b.k.a(str1);
    }
    int i = 60000;
    localProgressBar.setMax(i);
    localProgressBar = O;
    if (localProgressBar == null)
    {
      String str2 = "progressbar";
      c.g.b.k.a(str2);
    }
    localProgressBar.setProgress(i);
  }
  
  public final void af()
  {
    Object localObject = this;
    localObject = android.support.v4.content.d.a((Context)this);
    BroadcastReceiver localBroadcastReceiver = (BroadcastReceiver)A;
    ((android.support.v4.content.d)localObject).a(localBroadcastReceiver);
  }
  
  public final void ag()
  {
    h localh = (h)c();
    Bitmap localBitmap = o;
    localh.a(localBitmap);
  }
  
  public final void ah()
  {
    Object localObject1 = j();
    int i = 0;
    Object localObject2 = null;
    ((Toolbar)localObject1).setVisibility(0);
    localObject1 = X;
    if (localObject1 == null)
    {
      localObject2 = "closeReplyContact";
      c.g.b.k.a((String)localObject2);
    }
    i = 8;
    ((ImageView)localObject1).setVisibility(i);
    localObject1 = F;
    if (localObject1 != null) {
      ((View)localObject1).setVisibility(i);
    }
    localObject1 = V;
    String str;
    if (localObject1 == null)
    {
      str = "imageContainerV2";
      c.g.b.k.a(str);
    }
    ((FrameLayout)localObject1).setVisibility(i);
    localObject1 = K;
    if (localObject1 == null)
    {
      str = "replyWithText";
      c.g.b.k.a(str);
    }
    ((TextView)localObject1).setVisibility(i);
    localObject1 = (FlashReceiveFooterView)e();
    localObject2 = ad;
    if (localObject2 == null)
    {
      str = "editMessageText";
      c.g.b.k.a(str);
    }
    localObject2 = (CharSequence)((EditText)localObject2).getText();
    boolean bool = TextUtils.isEmpty((CharSequence)localObject2) ^ true;
    ((FlashReceiveFooterView)localObject1).c(bool);
    localObject1 = j;
    if (localObject1 != null)
    {
      ((ImageView)localObject1).setBackground(null);
      return;
    }
  }
  
  public final void ai()
  {
    Object localObject = ac;
    if (localObject == null)
    {
      str1 = "attachView";
      c.g.b.k.a(str1);
    }
    int i = 8;
    ((FlashAttachButton)localObject).setVisibility(i);
    ((FlashReceiveFooterView)e()).l();
    ((FlashReceiveFooterView)e()).q();
    localObject = ab;
    if (localObject != null) {
      ((FrameLayout)localObject).setVisibility(i);
    }
    localObject = Y;
    if (localObject == null)
    {
      str2 = "emojiDivider";
      c.g.b.k.a(str2);
    }
    ((View)localObject).setVisibility(i);
    localObject = K;
    if (localObject == null)
    {
      str2 = "replyWithText";
      c.g.b.k.a(str2);
    }
    ((TextView)localObject).setVisibility(i);
    localObject = C;
    if (localObject == null)
    {
      str2 = "flashText";
      c.g.b.k.a(str2);
    }
    ((AppCompatTextView)localObject).setVisibility(i);
    j().setVisibility(i);
    localObject = F;
    if (localObject != null) {
      ((View)localObject).setVisibility(i);
    }
    int j = R.id.buttonContainer;
    localObject = (LinearLayout)e(j);
    String str2 = "buttonContainer";
    c.g.b.k.a(localObject, str2);
    ((LinearLayout)localObject).setVisibility(i);
    localObject = V;
    if (localObject == null)
    {
      str1 = "imageContainerV2";
      c.g.b.k.a(str1);
    }
    i = 0;
    String str1 = null;
    ((FrameLayout)localObject).setVisibility(0);
    localObject = X;
    if (localObject == null)
    {
      str2 = "closeReplyContact";
      c.g.b.k.a(str2);
    }
    ((ImageView)localObject).setVisibility(0);
    ((FlashReceiveFooterView)e()).o();
    ((FlashReceiveFooterView)e()).n();
  }
  
  public final void aj()
  {
    Object localObject1 = ac;
    if (localObject1 == null)
    {
      localObject2 = "attachView";
      c.g.b.k.a((String)localObject2);
    }
    boolean bool = false;
    Object localObject2 = null;
    ((FlashAttachButton)localObject1).setVisibility(0);
    ((FlashReceiveFooterView)e()).m();
    localObject1 = V;
    if (localObject1 == null)
    {
      str1 = "imageContainerV2";
      c.g.b.k.a(str1);
    }
    int i = 8;
    ((FrameLayout)localObject1).setVisibility(i);
    localObject1 = Y;
    String str2;
    if (localObject1 == null)
    {
      str2 = "emojiDivider";
      c.g.b.k.a(str2);
    }
    ((View)localObject1).setVisibility(0);
    localObject1 = C;
    if (localObject1 == null)
    {
      str2 = "flashText";
      c.g.b.k.a(str2);
    }
    ((AppCompatTextView)localObject1).setVisibility(0);
    j().setVisibility(0);
    localObject1 = X;
    if (localObject1 == null)
    {
      str2 = "closeReplyContact";
      c.g.b.k.a(str2);
    }
    ((ImageView)localObject1).setVisibility(i);
    int j = R.id.buttonContainer;
    localObject1 = (LinearLayout)e(j);
    String str1 = "buttonContainer";
    c.g.b.k.a(localObject1, str1);
    ((LinearLayout)localObject1).setVisibility(0);
    ((FlashReceiveFooterView)e()).g();
    localObject1 = (FlashReceiveFooterView)e();
    localObject2 = ad;
    if (localObject2 == null)
    {
      str1 = "editMessageText";
      c.g.b.k.a(str1);
    }
    bool = TextUtils.isEmpty((CharSequence)((EditText)localObject2).getText()) ^ true;
    ((FlashReceiveFooterView)localObject1).c(bool);
  }
  
  public final void ak()
  {
    Object localObject = V;
    if (localObject == null)
    {
      String str1 = "imageContainerV2";
      c.g.b.k.a(str1);
    }
    int i = 8;
    ((FrameLayout)localObject).setVisibility(i);
    localObject = j();
    String str2 = null;
    ((Toolbar)localObject).setVisibility(0);
    localObject = X;
    if (localObject == null)
    {
      str2 = "closeReplyContact";
      c.g.b.k.a(str2);
    }
    ((ImageView)localObject).setVisibility(i);
    localObject = ac;
    if (localObject == null)
    {
      str2 = "attachView";
      c.g.b.k.a(str2);
    }
    ((FlashAttachButton)localObject).setVisibility(i);
    ((FlashReceiveFooterView)e()).r();
  }
  
  public final void al()
  {
    Object localObject = (FlashReceiveFooterView)e();
    int i = 8;
    ((FlashReceiveFooterView)localObject).setVisibility(i);
    localObject = K;
    if (localObject == null)
    {
      String str = "replyWithText";
      c.g.b.k.a(str);
    }
    ((TextView)localObject).setVisibility(i);
  }
  
  public final void am()
  {
    Object localObject = f().getMenu();
    int i = R.id.action_download;
    localObject = ((Menu)localObject).findItem(i);
    c.g.b.k.a(localObject, "contactHeaderView.menu.f…tem(R.id.action_download)");
    ((MenuItem)localObject).setVisible(true);
  }
  
  public final void an()
  {
    Object localObject = f().getMenu();
    int i = R.id.action_download;
    localObject = ((Menu)localObject).findItem(i);
    c.g.b.k.a(localObject, "contactHeaderView.menu.f…tem(R.id.action_download)");
    ((MenuItem)localObject).setVisible(false);
  }
  
  public final void b()
  {
    ((h)c()).g();
  }
  
  public final void b(int paramInt1, int paramInt2)
  {
    FlashAttachButton localFlashAttachButton = ac;
    if (localFlashAttachButton == null)
    {
      localObject = "attachView";
      c.g.b.k.a((String)localObject);
    }
    Object localObject = this;
    Drawable localDrawable = com.truecaller.utils.ui.b.a((Context)this, paramInt1, paramInt2);
    c.g.b.k.a(localDrawable, "ThemeUtils.getTintedDraw…his, drawable, tintColor)");
    localFlashAttachButton.setDrawable(localDrawable);
  }
  
  public final void b(Drawable paramDrawable)
  {
    c.g.b.k.b(paramDrawable, "drawable");
    ((FlashReceiveFooterView)e()).setBackground(paramDrawable);
  }
  
  public final void b(Uri paramUri)
  {
    c.g.b.k.b(paramUri, "imageUri");
    paramUri = d().a(paramUri);
    ag localag = ag;
    paramUri.a(localag);
    ((FlashReceiveFooterView)e()).c(true);
  }
  
  public final void b(Flash paramFlash)
  {
    c.g.b.k.b(paramFlash, "flash");
    Intent localIntent = u;
    paramFlash = (Parcelable)paramFlash;
    localIntent.putExtra("extra_flash", paramFlash);
    paramFlash = u;
    b(paramFlash);
  }
  
  public final void b(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "imageUrl");
    String str = "message";
    c.g.b.k.b(paramString2, str);
    super.b(paramString1, paramString2);
    paramString1 = C;
    if (paramString1 == null)
    {
      str = "flashText";
      c.g.b.k.a(str);
    }
    int i = 8;
    paramString1.setVisibility(i);
    paramString1 = D;
    if (paramString1 == null)
    {
      str = "imageText";
      c.g.b.k.a(str);
    }
    paramString2 = (CharSequence)paramString2;
    paramString1.setText(paramString2);
  }
  
  public final void b(String paramString1, String paramString2, String paramString3)
  {
    c.g.b.k.b(paramString1, "imageUrl");
    c.g.b.k.b(paramString2, "message");
    String str = "wallpaperUrl";
    c.g.b.k.b(paramString3, str);
    b(paramString1, paramString2);
    paramString1 = T;
    if (paramString1 == null)
    {
      str = "overlayBackgroundImage";
      c.g.b.k.a(str);
    }
    float f = k.c();
    paramString1.setAlpha(f);
    paramString1 = d().a(paramString3);
    paramString3 = T;
    if (paramString3 == null)
    {
      str = "overlayBackgroundImage";
      c.g.b.k.a(str);
    }
    f = 0.0F;
    str = null;
    paramString1.a(paramString3, null);
    paramString1 = C;
    if (paramString1 == null)
    {
      paramString3 = "flashText";
      c.g.b.k.a(paramString3);
    }
    int i = 8;
    paramString1.setVisibility(i);
    paramString1 = D;
    if (paramString1 == null)
    {
      paramString3 = "imageText";
      c.g.b.k.a(paramString3);
    }
    paramString2 = (CharSequence)paramString2;
    paramString1.setText(paramString2);
  }
  
  public final void b(List paramList)
  {
    c.g.b.k.b(paramList, "responses");
    Object localObject1 = H;
    if (localObject1 == null)
    {
      localObject2 = "btnYes";
      c.g.b.k.a((String)localObject2);
    }
    int i = 0;
    Object localObject2 = (CharSequence)paramList.get(0);
    ((Button)localObject1).setText((CharSequence)localObject2);
    localObject1 = J;
    if (localObject1 == null)
    {
      localObject2 = "btnNo";
      c.g.b.k.a((String)localObject2);
    }
    i = 1;
    paramList = (CharSequence)paramList.get(i);
    ((Button)localObject1).setText(paramList);
    paramList = I;
    if (paramList == null)
    {
      localObject1 = "btnOk";
      c.g.b.k.a((String)localObject1);
    }
    paramList.setVisibility(8);
  }
  
  public final void c(int paramInt)
  {
    super.c(paramInt);
    ImageView localImageView = R;
    if (localImageView == null)
    {
      String str = "overlayImage";
      c.g.b.k.a(str);
    }
    localImageView.setImageResource(paramInt);
  }
  
  public final void c(int paramInt1, int paramInt2)
  {
    FlashAttachButton localFlashAttachButton = ac;
    if (localFlashAttachButton == null)
    {
      localObject1 = "attachView";
      c.g.b.k.a((String)localObject1);
    }
    Object localObject1 = localFlashAttachButton.getContext();
    Drawable localDrawable = com.truecaller.utils.ui.b.a((Context)localObject1, paramInt1, paramInt2);
    Object localObject2 = "ThemeUtils.getTintedDraw…ontext, imageId, colorId)";
    c.g.b.k.a(localDrawable, (String)localObject2);
    localFlashAttachButton.setDrawable(localDrawable);
    paramInt1 = 0;
    localDrawable = null;
    localFlashAttachButton.setVisibility(0);
    paramInt2 = a;
    if (paramInt2 != 0)
    {
      localObject2 = e;
      int i = 4;
      if (localObject2 != null)
      {
        ((LinearLayout)localObject2).removeAllViews();
        ((LinearLayout)localObject2).setVisibility(i);
      }
      localObject2 = f;
      if (localObject2 != null)
      {
        ViewPropertyAnimator localViewPropertyAnimator = ((View)localObject2).animate();
        localViewPropertyAnimator.cancel();
        ((View)localObject2).setVisibility(i);
      }
      b = false;
      a = false;
    }
  }
  
  public final void c(Flash paramFlash)
  {
    c.g.b.k.b(paramFlash, "flash");
    Intent localIntent = w;
    paramFlash = (Parcelable)paramFlash;
    localIntent.putExtra("extra_flash", paramFlash);
    paramFlash = w;
    b(paramFlash);
  }
  
  public final void c(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "imageUrl");
    String str1 = "message";
    c.g.b.k.b(paramString2, str1);
    super.c(paramString1, paramString2);
    paramString1 = C;
    if (paramString1 == null)
    {
      str1 = "flashText";
      c.g.b.k.a(str1);
    }
    paramString1.setVisibility(8);
    paramString1 = F;
    str1 = null;
    if (paramString1 != null) {
      paramString1.setVisibility(0);
    }
    paramString1 = E;
    if (paramString1 == null)
    {
      String str2 = "imageTextV2";
      c.g.b.k.a(str2);
    }
    paramString1.setVisibility(0);
    paramString1 = E;
    if (paramString1 == null)
    {
      str1 = "imageTextV2";
      c.g.b.k.a(str1);
    }
    paramString2 = (CharSequence)paramString2;
    paramString1.setText(paramString2);
  }
  
  public final void c(String paramString1, String paramString2, String paramString3)
  {
    c.g.b.k.b(paramString1, "imageUrl");
    c.g.b.k.b(paramString2, "message");
    String str = "wallpaperUrl";
    c.g.b.k.b(paramString3, str);
    c(paramString1, paramString2);
    paramString1 = T;
    if (paramString1 == null)
    {
      str = "overlayBackgroundImage";
      c.g.b.k.a(str);
    }
    float f = k.c();
    paramString1.setAlpha(f);
    paramString1 = d().a(paramString3);
    paramString3 = T;
    if (paramString3 == null)
    {
      str = "overlayBackgroundImage";
      c.g.b.k.a(str);
    }
    f = 0.0F;
    str = null;
    paramString1.a(paramString3, null);
    paramString1 = C;
    if (paramString1 == null)
    {
      paramString3 = "flashText";
      c.g.b.k.a(paramString3);
    }
    int i = 8;
    paramString1.setVisibility(i);
    paramString1 = D;
    if (paramString1 == null)
    {
      paramString3 = "imageText";
      c.g.b.k.a(paramString3);
    }
    paramString2 = (CharSequence)paramString2;
    paramString1.setText(paramString2);
  }
  
  public final void c(List paramList)
  {
    c.g.b.k.b(paramList, "menuItems");
    FlashAttachButton localFlashAttachButton = ac;
    if (localFlashAttachButton == null)
    {
      String str = "attachView";
      c.g.b.k.a(str);
    }
    localFlashAttachButton.setMenuItems(paramList);
  }
  
  public final void c(boolean paramBoolean)
  {
    ((FlashReceiveFooterView)e()).setMessageCursorVisible(paramBoolean);
  }
  
  public final void d(int paramInt)
  {
    Object localObject = this;
    localObject = (Activity)this;
    String[] arrayOfString = { "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE" };
    android.support.v4.app.a.a((Activity)localObject, arrayOfString, paramInt);
  }
  
  public final void d(Flash paramFlash)
  {
    c.g.b.k.b(paramFlash, "flash");
    Intent localIntent = s;
    paramFlash = (Parcelable)paramFlash;
    localIntent.putExtra("extra_flash", paramFlash);
    paramFlash = s;
    b(paramFlash);
  }
  
  public final void d(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "videoUrl");
    String str = "message";
    c.g.b.k.b(paramString2, str);
    super.d(paramString1, paramString2);
    paramString1 = C;
    if (paramString1 == null)
    {
      str = "flashText";
      c.g.b.k.a(str);
    }
    int i = 8;
    paramString1.setVisibility(i);
    paramString1 = G;
    if (paramString1 == null)
    {
      str = "videoText";
      c.g.b.k.a(str);
    }
    i = 0;
    str = null;
    paramString1.setEnabled(false);
    paramString1 = G;
    if (paramString1 == null)
    {
      str = "videoText";
      c.g.b.k.a(str);
    }
    paramString2 = (CharSequence)paramString2;
    paramString1.setText(paramString2);
  }
  
  public final void d(boolean paramBoolean)
  {
    ((FlashReceiveFooterView)e()).c(paramBoolean);
  }
  
  public final View e(int paramInt)
  {
    Object localObject1 = ah;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      ah = ((HashMap)localObject1);
    }
    localObject1 = ah;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = ah;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  public final void e(Flash paramFlash)
  {
    c.g.b.k.b(paramFlash, "flash");
    Object localObject = FlashMediaService.b;
    localObject = this;
    localObject = (Context)this;
    c.g.b.k.b(localObject, "context");
    c.g.b.k.b(paramFlash, "flash");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>((Context)localObject, FlashMediaService.class);
    localIntent.setAction("action_image_download");
    paramFlash = (Parcelable)paramFlash;
    localIntent.putExtra("extra_flash", paramFlash);
    startService(localIntent);
  }
  
  public final void e(String paramString)
  {
    c.g.b.k.b(paramString, "hint");
    ((FlashReceiveFooterView)e()).setCameraModeHint(paramString);
  }
  
  public final void e(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "placeName");
    c.g.b.k.b(paramString2, "locationImageUrl");
    FlashReceiveFooterView localFlashReceiveFooterView = (FlashReceiveFooterView)e();
    w localw = d();
    localFlashReceiveFooterView.a(paramString1, paramString2, localw);
  }
  
  public final void e(boolean paramBoolean)
  {
    long l = k.f();
    Object localObject1 = new android/support/transition/f;
    ((android.support.transition.f)localObject1).<init>();
    Object localObject2 = (View)f();
    ((android.support.transition.f)localObject1).b((View)localObject2);
    localObject2 = (View)e();
    ((android.support.transition.f)localObject1).b((View)localObject2);
    int i = R.id.buttonContainer;
    localObject2 = (LinearLayout)e(i);
    ((android.support.transition.f)localObject1).b((View)localObject2);
    localObject2 = new com/truecaller/flashsdk/ui/incoming/FlashActivity$j;
    ((FlashActivity.j)localObject2).<init>(this, paramBoolean);
    localObject2 = (n.c)localObject2;
    ((android.support.transition.f)localObject1).a((n.c)localObject2);
    ((android.support.transition.f)localObject1).a(l);
    Object localObject3 = new android/support/transition/d;
    ((android.support.transition.d)localObject3).<init>();
    localObject2 = F;
    if (localObject2 != null) {
      ((android.support.transition.d)localObject3).b((View)localObject2);
    }
    ((android.support.transition.d)localObject3).a(l);
    Object localObject4 = new android/support/transition/s;
    ((s)localObject4).<init>();
    String str = null;
    ((s)localObject4).b(0);
    localObject3 = (n)localObject3;
    localObject3 = ((s)localObject4).a((n)localObject3);
    localObject1 = (n)localObject1;
    ((s)localObject3).a((n)localObject1);
    localObject3 = W;
    if (localObject3 == null)
    {
      str = "flashUIContainer";
      c.g.b.k.a(str);
    }
    localObject3 = (ViewGroup)localObject3;
    localObject4 = (n)localObject4;
    android.support.transition.p.a((ViewGroup)localObject3, (n)localObject4);
  }
  
  public final void f(int paramInt)
  {
    h localh = (h)c();
    String str = ((FlashReceiveFooterView)e()).getMessageText();
    int i = ((FlashReceiveFooterView)e()).getSelectionStart();
    int j = ((FlashReceiveFooterView)e()).getSelectionEnd();
    localh.a(str, paramInt, i, j);
  }
  
  public final void f(String paramString)
  {
    c.g.b.k.b(paramString, "name");
    FlashContactHeaderView localFlashContactHeaderView = f();
    Object localObject = new com/truecaller/flashsdk/ui/incoming/FlashActivity$n;
    ((FlashActivity.n)localObject).<init>(this, paramString);
    localObject = (Runnable)localObject;
    localFlashContactHeaderView.post((Runnable)localObject);
  }
  
  public final void f(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "mapImageUrl");
    c.g.b.k.b(paramString2, "message");
    b(paramString1, paramString2);
    paramString1 = h();
    paramString2 = new com/truecaller/flashsdk/ui/incoming/FlashActivity$o;
    paramString2.<init>(this);
    paramString2 = (View.OnClickListener)paramString2;
    paramString1.setOnClickListener(paramString2);
  }
  
  public final void f(boolean paramBoolean)
  {
    Object localObject1 = N;
    if (localObject1 == null)
    {
      localObject2 = "layerView";
      c.g.b.k.a((String)localObject2);
    }
    Object localObject2 = View.TRANSLATION_Y;
    int i = 2;
    Object localObject3 = new float[i];
    localObject3[0] = 0.0F;
    Object localObject4 = getResources();
    int j = R.dimen.bouncing_view_jump;
    float f1 = -((Resources)localObject4).getDimension(j);
    j = 1;
    localObject3[j] = f1;
    localObject1 = ObjectAnimator.ofFloat(localObject1, (Property)localObject2, (float[])localObject3);
    c.g.b.k.a(localObject1, "animator");
    int k = k.d();
    long l1 = k;
    ((ObjectAnimator)localObject1).setDuration(l1);
    localObject2 = new android/view/animation/DecelerateInterpolator;
    ((DecelerateInterpolator)localObject2).<init>();
    localObject2 = (TimeInterpolator)localObject2;
    ((ObjectAnimator)localObject1).setInterpolator((TimeInterpolator)localObject2);
    localObject2 = N;
    if (localObject2 == null)
    {
      localObject3 = "layerView";
      c.g.b.k.a((String)localObject3);
    }
    localObject3 = View.TRANSLATION_Y;
    localObject4 = new float[i];
    Resources localResources = getResources();
    int m = R.dimen.bouncing_view_jump;
    float f2 = -localResources.getDimension(m);
    localObject4[0] = f2;
    localObject4[j] = 0.0F;
    localObject2 = ObjectAnimator.ofFloat(localObject2, (Property)localObject3, (float[])localObject4);
    c.g.b.k.a(localObject2, "animator1");
    long l2 = k.d() * 2;
    ((ObjectAnimator)localObject2).setDuration(l2);
    int n = k.e();
    l2 = n;
    ((ObjectAnimator)localObject2).setStartDelay(l2);
    localObject3 = new android/view/animation/BounceInterpolator;
    ((BounceInterpolator)localObject3).<init>();
    localObject3 = (TimeInterpolator)localObject3;
    ((ObjectAnimator)localObject2).setInterpolator((TimeInterpolator)localObject3);
    localObject3 = x;
    Animator[] arrayOfAnimator = new Animator[i];
    localObject1 = (Animator)localObject1;
    arrayOfAnimator[0] = localObject1;
    localObject1 = localObject2;
    localObject1 = (Animator)localObject2;
    arrayOfAnimator[j] = localObject1;
    ((AnimatorSet)localObject3).playSequentially(arrayOfAnimator);
    localObject1 = new com/truecaller/flashsdk/ui/incoming/FlashActivity$k;
    ((FlashActivity.k)localObject1).<init>(this);
    localObject1 = (Animator.AnimatorListener)localObject1;
    ((ObjectAnimator)localObject2).addListener((Animator.AnimatorListener)localObject1);
    localObject1 = N;
    if (localObject1 == null)
    {
      localObject2 = "layerView";
      c.g.b.k.a((String)localObject2);
    }
    localObject2 = new com/truecaller/flashsdk/ui/incoming/FlashActivity$l;
    ((FlashActivity.l)localObject2).<init>(this);
    localObject2 = (View.OnTouchListener)localObject2;
    ((View)localObject1).setOnTouchListener((View.OnTouchListener)localObject2);
    localObject1 = L;
    if (localObject1 == null)
    {
      localObject2 = "swipeView";
      c.g.b.k.a((String)localObject2);
    }
    localObject2 = this;
    localObject2 = (BouncingView.b)this;
    ((BouncingView)localObject1).a((BouncingView.b)localObject2, paramBoolean);
  }
  
  public final void g(int paramInt)
  {
    Button localButton = J;
    String str;
    if (localButton == null)
    {
      str = "btnNo";
      c.g.b.k.a(str);
    }
    localButton.setTextColor(paramInt);
    localButton = H;
    if (localButton == null)
    {
      str = "btnYes";
      c.g.b.k.a(str);
    }
    localButton.setTextColor(paramInt);
    localButton = I;
    if (localButton == null)
    {
      str = "btnOk";
      c.g.b.k.a(str);
    }
    localButton.setTextColor(paramInt);
  }
  
  public final void g(String paramString)
  {
    c.g.b.k.b(paramString, "name");
    com.truecaller.flashsdk.ui.whatsnew.b localb = new com/truecaller/flashsdk/ui/whatsnew/b;
    Object localObject = this;
    localObject = (Context)this;
    int i = R.string.flash_miss_popup;
    int j = 1;
    Object[] arrayOfObject = new Object[j];
    arrayOfObject[0] = paramString;
    paramString = getString(i, arrayOfObject);
    String str = "getString(R.string.flash_miss_popup, name)";
    c.g.b.k.a(paramString, str);
    localb.<init>((Context)localObject, paramString);
    paramString = S;
    if (paramString == null)
    {
      localObject = "overlayFlashFromText";
      c.g.b.k.a((String)localObject);
    }
    paramString = (View)paramString;
    localb.a(paramString, 0);
  }
  
  public final void g(boolean paramBoolean)
  {
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    Button localButton = H;
    String str;
    if (localButton == null)
    {
      str = "btnYes";
      c.g.b.k.a(str);
    }
    localButton.setVisibility(paramBoolean);
    localButton = I;
    if (localButton == null)
    {
      str = "btnOk";
      c.g.b.k.a(str);
    }
    localButton.setVisibility(paramBoolean);
    localButton = J;
    if (localButton == null)
    {
      str = "btnNo";
      c.g.b.k.a(str);
    }
    localButton.setVisibility(paramBoolean);
  }
  
  public final boolean g(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "mapUri");
    c.g.b.k.b(paramString2, "packageString");
    Intent localIntent = new android/content/Intent;
    String str = "android.intent.action.VIEW";
    paramString1 = Uri.parse(paramString1);
    localIntent.<init>(str, paramString1);
    localIntent.setPackage(paramString2);
    paramString1 = getIntent();
    paramString2 = getPackageManager();
    paramString1 = paramString1.resolveActivity(paramString2);
    if (paramString1 != null) {
      startActivity(localIntent);
    }
    paramString1 = getIntent();
    paramString2 = getPackageManager();
    paramString1 = paramString1.resolveActivity(paramString2);
    return paramString1 != null;
  }
  
  public final void h(int paramInt)
  {
    Object localObject1 = O;
    if (localObject1 == null)
    {
      localObject2 = "progressbar";
      c.g.b.k.a((String)localObject2);
    }
    localObject1 = ((ProgressBar)localObject1).getProgressDrawable();
    Object localObject2 = PorterDuff.Mode.SRC_IN;
    ((Drawable)localObject1).setColorFilter(paramInt, (PorterDuff.Mode)localObject2);
    localObject1 = O;
    if (localObject1 == null)
    {
      localObject2 = "progressbar";
      c.g.b.k.a((String)localObject2);
    }
    localObject1 = ((ProgressBar)localObject1).getBackground();
    localObject2 = PorterDuff.Mode.SRC_IN;
    ((Drawable)localObject1).setColorFilter(paramInt, (PorterDuff.Mode)localObject2);
  }
  
  public final void h(String paramString)
  {
    c.g.b.k.b(paramString, "message");
    Object localObject = C;
    if (localObject == null)
    {
      String str = "flashText";
      c.g.b.k.a(str);
    }
    paramString = (CharSequence)paramString;
    ((AppCompatTextView)localObject).setText(paramString);
    paramString = C;
    if (paramString == null)
    {
      localObject = "flashText";
      c.g.b.k.a((String)localObject);
    }
    android.support.v4.e.a.b.a((TextView)paramString, 15);
  }
  
  public final void h(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "mapImageUrl");
    c.g.b.k.b(paramString2, "message");
    FlashReceiveFooterView localFlashReceiveFooterView = (FlashReceiveFooterView)e();
    w localw = d();
    localFlashReceiveFooterView.a(paramString2, paramString1, localw);
  }
  
  public final void h(boolean paramBoolean)
  {
    TextView localTextView = K;
    if (localTextView == null)
    {
      String str = "replyWithText";
      c.g.b.k.a(str);
    }
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localTextView.setVisibility(paramBoolean);
  }
  
  public final void i(int paramInt)
  {
    Object localObject1 = new android/support/v7/app/AlertDialog$Builder;
    Object localObject2 = this;
    localObject2 = (Context)this;
    ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
    int i = R.string.block_profile_popup_description;
    ((AlertDialog.Builder)localObject1).setMessage(i);
    i = R.string.sfc_ok;
    Object localObject3 = new com/truecaller/flashsdk/ui/incoming/FlashActivity$m;
    ((FlashActivity.m)localObject3).<init>(this);
    localObject3 = (DialogInterface.OnClickListener)localObject3;
    ((AlertDialog.Builder)localObject1).setPositiveButton(i, (DialogInterface.OnClickListener)localObject3);
    i = R.string.cancel;
    ((AlertDialog.Builder)localObject1).setNegativeButton(i, null);
    localObject1 = ((AlertDialog.Builder)localObject1).create();
    ((AlertDialog)localObject1).show();
    localObject2 = ((AlertDialog)localObject1).getButton(-1);
    localObject1 = ((AlertDialog)localObject1).getButton(-2);
    ((Button)localObject2).setTextColor(paramInt);
    ((Button)localObject1).setTextColor(paramInt);
  }
  
  public final void i(String paramString)
  {
    c.g.b.k.b(paramString, "mapUri");
    Intent localIntent = new android/content/Intent;
    paramString = Uri.parse(paramString);
    localIntent.<init>("android.intent.action.VIEW", paramString);
    a(localIntent);
  }
  
  public final void i(boolean paramBoolean)
  {
    View localView = N;
    if (localView == null)
    {
      String str = "layerView";
      c.g.b.k.a(str);
    }
    if (paramBoolean) {
      paramBoolean = false;
    } else {
      paramBoolean = true;
    }
    localView.setVisibility(paramBoolean);
  }
  
  public final void j(int paramInt)
  {
    AppCompatTextView localAppCompatTextView = C;
    if (localAppCompatTextView == null)
    {
      String str = "flashText";
      c.g.b.k.a(str);
    }
    localAppCompatTextView.setTextColor(paramInt);
  }
  
  public final void j(String paramString)
  {
    c.g.b.k.b(paramString, "message");
    ((FlashReceiveFooterView)e()).setMessageText(paramString);
  }
  
  public final void k()
  {
    super.k();
    int i = R.id.textReceiveFlash;
    Object localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.textReceiveFlash)");
    localObject1 = (AppCompatTextView)localObject1;
    C = ((AppCompatTextView)localObject1);
    i = R.id.receiveImageText;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.receiveImageText)");
    localObject1 = (TextView)localObject1;
    D = ((TextView)localObject1);
    i = R.id.videoText;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.videoText)");
    localObject1 = (TextView)localObject1;
    G = ((TextView)localObject1);
    i = R.id.btnYes;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.btnYes)");
    localObject1 = (Button)localObject1;
    H = ((Button)localObject1);
    i = R.id.btnOk;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.btnOk)");
    localObject1 = (Button)localObject1;
    I = ((Button)localObject1);
    i = R.id.btnNo;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.btnNo)");
    localObject1 = (Button)localObject1;
    J = ((Button)localObject1);
    i = R.id.replyWithText;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.replyWithText)");
    localObject1 = (TextView)localObject1;
    K = ((TextView)localObject1);
    i = R.id.swipeView;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.swipeView)");
    localObject1 = (BouncingView)localObject1;
    L = ((BouncingView)localObject1);
    i = R.id.arrowView;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.arrowView)");
    localObject1 = (ArrowView)localObject1;
    M = ((ArrowView)localObject1);
    i = R.id.overLayViewContainer;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.overLayViewContainer)");
    N = ((View)localObject1);
    i = R.id.progressBar;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.progressBar)");
    localObject1 = (ProgressBar)localObject1;
    O = ((ProgressBar)localObject1);
    i = R.id.overlayName;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.overlayName)");
    localObject1 = (TextView)localObject1;
    P = ((TextView)localObject1);
    i = R.id.overlayCaller;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.overlayCaller)");
    localObject1 = (TextView)localObject1;
    Q = ((TextView)localObject1);
    i = R.id.overlayUserImage;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.overlayUserImage)");
    localObject1 = (ImageView)localObject1;
    R = ((ImageView)localObject1);
    i = R.id.overlayCaller;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.overlayCaller)");
    localObject1 = (TextView)localObject1;
    S = ((TextView)localObject1);
    i = R.id.imageOverlayBackground;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.imageOverlayBackground)");
    localObject1 = (ImageView)localObject1;
    T = ((ImageView)localObject1);
    i = R.id.bodyStub;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.bodyStub)");
    U = ((View)localObject1);
    i = R.id.root_container;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.root_container)");
    localObject1 = (ConstraintLayout)localObject1;
    W = ((ConstraintLayout)localObject1);
    i = R.id.footerEmojiDivider;
    localObject1 = findViewById(i);
    c.g.b.k.a(localObject1, "findViewById(R.id.footerEmojiDivider)");
    Y = ((View)localObject1);
    i = R.id.edit_message_text;
    localObject1 = findViewById(i);
    Object localObject2 = "findViewById(R.id.edit_message_text)";
    c.g.b.k.a(localObject1, (String)localObject2);
    localObject1 = (EditText)localObject1;
    ad = ((EditText)localObject1);
    localObject1 = L;
    if (localObject1 == null)
    {
      localObject2 = "swipeView";
      c.g.b.k.a((String)localObject2);
    }
    int j = R.id.overLayViewContainer;
    ((BouncingView)localObject1).setDragViewResId(j);
    localObject1 = (FlashReceiveFooterView)e();
    localObject2 = this;
    localObject2 = (b.a)this;
    ((FlashReceiveFooterView)localObject1).setActionListener((b.a)localObject2);
    localObject1 = f();
    localObject2 = this;
    localObject2 = (FlashContactHeaderView.a)this;
    ((FlashContactHeaderView)localObject1).setContactClickListener$flash_release((FlashContactHeaderView.a)localObject2);
    localObject1 = J;
    if (localObject1 == null)
    {
      localObject2 = "btnNo";
      c.g.b.k.a((String)localObject2);
    }
    localObject2 = this;
    localObject2 = (View.OnClickListener)this;
    ((Button)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = H;
    if (localObject1 == null)
    {
      localObject3 = "btnYes";
      c.g.b.k.a((String)localObject3);
    }
    ((Button)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = I;
    if (localObject1 == null)
    {
      localObject3 = "btnOk";
      c.g.b.k.a((String)localObject3);
    }
    ((Button)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = S;
    if (localObject1 == null)
    {
      localObject2 = "overlayFlashFromText";
      c.g.b.k.a((String)localObject2);
    }
    c.g.b.k.b(localObject1, "receiver$0");
    localObject2 = new android/text/SpannableStringBuilder;
    Object localObject3 = ((TextView)localObject1).getText();
    ((SpannableStringBuilder)localObject2).<init>((CharSequence)localObject3);
    com.truecaller.flashsdk.assist.t.a((TextView)localObject1, (SpannableStringBuilder)localObject2);
    localObject2 = (CharSequence)localObject2;
    ((TextView)localObject1).setText((CharSequence)localObject2);
    i = R.id.buttonContainer;
    localObject1 = (LinearLayout)e(i);
    c.g.b.k.a(localObject1, "buttonContainer");
    j = 0;
    localObject2 = null;
    ((LinearLayout)localObject1).setVisibility(0);
    localObject1 = C;
    if (localObject1 == null)
    {
      localObject3 = "flashText";
      c.g.b.k.a((String)localObject3);
    }
    localObject3 = this;
    localObject3 = (ActionMode.Callback)this;
    ((AppCompatTextView)localObject1).setCustomSelectionActionModeCallback((ActionMode.Callback)localObject3);
    ((FlashReceiveFooterView)e()).c(false);
  }
  
  public final void k(int paramInt)
  {
    FlashAttachButton localFlashAttachButton = ac;
    if (localFlashAttachButton == null)
    {
      String str = "attachView";
      c.g.b.k.a(str);
    }
    localFlashAttachButton.setBackgroundColor(paramInt);
  }
  
  public final void k(String paramString)
  {
    c.g.b.k.b(paramString, "message");
    Object localObject = getSupportFragmentManager();
    int i = R.id.waiting_container;
    localObject = (e)((android.support.v4.app.j)localObject).a(i);
    if (localObject != null)
    {
      ((e)localObject).a(paramString);
      return;
    }
  }
  
  public final void l(int paramInt)
  {
    Handler localHandler = e;
    Runnable localRunnable = y;
    long l = paramInt;
    localHandler.postDelayed(localRunnable, l);
  }
  
  public final void l(String paramString)
  {
    c.g.b.k.b(paramString, "action");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.VIEW");
    paramString = Uri.parse(paramString);
    localIntent.setData(paramString);
    startActivity(localIntent);
  }
  
  public final void m(int paramInt)
  {
    ProgressBar localProgressBar = O;
    if (localProgressBar == null)
    {
      String str = "progressbar";
      c.g.b.k.a(str);
    }
    localProgressBar.setProgress(paramInt);
  }
  
  public final boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
  {
    return false;
  }
  
  public final void onClick(View paramView)
  {
    c.g.b.k.b(paramView, "v");
    h localh = (h)c();
    int i = paramView.getId();
    paramView = ((Button)paramView).getText().toString();
    localh.a(i, paramView);
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    paramBundle = getWindow();
    int i = 2621568;
    paramBundle.setFlags(i, i);
    paramBundle = a.a();
    Object localObject = new com/truecaller/flashsdk/ui/incoming/c;
    ((c)localObject).<init>(this);
    paramBundle = paramBundle.a((c)localObject);
    localObject = com.truecaller.flashsdk.core.c.b;
    localObject = com.truecaller.flashsdk.core.c.b();
    paramBundle.a((com.truecaller.flashsdk.core.a.a.a)localObject).a().a(this);
    paramBundle = d;
    if (paramBundle == null)
    {
      localObject = "coreSettings";
      c.g.b.k.a((String)localObject);
    }
    localObject = "featureShareImageInFlash";
    boolean bool = paramBundle.b((String)localObject);
    int j;
    if (bool) {
      j = R.layout.activity_receive_flashv2;
    } else {
      j = R.layout.activity_receive_flash;
    }
    setContentView(j);
    paramBundle = (h)c();
    localObject = this;
    localObject = (com.truecaller.flashsdk.ui.base.d)this;
    paramBundle.a((com.truecaller.flashsdk.ui.base.d)localObject);
  }
  
  public final boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    ae = paramActionMode;
    return true;
  }
  
  public final boolean onCreateOptionsMenu(Menu paramMenu)
  {
    c.g.b.k.b(paramMenu, "menu");
    FlashContactHeaderView localFlashContactHeaderView = f();
    int i = R.menu.menu_incoming_header;
    localFlashContactHeaderView.inflateMenu(i);
    localFlashContactHeaderView = f();
    Object localObject = new com/truecaller/flashsdk/ui/incoming/FlashActivity$h;
    ((FlashActivity.h)localObject).<init>(this);
    localObject = (Toolbar.OnMenuItemClickListener)localObject;
    localFlashContactHeaderView.setOnMenuItemClickListener((Toolbar.OnMenuItemClickListener)localObject);
    return super.onCreateOptionsMenu(paramMenu);
  }
  
  public final void onDestroy()
  {
    super.onDestroy();
    Object localObject = Z;
    if (localObject != null) {
      ((MapView)localObject).d();
    }
    localObject = this;
    localObject = android.support.v4.content.d.a((Context)this);
    BroadcastReceiver localBroadcastReceiver = (BroadcastReceiver)A;
    ((android.support.v4.content.d)localObject).a(localBroadcastReceiver);
  }
  
  public final void onDestroyActionMode(ActionMode paramActionMode)
  {
    ae = null;
  }
  
  public final void onEmoticonClicked(com.truecaller.flashsdk.a.d paramd)
  {
    c.g.b.k.b(paramd, "emoticon");
    h localh = (h)c();
    String str = ((FlashReceiveFooterView)e()).getMessageText();
    int i = ((FlashReceiveFooterView)e()).getSelectionStart();
    int j = ((FlashReceiveFooterView)e()).getSelectionEnd();
    localh.a(str, paramd, i, j);
  }
  
  public final boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    c.g.b.k.b(paramKeyEvent, "keyEvent");
    h localh = (h)c();
    boolean bool = localh.a(paramKeyEvent);
    if (!bool)
    {
      paramInt = super.onKeyDown(paramInt, paramKeyEvent);
      if (paramInt == 0) {
        return false;
      }
    }
    return true;
  }
  
  public final boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    return false;
  }
  
  public final boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    c.g.b.k.b(paramMenu, "menu");
    h localh = (h)c();
    localh.m();
    boolean bool = super.onPrepareOptionsMenu(paramMenu);
    return bool;
  }
  
  public final void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    c.g.b.k.b(paramArrayOfString, "permissions");
    c.g.b.k.b(paramArrayOfInt, "grantResults");
    ((h)c()).a(paramInt, paramArrayOfString, paramArrayOfInt);
  }
  
  public final void onStart()
  {
    super.onStart();
    Object localObject1 = this;
    localObject1 = (Context)this;
    Object localObject2 = android.support.v4.content.d.a((Context)localObject1);
    Object localObject3 = (BroadcastReceiver)A;
    IntentFilter localIntentFilter = z;
    ((android.support.v4.content.d)localObject2).a((BroadcastReceiver)localObject3, localIntentFilter);
    localObject1 = android.support.v4.content.d.a((Context)localObject1);
    localObject2 = (BroadcastReceiver)af;
    localObject3 = new android/content/IntentFilter;
    ((IntentFilter)localObject3).<init>("action_image_flash");
    ((android.support.v4.content.d)localObject1).a((BroadcastReceiver)localObject2, (IntentFilter)localObject3);
    ((h)c()).h();
  }
  
  public final void onStop()
  {
    super.onStop();
    Object localObject1 = getSystemService("power");
    if (localObject1 != null)
    {
      localObject1 = (PowerManager)localObject1;
      int i = Build.VERSION.SDK_INT;
      int j = 20;
      boolean bool;
      if (i >= j) {
        bool = ((PowerManager)localObject1).isInteractive();
      } else {
        bool = ((PowerManager)localObject1).isScreenOn();
      }
      if (!bool) {
        return;
      }
      localObject1 = (h)c();
      Object localObject2 = (CharSequence)((FlashReceiveFooterView)e()).getMessageText();
      ImageView localImageView = er;
      if (localImageView == null)
      {
        String str = "mapView";
        c.g.b.k.a(str);
      }
      j = localImageView.getVisibility();
      if (j == 0)
      {
        j = 1;
      }
      else
      {
        j = 0;
        localImageView = null;
      }
      ((h)localObject1).b((CharSequence)localObject2, j);
      localObject1 = this;
      localObject1 = android.support.v4.content.d.a((Context)this);
      localObject2 = (BroadcastReceiver)af;
      ((android.support.v4.content.d)localObject1).a((BroadcastReceiver)localObject2);
      return;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type android.os.PowerManager");
    throw ((Throwable)localObject1);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.incoming.FlashActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */