package com.truecaller.flashsdk.ui.incoming;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import c.g.b.k;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;

final class FlashActivity$b
  implements GoogleMap.InfoWindowAdapter
{
  private final Context a;
  private final String b;
  
  public FlashActivity$b(Context paramContext, String paramString)
  {
    a = paramContext;
    b = paramString;
  }
  
  public final View a()
  {
    Object localObject1 = a;
    int i = R.layout.layout_map_info_window;
    localObject1 = View.inflate((Context)localObject1, i, null);
    i = R.id.title;
    Object localObject2 = ((View)localObject1).findViewById(i);
    k.a(localObject2, "view.findViewById<TextView>(R.id.title)");
    localObject2 = (TextView)localObject2;
    CharSequence localCharSequence = (CharSequence)b;
    ((TextView)localObject2).setText(localCharSequence);
    k.a(localObject1, "view");
    return (View)localObject1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.incoming.FlashActivity.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */