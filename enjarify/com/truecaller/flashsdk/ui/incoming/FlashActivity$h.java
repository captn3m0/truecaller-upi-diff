package com.truecaller.flashsdk.ui.incoming;

import android.support.v7.widget.Toolbar.OnMenuItemClickListener;
import android.view.MenuItem;
import c.g.b.k;

final class FlashActivity$h
  implements Toolbar.OnMenuItemClickListener
{
  FlashActivity$h(FlashActivity paramFlashActivity) {}
  
  public final boolean onMenuItemClick(MenuItem paramMenuItem)
  {
    FlashActivity localFlashActivity = a;
    k.a(paramMenuItem, "it");
    return localFlashActivity.onOptionsItemSelected(paramMenuItem);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.incoming.FlashActivity.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */