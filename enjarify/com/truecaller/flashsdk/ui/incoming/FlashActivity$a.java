package com.truecaller.flashsdk.ui.incoming;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import c.g.b.k;
import com.truecaller.flashsdk.models.QueuedFlash;

public final class FlashActivity$a
{
  public static Intent a(Context paramContext, QueuedFlash paramQueuedFlash, boolean paramBoolean)
  {
    k.b(paramContext, "context");
    k.b(paramQueuedFlash, "flash");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, FlashActivity.class);
    localIntent.addFlags(268435456);
    localIntent.addFlags(536870912);
    localIntent.addFlags(67108864);
    paramQueuedFlash = (Parcelable)paramQueuedFlash;
    localIntent.putExtra("flash", paramQueuedFlash);
    localIntent.putExtra("ACTION", "flashing");
    localIntent.putExtra("show_overlay", paramBoolean);
    return localIntent;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.incoming.FlashActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */