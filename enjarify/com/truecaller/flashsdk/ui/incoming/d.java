package com.truecaller.flashsdk.ui.incoming;

import javax.inject.Provider;

public final class d
  implements dagger.a.d
{
  private final c a;
  private final Provider b;
  
  private d(c paramc, Provider paramProvider)
  {
    a = paramc;
    b = paramProvider;
  }
  
  public static d a(c paramc, Provider paramProvider)
  {
    d locald = new com/truecaller/flashsdk/ui/incoming/d;
    locald.<init>(paramc, paramProvider);
    return locald;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.incoming.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */