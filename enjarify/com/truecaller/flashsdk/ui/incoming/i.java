package com.truecaller.flashsdk.ui.incoming;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import c.g.b.v.c;
import c.g.b.z;
import c.u;
import c.x;
import com.truecaller.flashsdk.R.attr;
import com.truecaller.flashsdk.R.color;
import com.truecaller.flashsdk.R.drawable;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.assist.aa;
import com.truecaller.flashsdk.assist.ae;
import com.truecaller.flashsdk.assist.al;
import com.truecaller.flashsdk.assist.an;
import com.truecaller.flashsdk.assist.g;
import com.truecaller.flashsdk.assist.q;
import com.truecaller.flashsdk.core.b;
import com.truecaller.flashsdk.models.Contact;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.FlashExtras;
import com.truecaller.flashsdk.models.FlashLocationExtras;
import com.truecaller.flashsdk.models.ImageFlash;
import com.truecaller.flashsdk.models.Payload;
import com.truecaller.flashsdk.models.QueuedFlash;
import com.truecaller.flashsdk.models.ReplyActionsItem;
import com.truecaller.flashsdk.models.Sender;
import com.truecaller.utils.l;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ax;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public final class i
  extends com.truecaller.flashsdk.ui.base.c
  implements h
{
  private String A;
  private String B;
  private final c.d.f C;
  private final b D;
  private List E;
  private boolean F;
  private Uri G;
  private boolean H;
  private ImageFlash I;
  private Double J;
  private Double K;
  private boolean L;
  private final String[] M;
  private final aa N;
  private final com.truecaller.flashsdk.assist.d O;
  private final com.google.gson.f P;
  private final com.truecaller.flashsdk.assist.j Q;
  private QueuedFlash p;
  private Flash q;
  private boolean r;
  private int s;
  private boolean t;
  private String u;
  private boolean v;
  private boolean w;
  private boolean x;
  private boolean y;
  private boolean z;
  
  public i(c.d.f paramf, com.google.firebase.messaging.a parama, ae paramae, aa paramaa, al paramal, g paramg, com.truecaller.flashsdk.assist.d paramd, com.truecaller.flashsdk.assist.a parama1, com.truecaller.flashsdk.d.a parama2, com.truecaller.flashsdk.assist.y paramy, com.google.gson.f paramf1, com.truecaller.flashsdk.assist.j paramj, q paramq, l paraml, com.truecaller.common.g.a parama3)
  {
    super(paramf, paramae, parama, paramal, paramg, parama1, parama2, paramy, paramf1, paramq, paraml, parama3);
    N = paramaa;
    O = paramd;
    localObject = paramf1;
    P = paramf1;
    localObject = paramj;
    Q = paramj;
    A = "";
    B = "";
    localObject = paramf;
    C = paramf;
    localObject = com.truecaller.flashsdk.core.c.a();
    D = ((b)localObject);
    F = true;
    localObject = new String[] { "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE" };
    M = ((String[])localObject);
  }
  
  private final void a(an paraman)
  {
    paraman = a;
    int i = paraman.hashCode();
    int j = 3035641;
    if (i == j)
    {
      Object localObject1 = "busy";
      boolean bool = paraman.equals(localObject1);
      if (bool)
      {
        paraman = q;
        if (paraman == null) {
          return;
        }
        localObject1 = new com/truecaller/flashsdk/models/Payload;
        String str = "busy";
        Object localObject2 = this.i;
        int k = R.string.is_busy;
        Object[] arrayOfObject = new Object[0];
        localObject2 = ((al)localObject2).a(k, arrayOfObject);
        k = 0;
        ((Payload)localObject1).<init>(str, (String)localObject2, null, null);
        paraman.j();
        paraman.a((Payload)localObject1);
        paraman.a("final");
        localObject1 = D;
        ((b)localObject1).a(paraman);
        bool = true;
        t = bool;
        paraman = (j)a;
        if (paraman != null)
        {
          localObject1 = p;
          if (localObject1 != null)
          {
            localObject1 = (Flash)localObject1;
            paraman.d((Flash)localObject1);
          }
          else
          {
            return;
          }
        }
        z();
        paraman = (j)a;
        if (paraman != null)
        {
          paraman.x();
          return;
        }
      }
    }
  }
  
  private static void a(Flash paramFlash)
  {
    Object localObject1 = paramFlash.f();
    c.g.b.k.a(localObject1, "payload");
    Object localObject2 = (CharSequence)((Payload)localObject1).a();
    CharSequence localCharSequence = (CharSequence)"emoji";
    boolean bool = TextUtils.equals((CharSequence)localObject2, localCharSequence);
    if (bool) {
      localObject1 = ((Payload)localObject1).b();
    } else {
      localObject1 = com.truecaller.flashsdk.assist.c.a(((Payload)localObject1).a());
    }
    localObject2 = (CharSequence)paramFlash.e();
    bool = TextUtils.isEmpty((CharSequence)localObject2);
    if (!bool)
    {
      localObject2 = z.a;
      int i = 2;
      Object[] arrayOfObject = new Object[i];
      String str = paramFlash.e();
      arrayOfObject[0] = str;
      int j = 1;
      arrayOfObject[j] = localObject1;
      localObject1 = Arrays.copyOf(arrayOfObject, i);
      localObject1 = String.format("%s %s", (Object[])localObject1);
      localObject2 = "java.lang.String.format(format, *args)";
      c.g.b.k.a(localObject1, (String)localObject2);
    }
    paramFlash.b((String)localObject1);
  }
  
  private final void a(Payload paramPayload)
  {
    j localj = (j)a;
    if (localj == null) {
      return;
    }
    Object localObject1 = paramPayload.d();
    c.g.b.k.a(localObject1, "payload.attachment");
    localObject1 = (CharSequence)localObject1;
    Object localObject2 = new c/n/k;
    ((c.n.k)localObject2).<init>(",");
    String str = null;
    localObject1 = ((c.n.k)localObject2).a((CharSequence)localObject1, 0);
    boolean bool1 = ((List)localObject1).isEmpty();
    int j = 1;
    int i;
    Object localObject3;
    int k;
    if (!bool1)
    {
      i = ((List)localObject1).size();
      localObject2 = ((List)localObject1).listIterator(i);
      do
      {
        boolean bool2 = ((ListIterator)localObject2).hasPrevious();
        if (!bool2) {
          break;
        }
        localObject3 = (CharSequence)((ListIterator)localObject2).previous();
        k = ((CharSequence)localObject3).length();
        if (k == 0)
        {
          k = 1;
        }
        else
        {
          k = 0;
          localObject3 = null;
        }
      } while (k != 0);
      localObject1 = (Iterable)localObject1;
      i = ((ListIterator)localObject2).nextIndex() + j;
      localObject1 = c.a.m.d((Iterable)localObject1, i);
    }
    else
    {
      localObject1 = (List)c.a.y.a;
    }
    localObject1 = (Collection)localObject1;
    if (localObject1 != null)
    {
      localObject2 = new String[0];
      localObject1 = ((Collection)localObject1).toArray((Object[])localObject2);
      if (localObject1 != null)
      {
        localObject1 = (String[])localObject1;
        localObject2 = paramPayload.b();
        c.g.b.k.a(localObject2, "payload.message");
        B = ((String)localObject2);
        i = 3;
        localObject2 = new String[i];
        localObject3 = this.i;
        int m = R.string.sfc_yes;
        Object[] arrayOfObject = new Object[0];
        localObject3 = ((al)localObject3).a(m, arrayOfObject);
        localObject2[0] = localObject3;
        localObject3 = this.i;
        m = R.string.sfc_no;
        arrayOfObject = new Object[0];
        localObject3 = ((al)localObject3).a(m, arrayOfObject);
        localObject2[j] = localObject3;
        localObject3 = this.i;
        m = R.string.sfc_share;
        arrayOfObject = new Object[0];
        localObject3 = ((al)localObject3).a(m, arrayOfObject);
        m = 2;
        localObject2[m] = localObject3;
        localObject2 = c.a.m.c((Object[])localObject2);
        k = localObject1.length;
        if (k == m)
        {
          paramPayload = localObject1[0];
          A = paramPayload;
          paramPayload = localObject1[j];
          localObject1 = o;
          str = "featureShareImageInFlash";
          boolean bool3 = ((com.truecaller.common.g.a)localObject1).b(str);
          if (bool3)
          {
            localObject1 = A;
            str = B;
            localj.c((String)localObject1, str, paramPayload);
          }
          else
          {
            localObject1 = A;
            str = B;
            localj.b((String)localObject1, str, paramPayload);
          }
        }
        else
        {
          paramPayload = paramPayload.d();
          c.g.b.k.a(paramPayload, "payload.attachment");
          A = paramPayload;
          paramPayload = o;
          localObject1 = "featureShareImageInFlash";
          boolean bool4 = paramPayload.b((String)localObject1);
          if (bool4)
          {
            paramPayload = A;
            localObject1 = B;
            localj.c(paramPayload, (String)localObject1);
          }
          else
          {
            paramPayload = A;
            localObject1 = B;
            localj.b(paramPayload, (String)localObject1);
          }
        }
        z = j;
        paramPayload = c.a.m.g((Iterable)localObject2);
        localj.a(paramPayload);
        return;
      }
      paramPayload = new c/u;
      paramPayload.<init>("null cannot be cast to non-null type kotlin.Array<T>");
      throw paramPayload;
    }
    paramPayload = new c/u;
    paramPayload.<init>("null cannot be cast to non-null type java.util.Collection<T>");
    throw paramPayload;
  }
  
  private final void a(Payload paramPayload, boolean paramBoolean)
  {
    j localj = (j)a;
    if (localj == null) {
      return;
    }
    Object localObject1 = this.i.a(paramPayload);
    String str1 = "call_me_back";
    Object localObject2 = paramPayload.a();
    boolean bool1 = c.g.b.k.a(str1, localObject2);
    if (bool1)
    {
      localObject1 = this.i;
      int i = R.string.CallMeBackFlashMessage;
      localObject2 = new Object[0];
      localObject1 = ((al)localObject1).a(i, (Object[])localObject2);
    }
    else
    {
      str1 = "tc_pay_request";
      localObject2 = paramPayload.a();
      boolean bool2 = c.g.b.k.a(str1, localObject2);
      if (bool2)
      {
        localj.al();
      }
      else
      {
        bool2 = true;
        localj.g(bool2);
        localj.h(bool2);
      }
    }
    localj.h((String)localObject1);
    if (paramBoolean)
    {
      str2 = "emoji";
      paramPayload = paramPayload.a();
      bool3 = c.g.b.k.a(str2, paramPayload);
      if (bool3)
      {
        paramPayload = new com/truecaller/flashsdk/ui/customviews/a/d;
        paramPayload.<init>();
        paramPayload = (com.truecaller.flashsdk.ui.customviews.a.c)paramPayload;
        a((String)localObject1, paramPayload);
      }
    }
    paramPayload = o;
    String str2 = "featureShareImageInFlash";
    boolean bool3 = paramPayload.b(str2);
    if (bool3)
    {
      paramPayload = k;
      paramBoolean = R.attr.theme_incoming_text;
      int j = paramPayload.b(paramBoolean);
      localj.g(j);
      paramPayload = this.i;
      paramBoolean = R.drawable.reply_button_bg_selector;
      paramPayload = paramPayload.a(paramBoolean);
      localj.a(paramPayload);
      localj.R();
    }
  }
  
  private final void a(ReplyActionsItem paramReplyActionsItem)
  {
    Object localObject = (j)a;
    if (localObject == null) {
      return;
    }
    Flash localFlash = q;
    if (localFlash == null) {
      return;
    }
    String str1 = "busy";
    String str2 = paramReplyActionsItem.getType();
    int i = str2.hashCode();
    int j = 109400031;
    if (i != j)
    {
      j = 629233382;
      if (i == j)
      {
        String str3 = "deeplink";
        boolean bool1 = str2.equals(str3);
        if (bool1)
        {
          str2 = paramReplyActionsItem.getAction();
          ((j)localObject).l(str2);
          break label132;
        }
      }
    }
    else
    {
      localObject = "share";
      boolean bool2 = str2.equals(localObject);
      if (bool2)
      {
        y();
        break label132;
      }
    }
    str1 = "custom_flash";
    label132:
    localObject = new com/truecaller/flashsdk/models/Payload;
    str2 = paramReplyActionsItem.getName();
    ((Payload)localObject).<init>(str1, str2, null, null);
    localFlash.a((Payload)localObject);
    v();
    paramReplyActionsItem = paramReplyActionsItem.getType();
    a("ANDROID_FLASH_CUSTOM_BUTTON_CLICKED", paramReplyActionsItem);
  }
  
  private final void a(Sender paramSender)
  {
    j localj = (j)a;
    if (localj == null) {
      return;
    }
    Object localObject1 = paramSender.b();
    u = ((String)localObject1);
    localObject1 = p;
    boolean bool1;
    if (localObject1 != null)
    {
      localObject1 = ((QueuedFlash)localObject1).f();
    }
    else
    {
      bool1 = false;
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      localObject1 = ((Payload)localObject1).a();
      if (localObject1 != null) {}
    }
    else
    {
      localObject1 = "";
    }
    String str = paramSender.c();
    paramSender = paramSender.a();
    if (paramSender != null)
    {
      long l = paramSender.longValue();
      paramSender = String.valueOf(l);
      if (paramSender != null)
      {
        Object localObject2 = this.j;
        boolean bool2 = ((g)localObject2).f();
        int j = 0;
        label173:
        int i;
        Object[] arrayOfObject;
        if (bool2)
        {
          localObject2 = O;
          paramSender = ((com.truecaller.flashsdk.assist.d)localObject2).b(paramSender);
          if (paramSender != null)
          {
            localObject2 = (CharSequence)paramSender.getName();
            bool2 = TextUtils.isEmpty((CharSequence)localObject2);
            if (!bool2)
            {
              localObject2 = paramSender.getName();
              break label173;
            }
          }
          localObject2 = u;
          u = ((String)localObject2);
          if (paramSender != null)
          {
            localObject2 = (CharSequence)paramSender.getImageUrl();
            bool2 = TextUtils.isEmpty((CharSequence)localObject2);
            if (!bool2) {
              str = paramSender.getImageUrl();
            }
          }
        }
        else
        {
          paramSender = this.i;
          i = R.string.red_contacts_permission;
          arrayOfObject = new Object[0];
          paramSender = paramSender.a(i, arrayOfObject);
          localj.d(paramSender);
        }
        paramSender = (j)a;
        if (paramSender != null)
        {
          localObject2 = "tc_pay_request";
          bool1 = c.g.b.k.a(localObject1, localObject2);
          if (bool1)
          {
            localObject1 = this.i;
            i = R.string.payment_request_from;
            arrayOfObject = new Object[0];
            localObject1 = ((al)localObject1).a(i, arrayOfObject);
          }
          else
          {
            localObject1 = this.i;
            i = R.string.flash_received_from;
            arrayOfObject = new Object[0];
            localObject1 = ((al)localObject1).a(i, arrayOfObject);
          }
          localObject2 = u;
          if (localObject2 == null) {
            return;
          }
          paramSender.a((String)localObject1, (String)localObject2);
        }
        paramSender = str;
        paramSender = (CharSequence)str;
        if (paramSender != null)
        {
          boolean bool3 = c.n.m.a(paramSender);
          if (!bool3) {}
        }
        else
        {
          j = 1;
        }
        if (j == 0)
        {
          localj.a(str);
          return;
        }
        int k = R.drawable.ic_empty_avatar;
        localj.c(k);
        return;
      }
    }
  }
  
  private final void a(String paramString, com.truecaller.flashsdk.ui.customviews.a.c paramc)
  {
    paramString = paramc.a(paramString);
    if (paramString != null)
    {
      boolean bool = paramString instanceof com.truecaller.flashsdk.ui.customviews.a.a;
      if (bool)
      {
        paramc = (j)a;
        if (paramc != null)
        {
          paramString = (com.truecaller.flashsdk.ui.customviews.a.a)paramString;
          paramc.a(paramString);
        }
        return;
      }
      bool = paramString instanceof com.truecaller.flashsdk.ui.customviews.a.h;
      if (bool)
      {
        paramc = (j)a;
        if (paramc != null)
        {
          paramString = (com.truecaller.flashsdk.ui.customviews.a.h)paramString;
          paramc.a(paramString);
          return;
        }
      }
    }
  }
  
  private final void a(String paramString1, String paramString2)
  {
    Object localObject1 = p;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = localObject1;
    localObject2 = (Flash)localObject1;
    boolean bool1 = b((Flash)localObject2);
    if (bool1)
    {
      localObject2 = new android/os/Bundle;
      ((Bundle)localObject2).<init>();
      Object localObject3 = p;
      if (localObject3 != null)
      {
        localObject3 = ((QueuedFlash)localObject3).a();
        if (localObject3 != null)
        {
          localObject3 = ((Sender)localObject3).a();
          if (localObject3 != null)
          {
            long l1 = ((Long)localObject3).longValue();
            localObject3 = String.valueOf(l1);
            if (localObject3 != null) {
              break label99;
            }
          }
        }
      }
      localObject3 = "";
      label99:
      Object localObject4 = j;
      boolean bool2 = ((g)localObject4).f();
      if (bool2)
      {
        localObject4 = O;
        bool3 = ((com.truecaller.flashsdk.assist.d)localObject4).a((String)localObject3);
      }
      else
      {
        bool3 = false;
        localObject3 = null;
      }
      Object localObject5 = ((QueuedFlash)localObject1).f();
      c.g.b.k.a(localObject5, "flashCopy.payload");
      localObject5 = ((Payload)localObject5).a();
      ((Bundle)localObject2).putString("type", (String)localObject5);
      localObject5 = ((QueuedFlash)localObject1).f();
      String str = "flashCopy.payload";
      c.g.b.k.a(localObject5, str);
      localObject5 = ((Payload)localObject5).a();
      ((Bundle)localObject2).putString("type", (String)localObject5);
      localObject5 = ((QueuedFlash)localObject1).h();
      ((Bundle)localObject2).putString("flash_message_id", (String)localObject5);
      localObject4 = "flash_sender_id";
      localObject5 = ((QueuedFlash)localObject1).a();
      if (localObject5 != null)
      {
        localObject5 = ((Sender)localObject5).a();
        if (localObject5 != null)
        {
          long l2 = ((Long)localObject5).longValue();
          localObject5 = String.valueOf(l2);
          break label275;
        }
      }
      localObject5 = null;
      label275:
      ((Bundle)localObject2).putString((String)localObject4, (String)localObject5);
      localObject1 = ((QueuedFlash)localObject1).c();
      ((Bundle)localObject2).putString("flash_thread_id", (String)localObject1);
      ((Bundle)localObject2).putBoolean("flash_from_phonebook", bool3);
      localObject3 = paramString2;
      localObject3 = (CharSequence)paramString2;
      localObject4 = (CharSequence)"ANDROID_FLASH_MISSED";
      boolean bool3 = TextUtils.equals((CharSequence)localObject3, (CharSequence)localObject4);
      ((Bundle)localObject2).putBoolean("flash_missed", bool3);
      localObject1 = "flash_action_name";
      ((Bundle)localObject2).putString((String)localObject1, paramString2);
      paramString2 = D;
      paramString2.a(paramString1, (Bundle)localObject2);
    }
  }
  
  private final void b(Payload paramPayload, boolean paramBoolean)
  {
    Object localObject1 = o;
    boolean bool1 = ((com.truecaller.common.g.a)localObject1).b("featureShareImageInFlash");
    j localj = (j)a;
    if (localj == null) {
      return;
    }
    Object localObject2 = paramPayload.d();
    c.g.b.k.a(localObject2, "payload.attachment");
    localObject2 = (CharSequence)localObject2;
    Object localObject3 = new c/n/k;
    ((c.n.k)localObject3).<init>(",");
    localObject2 = ((c.n.k)localObject3).a((CharSequence)localObject2, 0);
    boolean bool2 = ((List)localObject2).isEmpty();
    int k = 1;
    int j;
    CharSequence localCharSequence;
    int m;
    if (!bool2)
    {
      j = ((List)localObject2).size();
      localObject3 = ((List)localObject2).listIterator(j);
      do
      {
        boolean bool3 = ((ListIterator)localObject3).hasPrevious();
        if (!bool3) {
          break;
        }
        localCharSequence = (CharSequence)((ListIterator)localObject3).previous();
        m = localCharSequence.length();
        if (m == 0)
        {
          m = 1;
        }
        else
        {
          m = 0;
          localCharSequence = null;
        }
      } while (m != 0);
      localObject2 = (Iterable)localObject2;
      j = ((ListIterator)localObject3).nextIndex() + k;
      localObject2 = c.a.m.d((Iterable)localObject2, j);
    }
    else
    {
      localObject2 = (List)c.a.y.a;
    }
    localObject2 = (Collection)localObject2;
    if (localObject2 != null)
    {
      localObject3 = new String[0];
      localObject2 = ((Collection)localObject2).toArray((Object[])localObject3);
      if (localObject2 != null)
      {
        localObject2 = (String[])localObject2;
        j = localObject2.length;
        m = 2;
        if (j < m) {
          return;
        }
        localObject3 = this.i;
        int n = R.string.map_url;
        int i1 = 4;
        Object localObject4 = new Object[i1];
        Object[] arrayOfObject = localObject2[0];
        localObject4[0] = arrayOfObject;
        arrayOfObject = localObject2[k];
        localObject4[k] = arrayOfObject;
        arrayOfObject = localObject2[0];
        localObject4[m] = arrayOfObject;
        int i2 = 3;
        Object localObject5 = localObject2[k];
        localObject4[i2] = localObject5;
        localObject3 = ((al)localObject3).a(n, (Object[])localObject4);
        Object localObject6 = (CharSequence)paramPayload.b();
        boolean bool4 = TextUtils.isEmpty((CharSequence)localObject6);
        if (!bool4)
        {
          localObject6 = paramPayload.b();
          localObject4 = "payload.message";
          c.g.b.k.a(localObject6, (String)localObject4);
        }
        else
        {
          localObject6 = this.i;
          i1 = R.string.i_am_here;
          arrayOfObject = new Object[0];
          localObject6 = ((al)localObject6).a(i1, arrayOfObject);
        }
        if (paramBoolean)
        {
          paramPayload = this.i;
          paramBoolean = R.string.lat_long;
          localObject1 = new Object[m];
          localCharSequence = localObject2[0];
          localObject1[0] = localCharSequence;
          localObject2 = localObject2[k];
          localObject1[k] = localObject2;
          paramPayload = paramPayload.a(paramBoolean, (Object[])localObject1);
          b = paramPayload;
          localj.h((String)localObject3, (String)localObject6);
          return;
        }
        if (bool1)
        {
          paramBoolean = false;
          Object localObject7 = null;
          paramPayload = paramPayload.e();
          if (paramPayload != null)
          {
            localObject7 = P;
            localObject1 = FlashExtras.class;
            paramPayload = (FlashExtras)((com.google.gson.f)localObject7).a(paramPayload, (Class)localObject1);
            localObject7 = paramPayload.getLocation();
          }
          if (localObject7 != null)
          {
            paramPayload = ((FlashLocationExtras)localObject7).getLat();
            J = paramPayload;
            paramPayload = ((FlashLocationExtras)localObject7).getLong();
            K = paramPayload;
            paramPayload = ((FlashLocationExtras)localObject7).getLocation_text();
            this.d = paramPayload;
            paramPayload = ((FlashLocationExtras)localObject7).getAddress();
            c = paramPayload;
            paramPayload = this.i;
            i = R.string.lat_long;
            localObject2 = new Object[m];
            localObject3 = String.valueOf(((FlashLocationExtras)localObject7).getLat());
            localObject2[0] = localObject3;
            localObject7 = String.valueOf(((FlashLocationExtras)localObject7).getLong());
            localObject2[k] = localObject7;
            paramPayload = paramPayload.a(i, (Object[])localObject2);
            b = paramPayload;
          }
          else
          {
            paramPayload = this;
            paramPayload = (i)this;
            localObject7 = Double.valueOf(Double.parseDouble(localObject2[0]));
            J = ((Double)localObject7);
            double d = Double.parseDouble(localObject2[k]);
            localObject7 = Double.valueOf(d);
            K = ((Double)localObject7);
            d = ((String)localObject6);
            localObject7 = i;
            i = R.string.lat_long;
            localObject3 = new Object[m];
            localCharSequence = localObject2[0];
            localObject3[0] = localCharSequence;
            localObject2 = localObject2[k];
            localObject3[k] = localObject2;
            localObject7 = ((al)localObject7).a(i, (Object[])localObject3);
            b = ((String)localObject7);
          }
          localj.B();
          paramPayload = this.k;
          paramBoolean = R.attr.colorPrimaryDark;
          int i3 = paramPayload.b(paramBoolean);
          localj.g(i3);
          i3 = R.attr.theme_bg_contact_transparent_header;
          localObject7 = this.k;
          int i = R.color.white;
          paramBoolean = ((com.truecaller.flashsdk.assist.a)localObject7).a(i);
          localj.a(i3, paramBoolean);
          localj.Q();
          return;
        }
        localj.f((String)localObject3, (String)localObject6);
        return;
      }
      paramPayload = new c/u;
      paramPayload.<init>("null cannot be cast to non-null type kotlin.Array<T>");
      throw paramPayload;
    }
    paramPayload = new c/u;
    paramPayload.<init>("null cannot be cast to non-null type java.util.Collection<T>");
    throw paramPayload;
  }
  
  private static boolean b(Flash paramFlash)
  {
    CharSequence localCharSequence = (CharSequence)paramFlash.h();
    boolean bool1 = TextUtils.isEmpty(localCharSequence);
    if (!bool1)
    {
      paramFlash = (CharSequence)paramFlash.c();
      boolean bool2 = TextUtils.isEmpty(paramFlash);
      if (!bool2) {
        return true;
      }
    }
    return false;
  }
  
  private final void d(boolean paramBoolean)
  {
    Object localObject1 = (j)a;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = p;
    if (localObject2 != null)
    {
      localObject2 = ((QueuedFlash)localObject2).f();
      if (localObject2 != null)
      {
        ((j)localObject1).W();
        Object localObject3 = ((Payload)localObject2).a();
        Object localObject4 = "emoji";
        boolean bool1 = c.g.b.k.a(localObject3, localObject4);
        int i;
        if (bool1)
        {
          i = -16777216;
        }
        else
        {
          localObject3 = this.k;
          k = R.attr.theme_incoming_text;
          i = ((com.truecaller.flashsdk.assist.a)localObject3).b(k);
        }
        ((j)localObject1).j(i);
        localObject3 = ((Payload)localObject2).e();
        int k = 0;
        localObject4 = null;
        Object localObject5;
        Object localObject6;
        if (localObject3 != null)
        {
          localObject5 = P;
          localObject6 = FlashExtras.class;
          localObject3 = (FlashExtras)((com.google.gson.f)localObject5).a((String)localObject3, (Class)localObject6);
          if (localObject3 != null)
          {
            localObject3 = ((FlashExtras)localObject3).getReplyActions();
          }
          else
          {
            i = 0;
            localObject3 = null;
          }
          E = ((List)localObject3);
        }
        localObject3 = E;
        int m = 2;
        int n = 3;
        boolean bool3 = true;
        Object localObject7;
        if (localObject3 != null)
        {
          if (localObject3 != null)
          {
            int i1 = ((List)localObject3).size();
            int i3 = 10;
            if (i1 == n)
            {
              localObject5 = (j)a;
              if (localObject5 != null)
              {
                localObject3 = (Iterable)localObject3;
                localObject6 = new java/util/ArrayList;
                i1 = c.a.m.a((Iterable)localObject3, i3);
                ((ArrayList)localObject6).<init>(i1);
                localObject6 = (Collection)localObject6;
                localObject3 = ((Iterable)localObject3).iterator();
                for (;;)
                {
                  boolean bool4 = ((Iterator)localObject3).hasNext();
                  if (!bool4) {
                    break;
                  }
                  localObject7 = ((ReplyActionsItem)((Iterator)localObject3).next()).getName();
                  ((Collection)localObject6).add(localObject7);
                }
                localObject6 = (List)localObject6;
                ((j)localObject5).a((List)localObject6);
              }
              z = bool3;
            }
            else
            {
              n = ((List)localObject3).size();
              if (n == m)
              {
                localObject5 = (j)a;
                if (localObject5 != null)
                {
                  localObject3 = (Iterable)localObject3;
                  localObject6 = new java/util/ArrayList;
                  int i2 = c.a.m.a((Iterable)localObject3, i3);
                  ((ArrayList)localObject6).<init>(i2);
                  localObject6 = (Collection)localObject6;
                  localObject3 = ((Iterable)localObject3).iterator();
                  for (;;)
                  {
                    boolean bool5 = ((Iterator)localObject3).hasNext();
                    if (!bool5) {
                      break;
                    }
                    localObject7 = ((ReplyActionsItem)((Iterator)localObject3).next()).getName();
                    ((Collection)localObject6).add(localObject7);
                  }
                  localObject6 = (List)localObject6;
                  ((j)localObject5).b((List)localObject6);
                }
                z = bool3;
              }
            }
          }
        }
        else
        {
          localObject3 = ((Payload)localObject2).c();
          if (localObject3 != null)
          {
            i = ((List)localObject3).size();
            if (i == n)
            {
              localObject3 = ((Payload)localObject2).c();
              localObject5 = "payload.responses";
              c.g.b.k.a(localObject3, (String)localObject5);
              ((j)localObject1).a((List)localObject3);
              z = bool3;
              break label750;
            }
          }
          localObject3 = ((Payload)localObject2).c();
          if (localObject3 != null)
          {
            i = ((List)localObject3).size();
            if (i == m)
            {
              z = bool3;
              localObject3 = ((Payload)localObject2).c();
              localObject5 = "payload.responses";
              c.g.b.k.a(localObject3, (String)localObject5);
              ((j)localObject1).b((List)localObject3);
              break label750;
            }
          }
          z = false;
          localObject3 = new java/util/ArrayList;
          ((ArrayList)localObject3).<init>();
          localObject5 = this.i;
          n = R.string.sfc_yes;
          localObject7 = new Object[0];
          localObject5 = ((al)localObject5).a(n, (Object[])localObject7);
          ((ArrayList)localObject3).add(localObject5);
          localObject5 = this.i;
          n = R.string.sfc_ok;
          localObject7 = new Object[0];
          localObject5 = ((al)localObject5).a(n, (Object[])localObject7);
          ((ArrayList)localObject3).add(localObject5);
          localObject5 = this.i;
          n = R.string.sfc_no;
          localObject7 = new Object[0];
          localObject5 = ((al)localObject5).a(n, (Object[])localObject7);
          ((ArrayList)localObject3).add(localObject5);
          localObject3 = (List)localObject3;
          ((j)localObject1).a((List)localObject3);
        }
        label750:
        localObject3 = (CharSequence)((Payload)localObject2).d();
        if (localObject3 != null)
        {
          bool2 = c.n.m.a((CharSequence)localObject3);
          if (!bool2)
          {
            bool2 = false;
            localObject3 = null;
            break label791;
          }
        }
        boolean bool2 = true;
        label791:
        m = 1901043637;
        int j;
        if (!bool2)
        {
          localObject8 = ((Payload)localObject2).a();
          if (localObject8 != null)
          {
            j = ((String)localObject8).hashCode();
            n = 100313435;
            if (j != n)
            {
              n = 112202875;
              if (j != n)
              {
                if (j == m)
                {
                  localObject1 = "location";
                  paramBoolean = ((String)localObject8).equals(localObject1);
                  if (paramBoolean) {
                    b((Payload)localObject2, false);
                  }
                }
              }
              else
              {
                localObject3 = "video";
                paramBoolean = ((String)localObject8).equals(localObject3);
                if (paramBoolean)
                {
                  localObject8 = ((Payload)localObject2).d();
                  localObject2 = ((Payload)localObject2).b();
                  if (localObject2 == null) {
                    localObject2 = "";
                  }
                  localObject3 = "videoUrl";
                  c.g.b.k.a(localObject8, (String)localObject3);
                  ((j)localObject1).d((String)localObject8, (String)localObject2);
                }
              }
            }
            else
            {
              localObject3 = "image";
              paramBoolean = ((String)localObject8).equals(localObject3);
              if (paramBoolean)
              {
                localObject8 = o;
                localObject3 = "featureShareImageInFlash";
                paramBoolean = ((com.truecaller.common.g.a)localObject8).b((String)localObject3);
                if (paramBoolean)
                {
                  localObject8 = this.k;
                  j = R.attr.colorPrimaryDark;
                  paramBoolean = ((com.truecaller.flashsdk.assist.a)localObject8).b(j);
                  ((j)localObject1).g(paramBoolean);
                  paramBoolean = R.attr.theme_bg_contact_transparent_header;
                  localObject3 = this.k;
                  n = R.color.white;
                  j = ((com.truecaller.flashsdk.assist.a)localObject3).a(n);
                  ((j)localObject1).a(paramBoolean, j);
                  ((j)localObject1).S();
                }
                a((Payload)localObject2);
              }
            }
          }
        }
        else
        {
          a((Payload)localObject2, paramBoolean);
        }
        Object localObject8 = p;
        if (localObject8 != null) {
          localObject4 = ((QueuedFlash)localObject8).s();
        }
        if (localObject4 != null)
        {
          localObject8 = p;
          if (localObject8 != null)
          {
            localObject8 = ((QueuedFlash)localObject8).s();
            if (localObject8 != null)
            {
              localObject1 = (j)a;
              if (localObject1 == null) {
                return;
              }
              localObject2 = ((Payload)localObject8).a();
              if (localObject2 == null) {
                return;
              }
              j = ((String)localObject2).hashCode();
              k = 3556653;
              if (j != k)
              {
                if (j != m) {
                  break label1237;
                }
                localObject1 = "location";
                boolean bool6 = ((String)localObject2).equals(localObject1);
                if (!bool6) {
                  break label1237;
                }
                b((Payload)localObject8, bool3);
                return;
              }
              localObject3 = "text";
              boolean bool7 = ((String)localObject2).equals(localObject3);
              if (!bool7) {
                break label1237;
              }
              localObject8 = ((Payload)localObject8).b();
              localObject2 = "payload.message";
              c.g.b.k.a(localObject8, (String)localObject2);
              ((j)localObject1).j((String)localObject8);
              break label1237;
            }
          }
          return;
        }
        label1237:
        return;
      }
    }
  }
  
  private final void t()
  {
    Object localObject1 = o;
    Object localObject2 = "featureShareImageInFlash";
    boolean bool = ((com.truecaller.common.g.a)localObject1).b((String)localObject2);
    if (bool)
    {
      localObject1 = (j)a;
      if (localObject1 != null)
      {
        ((j)localObject1).T();
        ((j)localObject1).P();
        int i = R.attr.theme_bg_contact_header;
        com.truecaller.flashsdk.assist.a locala = this.k;
        int j = R.attr.theme_incoming_text;
        int k = locala.b(j);
        ((j)localObject1).a(i, k);
        localObject2 = this.i;
        k = R.drawable.bg_solid_white_rad_24dp;
        localObject2 = ((al)localObject2).a(k);
        ((j)localObject1).b((Drawable)localObject2);
        localObject2 = this.i;
        k = R.string.type_a_flash;
        Object[] arrayOfObject = new Object[0];
        localObject2 = ((al)localObject2).a(k, arrayOfObject);
        ((j)localObject1).e((String)localObject2);
        return;
      }
    }
  }
  
  private final void u()
  {
    Object localObject1 = "downloadImage";
    a("ANDROID_FLASH_DOWNLOAD_IMAGE", (String)localObject1);
    Object localObject2 = p;
    if (localObject2 != null)
    {
      localObject1 = (j)a;
      if (localObject1 != null)
      {
        localObject2 = (Flash)localObject2;
        ((j)localObject1).e((Flash)localObject2);
        return;
      }
      return;
    }
  }
  
  private final void v()
  {
    j localj = (j)a;
    if (localj == null) {
      return;
    }
    Object localObject = q;
    if (localObject == null) {
      return;
    }
    g localg = j;
    boolean bool = localg.a();
    if (bool)
    {
      a((Flash)localObject);
      ((Flash)localObject).j();
      w();
      return;
    }
    localObject = this.i;
    int i = R.string.no_internet;
    Object[] arrayOfObject = new Object[0];
    localObject = ((al)localObject).a(i, arrayOfObject);
    localj.d((String)localObject);
  }
  
  private final void w()
  {
    int i = 1;
    t = i;
    Flash localFlash = q;
    if (localFlash == null) {
      return;
    }
    Object localObject1 = (j)a;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = o;
    Object localObject3 = "featureShareImageInFlash";
    boolean bool1 = ((com.truecaller.common.g.a)localObject2).b((String)localObject3);
    boolean bool2 = H;
    Object localObject4;
    if ((bool2) && (bool1))
    {
      localObject4 = G;
      if (localObject4 != null)
      {
        localObject2 = I;
        if (localObject2 == null)
        {
          localObject2 = new com/truecaller/flashsdk/models/ImageFlash;
          ((ImageFlash)localObject2).<init>();
          ((ImageFlash)localObject2).a(localFlash);
          ((ImageFlash)localObject2).a((Uri)localObject4);
          localObject4 = "reply";
          ((ImageFlash)localObject2).d((String)localObject4);
        }
        ((j)localObject1).a((ImageFlash)localObject2);
        ((j)localObject1).I();
        localObject4 = this.i;
        int m = R.string.flash_uploading_media;
        localObject2 = new Object[0];
        localObject4 = ((al)localObject4).a(m, (Object[])localObject2);
        ((j)localObject1).a((String)localObject4, false);
        if (localObject1 != null) {}
      }
      else
      {
        localObject4 = x.a;
      }
      return;
    }
    if (bool1)
    {
      boolean bool3 = L;
      if (bool3)
      {
        localObject1 = b;
        if (localObject1 == null) {
          return;
        }
        localObject1 = (CharSequence)localObject1;
        localObject2 = ",";
        localObject3 = new c/n/k;
        ((c.n.k)localObject3).<init>((String)localObject2);
        localObject1 = ((c.n.k)localObject3).a((CharSequence)localObject1, 0);
        bool1 = ((List)localObject1).isEmpty();
        int j;
        int k;
        if (!bool1)
        {
          j = ((List)localObject1).size();
          localObject2 = ((List)localObject1).listIterator(j);
          do
          {
            bool2 = ((ListIterator)localObject2).hasPrevious();
            if (!bool2) {
              break;
            }
            localObject3 = (CharSequence)((ListIterator)localObject2).previous();
            k = ((CharSequence)localObject3).length();
            if (k == 0)
            {
              k = 1;
            }
            else
            {
              k = 0;
              localObject3 = null;
            }
          } while (k != 0);
          localObject1 = (Iterable)localObject1;
          j = ((ListIterator)localObject2).nextIndex() + i;
          localObject1 = c.a.m.d((Iterable)localObject1, j);
        }
        else
        {
          localObject1 = (List)c.a.y.a;
        }
        localObject1 = (Collection)localObject1;
        if (localObject1 != null)
        {
          localObject2 = new String[0];
          localObject1 = ((Collection)localObject1).toArray((Object[])localObject2);
          if (localObject1 != null)
          {
            localObject1 = (String[])localObject1;
            j = localObject1.length;
            k = 2;
            if (j < k) {
              return;
            }
            localObject2 = c;
            localObject3 = Double.valueOf(Double.parseDouble(localObject1[0]));
            double d = Double.parseDouble(localObject1[i]);
            localObject4 = Double.valueOf(d);
            localObject1 = localFlash.f();
            c.g.b.k.a(localObject1, "replyFlashCopy.payload");
            localObject1 = ((Payload)localObject1).b();
            FlashLocationExtras localFlashLocationExtras = new com/truecaller/flashsdk/models/FlashLocationExtras;
            localFlashLocationExtras.<init>((String)localObject2, (Double)localObject3, (Double)localObject4, (String)localObject1);
            localObject4 = new com/truecaller/flashsdk/models/FlashExtras;
            int n = 3;
            ((FlashExtras)localObject4).<init>(null, null, localFlashLocationExtras, n, null);
            localObject1 = localFlash.f();
            c.g.b.k.a(localObject1, "replyFlashCopy.payload");
            localObject2 = P;
            localObject4 = ((com.google.gson.f)localObject2).b(localObject4);
            ((Payload)localObject1).b((String)localObject4);
            k();
            L = false;
          }
          else
          {
            localObject4 = new c/u;
            ((u)localObject4).<init>("null cannot be cast to non-null type kotlin.Array<T>");
            throw ((Throwable)localObject4);
          }
        }
        else
        {
          localObject4 = new c/u;
          ((u)localObject4).<init>("null cannot be cast to non-null type java.util.Collection<T>");
          throw ((Throwable)localObject4);
        }
      }
    }
    D.a(localFlash);
    x();
  }
  
  private final void x()
  {
    Object localObject1 = q;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = (j)a;
    if (localObject2 == null) {
      return;
    }
    Object localObject3 = o;
    boolean bool1 = ((com.truecaller.common.g.a)localObject3).b("featureShareImageInFlash");
    Object[] arrayOfObject = null;
    if (bool1)
    {
      H = false;
      localObject3 = null;
      G = null;
      ((j)localObject2).ak();
      i = R.attr.theme_bg_contact_header;
      localObject4 = this.k;
      int j = R.attr.theme_incoming_text;
      int k = ((com.truecaller.flashsdk.assist.a)localObject4).b(j);
      ((j)localObject2).a(i, k);
    }
    z();
    int i = 1;
    y = i;
    localObject3 = "ANDROID_FLASH_CLOSE_WAITING";
    Object localObject4 = new android/os/Bundle;
    ((Bundle)localObject4).<init>();
    Object localObject5 = "flash_context";
    boolean bool2 = y;
    String str;
    if (bool2) {
      str = "waiting";
    } else {
      str = "reply";
    }
    ((Bundle)localObject4).putString((String)localObject5, str);
    localObject5 = D;
    ((b)localObject5).a((String)localObject3, (Bundle)localObject4);
    localObject1 = ((Flash)localObject1).e();
    c.g.b.k.a(localObject1, "replyFlashCopy.history");
    localObject1 = b((String)localObject1);
    localObject3 = p;
    if (localObject3 != null)
    {
      localObject3 = ((QueuedFlash)localObject3).a();
      if (localObject3 != null)
      {
        localObject3 = ((Sender)localObject3).a();
        if (localObject3 != null)
        {
          long l = ((Long)localObject3).longValue();
          localObject3 = u;
          if (localObject3 == null) {
            return;
          }
          ((j)localObject2).a((String)localObject1, l, (String)localObject3);
          localObject1 = h;
          localObject3 = p;
          if (localObject3 != null)
          {
            localObject3 = ((QueuedFlash)localObject3).a();
            if (localObject3 != null)
            {
              localObject3 = ((Sender)localObject3).a();
              if (localObject3 != null)
              {
                l = ((Long)localObject3).longValue();
                ((ae)localObject1).b(l);
                localObject1 = p;
                if (localObject1 != null)
                {
                  localObject1 = (Flash)localObject1;
                  ((j)localObject2).d((Flash)localObject1);
                  localObject1 = this.i;
                  i = R.string.flash_sent_to;
                  arrayOfObject = new Object[0];
                  localObject1 = ((al)localObject1).a(i, arrayOfObject);
                  localObject3 = u;
                  if (localObject3 == null) {
                    return;
                  }
                  ((j)localObject2).a((String)localObject1, (String)localObject3);
                  localObject1 = p;
                  if (localObject1 != null)
                  {
                    localObject1 = ((QueuedFlash)localObject1).f();
                    if (localObject1 != null)
                    {
                      localObject2 = o;
                      localObject3 = "featureShareImageInFlash";
                      boolean bool3 = ((com.truecaller.common.g.a)localObject2).b((String)localObject3);
                      if (bool3)
                      {
                        localObject1 = ((Payload)localObject1).a();
                        localObject2 = "image";
                        boolean bool4 = c.g.b.k.a(localObject1, localObject2);
                        if (bool4)
                        {
                          localObject1 = (j)a;
                          if (localObject1 != null)
                          {
                            ((j)localObject1).ah();
                            return;
                          }
                        }
                      }
                      return;
                    }
                  }
                  return;
                }
                return;
              }
            }
          }
          return;
        }
      }
    }
  }
  
  private final void y()
  {
    j localj = (j)a;
    if (localj == null) {
      return;
    }
    g localg = j;
    boolean bool = localg.g();
    if (bool)
    {
      localj.ag();
    }
    else
    {
      int i = 11;
      localj.d(i);
    }
    a("ANDROID_FLASH_SHARE_IMAGE", "shareImage");
  }
  
  private final void z()
  {
    Object localObject1 = q;
    if (localObject1 == null) {
      return;
    }
    boolean bool1 = b((Flash)localObject1);
    if (bool1)
    {
      Bundle localBundle = new android/os/Bundle;
      localBundle.<init>();
      Object localObject2 = p;
      if (localObject2 != null)
      {
        localObject2 = ((QueuedFlash)localObject2).a();
        if (localObject2 != null)
        {
          localObject2 = ((Sender)localObject2).a();
          if (localObject2 != null)
          {
            long l = ((Long)localObject2).longValue();
            localObject2 = String.valueOf(l);
            if (localObject2 != null) {
              break label85;
            }
          }
        }
      }
      localObject2 = "";
      label85:
      Object localObject3 = j;
      boolean bool2 = ((g)localObject3).f();
      if (bool2)
      {
        localObject3 = O;
        bool2 = ((com.truecaller.flashsdk.assist.d)localObject3).a((String)localObject2);
      }
      else
      {
        bool2 = false;
        localObject3 = null;
      }
      String str1 = "type";
      String str2 = null;
      boolean bool3 = TextUtils.isEmpty(null);
      if (!bool3) {}
      do
      {
        bool3 = false;
        localObject4 = null;
        break;
        localObject4 = ((Flash)localObject1).f();
      } while (localObject4 == null);
      Object localObject4 = ((Payload)localObject4).a();
      localBundle.putString(str1, (String)localObject4);
      localBundle.putString("flash_receiver_id", (String)localObject2);
      str1 = "reply";
      localBundle.putString("flash_context", str1);
      localBundle.putBoolean("flash_from_phonebook", bool2);
      localObject3 = ((Flash)localObject1).h();
      localBundle.putString("flash_message_id", (String)localObject3);
      localObject2 = "flash_reply_id";
      localObject3 = p;
      if (localObject3 != null) {
        str2 = ((QueuedFlash)localObject3).h();
      }
      localBundle.putString((String)localObject2, str2);
      localObject3 = ((Flash)localObject1).c();
      localBundle.putString("flash_thread_id", (String)localObject3);
      localObject3 = String.valueOf(g);
      localBundle.putString("FlashFromHistory", (String)localObject3);
      localObject2 = "history_length";
      localObject3 = (CharSequence)((Flash)localObject1).e();
      bool2 = TextUtils.isEmpty((CharSequence)localObject3);
      if (!bool2)
      {
        int i = ((Flash)localObject1).e().length() / 2;
        localObject1 = String.valueOf(i);
      }
      else
      {
        localObject1 = "0";
      }
      localBundle.putString((String)localObject2, (String)localObject1);
      localObject1 = D;
      localObject2 = "ANDROID_FLASH_REPLIED";
      ((b)localObject1).a((String)localObject2, localBundle);
      g = false;
    }
  }
  
  public final void a()
  {
    super.a();
    boolean bool = t;
    if (!bool)
    {
      j localj = (j)a;
      if (localj == null) {
        return;
      }
      Object localObject = p;
      if (localObject != null)
      {
        localObject = (Flash)localObject;
        localj.b((Flash)localObject);
      }
      else
      {
        return;
      }
    }
    p = null;
    q = null;
    r = false;
    b = null;
    c = null;
    s = 0;
    t = false;
    u = null;
    v = false;
    w = false;
    x = false;
    L = false;
    G = null;
  }
  
  public final void a(int paramInt, String paramString)
  {
    c.g.b.k.b(paramString, "action");
    Flash localFlash = q;
    if (localFlash == null) {
      return;
    }
    Object localObject1 = (j)a;
    if (localObject1 == null) {
      return;
    }
    localObject1 = new c/g/b/v$c;
    ((v.c)localObject1).<init>();
    Object localObject2 = "";
    Object localObject3 = E;
    Object localObject4 = null;
    boolean bool3;
    if (localObject3 != null)
    {
      localObject5 = localObject3;
      localObject5 = (Collection)localObject3;
      boolean bool1 = ((Collection)localObject5).isEmpty();
      int j = 1;
      bool1 ^= j;
      if (!bool1)
      {
        bool3 = false;
        localObject3 = null;
      }
      if (localObject3 != null)
      {
        paramString = "final";
        localFlash.a(paramString);
        int m = R.id.btnYes;
        if (paramInt == m)
        {
          localObject6 = (ReplyActionsItem)((List)localObject3).get(0);
          a((ReplyActionsItem)localObject6);
          return;
        }
        m = R.id.btnNo;
        if (paramInt == m)
        {
          localObject6 = (ReplyActionsItem)((List)localObject3).get(j);
          a((ReplyActionsItem)localObject6);
          return;
        }
        m = R.id.btnOk;
        if (paramInt == m)
        {
          paramInt = 2;
          localObject6 = (ReplyActionsItem)((List)localObject3).get(paramInt);
          a((ReplyActionsItem)localObject6);
        }
        return;
      }
    }
    Object localObject6 = this;
    localObject6 = (i)this;
    localObject3 = i;
    int i = R.string.sfc_ok;
    Object localObject7 = new Object[0];
    localObject3 = ((al)localObject3).a(i, (Object[])localObject7);
    Object localObject5 = i;
    int k = R.string.sfc_yes;
    Object localObject8 = new Object[0];
    localObject5 = ((al)localObject5).a(k, (Object[])localObject8);
    localObject7 = i;
    int n = R.string.sfc_no;
    Object[] arrayOfObject = new Object[0];
    localObject7 = ((al)localObject7).a(n, arrayOfObject);
    localObject8 = i;
    int i1 = R.string.sfc_share;
    localObject4 = new Object[0];
    localObject4 = ((al)localObject8).a(i1, (Object[])localObject4);
    boolean bool4 = c.g.b.k.a(paramString, localObject5);
    if (bool4)
    {
      a = "accept";
      paramString = (String)localObject5;
    }
    else
    {
      boolean bool2 = c.g.b.k.a(paramString, localObject3);
      if (bool2)
      {
        a = "ok";
        paramString = (String)localObject3;
      }
      else
      {
        bool3 = c.g.b.k.a(paramString, localObject7);
        if (bool3)
        {
          a = "reject";
          paramString = (String)localObject7;
        }
        else
        {
          bool3 = c.g.b.k.a(paramString, localObject4);
          if (bool3)
          {
            ((i)localObject6).y();
            return;
          }
          localObject3 = p;
          if (localObject3 != null)
          {
            localObject3 = ((QueuedFlash)localObject3).f();
            if (localObject3 != null)
            {
              localObject3 = ((Payload)localObject3).a();
              break label486;
            }
          }
          bool3 = false;
          localObject3 = null;
          label486:
          localObject4 = "tc_pay_request";
          bool3 = c.g.b.k.a(localObject3, localObject4);
          if (bool3)
          {
            a = "busy";
            localFlash.a("final");
            paramString = (String)localObject2;
          }
          else
          {
            localObject2 = "custom_flash";
            a = localObject2;
          }
        }
      }
    }
    localObject2 = new com/truecaller/flashsdk/models/Payload;
    localObject1 = (String)a;
    ((Payload)localObject2).<init>((String)localObject1, paramString, null, null);
    localFlash.a((Payload)localObject2);
    ((i)localObject6).v();
  }
  
  public final void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    c.g.b.k.b(paramArrayOfString, "permissions");
    c.g.b.k.b(paramArrayOfInt, "grantResults");
    super.a(paramInt, paramArrayOfString, paramArrayOfInt);
    j localj = (j)a;
    if (localj == null) {
      return;
    }
    int i = 11;
    int j;
    Object localObject;
    if (paramInt == i)
    {
      paramInt = paramArrayOfInt.length;
      j = 1;
      if (paramInt == 0)
      {
        paramInt = 1;
      }
      else
      {
        paramInt = 0;
        localObject = null;
      }
      paramInt ^= j;
      if (paramInt != 0)
      {
        paramInt = paramArrayOfInt[0];
        if (paramInt == 0) {}
      }
      else
      {
        j = 0;
        paramArrayOfString = null;
      }
      if (j != 0)
      {
        localj.ag();
        return;
      }
      localObject = this.i;
      j = R.string.flash_storage_permission_required;
      paramArrayOfInt = new Object[0];
      localObject = ((al)localObject).a(j, paramArrayOfInt);
      localj.d((String)localObject);
      return;
    }
    i = 13;
    if (paramInt == i)
    {
      localObject = n;
      String[] arrayOfString = M;
      int k = arrayOfString.length;
      arrayOfString = (String[])Arrays.copyOf(arrayOfString, k);
      paramInt = ((l)localObject).a(paramArrayOfString, paramArrayOfInt, arrayOfString);
      if (paramInt != 0)
      {
        u();
        return;
      }
      localObject = this.i;
      j = R.string.flash_save_permission_required;
      paramArrayOfInt = new Object[0];
      localObject = ((al)localObject).a(j, paramArrayOfInt);
      localj.d((String)localObject);
    }
  }
  
  public final void a(Bitmap paramBitmap)
  {
    j localj = (j)a;
    if (localj == null) {
      return;
    }
    if (paramBitmap != null)
    {
      ag localag = (ag)bg.a;
      localObject1 = C;
      Object localObject2 = (c.d.f)ax.b();
      localObject1 = ((c.d.f)localObject1).plus((c.d.f)localObject2);
      localObject2 = new com/truecaller/flashsdk/ui/incoming/i$b;
      ((i.b)localObject2).<init>(this, paramBitmap, localj, null);
      localObject2 = (c.g.a.m)localObject2;
      e.a(localag, (c.d.f)localObject1, (c.g.a.m)localObject2, 2);
      return;
    }
    paramBitmap = this.i;
    int i = R.string.failed_to_share_image;
    Object localObject1 = new Object[0];
    paramBitmap = paramBitmap.a(i, (Object[])localObject1);
    localj.d(paramBitmap);
  }
  
  public final void a(Uri paramUri)
  {
    c.g.b.k.b(paramUri, "uri");
    H = true;
    Object[] arrayOfObject = null;
    L = false;
    G = paramUri;
    j localj = (j)a;
    if (localj != null)
    {
      localj.ai();
      localj.b(paramUri);
      int i = R.attr.theme_bg_contact_transparent_header;
      com.truecaller.flashsdk.assist.a locala = this.k;
      int j = R.color.white;
      int k = locala.a(j);
      localj.a(i, k);
      paramUri = this.i;
      k = R.drawable.flash_round_button_default_v2;
      paramUri = paramUri.a(k);
      localj.b(paramUri);
      paramUri = this.i;
      k = R.string.flash_hint_image_caption;
      arrayOfObject = new Object[0];
      paramUri = paramUri.a(k, arrayOfObject);
      localj.e(paramUri);
      return;
    }
  }
  
  public final void a(Bundle paramBundle, String paramString)
  {
    String str = "action";
    c.g.b.k.b(paramString, str);
    int i = paramString.hashCode();
    int j = -1856010814;
    boolean bool1 = true;
    boolean bool2;
    boolean bool3;
    if (i != j)
    {
      j = 959077621;
      if (i != j)
      {
        int k = 1734842775;
        if (i == k)
        {
          paramBundle = "type_flash_timer_expired";
          bool2 = paramString.equals(paramBundle);
          if (bool2)
          {
            t = bool1;
            paramBundle = (j)a;
            if (paramBundle != null)
            {
              paramBundle.x();
              return;
            }
          }
        }
      }
      else
      {
        str = "type_flash_received";
        bool3 = paramString.equals(str);
        if (bool3)
        {
          if (paramBundle == null) {
            return;
          }
          paramBundle = paramBundle.getParcelable("extra_flash");
          c.g.b.k.a(paramBundle, "flashBundle.getParcelable(EXTRA_FLASH)");
          paramBundle = (Flash)paramBundle;
          paramString = paramBundle.f();
          c.g.b.k.a(paramString, "flashReplied.payload");
          paramString = paramString.a();
          str = "call";
          bool3 = c.g.b.k.a(paramString, str);
          if (bool3)
          {
            paramBundle = paramBundle.a();
            c.g.b.k.a(paramBundle, "flashReplied.sender");
            paramBundle = paramBundle.a();
            paramString = p;
            if (paramString != null)
            {
              paramString = paramString.a();
              if (paramString != null)
              {
                paramString = paramString.a();
                break label221;
              }
            }
            bool3 = false;
            paramString = null;
            label221:
            bool2 = c.g.b.k.a(paramBundle, paramString);
            if (bool2)
            {
              paramBundle = (j)a;
              if (paramBundle != null)
              {
                paramString = this.i;
                i = R.string.calling_you_back;
                Object[] arrayOfObject = new Object[0];
                paramString = paramString.a(i, arrayOfObject);
                paramBundle.k(paramString);
              }
            }
          }
        }
      }
    }
    else
    {
      str = "type_publish_progress";
      bool3 = paramString.equals(str);
      if (bool3)
      {
        bool3 = x;
        if ((!bool3) && (paramBundle != null))
        {
          paramString = "extra_timer_progress";
          bool3 = paramBundle.containsKey(paramString);
          if (bool3)
          {
            paramString = "extra_timer_progress";
            long l1 = -1;
            long l2 = paramBundle.getLong(paramString, l1);
            boolean bool4 = l2 < l1;
            if (bool4)
            {
              int m = (int)l2;
              paramBundle = (j)a;
              if (paramBundle != null) {
                paramBundle.m(m);
              }
              l2 = m;
              l1 = 15000L;
              bool4 = l2 < l1;
              if (bool4)
              {
                bool2 = w;
                if (!bool2)
                {
                  paramBundle = (j)a;
                  if (paramBundle != null)
                  {
                    paramString = u;
                    if (paramString != null)
                    {
                      str = " ";
                      paramString = c.n.m.a(paramString, str);
                      if (paramString != null) {}
                    }
                    else
                    {
                      paramString = "";
                    }
                    paramBundle.g(paramString);
                  }
                  w = bool1;
                }
              }
            }
          }
        }
        return;
      }
    }
  }
  
  public final void a(com.truecaller.flashsdk.a.d paramd)
  {
    c.g.b.k.b(paramd, "emoticon");
    Flash localFlash = q;
    if (localFlash == null) {
      return;
    }
    l.b(2);
    Payload localPayload = new com/truecaller/flashsdk/models/Payload;
    paramd = paramd.a();
    localPayload.<init>("emoji", paramd, null, null);
    localFlash.a(localPayload);
    v();
  }
  
  public final void a(CharSequence paramCharSequence)
  {
    j localj = (j)a;
    if (localj != null)
    {
      boolean bool1 = false;
      if (paramCharSequence != null)
      {
        bool2 = c.n.m.a(paramCharSequence);
        if (!bool2)
        {
          bool2 = false;
          paramCharSequence = null;
          break label43;
        }
      }
      boolean bool2 = true;
      label43:
      if (bool2)
      {
        paramCharSequence = (CharSequence)A;
        if (paramCharSequence != null)
        {
          bool2 = c.n.m.a(paramCharSequence);
          if (!bool2)
          {
            bool2 = false;
            paramCharSequence = null;
            break label85;
          }
        }
        bool2 = true;
        label85:
        if (bool2)
        {
          paramCharSequence = G;
          if (paramCharSequence == null)
          {
            paramCharSequence = b;
            if (paramCharSequence != null)
            {
              paramCharSequence = (CharSequence)d;
              if (paramCharSequence != null)
              {
                bool2 = c.n.m.a(paramCharSequence);
                if (!bool2)
                {
                  bool2 = false;
                  paramCharSequence = null;
                  break label145;
                }
              }
              bool2 = true;
              label145:
              if (bool2) {}
            }
            else
            {
              bool2 = false;
              paramCharSequence = null;
              break label164;
            }
          }
        }
        bool2 = true;
        label164:
        if (!bool2) {}
      }
      else
      {
        bool1 = true;
      }
      localj.d(bool1);
      return;
    }
  }
  
  public final void a(CharSequence paramCharSequence, boolean paramBoolean)
  {
    c.g.b.k.b(paramCharSequence, "messageText");
    Flash localFlash = q;
    if (localFlash == null) {
      return;
    }
    paramCharSequence = paramCharSequence.toString();
    boolean bool = L;
    int i = 1;
    String str = null;
    if (bool)
    {
      localCharSequence = (CharSequence)d;
      if (localCharSequence != null)
      {
        bool = c.n.m.a(localCharSequence);
        if (!bool)
        {
          bool = false;
          localCharSequence = null;
          break label80;
        }
      }
      bool = true;
      label80:
      if (bool) {
        paramBoolean = true;
      }
    }
    bool = false;
    CharSequence localCharSequence = null;
    Object localObject1;
    Object localObject2;
    if (paramBoolean)
    {
      localObject1 = paramCharSequence;
      localObject1 = (CharSequence)paramCharSequence;
      paramBoolean = ((CharSequence)localObject1).length();
      if (paramBoolean)
      {
        i = 0;
        localObject2 = null;
      }
      if (i != 0)
      {
        paramCharSequence = this.i;
        paramBoolean = R.string.flash_shared_via;
        localObject2 = new Object[0];
        paramCharSequence = paramCharSequence.a(paramBoolean, (Object[])localObject2);
      }
      localObject1 = l;
      i = 4;
      ((com.truecaller.flashsdk.d.a)localObject1).b(i);
      localObject1 = new com/truecaller/flashsdk/models/Payload;
      localObject2 = "location";
      str = b;
      ((Payload)localObject1).<init>((String)localObject2, paramCharSequence, null, str);
    }
    else
    {
      paramBoolean = H;
      if (paramBoolean)
      {
        localObject1 = paramCharSequence;
        localObject1 = (CharSequence)paramCharSequence;
        paramBoolean = ((CharSequence)localObject1).length();
        if (paramBoolean)
        {
          i = 0;
          localObject2 = null;
        }
        if (i != 0)
        {
          paramCharSequence = this.i;
          paramBoolean = R.string.flash_shared_via;
          localObject2 = new Object[0];
          paramCharSequence = paramCharSequence.a(paramBoolean, (Object[])localObject2);
        }
        localObject1 = new com/truecaller/flashsdk/models/Payload;
        localObject2 = "image";
        ((Payload)localObject1).<init>((String)localObject2, paramCharSequence, null, null);
      }
      else
      {
        localObject1 = new com/truecaller/flashsdk/models/Payload;
        localObject2 = "text";
        ((Payload)localObject1).<init>((String)localObject2, paramCharSequence, null, null);
      }
    }
    localFlash.a((Payload)localObject1);
    v();
  }
  
  public final void a(String paramString, ImageFlash paramImageFlash)
  {
    j localj = (j)a;
    if (localj == null) {
      return;
    }
    if (paramString == null) {
      return;
    }
    int i = paramString.hashCode();
    int j = -1350001074;
    Object localObject;
    boolean bool2;
    int k;
    if (i != j)
    {
      j = -1223111947;
      if (i != j)
      {
        j = -849991191;
        boolean bool1 = true;
        if (i != j)
        {
          j = 1034431578;
          if (i == j)
          {
            localObject = "state_flash_failed";
            bool2 = paramString.equals(localObject);
            if (bool2)
            {
              if (paramImageFlash == null) {
                return;
              }
              I = paramImageFlash;
              paramString = I;
              if (paramString != null)
              {
                paramString = ((Flash)paramString).f();
                c.g.b.k.a(paramString, "(imageFlashDraft as Flash).payload");
                paramString = paramString.b();
                paramImageFlash = "(imageFlashDraft as Flash).payload.message";
                c.g.b.k.a(paramString, paramImageFlash);
                localj.a(paramString, bool1);
                localj.M();
                paramString = this.i;
                k = R.string.flash_sending_failed;
                localObject = new Object[0];
                paramString = paramString.a(k, (Object[])localObject);
                localj.d(paramString);
              }
              else
              {
                paramString = new c/u;
                paramString.<init>("null cannot be cast to non-null type com.truecaller.flashsdk.models.Flash");
                throw paramString;
              }
            }
          }
        }
        else
        {
          localObject = "state_uploading_failed";
          bool2 = paramString.equals(localObject);
          if (bool2)
          {
            if (paramImageFlash == null) {
              return;
            }
            I = paramImageFlash;
            paramString = I;
            if (paramString != null)
            {
              paramString = ((Flash)paramString).f();
              c.g.b.k.a(paramString, "(imageFlashDraft as Flash).payload");
              paramString = paramString.b();
              c.g.b.k.a(paramString, "(imageFlashDraft as Flash).payload.message");
              localj.a(paramString, bool1);
              paramString = this.i;
              k = R.string.flash_sending_failed;
              localObject = new Object[0];
              paramString = paramString.a(k, (Object[])localObject);
              localj.d(paramString);
              localj.J();
              return;
            }
            paramString = new c/u;
            paramString.<init>("null cannot be cast to non-null type com.truecaller.flashsdk.models.Flash");
            throw paramString;
          }
        }
      }
      else
      {
        paramImageFlash = "state_flash_sent";
        bool2 = paramString.equals(paramImageFlash);
        if (bool2)
        {
          paramString = this.i;
          k = R.string.flash_sent;
          localObject = new Object[0];
          paramString = paramString.a(k, (Object[])localObject);
          localj.a(paramString, false);
          localj.L();
          x();
        }
      }
    }
    else
    {
      paramImageFlash = "state_uploaded";
      bool2 = paramString.equals(paramImageFlash);
      if (bool2)
      {
        paramString = this.i;
        k = R.string.flash_sending_flash;
        localObject = new Object[0];
        paramString = paramString.a(k, (Object[])localObject);
        localj.a(paramString, false);
        localj.K();
        return;
      }
    }
  }
  
  public final boolean a(int paramInt)
  {
    Object localObject1 = (j)a;
    int i = 0;
    Object localObject2 = null;
    if (localObject1 == null) {
      return false;
    }
    int j = R.id.action_block_contact;
    boolean bool1 = true;
    Object localObject3;
    if (paramInt == j)
    {
      localObject3 = this.i;
      i = R.color.truecolor;
      paramInt = ((al)localObject3).b(i);
      ((j)localObject1).i(paramInt);
      return bool1;
    }
    j = R.id.action_view_profile;
    if (paramInt == j)
    {
      b();
      return bool1;
    }
    j = R.id.action_phone_call;
    if (paramInt == j)
    {
      localObject3 = p;
      if (localObject3 != null)
      {
        localObject3 = ((QueuedFlash)localObject3).a();
        if (localObject3 != null)
        {
          localObject3 = ((Sender)localObject3).a();
          if (localObject3 != null)
          {
            long l = ((Long)localObject3).longValue();
            localObject3 = String.valueOf(l);
            if (localObject3 != null)
            {
              Flash localFlash = q;
              if (localFlash == null) {
                return bool1;
              }
              Object localObject4 = this.j;
              boolean bool2 = ((g)localObject4).d();
              if (!bool2)
              {
                localObject4 = new android/content/Intent;
                localObject5 = "android.intent.action.VIEW";
                al localal = this.i;
                int k = R.string.tel_num;
                Object[] arrayOfObject1 = new Object[bool1];
                arrayOfObject1[0] = localObject3;
                localObject3 = Uri.parse(localal.a(k, arrayOfObject1));
                ((Intent)localObject4).<init>((String)localObject5, (Uri)localObject3);
                ((j)localObject1).a((Intent)localObject4);
              }
              else
              {
                localObject4 = new android/content/Intent;
                ((Intent)localObject4).<init>("android.intent.action.CALL");
                localObject5 = this.i;
                m = R.string.tel_num;
                Object[] arrayOfObject2 = new Object[bool1];
                arrayOfObject2[0] = localObject3;
                localObject3 = Uri.parse(((al)localObject5).a(m, arrayOfObject2));
                ((Intent)localObject4).setData((Uri)localObject3);
                ((j)localObject1).a((Intent)localObject4);
              }
              localObject3 = new com/truecaller/flashsdk/models/Payload;
              localObject4 = "call";
              Object localObject5 = this.i;
              int m = R.string.calling_you_back;
              localObject2 = new Object[0];
              localObject2 = ((al)localObject5).a(m, (Object[])localObject2);
              ((Payload)localObject3).<init>((String)localObject4, (String)localObject2, null, null);
              localFlash.a((Payload)localObject3);
              localFlash.a("final");
              localFlash.j();
              D.a(localFlash);
              z();
              t = bool1;
              localObject3 = p;
              if (localObject3 != null)
              {
                localObject3 = (Flash)localObject3;
                ((j)localObject1).d((Flash)localObject3);
                ((j)localObject1).x();
                return bool1;
              }
              return bool1;
            }
          }
        }
      }
      return bool1;
    }
    j = 16908332;
    if (paramInt == j)
    {
      paramInt = H;
      if (paramInt != 0)
      {
        localObject3 = G;
        if (localObject3 != null)
        {
          G = null;
          H = false;
          localObject3 = (j)a;
          if (localObject3 != null) {
            ((j)localObject3).aj();
          }
          t();
          d(false);
          break label682;
        }
      }
      paramInt = L;
      if (paramInt != 0)
      {
        localObject3 = (CharSequence)d;
        if (localObject3 != null)
        {
          paramInt = c.n.m.a((CharSequence)localObject3);
          if (paramInt == 0)
          {
            paramInt = 0;
            localObject3 = null;
            break label573;
          }
        }
        paramInt = 1;
        label573:
        if (paramInt != 0)
        {
          L = false;
          k();
          t();
          d(false);
          break label682;
        }
      }
      paramInt = t;
      if (paramInt == 0)
      {
        localObject3 = new com/truecaller/flashsdk/assist/an;
        localObject1 = this.i;
        j = R.string.sfc_busy;
        localObject2 = new Object[0];
        localObject1 = ((al)localObject1).a(j, (Object[])localObject2);
        localObject2 = "busy";
        ((an)localObject3).<init>((String)localObject1, (String)localObject2);
        a((an)localObject3);
        localObject3 = "ANDROID_FLASH_CLOSE";
        localObject1 = "close";
        a((String)localObject3, (String)localObject1);
      }
      else
      {
        ((j)localObject1).x();
      }
      label682:
      return bool1;
    }
    j = R.id.about;
    if (paramInt == j)
    {
      v = bool1;
      this.l.b(8);
      ((j)localObject1).r();
      return bool1;
    }
    j = R.id.action_download;
    if (paramInt == j)
    {
      localObject3 = this.j;
      paramInt = ((g)localObject3).g();
      if (paramInt != 0)
      {
        u();
      }
      else
      {
        paramInt = 13;
        ((j)localObject1).d(paramInt);
      }
      return bool1;
    }
    return false;
  }
  
  public final boolean a(Intent paramIntent)
  {
    c.g.b.k.b(paramIntent, "viewIntent");
    return paramIntent.hasExtra("flash");
  }
  
  public final boolean a(KeyEvent paramKeyEvent)
  {
    String str = "keyEvent";
    c.g.b.k.b(paramKeyEvent, str);
    int i = paramKeyEvent.getKeyCode();
    int j = 25;
    if (i != j)
    {
      i = paramKeyEvent.getKeyCode();
      int k = 164;
      if (i != k)
      {
        int m = paramKeyEvent.getKeyCode();
        i = 26;
        if (m != i) {
          return false;
        }
      }
    }
    paramKeyEvent = (j)a;
    i = 1;
    if (paramKeyEvent != null)
    {
      Object localObject = p;
      if (localObject != null)
      {
        localObject = (Flash)localObject;
        paramKeyEvent.c((Flash)localObject);
      }
      else
      {
        return i;
      }
    }
    return i;
  }
  
  public final void b()
  {
    Object localObject1 = D;
    Object localObject2 = q;
    if (localObject2 != null)
    {
      long l = ((Flash)localObject2).b();
      localObject2 = String.valueOf(l);
      if (localObject2 != null)
      {
        ((b)localObject1).b((String)localObject2);
        localObject2 = "viewProfile";
        a("ANDROID_FLASH_VIEW_PROFILE", (String)localObject2);
        localObject1 = (j)a;
        if (localObject1 != null)
        {
          ((j)localObject1).x();
          return;
        }
        return;
      }
    }
  }
  
  public final void b(CharSequence paramCharSequence, boolean paramBoolean)
  {
    String str1 = "messageText";
    c.g.b.k.b(paramCharSequence, str1);
    boolean bool1 = v;
    int j = 0;
    String str2 = null;
    if (bool1) {
      v = false;
    }
    int i = paramCharSequence.length();
    if (i == 0) {
      j = 1;
    }
    Object localObject;
    if (j == 0)
    {
      i = 0;
      str1 = null;
      if (paramBoolean)
      {
        localObject = new com/truecaller/flashsdk/models/Payload;
        str2 = "location";
        paramCharSequence = paramCharSequence.toString();
        String str3 = b;
        ((Payload)localObject).<init>(str2, paramCharSequence, null, str3);
      }
      else
      {
        localObject = new com/truecaller/flashsdk/models/Payload;
        str2 = "text";
        paramCharSequence = paramCharSequence.toString();
        ((Payload)localObject).<init>(str2, paramCharSequence, null, null);
      }
      paramCharSequence = p;
      if (paramCharSequence != null) {
        paramCharSequence.b((Payload)localObject);
      }
    }
    boolean bool2 = t;
    if (!bool2)
    {
      paramCharSequence = (j)a;
      if (paramCharSequence != null)
      {
        localObject = p;
        if (localObject != null)
        {
          localObject = (Flash)localObject;
          paramCharSequence.b((Flash)localObject);
        }
        return;
      }
    }
  }
  
  public final void b(boolean paramBoolean)
  {
    boolean bool1 = t;
    if (bool1) {
      return;
    }
    j localj = (j)a;
    if (localj == null) {
      return;
    }
    if (paramBoolean) {
      localj.q();
    }
    paramBoolean = true;
    localj.g(paramBoolean);
    Object localObject = p;
    if (localObject != null)
    {
      localObject = ((QueuedFlash)localObject).f();
      if (localObject != null)
      {
        localObject = ((Payload)localObject).a();
        if (localObject != null)
        {
          String str = "image";
          boolean bool2 = c.g.b.k.a(str, localObject);
          if (!bool2)
          {
            str = "location";
            bool3 = c.g.b.k.a(str, localObject);
            if (!bool3) {}
          }
          else
          {
            localObject = o;
            str = "featureShareImageInFlash";
            bool3 = ((com.truecaller.common.g.a)localObject).b(str);
            if (bool3)
            {
              bool3 = true;
              break label157;
            }
          }
        }
      }
    }
    boolean bool3 = false;
    localObject = null;
    label157:
    if (!bool3) {
      localj.h(paramBoolean);
    }
  }
  
  public final void c()
  {
    j localj = (j)a;
    if (localj != null)
    {
      localj.g(false);
      localj.h(false);
      return;
    }
  }
  
  public final void c(boolean paramBoolean)
  {
    j localj = (j)a;
    if (localj == null) {
      return;
    }
    Object localObject1 = p;
    if (localObject1 != null)
    {
      localObject1 = ((QueuedFlash)localObject1).f();
      if (localObject1 != null)
      {
        String str1 = "image";
        Object localObject2 = ((Payload)localObject1).a();
        boolean bool = c.g.b.k.a(str1, localObject2);
        if (bool) {
          return;
        }
        bool = false;
        str1 = null;
        if (!paramBoolean)
        {
          a((Payload)localObject1, false);
          return;
        }
        Object localObject3 = this.i;
        int i = R.string.geo_loc;
        Object[] arrayOfObject = new Object[2];
        Object localObject4 = ((Payload)localObject1).d();
        c.g.b.k.a(localObject4, "payload.attachment");
        arrayOfObject[0] = localObject4;
        localObject4 = ((Payload)localObject1).d();
        String str2 = "payload.attachment";
        c.g.b.k.a(localObject4, str2);
        int j = 1;
        arrayOfObject[j] = localObject4;
        localObject3 = ((al)localObject3).a(i, arrayOfObject);
        localObject2 = this.i;
        int k = R.string.map_activity;
        localObject4 = new Object[0];
        localObject2 = ((al)localObject2).a(k, (Object[])localObject4);
        paramBoolean = localj.g((String)localObject3, (String)localObject2);
        if (!paramBoolean)
        {
          localObject3 = this.i;
          i = R.string.map_browser;
          arrayOfObject = new Object[j];
          localObject1 = ((Payload)localObject1).d();
          localObject4 = "payload.attachment";
          c.g.b.k.a(localObject1, (String)localObject4);
          arrayOfObject[0] = localObject1;
          localObject3 = ((al)localObject3).a(i, arrayOfObject);
          localj.i((String)localObject3);
        }
        return;
      }
    }
  }
  
  public final void d()
  {
    j localj = (j)a;
    if (localj == null) {
      return;
    }
    localj.af();
    boolean bool = t;
    if (!bool)
    {
      Object localObject = p;
      if (localObject != null)
      {
        localObject = (Flash)localObject;
        localj.b((Flash)localObject);
      }
    }
    else
    {
      localj = null;
      H = false;
    }
    super.d();
  }
  
  public final void f()
  {
    super.f();
    j localj = (j)a;
    if (localj != null)
    {
      localj.X();
      int i = R.drawable.ic_flash_attach_file_24dp;
      int j = R.attr.colorButtonNormal;
      localj.b(i, j);
      return;
    }
  }
  
  public final void g()
  {
    super.g();
    j localj = (j)a;
    if (localj != null)
    {
      int i = R.drawable.ic_flash_close_black_24dp;
      int j = R.attr.colorButtonNormal;
      localj.b(i, j);
      al localal = this.i;
      j = R.color.white;
      i = localal.b(j);
      localj.k(i);
      return;
    }
  }
  
  public final void h()
  {
    boolean bool = v;
    if (bool)
    {
      v = false;
      return;
    }
    bool = t;
    if (!bool)
    {
      j localj = (j)a;
      if (localj != null)
      {
        QueuedFlash localQueuedFlash = p;
        if (localQueuedFlash == null) {
          return;
        }
        localj.a(localQueuedFlash);
        return;
      }
    }
  }
  
  public final void j()
  {
    super.j();
    H = false;
    L = true;
  }
  
  public final void k()
  {
    super.k();
    L = false;
    j localj = (j)a;
    if (localj != null)
    {
      localj.F();
      return;
    }
  }
  
  public final void l()
  {
    Object localObject1 = this.l;
    int i = 1;
    ((com.truecaller.flashsdk.d.a)localObject1).b(i);
    localObject1 = (j)a;
    if (localObject1 != null)
    {
      Object localObject2 = p;
      if (localObject2 != null)
      {
        localObject2 = ((QueuedFlash)localObject2).a();
        if (localObject2 != null)
        {
          Object localObject3 = ((Sender)localObject2).b();
          localObject2 = ((Sender)localObject2).a();
          if (localObject2 != null)
          {
            long l = ((Long)localObject2).longValue();
            localObject2 = String.valueOf(l);
            if (localObject2 != null)
            {
              Object localObject4 = j;
              boolean bool = ((g)localObject4).f();
              if (bool)
              {
                localObject4 = O;
                localObject2 = ((com.truecaller.flashsdk.assist.d)localObject4).b((String)localObject2);
                if (localObject2 != null)
                {
                  localObject4 = (CharSequence)((Contact)localObject2).getName();
                  bool = TextUtils.isEmpty((CharSequence)localObject4);
                  if (!bool)
                  {
                    localObject2 = ((Contact)localObject2).getName();
                    localObject3 = localObject2;
                  }
                }
              }
              c.g.b.k.a(localObject3, "name");
              localObject2 = c.n.m.a((String)localObject3, " ");
              break label179;
            }
          }
          localObject2 = ((String)localObject3).toString();
          break label179;
        }
      }
      localObject2 = "";
      label179:
      ((j)localObject1).f((String)localObject2);
      return;
    }
  }
  
  public final boolean m()
  {
    Object localObject = o;
    String str = "featureShareImageInFlash";
    boolean bool = ((com.truecaller.common.g.a)localObject).b(str);
    if (bool)
    {
      localObject = p;
      if (localObject != null)
      {
        localObject = ((QueuedFlash)localObject).f();
        if (localObject != null)
        {
          localObject = ((Payload)localObject).a();
          break label51;
        }
      }
      bool = false;
      localObject = null;
      label51:
      str = "image";
      bool = c.g.b.k.a(localObject, str);
      if (bool)
      {
        localObject = (j)a;
        if (localObject != null) {
          ((j)localObject).am();
        }
      }
      else
      {
        localObject = (j)a;
        if (localObject != null) {
          ((j)localObject).an();
        }
      }
    }
    return true;
  }
  
  public final void n()
  {
    Object localObject1 = new com/truecaller/flashsdk/assist/an;
    Object localObject2 = this.i;
    int i = R.string.sfc_busy;
    Object[] arrayOfObject = new Object[0];
    localObject2 = ((al)localObject2).a(i, arrayOfObject);
    ((an)localObject1).<init>((String)localObject2, "busy");
    a((an)localObject1);
    localObject1 = new java/lang/Thread;
    localObject2 = new com/truecaller/flashsdk/ui/incoming/i$a;
    ((i.a)localObject2).<init>(this);
    localObject2 = (Runnable)localObject2;
    ((Thread)localObject1).<init>((Runnable)localObject2);
    ((Thread)localObject1).setPriority(1);
    ((Thread)localObject1).start();
  }
  
  public final void o()
  {
    j localj = (j)a;
    if (localj == null) {
      return;
    }
    boolean bool = r;
    if (!bool)
    {
      int i = s;
      int j = k.a();
      if (i < j)
      {
        i = s + 1;
        s = i;
        i = k.b();
        localj.l(i);
        return;
      }
    }
    localj.Y();
    localj.Z();
  }
  
  public final void p()
  {
    j localj = (j)a;
    if (localj == null) {
      return;
    }
    boolean bool = true;
    r = bool;
    localj.aa();
    Object localObject = p;
    if (localObject != null)
    {
      localObject = (Flash)localObject;
      localj.c((Flash)localObject);
      return;
    }
  }
  
  public final void q()
  {
    Object localObject1 = (j)a;
    if (localObject1 == null) {
      return;
    }
    x = true;
    ((j)localObject1).ab();
    Object localObject2 = null;
    ((j)localObject1).i(false);
    ((j)localObject1).ac();
    ((j)localObject1).ad();
    QueuedFlash localQueuedFlash = p;
    if (localQueuedFlash != null) {
      localQueuedFlash.a(false);
    }
    localObject2 = p;
    if (localObject2 != null)
    {
      localObject2 = (Flash)localObject2;
      ((j)localObject1).a((Flash)localObject2);
      a("ANDROID_FLASH_OPENED", "opened");
      localObject1 = l;
      localObject2 = a;
      long l1 = ((aa)localObject2).h();
      long l2 = 1L;
      boolean bool = l1 < l2;
      if (!bool)
      {
        l1 = 0L;
      }
      else
      {
        localObject2 = a;
        l1 = ((aa)localObject2).h();
      }
      int i = a.a("receive_tooltips", 15);
      i = com.truecaller.flashsdk.d.a.a(l1, i);
      c(i);
      return;
    }
  }
  
  public final void r()
  {
    boolean bool1 = H;
    boolean bool2;
    if (bool1)
    {
      localObject = G;
      if (localObject != null) {}
    }
    else
    {
      bool1 = L;
      bool2 = true;
      if (!bool1) {
        break label67;
      }
      localObject = (CharSequence)d;
      if (localObject != null)
      {
        bool1 = c.n.m.a((CharSequence)localObject);
        if (!bool1)
        {
          bool1 = false;
          localObject = null;
          break label62;
        }
      }
      bool1 = true;
      label62:
      if (!bool1) {
        break label67;
      }
    }
    return;
    label67:
    bool1 = F ^ bool2;
    F = bool1;
    Object localObject = (j)a;
    if (localObject == null) {
      return;
    }
    ((j)localObject).N();
    boolean bool3 = F;
    if (bool3)
    {
      ((j)localObject).e(bool2);
      ((j)localObject).V();
      return;
    }
    ((j)localObject).e(false);
    ((j)localObject).U();
  }
  
  public final void s()
  {
    int i = 2;
    Object localObject1 = new com.truecaller.flashsdk.ui.customviews.a[i];
    com.truecaller.flashsdk.ui.customviews.a locala = new com/truecaller/flashsdk/ui/customviews/a;
    int j = R.drawable.flash_ic_location__selected_24dp;
    int k = R.string.sfc_location;
    Object localObject2 = this.k;
    int m = R.attr.theme_flash_attach_button_tint;
    int n = ((com.truecaller.flashsdk.assist.a)localObject2).b(m);
    localObject2 = locala;
    locala.<init>(0, j, k, n, (byte)0);
    localObject1[0] = locala;
    localObject2 = new com/truecaller/flashsdk/ui/customviews/a;
    int i1 = R.drawable.flash_ic_photo_camera_24dp;
    int i2 = R.string.flash_attach_camera;
    com.truecaller.flashsdk.assist.a locala1 = this.k;
    j = R.attr.theme_flash_attach_button_tint;
    int i3 = locala1.b(j);
    int i4 = 1;
    ((com.truecaller.flashsdk.ui.customviews.a)localObject2).<init>(i4, i1, i2, i3, (byte)0);
    m = 1;
    localObject1[m] = localObject2;
    localObject1 = c.a.m.c((Object[])localObject1);
    localObject2 = (j)a;
    if (localObject2 != null)
    {
      ((j)localObject2).c((List)localObject1);
      i = R.drawable.ic_flash_attach_file_24dp;
      m = R.attr.colorButtonNormal;
      ((j)localObject2).c(i, m);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.incoming.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */