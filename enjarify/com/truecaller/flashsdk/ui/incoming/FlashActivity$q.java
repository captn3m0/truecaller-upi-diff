package com.truecaller.flashsdk.ui.incoming;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import c.g.b.k;
import com.d.b.ag;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.assist.t;

public final class FlashActivity$q
  implements ag
{
  FlashActivity$q(FlashActivity paramFlashActivity) {}
  
  public final void a(Bitmap paramBitmap)
  {
    if (paramBitmap != null)
    {
      int i = paramBitmap.getWidth();
      int j = paramBitmap.getHeight();
      int k = 17;
      int m = -1;
      Object localObject2;
      String str;
      if (i > j)
      {
        localObject1 = new android/widget/FrameLayout$LayoutParams;
        ((FrameLayout.LayoutParams)localObject1).<init>(m, -2);
        gravity = k;
        j = 100;
        ((FrameLayout.LayoutParams)localObject1).setMargins(0, 0, 0, j);
        localObject2 = a;
        k = R.id.imageContentV2;
        localObject2 = (ImageView)((FlashActivity)localObject2).e(k);
        str = "imageContentV2";
        k.a(localObject2, str);
        localObject1 = (ViewGroup.LayoutParams)localObject1;
        ((ImageView)localObject2).setLayoutParams((ViewGroup.LayoutParams)localObject1);
        localObject1 = FlashActivity.f(a);
        if (localObject1 != null) {
          t.a((ImageView)localObject1, paramBitmap);
        }
        localObject1 = a;
        j = R.id.imageContentV2;
        localObject1 = (ImageView)((FlashActivity)localObject1).e(j);
        k.a(localObject1, "imageContentV2");
        localObject2 = ImageView.ScaleType.FIT_XY;
        ((ImageView)localObject1).setScaleType((ImageView.ScaleType)localObject2);
      }
      else
      {
        localObject1 = a;
        j = R.id.imageContentV2;
        localObject1 = (ImageView)((FlashActivity)localObject1).e(j);
        k.a(localObject1, "imageContentV2");
        localObject2 = ImageView.ScaleType.CENTER_CROP;
        ((ImageView)localObject1).setScaleType((ImageView.ScaleType)localObject2);
        localObject1 = new android/widget/FrameLayout$LayoutParams;
        ((FrameLayout.LayoutParams)localObject1).<init>(m, m);
        gravity = k;
        ((FrameLayout.LayoutParams)localObject1).setMargins(0, 0, 0, 0);
        localObject2 = a;
        k = R.id.imageContentV2;
        localObject2 = (ImageView)((FlashActivity)localObject2).e(k);
        str = "imageContentV2";
        k.a(localObject2, str);
        localObject1 = (ViewGroup.LayoutParams)localObject1;
        ((ImageView)localObject2).setLayoutParams((ViewGroup.LayoutParams)localObject1);
      }
      Object localObject1 = a;
      j = R.id.imageContentV2;
      ((ImageView)((FlashActivity)localObject1).e(j)).setImageBitmap(paramBitmap);
      localObject1 = a;
      FlashActivity.a((FlashActivity)localObject1, paramBitmap);
    }
  }
  
  public final void a(Drawable paramDrawable)
  {
    FlashActivity localFlashActivity = a;
    int i = R.id.imageContentV2;
    ((ImageView)localFlashActivity.e(i)).setImageDrawable(paramDrawable);
  }
  
  public final void b(Drawable paramDrawable)
  {
    FlashActivity localFlashActivity = a;
    int i = R.id.imageContentV2;
    ((ImageView)localFlashActivity.e(i)).setImageDrawable(paramDrawable);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.incoming.FlashActivity.q
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */