package com.truecaller.flashsdk.ui.incoming;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import c.g.b.k;
import com.truecaller.flashsdk.models.ImageFlash;

public final class FlashActivity$r
  extends BroadcastReceiver
{
  FlashActivity$r(FlashActivity paramFlashActivity) {}
  
  public final void onReceive(Context paramContext, Intent paramIntent)
  {
    String str = "context";
    k.b(paramContext, str);
    k.b(paramIntent, "intent");
    paramContext = paramIntent.getExtras();
    if (paramContext != null)
    {
      paramIntent = (h)a.c();
      str = paramContext.getString("extra_state");
      paramContext = (ImageFlash)paramContext.getParcelable("extra_image_flash");
      paramIntent.a(str, paramContext);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.incoming.FlashActivity.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */