package com.truecaller.flashsdk.ui.incoming;

import android.app.WallpaperManager;
import android.content.Context;
import c.g.b.k;
import com.truecaller.flashsdk.assist.aa;
import com.truecaller.flashsdk.assist.ae;
import com.truecaller.flashsdk.assist.al;
import com.truecaller.flashsdk.assist.b;
import com.truecaller.flashsdk.assist.d;
import com.truecaller.flashsdk.assist.g;
import com.truecaller.flashsdk.assist.j;
import com.truecaller.flashsdk.assist.q;
import com.truecaller.flashsdk.assist.y;
import com.truecaller.utils.l;

public final class c
{
  final FlashActivity a;
  
  public c(FlashActivity paramFlashActivity)
  {
    a = paramFlashActivity;
  }
  
  public static WallpaperManager a(FlashActivity paramFlashActivity)
  {
    k.b(paramFlashActivity, "activity");
    paramFlashActivity = WallpaperManager.getInstance((Context)paramFlashActivity);
    k.a(paramFlashActivity, "WallpaperManager.getInstance(activity)");
    return paramFlashActivity;
  }
  
  public static h a(c.d.f paramf, com.google.firebase.messaging.a parama, ae paramae, aa paramaa, al paramal, g paramg, d paramd, com.truecaller.flashsdk.assist.a parama1, com.truecaller.flashsdk.d.a parama2, y paramy, com.google.gson.f paramf1, j paramj, q paramq, l paraml, com.truecaller.common.g.a parama3)
  {
    k.b(paramf, "uiContext");
    k.b(parama, "firebaseMessaging");
    k.b(paramae, "recentEmojiManager");
    k.b(paramaa, "preferenceUtil");
    k.b(paramal, "resourceProvider");
    k.b(paramg, "deviceUtils");
    k.b(paramd, "contactUtils");
    k.b(parama1, "colorProvider");
    k.b(parama2, "toolTipsManager");
    k.b(paramy, "locationFormatter");
    k.b(paramf1, "gson");
    k.b(paramj, "fileUtils");
    k.b(paramq, "mediaHelper");
    k.b(paraml, "permissionUtil");
    k.b(parama3, "coreSettings");
    i locali = new com/truecaller/flashsdk/ui/incoming/i;
    locali.<init>(paramf, parama, paramae, paramaa, paramal, paramg, paramd, parama1, parama2, paramy, paramf1, paramj, paramq, paraml, parama3);
    return (h)locali;
  }
  
  public static com.truecaller.flashsdk.assist.a b(FlashActivity paramFlashActivity)
  {
    k.b(paramFlashActivity, "activity");
    b localb = new com/truecaller/flashsdk/assist/b;
    paramFlashActivity = (Context)paramFlashActivity;
    localb.<init>(paramFlashActivity);
    return (com.truecaller.flashsdk.assist.a)localb;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.incoming.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */