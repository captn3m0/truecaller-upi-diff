package com.truecaller.flashsdk.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;

public final class a
  extends AppCompatDialog
{
  ListAdapter a;
  private ListView b;
  private final Context c;
  
  public a(Context paramContext, int paramInt)
  {
    super(paramContext, paramInt);
    c = paramContext;
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = R.layout.flash_contact_picker;
    setContentView(i);
    i = R.id.listView;
    paramBundle = (ListView)findViewById(i);
    b = paramBundle;
    paramBundle = b;
    if (paramBundle != null)
    {
      ListAdapter localListAdapter = a;
      paramBundle.setAdapter(localListAdapter);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */