package com.truecaller.flashsdk.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import c.g.b.k;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;

public final class FlashSendFooterView
  extends b
  implements View.OnClickListener
{
  public FlashSendFooterView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private FlashSendFooterView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, '\000');
  }
  
  public final int getLayoutResource()
  {
    return R.layout.layout_send_flash_footer;
  }
  
  public final void k()
  {
    getSendLocation().setVisibility(8);
  }
  
  public final void onClick(View paramView)
  {
    String str = "v";
    k.b(paramView, str);
    int i = paramView.getId();
    int j = R.id.sendMessage;
    if (i == j)
    {
      paramView = (FlashSendFooterView.a)getActionListener();
      if (paramView != null) {
        paramView.N_();
      }
      return;
    }
    super.onClick(paramView);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.customviews.FlashSendFooterView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */