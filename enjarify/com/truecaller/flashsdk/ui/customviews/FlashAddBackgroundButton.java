package com.truecaller.flashsdk.ui.customviews;

import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;

public final class FlashAddBackgroundButton
  extends Toolbar
  implements View.OnClickListener
{
  public final TextView a;
  public final ImageView b;
  private FlashAddBackgroundButton.a c;
  
  public FlashAddBackgroundButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private FlashAddBackgroundButton(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    paramContext = LayoutInflater.from(paramContext);
    int i = R.layout.flash_add_background_button;
    Object localObject = this;
    localObject = (ViewGroup)this;
    paramContext.inflate(i, (ViewGroup)localObject, true);
    int j = R.id.addPhotoButton;
    paramContext = findViewById(j);
    k.a(paramContext, "findViewById(R.id.addPhotoButton)");
    paramContext = (ImageView)paramContext;
    b = paramContext;
    j = R.id.attachText;
    paramContext = findViewById(j);
    k.a(paramContext, "findViewById(R.id.attachText)");
    paramContext = (TextView)paramContext;
    a = paramContext;
  }
  
  public final FlashAddBackgroundButton.a getContactClickListener$flash_release()
  {
    return c;
  }
  
  public final void onClick(View paramView)
  {
    k.b(paramView, "v");
  }
  
  public final void setContactClickListener$flash_release(FlashAddBackgroundButton.a parama)
  {
    c = parama;
  }
  
  public final void setText(String paramString)
  {
    k.b(paramString, "title");
    TextView localTextView = a;
    paramString = (CharSequence)paramString;
    localTextView.setText(paramString);
  }
  
  public final void setTextColor(int paramInt)
  {
    a.setTextColor(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.customviews.FlashAddBackgroundButton
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */