package com.truecaller.flashsdk.ui.customviews.a;

import android.content.Context;
import android.support.constraint.ConstraintLayout.a;
import c.g.b.k;
import java.util.Random;

public final class i
  extends f
{
  public i(Context paramContext)
  {
    super(paramContext);
  }
  
  public final void a(ConstraintLayout.a parama)
  {
    k.b(parama, "layoutParams");
    f.a locala = getEmojiAttributes$flash_release();
    if (locala == null) {
      return;
    }
    k = 0;
    h = 21;
    float f1 = c;
    float f2 = getRandom().nextFloat();
    f1 *= f2;
    A = f1;
  }
  
  public final void b(ConstraintLayout.a parama)
  {
    k.b(parama, "layoutParams");
    f.a locala = getEmojiAttributes$flash_release();
    if (locala == null) {
      return;
    }
    h = 0;
    k = 21;
    float f1 = e;
    float f2 = e;
    float f3 = getRandom().nextFloat();
    f2 *= f3;
    f1 += f2;
    A = f1;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.customviews.a.i
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */