package com.truecaller.flashsdk.ui.customviews.a;

import android.content.Context;
import android.content.res.Resources;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintLayout.a;
import android.support.constraint.Guideline;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;
import com.truecaller.flashsdk.R.dimen;
import java.util.Random;

public abstract class f
  extends ConstraintLayout
{
  public final Random k;
  public f.a l;
  
  public f(Context paramContext)
  {
    super(paramContext);
    paramContext = new java/util/Random;
    paramContext.<init>();
    k = paramContext;
  }
  
  private final void a(int paramInt1, int paramInt2, float paramFloat, int paramInt3)
  {
    f.a locala = l;
    if (locala == null) {
      return;
    }
    if (paramInt3 > 0)
    {
      int i = 1;
      for (;;)
      {
        Object localObject1 = new android/widget/TextView;
        Object localObject2 = getContext();
        ((TextView)localObject1).<init>((Context)localObject2);
        int j = getChildCount();
        ((TextView)localObject1).setId(j);
        localObject2 = (CharSequence)a;
        ((TextView)localObject1).setText((CharSequence)localObject2);
        float f1 = paramInt2;
        float f2 = 0.0F;
        Random localRandom = null;
        ((TextView)localObject1).setTextSize(0, f1);
        ((TextView)localObject1).setAlpha(paramFloat);
        localObject2 = k;
        f1 = ((Random)localObject2).nextFloat();
        float f3 = -20.0F;
        f1 *= f3;
        int m = i % 2;
        if (m != 0) {
          f1 = -f1;
        }
        ((TextView)localObject1).setRotation(f1);
        localObject2 = new android/support/constraint/ConstraintLayout$a;
        ((ConstraintLayout.a)localObject2).<init>(paramInt1, paramInt1);
        q = 0;
        s = 0;
        localRandom = k;
        f2 = localRandom.nextFloat();
        z = f2;
        a((ConstraintLayout.a)localObject2);
        localObject1 = (View)localObject1;
        localObject2 = (ViewGroup.LayoutParams)localObject2;
        addView((View)localObject1, (ViewGroup.LayoutParams)localObject2);
        if (i == paramInt3) {
          break;
        }
        i += 1;
      }
    }
  }
  
  public abstract void a(ConstraintLayout.a parama);
  
  public abstract void b(ConstraintLayout.a parama);
  
  public final f.a getEmojiAttributes$flash_release()
  {
    return l;
  }
  
  protected final Random getRandom()
  {
    return k;
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    Object localObject1 = l;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = new android/support/constraint/Guideline;
    Object localObject3 = getContext();
    ((Guideline)localObject2).<init>((Context)localObject3);
    ((Guideline)localObject2).setId(21);
    localObject2 = (View)localObject2;
    localObject3 = new android/support/constraint/ConstraintLayout$a;
    ((ConstraintLayout.a)localObject3).<init>(-1, -2);
    float f = b;
    c = f;
    S = 0;
    localObject3 = (ViewGroup.LayoutParams)localObject3;
    addView((View)localObject2, (ViewGroup.LayoutParams)localObject3);
    localObject1 = getResources();
    int i = R.dimen.dp60;
    int j = ((Resources)localObject1).getDimensionPixelSize(i);
    localObject2 = getResources();
    int m = R.dimen.sp40;
    i = ((Resources)localObject2).getDimensionPixelSize(m);
    a(j, i, 1.0F, 4);
    localObject1 = getResources();
    i = R.dimen.dp48;
    j = ((Resources)localObject1).getDimensionPixelSize(i);
    localObject2 = getResources();
    m = R.dimen.sp32;
    i = ((Resources)localObject2).getDimensionPixelSize(m);
    a(j, i, 0.9F, 6);
    localObject1 = getResources();
    i = R.dimen.dp32;
    j = ((Resources)localObject1).getDimensionPixelSize(i);
    localObject2 = getResources();
    m = R.dimen.sp20;
    i = ((Resources)localObject2).getDimensionPixelSize(m);
    a(j, i, 0.8F, 8);
  }
  
  public final void setEmojiAttributes$flash_release(f.a parama)
  {
    l = parama;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.customviews.a.f
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */