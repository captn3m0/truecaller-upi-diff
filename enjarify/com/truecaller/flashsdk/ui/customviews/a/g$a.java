package com.truecaller.flashsdk.ui.customviews.a;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.view.View;
import android.view.ViewGroup;
import c.g.b.k;
import c.u;

final class g$a
  implements ValueAnimator.AnimatorUpdateListener
{
  g$a(View paramView, ViewGroup paramViewGroup) {}
  
  public final void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    View localView = a;
    k.a(localView, "view");
    String str = "it";
    k.a(paramValueAnimator, str);
    paramValueAnimator = paramValueAnimator.getAnimatedValue();
    if (paramValueAnimator != null)
    {
      float f = ((Float)paramValueAnimator).floatValue();
      localView.setRotation(f);
      return;
    }
    paramValueAnimator = new c/u;
    paramValueAnimator.<init>("null cannot be cast to non-null type kotlin.Float");
    throw paramValueAnimator;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.customviews.a.g.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */