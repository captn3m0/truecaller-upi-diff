package com.truecaller.flashsdk.ui.customviews.a;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.Activity;
import android.support.transition.n;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import c.g.b.k;
import java.util.Map;

public final class g
  extends n
{
  private final String n = "emojiView:rotation";
  
  private final void d(android.support.transition.u paramu)
  {
    Map localMap = a;
    k.a(localMap, "values.values");
    String str = n;
    paramu = b;
    k.a(paramu, "values.view");
    paramu = Float.valueOf(paramu.getRotation());
    localMap.put(str, paramu);
  }
  
  public final Animator a(ViewGroup paramViewGroup, android.support.transition.u paramu1, android.support.transition.u paramu2)
  {
    k.b(paramViewGroup, "sceneRoot");
    int i = 0;
    if ((paramu1 != null) && (paramu2 != null))
    {
      View localView = b;
      paramu1 = a;
      Object localObject = n;
      paramu1 = paramu1.get(localObject);
      if (paramu1 != null)
      {
        paramu1 = (Float)paramu1;
        float f1 = paramu1.floatValue();
        paramu2 = a;
        localObject = n;
        paramu2 = paramu2.get(localObject);
        if (paramu2 != null)
        {
          paramu2 = (Float)paramu2;
          float f2 = paramu2.floatValue();
          boolean bool = f1 < f2;
          if (!bool) {
            return null;
          }
          i = 2;
          localObject = new float[i];
          localObject[0] = f1;
          int j = 1;
          f1 = Float.MIN_VALUE;
          localObject[j] = f2;
          paramu1 = ValueAnimator.ofFloat((float[])localObject);
          paramu2 = new com/truecaller/flashsdk/ui/customviews/a/g$a;
          paramu2.<init>(localView, paramViewGroup);
          paramu2 = (ValueAnimator.AnimatorUpdateListener)paramu2;
          paramu1.addUpdateListener(paramu2);
          int k = 8;
          f2 = 1.1E-44F;
          paramu1.setRepeatCount(k);
          paramu1.setRepeatMode(i);
          paramViewGroup = paramViewGroup.getContext();
          if (paramViewGroup != null)
          {
            paramViewGroup = ((Activity)paramViewGroup).getWindow();
            k.a(paramViewGroup, "(sceneRoot.context as Activity).window");
            paramViewGroup = paramViewGroup.getDecorView();
            k.a(paramViewGroup, "(sceneRoot.context as Activity).window.decorView");
            paramViewGroup = paramViewGroup.getRootView();
            k.a(paramViewGroup, "(sceneRoot.context as Ac…window.decorView.rootView");
            long l = paramViewGroup.getHeight() / i;
            paramu1.setDuration(l);
            return (Animator)paramu1;
          }
          paramViewGroup = new c/u;
          paramViewGroup.<init>("null cannot be cast to non-null type android.app.Activity");
          throw paramViewGroup;
        }
        paramViewGroup = new c/u;
        paramViewGroup.<init>("null cannot be cast to non-null type kotlin.Float");
        throw paramViewGroup;
      }
      paramViewGroup = new c/u;
      paramViewGroup.<init>("null cannot be cast to non-null type kotlin.Float");
      throw paramViewGroup;
    }
    return null;
  }
  
  public final void a(android.support.transition.u paramu)
  {
    k.b(paramu, "transitionValues");
    d(paramu);
  }
  
  public final void b(android.support.transition.u paramu)
  {
    k.b(paramu, "transitionValues");
    d(paramu);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.customviews.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */