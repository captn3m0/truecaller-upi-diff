package com.truecaller.flashsdk.ui.customviews;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.view.r;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import c.a.m;
import c.g.b.k;
import c.u;
import c.x;
import com.truecaller.common.h.l;
import com.truecaller.flashsdk.R.drawable;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public final class FlashAttachButton
  extends FrameLayout
  implements View.OnClickListener
{
  public boolean a;
  public boolean b;
  public FlashAttachButton.b c;
  public ImageView d;
  public LinearLayout e;
  public View f;
  public boolean g;
  private List h;
  private FlashAttachButton.a i;
  
  public FlashAttachButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private FlashAttachButton(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
  }
  
  private final void a()
  {
    boolean bool1 = a;
    if (bool1)
    {
      Object localObject1 = h;
      if (localObject1 != null)
      {
        boolean bool2 = b;
        if (!bool2)
        {
          if (localObject1 == null) {
            return;
          }
          bool2 = true;
          b = bool2;
          Object localObject2 = e;
          if (localObject2 != null)
          {
            int k = ((LinearLayout)localObject2).getChildCount();
            int m = 0;
            while (m < k)
            {
              View localView = ((LinearLayout)localObject2).getChildAt(m);
              String str = "view";
              k.a(localView, str);
              a(false, localView, m);
              m += 1;
            }
            Object localObject3 = new com/truecaller/flashsdk/ui/customviews/FlashAttachButton$e;
            ((FlashAttachButton.e)localObject3).<init>((LinearLayout)localObject2);
            localObject3 = (Runnable)localObject3;
            int j = ((List)localObject1).size() * 300 / 4 + 300;
            long l1 = j;
            postDelayed((Runnable)localObject3, l1);
            a(false);
            localObject1 = d;
            if (localObject1 != null)
            {
              localObject1 = ((ImageView)localObject1).animate();
              bool2 = false;
              localObject1 = ((ViewPropertyAnimator)localObject1).rotation(0.0F);
              localObject2 = new android/view/animation/OvershootInterpolator;
              ((OvershootInterpolator)localObject2).<init>();
              localObject2 = (TimeInterpolator)localObject2;
              localObject1 = ((ViewPropertyAnimator)localObject1).setInterpolator((TimeInterpolator)localObject2);
              localObject2 = "animate()\n              …(OvershootInterpolator())";
              k.a(localObject1, (String)localObject2);
              long l2 = 300L;
              ((ViewPropertyAnimator)localObject1).setDuration(l2);
            }
            a = false;
            localObject1 = i;
            if (localObject1 != null)
            {
              ((FlashAttachButton.a)localObject1).L_();
              return;
            }
            return;
          }
          return;
        }
      }
    }
  }
  
  private final void a(int paramInt1, int paramInt2)
  {
    int j = Build.VERSION.SDK_INT;
    int k = 21;
    if (j >= k)
    {
      b(paramInt1, paramInt2);
      return;
    }
    b();
  }
  
  private final void a(int paramInt1, int paramInt2, Animator.AnimatorListener paramAnimatorListener)
  {
    int j = Build.VERSION.SDK_INT;
    int k = 21;
    if (j >= k)
    {
      b(paramInt1, paramInt2, paramAnimatorListener);
      return;
    }
    a(paramAnimatorListener);
  }
  
  private final void a(Animator.AnimatorListener paramAnimatorListener)
  {
    Object localObject1 = f;
    if (localObject1 != null)
    {
      ((View)localObject1).setAlpha(1.0F);
      localObject1 = ((View)localObject1).animate().alpha(0.0F);
      Object localObject2 = new android/view/animation/AccelerateDecelerateInterpolator;
      ((AccelerateDecelerateInterpolator)localObject2).<init>();
      localObject2 = (TimeInterpolator)localObject2;
      ((ViewPropertyAnimator)localObject1).setInterpolator((TimeInterpolator)localObject2).setDuration(300L).setListener(paramAnimatorListener);
      return;
    }
  }
  
  private final void a(boolean paramBoolean)
  {
    Object localObject = d;
    if (localObject != null)
    {
      int j = ((ImageView)localObject).getLeft();
      int k = ((ImageView)localObject).getRight();
      j = (j + k) / 2;
      k = ((ImageView)localObject).getTop();
      int m = ((ImageView)localObject).getBottom();
      k = (k + m) / 2;
      if (paramBoolean)
      {
        a(j, k);
        return;
      }
      localObject = new com/truecaller/flashsdk/ui/customviews/FlashAttachButton$c;
      ((FlashAttachButton.c)localObject).<init>(this, paramBoolean);
      localObject = (Animator.AnimatorListener)localObject;
      a(j, k, (Animator.AnimatorListener)localObject);
      return;
    }
  }
  
  private final void a(boolean paramBoolean, View paramView, int paramInt)
  {
    Object localObject1 = h;
    if (localObject1 == null) {
      return;
    }
    ViewPropertyAnimator localViewPropertyAnimator = paramView.animate();
    boolean bool1 = false;
    float f1 = 0.0F;
    Object localObject2 = null;
    int j = 1;
    if (paramBoolean)
    {
      int k = ((List)localObject1).size() - j;
      if (paramInt != k) {
        break label66;
      }
    }
    else
    {
      if (paramInt != 0) {
        break label66;
      }
    }
    bool1 = true;
    f1 = Float.MIN_VALUE;
    label66:
    if (bool1)
    {
      localObject2 = new com/truecaller/flashsdk/ui/customviews/FlashAttachButton$d;
      ((FlashAttachButton.d)localObject2).<init>(this);
      localObject2 = (Animator.AnimatorListener)localObject2;
      localViewPropertyAnimator.setListener((Animator.AnimatorListener)localObject2);
    }
    bool1 = 1112014848;
    f1 = 50.0F;
    j = 0;
    long l1 = 300L;
    if (paramBoolean)
    {
      paramBoolean = l.a(getContext(), f1);
      boolean bool2 = ((List)localObject1).size() - paramInt;
      f2 = paramBoolean * bool2;
      paramView.setTranslationY(f2);
      paramBoolean = R.id.flashAttachSubmenuTitle;
      localObject3 = paramView.findViewById(paramBoolean);
      k.a(localObject3, "view.findViewById<View>(….flashAttachSubmenuTitle)");
      ((View)localObject3).setAlpha(0.0F);
      long l2 = paramInt * 300 / 8;
      localViewPropertyAnimator.setStartDelay(l2);
      localObject3 = localViewPropertyAnimator.translationY(0.0F);
      localObject4 = new android/view/animation/OvershootInterpolator;
      int m = 1069547520;
      float f3 = 1.5F;
      ((OvershootInterpolator)localObject4).<init>(f3);
      localObject4 = (TimeInterpolator)localObject4;
      localObject3 = ((ViewPropertyAnimator)localObject3).setInterpolator((TimeInterpolator)localObject4);
      localObject4 = "translationY(0f)\n       …rshootInterpolator(1.5f))";
      k.a(localObject3, (String)localObject4);
      ((ViewPropertyAnimator)localObject3).setDuration(l1);
      paramBoolean = R.id.flashAttachSubmenuTitle;
      localObject3 = paramView.findViewById(paramBoolean).animate();
      ((ViewPropertyAnimator)localObject3).setStartDelay(l2);
      paramInt = 1065353216;
      float f4 = 1.0F;
      ((ViewPropertyAnimator)localObject3).alpha(f4);
      localObject1 = new android/view/animation/DecelerateInterpolator;
      ((DecelerateInterpolator)localObject1).<init>();
      localObject1 = (TimeInterpolator)localObject1;
      localObject3 = ((ViewPropertyAnimator)localObject3).setInterpolator((TimeInterpolator)localObject1);
      localObject1 = "setInterpolator(DecelerateInterpolator())";
      k.a(localObject3, (String)localObject1);
      ((ViewPropertyAnimator)localObject3).setDuration(l1);
      paramBoolean = g;
      if (paramBoolean)
      {
        paramBoolean = R.id.flashAttachSubmenuIcon;
        localObject3 = paramView.findViewById(paramBoolean);
        k.a(localObject3, "icon");
        ((View)localObject3).setAlpha(0.0F);
        ((View)localObject3).animate();
        localViewPropertyAnimator.setStartDelay(l2);
        localObject3 = localViewPropertyAnimator.alpha(f4);
        paramView = new android/view/animation/DecelerateInterpolator;
        paramView.<init>();
        paramView = (TimeInterpolator)paramView;
        localObject3 = ((ViewPropertyAnimator)localObject3).setInterpolator(paramView);
        paramView = "alpha(1f)\n              …DecelerateInterpolator())";
        k.a(localObject3, paramView);
        ((ViewPropertyAnimator)localObject3).setDuration(l1);
      }
      return;
    }
    paramBoolean = ((List)localObject1).size();
    boolean bool3 = paramInt + 1;
    long l3 = (paramBoolean - bool3) * 300 / 4;
    localViewPropertyAnimator.setStartDelay(l3);
    paramBoolean = l.a(getContext(), f1);
    bool1 = ((List)localObject1).size() - paramInt;
    float f2 = paramBoolean * bool1;
    Object localObject3 = localViewPropertyAnimator.translationY(f2);
    Object localObject4 = new android/view/animation/AccelerateInterpolator;
    ((AccelerateInterpolator)localObject4).<init>();
    localObject4 = (TimeInterpolator)localObject4;
    localObject3 = ((ViewPropertyAnimator)localObject3).setInterpolator((TimeInterpolator)localObject4);
    k.a(localObject3, "translationY(\n          …AccelerateInterpolator())");
    ((ViewPropertyAnimator)localObject3).setDuration(l1);
    paramBoolean = R.id.flashAttachSubmenuTitle;
    localObject3 = paramView.findViewById(paramBoolean).animate();
    paramInt = (((List)localObject1).size() - bool3) * 300 / 4;
    l3 = paramInt;
    ((ViewPropertyAnimator)localObject3).setStartDelay(l3);
    localObject3 = ((ViewPropertyAnimator)localObject3).alpha(0.0F);
    localObject4 = new android/view/animation/AccelerateInterpolator;
    ((AccelerateInterpolator)localObject4).<init>();
    localObject4 = (TimeInterpolator)localObject4;
    localObject3 = ((ViewPropertyAnimator)localObject3).setInterpolator((TimeInterpolator)localObject4);
    localObject4 = "alpha(0f)\n              …AccelerateInterpolator())";
    k.a(localObject3, (String)localObject4);
    ((ViewPropertyAnimator)localObject3).setDuration(l1);
    paramBoolean = g;
    if (paramBoolean)
    {
      paramBoolean = R.id.flashAttachSubmenuIcon;
      paramView.findViewById(paramBoolean).animate();
      paramBoolean = (((List)localObject1).size() - bool3) * 300 / 4;
      long l4 = paramBoolean;
      localViewPropertyAnimator.setStartDelay(l4);
      localObject3 = localViewPropertyAnimator.alpha(0.0F);
      paramView = new android/view/animation/AccelerateInterpolator;
      paramView.<init>();
      paramView = (TimeInterpolator)paramView;
      localObject3 = ((ViewPropertyAnimator)localObject3).setInterpolator(paramView);
      paramView = "alpha(0f)\n              …AccelerateInterpolator())";
      k.a(localObject3, paramView);
      ((ViewPropertyAnimator)localObject3).setDuration(l1);
    }
  }
  
  private final void b()
  {
    Object localObject1 = f;
    if (localObject1 != null)
    {
      ((View)localObject1).setAlpha(0.0F);
      ((View)localObject1).setVisibility(0);
      localObject1 = ((View)localObject1).animate().alpha(1.0F);
      Object localObject2 = new android/view/animation/AccelerateDecelerateInterpolator;
      ((AccelerateDecelerateInterpolator)localObject2).<init>();
      localObject2 = (TimeInterpolator)localObject2;
      ((ViewPropertyAnimator)localObject1).setInterpolator((TimeInterpolator)localObject2).setDuration(300L).setListener(null);
      return;
    }
  }
  
  private final void b(int paramInt1, int paramInt2)
  {
    View localView = f;
    if (localView != null)
    {
      int j = localView.getHeight();
      int k = localView.getWidth();
      j = Math.max(j, k);
      float f1 = j;
      Animator localAnimator = ViewAnimationUtils.createCircularReveal(localView, paramInt1, paramInt2, 0.0F, f1);
      paramInt2 = 0;
      localView.setVisibility(0);
      if (localAnimator != null)
      {
        localAnimator.start();
        return;
      }
      return;
    }
  }
  
  private final void b(int paramInt1, int paramInt2, Animator.AnimatorListener paramAnimatorListener)
  {
    View localView = f;
    Animator localAnimator;
    if (localView != null)
    {
      int j = localView.getWidth();
      int k = localView.getHeight();
      j = Math.max(j, k);
      float f1 = j;
      k = 0;
      localAnimator = ViewAnimationUtils.createCircularReveal(localView, paramInt1, paramInt2, f1, 0.0F);
    }
    else
    {
      paramInt1 = 0;
      localAnimator = null;
    }
    if (localAnimator != null)
    {
      localAnimator.addListener(paramAnimatorListener);
      localAnimator.start();
      return;
    }
  }
  
  private final int getSubMenuItemResourceId()
  {
    return R.layout.flash_attach_view_sub_menu;
  }
  
  public final void onClick(View paramView)
  {
    Object localObject1 = "v";
    k.b(paramView, (String)localObject1);
    boolean bool1 = a;
    if (bool1)
    {
      a();
      return;
    }
    bool1 = b;
    if (!bool1)
    {
      paramView = h;
      if (paramView == null) {
        return;
      }
      localObject1 = e;
      if (localObject1 == null) {
        return;
      }
      boolean bool3 = true;
      b = bool3;
      Object localObject2 = LayoutInflater.from(getContext());
      ((LinearLayout)localObject1).removeAllViews();
      paramView = (Iterable)paramView;
      Object localObject3 = new java/util/ArrayList;
      int m = m.a(paramView, 10);
      ((ArrayList)localObject3).<init>(m);
      localObject3 = (Collection)localObject3;
      paramView = paramView.iterator();
      m = 0;
      int n = 0;
      Object localObject4 = null;
      Object localObject5;
      for (;;)
      {
        boolean bool4 = paramView.hasNext();
        if (!bool4) {
          break;
        }
        localObject5 = paramView.next();
        int i1 = n + 1;
        if (n < 0) {
          m.a();
        }
        localObject5 = (a)localObject5;
        int i2 = getSubMenuItemResourceId();
        Object localObject6 = localObject1;
        localObject6 = (ViewGroup)localObject1;
        View localView = ((LayoutInflater)localObject2).inflate(i2, (ViewGroup)localObject6, false);
        int i3 = R.id.flashAttachSubmenuIcon;
        localObject6 = (ImageView)localView.findViewById(i3);
        int i4 = R.id.flashAttachSubmenuTitle;
        Object localObject7 = (TextView)localView.findViewById(i4);
        int i5 = c;
        ((TextView)localObject7).setText(i5);
        i5 = b;
        ((ImageView)localObject6).setImageResource(i5);
        i5 = d;
        ((TextView)localObject7).setTextColor(i5);
        localObject7 = PorterDuff.Mode.SRC_IN;
        ((ImageView)localObject6).setColorFilter(i5, (PorterDuff.Mode)localObject7);
        localObject7 = e;
        if (localObject7 != null)
        {
          String str = "icon";
          k.a(localObject6, str);
          ((ImageView)localObject6).setBackground((Drawable)localObject7);
        }
        i4 = f;
        if (i4 != 0)
        {
          localObject6 = (View)localObject6;
          localObject7 = ColorStateList.valueOf(i4);
          r.a((View)localObject6, (ColorStateList)localObject7);
        }
        localObject6 = new com/truecaller/flashsdk/ui/customviews/FlashAttachButton$g;
        ((FlashAttachButton.g)localObject6).<init>((a)localObject5, this, (LayoutInflater)localObject2, (LinearLayout)localObject1);
        localObject6 = (View.OnClickListener)localObject6;
        localView.setOnClickListener((View.OnClickListener)localObject6);
        ((LinearLayout)localObject1).addView(localView);
        localObject5 = "view";
        k.a(localView, (String)localObject5);
        a(bool3, localView, n);
        localObject4 = x.a;
        ((Collection)localObject3).add(localObject4);
        n = i1;
      }
      paramView = h;
      if (paramView != null)
      {
        localObject2 = d;
        if (localObject2 != null)
        {
          localObject3 = e;
          if (localObject3 != null)
          {
            localObject4 = ((LinearLayout)localObject3).getLayoutParams();
            if (localObject4 == null) {
              break label627;
            }
            localObject4 = (FrameLayout.LayoutParams)localObject4;
            localObject5 = getContext();
            float f1 = paramView.size() * 50;
            int j = l.a((Context)localObject5, f1);
            height = j;
            localObject4 = (ViewGroup.LayoutParams)localObject4;
            ((LinearLayout)localObject3).setLayoutParams((ViewGroup.LayoutParams)localObject4);
            boolean bool2 = g;
            n = 0;
            localObject4 = null;
            int k;
            if (bool2)
            {
              ((LinearLayout)localObject3).setTranslationY(0.0F);
              k = -((ImageView)localObject2).getMeasuredWidth();
              f1 = k;
              ((LinearLayout)localObject3).setTranslationX(f1);
            }
            else
            {
              k = -((ImageView)localObject2).getMeasuredHeight();
              f1 = k;
              ((LinearLayout)localObject3).setTranslationY(f1);
              ((LinearLayout)localObject3).setTranslationX(0.0F);
            }
          }
        }
      }
      ((LinearLayout)localObject1).setVisibility(0);
      a(bool3);
      a = bool3;
      paramView = i;
      if (paramView != null)
      {
        paramView.b();
        return;
        label627:
        paramView = new c/u;
        paramView.<init>("null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams");
        throw paramView;
      }
    }
  }
  
  protected final void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    ImageView localImageView = d;
    if (localImageView == null) {
      return;
    }
    FlashAttachButton.b localb = c;
    if (localb != null)
    {
      paramInt2 = a;
      paramInt3 = localImageView.getLeft() - paramInt2;
      paramInt4 = localImageView.getTop() - paramInt2;
      int j = localImageView.getRight() + paramInt2;
      paramBoolean = localImageView.getBottom() + paramInt2;
      localb.layout(paramInt3, paramInt4, j, paramBoolean);
      return;
    }
  }
  
  public final void setBackgroundColor(int paramInt)
  {
    Object localObject = getContext();
    int j = R.drawable.flash_round_button_default_v2;
    localObject = android.support.v4.app.a.a((Context)localObject, j);
    if (localObject != null)
    {
      PorterDuff.Mode localMode = PorterDuff.Mode.MULTIPLY;
      ((Drawable)localObject).setColorFilter(paramInt, localMode);
    }
    ImageView localImageView = d;
    if (localImageView != null)
    {
      localImageView.setBackground((Drawable)localObject);
      return;
    }
  }
  
  public final void setDrawable(Drawable paramDrawable)
  {
    k.b(paramDrawable, "drawable");
    ImageView localImageView = d;
    if (localImageView != null)
    {
      localImageView.setImageDrawable(paramDrawable);
      return;
    }
  }
  
  public final void setFabActionListener(FlashAttachButton.a parama)
  {
    k.b(parama, "fabActionListener");
    i = parama;
  }
  
  public final void setMenuItems(List paramList)
  {
    h = paramList;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.customviews.FlashAttachButton
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */