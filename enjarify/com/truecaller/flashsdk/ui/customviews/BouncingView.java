package com.truecaller.flashsdk.ui.customviews;

import android.content.Context;
import android.support.v4.view.r;
import android.support.v4.widget.p;
import android.support.v4.widget.p.a;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public class BouncingView
  extends FrameLayout
{
  private p a;
  private int b;
  private BouncingView.b c;
  
  public BouncingView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private BouncingView(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet, 0);
  }
  
  public final void a(BouncingView.b paramb, boolean paramBoolean)
  {
    c = paramb;
    if (!paramBoolean)
    {
      paramb = c;
      paramb.K_();
    }
  }
  
  public void computeScroll()
  {
    super.computeScroll();
    p localp = a;
    boolean bool = localp.b();
    if (bool) {
      r.e(this);
    }
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    Object localObject = new com/truecaller/flashsdk/ui/customviews/BouncingView$a;
    ((BouncingView.a)localObject).<init>(this, (byte)0);
    localObject = p.a(this, 1.0F, (p.a)localObject);
    a = ((p)localObject);
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    return a.a(paramMotionEvent);
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    a.b(paramMotionEvent);
    return true;
  }
  
  public void setDragViewResId(int paramInt)
  {
    b = paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.customviews.BouncingView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */