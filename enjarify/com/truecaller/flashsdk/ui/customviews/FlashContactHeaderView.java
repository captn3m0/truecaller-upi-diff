package com.truecaller.flashsdk.ui.customviews;

import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;

public final class FlashContactHeaderView
  extends Toolbar
  implements View.OnClickListener
{
  public final TextView a;
  public final ImageView b;
  private FlashContactHeaderView.a c;
  
  public FlashContactHeaderView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private FlashContactHeaderView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, 0);
    paramContext = LayoutInflater.from(paramContext);
    int i = R.layout.flash_contact_header;
    Object localObject = this;
    localObject = (ViewGroup)this;
    paramContext.inflate(i, (ViewGroup)localObject, true);
    int j = R.id.contact_logo;
    paramContext = findViewById(j);
    k.a(paramContext, "findViewById(R.id.contact_logo)");
    paramContext = (ImageView)paramContext;
    b = paramContext;
    j = R.id.contact_header_text;
    paramContext = findViewById(j);
    k.a(paramContext, "findViewById(R.id.contact_header_text)");
    paramContext = (TextView)paramContext;
    a = paramContext;
    paramContext = b;
    paramAttributeSet = this;
    paramAttributeSet = (View.OnClickListener)this;
    paramContext.setOnClickListener(paramAttributeSet);
    a.setOnClickListener(paramAttributeSet);
  }
  
  public final FlashContactHeaderView.a getContactClickListener$flash_release()
  {
    return c;
  }
  
  public final void onClick(View paramView)
  {
    String str = "v";
    k.b(paramView, str);
    paramView = c;
    if (paramView != null)
    {
      paramView.z();
      return;
    }
  }
  
  public final void setContactClickListener$flash_release(FlashContactHeaderView.a parama)
  {
    c = parama;
  }
  
  public final void setHeaderTextColor(int paramInt)
  {
    a.setTextColor(paramInt);
    Object localObject1 = new android/text/SpannableString;
    Object localObject2 = a.getText();
    ((SpannableString)localObject1).<init>((CharSequence)localObject2);
    int i = a.getText().length();
    Object localObject3 = ImageSpan.class;
    int j = 0;
    localObject1 = (ImageSpan[])((SpannableString)localObject1).getSpans(0, i, (Class)localObject3);
    localObject2 = "spans";
    k.a(localObject1, (String)localObject2);
    i = localObject1.length;
    while (j < i)
    {
      localObject3 = localObject1[j];
      k.a(localObject3, "imageSpan");
      localObject3 = ((ImageSpan)localObject3).getDrawable();
      PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
      ((Drawable)localObject3).setColorFilter(paramInt, localMode);
      j += 1;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.customviews.FlashContactHeaderView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */