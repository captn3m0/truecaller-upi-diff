package com.truecaller.flashsdk.ui.customviews;

import c.g.a.m;
import c.g.b.j;
import c.g.b.w;
import c.l.c;

final class FlashReceiveFooterView$c
  extends j
  implements m
{
  FlashReceiveFooterView$c(FlashReceiveFooterView paramFlashReceiveFooterView)
  {
    super(2, paramFlashReceiveFooterView);
  }
  
  public final c a()
  {
    return w.a(FlashReceiveFooterView.class);
  }
  
  public final String b()
  {
    return "onFocusChange";
  }
  
  public final String c()
  {
    return "onFocusChange(Landroid/view/View;Z)V";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.customviews.FlashReceiveFooterView.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */