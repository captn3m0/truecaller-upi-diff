package com.truecaller.flashsdk.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import c.g.a.m;
import c.g.b.k;
import com.d.b.ab;
import com.d.b.w;
import com.truecaller.flashsdk.R.drawable;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;
import com.truecaller.flashsdk.ui.UniformEmojiLayout;

public final class FlashReceiveFooterView
  extends b
  implements View.OnClickListener
{
  public EditText q;
  public ImageView r;
  private View s;
  
  public FlashReceiveFooterView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private FlashReceiveFooterView(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, '\000');
  }
  
  private final void d(int paramInt)
  {
    boolean bool = false;
    int i;
    if (paramInt == 0) {
      i = 8;
    } else {
      i = 0;
    }
    ImageView localImageView = r;
    if (localImageView == null)
    {
      String str = "mapView";
      k.a(str);
    }
    localImageView.setVisibility(paramInt);
    getRecentEmojiLayout().setVisibility(i);
    localImageView = getMoreEmojis();
    localImageView.setVisibility(i);
    if (paramInt == 0) {
      bool = true;
    }
    a(bool);
  }
  
  public final void a(String paramString1, String paramString2, w paramw)
  {
    k.b(paramString1, "placeName");
    k.b(paramString2, "locationImageUrl");
    String str = "picasso";
    k.b(paramw, str);
    paramString2 = paramw.a(paramString2);
    int i = R.drawable.ic_map_placeholder;
    paramString2 = paramString2.a(i);
    paramw = r;
    if (paramw == null)
    {
      str = "mapView";
      k.a(str);
    }
    str = null;
    paramString2.a(paramw, null);
    paramString2 = q;
    if (paramString2 == null)
    {
      paramw = "editMessageText";
      k.a(paramw);
    }
    paramw = paramString1;
    paramw = (CharSequence)paramString1;
    paramString2.setText(paramw);
    paramString2 = q;
    if (paramString2 == null)
    {
      paramw = "editMessageText";
      k.a(paramw);
    }
    int j = Math.min(paramString1.length(), 80);
    paramString2.setSelection(j);
    d(0);
  }
  
  public final void b()
  {
    super.b();
    int i = R.id.edit_message_text;
    Object localObject1 = findViewById(i);
    k.a(localObject1, "findViewById(R.id.edit_message_text)");
    localObject1 = (EditText)localObject1;
    q = ((EditText)localObject1);
    i = R.id.map_view;
    localObject1 = findViewById(i);
    k.a(localObject1, "findViewById(R.id.map_view)");
    localObject1 = (ImageView)localObject1;
    r = ((ImageView)localObject1);
    i = R.id.textPadding;
    localObject1 = findViewById(i);
    Object localObject2 = "findViewById(R.id.textPadding)";
    k.a(localObject1, (String)localObject2);
    s = ((View)localObject1);
    localObject1 = q;
    if (localObject1 == null)
    {
      localObject2 = "editMessageText";
      k.a((String)localObject2);
    }
    localObject2 = new com/truecaller/flashsdk/ui/customviews/FlashReceiveFooterView$b;
    Object localObject3 = this;
    localObject3 = (FlashReceiveFooterView)this;
    ((FlashReceiveFooterView.b)localObject2).<init>((FlashReceiveFooterView)localObject3);
    localObject2 = (c.g.a.b)localObject2;
    com.truecaller.flashsdk.assist.t.a((EditText)localObject1, (c.g.a.b)localObject2);
    localObject1 = q;
    if (localObject1 == null)
    {
      localObject2 = "editMessageText";
      k.a((String)localObject2);
    }
    localObject2 = new com/truecaller/flashsdk/ui/customviews/FlashReceiveFooterView$c;
    ((FlashReceiveFooterView.c)localObject2).<init>((FlashReceiveFooterView)localObject3);
    localObject2 = (m)localObject2;
    localObject3 = new com/truecaller/flashsdk/ui/customviews/c;
    ((c)localObject3).<init>((m)localObject2);
    localObject3 = (View.OnFocusChangeListener)localObject3;
    ((EditText)localObject1).setOnFocusChangeListener((View.OnFocusChangeListener)localObject3);
    localObject1 = q;
    if (localObject1 == null)
    {
      localObject2 = "editMessageText";
      k.a((String)localObject2);
    }
    com.truecaller.flashsdk.assist.t.a((EditText)localObject1);
  }
  
  public final int getLayoutResource()
  {
    return R.layout.flash_receive_footer;
  }
  
  public final String getMessageText()
  {
    EditText localEditText = q;
    if (localEditText == null)
    {
      String str = "editMessageText";
      k.a(str);
    }
    return localEditText.getText().toString();
  }
  
  public final int getSelectionEnd()
  {
    EditText localEditText = q;
    if (localEditText == null)
    {
      String str = "editMessageText";
      k.a(str);
    }
    return localEditText.getSelectionEnd();
  }
  
  public final int getSelectionStart()
  {
    EditText localEditText = q;
    if (localEditText == null)
    {
      String str = "editMessageText";
      k.a(str);
    }
    return localEditText.getSelectionStart();
  }
  
  public final void k()
  {
    int i = 8;
    d(i);
    EditText localEditText = q;
    if (localEditText == null)
    {
      String str = "editMessageText";
      k.a(str);
    }
    localEditText.setText(null);
  }
  
  public final void l()
  {
    UniformEmojiLayout localUniformEmojiLayout = getRecentEmojiLayout();
    int i = 8;
    localUniformEmojiLayout.setVisibility(i);
    getMoreEmojis().setVisibility(i);
  }
  
  public final void m()
  {
    getRecentEmojiLayout().setVisibility(0);
    getMoreEmojis().setVisibility(0);
  }
  
  public final void n()
  {
    EditText localEditText = q;
    if (localEditText == null)
    {
      String str = "editMessageText";
      k.a(str);
    }
    com.truecaller.utils.extensions.t.a((View)localEditText, true, 2);
  }
  
  public final void o()
  {
    EditText localEditText = q;
    if (localEditText == null)
    {
      String str = "editMessageText";
      k.a(str);
    }
    localEditText.requestFocus();
  }
  
  public final void onClick(View paramView)
  {
    Object localObject1 = "v";
    k.b(paramView, (String)localObject1);
    int i = paramView.getId();
    int j = R.id.sendMessage;
    if (i == j)
    {
      paramView = (FlashReceiveFooterView.a)getActionListener();
      if (paramView != null)
      {
        localObject1 = q;
        if (localObject1 == null)
        {
          localObject2 = "editMessageText";
          k.a((String)localObject2);
        }
        localObject1 = ((EditText)localObject1).getText();
        k.a(localObject1, "editMessageText.text");
        localObject1 = (CharSequence)localObject1;
        Object localObject2 = r;
        if (localObject2 == null)
        {
          String str = "mapView";
          k.a(str);
        }
        j = ((ImageView)localObject2).getVisibility();
        if (j == 0)
        {
          j = 1;
        }
        else
        {
          j = 0;
          localObject2 = null;
        }
        paramView.a((CharSequence)localObject1, j);
      }
      return;
    }
    super.onClick(paramView);
  }
  
  public final void p()
  {
    Object localObject = getSendLocation();
    int i = 4;
    ((ImageView)localObject).setVisibility(i);
    getSendLocation().setClickable(false);
    localObject = s;
    if (localObject == null)
    {
      String str1 = "extraSpace";
      k.a(str1);
    }
    ((View)localObject).setVisibility(i);
    localObject = s;
    if (localObject == null)
    {
      String str2 = "extraSpace";
      k.a(str2);
    }
    ((View)localObject).setClickable(false);
  }
  
  public final void q()
  {
    Object localObject = getSendLocation();
    int i = 8;
    ((ImageView)localObject).setVisibility(i);
    localObject = s;
    if (localObject == null)
    {
      String str = "extraSpace";
      k.a(str);
    }
    ((View)localObject).setVisibility(i);
  }
  
  public final void r()
  {
    Object localObject = q;
    if (localObject == null)
    {
      String str = "editMessageText";
      k.a(str);
    }
    ((EditText)localObject).setEnabled(true);
    getSendMessageProgress().setVisibility(8);
    localObject = getSendMessage();
    int i = R.drawable.flash_reply_button_selector;
    ((ImageView)localObject).setImageResource(i);
  }
  
  public final void setCameraModeHint(String paramString)
  {
    k.b(paramString, "hint");
    EditText localEditText = q;
    if (localEditText == null)
    {
      String str = "editMessageText";
      k.a(str);
    }
    paramString = (CharSequence)paramString;
    localEditText.setHint(paramString);
  }
  
  public final void setMessageCursorVisible(boolean paramBoolean)
  {
    EditText localEditText = q;
    if (localEditText == null)
    {
      String str = "editMessageText";
      k.a(str);
    }
    localEditText.setCursorVisible(paramBoolean);
  }
  
  public final void setMessageText(String paramString)
  {
    EditText localEditText = q;
    if (localEditText == null)
    {
      localObject = "editMessageText";
      k.a((String)localObject);
    }
    Object localObject = paramString;
    localObject = (CharSequence)paramString;
    localEditText.setText((CharSequence)localObject);
    localEditText = q;
    if (localEditText == null)
    {
      localObject = "editMessageText";
      k.a((String)localObject);
    }
    int i;
    if (paramString != null)
    {
      i = paramString.length();
    }
    else
    {
      i = 0;
      paramString = null;
    }
    localEditText.setSelection(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.customviews.FlashReceiveFooterView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */