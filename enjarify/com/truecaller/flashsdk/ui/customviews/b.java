package com.truecaller.flashsdk.ui.customviews;

import android.app.Activity;
import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import c.g.b.k;
import c.u;
import com.truecaller.flashsdk.R.drawable;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.a.d;
import com.truecaller.flashsdk.ui.UniformEmojiLayout;
import com.truecaller.flashsdk.ui.UniformEmojiLayout.a;

public abstract class b
  extends ConstraintLayout
  implements View.OnClickListener, UniformEmojiLayout.a
{
  protected ImageView k;
  protected ImageView l;
  protected ImageView m;
  protected ProgressBar n;
  protected ProgressBar o;
  protected UniformEmojiLayout p;
  private b.a q;
  private com.truecaller.flashsdk.ui.whatsnew.b r;
  
  public b(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  public b(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, (byte)0);
    paramContext = getContext();
    int i = getLayoutResource();
    Object localObject = this;
    localObject = (ViewGroup)this;
    View.inflate(paramContext, i, (ViewGroup)localObject);
    b();
  }
  
  public final void a(int paramInt)
  {
    b.a locala = q;
    if (locala != null)
    {
      locala.f(paramInt);
      return;
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    Object localObject = n;
    if (localObject == null)
    {
      str = "locationProgress";
      k.a(str);
    }
    int i = 8;
    ((ProgressBar)localObject).setVisibility(i);
    localObject = l;
    if (localObject == null)
    {
      str = "sendLocation";
      k.a(str);
    }
    i = 0;
    String str = null;
    ((ImageView)localObject).setVisibility(0);
    localObject = l;
    if (localObject == null)
    {
      str = "sendLocation";
      k.a(str);
    }
    ((ImageView)localObject).setSelected(paramBoolean);
  }
  
  public void b()
  {
    int i = R.id.moreEmojis;
    Object localObject1 = findViewById(i);
    k.a(localObject1, "findViewById(R.id.moreEmojis)");
    localObject1 = (ImageView)localObject1;
    k = ((ImageView)localObject1);
    i = R.id.sendLocation;
    localObject1 = findViewById(i);
    k.a(localObject1, "findViewById(R.id.sendLocation)");
    localObject1 = (ImageView)localObject1;
    l = ((ImageView)localObject1);
    i = R.id.sendMessage;
    localObject1 = findViewById(i);
    k.a(localObject1, "findViewById(R.id.sendMessage)");
    localObject1 = (ImageView)localObject1;
    m = ((ImageView)localObject1);
    i = R.id.locationProgress;
    localObject1 = findViewById(i);
    k.a(localObject1, "findViewById(R.id.locationProgress)");
    localObject1 = (ProgressBar)localObject1;
    n = ((ProgressBar)localObject1);
    i = R.id.recentEmojiLayout;
    localObject1 = findViewById(i);
    k.a(localObject1, "findViewById(R.id.recentEmojiLayout)");
    localObject1 = (UniformEmojiLayout)localObject1;
    p = ((UniformEmojiLayout)localObject1);
    i = R.id.sendMessageProgress;
    localObject1 = findViewById(i);
    Object localObject2 = "findViewById(R.id.sendMessageProgress)";
    k.a(localObject1, (String)localObject2);
    localObject1 = (ProgressBar)localObject1;
    o = ((ProgressBar)localObject1);
    localObject1 = k;
    if (localObject1 == null)
    {
      localObject2 = "moreEmojis";
      k.a((String)localObject2);
    }
    localObject2 = this;
    localObject2 = (View.OnClickListener)this;
    ((ImageView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = l;
    String str;
    if (localObject1 == null)
    {
      str = "sendLocation";
      k.a(str);
    }
    ((ImageView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = m;
    if (localObject1 == null)
    {
      str = "sendMessage";
      k.a(str);
    }
    ((ImageView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = p;
    if (localObject1 == null)
    {
      localObject2 = "recentEmojiLayout";
      k.a((String)localObject2);
    }
    localObject2 = this;
    localObject2 = (UniformEmojiLayout.a)this;
    ((UniformEmojiLayout)localObject1).setOnItemClickListener((UniformEmojiLayout.a)localObject2);
    localObject1 = m;
    if (localObject1 == null)
    {
      localObject2 = "sendMessage";
      k.a((String)localObject2);
    }
    ((ImageView)localObject1).setEnabled(false);
  }
  
  public final void b(boolean paramBoolean)
  {
    ImageView localImageView = k;
    if (localImageView == null)
    {
      String str = "moreEmojis";
      k.a(str);
    }
    if (paramBoolean) {
      paramBoolean = R.drawable.ic_smileys_send;
    } else {
      paramBoolean = R.drawable.ic_keyboard;
    }
    localImageView.setImageResource(paramBoolean);
  }
  
  public final void c()
  {
    Object localObject1 = getContext();
    k.a(localObject1, "context");
    Object localObject2 = getContext();
    int i = R.string.tip_use_emoji;
    int j = 1;
    Object[] arrayOfObject = new Object[j];
    char[] arrayOfChar = Character.toChars(128077);
    k.a(arrayOfChar, "Character.toChars(CODE_POINT_THUMBS_UP)");
    String str = new java/lang/String;
    str.<init>(arrayOfChar);
    arrayOfChar = null;
    arrayOfObject[0] = str;
    localObject2 = ((Context)localObject2).getString(i, arrayOfObject);
    k.a(localObject2, "context.getString(R.stri…s(CODE_POINT_THUMBS_UP)))");
    Object localObject3 = new com/truecaller/flashsdk/ui/whatsnew/b;
    ((com.truecaller.flashsdk.ui.whatsnew.b)localObject3).<init>((Context)localObject1, (String)localObject2);
    r = ((com.truecaller.flashsdk.ui.whatsnew.b)localObject3);
    localObject1 = r;
    if (localObject1 != null)
    {
      localObject2 = p;
      if (localObject2 == null)
      {
        localObject3 = "recentEmojiLayout";
        k.a((String)localObject3);
      }
      localObject2 = (View)localObject2;
      ((com.truecaller.flashsdk.ui.whatsnew.b)localObject1).a((View)localObject2, 0);
      return;
    }
  }
  
  public final void c(int paramInt)
  {
    Object localObject1 = new com/truecaller/flashsdk/ui/whatsnew/b;
    Object localObject2 = getContext();
    k.a(localObject2, "context");
    Object localObject3 = getContext().getString(paramInt);
    String str1 = "context.getString(locationTipRes)";
    k.a(localObject3, str1);
    int i = R.drawable.flash_ic_tooltip_left_bottom;
    ((com.truecaller.flashsdk.ui.whatsnew.b)localObject1).<init>((Context)localObject2, (String)localObject3, i);
    r = ((com.truecaller.flashsdk.ui.whatsnew.b)localObject1);
    localObject3 = r;
    if (localObject3 != null)
    {
      localObject1 = l;
      if (localObject1 == null)
      {
        localObject2 = "sendLocation";
        k.a((String)localObject2);
      }
      localObject1 = (View)localObject1;
      k.b(localObject1, "view");
      localObject2 = ((View)localObject1).getContext();
      if (localObject2 != null)
      {
        localObject2 = (Activity)localObject2;
        boolean bool = ((Activity)localObject2).isFinishing();
        if (!bool)
        {
          localObject2 = ((View)localObject1).getApplicationWindowToken();
          if (localObject2 != null)
          {
            localObject2 = a.getContentView();
            str1 = null;
            ((View)localObject2).measure(0, 0);
            localObject2 = a;
            i = -((View)localObject1).getMeasuredWidth() / 2;
            int j = ((View)localObject1).getMeasuredHeight();
            localObject3 = a.getContentView();
            String str2 = "popupWindow.contentView";
            k.a(localObject3, str2);
            paramInt = ((View)localObject3).getMeasuredHeight();
            j += paramInt;
            paramInt = -j;
            ((PopupWindow)localObject2).showAsDropDown((View)localObject1, i, paramInt);
          }
        }
        return;
      }
      localObject3 = new c/u;
      ((u)localObject3).<init>("null cannot be cast to non-null type android.app.Activity");
      throw ((Throwable)localObject3);
    }
  }
  
  public final void c(boolean paramBoolean)
  {
    ImageView localImageView = m;
    if (localImageView == null)
    {
      String str = "sendMessage";
      k.a(str);
    }
    localImageView.setEnabled(paramBoolean);
  }
  
  public final void d()
  {
    Object localObject = r;
    if (localObject != null)
    {
      localObject = a;
      if (localObject != null)
      {
        ((PopupWindow)localObject).dismiss();
        return;
      }
    }
  }
  
  public final void e()
  {
    ProgressBar localProgressBar = o;
    if (localProgressBar == null)
    {
      String str = "sendMessageProgress";
      k.a(str);
    }
    localProgressBar.setVisibility(0);
  }
  
  public final void f()
  {
    Object localObject = o;
    if (localObject == null)
    {
      str = "sendMessageProgress";
      k.a(str);
    }
    int i = 8;
    ((ProgressBar)localObject).setVisibility(i);
    localObject = m;
    if (localObject == null)
    {
      str = "sendMessage";
      k.a(str);
    }
    i = 0;
    String str = null;
    ((ImageView)localObject).setVisibility(0);
    localObject = m;
    if (localObject == null)
    {
      str = "sendMessage";
      k.a(str);
    }
    i = R.drawable.ic_flash_retry_24dp;
    ((ImageView)localObject).setImageResource(i);
  }
  
  public final void g()
  {
    Object localObject = o;
    if (localObject == null)
    {
      str = "sendMessageProgress";
      k.a(str);
    }
    int i = 8;
    ((ProgressBar)localObject).setVisibility(i);
    localObject = m;
    if (localObject == null)
    {
      str = "sendMessage";
      k.a(str);
    }
    i = 0;
    String str = null;
    ((ImageView)localObject).setVisibility(0);
    localObject = m;
    if (localObject == null)
    {
      str = "sendMessage";
      k.a(str);
    }
    i = R.drawable.flash_reply_button_selector;
    ((ImageView)localObject).setImageResource(i);
  }
  
  public final b.a getActionListener()
  {
    return q;
  }
  
  public abstract int getLayoutResource();
  
  protected final ProgressBar getLocationProgress()
  {
    ProgressBar localProgressBar = n;
    if (localProgressBar == null)
    {
      String str = "locationProgress";
      k.a(str);
    }
    return localProgressBar;
  }
  
  protected final ImageView getMoreEmojis()
  {
    ImageView localImageView = k;
    if (localImageView == null)
    {
      String str = "moreEmojis";
      k.a(str);
    }
    return localImageView;
  }
  
  protected final UniformEmojiLayout getRecentEmojiLayout()
  {
    UniformEmojiLayout localUniformEmojiLayout = p;
    if (localUniformEmojiLayout == null)
    {
      String str = "recentEmojiLayout";
      k.a(str);
    }
    return localUniformEmojiLayout;
  }
  
  protected final ImageView getSendLocation()
  {
    ImageView localImageView = l;
    if (localImageView == null)
    {
      String str = "sendLocation";
      k.a(str);
    }
    return localImageView;
  }
  
  protected final ImageView getSendMessage()
  {
    ImageView localImageView = m;
    if (localImageView == null)
    {
      String str = "sendMessage";
      k.a(str);
    }
    return localImageView;
  }
  
  protected final ProgressBar getSendMessageProgress()
  {
    ProgressBar localProgressBar = o;
    if (localProgressBar == null)
    {
      String str = "sendMessageProgress";
      k.a(str);
    }
    return localProgressBar;
  }
  
  public final void h()
  {
    Object localObject = o;
    if (localObject == null)
    {
      str = "sendMessageProgress";
      k.a(str);
    }
    int i = 8;
    ((ProgressBar)localObject).setVisibility(i);
    localObject = m;
    if (localObject == null)
    {
      str = "sendMessage";
      k.a(str);
    }
    i = 0;
    String str = null;
    ((ImageView)localObject).setVisibility(0);
    localObject = m;
    if (localObject == null)
    {
      str = "sendMessage";
      k.a(str);
    }
    i = R.drawable.flash_ic_send_button_selector;
    ((ImageView)localObject).setImageResource(i);
  }
  
  public final void i()
  {
    Object localObject = o;
    if (localObject == null)
    {
      str = "sendMessageProgress";
      k.a(str);
    }
    int i = 8;
    ((ProgressBar)localObject).setVisibility(i);
    localObject = m;
    if (localObject == null)
    {
      str = "sendMessage";
      k.a(str);
    }
    i = 0;
    String str = null;
    ((ImageView)localObject).setVisibility(0);
    localObject = m;
    if (localObject == null)
    {
      str = "sendMessage";
      k.a(str);
    }
    i = R.drawable.flash_reply_retry_button_selector;
    ((ImageView)localObject).setImageResource(i);
    localObject = m;
    if (localObject == null)
    {
      str = "sendMessage";
      k.a(str);
    }
    ((ImageView)localObject).setEnabled(true);
  }
  
  public final void j()
  {
    Object localObject = o;
    if (localObject == null)
    {
      str = "sendMessageProgress";
      k.a(str);
    }
    int i = 8;
    ((ProgressBar)localObject).setVisibility(i);
    localObject = m;
    if (localObject == null)
    {
      str = "sendMessage";
      k.a(str);
    }
    i = 0;
    String str = null;
    ((ImageView)localObject).setVisibility(0);
    localObject = m;
    if (localObject == null)
    {
      str = "sendMessage";
      k.a(str);
    }
    i = R.drawable.ic_flash_sent_24dp;
    ((ImageView)localObject).setImageResource(i);
  }
  
  public void onClick(View paramView)
  {
    Object localObject = "v";
    k.b(paramView, (String)localObject);
    int i = paramView.getId();
    int j = R.id.moreEmojis;
    if (i == j)
    {
      paramView = q;
      if (paramView != null) {
        paramView.O();
      }
      return;
    }
    j = R.id.sendLocation;
    if (i == j)
    {
      paramView = l;
      if (paramView == null)
      {
        localObject = "sendLocation";
        k.a((String)localObject);
      }
      boolean bool = paramView.isSelected();
      localObject = l;
      if (localObject == null)
      {
        str1 = "sendLocation";
        k.a(str1);
      }
      int i1 = 0;
      String str1 = null;
      int i2;
      String str2;
      if (bool)
      {
        i2 = 0;
        str2 = null;
      }
      else
      {
        i2 = 8;
      }
      ((ImageView)localObject).setVisibility(i2);
      localObject = n;
      if (localObject == null)
      {
        str2 = "locationProgress";
        k.a(str2);
      }
      if (bool) {
        i1 = 8;
      }
      ((ProgressBar)localObject).setVisibility(i1);
      localObject = q;
      if (localObject != null)
      {
        bool ^= true;
        ((b.a)localObject).b(bool);
        return;
      }
    }
  }
  
  public final void setActionListener(b.a parama)
  {
    q = parama;
  }
  
  protected final void setLocationProgress(ProgressBar paramProgressBar)
  {
    k.b(paramProgressBar, "<set-?>");
    n = paramProgressBar;
  }
  
  protected final void setMoreEmojis(ImageView paramImageView)
  {
    k.b(paramImageView, "<set-?>");
    k = paramImageView;
  }
  
  protected final void setRecentEmojiLayout(UniformEmojiLayout paramUniformEmojiLayout)
  {
    k.b(paramUniformEmojiLayout, "<set-?>");
    p = paramUniformEmojiLayout;
  }
  
  public void setRecentEmojis(d[] paramArrayOfd)
  {
    k.b(paramArrayOfd, "emojiList");
    UniformEmojiLayout localUniformEmojiLayout = p;
    if (localUniformEmojiLayout == null)
    {
      String str = "recentEmojiLayout";
      k.a(str);
    }
    localUniformEmojiLayout.setEmojis(paramArrayOfd);
  }
  
  protected final void setSendLocation(ImageView paramImageView)
  {
    k.b(paramImageView, "<set-?>");
    l = paramImageView;
  }
  
  protected final void setSendMessage(ImageView paramImageView)
  {
    k.b(paramImageView, "<set-?>");
    m = paramImageView;
  }
  
  protected final void setSendMessageProgress(ProgressBar paramProgressBar)
  {
    k.b(paramProgressBar, "<set-?>");
    o = paramProgressBar;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.customviews.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */