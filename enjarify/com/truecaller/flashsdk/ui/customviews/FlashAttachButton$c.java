package com.truecaller.flashsdk.ui.customviews;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import c.g.b.k;

public final class FlashAttachButton$c
  extends AnimatorListenerAdapter
{
  FlashAttachButton$c(FlashAttachButton paramFlashAttachButton, boolean paramBoolean) {}
  
  public final void onAnimationEnd(Animator paramAnimator)
  {
    String str = "animation";
    k.b(paramAnimator, str);
    paramAnimator = FlashAttachButton.d(a);
    if (paramAnimator != null)
    {
      paramAnimator.setVisibility(4);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.customviews.FlashAttachButton.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */