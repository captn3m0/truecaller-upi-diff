package com.truecaller.flashsdk.ui.customviews;

import android.support.v4.widget.p;
import android.support.v4.widget.p.a;
import android.view.View;

final class BouncingView$a
  extends p.a
{
  private BouncingView$a(BouncingView paramBouncingView) {}
  
  public final int a(View paramView)
  {
    return paramView.getMeasuredHeight();
  }
  
  public final int a(View paramView, int paramInt1, int paramInt2)
  {
    if (paramInt2 > 0)
    {
      int i = 10;
      if (paramInt1 < i) {
        return paramInt1;
      }
      return 0;
    }
    return paramInt1;
  }
  
  public final void a(int paramInt)
  {
    super.a(paramInt);
    BouncingView.b localb = BouncingView.c(a);
    if ((localb != null) && (paramInt == 0))
    {
      Object localObject = a;
      int i = BouncingView.a((BouncingView)localObject);
      localObject = ((BouncingView)localObject).findViewById(i);
      paramInt = ((View)localObject).getTop();
      if (paramInt < 0)
      {
        localObject = BouncingView.c(a);
        ((BouncingView.b)localObject).K_();
      }
    }
  }
  
  public final void a(View paramView, float paramFloat1, float paramFloat2)
  {
    super.a(paramView, paramFloat1, paramFloat2);
    paramFloat1 = 0.0F;
    int i = -1013579776;
    float f = -300.0F;
    boolean bool = paramFloat2 < f;
    if (!bool)
    {
      int j = paramView.getBottom();
      BouncingView localBouncingView = a;
      i = localBouncingView.getHeight() / 2;
      if (j >= i)
      {
        paramView = BouncingView.b(a);
        localObject = a;
        j = ((BouncingView)localObject).getTop();
        paramView.a(0, j);
        break label116;
      }
    }
    Object localObject = BouncingView.b(a);
    int k = -paramView.getHeight();
    ((p)localObject).a(0, k);
    label116:
    a.invalidate();
  }
  
  public final boolean a(View paramView, int paramInt)
  {
    int i = paramView.getId();
    BouncingView localBouncingView = a;
    paramInt = BouncingView.a(localBouncingView);
    return i == paramInt;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.customviews.BouncingView.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */