package com.truecaller.flashsdk.ui.customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.view.View;
import c.g.b.k;
import com.truecaller.common.h.l;

public final class FlashAttachButton$b
  extends View
{
  public static final FlashAttachButton.b.a b;
  final int a;
  private final Paint c;
  
  static
  {
    FlashAttachButton.b.a locala = new com/truecaller/flashsdk/ui/customviews/FlashAttachButton$b$a;
    locala.<init>((byte)0);
    b = locala;
  }
  
  public FlashAttachButton$b(Context paramContext)
  {
    super(paramContext);
    int i = l.a(getContext(), 4.0F);
    int j = l.a(getContext(), 0.0F);
    int k = l.a(getContext(), 2.0F);
    int m = Math.max(j, k) + i;
    a = m;
    Paint localPaint = new android/graphics/Paint;
    localPaint.<init>();
    localPaint.setStrokeWidth(1.0F);
    int n = 33000000;
    localPaint.setColor(n);
    Paint.Style localStyle = Paint.Style.FILL;
    localPaint.setStyle(localStyle);
    int i1 = 1;
    localPaint.setAntiAlias(i1);
    float f1 = i;
    float f2 = j;
    float f3 = k;
    localPaint.setShadowLayer(f1, f2, f3, n);
    c = localPaint;
    setLayerType(i1, null);
  }
  
  public final int getMOffset$flash_release()
  {
    return a;
  }
  
  protected final void onDraw(Canvas paramCanvas)
  {
    k.b(paramCanvas, "canvas");
    super.onDraw(paramCanvas);
    int i = paramCanvas.save();
    float f1 = getWidth() / 2;
    float f2 = getHeight() / 2;
    paramCanvas.translate(f1, f2);
    int j = getWidth() / 2;
    int k = a;
    f1 = j - k;
    Paint localPaint = c;
    paramCanvas.drawCircle(0.0F, 0.0F, f1, localPaint);
    paramCanvas.restoreToCount(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.customviews.FlashAttachButton.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */