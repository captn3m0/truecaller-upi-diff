package com.truecaller.flashsdk.ui.customviews;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.animation.AccelerateInterpolator;

public class ArrowView
  extends AppCompatImageView
{
  private float a = 0.0F;
  private float b = 0.0F;
  private float c = 0.0F;
  private float d = 0.0F;
  private ObjectAnimator e;
  private ObjectAnimator f;
  private ObjectAnimator g;
  private ObjectAnimator h;
  private Drawable i;
  private Drawable j;
  private Drawable k;
  private Drawable l;
  
  public ArrowView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private ArrowView(Context paramContext, AttributeSet paramAttributeSet, byte paramByte)
  {
    super(paramContext, paramAttributeSet, 0);
    int m = 2;
    Object localObject1 = new float[m];
    Object tmp38_36 = localObject1;
    tmp38_36[0] = 0.0F;
    tmp38_36[1] = 1.0F;
    paramContext = PropertyValuesHolder.ofFloat("alphaOne", (float[])localObject1);
    Object localObject2 = new float[m];
    Object tmp62_60 = localObject2;
    tmp62_60[0] = 0.0F;
    tmp62_60[1] = 1.0F;
    localObject1 = PropertyValuesHolder.ofFloat("alphaTwo", (float[])localObject2);
    Object localObject3 = new float[m];
    Object tmp87_85 = localObject3;
    tmp87_85[0] = 0.0F;
    tmp87_85[1] = 1.0F;
    localObject2 = PropertyValuesHolder.ofFloat("alphaThree", (float[])localObject3);
    float[] arrayOfFloat = new float[m];
    float[] tmp112_110 = arrayOfFloat;
    tmp112_110[0] = 0.0F;
    tmp112_110[1] = 1.0F;
    localObject3 = PropertyValuesHolder.ofFloat("alphaFour", arrayOfFloat);
    int n = 1;
    PropertyValuesHolder[] arrayOfPropertyValuesHolder = new PropertyValuesHolder[n];
    arrayOfPropertyValuesHolder[0] = localObject3;
    localObject3 = ObjectAnimator.ofPropertyValuesHolder(this, arrayOfPropertyValuesHolder);
    h = ((ObjectAnimator)localObject3);
    localObject3 = h;
    long l1 = 600L;
    ((ObjectAnimator)localObject3).setDuration(l1);
    h.setStartDelay(900L);
    localObject3 = h;
    int i1 = -1;
    ((ObjectAnimator)localObject3).setRepeatCount(i1);
    h.setRepeatMode(m);
    localObject3 = h;
    AccelerateInterpolator localAccelerateInterpolator = new android/view/animation/AccelerateInterpolator;
    localAccelerateInterpolator.<init>();
    ((ObjectAnimator)localObject3).setInterpolator(localAccelerateInterpolator);
    localObject3 = new PropertyValuesHolder[n];
    localObject3[0] = localObject2;
    localObject2 = ObjectAnimator.ofPropertyValuesHolder(this, (PropertyValuesHolder[])localObject3);
    g = ((ObjectAnimator)localObject2);
    g.setDuration(l1);
    g.setStartDelay(800L);
    g.setRepeatCount(i1);
    g.setRepeatMode(m);
    localObject2 = g;
    localObject3 = new android/view/animation/AccelerateInterpolator;
    ((AccelerateInterpolator)localObject3).<init>();
    ((ObjectAnimator)localObject2).setInterpolator((TimeInterpolator)localObject3);
    localObject2 = new PropertyValuesHolder[n];
    localObject2[0] = localObject1;
    localObject1 = ObjectAnimator.ofPropertyValuesHolder(this, (PropertyValuesHolder[])localObject2);
    f = ((ObjectAnimator)localObject1);
    f.setDuration(l1);
    f.setStartDelay(700L);
    f.setRepeatCount(i1);
    f.setRepeatMode(m);
    localObject1 = f;
    localObject2 = new android/view/animation/AccelerateInterpolator;
    ((AccelerateInterpolator)localObject2).<init>();
    ((ObjectAnimator)localObject1).setInterpolator((TimeInterpolator)localObject2);
    localObject1 = new PropertyValuesHolder[n];
    localObject1[0] = paramContext;
    paramContext = ObjectAnimator.ofPropertyValuesHolder(this, (PropertyValuesHolder[])localObject1);
    e = paramContext;
    e.setDuration(l1);
    e.setStartDelay(650L);
    e.setRepeatCount(i1);
    e.setRepeatMode(m);
    paramContext = e;
    paramAttributeSet = new android/view/animation/AccelerateInterpolator;
    paramAttributeSet.<init>();
    paramContext.setInterpolator(paramAttributeSet);
  }
  
  private static void a(Canvas paramCanvas, Drawable paramDrawable, float paramFloat)
  {
    int m = (int)(paramFloat * 255.0F);
    paramDrawable.setAlpha(m);
    paramDrawable.draw(paramCanvas);
  }
  
  public final void a()
  {
    ObjectAnimator localObjectAnimator = e;
    boolean bool;
    if (localObjectAnimator != null)
    {
      bool = localObjectAnimator.isRunning();
      if (bool)
      {
        localObjectAnimator = e;
        localObjectAnimator.cancel();
      }
    }
    localObjectAnimator = f;
    if (localObjectAnimator != null)
    {
      bool = localObjectAnimator.isRunning();
      if (bool)
      {
        localObjectAnimator = f;
        localObjectAnimator.cancel();
      }
    }
    localObjectAnimator = g;
    if (localObjectAnimator != null)
    {
      bool = localObjectAnimator.isRunning();
      if (bool)
      {
        localObjectAnimator = g;
        localObjectAnimator.cancel();
      }
    }
    localObjectAnimator = h;
    if (localObjectAnimator != null)
    {
      bool = localObjectAnimator.isRunning();
      if (bool)
      {
        localObjectAnimator = h;
        localObjectAnimator.cancel();
      }
    }
  }
  
  public final void b()
  {
    Object localObject = getDrawable();
    i = ((Drawable)localObject);
    localObject = i;
    if (localObject != null)
    {
      localObject = ((Drawable)localObject).getConstantState();
      if (localObject != null)
      {
        localObject = i.getConstantState().newDrawable().mutate();
        j = ((Drawable)localObject);
        localObject = i.getConstantState().newDrawable().mutate();
        k = ((Drawable)localObject);
        localObject = i.getConstantState().newDrawable().mutate();
        l = ((Drawable)localObject);
        localObject = j;
        int m = i.getBounds().bottom;
        int n = j.getIntrinsicWidth();
        int i1 = i.getBounds().bottom;
        int i2 = j.getIntrinsicHeight();
        i1 += i2;
        ((Drawable)localObject).setBounds(0, m, n, i1);
        localObject = k;
        m = j.getBounds().bottom;
        n = k.getIntrinsicWidth();
        i1 = j.getBounds().bottom;
        int i3 = k.getIntrinsicHeight();
        i1 += i3;
        ((Drawable)localObject).setBounds(0, m, n, i1);
        localObject = l;
        m = k.getBounds().bottom;
        n = l.getIntrinsicWidth();
        i1 = k.getBounds().bottom;
        i3 = l.getIntrinsicHeight();
        i1 += i3;
        ((Drawable)localObject).setBounds(0, m, n, i1);
        e.start();
        f.start();
        g.start();
        h.start();
        return;
      }
    }
  }
  
  public float getAlphaFour()
  {
    return a;
  }
  
  public float getAlphaOne()
  {
    return d;
  }
  
  public float getAlphaThree()
  {
    return b;
  }
  
  public float getAlphaTwo()
  {
    return c;
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    a();
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    Drawable localDrawable1 = i;
    if (localDrawable1 != null)
    {
      Drawable localDrawable2 = j;
      if (localDrawable2 != null)
      {
        localDrawable2 = k;
        if (localDrawable2 != null)
        {
          localDrawable2 = l;
          if (localDrawable2 != null)
          {
            float f1 = a;
            a(paramCanvas, localDrawable1, f1);
            localDrawable1 = j;
            f1 = b;
            a(paramCanvas, localDrawable1, f1);
            localDrawable1 = k;
            f1 = c;
            a(paramCanvas, localDrawable1, f1);
            localDrawable1 = l;
            f1 = d;
            a(paramCanvas, localDrawable1, f1);
            return;
          }
        }
      }
    }
  }
  
  public void setAlphaFour(float paramFloat)
  {
    float f1 = 100.0F;
    paramFloat = Math.round(paramFloat * f1) / f1;
    a = paramFloat;
    invalidate();
  }
  
  public void setAlphaOne(float paramFloat)
  {
    float f1 = 100.0F;
    paramFloat = Math.round(paramFloat * f1) / f1;
    d = paramFloat;
    invalidate();
  }
  
  public void setAlphaThree(float paramFloat)
  {
    float f1 = 100.0F;
    paramFloat = Math.round(paramFloat * f1) / f1;
    b = paramFloat;
    invalidate();
  }
  
  public void setAlphaTwo(float paramFloat)
  {
    float f1 = 100.0F;
    paramFloat = Math.round(paramFloat * f1) / f1;
    c = paramFloat;
    invalidate();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.customviews.ArrowView
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */