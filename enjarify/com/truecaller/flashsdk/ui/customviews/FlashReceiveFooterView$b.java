package com.truecaller.flashsdk.ui.customviews;

import c.g.a.b;
import c.g.b.j;
import c.g.b.w;
import c.l.c;

final class FlashReceiveFooterView$b
  extends j
  implements b
{
  FlashReceiveFooterView$b(FlashReceiveFooterView paramFlashReceiveFooterView)
  {
    super(1, paramFlashReceiveFooterView);
  }
  
  public final c a()
  {
    return w.a(FlashReceiveFooterView.class);
  }
  
  public final String b()
  {
    return "onMessageTextChanged";
  }
  
  public final String c()
  {
    return "onMessageTextChanged(Ljava/lang/CharSequence;)V";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.customviews.FlashReceiveFooterView.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */