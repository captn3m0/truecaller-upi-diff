package com.truecaller.flashsdk.ui;

import android.graphics.PorterDuff.Mode;
import android.widget.ImageView;

final class FlashButton$1
  implements Runnable
{
  FlashButton$1(FlashButton paramFlashButton) {}
  
  public final void run()
  {
    ImageView localImageView = FlashButton.a(a);
    int i = a.c;
    PorterDuff.Mode localMode = a.f;
    localImageView.setColorFilter(i, localMode);
    a.a(0);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.FlashButton.1
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */