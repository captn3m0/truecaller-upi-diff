package com.truecaller.flashsdk.ui;

import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.ProgressBar;
import c.g.b.k;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;

public final class ProgressAwareFlashButton
  extends FlashButton
{
  private final ProgressBar h;
  private final ImageView i;
  private CountDownTimer j;
  private ProgressAwareFlashButton.b k;
  
  public ProgressAwareFlashButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private ProgressAwareFlashButton(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, (byte)0);
    int m = R.id.progress_bar;
    paramContext = findViewById(m);
    k.a(paramContext, "findViewById(R.id.progress_bar)");
    paramContext = (ProgressBar)paramContext;
    h = paramContext;
    m = R.id.flash_button_image;
    paramContext = findViewById(m);
    k.a(paramContext, "findViewById(R.id.flash_button_image)");
    paramContext = (ImageView)paramContext;
    i = paramContext;
    h.setMax(60000);
  }
  
  private final void c()
  {
    ProgressAwareFlashButton.b localb = k;
    if (localb != null)
    {
      localb.a();
      return;
    }
  }
  
  protected final void a(int paramInt)
  {
    if (paramInt == 0)
    {
      c();
      return;
    }
    h.setProgress(paramInt);
    Object localObject = j;
    if (localObject != null) {
      ((CountDownTimer)localObject).cancel();
    }
    localObject = new com/truecaller/flashsdk/ui/ProgressAwareFlashButton$a;
    long l = paramInt;
    ((ProgressAwareFlashButton.a)localObject).<init>(this, l);
    ((ProgressAwareFlashButton.a)localObject).start();
    localObject = (CountDownTimer)localObject;
    j = ((CountDownTimer)localObject);
  }
  
  public final ProgressAwareFlashButton.b getFlashProgressFinishListener()
  {
    return k;
  }
  
  protected final int getLayout()
  {
    return R.layout.progress_aware_flash_button;
  }
  
  protected final void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    CountDownTimer localCountDownTimer = j;
    if (localCountDownTimer != null)
    {
      localCountDownTimer.cancel();
      return;
    }
  }
  
  public final void setFlashProgressFinishListener(ProgressAwareFlashButton.b paramb)
  {
    k = paramb;
  }
  
  public final void setThemeColor(int paramInt)
  {
    Object localObject = i;
    PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
    ((ImageView)localObject).setColorFilter(paramInt, localMode);
    localObject = h.getBackground();
    localMode = PorterDuff.Mode.SRC_IN;
    ((Drawable)localObject).setColorFilter(paramInt, localMode);
    localObject = h.getProgressDrawable();
    localMode = PorterDuff.Mode.SRC_IN;
    ((Drawable)localObject).setColorFilter(paramInt, localMode);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.ProgressAwareFlashButton
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */