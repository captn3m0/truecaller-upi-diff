package com.truecaller.flashsdk.ui.whatsnew;

import android.os.Bundle;
import java.util.List;

public abstract interface a$b
{
  public abstract void a();
  
  public abstract void a(long paramLong, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, boolean paramBoolean, String paramString6);
  
  public abstract void a(List paramList);
  
  public abstract void b();
  
  public abstract void c();
  
  public abstract Bundle d();
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.whatsnew.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */