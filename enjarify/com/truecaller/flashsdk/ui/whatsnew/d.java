package com.truecaller.flashsdk.ui.whatsnew;

import javax.inject.Provider;

public final class d
  implements dagger.a.d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  private final Provider d;
  
  private d(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
    d = paramProvider4;
  }
  
  public static d a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3, Provider paramProvider4)
  {
    d locald = new com/truecaller/flashsdk/ui/whatsnew/d;
    locald.<init>(paramProvider1, paramProvider2, paramProvider3, paramProvider4);
    return locald;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.whatsnew.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */