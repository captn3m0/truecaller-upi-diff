package com.truecaller.flashsdk.ui.whatsnew;

import c.d.f;
import c.g.b.k;
import com.truecaller.ba;
import com.truecaller.flashsdk.assist.aa;
import com.truecaller.flashsdk.core.b;
import com.truecaller.flashsdk.models.a;

public final class c
  extends ba
  implements a.a
{
  private String c;
  private String d;
  private boolean e;
  private String f;
  private String g;
  private final f h;
  private final f i;
  private final b j;
  private final aa k;
  
  public c(f paramf1, f paramf2, b paramb, aa paramaa)
  {
    super(paramf2);
    h = paramf1;
    i = paramf2;
    j = paramb;
    k = paramaa;
  }
  
  private final void e()
  {
    aa localaa = k;
    Boolean localBoolean = Boolean.FALSE;
    localaa.a("first_time_user", localBoolean);
  }
  
  public final void a()
  {
    e();
  }
  
  public final void a(int paramInt)
  {
    int m = 16908332;
    if (paramInt == m)
    {
      a.b localb = (a.b)b;
      if (localb != null)
      {
        localb.b();
        return;
      }
    }
  }
  
  public final void a(a parama)
  {
    k.b(parama, "contact");
    Object localObject1 = b;
    Object localObject2 = localObject1;
    localObject2 = (a.b)localObject1;
    if (localObject2 != null)
    {
      localObject1 = parama.b();
      k.a(localObject1, "contact.phoneNumber");
      long l = Long.parseLong((String)localObject1);
      String str1 = parama.a();
      k.a(str1, "contact.name");
      String str2 = c;
      String str3 = f;
      String str4 = d;
      boolean bool = e;
      String str5 = g;
      ((a.b)localObject2).a(l, str1, "flashShare", str2, str3, str4, bool, str5);
      return;
    }
  }
  
  public final void y_()
  {
    super.y_();
    e();
    a.b localb = (a.b)b;
    if (localb != null)
    {
      localb.b();
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.whatsnew.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */