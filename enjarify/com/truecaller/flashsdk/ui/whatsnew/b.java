package com.truecaller.flashsdk.ui.whatsnew;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.PopupWindow;
import android.widget.TextView;
import c.g.b.k;
import c.u;
import com.truecaller.flashsdk.R.attr;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;
import com.truecaller.utils.extensions.i;

public final class b
{
  public final PopupWindow a;
  
  public b(Context paramContext, String paramString, int paramInt)
  {
    Object localObject1 = LayoutInflater.from(paramContext);
    int i = R.layout.flash_v2_pop_up;
    localObject1 = ((LayoutInflater)localObject1).inflate(i, null);
    i = R.id.text;
    Object localObject2 = ((View)localObject1).findViewById(i);
    k.a(localObject2, "view.findViewById<TextView>(R.id.text)");
    localObject2 = (TextView)localObject2;
    paramString = (CharSequence)paramString;
    ((TextView)localObject2).setText(paramString);
    paramString = new android/widget/PopupWindow;
    i = -2;
    paramString.<init>((View)localObject1, i, i, false);
    localObject2 = new android/graphics/drawable/ColorDrawable;
    ((ColorDrawable)localObject2).<init>();
    localObject2 = (Drawable)localObject2;
    paramString.setBackgroundDrawable((Drawable)localObject2);
    paramString.setAnimationStyle(16973826);
    i = 1;
    paramString.setOutsideTouchable(i);
    localObject2 = new com/truecaller/flashsdk/ui/whatsnew/b$a;
    ((b.a)localObject2).<init>(paramString);
    localObject2 = (View.OnTouchListener)localObject2;
    paramString.setTouchInterceptor((View.OnTouchListener)localObject2);
    a = paramString;
    paramString = android.support.v4.content.b.a(paramContext, paramInt);
    if (paramString != null)
    {
      paramInt = R.attr.theme_contrast_bg;
      int j = i.a(paramContext, paramInt);
      PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
      paramString.setColorFilter(j, localMode);
    }
    k.a(localObject1, "view");
    ((View)localObject1).setBackground(paramString);
  }
  
  public final void a(View paramView, int paramInt)
  {
    k.b(paramView, "view");
    Object localObject = paramView.getContext();
    if (localObject != null)
    {
      localObject = (Activity)localObject;
      boolean bool = ((Activity)localObject).isFinishing();
      if (!bool)
      {
        localObject = paramView.getApplicationWindowToken();
        if (localObject != null)
        {
          localObject = a.getContentView();
          ((View)localObject).measure(0, 0);
          int j = paramView.getMeasuredWidth() / 2;
          k.a(localObject, "contentView");
          int k = ((View)localObject).getMeasuredWidth() / 2;
          j -= k;
          PopupWindow localPopupWindow = a;
          int m = paramView.getMeasuredHeight();
          int i = ((View)localObject).getMeasuredHeight();
          m = m + i + paramInt;
          paramInt = -m;
          localPopupWindow.showAsDropDown(paramView, j, paramInt);
        }
      }
      return;
    }
    paramView = new c/u;
    paramView.<init>("null cannot be cast to non-null type android.app.Activity");
    throw paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.whatsnew.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */