package com.truecaller.flashsdk.ui.whatsnew;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff.Mode;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ProgressBar;
import c.g.b.k;
import com.d.b.w;
import com.google.android.c.a.d;
import com.truecaller.flashsdk.R.attr;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;
import com.truecaller.flashsdk.assist.x;
import com.truecaller.flashsdk.ui.a.e.a;
import com.truecaller.flashsdk.ui.send.SendActivity;
import com.truecaller.flashsdk.ui.send.SendActivity.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class FlashWithFriendsActivity
  extends AppCompatActivity
  implements x, e.a, a.b
{
  public static final FlashWithFriendsActivity.a d;
  public w a;
  public a.a b;
  public com.truecaller.flashsdk.ui.a.b c;
  private ProgressBar e;
  private com.truecaller.flashsdk.ui.a.a f;
  
  static
  {
    FlashWithFriendsActivity.a locala = new com/truecaller/flashsdk/ui/whatsnew/FlashWithFriendsActivity$a;
    locala.<init>((byte)0);
    d = locala;
    AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
  }
  
  public final void a()
  {
    int i = R.id.progressBar;
    Object localObject1 = findViewById(i);
    Object localObject2 = "findViewById(R.id.progressBar)";
    k.a(localObject1, (String)localObject2);
    localObject1 = (ProgressBar)localObject1;
    e = ((ProgressBar)localObject1);
    i = R.id.toolbarMain;
    localObject1 = (Toolbar)findViewById(i);
    setSupportActionBar((Toolbar)localObject1);
    localObject1 = getSupportActionBar();
    if (localObject1 != null)
    {
      boolean bool = true;
      ((ActionBar)localObject1).setDisplayHomeAsUpEnabled(bool);
    }
    localObject1 = c;
    if (localObject1 == null)
    {
      localObject2 = "headerItemPresenter";
      k.a((String)localObject2);
    }
    localObject2 = this;
    localObject2 = (e.a)this;
    ((com.truecaller.flashsdk.ui.a.b)localObject1).a((e.a)localObject2);
  }
  
  public final void a(int paramInt, d paramd)
  {
    k.b(paramd, "fragment");
    o localo = getSupportFragmentManager().a();
    paramd = (Fragment)paramd;
    localo.a(paramInt, paramd).e();
  }
  
  public final void a(long paramLong, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, boolean paramBoolean, String paramString6)
  {
    Object localObject1 = "name";
    k.b(paramString1, (String)localObject1);
    try
    {
      localObject1 = SendActivity.q;
      Object localObject2 = this;
      localObject2 = (Context)this;
      localObject1 = SendActivity.a.a((Context)localObject2, paramLong, paramString1, paramString2, paramString3, paramString4, paramString5, paramBoolean, paramString6);
      startActivity((Intent)localObject1);
      return;
    }
    catch (NumberFormatException localNumberFormatException) {}
  }
  
  public final void a(List paramList)
  {
    k.b(paramList, "flashableContacts");
    Object localObject1 = e;
    if (localObject1 == null)
    {
      localObject2 = "progressBar";
      k.a((String)localObject2);
    }
    ((ProgressBar)localObject1).setVisibility(8);
    int i = R.id.favouriteList;
    localObject1 = (RecyclerView)findViewById(i);
    k.a(localObject1, "recyclerView");
    ((RecyclerView)localObject1).setVisibility(0);
    Object localObject3 = new android/support/v7/widget/LinearLayoutManager;
    Object localObject4 = this;
    localObject4 = (Context)this;
    int j = 1;
    ((LinearLayoutManager)localObject3).<init>((Context)localObject4, j, false);
    localObject3 = (RecyclerView.LayoutManager)localObject3;
    ((RecyclerView)localObject1).setLayoutManager((RecyclerView.LayoutManager)localObject3);
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    Object localObject5 = localObject2;
    localObject5 = (List)localObject2;
    localObject2 = new com/truecaller/flashsdk/models/c;
    localObject3 = getIntent();
    Object localObject6 = "intent";
    k.a(localObject3, (String)localObject6);
    localObject3 = ((Intent)localObject3).getExtras();
    ((com.truecaller.flashsdk.models.c)localObject2).<init>((Bundle)localObject3);
    ((List)localObject5).add(localObject2);
    paramList = (Collection)paramList;
    ((List)localObject5).addAll(paramList);
    paramList = new com/truecaller/flashsdk/ui/a/a;
    w localw = a;
    if (localw == null)
    {
      localObject2 = "picasso";
      k.a((String)localObject2);
    }
    Object localObject7 = this;
    localObject7 = (x)this;
    com.truecaller.flashsdk.ui.a.b localb = c;
    if (localb == null)
    {
      localObject2 = "headerItemPresenter";
      k.a((String)localObject2);
    }
    localObject6 = paramList;
    paramList.<init>((Context)localObject4, localw, (List)localObject5, (x)localObject7, localb);
    f = paramList;
    paramList = (RecyclerView.Adapter)f;
    ((RecyclerView)localObject1).setAdapter(paramList);
  }
  
  public final boolean a(int paramInt)
  {
    j localj = getSupportFragmentManager();
    Fragment localFragment = localj.a(paramInt);
    return localFragment == null;
  }
  
  public final void b()
  {
    finish();
  }
  
  public final void c()
  {
    int i = R.id.tc_logo;
    ImageView localImageView = (ImageView)findViewById(i);
    Object localObject = this;
    localObject = (Context)this;
    int j = R.attr.theme_incoming_text;
    int k = com.truecaller.utils.ui.b.a((Context)localObject, j);
    PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
    localImageView.setColorFilter(k, localMode);
    k.a(localImageView, "tcLogo");
    localImageView.setVisibility(0);
  }
  
  public final Bundle d()
  {
    Intent localIntent = getIntent();
    k.a(localIntent, "intent");
    return localIntent.getExtras();
  }
  
  public final void onBackPressed()
  {
    a.a locala = b;
    if (locala == null)
    {
      String str = "flashWithFriendsPresenter";
      k.a(str);
    }
    locala.a();
    super.onBackPressed();
  }
  
  public final void onCreate(Bundle paramBundle)
  {
    int i = com.truecaller.flashsdk.core.c.a().g();
    setTheme(i);
    super.onCreate(paramBundle);
    int j = R.layout.layout_flash_whatsnew;
    setContentView(j);
    paramBundle = com.truecaller.flashsdk.core.c.b;
    paramBundle = com.truecaller.flashsdk.core.c.b();
    Object localObject = new com/truecaller/flashsdk/ui/whatsnew/a/b;
    ((com.truecaller.flashsdk.ui.whatsnew.a.b)localObject).<init>();
    paramBundle.a((com.truecaller.flashsdk.ui.whatsnew.a.b)localObject).a(this);
    paramBundle = b;
    if (paramBundle == null)
    {
      localObject = "flashWithFriendsPresenter";
      k.a((String)localObject);
    }
    paramBundle.a(this);
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem != null)
    {
      int i = paramMenuItem.getItemId();
      a.a locala = b;
      if (locala == null)
      {
        String str = "flashWithFriendsPresenter";
        k.a(str);
      }
      locala.a(i);
    }
    return true;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.whatsnew.FlashWithFriendsActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */