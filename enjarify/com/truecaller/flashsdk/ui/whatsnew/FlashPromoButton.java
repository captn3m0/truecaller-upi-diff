package com.truecaller.flashsdk.ui.whatsnew;

import android.content.Context;
import android.util.AttributeSet;
import com.truecaller.flashsdk.R.layout;
import com.truecaller.flashsdk.ui.FlashButton;

public final class FlashPromoButton
  extends FlashButton
{
  public FlashPromoButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, (byte)0);
  }
  
  private FlashPromoButton(Context paramContext, AttributeSet paramAttributeSet, char paramChar)
  {
    super(paramContext, paramAttributeSet, (byte)0);
    setClickable(false);
  }
  
  public final void a(int paramInt)
  {
    float f;
    if (paramInt > 0)
    {
      paramInt = 1056964608;
      f = 0.5F;
    }
    else
    {
      paramInt = 1065353216;
      f = 1.0F;
    }
    setAlpha(f);
  }
  
  public final int getLayout()
  {
    return R.layout.flash_button_promo;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.whatsnew.FlashPromoButton
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */