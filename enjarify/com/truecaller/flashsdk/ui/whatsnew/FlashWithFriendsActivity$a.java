package com.truecaller.flashsdk.ui.whatsnew;

import android.content.Context;
import android.content.Intent;
import c.g.b.k;

public final class FlashWithFriendsActivity$a
{
  public static Intent a(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean, String paramString5)
  {
    k.b(paramContext, "context");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>(paramContext, FlashWithFriendsActivity.class);
    localIntent.putExtra("image", paramString1);
    localIntent.putExtra("video", paramString2);
    localIntent.putExtra("description", paramString3);
    localIntent.putExtra("mode", paramBoolean);
    localIntent.putExtra("promo", paramString4);
    localIntent.putExtra("background", paramString5);
    localIntent.addFlags(268435456);
    return localIntent;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.whatsnew.FlashWithFriendsActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */