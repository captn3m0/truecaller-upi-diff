package com.truecaller.flashsdk.ui;

import android.os.CountDownTimer;
import android.widget.ProgressBar;

public final class ProgressAwareFlashButton$a
  extends CountDownTimer
{
  public ProgressAwareFlashButton$a(ProgressAwareFlashButton paramProgressAwareFlashButton, long paramLong)
  {
    super(paramLong, l);
  }
  
  public final void onFinish()
  {
    ProgressAwareFlashButton.a(a);
  }
  
  public final void onTick(long paramLong)
  {
    ProgressBar localProgressBar = ProgressAwareFlashButton.b(a);
    int i = (int)paramLong;
    localProgressBar.setProgress(i);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.ProgressAwareFlashButton.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */