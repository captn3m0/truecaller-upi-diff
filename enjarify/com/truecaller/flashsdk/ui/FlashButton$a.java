package com.truecaller.flashsdk.ui;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import java.lang.ref.WeakReference;

final class FlashButton$a
  extends ContentObserver
{
  private final Uri a;
  private final WeakReference b;
  
  FlashButton$a(Uri paramUri, Handler paramHandler, FlashButton paramFlashButton)
  {
    super(paramHandler);
    a = paramUri;
    paramUri = new java/lang/ref/WeakReference;
    paramUri.<init>(paramFlashButton);
    b = paramUri;
  }
  
  public final void onChange(boolean paramBoolean)
  {
    Uri localUri = a;
    onChange(paramBoolean, localUri);
  }
  
  public final void onChange(boolean paramBoolean, Uri paramUri)
  {
    Object localObject = b.get();
    if (localObject != null)
    {
      localObject = (FlashButton)b.get();
      FlashButton.b((FlashButton)localObject);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.FlashButton.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */