package com.truecaller.flashsdk.ui.base;

import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Pair;
import c.u;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.tasks.Task;
import com.truecaller.flashsdk.R.attr;
import com.truecaller.flashsdk.R.color;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.assist.ae;
import com.truecaller.flashsdk.assist.al;
import com.truecaller.flashsdk.assist.g;
import com.truecaller.flashsdk.assist.q;
import com.truecaller.flashsdk.assist.q.a;
import com.truecaller.flashsdk.models.FlashLocation;
import com.truecaller.utils.l;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.bg;
import kotlinx.coroutines.e;

public abstract class c
  implements b
{
  protected d a;
  protected String b;
  protected String c;
  protected String d;
  protected boolean e;
  protected boolean f;
  protected boolean g;
  protected final ae h;
  protected final al i;
  protected final g j;
  protected final com.truecaller.flashsdk.assist.a k;
  protected final com.truecaller.flashsdk.d.a l;
  final q m;
  protected final l n;
  protected final com.truecaller.common.g.a o;
  private FlashLocation p;
  private String q;
  private final c.d.f r;
  private final String[] s;
  private final String[] t;
  private Uri u;
  private com.truecaller.flashsdk.a.d[] v;
  private final com.google.firebase.messaging.a w;
  private final com.truecaller.flashsdk.assist.y x;
  private final com.google.gson.f y;
  
  public c(c.d.f paramf, ae paramae, com.google.firebase.messaging.a parama, al paramal, g paramg, com.truecaller.flashsdk.assist.a parama1, com.truecaller.flashsdk.d.a parama2, com.truecaller.flashsdk.assist.y paramy, com.google.gson.f paramf1, q paramq, l paraml, com.truecaller.common.g.a parama3)
  {
    h = paramae;
    w = parama;
    i = paramal;
    j = paramg;
    k = parama1;
    l = parama2;
    x = paramy;
    y = paramf1;
    m = paramq;
    n = paraml;
    o = parama3;
    r = paramf;
    String[] tmp158_155 = new String[3];
    String[] tmp159_158 = tmp158_155;
    String[] tmp159_158 = tmp158_155;
    tmp159_158[0] = "android.permission.READ_EXTERNAL_STORAGE";
    tmp159_158[1] = "android.permission.WRITE_EXTERNAL_STORAGE";
    tmp159_158[2] = "android.permission.CAMERA";
    paramf = tmp159_158;
    s = paramf;
    paramf = new String[] { "android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION" };
    t = paramf;
  }
  
  private final void a(com.truecaller.flashsdk.a.d paramd, int paramInt1, int paramInt2)
  {
    d locald = a;
    if (locald == null) {
      return;
    }
    String str1 = paramd.a();
    int i1 = str1.length() + paramInt1;
    int i2 = 80;
    if (i1 < i2)
    {
      paramd = paramd.a();
      String str2 = "emoticon.emoji";
      c.g.b.k.a(paramd, str2);
      locald.a(paramd, paramInt1, paramInt2, i1);
      locald.F();
    }
  }
  
  protected static String b(String paramString)
  {
    c.g.b.k.b(paramString, "history");
    int i1 = paramString.length();
    String str = "";
    Object localObject = c.n.m.a(paramString, " ", str);
    int i2 = ((String)localObject).length();
    i1 -= i2;
    i2 = 8;
    if (i1 > i2)
    {
      str = " ";
      i1 -= i2;
      localObject = paramString;
      localObject = (CharSequence)paramString;
      int i4;
      for (int i3 = c.n.m.a((CharSequence)localObject, str, 0, false, 6);; i3 = c.n.m.a((CharSequence)localObject, str, i3, false, i4))
      {
        i4 = -1;
        i1 += i4;
        if ((i1 <= 0) || (i3 == i4)) {
          break;
        }
        i3 += 1;
        i4 = 4;
      }
      paramString = paramString.substring(i3);
      c.g.b.k.a(paramString, "(this as java.lang.String).substring(startIndex)");
      return paramString;
    }
    return paramString;
  }
  
  private final void m()
  {
    d locald = a;
    if (locald == null) {
      return;
    }
    Object localObject = j;
    boolean bool = ((g)localObject).b();
    if (bool)
    {
      locald.t();
      return;
    }
    localObject = b;
    if (localObject != null)
    {
      n();
      return;
    }
    f = false;
    locald.u();
  }
  
  private final void n()
  {
    Object localObject1 = a;
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = b;
    if (localObject2 == null) {
      return;
    }
    localObject2 = (CharSequence)localObject2;
    boolean bool1 = c.n.m.a((CharSequence)localObject2);
    if (!bool1)
    {
      Object localObject3 = new c/n/k;
      ((c.n.k)localObject3).<init>(",");
      bool1 = false;
      Object localObject4 = null;
      localObject2 = ((c.n.k)localObject3).a((CharSequence)localObject2, 0);
      boolean bool2 = ((List)localObject2).isEmpty();
      int i4 = 1;
      int i2;
      Object localObject5;
      int i5;
      if (!bool2)
      {
        i2 = ((List)localObject2).size();
        localObject3 = ((List)localObject2).listIterator(i2);
        do
        {
          boolean bool4 = ((ListIterator)localObject3).hasPrevious();
          if (!bool4) {
            break;
          }
          localObject5 = (CharSequence)((ListIterator)localObject3).previous();
          i5 = ((CharSequence)localObject5).length();
          if (i5 == 0)
          {
            i5 = 1;
          }
          else
          {
            i5 = 0;
            localObject5 = null;
          }
        } while (i5 != 0);
        localObject2 = (Iterable)localObject2;
        i2 = ((ListIterator)localObject3).nextIndex() + i4;
        localObject2 = c.a.m.d((Iterable)localObject2, i2);
      }
      else
      {
        localObject2 = (List)c.a.y.a;
      }
      localObject2 = (Collection)localObject2;
      if (localObject2 != null)
      {
        localObject3 = new String[0];
        localObject2 = ((Collection)localObject2).toArray((Object[])localObject3);
        if (localObject2 != null)
        {
          localObject2 = (String[])localObject2;
          i2 = localObject2.length;
          i5 = 2;
          if (i2 < i5) {
            return;
          }
          j();
          localObject3 = o;
          Object localObject6 = "featureShareImageInFlash";
          boolean bool3 = ((com.truecaller.common.g.a)localObject3).b((String)localObject6);
          int i6;
          Object[] arrayOfObject1;
          if (bool3)
          {
            localObject3 = d;
            if (localObject3 != null)
            {
              localObject5 = localObject3;
              localObject5 = (CharSequence)localObject3;
              i5 = ((CharSequence)localObject5).length();
              if (i5 == 0)
              {
                i5 = 1;
              }
              else
              {
                i5 = 0;
                localObject5 = null;
              }
              i5 ^= i4;
              if (i5 == 0)
              {
                bool3 = false;
                localObject3 = null;
              }
              if (localObject3 != null)
              {
                localObject5 = c;
                if (localObject5 == null)
                {
                  localObject5 = i;
                  i6 = R.string.i_am_here;
                  arrayOfObject1 = new Object[0];
                  localObject5 = ((al)localObject5).a(i6, arrayOfObject1);
                }
                localObject6 = localObject2[0];
                arrayOfObject1 = localObject2[i4];
                ((d)localObject1).a((String)localObject5, (String)localObject3, (String)localObject6, arrayOfObject1);
                i3 = R.attr.theme_bg_contact_transparent_header;
                localObject5 = k;
                i6 = R.color.white;
                i5 = ((com.truecaller.flashsdk.assist.a)localObject5).a(i6);
                ((d)localObject1).a(i3, i5);
                ((d)localObject1).H();
                if (localObject1 != null) {
                  break label571;
                }
              }
            }
            localObject3 = this;
            localObject3 = (c)this;
            localObject5 = p;
            localObject6 = i;
            i7 = R.string.i_am_here;
            arrayOfObject2 = new Object[0];
            localObject6 = ((al)localObject6).a(i7, arrayOfObject2);
            localObject5 = com.truecaller.flashsdk.models.b.a((FlashLocation)localObject5, (String)localObject6);
            localObject4 = localObject2[0];
            localObject2 = localObject2[i4];
            ((d)localObject1).a((String)localObject5, (String)localObject4, (String)localObject2);
            int i8 = R.attr.theme_bg_contact_transparent_header;
            localObject4 = k;
            int i3 = R.color.white;
            i1 = ((com.truecaller.flashsdk.assist.a)localObject4).a(i3);
            ((d)localObject1).a(i8, i1);
            ((d)localObject1).H();
            label571:
            return;
          }
          localObject3 = c;
          if (localObject3 == null)
          {
            localObject3 = i;
            i6 = R.string.i_am_here;
            arrayOfObject1 = new Object[0];
            localObject3 = ((al)localObject3).a(i6, arrayOfObject1);
          }
          localObject6 = i;
          int i7 = R.string.map_url;
          int i9 = 4;
          Object[] arrayOfObject2 = new Object[i9];
          Object localObject7 = localObject2[0];
          arrayOfObject2[0] = localObject7;
          localObject7 = localObject2[i4];
          arrayOfObject2[i4] = localObject7;
          localObject4 = localObject2[0];
          arrayOfObject2[i5] = localObject4;
          int i1 = 3;
          localObject2 = localObject2[i4];
          arrayOfObject2[i1] = localObject2;
          localObject2 = ((al)localObject6).a(i7, arrayOfObject2);
          ((d)localObject1).e((String)localObject3, (String)localObject2);
        }
        else
        {
          localObject1 = new c/u;
          ((u)localObject1).<init>("null cannot be cast to non-null type kotlin.Array<T>");
          throw ((Throwable)localObject1);
        }
      }
      else
      {
        localObject1 = new c/u;
        ((u)localObject1).<init>("null cannot be cast to non-null type java.util.Collection<T>");
        throw ((Throwable)localObject1);
      }
    }
  }
  
  private final void o()
  {
    Object localObject1 = o;
    Object localObject2 = "featureShareImageInFlash";
    boolean bool1 = ((com.truecaller.common.g.a)localObject1).b((String)localObject2);
    if (!bool1) {
      return;
    }
    localObject1 = a;
    if (localObject1 == null) {
      return;
    }
    localObject2 = n;
    String[] arrayOfString = s;
    int i1 = arrayOfString.length;
    arrayOfString = (String[])Arrays.copyOf(arrayOfString, i1);
    boolean bool2 = ((l)localObject2).a(arrayOfString);
    if (bool2)
    {
      localObject2 = q.a.a(m);
      u = ((Uri)localObject2);
      localObject2 = u;
      if (localObject2 != null)
      {
        ((d)localObject1).a((Uri)localObject2);
        return;
      }
      localObject2 = this;
      localObject2 = i;
      int i2 = R.string.try_again;
      Object[] arrayOfObject = new Object[0];
      localObject2 = ((al)localObject2).a(i2, arrayOfObject);
      ((d)localObject1).d((String)localObject2);
      return;
    }
    ((d)localObject1).w();
  }
  
  public void a()
  {
    g = false;
    f = false;
    d locald = a;
    if (locald != null)
    {
      locald.A();
      return;
    }
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    int i1 = 100;
    int i2 = -1;
    Object localObject1;
    Object localObject2;
    Object localObject3;
    if (paramInt1 != i1)
    {
      i1 = 1000;
      if (paramInt1 == i1)
      {
        localObject1 = a;
        if (localObject1 == null) {
          return;
        }
        f = false;
        if (paramInt2 != i2)
        {
          localObject2 = i;
          i1 = R.string.try_again;
          localObject3 = new Object[0];
          localObject2 = ((al)localObject2).a(i1, (Object[])localObject3);
          ((d)localObject1).b((String)localObject2);
          return;
        }
        ((d)localObject1).v();
        localObject2 = i;
        i1 = R.string.flash_fetching_location;
        localObject3 = new Object[0];
        localObject2 = ((al)localObject2).a(i1, (Object[])localObject3);
        ((d)localObject1).d((String)localObject2);
      }
    }
    else
    {
      localObject1 = o;
      Object localObject4 = "featureShareImageInFlash";
      paramInt1 = ((com.truecaller.common.g.a)localObject1).b((String)localObject4);
      if (paramInt1 != 0)
      {
        localObject1 = a;
        if (localObject1 == null) {
          return;
        }
        if (paramInt2 == i2)
        {
          localObject2 = (ag)bg.a;
          localObject4 = r;
          localObject3 = new com/truecaller/flashsdk/ui/base/c$a;
          ((c.a)localObject3).<init>(this, (d)localObject1, null);
          localObject3 = (c.g.a.m)localObject3;
          e.b((ag)localObject2, (c.d.f)localObject4, (c.g.a.m)localObject3, 2);
          return;
        }
        localObject2 = i;
        i1 = R.string.try_again;
        localObject3 = new Object[0];
        localObject2 = ((al)localObject2).a(i1, (Object[])localObject3);
        ((d)localObject1).d((String)localObject2);
      }
    }
  }
  
  public void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    c.g.b.k.b(paramArrayOfString, "permissions");
    Object localObject1 = "grantResults";
    c.g.b.k.b(paramArrayOfInt, (String)localObject1);
    int i1 = 10;
    Object localObject2;
    int i3;
    if (paramInt == i1)
    {
      localObject2 = n;
      localObject1 = t;
      i3 = localObject1.length;
      localObject1 = (String[])Arrays.copyOf((Object[])localObject1, i3);
      paramInt = ((l)localObject2).a(paramArrayOfString, paramArrayOfInt, (String[])localObject1);
      if (paramInt != 0)
      {
        m();
        return;
      }
      paramInt = 0;
      localObject2 = null;
      f = false;
      paramArrayOfString = a;
      if (paramArrayOfString != null)
      {
        paramArrayOfInt = i;
        i1 = R.string.try_again;
        localObject2 = new Object[0];
        localObject2 = paramArrayOfInt.a(i1, (Object[])localObject2);
        paramArrayOfString.b((String)localObject2);
        return;
      }
      return;
    }
    localObject1 = o;
    String str = "featureShareImageInFlash";
    boolean bool = ((com.truecaller.common.g.a)localObject1).b(str);
    if (bool)
    {
      int i2 = 12;
      if (paramInt == i2)
      {
        localObject2 = n;
        localObject1 = s;
        i3 = localObject1.length;
        localObject1 = (String[])Arrays.copyOf((Object[])localObject1, i3);
        paramInt = ((l)localObject2).a(paramArrayOfString, paramArrayOfInt, (String[])localObject1);
        if (paramInt != 0)
        {
          o();
          return;
        }
        localObject2 = a;
        if (localObject2 != null)
        {
          ((d)localObject2).d("Write access denied");
          return;
        }
      }
    }
  }
  
  protected final void a(long paramLong)
  {
    Object localObject1 = h;
    ((ae)localObject1).a(paramLong);
    Object localObject2 = h.a();
    com.truecaller.flashsdk.a.d[] arrayOfd = null;
    int i1 = 4;
    localObject2 = (Collection)((List)localObject2).subList(0, i1);
    if (localObject2 != null)
    {
      arrayOfd = new com.truecaller.flashsdk.a.d[0];
      localObject2 = ((Collection)localObject2).toArray(arrayOfd);
      if (localObject2 != null)
      {
        localObject2 = (com.truecaller.flashsdk.a.d[])localObject2;
        v = ((com.truecaller.flashsdk.a.d[])localObject2);
        localObject2 = a;
        if (localObject2 != null)
        {
          arrayOfd = v;
          if (arrayOfd == null)
          {
            localObject1 = "recentEmojis";
            c.g.b.k.a((String)localObject1);
          }
          ((d)localObject2).a(arrayOfd);
          return;
        }
        return;
      }
      localObject2 = new c/u;
      ((u)localObject2).<init>("null cannot be cast to non-null type kotlin.Array<T>");
      throw ((Throwable)localObject2);
    }
    localObject2 = new c/u;
    ((u)localObject2).<init>("null cannot be cast to non-null type java.util.Collection<T>");
    throw ((Throwable)localObject2);
  }
  
  public final void a(Location paramLocation)
  {
    d locald = a;
    if (locald == null) {
      return;
    }
    if (paramLocation == null)
    {
      locald.t();
      return;
    }
    Object localObject1 = x.a(paramLocation);
    if (localObject1 == null) {
      return;
    }
    al localal = i;
    int i1 = R.string.lat_long;
    Object[] arrayOfObject = new Object[2];
    Object localObject2 = first;
    c.g.b.k.a(localObject2, "latLongCopy.first");
    arrayOfObject[0] = localObject2;
    localObject1 = second;
    c.g.b.k.a(localObject1, "latLongCopy.second");
    arrayOfObject[1] = localObject1;
    localObject1 = localal.a(i1, arrayOfObject);
    b = ((String)localObject1);
    locald.a(paramLocation);
  }
  
  public void a(Uri paramUri)
  {
    c.g.b.k.b(paramUri, "uri");
  }
  
  public final void a(Bundle paramBundle)
  {
    c.g.b.k.b(paramBundle, "resultData");
    Object localObject1 = paramBundle.getString("com.truecaller.flashsdk.assist.LOCATION_DATA_AREA");
    String str = paramBundle.getString("com.truecaller.flashsdk.assist.LOCATION_DATA_CITY");
    Object localObject2 = paramBundle.getString("com.truecaller.flashsdk.assist.LOCATION_DATA_STREET");
    Object localObject3 = new com/truecaller/flashsdk/models/FlashLocation;
    ((FlashLocation)localObject3).<init>((String)localObject2, (String)localObject1, str);
    p = ((FlashLocation)localObject3);
    localObject3 = localObject2;
    localObject3 = (CharSequence)localObject2;
    int i1 = 0;
    if (localObject3 != null)
    {
      bool1 = c.n.m.a((CharSequence)localObject3);
      if (!bool1)
      {
        bool1 = false;
        localObject3 = null;
        break label98;
      }
    }
    boolean bool1 = true;
    label98:
    if (!bool1)
    {
      localObject1 = localObject2;
    }
    else
    {
      localObject2 = localObject1;
      localObject2 = (CharSequence)localObject1;
      if (localObject2 != null)
      {
        bool2 = c.n.m.a((CharSequence)localObject2);
        if (!bool2)
        {
          bool2 = false;
          localObject2 = null;
          break label150;
        }
      }
      boolean bool2 = true;
      label150:
      if (bool2)
      {
        localObject1 = str;
        localObject1 = (CharSequence)str;
        if (localObject1 != null)
        {
          boolean bool3 = c.n.m.a((CharSequence)localObject1);
          if (!bool3) {}
        }
        else
        {
          i1 = 1;
        }
        if (i1 == 0) {
          localObject1 = str;
        } else {
          localObject1 = paramBundle.getString("com.truecaller.flashsdk.assist.RESULT_DATA_KEY");
        }
      }
    }
    c = ((String)localObject1);
    n();
  }
  
  public final void a(Task paramTask)
  {
    c.g.b.k.b(paramTask, "task");
    d locald = a;
    if (locald == null) {
      return;
    }
    boolean bool = paramTask.b();
    int i2 = 0;
    Object[] arrayOfObject = null;
    if (bool)
    {
      f = false;
      locald.v();
      return;
    }
    paramTask = paramTask.e();
    bool = paramTask instanceof ResolvableApiException;
    if (!bool) {
      paramTask = null;
    }
    paramTask = (ResolvableApiException)paramTask;
    if (paramTask == null)
    {
      paramTask = i;
      i1 = R.string.try_again;
      arrayOfObject = new Object[0];
      paramTask = paramTask.a(i1, arrayOfObject);
      locald.b(paramTask);
      return;
    }
    int i1 = paramTask.a();
    i2 = 6;
    if (i1 == i2) {
      locald.a(paramTask);
    }
  }
  
  public abstract void a(com.truecaller.flashsdk.a.d paramd);
  
  public final void a(d paramd)
  {
    c.g.b.k.b(paramd, "presenterView");
    a = paramd;
    Intent localIntent = paramd.y();
    boolean bool = a(localIntent);
    if (bool)
    {
      b(paramd);
      return;
    }
    paramd.x();
  }
  
  public final void a(String paramString)
  {
    c.g.b.k.b(paramString, "videoUrl");
    q = paramString;
  }
  
  public final void a(String paramString, int paramInt1, int paramInt2, int paramInt3)
  {
    c.g.b.k.b(paramString, "messageText");
    com.truecaller.flashsdk.a.d[] arrayOfd = v;
    if (arrayOfd == null)
    {
      String str = "recentEmojis";
      c.g.b.k.a(str);
    }
    com.truecaller.flashsdk.a.d locald = arrayOfd[paramInt1];
    paramString = (CharSequence)paramString;
    boolean bool = c.n.m.a(paramString);
    if (bool)
    {
      bool = e;
      if (!bool)
      {
        a(locald);
        bool = true;
        g = bool;
        break label88;
      }
    }
    a(locald, paramInt2, paramInt3);
    label88:
    h.a(locald);
  }
  
  public final void a(String paramString, com.truecaller.flashsdk.a.d paramd, int paramInt1, int paramInt2)
  {
    c.g.b.k.b(paramString, "messageText");
    String str = "emoticon";
    c.g.b.k.b(paramd, str);
    paramString = (CharSequence)paramString;
    boolean bool = c.n.m.a(paramString);
    if (bool)
    {
      bool = e;
      if (!bool)
      {
        a(paramd);
        break label61;
      }
    }
    a(paramd, paramInt1, paramInt2);
    label61:
    h.a(paramd);
  }
  
  public final void a(String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    c.g.b.k.b(paramString, "messageText");
    d locald = a;
    if (locald == null) {
      return;
    }
    locald.a(paramBoolean1);
    locald.F();
    boolean bool;
    if (!paramBoolean1)
    {
      if (paramBoolean2)
      {
        locald.o();
        paramString = (CharSequence)paramString;
        bool = c.n.m.a(paramString);
        if (bool) {
          locald.c(false);
        }
      }
      else
      {
        locald.p();
        locald.G();
      }
    }
    else
    {
      bool = true;
      locald.c(bool);
      locald.q();
    }
  }
  
  public final void a(boolean paramBoolean)
  {
    d locald = a;
    if (locald == null) {
      return;
    }
    com.truecaller.common.g.a locala = o;
    Object localObject1 = "featureShareImageInFlash";
    boolean bool = locala.b((String)localObject1);
    if (bool) {
      locald.B();
    }
    bool = false;
    locala = null;
    d = null;
    if (paramBoolean)
    {
      Object localObject2 = j;
      paramBoolean = ((g)localObject2).a();
      if (paramBoolean)
      {
        localObject2 = j;
        paramBoolean = ((g)localObject2).e();
        if (paramBoolean)
        {
          m();
          return;
        }
        f = true;
        locald.s();
        return;
      }
      localObject2 = i;
      int i1 = R.string.no_internet;
      localObject1 = new Object[0];
      localObject2 = ((al)localObject2).a(i1, (Object[])localObject1);
      locald.b((String)localObject2);
      return;
    }
    k();
  }
  
  public abstract boolean a(Intent paramIntent);
  
  public final void b(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      break;
    case 1: 
      o();
      return;
    case 0: 
      d locald = a;
      if (locald != null) {
        locald.B();
      }
      paramInt = 1;
      a(paramInt);
    }
  }
  
  public final void b(Location paramLocation)
  {
    d locald = a;
    if (locald == null) {
      return;
    }
    Object localObject1 = null;
    if (paramLocation != null)
    {
      Object localObject2 = x.a(paramLocation);
      if (localObject2 != null)
      {
        al localal = i;
        int i1 = R.string.lat_long;
        Object[] arrayOfObject = new Object[2];
        Object localObject3 = first;
        c.g.b.k.a(localObject3, "latLongCopy.first");
        arrayOfObject[0] = localObject3;
        localObject2 = second;
        c.g.b.k.a(localObject2, "latLongCopy.second");
        arrayOfObject[1] = localObject2;
        localObject1 = localal.a(i1, arrayOfObject);
        b = ((String)localObject1);
        locald.a(paramLocation);
        return;
      }
      paramLocation = i;
      i2 = R.string.try_again;
      localObject1 = new Object[0];
      paramLocation = paramLocation.a(i2, (Object[])localObject1);
      locald.b(paramLocation);
      return;
    }
    paramLocation = i;
    int i2 = R.string.try_again;
    localObject1 = new Object[0];
    paramLocation = paramLocation.a(i2, (Object[])localObject1);
    locald.b(paramLocation);
  }
  
  public void b(d paramd)
  {
    c.g.b.k.b(paramd, "presenterView");
    paramd.k();
    com.truecaller.flashsdk.assist.a locala = k;
    int i1 = R.attr.theme_incoming_secondary_text;
    int i2 = locala.b(i1);
    paramd.d_(i2);
    locala = k;
    i1 = R.attr.theme_incoming_secondary_text;
    i2 = locala.b(i1);
    paramd.b(i2);
    paramd.a(true);
  }
  
  protected final void c(int paramInt)
  {
    int i1 = 4;
    Object localObject;
    if (paramInt != i1)
    {
      i1 = 8;
      if (paramInt != i1)
      {
        switch (paramInt)
        {
        default: 
          break;
        case 2: 
          localObject = a;
          if (localObject != null) {
            ((d)localObject).n();
          }
          return;
        case 1: 
          l();
          return;
        case 0: 
          return;
        }
      }
      else
      {
        localObject = a;
        if (localObject != null) {
          ((d)localObject).m();
        }
      }
    }
    else
    {
      localObject = o;
      String str = "featureShareImageInFlash";
      paramInt = ((com.truecaller.common.g.a)localObject).b(str);
      if (paramInt == 0)
      {
        localObject = a;
        if (localObject != null) {
          ((d)localObject).E();
        }
        return;
      }
    }
  }
  
  public void d()
  {
    a = null;
  }
  
  public final void e()
  {
    d locald = a;
    if (locald != null)
    {
      String str = q;
      if (str == null) {
        return;
      }
      locald.c(str);
      return;
    }
  }
  
  public void f() {}
  
  public void g()
  {
    d locald = a;
    if (locald != null)
    {
      locald.N();
      return;
    }
  }
  
  public final void i()
  {
    n();
  }
  
  public void j()
  {
    f = false;
  }
  
  public void k()
  {
    Object localObject = o;
    String str = "featureShareImageInFlash";
    boolean bool = ((com.truecaller.common.g.a)localObject).b(str);
    if (bool)
    {
      localObject = a;
      if (localObject != null) {
        ((d)localObject).D();
      }
      bool = false;
      b = null;
      localObject = a;
      if (localObject != null)
      {
        int i1 = R.attr.theme_bg_contact_header;
        com.truecaller.flashsdk.assist.a locala = k;
        int i2 = R.attr.theme_incoming_text;
        int i3 = locala.b(i2);
        ((d)localObject).a(i1, i3);
      }
      return;
    }
    localObject = a;
    if (localObject != null)
    {
      ((d)localObject).C();
      return;
    }
  }
  
  protected abstract void l();
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.base.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */