package com.truecaller.flashsdk.ui.base;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.support.v4.app.f;
import android.support.v4.app.j;
import android.support.v4.app.o;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.style.RelativeSizeSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import c.g.b.k;
import c.l.g;
import c.u;
import com.d.b.ab;
import com.d.b.ag;
import com.d.b.ai;
import com.d.b.w;
import com.google.android.c.a.c.a;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsRequest.Builder;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.truecaller.common.h.aq.d;
import com.truecaller.featuretoggles.e;
import com.truecaller.featuretoggles.e.a;
import com.truecaller.flashsdk.R.dimen;
import com.truecaller.flashsdk.R.drawable;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.menu;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.a.b.b;
import com.truecaller.flashsdk.a.f.a;
import com.truecaller.flashsdk.assist.FetchAddressIntentService;
import com.truecaller.flashsdk.assist.ae;
import com.truecaller.flashsdk.assist.t;
import com.truecaller.flashsdk.ui.customviews.FlashContactHeaderView;
import com.truecaller.flashsdk.ui.customviews.b.a;
import com.truecaller.flashsdk.ui.onboarding.FlashOnBoardingActivity;
import java.util.HashMap;

public abstract class BaseFlashActivity
  extends AppCompatActivity
  implements c.a, OnCompleteListener, b.b, com.truecaller.flashsdk.a.c.b, f.a, d, b.a
{
  public b a;
  public w b;
  public e c;
  public com.truecaller.common.g.a d;
  protected final Handler e;
  protected com.truecaller.flashsdk.ui.customviews.b f;
  protected FlashContactHeaderView g;
  protected View h;
  protected ImageView i;
  public ImageView j;
  protected com.truecaller.flashsdk.a.a k;
  protected View l;
  protected MapView m;
  protected Toolbar n;
  public Bitmap o;
  protected GoogleMap p;
  private com.google.android.c.a.c q;
  private ProgressBar r;
  private ResultReceiver s;
  private FusedLocationProviderClient t;
  private final com.truecaller.flashsdk.core.b u;
  private final ag v;
  private HashMap w;
  
  public BaseFlashActivity()
  {
    AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    Object localObject = new android/os/Handler;
    ((Handler)localObject).<init>();
    e = ((Handler)localObject);
    localObject = com.truecaller.flashsdk.core.c.a();
    u = ((com.truecaller.flashsdk.core.b)localObject);
    localObject = new com/truecaller/flashsdk/ui/base/BaseFlashActivity$c;
    ((BaseFlashActivity.c)localObject).<init>(this);
    localObject = (ag)localObject;
    v = ((ag)localObject);
  }
  
  public void A()
  {
    Object localObject1 = l;
    if (localObject1 == null)
    {
      str = "videoContainer";
      k.a(str);
    }
    int i1 = 8;
    ((View)localObject1).setVisibility(i1);
    int i2 = R.id.waiting_container;
    localObject1 = findViewById(i2);
    Object localObject2 = getSupportFragmentManager();
    int i3 = R.id.waiting_container;
    Fragment localFragment = ((j)localObject2).a(i3);
    localObject2 = ((j)localObject2).a();
    if (localFragment == null) {
      return;
    }
    ((o)localObject2).a(localFragment).e();
    localObject2 = "view";
    k.a(localObject1, (String)localObject2);
    ((View)localObject1).setVisibility(i1);
    localObject1 = i;
    if (localObject1 == null)
    {
      localObject2 = "imageContent";
      k.a((String)localObject2);
    }
    localObject2 = null;
    ((ImageView)localObject1).setImageDrawable(null);
    localObject1 = j;
    if (localObject1 != null) {
      ((ImageView)localObject1).setImageDrawable(null);
    }
    localObject1 = h;
    if (localObject1 == null)
    {
      localObject2 = "imageContainer";
      k.a((String)localObject2);
    }
    ((View)localObject1).setVisibility(i1);
    localObject1 = f;
    if (localObject1 == null)
    {
      str = "footerView";
      k.a(str);
    }
    i1 = 0;
    String str = null;
    ((com.truecaller.flashsdk.ui.customviews.b)localObject1).a(false);
    localObject1 = f;
    if (localObject1 == null)
    {
      localObject2 = "footerView";
      k.a((String)localObject2);
    }
    ((com.truecaller.flashsdk.ui.customviews.b)localObject1).setVisibility(0);
    localObject1 = f;
    if (localObject1 == null)
    {
      str = "footerView";
      k.a(str);
    }
    ((com.truecaller.flashsdk.ui.customviews.b)localObject1).d();
  }
  
  public final void M_()
  {
    Object localObject = k;
    if (localObject != null)
    {
      localObject = ((com.truecaller.flashsdk.a.a)localObject).c();
      if (localObject != null)
      {
        boolean bool = ((Boolean)localObject).booleanValue();
        b localb = a;
        if (localb == null)
        {
          String str = "presenter";
          k.a(str);
        }
        localb.b(bool);
        return;
      }
    }
  }
  
  public final void a()
  {
    b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localb.c();
  }
  
  public final void a(Location paramLocation)
  {
    k.b(paramLocation, "lastLocation");
    Object localObject1 = FetchAddressIntentService.a;
    localObject1 = this;
    localObject1 = (Context)this;
    Object localObject2 = s;
    if (localObject2 == null)
    {
      localObject3 = "addressResultReceiver";
      k.a((String)localObject3);
    }
    k.b(localObject1, "context");
    k.b(localObject2, "resultReceiver");
    k.b(paramLocation, "location");
    Object localObject3 = new android/content/Intent;
    ((Intent)localObject3).<init>((Context)localObject1, FetchAddressIntentService.class);
    localObject2 = (Parcelable)localObject2;
    ((Intent)localObject3).putExtra("com.truecaller.flashsdk.assist.RECEIVER", (Parcelable)localObject2);
    paramLocation = (Parcelable)paramLocation;
    ((Intent)localObject3).putExtra("com.truecaller.flashsdk.assist.LOCATION_DATA_EXTRA", paramLocation);
    ((Context)localObject1).startService((Intent)localObject3);
  }
  
  public final void a(Uri paramUri)
  {
    k.b(paramUri, "fileUri");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.media.action.IMAGE_CAPTURE");
    Object localObject1 = "output";
    Object localObject2 = paramUri;
    localObject2 = (Parcelable)paramUri;
    localIntent.putExtra((String)localObject1, (Parcelable)localObject2);
    int i1 = Build.VERSION.SDK_INT;
    int i2 = 21;
    if (i1 <= i2)
    {
      localObject1 = (CharSequence)"";
      paramUri = ClipData.newRawUri((CharSequence)localObject1, paramUri);
      localIntent.setClipData(paramUri);
      int i3 = 3;
      localIntent.addFlags(i3);
    }
    startActivityForResult(localIntent, 100);
  }
  
  public final void a(com.google.android.c.a.c.c paramc, com.google.android.c.a.b paramb)
  {
    k.b(paramc, "provider");
    k.b(paramb, "youTubeInitializationResult");
    paramc = this;
    paramc = (Context)this;
    int i1 = R.string.error_youtube_player;
    paramb = (CharSequence)getString(i1);
    Toast.makeText(paramc, paramb, 0).show();
  }
  
  public final void a(com.google.android.c.a.c.c paramc, com.google.android.c.a.c paramc1, boolean paramBoolean)
  {
    String str = "provider";
    k.b(paramc, str);
    paramc = "youTubePlayer";
    k.b(paramc1, paramc);
    q = paramc1;
    if (!paramBoolean)
    {
      paramc = q;
      if (paramc != null)
      {
        paramc1 = com.google.android.c.a.c.b.b;
        paramc.a(paramc1);
      }
      paramc = a;
      if (paramc == null)
      {
        paramc1 = "presenter";
        k.a(paramc1);
      }
      paramc.e();
    }
  }
  
  public final void a(ResolvableApiException paramResolvableApiException)
  {
    k.b(paramResolvableApiException, "exception");
    Object localObject = this;
    try
    {
      localObject = (Activity)this;
      paramResolvableApiException.a((Activity)localObject);
      return;
    }
    catch (IntentSender.SendIntentException localSendIntentException) {}
  }
  
  public final void a(ae paramae, long paramLong)
  {
    k.b(paramae, "recentEmojiManager");
    Object localObject1 = this;
    localObject1 = (f.a)this;
    Object localObject2 = c;
    if (localObject2 == null)
    {
      localObject3 = "featuresRegistry";
      k.a((String)localObject3);
    }
    Object localObject3 = m;
    Object localObject4 = e.a;
    int i1 = 39;
    localObject4 = localObject4[i1];
    localObject2 = ((e.a)localObject3).a((e)localObject2, (g)localObject4);
    boolean bool1 = ((com.truecaller.featuretoggles.b)localObject2).a();
    View localView;
    if (bool1)
    {
      localObject2 = new com/truecaller/flashsdk/a/c;
      localObject4 = this;
      localObject4 = (Context)this;
      localView = l();
      localObject3 = localObject2;
      ((com.truecaller.flashsdk.a.c)localObject2).<init>((Context)localObject4, localView, (f.a)localObject1, paramae, paramLong);
      boolean bool2 = c;
      if (bool2)
      {
        paramae = e.getViewTreeObserver();
        localObject5 = new com/truecaller/flashsdk/a/c$c;
        ((com.truecaller.flashsdk.a.c.c)localObject5).<init>((com.truecaller.flashsdk.a.c)localObject2);
        localObject5 = (ViewTreeObserver.OnGlobalLayoutListener)localObject5;
        paramae.addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)localObject5);
      }
      else
      {
        paramae = d.getResources();
        int i3 = R.dimen.keyboard_height;
        float f1 = paramae.getDimension(i3);
        int i2 = (int)f1;
        b = i2;
        i2 = b;
        ((com.truecaller.flashsdk.a.c)localObject2).a(i2);
      }
      paramae = this;
      paramae = (com.truecaller.flashsdk.a.c.b)this;
      Object localObject5 = "onSoftKeyboardOpenCloseListener";
      k.b(paramae, (String)localObject5);
      a = paramae;
      localObject2 = (com.truecaller.flashsdk.a.a)localObject2;
    }
    else
    {
      localObject2 = new com/truecaller/flashsdk/a/b;
      localObject4 = this;
      localObject4 = (Context)this;
      localView = l();
      localObject3 = localObject2;
      ((com.truecaller.flashsdk.a.b)localObject2).<init>((Context)localObject4, localView, (f.a)localObject1, paramae, paramLong);
      ((com.truecaller.flashsdk.a.b)localObject2).e();
      paramae = this;
      paramae = (b.b)this;
      ((com.truecaller.flashsdk.a.b)localObject2).a(paramae);
      localObject2 = (com.truecaller.flashsdk.a.a)localObject2;
    }
    k = ((com.truecaller.flashsdk.a.a)localObject2);
  }
  
  public void a(String paramString)
  {
    k.b(paramString, "contactImageUrl");
    Object localObject1 = g;
    if (localObject1 == null)
    {
      localObject2 = "contactHeaderView";
      k.a((String)localObject2);
    }
    Object localObject2 = b;
    if (localObject2 == null)
    {
      String str = "picasso";
      k.a(str);
    }
    k.b(paramString, "contactImageUrl");
    k.b(localObject2, "picasso");
    paramString = ((w)localObject2).a(paramString);
    localObject2 = (ai)aq.d.b();
    paramString = paramString.a((ai)localObject2);
    int i1 = R.drawable.ic_empty_avatar;
    paramString = paramString.a(i1);
    localObject1 = b;
    paramString.a((ImageView)localObject1, null);
  }
  
  public void a(String paramString1, String paramString2)
  {
    k.b(paramString1, "firstLine");
    k.b(paramString2, "boldText");
    Object localObject1 = g;
    if (localObject1 == null)
    {
      localObject2 = "contactHeaderView";
      k.a((String)localObject2);
    }
    k.b(paramString1, "firstText");
    k.b(paramString2, "boldText");
    localObject1 = a;
    k.b(localObject1, "receiver$0");
    k.b(paramString1, "firstPart");
    k.b(paramString2, "boldText");
    Object localObject2 = new android/text/SpannableStringBuilder;
    paramString1 = (CharSequence)paramString1;
    ((SpannableStringBuilder)localObject2).<init>(paramString1);
    paramString1 = new android/text/style/RelativeSizeSpan;
    paramString1.<init>(0.9F);
    int i1 = ((SpannableStringBuilder)localObject2).length();
    ((SpannableStringBuilder)localObject2).setSpan(paramString1, 0, i1, 33);
    t.a((TextView)localObject1, (SpannableStringBuilder)localObject2);
    paramString2 = (CharSequence)paramString2;
    ((SpannableStringBuilder)localObject2).append(paramString2);
    localObject2 = (CharSequence)localObject2;
    ((TextView)localObject1).setText((CharSequence)localObject2);
  }
  
  public final void a(boolean paramBoolean)
  {
    com.truecaller.flashsdk.ui.customviews.b localb = f;
    if (localb == null)
    {
      String str = "footerView";
      k.a(str);
    }
    localb.b(paramBoolean);
  }
  
  public final void a(com.truecaller.flashsdk.a.d[] paramArrayOfd)
  {
    k.b(paramArrayOfd, "emojisList");
    com.truecaller.flashsdk.ui.customviews.b localb = f;
    if (localb == null)
    {
      String str = "footerView";
      k.a(str);
    }
    localb.setRecentEmojis(paramArrayOfd);
  }
  
  public final void b(int paramInt)
  {
    int i1 = R.id.tc_logo;
    ImageView localImageView = (ImageView)findViewById(i1);
    PorterDuff.Mode localMode = PorterDuff.Mode.SRC_IN;
    localImageView.setColorFilter(paramInt, localMode);
  }
  
  public final void b(String paramString)
  {
    String str = "message";
    k.b(paramString, str);
    d(paramString);
    paramString = f;
    if (paramString == null)
    {
      str = "footerView";
      k.a(str);
    }
    paramString.a(false);
  }
  
  public void b(String paramString1, String paramString2)
  {
    k.b(paramString1, "imageUrl");
    String str = "message";
    k.b(paramString2, str);
    paramString2 = h;
    if (paramString2 == null)
    {
      str = "imageContainer";
      k.a(str);
    }
    paramString2.setVisibility(0);
    int i1 = R.id.imageContent;
    paramString2 = findViewById(i1);
    str = "findViewById(R.id.imageContent)";
    k.a(paramString2, str);
    paramString2 = (ImageView)paramString2;
    i = paramString2;
    paramString2 = b;
    if (paramString2 == null)
    {
      str = "picasso";
      k.a(str);
    }
    paramString1 = paramString2.a(paramString1);
    i1 = R.drawable.ic_map_placeholder;
    paramString1 = paramString1.a(i1);
    i1 = R.drawable.ic_map_placeholder;
    paramString1 = paramString1.b(i1);
    paramString2 = v;
    paramString1.a(paramString2);
  }
  
  public final void b(boolean paramBoolean)
  {
    b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localb.a(paramBoolean);
  }
  
  public final b c()
  {
    b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    return localb;
  }
  
  public void c(int paramInt)
  {
    FlashContactHeaderView localFlashContactHeaderView = g;
    if (localFlashContactHeaderView == null)
    {
      String str = "contactHeaderView";
      k.a(str);
    }
    b.setImageResource(paramInt);
  }
  
  public final void c(String paramString)
  {
    k.b(paramString, "videoUrl");
    com.google.android.c.a.c localc = q;
    if (localc != null)
    {
      localc.a(paramString);
      return;
    }
  }
  
  public void c(String paramString1, String paramString2)
  {
    k.b(paramString1, "imageUrl");
    k.b(paramString2, "message");
    int i1 = R.id.imageContentV2;
    paramString2 = findViewById(i1);
    String str = "findViewById(R.id.imageContentV2)";
    k.a(paramString2, str);
    paramString2 = (ImageView)paramString2;
    i = paramString2;
    i1 = R.id.imageBackgroundV2;
    paramString2 = (ImageView)findViewById(i1);
    j = paramString2;
    i1 = R.id.imageProgressBar;
    paramString2 = (ProgressBar)findViewById(i1);
    r = paramString2;
    paramString2 = h;
    if (paramString2 == null)
    {
      str = "imageContainer";
      k.a(str);
    }
    int i2 = 8;
    paramString2.setVisibility(i2);
    paramString2 = b;
    if (paramString2 == null)
    {
      str = "picasso";
      k.a(str);
    }
    paramString1 = paramString2.a(paramString1);
    i1 = R.drawable.ic_flash_media_placeholder;
    paramString1 = paramString1.a(i1);
    i1 = R.drawable.ic_flash_media_placeholder;
    paramString1 = paramString1.b(i1);
    paramString2 = v;
    paramString1.a(paramString2);
  }
  
  public final w d()
  {
    w localw = b;
    if (localw == null)
    {
      String str = "picasso";
      k.a(str);
    }
    return localw;
  }
  
  public void d(int paramInt)
  {
    Object localObject = this;
    localObject = (Activity)this;
    String[] arrayOfString = { "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE" };
    android.support.v4.app.a.a((Activity)localObject, arrayOfString, paramInt);
  }
  
  public final void d(String paramString)
  {
    k.b(paramString, "message");
    Object localObject = this;
    localObject = (Context)this;
    paramString = (CharSequence)paramString;
    Toast.makeText((Context)localObject, paramString, 0).show();
  }
  
  public void d(String paramString1, String paramString2)
  {
    k.b(paramString1, "videoUrl");
    Object localObject1 = "message";
    k.b(paramString2, (String)localObject1);
    try
    {
      paramString2 = l;
      if (paramString2 == null)
      {
        localObject1 = "videoContainer";
        k.a((String)localObject1);
      }
      int i1 = 0;
      localObject1 = null;
      paramString2.setVisibility(0);
      paramString2 = a;
      if (paramString2 == null)
      {
        localObject1 = "presenter";
        k.a((String)localObject1);
      }
      paramString2.a(paramString1);
      paramString1 = new com/google/android/c/a/d;
      paramString1.<init>();
      paramString2 = getSupportFragmentManager();
      paramString2 = paramString2.a();
      i1 = R.id.youtubeFragment;
      Object localObject2 = paramString1;
      localObject2 = (Fragment)paramString1;
      paramString2 = paramString2.a(i1, (Fragment)localObject2);
      paramString2.e();
      paramString2 = "AIzaSyCd6tpLEKJi-5w6SDpTpzj6UTZpS47j7fw";
      localObject1 = this;
      localObject1 = (c.a)this;
      paramString1.a(paramString2, (c.a)localObject1);
      return;
    }
    catch (Exception localException) {}
  }
  
  public final void d_(int paramInt)
  {
    Object localObject1 = n;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject2 = "toolbar";
      k.a((String)localObject2);
    }
    setSupportActionBar((Toolbar)localObject1);
    localObject1 = this;
    localObject1 = (Context)this;
    int i1 = R.drawable.ic_close_flash;
    localObject1 = android.support.v4.content.b.a((Context)localObject1, i1);
    if (localObject1 != null) {
      localObject1 = ((Drawable)localObject1).mutate();
    } else {
      localObject1 = null;
    }
    if (localObject1 != null)
    {
      localObject2 = PorterDuff.Mode.SRC_IN;
      ((Drawable)localObject1).setColorFilter(paramInt, (PorterDuff.Mode)localObject2);
    }
    ActionBar localActionBar = getSupportActionBar();
    if (localActionBar != null) {
      localActionBar.setHomeAsUpIndicator((Drawable)localObject1);
    }
    localActionBar = getSupportActionBar();
    if (localActionBar != null)
    {
      localActionBar.setDisplayHomeAsUpEnabled(true);
      return;
    }
  }
  
  public View e(int paramInt)
  {
    Object localObject1 = w;
    if (localObject1 == null)
    {
      localObject1 = new java/util/HashMap;
      ((HashMap)localObject1).<init>();
      w = ((HashMap)localObject1);
    }
    localObject1 = w;
    Object localObject2 = Integer.valueOf(paramInt);
    localObject1 = (View)((HashMap)localObject1).get(localObject2);
    if (localObject1 == null)
    {
      localObject1 = findViewById(paramInt);
      localObject2 = w;
      Integer localInteger = Integer.valueOf(paramInt);
      ((HashMap)localObject2).put(localInteger, localObject1);
    }
    return (View)localObject1;
  }
  
  protected final com.truecaller.flashsdk.ui.customviews.b e()
  {
    com.truecaller.flashsdk.ui.customviews.b localb = f;
    if (localb == null)
    {
      String str = "footerView";
      k.a(str);
    }
    return localb;
  }
  
  public final FlashContactHeaderView f()
  {
    FlashContactHeaderView localFlashContactHeaderView = g;
    if (localFlashContactHeaderView == null)
    {
      String str = "contactHeaderView";
      k.a(str);
    }
    return localFlashContactHeaderView;
  }
  
  public final View g()
  {
    View localView = h;
    if (localView == null)
    {
      String str = "imageContainer";
      k.a(str);
    }
    return localView;
  }
  
  protected final ImageView h()
  {
    ImageView localImageView = i;
    if (localImageView == null)
    {
      String str = "imageContent";
      k.a(str);
    }
    return localImageView;
  }
  
  protected final View i()
  {
    View localView = l;
    if (localView == null)
    {
      String str = "videoContainer";
      k.a(str);
    }
    return localView;
  }
  
  protected final Toolbar j()
  {
    Toolbar localToolbar = n;
    if (localToolbar == null)
    {
      String str = "toolbar";
      k.a(str);
    }
    return localToolbar;
  }
  
  public void k()
  {
    int i1 = R.id.flash_contact_header_view;
    Object localObject = findViewById(i1);
    k.a(localObject, "findViewById(R.id.flash_contact_header_view)");
    localObject = (FlashContactHeaderView)localObject;
    g = ((FlashContactHeaderView)localObject);
    i1 = R.id.footerView;
    localObject = findViewById(i1);
    k.a(localObject, "findViewById(R.id.footerView)");
    localObject = (com.truecaller.flashsdk.ui.customviews.b)localObject;
    f = ((com.truecaller.flashsdk.ui.customviews.b)localObject);
    i1 = R.id.toolbarMain;
    localObject = findViewById(i1);
    k.a(localObject, "findViewById(R.id.toolbarMain)");
    localObject = (Toolbar)localObject;
    n = ((Toolbar)localObject);
    i1 = R.id.imageContent;
    localObject = findViewById(i1);
    k.a(localObject, "findViewById(R.id.imageContent)");
    localObject = (ImageView)localObject;
    i = ((ImageView)localObject);
    i1 = R.id.flashImageContainer;
    localObject = findViewById(i1);
    k.a(localObject, "findViewById(R.id.flashImageContainer)");
    h = ((View)localObject);
    i1 = R.id.flashYoutubeContainer;
    localObject = findViewById(i1);
    k.a(localObject, "findViewById(R.id.flashYoutubeContainer)");
    l = ((View)localObject);
  }
  
  public abstract View l();
  
  public final void m()
  {
    Object localObject1 = new com/truecaller/flashsdk/ui/whatsnew/b;
    Object localObject2 = this;
    localObject2 = (Context)this;
    int i1 = R.string.tip_use_tutorial;
    Object localObject3 = getString(i1);
    String str1 = "getString(R.string.tip_use_tutorial)";
    k.a(localObject3, str1);
    int i2 = R.drawable.flash_ic_tooltip_top_right;
    ((com.truecaller.flashsdk.ui.whatsnew.b)localObject1).<init>((Context)localObject2, (String)localObject3, i2);
    localObject2 = n;
    if (localObject2 == null)
    {
      localObject3 = "toolbar";
      k.a((String)localObject3);
    }
    localObject2 = (View)localObject2;
    k.b(localObject2, "view");
    localObject3 = ((View)localObject2).getContext();
    if (localObject3 != null)
    {
      localObject3 = (Activity)localObject3;
      boolean bool = ((Activity)localObject3).isFinishing();
      if (!bool)
      {
        localObject3 = ((View)localObject2).getApplicationWindowToken();
        if (localObject3 != null)
        {
          localObject3 = a.getContentView();
          str1 = null;
          ((View)localObject3).measure(0, 0);
          localObject3 = a;
          i2 = ((View)localObject2).getMeasuredWidth();
          localObject1 = a.getContentView();
          String str2 = "popupWindow.contentView";
          k.a(localObject1, str2);
          int i3 = ((View)localObject1).getMeasuredWidth();
          i2 -= i3;
          i3 = -((View)localObject2).getMeasuredHeight() / 4;
          ((PopupWindow)localObject3).showAsDropDown((View)localObject2, i2, i3);
        }
      }
      return;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type android.app.Activity");
    throw ((Throwable)localObject1);
  }
  
  public final void n()
  {
    com.truecaller.flashsdk.ui.customviews.b localb = f;
    if (localb == null)
    {
      String str = "footerView";
      k.a(str);
    }
    localb.c();
  }
  
  public final void o()
  {
    com.truecaller.flashsdk.a.a locala = k;
    if (locala != null)
    {
      locala.a();
      return;
    }
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    paramIntent = a;
    if (paramIntent == null)
    {
      String str = "presenter";
      k.a(str);
    }
    paramIntent.a(paramInt1, paramInt2);
  }
  
  public void onComplete(Task paramTask)
  {
    k.b(paramTask, "task");
    b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localb.a(paramTask);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i1 = u.g();
    setTheme(i1);
    paramBundle = this;
    paramBundle = LocationServices.b((Activity)this);
    k.a(paramBundle, "LocationServices.getFuse…ationProviderClient(this)");
    t = paramBundle;
    paramBundle = new com/truecaller/flashsdk/ui/base/BaseFlashActivity$AddressResultReceiver;
    Handler localHandler = e;
    paramBundle.<init>(this, localHandler);
    paramBundle = (ResultReceiver)paramBundle;
    s = paramBundle;
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    k.b(paramMenu, "menu");
    MenuInflater localMenuInflater = getMenuInflater();
    int i1 = R.menu.flash_menu_top;
    localMenuInflater.inflate(i1, paramMenu);
    return super.onCreateOptionsMenu(paramMenu);
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject2 = "picasso";
      k.a((String)localObject2);
    }
    Object localObject2 = v;
    ((w)localObject1).d(localObject2);
    try
    {
      localObject1 = q;
      if (localObject1 == null) {
        return;
      }
      boolean bool = ((com.google.android.c.a.c)localObject1).b();
      if (bool) {
        ((com.google.android.c.a.c)localObject1).a();
      }
    }
    catch (IllegalStateException localIllegalStateException)
    {
      localObject1 = (Throwable)localIllegalStateException;
      com.truecaller.log.d.a((Throwable)localObject1);
    }
    localObject1 = a;
    if (localObject1 == null)
    {
      localObject2 = "presenter";
      k.a((String)localObject2);
    }
    ((b)localObject1).d();
  }
  
  public void onNewIntent(Intent paramIntent)
  {
    Object localObject = "intent";
    k.b(paramIntent, (String)localObject);
    super.onNewIntent(paramIntent);
    setIntent(paramIntent);
    paramIntent = a;
    if (paramIntent == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    paramIntent.a();
    paramIntent = a;
    if (paramIntent == null)
    {
      localObject = "presenter";
      k.a((String)localObject);
    }
    localObject = this;
    localObject = (d)this;
    paramIntent.a((d)localObject);
  }
  
  public final boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    k.b(paramMenuItem, "item");
    b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    int i1 = paramMenuItem.getItemId();
    return localb.a(i1);
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    k.b(paramArrayOfString, "permissions");
    k.b(paramArrayOfInt, "grantResults");
    b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localb.a(paramInt, paramArrayOfString, paramArrayOfInt);
  }
  
  public final void p()
  {
    com.truecaller.flashsdk.a.a locala = k;
    if (locala != null)
    {
      locala.b();
      return;
    }
  }
  
  public final void q()
  {
    com.truecaller.flashsdk.a.a locala = k;
    if (locala != null)
    {
      locala.dismiss();
      return;
    }
  }
  
  public final void r()
  {
    Object localObject = FlashOnBoardingActivity.b;
    localObject = this;
    localObject = (Context)this;
    k.b(localObject, "context");
    Intent localIntent = new android/content/Intent;
    localIntent.<init>((Context)localObject, FlashOnBoardingActivity.class);
    ((Context)localObject).startActivity(localIntent);
  }
  
  public final void s()
  {
    Object localObject = this;
    localObject = (Activity)this;
    String[] arrayOfString = { "android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION" };
    android.support.v4.app.a.a((Activity)localObject, arrayOfString, 10);
  }
  
  public final void t()
  {
    Object localObject1 = LocationRequest.a();
    ((LocationRequest)localObject1).b();
    Object localObject2 = new com/google/android/gms/location/LocationSettingsRequest$Builder;
    ((LocationSettingsRequest.Builder)localObject2).<init>();
    localObject1 = ((LocationSettingsRequest.Builder)localObject2).a((LocationRequest)localObject1);
    ((LocationSettingsRequest.Builder)localObject1).a();
    localObject2 = this;
    localObject2 = (Activity)this;
    Object localObject3 = LocationServices.a((Activity)localObject2);
    localObject1 = ((LocationSettingsRequest.Builder)localObject1).b();
    localObject1 = ((SettingsClient)localObject3).a((LocationSettingsRequest)localObject1);
    localObject3 = this;
    localObject3 = (OnCompleteListener)this;
    ((Task)localObject1).a((Activity)localObject2, (OnCompleteListener)localObject3);
  }
  
  public final void u()
  {
    Object localObject1 = t;
    if (localObject1 == null)
    {
      localObject2 = "fusedLocationClient";
      k.a((String)localObject2);
    }
    localObject1 = ((FusedLocationProviderClient)localObject1).a();
    Object localObject2 = this;
    localObject2 = (Activity)this;
    Object localObject3 = new com/truecaller/flashsdk/ui/base/BaseFlashActivity$a;
    Object localObject4 = a;
    if (localObject4 == null)
    {
      String str = "presenter";
      k.a(str);
    }
    ((BaseFlashActivity.a)localObject3).<init>((b)localObject4);
    localObject3 = (c.g.a.b)localObject3;
    localObject4 = new com/truecaller/flashsdk/ui/base/a;
    ((a)localObject4).<init>((c.g.a.b)localObject3);
    localObject4 = (OnSuccessListener)localObject4;
    ((Task)localObject1).a((Activity)localObject2, (OnSuccessListener)localObject4);
  }
  
  public final void v()
  {
    LocationRequest localLocationRequest = LocationRequest.a();
    localLocationRequest.b();
    localLocationRequest.e();
    localLocationRequest.c();
    localLocationRequest.d();
    FusedLocationProviderClient localFusedLocationProviderClient = t;
    if (localFusedLocationProviderClient == null)
    {
      localObject = "fusedLocationClient";
      k.a((String)localObject);
    }
    Object localObject = new com/truecaller/flashsdk/ui/base/BaseFlashActivity$b;
    ((BaseFlashActivity.b)localObject).<init>(this);
    localObject = (LocationCallback)localObject;
    Looper localLooper = Looper.getMainLooper();
    localFusedLocationProviderClient.a(localLocationRequest, (LocationCallback)localObject, localLooper);
  }
  
  public final void w()
  {
    Object localObject = this;
    localObject = (Activity)this;
    String[] tmp11_8 = new String[3];
    String[] tmp12_11 = tmp11_8;
    String[] tmp12_11 = tmp11_8;
    tmp12_11[0] = "android.permission.READ_EXTERNAL_STORAGE";
    tmp12_11[1] = "android.permission.WRITE_EXTERNAL_STORAGE";
    tmp12_11[2] = "android.permission.CAMERA";
    String[] arrayOfString = tmp12_11;
    android.support.v4.app.a.a((Activity)localObject, arrayOfString, 12);
  }
  
  public final void x()
  {
    finish();
  }
  
  public final Intent y()
  {
    Intent localIntent = getIntent();
    k.a(localIntent, "intent");
    return localIntent;
  }
  
  public final void z()
  {
    b localb = a;
    if (localb == null)
    {
      String str = "presenter";
      k.a(str);
    }
    localb.b();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.base.BaseFlashActivity
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */