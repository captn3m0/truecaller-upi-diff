package com.truecaller.flashsdk.ui.base;

import android.net.Uri;
import c.d.a.a;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.flashsdk.assist.q;
import kotlinx.coroutines.ag;

final class c$a$a
  extends c.d.b.a.k
  implements m
{
  int a;
  private ag c;
  
  c$a$a(c.a parama, c.d.c paramc)
  {
    super(2, paramc);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/flashsdk/ui/base/c$a$a;
    c.a locala1 = b;
    locala.<init>(locala1, paramc);
    paramObject = (ag)paramObject;
    c = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    a locala = a.a;
    int i = a;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        throw a;
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label126;
      }
      paramObject = b.c.m;
      Uri localUri = c.b(b.c);
      int j = 1;
      a = j;
      paramObject = ((q)paramObject).a(localUri, this);
      if (paramObject == locala) {
        return locala;
      }
      break;
    }
    return paramObject;
    label126:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.base.c.a.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */