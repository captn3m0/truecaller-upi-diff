package com.truecaller.flashsdk.ui.base;

import android.location.Location;
import android.os.Bundle;
import com.google.android.gms.tasks.Task;

public abstract interface b
{
  public abstract void a();
  
  public abstract void a(int paramInt1, int paramInt2);
  
  public abstract void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt);
  
  public abstract void a(Location paramLocation);
  
  public abstract void a(Bundle paramBundle);
  
  public abstract void a(Task paramTask);
  
  public abstract void a(d paramd);
  
  public abstract void a(CharSequence paramCharSequence);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString, int paramInt1, int paramInt2, int paramInt3);
  
  public abstract void a(String paramString, com.truecaller.flashsdk.a.d paramd, int paramInt1, int paramInt2);
  
  public abstract void a(String paramString, boolean paramBoolean1, boolean paramBoolean2);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract boolean a(int paramInt);
  
  public abstract void b();
  
  public abstract void b(int paramInt);
  
  public abstract void b(Location paramLocation);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c();
  
  public abstract void d();
  
  public abstract void e();
  
  public abstract void f();
  
  public abstract void g();
  
  public abstract void h();
  
  public abstract void i();
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.base.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */