package com.truecaller.flashsdk.ui.base;

import c.g.b.j;
import c.g.b.w;
import c.l.c;

final class BaseFlashActivity$a
  extends j
  implements c.g.a.b
{
  BaseFlashActivity$a(b paramb)
  {
    super(1, paramb);
  }
  
  public final c a()
  {
    return w.a(b.class);
  }
  
  public final String b()
  {
    return "onLastLocationObtained";
  }
  
  public final String c()
  {
    return "onLastLocationObtained(Landroid/location/Location;)V";
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.base.BaseFlashActivity.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */