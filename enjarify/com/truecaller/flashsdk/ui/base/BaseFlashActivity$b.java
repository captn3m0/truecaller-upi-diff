package com.truecaller.flashsdk.ui.base;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;

public final class BaseFlashActivity$b
  extends LocationCallback
{
  BaseFlashActivity$b(BaseFlashActivity paramBaseFlashActivity) {}
  
  public final void a(LocationAvailability paramLocationAvailability)
  {
    if (paramLocationAvailability != null)
    {
      boolean bool = paramLocationAvailability.a() ^ true;
      if (!bool) {
        paramLocationAvailability = null;
      }
      if (paramLocationAvailability != null)
      {
        a.c().b(null);
        return;
      }
    }
  }
  
  public final void a(LocationResult paramLocationResult)
  {
    Object localObject = a.c();
    if (paramLocationResult != null) {
      paramLocationResult = paramLocationResult.a();
    } else {
      paramLocationResult = null;
    }
    ((b)localObject).b(paramLocationResult);
    paramLocationResult = BaseFlashActivity.a(a);
    localObject = this;
    localObject = (LocationCallback)this;
    paramLocationResult.a((LocationCallback)localObject);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.base.BaseFlashActivity.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */