package com.truecaller.flashsdk.ui.base;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import com.d.b.ag;
import com.truecaller.flashsdk.assist.t;

public final class BaseFlashActivity$c
  implements ag
{
  BaseFlashActivity$c(BaseFlashActivity paramBaseFlashActivity) {}
  
  public final void a(Bitmap paramBitmap)
  {
    if (paramBitmap != null)
    {
      int i = paramBitmap.getWidth();
      int j = paramBitmap.getHeight();
      int k = 17;
      int m = -1;
      Object localObject2;
      if (i > j)
      {
        localObject1 = new android/widget/FrameLayout$LayoutParams;
        ((FrameLayout.LayoutParams)localObject1).<init>(m, -2);
        gravity = k;
        j = 100;
        ((FrameLayout.LayoutParams)localObject1).setMargins(0, 0, 0, j);
        localObject2 = a.h();
        localObject1 = (ViewGroup.LayoutParams)localObject1;
        ((ImageView)localObject2).setLayoutParams((ViewGroup.LayoutParams)localObject1);
        localObject1 = a.j;
        if (localObject1 != null) {
          t.a((ImageView)localObject1, paramBitmap);
        }
        localObject1 = a.h();
        localObject2 = ImageView.ScaleType.FIT_XY;
        ((ImageView)localObject1).setScaleType((ImageView.ScaleType)localObject2);
      }
      else
      {
        localObject1 = a.h();
        localObject2 = ImageView.ScaleType.CENTER_CROP;
        ((ImageView)localObject1).setScaleType((ImageView.ScaleType)localObject2);
        localObject1 = new android/widget/FrameLayout$LayoutParams;
        ((FrameLayout.LayoutParams)localObject1).<init>(m, m);
        gravity = k;
        ((FrameLayout.LayoutParams)localObject1).setMargins(0, 0, 0, 0);
        localObject2 = a.h();
        localObject1 = (ViewGroup.LayoutParams)localObject1;
        ((ImageView)localObject2).setLayoutParams((ViewGroup.LayoutParams)localObject1);
      }
      a.h().setImageBitmap(paramBitmap);
      Object localObject1 = a;
      o = paramBitmap;
    }
    paramBitmap = BaseFlashActivity.b(a);
    if (paramBitmap != null)
    {
      paramBitmap.setVisibility(8);
      return;
    }
  }
  
  public final void a(Drawable paramDrawable)
  {
    ImageView localImageView = a.h();
    localImageView.setImageDrawable(paramDrawable);
    paramDrawable = BaseFlashActivity.b(a);
    if (paramDrawable != null)
    {
      paramDrawable.setVisibility(8);
      return;
    }
  }
  
  public final void b(Drawable paramDrawable)
  {
    ImageView localImageView = a.h();
    localImageView.setImageDrawable(paramDrawable);
    paramDrawable = BaseFlashActivity.b(a);
    if (paramDrawable != null)
    {
      paramDrawable.setVisibility(0);
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.base.BaseFlashActivity.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */