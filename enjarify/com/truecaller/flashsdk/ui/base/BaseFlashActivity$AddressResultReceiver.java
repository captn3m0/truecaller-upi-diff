package com.truecaller.flashsdk.ui.base;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import c.g.b.k;

final class BaseFlashActivity$AddressResultReceiver
  extends ResultReceiver
{
  public BaseFlashActivity$AddressResultReceiver(BaseFlashActivity paramBaseFlashActivity, Handler paramHandler)
  {
    super(paramHandler);
  }
  
  protected final void onReceiveResult(int paramInt, Bundle paramBundle)
  {
    k.b(paramBundle, "resultData");
    a.c().a(paramBundle);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.base.BaseFlashActivity.AddressResultReceiver
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */