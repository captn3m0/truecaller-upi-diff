package com.truecaller.flashsdk.ui.base;

import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import com.google.android.gms.common.api.ResolvableApiException;
import com.truecaller.flashsdk.assist.ae;

public abstract interface d
{
  public abstract void A();
  
  public abstract void B();
  
  public abstract void C();
  
  public abstract void D();
  
  public abstract void E();
  
  public abstract void F();
  
  public abstract void G();
  
  public abstract void H();
  
  public abstract void I();
  
  public abstract void J();
  
  public abstract void K();
  
  public abstract void L();
  
  public abstract void M();
  
  public abstract void N();
  
  public abstract void a(int paramInt1, int paramInt2);
  
  public abstract void a(Location paramLocation);
  
  public abstract void a(Uri paramUri);
  
  public abstract void a(ResolvableApiException paramResolvableApiException);
  
  public abstract void a(ae paramae, long paramLong);
  
  public abstract void a(String paramString);
  
  public abstract void a(String paramString, int paramInt1, int paramInt2, int paramInt3);
  
  public abstract void a(String paramString1, String paramString2);
  
  public abstract void a(String paramString1, String paramString2, String paramString3);
  
  public abstract void a(String paramString1, String paramString2, String paramString3, String paramString4);
  
  public abstract void a(String paramString, boolean paramBoolean);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract void a(com.truecaller.flashsdk.a.d[] paramArrayOfd);
  
  public abstract void b(int paramInt);
  
  public abstract void b(String paramString);
  
  public abstract void b(String paramString1, String paramString2);
  
  public abstract void c(int paramInt);
  
  public abstract void c(String paramString);
  
  public abstract void c(String paramString1, String paramString2);
  
  public abstract void c(boolean paramBoolean);
  
  public abstract void d(int paramInt);
  
  public abstract void d(String paramString);
  
  public abstract void d(String paramString1, String paramString2);
  
  public abstract void d(boolean paramBoolean);
  
  public abstract void d_(int paramInt);
  
  public abstract void e(String paramString);
  
  public abstract void e(String paramString1, String paramString2);
  
  public abstract void k();
  
  public abstract void m();
  
  public abstract void n();
  
  public abstract void o();
  
  public abstract void p();
  
  public abstract void q();
  
  public abstract void r();
  
  public abstract void s();
  
  public abstract void t();
  
  public abstract void u();
  
  public abstract void v();
  
  public abstract void w();
  
  public abstract void x();
  
  public abstract Intent y();
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.base.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */