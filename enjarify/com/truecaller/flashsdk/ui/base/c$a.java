package com.truecaller.flashsdk.ui.base;

import android.net.Uri;
import c.d.a.a;
import c.d.f;
import c.g.a.m;
import c.o.b;
import c.x;
import com.truecaller.flashsdk.R.string;
import com.truecaller.flashsdk.assist.al;
import kotlinx.coroutines.ag;
import kotlinx.coroutines.ax;
import kotlinx.coroutines.g;

final class c$a
  extends c.d.b.a.k
  implements m
{
  Object a;
  int b;
  private ag e;
  
  c$a(c paramc, d paramd, c.d.c paramc1)
  {
    super(2, paramc1);
  }
  
  public final c.d.c a(Object paramObject, c.d.c paramc)
  {
    c.g.b.k.b(paramc, "completion");
    a locala = new com/truecaller/flashsdk/ui/base/c$a;
    c localc = c;
    d locald = d;
    locala.<init>(localc, locald, paramc);
    paramObject = (ag)paramObject;
    e = ((ag)paramObject);
    return locala;
  }
  
  public final Object a(Object paramObject)
  {
    Object localObject1 = a.a;
    int i = b;
    Object[] arrayOfObject = null;
    Object localObject3;
    switch (i)
    {
    default: 
      paramObject = new java/lang/IllegalStateException;
      ((IllegalStateException)paramObject).<init>("call to 'resume' before 'invoke' with coroutine");
      throw ((Throwable)paramObject);
    case 1: 
      boolean bool2 = paramObject instanceof o.b;
      if (bool2) {
        try
        {
          paramObject = (o.b)paramObject;
          paramObject = a;
          throw ((Throwable)paramObject);
        }
        catch (Exception paramObject) {}
      }
      break;
    case 0: 
      boolean bool1 = paramObject instanceof o.b;
      if (bool1) {
        break label295;
      }
      paramObject = e;
      Object localObject2 = c;
      localObject2 = c.a((c)localObject2);
      localObject3 = ax.b();
      localObject3 = (f)localObject3;
      localObject2 = ((f)localObject2).plus((f)localObject3);
      localObject3 = new com/truecaller/flashsdk/ui/base/c$a$a;
      ((c.a.a)localObject3).<init>(this, null);
      localObject3 = (m)localObject3;
      a = paramObject;
      int k = 1;
      b = k;
      paramObject = g.a((f)localObject2, (m)localObject3, this);
      if (paramObject == localObject1) {
        return localObject1;
      }
      break;
    }
    paramObject = (Uri)paramObject;
    if (paramObject != null)
    {
      localObject1 = c;
      ((c)localObject1).a((Uri)paramObject);
    }
    else
    {
      paramObject = d;
      localObject1 = c;
      localObject1 = i;
      int j = R.string.try_again;
      localObject3 = new Object[0];
      localObject1 = ((al)localObject1).a(j, (Object[])localObject3);
      ((d)paramObject).d((String)localObject1);
      break label291;
      com.truecaller.log.d.a((Throwable)paramObject);
      paramObject = d;
      localObject1 = c.i;
      j = R.string.try_again;
      arrayOfObject = new Object[0];
      localObject1 = ((al)localObject1).a(j, arrayOfObject);
      ((d)paramObject).d((String)localObject1);
    }
    label291:
    return x.a;
    label295:
    throw a;
  }
  
  public final Object invoke(Object paramObject1, Object paramObject2)
  {
    paramObject2 = (c.d.c)paramObject2;
    paramObject1 = (a)a(paramObject1, (c.d.c)paramObject2);
    paramObject2 = x.a;
    return ((a)paramObject1).a(paramObject2);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.ui.base.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */