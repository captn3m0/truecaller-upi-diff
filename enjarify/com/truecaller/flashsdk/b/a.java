package com.truecaller.flashsdk.b;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.text.TextUtils;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.f;
import com.truecaller.flashsdk.core.KidFlashService;
import com.truecaller.flashsdk.core.b;
import com.truecaller.flashsdk.core.c;
import com.truecaller.flashsdk.models.Flash;
import com.truecaller.flashsdk.models.Payload;
import com.truecaller.flashsdk.models.Sender;
import java.util.Locale;

public final class a
{
  private final Context a;
  private final f b;
  private final com.truecaller.flashsdk.c.a c;
  
  public a(Context paramContext, f paramf, com.truecaller.flashsdk.c.a parama)
  {
    a = paramContext;
    b = paramf;
    c = parama;
  }
  
  public final void a(RemoteMessage paramRemoteMessage)
  {
    Object localObject1 = c.a();
    Object localObject2 = b;
    paramRemoteMessage = Flash.a(paramRemoteMessage, (f)localObject2);
    if (paramRemoteMessage != null)
    {
      boolean bool1 = paramRemoteMessage.k();
      if (bool1)
      {
        localObject2 = paramRemoteMessage.a();
        if (localObject2 != null)
        {
          long l1 = aa;
          localObject2 = Long.valueOf(l1);
          if (localObject2 != null)
          {
            localObject2 = Locale.ROOT;
            localObject3 = "+%d";
            int j = 1;
            localObject4 = new Object[j];
            long l2 = aa;
            Long localLong = Long.valueOf(l2);
            localObject4[0] = localLong;
            localObject2 = String.format((Locale)localObject2, (String)localObject3, (Object[])localObject4);
            bool1 = ((b)localObject1).c((String)localObject2);
            if (bool1) {
              return;
            }
          }
        }
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>("+");
        long l3 = aa;
        Object localObject3 = Long.valueOf(l3);
        ((StringBuilder)localObject2).append(localObject3);
        localObject2 = ((StringBuilder)localObject2).toString();
        localObject3 = fa;
        Object localObject4 = "payment_success";
        boolean bool2 = TextUtils.equals((CharSequence)localObject3, (CharSequence)localObject4);
        if (bool2)
        {
          c.a(paramRemoteMessage);
          return;
        }
        localObject3 = fa;
        localObject4 = "call_me_back";
        bool2 = TextUtils.equals((CharSequence)localObject3, (CharSequence)localObject4);
        if (bool2)
        {
          int k = ((b)localObject1).f((String)localObject2);
          int i = 4;
          if (k < i)
          {
            c.a(paramRemoteMessage, null);
            return;
          }
        }
        localObject1 = fa;
        localObject2 = "payment_request";
        boolean bool3 = TextUtils.equals((CharSequence)localObject1, (CharSequence)localObject2);
        if (bool3)
        {
          c.b(paramRemoteMessage);
          return;
        }
        long l4 = SystemClock.elapsedRealtime();
        paramRemoteMessage.b(l4);
        localObject1 = new android/content/Intent;
        localObject2 = a;
        ((Intent)localObject1).<init>((Context)localObject2, KidFlashService.class);
        ((Intent)localObject1).putExtra("extra_flash", paramRemoteMessage);
        a.startService((Intent)localObject1);
        return;
      }
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */