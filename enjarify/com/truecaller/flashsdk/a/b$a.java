package com.truecaller.flashsdk.a;

import android.support.v4.view.o;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

final class b$a
  extends o
{
  private final List b;
  
  private b$a(b paramb, List paramList)
  {
    b = paramList;
  }
  
  public final void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    paramObject = (View)paramObject;
    paramViewGroup.removeView((View)paramObject);
  }
  
  public final int getCount()
  {
    return b.size();
  }
  
  public final Object instantiateItem(ViewGroup paramViewGroup, int paramInt)
  {
    View localView = b.get(paramInt)).a;
    paramViewGroup.addView(localView, 0);
    return localView;
  }
  
  public final boolean isViewFromObject(View paramView, Object paramObject)
  {
    return paramObject == paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.a.b.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */