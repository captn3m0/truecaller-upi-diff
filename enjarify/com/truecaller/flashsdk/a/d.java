package com.truecaller.flashsdk.a;

import java.io.Serializable;

public final class d
  implements Serializable
{
  final String a;
  
  public d(String paramString)
  {
    a = paramString;
  }
  
  public static d a(char paramChar)
  {
    d locald = new com/truecaller/flashsdk/a/d;
    String str = Character.toString(paramChar);
    locald.<init>(str);
    return locald;
  }
  
  public static d a(int paramInt)
  {
    d locald = new com/truecaller/flashsdk/a/d;
    int i = Character.charCount(paramInt);
    int j = 1;
    Object localObject;
    if (i == j)
    {
      localObject = String.valueOf(paramInt);
    }
    else
    {
      String str = new java/lang/String;
      localObject = Character.toChars(paramInt);
      str.<init>((char[])localObject);
      localObject = str;
    }
    locald.<init>((String)localObject);
    return locald;
  }
  
  static d a(String paramString)
  {
    d locald = new com/truecaller/flashsdk/a/d;
    locald.<init>(paramString);
    return locald;
  }
  
  public final String a()
  {
    return a;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof d;
    if (bool1)
    {
      String str = a;
      paramObject = a;
      boolean bool2 = str.contentEquals((CharSequence)paramObject);
      if (bool2) {
        return true;
      }
    }
    return false;
  }
  
  public final int hashCode()
  {
    return a.hashCode();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.a.d
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */