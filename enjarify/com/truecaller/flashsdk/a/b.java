package com.truecaller.flashsdk.a;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.f;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.widget.PopupWindow;
import com.truecaller.flashsdk.R.dimen;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;
import com.truecaller.flashsdk.assist.ae;
import java.util.Arrays;
import java.util.List;

public final class b
  extends PopupWindow
  implements ViewPager.f, a, h
{
  private final View a;
  private final Context b;
  private ae c;
  private f.a d;
  private b.b e;
  private int f;
  private View[] g;
  private long h = 0L;
  private boolean i = false;
  private boolean j = false;
  private int k;
  private boolean l;
  
  public b(Context paramContext, View paramView, f.a parama, ae paramae, long paramLong)
  {
    super(paramContext);
    int m = -1;
    k = m;
    l = true;
    a = paramView;
    b = paramContext;
    c = paramae;
    d = parama;
    h = paramLong;
    paramView = f();
    setContentView(paramView);
    setSoftInputMode(5);
    paramContext = paramContext.getResources();
    int n = R.dimen.keyboard_height;
    int i1 = (int)paramContext.getDimension(n);
    a(i1);
    paramContext = new android/graphics/drawable/ColorDrawable;
    paramContext.<init>(m);
    setBackgroundDrawable(paramContext);
  }
  
  private void a(int paramInt)
  {
    setWidth(-1);
    setHeight(paramInt);
  }
  
  private View f()
  {
    b localb = this;
    Object localObject1 = (LayoutInflater)b.getSystemService("layout_inflater");
    int m = R.layout.layout_emoji_keyboard;
    View localView = ((LayoutInflater)localObject1).inflate(m, null, false);
    int n = R.id.pager;
    localObject1 = localView.findViewById(n);
    Object localObject2 = localObject1;
    localObject2 = (ViewPager)localObject1;
    ((ViewPager)localObject2).a(this);
    b.a locala = new com/truecaller/flashsdk/a/b$a;
    int i1 = 6;
    f[] arrayOff = new f[i1];
    g localg = new com/truecaller/flashsdk/a/g;
    Object localObject3 = b;
    Object localObject4 = d;
    Object localObject5 = c;
    long l1 = h;
    localObject1 = localg;
    localg.<init>((Context)localObject3, (f.a)localObject4, (ae)localObject5, l1);
    arrayOff[0] = localg;
    f localf = new com/truecaller/flashsdk/a/f;
    localObject3 = b;
    localObject4 = i.c;
    f.a locala1 = d;
    ae localae = c;
    long l2 = h;
    localObject1 = localf;
    localObject5 = this;
    localf.<init>((Context)localObject3, (d[])localObject4, this, locala1, localae, l2);
    int i2 = 1;
    arrayOff[i2] = localf;
    localf = new com/truecaller/flashsdk/a/f;
    localObject3 = b;
    localObject4 = i.a;
    locala1 = d;
    localae = c;
    l2 = h;
    localObject1 = localf;
    localf.<init>((Context)localObject3, (d[])localObject4, this, locala1, localae, l2);
    int i3 = 2;
    arrayOff[i3] = localf;
    localf = new com/truecaller/flashsdk/a/f;
    localObject3 = b;
    localObject4 = i.b;
    locala1 = d;
    localae = c;
    l2 = h;
    localObject1 = localf;
    localf.<init>((Context)localObject3, (d[])localObject4, this, locala1, localae, l2);
    int i4 = 3;
    arrayOff[i4] = localf;
    localf = new com/truecaller/flashsdk/a/f;
    localObject3 = b;
    localObject4 = i.d;
    locala1 = d;
    localae = c;
    l2 = h;
    localObject1 = localf;
    localf.<init>((Context)localObject3, (d[])localObject4, this, locala1, localae, l2);
    int i5 = 4;
    arrayOff[i5] = localf;
    localf = new com/truecaller/flashsdk/a/f;
    localObject3 = b;
    localObject4 = i.e;
    locala1 = d;
    localae = c;
    l2 = h;
    localObject1 = localf;
    localf.<init>((Context)localObject3, (d[])localObject4, this, locala1, localae, l2);
    n = 5;
    arrayOff[n] = localf;
    localObject3 = Arrays.asList(arrayOff);
    locala.<init>(this, (List)localObject3, (byte)0);
    ((ViewPager)localObject2).setAdapter(locala);
    localObject3 = new View[i1];
    g = ((View[])localObject3);
    localObject3 = g;
    int i6 = R.id.tab_recents;
    localObject4 = localView.findViewById(i6);
    localObject3[0] = localObject4;
    localObject3 = g;
    i6 = R.id.tab_people;
    localObject4 = localView.findViewById(i6);
    localObject3[i2] = localObject4;
    localObject3 = g;
    i6 = R.id.tab_nature;
    localObject4 = localView.findViewById(i6);
    localObject3[i3] = localObject4;
    localObject3 = g;
    i6 = R.id.tab_objects;
    localObject4 = localView.findViewById(i6);
    localObject3[i4] = localObject4;
    localObject3 = g;
    i6 = R.id.tab_cars;
    localObject4 = localView.findViewById(i6);
    localObject3[i5] = localObject4;
    localObject3 = g;
    i6 = R.id.tab_punctuation;
    localObject4 = localView.findViewById(i6);
    localObject3[n] = localObject4;
    n = 0;
    localObject1 = null;
    for (;;)
    {
      localObject3 = g;
      i6 = localObject3.length;
      if (n >= i6) {
        break;
      }
      localObject3 = localObject3[n];
      localObject4 = new com/truecaller/flashsdk/a/-$$Lambda$b$oF5UdrZhrCTvobWrRWlZr6JoYS0;
      ((-..Lambda.b.oF5UdrZhrCTvobWrRWlZr6JoYS0)localObject4).<init>((ViewPager)localObject2, n);
      ((View)localObject3).setOnClickListener((View.OnClickListener)localObject4);
      n += 1;
    }
    n = R.id.backspace;
    localObject1 = localView.findViewById(n);
    localObject3 = new com/truecaller/flashsdk/a/-$$Lambda$b$M-6DWRTd8k4I2hul8v1vCgXbsgE;
    ((-..Lambda.b.M-6DWRTd8k4I2hul8v1vCgXbsgE)localObject3).<init>(localb);
    ((View)localObject1).setOnClickListener((View.OnClickListener)localObject3);
    localObject1 = c;
    n = ((ae)localObject1).b();
    if (n == 0) {
      localb.onPageSelected(0);
    } else {
      ((ViewPager)localObject2).a(n, false);
    }
    return localView;
  }
  
  public final void a()
  {
    boolean bool = l;
    int m = 80;
    if (bool)
    {
      localView = a;
      int n = -f;
      showAtLocation(localView, m, 0, n);
      return;
    }
    View localView = a;
    showAtLocation(localView, m, 0, 0);
  }
  
  public final void a(Context paramContext, d paramd, long paramLong)
  {
    c.a(paramd);
    c.b(paramLong);
  }
  
  public final void a(b.b paramb)
  {
    e = paramb;
  }
  
  public final void b()
  {
    boolean bool = i;
    if (bool)
    {
      a();
      return;
    }
    j = true;
  }
  
  public final Boolean c()
  {
    return Boolean.valueOf(i);
  }
  
  public final void d()
  {
    e = null;
  }
  
  public final void e()
  {
    boolean bool = l;
    if (bool)
    {
      localObject = a.getViewTreeObserver();
      -..Lambda.b.LliYSS_4nJvx5Jgt3nGsQK5Er2Q localLliYSS_4nJvx5Jgt3nGsQK5Er2Q = new com/truecaller/flashsdk/a/-$$Lambda$b$LliYSS_4nJvx5Jgt3nGsQK5Er2Q;
      localLliYSS_4nJvx5Jgt3nGsQK5Er2Q.<init>(this);
      ((ViewTreeObserver)localObject).addOnGlobalLayoutListener(localLliYSS_4nJvx5Jgt3nGsQK5Er2Q);
      return;
    }
    Object localObject = b.getResources();
    int n = R.dimen.keyboard_height;
    int m = (int)((Resources)localObject).getDimension(n);
    f = m;
    m = f;
    a(m);
  }
  
  public final void onPageScrollStateChanged(int paramInt) {}
  
  public final void onPageScrolled(int paramInt1, float paramFloat, int paramInt2) {}
  
  public final void onPageSelected(int paramInt)
  {
    int m = k;
    if (m == paramInt) {
      return;
    }
    if (m >= 0)
    {
      View[] arrayOfView = g;
      int n = arrayOfView.length;
      if (m < n)
      {
        View localView = arrayOfView[m];
        arrayOfView = null;
        localView.setSelected(false);
      }
    }
    g[paramInt].setSelected(true);
    k = paramInt;
    c.a(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.a.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */