package com.truecaller.flashsdk.a;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.f;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.f;
import android.support.v4.view.o;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import c.g.b.k;
import c.u;
import com.truecaller.flashsdk.R.array;
import com.truecaller.flashsdk.R.dimen;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.R.layout;
import com.truecaller.flashsdk.assist.ae;
import java.util.Arrays;
import java.util.List;

public final class c
  extends PopupWindow
  implements ViewPager.f, a, h
{
  public c.b a;
  public int b;
  public boolean c;
  public final Context d;
  public final View e;
  private boolean f;
  private boolean g;
  private int h;
  private final f.a i;
  private final ae j;
  private final long k;
  
  public c(Context paramContext, View paramView, f.a parama, ae paramae, long paramLong)
  {
    super(paramContext);
    d = paramContext;
    e = paramView;
    i = parama;
    j = paramae;
    k = paramLong;
    int m = -1;
    h = m;
    c = true;
    paramView = e();
    setContentView(paramView);
    setSoftInputMode(5);
    paramView = d.getResources();
    int n = R.dimen.keyboard_height;
    int i1 = (int)paramView.getDimension(n);
    a(i1);
    paramView = new android/graphics/drawable/ColorDrawable;
    paramView.<init>(m);
    paramView = (Drawable)paramView;
    setBackgroundDrawable(paramView);
  }
  
  private final View e()
  {
    c localc = this;
    Object localObject1 = d;
    Object localObject2 = "layout_inflater";
    localObject1 = ((Context)localObject1).getSystemService((String)localObject2);
    if (localObject1 != null)
    {
      localObject1 = (LayoutInflater)localObject1;
      int m = R.layout.layout_emoji_keyboardv2;
      localObject1 = ((LayoutInflater)localObject1).inflate(m, null, false);
      m = R.id.pager;
      localObject2 = (ViewPager)((View)localObject1).findViewById(m);
      int n = R.id.emojiTab;
      Object localObject3 = (TabLayout)((View)localObject1).findViewById(n);
      Object localObject4 = this;
      localObject4 = (ViewPager.f)this;
      ((ViewPager)localObject2).a((ViewPager.f)localObject4);
      localObject4 = new com/truecaller/flashsdk/a/c$a;
      Object localObject5 = new f[6];
      Object localObject6 = new com/truecaller/flashsdk/a/g;
      Object localObject7 = d;
      Object localObject8 = i;
      Object localObject9 = j;
      long l1 = k;
      Object localObject10 = localObject6;
      ((g)localObject6).<init>((Context)localObject7, (f.a)localObject8, (ae)localObject9, l1);
      localObject6 = (f)localObject6;
      localObject5[0] = localObject6;
      localObject10 = new com/truecaller/flashsdk/a/f;
      Context localContext = d;
      d[] arrayOfd = i.c;
      localObject7 = this;
      localObject7 = (h)this;
      localObject8 = i;
      localObject9 = j;
      l1 = k;
      Object localObject11 = localObject9;
      ((f)localObject10).<init>(localContext, arrayOfd, (h)localObject7, (f.a)localObject8, (ae)localObject9, l1);
      int i1 = 1;
      localObject5[i1] = localObject10;
      localObject8 = new com/truecaller/flashsdk/a/f;
      localObject9 = d;
      localObject11 = i.a;
      f.a locala = i;
      ae localae = j;
      long l2 = k;
      ((f)localObject8).<init>((Context)localObject9, (d[])localObject11, (h)localObject7, locala, localae, l2);
      localObject5[2] = localObject8;
      localObject8 = new com/truecaller/flashsdk/a/f;
      localObject9 = d;
      localObject11 = i.b;
      locala = i;
      localae = j;
      l2 = k;
      ((f)localObject8).<init>((Context)localObject9, (d[])localObject11, (h)localObject7, locala, localae, l2);
      localObject5[3] = localObject8;
      localObject8 = new com/truecaller/flashsdk/a/f;
      localObject9 = d;
      localObject11 = i.d;
      locala = i;
      localae = j;
      l2 = k;
      ((f)localObject8).<init>((Context)localObject9, (d[])localObject11, (h)localObject7, locala, localae, l2);
      localObject5[4] = localObject8;
      localObject8 = new com/truecaller/flashsdk/a/f;
      localObject9 = d;
      localObject11 = i.e;
      locala = i;
      localae = j;
      l2 = k;
      ((f)localObject8).<init>((Context)localObject9, (d[])localObject11, (h)localObject7, locala, localae, l2);
      localObject5[5] = localObject8;
      localObject5 = Arrays.asList((Object[])localObject5);
      k.a(localObject5, "Arrays.asList(\n         …ger, phone)\n            )");
      ((c.a)localObject4).<init>(this, (List)localObject5);
      localObject5 = "pager";
      k.a(localObject2, (String)localObject5);
      localObject4 = (o)localObject4;
      ((ViewPager)localObject2).setAdapter((o)localObject4);
      ((TabLayout)localObject3).setupWithViewPager((ViewPager)localObject2);
      localObject4 = d.getResources();
      int i2 = R.array.emojiicons;
      localObject4 = ((Resources)localObject4).obtainTypedArray(i2);
      i2 = ((TypedArray)localObject4).length();
      int i3 = 0;
      localObject10 = null;
      while (i3 < i2)
      {
        localObject7 = ((TabLayout)localObject3).a(i3);
        if (localObject7 != null)
        {
          i1 = ((TypedArray)localObject4).getResourceId(i3, -1);
          ((TabLayout.f)localObject7).a(i1);
        }
        i3 += 1;
      }
      ((TypedArray)localObject4).recycle();
      localObject3 = j;
      n = ((ae)localObject3).b();
      if (n == 0) {
        localc.onPageSelected(0);
      } else {
        ((ViewPager)localObject2).a(n, false);
      }
      k.a(localObject1, "view");
      return (View)localObject1;
    }
    localObject1 = new c/u;
    ((u)localObject1).<init>("null cannot be cast to non-null type android.view.LayoutInflater");
    throw ((Throwable)localObject1);
  }
  
  public final void a()
  {
    boolean bool = c;
    int m = 80;
    if (bool)
    {
      localView = e;
      int n = -b;
      showAtLocation(localView, m, 0, n);
      return;
    }
    View localView = e;
    showAtLocation(localView, m, 0, 0);
  }
  
  public final void a(int paramInt)
  {
    setWidth(-1);
    setHeight(paramInt);
  }
  
  public final void a(Context paramContext, d paramd, long paramLong)
  {
    k.b(paramContext, "context");
    k.b(paramd, "emoticon");
    j.a(paramd);
    j.b(paramLong);
  }
  
  public final void b()
  {
    boolean bool = f;
    Boolean localBoolean = Boolean.valueOf(bool);
    if (localBoolean != null)
    {
      bool = localBoolean.booleanValue();
      if (bool)
      {
        a();
        return;
      }
      g = true;
      return;
    }
  }
  
  public final Boolean c()
  {
    return Boolean.valueOf(f);
  }
  
  public final void d()
  {
    a = null;
  }
  
  public final void onPageScrollStateChanged(int paramInt) {}
  
  public final void onPageScrolled(int paramInt1, float paramFloat, int paramInt2) {}
  
  public final void onPageSelected(int paramInt)
  {
    int m = h;
    if (m == paramInt) {
      return;
    }
    h = paramInt;
    j.a(paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.a.c
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */