package com.truecaller.flashsdk.a;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.truecaller.flashsdk.R.layout;
import java.util.List;

final class e
  extends ArrayAdapter
{
  f.a a;
  private LayoutInflater b;
  
  e(Context paramContext, List paramList)
  {
    super(paramContext, i, paramList);
    paramContext = LayoutInflater.from(paramContext);
    b = paramContext;
  }
  
  e(Context paramContext, d[] paramArrayOfd)
  {
    super(paramContext, i, paramArrayOfd);
    paramContext = LayoutInflater.from(paramContext);
    b = paramContext;
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null)
    {
      paramView = b;
      int i = R.layout.adapter_emoji_item;
      paramView = paramView.inflate(i, paramViewGroup, false);
      paramViewGroup = new com/truecaller/flashsdk/a/e$a;
      paramViewGroup.<init>(this, paramView, (byte)0);
      paramView.setTag(paramViewGroup);
    }
    else
    {
      paramViewGroup = (e.a)paramView.getTag();
    }
    d locald = (d)getItem(paramInt);
    paramViewGroup.a(locald);
    return paramView;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.a.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */