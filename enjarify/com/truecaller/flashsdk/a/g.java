package com.truecaller.flashsdk.a;

import android.content.Context;
import android.view.View;
import android.widget.GridView;
import com.truecaller.flashsdk.R.id;
import com.truecaller.flashsdk.assist.ae;
import java.util.List;

final class g
  extends f
  implements h
{
  private final e c;
  
  g(Context paramContext, f.a parama, ae paramae, long paramLong)
  {
    super(paramContext, null, null, parama, paramae, paramLong);
    paramae = new com/truecaller/flashsdk/a/e;
    localObject = b.a();
    paramae.<init>(paramContext, (List)localObject);
    c = paramae;
    paramContext = c;
    paramae = new com/truecaller/flashsdk/a/-$$Lambda$g$R0dPw9u5OLDkNDaHdGam663QDf4;
    localObject = paramae;
    paramae.<init>(this, parama, null, paramLong);
    a = paramae;
    paramContext = a;
    int i = R.id.grid_view;
    paramContext = (GridView)paramContext.findViewById(i);
    parama = c;
    paramContext.setAdapter(parama);
  }
  
  public final void a(Context paramContext, d paramd, long paramLong)
  {
    b.a(paramd);
    paramContext = c;
    if (paramContext != null) {
      paramContext.notifyDataSetChanged();
    }
    b.b(paramLong);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.a.g
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */