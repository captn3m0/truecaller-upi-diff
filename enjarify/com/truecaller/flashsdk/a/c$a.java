package com.truecaller.flashsdk.a;

import android.support.v4.view.o;
import android.view.View;
import android.view.ViewGroup;
import c.g.b.k;
import java.util.List;

final class c$a
  extends o
{
  private final List b;
  
  public c$a(c paramc, List paramList)
  {
    b = paramList;
  }
  
  public final void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    k.b(paramViewGroup, "container");
    k.b(paramObject, "view");
    paramObject = (View)paramObject;
    paramViewGroup.removeView((View)paramObject);
  }
  
  public final int getCount()
  {
    return b.size();
  }
  
  public final Object instantiateItem(ViewGroup paramViewGroup, int paramInt)
  {
    k.b(paramViewGroup, "container");
    View localView = b.get(paramInt)).a;
    paramViewGroup.addView(localView, 0);
    k.a(localView, "v");
    return localView;
  }
  
  public final boolean isViewFromObject(View paramView, Object paramObject)
  {
    k.b(paramView, "view");
    k.b(paramObject, "key");
    return k.a(paramObject, paramView);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.a.c.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */