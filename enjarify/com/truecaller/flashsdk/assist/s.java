package com.truecaller.flashsdk.assist;

import dagger.a.d;
import javax.inject.Provider;

public final class s
  implements d
{
  private final Provider a;
  private final Provider b;
  private final Provider c;
  
  private s(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    a = paramProvider1;
    b = paramProvider2;
    c = paramProvider3;
  }
  
  public static s a(Provider paramProvider1, Provider paramProvider2, Provider paramProvider3)
  {
    s locals = new com/truecaller/flashsdk/assist/s;
    locals.<init>(paramProvider1, paramProvider2, paramProvider3);
    return locals;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.s
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */