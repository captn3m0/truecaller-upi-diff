package com.truecaller.flashsdk.assist;

import android.content.Context;
import com.truecaller.utils.extensions.i;

public final class b
  implements a
{
  private final Context a;
  
  public b(Context paramContext)
  {
    a = paramContext;
  }
  
  public final int a(int paramInt)
  {
    return android.support.v4.content.b.c(a, paramInt);
  }
  
  public final int b(int paramInt)
  {
    return i.a(a, paramInt);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.b
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */