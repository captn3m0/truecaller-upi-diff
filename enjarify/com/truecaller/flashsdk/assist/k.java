package com.truecaller.flashsdk.assist;

import android.content.Context;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import c.n.m;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;

public final class k
  implements j
{
  private final Context a;
  
  public k(Context paramContext)
  {
    a = paramContext;
  }
  
  private static void a(Closeable paramCloseable)
  {
    if (paramCloseable != null) {
      try
      {
        paramCloseable.close();
        return;
      }
      catch (IOException localIOException) {}
    }
  }
  
  /* Error */
  public final Uri a(android.graphics.Bitmap paramBitmap)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 31
    //   3: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: aconst_null
    //   7: astore_2
    //   8: new 33	java/io/File
    //   11: astore_3
    //   12: invokestatic 39	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   15: astore 4
    //   17: ldc 41
    //   19: astore 5
    //   21: aload_3
    //   22: aload 4
    //   24: aload 5
    //   26: invokespecial 44	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   29: aload_3
    //   30: invokevirtual 48	java/io/File:exists	()Z
    //   33: istore 6
    //   35: iload 6
    //   37: ifne +8 -> 45
    //   40: aload_3
    //   41: invokevirtual 51	java/io/File:mkdir	()Z
    //   44: pop
    //   45: new 33	java/io/File
    //   48: astore 4
    //   50: aload_3
    //   51: invokevirtual 55	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   54: astore_3
    //   55: aload_3
    //   56: invokevirtual 60	java/lang/String:toString	()Ljava/lang/String;
    //   59: astore_3
    //   60: ldc 62
    //   62: astore 5
    //   64: aload 4
    //   66: aload_3
    //   67: aload 5
    //   69: invokespecial 65	java/io/File:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   72: new 67	java/io/FileOutputStream
    //   75: astore_3
    //   76: aload_3
    //   77: aload 4
    //   79: invokespecial 70	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   82: getstatic 76	android/graphics/Bitmap$CompressFormat:JPEG	Landroid/graphics/Bitmap$CompressFormat;
    //   85: astore 5
    //   87: bipush 70
    //   89: istore 7
    //   91: aload_3
    //   92: astore 8
    //   94: aload_3
    //   95: checkcast 79	java/io/OutputStream
    //   98: astore 8
    //   100: aload_1
    //   101: aload 5
    //   103: iload 7
    //   105: aload 8
    //   107: invokevirtual 85	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   110: pop
    //   111: aload_0
    //   112: getfield 22	com/truecaller/flashsdk/assist/k:a	Landroid/content/Context;
    //   115: astore_1
    //   116: new 87	java/lang/StringBuilder
    //   119: astore 5
    //   121: aload 5
    //   123: invokespecial 88	java/lang/StringBuilder:<init>	()V
    //   126: aload_0
    //   127: getfield 22	com/truecaller/flashsdk/assist/k:a	Landroid/content/Context;
    //   130: astore 9
    //   132: aload 9
    //   134: invokevirtual 94	android/content/Context:getApplicationContext	()Landroid/content/Context;
    //   137: astore 9
    //   139: ldc 96
    //   141: astore 8
    //   143: aload 9
    //   145: aload 8
    //   147: invokestatic 98	c/g/b/k:a	(Ljava/lang/Object;Ljava/lang/String;)V
    //   150: aload 9
    //   152: invokevirtual 101	android/content/Context:getPackageName	()Ljava/lang/String;
    //   155: astore 9
    //   157: aload 5
    //   159: aload 9
    //   161: invokevirtual 105	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   164: pop
    //   165: ldc 107
    //   167: astore 9
    //   169: aload 5
    //   171: aload 9
    //   173: invokevirtual 105	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   176: pop
    //   177: aload 5
    //   179: invokevirtual 108	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   182: astore 5
    //   184: aload_1
    //   185: aload 5
    //   187: aload 4
    //   189: invokestatic 113	android/support/v4/content/FileProvider:a	(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;
    //   192: astore_2
    //   193: goto +22 -> 215
    //   196: astore_1
    //   197: goto +6 -> 203
    //   200: astore_1
    //   201: aconst_null
    //   202: astore_3
    //   203: aload_3
    //   204: checkcast 24	java/io/Closeable
    //   207: invokestatic 116	com/truecaller/flashsdk/assist/k:a	(Ljava/io/Closeable;)V
    //   210: aload_1
    //   211: athrow
    //   212: pop
    //   213: aconst_null
    //   214: astore_3
    //   215: aload_3
    //   216: checkcast 24	java/io/Closeable
    //   219: invokestatic 116	com/truecaller/flashsdk/assist/k:a	(Ljava/io/Closeable;)V
    //   222: aload_2
    //   223: areturn
    //   224: pop
    //   225: goto -10 -> 215
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	228	0	this	k
    //   0	228	1	paramBitmap	android.graphics.Bitmap
    //   7	216	2	localUri	Uri
    //   11	205	3	localObject1	Object
    //   15	173	4	localFile	File
    //   19	167	5	localObject2	Object
    //   33	3	6	bool	boolean
    //   89	15	7	i	int
    //   92	54	8	localObject3	Object
    //   130	42	9	localObject4	Object
    //   212	1	10	localIOException1	IOException
    //   224	1	11	localIOException2	IOException
    // Exception table:
    //   from	to	target	type
    //   82	85	196	finally
    //   94	98	196	finally
    //   105	111	196	finally
    //   111	115	196	finally
    //   116	119	196	finally
    //   121	126	196	finally
    //   126	130	196	finally
    //   132	137	196	finally
    //   145	150	196	finally
    //   150	155	196	finally
    //   159	165	196	finally
    //   171	177	196	finally
    //   177	182	196	finally
    //   187	192	196	finally
    //   8	11	200	finally
    //   12	15	200	finally
    //   24	29	200	finally
    //   29	33	200	finally
    //   40	45	200	finally
    //   45	48	200	finally
    //   50	54	200	finally
    //   55	59	200	finally
    //   67	72	200	finally
    //   72	75	200	finally
    //   77	82	200	finally
    //   8	11	212	java/io/IOException
    //   12	15	212	java/io/IOException
    //   24	29	212	java/io/IOException
    //   29	33	212	java/io/IOException
    //   40	45	212	java/io/IOException
    //   45	48	212	java/io/IOException
    //   50	54	212	java/io/IOException
    //   55	59	212	java/io/IOException
    //   67	72	212	java/io/IOException
    //   72	75	212	java/io/IOException
    //   77	82	212	java/io/IOException
    //   82	85	224	java/io/IOException
    //   94	98	224	java/io/IOException
    //   105	111	224	java/io/IOException
    //   111	115	224	java/io/IOException
    //   116	119	224	java/io/IOException
    //   121	126	224	java/io/IOException
    //   126	130	224	java/io/IOException
    //   132	137	224	java/io/IOException
    //   145	150	224	java/io/IOException
    //   150	155	224	java/io/IOException
    //   159	165	224	java/io/IOException
    //   171	177	224	java/io/IOException
    //   177	182	224	java/io/IOException
    //   187	192	224	java/io/IOException
  }
  
  public final Uri a(File paramFile)
  {
    c.g.b.k.b(paramFile, "file");
    Context localContext = a;
    Object localObject = localContext.getApplicationContext();
    c.g.b.k.a(localObject, "context.applicationContext");
    localObject = ((Context)localObject).getPackageName();
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append((String)localObject);
    localStringBuilder.append(".fileprovider");
    localObject = localStringBuilder.toString();
    paramFile = FileProvider.a(localContext, (String)localObject, paramFile);
    c.g.b.k.a(paramFile, "FileProvider.getUriForFi…roviderAuthority(), file)");
    return paramFile;
  }
  
  public final File a(String paramString)
  {
    c.g.b.k.b(paramString, "extension");
    File localFile = a.getExternalFilesDir(null);
    String str = String.valueOf(System.currentTimeMillis());
    c.g.b.k.b(str, "time");
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("IMG");
    localStringBuilder.append("_");
    localStringBuilder.append(str);
    str = localStringBuilder.toString();
    c.g.b.k.a(str, "StringBuilder(DEFAULT_IM…\n            }.toString()");
    paramString = File.createTempFile(str, paramString, localFile);
    c.g.b.k.a(paramString, "File.createTempFile(uniq…FileName, extension, dir)");
    return paramString;
  }
  
  public final String a(String paramString1, String paramString2)
  {
    c.g.b.k.b(paramString1, "prefix");
    c.g.b.k.b(paramString2, "extension");
    Object localObject = m.b(String.valueOf(System.currentTimeMillis()), 5);
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append(paramString1);
    localStringBuilder.append("_");
    localStringBuilder.append((String)localObject);
    paramString1 = localStringBuilder.toString();
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    ((StringBuilder)localObject).append(paramString1);
    ((StringBuilder)localObject).append(paramString2);
    return ((StringBuilder)localObject).toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.k
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */