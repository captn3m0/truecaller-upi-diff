package com.truecaller.flashsdk.assist;

import c.g.b.k;

public final class an
{
  public final String a;
  private final String b;
  private final int c;
  
  public an(String paramString1, String paramString2)
  {
    b = paramString1;
    c = 0;
    a = paramString2;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this != paramObject)
    {
      boolean bool2 = paramObject instanceof an;
      if (bool2)
      {
        paramObject = (an)paramObject;
        String str1 = b;
        String str2 = b;
        bool2 = k.a(str1, str2);
        if (bool2)
        {
          int i = c;
          int j = c;
          if (i == j)
          {
            i = 1;
          }
          else
          {
            i = 0;
            str1 = null;
          }
          if (i != 0)
          {
            str1 = a;
            paramObject = a;
            boolean bool3 = k.a(str1, paramObject);
            if (bool3) {
              return bool1;
            }
          }
        }
      }
      return false;
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    String str1 = b;
    int i = 0;
    if (str1 != null)
    {
      j = str1.hashCode();
    }
    else
    {
      j = 0;
      str1 = null;
    }
    j *= 31;
    int k = c;
    int j = (j + k) * 31;
    String str2 = a;
    if (str2 != null) {
      i = str2.hashCode();
    }
    return j + i;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>("RespondViewData(title=");
    String str = b;
    localStringBuilder.append(str);
    localStringBuilder.append(", iconResId=");
    int i = c;
    localStringBuilder.append(i);
    localStringBuilder.append(", tag=");
    str = a;
    localStringBuilder.append(str);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.an
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */