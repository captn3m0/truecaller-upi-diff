package com.truecaller.flashsdk.assist;

import android.text.Editable;
import android.text.TextWatcher;
import c.g.a.b;

public final class t$a
  implements TextWatcher
{
  t$a(b paramb) {}
  
  public final void afterTextChanged(Editable paramEditable) {}
  
  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {}
  
  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    a.invoke(paramCharSequence);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.t.a
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */