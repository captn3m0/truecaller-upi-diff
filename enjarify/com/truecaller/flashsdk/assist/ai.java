package com.truecaller.flashsdk.assist;

import android.content.Context;
import android.content.res.Resources;
import c.g.b.k;
import c.x;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class ai
  extends af
  implements ae
{
  private final Context c;
  
  public ai(Context paramContext, aa paramaa)
  {
    super(paramaa, i);
    c = paramContext;
  }
  
  private void d(String paramString)
  {
    k.b(paramString, "object");
    super.c(paramString);
  }
  
  public final List a()
  {
    return a;
  }
  
  public final void a(int paramInt) {}
  
  public final void a(long paramLong)
  {
    Object localObject1 = String.valueOf(paramLong);
    Object localObject2 = "recent_incoming".concat((String)localObject1);
    super.b((String)localObject2);
    int i = super.c();
    if (i == 0)
    {
      localObject2 = c.getResources();
      localObject1 = aj.a();
      Object localObject3 = new java/util/ArrayList;
      int j = localObject1.length;
      ((ArrayList)localObject3).<init>(j);
      localObject3 = (Collection)localObject3;
      j = localObject1.length;
      int k = 0;
      while (k < j)
      {
        int m = localObject1[k];
        Object localObject4 = ((Resources)localObject2).getString(m);
        String str = "resources.getString(it)";
        k.a(localObject4, str);
        d((String)localObject4);
        localObject4 = x.a;
        ((Collection)localObject3).add(localObject4);
        k += 1;
      }
      b(paramLong);
    }
  }
  
  public final int b()
  {
    return 0;
  }
  
  public final void b(long paramLong)
  {
    String str = String.valueOf(paramLong);
    str = "recent_incoming".concat(str);
    super.c(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.ai
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */