package com.truecaller.flashsdk.assist;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import c.g.b.k;

public final class ab
  implements aa
{
  private final SharedPreferences a;
  
  public ab(Context paramContext)
  {
    paramContext = PreferenceManager.getDefaultSharedPreferences(paramContext);
    k.a(paramContext, "PreferenceManager.getDef…haredPreferences(context)");
    a = paramContext;
  }
  
  private final void a(String paramString, long paramLong)
  {
    a.edit().putLong(paramString, paramLong).apply();
  }
  
  public final int a(String paramString, int paramInt)
  {
    k.b(paramString, "key");
    return a.getInt(paramString, paramInt);
  }
  
  public final String a(String paramString1, String paramString2)
  {
    k.b(paramString1, "key");
    k.b(paramString2, "def");
    return a.getString(paramString1, paramString2);
  }
  
  public final void a(String paramString)
  {
    k.b(paramString, "key");
    a.edit().remove(paramString).apply();
  }
  
  public final void a(String paramString, Object paramObject)
  {
    k.b(paramString, "key");
    Object localObject = "value";
    k.b(paramObject, (String)localObject);
    boolean bool1 = paramObject instanceof String;
    if (bool1)
    {
      paramObject = (String)paramObject;
      a.edit().putString(paramString, (String)paramObject).apply();
      return;
    }
    bool1 = paramObject instanceof Integer;
    if (bool1)
    {
      int i = ((Number)paramObject).intValue();
      a.edit().putInt(paramString, i).apply();
      return;
    }
    bool1 = paramObject instanceof Long;
    if (bool1)
    {
      long l = ((Number)paramObject).longValue();
      a(paramString, l);
      return;
    }
    bool1 = paramObject instanceof Boolean;
    if (bool1)
    {
      boolean bool2 = ((Boolean)paramObject).booleanValue();
      a.edit().putBoolean(paramString, bool2).apply();
      return;
    }
    bool1 = paramObject instanceof Float;
    if (bool1)
    {
      paramObject = (Number)paramObject;
      float f = ((Number)paramObject).floatValue();
      localObject = a.edit();
      paramString = ((SharedPreferences.Editor)localObject).putFloat(paramString, f);
      paramString.apply();
    }
  }
  
  public final boolean a()
  {
    Object localObject = a;
    String str1 = ac.a();
    boolean bool1 = ((SharedPreferences)localObject).contains(str1);
    str1 = null;
    if (bool1)
    {
      localObject = a;
      String str2 = ac.a();
      localObject = (CharSequence)((SharedPreferences)localObject).getString(str2, null);
      boolean bool2 = true;
      if (localObject != null)
      {
        i = ((CharSequence)localObject).length();
        if (i != 0)
        {
          i = 0;
          localObject = null;
          break label76;
        }
      }
      int i = 1;
      label76:
      if (i == 0) {
        return bool2;
      }
    }
    return false;
  }
  
  public final long b(String paramString)
  {
    k.b(paramString, "key");
    return a.getLong(paramString, -1);
  }
  
  public final String b()
  {
    Object localObject1 = a;
    Object localObject2 = ac.a();
    String str = "";
    localObject1 = ((SharedPreferences)localObject1).getString((String)localObject2, str);
    localObject2 = localObject1;
    localObject2 = (CharSequence)localObject1;
    if (localObject2 != null)
    {
      i = ((CharSequence)localObject2).length();
      if (i != 0)
      {
        i = 0;
        localObject2 = null;
        break label59;
      }
    }
    int i = 1;
    label59:
    if (i != 0) {
      localObject1 = null;
    }
    return (String)localObject1;
  }
  
  public final void c()
  {
    long l = d() + 1L;
    String str = ac.b();
    a(str, l);
  }
  
  public final boolean c(String paramString)
  {
    k.b(paramString, "key");
    return a.getBoolean(paramString, true);
  }
  
  public final long d()
  {
    SharedPreferences localSharedPreferences = a;
    String str = ac.b();
    return localSharedPreferences.getLong(str, 0L);
  }
  
  public final void d(String paramString)
  {
    SharedPreferences.Editor localEditor = a.edit();
    String str = ac.a();
    localEditor.putString(str, paramString).apply();
  }
  
  public final void e()
  {
    long l = f() + 1L;
    String str = ac.c();
    a(str, l);
  }
  
  public final long f()
  {
    SharedPreferences localSharedPreferences = a;
    String str = ac.c();
    return localSharedPreferences.getLong(str, 0L);
  }
  
  public final void g()
  {
    long l = h() + 1L;
    String str = ac.d();
    a(str, l);
  }
  
  public final long h()
  {
    SharedPreferences localSharedPreferences = a;
    String str = ac.d();
    return localSharedPreferences.getLong(str, 0L);
  }
  
  public final void i()
  {
    String str = ac.b();
    a(str);
    str = ac.d();
    a(str);
    a("send_tooltips");
    a("receive_tooltips");
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.ab
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */