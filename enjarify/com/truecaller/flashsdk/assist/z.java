package com.truecaller.flashsdk.assist;

import android.location.Location;
import android.util.Pair;
import c.g.b.k;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

public final class z
  implements y
{
  public final Pair a(Location paramLocation)
  {
    Object localObject1 = "location";
    k.b(paramLocation, (String)localObject1);
    try
    {
      localObject1 = new java/text/DecimalFormatSymbols;
      ((DecimalFormatSymbols)localObject1).<init>();
      char c = '.';
      ((DecimalFormatSymbols)localObject1).setDecimalSeparator(c);
      Object localObject2 = new java/text/DecimalFormat;
      String str = "#.#";
      ((DecimalFormat)localObject2).<init>(str);
      ((DecimalFormat)localObject2).setDecimalFormatSymbols((DecimalFormatSymbols)localObject1);
      double d = paramLocation.getLatitude();
      localObject1 = String.valueOf(d);
      localObject1 = ((DecimalFormat)localObject2).parse((String)localObject1);
      localObject1 = ((Number)localObject1).toString();
      d = paramLocation.getLongitude();
      paramLocation = String.valueOf(d);
      paramLocation = ((DecimalFormat)localObject2).parse(paramLocation);
      paramLocation = paramLocation.toString();
      localObject2 = new android/util/Pair;
      ((Pair)localObject2).<init>(localObject1, paramLocation);
      return (Pair)localObject2;
    }
    catch (ParseException localParseException) {}
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.z
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */