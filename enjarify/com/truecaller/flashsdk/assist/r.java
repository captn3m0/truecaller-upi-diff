package com.truecaller.flashsdk.assist;

import android.content.Context;
import android.net.Uri;
import c.d.a.a;
import c.d.c;
import c.g.b.k;
import c.o.b;
import okhttp3.ac;

public final class r
  implements q
{
  private final Context a;
  private final FlashBitmapConverter b;
  private final j c;
  
  public r(Context paramContext, FlashBitmapConverter paramFlashBitmapConverter, j paramj)
  {
    a = paramContext;
    b = paramFlashBitmapConverter;
    c = paramj;
  }
  
  public final Uri a(String paramString)
  {
    k.b(paramString, "extension");
    paramString = c.a(paramString);
    return c.a(paramString);
  }
  
  public final Object a(Uri paramUri, c paramc)
  {
    boolean bool = paramc instanceof r.b;
    if (bool)
    {
      localObject = paramc;
      localObject = (r.b)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        int m = b - j;
        b = m;
        break label77;
      }
    }
    Object localObject = new com/truecaller/flashsdk/assist/r$b;
    ((r.b)localObject).<init>(this, (c)paramc);
    label77:
    paramc = a;
    a locala = a.a;
    int j = b;
    switch (j)
    {
    default: 
      paramUri = new java/lang/IllegalStateException;
      paramUri.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramUri;
    case 1: 
      paramUri = (Uri)g;
      bool = paramc instanceof o.b;
      if (bool) {
        throw a;
      }
      break;
    case 0: 
      int k = paramc instanceof o.b;
      if (k != 0) {
        break label242;
      }
      if (paramUri == null) {
        return null;
      }
      paramc = b.a(paramUri);
      d = this;
      e = paramUri;
      f = paramUri;
      g = paramc;
      k = 1;
      b = k;
      paramUri = b(paramUri);
      if (paramUri == locala) {
        return locala;
      }
      paramUri = paramc;
    }
    return paramUri;
    label242:
    throw a;
  }
  
  public final ac a(Uri paramUri)
  {
    k.b(paramUri, "uri");
    f localf = new com/truecaller/flashsdk/assist/f;
    Context localContext = a;
    localf.<init>(localContext, paramUri);
    return (ac)localf;
  }
  
  public final Object b(Uri paramUri, c paramc)
  {
    boolean bool1 = paramc instanceof r.a;
    int k;
    if (bool1)
    {
      localObject = paramc;
      localObject = (r.a)paramc;
      int i = b;
      j = -1 << -1;
      i &= j;
      if (i != 0)
      {
        k = b - j;
        b = k;
        break label77;
      }
    }
    Object localObject = new com/truecaller/flashsdk/assist/r$a;
    ((r.a)localObject).<init>(this, (c)paramc);
    label77:
    paramc = a;
    a locala = a.a;
    int j = b;
    switch (j)
    {
    default: 
      paramUri = new java/lang/IllegalStateException;
      paramUri.<init>("call to 'resume' before 'invoke' with coroutine");
      throw paramUri;
    case 1: 
      bool3 = paramc instanceof o.b;
      if (bool3) {
        throw a;
      }
      break;
    case 0: 
      boolean bool2 = paramc instanceof o.b;
      if (bool2) {
        break label253;
      }
      if (paramUri == null) {
        break label242;
      }
      d = this;
      e = paramUri;
      f = paramUri;
      k = 1;
      b = k;
      paramc = b(paramUri);
      if (paramc == locala) {
        return locala;
      }
      break;
    }
    paramc = (Boolean)paramc;
    boolean bool3 = paramc.booleanValue();
    paramUri = Boolean.valueOf(bool3);
    if (paramUri != null)
    {
      bool3 = paramUri.booleanValue();
    }
    else
    {
      label242:
      bool3 = false;
      paramUri = null;
    }
    return Boolean.valueOf(bool3);
    label253:
    throw a;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.r
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */