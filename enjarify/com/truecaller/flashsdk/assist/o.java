package com.truecaller.flashsdk.assist;

import android.text.InputFilter;
import android.text.Spanned;
import c.a.ae;
import c.g.b.k;
import c.k.i;
import java.util.Collection;
import java.util.Iterator;

public final class o
  implements InputFilter
{
  public final CharSequence filter(CharSequence paramCharSequence, int paramInt1, int paramInt2, Spanned paramSpanned, int paramInt3, int paramInt4)
  {
    Object localObject1 = (Iterable)i.b(paramInt1, paramInt2);
    paramInt2 = localObject1 instanceof Collection;
    Object localObject2;
    if (paramInt2 != 0)
    {
      localObject2 = localObject1;
      localObject2 = (Collection)localObject1;
      paramInt2 = ((Collection)localObject2).isEmpty();
      if (paramInt2 != 0) {}
    }
    else
    {
      localObject1 = ((Iterable)localObject1).iterator();
      do
      {
        paramInt2 = ((Iterator)localObject1).hasNext();
        if (paramInt2 == 0) {
          break;
        }
        localObject2 = localObject1;
        localObject2 = (ae)localObject1;
        paramInt2 = ((ae)localObject2).a();
        if (paramCharSequence != null)
        {
          paramInt2 = paramCharSequence.charAt(paramInt2);
          localObject2 = Character.valueOf(paramInt2);
        }
        else
        {
          paramInt2 = 0;
          localObject2 = null;
        }
        localObject2 = String.valueOf(localObject2);
        String str = "\n";
        paramInt2 = k.a(localObject2, str);
      } while (paramInt2 == 0);
      i = 1;
      break label141;
    }
    int i = 0;
    paramCharSequence = null;
    label141:
    if (i != 0) {
      return (CharSequence)" ";
    }
    return null;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.o
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */