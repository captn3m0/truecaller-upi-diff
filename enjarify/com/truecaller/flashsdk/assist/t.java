package com.truecaller.flashsdk.assist;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import c.g.b.k;
import c.n.m;
import com.truecaller.flashsdk.R.drawable;
import com.truecaller.flashsdk.R.string;

public final class t
{
  public static final void a(EditText paramEditText)
  {
    k.b(paramEditText, "receiver$0");
    InputFilter[] arrayOfInputFilter = new InputFilter[2];
    Object localObject = new com/truecaller/flashsdk/assist/o;
    ((o)localObject).<init>();
    localObject = (InputFilter)localObject;
    arrayOfInputFilter[0] = localObject;
    localObject = new android/text/InputFilter$LengthFilter;
    ((InputFilter.LengthFilter)localObject).<init>(80);
    localObject = (InputFilter)localObject;
    arrayOfInputFilter[1] = localObject;
    paramEditText.setFilters(arrayOfInputFilter);
  }
  
  public static final void a(EditText paramEditText, c.g.a.b paramb)
  {
    k.b(paramEditText, "receiver$0");
    k.b(paramb, "onTextChangedImpl");
    Object localObject = new com/truecaller/flashsdk/assist/t$a;
    ((t.a)localObject).<init>(paramb);
    localObject = (TextWatcher)localObject;
    paramEditText.addTextChangedListener((TextWatcher)localObject);
  }
  
  public static final void a(ImageView paramImageView, Bitmap paramBitmap)
  {
    k.b(paramImageView, "receiver$0");
    k.b(paramBitmap, "image");
    float f1 = paramBitmap.getWidth();
    float f2 = 0.05F;
    int i = Math.round(f1 * f2);
    int j = Math.round(paramBitmap.getHeight() * f2);
    paramBitmap = Bitmap.createScaledBitmap(paramBitmap, i, j, false);
    Bitmap localBitmap = Bitmap.createBitmap(paramBitmap);
    Object localObject1 = RenderScript.create(paramImageView.getContext());
    k.a(localObject1, "RenderScript.create(context)");
    Object localObject2 = Element.U8_4((RenderScript)localObject1);
    localObject2 = ScriptIntrinsicBlur.create((RenderScript)localObject1, (Element)localObject2);
    paramBitmap = Allocation.createFromBitmap((RenderScript)localObject1, paramBitmap);
    localObject1 = Allocation.createFromBitmap((RenderScript)localObject1, localBitmap);
    ((ScriptIntrinsicBlur)localObject2).setRadius(7.5F);
    ((ScriptIntrinsicBlur)localObject2).setInput(paramBitmap);
    ((ScriptIntrinsicBlur)localObject2).forEach((Allocation)localObject1);
    ((Allocation)localObject1).copyTo(localBitmap);
    paramBitmap = new android/graphics/drawable/BitmapDrawable;
    localObject1 = paramImageView.getResources();
    paramBitmap.<init>((Resources)localObject1, localBitmap);
    paramBitmap = (Drawable)paramBitmap;
    paramImageView.setBackground(paramBitmap);
  }
  
  public static final void a(TextView paramTextView, SpannableStringBuilder paramSpannableStringBuilder)
  {
    Object localObject1 = paramTextView.getContext();
    int i = R.string.flash_unicode;
    localObject1 = ((Context)localObject1).getString(i);
    Object localObject2 = paramSpannableStringBuilder;
    localObject2 = (CharSequence)paramSpannableStringBuilder;
    k.a(localObject1, "flashEmoji");
    int j = 0;
    PorterDuff.Mode localMode = null;
    int k = 6;
    int m = m.a((CharSequence)localObject2, (String)localObject1, 0, false, k);
    i = -1;
    if (m != i)
    {
      localObject2 = paramTextView.getContext();
      k = R.drawable.ic_flash;
      localObject2 = android.support.v4.content.b.a((Context)localObject2, k);
      if (localObject2 != null)
      {
        localObject2 = ((Drawable)localObject2).mutate();
        if (localObject2 != null)
        {
          k = paramTextView.getLineHeight();
          ((Drawable)localObject2).setBounds(0, 0, k, k);
          int n = paramTextView.getCurrentTextColor();
          localMode = PorterDuff.Mode.SRC_IN;
          ((Drawable)localObject2).setColorFilter(n, localMode);
          paramTextView = new android/text/style/ImageSpan;
          paramTextView.<init>((Drawable)localObject2);
          i = m + 1;
          j = 33;
          paramSpannableStringBuilder.setSpan(paramTextView, m, i, j);
          return;
        }
      }
      return;
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.t
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */