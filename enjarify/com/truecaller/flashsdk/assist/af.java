package com.truecaller.flashsdk.assist;

import c.g.b.k;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public abstract class af
{
  final List a;
  final aa b;
  private final int c;
  
  public af(aa paramaa, int paramInt)
  {
    b = paramaa;
    c = paramInt;
    paramaa = new java/util/ArrayList;
    paramInt = c;
    paramaa.<init>(paramInt);
    paramaa = (List)paramaa;
    a = paramaa;
  }
  
  private Object a(int paramInt)
  {
    return a.get(paramInt);
  }
  
  private final void a(int paramInt, Object paramObject)
  {
    a.add(paramInt, paramObject);
  }
  
  protected abstract Object a(String paramString);
  
  protected abstract String b(Object paramObject);
  
  public final void b(String paramString)
  {
    k.b(paramString, "key");
    a.clear();
    paramString = b.a(paramString, "");
    StringTokenizer localStringTokenizer = new java/util/StringTokenizer;
    Object localObject = "\n";
    localStringTokenizer.<init>(paramString, (String)localObject);
    for (;;)
    {
      boolean bool = localStringTokenizer.hasMoreTokens();
      if (!bool) {
        break;
      }
      paramString = localStringTokenizer.nextToken();
      k.a(paramString, "tokenizer.nextToken()");
      paramString = a(paramString);
      localObject = a;
      int i = ((List)localObject).size();
      a(i, paramString);
    }
  }
  
  public final int c()
  {
    return a.size();
  }
  
  public final void c(Object paramObject)
  {
    List localList = a;
    boolean bool = localList.remove(paramObject);
    int i = c();
    int j = c;
    if ((i >= j) && (!bool))
    {
      localList = a;
      j += -1;
      localList.remove(j);
    }
    a(0, paramObject);
  }
  
  public final void c(String paramString)
  {
    Object localObject1 = "key";
    k.b(paramString, (String)localObject1);
    int i = c();
    if (i > 0)
    {
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      int j = 0;
      String str = null;
      i += -1;
      while (j < i)
      {
        Object localObject3 = a(j);
        localObject3 = b(localObject3);
        ((StringBuilder)localObject2).append((String)localObject3);
        localObject3 = "\n";
        ((StringBuilder)localObject2).append((String)localObject3);
        j += 1;
      }
      localObject1 = a(i);
      localObject1 = b(localObject1);
      ((StringBuilder)localObject2).append((String)localObject1);
      localObject1 = b;
      localObject2 = ((StringBuilder)localObject2).toString();
      str = "str.toString()";
      k.a(localObject2, str);
      ((aa)localObject1).a(paramString, localObject2);
    }
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.af
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */