package com.truecaller.flashsdk.assist;

import c.g.b.k;
import c.x;
import com.truecaller.flashsdk.a.d;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class ad
  extends af
  implements ae
{
  public ad(aa paramaa)
  {
    super(paramaa, i);
  }
  
  private void a(d paramd)
  {
    k.b(paramd, "object");
    super.c(paramd);
  }
  
  public final List a()
  {
    return a;
  }
  
  public final void a(int paramInt)
  {
    aa localaa = b;
    Integer localInteger = Integer.valueOf(paramInt);
    localaa.a("recent_page", localInteger);
  }
  
  public final void a(long paramLong)
  {
    Object localObject1 = String.valueOf(paramLong);
    Object localObject2 = "recent_emojis".concat((String)localObject1);
    super.b((String)localObject2);
    int i = super.c();
    if (i == 0)
    {
      localObject2 = ag.a;
      localObject1 = new java/util/ArrayList;
      int j = localObject2.length;
      ((ArrayList)localObject1).<init>(j);
      localObject1 = (Collection)localObject1;
      j = localObject2.length;
      int k = 0;
      while (k < j)
      {
        Object localObject3 = localObject2[k];
        String str = "it";
        k.a(localObject3, str);
        a((d)localObject3);
        localObject3 = x.a;
        ((Collection)localObject1).add(localObject3);
        k += 1;
      }
      b(paramLong);
    }
  }
  
  public final int b()
  {
    return b.a("recent_page", 0);
  }
  
  public final void b(long paramLong)
  {
    String str = String.valueOf(paramLong);
    str = "recent_emojis".concat(str);
    super.c(str);
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.ad
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */