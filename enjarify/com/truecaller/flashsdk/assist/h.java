package com.truecaller.flashsdk.assist;

import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build.VERSION;
import android.support.v4.app.a;
import c.u;
import com.truecaller.log.d;

public final class h
  implements g
{
  private final Context a;
  
  public h(Context paramContext)
  {
    a = paramContext;
  }
  
  private final NetworkInfo h()
  {
    try
    {
      Object localObject = a;
      String str = "connectivity";
      localObject = ((Context)localObject).getSystemService(str);
      localObject = (ConnectivityManager)localObject;
      if (localObject == null) {
        return null;
      }
      return ((ConnectivityManager)localObject).getActiveNetworkInfo();
    }
    catch (SecurityException localSecurityException) {}
    return null;
  }
  
  public final boolean a()
  {
    NetworkInfo localNetworkInfo = h();
    if (localNetworkInfo != null)
    {
      bool = localNetworkInfo.isConnected();
      if (bool) {
        return true;
      }
    }
    boolean bool = false;
    localNetworkInfo = null;
    return bool;
  }
  
  public final boolean b()
  {
    Object localObject = a;
    String str1 = "location";
    localObject = ((Context)localObject).getSystemService(str1);
    if (localObject != null)
    {
      localObject = (LocationManager)localObject;
      str1 = null;
      String str2 = "gps";
      boolean bool1;
      try
      {
        bool1 = ((LocationManager)localObject).isProviderEnabled(str2);
      }
      catch (SecurityException localSecurityException1)
      {
        d.a((Throwable)localSecurityException1);
        bool1 = false;
        str2 = null;
      }
      String str3 = "network";
      boolean bool2;
      try
      {
        bool2 = ((LocationManager)localObject).isProviderEnabled(str3);
      }
      catch (SecurityException localSecurityException2)
      {
        d.a((Throwable)localSecurityException2);
        bool2 = false;
        localObject = null;
      }
      return (!bool1) && (!bool2);
    }
    localObject = new c/u;
    ((u)localObject).<init>("null cannot be cast to non-null type android.location.LocationManager");
    throw ((Throwable)localObject);
  }
  
  public final boolean c()
  {
    int i = Build.VERSION.SDK_INT;
    int j = 26;
    return i >= j;
  }
  
  public final boolean d()
  {
    Context localContext = a;
    String str = "android.permission.CALL_PHONE";
    int i = a.a(localContext, str);
    return i == 0;
  }
  
  public final boolean e()
  {
    Context localContext = a;
    String str = "android.permission.ACCESS_FINE_LOCATION";
    int i = a.a(localContext, str);
    if (i == 0)
    {
      localContext = a;
      str = "android.permission.ACCESS_COARSE_LOCATION";
      i = a.a(localContext, str);
      if (i == 0) {
        return true;
      }
    }
    return false;
  }
  
  public final boolean f()
  {
    Context localContext = a;
    String str = "android.permission.READ_CONTACTS";
    int i = a.a(localContext, str);
    return i == 0;
  }
  
  public final boolean g()
  {
    Context localContext = a;
    String str = "android.permission.WRITE_EXTERNAL_STORAGE";
    int i = a.a(localContext, str);
    if (i == 0)
    {
      localContext = a;
      str = "android.permission.READ_EXTERNAL_STORAGE";
      i = a.a(localContext, str);
      if (i == 0) {
        return true;
      }
    }
    return false;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.h
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */