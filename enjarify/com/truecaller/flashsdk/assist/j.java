package com.truecaller.flashsdk.assist;

import android.graphics.Bitmap;
import android.net.Uri;
import java.io.File;

public abstract interface j
{
  public abstract Uri a(Bitmap paramBitmap);
  
  public abstract Uri a(File paramFile);
  
  public abstract File a(String paramString);
  
  public abstract String a(String paramString1, String paramString2);
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.j
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */