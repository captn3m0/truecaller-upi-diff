package com.truecaller.flashsdk.assist;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import c.n.m;
import com.truecaller.content.TruecallerContract;
import com.truecaller.content.TruecallerContract.a;
import com.truecaller.content.TruecallerContract.ah;
import com.truecaller.flashsdk.models.Contact;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;

public final class e
  implements d
{
  private final Context a;
  
  public e(Context paramContext)
  {
    a = paramContext;
  }
  
  /* Error */
  public final boolean a(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc 24
    //   3: invokestatic 16	c/g/b/k:b	(Ljava/lang/Object;Ljava/lang/String;)V
    //   6: iconst_0
    //   7: istore_2
    //   8: aconst_null
    //   9: astore_3
    //   10: getstatic 30	android/provider/ContactsContract$PhoneLookup:CONTENT_FILTER_URI	Landroid/net/Uri;
    //   13: astore 4
    //   15: aload_1
    //   16: invokestatic 36	android/net/Uri:encode	(Ljava/lang/String;)Ljava/lang/String;
    //   19: astore_1
    //   20: aload 4
    //   22: aload_1
    //   23: invokestatic 40	android/net/Uri:withAppendedPath	(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    //   26: astore 5
    //   28: aload_0
    //   29: getfield 22	com/truecaller/flashsdk/assist/e:a	Landroid/content/Context;
    //   32: astore_1
    //   33: aload_1
    //   34: invokevirtual 46	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   37: astore 6
    //   39: ldc 48
    //   41: astore_1
    //   42: iconst_1
    //   43: anewarray 50	java/lang/String
    //   46: dup
    //   47: iconst_0
    //   48: aload_1
    //   49: aastore
    //   50: astore 7
    //   52: aload 6
    //   54: aload 5
    //   56: aload 7
    //   58: aconst_null
    //   59: aconst_null
    //   60: aconst_null
    //   61: invokevirtual 56	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   64: astore_3
    //   65: aload_3
    //   66: ifnull +18 -> 84
    //   69: aload_3
    //   70: invokeinterface 62 1 0
    //   75: istore 8
    //   77: iload 8
    //   79: ifle +5 -> 84
    //   82: iconst_1
    //   83: istore_2
    //   84: aload_3
    //   85: ifnull +36 -> 121
    //   88: aload_3
    //   89: checkcast 65	java/io/Closeable
    //   92: astore_3
    //   93: aload_3
    //   94: invokestatic 70	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   97: goto +24 -> 121
    //   100: astore_1
    //   101: goto +22 -> 123
    //   104: astore_1
    //   105: aload_1
    //   106: checkcast 72	java/lang/Throwable
    //   109: astore_1
    //   110: aload_1
    //   111: invokestatic 77	com/truecaller/log/d:a	(Ljava/lang/Throwable;)V
    //   114: aload_3
    //   115: ifnull +6 -> 121
    //   118: goto -30 -> 88
    //   121: iload_2
    //   122: ireturn
    //   123: aload_3
    //   124: ifnull +12 -> 136
    //   127: aload_3
    //   128: checkcast 65	java/io/Closeable
    //   131: astore_3
    //   132: aload_3
    //   133: invokestatic 70	com/truecaller/utils/extensions/d:a	(Ljava/io/Closeable;)V
    //   136: aload_1
    //   137: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	138	0	this	e
    //   0	138	1	paramString	String
    //   7	115	2	bool	boolean
    //   9	124	3	localObject	Object
    //   13	8	4	localUri1	Uri
    //   26	29	5	localUri2	Uri
    //   37	16	6	localContentResolver	ContentResolver
    //   50	7	7	arrayOfString	String[]
    //   75	3	8	i	int
    // Exception table:
    //   from	to	target	type
    //   10	13	100	finally
    //   15	19	100	finally
    //   22	26	100	finally
    //   28	32	100	finally
    //   33	37	100	finally
    //   42	50	100	finally
    //   60	64	100	finally
    //   69	75	100	finally
    //   105	109	100	finally
    //   110	114	100	finally
    //   10	13	104	android/database/SQLException
    //   15	19	104	android/database/SQLException
    //   22	26	104	android/database/SQLException
    //   28	32	104	android/database/SQLException
    //   33	37	104	android/database/SQLException
    //   42	50	104	android/database/SQLException
    //   60	64	104	android/database/SQLException
    //   69	75	104	android/database/SQLException
  }
  
  public final Contact b(String paramString)
  {
    c.g.b.k.b(paramString, "phone");
    Object localObject1 = paramString;
    localObject1 = (CharSequence)paramString;
    int i = ((CharSequence)localObject1).length();
    int k = 1;
    int m = 0;
    Object localObject2 = null;
    if (i == 0)
    {
      i = 1;
    }
    else
    {
      i = 0;
      localObject3 = null;
    }
    Object localObject4 = null;
    if (i != 0) {
      return null;
    }
    Object localObject5 = "";
    m.a(paramString, "[^\\d]", (String)localObject5);
    Object localObject3 = "+";
    int n = 6;
    int i2 = m.a((CharSequence)localObject1, (String)localObject3, 0, false, n);
    i = -1;
    if (i2 == i)
    {
      localObject1 = "+";
      paramString = String.valueOf(paramString);
      paramString = ((String)localObject1).concat(paramString);
    }
    Object localObject6 = TruecallerContract.ah.b().buildUpon().appendQueryParameter("limit", "1").build();
    c.g.b.k.a(localObject6, "uri");
    String str = "data1=? AND data_type=4";
    String[] arrayOfString1 = new String[k];
    arrayOfString1[0] = paramString;
    long l1 = -1;
    localObject3 = a;
    localObject5 = ((Context)localObject3).getContentResolver();
    String[] arrayOfString2 = { "aggregated_contact_id" };
    localObject1 = ((ContentResolver)localObject5).query((Uri)localObject6, arrayOfString2, str, arrayOfString1, null);
    boolean bool1;
    if (localObject1 != null)
    {
      bool1 = ((Cursor)localObject1).moveToFirst();
      if (bool1) {
        l1 = ((Cursor)localObject1).getLong(0);
      }
    }
    if (localObject1 != null)
    {
      localObject1 = (Closeable)localObject1;
      com.truecaller.utils.extensions.d.a((Closeable)localObject1);
    }
    long l2 = 1L;
    boolean bool3 = l1 < l2;
    if (bool3) {
      return null;
    }
    localObject5 = a.getContentResolver();
    localObject6 = TruecallerContract.a.b();
    localObject3 = "contact_phonebook_id";
    String[] tmp267_264 = new String[3];
    String[] tmp268_267 = tmp267_264;
    String[] tmp268_267 = tmp267_264;
    tmp268_267[0] = "contact_name";
    tmp268_267[1] = localObject3;
    tmp268_267[2] = "contact_image_url";
    arrayOfString2 = tmp268_267;
    str = "_id=?";
    arrayOfString1 = new String[k];
    localObject1 = String.valueOf(l1);
    arrayOfString1[0] = localObject1;
    localObject1 = ((ContentResolver)localObject5).query((Uri)localObject6, arrayOfString2, str, arrayOfString1, null);
    if (localObject1 != null)
    {
      bool1 = ((Cursor)localObject1).moveToFirst();
      if (bool1)
      {
        int j = ((Cursor)localObject1).getColumnIndex("contact_name");
        localObject5 = "contact_phonebook_id";
        int i1 = ((Cursor)localObject1).getColumnIndex((String)localObject5);
        localObject6 = "contact_image_url";
        int i3 = ((Cursor)localObject1).getColumnIndex((String)localObject6);
        localObject3 = ((Cursor)localObject1).getString(j);
        if (localObject3 != null) {
          paramString = (String)localObject3;
        }
        long l3 = ((Cursor)localObject1).getLong(i1);
        localObject3 = ((Cursor)localObject1).getString(i3);
        if (localObject3 != null)
        {
          localObject5 = localObject3;
          localObject5 = (CharSequence)localObject3;
          i1 = ((CharSequence)localObject5).length();
          if (i1 == 0) {
            m = 1;
          }
          m ^= k;
          if (m == 0)
          {
            j = 0;
            localObject3 = null;
          }
          if (localObject3 != null)
          {
            localObject3 = Uri.parse((String)localObject3);
            break label485;
          }
        }
        j = 0;
        localObject3 = null;
        label485:
        Object localObject7;
        if (localObject3 != null)
        {
          localObject2 = (CharSequence)((Uri)localObject3).getHost();
          localObject5 = (CharSequence)"truecaller.com";
          boolean bool2 = org.c.a.a.a.k.f((CharSequence)localObject2, (CharSequence)localObject5);
          if (bool2)
          {
            localObject2 = "1";
            localObject5 = ((Uri)localObject3).getLastPathSegment();
            bool2 = c.g.b.k.a(localObject2, localObject5);
            if (bool2)
            {
              localObject2 = new java/util/ArrayList;
              localObject5 = (Collection)((Uri)localObject3).getPathSegments();
              ((ArrayList)localObject2).<init>((Collection)localObject5);
              i1 = ((ArrayList)localObject2).size() - k;
              ((ArrayList)localObject2).set(i1, "3");
              localObject3 = ((Uri)localObject3).buildUpon();
              localObject7 = (CharSequence)"/";
              localObject2 = (Iterable)localObject2;
              localObject7 = TextUtils.join((CharSequence)localObject7, (Iterable)localObject2);
              localObject3 = ((Uri.Builder)localObject3).path((String)localObject7).build();
              localObject7 = "fullSizeUri.buildUpon().…\", pathSegments)).build()";
              c.g.b.k.a(localObject3, (String)localObject7);
            }
          }
        }
        long l4 = 0L;
        boolean bool4 = l3 < l4;
        if (bool4)
        {
          localObject7 = TruecallerContract.b().buildUpon().appendPath("photo");
          localObject2 = "tcphoto";
          if (localObject3 != null)
          {
            localObject3 = ((Uri)localObject3).toString();
            if (localObject3 != null) {}
          }
          else
          {
            localObject3 = "";
          }
          localObject3 = ((Uri.Builder)localObject7).appendQueryParameter((String)localObject2, (String)localObject3);
          localObject7 = "pbid";
          localObject2 = String.valueOf(l3);
          localObject3 = ((Uri.Builder)localObject3).appendQueryParameter((String)localObject7, (String)localObject2).build();
        }
        if (localObject3 != null) {
          localObject4 = ((Uri)localObject3).toString();
        }
        localObject3 = new com/truecaller/flashsdk/models/Contact;
        ((Contact)localObject3).<init>(paramString, (String)localObject4);
        localObject4 = localObject3;
      }
    }
    if (localObject1 != null)
    {
      localObject1 = (Closeable)localObject1;
      com.truecaller.utils.extensions.d.a((Closeable)localObject1);
    }
    return (Contact)localObject4;
  }
}

/* Location:
 * Qualified Name:     com.truecaller.flashsdk.assist.e
 * Java Class Version: 5 (49.0)
 * JD-Core Version:    0.7.1
 */